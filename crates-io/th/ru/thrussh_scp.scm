(define-module (crates-io th ru thrussh_scp) #:use-module (crates-io))

(define-public crate-thrussh_scp-0.4.0 (c (n "thrussh_scp") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "thrussh_client") (r "^0.4") (d #t) (k 0)))) (h "03ndfl2zcpa32m9a029ibb7859rh7ca8d5g76fb09n9avh2k727w")))

(define-public crate-thrussh_scp-0.5.0 (c (n "thrussh_scp") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "thrussh_client") (r "^0.5") (d #t) (k 0)))) (h "08q4p2kr6s3b796g0zq30pinzh8rym0mnngzzsg9p6nam2g16jdy")))

