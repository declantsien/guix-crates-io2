(define-module (crates-io th ru thrussh-agent) #:use-module (crates-io))

(define-public crate-thrussh-agent-0.1.0 (c (n "thrussh-agent") (v "0.1.0") (d (list (d (n "clap") (r "^2.28") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "thrussh-keys") (r "^0.8.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.1") (d #t) (k 0)))) (h "1hni8jnp3xqj23pydgcbrv82sdvliai5y25j0ff0cglanz3fzsrl")))

