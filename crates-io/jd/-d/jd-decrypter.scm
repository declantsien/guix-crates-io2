(define-module (crates-io jd -d jd-decrypter) #:use-module (crates-io))

(define-public crate-jd-decrypter-0.1.1 (c (n "jd-decrypter") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "0p858akdzl1pb0nk4n9pwc2ddf2wvs8c3jhxgj2h48ql4r8kywnp")))

