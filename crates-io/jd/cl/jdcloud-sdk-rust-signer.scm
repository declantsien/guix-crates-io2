(define-module (crates-io jd cl jdcloud-sdk-rust-signer) #:use-module (crates-io))

(define-public crate-jdcloud-sdk-rust-signer-0.1.0 (c (n "jdcloud-sdk-rust-signer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1i19ik2l6gkmn7f7dk7d2yxwrryx6s87sycgcjdn502w5f81d06b") (f (quote (("default" "reqwest"))))))

