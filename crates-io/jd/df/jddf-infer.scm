(define-module (crates-io jd df jddf-infer) #:use-module (crates-io))

(define-public crate-jddf-infer-0.1.0 (c (n "jddf-infer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jddf") (r "^0.3") (d #t) (k 0)) (d (n "json-pointer") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i81dpcg11sdv9g9y3ia50002d7y4psma4xmdakvwsvd8hxisxcw")))

