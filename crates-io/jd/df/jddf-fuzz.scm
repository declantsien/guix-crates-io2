(define-module (crates-io jd df jddf-fuzz) #:use-module (crates-io))

(define-public crate-jddf-fuzz-0.1.0 (c (n "jddf-fuzz") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "jddf") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03rcq7676myicxjcxav27lfwyjlfbi9ql9nmc6fs5y7y30cnpjgp")))

