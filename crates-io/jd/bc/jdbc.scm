(define-module (crates-io jd bc jdbc) #:use-module (crates-io))

(define-public crate-jdbc-0.0.0 (c (n "jdbc") (v "0.0.0") (d (list (d (n "jni") (r "^0.21.1") (f (quote ("invocation"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "009zhb95l05k03v7gfpy1i434b2dbb34wanxbsl35zwcgzi6vhzp")))

(define-public crate-jdbc-0.1.0 (c (n "jdbc") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (f (quote ("invocation"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "12rw5ldkc3f6n1ikkdk6dapnl9x8b8kzmdys9ddvlidcvg4k6ry5") (f (quote (("default") ("chrono"))))))

