(define-module (crates-io u6 #{4_}# u64_array_bigints_macros) #:use-module (crates-io))

(define-public crate-u64_array_bigints_macros-0.2.0 (c (n "u64_array_bigints_macros") (v "0.2.0") (d (list (d (n "u64_array_bigints_core") (r "^0.2.0") (k 0)))) (h "1ib99bwhlv77mwz6yfxk2hzg6n4vl35mcpj5p7izgaij8g4h610d")))

(define-public crate-u64_array_bigints_macros-0.3.0 (c (n "u64_array_bigints_macros") (v "0.3.0") (d (list (d (n "u64_array_bigints_core") (r "^0.3.0") (k 0)))) (h "1g08vjx8ga5wnz2zj7nhagcq5crnhxy3n4mgviiwwxmywcmzhkn2")))

(define-public crate-u64_array_bigints_macros-0.3.1 (c (n "u64_array_bigints_macros") (v "0.3.1") (d (list (d (n "u64_array_bigints_core") (r "^0.3.1") (k 0)))) (h "0im6dfj88ragzfrjxr7wj3x4ayagnjvm1na7wj95dcqizhbzixhj")))

(define-public crate-u64_array_bigints_macros-0.3.2 (c (n "u64_array_bigints_macros") (v "0.3.2") (d (list (d (n "u64_array_bigints_core") (r "^0.3.2") (k 0)))) (h "0d8k88sfa9m7n5lg5psmyzrzmsr9jlqkwk0cr5dab8zj3da7njr1")))

(define-public crate-u64_array_bigints_macros-0.3.3 (c (n "u64_array_bigints_macros") (v "0.3.3") (d (list (d (n "u64_array_bigints_core") (r "^0.3.3") (k 0)))) (h "0hv8jhk8zlcmysxaa1j1h3nblwyynb1s2hin6dvanahnd8lygiyf")))

(define-public crate-u64_array_bigints_macros-0.3.4 (c (n "u64_array_bigints_macros") (v "0.3.4") (d (list (d (n "u64_array_bigints_core") (r "^0.3.4") (k 0)))) (h "1g7q8an0njmmfhcbv9l1c4mj05xd45awbakgwg73g8sfsi0yz7nh")))

