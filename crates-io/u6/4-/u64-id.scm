(define-module (crates-io u6 #{4-}# u64-id) #:use-module (crates-io))

(define-public crate-u64-id-0.1.0 (c (n "u64-id") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pxd2ylk0q4ng9l0xzh507pay9ik9gqbhwssmp1f1nj1wz6iwzh9") (f (quote (("default" "rand" "serde"))))))

