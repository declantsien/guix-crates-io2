(define-module (crates-io ua _g ua_generator) #:use-module (crates-io))

(define-public crate-ua_generator-0.1.0 (c (n "ua_generator") (v "0.1.0") (h "14vqkc53xp0yrsc8nji339fayn0wbizwn54glwqzqmj3yzv7k350")))

(define-public crate-ua_generator-0.1.1 (c (n "ua_generator") (v "0.1.1") (h "00s83fv031zbpn6flphpr1cyzycnmpayjcfm156hg0a586zm1a16")))

(define-public crate-ua_generator-0.1.2 (c (n "ua_generator") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "1y48ma8mv9xllyvkx94d0wz969wzd68mcvbgzblyqcpq02vgn0vc")))

(define-public crate-ua_generator-0.2.0 (c (n "ua_generator") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "099jjznqkwwr4jblnl5bgjngz4g2i8jl2w3x4i65z26xk09c118q") (y #t)))

(define-public crate-ua_generator-0.2.1 (c (n "ua_generator") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "0frzay9bk0sjh9p3dnk0a17p3c0azdc90wmzqgkcjibrgma20c1w") (y #t)))

(define-public crate-ua_generator-0.2.2 (c (n "ua_generator") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "0yf0vwi3qv757i0krx1v32ndqwwpagf07b0p3lcrs9l7kbfw0lbb")))

(define-public crate-ua_generator-0.2.3 (c (n "ua_generator") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "137hm7854hmwyq5fyddxwsp80ihrd1kzza0acaza4y6qz5lnd1kv")))

(define-public crate-ua_generator-0.2.4 (c (n "ua_generator") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "07rmzisirn9cqzqd7vviv73lk6qfvgjq66jyj028sjb0iw7dzkcr")))

(define-public crate-ua_generator-0.2.5 (c (n "ua_generator") (v "0.2.5") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "1yh5dpcmyvw7nmymjjzzpzm13hl0sdi3binmkz3yh3gj1gkx00nd")))

(define-public crate-ua_generator-0.3.0 (c (n "ua_generator") (v "0.3.0") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "1a8f6jisilm09x0m9lsynmqa5i9kgaskl1n8jvclib3xlmpjm5i1")))

(define-public crate-ua_generator-0.3.1 (c (n "ua_generator") (v "0.3.1") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "0gmf4r7m3c0mlqlvk9dbv0a7ldxzm06z8ygv238224xfprkrsqsd")))

(define-public crate-ua_generator-0.3.2 (c (n "ua_generator") (v "0.3.2") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "1i1q1z99xwsmhac2mz9hz4ilk8kx4drk9lcw1131kwna3187l8yb")))

(define-public crate-ua_generator-0.3.3 (c (n "ua_generator") (v "0.3.3") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "1crllwcjbg1m41pk9rav33ivn2hnbca8rzvycy7w2wngjslb7yzh") (y #t)))

(define-public crate-ua_generator-0.3.4 (c (n "ua_generator") (v "0.3.4") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "1v2l5xia06lh1j6yj0b4d5wnv39fd47iijzni3210pil9hnqv0yj") (y #t)))

(define-public crate-ua_generator-0.3.5 (c (n "ua_generator") (v "0.3.5") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "1s36gv66yaiwqsvivayy8caisd12fy9myaxr2466179lmxkwd48x")))

(define-public crate-ua_generator-0.3.6 (c (n "ua_generator") (v "0.3.6") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "0daz8kh7v88940s9hsir9r5awm2bfshqqgvjl5sm708s4y6vzbxn")))

(define-public crate-ua_generator-0.4.0 (c (n "ua_generator") (v "0.4.0") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "0d7gs1bqkn15abihladj0y4nbbcsryn96ga1g14axpl232b4q37n")))

(define-public crate-ua_generator-0.4.1 (c (n "ua_generator") (v "0.4.1") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 1)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "0rqhg1scficxpj1bd940qdswdi71ff5qrnbcar6chxiydli9q90d")))

(define-public crate-ua_generator-0.4.2 (c (n "ua_generator") (v "0.4.2") (d (list (d (n "fastrand") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "ureq") (r "^2") (f (quote ("json" "charset" "brotli"))) (d #t) (k 1)))) (h "0nizrxbrvbhzfwbdzmilnllbxvmcmavlbyffcfh6yl72s5mc9fi2")))

