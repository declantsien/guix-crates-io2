(define-module (crates-io ua pi uapi-version) #:use-module (crates-io))

(define-public crate-uapi-version-0.1.0 (c (n "uapi-version") (v "0.1.0") (h "194qyp9v0al6m3dd17xh69wms1wh2bl8nijnn0gfipjljldbc797")))

(define-public crate-uapi-version-0.2.0 (c (n "uapi-version") (v "0.2.0") (h "0zmm083fgrzymcmlwd191gppdljzsb15f66c4p31rvc2x7348nwx")))

(define-public crate-uapi-version-0.3.0 (c (n "uapi-version") (v "0.3.0") (h "0rhypixsiz4hlblsvvcxqp328ybq737wc5678w81wa3bk8rcgy10")))

(define-public crate-uapi-version-0.4.0 (c (n "uapi-version") (v "0.4.0") (h "0r9vmizrz93l8ihqckizzdi59b1cyyng7mrp0wbhgyx0x0gnp7w4")))

