(define-module (crates-io ua pi uapi) #:use-module (crates-io))

(define-public crate-uapi-0.1.0 (c (n "uapi") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.1") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "10br7h59yqbs0d2nipy9msprhsajxc8kj191x20xj12a7z960cdw")))

(define-public crate-uapi-0.1.1 (c (n "uapi") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.1") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1gdgkajymaax7ka8nzzwn82jwb96zvbcq17kwixqkdg2l1mmn6yr")))

(define-public crate-uapi-0.1.2 (c (n "uapi") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.1") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "0gdmn9p8qxsh8ky0b3y3vrs1fwknsn05v33xv5c13wpr0gj7bib5")))

(define-public crate-uapi-0.1.3 (c (n "uapi") (v "0.1.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.1") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "0hsgvfr5g2lj95xfjmp6vqyh9qc2z9d348lgdsna5qiwsjffysxn")))

(define-public crate-uapi-0.2.0 (c (n "uapi") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.1") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "0hw70xs0n74jv9cij6v1aqzy6chw7ghy2d99w8y9jljzci7h62jl")))

(define-public crate-uapi-0.2.1 (c (n "uapi") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.2") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "016mr0irc4n6fks8zi1gs6ls4rzw5f9i766yv0v1rr8zvwanmnk3")))

(define-public crate-uapi-0.2.2 (c (n "uapi") (v "0.2.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.2") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "15hmql73aqydf2hari667if63pz8s38ycpzbx95j7c9zvc1xwjy1")))

(define-public crate-uapi-0.2.3 (c (n "uapi") (v "0.2.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.3") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "064n3balz0y00ib9c8nmnk3kink7l7z4r5ji813jjb8b72bi5lbs")))

(define-public crate-uapi-0.2.4 (c (n "uapi") (v "0.2.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.3") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1z910vamir2h1121nr6y4k8dg3vp72aqwfqx676q91b74v2dhwhw")))

(define-public crate-uapi-0.2.5 (c (n "uapi") (v "0.2.5") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.4") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1vh3c327hbzaq2z19pfzq5an1dlf3agyq75nqlizr1k9v8cly4g0")))

(define-public crate-uapi-0.2.6 (c (n "uapi") (v "0.2.6") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.5") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "06hglbhd2jdpkskqg25lphfh458gny59sllg234h5lxhwy457hvd")))

(define-public crate-uapi-0.2.7 (c (n "uapi") (v "0.2.7") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.5") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "0ncvbnn5idy6arbr7nwg1xn397nj4i0zjl4jfvvxqgavhj6n0apa")))

(define-public crate-uapi-0.2.8 (c (n "uapi") (v "0.2.8") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.5") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "17sqhxchban1m75ni0a1nf56p2c2fjf1c9v4bndi0c052ii4dhhf")))

(define-public crate-uapi-0.2.9 (c (n "uapi") (v "0.2.9") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.5") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "0n2vm929wh3m4flc240md7hs7nx7cg2yhyf59fpddph0r0psn73y")))

(define-public crate-uapi-0.2.10 (c (n "uapi") (v "0.2.10") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.5") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "0x1j2kxpy3bmi0vvi5w1w67s60mhmvxzfixwlpi45lq10hj51501")))

(define-public crate-uapi-0.2.12 (c (n "uapi") (v "0.2.12") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.5") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1y19f0gh5m92k3wdmdky9jzc2prqxjgw65fjfd68m66iy77167v5")))

(define-public crate-uapi-0.2.13 (c (n "uapi") (v "0.2.13") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc") (r "^0.0.5") (d #t) (k 0) (p "uapi-proc")) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1rk1cl9y624phvgjl59zqnd8vqyfxi9fgd18zrdlh5hv1n277w1v")))

