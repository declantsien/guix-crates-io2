(define-module (crates-io ua rt uart16550) #:use-module (crates-io))

(define-public crate-uart16550-0.0.0 (c (n "uart16550") (v "0.0.0") (h "1zvhmkn9r8q477lyhk0a4mc9yzx2in06k2hb0qm55rphd5nl3p4r")))

(define-public crate-uart16550-0.0.1 (c (n "uart16550") (v "0.0.1") (h "16zvpw8ff75n66ra5g5igwwc080nxkr0dzd8zhzgw5fqraf6z7wk")))

