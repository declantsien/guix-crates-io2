(define-module (crates-io ua rt uart) #:use-module (crates-io))

(define-public crate-uart-0.1.0 (c (n "uart") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)))) (h "1j16arv7zhjds2xr6dnpjb9jx8vggfwwwpr6pxxr4fh6ca4wm4j7") (y #t)))

(define-public crate-uart-0.1.1 (c (n "uart") (v "0.1.1") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)))) (h "07v69fif2dyn3lcirjvdllyjni0xpnni7fyxfwq3mgkxgw4fm249") (y #t)))

(define-public crate-uart-0.1.2 (c (n "uart") (v "0.1.2") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)))) (h "02g3lfpvsr955vi62nsg1vivms868sw1nfycbcjj32yqgpq7qyfg")))

