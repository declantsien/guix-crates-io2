(define-module (crates-io ua rt uart_xilinx) #:use-module (crates-io))

(define-public crate-uart_xilinx-0.1.0 (c (n "uart_xilinx") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "0zniv0bac4ch7qw0ijninps38h7dmrisc0qqbqm17agm4i4x6p9q") (f (quote (("fmt") ("default"))))))

(define-public crate-uart_xilinx-0.2.0 (c (n "uart_xilinx") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "0akz8s3lrm4cycmdmm5apf06gjqk5vw2kpyiw2cpv5qlzf99sknc") (f (quote (("fmt") ("default"))))))

