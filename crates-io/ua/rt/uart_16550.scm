(define-module (crates-io ua rt uart_16550) #:use-module (crates-io))

(define-public crate-uart_16550-0.1.0 (c (n "uart_16550") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.2.0-alpha-019") (d #t) (k 0)))) (h "0vlnrim7w68g5rg3nz4w845d30ry99xrqn2w0ry6y8p3ilyrb7r6")))

(define-public crate-uart_16550-0.2.0 (c (n "uart_16550") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "x86_64") (r "^0.5.4") (d #t) (k 0)))) (h "1c4m9lrp2rdni9v4hk6ny5q8flji9zp1a2jfybw3pzii17v954sv")))

(define-public crate-uart_16550-0.2.1 (c (n "uart_16550") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.7.3") (d #t) (k 0)))) (h "12x52bgcgp5lgikarchgmfz7vzkrvp96da2plv0k5frdc35shgl0")))

(define-public crate-uart_16550-0.2.2 (c (n "uart_16550") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.8.3") (d #t) (k 0)))) (h "0czb8cvz3vmbbx893ysndcvw3xpn8x5rrkn4x0v19sqvzz40pz2q")))

(define-public crate-uart_16550-0.2.3 (c (n "uart_16550") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.9") (d #t) (k 0)))) (h "0wzyjd6p7rn8qk3r150apak9dlkif8n4g95f5140pcn7p7s7kmam")))

(define-public crate-uart_16550-0.2.4 (c (n "uart_16550") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.9.3") (k 0)))) (h "0cbi7mpzz66bmjw8rg1843arlli6883f3bbqbg0zpc42rcq0yjyl") (f (quote (("stable" "x86_64/stable") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2.5 (c (n "uart_16550") (v "0.2.5") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.9.3") (k 0)))) (h "0lmfyzglgqnjd25d5nbfhc1cy1mp3xszqshsyaiwjgl7f20ni0z9") (f (quote (("stable" "x86_64/stable") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2.6 (c (n "uart_16550") (v "0.2.6") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.10.2") (k 0)))) (h "15xiwkh0flhm51k73iqqbcprqvc71m4r9f8agfjilpmmg5xmwpib") (f (quote (("stable" "x86_64/stable") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2.7 (c (n "uart_16550") (v "0.2.7") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.11.0") (k 0)))) (h "1sfzyf91wpm0h1d29yr416b4kar15z5dhyqakgy689kiq46w93z5") (f (quote (("stable" "x86_64/stable") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2.8 (c (n "uart_16550") (v "0.2.8") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.12.1") (f (quote ("instructions"))) (k 0)))) (h "0indvzhdbcdi7ihdsq7dy431pwhzjbvswh34zijc07labriaijvl") (f (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/nightly") ("default" "x86_64/default"))))))

(define-public crate-uart_16550-0.2.9 (c (n "uart_16550") (v "0.2.9") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.12.2") (f (quote ("instructions"))) (k 0)))) (h "0gbn78fqsmss7hljsc4hvfyv2fdl75h12x3r5xvzijqmjacpfxy0") (f (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/nightly") ("default" "x86_64/default"))))))

(define-public crate-uart_16550-0.2.10 (c (n "uart_16550") (v "0.2.10") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.12.2") (f (quote ("instructions"))) (k 0)))) (h "037pmj9sydm2ck7fsqv5gklcf1q5khgxmzlcxg2bzb68lhb0pc5g") (f (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2.11 (c (n "uart_16550") (v "0.2.11") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.12.2") (f (quote ("instructions"))) (k 0)))) (h "15xy71wqnkfhc9vja4i181b1w76163hd82662xhscnllfzwcvf53") (f (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2.12 (c (n "uart_16550") (v "0.2.12") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.13.2") (f (quote ("instructions"))) (k 0)))) (h "1nbnp7sd5cqga537jnwkkll5ahb0s4v3qayh6myvmj8x334s6icd") (f (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2.13 (c (n "uart_16550") (v "0.2.13") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.0") (f (quote ("instructions"))) (k 0)))) (h "0yr6jhvbw5wnpf8inhddny87r4mskcwdwzlxym0ampmvkna376p0") (f (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2.14 (c (n "uart_16550") (v "0.2.14") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.0") (f (quote ("instructions"))) (k 0)))) (h "1p85aklqmcl14yr8v9ljgic0jsqpq0hd2qp6hmwsinl2dl76qfjh") (f (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2.15 (c (n "uart_16550") (v "0.2.15") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.0") (f (quote ("instructions"))) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "0x7k8kc3j5yvh54kk0clchbz6z6qkhjnlvvdwvzzhpzgh2a03bb5") (f (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2.16 (c (n "uart_16550") (v "0.2.16") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.0") (f (quote ("instructions"))) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "0nya1ds2xyg6351xmyb5i4n8zl9gj601yf4qc7vb1iskka5490dg") (f (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2.17 (c (n "uart_16550") (v "0.2.17") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.0") (f (quote ("instructions"))) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "109jrzad0fffz3z5qdbnm2dkdy6r44f6kdb5b2dc7a9qpdfkj5in") (f (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2.18 (c (n "uart_16550") (v "0.2.18") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.5") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.9") (f (quote ("instructions"))) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "1nb32gf757z6byvfcj5r3gvmlqjisklc0af5fkfrx55d029ynx5h") (f (quote (("stable") ("nightly") ("default"))))))

(define-public crate-uart_16550-0.2.19 (c (n "uart_16550") (v "0.2.19") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.5") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.9") (f (quote ("instructions"))) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "0bmbair385wl8cqgd2zdbk8bl2wpi9cnh8kjfi1vvm40g2lg4kv1") (f (quote (("stable") ("nightly") ("default"))))))

(define-public crate-uart_16550-0.3.0 (c (n "uart_16550") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.5") (d #t) (k 0)) (d (n "x86") (r "^0.52") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "11w6nzi7ila0d4w05b02368gkny4x4splfc5gks72v3gg5209h3d") (f (quote (("stable") ("nightly") ("default"))))))

