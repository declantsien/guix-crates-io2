(define-module (crates-io ua rt uart8250) #:use-module (crates-io))

(define-public crate-uart8250-0.1.0 (c (n "uart8250") (v "0.1.0") (d (list (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "1ldvid4xcgikj39j83cy1xrjkd81lrgaxz9rq0xxxwd95zh7gyp8")))

(define-public crate-uart8250-0.2.0 (c (n "uart8250") (v "0.2.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (o #t) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "1ss3asq5jxr26wvrvwra8zlszaaj1hncgjnvn5k00gkrc8cbpya9") (f (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.3.0 (c (n "uart8250") (v "0.3.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (o #t) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "1i3lgfilr6l92kw6bi91wa87kcymjxrd2jl61ny325dzrpvd897h") (f (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.3.1 (c (n "uart8250") (v "0.3.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (o #t) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "1jmg197j02c7ayzj9n65x0d9lp368ph60mk7abcamvh2wgg6jq00") (f (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.3.2 (c (n "uart8250") (v "0.3.2") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (o #t) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "0m0a87xxad591dvbajggs4y899l173cmv24zw8sqc26gjp2iqz3x") (f (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.4.0 (c (n "uart8250") (v "0.4.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (o #t) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "0dgv2jf4pxq7l1gy993279ng01m1k79ahsdrvkdjxmjqg12md9nq") (f (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.4.1 (c (n "uart8250") (v "0.4.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (o #t) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "12dibdmy62267vq7yylcmkf04vg1yl2gynq2psvnm2i4gg0wv70c") (f (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.4.2 (c (n "uart8250") (v "0.4.2") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (o #t) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "13d80h518s08mdzr4kp88lyv0hawbcb75b3rya6qw3gl5zhpfrs8") (f (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.5.0 (c (n "uart8250") (v "0.5.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "140vacv5m0wmlbxk028spkxp1ca5f0n2s2042v2j43xyih17d7gp") (f (quote (("fmt") ("default"))))))

(define-public crate-uart8250-0.6.0 (c (n "uart8250") (v "0.6.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "0ikfcp46drngr04jj525p39hjf8wavjfnds39xnl3q8qhkzj1xnh") (f (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

