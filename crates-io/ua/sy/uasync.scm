(define-module (crates-io ua sy uasync) #:use-module (crates-io))

(define-public crate-uasync-0.1.0 (c (n "uasync") (v "0.1.0") (d (list (d (n "async-channel") (r "^1") (d #t) (k 2)) (d (n "async-executor") (r "^1") (d #t) (k 2)) (d (n "async-task") (r "^4") (d #t) (k 2)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "easy-parallel") (r "^3") (d #t) (k 2)) (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync"))) (d #t) (k 2)))) (h "1i3vmkhfn930865pl4r6rfabl5ab725xnjxxry22s6fhlsiv7q4g")))

(define-public crate-uasync-0.1.1 (c (n "uasync") (v "0.1.1") (d (list (d (n "async-channel") (r "^1") (d #t) (k 2)) (d (n "async-executor") (r "^1") (d #t) (k 2)) (d (n "async-task") (r "^4") (d #t) (k 2)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "easy-parallel") (r "^3") (d #t) (k 2)) (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync"))) (d #t) (k 2)))) (h "1i1l9x9kmqz05dr5mwg9790199gd492rf478dzpigpg98zzpcks3")))

