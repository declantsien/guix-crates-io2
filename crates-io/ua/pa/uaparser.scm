(define-module (crates-io ua pa uaparser) #:use-module (crates-io))

(define-public crate-uaparser-0.2.2 (c (n "uaparser") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "onig") (r "^4.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)))) (h "127x91b5illb6gwz8z1ipmsvakjqrnf5g2qxf6li7f7apkdw5sg2")))

(define-public crate-uaparser-0.2.3 (c (n "uaparser") (v "0.2.3") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "onig") (r "^4.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)))) (h "0dgb1hwlwljxx4jaaikb17pbbmfd40f74nk36425hslm07qydc30")))

(define-public crate-uaparser-0.3.0 (c (n "uaparser") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "onig") (r "^4.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)))) (h "0gwdxdi5j2jxdljzf7838nr16819c9f1m3awy608wsn9dlcarc4j")))

(define-public crate-uaparser-0.3.1 (c (n "uaparser") (v "0.3.1") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "onig") (r "^4.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)))) (h "0q6g58sb94bpjp1g1qhk8dcykk7mhxnd70ls8gjxdbzv535j0dsh")))

(define-public crate-uaparser-0.3.2 (c (n "uaparser") (v "0.3.2") (d (list (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "onig") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "1kadd9zfy03lvfs41py24jd49vfqng5w30qb5vqy7mx3bxrmibib")))

(define-public crate-uaparser-0.3.3 (c (n "uaparser") (v "0.3.3") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)))) (h "12lfhb3al5cc7i7gaxj7kp5rm90w53x5gcm1wzirpdcrbcxn723c")))

(define-public crate-uaparser-0.3.4 (c (n "uaparser") (v "0.3.4") (d (list (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)))) (h "0h989vabpl6jkd94ny5wq3gbhkc0rkxd8w1qzk4x91lq6zffm3ar")))

(define-public crate-uaparser-0.4.0 (c (n "uaparser") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.99.7") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "1rsxgr28vqq3wlfddv3q1dzrasbc49acs1dj9nn92cw9y7vc6dhz")))

(define-public crate-uaparser-0.5.0 (c (n "uaparser") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "01gj5swf494vvq030ar9jhqa6gi3jyc16rqdchgrlhvm5bjjjwkq")))

(define-public crate-uaparser-0.5.1 (c (n "uaparser") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "0w89ai52lhhimb8iixwd6q500lqxla8b1ym6pr6j7j45bhq31kmp")))

(define-public crate-uaparser-0.6.0 (c (n "uaparser") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)))) (h "1i9cq0piqpxgmqx562rca3ffk7572r9szscx566j8cjx8np0bmy3")))

(define-public crate-uaparser-0.6.1 (c (n "uaparser") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)))) (h "09v97z8fkya449v23m636dajz9n3zpl89sbrr3bgmm1l0ixlwsfg")))

(define-public crate-uaparser-0.6.2 (c (n "uaparser") (v "0.6.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)))) (h "0y06wqnpsmkifgdks4zxkw9mkxa3nv50xbx2ns8vd7bcp7cxx01l")))

(define-public crate-uaparser-0.6.3 (c (n "uaparser") (v "0.6.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0iam0flcncnaa0y7vi1kx2ac9d2a12pm65fgrrsasp38z76qyk9a")))

