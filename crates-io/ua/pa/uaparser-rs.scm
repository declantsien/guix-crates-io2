(define-module (crates-io ua pa uaparser-rs) #:use-module (crates-io))

(define-public crate-uaparser-rs-0.1.0 (c (n "uaparser-rs") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)))) (h "1jb1ffvm3af80y1gfnwa4lxq51gq15iyaj11ghl9wjbl0rrg8zqx")))

