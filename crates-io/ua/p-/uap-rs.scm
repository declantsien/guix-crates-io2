(define-module (crates-io ua p- uap-rs) #:use-module (crates-io))

(define-public crate-uap-rs-0.1.0 (c (n "uap-rs") (v "0.1.0") (d (list (d (n "onig") (r "^4.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)))) (h "168dl6bmhn5892vndpjvgmxvandjv3g33drgkwsq4rcr5qg2kimm") (y #t)))

(define-public crate-uap-rs-0.2.0 (c (n "uap-rs") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "onig") (r "^4.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)))) (h "0qj5934wc2dnw64qdy5bq9xzi2isyn2wci5g38h2y11wpl8vazvi") (y #t)))

(define-public crate-uap-rs-0.2.1 (c (n "uap-rs") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "onig") (r "^4.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)))) (h "04wjz1asm8888ywpdl96gy6z4qmjn69xhcjxwydqpnkfs9lj3f3w") (y #t)))

(define-public crate-uap-rs-0.2.2 (c (n "uap-rs") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "onig") (r "^4.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)))) (h "0v2azy4fsjlkmw4jqpclhj4mqxgb7pcimvirv08825qin2lpl60i") (y #t)))

(define-public crate-uap-rs-0.0.0 (c (n "uap-rs") (v "0.0.0") (h "1j4j4908nwnng3c7w54vswslw30gz01h0x9xjnm8piwc677lx68q")))

