(define-module (crates-io ua p- uap-rust) #:use-module (crates-io))

(define-public crate-uap-rust-0.0.1 (c (n "uap-rust") (v "0.0.1") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "0fy5lnpx5wvha2y0bp39n3y912w8qwnc2lvvi2cpghxqax7887z3")))

(define-public crate-uap-rust-0.0.2 (c (n "uap-rust") (v "0.0.2") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "1ha5c7lzdvk2h7097ryh9wiiiy8awwivl01kk1k7mfa6zakxx885")))

(define-public crate-uap-rust-0.0.3 (c (n "uap-rust") (v "0.0.3") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "1srflkr6dimdmdq1b29vs5zdcf8ahrql5qvlnzqvg8q5sjy94iwc")))

(define-public crate-uap-rust-0.0.4 (c (n "uap-rust") (v "0.0.4") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "0v8d89d5f5b8nzfmmpcydwbyqm5v52v1nd8hiwcxwbd2917s539f")))

