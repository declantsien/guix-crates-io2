(define-module (crates-io ua bs uabs) #:use-module (crates-io))

(define-public crate-uabs-1.0.0 (c (n "uabs") (v "1.0.0") (d (list (d (n "signrel") (r "^1.0.0") (d #t) (k 0)))) (h "1qppax9qp7qz6vz9rf7qks0k0rc22aq1w89xl0c621qxf9y2yfxf")))

(define-public crate-uabs-2.0.0 (c (n "uabs") (v "2.0.0") (d (list (d (n "signrel") (r "^1.0.0") (d #t) (k 0)))) (h "13amml8dd3i1704s97s3wr7xayh2ny6jx9g7b6hr0v0ccxr54l06")))

(define-public crate-uabs-3.0.0 (c (n "uabs") (v "3.0.0") (d (list (d (n "signrel") (r "^2.0") (d #t) (k 0)))) (h "1hm8gqs5j4k3fddli5sjd6261lk5147j7fl2ba6fwr54933igg0g")))

