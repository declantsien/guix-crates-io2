(define-module (crates-io ua vc uavcan-core) #:use-module (crates-io))

(define-public crate-uavcan-core-0.0.1 (c (n "uavcan-core") (v "0.0.1") (d (list (d (n "bit_field") (r "^0.8.0") (d #t) (k 0)) (d (n "uavcan-derive") (r "^0.0.1") (d #t) (k 0)))) (h "0nqrmv93zg3wymfj6jygfyw7jzzirxgdlxi9wfmx8ymkqv6gm58f")))

