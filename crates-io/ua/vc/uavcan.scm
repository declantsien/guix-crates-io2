(define-module (crates-io ua vc uavcan) #:use-module (crates-io))

(define-public crate-uavcan-0.0.1 (c (n "uavcan") (v "0.0.1") (d (list (d (n "uavcan-core") (r "^0.0.1") (d #t) (k 0)))) (h "0jdzpq5pqq3xx6pbcm7sv7912icy5ivqn65iici29wv9hmp59z9a")))

(define-public crate-uavcan-0.1.0-preview0 (c (n "uavcan") (v "0.1.0-preview0") (d (list (d (n "bit_field") (r "^0.8.0") (d #t) (k 0)) (d (n "embedded_types") (r "^0.3.0") (d #t) (k 0)) (d (n "half") (r "^1.0.0") (k 0)) (d (n "uavcan-derive") (r "^0.1.0-preview0") (d #t) (k 0)) (d (n "ux") (r "^0.0.1") (k 0)))) (h "1rwgdgzcy485sc8ch842dvp5bf9kvw5n0kpak0l1748mggx8mmhy") (f (quote (("std" "ux/std" "half/std") ("default"))))))

