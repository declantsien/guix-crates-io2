(define-module (crates-io ua vc uavcan-derive) #:use-module (crates-io))

(define-public crate-uavcan-derive-0.0.1 (c (n "uavcan-derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "0n43y4xhb1x5bqchc80narsxb1q4mqgzrv34s70wj7gsiwszs36h")))

(define-public crate-uavcan-derive-0.1.0-preview0 (c (n "uavcan-derive") (v "0.1.0-preview0") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "149qc827piq8psrbzjpvkc3pp48bi10hfbpxfggcaj7b1zkwik83")))

