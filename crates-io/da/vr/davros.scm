(define-module (crates-io da vr davros) #:use-module (crates-io))

(define-public crate-davros-0.0.0 (c (n "davros") (v "0.0.0") (d (list (d (n "curve25519-dalek") (r "^0.13") (d #t) (k 0)))) (h "0gn33700is5xvq1ar43fi692c4xg6yngz2rp76da630s69plm35l") (f (quote (("std" "curve25519-dalek/std") ("nightly" "curve25519-dalek/radix_51") ("default" "std") ("bench"))))))

