(define-module (crates-io da ir dairy) #:use-module (crates-io))

(define-public crate-dairy-0.1.0 (c (n "dairy") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1c9xsmz06aqfaki0zzi2hvksjkzdnh0f49q6afq7fnaswk4lh4fj") (f (quote (("unix" "std") ("std") ("default" "std"))))))

(define-public crate-dairy-0.1.1 (c (n "dairy") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "14y7dnmyl0nw830p3bv2xasfq5j2xr7rpzbdbf52cxhkl7sd08ba") (f (quote (("unix" "std") ("std") ("default" "std"))))))

(define-public crate-dairy-0.2.0 (c (n "dairy") (v "0.2.0") (d (list (d (n "beef") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0kjdkq20xjl2s6dg6v2638q33hbaw1kp79fbcrhgj26hbyrfhq89") (f (quote (("std") ("default" "std"))))))

(define-public crate-dairy-0.2.1 (c (n "dairy") (v "0.2.1") (d (list (d (n "beef") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1g7hqs5phm68ipf8pmqhwdw5kkkwq0yd0h8zpcnbd2pwirbja0ly") (f (quote (("std") ("default" "std"))))))

(define-public crate-dairy-0.2.2 (c (n "dairy") (v "0.2.2") (d (list (d (n "beef") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0grxmb6wwzw6pyzmfgi5vyk3y9ihdvpvy3s1bk504w7mq405p29k") (f (quote (("std") ("default" "std"))))))

