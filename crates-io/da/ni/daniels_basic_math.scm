(define-module (crates-io da ni daniels_basic_math) #:use-module (crates-io))

(define-public crate-daniels_basic_math-0.1.0 (c (n "daniels_basic_math") (v "0.1.0") (h "1r5pv5fcymqsyvxp6h1fk9mbamf0fviqbybmmfbqaaghppv4zmlq") (y #t)))

(define-public crate-daniels_basic_math-0.1.1 (c (n "daniels_basic_math") (v "0.1.1") (h "1p5fc9nrvrlydk4wl31yawjsh2glaghszd3ilnipg6pb8cv46shm")))

