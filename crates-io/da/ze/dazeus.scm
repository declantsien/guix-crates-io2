(define-module (crates-io da ze dazeus) #:use-module (crates-io))

(define-public crate-dazeus-0.1.2 (c (n "dazeus") (v "0.1.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "unix_socket") (r "^0.2") (d #t) (k 0)))) (h "1mywpwq9ldkk3jq20a6sdd5j6vjly4bdi6hrn79j2hsglga9z5af")))

(define-public crate-dazeus-0.2.0 (c (n "dazeus") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3") (d #t) (k 0)))) (h "1ghlrk82h6jsvy8k2sbpw52m22jygk8cg1zq3wjlsrw8ww411l11")))

(define-public crate-dazeus-0.2.1 (c (n "dazeus") (v "0.2.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3") (d #t) (k 0)))) (h "0w72yhgkba04bf5qhak0d4kkhygaf7n4bkppyfqz3lsn3vllmyfs")))

(define-public crate-dazeus-0.3.0 (c (n "dazeus") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3") (d #t) (k 0)))) (h "0i0l88l25cs06q7cf668n7n01iqss6skx44hd4k2izm6v5xg5923")))

(define-public crate-dazeus-0.3.1 (c (n "dazeus") (v "0.3.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3") (d #t) (k 0)))) (h "0p6zf2v9995ly9rbd37cby5h62rng9mcpbqqz9bdfr5h3qxd83kz")))

(define-public crate-dazeus-0.3.2 (c (n "dazeus") (v "0.3.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3") (d #t) (k 0)))) (h "1cq90lw9kafxcb0gss8dzsand6s3aazj9rw2f7881x94l8aa7nfa")))

(define-public crate-dazeus-0.3.3 (c (n "dazeus") (v "0.3.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3") (d #t) (k 0)))) (h "059r9am5mmbsk8np2k85izmcsfr95s06qn0fgkwx2fflpp4gxqci")))

(define-public crate-dazeus-0.4.0 (c (n "dazeus") (v "0.4.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3") (d #t) (k 0)))) (h "1vqgxxsmk1nr29hi0p2d5yn04lfpazr8s8xkcppmz3irq20fx1hw")))

