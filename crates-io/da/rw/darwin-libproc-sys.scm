(define-module (crates-io da rw darwin-libproc-sys) #:use-module (crates-io))

(define-public crate-darwin-libproc-sys-0.1.0 (c (n "darwin-libproc-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rin4gdsfq33vizc03xssjg6259f68x7rzk4jysr1sayzlccqri7") (l "proc")))

(define-public crate-darwin-libproc-sys-0.1.1 (c (n "darwin-libproc-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h8m0cxx03hdflxvad43k79i5i5g0y5qmv9gn21i3nklil3il3f3") (l "proc")))

(define-public crate-darwin-libproc-sys-0.1.2 (c (n "darwin-libproc-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hb712hrwscxnlnwgk4297dib2r31399qjyw1p9wvvk6vrdvpkjp") (l "proc")))

(define-public crate-darwin-libproc-sys-0.2.0 (c (n "darwin-libproc-sys") (v "0.2.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "05j87fxcfn054y0ay4f53nfi6k3iarwd7gwvzm6alm2cp61s02pg") (l "proc")))

