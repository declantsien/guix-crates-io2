(define-module (crates-io da rw darwin-notify) #:use-module (crates-io))

(define-public crate-darwin-notify-0.1.0 (c (n "darwin-notify") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (o #t) (d #t) (k 0)))) (h "1sigrljkkrmz15vg055j5vsh8w2b3mzzrh8nj3fmakzlfvkgfahp") (f (quote (("sys") ("default")))) (s 2) (e (quote (("tracing" "dep:tracing"))))))

