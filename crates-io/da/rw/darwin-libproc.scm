(define-module (crates-io da rw darwin-libproc) #:use-module (crates-io))

(define-public crate-darwin-libproc-0.1.0 (c (n "darwin-libproc") (v "0.1.0") (d (list (d (n "darwin-libproc-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1g1yr7v6kwl65azg90lkyjnb9fb1lz27y3ls7wvdbxkidh5lnsvm")))

(define-public crate-darwin-libproc-0.1.1 (c (n "darwin-libproc") (v "0.1.1") (d (list (d (n "darwin-libproc-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (d #t) (k 0)))) (h "0c8agzrimk8n60vv025a2132n3wa92lj2wv8f3vnnr6rz25airdd")))

(define-public crate-darwin-libproc-0.1.2 (c (n "darwin-libproc") (v "0.1.2") (d (list (d (n "darwin-libproc-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (d #t) (k 0)))) (h "0iih7hsc00fgyw75j40cs9saq8wf0i922xl5wl4hz6hcjd8h1fcz")))

(define-public crate-darwin-libproc-0.2.0 (c (n "darwin-libproc") (v "0.2.0") (d (list (d (n "darwin-libproc-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "memchr") (r "~2.3") (d #t) (k 0)))) (h "05n5j0mc3jszprhsl5h02zqgapzsffmzjcdf3pizx1i5yiy9nqnc")))

