(define-module (crates-io da co daco) #:use-module (crates-io))

(define-public crate-daco-0.1.0 (c (n "daco") (v "0.1.0") (h "1f624d22dglw9kjdgy8sj3mmfmd121945c6csw1j856d1nzq8wxx") (y #t)))

(define-public crate-daco-0.1.1 (c (n "daco") (v "0.1.1") (h "030l8rn46i5pg079b3dgbgb65w1vy36kpbyc7xcy9avnwdv9cqpl") (y #t)))

