(define-module (crates-io da sh dash2html) #:use-module (crates-io))

(define-public crate-dash2html-1.0.0 (c (n "dash2html") (v "1.0.0") (d (list (d (n "regex") (r "^0.1.43") (d #t) (k 0)) (d (n "rusqlite") (r "^0.5.0") (d #t) (k 0)))) (h "1da150bd87civbfq7axixiv16frq8y6r1ksxf5wbq050qvfb3hgg")))

(define-public crate-dash2html-1.0.1 (c (n "dash2html") (v "1.0.1") (d (list (d (n "regex") (r "^0.1.43") (d #t) (k 0)) (d (n "rusqlite") (r "^0.5.0") (d #t) (k 0)))) (h "12csf98jy973gv8p3z531lgzjh5n2w8410f3i7wip52c2gxnp9hy")))

(define-public crate-dash2html-1.1.0 (c (n "dash2html") (v "1.1.0") (d (list (d (n "regex") (r "^0.1.43") (d #t) (k 0)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 0)))) (h "00q59kc5js34mq9sc7v0kdlzjh90dy1f1qb56q0w6jmazpvdxpyv")))

