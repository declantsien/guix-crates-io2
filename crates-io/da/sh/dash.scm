(define-module (crates-io da sh dash) #:use-module (crates-io))

(define-public crate-dash-0.1.0 (c (n "dash") (v "0.1.0") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h0xf0xd90iaknw4ch8bnbrn37i1kiyhfi6mwrfi9m0iccy2c24a")))

