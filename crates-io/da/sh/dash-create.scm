(define-module (crates-io da sh dash-create) #:use-module (crates-io))

(define-public crate-dash-create-1.0.0 (c (n "dash-create") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0k8xbn8s9b0qhadkjaf69xjb8n1db7xbmajjsdynajv9k0l4k469")))

(define-public crate-dash-create-1.1.0 (c (n "dash-create") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "06n37hsp48gvdxkr1z4zwvjsqvcckc8pb52inhbw47dgp0f0r5jx")))

(define-public crate-dash-create-1.1.1 (c (n "dash-create") (v "1.1.1") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "08nwh41zw4zpg39y9sa79dszs76dk1rf29ajrn35wbyj2jvhbl2k")))

(define-public crate-dash-create-1.1.2 (c (n "dash-create") (v "1.1.2") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0bxfbblczbsfmxr2izzfn6bypiq8pl44dv87nrgkng80a1x016lm")))

(define-public crate-dash-create-1.1.3 (c (n "dash-create") (v "1.1.3") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "16n9phvkahh7sxdvldwzw8s2ma3pcc4c7gfc4ijcp3rdvd5arskw")))

