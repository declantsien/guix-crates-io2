(define-module (crates-io da sh dashbutton) #:use-module (crates-io))

(define-public crate-dashbutton-0.1.0 (c (n "dashbutton") (v "0.1.0") (d (list (d (n "pcap") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1yvj1w9s1ni1n5hfsjqng3h19qy27bqn7k5dqpj2iy3xlck0dg4h")))

