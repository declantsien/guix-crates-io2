(define-module (crates-io da sh dashcache) #:use-module (crates-io))

(define-public crate-dashcache-0.1.0 (c (n "dashcache") (v "0.1.0") (d (list (d (n "ahash") (r "^0.3") (d #t) (k 0)) (d (n "dashmap") (r "^3") (d #t) (k 0)) (d (n "elysees") (r "^0.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)))) (h "0rp1g46qjv77yhbaglhzczwjdchvl5k1rlx1yy5f7vfislvzcqxs")))

(define-public crate-dashcache-0.1.1 (c (n "dashcache") (v "0.1.1") (d (list (d (n "ahash") (r "^0.3") (d #t) (k 0)) (d (n "dashmap") (r "^3") (d #t) (k 0)) (d (n "elysees") (r "^0.1.1") (d #t) (k 0)) (d (n "erasable") (r "^1.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "slice-dst") (r "^1.5.1") (d #t) (k 0)))) (h "0mk1a01z22bdmxhf9vzkaqmdib3nf0hvm92xgfxs788dzpv74pfj")))

(define-public crate-dashcache-0.1.2 (c (n "dashcache") (v "0.1.2") (d (list (d (n "ahash") (r "^0.3") (d #t) (k 0)) (d (n "dashmap") (r "^3") (d #t) (k 0)) (d (n "elysees") (r "^0.1.2") (d #t) (k 0)) (d (n "erasable") (r "^1.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "slice-dst") (r "^1.5.1") (d #t) (k 0)))) (h "166492zds3v7d219r24jf1jfcz1wf9a45ccx03ycyws0xc5jnf9a")))

(define-public crate-dashcache-0.2.0 (c (n "dashcache") (v "0.2.0") (d (list (d (n "ahash") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "dashmap") (r "^3") (d #t) (k 0)) (d (n "elysees") (r "^0.2") (d #t) (k 0)) (d (n "erasable") (r "^1.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slice-dst") (r "^1.5.1") (d #t) (k 0)))) (h "122fmnf8kn0m2z14r6i87bzzf6dmijwchras5sn1bgzg89fdj611")))

(define-public crate-dashcache-0.2.1 (c (n "dashcache") (v "0.2.1") (d (list (d (n "ahash") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "dashmap") (r "^3") (d #t) (k 0)) (d (n "elysees") (r "^0.2") (d #t) (k 0)) (d (n "erasable") (r "^1.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slice-dst") (r "^1.5.1") (d #t) (k 0)))) (h "029fbymjyjasmc2vqiqylxikph9wh5iaj3x4p3pxjq2q59c9adwx")))

(define-public crate-dashcache-0.2.2 (c (n "dashcache") (v "0.2.2") (d (list (d (n "ahash") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "dashmap") (r "^3") (d #t) (k 0)) (d (n "elysees") (r "^0.2") (d #t) (k 0)) (d (n "erasable") (r "^1.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slice-dst") (r "^1.5.1") (d #t) (k 0)))) (h "1j9vnkanx2xn8hmgzdmm50a3j165r3ypx3nwszigwmsmlz5n6vmn")))

