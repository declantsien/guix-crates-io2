(define-module (crates-io da sh dasher) #:use-module (crates-io))

(define-public crate-dasher-0.2.0 (c (n "dasher") (v "0.2.0") (d (list (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "04h212srxsiw29dqjzm6f1yymxpmvnfrmxj532286qmwvxx0vmhm")))

(define-public crate-dasher-0.3.0 (c (n "dasher") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "06nf6zi3nwjhw1xcnyl3klk9ird0k68bcsihb8jrpcgf6izr5b84")))

(define-public crate-dasher-0.3.1 (c (n "dasher") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0sj8lqzaz12k2dbm5k9pvyivibnavfjkzz2wik0g2fmdmy45gfvf")))

(define-public crate-dasher-0.3.2 (c (n "dasher") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0va0qyj8mshmblgac4hr5lmmd9lx4l4ayn7g0laf6izkyjpsq5lf")))

(define-public crate-dasher-0.3.3 (c (n "dasher") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1y22rkf14lx8hsjgmfygd26jcjbv6pydf6v0pq4q1djz97wkdqwk")))

