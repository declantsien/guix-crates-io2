(define-module (crates-io da sh dashu-macros) #:use-module (crates-io))

(define-public crate-dashu-macros-0.2.0 (c (n "dashu-macros") (v "0.2.0") (d (list (d (n "dashu-float") (r "^0.2.0") (d #t) (k 0)) (d (n "dashu-int") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "01cfbxvj5gw2b0mkyl7662pj4ah4djr1ym7yvmlp2v4r4v3cxy84") (r "1.61")))

(define-public crate-dashu-macros-0.3.0 (c (n "dashu-macros") (v "0.3.0") (d (list (d (n "dashu-base") (r "^0.3.0") (k 0)) (d (n "dashu-float") (r "^0.3.0") (k 0)) (d (n "dashu-int") (r "^0.3.0") (k 0)) (d (n "dashu-ratio") (r "^0.3.0") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1r8bng8blb1pb512ms9lsdq0himyl0asdxnsm0ha68gx33wpxf4n") (f (quote (("embedded")))) (r "1.61")))

(define-public crate-dashu-macros-0.3.1 (c (n "dashu-macros") (v "0.3.1") (d (list (d (n "dashu-base") (r "^0.3.0") (k 0)) (d (n "dashu-float") (r "^0.3.0") (k 0)) (d (n "dashu-int") (r "^0.3.0") (k 0)) (d (n "dashu-ratio") (r "^0.3.0") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0570xfvgljzkpzpxd0vj7byba0qq4d708mwfzidpsjkjnqjj9z7n") (f (quote (("embedded")))) (r "1.61")))

(define-public crate-dashu-macros-0.4.0 (c (n "dashu-macros") (v "0.4.0") (d (list (d (n "dashu-base") (r "^0.4.0") (k 0)) (d (n "dashu-float") (r "^0.4.0") (k 0)) (d (n "dashu-int") (r "^0.4.0") (k 0)) (d (n "dashu-ratio") (r "^0.4.0") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0m1zdh5c7zsi9aq970hpnhpgpxd4jqprxsmhsqhnpf0mwqcw75p4") (r "1.61")))

(define-public crate-dashu-macros-0.4.1 (c (n "dashu-macros") (v "0.4.1") (d (list (d (n "dashu-base") (r "^0.4.0") (k 0)) (d (n "dashu-float") (r "^0.4.2") (k 0)) (d (n "dashu-int") (r "^0.4.1") (k 0)) (d (n "dashu-ratio") (r "^0.4.1") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "045by0y6a3g7c064zw5sk6acdppr9sgg177dx7v6crrnyqz1qf4k") (r "1.61")))

