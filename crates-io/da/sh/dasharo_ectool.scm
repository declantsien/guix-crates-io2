(define-module (crates-io da sh dasharo_ectool) #:use-module (crates-io))

(define-public crate-dasharo_ectool-0.3.8 (c (n "dasharo_ectool") (v "0.3.8") (d (list (d (n "clap") (r "^3.2") (o #t) (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "hidapi") (r "^1.4") (f (quote ("linux-shared-hidraw"))) (o #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "redox_hwio") (r "^0.1.6") (o #t) (k 0)))) (h "14yl3qvysff7vd4nz1gbrvnww2pr7r2xvqisvnflw6s8shycrycg") (f (quote (("std" "libc" "downcast-rs/std") ("default" "std" "hidapi" "clap"))))))

