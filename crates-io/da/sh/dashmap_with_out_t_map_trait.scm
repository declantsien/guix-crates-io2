(define-module (crates-io da sh dashmap_with_out_t_map_trait) #:use-module (crates-io))

(define-public crate-dashmap_with_out_t_map_trait-5.0.0 (c (n "dashmap_with_out_t_map_trait") (v "5.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (f (quote ("send_guard"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0igx5n8yf0iyfjmjx95adixvxb9900rqvh2zhzx9h0d7bihrpr1f") (f (quote (("raw-api") ("default"))))))

