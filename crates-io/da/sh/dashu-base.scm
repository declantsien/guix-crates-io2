(define-module (crates-io da sh dashu-base) #:use-module (crates-io))

(define-public crate-dashu-base-0.1.0 (c (n "dashu-base") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0ykjih5c0jynwck3ca23ighshsbm4ds2iyla8r72cwc8gw8ymy8y") (f (quote (("std") ("default" "std"))))))

(define-public crate-dashu-base-0.1.1 (c (n "dashu-base") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0ia218jni797168fr16zlkjgb0i44imlvidnx1clacc76bc393hz") (f (quote (("std") ("default" "std"))))))

(define-public crate-dashu-base-0.2.0 (c (n "dashu-base") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "091blkh3x44bknyzcfdbkjz2nzvzmczz104ykcbvxj1r8y322ilv") (f (quote (("std") ("default" "std")))) (r "1.61")))

(define-public crate-dashu-base-0.2.1 (c (n "dashu-base") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0g5s2mq285mgishvwif8vrqdq4by64jd3xr3vl75iamb7pw4xv0d") (f (quote (("std") ("default" "std")))) (r "1.61")))

(define-public crate-dashu-base-0.3.0 (c (n "dashu-base") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "170gi20dr4p8xj62k8nnc28wwl7fhl49387q8wwwchlz87dprpyj") (f (quote (("std") ("default" "std")))) (r "1.61")))

(define-public crate-dashu-base-0.3.1 (c (n "dashu-base") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1alp04lsl4rlax9f3mfdwfrfbdqlg4ma1ns5823prkwf5d2qa9bg") (f (quote (("std") ("default" "std")))) (r "1.61")))

(define-public crate-dashu-base-0.4.0 (c (n "dashu-base") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0r22l1mqvhlq30wj1kq9ra9k6s3l5icva173xms9d60i6lazr0z1") (f (quote (("std") ("default" "std")))) (r "1.61")))

(define-public crate-dashu-base-0.4.1 (c (n "dashu-base") (v "0.4.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "12gjivqax003blwg9g9zrr4k1509042dnbgazxc8r9jsp3v0pf60") (f (quote (("std") ("default" "std")))) (r "1.61")))

