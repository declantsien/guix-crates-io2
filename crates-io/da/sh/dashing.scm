(define-module (crates-io da sh dashing) #:use-module (crates-io))

(define-public crate-dashing-0.0.1 (c (n "dashing") (v "0.0.1") (h "008hlikq194bfx0jxdvirknhsvj4p054pwa1mlzfdvjcr415misx")))

(define-public crate-dashing-0.0.2 (c (n "dashing") (v "0.0.2") (d (list (d (n "bytemuck") (r "^1.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hamcrest") (r "^0.1.5") (d #t) (k 2)) (d (n "image") (r "^0.20") (d #t) (k 2)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "time") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.8") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 0)))) (h "1l9k3g3611x9vkz2jsrkjnzwdnrwwbq8hvqgs2xnvps1558ccx0a")))

