(define-module (crates-io da sh dashmap-cache) #:use-module (crates-io))

(define-public crate-dashmap-cache-0.1.0 (c (n "dashmap-cache") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lsqj9917fz1al5q2wxqff68jcw998sd1yw1ds0ccyjrmmyygi4p")))

(define-public crate-dashmap-cache-0.1.1 (c (n "dashmap-cache") (v "0.1.1") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lhxslikm69nyb1mga6pmr9sccn0wrpbxizl4hs93avkl0jb0y0j")))

(define-public crate-dashmap-cache-0.1.2 (c (n "dashmap-cache") (v "0.1.2") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xj8grjqgn9rskg4b29732qfymaqwnpp200wi7xmvvx355mnc44n")))

(define-public crate-dashmap-cache-0.1.3 (c (n "dashmap-cache") (v "0.1.3") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w9cnwspc23zxjpfb0mrya0f62zw0b2qzmsj478kbri477ymhj0k")))

(define-public crate-dashmap-cache-0.1.4 (c (n "dashmap-cache") (v "0.1.4") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gc8wrqj04ij5y45mzy1ilp8y1ha5cafnrkb093f0w58zq1m2p0z")))

(define-public crate-dashmap-cache-0.1.5 (c (n "dashmap-cache") (v "0.1.5") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dm8ib6qsk3x8h121gpdfdzvp90jd81cv5rvi4pxnxwpddi096sp") (f (quote (("tokio") ("default"))))))

(define-public crate-dashmap-cache-0.1.6 (c (n "dashmap-cache") (v "0.1.6") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "04facnr5lrzwawxdqn2q0v66jnj6kkw2frfyycrm9fxjnsj64wfa") (f (quote (("default")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-dashmap-cache-0.1.7 (c (n "dashmap-cache") (v "0.1.7") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "1029m6q1bhvfx3ivxpsbfwsg48dc9cr1syf5nc5k2jdp7gf1yzcb") (f (quote (("default")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-dashmap-cache-0.1.8 (c (n "dashmap-cache") (v "0.1.8") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "1b1kmb1a389642agykb58fwh89grk9z8jyrl07sx2k3z2vr7qlbn") (f (quote (("default")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

