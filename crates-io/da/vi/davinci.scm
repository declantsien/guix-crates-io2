(define-module (crates-io da vi davinci) #:use-module (crates-io))

(define-public crate-davinci-0.1.0 (c (n "davinci") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xjjifl3jdzr2755nsm3h2nqdfplick02pxs4h65406pd9g94l4c")))

