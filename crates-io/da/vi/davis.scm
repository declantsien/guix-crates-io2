(define-module (crates-io da vi davis) #:use-module (crates-io))

(define-public crate-davis-0.1.0 (c (n "davis") (v "0.1.0") (d (list (d (n "configparser") (r "^3") (d #t) (k 0)) (d (n "lexopt") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mpdrs") (r "^0.1") (d #t) (k 0)))) (h "0zx4gh58wq5l7n8jj0g7n2f760ijwfypmycxb6makzikn13lgkax")))

(define-public crate-davis-0.1.1 (c (n "davis") (v "0.1.1") (d (list (d (n "configparser") (r "^3") (d #t) (k 0)) (d (n "lexopt") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mpdrs") (r "^0.1") (d #t) (k 0)))) (h "1rk3318gqak0svhsfbn78xc18rbca1z4r4bigak7kbdj83dy837i")))

