(define-module (crates-io da vi davids-test-crate) #:use-module (crates-io))

(define-public crate-davids-test-crate-0.1.0 (c (n "davids-test-crate") (v "0.1.0") (h "088byi5ihn73psvaa4qdcbfgdn0pfnmjnnnl4j4449fqw7pxnbhk")))

(define-public crate-davids-test-crate-0.1.1 (c (n "davids-test-crate") (v "0.1.1") (h "0jv9cwakccqhrfvrb95fj6whlspqzbqh8zb3dn9wrlciszpa6yn1")))

(define-public crate-davids-test-crate-0.1.2 (c (n "davids-test-crate") (v "0.1.2") (h "0i3ra2qa9v4kpvxhs8kabdgx1jxzip7xfhsri0jdazp0hvl94ych")))

