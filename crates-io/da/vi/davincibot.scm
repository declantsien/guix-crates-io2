(define-module (crates-io da vi davincibot) #:use-module (crates-io))

(define-public crate-davincibot-0.0.1 (c (n "davincibot") (v "0.0.1") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "diesel") (r "^1.0.0") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)) (d (n "text_io") (r "^0.1.7") (d #t) (k 0)))) (h "11x6j0c50fxvph8kbqgwkpw68rj7zc6irra4bjw954pnf1q7ic0y")))

(define-public crate-davincibot-0.0.2 (c (n "davincibot") (v "0.0.2") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "062ckpyszsjgj2yhgaj5iasmm2s8dil3snrj1pjl5f2qj1zbwcr5")))

(define-public crate-davincibot-0.0.3 (c (n "davincibot") (v "0.0.3") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "03aqm1wb028wlgw17kkzn2b5g49avrwvxlzm8wkgwk5zws0l4qj4")))

(define-public crate-davincibot-0.0.4 (c (n "davincibot") (v "0.0.4") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "13lzpgn63c25a8nwwdxfar7h6bva0w42f63fbx1p7ag20ygvdma6")))

(define-public crate-davincibot-0.0.5 (c (n "davincibot") (v "0.0.5") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "1hamrxgkbiwqnlfxffz7330nnbn6splbw7y547bzhgslky7xd8bg")))

(define-public crate-davincibot-0.0.6 (c (n "davincibot") (v "0.0.6") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "1hm40x5bk0mlhlcsdixi9xkv7b4r0ybw6dj254jyq8pfnivpsvz4")))

(define-public crate-davincibot-0.0.7 (c (n "davincibot") (v "0.0.7") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "08xnmsvrvx0qn2an8vrynppxdj71n7cq3v2hh4bmlyxzd5v6jn2x")))

(define-public crate-davincibot-0.0.8 (c (n "davincibot") (v "0.0.8") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1wvv0j1487k58i1isxwqh8w1haw9z73plx78mpyvl163k1m58nfi")))

(define-public crate-davincibot-0.0.9 (c (n "davincibot") (v "0.0.9") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1r4b73hsyxqkc8jxi0ws484znyfrxfndfiiq439cyz4vhxfchf7g")))

(define-public crate-davincibot-0.0.10 (c (n "davincibot") (v "0.0.10") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "114d3s9xbm2y57qbpv2h54p2g8piwhhqrpi1q9s8mbdx4cpknixp")))

(define-public crate-davincibot-0.0.11 (c (n "davincibot") (v "0.0.11") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0laj3pzi02prm47qmxyv96fd8w2q96q36yj1ccr16ams2pc14kxp")))

(define-public crate-davincibot-0.0.12 (c (n "davincibot") (v "0.0.12") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "rustyline") (r "^2.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "01kihni86iskzmb3z8yjfynr8yczkqdcbms728b9q2v7n4m2fxks")))

(define-public crate-davincibot-0.0.13 (c (n "davincibot") (v "0.0.13") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "rustyline") (r "^2.0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "05g4hzdrya5wkpr42f28ailn6ch98ifr5pv0y33zqc54y7hrja9d")))

(define-public crate-davincibot-0.0.14 (c (n "davincibot") (v "0.0.14") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "rustyline") (r "^2.0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1nnk61dbgxiga0j49fdq18vcjimd94i4h0l1xqfqjkpijvqbba85")))

(define-public crate-davincibot-0.0.16 (c (n "davincibot") (v "0.0.16") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "rustyline") (r "^2.0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "11afyrypnlj2rxf0zb8b0sxi7sqxwj7qnl9313gr8ximwrdpp8il")))

(define-public crate-davincibot-0.0.17 (c (n "davincibot") (v "0.0.17") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "rustyline") (r "^2.0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "17xlhb5byr26fxvjjpyy60bvjnci3rpidh93w715y11alh3iai1n")))

(define-public crate-davincibot-0.0.18 (c (n "davincibot") (v "0.0.18") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "rustyline") (r "^2.0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1v9gmwdxcviwzs7hy7fvyfq468s73fnp88b5nl5zl5ysxny36d8w")))

(define-public crate-davincibot-0.0.19 (c (n "davincibot") (v "0.0.19") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "edit-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.14") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "rustyline") (r "^2.0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0z2x2zrsjyl66k082b47cmdvaskrdgq05ljn6966hf5mg6ils134")))

