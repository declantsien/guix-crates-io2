(define-module (crates-io da vi david-set) #:use-module (crates-io))

(define-public crate-david-set-0.1.0 (c (n "david-set") (v "0.1.0") (h "0maqb89ly4l7gvb40fxwz88m6fkwbajn3l50byvqa8k7sfl15wl9") (y #t)))

(define-public crate-david-set-0.1.1 (c (n "david-set") (v "0.1.1") (h "0lghhbx0p3rq8xim6785bl75lrmznqaw916s2hdgxxjcy1f5s20f") (y #t)))

(define-public crate-david-set-0.1.2 (c (n "david-set") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0j5a5hvjx1c7s1llzxkjgcwinclq10gmdvv4991s59ml0lvvnr26") (y #t)))

