(define-module (crates-io da vi davif) #:use-module (crates-io))

(define-public crate-davif-1.0.0 (c (n "davif") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "libavif-sys") (r "^0.4.2") (f (quote ("codec-dav1d"))) (k 0)) (d (n "png") (r "^0.16.3") (d #t) (k 0)) (d (n "resize") (r "^0.4.0") (d #t) (k 0)))) (h "1h7pbp4cill2hvs0623rbhdgnlfw99cwiirl8mzhl5dp4xxc6lf8")))

