(define-module (crates-io da ku daku) #:use-module (crates-io))

(define-public crate-daku-0.1.0 (c (n "daku") (v "0.1.0") (h "1zq7pw2q5j5p65lvn5hdf1f2a61qjhgpjfw2vksigkl24hzaflli")))

(define-public crate-daku-0.2.0 (c (n "daku") (v "0.2.0") (h "1pz8xm9mz7gfkb3p961pp9yg54sq80715yjj6bnxw78r2gwiq608")))

(define-public crate-daku-0.3.0 (c (n "daku") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "05adhjm1ac1s5jhr6w6jvmn16b1zpq0b782lfirzbdk9wf8z286l") (f (quote (("prompt")))) (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-daku-0.3.1 (c (n "daku") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "0vjb1aq0jrxy24bpgnrxpj2sym845wcggyq5y0w03r9gr8psps20") (f (quote (("prompt")))) (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-daku-0.3.2 (c (n "daku") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "1ywdpw9ks2s8q8xzi0l0aw601x15g5ycbr03737cprad4xcqh6v3") (f (quote (("prompt")))) (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-daku-0.3.3 (c (n "daku") (v "0.3.3") (d (list (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "02ym9nw7d8bvdrygsk6rddgbm36cng2q8gln5bww1pdwncx8mirh") (f (quote (("prompt")))) (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-daku-0.4.0 (c (n "daku") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "1965hk0cpz6lyajjbrapzyxk187i63wyxy2zhf6qb6968sggw6g5") (f (quote (("prompt")))) (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-daku-0.5.0 (c (n "daku") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "0bnxwnwwqnks0dyyzv6fmhzgwfqj5aclziaq9ck0vrfwwgy3f7f3") (f (quote (("prompt")))) (s 2) (e (quote (("log" "dep:log"))))))

