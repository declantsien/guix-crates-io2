(define-module (crates-io da la dala) #:use-module (crates-io))

(define-public crate-dala-0.1.0 (c (n "dala") (v "0.1.0") (d (list (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.0") (d #t) (k 0)))) (h "1qr22vc2yqz2havxhc5ylhapmgfxdv5ny5vxizqgg59bclvq6msw")))

(define-public crate-dala-0.1.1 (c (n "dala") (v "0.1.1") (d (list (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.0") (d #t) (k 0)))) (h "06aw3izxalzwypaqndjjzrq8xxhl4minrrwlcf3fwv388f6lmqbf")))

(define-public crate-dala-0.2.0 (c (n "dala") (v "0.2.0") (d (list (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.0") (d #t) (k 0)))) (h "1yqk484aq6qzv4p065r1093pj9qfxjwggj9rhak1h3ipzvcr1xkf")))

(define-public crate-dala-0.3.0 (c (n "dala") (v "0.3.0") (d (list (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.0") (d #t) (k 0)))) (h "0xbi4nxlcd20qdcgx21j5xvqhxsh6srblfzhsf1nwfv8wqfy2qfx")))

