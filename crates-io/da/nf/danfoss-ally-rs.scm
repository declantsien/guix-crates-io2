(define-module (crates-io da nf danfoss-ally-rs) #:use-module (crates-io))

(define-public crate-danfoss-ally-rs-0.0.1 (c (n "danfoss-ally-rs") (v "0.0.1") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02blhkzrp6kv5w66qjkw3jfk30j6ji4fsl899mpcq8ldiqj0n8ik")))

(define-public crate-danfoss-ally-rs-0.0.2 (c (n "danfoss-ally-rs") (v "0.0.2") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ai715g763fi9na6fj5hr4mp8ifraq9swwzgp103x4s5zdpvay1m")))

(define-public crate-danfoss-ally-rs-0.0.3 (c (n "danfoss-ally-rs") (v "0.0.3") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wwzjzy9n04fzl0ns0k12zlbaqcy3sxgpicqbcxrmwqkk7r01qvb")))

