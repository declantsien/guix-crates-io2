(define-module (crates-io da ow daowy-common) #:use-module (crates-io))

(define-public crate-daowy-common-0.0.1 (c (n "daowy-common") (v "0.0.1") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "ic-kit") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "0rn4nmwz6p0zdg1nhww6lp5lvhzz3nc9qqiinfkg64w72295lam3")))

(define-public crate-daowy-common-0.0.2 (c (n "daowy-common") (v "0.0.2") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "ic-kit") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "1909akvxfifbvja5y8k17fx40laxd1bcn8mpdzci7dfmnlmablb3")))

