(define-module (crates-io da ct dactyl) #:use-module (crates-io))

(define-public crate-dactyl-0.1.1 (c (n "dactyl") (v "0.1.1") (d (list (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-integer") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)))) (h "0kz4406jr4pbpcjn6m11vzf79fjnflc87qhla3wq48768qrwd78k")))

(define-public crate-dactyl-0.1.2 (c (n "dactyl") (v "0.1.2") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-integer") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)))) (h "0slvijc4mcwwwh2cvlhm099a2kgc9qn1k2mafk8m1sgbdy4i6jhm")))

(define-public crate-dactyl-0.1.3 (c (n "dactyl") (v "0.1.3") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-integer") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)))) (h "0cijx3vjjdllb3rcbpvmrn77v1d39n4zc0zi3qjqlz6arq6y15jc")))

(define-public crate-dactyl-0.1.4 (c (n "dactyl") (v "0.1.4") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-integer") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)))) (h "106l37w7m2cgqrmfjlr7pyck19f44l17v5dsqwc9cc0fm317pl0g")))

(define-public crate-dactyl-0.1.5 (c (n "dactyl") (v "0.1.5") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-integer") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)))) (h "089422iz0zf3nm7k53h9vk2mfjww8daibbkihyw7ml3lmsasdhl7")))

(define-public crate-dactyl-0.1.6 (c (n "dactyl") (v "0.1.6") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-integer") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "1pmq1phc1zyj1c3pzaj3gwpcg7xrjci2946s6d1rsfv1nnbkd1w6")))

(define-public crate-dactyl-0.1.7 (c (n "dactyl") (v "0.1.7") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-integer") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "1dyhxlr6fzr3m3sp4685i4gpyzhnfmg9a9imcdvs6inc7w71cjnz")))

(define-public crate-dactyl-0.1.8 (c (n "dactyl") (v "0.1.8") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "04z2dcs2qhc4srvc7nljhzp8v4klwbzd2j6acm77x93nw69rs9ip")))

(define-public crate-dactyl-0.2.0 (c (n "dactyl") (v "0.2.0") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0k342fd9yq6xmwcypzs3kw2cl3b0yydy8wnaya29mhz8bdxmydkn") (r "1.56")))

(define-public crate-dactyl-0.2.1 (c (n "dactyl") (v "0.2.1") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "15r9s8dk9ndd80zg5q2zz9dgvj40675d6krfyqmlb9ln3aqscq7c") (r "1.56")))

(define-public crate-dactyl-0.2.2 (c (n "dactyl") (v "0.2.2") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0yifyvxfgwhvql519xd3b6ir4ii8lpbki8pap41pwdziv3yslw9q") (r "1.56")))

(define-public crate-dactyl-0.2.3 (c (n "dactyl") (v "0.2.3") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "03shb60i5bq2xh1hb5jwcd89sx3bvd9nw5ad21yjnjfmrd83vw88") (r "1.56")))

(define-public crate-dactyl-0.2.4 (c (n "dactyl") (v "0.2.4") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0p0jh3iqhskl8i0ab29micnxizxhcq8y9w6zqbpx2mz1gpq3sh8r") (r "1.56")))

(define-public crate-dactyl-0.3.0 (c (n "dactyl") (v "0.3.0") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)) (d (n "fastrand") (r "1.7.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0f667swgnbi29aplb21m2dbzlnmz2d66f2irgqmnalrf0cy4dw6f") (r "1.56")))

(define-public crate-dactyl-0.3.1 (c (n "dactyl") (v "0.3.1") (d (list (d (n "brunch") (r "0.2.*, >=0.2.4") (d #t) (k 2)) (d (n "fastrand") (r "1.7.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0nrbp9jf9rpamd1x0n2q77z357n2sknbgv25j5w6sh5g9lk35hda") (r "1.56")))

(define-public crate-dactyl-0.3.2 (c (n "dactyl") (v "0.3.2") (d (list (d (n "brunch") (r "0.2.*, >=0.2.4") (d #t) (k 2)) (d (n "fastrand") (r "1.7.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0yp8vy4rigkfdcvvh08m94y08snzzbg90l6ywifsxm7nbg8qac78") (r "1.56")))

(define-public crate-dactyl-0.3.3 (c (n "dactyl") (v "0.3.3") (d (list (d (n "brunch") (r "0.2.*, >=0.2.4") (d #t) (k 2)) (d (n "fastrand") (r "1.7.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0mdwc6q35h2hsgc2mwws982dsy58n5x9b6a9mpv7nzn6vm3mfxn5") (r "1.56")))

(define-public crate-dactyl-0.3.4 (c (n "dactyl") (v "0.3.4") (d (list (d (n "brunch") (r "0.2.*, >=0.2.5") (d #t) (k 2)) (d (n "fastrand") (r "1.7.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (f (quote ("std" "i128"))) (k 0)))) (h "1jqwmgjixx74jzjc4wg5g61yb36hngwss62l3azqgkyimmn4m1fs") (r "1.56")))

(define-public crate-dactyl-0.4.0 (c (n "dactyl") (v "0.4.0") (d (list (d (n "brunch") (r "0.2.*, >=0.2.5") (d #t) (k 2)) (d (n "fastrand") (r "1.7.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0qdjsp3bvzmsmdj88z9b0vhnhx8qqfp06ql3c0s54vdy882bn89q") (r "1.61")))

(define-public crate-dactyl-0.4.1 (c (n "dactyl") (v "0.4.1") (d (list (d (n "brunch") (r "0.2.*, >=0.2.5") (d #t) (k 2)) (d (n "fastrand") (r "1.8.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "08g4hq052sfl006ib06cf8w1d42daplwhw6kjwcvzjy44lhxkdzw") (r "1.63")))

(define-public crate-dactyl-0.4.2 (c (n "dactyl") (v "0.4.2") (d (list (d (n "brunch") (r "0.2.*, >=0.2.5") (d #t) (k 2)) (d (n "fastrand") (r "1.8.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "09adjfxigfw6m5dfm9jad5ffdrl58nwqbdh76i6l1wy36r9ikr9r") (r "1.63")))

(define-public crate-dactyl-0.4.3 (c (n "dactyl") (v "0.4.3") (d (list (d (n "brunch") (r "0.3.*") (d #t) (k 2)) (d (n "fastrand") (r "1.8.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "1hs8m4q9qp8gxkpmfq98sspdicizf5nc0xx55a3i830zmjf58xfr") (r "1.63")))

(define-public crate-dactyl-0.4.4 (c (n "dactyl") (v "0.4.4") (d (list (d (n "brunch") (r "0.3.*") (d #t) (k 2)) (d (n "fastrand") (r "1.8.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0x67naphwyrd8ri5wdqh5sh3q4gga30b2y58vbqy6c4pih6kqda8") (r "1.63")))

(define-public crate-dactyl-0.4.5 (c (n "dactyl") (v "0.4.5") (d (list (d (n "brunch") (r "0.3.*") (d #t) (k 2)) (d (n "fastrand") (r "1.8.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "049xr8l7idqkm66xbfb6lbqnr5d6anfv8khg7jqarxy7hnhyx66k") (r "1.63")))

(define-public crate-dactyl-0.4.6 (c (n "dactyl") (v "0.4.6") (d (list (d (n "brunch") (r "0.3.*") (d #t) (k 2)) (d (n "fastrand") (r "1.8.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "15d4h6g1xgk7cczr5b32mxlqh6f71fw0mxvmq7yjxdk2myhas9wp") (r "1.63")))

(define-public crate-dactyl-0.4.7 (c (n "dactyl") (v "0.4.7") (d (list (d (n "brunch") (r "0.4.*") (d #t) (k 2)) (d (n "fastrand") (r "1.8.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0widkdy31b9ibkdp1qxcbvpsm1hkvq322db36dvn514sij0pibg6") (r "1.63")))

(define-public crate-dactyl-0.4.8 (c (n "dactyl") (v "0.4.8") (d (list (d (n "brunch") (r "0.4.*") (d #t) (k 2)) (d (n "fastrand") (r "1.9.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0l3lzf4vyhsgg9h02ag9pqsx1wkdwg3b7imsb42zngf24s2bz1i8") (r "1.63")))

(define-public crate-dactyl-0.5.0 (c (n "dactyl") (v "0.5.0") (d (list (d (n "brunch") (r "0.4.*") (d #t) (k 2)) (d (n "fastrand") (r "1.9.*") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "17w9s8dsjj4wa3scax3zjv92fn22cjcz221lnk7nv57yfjq3vrbf") (r "1.70")))

(define-public crate-dactyl-0.5.1 (c (n "dactyl") (v "0.5.1") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0zrxprjk4wfbi690w1f7ddckk83zham3fbpxqckd89k83hkn5xvj") (r "1.70")))

(define-public crate-dactyl-0.5.2 (c (n "dactyl") (v "0.5.2") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "1z0fw186ygpi0624x52anwbvj0bw2a7pds1vkk54xkhw3sv6iina") (r "1.70")))

(define-public crate-dactyl-0.6.0 (c (n "dactyl") (v "0.6.0") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)))) (h "1smfwd7nslps8ancvf7s3ng28vrzg1wmbzfgbg80lm1w1bm1mpkg") (r "1.70")))

(define-public crate-dactyl-0.7.0 (c (n "dactyl") (v "0.7.0") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)))) (h "0893sv99kx7afgyd0mwni9szn1cb075yylh3fiimwwd2za35s1cl") (r "1.70")))

(define-public crate-dactyl-0.7.1 (c (n "dactyl") (v "0.7.1") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "num-format") (r "0.4.*") (d #t) (k 2)))) (h "1s44ay9yfx3ramr1c8m7f33dcr8lny06sxwkxbmm7fz5x3hqxwjw") (r "1.70")))

