(define-module (crates-io da le dalek-rangeproofs) #:use-module (crates-io))

(define-public crate-dalek-rangeproofs-0.1.0 (c (n "dalek-rangeproofs") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^0.8") (f (quote ("yolocrypto" "serde" "nightly"))) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.6") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.4") (d #t) (k 0)))) (h "1jcydz635bjifn74qy2qrs4izwz3ncd81vmkmjfd7g4zfhckdl7z") (f (quote (("bench")))) (y #t)))

