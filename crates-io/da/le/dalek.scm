(define-module (crates-io da le dalek) #:use-module (crates-io))

(define-public crate-dalek-0.0.0 (c (n "dalek") (v "0.0.0") (d (list (d (n "curve25519-dalek") (r "^0.9") (d #t) (k 0)) (d (n "dalek-credentials") (r "^0.0") (d #t) (k 0)) (d (n "dalek-rangeproofs") (r "^0.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^0.3") (d #t) (k 0)))) (h "13499y9s1y8ihfyf70p29ndp04k6qkx3krjhpp8jq4kic2hn3c9c")))

