(define-module (crates-io da le daleks) #:use-module (crates-io))

(define-public crate-daleks-0.5.0 (c (n "daleks") (v "0.5.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "1xw4syywhb2jv15wcnajmi2jd6dfaz1j2xcl09i05ijrflsnb1j9")))

(define-public crate-daleks-0.5.1 (c (n "daleks") (v "0.5.1") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "0wngfpdf62vhqm6jlcxcwzc2zwbz79zx9fb3jfnaismpn4v5rjgh")))

(define-public crate-daleks-0.6.0 (c (n "daleks") (v "0.6.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "xdg") (r "^2.2") (d #t) (k 0)))) (h "0nkyvnzjikbf1pbm2z7cdfjxn895yk91pxrgg0169pg37622z9fl")))

