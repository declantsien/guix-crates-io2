(define-module (crates-io da le dale) #:use-module (crates-io))

(define-public crate-dale-0.1.0 (c (n "dale") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "0kc12h19v0wikw8b3jbvk73jmy5qhzxh4xxvvzqxp6gn48ff62xg")))

(define-public crate-dale-0.1.1 (c (n "dale") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "1ihbhig6qkicy5pqrs6gjj8k23q7lbxzq0rbw4y6qlyhy5pdxb8z")))

(define-public crate-dale-0.1.2 (c (n "dale") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "14xrx8q8f80ixp740pf18zdm7v7fij1p161mz8l6ipdw5jlyinxs")))

(define-public crate-dale-0.1.3 (c (n "dale") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "1437sw479mgsw8d8idjmc5j0nxns47rzw4g9378vrqjpg206x0rq")))

(define-public crate-dale-0.1.4 (c (n "dale") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "19fxi3grxm06kjpl4n0j73mj7lxqwmizayjkl5wbl19d4ai99dhs")))

(define-public crate-dale-0.1.5 (c (n "dale") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "0pr84fla9p2k4fk40rhfx7shxv0i7b1bk33l8amzmg5g4q80l5qz")))

(define-public crate-dale-0.1.6 (c (n "dale") (v "0.1.6") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "1whlqzzc22ypf0j10ycqbg7ykv6lmxqf8yb6vdkpwiy82b5dfrjp")))

(define-public crate-dale-0.1.7 (c (n "dale") (v "0.1.7") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "15h9nw6893hmlnqbpcngy76bkdfrrzdmlwz7qqzb04g3nc9hf04s")))

(define-public crate-dale-0.1.8 (c (n "dale") (v "0.1.8") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1xr5s8z0vkbslii5h1000l3ky0lpn4a93b473vzhs3l36i3a2mc3")))

(define-public crate-dale-0.1.9 (c (n "dale") (v "0.1.9") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0dv7zs52sdx53ldp716xhr5vg2f46km9cw9fsbjnrvq0p2klh9c8")))

