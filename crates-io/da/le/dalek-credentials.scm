(define-module (crates-io da le dalek-credentials) #:use-module (crates-io))

(define-public crate-dalek-credentials-0.0.0 (c (n "dalek-credentials") (v "0.0.0") (d (list (d (n "curve25519-dalek") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)))) (h "064cg2w6ib6kfnnbk6cilmm7n0wz17771qi2lh028m1kgkyyznmv") (f (quote (("std" "rand") ("default" "std"))))))

