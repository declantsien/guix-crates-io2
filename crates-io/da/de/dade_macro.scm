(define-module (crates-io da de dade_macro) #:use-module (crates-io))

(define-public crate-dade_macro-0.2.0 (c (n "dade_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "18jrzkix4l687g2hd3s6hf6ci7g0f014wpymvvsyhbknxf19450l")))

