(define-module (crates-io da de dade_derive) #:use-module (crates-io))

(define-public crate-dade_derive-0.1.0 (c (n "dade_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1xy0kdpwj40c07959dqsf1hmpra41hcsf91ad4cfbsf2cl8hgypg") (y #t)))

(define-public crate-dade_derive-0.1.1 (c (n "dade_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "04kmr4lc17bp27hqvw879x858wy1hkajbgbqdm8sqj0p07c7b7y4") (y #t)))

(define-public crate-dade_derive-0.1.2 (c (n "dade_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "08hb1l081iqpzzbxxn24sm6d2jyjjdcma8my8qpmgy92imyd8h46") (y #t)))

(define-public crate-dade_derive-0.1.3 (c (n "dade_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0dqka2y911nvgp95m4vb5h1f7m7sz4hy0mj32yf6930rrmr7bf8n") (y #t)))

(define-public crate-dade_derive-0.1.4 (c (n "dade_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0mr5469kbzxlxzvm20sllcpa5mr6ilp4ax9178vg3ykg1brfchv1")))

