(define-module (crates-io da de dade) #:use-module (crates-io))

(define-public crate-dade-0.1.0 (c (n "dade") (v "0.1.0") (d (list (d (n "dade_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "18bf133dpqcldqr5ikrss0d0iscy2c0skmyr4qq7jmw03yklhm88") (y #t)))

(define-public crate-dade-0.1.1 (c (n "dade") (v "0.1.1") (d (list (d (n "dade_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "06jr8r4zv4jlwswrb78yjnps4bvj51pbgmzw0j06vcxkq7lvx9a4") (y #t)))

(define-public crate-dade-0.1.2 (c (n "dade") (v "0.1.2") (d (list (d (n "dade_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)))) (h "03xn085zqm2bgc0n10y9qmqmmi3mmkpvznz19pkma3vn5s86q31n") (y #t)))

(define-public crate-dade-0.1.3 (c (n "dade") (v "0.1.3") (d (list (d (n "dade_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)))) (h "12m3z64c7zhqfdbbgjh7zggmp54yxxmgfxhnz3l8f81qajjvdvia") (y #t)))

(define-public crate-dade-0.1.4 (c (n "dade") (v "0.1.4") (d (list (d (n "dade_derive") (r "^0.1.4") (d #t) (k 0)))) (h "19dnpmyffr17hy22w7fn219qy7pwxswijyn4yys8gc6ry0cc7c61")))

(define-public crate-dade-0.2.0 (c (n "dade") (v "0.2.0") (d (list (d (n "dade_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.56") (d #t) (k 2)))) (h "0z4ziks4jwk5a3xnww53lbvp6wwzz6mqfw0p8rgpzcsil0q6czbi")))

