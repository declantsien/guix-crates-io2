(define-module (crates-io da sp dasp_peak) #:use-module (crates-io))

(define-public crate-dasp_peak-0.11.0 (c (n "dasp_peak") (v "0.11.0") (d (list (d (n "dasp_frame") (r "^0.11") (k 0)) (d (n "dasp_sample") (r "^0.11") (k 0)))) (h "1gqmk6vkf2nvgzwb7yvjzjsib6kz7462b49xabcg68cwsxcqby2w") (f (quote (("std" "dasp_frame/std" "dasp_sample/std") ("default" "std"))))))

