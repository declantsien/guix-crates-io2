(define-module (crates-io da sp dasp_envelope) #:use-module (crates-io))

(define-public crate-dasp_envelope-0.11.0 (c (n "dasp_envelope") (v "0.11.0") (d (list (d (n "dasp_frame") (r "^0.11") (k 0)) (d (n "dasp_peak") (r "^0.11") (o #t) (k 0)) (d (n "dasp_ring_buffer") (r "^0.11") (k 0)) (d (n "dasp_rms") (r "^0.11") (o #t) (k 0)) (d (n "dasp_sample") (r "^0.11") (k 0)))) (h "1dk8fqdwwziyli5bnwxfbwk495w3h10x8pp8gyl03w8nf371gilf") (f (quote (("std" "dasp_frame/std" "dasp_peak/std" "dasp_ring_buffer/std" "dasp_rms/std" "dasp_sample/std") ("rms" "dasp_rms") ("peak" "dasp_peak") ("default" "std") ("all-no-std" "peak" "rms") ("all" "std" "all-no-std"))))))

