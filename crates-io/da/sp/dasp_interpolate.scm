(define-module (crates-io da sp dasp_interpolate) #:use-module (crates-io))

(define-public crate-dasp_interpolate-0.11.0 (c (n "dasp_interpolate") (v "0.11.0") (d (list (d (n "dasp_frame") (r "^0.11") (k 0)) (d (n "dasp_ring_buffer") (r "^0.11") (k 0)) (d (n "dasp_sample") (r "^0.11") (k 0)))) (h "11klgi0fw4gadv4hnnw34hfa50wrskm89ix6q1zcmdrvask7bjbz") (f (quote (("std" "dasp_frame/std" "dasp_ring_buffer/std" "dasp_sample/std") ("sinc") ("linear") ("floor") ("default" "std") ("all-no-std" "floor" "linear" "sinc") ("all" "std" "all-no-std"))))))

