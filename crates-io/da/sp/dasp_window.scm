(define-module (crates-io da sp dasp_window) #:use-module (crates-io))

(define-public crate-dasp_window-0.11.0 (c (n "dasp_window") (v "0.11.0") (d (list (d (n "dasp_sample") (r "^0.11") (k 0)))) (h "1x0icbx91znwd3bp8wcs9xim79p8yf5f49nl93y4bfh7l07bkg36") (f (quote (("std" "dasp_sample/std") ("rectangle") ("hanning") ("default" "std") ("all-no-std" "hanning" "rectangle") ("all" "std" "all-no-std"))))))

(define-public crate-dasp_window-0.11.1 (c (n "dasp_window") (v "0.11.1") (d (list (d (n "dasp_sample") (r "^0.11") (k 0)))) (h "0xjh9ivaw33mvv0kqi49mf8ii4dchqf9yb44id7cxli1i2wdgplr") (f (quote (("std" "dasp_sample/std") ("rectangle") ("hanning") ("default" "std") ("all-no-std" "hanning" "rectangle") ("all" "std" "all-no-std"))))))

