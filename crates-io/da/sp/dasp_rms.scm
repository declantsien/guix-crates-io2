(define-module (crates-io da sp dasp_rms) #:use-module (crates-io))

(define-public crate-dasp_rms-0.11.0 (c (n "dasp_rms") (v "0.11.0") (d (list (d (n "dasp_frame") (r "^0.11") (k 0)) (d (n "dasp_ring_buffer") (r "^0.11") (k 0)) (d (n "dasp_sample") (r "^0.11") (k 0)))) (h "1akm8mvh7as9fpny5zr2jyqm1bmyl9z568i8dr418l3y1frxrid6") (f (quote (("std" "dasp_frame/std" "dasp_ring_buffer/std" "dasp_sample/std") ("default" "std"))))))

