(define-module (crates-io da be dabet) #:use-module (crates-io))

(define-public crate-dabet-2.0.0 (c (n "dabet") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "recolored") (r "^1.9.3") (d #t) (k 0)))) (h "1kavvgn3br2n059cnnjjrhc3s3i9aqg29nbrj955r0wdzy1nmmfc")))

(define-public crate-dabet-3.0.0 (c (n "dabet") (v "3.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "recolored") (r "^1.9.3") (d #t) (k 0)))) (h "0iyzqrlpn6jcsv4vr39zsr3z3hi5lx2zbig5pgvpcsyqwx0d9z67")))

(define-public crate-dabet-3.0.1 (c (n "dabet") (v "3.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "recolored") (r "^1.9.3") (d #t) (k 0)))) (h "04j7addbagls3m5d034hmlk5pm07jz7vix2zmmv4k6933g885yls")))

