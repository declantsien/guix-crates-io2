(define-module (crates-io da be daberu) #:use-module (crates-io))

(define-public crate-daberu-0.1.0 (c (n "daberu") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "orfail") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "10gjdsda5rfl06g9a8nzk76ilqn89wd8wk3ijg5pkpgpj2rrcxb3")))

(define-public crate-daberu-0.1.1 (c (n "daberu") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "orfail") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "0aykqd6rgn7zxhqbabjpalckks6aqizr43czf7jkrwrwir51kq3v")))

