(define-module (crates-io da yd daydream) #:use-module (crates-io))

(define-public crate-daydream-0.1.0 (c (n "daydream") (v "0.1.0") (h "0ffh2nzan3la30hbym2bjkfk68x24fwsgg7bhzxzdchpdrfnahxc")))

(define-public crate-daydream-0.1.1 (c (n "daydream") (v "0.1.1") (h "1wj25qliw7qscrvsydk818hqgrdzd9vkknwg7msfq2jvz7nglgxl")))

