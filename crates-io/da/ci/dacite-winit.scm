(define-module (crates-io da ci dacite-winit) #:use-module (crates-io))

(define-public crate-dacite-winit-0.1.0 (c (n "dacite-winit") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "dacite") (r "^0.1.0") (d #t) (k 0)) (d (n "winit") (r "^0.6") (d #t) (k 0)))) (h "07l3v59ms03595yslglzivm0xwv5lwgm1rjk74dnl1yxlnlps4gv")))

(define-public crate-dacite-winit-0.2.0 (c (n "dacite-winit") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "dacite") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.6") (d #t) (k 0)))) (h "159l2nc0929ibq248ss6fbvl7wy6gda0bb2abyg0k7rccc58qws1")))

(define-public crate-dacite-winit-0.3.0 (c (n "dacite-winit") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "dacite") (r "^0.3") (d #t) (k 0)) (d (n "winit") (r "^0.6") (d #t) (k 0)))) (h "118qincz5nsdr46yd9zihf6pl54jr5nq8m0cb3lbhbcwaxmszann")))

(define-public crate-dacite-winit-0.4.0 (c (n "dacite-winit") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "dacite") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.7") (d #t) (k 0)))) (h "038icbvsdbna1dij31jggg89d7n02jaa7k8537fp8mfc8xbfix4q")))

(define-public crate-dacite-winit-0.5.0 (c (n "dacite-winit") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "dacite") (r "^0.5") (d #t) (k 0)) (d (n "winit") (r "^0.7") (d #t) (k 0)))) (h "1ij1axcmh464pax6n35ya76y7dm3zrphl43x5yknsbs2ccdmbs5c")))

(define-public crate-dacite-winit-0.6.0 (c (n "dacite-winit") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "dacite") (r "^0.6") (d #t) (k 0)) (d (n "winit") (r "^0.7") (d #t) (k 0)))) (h "14cxcjndrrh1axwqs4bq81c782hzqqxpkw8671krq42klirm913c")))

(define-public crate-dacite-winit-0.7.0 (c (n "dacite-winit") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "dacite") (r "^0.7") (d #t) (k 0)) (d (n "winit") (r "^0.7") (d #t) (k 0)))) (h "027bnjakqis2s4nxc0h54nh9nai9vznd5chg3qrcjk4i5qqpsz5n")))

