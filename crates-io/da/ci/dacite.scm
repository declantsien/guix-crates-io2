(define-module (crates-io da ci dacite) #:use-module (crates-io))

(define-public crate-dacite-0.1.0 (c (n "dacite") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.17") (f (quote ("no_function_prototypes" "vk_1_0_4" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "09gs3fyd4ba5a36yvvf9havi2d9cpfnz88kwp5j9yd4iw22w62h4")))

(define-public crate-dacite-0.2.0 (c (n "dacite") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.17") (f (quote ("no_function_prototypes" "vk_1_0_5" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "0knzdjqnnccsjd8k112kljjk0b5jqfmf0czf100w3hz17aazyx7d")))

(define-public crate-dacite-0.3.0 (c (n "dacite") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.17") (f (quote ("no_function_prototypes" "vk_1_0_6" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "02qhyzxvwv80vj2jdi7ay49q40bhbsm57v3gmp9z28pzs4bgyg9j")))

(define-public crate-dacite-0.3.1 (c (n "dacite") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.17") (f (quote ("no_function_prototypes" "vk_1_0_7" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "13b5xa8ajniy0wqchsrc8wgkdrg5ddn5kg3lsgb5rpzhq2kvl1za")))

(define-public crate-dacite-0.3.2 (c (n "dacite") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.17") (f (quote ("no_function_prototypes" "vk_1_0_8" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "19nq61rcb1kgwmpah48g93v4a6gis4zxv7zb4wsgcaqr2wmf16hh")))

(define-public crate-dacite-0.3.3 (c (n "dacite") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.17") (f (quote ("no_function_prototypes" "vk_1_0_9" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "0z1l65zdkcfx50iv4l8sjl3jv2y68zhfsp8074n33ifjgssqq87d")))

(define-public crate-dacite-0.3.4 (c (n "dacite") (v "0.3.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.17") (f (quote ("no_function_prototypes" "vk_1_0_10" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "1kciwy6lc794rwlki9ppz96y01189wbs2sfz92xl0dp0vmvibb0b")))

(define-public crate-dacite-0.3.5 (c (n "dacite") (v "0.3.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.17") (f (quote ("no_function_prototypes" "vk_1_0_11" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "1a5hc0lnxa89psrvrvrs69232ic96kn34ix6scx9167sfary8ck4")))

(define-public crate-dacite-0.3.6 (c (n "dacite") (v "0.3.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.17") (f (quote ("no_function_prototypes" "vk_1_0_12" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "0g4v7dbcqwkbr8j3mg7s6z5d6yq6ik552y01bkil4vw5skrjc28s")))

(define-public crate-dacite-0.4.0 (c (n "dacite") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.19") (f (quote ("no_function_prototypes" "vk_1_0_12" "core_1_0_13" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "026mzw69rg6mp32jz45cr6nxc8jjc2zd535d6pkyr3dvydxnz6bs")))

(define-public crate-dacite-0.5.0 (c (n "dacite") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.19") (f (quote ("no_function_prototypes" "vk_1_0_12" "core_1_0_20" "amd_shader_trinary_minmax_1" "amd_shader_explicit_vertex_parameter_1" "amd_gcn_shader_1" "ext_debug_report_3" "nv_dedicated_allocation_1" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "0azfrqk72q997d65d37kndcv7z66ygyy85mhcpq1ch9f86qxh6yl")))

(define-public crate-dacite-0.5.1 (c (n "dacite") (v "0.5.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.19") (f (quote ("no_function_prototypes" "vk_1_0_12" "core_1_0_21" "amd_shader_trinary_minmax_1" "amd_shader_explicit_vertex_parameter_1" "amd_gcn_shader_1" "ext_debug_report_3" "nv_dedicated_allocation_1" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "0xlivr35qi0vrn40gxzddwh5qhzri4sn7zmmxcqdkb4rn0ym8mx0")))

(define-public crate-dacite-0.6.0 (c (n "dacite") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.19") (f (quote ("no_function_prototypes" "vk_1_0_12" "core_1_0_25" "amd_shader_trinary_minmax_1" "amd_shader_explicit_vertex_parameter_1" "amd_gcn_shader_1" "ext_debug_report_3" "nv_dedicated_allocation_1" "img_format_pvrtc_1" "amd_draw_indirect_count_1" "nv_external_memory_capabilities_1" "nv_external_memory_1" "nv_external_memory_win32_1" "nv_win32_keyed_mutex_1" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "19agjha8hzxmlzr846wrzrf62l2jlh1dqnmwv3bkb4xb7dqcyz4k")))

(define-public crate-dacite-0.6.1 (c (n "dacite") (v "0.6.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.19") (f (quote ("no_function_prototypes" "vk_1_0_30" "khr_get_physical_device_properties2_1"))) (k 0)))) (h "0k8yr7rdwgkm2906v5phfz1b3l7cv3grzjkiim4n6bjl9kaqr605")))

(define-public crate-dacite-0.7.0 (c (n "dacite") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "vks") (r "^0.20") (d #t) (k 0)))) (h "0099jdl09192n3i83yckdpslszn0mfwl3nyvjhy8ihsgpkxsb7b7")))

