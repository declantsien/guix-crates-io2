(define-module (crates-io da ta datadiver-proto) #:use-module (crates-io))

(define-public crate-datadiver-proto-0.1.0 (c (n "datadiver-proto") (v "0.1.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)))) (h "13s6rimksvn6j7zgyv95kw4y9ra540iqv74hhv8ag9hrwc02zd0h") (y #t)))

(define-public crate-datadiver-proto-0.1.1 (c (n "datadiver-proto") (v "0.1.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)))) (h "0cgncxy36fv8i575i3yr00f4d412nh4lnzpm834w16ylvy4c0m28") (y #t)))

(define-public crate-datadiver-proto-0.1.2 (c (n "datadiver-proto") (v "0.1.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)))) (h "1mr63d394dni2lbbw1yhw44bcgll0f4qg0sr5njdxagizpy308q9") (y #t)))

(define-public crate-datadiver-proto-0.1.3 (c (n "datadiver-proto") (v "0.1.3") (d (list (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "0qw82h6fc862c802g11icgsrfxww7xhk34qipn5jni1l341qbdgd")))

