(define-module (crates-io da ta datamatrix) #:use-module (crates-io))

(define-public crate-datamatrix-0.1.0-alpha.0 (c (n "datamatrix") (v "0.1.0-alpha.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)))) (h "07fg3w1x4ff11w2aw2yqxjwf34picx1324f9l415li4zcbl1w1wr")))

(define-public crate-datamatrix-0.1.0-alpha.1 (c (n "datamatrix") (v "0.1.0-alpha.1") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)))) (h "1m7r41hwp83gri912sxwz94c41s7giijb64m49vbvxynpk6k46h8")))

(define-public crate-datamatrix-0.1.0-alpha.2 (c (n "datamatrix") (v "0.1.0-alpha.2") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)))) (h "1lgk2maf1ggq4kmnai7pfp93r2zzdw8pz4licm7hjf0xpdb3y5x4")))

(define-public crate-datamatrix-0.1.0-alpha.3 (c (n "datamatrix") (v "0.1.0-alpha.3") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)))) (h "05n8f9a0kjx0zb35pnqasnyzsk2a08v9s4ssgzp354al3hdbb93m")))

(define-public crate-datamatrix-0.1.0-beta.1 (c (n "datamatrix") (v "0.1.0-beta.1") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)))) (h "145jzx1vrxchy5yvybnwda28p1fww1m29jwk7gq0bn6app780pgf")))

(define-public crate-datamatrix-0.1.0-beta.2 (c (n "datamatrix") (v "0.1.0-beta.2") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)))) (h "0jc75ngcs6p472dgdjx1r7v0gnp11gzs7l1ks3fwhbky4plkzx45") (f (quote (("extended_eci" "encoding_rs") ("default"))))))

(define-public crate-datamatrix-0.1.0 (c (n "datamatrix") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)))) (h "0zzjz0zi8vbb1n2mg6f8aacly3175bvgm3xlnyx2r6w6sz1pifgp") (f (quote (("extended_eci" "encoding_rs") ("default")))) (y #t)))

(define-public crate-datamatrix-0.1.1 (c (n "datamatrix") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)))) (h "1v8xcjkxg3yik25rx7xfpgziii2bvmlfr4sq6san9jdnk21bfysm") (f (quote (("extended_eci" "encoding_rs") ("default"))))))

(define-public crate-datamatrix-0.2.0 (c (n "datamatrix") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "printpdf") (r "^0.4") (d #t) (k 2)))) (h "1bkl2q93x2smgvk9rg4aav5pml8ins56i4rbqd8pxa9hn2m3hwh3") (f (quote (("extended_eci" "encoding_rs") ("default"))))))

(define-public crate-datamatrix-0.2.1 (c (n "datamatrix") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "flagset") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "printpdf") (r "^0.4") (d #t) (k 2)))) (h "101nykm8g6lkvlmw829j3w36ijqqijqz40580cnda1lq0457hk7g") (f (quote (("extended_eci" "encoding_rs") ("default"))))))

(define-public crate-datamatrix-0.3.0 (c (n "datamatrix") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "enum-iterator") (r "^0.7") (d #t) (k 2)) (d (n "flagset") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "printpdf") (r "^0.5") (d #t) (k 2)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 2)))) (h "191fpd8m8910g4d5000izd2svb2cxp9php8zpr9kjsfvw3c35glw") (f (quote (("extended_eci" "encoding_rs") ("default"))))))

