(define-module (crates-io da ta data_downloader) #:use-module (crates-io))

(define-public crate-data_downloader-0.1.0 (c (n "data_downloader") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1v59h8pn0cdszc2xr5bm20f6akz1b4sv2gqb3jx2h9lxgw0d28gs")))

(define-public crate-data_downloader-0.2.0 (c (n "data_downloader") (v "0.2.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "zip") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "047brxzyhi4s5skymswz9c5rwbh207z9zp96nlq46rhldpdnn6sy") (s 2) (e (quote (("zip" "dep:zip")))) (r "1.70.0")))

