(define-module (crates-io da ta datamuse-api-wrapper) #:use-module (crates-io))

(define-public crate-datamuse-api-wrapper-0.1.0 (c (n "datamuse-api-wrapper") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0nb6s3r5q8dx11pjvdiksfhp3iwmmianf8swvfkk4i5b28kmp5x1")))

