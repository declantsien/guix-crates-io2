(define-module (crates-io da ta dataz) #:use-module (crates-io))

(define-public crate-dataz-0.1.0 (c (n "dataz") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1c31acllwi5717imzk23q6n7r9pchyixr9wfffhpz2vvmmmzf3cq") (f (quote (("default" "serde"))))))

(define-public crate-dataz-0.2.0 (c (n "dataz") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "174lsixilc9gnzjh12jnyjd6vl0daxh9x6v0s8cir1kqjiqg6hrx") (f (quote (("default" "serde"))))))

(define-public crate-dataz-0.3.0 (c (n "dataz") (v "0.3.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1nbmj57934w3i2ycid48pqya9imyr475qx52mb9wi5pvjny4iafs")))

