(define-module (crates-io da ta databuf_derive_impl) #:use-module (crates-io))

(define-public crate-databuf_derive_impl-0.1.0 (c (n "databuf_derive_impl") (v "0.1.0") (d (list (d (n "quote2") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ngzav935q19x5574bvjscazhj1dsj4rclckwqqb7yg9ri9fvqix")))

(define-public crate-databuf_derive_impl-0.1.1 (c (n "databuf_derive_impl") (v "0.1.1") (d (list (d (n "quote2") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "10xxfdr58gnsp86lw4n2gvbl54vsc15nwhxf4fa7jj30wsg87s0q")))

(define-public crate-databuf_derive_impl-0.2.0 (c (n "databuf_derive_impl") (v "0.2.0") (d (list (d (n "quote2") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0v8z0bbd85iv5i557swqzxyk1pj8rkzhnirqaz5jkvnnxvr648sy")))

(define-public crate-databuf_derive_impl-0.2.1 (c (n "databuf_derive_impl") (v "0.2.1") (d (list (d (n "quote2") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0gncr9j25h38g6jnr1v9m01wh5h6wpwg5liadngwzzskwv1qgms1")))

(define-public crate-databuf_derive_impl-0.2.3 (c (n "databuf_derive_impl") (v "0.2.3") (d (list (d (n "quote2") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "06n8klzvs796gak6dgzxlsxdd2jslf43g4vgf4ipvs0z0zmmdxns")))

