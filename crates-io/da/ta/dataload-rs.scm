(define-module (crates-io da ta dataload-rs) #:use-module (crates-io))

(define-public crate-dataload-rs-0.1.0 (c (n "dataload-rs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.4") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.4") (f (quote ("sync" "rt" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2.5") (d #t) (k 0)))) (h "0wpsq85ldh0hfg0cgqgmdvd2sxv1243djfxw4fsnl2dqbg3gl1bm")))

