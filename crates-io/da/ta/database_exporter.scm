(define-module (crates-io da ta database_exporter) #:use-module (crates-io))

(define-public crate-database_exporter-0.1.0 (c (n "database_exporter") (v "0.1.0") (d (list (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "fake") (r "^1.2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16sc3dml44382ya514vnppkidk1p613fnk1kh15f55ph0idq5alz")))

(define-public crate-database_exporter-0.1.1 (c (n "database_exporter") (v "0.1.1") (d (list (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "fake") (r "^1.2.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "057lwxvi1ff6nkcxb8qsh4kjc89fpxbgl81zmsilafcf6hd04jv3")))

(define-public crate-database_exporter-0.1.2 (c (n "database_exporter") (v "0.1.2") (d (list (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "fake") (r "^1.2.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lfdmjbpc09slm11ifzq5s1gv4af31hl999n8fcqqxvqh55xi34b")))

(define-public crate-database_exporter-0.1.3 (c (n "database_exporter") (v "0.1.3") (d (list (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "fake") (r "^1.2.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hy2dpd9jig06kc0kb5l220ni9v327mb2kidfsprjaa9aqk5fz3y")))

(define-public crate-database_exporter-0.1.4 (c (n "database_exporter") (v "0.1.4") (d (list (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "fake") (r "^1.2.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "023nwj1isdgqih588gwrh5ajmmxq01z821h9b0d8agmll4y7k1zh")))

