(define-module (crates-io da ta datafusion-bigtable) #:use-module (crates-io))

(define-public crate-datafusion-bigtable-0.1.0 (c (n "datafusion-bigtable") (v "0.1.0") (d (list (d (n "arrow") (r "^9.0.0") (f (quote ("prettyprint"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "bigtable_rs") (r "^0.1.5") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "datafusion") (r "^7.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync" "fs" "parking_lot"))) (d #t) (k 0)))) (h "02kgwks385i5pf0wdr7q6wf2k27l1ryl8fji0m41dflbf89i1nyh")))

