(define-module (crates-io da ta data-buffer) #:use-module (crates-io))

(define-public crate-data-buffer-0.1.0 (c (n "data-buffer") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.1") (d #t) (k 0)))) (h "0rnzpsnsxynz7rh5bf53f87fvb02c6wmra4ypx7nwnlhc0qc0vdz") (f (quote (("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.1.1 (c (n "data-buffer") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.1") (d #t) (k 0)))) (h "1byzypxdwvb8wvi601is62bswv6rq6mvknnwvxbbisz2a47cjvgs") (f (quote (("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.2.0 (c (n "data-buffer") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.1") (d #t) (k 0)))) (h "0b1yqag27yxqcywd8z9cylmhgdm1az6pbzdi9kiwrcixvj1q2i5f") (f (quote (("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.3.0 (c (n "data-buffer") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.1") (d #t) (k 0)))) (h "0fs2wfpsimdhgwal30mj9d33pr9lanwfrcxjlmkgqwa285241nki") (f (quote (("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.3.1 (c (n "data-buffer") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.1") (d #t) (k 0)))) (h "0iwy4asf1kdqi0g19xnrqnqdfkd96xyqp29x99hdax83jjgsxy1q") (f (quote (("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.4.0 (c (n "data-buffer") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.1") (d #t) (k 0)))) (h "10hlj4pb1m8nmirykyajyjp7v8n2xda9njxy1prmsm84l999msg8") (f (quote (("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.4.1 (c (n "data-buffer") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.1") (d #t) (k 0)))) (h "0v6vxcp9dw5ipmxrfp5l2pnipx29g5wz64z74xa77bhhyl4k2mmv") (f (quote (("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.4.2 (c (n "data-buffer") (v "0.4.2") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.1") (d #t) (k 0)))) (h "1v34zkhzqn35ahjv3y0zxdrik3n9v2m8snrhgdyn3qsdlw7lkrzl") (f (quote (("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.4.3 (c (n "data-buffer") (v "0.4.3") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.1") (d #t) (k 0)))) (h "08gf024ny8nsi55b4s96p8nicqmg1spxss4911w3pi7kxc9vq23f") (f (quote (("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.5.0 (c (n "data-buffer") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.1") (d #t) (k 0)))) (h "0dlzi84gyx45n4yx7l2h1a65jgc28jpvikaw63i1zgwjda7z2c0s") (f (quote (("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.5.1 (c (n "data-buffer") (v "0.5.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "163xicjhb53ipxrsll6s6wwj0xjflijkkrfkvhfzlbqvwdir0wb1") (f (quote (("serde_all" "serde" "serde_bytes") ("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.6.0 (c (n "data-buffer") (v "0.6.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bj29n7110vpv5nvmlahlrhzpnhcwz617yn7f4b0ql8zd235yrmr") (f (quote (("serde_all" "serde" "serde_bytes") ("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.6.1 (c (n "data-buffer") (v "0.6.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "016rs5n4n8rjnwykjcni9xbv7rvx81393nqc798bcgcnmw1drgvp") (f (quote (("serde_all" "serde" "serde_bytes") ("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.7.0 (c (n "data-buffer") (v "0.7.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08bz3y5qdhyvsy003n2hvyjs3zj5gkxigg4g7rvgmgn5dz9g8mf9") (f (quote (("serde_all" "serde" "serde_bytes") ("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.7.1 (c (n "data-buffer") (v "0.7.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1p8q0jdvfkvq4lia4kbk70fbjz7ab7yljcr3igild40knhnhl1cs") (f (quote (("serde_all" "serde" "serde_bytes") ("numeric" "num-traits"))))))

(define-public crate-data-buffer-0.8.0 (c (n "data-buffer") (v "0.8.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "reinterpret") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qw05yh6bx0d33r6bcvij300a7i0697qv223crdndi06wz1kdx9b") (f (quote (("serde_all" "serde" "serde_bytes") ("numeric" "num-traits"))))))

