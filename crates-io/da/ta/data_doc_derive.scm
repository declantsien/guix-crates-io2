(define-module (crates-io da ta data_doc_derive) #:use-module (crates-io))

(define-public crate-data_doc_derive-0.1.0 (c (n "data_doc_derive") (v "0.1.0") (d (list (d (n "data_doc") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nngkzl3597bc7dlhb3j6vllgfqld2ndxc8gszm114yqzi73rvfg")))

