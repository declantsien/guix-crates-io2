(define-module (crates-io da ta datadriven) #:use-module (crates-io))

(define-public crate-datadriven-0.1.0 (c (n "datadriven") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)))) (h "0i8fa1pjvpa88rl54bwyxc7fzlk4y56a1vyhm664w6wck721mji3")))

(define-public crate-datadriven-0.2.0 (c (n "datadriven") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1f6cplnlc6yx3pmx4r97g09f2zhy4krp6c25vvrk36fk4x5qvn0h")))

(define-public crate-datadriven-0.3.0 (c (n "datadriven") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1mx6ah0hm7b1ncnr7wgxi5zwxm98fj1ids5myysr7kcqfwi2i7ar")))

(define-public crate-datadriven-0.4.0 (c (n "datadriven") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0npzlnvackvvr9zibasbj00bbhrbi10i2b2qgia8wbgs7gjkfwwx")))

(define-public crate-datadriven-0.5.0 (c (n "datadriven") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1a36psy2gwnv48crs678g4rf14r6x7alrw7f0lj66c6xjz1w0iip")))

(define-public crate-datadriven-0.6.0 (c (n "datadriven") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.7") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "01j1gg47hpq3i66my3dka2j3p779jf2n3h52sqdh8q5nfwr6wjaw") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-datadriven-0.7.0 (c (n "datadriven") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.7") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "15vgmlcnlz7p1fjgls3avjlvn1qvciaawbc7j8nmccqgf3kz066z") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-datadriven-0.8.0 (c (n "datadriven") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.7") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "03sjvwm658hlppinrbbs8alydx0ja9bgpqccgyxqd73vrgm2v4p6") (f (quote (("default" "async") ("async" "futures"))))))

