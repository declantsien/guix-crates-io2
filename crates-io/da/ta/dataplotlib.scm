(define-module (crates-io da ta dataplotlib) #:use-module (crates-io))

(define-public crate-dataplotlib-0.1.0 (c (n "dataplotlib") (v "0.1.0") (d (list (d (n "piston_window") (r "^0.52.0") (d #t) (k 0)))) (h "18z9ck18l1bi5yf27j5d8ka1vy4fy53jgiz0brk2b8f6ifgsci46")))

(define-public crate-dataplotlib-0.1.1 (c (n "dataplotlib") (v "0.1.1") (d (list (d (n "piston_window") (r "^0.52.0") (d #t) (k 0)))) (h "06iavim47bljsrdb3sdi624rfdp8figfmhk5bwijfiyyfg2irkmg")))

(define-public crate-dataplotlib-0.1.2 (c (n "dataplotlib") (v "0.1.2") (d (list (d (n "piston_window") (r "^0.52.0") (d #t) (k 0)))) (h "18s8lwshqgrc0bqm2qi6b02cs971ls2aphyj7sxxafnia2xbnw1s")))

(define-public crate-dataplotlib-0.1.3 (c (n "dataplotlib") (v "0.1.3") (d (list (d (n "piston_window") (r "^0.52.0") (d #t) (k 0)))) (h "0gqdkpzrdxcvix2k6pxafbxxxnilszn3wss2wd924jsqxcd8qqa8")))

