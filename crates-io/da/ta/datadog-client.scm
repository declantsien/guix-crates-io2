(define-module (crates-io da ta datadog-client) #:use-module (crates-io))

(define-public crate-datadog-client-0.1.0 (c (n "datadog-client") (v "0.1.0") (d (list (d (n "mockito") (r "^0.29") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dvx3hybhf67688j23b2qvkqwmlhhr5xaxvm6gwbg1k7li9a33cf")))

(define-public crate-datadog-client-0.1.1 (c (n "datadog-client") (v "0.1.1") (d (list (d (n "mockito") (r "^0.29") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0j6l5wwm04dwdcfnhlzpflybwlz89nnv57fax6vramsa1mfxs745")))

(define-public crate-datadog-client-0.2.0 (c (n "datadog-client") (v "0.2.0") (d (list (d (n "mockito") (r "^0.29") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1nhmnkf4czp948fxcfk3s06qckgirnaw4xl2sp1yi4kmj66fy738")))

