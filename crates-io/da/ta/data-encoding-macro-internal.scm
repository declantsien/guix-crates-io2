(define-module (crates-io da ta data-encoding-macro-internal) #:use-module (crates-io))

(define-public crate-data-encoding-macro-internal-0.1.0 (c (n "data-encoding-macro-internal") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0gpppzx3yijs4mvhdg89cvk9mfsvkjxn8vm2313zb6lcj0zc6wsr") (f (quote (("stable" "proc-macro-hack"))))))

(define-public crate-data-encoding-macro-internal-0.1.1 (c (n "data-encoding-macro-internal") (v "0.1.1") (d (list (d (n "data-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1wawjbqwr5wgbwbckps8qsqchfma8y76fnkd9k3n2fvd1381rwxr") (f (quote (("stable" "proc-macro-hack"))))))

(define-public crate-data-encoding-macro-internal-0.1.2 (c (n "data-encoding-macro-internal") (v "0.1.2") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0h0xsgiq393vsvdn28380acfavh5n2j8bsd8afm6zi8dh4x240c1") (f (quote (("stable" "proc-macro-hack"))))))

(define-public crate-data-encoding-macro-internal-0.1.3 (c (n "data-encoding-macro-internal") (v "0.1.3") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1i7mpa8m0lmpzvkfk4bqnf52qilm9yp6sk6jr91y2fcwb9s948vz") (f (quote (("stable" "proc-macro-hack"))))))

(define-public crate-data-encoding-macro-internal-0.1.4 (c (n "data-encoding-macro-internal") (v "0.1.4") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0yc2znzdjrgmnjaylgr0c7vdhp4nphbs6caqgfgqcbnlgymhz0i5") (f (quote (("stable" "proc-macro-hack"))))))

(define-public crate-data-encoding-macro-internal-0.1.5 (c (n "data-encoding-macro-internal") (v "0.1.5") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0pkd6iqqk9rjfxab2hmaim3fmwahzxjn8q4yp0k22rqg191sq3pl") (f (quote (("stable" "proc-macro-hack"))))))

(define-public crate-data-encoding-macro-internal-0.1.6 (c (n "data-encoding-macro-internal") (v "0.1.6") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "00z5hydnry7i9v3yg1s99l0ym65z5mywq7y68pglav6mssl1icr2") (f (quote (("stable" "proc-macro-hack"))))))

(define-public crate-data-encoding-macro-internal-0.1.7 (c (n "data-encoding-macro-internal") (v "0.1.7") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0ylg6ckyjj97fbr4ccpydafjflkn03b2nmni1jzdbwwjlbvachfx") (f (quote (("stable" "proc-macro-hack"))))))

(define-public crate-data-encoding-macro-internal-0.1.8 (c (n "data-encoding-macro-internal") (v "0.1.8") (d (list (d (n "data-encoding") (r "^2.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "17hask05s31a4dsc01ri8a6h5fncgnmx59cim1s4n7azznp6sbcd") (f (quote (("stable" "proc-macro-hack"))))))

(define-public crate-data-encoding-macro-internal-0.1.9 (c (n "data-encoding-macro-internal") (v "0.1.9") (d (list (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ajzimpacnj4xa2h2bxb0l1mk29k61gr91z1aqycmar7jxlkxy7h")))

(define-public crate-data-encoding-macro-internal-0.1.10 (c (n "data-encoding-macro-internal") (v "0.1.10") (d (list (d (n "data-encoding") (r "^2.3") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1nrqy2c3azch285a9ya63qx43f25ldm58l0ans0fg5dav91fvfx5")))

(define-public crate-data-encoding-macro-internal-0.1.11 (c (n "data-encoding-macro-internal") (v "0.1.11") (d (list (d (n "data-encoding") (r "^2.4") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0wppwhrpg25zdcvyshvb6ximcr1wrinipzfpq6g56qz87k73zpwg") (r "1.47")))

(define-public crate-data-encoding-macro-internal-0.1.12 (c (n "data-encoding-macro-internal") (v "0.1.12") (d (list (d (n "data-encoding") (r "^2.5.0") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1wvn4p7wzr6p8fy8q9qpzgbvb9j1k3b5016867b7vcc95izx0iq0") (r "1.48")))

(define-public crate-data-encoding-macro-internal-0.1.13 (c (n "data-encoding-macro-internal") (v "0.1.13") (d (list (d (n "data-encoding") (r "^2.6.0") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0gyk580bjsjdy10n06f4gqh5kkqy296ndvcg21ychfzm1967ab9k") (r "1.48")))

