(define-module (crates-io da ta data_chain) #:use-module (crates-io))

(define-public crate-data_chain-0.1.0 (c (n "data_chain") (v "0.1.0") (d (list (d (n "clippy") (r "~0.0.64") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.4.15") (d #t) (k 0)) (d (n "maidsafe_utilities") (r "~0.5.4") (d #t) (k 0)) (d (n "rayon") (r "~0.4.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)) (d (n "sodiumoxide") (r "~0.0.10") (d #t) (k 0)))) (h "1hvn43hm93ldcss9kscg994656w925r23lww4nvvv72yf4bza0y4")))

(define-public crate-data_chain-0.1.1 (c (n "data_chain") (v "0.1.1") (d (list (d (n "clippy") (r "~0.0.64") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.4.15") (d #t) (k 0)) (d (n "maidsafe_utilities") (r "~0.5.4") (d #t) (k 0)) (d (n "rayon") (r "~0.4.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)) (d (n "sodiumoxide") (r "~0.0.10") (d #t) (k 0)))) (h "0aaidibamlx4v07n28mmw64nxaap8vsxdaf4i1afiva7n01giac4")))

