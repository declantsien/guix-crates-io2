(define-module (crates-io da ta database-tree) #:use-module (crates-io))

(define-public crate-database-tree-0.1.0-alpha.1 (c (n "database-tree") (v "0.1.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.4.1") (f (quote ("mysql" "chrono" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18fywypykc2v6ikwmy6j4xp7507qp9samlnylpqnbyibalixa8m9")))

(define-public crate-database-tree-0.1.0-alpha.2 (c (n "database-tree") (v "0.1.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15rlfrj7qld4gnivzbzlifpa23kpd56k4wanifk50lkgxd3x3kzh")))

(define-public crate-database-tree-0.1.0-alpha.3 (c (n "database-tree") (v "0.1.0-alpha.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0575jq6avdygbfxm9rj5b3dfssmq01n58wlz5h17699f1jfibrv2")))

(define-public crate-database-tree-0.1.0-alpha.4 (c (n "database-tree") (v "0.1.0-alpha.4") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ls2nxkxzxmjw7p4dcw18w6zkf836vma0ynsafbail0spjznicax")))

(define-public crate-database-tree-0.1.0-alpha.5 (c (n "database-tree") (v "0.1.0-alpha.5") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wwyly5k18b4bbg1rlnl7254x9hgwn9k350zf2i1vvfxssq5ix6f")))

