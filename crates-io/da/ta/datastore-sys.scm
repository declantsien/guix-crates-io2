(define-module (crates-io da ta datastore-sys) #:use-module (crates-io))

(define-public crate-datastore-sys-0.1.0 (c (n "datastore-sys") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "grpc") (r "0.*") (d #t) (k 0)) (d (n "protobuf") (r "1.*") (d #t) (k 0)))) (h "08hciwcf4l278r1rp1cddl3bd5cssdlh4pmzhdqi6siz7al9ywdz")))

