(define-module (crates-io da ta data-query-proc) #:use-module (crates-io))

(define-public crate-data-query-proc-0.1.0 (c (n "data-query-proc") (v "0.1.0") (d (list (d (n "data-query-lexical") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0w9mz7n736wl3pv4z6m0by8xx57zvfvh6hbaz89bl9qk5780j3lr") (y #t)))

(define-public crate-data-query-proc-0.1.1 (c (n "data-query-proc") (v "0.1.1") (d (list (d (n "data-query-lexical") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0wkhpk68dza0sqih00l10hk368ca46gm5f0ssbyz1qirw758w2ig") (y #t)))

(define-public crate-data-query-proc-0.1.3 (c (n "data-query-proc") (v "0.1.3") (d (list (d (n "data-query-lexical") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0bzlfrbkfzg6qd8ch73qfvmcqyi8j65w69z4nkf03qvy5aj14sfv")))

