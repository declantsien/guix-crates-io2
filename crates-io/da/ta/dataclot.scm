(define-module (crates-io da ta dataclot) #:use-module (crates-io))

(define-public crate-dataclot-0.0.0 (c (n "dataclot") (v "0.0.0") (d (list (d (n "server") (r "^0.0.0") (d #t) (k 0) (p "dataclot-server")) (d (n "tokio") (r "^1.34") (f (quote ("full"))) (d #t) (k 0)))) (h "0apfl81dbqwj9z6l3kz7qh0yjyjf9wzkfdhff33zlz5w4j65x3lb")))

(define-public crate-dataclot-0.0.1 (c (n "dataclot") (v "0.0.1") (d (list (d (n "server") (r "^0.0.1") (d #t) (k 0) (p "dataclot-server")) (d (n "tokio") (r "^1.34") (f (quote ("full"))) (d #t) (k 0)))) (h "1dhrjmaiifndy3ywpff4yx1y739pnyh7zi5427clyfz2207zj95y")))

