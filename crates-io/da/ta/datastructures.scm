(define-module (crates-io da ta datastructures) #:use-module (crates-io))

(define-public crate-datastructures-0.1.0 (c (n "datastructures") (v "0.1.0") (h "0jbd3cji2cs3gb5540xyx3ch5h5mp2hj4cyh58365bwj7xp7cf47")))

(define-public crate-datastructures-0.1.1 (c (n "datastructures") (v "0.1.1") (h "06rpa3s2b92wlh09064mb4c5vnya02ii3mrd3i53z9a8psi8z0b1")))

(define-public crate-datastructures-0.1.2 (c (n "datastructures") (v "0.1.2") (h "1qqdw616qmdayk7nb3y2mx1g4pgz96hmayl0lxs8h8y3w1gz7nh8")))

(define-public crate-datastructures-0.1.3 (c (n "datastructures") (v "0.1.3") (h "1lb6yy3kg3cr3nxljzcdngv8c7snnissd8fgdff5g9xsnwrh0ccf")))

(define-public crate-datastructures-0.1.4 (c (n "datastructures") (v "0.1.4") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1hfp7z2p44hsaqs5mnn9qfqq3m1z6drbdh1alar8d5cdckxdlb2j")))

