(define-module (crates-io da ta datasus-dbc) #:use-module (crates-io))

(define-public crate-datasus-dbc-0.1.0 (c (n "datasus-dbc") (v "0.1.0") (d (list (d (n "explode") (r "^0.1.2") (d #t) (k 0)))) (h "1mz57cgwijlmbyq6kwhviq5sl0xy56ra6qxk88ds3rjs9bvydg9j")))

(define-public crate-datasus-dbc-0.1.1 (c (n "datasus-dbc") (v "0.1.1") (d (list (d (n "explode") (r "^0.1.2") (d #t) (k 0)))) (h "06ifjikkc3zb9jsida2rk9qk3p2q6mzl5hc923sc0ihkv3kiy9vd")))

