(define-module (crates-io da ta data_tree) #:use-module (crates-io))

(define-public crate-data_tree-0.1.0 (c (n "data_tree") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)))) (h "0as5zdcvlcx1d0qi1xcg3h2v9lgbp7fjz1hfpvnm20jc44s9wks6")))

(define-public crate-data_tree-0.1.1 (c (n "data_tree") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f597zpqbcg4f32kd100vd1yna9adavgl6znnn7k66n9ng4id06i")))

(define-public crate-data_tree-0.1.2 (c (n "data_tree") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)))) (h "014f5hksypj4rqlsfik4fri86qbzwszglyfn1y3yckpmrawn141g")))

(define-public crate-data_tree-0.1.3 (c (n "data_tree") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mns4j7snppk2danalgdh81dd3vslvnb726fcpfhw2v2gz0wy1vb")))

(define-public crate-data_tree-0.1.4 (c (n "data_tree") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)))) (h "1abbvg9bh1z1dw3lvqh2k73mm0bp8x69m89k9yj5jwaip642b3mp")))

(define-public crate-data_tree-0.1.5 (c (n "data_tree") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hdmp9vgn7cilqx7bvjyjbwkwnl9pgj9zwiv0r5idli6n0yzr9pa")))

