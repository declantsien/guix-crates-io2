(define-module (crates-io da ta datapack) #:use-module (crates-io))

(define-public crate-datapack-1.0.0 (c (n "datapack") (v "1.0.0") (d (list (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0cbmk25qrrwpk23ilxsfpbsngv4j7q0nfx737crk301ill5fsqq5") (y #t)))

(define-public crate-datapack-1.0.1 (c (n "datapack") (v "1.0.1") (d (list (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1a0wa4wpwppwvhn7r9pd29xaifg6q9il2ajndfidnmia1mlxb7s2") (y #t)))

(define-public crate-datapack-1.0.2 (c (n "datapack") (v "1.0.2") (d (list (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "09lvnn67hw547vhar9x8wcdvab337acw3jcrr74clb21dsbqykv9")))

(define-public crate-datapack-2.0.0 (c (n "datapack") (v "2.0.0") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.6.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "093mhdzv72w5dvn7z27gfw2rmnv16riy49ad4rmym6b1jr7ww461")))

(define-public crate-datapack-2.0.1 (c (n "datapack") (v "2.0.1") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.6.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0ix66a3j7nxnsg0s240xzh2122aq2f5j8cqhwcnlsf5c5vdgh8qr")))

