(define-module (crates-io da ta datas) #:use-module (crates-io))

(define-public crate-datas-0.1.0 (c (n "datas") (v "0.1.0") (h "1cd0j2r2w815x21avywvwy0w21sgvpgrq05dj3lm2a3yffk7wn55") (y #t)))

(define-public crate-datas-0.1.1 (c (n "datas") (v "0.1.1") (h "1m6v4hs57gnnnyj4ab8rnpig8skh9gdgpqxfzp9fqcahg4hmsni8") (y #t)))

(define-public crate-datas-0.1.2 (c (n "datas") (v "0.1.2") (h "0g315wpi0x3rmkx6q8hkpwq3fhsg5p6vakv2nril2vsbdd1v2yf7") (y #t)))

(define-public crate-datas-0.1.3 (c (n "datas") (v "0.1.3") (h "1bhd3sh9h5sjrvyg1rsm663fl3w4k8gf35hwmdi2j93l1jxb4nh9") (y #t)))

(define-public crate-datas-0.1.4 (c (n "datas") (v "0.1.4") (h "1ps7gr4j7n3214fasydfskg8rz9sn36q8nmghc856d11q4h4yr9z") (y #t)))

(define-public crate-datas-0.1.5 (c (n "datas") (v "0.1.5") (h "1kjn02b6hahmfyk65gg6k1h2vcph3ni2na8vjsckm1ccam5iyq8a")))

