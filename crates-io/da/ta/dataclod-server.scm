(define-module (crates-io da ta dataclod-server) #:use-module (crates-io))

(define-public crate-dataclod-server-0.0.2 (c (n "dataclod-server") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "datafusion") (r "^33.0") (f (quote ("simd"))) (d #t) (k 0)) (d (n "datafusion-util") (r "^0.0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "pgwire") (r "^0.16.1") (d #t) (k 0)) (d (n "query") (r "^0.0.2") (d #t) (k 0) (p "dataclod-query")) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("full"))) (d #t) (k 0)))) (h "0hc0i88q9ig5m582gfv7czgr8p3cp8g4jkws3z0vxv7vyplq6gcr")))

