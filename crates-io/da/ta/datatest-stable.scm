(define-module (crates-io da ta datatest-stable) #:use-module (crates-io))

(define-public crate-datatest-stable-0.1.0 (c (n "datatest-stable") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0afj7wqv1im1sq2d97baflx2y4dfqnakivq9hjhpd354giasmgd7") (y #t)))

(define-public crate-datatest-stable-0.1.1 (c (n "datatest-stable") (v "0.1.1") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "07rkx728imqsp4rph1d238z33zclncfdbj31kz9l0vzz5ij05zx0")))

(define-public crate-datatest-stable-0.1.2 (c (n "datatest-stable") (v "0.1.2") (d (list (d (n "libtest-mimic") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0b9phqikk7jkaapc86xsp6ljpslbjgpmmmd838x6walpnwg2h1dj")))

(define-public crate-datatest-stable-0.1.3 (c (n "datatest-stable") (v "0.1.3") (d (list (d (n "libtest-mimic") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0xsrdv3i79j7dwffvvj52w49y17fr61pz3idwkv222lz9vj8dbsf")))

(define-public crate-datatest-stable-0.2.0 (c (n "datatest-stable") (v "0.2.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0jaq4idffnf6sasjrsz7rgfw05ig4k57q34z401paallgvp5ml36") (r "1.60")))

(define-public crate-datatest-stable-0.2.1 (c (n "datatest-stable") (v "0.2.1") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0zqd1fmyf1i69nc3832zyf5wz37ybhaz4rsw2ildqq3da1zihycg") (r "1.60")))

(define-public crate-datatest-stable-0.2.2 (c (n "datatest-stable") (v "0.2.2") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "09dyr5pa79dgzq7829c5rw0rlgpkz5gn17dchragijz996sm9lnb") (r "1.60")))

(define-public crate-datatest-stable-0.2.3 (c (n "datatest-stable") (v "0.2.3") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1nip930pwrgz25ciif9h9jr6j9zbazy3h35z9m7pgw094v8898r2") (r "1.60")))

(define-public crate-datatest-stable-0.2.4 (c (n "datatest-stable") (v "0.2.4") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1zl4wkdxl5r79aar4psg2jwp9kw0y2m28c8082yjzfbva7ps06sw") (r "1.65")))

(define-public crate-datatest-stable-0.2.5 (c (n "datatest-stable") (v "0.2.5") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1yh0jisfqa1y9f4xgr3hjp46x1zx355k1gczdq33jpr3jgpyb7nq") (r "1.65")))

(define-public crate-datatest-stable-0.2.6 (c (n "datatest-stable") (v "0.2.6") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "04vahwv142iaqcrj0zz4yv027ijl2b6jxi1xrxb39w23a4ibs20d") (r "1.65")))

(define-public crate-datatest-stable-0.2.7 (c (n "datatest-stable") (v "0.2.7") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.13.0") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1zk6j1wrknzrxfgjmpwkw38bqc1f50xswbpr2bv0gaz6wdqbq2x0") (r "1.66")))

(define-public crate-datatest-stable-0.2.8 (c (n "datatest-stable") (v "0.2.8") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "camino-tempfile") (r "^1.1.1") (d #t) (t "cfg(unix)") (k 2)) (d (n "fancy-regex") (r "^0.13.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "libtest-mimic") (r "^0.7.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "07cn255wfbinmrwzha30rhqlx2zf0dqqgb72h71g8zyq628spdrw") (r "1.66")))

(define-public crate-datatest-stable-0.2.9 (c (n "datatest-stable") (v "0.2.9") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "camino-tempfile") (r "^1.1.1") (d #t) (t "cfg(unix)") (k 2)) (d (n "fancy-regex") (r "^0.13.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "libtest-mimic") (r "^0.7.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1wg7aw2frq5iwdk0n1cnwvzxrkrw4iqslmylgcwmcfs643yv6q55") (r "1.66")))

