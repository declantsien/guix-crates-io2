(define-module (crates-io da ta datazoo) #:use-module (crates-io))

(define-public crate-datazoo-0.1.0 (c (n "datazoo") (v "0.1.0") (h "14r20if7sd5y5ssxki89ayb6qy3pkpp5cb8r6cpn3skx6xxi7cy9")))

(define-public crate-datazoo-0.2.0 (c (n "datazoo") (v "0.2.0") (d (list (d (n "enumset") (r "^1.1") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "sorted-iter") (r "^0.1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mk10ivcc41ykl83y6xqj7vpll0zcrn5siw0c5zcnxp6wwa5fg87") (f (quote (("default" "enumset"))))))

(define-public crate-datazoo-0.3.0 (c (n "datazoo") (v "0.3.0") (d (list (d (n "enumset") (r "^1.1") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "sorted-iter") (r "^0.1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1pa9gk71adb4d0hkysvwskzvq6a5cg7jhqd6kw3pzzrx81bbqdrn") (f (quote (("default" "enumset"))))))

(define-public crate-datazoo-0.4.0 (c (n "datazoo") (v "0.4.0") (d (list (d (n "enumset") (r "^1.1") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "sorted-iter") (r "^0.1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13b0s30mj0s3w5bl5ann4j5cf14syynsk61m7b55y20l5mv8zrdp") (f (quote (("default" "enumset"))))))

(define-public crate-datazoo-0.5.0 (c (n "datazoo") (v "0.5.0") (d (list (d (n "enumset") (r "^1.1") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "smallvec") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "sorted-iter") (r "^0.1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03vkz5wmmac0z3vssrqj6hj0mnm10h3fqwi7r5c33bsjjm6dm2kn") (f (quote (("default" "enumset"))))))

(define-public crate-datazoo-0.6.0 (c (n "datazoo") (v "0.6.0") (d (list (d (n "enumset") (r "^1.1") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "smallvec") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "sorted-iter") (r "^0.1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dbyvwn4fpgs7ligvxnhb5xlawbnggindab62983ac8frgrmgypm") (f (quote (("default" "enumset"))))))

(define-public crate-datazoo-0.7.0 (c (n "datazoo") (v "0.7.0") (d (list (d (n "enumset") (r "^1.1") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.1") (o #t) (d #t) (k 0)) (d (n "sorted-iter") (r "^0.1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0d6da8v84if3j54ji5zr93m3ivf2q4qvx05xxqi2wkz782i7a3cn") (f (quote (("default" "enumset"))))))

