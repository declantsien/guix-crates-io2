(define-module (crates-io da ta datacake-rocks) #:use-module (crates-io))

(define-public crate-datacake-rocks-0.0.1 (c (n "datacake-rocks") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "datacake-cluster") (r "^0.0.1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "moka") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7.39") (f (quote ("validation"))) (d #t) (k 0)) (d (n "rocksdb") (r "^0.19.0") (f (quote ("lz4"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1bvqf5wcgww85k5pll8zdwjgcwnfj8ia656yk03w8g140npgypls") (f (quote (("cache" "moka")))) (y #t)))

