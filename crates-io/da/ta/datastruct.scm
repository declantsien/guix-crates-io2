(define-module (crates-io da ta datastruct) #:use-module (crates-io))

(define-public crate-datastruct-0.1.0 (c (n "datastruct") (v "0.1.0") (d (list (d (n "datastruct_derive") (r "^0.1.0") (d #t) (k 0)))) (h "17hk98pcsj3flbj6i395wa0whp2cvard4vjy970v4mwwn3xzvifq") (y #t)))

(define-public crate-datastruct-0.1.1 (c (n "datastruct") (v "0.1.1") (d (list (d (n "datastruct_derive") (r "^0.1.1") (d #t) (k 0)))) (h "18bvn6pj4b42sd2kqy3l6an60x6jpfllbb2my81zndmyfpx44y2z")))

