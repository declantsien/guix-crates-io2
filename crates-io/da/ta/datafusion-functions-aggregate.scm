(define-module (crates-io da ta datafusion-functions-aggregate) #:use-module (crates-io))

(define-public crate-datafusion-functions-aggregate-38.0.0 (c (n "datafusion-functions-aggregate") (v "38.0.0") (d (list (d (n "arrow") (r "^51.0.0") (f (quote ("prettyprint"))) (d #t) (k 0)) (d (n "datafusion-common") (r "^38.0.0") (k 0)) (d (n "datafusion-execution") (r "^38.0.0") (d #t) (k 0)) (d (n "datafusion-expr") (r "^38.0.0") (d #t) (k 0)) (d (n "datafusion-physical-expr-common") (r "^38.0.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "sqlparser") (r "^0.45.0") (f (quote ("visitor"))) (d #t) (k 0)))) (h "16frbjv4w0wywjnadjhxbmf81785rvk6ragpz1dlpfcahg2acdhv") (r "1.73")))

