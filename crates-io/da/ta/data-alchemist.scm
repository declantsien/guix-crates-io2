(define-module (crates-io da ta data-alchemist) #:use-module (crates-io))

(define-public crate-data-alchemist-0.1.0 (c (n "data-alchemist") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0ikh4kq60c1ikg9ws4jd3sfjsa8p9q4ssygzklp178ixxkp45vj9")))

(define-public crate-data-alchemist-0.1.1 (c (n "data-alchemist") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0q6sap41kmk4x5fr7f3a0qfv9bd4dpvjr6pknm4605h0mh43ai9b")))

(define-public crate-data-alchemist-0.1.2 (c (n "data-alchemist") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1aq3hqh9crgpnh4xwpk5xfjapn6i76ixa5jz55qza1472kbivmi0")))

