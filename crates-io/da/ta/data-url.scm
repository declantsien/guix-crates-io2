(define-module (crates-io da ta data-url) #:use-module (crates-io))

(define-public crate-data-url-0.1.0 (c (n "data-url") (v "0.1.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "176wa1n8h71iwyaxhar4sqwrgrvb5sxk26az0fy88vnxrsffjgyk")))

(define-public crate-data-url-0.1.1 (c (n "data-url") (v "0.1.1") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14z15yiyklp5dv0k0q6pd83irrn0y8hj9y3fj17akkrbf37byc1s")))

(define-public crate-data-url-0.2.0 (c (n "data-url") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tester") (r "^0.9") (d #t) (k 2)))) (h "19828d6jby17ghi7vr0zia9sy3hlvvjbngrcsllmfh2zfg1kjx4d") (r "1.51")))

(define-public crate-data-url-0.3.0 (c (n "data-url") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tester") (r "^0.9") (d #t) (k 2)))) (h "17r8qh9kapw53m37g5bwjabryazls7mnpwspw01d1yrgnv8ikcs1") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.51")))

(define-public crate-data-url-0.3.1 (c (n "data-url") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tester") (r "^0.9") (d #t) (k 2)))) (h "0ahclz72myi350cs1xcsxdh1v0iljpfj4ghcy2fy46mpfhf7laaw") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.51")))

