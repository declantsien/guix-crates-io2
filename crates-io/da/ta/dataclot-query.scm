(define-module (crates-io da ta dataclot-query) #:use-module (crates-io))

(define-public crate-dataclot-query-0.1.0 (c (n "dataclot-query") (v "0.1.0") (d (list (d (n "datafusion") (r "^33.0") (f (quote ("simd"))) (d #t) (k 0)))) (h "177wih98z1axgzi6s1icqz7z6yqbzqx5sz0psfd9j8zclx5qp3m5")))

(define-public crate-dataclot-query-0.0.0 (c (n "dataclot-query") (v "0.0.0") (d (list (d (n "datafusion") (r "^33.0") (f (quote ("simd"))) (d #t) (k 0)))) (h "1b8slv8szi91zpp7j7imxffn2qdiz12cz3xkrxirzpjkhpwg8h7p")))

(define-public crate-dataclot-query-0.0.1 (c (n "dataclot-query") (v "0.0.1") (d (list (d (n "datafusion") (r "^33.0") (f (quote ("simd"))) (d #t) (k 0)))) (h "1v7yx7xzsg8c82rr9ndfhh1b9ipb106x4i5q5k9fr91jayj8007j")))

