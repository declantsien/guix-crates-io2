(define-module (crates-io da ta datatroll) #:use-module (crates-io))

(define-public crate-datatroll-0.1.0 (c (n "datatroll") (v "0.1.0") (h "157yjlnwwl8l4kaslrvwcz3xbvdcidrsjlrpk3s7pmcfgmh8hi6s")))

(define-public crate-datatroll-0.1.1 (c (n "datatroll") (v "0.1.1") (h "04h470ggyswynvgimp5gayln0v9magpmf2chdglgwzf6qqcvm6d5")))

(define-public crate-datatroll-0.1.2 (c (n "datatroll") (v "0.1.2") (h "0cw302w2k1a7vb0kw46cnws239shwkv68w35ljmvgmcg5qcv1n9d")))

(define-public crate-datatroll-0.1.3 (c (n "datatroll") (v "0.1.3") (h "1r8ifl89clbpjdcgn0qzgjjjvdgxa4590lfrvrcd6c68ayrz9lvs")))

