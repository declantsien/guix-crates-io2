(define-module (crates-io da ta data_model) #:use-module (crates-io))

(define-public crate-data_model-0.1.0 (c (n "data_model") (v "0.1.0") (d (list (d (n "assertions") (r "^0.1") (d #t) (k 0)))) (h "1lnwcrzavvyabga8kj71611fn5s52n26mf8ni6d770a8q35ls39w")))

(define-public crate-data_model-0.1.0-alpha.0 (c (n "data_model") (v "0.1.0-alpha.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.116") (d #t) (k 0)) (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything" "std" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07i8b6zbsql0831vgbfsa4mjhqi5fz7ssljmqr5cpwjw4jfglms8")))

(define-public crate-data_model-0.1.1-alpha.0 (c (n "data_model") (v "0.1.1-alpha.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.116") (d #t) (k 0)) (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything" "std" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02l2g0lrdidxhckjcx971zhdljv549i8zbk2gm5ggmd8zrb0sm0v")))

(define-public crate-data_model-0.1.1-alpha.1 (c (n "data_model") (v "0.1.1-alpha.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.116") (d #t) (k 0)) (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything" "std" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wciqh5g7504irbd5cvww0mmb57dgjpx0c4jkc3h3d979pfw9f1b")))

