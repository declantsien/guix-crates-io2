(define-module (crates-io da ta datafet) #:use-module (crates-io))

(define-public crate-datafet-0.1.0 (c (n "datafet") (v "0.1.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)))) (h "0zhmkia4z80f0vjlxgasa2nvnlibf6s6fhbv9vmhr9xdgi13qblr") (y #t)))

(define-public crate-datafet-0.2.0 (c (n "datafet") (v "0.2.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)))) (h "1mpii3a9l8hjpxgqp5sfc24pp53m1p09588pqbq27m2dvhv6ybni") (y #t)))

(define-public crate-datafet-0.2.1 (c (n "datafet") (v "0.2.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)))) (h "0hi47vmvyz7jql176ayzgk6vn5kbjzvjf9mq23rwcgvgaiaf8nxy") (y #t)))

(define-public crate-datafet-0.3.0 (c (n "datafet") (v "0.3.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)) (d (n "murmurhash64") (r "^0.3.1") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "02p7aypbdm5a9c6lg37d2plxbdjapw3d0vbsl9w2nlmmkdi5ifyh")))

(define-public crate-datafet-0.3.1 (c (n "datafet") (v "0.3.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0c4dm53gyrmqs2v70zf2x3brfxxz8hf0pbr4bav9qldb73cwg4mc") (f (quote (("time") ("eid"))))))

