(define-module (crates-io da ta datasaurust) #:use-module (crates-io))

(define-public crate-datasaurust-0.1.0 (c (n "datasaurust") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.37") (d #t) (k 0)) (d (n "kdam") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0qvddanxr6iczr31dynim6afcbq4jmpanxbnm24asgnny49683px")))

