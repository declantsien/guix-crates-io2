(define-module (crates-io da ta datalite) #:use-module (crates-io))

(define-public crate-datalite-0.1.0 (c (n "datalite") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mnvl7kn3ab2l0arwzwzcqyb0dy91m43l3pf3h91aprcbrphgm36")))

