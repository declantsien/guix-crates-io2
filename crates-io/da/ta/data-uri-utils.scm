(define-module (crates-io da ta data-uri-utils) #:use-module (crates-io))

(define-public crate-data-uri-utils-0.1.0 (c (n "data-uri-utils") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.21.2") (d #t) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "once_cell") (r "^0.2.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)))) (h "08zpp3r11my75k4gxkxrkc34qsgfqwaz8qxrpj1c9xd4zw9l7cgn")))

(define-public crate-data-uri-utils-0.2.0 (c (n "data-uri-utils") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)))) (h "07jbjgv9k3lj4x2b5cl3ckizh9zhv5lrs97ql9vqvqhanidhb6lm")))

