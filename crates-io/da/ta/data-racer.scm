(define-module (crates-io da ta data-racer) #:use-module (crates-io))

(define-public crate-data-racer-0.0.1 (c (n "data-racer") (v "0.0.1") (d (list (d (n "blake3") (r "^1.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18vd7m1r4lgkqgnw7n5fy716lar347w221h4svhpilb9msf73zs3") (y #t)))

(define-public crate-data-racer-0.1.0 (c (n "data-racer") (v "0.1.0") (d (list (d (n "blake3") (r "^1.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dxvyvf4kmlvfzg3j1ncvq7vddf3hmfc2a04rc2v2mv9iyzwlq0i") (y #t)))

(define-public crate-data-racer-0.1.1 (c (n "data-racer") (v "0.1.1") (d (list (d (n "blake3") (r "^1.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09qzwl1w2jn1fp7iwvgd0gb2y0y7s9ynvxwd28bn3hh55wc36y31") (y #t)))

(define-public crate-data-racer-0.1.2 (c (n "data-racer") (v "0.1.2") (d (list (d (n "blake3") (r "^1.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rkl7y66gpy5j30lypknd0fiifkhslvns87mh22l7r26qsq46hii") (y #t)))

(define-public crate-data-racer-0.2.0 (c (n "data-racer") (v "0.2.0") (d (list (d (n "blake3") (r "^1.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xr30xj7k2w1rdiil6sn9y3gfgmh9hkcxpsklh60i09zxnj1rdr7") (y #t)))

(define-public crate-data-racer-0.2.1 (c (n "data-racer") (v "0.2.1") (d (list (d (n "blake3") (r "^1.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1905xqc6n3q4njb3lp1dyials3503ncpxcg5nnkcxh5lg4cq90ya") (y #t)))

(define-public crate-data-racer-0.2.2 (c (n "data-racer") (v "0.2.2") (d (list (d (n "blake3") (r "^1.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dcm9cjijvpy8y96fj89g90hzc6f9qg7l4dk2cs061xklzar4niw") (y #t)))

(define-public crate-data-racer-0.2.3 (c (n "data-racer") (v "0.2.3") (d (list (d (n "blake3") (r "^1.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11ymzxgi240j3lihjbz73an1g1szjzs73ymw35jq1gf93l7llxjv") (y #t)))

