(define-module (crates-io da ta datagen-rs-progress-plugin) #:use-module (crates-io))

(define-public crate-datagen-rs-progress-plugin-0.1.0 (c (n "datagen-rs-progress-plugin") (v "0.1.0") (d (list (d (n "datagen-rs") (r "^0.1.0") (f (quote ("generate"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "06fgw71qciifrzc0y4sadly7faxrwajj9zccyir1lm6cy9xcim9b") (f (quote (("plugin") ("default"))))))

