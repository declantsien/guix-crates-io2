(define-module (crates-io da ta database) #:use-module (crates-io))

(define-public crate-database-0.0.1 (c (n "database") (v "0.0.1") (h "1672lyb71f9zrlfkvr6y7n8rf66xzh9kix5ay3nziyqc2a43jalj")))

(define-public crate-database-0.1.0 (c (n "database") (v "0.1.0") (d (list (d (n "sqlite") (r "^0.17") (o #t) (d #t) (k 0)))) (h "071pwfbgi5yi5cz8zpf3dy7r88dkgn47y8gk6qvy3qn5dzm7z521") (f (quote (("default" "sqlite"))))))

(define-public crate-database-0.2.0 (c (n "database") (v "0.2.0") (d (list (d (n "sqlite") (r "^0.17") (o #t) (d #t) (k 0)))) (h "14cm75zwjwjpj354xmr2m7ixa6s21dr3mvyl540fi4x4gn0a8z2i") (f (quote (("default" "sqlite"))))))

(define-public crate-database-0.3.0 (c (n "database") (v "0.3.0") (d (list (d (n "sqlite") (r "^0.17") (o #t) (d #t) (k 0)))) (h "1rc4s9zhicd007xx3cx95s71amky4x57gsa16p7d6vgksr08vri9") (f (quote (("default" "sqlite"))))))

(define-public crate-database-0.3.1 (c (n "database") (v "0.3.1") (d (list (d (n "sqlite") (r "^0.17") (o #t) (d #t) (k 0)))) (h "1qaqf2vbn5g7x9gqk57z70v7wz847pfspba6xfxgwrq633224nh6") (f (quote (("default" "sqlite"))))))

(define-public crate-database-0.3.2 (c (n "database") (v "0.3.2") (d (list (d (n "sqlite") (r "^0.17") (o #t) (d #t) (k 0)))) (h "0d6imv7fgrxf1y7xx3hzcs1dm12rniyip640irym5hqx2i4v1k1f") (f (quote (("default" "sqlite"))))))

(define-public crate-database-0.4.0 (c (n "database") (v "0.4.0") (h "0yc6aaz3wz935zi9vf4yha5pw0qm0v3nmnajflx8dmgyp34a5kay")))

(define-public crate-database-0.5.0 (c (n "database") (v "0.5.0") (h "0l1gqvd8pq97z12dlrk4zfk01bd2dx43p1z4jar2sv8ymwq1p1gl")))

