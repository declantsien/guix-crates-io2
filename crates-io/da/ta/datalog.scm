(define-module (crates-io da ta datalog) #:use-module (crates-io))

(define-public crate-datalog-0.0.1 (c (n "datalog") (v "0.0.1") (h "0yhzmw7xl9q088mh7wlm2f76bs0lzsm2bmvjbd4d75hxk96jgk09")))

(define-public crate-datalog-0.0.2 (c (n "datalog") (v "0.0.2") (h "0xpjaal06dbjskkw3n4llhz6rpjq7jc5cfxga6q00xcd10vp438j")))

