(define-module (crates-io da ta datamars) #:use-module (crates-io))

(define-public crate-datamars-0.1.0 (c (n "datamars") (v "0.1.0") (h "005h5q6v3xvj77kq628n1qg50j26y5611vq3ri0qzv3nm7sc2arf")))

(define-public crate-datamars-0.2.0 (c (n "datamars") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fl805f8d69a2d9qqfcy8p8qcpabvyic65mrazjxaglbxcyvwkl1")))

(define-public crate-datamars-0.2.1 (c (n "datamars") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fsncz9bqcga32l91fnjkmrk4746y8xsgsxxp8p42slg7j5xwfph")))

(define-public crate-datamars-0.2.2 (c (n "datamars") (v "0.2.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.1") (d #t) (k 0)))) (h "0jd8l102rymi7px83l6aqblrv1dwy275i2gsq695zwcnaxzwgj6h")))

(define-public crate-datamars-0.2.3 (c (n "datamars") (v "0.2.3") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.1") (d #t) (k 0)))) (h "1rx6v1261baymw1nc0br2sdiwzsdb56di6frkc3b8k21w13mq5xv")))

(define-public crate-datamars-0.3.0 (c (n "datamars") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.1") (d #t) (k 0)))) (h "1c46w0h3rvsf20l0qhxhpsx72zkd55wmg6y4y0ynnnahib29rqxk")))

(define-public crate-datamars-0.4.0 (c (n "datamars") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)))) (h "11vpb1ap5gcawvckn26mrw4v661v67a2vmk5qx9p0w4fpny2kk9d")))

