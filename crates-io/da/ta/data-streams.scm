(define-module (crates-io da ta data-streams) #:use-module (crates-io))

(define-public crate-data-streams-1.0.0 (c (n "data-streams") (v "1.0.0") (d (list (d (n "bytemuck") (r "^1.14.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (f (quote ("i128"))) (d #t) (k 0)) (d (n "simdutf8") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1nndlqxlnx077rdbfslyfa9pxv37jysd1wg1mlnnck5k60rnxn3k")))

