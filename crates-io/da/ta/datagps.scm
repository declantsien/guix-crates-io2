(define-module (crates-io da ta datagps) #:use-module (crates-io))

(define-public crate-dataGPS-0.1.0 (c (n "dataGPS") (v "0.1.0") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "1bp1my4ijkz49gs5900msv32a4grg4m2jjph8iqrhq47mi42gi9q")))

(define-public crate-dataGPS-0.1.1 (c (n "dataGPS") (v "0.1.1") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "04kmpmlf7vbryjmkj9ij646xscpqwqlsbb7a7n7yp5x928lr7qg6")))

