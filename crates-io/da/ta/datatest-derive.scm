(define-module (crates-io da ta datatest-derive) #:use-module (crates-io))

(define-public crate-datatest-derive-0.2.0 (c (n "datatest-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("full"))) (d #t) (k 0)))) (h "1rff70gr0yrimpfwi17yy5rqqz19gx3ml6afqq7fhsdl1ml9gkb5")))

(define-public crate-datatest-derive-0.2.1 (c (n "datatest-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("full"))) (d #t) (k 0)))) (h "1sb25n2wv3d8vjs62k0pzvjmjnq9g3r5sxl9am42408fnlpyi67q")))

(define-public crate-datatest-derive-0.2.2 (c (n "datatest-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("full"))) (d #t) (k 0)))) (h "00xk0darg0wsac8zvrccjvzd4dxzmlcsk59r39m4clnwdzrd5f6c")))

(define-public crate-datatest-derive-0.3.0 (c (n "datatest-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("full"))) (d #t) (k 0)))) (h "05zla8skcc2k8211zjsh14diqiqcdhngdp51l9p40scarrh5gfxr")))

(define-public crate-datatest-derive-0.3.1 (c (n "datatest-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("full"))) (d #t) (k 0)))) (h "1zaxwi9dcm3qrhvydizhdqndg9j9kmd0f3pvfq7a1s8zc22s44x6")))

(define-public crate-datatest-derive-0.3.2 (c (n "datatest-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("full"))) (d #t) (k 0)))) (h "1wsifnjl28w5gspcs3p6fnm57ws3xdlx47wsr8ixn5zhkb45m9md")))

(define-public crate-datatest-derive-0.3.4 (c (n "datatest-derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("full"))) (d #t) (k 0)))) (h "12gvgikg46gwd1mvb75nf0mhdsmxdzhnkg2q81yq0nvicj7sw1bh")))

(define-public crate-datatest-derive-0.3.5 (c (n "datatest-derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("full"))) (d #t) (k 0)))) (h "1ba20x3bfnfq86xi0i3j2hyj757mn7n4amvmg2sdhi5fiw7p8szz")))

(define-public crate-datatest-derive-0.4.0 (c (n "datatest-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "134cr5p1z9gyrv1sfnjjk7n88ybd6bf4glpsvm2ac3626wabmkm7")))

(define-public crate-datatest-derive-0.5.0 (c (n "datatest-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0fl1rjrgpwbwf6xracnh7p5wqg32f6kn47w3mr9z56xsbbzfbzzx")))

(define-public crate-datatest-derive-0.5.1 (c (n "datatest-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0pw660kpnzfglfnimj3xyknixbghy0qyyr04mhcmzivwh4ylni1s")))

(define-public crate-datatest-derive-0.5.2 (c (n "datatest-derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1gca8iqh3vk6qf8c04n2x2s90fvnd15vk2ag0jn07xak8dz3yi2k")))

(define-public crate-datatest-derive-0.6.0 (c (n "datatest-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0dv2708ncisziialxl67n0xlm05skhlbbcrczbdw07mbzd4nbv0l")))

(define-public crate-datatest-derive-0.6.1-alpha.0 (c (n "datatest-derive") (v "0.6.1-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0cnzi222b6r52h51vmwn3hca6jjv43wg5kcc0ajyc1mskjhpq4qg") (y #t)))

(define-public crate-datatest-derive-0.6.2 (c (n "datatest-derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1i8y218ipapg2wdmyqwnn04c039wqsrnkz2afdqqn7qyzcsyqayz")))

(define-public crate-datatest-derive-0.6.3 (c (n "datatest-derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "14kj0cxrhrcq9flqfgwv4qv06xnv7jj1n0g5pnsn4gfwlsgqdxpp")))

(define-public crate-datatest-derive-0.7.0 (c (n "datatest-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1fwbjym82dls9z24km0b3h8l1f4m073hmk5h95a4wxh5biqqhzsp")))

(define-public crate-datatest-derive-0.7.1 (c (n "datatest-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1sza64fmpd7clv54dh2isi6wbbn074hk9igxg0i2q9i0j9vj7hs1")))

(define-public crate-datatest-derive-0.8.0 (c (n "datatest-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "159nzjh6ajw78qdph4fkj78xz6bxcnr57nii0f7yw6xf05472cy6")))

