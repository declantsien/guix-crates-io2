(define-module (crates-io da ta data_reader) #:use-module (crates-io))

(define-public crate-data_reader-0.1.0 (c (n "data_reader") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2.7") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lexical") (r "^2.0.0") (d #t) (k 0)))) (h "1jss662mm32w49csf5ddgl3a6m2m9mv7hbv4sjsnhipv1zg6bcck")))

(define-public crate-data_reader-0.1.1 (c (n "data_reader") (v "0.1.1") (d (list (d (n "bytecount") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2.7") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lexical") (r "^2.0.0") (d #t) (k 0)))) (h "0666zds0zj67k41r9r264748z88kxi55sxza7zdd9cjnq6707d07")))

(define-public crate-data_reader-0.1.2 (c (n "data_reader") (v "0.1.2") (d (list (d (n "bytecount") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lexical") (r "^2.0.0") (d #t) (k 0)))) (h "0qz9sks05p0pfsaydf8i6b1c1xpjvqrv3190rklh5h60n3v017bn")))

(define-public crate-data_reader-0.1.3 (c (n "data_reader") (v "0.1.3") (d (list (d (n "bytecount") (r "^0.5.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lexical") (r "^2.1.0") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "0d8jx152n0bj1w9xcip7l7gz01rp7aihr2banfb2i7wi87kkv08g")))

(define-public crate-data_reader-0.2.0 (c (n "data_reader") (v "0.2.0") (d (list (d (n "bytecount") (r "^0.5.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lexical") (r "^2.1.0") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "03d53li4s83jq3nr034h55bhzm7ik69nx518dwnjz6kzhiarivvf")))

(define-public crate-data_reader-0.3.0 (c (n "data_reader") (v "0.3.0") (d (list (d (n "bytecount") (r "^0.5.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lexical") (r "^2.1.0") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "18gh6ylz6rrv4grshcqdi51a17ivsl63kpmv3840pva5cghj53lw")))

(define-public crate-data_reader-0.4.0 (c (n "data_reader") (v "0.4.0") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "fast-float") (r "^0.2") (d #t) (k 0)) (d (n "lexical") (r "^5.2.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (d #t) (k 0)) (d (n "memmap") (r "^0.3.0") (o #t) (d #t) (k 0) (p "memmap2")))) (h "0h9jdm16kr9ass58wsrjxq5cqphmk69srfb3wyxjk3pzzbssrccz") (f (quote (("mmap" "memmap"))))))

(define-public crate-data_reader-0.5.0 (c (n "data_reader") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "memmap") (r "^0.5.5") (o #t) (d #t) (k 0) (p "memmap2")))) (h "0r2pcc95ff0xl80mwwj1rp4zaacql42cpjg8hizri40g45sgwg67") (f (quote (("mmap" "memmap"))))))

