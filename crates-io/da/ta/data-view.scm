(define-module (crates-io da ta data-view) #:use-module (crates-io))

(define-public crate-data-view-0.1.0 (c (n "data-view") (v "0.1.0") (h "14m8bz7akmcp4xv6mzwc45wd27b2dsy29604kxww39y2k5ibv69f") (f (quote (("NE") ("BE")))) (y #t)))

(define-public crate-data-view-0.2.0 (c (n "data-view") (v "0.2.0") (h "1hzak49rig0c4gzx4735njbxnx23schrxc9hnai4xvglb46skkph") (f (quote (("NE") ("BE")))) (y #t)))

(define-public crate-data-view-0.2.1 (c (n "data-view") (v "0.2.1") (h "0c3s9hhjxczqaiinfxc349riadca76zva0jzbglaidzamxikancp") (f (quote (("NE") ("BE")))) (y #t)))

(define-public crate-data-view-1.0.0 (c (n "data-view") (v "1.0.0") (h "0bq6xmq6xbrskmsxhsv0aw9mk1ps70hxww2psknfm63iy0jkfm15") (f (quote (("NE") ("BE"))))))

(define-public crate-data-view-1.1.0 (c (n "data-view") (v "1.1.0") (h "0k3f0v6qqm11jpklbar4hwzv4jd9dy0b99sk06mpv1qjlrgyy4h0") (f (quote (("NE") ("BE"))))))

(define-public crate-data-view-2.0.0 (c (n "data-view") (v "2.0.0") (h "1x8qnykfvb5w4x2bajcxjdc5rjhbir4s9ndf6q8i5p6762zgsvnq") (f (quote (("NE") ("BE"))))))

(define-public crate-data-view-2.0.1 (c (n "data-view") (v "2.0.1") (h "0z8j89qm8ddmg6njmk2bw2k4b7iaz679p8884cffvhkirgw02yxh") (f (quote (("NE") ("BE"))))))

(define-public crate-data-view-3.0.0 (c (n "data-view") (v "3.0.0") (h "0dbsbhc33wmhkkfp8c3dmsjzhcp03i37in27z5lvzw592f6q2xcf") (f (quote (("nightly") ("NE" "nightly") ("BE" "nightly"))))))

(define-public crate-data-view-4.0.0 (c (n "data-view") (v "4.0.0") (h "1a5372df8k86ila7vlzqgq714d69ianli0hdbz11qjwka44n4lv0") (f (quote (("NE") ("BE"))))))

(define-public crate-data-view-4.0.1 (c (n "data-view") (v "4.0.1") (h "0hd54z566smypb51zragwpgjdmb2zfzg1jnyaabyrjmhqw5rizh2") (f (quote (("NE") ("BE"))))))

(define-public crate-data-view-4.1.0 (c (n "data-view") (v "4.1.0") (h "1ij9ab814pjf0n5kp12fb74pb4dyivk1ygh31b63r2aidj6fvnvn") (f (quote (("NE") ("BE"))))))

(define-public crate-data-view-4.1.1 (c (n "data-view") (v "4.1.1") (h "1l69fx9q1gx1asrjfsa6m7vbvw2xlx4vw0yrnz5997c7f4x8sp3h") (f (quote (("NE") ("BE"))))))

(define-public crate-data-view-4.2.0 (c (n "data-view") (v "4.2.0") (h "1yd2q7cv7mrdcq2z2k58rks0ch2kpnra32z8gmx79r18navmj7j7") (f (quote (("NE") ("BE"))))))

(define-public crate-data-view-5.0.0 (c (n "data-view") (v "5.0.0") (h "1pd098wz8szzqvs3jl95k63xpwqn6ihg401bfqlkp1s0ggnylnnm") (f (quote (("NE") ("BE"))))))

(define-public crate-data-view-5.1.0 (c (n "data-view") (v "5.1.0") (h "1p74may00kzh90llg67zpqxs1p3sh2hymxx9jjsipmildd594z90") (f (quote (("NE") ("BE"))))))

