(define-module (crates-io da ta datatier) #:use-module (crates-io))

(define-public crate-datatier-0.1.0 (c (n "datatier") (v "0.1.0") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "15vknh13w4qyqzmwddlzr8mgjsi2b51dang5xdafbrb4l2r686n9")))

