(define-module (crates-io da ta datamodel) #:use-module (crates-io))

(define-public crate-datamodel-0.1.0 (c (n "datamodel") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0n19a78py9w1w2h6x2s782qvw8r753916qnifhazikpgyn5yq7ai")))

(define-public crate-datamodel-0.3.0 (c (n "datamodel") (v "0.3.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1f2jr6mac2853q1p0fmk1d6ys94m45vgzm137jhzkh59qih7hvl3")))

(define-public crate-datamodel-0.4.0 (c (n "datamodel") (v "0.4.0") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0lp951v7lr0x81h567xb933ggg8c19hghmz09yhv0vjbj86fav61")))

(define-public crate-datamodel-0.5.0 (c (n "datamodel") (v "0.5.0") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1603j52xlwjwjj72zg782nlvvm5p4wkr3b33x0s22m7cvh5mzl40")))

(define-public crate-datamodel-0.5.1 (c (n "datamodel") (v "0.5.1") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0hgj6nid1jrqbd3mndb9hlgw9hs3lrmk24xcbnwwk8nr6xnq2n2s")))

(define-public crate-datamodel-0.5.3 (c (n "datamodel") (v "0.5.3") (d (list (d (n "indexmap") (r "^2.2.5") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1v2bgdm0zsqq34cks01kzzgp31vjyvlf5v0ypm6d2bazx57nvkal")))

