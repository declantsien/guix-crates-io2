(define-module (crates-io da ta data-test) #:use-module (crates-io))

(define-public crate-data-test-0.1.0 (c (n "data-test") (v "0.1.0") (h "1758kp0wb0k762nls2akjkap4myn27p6w7bjc0m65qmxadkmn0hl")))

(define-public crate-data-test-0.1.1 (c (n "data-test") (v "0.1.1") (h "11m4qd6qrl7r9skywxwcffzw5ahyvfan7ayx3hz3aflb9xvs30jn")))

(define-public crate-data-test-0.1.2 (c (n "data-test") (v "0.1.2") (h "16v7n2c2wyz0xfd6w6ir7c3dzmaqx994d2mz885i2q1azkf08f9d")))

