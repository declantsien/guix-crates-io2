(define-module (crates-io da ta datafusion-iceberg-sql) #:use-module (crates-io))

(define-public crate-datafusion-iceberg-sql-0.2.0 (c (n "datafusion-iceberg-sql") (v "0.2.0") (d (list (d (n "arrow-schema") (r "^50.0.0") (d #t) (k 0)) (d (n "datafusion-common") (r "^36.0.0") (d #t) (k 0)) (d (n "datafusion-expr") (r "^36.0.0") (d #t) (k 0)) (d (n "datafusion-sql") (r "^36.0.0") (d #t) (k 0)) (d (n "iceberg-rust") (r "^0.2.0") (d #t) (k 0)))) (h "02j90dzmzhgfb4wg4a00ivqcazwlhhf8nf5dag3lhgxq25sygr05")))

(define-public crate-datafusion-iceberg-sql-0.2.1 (c (n "datafusion-iceberg-sql") (v "0.2.1") (d (list (d (n "arrow-schema") (r "^50.0.0") (d #t) (k 0)) (d (n "datafusion-common") (r "^36.0.0") (d #t) (k 0)) (d (n "datafusion-expr") (r "^36.0.0") (d #t) (k 0)) (d (n "datafusion-sql") (r "^36.0.0") (d #t) (k 0)) (d (n "iceberg-rust") (r "^0.2.1") (d #t) (k 0)))) (h "07raaj9pc0vpqyj1x34kjkniqd2baajizjsva6kbb2d5pfgbxcp0")))

(define-public crate-datafusion-iceberg-sql-0.3.0 (c (n "datafusion-iceberg-sql") (v "0.3.0") (d (list (d (n "arrow-schema") (r "^50.0.0") (d #t) (k 0)) (d (n "datafusion-common") (r "^36.0.0") (d #t) (k 0)) (d (n "datafusion-expr") (r "^36.0.0") (d #t) (k 0)) (d (n "datafusion-sql") (r "^36.0.0") (d #t) (k 0)) (d (n "iceberg-rust") (r "^0.3.0") (d #t) (k 0)))) (h "130z2psiizdqc14yxm91zbvf2a20gzjffajd84cfifgi4rvjq7yz")))

