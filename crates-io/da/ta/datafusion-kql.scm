(define-module (crates-io da ta datafusion-kql) #:use-module (crates-io))

(define-public crate-datafusion-kql-0.0.1 (c (n "datafusion-kql") (v "0.0.1") (d (list (d (n "arrow-schema") (r "^49.0.0") (d #t) (k 0)) (d (n "datafusion-common") (r "^34.0.0") (d #t) (k 0)) (d (n "datafusion-expr") (r "^34.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "kqlparser") (r "^0.0.1") (d #t) (k 0)))) (h "02n1g8bs7c1zfli0hhc58ffm9hc02qg3hi2cl8xqd8ziw89l7hfn")))

