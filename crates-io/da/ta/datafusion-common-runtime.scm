(define-module (crates-io da ta datafusion-common-runtime) #:use-module (crates-io))

(define-public crate-datafusion-common-runtime-37.0.0 (c (n "datafusion-common-runtime") (v "37.0.0") (d (list (d (n "tokio") (r "^1.36") (f (quote ("macros" "rt" "sync"))) (d #t) (k 0)))) (h "0v57fg41jhkjs1cj03bary0fhaaw1pxxvcxf4mb3nhpmw4x73kx5") (r "1.72")))

(define-public crate-datafusion-common-runtime-37.1.0 (c (n "datafusion-common-runtime") (v "37.1.0") (d (list (d (n "tokio") (r "^1.36") (f (quote ("macros" "rt" "sync"))) (d #t) (k 0)))) (h "1z9xn0n37c4km2xv76w5hwviqhac6q960ki369fyicgiir6s9r53") (r "1.72")))

(define-public crate-datafusion-common-runtime-38.0.0 (c (n "datafusion-common-runtime") (v "38.0.0") (d (list (d (n "tokio") (r "^1.36") (f (quote ("macros" "rt" "sync"))) (d #t) (k 0)))) (h "1f2jrnynhx4yb94s7scsj4bcm0gvzzn240fs61m68lfbv2wdz3bf") (r "1.73")))

