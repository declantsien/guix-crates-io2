(define-module (crates-io da ta dataforge-rpc-defs) #:use-module (crates-io))

(define-public crate-dataforge-rpc-defs-0.0.1 (c (n "dataforge-rpc-defs") (v "0.0.1") (d (list (d (n "prost") (r "^0.11.8") (d #t) (k 0)) (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.4") (d #t) (k 1)))) (h "1hacj12h9y8558ps5g0qmrnnz69c5950l4aqhq134ckwc301sx59") (y #t)))

