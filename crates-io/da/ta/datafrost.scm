(define-module (crates-io da ta datafrost) #:use-module (crates-io))

(define-public crate-datafrost-0.1.0 (c (n "datafrost") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "mutability_marker") (r "^0.1.1") (k 0)) (d (n "slab") (r "^0.4.9") (k 0)) (d (n "sync_rw_cell") (r "^0.1.0") (k 0)) (d (n "task_pool") (r "^0.1.4") (k 0)) (d (n "wasm_sync") (r "^0.1.2") (k 0)))) (h "0736wv1nmyq3c21m53rjcpps5li2fs26mabp4zrjf3k12dgnzkrl")))

(define-public crate-datafrost-0.1.1 (c (n "datafrost") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "mutability_marker") (r "^0.1.1") (k 0)) (d (n "slab") (r "^0.4.9") (k 0)) (d (n "sync_rw_cell") (r "^0.1.0") (k 0)) (d (n "task_pool") (r "^0.1.4") (k 0)) (d (n "wasm_sync") (r "^0.1.2") (k 0)))) (h "0adbaijbmhvj00yaxng9lhwbxylxx9hi9dj62i5yjnpkq8fvns3d")))

(define-public crate-datafrost-0.1.2 (c (n "datafrost") (v "0.1.2") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "mutability_marker") (r "^0.1.1") (k 0)) (d (n "slab") (r "^0.4.9") (k 0)) (d (n "sync_rw_cell") (r "^0.1.0") (k 0)) (d (n "task_pool") (r "^0.1.4") (k 0)) (d (n "wasm_sync") (r "^0.1.2") (k 0)))) (h "04p4alzhb8h2vqf8422rsjpmll07jwbkq73a7sdzllrzkxdswfy9")))

(define-public crate-datafrost-0.1.3 (c (n "datafrost") (v "0.1.3") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "mutability_marker") (r "^0.1.1") (k 0)) (d (n "slab") (r "^0.4.9") (k 0)) (d (n "sync_rw_cell") (r "^0.1.0") (k 0)) (d (n "task_pool") (r "^0.1.4") (k 0)) (d (n "wasm_sync") (r "^0.1.2") (k 0)))) (h "08ksy7gp0n6pfnyl5wrgly7bi1gqs6zx91aqz2xh3fw7h7sd193a")))

(define-public crate-datafrost-0.1.4 (c (n "datafrost") (v "0.1.4") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "mutability_marker") (r "^0.1.1") (k 0)) (d (n "slab") (r "^0.4.9") (k 0)) (d (n "sync_rw_cell") (r "^0.1.0") (k 0)) (d (n "task_pool") (r "^0.1.4") (k 0)) (d (n "wasm_sync") (r "^0.1.2") (k 0)))) (h "0l74x1pchg4xdi762dl1m3yiwp93pz5c9dpwh2kq3vkfkral34ra")))

(define-public crate-datafrost-0.1.5 (c (n "datafrost") (v "0.1.5") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "mutability_marker") (r "^0.1.1") (k 0)) (d (n "slab") (r "^0.4.9") (k 0)) (d (n "sync_rw_cell") (r "^0.1.0") (k 0)) (d (n "task_pool") (r "^0.1.4") (k 0)) (d (n "wasm_sync") (r "^0.1.2") (k 0)))) (h "0dq3hc83aa4rr71x7zskxsxmy7ypqm44l4gh9k5lcc58amy30435")))

(define-public crate-datafrost-0.1.6 (c (n "datafrost") (v "0.1.6") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("std"))) (k 0)) (d (n "mutability_marker") (r "^0.1.1") (k 0)) (d (n "slab") (r "^0.4.9") (k 0)) (d (n "sync_rw_cell") (r "^0.1.0") (k 0)) (d (n "task_pool") (r "^0.1.4") (k 0)) (d (n "wasm_sync") (r "^0.1.2") (k 0)))) (h "1gvbd9xgd2wbh407d2xwp46d1ipl6bds89sfjd86092zh5zj0n3r")))

