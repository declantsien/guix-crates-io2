(define-module (crates-io da ta datastreams-rs) #:use-module (crates-io))

(define-public crate-datastreams-rs-0.1.0 (c (n "datastreams-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1sggn63ljglpr7qw5ixkfd15kra6xvhm3a465dapvl1bszhpi161")))

(define-public crate-datastreams-rs-0.2.0 (c (n "datastreams-rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1p3rlay6ymr865i6ap501cqww74wn0i5ac92lqqysfl8sgk09jxm")))

(define-public crate-datastreams-rs-0.3.0 (c (n "datastreams-rs") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1cil97z0db821iwad6hns2g5rkafbvc83lgd0gvhkywprhpmmrwn")))

