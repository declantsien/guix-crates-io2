(define-module (crates-io da ta databake) #:use-module (crates-io))

(define-public crate-databake-0.1.0 (c (n "databake") (v "0.1.0") (d (list (d (n "databake-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)))) (h "0rpwcvkgb5b458lrdxzyk878zyq3lqpjf5icwpsdqvc7y6y0mhpz") (f (quote (("derive" "databake-derive"))))))

(define-public crate-databake-0.1.1 (c (n "databake") (v "0.1.1") (d (list (d (n "databake-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)))) (h "0mnss64ic2xbanr3j4vvvbksa2vqpkkxs2qwkdc1158ixi7dqc1v") (f (quote (("derive" "databake-derive"))))))

(define-public crate-databake-0.1.2 (c (n "databake") (v "0.1.2") (d (list (d (n "databake-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)))) (h "0n2q96q3y5zx8sn6mmin3zg3jwl5vhhpba3s46x67s5xszb7fxy8") (f (quote (("derive" "databake-derive"))))))

(define-public crate-databake-0.1.3 (c (n "databake") (v "0.1.3") (d (list (d (n "databake-derive") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)))) (h "0mr8993z29f6jp86vy218dza0m05cdc46b58f1xcsmg42x3nqqnz") (s 2) (e (quote (("derive" "dep:databake-derive"))))))

(define-public crate-databake-0.1.4 (c (n "databake") (v "0.1.4") (d (list (d (n "databake-derive") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)))) (h "0lkzy1yb16ac9hrrkfc69hi3ld5kp5j3lgp82blw5v3isrqvisiz") (s 2) (e (quote (("derive" "dep:databake-derive"))))))

(define-public crate-databake-0.1.5 (c (n "databake") (v "0.1.5") (d (list (d (n "databake-derive") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)))) (h "1sa8k4vc9f7199z7r99661j37c9cvq9zma42amzrw00wwx8hhaql") (s 2) (e (quote (("derive" "dep:databake-derive"))))))

(define-public crate-databake-0.1.6 (c (n "databake") (v "0.1.6") (d (list (d (n "databake-derive") (r "^0.1.3") (o #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "fold"))) (d #t) (k 0)))) (h "0gfg93byqg7rhcafqwn57c6b5rl201b7bi0r4bxsl6ms29ing6wm") (s 2) (e (quote (("derive" "dep:databake-derive")))) (r "1.66")))

(define-public crate-databake-0.1.7 (c (n "databake") (v "0.1.7") (d (list (d (n "databake-derive") (r "^0.1.7") (o #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "0flmvn7ym0sz6mkh5mg08vcbxa6kjiknhj9bpspww54lwrr5s5w2") (s 2) (e (quote (("derive" "dep:databake-derive")))) (r "1.67")))

(define-public crate-databake-0.1.8 (c (n "databake") (v "0.1.8") (d (list (d (n "databake-derive") (r "^0.1.8") (o #t) (k 0)) (d (n "proc-macro2") (r "^1.0.61") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)))) (h "0kb0lnhka1fklrii3qaj40zcrbclfn8fyvy0r1whd3yaxkxzn13a") (s 2) (e (quote (("derive" "dep:databake-derive")))) (r "1.67")))

