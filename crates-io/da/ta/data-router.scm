(define-module (crates-io da ta data-router) #:use-module (crates-io))

(define-public crate-data-router-0.1.0 (c (n "data-router") (v "0.1.0") (d (list (d (n "counted_map") (r "^0.2.0") (d #t) (k 0)))) (h "1mrf7jmwy2j9h32mnyd185c056pqmiahlxb64ifb8xcqq2y0akaj")))

(define-public crate-data-router-0.2.0 (c (n "data-router") (v "0.2.0") (d (list (d (n "counted_map") (r "^0.3.0") (d #t) (k 0)))) (h "1nj6pb6m3q99x196qd9z6qc47856crk9msq9l2q3xclgj5f01hjm")))

(define-public crate-data-router-0.3.0 (c (n "data-router") (v "0.3.0") (d (list (d (n "counted_map") (r "^0.3.0") (d #t) (k 0)))) (h "1di4fagqrpaig9gx4ni28j4cvhlc3z1mi3lhjmdid13dac4lwcmd")))

