(define-module (crates-io da ta datafusion-extra) #:use-module (crates-io))

(define-public crate-datafusion-extra-0.0.2 (c (n "datafusion-extra") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "datafusion") (r "^34.0") (f (quote ("simd" "serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("full"))) (d #t) (k 0)))) (h "1dj3xa1s12806y972bgqwkismwca1nsrsydj0mn8lh809vqmqrnw")))

