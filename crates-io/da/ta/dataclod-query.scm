(define-module (crates-io da ta dataclod-query) #:use-module (crates-io))

(define-public crate-dataclod-query-0.0.1 (c (n "dataclod-query") (v "0.0.1") (d (list (d (n "datafusion") (r "^33.0") (f (quote ("simd"))) (d #t) (k 0)))) (h "04p55sw61bwykvsypv9b3k72krlzvgfh8wbwmp5axs8akj23rad9")))

(define-public crate-dataclod-query-0.0.2 (c (n "dataclod-query") (v "0.0.2") (d (list (d (n "datafusion") (r "^33.0") (f (quote ("simd"))) (d #t) (k 0)))) (h "1br0w6jnpg5k1rsvks19yf6cbbs90jn4yhqv5xs3s9dyy7z6i0n0")))

