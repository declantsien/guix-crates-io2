(define-module (crates-io da ta datafusion-arrow) #:use-module (crates-io))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180404 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180404") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "0zjg2fxdv1xqnfd4smjiv4gz235pwvvhma78kzqlx6zjmcsylklr") (y #t)))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180405 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180405") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "0ih05331bx9iiqnycs4n9wh965xbz28mkqac02x98xq2m56iaa6n") (y #t)))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180406 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180406") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "0qywx3v2r0frnkmz4nlh7ygmqvafrvl80k1pq0l8al563r6qnlfw") (y #t)))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180408 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180408") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "03ik4f2jddfigyvnppv513naxbdpizv5nvbyddkchx4zckfxqg64") (y #t)))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180409 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180409") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "1y2n2312mxxpqkz9rrapp22ava71dkrf5vakwbham42s1ax5ichl") (y #t)))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180409-2 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180409-2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "10a7f58lv2hgcdswjbck9g43kprcpbhg9ba931pfsp89k4kjd50b") (y #t)))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180410 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180410") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "16n2r69mmqhsd0s9m57c9m9qm3bmkk06bswlvzfb3xks1f0g7iam") (y #t)))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180410-2 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180410-2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "0dvv4xl9ji0x128kyr2pgkl0nd0yds171xa0mxfsra60jqz917rf") (y #t)))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180418 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180418") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "0n2v8a7ax82m7pdgi8ljdcd36h2rnjgqigvw1dn6jf7q0jc69icv") (y #t)))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180420 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180420") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "0g0pxsll39c1qd227zkav2mbqkszy986p4v4q71g8w8nj2xspnak") (y #t)))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180505 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180505") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "1pd8xiynj4jmlid38bi51q4461dky69sjq91jsycciq9h76bwkxk") (y #t)))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180519 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180519") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "1yq71hxz160yp34l7mkvj1rix3d9n9zgay84x65fa5xr0f9frkvh") (y #t)))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180519-2 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180519-2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "1ci7mib01hbxcniazaqbqp5fnxlk31nj9k97zjd85xri2dy0kh9d") (y #t)))

(define-public crate-datafusion-arrow-0.1.0-nightly-20180521 (c (n "datafusion-arrow") (v "0.1.0-nightly-20180521") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "027rbia6l3wkpj96w2vgcsha05rg4561frgj4a4ms08zrm18819m") (y #t)))

(define-public crate-datafusion-arrow-0.10.0-nightly-20181124 (c (n "datafusion-arrow") (v "0.10.0-nightly-20181124") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "0p00x596hj9falsg3ka0phybiwmslx78m3q0v9sx5jn5q6m1kml4") (y #t)))

(define-public crate-datafusion-arrow-0.12.0 (c (n "datafusion-arrow") (v "0.12.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "csv") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "061jpcbm6imz8l6is6kbxg86yd8h9b3r48f7iijq5f5p90v73wdl") (y #t)))

(define-public crate-datafusion-arrow-0.12.1 (c (n "datafusion-arrow") (v "0.12.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "csv") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("alloc" "rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "14cxsm2qh0nzs4psi0q2fb8h8fr9zdw1cv3kxf9rfyidy37zhgcn") (y #t)))

(define-public crate-datafusion-arrow-0.12.2 (c (n "datafusion-arrow") (v "0.12.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "csv") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("alloc" "rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "14ddklb7ykflz18nqvr2sql9g105q4hp6gg3lcz2ri4npm6ndbii") (y #t)))

(define-public crate-datafusion-arrow-0.12.3 (c (n "datafusion-arrow") (v "0.12.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "csv") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("alloc" "rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "1pjpbkga1ycgdr8hi2bgcl37s4h30lbxfghpdql5ng2x3b2abagd") (y #t)))

(define-public crate-datafusion-arrow-0.12.4 (c (n "datafusion-arrow") (v "0.12.4") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "csv") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("alloc" "rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "1ns683r1gm8q73qcrxy6bmqfsnp7g3c076p49gkd65alh27l2x0q") (y #t)))

