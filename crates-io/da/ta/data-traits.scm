(define-module (crates-io da ta data-traits) #:use-module (crates-io))

(define-public crate-data-traits-0.1.0 (c (n "data-traits") (v "0.1.0") (h "07cnxc01i2nz364bsmqcfnnzjarwdsp6if29m0yvcnjs35gbjgpa") (f (quote (("verify") ("type") ("schema") ("rest") ("reflect") ("peekable") ("iterator") ("generate") ("create") ("bytes") ("any"))))))

