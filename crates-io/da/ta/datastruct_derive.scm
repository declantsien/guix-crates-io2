(define-module (crates-io da ta datastruct_derive) #:use-module (crates-io))

(define-public crate-datastruct_derive-0.1.0 (c (n "datastruct_derive") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1wwlrvm8vjlq4v17hvjvmnkxscdhjsdhflfqqm9nx03jcp5dxkvw") (y #t)))

(define-public crate-datastruct_derive-0.1.1 (c (n "datastruct_derive") (v "0.1.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1l4b2ds8aks9lv79j5l0fr3bhyknf7lp2pagj9dw2gldcwq4nl9s")))

