(define-module (crates-io da ta dataseries) #:use-module (crates-io))

(define-public crate-dataseries-0.1.0 (c (n "dataseries") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1hakfj7ajlfckd83nb7xkk5svkymiadbccgsj40v0y2nh8qpxlwh")))

(define-public crate-dataseries-0.1.2 (c (n "dataseries") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "072cjazgxvam0yqghshw2nh70l8zxbj3dm1zkz90dlfpn2yr7hj6")))

(define-public crate-dataseries-0.1.3 (c (n "dataseries") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1my2lv8mnjk9c814s8lyr7r95wpm1g3kq0hcbmf33lwcvz7iznr4")))

(define-public crate-dataseries-0.1.4 (c (n "dataseries") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1nzaxw6fq7i9y6l5lv753kknjv5fgdv1jbp3lb7yd7z9zk9hjlgf")))

(define-public crate-dataseries-0.1.5 (c (n "dataseries") (v "0.1.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1y5di76nccqnk41m3a42abzi1adz06rnzaywy4pwqs3zfpmyqnpq")))

(define-public crate-dataseries-0.1.6 (c (n "dataseries") (v "0.1.6") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0agpiv5nfq48qpckij1x94bkmnan7i7jv4xvd1j6cfgm26444cq9")))

