(define-module (crates-io da ta datastore-mysql) #:use-module (crates-io))

(define-public crate-datastore-mysql-0.1.0 (c (n "datastore-mysql") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "datastore") (r "^0.1.4") (d #t) (k 0)) (d (n "datastore") (r "^0.1.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sqlx") (r "^0.5.13") (f (quote ("mysql" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "049raix54ygq32f3wxvbixmrwyrc08clficjmxljwsj3kbn834xd")))

(define-public crate-datastore-mysql-0.2.0 (c (n "datastore-mysql") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "datastore") (r "^0.1.5") (d #t) (k 0)) (d (n "datastore") (r "^0.1.5") (f (quote ("derive"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.2") (f (quote ("mysql" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0habad30vcv62dg864zddprc9xjsjmh18y7r5gh8b78292lkpp4l")))

