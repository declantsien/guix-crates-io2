(define-module (crates-io da ta data-plane-api) #:use-module (crates-io))

(define-public crate-data-plane-api-0.1.0 (c (n "data-plane-api") (v "0.1.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1pyzzy4an74bc24f2r0yhnnlqj1p2vbpm04wpfjvqswzanryjx3w")))

(define-public crate-data-plane-api-0.1.1 (c (n "data-plane-api") (v "0.1.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0vhzbghrq2fnsgdd953xlwn4fd2x575c5amwp3rn835hp59p9cwb")))

