(define-module (crates-io da ta data-stream) #:use-module (crates-io))

(define-public crate-data-stream-0.1.0 (c (n "data-stream") (v "0.1.0") (h "0aczm414iy8f1h2awlhp49fydqbplv9867jmhr9pp4gjy8m4as79")))

(define-public crate-data-stream-0.2.0 (c (n "data-stream") (v "0.2.0") (h "159whqr2crv35gxlil76l8410kp5b7xz2arqi3ri6gk8g2vlfns0")))

