(define-module (crates-io da ta datasize_derive) #:use-module (crates-io))

(define-public crate-datasize_derive-0.1.0 (c (n "datasize_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r3j6ak8v9fzbg84i18xm3phymnvk8lklcjlvnnrsnvmhzllb0l1")))

(define-public crate-datasize_derive-0.1.1 (c (n "datasize_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y1l28s9s57sgkwpq8hxqw53xgnzbnwphr0bfqclj202ilyalfcg")))

(define-public crate-datasize_derive-0.2.0 (c (n "datasize_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qgfy1spp8cc9dg2pzn39ax93dy6im7ahki1zpqi11qysv18wj9g")))

(define-public crate-datasize_derive-0.2.1 (c (n "datasize_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ikanag932rcgqbdrmfbghj7hqqwlrpjayh4rvzyxdchpxmvm026")))

(define-public crate-datasize_derive-0.2.2 (c (n "datasize_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bzlbjbc2d9xsc7xzhcv8rdw9q98xbgpni1w7fq0ipmi08mqbnch")))

(define-public crate-datasize_derive-0.2.3 (c (n "datasize_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0crw02zz50hgmp71kmpr7knyky22fg3xi8lp6wbj705lf3yzm0m5")))

(define-public crate-datasize_derive-0.2.4 (c (n "datasize_derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04klj5zynkihpzn58y7am3f48wknm1ygzlkljs1rai5zm6r7km7p")))

(define-public crate-datasize_derive-0.2.5 (c (n "datasize_derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0csyfxzx4jmn1ny835mx0n00vr5psw5x3pi072hnr4fyyr7yl5df") (f (quote (("detailed"))))))

(define-public crate-datasize_derive-0.2.6 (c (n "datasize_derive") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "084bxj0nbcc12j80i95ld3y4nsyr4w0yj2li00m216zavjgzkvqd") (f (quote (("detailed"))))))

(define-public crate-datasize_derive-0.2.7 (c (n "datasize_derive") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x9id1hxlgq9ygyrc17hrgb2pn22q5cz5470drip38rdcm115bkj") (f (quote (("detailed"))))))

(define-public crate-datasize_derive-0.2.8 (c (n "datasize_derive") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q6ivg5hc2gnky0gsbzd1j4x5vvqy5zpd2ldrh29xbbngaj362c2") (f (quote (("detailed")))) (y #t)))

(define-public crate-datasize_derive-0.2.9 (c (n "datasize_derive") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qvd6a3gpiby9zdrz126dcwkdyijmr21n80h19qf3djiqydbxg1f") (f (quote (("detailed"))))))

(define-public crate-datasize_derive-0.2.10 (c (n "datasize_derive") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11hx5yriqc3z85dnhkgkfhzx3xmrbd0dhrh9ynhczlfswqbdnr8h") (f (quote (("detailed"))))))

(define-public crate-datasize_derive-0.2.11 (c (n "datasize_derive") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zyarr6avmy436xrnsc5563kapnxfmj4363bi9nxsx37amiyxlzc") (f (quote (("detailed"))))))

(define-public crate-datasize_derive-0.2.12 (c (n "datasize_derive") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04j5y6cdgccn85jv0zcm5sg4sc1srkh38vysliylwbvg7kab9zhh") (f (quote (("detailed"))))))

(define-public crate-datasize_derive-0.2.13 (c (n "datasize_derive") (v "0.2.13") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s0yhkk1sm87n3257xwgc4x9ksbb32sddalh271pfxmqgarm1gaa") (f (quote (("detailed"))))))

(define-public crate-datasize_derive-0.2.14 (c (n "datasize_derive") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07m6hkikz2h1iqj88l90cdi6ymnlbm5hv84j110i8lllh7n1a14b") (f (quote (("detailed"))))))

(define-public crate-datasize_derive-0.2.15 (c (n "datasize_derive") (v "0.2.15") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mcidgxh4nfsz9dd64bvpq2xdawhsjxh8831ny2k54crb3hlwgk1") (f (quote (("detailed"))))))

