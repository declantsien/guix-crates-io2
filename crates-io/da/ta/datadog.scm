(define-module (crates-io da ta datadog) #:use-module (crates-io))

(define-public crate-datadog-0.0.0 (c (n "datadog") (v "0.0.0") (h "1vrlwl9sisbicx2pvfn5423n25kj4v2kxy2ksj230wjlxak27i7c") (y #t)))

(define-public crate-datadog-0.1.0 (c (n "datadog") (v "0.1.0") (h "10ls8dik2ndizcz2ahqhmnrrhwpifha50ylya9dwh7vxx5ydj388")))

