(define-module (crates-io da ta databend-thrift) #:use-module (crates-io))

(define-public crate-databend-thrift-0.17.0 (c (n "databend-thrift") (v "0.17.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0lxqsp9j82bw00c9d3fq7w9p6vzwynvly6q7yazcd35861mj4ggw") (f (quote (("server" "threadpool" "log") ("default" "server"))))))

