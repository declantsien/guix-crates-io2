(define-module (crates-io da ta data_export) #:use-module (crates-io))

(define-public crate-data_export-0.1.0 (c (n "data_export") (v "0.1.0") (d (list (d (n "data_export_macros") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0.63") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zxzgsi4my58zjnjjmr2la580a5r3jhliwz65a068m5p528b90ik") (f (quote (("default" "excel" "chrono" "derive") ("chrono" "rust_xlsxwriter/chrono")))) (y #t) (s 2) (e (quote (("excel" "dep:rust_xlsxwriter") ("derive" "dep:data_export_macros"))))))

