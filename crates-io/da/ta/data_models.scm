(define-module (crates-io da ta data_models) #:use-module (crates-io))

(define-public crate-data_models-0.1.0 (c (n "data_models") (v "0.1.0") (h "0yd6g1xndgix5g2z3ml2h6nqma0w1fsdz3kr8a77asmp2y8550sl")))

(define-public crate-data_models-0.1.1 (c (n "data_models") (v "0.1.1") (h "1m0a9hcfk8mzdvrmgfrnmka73vydmnr1597mzhb4h96j9wvh5wqa")))

(define-public crate-data_models-0.1.2 (c (n "data_models") (v "0.1.2") (h "1dsv6b32xgp6jlzb0np30xc13d7ybc71bbmhklw46v5lbyfayf89")))

(define-public crate-data_models-0.1.3 (c (n "data_models") (v "0.1.3") (h "0fvjdkw17m3ksw4i547gvix25kczwzmrjc8g2f858369jhczspds")))

(define-public crate-data_models-0.2.0 (c (n "data_models") (v "0.2.0") (h "1nm7pl58lww13iijpisvv3iljsac05qn2mylbvhl67idxk6gjdg2")))

(define-public crate-data_models-0.2.1 (c (n "data_models") (v "0.2.1") (h "05hzipdinixj8ybn6vjsl95rhx644ga4nwkbkdjn8knfyzr05qhh") (y #t)))

