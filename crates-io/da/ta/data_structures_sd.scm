(define-module (crates-io da ta data_structures_sd) #:use-module (crates-io))

(define-public crate-data_structures_SD-0.1.0 (c (n "data_structures_SD") (v "0.1.0") (h "0qy7a9mz4ixxvvhiw6g98hyfzfrfkkshywsxl99gqb3jcrrd714z")))

(define-public crate-data_structures_SD-0.1.1 (c (n "data_structures_SD") (v "0.1.1") (h "07w3rz2780lfcrmpirz327jii0lksysjy15lwiljmx935kafkfdz")))

