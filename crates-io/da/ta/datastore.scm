(define-module (crates-io da ta datastore) #:use-module (crates-io))

(define-public crate-datastore-0.1.0 (c (n "datastore") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)))) (h "13csj4vd90hs4q31ax3gy28787vakasp0aqhq5cj36g8rfbpq0z4")))

(define-public crate-datastore-0.1.1 (c (n "datastore") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "datastore_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1rr8rzfbwv6nrj0ip43d41sm4di6164swh4bwjvc9r83cnc9m4fr") (f (quote (("derive" "datastore_derive") ("default"))))))

(define-public crate-datastore-0.1.2 (c (n "datastore") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "datastore_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1lhs99c98abkgmpnmvi099kf5i87q5y7pbc6q8vknxp4mk3irsxr") (f (quote (("derive" "datastore_derive") ("default"))))))

(define-public crate-datastore-0.1.3 (c (n "datastore") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "datastore_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1b26nz0innzvnf6b6sn73zj25j65qy24s1f1bhnrnhx97vlnqqgx") (f (quote (("derive" "datastore_derive") ("default"))))))

(define-public crate-datastore-0.1.4 (c (n "datastore") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "datastore_derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1z1ij9ljz9x5kppbvyrxjc655aw2a6yrb5wpmzja5ik1rvqb7wrq") (f (quote (("derive" "datastore_derive") ("default"))))))

(define-public crate-datastore-0.1.5 (c (n "datastore") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "datastore_derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0w4gbv95j2jmbqbd1fb7lpcnf7jqazfdzcqrwf1gp2dmsh163h8w") (f (quote (("derive" "datastore_derive") ("default"))))))

