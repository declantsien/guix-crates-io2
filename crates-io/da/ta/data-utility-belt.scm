(define-module (crates-io da ta data-utility-belt) #:use-module (crates-io))

(define-public crate-data-utility-belt-0.1.0 (c (n "data-utility-belt") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "041clgwg874cy082ks6f7c98zh6m6ki2g1isa5fl46kz2xkdh28y")))

(define-public crate-data-utility-belt-0.1.1 (c (n "data-utility-belt") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15x0pdvlr6gha4fslvgr8h8pflpgr9llb11gwm7zb59zhv714aml")))

(define-public crate-data-utility-belt-0.1.2 (c (n "data-utility-belt") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "029zqkgh8b8qlc95bj1a1fjv90jcpqjg6rnr408nb76x5q9bm2n9")))

(define-public crate-data-utility-belt-0.1.3 (c (n "data-utility-belt") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00jpk5wgjcqrw26hs0l8dzgws8kk9jxcwdvqkf4ij7g2gidg54cq")))

(define-public crate-data-utility-belt-0.1.5 (c (n "data-utility-belt") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mb6k2p5rf2mwdwwkjk5bd6vbfs0vmvmw2agwaqmpb217aqpn09h")))

