(define-module (crates-io da ta datascience) #:use-module (crates-io))

(define-public crate-datascience-0.0.0 (c (n "datascience") (v "0.0.0") (h "08jcp46jfq3mysixrlx52n40rd5iipgv8w6lnasgjs17mjhc8c7l")))

(define-public crate-datascience-0.0.1 (c (n "datascience") (v "0.0.1") (h "0ac5hvj79gvgfwp4ii4r5l6m5my3np5m9fmsycii19jxxhkwzm4h")))

