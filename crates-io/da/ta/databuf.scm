(define-module (crates-io da ta databuf) #:use-module (crates-io))

(define-public crate-databuf-0.1.0 (c (n "databuf") (v "0.1.0") (d (list (d (n "databuf-derive") (r "^0.1") (d #t) (k 0)))) (h "0jlc3ai41j9ynai6idp220nyiks718g74dsa19zcs2vdlfbgwrx6") (f (quote (("NE") ("L2") ("BE"))))))

(define-public crate-databuf-0.1.1 (c (n "databuf") (v "0.1.1") (d (list (d (n "databuf-derive") (r "^0.1.1") (d #t) (k 0)))) (h "06zjxn385qajj2rsqggrbyqbh2xmpqfx89aaba5p9s9w3yy47hkd") (f (quote (("NE") ("L2") ("BE"))))))

(define-public crate-databuf-0.2.0 (c (n "databuf") (v "0.2.0") (d (list (d (n "databuf-derive") (r "^0.2") (d #t) (k 0)))) (h "0qfx8j61x32iihhs78baknv5m8k31x11d17nmhgf0235z0yhxvx9") (y #t)))

(define-public crate-databuf-0.2.1 (c (n "databuf") (v "0.2.1") (d (list (d (n "databuf-derive") (r "^0.2.1") (d #t) (k 0)))) (h "0sy2lph4dqpa5s9bknbydci42xcwwn1gqynag5mqh9kwwqcynv43") (y #t)))

(define-public crate-databuf-0.3.0 (c (n "databuf") (v "0.3.0") (d (list (d (n "databuf-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0dcg5mppsal54jp9qrm6wj41happ9p4m6nni3k6q4wfdlbyvc6ci")))

(define-public crate-databuf-0.3.1 (c (n "databuf") (v "0.3.1") (d (list (d (n "databuf-derive") (r "^0.3.1") (d #t) (k 0)))) (h "1mjmlxjlnrf2jv0kri8j256r0g3w7h78yr8fagnjaa1s0l9g6qqf")))

(define-public crate-databuf-0.4.0 (c (n "databuf") (v "0.4.0") (d (list (d (n "databuf-derive") (r "^0.4") (d #t) (k 0)))) (h "16ya19d2mz4bi2sl2l7slsrii2j6sajyrwxm00ffj025jlqy968d")))

(define-public crate-databuf-0.5.0 (c (n "databuf") (v "0.5.0") (d (list (d (n "databuf-derive") (r "^0.5") (d #t) (k 0)))) (h "17cb5i0kxgxbbnc954h00x1z59f5jrldhz0bmj6plcgfkgcx26ly")))

