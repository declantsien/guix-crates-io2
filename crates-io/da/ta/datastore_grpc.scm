(define-module (crates-io da ta datastore_grpc) #:use-module (crates-io))

(define-public crate-datastore_grpc-0.1.0 (c (n "datastore_grpc") (v "0.1.0") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.1") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "1y5ik375hpwrr7fyy521ksmbfx8qs5ifjgcspkpavhr0vq0pc828")))

(define-public crate-datastore_grpc-0.1.1 (c (n "datastore_grpc") (v "0.1.1") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.1") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "09qpi3vr8f8gdl45gv9yvkh20pcmnk8r70sncdf1hh5f850f7yaz")))

(define-public crate-datastore_grpc-0.4.0 (c (n "datastore_grpc") (v "0.4.0") (d (list (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.4.0") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.4.0") (d #t) (k 1)))) (h "1z0p0870pm8aklha2igh9lzl393wm3q2hd2ryppfyc1hrv3ximvn")))

(define-public crate-datastore_grpc-0.5.2 (c (n "datastore_grpc") (v "0.5.2") (d (list (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "prost-types") (r "^0.8") (d #t) (k 0)) (d (n "tonic") (r "^0.5.2") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.5.2") (d #t) (k 1)))) (h "0bs0lz95wjhjkslrwzrvh8b4jb5ijwc2d11cddw2z5qszm3jzb9z")))

