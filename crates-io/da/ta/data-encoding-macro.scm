(define-module (crates-io da ta data-encoding-macro) #:use-module (crates-io))

(define-public crate-data-encoding-macro-0.1.0 (c (n "data-encoding-macro") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "185sffr7lvz2nxpm7h3yw7mgp9g7izl0cmjx5c3y1dnspqps93ph") (f (quote (("stable" "data-encoding-macro-internal/stable" "proc-macro-hack") ("default" "stable"))))))

(define-public crate-data-encoding-macro-0.1.1 (c (n "data-encoding-macro") (v "0.1.1") (d (list (d (n "data-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1mwcm82r3y31y3g6xggf100gg4d0rhm8rg7g04c9af74p7jlzmq5") (f (quote (("stable" "data-encoding-macro-internal/stable" "proc-macro-hack") ("default" "stable"))))))

(define-public crate-data-encoding-macro-0.1.3 (c (n "data-encoding-macro") (v "0.1.3") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0bv06l4m58lfk4sqhymdlqr3h7pp6qpk6b7ps6dahv3gn2afcgsk") (f (quote (("stable" "data-encoding-macro-internal/stable" "proc-macro-hack") ("default" "stable"))))))

(define-public crate-data-encoding-macro-0.1.4 (c (n "data-encoding-macro") (v "0.1.4") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (o #t) (d #t) (k 0)))) (h "04d10i8vxp8srxiwl0fpn631xjlcbxp7s4mgr4ff7wkwprr8ags0") (f (quote (("stable" "data-encoding-macro-internal/stable" "proc-macro-hack") ("default" "stable"))))))

(define-public crate-data-encoding-macro-0.1.5 (c (n "data-encoding-macro") (v "0.1.5") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0zfy46pq8jbqdzdlsn5d52s8dy6ywmwgc8610b6f18wphp7blr4q") (f (quote (("stable" "data-encoding-macro-internal/stable" "proc-macro-hack") ("default" "stable"))))))

(define-public crate-data-encoding-macro-0.1.6 (c (n "data-encoding-macro") (v "0.1.6") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.6") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0dqbap8v537j9b5529mfkavdiad949ldxhbr6rn60r69gwvvz1wv") (f (quote (("stable" "data-encoding-macro-internal/stable" "proc-macro-hack") ("default" "stable"))))))

(define-public crate-data-encoding-macro-0.1.7 (c (n "data-encoding-macro") (v "0.1.7") (d (list (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0lrlxqpyr6ghliip3ad3ghnankas6vsgqpzmkksf81jk5gis558r") (f (quote (("stable" "data-encoding-macro-internal/stable" "proc-macro-hack") ("default" "stable"))))))

(define-public crate-data-encoding-macro-0.1.8 (c (n "data-encoding-macro") (v "0.1.8") (d (list (d (n "data-encoding") (r "^2.2") (d #t) (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0qfrqw6p7q34psj7rp4nipqfqz13ga0gi20zyyr5m3qjwpfqjr6y") (f (quote (("stable" "data-encoding-macro-internal/stable" "proc-macro-hack") ("default" "stable"))))))

(define-public crate-data-encoding-macro-0.1.9 (c (n "data-encoding-macro") (v "0.1.9") (d (list (d (n "data-encoding") (r "^2.3") (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.9") (d #t) (k 0)))) (h "0kbr1zy38mynracz4c5nnpla04y6ggji2ra8p5hisxnv1ghz6j97")))

(define-public crate-data-encoding-macro-0.1.10 (c (n "data-encoding-macro") (v "0.1.10") (d (list (d (n "data-encoding") (r "^2.3") (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.9") (d #t) (k 0)))) (h "05wvybcrzbyhzn33bbv9bhg5nhb8dyyaig11cq5nr9ib7pngx50a")))

(define-public crate-data-encoding-macro-0.1.11 (c (n "data-encoding-macro") (v "0.1.11") (d (list (d (n "data-encoding") (r "^2.3") (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.9") (d #t) (k 0)))) (h "0vr9bnb6fgph7nqhkx2m44zn8dkyb888ydaczvh4ygl2paq0m4nk")))

(define-public crate-data-encoding-macro-0.1.12 (c (n "data-encoding-macro") (v "0.1.12") (d (list (d (n "data-encoding") (r "^2.3") (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.10") (d #t) (k 0)))) (h "1jls0b9p4nsp5vcp2h53cc01m3drg8l4nh47idlzm27ys9y7p4l6")))

(define-public crate-data-encoding-macro-0.1.13 (c (n "data-encoding-macro") (v "0.1.13") (d (list (d (n "data-encoding") (r "^2.4") (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.11") (d #t) (k 0)))) (h "16dvya8kib5gazblxml2rahg98q87n0anmj9xapf2c01qqyb6169") (r "1.47")))

(define-public crate-data-encoding-macro-0.1.14 (c (n "data-encoding-macro") (v "0.1.14") (d (list (d (n "data-encoding") (r "^2.5.0") (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.12") (d #t) (k 0)))) (h "0gnkqpd3h24wy272vpdphp7z6gcbq9kyn8df5ggyyaglyl31rh10") (r "1.48")))

(define-public crate-data-encoding-macro-0.1.15 (c (n "data-encoding-macro") (v "0.1.15") (d (list (d (n "data-encoding") (r "^2.6.0") (k 0)) (d (n "data-encoding-macro-internal") (r "^0.1.13") (d #t) (k 0)))) (h "0fg6abipcn5h8qa09r5ki5dv3vhq4qaxn1ipsvb7c8k2p9n9nmgi") (r "1.48")))

