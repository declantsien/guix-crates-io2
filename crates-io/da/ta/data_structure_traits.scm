(define-module (crates-io da ta data_structure_traits) #:use-module (crates-io))

(define-public crate-data_structure_traits-0.1.0 (c (n "data_structure_traits") (v "0.1.0") (h "1zcd5ps3zfsn50idm13v7h5z3p9jkpcdpdmibf5c4ibjqbampb1h") (f (quote (("no_std"))))))

(define-public crate-data_structure_traits-0.1.1 (c (n "data_structure_traits") (v "0.1.1") (h "0mm9drf021p9r5qxvnjmkbhlpq5d9ckwhi8dark05aifqh3s5pbv") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-data_structure_traits-0.1.2 (c (n "data_structure_traits") (v "0.1.2") (h "0vpjckg25nwh1ckk45vh58kffbhlrxp3banlwws0pmd7a7xb8aci") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-data_structure_traits-0.1.3 (c (n "data_structure_traits") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "08akzp39rq4skq7p8xd4fxkfgfk728fjn1vzgxasaw0qdmb9z199") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-data_structure_traits-0.1.4 (c (n "data_structure_traits") (v "0.1.4") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "09f4p0ncja8zbgncj2r9dq06rjxikf7mvyz4hhilcqwgdf5yvky2") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-data_structure_traits-0.1.5 (c (n "data_structure_traits") (v "0.1.5") (h "01w7jrsz9130kkwvpwhpwyn89wqndhqb8qkrafb83zjb558fapkq") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-data_structure_traits-0.1.6 (c (n "data_structure_traits") (v "0.1.6") (h "0csqkaw5fp0r0pxcgrh6b5mlfbh5w805qfs222i9fkbiz8j2d316") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-data_structure_traits-0.1.7 (c (n "data_structure_traits") (v "0.1.7") (h "0bkjb9rysdmqsabagafj6llxhshbn5nd771rn711lv27gzrkzkcq") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-data_structure_traits-0.1.8 (c (n "data_structure_traits") (v "0.1.8") (d (list (d (n "hashmap_core") (r "^0.1") (d #t) (k 0)))) (h "072g49wxfmd8nbg5wm00x3wbgs4fvh1smkvjfn6v64aanb9jnacq") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-data_structure_traits-0.1.9 (c (n "data_structure_traits") (v "0.1.9") (d (list (d (n "hashmap_core") (r "^0.1") (d #t) (k 0)))) (h "0z02lp3caydhayjnrsks99rsh2qyvqx5q90vypcgz4an90ydl52f") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-data_structure_traits-0.1.10 (c (n "data_structure_traits") (v "0.1.10") (d (list (d (n "hashmap_core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1b1w43ilhmqfvh789jq0by5ym73fac9b08bz1b5as8blj3jkxh2a") (f (quote (("std") ("default" "std"))))))

(define-public crate-data_structure_traits-0.1.11 (c (n "data_structure_traits") (v "0.1.11") (d (list (d (n "hashmap_core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0p1jkw5rbhc9qi5ml0hl6f1q7ijw87djvgcipsrr7qvvm587jnwa") (f (quote (("std") ("default" "std"))))))

(define-public crate-data_structure_traits-0.1.12 (c (n "data_structure_traits") (v "0.1.12") (d (list (d (n "hashmap_core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1nydj30r24as5dkvlqsvy9j9m06l9sddh570pgd5ckjxx7dr3giv") (f (quote (("std") ("default" "std"))))))

