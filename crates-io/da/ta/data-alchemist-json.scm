(define-module (crates-io da ta data-alchemist-json) #:use-module (crates-io))

(define-public crate-data-alchemist-json-0.1.0 (c (n "data-alchemist-json") (v "0.1.0") (d (list (d (n "data-alchemist") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "065wjcmc1gzvj3hx8w7j152nwmcp3yhzb40z4jl2d1lcn41pzg33")))

(define-public crate-data-alchemist-json-1.0.0-rc.1 (c (n "data-alchemist-json") (v "1.0.0-rc.1") (d (list (d (n "data-alchemist") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0sw7wss82380brh0kjm3h4m8qwbbsvigwy9jm17z11a4vwlwk6zb")))

(define-public crate-data-alchemist-json-0.1.1 (c (n "data-alchemist-json") (v "0.1.1") (d (list (d (n "data-alchemist") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "00kx29p501xj82hi3zq0cmf7y0dhlsgjzrx49dwfvk8kf2f6143n")))

(define-public crate-data-alchemist-json-0.1.2 (c (n "data-alchemist-json") (v "0.1.2") (d (list (d (n "data-alchemist") (r "^0.1.2") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1imdbwfc904yarlwkdmkvqx9fw1maa5xsgii1s0ycq4zm1hxmphc")))

