(define-module (crates-io da ta datafusion-physical-expr-common) #:use-module (crates-io))

(define-public crate-datafusion-physical-expr-common-38.0.0 (c (n "datafusion-physical-expr-common") (v "38.0.0") (d (list (d (n "arrow") (r "^51.0.0") (f (quote ("prettyprint"))) (d #t) (k 0)) (d (n "datafusion-common") (r "^38.0.0") (d #t) (k 0)) (d (n "datafusion-expr") (r "^38.0.0") (d #t) (k 0)))) (h "1gp4ziy48dky3va2mdmz9p2rsarry8ijqgvv358c31054g1p4m4d") (r "1.73")))

