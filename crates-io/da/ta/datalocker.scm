(define-module (crates-io da ta datalocker) #:use-module (crates-io))

(define-public crate-datalocker-0.2.0 (c (n "datalocker") (v "0.2.0") (d (list (d (n "mysql") (r "^24.0.0") (d #t) (k 0)))) (h "1zkby7f7cdlnngff54abjrdl8anbx0d5yiil7im562ygzzgijdpl")))

(define-public crate-datalocker-0.2.1 (c (n "datalocker") (v "0.2.1") (d (list (d (n "mysql") (r "^24.0.0") (d #t) (k 0)))) (h "16zrcffv8bnbqc1hjwpw7rxbkv92mkvdg94nhbqpk5dcwi7r82wi")))

(define-public crate-datalocker-0.2.2 (c (n "datalocker") (v "0.2.2") (d (list (d (n "mysql") (r "^24.0.0") (d #t) (k 0)))) (h "08dhlqnihpwm3011ahbak87yvay6cpvj0grqwwgkr98bb1kzinl5")))

(define-public crate-datalocker-0.2.3 (c (n "datalocker") (v "0.2.3") (d (list (d (n "mysql") (r "^24.0.0") (d #t) (k 0)))) (h "0d8hh575abcgx79k7igh3l327shxs50m72wdmq9r5qigbpgaqyw6")))

(define-public crate-datalocker-0.3.0 (c (n "datalocker") (v "0.3.0") (d (list (d (n "mysql") (r "^24.0.0") (d #t) (k 0)))) (h "1q17apksq0if2clvm9s23x505i9c498z49w7dcq41vbza187g458")))

(define-public crate-datalocker-0.3.1 (c (n "datalocker") (v "0.3.1") (d (list (d (n "mysql") (r "^24.0.0") (d #t) (k 0)))) (h "0b4xlm9wi3mnapm0k97968y3qifnda41kf35wxmsq405phmqlh6f")))

