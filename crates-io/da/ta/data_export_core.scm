(define-module (crates-io da ta data_export_core) #:use-module (crates-io))

(define-public crate-data_export_core-0.1.0 (c (n "data_export_core") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "013c3k4hxvshhssybw2f80ar1c7kikvr1kjjg731mf1f4ci835nv") (y #t) (s 2) (e (quote (("chrono" "dep:chrono"))))))

