(define-module (crates-io da ta dataset) #:use-module (crates-io))

(define-public crate-dataset-0.2.1 (c (n "dataset") (v "0.2.1") (d (list (d (n "avro-rs") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "thrift") (r "^0.12") (d #t) (k 0)) (d (n "try_from") (r "^0.3") (d #t) (k 0)))) (h "1hl3qj46454gdmml4066x64724vm8r4nrakd122wlsvj7pyq4zf2") (y #t)))

(define-public crate-dataset-0.2.2 (c (n "dataset") (v "0.2.2") (d (list (d (n "avro-rs") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "thrift") (r "^0.12") (d #t) (k 0)) (d (n "try_from") (r "^0.3") (d #t) (k 0)))) (h "14drxamzx9yxszwf0z2ra5a1jvrcd45hbqkjinr34jjqfgy54kds") (y #t)))

