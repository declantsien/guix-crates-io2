(define-module (crates-io da ta datastore_derive) #:use-module (crates-io))

(define-public crate-datastore_derive-0.1.0 (c (n "datastore_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ba4qjdnwijiwi2ilps7pfrxlw6b3f1cnkcccapa7p9005i31cwp")))

(define-public crate-datastore_derive-0.1.1 (c (n "datastore_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0in1nlzhkn36addasyhrinqs8nq9gicqrdkxl30mnrzbf7kjxliw")))

(define-public crate-datastore_derive-0.1.2 (c (n "datastore_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02g1g6l816vqsvrg66ifjn1aqj2wjg5i6gn9s98l0w9napq1q8qj")))

