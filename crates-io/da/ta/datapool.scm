(define-module (crates-io da ta datapool) #:use-module (crates-io))

(define-public crate-datapool-0.1.3 (c (n "datapool") (v "0.1.3") (d (list (d (n "byteorder") (r "1.4.*") (d #t) (k 0)) (d (n "clap") (r "3.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)) (d (n "rand_chacha") (r "0.3.*") (d #t) (k 0)) (d (n "rand_distr") (r "0.4.*") (d #t) (k 0)))) (h "07620sbag4cjnra501wq0scicy1qb7mmakxg8jmva5dgm6b81c53")))

(define-public crate-datapool-0.1.4 (c (n "datapool") (v "0.1.4") (d (list (d (n "byteorder") (r "1.4.*") (d #t) (k 0)) (d (n "clap") (r "3.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)) (d (n "rand_chacha") (r "0.3.*") (d #t) (k 0)) (d (n "rand_distr") (r "0.4.*") (d #t) (k 0)))) (h "0b0499w66srn0vy59rk9p5i5pmj0vgg9sjwbw5riswg1y3ai7mll")))

