(define-module (crates-io da ta data-encoding) #:use-module (crates-io))

(define-public crate-data-encoding-1.0.0 (c (n "data-encoding") (v "1.0.0") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 2)))) (h "1cw5c8fk08ssw5n3f15sj58fh9g28v75sl6k65348ifr7cnpqcwg")))

(define-public crate-data-encoding-1.1.0 (c (n "data-encoding") (v "1.1.0") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 2)))) (h "1a64hfl6a866mqfilgjfbzxgizzvrrslsk6sm9f5m88vdyc03cfq")))

(define-public crate-data-encoding-1.1.1 (c (n "data-encoding") (v "1.1.1") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 2)))) (h "1gaxv8vnchw4mcssix9mha5w31134y9m3vkkikvp1dmnryhb8xi2")))

(define-public crate-data-encoding-1.1.2 (c (n "data-encoding") (v "1.1.2") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 2)))) (h "08v7nbnvhks153afa5s4zx6xwc1yjmb5fvsk48rfn1hripb06gzi")))

(define-public crate-data-encoding-1.2.0 (c (n "data-encoding") (v "1.2.0") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 2)))) (h "12b7lp2lhm45q4vggxlk11d4jw7qnwprg667kvh762yy16zxsryq")))

(define-public crate-data-encoding-2.0.0-rc.1 (c (n "data-encoding") (v "2.0.0-rc.1") (h "0as025jl7cd1qsyvbbs5i42j4vgpjcsvv5d2zgi5hr5bf2r6i1y4")))

(define-public crate-data-encoding-2.0.0-rc.2 (c (n "data-encoding") (v "2.0.0-rc.2") (h "19rz1h5kndjw6c92wkdhzm2d4v7jy7bb6pk35na68n11d4726njg")))

(define-public crate-data-encoding-2.0.0 (c (n "data-encoding") (v "2.0.0") (h "1y26b2l36zrysnw4fkfxmmy53dksdq3pi2dr978fklm6ryxbfsyj")))

(define-public crate-data-encoding-2.1.0 (c (n "data-encoding") (v "2.1.0") (h "03zq9akwlpip9kd65isb66vjwkca9ggjn3vprlqkjw89z28jb789")))

(define-public crate-data-encoding-2.1.1 (c (n "data-encoding") (v "2.1.1") (h "0pk5k77cmndpd2wpscmxjrf4fj4svnmj5vdjp1zxkw2blxqhbpv7")))

(define-public crate-data-encoding-2.1.2 (c (n "data-encoding") (v "2.1.2") (h "15xd6afhsjl08285piwczrafmckpp8i29padj8v12xhahshprx7l")))

(define-public crate-data-encoding-2.2.0 (c (n "data-encoding") (v "2.2.0") (h "1257yl80api5rdxq2zdrrl7kcp26brpmj5af4ckkd6x1b1hk9h0i") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-data-encoding-2.2.1 (c (n "data-encoding") (v "2.2.1") (h "0sdx3pagijwd9dqgxnrn2g88qqaj475bf71bi9yxvs7s9p019akj") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-data-encoding-2.3.0 (c (n "data-encoding") (v "2.3.0") (h "1mp89v55gd2czpzqzw010y4hg2cprpyywf6y04xb5qsy9v9f5l6l") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-data-encoding-2.3.1 (c (n "data-encoding") (v "2.3.1") (h "027rcrwdschrkdr2n9d24gnh03vl41qmvhjqn9vn6z1njy2n0flr") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-data-encoding-2.3.2 (c (n "data-encoding") (v "2.3.2") (h "0mvd8bjq5mq50fcf931cff57vwmbsvs1kpxynkzrshli98y3kqiy") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-data-encoding-2.3.3 (c (n "data-encoding") (v "2.3.3") (h "1yq8jnivxsjzl3mjbjdjg5kfvd17wawbmg1jvsfw6cqmn1n6dn13") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.46")))

(define-public crate-data-encoding-2.4.0 (c (n "data-encoding") (v "2.4.0") (h "023k3dk8422jgbj7k72g63x51h1mhv91dhw1j4h205vzh6fnrrn2") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.47")))

(define-public crate-data-encoding-2.5.0 (c (n "data-encoding") (v "2.5.0") (h "1rcbnwfmfxhlshzbn3r7srm3azqha3mn33yxyqxkzz2wpqcjm5ky") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.48")))

(define-public crate-data-encoding-2.6.0 (c (n "data-encoding") (v "2.6.0") (h "1qnn68n4vragxaxlkqcb1r28d3hhj43wch67lm4rpxlw89wnjmp8") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.48")))

