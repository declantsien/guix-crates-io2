(define-module (crates-io da ta dataview) #:use-module (crates-io))

(define-public crate-dataview-0.1.0 (c (n "dataview") (v "0.1.0") (d (list (d (n "derive_pod") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1492qsmx050zjnrx34nvq6ig6xzg1np3gp54m5ap4af4w59n1c6i") (f (quote (("nightly") ("default" "derive_pod"))))))

(define-public crate-dataview-0.1.1 (c (n "dataview") (v "0.1.1") (d (list (d (n "derive_pod") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1jrfi2n4qyyvkzx3mg1whhdpbsj5r43517c2gx8dxvw4dgx2jq6n") (f (quote (("nightly") ("default" "derive_pod"))))))

(define-public crate-dataview-0.1.2 (c (n "dataview") (v "0.1.2") (d (list (d (n "derive_pod") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1mdl84lvgkrqhvl27rfa2p1jhsbs9cbxl41i9zy4vzyhrai05a27") (f (quote (("default" "derive_pod"))))))

(define-public crate-dataview-1.0.0 (c (n "dataview") (v "1.0.0") (d (list (d (n "derive_pod") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "00dhxxrqi71cw16zkqzhggbv1jfinzzwkcljl2nf65gikhrpq80m") (f (quote (("int2ptr") ("default" "derive_pod"))))))

(define-public crate-dataview-1.0.1 (c (n "dataview") (v "1.0.1") (d (list (d (n "derive_pod") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1y8qh80mclz29g8dlvn040xgm16b3sjwakps7lx8rmqrkqr3mssh") (f (quote (("int2ptr") ("default" "derive_pod"))))))

