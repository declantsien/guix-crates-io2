(define-module (crates-io da ta datafu) #:use-module (crates-io))

(define-public crate-datafu-0.0.1 (c (n "datafu") (v "0.0.1") (h "0xxw4vkks2wchv1jy7s980zwb5wjy4kizyd29v29i5l8w0mxd6sw")))

(define-public crate-datafu-0.0.2 (c (n "datafu") (v "0.0.2") (h "0dgzzckdj69rmnm950fmxqqcqwq6jn60ch4wcnc2aq5hpidh6zhw")))

(define-public crate-datafu-0.0.3 (c (n "datafu") (v "0.0.3") (d (list (d (n "boolinator") (r "^2.4.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0rqsj9cg2v97gn5pziyxpbdlpzi38wniy6fk6b3plklxjxbc62lp") (f (quote (("stable") ("default"))))))

(define-public crate-datafu-0.0.4 (c (n "datafu") (v "0.0.4") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0qm0ckg77hwfiqnp3a2vfbwm8xr9fvg5iz16qxrkrkhbdjam2js1") (f (quote (("stable") ("default"))))))

(define-public crate-datafu-0.0.5 (c (n "datafu") (v "0.0.5") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14l4rkg35p5qkcm53hdnq2ys92qw8fyizh7ddriwhddvlbsnzh1p") (f (quote (("stable") ("default"))))))

(define-public crate-datafu-0.0.6 (c (n "datafu") (v "0.0.6") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "totally-safe-transmute") (r "^0.0.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0mrzf97yrz1cs9qij0h4aw80xxq95xyhhhamh0id22hq7232xvrv") (f (quote (("stable") ("default"))))))

(define-public crate-datafu-0.0.7 (c (n "datafu") (v "0.0.7") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "totally-safe-transmute") (r "^0.0.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0y1vnfmlas8jv19jbrd2pdkxr3a36l36xpb80i75m6sw5k9syfmq") (f (quote (("stable") ("default"))))))

(define-public crate-datafu-0.1.0-alpha.1 (c (n "datafu") (v "0.1.0-alpha.1") (d (list (d (n "erased-serde") (r "^0.3.21") (d #t) (k 0)) (d (n "impl_trait") (r "^0.1.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)) (d (n "charx") (r "^1.0.0") (d #t) (k 2)) (d (n "postcard") (r "^1.0.2") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "0grhnwk0bi3yspkaazqhx1nd9d5ic0pdyf9chyjj24i9idyl59mv") (f (quote (("stable") ("default" "stable"))))))

(define-public crate-datafu-0.1.0-alpha.2 (c (n "datafu") (v "0.1.0-alpha.2") (d (list (d (n "erased-serde") (r "^0.3.21") (d #t) (k 0)) (d (n "impl_trait") (r "^0.1.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)) (d (n "charx") (r "^1.0.0") (d #t) (k 2)) (d (n "postcard") (r "^1.0.2") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "1vj1klmzsap14ap5bf3sfv028axr450ad15xhgw0mg81f3m6w664") (f (quote (("stable") ("default" "stable"))))))

