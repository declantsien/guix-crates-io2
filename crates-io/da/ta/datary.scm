(define-module (crates-io da ta datary) #:use-module (crates-io))

(define-public crate-datary-0.1.0 (c (n "datary") (v "0.1.0") (d (list (d (n "ouroboros") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1qlicadrnvypk451j1z2n9sk065rhsm4ndi41hsdjwhb035x7wr8") (f (quote (("optimized" "ouroboros") ("default" "optimized"))))))

