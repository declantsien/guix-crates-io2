(define-module (crates-io da ta data-pager) #:use-module (crates-io))

(define-public crate-data-pager-0.1.0 (c (n "data-pager") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (f (quote ("rust_1_61"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)))) (h "1hvg6b27jfgnr37ij0vhknv29qbbl8pbsm39js0j7w9a1i99wfb2")))

(define-public crate-data-pager-0.1.1 (c (n "data-pager") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (f (quote ("rust_1_61"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)))) (h "1jjv6y7zvl301n4wmax2m5z0kf5f79cfiap50brmwfws834dnmax")))

(define-public crate-data-pager-0.2.0 (c (n "data-pager") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (f (quote ("rust_1_61"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)))) (h "1f0qmmspv1wshgz6lm3gpbc3qdmjd4llkws9gm22ifcysil94gv2")))

(define-public crate-data-pager-0.2.1 (c (n "data-pager") (v "0.2.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (f (quote ("rust_1_61"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)))) (h "199zsdwaq5apzzv9lfwpiqjd53f7v9r50aihabbij6hb2npkpfzy")))

