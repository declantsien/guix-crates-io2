(define-module (crates-io da ta data-encoding-bin) #:use-module (crates-io))

(define-public crate-data-encoding-bin-0.1.0 (c (n "data-encoding-bin") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "0xm1xr2bzbzdpinkhq1jrbl4nmc0rnbcsrwf6f7fn3f7zjl9kj76")))

(define-public crate-data-encoding-bin-0.1.1 (c (n "data-encoding-bin") (v "0.1.1") (d (list (d (n "data-encoding") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "1r0kkp79zcqra3czjclc1jp1ya96vhv2ra55xgrj2y650jaih0db")))

(define-public crate-data-encoding-bin-0.2.0 (c (n "data-encoding-bin") (v "0.2.0") (d (list (d (n "data-encoding") (r "^2.0.0-rc.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "0g6a2rqf5nkc2khr558w1adpi0m6n4yzncbig9bkiwp1a2mar5j8")))

(define-public crate-data-encoding-bin-0.2.1 (c (n "data-encoding-bin") (v "0.2.1") (d (list (d (n "data-encoding") (r "^2.0.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 0)))) (h "0m5wvip416qislh84gay91vxj2rxjwpbrmlabxwrw84nn963fqc2")))

(define-public crate-data-encoding-bin-0.2.2 (c (n "data-encoding-bin") (v "0.2.2") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 0)))) (h "0wb1gs7pfgn6b22bxsd30v5fhgf8lr0i41lfw0jlhfac3vpj46z2")))

(define-public crate-data-encoding-bin-0.2.3 (c (n "data-encoding-bin") (v "0.2.3") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0gnhzkz0b9ih5qi26wg2y1lyx6ah5b3y3km331m7yq8q6v84hcj0")))

(define-public crate-data-encoding-bin-0.3.0 (c (n "data-encoding-bin") (v "0.3.0") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0g1cnryl06038rm3kzpvviq5g4cc8vchrcyqw6dvh574qv1d5xcd")))

(define-public crate-data-encoding-bin-0.3.1 (c (n "data-encoding-bin") (v "0.3.1") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0salvbvdrx2gvf54jph861x0v1kpqgz84wwps4iaxhizwz8zz71m")))

(define-public crate-data-encoding-bin-0.3.2 (c (n "data-encoding-bin") (v "0.3.2") (d (list (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0k1rba4vz2y4416qsr6539dw18k0mc3zgsxplrjm5rdvpn5zw3xf")))

(define-public crate-data-encoding-bin-0.3.3 (c (n "data-encoding-bin") (v "0.3.3") (d (list (d (n "data-encoding") (r "^2.5.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "14ic3nbhi6vs1z2pm2z1py448i5xvbw1393sg9m7yr7yzllic718")))

(define-public crate-data-encoding-bin-0.3.4 (c (n "data-encoding-bin") (v "0.3.4") (d (list (d (n "data-encoding") (r "^2.6.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "13y75z155q67mw9p6vh29yjd9af586sphr2crfyq753vma7bqpl2")))

