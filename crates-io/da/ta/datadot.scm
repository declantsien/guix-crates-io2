(define-module (crates-io da ta datadot) #:use-module (crates-io))

(define-public crate-datadot-0.0.1 (c (n "datadot") (v "0.0.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dynamic_reload") (r "^0.3.0") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "socket2") (r "^0.3.8") (f (quote ("reuseport"))) (d #t) (k 0)))) (h "1ajqi4a64bya9z0arbwvyfprybs00v3blm67plf84z3fh7kz3944") (f (quote (("helper"))))))

