(define-module (crates-io da ta data-url-encode-macro-impl) #:use-module (crates-io))

(define-public crate-data-url-encode-macro-impl-1.0.0 (c (n "data-url-encode-macro-impl") (v "1.0.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "0ha2vdf92sd4hsy8zp31lva0qyd05xjzrm832bbxz1n6hs6ch026")))

(define-public crate-data-url-encode-macro-impl-1.0.1 (c (n "data-url-encode-macro-impl") (v "1.0.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "1mzxbn8llw8rzijf68hccp5ggfa8zfvk2kl591cmhr89xgv0xlf8")))

