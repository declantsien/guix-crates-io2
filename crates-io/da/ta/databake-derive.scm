(define-module (crates-io da ta databake-derive) #:use-module (crates-io))

(define-public crate-databake-derive-0.1.0 (c (n "databake-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0n86d62qdn7my058lvmbrff5xfzqlvhdfyi21vrpawmkm9klpsnw")))

(define-public crate-databake-derive-0.1.1 (c (n "databake-derive") (v "0.1.1") (d (list (d (n "databake") (r "^0.1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0bj3yr12xqkz9apgn337qz5sbmvx2yqp6x6ra824rj601w37lp4h")))

(define-public crate-databake-derive-0.1.3 (c (n "databake-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "1p473v2z1wgy5w96cvg0vp02dx060yqzdaggy0gax2c48qyaaldy")))

(define-public crate-databake-derive-0.1.4 (c (n "databake-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "14f0c3iblphxlgldbzgj2c8w0vvnxs2yzz00zv2n7036jqlhgcyq")))

(define-public crate-databake-derive-0.1.5 (c (n "databake-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0paxmjmlgc19650nrxfnnp57bibsfx7wnzghw3v27kksv61qmgzx")))

(define-public crate-databake-derive-0.1.6 (c (n "databake-derive") (v "0.1.6") (d (list (d (n "databake") (r "^0.1.3") (f (quote ("derive"))) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1yq8w4k5j8jbn6k9cvnr0f35x5dvhw3v9lfki41azwamwbgr81jz") (r "1.66")))

(define-public crate-databake-derive-0.1.7 (c (n "databake-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0hqsjizibp0bb5m4kiqk9g2gixywqlxn513w5a366dpjv20z4yip") (r "1.67")))

(define-public crate-databake-derive-0.1.8 (c (n "databake-derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.61") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.21") (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0yymbr1z93k7lg0pl5mw9mjhw8fpsfykg7bmkvmir9h1wmfjfy20") (r "1.67")))

