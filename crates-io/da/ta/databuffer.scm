(define-module (crates-io da ta databuffer) #:use-module (crates-io))

(define-public crate-databuffer-1.0.0 (c (n "databuffer") (v "1.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "17hir64qfr4yr2w3dgxkxplw5kfpnp1l376jzhdlw7j0xval80m5")))

(define-public crate-databuffer-1.0.1 (c (n "databuffer") (v "1.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0md9d4xma98mk1b3svxaj9b8xv8i4rcwi9gliyl1ili55kyb12xs")))

(define-public crate-databuffer-1.1.0 (c (n "databuffer") (v "1.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0c7ax66cwkv63i6irhd2dx0yzyj0ixmfcxvg2c3y6baq12vayms8")))

(define-public crate-databuffer-1.1.1 (c (n "databuffer") (v "1.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "192ilfpylb0nlirw95lmmjs10jdzw1c97yhpmv23bvh1njkqzpci")))

(define-public crate-databuffer-1.1.2 (c (n "databuffer") (v "1.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0a13f4xq2lfsa93hqxdfiq5pjw68nmdasyn9nn51z5dw0f72cx9a")))

(define-public crate-databuffer-1.1.3 (c (n "databuffer") (v "1.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1rqla2xqjiw42q2jxb0ywwxg04b7fjjv81j4iq6vjg2vgyksypmz")))

(define-public crate-databuffer-1.1.4 (c (n "databuffer") (v "1.1.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1r4yy2sd61cvgc22lk5rdmcx8b1b1m1g7as67vjwzcb7z1vpg05y")))

