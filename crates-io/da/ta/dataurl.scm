(define-module (crates-io da ta dataurl) #:use-module (crates-io))

(define-public crate-dataurl-0.0.1 (c (n "dataurl") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.29") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0p8vyq4f85wsxzjmp6avnsm2hl0gm8fyb6v1jlzivhh0m1ykyfza")))

(define-public crate-dataurl-0.1.0 (c (n "dataurl") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.29") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "161acdfm8nxzm30nk7c6js4hkn2cf3p65v7hrdayagcsx2f2n3rk")))

(define-public crate-dataurl-0.1.1 (c (n "dataurl") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.29") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1fcxhxwx2xiq4nnwg5jybnzc52y4dpvf2wfxdyg8vl1p3d5yh8f4")))

(define-public crate-dataurl-0.1.2 (c (n "dataurl") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.29") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "05cg9yhvixlcyq9gfmg8zdz395k18mda08x7ihqkscjpv17g388p")))

