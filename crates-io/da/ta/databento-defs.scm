(define-module (crates-io da ta databento-defs) #:use-module (crates-io))

(define-public crate-databento-defs-0.3.0 (c (n "databento-defs") (v "0.3.0") (d (list (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16hrn9gc81d7ggyhqq0yk1wrbdlqv18wnqark648md7np8lwk7hj") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-databento-defs-0.3.1 (c (n "databento-defs") (v "0.3.1") (d (list (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06gl8ynqdrkqvbc4sm7h31m8n3wya15wb3q3dnp6akcy9s1qam7j") (f (quote (("trivial_copy") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-databento-defs-0.4.0 (c (n "databento-defs") (v "0.4.0") (d (list (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ssp9k8hpynvl4l8gy0rja1nywd8vxwq1aj1j2gh9a37z67897k1") (f (quote (("trivial_copy") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-databento-defs-0.4.1 (c (n "databento-defs") (v "0.4.1") (d (list (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cqhk5s8f46ayx25pvlyfnwia5wjdxf46m74qhjgymcc2k4vzwgv") (f (quote (("trivial_copy") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-databento-defs-0.4.2 (c (n "databento-defs") (v "0.4.2") (d (list (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04fy8i9hqb3qaqmkfz9ybjwslvhawhwkhimm74l1bl0ikg1k9m96") (f (quote (("trivial_copy") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

