(define-module (crates-io da ta datafusion-util) #:use-module (crates-io))

(define-public crate-datafusion-util-0.0.1 (c (n "datafusion-util") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "datafusion") (r "^33.0") (f (quote ("simd"))) (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (d #t) (k 0)))) (h "107kxa6rpdriqgpmcg6klcj7w0wikfjwkn33b4vn4wrsz172dzij")))

(define-public crate-datafusion-util-0.0.2 (c (n "datafusion-util") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "datafusion") (r "^33.0") (f (quote ("simd"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (d #t) (k 0)))) (h "1fhdif6yk6cz5y1ik3wq6h4wkvw695jakmgrwxnihsz67gsmlgym")))

