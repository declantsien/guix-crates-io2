(define-module (crates-io da ta datafusion-federation) #:use-module (crates-io))

(define-public crate-datafusion-federation-0.1.0 (c (n "datafusion-federation") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "datafusion") (r "^34.0.0") (d #t) (k 0)))) (h "1270csc4pn8vxaml2f28id3p8yfc6m28sbg1zm4400ld7l7zbc32") (y #t)))

(define-public crate-datafusion-federation-0.1.1 (c (n "datafusion-federation") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "datafusion") (r "^34.0.0") (d #t) (k 0)))) (h "0d02pa0b61mr4amp1ggl4gc77ny1hibjcxsp17fhsf9j4b3ssxjl") (y #t)))

(define-public crate-datafusion-federation-0.1.2 (c (n "datafusion-federation") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "datafusion") (r "^34.0.0") (d #t) (k 0)))) (h "1qdvgxh9yryvir4r458ads6v9aq51ads0qnnvk7gqzfdhjprw6w1") (y #t)))

(define-public crate-datafusion-federation-0.1.3 (c (n "datafusion-federation") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "datafusion") (r "^34.0.0") (d #t) (k 0)))) (h "19xbv5mwain607z3azhg978rrddn1x25868cpbg62nanrwmg3cwa") (y #t)))

(define-public crate-datafusion-federation-0.1.4 (c (n "datafusion-federation") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "datafusion") (r "^34.0.0") (d #t) (k 0)))) (h "10h16nr49zg6lzigsbix6kabz0xx7dhk9gqgncz0yj2vfy7ndmz7")))

