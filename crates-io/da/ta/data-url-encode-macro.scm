(define-module (crates-io da ta data-url-encode-macro) #:use-module (crates-io))

(define-public crate-data-url-encode-macro-1.0.0 (c (n "data-url-encode-macro") (v "1.0.0") (d (list (d (n "data-url-encode-macro-impl") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "0v9kddd9z67qm43vvpm2pf540xhh1bb0h3xddq0kvk11709i35vy")))

(define-public crate-data-url-encode-macro-1.0.1 (c (n "data-url-encode-macro") (v "1.0.1") (d (list (d (n "data-url-encode-macro-impl") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "1cfzx4d31dmlmxvqdwqavv6v51amv8d124abp3m21zh8crn3w2qf")))

