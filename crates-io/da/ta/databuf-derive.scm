(define-module (crates-io da ta databuf-derive) #:use-module (crates-io))

(define-public crate-databuf-derive-0.1.0 (c (n "databuf-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0rg98cwaj2bx3pszlnyykj49c1cqmgnzi9y3isbks359mdq7cdh7")))

(define-public crate-databuf-derive-0.1.1 (c (n "databuf-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03bjmf6lph0c16xqkqh3qgff59wvg6njb43nhqgqaab424fg52rz")))

(define-public crate-databuf-derive-0.2.0 (c (n "databuf-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cknppprx8sjwp8ybg4x6jd2k9cpy50javvf81bxp6ld2zv7wpyv")))

(define-public crate-databuf-derive-0.2.1 (c (n "databuf-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lkmrrz7yc2yc6irgkrkz74hvnigqllqvl2hy5a9l4ajyw35h0vz")))

(define-public crate-databuf-derive-0.3.0 (c (n "databuf-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1w398spf6hl8csljvkwfzs1v9zi0gp5y6fapm9niyvfzrc3zib21")))

(define-public crate-databuf-derive-0.3.1 (c (n "databuf-derive") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18sbv2hl5ql70nwfqm4fnlmm6b0ngnw4z0igssi8g571fs8n96lk")))

(define-public crate-databuf-derive-0.4.0 (c (n "databuf-derive") (v "0.4.0") (d (list (d (n "databuf_derive_impl") (r "^0.1") (d #t) (k 0)))) (h "0f1k88jikifh3nrp4zg1a3943d7fz51brk52xwn7a5r2qa4gcy17")))

(define-public crate-databuf-derive-0.5.0 (c (n "databuf-derive") (v "0.5.0") (d (list (d (n "databuf_derive_impl") (r "^0.2") (d #t) (k 0)))) (h "1snh4nzlkzjhg9mhfphilrr4f0dkypx9jv524910id7wr2ghq104")))

