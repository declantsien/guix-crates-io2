(define-module (crates-io da ta datamorph) #:use-module (crates-io))

(define-public crate-datamorph-1.0.0 (c (n "datamorph") (v "1.0.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.12.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.1") (d #t) (k 0)))) (h "161jrashq9wnjjwj41z2y4a8ibny8lxqmil41x5masfzghp4vynl")))

(define-public crate-datamorph-1.0.1 (c (n "datamorph") (v "1.0.1") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.12.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.1") (d #t) (k 0)))) (h "1sw6zyfyn9lfdjxcvhs66sjv3ycc77521xsan3a20w4zk4px2j9d")))

