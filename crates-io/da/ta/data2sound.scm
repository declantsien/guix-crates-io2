(define-module (crates-io da ta data2sound) #:use-module (crates-io))

(define-public crate-data2sound-0.1.0 (c (n "data2sound") (v "0.1.0") (d (list (d (n "file-utils") (r "=0.1.5") (d #t) (k 0)) (d (n "hound") (r "=3.5.0") (d #t) (k 0)))) (h "069s9d3gsk52b8cma6lk2xl6qmwjk0ryxjhk13qsjvcby0qh88hn") (y #t) (r "1.56.1")))

(define-public crate-data2sound-0.1.1 (c (n "data2sound") (v "0.1.1") (d (list (d (n "file-utils") (r "=0.1.5") (d #t) (k 0)) (d (n "hound") (r "=3.5.0") (d #t) (k 0)))) (h "1xlr3yq7yircy3f2ycyr6f3n332pwx7v4158dqw71h02a52mm2y0") (y #t) (r "1.56.1")))

(define-public crate-data2sound-0.1.2 (c (n "data2sound") (v "0.1.2") (d (list (d (n "file-utils") (r "=0.1.5") (d #t) (k 0)) (d (n "hound") (r "=3.5.0") (d #t) (k 0)))) (h "0fcihf27hid1wfv34gv65mmkd0mg46fz0bs8j93ayds9l3qa2dg5") (y #t) (r "1.56.1")))

(define-public crate-data2sound-0.1.3 (c (n "data2sound") (v "0.1.3") (d (list (d (n "file-utils") (r "=0.1.5") (d #t) (k 0)) (d (n "hound") (r "=3.5.0") (d #t) (k 0)))) (h "1s8jrfkd1il6qxwy0swpjxrfgfsivj9y684zsb4wnjsjq7fsg9p8") (y #t) (r "1.56.1")))

(define-public crate-data2sound-0.1.4 (c (n "data2sound") (v "0.1.4") (d (list (d (n "file-utils") (r "=0.1.5") (d #t) (k 0)) (d (n "hound") (r "=3.5.0") (d #t) (k 0)))) (h "113n3y6rrqyw1z9gmvk9xj42rjvsi3njql8rd2c3wqp3gcj4npnz") (y #t) (r "1.56.1")))

(define-public crate-data2sound-0.1.5 (c (n "data2sound") (v "0.1.5") (d (list (d (n "file-utils") (r "=0.1.5") (d #t) (k 0)) (d (n "hound") (r "=3.5.0") (d #t) (k 0)))) (h "1afl79wxab3pq37h84miia5q53kizkswzialf45m8da4crq6b1vy") (r "1.56.1")))

(define-public crate-data2sound-0.1.6 (c (n "data2sound") (v "0.1.6") (d (list (d (n "file-utils") (r "=0.1.5") (d #t) (k 0)) (d (n "hound") (r "=3.5.0") (d #t) (k 0)))) (h "1ags9y4k9ys4vn984ywabjsywi1kjk1gxh5016rsi1v4pixskjfl") (r "1.56.1")))

(define-public crate-data2sound-0.2.0 (c (n "data2sound") (v "0.2.0") (h "1yk11jkgdd96cqqrgj4rmaq6xk1w1z4klm3lqpynxf19xrflbkcv") (r "1.56.1")))

(define-public crate-data2sound-0.2.1 (c (n "data2sound") (v "0.2.1") (h "0xdssx5z9l5c4wxin6wx6jr8vqrjp94ff5mw3y5h5a0j457wa6j1") (r "1.56.1")))

(define-public crate-data2sound-0.2.2 (c (n "data2sound") (v "0.2.2") (h "0qh6s0jjigl1swzzrlh3szj3l2nwzhppkf1pr6hsdf6sxibm3b2g") (r "1.56.1")))

