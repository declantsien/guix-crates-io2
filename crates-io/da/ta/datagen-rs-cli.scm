(define-module (crates-io da ta datagen-rs-cli) #:use-module (crates-io))

(define-public crate-datagen-rs-cli-0.1.0 (c (n "datagen-rs-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "datagen-rs") (r "^0.1.0") (f (quote ("all"))) (d #t) (k 0)) (d (n "datagen-rs-progress-plugin") (r "^0.1.0") (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "num-format") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "11jn4prgrsahvlb8wvic7wjywrqq2bkw92nbw8lbbszhpjfg4zzh")))

