(define-module (crates-io da ta dataflow_nlp) #:use-module (crates-io))

(define-public crate-dataflow_nlp-0.1.0 (c (n "dataflow_nlp") (v "0.1.0") (d (list (d (n "dataflow") (r "^0.3.2") (d #t) (k 0)) (d (n "lentrait") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokenizers") (r "^0.11") (d #t) (k 0)))) (h "0wgig684hnzdzrxy999mwqp8z69isix3byqia80lxjdadnsjl056")))

(define-public crate-dataflow_nlp-0.1.1 (c (n "dataflow_nlp") (v "0.1.1") (d (list (d (n "dataflow") (r "^0.4") (d #t) (k 0)) (d (n "lentrait") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokenizers") (r "^0.11") (d #t) (k 0)))) (h "1ixllayqbasw6bdiyd3ja45vynav183bl2121inp2md4yvbn9307")))

