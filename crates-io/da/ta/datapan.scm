(define-module (crates-io da ta datapan) #:use-module (crates-io))

(define-public crate-datapan-0.1.1 (c (n "datapan") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.13") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.4") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "simd-json") (r "^0.2.2") (d #t) (k 0)))) (h "1606mvy1v62qy39qgvprknafbizlmn6anbk1nzxipr6vg9p7by38")))

