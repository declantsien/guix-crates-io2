(define-module (crates-io da ta datafrog) #:use-module (crates-io))

(define-public crate-datafrog-0.1.0 (c (n "datafrog") (v "0.1.0") (h "0hp1wx1vz1j5vfz6hpb4blyf5ngqnl4i0indxv7csxzy9yzj9mqn")))

(define-public crate-datafrog-1.0.0 (c (n "datafrog") (v "1.0.0") (h "027sp7c4w3jri5jqxhpn33p9srxv4zgc40c5f6851cwvfz6fm8vi")))

(define-public crate-datafrog-2.0.0 (c (n "datafrog") (v "2.0.0") (d (list (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "0b53ravz3v8303h41j81z4565xhc8knddj7sd6292wwmpb09kfkp")))

(define-public crate-datafrog-2.0.1 (c (n "datafrog") (v "2.0.1") (d (list (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "0sbbwx67gw45yarq4ff431v3bqwa7qvb2r5jkrb2d8vgnb9ambx0")))

