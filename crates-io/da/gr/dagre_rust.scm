(define-module (crates-io da gr dagre_rust) #:use-module (crates-io))

(define-public crate-dagre_rust-0.0.1 (c (n "dagre_rust") (v "0.0.1") (d (list (d (n "graphlib_rust") (r "^0.0.1") (d #t) (k 0)) (d (n "ordered_hashmap") (r "^0.0.3") (d #t) (k 0)))) (h "1d6sqrp88mnapsqg5k7c37jvr2w30rrzv8s60lbb65x4hgl87w6r") (y #t)))

(define-public crate-dagre_rust-0.0.2 (c (n "dagre_rust") (v "0.0.2") (d (list (d (n "graphlib_rust") (r "^0.0.1") (d #t) (k 0)) (d (n "ordered_hashmap") (r "^0.0.3") (d #t) (k 0)))) (h "197y0lcgbcmfhmrfiny95gcac7rvwlzk24layvyy3vdfkmyq3vrc")))

(define-public crate-dagre_rust-0.0.3 (c (n "dagre_rust") (v "0.0.3") (d (list (d (n "graphlib_rust") (r "^0.0.1") (d #t) (k 0)) (d (n "ordered_hashmap") (r "^0.0.3") (d #t) (k 0)))) (h "1q54f8m8k76139cl75vyy8p7qb8zpk389zds8hidbgggyfn92r9j")))

(define-public crate-dagre_rust-0.0.4 (c (n "dagre_rust") (v "0.0.4") (d (list (d (n "graphlib_rust") (r "^0.0.1") (d #t) (k 0)) (d (n "ordered_hashmap") (r "^0.0.3") (d #t) (k 0)))) (h "1nks01vz3vhl8i95pqarxi39svkq2k5vndzdvk9wgzgy90qaixmq")))

(define-public crate-dagre_rust-0.0.5 (c (n "dagre_rust") (v "0.0.5") (d (list (d (n "graphlib_rust") (r "^0.0.2") (d #t) (k 0)) (d (n "ordered_hashmap") (r "^0.0.3") (d #t) (k 0)))) (h "09hr2ajgj668l61x8qnmq0zg9x72yrq1mak52ad9fcia5pr483my")))

