(define-module (crates-io da rt darth-ui) #:use-module (crates-io))

(define-public crate-darth-ui-0.1.0 (c (n "darth-ui") (v "0.1.0") (d (list (d (n "darth-rust") (r "^4.1.8") (d #t) (k 0)) (d (n "leptos") (r "^0.6.11") (f (quote ("csr" "nightly"))) (d #t) (k 0)) (d (n "leptos-font-icons") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "04rxlmwz0shbrpwnplnlaamga63xlp5f586q2lksh6bkzmr9jsb5")))

(define-public crate-darth-ui-0.1.1 (c (n "darth-ui") (v "0.1.1") (d (list (d (n "darth-rust") (r "^4.2.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.69") (d #t) (k 0)) (d (n "leptos") (r "^0.6.11") (f (quote ("csr" "nightly"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (o #t) (d #t) (k 0)))) (h "08zwia11fzsz2bc1n4pr4pmdjay9vbgpay3wkks0byml77dl7p6z") (f (quote (("icons") ("form" "regex") ("examples" "form" "container" "button" "icons") ("default" "examples") ("container") ("button"))))))

(define-public crate-darth-ui-0.1.2 (c (n "darth-ui") (v "0.1.2") (d (list (d (n "darth-rust") (r "^4.2.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.69") (d #t) (k 0)) (d (n "leptos") (r "^0.6.11") (f (quote ("csr" "nightly"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (o #t) (d #t) (k 0)))) (h "1crizkwp0salwqxlqnzxnn6q9wcgczw0grag12kn9az4lf7asdry") (f (quote (("input-text") ("icons") ("form" "regex" "input-text") ("examples" "form" "container" "button" "icons" "input-text") ("default" "examples") ("container") ("button"))))))

