(define-module (crates-io da rt dart6ul-gpio) #:use-module (crates-io))

(define-public crate-dart6ul-gpio-0.1.0 (c (n "dart6ul-gpio") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0djlvmjhvl6gl26pxpgk5s8w74vbdl99kbpdvcm586r20gad6xyn")))

(define-public crate-dart6ul-gpio-0.1.1 (c (n "dart6ul-gpio") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0137s1gpzj0a6fgghfps1x364x582rmj30xmb5n2qlhi8grxii8z")))

(define-public crate-dart6ul-gpio-0.1.2 (c (n "dart6ul-gpio") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0ckfw0c6w7rpmg38vkvd5g7hvfkm5cjfdj7q7dla1qcz8rxcm728")))

(define-public crate-dart6ul-gpio-0.2.0 (c (n "dart6ul-gpio") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1dl4b3gjqn6056a2plw0dkmxqb7ylc7z36lymikkc09ydn80rzrq")))

