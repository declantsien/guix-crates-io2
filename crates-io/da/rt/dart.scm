(define-module (crates-io da rt dart) #:use-module (crates-io))

(define-public crate-dart-0.1.0 (c (n "dart") (v "0.1.0") (d (list (d (n "dart-sys") (r "^2.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "mashup") (r "^0.1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0cafkffcw57fqnmdl6sfj7fa1zzzi53ryfy55rvq2hnhldjr1i1l")))

(define-public crate-dart-0.1.1 (c (n "dart") (v "0.1.1") (d (list (d (n "dart-sys") (r "^2.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "mashup") (r "^0.1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0mjxdnxg2nb5paiirvxq7lskqw6cd0khibhanm2gxkq9yl1j838z")))

