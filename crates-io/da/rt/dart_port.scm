(define-module (crates-io da rt dart_port) #:use-module (crates-io))

(define-public crate-dart_port-0.0.0 (c (n "dart_port") (v "0.0.0") (h "17i0s9iibxa8wd7vhm5p7arpcj4c206vg2rvhgihi9lqcn113v6j") (f (quote (("default")))) (r "1.70")))

(define-public crate-dart_port-0.0.1 (c (n "dart_port") (v "0.0.1") (d (list (d (n "dart-sys") (r "^4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fyv0w6c2xzyvrg8xqzkvl7i6nwpjkxl4yff41z5dfiki1xvici5") (f (quote (("static") ("dynamic") ("default" "dynamic")))) (r "1.70")))

