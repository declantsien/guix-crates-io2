(define-module (crates-io da rt dart-semver) #:use-module (crates-io))

(define-public crate-dart-semver-0.1.0 (c (n "dart-semver") (v "0.1.0") (d (list (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)))) (h "11iipc5ll9xr5plcdkqiw0ma4iq9gx78xnhafh79vn7iwlf5g4f8")))

(define-public crate-dart-semver-0.1.1 (c (n "dart-semver") (v "0.1.1") (d (list (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)))) (h "1a5jpayib0rgn8nilmgm5h5pxzjmwpkwas04z863k3qrkcpwlblx")))

(define-public crate-dart-semver-0.1.2 (c (n "dart-semver") (v "0.1.2") (d (list (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)))) (h "1mprpmi8a8vhzb8mkcihcpr06yzv2ihzhf0s1j8pl6a5fqdbn2nm")))

(define-public crate-dart-semver-0.1.3 (c (n "dart-semver") (v "0.1.3") (d (list (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)))) (h "1fj9w9nh6s52c7m53crzvfrmxpccc18xnak78ndq56pm0jcyxvki")))

(define-public crate-dart-semver-0.2.0 (c (n "dart-semver") (v "0.2.0") (d (list (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.0") (d #t) (k 0)))) (h "1xvyk9zy661a2gxm8g0isn1q7zyihj6fw7b3rf2772pqhxa4nnbx")))

