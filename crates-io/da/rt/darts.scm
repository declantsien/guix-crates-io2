(define-module (crates-io da rt darts) #:use-module (crates-io))

(define-public crate-darts-0.1.0 (c (n "darts") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07wqm5lnch6pgzpv4dd1fgq53brlzg5xlgcpr88nxb90wyiv28qq") (f (quote (("serialization" "bincode" "serde") ("searcher") ("default"))))))

