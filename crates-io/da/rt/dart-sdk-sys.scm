(define-module (crates-io da rt dart-sdk-sys) #:use-module (crates-io))

(define-public crate-dart-sdk-sys-2.12.1 (c (n "dart-sdk-sys") (v "2.12.1") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1bjzijbq4p492dwslv9wwlzq9gmnq6p40749zinb10rwq145x18p") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.12.2 (c (n "dart-sdk-sys") (v "2.12.2") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1389crmh5rhzhly5yil0h61mp2hk1n2q718vlcxpy1mzlcn0lxsh") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.12.3 (c (n "dart-sdk-sys") (v "2.12.3") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "08ljipz4aqxa06n67gznmhp4fqlq7l63yk0aywysscb4yh0vm43r") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.12.4 (c (n "dart-sdk-sys") (v "2.12.4") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0yvhrqbv9mgmynvm6x8pmp56hqvyc7d5lmjv5zgrr57i79zl96al") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.13.0 (c (n "dart-sdk-sys") (v "2.13.0") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1bwkmbyr4fr2ai941cs6vh9l90ll1ln3gmvdlzahc7x40cya1iw8") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.13.1 (c (n "dart-sdk-sys") (v "2.13.1") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0405ndg72l6jibmvmwpzgd17biniz8c7q0i83pyljy3xb7x2gc66") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.13.3 (c (n "dart-sdk-sys") (v "2.13.3") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "19yx6bszazhm1mz53bv3lmql1yjg8hn4mk07scv0vl7z7l8k9w6l") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.13.4 (c (n "dart-sdk-sys") (v "2.13.4") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "09z3kx2idmk85gd05ccrvz526w4dqc1n6kz22xh10cvhmkiiahd0") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.14.0 (c (n "dart-sdk-sys") (v "2.14.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0x1h4rs6fjkfhz3m71vnr2ph5pxda2rh8lm514fd8pwasqzym42r") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.14.1 (c (n "dart-sdk-sys") (v "2.14.1") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1rqd2d30hbjqjd8hv2cr7gr2l8151x8df0f3apnwssywnrq16mhr") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.14.2 (c (n "dart-sdk-sys") (v "2.14.2") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1rvq42zy2yfx43h3fxk15k872192k88yc4rhq181y6hgb69kym4v") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.14.3 (c (n "dart-sdk-sys") (v "2.14.3") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0b8ym7p9z2yx5aaakgl5ls0yfvsrjif1q3xsr7jmbz5f454dz919") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.14.4 (c (n "dart-sdk-sys") (v "2.14.4") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "16478l7f2p80br8n8888i89z3qjinkrjl4qb53kfz59skpf3w25d") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.15.0 (c (n "dart-sdk-sys") (v "2.15.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0g2q0m4nx00c2hcnh0g34nsgswwvna10pxj04x9py8gz38kgb5z2") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.15.1 (c (n "dart-sdk-sys") (v "2.15.1") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0jma1gnclhqkyy0xlmd9cp2m25n55yg21hhn25z7bhnbx8730l9x") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.10.4 (c (n "dart-sdk-sys") (v "2.10.4") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0xqw5hmwirbb0x3nbkadd4lagnrw78hdx0h50faldqx0jb2zrhi3") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.16.0 (c (n "dart-sdk-sys") (v "2.16.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1mc4jfrvx4760137a0dbmrsdy4i0pvpnsgpjabss15yg6j8vi6jx") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.16.1 (c (n "dart-sdk-sys") (v "2.16.1") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1147c51fxypa7zhk9h661z1c4sh1b0islywl8cpjxgb999mz6mm5") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.16.2 (c (n "dart-sdk-sys") (v "2.16.2") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0qygn50xp1dg63ywplkn4nb0kmms2rdls0m4hcnx5kc7r98fxal9") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.17.0 (c (n "dart-sdk-sys") (v "2.17.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0b2j87km80b6553iwkdby2vrg7shcsrz6krwkllvdmfjgh0phvib") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.17.1 (c (n "dart-sdk-sys") (v "2.17.1") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "07imw746jdxyyhr0p6wqsgq506i8zm4f5aab21h7lkmjn2fhjbms") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.18.1 (c (n "dart-sdk-sys") (v "2.18.1") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "11xbg1vjb5qg3hij5bjk5sz48qv5s1gm16iacpah8pg3ycisrqis") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.18.6 (c (n "dart-sdk-sys") (v "2.18.6") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0cds19clqczh74ihbw98nnljfm8xx84rrp2a8dxy7vpxzrpcz3hk") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

(define-public crate-dart-sdk-sys-2.19.3 (c (n "dart-sdk-sys") (v "2.19.3") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)))) (h "11iglzd6wpbfn2791jfz8j77p1qyh4r65izhp111yrvbhfz8wiwq") (f (quote (("download-sources") ("build-bindings" "bindgen"))))))

