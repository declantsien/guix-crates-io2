(define-module (crates-io da nd dandy) #:use-module (crates-io))

(define-public crate-dandy-0.1.0 (c (n "dandy") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1b1qb62d9wlpdziihbz281943p9vz1lhfgsyp2c6yysav0amw1md")))

(define-public crate-dandy-0.1.1 (c (n "dandy") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0af2kzmcc8fi24ni8fwb33r1v5lwyiykaafbm4zxpm3vh328bxh3")))

(define-public crate-dandy-0.1.2 (c (n "dandy") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0wd7nfisnngcjd3yipyng5sb5l0dx4smfm0ljgmsxrz2fzbvi5xw")))

(define-public crate-dandy-0.1.3 (c (n "dandy") (v "0.1.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1mzyp4i8f2q89a8qbbmjwpn569iy11v2baq7j20hzyi6vgvddlfg")))

(define-public crate-dandy-0.1.4 (c (n "dandy") (v "0.1.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0kajg0w8rf5qn6qj38vlai30z0bwz2g6c2f1qcc40kkv6990i036")))

(define-public crate-dandy-0.1.5 (c (n "dandy") (v "0.1.5") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0fgsg7jcy1jgqbpmdj098ybfzc7jazfhzwqq5jh712v2rs6663lw")))

(define-public crate-dandy-0.1.6 (c (n "dandy") (v "0.1.6") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0a3kmxljbp8gbq63lyd20ppa1rh5nvg15szqd3f4iiimknmmwl24")))

(define-public crate-dandy-0.1.7 (c (n "dandy") (v "0.1.7") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0nqhy3lp22cbc7qnvj0jp6ng72hgjr0n2kkpwj1ssq165qmmkmx4")))

(define-public crate-dandy-0.1.8 (c (n "dandy") (v "0.1.8") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0kvbfzslzd88bzm50bni0v9c30d5zfkvbks5zcif89w5pd5yd7mp")))

(define-public crate-dandy-0.1.9 (c (n "dandy") (v "0.1.9") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1cxkxzjd9xjhr8kivxkbdfvy4lq36yfxl57qq7cbawv8dbvlflpk")))

(define-public crate-dandy-0.1.10 (c (n "dandy") (v "0.1.10") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1zlq7gdxd3arnw9x5kib2hm63pqcy2rj49d8iziczpxrl1w64x3c")))

