(define-module (crates-io da ng danger) #:use-module (crates-io))

(define-public crate-danger-0.1.0 (c (n "danger") (v "0.1.0") (d (list (d (n "reqwest") (r "^0") (d #t) (k 0)) (d (n "schemafy") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02v1czam4gmpc2r6rbis5axldb9gsaijygr8pspln7zak23dax2f")))

