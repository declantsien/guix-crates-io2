(define-module (crates-io da ng dangerous_option) #:use-module (crates-io))

(define-public crate-dangerous_option-0.1.0 (c (n "dangerous_option") (v "0.1.0") (h "12izl8f7h9w9yxam8vv55r38cvwdvw8xz5c3gkxc7dqm2mw7bz4n")))

(define-public crate-dangerous_option-0.2.0 (c (n "dangerous_option") (v "0.2.0") (h "0qirj5yrzq0g86n2g614dmbmyl04xln7kba9b8z281yyrpzqq84n")))

