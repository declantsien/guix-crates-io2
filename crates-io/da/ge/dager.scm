(define-module (crates-io da ge dager) #:use-module (crates-io))

(define-public crate-dager-0.1.0 (c (n "dager") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "03v2q3v18iq26dwb2nn2q5b6xzk99f836bcfxnsva9qba44q03ay")))

(define-public crate-dager-0.1.1 (c (n "dager") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r ">=1.13.0, <2.0.0") (d #t) (k 0)))) (h "1hznh9i39w59skx6wq4bmc3j2fhgvwgbgx7hn12yn80q8c9j83bg")))

