(define-module (crates-io da yl daylio) #:use-module (crates-io))

(define-public crate-daylio-0.1.0 (c (n "daylio") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r8cxpj9hm9dcclyn1hya58m2w4040093vrvgp5bnbaadr65kl5r")))

(define-public crate-daylio-0.2.0 (c (n "daylio") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z76kd47hk0xdvfxxh3539whr6wcwfrzszbvmqp5m24sdj6hqslm")))

