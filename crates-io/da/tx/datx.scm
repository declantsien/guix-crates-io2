(define-module (crates-io da tx datx) #:use-module (crates-io))

(define-public crate-datx-0.1.0 (c (n "datx") (v "0.1.0") (h "05j02rq6gax221jjmfa81plvmyr938iylifyfxang105lpv7s2p6")))

(define-public crate-datx-0.1.1 (c (n "datx") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "all"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15wygvl5bpnfa6v6c1mq1w4snv3pq59xp8fla8rkb091q46qq6jj")))

