(define-module (crates-io da rr darrentsung_debug_parser) #:use-module (crates-io))

(define-public crate-darrentsung_debug_parser-0.1.3 (c (n "darrentsung_debug_parser") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8") (d #t) (k 0)))) (h "0dhm5k47mh162j6qw4r07xpx8ii6f60h10mcysdl8p81kgqjvq2y")))

(define-public crate-darrentsung_debug_parser-0.2.0 (c (n "darrentsung_debug_parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8") (d #t) (k 0)))) (h "09z7fa50yxwcfwcvffmbmxpmchhicicjl94hhrs4wmssa0is1d63")))

(define-public crate-darrentsung_debug_parser-0.3.0 (c (n "darrentsung_debug_parser") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8") (d #t) (k 0)))) (h "0gv7vx1mi9nw0zamrkag8gzs59frbrg2flzf38zjxyv23q5r33q8")))

(define-public crate-darrentsung_debug_parser-0.3.1 (c (n "darrentsung_debug_parser") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8") (d #t) (k 0)))) (h "1i7xb326zcaxwwskidxk0p4ka6cgpwzmrsdycj73rkh7g358wj5z")))

