(define-module (crates-io da ba dababy) #:use-module (crates-io))

(define-public crate-dababy-0.1.0 (c (n "dababy") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1czv78l1n2wk867h0b6gcsa5xf7iivv15vypbzhp198s7d33k2n9")))

(define-public crate-dababy-0.1.1 (c (n "dababy") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "168n8xk66c64jmb6jgp8p8x4l9rzsda514c22p8lgzznszxb3hfd")))

(define-public crate-dababy-0.1.2 (c (n "dababy") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1ymkjs5zadldwqihkpkmyfb1w2ryy1l64cq0n3jqrp04dafrn01v")))

(define-public crate-dababy-0.1.3 (c (n "dababy") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1qw375mi8ynipfavlmmdqj59sg02l44nkka8v0mvlw37qg9g6fsn")))

(define-public crate-dababy-0.1.4 (c (n "dababy") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1nvkvb3sbrnp3rvx5yibplqhvnvmc01kkyz1mnw58grm4bv5axkc")))

(define-public crate-dababy-0.1.5 (c (n "dababy") (v "0.1.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "17dgzj1xq21bjw7bnbkgkv1d9i196sw16ij4ps59gdazcd3kavdm")))

(define-public crate-dababy-0.1.6 (c (n "dababy") (v "0.1.6") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0krda1xgadhp5ha3rq8scmwlvfvamc41fpxpwbgs12zicv6vm2px")))

(define-public crate-dababy-0.1.7 (c (n "dababy") (v "0.1.7") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1fnfsr1ppb51n0wjxh0vlqzlr7bzsxd53ia18vspwy32a3mzisip")))

(define-public crate-dababy-0.2.0 (c (n "dababy") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0px9q066rcsdhz7x3hqa4jgfx379l81b6av4jn12hjv6idc6sgj5")))

(define-public crate-dababy-0.2.1 (c (n "dababy") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1xqsd5afnxhq7l1hm4vsh22gfq5y320ybx1br2fyvb4hmwchzp55")))

