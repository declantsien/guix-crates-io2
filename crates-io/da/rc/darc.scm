(define-module (crates-io da rc darc) #:use-module (crates-io))

(define-public crate-darc-0.0.1 (c (n "darc") (v "0.0.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1b6jgq315isfs95zrgmjgq6x3sddprmay6z431wid8khyidybhk7")))

(define-public crate-darc-0.0.2 (c (n "darc") (v "0.0.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "08hzzc2wgvgdn6yvijzgj00rry786f940fi40vkrm3b9ryjl308p")))

