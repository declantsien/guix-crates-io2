(define-module (crates-io da em daemonize-rs) #:use-module (crates-io))

(define-public crate-daemonize-rs-0.1.0 (c (n "daemonize-rs") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "0zqcrlymmnh36d9yy5fs9vs0ybwfwnw8wr3i21mqq15x3m9kzqbs") (y #t)))

