(define-module (crates-io da em daemonbase) #:use-module (crates-io))

(define-public crate-daemonbase-0.1.0 (c (n "daemonbase") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "~4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("fs" "process" "user"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0.95") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syslog") (r "^6") (d #t) (t "cfg(unix)") (k 0)) (d (n "toml_edit") (r "^0.20") (d #t) (k 0)))) (h "0x68dcabqm95jjk2pkfpnxb3fh7m1ssim77kd29r02gqga5ijbif") (r "1.70")))

(define-public crate-daemonbase-0.1.1 (c (n "daemonbase") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "~4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("fs" "process" "user"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0.95") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syslog") (r "^6") (d #t) (t "cfg(unix)") (k 0)) (d (n "toml_edit") (r "^0.22") (d #t) (k 0)))) (h "0byy7d45isvck9205jgnlp8jhwlvaap0hnp92xx45xfpqbf3bxp7") (r "1.70")))

