(define-module (crates-io da em daemonize-simple) #:use-module (crates-io))

(define-public crate-daemonize-simple-0.1.0 (c (n "daemonize-simple") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "07dd07wr45w3d0jwcdf51ha1dfqgdyvxi3f4s1sfqw5ws82aw31d")))

(define-public crate-daemonize-simple-0.1.1 (c (n "daemonize-simple") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1rv6m2bjd6hik224sjcv3y0rmrh8ajknkrr6hkk54fln2cf2wvnq")))

(define-public crate-daemonize-simple-0.1.2 (c (n "daemonize-simple") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "08yk84vddwm66nbcxvf7jw7fzf9zngkjwjyjhgr9yffabsbzv2yr")))

(define-public crate-daemonize-simple-0.1.3 (c (n "daemonize-simple") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00q49ibzm28aqd8b2aikvx8r7z7pd62xq2azakpff51li048r118") (y #t)))

(define-public crate-daemonize-simple-0.1.4 (c (n "daemonize-simple") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1amb0dq9ls6qyr7lr6vlbzxdcfcsb7r3p63jxnm8ly50iagmhqsg")))

(define-public crate-daemonize-simple-0.1.5 (c (n "daemonize-simple") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pvgcmz91r1h2v7lw7mib3b1jgpl0d29d5qcvdpy1c08sxp16g69")))

