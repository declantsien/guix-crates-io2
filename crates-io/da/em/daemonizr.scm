(define-module (crates-io da em daemonizr) #:use-module (crates-io))

(define-public crate-daemonizr-0.1.0 (c (n "daemonizr") (v "0.1.0") (d (list (d (n "nix") (r "^0.20.0") (d #t) (k 0)))) (h "0avjbvk093mjqmd4jnngk83fn11lrlzfxk4aqr13sfb0n6409h3j") (y #t)))

(define-public crate-daemonizr-0.1.1 (c (n "daemonizr") (v "0.1.1") (d (list (d (n "nix") (r "^0.20.0") (d #t) (k 0)))) (h "01m3iv5lwpa7vihnv4pgnyxk45gdrf6km5wxyqlqqjx6jj5h4w1s") (y #t)))

(define-public crate-daemonizr-0.1.2 (c (n "daemonizr") (v "0.1.2") (d (list (d (n "nix") (r "^0.20.0") (d #t) (k 0)))) (h "096gxi3q4nyxhg538wl4wni6f7n7ki5v76s1ndcq88njqpps7v59") (y #t)))

(define-public crate-daemonizr-0.1.3 (c (n "daemonizr") (v "0.1.3") (d (list (d (n "nix") (r "^0.20.0") (d #t) (k 0)))) (h "1gv0yfaf37r6swyfar40spcikq77r3wcvp7h5ldhzc0v33a6v4jl")))

(define-public crate-daemonizr-0.1.4 (c (n "daemonizr") (v "0.1.4") (d (list (d (n "nix") (r "^0.23.1") (d #t) (k 0)))) (h "1vk7n4q3hm3q0l904m3rmsplfnv714m1pz8i9iqglr4a7cv92hfq")))

(define-public crate-daemonizr-0.1.5 (c (n "daemonizr") (v "0.1.5") (d (list (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "0sbj9y9rd374z43iwya5vl0x4g0xwgq817m9hdacj87k7kama37f")))

