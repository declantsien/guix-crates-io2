(define-module (crates-io da em daemonize-me) #:use-module (crates-io))

(define-public crate-daemonize-me-0.1.0-alpha (c (n "daemonize-me") (v "0.1.0-alpha") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "0sr0l7pal1vxmmjdippci7jyvkgp1arpn57xdk5rb4j04v93nwsa")))

(define-public crate-daemonize-me-0.1.1-alpha (c (n "daemonize-me") (v "0.1.1-alpha") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "1xh2fa6a9bfbbpx046bhw80zxjmgir004l58nmfg2mb9ljra9wc6")))

(define-public crate-daemonize-me-0.1.2-alpha (c (n "daemonize-me") (v "0.1.2-alpha") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "0lfzn17hv69lx1cpmgjrgg40b985djj2yddhbwygl68xalgcc0yr")))

(define-public crate-daemonize-me-0.1.2-bsd-fix (c (n "daemonize-me") (v "0.1.2-bsd-fix") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "11cjd1r36xfcnmpdnd0l3r7cm1hm8pkzh67zwalhb8xscixcqg97")))

(define-public crate-daemonize-me-0.1.3-alpha (c (n "daemonize-me") (v "0.1.3-alpha") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "16c2l6ngjinlqhqq6v1szdys3mkinhkshr3vk73k56h10qx11jf8")))

(define-public crate-daemonize-me-0.2.0-pre (c (n "daemonize-me") (v "0.2.0-pre") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "17z3d4z4lkqkghj5vkab8zkx4b70k9q5k1ac9pf84f8l92p7is8j")))

(define-public crate-daemonize-me-0.2.1-pre (c (n "daemonize-me") (v "0.2.1-pre") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "0v8yzvx57nbvnf3s4nm9xb8axlrf2ci4v0pwrzk2bmp3l9k0gdsj")))

(define-public crate-daemonize-me-0.2.1 (c (n "daemonize-me") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "1m7ylfxqq6jh00vwh3wvx3x4yyjid0kcld4zg3ijpsz1hll83clh")))

(define-public crate-daemonize-me-0.3.0 (c (n "daemonize-me") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1zh7ysxd48dmm6xkjr9qacwxj3c1a3cpjdpiijpb5mikakhwg20f")))

(define-public crate-daemonize-me-0.3.1 (c (n "daemonize-me") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "15zippif5pfbapc2v4a40n7y7njq3byna7zw8dn7idgbgyj69107")))

(define-public crate-daemonize-me-1.0.0 (c (n "daemonize-me") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "0p2pg9r8i2a98j9vg2rlsxypsxrdqikxvlcldnqbmvbi5fx0iayi")))

(define-public crate-daemonize-me-2.0.0-alpha (c (n "daemonize-me") (v "2.0.0-alpha") (d (list (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "18wpigaigqf38lqjb7lx55b2diff7j2njrcqrzykbibynchnxkjq") (y #t)))

(define-public crate-daemonize-me-2.0.1-alpha (c (n "daemonize-me") (v "2.0.1-alpha") (d (list (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "0y12hdidmmwadi5ppg9bqv6g31d70p33j9razwgj2gxnz5673w1w") (y #t)))

(define-public crate-daemonize-me-2.0.0-alpha2 (c (n "daemonize-me") (v "2.0.0-alpha2") (d (list (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "17dyl3vwpa2rkp0569g4n3v254gx9p5vz7cs6996zcqiz8qavgck") (y #t)))

(define-public crate-daemonize-me-1.0.1 (c (n "daemonize-me") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "11555yfv0pkhjx8iqdlfd6v2dq3w0817x015kys31yj8amx3p4al")))

(define-public crate-daemonize-me-1.0.2 (c (n "daemonize-me") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1v3hfk4lr05bbs93947psrbvigzdwzfzwi7rzb00cw1pa7g34csq")))

(define-public crate-daemonize-me-2.0.0 (c (n "daemonize-me") (v "2.0.0") (d (list (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gx7bjsl4d6q5hrfvanxyxsdm8q5aks7kglrrqs8hvv5mk3689ac")))

(define-public crate-daemonize-me-2.0.1 (c (n "daemonize-me") (v "2.0.1") (d (list (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c1qrydhfcvq73dw6v42p4yrsqvjl75mx6x2n6axfchd9p2774xp")))

