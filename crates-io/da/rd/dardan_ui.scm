(define-module (crates-io da rd dardan_ui) #:use-module (crates-io))

(define-public crate-dardan_ui-0.1.0 (c (n "dardan_ui") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)))) (h "17dirq4pvbp6dgxv3wayamni2qzjx5yqsbbir2wa03cffvwa31nl")))

(define-public crate-dardan_ui-0.1.1 (c (n "dardan_ui") (v "0.1.1") (d (list (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)))) (h "1zn31ibdxwvzfq6vbdz70k1canbx6irwshhk9w33lq42l9fvc444")))

