(define-module (crates-io da cx dacx0501) #:use-module (crates-io))

(define-public crate-dacx0501-0.1.0 (c (n "dacx0501") (v "0.1.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)))) (h "0p3zx0h6lfcymk2brka7ly9grw3sjsykqai9q82hijmdff1q3pv3")))

(define-public crate-dacx0501-0.2.0 (c (n "dacx0501") (v "0.2.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)))) (h "1k2ghj41qr91hpkxvsbc0w8nj5xzfhbs6aiqx5jwi6cggd4y3kpw")))

(define-public crate-dacx0501-0.2.1 (c (n "dacx0501") (v "0.2.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)))) (h "1l0hqj3c0a0rm80s44hmi3zam2vlcfxzp1ry54xrk11wbm8sm50g")))

(define-public crate-dacx0501-0.2.2 (c (n "dacx0501") (v "0.2.2") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)))) (h "0gddlabhmnignlrkwh3mrzwmb4mb5ms9i5p89gwqv5lhpjfg6gic")))

