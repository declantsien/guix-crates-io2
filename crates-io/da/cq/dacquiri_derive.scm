(define-module (crates-io da cq dacquiri_derive) #:use-module (crates-io))

(define-public crate-dacquiri_derive-0.2.0 (c (n "dacquiri_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "053lz7dn05v4rp6yrrgyildib2hfbn4mf7wvf5i0z2zb598ghm6k")))

(define-public crate-dacquiri_derive-0.2.1 (c (n "dacquiri_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16l08p0d3dvnh29rvw4jycnq95ayqnfy0xi7ip8xwdzf9j6xkdyc")))

(define-public crate-dacquiri_derive-0.2.2 (c (n "dacquiri_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00qnch7axa9vjpchibngz9yb0ar8zqn54qq77lhbkyp9qjcpa1mf")))

(define-public crate-dacquiri_derive-0.3.0 (c (n "dacquiri_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lkcqxj4gqgg1w2gmsfx3175sx03l7fjin9ppvjyg0gjsqfizirz")))

(define-public crate-dacquiri_derive-0.3.1 (c (n "dacquiri_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "032rqvzyrmd50xnz9xzgy7wzc9dgjq9x4jq9yblmck9682vg5xl6")))

(define-public crate-dacquiri_derive-0.4.0 (c (n "dacquiri_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mjblh45xia469vklyb1sns5kb5ax88xbi34lr0mfixa680m2h4a")))

(define-public crate-dacquiri_derive-0.4.2 (c (n "dacquiri_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12164pids0ncz0cf2v68dhmz2g92xl32vv7b8h2lr3znhzplvh89")))

(define-public crate-dacquiri_derive-0.4.3 (c (n "dacquiri_derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fv578xim437b8rgvp3hwa53793a692glmppcdv6bqxkmhhwdlj9")))

(define-public crate-dacquiri_derive-0.4.4 (c (n "dacquiri_derive") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15c0phshl1qpfcfjmz705j58j9klqm9p302rsdlcaaqg64p3321n")))

(define-public crate-dacquiri_derive-0.5.0-rc1 (c (n "dacquiri_derive") (v "0.5.0-rc1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "120aqp91ngqh1ywi89yrsp953dqm7nha73xhj5asqlvw3p21clpx") (f (quote (("unstable_policy_inheritance") ("default"))))))

(define-public crate-dacquiri_derive-0.5.0 (c (n "dacquiri_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12hma1m3jnqk31fw56nmn3vrr5fx0b4xc4j75n08gvjc3lsxif56") (f (quote (("unstable_policy_inheritance") ("default"))))))

(define-public crate-dacquiri_derive-0.5.1 (c (n "dacquiri_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qx9ydaxil5zmqkvx3s6m4x944x31779fsng2jsisk15y0pgf4if") (f (quote (("unstable_policy_inheritance") ("unstable_entity_proof_inheritance") ("default"))))))

