(define-module (crates-io da e- dae-parser) #:use-module (crates-io))

(define-public crate-dae-parser-0.1.0 (c (n "dae-parser") (v "0.1.0") (d (list (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)))) (h "08370cl75dpfjs6bnm654f5ksgkn5wvb49dmc7v8a9y7qfcl97kb")))

(define-public crate-dae-parser-0.1.1 (c (n "dae-parser") (v "0.1.1") (d (list (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)))) (h "1kxg59603s2zn0z0r0rrsr808q9r4z8fj9aag8kni53chh8l9xil")))

(define-public crate-dae-parser-0.2.0 (c (n "dae-parser") (v "0.2.0") (d (list (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)))) (h "0lxxn4f1zxyacww4sf7dclwrx5ybmv8jygs9grc9x161165pc7h1")))

(define-public crate-dae-parser-0.3.0 (c (n "dae-parser") (v "0.3.0") (d (list (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)))) (h "1pg6dhx8s8arlpvxpkgf9qr09qybjdw18dkzh3p46k9b9ayyd343")))

(define-public crate-dae-parser-0.3.1 (c (n "dae-parser") (v "0.3.1") (d (list (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)))) (h "12vl2fc9nfw93v6i07m4l7kwazbx69m8b2b7638xbrlpj722mf8m")))

(define-public crate-dae-parser-0.4.0 (c (n "dae-parser") (v "0.4.0") (d (list (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0c2zjnyri0zg77d5amvlxrdxdzj5xrq6jxvc7yysh4h3gkwwl1wg")))

(define-public crate-dae-parser-0.5.0 (c (n "dae-parser") (v "0.5.0") (d (list (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "07d3fm1zpw8fmm4hzfdklzy38682pc42lvj0g9zv7jcf35avmajp")))

(define-public crate-dae-parser-0.6.0 (c (n "dae-parser") (v "0.6.0") (d (list (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "1f13gh51wi10zxbc0rxgd0q72x37n1jf5n7y5ms93hvp3jq7smz8")))

(define-public crate-dae-parser-0.6.1 (c (n "dae-parser") (v "0.6.1") (d (list (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0whkfj3jdyv5963xzrqhp7r2a1sbfqilk5y0fyffgk058xlhwvaj")))

(define-public crate-dae-parser-0.6.2 (c (n "dae-parser") (v "0.6.2") (d (list (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "10c75frrmi87wkrwn36l7kp8mj1s90ixjl3lrmh6n4h6fq0w1pc2")))

(define-public crate-dae-parser-0.7.0 (c (n "dae-parser") (v "0.7.0") (d (list (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "1g7q1ridkafbq9xnz54nf7qglw2qzrlap9h58mcjkv8v1wqq264n")))

(define-public crate-dae-parser-0.7.1 (c (n "dae-parser") (v "0.7.1") (d (list (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "1jzqgdmhpqskhm6ik525j23im9mln2pdpj36i5av51p4irrb3x2g")))

(define-public crate-dae-parser-0.8.0 (c (n "dae-parser") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0rmcp2v36gv5kdy5bssjhlj2f2y8c6ar5hrdzspmmw13c78rk4w5")))

(define-public crate-dae-parser-0.8.1 (c (n "dae-parser") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "1ha45i23i8v5q194cfglskq8pdjvmllgbi2h0vdabbmxzfq2hprf")))

(define-public crate-dae-parser-0.8.2 (c (n "dae-parser") (v "0.8.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0y6j737g2c26biiawm1gh2ylw3165yygcz36636hjj1fzxn9awag")))

(define-public crate-dae-parser-0.8.3 (c (n "dae-parser") (v "0.8.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0h6m73qdck17i52s6gffpxl9zv1znqphvy2qmqc4biql5bkf5i7b")))

(define-public crate-dae-parser-0.8.4 (c (n "dae-parser") (v "0.8.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "minidom") (r "^0.13") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0qcdxchp1xfx1ph33pnbgzpybmrbknk5vz1mxi2vmgcic81bhic9")))

(define-public crate-dae-parser-0.8.5 (c (n "dae-parser") (v "0.8.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "minidom") (r "^0.14") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0w1wfzlnxrslzsmd865ixb888r5sq4npgp27pvxz6wbc6plj2fm3")))

(define-public crate-dae-parser-0.8.6 (c (n "dae-parser") (v "0.8.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "minidom") (r "^0.14") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0p94n6hzyq8rby4g4n0y7hl65m6d9fzn4a3a4qc9w5sfakn90rvb")))

(define-public crate-dae-parser-0.8.7 (c (n "dae-parser") (v "0.8.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "minidom") (r "^0.14") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "15bim97qh3hchxw0w8spnndw1c8rppswmjq341hc2774kflqlddg")))

(define-public crate-dae-parser-0.8.8 (c (n "dae-parser") (v "0.8.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "minidom") (r "^0.14") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "16zhjmfvm42zvj32v9kxf7f66qfws2wdp1s68cb0bp7i572b0jsn")))

(define-public crate-dae-parser-0.9.0 (c (n "dae-parser") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "minidom-14") (r "^0.15") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0c9qp478z5faxdib370qq5v0p24vvgncpmfkm3vx3g6lgmpfkx51")))

(define-public crate-dae-parser-0.10.0 (c (n "dae-parser") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "minidom-14") (r "^0.16") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "015mf1kqqbn4ymjb5sr1vjii6ypxy57kb2rfkgcm19134wdcqh36")))

