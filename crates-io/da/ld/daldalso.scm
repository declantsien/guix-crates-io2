(define-module (crates-io da ld daldalso) #:use-module (crates-io))

(define-public crate-daldalso-0.1.0 (c (n "daldalso") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "0ygxck8w7jjhzx9j5rpa7zkznr8fbnbmzb9csqz14sc937v4jkgr")))

