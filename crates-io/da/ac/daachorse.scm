(define-module (crates-io da ac daachorse) #:use-module (crates-io))

(define-public crate-daachorse-0.1.0 (c (n "daachorse") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0ijsm9cyyxplinxdk0if3s08svnfx11h9i8pbfkz7xvg94d6ac78")))

(define-public crate-daachorse-0.1.1 (c (n "daachorse") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0aypvag0rsr7a0ig9qskiv19xgddz8pv5c3x9s44rk0fp1i4jd2w")))

(define-public crate-daachorse-0.2.0 (c (n "daachorse") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1wgpnkajag4x46m795b5pzw3rgiaxkj4j8whiffph5w80y5dvhxz")))

(define-public crate-daachorse-0.2.1 (c (n "daachorse") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0fnh4s3j7z4mzvj3m2bb6pk746g33i49sicb2zvz28jbakizmmwh")))

(define-public crate-daachorse-0.3.0 (c (n "daachorse") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0qyi3b558fkhixhivhxkb49r08bl1hxnn3il5ydwik8xydhg8imf")))

(define-public crate-daachorse-0.4.0 (c (n "daachorse") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "119nzc98z52mjgx2llx82148csrdnpcifkqwsp9mmnmacg9fzpww")))

(define-public crate-daachorse-0.4.1 (c (n "daachorse") (v "0.4.1") (h "0bx8h5yfai37i02ymkvl41wvxsi1dp6rndf1g8a0y1w689gqik4n") (f (quote (("std") ("default" "std"))))))

(define-public crate-daachorse-0.4.2 (c (n "daachorse") (v "0.4.2") (h "0zlnb1h52rwh94sgmxkrw9zm26lbmjv86ln33wdbj72gxqi81gpj") (f (quote (("std") ("default" "std"))))))

(define-public crate-daachorse-0.4.3 (c (n "daachorse") (v "0.4.3") (h "19g77ywf1dazxq4spqg3nqcaz0zmmbyhmp2djv9b2ycy25cnb3h4") (f (quote (("std") ("default" "std"))))))

(define-public crate-daachorse-1.0.0-rc.1 (c (n "daachorse") (v "1.0.0-rc.1") (h "074s3fbm4fi6f53pzhn2zap0m6k5f2k0clgkqrrnmrgiw7b1rvlx") (f (quote (("default" "alloc") ("alloc")))) (r "1.58")))

(define-public crate-daachorse-1.0.0 (c (n "daachorse") (v "1.0.0") (h "0djcm42cxp9jmpj09mlzy54dpb8dhcpa5l0491zka2g59dxfzdv3") (f (quote (("default" "alloc") ("alloc")))) (r "1.58")))

