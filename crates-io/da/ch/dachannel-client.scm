(define-module (crates-io da ch dachannel-client) #:use-module (crates-io))

(define-public crate-dachannel-client-0.1.0 (c (n "dachannel-client") (v "0.1.0") (d (list (d (n "dachannel") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)))) (h "0khciqvlfbpqmqnf8l7mf6abmlhsdqvgjjqxhxf6yrcr3bhrl90v")))

