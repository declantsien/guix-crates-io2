(define-module (crates-io da gg dagger-rust) #:use-module (crates-io))

(define-public crate-dagger-rust-0.1.0 (c (n "dagger-rust") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "dagger-sdk") (r "^0.2.22") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (d #t) (k 0)))) (h "0731pa7q5h4lws2rk45qpjs7bs6mr8lp0ap4gxbir78akpgaw62w")))

(define-public crate-dagger-rust-0.2.0 (c (n "dagger-rust") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "dagger-sdk") (r "^0.2.22") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (d #t) (k 0)))) (h "1wfmbvqghp73lzmisrh3135q76qxyykbvyc6428n07lfzgrmki23")))

