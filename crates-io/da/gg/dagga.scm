(define-module (crates-io da gg dagga) #:use-module (crates-io))

(define-public crate-dagga-0.1.0 (c (n "dagga") (v "0.1.0") (d (list (d (n "dot2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "1xfhpsihbgy5v77mins1kncr5pnmj2bcjqz5brpx89j7fzw7q8j0") (f (quote (("dot" "dot2") ("default" "dot"))))))

(define-public crate-dagga-0.1.1 (c (n "dagga") (v "0.1.1") (d (list (d (n "dot2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "1cplm7dvvj52gc8fjly52182lz8l72jwa05ddfwn512i0pzi4skl") (f (quote (("dot" "dot2") ("default" "dot"))))))

(define-public crate-dagga-0.2.0 (c (n "dagga") (v "0.2.0") (d (list (d (n "dot2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "086k1mm9l7ywyizpj8pcdbdivmw45ax4zabzb720n0ld88irwz7f") (f (quote (("dot" "dot2") ("default" "dot"))))))

(define-public crate-dagga-0.2.1 (c (n "dagga") (v "0.2.1") (d (list (d (n "dot2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "0s3bd0hi1j81dab65cicksphibqzbp1kfxr7h5frqz1hrmyhvkq4") (f (quote (("dot" "dot2") ("default" "dot"))))))

