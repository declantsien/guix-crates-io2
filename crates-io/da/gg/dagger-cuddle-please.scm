(define-module (crates-io da gg dagger-cuddle-please) #:use-module (crates-io))

(define-public crate-dagger-cuddle-please-0.1.0 (c (n "dagger-cuddle-please") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "dagger-sdk") (r "^0.2.22") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "0br0r3485m50b89wdmn5ssd2sknmlqx01894mkvyncixhwgb99y9")))

(define-public crate-dagger-cuddle-please-0.2.0 (c (n "dagger-cuddle-please") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "dagger-sdk") (r "^0.2.22") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "17jc0pvf57pidzl6jm1hdszy4lkry0lfpm4rp42z0h8kav2zfwp1")))

