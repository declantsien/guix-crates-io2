(define-module (crates-io da gg daggy) #:use-module (crates-io))

(define-public crate-daggy-0.1.0 (c (n "daggy") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.1.1") (d #t) (k 0)))) (h "1scjgysa733g3l7i8zwis25gcdia7hbjn0z8vsiir672sdfr1b26")))

(define-public crate-daggy-0.1.1 (c (n "daggy") (v "0.1.1") (d (list (d (n "petgraph") (r "^0.1.1") (d #t) (k 0)))) (h "1gn5g1mm3f5v4amhn7g59ifvlg3dc1z1mk3g0jwidmzrd0q7nw5y")))

(define-public crate-daggy-0.1.2 (c (n "daggy") (v "0.1.2") (d (list (d (n "petgraph") (r "^0.1.1") (d #t) (k 0)))) (h "1jd0a628lxdfs78fj7f8083dabzhgjr2y54yasgsz0k8sj33645f")))

(define-public crate-daggy-0.1.3 (c (n "daggy") (v "0.1.3") (d (list (d (n "petgraph") (r "^0.1.1") (d #t) (k 0)))) (h "041nln2s59a6b050h567rjizhzwfz9sr2817dqbgg47a7hk2v8vi")))

(define-public crate-daggy-0.1.4 (c (n "daggy") (v "0.1.4") (d (list (d (n "petgraph") (r "^0.1.1") (d #t) (k 0)))) (h "1nksy99x8idicb5jp9i9zfwfyg1909ixa3s5m910mkr7j977vb7z")))

(define-public crate-daggy-0.2.0 (c (n "daggy") (v "0.2.0") (d (list (d (n "petgraph") (r "^0.1.1") (d #t) (k 0)))) (h "09mb3b3ingjnciida883xahbaj34shjz1kfj1pjcx4vc3apix3b6")))

(define-public crate-daggy-0.2.1 (c (n "daggy") (v "0.2.1") (d (list (d (n "petgraph") (r "^0.1.1") (d #t) (k 0)))) (h "08v83x85i9qi12lrkbwq7ij1bw7bx0l7gs3jw83xvfmdhmannxx0")))

(define-public crate-daggy-0.3.0 (c (n "daggy") (v "0.3.0") (d (list (d (n "petgraph") (r "^0.1.15") (d #t) (k 0)))) (h "0piks395siiqrnj4v95kppvw942mkhbx7j3zyyvz3pk590nda4id")))

(define-public crate-daggy-0.4.0 (c (n "daggy") (v "0.4.0") (d (list (d (n "petgraph") (r "^0.2.2") (d #t) (k 0)))) (h "0yybw5nlajrqlr99b6g6byqrskv5rfsvqg54l7sp3wi1nw6x9yb2")))

(define-public crate-daggy-0.4.1 (c (n "daggy") (v "0.4.1") (d (list (d (n "petgraph") (r "^0.2.2") (d #t) (k 0)))) (h "0scwzfsyy317vgbma58cf7j48xh8279p45s39y43w8hxq4dbdafn")))

(define-public crate-daggy-0.5.0 (c (n "daggy") (v "0.5.0") (d (list (d (n "petgraph") (r "^0.4.5") (k 0)))) (h "1qh9ckpbdvqzxgjh1nrd63kpp83rvvwxkr7cj00g7h8vgpda14wj")))

(define-public crate-daggy-0.6.0 (c (n "daggy") (v "0.6.0") (d (list (d (n "petgraph") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0hc8ly75x2d8vq9a5hi3jjplkryd674875gn58jpp3a1fpq9w2g2") (f (quote (("serde-1" "petgraph/serde-1" "serde"))))))

(define-public crate-daggy-0.7.0 (c (n "daggy") (v "0.7.0") (d (list (d (n "petgraph") (r "^0.5") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1cyc9wy8dd06vrzy92lh02n1yvaj5kami3ps3q2fl6g4z1rpc3n9") (f (quote (("stable_dag" "petgraph/stable_graph") ("serde-1" "petgraph/serde-1" "serde"))))))

(define-public crate-daggy-0.8.0 (c (n "daggy") (v "0.8.0") (d (list (d (n "petgraph") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "00kn3spyjq04rypnavqh1scc1m86ajlappp4kaih3mp9am731aci") (f (quote (("stable_dag" "petgraph/stable_graph") ("serde-1" "petgraph/serde-1" "serde"))))))

