(define-module (crates-io da gg dagger_layout) #:use-module (crates-io))

(define-public crate-dagger_layout-0.1.0 (c (n "dagger_layout") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "glium") (r "^0.34") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.13") (d #t) (k 0)) (d (n "vek") (r "^0.17") (d #t) (k 0)))) (h "0jwnrrk7cvgrahd5jr2ijrjl49w2s4cfbzflfdxzpanfhp06c4ka") (s 2) (e (quote (("glium" "dep:glium"))))))

