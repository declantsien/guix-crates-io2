(define-module (crates-io da gg dagger_macros) #:use-module (crates-io))

(define-public crate-dagger_macros-0.1.0 (c (n "dagger_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0z1w1vplm1w7xx0j8i4fj9hlvl5zkq3y3bwvbfbi41hzr8n1anb7")))

