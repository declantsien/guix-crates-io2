(define-module (crates-io da gg dagger) #:use-module (crates-io))

(define-public crate-dagger-0.1.0 (c (n "dagger") (v "0.1.0") (d (list (d (n "dagger_layout") (r "^0.1") (f (quote ("glium"))) (d #t) (k 0)) (d (n "dagger_macros") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.34") (d #t) (k 0)) (d (n "glutin") (r "^0.31") (d #t) (k 0)) (d (n "vek") (r "^0.17") (d #t) (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 0)))) (h "037b2qj9hvxi7jwvb6iln69r590qkfm3349hsqxj5j6jfs95gcl4")))

