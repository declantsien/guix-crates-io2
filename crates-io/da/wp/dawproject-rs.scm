(define-module (crates-io da wp dawproject-rs) #:use-module (crates-io))

(define-public crate-dawproject-rs-0.1.0 (c (n "dawproject-rs") (v "0.1.0") (h "1c818xkcvlgiavlwglirg9xjq428xxi529xcmqw5widnqzixjjhl") (y #t)))

(define-public crate-dawproject-rs-0.1.1 (c (n "dawproject-rs") (v "0.1.1") (d (list (d (n "cargo-limit") (r "^0.0.10") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "fake") (r "^2.9.2") (f (quote ("derive" "always-true-rng"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)) (d (n "xml") (r "^0.8.10") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0chnr200gb6wcnvzm9y23xlsqpj6xc3vx2f95gidcgsn1vpai11x")))

