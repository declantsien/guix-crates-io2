(define-module (crates-io da g- dag-cbor) #:use-module (crates-io))

(define-public crate-dag-cbor-0.1.0 (c (n "dag-cbor") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "libipld-base") (r "^0.1.0") (d #t) (k 0)) (d (n "multihash") (r "^0.10.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1lnjy1f8xkcapvvz70yfq5hkilpk7s0766ycvywf28j9przl8zvw")))

