(define-module (crates-io da g- dag-cbor-derive) #:use-module (crates-io))

(define-public crate-dag-cbor-derive-0.1.0 (c (n "dag-cbor-derive") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.23") (d #t) (k 2)))) (h "1rpmn0cixg4zbqwijkafwzkbzhsnvv8p7cf6h5qf9bvgmshmpvdl")))

