(define-module (crates-io da g- dag-pb) #:use-module (crates-io))

(define-public crate-dag-pb-0.1.0 (c (n "dag-pb") (v "0.1.0") (d (list (d (n "libipld-base") (r "^0.1.0") (d #t) (k 0)) (d (n "multihash") (r "^0.10.1") (d #t) (k 2)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0vnvlwripa164z6fj276m91n88102gqw58dal5ayiac574prc568")))

