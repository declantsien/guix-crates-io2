(define-module (crates-io da t- dat-network-protocol) #:use-module (crates-io))

(define-public crate-dat-network-protocol-0.1.1 (c (n "dat-network-protocol") (v "0.1.1") (d (list (d (n "protobuf") (r "^1.4.3") (d #t) (k 0)) (d (n "protoc-rust") (r "^1.4") (d #t) (k 1)))) (h "03i8yznjwgn7c250gzjr5cx431lbjbc87wcdjp503sji6f0nc1d9")))

(define-public crate-dat-network-protocol-0.1.2 (c (n "dat-network-protocol") (v "0.1.2") (d (list (d (n "protobuf") (r "^1.4.3") (d #t) (k 0)) (d (n "protoc-rust") (r "^1.4") (d #t) (k 1)))) (h "06wcysrj135cxa0j2nvk3ncbm80shzvd7riv97sg3pdk2dswlr81")))

(define-public crate-dat-network-protocol-0.1.3 (c (n "dat-network-protocol") (v "0.1.3") (d (list (d (n "protobuf") (r "~1.5") (d #t) (k 0)) (d (n "protoc-rust") (r "~1.5") (d #t) (k 1)))) (h "04xjhi4pmwabn3kpinav7544jq5nphrmbsv27rw110zj0kmzac2k")))

(define-public crate-dat-network-protocol-0.1.4 (c (n "dat-network-protocol") (v "0.1.4") (d (list (d (n "protobuf") (r "~1.5") (d #t) (k 0)) (d (n "protoc-rust") (r "~1.5") (d #t) (k 1)))) (h "1rvnldkj5zy8mvk7kbhg1lfsx4x9vhq4iygzqfgwya0h6n1jizzm")))

(define-public crate-dat-network-protocol-0.2.0 (c (n "dat-network-protocol") (v "0.2.0") (d (list (d (n "protobuf") (r "= 2.10.1") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "= 2.10.1") (d #t) (k 1)))) (h "0m3pfgxw9rdd1k27dz9snhjmyzn2wi0hq5bzvk96s5pwkfniwk1v")))

(define-public crate-dat-network-protocol-0.3.0 (c (n "dat-network-protocol") (v "0.3.0") (d (list (d (n "protobuf") (r "= 2.10.1") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "= 2.10.1") (d #t) (k 1)))) (h "1sk5vswpzinm1c68fbnw81jri3lic5baqqb7l3g87h05sl4qa4dq")))

