(define-module (crates-io da kv dakv_cuckoo) #:use-module (crates-io))

(define-public crate-dakv_cuckoo-0.1.0 (c (n "dakv_cuckoo") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "seahash") (r "^4.0.0") (d #t) (k 0)))) (h "1pd1dd5nlvmg55ggwcc5rkvfw2c1ybnnpalicpq2215wln0560cr")))

(define-public crate-dakv_cuckoo-0.1.1 (c (n "dakv_cuckoo") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "seahash") (r "^4.0.0") (d #t) (k 0)))) (h "05phs3x534rv6izfd49byfnhg5jwhws3ngd5hnzz1npgm0ckrixl")))

