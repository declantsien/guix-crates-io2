(define-module (crates-io da kv dakv_skiplist) #:use-module (crates-io))

(define-public crate-dakv_skiplist-0.1.2 (c (n "dakv_skiplist") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "0c6wph66a33bmzkgrbjcizc3n2wqyxbasqylbvljdrvkmk7vf35m") (y #t)))

(define-public crate-dakv_skiplist-0.1.3 (c (n "dakv_skiplist") (v "0.1.3") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1brpxmh74n6a8l0wk2c4fwy44j789i3xjab9pi6jb820nv13j2c6")))

