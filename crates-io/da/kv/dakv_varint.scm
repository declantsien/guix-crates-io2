(define-module (crates-io da kv dakv_varint) #:use-module (crates-io))

(define-public crate-dakv_varint-0.1.0 (c (n "dakv_varint") (v "0.1.0") (h "1fzbzcxnxd9zslaryhs0gaw6703q1l26n5hmz8xxz58a7az3yvab")))

(define-public crate-dakv_varint-0.1.1 (c (n "dakv_varint") (v "0.1.1") (h "16yinzli0whsl2bz1absf9vhwl90b9jfl4dl6rsn8vaa81d21gqs")))

