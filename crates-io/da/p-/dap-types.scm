(define-module (crates-io da p- dap-types) #:use-module (crates-io))

(define-public crate-dap-types-0.0.0 (c (n "dap-types") (v "0.0.0") (h "128n4wj7d4m8wm4hznb1qdaqgdwvfzgr97kakyjr7mlbdh584d5q") (y #t)))

(define-public crate-dap-types-0.0.1 (c (n "dap-types") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0fckmknzzi4qkp5vl72kkygg4bvap9hr8f439llwmvhbxmifriks")))

