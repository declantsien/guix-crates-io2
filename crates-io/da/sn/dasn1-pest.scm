(define-module (crates-io da sn dasn1-pest) #:use-module (crates-io))

(define-public crate-dasn1-pest-0.1.0 (c (n "dasn1-pest") (v "0.1.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "1h7gdl32dl2ydq2sxhgjf9nwqp8224lw3xgd28rcmi4p321lvbpl")))

(define-public crate-dasn1-pest-0.1.1 (c (n "dasn1-pest") (v "0.1.1") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "0g575vb1cgrlkpxnpqfg6zmjxk6k2jn7dblrbyy1l7cha4nmhfyl")))

