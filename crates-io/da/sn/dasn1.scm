(define-module (crates-io da sn dasn1) #:use-module (crates-io))

(define-public crate-dasn1-0.1.0 (c (n "dasn1") (v "0.1.0") (d (list (d (n "core") (r "^0.1") (d #t) (k 0) (p "dasn1-core")) (d (n "der") (r "^0.1") (o #t) (d #t) (k 0) (p "dasn1-der")) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "notation") (r "^0.1") (o #t) (d #t) (k 0) (p "dasn1-notation")))) (h "1bpabsa48x1qv1yy4355alzd5hmkysvgqvl0yz6cc60lrx7h90m9")))

(define-public crate-dasn1-0.1.1 (c (n "dasn1") (v "0.1.1") (d (list (d (n "core") (r "^0.1.1") (d #t) (k 0) (p "dasn1-core")) (d (n "der") (r "^0.1.1") (o #t) (d #t) (k 0) (p "dasn1-der")) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "notation") (r "^0.1.1") (o #t) (d #t) (k 0) (p "dasn1-notation")))) (h "0bn7ww8qn3iar5yy4pdxl5a53bg3rwdgbpp7mw67xc0i8h2kihq7")))

