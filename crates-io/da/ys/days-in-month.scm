(define-module (crates-io da ys days-in-month) #:use-module (crates-io))

(define-public crate-days-in-month-0.1.0 (c (n "days-in-month") (v "0.1.0") (d (list (d (n "leap-year") (r "^0.1") (d #t) (k 0)))) (h "0c036a8j8ywmz7fsnbhqzzvfpw18jzd5gkw3mhcwmwbzhjq3am25")))

(define-public crate-days-in-month-1.0.0 (c (n "days-in-month") (v "1.0.0") (d (list (d (n "leap-year") (r "^0.1") (d #t) (k 0)))) (h "0clw9zq9lg6lx04zwdg1afvd0cwrgs7208gmb25cvzdfgy1s63xd")))

(define-public crate-days-in-month-2.0.0 (c (n "days-in-month") (v "2.0.0") (d (list (d (n "leap-year") (r "^0.1") (d #t) (k 0)))) (h "1wg1w7x1brnn810y6iwz1fwmjz0my60zd5piqv6cb6vpqmnyafh0")))

