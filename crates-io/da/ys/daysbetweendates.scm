(define-module (crates-io da ys daysbetweendates) #:use-module (crates-io))

(define-public crate-daysbetweendates-0.1.0 (c (n "daysbetweendates") (v "0.1.0") (h "170vak2dpb0f07w1lbqj8f22a5g5y3w1xx47gxghh5vx59frpd23")))

(define-public crate-daysbetweendates-0.1.1 (c (n "daysbetweendates") (v "0.1.1") (h "1cn7x0ks660s3iwky7y4jlsvqh63v9r0pc0ci0zfr0w2cvzcmdr7")))

(define-public crate-daysbetweendates-0.1.2 (c (n "daysbetweendates") (v "0.1.2") (h "0w48pp5yx9jw16bn9qfiv58867hs8ld2y6lf0y43kcp1mqaxawxi")))

