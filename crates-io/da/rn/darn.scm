(define-module (crates-io da rn darn) #:use-module (crates-io))

(define-public crate-darn-0.1.0 (c (n "darn") (v "0.1.0") (d (list (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)))) (h "038lq8mgb6afcswaxj0sac6jnmy8jyk73140qv15lbipv2xs64yn")))

(define-public crate-darn-0.1.1 (c (n "darn") (v "0.1.1") (d (list (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)))) (h "1kmvcacqq5xi44i63hjvdk2dgardn6yvx82r26ynid0hf5vh8p3w")))

(define-public crate-darn-0.1.2 (c (n "darn") (v "0.1.2") (d (list (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)))) (h "1rnk9xgnis9g4qm81gsm828mlnacqxvpfcx7i2al09739g6k218j")))

(define-public crate-darn-0.1.3 (c (n "darn") (v "0.1.3") (d (list (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)))) (h "1arr7ighy9a0qwz0ksy2dr6p5z5wx3wjj2fwzb6nylhsnfhphk4z")))

(define-public crate-darn-0.1.4 (c (n "darn") (v "0.1.4") (d (list (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)))) (h "1r8nasfjk7akb3sl4h5ns7fvffpqlf66lbq7p8b0fkqr0nj98yci")))

(define-public crate-darn-0.1.5 (c (n "darn") (v "0.1.5") (d (list (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)))) (h "0mr1a118gdd71ycxainna9imkjw0niyb2ycf1jyhrw24swjxx4ja")))

(define-public crate-darn-0.1.6 (c (n "darn") (v "0.1.6") (d (list (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)))) (h "091jkbwiw05i5gpd1ms02xrxsdhq9hh4p60r4zl4zshacbq8mhp2")))

(define-public crate-darn-0.1.7 (c (n "darn") (v "0.1.7") (d (list (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)))) (h "136wc7xkgpbhybzssfq4w49vwf4mn1m7zfq0l3iqrpy666d61kg8")))

(define-public crate-darn-0.1.8 (c (n "darn") (v "0.1.8") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "16krgzin4qaig5dfl10zgbc4cb831pm1l345q1yabl9lxcddj7wz")))

(define-public crate-darn-0.1.9 (c (n "darn") (v "0.1.9") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "1ii98f9npdp1g7s0m42qxhdx5a0196nbwi8sjx2nx38mywfdvy11")))

(define-public crate-darn-0.1.10 (c (n "darn") (v "0.1.10") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "09snd973kwsasifssy7h3xb8ypnd8ylgc4817138036kc3gkc3qd")))

(define-public crate-darn-0.1.11 (c (n "darn") (v "0.1.11") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "07c36g6v2fy5381lvj2a423n04ailf2v25dljlsnnrbpw01z09ps")))

(define-public crate-darn-0.1.12 (c (n "darn") (v "0.1.12") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "1qwhm044c021pabx84ccvgaw3agyz6bs6x3dp4p91k64vrlzhp8q")))

(define-public crate-darn-0.1.13 (c (n "darn") (v "0.1.13") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "15jc7911pxdas4pjlk4vq0ich58qcxdbkx94200bkgzf1ww7hxzi")))

(define-public crate-darn-0.1.14 (c (n "darn") (v "0.1.14") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "1dkxbrxxqyqvw88ncp5d54270gg5dpm5xz7x9mfimzixgsykq24r")))

(define-public crate-darn-0.1.15 (c (n "darn") (v "0.1.15") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "1k8cg5yifwx24hjk89vh0f489xpfpmnxq5czb7dpmd5339jyqaq1")))

(define-public crate-darn-0.2.0 (c (n "darn") (v "0.2.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "1xp97an851ggny7ws3vid2k6yx5h9vhh838v2magp9y4ml1jikkj")))

(define-public crate-darn-0.3.0 (c (n "darn") (v "0.3.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "1fyfb0q9ydhvv7288s27z05blnlf49dg1pz25y1gs9hlf87g9m6z")))

(define-public crate-darn-0.3.1 (c (n "darn") (v "0.3.1") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "17y50ji8yrd8rw8gy1h2ca2kj19qdw6gkyc854qwnjliidq477zx")))

(define-public crate-darn-0.3.2 (c (n "darn") (v "0.3.2") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "1j1fpkbpkvdicf80a76p560rwndhwfq9cnz3kjgbw409526q20jn")))

(define-public crate-darn-0.3.3 (c (n "darn") (v "0.3.3") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "0jng4vajbbc10jsvpi5p2zj4vvlm7xki60k7bma738h6111v5h19")))

(define-public crate-darn-0.3.4 (c (n "darn") (v "0.3.4") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "047rbkjcw64bx76k1kmdr6dsxczhii3khj0lkjznirjm8jpq9yhc")))

