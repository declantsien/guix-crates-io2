(define-module (crates-io da go dagon) #:use-module (crates-io))

(define-public crate-dagon-0.0.1 (c (n "dagon") (v "0.0.1") (h "048855cjaw302nlrp6l7gy0w1wsx86h9926w2nyr5sb9yiaph5z6")))

(define-public crate-dagon-0.0.2 (c (n "dagon") (v "0.0.2") (d (list (d (n "acacia") (r "^0.1") (d #t) (k 0)) (d (n "clippy") (r "^0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "herbie-lint") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r ">= 0.9, < 1.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.6") (d #t) (k 0)) (d (n "rmp-serialize") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0dx8ma0fda9aaznwnsr28klr9qrvlhxakp5ld26hhvpp4ssgc7bj") (f (quote (("default"))))))

