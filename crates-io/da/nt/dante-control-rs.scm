(define-module (crates-io da nt dante-control-rs) #:use-module (crates-io))

(define-public crate-dante-control-rs-0.1.0 (c (n "dante-control-rs") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mdns-sd") (r "^0.7.4") (d #t) (k 0)))) (h "0mkd08ggwi64qbyz37g4g2gi6zj05jf1q3a4qc1qggcjz4bah2xc")))

(define-public crate-dante-control-rs-0.2.0 (c (n "dante-control-rs") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mdns-sd") (r "^0.7.4") (d #t) (k 0)))) (h "1y99arh982pd1lvczd501wwf897zfis12zzanr1k3syr520b2vy6")))

(define-public crate-dante-control-rs-0.3.0 (c (n "dante-control-rs") (v "0.3.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mdns-sd") (r "^0.7.4") (d #t) (k 0)))) (h "04k6qf622mip0hgbskbx6wkd8fdgkdqg5kiajryngjd7scbpxyi9")))

(define-public crate-dante-control-rs-0.4.0 (c (n "dante-control-rs") (v "0.4.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mdns-sd") (r "^0.7.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1358k33w92za5wnirq3sq39010kv6lq5zxfbw3mnmzlgziq5xjc5")))

(define-public crate-dante-control-rs-0.5.0 (c (n "dante-control-rs") (v "0.5.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mdns-sd") (r "^0.7.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "13c203q56bvwdmcrf8kqlmj47jy1m14f7xlhvn7spx9ix21qxxqg")))

(define-public crate-dante-control-rs-0.6.0 (c (n "dante-control-rs") (v "0.6.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mdns-sd") (r "^0.7.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0f8hd5453blz9n5dn8ncyy9c08255vj82yisn402s01yflx78xd7")))

(define-public crate-dante-control-rs-0.7.0 (c (n "dante-control-rs") (v "0.7.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mdns-sd") (r "^0.7.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "13yww7hppgf39m17hc3q9xndxqj4h421z9mnfn4h0jibh24c01k7")))

(define-public crate-dante-control-rs-0.8.0 (c (n "dante-control-rs") (v "0.8.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mdns-sd") (r "^0.7.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "13nghvgppj385lzv6fmxjqcsjs7jj590sxnly885cqij2sixvalh")))

(define-public crate-dante-control-rs-0.8.1 (c (n "dante-control-rs") (v "0.8.1") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mdns-sd") (r "^0.7.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0l5xnbhg38s9ih7b5f0w3ac54n041q251p755iah1ynkr3hlvvz6")))

(define-public crate-dante-control-rs-0.8.2 (c (n "dante-control-rs") (v "0.8.2") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mdns-sd") (r "^0.7.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "06f5jvl3bh1f4nsjij6nacn5z902k75f5pxgz8x6f6amr977kq5r")))

