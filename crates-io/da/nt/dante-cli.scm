(define-module (crates-io da nt dante-cli) #:use-module (crates-io))

(define-public crate-dante-cli-0.1.0 (c (n "dante-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dante-control-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)))) (h "18b7qim6l8ndcvmn5x3blyd4a5xzd4slnhfadx3a33pcns3h35cp")))

(define-public crate-dante-cli-0.2.0 (c (n "dante-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dante-control-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)))) (h "1y54cprlidq25lwvlbc5nkjf767w959av7ggvdispdzfpjar39cm")))

(define-public crate-dante-cli-0.2.1 (c (n "dante-cli") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dante-control-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)))) (h "1sjap3kcwzj32ypndaj3dqzqrg35bh5wj75qxr0w97h40rs5vy9l")))

(define-public crate-dante-cli-0.3.0 (c (n "dante-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dante-control-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)))) (h "16xd5i0dl43lnkwa8jkkvk8m2wa5zxhjpz5qc74mnayk1karxp28")))

(define-public crate-dante-cli-0.4.0 (c (n "dante-cli") (v "0.4.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dante-control-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "shellwords") (r "^1.1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0v10ambpmgkqchlp8qxchc1mb3kzdfxih7k8gqkc80nddwijr3vs")))

(define-public crate-dante-cli-0.5.0 (c (n "dante-cli") (v "0.5.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dante-control-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "shellwords") (r "^1.1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1ywjzjh8y10ldbrixpp2vgc24v9zdzmbwchyy06rhix5ql0lz85c")))

(define-public crate-dante-cli-0.6.0 (c (n "dante-cli") (v "0.6.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dante-control-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "shellwords") (r "^1.1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1pbdz40b4d4kpp26aj8yakffxl3h82xvxvpzj199lnv42gxffqpk")))

(define-public crate-dante-cli-0.6.1 (c (n "dante-cli") (v "0.6.1") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dante-control-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "shellwords") (r "^1.1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0i2r8xh2xw8nirklk52ig4fgp7in4qglrsni9aqy063nk40rbfxh")))

(define-public crate-dante-cli-0.6.2 (c (n "dante-cli") (v "0.6.2") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dante-control-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "shellwords") (r "^1.1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "12p8jihfx6jwlk62ixcdl4absxq8svdsaal8mzmibwsppxi39l5g")))

(define-public crate-dante-cli-0.7.0 (c (n "dante-cli") (v "0.7.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dante-control-rs") (r "^0.7.0") (d #t) (k 0)) (d (n "shellwords") (r "^1.1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1b4zrqvpggn0rhlc5gmz3299cyf8982c9ri88gacv1m5qhgnzhgq")))

(define-public crate-dante-cli-0.8.0 (c (n "dante-cli") (v "0.8.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dante-control-rs") (r "^0.8.1") (d #t) (k 0)) (d (n "shellwords") (r "^1.1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0i2ynfky7i7sizr3k2rbgikzkmw84mv691qvr7jbmk0zr77s0185")))

(define-public crate-dante-cli-0.8.1 (c (n "dante-cli") (v "0.8.1") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dante-control-rs") (r "^0.8.1") (d #t) (k 0)) (d (n "shellwords") (r "^1.1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0i0wwazx7ar7lsnkn8jrz398nxdw9d1rb3sxv5pkjmv6d69skwcg")))

