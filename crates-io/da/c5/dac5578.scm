(define-module (crates-io da c5 dac5578) #:use-module (crates-io))

(define-public crate-dac5578-0.1.0 (c (n "dac5578") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)))) (h "06lrz580mja92h69qjq42vccfii80690yfb874c0nwm55bpc4j49")))

(define-public crate-dac5578-0.1.1 (c (n "dac5578") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)))) (h "1p66bxp6j99c9672bxzx6dba04r0krxgmsylbz6dic9nlfwclzij")))

(define-public crate-dac5578-0.2.0 (c (n "dac5578") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)))) (h "03iqpf7mi5qvhhmkz246qq76iliainhz9y1j2al9imdw9ilxcnlg")))

(define-public crate-dac5578-0.2.1 (c (n "dac5578") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)))) (h "060szqhdjwmdpq3dzq8g44cgmhxjhpg6j4qq45adawr7hy4q57rw")))

