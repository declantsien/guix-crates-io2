(define-module (crates-io da ik daikin_altherma) #:use-module (crates-io))

(define-public crate-daikin_altherma-0.0.1 (c (n "daikin_altherma") (v "0.0.1") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tungstenite") (r "^0.21.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1302hc0mqcw9zx9237qf9bmwkv20l0bm1sh3h5wqrsx8ibpvhx31")))

(define-public crate-daikin_altherma-0.0.2 (c (n "daikin_altherma") (v "0.0.2") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tungstenite") (r "^0.21.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ifjwxhlx2jc4pmjifn3yi2l8qmg0dzkysz1mii1h0rf0sjb3pgn")))

