(define-module (crates-io da ik daikon-stl) #:use-module (crates-io))

(define-public crate-daikon-stl-0.1.0 (c (n "daikon-stl") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)))) (h "0ig5fdrkvvq48839lmshv9js98bjyxf4szxmq6gq5md4372dk8pc")))

