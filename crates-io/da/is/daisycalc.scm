(define-module (crates-io da is daisycalc) #:use-module (crates-io))

(define-public crate-daisycalc-0.2.8 (c (n "daisycalc") (v "0.2.8") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)))) (h "14z0d20314gx6535madj0xgjplzq9p5368l3fkdi7zjv5zb9xnfx")))

(define-public crate-daisycalc-0.2.9 (c (n "daisycalc") (v "0.2.9") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)))) (h "11hklfmcf34c9j1afp8zhk477znh2p7l2kf7ax28wpf82x0s0dx8")))

(define-public crate-daisycalc-0.2.10 (c (n "daisycalc") (v "0.2.10") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)))) (h "0s8y6flbq13f5gkc1dpfdfmcigxrsky7q4i0x3mspjyw0glpp5nc")))

(define-public crate-daisycalc-0.2.11 (c (n "daisycalc") (v "0.2.11") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)))) (h "0gk3ijqp2pr3fjbikfnicg08rskn8b79mfilin84nv13mc1fayxi")))

(define-public crate-daisycalc-0.2.12 (c (n "daisycalc") (v "0.2.12") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)))) (h "1wal577rdhxn811dspx7swdgxc6ykrfs1714gajnklw54m4v420v")))

(define-public crate-daisycalc-0.2.13 (c (n "daisycalc") (v "0.2.13") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)))) (h "18gxpf7wzbl3rs8s3j3hhjqa9z6band1k5fs9sj3dcnnz49q2jhi")))

(define-public crate-daisycalc-0.2.14 (c (n "daisycalc") (v "0.2.14") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)))) (h "02fvakz26p0idpgcirjn9hmjvd8h9sgdgx44rfz7j2hz4sgwkbfj")))

(define-public crate-daisycalc-0.2.15 (c (n "daisycalc") (v "0.2.15") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)))) (h "1xy5chhrsqm7h4jyhqh6nnyd7s27pdz12wiimii1kcwv7nijx88h")))

(define-public crate-daisycalc-0.2.16 (c (n "daisycalc") (v "0.2.16") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)))) (h "1an4d79myzczr4blsp5mjyl4a3g95w4698bpqa0xcss6ljd317dw")))

(define-public crate-daisycalc-1.0.0 (c (n "daisycalc") (v "1.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)))) (h "05xic1qnp5qpby2ag5l0vixkiwycgyz6nyysd411i20x4jcpijkw")))

(define-public crate-daisycalc-1.0.1 (c (n "daisycalc") (v "1.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)))) (h "1yb2mmgdfcz1qknzs0mznhxsknj5gr69hmr1c86949rklhs3fmcp")))

(define-public crate-daisycalc-1.1.0 (c (n "daisycalc") (v "1.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)))) (h "08hgz9k1479a74430j5gi5h9qw0ijzygyna0mzxr1pj0bvl7ril0")))

(define-public crate-daisycalc-1.1.2 (c (n "daisycalc") (v "1.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1sz6hgx9ajgwhkgz9ng92av4lfijrcnk9xcqjzh8h4rm0mayhr23")))

(define-public crate-daisycalc-1.1.3 (c (n "daisycalc") (v "1.1.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0s1ri0vnjrj41lypq170fy0gcv76n0c9npw44zlg782nlgxpvwz8")))

(define-public crate-daisycalc-1.1.4 (c (n "daisycalc") (v "1.1.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1pjx6ml8sa2c1279sgdcxi2yrpa685iighkg604w11pd2ihl4hpi")))

(define-public crate-daisycalc-1.1.5 (c (n "daisycalc") (v "1.1.5") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1b8zlkz4nv7qaq1z0d0523c9d47j37xbdvvsnmbcckj0kjs4xz3n")))

(define-public crate-daisycalc-1.1.6 (c (n "daisycalc") (v "1.1.6") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0cvav32r65b30rdkw8ij27xbvyrsjg050frlr97zb36irbih3sfx")))

(define-public crate-daisycalc-1.1.7 (c (n "daisycalc") (v "1.1.7") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0l3f3hsnlvv03cm8j0cjr3ff8vxqqskq036gb790aa9bw2rf3xrq")))

