(define-module (crates-io da wg dawg) #:use-module (crates-io))

(define-public crate-dawg-0.0.1 (c (n "dawg") (v "0.0.1") (d (list (d (n "config") (r "^0.13.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.2") (f (quote ("offline" "runtime-tokio-native-tls" "uuid" "macros" "postgres" "migrate" "json"))) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0rbx96i4hx4yxfswlkjspgqrcq2jzykxng1bgsbckwh7kbimlxvv") (f (quote (("threading"))))))

(define-public crate-dawg-0.0.2 (c (n "dawg") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "113n5s81ciy7cxk36bvv2bx3l4q2g3h8v29xgjrgkhggw39fixbq") (f (quote (("threading")))) (y #t)))

(define-public crate-dawg-0.0.3 (c (n "dawg") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "05gcxp1lj1jmw4yw1wpyqqq5d5qykxlyxh1q4340hrjcpwf2rvn4") (f (quote (("threading"))))))

(define-public crate-dawg-0.0.4 (c (n "dawg") (v "0.0.4") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "03s4zrdsn3sryvim85p53594glhciigd5f4yvq82sd4pgzm4gmaq") (f (quote (("threading"))))))

(define-public crate-dawg-0.0.5 (c (n "dawg") (v "0.0.5") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1y0y59g5kzqi0d7g14qs6z2h2ahyz23lc1mhx3w2qs2qnd5a0fii") (f (quote (("threading"))))))

