(define-module (crates-io da wr dawr) #:use-module (crates-io))

(define-public crate-dawr-0.0.1 (c (n "dawr") (v "0.0.1") (d (list (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "portaudio") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0d5jdb5gpl8hnsqhzgz3hc82bhqvh40f9lspmn4n6gdyjmnq2cqs")))

(define-public crate-dawr-0.0.2 (c (n "dawr") (v "0.0.2") (d (list (d (n "hound") (r "^3.4") (d #t) (k 0)) (d (n "portaudio") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0mj8hwl2d7a5394mgifabi2vl289jgwhqcva3shyg5hp56wz83xn")))

