(define-module (crates-io da li dali) #:use-module (crates-io))

(define-public crate-dali-0.2.0 (c (n "dali") (v "0.2.0") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "luminance") (r "^0.33.0") (d #t) (k 0)) (d (n "luminance-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "luminance-glfw") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "12gzivn4sg60imda1ijawx8hr1hxxjfy1vji8grb704y7d9wxsgh")))

(define-public crate-dali-0.2.1 (c (n "dali") (v "0.2.1") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "luminance") (r "^0.34") (d #t) (k 0)) (d (n "luminance-derive") (r "^0.2") (d #t) (k 0)) (d (n "luminance-glfw") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "054204kym7z1c38inxphp4grmv8g26j8lyqkbdvr2xckdxz23kh0")))

(define-public crate-dali-0.2.2 (c (n "dali") (v "0.2.2") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "luminance") (r "^0.34") (d #t) (k 0)) (d (n "luminance-derive") (r "^0.2") (d #t) (k 0)) (d (n "luminance-glfw") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 2)))) (h "1igdyjkfvvkpn7z5x3lj77hagpvlm9jfi6kvi3irr16sl1p34a7f")))

(define-public crate-dali-0.2.3 (c (n "dali") (v "0.2.3") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "luminance") (r "^0.35") (d #t) (k 0)) (d (n "luminance-derive") (r "^0.3") (d #t) (k 0)) (d (n "luminance-glfw") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 2)))) (h "14zrx604mmdbrpjpq69yi4268v1323xmn35df7i013jnrfgv0cj2")))

(define-public crate-dali-0.3.0 (c (n "dali") (v "0.3.0") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "imageproc") (r "^0.19.2") (d #t) (k 2)) (d (n "luminance") (r "^0.37.1") (d #t) (k 0)) (d (n "luminance-derive") (r "^0.5") (d #t) (k 0)) (d (n "luminance-glfw") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 2)))) (h "1v3lks2s14jwznm85ik08ii93131fywvrr53jjnzdmzmx79gdmnl")))

