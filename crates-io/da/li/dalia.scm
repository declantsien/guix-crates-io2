(define-module (crates-io da li dalia) #:use-module (crates-io))

(define-public crate-dalia-1.0.0 (c (n "dalia") (v "1.0.0") (d (list (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)))) (h "14sf8b5ihc99asmq7m25494k2xm3nkq1p041mwp7sypm0af66a1h")))

(define-public crate-dalia-1.0.1 (c (n "dalia") (v "1.0.1") (d (list (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)))) (h "0sp9k93mf387p7zgqsih2ksxrn38whjqcf7v19n6zc4a87z8shvp")))

(define-public crate-dalia-1.0.2 (c (n "dalia") (v "1.0.2") (d (list (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)))) (h "0yhr1a9lsan6lvv3rxsazipc0046v8f6h0zff8c18li8dh2capbl")))

(define-public crate-dalia-1.1.0 (c (n "dalia") (v "1.1.0") (d (list (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2") (d #t) (k 2)))) (h "1i7bwh0bk4vhcvd6wz624h29xhrw461ql630c9lbi9qnngazaml8")))

(define-public crate-dalia-1.1.1 (c (n "dalia") (v "1.1.1") (d (list (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2") (d #t) (k 2)))) (h "1rnk301z5n11gf0fk4d6dhq1xq5ln86fwmwsh7w7lmjwb9pjbzy8")))

(define-public crate-dalia-1.2.0 (c (n "dalia") (v "1.2.0") (d (list (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2") (d #t) (k 2)))) (h "1i3yah5imz8mkja83gygi480xsx6fbps60lsaidccg432kkig00b")))

