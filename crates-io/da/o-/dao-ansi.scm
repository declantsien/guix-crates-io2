(define-module (crates-io da o- dao-ansi) #:use-module (crates-io))

(define-public crate-dao-ansi-0.1.0 (c (n "dao-ansi") (v "0.1.0") (h "0vz9dfvy4li9s30p5dp9kizmkc61mzn6jf6mbp08fh7n4rd2dp1b")))

(define-public crate-dao-ansi-0.1.1 (c (n "dao-ansi") (v "0.1.1") (h "1my1dwzm6a12i1hqdwl6mwr93fddyq6ap8cj7lmyb86ykvcm0yi8")))

(define-public crate-dao-ansi-0.1.2 (c (n "dao-ansi") (v "0.1.2") (h "03m9ygqnvbli4l6jak2w3cqnyyzq9z6agzafir8lmhkqcwn5iz08")))

(define-public crate-dao-ansi-0.1.3 (c (n "dao-ansi") (v "0.1.3") (h "07wrf37x0f4wch9ifdixhkjzs66f5lk4zg2ixzb0nkl93jwliqhy")))

