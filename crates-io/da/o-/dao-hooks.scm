(define-module (crates-io da o- dao-hooks) #:use-module (crates-io))

(define-public crate-dao-hooks-2.3.0 (c (n "dao-hooks") (v "2.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-hooks") (r "^2.3.0") (d #t) (k 0)) (d (n "cw4") (r "^1.1") (d #t) (k 0)) (d (n "dao-voting") (r "^2.3.0") (d #t) (k 0)))) (h "0b2d7i7slnn1cq300h85h6gpfhs6fnfcpj973j4j6nb4lpfkzgxa")))

