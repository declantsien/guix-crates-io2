(define-module (crates-io da o- dao-dao-macros) #:use-module (crates-io))

(define-public crate-dao-dao-macros-2.1.0 (c (n "dao-dao-macros") (v "2.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 2)) (d (n "cw-hooks") (r "^2.0.1") (d #t) (k 2)) (d (n "dao-interface") (r "^2.1.0") (d #t) (k 2)) (d (n "dao-voting") (r "^2.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zv3rm7vzsn9xmj7j4c7hypzm1rpl8xcgrzp6117azmrrv4cy42h") (y #t)))

(define-public crate-dao-dao-macros-2.1.2 (c (n "dao-dao-macros") (v "2.1.2") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ihdzvsvc828njjyl9yb8x52qsg8z5qai87rk03b8484y50x4xi8") (y #t)))

(define-public crate-dao-dao-macros-2.1.3 (c (n "dao-dao-macros") (v "2.1.3") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x654wxcx5xiy0r4fr255kacd8nbbcvqvixp0x0l7fbf3a9iy8d6") (y #t)))

(define-public crate-dao-dao-macros-2.1.5 (c (n "dao-dao-macros") (v "2.1.5") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xp7rsxwgg3fir889bb2p0xziyq2b3aw06svvkjz5l9c4xs7fiiz") (y #t)))

(define-public crate-dao-dao-macros-2.2.0 (c (n "dao-dao-macros") (v "2.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13f2jyfk52zfdkaldp8r9kl31xrnyyyi6d82yazvcvwjgdbbgjsr")))

(define-public crate-dao-dao-macros-2.3.0 (c (n "dao-dao-macros") (v "2.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dcv2fr5lvr4xl37sk8g2ni7ifi4s24akmq11127rrly8qshfvmn")))

