(define-module (crates-io da o- dao-vote-hooks) #:use-module (crates-io))

(define-public crate-dao-vote-hooks-2.1.0 (c (n "dao-vote-hooks") (v "2.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-hooks") (r "^2.0.1") (d #t) (k 0)) (d (n "dao-voting") (r "^2.1.0") (d #t) (k 0)))) (h "1n9rf5v338mz3xa61jykaciv3ff84z0niiabdd1sgqsvmyw5rc1y") (y #t)))

(define-public crate-dao-vote-hooks-2.1.2 (c (n "dao-vote-hooks") (v "2.1.2") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-hooks") (r "^2.0.1") (d #t) (k 0)) (d (n "dao-voting") (r "^2.1.2") (d #t) (k 0)))) (h "0lc89x90rdn87cd8d0179hlhn30h11lprs6zd7q9ypc2vay773i3") (y #t)))

(define-public crate-dao-vote-hooks-2.1.3 (c (n "dao-vote-hooks") (v "2.1.3") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-hooks") (r "^2.1.3") (d #t) (k 0)) (d (n "dao-voting") (r "^2.1.3") (d #t) (k 0)))) (h "1jb1fwwgzglg4b4h2amwfr35xhdh4wyhg2pk0xr49liyphjwldh9") (y #t)))

(define-public crate-dao-vote-hooks-2.1.5 (c (n "dao-vote-hooks") (v "2.1.5") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-hooks") (r "^2.1.5") (d #t) (k 0)) (d (n "dao-voting") (r "^2.1.5") (d #t) (k 0)))) (h "0rac7fsymcxl658qn5n6wglavfwrqa9n4i4q5vn8hgsxyhshyxf3") (y #t)))

(define-public crate-dao-vote-hooks-2.2.0 (c (n "dao-vote-hooks") (v "2.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-hooks") (r "^2.2.0") (d #t) (k 0)) (d (n "dao-voting") (r "^2.2.0") (d #t) (k 0)))) (h "1yfq78ra45gllkn6rnnqb05i8hys264sxj3ad4fb0xvla4b2hndm")))

