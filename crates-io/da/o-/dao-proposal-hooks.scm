(define-module (crates-io da o- dao-proposal-hooks) #:use-module (crates-io))

(define-public crate-dao-proposal-hooks-2.1.0 (c (n "dao-proposal-hooks") (v "2.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-hooks") (r "^2.0.1") (d #t) (k 0)) (d (n "dao-voting") (r "^2.1.0") (d #t) (k 0)))) (h "0z0kzinnacgzwxcysgdyrwhag2p0dixjz9lqdal689sfm0kw39d8") (y #t)))

(define-public crate-dao-proposal-hooks-2.1.2 (c (n "dao-proposal-hooks") (v "2.1.2") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-hooks") (r "^2.0.1") (d #t) (k 0)) (d (n "dao-voting") (r "^2.1.2") (d #t) (k 0)))) (h "0vdjd47lzgynx35vj8vmps5p7jzfa72fsahqk2fr2kw445s2b5kc") (y #t)))

(define-public crate-dao-proposal-hooks-2.1.3 (c (n "dao-proposal-hooks") (v "2.1.3") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-hooks") (r "^2.1.3") (d #t) (k 0)) (d (n "dao-voting") (r "^2.1.3") (d #t) (k 0)))) (h "1zmjpni2db3ainjnwapyib8rdm7xmh86lcrl7llywwmr1wzgkj65") (y #t)))

(define-public crate-dao-proposal-hooks-2.1.5 (c (n "dao-proposal-hooks") (v "2.1.5") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-hooks") (r "^2.1.5") (d #t) (k 0)) (d (n "dao-voting") (r "^2.1.5") (d #t) (k 0)))) (h "19vb4v0am032snr27p5lidk5vnlfcdk6k4cryn17l0hylfpvqzx5") (y #t)))

(define-public crate-dao-proposal-hooks-2.2.0 (c (n "dao-proposal-hooks") (v "2.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-hooks") (r "^2.2.0") (d #t) (k 0)) (d (n "dao-voting") (r "^2.2.0") (d #t) (k 0)))) (h "0xxdzl7j6b6kknpbcd7rmfm9nkg55hd849xfaypf3646vmjvm0g0")))

