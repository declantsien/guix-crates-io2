(define-module (crates-io da o- dao-cw721-extensions) #:use-module (crates-io))

(define-public crate-dao-cw721-extensions-2.3.0 (c (n "dao-cw721-extensions") (v "2.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-controllers") (r "^1.1") (d #t) (k 0)) (d (n "cw4") (r "^1.1") (d #t) (k 0)))) (h "1il5agclbv4w6jsybx18376jl8sslaagp1iqcrf2p26chj3nwxwp")))

