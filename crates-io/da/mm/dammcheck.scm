(define-module (crates-io da mm dammcheck) #:use-module (crates-io))

(define-public crate-dammcheck-0.1.0 (c (n "dammcheck") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.19") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cxm77xp4784vvia827kqkcyvq7im3yxzrbbvgzckjic3747li92") (f (quote (("extension-module" "pyo3/extension-module") ("default" "extension-module"))))))

(define-public crate-dammcheck-0.1.1 (c (n "dammcheck") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.19") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "00946qbvwn09z4ygmmrxbpkiry7q2vpy280a67m6fxkixdd4rpv4") (f (quote (("extension-module" "pyo3/extension-module") ("default" "extension-module"))))))

(define-public crate-dammcheck-0.1.2 (c (n "dammcheck") (v "0.1.2") (d (list (d (n "pyo3") (r "^0.19") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mkxzficxc7jrvpgzhz83ih4wiy7rjpkk6b57pg94fy8sz0m13cj") (f (quote (("extension-module" "pyo3/extension-module") ("default" "extension-module"))))))

