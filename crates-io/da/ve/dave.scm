(define-module (crates-io da ve dave) #:use-module (crates-io))

(define-public crate-dave-0.1.0 (c (n "dave") (v "0.1.0") (h "021b2pm0hqjshygnf6a76nkr76539n8p97r2dn90an1jwfg2q2qj")))

(define-public crate-dave-0.1.1 (c (n "dave") (v "0.1.1") (d (list (d (n "hal9000") (r "^0.1.0") (d #t) (k 0)))) (h "0vzspbsyrmaq2rlw8xpszq2yz3amkz9mvj8a1ayfzgwri0pax42a")))

