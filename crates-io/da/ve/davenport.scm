(define-module (crates-io da ve davenport) #:use-module (crates-io))

(define-public crate-davenport-0.1.0 (c (n "davenport") (v "0.1.0") (h "00igvlvcliws19rjw8dcp382sz3hr7si8jcbc9vj8vjalbfpinsm")))

(define-public crate-davenport-0.1.1 (c (n "davenport") (v "0.1.1") (h "09qgm5yl2m2n15d10d4gy8b31ycb5fcszxa6bdvigy24w0znmmnz")))

