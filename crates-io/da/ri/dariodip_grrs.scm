(define-module (crates-io da ri dariodip_grrs) #:use-module (crates-io))

(define-public crate-dariodip_grrs-0.1.0 (c (n "dariodip_grrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "0m2nmaalqvrlw0n5vghn9sr7lz6ij3kaw5dgmpczgx4dpfl7hsmy")))

