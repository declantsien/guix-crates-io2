(define-module (crates-io da hl dahl-roxido) #:use-module (crates-io))

(define-public crate-dahl-roxido-0.1.0 (c (n "dahl-roxido") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2.0") (d #t) (k 0)))) (h "0dx5zbg16irc5vbxyxlbzgi19fi23bfnsf97qk6gw44y3g17kdyx") (y #t)))

(define-public crate-dahl-roxido-0.2.0 (c (n "dahl-roxido") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2.0") (d #t) (k 0)))) (h "0j0wyvp2y9jp8nqm7swr6s3irzhxgipxq8fxjl5mjkgqz79g3sir") (y #t)))

(define-public crate-dahl-roxido-0.3.2 (c (n "dahl-roxido") (v "0.3.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.3.0") (d #t) (k 0)))) (h "16ymrvsb6n8y0k88ggjwwal204r439qgddxbkk5s7wq6ihxn9n21") (y #t)))

(define-public crate-dahl-roxido-0.3.3 (c (n "dahl-roxido") (v "0.3.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.3.0") (d #t) (k 0)))) (h "0m84kq89lc80jv04q3m0k9zs4qfd0kk1pf05yngjmga91p97l1s5") (y #t)))

