(define-module (crates-io da hl dahl-randompartition) #:use-module (crates-io))

(define-public crate-dahl-randompartition-0.1.0 (c (n "dahl-randompartition") (v "0.1.0") (d (list (d (n "dahl-partition") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1xhdggyn8q9p5hnq2vwky296cqshhwq2w77370x17br7n5bknas8")))

