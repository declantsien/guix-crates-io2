(define-module (crates-io da hl dahl-bellnumber) #:use-module (crates-io))

(define-public crate-dahl-bellnumber-0.1.0 (c (n "dahl-bellnumber") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "1pg8842m26swy5hnbyiqcmj77jc8dfw3jyy5h4g4bhhy23970d3a")))

(define-public crate-dahl-bellnumber-0.1.1 (c (n "dahl-bellnumber") (v "0.1.1") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "06xqs8z1430xfyxdphkrrrmih85l00gz7dks04n5nlqdhjlbqk72")))

