(define-module (crates-io da hl dahl-partition) #:use-module (crates-io))

(define-public crate-dahl-partition-0.1.0 (c (n "dahl-partition") (v "0.1.0") (h "0dd9jcbpq6scaf4k4fvcwjf7p7y1ap06njycr2z54sfj45h7y9y2")))

(define-public crate-dahl-partition-0.2.0 (c (n "dahl-partition") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1gbwjw3cqps32ikwhhinkaww83yacfd03m3g7i7mjdr24c1li5mf")))

(define-public crate-dahl-partition-0.2.1 (c (n "dahl-partition") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "162izsb15gak8bsxgcaxiacn5azw20wjs046kyh72jb7vg76vadm")))

(define-public crate-dahl-partition-0.3.0 (c (n "dahl-partition") (v "0.3.0") (h "08kxkra3fyvkf2sxsb3lqczgs2fj1z28g01bbbm11wzjc4lalxk3")))

(define-public crate-dahl-partition-0.4.0 (c (n "dahl-partition") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1y1kzi61nlij4f366lswpxlww4bi5l4nqk080p38ak9r9cg2jbgx")))

(define-public crate-dahl-partition-0.5.0 (c (n "dahl-partition") (v "0.5.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0hv8x2zidw93iaf7xbd0jpb8jdhv9ff9bkh3frjjy6qvsd5iz824")))

(define-public crate-dahl-partition-0.5.1 (c (n "dahl-partition") (v "0.5.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0g7899iaxi9wr3cqhzyf3d7sjhij2jb2ga66qihx0zi6gcizw9lc")))

(define-public crate-dahl-partition-0.5.2 (c (n "dahl-partition") (v "0.5.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "06vf6wrymwxg1bzwhzp26s34pv0d6fn3597i67w9979n5ln8lw4j")))

(define-public crate-dahl-partition-0.5.3 (c (n "dahl-partition") (v "0.5.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "04n0y1915ih67ah1j3m20f2jjfz5z5kwaj5gyzpg9f8xk0fsjb2z")))

