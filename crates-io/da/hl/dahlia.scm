(define-module (crates-io da hl dahlia) #:use-module (crates-io))

(define-public crate-dahlia-1.0.0 (c (n "dahlia") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "17scg25l9vy3dldjqyscdfqp9qr7mzs66w9cr1r61z2jpvi099lm")))

(define-public crate-dahlia-1.1.0 (c (n "dahlia") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1pyj21v0flqg4b2fp66s1pxmhqkcy63jd0s5ip0bibga2d96i6qs")))

