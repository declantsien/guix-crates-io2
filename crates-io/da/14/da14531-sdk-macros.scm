(define-module (crates-io da #{14}# da14531-sdk-macros) #:use-module (crates-io))

(define-public crate-da14531-sdk-macros-0.1.0 (c (n "da14531-sdk-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "1906rh0adykq4wr5140gygd08mxjdd50ivl9fy68d84q160rrg1b") (r "1.60")))

(define-public crate-da14531-sdk-macros-0.1.1 (c (n "da14531-sdk-macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "1xzg6gg952158srqa92qqspgbchid5fjvjxp2s8pig3hsisdzp8d") (r "1.60")))

