(define-module (crates-io da #{14}# da14531) #:use-module (crates-io))

(define-public crate-da14531-0.1.0 (c (n "da14531") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "08hidxx6gz3cbfrn1mmryx3pk6dx64hzn8x32znxanxzd7ga171z") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-da14531-0.2.0 (c (n "da14531") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0h0bhh0pbqqin06c2gzmnsfbyh7l03m0i05fwp2s77qjrlz1qg5k") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-da14531-0.2.1 (c (n "da14531") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0xq3qpv4pn1hk5xqk7nqkaqx7g97zkbc4ssfr8d6cxppq6cc5hl4") (f (quote (("rt" "cortex-m-rt/device"))))))

