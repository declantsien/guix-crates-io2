(define-module (crates-io da #{14}# da14531-hal) #:use-module (crates-io))

(define-public crate-da14531-hal-0.2.2 (c (n "da14531-hal") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "da14531") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (k 0)))) (h "1n0m3dnxpsxkgjy3i7dwdpbmnfvc242k0rjk4fsnafmifi656sa7") (r "1.60")))

