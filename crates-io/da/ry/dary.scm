(define-module (crates-io da ry dary) #:use-module (crates-io))

(define-public crate-dary-0.1.1 (c (n "dary") (v "0.1.1") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)))) (h "1v8sw76c6z9p08w5cdskqgjcw6ncxaccr017c6ya2qg0dv3c9a4m")))

