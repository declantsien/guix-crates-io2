(define-module (crates-io da ry dary_heap) #:use-module (crates-io))

(define-public crate-dary_heap-0.1.0 (c (n "dary_heap") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1pg3zj75jrpawb2fcvd4n2sl007cgb2imi7x6zmk6jaz3yib3bkc") (f (quote (("trusted_len") ("shrink_to") ("retain") ("into_iter_sorted") ("extend_one") ("exact_size_is_empty") ("drain_sorted"))))))

(define-public crate-dary_heap-0.1.1 (c (n "dary_heap") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "0kdlicl99b63j1ny3yxcsy9dhijghr1hbm0vwb6flp3s1z9k0h9r") (f (quote (("unstable_nightly") ("unstable"))))))

(define-public crate-dary_heap-0.2.0 (c (n "dary_heap") (v "0.2.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "06hl1fswphs7lb1xyl0iswkyzhwvw4d9fwzz989bddbrmv21nlgx") (f (quote (("unstable_nightly") ("unstable"))))))

(define-public crate-dary_heap-0.2.1 (c (n "dary_heap") (v "0.2.1") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "14xpzyhw7j1ks3d8mfh0v7rsyy2c6gaw6n7hywjc1az8f47m1w0z") (f (quote (("unstable_nightly") ("unstable"))))))

(define-public crate-dary_heap-0.2.2 (c (n "dary_heap") (v "0.2.2") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1447zwxyksqrb2vyrz9lv1bf1c3qky1bfswls5k1zry5g4dv1h6p") (f (quote (("unstable_nightly") ("unstable"))))))

(define-public crate-dary_heap-0.2.3 (c (n "dary_heap") (v "0.2.3") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1bif095fsb1k9i37dbx184g9nvdh11h4c57nf8j1zsxsh4kmylic") (f (quote (("unstable_nightly") ("unstable"))))))

(define-public crate-dary_heap-0.3.0 (c (n "dary_heap") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1l2yzcpxdpirkdw8a0nvkp4mhzg0dgxkypjvnz8kv2zwq36l69cd") (f (quote (("unstable_nightly") ("unstable"))))))

(define-public crate-dary_heap-0.3.1 (c (n "dary_heap") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1nwwsdri0mbmkl9awcjzxmy9ya7mqc3c7qxwinry4533izmpc0xm") (f (quote (("unstable_nightly") ("unstable"))))))

(define-public crate-dary_heap-0.2.4 (c (n "dary_heap") (v "0.2.4") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1al6lhpzimy9wq8gmd7n93sycqixvby5f60z3y9hgqhhs2v3cv4y") (f (quote (("unstable_nightly") ("unstable"))))))

(define-public crate-dary_heap-0.3.2 (c (n "dary_heap") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0n38mi3z4y78kfsd1wn5wlsff6lc0472jw0hzd30mrm2m42g6yaj") (f (quote (("unstable_nightly") ("unstable") ("extra"))))))

(define-public crate-dary_heap-0.2.5 (c (n "dary_heap") (v "0.2.5") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1wqvl26nbfj1ss0dlksirbs5fqs4zad803xfzvkripsy5xk730cn") (f (quote (("unstable_nightly") ("unstable"))))))

(define-public crate-dary_heap-0.3.3 (c (n "dary_heap") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0ms04hvss297dbpzl69xd4jlm2avchwxx9q5h7qll1bgw729xzdd") (f (quote (("unstable_nightly") ("unstable") ("extra"))))))

(define-public crate-dary_heap-0.3.4 (c (n "dary_heap") (v "0.3.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "00lrhkb7p5xwy097l07hs4fndz14hrypd6r1gf8hciv3qxscvvl2") (f (quote (("unstable_nightly") ("unstable") ("extra"))))))

(define-public crate-dary_heap-0.2.6 (c (n "dary_heap") (v "0.2.6") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1y1p72b84lqzl3r8mv4afw333r6gqw34g4fppkhlzr6hx75n2mpm") (f (quote (("unstable_nightly") ("unstable"))))))

(define-public crate-dary_heap-0.3.5 (c (n "dary_heap") (v "0.3.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0r4q6d73qp9pd3crrpfixxrd2xvz864gz8gafl2ds60ap8jdc8dq") (f (quote (("unstable_nightly") ("unstable") ("extra"))))))

(define-public crate-dary_heap-0.3.6 (c (n "dary_heap") (v "0.3.6") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1jm04p72s7xij3cr71h59dw07s63nah5b10sh8akcr2129zx2qkp") (f (quote (("unstable_nightly") ("unstable") ("extra"))))))

