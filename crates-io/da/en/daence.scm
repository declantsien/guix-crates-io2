(define-module (crates-io da en daence) #:use-module (crates-io))

(define-public crate-daence-0.0.0 (c (n "daence") (v "0.0.0") (d (list (d (n "chacha20") (r "^0.9.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.5") (d #t) (k 0)) (d (n "poly1305") (r "^0.7.2") (d #t) (k 0)) (d (n "subtle") (r "^2.4.1") (d #t) (k 0)) (d (n "typenum") (r "^1.15.0") (d #t) (k 0)) (d (n "universal-hash") (r "^0.4.1") (d #t) (k 0)))) (h "0zj762w5rgkajiy4n4rgx77czc97pich41ahwcxyjrj2z881bksw") (r "1.58")))

