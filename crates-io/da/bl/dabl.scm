(define-module (crates-io da bl dabl) #:use-module (crates-io))

(define-public crate-dabl-0.2.0 (c (n "dabl") (v "0.2.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "libdnscheck") (r "^0.2.0") (d #t) (k 0)))) (h "0wbljrc6xra5qsiwmjvqyan4bghmvdmlzzm04cwp0g12fd1xcy25")))

(define-public crate-dabl-0.3.0 (c (n "dabl") (v "0.3.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "libdnscheck") (r "^0.3.0") (d #t) (k 0)))) (h "0hgj91qhzkk5jgzxfqigz9cqg96a5xda396ai7w6a1ibd44brxdl")))

(define-public crate-dabl-0.4.0 (c (n "dabl") (v "0.4.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "libdnscheck") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "~0.3") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "1lz6lm8if324lifgrhvl3nh45dal60khb02qvz5hig60sndxabyx") (f (quote (("resolved" "libdnscheck/resolved") ("default" "resolved"))))))

(define-public crate-dabl-0.5.0 (c (n "dabl") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libdnscheck") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "1xszx1v26aq9pz52wcdwcg6q2fky8xcqcfa7zqbay8kn4p4f10r7") (f (quote (("resolved" "libdnscheck/resolved"))))))

(define-public crate-dabl-0.5.1 (c (n "dabl") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libdnscheck") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "0i8w9q653ly2kamymqwqbkndiqrdp6iibmv0km8xjzh79zqgax8r") (f (quote (("resolved" "libdnscheck/resolved"))))))

