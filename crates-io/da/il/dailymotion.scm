(define-module (crates-io da il dailymotion) #:use-module (crates-io))

(define-public crate-dailymotion-0.0.1 (c (n "dailymotion") (v "0.0.1") (d (list (d (n "oauth2") (r "^4.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0an9263kf2fbk335r7ckymwciwr2vsy62r7pwlxxdmv5hxc7i4gl")))

(define-public crate-dailymotion-0.0.2 (c (n "dailymotion") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "oauth2") (r "^4.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1n8cmafypv96zimm5x2rpfh2lqg10pwryv2icms08pvb83afybqv")))

