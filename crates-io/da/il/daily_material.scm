(define-module (crates-io da il daily_material) #:use-module (crates-io))

(define-public crate-daily_material-0.1.0 (c (n "daily_material") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "teloxide") (r "^0.9") (f (quote ("macros" "auto-send"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0xajra9al5vn53dk2dhwndm91sdas0yv3gy5wkz19y762gls3brr")))

(define-public crate-daily_material-1.0.0 (c (n "daily_material") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "teloxide") (r "^0.9") (f (quote ("macros" "auto-send"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1n7mllj2xlbvqjggq1bkqmgf2dyr8ichjqd2hi36d538hsik3m7g")))

