(define-module (crates-io da il daily-bread) #:use-module (crates-io))

(define-public crate-daily-bread-0.1.0 (c (n "daily-bread") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "00f2glsvyvbclm006x7fk5mlv1an7vb08l9w5s206nplzlm1k33r")))

(define-public crate-daily-bread-0.1.1 (c (n "daily-bread") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "directories") (r "^2.0.2") (d #t) (k 0)))) (h "0aq0m91z3bzanp013fzfaiqn11kaccr7ina6dpiajf9i0jazygnn")))

