(define-module (crates-io da il daily) #:use-module (crates-io))

(define-public crate-daily-0.0.1 (c (n "daily") (v "0.0.1") (h "0j5bzlll7mc8lpd6wrgk3rnd9l9wil1gywpvvlzpnkx1nl7qq92y") (y #t)))

(define-public crate-daily-0.0.2 (c (n "daily") (v "0.0.2") (h "0fk5zj3akbbdxizamp4y5m8qjg0q4mhp54mpy9lp48r351l5amdi") (y #t)))

