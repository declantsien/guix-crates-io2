(define-module (crates-io da ic daicon-native) #:use-module (crates-io))

(define-public crate-daicon-native-0.1.0 (c (n "daicon-native") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "daicon") (r "^0.10.0") (d #t) (k 0)) (d (n "stewart") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1n11izwbpzcad7qgqldrilnl11y1i8sg067lvmc3za9bv6vxdyxr")))

(define-public crate-daicon-native-0.2.0 (c (n "daicon-native") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "daicon") (r "^0.11.0") (d #t) (k 0)) (d (n "stewart") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0is2fbiksmzyqgxqdh50bppsn0xy026450iriq35yiila5n0bsfh")))

