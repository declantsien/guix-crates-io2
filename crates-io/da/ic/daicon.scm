(define-module (crates-io da ic daicon) #:use-module (crates-io))

(define-public crate-daicon-0.1.0 (c (n "daicon") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)) (d (n "wrapmuck") (r "^0.1.0") (d #t) (k 0)))) (h "0hwg8zmxrhj4a3j5qgjr3lxfv08pb3h6bz18ggahzprx4zmigjw2")))

(define-public crate-daicon-0.2.0 (c (n "daicon") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "bytemuck") (r "^1.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)) (d (n "wrapmuck") (r "^0.2.0") (d #t) (k 0)))) (h "1wm2ky5jfaypdfq9k6bl7jd35a3bgza0fqpdzh8jab3kdi6gc91n")))

(define-public crate-daicon-0.3.0 (c (n "daicon") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "bytemuck") (r "^1.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "1pssibgim93nidwfyqpvarj29ya3j0hk04z8r3y6ayq1v5i5ippw")))

(define-public crate-daicon-0.4.0 (c (n "daicon") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "bytemuck") (r "^1.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "1r8q942y102bazg2b4sa8jimnw8a5g95a12c5d3j5yn3ff942bsb")))

(define-public crate-daicon-0.5.0 (c (n "daicon") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "bytemuck") (r "^1.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "1h25wa21sivyka8fgsfk76njv3b95nplmidsxiki7jmfazmxyb2m")))

(define-public crate-daicon-0.6.0 (c (n "daicon") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "bytemuck") (r "^1.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "0baxp50776c3pwz8lq5lx5aj3fbkdkg6jcr1f4gcmxfsbpk6zd74")))

(define-public crate-daicon-0.7.0 (c (n "daicon") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "bytemuck") (r "^1.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "0jjrq1i03b6fjpji2j50hifi93cbi4ha2qbgvg8ikx3gzaka0dw7")))

(define-public crate-daicon-0.8.0 (c (n "daicon") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "bytemuck") (r "^1.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "0wlkwp83n28pf8rbyx4x5d02xdazswfww3k205nsvc88vhqyq854")))

(define-public crate-daicon-0.9.0 (c (n "daicon") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "bytemuck") (r "^1.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "1m0xyqwady1msync1445va39hrgr4j7nw50ip2z7dbifyaw849c7")))

(define-public crate-daicon-0.10.0 (c (n "daicon") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "daicon-types") (r "^0.1.0") (d #t) (k 0)) (d (n "stewart") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "12rmaik8p7sp8zlxzz718l2w3ix7pqdl0zy5bhnngslh57kg0hc2")))

(define-public crate-daicon-0.11.0 (c (n "daicon") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "daicon-types") (r "^0.1.0") (d #t) (k 0)) (d (n "stewart") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1g9m7y82i3q8yjxd0c3viwyw2wk3da4rz487hs48xx62lpxf2pjh")))

