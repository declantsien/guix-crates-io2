(define-module (crates-io da ya dayan) #:use-module (crates-io))

(define-public crate-dayan-0.0.0 (c (n "dayan") (v "0.0.0") (d (list (d (n "latexify") (r "^0.0.1") (d #t) (k 0)) (d (n "pex") (r "^0.2.4") (d #t) (k 0)))) (h "1w5pyl69iki1ibhk4lfxy3fas7ki88y9h1rl2nrys7c01jh1v65c") (f (quote (("default"))))))

(define-public crate-dayan-0.0.1 (c (n "dayan") (v "0.0.1") (d (list (d (n "latexify") (r "^0.1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pex") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0dj40ngp5g7395wq7wvxqyfsl2x2j6wqxn24hz9nra6cn0fvwk4f") (f (quote (("default"))))))

