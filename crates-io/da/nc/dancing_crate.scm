(define-module (crates-io da nc dancing_crate) #:use-module (crates-io))

(define-public crate-dancing_crate-0.2.2 (c (n "dancing_crate") (v "0.2.2") (h "03arhlddlf8qx119nkk8asizhcxzwlgpz833i6skbyl6jd8xs8i7")))

(define-public crate-dancing_crate-0.2.3 (c (n "dancing_crate") (v "0.2.3") (h "0a6pcqvinkb55235yn1xqxaikfd6aa9h6x7vq6sfv95kxrvvslv7")))

(define-public crate-dancing_crate-0.0.2 (c (n "dancing_crate") (v "0.0.2") (h "0dfdw648ixlag30gvg465gdml8c1hvryqs76lp1zjb5mdvh4h7bl")))

(define-public crate-dancing_crate-0.2.4 (c (n "dancing_crate") (v "0.2.4") (h "1qmvyngxja8xbybpavnf8x4ncbgihymyhs58vpkqn5by0qa9v2rx")))

