(define-module (crates-io da nk dankboi) #:use-module (crates-io))

(define-public crate-dankboi-0.1.0 (c (n "dankboi") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.4") (d #t) (k 0)) (d (n "serenity") (r "^0.5.11") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "1cw3cpwmc446l3pl144adq780yv117mp6wafpgz6php9hwwvx7xp")))

(define-public crate-dankboi-0.1.1 (c (n "dankboi") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.4") (d #t) (k 0)) (d (n "serenity") (r "^0.5.11") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "0p2ich7zp5nz05kx2ry81yzirhn1a6fq7jkrwrqwlzk4l5drj82j")))

(define-public crate-dankboi-0.1.2 (c (n "dankboi") (v "0.1.2") (d (list (d (n "rand") (r "^0.6.4") (d #t) (k 0)) (d (n "serenity") (r "^0.5.11") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "01g4yhc5gjcyrwvx6hay555jiwhz2qx2mx4kpqkgmqyqjxkq4g4k")))

