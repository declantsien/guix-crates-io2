(define-module (crates-io da fu dafunk-core) #:use-module (crates-io))

(define-public crate-dafunk-core-0.1.0 (c (n "dafunk-core") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-tungstenite") (r "^0.19.0") (d #t) (k 2)))) (h "1w94r0iqypdw7ckaqg8bpdhnssrr06xghnd1l3s4fprkx0d5qrsc")))

(define-public crate-dafunk-core-0.1.1 (c (n "dafunk-core") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-tungstenite") (r "^0.19.0") (d #t) (k 2)))) (h "1h7ki7b41wby713m6yrbng3f741lisxk1jb7d59f3j7i7g2jkyv0")))

