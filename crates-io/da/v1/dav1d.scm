(define-module (crates-io da v1 dav1d) #:use-module (crates-io))

(define-public crate-dav1d-0.3.0 (c (n "dav1d") (v "0.3.0") (d (list (d (n "dav1d-sys") (r "^0.2.0") (d #t) (k 0)))) (h "11s4v21ala10vmy5ha9gx7pcm0y0am0621sl2klw7xzzl4w6p8xz")))

(define-public crate-dav1d-0.4.0 (c (n "dav1d") (v "0.4.0") (d (list (d (n "dav1d-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1j8wqk62kr3ki5cmc4k0bvq9pvaz4gjc76apgqrwy0rf623vk50r") (f (quote (("build" "dav1d-sys/build"))))))

(define-public crate-dav1d-0.5.0 (c (n "dav1d") (v "0.5.0") (d (list (d (n "dav1d-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0iz1z9pbnqnggqbrn6w7hx9q5z6khbgq64w1acab9wms1zidj87a") (f (quote (("build" "dav1d-sys/build"))))))

(define-public crate-dav1d-0.5.1 (c (n "dav1d") (v "0.5.1") (d (list (d (n "dav1d-sys") (r "^0.3.1") (d #t) (k 0)))) (h "1nq8fgjyk1qp0pqs5qgn38cj5zb1vypscacxfmsaj4xhlkrslh1n") (f (quote (("build" "dav1d-sys/build"))))))

(define-public crate-dav1d-0.5.2 (c (n "dav1d") (v "0.5.2") (d (list (d (n "dav1d-sys") (r "^0.3.2") (d #t) (k 0)))) (h "0bcjy00fs4vf8ni3jl2wsxqycs0vv6dw4fbksvpy5i1y1q5yfd7a") (f (quote (("build" "dav1d-sys/build"))))))

(define-public crate-dav1d-0.6.0 (c (n "dav1d") (v "0.6.0") (d (list (d (n "dav1d-sys") (r "^0.3.3") (d #t) (k 0)))) (h "0pn6r1a9qfrpg2xwc7ci2iddvnzxb17ddca0bwymgi839cxc2chl")))

(define-public crate-dav1d-0.6.1 (c (n "dav1d") (v "0.6.1") (d (list (d (n "dav1d-sys") (r "^0.3.5") (d #t) (k 0)))) (h "0qz2lx37pmx798lysgh6k5lk5y20ckr7pp8c1p6v2z0p721i913j")))

(define-public crate-dav1d-0.7.0 (c (n "dav1d") (v "0.7.0") (d (list (d (n "dav1d-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1rcgi6f3bcqs6gawxpn3myqb6mn536rjsj4i27sryaacmxflaccz")))

(define-public crate-dav1d-0.8.0 (c (n "dav1d") (v "0.8.0") (d (list (d (n "dav1d-sys") (r "^0.5.0") (d #t) (k 0)))) (h "12iashg7429zjzkh6fg85dd766c82q6jmx6zm8pnrm5p7zb9yrnr")))

(define-public crate-dav1d-0.9.0 (c (n "dav1d") (v "0.9.0") (d (list (d (n "dav1d-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0axph7xflqk89yks3mza1dydbygsj8q3a0vs8y6npm8zn4ld79m5")))

(define-public crate-dav1d-0.9.1 (c (n "dav1d") (v "0.9.1") (d (list (d (n "dav1d-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1i3834wh9p9v55wzy40z0ssl342sgk99vx7sw1scdqa4nr5hczcm")))

(define-public crate-dav1d-0.10.0 (c (n "dav1d") (v "0.10.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "dav1d-sys") (r "^0.8.0") (d #t) (k 0)))) (h "12pxwk3qyahrvlcl3sgsadv083wj8rscvlwcg11qxq44y8488009") (f (quote (("v1_1" "dav1d-sys/v1_1")))) (y #t)))

(define-public crate-dav1d-0.9.2 (c (n "dav1d") (v "0.9.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "dav1d-sys") (r "^0.8.0") (d #t) (k 0)))) (h "176cikf6313rd5r5bgahbik1w11sv60dr74a9jfdvv3v8azvshy3") (f (quote (("v1_1" "dav1d-sys/v1_1")))) (y #t)))

(define-public crate-dav1d-0.9.3 (c (n "dav1d") (v "0.9.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "dav1d-sys") (r "^0.7.1") (d #t) (k 0)))) (h "1cvv1fxc4jbqbii78p3m4mhzr0zrhqr5q663hpxrdmdw6w5b4an0") (f (quote (("v1_1" "dav1d-sys/v1_1"))))))

(define-public crate-dav1d-0.9.4 (c (n "dav1d") (v "0.9.4") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "dav1d-sys") (r "^0.7.1") (d #t) (k 0)))) (h "0xacd2hyr6fjwd7nq94jclsc2h6cm2va9nazdc433scfwp447jl7") (f (quote (("v1_1" "dav1d-sys/v1_1"))))))

(define-public crate-dav1d-0.9.5 (c (n "dav1d") (v "0.9.5") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "dav1d-sys") (r "^0.7.2") (d #t) (k 0)))) (h "057l2llgvqn44kwvjgmznzsqmw8n5jpdd9w7rsy8w4v0wgjzgs9s") (f (quote (("v1_1" "dav1d-sys/v1_1"))))))

(define-public crate-dav1d-0.9.6 (c (n "dav1d") (v "0.9.6") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "dav1d-sys") (r "^0.7.3") (d #t) (k 0)))) (h "1j5dpkqs07kq4mfyxa3m9z1lf897lrmqc8frzga83p0zx0x8bgln") (f (quote (("v1_1" "dav1d-sys/v1_1"))))))

(define-public crate-dav1d-0.10.1 (c (n "dav1d") (v "0.10.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "dav1d-sys") (r "^0.8.1") (d #t) (k 0)))) (h "0c827hzq86r1w51b2l774mia3yhxa7p3r4qfdca1znkw5s8rljxh")))

(define-public crate-dav1d-0.10.2 (c (n "dav1d") (v "0.10.2") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "dav1d-sys") (r "^0.8.2") (d #t) (k 0)))) (h "0c3h6w2yz2wks52lhmx19afldcyihv8zcgnly1gyhyjbjnlizsyp")))

(define-public crate-dav1d-0.10.3 (c (n "dav1d") (v "0.10.3") (d (list (d (n "av-data") (r "^0.4.2") (d #t) (k 0)) (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "dav1d-sys") (r "^0.8.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "1qd13sm1bfbc5chjgrzk4syffkky994lkyzhqrqklqxg1fj58jqd")))

