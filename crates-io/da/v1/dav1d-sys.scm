(define-module (crates-io da v1 dav1d-sys) #:use-module (crates-io))

(define-public crate-dav1d-sys-0.1.0 (c (n "dav1d-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.12") (d #t) (k 1)))) (h "1xqphls1wpnq28h1dlqp35k17isv1raypqlp3m7dl24xavwaci0c")))

(define-public crate-dav1d-sys-0.1.1 (c (n "dav1d-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.12") (d #t) (k 1)))) (h "1ark8i151fmcqz79x6iwvvs22v0j2m9hydsvmhrgnzjjwwn2bxyl")))

(define-public crate-dav1d-sys-0.1.2 (c (n "dav1d-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.12") (d #t) (k 1)))) (h "0lq3lqd63pcgc3sxr09r978k6pgx0v33lb9b03r7ckvnm012xnmv")))

(define-public crate-dav1d-sys-0.2.0 (c (n "dav1d-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.12") (d #t) (k 1)))) (h "1bam0h2wldricgk9di9s20ggf0cdmkvkqv9ksf92vp2ya667c3jx")))

(define-public crate-dav1d-sys-0.2.1 (c (n "dav1d-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.12") (d #t) (k 1)))) (h "1bcnzdiqzisqka88vdi7ik225qz8qmhr0634dd3g6y92jqvmcr04") (f (quote (("build"))))))

(define-public crate-dav1d-sys-0.3.0 (c (n "dav1d-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.12") (d #t) (k 1)))) (h "1022czzp3s54r42x6rhr870w1fwzyp7b6qn0zirpz55zmqjpgnwa") (f (quote (("build"))))))

(define-public crate-dav1d-sys-0.3.1 (c (n "dav1d-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.12") (d #t) (k 1)))) (h "13hd1r4hiai0hnhc6a9savgddk29ivnclny5fqz7sgb3nadkvsb1") (f (quote (("build"))))))

(define-public crate-dav1d-sys-0.3.2 (c (n "dav1d-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "1jdxhnlxcml6jd67lx78ifzkn1xm18zfk4li7vjdh3fa61i073kx") (f (quote (("build"))))))

(define-public crate-dav1d-sys-0.3.3 (c (n "dav1d-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "system-deps") (r "^2.0") (d #t) (k 1)))) (h "1pngmfsim56377xvwjaz62vawvvxj4jfnr63qp93gvl36asx82gi")))

(define-public crate-dav1d-sys-0.3.4 (c (n "dav1d-sys") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "system-deps") (r "^3") (d #t) (k 1)))) (h "020lla2l703iy69gbksq18snj2b1sp7vmjf39qqykd4242d4msr5")))

(define-public crate-dav1d-sys-0.3.5 (c (n "dav1d-sys") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "10y8637snqc3kb9mhs8p9zi8171ba2hlbvhk06vs6hfifx60rr48")))

(define-public crate-dav1d-sys-0.4.0 (c (n "dav1d-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "188pyql0s2xin0slnrkpcb7gyld8mz1ldrkqhgwjfnivmpr7zk5k")))

(define-public crate-dav1d-sys-0.5.0 (c (n "dav1d-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "00g57dn0x6igr1ljrlwifybkgqvz7r4dvj3y2h3inrlsxd1jnzi3")))

(define-public crate-dav1d-sys-0.6.0 (c (n "dav1d-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "12da79929a865cydik1h8bbbyzsfm6krwa8w68s6f3pnq5y4mhp6")))

(define-public crate-dav1d-sys-0.7.0 (c (n "dav1d-sys") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "16gzjsfnvfd5zr8mrx5n9mdd4vjvwfwpk9hfscgz7sjyzjdjzcm0")))

(define-public crate-dav1d-sys-0.8.0 (c (n "dav1d-sys") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "11r720fdnlq14hi1gcc2w11h9d8ys7pldd8768m4dda9lcbkh28z") (f (quote (("v1_1")))) (y #t)))

(define-public crate-dav1d-sys-0.7.1 (c (n "dav1d-sys") (v "0.7.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "0bgaaaiwjs3yykzzb420f66j2s11j1c25adb8rgpk2y12jxl4mb1") (f (quote (("v1_1"))))))

(define-public crate-dav1d-sys-0.7.2 (c (n "dav1d-sys") (v "0.7.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "1gi164rn93q6qpy5s2760lbdw1yz2plhrlmg06v749xkzlyphhs3") (f (quote (("v1_1"))))))

(define-public crate-dav1d-sys-0.7.3 (c (n "dav1d-sys") (v "0.7.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "system-deps") (r "^6.2") (d #t) (k 1)))) (h "13z5qvf35lkda67l6l1bkdp1gmqg75cqfblldxh4n8rbmn4zsj9s") (f (quote (("v1_1"))))))

(define-public crate-dav1d-sys-0.8.1 (c (n "dav1d-sys") (v "0.8.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "system-deps") (r "^6.2") (d #t) (k 1)))) (h "0ad50rjpz2h6r4grwbppdqadd4i2zli84lgxc4zgk3wv7a15s6m1")))

(define-public crate-dav1d-sys-0.8.2 (c (n "dav1d-sys") (v "0.8.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "system-deps") (r "^6.2") (d #t) (k 1)))) (h "158fqp97ny3206sydnimc2jy1c1gcxa4llqvvkp3ii2dixg1rjvf")))

