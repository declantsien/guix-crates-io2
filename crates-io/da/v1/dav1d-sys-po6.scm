(define-module (crates-io da v1 dav1d-sys-po6) #:use-module (crates-io))

(define-public crate-dav1d-sys-po6-0.1.0 (c (n "dav1d-sys-po6") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "po6") (r "^0.1.0") (d #t) (k 1)))) (h "07aq1xlydfnsjjbzdi594fwrskixjf4j7xqisvqjliwm3i550b90")))

(define-public crate-dav1d-sys-po6-0.1.1 (c (n "dav1d-sys-po6") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "po6") (r "^0.1.0") (d #t) (k 1)))) (h "1f03kjxdhw2qiswy0d88x9mnvyngagzamin0b6cj28ikvgw83xbw")))

(define-public crate-dav1d-sys-po6-0.1.2 (c (n "dav1d-sys-po6") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "po6") (r "^0.1.0") (d #t) (k 1)))) (h "04mf5x6j8fx3fp93n8wghb6xvria1p696wzify6fkz5rsnrlss2v")))

(define-public crate-dav1d-sys-po6-0.1.3 (c (n "dav1d-sys-po6") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "po6") (r "^0.1.0") (d #t) (k 1)))) (h "1kn9h6fjvnyv3nyl3na62xci7d3jfjc330ms5kx9n1lwy5mfslww")))

(define-public crate-dav1d-sys-po6-0.1.4 (c (n "dav1d-sys-po6") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "po6") (r "^0.1.2") (d #t) (k 1)))) (h "0y8n2zb1lf89cfa7rnkd069zqz6zbx0hb25azf688sjv0k3x54gc")))

(define-public crate-dav1d-sys-po6-0.1.5 (c (n "dav1d-sys-po6") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "po6") (r "^0.1.3") (d #t) (k 1)))) (h "0sdx214xh6ix55f7bjy8bbcgfv6yhmv1j8pcarahqpd3lljmmbqk")))

(define-public crate-dav1d-sys-po6-0.1.6 (c (n "dav1d-sys-po6") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "po6") (r "^0.1.5") (d #t) (k 1)))) (h "0l9rrn2162lzbqplml68yll982bpyy75n03f3zzq9hfxkrm7f0n8")))

(define-public crate-dav1d-sys-po6-0.1.7 (c (n "dav1d-sys-po6") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "po6") (r "^0.1.7") (d #t) (k 1)))) (h "1yxska8d1la7szv2k6ry7wi3zjpn1k4a5q0hz7gc3dc4dsnwp90a")))

