(define-module (crates-io da pr dapr-derive) #:use-module (crates-io))

(define-public crate-dapr-derive-0.1.0-alpha.1 (c (n "dapr-derive") (v "0.1.0-alpha.1") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1aarginpbmr8w7rkncpggh1y4bw6xb0mpczjrgmbhgzf6ckvk1b8")))

(define-public crate-dapr-derive-0.1.0-alpha.2 (c (n "dapr-derive") (v "0.1.0-alpha.2") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1dvavigz30zr64xz32vdakmd759fy7zq2k68bcqlbpbphrdgk9hw")))

