(define-module (crates-io da pr dapr-macros) #:use-module (crates-io))

(define-public crate-dapr-macros-0.14.0 (c (n "dapr-macros") (v "0.14.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1lj5b829aq58xy4arqz3rb4dsj8isx43il6rfckwx0ld0vsn2bi0")))

