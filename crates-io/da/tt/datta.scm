(define-module (crates-io da tt datta) #:use-module (crates-io))

(define-public crate-datta-0.1.0 (c (n "datta") (v "0.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1drp93rz1xbjh3ix3i49l5bqqs39dy9q4dk8jw62n8j0599226rv")))

(define-public crate-datta-0.1.1 (c (n "datta") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.0") (d #t) (k 0)))) (h "02k9cyndhga58srhhaq7bxfqsnfag8c55ikl9w03a7gh5ppbd5r8")))

