(define-module (crates-io da mn damndiff) #:use-module (crates-io))

(define-public crate-damndiff-0.1.0 (c (n "damndiff") (v "0.1.0") (h "1chp1k6sv0n2g9ai7b8njg0flh6l585xap31iyv16l6lnb971jw7")))

(define-public crate-damndiff-0.1.1 (c (n "damndiff") (v "0.1.1") (h "0x1wy9xz59ldhfs68layicm6q31vba0xcq3066rbqafasbl5v0md")))

(define-public crate-damndiff-0.1.2 (c (n "damndiff") (v "0.1.2") (h "0f1h3llazfjxh15l3qp4amkv40czw5qmmm2vccsrn2mrhi4by3ys")))

(define-public crate-damndiff-0.1.3 (c (n "damndiff") (v "0.1.3") (h "17fpkf1j06rwzfk39ddm5b8ivjklxarghzh9zl13vhmpvgrdjpvc")))

(define-public crate-damndiff-0.1.4 (c (n "damndiff") (v "0.1.4") (h "1fxgdmwk5f2i2y40fciv2jfsg2v52hvq9rd8aiiz2ah6dcracak9")))

(define-public crate-damndiff-0.1.5 (c (n "damndiff") (v "0.1.5") (h "0wfmrnhf27gvxh1qnbiwxb0g3vcsb56k37l7p08qzrirq4f1lpm7")))

(define-public crate-damndiff-0.1.6 (c (n "damndiff") (v "0.1.6") (h "01xsvh56bak3lghrvxxib5yka8h47wzx4diva4zpbdz6rxr88lcj")))

(define-public crate-damndiff-0.1.7 (c (n "damndiff") (v "0.1.7") (h "1lv52p21hcv9k0fwwj011x8lm13ph9kc791hdsdj0kg6gg06gb4d")))

