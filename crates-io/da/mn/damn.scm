(define-module (crates-io da mn damn) #:use-module (crates-io))

(define-public crate-damn-0.1.0 (c (n "damn") (v "0.1.0") (h "1k9wxz3l9aszzbyvrhr88n8z312953axy8hp6i259jr6vw758d1p")))

(define-public crate-damn-0.1.1 (c (n "damn") (v "0.1.1") (h "1arqaliq7gavw0lv18x1m2cagpkd0qcc91s41xlj3w8dix8p7grd")))

(define-public crate-damn-0.1.2 (c (n "damn") (v "0.1.2") (d (list (d (n "damn-core") (r "^0.1.2") (d #t) (k 0)))) (h "1w3kk85hgxlz5dqp1956rrfr1wc28xf531nc7q34dv6mplpnx0ni")))

