(define-module (crates-io da ml daml-macro) #:use-module (crates-io))

(define-public crate-daml-macro-0.1.0 (c (n "daml-macro") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "daml-grpc") (r "^0.1.0") (d #t) (k 0)))) (h "0w7qy88lfysya2pk4454lvx8xlm5n1q2r75aq61v105z64vkvx1s") (r "1.59.0")))

(define-public crate-daml-macro-0.1.1 (c (n "daml-macro") (v "0.1.1") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "daml-grpc") (r "^0.1.1") (d #t) (k 0)))) (h "1m16nanm6v7whw980v3sm6ha8arv73kf6fzqamk6rnbc54lqjmh4") (r "1.59.0")))

(define-public crate-daml-macro-0.2.0 (c (n "daml-macro") (v "0.2.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "daml-grpc") (r "^0.2.0") (d #t) (k 0)))) (h "1vvppwbcvfn9a39p731yyx77i3ckyriygv5pgw3pfrwc0lmx2qcp") (r "1.59.0")))

(define-public crate-daml-macro-0.2.1 (c (n "daml-macro") (v "0.2.1") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "daml-grpc") (r "^0.2.1") (d #t) (k 0)))) (h "1f9gzk8cazlr8cyd7dm032xk7d5hg4wq7w8xjxfm0m8wd2b5kx15") (r "1.59.0")))

(define-public crate-daml-macro-0.2.2 (c (n "daml-macro") (v "0.2.2") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "daml-grpc") (r "^0.2.2") (d #t) (k 0)))) (h "0gwbymkip8rq92b0mxb8w8vhmswp4yynqn6lxrwar578g4kzncn8") (r "1.59.0")))

