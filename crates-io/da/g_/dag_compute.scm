(define-module (crates-io da g_ dag_compute) #:use-module (crates-io))

(define-public crate-dag_compute-0.1.0 (c (n "dag_compute") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "small_rng"))) (k 2)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r ">=0.9.3, <0.10.0") (f (quote ("html_root_url_updated"))) (k 2)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "0x0gmhahf08y8i1llf8yc2k9qcjskjqdxhpdmn0drxc4rzzazmdp")))

