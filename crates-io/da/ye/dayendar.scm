(define-module (crates-io da ye dayendar) #:use-module (crates-io))

(define-public crate-dayendar-0.1.0 (c (n "dayendar") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "09rqgncz67aqp3impy8m8nynw3lvnw594h119fq6wv2acdfr5mz0") (y #t)))

(define-public crate-dayendar-0.1.1 (c (n "dayendar") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "0x4hh88x5cz0jcxr4zhjvp1i0jq55aw4h4glcd42nwp4bpwcdr20")))

(define-public crate-dayendar-0.1.2 (c (n "dayendar") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "grcov") (r "^0.8.19") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "0ryhr6fc8jpppfazxfs1ay24dgdyn3vgyjagg1dwsrkdm1h8mh3j") (f (quote (("testing"))))))

