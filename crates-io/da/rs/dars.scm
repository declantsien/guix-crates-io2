(define-module (crates-io da rs dars) #:use-module (crates-io))

(define-public crate-dars-0.1.0 (c (n "dars") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.10") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.6") (d #t) (k 0)) (d (n "ndarray-odeint") (r "^0.7") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.6") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "procedurals") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "170bj8r1cr5s29ny97sdnvjn7y9gn44d418aibsbdv4l80xi7w78")))

