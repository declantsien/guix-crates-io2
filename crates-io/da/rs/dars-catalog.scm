(define-module (crates-io da rs dars-catalog) #:use-module (crates-io))

(define-public crate-dars-catalog-0.1.0 (c (n "dars-catalog") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rust-embed") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "0ysj1576mzpfcc9q3halmncbj6509xshr9l0k87baj7ihzh738pj")))

