(define-module (crates-io da gl daglib) #:use-module (crates-io))

(define-public crate-daglib-0.1.0 (c (n "daglib") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0y41v0cl3c9h4a0jyrl4cm2ld144n8jdzml85xdq273kj77nmsx8")))

