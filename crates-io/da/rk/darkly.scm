(define-module (crates-io da rk darkly) #:use-module (crates-io))

(define-public crate-darkly-0.1.0 (c (n "darkly") (v "0.1.0") (d (list (d (n "darkly-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1rck48y1l6vqnwn0fq19sc23w2mmhq8vihkm6mr0032vihi2grzl")))

(define-public crate-darkly-0.2.0 (c (n "darkly") (v "0.2.0") (d (list (d (n "darkly-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0s87301mxzvv5f5gisaczihk185wp691zf9iky7nrjv4lmlcfp2n")))

