(define-module (crates-io da rk dark-crystal-secret-sharing-rust) #:use-module (crates-io))

(define-public crate-dark-crystal-secret-sharing-rust-0.1.0 (c (n "dark-crystal-secret-sharing-rust") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sharks") (r "^0.5.0") (d #t) (k 0)) (d (n "xsalsa20poly1305") (r "^0.8.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.3.0") (d #t) (k 0)))) (h "0wf7z4ywz4qwyznzan7cdsb38kiai2vd1wyzi25jyn6cc1r7m162")))

(define-public crate-dark-crystal-secret-sharing-rust-0.1.1 (c (n "dark-crystal-secret-sharing-rust") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sharks") (r "^0.5.0") (d #t) (k 0)) (d (n "xsalsa20poly1305") (r "^0.8.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.3.0") (d #t) (k 0)))) (h "17x5xhf38slzswl5flax64szl1klcndm3cqfi9sr8pm6p296wnpz")))

(define-public crate-dark-crystal-secret-sharing-rust-0.2.1 (c (n "dark-crystal-secret-sharing-rust") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sharks") (r "^0.5.0") (d #t) (k 0)) (d (n "xsalsa20poly1305") (r "^0.8.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.3.0") (d #t) (k 0)))) (h "1nwni3m686cmbl9ylay8g9mdqimyzi33hv4j7dpamscqapxk72b1")))

(define-public crate-dark-crystal-secret-sharing-rust-0.2.2 (c (n "dark-crystal-secret-sharing-rust") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sharks") (r "^0.5.0") (d #t) (k 0)) (d (n "xsalsa20poly1305") (r "^0.8.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.3.0") (d #t) (k 0)))) (h "0x7d0lxin7wnnsamfhmxwfpwwb9kjq46249b688sif9mvbp7qy0h")))

