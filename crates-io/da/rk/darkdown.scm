(define-module (crates-io da rk darkdown) #:use-module (crates-io))

(define-public crate-darkdown-0.1.0 (c (n "darkdown") (v "0.1.0") (h "1hhmk3pyqy3wq07gavk6lqs30s6pq8qmgpcm1dxa6lwnxd42xas0")))

(define-public crate-darkdown-0.1.1 (c (n "darkdown") (v "0.1.1") (h "0bwnp7v4f28a82ln5d726iardi1xfkp7fbiy3rb6531ibbinkxfz")))

(define-public crate-darkdown-0.1.2 (c (n "darkdown") (v "0.1.2") (h "0fn8yzq7x2kmnhy844aal5hdzksq12ii0pbs2i7lk8bn2vdh8w6v")))

(define-public crate-darkdown-0.1.3 (c (n "darkdown") (v "0.1.3") (h "0h6nqgvgy7gr20h30dbxqkgzvpwljpph7anpfnycc6pqbfm25a1w")))

(define-public crate-darkdown-0.1.4 (c (n "darkdown") (v "0.1.4") (h "00q9z01a5cpg05icy6cjyqi8ah75pcrh5n8j761i5srk12s842r3")))

(define-public crate-darkdown-0.1.5 (c (n "darkdown") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0shgl8427zdizwnzkqxybw45psmng7xpx25ipw2r1wx5zs3sz963")))

