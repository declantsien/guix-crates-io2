(define-module (crates-io da rk darknet-sys) #:use-module (crates-io))

(define-public crate-darknet-sys-0.1.0 (c (n "darknet-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "0sqnmxk2nr76jjfp86nmcsgvj7382s42xa2bk6dyqpwm6hayb3n0")))

(define-public crate-darknet-sys-0.1.1 (c (n "darknet-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "1bylphigpzrsnjyma05hhbwqrdgbq5fqy0fqziys0xssymmhk9nm")))

(define-public crate-darknet-sys-0.1.2 (c (n "darknet-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "1lym9lclyjph1ngfkwdmfvk04rjxbjgwj235292x9jy7mhgw6b2g")))

(define-public crate-darknet-sys-0.1.3 (c (n "darknet-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "1mxwbj26mhcvji4gfrw42255zbc6fpy8a9d1s38sjkk3rkv5s3gf") (y #t)))

(define-public crate-darknet-sys-0.1.4 (c (n "darknet-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "1sp5a04bshvpzdps6kry0hv030aa5k9998303b0f6z2xb5lshl7z")))

(define-public crate-darknet-sys-0.2.0 (c (n "darknet-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)))) (h "0wbvz78cjch5s4c26hl5mj35kav2vlwqwf439m15c221fs1vb16m") (f (quote (("runtime") ("buildtime-bindgen"))))))

(define-public crate-darknet-sys-0.2.1 (c (n "darknet-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)))) (h "1cips9d25hdhxiwzl7687g92mn81cm5x75d3adzjb2lx7mf126bi") (f (quote (("runtime") ("link-dynamic") ("buildtime-bindgen"))))))

(define-public crate-darknet-sys-0.2.2 (c (n "darknet-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)))) (h "1wy4mp1hrvsbbdsjm6jhcc6zrsxy2ynmj58rv5lmcdpcabqqgma0") (f (quote (("runtime") ("dylib") ("buildtime-bindgen"))))))

(define-public crate-darknet-sys-0.2.3 (c (n "darknet-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)))) (h "1dv27a1z68fparybk99j97z914gwhf02np9waq3lm1sdlv97y4by") (f (quote (("runtime") ("dylib") ("docs-rs") ("buildtime-bindgen"))))))

(define-public crate-darknet-sys-0.2.4 (c (n "darknet-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)))) (h "0gs0ggmnw2j4466i9s3sbflyfq8p57cfwq3cx83nxnk0lwjc9hha") (f (quote (("runtime") ("enable-opencv") ("enable-cuda") ("dylib") ("docs-rs") ("buildtime-bindgen"))))))

(define-public crate-darknet-sys-0.2.5 (c (n "darknet-sys") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)))) (h "1yl6qyvjighc621db6f0k11396xabhj8zc7j10h9hs3zmpy5al5g") (f (quote (("runtime") ("enable-opencv") ("enable-cuda") ("dylib") ("docs-rs") ("buildtime-bindgen"))))))

(define-public crate-darknet-sys-0.2.6 (c (n "darknet-sys") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)))) (h "0jalkm74fpadsprp228a9dryvsj6b16yhq599s6m1mg2iz676kr9") (f (quote (("runtime") ("enable-opencv") ("enable-cudnn") ("enable-cuda") ("dylib") ("docs-rs") ("buildtime-bindgen")))) (y #t)))

(define-public crate-darknet-sys-0.3.0 (c (n "darknet-sys") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)))) (h "18m2q3qwm2kr1bv8i9ivn9l1war6fv758laa7xy39bjw83fyar36") (f (quote (("runtime") ("enable-opencv") ("enable-cudnn") ("enable-cuda") ("dylib") ("docs-rs") ("buildtime-bindgen"))))))

(define-public crate-darknet-sys-0.3.1 (c (n "darknet-sys") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)))) (h "0l71pg744hl1v7idxv0nsb4p0a46gkxp1xwa9bi8wviwk4g4pxdd") (f (quote (("runtime") ("enable-opencv") ("enable-cudnn") ("enable-cuda") ("dylib") ("docs-rs") ("buildtime-bindgen"))))))

(define-public crate-darknet-sys-0.3.2 (c (n "darknet-sys") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)))) (h "0mn3ryl39x4amjg6yd44qcd9vw6443vv4h0s1hp9wnnxps33clvf") (f (quote (("runtime") ("enable-openmp") ("enable-opencv") ("enable-cudnn") ("enable-cuda") ("dylib") ("docs-rs") ("default" "enable-openmp") ("buildtime-bindgen"))))))

(define-public crate-darknet-sys-0.3.5 (c (n "darknet-sys") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)))) (h "02zmw4aqabgqdx9znqn166ya4z48ayhsvc7zd57xcy7hczrlqf1y") (f (quote (("runtime") ("enable-openmp") ("enable-opencv") ("enable-cudnn") ("enable-cuda") ("dylib") ("docs-rs") ("default" "enable-openmp") ("buildtime-bindgen"))))))

(define-public crate-darknet-sys-0.4.0 (c (n "darknet-sys") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)))) (h "1ph6j7gh1zidyxifgi4qf6dhav0ic395yr4p84j8d1569g0lfazg") (f (quote (("runtime") ("enable-openmp") ("enable-opencv") ("enable-cudnn") ("enable-cuda") ("dylib") ("docs-rs") ("default" "enable-openmp") ("buildtime-bindgen"))))))

