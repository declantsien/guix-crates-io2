(define-module (crates-io da rk darkness-check) #:use-module (crates-io))

(define-public crate-darkness-check-0.1.0 (c (n "darkness-check") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.13.2") (f (quote ("json" "toml"))) (d #t) (k 0)) (d (n "console") (r "^0.15.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("serde_json" "json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bhijlvs1zqgz18vj1dfbhazl5g172r3s9bx6wz7ry41dfwx8wvs")))

