(define-module (crates-io da rk darkly-macros) #:use-module (crates-io))

(define-public crate-darkly-macros-0.1.0 (c (n "darkly-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wm9m6k36g35wsl7vvax515mipxm73355mvgwfbav09ypymsaq5z")))

(define-public crate-darkly-macros-0.2.0 (c (n "darkly-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lk8fdzk7kw5xhm0alkvrn54dkbgbpqaz3mc6jz56ylzh6hwlf1k")))

