(define-module (crates-io da rk darktoken) #:use-module (crates-io))

(define-public crate-darktoken-0.0.5 (c (n "darktoken") (v "0.0.5") (d (list (d (n "bls12_381") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "06cxh1l1lc5bdwqfpz8kv79jjjvfwf5wpkazh8yn5xbnv8lbdgcl")))

(define-public crate-darktoken-0.0.6 (c (n "darktoken") (v "0.0.6") (d (list (d (n "bls12_381") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "dirs") (r "^2.0.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "16ygia16bmjljb9lz7zfbi9iikpbmw1wb60rxq3x5rdhmgfq0bir")))

