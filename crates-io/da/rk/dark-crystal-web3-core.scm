(define-module (crates-io da rk dark-crystal-web3-core) #:use-module (crates-io))

(define-public crate-dark-crystal-web3-core-0.1.0 (c (n "dark-crystal-web3-core") (v "0.1.0") (d (list (d (n "bip39") (r "^1.0.1") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.6") (d #t) (k 0)) (d (n "dark-crystal-key-backup-rust") (r "^0.2.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_qs") (r "^0.8.5") (d #t) (k 0)))) (h "01bvfbvmykwpw601ivqvvadjb1i2kk7r01zqjwkdcw5lxksw9zym")))

(define-public crate-dark-crystal-web3-core-0.1.1 (c (n "dark-crystal-web3-core") (v "0.1.1") (d (list (d (n "bip39") (r "^1.0.1") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.6") (d #t) (k 0)) (d (n "dark-crystal-key-backup-rust") (r "^0.2.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_qs") (r "^0.8.5") (d #t) (k 0)))) (h "1ls5hrfcjwjdcwmky3ddqdrz20gi4nr029m5z8iadml2fgm71vsc")))

