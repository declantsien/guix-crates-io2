(define-module (crates-io da rk dark-crystal-key-backup-rust) #:use-module (crates-io))

(define-public crate-dark-crystal-key-backup-rust-0.1.0 (c (n "dark-crystal-key-backup-rust") (v "0.1.0") (d (list (d (n "crypto_box") (r "^0.7.1") (d #t) (k 0)) (d (n "dark-crystal-secret-sharing-rust") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.3.0") (d #t) (k 0)))) (h "0kalvpbyp77x2v8kqbqdws67xx8vz50n1x2p600m9wdkqd894s7b")))

(define-public crate-dark-crystal-key-backup-rust-0.1.1 (c (n "dark-crystal-key-backup-rust") (v "0.1.1") (d (list (d (n "crypto_box") (r "^0.7.1") (d #t) (k 0)) (d (n "dark-crystal-secret-sharing-rust") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.3.0") (d #t) (k 0)))) (h "04k0dfgzkccxha5rs70z51jkh4759idffrzdm1cysk5g8clwzysv")))

(define-public crate-dark-crystal-key-backup-rust-0.1.2 (c (n "dark-crystal-key-backup-rust") (v "0.1.2") (d (list (d (n "crypto_box") (r "^0.7.1") (d #t) (k 0)) (d (n "dark-crystal-secret-sharing-rust") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.3.0") (d #t) (k 0)))) (h "1f0p0c63h872rc9waz309r7507j4awazycnpfc5715rpfs96c3z7")))

(define-public crate-dark-crystal-key-backup-rust-0.2.2 (c (n "dark-crystal-key-backup-rust") (v "0.2.2") (d (list (d (n "crypto_box") (r "^0.7.1") (d #t) (k 0)) (d (n "dark-crystal-secret-sharing-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.3.0") (d #t) (k 0)))) (h "0fivzm76g99d42cb601bsqx6ih5kd46gakcgx17sb8lvc31zpqzz")))

(define-public crate-dark-crystal-key-backup-rust-0.2.3 (c (n "dark-crystal-key-backup-rust") (v "0.2.3") (d (list (d (n "crypto_box") (r "^0.7.1") (d #t) (k 0)) (d (n "dark-crystal-secret-sharing-rust") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.3.0") (d #t) (k 0)))) (h "1ksxzcc5ax7ghhbwjjcdwvps2234p5cr4xqxrbwxwgnk0sd0ywqj")))

