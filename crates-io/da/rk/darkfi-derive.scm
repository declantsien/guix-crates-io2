(define-module (crates-io da rk darkfi-derive) #:use-module (crates-io))

(define-public crate-darkfi-derive-0.3.0 (c (n "darkfi-derive") (v "0.3.0") (d (list (d (n "darkfi-derive-internal") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "15kwkj2gz07k25p7ki038c72dam2m9szlsz8pgz5hn2mrscdqmwk")))

(define-public crate-darkfi-derive-0.4.1 (c (n "darkfi-derive") (v "0.4.1") (d (list (d (n "darkfi-derive-internal") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1qlijwc74ckpyvy4alb1m3x8wp42ddbqdcmg6nq2s0vvs5918vms")))

