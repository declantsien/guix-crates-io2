(define-module (crates-io da rk darkside) #:use-module (crates-io))

(define-public crate-darkside-1.0.0 (c (n "darkside") (v "1.0.0") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)))) (h "083spyxzaci5ddl831lq309899lqrqv25klvab6k510wi1gjs2rv")))

(define-public crate-darkside-1.0.1 (c (n "darkside") (v "1.0.1") (d (list (d (n "ncurses") (r "^5.99.0") (f (quote ("wide"))) (d #t) (k 0)))) (h "1900dnjm06sw895fw25dd3g5cyljwrpj8kmpzg699hqwnxbzgiwm")))

(define-public crate-darkside-1.1.0 (c (n "darkside") (v "1.1.0") (d (list (d (n "ncurses") (r "^5.99.0") (f (quote ("wide"))) (d #t) (k 0)))) (h "1iknpnay5digwkj1mnvrxwm01aqdazrp5rzq7c03jj86ik87j72m")))

(define-public crate-darkside-1.1.1 (c (n "darkside") (v "1.1.1") (d (list (d (n "ncurses") (r "^5.99.0") (f (quote ("wide"))) (d #t) (k 0)))) (h "1ybck0p8zab69x4xr9cxla5ns2nvl3jjgx3cx9h8msaynbcm3anc")))

(define-public crate-darkside-1.2.0 (c (n "darkside") (v "1.2.0") (d (list (d (n "ncurses") (r "^5.99.0") (f (quote ("wide"))) (d #t) (k 0)))) (h "1xk4lrqd6sxm0ajw8a97hbrf4bj3jss41fv3nbn0nl2bqknswm9c")))

(define-public crate-darkside-1.2.1 (c (n "darkside") (v "1.2.1") (d (list (d (n "ncurses") (r "^5.99.0") (f (quote ("wide"))) (d #t) (k 0)))) (h "13v1n0m8zm0vxzz4kg50kkqpl6fxx68hb96q6ib2i0bjdxvj9d2b")))

(define-public crate-darkside-1.2.2 (c (n "darkside") (v "1.2.2") (d (list (d (n "ncurses") (r "^5.99.0") (f (quote ("wide"))) (d #t) (k 0)))) (h "1c4k8z3y8zj81br25fzj2kxqqr1dng34wif406a90jgsjz61lx09")))

(define-public crate-darkside-1.2.3 (c (n "darkside") (v "1.2.3") (d (list (d (n "ncurses") (r "^5.99.0") (f (quote ("wide"))) (d #t) (k 0)))) (h "1pkknqpnbbn3xs96411cn43741rrdjv9492kbaiw1qy7h0dv1ykf")))

(define-public crate-darkside-1.2.4 (c (n "darkside") (v "1.2.4") (d (list (d (n "ncurses") (r "^5.99.0") (f (quote ("wide"))) (d #t) (k 0)))) (h "19ic93v4p525a3grz5gw94kvj1yh4whdhxp4jgpgpymsld95r216")))

(define-public crate-darkside-1.2.5 (c (n "darkside") (v "1.2.5") (d (list (d (n "ncurses") (r "^5.99.0") (f (quote ("wide"))) (d #t) (k 0)))) (h "1avyraa0dy1pxhqizgpjzh7dm93905fbgkrilkmlcy32yr749l81")))

