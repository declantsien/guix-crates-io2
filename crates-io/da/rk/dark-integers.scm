(define-module (crates-io da rk dark-integers) #:use-module (crates-io))

(define-public crate-dark-integers-0.0.1 (c (n "dark-integers") (v "0.0.1") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "subtle") (r "^2.4") (d #t) (k 0)))) (h "1a31mscrhkd4lp75vmngnxkq0f69nld0x37nlaa9yhhj82zzck49")))

