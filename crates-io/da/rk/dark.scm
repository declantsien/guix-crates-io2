(define-module (crates-io da rk dark) #:use-module (crates-io))

(define-public crate-dark-0.1.0 (c (n "dark") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "compress") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1n2q5xpnqvpb3a74bzdjfpzcb924zma26sxqhz3lgjmf88gq2qrs") (f (quote (("unstable") ("default"))))))

