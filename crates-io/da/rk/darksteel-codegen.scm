(define-module (crates-io da rk darksteel-codegen) #:use-module (crates-io))

(define-public crate-darksteel-codegen-0.1.0 (c (n "darksteel-codegen") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mj1yfpa7b857n69a2nd7j9sq5qgc1qhssgyy608zrk46jd23b9d") (y #t)))

