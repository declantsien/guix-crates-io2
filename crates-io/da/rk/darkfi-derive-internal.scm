(define-module (crates-io da rk darkfi-derive-internal) #:use-module (crates-io))

(define-public crate-darkfi-derive-internal-0.3.0 (c (n "darkfi-derive-internal") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "099qkw3qp2xidgk99z4yisribbqshjqzyf1s76a5y9w183nkisx2")))

(define-public crate-darkfi-derive-internal-0.4.1 (c (n "darkfi-derive-internal") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1mxjcccsvayzzfw1bf0wg9vpvjcicvkq9qqhsfyhwfnd1qbbz2ar")))

