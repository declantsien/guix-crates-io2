(define-module (crates-io da rk dark-vm) #:use-module (crates-io))

(define-public crate-dark-vm-0.1.0 (c (n "dark-vm") (v "0.1.0") (h "1g0hkq41grqp2bwqx0hcasq7xzkkqxl608cmd4r0xf21llzj3vql")))

(define-public crate-dark-vm-0.2.0 (c (n "dark-vm") (v "0.2.0") (h "1d3441kcfqg1zpdrnj55j90dx1va1b6v154w4imk37lsinqz97ax")))

(define-public crate-dark-vm-0.3.0 (c (n "dark-vm") (v "0.3.0") (h "1qn4dhwgnfgm9xn5hivmx3arwxg6w06khwnqz3lgjw86g431qqrw")))

(define-public crate-dark-vm-0.4.0 (c (n "dark-vm") (v "0.4.0") (h "13w7ybzdrbfh92lpg97jbi5cqi2gnfbws1lrhs0m5315mg4hk5g3")))

(define-public crate-dark-vm-0.4.1 (c (n "dark-vm") (v "0.4.1") (h "1fivnv9lrzhbjsxxp7922kjbbalz2kbm9895r9jxnnhmyhg8mzqi")))

(define-public crate-dark-vm-0.4.2 (c (n "dark-vm") (v "0.4.2") (h "1p7sw4xqqc70zc032r02bpdsxq39hjaica7pf89bj84rkj46l1w7")))

(define-public crate-dark-vm-0.4.3 (c (n "dark-vm") (v "0.4.3") (h "054jcw48zps7vkj779cwzanxdc4mk7x4026rb71nn1hayvi1bbjs")))

