(define-module (crates-io da rk darkmagic) #:use-module (crates-io))

(define-public crate-darkmagic-0.0.0 (c (n "darkmagic") (v "0.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rawloader") (r "^0.36.3") (d #t) (k 0)))) (h "1fmdd906zkcixzibsybwxvi2zp7vgszvl7rjyjanbh08h4vjic2a")))

