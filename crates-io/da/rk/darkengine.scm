(define-module (crates-io da rk darkengine) #:use-module (crates-io))

(define-public crate-darkengine-0.1.0 (c (n "darkengine") (v "0.1.0") (d (list (d (n "bmfont") (r "^0.2") (d #t) (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "ears") (r "^0.5") (d #t) (k 0)) (d (n "glfw") (r "^0.23") (d #t) (k 0)) (d (n "glium") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.19") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (d #t) (k 0)))) (h "0kxdrpggyab3cc88h901b2lm0fh88qfhli3f8d2zicnn8mvq3m8w")))

