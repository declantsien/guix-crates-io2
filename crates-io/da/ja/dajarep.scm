(define-module (crates-io da ja dajarep) #:use-module (crates-io))

(define-public crate-dajarep-0.1.0 (c (n "dajarep") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "lindera") (r "^0.18.0") (f (quote ("ipadic"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "10rhais9kwrl9jbbjkc2psmcg4lzp4avjildj88pwcvf8my8q6bx")))

