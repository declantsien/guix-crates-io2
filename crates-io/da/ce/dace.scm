(define-module (crates-io da ce dace) #:use-module (crates-io))

(define-public crate-dace-0.1.0 (c (n "dace") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.15") (f (quote ("intel-mkl-static"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1z8q0x4ag2m00b9inri1n75wfaw6m1ypjyi0aisgvkk3rk59pmqf")))

(define-public crate-dace-0.1.1 (c (n "dace") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.15") (f (quote ("intel-mkl-static"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1aaaz6ppx0f87z7jkn824dcx4p8lb2ljas5liiqrzcjrq8mxdzjs")))

(define-public crate-dace-0.2.0 (c (n "dace") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1s5vv1ppf8apppw33p8gzb5dnk0khgci66v7sc1rqgzccfdy9vfb")))

(define-public crate-dace-0.2.1 (c (n "dace") (v "0.2.1") (d (list (d (n "auto_ops") (r "^0.3") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1g34g8byd2dja689b5q2nngsw65pykvvz0vnsxzhvjzamf3cwylb")))

