(define-module (crates-io da st dastral) #:use-module (crates-io))

(define-public crate-dastral-0.1.0 (c (n "dastral") (v "0.1.0") (h "1wbqis8bwj5l5g6hasmgh69i3dwkasfp5w18s8l6nbp6gzphbpnd")))

(define-public crate-dastral-0.1.1 (c (n "dastral") (v "0.1.1") (h "1ngmcfgx2f552klm6n66b5x75jydd8w1m3m6jzz3l3nz11frxjn5")))

