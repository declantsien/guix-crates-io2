(define-module (crates-io da rl darling-installer) #:use-module (crates-io))

(define-public crate-darling-installer-0.1.0 (c (n "darling-installer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "1zgrd0yaif6grlvyyya4y9pvrl9azdspwmn83aspg8kg7fmzjk3w")))

(define-public crate-darling-installer-0.1.1 (c (n "darling-installer") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "0zj92hk53n6lnqi4nzv7k98iylwk3v0b275ckddpcjai6qmwhx33")))

(define-public crate-darling-installer-0.1.2 (c (n "darling-installer") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "0829bd8ss847rcg0brz34dv2g3vkpikj0rmwhkaznzhh85yl34rw")))

(define-public crate-darling-installer-0.1.3 (c (n "darling-installer") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "0xyywyrdyw54vk68sm0gvqy258d1321lm493q03fb98gca6kq727")))

(define-public crate-darling-installer-0.1.4 (c (n "darling-installer") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "1gajwzyqdxfy8qkx0vvmjqzixzf50bdanbq2p03003xlmrca0v42")))

