(define-module (crates-io da rl darling-npm) #:use-module (crates-io))

(define-public crate-darling-npm-0.1.0 (c (n "darling-npm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.2") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)))) (h "10234pds14xb874ql6j7mrxvnha5a921800aipjmmlgmfds2b7i2")))

(define-public crate-darling-npm-0.1.1 (c (n "darling-npm") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.2") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)))) (h "174irj77id4hh9bw057ykik6hzldfapyfd6wbs6h7v5lrqgvi636")))

