(define-module (crates-io da rl darling-binary) #:use-module (crates-io))

(define-public crate-darling-binary-0.1.0 (c (n "darling-binary") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.1") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.9") (d #t) (k 0)) (d (n "unindent") (r "^0.2.3") (d #t) (k 0)))) (h "1bc3prl3n381h1payf3wx6z738i0n81ssyb0lix31qmg1fwp9ly6")))

(define-public crate-darling-binary-0.1.1 (c (n "darling-binary") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.1") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.9") (d #t) (k 0)) (d (n "unindent") (r "^0.2.3") (d #t) (k 0)))) (h "0h8lsn204s6k5q5xrj3kb5hynk1f52if5y74316c0w2s49hgdw4p")))

