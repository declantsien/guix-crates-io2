(define-module (crates-io da rl darling-cargo) #:use-module (crates-io))

(define-public crate-darling-cargo-0.1.0 (c (n "darling-cargo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.2") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0ygrnmz3n486fqb5kbxfraisynpcpw5j4alx9jari5jvgnjdkz03")))

(define-public crate-darling-cargo-0.1.1 (c (n "darling-cargo") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.2") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0v5r26qdmxsppgg7fs0xp6bwxvm9cwl1md8xffmq1wmx11jkwsh3")))

(define-public crate-darling-cargo-0.1.2 (c (n "darling-cargo") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.2") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)))) (h "03qak0bi0k3lxbjyx05cll08jrdwks7h0xxqv7kncl9bs9mqf3l4")))

