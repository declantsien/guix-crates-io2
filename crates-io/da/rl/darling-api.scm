(define-module (crates-io da rl darling-api) #:use-module (crates-io))

(define-public crate-darling-api-0.1.0 (c (n "darling-api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)))) (h "006l25lqrnqiwrsdwdfdcn86i7rc1i7m758k7nlrff60avp5lfnn")))

(define-public crate-darling-api-0.1.1 (c (n "darling-api") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)))) (h "01mq12xj0rgi39blcdcy7ps8i3rav1prvqbpfi6jmnbc863vhf6m")))

(define-public crate-darling-api-0.1.2 (c (n "darling-api") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)))) (h "0fw27h74sqby1njrngr8242szlrfm1brq86km91y13l10a0iqc40")))

(define-public crate-darling-api-0.1.3 (c (n "darling-api") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)))) (h "04b7hcbzdqlkzp3pxivdb35pglkpgd9bzidg5aicglv25m4avrgn")))

(define-public crate-darling-api-0.1.4 (c (n "darling-api") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)))) (h "0biy5ra67iici5hkfdsq5c52hqnbgf1vigpll2r87gg1qiay52hh")))

