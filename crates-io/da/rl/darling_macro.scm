(define-module (crates-io da rl darling_macro) #:use-module (crates-io))

(define-public crate-darling_macro-0.1.0 (c (n "darling_macro") (v "0.1.0") (d (list (d (n "darling_core") (r "= 0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1rva1c4a09j495qgyv19fihdplr1rway91qn46gw41q5s81yv49c")))

(define-public crate-darling_macro-0.1.1 (c (n "darling_macro") (v "0.1.1") (d (list (d (n "darling_core") (r "= 0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1ry52ybi3dh76i9a0phy5zm47p15prvr3z9kh1z6l2xrsbbnzm7i")))

(define-public crate-darling_macro-0.1.2 (c (n "darling_macro") (v "0.1.2") (d (list (d (n "darling_core") (r "= 0.1.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0rgzfnj8crjdngi1bl0i4qk9rf5j6sijn2gg6kx1bh2j6c98f79v")))

(define-public crate-darling_macro-0.1.3 (c (n "darling_macro") (v "0.1.3") (d (list (d (n "darling_core") (r "= 0.1.3") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0bw2camk6kih981i4rr6fchlhpmz28rzq7ljys81n569ryck2sn2")))

(define-public crate-darling_macro-0.1.4 (c (n "darling_macro") (v "0.1.4") (d (list (d (n "darling_core") (r "= 0.1.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0cpwnrqlsb170kiw9x0ma8c6cln12xvki2l4qvgmb12rv47a2bxa")))

(define-public crate-darling_macro-0.1.5 (c (n "darling_macro") (v "0.1.5") (d (list (d (n "darling_core") (r "= 0.1.5") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1pshx1wsxcrg1x9vn76ikryh5i77vsfijg9ysfp24f6wp26wk3g3")))

(define-public crate-darling_macro-0.1.6 (c (n "darling_macro") (v "0.1.6") (d (list (d (n "darling_core") (r "= 0.1.6") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1a4hqaj40308yrvbmf5gbikqyhh0zy0yki6sg3pinp224ds61dvz")))

(define-public crate-darling_macro-0.2.0 (c (n "darling_macro") (v "0.2.0") (d (list (d (n "darling_core") (r "= 0.2.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1yc03z69hyc5vcj0d0ha82lz3n2a2344x8fl5m4xvhx018bfr1la")))

(define-public crate-darling_macro-0.2.1 (c (n "darling_macro") (v "0.2.1") (d (list (d (n "darling_core") (r "= 0.2.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0rb616j3p9cdi901czrr6qh3d7d5600vnfkzf0wjpy8r540m3j7y")))

(define-public crate-darling_macro-0.2.2 (c (n "darling_macro") (v "0.2.2") (d (list (d (n "darling_core") (r "= 0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "134jq0rn18gz7ng5hs8i6akzjqqrqvinx7mx1nbnjvxqmgg1nn01")))

(define-public crate-darling_macro-0.3.0 (c (n "darling_macro") (v "0.3.0") (d (list (d (n "darling_core") (r "= 0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "10jdfjpfwchh7456dj7q8h2gdcqfhr029ir71jhp6q2z8nxdlgn5")))

(define-public crate-darling_macro-0.3.1 (c (n "darling_macro") (v "0.3.1") (d (list (d (n "darling_core") (r "= 0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0pfyngqizsdvgq73zrcag8yz94xml6xkgvq4jr2i3k6fhgs40s1h")))

(define-public crate-darling_macro-0.3.2 (c (n "darling_macro") (v "0.3.2") (d (list (d (n "darling_core") (r "= 0.3.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0hcy4rjm1m77scrnj8gln9019hf2nil5dwy7sp6h1kbjm5srbpdl")))

(define-public crate-darling_macro-0.3.3 (c (n "darling_macro") (v "0.3.3") (d (list (d (n "darling_core") (r "= 0.3.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0rwh86pay4fbz03rg8hqmbz3nd8jb22kis9hgw7ps2p1nldpcf63") (y #t)))

(define-public crate-darling_macro-0.4.0 (c (n "darling_macro") (v "0.4.0") (d (list (d (n "darling_core") (r "= 0.4.0") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "13jdbb13ww0ysbls735q9z1dzsbywq6igpz12avkvbxfvs7s6sgb")))

(define-public crate-darling_macro-0.5.0 (c (n "darling_macro") (v "0.5.0") (d (list (d (n "darling_core") (r "= 0.5.0") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0470zgkv3y7cm6jpcqh9ndx20yc8jjf2lxajbv7kw6ji62218686")))

(define-public crate-darling_macro-0.6.0 (c (n "darling_macro") (v "0.6.0") (d (list (d (n "darling_core") (r "= 0.6.0") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "00sc2j8q518q3cpvgrxg2pcsyyh2g8izway4k6bkwh57l9nxsk1s")))

(define-public crate-darling_macro-0.6.1 (c (n "darling_macro") (v "0.6.1") (d (list (d (n "darling_core") (r "= 0.6.1") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1n9wg1lrl7w7kwq65aj7237ipm4h4vv4bld7imcpk6f4licvj2s0")))

(define-public crate-darling_macro-0.6.2 (c (n "darling_macro") (v "0.6.2") (d (list (d (n "darling_core") (r "= 0.6.2") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1i35ghrpmvpjps3rky08ajcxabc05p485c6vvgyb8cn7sbrw165y")))

(define-public crate-darling_macro-0.6.3 (c (n "darling_macro") (v "0.6.3") (d (list (d (n "darling_core") (r "= 0.6.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0b1cr8542mgwzflbdn8zkfni4m57p4pp5v88rn5davvgppwhf0i8")))

(define-public crate-darling_macro-0.7.0 (c (n "darling_macro") (v "0.7.0") (d (list (d (n "darling_core") (r "= 0.7.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "178pz3r6c5ffzky12fmzyvw2b1ag7w2j3jz9kbxp3qc4bfm3nha0")))

(define-public crate-darling_macro-0.8.0 (c (n "darling_macro") (v "0.8.0") (d (list (d (n "darling_core") (r "= 0.8.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1sysi5xd8zx7xgvgxw5q0k38m00syi3n2cbvba9s5qkblh5hawwr")))

(define-public crate-darling_macro-0.8.1 (c (n "darling_macro") (v "0.8.1") (d (list (d (n "darling_core") (r "= 0.8.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0pap5yi39g98wl0ik397d233642rr5wzy4l0awi6mbxypksfzi4r")))

(define-public crate-darling_macro-0.8.2 (c (n "darling_macro") (v "0.8.2") (d (list (d (n "darling_core") (r "= 0.8.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0gc1ixgwqq28z47ah8k4rxpqcrp96v5yzdkz1vyl3apyx80a9jb7")))

(define-public crate-darling_macro-0.8.3 (c (n "darling_macro") (v "0.8.3") (d (list (d (n "darling_core") (r "= 0.8.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1zg7c5vvrrl6sva8s7c8cjv137nya8c96cynf257sjim10y4ph5q")))

(define-public crate-darling_macro-0.8.4 (c (n "darling_macro") (v "0.8.4") (d (list (d (n "darling_core") (r "= 0.8.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "0xk32x5hlq42giphd422bmcb8bmm2kzabzvay7bcpcimyak69q34")))

(define-public crate-darling_macro-0.8.5 (c (n "darling_macro") (v "0.8.5") (d (list (d (n "darling_core") (r "= 0.8.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "0jf54c0rh0vgddlrkz3v3ncpjihy9w64i6l82ll0k9p1yl7my4g4")))

(define-public crate-darling_macro-0.8.6 (c (n "darling_macro") (v "0.8.6") (d (list (d (n "darling_core") (r "= 0.8.6") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "1c87ka0valy3an1c4qvmg6q7yq3zcnij1phc4j2l65sfpn3qjki4")))

(define-public crate-darling_macro-0.9.0 (c (n "darling_macro") (v "0.9.0") (d (list (d (n "darling_core") (r "= 0.9.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "1lcq9418w0vmvncg4a3n9k64zjvqz0048aviqi0rmlpiqv0xmn66")))

(define-public crate-darling_macro-0.10.0 (c (n "darling_macro") (v "0.10.0") (d (list (d (n "darling_core") (r "= 0.10.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1s15mpyb9nyms4y0kkp214qlqhrspz32l7lla16vv36v5rav0mhv")))

(define-public crate-darling_macro-0.10.1 (c (n "darling_macro") (v "0.10.1") (d (list (d (n "darling_core") (r "= 0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1hb2bajmf18kgbg6rzvxa78ph7bbsrlnlacq52vi021cwlrf9lqc")))

(define-public crate-darling_macro-0.10.2 (c (n "darling_macro") (v "0.10.2") (d (list (d (n "darling_core") (r "= 0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0wlv31cxkrjijz5gv13hvk55c9lmd781aj12c8n84sa9mksa5dfr")))

(define-public crate-darling_macro-0.10.3 (c (n "darling_macro") (v "0.10.3") (d (list (d (n "darling_core") (r "=0.10.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0blshkwscmyv05p7dw0bf7ifq3gi5dgivlw5fp8pmhghkqv3rxj4") (y #t)))

(define-public crate-darling_macro-0.11.0 (c (n "darling_macro") (v "0.11.0") (d (list (d (n "darling_core") (r "=0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1207h8lpjmxamxb5dcrlvf9apvm1dqm09ilaj78ynb3r0b48lqgh")))

(define-public crate-darling_macro-0.12.0 (c (n "darling_macro") (v "0.12.0") (d (list (d (n "darling_core") (r "=0.12.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0mcprj9n9vg84v03zg8pv7y3w1pv398f3jhhwzskl0hdsp29mkg9")))

(define-public crate-darling_macro-0.12.1 (c (n "darling_macro") (v "0.12.1") (d (list (d (n "darling_core") (r "= 0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "03zg92w1011p0l1wq0kdhirhywg9pg4cyds54v0ynx6lxwql2646")))

(define-public crate-darling_macro-0.12.2 (c (n "darling_macro") (v "0.12.2") (d (list (d (n "darling_core") (r "= 0.12.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "10cbcdx6s188labavrvj90r1p6wyxbd7qkpw1ski4kshrrrh08n0")))

(define-public crate-darling_macro-0.12.3 (c (n "darling_macro") (v "0.12.3") (d (list (d (n "darling_core") (r "=0.12.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1mc7qjdyx54xskzw93gkjgisrfzabxqkw6mkwf99fbsbsm2i8yha")))

(define-public crate-darling_macro-0.12.4 (c (n "darling_macro") (v "0.12.4") (d (list (d (n "darling_core") (r "=0.12.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0nn9mxl7gs827rx5s6lbjvvghipxjdg2qpdjyxk7yym3vvqard99")))

(define-public crate-darling_macro-0.13.0-beta (c (n "darling_macro") (v "0.13.0-beta") (d (list (d (n "darling_core") (r "=0.13.0-beta") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (d #t) (k 0)))) (h "0jgzb58pqwsi6k2q40b5bycma9ivmsp596mhh4kls5bs58zv9zsr")))

(define-public crate-darling_macro-0.13.0 (c (n "darling_macro") (v "0.13.0") (d (list (d (n "darling_core") (r "=0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (d #t) (k 0)))) (h "1k0mky8zrz9ayzmjw2yp6l7jbh4ysimrq24zsgkfa3qk8zqvzrxd")))

(define-public crate-darling_macro-0.13.1 (c (n "darling_macro") (v "0.13.1") (d (list (d (n "darling_core") (r "=0.13.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (d #t) (k 0)))) (h "0jzljnd0y7idi5lb7lhvymh3nkhaf32ksx0d38hv7zjjfcxipi3j")))

(define-public crate-darling_macro-0.13.2 (c (n "darling_macro") (v "0.13.2") (d (list (d (n "darling_core") (r "=0.13.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (d #t) (k 0)))) (h "03kzpv7rnqlci8lgyszp0sd3pc3s7cd5zcbgpll7am6x46rdc7dj")))

(define-public crate-darling_macro-0.13.3 (c (n "darling_macro") (v "0.13.3") (d (list (d (n "darling_core") (r "=0.13.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (d #t) (k 0)))) (h "1bxl5sflc3gg76mam5vfxwdyknv51c2a4ih10mbh2f6691qi9bpg")))

(define-public crate-darling_macro-0.13.4 (c (n "darling_macro") (v "0.13.4") (d (list (d (n "darling_core") (r "=0.13.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (d #t) (k 0)))) (h "0d8q8ibmsb1yzby6vwgh2wx892jqqfv9clwhpm19rprvz1wjd5ww")))

(define-public crate-darling_macro-0.14.0 (c (n "darling_macro") (v "0.14.0") (d (list (d (n "darling_core") (r "=0.14.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1vbvhff86b77cisy5abrcppls4x0pg1vzyz9k9wvc350fmd7xpb4")))

(define-public crate-darling_macro-0.14.1 (c (n "darling_macro") (v "0.14.1") (d (list (d (n "darling_core") (r "=0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1dag2f4bq38vdn886slqczip5qzhvb95317kl04zrlnbpz2nkz6x")))

(define-public crate-darling_macro-0.14.2 (c (n "darling_macro") (v "0.14.2") (d (list (d (n "darling_core") (r "=0.14.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "0zmb5awifqajh43m9mmd7flrpzwxm2q05m1dc9a2ch790wj8263n")))

(define-public crate-darling_macro-0.14.3 (c (n "darling_macro") (v "0.14.3") (d (list (d (n "darling_core") (r "=0.14.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "11b6llzgvqq69l4f47glmy6j1acri8nzflbg3knffp9di9ck0qmk")))

(define-public crate-darling_macro-0.14.4 (c (n "darling_macro") (v "0.14.4") (d (list (d (n "darling_core") (r "=0.14.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "13mlyd5w275c815k0ijf6g4c446hs8b3m2h4an5isqgpr7dv9am4")))

(define-public crate-darling_macro-0.20.0 (c (n "darling_macro") (v "0.20.0") (d (list (d (n "darling_core") (r "=0.20.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1ssijnsfykprswkjhfy2rxv9m0fy9s2ayk7v12c2m2hv5fv85yvv")))

(define-public crate-darling_macro-0.20.1 (c (n "darling_macro") (v "0.20.1") (d (list (d (n "darling_core") (r "=0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "06k18lagz6r6gyq1gaa6b9lklqh2k5d9pvqzwv1hkv0jkzzmi8r9")))

(define-public crate-darling_macro-0.20.3 (c (n "darling_macro") (v "0.20.3") (d (list (d (n "darling_core") (r "=0.20.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1mg2k1f0v33s271lpn4m5mxcfjqnmg61bf77svb44cyngay9nsl3")))

(define-public crate-darling_macro-0.20.4 (c (n "darling_macro") (v "0.20.4") (d (list (d (n "darling_core") (r "=0.20.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1kdr0zpgl0mgfz1r8j77cfyhdfn5399mqsqafvmiz6wpjfyqhb8d")))

(define-public crate-darling_macro-0.20.5 (c (n "darling_macro") (v "0.20.5") (d (list (d (n "darling_core") (r "=0.20.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0xsg8ja6ncw9zpf7sdfinmp459z5vi97fp3y7gcy2j91gbb4a58x")))

(define-public crate-darling_macro-0.20.6 (c (n "darling_macro") (v "0.20.6") (d (list (d (n "darling_core") (r "=0.20.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1gl6gh9kr71zi1j5nji4nia7r85srny9mgx9q81khqfgmj8i7af5")))

(define-public crate-darling_macro-0.20.7 (c (n "darling_macro") (v "0.20.7") (d (list (d (n "darling_core") (r "=0.20.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0mzdyhn97cldib4lr077l1qqybp5i1gpfs8ip6j5jp8cz6ind8yn")))

(define-public crate-darling_macro-0.20.8 (c (n "darling_macro") (v "0.20.8") (d (list (d (n "darling_core") (r "=0.20.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0gwkz0cjfy3fgcc1zmm7azzhj5qpja34s0cklcria4l38sjyss56")))

(define-public crate-darling_macro-0.20.9 (c (n "darling_macro") (v "0.20.9") (d (list (d (n "darling_core") (r "=0.20.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0y015yy33p85sgpq7shm49clss78p71871gf7sss3cc26jsang3k") (r "1.56")))

