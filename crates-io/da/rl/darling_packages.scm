(define-module (crates-io da rl darling_packages) #:use-module (crates-io))

(define-public crate-darling_packages-0.1.0 (c (n "darling_packages") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.9") (d #t) (k 0)) (d (n "unindent") (r "^0.2.3") (d #t) (k 0)))) (h "1ipl3f82cmnvax1zikak9amq1x7n1ycxxzbxrw6677h75glj5g5v")))

