(define-module (crates-io da rl darling-arch) #:use-module (crates-io))

(define-public crate-darling-arch-0.1.0 (c (n "darling-arch") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "darling_packages") (r "^0.1.0") (d #t) (k 0)))) (h "1k87da8l21v67s1y08yrvpr77x8hk9clml4f6yn1r29cz6mnjzbx")))

(define-public crate-darling-arch-0.1.1 (c (n "darling-arch") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "darling_packages") (r "^0.1.0") (d #t) (k 0)))) (h "0s1dmqlyg96fjd1b82jraic40lpwxwxxbyp1zjnr0dxmp5hi1pbz")))

(define-public crate-darling-arch-0.1.2 (c (n "darling-arch") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.0") (d #t) (k 0)))) (h "138sqdy4ir1hspkmg7psmlj68kqxnzqs3wcp6y5lkwlk0fp9v823")))

(define-public crate-darling-arch-0.1.3 (c (n "darling-arch") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.0") (d #t) (k 0)))) (h "1c08q4003bany55zwwc64cvs4pdg39ya9gpnw5lf985jr28p7sm4")))

(define-public crate-darling-arch-0.1.4 (c (n "darling-arch") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.0") (d #t) (k 0)))) (h "0y80kl0yj54q99qwzbnz2303ffhkizk5kfcms6c8agzz7cyryaa7")))

(define-public crate-darling-arch-0.1.5 (c (n "darling-arch") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.1") (d #t) (k 0)))) (h "1a0x9c6vsrw7fkv05sldr9ygk0jppiv9x8gg15ihsy9hdq7k3dnm")))

(define-public crate-darling-arch-0.1.6 (c (n "darling-arch") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.1") (d #t) (k 0)))) (h "1kqyqbx54npxcvipvqvxh29bmvz1g8kf4n4n5xa2krd2gbai6cbp")))

(define-public crate-darling-arch-0.1.7 (c (n "darling-arch") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.1") (d #t) (k 0)))) (h "06wl8vj770mccr58lw58kiy1jycrxm7s0mx2q0czzi68wmsidvf5")))

(define-public crate-darling-arch-0.1.8 (c (n "darling-arch") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.2") (d #t) (k 0)))) (h "0a0sax6lcwrisxsvw61qgwicgp9p3dak4y3x6lslb9ys2pjvm684")))

