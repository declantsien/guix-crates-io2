(define-module (crates-io da rl darling-template) #:use-module (crates-io))

(define-public crate-darling-template-0.1.0 (c (n "darling-template") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.2") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)))) (h "16i90ckf8j8j886dmcwc06y5irp01sihsb649p3ivgjxpmd0431f")))

(define-public crate-darling-template-0.1.1 (c (n "darling-template") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.2") (d #t) (k 0)))) (h "0p1fazg6hx64a65i52sww3ir57kbdcy8flx2yjgmin1lghnyiwjg")))

