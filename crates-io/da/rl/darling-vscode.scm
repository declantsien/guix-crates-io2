(define-module (crates-io da rl darling-vscode) #:use-module (crates-io))

(define-public crate-darling-vscode-0.1.0 (c (n "darling-vscode") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "0mn41xp93fsybblziniwq29j5m5sfngbnghw9i8nj0kri0sjydx9")))

(define-public crate-darling-vscode-0.1.1 (c (n "darling-vscode") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "18w38bhaknil8p9rvlwjgkjfhlrg1yqgvk94qfv40rjwplsmz0hd")))

(define-public crate-darling-vscode-0.1.2 (c (n "darling-vscode") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "darling-api") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "0jypp7gja02g4wzppqwkb78y1zkvl11bnbq3ws1lqb9ch16h62xz")))

