(define-module (crates-io da ll dalloriam-cloud-protocol) #:use-module (crates-io))

(define-public crate-dalloriam-cloud-protocol-0.1.0 (c (n "dalloriam-cloud-protocol") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "09mkak6pqx1l9k9pjcvys0imbjpr0a1k17m87n1nxnilbyy8z8xh") (f (quote (("hose"))))))

(define-public crate-dalloriam-cloud-protocol-0.1.1 (c (n "dalloriam-cloud-protocol") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ar5q6x4jmjm6jbqmijwbrvrvpl45dp55p0wpl11h2vkfdcbfaq1") (f (quote (("hose" "chrono"))))))

(define-public crate-dalloriam-cloud-protocol-0.1.2 (c (n "dalloriam-cloud-protocol") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xq5y7w7zyl51d12zzvkcv4xb7mbah1p2vlljaqmr34391br3cig") (f (quote (("hose" "chrono"))))))

(define-public crate-dalloriam-cloud-protocol-0.1.3 (c (n "dalloriam-cloud-protocol") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n6j1f7hxn20a550h0nyy8g6bix9cyd24v0fjmbhqa5nykzinh0g") (f (quote (("hose" "chrono"))))))

(define-public crate-dalloriam-cloud-protocol-0.2.0 (c (n "dalloriam-cloud-protocol") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qafpig4m2jvhsv6fckq5wc8hvcbkvx59smcc2n19jn4v99snyi9") (f (quote (("jd") ("hose" "chrono"))))))

(define-public crate-dalloriam-cloud-protocol-0.3.0 (c (n "dalloriam-cloud-protocol") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.6.2") (f (quote ("postgres" "runtime-tokio-rustls" "all-types"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("serde"))) (o #t) (d #t) (k 0)))) (h "0y27bzvahn01q5j8hsasqzmqzrcx3rf9fkhl316s1g8xx6hyw990") (f (quote (("media" "time") ("jd") ("hose" "chrono"))))))

(define-public crate-dalloriam-cloud-protocol-0.3.1 (c (n "dalloriam-cloud-protocol") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.6.2") (f (quote ("postgres" "runtime-tokio-rustls" "all-types"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("serde"))) (o #t) (d #t) (k 0)))) (h "1j2vc73g0falpv9n0q14w1fsssb0h274h8qprq64acrv372dl7n9") (f (quote (("media" "time") ("jd") ("hose" "chrono"))))))

