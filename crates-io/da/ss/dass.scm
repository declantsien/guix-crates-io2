(define-module (crates-io da ss dass) #:use-module (crates-io))

(define-public crate-dass-0.1.0 (c (n "dass") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1arwfvgii8gaqxr1lciszcn8awsms2797yjhw40j1b4y97lg42ga")))

(define-public crate-dass-0.2.0 (c (n "dass") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ibcb9754s54il864favrs4bd9w6fa909v1q15i59q7cc4zf1lmx")))

