(define-module (crates-io da ss dassign) #:use-module (crates-io))

(define-public crate-dassign-0.1.0 (c (n "dassign") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0kaq2a640q3393q32jj04hyab7h14il3vd6fig5i6fs4rhinm1xh")))

(define-public crate-dassign-0.2.0 (c (n "dassign") (v "0.2.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0r5swnk9302k0yflr35gjakcdai5093zxm8hpg96cyapbmjzh2vl")))

