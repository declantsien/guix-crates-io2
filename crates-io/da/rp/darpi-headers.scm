(define-module (crates-io da rp darpi-headers) #:use-module (crates-io))

(define-public crate-darpi-headers-0.1.0-beta (c (n "darpi-headers") (v "0.1.0-beta") (d (list (d (n "darpi") (r "^0.1.2-beta") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)))) (h "0h5k8ilwi3ran0z6bjzwnywdnh4k7c0ln1p0n73xxlir8gq02lri")))

(define-public crate-darpi-headers-0.1.0-beta.1 (c (n "darpi-headers") (v "0.1.0-beta.1") (d (list (d (n "darpi") (r "^0.1.4-beta.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)))) (h "01kr3n3xkb8r223dap3g63as69bfqj8f2hln8w31n1y7qzfc7ayw")))

