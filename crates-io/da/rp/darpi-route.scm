(define-module (crates-io da rp darpi-route) #:use-module (crates-io))

(define-public crate-darpi-route-0.1.0 (c (n "darpi-route") (v "0.1.0") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)))) (h "1rblscadvm6spjc49b3k8b94wyh9ddyn6wyk14zchgaf8i606nid")))

(define-public crate-darpi-route-0.1.0-beta (c (n "darpi-route") (v "0.1.0-beta") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)))) (h "1zy37nnmml6xd9hgqp76wji9av43v94i14134f5vg6wiyvgcj7xp")))

