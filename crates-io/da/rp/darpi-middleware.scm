(define-module (crates-io da rp darpi-middleware) #:use-module (crates-io))

(define-public crate-darpi-middleware-0.1.0 (c (n "darpi-middleware") (v "0.1.0") (d (list (d (n "darpi") (r "^0.1.0") (d #t) (k 0)) (d (n "darpi-code-gen") (r "^0.1.0") (d #t) (k 0)) (d (n "darpi-web") (r "^0.1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.13.9") (d #t) (k 0)))) (h "10n3fpslqkl54rrkdw7x1jlz2ixnaz5wnzgyk4sgcigghksamvrz")))

(define-public crate-darpi-middleware-0.1.0-beta.1 (c (n "darpi-middleware") (v "0.1.0-beta.1") (d (list (d (n "async-compression") (r "^0.3.7") (f (quote ("all" "futures-io"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "darpi") (r "^0.1.4-beta.1") (d #t) (k 0)) (d (n "darpi-headers") (r "^0.1.0-beta.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.8") (d #t) (k 0)) (d (n "jsonwebtoken") (r "=7.2") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shaku") (r "^0.5.0") (f (quote ("thread_safe"))) (d #t) (k 0)))) (h "0bjap8igl5vwzjqchmfnrcr1bcivvzq0l756wbsl1prlljblcap1")))

