(define-module (crates-io da lv dalvik) #:use-module (crates-io))

(define-public crate-dalvik-0.1.0 (c (n "dalvik") (v "0.1.0") (h "043c499c4zqxbrap8zhsigm2vd5yz2p6fdn7m83s6g2gbq564s3y")))

(define-public crate-dalvik-0.1.1 (c (n "dalvik") (v "0.1.1") (h "0625s789lmw9h1hk3kri8p8sgp4c6bzhwqrnza45rr9m55pnx7il")))

(define-public crate-dalvik-0.1.2 (c (n "dalvik") (v "0.1.2") (h "14pw8l71kzv63hqj35inlmpp9f8ql2y1iziw4nrpa436l63s0jql")))

(define-public crate-dalvik-0.2.0 (c (n "dalvik") (v "0.2.0") (h "13npfvpr066xdf11ry4k5q5kgc237qkf25158klj31n87b1i6yij")))

