(define-module (crates-io da c8 dac8564) #:use-module (crates-io))

(define-public crate-dac8564-0.1.0 (c (n "dac8564") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "05wi001yzw91szb6wqrsqxx9s5prvnkcwj3hzs4x7nbc2s3a763q") (f (quote (("default")))) (y #t)))

(define-public crate-dac8564-0.0.1 (c (n "dac8564") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1mbc4hqjpwprckgq754fkgymqd3ad5zbqfjc9078fv7sgf2w1n7d") (f (quote (("default"))))))

(define-public crate-dac8564-0.0.2 (c (n "dac8564") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "06w9h57h9x93wbf5hi2nbzsfmi2rzkasj17lca3yk511nd1sjh36") (f (quote (("default"))))))

(define-public crate-dac8564-0.0.3 (c (n "dac8564") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1n8jk5zq6bzn61zzgxyd84g65n7mfryhc9bajdx45kb86ay1a395") (f (quote (("default"))))))

(define-public crate-dac8564-0.0.4 (c (n "dac8564") (v "0.0.4") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "146jk9f5z9arq2mdxcq8hdlwd2affbacv88596g9xv26yqisrjgq") (f (quote (("default"))))))

