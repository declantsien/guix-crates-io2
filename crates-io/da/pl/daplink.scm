(define-module (crates-io da pl daplink) #:use-module (crates-io))

(define-public crate-daplink-0.1.0 (c (n "daplink") (v "0.1.0") (d (list (d (n "arm-memory") (r "^0.1.0") (d #t) (k 0)) (d (n "coresight") (r "^0.1.0") (d #t) (k 0)) (d (n "debug-probe") (r "^0.1.0") (d #t) (k 0)) (d (n "hidapi") (r "^0.5.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rental") (r "^0.5.4") (d #t) (k 0)) (d (n "scroll") (r "^0.9.2") (d #t) (k 0)))) (h "095ydy394cb3xgv95ndc3w73ypq5h9ij8513gwylsras3b448hwj") (y #t)))

