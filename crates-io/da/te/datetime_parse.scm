(define-module (crates-io da te datetime_parse) #:use-module (crates-io))

(define-public crate-datetime_parse-0.0.1-alpha (c (n "datetime_parse") (v "0.0.1-alpha") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0fc91nfczh8pkzd21pn2i6hmyjaxbc1c4j2a94vnvjnkhksblr7i")))

(define-public crate-datetime_parse-0.0.1-beta.1 (c (n "datetime_parse") (v "0.0.1-beta.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "15fq605rfz61c58afx42cxzda13fvsdw7lh38xdqybjqg5bi2rcb")))

(define-public crate-datetime_parse-0.0.1-beta.2 (c (n "datetime_parse") (v "0.0.1-beta.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1zvy4hdy1qxb1ma07qgancx1aa18kgylxwhdprn5y68cqi2q0kcg")))

(define-public crate-datetime_parse-0.0.1-beta.3 (c (n "datetime_parse") (v "0.0.1-beta.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "01n2srkpnfdbhgmgdiwnya5jq3yp9vwkh2b2dyi9y6ciwvphdgs6")))

(define-public crate-datetime_parse-0.0.1-beta.4 (c (n "datetime_parse") (v "0.0.1-beta.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0ikjs0zyr6imh5mbcpx6vlz6wm1vkg1vlrigbnnf4rhrcm6zgycj")))

(define-public crate-datetime_parse-0.0.1-beta.6 (c (n "datetime_parse") (v "0.0.1-beta.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1jxfhd9370k3713y4wxa3dss7gq97ppaq76wfl3kcv9f2ngcz4q5")))

(define-public crate-datetime_parse-0.0.1-beta.7 (c (n "datetime_parse") (v "0.0.1-beta.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1xbvg4abbnv6aj8rwdykhkafcawa7i2nq01fqi4rg0fb2fdj82hb")))

(define-public crate-datetime_parse-0.0.1-beta.8 (c (n "datetime_parse") (v "0.0.1-beta.8") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1m73m6dm25k8xi4v9vzivigj04wl6s8y83vcr4hcmc6fa9690mx6")))

(define-public crate-datetime_parse-0.0.1-beta.9 (c (n "datetime_parse") (v "0.0.1-beta.9") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "19s8wqlbqb5a4gd6izsplj5idd91cnqlns3ijfrnj0zdbyw9xfbs")))

(define-public crate-datetime_parse-0.0.1-beta.10 (c (n "datetime_parse") (v "0.0.1-beta.10") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "186dhf8szq9ab1x8bi09wn5qfbvp8n19g2qj4ka8sh0jabsd1199")))

(define-public crate-datetime_parse-0.0.1-beta.11 (c (n "datetime_parse") (v "0.0.1-beta.11") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1ajpkq3rjx5gccbhyk8v6ksr6i16v8z3gbdw0kkmr9rh6abdll9x")))

