(define-module (crates-io da te dateframe) #:use-module (crates-io))

(define-public crate-dateframe-0.1.1 (c (n "dateframe") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0r8c8ckvrzyw5q56f1bkdkx3faddipxy3glpx2cxpd9l1n99bml9")))

(define-public crate-dateframe-0.2.0 (c (n "dateframe") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "11vbqv3d7wh0bd0wkvppzki9h1b4zf3wyk5hhy9bqqjij86y88a5")))

(define-public crate-dateframe-0.2.1 (c (n "dateframe") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0da53iilhrshd84zj5ch4ywsl4dpadbsyfhqjmmlppa49mqsgz7r")))

(define-public crate-dateframe-0.2.3 (c (n "dateframe") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "1zp74k6nkxp14slmcpd8sqdg7asckriagp28m6gxc0i2kqjvv9zv")))

(define-public crate-dateframe-0.2.4 (c (n "dateframe") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "06gb5zcv1p109fp2ap1fahkh2bklm778rq9a0n9kik6q3561kwvz")))

