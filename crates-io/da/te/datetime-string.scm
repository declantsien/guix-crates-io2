(define-module (crates-io da te datetime-string) #:use-module (crates-io))

(define-public crate-datetime-string-0.1.0 (c (n "datetime-string") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.116") (d #t) (k 2)))) (h "0i0cqnc20f7wwqaqy3a4rnwz75x3933n74hxi9rsihsqfcqarzhr") (f (quote (("std_with_serde" "std" "serde/std") ("std" "alloc") ("default" "std") ("alloc_with_serde" "alloc" "serde/alloc") ("alloc"))))))

(define-public crate-datetime-string-0.2.0 (c (n "datetime-string") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.116") (d #t) (k 2)))) (h "1drj9cnws1z2r1i7w2x7jhkp9hsffvly2p43nv2cwdprmgk45nfl") (f (quote (("std_with_serde" "std" "serde/std") ("std" "alloc") ("default" "std") ("alloc_with_serde" "alloc" "serde/alloc") ("alloc"))))))

(define-public crate-datetime-string-0.2.1 (c (n "datetime-string") (v "0.2.1") (d (list (d (n "chrono04") (r "^0.4.19") (o #t) (k 0) (p "chrono")) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.116") (d #t) (k 2)))) (h "12ifj6b66s2363vqzrbi4w4a327wnpagw2k1z1zlffjay9firvk4") (f (quote (("std_with_serde" "std" "serde/std") ("std" "alloc") ("default" "std") ("alloc_with_serde" "alloc" "serde/alloc") ("alloc"))))))

(define-public crate-datetime-string-0.2.2 (c (n "datetime-string") (v "0.2.2") (d (list (d (n "chrono04") (r "^0.4.19") (o #t) (k 0) (p "chrono")) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.116") (d #t) (k 2)) (d (n "time03") (r "^0.3.7") (o #t) (k 0) (p "time")))) (h "1rgnp571zyzxi1xd84ijiz3wxgsb6xnhz8jxk573b7shmrg6dp2f") (f (quote (("std_with_serde" "serde-std") ("std" "alloc") ("serde-std" "std" "serde/std") ("serde-alloc" "alloc" "serde/alloc") ("default" "std") ("alloc_with_serde" "serde-alloc") ("alloc"))))))

