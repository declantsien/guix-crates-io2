(define-module (crates-io da te date-differencer) #:use-module (crates-io))

(define-public crate-date-differencer-0.1.0 (c (n "date-differencer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "random-number") (r "^0.1.8") (d #t) (k 2)) (d (n "year-helper") (r "^0.2.0") (d #t) (k 0)))) (h "1rss1saqkg30xdviq3n0frwx5fgpbpisr907dcaz2i7q23kk87yy") (r "1.56")))

(define-public crate-date-differencer-0.1.1 (c (n "date-differencer") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "random-number") (r "^0.1.8") (d #t) (k 2)) (d (n "year-helper") (r "^0.2.0") (d #t) (k 0)))) (h "02amvnx7a5xljvpl6w5pjdjjpxvybnycydc7gq6n4kfv1kcxpwlm") (r "1.60")))

(define-public crate-date-differencer-0.1.2 (c (n "date-differencer") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 2)) (d (n "random-number") (r "^0.1.8") (d #t) (k 2)) (d (n "year-helper") (r "^0.2") (d #t) (k 0)))) (h "031bqy0s4q06z1qxmy8v9adj70qd1hq3wrx1dvvhc0mc7hvkfr4s") (r "1.60")))

(define-public crate-date-differencer-0.1.3 (c (n "date-differencer") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 2)) (d (n "random-number") (r "^0.1") (d #t) (k 2)) (d (n "year-helper") (r "^0.2") (d #t) (k 0)))) (h "1ipbyppzljw6sk4xq9qnnn6bazsiaqvzmy8hkkdiii3hhzwqfm44") (r "1.60")))

