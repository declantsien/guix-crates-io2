(define-module (crates-io da te dateless) #:use-module (crates-io))

(define-public crate-dateless-0.1.0 (c (n "dateless") (v "0.1.0") (h "1zy7dda83mcp6fwr9azv5vjlah2znh4c5km2h0nybjakf5zg98k9") (y #t)))

(define-public crate-dateless-0.1.1 (c (n "dateless") (v "0.1.1") (h "0gbp9zq5vj4lv265maah6xc45csh09g2w8d3z5qzykb627w7q3rk")))

(define-public crate-dateless-0.2.0 (c (n "dateless") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "optional_struct") (r "^0.2.0") (d #t) (k 0)))) (h "1cjn03wr2frdr76cynfki48khxybjv2xkrm4z3rn19mn9f6yd6x9")))

(define-public crate-dateless-0.2.1 (c (n "dateless") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "optional_struct") (r "^0.2.0") (d #t) (k 0)))) (h "1q72yw03r5swc9i9jzga0q3rkkijdhnmfzg9gmq0z1krfikqp7lh")))

(define-public crate-dateless-0.2.2 (c (n "dateless") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "optional_struct") (r "^0.2.0") (d #t) (k 0)))) (h "1krdlv1r863kfisvpxrnl5h7gmp6zajapa5mxyfivmv3mcdnxb2c") (y #t)))

(define-public crate-dateless-0.2.3 (c (n "dateless") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (o #t) (d #t) (t "cfg(feature = \"serde_support\")") (k 0)) (d (n "optional_struct") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wq2y2fpgyn4ix4lfgjbl80bd1aqn521g89r9ycby81d15j0id6a") (f (quote (("serde_support" "serde" "chrono") ("default"))))))

(define-public crate-dateless-0.3.0 (c (n "dateless") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (o #t) (d #t) (t "cfg(feature = \"serde_support\")") (k 0)) (d (n "optfield") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "typetag") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "02ng82qjvvck8g0bs5m72k828a1dxxm4hllxhww6bhsm2pdf67ln") (f (quote (("serde_support" "serde" "chrono" "typetag") ("default" "serde_support"))))))

(define-public crate-dateless-0.3.1 (c (n "dateless") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (o #t) (d #t) (t "cfg(feature = \"serde_support\")") (k 0)) (d (n "optfield") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "typetag") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "1b09jcpsj5vgyzg23zi946p7ni44pq9vwwi007f7b9dg1x5h4a15") (f (quote (("serde_support" "serde" "chrono" "typetag") ("default" "serde_support"))))))

