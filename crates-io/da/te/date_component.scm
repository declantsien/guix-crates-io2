(define-module (crates-io da te date_component) #:use-module (crates-io))

(define-public crate-date_component-0.1.0 (c (n "date_component") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "122vx20x3knh7732lmq0vjb8q0d15az0qqg0jawm8ahwags9rr25") (y #t)))

(define-public crate-date_component-0.1.1 (c (n "date_component") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "14404dr1d2slggip9q9jr8swpnxydza7clqbnmj1g8j3yi26qwbm") (y #t)))

(define-public crate-date_component-0.1.2 (c (n "date_component") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0lpngay1f291cklcwbfmfnw74iwb55zbqnzgsw62jjrj15gzn4q2") (y #t)))

(define-public crate-date_component-0.1.3 (c (n "date_component") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0q8mnfjmi1xwqix3n4diljcg7fd4r0jna9hpzzpmx07j0y276wz0") (y #t)))

(define-public crate-date_component-0.1.4 (c (n "date_component") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1j5hppdgw78fghp048q4nd653z2715x7wv8773npl46pfnl6kdwx")))

(define-public crate-date_component-0.1.5 (c (n "date_component") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0q2344fcdx8x6qfwy402ra4l3dd70zvbl7a36q0c6pl512jrp7ms") (y #t)))

(define-public crate-date_component-0.2.0 (c (n "date_component") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1sjic9fq3w48w0zrjxjllsy9m68f34n5mfw34wr8yl2407qnxrj1")))

(define-public crate-date_component-0.3.0 (c (n "date_component") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "01pgw0nnna9vp9r7hb52rw44asfqi0i7h76dr7kqwysydji0b407")))

(define-public crate-date_component-0.3.1 (c (n "date_component") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "15vr7hhqnmffhfildra4qg7vr04gyrwar13w54dnaa0gvlvvzb91")))

(define-public crate-date_component-0.4.0 (c (n "date_component") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "1cfmzng28dzdwxxixgwgqmj2k1h903vsc6jlgw42npkzk74jznsl")))

(define-public crate-date_component-0.4.1 (c (n "date_component") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "05rzwsnkc3sn3jw7hzk7mpkpl4s546yjvld428b2y32yh8c43kdw")))

