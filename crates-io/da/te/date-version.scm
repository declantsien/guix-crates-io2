(define-module (crates-io da te date-version) #:use-module (crates-io))

(define-public crate-date-version-0.1.0 (c (n "date-version") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.19") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1p8k7m595jynci78bggqwhj715zv753smxgvk50dm57yxdc9ngs5")))

(define-public crate-date-version-0.1.1 (c (n "date-version") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.19") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)))) (h "1d8v5cx79r3mwbkifbhnyapg4jzi07zk4l8zscbh0kmg2vavz295")))

