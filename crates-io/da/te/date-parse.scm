(define-module (crates-io da te date-parse) #:use-module (crates-io))

(define-public crate-date-parse-1.0.0 (c (n "date-parse") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "141vg1kyghg83hdb8k58cdrs0r0s8gdgz90flihz020m4drb4i9d")))

(define-public crate-date-parse-1.0.1 (c (n "date-parse") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1jp75i8mvrrr9s901a69rwabsw3j3381xsjldfh98b26rcyc55cf")))

