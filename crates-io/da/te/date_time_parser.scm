(define-module (crates-io da te date_time_parser) #:use-module (crates-io))

(define-public crate-date_time_parser-0.1.0 (c (n "date_time_parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0rk77ic7lkgiacas07gczgcv7qbn3g8zkv0a0bklmkxwyx3j4b89")))

(define-public crate-date_time_parser-0.1.1 (c (n "date_time_parser") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0v7dbldrj8s5p45rz6f8yc8q86rv1zsmfhxnp1n5b6rdd4zasv7q")))

(define-public crate-date_time_parser-0.2.0 (c (n "date_time_parser") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0npvjglxpnrwgbks3zfg2vpkm30p7595ygjhmirhfrqkwnb1slph")))

