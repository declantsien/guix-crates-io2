(define-module (crates-io da te date_iter) #:use-module (crates-io))

(define-public crate-date_iter-0.1.0 (c (n "date_iter") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "08dcg13w1f7ghb280vzp4xpmif3bw4aqvln5pn9khd30yrc900zc")))

(define-public crate-date_iter-0.1.1 (c (n "date_iter") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0kg5bpnj383cp53ni1hsqrwf2xfiy1kajs12igrn7hc0h0j9iq9s")))

