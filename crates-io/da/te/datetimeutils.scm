(define-module (crates-io da te datetimeutils) #:use-module (crates-io))

(define-public crate-datetimeutils-0.1.0 (c (n "datetimeutils") (v "0.1.0") (h "18qfrn201dx2g770ddbsi5haxr2a3kn171d1qzdspsjpxraza45f")))

(define-public crate-datetimeutils-0.1.1 (c (n "datetimeutils") (v "0.1.1") (h "1wwh60h2cr1rbsvy0f9y03jxmc9lbx0h492hw9wlsq53sjazhbcr")))

(define-public crate-datetimeutils-0.1.2 (c (n "datetimeutils") (v "0.1.2") (h "0312gmv0b5zxlz0yqda1m496y7lpqh828ldlkzzqw8326al4zv80")))

(define-public crate-datetimeutils-0.1.3 (c (n "datetimeutils") (v "0.1.3") (h "057ha6if07l20hfrc8zy7h7afzy287sza7wmp7k3z38j0rrmwpf5")))

