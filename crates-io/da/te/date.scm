(define-module (crates-io da te date) #:use-module (crates-io))

(define-public crate-date-0.0.1 (c (n "date") (v "0.0.1") (d (list (d (n "time") (r "^0.0.3") (d #t) (k 0)))) (h "0a6xfzhylq2n8gpfbq1vp9ws7xxqzzygz6lrflqn7ribga2y9xn0")))

(define-public crate-date-0.0.2 (c (n "date") (v "0.0.2") (d (list (d (n "time") (r "^0.0.3") (d #t) (k 0)))) (h "1p067v9rlzl35kxx9d1mhiv2az8p10gvmd1dhjs1kkv9xkvg8fb6")))

(define-public crate-date-0.0.3 (c (n "date") (v "0.0.3") (d (list (d (n "time") (r "^0.1.1") (d #t) (k 0)))) (h "09wfhc1f1429pk271wgimf4qz0gf56smvjxi7pag1s0bnpg4jrmb")))

(define-public crate-date-0.0.4 (c (n "date") (v "0.0.4") (d (list (d (n "time") (r "^0.1.2") (d #t) (k 0)))) (h "1169559ip0j30rjmsf86xfr74510m05hn32khqn8s3bklqy4nllx")))

(define-public crate-date-0.0.5 (c (n "date") (v "0.0.5") (d (list (d (n "time") (r "^0.1.4") (d #t) (k 0)))) (h "1f0pr9cgvga56zxpf9rfqcxny8sy9p21axlsgxsvgry9bj97xsz9")))

(define-public crate-date-0.0.6 (c (n "date") (v "0.0.6") (d (list (d (n "time") (r "^0.1.5") (d #t) (k 0)))) (h "1f3f001msa5r2n8mrkwra5n0cw49gbhznarshav2r40hzkb9yxfh")))

(define-public crate-date-0.0.7 (c (n "date") (v "0.0.7") (d (list (d (n "time") (r "^0.1.11") (d #t) (k 0)))) (h "1zvnc7n626vyayg6rirk5vjrzw5m14z07pm6dlppf67wmlqgjbkn")))

(define-public crate-date-0.1.0 (c (n "date") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1q11yfx1nfcgpb7f9w4zyx3p1v13lws18qplkwaj8kmw5b9664yb")))

(define-public crate-date-0.1.1 (c (n "date") (v "0.1.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0yl34m2l0fqxnv98nvsfqhwaa35147pvwj6siflhjyncmv52ib7c")))

(define-public crate-date-0.1.2 (c (n "date") (v "0.1.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1bqrrxjkwsmzlk5k3h1x7mlj06vb0igdhyri69gg88s2zmrlz7nr")))

(define-public crate-date-0.1.3 (c (n "date") (v "0.1.3") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0j7ryq1sd84d613bia84bznhrccgiinmiilk5ral108ysdkzchf3")))

