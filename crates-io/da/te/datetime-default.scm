(define-module (crates-io da te datetime-default) #:use-module (crates-io))

(define-public crate-datetime-default-0.1.0 (c (n "datetime-default") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("clock"))) (d #t) (k 0)))) (h "0a386d0zj36yap1bcq4ij2spkkqcsrf1cxfafrwi0ida4ncfpr7v")))

(define-public crate-datetime-default-0.1.1 (c (n "datetime-default") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (d #t) (k 0)))) (h "1cm3aia0j7cxxhnmrq66fj20gw4b7s5adhdhjfzdnjfigb42cpr0")))

(define-public crate-datetime-default-1.0.0 (c (n "datetime-default") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (d #t) (k 0)))) (h "151j3zkqzvxwm7s3m2cskm9w3q7nik3x123aid0dk6ry7ghx3pl7")))

(define-public crate-datetime-default-1.0.1 (c (n "datetime-default") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (d #t) (k 0)))) (h "1pc9lz08qvb5xyv6cg645zi0v9skbjya761qaax56pbbq83zval4")))

(define-public crate-datetime-default-1.1.0 (c (n "datetime-default") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (d #t) (k 0)) (d (n "schemars-08") (r "^0.8") (f (quote ("chrono" "derive"))) (o #t) (k 0) (p "schemars")) (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "0bz0jhk7006jn7g2p0v2mysvd0qwsrbxw253xxaihg41hpwlrsna") (f (quote (("schemars" "schemars-08") ("default"))))))

(define-public crate-datetime-default-1.1.1 (c (n "datetime-default") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("chrono" "derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08y8sbj2sm51xfsh6h9hwnz3mp9gy100qj562kxz0r0zqvmh1kqq") (f (quote (("default")))) (s 2) (e (quote (("schemars" "dep:schemars"))))))

