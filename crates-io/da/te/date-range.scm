(define-module (crates-io da te date-range) #:use-module (crates-io))

(define-public crate-date-range-0.0.0 (c (n "date-range") (v "0.0.0") (h "1fa6rjrxmp4bflr5j8jvmxyw43hfpkb98xfq4kirbsnn14sphmnh")))

(define-public crate-date-range-0.1.0 (c (n "date-range") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)))) (h "07n5cj719wkywkf8csyjbkmm6dhmg83vnf0f71q58c8qck4a10s0")))

(define-public crate-date-range-0.1.1 (c (n "date-range") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)))) (h "1ad6w7v8lhpxsxi6yvsdkh9y8q906avnvs3yf877czmn899yniz6")))

(define-public crate-date-range-0.2.0 (c (n "date-range") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)))) (h "1gzx5r1nl47w85pab6dk5dy9w5ghf8xlhyimhp75v8il854h6z7l")))

(define-public crate-date-range-0.2.1 (c (n "date-range") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)))) (h "1ach8ddrl3k3fcwl6vnkd4292mpcg4izrgpfajpvcy6657pf8iaa")))

(define-public crate-date-range-0.2.2 (c (n "date-range") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)))) (h "1jbrgzv5a99d8zw72bf9cfdyvgvyjm72j7x0i6r56bjzhhi8h9mw")))

(define-public crate-date-range-0.3.0 (c (n "date-range") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)))) (h "1lpgnrqg0p6gpaxm4irlj7j7j4ir4ahg6zvh1gl1pzdc7fvwacmk")))

