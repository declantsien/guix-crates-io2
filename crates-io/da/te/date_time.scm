(define-module (crates-io da te date_time) #:use-module (crates-io))

(define-public crate-date_time-1.2.0 (c (n "date_time") (v "1.2.0") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "0nkc4v2l7jwyhz83bhy8ngqr48p3c73qfqz651c5cpbhhwxm4ijz")))

(define-public crate-date_time-1.2.1 (c (n "date_time") (v "1.2.1") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "0kcf1ac94jpzkwd2mw6ilsj7912j03j6f324w5q3ya69gdpc03qs")))

(define-public crate-date_time-1.3.0 (c (n "date_time") (v "1.3.0") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "0q90yl2fzzsyyx7fy5spm71fp3v23x1xdiham0130170c72dmhjg")))

(define-public crate-date_time-1.4.0 (c (n "date_time") (v "1.4.0") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "0k4m3vh6bmjrc8sxarl09mn82p2qndbhpwj1bqhlbi865zbxlz1y")))

(define-public crate-date_time-1.4.1 (c (n "date_time") (v "1.4.1") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "0l01mar1jww8k849wnhsijisarg5il3z8zk1g2piqzz00fivbngk")))

(define-public crate-date_time-1.4.2 (c (n "date_time") (v "1.4.2") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "0azbmvbp3vb7bcfs514ng7lz1nwiynkrxa35h55jsh0c019809cm")))

(define-public crate-date_time-1.4.3 (c (n "date_time") (v "1.4.3") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "1y74sj9gwkm84vwkx00mr1y4xpq5mq6g638zknjx1v80nfmjcp6s")))

(define-public crate-date_time-1.4.4 (c (n "date_time") (v "1.4.4") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "076i2aisn1ixwcs8pl5d1az0fq0z18d7v841xz73fyf47bqlhl8l")))

(define-public crate-date_time-1.5.0 (c (n "date_time") (v "1.5.0") (d (list (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "0arv1qmzvd4xafrn5d48q26zvhfd6vfdbzijs37a3a3kfdf2vbjv")))

(define-public crate-date_time-2.0.0 (c (n "date_time") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "038mmhal4yrwfwbimxwp8fkavlxxk0nigdsiv47gyhalw9j527rw")))

(define-public crate-date_time-2.0.1 (c (n "date_time") (v "2.0.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "1k9qlb4brcpv7aikaakhjkzznlyzsm2k9xm66sc8xckr3bk4691b")))

(define-public crate-date_time-2.1.0 (c (n "date_time") (v "2.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "0lpsaxsq0hjz1zd5gmwm0yldy50wq0amzzbjnzdsac2gdg1vhfbc")))

(define-public crate-date_time-2.1.1 (c (n "date_time") (v "2.1.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "02f1lp3zn507f4p7yrbkya5c7pdnpfdf0sw9njj1bb5id8adr6gc")))

(define-public crate-date_time-2.2.0 (c (n "date_time") (v "2.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dvknh0bdhsz9zw8ss5bsydppk3yc7ck3cbcwhdr8mb13pkiha4l") (f (quote (("serde_support" "serde") ("default"))))))

