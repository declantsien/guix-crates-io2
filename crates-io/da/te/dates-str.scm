(define-module (crates-io da te dates-str) #:use-module (crates-io))

(define-public crate-dates-str-1.0.0 (c (n "dates-str") (v "1.0.0") (d (list (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "0x5c03jixxh98as1hs4j9ws1ffd7qbkvy0r3sz5141j63y4qk3hi")))

(define-public crate-dates-str-1.0.1 (c (n "dates-str") (v "1.0.1") (d (list (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "0fhay2kpazcjq97g7v3x7wwg4bhdn1rm56qf8x5kpns5753qragq")))

(define-public crate-dates-str-1.1.0 (c (n "dates-str") (v "1.1.0") (d (list (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "192kmx01m9rf281mgvfq0k15nkil9rly0i1xd0zr4dha9k4y5qfh")))

(define-public crate-dates-str-1.2.0 (c (n "dates-str") (v "1.2.0") (d (list (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "1k325601ys8r8h2n7hlq90sblw4bi190vhx3jgynq4ns4fn8zih9")))

(define-public crate-dates-str-1.3.0 (c (n "dates-str") (v "1.3.0") (d (list (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "1dbqxwgnz1rm20650gdp75dz6nhhhh7ry9maxd8b8rwr4ddrf0yw")))

(define-public crate-dates-str-1.4.2 (c (n "dates-str") (v "1.4.2") (d (list (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "1dwcbn5znkwq2cy667zkkqm6zzjgfbpsn09mk15599wra7vap1lr")))

(define-public crate-dates-str-1.4.3 (c (n "dates-str") (v "1.4.3") (d (list (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "1mfvx7ylvn52d3q2i0y59f4kz0kghn1wls8zxavxj4g7g5av01i8")))

(define-public crate-dates-str-1.4.4 (c (n "dates-str") (v "1.4.4") (d (list (d (n "snafu") (r "^0.8.2") (d #t) (k 0)))) (h "0kf5phbafa9smbk4b9s5vs9bgmrvj5bkj4ajafhwz9sznqjh37hw")))

