(define-module (crates-io da te date-time-matrix) #:use-module (crates-io))

(define-public crate-date-time-matrix-0.1.0 (c (n "date-time-matrix") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hour") (r "^0.1") (k 0)) (d (n "weekday") (r "^0.2") (k 0)))) (h "0b9v6by2lfa1pdkdwr04ma7cwjg1j7pczh6nccj2hkmymh6rc2wb") (f (quote (("with-chrono" "chrono" "weekday/with-chrono" "hour/with-chrono") ("default" "with-chrono"))))))

(define-public crate-date-time-matrix-0.1.1 (c (n "date-time-matrix") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hour") (r "^0.1") (k 0)) (d (n "weekday") (r "^0.2") (k 0)))) (h "0jr2y1rlaz08v40mmkr1rs5i0zpq0dcxyz6pgganfslrxwm1rdq1") (f (quote (("with-chrono" "chrono" "weekday/with-chrono" "hour/with-chrono") ("default" "with-chrono"))))))

(define-public crate-date-time-matrix-0.1.2 (c (n "date-time-matrix") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hour") (r "^0.1") (k 0)) (d (n "weekday") (r "^0.2") (k 0)))) (h "0ssxws4452kiqas91b3a528dwbn3d25s8q2ngr0n678sya2p1rr0") (f (quote (("with-chrono" "chrono" "weekday/with-chrono" "hour/with-chrono") ("default" "with-chrono"))))))

(define-public crate-date-time-matrix-0.1.3 (c (n "date-time-matrix") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hour") (r "^0.1") (k 0)) (d (n "weekday") (r "^0.2") (k 0)))) (h "14vhsy730dc8lyy2aj2l85hvs7h3a4ykrf2kb2pva1ycs13cpq1v") (f (quote (("with-chrono" "chrono" "weekday/with-chrono" "hour/with-chrono") ("default" "with-chrono"))))))

(define-public crate-date-time-matrix-0.1.4 (c (n "date-time-matrix") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hour") (r "^0.1") (k 0)) (d (n "weekday") (r "^0.3") (k 0)))) (h "0afh9zii3fr751rkrjp2hgs09520i68gck1dw9w213xck7v4kg33") (f (quote (("with-chrono" "chrono" "weekday/with-chrono" "hour/with-chrono") ("default" "with-chrono"))))))

