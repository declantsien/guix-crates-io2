(define-module (crates-io da te date_header) #:use-module (crates-io))

(define-public crate-date_header-1.0.3 (c (n "date_header") (v "1.0.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "regex") (r "^1.9.3") (d #t) (k 2)))) (h "06bv2m69j2xkb8zabzwbjig0x9d5yg5yvb949h22s4vjffi65mnn")))

(define-public crate-date_header-1.0.4 (c (n "date_header") (v "1.0.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "regex") (r "^1.9.3") (d #t) (k 2)))) (h "0qd35bclm2bh97smjnn6h49gq71zvb2096vcpm0m0a95542la85h")))

(define-public crate-date_header-1.0.5 (c (n "date_header") (v "1.0.5") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "regex") (r "^1.9.3") (d #t) (k 2)))) (h "078zb823qrzxj8milpk7vbmf303gmfk4nj7g4yqgnc0sxlbc80qc")))

