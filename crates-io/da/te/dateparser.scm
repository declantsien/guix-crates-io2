(define-module (crates-io da te dateparser) #:use-module (crates-io))

(define-public crate-dateparser-0.1.0 (c (n "dateparser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0d31dqlkc4ch7jpsr44ihhpcvfxz7gb9572p68slj1cd2g0qnadd")))

(define-public crate-dateparser-0.1.1 (c (n "dateparser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "06vkf6cwlc6wy7d5gdr4idnfznb59hxi6phximb6clqjwwz1b1iv")))

(define-public crate-dateparser-0.1.2 (c (n "dateparser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "063lrpp77ygplp51gl6fpnl1za4g9v7vbirjwci1qqyh5i43i0bb")))

(define-public crate-dateparser-0.1.3 (c (n "dateparser") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "15jibb3w3zfvg86ghypdczr4j8rcw0nbyzdm2x0gd0jngcv6m7hn")))

(define-public crate-dateparser-0.1.4 (c (n "dateparser") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0c5ya6n9di0yqi5798nj1zand9lgdm6g989pxan23d431qiirhfz")))

(define-public crate-dateparser-0.1.5 (c (n "dateparser") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1d7nwjz8s8r7ry7w8dzv6g2n4ryanv0zlif94zdq14zd4njy9drh")))

(define-public crate-dateparser-0.1.6 (c (n "dateparser") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1n1pyb561bf1zckm3a0wc1fqry98h8hryfdb5nlrpdv6p94fxp41")))

(define-public crate-dateparser-0.1.7 (c (n "dateparser") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0wbam92209zbqr4z6r3zpf4s7kp6ykcpp1qg01rxpbafnwrn89b3")))

(define-public crate-dateparser-0.1.8 (c (n "dateparser") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1i00pd2spxsz1d89n0ssf79q1w8ijx0p0rybx5d9z8lpzqhjmqf9")))

(define-public crate-dateparser-0.2.0 (c (n "dateparser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0zfggid1p82fygxk2rqww13rvppwxng2r6hajfpj4j7f8kdkgr8q")))

(define-public crate-dateparser-0.2.1 (c (n "dateparser") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0f22d7c6is9w5pi496zsp1k95vmdv65p6bm0v3nfb6p0xqglbvy2")))

