(define-module (crates-io da te date-formats-workflow) #:use-module (crates-io))

(define-public crate-date-formats-workflow-1.0.0 (c (n "date-formats-workflow") (v "1.0.0") (d (list (d (n "alfred") (r "^4.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1k3926zsps6cxy9hcylvli2d8pgplp740kvrzpb0dyrcslpgvic6")))

(define-public crate-date-formats-workflow-1.1.0 (c (n "date-formats-workflow") (v "1.1.0") (d (list (d (n "alfred") (r "^4.0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "14ldv25r6hw9g8mx9w4xp30vlpq8pa2f5fh85qhp6gxvrs9g163m")))

(define-public crate-date-formats-workflow-1.3.0 (c (n "date-formats-workflow") (v "1.3.0") (d (list (d (n "alfred") (r "^4.0.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dateparser") (r "^0.1.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1k8fr38hlkkw6qi6fkr4kisnp13w59pj5cw1dndv27i512abnyg1") (y #t)))

(define-public crate-date-formats-workflow-1.3.1 (c (n "date-formats-workflow") (v "1.3.1") (d (list (d (n "alfred") (r "^4.0.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "08yghs0r3cgsqlz388zhpiaqdq6q2hiznmwa996wc8rj6mw1hfwh")))

(define-public crate-date-formats-workflow-1.4.0 (c (n "date-formats-workflow") (v "1.4.0") (d (list (d (n "alfred") (r "^4.0.2") (d #t) (k 0)) (d (n "anydate") (r "^0.3.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1ps6xab5wg41lfc33xl0yi02rv00wcj5bhkj6cgw14cq1dibrda7")))

