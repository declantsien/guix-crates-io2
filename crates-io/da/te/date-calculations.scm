(define-module (crates-io da te date-calculations) #:use-module (crates-io))

(define-public crate-date-calculations-0.1.0 (c (n "date-calculations") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1mh1ymycq034z3rc50268p9g754cg6l9bmxfmg29dx7ypj14w164")))

(define-public crate-date-calculations-0.1.1 (c (n "date-calculations") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1rr8qnskx7xp4f066l794rz2khjnzl0ky33qma564q4jxhf8j72v")))

