(define-module (crates-io da te date_utils) #:use-module (crates-io))

(define-public crate-date_utils-0.0.1 (c (n "date_utils") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)))) (h "0sbqcj0ff7kzk6i32lw6d4z13j3wyyfsx0d39anarx3lcx1fhy2s")))

(define-public crate-date_utils-0.0.2 (c (n "date_utils") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "0v000xc4k91q9kighk0sb8vsilx2qaw8rd1mn6h60xmyjv7w5nc6")))

(define-public crate-date_utils-0.0.3 (c (n "date_utils") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "0sf3x3n1pan60zs37gcjdi0ckmw5bsxqyb6iks31qylhf2ch0jjs")))

(define-public crate-date_utils-0.0.4 (c (n "date_utils") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0g31c1c16a0h5ki8b6jz4ljdlkq242cav59qcr4bfz55k3khlg27") (f (quote (("year") ("period") ("now") ("month") ("minute") ("hour") ("full" "hour" "year" "month" "day" "minute" "now" "period") ("default" "common") ("day") ("common"))))))

