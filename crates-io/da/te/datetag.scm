(define-module (crates-io da te datetag) #:use-module (crates-io))

(define-public crate-datetag-0.1.0 (c (n "datetag") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "13f66027vp6l433pwp0bb8zkqiy1br5j4x22nid1289gmrzznc5q")))

(define-public crate-datetag-0.1.1 (c (n "datetag") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "10601vpmzqgv95rinx0visiwdnp8fp017zz0a33nibj0jhcdiin2")))

(define-public crate-datetag-0.1.2 (c (n "datetag") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1vc2qjdzb72pmab5i0zskj9akdcpdz73axy3p623n7gs8cwpwgb1")))

(define-public crate-datetag-0.1.3 (c (n "datetag") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0hl57qddkrfjicfj4w6ji7g2gir6yi4frgi2cpal87s32yfnw7qb")))

(define-public crate-datetag-0.1.4 (c (n "datetag") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cv4y53r1hvqf85mwykrml0gsdh47hqlpcm8pqy6405acsgd13al")))

(define-public crate-datetag-0.1.5 (c (n "datetag") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "04sscwp6inpvpiczhpg11c1801ia44kcbi4p3ik0518yiq7vw72q")))

(define-public crate-datetag-0.1.6 (c (n "datetag") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-markdown") (r "^0.1.3") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)))) (h "0df71qlhn1b6dsg3qi4kbd8bgr6xaly9f373r94p55w82hcwldzy")))

(define-public crate-datetag-0.1.7 (c (n "datetag") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-markdown") (r "^0.1.3") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "09vamzsi4g89z9gxp74f83z7iv8gng0jmwfs6whr30zvhg6wmq3l")))

(define-public crate-datetag-0.1.8 (c (n "datetag") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-markdown") (r "^0.1.3") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "1m3k771n2iczsl5l7jzdii76r80kmhyvv4dvszlq0qb6czrn3c2a")))

(define-public crate-datetag-0.2.0 (c (n "datetag") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.6") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "07hww3pc1bgxn4n3mk3ra0qyafdw18byg09qp0bdh71xycjl7rsb")))

(define-public crate-datetag-0.2.1 (c (n "datetag") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.6") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (t "cfg(windows)") (k 1)))) (h "05mrxw5ghhhy8m9y98yywx7w52fp5zinhhjmydf7n7490h761qhj")))

(define-public crate-datetag-0.3.0 (c (n "datetag") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "assert_fs") (r "^1.1.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.6") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (t "cfg(windows)") (k 1)))) (h "16fs9v0s4ggfq85vrw3badg35rcvab8jh2kz6c6jxkhf20bzwwjf")))

