(define-module (crates-io da te date_validation_types) #:use-module (crates-io))

(define-public crate-date_validation_types-2.0.0 (c (n "date_validation_types") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display" "from" "into"))) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "10h22y79iaarx2h65183bl9gyqmnmpwmfwjralb62al961nsm1qr") (f (quote (("chrono")))) (r "1.60.0")))

