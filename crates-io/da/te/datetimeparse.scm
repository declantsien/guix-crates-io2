(define-module (crates-io da te datetimeparse) #:use-module (crates-io))

(define-public crate-datetimeparse-0.1.0 (c (n "datetimeparse") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1azmnbakg7q8xqr2mi93h0l8d8gnk8cqgcyg5nrh5ywr36k3mvi3") (f (quote (("default" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-datetimeparse-0.2.0 (c (n "datetimeparse") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1rg4809bsq75543k9rl7nvag34h7vji7sb56plsvbpx7qsjbxj5l") (f (quote (("default" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-datetimeparse-0.3.0 (c (n "datetimeparse") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "131g6xkz90vk0nc29d0cyibgd9ywsydwf8p3xgf6x7a478lzqk7g") (f (quote (("default" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

