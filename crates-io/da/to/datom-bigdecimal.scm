(define-module (crates-io da to datom-bigdecimal) #:use-module (crates-io))

(define-public crate-datom-bigdecimal-0.3.0 (c (n "datom-bigdecimal") (v "0.3.0") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "07ccv9qj0qj8l66jlv7r19xisbawwsjd406x8j7jl7smvmxib73i") (f (quote (("string-only"))))))

(define-public crate-datom-bigdecimal-0.3.1 (c (n "datom-bigdecimal") (v "0.3.1") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0941914i4apajhbmwaxxhzbbrqz4g0hkrsjqrl4j3cwpk30vxf42") (f (quote (("string-only"))))))

