(define-module (crates-io da da dadada) #:use-module (crates-io))

(define-public crate-dadada-0.9.2 (c (n "dadada") (v "0.9.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.3") (d #t) (k 0)))) (h "11y9rh0cii3hs1i2f5cg9vdgxqfmjasrw52rk28rfpkndyzdd1h6")))

(define-public crate-dadada-0.9.4 (c (n "dadada") (v "0.9.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.3") (d #t) (k 0)))) (h "0fdlxhxk9qwpsrafw9428gdabb41fir8icdiksdv4mk1q181z6m2")))

