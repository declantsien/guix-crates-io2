(define-module (crates-io da da dada-poem-generator) #:use-module (crates-io))

(define-public crate-dada-poem-generator-0.1.0 (c (n "dada-poem-generator") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "04m2namqgvhi8iylfsj44d4yl5crx5msq66r7ps0blxdz4zmykz2")))

(define-public crate-dada-poem-generator-0.1.1 (c (n "dada-poem-generator") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "070hb2acf2nkc4x761mfm927mw1as7w0ww37dqw5hyf69pdr6h26")))

