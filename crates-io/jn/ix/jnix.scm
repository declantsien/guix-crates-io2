(define-module (crates-io jn ix jnix) #:use-module (crates-io))

(define-public crate-jnix-0.1.0 (c (n "jnix") (v "0.1.0") (d (list (d (n "jni") (r "^0.14") (d #t) (k 0)) (d (n "jnix-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "jnix-macros") (r "^0.1.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "0snkn0r9wq1413nyhbw8ri2gzncfm1qwkzhihfif4i675nhs3d8l") (f (quote (("derive" "jnix-macros"))))))

(define-public crate-jnix-0.1.1 (c (n "jnix") (v "0.1.1") (d (list (d (n "jni") (r "^0.14") (d #t) (k 0)) (d (n "jnix-macros") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "jnix-macros") (r "^0.1.1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "0s3cy107spwavww36azw6ywwivnrsmdmsq29dv3i2490mxqlw90c") (f (quote (("derive" "jnix-macros"))))))

(define-public crate-jnix-0.1.2 (c (n "jnix") (v "0.1.2") (d (list (d (n "jni") (r "^0.14") (d #t) (k 0)) (d (n "jnix-macros") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "jnix-macros") (r "^0.1.2") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "06p6by0shfcs1f2iwsmpy01py78qd4cxpgix206r7g4aabzmhjs3") (f (quote (("derive" "jnix-macros"))))))

(define-public crate-jnix-0.2.0 (c (n "jnix") (v "0.2.0") (d (list (d (n "jni") (r "^0.14") (d #t) (k 0)) (d (n "jnix-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "jnix-macros") (r "^0.2.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "0ksrbgfgaa17prvcjv7my9s8jxm9shl3l07nia6wwcwn4jkkj3mz") (f (quote (("derive" "jnix-macros"))))))

(define-public crate-jnix-0.2.1 (c (n "jnix") (v "0.2.1") (d (list (d (n "jni") (r "^0.14") (d #t) (k 0)) (d (n "jnix-macros") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "jnix-macros") (r "^0.2.1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "125b04ssaqcgvhqnqixhk82rf3blmbh1r5jg0pc9izcwcix25nbq") (f (quote (("derive" "jnix-macros"))))))

(define-public crate-jnix-0.2.2 (c (n "jnix") (v "0.2.2") (d (list (d (n "jni") (r "^0.14") (d #t) (k 0)) (d (n "jnix-macros") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "jnix-macros") (r "^0.2.2") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "1v5iq96802dwc7yglcggi4xbvi71g8ir1whpgvac3f6krjlwh9nd") (f (quote (("derive" "jnix-macros"))))))

(define-public crate-jnix-0.2.3 (c (n "jnix") (v "0.2.3") (d (list (d (n "jni") (r "^0.14") (d #t) (k 0)) (d (n "jnix-macros") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "jnix-macros") (r "^0.2.3") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "0mxw5an4g4vcc3c0q30xs48qqkr1nizdfb5fh4hripx47j9awm33") (f (quote (("derive" "jnix-macros"))))))

(define-public crate-jnix-0.2.4 (c (n "jnix") (v "0.2.4") (d (list (d (n "jni") (r "^0.14") (d #t) (k 0)) (d (n "jnix-macros") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "jnix-macros") (r "^0.2.4") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "0nxj3d8pcfg8r8z2sh2j4byv392d610iafmj33sw45fcsf9w8jsq") (f (quote (("derive" "jnix-macros"))))))

(define-public crate-jnix-0.3.0 (c (n "jnix") (v "0.3.0") (d (list (d (n "jni") (r ">=0.14.0, <0.15.0") (d #t) (k 0)) (d (n "jnix-macros") (r ">=0.3.0, <0.4.0") (o #t) (d #t) (k 0)) (d (n "jnix-macros") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.9.0, <0.10.0") (d #t) (k 0)))) (h "12ny0h85hjmydk607xgnxdcnfw1mf2v73pvx5nzlgl8aamhh6q8w") (f (quote (("derive" "jnix-macros"))))))

(define-public crate-jnix-0.4.0 (c (n "jnix") (v "0.4.0") (d (list (d (n "jni") (r "^0.14") (d #t) (k 0)) (d (n "jnix-macros") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "jnix-macros") (r "^0.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0i813bky9nlam7nsm3wvx5ay9vn7a2n81lg89jgx71scjgpah6px") (f (quote (("derive" "jnix-macros"))))))

(define-public crate-jnix-0.5.0 (c (n "jnix") (v "0.5.0") (d (list (d (n "jni") (r "^0.19") (d #t) (k 0)) (d (n "jnix-macros") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "jnix-macros") (r "^0.4.1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1z0az9zvzrlv12clh1xjq35r8rsbwig9iqp9wrsxwn8dhi0sgkxf") (f (quote (("derive" "jnix-macros"))))))

(define-public crate-jnix-0.5.1 (c (n "jnix") (v "0.5.1") (d (list (d (n "jni") (r "^0.19") (d #t) (k 0)) (d (n "jnix-macros") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "jnix-macros") (r "^0.4.1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "16gmm79ppd5rmawybnc0570g2b9zbrz0vlnyaswqwmj83va9gmsz") (f (quote (("derive" "jnix-macros"))))))

