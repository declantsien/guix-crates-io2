(define-module (crates-io jn ix jnix-macros) #:use-module (crates-io))

(define-public crate-jnix-macros-0.1.0 (c (n "jnix-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0im9a9xfxz11csqmlp6ij7ggvpw6yvziz1drf4wlv3v2ww01nf7n")))

(define-public crate-jnix-macros-0.1.1 (c (n "jnix-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1za3cmmxj8n8lkask5sbp9fsijjbb0114gxqhq35gdapp2g4jcls")))

(define-public crate-jnix-macros-0.1.2 (c (n "jnix-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hd6dgqdbjx79bdxf5ixrcqldcricp610q165x28vcaqvmgxb4rr")))

(define-public crate-jnix-macros-0.2.0 (c (n "jnix-macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p9c5dirw9s5gcprq9pbcz6ngqi7snwy9ddqhm8dkrsn4lav7dgw")))

(define-public crate-jnix-macros-0.2.1 (c (n "jnix-macros") (v "0.2.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xz1vhldazxhd6i9c8k77wd0s2qcj86anzl63f0k6yb2ymlgixhb")))

(define-public crate-jnix-macros-0.2.2 (c (n "jnix-macros") (v "0.2.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13vg9mdrir0wnvcsqj31ir82i8425rhsmd24linsniahj39d26d8")))

(define-public crate-jnix-macros-0.2.3 (c (n "jnix-macros") (v "0.2.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0i0bfzz91qrwqnhyi12m7fpbh2lv8jlwv3si60xz2mh18bz350f7")))

(define-public crate-jnix-macros-0.2.4 (c (n "jnix-macros") (v "0.2.4") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h2293m6497y9k6bzi2mbnhjx5bb9945y8b44dg8hcp20a68mcbk")))

(define-public crate-jnix-macros-0.3.0 (c (n "jnix-macros") (v "0.3.0") (d (list (d (n "heck") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l3j2payvj0fbml3nh1mc2sjpvpmlglpacaf12gikif1xpq7zs0k")))

(define-public crate-jnix-macros-0.4.0 (c (n "jnix-macros") (v "0.4.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fidvi30jryfgyrvyi4aq7kimcljwrrbj3r82m1ph0ksgr28r8k6")))

(define-public crate-jnix-macros-0.4.1 (c (n "jnix-macros") (v "0.4.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1y7zp9rps4iv27zripljlkcq2vzw3z9w129l7z1qiblpdpz4sbq0")))

