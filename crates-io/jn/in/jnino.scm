(define-module (crates-io jn in jnino) #:use-module (crates-io))

(define-public crate-jnino-0.1.0 (c (n "jnino") (v "0.1.0") (d (list (d (n "derive_jface") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lzb1xsgl8f13s3r23fhh5g0f4gwi6c18nk3q0rbqfwn0nqw920x")))

(define-public crate-jnino-0.1.1 (c (n "jnino") (v "0.1.1") (d (list (d (n "derive_jface") (r "^0.1") (d #t) (k 0)) (d (n "jni") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0d8038323qa9y2z4rvdk006xmwpd8pbvxf3b1j1ppyb297182iv5")))

