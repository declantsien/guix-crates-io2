(define-module (crates-io jn i- jni-glue) #:use-module (crates-io))

(define-public crate-jni-glue-0.0.2 (c (n "jni-glue") (v "0.0.2") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "05f8nnfkj9kr8n7cfb98kp48nhfskjvg1zi2gq5z6ryvg4d9pb73") (f (quote (("unsafe-manual-jni-load-unload") ("default"))))))

(define-public crate-jni-glue-0.0.3 (c (n "jni-glue") (v "0.0.3") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "15nmn1c5ymqhlyg1lp7mghqadmm7gzz1jl838xkhbqwq1b8g641a") (f (quote (("unsafe-manual-jni-load-unload") ("default"))))))

(define-public crate-jni-glue-0.0.5 (c (n "jni-glue") (v "0.0.5") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1088vnf2789ggs5gmf6nxhxrajl2cwwa871gnbcl11pz9pgn9f73") (f (quote (("unsafe-manual-jni-load-unload") ("default"))))))

(define-public crate-jni-glue-0.0.6 (c (n "jni-glue") (v "0.0.6") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "01yn4zb68issii97imi99whskmgk4zlrxnp1vp82q650vh2gcvmf") (f (quote (("unsafe-manual-jni-load-unload") ("default"))))))

(define-public crate-jni-glue-0.0.7 (c (n "jni-glue") (v "0.0.7") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0apingylbixwd34pwccdh6lpzw4irjirsyiqlkpz778w9mr2by71") (f (quote (("unsafe-manual-jni-load-unload") ("default"))))))

(define-public crate-jni-glue-0.0.8 (c (n "jni-glue") (v "0.0.8") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1xslmgwilnda1s4jb7vlnlj45b24fp5zkbmhlz8h3iiwkx8dx56v") (f (quote (("unsafe-manual-jni-load-unload") ("default"))))))

(define-public crate-jni-glue-0.0.9 (c (n "jni-glue") (v "0.0.9") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1632dkxxndmj4yq630vkqz15xn1c1w708clgyzi99id4wh63pyz9") (f (quote (("unsafe-manual-jni-load-unload") ("default"))))))

(define-public crate-jni-glue-0.0.10 (c (n "jni-glue") (v "0.0.10") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "054kc2hkdfjiihy7ssrn97s9hs35c2v32ph2h0jlv4vkazx39ddb") (f (quote (("unsafe-manual-jni-load-unload") ("default"))))))

