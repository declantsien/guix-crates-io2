(define-module (crates-io jn i- jni-mangle) #:use-module (crates-io))

(define-public crate-jni-mangle-0.1.0 (c (n "jni-mangle") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "1qh8hqdg1imx9m9q4krppvd0h0bg1l9jbs21cvcw5hrfgvfapgcw")))

(define-public crate-jni-mangle-0.1.1 (c (n "jni-mangle") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "1mm9pbf1rcg2lvfwdhhdcp84jrkzjl240q2xwn8gql2gakssgbrg")))

(define-public crate-jni-mangle-0.1.2 (c (n "jni-mangle") (v "0.1.2") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "1hn9qfkkx6c232kr6aw0n10ak547gls6c8sqsn651pln5azsli2g")))

