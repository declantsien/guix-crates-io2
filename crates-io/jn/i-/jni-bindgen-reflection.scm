(define-module (crates-io jn i- jni-bindgen-reflection) #:use-module (crates-io))

(define-public crate-jni-bindgen-reflection-0.0.10 (c (n "jni-bindgen-reflection") (v "0.0.10") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "bugsalot") (r "^0.2.0") (d #t) (k 0)))) (h "1im9c1y2acjx2mcfk6l7ldb7brmjkjmnag16d9r4zrf18wn2pxk6")))

