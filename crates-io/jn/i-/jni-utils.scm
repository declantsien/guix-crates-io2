(define-module (crates-io jn i- jni-utils) #:use-module (crates-io))

(define-public crate-jni-utils-0.1.0 (c (n "jni-utils") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (d #t) (k 0)))) (h "0mxlrm4s8j01sw5xnya0k3dyc1r2xwmjk1garfdxvgkhhiih0fhd") (f (quote (("build-java-support"))))))

(define-public crate-jni-utils-0.1.1 (c (n "jni-utils") (v "0.1.1") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (d #t) (k 0)))) (h "1i56wy306kdcyn21vvfy1aq7mw0i0mk00w0l3y8xwqdd7qn9z7i5") (f (quote (("build-java-support"))))))

