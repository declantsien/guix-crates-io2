(define-module (crates-io jn i- jni-sys) #:use-module (crates-io))

(define-public crate-jni-sys-0.1.0 (c (n "jni-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14rgjaps5qg2fkxi3h35mv1kwmv1p3qgmmk69ha2xn31ms7w69cs")))

(define-public crate-jni-sys-0.2.0 (c (n "jni-sys") (v "0.2.0") (h "1v25ghxvsfzbq05rxgfcnh40yq95q2a00lhmic8f6ihw1gzhzhza") (y #t)))

(define-public crate-jni-sys-0.2.1 (c (n "jni-sys") (v "0.2.1") (h "1isjrw6kd38xn7b9a5sbnv774ribwcr7swdw331s4qncxvxl22ai")))

(define-public crate-jni-sys-0.2.2 (c (n "jni-sys") (v "0.2.2") (h "01n2jxcijz78ycwljvr739732n20r38518ixhcjknwr58snhzfl3")))

(define-public crate-jni-sys-0.2.3 (c (n "jni-sys") (v "0.2.3") (h "1ssr07888in5wg8yxxpzvsk25csb63p0cr91yahyfrzazflrzzrf")))

(define-public crate-jni-sys-0.2.4 (c (n "jni-sys") (v "0.2.4") (h "1w175n254ybzl0r1y3a33h7d9fpnxd9gmwa274xnxbsq9fxiznm7")))

(define-public crate-jni-sys-0.2.5 (c (n "jni-sys") (v "0.2.5") (h "1f4ir8yk5m0ayzlylmdq6a39xdwnp49v7qizln1qvaq9i2xal2ny")))

(define-public crate-jni-sys-0.3.0 (c (n "jni-sys") (v "0.3.0") (h "0c01zb9ygvwg9wdx2fii2d39myzprnpqqhy7yizxvjqp5p04pbwf")))

(define-public crate-jni-sys-0.4.0 (c (n "jni-sys") (v "0.4.0") (d (list (d (n "jni-sys-macros") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "065q4c15wrx80w9d64sd9adizwrkmz0lazc9w2hmd39bg0nk22n3") (r "1.65.0")))

