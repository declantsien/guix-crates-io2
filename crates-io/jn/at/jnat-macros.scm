(define-module (crates-io jn at jnat-macros) #:use-module (crates-io))

(define-public crate-jnat-macros-0.1.0 (c (n "jnat-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0p54d0kzlh19vf08wgv5c7019s76gaky4vxp4nk7i6r46scgqnl4")))

(define-public crate-jnat-macros-0.2.0 (c (n "jnat-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1l9xsdmkjvf50m6f6gmw6xqvyq8gl1a8jgfib4d71dbpirsyvq7h")))

