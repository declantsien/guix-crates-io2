(define-module (crates-io jn at jnat) #:use-module (crates-io))

(define-public crate-jnat-0.1.0 (c (n "jnat") (v "0.1.0") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "04i0rj000bgabwhr80z54jc9p9qpbmkc13296qaispp21prypqan")))

(define-public crate-jnat-0.1.1 (c (n "jnat") (v "0.1.1") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "007x0rlc4h6h1048i0kbgyq3cnz0ls3yr6lasv4z4azxkc3mw0f9")))

(define-public crate-jnat-0.1.2 (c (n "jnat") (v "0.1.2") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "0zi744fd6ywgy0pchpll63abz6q65l055509m6sxgjbwdgii1ygp")))

(define-public crate-jnat-0.2.0 (c (n "jnat") (v "0.2.0") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "1252ngg1i7qi67nwykqbrzzz3ixpvmjc91jjzx8czf36f01p53bc")))

(define-public crate-jnat-0.2.1 (c (n "jnat") (v "0.2.1") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "13jhrqw221nla62jin146smw6d2mxwpr3zwgj6ixgbvzc67yiy2i")))

(define-public crate-jnat-0.2.2 (c (n "jnat") (v "0.2.2") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "0hfarq5bwp6yapswzvaclfvr2fbzv6zgwnllj3s0lxjwgfcdv8ny")))

(define-public crate-jnat-0.2.3 (c (n "jnat") (v "0.2.3") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "1c4lgy6k27j0l4wwfs01jdyak1nhwp61svmd40vd486sl7kvj6hk") (f (quote (("jni") ("default" "jni"))))))

(define-public crate-jnat-0.2.4 (c (n "jnat") (v "0.2.4") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "1kp05zfjjbicfz43fqs66r461ryswzyw2hz8hqnvh0rgjxnlw3n3") (f (quote (("jni") ("default" "jni"))))))

(define-public crate-jnat-0.3.0 (c (n "jnat") (v "0.3.0") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "1pkridnpzbl5fccdalqk0f1gfj076mgjplspxahjjgv50ddlsx0a") (f (quote (("jni") ("default" "jni"))))))

(define-public crate-jnat-0.4.0 (c (n "jnat") (v "0.4.0") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "1bhldjsp2na3b9mp7xia6yf3xk10m8vdh35lhscahmk0g1qaq15k") (f (quote (("jni") ("default" "jni"))))))

(define-public crate-jnat-0.4.1 (c (n "jnat") (v "0.4.1") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "0wfw7zjxg7yzdxj1530sbm36y4lcr559xbd52jyqp7pyrgw7p6sk") (f (quote (("jni") ("default" "jni"))))))

(define-public crate-jnat-0.4.2 (c (n "jnat") (v "0.4.2") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "1vpx3fcvc6hip03y53nmcb8ch8bkgvzjr9f8mkhwyb116rgkvrik") (f (quote (("jni") ("default" "jni"))))))

(define-public crate-jnat-0.4.3 (c (n "jnat") (v "0.4.3") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "1yj6l55j0c7nxb8xa6y8rwglxqxg3dr8ar7j1pk91wwcaa4sl5il") (f (quote (("jni") ("default" "jni"))))))

(define-public crate-jnat-0.5.0 (c (n "jnat") (v "0.5.0") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "14ij55j4sc9cxhv7bmj0w6pgmps6jam859j19zcyh6p4kr1y322b") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.5.1 (c (n "jnat") (v "0.5.1") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "10xhnckzn8qw4y5yly418rvqba932a5b7i8qdfgh8xgxh8wpvx0r") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.6.0 (c (n "jnat") (v "0.6.0") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "0hpggr9g3xkp4d7hp0hrszrr4zg7wc0fj0bs4gph8b0v6m6r5479") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.6.1 (c (n "jnat") (v "0.6.1") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "08mpqkkak7bvxhzixn00dpzylv6syk1q1plc0cxgmkqxgnj6jqkq") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.7.0 (c (n "jnat") (v "0.7.0") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "18a26kqq4x0nd0r713sl325kwfl5mv6c8w5nw27v1395ksvran0k") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.7.1 (c (n "jnat") (v "0.7.1") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "059zdjlcy2fpjid456fihbsrkwcznldi6wzl73d8wv76j227wcq8") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.7.2 (c (n "jnat") (v "0.7.2") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "0aymdnbapvq7drw2sl3kqyjbsizkc0hwpv0kgjswgl5k7kbh33cz") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.8.0 (c (n "jnat") (v "0.8.0") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "191dlcvg1mi0zyw7p4c7kcfv0bxr80pdswkcdsdn7n5qrdmrmfmj") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.8.1 (c (n "jnat") (v "0.8.1") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "15czgj4brs8scg6zyzqyfw520zm1zb47bganipncdh6k3r5035a1") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.9.0 (c (n "jnat") (v "0.9.0") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "1qmdk97m50afgqpq3jzpd3yflmqxfzgpc3sv1fqy959h3g0c3103") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.10.0 (c (n "jnat") (v "0.10.0") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "1vwq5lnarv3hsdq5izvvnbxygbrkb78s79f56iqf9bi77bjqsbdr") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.10.1 (c (n "jnat") (v "0.10.1") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "19v7iqwvjxy6xx8fmanckhrvyc8qnic9zc7z9bfj4gbrqxh3cjlr") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.10.2 (c (n "jnat") (v "0.10.2") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "0ggddpv667y2hlfzjcsvrjjbgzgchgppmjjkrkpmaz9ndfqi6rbl") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.11.0 (c (n "jnat") (v "0.11.0") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "0wbm3bhviq9m2w6b1md7wgfq7lisqcxav6hr6iyvxxm5m16q3jwz") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.11.1 (c (n "jnat") (v "0.11.1") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "1zszb29gbs05fln5v1nkqmsbd7bialzwmpv9451i0qcdidzpjqmq") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

(define-public crate-jnat-0.11.2 (c (n "jnat") (v "0.11.2") (d (list (d (n "inventory") (r "^0.3.11") (d #t) (k 2)) (d (n "jnat-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)))) (h "0bwbg761cg08irhr0ljn04rq2k30m4a1rpxg0zrapz8nnqhjsn95") (f (quote (("jni") ("default" "jni" "jnat-macros"))))))

