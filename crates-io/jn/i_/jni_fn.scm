(define-module (crates-io jn i_ jni_fn) #:use-module (crates-io))

(define-public crate-jni_fn-0.1.0 (c (n "jni_fn") (v "0.1.0") (d (list (d (n "jni") (r "^0.19") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xbvbxgxvsh3092y7wbhmfpwy03igij085bds7biajjjc8chw0sf")))

(define-public crate-jni_fn-0.1.1 (c (n "jni_fn") (v "0.1.1") (d (list (d (n "jni") (r "^0.19") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c167f7gp6p6zkdsvp4vgffp5vzin152ddbhbp6nq4q21b061rgy")))

(define-public crate-jni_fn-0.1.2 (c (n "jni_fn") (v "0.1.2") (d (list (d (n "jni") (r "^0.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ya2i4z7j1z5gsrqmi8ls0qxbgavxnlwksiwj8xksks2kbhzf9wh")))

