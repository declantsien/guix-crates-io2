(define-module (crates-io ee pr eeprom24x) #:use-module (crates-io))

(define-public crate-eeprom24x-0.1.0 (c (n "eeprom24x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "14lgi3621kpxywl1z1aamhdcbsnjw1ww3jfgwn458j9rbfnz94w2") (f (quote (("default"))))))

(define-public crate-eeprom24x-0.1.1 (c (n "eeprom24x") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "14rh2ymf6hgpnbysd8cmg6yr7dkhw3s62cnx0nfn8dk6lp31wdh3") (f (quote (("default"))))))

(define-public crate-eeprom24x-0.2.0 (c (n "eeprom24x") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1li3ld6ixsvalap94flhs2x4wdn1x87j8ixqrhpp0f2ankbyhm1h")))

(define-public crate-eeprom24x-0.2.1 (c (n "eeprom24x") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0zyjczf783j4fna5x52a5m974qvhcyfq7qsgdywkp522rkn34aqm")))

(define-public crate-eeprom24x-0.3.0 (c (n "eeprom24x") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "01s1sjc67smxizpnij1005vmaapj2xgv5yn50kg9g6jm3bcfi07n")))

(define-public crate-eeprom24x-0.4.0 (c (n "eeprom24x") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0qr0f4sipgckrjlvfzyd5l3aywgjqvyfgb8c5fkpdsqrrli68223")))

(define-public crate-eeprom24x-0.5.0 (c (n "eeprom24x") (v "0.5.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "embedded-storage") (r "^0.2.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 2)))) (h "1qpkh1hvaj73r68qxvpc9fzrk364y2ck74lxhm9dn6dryw8fyzbr")))

(define-public crate-eeprom24x-0.6.0 (c (n "eeprom24x") (v "0.6.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)) (d (n "embedded-storage") (r "^0.3.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 2)))) (h "17aj0rdmz0srf0m16jxm9sg09bhghqw70qvhl86icr22vb3q5gxz")))

(define-public crate-eeprom24x-0.6.1 (c (n "eeprom24x") (v "0.6.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)) (d (n "embedded-storage") (r "^0.3.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1.1") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 2)))) (h "04fb7zc9gsdqpy9z89pp13q3962xyi38a4c7id6mffq3fqfhf2js")))

(define-public crate-eeprom24x-0.7.0 (c (n "eeprom24x") (v "0.7.0") (d (list (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1"))) (d #t) (k 2)) (d (n "embedded-storage") (r "^0.3.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)))) (h "1276k8p0h063p3g88b3j3i5g8krqwa9i2k783mqkihpfmsq4czq3")))

(define-public crate-eeprom24x-0.7.1 (c (n "eeprom24x") (v "0.7.1") (d (list (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1"))) (d #t) (k 2)) (d (n "embedded-storage") (r "^0.3.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)))) (h "1lggyk4kvdyp55c0cfznjwaa8qdz4y9k9bvm4l8g2lixix6lf4kj")))

(define-public crate-eeprom24x-0.7.2 (c (n "eeprom24x") (v "0.7.2") (d (list (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1"))) (d #t) (k 2)) (d (n "embedded-storage") (r "^0.3.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)))) (h "1vwfbbqh9b7q35g84fx3vmjy3yrrvgdw1rxmgc4jkzw3lay32ylz") (s 2) (e (quote (("defmt-03" "dep:defmt" "embedded-hal/defmt-03"))))))

