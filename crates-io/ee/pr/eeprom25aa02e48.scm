(define-module (crates-io ee pr eeprom25aa02e48) #:use-module (crates-io))

(define-public crate-eeprom25aa02e48-0.1.0 (c (n "eeprom25aa02e48") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "ftd2xx-embedded-hal") (r "^0.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "16qr5hdwvlzdd0q4kkr3sk1arydlbdzl5c4nzj99h3gm07wm2b6c")))

(define-public crate-eeprom25aa02e48-0.2.0 (c (n "eeprom25aa02e48") (v "0.2.0") (d (list (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "~0.7") (d #t) (k 2)) (d (n "ftd2xx-embedded-hal") (r "~0.3") (d #t) (k 2)) (d (n "version-sync") (r "~0.9") (d #t) (k 2)))) (h "17xc3sx5srbrm3z7kj1j63cckb44b6yxbmh3zvryvs0ap7dwn8yf")))

(define-public crate-eeprom25aa02e48-1.0.0 (c (n "eeprom25aa02e48") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)) (d (n "ftdi-embedded-hal") (r "^0.21") (f (quote ("libftd2xx"))) (d #t) (k 2)))) (h "0mgnq6x330b8q304pbfmlnflc8myz8pmsw3c7dy2kl495ayq900z")))

(define-public crate-eeprom25aa02e48-1.0.1 (c (n "eeprom25aa02e48") (v "1.0.1") (d (list (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)) (d (n "ftdi-embedded-hal") (r "^0.21") (f (quote ("libftd2xx"))) (d #t) (k 2)))) (h "1y1vpcan2yhzhp57lwcb17rrjj3fyld16xjvy6hhwyjwm297sfwc")))

