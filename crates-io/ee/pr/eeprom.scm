(define-module (crates-io ee pr eeprom) #:use-module (crates-io))

(define-public crate-eeprom-0.1.0 (c (n "eeprom") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.3.4") (d #t) (k 2)) (d (n "stm32-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "stm32f103xx") (r "^0.7.5") (o #t) (d #t) (k 0)))) (h "0d9p1j24zv2wqsf2pir2nmmchlir92v3av0jxiaynbdz2iih6ml4") (f (quote (("use-stm32f103xx" "stm32-hal/use-stm32f103xx" "stm32f103xx") ("default-eeprom") ("default" "default-eeprom"))))))

(define-public crate-eeprom-0.2.0 (c (n "eeprom") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "stm32f1xx-hal") (r "^0.8.0") (f (quote ("stm32f103"))) (o #t) (d #t) (k 0)))) (h "02zyad383r4w8b4kmjlw639kzb5yk62v7066frkq855pknarvfnb") (f (quote (("stm32f103" "stm32f1xx-hal" "stm32f1xx-hal/stm32f103"))))))

(define-public crate-eeprom-0.3.0 (c (n "eeprom") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "stm32f1xx-hal") (r "^0.9.0") (f (quote ("stm32f103"))) (o #t) (d #t) (k 0)))) (h "1shjrvbdqyga314wrdpf0gvmwlvzcz6193a4pll2j1cxlvp7kmd1") (f (quote (("stm32f103" "stm32f1xx-hal" "stm32f1xx-hal/stm32f103"))))))

(define-public crate-eeprom-0.3.1 (c (n "eeprom") (v "0.3.1") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "stm32f1xx-hal") (r "^0.9.0") (f (quote ("stm32f103"))) (o #t) (d #t) (k 0)))) (h "14h25yw5qjp5hvcn11z2jnji2lf5i2a8jr869ppicch6wcdayzyq") (f (quote (("stm32f103" "stm32f1xx-hal" "stm32f1xx-hal/stm32f103"))))))

