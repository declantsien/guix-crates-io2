(define-module (crates-io ee lu eelu-login) #:use-module (crates-io))

(define-public crate-eelu-login-0.1.3 (c (n "eelu-login") (v "0.1.3") (d (list (d (n "open") (r "^4.0.1") (d #t) (k 0)) (d (n "sis-login") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (d #t) (k 0)))) (h "1wvqm5r93j065kavsi18397ayns1nzbx4dfq3rmp0b1hlvqi6rjk")))

(define-public crate-eelu-login-0.2.0 (c (n "eelu-login") (v "0.2.0") (d (list (d (n "open") (r "^4.0.1") (d #t) (k 0)) (d (n "sis-login") (r "^0.2.3") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (d #t) (k 0)))) (h "0aw9hxy3603zd7xyyhg42c2xbsq9dj3vxxxpf3di3k7rba15q7qz")))

(define-public crate-eelu-login-0.2.1 (c (n "eelu-login") (v "0.2.1") (d (list (d (n "open") (r "^4") (d #t) (k 0)) (d (n "sis-login") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0pw642nmb8smbkwfxjkqg3aaaldmllb15q8cy9ysgbs38ppf098p")))

(define-public crate-eelu-login-0.3.0 (c (n "eelu-login") (v "0.3.0") (d (list (d (n "open") (r "^4") (d #t) (k 0)) (d (n "sis-login") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "037myvi8hw00phb2906r2061lnly9368z5nmjfv83i9jj9vghgpl")))

(define-public crate-eelu-login-0.3.1 (c (n "eelu-login") (v "0.3.1") (d (list (d (n "open") (r "^5") (d #t) (k 0)) (d (n "sis-login") (r "^0.2.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0r1g420q1p68sm5mf3zqxszw6zvhziw0zn5kxbrvg4g8vphm67xr")))

(define-public crate-eelu-login-0.3.2 (c (n "eelu-login") (v "0.3.2") (d (list (d (n "open") (r "^5") (d #t) (k 0)) (d (n "sis-login") (r "^0.2.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1sx9n13rbh2bqh70m6gc6r2rxpybc9zrq7yrpmxlkim796xzk2z4")))

