(define-module (crates-io ee i_ eei_vfd) #:use-module (crates-io))

(define-public crate-eei_vfd-0.1.0 (c (n "eei_vfd") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (t "cfg(unix)") (k 2)))) (h "0d9f5014wnr36n5pnbwhkql92dzg5izk1chd34a8dn0cyjjx08ns") (f (quote (("type_a_alternative_faster_lut") ("linux-dev") ("graphics" "embedded-graphics-core") ("default" "graphics" "linux-dev"))))))

