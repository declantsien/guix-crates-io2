(define-module (crates-io ee ri eeric) #:use-module (crates-io))

(define-public crate-eeric-0.0.0 (c (n "eeric") (v "0.0.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0l8n5yxkk8khs88vavbhc6dfz8mr3yrfls35gz3k5pm03b4ixc7c")))

(define-public crate-eeric-0.0.1 (c (n "eeric") (v "0.0.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "02in25gi31xza2zgspwq7ly8x0b0v8kd8a1vrnszr9w5kljbcwc9")))

(define-public crate-eeric-0.0.2 (c (n "eeric") (v "0.0.2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0pqxkkrydlj1mvd7mnd9l3shs4az9vwi1200g8y1zlspzsxvzxjw")))

(define-public crate-eeric-0.0.3 (c (n "eeric") (v "0.0.3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1njr0zchs473yq6spbylqhh4j2qwlw2chmi1sia8jp4vc93f43g9")))

(define-public crate-eeric-0.0.4 (c (n "eeric") (v "0.0.4") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0140gc35anrpnxvi9ldz76p076bykm42f52rql3bfij7l1ifhwm8")))

(define-public crate-eeric-0.0.5 (c (n "eeric") (v "0.0.5") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0gzk27jb5is9nk72sqfb9gjrq8rw0rqpswcjzljfyvbxr0hn31y9")))

(define-public crate-eeric-0.0.6 (c (n "eeric") (v "0.0.6") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "03nn9mn8b2szyxiz6qh13xcssprmn3qjbl5v6p7yr8smmbcm67nw")))

(define-public crate-eeric-0.0.7 (c (n "eeric") (v "0.0.7") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1rfpbw9d4l7njrnxcqm067vfr3nyjlz8fjsg9xwksh53y0sf3hh3")))

(define-public crate-eeric-0.0.8 (c (n "eeric") (v "0.0.8") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1v5xdjmm298dp44phsk1z9jwxk2xrb0faph9r30scv91b4dwgsmq")))

(define-public crate-eeric-0.0.9 (c (n "eeric") (v "0.0.9") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "006jz62yd0d77741fhp2yjx43kkjl49b85a7bcv7xxzgj2521hb3")))

(define-public crate-eeric-0.0.10 (c (n "eeric") (v "0.0.10") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1qqbqlgra4c83ix32prbgqr92arv9iwzrrkis96cyg3zimm9wb1h")))

(define-public crate-eeric-0.0.11 (c (n "eeric") (v "0.0.11") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0nwcm2npbywj9zsyd68qwv0zpglpww0fy7h13zfcxb72l8vxiy71")))

(define-public crate-eeric-0.0.12 (c (n "eeric") (v "0.0.12") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0psj063a37hi63yhl8ir0sm2jfdh63mkn76lz5hnlacjnfpz5lgr")))

(define-public crate-eeric-0.0.13 (c (n "eeric") (v "0.0.13") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0x3w0lhwra6cgnxld377kzdlfa3l954s7scxxy3jj2abdmcqgkbl")))

(define-public crate-eeric-0.0.14 (c (n "eeric") (v "0.0.14") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1ksg2mqi16rsj3py7fvhypr14lkn1d24lqyrd2dr3c9fvfn97axj")))

(define-public crate-eeric-0.0.15 (c (n "eeric") (v "0.0.15") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "09q955ibri6qwl8w87l820d8x4p4q21dhr3lnnxggzdff2c12bax")))

(define-public crate-eeric-0.0.16 (c (n "eeric") (v "0.0.16") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0036yshwx05ymcn29zxdla2gbg7g33cjl5ldrlk8c530lqc5d32x")))

(define-public crate-eeric-0.0.17 (c (n "eeric") (v "0.0.17") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0zqnvfykyzlfhhks6j8z5d3c8sc4srn65hayjcwkbhx2ryx4s1r4")))

(define-public crate-eeric-0.0.18 (c (n "eeric") (v "0.0.18") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0x7cz6syqd0fmhm5i73kb8wyhspbpaxn6fm51n3sfv7m2wd0qig1")))

(define-public crate-eeric-0.0.19 (c (n "eeric") (v "0.0.19") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1ffg10byxqw0p665x54nask0ygwz0jad4i02ymrg437w8aslrvrn")))

(define-public crate-eeric-0.0.20 (c (n "eeric") (v "0.0.20") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0m2l6afzbx5jzlklagvrvyzgiim62azkd15f6yylmjd3mp1k2cx8")))

(define-public crate-eeric-0.0.21 (c (n "eeric") (v "0.0.21") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0y07rswxi7vjzqvfznybiwg4b78r52q6vsids18hlirv8g20bcg7")))

(define-public crate-eeric-0.0.22 (c (n "eeric") (v "0.0.22") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1nma36jb3mqv2s1xnp55708vi6avxxqzx07xlm5zc3jzcvcrs484")))

(define-public crate-eeric-0.0.23 (c (n "eeric") (v "0.0.23") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "10kc1l3xvkwq3hl2fb5hsjzg9xyi2ly5v9jq8s6826yqa8g3mz3d")))

(define-public crate-eeric-0.0.24 (c (n "eeric") (v "0.0.24") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0cp6w8rgqnf8qcyqk9kq7h4pf1bzamgkmxh0l8vlz9lx75m3ay6j")))

(define-public crate-eeric-0.0.25 (c (n "eeric") (v "0.0.25") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0wiy5v4vjixx1p2hnsqmsqa5zc0676xjyd1jkd0gzymb2qc0qxwg")))

(define-public crate-eeric-0.0.26 (c (n "eeric") (v "0.0.26") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0jp98wdgzrv3klk1qna4vhq233819mdcaqvaw7zhmabanq2rvhr2")))

(define-public crate-eeric-0.0.27 (c (n "eeric") (v "0.0.27") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1m8dx6h7h3m7ilcq2viy9srjia9qqp54i559xkd8ns1xcld6vcf4")))

(define-public crate-eeric-0.0.28 (c (n "eeric") (v "0.0.28") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0530lgd3ssdnqiqhsv9czcfj84m4jhlwhvdf2xvm1b0pwwlsvfig")))

(define-public crate-eeric-0.0.29 (c (n "eeric") (v "0.0.29") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "18k5rnmyz6kgmzaclwv1z1cdljpj9nklb6450mb8gzxnz27hmj8w")))

(define-public crate-eeric-0.0.30 (c (n "eeric") (v "0.0.30") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1hpmcwg1mz19fhxvm40p7516q9ygf1gc8rwv9gzqm5b8f0nz8g0h")))

(define-public crate-eeric-0.0.31 (c (n "eeric") (v "0.0.31") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0bw5f8ckrlxw4zl3grdks3dgg75jvbjpbzw1y1dp8xypawa17i8z")))

(define-public crate-eeric-0.0.32 (c (n "eeric") (v "0.0.32") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1fznyy7in5r8lqwgz6m0zi87mj14kqiazmn92if5qf7knh3n0791")))

(define-public crate-eeric-0.0.33 (c (n "eeric") (v "0.0.33") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0fmqf4kdi9n44wl9pzlm9w1i19q3lfc3zxspylcc1msskriqf7aw")))

(define-public crate-eeric-0.0.34 (c (n "eeric") (v "0.0.34") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1lhrmr4qx218jqxrzr3dgmhmmw2npg05x60d72jm61sfzji45abn")))

(define-public crate-eeric-0.0.35 (c (n "eeric") (v "0.0.35") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1jzz94kr2jpcxvwr81sdhbfl29ijc485hq879c2g8fhqdb3smzwn")))

(define-public crate-eeric-0.0.36 (c (n "eeric") (v "0.0.36") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "16wnrfwh5kd9jwradkvfdmpbwrkwc7pxnm7cfax4yd0q4m6cppna")))

(define-public crate-eeric-0.0.37 (c (n "eeric") (v "0.0.37") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "06lyp8sy4bkcgd33c71gvsyz16ndi22l0599sb8hfy59rxlj1mxa")))

(define-public crate-eeric-0.0.38 (c (n "eeric") (v "0.0.38") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1a26n0vmyf62fw7gm8mpbbyb0j4i6490z1xq3l7nsa1bw6l2k89y")))

(define-public crate-eeric-0.0.39 (c (n "eeric") (v "0.0.39") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "15yfpy9r7x1bkfmzkk5yic9bjj6mc3qsdcvqhy2w8x3zki8zfm2r")))

(define-public crate-eeric-0.0.40 (c (n "eeric") (v "0.0.40") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0xw3jll6dmk71kgz09j189bmf624n6v8bsmid1xnsirarbkv3ay6")))

(define-public crate-eeric-0.0.41 (c (n "eeric") (v "0.0.41") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "023hg1skida1q68iy4kvaxmfg64vgc1g06hn4b0i4pd6hswgk0m8")))

(define-public crate-eeric-0.0.42 (c (n "eeric") (v "0.0.42") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1ibm7bkn0fxmdvqgrvahki97fb6p7174ny29jjcxjfyddswhlj4q")))

(define-public crate-eeric-0.0.44 (c (n "eeric") (v "0.0.44") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1pvsj9j3svkf1bisajq1c4qkb7ps40vnply9q7mz2z8m6y3lvm30")))

(define-public crate-eeric-0.0.45 (c (n "eeric") (v "0.0.45") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "08wrz9ncz7d8ma13gid9dy118yiilwhp1lhnbqknrvm7s8yf6cw8")))

(define-public crate-eeric-0.0.46 (c (n "eeric") (v "0.0.46") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "003i5gjmy4g6rg3jsp0j5ghwx2lxwnn6hrnklmp5m4qz2r0blfdc")))

(define-public crate-eeric-0.0.47 (c (n "eeric") (v "0.0.47") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0q8hd8aipyigcis1h22q53m0v46njkrgw2lmmxg2vdq3w4vs51kc")))

(define-public crate-eeric-0.0.48 (c (n "eeric") (v "0.0.48") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1spbp6awx11sk7jvk8iv24d2l4fsafrzri5v46y31xyvz2x4kqiy")))

(define-public crate-eeric-0.0.49 (c (n "eeric") (v "0.0.49") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "023g2ryg2842wl2phy6sc8mjhdwhsyx9bn2jga0ja6s2y5ynyf8x")))

(define-public crate-eeric-0.0.50 (c (n "eeric") (v "0.0.50") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "instant") (r "^0.1.12") (f (quote ("stdweb"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1zkddklx7vdjng5ak42bwn7sz2azkplfc5bldirlzp05gd9381xp")))

(define-public crate-eeric-0.0.51 (c (n "eeric") (v "0.0.51") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1dfgvwq1zjx2al5zpdshjjy8qjikfvp3578yipcaf4c6jw6750ww")))

(define-public crate-eeric-0.0.52 (c (n "eeric") (v "0.0.52") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1h49d26vd3glr5s4rbbq5kjfqhnrg64d8v3hl19c05zcqp2vzlhf")))

(define-public crate-eeric-0.1.0-rc.1 (c (n "eeric") (v "0.1.0-rc.1") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1558lqgvixq87h6ryjmsvf810cws4l7h4rpvwa2cnxf997kl9x4x")))

(define-public crate-eeric-0.1.0-rc.2 (c (n "eeric") (v "0.1.0-rc.2") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "145q9lkr1fnqvl3jnz4d620z0fhlpa419pb1qab9mad9gvbgvasx")))

(define-public crate-eeric-0.1.0-rc.3 (c (n "eeric") (v "0.1.0-rc.3") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "11rd6d7nchj82kl6mk4pa5cs9dbcwvkf5j9b8clybq2d3sqyddqf")))

(define-public crate-eeric-0.1.0-rc.4 (c (n "eeric") (v "0.1.0-rc.4") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0manygcbn50116480409z31i9bn1qg9y0fzvlihqgysj2wj5n283")))

(define-public crate-eeric-0.1.0-rc.5 (c (n "eeric") (v "0.1.0-rc.5") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "071si7hnv0r1wvyagg0vg4f4p4132q77hgmxlnkkm734h5ac4bfc")))

