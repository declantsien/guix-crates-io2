(define-module (crates-io ee ri eeric-core) #:use-module (crates-io))

(define-public crate-eeric-core-0.1.0 (c (n "eeric-core") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0wvc3f195yw2dnwf85p2ac10d5a9z3kfl7rhdrf21f330g8jwk8x")))

(define-public crate-eeric-core-0.1.2 (c (n "eeric-core") (v "0.1.2") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "02dyaqm9dk77l0x82qfl40lpr4wa9l8g7mydxnf1nmjdrr18j39x")))

