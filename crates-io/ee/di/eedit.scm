(define-module (crates-io ee di eedit) #:use-module (crates-io))

(define-public crate-eedit-0.1.0 (c (n "eedit") (v "0.1.0") (d (list (d (n "argon2") (r "^0.5.3") (d #t) (k 0)) (d (n "argon_hash_password") (r "^0.1.2") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10.1") (f (quote ("stream"))) (d #t) (k 0)) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)) (d (n "scrawl") (r "^2.0.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.7.0") (d #t) (k 0)))) (h "0bg01j52lbprkaqms976flr40mrvq9ds3afzv02q02gbpyvwc85z")))

(define-public crate-eedit-0.1.1 (c (n "eedit") (v "0.1.1") (d (list (d (n "argon2") (r "^0.5.3") (d #t) (k 0)) (d (n "argon_hash_password") (r "^0.1.2") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10.1") (f (quote ("stream"))) (d #t) (k 0)) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)) (d (n "scrawl") (r "^2.0.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.7.0") (d #t) (k 0)))) (h "12p9l8rkw3nf9ckb3qyiysgd62bkhngbs6qw624ppns3900w6g09")))

