(define-module (crates-io tu mu tumult) #:use-module (crates-io))

(define-public crate-tumult-0.1.0 (c (n "tumult") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "serde_xml") (r "^0.7.0") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "09xadhzikdis03wmvfsh869x2pln78nrh8xd1ylr6zfhxk7l2rv5") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

