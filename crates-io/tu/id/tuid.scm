(define-module (crates-io tu id tuid) #:use-module (crates-io))

(define-public crate-tuid-0.0.0 (c (n "tuid") (v "0.0.0") (d (list (d (n "bstr") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1") (o #t) (k 0)) (d (n "faster-hex") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "fastrand") (r "^1.5") (o #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (k 0)))) (h "0j7l58ql6dw1240rmclrihglszib5cgxxj7715iiw6h79l85fb9p") (f (quote (("hex" "faster-hex" "bstr") ("full" "default" "approx") ("default" "fastrand" "uuid" "hex" "coarse") ("coarse" "coarsetime") ("approx" "coarse")))) (y #t) (r "1.57")))

