(define-module (crates-io tu ve tuvero) #:use-module (crates-io))

(define-public crate-tuvero-0.1.0 (c (n "tuvero") (v "0.1.0") (h "1a69h1wm46zhk9xr9hhybmlc9ljyjb6qgmammhzdfhlhn33sn1dl")))

(define-public crate-tuvero-0.1.1 (c (n "tuvero") (v "0.1.1") (h "0qibzx0nvl0d04zs0inf4xri5bipmbqckahqaq0rap5xba5jhmpi")))

