(define-module (crates-io tu du tudu) #:use-module (crates-io))

(define-public crate-tudu-1.0.0 (c (n "tudu") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "17wkzp5i81a5rjy8snm35sv4rrhf4iqv74jr4g2ccp2x7hfhharx")))

(define-public crate-tudu-1.1.0 (c (n "tudu") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "09cw6mwqjicmm322zyial3wz3zh3g63ca84c2l4cv1i861jgj83p")))

