(define-module (crates-io tu ix tuix_derive) #:use-module (crates-io))

(define-public crate-tuix_derive-0.2.0 (c (n "tuix_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "00807ba8qmqv8pqcxq1041a1j80wwckdfymmgjn5rrs364bzfhq3")))

