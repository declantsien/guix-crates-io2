(define-module (crates-io tu ix tuix_core) #:use-module (crates-io))

(define-public crate-tuix_core-0.2.0 (c (n "tuix_core") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cssparser") (r "^0.27.2") (d #t) (k 0)) (d (n "femtovg") (r "^0.2.5") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "keyboard-types") (r "^0.5.0") (k 0)) (d (n "morphorm") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0sw3qk5sqxylgbxl7s68v4lrkkpj5is9hr53z2zq084ks03g5psf")))

