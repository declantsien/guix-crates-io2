(define-module (crates-io tu ix tuix_widgets) #:use-module (crates-io))

(define-public crate-tuix_widgets-0.2.0 (c (n "tuix_widgets") (v "0.2.0") (d (list (d (n "femtovg") (r "^0.2.5") (d #t) (k 0)) (d (n "keyboard-types") (r "^0.5.0") (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "tuix_core") (r "^0.2.0") (d #t) (k 0)))) (h "1kx21crpr3k6i85z60a176c4qvw4jfdmf2hi5v5s5m8ygmyc8nlc")))

