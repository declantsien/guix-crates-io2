(define-module (crates-io tu ix tuix_glutin) #:use-module (crates-io))

(define-public crate-tuix_glutin-0.2.0 (c (n "tuix_glutin") (v "0.2.0") (d (list (d (n "femtovg") (r "^0.2.5") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "glutin") (r "^0.27.0") (d #t) (k 0)) (d (n "keyboard-types") (r "^0.5.0") (k 0)) (d (n "tuix_core") (r "^0.2") (d #t) (k 0)))) (h "1jw574dkmd7pnlf9lqb26vbjq4dl8g2w6rfpnwyawg3y8zgshbag")))

