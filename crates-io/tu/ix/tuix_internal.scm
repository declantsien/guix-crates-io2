(define-module (crates-io tu ix tuix_internal) #:use-module (crates-io))

(define-public crate-tuix_internal-0.2.0 (c (n "tuix_internal") (v "0.2.0") (d (list (d (n "tuix_core") (r "^0.2.0") (d #t) (k 0)) (d (n "tuix_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "tuix_glutin") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tuix_widgets") (r "^0.2.0") (d #t) (k 0)))) (h "0s5bg2z4ml8wjyxm8k8w5y0jf05844vmk45fdjmgc7x5m588b3vi") (f (quote (("glutin" "tuix_glutin") ("default" "glutin"))))))

