(define-module (crates-io tu ix tuix) #:use-module (crates-io))

(define-public crate-tuix-0.1.0 (c (n "tuix") (v "0.1.0") (d (list (d (n "cssparser") (r "^0.27.2") (d #t) (k 0)) (d (n "femtovg") (r "^0.1.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.24.1") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)))) (h "1f9qxj607zbh8zdvk1h4ncp4v2nzic915cx8915nrm8bl4p1p5qh")))

(define-public crate-tuix-0.2.0 (c (n "tuix") (v "0.2.0") (d (list (d (n "english-numbers") (r "^0.3.3") (d #t) (k 2)) (d (n "femtovg") (r "^0.2.5") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "tuix_dylib") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tuix_internal") (r "^0.2.0") (d #t) (k 0)))) (h "0zpz2yxm3pd3ywhs2m6znf4h7b9dv5jpbjcbb91i0y4nckwz0ggn") (f (quote (("tuix_glutin" "tuix_internal/tuix_glutin") ("dynamic" "tuix_dylib") ("default" "tuix_glutin"))))))

