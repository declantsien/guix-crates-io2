(define-module (crates-io tu is tuisic) #:use-module (crates-io))

(define-public crate-tuisic-1.0.0 (c (n "tuisic") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "mpv") (r "^0.2.3") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "xdg-user") (r "^0.2.1") (d #t) (k 0)))) (h "079s4r89nha7qdv6w46jp3d8fhzzx2h01c6lx001cfvnppsx4p14")))

(define-public crate-tuisic-1.0.1 (c (n "tuisic") (v "1.0.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "mpv") (r "^0.2.3") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "xdg-user") (r "^0.2.1") (d #t) (k 0)))) (h "0mffw8rpaqnlhgjg22r3a084dj7bvw1992hyv2jbglm87jk57wrs") (r "1.70")))

