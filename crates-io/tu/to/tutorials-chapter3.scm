(define-module (crates-io tu to tutorials-chapter3) #:use-module (crates-io))

(define-public crate-tutorials-chapter3-0.1.0 (c (n "tutorials-chapter3") (v "0.1.0") (d (list (d (n "crypto-rsl") (r "^0.1.0") (d #t) (k 0)) (d (n "ssh2-rsl") (r "^0.1.0") (d #t) (k 0)) (d (n "tar-rsl") (r "^0.1.0") (d #t) (k 0)) (d (n "toml-rsl") (r "^0.1.0") (d #t) (k 0)) (d (n "xml-rsl") (r "^0.1.0") (d #t) (k 0)))) (h "14h6kjm5jxhfyzdniimx94h6336n5c03hkc7r9cp1j7r2w09545l")))

