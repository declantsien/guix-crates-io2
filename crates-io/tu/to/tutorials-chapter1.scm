(define-module (crates-io tu to tutorials-chapter1) #:use-module (crates-io))

(define-public crate-tutorials-chapter1-0.1.0 (c (n "tutorials-chapter1") (v "0.1.0") (d (list (d (n "crypto-rsl") (r "^0.1.0") (d #t) (k 0)) (d (n "jdks") (r "^0.1.3") (d #t) (k 0)) (d (n "ksre-tui") (r "^0.2.0") (d #t) (k 0)) (d (n "sdkman-cli-native") (r "^0.5.0") (d #t) (k 0)) (d (n "ssh2-rsl") (r "^0.1.0") (d #t) (k 0)))) (h "1j6yhvjklsmw010pxq53af2jznwr9dy0grh0pnjmb76xc1rbyhls")))

