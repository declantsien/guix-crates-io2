(define-module (crates-io tu nt tuntap) #:use-module (crates-io))

(define-public crate-tuntap-0.0.1 (c (n "tuntap") (v "0.0.1") (h "031nx8ssi3fs155jibj61r1y2fq353ayvs0ckgm9ffssrd1bi4p1")))

(define-public crate-tuntap-0.1.0 (c (n "tuntap") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "nix") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.0") (f (quote ("async-await-preview"))) (d #t) (k 0)))) (h "14qjv98qrhlrp2sk084xr8pd8xa1g8kxbfrvbyk4ls9w038n4yfa")))

(define-public crate-tuntap-0.2.0 (c (n "tuntap") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "nix") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.0") (f (quote ("async-await-preview"))) (d #t) (k 0)))) (h "1lbs889d81ixmwklyij1l6nf091gahydsf2wgjlls1m5ar3brwc0")))

