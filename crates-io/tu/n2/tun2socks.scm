(define-module (crates-io tu n2 tun2socks) #:use-module (crates-io))

(define-public crate-tun2socks-0.0.1 (c (n "tun2socks") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.79") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0n1kk20cccisapqfv9c6w6yhh1khif0q1a4xvbcqzvibhqfz3j1r") (y #t)))

(define-public crate-tun2socks-0.0.2 (c (n "tun2socks") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.79") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1nif30w1wqkfjj8820bwgkdv5krkxs8bgbh813iwgzjdcdvgdk6p") (y #t)))

(define-public crate-tun2socks-0.1.0 (c (n "tun2socks") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.79") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0f6q7kwfr7azc4c4q5pd4wn46zqqf7zm71xgmr1p4nyzx5jzxp39") (y #t)))

(define-public crate-tun2socks-0.1.1 (c (n "tun2socks") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.79") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1v5bam9jaj4w5k2fzxgi2fbj89shqwqci388ydca8d8imp4mx024") (y #t)))

(define-public crate-tun2socks-0.1.2 (c (n "tun2socks") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.79") (f (quote ("parallel"))) (d #t) (k 1)))) (h "19r1jay1dj9mk97a2d9sx107k1gx5pq1lxl0klav8niwhb8ajzvi") (y #t)))

(define-public crate-tun2socks-0.1.3 (c (n "tun2socks") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.79") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0sm4qvab2rq9r2303jbpgd2mga0vsf17jsyaiqmq9gz8gdc3rmja")))

(define-public crate-tun2socks-0.1.4 (c (n "tun2socks") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.79") (f (quote ("parallel"))) (d #t) (k 1)))) (h "09qs6qc24ra9ydd8aj330sp6bcjpfn92f68wbr8dpazd0a540kl9")))

