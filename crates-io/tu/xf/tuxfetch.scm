(define-module (crates-io tu xf tuxfetch) #:use-module (crates-io))

(define-public crate-tuxfetch-0.1.0 (c (n "tuxfetch") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "nixinfo") (r "^0.2.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.20.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "whoami") (r "^1.1.2") (d #t) (k 0)))) (h "1zirh129js5b3in5fwm7jq17zrdai8564h7340qvgr5vhlc43g2m")))

