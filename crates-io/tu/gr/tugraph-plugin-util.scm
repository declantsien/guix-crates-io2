(define-module (crates-io tu gr tugraph-plugin-util) #:use-module (crates-io))

(define-public crate-tugraph-plugin-util-0.1.0 (c (n "tugraph-plugin-util") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "libtugraph-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tugraph") (r "^0.1.0") (d #t) (k 0)) (d (n "tugraph_plugin_util-proc_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1pjn0ywip86fyjlc9r1pnr8ibpbxmwqwkxvna9n0d0nhpkzj59nz") (r "1.64.0")))

(define-public crate-tugraph-plugin-util-0.1.2 (c (n "tugraph-plugin-util") (v "0.1.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "libtugraph-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "tugraph") (r "^0.1.3") (d #t) (k 0)) (d (n "tugraph_plugin_util-proc_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0gn7v55bcy39wj1qhkrnrl5xfgl49q9ibgx52g5b7hcqvgcl9nk0") (r "1.64.0")))

