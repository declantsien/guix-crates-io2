(define-module (crates-io tu gr tugraph) #:use-module (crates-io))

(define-public crate-tugraph-0.1.0 (c (n "tugraph") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libtugraph-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1i6al4pc6rlz9qpla8f1aba1v124573fkcmyzhnmf2xfvra6fjr1") (r "1.68.0")))

(define-public crate-tugraph-0.1.1 (c (n "tugraph") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libtugraph-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1959m5l32a9l4iwphsx1f38ddrlfg24hgrw1142jglcxshys5ksr") (r "1.68.0")))

(define-public crate-tugraph-0.1.2 (c (n "tugraph") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libtugraph-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "173rpbyjywd7kh9q6cb0ac7ay38fjv579il81yi2pfc1q07i0nfz") (r "1.68.0")))

(define-public crate-tugraph-0.1.3 (c (n "tugraph") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libtugraph-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1mcwb5wyhddikdvh8gqva9cy88ab8nwd4cl4wsgdgyplqzv7m8y6") (r "1.68.0")))

(define-public crate-tugraph-0.1.4 (c (n "tugraph") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libtugraph-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1dkmkw36gfhzvv2xhddc1pna1cc27icd6av5xsqdx5ni8dk6c0fp") (r "1.68.0")))

