(define-module (crates-io tu gr tugraph_plugin_util-proc_macros) #:use-module (crates-io))

(define-public crate-tugraph_plugin_util-proc_macros-0.1.0 (c (n "tugraph_plugin_util-proc_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1vya7kz6dyy1vp2r7ridigribld9bw4mip46wi4hl4sj7ikk5ljf")))

(define-public crate-tugraph_plugin_util-proc_macros-0.1.1 (c (n "tugraph_plugin_util-proc_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "19r15dq82ki4zz8a50ncrx6c9v9jyj0qkmab2wmrlz2y3dyk2adk")))

