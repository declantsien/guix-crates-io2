(define-module (crates-io tu -r tu-result) #:use-module (crates-io))

(define-public crate-tu-result-0.1.0 (c (n "tu-result") (v "0.1.0") (h "0i6xncw2h8vsv5fxi51bim87hisy2sccbljb6vk54a7cxfbx5mq6")))

(define-public crate-tu-result-0.1.1 (c (n "tu-result") (v "0.1.1") (h "0wahsbqfgghhd556vpzrw0b7scg61nxbhkpvn3kfzmmclihqd1jk")))

