(define-module (crates-io tu on tuono_lib) #:use-module (crates-io))

(define-public crate-tuono_lib-0.0.5 (c (n "tuono_lib") (v "0.0.5") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "erased-serde") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ssr_rs") (r "^0.5.1") (d #t) (k 0)) (d (n "tuono_lib_macros") (r "^0.0.5") (d #t) (k 0)))) (h "19jjpagn23n7ipv1rkrsxnv63a43j3xh3ggwzsds41yyxnx7wl9g")))

(define-public crate-tuono_lib-0.0.6 (c (n "tuono_lib") (v "0.0.6") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "erased-serde") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ssr_rs") (r "^0.5.1") (d #t) (k 0)) (d (n "tuono_lib_macros") (r "^0.0.6") (d #t) (k 0)))) (h "1299y3mlx65w7gg6bxnd5b3a25m6wbpk0p0ydg30rcbwmyjygs7f")))

