(define-module (crates-io tu on tuono_lib_macros) #:use-module (crates-io))

(define-public crate-tuono_lib_macros-0.0.5 (c (n "tuono_lib_macros") (v "0.0.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.59") (f (quote ("full"))) (d #t) (k 0)))) (h "0awvpzqz5g5s023m775wyspg566z4s4r1xn42zmmvljsp4s80h9j")))

(define-public crate-tuono_lib_macros-0.0.6 (c (n "tuono_lib_macros") (v "0.0.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.59") (f (quote ("full"))) (d #t) (k 0)))) (h "1kk85vmvlvfd08l776xy01xg5zxbac4cnh4rd75xczjsvr6wrjyq")))

