(define-module (crates-io tu pp tupperware) #:use-module (crates-io))

(define-public crate-tupperware-0.1.0 (c (n "tupperware") (v "0.1.0") (h "0w159dzpz2yx9i94m7c44f8vm7q35hlklfb125ggj2a6600hyl7b")))

(define-public crate-tupperware-0.2.0 (c (n "tupperware") (v "0.2.0") (h "0k75x10vd43304fs4im499w6yhk2gps7kis9pva12bzn2s1v4hvx")))

