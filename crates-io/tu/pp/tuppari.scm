(define-module (crates-io tu pp tuppari) #:use-module (crates-io))

(define-public crate-tuppari-0.1.0 (c (n "tuppari") (v "0.1.0") (h "12cr4h0sygna2lx4xgasdiilmxqn4dpk2yn840b3fyngh9xphkix")))

(define-public crate-tuppari-0.1.1 (c (n "tuppari") (v "0.1.1") (h "029rimxc344l6sy02yqfbmbknvgm2wv0mh9plxwjnb383pzhknhr")))

