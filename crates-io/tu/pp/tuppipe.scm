(define-module (crates-io tu pp tuppipe) #:use-module (crates-io))

(define-public crate-tuppipe-0.1.0 (c (n "tuppipe") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0shi0zpv4rgq76jcg16pf6j9q9r08svlmqsdiq154asy69692ayq")))

(define-public crate-tuppipe-0.1.1 (c (n "tuppipe") (v "0.1.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0jkscrrb4wqciacvn93v4j7p4anrrqds907dgc1p5fjkcmhzgyq7")))

(define-public crate-tuppipe-0.1.2 (c (n "tuppipe") (v "0.1.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1nv6bjph2vihjg12x3jk38xyw4yshawgqanjb5sjwr7i7j3bwjl5") (f (quote (("fn-pipes") ("default" "fn-pipes"))))))

(define-public crate-tuppipe-0.1.3 (c (n "tuppipe") (v "0.1.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1hrfrsmd1s07jnxvx04s7rikqj9iskm5nq5m7k5kgwb3585x0daw") (f (quote (("fn-pipes") ("default" "fn-pipes"))))))

