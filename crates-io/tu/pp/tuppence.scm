(define-module (crates-io tu pp tuppence) #:use-module (crates-io))

(define-public crate-tuppence-0.0.1 (c (n "tuppence") (v "0.0.1") (d (list (d (n "actix-web") (r "^4.3.1") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "cashu") (r "^0.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "08yfk5sdrlqxh37dl74xg6dlwi4lx0szdfwmwdlhpkdrixacx0ln")))

