(define-module (crates-io tu pl tuple_length) #:use-module (crates-io))

(define-public crate-tuple_length-0.1.1 (c (n "tuple_length") (v "0.1.1") (d (list (d (n "tuple_macro") (r "^0.1.1") (d #t) (k 0)))) (h "0zn693fqaapr7vn2qpzpanv93r2vpzkspcd6ddj567xq0fz6hd9a") (f (quote (("64" "tuple_macro/64") ("32" "tuple_macro/32") ("16" "tuple_macro/16"))))))

(define-public crate-tuple_length-0.1.2 (c (n "tuple_length") (v "0.1.2") (d (list (d (n "tuple_macro") (r "^0.1.1") (d #t) (k 0)))) (h "1mvjmc07iv26c25hjz7ffxv1nhw4nflqpwgpgsghmsgyp10d5zgk") (f (quote (("64" "tuple_macro/64") ("32" "tuple_macro/32") ("16" "tuple_macro/16"))))))

(define-public crate-tuple_length-0.2.0 (c (n "tuple_length") (v "0.2.0") (d (list (d (n "tuple_macro") (r "^0.2.0") (d #t) (k 0)))) (h "1cxxlv173sdh0kkhvr5ijqnpx54hzrh49xv2sj83pcb11p80ppzg") (f (quote (("tup_len_64" "tuple_macro/tup_len_64") ("tup_len_32" "tuple_macro/tup_len_32") ("tup_len_16" "tuple_macro/tup_len_16"))))))

