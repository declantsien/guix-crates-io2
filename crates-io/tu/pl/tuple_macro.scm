(define-module (crates-io tu pl tuple_macro) #:use-module (crates-io))

(define-public crate-tuple_macro-0.1.1 (c (n "tuple_macro") (v "0.1.1") (h "06hp2xj5ccskwwv0d1jk89hpyy2ysch04r8dm4ggf1lfmpfiwdvs") (f (quote (("64") ("32") ("16"))))))

(define-public crate-tuple_macro-0.2.0 (c (n "tuple_macro") (v "0.2.0") (h "0lv45719fi6av5y1iral0bzvsi9q6y6kmfwszxqvx4l96cjxgzyb") (f (quote (("tup_len_64") ("tup_len_32") ("tup_len_16"))))))

(define-public crate-tuple_macro-0.2.1 (c (n "tuple_macro") (v "0.2.1") (h "06jsmrdzi34lh651h78dgnxnnsnir2wrx4gvy99yv4c9hxi75v7n") (f (quote (("tup_len_64") ("tup_len_32") ("tup_len_16") ("from_tup_64") ("from_tup_32") ("from_tup_16"))))))

(define-public crate-tuple_macro-0.2.2 (c (n "tuple_macro") (v "0.2.2") (h "131rbrl4zr20qbr06hj97rw5r4z79gb2bi2lm5cj8f782ifaxin1") (f (quote (("tup_len_64") ("tup_len_32") ("tup_len_16") ("from_tup_8") ("from_tup_64") ("from_tup_32") ("from_tup_16"))))))

