(define-module (crates-io tu pl tuple_swizzle) #:use-module (crates-io))

(define-public crate-tuple_swizzle-1.0.0 (c (n "tuple_swizzle") (v "1.0.0") (h "1a464ds025cbkj0qlar1ny0hsa21ryxllspb8977bhfi769ff8xf")))

(define-public crate-tuple_swizzle-1.0.1 (c (n "tuple_swizzle") (v "1.0.1") (h "1wcbhclf9dip6wgb0yxdd1y9jihla8lfzrxfpsqqgsh8xjgvlzl9")))

