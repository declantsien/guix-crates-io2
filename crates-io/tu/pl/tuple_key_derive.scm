(define-module (crates-io tu pl tuple_key_derive) #:use-module (crates-io))

(define-public crate-tuple_key_derive-0.1.0 (c (n "tuple_key_derive") (v "0.1.0") (d (list (d (n "buffertk") (r "^0.2") (d #t) (k 0)) (d (n "derive_util") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "118pycayphmk8cpyc4a9whk1k8b89a5hjdql2y9izwy5p88lm7bg")))

(define-public crate-tuple_key_derive-0.1.1 (c (n "tuple_key_derive") (v "0.1.1") (d (list (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "derive_util") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10qfdbwpxpbs7g9mw7l8zj0wlmjn8m2fy40d31yz2505lq9hxwp6")))

(define-public crate-tuple_key_derive-0.2.0 (c (n "tuple_key_derive") (v "0.2.0") (d (list (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "derive_util") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0x5gd77fr00frml519fdxh0v4za1g2s0nb69rsassfa3yzlcaa0p")))

(define-public crate-tuple_key_derive-0.3.0 (c (n "tuple_key_derive") (v "0.3.0") (d (list (d (n "buffertk") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prototk") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z3zvs9fbxqf64nmffivqj9n4c4iccsvav5gvywnqqfv92cw1l71")))

(define-public crate-tuple_key_derive-0.4.0 (c (n "tuple_key_derive") (v "0.4.0") (d (list (d (n "buffertk") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prototk") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vym3n9br417ab2zhhs35394b229imca85f66slzkbg1n9jzjlyr")))

(define-public crate-tuple_key_derive-0.5.0 (c (n "tuple_key_derive") (v "0.5.0") (d (list (d (n "buffertk") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prototk") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0w93szgiz7nbbbgrmji5jfvcxakwvp7i9r1f8dl2zfciwsh8yy6n")))

