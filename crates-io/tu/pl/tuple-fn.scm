(define-module (crates-io tu pl tuple-fn) #:use-module (crates-io))

(define-public crate-tuple-fn-1.0.0 (c (n "tuple-fn") (v "1.0.0") (h "1q20qwd8pdn9fln8s7m24v06w7134yi81aahf4f8nn6d2knmyf9s")))

(define-public crate-tuple-fn-1.1.0 (c (n "tuple-fn") (v "1.1.0") (h "13cbm69kf2ccpahr3032ssiz73pvi86si1p5iw29lvpqplsk1s89")))

(define-public crate-tuple-fn-1.2.0 (c (n "tuple-fn") (v "1.2.0") (h "1fnmi9yx7526pin021p74jx62jz0q8cq52jnkqj1d6qa8jz6f26g")))

