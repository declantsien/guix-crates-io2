(define-module (crates-io tu pl tuple_key) #:use-module (crates-io))

(define-public crate-tuple_key-0.1.0 (c (n "tuple_key") (v "0.1.0") (d (list (d (n "buffertk") (r "^0.2") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "tuple_key_derive") (r "^0.1") (d #t) (k 0)))) (h "0kb0lyl56pkwgi0w0z7lrfxp08fapixsq5k4r6anaqdx99xhlrda")))

(define-public crate-tuple_key-0.1.1 (c (n "tuple_key") (v "0.1.1") (d (list (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "tuple_key_derive") (r "^0.1") (d #t) (k 0)))) (h "1gn2qzp7xyx8vw6ddwfb2lckdi7f22y9lsvf9p0pqpv7ll0wqgml")))

(define-public crate-tuple_key-0.2.0 (c (n "tuple_key") (v "0.2.0") (d (list (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "tuple_key_derive") (r "^0.2") (d #t) (k 0)))) (h "15jwfwmfwq7r8cdxddsifgqxdw45i76x36506qvbdy93d9mg1124")))

(define-public crate-tuple_key-0.3.0 (c (n "tuple_key") (v "0.3.0") (d (list (d (n "buffertk") (r "^0.4") (d #t) (k 0)) (d (n "prototk") (r "^0.4") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.4") (d #t) (k 0)) (d (n "tuple_key_derive") (r "^0.3") (d #t) (k 0)) (d (n "zerror") (r "^0.2") (d #t) (k 0)) (d (n "zerror_core") (r "^0.3") (d #t) (k 0)))) (h "10gmcb0chy25dg6a4kfbr1gsy8mm4a2grcmql1lc5xl11h4shr2c")))

(define-public crate-tuple_key-0.4.0 (c (n "tuple_key") (v "0.4.0") (d (list (d (n "buffertk") (r "^0.5") (d #t) (k 0)) (d (n "prototk") (r "^0.5") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.5") (d #t) (k 0)) (d (n "tuple_key_derive") (r "^0.4") (d #t) (k 0)) (d (n "zerror") (r "^0.3") (d #t) (k 0)) (d (n "zerror_core") (r "^0.4") (d #t) (k 0)))) (h "1dncfz6rr2809pshwkz0ay5hfnlcwpl3f3xbd4kd0d3vmbx2s2ns")))

(define-public crate-tuple_key-0.5.0 (c (n "tuple_key") (v "0.5.0") (d (list (d (n "buffertk") (r "^0.6") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "prototk") (r "^0.6") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.6") (d #t) (k 0)) (d (n "tuple_key_derive") (r "^0.5") (d #t) (k 0)) (d (n "zerror") (r "^0.4") (d #t) (k 0)) (d (n "zerror_core") (r "^0.5") (d #t) (k 0)) (d (n "zerror_derive") (r "^0.3") (d #t) (k 0)))) (h "0iji6z82896r2qlvlbqy7kzxsnyrz3xx35xifmh9b8i1v4ks1ljg")))

