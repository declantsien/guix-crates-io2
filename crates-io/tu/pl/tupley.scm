(define-module (crates-io tu pl tupley) #:use-module (crates-io))

(define-public crate-tupley-0.0.1 (c (n "tupley") (v "0.0.1") (h "066hikj2s4xzdafw3v8d2i63hrhmj2da04kl01lx9qsb0g620b50") (y #t)))

(define-public crate-tupley-0.0.11 (c (n "tupley") (v "0.0.11") (h "0v5fkr7draqjm4iadalwy9d9p2xjmhkrng5dccgc67a47v1sxkvg") (y #t)))

(define-public crate-tupley-0.0.12 (c (n "tupley") (v "0.0.12") (h "098fs4is2b19dqspdzar3svrx07w9xhawwwjakkrp3pn54xghbcc") (y #t)))

(define-public crate-tupley-0.0.13 (c (n "tupley") (v "0.0.13") (h "14hx1hnviq2xiz08780x2zih6fnm37kgk6x6bw0hjsxpznvjpyps") (y #t)))

(define-public crate-tupley-0.1.0 (c (n "tupley") (v "0.1.0") (h "1xcsqwmg2807izhnxqngllcp05pbcar9r56l9ggxqi8q43fgcsj1") (f (quote (("len-generic") ("full" "len-generic") ("default"))))))

(define-public crate-tupley-0.1.1 (c (n "tupley") (v "0.1.1") (h "0b3viwshc33xs320cipfnhsbk01asiihavnxkyrylhb030lmjhwg") (f (quote (("len-generic") ("full" "len-generic") ("default"))))))

