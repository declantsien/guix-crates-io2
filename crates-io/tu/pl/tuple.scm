(define-module (crates-io tu pl tuple) #:use-module (crates-io))

(define-public crate-tuple-0.1.0 (c (n "tuple") (v "0.1.0") (h "1lkp7qz0309al6h52fdvn6sg9wlp5i0d3sj12908mb7wifqi8i9d")))

(define-public crate-tuple-0.1.1 (c (n "tuple") (v "0.1.1") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)))) (h "1r1ayy8h2x56xxxg75r53gr7lbvlhdvykjbn3dwxbi5a8d7hxx89") (f (quote (("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.1.2 (c (n "tuple") (v "0.1.2") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)))) (h "0h1ayvhai9bwq56h9fv51m4n2wh5b3j9as6jb446ms6b4nbhw2zr") (f (quote (("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.1.3 (c (n "tuple") (v "0.1.3") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)))) (h "0mnxbinb4drqb95sid7zfmhfrsqmdalla6i35frk31xhwxvkfq3h") (f (quote (("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.1.4 (c (n "tuple") (v "0.1.4") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)))) (h "1nlyhv1ldn62m6i05391ngx06436ixdkymzgyqwgd8j0jsmil1nv") (f (quote (("impl_num" "num-traits") ("default" "impl_num")))) (y #t)))

(define-public crate-tuple-0.1.5 (c (n "tuple") (v "0.1.5") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)))) (h "1avvgp18xr12470vgig3slimsxzv8nsnhj151wvdxy1h72br20yx") (f (quote (("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.1.6 (c (n "tuple") (v "0.1.6") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)))) (h "1mha1vk2x645kcffgzsrhrflsd4pfzvy96inhn1zz68j6zj6hpyl") (f (quote (("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.1.7 (c (n "tuple") (v "0.1.7") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "simd") (r "0.2.*") (o #t) (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0dryai5hkz7m3nzh9qdfl58m8pyy15p6d7zlgnvbygjrs5n96sp5") (f (quote (("impl_simd" "simd") ("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.2.0 (c (n "tuple") (v "0.2.0") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "simd") (r "0.2.*") (o #t) (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1fkh76kdjh789vw7r6ychril9whj8y019lw672xs0ih9y1vgggys") (f (quote (("impl_simd" "simd") ("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.2.1 (c (n "tuple") (v "0.2.1") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "simd") (r "0.2.*") (o #t) (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "04qiaczf6hkyfzdpr4vlcd1a5g5mva4nclbkv19dpdkxl26v0p5y") (f (quote (("impl_simd" "simd") ("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.2.2 (c (n "tuple") (v "0.2.2") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "simd") (r "0.2.*") (o #t) (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "16j7xck6w0qfm1q1phpjv7jv7xszbamw8rphs45z4m3zn133glps") (f (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.2.3 (c (n "tuple") (v "0.2.3") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "simd") (r "0.2.*") (o #t) (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1kc0iyaglapklka6sqf4c56sik5916p5n0d99hl4af9dxkxbbkpf") (f (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3.0 (c (n "tuple") (v "0.3.0") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "simd") (r "0.2.*") (o #t) (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0xzikmbwiws8qn7d5ay2ryl7n6jc0dhp8sxsn744858vshdfb2xl") (f (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3.1 (c (n "tuple") (v "0.3.1") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "simd") (r "0.2.*") (o #t) (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1n5ks368kslbqbh0l22jv414wvrdhmh6wsw8d6kgv6bncnjnpmdb") (f (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3.2 (c (n "tuple") (v "0.3.2") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "simd") (r "0.2.*") (o #t) (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0xhv1bqv6bypaws3k64r9hyzl1qk9l8f7pxzq5r5d9dkqa3cymbp") (f (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3.3 (c (n "tuple") (v "0.3.3") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "simd") (r "0.2.*") (o #t) (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0q4gcr98bcxn2b556qx93p9isachppjpslnwlrpxnywcq2d606za") (f (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3.4 (c (n "tuple") (v "0.3.4") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "simd") (r "0.2.*") (o #t) (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "102kk7gr9xm1vyqhj5wrnwk7rjpz8yz7qabypimzlmjzml9hkfxf") (f (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3.5 (c (n "tuple") (v "0.3.5") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "simd") (r "0.2.*") (o #t) (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1y19gy4s172vfzkl0485m2glcrcqa0i879kp3qn1qfwpfibmm2hl") (f (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3.8 (c (n "tuple") (v "0.3.8") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "stdsimd") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "0lhzf2j4lnvg07w5xidh5k1ypim1b7p3rkbsf9p6ydq4fyha8qv6") (f (quote (("impl_simd" "stdsimd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3.9 (c (n "tuple") (v "0.3.9") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "stdsimd") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "0j8npmrvxl31qnjwhxxcnq9igxqihnij019818nlrvkn0fa9b387") (f (quote (("nightly") ("impl_simd" "stdsimd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.4.0 (c (n "tuple") (v "0.4.0") (d (list (d (n "num-traits") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)))) (h "1anyzp2s3dlcwwnhs5vk00m66gj76wh49zwr0jhfr3mrdwbjgm9y") (f (quote (("nightly") ("impl_simd" "nightly") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.4.1 (c (n "tuple") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "simd") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1y4smbdzvzpf71sr721ig4c0hmq38blrq347ix2ahjkz1drfa6q5") (f (quote (("nightly") ("impl_simd" "nightly" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.4.2 (c (n "tuple") (v "0.4.2") (d (list (d (n "num-traits") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "simd") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1whpydck7jkx6kz4wzws6pwsyaskd2rbasl9xc1kafbz3fl96i01") (f (quote (("nightly") ("impl_simd" "nightly" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.5.0 (c (n "tuple") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "packed_simd") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "15iq6laxmi7myc07w99hg01r5nlx7dj1hy3i4dmhsxdw37ja0acp") (f (quote (("nightly") ("impl_simd" "nightly" "packed_simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.5.1 (c (n "tuple") (v "0.5.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "packed_simd") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "03kxril16i1q33v2y6nn72wnd8a1q5hmzp17r5s12zh486i0p91r") (f (quote (("nightly") ("impl_simd" "nightly" "packed_simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.5.2 (c (n "tuple") (v "0.5.2") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zn3q7dly0d2jqpg04rxc9a37adc2zgkxvm8icaq3527ffyzdfcv") (f (quote (("std") ("nightly") ("impl_simd" "nightly") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde" "std"))))))

