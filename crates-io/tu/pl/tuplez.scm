(define-module (crates-io tu pl tuplez) #:use-module (crates-io))

(define-public crate-tuplez-0.1.0 (c (n "tuplez") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.1.0") (d #t) (k 0)))) (h "167n15ssz1xc0asr60jpcnfdn4645iga3f29lr6p6ph8xb0q39rd") (f (quote (("default") ("any_array")))) (y #t)))

(define-public crate-tuplez-0.1.1 (c (n "tuplez") (v "0.1.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1mnhvhc3m5mjab775mk288lc7gl9ych8x6pig730nh4xgbyfsvag") (f (quote (("default") ("any_array"))))))

(define-public crate-tuplez-0.1.3 (c (n "tuplez") (v "0.1.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.1.2") (d #t) (k 0)))) (h "07a4mj8ax13pd16ljslqdcgzwcnav3qdygifm4z87w3q4sw35bm0") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.1.4 (c (n "tuplez") (v "0.1.4") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.1.2") (d #t) (k 0)))) (h "0vrkph1bdiqwqjj6fn96b09h1ynwcc3m3qfwgp38z0a05gk47hv8") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.2.0 (c (n "tuplez") (v "0.2.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.1.3") (d #t) (k 0)))) (h "0dd9zx68zf7qkc2w9wifwz7kxl5p5bky0rzryi8l5s2dz36bw123") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.2.1 (c (n "tuplez") (v "0.2.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.1.3") (d #t) (k 0)))) (h "07h6rv20sp0i7v3nphjrcgj6xbkw2xl5q85dm0hzmxsqjz6h65gg") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.3.0 (c (n "tuplez") (v "0.3.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1vkrnqbfz4k7dparcw8c4pkfjirggqchra4h6s9n6n5kjcgbj1wb") (f (quote (("unwrap") ("default" "unwrap") ("any_array")))) (y #t)))

(define-public crate-tuplez-0.3.1 (c (n "tuplez") (v "0.3.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.2.0") (d #t) (k 0)))) (h "055y8m1mvmzfhhg3cm604k5lrlrpir2abacn7znhfd1rx6k3fdcl") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.3.2 (c (n "tuplez") (v "0.3.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0kx9775yiryh9973263vxh33q6g2hvrr486kndqibwyqvgyndjnj") (f (quote (("unwrap") ("default" "unwrap") ("any_array")))) (y #t)))

(define-public crate-tuplez-0.3.3 (c (n "tuplez") (v "0.3.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.2.0") (d #t) (k 0)))) (h "02vxg1cl74icpqv7n1n8hg0778w5b1l8kmn4cwnwkrd7aql4bl3v") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.3.4 (c (n "tuplez") (v "0.3.4") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.2.0") (d #t) (k 0)))) (h "142210qn79s1rv077p22lpb34f2983iz8iqclk64wa9p421929mp") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.4.0 (c (n "tuplez") (v "0.4.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0h5m4hi91n6as4bzqi4fkpxqzbznnaldvv0hjb7fn3010jn90f2c") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.4.1 (c (n "tuplez") (v "0.4.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.3.0") (d #t) (k 0)))) (h "16sy7q9c4ffxsgykjg9v6c8zmv4620x4jmijzynqvnry5cvfa67x") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.4.2 (c (n "tuplez") (v "0.4.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.3.1") (d #t) (k 0)))) (h "1rlw180k2h847nxwzi2l9g7byrm1j1yriq6lmf6i4yhx47vbz3rh") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.4.3 (c (n "tuplez") (v "0.4.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.3.1") (d #t) (k 0)))) (h "1a3il2kipd82q179c1dn72n4frjdamb0jxzdzhhln4k5qxhivdb5") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.5.0 (c (n "tuplez") (v "0.5.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.3.1") (d #t) (k 0)))) (h "1z74icpn5grr7mdsx6wmqyallqs7ph44hkd9h8vd60wcyphmqxrq") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.5.1 (c (n "tuplez") (v "0.5.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.3.1") (d #t) (k 0)))) (h "1lhsf73yn3x68ffyfrnbbqyzwx96mds6pk96p0l4jdpfsnk5rp4w") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.5.2 (c (n "tuplez") (v "0.5.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.3.1") (d #t) (k 0)))) (h "03qglifm58x6ax421h5kpmwlyzy6iw2b9f9lgh84rfgr0r2ms9nr") (f (quote (("unwrap") ("default" "unwrap") ("any_array")))) (y #t)))

(define-public crate-tuplez-0.5.3 (c (n "tuplez") (v "0.5.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.3.1") (d #t) (k 0)))) (h "0kkx426a6gyyx6mhl8phpyq4na5h8gd972bl3lnssc0m63xx23fn") (f (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.6.0 (c (n "tuplez") (v "0.6.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.3.2") (d #t) (k 0)))) (h "1v0armd5l3r4la30s81x3llwfqa0ss5hwnc25qcw8h89cpgm2k4k") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.6.1 (c (n "tuplez") (v "0.6.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.3.3") (d #t) (k 0)))) (h "0k7hph0i4m31dnrilk7rvb7sslk7a6s38ksk0qshzvdphb8swy1g") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.6.2 (c (n "tuplez") (v "0.6.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.3.4") (d #t) (k 0)))) (h "1s20frnmcpz501hlwn00f8ly4nrap13528w32hx3f77d9frhvjyg") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.6.3 (c (n "tuplez") (v "0.6.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.3.5") (d #t) (k 0)))) (h "11cxrn8lzbkk2k6nai368hkk9srs8wghmrgni1n7r7k7xfvvhvc2") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.7.0 (c (n "tuplez") (v "0.7.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.4.0") (d #t) (k 0)))) (h "0q69bjfx8bk92yqzdzdcms1k39cfbkmsda2n4j5yfmdhyg263j3s") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array")))) (y #t)))

(define-public crate-tuplez-0.7.1 (c (n "tuplez") (v "0.7.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.4.0") (d #t) (k 0)))) (h "0xfn18i6y9i1mkpgfjpd8xg9hl19q9h310a3qk2zqb0jm5709myz") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.8.0 (c (n "tuplez") (v "0.8.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.5.0") (d #t) (k 0)))) (h "02s5bcfks0bz7mkd1ky62qal4031a69xblkqz6715faxrw58mxd4") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.8.1 (c (n "tuplez") (v "0.8.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.5.1") (d #t) (k 0)))) (h "0w1qpfgmwgx3z34szv3gmq2p3ql0kw13lqg7rr3id2vz8g38zpp7") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.8.2 (c (n "tuplez") (v "0.8.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.5.2") (d #t) (k 0)))) (h "0waq3hkv1gxznw8vspj0db5kb8bmabyn047rq6w2mdzv4fa5mamb") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.9.0 (c (n "tuplez") (v "0.9.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.5.2") (d #t) (k 0)))) (h "0jwbyydfsgmmchkqfd1bny857as2mrmyikgi64dv505g482g4fxj") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.9.1 (c (n "tuplez") (v "0.9.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.5.2") (d #t) (k 0)))) (h "1gh2w7bvny5kbc1sqqbljs5gzfbaxxm3z9w260nf479db49y9gbj") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.9.2 (c (n "tuplez") (v "0.9.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.5.2") (d #t) (k 0)))) (h "1pacjq11xgim0jg8g0b3sfxp5y0nszwz8sx9x9z66slv4vkm0f0f") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.10.0 (c (n "tuplez") (v "0.10.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.5.2") (d #t) (k 0)))) (h "0hra5wm103w63m4b259zk74d5lbjnwvzivqyvhnils8flvn6yrr5") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.10.1 (c (n "tuplez") (v "0.10.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.5.2") (d #t) (k 0)))) (h "1jykfp6kbfjq3fgjym7m6jib4r36lfsdishamw3m7w9nkcbz5syg") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.10.2 (c (n "tuplez") (v "0.10.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.5.2") (d #t) (k 0)))) (h "176w6virbbqwr654hqa7pddqd3ih3756yz263hjr5zwyx479fyba") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.11.0 (c (n "tuplez") (v "0.11.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.5.2") (d #t) (k 0)))) (h "0ak317pg6aiy0byfpcjya6h02q7raz21iscif67pfnqv0px6q51k") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.11.1 (c (n "tuplez") (v "0.11.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.5.2") (d #t) (k 0)))) (h "0vs7a4w64b6hbm29hqd77di8m98vis7k0aq4ri7d917gr3bwphn9") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.11.2 (c (n "tuplez") (v "0.11.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.5.2") (d #t) (k 0)))) (h "0462divl9wlnfsj3g93ssvb24kzi786blc2xcrqj5mdfi467c7d2") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.12.0 (c (n "tuplez") (v "0.12.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.0") (d #t) (k 0)))) (h "15k5gzb3c8sgv40gfaqqd5fz2ggir1g536b2dfy5dp00jbhs2313") (f (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.13.0 (c (n "tuplez") (v "0.13.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.0") (d #t) (k 0)))) (h "1shha975khm18ckjc3xy28miyiaqv1lfgcky20xd9sdnmh2s277a") (f (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array")))) (y #t)))

(define-public crate-tuplez-0.14.0 (c (n "tuplez") (v "0.14.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.0") (d #t) (k 0)))) (h "0ykrbzcvqlc0pyhj63mwp2iskv28fv2li466jfinwr9xirbi307x") (f (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14.1 (c (n "tuplez") (v "0.14.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.0") (d #t) (k 0)))) (h "1f8dbqnrmrgvp262kxirzp368wwbfjbyhb2ajliybczi5gr7br49") (f (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14.2 (c (n "tuplez") (v "0.14.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.0") (d #t) (k 0)))) (h "1kk48jyiq3qnmn7w4hqxn1q08y7nbm7axsvirsgzhzy0n3qxcgra") (f (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14.3 (c (n "tuplez") (v "0.14.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.0") (d #t) (k 0)))) (h "00s79qziqvm7whcfc55xjbsbhvbv7cgqzzhqgniaz4k2f3lq7nqa") (f (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14.4 (c (n "tuplez") (v "0.14.4") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.0") (d #t) (k 0)))) (h "0srrll671vdhjp3dxs2g89h76h3pcyjdpcxgjplw5yg5dkrm5pcv") (f (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14.5 (c (n "tuplez") (v "0.14.5") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.0") (d #t) (k 0)))) (h "1sszr5njxjcri9l8lijaj4z8jvzi3zc34la8q47k01lrvy8m74yy") (f (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14.6 (c (n "tuplez") (v "0.14.6") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.0") (d #t) (k 0)))) (h "0mfyxp7ln38df2x36mcv53702mvncmdv5xj00k9k828slfpylyw9") (f (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14.7 (c (n "tuplez") (v "0.14.7") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.0") (d #t) (k 0)))) (h "03jpvs74a03pgdgm2iw665v9b5abnj6rjxv608g0mjc5kppyzwsq") (f (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14.8 (c (n "tuplez") (v "0.14.8") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.0") (d #t) (k 0)))) (h "1rmxq764gv5fwp3vywrscw0qgrqmmxczlnkw1dv4c8razvvjbb8k") (f (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array")))) (y #t)))

(define-public crate-tuplez-0.14.9 (c (n "tuplez") (v "0.14.9") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.0") (d #t) (k 0)))) (h "12wpxzyvk55abj15rrccvcrni8n03hh96r34f33idav2hp30268v") (f (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14.10 (c (n "tuplez") (v "0.14.10") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.1") (d #t) (k 0)))) (h "0k7qpgxw727mgbgj4ggb1ykigjvi63y97dd3ay9ign20dn6w7ayk") (f (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14.11 (c (n "tuplez") (v "0.14.11") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.1") (d #t) (k 0)))) (h "1h2nx32jc5y8l0hxy11hw94z75lbjjfapxzg6l1rbsdmsi3r5qbx") (f (quote (("unwrap") ("uninit") ("full-no-std" "serde" "uninit" "unwrap") ("full-nightly" "full" "any_array") ("full" "std" "full-no-std") ("default" "std" "unwrap") ("any_array")))) (s 2) (e (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-tuplez-0.14.12 (c (n "tuplez") (v "0.14.12") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tuplez-macros") (r "^0.6.1") (d #t) (k 0)))) (h "03774by2kyhjqmnxhnn7pjzdjyqh5zyaamnpikavglrb2zd1lxvd") (f (quote (("unwrap") ("uninit") ("full-no-std" "serde" "uninit" "unwrap") ("full-nightly" "full" "any_array") ("full" "std" "full-no-std") ("default" "std" "unwrap") ("any_array")))) (s 2) (e (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-tuplez-0.14.13 (c (n "tuplez") (v "0.14.13") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tuplez-macros") (r "^0.7.0") (d #t) (k 0)))) (h "08c81xnripfgk9vx2xqdgdjix2g9ybk4r3854ww9kq6l8cmzv9g5") (f (quote (("unwrap") ("uninit") ("full-no-std" "serde" "uninit" "unwrap") ("full-nightly" "full" "any_array") ("full" "std" "full-no-std") ("default" "std" "unwrap") ("any_array")))) (y #t) (s 2) (e (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-tuplez-0.14.14-alpha (c (n "tuplez") (v "0.14.14-alpha") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tuplez-macros") (r "^0.7.0") (d #t) (k 0)))) (h "0v2ca2zf11pw6qfck37g0llipys6bzyvnkv15013sfgmgi3dnjvp") (f (quote (("unwrap") ("uninit") ("full-no-std" "serde" "uninit" "unwrap") ("full-nightly" "full" "any_array") ("full" "std" "full-no-std") ("default" "std" "unwrap") ("any_array")))) (s 2) (e (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-tuplez-0.14.14 (c (n "tuplez") (v "0.14.14") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tuplez-macros") (r "^0.7.0") (d #t) (k 0)))) (h "142slwfx9iijmjpzhm9i2s1sc59ja0kjzxxrf9ic1rq7vrprrjjp") (f (quote (("unwrap") ("uninit") ("full-no-std" "serde" "uninit" "unwrap") ("full-nightly" "full" "any_array") ("full" "std" "full-no-std") ("default" "std" "unwrap") ("any_array")))) (s 2) (e (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

