(define-module (crates-io tu pl tupleops-macros) #:use-module (crates-io))

(define-public crate-tupleops-macros-0.1.0 (c (n "tupleops-macros") (v "0.1.0") (d (list (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)) (d (n "tupleops-generator") (r "^0.1.0") (d #t) (k 0)))) (h "1akwf6sd9bwbf1fm5vgmg3g1kh7ll2qchc2dllj6xiz1bl10nfxf")))

