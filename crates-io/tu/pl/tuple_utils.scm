(define-module (crates-io tu pl tuple_utils) #:use-module (crates-io))

(define-public crate-tuple_utils-0.1.0 (c (n "tuple_utils") (v "0.1.0") (h "16gdgnbpk6ldylnbc0ksxbscnykiqzjqwmd5ysflb6xqxglci6h6")))

(define-public crate-tuple_utils-0.2.0 (c (n "tuple_utils") (v "0.2.0") (h "0zhfnby28530hks9203wfs4aambsczr6mi1i7dmyk8zhp1xwvznb")))

(define-public crate-tuple_utils-0.3.0 (c (n "tuple_utils") (v "0.3.0") (h "1wgl7a32a9gvcxiyxznmglc9kc4d2j7c4dfzpr3nzcf5w8c490s4")))

(define-public crate-tuple_utils-0.4.0 (c (n "tuple_utils") (v "0.4.0") (h "1sch8brl6j1xlr7r6mkql4vzm8mafqj1b5w7h8qcswzgja9szyng")))

