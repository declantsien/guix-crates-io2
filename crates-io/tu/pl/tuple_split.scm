(define-module (crates-io tu pl tuple_split) #:use-module (crates-io))

(define-public crate-tuple_split-0.1.0 (c (n "tuple_split") (v "0.1.0") (d (list (d (n "blk_count_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "tupleops") (r "^0.1.1") (f (quote ("concat"))) (d #t) (k 0)))) (h "01473x68f3ygg58ngsaknskjwv25bws4p2npqqksn5wp1n79znkl")))

(define-public crate-tuple_split-0.1.1 (c (n "tuple_split") (v "0.1.1") (d (list (d (n "blk_count_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "tupleops") (r "^0.1.1") (f (quote ("concat"))) (d #t) (k 0)))) (h "1jg2yar1zlxv37gls0y5vj8kmgx21qpz7gvmc3w0wvc2n7jyc9cx")))

