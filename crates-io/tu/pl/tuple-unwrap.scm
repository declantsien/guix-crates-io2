(define-module (crates-io tu pl tuple-unwrap) #:use-module (crates-io))

(define-public crate-tuple-unwrap-0.1.0 (c (n "tuple-unwrap") (v "0.1.0") (h "11nmkwm6kny5bzp4fqygsdlpzdq983xficgxpdc567wi42ai76cb")))

(define-public crate-tuple-unwrap-0.2.0 (c (n "tuple-unwrap") (v "0.2.0") (h "0fhvapl6cx88i8p8cxk0d6m5z9k1053svy5gagnxfdnm3ngcp70r")))

