(define-module (crates-io tu pl tuple-types) #:use-module (crates-io))

(define-public crate-tuple-types-0.1.0 (c (n "tuple-types") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "1xkimdk9gn3ab40m1m6v4c1c6kagqvxzla75sih2g42a4v1a8626")))

(define-public crate-tuple-types-0.1.1 (c (n "tuple-types") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "06x205wf84a8ralqiwp7qkd4mx19vpzg9m3byny0sqmcqa76b675")))

