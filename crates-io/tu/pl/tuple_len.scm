(define-module (crates-io tu pl tuple_len) #:use-module (crates-io))

(define-public crate-tuple_len-0.1.0 (c (n "tuple_len") (v "0.1.0") (h "1knlz1lrb9py6hbbay58qvxj8sdh1hak7ryn3l7lwryhwj21mhvv")))

(define-public crate-tuple_len-0.1.1 (c (n "tuple_len") (v "0.1.1") (h "17g1iw2w56xd6ka50zcl23l0mqnzgf2j47gzbqy69vv5cvqdizwm")))

(define-public crate-tuple_len-1.0.0 (c (n "tuple_len") (v "1.0.0") (h "0aglj0fyj03mgiszf1b1ymjz47a9i1z3ksbbzxw8ldksddqfrxbi")))

(define-public crate-tuple_len-1.1.0 (c (n "tuple_len") (v "1.1.0") (h "1fy2bb8ln45sph2ymh47g6gc66i2jl8l5r8x9df8zvzfk5hik4i7")))

(define-public crate-tuple_len-2.0.0 (c (n "tuple_len") (v "2.0.0") (h "14c6afc74xgpxz0rz142qjdk4hyfylg4y3n93p0wxqpz7znp3q48")))

(define-public crate-tuple_len-3.0.0 (c (n "tuple_len") (v "3.0.0") (h "16ijkawaylqnwmgblz61irha8hgbjm12g8vhgbslcx95zcb4drzi")))

