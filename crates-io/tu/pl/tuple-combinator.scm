(define-module (crates-io tu pl tuple-combinator) #:use-module (crates-io))

(define-public crate-tuple-combinator-0.1.0 (c (n "tuple-combinator") (v "0.1.0") (h "07k37ap1b705fcd2hx4j7rz97c9khg734csbw7nh7pimdzkjvf0w")))

(define-public crate-tuple-combinator-0.1.1 (c (n "tuple-combinator") (v "0.1.1") (h "0b3ycnc243nm8yb17fxa1001ymcz6a280sv950vbvsrgqka3iz8s")))

(define-public crate-tuple-combinator-0.2.0 (c (n "tuple-combinator") (v "0.2.0") (h "1w2qyx9j81drig6w3d52ynhhxqjdzn3f8ccv111amvv6gpjfw1yz")))

(define-public crate-tuple-combinator-0.2.1 (c (n "tuple-combinator") (v "0.2.1") (h "1gbiv9zzpxg5069p48dfbvx0xa49f6f0ls0ikmcldskvikckjg8w")))

