(define-module (crates-io tu pl tuplex) #:use-module (crates-io))

(define-public crate-tuplex-0.0.1 (c (n "tuplex") (v "0.0.1") (h "002vm1g36hpr8asdq1ip7n9mj73ijvzxvf27bgw17d2jb988iy7n")))

(define-public crate-tuplex-0.1.0 (c (n "tuplex") (v "0.1.0") (h "08982g9572ck1l8rnyma3cl8ph2570di4wpwr9qndgad9qa7p912")))

(define-public crate-tuplex-0.1.1 (c (n "tuplex") (v "0.1.1") (h "0hgsnsj32y36z94n3mvxxc05j4lmr84h02g15lxrc9mgyv9vbmqh")))

(define-public crate-tuplex-0.1.2 (c (n "tuplex") (v "0.1.2") (h "1ah85a6id43v9ahnrf24yy839pkfcal4zlsmg7rxri2lahfwhsk7") (f (quote (("std") ("default" "std"))))))

