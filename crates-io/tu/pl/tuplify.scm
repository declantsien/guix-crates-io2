(define-module (crates-io tu pl tuplify) #:use-module (crates-io))

(define-public crate-tuplify-1.0.0 (c (n "tuplify") (v "1.0.0") (h "157z7g4gfm3pcp507bl72i7ijhx1vj6s6ws5nnzaxpm6ymvhjvm2")))

(define-public crate-tuplify-1.0.1 (c (n "tuplify") (v "1.0.1") (h "0m5b5qzah5561fm8swacvpcaypmlalfwq58as64n128kcphcbi2c")))

(define-public crate-tuplify-1.0.2 (c (n "tuplify") (v "1.0.2") (h "0pxvam5l391iwgra18n73if3a7hxsla6nhm6bpklmvk79bkidiah")))

(define-public crate-tuplify-1.0.3 (c (n "tuplify") (v "1.0.3") (h "1dgmpw09nrbif3x37zhdj9h0awmlk9p8pzx2byqrlqw4i85awvx7")))

(define-public crate-tuplify-1.1.3 (c (n "tuplify") (v "1.1.3") (h "1rkmxqdwfyp5im5wb4cqbk1lp95qcizlyfvhl3xa11yxfrrj0j6b")))

(define-public crate-tuplify-1.1.4 (c (n "tuplify") (v "1.1.4") (h "11s0phsh25j6xlk7yh05467gcbsmb7nb64fnfddawg2wwn5v3jhk")))

