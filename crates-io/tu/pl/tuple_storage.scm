(define-module (crates-io tu pl tuple_storage) #:use-module (crates-io))

(define-public crate-tuple_storage-0.1.0 (c (n "tuple_storage") (v "0.1.0") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1p8bbg224i4qc0c2mqhnyy873kr4y5ds1kcv7ndkirz513ss9s8r")))

(define-public crate-tuple_storage-0.1.1 (c (n "tuple_storage") (v "0.1.1") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1f962pjwvg4rkvg73fgr7ring1hsx6lgdq1x4d9kywh8xc2sb3kd")))

