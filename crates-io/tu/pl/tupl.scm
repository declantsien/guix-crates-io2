(define-module (crates-io tu pl tupl) #:use-module (crates-io))

(define-public crate-tupl-0.1.0 (c (n "tupl") (v "0.1.0") (h "0wj3rwd1brrpmi4d3pvcb8pfp1fv22liwp29p8kkmi6xx8gi723f") (r "1.65")))

(define-public crate-tupl-0.2.0 (c (n "tupl") (v "0.2.0") (h "152gwh6w0z65lrzpys0kdgpcfvmmfxzw6jcfpi0vg7rldi407ik7") (r "1.65")))

(define-public crate-tupl-0.3.0 (c (n "tupl") (v "0.3.0") (h "0m2drq6v3ghh98a4dhwgrjq5js130dx1izrqb59l500aynpjlbi8") (r "1.65")))

(define-public crate-tupl-0.3.1 (c (n "tupl") (v "0.3.1") (h "17hhnrscxm4vs0271jqi20za4kz2x3hrvjcj00rhlxidr17lz4ch") (r "1.65")))

(define-public crate-tupl-0.4.0 (c (n "tupl") (v "0.4.0") (h "1i0lhb26zvks8f1pj58nx7a0xhay4knw1k8q2jg3bmvlca4hj0pp") (r "1.65")))

