(define-module (crates-io tu pl tuple-iter) #:use-module (crates-io))

(define-public crate-tuple-iter-0.1.0 (c (n "tuple-iter") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)))) (h "1xdgsqbbwz4191hxgzbzavlgaq63md6fw2kvlcmlzlkg0kdx9aic")))

