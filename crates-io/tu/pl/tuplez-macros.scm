(define-module (crates-io tu pl tuplez-macros) #:use-module (crates-io))

(define-public crate-tuplez-macros-0.1.0 (c (n "tuplez-macros") (v "0.1.0") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1cgxqhpra4sjz2580n8li70yrvn5ddhim5kvjs8nbfblv65bm11h") (y #t)))

(define-public crate-tuplez-macros-0.1.1 (c (n "tuplez-macros") (v "0.1.1") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "188yxspll9m15aiqhxww0kzjlrf7h47jb5f36m7j3zkhaid02m4s")))

(define-public crate-tuplez-macros-0.1.2 (c (n "tuplez-macros") (v "0.1.2") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "03fpdx47jjsfv7dsi9pp1y1my6z8s65a75bdbirhk04ybj604bq7")))

(define-public crate-tuplez-macros-0.1.3 (c (n "tuplez-macros") (v "0.1.3") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "04d3ldacy6pv4aynbkn47297kpl57qzn5vysznzkhv2birr9hg5w")))

(define-public crate-tuplez-macros-0.2.0 (c (n "tuplez-macros") (v "0.2.0") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1rxym9k4nprhwrzch3sm3mq6vbir58zp995ydya3bm3rk6szirq9")))

(define-public crate-tuplez-macros-0.3.0 (c (n "tuplez-macros") (v "0.3.0") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1ah3c8c9brrdxbmhqsy51a9c3mq922rc62xavmkgz2c9g2339mrb")))

(define-public crate-tuplez-macros-0.3.1 (c (n "tuplez-macros") (v "0.3.1") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1kgy7vj60mj8zix41rx70gxq1djrpnj13cz2xjnq833mdxnjiri6")))

(define-public crate-tuplez-macros-0.3.2 (c (n "tuplez-macros") (v "0.3.2") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0i15axhbn5h74wqvz47gj131al4mgf8ccsg48csx8727gpqd3ybj")))

(define-public crate-tuplez-macros-0.3.3 (c (n "tuplez-macros") (v "0.3.3") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "18mawfn7pk098wvyqx3c8f7rhj2g3vp39xnn6fqgrrlazf78nkwc")))

(define-public crate-tuplez-macros-0.3.4 (c (n "tuplez-macros") (v "0.3.4") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0rqdiv1x0m0gpa7nxkf1ayd5amjh0dyfyjy0gmnjqc7mkiq9igf3")))

(define-public crate-tuplez-macros-0.3.5 (c (n "tuplez-macros") (v "0.3.5") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0zd2hfcwz0v4m0zr34sqxfgakfiq05n4dn8ypmcah6fbr7hxsla7")))

(define-public crate-tuplez-macros-0.4.0 (c (n "tuplez-macros") (v "0.4.0") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "12f7q23z3jac65lqfwn91h9rhsqiapmfkwb2q8g9f80h1vldq3xf")))

(define-public crate-tuplez-macros-0.5.0 (c (n "tuplez-macros") (v "0.5.0") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0yzrdb95i2ik63xamaqq221mdcqci9cwnm3wrsnfdj0xf169n1rg")))

(define-public crate-tuplez-macros-0.5.1 (c (n "tuplez-macros") (v "0.5.1") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0d5wnxk67f1mzv9y4b07nv13a31rzs3bshynq0dkyi09v3fvf7ww")))

(define-public crate-tuplez-macros-0.5.2 (c (n "tuplez-macros") (v "0.5.2") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "083122hzn2a3jihfwjqmcsavq6hfianf7xlj8hjj5d5h3rc34wgq")))

(define-public crate-tuplez-macros-0.6.0 (c (n "tuplez-macros") (v "0.6.0") (d (list (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1dhimj1rg0cghznxd2g3gd9p6fclqwl5zy5rjk2r7jxbv1rl4k45")))

(define-public crate-tuplez-macros-0.6.1 (c (n "tuplez-macros") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cfvz740hwam0fv2p8a5v0hhrzdg9y5zsvimwynpacc9acjhns3x")))

(define-public crate-tuplez-macros-0.7.0 (c (n "tuplez-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17hign831gmp161h2p69q9c5qjcfswnsxch6daq2mj3cnjcac29a")))

