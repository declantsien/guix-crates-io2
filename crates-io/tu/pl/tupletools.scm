(define-module (crates-io tu pl tupletools) #:use-module (crates-io))

(define-public crate-tupletools-0.1.0 (c (n "tupletools") (v "0.1.0") (h "178dxaxv14hqkdpj16a51izk803ly7gx7ragz9vk1p507zn336mp") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-tupletools-0.1.1 (c (n "tupletools") (v "0.1.1") (h "1hy65b9mkb00ma5c2aw30br8vjqysaxy9v6zdhwh19xclj1p15jh") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-tupletools-0.2.1 (c (n "tupletools") (v "0.2.1") (h "11mm63vwg527zpnvs0hg1p5gbal7vhqq5b92ggdd70f40bvax8jq") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-tupletools-0.2.2 (c (n "tupletools") (v "0.2.2") (h "1gi84m1ip077lis528msznddsw9gdb4dv4kcv6r7wllxhl2rw530") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-tupletools-0.2.3 (c (n "tupletools") (v "0.2.3") (h "1nqky8lgmr1cpcf8xxvx185bafnc0w4mb2hhn5rmnnfr0g1c865y") (f (quote (("no_std"))))))

(define-public crate-tupletools-0.2.4 (c (n "tupletools") (v "0.2.4") (h "04dmlzn8cyz4vzpqi07crd8y83xp5c4jp3ckj50zsa3p3k7zndm4") (f (quote (("no_std"))))))

(define-public crate-tupletools-0.2.5 (c (n "tupletools") (v "0.2.5") (h "07z3q0qm0ygdxjvznabd0dpkpif92k2sjzk0wy795igj81iakz5m") (f (quote (("no_std"))))))

(define-public crate-tupletools-0.3.0 (c (n "tupletools") (v "0.3.0") (h "1fzf9x97x5i4sa4jd0gw32q3nyp5r4sqvfar22wrb30smvk4pnh3") (f (quote (("no_std"))))))

(define-public crate-tupletools-0.4.0 (c (n "tupletools") (v "0.4.0") (h "05gdh34az4ickpc6iafh84d003vqpskqrkvmr3408dkh6r6jhfb9") (f (quote (("no_std"))))))

(define-public crate-tupletools-0.4.1 (c (n "tupletools") (v "0.4.1") (h "0yc2m5pj2achwxvnwph687lxgm3ipij6iywkd8gmx8psnzas1k2c") (f (quote (("no_std"))))))

