(define-module (crates-io tu pl tuple_tricks) #:use-module (crates-io))

(define-public crate-tuple_tricks-0.1.0 (c (n "tuple_tricks") (v "0.1.0") (d (list (d (n "make_tuple_traits") (r "^0.1.0") (d #t) (k 0)))) (h "1pdl85b452r3pnz7r84gp9k98yxbz79qv5q071m4wzjabj5a05az")))

(define-public crate-tuple_tricks-0.2.0 (c (n "tuple_tricks") (v "0.2.0") (d (list (d (n "make_tuple_traits") (r "^0.2.0") (d #t) (k 0)))) (h "1jrx6hylvgj5qg7hzic7w8w4h1h0si36c2mfs6j5ijbl24ig640c")))

(define-public crate-tuple_tricks-0.2.1 (c (n "tuple_tricks") (v "0.2.1") (d (list (d (n "make_tuple_traits") (r "^0.2") (d #t) (k 0)))) (h "03ic6l4xbxvfkw9cp9h1c9z69qh7bajy8riv07zdr3n22p3by0w0")))

