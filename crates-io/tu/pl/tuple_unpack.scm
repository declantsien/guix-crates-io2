(define-module (crates-io tu pl tuple_unpack) #:use-module (crates-io))

(define-public crate-tuple_unpack-1.0.0 (c (n "tuple_unpack") (v "1.0.0") (d (list (d (n "seq-macro") (r "^0.3.1") (d #t) (k 0)))) (h "1z3w8cn5xh3zjrjmh4akbi3y183d5x1qf45f5sg8469nbrb7wcck")))

(define-public crate-tuple_unpack-1.0.1 (c (n "tuple_unpack") (v "1.0.1") (d (list (d (n "seq-macro") (r "^0.3.1") (d #t) (k 0)))) (h "1p00bsd071yj4nvz1vnp27q1dq1xxva80bw7ahkskflkhri1igg0")))

