(define-module (crates-io tu pl tuple-map) #:use-module (crates-io))

(define-public crate-tuple-map-0.0.1 (c (n "tuple-map") (v "0.0.1") (h "1ppw15vk1p3wm4ia8figzv8k50n9a8h4mjlaxy3ypnkhcngb2gx3")))

(define-public crate-tuple-map-0.1.0 (c (n "tuple-map") (v "0.1.0") (h "1rqkp7vzchas68byjkn2466lapz9smzszfacnysj1cjzx8kh42rx")))

(define-public crate-tuple-map-0.2.0 (c (n "tuple-map") (v "0.2.0") (h "08ap1sbypw5sb0wlmlz41gwi5695mzqrlbk44svmk2zh51jsgkvm")))

(define-public crate-tuple-map-0.3.0 (c (n "tuple-map") (v "0.3.0") (h "0phm9z4kl9askl5cxi8c0qiny31y78p2xgy4rx9vh566bi5xk3ra")))

(define-public crate-tuple-map-0.4.0 (c (n "tuple-map") (v "0.4.0") (h "0qixmgjy86dmqfp9gra34sm1wysra0j9iymphgv7l8r1f6fr3m93")))

