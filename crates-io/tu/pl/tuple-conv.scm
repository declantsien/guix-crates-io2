(define-module (crates-io tu pl tuple-conv) #:use-module (crates-io))

(define-public crate-tuple-conv-1.0.0 (c (n "tuple-conv") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1lvynrv8x540n1kwn9ykagbjsrd9b84an985v0k1kphwbs7zkahw")))

(define-public crate-tuple-conv-1.0.1 (c (n "tuple-conv") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1p8nh7xqfxwainr31bsgrdqfid5cprgscw9xpg0j80rmhd333mhz")))

