(define-module (crates-io tu pl tuple-arity) #:use-module (crates-io))

(define-public crate-tuple-arity-0.1.0 (c (n "tuple-arity") (v "0.1.0") (h "0vrgvvsilm36qrcyz9b8zxyawiv19jynzqgazxw9hz311jd17vvf")))

(define-public crate-tuple-arity-0.1.1 (c (n "tuple-arity") (v "0.1.1") (h "1fqw64knyr53x3cyyi3da7lqnnnj0bl2yam37gl1cf5zjrnlz8db")))

(define-public crate-tuple-arity-0.1.2 (c (n "tuple-arity") (v "0.1.2") (h "0n6lw3a84j1s5v901ih754d9jyz26xw7m46flfphbn0xdh5vagq1")))

