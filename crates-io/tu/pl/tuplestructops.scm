(define-module (crates-io tu pl tuplestructops) #:use-module (crates-io))

(define-public crate-tuplestructops-0.1.0 (c (n "tuplestructops") (v "0.1.0") (d (list (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "1jpha4n9pdz7sqhb74as8j52dag8k64ikgmbl79ycl3nh9barw4a") (f (quote (("tuple_32") ("tuple_28") ("tuple_24") ("tuple_20"))))))

(define-public crate-tuplestructops-0.1.1 (c (n "tuplestructops") (v "0.1.1") (d (list (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "1hz2a8m1kjkz257g3g58vrkwggm4d8gdk1sp4izkbzgyc3gvv77d") (f (quote (("tuple_32") ("tuple_28") ("tuple_24") ("tuple_20"))))))

(define-public crate-tuplestructops-0.1.2 (c (n "tuplestructops") (v "0.1.2") (d (list (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "070n8l4k9yca59pj1bpx2j0r21zwgk4ryv6f7qxhxm8iaq3h2sns") (f (quote (("tuple_32") ("tuple_28") ("tuple_24") ("tuple_20"))))))

(define-public crate-tuplestructops-0.2.0 (c (n "tuplestructops") (v "0.2.0") (d (list (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "1h55n9bj0zpkyd5lfkj4ka4m64cjq43ddpvbpdwv0inp4wf4zgi7") (f (quote (("tuple_32") ("tuple_24") ("impl_docs"))))))

(define-public crate-tuplestructops-0.3.0 (c (n "tuplestructops") (v "0.3.0") (d (list (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "04kr11hv8fhlg4n1sm38y4jz3cqc6sly1m5gn6f860dfrmhc1nba") (f (quote (("tuple_32") ("tuple_24") ("impl_docs"))))))

