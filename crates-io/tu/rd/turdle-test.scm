(define-module (crates-io tu rd turdle-test) #:use-module (crates-io))

(define-public crate-turdle-test-0.1.0 (c (n "turdle-test") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "macrotest") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (k 0)))) (h "1q2jpdf5lr4dmmszdqazd2bxw93szl4rvv8jihmr1fk91zqmpyyg") (r "1.59")))

