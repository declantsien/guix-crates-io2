(define-module (crates-io tu im tuimager) #:use-module (crates-io))

(define-public crate-tuimager-0.1.0 (c (n "tuimager") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.0") (d #t) (k 0)) (d (n "tui") (r "^0.21") (f (quote ("crossterm" "macros"))) (k 0) (p "ratatui")) (d (n "viuer") (r "^0.6.2") (d #t) (k 0)))) (h "0ls3kvjsb7mcw6k5qaym12a9jyyb8hmzvp9cik1qm8mywcrh3ggv")))

