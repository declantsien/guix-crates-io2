(define-module (crates-io tu ri turingmachine-rs) #:use-module (crates-io))

(define-public crate-turingmachine-rs-0.1.0 (c (n "turingmachine-rs") (v "0.1.0") (h "0svcad3v400gwls9gm39c2792z1ypvqacvfsb4gmva5d79vz033k")))

(define-public crate-turingmachine-rs-0.2.0 (c (n "turingmachine-rs") (v "0.2.0") (h "0vjqjssm1kxmlwv64h54lg3p6y3p3miiwvq2hiw9wjhz482k4mvk")))

