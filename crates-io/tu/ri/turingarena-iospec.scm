(define-module (crates-io tu ri turingarena-iospec) #:use-module (crates-io))

(define-public crate-turingarena-iospec-0.1.0 (c (n "turingarena-iospec") (v "0.1.0") (d (list (d (n "annotate-snippets") (r "^0.9.0") (f (quote ("color"))) (d #t) (k 0)) (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "codemap") (r "^0.1.3") (d #t) (k 0)) (d (n "genco") (r "^0.15.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "11my3h4jmdyhas81rfwmh6dynq3qq06mm87z08p0j21n2hnj84ki")))

