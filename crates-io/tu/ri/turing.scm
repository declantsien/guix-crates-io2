(define-module (crates-io tu ri turing) #:use-module (crates-io))

(define-public crate-turing-0.0.0 (c (n "turing") (v "0.0.0") (d (list (d (n "bitvec") (r "^0.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wpbigwdp9xm7gqamgcypzib3zn22wj0zq54qmpqrwf59m62q8im")))

(define-public crate-turing-0.1.0 (c (n "turing") (v "0.1.0") (h "0cdxxsgff148xrz43kixdmqm5mvf28f5n6n2mx0myvb3q20yia06")))

