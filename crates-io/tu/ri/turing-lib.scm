(define-module (crates-io tu ri turing-lib) #:use-module (crates-io))

(define-public crate-turing-lib-1.1.2 (c (n "turing-lib") (v "1.1.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)))) (h "1fw99s4kv4123x6ipj304m3i6cfcnrbh64gbdi83nq4czjp536bn")))

(define-public crate-turing-lib-1.1.3 (c (n "turing-lib") (v "1.1.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)))) (h "12n2nqpcq7qmgv39y2smg36aidbngwidc6qfhvs917daw1glsmgy")))

(define-public crate-turing-lib-2.0.0 (c (n "turing-lib") (v "2.0.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "130w4p2qszschd4hvr7v8dn4wam33wf04bkzzgzan40gf283qd39")))

(define-public crate-turing-lib-2.0.1 (c (n "turing-lib") (v "2.0.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j6c8gjx11salabc2hfnrp1fkdq2czkxk1flp0fp6ra51367hydq")))

(define-public crate-turing-lib-2.0.2 (c (n "turing-lib") (v "2.0.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17jjyc05qi9wdgwc1x8g0adh7v15ch8cnw46wynji2a0sq97p6q8")))

(define-public crate-turing-lib-2.0.3 (c (n "turing-lib") (v "2.0.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16nkj8pxsj9qjb4c1b277sbcczk0c2vn8mwysnlisnb2y4icw3dv")))

(define-public crate-turing-lib-2.1.0 (c (n "turing-lib") (v "2.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ydd01h3ls09r4n95s8i3msgx5f9r9gd2xb2bz2qr9bn86xaynz1")))

(define-public crate-turing-lib-2.1.1 (c (n "turing-lib") (v "2.1.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0iirzawdxy9dwiyix5hppi583a9g2030zlmsz5v8bj76l24ki93l")))

(define-public crate-turing-lib-2.1.2 (c (n "turing-lib") (v "2.1.2") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19w49jaqn2midla9njajfakd227ajfm83g44j8s64jvvx088n9a8")))

(define-public crate-turing-lib-2.1.3 (c (n "turing-lib") (v "2.1.3") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b0dqcbi0isix49s6n8c8sca4abcllbg26bz89389q5zi2xsp6d2")))

(define-public crate-turing-lib-2.1.4 (c (n "turing-lib") (v "2.1.4") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xky90m590jc5g2w901qjdgrsir4rvyg0sb8rxfrgpv8m63fj37z")))

(define-public crate-turing-lib-2.1.5 (c (n "turing-lib") (v "2.1.5") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jh9d9ag4sgr12w7d0sy83fdyy5ig4sp51mqb03b9vrlklxjrqd1")))

(define-public crate-turing-lib-2.1.6 (c (n "turing-lib") (v "2.1.6") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09iv7mkhyrvvrxmn9hbmyrwcm818j3drkczq8s8anw7k7hdhd4fa")))

