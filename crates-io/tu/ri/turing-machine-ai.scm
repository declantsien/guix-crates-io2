(define-module (crates-io tu ri turing-machine-ai) #:use-module (crates-io))

(define-public crate-turing-machine-ai-0.1.0 (c (n "turing-machine-ai") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v7qclqgqfhbs94i8jyyjxkn5lv7bp3i5flw5lp5p1pzw1mvh1c2")))

