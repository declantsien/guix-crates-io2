(define-module (crates-io tu ri turingfeeds-helpers) #:use-module (crates-io))

(define-public crate-turingfeeds-helpers-1.0.0-beta.1 (c (n "turingfeeds-helpers") (v "1.0.0-beta.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "custom_codes") (r "^1.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n360ydvzcd7n4xggjipmzq9hrbh0a6vps9gvkjr3a5pn2kzrai0") (y #t)))

