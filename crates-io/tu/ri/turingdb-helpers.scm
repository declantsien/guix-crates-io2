(define-module (crates-io tu ri turingdb-helpers) #:use-module (crates-io))

(define-public crate-turingdb-helpers-2.0.0-beta.2 (c (n "turingdb-helpers") (v "2.0.0-beta.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.36") (d #t) (k 0)) (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "custom_codes") (r "^2.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tai64") (r "^3.1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1mwzr1ns2cahh4c4m9x4aqb3288j1swf83l0f8fd99sklc4xfvza")))

(define-public crate-turingdb-helpers-2.0.0-beta.3 (c (n "turingdb-helpers") (v "2.0.0-beta.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "custom_codes") (r "^2.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tai64") (r "^3.1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1yfjccnhdvfjck5caxk83v33sxb15mnxj6xhfl7xa9gf232qxyi4")))

(define-public crate-turingdb-helpers-2.0.0-beta.4 (c (n "turingdb-helpers") (v "2.0.0-beta.4") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "custom_codes") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tai64") (r "^3.1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1v81mfqdivbmhdqrbm0jqxjfjxszm404c71nlbky3qv71niza4h2")))

