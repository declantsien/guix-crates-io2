(define-module (crates-io tu ri turing-machine-rs) #:use-module (crates-io))

(define-public crate-turing-machine-rs-0.1.0-alpha (c (n "turing-machine-rs") (v "0.1.0-alpha") (h "038r9ps9q2ha7j1gc5dlqksmjshx3039zszqzd4sl5jpbx4c83l7") (f (quote (("string-as-copy") ("str-as-copy") ("default")))) (r "1.56")))

(define-public crate-turing-machine-rs-0.2.0 (c (n "turing-machine-rs") (v "0.2.0") (h "15my5nfrpn3k6rpiqv6bc1qdrgkkx9sw2skjhxrfiqz4d31p39ww") (r "1.56")))

