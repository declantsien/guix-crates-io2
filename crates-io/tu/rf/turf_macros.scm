(define-module (crates-io tu rf turf_macros) #:use-module (crates-io))

(define-public crate-turf_macros-0.1.0 (c (n "turf_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.1.0") (d #t) (k 0)))) (h "053xpfc5cdxdl4s93j97wrl4hy0kyn9s6bi51jb1dm8srfm8v5y2")))

(define-public crate-turf_macros-0.1.1 (c (n "turf_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.1.1") (d #t) (k 0)))) (h "1rjcrl86wlm89lzadyi3dnsaxhd9fjyaksi68yxyiibkynwzh7fz")))

(define-public crate-turf_macros-0.2.0 (c (n "turf_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.2.0") (d #t) (k 0)))) (h "15sqpzlvh0s8mw67wg6zalagz9l94fbmv8lpbkzq3skjyn97ncap")))

(define-public crate-turf_macros-0.2.1 (c (n "turf_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.2.0") (d #t) (k 0)))) (h "15lmk274yf2gy2186mfd9n3khlwfjpgvxj9mil7hicbfbpx10rzr")))

(define-public crate-turf_macros-0.3.0 (c (n "turf_macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.3.0") (d #t) (k 0)))) (h "13ani356mkdq946r324nachzbz44wp1m6ycswvv4wxnb05ivd4sd")))

(define-public crate-turf_macros-0.3.1 (c (n "turf_macros") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.3.1") (d #t) (k 0)))) (h "0k7pvd8wkkg2wnh5bvriqnribrbl33302pm08s418ah1rjqxdybq")))

(define-public crate-turf_macros-0.3.2 (c (n "turf_macros") (v "0.3.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.3.2") (d #t) (k 0)))) (h "05vbv2mlmblwmahmiyl44b8cp97ahv7ka1gv7zjsxcngjbjadf10")))

(define-public crate-turf_macros-0.4.0 (c (n "turf_macros") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.4.0") (d #t) (k 0)))) (h "098wv61yl9imhahxj30vayh7bmadlxg1da57ahsyw9rpn7xb5s0j") (f (quote (("once_cell" "turf_internals/once_cell"))))))

(define-public crate-turf_macros-0.4.1 (c (n "turf_macros") (v "0.4.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.4.1") (d #t) (k 0)))) (h "045qaxhk18ymshywg1fsyxdwfdpqi3159cc8xj4cx61lqsxq8j4r") (f (quote (("once_cell" "turf_internals/once_cell"))))))

(define-public crate-turf_macros-0.5.0 (c (n "turf_macros") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.5.0") (d #t) (k 0)))) (h "0q34wvq9kvznywwcllnmvd9xjr9qfszv0cd5016q3ciir6vlz22q") (f (quote (("once_cell" "turf_internals/once_cell"))))))

(define-public crate-turf_macros-0.6.0 (c (n "turf_macros") (v "0.6.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.6.0") (d #t) (k 0)))) (h "11zlhmbyyqq7jrsb50sq9f8vdwnjs7ly0n0yb4d7wrdy95s9x0wy") (f (quote (("once_cell" "turf_internals/once_cell"))))))

(define-public crate-turf_macros-0.6.1 (c (n "turf_macros") (v "0.6.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.6.1") (d #t) (k 0)))) (h "18sfvv07ykm16n4rpyvwqfjg6rbdxhx1vlwf9aw70cvsmsp8dmlf") (f (quote (("once_cell" "turf_internals/once_cell"))))))

(define-public crate-turf_macros-0.6.2 (c (n "turf_macros") (v "0.6.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.6.2") (d #t) (k 0)))) (h "1wqwbn25zz1v2q47f9k5ln8afvk96nl6mb96djl12igd4mylzm4j") (f (quote (("once_cell" "turf_internals/once_cell"))))))

(define-public crate-turf_macros-0.7.0 (c (n "turf_macros") (v "0.7.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.7.0") (d #t) (k 0)))) (h "18pswk30s5lwzhz5dbvf2qlxgs2r1vji8nd90jfzary9ji1qsdh3") (f (quote (("once_cell" "turf_internals/once_cell"))))))

(define-public crate-turf_macros-0.7.1 (c (n "turf_macros") (v "0.7.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.7.1") (d #t) (k 0)))) (h "1g738nhz8h3kcdbmipig4wzr33k7rl2pg5ys011a3fbwx4rxfjj3") (f (quote (("once_cell" "turf_internals/once_cell"))))))

(define-public crate-turf_macros-0.8.0 (c (n "turf_macros") (v "0.8.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "turf_internals") (r "^0.8.0") (d #t) (k 0)))) (h "1s6f6ahqcp1h78b1c9qzhp6mdcwnrdi53kc4sp233cnbsn5zi02c")))

