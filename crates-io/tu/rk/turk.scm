(define-module (crates-io tu rk turk) #:use-module (crates-io))

(define-public crate-turk-0.1.0 (c (n "turk") (v "0.1.0") (h "0s5f1s94ycga0c2xxl555lw3jfzhjacazvl5mdwyinsa0h0l8k3n")))

(define-public crate-turk-0.2.0 (c (n "turk") (v "0.2.0") (h "1dg41y35sljr5anb26xcx6qq9lg6z97mihbg1cf0abaw20arrj3p")))

(define-public crate-turk-0.3.0 (c (n "turk") (v "0.3.0") (h "1xn0ys5bkvj3sig2nmkm67j3bmrwcvwjkk1a848h0rlfwjxnqmxx")))

