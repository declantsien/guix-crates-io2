(define-module (crates-io tu lb tulb) #:use-module (crates-io))

(define-public crate-tulb-0.11.21 (c (n "tulb") (v "0.11.21") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "1azvmn51mnwp8b42v21jk73jcxvb9iak1iifznsr02vzk7yd8sa2") (y #t)))

