(define-module (crates-io tu io tuio-rs) #:use-module (crates-io))

(define-public crate-tuio-rs-0.1.0 (c (n "tuio-rs") (v "0.1.0") (d (list (d (n "dyn_partial_eq") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.1") (d #t) (k 0)) (d (n "ringbuffer") (r "^0.12.0") (d #t) (k 0)) (d (n "rosc") (r "^0.9.1") (d #t) (k 0)))) (h "0f6wxhgficp2pyphws1sk3lc56hj2s7vqyl1bafwp93xmvqgy2ks")))

(define-public crate-tuio-rs-0.2.0 (c (n "tuio-rs") (v "0.2.0") (d (list (d (n "dyn_partial_eq") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.1") (d #t) (k 0)) (d (n "ringbuffer") (r "^0.12.0") (d #t) (k 0)) (d (n "rosc") (r "^0.9.1") (d #t) (k 0)))) (h "0mvai4kmz4p4bbd1r980sih7dn1dl6k4qm75k56kjp68k218bii4")))

