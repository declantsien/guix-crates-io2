(define-module (crates-io tu nn tunnel) #:use-module (crates-io))

(define-public crate-tunnel-0.1.0 (c (n "tunnel") (v "0.1.0") (d (list (d (n "openssl") (r "^0.10.16") (f (quote ("vendored"))) (d #t) (k 0)))) (h "01y2qhr72bhk1bnwb0npbn504m38pc0b4fi00dqss6zifgwll0n5")))

(define-public crate-tunnel-0.2.0 (c (n "tunnel") (v "0.2.0") (d (list (d (n "openssl") (r "^0.10.16") (f (quote ("vendored"))) (d #t) (k 0)))) (h "176x4sjvqhffgdl806vkbypl6yz46vdwvcbzvshl3d5svz3667qv")))

