(define-module (crates-io tu nn tunnelto_lib) #:use-module (crates-io))

(define-public crate-tunnelto_lib-0.1.10 (c (n "tunnelto_lib") (v "0.1.10") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hzrhpqd5471gh3isc8sbl04dccmv975p40302qq7g7a90kl5p3r")))

(define-public crate-tunnelto_lib-0.1.14 (c (n "tunnelto_lib") (v "0.1.14") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "1ix78f7w97yv72aafrdca9pib17kckl2m4lxywr5wzsi2qfrmc99")))

(define-public crate-tunnelto_lib-0.1.16 (c (n "tunnelto_lib") (v "0.1.16") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "10a3wxmrkdcahi8lacghs6iakx9n8n2q1r6avv731857ryzylxsz")))

(define-public crate-tunnelto_lib-0.1.17 (c (n "tunnelto_lib") (v "0.1.17") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "118cy5sx16jm7pqids79ml3bna7bqfpkyyrkfj57w01r0adq06ak")))

(define-public crate-tunnelto_lib-0.1.19 (c (n "tunnelto_lib") (v "0.1.19") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)))) (h "0zbamszk4c68l33rxcmj06ir1p50k5igz8854x1wvarjs9d5sbnw")))

