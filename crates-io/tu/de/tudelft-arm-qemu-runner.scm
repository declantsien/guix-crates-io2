(define-module (crates-io tu de tudelft-arm-qemu-runner) #:use-module (crates-io))

(define-public crate-tudelft-arm-qemu-runner-0.1.0 (c (n "tudelft-arm-qemu-runner") (v "0.1.0") (h "0h0m0jpb6hq52rbnkrf4ib39sdxyww980xc1qy1imvr13kym2crh")))

(define-public crate-tudelft-arm-qemu-runner-0.1.1 (c (n "tudelft-arm-qemu-runner") (v "0.1.1") (h "1kkm9k3qnzvz8dvhv8raq61rrw118nl1nn2kh552q37sp1xs8w3x")))

(define-public crate-tudelft-arm-qemu-runner-0.2.0 (c (n "tudelft-arm-qemu-runner") (v "0.2.0") (d (list (d (n "tracing") (r "^0.1.38") (d #t) (k 0)))) (h "1yrghn9bnhb25c9gfv757xa6249bmcsag9vkqwqqr5x396g9kk8l")))

