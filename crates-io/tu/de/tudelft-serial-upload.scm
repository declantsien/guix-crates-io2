(define-module (crates-io tu de tudelft-serial-upload) #:use-module (crates-io))

(define-public crate-tudelft-serial-upload-0.1.0 (c (n "tudelft-serial-upload") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "serial2") (r "^0.1.7") (d #t) (k 0)) (d (n "serial_enumerator") (r "^0.2.5") (d #t) (k 0)))) (h "0xpc2kypg1f1y3hmf64l4jxiyz32falfdqi4k5i61gg1mww9x6c1")))

(define-public crate-tudelft-serial-upload-0.1.1 (c (n "tudelft-serial-upload") (v "0.1.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "serial2") (r "^0.1.7") (d #t) (k 0)) (d (n "serial_enumerator") (r "^0.2.5") (d #t) (k 0)))) (h "1g1xzavnmcrc7cfvdgv41lvcqcb450chhwx047rrwqp0168nsqx0")))

(define-public crate-tudelft-serial-upload-0.1.2 (c (n "tudelft-serial-upload") (v "0.1.2") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "serial2") (r "^0.1") (d #t) (k 0)) (d (n "serial_enumerator") (r "^0.2") (d #t) (k 0)))) (h "1rjz12cwwwigjcqh0yxv8c03ysyzq59gz6n37c5a1jadl6i85kha")))

(define-public crate-tudelft-serial-upload-1.0.0 (c (n "tudelft-serial-upload") (v "1.0.0") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "serial2") (r "^0.1") (d #t) (k 0)) (d (n "serial_enumerator") (r "^0.2") (d #t) (k 0)))) (h "0gz1s12q8v7m0l6nyb6zc34nmdcchwzj4hf5r09xcsdx567wcphi")))

(define-public crate-tudelft-serial-upload-1.0.1 (c (n "tudelft-serial-upload") (v "1.0.1") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "serial2") (r "^0.2.1") (d #t) (k 0)) (d (n "serial_enumerator") (r "^0.2") (d #t) (k 0)))) (h "178brfqlf0420dmbbpklms0a1d8lvh2ziqmwmzr9r9byz96ib6np")))

(define-public crate-tudelft-serial-upload-1.0.1-rc2 (c (n "tudelft-serial-upload") (v "1.0.1-rc2") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "serial2") (r "^0.1.1") (d #t) (k 0)) (d (n "serial_enumerator") (r "^0.2") (d #t) (k 0)))) (h "1inzqqhnbcccl3h13jxiiydjxfv4m88pjygfmkg1dhwf2q6zq3fd")))

(define-public crate-tudelft-serial-upload-1.0.1-rc3 (c (n "tudelft-serial-upload") (v "1.0.1-rc3") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "serial2") (r "^0.1.1") (d #t) (k 0)) (d (n "serial_enumerator") (r "^0.2") (d #t) (k 0)))) (h "103a7l5m7sv15sdr4mnwipyhwh8zjhf742lfjrzq2mmg3x9xhf4n")))

(define-public crate-tudelft-serial-upload-1.1.0 (c (n "tudelft-serial-upload") (v "1.1.0") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "serial2") (r "^0.1.1") (d #t) (k 0)) (d (n "serial_enumerator") (r "^0.2") (d #t) (k 0)))) (h "1r8gc2lwa9r6grpkc93cjgx4714ndv3nrrcparfzmsp77gynifwm")))

(define-public crate-tudelft-serial-upload-2.0.0 (c (n "tudelft-serial-upload") (v "2.0.0") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "serial2") (r "=0.2.16") (d #t) (k 0)) (d (n "serial_enumerator") (r "^0.2") (d #t) (k 0)))) (h "00vk6k51aq6hpx1pj04yiklq7rha6yyvrlhq653lqiz95l0ddsqs")))

