(define-module (crates-io tu de tudelft-nes-ppu) #:use-module (crates-io))

(define-public crate-tudelft-nes-ppu-1.0.0 (c (n "tudelft-nes-ppu") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "165b2xalqq2xkvwiii8xxwfdcwh38rwrp2nx9a09l0nzwm69pgss")))

(define-public crate-tudelft-nes-ppu-1.0.1 (c (n "tudelft-nes-ppu") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "1kblr59g31vr1n7bxac6k6jy2lvpy57rflsim2rcmis7k9k9w5bx")))

(define-public crate-tudelft-nes-ppu-1.0.2 (c (n "tudelft-nes-ppu") (v "1.0.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "1dyb7rav9zn1iflxkr8fazb5hvdvq5cagh3qr5lzf35j6g2b5n5n")))

(define-public crate-tudelft-nes-ppu-1.0.3 (c (n "tudelft-nes-ppu") (v "1.0.3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "13jn6qckzpnk70ii6vb5lkhb6ajb18h522g2fjawxnsq6ylly5s1")))

(define-public crate-tudelft-nes-ppu-1.0.4 (c (n "tudelft-nes-ppu") (v "1.0.4") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "17qgh6fxzhn7sy316x5k2jrg1zdccyzp5xzifp20mq9qwpgbkjyh")))

(define-public crate-tudelft-nes-ppu-2.0.0 (c (n "tudelft-nes-ppu") (v "2.0.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "0c4ngcqzidhsds767rxln0gxc3y7rzkzlgwjav6m3sc76nvv65bs")))

(define-public crate-tudelft-nes-ppu-2.0.1 (c (n "tudelft-nes-ppu") (v "2.0.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "1ld6h0vh4y4gcz4qxs1x5fly6z7ck678p50wya0902clp0kzs2ac") (y #t)))

(define-public crate-tudelft-nes-ppu-2.0.2 (c (n "tudelft-nes-ppu") (v "2.0.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "09hh10jqcak68g0af5mq3x2n8whi889hxj5mnls6gwh7w5rcla55")))

(define-public crate-tudelft-nes-ppu-1.0.5 (c (n "tudelft-nes-ppu") (v "1.0.5") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "168zkgbxs457wqgywqw0q57d8w1c7v8zxxnz62gw27c4fkx0bsnm")))

(define-public crate-tudelft-nes-ppu-1.0.6 (c (n "tudelft-nes-ppu") (v "1.0.6") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "1d07chxgkbhrra6k40yc9wlwfmnlc0jqsjad8c6kwpy4sr9hasrj")))

(define-public crate-tudelft-nes-ppu-2.1.0 (c (n "tudelft-nes-ppu") (v "2.1.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pixels") (r "^0.13.0") (d #t) (k 0)) (d (n "winit") (r "^0.28.6") (d #t) (k 0)))) (h "0x587sy2mb9d3cgv7gy94yyjzvw4w2bji2wn4jncd9f5dzvixqiz")))

