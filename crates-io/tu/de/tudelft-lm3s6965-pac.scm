(define-module (crates-io tu de tudelft-lm3s6965-pac) #:use-module (crates-io))

(define-public crate-tudelft-lm3s6965-pac-0.1.0 (c (n "tudelft-lm3s6965-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "01dsw0fmjy998889wf1l5sh2xv6fv3hxwj3b168lsf2n7b0bqknr") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-tudelft-lm3s6965-pac-0.1.1 (c (n "tudelft-lm3s6965-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "16x27cp8fw0wmfqk27bzdq1b3h0hl5888mjqsjzwh4vf661haqzz") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-tudelft-lm3s6965-pac-0.1.2 (c (n "tudelft-lm3s6965-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0fnajmajm631bix2pb6h5fwgf1427hb0wx6d524cr15c0n9lqng2") (f (quote (("rt" "cortex-m-rt/device"))))))

