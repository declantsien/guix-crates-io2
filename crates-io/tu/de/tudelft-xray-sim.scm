(define-module (crates-io tu de tudelft-xray-sim) #:use-module (crates-io))

(define-public crate-tudelft-xray-sim-1.0.0 (c (n "tudelft-xray-sim") (v "1.0.0") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1js33p764ha3xf8lg6jd5gdjcmgnfj8pxrg00b7fzfyr02f44liz")))

