(define-module (crates-io tu de tudelft-nes-test) #:use-module (crates-io))

(define-public crate-tudelft-nes-test-0.1.0 (c (n "tudelft-nes-test") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tudelft-nes-ppu") (r "^1.0.3") (d #t) (k 0)))) (h "07slr67y5f893lsl27j7p8lhnzwwwkqa09dv6fkyqs95k30011fx")))

(define-public crate-tudelft-nes-test-0.1.1 (c (n "tudelft-nes-test") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tudelft-nes-ppu") (r "^1.0.3") (d #t) (k 0)))) (h "1vmzx72608vnhg6h6slaxmg0r49hk12iq0gjqfkhbs7q3acfmxiw")))

(define-public crate-tudelft-nes-test-1.0.0 (c (n "tudelft-nes-test") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tudelft-nes-ppu") (r "^1.0.4") (d #t) (k 0)))) (h "07s3r8ayyx7fjp2lxaqczp68i6nzdrndpw0yzq9912gqg65n8ydw")))

(define-public crate-tudelft-nes-test-1.1.0 (c (n "tudelft-nes-test") (v "1.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tudelft-nes-ppu") (r "^1.0.4") (d #t) (k 0)))) (h "16jzkms9hk2r545kwqnncnvffcp3wnksrb124sxcfifl5h161s61")))

(define-public crate-tudelft-nes-test-1.1.1 (c (n "tudelft-nes-test") (v "1.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tudelft-nes-ppu") (r "^1.0.4") (d #t) (k 0)))) (h "15m2310gg6inr6x5q5gsab18y7ij8ny8b4zr3h6fbwvqyija7lvs")))

(define-public crate-tudelft-nes-test-1.1.2 (c (n "tudelft-nes-test") (v "1.1.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tudelft-nes-ppu") (r "^1.0.4") (d #t) (k 0)))) (h "1ws3sxmlmybrd5vsd9yhsn2np98lpwwir6fsm2vwcw96ikjwfsgj")))

(define-public crate-tudelft-nes-test-1.1.3 (c (n "tudelft-nes-test") (v "1.1.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tudelft-nes-ppu") (r "^1.0.4") (d #t) (k 0)))) (h "0x1c9in5acsimxwzh6h53qlll4pk4arzv82m5dnwfv268b3bza0r")))

(define-public crate-tudelft-nes-test-1.1.4 (c (n "tudelft-nes-test") (v "1.1.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tudelft-nes-ppu") (r "^1.0.4") (d #t) (k 0)))) (h "1gglgjlvhdkc6i06v4mjqcmzc1h1zbal77xpli2gw8061mx1y2ij")))

(define-public crate-tudelft-nes-test-1.1.5 (c (n "tudelft-nes-test") (v "1.1.5") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tudelft-nes-ppu") (r "^1.0") (d #t) (k 0)))) (h "1ygpjcdbxds3bgj8mb9j846flbpbsc130wvha0v3i6rvf2cqmgly")))

(define-public crate-tudelft-nes-test-2.0.0 (c (n "tudelft-nes-test") (v "2.0.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tudelft-nes-ppu") (r "^2.0.0") (d #t) (k 0)))) (h "16lx353bbdnzr0bivn5m6kai856ifvmggmhx84csxzqq8wi6nhwq")))

(define-public crate-tudelft-nes-test-2.1.0 (c (n "tudelft-nes-test") (v "2.1.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tudelft-nes-ppu") (r "^2") (d #t) (k 0)))) (h "16hh0jysxipx8fnd03v0c52gfzwp697913bi1n3ahz4g139bp70z")))

