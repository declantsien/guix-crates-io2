(define-module (crates-io tu nm tunm_proto) #:use-module (crates-io))

(define-public crate-tunm_proto-0.1.9 (c (n "tunm_proto") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0slsx206kffcacg5g5la9ld9l4brnf1x9vd7qca7zl11l3j45sn3")))

(define-public crate-tunm_proto-0.1.10 (c (n "tunm_proto") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "19bm3kb0l94ck1wvlqf6vs38hfp71gffcwrjvv0ik0wji54xpj8z")))

(define-public crate-tunm_proto-0.1.11 (c (n "tunm_proto") (v "0.1.11") (d (list (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0yxzrqa8m6jslwiw1n85xldikxvmja1q3g8i7x8ihncr8q06rfyj")))

(define-public crate-tunm_proto-0.1.12 (c (n "tunm_proto") (v "0.1.12") (d (list (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "1f00vxxjal36b62kixyj8y2qbl63w6rh4ang2qi0l8n3rzjm7nn8")))

(define-public crate-tunm_proto-0.1.13 (c (n "tunm_proto") (v "0.1.13") (h "03krz9l4aa8p5j0iis8hbg6sz9ljm93hrwzn7w2pb39rgrbyzgxl")))

(define-public crate-tunm_proto-0.1.14 (c (n "tunm_proto") (v "0.1.14") (h "1f7xq6v1skqv547zcvag0hj4d3kg6m9azlpb8i8x0c8zz37a3jwz")))

(define-public crate-tunm_proto-0.1.15 (c (n "tunm_proto") (v "0.1.15") (h "033m7jyfni0nfy6wyb5y6ryixrjrz5j765ysrzjhi4777bcvqs1p")))

(define-public crate-tunm_proto-0.1.16 (c (n "tunm_proto") (v "0.1.16") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "14zi9y450skrz6bm2w9ww949s761gqi63zi43yfqgmgfmrbg4fw1")))

(define-public crate-tunm_proto-0.1.17 (c (n "tunm_proto") (v "0.1.17") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "17mbkkrmlv3n9d90mvrsia5yg0r68yxm8kr0kp9l9l5qb58ih05j")))

(define-public crate-tunm_proto-0.1.18 (c (n "tunm_proto") (v "0.1.18") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0b4w4i1n0jq8wdkdw7ajn1m2723874wfravbxcymj81y6n5k8yjz")))

(define-public crate-tunm_proto-0.1.19 (c (n "tunm_proto") (v "0.1.19") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "1lsn2adcaisjifr47phil7hk8dkjizl4511gzvik71b9r8w2bfmi")))

