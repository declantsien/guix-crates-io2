(define-module (crates-io tu nm tunm_timer) #:use-module (crates-io))

(define-public crate-tunm_timer-0.1.0 (c (n "tunm_timer") (v "0.1.0") (d (list (d (n "rbtree") (r "^0.1") (d #t) (k 0)))) (h "0ifg4z9ik58a2sh233m62rzd4627x52aqm54id23xpzczzk7f1aj")))

(define-public crate-tunm_timer-0.1.1 (c (n "tunm_timer") (v "0.1.1") (d (list (d (n "rbtree") (r "^0.1") (d #t) (k 0)))) (h "1rid32xjq151fg5brv2236si1kv5d6gj8hmbklydkr6jm2m3pnch")))

(define-public crate-tunm_timer-0.1.2 (c (n "tunm_timer") (v "0.1.2") (d (list (d (n "rbtree") (r "^0.1") (d #t) (k 0)))) (h "1201w58prdwv1m83i0x2xx5syxfn00amrwx9398xk2qg3zrmij1w")))

(define-public crate-tunm_timer-0.1.3 (c (n "tunm_timer") (v "0.1.3") (d (list (d (n "rbtree") (r "^0.1") (d #t) (k 0)))) (h "0ks2hv8fw0nkh34x08a0c5iyjpa0jr04z4y54i5cdqy4rmi04my4")))

(define-public crate-tunm_timer-0.1.4 (c (n "tunm_timer") (v "0.1.4") (d (list (d (n "rbtree") (r "^0.1") (d #t) (k 0)))) (h "1fncm1kmb0m4kqm5acn126d49i5m7nscs76x1g11rd0hq9clkw11")))

