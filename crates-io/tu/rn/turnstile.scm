(define-module (crates-io tu rn turnstile) #:use-module (crates-io))

(define-public crate-turnstile-1.0.2 (c (n "turnstile") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.7") (d #t) (k 0)))) (h "166v3rkm82bsq9jbr66jxy9hbm4hz486rd1qccb62n9pxsfrly8s")))

(define-public crate-turnstile-1.0.4 (c (n "turnstile") (v "1.0.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.7") (d #t) (k 0)))) (h "08567q1qhw48dnw1jddchf2d2n07gdh2mya9hlzw1k4xilckzwxm")))

(define-public crate-turnstile-1.0.6 (c (n "turnstile") (v "1.0.6") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.7") (d #t) (k 0)))) (h "0r3fjaqi9by7803gx0bd9v55383zzfj7sa6kvbal4jj0pj62wc6i")))

(define-public crate-turnstile-1.0.8 (c (n "turnstile") (v "1.0.8") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.7") (d #t) (k 0)))) (h "1v11v1ddl50caaqfv2hfgwcj9g7vv8q0ylw9ayqp76m8na97hb0z")))

(define-public crate-turnstile-1.0.10 (c (n "turnstile") (v "1.0.10") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.7") (d #t) (k 0)))) (h "1xgdpydfnpnm8lk9fkkzgr0zdpif11l4k1md34r45fq2k6jwmjlr")))

(define-public crate-turnstile-1.0.12 (c (n "turnstile") (v "1.0.12") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.7") (d #t) (k 0)))) (h "1wafhf860bjiymm3fxr4dy6sfjajah5fb3si1rjal2xjlm2zvmcb")))

