(define-module (crates-io tu rn turnstiles) #:use-module (crates-io))

(define-public crate-turnstiles-0.1.1 (c (n "turnstiles") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.1.0") (d #t) (k 2)))) (h "14z6s6426jhwjkcf35fz5fbgh6rw478p6g99ibmw0nc9qfrw423x")))

(define-public crate-turnstiles-0.1.2 (c (n "turnstiles") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.1.0") (d #t) (k 2)))) (h "1j9jq4qp4axzjg1n1fmk7aq0rm26h8r5d5z2a5x2h5ysrmvpwd79")))

(define-public crate-turnstiles-0.1.3 (c (n "turnstiles") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.1.0") (d #t) (k 2)))) (h "07m37cv3wwkcqx99fc8iyf68igww2q6paciv7k267jcdqgmwfc4f")))

(define-public crate-turnstiles-0.2.0 (c (n "turnstiles") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.1.0") (d #t) (k 2)))) (h "1qdh605grdklyy13wvml8r1rlylhj08lc62hj372h8mrfrj15a9k")))

(define-public crate-turnstiles-0.2.1 (c (n "turnstiles") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.1.0") (d #t) (k 2)))) (h "12d135b9ynq7mm7jqyvmxvjfqr525c8rm1909bqm65b2zl6nwc8y")))

(define-public crate-turnstiles-0.4.0 (c (n "turnstiles") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "slog") (r "^2.7.0") (f (quote ("release_max_level_debug"))) (d #t) (k 2)) (d (n "slog-async") (r "^2.7.0") (d #t) (k 2)) (d (n "slog-json") (r "^2.4.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.1.0") (d #t) (k 2)))) (h "0ipdvxp729i1x7af5ss57w2awjsqq7dlv0936x8nkirq3qwnk0kh")))

(define-public crate-turnstiles-0.4.1 (c (n "turnstiles") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "slog") (r "^2.7.0") (f (quote ("release_max_level_debug"))) (d #t) (k 2)) (d (n "slog-async") (r "^2.7.0") (d #t) (k 2)) (d (n "slog-json") (r "^2.4.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.1.0") (d #t) (k 2)))) (h "1h2shfdyibyzqvciczm8wz9hqahxr55l9fnwmcb8qfpzah59wpba")))

