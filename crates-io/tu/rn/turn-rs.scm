(define-module (crates-io tu rn turn-rs) #:use-module (crates-io))

(define-public crate-turn-rs-1.0.1 (c (n "turn-rs") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "faster-stun") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time"))) (d #t) (k 0)))) (h "14kxlygmbhn5mjvfm7jhp9igvy93xsrfmxay7jf06802nsrhmgwr")))

(define-public crate-turn-rs-1.1.0 (c (n "turn-rs") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "faster-stun") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time"))) (d #t) (k 0)))) (h "00f02f8szipm40p9ayjaf4cry50q6c1km0zv0s9c3ncvmskbgc4x")))

(define-public crate-turn-rs-1.2.1 (c (n "turn-rs") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "faster-stun") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1i9dbz7hwih75vwpcbld6dpymci62jqnm0n5kvq3bln8zi8f4dkg")))

(define-public crate-turn-rs-1.2.2 (c (n "turn-rs") (v "1.2.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "faster-stun") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "13py24wg2hm4x1l1cy0r6yf0sp2c6qkvfbf4kbcyrb8a6whvnlmg")))

