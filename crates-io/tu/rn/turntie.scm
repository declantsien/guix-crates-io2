(define-module (crates-io tu rn turntie) #:use-module (crates-io))

(define-public crate-turntie-0.1.0 (c (n "turntie") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("net" "sync" "macros"))) (d #t) (k 0)) (d (n "turnclient") (r "^0.5.0") (d #t) (k 0)))) (h "0xqyjzmp6xd394vh5bja8yqalbncj4ywwp27i76ygb20r7vzab9h")))

