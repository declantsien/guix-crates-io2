(define-module (crates-io tu rn turntie-cli) #:use-module (crates-io))

(define-public crate-turntie-cli-0.1.0 (c (n "turntie-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt" "macros" "io-util" "io-std"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("codec"))) (d #t) (k 0)) (d (n "turntie") (r "^0.1.0") (d #t) (k 0)))) (h "1f8p9wqzfhxg0fdbhyidka2i7wlmv9fz69ymvxmqqj62fn98rcnk")))

