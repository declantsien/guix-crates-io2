(define-module (crates-io tu rn turngate) #:use-module (crates-io))

(define-public crate-turngate-0.1.0 (c (n "turngate") (v "0.1.0") (d (list (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0l3i5r4w54sc818xilncrwrymywf5j9fsjps4qs22il29jsfd41q")))

