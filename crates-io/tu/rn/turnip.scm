(define-module (crates-io tu rn turnip) #:use-module (crates-io))

(define-public crate-turnip-0.1.0 (c (n "turnip") (v "0.1.0") (h "0g9b1dfsjsjrjsprqbfqk6l3mcjpig09zsgff7czd0jkrpfh1fsm") (y #t) (r "1.56.0")))

(define-public crate-turnip-1.0.0 (c (n "turnip") (v "1.0.0") (h "1gpxf6nnzdmb4prkj4pglvi822vcdavrpmza8260ivk303pppw3c") (y #t) (r "1.56.0")))

(define-public crate-turnip-1.0.1 (c (n "turnip") (v "1.0.1") (h "0zrcycqc8m6gimpx99lwqdvbryfqx9il2zfkbvpsqz2y6vla2265") (y #t) (r "1.56.0")))

(define-public crate-turnip-1.0.2 (c (n "turnip") (v "1.0.2") (h "0wvm7pj8x257k96a8p128k5gk29ljvv537brywf1wliz4r68dkqf") (y #t) (r "1.56.0")))

(define-public crate-turnip-1.1.0 (c (n "turnip") (v "1.1.0") (h "0830nq8052yirqdvng4sag80gijycy3badswc1j6r17gc0r12c4q") (y #t) (r "1.56.0")))

(define-public crate-turnip-1.1.1 (c (n "turnip") (v "1.1.1") (h "0wn782f75hml4wi8hijbcmj94h67i2zi22g63xn4brvsdsbqys47") (y #t) (r "1.56.0")))

(define-public crate-turnip-1.1.2 (c (n "turnip") (v "1.1.2") (h "16b4cnlc8l7rqn5yrp4dk8csrplrsg500hmg2b8fmnqf9z8q9y1z") (y #t) (r "1.56.0")))

(define-public crate-turnip-1.1.3 (c (n "turnip") (v "1.1.3") (h "1q2sbzyjfpjfwq63lwryylaz6xwyr9y1wg3dz3sp3p2xv9ifv5gf") (r "1.56.0")))

