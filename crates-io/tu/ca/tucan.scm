(define-module (crates-io tu ca tucan) #:use-module (crates-io))

(define-public crate-tucan-0.1.0 (c (n "tucan") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "siphasher") (r "^0.3.10") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "1yjwfxfqh84nj8s68hw9fwiwr6pxj2wj6zpxx52n9gjw31bh03n8")))

(define-public crate-tucan-0.1.1 (c (n "tucan") (v "0.1.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "twox-hash") (r "^1") (d #t) (k 0)))) (h "09pysyxnbvq0qasc0khn4gbhq0di99d49s92z5hg5mkcybvd6wwj") (y #t)))

(define-public crate-tucan-0.1.2 (c (n "tucan") (v "0.1.2") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "twox-hash") (r "^1") (d #t) (k 0)))) (h "00bxf6gn0wlhfpvp54y799a0pglj2g7dfn8q09lwcd4frqic37m6")))

(define-public crate-tucan-0.1.3 (c (n "tucan") (v "0.1.3") (d (list (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "twox-hash") (r "^1") (d #t) (k 0)))) (h "056wqbwa0fn4frsszxp7f6n7mpfhhcai66pg83khf51c4qq58gsz") (f (quote (("default") ("concurrent" "dashmap" "once_cell"))))))

(define-public crate-tucan-0.1.4 (c (n "tucan") (v "0.1.4") (d (list (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "twox-hash") (r "^1") (d #t) (k 0)))) (h "1629mxs0r6q1xif3arjjz6bafvpsnnrfrv8ba6zi7bghjj5pbcad") (f (quote (("default") ("concurrent" "dashmap" "once_cell"))))))

(define-public crate-tucan-0.1.5 (c (n "tucan") (v "0.1.5") (d (list (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "twox-hash") (r "^1") (d #t) (k 0)))) (h "1z1jq025p6y70wqkqh183w9c79916212sxy5hfgp0nnadkb4xd3q") (f (quote (("default") ("concurrent" "dashmap"))))))

