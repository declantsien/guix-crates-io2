(define-module (crates-io tu ni tuning-library-rs) #:use-module (crates-io))

(define-public crate-tuning-library-rs-0.1.0 (c (n "tuning-library-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0chh5shrpkljrnrp753crxlvvb5vacn7kw16xk5fhwvgafndjpvf")))

