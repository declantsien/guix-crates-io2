(define-module (crates-io tu ni tuning_systems) #:use-module (crates-io))

(define-public crate-tuning_systems-0.1.0 (c (n "tuning_systems") (v "0.1.0") (h "0vb3m3nrwmknx03zf22b18wjmrfk7wg5ai9jgp692j7ijagb4rwd")))

(define-public crate-tuning_systems-0.2.0 (c (n "tuning_systems") (v "0.2.0") (h "0avfszjz0580g3mn5xwl4kdnz95d6rgnyb0r8d6iygl1piflwv0z")))

