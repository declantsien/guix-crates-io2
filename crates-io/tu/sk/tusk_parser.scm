(define-module (crates-io tu sk tusk_parser) #:use-module (crates-io))

(define-public crate-tusk_parser-0.1.0 (c (n "tusk_parser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "1.0.*") (d #t) (k 0)) (d (n "tusk_lexer") (r "^0.4.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0701p0zi2jb9r48cqhw49wcmly3wm2ils9aqh33qazc3a3v0ff1c")))

(define-public crate-tusk_parser-0.1.1 (c (n "tusk_parser") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "1.0.*") (d #t) (k 0)) (d (n "tusk_lexer") (r "^0.4.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "19843n32r6g3p1myqhqx4wifand9bhx84cz7jidp27r5vgxnj3f4")))

(define-public crate-tusk_parser-0.1.2 (c (n "tusk_parser") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "1.0.*") (d #t) (k 0)) (d (n "tusk_lexer") (r "^0.4.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0a89wygyn6s1qhknjasjs6j9x3hhcc5wqrdy7xk0bf2pcih0zgzg")))

