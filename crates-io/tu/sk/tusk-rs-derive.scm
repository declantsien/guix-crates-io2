(define-module (crates-io tu sk tusk-rs-derive) #:use-module (crates-io))

(define-public crate-tusk-rs-derive-0.1.0 (c (n "tusk-rs-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1vszwnbgjqmbsdzammyliy66awbg6gal22rjl5fp672g9kcz3sjz")))

(define-public crate-tusk-rs-derive-0.1.1 (c (n "tusk-rs-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0bg9vphmr7llx6a40n4xz00gc76av7clp6jl5z6sp0n1k494f4si")))

(define-public crate-tusk-rs-derive-0.1.2 (c (n "tusk-rs-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1rb1xkj924jgl692s7s5rxzg6z94d8ax5flk2rnzhxrnj09c5rbz")))

(define-public crate-tusk-rs-derive-0.1.3 (c (n "tusk-rs-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "114vax809vk6qlqk57s9is8455q035lhp4lrydk4x40z45rqj1g8")))

(define-public crate-tusk-rs-derive-0.1.4 (c (n "tusk-rs-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0a5ic9h52z1q5gyzigl7p0v7wsfxqd9rqf71bzz8faxqb52mablr")))

(define-public crate-tusk-rs-derive-0.1.5 (c (n "tusk-rs-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "142v8hl852yv8ib2f7213zcx8jjj4p5nndkii8s0pkc989ahrvgs")))

(define-public crate-tusk-rs-derive-0.1.6 (c (n "tusk-rs-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1w1nvsa02gnfdgsdcdhdfza4kwqc55c0kyam5lyk9a22mzc8nrih")))

(define-public crate-tusk-rs-derive-0.1.7 (c (n "tusk-rs-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1zaikniwn9jgqcj2r2biyzsabzlahvy3hqhndm6948vik2049b76")))

(define-public crate-tusk-rs-derive-0.1.8 (c (n "tusk-rs-derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0a7rwidmff5ly7jmdbdwz0kjgrxdri0nwf66v8w3ysjmfn7dbdbh")))

(define-public crate-tusk-rs-derive-0.1.9 (c (n "tusk-rs-derive") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "08zg85f6b1scydrnl1wmi9yzw278xq2i7p7jyx61pg85q9r7k98q")))

(define-public crate-tusk-rs-derive-0.1.10 (c (n "tusk-rs-derive") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "154yzdppbi48dqgvisv55q3g4345zhf1qd5cf37lgayjyjsgkgb6")))

(define-public crate-tusk-rs-derive-0.1.11 (c (n "tusk-rs-derive") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1c15rzl2092cp9y4nqin9k0fqc5ni3d7hkd5nl7l81cxsss84l4r")))

(define-public crate-tusk-rs-derive-0.1.12 (c (n "tusk-rs-derive") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0qylvz1jkz3gvvmxspf8mmkxwkyqnb18h8wpijhxlh9k4cbflg6m")))

(define-public crate-tusk-rs-derive-0.1.13 (c (n "tusk-rs-derive") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1lzzmb5jljiwnl5nj40xzyp3lz2vyxgp8rcqz4gayal62hhdwh7g")))

(define-public crate-tusk-rs-derive-0.1.14 (c (n "tusk-rs-derive") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "19bwcxhzxw2w35vnmk1bjz5sh4ymmlghx6fdcv9qml644dww6m61")))

(define-public crate-tusk-rs-derive-0.1.15 (c (n "tusk-rs-derive") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0a78gqqzl9422wk6ai1xa4g2kbdfv8zlj5ljrapvy9rfls395f6w")))

(define-public crate-tusk-rs-derive-0.2.0 (c (n "tusk-rs-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0289i5jddk9xyd16nj8lxbfy9sv6ypg0bipxfg65jqbbs6xr5zyx")))

(define-public crate-tusk-rs-derive-0.2.1 (c (n "tusk-rs-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "05z8zwdnx4lyl3f31rx7dpjj9y7sy5rgrb4scsxbw8iqdjpk4np0")))

(define-public crate-tusk-rs-derive-0.2.2 (c (n "tusk-rs-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "07mhl8jcddirasw56agswryz1dycmw7r7mfda34j9405g6y61m3y")))

(define-public crate-tusk-rs-derive-0.2.3 (c (n "tusk-rs-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0zfrsxrc146vm12wphfdq0ab8ya2apx1fhfr51vj8sh386b3zbwk")))

(define-public crate-tusk-rs-derive-0.2.4 (c (n "tusk-rs-derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1wq0vq7xqvvi3b79if3r1bssdk9dv9dr6f99jh8ga1jjzgg13l9n")))

(define-public crate-tusk-rs-derive-0.2.5 (c (n "tusk-rs-derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0i0pss1bf6a70134na19227pxg3rn7icny0xb35a660galy68sj3")))

(define-public crate-tusk-rs-derive-0.2.6 (c (n "tusk-rs-derive") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "01vhdgz5wih4zgsnl88dsjh30g6siypbsbf778kydvcqpksvl8pz")))

(define-public crate-tusk-rs-derive-0.2.7 (c (n "tusk-rs-derive") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1x4hn0szz3aqaf9l2j539dzjj3i5ryja1ibf5cyxm7as73i8n7jw")))

(define-public crate-tusk-rs-derive-0.2.8 (c (n "tusk-rs-derive") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "01v9h144fyksvj4lky4a79gcf67zws9dlx5w1j1an2qwaqwp1qrb")))

(define-public crate-tusk-rs-derive-0.3.0 (c (n "tusk-rs-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0ffxya9z6626rxafk1w6cqxh2ca3ijkcs781n4bqqd1jq9nr9m0h")))

(define-public crate-tusk-rs-derive-0.3.1 (c (n "tusk-rs-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1ysbf767zz50957972lh512zm6f79sivjcxhggf8l8rfgj6a2yh6")))

(define-public crate-tusk-rs-derive-0.3.2 (c (n "tusk-rs-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1igz9waywrqdvmk3m383iw77qbs47ihsbfgd60w0n7767h43gqc4")))

(define-public crate-tusk-rs-derive-0.3.3 (c (n "tusk-rs-derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1glbb9m97p158k1s5xzhmbxmdrrjgsdyp636mgw0bdaqzyp1ilv0")))

(define-public crate-tusk-rs-derive-0.3.4 (c (n "tusk-rs-derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0j5sndlg91r0dablb15p6kwzsdqclmhcbny5az7a9j8102mzq8s0")))

(define-public crate-tusk-rs-derive-0.4.0 (c (n "tusk-rs-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1gyds7z7fwlbvqgjrb6wp0h8q87ryhkkv7fgc6xvjwy5iacivd9i")))

(define-public crate-tusk-rs-derive-0.5.0 (c (n "tusk-rs-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "11492kgp73m1s09xillcfg6a40zb76wvrp752ym5107j2qv6ryq3")))

(define-public crate-tusk-rs-derive-0.6.0 (c (n "tusk-rs-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1p3g4mq8hmlxfr69xkdqxmilmhcxc44wq3zlzv07q6v9mpcf891k")))

(define-public crate-tusk-rs-derive-0.7.0 (c (n "tusk-rs-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0lqsbla464crjphag4cc2jvxfnxd8my0fisib0wq6rbn0hdmgvyd")))

(define-public crate-tusk-rs-derive-0.7.1 (c (n "tusk-rs-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "03q9sd09c8ng8r77j5a44a8hiszz5r88gck3wxka44azxj6j2d87")))

(define-public crate-tusk-rs-derive-0.7.2 (c (n "tusk-rs-derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0gs0vh86qh24nv6zclgww86cw21d9kbls3lqckshmabj2bbb4rd2")))

(define-public crate-tusk-rs-derive-0.7.3 (c (n "tusk-rs-derive") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0y3a7sapi1hl0l8ahvx9d8fna1z2ggmb6zy1rwwm2g67d32lvkcn")))

