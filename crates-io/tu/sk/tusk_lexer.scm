(define-module (crates-io tu sk tusk_lexer) #:use-module (crates-io))

(define-public crate-tusk_lexer-0.1.0 (c (n "tusk_lexer") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0fgqv43rk71n83543112r90g8i0il77q4kpqhqixsl4md529bf6r")))

(define-public crate-tusk_lexer-0.2.0 (c (n "tusk_lexer") (v "0.2.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0cp3w0zpf62rh6x1822pfhkww8awzv5p7ghwdyjcl3cmvf140nzy")))

(define-public crate-tusk_lexer-0.2.1 (c (n "tusk_lexer") (v "0.2.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1ab9hwri62m0kbzhsy2y4ylrr342ygnr17slw3cbxc485iib6flz")))

(define-public crate-tusk_lexer-0.2.2 (c (n "tusk_lexer") (v "0.2.2") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1mjxsbhbbsfgps4b0jg87av1brjcvhn5rpsr31vmx30x1d5l3sya")))

(define-public crate-tusk_lexer-0.2.3 (c (n "tusk_lexer") (v "0.2.3") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1wv54i8rq79danycrvr64zxj70w3yhd68xma9s76f8j4vgncak6m")))

(define-public crate-tusk_lexer-0.2.4 (c (n "tusk_lexer") (v "0.2.4") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0fanbhy5k45r95xmkw141b3qsa0920f69zhlc7419jdlwdly5a0j")))

(define-public crate-tusk_lexer-0.2.5 (c (n "tusk_lexer") (v "0.2.5") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1yn1dq3mb2a5inbjcc5m3p4gmdgp7w5cjygkbc8572fs4lbk23m8")))

(define-public crate-tusk_lexer-0.2.6 (c (n "tusk_lexer") (v "0.2.6") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1xb1nkxn5h803hlchvdwg2whxpjqcmkhvrr6jixl8hn8arbljhbk")))

(define-public crate-tusk_lexer-0.3.0 (c (n "tusk_lexer") (v "0.3.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0friqjkfr0alb4chh8rqqp1p7vccqy6yxyrjwwwpkwmbvj92y1gl")))

(define-public crate-tusk_lexer-0.3.1 (c (n "tusk_lexer") (v "0.3.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "10qjm0c0qbzda02fwgmcjsb32nf38caca4ldd26ii7qjc2j7pfj1")))

(define-public crate-tusk_lexer-0.3.2 (c (n "tusk_lexer") (v "0.3.2") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0nwz0dmw7fcfx5g4mplrlqa5pmwarwmizlnsdk984d0f5lxq98fl")))

(define-public crate-tusk_lexer-0.3.3 (c (n "tusk_lexer") (v "0.3.3") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1z13b3rn33ys5mxch54f3psmqnwjym6pjwd8h7hslrk33z1i1mak")))

(define-public crate-tusk_lexer-0.3.4 (c (n "tusk_lexer") (v "0.3.4") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1jkrbc0m2x4cq6lsx0nxgb42g5m4kpzk6fl833wpg4a2xfl55yfy")))

(define-public crate-tusk_lexer-0.4.0 (c (n "tusk_lexer") (v "0.4.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "187qh3nd81qm6wdza20jz0m9r8bij9myph38sifnzl7wig7951jr")))

(define-public crate-tusk_lexer-0.4.1 (c (n "tusk_lexer") (v "0.4.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1zg5137hc4ff09jaq1589z87k5bsaqnc4p58lzq8vivmnyg47660")))

(define-public crate-tusk_lexer-0.4.2 (c (n "tusk_lexer") (v "0.4.2") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0m3y9vm65957y2jfyz39gfs9ifzma1bg5rg4z9vavr5bdcfr6r8g")))

(define-public crate-tusk_lexer-0.4.3 (c (n "tusk_lexer") (v "0.4.3") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0f5h199nq4z90lg6rmvck9q1wlgbxlr0gl42pnacd0i3g2krkjrk")))

(define-public crate-tusk_lexer-0.4.4 (c (n "tusk_lexer") (v "0.4.4") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0piipksf1gc6h49armpr7g66171wszlpalcqvgfxhzl5pg855p64")))

(define-public crate-tusk_lexer-0.4.5 (c (n "tusk_lexer") (v "0.4.5") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1001mz9bm8gdnwbchf0jdmdb5km9yl57rlmmm5mqfpv83amfip28")))

(define-public crate-tusk_lexer-0.4.6 (c (n "tusk_lexer") (v "0.4.6") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0mljqz0cyr56zpmkmwrznjl69r17pzsslc2w5zl4zf6i2n186wip")))

(define-public crate-tusk_lexer-0.4.7 (c (n "tusk_lexer") (v "0.4.7") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0i973a7fqgighc8zl131ajncyq449vfxqihag9xjwlanw42vak6i")))

