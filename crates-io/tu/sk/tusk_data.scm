(define-module (crates-io tu sk tusk_data) #:use-module (crates-io))

(define-public crate-tusk_data-0.0.1 (c (n "tusk_data") (v "0.0.1") (d (list (d (n "chrono") (r "^0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "criterion") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 2)) (d (n "uuid") (r "^0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1mgvdv0bls1nm2q2bcnl4w7fr9fgb4blg892hn4vc1r3lc15ly6c")))

