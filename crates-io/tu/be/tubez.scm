(define-module (crates-io tu be tubez) #:use-module (crates-io))

(define-public crate-tubez-0.0.1 (c (n "tubez") (v "0.0.1") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "hyper") (r "^0.14.18") (f (quote ("http2" "tcp"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "simple_logger") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0lp9zs269q5brjfhipqw7hqfqqj8l001kh3bsh956h6y1g66mmp7") (f (quote (("server" "hyper/server") ("client" "hyper/client"))))))

