(define-module (crates-io tu mb tumbleweed_derive) #:use-module (crates-io))

(define-public crate-tumbleweed_derive-0.1.0 (c (n "tumbleweed_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m083b39a2py5npqb4kmrc52gqb4nq0wq8xhdn8a51q3v43ixaqx")))

