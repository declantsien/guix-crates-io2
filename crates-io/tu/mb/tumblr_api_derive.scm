(define-module (crates-io tu mb tumblr_api_derive) #:use-module (crates-io))

(define-public crate-tumblr_api_derive-0.1.0 (c (n "tumblr_api_derive") (v "0.1.0") (d (list (d (n "darling") (r "~0.20.2") (f (quote ("suggestions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "196w8shdlgs8jchk4rmn6grhc9l75sv2bwnavbqz9mqdp75jvxsx")))

