(define-module (crates-io tu mb tumblr) #:use-module (crates-io))

(define-public crate-tumblr-0.1.0 (c (n "tumblr") (v "0.1.0") (d (list (d (n "oauth-client") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kzgvc8pdkfjmia769x02xjbva8w7yzwcy6hqrahzh3c2qbh24ll")))

