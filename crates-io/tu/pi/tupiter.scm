(define-module (crates-io tu pi tupiter) #:use-module (crates-io))

(define-public crate-tupiter-0.1.0 (c (n "tupiter") (v "0.1.0") (d (list (d (n "tupiter-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0jy0lfhzblhjy6x5j0yvbkwrnajmajlrz1vqfb5pw7zqsai1vw5b")))

(define-public crate-tupiter-0.1.1 (c (n "tupiter") (v "0.1.1") (d (list (d (n "tupiter-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "05jj6smj56fih52fv4awbkwq956b8a1yi3nvc6dhanl39jy0pvr6")))

