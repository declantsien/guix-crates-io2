(define-module (crates-io tu xt tuxtrain) #:use-module (crates-io))

(define-public crate-tuxtrain-0.0.1 (c (n "tuxtrain") (v "0.0.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "procfs") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1y4jpi80k494k59p9rrslm1ii48pfp3zgz08dlgj4snxhg877wq7")))

(define-public crate-tuxtrain-0.0.2 (c (n "tuxtrain") (v "0.0.2") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "procfs") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "18i2i3rwikv4q3n9ygjpyhq36ccqadjqj996aqffnsdih4f6g6gc")))

(define-public crate-tuxtrain-0.0.3 (c (n "tuxtrain") (v "0.0.3") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "procfs") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1xfhz5nkvdl1z33hxqc092iyxjqqhx3if2x0w4pg0pzm36zw4f4y")))

(define-public crate-tuxtrain-0.0.4 (c (n "tuxtrain") (v "0.0.4") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "procfs") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1gwlfpc3dzls9hpms2kipljq6aldscnvrxh1mr9hm61qj00vn56n")))

