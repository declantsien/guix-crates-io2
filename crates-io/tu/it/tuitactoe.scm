(define-module (crates-io tu it tuitactoe) #:use-module (crates-io))

(define-public crate-tuitactoe-0.1.0 (c (n "tuitactoe") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tui") (r "^0.15") (f (quote ("crossterm"))) (k 0)))) (h "1m31z6kc6j9n55ikzp8d09l3881gamkzljdywfp738p7lphcmlv9")))

(define-public crate-tuitactoe-0.1.1 (c (n "tuitactoe") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tui") (r "^0.15") (f (quote ("crossterm"))) (k 0)))) (h "1fh1877dkcd1ngcfdjqkx4z0m2bvmjs8s218sd0b5wzlpw0qcj94")))

(define-public crate-tuitactoe-0.1.2 (c (n "tuitactoe") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tui") (r "^0.15") (f (quote ("crossterm"))) (k 0)))) (h "0mxx0ql47i8xndkp4sd13rmyhd8wpy907bajqi5h91zxdlz43v15") (y #t)))

