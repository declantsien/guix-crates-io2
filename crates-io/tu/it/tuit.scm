(define-module (crates-io tu it tuit) #:use-module (crates-io))

(define-public crate-tuit-0.0.0 (c (n "tuit") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0) (p "thiserror-core")))) (h "0kr7bc04n5qcw0x810qnk55m5lmg86vnbc5fp99izf0v4kgycci4") (f (quote (("widgets") ("std" "alloc") ("default" "widgets") ("ansi_terminal" "std") ("alloc"))))))

