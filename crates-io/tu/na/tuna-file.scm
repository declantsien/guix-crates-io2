(define-module (crates-io tu na tuna-file) #:use-module (crates-io))

(define-public crate-tuna-file-0.1.0 (c (n "tuna-file") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "tuna") (r "^0.1") (d #t) (k 0)))) (h "02by3ycir1095p3dvawyylxm9a27w6hrv3l4a6psnxhjzdqb1rb3")))

