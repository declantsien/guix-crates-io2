(define-module (crates-io tu na tuna-web) #:use-module (crates-io))

(define-public crate-tuna-web-0.0.1 (c (n "tuna-web") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.25") (d #t) (k 0)) (d (n "tiny_http") (r "^0.8.2") (d #t) (k 0)) (d (n "tuna") (r "^0.0.2") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)))) (h "0zyk311q86ilmzkh5qv398yi1a4glmphp8szg2yziy4wpcbqfvha")))

(define-public crate-tuna-web-0.0.2 (c (n "tuna-web") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.25") (d #t) (k 0)) (d (n "tiny_http") (r "^0.8.2") (d #t) (k 0)) (d (n "tuna") (r "^0.0.3") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)))) (h "0ixghflqgn226x8za80ih7ixj3wlb7swgbirp4jvw7iaqpxxq4lg")))

