(define-module (crates-io tu na tuna) #:use-module (crates-io))

(define-public crate-tuna-0.0.1 (c (n "tuna") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.25") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "19r06ckbs0ybrfifdxqysaxw02xgq39bn6swmhwl0j9qij3v05m0") (f (quote (("wasm-bindgen" "parking_lot/wasm-bindgen") ("default") ("auto-register"))))))

(define-public crate-tuna-0.0.2 (c (n "tuna") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.25") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "1g5d1jw6z1l316zjq800b2n4bzbx7agbay3hpgwa6d99cv7kvims") (f (quote (("wasm-bindgen" "parking_lot/wasm-bindgen") ("default") ("auto-register"))))))

(define-public crate-tuna-0.0.3 (c (n "tuna") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.25") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "1cg9kflzdik4a0m0bycazxl4kl9q4yyiamzc246ny741mcy8k4cf") (f (quote (("wasm-bindgen" "parking_lot/wasm-bindgen") ("default"))))))

(define-public crate-tuna-0.1.0 (c (n "tuna") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.25") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tuna-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1cbi1kr4xk5c521dd72xjwvz4pz5y61idhbf273gnzly42aayyih") (f (quote (("wasm-bindgen" "parking_lot/wasm-bindgen") ("default"))))))

