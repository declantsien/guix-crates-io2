(define-module (crates-io tu na tunapanel) #:use-module (crates-io))

(define-public crate-tunapanel-0.1.0 (c (n "tunapanel") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "handlebars") (r "^0.26") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ig5c9xgj6dxpsbwlw0sszbjkmylhyz5ly8ida3cb7yh1dnnkla0")))

(define-public crate-tunapanel-0.1.1 (c (n "tunapanel") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "handlebars") (r "^0.26") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "10ggby96wjgfkbd95dcffrq5cfz5sl09l62m4pagw1vnglgab2ki")))

