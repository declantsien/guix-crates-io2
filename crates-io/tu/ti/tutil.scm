(define-module (crates-io tu ti tutil) #:use-module (crates-io))

(define-public crate-tutil-0.1.0 (c (n "tutil") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.54") (o #t) (d #t) (k 0)))) (h "0idgjp5xf8glwj2x2vnyyy4i0g3xa37r0xkckya4kq4k5yjs2x8b") (f (quote (("lints" "clippy") ("default")))) (y #t)))

(define-public crate-tutil-0.1.1 (c (n "tutil") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.54") (o #t) (d #t) (k 0)))) (h "0a2ll8cny9dy7pn1kq3hyq278v3fpvyjawsmx6crszc6xvd03z1k") (f (quote (("lints" "clippy") ("default")))) (y #t)))

(define-public crate-tutil-0.2.0 (c (n "tutil") (v "0.2.0") (d (list (d (n "clippy") (r "~0.0") (o #t) (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1fcl9mnvsrl2gik3b4xgh1iv61878a77zfm36nni9vxwia35mdw8") (f (quote (("lints" "clippy") ("default")))) (y #t)))

