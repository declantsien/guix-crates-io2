(define-module (crates-io tu li tulip-arrform) #:use-module (crates-io))

(define-public crate-tulip-arrform-0.1.1 (c (n "tulip-arrform") (v "0.1.1") (h "1l3p8pyyi7g1miqz9p8b27fwazjmnbma601blgh169p8iw6cgx5n")))

(define-public crate-tulip-arrform-0.1.2 (c (n "tulip-arrform") (v "0.1.2") (h "1l35c8mpmzw8yxrff1fbm5hbrqk8n8wpc0l1kkiylq4is1wvrs96")))

