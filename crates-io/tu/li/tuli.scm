(define-module (crates-io tu li tuli) #:use-module (crates-io))

(define-public crate-tuli-0.1.0 (c (n "tuli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)))) (h "1d92f2kr697790bik2gmwsmmghzij4lyr57nv0jw9gr7zglkk67n")))

(define-public crate-tuli-0.1.1 (c (n "tuli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)))) (h "0arxn4h8gbw7ijjbgrgzxqiz3r33wfijriqirihg0rfb2lyb6q1n")))

