(define-module (crates-io tu li tulip-sled-util) #:use-module (crates-io))

(define-public crate-tulip-sled-util-0.1.0 (c (n "tulip-sled-util") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "1paw1cs7c4wn43nz4dxi5mdw381ih90b0si3ns50yz6bc6v7v173")))

(define-public crate-tulip-sled-util-0.1.1 (c (n "tulip-sled-util") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "11h2cpi4jikpphzv8hwam9hwb6d7f7b70qwada6885hi6j3jc6nr")))

(define-public crate-tulip-sled-util-0.1.2 (c (n "tulip-sled-util") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0b6m8m2qvy32fgg0agciwn3sikdv1641kdi47v057jlzlymyc69x")))

(define-public crate-tulip-sled-util-0.1.3 (c (n "tulip-sled-util") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "1xn9fdcmzrrxindc9ycz7852a2bdbzfnkyjbzq5z0kfwgcafm6nq")))

(define-public crate-tulip-sled-util-0.1.4 (c (n "tulip-sled-util") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "11p1lh4ry8508ng7lp1h0idgkc22his4f4x66zqz79pkvf5l5812")))

