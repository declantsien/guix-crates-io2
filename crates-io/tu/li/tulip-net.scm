(define-module (crates-io tu li tulip-net) #:use-module (crates-io))

(define-public crate-tulip-net-0.6.0 (c (n "tulip-net") (v "0.6.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "ureq") (r "^2.7.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0wh8w1hccs6sivmwsqhhj9zw6wiqyqyc0ps0m3yw8862fgmh89j9")))

