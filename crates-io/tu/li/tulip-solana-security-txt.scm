(define-module (crates-io tu li tulip-solana-security-txt) #:use-module (crates-io))

(define-public crate-tulip-solana-security-txt-0.1.0 (c (n "tulip-solana-security-txt") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "twoway") (r "^0.2.2") (d #t) (k 0)))) (h "10g8bi8jj48ddv5gnizfc6v48iv7d1hmbf3ffc10qxa4xl0q9fv1")))

