(define-module (crates-io tu li tulisp-proc-macros) #:use-module (crates-io))

(define-public crate-tulisp-proc-macros-0.1.0 (c (n "tulisp-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "183p2jmgnj0xkrkj7sjhcvkphmqy1iff1d3f6akj5j5hwivrrgkz")))

(define-public crate-tulisp-proc-macros-0.2.0 (c (n "tulisp-proc-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0svkwrpvp6fnwd8yja0a863d8ry0izphck1s63469g6r474b6n9v")))

(define-public crate-tulisp-proc-macros-0.3.0 (c (n "tulisp-proc-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k0d59rf35hvxg9s1sxfr5f9azvqvfaqhd8aim6hkz0hr3dmhlib")))

(define-public crate-tulisp-proc-macros-0.4.0 (c (n "tulisp-proc-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p8rwhjlw8wlm6yvr2h2q5zq28j37chjxsxr1r87nln7gv5dbqps")))

(define-public crate-tulisp-proc-macros-0.4.1 (c (n "tulisp-proc-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11vwpxs3hh7qd2q283zl2rscg0i5wiv69fbh5lbl1gc95pwq85vr")))

