(define-module (crates-io tu li tulip-realms-sdk) #:use-module (crates-io))

(define-public crate-tulip-realms-sdk-0.1.1 (c (n "tulip-realms-sdk") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)) (d (n "solana-client") (r "^1.10.26") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.26") (d #t) (k 0)) (d (n "spl-governance") (r "^2.1.1") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt-multi-thread" "full"))) (d #t) (k 2)) (d (n "tulip-sled-util") (r "^0.1.4") (d #t) (k 0)))) (h "0mql8rl782hj5dcmpdvbmpssray53si9m2y9z29lsbinh5mv2s0a")))

