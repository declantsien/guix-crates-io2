(define-module (crates-io tu li tulipv2-sdk-farms) #:use-module (crates-io))

(define-public crate-tulipv2-sdk-farms-0.9.0 (c (n "tulipv2-sdk-farms") (v "0.9.0") (d (list (d (n "anchor-lang") (r "^0.22.0") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "0sjqwlcvglx428vp6yb4wwp04sl2772ccb05cp51l9sp9fhyggjr")))

(define-public crate-tulipv2-sdk-farms-0.9.2 (c (n "tulipv2-sdk-farms") (v "0.9.2") (d (list (d (n "anchor-lang") (r "^0.22.0") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "1ybv08in8c6wqrcs1z0zgx7qsvhqh1vw1vqxixa4v6g9kggk43p6")))

(define-public crate-tulipv2-sdk-farms-0.9.3 (c (n "tulipv2-sdk-farms") (v "0.9.3") (d (list (d (n "anchor-lang") (r "^0.22.0") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "14jmcpjwh09pmsvk8rkcx1jznj3rs2c7rkdzcrd7yr3hywb7f886")))

(define-public crate-tulipv2-sdk-farms-0.9.4 (c (n "tulipv2-sdk-farms") (v "0.9.4") (d (list (d (n "anchor-lang") (r "^0.22.0") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "0zmbl5hni04nb3x64mpvayss0il8ry9x13jhzn93h88a0j487qds")))

(define-public crate-tulipv2-sdk-farms-0.9.5 (c (n "tulipv2-sdk-farms") (v "0.9.5") (d (list (d (n "anchor-lang") (r "^0.22.0") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "07dmmdqmidc4y3kn7kh99ms61m71lgcxd8rp5qxanyxiraj7jcxd")))

(define-public crate-tulipv2-sdk-farms-0.9.6 (c (n "tulipv2-sdk-farms") (v "0.9.6") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "1wvjc66rpx78k9b2jrpasdrzgyx13c4rnwf7yipkikmvhb02xqjs")))

(define-public crate-tulipv2-sdk-farms-0.9.7 (c (n "tulipv2-sdk-farms") (v "0.9.7") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "0a8wliiy954yhm3f96hi1bki4wcl83h281agljjn3gnsbvlga5my")))

(define-public crate-tulipv2-sdk-farms-0.9.8 (c (n "tulipv2-sdk-farms") (v "0.9.8") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "0vrfrqghlwfa35nw7ph2n5rbj81hv0i3zyc3dc0m5lbwk81pv2jp")))

(define-public crate-tulipv2-sdk-farms-0.9.9 (c (n "tulipv2-sdk-farms") (v "0.9.9") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "10drmdihwf9sdn0pcpkbf8bk2prq3i3asgdng1hh5af0apkhdc1m")))

(define-public crate-tulipv2-sdk-farms-0.9.10 (c (n "tulipv2-sdk-farms") (v "0.9.10") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "0gkp6nhjvp5v1yxih1j0mbfizpq6gc8a69dph7smhkha3ahaqmc9")))

(define-public crate-tulipv2-sdk-farms-0.9.11 (c (n "tulipv2-sdk-farms") (v "0.9.11") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "0qm8pjvmhcppyrz39m47p25a9a26g4vs8snkbpxqb8ww9qkm0ndf")))

(define-public crate-tulipv2-sdk-farms-0.9.12 (c (n "tulipv2-sdk-farms") (v "0.9.12") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "0b1q3kc9ryd4sm0qz8lh5qn5vlcb90dwj45s2hgxy4hhb6wn0fwj")))

(define-public crate-tulipv2-sdk-farms-0.9.15 (c (n "tulipv2-sdk-farms") (v "0.9.15") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "1yjvyljwnkwb3ncpgnbwqmpx5d6rpisz42qa0xzgwc3al4wdiy39")))

(define-public crate-tulipv2-sdk-farms-0.9.16 (c (n "tulipv2-sdk-farms") (v "0.9.16") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "1d0q28ph8wwrkvc2gxd244mngknw379x7cmcnycszi12jqwjmzzd")))

(define-public crate-tulipv2-sdk-farms-0.9.17 (c (n "tulipv2-sdk-farms") (v "0.9.17") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "16xab0w58vqm9zixd0mbcry9q61y1iz6lrwkh167ip3lv3l8ipk0")))

(define-public crate-tulipv2-sdk-farms-0.9.18 (c (n "tulipv2-sdk-farms") (v "0.9.18") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "1mk0n4s89l5al98g3065h4vh12w9n1a284s76mahf6chbf701wh6")))

(define-public crate-tulipv2-sdk-farms-0.9.19 (c (n "tulipv2-sdk-farms") (v "0.9.19") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "0q4xx10brks623fclq8qgbpbr572dm9da6vv5fhh5rf75zcqj2rv")))

(define-public crate-tulipv2-sdk-farms-0.9.21 (c (n "tulipv2-sdk-farms") (v "0.9.21") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "tulip-arrform") (r "^0.1.1") (d #t) (k 0)))) (h "123kh8r908y1g8m7ydx7v3gb7i664ym8i9x54s81qbw9fp1dpwih")))

