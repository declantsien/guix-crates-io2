(define-module (crates-io tu li tulisp) #:use-module (crates-io))

(define-public crate-tulisp-0.2.0 (c (n "tulisp") (v "0.2.0") (d (list (d (n "tulisp-proc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0nnjj22477pn5rj5vlc8ssfhr3ljpkzjfwnmr1z45s340mv2qf31")))

(define-public crate-tulisp-0.3.0 (c (n "tulisp") (v "0.3.0") (d (list (d (n "tulisp-proc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0nz58c5kp3xwiwfmmn5nsssdq4fkn59bf9025p1cxhbklw1s18im")))

(define-public crate-tulisp-0.4.0 (c (n "tulisp") (v "0.4.0") (d (list (d (n "tulisp-proc-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1x9g3kycsbmb3mf1q1miswv96w2kpl4926r7hdbl8d2hi5xpaziy")))

(define-public crate-tulisp-0.5.0 (c (n "tulisp") (v "0.5.0") (d (list (d (n "tulisp-proc-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1yv2avsl35fbnn1g8mxd007y59wr0gxaan3wv099gs2n0xh7x9d3")))

(define-public crate-tulisp-0.6.0 (c (n "tulisp") (v "0.6.0") (d (list (d (n "tulisp-proc-macros") (r "^0.2.0") (d #t) (k 0)))) (h "17am5jn1qfsskjrbhyvbdyxjxbwx5wfpy4gkpm4n7bk94qpdgs3x")))

(define-public crate-tulisp-0.7.0 (c (n "tulisp") (v "0.7.0") (d (list (d (n "tulisp-proc-macros") (r "^0.2.0") (d #t) (k 0)))) (h "057d8p58ljq96glrx7lzj35lmbr408v9wjza37ah7hqc4vqhybsf")))

(define-public crate-tulisp-0.8.0 (c (n "tulisp") (v "0.8.0") (d (list (d (n "tulisp-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "06n3cqln3i2a8ip2ycwlwv67m2i2y913z2v689w309gg9b5z4sr2")))

(define-public crate-tulisp-0.9.0 (c (n "tulisp") (v "0.9.0") (d (list (d (n "tulisp-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1f8xj64b5k7i1zpdbaizlvv3yd43lfsrmghvrbh1fsr82mwzcgd9")))

(define-public crate-tulisp-0.9.1 (c (n "tulisp") (v "0.9.1") (d (list (d (n "tulisp-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0cfqzix75ixq9508pxrp9q2bs6lb5i2qg3iib9h1mzby4xm30fal")))

(define-public crate-tulisp-0.9.2 (c (n "tulisp") (v "0.9.2") (d (list (d (n "tulisp-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0xmi4qypszpmircqzqhg08vh4jg3sbx30015lzcs4vhcbpsjvf6x")))

(define-public crate-tulisp-0.10.0 (c (n "tulisp") (v "0.10.0") (d (list (d (n "tulisp-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "09jaavwqqynwzvg4v0f37s55andn3ldn9vicwfwiklpqkg59imkn")))

(define-public crate-tulisp-0.10.1 (c (n "tulisp") (v "0.10.1") (d (list (d (n "tulisp-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0adz0vgagwcpl5vjwffj45vxb26hc500jfqwj9cvki20kf45l892")))

(define-public crate-tulisp-0.10.2 (c (n "tulisp") (v "0.10.2") (d (list (d (n "tulisp-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "04k7p0mzlclhz2yhpnnb0px2wqb5vgkwx3qg5n0rmb2p3ypx0jg1")))

(define-public crate-tulisp-0.11.0 (c (n "tulisp") (v "0.11.0") (d (list (d (n "tulisp-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1y7j09jajsw05a9naszii96y4d5azqxha8l02za6m55lphlwvnpz")))

(define-public crate-tulisp-0.11.1 (c (n "tulisp") (v "0.11.1") (d (list (d (n "tulisp-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0px8dyjwbs6rfr0xgispqsgd8k9d6vl4ifzk21c81i1apyrxfq9x")))

(define-public crate-tulisp-0.11.2 (c (n "tulisp") (v "0.11.2") (d (list (d (n "tulisp-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1381mkbk7z4ny5p9axmvyxdf0ha5agrm96nfvly65b15l473n85m")))

(define-public crate-tulisp-0.12.0 (c (n "tulisp") (v "0.12.0") (d (list (d (n "tulisp-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "02n8b4y4pp5c7q6zsq425xa0vxxvx798qfw5gvfmzfbfgl0fm7hv")))

(define-public crate-tulisp-0.13.0 (c (n "tulisp") (v "0.13.0") (d (list (d (n "tulisp-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0vwv7casz9nfd10xj3w24x6qkpgy4hkiqcqjiliy13wqzvk31c9d")))

(define-public crate-tulisp-0.13.1 (c (n "tulisp") (v "0.13.1") (d (list (d (n "tulisp-proc-macros") (r "^0.3.0") (d #t) (k 0)))) (h "00c4dr4hpzzdw2y0ksyligrl6jbvsj53sm3pmgz9mjdg8kim6vfa")))

(define-public crate-tulisp-0.14.0 (c (n "tulisp") (v "0.14.0") (d (list (d (n "tulisp-proc-macros") (r "^0.4.1") (d #t) (k 0)))) (h "1kvpzvgggwzs3g53nfnnybx4vwi1vsq67rqyb2fsbvbri73fvfy1")))

(define-public crate-tulisp-0.15.0 (c (n "tulisp") (v "0.15.0") (d (list (d (n "tulisp-proc-macros") (r "^0.4.1") (d #t) (k 0)))) (h "0lwfm773zw6vr5zqkcwqan5636wqm1aabwm9xvsw0qs9rxzqpz2y")))

(define-public crate-tulisp-0.16.0 (c (n "tulisp") (v "0.16.0") (d (list (d (n "tulisp-proc-macros") (r "^0.4.1") (d #t) (k 0)))) (h "0xcb2zvva40ld9a2wkl2907js5h6m5gyyj6lzrbafmrrql8a87kg")))

(define-public crate-tulisp-0.16.1 (c (n "tulisp") (v "0.16.1") (d (list (d (n "tulisp-proc-macros") (r "^0.4.1") (d #t) (k 0)))) (h "17bzc8fcpbl7rmm9617lw1algg5j240vysngc9bbkpfbbg3gwr17")))

(define-public crate-tulisp-0.16.2 (c (n "tulisp") (v "0.16.2") (d (list (d (n "tulisp-proc-macros") (r "^0.4.1") (d #t) (k 0)))) (h "0hnn28j1h00v0iwrchjxxcq8lz853skx4g38w9d4587kiijfigzl")))

(define-public crate-tulisp-0.16.3 (c (n "tulisp") (v "0.16.3") (d (list (d (n "tulisp-proc-macros") (r "^0.4.1") (d #t) (k 0)))) (h "0qn9cpygq0hiy6zzpp0gny8inqc8mppr4fyxfrqvk41nav4a6qmc")))

(define-public crate-tulisp-0.17.0 (c (n "tulisp") (v "0.17.0") (d (list (d (n "tulisp-proc-macros") (r "^0.4.1") (d #t) (k 0)))) (h "106pc9b2kpnh9sqgz8ari2gr7lbpsswiydnvbfr6gp7arxrfgnls")))

