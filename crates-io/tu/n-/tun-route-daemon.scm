(define-module (crates-io tu n- tun-route-daemon) #:use-module (crates-io))

(define-public crate-tun-route-daemon-0.1.0 (c (n "tun-route-daemon") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net-route") (r "^0.2.5") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xdz8w9wf1jp84isp5aw3n44ms3qpifrgy7vw54m0iy8bb132rlp")))

(define-public crate-tun-route-daemon-0.1.1 (c (n "tun-route-daemon") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net-route") (r "^0.2.5") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13c2fxvbhpxck9fzfa7gjk837jvr2lj95vjq1ij7vg3vwb11fqg9")))

(define-public crate-tun-route-daemon-0.1.2 (c (n "tun-route-daemon") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net-route") (r "^0.2.5") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19iq9ysl4j5vymy452aq4xzmjgqzyb3m8yxljqff47d9y1fsy72a") (y #t)))

(define-public crate-tun-route-daemon-0.1.3 (c (n "tun-route-daemon") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net-route") (r "^0.2.5") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06al4gpkgl4mbih5g6ry0a481vjhc9mr4iynnscv3d60l0l2cy39")))

(define-public crate-tun-route-daemon-0.1.5 (c (n "tun-route-daemon") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "net-route") (r "^0.2.5") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1l2kshv07ddw79b7xkh68cak3x0bpwnl7lb7qxmakk0a09mynr7j")))

