(define-module (crates-io tu n- tun-tap-mac) #:use-module (crates-io))

(define-public crate-tun-tap-mac-0.1.2 (c (n "tun-tap-mac") (v "0.1.2") (d (list (d (n "cc") (r "~1") (d #t) (k 1)) (d (n "futures") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (o #t) (d #t) (k 0)) (d (n "mio") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "tokio-core") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "0cify866wk25mp44fj53g3kfs9makip9zvkf49nljvhdafrczyab") (f (quote (("tokio" "futures" "libc" "mio" "tokio-core") ("default" "tokio"))))))

