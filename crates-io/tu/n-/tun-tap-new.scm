(define-module (crates-io tu n- tun-tap-new) #:use-module (crates-io))

(define-public crate-tun-tap-new-0.1.0 (c (n "tun-tap-new") (v "0.1.0") (d (list (d (n "cc") (r "~1") (d #t) (k 1)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "mio") (r "~0.6") (d #t) (k 0)) (d (n "tokio") (r "~0.1") (d #t) (k 0)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "1d4j2c7qlrhbdx028qq1hnzgakixg8rvhw06157pm1gbi09lcz3m")))

