(define-module (crates-io tu n- tun-sync) #:use-module (crates-io))

(define-public crate-tun-sync-0.1.0 (c (n "tun-sync") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "ioctl") (r "^0.6") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\"))") (k 0) (p "ioctl-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0880lhlrp7rl146m1acf1mkxl3srhpnn2nk237wrblr8qiwmqxmd")))

(define-public crate-tun-sync-0.1.1 (c (n "tun-sync") (v "0.1.1") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "ioctl") (r "^0.6") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\"))") (k 0) (p "ioctl-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "16qzf67avsb02ys31jb4rwni24c1b8dyidlclw7hxx32fyyfa3wf")))

