(define-module (crates-io tu n- tun-driver) #:use-module (crates-io))

(define-public crate-tun-driver-0.1.0 (c (n "tun-driver") (v "0.1.0") (d (list (d (n "cc") (r "~1") (d #t) (k 1)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "mio") (r "~0.6") (d #t) (k 0)) (d (n "tokio") (r "~0.1") (d #t) (k 0)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "0692r5b45b5qlp99sffsydgzmv1050q7bxvwg588fg9c62r4z2gb")))

