(define-module (crates-io tu -e tu-error) #:use-module (crates-io))

(define-public crate-tu-error-0.1.0 (c (n "tu-error") (v "0.1.0") (h "07w80sfbbj2czdr7gs3730lcq0mh868sj9s40hz5c8dz3jr0cvay")))

(define-public crate-tu-error-0.1.1 (c (n "tu-error") (v "0.1.1") (h "1d79innw1gsnjaq1bikja4w92v0fixwl6fl44m37id79zkg65dfi")))

