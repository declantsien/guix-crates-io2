(define-module (crates-io tu rs turs) #:use-module (crates-io))

(define-public crate-turs-0.0.2 (c (n "turs") (v "0.0.2") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2.26.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "tico") (r "^1.0.0") (d #t) (k 0)))) (h "067shinbhlhbvcigyjsaz4gh036y1mv6vva8vg9g88n5cr6b62j0")))

(define-public crate-turs-0.0.3 (c (n "turs") (v "0.0.3") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2.26.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "tico") (r "^1.0.0") (d #t) (k 0)))) (h "06rvdb8s9n6fjvr6dc938ilm45y2n9lhr5g1bnwc8jq052n8sixd")))

(define-public crate-turs-0.0.4 (c (n "turs") (v "0.0.4") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2.26.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "tico") (r "^1.0.0") (d #t) (k 0)))) (h "0kjvx9cxfxg11x85qfn4lqy3h3r6pkl4cdssms72i8gn01qva1gy")))

(define-public crate-turs-0.0.5 (c (n "turs") (v "0.0.5") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2.26.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "tico") (r "^1.0.0") (d #t) (k 0)))) (h "1cvn67mqww47b0785374zmsk86ycq0j6vby3c8xnhxxibgjap6sx")))

(define-public crate-turs-0.0.6 (c (n "turs") (v "0.0.6") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "tico") (r "^2.0") (d #t) (k 0)))) (h "00fc6a2vjm2ni1w431fbbm70bxm6dkvq4jkd9fgcq87fjcvnjrjq")))

(define-public crate-turs-0.0.7 (c (n "turs") (v "0.0.7") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "tico") (r "^2.0") (d #t) (k 0)))) (h "1jy712r7c473fr4jbh5zc4x5r8nigiyyz755sb5s4h0yj3r2hbjx")))

(define-public crate-turs-0.0.8 (c (n "turs") (v "0.0.8") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "tico") (r "^2.0") (d #t) (k 0)))) (h "06ggvq8qsf66da79m6mmk9wnfyiwil2fzv7ipkxr1w4g7n58zmgc")))

(define-public crate-turs-0.1.0 (c (n "turs") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "git2") (r "^0.16.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.1.0") (d #t) (k 0)) (d (n "tico") (r "^2.0") (d #t) (k 0)))) (h "0q5agk8pk05vwd78kc0mls1q05m8hflflnzhdf5xwws8zx4n32f6")))

