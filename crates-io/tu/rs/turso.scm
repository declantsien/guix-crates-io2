(define-module (crates-io tu rs turso) #:use-module (crates-io))

(define-public crate-turso-0.1.0 (c (n "turso") (v "0.1.0") (d (list (d (n "menva") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter" "json"))) (d #t) (k 0)))) (h "045ws78rbnal2wl11jy7qc3pn9laninz78dci7rz1spzagb2pdac")))

