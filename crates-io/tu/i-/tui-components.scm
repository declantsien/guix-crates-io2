(define-module (crates-io tu i- tui-components) #:use-module (crates-io))

(define-public crate-tui-components-0.1.0 (c (n "tui-components") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tui") (r "^0.16.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "1jrgkj8r5d0na3h0fcn3lswgbdlvg1ylxb8cim8xf1cfbl6jbfv1")))

(define-public crate-tui-components-0.1.1 (c (n "tui-components") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tui") (r "^0.16.0") (f (quote ("crossterm"))) (k 0)))) (h "0p5nggldbfaijn7lzfmiwwab60w73hgf7j3y1lq32n8g066hzi40")))

(define-public crate-tui-components-0.1.2 (c (n "tui-components") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tui") (r "^0.16.0") (f (quote ("crossterm"))) (k 0)))) (h "1ns6mr4174i98bzgg6cizy01367wigiviz0ja4k2ynjd0lin46rs")))

