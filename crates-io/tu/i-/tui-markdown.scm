(define-module (crates-io tu i- tui-markdown) #:use-module (crates-io))

(define-public crate-tui-markdown-0.1.0 (c (n "tui-markdown") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0z4rrdv1xhbjqy5fv79bp0n6rpqzbrlr64k7rydp0hy0l0x3js59")))

(define-public crate-tui-markdown-0.1.1 (c (n "tui-markdown") (v "0.1.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "16jf646rz4dyjnh70jphk4gj0gp72f55mcibzm2cm0c7g83nh6cm")))

(define-public crate-tui-markdown-0.2.0 (c (n "tui-markdown") (v "0.2.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1j4fj66fqm27ips8v0ilbwnw03c1djn6r0vd8afkh2vva6n0v7ak")))

(define-public crate-tui-markdown-0.2.1 (c (n "tui-markdown") (v "0.2.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "12kz3xpjfml62gaf7f7008vwkwh0whjvrddr63cwkxjlj3k3b4wa")))

(define-public crate-tui-markdown-0.2.2 (c (n "tui-markdown") (v "0.2.2") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "08g78v99sd6vpigfwfm5an2bkf47s69yhcb92y44n6cm6xz6ddv0")))

(define-public crate-tui-markdown-0.2.3 (c (n "tui-markdown") (v "0.2.3") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.10.3") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "180n1xyp6rllkbjqbrb55fhkh5xw3pkr9qmqyj43wkfaryphr0if")))

(define-public crate-tui-markdown-0.2.4 (c (n "tui-markdown") (v "0.2.4") (d (list (d (n "itertools") (r "^0.13.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.11.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0fqzgy9p9d7ckw9dwpprpz29dk5amm1r2nn95gp75fkav39rxcmf")))

