(define-module (crates-io tu i- tui-image) #:use-module (crates-io))

(define-public crate-tui-image-0.1.0 (c (n "tui-image") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 2)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "tui") (r "^0.5") (d #t) (k 0)))) (h "1a0ljkq1w2msisvqgkpfp6a4q8a3fm6f2v0hf7fh5f40nyrzdg8b")))

(define-public crate-tui-image-0.2.0 (c (n "tui-image") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "tui") (r "^0.18") (d #t) (k 0)) (d (n "tui") (r "^0.18") (f (quote ("termion"))) (k 2)))) (h "083yicr747h174bqxp9fmh91lb4iqh2rlqlxjfdzd34c0z8h86kx")))

