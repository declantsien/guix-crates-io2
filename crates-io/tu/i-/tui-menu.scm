(define-module (crates-io tu i- tui-menu) #:use-module (crates-io))

(define-public crate-tui-menu-0.1.0 (c (n "tui-menu") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 2)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "0fxjsnpcrm3nkd41xqc7bsq7lvys1n7j7xm91913h16hri6lc3hp")))

(define-public crate-tui-menu-0.1.1 (c (n "tui-menu") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 2)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "0qsx000liigd932jqv8bcii3ksl91yycgixknqvjp9d3qa1qxj9s")))

(define-public crate-tui-menu-0.2.0 (c (n "tui-menu") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "1lyy224jqzirc3zi832lxhlxn4yp21k0dbl1f9mk6mva8ahjyq8b")))

(define-public crate-tui-menu-0.2.1 (c (n "tui-menu") (v "0.2.1") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "1yabvrrlrwxvv3bzkhk8czij01jxgxl5m2lhnhq5br48b34s94fk")))

