(define-module (crates-io tu i- tui-clap) #:use-module (crates-io))

(define-public crate-tui-clap-0.1.0 (c (n "tui-clap") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "tui") (r "^0.15") (f (quote ("crossterm"))) (k 0)))) (h "11kqmr85n90f9chf2vbmxhjzmvi8zhmxy85fh4lb4dsbgav1i0gn")))

(define-public crate-tui-clap-0.1.1 (c (n "tui-clap") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "tui") (r "^0.15") (f (quote ("crossterm"))) (k 0)))) (h "07ahvwfblw902xnpywzjpg61kb7nhl3n14bw8a94qjqh2fxbpsv8")))

(define-public crate-tui-clap-0.2.0 (c (n "tui-clap") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "tui") (r "^0.15") (f (quote ("crossterm"))) (k 0)))) (h "0x5hs27xw7bl1p13jhx45993shrc2kpb3vk5v7c7f8pc6bmmkj3g")))

