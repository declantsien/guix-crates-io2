(define-module (crates-io tu i- tui-scrollview) #:use-module (crates-io))

(define-public crate-tui-scrollview-0.1.0 (c (n "tui-scrollview") (v "0.1.0") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0-alpha.1") (d #t) (k 0)))) (h "1bpl1qrii0yjfhdnqx68gxyjfhf339m0p78kgcjw70rc571d47q2")))

(define-public crate-tui-scrollview-0.1.1 (c (n "tui-scrollview") (v "0.1.1") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0-alpha.1") (d #t) (k 0)))) (h "1id4gbb73wv1kvygdna5ajrvmyjcqd0zmcxr1yxl1m12y1p9nkbg")))

(define-public crate-tui-scrollview-0.1.2 (c (n "tui-scrollview") (v "0.1.2") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0-alpha.1") (d #t) (k 0)))) (h "09ynqfxkvyc64swnkd5sx5l3m0xphhvv48nibggj08ci8s308aq4")))

(define-public crate-tui-scrollview-0.1.3 (c (n "tui-scrollview") (v "0.1.3") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.0-alpha.1") (d #t) (k 0)))) (h "09bqcky49flhcqrfjjipwfywhzv8mww6b59nq0q3akbk09040dqi")))

(define-public crate-tui-scrollview-0.1.4 (c (n "tui-scrollview") (v "0.1.4") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.0-alpha.1") (d #t) (k 0)))) (h "1lxzzzs48lai9yp6pvdvxsdli9y1758qd9rjxai8m4i98cvzj7b2")))

(define-public crate-tui-scrollview-0.1.5 (c (n "tui-scrollview") (v "0.1.5") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.0-alpha.1") (d #t) (k 0)))) (h "1vddgnl6rs3a6q9hcpqh79w37mdngsjmpn9jq5rxfi5pljniy30y")))

(define-public crate-tui-scrollview-0.2.0 (c (n "tui-scrollview") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.0-alpha.2") (d #t) (k 0)))) (h "0xnbgyi78n3vswwkw2lwdni0qykpv0pyl69fmasl2h34b5paxhad")))

(define-public crate-tui-scrollview-0.2.1 (c (n "tui-scrollview") (v "0.2.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.0-alpha.3") (d #t) (k 0)))) (h "08l7s5bppl338lfmqg53cphvgihl23101fvgcfn5y7zpfrd9n10w")))

(define-public crate-tui-scrollview-0.3.0 (c (n "tui-scrollview") (v "0.3.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)))) (h "1m57vf8zzs49raaimrxqhv93amcmbx3s1995n59qqj6g5yybiy5b")))

(define-public crate-tui-scrollview-0.3.1 (c (n "tui-scrollview") (v "0.3.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "1573qw75brm0bfbk315lx1afwq1592mlc9jd6rz31kj94qhq449n")))

(define-public crate-tui-scrollview-0.3.2 (c (n "tui-scrollview") (v "0.3.2") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "1n2myj0i7cgjyfr13idwnlda99pcira440rm67q7wf6hzv11hq5b")))

(define-public crate-tui-scrollview-0.3.3 (c (n "tui-scrollview") (v "0.3.3") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.1") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "119gr2k9k2qm31pmi0gk7dp1sxw6zp95c0zl9v6a9rvscid46fiy")))

(define-public crate-tui-scrollview-0.3.4 (c (n "tui-scrollview") (v "0.3.4") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.1") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "1x8diwd911ab8qp8hrk8491gs7d61s7r3d8399iwcxbqw99gs8dz")))

(define-public crate-tui-scrollview-0.3.5 (c (n "tui-scrollview") (v "0.3.5") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.1") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)) (d (n "rstest") (r "^0.19.0") (d #t) (k 0)))) (h "13lhg7n3sxwlyi86dq0wwikgaclpj5mnwv671zvpkxaqpafpvpqy")))

(define-public crate-tui-scrollview-0.3.6 (c (n "tui-scrollview") (v "0.3.6") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.1") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.3") (d #t) (k 0)) (d (n "rstest") (r "^0.19.0") (d #t) (k 0)))) (h "0ik0vz5hvnb5x5mrkh7kvdrhmbiislgwxpgsnp2xy2h858f016wc")))

