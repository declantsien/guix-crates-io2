(define-module (crates-io tu i- tui-news) #:use-module (crates-io))

(define-public crate-tui-news-0.1.0 (c (n "tui-news") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.22") (d #t) (k 0)) (d (n "open") (r "^2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r "^0.17") (d #t) (k 0)))) (h "1p24jp240flilzxjni73w0522453ks7vkdd6vwpdbbxqm4sx4k6p")))

(define-public crate-tui-news-0.1.1 (c (n "tui-news") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.22") (d #t) (k 0)) (d (n "open") (r "^2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r "^0.17") (d #t) (k 0)))) (h "09f5zc7b03cznji3dwwah216j655k5pnc8myravm9nyl6lrvyylr")))

