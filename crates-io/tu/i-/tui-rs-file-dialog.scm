(define-module (crates-io tu i- tui-rs-file-dialog) #:use-module (crates-io))

(define-public crate-tui-rs-file-dialog-0.1.0 (c (n "tui-rs-file-dialog") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1sw7pnqph8c510crhfvw18k7v27bcvwnni4v3amrmh27z5f21q10") (y #t)))

