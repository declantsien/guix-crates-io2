(define-module (crates-io tu i- tui-textbox) #:use-module (crates-io))

(define-public crate-tui-textbox-0.1.0 (c (n "tui-textbox") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)))) (h "0hadmak0xvjikjcvsfhf5ljjm68s8l2ak3zxdyvv9yg5cdlbg8xy") (f (quote (("default"))))))

(define-public crate-tui-textbox-0.2.0 (c (n "tui-textbox") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)))) (h "05ga53w8wwaq0yydsp3280aqlns0a3icmxgi6imhvfnmyxycbwaq") (f (quote (("default"))))))

