(define-module (crates-io tu i- tui-input) #:use-module (crates-io))

(define-public crate-tui-input-0.1.0 (c (n "tui-input") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nsvf2xv0dsgfsrzl5c2dgf4ikl4a27fq2wphb3i3qkc4rb0p7jn") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.1.1 (c (n "tui-input") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (o #t) (d #t) (k 0)))) (h "1dizjv4mp4rpzs2f3sf8jr3vhf3561q2faqqhv8z6ag7qlhc3nqy") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.1.2 (c (n "tui-input") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (o #t) (d #t) (k 0)))) (h "1abcfnykr4xpx39y1pn1rmzr5mw4645bhlgb49ra3x2idws72lj2") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.2.0 (c (n "tui-input") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0qc5362i6zinr5klnlanv5rlnrqqrxk70igig48fklafp06jmipm") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.2.1 (c (n "tui-input") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0cfm8rgjgv3pfq1v92f14d4xw08gn4bvg5rz5k59c0j8n5rv66sy") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.3.0 (c (n "tui-input") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0lxnj0sd377f7j0pixll482yykmpjblqkib54isb2vzwjrd1hmg3") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.4.0 (c (n "tui-input") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1y8gdhfcfhkbp771mad20yz4cqkjm7nb2jcqrickagswi3chkqjr") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.4.1 (c (n "tui-input") (v "0.4.1") (d (list (d (n "crossterm") (r "^0.23.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (o #t) (d #t) (k 0)))) (h "0p7azfmwhl5p8h8nbhjbns8havl9nsx9nl9jzd6n1gggvxqi3kzq") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.4.2 (c (n "tui-input") (v "0.4.2") (d (list (d (n "crossterm") (r "^0.23.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (o #t) (d #t) (k 0)))) (h "1y53zx5h1bb6k27m1c0lkvn0lbxsnc2xysvc27i5v7gj8py47cmp") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.5.0 (c (n "tui-input") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.25.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (o #t) (d #t) (k 0)))) (h "01bnwawky31d4mrx2a5n18v6gpzjhh47gr5njgazjns6fgbixf86") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.5.1 (c (n "tui-input") (v "0.5.1") (d (list (d (n "crossterm") (r "^0.25.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (o #t) (d #t) (k 0)))) (h "1pl54j8rcaf8bp7kd22gryl56sg3hir8ghbp354hp55ip5rrbzdq") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.6.0 (c (n "tui-input") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.25.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (o #t) (d #t) (k 0)))) (h "08qgkvs0fzmmyfdranpbbw02llicfz39akp9aiyk1r8ym4kazvz4") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.6.1 (c (n "tui-input") (v "0.6.1") (d (list (d (n "crossterm") (r "^0.25.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "06x4bbnfgdl4jwmay532b0xvzvhj6nw6hl34m72gyaldqjrhg8nn") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.7.0 (c (n "tui-input") (v "0.7.0") (d (list (d (n "crossterm") (r "^0.26.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1ppxh3r3diq34lp0wqz00zpncgi5nv4h66bljnia3ajjn499nha0") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.7.1 (c (n "tui-input") (v "0.7.1") (d (list (d (n "crossterm") (r "^0.26.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0yr2i118ivlcfa903whl5h2w43q29rasrphhr86v82gfdks1f3ww") (f (quote (("default" "crossterm"))))))

(define-public crate-tui-input-0.8.0 (c (n "tui-input") (v "0.8.0") (d (list (d (n "crossterm") (r "^0.27.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "00fwd3w06kb2w83jcdcffcwbayf8ch5rsria1a04rbx3cgw8brxk") (f (quote (("default" "crossterm"))))))

