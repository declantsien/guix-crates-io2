(define-module (crates-io tu i- tui-widget-list) #:use-module (crates-io))

(define-public crate-tui-widget-list-0.1.0 (c (n "tui-widget-list") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "ratatui") (r "^0.20") (d #t) (k 0)))) (h "1z5hmk7s1n5mc5pd54s1g9aj8nd8qmbhxyfmmqp5fk3l8kd6c7vb")))

(define-public crate-tui-widget-list-0.1.1 (c (n "tui-widget-list") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "ratatui") (r "^0.20") (d #t) (k 0)))) (h "1fj9zn28dvf994z7dd78656b0clfhc3zflp52clprdwg78qcmsx2")))

(define-public crate-tui-widget-list-0.2.0 (c (n "tui-widget-list") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "ratatui") (r "^0.20") (d #t) (k 0)))) (h "05vgajidjj6kgslx6p1yx79ppqw5nn5a1bnxcj753x88938xarpm")))

(define-public crate-tui-widget-list-0.2.1 (c (n "tui-widget-list") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "ratatui") (r "^0.20") (d #t) (k 0)))) (h "0b035nx7ynan84dxhfyrcz2bh1059yiv2inrk2rp1abfph2w39mq")))

(define-public crate-tui-widget-list-0.2.2 (c (n "tui-widget-list") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "ratatui") (r "^0.20") (d #t) (k 0)))) (h "19s1qk7p3kj3r2svsxgpsp6pw29ysiqrqn00knd9513ghz6xn1lf")))

(define-public crate-tui-widget-list-0.2.3 (c (n "tui-widget-list") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 0)))) (h "1qn83yzykvhik7hyfdg8ycn4qyax13si4x8jppqw41sfphv0w3x6")))

(define-public crate-tui-widget-list-0.3.0 (c (n "tui-widget-list") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 0)))) (h "0xlh1y6kqqh5mvi8xbgz18w510ky9dxxb0bchkgbqgb4xf3cknby")))

(define-public crate-tui-widget-list-0.3.1 (c (n "tui-widget-list") (v "0.3.1") (d (list (d (n "crossterm") (r "0.*") (d #t) (k 0)) (d (n "ratatui") (r "0.*") (d #t) (k 0)))) (h "05aq2hhkvs67bs4aai0fpmbq8hmqipxqrx8lsfn12cbyd40z3gfi")))

(define-public crate-tui-widget-list-0.4.0 (c (n "tui-widget-list") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "ratatui") (r "^0.24") (d #t) (k 0)))) (h "1km5h0y4m81r57mi6mv32rfbf17xdmkq1x3h2kq2x4fn5fg8s28p") (y #t)))

(define-public crate-tui-widget-list-0.5.0 (c (n "tui-widget-list") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "ratatui") (r "^0.24") (d #t) (k 0)))) (h "0gb0n3rvbd0z3nwh4vah4cr9x4np4x0sbqczf5w7jv71fbzxs71m")))

(define-public crate-tui-widget-list-0.6.0 (c (n "tui-widget-list") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.25") (d #t) (k 0)))) (h "0bhrj24hd7kg5xbkanzr70hqgi84y61kazhcc04dqkijaa6sxw84")))

(define-public crate-tui-widget-list-0.6.1 (c (n "tui-widget-list") (v "0.6.1") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.25") (d #t) (k 0)))) (h "0alfcl0930518r3xhj1pm4hanc10s4ppc19x4sq92k9psxlygbq9")))

(define-public crate-tui-widget-list-0.7.0 (c (n "tui-widget-list") (v "0.7.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)))) (h "19ad1pi47q9dlwrw31qbhkwnncc0n7f9ny500xg3jgm26jl4qcis") (y #t)))

(define-public crate-tui-widget-list-0.7.1 (c (n "tui-widget-list") (v "0.7.1") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)))) (h "1mips7bzgxz8kj9adr6nm6ly6il8wlf2barb6gh49zjgs4xy6wm8")))

(define-public crate-tui-widget-list-0.7.2 (c (n "tui-widget-list") (v "0.7.2") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)))) (h "0a5pfcgn1m7lhgqhqldy14z5wj873ac3sv7pqg22s2mc0vifx453")))

(define-public crate-tui-widget-list-0.8.0 (c (n "tui-widget-list") (v "0.8.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)))) (h "1sh0fd5kqifdpf5iyhhgfawpsj3hd7mnyflqsxl2qasmw848pxhr") (y #t)))

(define-public crate-tui-widget-list-0.8.1 (c (n "tui-widget-list") (v "0.8.1") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)))) (h "1v7lwzy4ddcbqfrbdpx7znwwm0bwrjwl8n3n278zvhbcj85l8b4d")))

(define-public crate-tui-widget-list-0.8.2 (c (n "tui-widget-list") (v "0.8.2") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)))) (h "1s7nqq0i1dlmmk5c7ms68ipy9v01zz28953138nv5bvshihl0rlk")))

(define-public crate-tui-widget-list-0.8.3 (c (n "tui-widget-list") (v "0.8.3") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)))) (h "109zlgmmcgzkgh5m6a4cfc72b4b08phfdksrbrcc1lk2fmvig98g")))

(define-public crate-tui-widget-list-0.9.0 (c (n "tui-widget-list") (v "0.9.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)))) (h "1zfw3n64nm6x3dz4bxkpw5wymb6g54zkbbd9f1yrcak7n0k6cwnz")))

