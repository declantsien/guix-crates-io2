(define-module (crates-io tu i- tui-dashboard) #:use-module (crates-io))

(define-public crate-tui-dashboard-0.1.0 (c (n "tui-dashboard") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "ratatui-macros") (r "^0.2.4") (d #t) (k 0)) (d (n "tui-big-text") (r "^0.3.6") (d #t) (k 0)))) (h "0vdfwy7f6vqjk3cy7fc9hshawh9nk1a03imglps10jpdvf0nlpam")))

(define-public crate-tui-dashboard-0.1.1 (c (n "tui-dashboard") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "ratatui-macros") (r "^0.2.4") (d #t) (k 0)) (d (n "tui-big-text") (r "^0.3.6") (d #t) (k 0)))) (h "14pb66gmdwk5r25imb11lzib5vc4k0yzxbah6js9xmmgvcfn7pa2")))

(define-public crate-tui-dashboard-0.1.2 (c (n "tui-dashboard") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "ratatui-macros") (r "^0.2.4") (d #t) (k 0)) (d (n "tui-big-text") (r "^0.3.6") (d #t) (k 0)))) (h "12ic9sr75s2rry7i5p07zd5ld9wczkp8h3pgcb08dgnywnmx25p1")))

