(define-module (crates-io tu i- tui-pattern-highlighter) #:use-module (crates-io))

(define-public crate-tui-pattern-highlighter-0.1.0 (c (n "tui-pattern-highlighter") (v "0.1.0") (d (list (d (n "ratatui") (r "^0.26.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "02mz8rkpd8bp2r20rlf3vxyggk3j53bval73s6b3qsa4snp0z971")))

