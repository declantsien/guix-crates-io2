(define-module (crates-io tu i- tui-tools) #:use-module (crates-io))

(define-public crate-tui-tools-0.1.0 (c (n "tui-tools") (v "0.1.0") (d (list (d (n "enable-ansi-support") (r "^0.2.1") (d #t) (k 0)))) (h "0z3m38z2z6njqh8nflj8cxcma03mdhg332lngk5yjn66z6ic2bg1")))

(define-public crate-tui-tools-0.1.1 (c (n "tui-tools") (v "0.1.1") (d (list (d (n "enable-ansi-support") (r "^0.2.1") (d #t) (k 0)))) (h "0ybjmk53dalwii6w40m37l5a2fx8nian3hpi8mnbpzqk9p9lgfhp")))

(define-public crate-tui-tools-0.1.2 (c (n "tui-tools") (v "0.1.2") (d (list (d (n "enable-ansi-support") (r "^0.2.1") (d #t) (k 0)))) (h "0d5w1nvabnnna3cl14bgpf4cf6pac2kw7l1h0acrkdc4xpp0a4wq")))

