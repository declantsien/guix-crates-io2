(define-module (crates-io tu i- tui-tree-widget) #:use-module (crates-io))

(define-public crate-tui-tree-widget-0.1.0 (c (n "tui-tree-widget") (v "0.1.0") (d (list (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "tui") (r "^0.12") (k 0)) (d (n "tui") (r "^0.12") (f (quote ("termion"))) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0lgmkwqqd5rpdr6kk61xv3z5cd45jggpnhnr4mpnccvg9ji3ncab")))

(define-public crate-tui-tree-widget-0.2.0 (c (n "tui-tree-widget") (v "0.2.0") (d (list (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "tui") (r "^0.13") (k 0)) (d (n "tui") (r "^0.13") (f (quote ("termion"))) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "17ks5ks1d5h9xf39f912v088m02fm2l9k2f7sk0xxf0lliac6yzn")))

(define-public crate-tui-tree-widget-0.3.0 (c (n "tui-tree-widget") (v "0.3.0") (d (list (d (n "termion") (r "^1") (d #t) (k 2)) (d (n "tui") (r "^0.14") (k 0)) (d (n "tui") (r "^0.14") (f (quote ("termion"))) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "08jh2s6s76jf45959rha3z5j8mmiirfhqfpl46q41qbavfca14a5")))

(define-public crate-tui-tree-widget-0.4.0 (c (n "tui-tree-widget") (v "0.4.0") (d (list (d (n "termion") (r "^1") (d #t) (k 2)) (d (n "tui") (r "^0.14") (k 0)) (d (n "tui") (r "^0.14") (f (quote ("termion"))) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "18sipwr7qcpa522fk8nr3by75l1dpnwh98v88hgz6hsdjrmv09x4")))

(define-public crate-tui-tree-widget-0.5.0 (c (n "tui-tree-widget") (v "0.5.0") (d (list (d (n "termion") (r "^1") (d #t) (k 2)) (d (n "tui") (r "^0.14") (k 0)) (d (n "tui") (r "^0.14") (f (quote ("termion"))) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1wzp966k2wzxw63qrf7n7scijm1nax30d0fd3qhan3g62n7frv2p")))

(define-public crate-tui-tree-widget-0.6.0 (c (n "tui-tree-widget") (v "0.6.0") (d (list (d (n "termion") (r "^1") (d #t) (k 2)) (d (n "tui") (r "^0.15") (k 0)) (d (n "tui") (r "^0.15") (f (quote ("termion"))) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0jaszxgzrhdlsz3p1642xzvv9pxhcwf0wqn3yp1gng7cw79bv2ig")))

(define-public crate-tui-tree-widget-0.7.0 (c (n "tui-tree-widget") (v "0.7.0") (d (list (d (n "termion") (r "^1") (d #t) (k 2)) (d (n "tui") (r "^0.16") (k 0)) (d (n "tui") (r "^0.16") (f (quote ("termion"))) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1n7w4666j4ahxnzr622i9mhzc96ix01swbk4i08mc729a51lp9b5")))

(define-public crate-tui-tree-widget-0.8.0 (c (n "tui-tree-widget") (v "0.8.0") (d (list (d (n "termion") (r "^1") (d #t) (k 2)) (d (n "tui") (r "^0.17") (k 0)) (d (n "tui") (r "^0.17") (f (quote ("termion"))) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0lqwxvsf3b092gzqypr7i7gzxyrfr37ran168h6yanx7dwvdra8s") (r "1.56.1")))

(define-public crate-tui-tree-widget-0.8.1 (c (n "tui-tree-widget") (v "0.8.1") (d (list (d (n "termion") (r "^1") (d #t) (k 2)) (d (n "tui") (r "^0.17") (k 0)) (d (n "tui") (r "^0.17") (f (quote ("termion"))) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0gc7s4f0mpfhndnfn4bczw6r9730sj177ddzhlp382c73rvca1cd") (r "1.56.1")))

(define-public crate-tui-tree-widget-0.9.0 (c (n "tui-tree-widget") (v "0.9.0") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 2)) (d (n "tui") (r "^0.18") (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1sxhp62nz3k5rcdjq6489gjr61zmag3ngxxrbasx6pw8f8z71403") (r "1.56.1")))

(define-public crate-tui-tree-widget-0.10.0 (c (n "tui-tree-widget") (v "0.10.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 2)) (d (n "tui") (r "^0.19") (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0cy3w98biaach0p7xd4l1ns0mhkikn996yvgab2hcg0pvh5gy22z") (r "1.56.1")))

(define-public crate-tui-tree-widget-0.11.0 (c (n "tui-tree-widget") (v "0.11.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 2)) (d (n "tui") (r "^0.19") (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1wssrr4j135m9d723p54pp9rhh13xvbwsfajgrgxs9lg13i69kb4") (r "1.56.1")))

(define-public crate-tui-tree-widget-0.12.0 (c (n "tui-tree-widget") (v "0.12.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 2)) (d (n "tui") (r "^0.19") (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1jwq1d6l70h7j7sd88p6pm1akph5sgbhaqyyvsk3l9kfx03a6v5g") (r "1.56.1")))

(define-public crate-tui-tree-widget-0.13.0 (c (n "tui-tree-widget") (v "0.13.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.23") (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1mc40y6bfp6hgx8mvcxq381sy55y27sc4ng2n5gr4kwgzdr927zh")))

(define-public crate-tui-tree-widget-0.14.0 (c (n "tui-tree-widget") (v "0.14.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.24") (k 0)) (d (n "ratatui") (r "^0.24") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0rfb1ihd6as7bq0j33i58rxfckpcp091b4s7gzxvccr5lmx3174v")))

(define-public crate-tui-tree-widget-0.15.0 (c (n "tui-tree-widget") (v "0.15.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.24") (k 0)) (d (n "ratatui") (r "^0.24") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "19jdawk9pq6ysm6fa04ibgzxwgnkd8ysm069h3f46c5wf9fz7s41")))

(define-public crate-tui-tree-widget-0.16.0 (c (n "tui-tree-widget") (v "0.16.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.25") (k 0)) (d (n "ratatui") (r "^0.25") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0k3fjk5na7filnyz444wpybv57shinvvbi02k94r5wy452ri2q0k")))

(define-public crate-tui-tree-widget-0.17.0 (c (n "tui-tree-widget") (v "0.17.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (k 0)) (d (n "ratatui") (r "^0.26") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0448l8af17d14i5yac867x2c53z91vgba65i5qx98bglc6q7ncaw")))

(define-public crate-tui-tree-widget-0.18.0 (c (n "tui-tree-widget") (v "0.18.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (k 0)) (d (n "ratatui") (r "^0.26") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1k1dxp7m8v1zrpdwib7mr912cpsn0vprzh7pjh2q7dilpii5w1d9")))

(define-public crate-tui-tree-widget-0.19.0 (c (n "tui-tree-widget") (v "0.19.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (k 0)) (d (n "ratatui") (r "^0.26") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1w2wpbg4w853v7al3dm6vipc3m74zdd4i0wg1g2ikrw78n96y37v")))

(define-public crate-tui-tree-widget-0.20.0 (c (n "tui-tree-widget") (v "0.20.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "pprof") (r "^0.13") (f (quote ("criterion" "flamegraph"))) (d #t) (t "cfg(target_family = \"unix\")") (k 2)) (d (n "ratatui") (r "^0.26") (k 0)) (d (n "ratatui") (r "^0.26") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1k3xmn333cn35i6fqyiafg033sm1v4v39zjcmincp24dmpl1s856")))

