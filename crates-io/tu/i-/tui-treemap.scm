(define-module (crates-io tu i- tui-treemap) #:use-module (crates-io))

(define-public crate-tui-treemap-0.1.0 (c (n "tui-treemap") (v "0.1.0") (d (list (d (n "treemap") (r "^0.3") (d #t) (k 0)) (d (n "tui") (r "^0.19") (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 2)) (d (n "tui") (r "^0.19") (f (quote ("crossterm"))) (d #t) (k 2)))) (h "0fhah2cvs1gsnfnrcql0b2xx63y0nwbxxb0w5znxsipkqyymg1vw")))

