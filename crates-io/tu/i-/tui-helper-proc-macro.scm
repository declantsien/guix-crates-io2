(define-module (crates-io tu i- tui-helper-proc-macro) #:use-module (crates-io))

(define-public crate-tui-helper-proc-macro-0.1.2 (c (n "tui-helper-proc-macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pwvj11y2y0b5rn8mxy773za73b54m875ljzf6qi4dqiphjxkfiq")))

(define-public crate-tui-helper-proc-macro-0.2.0 (c (n "tui-helper-proc-macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yp9f0cnmw1wimmn1ncq2vcafbhkgyk8pf5mlpambjrdxk8syar7")))

(define-public crate-tui-helper-proc-macro-0.2.1 (c (n "tui-helper-proc-macro") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01qm48jlj6swcsy59f30wym09b2anpr1fbm3d9abwj9wn3314h9p")))

(define-public crate-tui-helper-proc-macro-0.3.0 (c (n "tui-helper-proc-macro") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08fg9w1a0v9d4qq5kc4sd5zbzsymvfjgxks16jnkan1zqnz2wlga")))

(define-public crate-tui-helper-proc-macro-0.3.1 (c (n "tui-helper-proc-macro") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a8b65hm7mm2x7797cbp9b0br64id17xj5bxhmxa6hill0isyyvr")))

(define-public crate-tui-helper-proc-macro-0.3.2 (c (n "tui-helper-proc-macro") (v "0.3.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10yl3ax6flb2sg3qkfxbqdipgmyxmf0wwkyvdl6nib2dxhw7wmm9")))

(define-public crate-tui-helper-proc-macro-0.3.3 (c (n "tui-helper-proc-macro") (v "0.3.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hihyg6qxnpzxfcd9b7z77k1v7abmynfwv00cnzhb0yw33bcvai6")))

(define-public crate-tui-helper-proc-macro-0.4.0 (c (n "tui-helper-proc-macro") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19h70kcis9ng0n41rm0dpm9r7nasr08jwyl4w9pckg0fv9axpkbx")))

(define-public crate-tui-helper-proc-macro-0.4.1 (c (n "tui-helper-proc-macro") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f93455vv3m9gg98qm85fna8mz5ypn255cjlca62jkvvzdzxk05j")))

(define-public crate-tui-helper-proc-macro-0.4.2 (c (n "tui-helper-proc-macro") (v "0.4.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10gg1q00fzxqqsab9phyl2kwi9m9h3rhp7mimn552c5j71vpz99s")))

(define-public crate-tui-helper-proc-macro-0.5.0 (c (n "tui-helper-proc-macro") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17ppnigrw0z81nf5bk9vikv71yv2d49xm13vgj8gb206n7hwcccr")))

(define-public crate-tui-helper-proc-macro-0.5.1 (c (n "tui-helper-proc-macro") (v "0.5.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "072sjxxy1vdw75wfmahkg9lry3qgcdcbp5zvyfa5xwlimsc1sr2n")))

(define-public crate-tui-helper-proc-macro-0.5.2 (c (n "tui-helper-proc-macro") (v "0.5.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1flm32c1q90pjk76hbm0070mx260nv1ryknnjgi3ybwcwfr4v8hb")))

