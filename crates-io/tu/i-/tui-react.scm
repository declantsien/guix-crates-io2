(define-module (crates-io tu i- tui-react) #:use-module (crates-io))

(define-public crate-tui-react-0.1.0 (c (n "tui-react") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.6.0") (d #t) (k 0)))) (h "01i42b3dnjz0s88kywhpr4yifjvd3a57ll4gzpph0bwpng7ji2c0")))

(define-public crate-tui-react-0.1.1 (c (n "tui-react") (v "0.1.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.6.0") (d #t) (k 0)))) (h "1ml7gy2a67zjqxj0ivndhc8rfy9c7sx6s6wj7mfqg1i5zqgh2v3l")))

(define-public crate-tui-react-0.1.2 (c (n "tui-react") (v "0.1.2") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.6.0") (d #t) (k 0)))) (h "17pqlbhkfgs163xjcds17526hvca6qgmw7yqj0plyllfznm2jwyz")))

(define-public crate-tui-react-0.2.0 (c (n "tui-react") (v "0.2.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.8.0") (d #t) (k 0)))) (h "18whkp4f5dwhh1dlx1442dr2kw9x5cbkaf8c4p2z7lvl46ccw107")))

(define-public crate-tui-react-0.2.1 (c (n "tui-react") (v "0.2.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.8.0") (d #t) (k 0)))) (h "1dand12z7riifyjrapazgwg7vc50xh65i2wp7a1hxhvwz0r6fw09")))

(define-public crate-tui-react-0.2.2 (c (n "tui-react") (v "0.2.2") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.8.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "11f7zgr0zq0i2gaa3m8x6dvdp3agyxwv1vp54g1rw6qcs4g3k6l1")))

(define-public crate-tui-react-0.3.0 (c (n "tui-react") (v "0.3.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.8.0") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "0y2l2yy1hkqkspfbdgvv6y6h4r2l6zg65mbkdvh1svd4iki5di0n")))

(define-public crate-tui-react-0.4.0 (c (n "tui-react") (v "0.4.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1m0khk7baw3yg2d39hmcfvxyi1kf638gvkbq9gmiwnh8a2ksm1f5")))

(define-public crate-tui-react-0.4.1 (c (n "tui-react") (v "0.4.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1pv998nwm7m0z736h5lda4w7j2z0a9nnd4kxz5d2g8kq7dwxx84l")))

(define-public crate-tui-react-0.10.0 (c (n "tui-react") (v "0.10.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.10.0") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "0y5g9gwwf311yl2g1vip1slp90965zbqclwsqwnglq763licbfr1")))

(define-public crate-tui-react-0.10.1 (c (n "tui-react") (v "0.10.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.10.0") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "0qbw8yvvna024fxmrf909hjacvrbfy86ch0z6m3p15ah4lbylyx9")))

(define-public crate-tui-react-0.12.0 (c (n "tui-react") (v "0.12.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.12.0") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1svfzalpk8jqjcbglqlr86hqn6insshdnmyvyl5rp1pigl4f4l3p")))

(define-public crate-tui-react-0.13.0 (c (n "tui-react") (v "0.13.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.13.0") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1vqhix53saa0fla3yak1vrrn7asvsl3ax3ir0y2gjwawld08bbpk")))

(define-public crate-tui-react-0.14.0 (c (n "tui-react") (v "0.14.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.14.0") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1yplhgpsiqk6sd2yw0xyq59zsxqwi19nmzni8q6srfssxyqjsjgy")))

(define-public crate-tui-react-0.15.0 (c (n "tui-react") (v "0.15.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.15.0") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "09l16j3k86fbrz0rxswc6nsr3bhx70y26awkh1asprsbxfkqji20")))

(define-public crate-tui-react-0.16.0 (c (n "tui-react") (v "0.16.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.16.0") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "0rrbn3cyddq3yy45ilvxlzlpskrvgk48f8ikgxfy6b1ir2xgg1in")))

(define-public crate-tui-react-0.17.0 (c (n "tui-react") (v "0.17.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.17.0") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1va3imj01d6hy5hyhn0zg73l5dqk79fscvzf354svzbxkjdkw3aa")))

(define-public crate-tui-react-0.19.0 (c (n "tui-react") (v "0.19.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "0kcgci93f8kxmvdydjkc9hcj80cbba2gg4b8m2pds0dgk8q3fb2l")))

(define-public crate-tui-react-0.20.0 (c (n "tui-react") (v "0.20.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.20.1") (k 0) (p "ratatui")) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "19zr81rz3i7ckp0f0jmz7r8iv5jcj0a1g71zpkr2n46svxz3mw8i")))

(define-public crate-tui-react-0.21.0 (c (n "tui-react") (v "0.21.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.24.0") (k 0) (p "ratatui")) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "0nzdm6z25ha4vhky0l9gwaqr9yx0rm791r3bl35lk7l5kp768gd9")))

(define-public crate-tui-react-0.22.0 (c (n "tui-react") (v "0.22.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.25.0") (k 0) (p "ratatui")) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "04b5av7fpg46bz3zbgqxdwkrx42lcfcap1afxhd6qrnlw9rwvyig")))

(define-public crate-tui-react-0.23.0 (c (n "tui-react") (v "0.23.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.26.0") (k 0) (p "ratatui")) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1n209j4z3k5kvzs1xnr85rr2adf09fzybbygws3ayj2zwxm09l06")))

(define-public crate-tui-react-0.23.1 (c (n "tui-react") (v "0.23.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.26.0") (k 0) (p "ratatui")) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "03f1mpx7ahgzkkx74s9b5yxjzi6k6pvymgk2l4scs6q5ra9m1ihi")))

(define-public crate-tui-react-0.23.2 (c (n "tui-react") (v "0.23.2") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tui") (r "^0.26.0") (k 0) (p "ratatui")) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1inc78yngwkambrpcs1alallndysnzdq6p8pd7c12cqgw00sf519")))

