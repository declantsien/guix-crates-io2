(define-module (crates-io tu i- tui-markup-ansi-macro) #:use-module (crates-io))

(define-public crate-tui-markup-ansi-macro-0.1.0 (c (n "tui-markup-ansi-macro") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "tui-markup") (r "^0.2.1") (f (quote ("ansi"))) (d #t) (k 0)))) (h "1cyci8m5g2dv7dmxmzyh30y3hwhs40ajs78c1idag4jrxabwgwmk")))

