(define-module (crates-io tu i- tui-popup) #:use-module (crates-io))

(define-public crate-tui-popup-0.1.0 (c (n "tui-popup") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)))) (h "0ljknhrc6bp160d185cbkrba0l06wy5z66q1xj3fii1gv51d6nlw")))

(define-public crate-tui-popup-0.1.1 (c (n "tui-popup") (v "0.1.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)))) (h "1gli6mbjyp8ms7agf5kvjgqbc1a2mfdfb31pha38xcy5w3xm18gr")))

(define-public crate-tui-popup-0.2.0 (c (n "tui-popup") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "derive_setters") (r "^0.1.6") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)))) (h "1aracqfswzqjdy8v1lq9jfyfxkidl2fyr1242cp95pps36cv507i")))

(define-public crate-tui-popup-0.2.1 (c (n "tui-popup") (v "0.2.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "derive-getters") (r "^0.3.0") (d #t) (k 0)) (d (n "derive_setters") (r "^0.1.6") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)))) (h "056w542gnr3qscxh9943dm23kb5217sbbsylqbsqiz6llkgyawy7")))

(define-public crate-tui-popup-0.2.2 (c (n "tui-popup") (v "0.2.2") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "derive-getters") (r "^0.3.0") (d #t) (k 0)) (d (n "derive_setters") (r "^0.1.6") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)))) (h "03ynndn9s5y75mzsakxqm4d4w4hm1am7p59v7wl3vai62qiqhqgh")))

(define-public crate-tui-popup-0.2.3 (c (n "tui-popup") (v "0.2.3") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "derive-getters") (r "^0.3.0") (d #t) (k 0)) (d (n "derive_setters") (r "^0.1.6") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "0lhl1xl2b19bpdzrv6m72dcfwp4mnr12jnv5hi5p2qcw6gz3lbb4")))

(define-public crate-tui-popup-0.2.4 (c (n "tui-popup") (v "0.2.4") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "derive-getters") (r "^0.3.0") (d #t) (k 0)) (d (n "derive_setters") (r "^0.1.6") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.1") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "0lgz33hkf3k0xk49hyywmzbniwfw9slrl3h1lca2d0iw1rfl5ha5")))

(define-public crate-tui-popup-0.3.0 (c (n "tui-popup") (v "0.3.0") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "derive-getters") (r "^0.3.0") (d #t) (k 0)) (d (n "derive_setters") (r "^0.1.6") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.1") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.2") (f (quote ("unstable-widget-ref"))) (d #t) (k 0)))) (h "1k4r0mmdpjym2b2zzzw49q443cq5s68zpj44fd4hzxsbfgf5yw38")))

(define-public crate-tui-popup-0.3.1 (c (n "tui-popup") (v "0.3.1") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "derive-getters") (r "^0.3.0") (d #t) (k 0)) (d (n "derive_setters") (r "^0.1.6") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.1") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.2") (f (quote ("unstable-widget-ref"))) (d #t) (k 0)))) (h "1s676i8gk2c9ida1vdpvw9w8kvx4z9nqlhfsyhkg6wn4lhjcpd4v")))

(define-public crate-tui-popup-0.3.2 (c (n "tui-popup") (v "0.3.2") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "derive-getters") (r "^0.4.0") (d #t) (k 0)) (d (n "derive_setters") (r "^0.1.6") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.1") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.3") (f (quote ("unstable-widget-ref"))) (d #t) (k 0)))) (h "0z2z5dnljqh4r8nsfg609h2h73clhykisg45r9hib7df25glwshc")))

