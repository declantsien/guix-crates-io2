(define-module (crates-io tu i- tui-image-rgba-updated) #:use-module (crates-io))

(define-public crate-tui-image-rgba-updated-0.2.0 (c (n "tui-image-rgba-updated") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 2)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "tui") (r "^0.14") (d #t) (k 0)))) (h "01aishcjx2kpqv3jp23qp75szjr4syd13gsxvl08p9gv9cal28xb")))

(define-public crate-tui-image-rgba-updated-0.2.1 (c (n "tui-image-rgba-updated") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "tui") (r "^0.18") (d #t) (k 0)) (d (n "tui") (r "^0.18") (f (quote ("termion"))) (k 2)))) (h "05a3yrkk18bm8vw70nxy7knyq3lrqhr8xs8h7jk11p0g5mqh2r83")))

(define-public crate-tui-image-rgba-updated-0.2.2 (c (n "tui-image-rgba-updated") (v "0.2.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "tui") (r "^0.18") (d #t) (k 0)) (d (n "tui") (r "^0.18") (f (quote ("termion"))) (k 2)))) (h "13j4avzpgl7m4b5g0fsrws915hiz8qw73k47rzi1bnar3jirwlm3")))

