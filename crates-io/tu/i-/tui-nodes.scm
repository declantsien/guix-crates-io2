(define-module (crates-io tu i- tui-nodes) #:use-module (crates-io))

(define-public crate-tui-nodes-0.1.0 (c (n "tui-nodes") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tui") (r "^0.19") (k 0)))) (h "16vhrpxp31k88f5863n4vvg94m2pdffmcmxsqdmd53qp0bksihli")))

(define-public crate-tui-nodes-0.1.1 (c (n "tui-nodes") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tui") (r "^0.19") (k 0)) (d (n "tui") (r "^0.19") (f (quote ("crossterm"))) (d #t) (k 2)))) (h "160fd0jpykbr1q2prslnvi4ra4jknq45d2p9kaddmhcpqqs2fyld")))

(define-public crate-tui-nodes-0.2.0 (c (n "tui-nodes") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.8.2") (d #t) (k 0)) (d (n "tui") (r "^0.19") (k 0)) (d (n "tui") (r "^0.19") (f (quote ("crossterm"))) (d #t) (k 2)))) (h "0njbsmx8zkr7m2ilwrp44f65dg119f3nx0m9yx2afs55nbyl2va0")))

(define-public crate-tui-nodes-0.3.0 (c (n "tui-nodes") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ratatui") (r "^0.21") (k 0)) (d (n "ratatui") (r "^0.21") (f (quote ("crossterm"))) (d #t) (k 2)) (d (n "sorted-vec") (r "^0.8.2") (d #t) (k 0)))) (h "0njzrda3v8v0486k1za7xyb5g63bbks42cnly7wpyzlqkrrvm9jq")))

(define-public crate-tui-nodes-0.3.1 (c (n "tui-nodes") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ratatui") (r "^0.21") (k 0)) (d (n "ratatui") (r "^0.21") (f (quote ("crossterm"))) (d #t) (k 2)) (d (n "sorted-vec") (r "^0.8.2") (d #t) (k 0)))) (h "17w106dfaj3sn9bbwh1mdmxn37v1lw63w3c5f1msp9298gdzhxff")))

(define-public crate-tui-nodes-0.4.0 (c (n "tui-nodes") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ratatui") (r "^0.26") (k 0)) (d (n "ratatui") (r "^0.26") (f (quote ("crossterm"))) (d #t) (k 2)) (d (n "sorted-vec") (r "^0.8.2") (d #t) (k 0)))) (h "1nvgck2wmb3cz7ia2hm35zjq2m3nhxqaybycrs4xwh44qbv7n10f")))

