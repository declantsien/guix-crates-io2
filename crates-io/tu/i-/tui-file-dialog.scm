(define-module (crates-io tu i- tui-file-dialog) #:use-module (crates-io))

(define-public crate-tui-file-dialog-0.1.0 (c (n "tui-file-dialog") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1zhx0244r38xccnb6blrwmkyddfhiv061zi0hligjm4lpvvgb55k")))

