(define-module (crates-io tu i- tui-bars) #:use-module (crates-io))

(define-public crate-tui-bars-0.1.0 (c (n "tui-bars") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "03b5a1mxb8j6iwm43swl0ir0r6p5hnlyszf4d6ad7qiixsqkzk94")))

