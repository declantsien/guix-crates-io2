(define-module (crates-io tu i- tui-image-viewer) #:use-module (crates-io))

(define-public crate-tui-image-viewer-0.1.0 (c (n "tui-image-viewer") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0g9m6cs0a1g9jjj2b88nm8pys5m4hdjw8ra7bbx2r3hwlm7yp0v8")))

