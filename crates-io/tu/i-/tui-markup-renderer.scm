(define-module (crates-io tu i- tui-markup-renderer) #:use-module (crates-io))

(define-public crate-tui-markup-renderer-0.1.0 (c (n "tui-markup-renderer") (v "0.1.0") (d (list (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "1vw2fydh6syjvw9w6dkkdj9sba9rpfaqzciv4ghb4syl666rxgb3")))

(define-public crate-tui-markup-renderer-0.1.1 (c (n "tui-markup-renderer") (v "0.1.1") (d (list (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0n9ysang9l9yc45ys0w88gs1q49796g1i7df25vbasck1w4q86y5")))

(define-public crate-tui-markup-renderer-1.0.0 (c (n "tui-markup-renderer") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0a6jdbbql6kamdj498pid34zanmp2kmvr4ky9z87iia5wh8h9mp0")))

(define-public crate-tui-markup-renderer-1.0.1 (c (n "tui-markup-renderer") (v "1.0.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0ws9h62dgr0k9zz34nwnry0fmll9lh1hk0mys095nxkhjpaj1ygp")))

(define-public crate-tui-markup-renderer-1.1.0 (c (n "tui-markup-renderer") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0cgdxhz018m3yd1935qvyfccnf1m4dr7labyla0ngq4vhm57w1bq")))

(define-public crate-tui-markup-renderer-1.1.1 (c (n "tui-markup-renderer") (v "1.1.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "1sjd87q4a7b4b2fn4j4qrsk209mlhq3qijpkgvifmi5kp9idcj7v")))

