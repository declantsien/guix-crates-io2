(define-module (crates-io tu i- tui-tree-widget-table) #:use-module (crates-io))

(define-public crate-tui-tree-widget-table-0.1.0 (c (n "tui-tree-widget-table") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.23") (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1wrx9x6xsadd5dmjkhm83immlav6kka1rpbynvli2a62d2d55xcj")))

(define-public crate-tui-tree-widget-table-0.1.1 (c (n "tui-tree-widget-table") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "ratatui") (r "^0.23") (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0xdypc4wl5z2l5mwajqcnljyzh39dnxwqhby4hzghd6j64rdma03")))

