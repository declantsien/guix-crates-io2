(define-module (crates-io tu i- tui-vim-editor) #:use-module (crates-io))

(define-public crate-tui-vim-editor-0.1.0 (c (n "tui-vim-editor") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "ratatui") (r "^0.23") (d #t) (k 0)))) (h "0lp5r7ifa51yrmbypw1pp6lxl3dwywjdbh95d6r1gxfbfx9snjj4") (y #t)))

(define-public crate-tui-vim-editor-0.2.0 (c (n "tui-vim-editor") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "ratatui") (r "^0.24") (d #t) (k 0)))) (h "0y84yi4ganlf0hfy069ps7lmm89mm2d4z0x3f7wsh7c55md4982q") (y #t)))

(define-public crate-tui-vim-editor-0.3.0 (c (n "tui-vim-editor") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "ratatui") (r "^0.24") (d #t) (k 0)))) (h "07ak6k51cqpzk1f7wkkkx4rw54gqgfmjhr8avw91h67fh1l2hxqf") (y #t)))

