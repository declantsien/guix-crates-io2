(define-module (crates-io tu rl turl) #:use-module (crates-io))

(define-public crate-turl-0.1.0 (c (n "turl") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0z2c040fwb4y2v5lzd1w2nrl3hl93l8vm9yr0qyn6ly8k01hdisp")))

(define-public crate-turl-0.2.0 (c (n "turl") (v "0.2.0") (d (list (d (n "data-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1rpd6crb68zpyjn66mp525912wsv4f997k53gfyvqy8f086064qi")))

(define-public crate-turl-0.3.0 (c (n "turl") (v "0.3.0") (d (list (d (n "data-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "1qisal2pkpk7cn1lirf5kvp45awham14rnmcjh3hqkrwh42bf30b")))

