(define-module (crates-io tu ne tuneutils) #:use-module (crates-io))

(define-public crate-tuneutils-0.1.1 (c (n "tuneutils") (v "0.1.1") (d (list (d (n "bv") (r "^0.10.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "eval") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "j2534") (r "^0.1") (o #t) (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1gz59d3nznibb7w2f09gp67gv8qil3nyqvppqhx4q2iq1mgara3j") (f (quote (("windows" "j2534") ("socketcan"))))))

