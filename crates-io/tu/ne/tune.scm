(define-module (crates-io tu ne tune) #:use-module (crates-io))

(define-public crate-tune-0.1.0 (c (n "tune") (v "0.1.0") (d (list (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1hc6h74h9cfzqq1z9ylkrd7ljr221hp0hla3zlhf8qwphmn9qs8m")))

(define-public crate-tune-0.2.0 (c (n "tune") (v "0.2.0") (d (list (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1rll0hs87nma3cwpfsm89rlnad735cvgbpcj2lnsd2yqqs3jikzz")))

(define-public crate-tune-0.2.1 (c (n "tune") (v "0.2.1") (d (list (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "16bzg0kq5l4ifbdhd2s36a1ai2j4qi0zcwnk11ps7jvn48jm6yiy")))

(define-public crate-tune-0.3.0 (c (n "tune") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0gn2759v665pdbyyhhn70kvb0s10rhp8ygb3ww685gfs5rfp385z")))

(define-public crate-tune-0.4.0 (c (n "tune") (v "0.4.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0clh5iczax8hjlc0jbk1s3pdpn50hzbfa750x02hd8mp1j10fbvm")))

(define-public crate-tune-0.5.0 (c (n "tune") (v "0.5.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "0vi8d86ra6x63mwbb36lhlb4n7xql5lkyqbvgihv9pccq80flwss")))

(define-public crate-tune-0.6.0 (c (n "tune") (v "0.6.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0dr0ssp08knvwcvjl6x2qzxylhk4dk4s8grhj85919hfw7z1qm93")))

(define-public crate-tune-0.7.0 (c (n "tune") (v "0.7.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0l7knkr7zl3l7yyvybb0riybv19kmifi0vrv0zzlaxaq84vayhqr")))

(define-public crate-tune-0.8.0 (c (n "tune") (v "0.8.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0a3n6mk2nsc07x8klgizal472mb9kh8sc0wm2vf0pdc7ji5fpzzq")))

(define-public crate-tune-0.9.0 (c (n "tune") (v "0.9.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0ddw4hmnb6mk0ps2g5x22a2lq1jk9dzl0ldkmhbf2jsrn9jmzcvf")))

(define-public crate-tune-0.10.0 (c (n "tune") (v "0.10.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "05g0pvawl4wm9j1fbxbvbqv8iv4hiim9fx1avmfi8mc4ks4ydsf7")))

(define-public crate-tune-0.11.0 (c (n "tune") (v "0.11.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0w7zpap4l5a2whzb6ilrbvhp601aghg4vqbckmc3lgrnq80g8q46")))

(define-public crate-tune-0.12.0 (c (n "tune") (v "0.12.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0klbm6yz9nw3cfskzi941pckpaiiff92smqvxrda6ja03iz3c4wk")))

(define-public crate-tune-0.13.0 (c (n "tune") (v "0.13.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1ipaz0gl6v6m9bqdhs13mxarahfa5dfqbxw8vhgdqpic4r4k2zci")))

(define-public crate-tune-0.14.0 (c (n "tune") (v "0.14.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1d6cjx63pcd3vx2b4fyq4n02vx450xavhw3q1c0k6rfc0f5bqh5c")))

(define-public crate-tune-0.15.0 (c (n "tune") (v "0.15.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0khjq349s0432cmss4m5s15qds2fxpwav222nsgr5ns53xkvpzgj")))

(define-public crate-tune-0.16.0 (c (n "tune") (v "0.16.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0vcc64mk1fawrr0scbxbi66m537glmjj1k9l5a8l8bjbsr8hrxnv")))

(define-public crate-tune-0.17.0 (c (n "tune") (v "0.17.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "15wnscw7vsqfcmccmd30y3n58y35kikkszm8xiypbw2mliladsqr")))

(define-public crate-tune-0.18.0 (c (n "tune") (v "0.18.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "16dsqicw7dm7k7f2jrza9vg99dz8c8i9ylz9295rmp7c21bs7kbk")))

(define-public crate-tune-0.19.0 (c (n "tune") (v "0.19.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0i86cjc4dbd2qb5zd9f25zbzbvyz7i66ihl9hwprv803m61j1kv7")))

(define-public crate-tune-0.20.0 (c (n "tune") (v "0.20.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1k53xa37jivmhcnnlgi6c7077fbdc9819b2jbrsgp3sj1yfh7l1q")))

(define-public crate-tune-0.21.0 (c (n "tune") (v "0.21.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1clyn4x03986mn7yrbgd8791cg2nlsdlax1ggizxkxf393vnms8m")))

(define-public crate-tune-0.21.1 (c (n "tune") (v "0.21.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "17lnbhc74ycfh782zpjsq6b4vi010pvpxgqb9iny1fp8yd29szkk")))

(define-public crate-tune-0.22.0 (c (n "tune") (v "0.22.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0k6af8whk81n0cqiwil2hf9zgmw58d1nfx9xyq79lsjxaf1yfnas")))

(define-public crate-tune-0.23.0 (c (n "tune") (v "0.23.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1w5jpfpn5k240mbsrihrdvsasrh3ji0m1d06qjx84mhp1zr3x99k")))

(define-public crate-tune-0.24.0 (c (n "tune") (v "0.24.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0jp1cpjmi1nvh4chy044a9bp16n5qxzi6s7i11088lazsg2i80fq")))

(define-public crate-tune-0.25.0 (c (n "tune") (v "0.25.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1wz6b9y9z31ns30jsl97wdw071k5bnyiq9nq2pjddnc1yb7pm7mw")))

(define-public crate-tune-0.26.0 (c (n "tune") (v "0.26.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0ahxxdzr91icm72ya5jvgg3r2025mxy8k3vnafgcxxn11fbpy981")))

(define-public crate-tune-0.27.0 (c (n "tune") (v "0.27.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "08x1g3cdlhrpdbghrqgmjxazmrxpwnd202k7nnari19llf3y8zbs")))

(define-public crate-tune-0.28.0 (c (n "tune") (v "0.28.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1k6h1808782pf79aykxdxggwx36x4v071ng9ysdhhai50xfn7x1v") (r "1.56")))

(define-public crate-tune-0.29.0 (c (n "tune") (v "0.29.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "08mwdaiiaggff0vdddd2ybfvww3bmbx8924zk5sys5b5gnrqi3dx") (r "1.56")))

(define-public crate-tune-0.30.0 (c (n "tune") (v "0.30.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1i0xwr5di3d2gagdhahmlgd2s7n62kgb6pfwp71g3ai1z9q458cp") (r "1.56")))

(define-public crate-tune-0.31.0 (c (n "tune") (v "0.31.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1kqk14805pricqgpyg2fnz9c36ygi6r832j21rn1ivzxwzr39zdz") (r "1.61")))

(define-public crate-tune-0.32.0 (c (n "tune") (v "0.32.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1z2mh3r6j80a8bhypzi77j7y0b4y7lacdlp6csrhhdmy5a7fy76n") (r "1.61")))

(define-public crate-tune-0.32.1 (c (n "tune") (v "0.32.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1kjdbgnzv1r6g5jpi3f44h9k3mns990zz5v8ix35kbl6py9c3wc8") (r "1.61")))

(define-public crate-tune-0.33.0 (c (n "tune") (v "0.33.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1jlw5h54g1fn1xyxbp3lahp82mh5g7cscmygw1la8iv53pqaz43h") (r "1.61")))

(define-public crate-tune-0.34.0 (c (n "tune") (v "0.34.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1kamv8sz0nj0h1c3i0s8x0k8avqdk906dgi2wk76a81cc9cj83bp") (r "1.66")))

