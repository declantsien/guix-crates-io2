(define-module (crates-io tu iw tuiwindow) #:use-module (crates-io))

(define-public crate-tuiwindow-0.1.0 (c (n "tuiwindow") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1dd2qv79h5lywcijr216j6cad9nzlqp9vhw7bdn0cc66fmmvnjmq")))

(define-public crate-tuiwindow-0.1.1 (c (n "tuiwindow") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0b2i7akj0rcfa5n181a4zv52n0krc098glyw7xljch9nzgxr97p1")))

