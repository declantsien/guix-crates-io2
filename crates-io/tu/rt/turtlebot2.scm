(define-module (crates-io tu rt turtlebot2) #:use-module (crates-io))

(define-public crate-turtlebot2-0.1.0 (c (n "turtlebot2") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "05m3sb4vydkl5vcgsi9baj0qqm2i4zaf82awfrdgcz36787h18h5")))

(define-public crate-turtlebot2-0.1.1 (c (n "turtlebot2") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0v6yv01gfvrbrynq6jcv1870a36x89yfpjvd4apwv22igr4qi1fn")))

(define-public crate-turtlebot2-0.1.2 (c (n "turtlebot2") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0mkawj9vxm2i4asq8sf1gl3y3w7kv7q4w6bysrnhb4djkdabbwwj")))

(define-public crate-turtlebot2-0.1.3 (c (n "turtlebot2") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0r2mcrwdj06fk56yll3dw8wfm3r91v426g3awmdr508f3hijkgb2")))

(define-public crate-turtlebot2-0.1.4 (c (n "turtlebot2") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0r4fv5bpb53jw3b2fvvafid5mf2mc7sdqh0mp24yfw3lhjb1haka")))

