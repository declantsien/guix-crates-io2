(define-module (crates-io tu rt turtle-lang) #:use-module (crates-io))

(define-public crate-turtle-lang-0.1.0 (c (n "turtle-lang") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "relative-path") (r "^1.3.2") (d #t) (k 0)) (d (n "rustyline") (r "^6") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.3") (d #t) (k 0)))) (h "1wipqpcgbdrfb47p4hvyq8fbg8qc460z46ycxxi92bb2wh189h1w")))

(define-public crate-turtle-lang-0.1.1 (c (n "turtle-lang") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "relative-path") (r "^1.3.2") (d #t) (k 0)) (d (n "rustyline") (r "^6") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.3") (d #t) (k 0)))) (h "0hfk7h6mn6v6lmw07j7863ai7l12nwk8km7nq9bs9yg72bmdn4sr")))

