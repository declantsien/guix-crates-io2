(define-module (crates-io tu rt turtle) #:use-module (crates-io))

(define-public crate-turtle-0.1.0 (c (n "turtle") (v "0.1.0") (h "1wwjqhsy99b7g5vvaqkxnhkimg10k9ck9bmnfsa9w929n6wdmyyk") (y #t)))

(define-public crate-turtle-0.1.1 (c (n "turtle") (v "0.1.1") (h "0s3m0i3j8kpaipqfdzdzr3nw6z9wc344b2a8pj5r2kvk99hqsn3h") (y #t)))

(define-public crate-turtle-0.1.2 (c (n "turtle") (v "0.1.2") (h "0zpggjfb8lylhxpzmhfrwfd98b97jnarcybrfimq0sys9pms9la7") (y #t)))

(define-public crate-turtle-0.1.3 (c (n "turtle") (v "0.1.3") (h "0yvab15xj01wxvij8rrbhwww1rrv03lkbxk0dppi7z255ah9in6l") (y #t)))

(define-public crate-turtle-1.0.0-alpha.0 (c (n "turtle") (v "1.0.0-alpha.0") (d (list (d (n "fps_clock") (r "^2.0.0") (d #t) (k 0)) (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)))) (h "10icy1sx1fcy4k1l475m820sj59m1cldpag5kid23hz4rlyq7s78")))

(define-public crate-turtle-1.0.0-alpha.1 (c (n "turtle") (v "1.0.0-alpha.1") (d (list (d (n "fps_clock") (r "^2.0.0") (d #t) (k 0)) (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)))) (h "1rmm6x1b2497x3varmwpkpasa1k7765pcj38aj3ayi45dldrymfl")))

(define-public crate-turtle-1.0.0-alpha.2 (c (n "turtle") (v "1.0.0-alpha.2") (d (list (d (n "fps_clock") (r "^2.0.0") (d #t) (k 0)) (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)))) (h "1wndi7mcvbsjgjfsmxr1rvy75z76im2sbgs0ynjfym85gpzyjcjb")))

(define-public crate-turtle-1.0.0-alpha.3 (c (n "turtle") (v "1.0.0-alpha.3") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)))) (h "0sblzmxz9ycrskcj63dyvx3slazh40chc8v4i34skdf8dxbfxd0k")))

(define-public crate-turtle-1.0.0-alpha.4 (c (n "turtle") (v "1.0.0-alpha.4") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)))) (h "0gdyys4fl7g58scg92rfy8w4m9aidsvad2gl1vmyrhv5fwcfaizm") (f (quote (("test"))))))

(define-public crate-turtle-1.0.0-alpha.5 (c (n "turtle") (v "1.0.0-alpha.5") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)))) (h "0x5vsvls1k9bfkc9j8wb0w37r94xbfv4jkrp4kzvs58mf3mm7sya") (f (quote (("test"))))))

(define-public crate-turtle-1.0.0-alpha.6 (c (n "turtle") (v "1.0.0-alpha.6") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "1njj0p5ncqaq80an2fgcrif69vdnkm2z45930cxyi3w69zyj34hv") (f (quote (("test"))))))

(define-public crate-turtle-1.0.0-alpha.7 (c (n "turtle") (v "1.0.0-alpha.7") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "1ng1f8py2i0fdhylf55hbd4wls72ppfpis450n0x7l50z7c60dbm") (f (quote (("test"))))))

(define-public crate-turtle-1.0.0-alpha.8 (c (n "turtle") (v "1.0.0-alpha.8") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "1blqm12qrb9qipn9gn9zdkjniyvngk06b6pjfy4krk8v8b2zgd37") (f (quote (("test"))))))

(define-public crate-turtle-1.0.0-rc.1 (c (n "turtle") (v "1.0.0-rc.1") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "0m2b0klkshq5fwgdbk1ivjpli0ka3fprma21wlcxnijvr0q68pgi") (f (quote (("test"))))))

(define-public crate-turtle-1.0.0-rc.2 (c (n "turtle") (v "1.0.0-rc.2") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "1i1ywfild2s9dc4dn4xbkxswq897my8klnkwla09y9ryr8ngk7aj") (f (quote (("test") ("desktop" "piston_window") ("default" "desktop"))))))

(define-public crate-turtle-1.0.0-rc.3 (c (n "turtle") (v "1.0.0-rc.3") (d (list (d (n "bitvec") (r "^0.16") (d #t) (k 2)) (d (n "interpolation") (r "^0.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.105") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.6.0") (d #t) (k 0)))) (h "0j5kvvmiili5bs63w3vsjpg20rpy6bp37kb6mivrlfvrhh8yhzg4") (f (quote (("unstable") ("test"))))))

