(define-module (crates-io tu rt turtle-svg) #:use-module (crates-io))

(define-public crate-turtle-svg-0.1.0 (c (n "turtle-svg") (v "0.1.0") (d (list (d (n "svg") (r "^0.10.0") (d #t) (k 0)))) (h "0iqsnrmas24q0amivmxv6yb6is6imw722dmibnw6hnsg5ddgyjzi")))

(define-public crate-turtle-svg-0.1.1 (c (n "turtle-svg") (v "0.1.1") (d (list (d (n "svg") (r "^0.10.0") (d #t) (k 0)))) (h "0z9y72iahzv96738gm8r3xh4i70zzc0dynxv35337g0j4kkfix69")))

