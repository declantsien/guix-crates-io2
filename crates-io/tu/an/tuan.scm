(define-module (crates-io tu an tuan) #:use-module (crates-io))

(define-public crate-tuan-0.1.0 (c (n "tuan") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0ymd65vsbbwb3i2vimjnxwpzq3a4hxssjr79fcvjmvlb6cb2rmkr")))

