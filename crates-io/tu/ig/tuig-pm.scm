(define-module (crates-io tu ig tuig-pm) #:use-module (crates-io))

(define-public crate-tuig-pm-0.0.1 (c (n "tuig-pm") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1y640qfx9p4ff5c75qsqb34cmp3if1chw04gldwymjv7jd49xfbz")))

(define-public crate-tuig-pm-0.0.2 (c (n "tuig-pm") (v "0.0.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1w1s31szc403n940rrnqxxkgj5d69rvkfv379dy6ys2sl2y8cr80")))

(define-public crate-tuig-pm-0.0.3 (c (n "tuig-pm") (v "0.0.3") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0vav2krw2qmpzc2f4i5qki3kqxrkk043fwmysmqm78b7xfj474qy")))

(define-public crate-tuig-pm-0.0.4 (c (n "tuig-pm") (v "0.0.4") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0c120g94rxrd35rbksphd0librsn99kpgn2q9rgyfr8bkxwbqx34")))

(define-public crate-tuig-pm-0.0.5 (c (n "tuig-pm") (v "0.0.5") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0fqwsnd9v5ydcb830d0bjq0kk45v4015jzhj0xl4c1lzwr4pm3az") (r "1.64")))

