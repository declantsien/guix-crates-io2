(define-module (crates-io tu ig tuig-ui) #:use-module (crates-io))

(define-public crate-tuig-ui-0.0.5 (c (n "tuig-ui") (v "0.0.5") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "tuig-iosys") (r "^0.0.5") (d #t) (k 0)) (d (n "tuig-pm") (r "^0.0.5") (d #t) (k 0)))) (h "00vbab5pa0xpa2si06kks673aqm3pa90cayfzjdmfjgdz5j7zni3") (r "1.64")))

