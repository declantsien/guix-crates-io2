(define-module (crates-io tu gg tugger-binary-analysis) #:use-module (crates-io))

(define-public crate-tugger-binary-analysis-0.1.0 (c (n "tugger-binary-analysis") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "goblin") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "version-compare") (r "^0.0") (d #t) (k 0)))) (h "0j0jzddd4055241ncs4zna99npc1waf8p9vnvy8qgvzvpp7pv32k")))

(define-public crate-tugger-binary-analysis-0.2.0 (c (n "tugger-binary-analysis") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "goblin") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "version-compare") (r "^0.0") (d #t) (k 0)))) (h "199zyf03q37sqiwkfb5jn4rlp0326v4kpdnz8q2348mdnyqzxhjk")))

(define-public crate-tugger-binary-analysis-0.3.0 (c (n "tugger-binary-analysis") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "goblin") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "version-compare") (r "^0.0") (d #t) (k 0)))) (h "16pnl7hrjrjar7in7c6nx5zjsalwl7qg9n1pkrjz9cjxndsx3kq2")))

(define-public crate-tugger-binary-analysis-0.4.0 (c (n "tugger-binary-analysis") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "goblin") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "version-compare") (r "^0.1") (d #t) (k 0)))) (h "1d1ka6hfvr49yq7kkvhbq9rcn7lffmnmkyif11bz74zqksybms7c")))

(define-public crate-tugger-binary-analysis-0.5.0 (c (n "tugger-binary-analysis") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "goblin") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "version-compare") (r "^0.1") (d #t) (k 0)))) (h "1a5lxilks9mdf6jpyymg8hlxi4pkb5rz0jac53rs404qn6f37vwn")))

(define-public crate-tugger-binary-analysis-0.6.0 (c (n "tugger-binary-analysis") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "goblin") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "version-compare") (r "^0.1") (d #t) (k 0)))) (h "1zac8n7sh84isci7ijvlvmmsb03nhlz0qnwnp8pykgd9a3s32yls")))

(define-public crate-tugger-binary-analysis-0.7.0 (c (n "tugger-binary-analysis") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "goblin") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.1") (d #t) (k 0)))) (h "07z57bn6fizgcd49086gsm97yy5nnhzziqqs6zs1d0cawaganajd")))

