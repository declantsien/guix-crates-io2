(define-module (crates-io tu gg tugger-rpm) #:use-module (crates-io))

(define-public crate-tugger-rpm-0.1.0 (c (n "tugger-rpm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rpm-rs") (r "^0.6") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "tugger-file-manifest") (r "^0.1.0") (d #t) (k 0)))) (h "1ijjbr4g9jycg7dx997zxl7x51xml6b5xxyn3624r4ib6gl8al9q")))

(define-public crate-tugger-rpm-0.2.0 (c (n "tugger-rpm") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rpm-rs") (r "^0.6") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "tugger-file-manifest") (r "^0.2.0") (d #t) (k 0)))) (h "1slm8j2l8ccrpiyb31yyl0lnw9b9yfln3kwgq741y5p5mp2mjkcq")))

(define-public crate-tugger-rpm-0.3.0 (c (n "tugger-rpm") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rpm-rs") (r "^0.6") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "tugger-file-manifest") (r "^0.2.0") (d #t) (k 0)))) (h "140pqza8x3ncq5k69818f26v4wby0czx2nng1q2xpjhvcbmwcp87")))

(define-public crate-tugger-rpm-0.4.0 (c (n "tugger-rpm") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rpm-rs") (r "^0.6") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "tugger-file-manifest") (r "^0.3.0") (d #t) (k 0)))) (h "0rnnv6qv1kh6npplb69d235nvzhgv3rbmgy6gc04bgmmi35g2mc9")))

(define-public crate-tugger-rpm-0.5.0 (c (n "tugger-rpm") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rpm-rs") (r "^0.6") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "tugger-file-manifest") (r "^0.4.0") (d #t) (k 0)))) (h "1k83xq4j6bswvjxw06h5jnrb3kr3pzh0xwlkg86lc05p86an3d1c")))

(define-public crate-tugger-rpm-0.6.0 (c (n "tugger-rpm") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rpm-rs") (r "^0.6") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "tugger-file-manifest") (r "^0.5.0") (d #t) (k 0)))) (h "0xglgsm6f80hmmdncpn85ssr8hfd4rdwfhfi3pc8szmgqp6s22pm")))

