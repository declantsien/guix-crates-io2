(define-module (crates-io tu gg tugger-file-manifest) #:use-module (crates-io))

(define-public crate-tugger-file-manifest-0.1.0 (c (n "tugger-file-manifest") (v "0.1.0") (h "1pzq24rhbkvciayw7mxijn5wwhljlyjbwy2i0ixihbzci3r1ww19")))

(define-public crate-tugger-file-manifest-0.2.0 (c (n "tugger-file-manifest") (v "0.2.0") (h "129ajj572d8ghi4asrlqbbypcjq4wqnr0q3a01nxfbj7dsczppwz")))

(define-public crate-tugger-file-manifest-0.3.0 (c (n "tugger-file-manifest") (v "0.3.0") (h "0rdsxlwq4js5vzvbm5q1364c7051px7n882wcmjrsd4hxgvl3l0y")))

(define-public crate-tugger-file-manifest-0.4.0 (c (n "tugger-file-manifest") (v "0.4.0") (h "1ja9dv6bv7zrq81bi72iznwrszc8j4x34x9xdkpqw5akqbzr1y8z")))

(define-public crate-tugger-file-manifest-0.5.0 (c (n "tugger-file-manifest") (v "0.5.0") (d (list (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "15qi4sgk9ax5hv9b0fmi6vn2d7dh8xvm3bg7q4jk67ivhzl4gig8")))

(define-public crate-tugger-file-manifest-0.6.0 (c (n "tugger-file-manifest") (v "0.6.0") (d (list (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0vq2xz3dnfpdz04qjyya8wimda5slmfz02nmkw50l22hj331ms99")))

(define-public crate-tugger-file-manifest-0.7.0 (c (n "tugger-file-manifest") (v "0.7.0") (d (list (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "10r9q6s31ksfa8sqc0pk6rdwp1hdvd2pfwmyvdvi556p27dachp8")))

(define-public crate-tugger-file-manifest-0.8.0 (c (n "tugger-file-manifest") (v "0.8.0") (d (list (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1d7jy1k9gab09li2fq0zlhry3qcq6d39dq28hxnvjbqlpzmnbcxp")))

(define-public crate-tugger-file-manifest-0.9.0 (c (n "tugger-file-manifest") (v "0.9.0") (d (list (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0ibx8r3fsfllmsdi76dsz9r2h2lnl4hm5gkviacp2ajp64bg44jw")))

(define-public crate-tugger-file-manifest-0.10.0 (c (n "tugger-file-manifest") (v "0.10.0") (d (list (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1gkhxaqbxyjwa8capwawx1j668myxmr7aki8j129kdiqdmy7md2f")))

