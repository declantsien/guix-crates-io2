(define-module (crates-io tu gg tugger-licensing) #:use-module (crates-io))

(define-public crate-tugger-licensing-0.1.0 (c (n "tugger-licensing") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "spdx") (r "^0.3") (d #t) (k 0)))) (h "0k8xjz9hardd36h3ad2759frkzngpnwd9czmbr8q8qsmksnmzl5g")))

(define-public crate-tugger-licensing-0.2.0 (c (n "tugger-licensing") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "spdx") (r "^0.4") (d #t) (k 0)))) (h "1xhwsaraimr89csy369rpazb08c4k3fwzwjsb0p8rvq2xpf8p075")))

(define-public crate-tugger-licensing-0.3.0 (c (n "tugger-licensing") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "spdx") (r "^0.4") (d #t) (k 0)))) (h "04a8mvyz8ihypda775lra3c95gjzndxi7s4p9nvlgi9fva893hxq")))

(define-public crate-tugger-licensing-0.4.0 (c (n "tugger-licensing") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "spdx") (r "^0.4") (d #t) (k 0)))) (h "0sbyhr1f321dapamhfw6gas6crd5l9mjgb4rqzc6vl08x7jb6w1r")))

(define-public crate-tugger-licensing-0.5.0 (c (n "tugger-licensing") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "spdx") (r "^0.6") (d #t) (k 0)))) (h "13hz9pqs6jddq3k6ggmk1y6zkkdrmr68kmw3ipr01aing892qiz9")))

(define-public crate-tugger-licensing-0.6.0 (c (n "tugger-licensing") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "spdx") (r "^0.8") (d #t) (k 0)))) (h "0njr78n2xvpk90vdw4v7qb93a26c2vm7fcb9pknc9xaj1hwcmdmp")))

