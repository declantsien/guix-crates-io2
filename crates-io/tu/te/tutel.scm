(define-module (crates-io tu te tutel) #:use-module (crates-io))

(define-public crate-tutel-0.2.5 (c (n "tutel") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "bpaf") (r "^0.4.9") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0mmy1vwvh7m2b7irzx39flsykz01a27hzkv98db1s9kmcmq6sqx7")))

(define-public crate-tutel-0.2.6 (c (n "tutel") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "bpaf") (r "^0.4.9") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0b2ymb0k9mnizn5pjyvgcdsrcnsi7x4f0s8zxgczjdk2l2lx2dls")))

(define-public crate-tutel-0.2.7 (c (n "tutel") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "bpaf") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0z8n4dp287qy3b8qwj68cjgs0adzydqy4xww6vi84851vnpq676g")))

(define-public crate-tutel-0.2.8 (c (n "tutel") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "bpaf") (r "^0.7.1") (f (quote ("autocomplete"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0jgchy4z2v8nn8paia6rs2wz92s89hyaicsilds2bb8i5gs6iaz3")))

(define-public crate-tutel-0.2.9 (c (n "tutel") (v "0.2.9") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "bpaf") (r "^0.7.1") (f (quote ("autocomplete"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1nv6yvygvf5dam1jcva3i4qnkza7c6mx0l50a4irhnajhgylh7pb")))

(define-public crate-tutel-0.2.10 (c (n "tutel") (v "0.2.10") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "bpaf") (r "^0.7.1") (f (quote ("autocomplete"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "04xv31nfgpn8zyd9jl3bb8fkarb68qn8bz6qxg7b8w1l6zs6agnh")))

