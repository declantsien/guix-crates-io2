(define-module (crates-io tu i_ tui_selector) #:use-module (crates-io))

(define-public crate-tui_selector-0.1.0 (c (n "tui_selector") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1ayfc3yllgbi3iny4w0rnmg8bclszxpk3jb0plh5j46cpf3b4zvr")))

(define-public crate-tui_selector-0.1.1 (c (n "tui_selector") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1mgw82ry93bm7sjwgsqh9ix1z2gm8z0nncaadbkjsrzq80mldb12")))

