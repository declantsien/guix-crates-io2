(define-module (crates-io tu i_ tui_draw) #:use-module (crates-io))

(define-public crate-tui_draw-0.0.0 (c (n "tui_draw") (v "0.0.0") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "1k6f20a3q7j8m1wzld3ns8lkiddkvasj8nnirxv8k1s09mv5avk4")))

(define-public crate-tui_draw-0.1.0 (c (n "tui_draw") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "0dl0klhaqdjv1zjx0irjpdfwc2q2bsdf0k2j8mx26m55ryjl53x1")))

(define-public crate-tui_draw-0.2.0 (c (n "tui_draw") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "1zln7s4k5kpx03qv0q8k8lhnp1hn3ljqhwl55jx0lqfa23hzcs09")))

(define-public crate-tui_draw-0.2.1 (c (n "tui_draw") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "1hqqbh97jg7zigczypfqssr13cc3dc2zkhwdnxaldkd8qax5l8py")))

