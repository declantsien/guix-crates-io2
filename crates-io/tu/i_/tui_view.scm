(define-module (crates-io tu i_ tui_view) #:use-module (crates-io))

(define-public crate-tui_view-0.1.0 (c (n "tui_view") (v "0.1.0") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "1rr2izzswk5n5vhji1zyxkdx18kdfb8az4zva76agydyifxr7ca5")))

(define-public crate-tui_view-0.2.0 (c (n "tui_view") (v "0.2.0") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "1g3lmkhirqy0y3c9g7asyq4v2db82f9yddyfmban5psyn5ahdkg4")))

(define-public crate-tui_view-0.2.1 (c (n "tui_view") (v "0.2.1") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "00ldr4vkh46qjryn7qfk15qdwmyxjrbl49356xsjyqiqb3h0s2ri")))

(define-public crate-tui_view-0.2.2 (c (n "tui_view") (v "0.2.2") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "0q85cs53an4ci9q1rh4s9frd76lifyml587kqzkazs46vn5ilxc7")))

(define-public crate-tui_view-0.2.3 (c (n "tui_view") (v "0.2.3") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "16qamx2m1a0pjffnjxr2dx8lpj0rhs0q77mvc4ykpxr8fmzr96gl")))

(define-public crate-tui_view-0.2.4 (c (n "tui_view") (v "0.2.4") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.20") (d #t) (k 0)))) (h "0ai3y338ndjf4dqfzzb55cd20l44zjqc8lc3adcf9n5fm0knm08i")))

(define-public crate-tui_view-0.2.5 (c (n "tui_view") (v "0.2.5") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.20") (d #t) (k 0)))) (h "1cdkzxlnvifbf9ixprrnim86hj7l14zgirjqarkrl8b71q18hjfl")))

