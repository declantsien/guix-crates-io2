(define-module (crates-io tu i_ tui_test) #:use-module (crates-io))

(define-public crate-tui_test-0.1.0 (c (n "tui_test") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "hlua") (r "^0.4.1") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "15m0bp1byyd7dd3ck88zghrk2yr2sr744v4mkphpnr8jn1y2qi8k") (y #t)))

