(define-module (crates-io tu i_ tui_input_dialog) #:use-module (crates-io))

(define-public crate-tui_input_dialog-0.1.0 (c (n "tui_input_dialog") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "0ky2ak9ah5793zqs40b3sy161590wp2hvwzfv8g0x1b0avh098r7") (f (quote (("default" "crossterm")))) (s 2) (e (quote (("crossterm" "dep:crossterm"))))))

