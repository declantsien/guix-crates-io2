(define-module (crates-io tu i_ tui_tiling) #:use-module (crates-io))

(define-public crate-tui_tiling-0.1.0 (c (n "tui_tiling") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.67") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0w4hbd76f5i0dwvg9san34cs927wqibrq7ccmfi892n87ihp74qb")))

