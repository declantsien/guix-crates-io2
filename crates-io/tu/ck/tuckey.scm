(define-module (crates-io tu ck tuckey) #:use-module (crates-io))

(define-public crate-tuckey-0.1.0 (c (n "tuckey") (v "0.1.0") (d (list (d (n "test-case") (r "^3.3.1") (d #t) (k 0)))) (h "1fbv040vzsrncwg76b0wl6v3a88wcyi6m545gplk7ggi0zxh9vij")))

(define-public crate-tuckey-0.1.1 (c (n "tuckey") (v "0.1.1") (d (list (d (n "test-case") (r "^3.3.1") (d #t) (k 0)))) (h "0kpjkavj8vgqwackda7z6h91f30bj3l7n26qd5whjjdf6rc6hh34")))

(define-public crate-tuckey-0.1.2 (c (n "tuckey") (v "0.1.2") (d (list (d (n "test-case") (r "^3.3.1") (d #t) (k 0)))) (h "1lzd13qd4pg79k2sbz5y2wph2shlg039q3awc29jz8rsmqx23x5i")))

(define-public crate-tuckey-0.1.3 (c (n "tuckey") (v "0.1.3") (d (list (d (n "test-case") (r "^3.3.1") (d #t) (k 0)))) (h "089c1k8xq426pw4rpn9fbppkaz1x6zy9w9bvkcwn3lrbw3srkxnj")))

(define-public crate-tuckey-0.1.4 (c (n "tuckey") (v "0.1.4") (d (list (d (n "test-case") (r "^3.3.1") (d #t) (k 0)))) (h "0ivckcncnlz3g6z4xya9im27l332n6xc4vknlrkm51k6q818hrs2")))

(define-public crate-tuckey-0.1.5 (c (n "tuckey") (v "0.1.5") (d (list (d (n "test-case") (r "^3.3.1") (d #t) (k 0)))) (h "19qwx824qr3c3n0w51zfv3s8ggkc2hadq2k6l8a9ap733yl70gvh")))

(define-public crate-tuckey-0.1.6 (c (n "tuckey") (v "0.1.6") (d (list (d (n "test-case") (r "^3.3.1") (d #t) (k 0)))) (h "18rbz7qmsdfh4yczj02wj9mpiv4cmpgaf1vyxfmk5p38np7ix07v")))

(define-public crate-tuckey-0.1.7 (c (n "tuckey") (v "0.1.7") (d (list (d (n "test-case") (r "^3.3.1") (d #t) (k 0)))) (h "0sawm6y32w0fbzvv21j85zsyy9g50jgfrkaz6qa8691ih6hizdl9")))

