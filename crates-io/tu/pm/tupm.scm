(define-module (crates-io tu pm tupm) #:use-module (crates-io))

(define-public crate-tupm-0.1.0 (c (n "tupm") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "cursive") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "matches") (r "^0.1.6") (d #t) (k 2)) (d (n "multipart") (r "^0.13.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9.14") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.14") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.6.2") (d #t) (k 0)) (d (n "rpassword") (r "^0.4.0") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "1xpqbyzxz1a4jidxszi5yypj98273avb1h33xcx8ndvjz681gnkx") (f (quote (("test_database") ("default"))))))

