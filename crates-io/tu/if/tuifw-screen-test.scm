(define-module (crates-io tu if tuifw-screen-test) #:use-module (crates-io))

(define-public crate-tuifw-screen-test-0.1.8 (c (n "tuifw-screen-test") (v "0.1.8") (d (list (d (n "tuifw-screen-base") (r "^0.1.8") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1bxbgk506nvyv4mgclag609fqh07594hygdhzxpjjx3w5sfkn8ky")))

(define-public crate-tuifw-screen-test-0.1.9 (c (n "tuifw-screen-test") (v "0.1.9") (d (list (d (n "tuifw-screen-base") (r "^0.1.9") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "17n0pjz83s1k3smw537hhc89bzjvk086lh3mc7nm0icnnjjb8x4w")))

(define-public crate-tuifw-screen-test-0.1.10 (c (n "tuifw-screen-test") (v "0.1.10") (d (list (d (n "tuifw-screen-base") (r "^0.1.10") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0hmxwh98i1d6dyk6x275y67vyvbxgq85fpl425431n781yrd6pl2")))

(define-public crate-tuifw-screen-test-0.2.0 (c (n "tuifw-screen-test") (v "0.2.0") (d (list (d (n "tuifw-screen-base") (r "^0.2.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "108gf80kdd725cbsf0b2n4rsk3yzbmv23w15pc2wkcwwizryvn75")))

(define-public crate-tuifw-screen-test-0.3.0 (c (n "tuifw-screen-test") (v "0.3.0") (d (list (d (n "tuifw-screen-base") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "00bz10msrmv656ly92qma4nlh8cfj3kjq0780l16vv0pkqvpcpsa")))

(define-public crate-tuifw-screen-test-0.4.0 (c (n "tuifw-screen-test") (v "0.4.0") (d (list (d (n "tuifw-screen-base") (r "^0.4.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1px0hkwsqcpbbrc511ac35ccq837byh7fhcdqdxcc6djyl8sv016")))

(define-public crate-tuifw-screen-test-0.4.1 (c (n "tuifw-screen-test") (v "0.4.1") (d (list (d (n "tuifw-screen-base") (r "^0.4.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1bs2d2s5shhbi36xsfya7ji9x74vc9jpi85yhxbsrmdv6k1fmb9g")))

(define-public crate-tuifw-screen-test-0.5.0 (c (n "tuifw-screen-test") (v "0.5.0") (d (list (d (n "tuifw-screen-base") (r "^0.5.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0kj8458cgwh7n40h81zaflgnap7d2cy64g9krp43pj6xs21nykph")))

(define-public crate-tuifw-screen-test-0.6.0 (c (n "tuifw-screen-test") (v "0.6.0") (d (list (d (n "tuifw-screen-base") (r "^0.6.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1siidsmzw5prw9csrk6yaa607cnh0vnfayyw5a33mkdv2frzq5yg")))

(define-public crate-tuifw-screen-test-0.7.0 (c (n "tuifw-screen-test") (v "0.7.0") (d (list (d (n "tuifw-screen-base") (r "^0.7.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0046yi4l1hadnfc2avww92skyq13w6i92dzcii3mbq24q00v2xcw")))

(define-public crate-tuifw-screen-test-0.8.0 (c (n "tuifw-screen-test") (v "0.8.0") (d (list (d (n "tuifw-screen-base") (r "^0.8.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "160djkx5xhl7kr33rb0sn1j77d7vf56w6d5sprvvfgrv2915whk0")))

(define-public crate-tuifw-screen-test-0.8.1 (c (n "tuifw-screen-test") (v "0.8.1") (d (list (d (n "tuifw-screen-base") (r "^0.8.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "03jxa4dqj13ywk7ija1kkklv74mcsdv7cc694mqjlq4cb2w5mf6k")))

(define-public crate-tuifw-screen-test-0.9.0 (c (n "tuifw-screen-test") (v "0.9.0") (d (list (d (n "tuifw-screen-base") (r "^0.9.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0bm6z61s0yvd9wg7rvlgd3d791aqvp9cgv74iv9i5xgix6ij6ajc")))

(define-public crate-tuifw-screen-test-0.10.0 (c (n "tuifw-screen-test") (v "0.10.0") (d (list (d (n "errno-no-std") (r "^0.0.3") (k 0)) (d (n "tuifw-screen-base") (r "^0.10.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0h63306zqjbrv7h6dbpp512sgbnbv5qfhi4bam17s8aqfvl78xia")))

(define-public crate-tuifw-screen-test-0.11.0 (c (n "tuifw-screen-test") (v "0.11.0") (d (list (d (n "errno-no-std") (r "^0.0.4") (k 0)) (d (n "tuifw-screen-base") (r "^0.11.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0wqzjwa645r0v7p8nyzn52dx0qpj5lvrkm77nwd6wsqq9i9m2z2h")))

(define-public crate-tuifw-screen-test-0.12.0 (c (n "tuifw-screen-test") (v "0.12.0") (d (list (d (n "errno-no-std") (r "^0.1.0") (k 0)) (d (n "tuifw-screen-base") (r "^0.12.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1g1549c3j08qw8yvklnynx2lja7qz1lpfh2c002qd2pcjy5khkrd")))

(define-public crate-tuifw-screen-test-0.12.1 (c (n "tuifw-screen-test") (v "0.12.1") (d (list (d (n "errno-no-std") (r "^0.1.0") (k 0)) (d (n "tuifw-screen-base") (r "^0.12.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1jzixldaq6wpyrdg8d7nk5jd6pgxqiw2j79vsk2sp7skggr0b9j5")))

(define-public crate-tuifw-screen-test-0.12.2 (c (n "tuifw-screen-test") (v "0.12.2") (d (list (d (n "errno-no-std") (r "^0.1.0") (k 0)) (d (n "tuifw-screen-base") (r "^0.12.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1gzqbfqnm5sw2zaqsbbylaxadmzl8xb4lc3vbgcpcp5nnx6lpkyc")))

(define-public crate-tuifw-screen-test-0.12.3 (c (n "tuifw-screen-test") (v "0.12.3") (d (list (d (n "errno-no-std") (r "^0.1.0") (k 0)) (d (n "tuifw-screen-base") (r "^0.12.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0060cyw9wg8fdzplpz1pwzywy0cd6dfx4cn8y0i9qxnqi4s6wlgj")))

(define-public crate-tuifw-screen-test-0.12.4 (c (n "tuifw-screen-test") (v "0.12.4") (d (list (d (n "errno-no-std") (r "^0.1.0") (k 0)) (d (n "tuifw-screen-base") (r "^0.12.4") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1rpyx9nc7f183qiq414biyjc7lip44r6qspdvi84pn3cd6s8fkig")))

(define-public crate-tuifw-screen-test-0.13.0 (c (n "tuifw-screen-test") (v "0.13.0") (d (list (d (n "errno-no-std") (r "^0.1.0") (k 0)) (d (n "tuifw-screen-base") (r "^0.13.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0qjqi4zq6vd75blkq3awff7y76pr7wrfp6gqfviybgqziggyaxhj")))

(define-public crate-tuifw-screen-test-0.13.3 (c (n "tuifw-screen-test") (v "0.13.3") (d (list (d (n "errno-no-std") (r "^0.1.0") (k 0)) (d (n "tuifw-screen-base") (r "^0.13.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1wg7cfca7ypk7jrgadrnykijiklcdry1nqx3rxgbfh5b90fzbl4w")))

(define-public crate-tuifw-screen-test-0.13.4 (c (n "tuifw-screen-test") (v "0.13.4") (d (list (d (n "errno-no-std") (r "^0.1.0") (k 0)) (d (n "tuifw-screen-base") (r "^0.13.4") (d #t) (k 0)))) (h "0zfb8nbbhcqg3dqnw45xpi3pwkhlhhryq9k29hz6l9ac2jj1gzvp")))

(define-public crate-tuifw-screen-test-0.13.5 (c (n "tuifw-screen-test") (v "0.13.5") (d (list (d (n "errno-no-std") (r "^0.1.0") (k 0)) (d (n "tuifw-screen-base") (r "^0.13.5") (d #t) (k 0)))) (h "07c1m8n940wzqhfnawfq5ngpl3dkffcgdwmyh6kjfpvhpvzrgk62")))

(define-public crate-tuifw-screen-test-0.14.0 (c (n "tuifw-screen-test") (v "0.14.0") (d (list (d (n "errno-no-std") (r "^0.1.0") (k 0)) (d (n "tuifw-screen-base") (r "^0.14.0") (d #t) (k 0)))) (h "19ai5758mjdyac9gihxy7j1jfwnjkj1ll1cq24755bk6l9zmxwgf")))

(define-public crate-tuifw-screen-test-0.14.1 (c (n "tuifw-screen-test") (v "0.14.1") (d (list (d (n "errno-no-std") (r "^0.1.0") (k 0)) (d (n "tuifw-screen-base") (r "^0.14.1") (d #t) (k 0)))) (h "1ip07sccj6k0r95k2jx5jxaa9qm7isxiiky90a3z34cnw8i06chg")))

(define-public crate-tuifw-screen-test-0.15.0 (c (n "tuifw-screen-test") (v "0.15.0") (d (list (d (n "errno-no-std") (r "^0.1.5") (k 0)) (d (n "tuifw-screen-base") (r "^0.15.0") (d #t) (k 0)))) (h "0l0igm9gfgkf91bd4zip0jvy7xw3lsbc7xaf98xbd1zmkmi4yypr")))

(define-public crate-tuifw-screen-test-0.15.1 (c (n "tuifw-screen-test") (v "0.15.1") (d (list (d (n "errno-no-std") (r "^0.1.5") (k 0)) (d (n "tuifw-screen-base") (r "^0.15.1") (d #t) (k 0)))) (h "0brmrxs0sn424iyw6366qy7aip6dcqhww70wzvyfmihm0k42qmzv")))

(define-public crate-tuifw-screen-test-0.15.2 (c (n "tuifw-screen-test") (v "0.15.2") (d (list (d (n "errno-no-std") (r "^0.1.5") (k 0)) (d (n "tuifw-screen-base") (r "^0.15.2") (d #t) (k 0)))) (h "0nl9rlkcxsh0iwzkbvllpf4kzfx1fmgb7722hpkz2nh762gjcl5k")))

(define-public crate-tuifw-screen-test-0.16.0 (c (n "tuifw-screen-test") (v "0.16.0") (d (list (d (n "tuifw-screen-base") (r "^0.16.0") (d #t) (k 0)))) (h "1sl8f7rr6lr3hkphmf337xyqfrn6zzjiiskjs4l92cz7f5pzyxi1")))

(define-public crate-tuifw-screen-test-0.16.1 (c (n "tuifw-screen-test") (v "0.16.1") (d (list (d (n "tuifw-screen-base") (r "^0.16.1") (d #t) (k 0)))) (h "0fzp7xw32dz62slqidk85k7jmasknfnj63pl72y09r5l3m63c7hf")))

(define-public crate-tuifw-screen-test-0.16.2 (c (n "tuifw-screen-test") (v "0.16.2") (d (list (d (n "tuifw-screen-base") (r "^0.16.2") (d #t) (k 0)))) (h "0604mv6b1mzvih9xkw3c29scs1fvqisb5ygi426jijhzi5gyil5k")))

(define-public crate-tuifw-screen-test-0.16.3 (c (n "tuifw-screen-test") (v "0.16.3") (d (list (d (n "tuifw-screen-base") (r "^0.16.3") (d #t) (k 0)))) (h "0l4dk4m75cvz400n2q3hmpaf7jq4g7j5g0fhqi829lwid6q4nhdx")))

(define-public crate-tuifw-screen-test-0.17.0 (c (n "tuifw-screen-test") (v "0.17.0") (d (list (d (n "tuifw-screen-base") (r "^0.17.0") (d #t) (k 0)))) (h "19ki84id7lyl4sg9h5ybzna69h46jwzdbb5jrh3hqccm4lfxavw8")))

(define-public crate-tuifw-screen-test-0.17.1 (c (n "tuifw-screen-test") (v "0.17.1") (d (list (d (n "tuifw-screen-base") (r "^0.17.1") (d #t) (k 0)))) (h "0ksqvd26fshbqnskvlfgalngcx3qspiap5rhyvvpq6bghppcd6cj")))

(define-public crate-tuifw-screen-test-0.17.2 (c (n "tuifw-screen-test") (v "0.17.2") (d (list (d (n "tuifw-screen-base") (r "^0.17.2") (d #t) (k 0)))) (h "1d0k1rsj34hcm7jjxwsy5zzmicc11a5aqi239p4vh1r4ls0ix6wz")))

(define-public crate-tuifw-screen-test-0.18.0 (c (n "tuifw-screen-test") (v "0.18.0") (d (list (d (n "tuifw-screen-base") (r "^0.18.0") (d #t) (k 0)))) (h "0ixm6dq0xid93g6lr7s7dvscavf3fcpglhwpvgb2k97mgzqbz85a")))

(define-public crate-tuifw-screen-test-0.18.1 (c (n "tuifw-screen-test") (v "0.18.1") (d (list (d (n "tuifw-screen-base") (r "^0.18.1") (d #t) (k 0)))) (h "1j834z46dzj8p6ypa4qq0zjlnkjz05b1736nxk0qv1bbwmazk94b")))

(define-public crate-tuifw-screen-test-0.18.2 (c (n "tuifw-screen-test") (v "0.18.2") (d (list (d (n "tuifw-screen-base") (r "^0.18.2") (d #t) (k 0)))) (h "03jp4pkadpk8nr21mh9fdbyb20gxhlyfwq4d91xvkmbab03fn6m2")))

(define-public crate-tuifw-screen-test-0.19.0 (c (n "tuifw-screen-test") (v "0.19.0") (d (list (d (n "tuifw-screen-base") (r "^0.19.0") (d #t) (k 0)))) (h "1dsn2095p2lipirajikzk1cfvxh2j2jpb2x8mgdqx5y779bhh7sd")))

(define-public crate-tuifw-screen-test-0.19.1 (c (n "tuifw-screen-test") (v "0.19.1") (d (list (d (n "tuifw-screen-base") (r "^0.19.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0mp72r4vmli6hhvilgbf7c45sh850g9lf7d69cbl0gi1n3gn6777")))

(define-public crate-tuifw-screen-test-0.20.0 (c (n "tuifw-screen-test") (v "0.20.0") (d (list (d (n "tuifw-screen-base") (r "^0.20.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0j6f8mmmvnjgji14iwdllmjg3agmjg69j3hb30wrqlrvqr574isq")))

(define-public crate-tuifw-screen-test-0.21.0 (c (n "tuifw-screen-test") (v "0.21.0") (d (list (d (n "tuifw-screen-base") (r "^0.21.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "192knsz4xd1r04a7n0ml2kqxp7y01f3bkwzfcgr05li7086lhssk")))

(define-public crate-tuifw-screen-test-0.21.1 (c (n "tuifw-screen-test") (v "0.21.1") (d (list (d (n "tuifw-screen-base") (r "^0.21.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0b1prrml48z8dn2w3s94sybbwjfqprksrn1pkwg1z8vg9n9x8qyr")))

(define-public crate-tuifw-screen-test-0.22.0 (c (n "tuifw-screen-test") (v "0.22.0") (d (list (d (n "tuifw-screen-base") (r "^0.22.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "16yir2ig3j15jqpr783hnb5p6cviamlbi4nwhzi1lghjfsa41b5k")))

(define-public crate-tuifw-screen-test-0.23.0 (c (n "tuifw-screen-test") (v "0.23.0") (d (list (d (n "tuifw-screen-base") (r "^0.23.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0sh5x2p2944jakdih3pmwdrhqcbvf18ax19pgj3ilqxp85481d1b")))

(define-public crate-tuifw-screen-test-0.24.0 (c (n "tuifw-screen-test") (v "0.24.0") (d (list (d (n "tuifw-screen-base") (r "^0.24.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0p4sndc6rvq9cifl2dapk5jddb2wfzfkdvxvgndrfh5wvr8d415s")))

