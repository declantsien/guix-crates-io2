(define-module (crates-io tu ke tukey_test) #:use-module (crates-io))

(define-public crate-tukey_test-0.1.0 (c (n "tukey_test") (v "0.1.0") (h "0nggsx8yb2w1gxgwlbjdvmm1j6zhx5vlzg0q4gnvgcvaf6k47a38") (y #t)))

(define-public crate-tukey_test-0.1.1 (c (n "tukey_test") (v "0.1.1") (h "16z4nxprbkwjwbq9r4pdzdvv0iavz9bmvsvql5sd8qv0l8idzb70")))

