(define-module (crates-io tu nd tundra) #:use-module (crates-io))

(define-public crate-tundra-0.1.0 (c (n "tundra") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (f (quote ("unstable-rendered-line-info"))) (d #t) (k 0)))) (h "1kkw0lqvfzzld75z8nn74m8gx94q0fz7ilsj29w2dmjrgwk333fv")))

(define-public crate-tundra-0.2.0 (c (n "tundra") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (f (quote ("unstable-rendered-line-info"))) (d #t) (k 0)))) (h "1xs91g5bxx0f2q1lpbw5xa5vmh1v1wq7i4s9cpchv2l3xsvqly2z")))

