(define-module (crates-io tu nd tundevice) #:use-module (crates-io))

(define-public crate-tundevice-0.1.0 (c (n "tundevice") (v "0.1.0") (d (list (d (n "async-dup") (r "^1") (d #t) (k 0)) (d (n "blocking") (r "^1.0.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "flume") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "1vs27w3mpldawcxxa7rpiyn97c3cg8jmkzk1rbqk292jpxk9l1x3")))

(define-public crate-tundevice-0.1.1 (c (n "tundevice") (v "0.1.1") (d (list (d (n "async-dup") (r "^1") (d #t) (k 0)) (d (n "blocking") (r "^1.0.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "flume") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "0jg6nmf0y5f2k2pb1fnpwaxx37zv0q3q5p8klgrdwrwjq45vmxn3")))

(define-public crate-tundevice-0.1.2 (c (n "tundevice") (v "0.1.2") (d (list (d (n "async-dup") (r "^1") (d #t) (k 0)) (d (n "blocking") (r "^1.0.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "flume") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "19kqdh1qc5b3559j94rrvgwzb4v3hlxi324cxdp3zwzkyhmad24p")))

(define-public crate-tundevice-0.1.3 (c (n "tundevice") (v "0.1.3") (d (list (d (n "async-dup") (r "^1") (d #t) (k 0)) (d (n "blocking") (r "^1.0.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "flume") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "1y2hapbmrld4npr298q27hn78f8y6w2q8c09p0gb71vpw62r1phd")))

(define-public crate-tundevice-0.1.4 (c (n "tundevice") (v "0.1.4") (d (list (d (n "async-dup") (r "^1") (d #t) (k 0)) (d (n "blocking") (r "^1.0.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "flume") (r "^0.10.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "0igbmqj16p52jhqrxz64f7ww5jkzagg2wza7l621y5xmhxgnwmak")))

