(define-module (crates-io tu sh tushare) #:use-module (crates-io))

(define-public crate-tushare-0.1.0 (c (n "tushare") (v "0.1.0") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "polars") (r "^0.39.2") (f (quote ("lazy" "json"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "176j8j02h9j10m99g7wx0zv7cd8xaf1ygdm0l647bny3ik0hzhls")))

(define-public crate-tushare-0.1.1 (c (n "tushare") (v "0.1.1") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "polars") (r "^0.39.2") (f (quote ("lazy" "json"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "01xy8pqqambm29nydg9hn7rq9090jg22szkims6cfvml2rwgfl7g")))

