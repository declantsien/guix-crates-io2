(define-module (crates-io tu do tudo) #:use-module (crates-io))

(define-public crate-tudo-0.1.0 (c (n "tudo") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "inquire") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16wfic6pcinmirva64xqmcz3i83gjgwllsw2dl4mj9i72pbhlbli")))

