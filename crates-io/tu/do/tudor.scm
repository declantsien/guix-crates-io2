(define-module (crates-io tu do tudor) #:use-module (crates-io))

(define-public crate-tudor-0.0.1 (c (n "tudor") (v "0.0.1") (h "1y3axqg33i5pnb3z53b7vi22h9jy3xgaazjyb4fnn0zgg57qnkla")))

(define-public crate-tudor-0.0.2 (c (n "tudor") (v "0.0.2") (h "1ql9r4p9bbvzgpgjv43adjkiw2qnxis3l77plpsf296l2d0y7k2k")))

(define-public crate-tudor-0.0.3 (c (n "tudor") (v "0.0.3") (h "19k2i9dlw35vbxh6pz4gdfny799ja6vfm3v98jkhw8dcz8hjq0wg")))

