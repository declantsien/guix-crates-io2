(define-module (crates-io tu rb turbocafe) #:use-module (crates-io))

(define-public crate-turbocafe-0.0.0 (c (n "turbocafe") (v "0.0.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (f (quote ("std" "multihash-impl" "blake3"))) (k 0)))) (h "1vxmn5p0ss06wjbh13142221mgdzwbxrvwi12jm1sbn8rrhh7p70")))

(define-public crate-turbocafe-0.0.1 (c (n "turbocafe") (v "0.0.1") (d (list (d (n "bs58") (r "^0.4") (d #t) (k 0)) (d (n "multihash") (r "^0.13") (f (quote ("std" "multihash-impl" "blake3"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "turbosql") (r "^0.1") (d #t) (k 0)))) (h "0p0jx3vsns1hql88krfhff5n01iic4rdjm9wx2kmqdg8mqpsyjsx")))

