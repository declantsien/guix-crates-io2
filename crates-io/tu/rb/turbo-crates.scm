(define-module (crates-io tu rb turbo-crates) #:use-module (crates-io))

(define-public crate-turbo-crates-0.0.0 (c (n "turbo-crates") (v "0.0.0") (h "1rjz878yl3sqvlb5czqj4fg4l877ibkmx9z870ridfcm7smbhlfy")))

(define-public crate-turbo-crates-0.0.1 (c (n "turbo-crates") (v "0.0.1") (d (list (d (n "turbo-crates-testing-proc-macros") (r "^0.0.1") (d #t) (k 0)))) (h "14fy8w1xhimdzr2gs5z6h7yckmix4j721qxklndkwsv28zssiv6l")))

(define-public crate-turbo-crates-0.0.2 (c (n "turbo-crates") (v "0.0.2") (d (list (d (n "turbo-crates-testing-proc-macros") (r "^0.0.2") (d #t) (k 0)))) (h "1562vv8y8nyj9bp3g47sh2d6xs7qi1mg0q52i04y33c66j3sb6wz")))

(define-public crate-turbo-crates-0.0.3 (c (n "turbo-crates") (v "0.0.3") (d (list (d (n "turbo-crates-testing-proc-macros") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "0735x76jw34xpqcj42gz37wsvgb6qhgxni6yg32jgbyjnfckxpvd") (f (quote (("testing" "testing-proc_macros") ("full" "testing") ("default" "full")))) (s 2) (e (quote (("testing-proc_macros" "dep:turbo-crates-testing-proc-macros"))))))

