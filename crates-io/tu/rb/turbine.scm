(define-module (crates-io tu rb turbine) #:use-module (crates-io))

(define-public crate-turbine-0.0.1 (c (n "turbine") (v "0.0.1") (h "1v037slwbx82hd20jc0nbrn9aqj43v0m1l89szgv1d4s31yg27qw")))

(define-public crate-turbine-0.0.2 (c (n "turbine") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "camera_controllers") (r "^0.23.0") (d #t) (k 0)) (d (n "conrod") (r "^0.56.0") (d #t) (k 0)) (d (n "find_folder") (r "^0.3.0") (d #t) (k 0)) (d (n "gfx") (r "^0.16.1") (d #t) (k 0)) (d (n "gfx_debug_draw") (r "^0.19.0") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.14.4") (d #t) (k 0)) (d (n "gfx_text") (r "^0.20.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "piston_meta") (r "^0.29.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.46.0") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.1") (d #t) (k 0)))) (h "0kiis9pkl48z946xklsdqncm6sanbnpb80sdvqdlwydabrhhmcpg")))

