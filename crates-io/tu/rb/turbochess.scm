(define-module (crates-io tu rb turbochess) #:use-module (crates-io))

(define-public crate-turbochess-0.1.0 (c (n "turbochess") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "term-table") (r "^1.3.2") (d #t) (k 0)))) (h "11386c7ygj6g0w5y9c383cxh8lk9g5rj51nkkfarc8appg27wxh0")))

(define-public crate-turbochess-0.2.0 (c (n "turbochess") (v "0.2.0") (h "0h7x791shj21cjn3xni8wa13npq910mp3qk5p7ibpi138y89s7wj")))

