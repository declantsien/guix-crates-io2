(define-module (crates-io tu rb turboscan) #:use-module (crates-io))

(define-public crate-turboscan-0.1.0 (c (n "turboscan") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1nvky5ydmh4m8h4kia4qzbkg67a1mzir0vw82lxnnz2klvpvw9x7")))

(define-public crate-turboscan-0.1.1 (c (n "turboscan") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0h984znb9vsx6zlm4xdg0sg99j1jvgkkg88b8d6dgh4pcikddai1")))

(define-public crate-turboscan-0.1.2 (c (n "turboscan") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "18zgzhjb0wpbl4y555cd7znr7xmp58kf73aqyqm5anp12aiscz3q")))

(define-public crate-turboscan-0.1.3 (c (n "turboscan") (v "0.1.3") (d (list (d (n "clap") (r "^4.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1i8v43a39wrf1kfv4cwdh25vpvmcq6djg2j1ncpnhil7q9hgy63n")))

(define-public crate-turboscan-0.1.4 (c (n "turboscan") (v "0.1.4") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0zgcyi5b40kafcv4gm9f77cy9fkar7qfr79ziamav03lza2wlxr1")))

