(define-module (crates-io tu rb turbogrep) #:use-module (crates-io))

(define-public crate-turbogrep-0.1.0 (c (n "turbogrep") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "0nfdb0b4jcg2dp6vdxbp62fdmss31jd6cksxk42rhf0yvar0339d")))

(define-public crate-turbogrep-0.2.0 (c (n "turbogrep") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "01v4hs4780mzg2c3yalmrprg0zbjj61iily4v2zc3b5r61i9pcw8")))

(define-public crate-turbogrep-0.3.0 (c (n "turbogrep") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "0ac42kicdm7f0xsydl4d5wsinqcd7r9brrf6myr4kqrw1vm8x44r")))

