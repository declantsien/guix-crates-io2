(define-module (crates-io tu rb turbo_atlas_icons) #:use-module (crates-io))

(define-public crate-turbo_atlas_icons-0.1.0 (c (n "turbo_atlas_icons") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("jpeg" "tga" "serialize"))) (d #t) (k 0)) (d (n "bevy_asset_loader") (r "^0.20.1") (f (quote ("progress_tracking" "2d" "standard_dynamic_assets"))) (d #t) (k 0)))) (h "1nzf5j8516h9ap198y207lf8vi8b6hi9bpln1lxiikyvin734f2c")))

