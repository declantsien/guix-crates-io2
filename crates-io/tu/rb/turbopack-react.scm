(define-module (crates-io tu rb turbopack-react) #:use-module (crates-io))

(define-public crate-turbopack-react-0.0.0 (c (n "turbopack-react") (v "0.0.0") (h "07c9kjp5ix1ca41r5zv46c2almabfzl9b6hvsgrcbyb042aavhx0") (y #t)))

(define-public crate-turbopack-react-1.0.0 (c (n "turbopack-react") (v "1.0.0") (h "1sxzb3bw5l3vnmbl7lr43rvav4baibaqdqb6m03g7b42d5biwjp4") (y #t)))

