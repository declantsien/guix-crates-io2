(define-module (crates-io tu rb turbonone) #:use-module (crates-io))

(define-public crate-turbonone-0.1.0 (c (n "turbonone") (v "0.1.0") (h "0qhqn2ihm1i6dj25fpw2yw6xm0hvai0zinc3qkgws1m01017i3gv") (y #t)))

(define-public crate-turbonone-0.1.1 (c (n "turbonone") (v "0.1.1") (h "084zhpn7n2zd4dfr02i0480rxij46axb0srgzrb6qwviymngyr5f") (y #t)))

(define-public crate-turbonone-0.1.2 (c (n "turbonone") (v "0.1.2") (h "1kapm6kb4ilfbbdhkwfqlp932xxr9v1xnxbg7gmlpnlrr7bh2s28") (y #t)))

(define-public crate-turbonone-0.1.3 (c (n "turbonone") (v "0.1.3") (h "16zg0i8jmzv8j8j0p6hwgy1p607l4ivfb3gbmbzxv96hrc3rh3f9")))

(define-public crate-turbonone-0.2.0 (c (n "turbonone") (v "0.2.0") (h "13jmbfz4yvy2hm94whsd5ci3ivgvncw9v5f3picijk7nhznlz9p6")))

(define-public crate-turbonone-0.2.1 (c (n "turbonone") (v "0.2.1") (h "0cjvsccyi9zf85a7jqvx04c5ajqdk8mi7v668nhhj4dr2many8q1")))

