(define-module (crates-io tu rb turbofish) #:use-module (crates-io))

(define-public crate-turbofish-0.0.0 (c (n "turbofish") (v "0.0.0") (d (list (d (n "futures") (r "^0.3.6") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13.8") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.1") (d #t) (k 0)))) (h "11bb9krfwj9qh1xrkmqy96l6pmjgb5680i6zvnflhr9vz7b6mr4y")))

