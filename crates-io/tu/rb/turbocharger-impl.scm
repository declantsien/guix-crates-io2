(define-module (crates-io tu rb turbocharger-impl) #:use-module (crates-io))

(define-public crate-turbocharger-impl-0.0.0 (c (n "turbocharger-impl") (v "0.0.0") (h "0crw6177wja4yypp6j4gn2dn1ycn4r1lppa7fljn03l8rrs6chbp")))

(define-public crate-turbocharger-impl-0.1.0 (c (n "turbocharger-impl") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0lzv2xip32brsxgn14nm3fmrhbd6858hymfb6n22165v7r5dzj7j")))

(define-public crate-turbocharger-impl-0.2.0 (c (n "turbocharger-impl") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1jm7viwgc1ra34fybp4pw3hb6p7d1zgb81hhywswz5n801cpjj7z") (r "1.56")))

(define-public crate-turbocharger-impl-0.3.0 (c (n "turbocharger-impl") (v "0.3.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "todo-or-die") (r "^0.1") (f (quote ("crate"))) (d #t) (k 2)))) (h "069q8r9pv28rn9yiz6n1zm6f40yjw33xxqhs3nm2apm5rbjv30ry") (f (quote (("svelte") ("full" "svelte")))) (r "1.56")))

(define-public crate-turbocharger-impl-0.4.0 (c (n "turbocharger-impl") (v "0.4.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "todo-or-die") (r "^0.1") (f (quote ("crate"))) (d #t) (k 2)))) (h "1p899kigq1daa0didniz9m41bsizpbgf3sdn55yvamfdadmnfrqm") (f (quote (("svelte") ("full" "svelte") ("debug_expansions")))) (r "1.62")))

