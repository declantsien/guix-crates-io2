(define-module (crates-io tu rb turboshake) #:use-module (crates-io))

(define-public crate-turboshake-0.1.0 (c (n "turboshake") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "018x7z5j67yc9r723z676qr4qpr87w51w9qbn9jxv5r9bcdibdr7")))

(define-public crate-turboshake-0.1.1 (c (n "turboshake") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1kmxlib6pzx45602qwk9741s9p4xhydhhmir9nr79wf87wvmm27m")))

(define-public crate-turboshake-0.1.2 (c (n "turboshake") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1dzqc2qndr3dprzbyb5q55lnwa2plcr6vs00w0flpjls2n62zdwb")))

(define-public crate-turboshake-0.1.3 (c (n "turboshake") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0n1113v1m51al7b08bxbcf5cm3kpdz80rf0gigzz3yf2x0y79xyd") (f (quote (("dev"))))))

(define-public crate-turboshake-0.1.4 (c (n "turboshake") (v "0.1.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1cvl8v2bapqjih75p88pz32ry43p7da2kmhnb5f3p8hd5irxd3nr") (f (quote (("dev"))))))

(define-public crate-turboshake-0.1.5 (c (n "turboshake") (v "0.1.5") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1i97p0kq551crlgjpbdkpi7g8yhi1ll9jxzz6g4dh0md0visbr6a") (f (quote (("simdx4") ("simdx2") ("dev"))))))

(define-public crate-turboshake-0.1.6 (c (n "turboshake") (v "0.1.6") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1cl6pwnv00ixpsxf863aycph8nv8x4095bhcfwcxalph452ivnrf") (f (quote (("simdx4") ("simdx2") ("dev"))))))

(define-public crate-turboshake-0.1.7 (c (n "turboshake") (v "0.1.7") (d (list (d (n "criterion") (r "=0.5.1") (d #t) (k 2)) (d (n "crunchy") (r "=0.2.2") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)) (d (n "test-case") (r "=3.2.1") (d #t) (k 2)))) (h "058p3ypm8byykn0b3dg84dnyrhir24brl0xaavfmbmihfvphwnkg") (f (quote (("simdx4") ("simdx2") ("dev"))))))

(define-public crate-turboshake-0.1.8 (c (n "turboshake") (v "0.1.8") (d (list (d (n "criterion") (r "=0.5.1") (d #t) (k 2)) (d (n "crunchy") (r "=0.2.2") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)) (d (n "test-case") (r "=3.2.1") (d #t) (k 2)))) (h "1za0lsl5vg9s9aczf791z70wg2d1z5c8fj1qll0lciqq9jic5ik2") (f (quote (("simdx4") ("simdx2") ("dev"))))))

(define-public crate-turboshake-0.1.9 (c (n "turboshake") (v "0.1.9") (d (list (d (n "criterion") (r "=0.5.1") (d #t) (k 2)) (d (n "crunchy") (r "=0.2.2") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)) (d (n "test-case") (r "=3.2.1") (d #t) (k 2)))) (h "1vlwdjvckhb5z0zmvh86rqrgmxdbsan5267k755nsianz9q6gi94") (f (quote (("simdx4") ("simdx2") ("dev"))))))

(define-public crate-turboshake-0.2.0 (c (n "turboshake") (v "0.2.0") (d (list (d (n "criterion") (r "=0.5.1") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "=0.6.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\", target_arch = \"aarch64\", target_arch = \"loongarch64\"))") (k 2)) (d (n "crunchy") (r "=0.2.2") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)) (d (n "test-case") (r "=3.2.1") (d #t) (k 2)))) (h "1cs984m453d8hb3cxrkw0nr59lfczky1mgsb724h1iz11mq9vpsk") (f (quote (("simdx4") ("simdx2") ("dev"))))))

