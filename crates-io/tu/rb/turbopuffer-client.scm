(define-module (crates-io tu rb turbopuffer-client) #:use-module (crates-io))

(define-public crate-turbopuffer-client-0.0.1 (c (n "turbopuffer-client") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.12.2") (f (quote ("gzip" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1razbv323lliwkgkrii297n1zpw0yd7i2vi7gl60867dirx7jdmw")))

(define-public crate-turbopuffer-client-0.0.2 (c (n "turbopuffer-client") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "reqwest") (r "^0.12") (f (quote ("gzip" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0v5xnnjc2fsvwcnv2azvrl77j0400dys9706ll2cflh8lrppff98")))

(define-public crate-turbopuffer-client-0.0.3 (c (n "turbopuffer-client") (v "0.0.3") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "reqwest") (r "^0.12") (f (quote ("gzip" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0wd2n84196nbk2rv153dz3jn96qsybm4bz6d21igb9vf6v429za2")))

