(define-module (crates-io tu rb turbo) #:use-module (crates-io))

(define-public crate-turbo-0.1.0 (c (n "turbo") (v "0.1.0") (h "02m43ahznnv606248dmz6zzjfr6fcrkb3n6slmwg9wympcp3w6xm")))

(define-public crate-turbo-0.2.0 (c (n "turbo") (v "0.2.0") (d (list (d (n "finite-fields") (r "^0.7.7") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0rir34al6b7q1fbkq3agifg98shxg8pd3vhd08fkxd6dlgp6hzqg")))

(define-public crate-turbo-0.2.1 (c (n "turbo") (v "0.2.1") (d (list (d (n "finite-fields") (r "^0.7.8") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "04nh3x2iiaky1vq171hfzlffk95p4s5lga8azwqw1gmv38ghwkfr")))

(define-public crate-turbo-0.2.2 (c (n "turbo") (v "0.2.2") (d (list (d (n "finite-fields") (r "^0.8.0") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0ysy068kaq7q4glba82alfk8ra3qwj7l3kwgawk7pmg6a6jrx3dv")))

(define-public crate-turbo-0.3.0 (c (n "turbo") (v "0.3.0") (d (list (d (n "daggy") (r "^0.4.1") (d #t) (k 0)) (d (n "finite-fields") (r "^0.9.5") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "01003a6zrqyayz2jsf8z1xkns3sr1f1pry8rga5qk3zzw8m97yn9") (f (quote (("nightly"))))))

(define-public crate-turbo-0.4.2 (c (n "turbo") (v "0.4.2") (d (list (d (n "daggy") (r "^0.4.1") (d #t) (k 0)) (d (n "finite-fields") (r "^0.10.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0dmvwr93gmf8b6hzx3nnsqfmq9fwzx43n78q7bmj6vrjqfv58bp5")))

