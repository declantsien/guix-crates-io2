(define-module (crates-io tu rb turbosloth) #:use-module (crates-io))

(define-public crate-turbosloth-0.1.0 (c (n "turbosloth") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "hotwatch") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "rt-threaded"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "turbosloth-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "0qipgl7i8s14iizbcjq2a5d8zj7dfrlyk35a79v48q3f7csdcvkg")))

(define-public crate-turbosloth-0.2.0 (c (n "turbosloth") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "hotwatch") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "rt-threaded"))) (d #t) (k 2)) (d (n "twox-hash") (r "^1.5") (k 0)))) (h "0k3qvja739w4sm3iqg4rbxk0mwqdfw997yzbjwrr80jl3pmm5xn4")))

