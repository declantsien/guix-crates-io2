(define-module (crates-io tu rb turbo-json-checker) #:use-module (crates-io))

(define-public crate-turbo-json-checker-1.0.0 (c (n "turbo-json-checker") (v "1.0.0") (d (list (d (n "packed_simd") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "snap") (r "^1.0.0") (d #t) (k 2)))) (h "0ahbl9vq4rxmxvww9d2pp01b02cv1l6j4710hvc45f3qapywpy22") (f (quote (("nightly" "packed_simd") ("default")))) (y #t)))

(define-public crate-turbo-json-checker-1.0.1 (c (n "turbo-json-checker") (v "1.0.1") (d (list (d (n "packed_simd") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "snap") (r "^1.0.0") (d #t) (k 2)))) (h "1gh2zzaq0srg39knnzhb22nvvpb6ljd84iawmf9q1bigkznxjgwn") (f (quote (("nightly" "packed_simd") ("default"))))))

(define-public crate-turbo-json-checker-2.0.0 (c (n "turbo-json-checker") (v "2.0.0") (d (list (d (n "packed_simd") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "snap") (r "^1.0.0") (d #t) (k 2)))) (h "1g83a2x8p41w2fn8sc3j4pg6cyc5cdfkkaq6kg2fqbimnck5yhcw") (f (quote (("nightly" "packed_simd") ("default"))))))

(define-public crate-turbo-json-checker-2.0.1 (c (n "turbo-json-checker") (v "2.0.1") (d (list (d (n "packed_simd") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "snap") (r "^1.0.0") (d #t) (k 2)))) (h "0l4n1qzmh6fachjnb8aksil5rvil1abwdiq3ajng0xakj8bwhxnd") (f (quote (("nightly" "packed_simd") ("default"))))))

