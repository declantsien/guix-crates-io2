(define-module (crates-io tu rb turbine_reactive) #:use-module (crates-io))

(define-public crate-turbine_reactive-0.1.0 (c (n "turbine_reactive") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "read_color") (r "^0.1.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.1") (d #t) (k 0)))) (h "0yi88j5sgs9jycxcril501492xc4zn29f3n347bq8nm75h3ham2h")))

