(define-module (crates-io tu rb turbolib) #:use-module (crates-io))

(define-public crate-turbolib-0.3.3 (c (n "turbolib") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "aws-config") (r "^0.12.0") (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^0.12.0") (d #t) (k 0)) (d (n "aws-smithy-http") (r "^0.42.0") (d #t) (k 0)) (d (n "aws-types") (r "^0.12.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0f0givvsnv7dxbwj5mgl9i8mwbana8k40dlf33lf23wsvyip1pcn") (r "1.57.0")))

