(define-module (crates-io tu rb turbo-crates-testing-proc-macros) #:use-module (crates-io))

(define-public crate-turbo-crates-testing-proc-macros-0.0.1 (c (n "turbo-crates-testing-proc-macros") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "1lzh4fpqnvcf1riyhgaji12qa7gbykvdz36pkjby7br8cq0yr83q")))

(define-public crate-turbo-crates-testing-proc-macros-0.0.2 (c (n "turbo-crates-testing-proc-macros") (v "0.0.2") (d (list (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "0k62kp62mz5hxr11103i015qd1cgid91mxbcc38i4fl2xvr9n124")))

