(define-module (crates-io tu rb turbostate) #:use-module (crates-io))

(define-public crate-turbostate-0.1.0 (c (n "turbostate") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "1abv4857wi3zqwldsnv4z2z4caayacgyjbyqqr0zzvbl342g1y2d") (f (quote (("from_residual") ("async" "tokio" "async-recursion"))))))

(define-public crate-turbostate-0.1.1 (c (n "turbostate") (v "0.1.1") (d (list (d (n "async-recursion") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "07zi9v8f8x26l0lzjqbsg9ksk7qsklifis9wriakybqackf7njw6") (f (quote (("from_residual") ("async" "tokio" "async-recursion"))))))

