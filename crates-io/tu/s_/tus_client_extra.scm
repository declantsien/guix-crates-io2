(define-module (crates-io tu s_ tus_client_extra) #:use-module (crates-io))

(define-public crate-tus_client_extra-0.1.1 (c (n "tus_client_extra") (v "0.1.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "02k97ssqfrq2098q73xvzps07q020y4wvb6dhk7598dn75fhkr8m")))

