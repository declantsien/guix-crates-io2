(define-module (crates-io tu s_ tus_async_client) #:use-module (crates-io))

(define-public crate-tus_async_client-0.1.0 (c (n "tus_async_client") (v "0.1.0") (d (list (d (n "base64") (r "=0.10.1") (d #t) (k 0)) (d (n "reqwest") (r "=0.11.3") (d #t) (k 0)))) (h "0pjc8k8bxby8n6aln2g81a0fmpzbxsy913724ba74av3si2280m2")))

(define-public crate-tus_async_client-0.2.0 (c (n "tus_async_client") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1sg4s05mcccxgr4ih0ng5kapr9jggn14pafi9wi0crf0xf37iz5j")))

(define-public crate-tus_async_client-0.2.1 (c (n "tus_async_client") (v "0.2.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "0hiw8s2m91a5d4byjxcr7kpca612hzps4m30ljmcyqs268lbfj8p")))

