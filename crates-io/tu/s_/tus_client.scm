(define-module (crates-io tu s_ tus_client) #:use-module (crates-io))

(define-public crate-tus_client-0.1.0 (c (n "tus_client") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0wicnlyp473hzybm7cfqfg8si506lq3ljl9m4rn6jhk9qjwnvwrc")))

(define-public crate-tus_client-0.1.1 (c (n "tus_client") (v "0.1.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1s3hzn07ngmmail8fays0kh3476pax7s0g853kxmm7rcr3lc5z6z")))

