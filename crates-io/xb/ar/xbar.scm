(define-module (crates-io xb ar xbar) #:use-module (crates-io))

(define-public crate-xbar-1.0.0 (c (n "xbar") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "svg") (r "^0.5.12") (d #t) (k 2)))) (h "07wz93gq5z7pljg77cx6s6l0aca3j91d6xvry87v587jb9q7q39y") (y #t)))

(define-public crate-xbar-1.0.1 (c (n "xbar") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "svg") (r "^0.5.12") (d #t) (k 2)))) (h "1afh2jsqzfmdzfgzyycj0snq5jhrsy8c1p03rs8x8r5kzhz4x13v")))

