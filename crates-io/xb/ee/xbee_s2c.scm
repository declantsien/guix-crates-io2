(define-module (crates-io xb ee xbee_s2c) #:use-module (crates-io))

(define-public crate-xbee_s2c-0.1.0 (c (n "xbee_s2c") (v "0.1.0") (d (list (d (n "arraydeque") (r "^0.4") (k 0)) (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1zmkka0149rrrbgzir18n97589q5lmzakwwrfr96q01zzlk1krcp")))

