(define-module (crates-io xb it xbitwise) #:use-module (crates-io))

(define-public crate-xbitwise-0.1.0 (c (n "xbitwise") (v "0.1.0") (h "0hwipbqsp6pzrgjmcq57v2zmbf8d47izs4fcfph7lyxk86ai0940") (f (quote (("unsigned" "u8" "u16" "u32" "u64" "u128") ("u8") ("u64") ("u32") ("u16") ("u128") ("signed" "i8" "i16" "i32" "i64" "i128") ("i8") ("i64") ("i32") ("i16") ("i128") ("default" "signed" "unsigned")))) (r "1.31.0")))

