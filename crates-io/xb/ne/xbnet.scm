(define-module (crates-io xb ne xbnet) #:use-module (crates-io))

(define-public crate-xbnet-1.1.0 (c (n "xbnet") (v "1.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "etherparse") (r "^0.9.0") (d #t) (k 0)) (d (n "format_escape_default") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.4") (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tun-tap") (r "^0.1.2") (k 0)))) (h "1pbr2ff7cmzbvc09z76szn9mfzrrlln217vhdqcj758gxjn1dzww")))

