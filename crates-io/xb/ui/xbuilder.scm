(define-module (crates-io xb ui xbuilder) #:use-module (crates-io))

(define-public crate-xbuilder-0.1.0 (c (n "xbuilder") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "format_xml") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.33.1") (f (quote ("serde-str" "serde-with-str"))) (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.33.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "0fdcdpjihi85za81jxd4id91fdd8b3y1jzpvysmrvi8c8b4ld6sd")))

