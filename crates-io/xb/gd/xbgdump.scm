(define-module (crates-io xb gd xbgdump) #:use-module (crates-io))

(define-public crate-xbgdump-0.1.0 (c (n "xbgdump") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (f (quote ("png" "pnm"))) (k 0)) (d (n "imageproc") (r "^0.22.0") (k 0)) (d (n "x11rb") (r "^0.8.1") (d #t) (k 0)))) (h "09niwr085nnzi3pryjg32iw4wh4l28jw66nh534sr6g5sagmai21")))

(define-public crate-xbgdump-0.1.3 (c (n "xbgdump") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (f (quote ("png" "pnm"))) (k 0)) (d (n "imageproc") (r "^0.22.0") (k 0)) (d (n "x11rb") (r "^0.8.1") (d #t) (k 0)))) (h "0n5c77bj7nzwvjlc11r92685xmlyr22dyjlnx5v9b7ahlf9lf3pd")))

(define-public crate-xbgdump-0.1.4 (c (n "xbgdump") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (f (quote ("png" "pnm"))) (k 0)) (d (n "imageproc") (r "^0.22.0") (k 0)) (d (n "x11rb") (r "^0.8.1") (d #t) (k 0)))) (h "121shsa2db164vid6qapk28mddi2ybdw11r68zb8xkq84hb1a8hk")))

(define-public crate-xbgdump-0.1.5 (c (n "xbgdump") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (f (quote ("png" "pnm"))) (k 0)) (d (n "x11rb") (r "^0.8.1") (f (quote ("randr"))) (d #t) (k 0)))) (h "1hd453z0hk7y0jabd0l5swsd8vyii85bw4wr8bpyksm5hy0w5kf3")))

(define-public crate-xbgdump-0.1.7 (c (n "xbgdump") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (f (quote ("png" "pnm"))) (k 0)) (d (n "x11rb") (r "^0.8.1") (f (quote ("randr"))) (d #t) (k 0)))) (h "1mjxzxrk3aqixsdk155zcndhxnff28vmqarzyrffpswp5hdcvyfc")))

