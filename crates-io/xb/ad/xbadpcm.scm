(define-module (crates-io xb ad xbadpcm) #:use-module (crates-io))

(define-public crate-xbadpcm-0.1.0 (c (n "xbadpcm") (v "0.1.0") (h "0ddy1n2hgxfpwymc8nb59bqa1xscv5ii9s6537bzx7x88sg5vnfh") (f (quote (("std") ("default" "std"))))))

(define-public crate-xbadpcm-0.1.1 (c (n "xbadpcm") (v "0.1.1") (h "06cqsqg74mzd8hq2fnyx27cdxvv877sccjixx5jmvz6pdpz2qdw9") (f (quote (("std") ("default" "std"))))))

