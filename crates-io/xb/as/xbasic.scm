(define-module (crates-io xb as xbasic) #:use-module (crates-io))

(define-public crate-xbasic-0.1.0 (c (n "xbasic") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0clxgw8z813fh1p5p3byakxay9y8g98djdj68zg8nmakdzhaw9kb")))

(define-public crate-xbasic-0.1.1 (c (n "xbasic") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0iaqccimzmdfjzjpi4f9gibwvs9wcma2xk8kmys4lfsg6igmkb62")))

(define-public crate-xbasic-0.1.2 (c (n "xbasic") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "10a1rjc5grp67jvsdrr6vls0wvsyq1xm6pbvqdh7vlv0nxgr2vij")))

(define-public crate-xbasic-0.1.3 (c (n "xbasic") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "04aymk56dwgb2ldcdnwdp0i79fxavarir2vfmhad4b0szw3z4mrp")))

(define-public crate-xbasic-0.1.4 (c (n "xbasic") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "040xajzyjaaj1xsxp23q3xz0yykn5pikmfgxmnayya8scik0cyaj")))

(define-public crate-xbasic-0.1.5 (c (n "xbasic") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0hg5yz6lzmkzm7pw1fsbgnn2pi7vxg6cm705jh8iv4j6z4f5h799")))

(define-public crate-xbasic-0.2.0 (c (n "xbasic") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "147ys42sbi7iynlkc9m8ra2cyy31n1qbd9jyj5mjfyn03kvxj88m")))

(define-public crate-xbasic-0.2.1 (c (n "xbasic") (v "0.2.1") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "num-derive") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "num-traits") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "phf") (r ">=0.8.0, <0.9.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0xlfd3h2dijqnrlqkqg63pg2yjhpkiv0jcif1sakkfd5aavl6da5")))

(define-public crate-xbasic-0.3.0 (c (n "xbasic") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "1b2h8sqya1dk6mfm9h4pc28sk10m72v1cpp3hsbms12h3sfdrkrv")))

(define-public crate-xbasic-0.3.1 (c (n "xbasic") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "1fnnpl9dfgibjsrnf52k11pk9rqmph9gs1lr7sv481l4l011wwgz")))

(define-public crate-xbasic-0.3.2 (c (n "xbasic") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "1pzh0nkl6vw796hc8zgjg9z57lvy9cxyz8lvsiqzr7jffpvxa3g3")))

