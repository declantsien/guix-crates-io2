(define-module (crates-io xb in xbinary) #:use-module (crates-io))

(define-public crate-xbinary-0.1.0 (c (n "xbinary") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "1h36kidrbmdbpgna18yi1zfzsx3nrpfgcfqmhx6syxbh47s2dxnc")))

(define-public crate-xbinary-0.1.1 (c (n "xbinary") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "0as5890jb6qdfjvfi5rc0b90r84b3j53m4zgj94c9wy3w1gdhyw2")))

(define-public crate-xbinary-0.1.2 (c (n "xbinary") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "0zynlrzk8xhpw2dkafwbigzff4ckpaf4ysbrydmgj8pc32bp2brr")))

(define-public crate-xbinary-0.1.3 (c (n "xbinary") (v "0.1.3") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "1z75xlalyfb5lnsjhb7hzi39qx8gj4clgpni4g9bvryfha8viaxp")))

(define-public crate-xbinary-0.1.4 (c (n "xbinary") (v "0.1.4") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "0bhf00g1bqa9agh0qbdv48bdch7ivkp59628dbp0xjr4czaaxfdy")))

(define-public crate-xbinary-0.1.5 (c (n "xbinary") (v "0.1.5") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "14az3rifq2aaknsbyg6qhc6m7cwky14rbb94czr9qlhx50bkkfm1")))

