(define-module (crates-io xb in xbin) #:use-module (crates-io))

(define-public crate-xbin-0.1.1 (c (n "xbin") (v "0.1.1") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)))) (h "16zd3kmz1c1pfhkmkz1m37jpn60ma9cggh673rij14dy4psrcf5g")))

(define-public crate-xbin-0.1.3 (c (n "xbin") (v "0.1.3") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)))) (h "118534a9rybz1jckxg3jfl04zdk169p2rxh0mdpgp109hng0fbr1")))

