(define-module (crates-io e- ma e-macros) #:use-module (crates-io))

(define-public crate-e-macros-0.1.0 (c (n "e-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1v99azc5hgsdn2bxxf0bygybhs0nbaqczcl4w21v9wyf2fdvhyqw") (y #t) (r "1.76.0")))

(define-public crate-e-macros-0.1.1 (c (n "e-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0wgbhjck307f7apzh4f3bf0ypg8im6cnzb0y6w0db194jw972r8w") (y #t) (r "1.76.0")))

(define-public crate-e-macros-0.1.2 (c (n "e-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1bgg15kcg8nsbp5c5hsgkrfhmhli4vjz9g3wk41cddifbi85d0qi") (y #t) (r "1.76.0")))

(define-public crate-e-macros-0.1.3 (c (n "e-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "123xn2zm1rd8ljgjiaxm27bysvydganw0vhv1kc5n2dyff0yjkdg") (y #t) (r "1.76.0")))

(define-public crate-e-macros-0.1.4 (c (n "e-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0j86qgjlv304xkzwm57wa517jlbrpzrjpc4ycb35y2rm8frs0pgf") (y #t) (r "1.76.0")))

(define-public crate-e-macros-0.1.5 (c (n "e-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "16amhfkwgk6hrmjcdjb4yl8iybq3amvyn8xz65zvxg8xzy08hfiz") (r "1.76.0")))

(define-public crate-e-macros-0.1.6 (c (n "e-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0zkcf7nwpz8b959v00rv8s520r5s5jp3i6vzcmybg615pd7xy7y3") (r "1.76.0")))

