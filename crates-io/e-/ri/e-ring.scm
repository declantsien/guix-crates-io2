(define-module (crates-io e- ri e-ring) #:use-module (crates-io))

(define-public crate-e-ring-0.1.0 (c (n "e-ring") (v "0.1.0") (h "169vpqg0nkac5zvhzlphvzdi81z6kmv411h8f44dbc47yw4q707g")))

(define-public crate-e-ring-0.2.0 (c (n "e-ring") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "0md21ihgvkqzica5nzx8axr29748smmvbms6fv63kdvdmi9bbk23") (f (quote (("hist" "embedded-graphics"))))))

(define-public crate-e-ring-0.3.0 (c (n "e-ring") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0425ksm3q0ii9766nhp0gzqb9mdmqwfsrzp4hglqjd7csz9xn2b6") (f (quote (("hist" "embedded-graphics"))))))

