(define-module (crates-io e- wr e-write-buffer) #:use-module (crates-io))

(define-public crate-e-write-buffer-0.1.0 (c (n "e-write-buffer") (v "0.1.0") (h "0bj0x8h3c7r28p47wzkcrzy1wj08fh4kr7p7h6pik8di3vkw5j30")))

(define-public crate-e-write-buffer-0.2.0 (c (n "e-write-buffer") (v "0.2.0") (h "1klfvh2k21fh2z9dhakybxzz752svz8dhdds1zqgvmw7ax8cg5cs")))

(define-public crate-e-write-buffer-0.3.0 (c (n "e-write-buffer") (v "0.3.0") (h "0nvqk09bjd7balsxyfrqyn3gpi4amg45vyfmgr4lmasd9jlj1738")))

(define-public crate-e-write-buffer-0.4.0 (c (n "e-write-buffer") (v "0.4.0") (h "0zyrgcw4h9232hixzdsw78s704ary935xgvjing7bqba2ygp4xr1")))

(define-public crate-e-write-buffer-0.5.0 (c (n "e-write-buffer") (v "0.5.0") (h "01984av3dxy7485agbi92yb7q7mms8dp2mfbbclkhiqm1frbgdzn")))

(define-public crate-e-write-buffer-0.6.0 (c (n "e-write-buffer") (v "0.6.0") (h "0xcf6nl3ac1zmvplwn2fwjh3ilnn7anmpkv5ddnz8b14bgbjgfmz")))

(define-public crate-e-write-buffer-0.6.1 (c (n "e-write-buffer") (v "0.6.1") (h "0lagxm5vznjh7ikcpg9x5r2hqgxqjxwpp0ni74fp1zakgdf4whh6")))

