(define-module (crates-io ik ar ikarusdef) #:use-module (crates-io))

(define-public crate-ikarusdef-0.1.0 (c (n "ikarusdef") (v "0.1.0") (d (list (d (n "indoc") (r "^2.0.1") (d #t) (k 0)))) (h "00ijkpmqgc0wkya0378d7vl7r13crzcks7p5mm6ajscs4h9lypri")))

(define-public crate-ikarusdef-0.1.1 (c (n "ikarusdef") (v "0.1.1") (d (list (d (n "indoc") (r "^2.0.1") (d #t) (k 0)))) (h "0xp1prbbswx8yrgwi6sbmw00nmp3b08ch1gy08j1adfvvg3k4a3a")))

(define-public crate-ikarusdef-0.1.2 (c (n "ikarusdef") (v "0.1.2") (d (list (d (n "indoc") (r "^2.0.1") (d #t) (k 0)))) (h "1kk4k2i6a4wgwwrjlnvg6mhc0dshrvw4vwvkb41fdj00xwcplws7")))

(define-public crate-ikarusdef-0.1.3 (c (n "ikarusdef") (v "0.1.3") (d (list (d (n "indoc") (r "^2.0.1") (d #t) (k 0)))) (h "1bgaapk3x6szjdvcbh09g2sr4i51lg9bixazpgifib9g1in34nl6")))

(define-public crate-ikarusdef-0.1.4 (c (n "ikarusdef") (v "0.1.4") (d (list (d (n "indoc") (r "^2.0.1") (d #t) (k 0)))) (h "03q1f1c9nlxmdhsfc57pgip7lkzmmzr20m061zajbpzjnqc84sjp")))

