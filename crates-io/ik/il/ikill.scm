(define-module (crates-io ik il ikill) #:use-module (crates-io))

(define-public crate-ikill-1.0.0 (c (n "ikill") (v "1.0.0") (d (list (d (n "heim") (r "^0.1.0-alpha.1") (f (quote ("process" "runtime-tokio"))) (k 0)) (d (n "skim") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full" "macros"))) (d #t) (k 0)))) (h "1pr07g9995c6khpd1xq16ibcckjpypjw5w4c8i2dj79bk9fmd4ni")))

(define-public crate-ikill-1.1.0 (c (n "ikill") (v "1.1.0") (d (list (d (n "heim") (r "^0.1.0-alpha.1") (f (quote ("process" "runtime-tokio"))) (k 0)) (d (n "skim") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full" "macros"))) (d #t) (k 0)))) (h "13pp3w3wp20nl3hnjx2jr9pwgg1zv1sc97133qb0crng7jdi25sk")))

(define-public crate-ikill-1.2.0 (c (n "ikill") (v "1.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "heim") (r "^0.1.0-alpha.1") (f (quote ("process" "runtime-tokio"))) (k 0)) (d (n "skim") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full" "macros"))) (d #t) (k 0)))) (h "1nfx34bzccqy4bxq8vb97pvj95lcna9xv2nkarar1hgrzjsgla4i")))

(define-public crate-ikill-1.3.0 (c (n "ikill") (v "1.3.0") (d (list (d (n "heim") (r "^0.1.0-rc.1") (f (quote ("process"))) (k 0)) (d (n "skim") (r "^0.9.4") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "0pyzxq6r3nf04xn5rzlvv4rjqdaxbgacldr68s6nj28flx1jrrr9")))

(define-public crate-ikill-1.4.0 (c (n "ikill") (v "1.4.0") (d (list (d (n "heim") (r "^0.1.0-rc.1") (f (quote ("process"))) (k 0)) (d (n "skim") (r "^0.9.4") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "0pl6s7vrb68jdyx1sic8lprh6cip0kh4wkfy323cnqgdgg2c4s2z")))

(define-public crate-ikill-1.5.0 (c (n "ikill") (v "1.5.0") (d (list (d (n "heim") (r "^0.1.0-rc.1") (f (quote ("process"))) (k 0)) (d (n "skim") (r "^0.9.4") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "0nm8zpgqcbyp0026xnmgx8664rqd5zv01knisrd09bymq4bjqlnc")))

