(define-module (crates-io ik on ikon) #:use-module (crates-io))

(define-public crate-ikon-0.1.0-beta.0 (c (n "ikon") (v "0.1.0-beta.0") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "0r37ix8nbbj709ivb7mi22k1n8c7s06amscbgmkzrqy9v125rrrr") (y #t)))

(define-public crate-ikon-0.1.0-beta.1 (c (n "ikon") (v "0.1.0-beta.1") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "1h4fijrz9s1nbgr6c7kins0hrc1nr933wz4i6vl501v3a1gym3sp") (y #t)))

(define-public crate-ikon-0.1.0-beta.2 (c (n "ikon") (v "0.1.0-beta.2") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "1i4hfj8as63dw9d22dncj6mvfqmmvcmlgqxqa9z9ysblb5q7ybcw") (y #t)))

(define-public crate-ikon-0.1.0-beta.3 (c (n "ikon") (v "0.1.0-beta.3") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "1h35yjyhnrcqw6xnzkxdfyj3g1njsxr99fy4lg75709abwnhhvsi") (y #t)))

(define-public crate-ikon-0.1.0-beta.4 (c (n "ikon") (v "0.1.0-beta.4") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "1gm8plk1ck2nh6kd0dh5s070c11cc175jhq6c8dpvb3njr0sych5") (y #t)))

(define-public crate-ikon-0.1.0-beta.5 (c (n "ikon") (v "0.1.0-beta.5") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "17qj4bqp9nrdxq7fvgn841nn6z2bc20890svg4illzgkdsl8zb8s") (y #t)))

(define-public crate-ikon-0.1.0-beta.6 (c (n "ikon") (v "0.1.0-beta.6") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "02vqywwn8702shnx6hv2s9spwp61ddif87y8hm7g170113756kdq") (y #t)))

(define-public crate-ikon-0.1.0-beta.7 (c (n "ikon") (v "0.1.0-beta.7") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "0y5zws4bfh7vjy74slnbyryrca51451bis1bxz9xkp805792xbih") (y #t)))

(define-public crate-ikon-0.1.0-beta.8 (c (n "ikon") (v "0.1.0-beta.8") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "0jz26wq1nbkn1bkcqranxiki9030x8hrmm4nn2yfiyyxhpjv75np") (y #t)))

(define-public crate-ikon-0.1.0-beta.9 (c (n "ikon") (v "0.1.0-beta.9") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "1r69hhhs2qjxbmp7hcvvwjfwmgala3ri91h1mhybvjjl9dmvw0id") (y #t)))

(define-public crate-ikon-0.1.0-beta.10 (c (n "ikon") (v "0.1.0-beta.10") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "10qj8pzwi14wh6i7r98h3np6a3b8qd615wc58b5cap9r9b9lghz3") (y #t)))

(define-public crate-ikon-0.1.0-beta.11 (c (n "ikon") (v "0.1.0-beta.11") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "1ahnnagbjg8yl8j6r4vxpf5yfn0dv2iygvvlz5vg9xcl1h8h5h8v") (y #t)))

(define-public crate-ikon-0.1.0-beta.12 (c (n "ikon") (v "0.1.0-beta.12") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "0w404s78rpwg51c17m6085qq79vn9f1pvzkvm3fsmyk1xq50p7if") (y #t)))

(define-public crate-ikon-0.1.0-beta.13 (c (n "ikon") (v "0.1.0-beta.13") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "14rgdkr6a2pfhsayrjyik2xhl34shalr7rd4n1vd8pkf3kc1ksdv") (y #t)))

(define-public crate-ikon-0.1.0-beta.14 (c (n "ikon") (v "0.1.0-beta.14") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "08wpn437533qjb0sqd98q0abx8h8ygcz7c9vsdm75bjppawyq1ya") (y #t)))

(define-public crate-ikon-0.1.0-beta.15 (c (n "ikon") (v "0.1.0-beta.15") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "18qpavaa120yps15mj1cwvgi1l4snm9j5gcq9ykm9dqkz261396h") (y #t)))

(define-public crate-ikon-0.1.0-beta.16 (c (n "ikon") (v "0.1.0-beta.16") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "resvg") (r "^0.8.0") (f (quote ("raqote-backend"))) (d #t) (k 0)))) (h "1mf46xpk3cafsjlki1ph3z6g51qgw6w70i1b9m51bw8k8kichms8") (y #t)))

