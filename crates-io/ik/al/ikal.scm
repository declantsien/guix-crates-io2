(define-module (crates-io ik al ikal) #:use-module (crates-io))

(define-public crate-ikal-0.1.0 (c (n "ikal") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "std"))) (k 0)) (d (n "ikal-derive") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c0xfh22xdx3v7ql3gndb11kvy6icq9hk4rllhv4dzg39137py74")))

(define-public crate-ikal-0.2.0 (c (n "ikal") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.37") (f (quote ("clock" "std"))) (k 0)) (d (n "ikal-derive") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1liwqy8sr4ws9agv6g63ll0lc1g51ayc4rzcajanvsgr8fwd5m6s")))

