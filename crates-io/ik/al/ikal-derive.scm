(define-module (crates-io ik al ikal-derive) #:use-module (crates-io))

(define-public crate-ikal-derive-0.1.0 (c (n "ikal-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1y2zyqlgf9cadcq8smlc2wq35irf4f66kqcbcask5nbjz254kb20")))

(define-public crate-ikal-derive-0.2.0 (c (n "ikal-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dmy65sl9g971lkzk2fn4nqkf6zdnkxk9pkrql9sdn072kchcprn")))

