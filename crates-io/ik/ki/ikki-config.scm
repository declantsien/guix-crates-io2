(define-module (crates-io ik ki ikki-config) #:use-module (crates-io))

(define-public crate-ikki-config-0.1.1 (c (n "ikki-config") (v "0.1.1") (d (list (d (n "camino") (r "^1.0.9") (d #t) (k 0)) (d (n "kdl") (r "^4.3.0") (d #t) (k 0)) (d (n "knuffel") (r "^2.0.0") (d #t) (k 0)) (d (n "miette") (r "^5.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "toposort") (r "^0.1.0") (d #t) (k 0)))) (h "1ri61vdwll74q6bf2zj6hb2f61v2k8hs4nqzl3qjd3rnlj75a74l")))

(define-public crate-ikki-config-0.1.2 (c (n "ikki-config") (v "0.1.2") (d (list (d (n "camino") (r "^1.0.9") (d #t) (k 0)) (d (n "kdl") (r "^4.3.0") (d #t) (k 0)) (d (n "knuffel") (r "^2.0.0") (d #t) (k 0)) (d (n "miette") (r "^5.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "toposort") (r "^0.1.0") (d #t) (k 0)))) (h "1s8jbb22cph1qhqlgbs1ww3ljd6vx2p046yss92gi0w9n4cl4c4p")))

(define-public crate-ikki-config-0.2.0 (c (n "ikki-config") (v "0.2.0") (d (list (d (n "camino") (r "^1.0.9") (d #t) (k 0)) (d (n "kdl") (r "^4.3.0") (d #t) (k 0)) (d (n "knuffel") (r "^2.0.0") (d #t) (k 0)) (d (n "miette") (r "^5.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "toposort") (r "^0.1.0") (d #t) (k 0)))) (h "17mk1krm42wwh03pz0bicyb55k7sd14miwr3a2c4a6yph40yl2y8")))

