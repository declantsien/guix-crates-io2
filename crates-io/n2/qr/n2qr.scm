(define-module (crates-io n2 qr n2qr) #:use-module (crates-io))

(define-public crate-n2qr-0.1.0 (c (n "n2qr") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "hex-rgb") (r "^0.1.1") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.8.0") (d #t) (k 0)))) (h "0ynny70sraw5dmvr13i3094adzywjh06qrg17w36fx9pf82r0rz2") (r "1.61.0")))

