(define-module (crates-io on it onitama) #:use-module (crates-io))

(define-public crate-onitama-0.1.0 (c (n "onitama") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "0kia87f7brfn2d7j4vnz40rvm5bsj2qp1ckd7d2dsmv793w7cgwc")))

(define-public crate-onitama-0.1.1 (c (n "onitama") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "1fvvahgz85pv1y0naslccysn907fdq8zrq499a79ljhc14lb593j")))

(define-public crate-onitama-0.1.2 (c (n "onitama") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q6896730x6anqrrir84g3p5vq33nj2mvrax2lxrwsxllzabaf85")))

