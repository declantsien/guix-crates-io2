(define-module (crates-io on _d on_drop) #:use-module (crates-io))

(define-public crate-on_drop-0.1.0 (c (n "on_drop") (v "0.1.0") (h "1ncf64qgyvhcab4b6g1s5wdc6947g2sxdkp8lkw71h31nj4dp9sf")))

(define-public crate-on_drop-0.1.1 (c (n "on_drop") (v "0.1.1") (h "0k00dvwqlis6nvajhsrr4xbhivy1n2gxrjjcg0pmcffyh37dnsqi")))

(define-public crate-on_drop-0.1.2 (c (n "on_drop") (v "0.1.2") (h "00kpg10wpyh71bmmrxlbwhkjwswfjvn899gxq3pdjqsxxnbl2bcp")))

