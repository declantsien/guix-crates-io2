(define-module (crates-io on ay onay_stat) #:use-module (crates-io))

(define-public crate-onay_stat-0.1.0 (c (n "onay_stat") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)))) (h "1mrsfclfdzy32fp4yrf2isa5v4hm0j7rpbh0a1y1912f8cfr6qfv") (y #t)))

(define-public crate-onay_stat-0.1.1 (c (n "onay_stat") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)))) (h "0bfkqa7js14n81x0g55kfg229yxkljrxdn6x2fsbsdsh39q1zlh4") (y #t)))

(define-public crate-onay_stat-0.1.2 (c (n "onay_stat") (v "0.1.2") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)))) (h "0x4l3pzcav25kyd8ahrn27ycxz151kfn3kyirng3wx2ajvlvf8dn") (y #t)))

(define-public crate-onay_stat-0.1.3 (c (n "onay_stat") (v "0.1.3") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1dsva2npwwx1j4wlm2jfmsgx0kll6srcq2hzhvsxpkzfv2yrdvkr")))

