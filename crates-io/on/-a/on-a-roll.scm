(define-module (crates-io on -a on-a-roll) #:use-module (crates-io))

(define-public crate-on-a-roll-0.1.0 (c (n "on-a-roll") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "diesel") (r "^2.1.4") (f (quote ("sqlite" "returning_clauses_for_sqlite_3_35"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^2.1.0") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "libsqlite3-sys") (r "^0.27") (f (quote ("bundled"))) (d #t) (k 0)))) (h "100y74w5ddrgjfysx3dci6hb3s1ihri94nn08rfynl7z9c0fwwxn")))

