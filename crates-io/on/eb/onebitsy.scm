(define-module (crates-io on eb onebitsy) #:use-module (crates-io))

(define-public crate-onebitsy-0.1.0 (c (n "onebitsy") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.8.0") (f (quote ("stm32f415" "rt"))) (d #t) (k 0)))) (h "1awznzccmlrszx1adb9js63w944d0i6m7mqdrv7d5lnwlim45rxd") (f (quote (("rt" "stm32f4xx-hal/rt"))))))

(define-public crate-onebitsy-0.1.1 (c (n "onebitsy") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.8.0") (f (quote ("stm32f415" "rt"))) (d #t) (k 0)))) (h "1wq7rdcam1cppzy3k7bxqcm94l74p9chzg0y2xig7kxar9a4g1m0") (f (quote (("rt" "stm32f4xx-hal/rt"))))))

(define-public crate-onebitsy-0.1.2 (c (n "onebitsy") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.8.0") (f (quote ("stm32f415" "rt"))) (d #t) (k 0)))) (h "04acz2wnxd0jda6idncmkg01lwyq535h25203czjcgbn9gnsqyjd") (f (quote (("rt" "stm32f4xx-hal/rt"))))))

(define-public crate-onebitsy-0.1.3 (c (n "onebitsy") (v "0.1.3") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.8.0") (f (quote ("stm32f415" "rt"))) (d #t) (k 0)))) (h "14ll2h8j5ac6rgpmd8w24byg7mqjggqjdm2309n902wz7bia7q3h") (f (quote (("rt" "stm32f4xx-hal/rt"))))))

(define-public crate-onebitsy-0.2.0 (c (n "onebitsy") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32f4xx-hal") (r "^0.8") (f (quote ("stm32f415" "rt"))) (k 0)))) (h "0qaizf1kw4l03v6yvliniyim19w8gbxjjkm0yg0x3dbbd74l6w3j")))

