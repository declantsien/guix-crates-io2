(define-module (crates-io on ig onigiri) #:use-module (crates-io))

(define-public crate-onigiri-0.1.0 (c (n "onigiri") (v "0.1.0") (h "0x9z9510mvn5f906w7vpgay1iwhqr6ifc1m7yznwwq06pm0221ng")))

(define-public crate-onigiri-0.1.1 (c (n "onigiri") (v "0.1.1") (h "003dqgfq7xallr4d6m4m0n8cbl0s4mbb0v349pk4r1p8v078ld0l")))

(define-public crate-onigiri-0.1.2 (c (n "onigiri") (v "0.1.2") (h "0ba3qdfwav6yr97vvpy09z83dm94gady6w4amnr6hfx0gzirflwq")))

(define-public crate-onigiri-0.1.3 (c (n "onigiri") (v "0.1.3") (h "1fskxh95nwqvr2r5fi45276jblr1zld9cr8a05r9c5hk31rhnwz2")))

(define-public crate-onigiri-0.1.4 (c (n "onigiri") (v "0.1.4") (h "1qf4s635jhandd1yjnmp9qssqdvczb0wld3v463j7v3q3akwi52b")))

(define-public crate-onigiri-0.1.5 (c (n "onigiri") (v "0.1.5") (h "1nbvw9j6kzcba4gjhhscrs07dc7zrx2kw7d63zq75gqbb79ij7f8")))

(define-public crate-onigiri-0.1.6 (c (n "onigiri") (v "0.1.6") (h "12232fap6n7cspjlqnrg9yhws1kxag3cz7v6lvn4zh62w5vmp3my")))

(define-public crate-onigiri-0.1.7 (c (n "onigiri") (v "0.1.7") (h "0da2laqzfj3wm3s8hkkmpr05bdnsnd2173nmndcb0fm50l7grlb6")))

(define-public crate-onigiri-0.1.8 (c (n "onigiri") (v "0.1.8") (h "0vc4ibzgz8cm1v3m3y25x28mvgnm8pvnym2bqxcbqg6g312dhblp")))

(define-public crate-onigiri-0.1.9 (c (n "onigiri") (v "0.1.9") (h "0b2v2gbf3m1ga57s4214hfizgmyv6laqall0zzpy39dwry4lq53y")))

(define-public crate-onigiri-0.1.10 (c (n "onigiri") (v "0.1.10") (h "0yfv9ji07f3pjb5zlbkpx1qf08sxgk9986hnvw5wqx1sx81j3vc9")))

(define-public crate-onigiri-0.1.11 (c (n "onigiri") (v "0.1.11") (h "1ya5msgs7jlqa16pkk9m5q48pnb7dd78g0czilz9lk92v0630a4s")))

(define-public crate-onigiri-0.1.12 (c (n "onigiri") (v "0.1.12") (h "1z1rgf1cr2hsnwaz5zg51fiv3izv27wh56gf00bf3zpp8kih7hz8")))

(define-public crate-onigiri-0.1.13 (c (n "onigiri") (v "0.1.13") (h "0mn8yvzykilqf69bs3nn7178wyn1ijpbjfx70h77719b2w80rl6s")))

(define-public crate-onigiri-0.1.14 (c (n "onigiri") (v "0.1.14") (h "0v1i72vl09zvq62xidm7awccjmlqi48rifsz14xgz0c8srzavyfr")))

(define-public crate-onigiri-0.1.15 (c (n "onigiri") (v "0.1.15") (h "1wjbnjkxx5rdzsky3s034gfxl4i0mhh2adj10hydsx6y6shfalpj")))

(define-public crate-onigiri-0.2.0 (c (n "onigiri") (v "0.2.0") (h "1ivci518w36h67w2qgkbdm05xyf5z7cxmlzl5lk8zcq1j74zkncv")))

