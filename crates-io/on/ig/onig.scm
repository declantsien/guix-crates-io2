(define-module (crates-io on ig onig) #:use-module (crates-io))

(define-public crate-onig-0.0.1 (c (n "onig") (v "0.0.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1wqla6dj8xb7jpaznpwzn9pvjw2w1miz23d7qxcy0dcqv8j01phj")))

(define-public crate-onig-0.1.0 (c (n "onig") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "onig_sys") (r "^0.1") (d #t) (k 0)))) (h "0yfbsh7g5j7dkaq97dka5rrjl9clm7kg2lijl5jjnvpv67rp0rci")))

(define-public crate-onig-0.2.0 (c (n "onig") (v "0.2.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "onig_sys") (r "^0.1.0") (d #t) (k 0)))) (h "1j7fg420ap61qwv91p9mky8aq08zy9ibx2k6xghqgsypa0q84sbx")))

(define-public crate-onig-0.3.1 (c (n "onig") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "onig_sys") (r "^0.2") (d #t) (k 0)))) (h "04c3zsmwaw6zrm0pgdcjfnh7s0m1xqc65vb7cji4nx6ls76cj6wv")))

(define-public crate-onig-0.3.2 (c (n "onig") (v "0.3.2") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "onig_sys") (r "^0.3") (d #t) (k 0)))) (h "1n8zf2a4ng3b8y7nzy846r87hh9azksda750ywsazl0qm7jpisi0")))

(define-public crate-onig-0.3.4 (c (n "onig") (v "0.3.4") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^0.5") (d #t) (k 0)))) (h "0f80l9bbd1252ccwx0ss4hbgrms5w9vpipcmf7hqassvny5ahhv0")))

(define-public crate-onig-0.4.0 (c (n "onig") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^0.6") (d #t) (k 0)))) (h "04rgi8vrqzljbhkk1lwd481zi19rswn10kb5jf46pbqpkyj24azr")))

(define-public crate-onig-0.5.1 (c (n "onig") (v "0.5.1") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^0.7") (d #t) (k 0)))) (h "0m4il99ijsvnyiww5msj9hjhiflqcnqf1cg7i9l3hvdzd6h0qyi7")))

(define-public crate-onig-0.5.2 (c (n "onig") (v "0.5.2") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^0.7.1") (d #t) (k 0)))) (h "1g9ff1j0rvcw328q57d19alk77n1p9m60g74k3m48y4bxhmiyv9w") (f (quote (("std-pattern"))))))

(define-public crate-onig-0.6.0 (c (n "onig") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^0.8.0") (d #t) (k 0)))) (h "0b7gq6azldv5mak4b06y9pkzxzk1yyy4h7gskbjl1g30h9a8sgrs") (f (quote (("std-pattern"))))))

(define-public crate-onig-0.6.1 (c (n "onig") (v "0.6.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^0.8.1") (d #t) (k 0)))) (h "1lmas4y3c1451xyqar3dby3djya3rrmckwb1jpvfnglpd9akvwmr") (f (quote (("std-pattern"))))))

(define-public crate-onig-1.0.0 (c (n "onig") (v "1.0.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^1.0") (d #t) (k 0)))) (h "1w6fx1w48v1xd0wy4b7qz3brs89pkk1w718dhnfrk93s29gd8fa2") (f (quote (("std-pattern") ("static-libonig" "onig_sys/static_onig"))))))

(define-public crate-onig-1.1.0 (c (n "onig") (v "1.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^61.1") (d #t) (k 0)))) (h "00ifwwiw94lc045a4mpdlgjqvvcclfqsfk6glr7drbg6bk79d7cw") (f (quote (("std-pattern") ("static-libonig" "onig_sys/static_onig"))))))

(define-public crate-onig-1.2.0 (c (n "onig") (v "1.2.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^61.1") (d #t) (t "cfg(not(target_env = \"musl\"))") (k 0)) (d (n "onig_sys") (r "^61.1") (f (quote ("static_onig"))) (d #t) (t "cfg(target_env = \"musl\")") (k 0)))) (h "1dvwfac3z045zg2y8gjxyahsw2y3hai6x5c2hp8yl4gsadp5hpqc") (f (quote (("std-pattern") ("static-libonig" "onig_sys/static_onig"))))))

(define-public crate-onig-1.2.1 (c (n "onig") (v "1.2.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^61.1") (d #t) (t "cfg(not(target_env = \"musl\"))") (k 0)) (d (n "onig_sys") (r "^61.1") (f (quote ("static_onig"))) (d #t) (t "cfg(target_env = \"musl\")") (k 0)))) (h "09n7f1p52ky9wrr6c0fs4khyv3h6a6m4wf5lsn3cgp2vs4717r3q") (f (quote (("std-pattern") ("static-libonig" "onig_sys/static_onig"))))))

(define-public crate-onig-1.2.2 (c (n "onig") (v "1.2.2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^61.3") (d #t) (t "cfg(not(target_env = \"musl\"))") (k 0)) (d (n "onig_sys") (r "^61.3") (f (quote ("static_onig"))) (d #t) (t "cfg(target_env = \"musl\")") (k 0)))) (h "0xp6iz618dr45yjm2g6jwlk843vkj8kxvx8wrlghc3l1n7yssn8z") (f (quote (("std-pattern") ("static-libonig" "onig_sys/static_onig"))))))

(define-public crate-onig-1.2.3 (c (n "onig") (v "1.2.3") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^63.0") (d #t) (t "cfg(not(target_env = \"musl\"))") (k 0)) (d (n "onig_sys") (r "^63.0") (f (quote ("static_onig"))) (d #t) (t "cfg(target_env = \"musl\")") (k 0)))) (h "0ajbnqcpr7lg5jzya5m6a6bhyw9rhdr7jwmqwrzxkwyxxdq8rkj3") (f (quote (("std-pattern") ("static-libonig" "onig_sys/static_onig"))))))

(define-public crate-onig-1.2.4 (c (n "onig") (v "1.2.4") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^63.0.1") (d #t) (t "cfg(not(target_env = \"musl\"))") (k 0)) (d (n "onig_sys") (r "^63.0.1") (f (quote ("static_onig"))) (d #t) (t "cfg(target_env = \"musl\")") (k 0)))) (h "042ipwxj9w681yz2lvn544kmka3xlyk6l9v2vgj2shr2433w07dr") (f (quote (("std-pattern") ("static-libonig" "onig_sys/static_onig"))))))

(define-public crate-onig-1.3.0 (c (n "onig") (v "1.3.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^63.0.2") (d #t) (t "cfg(not(target_env = \"musl\"))") (k 0)) (d (n "onig_sys") (r "^63.0.2") (f (quote ("static_onig"))) (d #t) (t "cfg(target_env = \"musl\")") (k 0)))) (h "1pak2nlc7jibh9crplnicakdaqfv207v0kz8k1q1cyvhrylrsqgf") (f (quote (("std-pattern") ("static-libonig" "onig_sys/static_onig"))))))

(define-public crate-onig-1.4.0 (c (n "onig") (v "1.4.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^64.0.0") (d #t) (t "cfg(not(target_env = \"musl\"))") (k 0)) (d (n "onig_sys") (r "^64.0.0") (f (quote ("static_onig"))) (d #t) (t "cfg(target_env = \"musl\")") (k 0)))) (h "1nl2hwyrlfpfmh7skrnqaylnrr743jqxvqn8m2vc7bizi9mf8i72") (f (quote (("std-pattern") ("static-libonig" "onig_sys/static_onig"))))))

(define-public crate-onig-1.4.1 (c (n "onig") (v "1.4.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^64.0.1") (d #t) (t "cfg(not(target_env = \"musl\"))") (k 0)) (d (n "onig_sys") (r "^64.0.1") (f (quote ("static_onig"))) (d #t) (t "cfg(target_env = \"musl\")") (k 0)))) (h "1mw9xlyy4lzcbvnrx6cyjnhqm9l0wnh77xykimlnh3q0zw7cva0d") (f (quote (("std-pattern") ("static-libonig" "onig_sys/static_onig"))))))

(define-public crate-onig-1.5.0 (c (n "onig") (v "1.5.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^64.0.1") (d #t) (t "cfg(not(target_env = \"musl\"))") (k 0)) (d (n "onig_sys") (r "^64.0.1") (f (quote ("static_onig"))) (d #t) (t "cfg(target_env = \"musl\")") (k 0)))) (h "01ndq4zp2gq7pncv74kgbnqnmpsiwxcyp85v3grglkb4kb7mk3pd") (f (quote (("std-pattern") ("static-libonig" "onig_sys/static_onig"))))))

(define-public crate-onig-1.6.0 (c (n "onig") (v "1.6.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^65.0.0") (d #t) (t "cfg(not(target_env = \"musl\"))") (k 0)) (d (n "onig_sys") (r "^65.0.0") (f (quote ("static_onig"))) (d #t) (t "cfg(target_env = \"musl\")") (k 0)))) (h "08m5vqd0rbkwy983dra3axffvmswxfisr24wvkxka3q14h23lil2") (f (quote (("std-pattern") ("static-libonig" "onig_sys/static_onig"))))))

(define-public crate-onig-1.6.1 (c (n "onig") (v "1.6.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^65.0.1") (d #t) (t "cfg(not(target_env = \"musl\"))") (k 0)) (d (n "onig_sys") (r "^65.0.1") (f (quote ("static_onig"))) (d #t) (t "cfg(target_env = \"musl\")") (k 0)))) (h "1mrmvhvgahkspxqawm093kv0iffvs9pdkp5fm3nksc4p67ws6w8j") (f (quote (("std-pattern") ("static-libonig" "onig_sys/static_onig"))))))

(define-public crate-onig-2.0.0 (c (n "onig") (v "2.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^66.1.0") (d #t) (k 0)))) (h "1xvgkabji5v4cpwdndma0a8scsr11xpvndl8mds5b76iqgkpcagd") (f (quote (("std-pattern"))))))

(define-public crate-onig-2.0.1 (c (n "onig") (v "2.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^66.1.1") (d #t) (k 0)))) (h "09kvramv6k8x1iq2p0c8snh04kxwghwga54aj9i91jmz1lv52b85") (f (quote (("std-pattern"))))))

(define-public crate-onig-2.0.2 (c (n "onig") (v "2.0.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^66.1.3") (d #t) (k 0)))) (h "1kcvp1888cia34xx99c1jaaw8z93k44n41r1mpj88yal18szayiy") (f (quote (("std-pattern"))))))

(define-public crate-onig-3.0.0 (c (n "onig") (v "3.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^66.1.4") (d #t) (k 0)))) (h "03kp4dalwp0w6m0ayh2y62s26w7lkqcx84jdva37rlc80d76vdmf") (f (quote (("std-pattern"))))))

(define-public crate-onig-3.0.1 (c (n "onig") (v "3.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^66.1.4") (d #t) (k 0)))) (h "11k2pqpkgiiqd9gmj479k8gzd6lzn9warnr76n8x604v4iwa2552") (f (quote (("std-pattern"))))))

(define-public crate-onig-3.1.0 (c (n "onig") (v "3.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^67.0.1") (d #t) (k 0)))) (h "1xlkgmvdzxmd9n5p6s5v8ai2fg72mzyhgpih4fk9fpy22fbf9f0x") (f (quote (("std-pattern"))))))

(define-public crate-onig-3.1.1 (c (n "onig") (v "3.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^67.1.0") (d #t) (k 0)))) (h "06qn763ld8mvzzkql7p2ms1fsbbyfa59n61bqbpg7nf49alm975z") (f (quote (("std-pattern"))))))

(define-public crate-onig-3.2.0 (c (n "onig") (v "3.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^68.0.0") (d #t) (k 0)))) (h "1k0j4ynrq4wplanvq4ma4b25zszg3w2j5mkjg3pxj7xj2cyrnb0f") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug"))))))

(define-public crate-onig-3.2.1 (c (n "onig") (v "3.2.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^68.0.1") (d #t) (k 0)))) (h "1nbdjsdclypd2xk6aq0x9j6h92xvy5fr58c4zwn8p8zg24s3nsxy") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug"))))))

(define-public crate-onig-3.2.2 (c (n "onig") (v "3.2.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^68.0.1") (d #t) (k 0)))) (h "1fnisifc7gs63awncikmm13hmmljsk6d5ik8azm78332lilb5vpm") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug"))))))

(define-public crate-onig-4.0.0 (c (n "onig") (v "4.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^68.0.1") (d #t) (k 0)))) (h "0rfkw4k7mlvfgphpmhzc9pcv9379kmlb3ic4yxdw0j2ims89n2bw") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug"))))))

(define-public crate-onig-4.0.1 (c (n "onig") (v "4.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^68.0.2") (k 0)))) (h "0nzy0gj1vzfn8jqwngbpk0sl1mqcsjbmgz9svmc6sjj7apwjpsp4") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api") ("default" "posix-api"))))))

(define-public crate-onig-4.1.0 (c (n "onig") (v "4.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^68.2.0") (d #t) (k 0)))) (h "1vxqc7x1mx7f552vfl3df6b30k7n7ssz2ni6jfr5pf3qwn1y5g27") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api"))))))

(define-public crate-onig-4.2.0 (c (n "onig") (v "4.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^69.0.0") (d #t) (k 0)))) (h "0n5jwj39fhdlxzm8wq127x7nz7kr1j6wxwvm7l812llakfn9s3cz") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api"))))))

(define-public crate-onig-4.2.1 (c (n "onig") (v "4.2.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "onig_sys") (r "^69.0.0") (d #t) (k 0)))) (h "0xmpgybar4k8anm2760i3c5yapb7i96mxhy9cbkgjain4b5yisrz") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api"))))))

(define-public crate-onig-4.3.0 (c (n "onig") (v "4.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "onig_sys") (r "^69.1.0") (d #t) (k 0)))) (h "1rqzdjpdp4gr8py90zc2pq082b6a9b8v71w678qhbkds8li81vb4") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api"))))))

(define-public crate-onig-4.3.1 (c (n "onig") (v "4.3.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "onig_sys") (r "^69.1.0") (d #t) (k 0)))) (h "0vimlg02ba1scb3w0i6p0ya6h6zy6s33rx8xd9i0vwx86mfs0s9f") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api"))))))

(define-public crate-onig-4.3.2 (c (n "onig") (v "4.3.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "onig_sys") (r "^69.1.0") (d #t) (k 0)))) (h "1rr6gx0qpx1h8mxjgdawhdckl74k293kg410pr4rz8fqvad9him6") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api"))))))

(define-public crate-onig-5.0.0 (c (n "onig") (v "5.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "onig_sys") (r "^69.2.0") (d #t) (k 0)))) (h "0ivr0wq1zlyjhhkxpsnmpncg92sjx3rha8pnp3m1mzvgk7y27rz4") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api"))))))

(define-public crate-onig-4.3.3 (c (n "onig") (v "4.3.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "onig_sys") (r "= 69.1.0") (d #t) (k 0)))) (h "10xk5xfk3f3kq62s2sfaflsgr1v0v97xz6fl19gz9hmqn6rgq645") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api"))))))

(define-public crate-onig-6.0.0 (c (n "onig") (v "6.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "onig_sys") (r "^69.5.0") (k 0)))) (h "12jsl1kbii78l0c42w4fdwg1fv5wczn5lrc6i9z2zkigl3ccr4dx") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api") ("generate" "onig_sys/generate") ("default" "generate"))))))

(define-public crate-onig-6.1.0 (c (n "onig") (v "6.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "onig_sys") (r "^69.5.1") (k 0)))) (h "083yasllxj1kcilszvnwidcrdyvpbp0g952ncrrm9a1dhq9ms5ca") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api") ("generate" "onig_sys/generate") ("default" "generate"))))))

(define-public crate-onig-6.1.1 (c (n "onig") (v "6.1.1") (d (list (d (n "bitflags") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.2.0, <2.0.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "onig_sys") (r ">=69.6.0, <70.0.0") (k 0)))) (h "1mcx125hh22kx2d0676hkk2gli6v8r6c4rp3wh5qy0dwxpcnzd1h") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api") ("generate" "onig_sys/generate") ("default" "generate"))))))

(define-public crate-onig-6.2.0 (c (n "onig") (v "6.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "onig_sys") (r "^69.7.0") (k 0)))) (h "1h56qqgx1f2nyg7ki2yp4378gjf0dsklng6117snll9vwz0d6vxi") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api") ("generate" "onig_sys/generate") ("default" "generate"))))))

(define-public crate-onig-6.3.0 (c (n "onig") (v "6.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "onig_sys") (r "^69.7.0") (k 0)))) (h "1928v3z1h3gqc2qpjhwd2sbk55d2q1zhpsg3b7h3w7pn837h6x5i") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api") ("generate" "onig_sys/generate") ("default" "generate"))))))

(define-public crate-onig-6.3.1 (c (n "onig") (v "6.3.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "onig_sys") (r "^69.7.1") (k 0)))) (h "09djc7465na5w6f6anx7k766szqci0336wbddvm9wf5vjcngxpb7") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api") ("generate" "onig_sys/generate") ("default" "generate"))))))

(define-public crate-onig-6.3.2 (c (n "onig") (v "6.3.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "onig_sys") (r "^69.8.0") (k 0)))) (h "18yj1l8ibh8pid6yqgbz4zvcxa59hvdgv2xk6ikb1j690hjm1cqy") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api") ("generate" "onig_sys/generate") ("default" "generate"))))))

(define-public crate-onig-6.4.0 (c (n "onig") (v "6.4.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)) (d (n "onig_sys") (r "^69.8.1") (k 0)))) (h "0kyaz2fwa5dkr04rvk5ga2yv5jkqn1ymblvpdlf1gn9afb432jwc") (f (quote (("std-pattern") ("print-debug" "onig_sys/print-debug") ("posix-api" "onig_sys/posix-api") ("generate" "onig_sys/generate") ("default" "generate"))))))

