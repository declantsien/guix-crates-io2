(define-module (crates-io on ig oniguruma) #:use-module (crates-io))

(define-public crate-oniguruma-0.1.0 (c (n "oniguruma") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "153a921yvycc6681bpiy4ns59cb6ql1jdgx5l8cnjl5dfvfb2d13")))

(define-public crate-oniguruma-0.2.0 (c (n "oniguruma") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q0hvf1a4crpjjrqg09l9q9qar6ib7ax2rizdrm2zg9x3z2byvx9")))

(define-public crate-oniguruma-0.3.0 (c (n "oniguruma") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zn2ckn3q6zz7r83yzlnkzbrs7mvxgqin4q8xx0y19fizaqnhagn")))

(define-public crate-oniguruma-0.3.1 (c (n "oniguruma") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hgxcl7drnfwvx483rz66yn116f6iv4z7dylj5b2v9hpi04yf6pv") (y #t)))

