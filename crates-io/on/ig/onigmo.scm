(define-module (crates-io on ig onigmo) #:use-module (crates-io))

(define-public crate-onigmo-0.0.1 (c (n "onigmo") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.32") (d #t) (k 0)) (d (n "onigmo-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0dh8jjv08cv969jiz1w733y1yjv449b9vqdqm80zafgxzgxscn1q")))

