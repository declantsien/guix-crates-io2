(define-module (crates-io on li online) #:use-module (crates-io))

(define-public crate-online-0.0.1 (c (n "online") (v "0.0.1") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "simple-error") (r "^0.1.13") (d #t) (k 0)))) (h "0jdjbf1rw95zqg5kjrl1v15y4214bks3v5j3cq2m5drsgwjv2dll")))

(define-public crate-online-0.1.0 (c (n "online") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "simple-error") (r "^0.1.13") (d #t) (k 0)))) (h "1q08dmxjlj2sjql0ffv3yfdw5g26bzln4zx69a182wh7k9krpq8y")))

(define-public crate-online-0.1.1 (c (n "online") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "simple-error") (r "^0.1.13") (d #t) (k 0)))) (h "09v1p11jz89j2cnqmjwqlxqd20ycnz648if5l7x61xy8ch0xp3bg")))

(define-public crate-online-0.1.2 (c (n "online") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "simple-error") (r "^0.1.13") (d #t) (k 0)))) (h "1carkw06ysyjk3phk0p84y24yf0f1czi1d285ld3hbxz1lz17zaj")))

(define-public crate-online-0.2.0 (c (n "online") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "simple-error") (r "^0.1.13") (d #t) (k 0)))) (h "1c0knpcm29k5gh5w4pclszm3j1kas649s1cxqiswzib65l6pz4v8")))

(define-public crate-online-0.2.2 (c (n "online") (v "0.2.2") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)))) (h "14cinvx517p8kk7kgmdkg7bqxi8jikyk5xzzfaq02alizhcc3344")))

(define-public crate-online-1.0.0 (c (n "online") (v "1.0.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "14ss18c64m2ygikbjz8kgirzl7mxl5kmfs9fm0kzsrj4zkh6k4rh")))

(define-public crate-online-2.0.0 (c (n "online") (v "2.0.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0dc81sn7fdd5g0gxvbska2qbrya0r166mml336cdv4idq73fbyy0")))

(define-public crate-online-3.0.0 (c (n "online") (v "3.0.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "0ss6fhak069s0zqx7vnbqi0zrac31pkszap9909li0g6ayszdzbx") (f (quote (("sync") ("default" "async-std-runtime") ("async-std-runtime" "async-std"))))))

(define-public crate-online-3.0.1 (c (n "online") (v "3.0.1") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "1rf5iv8ii1lah7ni94a0lmipra5dmgwm2zvhc40mqyahsmdri03n") (f (quote (("sync") ("default" "async-std-runtime") ("async-std-runtime" "async-std"))))))

(define-public crate-online-3.0.2 (c (n "online") (v "3.0.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.3") (d #t) (k 2)))) (h "0h1mwlyx70fhhydl41dbjwajjs0k0c70g6v39kh6x02qjs7h10yd") (f (quote (("sync") ("default" "async-std-runtime") ("async-std-runtime" "async-std"))))))

(define-public crate-online-4.0.0 (c (n "online") (v "4.0.0") (d (list (d (n "pretty_assertions") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("net" "time" "rt-multi-thread" "macros"))) (o #t) (k 0)))) (h "0szmkmaavlg0ypimxmfyn7bicdw8nb1g6ysna27ilmbzymvxmwwj") (f (quote (("tokio-runtime" "tokio") ("sync-runtime") ("default" "sync-runtime"))))))

(define-public crate-online-4.0.1 (c (n "online") (v "4.0.1") (d (list (d (n "pretty_assertions") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("net" "time" "rt-multi-thread" "macros"))) (o #t) (k 0)))) (h "1qb8z43fg7m73k4m1zlwhhpbq8zj6rakblffzrikzhznx8wgpc98") (f (quote (("tokio-runtime" "tokio") ("sync-runtime") ("default" "sync-runtime"))))))

(define-public crate-online-4.0.2 (c (n "online") (v "4.0.2") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("net" "time" "rt-multi-thread" "macros"))) (o #t) (k 0)))) (h "0hzcnnl260l6g63s4n6qf11v0vp82a4rbxwmm3mqan76wxalnkb4") (f (quote (("tokio-runtime" "tokio") ("sync-runtime") ("default" "sync-runtime"))))))

