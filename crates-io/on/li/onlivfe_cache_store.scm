(define-module (crates-io on li onlivfe_cache_store) #:use-module (crates-io))

(define-public crate-onlivfe_cache_store-0.0.0-alpha.0 (c (n "onlivfe_cache_store") (v "0.0.0-alpha.0") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "bson") (r "^2") (d #t) (k 0)) (d (n "chilloutvr") (r "^0.2.1") (d #t) (k 0)) (d (n "directories") (r "^4") (d #t) (k 0)) (d (n "neos") (r "^0.6.1") (d #t) (k 0)) (d (n "onlivfe") (r "=0.0.0-alpha.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "vrc") (r "^0.1.0-alpha.3") (d #t) (k 0)))) (h "0i22narm8znw0574czzyrlwqwv3h6ndh67khcsqrbxhds9bvrnaq") (y #t)))

