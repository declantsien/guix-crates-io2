(define-module (crates-io on li onlinematching) #:use-module (crates-io))

(define-public crate-onlinematching-0.1.0 (c (n "onlinematching") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1k90cbrsci51zkpddld976af35jkcmq72a92zffq84ljgvpsxw8b")))

(define-public crate-onlinematching-0.1.1 (c (n "onlinematching") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1120mprrz3xdspygvvy7sqhl9gyw1dpac1c11ncq0x5da73gyyy5")))

(define-public crate-onlinematching-0.1.3 (c (n "onlinematching") (v "0.1.3") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0xkjpfcrr0mlbvmfq62rxp775j4gv4rslgmdb79k1y7f837v8ngd")))

(define-public crate-onlinematching-0.2.1 (c (n "onlinematching") (v "0.2.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0dv7aj529y2l3dwvfz4w4mf3ikikwkdfjb62js9grkb8d7f8sa4d")))

(define-public crate-onlinematching-0.2.5 (c (n "onlinematching") (v "0.2.5") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "17jvz8m1d3avaz3c6z67wvc4p3rrigdx0y2vzbv0r66jh0lgghz4")))

(define-public crate-onlinematching-0.3.0 (c (n "onlinematching") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0vakvqakb2i6fkwngmcm3jnidi59vsf4408lznsahh8qfq3bs186")))

(define-public crate-onlinematching-0.3.1 (c (n "onlinematching") (v "0.3.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1ypim0sd62d3kjvw9jz9qw0s3b45gr670f1h8113ip1zzn371d1f")))

(define-public crate-onlinematching-0.3.2 (c (n "onlinematching") (v "0.3.2") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1nn9csz2zld3hpw8s5933rcb91zzyb40j5r85k0qq9w3f2gylgi7")))

