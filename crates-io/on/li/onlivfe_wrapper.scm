(define-module (crates-io on li onlivfe_wrapper) #:use-module (crates-io))

(define-public crate-onlivfe_wrapper-0.0.0-alpha.0 (c (n "onlivfe_wrapper") (v "0.0.0-alpha.0") (d (list (d (n "dotenvy") (r "^0.15.6") (d #t) (k 0)) (d (n "onlivfe") (r "=0.0.0-alpha.0") (d #t) (k 0)) (d (n "onlivfe_net") (r "=0.0.0-alpha.0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1pb83iih50ql8gvl863am3x84mbgpg9vlzjfz2z38r2sycrgx40q") (y #t)))

