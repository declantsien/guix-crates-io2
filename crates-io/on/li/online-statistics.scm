(define-module (crates-io on li online-statistics) #:use-module (crates-io))

(define-public crate-online-statistics-0.1.0 (c (n "online-statistics") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11rmg9vvp004rs01dibq0mlq0yhc1pdsi2w2rvmr5a0gxppmng38")))

(define-public crate-online-statistics-0.2.0 (c (n "online-statistics") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04ixm6r9mhk68rnsmiyfjcyzjnpc9850h7vya47jd9165c0pdcjw")))

(define-public crate-online-statistics-0.2.1 (c (n "online-statistics") (v "0.2.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r7hsanfh149if8q9pn0niz87qifbqxpndm93059bfdmwkxjxchw")))

(define-public crate-online-statistics-0.2.2 (c (n "online-statistics") (v "0.2.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "115h9jpibm5038zrr1ifsbvczr09xarl0khd9n011nsbyhdpcclm")))

(define-public crate-online-statistics-0.2.3 (c (n "online-statistics") (v "0.2.3") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yasqw8h6qdnw03xim6zhp00i8d098la879agnwv989180zi4pg9")))

(define-public crate-online-statistics-0.2.4 (c (n "online-statistics") (v "0.2.4") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zvwkavgqli01dvl6m6ch6h3g6xqmslni3c3g2pmxqk2kx31pw61")))

(define-public crate-online-statistics-0.2.5 (c (n "online-statistics") (v "0.2.5") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1krdnvli9vq3f9mkz8w87kmm2ihdppw1l7agv39hvj6rlfxvxpcz")))

(define-public crate-online-statistics-0.2.6 (c (n "online-statistics") (v "0.2.6") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17nj7s2iismqgf8j9838sv7qxg93mjk287hl8bcml528vz955bbn")))

