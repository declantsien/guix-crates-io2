(define-module (crates-io on li onlivfe_db_store) #:use-module (crates-io))

(define-public crate-onlivfe_db_store-0.0.0-alpha.0 (c (n "onlivfe_db_store") (v "0.0.0-alpha.0") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "chilloutvr") (r "^0.2.1") (d #t) (k 0)) (d (n "neos") (r "^0.6.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "onlivfe") (r "=0.0.0-alpha.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.0-alpha.1") (f (quote ("runtime-tokio" "sqlite" "json" "uuid" "time" "migrate" "macros"))) (k 0)) (d (n "vrc") (r "^0.1.0-alpha.3") (d #t) (k 0)))) (h "0mmwl7hd34p46hmfw2s4ha66cjmrqf0mqj9yh2pgzxnsgajkklyf") (y #t)))

