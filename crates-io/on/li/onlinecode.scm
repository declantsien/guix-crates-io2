(define-module (crates-io on li onlinecode) #:use-module (crates-io))

(define-public crate-onlinecode-0.1.0 (c (n "onlinecode") (v "0.1.0") (h "1wnc405b8j723bj0z0j75060n66mknnd524lcmjsdjp2fr1r9fs1")))

(define-public crate-onlinecode-0.1.1 (c (n "onlinecode") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.6") (d #t) (k 0)))) (h "10aixq9dnx6djr974nm5r6ndi8529wy3lcv68flaksh8nkpzfrkx")))

