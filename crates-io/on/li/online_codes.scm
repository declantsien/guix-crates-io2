(define-module (crates-io on li online_codes) #:use-module (crates-io))

(define-public crate-online_codes-0.1.0 (c (n "online_codes") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_core") (r "^0.3") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.1") (d #t) (k 0)))) (h "0ikxkjqwgrp8knj93jz5f62x8qhd3kxgw4czvqra4jkazw6f173b")))

