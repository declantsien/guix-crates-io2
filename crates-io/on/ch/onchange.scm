(define-module (crates-io on ch onchange) #:use-module (crates-io))

(define-public crate-onchange-0.1.0 (c (n "onchange") (v "0.1.0") (h "0f012hpqhrkk560fi11i4rz75m1f1h4fs18idrcwxf9397whxpn5") (y #t)))

(define-public crate-onchange-0.1.1 (c (n "onchange") (v "0.1.1") (h "07ivifklqlim4znf8s24gihyzcg9pfl4bsvin6is3ysd17spjrja")))

