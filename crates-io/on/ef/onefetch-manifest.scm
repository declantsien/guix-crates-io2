(define-module (crates-io on ef onefetch-manifest) #:use-module (crates-io))

(define-public crate-onefetch-manifest-2.14.1 (c (n "onefetch-manifest") (v "2.14.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.13.0") (d #t) (k 0)) (d (n "npm-package-json") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d7s4649dh2ixrnnjk71j27a2hm6522vcc2zzkzx3krv2ms4dynj")))

(define-public crate-onefetch-manifest-2.14.2 (c (n "onefetch-manifest") (v "2.14.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.13.0") (d #t) (k 0)) (d (n "npm-package-json") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "065bbg127q3aa7q4cz2fazs5wz3pb23pnzpn6frsc443lcqhdj34")))

(define-public crate-onefetch-manifest-2.15.0 (c (n "onefetch-manifest") (v "2.15.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.13.0") (d #t) (k 0)) (d (n "npm-package-json") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12bsw9yv55n7a909hpp5hvbqkdqz0rnkddg3vpvn94pmzjg9kycr")))

(define-public crate-onefetch-manifest-2.15.1 (c (n "onefetch-manifest") (v "2.15.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.13.0") (d #t) (k 0)) (d (n "npm-package-json") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jwqpishmliywlbm9cin4y1n5ilr8wz98d09hyr749907hj43ads")))

(define-public crate-onefetch-manifest-2.16.0 (c (n "onefetch-manifest") (v "2.16.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.14.1") (d #t) (k 0)) (d (n "npm-package-json") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p6h64jfxalk7ambb8zai095hv67fhdiiyj275jz5xp93w9hnpfp")))

(define-public crate-onefetch-manifest-2.17.0 (c (n "onefetch-manifest") (v "2.17.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.15.2") (d #t) (k 0)) (d (n "npm-package-json") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06d4ybqs8h65aghgkwg57ncxgnqnlapmdsvy1pf1i9xjs8qgq1k7")))

(define-public crate-onefetch-manifest-2.17.1 (c (n "onefetch-manifest") (v "2.17.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.15.2") (d #t) (k 0)) (d (n "npm-package-json") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zwnwg2mpaqdmrs6p51s2ibvzmhphp0s3xjddgzmcfhrpcniwwm8")))

(define-public crate-onefetch-manifest-2.18.0 (c (n "onefetch-manifest") (v "2.18.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.15.3") (d #t) (k 0)) (d (n "npm-package-json") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fx2pgim3ady7i3vvhidggvdnl7r16a0wdj4989i22l1ahfslqdp")))

(define-public crate-onefetch-manifest-2.18.1 (c (n "onefetch-manifest") (v "2.18.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.15.3") (d #t) (k 0)) (d (n "npm-package-json") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v9i7qqbif25zg16h3fvn0yqk9jqibfxzi9hjmaw08apbad5kggk")))

(define-public crate-onefetch-manifest-2.19.0 (c (n "onefetch-manifest") (v "2.19.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.17.1") (d #t) (k 0)) (d (n "npm-package-json") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11zp7n6pfg8p5v0izpzdvw7crhv096iavyz2vja4djb5q52a5m7h")))

(define-public crate-onefetch-manifest-2.20.0 (c (n "onefetch-manifest") (v "2.20.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.19.1") (d #t) (k 0)) (d (n "npm-package-json") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qbwqq5scrshmcvhk8sijca5jm5wmr44wwl6lpgw43ibw34zp2qr")))

(define-public crate-onefetch-manifest-2.21.0 (c (n "onefetch-manifest") (v "2.21.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.20.2") (d #t) (k 0)) (d (n "npm-package-json") (r "^0.1.3") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ym3f283mcn53lrldkggbf9csfcp7dd9sxn2h1ii0q0n76w45l6g")))

