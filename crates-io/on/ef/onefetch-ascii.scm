(define-module (crates-io on ef onefetch-ascii) #:use-module (crates-io))

(define-public crate-onefetch-ascii-2.15.0 (c (n "onefetch-ascii") (v "2.15.0") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1cfaqxm5lqm99j2cvkdj3z9cj2qh8ikm5d1i0xz0rlyh12if5i8f")))

(define-public crate-onefetch-ascii-2.15.1 (c (n "onefetch-ascii") (v "2.15.1") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "15790657k77i8adxqpf9d5sg4dsaf5ylr9pqj8fcmd10hzv8w7qj")))

(define-public crate-onefetch-ascii-2.16.0 (c (n "onefetch-ascii") (v "2.16.0") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0myq6rpk4jr0hsxgd4hhvpmccpnc3pa4k0i8rqvq7zhlxmd0rr0k")))

(define-public crate-onefetch-ascii-2.17.0 (c (n "onefetch-ascii") (v "2.17.0") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1zp5wbf5n161blksdgnnlb7p1801z5r4dirw4cwgwm45d9r1w71x")))

(define-public crate-onefetch-ascii-2.17.1 (c (n "onefetch-ascii") (v "2.17.1") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1vipb251dyrzamfvwh67as7i85qgcbmn1sfd0061z4mqjvbla36a")))

(define-public crate-onefetch-ascii-2.18.0 (c (n "onefetch-ascii") (v "2.18.0") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "00h3jba0hsnspk1qgkq7jfbhqmiscywgc02fi2bwxg186614mxqw")))

(define-public crate-onefetch-ascii-2.18.1 (c (n "onefetch-ascii") (v "2.18.1") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "12crhs9kna86y3m46si8z5nhjsh0r1avw6cyzbgzny0bd5jb393y")))

(define-public crate-onefetch-ascii-2.19.0 (c (n "onefetch-ascii") (v "2.19.0") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "173h47d5kf62z2394kz5yi67b9k4fi079k0g68km7iy5327gnx9j")))

(define-public crate-onefetch-ascii-2.20.0 (c (n "onefetch-ascii") (v "2.20.0") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "15qlmd6bxfq4i8jdk3jdi54xhihj4qp6v4sj3svz0isnjd4d1ai7")))

(define-public crate-onefetch-ascii-2.21.0 (c (n "onefetch-ascii") (v "2.21.0") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0xi36w5pv3qx9f7kil4k596bxq3dnv2pp2pagnzl22ygqkqdhg1s")))

