(define-module (crates-io on i- oni-comb-toys-rs) #:use-module (crates-io))

(define-public crate-oni-comb-toys-rs-0.0.1 (c (n "oni-comb-toys-rs") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "oni-comb-parser-rs") (r "^0.0.2") (d #t) (k 0)))) (h "1kbqrz7s24n180p6vfq5580vkg9lzmn1dn7wff2vzxprf1fi43yd")))

(define-public crate-oni-comb-toys-rs-0.0.2 (c (n "oni-comb-toys-rs") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "oni-comb-parser-rs") (r "^0.0.5") (d #t) (k 0)))) (h "03xb3bymjk618h4cfxi5h0knv6k1gkp4xiswrxdz34f9r8syqg5b")))

