(define-module (crates-io on ix onix) #:use-module (crates-io))

(define-public crate-onix-0.1.0 (c (n "onix") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "drm") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl" "mman" "poll" "fs"))) (d #t) (k 0)))) (h "0qq9fax0mv9l81jlic9w7q6wx0dlh3fd40h212x8bk96x197gvgn")))

