(define-module (crates-io on c- onc-rpc) #:use-module (crates-io))

(define-public crate-onc-rpc-0.1.0 (c (n "onc-rpc") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "133lahzga05fpbf9f71x1rqccvg0swg4krg7izvndrx8l2s7nw24")))

(define-public crate-onc-rpc-0.2.0 (c (n "onc-rpc") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0zsc18cx0wwi8a5cwdv97ixwvm81z1gc3kl18515navaf3wbx8pk")))

(define-public crate-onc-rpc-0.2.1 (c (n "onc-rpc") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "11ia5m0r6aw7y02nig1y6yqhr3yp8ps3qhy711splw8v38mrj61k")))

(define-public crate-onc-rpc-0.2.2 (c (n "onc-rpc") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1m1ap11wz356161c2j1dccljbvh2k1zrcshp60l1cy4sxfxms38n")))

(define-public crate-onc-rpc-0.2.3 (c (n "onc-rpc") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0wv94kkjvc1fflc47ds03v9ni4s4l0f7y9h3ddmvw18zvfryi50k")))

(define-public crate-onc-rpc-0.2.4 (c (n "onc-rpc") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "11nqnih5k47w78zyb90xmqsjj94mkayfz3g7dpss59qh84i2a8l9")))

(define-public crate-onc-rpc-0.2.5 (c (n "onc-rpc") (v "0.2.5") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "05fnz704fc97wfra309lyc2pqmwzsn4xj06cw5wrd5kb6x8x56l6")))

(define-public crate-onc-rpc-0.3.0 (c (n "onc-rpc") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "bytes") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (f (quote ("alloc" "std"))) (k 2)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "194hx6fslgg7ab09w2rr479dr35ypbzzz0nvalfdfggr2sz88rch") (f (quote (("default" "bytes")))) (s 2) (e (quote (("bytes" "dep:bytes"))))))

