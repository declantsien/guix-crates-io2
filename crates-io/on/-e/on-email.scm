(define-module (crates-io on -e on-email) #:use-module (crates-io))

(define-public crate-on-email-0.1.0 (c (n "on-email") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "imap") (r "2.3.*") (d #t) (k 0)) (d (n "native-tls") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1h8a49cz67zida36idrryfrr4fxg9q20y98nfp6vigzwxgxiq4z8")))

