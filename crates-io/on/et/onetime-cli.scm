(define-module (crates-io on et onetime-cli) #:use-module (crates-io))

(define-public crate-onetime-cli-0.1.0 (c (n "onetime-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1bbrl7lvg2mi4dypcha7pa8hzmxjiyd2vxkd8zg99bclhywmd1qz")))

(define-public crate-onetime-cli-0.2.0 (c (n "onetime-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zf7pr6a9xsjxjpn9j87pisfp582ix18gkl6yxava7qxhrxzddgf")))

(define-public crate-onetime-cli-0.3.0 (c (n "onetime-cli") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "md5-rs") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)))) (h "1y4vl2llrbgad6mg0i3g88iiywh22qa99m003shi4kab9zixkmb4")))

(define-public crate-onetime-cli-0.4.0 (c (n "onetime-cli") (v "0.4.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "md5-rs") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)))) (h "06a0ysnazrhaf8gqpf6s5ihkrb5qck6c03q363il6z3qgnf3j9rs")))

(define-public crate-onetime-cli-0.4.1 (c (n "onetime-cli") (v "0.4.1") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.4.7") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "md5-rs") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)))) (h "07fg0785pn0mp6lnk3lrr49gixxsfrlg7ygji18rgacgpgssbxhh")))

