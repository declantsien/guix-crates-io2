(define-module (crates-io on et onetime) #:use-module (crates-io))

(define-public crate-onetime-0.1.0 (c (n "onetime") (v "0.1.0") (d (list (d (n "event-listener-strategy") (r "^0.5") (k 0)) (d (n "futures-lite") (r "^2") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "smol") (r "^2") (d #t) (k 2)))) (h "0km7mi73gnpz1wz7gbcidqa3ryricbyx8dl1fqyaljrbm6g308q8") (f (quote (("std" "event-listener-strategy/std") ("default" "std"))))))

(define-public crate-onetime-0.1.1 (c (n "onetime") (v "0.1.1") (d (list (d (n "event-listener-strategy") (r "^0.5") (k 0)) (d (n "futures-lite") (r "^2") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "smol") (r "^2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jz91v50ria8kxpx9k0cwqgc1yvkah2m3x171b7y2gq8qipvd6sq") (f (quote (("std" "event-listener-strategy/std") ("default" "std"))))))

(define-public crate-onetime-0.1.2 (c (n "onetime") (v "0.1.2") (d (list (d (n "event-listener-strategy") (r "^0.5") (k 0)) (d (n "futures-lite") (r "^2") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "smol") (r "^2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ik4la5w22bami6kx3di4iml799qbhchi2jh7x8qma1xcv8xq789") (f (quote (("std" "event-listener-strategy/std") ("default" "std"))))))

