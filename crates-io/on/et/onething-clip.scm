(define-module (crates-io on et onething-clip) #:use-module (crates-io))

(define-public crate-onething-clip-0.1.0 (c (n "onething-clip") (v "0.1.0") (d (list (d (n "config") (r "^0.14.0") (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.26") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vjwcvmwlh8apzj2ajh70zdqc9bha0q8h40cdilxwkkqm3sqbi4c")))

(define-public crate-onething-clip-0.1.1 (c (n "onething-clip") (v "0.1.1") (d (list (d (n "config") (r "^0.14.0") (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.26") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "003s3qbbb0x92z216b1ll5zc2vf66v494ykxnj1062j89z5a4w0z")))

