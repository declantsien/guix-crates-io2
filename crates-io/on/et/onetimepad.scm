(define-module (crates-io on et onetimepad) #:use-module (crates-io))

(define-public crate-onetimepad-0.1.0 (c (n "onetimepad") (v "0.1.0") (d (list (d (n "clap") (r "^4.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "18mx8plav1wb5c9b23j5jnnxan5avaz4ldw41b7515rh9wlszg4h") (s 2) (e (quote (("rand" "dep:rand") ("binary" "dep:clap" "clap/derive" "rand"))))))

