(define-module (crates-io on ec onechatsocial-presence) #:use-module (crates-io))

(define-public crate-onechatsocial-presence-0.6.13 (c (n "onechatsocial-presence") (v "0.6.13") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "redis-kiss") (r "^0.1.4") (d #t) (k 0)))) (h "0zwllcdjrva90jj0n2f3izwqkc9mlakahc7m5avpl812dwl16mls") (f (quote (("redis-is-patched"))))))

(define-public crate-onechatsocial-presence-0.6.14 (c (n "onechatsocial-presence") (v "0.6.14") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "redis-kiss") (r "^0.1.4") (d #t) (k 0)))) (h "1h095n5ckbmvkyfmmxd2swbf7ramkzn717aswq3zahwj8137w1zp") (f (quote (("redis-is-patched"))))))

(define-public crate-onechatsocial-presence-0.6.15 (c (n "onechatsocial-presence") (v "0.6.15") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "redis-kiss") (r "^0.1.4") (d #t) (k 0)))) (h "1nn49m9b777yjkj9zid1li92cdwlz1zj5s1cf4hhnqlc09nz1jp2") (f (quote (("redis-is-patched"))))))

(define-public crate-onechatsocial-presence-0.6.16 (c (n "onechatsocial-presence") (v "0.6.16") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "redis-kiss") (r "^0.1.4") (d #t) (k 0)))) (h "0iay4p6jvi4wvknm2558ckll7xhmyjd4v5z6c6prn0r04q7laaad") (f (quote (("redis-is-patched"))))))

