(define-module (crates-io on ag onager) #:use-module (crates-io))

(define-public crate-onager-0.0.1 (c (n "onager") (v "0.0.1") (h "0j3drc8hvfmb39n0qyzis7c0cx1alfa38crbi44dlzxf4swp833z")))

(define-public crate-onager-0.0.2 (c (n "onager") (v "0.0.2") (h "0s3mzzdclrpq7l9j4ix8znz8w8ijp2bkacyxavjnq73y4a803879")))

