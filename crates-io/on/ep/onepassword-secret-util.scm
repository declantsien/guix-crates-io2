(define-module (crates-io on ep onepassword-secret-util) #:use-module (crates-io))

(define-public crate-onepassword-secret-util-0.0.1 (c (n "onepassword-secret-util") (v "0.0.1") (d (list (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1zri1f10k3gk84j6n2k76rf470zmy9h1a0bkbcr37cm5y953b53m")))

(define-public crate-onepassword-secret-util-0.0.2 (c (n "onepassword-secret-util") (v "0.0.2") (d (list (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "03vyyzq35k9sd0zpkdxzgskl674fi027bff447430b4drmrnjian")))

(define-public crate-onepassword-secret-util-0.0.3 (c (n "onepassword-secret-util") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1adyh14l5gl379x1c9azbsaai1vvzwzwa4qpvx4yrsg70vzqwc0k")))

(define-public crate-onepassword-secret-util-0.0.4 (c (n "onepassword-secret-util") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1sx1c72ygy1xnkhnx3xr4whdhdcmpw49qqb7qnmqi3k2q46jjd6k")))

(define-public crate-onepassword-secret-util-0.0.5 (c (n "onepassword-secret-util") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0v03l75isb5y228yp46s51xs6dszlymk4f952a9k3xsx06g98l1j")))

(define-public crate-onepassword-secret-util-0.0.6 (c (n "onepassword-secret-util") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0yvk7gfdcjkqi0fx4kr6r7si1c4wsyfi1p460d2zmrhq0vsvh76r")))

