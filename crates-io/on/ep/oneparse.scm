(define-module (crates-io on ep oneparse) #:use-module (crates-io))

(define-public crate-oneparse-0.1.0 (c (n "oneparse") (v "0.1.0") (h "1la6gjiyxr3gcv464wqcl32l76nv64bhq9h5p6f214yg4vl3p5q9")))

(define-public crate-oneparse-0.1.1 (c (n "oneparse") (v "0.1.1") (h "1mr49g88sr3p1pgqbmm8lg2lsrbc7nnkns6ff4sjw9z5m3qq80a0")))

(define-public crate-oneparse-0.1.2 (c (n "oneparse") (v "0.1.2") (h "12c93r6c3l3zzdl83hb5dhq289qwmfzcmx3linkss9fdkqpw17gy")))

(define-public crate-oneparse-0.1.4 (c (n "oneparse") (v "0.1.4") (h "0wxnh1smhjrf486dhq9a8k3x8l4z4rk408cfb6dx0gphmajjb1sf")))

