(define-module (crates-io on i_ oni_trace) #:use-module (crates-io))

(define-public crate-oni_trace-0.1.0 (c (n "oni_trace") (v "0.1.0") (d (list (d (n "deflate") (r "^0.7.18") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "1iw6szpl1n2x9xv57liv4sn9r1jx8xmqdv8sj14j727291v5gjcn") (f (quote (("trace_location") ("trace"))))))

