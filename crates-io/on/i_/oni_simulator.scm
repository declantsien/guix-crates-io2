(define-module (crates-io on i_ oni_simulator) #:use-module (crates-io))

(define-public crate-oni_simulator-0.1.0 (c (n "oni_simulator") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "slotmap") (r "^0.2") (d #t) (k 0)))) (h "132fpdkwxs040pj1dsgzqy5afgcpbr8hkdcfb09bvw5kqj660n8w")))

(define-public crate-oni_simulator-0.1.2 (c (n "oni_simulator") (v "0.1.2") (d (list (d (n "generic-array") (r "^0.11") (d #t) (k 0)) (d (n "oni_trace") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "slotmap") (r "^0.2") (d #t) (k 0)))) (h "1w8jjy5hcckg5zxzp8s5gnh2rlkrkhs7xiibigvdz8lajkj5cmfy")))

