(define-module (crates-io on la onla) #:use-module (crates-io))

(define-public crate-onla-0.1.0 (c (n "onla") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "014x7q4lsyrhzlyavkwgazxczsf3khzbs4wr4hwxfd0xs4h4i8rr") (y #t)))

