(define-module (crates-io on el oneline-eyre) #:use-module (crates-io))

(define-public crate-oneline-eyre-0.1.0 (c (n "oneline-eyre") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)))) (h "15rh9ih8jpy104zfvyzx2ifiq465fvklisjql66cxh49wshifbw6") (r "1.56")))

