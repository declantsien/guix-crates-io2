(define-module (crates-io on el onelo) #:use-module (crates-io))

(define-public crate-onelo-0.1.0 (c (n "onelo") (v "0.1.0") (d (list (d (n "clap") (r "=3.0.0-beta.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24") (f (quote ("bundled" "functions" "limits" "load_extension"))) (d #t) (k 0)))) (h "0r8ypkhq4p6f1f80khqbcmxi8q7db5qd6vzpa83ajcm2hlrsli6c")))

