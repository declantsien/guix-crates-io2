(define-module (crates-io on el oneline-template) #:use-module (crates-io))

(define-public crate-oneline-template-0.1.0 (c (n "oneline-template") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "unstructured") (r "^0.5") (d #t) (k 0)))) (h "0ga748sqcsz9d35123n2yyn329rqy1jzxfa821kacg17z7iyqzwl")))

