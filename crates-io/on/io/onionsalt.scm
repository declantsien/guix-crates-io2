(define-module (crates-io on io onionsalt) #:use-module (crates-io))

(define-public crate-onionsalt-0.2.0 (c (n "onionsalt") (v "0.2.0") (d (list (d (n "arrayref") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1c0c1byqvjyj3jlg5lqaxz8jmf3482fp3m96wfn5szs52xyqd9h0")))

(define-public crate-onionsalt-0.2.1 (c (n "onionsalt") (v "0.2.1") (d (list (d (n "arrayref") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1pdk54nlyalmy2nmxkv2dv4kgqb1in6l0pnhdjkgr3zg4ljw59m0")))

(define-public crate-onionsalt-0.2.2 (c (n "onionsalt") (v "0.2.2") (d (list (d (n "arrayref") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "05irxvkgsiayrs296wyfjf47y5xddz1gk9mpzh2lqprf5v29iyi6")))

(define-public crate-onionsalt-0.4.0 (c (n "onionsalt") (v "0.4.0") (d (list (d (n "arrayref") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0yzg3s1d2pb29yfrhhqr9py1fb24mzchlgiy18grl1l89fivpqqi")))

(define-public crate-onionsalt-0.4.1 (c (n "onionsalt") (v "0.4.1") (d (list (d (n "arrayref") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0gfp5wk737bdsbc74hbinm97ymbg5dqblh39lpxnlm185hy6846s")))

(define-public crate-onionsalt-0.4.2 (c (n "onionsalt") (v "0.4.2") (d (list (d (n "arrayref") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "1vb82q2ccnv9d30xnqda9sy18w9h7zf583m01zw0c1p0icyhpyr7")))

