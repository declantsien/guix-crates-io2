(define-module (crates-io on io oniongen-rs) #:use-module (crates-io))

(define-public crate-oniongen-rs-0.1.0 (c (n "oniongen-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "12hgk2qkarhqjz9mr8sg69060nykwps7jgv022036lwx4qd2c205")))

(define-public crate-oniongen-rs-0.2.0 (c (n "oniongen-rs") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "0bq6pdx6h1d2nzg9x02hap78nc3rind6h7441719h33g3byif17m")))

(define-public crate-oniongen-rs-0.3.0 (c (n "oniongen-rs") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "16vlg2v1hjx05bhhjawdpslczmamvaxqm7a0f2kq46vr6vqyzv4l")))

(define-public crate-oniongen-rs-0.3.1 (c (n "oniongen-rs") (v "0.3.1") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "1v29kijdp0rmsb3jqhja15z85nqivvxzl23w3ccynvnlq8a3gf8i")))

(define-public crate-oniongen-rs-0.3.2 (c (n "oniongen-rs") (v "0.3.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("std"))) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("std" "u64_backend"))) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "0sm11dw8v9k9p2kza0x2nv782m82nz9i9gcqjwkwx4q5x8q1fqnz")))

(define-public crate-oniongen-rs-0.4.0 (c (n "oniongen-rs") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("std"))) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("std" "u64_backend"))) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "11ihwakpad0gqbj2rwif64bxf56j8syqalrq3q8vixxivqpmg2b4")))

(define-public crate-oniongen-rs-0.4.1 (c (n "oniongen-rs") (v "0.4.1") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("std"))) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("std" "u64_backend"))) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "1h3c37k2gdjrp7q954hb0qgjxvjg3694c1q48agdfxsycd67rjpa")))

(define-public crate-oniongen-rs-0.4.2 (c (n "oniongen-rs") (v "0.4.2") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("std"))) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("std" "u64_backend"))) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "00m7pp5cw70an7xrcysac88xm7mghmi9h5hpvxi6g7195awlpfc2")))

(define-public crate-oniongen-rs-0.4.3 (c (n "oniongen-rs") (v "0.4.3") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("std"))) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("std" "u64_backend"))) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "1fyc0zhf3wplrhjrkc3mjz3m04ps7b7qzi5bvmqd2czipr0hf42a")))

(define-public crate-oniongen-rs-0.4.4 (c (n "oniongen-rs") (v "0.4.4") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("std"))) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("std" "u64_backend"))) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "0h55mz7f575gng058p5fdi2cqwpa0psy4xrz7iiwpi535a4jvc21")))

(define-public crate-oniongen-rs-0.4.5 (c (n "oniongen-rs") (v "0.4.5") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("std"))) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("std" "u64_backend"))) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.2") (d #t) (k 0)))) (h "1zkpib6085d1lr7wnfx31ry40mjh1kxrwfrfjsm84cy1g53kaykn")))

(define-public crate-oniongen-rs-0.5.0 (c (n "oniongen-rs") (v "0.5.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("std"))) (k 0)) (d (n "ed25519-compact") (r "^1.0.11") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.2") (d #t) (k 0)))) (h "1j1fkssnvgsx5c4696hk695l34qs0jdalq9rmahi6g2blx98s52z")))

(define-public crate-oniongen-rs-0.5.1 (c (n "oniongen-rs") (v "0.5.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("std"))) (k 0)) (d (n "ed25519-compact") (r "^1.0.11") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.2") (d #t) (k 0)))) (h "0z6mbxj7mzaf6xhm0nz40nz692255hkwvjc7kvvmhzv2cvbx8wd5")))

(define-public crate-oniongen-rs-0.5.2 (c (n "oniongen-rs") (v "0.5.2") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("std"))) (k 0)) (d (n "ed25519-compact") (r "^1.0.11") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.4") (d #t) (k 0)))) (h "0kpr8fq1sidg60alb8480qxq5b5by7imvsg6nn676m2g9f6b683y")))

(define-public crate-oniongen-rs-0.6.0 (c (n "oniongen-rs") (v "0.6.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("std"))) (k 0)) (d (n "ed25519-compact") (r "^1.0.11") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.4") (d #t) (k 0)))) (h "0z9s256c38ss9nrr7vb1rc4npkrpq1gxfnly2001wam7pcdyxikw")))

(define-public crate-oniongen-rs-0.6.1 (c (n "oniongen-rs") (v "0.6.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("std"))) (k 0)) (d (n "ed25519-compact") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.5") (d #t) (k 0)))) (h "12gyspzsq107jc3arbb464j4p1vkl39kxqhajylphwpmyikhphd5")))

(define-public crate-oniongen-rs-0.6.2 (c (n "oniongen-rs") (v "0.6.2") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("cargo" "help" "error-context" "suggestions" "std" "usage"))) (k 0)) (d (n "ed25519-compact") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.5") (d #t) (k 0)))) (h "1lhp43whjjlr7mqq9lczg75ihf1cfx7dff9s15gl4srr6kwjwl5l")))

(define-public crate-oniongen-rs-0.6.3 (c (n "oniongen-rs") (v "0.6.3") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("cargo" "help" "error-context" "suggestions" "std" "usage"))) (k 0)) (d (n "ed25519-compact") (r "^2.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "0qdx15d8albwbn3kwikagw0nsl8brhkv3iay3yih16z4jj19psln")))

(define-public crate-oniongen-rs-0.6.4 (c (n "oniongen-rs") (v "0.6.4") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("cargo" "help" "error-context" "suggestions" "std" "usage"))) (k 0)) (d (n "ed25519-compact") (r "^2.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "0qx8ry5af0rggyd2hb3zisv4ybnk7rzf6a2r5manpypv824q8vzy")))

(define-public crate-oniongen-rs-0.6.5 (c (n "oniongen-rs") (v "0.6.5") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("cargo" "help" "error-context" "suggestions" "std" "usage"))) (k 0)) (d (n "ed25519-compact") (r "^2.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "0rz0pi2sga6bk45fvf788iwzvfp9sb0lmw7wzkkmbfy5ndk6jqby")))

