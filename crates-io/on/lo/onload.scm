(define-module (crates-io on lo onload) #:use-module (crates-io))

(define-public crate-onload-0.1.0 (c (n "onload") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)))) (h "09jhv48nhf4cy94vz0rpw2mfqmrgpdvphcsn66zklbhhi1hij15z") (f (quote (("release") ("default" "release") ("debug"))))))

(define-public crate-onload-0.1.1 (c (n "onload") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)))) (h "0j1sn0ljvl9ziczdz8xd7bxx4x3ijgxk00p2kbsx576jnbfjb4vx") (f (quote (("release") ("default" "release") ("debug"))))))

(define-public crate-onload-0.1.2 (c (n "onload") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)))) (h "12f4vlk6azllmbigy8clmz0sl5rc2z9yyg31b5xj5jqh3dwmic4j") (f (quote (("release") ("default" "release") ("debug" "bindgen"))))))

(define-public crate-onload-0.1.3 (c (n "onload") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)))) (h "1zml9lnfhgy3kpcnn31d5gqc3m4rbr4py4r8h30grp0jlhgi2gfg") (f (quote (("release") ("default" "release") ("debug" "bindgen"))))))

(define-public crate-onload-0.2.0 (c (n "onload") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)))) (h "16cm9hyrhymahshvx2d2yjcwnzmr5wy1c3g3x4szfqpkvd1z5086") (f (quote (("release") ("default" "release") ("debug" "bindgen"))))))

