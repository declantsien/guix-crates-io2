(define-module (crates-io on em onemax-oxigen) #:use-module (crates-io))

(define-public crate-onemax-oxigen-1.0.0 (c (n "onemax-oxigen") (v "1.0.0") (d (list (d (n "oxigen") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0h76pgbq7m0ii67dvac8v0pa2187a1qm46l5lzlhziz0rm0bmqxy")))

(define-public crate-onemax-oxigen-2.0.0 (c (n "onemax-oxigen") (v "2.0.0") (d (list (d (n "oxigen") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0jz0lz8db69m0hdcrh99xrir7f6zw0m9fjl2b0srjpr0q486z12z")))

