(define-module (crates-io on ei oneitfarm-interface-httpserver) #:use-module (crates-io))

(define-public crate-oneitfarm-interface-httpserver-0.7.0 (c (n "oneitfarm-interface-httpserver") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.10.0") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.5.0") (d #t) (k 1)))) (h "1qdqizlrryr4c1smgz4rf9mip4zb7mwi77wilqianl6nza4c9k78") (f (quote (("default"))))))

(define-public crate-oneitfarm-interface-httpserver-0.7.1 (c (n "oneitfarm-interface-httpserver") (v "0.7.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.10.0") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.5.0") (d #t) (k 1)))) (h "15975x1fsv2f5hhxq763lv6qb4dq3f2gl1n6m5vj5cbai5lj8jw4") (f (quote (("default"))))))

