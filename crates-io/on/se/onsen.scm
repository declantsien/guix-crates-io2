(define-module (crates-io on se onsen) #:use-module (crates-io))

(define-public crate-onsen-0.1.0 (c (n "onsen") (v "0.1.0") (h "0inw7adqa4i8rwwzkfsv8ydfx3556j9zdl32lwkisbc108i9966h")))

(define-public crate-onsen-0.2.0 (c (n "onsen") (v "0.2.0") (h "01xp05a34859vbr4bdiqnxw64pqy1pcxlmnhfprsf8nmk8cyw5qm")))

(define-public crate-onsen-0.3.0 (c (n "onsen") (v "0.3.0") (h "1d3vzzq3bqa9syhcvh1z9jvrq3y2micd2x3icqa6dq71sgj20lqv")))

(define-public crate-onsen-0.4.0 (c (n "onsen") (v "0.4.0") (h "0bhqnk4b26yq64qf5wlvsvw3wxwyyp00yy0c5vprq9f6zbi9qgxm")))

(define-public crate-onsen-0.5.0 (c (n "onsen") (v "0.5.0") (h "0363ciarghfx42dp4vzn1849yg447kf8xgizsk2k99kgf8bawb2l")))

(define-public crate-onsen-0.6.0 (c (n "onsen") (v "0.6.0") (h "121sri6yk4jl468hvsyzpcfkiwllm4diwk9v6inkbz9x61syqrcf")))

(define-public crate-onsen-0.6.1 (c (n "onsen") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1y0s4viz1vq42gzj2pf9qhpgdbrzla6l8dyn71pircfhhvwk45cc")))

(define-public crate-onsen-0.7.0 (c (n "onsen") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1imphjwhp9gsbw1kq86q4inak1km91s3xrg1q5jndhlpvg4m9a9w")))

(define-public crate-onsen-0.7.1 (c (n "onsen") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0gbpdcyknvwj0qvhxjmnzb4bblcwdraqwvrmxchi260fibim7rvb")))

(define-public crate-onsen-0.8.0 (c (n "onsen") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "06lyill60nvdpz2a2rqab1c1v5kv4sqq5z2lm74ndk0plprnclnf")))

(define-public crate-onsen-0.9.0 (c (n "onsen") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "15f029z3fbjypbihw26w5lfn4wxid8ygf5mmi6arhlmgsh98wbsy")))

(define-public crate-onsen-0.9.1 (c (n "onsen") (v "0.9.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "05gkrivnfcbx48xixyrk1m0d6rgah8rvpqc7bngddkkf5p7a3l3v")))

(define-public crate-onsen-0.10.0 (c (n "onsen") (v "0.10.0") (d (list (d (n "assoc_static") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "threadcell") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "09rvw8nqqdfkw8vb3gvw718mdwhfvkkfx1ff2scqyn59pl6sjzra") (f (quote (("st_tbox" "tbox" "stpool") ("default" "st_tbox")))) (s 2) (e (quote (("tbox" "dep:assoc_static") ("stpool" "dep:threadcell"))))))

(define-public crate-onsen-0.11.0 (c (n "onsen") (v "0.11.0") (d (list (d (n "assoc_static") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)) (d (n "threadcell") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0gbwgxk8i556aps9f9pwnab9jbkpp20hjg3khaz81xka9ab99acr") (f (quote (("st_tbox" "tbox" "stpool") ("default" "st_tbox")))) (s 2) (e (quote (("tbox" "dep:assoc_static") ("stpool" "dep:threadcell"))))))

(define-public crate-onsen-0.12.0 (c (n "onsen") (v "0.12.0") (d (list (d (n "assoc_static") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)) (d (n "threadcell") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "1jky0mf2hnaj2mbqga7f9rg520dila35yv9rilx24aif3nx2p7dx") (f (quote (("st_tbox" "tbox" "stpool") ("default" "st_tbox")))) (s 2) (e (quote (("tbox" "dep:assoc_static") ("stpool" "dep:threadcell"))))))

(define-public crate-onsen-0.13.0 (c (n "onsen") (v "0.13.0") (d (list (d (n "assoc_static") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)) (d (n "threadcell") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "02sfxh1j807adzi6lf6a90i7xc542vgkrcpp6vg4max34i2ymc47") (f (quote (("st_tbox" "tbox" "stpool") ("default" "st_tbox")))) (s 2) (e (quote (("tbox" "dep:assoc_static") ("stpool" "dep:threadcell"))))))

(define-public crate-onsen-0.14.0 (c (n "onsen") (v "0.14.0") (d (list (d (n "assoc_static") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)) (d (n "threadcell") (r "^0.8.1") (o #t) (d #t) (k 0)))) (h "1m5zwkj3rqjb55mh023g9c0wk5wig2l6ifqc1p1pr4pfw1nww0d2") (f (quote (("st_tbox" "tbox" "stpool") ("default" "st_tbox")))) (s 2) (e (quote (("tbox" "dep:assoc_static") ("stpool" "dep:threadcell"))))))

