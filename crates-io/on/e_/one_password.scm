(define-module (crates-io on e_ one_password) #:use-module (crates-io))

(define-public crate-one_password-0.1.0 (c (n "one_password") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^1.0") (d #t) (k 0)))) (h "1lybmh4zi6a6dd1afs20b6ij9zxns47a9kw97lqv90znpa45fpx8")))

