(define-module (crates-io on e_ one_way_slot_map) #:use-module (crates-io))

(define-public crate-one_way_slot_map-0.1.0 (c (n "one_way_slot_map") (v "0.1.0") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0jsclpvc20c6a1bkzm2s4dhsl72ni5861ps8pmarm1pvvc7b1jra")))

(define-public crate-one_way_slot_map-0.1.1 (c (n "one_way_slot_map") (v "0.1.1") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0a6pp1ihw6ycg3r145866frqajpgi2p9rc4g486krg5pvih09brr")))

(define-public crate-one_way_slot_map-0.1.2 (c (n "one_way_slot_map") (v "0.1.2") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1w3c6axypp9diiqicd05kl183p90xkhm93yw180frxhcy0iihzfg") (y #t)))

(define-public crate-one_way_slot_map-0.1.3 (c (n "one_way_slot_map") (v "0.1.3") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "16rrihx4cvbll8axsim56d2b6jxgcfqc5dmq3i0q6wps7n7pya7g")))

(define-public crate-one_way_slot_map-0.2.0 (c (n "one_way_slot_map") (v "0.2.0") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "16hg5k3nm1qar3hhawmd5psdx5vc9isp5xrcl7f974fbxg1qg5fn")))

(define-public crate-one_way_slot_map-0.2.1 (c (n "one_way_slot_map") (v "0.2.1") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "02q40b430fgjmwcfivg2cxgx55wmlhl6b48iqgg3jy6hglv5abcp")))

(define-public crate-one_way_slot_map-0.2.2 (c (n "one_way_slot_map") (v "0.2.2") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "15c9zl5qysr9qc71c7aj11kv3nq11q21mjf6jda6kbcr83q0dwgz")))

(define-public crate-one_way_slot_map-0.3.0 (c (n "one_way_slot_map") (v "0.3.0") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "03zln4aalmv80zkb6lap4d1ayigpb4qvmmr4gmjpv3b0x87ns7n6") (y #t)))

(define-public crate-one_way_slot_map-0.3.1 (c (n "one_way_slot_map") (v "0.3.1") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "13rwagxxaky9w0dx1wglqqj8lipd1mr1f422bf5k6lrqfxp6gz6g")))

(define-public crate-one_way_slot_map-0.4.0 (c (n "one_way_slot_map") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1yrh6b2hxgyjvmkqp1vgicxiccg64mjf03fbwh777zaxz3ykxmm2")))

(define-public crate-one_way_slot_map-0.4.1 (c (n "one_way_slot_map") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "06xpn4iwicbynd143wk1x3i31a2lq6gphzs1pylqzs8p5vxr0sdd")))

(define-public crate-one_way_slot_map-0.4.2 (c (n "one_way_slot_map") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0qh50fc1wc07gz8ki8scfc87l7ilaw8nrgmf76b18wac7v7arf5g")))

