(define-module (crates-io on e_ one_doc) #:use-module (crates-io))

(define-public crate-one_doc-0.1.1 (c (n "one_doc") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0z3b97km2s4fiibzvn4bkk9idr6jdbx94h2bb6pwyh327b68vybp")))

