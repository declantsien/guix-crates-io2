(define-module (crates-io on e_ one_two_eight) #:use-module (crates-io))

(define-public crate-one_two_eight-0.1.0 (c (n "one_two_eight") (v "0.1.0") (d (list (d (n "buffertk") (r "^0.2") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)))) (h "1gxv6b91v9hvrznhhzi39kbabnlznwic60bgbcx9qdcfpll78cj3")))

(define-public crate-one_two_eight-0.1.1 (c (n "one_two_eight") (v "0.1.1") (d (list (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)))) (h "04g3nbwqra1dvgzjd1hdkyi6zv06266g2vsfqmd3wm78gh6r4hiv")))

(define-public crate-one_two_eight-0.2.0 (c (n "one_two_eight") (v "0.2.0") (d (list (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)))) (h "0adh4dyg66m5xrm52c90fm3vmn94mpn7r48n8783dl3x6inhgs96")))

(define-public crate-one_two_eight-0.3.0 (c (n "one_two_eight") (v "0.3.0") (d (list (d (n "buffertk") (r "^0.4") (d #t) (k 0)) (d (n "prototk") (r "^0.4") (d #t) (k 0)))) (h "0f10jnn97rcqwv8229jsvp5mhy9jj610pbf0m3hzw5c5i9pyjpf7")))

(define-public crate-one_two_eight-0.4.0 (c (n "one_two_eight") (v "0.4.0") (d (list (d (n "buffertk") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "prototk") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1s0z7qqhvxqjvj9jdk2skzb5hlv6wllqsxn02iy2gf8sd1ybfhjg") (f (quote (("generate_id_prototk" "buffertk" "prototk") ("default" "generate_id_prototk"))))))

