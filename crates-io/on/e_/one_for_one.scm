(define-module (crates-io on e_ one_for_one) #:use-module (crates-io))

(define-public crate-one_for_one-1.0.0 (c (n "one_for_one") (v "1.0.0") (d (list (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "signal"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0, >=0.7") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0, >=0.1") (d #t) (k 0)))) (h "0wiimhnlqciz3s7qlnbxbq76w5zg093ffl4ryrpyqkp53ch2i3mq")))

(define-public crate-one_for_one-2.0.0 (c (n "one_for_one") (v "2.0.0") (d (list (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "signal"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0, >=0.7") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0, >=0.1") (d #t) (k 0)))) (h "0cnplgildqmwhs1j6izd478dbkpvl4dpqkqk8msw7v9w6pa47ggq")))

(define-public crate-one_for_one-2.1.0 (c (n "one_for_one") (v "2.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "signal"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0, >=0.7") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0, >=0.1") (d #t) (k 0)))) (h "0kqbld6i04f3fjh2fx46dshbl7081jk4l1z2qfdrjm69c2dh3jd7")))

