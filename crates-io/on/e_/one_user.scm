(define-module (crates-io on e_ one_user) #:use-module (crates-io))

(define-public crate-one_user-0.1.0 (c (n "one_user") (v "0.1.0") (d (list (d (n "bitvec") (r "0.22.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*.*") (d #t) (k 0)) (d (n "quote") (r "1.*.*") (d #t) (k 0)) (d (n "syn") (r "1.*.*") (d #t) (k 0)))) (h "1rf93aka5kc5f8fhy4h9d6cz9kpqka5r98m1zqmphr27gfa4kpdh")))

(define-public crate-one_user-0.1.1 (c (n "one_user") (v "0.1.1") (d (list (d (n "bitvec") (r "0.22.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*.*") (d #t) (k 0)) (d (n "quote") (r "1.*.*") (d #t) (k 0)) (d (n "syn") (r "1.*.*") (d #t) (k 0)))) (h "0a64qwaniazg228509iqq9lysqkv2yb755pmxv16mwnlsy8s1hbd")))

(define-public crate-one_user-0.1.2 (c (n "one_user") (v "0.1.2") (d (list (d (n "bitvec") (r "0.22.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*.*") (d #t) (k 0)) (d (n "quote") (r "1.*.*") (d #t) (k 0)) (d (n "syn") (r "1.*.*") (d #t) (k 0)))) (h "0dc0x3f5j5vibn8jj76fa81vmms2m1pgpk4anwkv0k4gwlq5gbc8")))

(define-public crate-one_user-0.2.0 (c (n "one_user") (v "0.2.0") (d (list (d (n "bitvec") (r "0.22.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*.*") (d #t) (k 0)) (d (n "quote") (r "1.*.*") (d #t) (k 0)) (d (n "syn") (r "1.*.*") (d #t) (k 0)))) (h "0q9f96n90inm2kbdwklf6l2864y4bzsr1lqzbr1mgw9369p6a9f6")))

(define-public crate-one_user-0.3.0 (c (n "one_user") (v "0.3.0") (d (list (d (n "bitvec") (r "0.22.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*.*") (d #t) (k 0)) (d (n "quote") (r "1.*.*") (d #t) (k 0)) (d (n "syn") (r "1.*.*") (d #t) (k 0)))) (h "1b3x7h0h3i7as3s89r40q4wz4klv1gz5n9nfkkcjmbrl0j4vw4nn")))

(define-public crate-one_user-0.3.1 (c (n "one_user") (v "0.3.1") (d (list (d (n "bitvec") (r "0.22.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*.*") (d #t) (k 0)) (d (n "quote") (r "1.*.*") (d #t) (k 0)) (d (n "syn") (r "1.*.*") (d #t) (k 0)))) (h "0fvc71gkzwlpcrpjnc5cg7gj71069bpwhlk2mjx3mjg95dicpkzz")))

(define-public crate-one_user-0.3.2 (c (n "one_user") (v "0.3.2") (d (list (d (n "bitvec") (r "1.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (d #t) (k 0)))) (h "05miln9mzqz4rrggzcfprdl0i66d8vlsp5scfh1m668wprrc8dls")))

