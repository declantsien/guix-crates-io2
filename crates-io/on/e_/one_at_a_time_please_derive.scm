(define-module (crates-io on e_ one_at_a_time_please_derive) #:use-module (crates-io))

(define-public crate-one_at_a_time_please_derive-1.0.0 (c (n "one_at_a_time_please_derive") (v "1.0.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "1ycsmyp8adni2c8yizps41laqw5sd88ap4xzj39apwkwj9rqc4mi")))

(define-public crate-one_at_a_time_please_derive-1.0.1 (c (n "one_at_a_time_please_derive") (v "1.0.1") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0wfgm0fj1mywwkbzld8pwkdnc3ra26v1hcka2h4wnfrkmqrsfhha")))

