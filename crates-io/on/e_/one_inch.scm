(define-module (crates-io on e_ one_inch) #:use-module (crates-io))

(define-public crate-one_inch-0.0.1 (c (n "one_inch") (v "0.0.1") (h "0vqrj4gpqbw75b50xcind3qhkn9x5lii827vgjhwr8q3bhmbk18b")))

(define-public crate-one_inch-0.1.1 (c (n "one_inch") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s4radqc3lg9nmjd1qxjd3iq3qafx1j8si7rxbf7w00ap8w1bvj2")))

