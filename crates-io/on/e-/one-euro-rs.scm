(define-module (crates-io on e- one-euro-rs) #:use-module (crates-io))

(define-public crate-one-euro-rs-0.1.0 (c (n "one-euro-rs") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0f5asikniz6ammsi4xb9nhnblk3hk6s521qrxzzv3fryq8nslvd3")))

(define-public crate-one-euro-rs-0.2.0 (c (n "one-euro-rs") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0g2mn6b950bc4bs58279dkld2kdinf462kcqrddzd8kix3mlr6am")))

