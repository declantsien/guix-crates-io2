(define-module (crates-io on e- one-time-signatures-cry4) #:use-module (crates-io))

(define-public crate-one-time-signatures-cry4-0.1.0 (c (n "one-time-signatures-cry4") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1dj7pcfybhj7c04hj47lwq0qn5z55yqyh88fn9is5k9x6bhpi5mh")))

(define-public crate-one-time-signatures-cry4-0.1.1 (c (n "one-time-signatures-cry4") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1x5nffqzvsj5vf4hsxba0lxi799fn5x8ms768rlibsg799k58586") (f (quote (("build-binary"))))))

