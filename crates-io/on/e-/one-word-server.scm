(define-module (crates-io on e- one-word-server) #:use-module (crates-io))

(define-public crate-one-word-server-0.1.0 (c (n "one-word-server") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "random_word") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "net" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1fmf9z5k2c9jy8x1qddl94q85yx0svb0azs0my091ab38w776rgi")))

