(define-module (crates-io on e- one-shot-mutex) #:use-module (crates-io))

(define-public crate-one-shot-mutex-0.1.0 (c (n "one-shot-mutex") (v "0.1.0") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)))) (h "1kkfxc887xrwv333lz4sqrcb066paz19b1ldvjsdxc0hjak55cks")))

(define-public crate-one-shot-mutex-0.1.1 (c (n "one-shot-mutex") (v "0.1.1") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)))) (h "1qk7cbzgr0igs3h1wxgy02ncl34wpjnxczxni0acnhqfyyfg6ciw")))

