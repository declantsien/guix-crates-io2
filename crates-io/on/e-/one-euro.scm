(define-module (crates-io on e- one-euro) #:use-module (crates-io))

(define-public crate-one-euro-0.1.0 (c (n "one-euro") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.4") (k 0)) (d (n "approx") (r "^0.5.1") (k 2)) (d (n "csv") (r "^1.1.6") (k 2)) (d (n "nalgebra") (r "^0.31.4") (f (quote ("std"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)))) (h "0zkdyayn99cn817hy165y4yz674van64lcvclkdbd0n5ccp1k8hc")))

(define-public crate-one-euro-0.2.0 (c (n "one-euro") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.32.1") (k 0)) (d (n "approx") (r "^0.5.1") (k 2)) (d (n "csv") (r "^1.1.6") (k 2)) (d (n "nalgebra") (r "^0.32.1") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)))) (h "0k605863x2kidkqwsw8j60p5hlvjd13mj739gafdnh3ib9d6gp18")))

(define-public crate-one-euro-0.3.0 (c (n "one-euro") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.32.1") (k 0)) (d (n "approx") (r "^0.5.1") (k 2)) (d (n "csv") (r "^1.1.6") (k 2)) (d (n "nalgebra") (r "^0.32.1") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)))) (h "1cwb5gjndx0dmv6vnzx0ks937pv497jyk3hicf6dfqj0qrvrlqj6")))

(define-public crate-one-euro-0.4.0 (c (n "one-euro") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.32.1") (k 0)) (d (n "approx") (r "^0.5.1") (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "csv") (r "^1.1.6") (k 2)) (d (n "nalgebra") (r "^0.32.1") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)))) (h "0410cpxwx23xy1mjfji087n4zl7y3p5wy5ii6gl5r0izski3miin")))

(define-public crate-one-euro-0.4.1 (c (n "one-euro") (v "0.4.1") (d (list (d (n "nalgebra") (r "^0.32.1") (k 0)) (d (n "approx") (r "^0.5.1") (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "csv") (r "^1.1.6") (k 2)) (d (n "nalgebra") (r "^0.32.1") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)))) (h "1m2rjimm4ybwmmv2jv31q3nm6nfyva0idl9ywl9y8ysjxhq1czzd")))

(define-public crate-one-euro-0.5.0 (c (n "one-euro") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.32.1") (k 0)) (d (n "approx") (r "^0.5.1") (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "csv") (r "^1.1.6") (k 2)) (d (n "nalgebra") (r "^0.32.1") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)))) (h "1djij5f9wydzj5f3irff8pdp0dp62jk3hcsxpidz0d5rvd2v9ysf")))

(define-public crate-one-euro-0.6.0 (c (n "one-euro") (v "0.6.0") (d (list (d (n "nalgebra") (r "^0.32.1") (k 0)) (d (n "approx") (r "^0.5.1") (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "csv") (r "^1.1.6") (k 2)) (d (n "nalgebra") (r "^0.32.1") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)))) (h "150ickpllr63wgs788jkhrmk3f591cn4as0sha5fnwhf3m9bbblh")))

