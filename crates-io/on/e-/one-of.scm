(define-module (crates-io on e- one-of) #:use-module (crates-io))

(define-public crate-one-of-0.1.0 (c (n "one-of") (v "0.1.0") (h "101lf3vsw6qdg7k481xf765632i2rnffzjaj5srzgpijn5ys73ap")))

(define-public crate-one-of-0.2.0 (c (n "one-of") (v "0.2.0") (h "13z4kf1g48j8bmwvvnkwcybhlw8sdwjh3pdh0kw0zzrwc1wmhn9z")))

(define-public crate-one-of-0.2.1 (c (n "one-of") (v "0.2.1") (h "07yxkwijglfd213yr841vr55nd0dap95nmqav963c3hxjbwzckmg") (y #t)))

(define-public crate-one-of-0.2.2 (c (n "one-of") (v "0.2.2") (h "1fa3cdhmwi8a5zhp7dzg11k1kksm2vbxsp6hqir1a4hnqahxl9m5")))

(define-public crate-one-of-0.2.3 (c (n "one-of") (v "0.2.3") (h "148d6jkxga09p1r4dhv5gw266jvswvqyq80lwhpkkg6ldp0p013m")))

