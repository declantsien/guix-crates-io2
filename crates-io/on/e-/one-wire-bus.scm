(define-module (crates-io on e- one-wire-bus) #:use-module (crates-io))

(define-public crate-one-wire-bus-0.1.0 (c (n "one-wire-bus") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "10b2lrqysmswfm16jrs8lbcdhrwrgqq3mpcx8fbf8i1qzyhz9xdg")))

(define-public crate-one-wire-bus-0.1.1 (c (n "one-wire-bus") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "043phkapxhm5yn9h9c2df1w89yx3xphdp8f109kr33sg0w9iwxm9")))

