(define-module (crates-io on e- one-wip) #:use-module (crates-io))

(define-public crate-one-wip-0.1.1 (c (n "one-wip") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11b6gh140l1df443xcy69z983cv3qznpdyvbmj0d4gv13jrb86hq")))

