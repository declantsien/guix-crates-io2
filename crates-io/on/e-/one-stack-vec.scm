(define-module (crates-io on e- one-stack-vec) #:use-module (crates-io))

(define-public crate-one-stack-vec-0.4.0 (c (n "one-stack-vec") (v "0.4.0") (h "00wk2sk7701ngw90yn8p6577ffa3my1zmk0pfc7piazf1a22j3f3")))

(define-public crate-one-stack-vec-0.5.0 (c (n "one-stack-vec") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0rs2d5wjkq0x7slljwk4k1awip9c1k4lq33nfhhsspky2l1g6vd9")))

(define-public crate-one-stack-vec-0.5.1 (c (n "one-stack-vec") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0jb68n5db7q6s6jm1z389g7ifisfcl1nb54pkb8qws5gi2srcvha")))

