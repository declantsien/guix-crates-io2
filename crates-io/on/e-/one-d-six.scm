(define-module (crates-io on e- one-d-six) #:use-module (crates-io))

(define-public crate-one-d-six-0.1.0 (c (n "one-d-six") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1mzapzpih3lvqyakhnxlp91a41x4a7fiirmi8ycwgy7b57rzckav")))

(define-public crate-one-d-six-0.2.0 (c (n "one-d-six") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "01rgymdwhqpa64f91g3yjvyakd64pmaky9cxbybjlf66ff9gawpc") (y #t)))

(define-public crate-one-d-six-0.2.1 (c (n "one-d-six") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1ficajxc9zjdhrpn3vmk6mrqyib2p765gp2y6sk5ngs0bqjghidr")))

(define-public crate-one-d-six-0.3.0 (c (n "one-d-six") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0j9cf9qhr024hmvykkv9k6g4055ss0pix9yy4xv8rldqcld8qxxz")))

(define-public crate-one-d-six-0.4.0 (c (n "one-d-six") (v "0.4.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0mhn894ddhkfz6nyyrzvp8qqbrvs5a9y65xa183ryvwfrl4by3al")))

(define-public crate-one-d-six-0.5.0 (c (n "one-d-six") (v "0.5.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1xgv1jznb858pgznm82j78zg4vyv0436rjcwj8dh927ixkgdk8sw")))

