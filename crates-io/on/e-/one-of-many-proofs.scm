(define-module (crates-io on e- one-of-many-proofs) #:use-module (crates-io))

(define-public crate-one-of-many-proofs-0.1.0 (c (n "one-of-many-proofs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "curve25519-dalek") (r "^2") (f (quote ("simd_backend" "nightly" "serde" "alloc"))) (k 0)) (d (n "merlin") (r "^2") (k 0)) (d (n "polynomials") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "sha3") (r "^0.8") (k 0)))) (h "1c0vbwbfjjbsimp5d6zfifabl1n96xff2k4fm7jnr134ik1qnq51") (f (quote (("std") ("default" "std" "serde"))))))

