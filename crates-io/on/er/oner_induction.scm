(define-module (crates-io on er oner_induction) #:use-module (crates-io))

(define-public crate-oner_induction-0.2.0 (c (n "oner_induction") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)))) (h "13r8p30mg8x9i3csqqliyyqvav0il1yrcr9r7wgfxp3rplcykix9")))

(define-public crate-oner_induction-0.2.1 (c (n "oner_induction") (v "0.2.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0j273h20bs04gmz2iii0ni1b70ycvrgda6gipwbx1ablv1q14zgf")))

