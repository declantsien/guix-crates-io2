(define-module (crates-io on ti ontio-codegen) #:use-module (crates-io))

(define-public crate-ontio-codegen-0.1.0 (c (n "ontio-codegen") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01p916532ga4nvfxqqnrwhxbb01hh5955ghscf2a3wyvc3949qrq")))

(define-public crate-ontio-codegen-0.2.0 (c (n "ontio-codegen") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.1") (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nqli3gx3dmznr4zyv6j2dfx3c1r9l8p87hfbxr5g7lcqif2bwgl") (f (quote (("mock" "ontio-std/mock"))))))

(define-public crate-ontio-codegen-0.2.1 (c (n "ontio-codegen") (v "0.2.1") (d (list (d (n "heck") (r "^0.3.1") (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cpnsxwhs01cjkfig49wnhmfvp1sfgryp91nl06cjrc8vb2nh3f7")))

