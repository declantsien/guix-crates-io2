(define-module (crates-io on ti ontimeexecuter) #:use-module (crates-io))

(define-public crate-ontimeexecuter-0.1.0 (c (n "ontimeexecuter") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "settimeout") (r "^0.1.2") (d #t) (k 0)))) (h "0plf6s4k4n20vwz95yrpz4wjr7zwc847j5hnnay2l7kq56vpciam")))

