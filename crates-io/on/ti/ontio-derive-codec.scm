(define-module (crates-io on ti ontio-derive-codec) #:use-module (crates-io))

(define-public crate-ontio-derive-codec-0.1.0 (c (n "ontio-derive-codec") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ahglvf6ljr5samdqajscmzyycm0k2y4rs743nx3m4py4k308ann")))

(define-public crate-ontio-derive-codec-0.2.0 (c (n "ontio-derive-codec") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "127yxlvw3jy6pf57ghcw5dfk0ni5nmsxchhgv0kk1frx5x1j5kd5")))

