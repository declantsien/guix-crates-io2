(define-module (crates-io on ce once_spin) #:use-module (crates-io))

(define-public crate-once_spin-0.1.0 (c (n "once_spin") (v "0.1.0") (h "1g46s12rf15dzpaiwp21vf934zaslgj0wzad76zgcbv468aj88ha") (y #t)))

(define-public crate-once_spin-0.1.1 (c (n "once_spin") (v "0.1.1") (h "16vvh98j6fm96sl1mfp7gki4nyglpjjs7da2xcp8gin6hqaa7a8y") (y #t)))

(define-public crate-once_spin-0.1.2 (c (n "once_spin") (v "0.1.2") (h "181xq6kfhvpcpr04lnvk0qhaiygm4jma78psxvqkv003j7yfc6gk") (y #t)))

(define-public crate-once_spin-0.1.3 (c (n "once_spin") (v "0.1.3") (h "1kgfq3nyl4j5gq9iw0ks8b057mg5h9mpnni87qhyj42jis9k92v1") (y #t)))

