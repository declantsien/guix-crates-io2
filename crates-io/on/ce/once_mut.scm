(define-module (crates-io on ce once_mut) #:use-module (crates-io))

(define-public crate-once_mut-0.1.0 (c (n "once_mut") (v "0.1.0") (h "1ilg6n0y5krsfnd5misgjr5d0bk3lim05l7c6d9h0x0pnbqgryhh") (y #t)))

(define-public crate-once_mut-0.1.1 (c (n "once_mut") (v "0.1.1") (h "0gb5rdpqy2hjmhwlyqvb0b5nki41nzzxn4fxg0348g0q0xhdp5v1")))

