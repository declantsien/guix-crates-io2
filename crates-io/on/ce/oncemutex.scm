(define-module (crates-io on ce oncemutex) #:use-module (crates-io))

(define-public crate-oncemutex-0.0.1 (c (n "oncemutex") (v "0.0.1") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "1bx4dq6kq058mcbixyfnbk43p0vc0261g8wkba6yjdwydq4f5zgg")))

(define-public crate-oncemutex-0.0.2 (c (n "oncemutex") (v "0.0.2") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "0ypla94kgd39vh2x5k3yz894ms2pdarvqz5gr7armfb23fa816b9")))

(define-public crate-oncemutex-0.0.3 (c (n "oncemutex") (v "0.0.3") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "0n4cpaqf24y4kpyp8vqwk6dk1lhfwahhxbglc8q0blwy579cchy8")))

(define-public crate-oncemutex-0.0.4 (c (n "oncemutex") (v "0.0.4") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "15878sp7pclvq7vmdrrf99xi2wsyxk7ggi4xbqdxz7c5wx5zh6qv")))

(define-public crate-oncemutex-0.0.5 (c (n "oncemutex") (v "0.0.5") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "0vlkdy7zld1ii0zzjxzjcrdcaychzs9bp7yv74nhij11qdixxxys")))

(define-public crate-oncemutex-0.0.6 (c (n "oncemutex") (v "0.0.6") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "1yflfmdr10fg5vf594z8lrn30xcmnrha0fsi5iwab75k87x2lfz2")))

(define-public crate-oncemutex-0.1.0 (c (n "oncemutex") (v "0.1.0") (h "1vpv4vizym598wwn7c0d86f1sfx33w13lw4cwy6fvz8xvxnk6c2h")))

(define-public crate-oncemutex-0.1.1 (c (n "oncemutex") (v "0.1.1") (h "1qmzibgx46k3xfh0mbljp46cfy9vx62frrx5x1ph18zlcvj1vla4")))

