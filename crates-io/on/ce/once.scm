(define-module (crates-io on ce once) #:use-module (crates-io))

(define-public crate-once-0.1.0 (c (n "once") (v "0.1.0") (h "0vmvp0mld1z7ajlzcrlbp3y32r09wnl1qv2qqba7rnq3ap9ny6ls")))

(define-public crate-once-0.1.1 (c (n "once") (v "0.1.1") (h "0hd369qbigrd787p6bp28r7015sajhxjy6jr9v16hxvws4c0njm1")))

(define-public crate-once-0.1.2 (c (n "once") (v "0.1.2") (h "0mxa86iv8ya8zym6ljb0zmkm1fi5mwiv2981d7bnpm4llirl7anl")))

(define-public crate-once-0.1.3 (c (n "once") (v "0.1.3") (h "039hf3z4h7bhv9crsygg9zr00dciblhj5ahm8rr8w9a9x3gi6hgh")))

(define-public crate-once-0.2.0 (c (n "once") (v "0.2.0") (h "178is24k9x0gab991cag4rb9n6iz28kb91y0nlwavig441clh5x6")))

(define-public crate-once-0.2.1 (c (n "once") (v "0.2.1") (h "1vi4apk2nj0w4ijq78j3yjvigfqj5hr8z51nssqgp7yv3kr4ja0v")))

(define-public crate-once-0.3.0 (c (n "once") (v "0.3.0") (h "0xfnjnka03v71fwvkj658aa523592wnam3h931zmszc8nwqyalkg") (y #t)))

(define-public crate-once-0.3.1 (c (n "once") (v "0.3.1") (h "1d5r964jgdr7sr60z2l6h4yqrzqlc26mwp9xsc0a63d5ikcr5svn")))

(define-public crate-once-0.3.2 (c (n "once") (v "0.3.2") (h "1sccd1c2x8wxcpa53pnjqy2gx7815h5290n184wxkvcksj6qdr8s")))

(define-public crate-once-0.3.3 (c (n "once") (v "0.3.3") (h "0wvd3iasc4yws2mpiqg2shkcgx2yral553g5rgv0qq9lryjbf7wk")))

(define-public crate-once-0.3.4 (c (n "once") (v "0.3.4") (h "0a4fy3zwcxhwcfc7ainidw8cnibqqlv0854lnxig2mgp81dfggv0")))

