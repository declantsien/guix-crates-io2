(define-module (crates-io on ce once_self_cell) #:use-module (crates-io))

(define-public crate-once_self_cell-0.1.0 (c (n "once_self_cell") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 0)))) (h "0xxa2haj4kdilxmi9fzifnliym3hs2i63d3b02lfbmzyg1n3bpln") (y #t)))

(define-public crate-once_self_cell-0.2.0 (c (n "once_self_cell") (v "0.2.0") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 0)))) (h "1mmvyxkxhi8122zgaf17ad1b6hshib2b7hi57sp24r87xi748b37") (y #t)))

(define-public crate-once_self_cell-0.3.0 (c (n "once_self_cell") (v "0.3.0") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "12gp60clj4rrvi0avd97qwjdk9705q6p35bpyn2j2l944a08l6jp") (y #t)))

(define-public crate-once_self_cell-0.4.0 (c (n "once_self_cell") (v "0.4.0") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "1k7s94wrqb837kzflm3v0na7bn16rc6lc1qciqsny6rrbn5zb1hf") (y #t)))

(define-public crate-once_self_cell-0.5.0 (c (n "once_self_cell") (v "0.5.0") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "1rd89fivjjw417wxfjvr6jsji39mxmcgma4d7w1v9wnvlmmaa8i5") (y #t)))

(define-public crate-once_self_cell-0.6.0 (c (n "once_self_cell") (v "0.6.0") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "1faf2ybvi5l3r76k3d38ak1r9iac0alxx8k1qdvrkjcfbyghfqlr") (y #t)))

(define-public crate-once_self_cell-0.6.1 (c (n "once_self_cell") (v "0.6.1") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "1sz2ad7qpvy7y632j8s2w1in3jqa9fmpsy2icbf5g756fjr182w7") (y #t)))

(define-public crate-once_self_cell-0.6.2 (c (n "once_self_cell") (v "0.6.2") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "1n3kzgmskg9rmn0vvvkib9qk93r9fkhmbww3znhp6z83gpyzysv7") (y #t)))

(define-public crate-once_self_cell-0.6.3 (c (n "once_self_cell") (v "0.6.3") (d (list (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "0zb86nnmy9x59m999xskkxi4blbpnrn7khb8fjsibyyqvj9d2n2f")))

