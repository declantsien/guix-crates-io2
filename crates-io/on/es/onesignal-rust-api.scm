(define-module (crates-io on es onesignal-rust-api) #:use-module (crates-io))

(define-public crate-onesignal-rust-api-1.0.1 (c (n "onesignal-rust-api") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1pwkd0k4cgkcy2fz50df3l27i6gpblwxcbh6jyhjlv207a383559") (y #t)))

(define-public crate-onesignal-rust-api-1.0.0 (c (n "onesignal-rust-api") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "199zsx7n4v8jpg6q42g2sznjbhyvw3y5dzva0zrxiqj6a0xxpbk8")))

(define-public crate-onesignal-rust-api-1.0.2 (c (n "onesignal-rust-api") (v "1.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0yiccnzzcyvpqz8lw3rbb2wjh1zl38zsqff7cqzn4z8blczglrkh")))

(define-public crate-onesignal-rust-api-2.0.0 (c (n "onesignal-rust-api") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0qsfrgjjb75dq07c7y3292rn6q4rrgz4z74ji5i4ipbg4xkcmzc7")))

(define-public crate-onesignal-rust-api-2.0.2 (c (n "onesignal-rust-api") (v "2.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "158xp0cgiiq7lsga3fll61206qnhaim78y8v0ai1vb4dkz10lqiq")))

