(define-module (crates-io on es onesignal-rest) #:use-module (crates-io))

(define-public crate-onesignal-rest-0.0.0 (c (n "onesignal-rest") (v "0.0.0") (h "0iv2l9jhkd4b93vf0nyi51jgrkjm2yxwwm0212rvdcinha0n1c5w")))

(define-public crate-onesignal-rest-0.1.0 (c (n "onesignal-rest") (v "0.1.0") (d (list (d (n "http-api-client-endpoint") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (k 0)))) (h "07q2vq96ah3rsby91j850hj1kdi3r7q9ba2w7k7aj8xa06vj620h")))

(define-public crate-onesignal-rest-0.1.1 (c (n "onesignal-rest") (v "0.1.1") (d (list (d (n "http-api-client-endpoint") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (k 0)))) (h "13nbyyy54rp2kknq07w0ns6vvir7nvg4i1s3xi1b9lr65mkbsi5j")))

