(define-module (crates-io on es oneshotserver) #:use-module (crates-io))

(define-public crate-oneshotserver-1.0.0 (c (n "oneshotserver") (v "1.0.0") (d (list (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14.23") (f (quote ("http1" "tcp" "server"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 0)))) (h "102vspxr2fmnv0hzzhjbpf8nk0lwxcsyybaz2jbx6simfbim11kw")))

