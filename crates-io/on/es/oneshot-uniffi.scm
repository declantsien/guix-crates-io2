(define-module (crates-io on es oneshot-uniffi) #:use-module (crates-io))

(define-public crate-oneshot-uniffi-0.1.5 (c (n "oneshot-uniffi") (v "0.1.5") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "17nrggh81hmxnkwzzm143b8c3a1yd3f9n49xg2hfd9z7fj3rir4s") (f (quote (("std") ("default" "std" "async") ("async")))) (r "1.60.0")))

(define-public crate-oneshot-uniffi-0.1.6 (c (n "oneshot-uniffi") (v "0.1.6") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "175j04f7rpgy552h5m51ywq7184cqhcfs31dsxanjvwpg1f8sm3c") (f (quote (("std") ("default" "std" "async") ("async")))) (r "1.60.0")))

