(define-module (crates-io on es oneshot-broadcast) #:use-module (crates-io))

(define-public crate-oneshot-broadcast-0.0.1 (c (n "oneshot-broadcast") (v "0.0.1") (d (list (d (n "safer_owning_ref") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("full"))) (d #t) (k 2)))) (h "1ivnkfvvsndw98nyarii0rqnifp7c3xql6zmn6acb6qiirv571rc")))

(define-public crate-oneshot-broadcast-0.0.2 (c (n "oneshot-broadcast") (v "0.0.2") (d (list (d (n "safer_owning_ref") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("full"))) (d #t) (k 2)))) (h "1vij8mh97cpg3bi7h2ncdxgrp1s1m4x3zf9nx65v5ym6hbdqr9fd")))

