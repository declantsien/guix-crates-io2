(define-module (crates-io on es onestop) #:use-module (crates-io))

(define-public crate-onestop-0.0.1 (c (n "onestop") (v "0.0.1") (h "0bgl4sad6iy0lbx5r01bv1a4sf3jzgc88sqgxjdvv5dzrzaf37ad")))

(define-public crate-onestop-0.0.2 (c (n "onestop") (v "0.0.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "053mywcvsjaskryx0z5yhbxckkr05ph44dk269hd0pr4lg5xa7sw") (s 2) (e (quote (("utils" "dep:serde" "dep:toml"))))))

