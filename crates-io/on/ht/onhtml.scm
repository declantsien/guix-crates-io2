(define-module (crates-io on ht onhtml) #:use-module (crates-io))

(define-public crate-onhtml-4.0.0 (c (n "onhtml") (v "4.0.0") (d (list (d (n "duplicate") (r "^0") (d #t) (k 0)))) (h "01k0zg40dybd2aizqy35d5h3c3djw9p0jgwjfajiw80rjnnqvxir") (r "1.56")))

(define-public crate-onhtml-4.0.1 (c (n "onhtml") (v "4.0.1") (d (list (d (n "duplicate") (r "^0") (d #t) (k 0)))) (h "1ica80h5zb7jnvfw3bbf47cy03rl63v6wcdbwqabx20b6qrichg7") (r "1.56")))

(define-public crate-onhtml-4.0.2 (c (n "onhtml") (v "4.0.2") (d (list (d (n "duplicate") (r "^0") (d #t) (k 0)))) (h "157ap6vjdsr09lzh1xvjmjl1nry4kbdjj114qk93n5djmzcxak4s") (r "1.56")))

