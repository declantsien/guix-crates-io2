(define-module (crates-io on yx onyx) #:use-module (crates-io))

(define-public crate-onyx-0.1.0 (c (n "onyx") (v "0.1.0") (d (list (d (n "glium") (r "^0.16") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)))) (h "094bfqqgk2qd1amj5sdygq00h5161ckps5zi4x0s1l36fxi02d8a")))

(define-public crate-onyx-0.2.0 (c (n "onyx") (v "0.2.0") (d (list (d (n "glium") (r "^0.16") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1ky8ivn4103ysf81hzafw4z0in6q67q1n49znghfaqhvjchgfdap")))

