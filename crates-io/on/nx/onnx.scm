(define-module (crates-io on nx onnx) #:use-module (crates-io))

(define-public crate-onnx-0.1.0 (c (n "onnx") (v "0.1.0") (d (list (d (n "protobuf") (r "^1.4") (d #t) (k 0)) (d (n "protoc-rust") (r "^1.4") (d #t) (k 1)))) (h "0pfky47snxng3zvra5akf56ky08dkgjs23s0lglk8mr0s0fkv5xq") (f (quote (("proto3") ("default"))))))

