(define-module (crates-io on nx onnxruntime-sys-patch) #:use-module (crates-io))

(define-public crate-onnxruntime-sys-patch-0.0.14 (c (n "onnxruntime-sys-patch") (v "0.0.14") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.1") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "1kw3rpd84z42qkpz1mfgzh2smirpvav85wxfbiggg114yf25ii1b") (f (quote (("generate-bindings" "bindgen") ("disable-sys-build-script") ("default"))))))

