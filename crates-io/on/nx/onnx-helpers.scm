(define-module (crates-io on nx onnx-helpers) #:use-module (crates-io))

(define-public crate-onnx-helpers-0.0.1 (c (n "onnx-helpers") (v "0.0.1") (d (list (d (n "bytes") (r "^0.5.0") (d #t) (k 0)) (d (n "onnx-pb") (r "^0.0.3") (d #t) (k 0)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)))) (h "1qkqdhbca75w770p6mfd01knqqdyvgf8yj0f268ilc12s36dmvrv")))

(define-public crate-onnx-helpers-0.1.0 (c (n "onnx-helpers") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.0") (d #t) (k 0)) (d (n "onnx-pb") (r "^0.1.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)))) (h "1413pgmz5lq65anjwdh1r7kv0rj8ysfn729w1xnnalf9p0hh7g9d")))

(define-public crate-onnx-helpers-0.2.0 (c (n "onnx-helpers") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5.0") (d #t) (k 0)) (d (n "onnx-pb") (r "^0.1.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)))) (h "0m187pjnkj4ak2xmd0mk96a0pl9a8l3g8bc4qqrbd3pbcb7174f3")))

(define-public crate-onnx-helpers-1.0.0 (c (n "onnx-helpers") (v "1.0.0") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "onnx-pb") (r "^0.1.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)))) (h "01jpxardzvbxgixs5b4p62lx5ysjy0swdiqg9fd8hgyxn9kg4cq6")))

(define-public crate-onnx-helpers-1.2.0 (c (n "onnx-helpers") (v "1.2.0") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "onnx-pb") (r "^0.1.1") (d #t) (k 0)) (d (n "onnx-shape-inference") (r "^1.0.0") (d #t) (k 2)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)))) (h "1sxcdzyicl3kfcrzmdwwshi2lz4jc5f2r1ry8q9kkmx47fjvz8sj")))

(define-public crate-onnx-helpers-1.3.0 (c (n "onnx-helpers") (v "1.3.0") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "onnx-pb") (r "^0.1.1") (d #t) (k 0)) (d (n "onnx-shape-inference") (r "^1.0.0") (d #t) (k 2)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)))) (h "124ifx091svpakayf0rng2ivqcxv1k4mdxfzxvidfgykzy6l9gca")))

(define-public crate-onnx-helpers-2.1.0 (c (n "onnx-helpers") (v "2.1.0") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "onnx-pb") (r "^0.1.1") (d #t) (k 0)) (d (n "onnx-shape-inference") (r "^1.1.0") (d #t) (k 2)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)))) (h "11yyg1apxb5kd89igrqfbi8wk6171rh7pl23avijlr9ppz3rxjbd")))

(define-public crate-onnx-helpers-2.2.1 (c (n "onnx-helpers") (v "2.2.1") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "onnx-pb") (r "^0.1.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)))) (h "08ijj40334y4770c4q1zahjjn3632rghh8c3pb7qalvqcr66amhd")))

