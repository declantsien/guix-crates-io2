(define-module (crates-io on nx onnxruntime-sys-ng) #:use-module (crates-io))

(define-public crate-onnxruntime-sys-ng-1.15.1 (c (n "onnxruntime-sys-ng") (v "1.15.1") (d (list (d (n "bindgen") (r "^0.66.1") (o #t) (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.1") (d #t) (k 1)) (d (n "zip") (r "^0.6.6") (d #t) (k 1)))) (h "0vy5alk5b74vbfqc06hibb9ggznfksy4dv0m3j76vnzwq13qvl2p") (f (quote (("generate-bindings" "bindgen") ("disable-sys-build-script") ("default") ("cuda"))))))

(define-public crate-onnxruntime-sys-ng-1.16.1 (c (n "onnxruntime-sys-ng") (v "1.16.1") (d (list (d (n "bindgen") (r "^0.66.1") (o #t) (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.1") (d #t) (k 1)) (d (n "zip") (r "^0.6.6") (d #t) (k 1)))) (h "1k66ga654fyzppff0z27vfyhnww4pwz6qvxjbjg0iai23mpm4psd") (f (quote (("generate-bindings" "bindgen") ("disable-sys-build-script") ("default") ("cuda"))))))

