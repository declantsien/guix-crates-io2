(define-module (crates-io on nx onnx-pb) #:use-module (crates-io))

(define-public crate-onnx-pb-0.0.1 (c (n "onnx-pb") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.5.0") (d #t) (k 1)))) (h "0rylw7cinbcyzax90hvkdmfi41g588a9pmf5p8y69v9rxyx651wa")))

(define-public crate-onnx-pb-0.0.2 (c (n "onnx-pb") (v "0.0.2") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.5.0") (d #t) (k 1)))) (h "05fs1lgrrzxym0bhnagjjf6chz4yyj6vbgcap90243wfzaqf7zpv")))

(define-public crate-onnx-pb-0.0.3 (c (n "onnx-pb") (v "0.0.3") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.5.0") (d #t) (k 1)))) (h "1favg79bd6vani6925xfv5dgkn2awk66h0bf2dllqcmrnvcpd8gj")))

(define-public crate-onnx-pb-0.0.4 (c (n "onnx-pb") (v "0.0.4") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.5.0") (d #t) (k 1)))) (h "0gp0v8y2zki5lg6yqbzkjmhcvf92h91avh9l6cl09icz77x1dqp2")))

(define-public crate-onnx-pb-0.1.0 (c (n "onnx-pb") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)))) (h "01dl71b13jy7z1x3kwx8z0n2gxj9s3wkaamwppwbnzpm0yyzgx3d")))

(define-public crate-onnx-pb-0.1.1 (c (n "onnx-pb") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)))) (h "0zn06cjpwhqmqb582grjkvpg9mcwia4hxp3cp9yrshpp229js45m")))

(define-public crate-onnx-pb-0.1.2 (c (n "onnx-pb") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)))) (h "0ln2idnqdx33d4dkqhhka8bvxcl0a4igccpm4zb2r48r45yhm06b")))

(define-public crate-onnx-pb-0.1.3 (c (n "onnx-pb") (v "0.1.3") (d (list (d (n "bytes") (r "^0.5.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)))) (h "0vfa1zf7r4v9r513crg83024pqliggdrjzi4fx50ypxf53myx7gc")))

(define-public crate-onnx-pb-0.1.4 (c (n "onnx-pb") (v "0.1.4") (d (list (d (n "bytes") (r "^0.5.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)))) (h "1i1406z9hybw7mhc09v94b1az91dgl56jz3dgysi7p6hqaprak5y")))

