(define-module (crates-io on nx onnxruntime-sys) #:use-module (crates-io))

(define-public crate-onnxruntime-sys-0.0.1 (c (n "onnxruntime-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "0bbikc67443q13d98vjdlagi3l7zxia92s048b8cnhf2w3jlhwj6")))

(define-public crate-onnxruntime-sys-0.0.2 (c (n "onnxruntime-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "0c492jv1dy9lqlczlw0pgav36p5hvj03zk9w8dbmcyjvnx2j1p2k")))

(define-public crate-onnxruntime-sys-0.0.3 (c (n "onnxruntime-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (t "cfg(unix)") (k 1)) (d (n "tar") (r "^0.4") (d #t) (t "cfg(unix)") (k 1)) (d (n "ureq") (r "^1.3") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (t "cfg(windows)") (k 1)))) (h "0n3cb6bj7njm5afzh6q44lddadliz19c0p41ifzch6arj01r7yha")))

(define-public crate-onnxruntime-sys-0.0.4 (c (n "onnxruntime-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (t "cfg(unix)") (k 1)) (d (n "tar") (r "^0.4") (d #t) (t "cfg(unix)") (k 1)) (d (n "ureq") (r "^1.3") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (t "cfg(windows)") (k 1)))) (h "19k7k6jk28akd4jlbar5qg7f373zjgff0bjrbl3z4h9aplr9kh5x") (f (quote (("doc-only"))))))

(define-public crate-onnxruntime-sys-0.0.5 (c (n "onnxruntime-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (t "cfg(unix)") (k 1)) (d (n "tar") (r "^0.4") (d #t) (t "cfg(unix)") (k 1)) (d (n "ureq") (r "^1.3") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (t "cfg(windows)") (k 1)))) (h "1ch5xd9m6i5yyjkmk4fnldch3mvyv5i25cxdj2f0yp156b7rpr4f") (f (quote (("disable-bindgen"))))))

(define-public crate-onnxruntime-sys-0.0.6 (c (n "onnxruntime-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (t "cfg(unix)") (k 1)) (d (n "tar") (r "^0.4") (d #t) (t "cfg(unix)") (k 1)) (d (n "ureq") (r "^1.3") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (t "cfg(windows)") (k 1)))) (h "0b249zdk29kj45bxiqimdl5bran2fkklrh2a7x5wdsa2qn8ys0fq") (f (quote (("disable-bindgen"))))))

(define-public crate-onnxruntime-sys-0.0.8 (c (n "onnxruntime-sys") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (t "cfg(unix)") (k 1)) (d (n "tar") (r "^0.4") (d #t) (t "cfg(unix)") (k 1)) (d (n "ureq") (r "^1.4") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (t "cfg(windows)") (k 1)))) (h "1ixhqj5jjanfp4z0s7n85y1fx1k6qmprda057ghgfc19xj9hd3dm") (f (quote (("disable-bindgen"))))))

(define-public crate-onnxruntime-sys-0.0.9 (c (n "onnxruntime-sys") (v "0.0.9") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (t "cfg(unix)") (k 1)) (d (n "tar") (r "^0.4") (d #t) (t "cfg(unix)") (k 1)) (d (n "ureq") (r "^1.5.1") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (t "cfg(windows)") (k 1)))) (h "1da63shya4vb1yjgdx430w4s9fv0d4msp6gc0cbf9iynl467wvar") (f (quote (("disable-bindgen"))))))

(define-public crate-onnxruntime-sys-0.0.10 (c (n "onnxruntime-sys") (v "0.0.10") (d (list (d (n "bindgen") (r "^0.55") (o #t) (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^1.5.1") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "14jv134w9qbi5d75jj4rv7iv58rjrnynwzjq8clk4xny5v1mxn0n") (f (quote (("generate-bindings" "bindgen") ("disable-sys-build-script") ("default"))))))

(define-public crate-onnxruntime-sys-0.0.11 (c (n "onnxruntime-sys") (v "0.0.11") (d (list (d (n "bindgen") (r "^0.55") (o #t) (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^1.5.1") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "09xrnk3933zsnlqkl7d31gwgckwg0nf88si8b7z6r8pqvvlv6hy6") (f (quote (("generate-bindings" "bindgen") ("disable-sys-build-script") ("default"))))))

(define-public crate-onnxruntime-sys-0.0.12 (c (n "onnxruntime-sys") (v "0.0.12") (d (list (d (n "bindgen") (r "^0.55") (o #t) (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.1") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "0b0a90zs5dg1lb3sbck5b6z8bjshsibc9yp86vad8hjdps062f66") (f (quote (("generate-bindings" "bindgen") ("disable-sys-build-script") ("default"))))))

(define-public crate-onnxruntime-sys-0.0.13 (c (n "onnxruntime-sys") (v "0.0.13") (d (list (d (n "bindgen") (r "^0.55") (o #t) (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.1") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "1jfq0fvkfzmglzcf264q2ymajaly22bx6qhqslvxi39cjgppr4qg") (f (quote (("generate-bindings" "bindgen") ("disable-sys-build-script") ("default"))))))

(define-public crate-onnxruntime-sys-0.0.14 (c (n "onnxruntime-sys") (v "0.0.14") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.1") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "16cqj1nsfsx4bf5i3ri8bk3x931f7z1ikgl269ky69a5fd8gaq6j") (f (quote (("generate-bindings" "bindgen") ("disable-sys-build-script") ("default"))))))

