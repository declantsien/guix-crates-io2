(define-module (crates-io on nx onnx-protobuf) #:use-module (crates-io))

(define-public crate-onnx-protobuf-0.1.0 (c (n "onnx-protobuf") (v "0.1.0") (d (list (d (n "protobuf") (r "^2.28.0") (d #t) (k 0)))) (h "1sdcrm02w58y93ggx3k7i4bridag90g4wpvn2g066la2p5yvqlp7")))

(define-public crate-onnx-protobuf-0.2.0 (c (n "onnx-protobuf") (v "0.2.0") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)))) (h "0l30lj50yznkv0vsi471q6dzzpfxn1kf6j2ajmx0k15xb850nc05")))

(define-public crate-onnx-protobuf-0.2.1 (c (n "onnx-protobuf") (v "0.2.1") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)) (d (n "safetensors") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1dkjd6p0hlk8dcm8v79016q6nys07vsc6925njzkn3wp11n4rrbd")))

(define-public crate-onnx-protobuf-0.2.2 (c (n "onnx-protobuf") (v "0.2.2") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.4.0") (d #t) (k 2)))) (h "1lgjjr02z9l7ijjhly66jd990vi7jjkcmcbgwylzy6qd7j1alaxl")))

(define-public crate-onnx-protobuf-0.2.3 (c (n "onnx-protobuf") (v "0.2.3") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.4.0") (d #t) (k 2)))) (h "023d6x70zmi8kp10pvin1rkldynzf89ff96ywgqv42bjnzabr0kw")))

