(define-module (crates-io on nx onnx-shape-inference) #:use-module (crates-io))

(define-public crate-onnx-shape-inference-0.0.1 (c (n "onnx-shape-inference") (v "0.0.1") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0l1i7siylrcx0yx51rlw6xg61xryi54zhl9mr60yxb7js2148x0h")))

(define-public crate-onnx-shape-inference-0.0.2 (c (n "onnx-shape-inference") (v "0.0.2") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0pr6cw5cw4ac4lqlc0xj4xwxd5mdjmykhdlgnrq92mqf9nmnv2cg")))

(define-public crate-onnx-shape-inference-1.0.0 (c (n "onnx-shape-inference") (v "1.0.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "onnx-pb") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "0jd12s101a7ma1yjcxpsvc9lh62rq4cf65f6hgn0vjs8k375aihp") (f (quote (("proto" "onnx-pb" "prost") ("default" "proto"))))))

(define-public crate-onnx-shape-inference-1.1.0 (c (n "onnx-shape-inference") (v "1.1.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "onnx-pb") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "14n673jlq7g0hxkffm7j0lwzxfhpmwsqah8a2b3r2sbrl7z4gb4s") (f (quote (("proto" "onnx-pb" "prost") ("default" "proto"))))))

