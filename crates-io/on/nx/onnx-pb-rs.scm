(define-module (crates-io on nx onnx-pb-rs) #:use-module (crates-io))

(define-public crate-onnx-pb-rs-0.1.0 (c (n "onnx-pb-rs") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.5.0") (d #t) (k 1)))) (h "1jg3v49j578g11qqgyng37hvhgwwsmlr70i017i6nqlbzkjqhz8y") (y #t)))

