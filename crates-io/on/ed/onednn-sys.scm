(define-module (crates-io on ed onednn-sys) #:use-module (crates-io))

(define-public crate-onednn-sys-0.0.1 (c (n "onednn-sys") (v "0.0.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0scbh9lq9858zfhwhv4vq13sr9x5r36hcvw7ysmh7naxh6nvfhs5") (l "dnnl")))

