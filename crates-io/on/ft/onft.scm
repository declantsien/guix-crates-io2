(define-module (crates-io on ft onft) #:use-module (crates-io))

(define-public crate-onft-0.1.0-beta.1 (c (n "onft") (v "0.1.0-beta.1") (d (list (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)))) (h "199bs9rb1dg0ix6w7z6fa5n3z42czxmxyxgy85i7bdwjcz9jwr8k")))

(define-public crate-onft-0.1.0-beta.2 (c (n "onft") (v "0.1.0-beta.2") (d (list (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)))) (h "0575c51lxxb8lwj6yf6rix6hrwv3na0mpmcg7gagaz0pr38qiqqq")))

