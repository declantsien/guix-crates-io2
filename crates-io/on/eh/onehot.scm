(define-module (crates-io on eh onehot) #:use-module (crates-io))

(define-public crate-onehot-0.1.0 (c (n "onehot") (v "0.1.0") (d (list (d (n "bitmatrix") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "onehot-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "onehot-derive") (r "^0.1") (d #t) (k 2)))) (h "0n3n1wi92vmlzjzank5d2v1605k2391v5bmy6zfl97nlxy4s1i2j") (f (quote (("matrix" "bitmatrix") ("derive" "onehot-derive"))))))

