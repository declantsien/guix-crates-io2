(define-module (crates-io on _y on_your_marks) #:use-module (crates-io))

(define-public crate-on_your_marks-0.1.0 (c (n "on_your_marks") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1vxalrsfa21jj3cppv728i7a3c12j5111gqi4nqr9sn8ans02n1w")))

(define-public crate-on_your_marks-0.1.1 (c (n "on_your_marks") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18xf0n0m0im77if0masln21lspkcncjq3h9i1fq2y5fp1rz5fv0x")))

