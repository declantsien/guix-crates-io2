(define-module (crates-io on ly only_torch) #:use-module (crates-io))

(define-public crate-only_torch-0.0.1 (c (n "only_torch") (v "0.0.1") (h "1g0z621x62dkpkravxjprrbgql9zgp17vl2s2j78fdagd16cxvqs")))

(define-public crate-only_torch-0.0.2 (c (n "only_torch") (v "0.0.2") (h "1is6ys5cifdrrhbq605v3a3h9zad22mn8cfajp4wvzra3ckp9bac")))

(define-public crate-only_torch-0.0.3 (c (n "only_torch") (v "0.0.3") (h "1z24kdhxz74yzhpx5jb95pc1gknbc6pzv591hqzp26n09805bppd")))

(define-public crate-only_torch-0.0.5 (c (n "only_torch") (v "0.0.5") (h "0xm39l8dkj384hwn18xj3h897ismbrx9np5xx5638f4csg0ifgbk")))

(define-public crate-only_torch-0.0.6 (c (n "only_torch") (v "0.0.6") (h "1wvbr53sxckr0488p5pkalmw3drpzbifmfjznc2knpr8did6r22w")))

(define-public crate-only_torch-0.0.7 (c (n "only_torch") (v "0.0.7") (h "0npwfld0gjnnairy1agn20ab9764cidvhhj3bj5ijz9f954l8fk9")))

(define-public crate-only_torch-0.0.8 (c (n "only_torch") (v "0.0.8") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1ydchqp8fqx77fz5b07nk151lr1bnr8q6q1cqp6k8fb0ncii37sg")))

