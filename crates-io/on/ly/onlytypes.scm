(define-module (crates-io on ly onlytypes) #:use-module (crates-io))

(define-public crate-onlytypes-0.1.0 (c (n "onlytypes") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jqdnl871r68f9625z4y5jfmj0q1xcc01sf8rqhmh5fgyi2iajgc")))

(define-public crate-onlytypes-0.1.1 (c (n "onlytypes") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cir9k2xjh7v3r1b533479phcyvsbz58dap9fp6al19i7kzcx9w5")))

(define-public crate-onlytypes-0.1.2 (c (n "onlytypes") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f0pvp5ji7wwkqpk7s40k5c5bar8ndmrm73yy76ijw5r504i55by")))

(define-public crate-onlytypes-0.1.4 (c (n "onlytypes") (v "0.1.4") (d (list (d (n "otcore") (r "^0.1.4") (d #t) (k 0) (p "onlytypes-core")))) (h "0npfp8gz2a1y14qvbrvdgb36zfsqmfhr9582qhlsln7jidal84qy")))

