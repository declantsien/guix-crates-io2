(define-module (crates-io on ly only-brain) #:use-module (crates-io))

(define-public crate-only-brain-0.1.0 (c (n "only-brain") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07375w5i9dmkqgl7lbhhjqijgxz5b73za4x9vw5hilg7py7fhgns")))

(define-public crate-only-brain-0.1.1 (c (n "only-brain") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zc5n6nfwlyhz03q0iy7w3jpbhshwfzbfhvmcnb13lkqcq7a7p8a")))

(define-public crate-only-brain-0.1.2 (c (n "only-brain") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1pjqbpyi02pqpjak27sy6fpmgqnhp1a7w24lgv7vq49ivdsnrafw")))

(define-public crate-only-brain-0.1.3 (c (n "only-brain") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "108wnkf1aypm3l8ivnk3d56yzzac2qislnz9xay935fvkbndjjrn")))

(define-public crate-only-brain-0.1.4 (c (n "only-brain") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xk13hyfd62l5ijmi7a90jxwmr7qbv1893ilww5k9wmi6mh0vc5f")))

