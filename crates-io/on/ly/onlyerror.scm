(define-module (crates-io on ly onlyerror) #:use-module (crates-io))

(define-public crate-onlyerror-0.1.0 (c (n "onlyerror") (v "0.1.0") (d (list (d (n "error-iter") (r "^0.4") (d #t) (k 2)) (d (n "myn") (r "^0.1") (d #t) (k 0)))) (h "1g7qnl9sdn0nfq491xy7fnivpf6h4ps0llzkb6r85vzpr5ij1rix") (f (quote (("std") ("default" "std"))))))

(define-public crate-onlyerror-0.1.1 (c (n "onlyerror") (v "0.1.1") (d (list (d (n "error-iter") (r "^0.4") (d #t) (k 2)) (d (n "myn") (r "^0.1") (d #t) (k 0)))) (h "1wwj8lpch11cl6q771psxgn7zm87q24zvy756ly2csin17v0kcvw") (f (quote (("std") ("default" "std")))) (r "1.62.0")))

(define-public crate-onlyerror-0.1.2 (c (n "onlyerror") (v "0.1.2") (d (list (d (n "error-iter") (r "^0.4") (d #t) (k 2)) (d (n "myn") (r "^0.1") (d #t) (k 0)))) (h "1x19nlkkwgvab8zhd39pf693l5dmipr27c49ngrg4mwvz453716j") (f (quote (("std") ("default" "std")))) (r "1.62.0")))

(define-public crate-onlyerror-0.1.3 (c (n "onlyerror") (v "0.1.3") (d (list (d (n "error-iter") (r "^0.4") (d #t) (k 2)) (d (n "myn") (r "^0.1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1hrg5qrr5rzfx9hpaqr7xf6y0dqpmz1gcc8mk2fggnfg8rgnhrla") (f (quote (("std") ("default" "std")))) (r "1.62.0")))

(define-public crate-onlyerror-0.1.4 (c (n "onlyerror") (v "0.1.4") (d (list (d (n "error-iter") (r "^0.4") (d #t) (k 2)) (d (n "myn") (r "^0.2") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1nw70pf1yvgdahfgrvbsgddhrrh6as057227vk77m6yd5kmd89lc") (f (quote (("std") ("default" "std")))) (r "1.62.0")))

