(define-module (crates-io on ly onlyargs) #:use-module (crates-io))

(define-public crate-onlyargs-0.1.0 (c (n "onlyargs") (v "0.1.0") (d (list (d (n "error-iter") (r "^0.4") (d #t) (k 2)))) (h "04kgvf5rxv6gdl1dpxlcxzrajfjnwd0rxp5yn539yf91prfhid4s")))

(define-public crate-onlyargs-0.1.1 (c (n "onlyargs") (v "0.1.1") (d (list (d (n "error-iter") (r "^0.4") (d #t) (k 2)))) (h "0ybwadcz345509pkch03scvgvim34alzfm1kac9b4xh28qbx156i")))

(define-public crate-onlyargs-0.1.2 (c (n "onlyargs") (v "0.1.2") (d (list (d (n "error-iter") (r "^0.4") (d #t) (k 2)))) (h "11rmzzpxbvx6r1yp62sl96yfmjm2f420z32p2iwymlb30n0lb9a3")))

(define-public crate-onlyargs-0.1.3 (c (n "onlyargs") (v "0.1.3") (d (list (d (n "error-iter") (r "^0.4") (d #t) (k 2)))) (h "0cy7ip3pv5nviyln5vkxz2pjfmpsky68126lch1c66gxyr0a7aqk") (r "1.62.0")))

(define-public crate-onlyargs-0.2.0 (c (n "onlyargs") (v "0.2.0") (d (list (d (n "error-iter") (r "^0.4") (d #t) (k 2)))) (h "1131zsmsr4p8dqfzwl1y02a2a02jngsnl1ad52l9s4f0nmg8mmq0") (r "1.62.0")))

