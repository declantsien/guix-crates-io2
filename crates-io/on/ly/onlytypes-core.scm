(define-module (crates-io on ly onlytypes-core) #:use-module (crates-io))

(define-public crate-onlytypes-core-0.1.4 (c (n "onlytypes-core") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y7rc4xvvklxysiiijzsidhi6wq0kach6chac9ys183154nj5783") (f (quote (("debugging"))))))

