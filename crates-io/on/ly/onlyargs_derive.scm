(define-module (crates-io on ly onlyargs_derive) #:use-module (crates-io))

(define-public crate-onlyargs_derive-0.1.0 (c (n "onlyargs_derive") (v "0.1.0") (d (list (d (n "myn") (r "^0.1") (d #t) (k 0)) (d (n "onlyargs") (r "^0.1") (d #t) (k 0)))) (h "0935vxc72ikq6xfyahpnxjkxi9chd17gq3hq6ihjg37pw0akng85")))

(define-public crate-onlyargs_derive-0.1.1 (c (n "onlyargs_derive") (v "0.1.1") (d (list (d (n "myn") (r "^0.1") (d #t) (k 0)) (d (n "onlyargs") (r "^0.1") (d #t) (k 0)))) (h "0mzgvjm3146klmdn7aphidbmqxhcafgfs34b2vilb1hzifh5hm1a")))

(define-public crate-onlyargs_derive-0.1.2 (c (n "onlyargs_derive") (v "0.1.2") (d (list (d (n "myn") (r "^0.1") (d #t) (k 0)) (d (n "onlyargs") (r "^0.1") (d #t) (k 0)))) (h "1mk5qmc8v1qh70iv6l7npaycvxm4wp7chh9fadzx2fc8v3amcjj0")))

(define-public crate-onlyargs_derive-0.1.3 (c (n "onlyargs_derive") (v "0.1.3") (d (list (d (n "myn") (r "^0.2.1") (d #t) (k 0)) (d (n "onlyargs") (r "^0.1") (d #t) (k 0)))) (h "0pv4h8pcz9fbccvpy5dj5cf9cmkfj0f60i1hc3v54wajndzxi4w7") (r "1.62.0")))

(define-public crate-onlyargs_derive-0.1.4 (c (n "onlyargs_derive") (v "0.1.4") (d (list (d (n "myn") (r "^0.2.1") (d #t) (k 0)) (d (n "onlyargs") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "06r866b2pn66x02qnr1478mpvfrd77frgmjk6ybqdb8p697gp32f") (r "1.62.0")))

(define-public crate-onlyargs_derive-0.2.0 (c (n "onlyargs_derive") (v "0.2.0") (d (list (d (n "myn") (r "^0.2.1") (d #t) (k 0)) (d (n "onlyargs") (r "^0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1mkwckyj84dnhbphz2bmqqzmdfk58wc08cfcfizdfhxyrv4dx5ly") (r "1.62.0")))

