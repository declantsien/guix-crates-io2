(define-module (crates-io on vi onvif-rs) #:use-module (crates-io))

(define-public crate-onvif-rs-0.0.1 (c (n "onvif-rs") (v "0.0.1") (d (list (d (n "quick-xml") (r "^0.12.0") (d #t) (k 0)))) (h "1plgq005ks1781nl45px8p6ljf6yqdjzw6vynv93ac18vziych6q")))

(define-public crate-onvif-rs-0.0.2 (c (n "onvif-rs") (v "0.0.2") (d (list (d (n "quick-xml") (r "^0.12.0") (d #t) (k 0)))) (h "1zw04gywbpngn53zaz03d1pc9s23x9wl78wbdsm65lh1yf1bqc1c")))

(define-public crate-onvif-rs-0.0.3 (c (n "onvif-rs") (v "0.0.3") (d (list (d (n "quick-xml") (r "^0.12.0") (d #t) (k 0)))) (h "1idx4xrh5wdy7dsiy5m8wpjkldf2wpl6fsfsml07gl41q4iccg66")))

(define-public crate-onvif-rs-0.0.4 (c (n "onvif-rs") (v "0.0.4") (d (list (d (n "derive_builder") (r "^0.5.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.12.0") (d #t) (k 0)))) (h "0xb5sy65xqphjrgwlhv0msqd28c09ikg4yn5agvq0pici4s8hzmd")))

