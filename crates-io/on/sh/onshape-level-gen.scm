(define-module (crates-io on sh onshape-level-gen) #:use-module (crates-io))

(define-public crate-onshape-level-gen-0.1.4 (c (n "onshape-level-gen") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "creak") (r "^0.3") (f (quote ("vorbis"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spectrum-analyzer") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gh7kv78pyqd070c5lkaifq8pk7m1ns1b8wkjlq0afbx9z6hdzkp")))

(define-public crate-onshape-level-gen-0.1.5 (c (n "onshape-level-gen") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "creak") (r "^0.3") (f (quote ("vorbis"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spectrum-analyzer") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0l93r4wjajc5h0ba32vnwzmhkilhzfka98fmbb2m2bagfj11ngd2")))

(define-public crate-onshape-level-gen-0.1.6 (c (n "onshape-level-gen") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "creak") (r "^0.3") (f (quote ("vorbis"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spectrum-analyzer") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xayhwx7rkl6ypv46icjb1b0b0h3rp0pki0szyh48ml4rpbgfckx")))

