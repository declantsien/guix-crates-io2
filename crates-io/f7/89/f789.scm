(define-module (crates-io f7 #{89}# f789) #:use-module (crates-io))

(define-public crate-f789-0.1.0 (c (n "f789") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 0)))) (h "1ikkwzckxl7b0zf1yjqvxb20b8yq2717jx1qsccajpvpnc24glp4")))

