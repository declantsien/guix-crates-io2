(define-module (crates-io mu cl mucli) #:use-module (crates-io))

(define-public crate-mucli-0.1.0 (c (n "mucli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.15") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simplecrypt") (r "^1.0.2") (d #t) (k 0)))) (h "18kb5d3n1pdlrvz2xqr2zn9wry7k2binhinrhbhypvd6cigahs5f")))

