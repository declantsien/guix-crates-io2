(define-module (crates-io mu dd mudders) #:use-module (crates-io))

(define-public crate-mudders-0.0.0 (c (n "mudders") (v "0.0.0") (h "1l9js7f6cjwx2kx2xhmxbh5vqhx2bvihmmfq08s4ls20lp1nzzjr")))

(define-public crate-mudders-0.0.1 (c (n "mudders") (v "0.0.1") (h "15pvzjfpz3p8pi759l4ifx5z6p3ar34di32khh9my25kxml3bbrj")))

(define-public crate-mudders-0.0.2 (c (n "mudders") (v "0.0.2") (h "1jbpkc8ckyiw0q9a8smqfknhc1zcw8yakcm9mif128hqqy87hqwx")))

(define-public crate-mudders-0.0.3 (c (n "mudders") (v "0.0.3") (h "1qk4wcjxrc7gsn3ik797gkhpmw4ckganqyb820r6xg524zwz4lgs")))

(define-public crate-mudders-0.0.4 (c (n "mudders") (v "0.0.4") (h "0mxx0x0972rd8xadjxh91q6fmqk9wv3ign2a2j1bcddw8iijcni9")))

