(define-module (crates-io mu dd muddy) #:use-module (crates-io))

(define-public crate-muddy-0.2.0 (c (n "muddy") (v "0.2.0") (d (list (d (n "chacha20poly1305") (r "^0.10") (d #t) (k 0)) (d (n "const-hex") (r "^1.11") (d #t) (k 0)) (d (n "muddy_macros") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)))) (h "1ff3w2ikwvg0zmm1nqaik17qxdfi7my8cayprzg7k1v3hz86i2r2")))

(define-public crate-muddy-0.2.1 (c (n "muddy") (v "0.2.1") (d (list (d (n "chacha20poly1305") (r "^0.10") (d #t) (k 0)) (d (n "const-hex") (r "^1.11") (d #t) (k 0)) (d (n "muddy_macros") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)))) (h "1ab28fy0wwhzzhls92nvg4azqcf3p0ga8003kagpa75rbhc28dj1")))

