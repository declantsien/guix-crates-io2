(define-module (crates-io mu dd mudder) #:use-module (crates-io))

(define-public crate-mudder-0.1.0 (c (n "mudder") (v "0.1.0") (h "1gdkh291sn643bsgcgcy40x6nl95l9ddwkkhcn4chw236csa4a7p") (y #t)))

(define-public crate-mudder-0.1.3 (c (n "mudder") (v "0.1.3") (d (list (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1vvcknx81q02x7a7x2hs39fjq6cj967kqdhsajvjcr1cl4206jbf")))

(define-public crate-mudder-0.1.4 (c (n "mudder") (v "0.1.4") (d (list (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0fplyqgwzid62ri0d3s26qvg9vfl7dgl6mq316p58mrg6nli0yir")))

(define-public crate-mudder-0.1.5 (c (n "mudder") (v "0.1.5") (d (list (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0cyg43ps9jaszzwnab79wy2c7lrm0scq8jhymrqmigng0pqdmj1c")))

