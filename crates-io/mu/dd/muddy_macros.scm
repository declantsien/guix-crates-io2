(define-module (crates-io mu dd muddy_macros) #:use-module (crates-io))

(define-public crate-muddy_macros-0.2.0 (c (n "muddy_macros") (v "0.2.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cwkhl9fvrqndj4zijghylkk9hwawpb61nicg0cycyxsv7fsp3kq")))

(define-public crate-muddy_macros-0.2.1 (c (n "muddy_macros") (v "0.2.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08hdbrl3jkzar5y4nc203kqjh6ingkz7ji4x18jhlhy30d53r03j")))

