(define-module (crates-io mu on muon-rs) #:use-module (crates-io))

(define-public crate-muon-rs-0.1.1 (c (n "muon-rs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0s672ysvkyvj8xw9gyxqm3w8q93as9qvi4f96gpv4siab8r610wf")))

(define-public crate-muon-rs-0.1.2 (c (n "muon-rs") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1j9486bbdqfaxbxkb5v1bvp0krk0gxhvygp7yd0slwaazrbjv7ji")))

(define-public crate-muon-rs-0.2.0 (c (n "muon-rs") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "173974sbvj162cvlaf5zqj5p7h4337cms839z37jk62aa5vw5x10")))

(define-public crate-muon-rs-0.2.1 (c (n "muon-rs") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0kpgiaaz4ibah12ahgd7mh986jpy74r7nlvlqq5q7wx3y8jag419")))

(define-public crate-muon-rs-0.2.2 (c (n "muon-rs") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "15a05wmc15m0jwy8qvwwp52pcb7yk9w7qk87gsjmlsa779i16ahp")))

(define-public crate-muon-rs-0.2.3 (c (n "muon-rs") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0yxy17df87np8waghb9wr8rdgg5ky9sj7bn9giqvi3qzrpvy00lz")))

