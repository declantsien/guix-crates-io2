(define-module (crates-io mu on muonts) #:use-module (crates-io))

(define-public crate-muonts-0.1.0 (c (n "muonts") (v "0.1.0") (d (list (d (n "burn") (r "^0.11.1") (f (quote ("ndarray" "train" "autodiff"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "036xq0vpbj3r200h7f95pd868j4bklrjzl6b07sj44847r0x6qw7")))

