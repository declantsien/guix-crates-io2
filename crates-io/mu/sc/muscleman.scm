(define-module (crates-io mu sc muscleman) #:use-module (crates-io))

(define-public crate-muscleman-0.1.0 (c (n "muscleman") (v "0.1.0") (h "0kkz73p3r11y63sdsqxk2lk1aagds86sghs799ihh8ag90pn84qv")))

(define-public crate-muscleman-0.2.0 (c (n "muscleman") (v "0.2.0") (h "1f4z5y8cqnj6niqavdg77g2iv13f1nr7cqlhd5n0s2j59s1p2kw4") (y #t)))

(define-public crate-muscleman-0.2.1 (c (n "muscleman") (v "0.2.1") (h "1k2nbxkjxw1kp1inmw5gn56yizlcb6dr875n1mvlsd7ak4b1g058")))

(define-public crate-muscleman-0.3.0 (c (n "muscleman") (v "0.3.0") (h "00g08fyhb24vkpg6g96yj8zag9mnhxpsvg2n6xnxg9wy2xd46dy2")))

(define-public crate-muscleman-0.3.1 (c (n "muscleman") (v "0.3.1") (h "1wa7nlch8hcl37a4wh3m1h3p5bgb5fvbiwq9fzk0mmvgxfvn619x")))

