(define-module (crates-io mu sc muscab1-pac) #:use-module (crates-io))

(define-public crate-muscab1-pac-0.0.1 (c (n "muscab1-pac") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1ha2hmwxa2a5kjn71ri10cwi3laz0rw401wh3s10bmxrs97v5gpm")))

