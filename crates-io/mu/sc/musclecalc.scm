(define-module (crates-io mu sc musclecalc) #:use-module (crates-io))

(define-public crate-musclecalc-0.0.0-alpha (c (n "musclecalc") (v "0.0.0-alpha") (h "1ajfgq11iis7jzn0xabgzfwknf07k1sfgs5wrw3wbnybz1him6yq") (y #t)))

(define-public crate-musclecalc-0.1.0-alpha (c (n "musclecalc") (v "0.1.0-alpha") (h "070mgwl6nwpf59mw0fj8gmrzg9d2jg8jsrr877rblfl4fsjg32cn") (y #t)))

(define-public crate-musclecalc-0.1.0 (c (n "musclecalc") (v "0.1.0") (h "0amrhk26qnwda1kp2ngydz95b2lkfrpydwnndnjssg4267lplzyb")))

(define-public crate-musclecalc-0.2.0 (c (n "musclecalc") (v "0.2.0") (h "1849qng8s7flzsmlmmv46zm4w6h4502wfwd04s8nhdyra832asd3")))

(define-public crate-musclecalc-0.2.1 (c (n "musclecalc") (v "0.2.1") (h "0kmqkn2xhslyihw5bz73yxs8zfwlvhpm67xf5liidn0izplnqrhd")))

(define-public crate-musclecalc-0.2.2 (c (n "musclecalc") (v "0.2.2") (h "057hlaybcrrlnkm405fii6410d3drsqry8sw58qz0dg7caiqgrxk")))

(define-public crate-musclecalc-0.2.3 (c (n "musclecalc") (v "0.2.3") (h "1hh3xw203rasiz9xz6bwrxz3i9ng6rh95i3zks1h04m7vg0zqy2f")))

(define-public crate-musclecalc-0.2.4 (c (n "musclecalc") (v "0.2.4") (h "0khzjh82frrd6l8llfa9rpcy3k35bzhismgrha28m6npqp3fpwh3")))

