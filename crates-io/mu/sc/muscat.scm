(define-module (crates-io mu sc muscat) #:use-module (crates-io))

(define-public crate-muscat-0.1.0 (c (n "muscat") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "npyz") (r "^0.7.4") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fmrg40w9xms6sa5qv3s7jp1vn0z9g544imdn7xvw9mdlgkw7vd8") (y #t)))

(define-public crate-muscat-0.1.1 (c (n "muscat") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "npyz") (r "^0.7.4") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1219rw7w1afpa1bhrqznkfc2320gm49wgmva624dk21gzrv5x911")))

(define-public crate-muscat-0.2.0 (c (n "muscat") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "npyz") (r "^0.7.4") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01j6j3h9krhb8727nzz4bj639mbsw2mg58ggpjvwfynch3n9bgnp")))

