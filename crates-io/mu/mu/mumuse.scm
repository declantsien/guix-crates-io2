(define-module (crates-io mu mu mumuse) #:use-module (crates-io))

(define-public crate-mumuse-0.1.0 (c (n "mumuse") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)))) (h "0vs87klabw2zh5xdll0md8wjmghnpjrkxdmpiz51p6n5r53cz08w")))

(define-public crate-mumuse-0.2.0 (c (n "mumuse") (v "0.2.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)))) (h "14jqndmllsbd0vwbqv65dzfvx2dxbj38r93dz39zmnz2dl8r1z33")))

(define-public crate-mumuse-0.3.0 (c (n "mumuse") (v "0.3.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1hipvycnfz04bah5wq8gx2yv29c73v6cja4vj5hwlhp2d63fiv3h")))

(define-public crate-mumuse-0.4.0 (c (n "mumuse") (v "0.4.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0n9ccpm0criz80nmcz835ywr7bz3dc82wqss811i255j8hiwiqm2")))

(define-public crate-mumuse-0.5.0 (c (n "mumuse") (v "0.5.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1g8lb0yh9aqzrcs5n53k0nrhwd96qjy6hbdqb5smjl7l0qkhgcj4") (r "1.58.0")))

(define-public crate-mumuse-0.6.0 (c (n "mumuse") (v "0.6.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "183p41jnfg75xc942bav0d60fy4849zxvws8cnf0dnvxc756dqcl") (r "1.58.0")))

(define-public crate-mumuse-0.7.0 (c (n "mumuse") (v "0.7.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14mmi5hf4g804g2ihryhr43ffsf7yaxgs6cmpr6a59xfzkf786z2") (r "1.58.0")))

