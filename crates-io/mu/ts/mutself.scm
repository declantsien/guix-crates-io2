(define-module (crates-io mu ts mutself) #:use-module (crates-io))

(define-public crate-mutself-0.1.0 (c (n "mutself") (v "0.1.0") (d (list (d (n "object") (r "^0.30") (f (quote ("read_core" "write_std" "elf"))) (t "cfg(unix)") (k 0)) (d (n "object") (r "^0.30") (f (quote ("read_core" "write_std" "pe"))) (t "cfg(windows)") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing" "clone-impls" "proc-macro" "full" "extra-traits"))) (k 0)))) (h "0lrzazxn6ry50mg3acv4ra60bma8a72ybw9ifwfpbac544hlp84g") (r "1.61")))

(define-public crate-mutself-0.2.0 (c (n "mutself") (v "0.2.0") (d (list (d (n "object") (r "^0.31") (f (quote ("read_core" "write_std" "elf"))) (t "cfg(unix)") (k 0)) (d (n "object") (r "^0.31") (f (quote ("read_core" "write_std" "pe"))) (t "cfg(windows)") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing" "clone-impls" "proc-macro" "full" "extra-traits"))) (k 0)))) (h "1yjk6128fjfhj0r0jl82x2dhcm2jpmb5arlffpxyajdpg0cgfllv") (r "1.61")))

