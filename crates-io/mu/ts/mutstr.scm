(define-module (crates-io mu ts mutstr) #:use-module (crates-io))

(define-public crate-mutstr-0.1.0 (c (n "mutstr") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1syrqajm19z106yg6igjqx7y3lw7w02ck3kcqrwp4m1la2bqk1s7") (f (quote (("drop") ("default" "drop"))))))

(define-public crate-mutstr-0.1.1 (c (n "mutstr") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1c2jfkp17sb485pjzpr1mgs6svc13pba6r329ffg08l2qn7vi77q") (f (quote (("drop") ("default" "drop"))))))

(define-public crate-mutstr-0.2.0 (c (n "mutstr") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qxppcs4bh63maf1hxkpkimk90ln21k832sa1qnrf9zgi07vdiks") (f (quote (("drop") ("default" "drop"))))))

