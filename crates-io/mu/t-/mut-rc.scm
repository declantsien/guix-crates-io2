(define-module (crates-io mu t- mut-rc) #:use-module (crates-io))

(define-public crate-mut-rc-0.1.0 (c (n "mut-rc") (v "0.1.0") (h "0gd9yk6jd8k3jb35iksfxgjh5vy44lxm5rna9xn0bkqnymfg25fq")))

(define-public crate-mut-rc-0.1.1 (c (n "mut-rc") (v "0.1.1") (h "090i80af5yprms5r9apbx530an0886118d3hi3vdbafvvmydwawg")))

(define-public crate-mut-rc-0.1.2 (c (n "mut-rc") (v "0.1.2") (h "0gsnpr8kl4dga1sk5yci97dlwh9whmrpfvzh0451fxqs45dh61x1")))

