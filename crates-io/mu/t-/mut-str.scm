(define-module (crates-io mu t- mut-str) #:use-module (crates-io))

(define-public crate-mut-str-1.0.0-alpha.1 (c (n "mut-str") (v "1.0.0-alpha.1") (h "00yn120dpjzll59ia501rj0hgc2zrq4g74if1c2h2a965k2gsghx") (f (quote (("std") ("default" "std"))))))

(define-public crate-mut-str-1.0.0-alpha.2 (c (n "mut-str") (v "1.0.0-alpha.2") (h "0kfkgzhy6s1gbry1r0dh53pmr49hk4h5niba17vwmqf4qx8gym0h") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-mut-str-1.0.0-alpha.3 (c (n "mut-str") (v "1.0.0-alpha.3") (h "0ybfngm1allsh18naqi2l861ffyj6b5dfcv9v3fb8yfaxcd83255") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-mut-str-1.0.0 (c (n "mut-str") (v "1.0.0") (h "0jxarw6yab400k74y01xmf210l004q631s3wbiy6fmp19ikda4b7") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-mut-str-1.0.1 (c (n "mut-str") (v "1.0.1") (h "15z3gxxpw3vcplbyvhyv66iz2iirpw7dv0nsfzdvcsnlii0bsxr2") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-mut-str-1.0.2 (c (n "mut-str") (v "1.0.2") (h "1fniy3x85b1d320b2npc9py50bam63pr3gkfccwh8f2l596zdgkf") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-mut-str-1.1.0-alpha.1 (c (n "mut-str") (v "1.1.0-alpha.1") (h "0kldkl28xy23l1aygawl1yv24wg7jkzdbz37zc4p564yhvng0x3q") (f (quote (("std" "alloc") ("future") ("default" "std") ("alloc"))))))

