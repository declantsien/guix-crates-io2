(define-module (crates-io mu t- mut-binary-heap) #:use-module (crates-io))

(define-public crate-mut-binary-heap-0.1.0 (c (n "mut-binary-heap") (v "0.1.0") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)))) (h "080mq66729mkmsdzmk2yc7kwzq8858w87cnhxm39vq6cav9h3fxa") (r "1.56.0")))

