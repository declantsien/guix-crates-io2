(define-module (crates-io mu n_ mun_project) #:use-module (crates-io))

(define-public crate-mun_project-0.1.0 (c (n "mun_project") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "paths") (r "=0.1.0") (d #t) (k 0) (p "mun_paths")) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0x6bwqg1f8yf6vf81y3phb1vwypf1ixn5w0gpgba8jiwdxcvp41b")))

(define-public crate-mun_project-0.4.0 (c (n "mun_project") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "mun_paths") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (f (quote ("std"))) (k 0)) (d (n "semver") (r "^1.0") (f (quote ("serde"))) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (k 0)) (d (n "toml") (r "^0.5") (k 0)))) (h "10yzydpkmqgz69fmy42kgv3ach2xgv8y1lzf76j44zcais7qp1rq")))

