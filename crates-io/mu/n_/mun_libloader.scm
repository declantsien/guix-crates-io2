(define-module (crates-io mu n_ mun_libloader) #:use-module (crates-io))

(define-public crate-mun_libloader-0.1.0 (c (n "mun_libloader") (v "0.1.0") (d (list (d (n "abi") (r "=0.3.0") (d #t) (k 0) (p "mun_abi")) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0anrmrhxpsc8iw43024gb28h9aiajaylqsdcjjd93gxvy31br2g8")))

(define-public crate-mun_libloader-0.4.0 (c (n "mun_libloader") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "libloading") (r "^0.7") (k 0)) (d (n "mun_abi") (r "^0.4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (k 0)))) (h "1cb1266hb4m8nspx82ca6mvi07zr235klpi2idzsfnabj371j1pg")))

