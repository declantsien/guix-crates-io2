(define-module (crates-io mu n_ mun_vfs) #:use-module (crates-io))

(define-public crate-mun_vfs-0.1.0 (c (n "mun_vfs") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.4") (d #t) (k 0)) (d (n "paths") (r "=0.1.0") (d #t) (k 0) (p "mun_paths")) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1siw3gzv5rm0vqqvv3lycsa33p9nh9kxqdnyvqfwpcc578k7vhdy")))

(define-public crate-mun_vfs-0.4.0 (c (n "mun_vfs") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (k 0)) (d (n "log") (r "^0.4.11") (k 0)) (d (n "mun_paths") (r "^0.4.0") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0fyzr4mrdkzj8s72jz0crlp9abgz8ch9cyx7fi2wm5zdghxzshpz")))

