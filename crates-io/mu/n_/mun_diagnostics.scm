(define-module (crates-io mu n_ mun_diagnostics) #:use-module (crates-io))

(define-public crate-mun_diagnostics-0.1.0 (c (n "mun_diagnostics") (v "0.1.0") (d (list (d (n "mun_hir") (r "=0.3.0") (d #t) (k 0) (p "mun_hir")) (d (n "mun_syntax") (r "=0.3.0") (d #t) (k 0)))) (h "0jpfjzyjn2z9g09gmly8i92vqrlnxppqgh0vyygmzb6cayham7gf")))

(define-public crate-mun_diagnostics-0.4.0 (c (n "mun_diagnostics") (v "0.4.0") (d (list (d (n "mun_hir") (r "^0.4.0") (d #t) (k 0)) (d (n "mun_syntax") (r "^0.4.0") (d #t) (k 0)))) (h "0a6aprfnma96fc599vvqwf9k3iz3x1x9q91cp8vd0x5b9s721cr5")))

