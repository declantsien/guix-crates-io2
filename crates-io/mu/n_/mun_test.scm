(define-module (crates-io mu n_ mun_test) #:use-module (crates-io))

(define-public crate-mun_test-0.4.0 (c (n "mun_test") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "itertools") (r "^0.10.3") (k 0)) (d (n "mun_compiler") (r "^0.4.0") (d #t) (k 0)) (d (n "mun_hir") (r "^0.4.0") (d #t) (k 0)) (d (n "mun_paths") (r "^0.4.0") (d #t) (k 0)) (d (n "mun_runtime") (r "^0.4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (k 0)))) (h "15liw23921cj964f4jkb1xfq4a2zaxqm4wjxjlyns619b2k9xxw1")))

