(define-module (crates-io mu n_ mun_abi) #:use-module (crates-io))

(define-public crate-mun_abi-0.2.0 (c (n "mun_abi") (v "0.2.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "05q02djch4ayk1z64zq7d18wc1drp0g7z4wa13r7ify98rkxxcli")))

(define-public crate-mun_abi-0.3.0 (c (n "mun_abi") (v "0.3.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "02hhlf5k8wikywi6x979qq9lgjnsxcqym09i77qp9hd3nph6hg45")))

(define-public crate-mun_abi-0.4.0 (c (n "mun_abi") (v "0.4.0") (d (list (d (n "extendhash") (r "^1.0.9") (k 0)) (d (n "itertools") (r "^0.10.3") (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (k 0)) (d (n "serde") (r "^1.0.137") (o #t) (k 0)))) (h "1xz8v6024l22j3c3g45i23vdyg179b04chxxd6fi4560532pnj5k")))

