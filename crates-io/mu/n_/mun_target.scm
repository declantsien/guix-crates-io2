(define-module (crates-io mu n_ mun_target) #:use-module (crates-io))

(define-public crate-mun_target-0.2.0 (c (n "mun_target") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "insta") (r "^0.16") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0hcny9wk8n1hjajdhnzp6519vcsq3ry44ayc2f4zq7gzy5yalcjv")))

(define-public crate-mun_target-0.3.0 (c (n "mun_target") (v "0.3.0") (d (list (d (n "insta") (r "^0.16") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0lx8ynx63x7bkip98v6ph0ys2qzqrrlw9xh68vc4g142akgcciaq")))

(define-public crate-mun_target-0.4.0 (c (n "mun_target") (v "0.4.0") (d (list (d (n "insta") (r "^1.12.0") (k 2)) (d (n "log") (r "^0.4.8") (k 0)))) (h "1b6f6slp5qcfa8wh558zyjznbxi418xmi4sapi4133qwilsvyaax")))

