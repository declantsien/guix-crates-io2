(define-module (crates-io mu n_ mun_lld) #:use-module (crates-io))

(define-public crate-mun_lld-70.2.0 (c (n "mun_lld") (v "70.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 1)))) (h "0yq4vk43bcchmrcrahhp7bjzqraxms5qhmqm8fk8ggpir8186bcw")))

(define-public crate-mun_lld-110.0.0 (c (n "mun_lld") (v "110.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 1)))) (h "03zqkbfc5571rj31gvmqaah660vka8l9dn51cn6ay0jlkz614hn3")))

