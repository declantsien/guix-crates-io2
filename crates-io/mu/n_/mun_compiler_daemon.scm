(define-module (crates-io mu n_ mun_compiler_daemon) #:use-module (crates-io))

(define-public crate-mun_compiler_daemon-0.3.0 (c (n "mun_compiler_daemon") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mun_codegen") (r "=0.3.0") (d #t) (k 0)) (d (n "mun_compiler") (r "=0.3.0") (d #t) (k 0)) (d (n "mun_hir") (r "=0.3.0") (d #t) (k 0)) (d (n "mun_project") (r "=0.1.0") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "1898xs71pl7j3fxspfalgipxwg5hcx9czwz5vf5hdjqjal2l5bia")))

(define-public crate-mun_compiler_daemon-0.4.0 (c (n "mun_compiler_daemon") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.31") (k 0)) (d (n "ctrlc") (r "^3.1") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "mun_codegen") (r "^0.4.0") (d #t) (k 0)) (d (n "mun_compiler") (r "^0.4.0") (d #t) (k 0)) (d (n "mun_hir") (r "^0.4.0") (d #t) (k 0)) (d (n "mun_project") (r "^0.4.0") (d #t) (k 0)) (d (n "notify") (r "^4.0") (k 0)))) (h "10rkv160dhgv5hc7p7jqfq4s8bhl8xang3c7nl9hcbq65z95gc4v")))

