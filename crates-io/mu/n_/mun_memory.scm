(define-module (crates-io mu n_ mun_memory) #:use-module (crates-io))

(define-public crate-mun_memory-0.1.0 (c (n "mun_memory") (v "0.1.0") (d (list (d (n "abi") (r "= 0.2.0") (d #t) (k 0) (p "mun_abi")) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 2)))) (h "0597kbz93j5lmgggslirypmdnwimswk08c5rj7ll0m645151ks4w")))

(define-public crate-mun_memory-0.2.0 (c (n "mun_memory") (v "0.2.0") (d (list (d (n "abi") (r "=0.3.0") (d #t) (k 0) (p "mun_abi")) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 2)))) (h "1blqpjpfzmsan87260zv1myhv2rzd0jmqm9avsn64fhphmm1zy88")))

(define-public crate-mun_memory-0.4.0 (c (n "mun_memory") (v "0.4.0") (d (list (d (n "insta") (r "^1.12.0") (f (quote ("ron"))) (k 2)) (d (n "itertools") (r "^0.10.3") (k 0)) (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "mun_abi") (r "^0.4.0") (d #t) (k 0)) (d (n "mun_capi_utils") (r "^0.4.0") (d #t) (k 0)) (d (n "mun_capi_utils") (r "^0.4.0") (f (quote ("insta"))) (d #t) (k 2)) (d (n "once_cell") (r "^1.4.0") (k 0)) (d (n "parking_lot") (r "^0.12.0") (k 0)) (d (n "paste") (r "^1.0") (k 2)) (d (n "rustc-hash") (r "^1.1") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.19") (k 0)))) (h "0hq70jkhn0ccl1ndda1vkjhfvnvgiz1wqr6xjhppjmlwn3jn1bx6")))

