(define-module (crates-io mu n_ mun_codegen_macros) #:use-module (crates-io))

(define-public crate-mun_codegen_macros-0.1.0 (c (n "mun_codegen_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1f1c58yy12gi745h3yda8270gjy60rkxc6pnwnn3hc5shhl3iik5")))

(define-public crate-mun_codegen_macros-0.4.0 (c (n "mun_codegen_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "09b84jrjc0x2lqszvrliyp93l7fw2424hplhnpg7k28k6k8gnpm0")))

