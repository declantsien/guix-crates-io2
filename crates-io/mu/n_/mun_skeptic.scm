(define-module (crates-io mu n_ mun_skeptic) #:use-module (crates-io))

(define-public crate-mun_skeptic-0.1.0 (c (n "mun_skeptic") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.4.7") (d #t) (k 0)) (d (n "mun_compiler") (r "=0.3.0") (d #t) (k 0)) (d (n "mun_runtime") (r "=0.3.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1cfjisyil5hrbrqwirb5rj5z2rd7jy2q4r21wyamws7fh68w0qc3")))

(define-public crate-mun_skeptic-0.4.0 (c (n "mun_skeptic") (v "0.4.0") (d (list (d (n "bytecount") (r "^0.6.2") (k 0)) (d (n "itertools") (r "^0.10.0") (k 0)) (d (n "mdbook") (r "^0.4.7") (k 0)) (d (n "mun_compiler") (r "^0.4.0") (d #t) (k 0)) (d (n "mun_runtime") (r "^0.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.1") (k 0)) (d (n "tempdir") (r "^0.3.7") (k 0)))) (h "1ixs4v0bksxa6f5dm08xlwqk65zvmhv5qsh9sqx97s5g9f1nqvgr")))

