(define-module (crates-io mu n_ mun_paths) #:use-module (crates-io))

(define-public crate-mun_paths-0.1.0 (c (n "mun_paths") (v "0.1.0") (d (list (d (n "relative-path") (r "^1.2") (d #t) (k 0)))) (h "0bqfqapg40wb3q1m7l7681z5bqjk06kdbng1p7qf1gi1vqzygw21")))

(define-public crate-mun_paths-0.4.0 (c (n "mun_paths") (v "0.4.0") (d (list (d (n "relative-path") (r "^1.2") (k 0)))) (h "1bg2vjkr3viqyisjxaspwv58rvgk9pg4nj5590f61akll4pqf3rc")))

