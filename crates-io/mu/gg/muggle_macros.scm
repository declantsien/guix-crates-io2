(define-module (crates-io mu gg muggle_macros) #:use-module (crates-io))

(define-public crate-muggle_macros-0.1.0 (c (n "muggle_macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "080l9kn65a9xf3a6m7zw30za6svpipacz2l18zkjdfmnxk5bpra4")))

