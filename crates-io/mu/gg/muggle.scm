(define-module (crates-io mu gg muggle) #:use-module (crates-io))

(define-public crate-muggle-0.1.0 (c (n "muggle") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (f (quote ("std"))) (o #t) (k 0)))) (h "0n7fpabl4sgln45837402mmnhywysp6dylbh11z1vaygsl3f5287") (f (quote (("trace-errors" "tracing") ("trace-calls" "tracing"))))))

(define-public crate-muggle-0.1.1 (c (n "muggle") (v "0.1.1") (d (list (d (n "angel") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "muggle_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "01cynjbvgiilxn17k89z43djg7zv7si3sxwaana0rcbbxawqdb3w") (f (quote (("trace-errors" "angel/trace-errors") ("trace-calls" "angel/trace-calls"))))))

