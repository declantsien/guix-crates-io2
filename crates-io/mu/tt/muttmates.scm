(define-module (crates-io mu tt muttmates) #:use-module (crates-io))

(define-public crate-muttmates-0.1.0 (c (n "muttmates") (v "0.1.0") (d (list (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0jf3kw4ccci6n49n93b37nnda134c6iw45srg5ymaf4387fcvcy3")))

(define-public crate-muttmates-0.1.1 (c (n "muttmates") (v "0.1.1") (d (list (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "19d1vjng6gdy41gyf4icf60904hvb1yhbpm82znp5cbimbncx6b3")))

(define-public crate-muttmates-0.1.2 (c (n "muttmates") (v "0.1.2") (d (list (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1l66rim8imvypa3rlbd3m7x6z9mqii9hnfa5kvijvxz01zs2j5d0")))

(define-public crate-muttmates-0.1.3 (c (n "muttmates") (v "0.1.3") (d (list (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0y5983r2fsq1y2i9kr1rklrw615ianlgj3pmd1frrcxw3d46rlmi")))

