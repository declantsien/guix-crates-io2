(define-module (crates-io mu ld muldiv) #:use-module (crates-io))

(define-public crate-muldiv-0.1.0 (c (n "muldiv") (v "0.1.0") (d (list (d (n "num") (r "0.1.*") (d #t) (k 2)) (d (n "rand") (r "0.3.*") (d #t) (k 2)))) (h "1ndylirsmq6bv2jxp8ds4gb6rij1msihxgfrl75z4vdkpvg2cz0v") (f (quote (("x86-64-assembly") ("default"))))))

(define-public crate-muldiv-0.1.1 (c (n "muldiv") (v "0.1.1") (d (list (d (n "num") (r "0.1.*") (d #t) (k 2)) (d (n "rand") (r "0.3.*") (d #t) (k 2)))) (h "0gjvdhij5c6xbfzglnz58vpy2hwqrhs69qi0rhc2mn4c5smgbghw") (f (quote (("x86-64-assembly") ("default"))))))

(define-public crate-muldiv-0.2.0 (c (n "muldiv") (v "0.2.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "03wqr4v1lwm18m094mz9vwpysyfgjpmf0dbqi5n5cb53s82rl6j5")))

(define-public crate-muldiv-0.2.1 (c (n "muldiv") (v "0.2.1") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "014jlry2l2ph56mp8knw65637hh49q7fmrraim2bx9vz0a638684")))

(define-public crate-muldiv-1.0.0 (c (n "muldiv") (v "1.0.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1wq74n9sh4mgc31hrs56bzrq29w8v9g9znyyrhl2f60ll7fnw4xm")))

(define-public crate-muldiv-1.0.1 (c (n "muldiv") (v "1.0.1") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1c6ljsp41n8ijsx7zicwfm135drgyhcms12668ivvsbm1r98frwm")))

