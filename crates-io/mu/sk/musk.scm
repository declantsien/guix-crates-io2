(define-module (crates-io mu sk musk) #:use-module (crates-io))

(define-public crate-musk-6.6.6 (c (n "musk") (v "6.6.6") (h "06bnpj20530qm1jxdazkb49zvj2316qchfyi6clba0rnjk1zmai5") (y #t)))

(define-public crate-musk-0.0.0 (c (n "musk") (v "0.0.0") (h "0a98721mr1fqiqbr3hnlikgpg7dd73bgc26h7qm3bb0mvly0ar4b")))

