(define-module (crates-io mu bo mubo) #:use-module (crates-io))

(define-public crate-mubo-0.1.0 (c (n "mubo") (v "0.1.0") (h "1j8f8dkm2r2gkwsb0260795v1yhxqsq2pjbpbjpq9pj1g0rwmmps")))

(define-public crate-mubo-0.1.1 (c (n "mubo") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0xhfi1lcdzwy6883li33rafgxv13kkpfb4vgfn9xk0c98km07wig")))

(define-public crate-mubo-0.1.2 (c (n "mubo") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0885vpxzj8svc7d30bjn2ky9plyd117w4vdsm1bww7yclrfqgqnp")))

(define-public crate-mubo-0.1.3 (c (n "mubo") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1xlz0f0sahkafxxnnb13kbllkps5k5p15jzf2ppk73rd6329n5is")))

(define-public crate-mubo-0.1.4 (c (n "mubo") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "13p770nl0x64jlhh3mqnji0wvk6bg2227lddgmf35mpy6c7lg27m")))

(define-public crate-mubo-0.1.5 (c (n "mubo") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0fqd7iw8pgxhajpfws3j6vb1a4jsyxrgc8iajzw6imw36ph9l8kh")))

(define-public crate-mubo-0.1.6 (c (n "mubo") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1z4knczz1rnivyw7abyfapbpxgpjx4p122js1d69rr838xi3a565")))

