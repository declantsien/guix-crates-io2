(define-module (crates-io mu sl musli-storage) #:use-module (crates-io))

(define-public crate-musli-storage-0.0.1 (c (n "musli-storage") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.56") (o #t) (d #t) (k 0)) (d (n "musli") (r "^0.0.1") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.1") (d #t) (k 0)))) (h "16pw8zvx5f083wj5j1vymh83ad5zl0dv0idy11v0v49psy39gb97") (f (quote (("test" "anyhow") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.2 (c (n "musli-storage") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.56") (o #t) (d #t) (k 0)) (d (n "musli") (r "^0.0.2") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.2") (d #t) (k 0)))) (h "12lgn96w84qxah2dfygsjav9vwpca3v8s4ny99y5qwss6rg21rzz") (f (quote (("test" "anyhow") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.3 (c (n "musli-storage") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.56") (o #t) (d #t) (k 0)) (d (n "musli") (r "^0.0.3") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.3") (d #t) (k 0)))) (h "18ja45pvbmnjbj7fvx8dyd8q7n0fwcb007n86ha7r5f4wlic29v4") (f (quote (("test" "anyhow") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.4 (c (n "musli-storage") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.56") (o #t) (d #t) (k 0)) (d (n "musli") (r "^0.0.4") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.4") (d #t) (k 0)))) (h "1sal81v3r5jbkyxa34nvznf699p7wrr4j5312kvgj0nifvhd8ydg") (f (quote (("test" "anyhow") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.5 (c (n "musli-storage") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.56") (o #t) (d #t) (k 0)) (d (n "musli") (r "^0.0.5") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.5") (d #t) (k 0)))) (h "16xswdr1ad0d0vybrbi9jbzlfg1ibbq2km3iqyznp4h2n4gj3law") (f (quote (("test" "anyhow") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.6 (c (n "musli-storage") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.56") (o #t) (d #t) (k 0)) (d (n "musli") (r "^0.0.6") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.6") (d #t) (k 0)))) (h "0jqw24wxm3fvzckirir9jsdqzy7xr5y00cjhqrwkpfxw9g6jv2v7") (f (quote (("test" "anyhow") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.7 (c (n "musli-storage") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.56") (o #t) (d #t) (k 0)) (d (n "musli") (r "^0.0.7") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.7") (d #t) (k 0)))) (h "191rd9vv7gz4vx1a1sj52ix5b2r8zkkbh3nk9m6vvpybjh5gzqwy") (f (quote (("test" "anyhow") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.8 (c (n "musli-storage") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.56") (o #t) (d #t) (k 0)) (d (n "musli") (r "^0.0.8") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.8") (d #t) (k 0)))) (h "0mx2j62f052l88vcg4jjw3qyskis8f0b547anzxqrdfj02xbp8i3") (f (quote (("test" "anyhow") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.9 (c (n "musli-storage") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.56") (o #t) (d #t) (k 0)) (d (n "musli") (r "^0.0.9") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.9") (d #t) (k 0)))) (h "0lcx5rggf3yjw1l9bgx81m6nd9k93qgqz67szxx2230rkl1jsahl") (f (quote (("test" "anyhow") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.10 (c (n "musli-storage") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.56") (o #t) (d #t) (k 0)) (d (n "musli") (r "^0.0.10") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.10") (d #t) (k 0)))) (h "1pldrax6irf017x33xw2lgj4ps7pjfpqfsvm0v3sxd96713gm4px") (f (quote (("test" "anyhow") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.11 (c (n "musli-storage") (v "0.0.11") (d (list (d (n "anyhow") (r "^1.0.56") (o #t) (d #t) (k 0)) (d (n "musli") (r "^0.0.11") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.11") (d #t) (k 0)))) (h "18pyq5akq0glsfbya56zp68dphmai7fai3sni8p5xn2vn9hhlw2a") (f (quote (("test" "anyhow") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.12 (c (n "musli-storage") (v "0.0.12") (d (list (d (n "anyhow") (r "^1.0.56") (o #t) (d #t) (k 0)) (d (n "musli") (r "^0.0.12") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.12") (d #t) (k 0)))) (h "0hc0k3zlrk3d5x6avry4277446yvra6xs6iy22c6iy10nsxicz5a") (f (quote (("test" "anyhow") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.13 (c (n "musli-storage") (v "0.0.13") (d (list (d (n "musli") (r "^0.0.13") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.13") (d #t) (k 0)))) (h "1s80n0pgw84ny976mspiyxnx9dm94zrc4pm3gpqmh36pw965bpdb") (f (quote (("test") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.14 (c (n "musli-storage") (v "0.0.14") (d (list (d (n "musli") (r "^0.0.14") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.14") (d #t) (k 0)))) (h "0w0719456p6gbixys9apvblnv7ii955h57nqg88idfsc0zz4bk3l") (f (quote (("test") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.15 (c (n "musli-storage") (v "0.0.15") (d (list (d (n "musli") (r "^0.0.15") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.15") (d #t) (k 0)))) (h "1zr1s88w0b6fk3mbc37jcpsa2spzlqsf8bslvzl09mx2wrqbpy4c") (f (quote (("test") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.16 (c (n "musli-storage") (v "0.0.16") (d (list (d (n "musli") (r "^0.0.16") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.16") (d #t) (k 0)))) (h "1w8nvnqwmvrhpak3796rcj44bwd9544w9jn7fng4jlminpq3cms5") (f (quote (("test") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.17 (c (n "musli-storage") (v "0.0.17") (d (list (d (n "musli") (r "^0.0.17") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.17") (d #t) (k 0)))) (h "03gaymplma65i5sqz7q5d794ajmrs5dldgpfp6hfnd8x7mnlwhz4") (f (quote (("test") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.18 (c (n "musli-storage") (v "0.0.18") (d (list (d (n "musli") (r "^0.0.18") (d #t) (k 0)) (d (n "musli-binary-common") (r "^0.0.18") (d #t) (k 0)))) (h "0qchk3dsq6zlgg8aj8f60rfnn5k64kf8nsqw90k9cq3l4jfs7smw") (f (quote (("test") ("std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.19 (c (n "musli-storage") (v "0.0.19") (d (list (d (n "musli") (r "^0.0.19") (k 0)) (d (n "musli-binary-common") (r "^0.0.19") (k 0)))) (h "0i1yingslf61nv7hrhj2mz840wswgkaljxsvvliwnn178a7xh5c9") (f (quote (("test" "std") ("std" "musli/std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.20 (c (n "musli-storage") (v "0.0.20") (d (list (d (n "musli") (r "^0.0.20") (k 0)) (d (n "musli-binary-common") (r "^0.0.20") (k 0)))) (h "1027mfwr2aqwqpv7dldawhy84v2hsnl1xiw97kqr6fk54qg6zg6n") (f (quote (("test" "std") ("std" "musli/std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.21 (c (n "musli-storage") (v "0.0.21") (d (list (d (n "musli") (r "^0.0.21") (k 0)) (d (n "musli-binary-common") (r "^0.0.21") (k 0)))) (h "1xxacng57kmkdf1cga2chx9md5cd4k7f4hvfbn114j9gc41pkhyq") (f (quote (("test" "std") ("std" "musli/std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.22 (c (n "musli-storage") (v "0.0.22") (d (list (d (n "musli") (r "^0.0.22") (k 0)) (d (n "musli-binary-common") (r "^0.0.22") (k 0)))) (h "0bd43pbi0rklpm6px7wqp3m2y3kqv0f124pzyv12r2b5bk4nbfgd") (f (quote (("test" "std") ("std" "musli/std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.23 (c (n "musli-storage") (v "0.0.23") (d (list (d (n "musli") (r "^0.0.23") (k 0)) (d (n "musli-binary-common") (r "^0.0.23") (k 0)))) (h "17zjyas4pwqgqhc8m5bkkv5awhxsd3dgnya2q2yyfjy4khyd2lw6") (f (quote (("test" "std") ("std" "musli/std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.24 (c (n "musli-storage") (v "0.0.24") (d (list (d (n "musli") (r "^0.0.24") (k 0)) (d (n "musli-binary-common") (r "^0.0.24") (k 0)))) (h "0a1517andmmvyrcxg2fglg559wzpzpbji54yj001xmlyvbkblik6") (f (quote (("test" "std") ("std" "musli/std" "musli-binary-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.25 (c (n "musli-storage") (v "0.0.25") (d (list (d (n "musli") (r "^0.0.25") (k 0)) (d (n "musli-common") (r "^0.0.25") (k 0)))) (h "0fiwwy7nvf9zmlb6k4w2vqw9k5hsz85l2vin4lwjwbmpm69kkw7n") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.26 (c (n "musli-storage") (v "0.0.26") (d (list (d (n "musli") (r "^0.0.26") (k 0)) (d (n "musli-common") (r "^0.0.26") (k 0)))) (h "0rbdrcf972i26lcxzwbv6rlbclw0khy5lychyfmv8q89gf1z0sn8") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.27 (c (n "musli-storage") (v "0.0.27") (d (list (d (n "musli") (r "^0.0.27") (k 0)) (d (n "musli-common") (r "^0.0.27") (k 0)))) (h "1fxmrgp0d40xkg07n9951jawsdiln2fg6lhsc68n3h7dylg066np") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.28 (c (n "musli-storage") (v "0.0.28") (d (list (d (n "musli") (r "^0.0.28") (k 0)) (d (n "musli-common") (r "^0.0.28") (k 0)))) (h "19v4sxn6z2qdkkr92cj6mqlnkchxnly3n3ngvsfbk9v7bg7ffdc3") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.29 (c (n "musli-storage") (v "0.0.29") (d (list (d (n "musli") (r "^0.0.29") (k 0)) (d (n "musli-common") (r "^0.0.29") (k 0)))) (h "06598rdhrw28yhv2dhgpkira15aw7578m8isqrvq2n8zqpknzllb") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.30 (c (n "musli-storage") (v "0.0.30") (d (list (d (n "musli") (r "^0.0.30") (k 0)) (d (n "musli-common") (r "^0.0.30") (k 0)))) (h "00m6skdzs62k06yjyqgrdb0klwpwrbi4lwil4c1xl2wbym9p9356") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.31 (c (n "musli-storage") (v "0.0.31") (d (list (d (n "musli") (r "^0.0.31") (k 0)) (d (n "musli-common") (r "^0.0.31") (k 0)))) (h "0lx1drp3x4110f00gqjin57fl2pwa1wxfvcj4z8pf15bdfz39i9f") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.32 (c (n "musli-storage") (v "0.0.32") (d (list (d (n "musli") (r "^0.0.32") (k 0)) (d (n "musli-common") (r "^0.0.32") (k 0)))) (h "1pa0j8gznh56lpspmgyn89g2765h1iq74kl4ab9n1z4knp7y664w") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.33 (c (n "musli-storage") (v "0.0.33") (d (list (d (n "musli") (r "^0.0.33") (k 0)) (d (n "musli-common") (r "^0.0.33") (k 0)))) (h "16fd6dpc60fzkpaj78pc76xbxszs0ynqcay43irpn5zh7wjdws4g") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.34 (c (n "musli-storage") (v "0.0.34") (d (list (d (n "musli") (r "^0.0.34") (k 0)) (d (n "musli-common") (r "^0.0.34") (k 0)))) (h "1880fjqkwql034kjwal6b1i8apcj94l3azh0xclzra23krpl89ic") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.35 (c (n "musli-storage") (v "0.0.35") (d (list (d (n "musli") (r "^0.0.35") (k 0)) (d (n "musli-common") (r "^0.0.35") (k 0)))) (h "09cbrqihlv2aj42qfp8mlrvy4zh1r9adydp4cgyda5fs4bkyc1pb") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.36 (c (n "musli-storage") (v "0.0.36") (d (list (d (n "musli") (r "^0.0.36") (k 0)) (d (n "musli-common") (r "^0.0.36") (k 0)))) (h "0gcnymw1ryqx5ndr4vrcj4yk9slsxgncsq73fdwalbaha952ax4m") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.37 (c (n "musli-storage") (v "0.0.37") (d (list (d (n "musli") (r "^0.0.37") (k 0)) (d (n "musli-common") (r "^0.0.37") (k 0)))) (h "1j7af927df3gzhlgaih8mm9m00iz00i7q12dhvndg437hnvkd2mv") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std"))))))

(define-public crate-musli-storage-0.0.38 (c (n "musli-storage") (v "0.0.38") (d (list (d (n "musli") (r "^0.0.38") (k 0)) (d (n "musli-common") (r "^0.0.38") (k 0)))) (h "00yl7j42lwcmm96jipvdq1gzgxabnyz74i619n9bzgb0wq8nsd3k") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std")))) (r "1.65")))

(define-public crate-musli-storage-0.0.39 (c (n "musli-storage") (v "0.0.39") (d (list (d (n "musli") (r "^0.0.39") (k 0)) (d (n "musli-common") (r "^0.0.39") (k 0)))) (h "0mzci4vc7g8fiqqmf6arkq3lgxzlbirip4c1vr7d4n3x2ywskwj9") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std")))) (r "1.65")))

(define-public crate-musli-storage-0.0.40 (c (n "musli-storage") (v "0.0.40") (d (list (d (n "musli") (r "^0.0.40") (k 0)) (d (n "musli-common") (r "^0.0.40") (k 0)))) (h "05j8f2pf4b9pcf9h1ysi4jdimhcqljd8byvxi6aqycv4wzaysmg7") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std")))) (r "1.65")))

(define-public crate-musli-storage-0.0.41 (c (n "musli-storage") (v "0.0.41") (d (list (d (n "musli") (r "^0.0.41") (k 0)) (d (n "musli-common") (r "^0.0.41") (k 0)))) (h "1y87j6imjwszfkzvxqhqzsvhd5ixs3g4kn625rixsvqwhq62b4ms") (f (quote (("test" "std") ("std" "musli/std" "musli-common/std") ("default" "std")))) (r "1.65")))

(define-public crate-musli-storage-0.0.42 (c (n "musli-storage") (v "0.0.42") (d (list (d (n "musli") (r "^0.0.42") (k 0)) (d (n "musli-common") (r "^0.0.42") (k 0)))) (h "0z4k8ms8irnjslwk1r45r0vsvadkx4h44d71cb213xk6n601zz5j") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("default" "std") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.65")))

(define-public crate-musli-storage-0.0.43 (c (n "musli-storage") (v "0.0.43") (d (list (d (n "musli") (r "^0.0.43") (k 0)) (d (n "musli-common") (r "^0.0.43") (k 0)))) (h "0d2hiz27q7lbn3x08mxb0788m522x5w9vb2sawnr8nxpj3gksxcw") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("default" "std") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.65")))

(define-public crate-musli-storage-0.0.44 (c (n "musli-storage") (v "0.0.44") (d (list (d (n "musli") (r "^0.0.44") (k 0)) (d (n "musli-common") (r "^0.0.44") (k 0)))) (h "148n7cmn535vfa34aqv5jfgpsdr5k0kyia7m5d7n0zb024azlcv4") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("default" "std") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.65")))

(define-public crate-musli-storage-0.0.45 (c (n "musli-storage") (v "0.0.45") (d (list (d (n "musli") (r "^0.0.45") (k 0)) (d (n "musli-common") (r "^0.0.45") (k 0)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0adl6c7gpib9baka9flvmwfl6mgjsbl2vvs1qzfz472viw560jmk") (f (quote (("test") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (s 2) (e (quote (("std" "musli/std" "musli-common/std" "alloc" "simdutf8?/std")))) (r "1.65")))

(define-public crate-musli-storage-0.0.46 (c (n "musli-storage") (v "0.0.46") (d (list (d (n "musli") (r "^0.0.46") (k 0)) (d (n "musli-common") (r "^0.0.46") (k 0)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1274d1rpd81qsyzy18dsgg8r9mpck9d8vdrsgal0z1baxgli7jxi") (f (quote (("test") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (s 2) (e (quote (("std" "musli/std" "musli-common/std" "alloc" "simdutf8?/std")))) (r "1.65")))

(define-public crate-musli-storage-0.0.47 (c (n "musli-storage") (v "0.0.47") (d (list (d (n "musli") (r "^0.0.47") (k 0)) (d (n "musli-common") (r "^0.0.47") (k 0)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0c90aclnq407hy70mdwkil0qg8hmy7977ry8xvgmljrp4dk820ac") (f (quote (("test") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (s 2) (e (quote (("std" "musli/std" "musli-common/std" "alloc" "simdutf8?/std")))) (r "1.66")))

(define-public crate-musli-storage-0.0.48 (c (n "musli-storage") (v "0.0.48") (d (list (d (n "musli") (r "^0.0.48") (k 0)) (d (n "musli-common") (r "^0.0.48") (k 0)))) (h "1jsr95cszb3r82fsgqqc6gnhmnxayld6aj4q4y7zayjwr45jw4mq") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.66")))

(define-public crate-musli-storage-0.0.49 (c (n "musli-storage") (v "0.0.49") (d (list (d (n "musli") (r "^0.0.49") (k 0)) (d (n "musli-common") (r "^0.0.49") (k 0)))) (h "0q3yxqxs932gjx9rhnrmi7kir6gn8gcydbj1r9q4avyhbk6h8j9c") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.66")))

(define-public crate-musli-storage-0.0.50 (c (n "musli-storage") (v "0.0.50") (d (list (d (n "musli") (r "^0.0.50") (k 0)) (d (n "musli-common") (r "^0.0.50") (k 0)))) (h "1kv6n1z4xqmdmfnaw4bk6zvzdnsaa0shc5slmx7xl09pjcjav2y3") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.51 (c (n "musli-storage") (v "0.0.51") (d (list (d (n "musli") (r "^0.0.51") (k 0)) (d (n "musli-common") (r "^0.0.51") (k 0)))) (h "01kgd04hcd1h01x3z5hnrdpdlk4nqgl9gwpkp5cq167bd6rm98db") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.52 (c (n "musli-storage") (v "0.0.52") (d (list (d (n "musli") (r "^0.0.52") (k 0)) (d (n "musli-common") (r "^0.0.52") (k 0)))) (h "1x35f430qq5yg7cj6igamqwbszrdfrx1wpb50076fnf978vlrd1w") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.53 (c (n "musli-storage") (v "0.0.53") (d (list (d (n "musli") (r "^0.0.53") (k 0)) (d (n "musli-common") (r "^0.0.53") (k 0)))) (h "1g870vpaml1mq0cgma66iraqay5yhvz6i016cvcxld630ffar736") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.54 (c (n "musli-storage") (v "0.0.54") (d (list (d (n "musli") (r "^0.0.54") (k 0)) (d (n "musli-common") (r "^0.0.54") (k 0)))) (h "1lxd4am64hsyr45iw1lb4fq7wzcrx4bdpy37g3jgsq8ql59qbzl0") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.55 (c (n "musli-storage") (v "0.0.55") (d (list (d (n "musli") (r "^0.0.55") (k 0)) (d (n "musli-common") (r "^0.0.55") (k 0)))) (h "0pzz46d92cj1kb34j86cra88mcldnvs8652g1rvpyaycxc12bxj6") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.56 (c (n "musli-storage") (v "0.0.56") (d (list (d (n "musli") (r "^0.0.56") (k 0)) (d (n "musli-common") (r "^0.0.56") (k 0)))) (h "15migf1k8nrvf6yfkw7mrlk9w2q2g1s5kw8sfi56h4kgk2x4xil7") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.57 (c (n "musli-storage") (v "0.0.57") (d (list (d (n "musli") (r "^0.0.57") (k 0)) (d (n "musli-common") (r "^0.0.57") (k 0)))) (h "13gbsn7qkcfv4mpfkzaq6bvqc7gsyh920gkn64xfwdrn09xsahq5") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.58 (c (n "musli-storage") (v "0.0.58") (d (list (d (n "musli") (r "^0.0.58") (k 0)) (d (n "musli-common") (r "^0.0.58") (k 0)))) (h "1j8ipj4dg76whkxmiva4qn2h9cw4gkh0pp5c3s01rbz7a79x6l6j") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.59 (c (n "musli-storage") (v "0.0.59") (d (list (d (n "musli") (r "^0.0.59") (k 0)) (d (n "musli-common") (r "^0.0.59") (k 0)))) (h "1vrm8xx3l9ix2r55kj132a7r52yhmsm0rhv5aij74n48fd8b0b7b") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.60 (c (n "musli-storage") (v "0.0.60") (d (list (d (n "musli") (r "^0.0.60") (k 0)) (d (n "musli-common") (r "^0.0.60") (k 0)))) (h "1a1m6dqlj9nqn8d6mq5s39mksx9jz429az6lf5klb1jk47f1y5q5") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.61 (c (n "musli-storage") (v "0.0.61") (d (list (d (n "musli") (r "^0.0.61") (k 0)) (d (n "musli-common") (r "^0.0.61") (k 0)))) (h "0dc7drn2ayni99iqczkr8il86yrld989zzd9mmx3lly04jl5v3gn") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.62 (c (n "musli-storage") (v "0.0.62") (d (list (d (n "musli") (r "^0.0.62") (k 0)) (d (n "musli-common") (r "^0.0.62") (k 0)))) (h "18vkw8ssv5fkvjwwiaxm4i3dhmn8jf2ql12mj1xzvykbyav3igj8") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.63 (c (n "musli-storage") (v "0.0.63") (d (list (d (n "musli") (r "^0.0.63") (k 0)) (d (n "musli-common") (r "^0.0.63") (k 0)))) (h "1dr0ysz7lbkp6x8n4f66zbnhvpkha46az1fcgqrn2jwh9pv2rl30") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.64 (c (n "musli-storage") (v "0.0.64") (d (list (d (n "musli") (r "^0.0.64") (k 0)) (d (n "musli-common") (r "^0.0.64") (k 0)))) (h "0rvbajcglnwbqp7a5b94s15cbbyzgs18akhy8k89ainqwway9mcd") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.65 (c (n "musli-storage") (v "0.0.65") (d (list (d (n "musli") (r "^0.0.65") (k 0)) (d (n "musli-common") (r "^0.0.65") (k 0)))) (h "11d0wfa5i76zcpaifpkqnj38qia5b35rnph5faazf1c8xhbh42v9") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.66 (c (n "musli-storage") (v "0.0.66") (d (list (d (n "musli") (r "^0.0.66") (k 0)) (d (n "musli-common") (r "^0.0.66") (k 0)))) (h "06jzjl64mmi4lybp4bzyfi658w6l9h5gzg21g4bca8qj9kllqc3m") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.67 (c (n "musli-storage") (v "0.0.67") (d (list (d (n "musli") (r "^0.0.67") (k 0)) (d (n "musli-common") (r "^0.0.67") (k 0)))) (h "1c9lw2zjkk5v9rjmqdkavgxjsgpdcrpw26chq1g9b0xs40sadyvj") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.68 (c (n "musli-storage") (v "0.0.68") (d (list (d (n "musli") (r "^0.0.68") (k 0)) (d (n "musli-common") (r "^0.0.68") (k 0)))) (h "15r2bsxkqvkly1xz10k7fwk31ncw9knmlhbrs6yxc90r4yj8v9x6") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.69 (c (n "musli-storage") (v "0.0.69") (d (list (d (n "musli") (r "^0.0.69") (k 0)) (d (n "musli-common") (r "^0.0.69") (k 0)))) (h "0n8zi0cg3x7y5pd98dfim0jkmaq8hvk2bqrld41qvrp2hhw4gkci") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.70 (c (n "musli-storage") (v "0.0.70") (d (list (d (n "musli") (r "^0.0.70") (k 0)) (d (n "musli-common") (r "^0.0.70") (k 0)))) (h "18h7jzxwc998wp0fzxjla9d5in58bfc4rydzzzcsll7624iis2cr") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.71 (c (n "musli-storage") (v "0.0.71") (d (list (d (n "musli") (r "^0.0.71") (k 0)) (d (n "musli-common") (r "^0.0.71") (k 0)))) (h "02xp6qhqs85x5jajwjzc21q3ibyqcxg13ybk41qlamp2mbcx59nq") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.72 (c (n "musli-storage") (v "0.0.72") (d (list (d (n "musli") (r "^0.0.72") (k 0)) (d (n "musli-common") (r "^0.0.72") (k 0)))) (h "0rffyjv1dnynra84s40vw78x7k60p9xjf19m3h0bfrx0bj5y28ic") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.73 (c (n "musli-storage") (v "0.0.73") (d (list (d (n "musli") (r "^0.0.73") (k 0)) (d (n "musli-common") (r "^0.0.73") (k 0)))) (h "0clv9m261f6gjphpf0blp5sdhxn0f0rw71fan8c39y43mx2mhsk6") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.74 (c (n "musli-storage") (v "0.0.74") (d (list (d (n "musli") (r "^0.0.74") (k 0)) (d (n "musli-common") (r "^0.0.74") (k 0)))) (h "171bb9v4i4yhqcx17139qdgz1qghl784zb0ns1jm09a3mmia8s28") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.75 (c (n "musli-storage") (v "0.0.75") (d (list (d (n "musli") (r "^0.0.75") (k 0)) (d (n "musli-common") (r "^0.0.75") (k 0)))) (h "03qn3g95snmp35a94rjmykmk990q639kw63wizs1279xwhrk963n") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.76 (c (n "musli-storage") (v "0.0.76") (d (list (d (n "musli") (r "^0.0.76") (k 0)) (d (n "musli-common") (r "^0.0.76") (k 0)))) (h "1l3ziwbp16vxsjmkw56h34gnf0v3v709k1962k71bg8qsvhxgnql") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.77 (c (n "musli-storage") (v "0.0.77") (d (list (d (n "musli") (r "^0.0.77") (k 0)) (d (n "musli-common") (r "^0.0.77") (k 0)))) (h "0iv96izp466h41q7nssv4wlvb36b3b7vwdy6xscrbg95lcgqp3ww") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.78 (c (n "musli-storage") (v "0.0.78") (d (list (d (n "musli") (r "^0.0.78") (k 0)) (d (n "musli-common") (r "^0.0.78") (k 0)))) (h "0fnlx6cnsn1g5hx9yl01swm0xknn6ik9m38dvjqa1vyrzlq9pr90") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.79 (c (n "musli-storage") (v "0.0.79") (d (list (d (n "musli") (r "^0.0.79") (k 0)) (d (n "musli-common") (r "^0.0.79") (k 0)))) (h "1ism9hjx5rsj32xa7rf4h0jndc716qshvm7lfy48dnrfj0xjl3iq") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.80 (c (n "musli-storage") (v "0.0.80") (d (list (d (n "musli") (r "^0.0.80") (k 0)) (d (n "musli-common") (r "^0.0.80") (k 0)))) (h "1yi4hfkc4va75myj734vmnni2rmgxlgh5idf3gzsfwcadjk35szv") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.81 (c (n "musli-storage") (v "0.0.81") (d (list (d (n "musli") (r "^0.0.81") (k 0)) (d (n "musli-common") (r "^0.0.81") (k 0)))) (h "1pcg9rzb4alg96l83hllj1xglmxrv1n0a5s4pr4qlda56nfdmid0") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.82 (c (n "musli-storage") (v "0.0.82") (d (list (d (n "musli") (r "^0.0.82") (k 0)) (d (n "musli-common") (r "^0.0.82") (k 0)))) (h "02ra57jhp7djghv0xr1ygs2rmvk47mw75dzjym1l8n5iwfzs8vzl") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.83 (c (n "musli-storage") (v "0.0.83") (d (list (d (n "musli") (r "^0.0.83") (k 0)) (d (n "musli-common") (r "^0.0.83") (k 0)))) (h "1kb73ygs3qlvjhajr2m6wag0cxqfflwq56clsxxm30p697c71p0f") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.84 (c (n "musli-storage") (v "0.0.84") (d (list (d (n "musli") (r "^0.0.84") (k 0)) (d (n "musli-common") (r "^0.0.84") (k 0)))) (h "17r6bp4dqc29m1gh52d2jlcdnw3pj5y57aqg740353mf9gx8b234") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.85 (c (n "musli-storage") (v "0.0.85") (d (list (d (n "musli") (r "^0.0.85") (k 0)) (d (n "musli-common") (r "^0.0.85") (k 0)))) (h "1zxqs4v0ya4v1s7fdqlchlq5n3hf7f6w7i7i7aakbvqsn0yjib66") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.86 (c (n "musli-storage") (v "0.0.86") (d (list (d (n "musli") (r "^0.0.86") (k 0)) (d (n "musli-common") (r "^0.0.86") (k 0)))) (h "0zlbfhw0dkz6gxqb42b97cdj4kis89z7swqagk7svnwzpln96mqr") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.87 (c (n "musli-storage") (v "0.0.87") (d (list (d (n "musli") (r "^0.0.87") (k 0)) (d (n "musli-common") (r "^0.0.87") (k 0)))) (h "1bws6d461sj6329jmvdvamqx4lisb241j3xjs6nkcfvwghlak0rf") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.88 (c (n "musli-storage") (v "0.0.88") (d (list (d (n "musli") (r "^0.0.88") (k 0)) (d (n "musli-common") (r "^0.0.88") (k 0)))) (h "0kcanaphh9q8qxjywkjrw2nf5valmsdiykw5czk7pvb0q3mzv4l5") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.89 (c (n "musli-storage") (v "0.0.89") (d (list (d (n "musli") (r "^0.0.89") (k 0)) (d (n "musli-common") (r "^0.0.89") (k 0)))) (h "1gyijjkkfqrqqv5jf1brnjmyplyfql2lys4djwsbmisdifvs5l86") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.90 (c (n "musli-storage") (v "0.0.90") (d (list (d (n "musli") (r "^0.0.90") (k 0)) (d (n "musli-common") (r "^0.0.90") (k 0)))) (h "06lvjxi3vf00w7vxsfkmljy1klizmibg5acds3k0009kma8s6zfp") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.91 (c (n "musli-storage") (v "0.0.91") (d (list (d (n "musli") (r "^0.0.91") (k 0)) (d (n "musli-common") (r "^0.0.91") (k 0)))) (h "0w93gaqn2yd43pg11mx2b52nv2rbfk5pscnqx8xyimwa0is0a0vq") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.92 (c (n "musli-storage") (v "0.0.92") (d (list (d (n "musli") (r "^0.0.92") (k 0)) (d (n "musli-common") (r "^0.0.92") (k 0)))) (h "11wili150jq0v6viyyzarscaqlmdp8qwrfgb9ar3wgc7l365gc09") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.93 (c (n "musli-storage") (v "0.0.93") (d (list (d (n "musli") (r "^0.0.93") (k 0)) (d (n "musli-common") (r "^0.0.93") (k 0)))) (h "1qg44p2v37870h8vhv175k20xhkgjp2wipnajfxsnin3qb39p6sk") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.94 (c (n "musli-storage") (v "0.0.94") (d (list (d (n "musli") (r "^0.0.94") (k 0)) (d (n "musli-common") (r "^0.0.94") (k 0)))) (h "0rbgvjl11zwdzhqv680c2afp00cvj1m2sspy863k80ajkj40giy5") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.73")))

(define-public crate-musli-storage-0.0.95 (c (n "musli-storage") (v "0.0.95") (d (list (d (n "musli") (r "^0.0.95") (k 0)) (d (n "musli-common") (r "^0.0.95") (k 0)))) (h "0n5ln98jszhab5xzs8ipraawzcgqydfqs4s9y8pc08l4fd452q0n") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.67")))

(define-public crate-musli-storage-0.0.96 (c (n "musli-storage") (v "0.0.96") (d (list (d (n "musli") (r "^0.0.96") (k 0)) (d (n "musli-common") (r "^0.0.96") (k 0)))) (h "0b45dix4y27gzyg1pirhinvvs54jg1zfj2gbgpwkxqnjajdir24w") (f (quote (("test") ("std" "musli/std" "musli-common/std" "alloc") ("simdutf8" "musli-common/simdutf8") ("default" "std" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.67")))

(define-public crate-musli-storage-0.0.97 (c (n "musli-storage") (v "0.0.97") (d (list (d (n "musli") (r "^0.0.97") (k 0)) (d (n "musli-common") (r "^0.0.97") (k 0)))) (h "1gqy69vy481kyq2izwiwwjvjhipwrhf0hlmzl6l64qmqr7vl560p") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.98 (c (n "musli-storage") (v "0.0.98") (d (list (d (n "musli") (r "^0.0.98") (k 0)) (d (n "musli-common") (r "^0.0.98") (k 0)))) (h "0k830igjbn5bvxgzviiipxhm7ym2wxhfk0lscfqmz444i5w4cgcz") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.99 (c (n "musli-storage") (v "0.0.99") (d (list (d (n "musli") (r "^0.0.99") (k 0)) (d (n "musli-common") (r "^0.0.99") (k 0)))) (h "1fm288rdrfw5d3dxzf1qb4zxlgflxia9i1dpzk6gr8zvagggz758") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.100 (c (n "musli-storage") (v "0.0.100") (d (list (d (n "musli") (r "^0.0.100") (k 0)) (d (n "musli-common") (r "^0.0.100") (k 0)))) (h "0pylzw1csx6qh8h2qj1crfrgd8478s9lm4c4j6gaisvnz9sfaq01") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.101 (c (n "musli-storage") (v "0.0.101") (d (list (d (n "musli") (r "^0.0.101") (k 0)) (d (n "musli-common") (r "^0.0.101") (k 0)))) (h "1zppsmkw43xdj3wb3zxq999wpmgwb8s0dsfba7hfh1v9kqgvz02p") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.102 (c (n "musli-storage") (v "0.0.102") (d (list (d (n "musli") (r "^0.0.102") (k 0)) (d (n "musli-common") (r "^0.0.102") (k 0)))) (h "0xv87bklhrcdarh88d4fp4kc8g0rxsg6idn33whjzx22h4cxxj2s") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.103 (c (n "musli-storage") (v "0.0.103") (d (list (d (n "musli") (r "^0.0.103") (k 0)) (d (n "musli-common") (r "^0.0.103") (k 0)))) (h "1nmg21a5my1267a9s7aycwa5rx66yfvj067z98w4zyfzn650p2pq") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.104 (c (n "musli-storage") (v "0.0.104") (d (list (d (n "musli") (r "^0.0.104") (k 0)) (d (n "musli-common") (r "^0.0.104") (k 0)))) (h "1972q99vzvpf8irl1zri0m2cl0g7zns3hyvfvj9k3byg19g47rbi") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.105 (c (n "musli-storage") (v "0.0.105") (d (list (d (n "musli") (r "^0.0.105") (k 0)) (d (n "musli-common") (r "^0.0.105") (k 0)))) (h "1qbqynaj148ffy0b8k1i6c8pzysrsvmq7gxgc32gm9mk9qgqxpp7") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.106 (c (n "musli-storage") (v "0.0.106") (d (list (d (n "musli") (r "^0.0.106") (k 0)) (d (n "musli-common") (r "^0.0.106") (k 0)))) (h "0kxllrp3ar8nav5xv7kjjcml607nq82h1cplvj4yx03krm4fy1id") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.107 (c (n "musli-storage") (v "0.0.107") (d (list (d (n "musli") (r "^0.0.107") (k 0)) (d (n "musli-common") (r "^0.0.107") (k 0)))) (h "0cd0hqc4dbsvm0z6wqj77672v8ggvj89flrzsr2in83q479flkix") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.108 (c (n "musli-storage") (v "0.0.108") (d (list (d (n "musli") (r "^0.0.108") (k 0)) (d (n "musli-common") (r "^0.0.108") (k 0)))) (h "11jy8k8g9s657kmmpsd943ibra2g79lm34jfk1fjv02sf3sf9x4m") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.109 (c (n "musli-storage") (v "0.0.109") (d (list (d (n "musli") (r "^0.0.109") (k 0)) (d (n "musli-common") (r "^0.0.109") (k 0)))) (h "1kvjyr7a3svbais2n3vc55jz7inrih9dwq16ch5pc9mhd0xrch89") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.110 (c (n "musli-storage") (v "0.0.110") (d (list (d (n "musli") (r "^0.0.110") (k 0)) (d (n "musli-common") (r "^0.0.110") (k 0)))) (h "017yqxxsmjdcj3sza2msr87sy168a73889cbv1gc3xndcylcx1bp") (f (quote (("test") ("std" "musli/std" "musli-common/std") ("simdutf8" "musli-common/simdutf8") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-common/alloc")))) (r "1.76")))

(define-public crate-musli-storage-0.0.111 (c (n "musli-storage") (v "0.0.111") (d (list (d (n "musli") (r "^0.0.111") (k 0)) (d (n "musli-utils") (r "^0.0.111") (k 0)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1m051nphb98h1z26ssg951d004p7975wbx4mlybdnlxsbrjhj4cp") (f (quote (("test") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-utils/alloc")))) (s 2) (e (quote (("std" "musli/std" "musli-utils/std" "simdutf8?/std") ("simdutf8" "dep:simdutf8")))) (r "1.76")))

(define-public crate-musli-storage-0.0.112 (c (n "musli-storage") (v "0.0.112") (d (list (d (n "musli") (r "^0.0.112") (k 0)) (d (n "musli-utils") (r "^0.0.112") (k 0)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1h42zzadfiv3zy2xmnnhak2wnzzv0bxw0wnw89bp5fxbrzwq901q") (f (quote (("test") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-utils/alloc")))) (s 2) (e (quote (("std" "musli/std" "musli-utils/std" "simdutf8?/std") ("simdutf8" "dep:simdutf8")))) (r "1.76")))

(define-public crate-musli-storage-0.0.113 (c (n "musli-storage") (v "0.0.113") (d (list (d (n "musli") (r "^0.0.113") (k 0)) (d (n "musli-utils") (r "^0.0.113") (k 0)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1nwpr2qha48622mvw9laz5wj1d5h64mp9bijc3a3i2p84mi77w1y") (f (quote (("test") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-utils/alloc")))) (s 2) (e (quote (("std" "musli/std" "musli-utils/std" "simdutf8?/std") ("simdutf8" "dep:simdutf8")))) (r "1.76")))

(define-public crate-musli-storage-0.0.114 (c (n "musli-storage") (v "0.0.114") (d (list (d (n "musli") (r "^0.0.114") (k 0)) (d (n "musli-utils") (r "^0.0.114") (k 0)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0pq8m6p1mrh9hm1xijxmhy2i3qbyv9zw3w7zx4q6v7ql9s6fvkwx") (f (quote (("test") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-utils/alloc")))) (s 2) (e (quote (("std" "musli/std" "musli-utils/std" "simdutf8?/std") ("simdutf8" "dep:simdutf8")))) (r "1.76")))

(define-public crate-musli-storage-0.0.115 (c (n "musli-storage") (v "0.0.115") (d (list (d (n "musli") (r "^0.0.115") (k 0)) (d (n "musli-utils") (r "^0.0.115") (k 0)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1b9g20gg1lmdayag5a4mc8y0x8a8khn8yn9xc0vdxmhi0q6p4a1n") (f (quote (("test") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-utils/alloc")))) (s 2) (e (quote (("std" "musli/std" "musli-utils/std" "simdutf8?/std") ("simdutf8" "dep:simdutf8")))) (r "1.76")))

(define-public crate-musli-storage-0.0.116 (c (n "musli-storage") (v "0.0.116") (d (list (d (n "musli") (r "^0.0.116") (k 0)) (d (n "musli-utils") (r "^0.0.116") (k 0)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "12c2jax3ayabfs36wi0c44fnmynpif6sj0ilpzqpjpv0dsng3n3k") (f (quote (("test") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-utils/alloc")))) (s 2) (e (quote (("std" "musli/std" "musli-utils/std" "simdutf8?/std") ("simdutf8" "dep:simdutf8")))) (r "1.76")))

(define-public crate-musli-storage-0.0.117 (c (n "musli-storage") (v "0.0.117") (d (list (d (n "musli") (r "^0.0.117") (k 0)) (d (n "musli-utils") (r "^0.0.117") (k 0)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0ppphxsr1vdgm4j8pc2rv964imvqbhw2j895izbsblihpv11375f") (f (quote (("test") ("default" "std" "alloc" "simdutf8") ("alloc" "musli/alloc" "musli-utils/alloc")))) (s 2) (e (quote (("std" "musli/std" "musli-utils/std" "simdutf8?/std") ("simdutf8" "dep:simdutf8")))) (r "1.76")))

