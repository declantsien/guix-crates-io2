(define-module (crates-io mu sl musli-macros) #:use-module (crates-io))

(define-public crate-musli-macros-0.0.1 (c (n "musli-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "16l7i53gd4mk50njcx8gi2xp49f2nzaw6l9xhm5m41j05c75laap")))

(define-public crate-musli-macros-0.0.2 (c (n "musli-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "139ribc1mni0n9n5nz5xvrkcycqlzfakqsqvj1l3y104hkjr396k")))

(define-public crate-musli-macros-0.0.3 (c (n "musli-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1gkq2wbbwmx2f7cfwd38rrfxn0l8sc92w5z5cznz99x5d3hj98k5")))

(define-public crate-musli-macros-0.0.4 (c (n "musli-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "0bhv1npfdlrg944a1wb4ij4a6l4d1zx2kfhw841xanj0jazsh0a6")))

(define-public crate-musli-macros-0.0.5 (c (n "musli-macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "0lalhp4njg0wnrhvl2n5cmizz6c63nxyk5svp0j7c9l0pd7fv1dz")))

(define-public crate-musli-macros-0.0.6 (c (n "musli-macros") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1qirwj6pjx67pm938v78mr2q5h83mynbfs71dizhldrlayahv4z3")))

(define-public crate-musli-macros-0.0.7 (c (n "musli-macros") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1wmbkcv98zsarizbzh8as634grqga83kgfbbh20xw3h5fqfrsbvz")))

(define-public crate-musli-macros-0.0.8 (c (n "musli-macros") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "0vvl2m37yznajjqm3prwqlk74b5lc3fblnjdma8y4ihgq5lwkwvx")))

(define-public crate-musli-macros-0.0.9 (c (n "musli-macros") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "181ilw9aaxcnvvvq55m83xwvsq2phxjxx3ipsjrcwnqzkri3cwkf")))

(define-public crate-musli-macros-0.0.10 (c (n "musli-macros") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "13r3sdypm78m2kcm3sp63ayaladk9pp6yr0dy55pjk2rsxy3y8wb")))

(define-public crate-musli-macros-0.0.11 (c (n "musli-macros") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1pa1lb99vpn08h75g6s7464fj66ygswvi9ilwllj6hflfslx81q4")))

(define-public crate-musli-macros-0.0.12 (c (n "musli-macros") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "0vz6i6fxfqqjwqs43gmiwsmn1342j742v0g9xyy8fzkigvnn8vxs")))

(define-public crate-musli-macros-0.0.14 (c (n "musli-macros") (v "0.0.14") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "0wq0ab9bl4vb6nhdi125lrqmdy11vp43kqg357mi1mp1p3gv43s0")))

(define-public crate-musli-macros-0.0.15 (c (n "musli-macros") (v "0.0.15") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1fc3kc1yn59bdfa3b8hwgd1zc1dqf6sph8jn4x0qbbp2d0km5sxr")))

(define-public crate-musli-macros-0.0.16 (c (n "musli-macros") (v "0.0.16") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1wpj4mqfqvis9r9i22gv86igqc0ls07j1v9klj2dl1qff95yh1d4")))

(define-public crate-musli-macros-0.0.17 (c (n "musli-macros") (v "0.0.17") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "0nj1ikh2x8sv6k4z644wwjf4q707anr4vmxvpk1x8b7b9v0fvbgx")))

(define-public crate-musli-macros-0.0.18 (c (n "musli-macros") (v "0.0.18") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1hdrdzcr8cgkxnrslb8hwabghlrdzc7cj72y26plfk73c141ajl5")))

(define-public crate-musli-macros-0.0.19 (c (n "musli-macros") (v "0.0.19") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1dpfh24q5jpivzhi5b3i4gsa67100xwzkb2b7b1k95w8q2bw4p81")))

(define-public crate-musli-macros-0.0.20 (c (n "musli-macros") (v "0.0.20") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1s085kr2qmd09qb2493hf8iq9ikbwplzh3w054vj3g9cfymdxpqs")))

(define-public crate-musli-macros-0.0.21 (c (n "musli-macros") (v "0.0.21") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "0yz469wfblwp66gxqqj93m1ra2xird3c0lslyc5pswsk7qqm7ya3")))

(define-public crate-musli-macros-0.0.22 (c (n "musli-macros") (v "0.0.22") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "0wx20dbaw89vjmmcdikgjg4rxyrzsasnhcfmmbg9sgj0z47i2lhb")))

(define-public crate-musli-macros-0.0.23 (c (n "musli-macros") (v "0.0.23") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "06ayfirnhw19djmln7c1s0fmpg263vyf9jsfxqb6kpdya342sxc8")))

(define-public crate-musli-macros-0.0.24 (c (n "musli-macros") (v "0.0.24") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "120i00sx3wzqwy9cxdxd246chbxc8ixjld8k17vdb0xdkgp21bh6")))

(define-public crate-musli-macros-0.0.25 (c (n "musli-macros") (v "0.0.25") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0slipnflqppnnhqpqb3wmzbcb9rwjajkybqhc87rd3049lbkmfac")))

(define-public crate-musli-macros-0.0.26 (c (n "musli-macros") (v "0.0.26") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03ky51723l354w3b484qzl2wpn9cwyb1pnv4jdif4gxdwxjp3ah4")))

(define-public crate-musli-macros-0.0.27 (c (n "musli-macros") (v "0.0.27") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0513z1lnib50vkpk5k57jqq5zmn1i28rdbihpqhzqvj6rjzrdj1j")))

(define-public crate-musli-macros-0.0.28 (c (n "musli-macros") (v "0.0.28") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1b4lyj1197r6xzwbq861dirndw551vygw81vixjsqkc83qi6lgck")))

(define-public crate-musli-macros-0.0.29 (c (n "musli-macros") (v "0.0.29") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wi8j81l8wdx2lm9d3llzf2v0x61y8jkh0xmb1xshqh8pdpz9ksh")))

(define-public crate-musli-macros-0.0.30 (c (n "musli-macros") (v "0.0.30") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "13p9pm23lg3i8r81dr7c880nbgzzwziynkh6b1106qqqygjzh0f6")))

(define-public crate-musli-macros-0.0.31 (c (n "musli-macros") (v "0.0.31") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03mw0v36zw58jy0g5nw43n4ryp1v13pikcwn0gdbgmjzi2ffybax")))

(define-public crate-musli-macros-0.0.32 (c (n "musli-macros") (v "0.0.32") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0kfr10im7zkb8amjhbg22rjriz4vpg000g9rqf7z0iwxriz993gj")))

(define-public crate-musli-macros-0.0.33 (c (n "musli-macros") (v "0.0.33") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0mq02zxwqlq8yp0185ssxwmrxqfwh83qa0z4x6qyh1li4jmk4brg")))

(define-public crate-musli-macros-0.0.34 (c (n "musli-macros") (v "0.0.34") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1dfj2c7y00symdnyb1abz17ixrz5im2bjppsm7l91l0hx3g1059b")))

(define-public crate-musli-macros-0.0.35 (c (n "musli-macros") (v "0.0.35") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1vgh49r7bzdgc29hc6lqw4vzkz9kbj0f0vc55lw5wqjdj7vhdlmy")))

(define-public crate-musli-macros-0.0.36 (c (n "musli-macros") (v "0.0.36") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dapl1d13ywn6d42l805czz8sa7aaybqcviw28sqgbhjbx3lzh54")))

(define-public crate-musli-macros-0.0.37 (c (n "musli-macros") (v "0.0.37") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1jcqqjylcyvmaxsfc8pwqd96zl0mrg9q4fhk0ic838irsn76qxj9")))

(define-public crate-musli-macros-0.0.38 (c (n "musli-macros") (v "0.0.38") (d (list (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "051c19x4d9w2g19xjn28qryr9cpqma5cg4z4iqgvmwq5fybyfhwz") (r "1.65")))

(define-public crate-musli-macros-0.0.39 (c (n "musli-macros") (v "0.0.39") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hr3nc6kyj9qn3fjvjx9ij5r338khlkvc7g3cs98vsqgyyh615ai") (r "1.65")))

(define-public crate-musli-macros-0.0.40 (c (n "musli-macros") (v "0.0.40") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b2hyi979w61s6ms4dh2fs1lcqpwhncsnc724nzihhz0k09p4v0p") (r "1.65")))

(define-public crate-musli-macros-0.0.41 (c (n "musli-macros") (v "0.0.41") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z24472qdvswgf0lb7s726wnx5bbc3j0ypr8saj5l7j2dcgwj54g") (r "1.65")))

(define-public crate-musli-macros-0.0.42 (c (n "musli-macros") (v "0.0.42") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "095c5xnbybr5b58wiy10r0v0rhy180k6m2jjld7vq897mkjb06lz") (r "1.65")))

(define-public crate-musli-macros-0.0.43 (c (n "musli-macros") (v "0.0.43") (d (list (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dvqzm95c5r0x40g7h3ckczz40813x553zbrbw156hnmp7wp0fkz") (r "1.65")))

(define-public crate-musli-macros-0.0.44 (c (n "musli-macros") (v "0.0.44") (d (list (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pb1xwk6r5727z4s4g2zm7y015ldh8gdyw4vkwzvwyimj82kckqr") (r "1.65")))

(define-public crate-musli-macros-0.0.45 (c (n "musli-macros") (v "0.0.45") (d (list (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ad6s1wp37zz9qx2vd9hijdc5br4xc427yqx9rjg3qdq09dg9hh9") (r "1.65")))

(define-public crate-musli-macros-0.0.46 (c (n "musli-macros") (v "0.0.46") (d (list (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1f5mqpigfqgc3hq51q6l2qg1grm3z1whp2s4j75sy4834v5br9w5") (r "1.65")))

(define-public crate-musli-macros-0.0.47 (c (n "musli-macros") (v "0.0.47") (d (list (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17rivfw6k22bbvl2pjjj7ws8hhq4f9hinik6zpz14hjdpsr3pydx") (r "1.66")))

(define-public crate-musli-macros-0.0.48 (c (n "musli-macros") (v "0.0.48") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "136w7wxp2f2af4sinpprn9gdxqr8wk5ps33zzfzi9qykkyy862fz") (r "1.66")))

(define-public crate-musli-macros-0.0.49 (c (n "musli-macros") (v "0.0.49") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05zb20n05381x0rcmg90b887zg02aa1pj34s8najaq87z2mc4gcg") (r "1.66")))

(define-public crate-musli-macros-0.0.50 (c (n "musli-macros") (v "0.0.50") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0607kxv2irn5g0ahbryhsr0zybsndjrk7g0km0kjg2mvj0cds3sl") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.51 (c (n "musli-macros") (v "0.0.51") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0alfiz7k815sspkzijqzkhc6qycqammbndzmnsamgdh81r7rkqfz") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.52 (c (n "musli-macros") (v "0.0.52") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04czqwqa93rd8kd4d9zwbjr7i0lv3h79da973r9ziq8irzc8hc63") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.53 (c (n "musli-macros") (v "0.0.53") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gbjnms43vlpvvn9mn8yxp39irm90ari5mdw29j33lxhklxs2w9x") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.54 (c (n "musli-macros") (v "0.0.54") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04v0gg6rk7s3irvr8gnwad5nfkrg98dpss03aag0q3gkzj45z34d") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.55 (c (n "musli-macros") (v "0.0.55") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vp3xg3wxqlydhb9bk48rqi83m0wv3lm96lx8slpw5fazxs5v4rh") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.56 (c (n "musli-macros") (v "0.0.56") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w91nmqjam5zn5f52308frgf0l3qmjv9vbbwy9kjhm81csjapcp0") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.57 (c (n "musli-macros") (v "0.0.57") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "022av7lav3j81fadyclp4hhb4k8qm6xrqf3s3nfqw261y7sl8nqx") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.58 (c (n "musli-macros") (v "0.0.58") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0906rnipqz6sdf7lbqn7igfwjjh192dz30qsy6373kmyrr3fjpp8") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.59 (c (n "musli-macros") (v "0.0.59") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p35r4zb7dg3s4n50938qh345p95d2r58a3qzy8axknf121vln8v") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.60 (c (n "musli-macros") (v "0.0.60") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11d1d2vaimx81wmfg7lz4khspz86x6yhijmi41r4vqsw0r6vy32y") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.61 (c (n "musli-macros") (v "0.0.61") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hiaq4lwvs5l9abs8vcg19115wz64dc9z95difgjv04q35prscyl") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.62 (c (n "musli-macros") (v "0.0.62") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pgfj1xv6fnivafdsy7hklk340kviq4qx7mg46fn8qmrhxmvzldj") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.63 (c (n "musli-macros") (v "0.0.63") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l6ib2y86y7czig04qbps9g5ywwaww569zj4asxh21sxj93dlkxj") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.64 (c (n "musli-macros") (v "0.0.64") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fmscx85lm2h34w4bqnvvn60y44m42vwmymkwh04hd66wlzaym53") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.65 (c (n "musli-macros") (v "0.0.65") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17cspc598bd6j8s206585b78x8byck0680jz36p270am1pp9653x") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.66 (c (n "musli-macros") (v "0.0.66") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hav22r65f68jix6gdl26bxppa2n4crpyi1si30kq5py2a4qals5") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.67 (c (n "musli-macros") (v "0.0.67") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19i99cndsm6jkk1j8x3bpzs1hmc2y2sww0mlnwdjjffvqx372mmy") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.68 (c (n "musli-macros") (v "0.0.68") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w5dc1s6cpxzn3rmz1528sgbiz45kkvdkbvn59bbag96mjzv53gn") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.69 (c (n "musli-macros") (v "0.0.69") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vbxrwflv5ddvl49jli27p5m8xwk146zn060339p42xjy81q7lcf") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.70 (c (n "musli-macros") (v "0.0.70") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1q8q873qspxmyq8sv20cr4pam4rkrj16dh5cz2hcvg20lwkf42n4") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.71 (c (n "musli-macros") (v "0.0.71") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yvpg69fgnrfwl2f5dyywdnb49yasai2v471nkadmq32904bkibq") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.72 (c (n "musli-macros") (v "0.0.72") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "163l3x1ql1bprxr15g10vjff56zllaym8mgkl8ykgd9n3crk5r3j") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.73 (c (n "musli-macros") (v "0.0.73") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gghqay1q7655x7yjs8a7kc7vabp2gx3ykz4s86vms76sphngf1l") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.74 (c (n "musli-macros") (v "0.0.74") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fparp5vliyzdn995a05qqq2zi0xsijmpi2794f4d8bms1z8cgga") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.75 (c (n "musli-macros") (v "0.0.75") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bg951kdnixbiv4xidrzg8fp8j5n6slwamplih8fmjzq4blyhika") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.76 (c (n "musli-macros") (v "0.0.76") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gjknw98kz382md5acnqi7j11pk78vkd83fyr6i9w70v7961dq0a") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.77 (c (n "musli-macros") (v "0.0.77") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11gaqdryqqz2nw657a8r31whg1v77rvkvimhzf1x6wf1b3irv21a") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.78 (c (n "musli-macros") (v "0.0.78") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vzz4p1ssxmjkgmyji2mss053ii4dvhnkv5lr2z6yfbp6p6ngd66") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.79 (c (n "musli-macros") (v "0.0.79") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bqia4kh0mrwpdpwll0dn1k4g47jb37y970zblhviphqhrjnqp5k") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.80 (c (n "musli-macros") (v "0.0.80") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k52frzx00xyf1n9mz94y8v1g51cn0cysxjrf4kclpbbak1mp590") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.81 (c (n "musli-macros") (v "0.0.81") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19wn64rpwdzpiwlfn7x3zpqadbimak46br8r1j1md301larh8cd3") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.82 (c (n "musli-macros") (v "0.0.82") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lq33vcliqlvhs3l06hlv8c0qyv0yybf7nq1zb5y1n47q7l8ayn4") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.83 (c (n "musli-macros") (v "0.0.83") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bcch3vivbpyxahpaw2sl6vg1rjyag9n286aa1r6j1cg4xjafc0l") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.84 (c (n "musli-macros") (v "0.0.84") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13nm2am49qkqj2kih3ylb2x198hffywfw98y9dl0y21f5z8jgw7f") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.85 (c (n "musli-macros") (v "0.0.85") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zhg05lsaf5jaj01j7b90gkm96h2g5nw0pyn03qkxq8xvbkraq1y") (f (quote (("test") ("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-macros-0.0.86 (c (n "musli-macros") (v "0.0.86") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rv5d11gsnr6rdp5k6lnzrq2m6bgajnxyas7h0b6mn7fbvil3dnq") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.87 (c (n "musli-macros") (v "0.0.87") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pdd4xg720rbljxnpa74wsrmzmz5ha6i046r85vbkyi202m8rpyx") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.88 (c (n "musli-macros") (v "0.0.88") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "182hi8c9mvmds8ipljx48lcapdfji8p2q3mz1jnr6yw6h1sxdvyz") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.89 (c (n "musli-macros") (v "0.0.89") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r0hwc1fajrhr3a8djvsy927wk5iw65ji2cynv7zizlm6nl6m7jr") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.90 (c (n "musli-macros") (v "0.0.90") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zrjbngrd0dbqzjaf2s45pyhkd5kgap71abycq172p4sg2y2l7lk") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.91 (c (n "musli-macros") (v "0.0.91") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "056ybb0cdi8s3hdgvpzpl959dk4cj7i5c4mbfypvbxkbhbknm4wi") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.92 (c (n "musli-macros") (v "0.0.92") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04mpsxp75sdaknw7n5jpbmh7fbng02r11xrhjdjvidjrpzqhf6mw") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.93 (c (n "musli-macros") (v "0.0.93") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04fl0y5kkrnjhfvmx3fh3k1p1v52x744g6010h76fzq7w5xsg0cn") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.94 (c (n "musli-macros") (v "0.0.94") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gx6f2bj3jx67l70pvvm3n2dadq1z0nbb9f9zqba71bdgmdz0dfk") (f (quote (("test")))) (r "1.73")))

(define-public crate-musli-macros-0.0.95 (c (n "musli-macros") (v "0.0.95") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nw8h4dq83f3n2h9jldcvppxiv809lyp3vlzjgx3nn1zmr4g2wym") (f (quote (("test")))) (r "1.67")))

(define-public crate-musli-macros-0.0.96 (c (n "musli-macros") (v "0.0.96") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mz0yz4c9ar57917zymvhiz5v88n4h1d9r4a8m2lmbqphmbyyiij") (f (quote (("test")))) (r "1.67")))

(define-public crate-musli-macros-0.0.97 (c (n "musli-macros") (v "0.0.97") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0va991b3b0jhnmifl8c956aiqv6vr0vnklpdyf85ql716m49ind5") (f (quote (("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.98 (c (n "musli-macros") (v "0.0.98") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07hm2h578dimlhima9r1d64vbcwcq5qajv591aq71yd4wi9arhlv") (f (quote (("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.99 (c (n "musli-macros") (v "0.0.99") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qf4abwh3a1ngdy4jxf1hnfh3w1vjqrl473p1yzdfr4bj3hjvlkf") (f (quote (("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.100 (c (n "musli-macros") (v "0.0.100") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03fmv08fgfwwpwv2xhldjrzp2alkjx1p4ciaviwyfraf4bzb7s0n") (f (quote (("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.101 (c (n "musli-macros") (v "0.0.101") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ib4qwdfcqipkc4dyddkka57n1k6pfq0nmgx0x910vwy226kn7sq") (f (quote (("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.102 (c (n "musli-macros") (v "0.0.102") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l17y7ilm424jw26xx9cb3l57k7rpy4jaxa6cycv8hvnw52ckwxg") (f (quote (("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.103 (c (n "musli-macros") (v "0.0.103") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ql8jl97zvd2cxzypjanx8dh1czwflxik9x9r87di5k41v17d1w7") (f (quote (("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.104 (c (n "musli-macros") (v "0.0.104") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rgdbdszg5gh74z7f5gqlk98y8pij62vllij4qg8v3173xlqkk6a") (f (quote (("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.105 (c (n "musli-macros") (v "0.0.105") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "166qfdcdj4k1gxddmvaq84gz6bjrsd4qc5c9kan9hf07b9cwc4wi") (f (quote (("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.106 (c (n "musli-macros") (v "0.0.106") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vy59b5r3znyfall8k8132d263fx29gc3qqm1fl01pgyrwkqiy8f") (f (quote (("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.107 (c (n "musli-macros") (v "0.0.107") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00kav02851pfb6pk6vwx3m4rclhpnya270rwjh081vp0q64pjwgn") (f (quote (("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.108 (c (n "musli-macros") (v "0.0.108") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11094g4f39z69q0y0wpyn6lligh6i3gvb8nbk7frd3s33cqzjzmx") (f (quote (("verbose") ("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.109 (c (n "musli-macros") (v "0.0.109") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kbkjl6xi8dn81fypaxqrzwiqqwp92xygsmfigq4ahj67m68lgp7") (f (quote (("verbose") ("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.110 (c (n "musli-macros") (v "0.0.110") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0adz3bwr2w0r7s4l815dpw0n15zxfza2qdpcg9h4bnd4r2lvz4s8") (f (quote (("verbose") ("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.111 (c (n "musli-macros") (v "0.0.111") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r3rg1j6nzxmgs2znrxrj8m0h51j7f8sibdqairqf6syhlys998x") (f (quote (("verbose") ("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.112 (c (n "musli-macros") (v "0.0.112") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pz5r5i01yn7hmyjxn86qvb3yrbixjq4g689sl95f7j21ngdqwak") (f (quote (("verbose") ("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.113 (c (n "musli-macros") (v "0.0.113") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1c1n119xfi2nll4hxaj4hkgdfyqm21bkx4h8gz3awkfxjww17ys6") (f (quote (("verbose") ("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.114 (c (n "musli-macros") (v "0.0.114") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dnz32bzxk3b0jgaalg9kym9429pi1y7wq4ipayh5izfp857l7m0") (f (quote (("verbose") ("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.115 (c (n "musli-macros") (v "0.0.115") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "099bdjzzpczw9iy9ynz2flwjwxw4xrs8wjn1g8565zyid0ckhpik") (f (quote (("verbose") ("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.116 (c (n "musli-macros") (v "0.0.116") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cwd6a5h6q19swizpil2zgz6zj72lnk8mn2c1z2fs6lb3vaf1w82") (f (quote (("verbose") ("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.117 (c (n "musli-macros") (v "0.0.117") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1syz8m3rwsn58f10xw2r5fagapk8zkmpmlh9k781s813zja90zmh") (f (quote (("verbose") ("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.118 (c (n "musli-macros") (v "0.0.118") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "138gqyaam13id7pgpgq8ijli5gb86my7lahbb6hc94ssbhbq9qn8") (f (quote (("verbose") ("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.119 (c (n "musli-macros") (v "0.0.119") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kiygdnnikh514rhv778dq1pni4r5hlvs6z99l6yfmz120zhw5hg") (f (quote (("verbose") ("test")))) (r "1.76")))

(define-public crate-musli-macros-0.0.120 (c (n "musli-macros") (v "0.0.120") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ri648iynm9h8jhkij39vslskmyi6xw6bsbgq3yych5sqjp4qv93") (f (quote (("verbose")))) (r "1.76")))

(define-public crate-musli-macros-0.0.121 (c (n "musli-macros") (v "0.0.121") (d (list (d (n "musli") (r "=0.0.121") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mszzdgkz9fyvgg584n4j0cznsg02dprhsgxfps5a024xz42wka6") (f (quote (("verbose")))) (r "1.76")))

(define-public crate-musli-macros-0.0.122 (c (n "musli-macros") (v "0.0.122") (d (list (d (n "musli") (r "=0.0.122") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zzbrgzzf1gs6jvfrj6iiqb7kiwxwa16ndyaknyica6lgg7f5sm8") (f (quote (("verbose")))) (r "1.76")))

