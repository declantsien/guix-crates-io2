(define-module (crates-io mu sl musli-binary-common) #:use-module (crates-io))

(define-public crate-musli-binary-common-0.0.1 (c (n "musli-binary-common") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1c3ahldzvaf1b6j9zh6p8426grkp6dvp9hf11v4cajnjrayjv14j") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.2 (c (n "musli-binary-common") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18pmwb6d83mc65ilf8j5sshwysbajpsiy913j9p0cja3pvlyjp1h") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.3 (c (n "musli-binary-common") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0a6ip1bfqbh1n8bjm315l0bak0v6wzg5cxmakimbk3lh6pml3p99") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.4 (c (n "musli-binary-common") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0v1y4nxgiqddfxala5rq8w3phzr98h687vlkhyjwz1nf83jam11l") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.5 (c (n "musli-binary-common") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "05rmkk810dvmnk7bcxbf1dng5v2x7hil76is0mjrv26nyki14pjh") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.6 (c (n "musli-binary-common") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1874ks64bwhhfq8b758gkinn3y76a24mh7kfmgyr8pznr9fryxf3") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.7 (c (n "musli-binary-common") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0mbpzi9ih553f24m1l4mskr59vcn1f354p1crgvl7awj6srz8l0f") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.8 (c (n "musli-binary-common") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1rb29m1a2fvhjjira08nckzilfrxnxp8f65m3xp404c4584v9rak") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.9 (c (n "musli-binary-common") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "166kzji07g1ww5zqmz4hhdb40xbkz53cnwrxp3nc3qg6n9whific") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.10 (c (n "musli-binary-common") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0p7y0y40h57fdxkyk5l96wcqqa9g3w3yrzpyxck40jn2l8q7bviz") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.11 (c (n "musli-binary-common") (v "0.0.11") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1fj0cw7wx1k319sc5aivb1jnwhl368s8yxssap2fnrwbj7d1y5q9") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.12 (c (n "musli-binary-common") (v "0.0.12") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1i3hdx0qs13z0p1jv1amxp5vkihccwvg4s8il23zv4vnzwiizfa1") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.13 (c (n "musli-binary-common") (v "0.0.13") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "11mwavs4dn27z8wihlqdhpq06z62cf9wv9vzjq4vbr3h74mvzxk3") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.14 (c (n "musli-binary-common") (v "0.0.14") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "01iaii2b4hpcj58fx5hci650qznr8gq83knyyrvf8vyyyjd8cwy4") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.15 (c (n "musli-binary-common") (v "0.0.15") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0w8pdsyzmqjq753h6r0rhwq9s6jk43dgjaw2fzac9p86i4cf1s5y") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.16 (c (n "musli-binary-common") (v "0.0.16") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0kcw9iq0bbbhw6281wcas1qwyfpl84r55zngnandb2s7hd058sv5") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.17 (c (n "musli-binary-common") (v "0.0.17") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1a3v7zbjfyd2brdfxi62h4nrwr8a4mgkbymsri8z43s1zf6630mb") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.18 (c (n "musli-binary-common") (v "0.0.18") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli") (r "^0.0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0x0xd96dwsgzqbpmmk190pp3k37zkda1y0idklxih1fm899bgw1p") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.19 (c (n "musli-binary-common") (v "0.0.19") (d (list (d (n "musli") (r "^0.0.19") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0cgh504habwwbb02j266lj61bzn8vaxkcx2179i0jhj6nz2i7vi5") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.20 (c (n "musli-binary-common") (v "0.0.20") (d (list (d (n "musli") (r "^0.0.20") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1b54a440f6cpnydfis2kcjdxpxlfzhb23hz2kd0p8hl0qxwsydwi") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.21 (c (n "musli-binary-common") (v "0.0.21") (d (list (d (n "musli") (r "^0.0.21") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "07p4f43r6hz4g0r1wml5avbpfvkiy1705aaccwy4shvdg26m6y7g") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.22 (c (n "musli-binary-common") (v "0.0.22") (d (list (d (n "musli") (r "^0.0.22") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0smi3in0g0qzh3gnqz6y3x15pfssny0ra2bj3wxrhpqfz6xp9k0z") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.23 (c (n "musli-binary-common") (v "0.0.23") (d (list (d (n "musli") (r "^0.0.23") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0x4br2j3vkfd9z9lydkr1i8j7hky9vaq0zfdzaai7z96ci5b0r7g") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-binary-common-0.0.24 (c (n "musli-binary-common") (v "0.0.24") (d (list (d (n "musli") (r "^0.0.24") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1zi6sgb55cbk3wc1jf2frxqy6185bggwky7rfwfhhn7w0gaxbcqf") (f (quote (("std" "musli/std") ("default" "std"))))))

