(define-module (crates-io mu sl muslib) #:use-module (crates-io))

(define-public crate-muslib-0.1.0 (c (n "muslib") (v "0.1.0") (d (list (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.2") (f (quote ("abi3-py311" "extension-module"))) (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (d #t) (k 0)))) (h "1b7s41if43zndin9scwph33h0c38bsc1rrdhwws7c0h9aw9v6dxr")))

(define-public crate-muslib-0.1.1 (c (n "muslib") (v "0.1.1") (d (list (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.2") (f (quote ("abi3-py311" "extension-module"))) (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (d #t) (k 0)))) (h "17r2sgkkg6faw04ib77q53iim8v1k3frp8fvvzl1vp88xzfvki0p")))

