(define-module (crates-io mu sl musli-common) #:use-module (crates-io))

(define-public crate-musli-common-0.0.25 (c (n "musli-common") (v "0.0.25") (d (list (d (n "musli") (r "^0.0.25") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1lcmihkc226msjszbldbf50gdwg9vx9hk7kj7lbcy0h9zi85wmkz") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-common-0.0.26 (c (n "musli-common") (v "0.0.26") (d (list (d (n "musli") (r "^0.0.26") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1g7qcnf72q53qps0jqqbssy038ly8907xs31awbfdwl6yadb4yh6") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-common-0.0.27 (c (n "musli-common") (v "0.0.27") (d (list (d (n "musli") (r "^0.0.27") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0xyj2wscxfd1imhwiqdlcx92hmicc4aadavabkc9hyzpra73wnwl") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-common-0.0.28 (c (n "musli-common") (v "0.0.28") (d (list (d (n "musli") (r "^0.0.28") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0kj2rncavsnn1gvqjy5qrspmbnr7mzzy9y3syncgdpvmslc84qn5") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-common-0.0.29 (c (n "musli-common") (v "0.0.29") (d (list (d (n "musli") (r "^0.0.29") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03kszifnwdkml3zq60lrrpi37kvvxhnc1pibfb3ki90qvgjzksbj") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-common-0.0.30 (c (n "musli-common") (v "0.0.30") (d (list (d (n "musli") (r "^0.0.30") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0llhdlnvbdq2z4v72rc9g9589yxkxda8rjk4986qjgpdd78hzwg5") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-common-0.0.31 (c (n "musli-common") (v "0.0.31") (d (list (d (n "musli") (r "^0.0.31") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0k0rdaqz5bpp0f1v5ld1mm72s0yl7p3i1ay2lzh2f3mlz0yi4y1n") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-common-0.0.32 (c (n "musli-common") (v "0.0.32") (d (list (d (n "musli") (r "^0.0.32") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1wwx4g6rsb83ikl5s01hzm4sc20mv4yx9x0pzw406faxsl1cds0g") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-common-0.0.33 (c (n "musli-common") (v "0.0.33") (d (list (d (n "musli") (r "^0.0.33") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0jrdx2iznfrpakv3k7p8qm2b6wgxmfjl8fnrh3zl06sar153bvf4") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-common-0.0.34 (c (n "musli-common") (v "0.0.34") (d (list (d (n "musli") (r "^0.0.34") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "170b2kh3bs9wshcdk6vs2rkjw73b7jh20acw50y9yr7k5f4pdhq5") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-common-0.0.35 (c (n "musli-common") (v "0.0.35") (d (list (d (n "musli") (r "^0.0.35") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1psc9n78n8p3x55mxn42cy31bf85sy26yy5m5q1j3aqwlrh1ja01") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-common-0.0.36 (c (n "musli-common") (v "0.0.36") (d (list (d (n "musli") (r "^0.0.36") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "06pv035h81r1nydkh2mfs1y92ajwxfmvq7i6f478mj764c4qxyb2") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-common-0.0.37 (c (n "musli-common") (v "0.0.37") (d (list (d (n "musli") (r "^0.0.37") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1vq78q6xjkfgnjiamwg63d8p34p6iizmswcxiq3dvsvkd2yramlm") (f (quote (("std" "musli/std") ("default" "std"))))))

(define-public crate-musli-common-0.0.38 (c (n "musli-common") (v "0.0.38") (d (list (d (n "musli") (r "^0.0.38") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1crwgd1x3k6989y31wggqyg2f9kmjik11g3sr0anqsdg8big3k6a") (f (quote (("std" "musli/std") ("default" "std")))) (r "1.65")))

(define-public crate-musli-common-0.0.39 (c (n "musli-common") (v "0.0.39") (d (list (d (n "musli") (r "^0.0.39") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "021igwhdbwjwnhl9kl9qxzvavyw3mgr2mm5b9bickpqc31zg86q9") (f (quote (("std" "musli/std") ("default" "std")))) (r "1.65")))

(define-public crate-musli-common-0.0.40 (c (n "musli-common") (v "0.0.40") (d (list (d (n "musli") (r "^0.0.40") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0yy00fi6w5slllmb7620xnyv5wqp5cnfpiv4fhzmm1ick8mxvv9j") (f (quote (("std" "musli/std") ("default" "std")))) (r "1.65")))

(define-public crate-musli-common-0.0.41 (c (n "musli-common") (v "0.0.41") (d (list (d (n "musli") (r "^0.0.41") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "174l9554s2vy0zv21acbx6zv4b5wgxsrxdk198whrrba83yiyps2") (f (quote (("std" "musli/std") ("default" "std")))) (r "1.65")))

(define-public crate-musli-common-0.0.42 (c (n "musli-common") (v "0.0.42") (d (list (d (n "musli") (r "^0.0.42") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1z2x97drvar8widy7kww2717gshipj0j0jw91ym7had679i4d10p") (f (quote (("std" "musli/std") ("default" "std") ("alloc")))) (r "1.65")))

(define-public crate-musli-common-0.0.43 (c (n "musli-common") (v "0.0.43") (d (list (d (n "musli") (r "^0.0.43") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1k4kxrwwjlw55slg7sp37ss5ssbbcibxjm6kz4pavp9ml3v6xwq1") (f (quote (("std" "musli/std") ("default" "std") ("alloc")))) (r "1.65")))

(define-public crate-musli-common-0.0.44 (c (n "musli-common") (v "0.0.44") (d (list (d (n "musli") (r "^0.0.44") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0n2l042a2mkdhy8dgw12f2y95fmrpn53wibkd5ikcfsm166z17cc") (f (quote (("std" "musli/std") ("default" "std") ("alloc")))) (r "1.65")))

(define-public crate-musli-common-0.0.45 (c (n "musli-common") (v "0.0.45") (d (list (d (n "musli") (r "^0.0.45") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1z75lmmybdpqdl27cjipq2b91x0p78srfrjqzpnw50vj3xrbi7qi") (f (quote (("std" "musli/std") ("default" "std") ("alloc")))) (r "1.65")))

(define-public crate-musli-common-0.0.46 (c (n "musli-common") (v "0.0.46") (d (list (d (n "musli") (r "^0.0.46") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "19s91zm24qnp7v78iin85qldm9xskggy9jqn5xpshx258sim2lly") (f (quote (("std" "musli/std") ("default" "std") ("alloc")))) (r "1.65")))

(define-public crate-musli-common-0.0.47 (c (n "musli-common") (v "0.0.47") (d (list (d (n "musli") (r "^0.0.47") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1b7ccnn8jkm01ayjrg8pysgm8212r3lw05y73haxrqbjlinf79g9") (f (quote (("std" "musli/std") ("default" "std") ("alloc")))) (r "1.66")))

(define-public crate-musli-common-0.0.48 (c (n "musli-common") (v "0.0.48") (d (list (d (n "musli") (r "^0.0.48") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "05ff699fv3adbhnh32mi2a50x7jcgzjcn83qhdpfmb1sr6kwvkl5") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.66")))

(define-public crate-musli-common-0.0.49 (c (n "musli-common") (v "0.0.49") (d (list (d (n "musli") (r "^0.0.49") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0kb3kgcbq288ldzm8wd5ixvm95r974wbn2cxb40d7k2k3ad52bkc") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.66")))

(define-public crate-musli-common-0.0.50 (c (n "musli-common") (v "0.0.50") (d (list (d (n "musli") (r "^0.0.50") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0nchid920mp75b2i8pqzmbb185j3nl7wvcnv28hdwl5g2xwn9kpd") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.51 (c (n "musli-common") (v "0.0.51") (d (list (d (n "musli") (r "^0.0.51") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1a42mcx17i63411xj9aq5qya9fz19xiyrb942pbbvx8vqlsx8z3c") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.52 (c (n "musli-common") (v "0.0.52") (d (list (d (n "musli") (r "^0.0.52") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0vg4bffx4154znmg0wia2sy9fzspnb1gb359rwhj067jpimb5fmy") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.53 (c (n "musli-common") (v "0.0.53") (d (list (d (n "musli") (r "^0.0.53") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "17vymp3zfy2sm32f5mmfmai4fjj6k22ydrj0qlrcv69zqlf0wgwp") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.54 (c (n "musli-common") (v "0.0.54") (d (list (d (n "musli") (r "^0.0.54") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1r90vhx8590f1r767d70zdwfihg84h4scy3h6na5n3v3ni0apyb6") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.55 (c (n "musli-common") (v "0.0.55") (d (list (d (n "musli") (r "^0.0.55") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1a55pah889mn9bmjqhd6lxg6xpfhhg3dzl6vxwxv5d8ipxkipl4s") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.56 (c (n "musli-common") (v "0.0.56") (d (list (d (n "musli") (r "^0.0.56") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1qf06hk86jqscwf974vwvzfqg6am4wl513i4wcb1fkigwjnsdcpr") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.57 (c (n "musli-common") (v "0.0.57") (d (list (d (n "musli") (r "^0.0.57") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0l1kymvqvc7syp4md8k14nwvxxi6y05kpa376lp3s63w5g9zfgsl") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.58 (c (n "musli-common") (v "0.0.58") (d (list (d (n "musli") (r "^0.0.58") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1kc337xma5xp1xk0m042pbfs1qq16splpmy18h4kp8m9cxbz1kfx") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.59 (c (n "musli-common") (v "0.0.59") (d (list (d (n "musli") (r "^0.0.59") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1rbnb70pfncpsqsf4h1blkmyxh892b7qinqs0ipz1pv9cakzzxy1") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.60 (c (n "musli-common") (v "0.0.60") (d (list (d (n "musli") (r "^0.0.60") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "19r9wzbyi78zpl9rs8yb7gk42nbnbpq7j9pgx33siaaxxpqzyy2x") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.61 (c (n "musli-common") (v "0.0.61") (d (list (d (n "musli") (r "^0.0.61") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "00w1fbhsa1gp2rnqrs7b6h8dngays2b2l699qgl5m9bhn3afclcr") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.62 (c (n "musli-common") (v "0.0.62") (d (list (d (n "musli") (r "^0.0.62") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1fm809a6f9589j62ha52gmqvnzpjx4pw7najd54zvszqp05b91i9") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.63 (c (n "musli-common") (v "0.0.63") (d (list (d (n "musli") (r "^0.0.63") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "04cs8gzlwcawww4d8swiajd3sc4nn7v616c5scslbsk0mlb4gfq3") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.64 (c (n "musli-common") (v "0.0.64") (d (list (d (n "musli") (r "^0.0.64") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0jsdhi53fffz6l0a4bzzix1yplv9fa8ra5avf9jqqhx8ihgyknhq") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.65 (c (n "musli-common") (v "0.0.65") (d (list (d (n "musli") (r "^0.0.65") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "120vs91fp9jzzsswi5rw5f4y5487m2iyi56s3izwy530psh3w0m3") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.66 (c (n "musli-common") (v "0.0.66") (d (list (d (n "musli") (r "^0.0.66") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0i2n0nn8290k6k0rfq58hrzyg4iy30zk104yhxf0837wkl08crxr") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.67 (c (n "musli-common") (v "0.0.67") (d (list (d (n "musli") (r "^0.0.67") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1rsy2ap9jg4971sshbalfl8knwz74dx5xkh8c218qv8mr43lxgll") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.68 (c (n "musli-common") (v "0.0.68") (d (list (d (n "musli") (r "^0.0.68") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0jdkp1083h10xv4xbkfihl9zagfa5pbvph1xdkflg90dmh17zv1h") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.69 (c (n "musli-common") (v "0.0.69") (d (list (d (n "musli") (r "^0.0.69") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0nkfhaami514xdyi6yzc8psdvfjl68iix7q51vh88fj6biiaifg5") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.70 (c (n "musli-common") (v "0.0.70") (d (list (d (n "musli") (r "^0.0.70") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1knwdmqv3gi43p9kda4m9jsinp4kbwwhsizy478kihy7yv4gyyi1") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.71 (c (n "musli-common") (v "0.0.71") (d (list (d (n "musli") (r "^0.0.71") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1r91fdj2p2pd2b1cv5sixiy7687qz2jqsfgzmw8bzln1ydj7srmv") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.72 (c (n "musli-common") (v "0.0.72") (d (list (d (n "musli") (r "^0.0.72") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1n8d4f1gbzn1mac6fxsg84vwjab4pbx5c9c8j1y0i4mx0mdq77nz") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.73 (c (n "musli-common") (v "0.0.73") (d (list (d (n "musli") (r "^0.0.73") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0j0nx7xap3f7rfkknmpdi3g0j8515p144p3spfb50lh6canxhhs3") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.74 (c (n "musli-common") (v "0.0.74") (d (list (d (n "musli") (r "^0.0.74") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0vjii410rk6psjn2jfi8r08rdz4s6sz79gyw4nbimj8xgckrk5xr") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.75 (c (n "musli-common") (v "0.0.75") (d (list (d (n "musli") (r "^0.0.75") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0qql7k7hgljvjiy2w55iqjsv28sq2gnn6y4bai71xiqss37nj0i0") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.76 (c (n "musli-common") (v "0.0.76") (d (list (d (n "musli") (r "^0.0.76") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "04fnbs2f7x3xcicxlzfihfr978l96j8kmwhgipabvdp1s3icvzn0") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.77 (c (n "musli-common") (v "0.0.77") (d (list (d (n "musli") (r "^0.0.77") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0nmi2ig9a9la67m94mglrmzzp2081n1r2kq8szyv1hkzdmx290dr") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.78 (c (n "musli-common") (v "0.0.78") (d (list (d (n "musli") (r "^0.0.78") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1j7x9pfjh0s00ngkg8z7hx9im2ab6zla7vm3782z00mp5ghh6q70") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.79 (c (n "musli-common") (v "0.0.79") (d (list (d (n "musli") (r "^0.0.79") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0296da43cd4p94wbv88axd7ary7cx017qbgkmj7005ic5v3kr22d") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.80 (c (n "musli-common") (v "0.0.80") (d (list (d (n "musli") (r "^0.0.80") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1x36gzdbz7lwqgaflyy65mzj1zfc6wkx0nqpb1rycd99zsk3hn5j") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.81 (c (n "musli-common") (v "0.0.81") (d (list (d (n "musli") (r "^0.0.81") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1006z002yjdyfrlz1h8dw1n7xa1fsd7c4mcak1pgvya51wrbx1wx") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.82 (c (n "musli-common") (v "0.0.82") (d (list (d (n "musli") (r "^0.0.82") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0pay2bcg0sqsqin031x69rn6gv0jbfx1qw7x6fq9fga2zv38dwda") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.83 (c (n "musli-common") (v "0.0.83") (d (list (d (n "musli") (r "^0.0.83") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "07lbfslxr0gb66rd5vs8yhk648rwr1cx4i6nh1lp6q8al300zbaw") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.84 (c (n "musli-common") (v "0.0.84") (d (list (d (n "musli") (r "^0.0.84") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "14p5hg0jzgcj9wcgsnjq81wlla65zbx1rls0506dk4jgp7a6w2qf") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.85 (c (n "musli-common") (v "0.0.85") (d (list (d (n "musli") (r "^0.0.85") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1mf1b36a8x12pd8idn8v1kggz7ngwg2x7k3jrbm40drby1y4cwqc") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.86 (c (n "musli-common") (v "0.0.86") (d (list (d (n "musli") (r "^0.0.86") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0ckgjzypcglkzchpsshciy9qykyv5vxadvq8cfacmgbv8rqza7f9") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.87 (c (n "musli-common") (v "0.0.87") (d (list (d (n "musli") (r "^0.0.87") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1lmw4x88y9rlzm7grg4nmkm14jncfbgwwskmcw8vqqaqqv9jraw4") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.88 (c (n "musli-common") (v "0.0.88") (d (list (d (n "musli") (r "^0.0.88") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0ygkwy3hrwkdk8f81ir7ix6r3b4nf9hwygy6dink44pkv1a8m3j3") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.89 (c (n "musli-common") (v "0.0.89") (d (list (d (n "musli") (r "^0.0.89") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1353skxfzpylyw0v2qb832a79i2d2sdcpi1lnmhkyk8akjlgsl67") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.90 (c (n "musli-common") (v "0.0.90") (d (list (d (n "musli") (r "^0.0.90") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1n80sadbjm5b6rnf76bb2qq5j08bcyv8ah0ag9dcdg37919zg7hb") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.91 (c (n "musli-common") (v "0.0.91") (d (list (d (n "musli") (r "^0.0.91") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0d49v7605l47pxqzpcsfm27dcxck1g3iki9g4rjxrzvdkli6can6") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.92 (c (n "musli-common") (v "0.0.92") (d (list (d (n "musli") (r "^0.0.92") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0ifps6iwzrx7fkb39dhk53hna2bxk3mxqkz0jv2kqy68lvhc6gay") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.93 (c (n "musli-common") (v "0.0.93") (d (list (d (n "musli") (r "^0.0.93") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0rq910fafd1hz90a38vwh9lzqw7ff46x7h5krpsy07ywljmm943p") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.94 (c (n "musli-common") (v "0.0.94") (d (list (d (n "musli") (r "^0.0.94") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "10crxzypyw7vaig8k0nm51rqya0q5x2w9wlh3bs3354m253x8dg2") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.73")))

(define-public crate-musli-common-0.0.95 (c (n "musli-common") (v "0.0.95") (d (list (d (n "musli") (r "^0.0.95") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0ssgbkwgcs7rwmyw7vlig3c1dv07mcw6bc31rlif39g4pxc3g6vj") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.67")))

(define-public crate-musli-common-0.0.96 (c (n "musli-common") (v "0.0.96") (d (list (d (n "musli") (r "^0.0.96") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0kp6pf6r9qq5973gnsnfsnqdhpi22dl83pn4196adx0d8pssk8mv") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std")))) (r "1.67")))

(define-public crate-musli-common-0.0.97 (c (n "musli-common") (v "0.0.97") (d (list (d (n "musli") (r "^0.0.97") (k 0)) (d (n "musli-allocator") (r "^0.0.97") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0a6gjmwcm31ibi411mgvz8p4p6qpg10mcfyy3s719ksa2s2v58fs") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

(define-public crate-musli-common-0.0.98 (c (n "musli-common") (v "0.0.98") (d (list (d (n "musli") (r "^0.0.98") (k 0)) (d (n "musli-allocator") (r "^0.0.98") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1ws1nvmdry7ck3bb6zbfhs4dksbww6bcd83ys676d8pmkbsg6ssl") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

(define-public crate-musli-common-0.0.99 (c (n "musli-common") (v "0.0.99") (d (list (d (n "musli") (r "^0.0.99") (k 0)) (d (n "musli-allocator") (r "^0.0.99") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "186h0jybxr341imi6kp31vbihcnr15xgbzdvir964yd98ggs4ga1") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

(define-public crate-musli-common-0.0.100 (c (n "musli-common") (v "0.0.100") (d (list (d (n "musli") (r "^0.0.100") (k 0)) (d (n "musli-allocator") (r "^0.0.100") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1i22c6q5dc4b4ly3w8i1fv0cv0iyvhxrklrc663b8fr5jq0jqpq7") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

(define-public crate-musli-common-0.0.101 (c (n "musli-common") (v "0.0.101") (d (list (d (n "musli") (r "^0.0.101") (k 0)) (d (n "musli-allocator") (r "^0.0.101") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1cbacdap4vlm9z8nzav4d5jhaxlqipifiigms360jl1kv24f9xac") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

(define-public crate-musli-common-0.0.102 (c (n "musli-common") (v "0.0.102") (d (list (d (n "musli") (r "^0.0.102") (k 0)) (d (n "musli-allocator") (r "^0.0.102") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1vaxs45z4vyvf609iyindmsp2w012bz3ywsf4cm8bxk2vfvbfpll") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

(define-public crate-musli-common-0.0.103 (c (n "musli-common") (v "0.0.103") (d (list (d (n "musli") (r "^0.0.103") (k 0)) (d (n "musli-allocator") (r "^0.0.103") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0kzcvm4jhcv2qxfaysyxkyypgq1v4c7h4xa50mzc7g9sa2m7dpjq") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

(define-public crate-musli-common-0.0.104 (c (n "musli-common") (v "0.0.104") (d (list (d (n "musli") (r "^0.0.104") (k 0)) (d (n "musli-allocator") (r "^0.0.104") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0hj0xih19jswxhbyrnffhdk6r27zv793bw9sys8hzhbwp5sd97hl") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

(define-public crate-musli-common-0.0.105 (c (n "musli-common") (v "0.0.105") (d (list (d (n "musli") (r "^0.0.105") (k 0)) (d (n "musli-allocator") (r "^0.0.105") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0cqqq1qw918vrwl28qddnyvwdrq4w03c6caawg4vycjvdrrvrh9n") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

(define-public crate-musli-common-0.0.106 (c (n "musli-common") (v "0.0.106") (d (list (d (n "musli") (r "^0.0.106") (k 0)) (d (n "musli-allocator") (r "^0.0.106") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "1fikgb1abzr1ljh5vlpnc5l5vkk64g37ija922021d268an18w64") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

(define-public crate-musli-common-0.0.107 (c (n "musli-common") (v "0.0.107") (d (list (d (n "musli") (r "^0.0.107") (k 0)) (d (n "musli-allocator") (r "^0.0.107") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0qlqg68wvj3j0ch8547bscjg0fn1vfas022pmw4iq4vmy3ks1cvm") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

(define-public crate-musli-common-0.0.108 (c (n "musli-common") (v "0.0.108") (d (list (d (n "musli") (r "^0.0.108") (k 0)) (d (n "musli-allocator") (r "^0.0.108") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "059n25l6p48i5dqcyhgjjw9qpb5vhb023zc3kbwrivf4q4phhr5y") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

(define-public crate-musli-common-0.0.109 (c (n "musli-common") (v "0.0.109") (d (list (d (n "musli") (r "^0.0.109") (k 0)) (d (n "musli-allocator") (r "^0.0.109") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "0xgk70w6kn7cr1f4844h8ml9cnpgrn78pca2css6xqz4ix44lf88") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

(define-public crate-musli-common-0.0.110 (c (n "musli-common") (v "0.0.110") (d (list (d (n "musli") (r "^0.0.110") (k 0)) (d (n "musli-allocator") (r "^0.0.110") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)))) (h "05xm6an9paj9h4cll6pb9vq89kkshw7pxvdcbshfvzzdj87bk5h1") (f (quote (("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (s 2) (e (quote (("std" "musli/std" "simdutf8?/std" "musli-allocator/std")))) (r "1.76")))

