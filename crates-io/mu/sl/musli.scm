(define-module (crates-io mu sl musli) #:use-module (crates-io))

(define-public crate-musli-0.0.1 (c (n "musli") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "0h7llkjvxidsfcdicam1pfc1scs1zbx4fj5nijp3ky8nwggpr20x") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.2 (c (n "musli") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "0a26yzaq1i8ljg4lppg5zdgm9i87xmlwbhi123cm8faj1ib7msmn") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.3 (c (n "musli") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "065js2g9csg6yhv32jgp5yg69dcncvmsdsnqa92hkv5sjffdkzdv") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.4 (c (n "musli") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "0hmz5n5wjbs2kxxx75gzy0bcz3vxcvcclmxjv29y7708spca53ya") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.5 (c (n "musli") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "09h5anii7p1r8d7a4bg97ipn36wvdxqsiy7x2wmrcdnwv3dq884m") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.6 (c (n "musli") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.6") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "0wggkrbvayi9fsnw6bvv8p6450qwq0qml9l38is4ijlf4vhdanf6") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.7 (c (n "musli") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.7") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "0s13gm7cngc39afqr029r720fh9fwxxlra9irfvlkllf38js559s") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.8 (c (n "musli") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.8") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "1m42y6x31ahs2m2d42klb0a6xm9ddycjsz2sm4iff8vz9xsn5vl9") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.9 (c (n "musli") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.9") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "0mbkbkd2dqax4vsir9njn259cgvnyc28d3b4ydgyj47cj7dpvrrs") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.10 (c (n "musli") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.10") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "1cjd2gqs6321ga393pmf8q6rgjsfi067g6ram18ima1b2p3qfa1x") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.11 (c (n "musli") (v "0.0.11") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.11") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.60") (d #t) (k 2)))) (h "0nrqivdbgyz35xwng1dk0nn762v48dircm8a8zah2ipf4jf5rqvw") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.12 (c (n "musli") (v "0.0.12") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.12") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.60") (d #t) (k 2)))) (h "18hijalkiq47ljmygj9fvabj5wa8yp6jpzcs4x51x5wvvlxz1cfb") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.13 (c (n "musli") (v "0.0.13") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.13") (d #t) (k 0)))) (h "1zyf57jw6qcxfn6w6438hkl9cc9km1m44kvj8xzkcss9wrsqiv61") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.14 (c (n "musli") (v "0.0.14") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.14") (d #t) (k 0)))) (h "0ql2g3d46zm2401fdyx375kdzsplfgy1nmazlmsgxw6vlah8pg5s") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.15 (c (n "musli") (v "0.0.15") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.15") (d #t) (k 0)))) (h "0wwwrcwrg94pdi1xa265cim69hciz1q5sxqa5ah53y5ipm134yvy") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.16 (c (n "musli") (v "0.0.16") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.16") (d #t) (k 0)))) (h "1kbrbldgmppy4kcyr7bd72j1v2vb8wihl3gqs01g7g3cyx5r07ia") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.17 (c (n "musli") (v "0.0.17") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.17") (d #t) (k 0)))) (h "0v50bj872pazv8x3gn41w62k4qihdsdpfr43givpvhvwlap8598h") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.18 (c (n "musli") (v "0.0.18") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "musli-macros") (r "^0.0.18") (d #t) (k 0)))) (h "1skqzq3bpkijyb8alvqzw7pq1p0ajyjzzdzbb7mx9dd3vh7c70x1") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.19 (c (n "musli") (v "0.0.19") (d (list (d (n "musli-macros") (r "^0.0.19") (d #t) (k 0)))) (h "0xa4m0ya0glsffh9396iby8csr4ahxs74vqsn2qc4z0r13nvh3kh") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.20 (c (n "musli") (v "0.0.20") (d (list (d (n "musli-macros") (r "^0.0.20") (d #t) (k 0)))) (h "1lcja9bnrybdz7f09b46x61pn1n9hwqqd7d33px9mzhfv5rsknlf") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.21 (c (n "musli") (v "0.0.21") (d (list (d (n "musli-macros") (r "^0.0.21") (d #t) (k 0)))) (h "0zi6075i7dadppwg3hx0cwsl7f3420v2f1j5v9b4y66mpc3m92kk") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.22 (c (n "musli") (v "0.0.22") (d (list (d (n "musli-macros") (r "^0.0.22") (d #t) (k 0)))) (h "0rkll0q15042z379pkn58amzwrmasib35jvf8qgx24pqcafwiv2i") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.23 (c (n "musli") (v "0.0.23") (d (list (d (n "musli-macros") (r "^0.0.23") (d #t) (k 0)))) (h "14f5qwpwm7kjkz890gnjxxg6bv1s7r2p82iqdsvg62cjpy1x8pid") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.24 (c (n "musli") (v "0.0.24") (d (list (d (n "musli-macros") (r "^0.0.24") (d #t) (k 0)))) (h "179p5rmkspd1p3lvaik4ghqi0k7zwirqh1zrb303fmhcgpnn7hbw") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.25 (c (n "musli") (v "0.0.25") (d (list (d (n "musli-macros") (r "^0.0.25") (d #t) (k 0)))) (h "06jyc9llbl095772kinda5c3a0yqap6pvbk7hws290w4l7cy68rj") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.26 (c (n "musli") (v "0.0.26") (d (list (d (n "musli-macros") (r "^0.0.26") (d #t) (k 0)))) (h "147sf1d6g429a4md8hv389bgrnvbf7bv9x13lhm1lbn46aazzfyg") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.27 (c (n "musli") (v "0.0.27") (d (list (d (n "musli-macros") (r "^0.0.27") (d #t) (k 0)))) (h "1qz4f4ghxksag86v70hv4qwfb3blhzp4jxigb9mp1vgnzk3daicr") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.28 (c (n "musli") (v "0.0.28") (d (list (d (n "musli-macros") (r "^0.0.28") (d #t) (k 0)))) (h "0325m005lb098l9xr4cvrzgab7rybjpxbfj33f8fq1p0as1inw1h") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.29 (c (n "musli") (v "0.0.29") (d (list (d (n "musli-macros") (r "^0.0.29") (d #t) (k 0)))) (h "1bgwzspg4hk6fazdmgnnl83gbdkq516dwl8za88h1jly52qrd9fz") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.30 (c (n "musli") (v "0.0.30") (d (list (d (n "musli-macros") (r "^0.0.30") (d #t) (k 0)))) (h "0lf2fsf38iig0wxgfhkpjxryf3niqa3g9a6mhv4hr5f8iq5klfc4") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.31 (c (n "musli") (v "0.0.31") (d (list (d (n "musli-macros") (r "^0.0.31") (d #t) (k 0)))) (h "0m18sagdkv2ln9lzzhylw6hyshk57jpzw3yyjyas0v6w4hsgisqv") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.32 (c (n "musli") (v "0.0.32") (d (list (d (n "musli-macros") (r "^0.0.32") (d #t) (k 0)))) (h "09d26r8sbhj63mn6aac4qr4ja9zyp9hdgzvv6iy56varjniqf9j7") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.33 (c (n "musli") (v "0.0.33") (d (list (d (n "musli-macros") (r "^0.0.33") (d #t) (k 0)))) (h "1h2pacr9vnzi3sxg9klqip0gijsbgq11z29w2zr1z8gjccy1dxfm") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.34 (c (n "musli") (v "0.0.34") (d (list (d (n "musli-macros") (r "^0.0.34") (d #t) (k 0)))) (h "0m94bwn4baiwxf0yksj7ji1q658bq04qqwssagab2hspy9qg7bpn") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.35 (c (n "musli") (v "0.0.35") (d (list (d (n "musli-macros") (r "^0.0.35") (d #t) (k 0)))) (h "04bkkffrw6k34sm43klgab4h95w04gw06gdbcx2l0nqd6n5ci7ln") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.36 (c (n "musli") (v "0.0.36") (d (list (d (n "musli-macros") (r "^0.0.36") (d #t) (k 0)))) (h "03awhq8ggs036iasrxm5a6jyqxizxm9j6ns2zpkr69a72idg57p5") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.37 (c (n "musli") (v "0.0.37") (d (list (d (n "musli-macros") (r "^0.0.37") (d #t) (k 0)))) (h "1wm2qifr9mjl3rhax1h2m5gywr2vpqm3f65n8d3arcshyp6jz4nb") (f (quote (("std") ("default" "std"))))))

(define-public crate-musli-0.0.38 (c (n "musli") (v "0.0.38") (d (list (d (n "musli-macros") (r "=0.0.38") (d #t) (k 0)))) (h "12a2h8zdhfslcxdc1rxm2sp6c0r1lfyrhii76xzcrr7ziwmayii1") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-musli-0.0.39 (c (n "musli") (v "0.0.39") (d (list (d (n "musli-macros") (r "=0.0.39") (d #t) (k 0)))) (h "0xxw9kmc25gdf1l640cdhv26xrj9598vx9s6xyy0d2whwgps5kss") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-musli-0.0.40 (c (n "musli") (v "0.0.40") (d (list (d (n "musli-macros") (r "=0.0.40") (d #t) (k 0)))) (h "0jcyp10qcafwk8mwx0nsmxgfkjbw8qyy911xl9f7cvdwgziiasra") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-musli-0.0.41 (c (n "musli") (v "0.0.41") (d (list (d (n "musli-macros") (r "=0.0.41") (d #t) (k 0)))) (h "1bc9p6khhyzsi2qc1kscxf6285l5vajzhwzwg4243aj36rxpd4jz") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-musli-0.0.42 (c (n "musli") (v "0.0.42") (d (list (d (n "musli-macros") (r "=0.0.42") (d #t) (k 0)))) (h "1frjlgn3cp6cdrsljfvsgd5ny4zjfyw18i0ig4490cs8s96i48cw") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.65")))

(define-public crate-musli-0.0.43 (c (n "musli") (v "0.0.43") (d (list (d (n "musli-macros") (r "=0.0.43") (d #t) (k 0)))) (h "0isk5lj3qdzix5712bnckyykz0v1vcw0vg82hxxhhp8nv66qdirs") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.65")))

(define-public crate-musli-0.0.44 (c (n "musli") (v "0.0.44") (d (list (d (n "musli-macros") (r "=0.0.44") (d #t) (k 0)))) (h "1jv2ay51zkb9rfl2dnyj7s207i925f7kai72ls44lzhv49130fp6") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.65")))

(define-public crate-musli-0.0.45 (c (n "musli") (v "0.0.45") (d (list (d (n "musli-macros") (r "=0.0.45") (d #t) (k 0)))) (h "14cmaq5d7kc4sy9cpwqafv4g9y5qh31711bif87zjs4hkkpns1gs") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.65")))

(define-public crate-musli-0.0.46 (c (n "musli") (v "0.0.46") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "musli-macros") (r "=0.0.46") (d #t) (k 0)))) (h "1f12mfwhfcbf2ijzrxlmfdq3zpzj0wcfm3qiqrl0gk8p09zkq1np") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.65")))

(define-public crate-musli-0.0.47 (c (n "musli") (v "0.0.47") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "musli-macros") (r "=0.0.47") (d #t) (k 0)))) (h "0pd8yfaqdvzcmsyz1mgn6c3bcbmw7c5xfxivm6h7qlsl28dr1ynf") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.66")))

(define-public crate-musli-0.0.48 (c (n "musli") (v "0.0.48") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "musli-macros") (r "=0.0.48") (d #t) (k 0)))) (h "0p5b58l3pzbd40wdlkpblwp9a7m8z9spv6z31ip6mh33c8izjsbn") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.66")))

(define-public crate-musli-0.0.49 (c (n "musli") (v "0.0.49") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "musli-macros") (r "=0.0.49") (d #t) (k 0)))) (h "0kirf6jp3d8df2pdkq489bqs300j668n24sjrhmp6zr8h8fwjkcb") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.66")))

(define-public crate-musli-0.0.50 (c (n "musli") (v "0.0.50") (d (list (d (n "musli-macros") (r "=0.0.50") (d #t) (k 0)))) (h "1cnraw6mf3n01kgsc5wggk4nbbd26fgk6gddpkv80h9drwlmarzv") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.51 (c (n "musli") (v "0.0.51") (d (list (d (n "musli-macros") (r "=0.0.51") (d #t) (k 0)))) (h "1lz3dcizb4ndraagylaqimw42r9xr4ca0kz3g59r5b7c0bnmy4q4") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.52 (c (n "musli") (v "0.0.52") (d (list (d (n "musli-macros") (r "=0.0.52") (d #t) (k 0)))) (h "18a2vv0b43fm1lzlzcj95w6a45qyy50ilm5qxni4257j90cisc73") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.53 (c (n "musli") (v "0.0.53") (d (list (d (n "musli-macros") (r "=0.0.53") (d #t) (k 0)))) (h "1crlbzk90ad6w80hz664dzx5cx8a1dqjnzm46kqz5ks6lsl9h0rd") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.54 (c (n "musli") (v "0.0.54") (d (list (d (n "musli-macros") (r "=0.0.54") (d #t) (k 0)))) (h "1gxk80v6bdzmyfdp6a327n5kmspc6knwh6wyvbzlzahsrmxgy485") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.55 (c (n "musli") (v "0.0.55") (d (list (d (n "musli-macros") (r "=0.0.55") (d #t) (k 0)))) (h "180409h6ddkba3n7azw9mprdvm313px52ik0r1imfjcnnf8mchj2") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.56 (c (n "musli") (v "0.0.56") (d (list (d (n "musli-macros") (r "=0.0.56") (d #t) (k 0)))) (h "1wxfzc5c4l9zkam5vmywi1k6g67bghgdjjqay2dh86m73ix0m9jq") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.57 (c (n "musli") (v "0.0.57") (d (list (d (n "musli-macros") (r "=0.0.57") (d #t) (k 0)))) (h "0vcf3srfz905863gm4wg7a7f60nihih3kb32scyi9n3j5228fkr9") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.58 (c (n "musli") (v "0.0.58") (d (list (d (n "musli-macros") (r "=0.0.58") (d #t) (k 0)))) (h "1ppprhg7as8msn00dpxh5lqwjm3ggjdwa53j65z7xiknnzj610aw") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.59 (c (n "musli") (v "0.0.59") (d (list (d (n "musli-macros") (r "=0.0.59") (d #t) (k 0)))) (h "0avf928fcap9ikwyshvj594qs3dizdd3x2sp3qcv62ivad03q6qm") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.60 (c (n "musli") (v "0.0.60") (d (list (d (n "musli-macros") (r "=0.0.60") (d #t) (k 0)))) (h "1j6n7iy8rdpja58gaaahkbnf5dzd8a3jgy9d0lpjcbk3iqi5c14p") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.61 (c (n "musli") (v "0.0.61") (d (list (d (n "musli-macros") (r "=0.0.61") (d #t) (k 0)))) (h "11q9fz8zmxik4rvdjgi95crqabi0h6pgcc161s3nyxxjk3yhaip4") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.62 (c (n "musli") (v "0.0.62") (d (list (d (n "musli-macros") (r "=0.0.62") (d #t) (k 0)))) (h "16cibi5v9ihx17xpz7gyrq999v12jscw3cy4dxp7qbb711qs6jp5") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.63 (c (n "musli") (v "0.0.63") (d (list (d (n "musli-macros") (r "=0.0.63") (d #t) (k 0)))) (h "0n7wgyvi1p545sy1p6r4s0cilrad8ljvssk3fik59xrrykamn061") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.64 (c (n "musli") (v "0.0.64") (d (list (d (n "musli-macros") (r "=0.0.64") (d #t) (k 0)))) (h "03n5r13k8xxqs3q5gw8rlnl6lidjsrsnmxxicib2ifs8ab6qv2lp") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.65 (c (n "musli") (v "0.0.65") (d (list (d (n "musli-macros") (r "=0.0.65") (d #t) (k 0)))) (h "1rhwlyfkixaxjd50ca4jd25lm0k8g42yvk67cvjhamd8d0mj9b7l") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.66 (c (n "musli") (v "0.0.66") (d (list (d (n "musli-macros") (r "=0.0.66") (d #t) (k 0)))) (h "1lkj5b73fqdzqaplp5fjfjhvbk7lhsavq9hb2ci7r4sgdmi0r25s") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.67 (c (n "musli") (v "0.0.67") (d (list (d (n "musli-macros") (r "=0.0.67") (d #t) (k 0)))) (h "0f4qi3s6dsgh840vs65n15l1mxybm14yg4xyhw1gsis7fzmxiskq") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.68 (c (n "musli") (v "0.0.68") (d (list (d (n "musli-macros") (r "=0.0.68") (d #t) (k 0)))) (h "1rahv87mg5srcq45v69g9banzpwdizankb0jgf4xrcamsn7vy637") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.69 (c (n "musli") (v "0.0.69") (d (list (d (n "musli-macros") (r "=0.0.69") (d #t) (k 0)))) (h "18q55b0hk4wc3b1lcfn2k151frnghd02nkg7v4x4psmz57s6m8s1") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.70 (c (n "musli") (v "0.0.70") (d (list (d (n "musli-macros") (r "=0.0.70") (d #t) (k 0)))) (h "00dlqi45cxsqr4ri6y7w47chpdvbsaz7cy174n7jiknrd24dfhar") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.71 (c (n "musli") (v "0.0.71") (d (list (d (n "musli-macros") (r "=0.0.71") (d #t) (k 0)))) (h "0v497r5qb3npf65pd5sk8868366ypjk0s71zl04niayyyih3amrg") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.72 (c (n "musli") (v "0.0.72") (d (list (d (n "musli-macros") (r "=0.0.72") (d #t) (k 0)))) (h "1hjw14gz9ra4bilp9yi3pmk8yzpvj7aci7fj9ppx1bs39dm7j82z") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.73 (c (n "musli") (v "0.0.73") (d (list (d (n "musli-macros") (r "=0.0.73") (d #t) (k 0)))) (h "1sppiii9kdqlw4z81v4fwkqv846b2ll1xk3y1gdi1xwgqpv0b0la") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.74 (c (n "musli") (v "0.0.74") (d (list (d (n "musli-macros") (r "=0.0.74") (d #t) (k 0)))) (h "1w71y1rfbhfnz8x79h9lspgi35xcr4mb90sq50g8r2fr1gkbgyp7") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.75 (c (n "musli") (v "0.0.75") (d (list (d (n "musli-macros") (r "=0.0.75") (d #t) (k 0)))) (h "1iiv287k7bzymbm1yi59ab74027jr5pphcp96rh2gq1jw3qcnb4a") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.76 (c (n "musli") (v "0.0.76") (d (list (d (n "musli-macros") (r "=0.0.76") (d #t) (k 0)))) (h "1xln2l59vg8srx2hva7i49fb1bdmcmhp0a6azjkaj23fldg7f5cf") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.77 (c (n "musli") (v "0.0.77") (d (list (d (n "musli-macros") (r "=0.0.77") (d #t) (k 0)))) (h "1hzkc1rrsm7zqa9l46j76y5yfr06fhk9f3jz9wdz51yj85hsqcnh") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.78 (c (n "musli") (v "0.0.78") (d (list (d (n "musli-macros") (r "=0.0.78") (d #t) (k 0)))) (h "14w0qlljyrflwyfcp2w0lvz527rmrw24jj4dlg89yz354vhzipgg") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.79 (c (n "musli") (v "0.0.79") (d (list (d (n "musli-macros") (r "=0.0.79") (d #t) (k 0)))) (h "1072pi4xz7pgsjg7akm0mjcbp8k3jwrrvlxps299ixmaihmzz620") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.80 (c (n "musli") (v "0.0.80") (d (list (d (n "musli-macros") (r "=0.0.80") (d #t) (k 0)))) (h "1x75xsyrhqbybf3ha7986lsigcsvd05b45sw00ii6pcwr520vlgz") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.81 (c (n "musli") (v "0.0.81") (d (list (d (n "musli-macros") (r "=0.0.81") (d #t) (k 0)))) (h "1b2difgahgbh1qcn7i19zk799dgzx1wkw98ziwnkxfdc0bqap4kr") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.82 (c (n "musli") (v "0.0.82") (d (list (d (n "musli-macros") (r "=0.0.82") (d #t) (k 0)))) (h "1142k9wsnf2758hbbhj3vvi4qykc0bjshxl0n7lc4dxjihg8v4dg") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.83 (c (n "musli") (v "0.0.83") (d (list (d (n "musli-macros") (r "=0.0.83") (d #t) (k 0)))) (h "03wb7gkh839raizv24i6hm052krr70wn5yn7vkxyr5wyffg1axwm") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.84 (c (n "musli") (v "0.0.84") (d (list (d (n "musli-macros") (r "=0.0.84") (d #t) (k 0)))) (h "19g8ndkn045qmcnw6jz8lnmxf1pawn0ndgx08y7ppg7829xjk4mn") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.85 (c (n "musli") (v "0.0.85") (d (list (d (n "musli-macros") (r "=0.0.85") (d #t) (k 0)))) (h "0lw0ggjd76amqcvk4nvs0aq407zpjr0sip440dmf435a99h544jf") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.86 (c (n "musli") (v "0.0.86") (d (list (d (n "musli-macros") (r "=0.0.86") (d #t) (k 0)))) (h "16lrjn1wakfnwp23dawdjrb8wh0ibwm6sm1a7z3abrr2an5iv6c0") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.87 (c (n "musli") (v "0.0.87") (d (list (d (n "musli-macros") (r "=0.0.87") (d #t) (k 0)))) (h "1y8473gwq5a4xgmzb7rv8vqabmffjxf71qz1zfqm68441jgb0w9f") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.88 (c (n "musli") (v "0.0.88") (d (list (d (n "musli-macros") (r "=0.0.88") (d #t) (k 0)))) (h "0c4prbwcxcx30j9fmrs5xg1vq4d91akj9mlh58k233adhdinsgx6") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.89 (c (n "musli") (v "0.0.89") (d (list (d (n "musli-macros") (r "=0.0.89") (d #t) (k 0)))) (h "08ij31qcz1g6jmgwicvd92i28idj1j0mw7yl5043ark4idq2azsy") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.90 (c (n "musli") (v "0.0.90") (d (list (d (n "musli-macros") (r "=0.0.90") (d #t) (k 0)))) (h "0cc3rwvfv76yq3vlix6dwy85w5im8wv3qv0vyzmj7fhr7p9b95ki") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.91 (c (n "musli") (v "0.0.91") (d (list (d (n "musli-macros") (r "=0.0.91") (d #t) (k 0)))) (h "1mf8wqgi6xjg0lmhm5h5z4c3n5gyryvs6cmqddnnk9lycl6pg5y7") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.92 (c (n "musli") (v "0.0.92") (d (list (d (n "musli-macros") (r "=0.0.92") (d #t) (k 0)))) (h "05s26a1q8asgd2hg2d20nbzc51lkc2pngc6p8pyfyw18g1iqch5m") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.93 (c (n "musli") (v "0.0.93") (d (list (d (n "musli-macros") (r "=0.0.93") (d #t) (k 0)))) (h "1121vvlfkzq79gmvlfblis8b4a01na4mhzbl1xxm8csrzg110c12") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.94 (c (n "musli") (v "0.0.94") (d (list (d (n "musli-macros") (r "=0.0.94") (d #t) (k 0)))) (h "0m5kcf4x7ds88ar9g17xv0ygi0fz8kskpfkis6npb54i6kgp9iix") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.73")))

(define-public crate-musli-0.0.95 (c (n "musli") (v "0.0.95") (d (list (d (n "musli-macros") (r "=0.0.95") (d #t) (k 0)))) (h "102r6pfvw5rs25na37yxi20y8hfny7jhf3x7pkn5nr188hamyggx") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.67")))

(define-public crate-musli-0.0.96 (c (n "musli") (v "0.0.96") (d (list (d (n "musli-macros") (r "=0.0.96") (d #t) (k 0)))) (h "1chpmv9zqdmf4i3sb1k40nr383m63bnzzindpc8pawpcck3qnccg") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.67")))

(define-public crate-musli-0.0.97 (c (n "musli") (v "0.0.97") (d (list (d (n "musli-macros") (r "=0.0.97") (d #t) (k 0)))) (h "036ly72yrzwjp2c26pkijlifv983ddk27hglrmjyzl0g3xd8fvy8") (f (quote (("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.98 (c (n "musli") (v "0.0.98") (d (list (d (n "musli-macros") (r "=0.0.98") (d #t) (k 0)))) (h "1ixx9s489rv9mlma66jkvi9hisf86dhsklhvp7fgnya7f82dzs6h") (f (quote (("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.99 (c (n "musli") (v "0.0.99") (d (list (d (n "musli-macros") (r "=0.0.99") (d #t) (k 0)))) (h "0k56f06si2m807b2mnlnf0hgjrg2z2ywadplimm88di7hp8cwpvl") (f (quote (("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.100 (c (n "musli") (v "0.0.100") (d (list (d (n "musli-macros") (r "=0.0.100") (d #t) (k 0)))) (h "0n3rg6ni0w0ljxhhnp6n2d41nbxivrihww08s69krarqsqmg542b") (f (quote (("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.101 (c (n "musli") (v "0.0.101") (d (list (d (n "musli-macros") (r "=0.0.101") (d #t) (k 0)))) (h "0p26pmxgw212pgwblbsgpjdc5q0l9bkhz3733m4lp1sbwi4smf93") (f (quote (("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.102 (c (n "musli") (v "0.0.102") (d (list (d (n "musli-macros") (r "=0.0.102") (d #t) (k 0)))) (h "17v3xvqxywdvs6z7bqvr9wpnas0adgsp0xa7gk656mynd39gl5m5") (f (quote (("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.103 (c (n "musli") (v "0.0.103") (d (list (d (n "musli-macros") (r "=0.0.103") (d #t) (k 0)))) (h "13278njklk1cw951gd6yrbnar9x9dmysh951lwj970yhgy4zzbzs") (f (quote (("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.104 (c (n "musli") (v "0.0.104") (d (list (d (n "musli-macros") (r "=0.0.104") (d #t) (k 0)))) (h "1m96hnqz5yzzwmm4rvp5imh61lqy49w8qwkzdgaswb2g0ms0gf48") (f (quote (("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.105 (c (n "musli") (v "0.0.105") (d (list (d (n "musli-macros") (r "=0.0.105") (d #t) (k 0)))) (h "0223ndkrrlj5b0vkc531wfsg100h0f8jra1v4xi10823yhb70100") (f (quote (("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.106 (c (n "musli") (v "0.0.106") (d (list (d (n "musli-macros") (r "=0.0.106") (d #t) (k 0)))) (h "12hkh7558nbv6ih4pyhjg0djvljh66gwd25xxxiflcjxnb07np43") (f (quote (("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.107 (c (n "musli") (v "0.0.107") (d (list (d (n "musli-macros") (r "=0.0.107") (d #t) (k 0)))) (h "1hv3w5wx63vf2bza8nflmz3x947klsaxzy2kgapc2y91a3iw4p4w") (f (quote (("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.108 (c (n "musli") (v "0.0.108") (d (list (d (n "musli-macros") (r "=0.0.108") (d #t) (k 0)))) (h "0ncdxf8jbsds2f4cjsxgy4mr4mnz2bx2r1d3hq6cq4rmlskjk1jr") (f (quote (("verbose" "musli-macros/verbose") ("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.109 (c (n "musli") (v "0.0.109") (d (list (d (n "musli-macros") (r "=0.0.109") (d #t) (k 0)))) (h "0dc3gyb7da4ggmk922vvdh1nslh9jd2gdi188vzpqsilv5bclmm9") (f (quote (("verbose" "musli-macros/verbose") ("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.110 (c (n "musli") (v "0.0.110") (d (list (d (n "musli-macros") (r "=0.0.110") (d #t) (k 0)))) (h "0i4r1wswfj2vg3bjw4jc6a25sy1k5cd9byks8d4cx37bza79xli8") (f (quote (("verbose" "musli-macros/verbose") ("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.111 (c (n "musli") (v "0.0.111") (d (list (d (n "musli-macros") (r "=0.0.111") (d #t) (k 0)))) (h "1jhv4hvdcf41w83dmh27nrmya89cnjpm78l5qpr1qmdci4b5mbzp") (f (quote (("verbose" "musli-macros/verbose") ("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.112 (c (n "musli") (v "0.0.112") (d (list (d (n "musli-macros") (r "=0.0.112") (d #t) (k 0)))) (h "1xaz74089kr4miivqmxs9db8v77g3dy7dpl7rsjacrqlwv7mxvgc") (f (quote (("verbose" "musli-macros/verbose") ("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.113 (c (n "musli") (v "0.0.113") (d (list (d (n "musli-macros") (r "=0.0.113") (d #t) (k 0)))) (h "1y4cfk0sjrhg4dkvv3ankd0r6f9hq6awqlx2ibppb6byy9wmiz47") (f (quote (("verbose" "musli-macros/verbose") ("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.114 (c (n "musli") (v "0.0.114") (d (list (d (n "musli-macros") (r "=0.0.114") (d #t) (k 0)))) (h "00d9zdnshfx4ancdxi22prg88cmr2px9r7ha59js9rbl89yavpa0") (f (quote (("verbose" "musli-macros/verbose") ("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.115 (c (n "musli") (v "0.0.115") (d (list (d (n "musli-macros") (r "=0.0.115") (d #t) (k 0)))) (h "1zvgvj4p2dzf2465042cns2yrk9xn2mkgiv72wvhifqbixvijypv") (f (quote (("verbose" "musli-macros/verbose") ("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.116 (c (n "musli") (v "0.0.116") (d (list (d (n "musli-macros") (r "=0.0.116") (d #t) (k 0)))) (h "1skdl9ajfysqsznwh1zn0by78835s3rqc13p84wqd6j7n59m1gd2") (f (quote (("verbose" "musli-macros/verbose") ("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.117 (c (n "musli") (v "0.0.117") (d (list (d (n "musli-macros") (r "=0.0.117") (d #t) (k 0)))) (h "0ss3vdgmqssphiajb2qifppbhkq5jica1zscl8j3pchgbfc9li5g") (f (quote (("verbose" "musli-macros/verbose") ("std") ("default" "std" "alloc") ("alloc")))) (r "1.76")))

(define-public crate-musli-0.0.118 (c (n "musli") (v "0.0.118") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 2)) (d (n "itoa") (r "^1.0.10") (o #t) (d #t) (k 0)) (d (n "lexical") (r "^6.1.1") (f (quote ("parse-floats"))) (o #t) (k 0)) (d (n "musli-core") (r "=0.0.118") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ryu") (r "^1.0.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)) (d (n "trybuild") (r "^1.0.90") (d #t) (k 2)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 2)))) (h "17zgvrxpy8sdhysipb9ciddvham9fyfflf7lsrwa9iqfm3vgs46y") (f (quote (("wire") ("verbose" "musli-core/verbose") ("value") ("test") ("storage") ("parse-full") ("descriptive" "value") ("default" "std" "alloc") ("alloc" "musli-core/alloc")))) (s 2) (e (quote (("std" "musli-core/std" "lexical?/std" "serde?/std" "simdutf8?/std") ("serde" "dep:serde") ("json" "value" "dep:lexical" "dep:itoa" "dep:ryu")))) (r "1.76")))

(define-public crate-musli-0.0.119 (c (n "musli") (v "0.0.119") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 2)) (d (n "itoa") (r "^1.0.10") (o #t) (d #t) (k 0)) (d (n "lexical") (r "^6.1.1") (f (quote ("parse-floats"))) (o #t) (k 0)) (d (n "musli-core") (r "=0.0.119") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ryu") (r "^1.0.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)) (d (n "trybuild") (r "^1.0.90") (d #t) (k 2)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 2)))) (h "09nszkik1adkkp6avhp76yd9rn50laxp77n4gnxkys068fz7gk8i") (f (quote (("wire") ("verbose" "musli-core/verbose") ("value") ("test" "storage" "wire" "descriptive" "json" "parse-full" "value" "serde") ("storage") ("parse-full") ("descriptive" "value") ("default" "std" "alloc") ("alloc" "musli-core/alloc")))) (s 2) (e (quote (("std" "musli-core/std" "lexical?/std" "serde?/std" "simdutf8?/std") ("serde" "dep:serde") ("json" "value" "dep:lexical" "dep:itoa" "dep:ryu")))) (r "1.76")))

(define-public crate-musli-0.0.120 (c (n "musli") (v "0.0.120") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 2)) (d (n "itoa") (r "^1.0.10") (o #t) (d #t) (k 0)) (d (n "musli-core") (r "=0.0.120") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ryu") (r "^1.0.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)) (d (n "trybuild") (r "^1.0.90") (d #t) (k 2)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 2)))) (h "1rm6qk6bj6a04my8qjc0j9mhn3v2a50k76mg2x07dgqs67hz1a8h") (f (quote (("wire") ("verbose" "musli-core/verbose") ("value") ("test" "storage" "wire" "descriptive" "json" "parse-full" "value" "serde") ("storage") ("parse-full") ("descriptive" "value") ("default" "std" "alloc") ("alloc" "musli-core/alloc")))) (s 2) (e (quote (("std" "musli-core/std" "serde?/std" "simdutf8?/std") ("serde" "dep:serde") ("json" "value" "dep:itoa" "dep:ryu")))) (r "1.76")))

(define-public crate-musli-0.0.121 (c (n "musli") (v "0.0.121") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 2)) (d (n "itoa") (r "^1.0.10") (o #t) (d #t) (k 0)) (d (n "musli-core") (r "=0.0.121") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ryu") (r "^1.0.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)) (d (n "trybuild") (r "^1.0.90") (d #t) (k 2)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 2)))) (h "015wfp3rqgyqsd80dmqggp3ykc6c3n79pv86zz446hcpdn439c8g") (f (quote (("wire") ("verbose" "musli-core/verbose") ("value") ("test" "storage" "wire" "descriptive" "json" "parse-full" "value" "serde") ("storage") ("parse-full") ("descriptive" "value") ("default" "std" "alloc") ("alloc" "musli-core/alloc")))) (s 2) (e (quote (("std" "musli-core/std" "serde?/std" "simdutf8?/std") ("serde" "dep:serde") ("json" "value" "dep:itoa" "dep:ryu")))) (r "1.76")))

(define-public crate-musli-0.0.122 (c (n "musli") (v "0.0.122") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 2)) (d (n "itoa") (r "^1.0.10") (o #t) (d #t) (k 0)) (d (n "musli-core") (r "=0.0.122") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ryu") (r "^1.0.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (o #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (o #t) (k 0)) (d (n "trybuild") (r "^1.0.90") (d #t) (k 2)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 2)))) (h "1dya0riyk07x637fhf67dgmn714h05c31b59w94wfa9kg64isykx") (f (quote (("wire") ("verbose" "musli-core/verbose") ("value") ("test" "storage" "wire" "descriptive" "json" "parse-full" "value" "serde") ("storage") ("parse-full") ("descriptive" "value") ("default" "std" "alloc")))) (s 2) (e (quote (("std" "musli-core/std" "serde?/std" "simdutf8?/std") ("serde" "dep:serde") ("json" "value" "dep:itoa" "dep:ryu") ("alloc" "musli-core/alloc" "serde?/alloc")))) (r "1.76")))

