(define-module (crates-io mu sl musli-utils) #:use-module (crates-io))

(define-public crate-musli-utils-0.0.111 (c (n "musli-utils") (v "0.0.111") (d (list (d (n "musli") (r "^0.0.111") (k 0)) (d (n "musli-allocator") (r "^0.0.111") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1czfbm5qcgnghmrj66lj8labf1fbkbd2c68nr54ca7ppgps8v1fn") (f (quote (("std" "musli/std" "musli-allocator/std") ("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (r "1.76")))

(define-public crate-musli-utils-0.0.112 (c (n "musli-utils") (v "0.0.112") (d (list (d (n "musli") (r "^0.0.112") (k 0)) (d (n "musli-allocator") (r "^0.0.112") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0j95jwqcki4ryhyn9iw0mwr8j41bqrlx69w484aylj9b53bnfa5q") (f (quote (("std" "musli/std" "musli-allocator/std") ("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (r "1.76")))

(define-public crate-musli-utils-0.0.113 (c (n "musli-utils") (v "0.0.113") (d (list (d (n "musli") (r "^0.0.113") (k 0)) (d (n "musli-allocator") (r "^0.0.113") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1x6l8yipbvb8z59w5rzp9186yww8k0s06hgyyc0d4gzf06lpkj8p") (f (quote (("std" "musli/std" "musli-allocator/std") ("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (r "1.76")))

(define-public crate-musli-utils-0.0.114 (c (n "musli-utils") (v "0.0.114") (d (list (d (n "musli") (r "^0.0.114") (k 0)) (d (n "musli-allocator") (r "^0.0.114") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1zi06dss9vxkhwpykh0jfcycv0vgf7f7szbjcmapsh26wp7dl0dl") (f (quote (("std" "musli/std" "musli-allocator/std") ("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (r "1.76")))

(define-public crate-musli-utils-0.0.115 (c (n "musli-utils") (v "0.0.115") (d (list (d (n "musli") (r "^0.0.115") (k 0)) (d (n "musli-allocator") (r "^0.0.115") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0q2fh8n70l9y7ap5d07sawa76i9g9naax235cnzs8vr56arsixqc") (f (quote (("std" "musli/std" "musli-allocator/std") ("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (r "1.76")))

(define-public crate-musli-utils-0.0.116 (c (n "musli-utils") (v "0.0.116") (d (list (d (n "musli") (r "^0.0.116") (k 0)) (d (n "musli-allocator") (r "^0.0.116") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "12ric2qa364cdh1dqb6p7rs3q2qm2861rzw0pxxhyd97mm6n0lah") (f (quote (("std" "musli/std" "musli-allocator/std") ("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (r "1.76")))

(define-public crate-musli-utils-0.0.117 (c (n "musli-utils") (v "0.0.117") (d (list (d (n "musli") (r "^0.0.117") (k 0)) (d (n "musli-allocator") (r "^0.0.117") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0nz70bzpxnxwy07d5yiq9n4lls9c7lymn5j1alkc03mpb7izr26a") (f (quote (("std" "musli/std" "musli-allocator/std") ("default" "std" "alloc") ("alloc" "musli-allocator/alloc")))) (r "1.76")))

