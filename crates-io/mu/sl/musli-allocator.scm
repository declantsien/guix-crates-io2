(define-module (crates-io mu sl musli-allocator) #:use-module (crates-io))

(define-public crate-musli-allocator-0.0.97 (c (n "musli-allocator") (v "0.0.97") (d (list (d (n "musli") (r "^0.0.97") (k 0)))) (h "0dfp85j27n53vlwxrjjf4yni4i2a0bn2jn1mny5szh20ahbf9ghc") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.98 (c (n "musli-allocator") (v "0.0.98") (d (list (d (n "musli") (r "^0.0.98") (k 0)))) (h "0qbfc23xdir1kc7wix614i2nj0zyqn8apwc1x26yp8ik6pdm9dbn") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.99 (c (n "musli-allocator") (v "0.0.99") (d (list (d (n "musli") (r "^0.0.99") (k 0)))) (h "0ssl7f4f0gj8770q9db80vp1gyf2cs6f8lyf7mbimrkrzmmqd5r3") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.100 (c (n "musli-allocator") (v "0.0.100") (d (list (d (n "musli") (r "^0.0.100") (k 0)))) (h "0ck2af5zyzvqk55r7g161rpm7pnpf29qanbdmqa89i7sizriv5wh") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.101 (c (n "musli-allocator") (v "0.0.101") (d (list (d (n "musli") (r "^0.0.101") (k 0)))) (h "104qiaby10vf4wfm9jh7zpw3220y8nskf2ndwmg7fjwvn5r1bsa9") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.102 (c (n "musli-allocator") (v "0.0.102") (d (list (d (n "musli") (r "^0.0.102") (k 0)))) (h "1f6bgk2d2yifxagci268326bppmbwavj90xv5r4xva80ycsqkgc6") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.103 (c (n "musli-allocator") (v "0.0.103") (d (list (d (n "musli") (r "^0.0.103") (k 0)))) (h "105p7z8qrb0cqqvygshz13rz93islpkm0fbpiwzb2xgkbd798gbn") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.104 (c (n "musli-allocator") (v "0.0.104") (d (list (d (n "musli") (r "^0.0.104") (k 0)))) (h "06qvs4g6prb3478srxvjm8lqmcdw6n1sfwih105gvqyq5faamvqy") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.105 (c (n "musli-allocator") (v "0.0.105") (d (list (d (n "musli") (r "^0.0.105") (k 0)))) (h "02sgp8phja3cvfmyy7sfzk6wli7lyi0gqp8zqwi8gqzaxgr12018") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.106 (c (n "musli-allocator") (v "0.0.106") (d (list (d (n "musli") (r "^0.0.106") (k 0)))) (h "1zzq77i6jy059jg0x552fqcy3qcmwkyxyc6h4qj6w0fz26h1w8ai") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.107 (c (n "musli-allocator") (v "0.0.107") (d (list (d (n "musli") (r "^0.0.107") (k 0)))) (h "14b1rj8kxg9g91814h9y81nax40n96f0c36sm5rrixbajvvci2wl") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.108 (c (n "musli-allocator") (v "0.0.108") (d (list (d (n "musli") (r "^0.0.108") (k 0)))) (h "0wfl4qswlqvmk54wgg6m3ns3rvisvgbs3rg0i870028m3pyanc4s") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.109 (c (n "musli-allocator") (v "0.0.109") (d (list (d (n "musli") (r "^0.0.109") (k 0)))) (h "0dxsa61w0sl74z9awqw20dkgghrznxv25y3f64wwfsi9g72rc13y") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.110 (c (n "musli-allocator") (v "0.0.110") (d (list (d (n "musli") (r "^0.0.110") (k 0)))) (h "02p5dp9swbkxiny1nvf1vg1h0cflfbbcpdnzcx2p0xk132czqad8") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.111 (c (n "musli-allocator") (v "0.0.111") (d (list (d (n "musli") (r "^0.0.111") (k 0)))) (h "1all04amsih0maz06is1fs7ghxssix9i4bv24l9fjwa6hihdja6z") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.112 (c (n "musli-allocator") (v "0.0.112") (d (list (d (n "musli") (r "^0.0.112") (k 0)))) (h "06bjvci9qrqh6kvkhfy9crkj8rziabim3zq753ars7y1li235a3d") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.113 (c (n "musli-allocator") (v "0.0.113") (d (list (d (n "musli") (r "^0.0.113") (k 0)))) (h "110crw3qipyrsgir55zvp6wcpx0x9a1ff67rqh1xizmclm14czh4") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.114 (c (n "musli-allocator") (v "0.0.114") (d (list (d (n "musli") (r "^0.0.114") (k 0)))) (h "0rx1yl9v64yl132hqs957xpsx07vybdzw519bannbc87rkibj9hh") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.115 (c (n "musli-allocator") (v "0.0.115") (d (list (d (n "musli") (r "^0.0.115") (k 0)))) (h "03yrsfrf57r2d1ynnx2hj3fqwr8k9a2nkbjgq67av3zbs7czd5xd") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.116 (c (n "musli-allocator") (v "0.0.116") (d (list (d (n "musli") (r "^0.0.116") (k 0)))) (h "0bn6zzajx52wqr5gyy6af0fzi53ackvx9xv69zyw13yjdwjbphg9") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

(define-public crate-musli-allocator-0.0.117 (c (n "musli-allocator") (v "0.0.117") (d (list (d (n "musli") (r "^0.0.117") (k 0)))) (h "19bfgwhybdbc8j7zg8iyb10m5nvr0hjdq3cg9vm6zskw5vqi1xln") (f (quote (("std" "musli/std") ("default" "std" "alloc") ("alloc" "musli/alloc"))))))

