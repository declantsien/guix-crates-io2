(define-module (crates-io mu sl musli-zerocopy-macros) #:use-module (crates-io))

(define-public crate-musli-zerocopy-macros-0.0.86 (c (n "musli-zerocopy-macros") (v "0.0.86") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09lnfv6d0iwi6qxw98bvl7w9c7d3n4aw1hfs5azs75pmqaw2w14v") (f (quote (("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-zerocopy-macros-0.0.87 (c (n "musli-zerocopy-macros") (v "0.0.87") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0aalidfw118h9h5w4z61wp0amm4z6lzk5wiyrkbriny6rliraib0") (f (quote (("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-zerocopy-macros-0.0.88 (c (n "musli-zerocopy-macros") (v "0.0.88") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "014y1izg6gikgfaxlm2083sxgjk7y4lzlkpx7645w86q6hq0yrlh") (f (quote (("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-zerocopy-macros-0.0.89 (c (n "musli-zerocopy-macros") (v "0.0.89") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pp41dwnicg26j1wad3j5a402p0jvazdrz4j10p0gh5hh6fhbjgl") (f (quote (("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-zerocopy-macros-0.0.90 (c (n "musli-zerocopy-macros") (v "0.0.90") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s63byl91ags396127xzlgvdqic6hsh98zbkzimrxjj58z2pc0d3") (f (quote (("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-zerocopy-macros-0.0.91 (c (n "musli-zerocopy-macros") (v "0.0.91") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1f81ygbrz8yrl679d4hqx4pgdhwlh60a2d6dgwd0f484gdcx1xl1") (f (quote (("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-zerocopy-macros-0.0.92 (c (n "musli-zerocopy-macros") (v "0.0.92") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1c45p1r6v9svf1ik8vhs544j8835667z5ddzlpw9m1hza45p7hsc") (f (quote (("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-zerocopy-macros-0.0.93 (c (n "musli-zerocopy-macros") (v "0.0.93") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mb1nlpqj0rmqh45i70f8yi84sskdm90g21rg26w694sc8dpx610") (f (quote (("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-zerocopy-macros-0.0.94 (c (n "musli-zerocopy-macros") (v "0.0.94") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d3vmrfa4zbg12rv7m4j45adga7yj01vw6mh9s5in89ykwz04621") (f (quote (("sneaky-fields")))) (r "1.73")))

(define-public crate-musli-zerocopy-macros-0.0.95 (c (n "musli-zerocopy-macros") (v "0.0.95") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ndzgxj4kkkqmijrgirpghsryq3grkswakfvarg0xl9pa5y46pl6") (f (quote (("sneaky-fields")))) (r "1.67")))

(define-public crate-musli-zerocopy-macros-0.0.96 (c (n "musli-zerocopy-macros") (v "0.0.96") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11xic741ylddl9y5vhk6pp4shp9y034bf38aw0a19xjcjxmcn2cs") (f (quote (("sneaky-fields")))) (r "1.67")))

(define-public crate-musli-zerocopy-macros-0.0.97 (c (n "musli-zerocopy-macros") (v "0.0.97") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rsqbddr21yackx9x3xlisr6mhc8kakbanrxip74pir1kj2sqmgg") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.98 (c (n "musli-zerocopy-macros") (v "0.0.98") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b2dri9wqqyxn1w3d0mdvzgpvm986r1zjc7751zx8wkwnzpj6yqa") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.99 (c (n "musli-zerocopy-macros") (v "0.0.99") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dm41rfn2aw7zh76ab0rhq2p8bq3wvvjd6f8y0pc5mc1w32s1mgs") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.100 (c (n "musli-zerocopy-macros") (v "0.0.100") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nix9yakz71zjiy503nhgjrd6n9qp5sqsdj3yzclsypj7dygq28g") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.101 (c (n "musli-zerocopy-macros") (v "0.0.101") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18rifj610id5n2s47q7ijrl1l6qmx2yc1fahhllb6xrilf9s5gbd") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.102 (c (n "musli-zerocopy-macros") (v "0.0.102") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0crgc7zv3pawl4wwyki1cs2488j7alm8wk916rjj0q84zczw3r8d") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.103 (c (n "musli-zerocopy-macros") (v "0.0.103") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qmc7ncbg5w2maqxi2j1dakq0h5r5gl5ndcw8wbcbh5lf5i7nlbf") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.104 (c (n "musli-zerocopy-macros") (v "0.0.104") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i3acdp247frj0b48a6yp6gc7bqql9a2ai98cnga2xf2nlkywmxm") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.105 (c (n "musli-zerocopy-macros") (v "0.0.105") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11szf6l99ljf4z2dn7f5sxry9a1xh36m2n1jmw8p9n80wh036mwb") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.106 (c (n "musli-zerocopy-macros") (v "0.0.106") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jmvi3m4qqb2wmqswz2c183byhiyy617mi17a276bp2p3wlhp19b") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.107 (c (n "musli-zerocopy-macros") (v "0.0.107") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01q3xsmc2rxqizd4g22njadcz2q7kd43slb3kf181bi4gdfg1kjw") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.108 (c (n "musli-zerocopy-macros") (v "0.0.108") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l7657ac8kh6r5jw53ji4wk9b8cm9xxqcsbrg8zm6j048q1xqk0v") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.109 (c (n "musli-zerocopy-macros") (v "0.0.109") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dcaipwvl5sj0z9nkvz6zvbm8nd9h1b7xa7ahp945m62ghgabg97") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.110 (c (n "musli-zerocopy-macros") (v "0.0.110") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wn7kf153kj2d91sx6nylirfgfjb6l6jld52xnqqkxk9knc9pmym") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.111 (c (n "musli-zerocopy-macros") (v "0.0.111") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y37pc2frc5cs53bv6hh2a813k12ly47kkb54j5xxcfibhx494zm") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.112 (c (n "musli-zerocopy-macros") (v "0.0.112") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05n10ksvbqzfgqhs0d46812qa4pc25xqfd5g8zb67zdbfyrb6lcb") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.113 (c (n "musli-zerocopy-macros") (v "0.0.113") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08d1gy7gkrz63ylbcsxkygn3akbn4qcqq6imjgj7gyjfppgjndp2") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.114 (c (n "musli-zerocopy-macros") (v "0.0.114") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rwvbwsxrz10l6x2d4c5n77yzxamirc5dw7s366z2d22hzqc5d10") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.115 (c (n "musli-zerocopy-macros") (v "0.0.115") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jdhgab4yq27hg06x5cwc45a98vwzn7p4yfy86263cpnaishdikr") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.116 (c (n "musli-zerocopy-macros") (v "0.0.116") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dygnjc0vm6nkk7g9v969bsc1gppbyfxwy352s5k1y42m0azg49d") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.117 (c (n "musli-zerocopy-macros") (v "0.0.117") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yqkxpfdsfh14g4s6wyv4ab38b1grs2jhhvbhk3ska4m96xqnygy") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.118 (c (n "musli-zerocopy-macros") (v "0.0.118") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15l6dbv1rsz1lsl8snn7vd3cigdv7jci9gjjlfywxxshsismlffr") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.119 (c (n "musli-zerocopy-macros") (v "0.0.119") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hyhw57xjsp7qcbkkzzgnrfncdfnwd3ch12hdf9m0sffhmppawpl") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.120 (c (n "musli-zerocopy-macros") (v "0.0.120") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l54rwyd5x0i9ipab38crjhph2154h0bymjw5npdv4jxvs0pfg04") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.121 (c (n "musli-zerocopy-macros") (v "0.0.121") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18ap6alrxfw6byfxr5qrvcyidkdz5z7zg1yq5vkc3gc3r1x8fc1b") (f (quote (("sneaky-fields")))) (r "1.76")))

(define-public crate-musli-zerocopy-macros-0.0.122 (c (n "musli-zerocopy-macros") (v "0.0.122") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b2spdzc7v22jw2dzs9bx7k61bjwvmbyw9g3ykydch8dsnbq91vj") (f (quote (("sneaky-fields")))) (r "1.76")))

