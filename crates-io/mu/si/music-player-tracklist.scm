(define-module (crates-io mu si music-player-tracklist) #:use-module (crates-io))

(define-public crate-music-player-tracklist-0.1.0 (c (n "music-player-tracklist") (v "0.1.0") (d (list (d (n "atlist-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "music-player-entity") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0n3lqlgshykwgc560zch5pqiy2wa89nkigw7pr5lgyzy3vd2qn9d")))

(define-public crate-music-player-tracklist-0.1.1 (c (n "music-player-tracklist") (v "0.1.1") (d (list (d (n "atlist-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "music-player-entity") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0a44k09903kcrp4vsl55jmmjb2jxk6s9wn9cnkxz9wzgmgia47gd")))

(define-public crate-music-player-tracklist-0.1.2 (c (n "music-player-tracklist") (v "0.1.2") (d (list (d (n "atlist-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "music-player-entity") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zpypkz6yabp0717vjnp2cvwjdrk7bkzqv8gkm8d730g56n297hq")))

(define-public crate-music-player-tracklist-0.1.3 (c (n "music-player-tracklist") (v "0.1.3") (d (list (d (n "atlist-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "music-player-entity") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0yqvafipvsicw5lhdpbms09p0lp7njn405ypzi22gjjz2l41l8wj")))

(define-public crate-music-player-tracklist-0.1.4 (c (n "music-player-tracklist") (v "0.1.4") (d (list (d (n "atlist-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "music-player-entity") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1w8cvyjjr5ankm1yhg2wj3hs7ffniyfgl138j477am03vxc47yc7")))

(define-public crate-music-player-tracklist-0.1.5 (c (n "music-player-tracklist") (v "0.1.5") (d (list (d (n "atlist-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "music-player-entity") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10b9mkx3g7f0p37h7zscw4bi19gsafzgzgyp8lhd9mc032gmh7aa")))

(define-public crate-music-player-tracklist-0.1.6 (c (n "music-player-tracklist") (v "0.1.6") (d (list (d (n "atlist-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "music-player-entity") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1amwm87fvx2nq66mkhznvbwgqh5iqv5w7s2ilan33h48kvmdj1kl")))

(define-public crate-music-player-tracklist-0.1.7 (c (n "music-player-tracklist") (v "0.1.7") (d (list (d (n "atlist-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "music-player-entity") (r "^0.1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rrjn2n5bvfkirv39a26vcbifdzpnzr3xvxnv5cbdha3lrkrsj4y")))

(define-public crate-music-player-tracklist-0.1.8 (c (n "music-player-tracklist") (v "0.1.8") (d (list (d (n "atlist-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "music-player-entity") (r "^0.1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1l9paf2q2812i1w9gjk228n3kxzjmr59asni86pjrjb301nvfh61")))

