(define-module (crates-io mu si musical_scales) #:use-module (crates-io))

(define-public crate-musical_scales-0.1.0 (c (n "musical_scales") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "14hn267yhycg5hmjxq253bx1ca7jrx26n2q4zkkd7f1z2fr5pi4x")))

(define-public crate-musical_scales-0.2.0 (c (n "musical_scales") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "0vz8dgr10b4wm0crxwp20hk4hkr0mrbjb9kdm03ac6lb7vsmdaqb")))

