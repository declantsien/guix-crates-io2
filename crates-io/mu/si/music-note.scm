(define-module (crates-io mu si music-note) #:use-module (crates-io))

(define-public crate-music-note-0.1.0 (c (n "music-note") (v "0.1.0") (h "1axlb2c912xgp1mlnlgr8wm2gpzfy0mxkncfbvgxdxqnwq7v2cvk")))

(define-public crate-music-note-0.2.0 (c (n "music-note") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "17dcc2qkcdn34mivsvgi22qz8l55b25pgh6zv40j5r285x83bvhz")))

(define-public crate-music-note-0.3.0 (c (n "music-note") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0pqzmq9xg40xwm9mf4vazfzq5bj9vbz4mai1sbnbjkk9nwqrxdhw")))

(define-public crate-music-note-0.3.1 (c (n "music-note") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0m2r8xglgmjxzan81rh9h4cd9nqmwcl6z9gijrik7r64f3wp0bbf")))

