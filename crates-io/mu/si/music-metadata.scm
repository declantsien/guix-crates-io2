(define-module (crates-io mu si music-metadata) #:use-module (crates-io))

(define-public crate-music-metadata-0.1.0 (c (n "music-metadata") (v "0.1.0") (h "1xvcvfyr04nwwv42qx2wlihx5fyzgdd05yk3scdy7pi79ws8x6lf")))

(define-public crate-music-metadata-0.1.1 (c (n "music-metadata") (v "0.1.1") (h "04v7wxkqp7gwipi21bzcwk305mnrp4p089z1y246f2gbpdl6zg35")))

(define-public crate-music-metadata-0.1.2 (c (n "music-metadata") (v "0.1.2") (h "1bs3q594907iiyg5jdkpx8wajg613lznnfvx1qx3876pzbfwhkad")))

(define-public crate-music-metadata-0.2.0 (c (n "music-metadata") (v "0.2.0") (h "158pfiygkrqnxai3ich31al9jcikb6qm7d57fbzw80abzzcy33y1")))

(define-public crate-music-metadata-0.3.0 (c (n "music-metadata") (v "0.3.0") (h "1v28xcw1rgsbn5lkjaawgynlsp45a2dhk3wr3dykzf4lm8ppqkf8")))

