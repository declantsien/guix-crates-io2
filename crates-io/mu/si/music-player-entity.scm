(define-module (crates-io mu si music-player-entity) #:use-module (crates-io))

(define-public crate-music-player-entity-0.1.0 (c (n "music-player-entity") (v "0.1.0") (d (list (d (n "sea-orm") (r "^0.9.2") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)))) (h "11a1gqxlj9kczgj0qxj3d5scllmf9r5035n20j3s08c491qmlyc0")))

(define-public crate-music-player-entity-0.1.1 (c (n "music-player-entity") (v "0.1.1") (d (list (d (n "sea-orm") (r "^0.9.2") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)))) (h "1rskzz01jcr2sv2y03r4mv1ichsww1jabjcscmxsfy13v86bjf6h")))

(define-public crate-music-player-entity-0.1.2 (c (n "music-player-entity") (v "0.1.2") (d (list (d (n "sea-orm") (r "^0.9.2") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)))) (h "00z9r80vnm5jc8gqap85gxj6kmxr3n1pbk3x8wk6jbs7xgbr883k")))

(define-public crate-music-player-entity-0.1.3 (c (n "music-player-entity") (v "0.1.3") (d (list (d (n "sea-orm") (r "^0.9.2") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)))) (h "1ck4x0vgk8w94ffdgkwnqbagl5f6d2lwqnsw4l88xz5ilkqxpz58")))

(define-public crate-music-player-entity-0.1.4 (c (n "music-player-entity") (v "0.1.4") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "music-player-types") (r "^0.1.0") (d #t) (k 0)) (d (n "sea-orm") (r "^0.9.2") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)))) (h "1dg6fx9s3493gywz8rsjysb8gvpb25955cyww3kil849jxdxwnra")))

(define-public crate-music-player-entity-0.1.5 (c (n "music-player-entity") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "music-player-types") (r "^0.1.1") (d #t) (k 0)) (d (n "sea-orm") (r "^0.9.2") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)))) (h "1x58v86i9a6im3ph9ky4aa5p13dxzplpdqm2i2f4rmdly2s7sx3x")))

(define-public crate-music-player-entity-0.1.6 (c (n "music-player-entity") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "music-player-types") (r "^0.1.2") (d #t) (k 0)) (d (n "sea-orm") (r "^0.9.2") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)))) (h "1wwdlszkp8372cr9kc1z7a5f4ak946v4r0iq57yg7zcwfyafjwp9")))

(define-public crate-music-player-entity-0.1.7 (c (n "music-player-entity") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "music-player-types") (r "^0.1.3") (d #t) (k 0)) (d (n "sea-orm") (r "^0.9.2") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "upnp-client") (r "^0.1.6") (d #t) (k 0)))) (h "0lhgysij97isvxqxr506y25b4nhmma797g9nwa9f3j41ia2nw5rp")))

