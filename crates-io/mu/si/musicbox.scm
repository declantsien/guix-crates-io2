(define-module (crates-io mu si musicbox) #:use-module (crates-io))

(define-public crate-musicbox-0.1.0 (c (n "musicbox") (v "0.1.0") (h "0970j8s701rw68lybxxplw3mqzi8waj95c60bywggpggljnzh1x9")))

(define-public crate-musicbox-0.1.1 (c (n "musicbox") (v "0.1.1") (h "0lwwqbp9fyrvn94k0kd7nn5c77i9h6x5kbfbcb0ijnbfillj8dl0")))

(define-public crate-musicbox-0.1.3 (c (n "musicbox") (v "0.1.3") (h "09y49fnin70fhwm17xay1gympgxw7a6c48x531zzmfjn9wz57k4k")))

(define-public crate-musicbox-0.1.4 (c (n "musicbox") (v "0.1.4") (h "1i1f727ncmvxi1yr3kvvg8akkd8bqyaa59dl8czh0mac9fsh1ryj")))

(define-public crate-musicbox-0.1.5 (c (n "musicbox") (v "0.1.5") (h "05b8yhsapfc7xx8hiablv9i014ck4yd9l2fwr8ha5fsgifg09zm0")))

(define-public crate-musicbox-0.1.6 (c (n "musicbox") (v "0.1.6") (h "1xh18ifks8gsgqal7m32wpfb5d8zq150q8rgxs63c5iw7gzwcdyi")))

(define-public crate-musicbox-0.1.7 (c (n "musicbox") (v "0.1.7") (h "0j7zdnis6xp34z3p5mqjxssrkixkj8j5vzw9bi0c6fbh37r92h3a")))

(define-public crate-musicbox-0.2.0 (c (n "musicbox") (v "0.2.0") (h "16vi9scl3r97xlwvb1y96q25za255xcxb61f3mwqqwm5wnbiw7qv")))

(define-public crate-musicbox-0.2.1 (c (n "musicbox") (v "0.2.1") (h "1l2sj3mbf8155rmm745pp29b9g74yi1lh6rk1qnlzcvi3q9fygf6")))

(define-public crate-musicbox-0.2.2 (c (n "musicbox") (v "0.2.2") (h "0g8hqiffzaa2hrcq4p647c7ap04b0m06qfypz7xjg10idqcg6phy")))

(define-public crate-musicbox-0.3.0 (c (n "musicbox") (v "0.3.0") (h "0s36w9ndv0f45w39zsvbzaqmb46amxkzww4d5mqlm6i58fpdigby")))

