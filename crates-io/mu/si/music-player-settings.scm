(define-module (crates-io mu si music-player-settings) #:use-module (crates-io))

(define-public crate-music-player-settings-0.1.0 (c (n "music-player-settings") (v "0.1.0") (d (list (d (n "config") (r "^0.13.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1w828ybn560gvi3nl6arahk05cs3c2xass56ysa5h6k8bh35vr3c")))

(define-public crate-music-player-settings-0.1.1 (c (n "music-player-settings") (v "0.1.1") (d (list (d (n "config") (r "^0.13.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0rwxzs8a6dy853kjlcwij9l4z9fg60gcpl96klrmksbdp79xplpk")))

(define-public crate-music-player-settings-0.1.2 (c (n "music-player-settings") (v "0.1.2") (d (list (d (n "config") (r "^0.13.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1f9110pc1l8wlfj20jpcx2pszj5bay1a88k8haij7jhnq03ckv6r")))

(define-public crate-music-player-settings-0.1.3 (c (n "music-player-settings") (v "0.1.3") (d (list (d (n "config") (r "^0.13.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0f9655612izwxr4c0ychfsb5dmmp2xmzkfl0qlwhalcqvas8jxag")))

(define-public crate-music-player-settings-0.1.4 (c (n "music-player-settings") (v "0.1.4") (d (list (d (n "config") (r "^0.13.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vfhfa5byv1lspvqknha40cwr7cmm0r09z71ihjxyx6by22i6sgk")))

(define-public crate-music-player-settings-0.1.5 (c (n "music-player-settings") (v "0.1.5") (d (list (d (n "config") (r "^0.13.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1j36f911mg6czimpbbdd5q5h3nr2nfc071sbhqrf4gjg4clpxywd")))

(define-public crate-music-player-settings-0.1.6 (c (n "music-player-settings") (v "0.1.6") (d (list (d (n "config") (r "^0.13.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1k2a36c9hb9vjfrwn5az319jmqvlfa2kaawpw63x82py3lm0q8ql")))

