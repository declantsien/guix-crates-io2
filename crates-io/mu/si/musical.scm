(define-module (crates-io mu si musical) #:use-module (crates-io))

(define-public crate-musical-0.0.1 (c (n "musical") (v "0.0.1") (d (list (d (n "soloud") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1nvaikxb7b6d3ri1q5qs80y6ncj23y0njg60zjxjm7im80h2jkca")))

