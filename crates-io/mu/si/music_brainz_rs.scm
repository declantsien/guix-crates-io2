(define-module (crates-io mu si music_brainz_rs) #:use-module (crates-io))

(define-public crate-music_brainz_rs-0.1.0 (c (n "music_brainz_rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qnsplmxf0b73bi2bdd9ryhy9zy6wh01l0rslz1jc936vxdsy5zj") (y #t)))

(define-public crate-music_brainz_rs-0.1.1 (c (n "music_brainz_rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sypp0s8x6v0n31fhw197cd0yv3jdjkz3fcb02y0ha96id0kdf43") (y #t)))

(define-public crate-music_brainz_rs-0.1.2 (c (n "music_brainz_rs") (v "0.1.2") (h "0rd4v74c4zqpfdmdlcnr1knicv76bzfbz2bvbp2mq76n4hbfyzfi") (y #t)))

