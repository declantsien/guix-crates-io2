(define-module (crates-io mu si music-math) #:use-module (crates-io))

(define-public crate-music-math-0.1.0 (c (n "music-math") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1pfv251gy2x6zdssal3a3nbmm0wgin9dgrddzd6lr83d583iswj1") (f (quote (("full") ("default")))) (r "1.56.0")))

(define-public crate-music-math-0.1.1 (c (n "music-math") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0l00jqqnh2cbvy2fkqmp1w6q9c9sqgy9d832yiqidmzcg7i7ax4a") (f (quote (("full") ("default")))) (r "1.56.0")))

