(define-module (crates-io mu si music-timer) #:use-module (crates-io))

(define-public crate-music-timer-0.1.1 (c (n "music-timer") (v "0.1.1") (h "01wz88yiwgil4cdbmn6j88vz8mj5lsvwm576kd7mv7r44sh85g5a")))

(define-public crate-music-timer-0.1.2 (c (n "music-timer") (v "0.1.2") (h "000z89l1cpa5s0y70w6vwcqzcjqdk77svwij45001xq4l15g2ihp")))

(define-public crate-music-timer-0.1.3 (c (n "music-timer") (v "0.1.3") (h "07yiv1hqxzn7ydnl0abgdparbkqi9lmp26s0hziibxvq6m18p0zj")))

(define-public crate-music-timer-0.1.4 (c (n "music-timer") (v "0.1.4") (h "0gmxydp2pznmgkmw9h4hq87fg0j50q5v6yf0d3abi0lckavqfsjw")))

(define-public crate-music-timer-0.1.5 (c (n "music-timer") (v "0.1.5") (h "033f2abbn5vx62ml56hk5lly7axjrdmyb8k2fmfm0l2y1kxf8vb6")))

(define-public crate-music-timer-0.1.6 (c (n "music-timer") (v "0.1.6") (h "1bv4bp59bl0mrwv8yj38fwg19bpdkmz90kp918q2lx62db3riimv")))

