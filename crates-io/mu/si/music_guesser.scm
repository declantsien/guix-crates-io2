(define-module (crates-io mu si music_guesser) #:use-module (crates-io))

(define-public crate-music_guesser-1.0.0 (c (n "music_guesser") (v "1.0.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "gdk") (r "^0.15.4") (d #t) (k 0)) (d (n "glib") (r "^0.15.12") (d #t) (k 0)) (d (n "gtk") (r "^0.15.5") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1iw87hrdqdj5x0k1mghkxaak26wjrh0w6bjh4zqlmy63nyj6m0p8")))

(define-public crate-music_guesser-1.0.1 (c (n "music_guesser") (v "1.0.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "gdk") (r "^0.15.4") (d #t) (k 0)) (d (n "glib") (r "^0.15.12") (d #t) (k 0)) (d (n "gtk") (r "^0.15.5") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xjif8cjf15vk17x6za9pz7rj2mg8yv9ry94wdfd8zvwp87zjbsa")))

(define-public crate-music_guesser-1.0.2 (c (n "music_guesser") (v "1.0.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "gdk") (r "^0.15.4") (d #t) (k 0)) (d (n "glib") (r "^0.15.12") (d #t) (k 0)) (d (n "gtk") (r "^0.15.5") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07bdzh1r5gpikrpskkf6bhv9n4q6689lh07lg0rppqqhi9kgsffg")))

(define-public crate-music_guesser-1.0.3 (c (n "music_guesser") (v "1.0.3") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "gdk") (r "^0.15.4") (d #t) (k 0)) (d (n "glib") (r "^0.15.12") (d #t) (k 0)) (d (n "gtk") (r "^0.15.5") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xmd3yl624qpb8g2jb5zlkhcmg77cl6vp6ikxvjca9v5hnpm220g")))

