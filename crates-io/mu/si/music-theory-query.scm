(define-module (crates-io mu si music-theory-query) #:use-module (crates-io))

(define-public crate-music-theory-query-0.1.0 (c (n "music-theory-query") (v "0.1.0") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "1yi3cxhcnaidnhf5697fyd60a2rj761nw8066svwny858w53w2xn") (y #t)))

(define-public crate-music-theory-query-0.1.1 (c (n "music-theory-query") (v "0.1.1") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "0nm2sr907k1vwppsnq716ia7mkv0r4qlf1m5k4b3qxk3vywcdzv3") (y #t)))

(define-public crate-music-theory-query-0.1.2 (c (n "music-theory-query") (v "0.1.2") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "0gc747crr4d634zbfg3nd897jjq8f2vfv59xpsldlgr2k7fcd3bs") (y #t)))

(define-public crate-music-theory-query-0.1.3 (c (n "music-theory-query") (v "0.1.3") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "1z408sd5iy3f98rzgqhbdp32agw0lfvq92xlqmi4snlxms4a20i9") (y #t)))

(define-public crate-music-theory-query-0.1.4 (c (n "music-theory-query") (v "0.1.4") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "07al7p2pi7v2g6dn2zjvcw58qp5hdhc0vraw1p04bwr5i0an7m8g") (y #t)))

(define-public crate-music-theory-query-0.1.5 (c (n "music-theory-query") (v "0.1.5") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "1qr5i3yr2igc0ncw10hkf1zqklbi0pbrdjnhpwqnim2rh2zdw431") (y #t)))

(define-public crate-music-theory-query-0.1.6 (c (n "music-theory-query") (v "0.1.6") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "139cgjkw8v4yvypa7g1fp64b28rf6xyzyfpfdhxdkg9544y8gnq9") (y #t)))

(define-public crate-music-theory-query-0.1.7 (c (n "music-theory-query") (v "0.1.7") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "18g9zijak9wkgx0ja4l7yp68c0j1gw2bc2zqmr4lpns7l5c1mlk4") (y #t)))

(define-public crate-music-theory-query-0.1.8 (c (n "music-theory-query") (v "0.1.8") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "19n3ykpdafcpsv2wc7a9inx4sl9gbpynrlba4v1wb0fjl56djhb5") (y #t)))

(define-public crate-music-theory-query-0.1.9 (c (n "music-theory-query") (v "0.1.9") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "12wargzh9zkfbncl38rpfgdyb6lxpmh281ms1i0s5hnriwn8dhf6") (y #t)))

(define-public crate-music-theory-query-0.1.10 (c (n "music-theory-query") (v "0.1.10") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "1ay8vmvaacc1dxwzwi3ndmk100yig15b27g8cyqk5vmp3q0qsidx") (y #t)))

(define-public crate-music-theory-query-0.1.11 (c (n "music-theory-query") (v "0.1.11") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "1ak7ib2c66izdzljm6kyhxlyk7w26nsmw7jp5zdxqac1h3g1fzcz") (y #t)))

(define-public crate-music-theory-query-0.1.12 (c (n "music-theory-query") (v "0.1.12") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "0w3dh00h8dw9dhyr655ib38v257nvymcvl0n9lxcnkzks3ydlg23") (y #t)))

(define-public crate-music-theory-query-0.1.13 (c (n "music-theory-query") (v "0.1.13") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "0nflrym5qq52s10nsrirkndqpihi5l1bxccn8b4r0kgg3pjph1vy")))

(define-public crate-music-theory-query-0.1.14 (c (n "music-theory-query") (v "0.1.14") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "0204ba4ny730sj3b7grmysjfnmwpp84l8jb3nl14qc3y0l9b9qyy") (y #t)))

(define-public crate-music-theory-query-0.1.15 (c (n "music-theory-query") (v "0.1.15") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)))) (h "0pnxgz8211a2kg75rpm3rd7jpd0x4kpqywbx20y9c30rxbh11vx5") (y #t)))

(define-public crate-music-theory-query-0.2.0 (c (n "music-theory-query") (v "0.2.0") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)) (d (n "vec-string") (r "^0.1.1") (d #t) (k 0)))) (h "0nxrb5y717q5xpimbsfa4ll6ka8jiirdwqxi49zc5l9fsj0jjkbn") (y #t)))

(define-public crate-music-theory-query-0.2.1 (c (n "music-theory-query") (v "0.2.1") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)) (d (n "vec-string") (r "^0.1.1") (d #t) (k 0)))) (h "01mnj0zixnri80c7gfarbxmggyqyj1rw9w3v589lxygxvgaq51kk") (y #t)))

(define-public crate-music-theory-query-0.2.2 (c (n "music-theory-query") (v "0.2.2") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)) (d (n "vec-string") (r "^0.1.1") (d #t) (k 0)))) (h "0fsqwgnis66fchnp73ficr76jb6jy90qqmnvcx28xcyqjf1d4ipc")))

(define-public crate-music-theory-query-0.2.3 (c (n "music-theory-query") (v "0.2.3") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)) (d (n "vec-string") (r "^0.1.1") (d #t) (k 0)))) (h "0japr65j6gjsvzn2gld42jmqjpnyfqvzww67hgdkw3b6w9laaprq")))

(define-public crate-music-theory-query-1.0.0 (c (n "music-theory-query") (v "1.0.0") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)) (d (n "vec-string") (r "^0.1.1") (d #t) (k 0)))) (h "1mbi92y9qqfp1ikd9ns28p9yca6464mw3n118lpi179ni37w49iv")))

(define-public crate-music-theory-query-1.0.1 (c (n "music-theory-query") (v "1.0.1") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)) (d (n "vec-string") (r "^0.1.1") (d #t) (k 0)))) (h "0gi8kafdxx2vm0nbcrmwnvwg7vdbsz72n8a4rgv05i0z9bkbh9aa")))

(define-public crate-music-theory-query-1.0.2 (c (n "music-theory-query") (v "1.0.2") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)) (d (n "vec-string") (r "^0.1.1") (d #t) (k 0)))) (h "0fj8mbvlp9yqbk7679sk73fbph5vj3p0bgpf7g24hcca8vxwff1k")))

(define-public crate-music-theory-query-1.1.0 (c (n "music-theory-query") (v "1.1.0") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)) (d (n "vec-string") (r "^0.1.1") (d #t) (k 0)))) (h "16xvlpxnj296gvw4wbr2fip8091xh110n7b645yzrzsyw0iykqmf")))

(define-public crate-music-theory-query-1.1.1 (c (n "music-theory-query") (v "1.1.1") (d (list (d (n "fnrs") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.13.0") (d #t) (k 0)) (d (n "lapp") (r "^0.4.0") (d #t) (k 0)) (d (n "vec-string") (r "^0.2.0") (d #t) (k 0)))) (h "187rclqn5k6490ax7wyp2nbw3a15ja77s1zrq9lvwwnnmqsm2g6l")))

