(define-module (crates-io mu si musical-note) #:use-module (crates-io))

(define-public crate-musical-note-0.1.0 (c (n "musical-note") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1mka7r3an9nxkv7pbm4nsqjcszbv44w48xgv81fqiw1fhq855jmd")))

(define-public crate-musical-note-0.1.10 (c (n "musical-note") (v "0.1.10") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vrq5rivkhxca9dgalvgikqada4ad3mibv86z4mljsfx3hzadl9w")))

(define-public crate-musical-note-0.1.101 (c (n "musical-note") (v "0.1.101") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "19qp32sayy8i0dw526dcna1xv6xg49rcsb3gma762g16c776lrjg")))

(define-public crate-musical-note-0.1.102 (c (n "musical-note") (v "0.1.102") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0v24ikvrhbw3gynzw9l4zn0459qlvghcyla560150l4gn7s8g4lp")))

(define-public crate-musical-note-0.1.103 (c (n "musical-note") (v "0.1.103") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xqy47iajlm5nd65ihhxsnh7gcybwd2lmqc6w00dhw1cc9kdichc")))

(define-public crate-musical-note-0.1.104 (c (n "musical-note") (v "0.1.104") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rw4sk4bw722adywvywzm6gfxs73ml823hkixxh8ndzqx5ak1fip")))

(define-public crate-musical-note-0.1.105 (c (n "musical-note") (v "0.1.105") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16kjipdzg618zgmyjii73anj48s2r9f3zgpayhi37q80sg4yimma")))

