(define-module (crates-io mu si music-lounge-client) #:use-module (crates-io))

(define-public crate-music-lounge-client-0.6.0 (c (n "music-lounge-client") (v "0.6.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "music-lounge-core") (r "^0") (f (quote ("player"))) (d #t) (k 0)) (d (n "rust-utils") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0n7ca5lq0myiyb5bmgaq8alxrrrkpq9m4jjvvhmkhl71wfppjr55")))

(define-public crate-music-lounge-client-0.6.1 (c (n "music-lounge-client") (v "0.6.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "music-lounge-core") (r "^0") (f (quote ("player"))) (d #t) (k 0)) (d (n "rust-utils") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1lh51vspwrjapbk5x77pb05z9piijkgcy1spzyzaa73b6vci669v")))

(define-public crate-music-lounge-client-0.7.0 (c (n "music-lounge-client") (v "0.7.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "music-lounge-core") (r "^0") (f (quote ("client"))) (d #t) (k 0)) (d (n "rust-utils") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1q7zzjrgmzr9iq4fy3bj9s9qldryhm6sf3v2869mh5zrrmzslgr7")))

