(define-module (crates-io mu si musixmatch) #:use-module (crates-io))

(define-public crate-musixmatch-0.1.0 (c (n "musixmatch") (v "0.1.0") (d (list (d (n "api-request-utils-rs") (r "^0.1.8") (d #t) (k 0)) (d (n "default-args") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1gy92lfsj1gqg7nl6alk8hvn139n7pr4qyc3fyy1mby5qqc2i9py") (y #t) (s 2) (e (quote (("marcos" "dep:default-args"))))))

(define-public crate-musixmatch-0.1.1 (c (n "musixmatch") (v "0.1.1") (d (list (d (n "api-request-utils-rs") (r "^0.1.8") (d #t) (k 0)) (d (n "default-args") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0shhrv493xn0909q4qfa89a85fkda7sn56sh4vpd777r4b6dn0s3") (s 2) (e (quote (("marcos" "dep:default-args"))))))

(define-public crate-musixmatch-0.1.2 (c (n "musixmatch") (v "0.1.2") (d (list (d (n "api-request-utils-rs") (r "^0.1.8") (d #t) (k 0)) (d (n "default-args") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1c32xadg369am0a3kpvddvzygk5zdjylhy2h1imqmzlbyvlv65yx") (s 2) (e (quote (("marcos" "dep:default-args"))))))

(define-public crate-musixmatch-0.1.3 (c (n "musixmatch") (v "0.1.3") (d (list (d (n "api-request-utils-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "default-args") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b36398pr86ygdjviw8fqs6dmg7advpws6vjz4844iq15s39h08p") (s 2) (e (quote (("marcos" "dep:default-args"))))))

(define-public crate-musixmatch-0.1.4 (c (n "musixmatch") (v "0.1.4") (d (list (d (n "api-request-utils-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "default-args") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x2a5jwqy6ahp2rhq4m02m8k870vfwxl403lc26aaj6g3wscbmc6") (s 2) (e (quote (("marcos" "dep:default-args"))))))

