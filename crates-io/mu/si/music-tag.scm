(define-module (crates-io mu si music-tag) #:use-module (crates-io))

(define-public crate-music-tag-0.1.0 (c (n "music-tag") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "id3") (r "^1.9.0") (d #t) (k 0)) (d (n "imagesize") (r "^0.12.0") (d #t) (k 0)) (d (n "metaflac") (r "^0.2.5") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.11.0") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (d #t) (k 0)))) (h "0myfy24ipwpjd5hbar0hhpnfbv69vdmi9fcrf7zfhdfzxvxq04pd")))

