(define-module (crates-io mu sa musage) #:use-module (crates-io))

(define-public crate-musage-0.0.1 (c (n "musage") (v "0.0.1") (h "1ix5id5qkv40ldsm86zqdvbssi75hjknakrz0vdmin09plfi7msn") (y #t)))

(define-public crate-musage-0.0.2 (c (n "musage") (v "0.0.2") (d (list (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "procfs") (r "^0.12.0") (d #t) (k 0)))) (h "16hjmcm3fcbbsn09gcqrgda1zl942m0ida14kccmml110w0jkr3f") (y #t)))

(define-public crate-musage-0.0.3 (c (n "musage") (v "0.0.3") (d (list (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "procfs") (r "^0.12.0") (d #t) (k 0)))) (h "15lgd0cpyxdz1449lyiadrfx1xb4n62n9x54y6hjiczqj4g2gfh4")))

(define-public crate-musage-0.0.4 (c (n "musage") (v "0.0.4") (d (list (d (n "autoclap") (r "^0.2.1") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "procfs") (r "^0.12.0") (d #t) (k 0)))) (h "1ljb42jib75k0y1c3qmg1y0priyz02ch9c38g3ngjc9mrz4681lq")))

(define-public crate-musage-0.1.0 (c (n "musage") (v "0.1.0") (d (list (d (n "autoclap") (r "^0.2.2") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "procfs") (r "^0.12.0") (d #t) (k 0)))) (h "18bk3bdhdx4ifzn8jrsfc4jfhpzqvwx0p3fjwil144pwnd1smy4n")))

(define-public crate-musage-0.1.1 (c (n "musage") (v "0.1.1") (d (list (d (n "autoclap") (r "^0.2.2") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "procfs") (r "^0.12.0") (d #t) (k 0)))) (h "1f29p6g53m3g82biaamfg09l8c3ygdr4gs75z59arb3pmhlphpjc")))

(define-public crate-musage-0.1.2 (c (n "musage") (v "0.1.2") (d (list (d (n "autoclap") (r "^0.2.7") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.8") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "procfs") (r "^0.14.1") (d #t) (k 0)))) (h "1dhiaknaqrfkppldrx23ms18m32dqynz3bpjf7x95ddbxjyqz05p")))

(define-public crate-musage-0.1.3 (c (n "musage") (v "0.1.3") (d (list (d (n "autoclap") (r "^0.3.9") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.17") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "procfs") (r "^0.14.1") (d #t) (k 0)))) (h "1akn0k2nfhi995m1qdlb7pipynccd8r3f5r3b60kc3i465vimfns")))

(define-public crate-musage-0.1.4 (c (n "musage") (v "0.1.4") (d (list (d (n "autoclap") (r "^0.3.15") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.19") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "procfs") (r "^0.15.1") (d #t) (k 0)))) (h "1i87lq1zgyp5xn11x24rr6jqvr1jvw747k0m3md2z7y79lmnmrr5")))

(define-public crate-musage-0.1.5 (c (n "musage") (v "0.1.5") (d (list (d (n "autoclap") (r "^0.3.15") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.19") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "procfs") (r "^0.15.1") (d #t) (k 0)))) (h "0hhrbbmrv5d1xsiw1rc9dccbfc9zvi6abhkpiknchmwr5ppw8fbz")))

(define-public crate-musage-0.1.6 (c (n "musage") (v "0.1.6") (d (list (d (n "autoclap") (r "^0.3.15") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.19") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "procfs") (r "^0.16.0") (d #t) (k 0)))) (h "1vwgghi2hhxsj0a5bryk1fpq6c8anlxn7wihb6hhhfmsf647qa4a")))

