(define-module (crates-io mu ni muninn) #:use-module (crates-io))

(define-public crate-muninn-0.1.0 (c (n "muninn") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0wxvlw4n26bgifqgq3sbr8dhz0cv0vklnakvf1yg6vq6m4ml0anp")))

