(define-module (crates-io mu ni muninn_query) #:use-module (crates-io))

(define-public crate-muninn_query-0.1.0 (c (n "muninn_query") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_consume") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "111vnlg3ysxn0gb8skkza3gifbsn3d54rl1gn1rrw5ppb81255wb")))

(define-public crate-muninn_query-0.2.0 (c (n "muninn_query") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^6.2.0") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0.1") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0.121") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")))) (h "1jqg50hdrslmh69m7jrv7nmzh15gkhicif6va9jw4dzn8npf47h6") (f (quote (("serde" "serde_crate" "chrono/serde"))))))

(define-public crate-muninn_query-0.3.0 (c (n "muninn_query") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^6.2.0") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0.1") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0.121") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")))) (h "0ikkk1wd3yn2c9f6xglf6zixvsc1a6vfb2vrlx292kv1vn5dikzd") (f (quote (("serde" "serde_crate" "chrono/serde"))))))

(define-public crate-muninn_query-0.3.1 (c (n "muninn_query") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^6.2.0") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0.1") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0.121") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")))) (h "0jby7akh34j7864css33jh8zcahhii75mg9m8qnwk3vzws7535vp") (f (quote (("serde" "serde_crate" "chrono/serde"))))))

(define-public crate-muninn_query-0.3.2 (c (n "muninn_query") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^6.2.0") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0.1") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0.121") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")))) (h "09wib13ksrzivg9ys1hpwwpzy019y4mwq4h650wrgh88s37v017a") (f (quote (("serde" "serde_crate" "chrono/serde"))))))

(define-public crate-muninn_query-0.4.0 (c (n "muninn_query") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^6.2.0") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0.1") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0.121") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")))) (h "0gmq2pq8fxks1vyladyb09yvx468yrrj6cw3vn8a0dx4bxysn4ky") (f (quote (("serde" "serde_crate" "chrono/serde"))))))

(define-public crate-muninn_query-0.4.1 (c (n "muninn_query") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^6.2.0") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0.1") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0.121") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")))) (h "1f4y9n8s0aif84pc862h7bxjs280sbk82sn7z5gi4szxagi7kmqd") (f (quote (("serde" "serde_crate" "chrono/serde"))))))

(define-public crate-muninn_query-0.5.0 (c (n "muninn_query") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "serde_crate") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")))) (h "117q2h04h92mrjvjyyynqraqimj8l9rs1pairi45srws43xxps7s") (f (quote (("serde" "serde_crate" "chrono/serde"))))))

