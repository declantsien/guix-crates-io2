(define-module (crates-io mu ni munin-if1sec) #:use-module (crates-io))

(define-public crate-munin-if1sec-0.2.0 (c (n "munin-if1sec") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "munin-plugin") (r "^0.1.12") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 0)))) (h "0v0jmlhk2y3cpnypw6dx3i5wbkj4h54f0438j7qjf507sfllfsnq")))

(define-public crate-munin-if1sec-0.2.1 (c (n "munin-if1sec") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "munin-plugin") (r "^0.1.14") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 0)))) (h "1qhic0k6limnpyhg4ls443c6yhcrimqk9ks290kwv1wkg2d7rx2i")))

