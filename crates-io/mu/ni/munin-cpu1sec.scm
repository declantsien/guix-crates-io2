(define-module (crates-io mu ni munin-cpu1sec) #:use-module (crates-io))

(define-public crate-munin-cpu1sec-0.2.0 (c (n "munin-cpu1sec") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "munin-plugin") (r "^0.1.12") (d #t) (k 0)) (d (n "parse_int") (r "^0.6") (d #t) (k 0)) (d (n "procfs") (r "^0.12") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1vfng81dmj4pm869i62p2xjm1ncf2s10arv1ww78013nc3fsdjfs")))

(define-public crate-munin-cpu1sec-0.2.1 (c (n "munin-cpu1sec") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "munin-plugin") (r "^0.1.12") (d #t) (k 0)) (d (n "procfs") (r "^0.12") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 0)))) (h "09ngj4v4gxzgkkd1qya5nff2zn2808h8fm9cbzw92wwjxzdh3vyg")))

(define-public crate-munin-cpu1sec-0.2.2 (c (n "munin-cpu1sec") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "munin-plugin") (r "^0.1.14") (d #t) (k 0)) (d (n "procfs") (r "^0.12") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 0)))) (h "0l7vgyam5as80yhkin1bgjhlyxyj5w06grps8j4zvbdqrfmjq86a")))

