(define-module (crates-io mu mc mumctl) #:use-module (crates-io))

(define-public crate-mumctl-0.3.0 (c (n "mumctl") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "ipc-channel") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mumlib") (r "^0.3") (d #t) (k 0)))) (h "1n3x6iy9n79d8z0hl9x05hra7x8nyakcwwy4zskvap7x67mppb57") (y #t)))

(define-public crate-mumctl-0.3.1 (c (n "mumctl") (v "0.3.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "ipc-channel") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mumlib") (r "^0.3.1") (d #t) (k 0)))) (h "1s5idrwl8nr6hqpgp420r3kcwwnb0p89hn0k74pz64h5gzgsn51d") (y #t)))

(define-public crate-mumctl-0.4.0 (c (n "mumctl") (v "0.4.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mumlib") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "06lszz4z5kg424sd8r8aa9ggj9xik9m2fq5514a0wglhi57c0rp4") (y #t)))

