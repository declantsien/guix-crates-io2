(define-module (crates-io mu sd musdk-derive) #:use-module (crates-io))

(define-public crate-musdk-derive-0.1.0 (c (n "musdk-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1xxw5qps174mbjmj1y4b9cgg5lffyi2a6yp5q23ympy7rz58c3ii")))

