(define-module (crates-io mu sd musdk-common) #:use-module (crates-io))

(define-public crate-musdk-common-0.1.0 (c (n "musdk-common") (v "0.1.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1mdws9zqspwlyhz4pa3jf822jcdhg7jl2lb4fwfmkvwny3ac83qp")))

