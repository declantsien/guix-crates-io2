(define-module (crates-io mu sd musdk) #:use-module (crates-io))

(define-public crate-musdk-0.1.0 (c (n "musdk") (v "0.1.0") (d (list (d (n "musdk-common") (r "^0.1") (d #t) (k 0)) (d (n "musdk-derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bny7wypv285jbyq1cjadajfbvgylwp1598j05g0wbgqfbd9wd7x") (f (quote (("json" "serde" "serde_json") ("http" "serde_urlencoded") ("default" "json" "http"))))))

