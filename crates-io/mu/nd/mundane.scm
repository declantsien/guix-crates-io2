(define-module (crates-io mu nd mundane) #:use-module (crates-io))

(define-public crate-mundane-0.1.0 (c (n "mundane") (v "0.1.0") (h "1dfa3101vsmi9si48cs1bl2afpx8r8vhxffs10lgjrllqp01c178")))

(define-public crate-mundane-0.2.0 (c (n "mundane") (v "0.2.0") (h "1qbjgl6dg5vpdsy9rhyngvhps79qlys6qjimy5hzvmna06dwyaav") (f (quote (("rand-bytes") ("kdf") ("insecure"))))))

(define-public crate-mundane-0.2.1 (c (n "mundane") (v "0.2.1") (h "0dbnjfi8wq1ccss69q7dy2sdb1dnnfjbldpjw6fdsqragmwhmdka") (f (quote (("rand-bytes") ("kdf") ("insecure")))) (y #t)))

(define-public crate-mundane-0.2.2 (c (n "mundane") (v "0.2.2") (h "0immrhzd1gk6ax3z7ir1ma8jig4qhqvp2jqmf0wdnvmy68x4kqcs") (f (quote (("rand-bytes") ("kdf") ("insecure"))))))

(define-public crate-mundane-0.3.0 (c (n "mundane") (v "0.3.0") (h "04q8373hn7cm8fvifwj4njwvas3vnz3h69450087v3dxzicjwn0j") (f (quote (("rand-bytes") ("kdf") ("insecure") ("experimental-sha512-ec"))))))

(define-public crate-mundane-0.4.0 (c (n "mundane") (v "0.4.0") (d (list (d (n "goblin") (r "^0.0.24") (d #t) (k 1)))) (h "0pvrgfj9bbfqpcicn7dz3w3l07i6fyd12zalk5naah5w2ad1a2yv") (f (quote (("run-symbol-conflict-test") ("rsa-test-generate-large-keys") ("rsa-pkcs1v15") ("kdf") ("insecure") ("experimental-sha512-ec") ("default" "rsa-test-generate-large-keys") ("bytes"))))))

(define-public crate-mundane-0.4.1 (c (n "mundane") (v "0.4.1") (d (list (d (n "goblin") (r "^0.0.24") (d #t) (k 1)))) (h "0labydgjhazh8qgsimwx6zd622ibhzj35qjgbcgz199b26hp7psq") (f (quote (("run-symbol-conflict-test") ("rsa-test-generate-large-keys") ("rsa-pkcs1v15") ("kdf") ("insecure") ("experimental-sha512-ec") ("default" "rsa-test-generate-large-keys") ("bytes")))) (y #t)))

(define-public crate-mundane-0.4.2 (c (n "mundane") (v "0.4.2") (d (list (d (n "goblin") (r "^0.0.24") (d #t) (k 1)))) (h "0b2k541a002880mms7i6mw22ywsbpfmd3g0j9al570ky1pmq4mkk") (f (quote (("run-symbol-conflict-test") ("rsa-test-generate-large-keys") ("rsa-pkcs1v15") ("kdf") ("insecure") ("experimental-sha512-ec") ("default" "rsa-test-generate-large-keys") ("bytes"))))))

(define-public crate-mundane-0.4.3 (c (n "mundane") (v "0.4.3") (d (list (d (n "goblin") (r "^0.0.24") (d #t) (k 1)))) (h "00470k07yfpxdaf2jgqfrz8mb1w4clx6kv23d29a4crzvkypbri5") (f (quote (("run-symbol-conflict-test") ("rsa-test-generate-large-keys") ("rsa-pkcs1v15") ("kdf") ("insecure") ("experimental-sha512-ec") ("default" "rsa-test-generate-large-keys") ("bytes"))))))

(define-public crate-mundane-0.4.4 (c (n "mundane") (v "0.4.4") (d (list (d (n "goblin") (r "^0.0.24") (d #t) (k 1)))) (h "0z09kp3lzspr5lcrip7knaayp7hsj7mg0jxiljx9gdkapdaah3wy") (f (quote (("run-symbol-conflict-test") ("rsa-test-generate-large-keys") ("rsa-pkcs1v15") ("kdf") ("insecure") ("experimental-sha512-ec") ("default" "rsa-test-generate-large-keys") ("bytes"))))))

(define-public crate-mundane-0.5.0 (c (n "mundane") (v "0.5.0") (d (list (d (n "goblin") (r "^0.0.24") (d #t) (k 1)))) (h "018kl1jzszd7ydlh23lnkfsnvza3fvlg41mwqdr3l4sbgh358y14") (f (quote (("run-symbol-conflict-test") ("rsa-test-generate-large-keys") ("rsa-pkcs1v15") ("kdf") ("insecure") ("experimental-sha512-ec") ("default" "rsa-test-generate-large-keys") ("bytes"))))))

