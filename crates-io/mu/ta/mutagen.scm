(define-module (crates-io mu ta mutagen) #:use-module (crates-io))

(define-public crate-mutagen-0.1.0 (c (n "mutagen") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1ns2jgkllkv41hnjban303rnvby1g57kgwr1kdk6an5q2nrazwl2")))

(define-public crate-mutagen-0.1.1 (c (n "mutagen") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0bd44q0iqlvv7jdxmz96nv0f5qw227f9da9aqb2awy74mrixkz25")))

(define-public crate-mutagen-0.1.2 (c (n "mutagen") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0ffrm5ysyhpkkzhg6m89c0n0vy0pxlw4dhz5rxqfcxwdxqvfcd31")))

