(define-module (crates-io mu ta mutability_marker) #:use-module (crates-io))

(define-public crate-mutability_marker-0.1.0 (c (n "mutability_marker") (v "0.1.0") (h "0cxlf0y5fw13xalh4flvpfj69y4786m29dhjmi06w2c1l30pk5j4")))

(define-public crate-mutability_marker-0.1.1 (c (n "mutability_marker") (v "0.1.1") (h "1m9dssdq45qpxmilif00zckbp218dpyh3c25xv9q3rhlk3dh24sw")))

