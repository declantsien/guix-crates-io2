(define-module (crates-io mu ta muta-apm-derive) #:use-module (crates-io))

(define-public crate-muta-apm-derive-0.1.0-alpha.1 (c (n "muta-apm-derive") (v "0.1.0-alpha.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "creep") (r "^0.2") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "overlord") (r "^0.2.0-alpha.11") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustracing") (r "^0.4") (d #t) (k 2)) (d (n "rustracing_jaeger") (r "^0.4") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1g1211ym6da60ycsmx3f4r77qc6lvvk10al8hfciikfd97r6aqnv")))

(define-public crate-muta-apm-derive-0.1.0-alpha.2 (c (n "muta-apm-derive") (v "0.1.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0af4y0blg37q0yxljvlkgm36sqixys1gnsdfqcv6jx04d33c8a5g")))

(define-public crate-muta-apm-derive-0.1.0-alpha.3 (c (n "muta-apm-derive") (v "0.1.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18jachjiirhi6xayymqw8z158plylwkyl4clvvz0pvxpkasgyc44")))

(define-public crate-muta-apm-derive-0.1.0-alpha.4 (c (n "muta-apm-derive") (v "0.1.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sh3ym5pavlykfbhk79f4jzllwfa0lhk1f6j2mybgwbj2ja58fr9")))

(define-public crate-muta-apm-derive-0.1.0-alpha.5 (c (n "muta-apm-derive") (v "0.1.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yx3dv5gj4sajwyfipm9f71i1nc8s503k09m9p389v3ip02f5pa5")))

(define-public crate-muta-apm-derive-0.1.0-alpha.6 (c (n "muta-apm-derive") (v "0.1.0-alpha.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c7pqv1k4l39jd2xp8i4gha37gndq5vj27a4z1fchbhd58mfikkc")))

(define-public crate-muta-apm-derive-0.1.0-alpha.7 (c (n "muta-apm-derive") (v "0.1.0-alpha.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08k2af7gcsnddqhnnq22b70p3cfh9f4ygpks07akx6bincw8pj9i")))

(define-public crate-muta-apm-derive-0.1.0-alpha.8 (c (n "muta-apm-derive") (v "0.1.0-alpha.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06czg2pgrw4abcg4qbbhf08vilhv057bi008nx79pcwl3wifcyi4")))

(define-public crate-muta-apm-derive-0.1.0-alpha.9 (c (n "muta-apm-derive") (v "0.1.0-alpha.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "184v8k31ka040szacgqv0avzkrcj8j0xyh8vjaljd8nybdlzrazs")))

(define-public crate-muta-apm-derive-0.1.0-alpha.10 (c (n "muta-apm-derive") (v "0.1.0-alpha.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mjgr6wbhzqq63v1yapvr9y8sp59f5x1fa8mlr8w294kq1qjk1b7")))

(define-public crate-muta-apm-derive-0.1.0-alpha.11 (c (n "muta-apm-derive") (v "0.1.0-alpha.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0prvkd8mw20kq4smqvdpbn2x2mzhc3zjrvp26p6zwpxwcpy94mzk")))

(define-public crate-muta-apm-derive-0.1.0-alpha.12 (c (n "muta-apm-derive") (v "0.1.0-alpha.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r665jqf11gl5f7zfg0gx374g81mgrikfnpmmfqgps885pk5gp14")))

(define-public crate-muta-apm-derive-0.1.0 (c (n "muta-apm-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0iip0mfzl4s4hpxnf0i4rv635n1yps4s1p87gwddzxn3isvi5c21")))

