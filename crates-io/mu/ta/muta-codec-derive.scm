(define-module (crates-io mu ta muta-codec-derive) #:use-module (crates-io))

(define-public crate-muta-codec-derive-0.2.0 (c (n "muta-codec-derive") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rlp") (r "^0.4") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1p0ydx3r8wvr5d0j2aj48j0jvnsy2vci0rxbglj73kcfyhk8mw5l")))

(define-public crate-muta-codec-derive-0.2.1 (c (n "muta-codec-derive") (v "0.2.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rlp") (r "^0.4") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l7dbpf500pb4nz1dfpgg04s469x9982z588fvl19k81k0hk1nm7")))

(define-public crate-muta-codec-derive-0.2.2 (c (n "muta-codec-derive") (v "0.2.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rlp") (r "^0.4") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m2rb9lx0m30vc25hf9xy5y13jgjhaqibdxigjs4wzff7xhxd1s5")))

