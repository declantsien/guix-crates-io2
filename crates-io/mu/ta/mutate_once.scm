(define-module (crates-io mu ta mutate_once) #:use-module (crates-io))

(define-public crate-mutate_once-0.1.0 (c (n "mutate_once") (v "0.1.0") (h "0g9iav1lyvwh43kihdh9312gy0qfkj8s7rk1ba9n255k8s5s8zr6")))

(define-public crate-mutate_once-0.1.1 (c (n "mutate_once") (v "0.1.1") (h "0ys9mpjhwsj5md10ykmkin0wv7bz8dvc292hqczs9l5l4cd6ikqn")))

