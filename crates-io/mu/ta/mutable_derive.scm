(define-module (crates-io mu ta mutable_derive) #:use-module (crates-io))

(define-public crate-mutable_derive-0.1.0 (c (n "mutable_derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1d3krll9xv0514dyj73qwn7qqrapf9x28ygm2dxy5kkrksy0cyaa")))

(define-public crate-mutable_derive-0.2.0 (c (n "mutable_derive") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1h73039nc4p7nbbnqqz5sc3q44dy4isi54yjbx33h7p2vl3f8d9y")))

(define-public crate-mutable_derive-0.2.1 (c (n "mutable_derive") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0lfj3i54hpgbqsxjwz7k4h6f999237lkp1646cxwj44r7ygqs378")))

(define-public crate-mutable_derive-0.3.0 (c (n "mutable_derive") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0sz9d099vl6c90ffvnjx2hdsya8frn39f42bicvxdp62c9qx6v4w")))

(define-public crate-mutable_derive-0.3.1 (c (n "mutable_derive") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0fsq5j0yp14mzdq9bgy0dhrvag0wazdfpqd6zqkrmajpflx765r1")))

(define-public crate-mutable_derive-0.3.2 (c (n "mutable_derive") (v "0.3.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0ckrmb1mwnwg28v7cqdg9j5f6x7sryf76cqliyx1xmrr7khy5wbs")))

(define-public crate-mutable_derive-0.3.3 (c (n "mutable_derive") (v "0.3.3") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0ar7dpcmvb642mxdcgq1cwbwp4mw5s9sr1hlx97h9fyrfc8qin0d")))

