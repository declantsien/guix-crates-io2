(define-module (crates-io mu ta mutagen-plugin) #:use-module (crates-io))

(define-public crate-mutagen-plugin-0.1.0 (c (n "mutagen-plugin") (v "0.1.0") (d (list (d (n "mutagen") (r "^0.1.0") (d #t) (k 0)))) (h "0kds3a0zjv2zr2d6dmkkyl0rs7zp0sg2l28l453nj3q3lzapgfxz")))

(define-public crate-mutagen-plugin-0.1.1 (c (n "mutagen-plugin") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.3.11") (d #t) (k 2)) (d (n "mutagen") (r "^0.1.1") (d #t) (k 0)))) (h "1y3lz0b796ca13arpmms8x33619nik0ifgyfxicq4mq3yym1dpqc")))

(define-public crate-mutagen-plugin-0.1.2 (c (n "mutagen-plugin") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.3.11") (d #t) (k 2)) (d (n "mutagen") (r "^0.1.2") (d #t) (k 0)))) (h "1w4xiaykyi35w2a7c4b4nsxd9mspvh74i52kwiqdmy3aslh183vi")))

