(define-module (crates-io mu ta mutable-constant) #:use-module (crates-io))

(define-public crate-mutable-constant-0.0.1 (c (n "mutable-constant") (v "0.0.1") (h "16p1h6ngnz03r5777lmhm35dh0fgbnwm5gcgdxyjcp2q7fyc25mb")))

(define-public crate-mutable-constant-0.1.0 (c (n "mutable-constant") (v "0.1.0") (h "03q5j6jivlp2rwlcgg2r8hrhx52b3b4vpqk9lr9r6y0jgmf09q8p")))

