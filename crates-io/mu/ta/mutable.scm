(define-module (crates-io mu ta mutable) #:use-module (crates-io))

(define-public crate-mutable-0.1.0 (c (n "mutable") (v "0.1.0") (d (list (d (n "mutable_derive") (r "^0.1.0") (d #t) (k 0)))) (h "077fasbliv7lpxyys1wrazm3s9l5hhw5nd8q2laphq20hj3i3lzr")))

(define-public crate-mutable-0.2.0 (c (n "mutable") (v "0.2.0") (d (list (d (n "mutable_derive") (r "^0.2.0") (d #t) (k 0)))) (h "0r1jxsg8xjiqlwwzczv8vfq0k07xgkj1hv0vpi36a2lp6vmfs0sd")))

(define-public crate-mutable-0.2.1 (c (n "mutable") (v "0.2.1") (d (list (d (n "mutable_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (d #t) (k 0)))) (h "14wq1743jx2hxmp3fwqw85kk2kgrg56wjnpd5kl421v2klhn2l34") (f (quote (("uuid"))))))

(define-public crate-mutable-0.2.2 (c (n "mutable") (v "0.2.2") (d (list (d (n "mutable_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (d #t) (k 0)))) (h "0a6ikfmaxzzlfbghqplj8lj6iz1rwxsbqzy2rz3hic1vbxqy2q9k") (f (quote (("uuid"))))))

(define-public crate-mutable-0.2.3 (c (n "mutable") (v "0.2.3") (d (list (d (n "mutable_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (d #t) (k 0)))) (h "1v34sj1cpilbwwca8vqqkl27gc0hl4hn7qw5kd9m6siagn6pdgjq") (f (quote (("uuid"))))))

(define-public crate-mutable-0.2.4 (c (n "mutable") (v "0.2.4") (d (list (d (n "mutable_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (d #t) (k 0)))) (h "1lzgj6fswqygxd1lsr2zkzlfpkz6869xswbzm5wc31la1f0wzf1r") (f (quote (("uuid"))))))

(define-public crate-mutable-0.2.5 (c (n "mutable") (v "0.2.5") (d (list (d (n "mutable_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (d #t) (k 0)))) (h "0szckg3j4apsby3cav4vxsfb6l1qni8v417fi3crw9h9nnml1ijz") (f (quote (("uuid"))))))

(define-public crate-mutable-0.2.6 (c (n "mutable") (v "0.2.6") (d (list (d (n "mutable_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (d #t) (k 0)))) (h "1jxzvq0dsjvzycq6vdxikkz38v06wqij8ij91cfz8chyim9z62sk") (f (quote (("uuid"))))))

(define-public crate-mutable-0.2.7 (c (n "mutable") (v "0.2.7") (d (list (d (n "mutable_derive") (r "^0.3.2") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (d #t) (k 0)))) (h "161y5jkn25f72jac3qagn6hz6s1vic6v878cin28pql2n796v8hg") (f (quote (("uuid"))))))

(define-public crate-mutable-0.2.8 (c (n "mutable") (v "0.2.8") (d (list (d (n "mutable_derive") (r "^0.3.3") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (d #t) (k 0)))) (h "05wzz8insilrxq5rx60y4d31nfycvrzj92i42n2vp1brk9q2b0sr") (f (quote (("uuid"))))))

