(define-module (crates-io mu ta muta-vendor-prost) #:use-module (crates-io))

(define-public crate-muta-vendor-prost-0.5.0 (c (n "muta-vendor-prost") (v "0.5.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "muta-vendor-prost-derive") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0f8hxfapqfwrbn4a4bfq2cxgsvkpxpdm4wm73hiczh4f818hqfzg") (f (quote (("no-recursion-limit") ("default" "muta-vendor-prost-derive")))) (y #t)))

(define-public crate-muta-vendor-prost-0.5.1 (c (n "muta-vendor-prost") (v "0.5.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "muta-vendor-prost-derive") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "18zlac20pmygxbb589z9p5vvs0acfywp23anq33pvg22v12n6m31") (f (quote (("no-recursion-limit") ("default" "muta-vendor-prost-derive")))) (y #t)))

