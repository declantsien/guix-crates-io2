(define-module (crates-io mu ta muta-std) #:use-module (crates-io))

(define-public crate-muta-std-0.1.0 (c (n "muta-std") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "ckb-allocator") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0ky6fi374hradqk9z4fmc8xd5hz5v972xfriqkccbn2zmsbzjdw2") (f (quote (("default" "ckb-allocator"))))))

