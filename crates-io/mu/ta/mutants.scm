(define-module (crates-io mu ta mutants) #:use-module (crates-io))

(define-public crate-mutants-0.0.1 (c (n "mutants") (v "0.0.1") (h "02fj4mln0lfqr8b77pj1b73lppsbfbk0lrjj85ipiiyshmwnvhcm")))

(define-public crate-mutants-0.0.2 (c (n "mutants") (v "0.0.2") (h "1j8p9jggfmdnmnw5w1qhdhwhf1nmzhcydsgwmb799jqwbyky5hlm")))

(define-public crate-mutants-0.0.3 (c (n "mutants") (v "0.0.3") (h "09k1jp50slkabs90vbj2fwa2z1cgcw0vvkhrn43hx5i68x98f0mw")))

