(define-module (crates-io mu ta mutablex) #:use-module (crates-io))

(define-public crate-mutablex-0.1.0 (c (n "mutablex") (v "0.1.0") (d (list (d (n "futures-signals") (r "^0.3.33") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (d #t) (k 0)))) (h "1wvlgjxjafry4aqakki1ya7d183xgyyvi699d7xpmqmxr4kncrqa")))

(define-public crate-mutablex-0.1.1 (c (n "mutablex") (v "0.1.1") (d (list (d (n "futures-signals") (r "^0.3.33") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (d #t) (k 0)))) (h "0is2jlh34gvm1ca3741z5ncq2mrhf1ic4hii3qg889gq8wnmilyi")))

