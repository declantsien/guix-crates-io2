(define-module (crates-io mu nc muncher) #:use-module (crates-io))

(define-public crate-muncher-0.5.0 (c (n "muncher") (v "0.5.0") (h "1l3k8gz9vzfmr2f4ljrvln2hnfdzxvigcdpya8ni6b8pkndhswx7")))

(define-public crate-muncher-0.6.0 (c (n "muncher") (v "0.6.0") (h "1bifaj56ji8cn7qw2phfmq6a1lvkpmydi205c17jfnyha3199zfw")))

(define-public crate-muncher-0.6.1 (c (n "muncher") (v "0.6.1") (h "0wc6jxfz9bkjhb2g6cwycnrvhvz2sscxlhxq046r8ilxwciyxbph")))

(define-public crate-muncher-0.6.2 (c (n "muncher") (v "0.6.2") (h "19r88fjd8rd2i8n9z79law59ydacl2g6nv0760sn2x0ygq38md7r")))

(define-public crate-muncher-0.7.0 (c (n "muncher") (v "0.7.0") (h "1srr15ps4dhb0g33cshgsdbr3qckyiwgb9ki26rp5nv7mnj7fn86")))

