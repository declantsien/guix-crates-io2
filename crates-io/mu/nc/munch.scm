(define-module (crates-io mu nc munch) #:use-module (crates-io))

(define-public crate-munch-0.1.0 (c (n "munch") (v "0.1.0") (h "19jyq55c9z7x45523w3rs18pkq9sg76m31fb1gz028lsg8s67xc8")))

(define-public crate-munch-0.2.0 (c (n "munch") (v "0.2.0") (h "1q2w2a44c03387m0zj8hrcxkdkl9lnmkgis7648dj82qnbjp7vw9")))

(define-public crate-munch-0.3.0 (c (n "munch") (v "0.3.0") (h "14hirhhlka8qqagwk7axmn4sg00hjhxb9zrpzryjrgks59h6105x")))

(define-public crate-munch-0.3.1 (c (n "munch") (v "0.3.1") (h "123k0qliadfzzmiyxxq0xd82c21id09v67bm7j004kxv73bxajfs")))

(define-public crate-munch-0.4.0 (c (n "munch") (v "0.4.0") (h "1rmd05cq0k64b26g5315j4ka8lr551sswbkmg44l74f9i9pg944b")))

(define-public crate-munch-0.5.0 (c (n "munch") (v "0.5.0") (h "09gy20ff2mhpadzc6ifg4zn5bn7i96bcnh77wh3n8lqkc8i1pvdx")))

(define-public crate-munch-0.5.1 (c (n "munch") (v "0.5.1") (h "0amwi1jg9fymsxc16xbldy6wvqrli4w1wz755pvs1lq4rqs4nil5")))

(define-public crate-munch-0.6.0 (c (n "munch") (v "0.6.0") (h "0zg9jh3dr10kvzw2lrli2hgw57sgifzkh436yrglqjf1igqhqws0")))

(define-public crate-munch-0.7.0 (c (n "munch") (v "0.7.0") (h "1pl8rcfipsb5f90gyj3g2xjdadz7qmq0vk0641f0p1fz93sl46hv")))

(define-public crate-munch-0.8.0 (c (n "munch") (v "0.8.0") (h "16k6h7djal9k739c9dfv1js8ragvmk8w6dr20jpkwgvy88vyrwl2")))

