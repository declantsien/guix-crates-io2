(define-module (crates-io mu tr mutringbuf) #:use-module (crates-io))

(define-public crate-mutringbuf-0.1.0-alpha.0 (c (n "mutringbuf") (v "0.1.0-alpha.0") (d (list (d (n "cpal") (r "^0.15.2") (d #t) (t "cfg(cpal)") (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (t "cfg(bench)") (k 0)) (d (n "crossbeam-utils") (r "^0.8.16") (k 0)))) (h "0qc4fl3kg8yiiyysf4hir1hw930bsnyn325bj7253r011zrc3946") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-mutringbuf-0.0.1-alpha.1 (c (n "mutringbuf") (v "0.0.1-alpha.1") (d (list (d (n "cpal") (r "^0.15.2") (d #t) (t "cfg(cpal)") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (t "cfg(bench)") (k 0)) (d (n "crossbeam-utils") (r "^0.8.16") (k 0)))) (h "19grp1g02qslyfilgplf8smcxlv26bs0j4kjn2k3icpsp2gi6piq") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-mutringbuf-0.1.0-alpha.1 (c (n "mutringbuf") (v "0.1.0-alpha.1") (d (list (d (n "cpal") (r "^0.15.2") (d #t) (t "cfg(cpal)") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (t "cfg(bench)") (k 0)) (d (n "crossbeam-utils") (r "^0.8.16") (k 0)))) (h "0dlm4rx0hp7khby5vcpab195c4imlc4qygd1amkigl221dxvfhkb") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-mutringbuf-0.1.0 (c (n "mutringbuf") (v "0.1.0") (d (list (d (n "cpal") (r "^0.15.2") (d #t) (t "cfg(cpal)") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (t "cfg(bench)") (k 0)) (d (n "crossbeam-utils") (r "^0.8.16") (k 0)))) (h "15q24qlq9rawk8zmi7gdicqy3d7wcvwwc7a0wkimzaayrgd3cjhb") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-mutringbuf-0.1.1 (c (n "mutringbuf") (v "0.1.1") (d (list (d (n "cpal") (r "^0.15.2") (d #t) (t "cfg(cpal)") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (t "cfg(bench)") (k 0)) (d (n "crossbeam-utils") (r "^0.8.16") (k 0)))) (h "19y7hbzpi3d7w8qidb0bh0k7iqdmy7yk4hn3541dgnyd1y6cljbq") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-mutringbuf-0.1.2 (c (n "mutringbuf") (v "0.1.2") (d (list (d (n "cpal") (r "^0.15.3") (d #t) (t "cfg(cpal)") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (t "cfg(bench)") (k 0)) (d (n "crossbeam-utils") (r "^0.8.19") (k 0)))) (h "1c1h9qajv4s5iyj2crckqlib8ycbhmprwkd61fmrjszm2ngncxnc") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-mutringbuf-0.1.3 (c (n "mutringbuf") (v "0.1.3") (d (list (d (n "cpal") (r "^0.15.3") (d #t) (t "cfg(cpal)") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (t "cfg(bench)") (k 0)) (d (n "crossbeam-utils") (r "^0.8.19") (k 0)))) (h "1vk0cpgnfflvhmvrlw59ryj3yg02xf04ickrlvkl0xcm7dvms1la") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-mutringbuf-0.2.0 (c (n "mutringbuf") (v "0.2.0") (d (list (d (n "cpal") (r "^0.15.3") (d #t) (t "cfg(cpal)") (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (t "cfg(bench)") (k 2)) (d (n "crossbeam-utils") (r "^0.8.19") (k 0)))) (h "1a7f1y4hc9nzml0i7mgk1z45f3w4gg25h53h49v0nrra6jzadn2i") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-mutringbuf-0.3.0 (c (n "mutringbuf") (v "0.3.0") (d (list (d (n "cpal") (r "^0.15.3") (d #t) (t "cfg(cpal)") (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (t "cfg(bench)") (k 2)) (d (n "crossbeam-utils") (r "^0.8.19") (k 0)) (d (n "tokio-test") (r "^0.4.4") (d #t) (k 2)))) (h "1j2k96wklcg4aknmqjdkznkgj3751w3hfaiqwjbz0rg2zhyr8vhl") (f (quote (("default" "alloc") ("async") ("alloc"))))))

