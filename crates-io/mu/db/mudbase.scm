(define-module (crates-io mu db mudbase) #:use-module (crates-io))

(define-public crate-mudbase-0.1.0 (c (n "mudbase") (v "0.1.0") (h "0rwhlf3ifdsgla6x98b4ibfi4hhag7v2vwy48dxbbax1vh7givdy")))

(define-public crate-mudbase-0.1.1 (c (n "mudbase") (v "0.1.1") (h "0vlvbhgbqav4m17cdy7pfc65lw2n35wx63sw3hifmpwg7dkhv0wm")))

(define-public crate-mudbase-0.1.2 (c (n "mudbase") (v "0.1.2") (h "0szrp8ilkxw83y1wi2iqp06k2p6a7drpxk8rwlz768mdp4bbmsj7")))

(define-public crate-mudbase-0.1.3 (c (n "mudbase") (v "0.1.3") (d (list (d (n "mudbase_server") (r "^0.1.0") (d #t) (k 0)))) (h "1n0sdk61c939yfdxf4q3g6h771iny8xjacwpjvxci85yzxki4vrv")))

(define-public crate-mudbase-0.1.4 (c (n "mudbase") (v "0.1.4") (d (list (d (n "mudbase_server") (r "^0.1.0") (d #t) (k 0)))) (h "1g28mkwiaznfcdrmdw938cfqa15zq7zngzcr9nxrcgissmgzwwrl")))

