(define-module (crates-io mu mb mumble-link) #:use-module (crates-io))

(define-public crate-mumble-link-0.1.0 (c (n "mumble-link") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "15dx4qi6ad7l7g2a1f689zl3vsl4wb6wy73sv8ppbjnkhxyzz4ks")))

(define-public crate-mumble-link-0.2.0 (c (n "mumble-link") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("memoryapi" "handleapi" "winnt"))) (d #t) (k 0)))) (h "119j8l31jx8pd7wvwqyvs2vx851i1inf8vmgzb8azryl8lvwbx67")))

