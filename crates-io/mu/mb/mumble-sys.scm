(define-module (crates-io mu mb mumble-sys) #:use-module (crates-io))

(define-public crate-mumble-sys-0.1.0 (c (n "mumble-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "~0.55.1") (d #t) (k 1)) (d (n "collect_slice") (r "^1.2.0") (d #t) (k 0)) (d (n "const_format") (r "~0.1.0") (d #t) (k 1)) (d (n "parking_lot") (r "~0.11") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "026054z0sziqnpj8v4wjaah0z3gcs953hdb6x5447f00qxj3djz4") (f (quote (("idebuild") ("default"))))))

