(define-module (crates-io mu mb mumblelink_reader) #:use-module (crates-io))

(define-public crate-mumblelink_reader-0.1.0 (c (n "mumblelink_reader") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "minwindef" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "11y6xc5nnr63kbsqsgnn6mz1z7kw9rn2wkf7k72if4jai3y5svs2")))

(define-public crate-mumblelink_reader-0.2.0 (c (n "mumblelink_reader") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "minwindef" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0s5pa6wkxfwdjw0hqs09a6l9xka9bk0f8l1dyi64zd2ciawbjqzm")))

(define-public crate-mumblelink_reader-0.2.1 (c (n "mumblelink_reader") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "minwindef" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0amli9ljfc08z347m83as6kh9ziikcs6rx5lmmzf3l5b96wb2hr2")))

(define-public crate-mumblelink_reader-0.2.2 (c (n "mumblelink_reader") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "minwindef" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1g102j2l0cicdn43s1yvaz2ff666i3yd5n8a34wfmdckx1sbs34s")))

(define-public crate-mumblelink_reader-0.2.3 (c (n "mumblelink_reader") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "minwindef" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1mrwq7kxz668k243a49gk42qhcak91ks3ffj23lmalkfrda20v8n")))

(define-public crate-mumblelink_reader-0.3.0 (c (n "mumblelink_reader") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "minwindef" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0qvxwkps1q3ccjcbm937454fi14m1yay9l0cmqwxvkf7c224rkyh")))

(define-public crate-mumblelink_reader-0.3.1 (c (n "mumblelink_reader") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "minwindef" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1pq8dc4miq8ysb317mnp74s8gvzni8s68calkvnnnwpxg8gfavkb")))

(define-public crate-mumblelink_reader-0.3.2 (c (n "mumblelink_reader") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "minwindef" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ww23jfihmrzl5rlvn08q8sgyk7fkih0y959idjxin5mfyr5iy6r")))

(define-public crate-mumblelink_reader-0.3.3 (c (n "mumblelink_reader") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "minwindef" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0aj4r1p7dyqxhxpqdxxjrj2afkd6gs35yldcm1csqwnq6m2zq0hv")))

(define-public crate-mumblelink_reader-0.3.4 (c (n "mumblelink_reader") (v "0.3.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "minwindef" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0hmwf9wkzzpw2l1n0riqb4n3yiwyzzsiqh3adak6ynnwjpkz7zf7")))

(define-public crate-mumblelink_reader-0.3.5 (c (n "mumblelink_reader") (v "0.3.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winnt" "minwindef" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vx65vamxglvphvpflzd3v7rzg2kkja32h62xqf3p97vhgfybdkv")))

