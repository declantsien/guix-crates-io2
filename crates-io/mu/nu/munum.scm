(define-module (crates-io mu nu munum) #:use-module (crates-io))

(define-public crate-munum-0.1.0 (c (n "munum") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ifwhj5l2gclrn75gyx7hnzyb2lgmijawskx2kgs9ba2s81yymfz") (f (quote (("std" "num/std") ("libm" "num/libm") ("default" "std"))))))

(define-public crate-munum-0.1.1 (c (n "munum") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jidsbvvh8sfamwam83smmn05hma0fqr9gmwlrhrbg9hwdzj983d") (f (quote (("std" "num/std") ("libm" "num/libm") ("default" "std"))))))

(define-public crate-munum-0.2.0 (c (n "munum") (v "0.2.0") (d (list (d (n "num") (r "^0.4") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "165izvzicj5i8bilhbsnpx835d6vlrqgr5dabyva009bpdqhh1fk") (f (quote (("wasm") ("std" "num/std") ("libm" "num/libm") ("jsmath") ("default" "std"))))))

