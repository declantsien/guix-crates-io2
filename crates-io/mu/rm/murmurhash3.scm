(define-module (crates-io mu rm murmurhash3) #:use-module (crates-io))

(define-public crate-murmurhash3-0.0.1 (c (n "murmurhash3") (v "0.0.1") (h "196c5nww4h7szscya2skpy9qahzp0lmda6ma7bqihpak3p08xnyj")))

(define-public crate-murmurhash3-0.0.2 (c (n "murmurhash3") (v "0.0.2") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "06a6bgyqs2q3z82garqk41rlwbc8448nwb4crzr5bg661s52f18m")))

(define-public crate-murmurhash3-0.0.3 (c (n "murmurhash3") (v "0.0.3") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "1a4mlr6k1p3fhqm3f352jfafa1zp47wvbgqxk8c5blzv3ywgcmf9")))

(define-public crate-murmurhash3-0.0.4 (c (n "murmurhash3") (v "0.0.4") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "02vy9363knyzpff1z530mkkkl1wsaxqmsk28hlyj4iiamww8l8rh")))

(define-public crate-murmurhash3-0.0.5 (c (n "murmurhash3") (v "0.0.5") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "0r1n79wr1z5pl3rlvas9mwrgxyny4wnvyrrp1120aj7lr9r37652") (f (quote (("nightly"))))))

