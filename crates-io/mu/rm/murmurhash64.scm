(define-module (crates-io mu rm murmurhash64) #:use-module (crates-io))

(define-public crate-murmurhash64-0.1.0 (c (n "murmurhash64") (v "0.1.0") (h "0fisms0slin2il67510dwlya9x8yr287f3jjv35r4imllj42iqzh")))

(define-public crate-murmurhash64-0.1.1 (c (n "murmurhash64") (v "0.1.1") (h "04p2f62zy6n759yssm16vpjb7hfqhmhwyvfl5vfy2k3ji43zn3sp")))

(define-public crate-murmurhash64-0.2.0 (c (n "murmurhash64") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "189hlcxb9pmgcpknh7vla3vhp9ddn942s77h4i087xckj8sar23l")))

(define-public crate-murmurhash64-0.3.0 (c (n "murmurhash64") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0lc7n8612m89caf8xh2dxsgaxbi74xjf0kpf63d0gjfyx7vdh5cx") (f (quote (("hasher"))))))

(define-public crate-murmurhash64-0.3.1 (c (n "murmurhash64") (v "0.3.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0174pwq37czfk296rc7ibvrxgdpn0wh3nxfpada651c9x94gwags") (f (quote (("hasher"))))))

