(define-module (crates-io mu rm murmurhash) #:use-module (crates-io))

(define-public crate-murmurhash-0.1.0 (c (n "murmurhash") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1pav57p730i2nspg26c6l6j5njqlg76fnswyy8r1b68rcrmi58bi") (y #t)))

(define-public crate-murmurhash-0.1.1 (c (n "murmurhash") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "07mhy94dwfm53ycwl3xfnkagdgph5pwm75x9gwz4w7jbxxzzsj0m") (y #t)))

(define-public crate-murmurhash-0.1.2 (c (n "murmurhash") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "0g59k4s59s1r72v72sabxd9xvqz8yvym56dbswsgl0sq1sfjh8cj") (y #t)))

(define-public crate-murmurhash-0.1.3 (c (n "murmurhash") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "0z9an11f7y1fiji1y93dzzyh9alkj2a2w1py3kxh2l37lyw28c6h") (y #t)))

(define-public crate-murmurhash-0.1.4 (c (n "murmurhash") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "04achm0wl1idmmlglmy4nbi72d32fmg0s2vc9al4ckywn287k58f") (y #t)))

(define-public crate-murmurhash-0.1.5 (c (n "murmurhash") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1lbplvsb9d6zi112nfaw090h12k6xabz716nm96jlg96x9z3frn5") (y #t)))

(define-public crate-murmurhash-0.1.6 (c (n "murmurhash") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1b0mmjcvd6j6sa2abylv1rc1m0ahjzy7lb173pw27ri8nk8drwgz")))

