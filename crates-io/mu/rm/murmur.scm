(define-module (crates-io mu rm murmur) #:use-module (crates-io))

(define-public crate-murmur-0.1.0 (c (n "murmur") (v "0.1.0") (h "18p0yk5v72x0cz1bc8a421lwznnx2js4qafqsfs1p0hxx4bf9kg8") (y #t)))

(define-public crate-murmur-0.3.0 (c (n "murmur") (v "0.3.0") (d (list (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 0)) (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1c40zq6ir91wmi9q8h3bhihwwx3wshr5b8il5mzp67kmg6lw50dy") (f (quote (("default")))) (y #t)))

(define-public crate-murmur-0.3.1 (c (n "murmur") (v "0.3.1") (d (list (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 0)) (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "168qfxf4g3lcgx9sk22ry0hy6kj12lagwn2cddbyizsq9445slzy") (f (quote (("default")))) (y #t)))

(define-public crate-murmur-0.3.2 (c (n "murmur") (v "0.3.2") (d (list (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 0)) (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0ipc1g1nkns7rm4bn95g6igdv4fhgm9ngg933pyms174iphi3vxp") (f (quote (("default")))) (y #t)))

(define-public crate-murmur-0.3.3 (c (n "murmur") (v "0.3.3") (d (list (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0cd8ycq9pkvbw3aj9v6kxylxlad23iy4vxbww10hdsp8fr0x2sl7") (f (quote (("default")))) (y #t)))

(define-public crate-murmur-0.3.4 (c (n "murmur") (v "0.3.4") (d (list (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1in1d9k203i0np6015fil5wkznrv4k5jlyjrip5yvia25vq0x65x") (f (quote (("default")))) (y #t)))

(define-public crate-murmur-0.3.6 (c (n "murmur") (v "0.3.6") (d (list (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "enum-iterator") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12f29k38d38bhxly7v3wfvqd43h1nsj0v8nsi5ihfhq7s5k8gswr") (f (quote (("default")))) (y #t)))

(define-public crate-murmur-1.0.0 (c (n "murmur") (v "1.0.0") (d (list (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "enum-iterator") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0") (d #t) (k 0)))) (h "1q6xh32l3wb12bgca6x4ihy7k2izmslc5kynkagr7bbgx5sghvlg") (f (quote (("default"))))))

(define-public crate-murmur-1.0.1 (c (n "murmur") (v "1.0.1") (d (list (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "enum-iterator") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0") (d #t) (k 0)))) (h "11qy1dzyngs9ap8l9z1ghqq7ylhp8jivpf7z3khnqy8ffw2d3vc3") (f (quote (("default"))))))

(define-public crate-murmur-1.1.0 (c (n "murmur") (v "1.1.0") (d (list (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "enum-iterator") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0") (d #t) (k 0)))) (h "1c9qacmkjk9v7avzkp6ywm162gl52bzijx2ka0b2ndhds5wfvshj") (f (quote (("default"))))))

(define-public crate-murmur-1.2.0 (c (n "murmur") (v "1.2.0") (d (list (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "enum-iterator") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0") (d #t) (k 0)))) (h "1aacizb9z8k3468q88z7ncbidg7nx8cq0fh0p8xqs6wvq55hixyz") (f (quote (("default"))))))

(define-public crate-murmur-1.2.1 (c (n "murmur") (v "1.2.1") (d (list (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "enum-iterator") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0") (d #t) (k 0)))) (h "0c583fkbvh0fjg7grq0h4mp97ivvid1p0pd46qx7isy1mrahw654") (f (quote (("default"))))))

(define-public crate-murmur-2.0.0 (c (n "murmur") (v "2.0.0") (d (list (d (n "color-backtrace") (r "^0.6.1") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "enum-iterator") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0") (d #t) (k 0)))) (h "1qmmcwi8r3zykdz2yzavxgc0y3lfr3mx0xy90cw2fv5s00kxb0zv") (f (quote (("experimental") ("default"))))))

