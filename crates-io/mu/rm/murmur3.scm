(define-module (crates-io mu rm murmur3) #:use-module (crates-io))

(define-public crate-murmur3-0.0.2 (c (n "murmur3") (v "0.0.2") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "0x4yppch73r9zdnznfanabmdim15gsbzc1y59hpngbwcqd43ypli")))

(define-public crate-murmur3-0.1.0 (c (n "murmur3") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "1caaq6pa6786fmk5b7y65g7s3mna8ld2m54bvdpqqaz0gcr46xd4")))

(define-public crate-murmur3-0.2.0 (c (n "murmur3") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "1b4jy5ln0l9cxj9fk3f2588if5k2n9gc4cvp55x1nqd3b8xpw0vv")))

(define-public crate-murmur3-0.3.0 (c (n "murmur3") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)))) (h "08iycs8gzvym8ffjk7s5sdnzdhm36nqbd0f5j6cfdz2g2s2zxrzc")))

(define-public crate-murmur3-0.4.0 (c (n "murmur3") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "1r29i708216zjrcy4bgjsbmikwpii0bxc32y7hqrqp9skdsgryx9")))

(define-public crate-murmur3-0.4.1 (c (n "murmur3") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "073p2hncdnngin5mjbxnc8v35x4azqclmz4d712ga0zwkrcgk651")))

(define-public crate-murmur3-0.5.0 (c (n "murmur3") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.2") (f (quote ("i128"))) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "00y0aqka56hwaa4i2cmgs8kmywkkswvpkr3wqvm223qbgdzxrhbg")))

(define-public crate-murmur3-0.5.1 (c (n "murmur3") (v "0.5.1") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1zrcnvf32nzag6r5ap7i3xqacq9pzmdb1iih4ri8xlw5wj457b9y")))

(define-public crate-murmur3-0.5.2 (c (n "murmur3") (v "0.5.2") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0jvi9hsppwln53xvcvad79zm0jx2qb531q7qnqlhkfijy4f12llj")))

