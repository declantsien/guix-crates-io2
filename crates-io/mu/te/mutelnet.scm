(define-module (crates-io mu te mutelnet) #:use-module (crates-io))

(define-public crate-mutelnet-0.0.1 (c (n "mutelnet") (v "0.0.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "telnet-codec") (r "^0.1") (d #t) (k 0)))) (h "0bp076xf9vbjv8pnpw1s5hwsl4k47s65yhz6d66747q6kfqqwsyd")))

(define-public crate-mutelnet-0.0.2 (c (n "mutelnet") (v "0.0.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "telnet-codec") (r "^0.1") (d #t) (k 0)))) (h "1dfnzx8bc6p8d5l25qkjlbq27pd75dp6skvxxlbbqsril6pm0n7g") (y #t)))

(define-public crate-mutelnet-0.0.3 (c (n "mutelnet") (v "0.0.3") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "telnet-codec") (r "^0.1") (d #t) (k 0)))) (h "0bgh9k0sx7xjl41z23nli3szlzkmg8chqh1wg4xaj0dxv2bhf1xy")))

