(define-module (crates-io mu te mutex-watersheds) #:use-module (crates-io))

(define-public crate-mutex-watersheds-0.1.0 (c (n "mutex-watersheds") (v "0.1.0") (d (list (d (n "disjoint-sets") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1zhflh74vz4314xvl8jdh7xygscix6c18ikxba17isdlkyaxc2lm")))

(define-public crate-mutex-watersheds-0.1.1 (c (n "mutex-watersheds") (v "0.1.1") (d (list (d (n "disjoint-sets") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "0irip8zd8n07kyb37ss8qyq7b6zhs4w5k6016ybbhjsmn4mfh63m")))

