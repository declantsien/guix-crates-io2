(define-module (crates-io mu te mutex-trait) #:use-module (crates-io))

(define-public crate-mutex-trait-0.1.0 (c (n "mutex-trait") (v "0.1.0") (h "1is6n23l5ayp1igjrccikvv5cir3ln1d2icj7rmq3gkdmlvv2kda")))

(define-public crate-mutex-trait-0.2.0 (c (n "mutex-trait") (v "0.2.0") (h "0fndccy267i3m9v2zsmxhm145lfhmcwydna33j5jzq8rshw1dfxl")))

