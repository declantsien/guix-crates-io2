(define-module (crates-io mu te mutex-timeouts) #:use-module (crates-io))

(define-public crate-mutex-timeouts-0.1.0 (c (n "mutex-timeouts") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "1k702w5vzrljxxws7rs1zsv8fsqpj8pi1zzxj2b1zzl1ibwgr0i4")))

(define-public crate-mutex-timeouts-0.1.1 (c (n "mutex-timeouts") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "1pgy5b2751r4w5bfzjnvrd82f86jhrmq0n1wq1ydp0fdv7h4jg4g")))

(define-public crate-mutex-timeouts-0.2.0 (c (n "mutex-timeouts") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "0w547yjl2k7rf23m097a9idxnx1s0r9i70ad13sfazblrh304x79")))

(define-public crate-mutex-timeouts-0.3.0 (c (n "mutex-timeouts") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "1nf2m9mdyq9igk49zx2gl7jj6w7ifx6ni3s4dawvpd0v5r8a3sdj")))

