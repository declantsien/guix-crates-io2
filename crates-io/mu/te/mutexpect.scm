(define-module (crates-io mu te mutexpect) #:use-module (crates-io))

(define-public crate-mutexpect-0.2.0 (c (n "mutexpect") (v "0.2.0") (d (list (d (n "pattern_partition_prediction") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tabfile") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twobit") (r "^0.1") (d #t) (k 0)))) (h "105cam6mw4waapci0qnr5is4wpdnsa97qymknff1dmds4yz2xb5x")))

(define-public crate-mutexpect-0.2.3 (c (n "mutexpect") (v "0.2.3") (d (list (d (n "pattern_partition_prediction") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tabfile") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twobit") (r "^0.1") (d #t) (k 0)))) (h "11xlpa979ks3vdqxvz2rhimbx30nprlb052qm6frs4a8bbzq7y0j")))

(define-public crate-mutexpect-0.2.4 (c (n "mutexpect") (v "0.2.4") (d (list (d (n "pattern_partition_prediction") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tabfile") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twobit") (r "^0.1") (d #t) (k 0)))) (h "1s98zaa6kq4h6yxsgkllsp1ja9xdfq1p5nryy8mv8bhnjp72zbac")))

(define-public crate-mutexpect-0.2.5 (c (n "mutexpect") (v "0.2.5") (d (list (d (n "pattern_partition_prediction") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tabfile") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twobit") (r "^0.1") (d #t) (k 0)))) (h "0sjxm26dzbbpl321fwa1kkdv0bl5zpm9kx4gm2j9lm06yj4nn6wj")))

(define-public crate-mutexpect-0.2.7 (c (n "mutexpect") (v "0.2.7") (d (list (d (n "pattern_partition_prediction") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tabfile") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "twobit") (r "^0.1") (d #t) (k 0)))) (h "14vk3fc2kgnhpxnz93m8igb9shl3zsfh4dnmdny81v2akfkwrrsr")))

