(define-module (crates-io mu te mutex-extra) #:use-module (crates-io))

(define-public crate-mutex-extra-0.9.6 (c (n "mutex-extra") (v "0.9.6") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)))) (h "1cmc72c4pxpgq07n57vlnsb1rpsasidiyf6lx5aw7jhqv7cjampq")))

(define-public crate-mutex-extra-0.9.7 (c (n "mutex-extra") (v "0.9.7") (h "19bv47bw0vnvn37n8sslkidj83nzbvg23jc3r145h7yw03gjabw0")))

(define-public crate-mutex-extra-0.9.8 (c (n "mutex-extra") (v "0.9.8") (h "1kbkg2srdplhika0nccm5vhb54a7z8ngjy1p3m5vdjm8ihlxmd6m")))

(define-public crate-mutex-extra-0.9.9 (c (n "mutex-extra") (v "0.9.9") (h "1076vndvw7s83bfklmf6vs66s0s25xir26zm97jqv7ccfl30f27r")))

