(define-module (crates-io mu _r mu_rust_message_client) #:use-module (crates-io))

(define-public crate-mu_rust_message_client-0.1.0 (c (n "mu_rust_message_client") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "mu_rust_message_common") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)))) (h "1322r36sypaq9s9a6j2lza81z3x7m0pfajmfxkmkfhdsqhdi7clw")))

