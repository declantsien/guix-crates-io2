(define-module (crates-io mu _r mu_rust_common) #:use-module (crates-io))

(define-public crate-mu_rust_common-0.1.0 (c (n "mu_rust_common") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("formatting" "local-offset" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter" "time" "local-time"))) (d #t) (k 0)))) (h "0agwqvgflrs44gbcfiv35jfph109arg170jwr9kxmyc7l05yg3ay")))

