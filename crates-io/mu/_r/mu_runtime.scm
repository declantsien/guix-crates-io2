(define-module (crates-io mu _r mu_runtime) #:use-module (crates-io))

(define-public crate-mu_runtime-0.2.0 (c (n "mu_runtime") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "aws_lambda_events") (r "^0.4.0") (d #t) (k 2)) (d (n "httpmock") (r "^0.5.8") (d #t) (k 2)) (d (n "hyper") (r "^0.14") (f (quote ("client" "http1" "tcp"))) (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "rusoto_core") (r "^0.46.0") (d #t) (k 2)) (d (n "rusoto_dynamodb") (r "^0.46.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1jp79zd07fmydxvi843aibgaar571n8rihbzvn3mdfya3vpy5ns7")))

