(define-module (crates-io mu _r mu_rust_message_common) #:use-module (crates-io))

(define-public crate-mu_rust_message_common-0.1.0 (c (n "mu_rust_message_common") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "07yyd1dgby6ysbs6fq07fck7adfxdygwdxnn9cdn0b49f758xrf8")))

