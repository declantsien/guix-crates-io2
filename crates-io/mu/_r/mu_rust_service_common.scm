(define-module (crates-io mu _r mu_rust_service_common) #:use-module (crates-io))

(define-public crate-mu_rust_service_common-0.1.0 (c (n "mu_rust_service_common") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "axum") (r "^0.6.16") (d #t) (k 0)) (d (n "mu_rust_common") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1dhvqjgxl1qr7jdk2chfn3f0hyjpb7hhq9wn4nhyindqwnfnd6lm")))

