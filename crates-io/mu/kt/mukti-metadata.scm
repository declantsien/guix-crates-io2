(define-module (crates-io mu kt mukti-metadata) #:use-module (crates-io))

(define-public crate-mukti-metadata-0.1.0 (c (n "mukti-metadata") (v "0.1.0") (d (list (d (n "semver") (r "^1.0.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0i3nd1jq351ijr1fw3vaa4ccznxc95902jx137gq6fc4nv6kizqf") (r "1.59")))

(define-public crate-mukti-metadata-0.2.1 (c (n "mukti-metadata") (v "0.2.1") (d (list (d (n "semver") (r "^1.0.20") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0vdw8h24x1b47a1mvc5hlv59l6xy2hclfsa5wg7dnqnkl0x1sr1p") (r "1.59")))

