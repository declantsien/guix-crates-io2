(define-module (crates-io mu ml mumlib) #:use-module (crates-io))

(define-public crate-mumlib-0.3.0 (c (n "mumlib") (v "0.3.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0cq4zamacsgj0j2a38vglkvjykgjzsiqhih3vim2pn387pcfigci")))

(define-public crate-mumlib-0.3.1 (c (n "mumlib") (v "0.3.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1nsz4rswkgms4f4mi9rsd1c22byfipy6wpfjdb62k4xsqgb1cb69")))

(define-public crate-mumlib-0.4.0 (c (n "mumlib") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0bsdgsqpcf28yw7canpw9w8a5ngm816ri14lrjkvdzp745r6mwq4")))

(define-public crate-mumlib-0.5.0 (c (n "mumlib") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1mdjixmf94aanbxj9djm92hqyxgh5p91w7j8p97gm67cp63b18fm")))

(define-public crate-mumlib-0.5.1 (c (n "mumlib") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "13rapxih9ixr29qn2b4hlia0f67dadaj1131cs37rryj9z725k13")))

