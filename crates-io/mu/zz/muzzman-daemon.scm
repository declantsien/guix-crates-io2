(define-module (crates-io mu zz muzzman-daemon) #:use-module (crates-io))

(define-public crate-muzzman-daemon-0.0.1 (c (n "muzzman-daemon") (v "0.0.1") (d (list (d (n "bytes-kman") (r "^0.1.18") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "muzzman-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)))) (h "045d538n1kp2ckxr2158dzv0hifi52cqig4g99yvvimg2sxkbvrm")))

(define-public crate-muzzman-daemon-0.0.2 (c (n "muzzman-daemon") (v "0.0.2") (d (list (d (n "bytes-kman") (r "^0.1.18") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "muzzman-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)))) (h "1hhhyxb80pikxshsrm0yxxn2sgk1szhmy6nsvp7pcqivvfs1lapf")))

(define-public crate-muzzman-daemon-0.0.3 (c (n "muzzman-daemon") (v "0.0.3") (d (list (d (n "bytes-kman") (r "^0.1.18") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "muzzman-lib") (r "^0.3.2") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)))) (h "0dln3gjfz88dica2hzsij86vzcgcnljll9v2akwbs15l9r5knvvq")))

(define-public crate-muzzman-daemon-0.0.4 (c (n "muzzman-daemon") (v "0.0.4") (d (list (d (n "bytes-kman") (r "^0.1.18") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "muzzman-lib") (r "^0.3.3") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)))) (h "1bkshzy3rl1gic345b8pqn1xpj51blg31ldv2759mijcvcmw7lsn")))

(define-public crate-muzzman-daemon-0.0.5 (c (n "muzzman-daemon") (v "0.0.5") (d (list (d (n "bytes-kman") (r "^0.1.18") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "muzzman-lib") (r "^0.3.4") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)))) (h "1r6c2v0g5rffviq6zvy99xf84vj4jpxa0sj8zj4cjf4k6q8mmlc1")))

(define-public crate-muzzman-daemon-0.0.6 (c (n "muzzman-daemon") (v "0.0.6") (d (list (d (n "bytes-kman") (r "^0.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "muzzman-lib") (r "^0.3.5") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)))) (h "0q5ka3d3d9vlr0sy56fxm6x46x1977mpjf7hlyg0xskzlrc369g0")))

(define-public crate-muzzman-daemon-0.0.7 (c (n "muzzman-daemon") (v "0.0.7") (d (list (d (n "bytes-kman") (r "^0.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "muzzman-lib") (r "^0.3.5") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)))) (h "02dlx6wibkp12jvzpp90hfmn5wbdm4x48pipmd153g53z8az8vg6")))

(define-public crate-muzzman-daemon-0.0.8 (c (n "muzzman-daemon") (v "0.0.8") (d (list (d (n "bytes-kman") (r "^0.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "muzzman-lib") (r "^0.3.5") (d #t) (k 0)) (d (n "polling") (r "^2.5.2") (d #t) (k 0)))) (h "0av8wbl0pjzl6gl66mkb1zxm11qayarsmb2icxyv3c8anrdwfbc2")))

