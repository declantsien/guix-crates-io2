(define-module (crates-io mu zz muzzman-lib-macros) #:use-module (crates-io))

(define-public crate-muzzman-lib-macros-0.1.0 (c (n "muzzman-lib-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("derive" "full" "quote"))) (d #t) (k 0)))) (h "0jxcwa1an6na8kqqmvy0kz4pdylp901am3cv9z3zink2k81hfd16")))

(define-public crate-muzzman-lib-macros-0.1.1 (c (n "muzzman-lib-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("derive" "full" "quote"))) (d #t) (k 0)))) (h "0vqm3pjj18mvqwvglznzh6w6237vgm69rzq4gflssd5n7wl7fpy7")))

(define-public crate-muzzman-lib-macros-0.1.2 (c (n "muzzman-lib-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("derive" "full" "quote"))) (d #t) (k 0)))) (h "0jka25abynd5nmzlz4w8nh47kdc0g4682s957iw99y35zr2bqszk")))

(define-public crate-muzzman-lib-macros-0.1.3 (c (n "muzzman-lib-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("derive" "full" "quote"))) (d #t) (k 0)))) (h "15vy9np26qip425palkfxnw59p3fc725c1gamcli7rdb29r1yc9k")))

