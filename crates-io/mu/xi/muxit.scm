(define-module (crates-io mu xi muxit) #:use-module (crates-io))

(define-public crate-muxit-0.1.0 (c (n "muxit") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.3.1") (d #t) (k 0)))) (h "1gwhh6dz20glphmy9g7q1qlvhwvvq1381n9alxx33qlrplaxdm1g")))

(define-public crate-muxit-0.2.0 (c (n "muxit") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "tmux_interface") (r "^0.3.1") (d #t) (k 0)))) (h "1nccwab92fwjjqd650a4whx31srnyxh8ryfp6qy44jdslsi5hlcx")))

