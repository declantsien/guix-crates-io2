(define-module (crates-io mu st mustache) #:use-module (crates-io))

(define-public crate-mustache-0.3.0 (c (n "mustache") (v "0.3.0") (h "0k9r7c7fqxz6ajv2swkcvvwphpnirqg92dqc774nh4wry21qrhk1")))

(define-public crate-mustache-0.4.0 (c (n "mustache") (v "0.4.0") (h "1q8wlfnq5qbj3n1qddw4kvaia2nsbq0bn0833c4shmk1f0k4h5as")))

(define-public crate-mustache-0.5.0 (c (n "mustache") (v "0.5.0") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "1i3njzgxhnb2h1nyv2bbx7w73a4djms4apszjpcqfi4nq1ak0h3g")))

(define-public crate-mustache-0.6.0 (c (n "mustache") (v "0.6.0") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "09kyy4z9iyfasfkzcgnyhv4bi1w6q0zs6qmk4az5vbc745kzhz2p")))

(define-public crate-mustache-0.6.1 (c (n "mustache") (v "0.6.1") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "1klmfj74021vr9b6va99q07f5652ff5f27f6c5w0a5ffa6zn6f78")))

(define-public crate-mustache-0.6.2 (c (n "mustache") (v "0.6.2") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "12xadbz3cd922qsqqjjfc4cm79sf5n6p8bq8v7kinpbn7j3ffwjr") (f (quote (("unstable"))))))

(define-public crate-mustache-0.6.3 (c (n "mustache") (v "0.6.3") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "0hfpyy5m8vj73iqld8hicxbpjxh2dx6rgv18r23gxcd955fc1ypj") (f (quote (("unstable"))))))

(define-public crate-mustache-0.7.0 (c (n "mustache") (v "0.7.0") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1xbbyndqvjlygbs446p3w0mwa7h8m8ciyn71zpkmcyqbk61x3zv9") (f (quote (("unstable"))))))

(define-public crate-mustache-0.8.0 (c (n "mustache") (v "0.8.0") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1ggscl1a8falcsdxjak56j22k7innfnmligfw29127wbwpgri8jz") (f (quote (("unstable"))))))

(define-public crate-mustache-0.8.1 (c (n "mustache") (v "0.8.1") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "143kysb35aiabnblh5dkj2gvy4pl8m0yaw6564naqih416zjsiqw") (f (quote (("unstable"))))))

(define-public crate-mustache-0.8.2 (c (n "mustache") (v "0.8.2") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "04vx18gi09jrxd1v7c4ij0ixdgbh0wfgjpqa5lbwjkrk37j09c6x") (f (quote (("unstable"))))))

(define-public crate-mustache-0.9.0 (c (n "mustache") (v "0.9.0") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1dfakqld6zf995nnkgs9ybccgps4zcbfd4adaa2162njqpqnx5ai") (f (quote (("unstable"))))))

