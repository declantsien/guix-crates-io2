(define-module (crates-io mu st musty-proc-macro) #:use-module (crates-io))

(define-public crate-musty-proc-macro-0.1.0 (c (n "musty-proc-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1qw5pnfjmq9g4443477r199b6if0d96spxs9mz95wjw21mvw8n2f") (f (quote (("mongodb") ("default" "mongodb"))))))

(define-public crate-musty-proc-macro-0.2.0 (c (n "musty-proc-macro") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1x0l1y4x90429j1y8zkm2lfiw9np7bh3p4dbrv2y5hzcjdnzi566") (f (quote (("mongodb") ("default" "mongodb"))))))

(define-public crate-musty-proc-macro-0.2.1 (c (n "musty-proc-macro") (v "0.2.1") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "09fdq336cvinbrjga62h2frkd77jy9yjjdi3wi3agcdn9b7jfzmg") (f (quote (("mongodb") ("default" "mongodb"))))))

(define-public crate-musty-proc-macro-0.3.0 (c (n "musty-proc-macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "179qv20wcjrw6i3m1g3263wilw4idzybg7lx5dj08cp00pnkdsz0") (f (quote (("mongodb") ("default" "mongodb"))))))

