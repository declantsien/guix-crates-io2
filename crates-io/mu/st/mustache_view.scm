(define-module (crates-io mu st mustache_view) #:use-module (crates-io))

(define-public crate-mustache_view-0.1.0 (c (n "mustache_view") (v "0.1.0") (d (list (d (n "mustache") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1x8maq58vp399g3x50pz47zgv1icl4dd6anspkx59a5c3z4rryy4")))

