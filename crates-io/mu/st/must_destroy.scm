(define-module (crates-io mu st must_destroy) #:use-module (crates-io))

(define-public crate-must_destroy-0.1.0 (c (n "must_destroy") (v "0.1.0") (h "0qprwmbkwy88h62vdcg31kyqj570lfnygrdwwry0h3n8afc6cx08")))

(define-public crate-must_destroy-0.2.0 (c (n "must_destroy") (v "0.2.0") (h "1llzzlgzc793iskqsagccnhxzcxcg4dg2gycqssdj9ikrcvyb2zp")))

(define-public crate-must_destroy-0.2.1 (c (n "must_destroy") (v "0.2.1") (h "0zw1wqd1w1wzv2yp9j2gv76bjq1vsyvk8zcd4v27vl5khn4mppyq")))

(define-public crate-must_destroy-0.3.0 (c (n "must_destroy") (v "0.3.0") (h "1g9hwdiw4x15z60ki7jjj7ll9v2k6v1zigrnp2z60w44i4iy6ign")))

(define-public crate-must_destroy-0.3.1 (c (n "must_destroy") (v "0.3.1") (h "13qqf7dvjcgzfcspvq9r32fys3926l53fhr3dg5l95wmxmfxnvyy")))

