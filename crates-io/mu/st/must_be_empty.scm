(define-module (crates-io mu st must_be_empty) #:use-module (crates-io))

(define-public crate-must_be_empty-0.1.0 (c (n "must_be_empty") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("visit" "full" "printing"))) (d #t) (k 0)))) (h "1fvm388pwslj8n46d08vdv16ry9glmkgyy1avifzc4ivz93g3756") (f (quote (("warn") ("only_on_release") ("only_on_debug") ("default" "warn"))))))

(define-public crate-must_be_empty-0.1.1 (c (n "must_be_empty") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("visit" "full" "printing"))) (d #t) (k 0)))) (h "04lkbydavdbdf999kpgiissgh74jkmnrpnskfz0vjhp55ikn38kh") (f (quote (("warn") ("only_on_release") ("only_on_debug") ("default" "warn"))))))

