(define-module (crates-io mu st must) #:use-module (crates-io))

(define-public crate-must-0.1.0 (c (n "must") (v "0.1.0") (h "06jdq9c9hdsrkmymsfr3fz8g6sy3yypmi65v3nzs4c7jzhc9f6f1") (f (quote (("unstable") ("default"))))))

(define-public crate-must-0.1.1 (c (n "must") (v "0.1.1") (h "1flivzam6p5ly75flcr44x00qp41afaz9gkp3vibvwsmk5dxzgnp") (f (quote (("unstable") ("default"))))))

(define-public crate-must-0.1.2 (c (n "must") (v "0.1.2") (h "0pz2297nwnwkrmj286jg6a36y77dgi5rgazg9mcmb53zcq1x55a9") (f (quote (("unstable") ("default"))))))

(define-public crate-must-0.1.3 (c (n "must") (v "0.1.3") (h "10cc2pchwrhmq40395rl6nn6z4j1xqidb0nwls9vr1fss2a3x3wz") (f (quote (("unstable") ("default"))))))

(define-public crate-must-0.2.0 (c (n "must") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mutator") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0cdw9002y8gpyqshfiwiigf53fnm4790pb6x346hjxr3q4jmsryc") (f (quote (("unstable") ("default"))))))

