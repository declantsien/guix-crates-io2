(define-module (crates-io mu st must_future) #:use-module (crates-io))

(define-public crate-must_future-0.1.0 (c (n "must_future") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "02d6vl7y0mwiqfn774f21in6znh4ysxvjzziv54p1dkkrw2f3n0a")))

(define-public crate-must_future-0.1.1 (c (n "must_future") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1sxk3wj4i2yh4l44mf61lwa5ylba8c4i4z2mz8x26mf0j8xfggls")))

(define-public crate-must_future-0.1.2 (c (n "must_future") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1gpfkr2n341ld8l81xz32xzcm77hw7jbdab7dj8d561g7knzyq51")))

