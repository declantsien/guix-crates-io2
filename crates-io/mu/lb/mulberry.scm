(define-module (crates-io mu lb mulberry) #:use-module (crates-io))

(define-public crate-mulberry-0.0.0 (c (n "mulberry") (v "0.0.0") (d (list (d (n "nalgebra") (r "^0.21.1") (o #t) (d #t) (k 0)))) (h "1cr3n8bln9hmqvvd9fld304vmvkh8i5gcqbvpgsqz6dn4gl3pl23")))

(define-public crate-mulberry-0.0.1 (c (n "mulberry") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.21.1") (o #t) (d #t) (k 0)))) (h "0phgz4a5lm72yncg4ljv1cgd8swkdwf1wf94w30d08brf90hwp47")))

(define-public crate-mulberry-0.1.0 (c (n "mulberry") (v "0.1.0") (d (list (d (n "alga") (r "^0.9.3") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.1") (f (quote ("alga"))) (o #t) (d #t) (k 0)))) (h "123fqc2nx6mm76jxizc0khqrsn1nbc5wnxmkachj9rxbszmx9l25")))

