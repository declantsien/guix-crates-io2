(define-module (crates-io mu ch muchutils) #:use-module (crates-io))

(define-public crate-muchutils-0.1.0 (c (n "muchutils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (d #t) (k 0)) (d (n "iowrap") (r "^0.2") (d #t) (k 0)) (d (n "lru") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "tempfile-fast") (r "^0.3") (d #t) (k 0)))) (h "0v335d3ycby63fsy7khzdlxi4p6ivjgmv5v3n3y096l3l14fznkz")))

