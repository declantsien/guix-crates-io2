(define-module (crates-io mu _a mu_alb) #:use-module (crates-io))

(define-public crate-mu_alb-0.2.0 (c (n "mu_alb") (v "0.2.0") (d (list (d (n "aws_lambda_events") (r "^0.4.0") (d #t) (k 0)) (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "httpmock") (r "^0.5.8") (d #t) (k 2)) (d (n "mu_runtime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0fwvq8jxk2zzc54ac9j3hp5ks1k018a4bzbqhff9h9yvxvzawl55") (f (quote (("multi_header"))))))

