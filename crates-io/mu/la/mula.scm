(define-module (crates-io mu la mula) #:use-module (crates-io))

(define-public crate-mula-0.1.0 (c (n "mula") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "mula_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "spmc") (r "^0.3.0") (d #t) (k 0)))) (h "0hbp4jkzybw7j89ksh2bj73k7l7jhcmcfdrqrl02rfcd5ijqrfbc")))

(define-public crate-mula-0.1.1 (c (n "mula") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "mula_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "spmc") (r "^0.3.0") (d #t) (k 0)))) (h "1m3gl3khfiwf15wc0yy4czwfrg88x6z98hmsd3l0y96hm052zx5f")))

