(define-module (crates-io mu xm muxme) #:use-module (crates-io))

(define-public crate-muxme-0.0.2 (c (n "muxme") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "jacklog") (r "^0.0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0hpriq3h83k3wjk8d0hkksr670jqqwlizblzpi65cv6vpa61mxli")))

(define-public crate-muxme-0.0.4 (c (n "muxme") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("color" "derive" "std" "unstable-doc"))) (k 0)) (d (n "jacklog") (r "^0.2.0") (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std" "attributes"))) (k 0)))) (h "08csc7br3jvdsvi35p4sy695iqsb8m9w8mhn392xscmx94rcnhgk")))

