(define-module (crates-io mu _c mu_core_types) #:use-module (crates-io))

(define-public crate-mu_core_types-1.0.0 (c (n "mu_core_types") (v "1.0.0") (h "0ffvcry1m6nnbn8pabcjlgs0bgy4irr10km04yb8jmdn9y2r2nqc")))

(define-public crate-mu_core_types-1.0.1 (c (n "mu_core_types") (v "1.0.1") (h "074a6q21wsbq9dsbmr8ryz2xryx4cri3bfinb8dx248zjdmainiq")))

