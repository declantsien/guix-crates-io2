(define-module (crates-io mu ne munemo-rs) #:use-module (crates-io))

(define-public crate-munemo-rs-0.1.0 (c (n "munemo-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0fmvn6hx2nllw6xz7kj8w9488jsmqd7k43ybnd68pi3micixzmvq")))

(define-public crate-munemo-rs-0.1.1 (c (n "munemo-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "12bj2aca3d6n395fic1lzpjsb0lwvcf4c563pilc4dkky8xmz2rg")))

