(define-module (crates-io mu su musubi) #:use-module (crates-io))

(define-public crate-musubi-0.1.0 (c (n "musubi") (v "0.1.0") (h "0lc6zwp6fkz9v1l910p2x651mh98gz9qsmjhd1cy8rrwg5ymrihq")))

(define-public crate-musubi-0.1.1 (c (n "musubi") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "1x72j6n70zkhb0yj77in7i3nk85k1hc29rx7lmq1d0sikpy4pfal")))

