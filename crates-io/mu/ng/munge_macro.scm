(define-module (crates-io mu ng munge_macro) #:use-module (crates-io))

(define-public crate-munge_macro-0.3.0 (c (n "munge_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lik46drxqh0li7xrsq2d4bqnm34k2yhqdmy5k15r3f5xbhn6j6c")))

(define-public crate-munge_macro-0.4.0 (c (n "munge_macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fmlaxxzgi7i3mblnr01v23qkmdavla66dgnqi9ddqqa5hbs5afx")))

