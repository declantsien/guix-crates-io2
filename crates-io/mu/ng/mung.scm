(define-module (crates-io mu ng mung) #:use-module (crates-io))

(define-public crate-mung-0.1.0 (c (n "mung") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "02s7amr4hkg3yfz0l8j8ynans0ngmmyhx49b2pgfq9ikdn3s4bmk")))

