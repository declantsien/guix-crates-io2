(define-module (crates-io mu ng munge_auth) #:use-module (crates-io))

(define-public crate-munge_auth-0.1.0 (c (n "munge_auth") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "munge-sys") (r "^0.1") (d #t) (k 0)))) (h "055wkpr9ag8n87h51iv478mgvpzarrs6h9qfx5iwi63jc8dpss52")))

(define-public crate-munge_auth-0.1.1 (c (n "munge_auth") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "munge-sys") (r "^0.1.1") (f (quote ("static"))) (d #t) (k 0)))) (h "0nf7q7ifpgidr8yxg5vv35nqwq4vkzc3a5g3kdvlav67vhwlxy3l")))

