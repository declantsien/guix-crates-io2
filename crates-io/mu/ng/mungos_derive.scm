(define-module (crates-io mu ng mungos_derive) #:use-module (crates-io))

(define-public crate-mungos_derive-0.3.21 (c (n "mungos_derive") (v "0.3.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1w4jfqzjisfzlgfxzj46ax76mnl992hzf2kzvr57yjy7yhcfgmaz")))

(define-public crate-mungos_derive-0.3.22 (c (n "mungos_derive") (v "0.3.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1s5x4srn9397yp77avlcwxi1ms3g7fwcikhx7km3fxnadrha8ci9")))

(define-public crate-mungos_derive-0.3.23 (c (n "mungos_derive") (v "0.3.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "034qr5636jh4kr1z5lcyfbpa0dhk0gxklijsvsi3bfhv1ggb9psj")))

(define-public crate-mungos_derive-0.4.1 (c (n "mungos_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0qhqvqq4am20hp5p8jxzkjsvz65h1cc6vw372yfr23dpw1z3ad80")))

(define-public crate-mungos_derive-0.4.3 (c (n "mungos_derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0b9ldvapvbb26s6ijacasj66j29jm6c01jsz83nwa6d5qpzapigf")))

(define-public crate-mungos_derive-0.4.4 (c (n "mungos_derive") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "01gr3hnd5x4qkz0r5a7ddpzdgy15h58q2b8rvla444c6lm13xy8i")))

(define-public crate-mungos_derive-0.4.8 (c (n "mungos_derive") (v "0.4.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1m79g9b4wdhzgqd850046jdar90nj8yqppw6mb4f77ha5m2bqkm4")))

(define-public crate-mungos_derive-0.4.10 (c (n "mungos_derive") (v "0.4.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1v6mqs85y8hy7m3zrin63fykkk9ay1c5872m65dqgdkwf8bv2f86")))

(define-public crate-mungos_derive-0.4.12 (c (n "mungos_derive") (v "0.4.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1akbm1r1jcnmaa1hpmljxkhmkpva7lh59l14flknqxkcf8xh55kw")))

(define-public crate-mungos_derive-0.4.13 (c (n "mungos_derive") (v "0.4.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1vnz3fjzvn0nvx7jr75ilfq4lz94wnmkbrybq1f072pp3wf5yxwk")))

(define-public crate-mungos_derive-0.4.15 (c (n "mungos_derive") (v "0.4.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0mr7kw3r9gcadq6ncf6ws6lfyqpwwsiqr9iib92ynpx1cpza7ajx")))

