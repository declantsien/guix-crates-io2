(define-module (crates-io mu ng munge) #:use-module (crates-io))

(define-public crate-munge-0.1.0 (c (n "munge") (v "0.1.0") (h "13gvf30nx579fgs7vnq4wzaycagvd83vfzz7x68b46r7q7dahy1k")))

(define-public crate-munge-0.1.1 (c (n "munge") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "^0.7") (d #t) (k 2)))) (h "1148ih5p6hah2j5akzfnqr7rzsfj3ay4dm19n8v7gd2baxqjdld9")))

(define-public crate-munge-0.2.0 (c (n "munge") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.7") (d #t) (k 2)))) (h "0fb1vgf8w2zdigixnvfyrrrj8ljz78s1blk3xbgzcdy9dnci00sf")))

(define-public crate-munge-0.3.0 (c (n "munge") (v "0.3.0") (d (list (d (n "compiletest_rs") (r "^0.7") (d #t) (k 2)) (d (n "munge_macro") (r "^0.3") (d #t) (k 0)))) (h "1a5821nirr8v5fp1k131j2csp5d4055p49cfqairkwfilgkj5gmb")))

(define-public crate-munge-0.4.0 (c (n "munge") (v "0.4.0") (d (list (d (n "munge_macro") (r "^0.4.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "02dbwkslc09q2ai2piq977xk9mqmjmhrrgcbrk70wkfpllwyg95c")))

