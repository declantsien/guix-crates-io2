(define-module (crates-io mu ng mungos_rocket) #:use-module (crates-io))

(define-public crate-mungos_rocket-0.0.1 (c (n "mungos_rocket") (v "0.0.1") (h "15j0gsvsxlaw5v3ahmmqhizkmpm51wl9nrcmz4b1j26wmvrmkrb4")))

(define-public crate-mungos_rocket-0.0.2 (c (n "mungos_rocket") (v "0.0.2") (h "1zsan2i0b2h9h191xgzsy9mgkl376iq3aga5ddlcqaccpkshxdfg")))

(define-public crate-mungos_rocket-0.0.3 (c (n "mungos_rocket") (v "0.0.3") (h "1wqr33vcxjina6zkidjfn4513i1236ih3w0j4m8rppca35bk5spb")))

(define-public crate-mungos_rocket-0.0.4 (c (n "mungos_rocket") (v "0.0.4") (h "0z5rzrxmbmlcxxa08f25izrnp6bzd3pvkl7bgdbbc1jx3lwj6wks")))

(define-public crate-mungos_rocket-0.0.5 (c (n "mungos_rocket") (v "0.0.5") (h "0nkhccy27dds03iqsp76ggxq7430hkmrmmgy4fpsmrpww4nybn22")))

(define-public crate-mungos_rocket-0.0.6 (c (n "mungos_rocket") (v "0.0.6") (h "0vhagp64x9j0h5mvs2k2w9ccaipkj0hd1l1zfn163b1yw086gx7i")))

(define-public crate-mungos_rocket-0.0.7 (c (n "mungos_rocket") (v "0.0.7") (h "0arkx668hsglqa3zbpqyb7h1q3swd0yrwj413x8bzsv0n0qfaxw8")))

