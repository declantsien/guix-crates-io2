(define-module (crates-io mu lt multiparty) #:use-module (crates-io))

(define-public crate-multiparty-0.1.0 (c (n "multiparty") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 2)) (d (n "httparse") (r "^1") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.4") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "try-lock") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "012b9aig0z7x0caaagfvxf4bmpwnxvva7h5l6c5iwjkdk9ccc7pd") (f (quote (("server" "httparse" "memchr") ("futures03" "futures-core" "pin-project-lite" "try-lock"))))))

