(define-module (crates-io mu lt multidimension) #:use-module (crates-io))

(define-public crate-multidimension-0.1.0 (c (n "multidimension") (v "0.1.0") (h "0kmwf7ni9xryarb4jkaxz3a7qgma02jqjq70pkr5jczddjcmnx90") (r "1.71.1")))

(define-public crate-multidimension-0.2.0 (c (n "multidimension") (v "0.2.0") (h "05z8sh1swnspkq5psa9f3hrmxvry4qsd2ps5c5l3ni9q1n7dfghd") (r "1.71.1")))

(define-public crate-multidimension-0.2.1 (c (n "multidimension") (v "0.2.1") (h "01jr3f438fzxz8dsjwlnfk1dvjrci9ifi5vnfkz0vn5gspsjxnyk") (r "1.71.1")))

(define-public crate-multidimension-0.2.2 (c (n "multidimension") (v "0.2.2") (h "1rd8whh3w193ajj2wnvqnx0fm1fwclvsamcb9llqlvcr4d5i943n") (r "1.71.1")))

(define-public crate-multidimension-0.2.3 (c (n "multidimension") (v "0.2.3") (h "1lh8ji836ycql290rlfi9mwa7yvrwpcdl6n13bm614zjf6mzm35v") (y #t) (r "1.71.1")))

(define-public crate-multidimension-0.3.0 (c (n "multidimension") (v "0.3.0") (h "1zf6cxramvniydqrik8dr10959iyp4id41jn7rd3hv211yysf5qd") (r "1.71.1")))

(define-public crate-multidimension-0.3.1 (c (n "multidimension") (v "0.3.1") (h "16rdq7c1n553brz1ihf4zxhzw93phsi7pv4rdby1692j99ydl6x6") (r "1.71.1")))

(define-public crate-multidimension-0.3.2 (c (n "multidimension") (v "0.3.2") (h "1crg33fz256429ap4xqxy05xdxlnm990afvjqsmfplzhsqd6cnpx") (y #t) (r "1.71.1")))

(define-public crate-multidimension-0.3.3 (c (n "multidimension") (v "0.3.3") (h "0w47ndldb602kwm2fc41sh1a55hjacvpgrhb93h1i52xrfhgvkjd") (r "1.71.1")))

