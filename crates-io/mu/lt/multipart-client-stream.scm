(define-module (crates-io mu lt multipart-client-stream) #:use-module (crates-io))

(define-public crate-multipart-client-stream-0.1.0 (c (n "multipart-client-stream") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "common-multipart-rfc7578") (r "^0.6.0") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("io"))) (k 0)))) (h "0sc6mndwgzr7i2vz0y85m1a3431vzr40g1cj7280279lyy2yqhw3")))

