(define-module (crates-io mu lt multer-derive-macros) #:use-module (crates-io))

(define-public crate-multer-derive-macros-0.1.0-alpha (c (n "multer-derive-macros") (v "0.1.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1vvlc0rqgjxnzxirj1lwfyr9xvlhq8jzvlm5fi6lnksr91mrbrld")))

(define-public crate-multer-derive-macros-0.1.1-alpha (c (n "multer-derive-macros") (v "0.1.1-alpha") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "021wfr2dljdc7h9slws9qgv5yl17dwl4fhdg6wf5d9hh2c8wd1dw")))

