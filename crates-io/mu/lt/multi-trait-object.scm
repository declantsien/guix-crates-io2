(define-module (crates-io mu lt multi-trait-object) #:use-module (crates-io))

(define-public crate-multi-trait-object-0.1.0 (c (n "multi-trait-object") (v "0.1.0") (d (list (d (n "dyn-clone") (r "^1.0.4") (d #t) (k 2)))) (h "10nkhh4gina70pwxdim8s7la5k4rywz1z4fg38ayi8pvg2i09wy4")))

(define-public crate-multi-trait-object-0.1.1 (c (n "multi-trait-object") (v "0.1.1") (h "19zqvcldrjxfwwibk26xjx5bvyi5vyy7yqzikvqn4x686angdvrp")))

(define-public crate-multi-trait-object-0.1.2 (c (n "multi-trait-object") (v "0.1.2") (h "0r3cdzlfq6d78jdy8viymakbdsd3cgqjcsb5pmfxrv2ii7i0r9ql")))

(define-public crate-multi-trait-object-0.1.3 (c (n "multi-trait-object") (v "0.1.3") (h "0dachjr4xv6vw5yjbyakh46q631w3giwlv1j1i6lqjyzanwaia7i")))

(define-public crate-multi-trait-object-0.2.0 (c (n "multi-trait-object") (v "0.2.0") (h "075b6afzyl58l6684wl67i8ziqflgs6izmg7cdxr5iw65gnwjm2s")))

