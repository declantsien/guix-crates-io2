(define-module (crates-io mu lt multisig-lite) #:use-module (crates-io))

(define-public crate-multisig-lite-0.0.1 (c (n "multisig-lite") (v "0.0.1") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)))) (h "1x90qijxcmnh11jwnjgk9nqs0g56fdk3zbrkhmqf1ky23a18nlx1") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-multisig-lite-0.0.2 (c (n "multisig-lite") (v "0.0.2") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)))) (h "08wy9lyjaxq5pk3dwsyv1nja40vvs1zhyqgfycsbz5xg5xb1h5bi") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-multisig-lite-0.0.3 (c (n "multisig-lite") (v "0.0.3") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)))) (h "0glbj525r0ayhv2ssaici7wxsb74by9s94gl2jn95q0haacf00bs") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-multisig-lite-0.0.4 (c (n "multisig-lite") (v "0.0.4") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)))) (h "1yprvjjhnpvg2m5svb0d8fnf3xsvjpli83gd902b1vcfm5nargqz") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-multisig-lite-0.0.5 (c (n "multisig-lite") (v "0.0.5") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)))) (h "09p8chsypcj8f3wibx02fyhn673gzdlkcqnx2fp5i3c1ljvqr205") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-multisig-lite-0.0.6 (c (n "multisig-lite") (v "0.0.6") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)))) (h "0sv5rp5pqhpx4g6y9hhjxgni72mvi78pmcpj0gl7ln24pn2pii5x") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-multisig-lite-0.0.7 (c (n "multisig-lite") (v "0.0.7") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "anchor-client") (r "^0.26.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "shellexpand") (r "^3") (d #t) (k 2)) (d (n "solana-client") (r "~1.14") (d #t) (k 2)) (d (n "solana-program-test") (r "~1.14") (d #t) (k 2)) (d (n "solana-sdk") (r "~1.14") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "13f6zhiaxkvd9cbyq6h2p7mi0vpqipir28pkcqcjzkcdaglgfss8") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("localnet") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-multisig-lite-0.0.8 (c (n "multisig-lite") (v "0.0.8") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "anchor-client") (r "^0.26.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "shellexpand") (r "^3") (d #t) (k 2)) (d (n "solana-client") (r "~1.14") (d #t) (k 2)) (d (n "solana-program-test") (r "~1.14") (d #t) (k 2)) (d (n "solana-sdk") (r "~1.14") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1ay1nkppy23h5pwnx32v4im0qpx0fyrf4w4jcr63r23n76y4cl22") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("localnet") ("default") ("cpi" "no-entrypoint"))))))

