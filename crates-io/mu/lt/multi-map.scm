(define-module (crates-io mu lt multi-map) #:use-module (crates-io))

(define-public crate-multi-map-1.0.0 (c (n "multi-map") (v "1.0.0") (h "09a04s1fn4qvvgrc3b7zbs5d1r8i6891m97lqxi8fhdc90nlp5x2")))

(define-public crate-multi-map-1.0.1 (c (n "multi-map") (v "1.0.1") (h "10plgm0y774nlb9w72lkvxria8yy24nkqifid52vh0sv96b9ya4l")))

(define-public crate-multi-map-1.0.2 (c (n "multi-map") (v "1.0.2") (h "0g0rzcci4ks8cba7sds1ggimpz7qb5ryrmhjrn6jjq9jvbdqgi7z")))

(define-public crate-multi-map-1.0.3 (c (n "multi-map") (v "1.0.3") (h "0w1b0a1yr16bair5aciqmj722dqkj8r0a96mdf3zxgsf8dgcqnp3")))

(define-public crate-multi-map-1.1.0 (c (n "multi-map") (v "1.1.0") (h "0i4f8nsh9p9m9liv6xwc6s7a5d9sz72n2afrxdbn8pspmm9lj321")))

(define-public crate-multi-map-1.2.0 (c (n "multi-map") (v "1.2.0") (h "0r12bm5076cckszsszq0g47rv1n2x22gsqxjxjhsn73z5r3zfhav")))

(define-public crate-multi-map-1.3.0 (c (n "multi-map") (v "1.3.0") (h "0iix8h4dcpligrxl3q2lafkg02sn761ylxvmfq0lmxwmszb539dv")))

