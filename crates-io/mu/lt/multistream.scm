(define-module (crates-io mu lt multistream) #:use-module (crates-io))

(define-public crate-multistream-0.1.0 (c (n "multistream") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easy-tokio-rustls") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sidrfw9p4kwhk87qyzlv64wx3aivf178qmbh273fikbnswcyxpv")))

