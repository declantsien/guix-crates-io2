(define-module (crates-io mu lt multishot) #:use-module (crates-io))

(define-public crate-multishot-0.1.0 (c (n "multishot") (v "0.1.0") (d (list (d (n "executor") (r "^0.8") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(multishot_loom)") (k 0)))) (h "0jkb6zm6x44gdz2f9bfzx8cbab31w5lb5bjg8smmb4rfs9x0n4l5") (r "1.56")))

(define-public crate-multishot-0.2.0 (c (n "multishot") (v "0.2.0") (d (list (d (n "executor") (r "^0.8") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(multishot_loom)") (k 0)))) (h "1v7cmi0sp4yql1h24bngcvyzmrlms25gnmcyddzg4gcza9h03vwb") (r "1.56")))

(define-public crate-multishot-0.3.0 (c (n "multishot") (v "0.3.0") (d (list (d (n "executor") (r "^0.8") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(multishot_loom)") (k 0)))) (h "0ck3lyxiw6pxxki0d60d5fy7zsrc2ya7878fcbjbx819fjj9fz5w") (y #t) (r "1.56")))

(define-public crate-multishot-0.3.1 (c (n "multishot") (v "0.3.1") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(multishot_loom)") (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 2)))) (h "1w0pv1kfw80zqczlmmprvsardnrx8xg68sshc200w7bbnar7fbsk") (y #t) (r "1.56")))

(define-public crate-multishot-0.3.2 (c (n "multishot") (v "0.3.2") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(multishot_loom)") (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 2)))) (h "00m8rra1n2gc50qsvk2siv3kmmyqjr27dzqd7fzsnfzinz79i60i") (r "1.56")))

