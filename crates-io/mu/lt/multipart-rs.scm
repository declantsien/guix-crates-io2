(define-module (crates-io mu lt multipart-rs) #:use-module (crates-io))

(define-public crate-multipart-rs-0.1.0 (c (n "multipart-rs") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)))) (h "1cvfjl29ipnfbhdaz1g8sw4akbf93810zmsbamv9zl6czns6890f")))

(define-public crate-multipart-rs-0.1.1 (c (n "multipart-rs") (v "0.1.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)))) (h "0w1rq80yihciivhn4i3j8z4y5q7m5r8yc48db8d17fjlyyqg1vqc")))

(define-public crate-multipart-rs-0.1.2 (c (n "multipart-rs") (v "0.1.2") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)))) (h "0xh8qn93xp3v99anj2kr5rsljiq08yppqp4x1n8phn3r8hsqia1g")))

(define-public crate-multipart-rs-0.1.3 (c (n "multipart-rs") (v "0.1.3") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)))) (h "1cx403a12x8vzcsdynabbfrsc8vh1dvjym116irpgv9ig5glyiif")))

(define-public crate-multipart-rs-0.1.4 (c (n "multipart-rs") (v "0.1.4") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)))) (h "1pkv1kh41np172ja5vq2vvzywwnbvrxdzyzhh8hb1pgc91zzs3bn")))

(define-public crate-multipart-rs-0.1.5 (c (n "multipart-rs") (v "0.1.5") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)))) (h "15j0xbqf64fdw96s9nnmwc5krpv5j0nqqkm4fjp01srjj17grsdb")))

(define-public crate-multipart-rs-0.1.6 (c (n "multipart-rs") (v "0.1.6") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)))) (h "1g471ibkchgvrcpyjhafpzgx1j0a8j4i3g2sfn38r4ldrb51jbnq")))

(define-public crate-multipart-rs-0.1.7 (c (n "multipart-rs") (v "0.1.7") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)))) (h "18z2lms3mq4wc12k11hv8cw6f4p025zdvrwakzkxh9bn2bbb1i8l")))

(define-public crate-multipart-rs-0.1.8 (c (n "multipart-rs") (v "0.1.8") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)))) (h "0p9vv9mwk8l47nhggghrv14xyrmqpibk7lr3rixlby43nma986qv")))

