(define-module (crates-io mu lt multiref) #:use-module (crates-io))

(define-public crate-multiref-0.1.0 (c (n "multiref") (v "0.1.0") (h "0d4m5c8z8g81awpp4qvqj45mpg604lsbq1b2angwixgpppr0qw3a") (y #t)))

(define-public crate-multiref-0.1.1 (c (n "multiref") (v "0.1.1") (h "08p5g0jrmr9w45707wk8cfql6n1hmqp2yca8506qmnqn3svq37kc") (y #t)))

(define-public crate-multiref-0.1.2 (c (n "multiref") (v "0.1.2") (h "1drqy3vn26dj9m7azrrdrppw92r21n65cr1awl4q3h0jh5b6znpk")))

