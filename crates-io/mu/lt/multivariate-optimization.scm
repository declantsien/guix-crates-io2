(define-module (crates-io mu lt multivariate-optimization) #:use-module (crates-io))

(define-public crate-multivariate-optimization-0.0.1 (c (n "multivariate-optimization") (v "0.0.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1mw5wd74pi5ak0y4dwb03dnym64hmx11yglg74icbh32kjb2rnhh")))

(define-public crate-multivariate-optimization-0.0.2 (c (n "multivariate-optimization") (v "0.0.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0fb3bywxzhmmillw99snv607k39zwc09596bw1z5mf4n1g57s02j")))

