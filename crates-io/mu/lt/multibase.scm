(define-module (crates-io mu lt multibase) #:use-module (crates-io))

(define-public crate-multibase-0.2.0 (c (n "multibase") (v "0.2.0") (d (list (d (n "base-x") (r "^0.1.0") (d #t) (k 0)))) (h "1v5gm7flhhcp4daa81b6ff47siq82vh1fm7fa3yjnx259rsydzs1")))

(define-public crate-multibase-0.3.0 (c (n "multibase") (v "0.3.0") (d (list (d (n "base-x") (r "~0.1.0") (d #t) (k 0)))) (h "1mvi9jndk9z70hbd54rryy8r4gg1l9p5y14c1g0lrqry61yakpbk")))

(define-public crate-multibase-0.4.0 (c (n "multibase") (v "0.4.0") (d (list (d (n "base-x") (r "~0.1.3") (d #t) (k 0)))) (h "0g5iq2gd3mrpxz44kqzy52m346smmcnlx0ckj2255i2gikrrlhia")))

(define-public crate-multibase-0.5.0 (c (n "multibase") (v "0.5.0") (d (list (d (n "base-x") (r "^0.2") (d #t) (k 0)))) (h "0ldcwhk8ampv21l5igbkc4hgpry6z4mns2n1ly9sak7zsa4b4m9h")))

(define-public crate-multibase-0.6.0 (c (n "multibase") (v "0.6.0") (d (list (d (n "base-x") (r "^0.2") (d #t) (k 0)))) (h "1mjhg37zmbzy4nmsslfqgmwrvy0axzyqsk4jk5mf3mhg12n5vhxr")))

(define-public crate-multibase-0.7.0 (c (n "multibase") (v "0.7.0") (d (list (d (n "base-x") (r "^0.2") (d #t) (k 0)) (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "051brwlh5nqfnnb0pjpbxk67nzi4i5kcnbb1g1j0bdy77gyznh72")))

(define-public crate-multibase-0.8.0 (c (n "multibase") (v "0.8.0") (d (list (d (n "base-x") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "data-encoding") (r "^2.2") (d #t) (k 0)) (d (n "data-encoding-macro") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1qk76q47fwfw97439lylvsfy2n4s57spx1mf83hi5zshjq1n135p")))

(define-public crate-multibase-0.9.0 (c (n "multibase") (v "0.9.0") (d (list (d (n "base-x") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "data-encoding") (r "^2.2") (d #t) (k 0)) (d (n "data-encoding-macro") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0ikgahqqb3mqm8ag4r1r213birk753a0hqq6k02qsimy6h1gmprc")))

(define-public crate-multibase-0.9.1 (c (n "multibase") (v "0.9.1") (d (list (d (n "base-x") (r "^0.2.7") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "data-encoding") (r "^2.3.1") (f (quote ("alloc"))) (k 0)) (d (n "data-encoding-macro") (r "^0.1.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "014l697md16829k41hzmfx4in9jzhn774q5292bsq10z7kn3jdcv") (f (quote (("std" "data-encoding/std") ("default" "std"))))))

