(define-module (crates-io mu lt multindex) #:use-module (crates-io))

(define-public crate-multindex-0.1.0 (c (n "multindex") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.3") (d #t) (k 2)))) (h "1zwyzm93wn24hv9s02j1rgvk5fckryq5dq2km4i0kmcn7p25nzcn") (f (quote (("testing"))))))

(define-public crate-multindex-0.1.1 (c (n "multindex") (v "0.1.1") (d (list (d (n "fastrand") (r "^1.3") (d #t) (k 2)))) (h "12gdmk4a60idh749gj99jz97sa1nfg7nq9mwsv977lxh57my7y7r") (f (quote (("testing"))))))

(define-public crate-multindex-0.1.2 (c (n "multindex") (v "0.1.2") (d (list (d (n "fastrand") (r "^1.3") (d #t) (k 2)))) (h "04mbz5ygzzij7bam81livhibdp5h9kmm580zf14mkd3354y5ccc6") (f (quote (("testing"))))))

(define-public crate-multindex-0.1.3 (c (n "multindex") (v "0.1.3") (d (list (d (n "fastrand") (r "^1.3") (d #t) (k 2)))) (h "0j5mqshfw0qfv3rwx9i352l6zj9aqidvgx59izrri91mwxnv0lwn") (f (quote (("testing"))))))

