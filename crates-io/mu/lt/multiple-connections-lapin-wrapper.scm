(define-module (crates-io mu lt multiple-connections-lapin-wrapper) #:use-module (crates-io))

(define-public crate-multiple-connections-lapin-wrapper-0.1.0 (c (n "multiple-connections-lapin-wrapper") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0") (d #t) (k 0)) (d (n "lapin") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-executor-trait") (r "^2.1") (d #t) (k 0)) (d (n "tokio-reactor-trait") (r "^1.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "189f903d0qkk03cclrkrwrq0ms9zgnd6wvqnvjl3a1x0m98xxv96")))

