(define-module (crates-io mu lt multi_thread_web_crawler) #:use-module (crates-io))

(define-public crate-multi_thread_web_crawler-0.1.0 (c (n "multi_thread_web_crawler") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dpdqh16nrdrhrii1iv5idcxsz2pm4fnqhbc5725df63q19j4jmm")))

