(define-module (crates-io mu lt multiplex) #:use-module (crates-io))

(define-public crate-multiplex-0.1.0 (c (n "multiplex") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "atomic-counter") (r "^1.0.1") (d #t) (k 0)) (d (n "awc") (r "^1.0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5.5") (d #t) (k 0)) (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sq5cvg03q6kqcj8j23achczdpwzz0ncw07amh9fxjk93zs5wz58")))

