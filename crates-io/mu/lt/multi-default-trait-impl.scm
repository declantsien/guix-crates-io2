(define-module (crates-io mu lt multi-default-trait-impl) #:use-module (crates-io))

(define-public crate-multi-default-trait-impl-0.1.0 (c (n "multi-default-trait-impl") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l8i6hchlc1dhcbrybwz6d99qgd4biz48mnvcjvk0q9vfnz60fis")))

(define-public crate-multi-default-trait-impl-0.1.1 (c (n "multi-default-trait-impl") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l50rx8aprhrnbjx4k148zq4kwa4r5rgpskyx7qslzngm5msswqr")))

(define-public crate-multi-default-trait-impl-0.1.2 (c (n "multi-default-trait-impl") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r6y5zb6kg655zi02yk4amkwsgds5ay9ag1dk30cls7rn3dlvvqs")))

