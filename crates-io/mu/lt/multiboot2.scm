(define-module (crates-io mu lt multiboot2) #:use-module (crates-io))

(define-public crate-multiboot2-0.1.0 (c (n "multiboot2") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.4.0") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1m0nnbc98faf6m8z20cph85sk0dg8xnys66kkav1zj1wd92s82dj")))

(define-public crate-multiboot2-0.2.0 (c (n "multiboot2") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.4.0") (f (quote ("no_std"))) (d #t) (k 0)))) (h "172bhhhmsdn4wk7cslaq3hmlashjgpc7jyxlldrdjqi2n9vf12yg")))

(define-public crate-multiboot2-0.2.1 (c (n "multiboot2") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.4.0") (f (quote ("no_std"))) (d #t) (k 0)))) (h "045w0szq0zvjfhmq130mqdz8knlhm93x3c17zqsjsmylqxkhas4s")))

(define-public crate-multiboot2-0.3.0 (c (n "multiboot2") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.4.0") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0gjf4cdkhy3h3kw790sjjfdgva3xnn6d02iskz6sdmznfz8m9hmi")))

(define-public crate-multiboot2-0.3.1 (c (n "multiboot2") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.4.0") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1bbr83sxrwiljzf27g93zbyfx0g2g4khrkgrgfc3w80nnr9dpbl8")))

(define-public crate-multiboot2-0.3.2 (c (n "multiboot2") (v "0.3.2") (d (list (d (n "bitflags") (r "^0.4.0") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0sbzchwbaqra2j0xkf9xb3bqgi2xfkq8lax477m0f9hg6435ksnw")))

(define-public crate-multiboot2-0.5.0 (c (n "multiboot2") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)))) (h "0ibvvb2qsqr9pqlhg7yy5irlfma79q09qm0fnac96w0i7qpgj9k5")))

(define-public crate-multiboot2-0.6.0 (c (n "multiboot2") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)))) (h "1xwipcr77x2byvzx0sgxk9wpqpn825s13adp7w4sxncci1z7si6w")))

(define-public crate-multiboot2-0.7.0 (c (n "multiboot2") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)))) (h "11hssg5bfv5gaxv7m1wg13y7p4qzkk99k5kwkx40sv9asi3x6z8k")))

(define-public crate-multiboot2-0.7.1 (c (n "multiboot2") (v "0.7.1") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)))) (h "10fx1zjq2rd806j95kisajsymc0kf3ydaa0l6jmmr9k3nsdfifvf")))

(define-public crate-multiboot2-0.8.0 (c (n "multiboot2") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)))) (h "0rs1f4z2c9r5ycmvgsqkdilniw1gkzvhfhcc9rhjr334p59fpa31")))

(define-public crate-multiboot2-0.8.1 (c (n "multiboot2") (v "0.8.1") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)))) (h "0dx6px85y652i4c2066lmzpl5d69y8gfglbc6ikxdr288h4rhr2z")))

(define-public crate-multiboot2-0.8.2 (c (n "multiboot2") (v "0.8.2") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)))) (h "1mj03xiz7970wqbzi4ixb15rq8vn1ndpc01dm9w2q51qzqdmr0r4")))

(define-public crate-multiboot2-0.9.0 (c (n "multiboot2") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)))) (h "0ncpi2wb01c3id874b4mfly8dy9n07l4ih6dy7k97ik3xsj4k9p9")))

(define-public crate-multiboot2-0.10.0 (c (n "multiboot2") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)))) (h "0gakvbvfnn1fwm2r659ibcjl0cgpplnc824cn8lz9ci56c1bs0hr")))

(define-public crate-multiboot2-0.10.1 (c (n "multiboot2") (v "0.10.1") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)))) (h "1zdikn4pnq11wxm483j8dpicj6ysa2wn9jc4j3njvvzj1y4zrs8k")))

(define-public crate-multiboot2-0.11.0 (c (n "multiboot2") (v "0.11.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "1wyv2s31awsv64dsl29nb94q8qvg23bzmwnxnl8msc88zn8p7j2j")))

(define-public crate-multiboot2-0.12.0 (c (n "multiboot2") (v "0.12.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "1dwqhzkrg5wksbc3yp1ygqj7il8bm86dn3h8kpk8k2kxc9by8prv")))

(define-public crate-multiboot2-0.12.1 (c (n "multiboot2") (v "0.12.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0yr1zapkyr6f6grg9p6dnmg5k3bkz65rxcxj8c3davacamxf22aq")))

(define-public crate-multiboot2-0.12.2 (c (n "multiboot2") (v "0.12.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0raq0046422qiccrk342nig7vg9z9q65p19jyfabvvx3b7cg2kh0")))

(define-public crate-multiboot2-0.13.0 (c (n "multiboot2") (v "0.13.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0gm4h7ax8lav7klpmcm47fghc5ichdjqn6cxhg4wx67k0z701s7p") (y #t)))

(define-public crate-multiboot2-0.13.1 (c (n "multiboot2") (v "0.13.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0wc1h1924plrr6ywsyxgdahq7pvc9iim9akl8py6zbsmlkymb5m7")))

(define-public crate-multiboot2-0.13.2 (c (n "multiboot2") (v "0.13.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0vg6b1ny5k0h4k9wsdg6vbihdszpcnb0grdqnff57aqk4z3nj9zp")))

(define-public crate-multiboot2-0.13.3 (c (n "multiboot2") (v "0.13.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "18icyvsyplq2mg429j1h3c0nyy69r32ijjm2i3af3c54i40y226l")))

(define-public crate-multiboot2-0.14.0 (c (n "multiboot2") (v "0.14.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "13br1qyk9zk1fb3h9fd5qikachdhh3bkw7k2yp8dhxga29phn5z6")))

(define-public crate-multiboot2-0.14.1 (c (n "multiboot2") (v "0.14.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "020mf6g98nfkcv7nmj3pg04k8zrb08di1a8a5834dnz5q8vks3mx")))

(define-public crate-multiboot2-0.14.2 (c (n "multiboot2") (v "0.14.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)))) (h "0579zxz8pcb95rm6gh20lc5iygc6haav54mh8x1hqnwzs3230qs9") (f (quote (("unstable") ("default"))))))

(define-public crate-multiboot2-0.15.0 (c (n "multiboot2") (v "0.15.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)))) (h "016h0qahg7zzixbmk6il5wxm88b92h59w7vg7bmycg5w7vhd9vi0") (f (quote (("unstable") ("default"))))))

(define-public crate-multiboot2-0.15.1 (c (n "multiboot2") (v "0.15.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "log") (r "^0.4") (k 0)))) (h "095mpqkqb1wa7vq1z1ibn4asqc3yw41gmy7z3g9vsmv4dw8jkc0f") (f (quote (("unstable") ("default"))))))

(define-public crate-multiboot2-0.16.0 (c (n "multiboot2") (v "0.16.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "ptr_meta") (r "^0.2") (k 0)) (d (n "uefi-raw") (r "^0.3") (k 0)))) (h "06mk8zfzfzwypws1r7ahsi1p1sq7frhmni91ipck8ivh7xxllqjb") (f (quote (("unstable") ("default" "builder") ("builder" "alloc") ("alloc")))) (r "1.68")))

(define-public crate-multiboot2-0.17.0 (c (n "multiboot2") (v "0.17.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "ptr_meta") (r "^0.2") (k 0)) (d (n "uefi-raw") (r "^0.3") (k 0)))) (h "1d7d9nzb7d3xc43cf4xlfzj3a48zpcfq6ybalrf9kfjyy4bv2glb") (f (quote (("unstable") ("default" "builder") ("builder" "alloc") ("alloc")))) (r "1.68")))

(define-public crate-multiboot2-0.18.0 (c (n "multiboot2") (v "0.18.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "ptr_meta") (r "^0.2") (k 0)) (d (n "uefi-raw") (r "^0.3") (k 0)))) (h "1w6cp4r2wp8fxi0wljjlhdh455pq5gyh03hngx34sz685wk324g5") (f (quote (("unstable") ("default" "builder") ("builder" "alloc") ("alloc")))) (r "1.68")))

(define-public crate-multiboot2-0.18.1 (c (n "multiboot2") (v "0.18.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "ptr_meta") (r "^0.2") (k 0)) (d (n "uefi-raw") (r "^0.3") (k 0)))) (h "1m2sic03h3ajsh9jsfczxl1fwjyig4hkl6mkmg0v2ilcl5j7aam6") (f (quote (("unstable") ("default" "builder") ("builder" "alloc") ("alloc")))) (r "1.68")))

(define-public crate-multiboot2-0.19.0 (c (n "multiboot2") (v "0.19.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "ptr_meta") (r "^0.2") (k 0)) (d (n "uefi-raw") (r "^0.3") (k 0)))) (h "046hp7vx1ikrsl5hsn1sgdlcv2k3rgnfaq6fydx79xy23zbi6ymy") (f (quote (("unstable") ("default" "builder") ("builder" "alloc") ("alloc")))) (r "1.68")))

(define-public crate-multiboot2-0.20.0 (c (n "multiboot2") (v "0.20.0") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)) (d (n "derive_more") (r "~0.99") (f (quote ("display"))) (k 0)) (d (n "log") (r "~0.4") (k 0)) (d (n "ptr_meta") (r "~0.2") (k 0)) (d (n "uefi-raw") (r "~0.5") (k 0)))) (h "11kbplwxv4zgrwmk89zs8hn22whg16sa4y3c4br2g4dlc6sf2rrx") (f (quote (("unstable") ("default" "builder") ("builder" "alloc") ("alloc")))) (r "1.70")))

(define-public crate-multiboot2-0.20.1 (c (n "multiboot2") (v "0.20.1") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)) (d (n "derive_more") (r "~0.99") (f (quote ("display"))) (k 0)) (d (n "log") (r "~0.4") (k 0)) (d (n "ptr_meta") (r "~0.2") (k 0)) (d (n "uefi-raw") (r "~0.5") (k 0)))) (h "1z311b4imak76kwagik7h87vv6r0dd02gcq8gf8hmlpp48rj9xc5") (f (quote (("unstable") ("default" "builder") ("builder" "alloc") ("alloc")))) (r "1.70")))

(define-public crate-multiboot2-0.20.2 (c (n "multiboot2") (v "0.20.2") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)) (d (n "derive_more") (r "~0.99") (f (quote ("display"))) (k 0)) (d (n "log") (r "~0.4") (k 0)) (d (n "ptr_meta") (r "~0.2") (k 0)) (d (n "uefi-raw") (r "~0.5") (k 0)))) (h "1c0ln4477xhkpclxr1kavhcvjm6vqfm79j6h7wb5acvqsi4g0zmd") (f (quote (("unstable") ("default" "builder") ("builder" "alloc") ("alloc")))) (r "1.70")))

