(define-module (crates-io mu lt multi-seq-align) #:use-module (crates-io))

(define-public crate-multi-seq-align-0.1.0 (c (n "multi-seq-align") (v "0.1.0") (d (list (d (n "displaydoc") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1w0488spzx9s9a1mzc0d757gsgrlxiig7rm450bz7dg63c594ykd")))

(define-public crate-multi-seq-align-0.2.0 (c (n "multi-seq-align") (v "0.2.0") (d (list (d (n "displaydoc") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hawmig9xrj75w2z8mkky0an9sfrpm8lhq725a9j9lpjlszmqw1y")))

