(define-module (crates-io mu lt multiset-hash) #:use-module (crates-io))

(define-public crate-multiset-hash-0.1.0 (c (n "multiset-hash") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^3.1.0") (d #t) (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 2)))) (h "0sqgxx9w8rr470pfdzvwbkmh6n76b1dihrnmnvx0laiz9x6n6iq7")))

(define-public crate-multiset-hash-0.2.0 (c (n "multiset-hash") (v "0.2.0") (d (list (d (n "curve25519-dalek") (r "^3.1.0") (d #t) (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 2)))) (h "0xj78bfk3zd1kw8x893332pifxrmkgwkwb2y3jw0pq9a6ywr181y")))

