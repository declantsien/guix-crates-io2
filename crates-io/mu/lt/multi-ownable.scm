(define-module (crates-io mu lt multi-ownable) #:use-module (crates-io))

(define-public crate-multi-ownable-0.1.0 (c (n "multi-ownable") (v "0.1.0") (d (list (d (n "near-sdk") (r "^4.0.0-pre.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)))) (h "0x1lvldz90vicybk9bfg6k7pgxkadpykw9cbwlzzfi5438c7va3k")))

(define-public crate-multi-ownable-0.1.1 (c (n "multi-ownable") (v "0.1.1") (d (list (d (n "near-sdk") (r "^4.0.0-pre.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)))) (h "0whlpck31q4sg5hlm41syc45kwcgq8ymki468drl5gcrim3w5x4d")))

(define-public crate-multi-ownable-0.1.2 (c (n "multi-ownable") (v "0.1.2") (d (list (d (n "near-sdk") (r "^4.0.0-pre.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)))) (h "1fygy23z2ci82x2c76zcz5w21rhkbb6z5fq37d2k3nn05siw3jh2")))

