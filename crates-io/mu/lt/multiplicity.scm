(define-module (crates-io mu lt multiplicity) #:use-module (crates-io))

(define-public crate-multiplicity-0.1.0 (c (n "multiplicity") (v "0.1.0") (d (list (d (n "bls12_381") (r "^0.6.0") (d #t) (k 2)) (d (n "ff") (r "^0.11") (d #t) (k 0)))) (h "06mxhyqclfm2fcq8zpb9w0cblx5mihikcdnwsdz3sxhz6ps5janq")))

(define-public crate-multiplicity-0.1.1 (c (n "multiplicity") (v "0.1.1") (d (list (d (n "bls12_381") (r "^0.6.0") (d #t) (k 2)) (d (n "ff") (r "^0.11") (d #t) (k 0)))) (h "1kphg0b27vyccp6877fqv5mgrbibfqdvfg70hfikh9rbnd5sbzdl")))

