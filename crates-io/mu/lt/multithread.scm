(define-module (crates-io mu lt multithread) #:use-module (crates-io))

(define-public crate-multithread-0.1.0-pre.1 (c (n "multithread") (v "0.1.0-pre.1") (h "07q101r75y3z55yy6lp76synydbpi3fgkh3lhyv44k7f8sb758k9")))

(define-public crate-multithread-0.1.0-pre.2 (c (n "multithread") (v "0.1.0-pre.2") (h "1dkx4xg1wbcv3gn1328cfsawc8zfmdm4vqf4q4cvqmqgxnd82gl9")))

(define-public crate-multithread-0.1.0-pre.3 (c (n "multithread") (v "0.1.0-pre.3") (h "1xar8k6sg6f4bfq5zyqkdbk9m54pxs6gnpyhiirlpny70jc372y9")))

(define-public crate-multithread-0.1.0-pre.4 (c (n "multithread") (v "0.1.0-pre.4") (h "1ga561kghmb1dz1av45gcrvl2zkqym74dinc9ddgbh5c0giw6r0c")))

