(define-module (crates-io mu lt multi-window-output) #:use-module (crates-io))

(define-public crate-multi-window-output-0.1.0 (c (n "multi-window-output") (v "0.1.0") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0sjx5cd21cb3lhmihxdd4hshvm147926lwh2c0z8fxbcrl4jygjb") (y #t)))

(define-public crate-multi-window-output-0.1.1 (c (n "multi-window-output") (v "0.1.1") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "05vqfipza8g3wd5spwff0xkl2rl5ig17j7f8farjwhwcjrfl7q7h")))

(define-public crate-multi-window-output-0.1.2 (c (n "multi-window-output") (v "0.1.2") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "17hd5rd9hy56c73ia6n510s8s6g2s0j7bdvhw0iiw1752mx2zgyj") (y #t)))

(define-public crate-multi-window-output-0.1.3 (c (n "multi-window-output") (v "0.1.3") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0vikvrrb344rqygk8bnyirffaxnacj1y3533mhpiiwb7bkjwg6x3") (y #t)))

(define-public crate-multi-window-output-0.1.4 (c (n "multi-window-output") (v "0.1.4") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "01ma6yqvymjdbs6cnxmgjr3j29101n37clqqnxfcsi3b04ghixxf")))

