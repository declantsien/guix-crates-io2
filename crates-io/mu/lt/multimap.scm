(define-module (crates-io mu lt multimap) #:use-module (crates-io))

(define-public crate-multimap-0.1.0 (c (n "multimap") (v "0.1.0") (h "0rbv78g59wm8i06pih4b4c20g46232zra0izl885kc6n79zk7784")))

(define-public crate-multimap-0.2.0 (c (n "multimap") (v "0.2.0") (h "1m6mjl7vhmnhlx090mnw8dwdfckhykwg2a340qhh90w1kyvvcp78")))

(define-public crate-multimap-0.3.0 (c (n "multimap") (v "0.3.0") (h "1wpvfmljvi05ca5cfjdcgcw3s931fnd5nmafwj2n3q089mvz88wj")))

(define-public crate-multimap-0.4.0 (c (n "multimap") (v "0.4.0") (d (list (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "~1.0") (d #t) (k 2)))) (h "0l81iahqd4x66ibxvvhxwa3kny9ydznbjgqndqbyv0vm2aglpc1f") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

(define-public crate-multimap-0.5.0 (c (n "multimap") (v "0.5.0") (d (list (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "~1.0") (d #t) (k 2)))) (h "1fpk7f97nfqf5nb5cg3hbnplfngc2950w0kr4nkbjlscwq2zwf0q") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

(define-public crate-multimap-0.6.0 (c (n "multimap") (v "0.6.0") (d (list (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "~1.0") (d #t) (k 2)))) (h "18yg7760987rqfdpb29wbvmmmmv112niigmr0dqhm0slin0ly8yy") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

(define-public crate-multimap-0.7.0 (c (n "multimap") (v "0.7.0") (d (list (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "~1.0") (d #t) (k 2)))) (h "1sbwm9qhxja9lgx87y4ziqz6dri3vp0lbq352vxaj7wyd42a9ccp") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

(define-public crate-multimap-0.8.0 (c (n "multimap") (v "0.8.0") (d (list (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "~1.0") (d #t) (k 2)))) (h "1p2897583jl6hb19zp38jdriwqxgba7syczl23xppqz001fvszx9") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

(define-public crate-multimap-0.8.1 (c (n "multimap") (v "0.8.1") (d (list (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "~1.0") (d #t) (k 2)))) (h "1kjqm707yz63adj6jby63as98ajhijdrql8g1g9w2mlpvvgkm26q") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

(define-public crate-multimap-0.8.2 (c (n "multimap") (v "0.8.2") (d (list (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "~1.0") (d #t) (k 2)))) (h "0cq3hlqwyxz0hmcpbajghhc832ln6h0qszvf89kv8fx875hhfm8j") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

(define-public crate-multimap-0.8.3 (c (n "multimap") (v "0.8.3") (d (list (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "~1.0") (d #t) (k 2)))) (h "0sicyz4n500vdhgcxn4g8jz97cp1ijir1rnbgph3pmx9ckz4dkp5") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

(define-public crate-multimap-0.9.0 (c (n "multimap") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0c86fdd0vdgf9v30w55dqxajg8q7l359hy54v4v6mhrxm5495nvh") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

(define-public crate-multimap-0.9.1 (c (n "multimap") (v "0.9.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "04fmnxhpawndrr5x730s3gb4h77ldbrnlww86a8vsb9mkf5x79g1") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

(define-public crate-multimap-0.10.0 (c (n "multimap") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "00vs2frqdhrr8iqx4y3fbq73ax5l12837fvbjrpi729d85alrz6y") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

