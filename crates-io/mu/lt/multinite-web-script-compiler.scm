(define-module (crates-io mu lt multinite-web-script-compiler) #:use-module (crates-io))

(define-public crate-multinite-web-script-compiler-0.1.0 (c (n "multinite-web-script-compiler") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.8") (f (quote ("pattern"))) (d #t) (k 0)))) (h "13q3qxnr8221icqqcxy967m5qhzkm3xlbsg625r76i2jrhn810h7") (y #t)))

(define-public crate-multinite-web-script-compiler-1.0.0 (c (n "multinite-web-script-compiler") (v "1.0.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.8") (f (quote ("pattern"))) (d #t) (k 0)))) (h "06c4yqclmrcjwppj9kaqj0xzp7d8rszpa771qwpwfax3jpsvvf9l")))

(define-public crate-multinite-web-script-compiler-1.1.0 (c (n "multinite-web-script-compiler") (v "1.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.8") (f (quote ("pattern"))) (d #t) (k 0)))) (h "0lbqri8pqxggrwgp4ps4r6v23xllzdjcw2xiay5z07fkcwhc3naa")))

(define-public crate-multinite-web-script-compiler-1.1.1 (c (n "multinite-web-script-compiler") (v "1.1.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.8") (f (quote ("pattern"))) (d #t) (k 0)))) (h "19mpca24a1jhad5bhw51sc7iw2559lv91fvyl5mbn4bncb9b86yl")))

