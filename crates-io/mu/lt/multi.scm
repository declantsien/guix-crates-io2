(define-module (crates-io mu lt multi) #:use-module (crates-io))

(define-public crate-multi-0.1.0 (c (n "multi") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04c0j0kbacijansjmg9aircn64pwxwfb11wgn3bcv9xgzw0n3xnc")))

(define-public crate-multi-0.1.1 (c (n "multi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fnxqm42svd3x6w5mfj4b8dfv4l6hjyslnhigc8z7rp3hskk25hy")))

