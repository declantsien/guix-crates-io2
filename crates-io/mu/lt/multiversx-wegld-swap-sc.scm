(define-module (crates-io mu lt multiversx-wegld-swap-sc) #:use-module (crates-io))

(define-public crate-multiversx-wegld-swap-sc-0.39.0 (c (n "multiversx-wegld-swap-sc") (v "0.39.0") (d (list (d (n "multiversx-sc") (r "^0.39.0") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.39.0") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.39.0") (d #t) (k 2)))) (h "1m82wfjry0x0b98krnzhm47lmfvvncggh1nz2212z3wlhz2r1p4g")))

(define-public crate-multiversx-wegld-swap-sc-0.39.1 (c (n "multiversx-wegld-swap-sc") (v "0.39.1") (d (list (d (n "multiversx-sc") (r "^0.39.1") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.39.1") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.39.1") (d #t) (k 2)))) (h "158vanj202f68kynpak815ki16lc91a5i79a9jxngy608pg6k6xh")))

(define-public crate-multiversx-wegld-swap-sc-0.39.2 (c (n "multiversx-wegld-swap-sc") (v "0.39.2") (d (list (d (n "multiversx-sc") (r "^0.39.2") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.39.2") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.39.2") (d #t) (k 2)))) (h "0nganymdm8yrlp3x3sbiy186ng9a48im9g10xxnr1b9yplpj3007")))

(define-public crate-multiversx-wegld-swap-sc-0.39.3 (c (n "multiversx-wegld-swap-sc") (v "0.39.3") (d (list (d (n "multiversx-sc") (r "^0.39.3") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.39.3") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.39.3") (d #t) (k 2)))) (h "00mgqz4rw8zc12kf28a4lfx0c877b30fir4h6r8k7kv1ifys4qfy")))

(define-public crate-multiversx-wegld-swap-sc-0.39.4 (c (n "multiversx-wegld-swap-sc") (v "0.39.4") (d (list (d (n "multiversx-sc") (r "^0.39.4") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.39.4") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.39.4") (d #t) (k 2)))) (h "0iqwk46xqq69087hxgypryqzg5qwaz1dc3pn4kxbzn1s1szqllzd")))

(define-public crate-multiversx-wegld-swap-sc-0.39.5 (c (n "multiversx-wegld-swap-sc") (v "0.39.5") (d (list (d (n "multiversx-sc") (r "^0.39.5") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.39.5") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.39.5") (d #t) (k 2)))) (h "0h72h65c0cd7cs62a8kc1nhdlaxinly3988h2b5qiv0an3m7404m")))

(define-public crate-multiversx-wegld-swap-sc-0.39.6 (c (n "multiversx-wegld-swap-sc") (v "0.39.6") (d (list (d (n "multiversx-sc") (r "^0.39.6") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.39.6") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.39.6") (d #t) (k 2)))) (h "0nik62ap19wibf92qi6kd70rqfw5dphwlg9vdp20kjnayjwr47b1")))

(define-public crate-multiversx-wegld-swap-sc-0.39.7 (c (n "multiversx-wegld-swap-sc") (v "0.39.7") (d (list (d (n "multiversx-sc") (r "^0.39.7") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.39.7") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.39.7") (d #t) (k 2)))) (h "0pa2pk22h447i99xj0f7z8j4n79l2jr2wsqwk35rscq29dn4aa99")))

(define-public crate-multiversx-wegld-swap-sc-0.39.8 (c (n "multiversx-wegld-swap-sc") (v "0.39.8") (d (list (d (n "multiversx-sc") (r "^0.39.8") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.39.8") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.39.8") (d #t) (k 2)))) (h "1maqp2y3ivb1cl0fycj2y02mhmqaaghiswa7mx508mzy9b02rw3a")))

(define-public crate-multiversx-wegld-swap-sc-0.40.0 (c (n "multiversx-wegld-swap-sc") (v "0.40.0") (d (list (d (n "multiversx-sc") (r "^0.40.0") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.40.0") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.40.0") (d #t) (k 2)))) (h "0l7k81kfg46k0f5pv1kk8lglakzklh4fd3m49bggkbjha9ivdc57")))

(define-public crate-multiversx-wegld-swap-sc-0.40.1 (c (n "multiversx-wegld-swap-sc") (v "0.40.1") (d (list (d (n "multiversx-sc") (r "^0.40.1") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.40.1") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.40.1") (d #t) (k 2)))) (h "089xybn3x2nzs8q2l5hc4kvza8017ss6akh2fawkqsiknrkfpj4x")))

(define-public crate-multiversx-wegld-swap-sc-0.41.0 (c (n "multiversx-wegld-swap-sc") (v "0.41.0") (d (list (d (n "multiversx-sc") (r "^0.41.0") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.41.0") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.41.0") (d #t) (k 2)))) (h "0kn121v965rj489pn58bw48pdfd0cinfglqac8yvf5q9wxqm9g59")))

(define-public crate-multiversx-wegld-swap-sc-0.41.1 (c (n "multiversx-wegld-swap-sc") (v "0.41.1") (d (list (d (n "multiversx-sc") (r "^0.41.1") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.41.1") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.41.1") (d #t) (k 2)))) (h "07sdsx5al17c8fqz6bcqnqqhwdnw5nj5wph3qfz8g04ds56dnmp9")))

(define-public crate-multiversx-wegld-swap-sc-0.41.2 (c (n "multiversx-wegld-swap-sc") (v "0.41.2") (d (list (d (n "multiversx-sc") (r "^0.41.2") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.41.2") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.41.2") (d #t) (k 2)))) (h "1f40pj4c5hj71mix2gqm7k1j2gn4h4b5cy2mn0p5cfk0sqjp8kzk")))

(define-public crate-multiversx-wegld-swap-sc-0.41.3 (c (n "multiversx-wegld-swap-sc") (v "0.41.3") (d (list (d (n "multiversx-sc") (r "^0.41.3") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.41.3") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.41.3") (d #t) (k 2)))) (h "1mdds1j1abn76l2ai2hbyflwczw0h49faz9jgs91sryhjxwwcskn")))

(define-public crate-multiversx-wegld-swap-sc-0.42.0 (c (n "multiversx-wegld-swap-sc") (v "0.42.0") (d (list (d (n "multiversx-sc") (r "^0.42.0") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.42.0") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.42.0") (d #t) (k 2)))) (h "1hr9h2szyaz5mqxiaisxycqqcv9xcn2ggl6nibfxi37y11ja3k1b")))

(define-public crate-multiversx-wegld-swap-sc-0.43.0 (c (n "multiversx-wegld-swap-sc") (v "0.43.0") (d (list (d (n "multiversx-sc") (r "^0.43.0") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.43.0") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.43.0") (d #t) (k 2)))) (h "1w82aba62z4c1q7385lwnb1ada27b3jyk4yn0bm2m592ryjx4jzy")))

(define-public crate-multiversx-wegld-swap-sc-0.43.1 (c (n "multiversx-wegld-swap-sc") (v "0.43.1") (d (list (d (n "multiversx-sc") (r "^0.43.1") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.43.1") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.43.1") (d #t) (k 2)))) (h "1gw6ch872l4ycjb59zgvfmw7l150bhk7wlwvzydjz1s75jd21bxr")))

(define-public crate-multiversx-wegld-swap-sc-0.43.2 (c (n "multiversx-wegld-swap-sc") (v "0.43.2") (d (list (d (n "multiversx-sc") (r "^0.43.2") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.43.2") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.43.2") (d #t) (k 2)))) (h "02bl74im5mvc5ac6vv6a0r7y9ydh80nvy819dvpq785jbh1qx47m")))

(define-public crate-multiversx-wegld-swap-sc-0.43.3 (c (n "multiversx-wegld-swap-sc") (v "0.43.3") (d (list (d (n "multiversx-sc") (r "^0.43.3") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.43.3") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.43.3") (d #t) (k 2)))) (h "0hwha2cpqiy6mn5sdwiz3ypw5ir3b5d9757s7kklqcc6kh9n1yyi")))

(define-public crate-multiversx-wegld-swap-sc-0.43.4 (c (n "multiversx-wegld-swap-sc") (v "0.43.4") (d (list (d (n "multiversx-sc") (r "^0.43.4") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.43.4") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.43.4") (d #t) (k 2)))) (h "01gvvpn2gddbl9ddkn4lqri6g6gdz70bfnvhf2l2ls3xm8q1cvnj")))

(define-public crate-multiversx-wegld-swap-sc-0.43.5 (c (n "multiversx-wegld-swap-sc") (v "0.43.5") (d (list (d (n "multiversx-sc") (r "^0.43.5") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.43.5") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.43.5") (d #t) (k 2)))) (h "1mymx8n6k1gmlnxgvkfnv8n38vn3kjg433jd7a0c4r1l8y0vj1qx")))

(define-public crate-multiversx-wegld-swap-sc-0.44.0 (c (n "multiversx-wegld-swap-sc") (v "0.44.0") (d (list (d (n "multiversx-sc") (r "^0.44.0") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.44.0") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.44.0") (d #t) (k 2)))) (h "0yn73y8fln5i8yfbrwmqymby0ybiwzlrmv16i7dh14azjim5qhnw")))

(define-public crate-multiversx-wegld-swap-sc-0.45.0 (c (n "multiversx-wegld-swap-sc") (v "0.45.0") (d (list (d (n "multiversx-sc") (r "^0.45.0") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.45.0") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.45.0") (d #t) (k 2)))) (h "061l302f4kwsra7bcrv0s5yc18kqdr03rwc99lzdfgg5698cknjx")))

(define-public crate-multiversx-wegld-swap-sc-0.45.1 (c (n "multiversx-wegld-swap-sc") (v "0.45.1") (d (list (d (n "multiversx-sc") (r "^0.45.1") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.45.1") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.45.1") (d #t) (k 2)))) (h "12b99hvisgszj0zny16hnpzwc0br5mqjzl1322ipqpyzsvmmv7m1")))

(define-public crate-multiversx-wegld-swap-sc-0.45.2 (c (n "multiversx-wegld-swap-sc") (v "0.45.2") (d (list (d (n "multiversx-sc") (r "^0.45.2") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.45.2") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.45.2") (d #t) (k 2)))) (h "0qppllcaq1fpmq55c5x1pprngiq1y2jjy2mrkrwm8ryrazr5la67")))

(define-public crate-multiversx-wegld-swap-sc-0.46.0 (c (n "multiversx-wegld-swap-sc") (v "0.46.0") (d (list (d (n "multiversx-sc") (r "^0.46.0") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.46.0") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.46.0") (d #t) (k 2)))) (h "1f16ai5235kqc4z8ml3j2l2pbmb6gj9v3mgrf4lbhpp500xdr70m")))

(define-public crate-multiversx-wegld-swap-sc-0.46.1 (c (n "multiversx-wegld-swap-sc") (v "0.46.1") (d (list (d (n "multiversx-sc") (r "^0.46.1") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.46.1") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.46.1") (d #t) (k 2)))) (h "1lqg531lv76yirvzhszykwcgplh7bpdhy3isjalw3fw82xas7lmp")))

(define-public crate-multiversx-wegld-swap-sc-0.47.0 (c (n "multiversx-wegld-swap-sc") (v "0.47.0") (d (list (d (n "multiversx-sc") (r "^0.47.0") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.47.0") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.47.0") (d #t) (k 2)))) (h "1k7ylqis1i0q8q4qsv7slx8zx6jxdp3y5xpyjx7jmfh1l8v102yc")))

(define-public crate-multiversx-wegld-swap-sc-0.47.1 (c (n "multiversx-wegld-swap-sc") (v "0.47.1") (d (list (d (n "multiversx-sc") (r "^0.47.1") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.47.1") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.47.1") (d #t) (k 2)))) (h "0n6801ygnw7yyrgjjzdk2n2ir7pnw9rw6blq5y3kk6iv9bsczvmn")))

(define-public crate-multiversx-wegld-swap-sc-0.47.2 (c (n "multiversx-wegld-swap-sc") (v "0.47.2") (d (list (d (n "multiversx-sc") (r "^0.47.2") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.47.2") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.47.2") (d #t) (k 2)))) (h "0v8iiiwssq63v7hnr50xjyf5zfdz39x9fxwc9n7wmhyn90fh607b")))

(define-public crate-multiversx-wegld-swap-sc-0.47.3 (c (n "multiversx-wegld-swap-sc") (v "0.47.3") (d (list (d (n "multiversx-sc") (r "^0.47.3") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.47.3") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.47.3") (d #t) (k 2)))) (h "17nm3wd6h7la2mn69ymvv8pl9dxd6d4jr18yv7wck4d3aziig3b4")))

(define-public crate-multiversx-wegld-swap-sc-0.47.4 (c (n "multiversx-wegld-swap-sc") (v "0.47.4") (d (list (d (n "multiversx-sc") (r "^0.47.4") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.47.4") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.47.4") (d #t) (k 2)))) (h "01b6w10qb8r8rfbhlm00ig73drab3qnpj123m6rqx7a4qafz7sks")))

(define-public crate-multiversx-wegld-swap-sc-0.47.5 (c (n "multiversx-wegld-swap-sc") (v "0.47.5") (d (list (d (n "multiversx-sc") (r "^0.47.5") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.47.5") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.47.5") (d #t) (k 2)))) (h "1rpqnqmbpyivi34z2alx9cpxr2y6db1ghjaf64pih2icd5p2zg4w")))

(define-public crate-multiversx-wegld-swap-sc-0.47.6 (c (n "multiversx-wegld-swap-sc") (v "0.47.6") (d (list (d (n "multiversx-sc") (r "^0.47.6") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.47.6") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.47.6") (d #t) (k 2)))) (h "14ssmmwv1z52wa8ahyj8jqx1zzq4blw7lzfapvqa3nx0nqrsr175")))

(define-public crate-multiversx-wegld-swap-sc-0.47.7 (c (n "multiversx-wegld-swap-sc") (v "0.47.7") (d (list (d (n "multiversx-sc") (r "^0.47.7") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.47.7") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.47.7") (d #t) (k 2)))) (h "1m21d33y42537pn1g4b8r64241gvryrlk0rhhmmcw06wr8z4kfbn")))

(define-public crate-multiversx-wegld-swap-sc-0.47.8 (c (n "multiversx-wegld-swap-sc") (v "0.47.8") (d (list (d (n "multiversx-sc") (r "^0.47.8") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.47.8") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.47.8") (d #t) (k 2)))) (h "12rhlpjavz01vynfn611lf049r1l00ag8yjn3f92ya3q73kj7dp9")))

(define-public crate-multiversx-wegld-swap-sc-0.48.0-alpha.1 (c (n "multiversx-wegld-swap-sc") (v "0.48.0-alpha.1") (d (list (d (n "multiversx-sc") (r "^0.48.0-alpha.1") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.48.0-alpha.1") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.48.0-alpha.1") (d #t) (k 2)))) (h "14lsi540xjghy1ckq2j7p237brj4zcimlh6228byvq53kk47css6")))

(define-public crate-multiversx-wegld-swap-sc-0.48.0 (c (n "multiversx-wegld-swap-sc") (v "0.48.0") (d (list (d (n "multiversx-sc") (r "^0.48.0") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.48.0") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.48.0") (d #t) (k 2)))) (h "0kz41xv75qplid4n4k5l7iqfdqccddh4sdv1b2ay1m7cl3wk9w0z")))

(define-public crate-multiversx-wegld-swap-sc-0.49.0-alpha.2 (c (n "multiversx-wegld-swap-sc") (v "0.49.0-alpha.2") (d (list (d (n "multiversx-sc") (r "^0.49.0-alpha.2") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.49.0-alpha.2") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.49.0-alpha.2") (d #t) (k 2)))) (h "0cjjlcc4l5gv2rh38abcny2x548vd92j6pvhi1rmcsa7cw6mnxv9")))

(define-public crate-multiversx-wegld-swap-sc-0.49.0-alpha.3 (c (n "multiversx-wegld-swap-sc") (v "0.49.0-alpha.3") (d (list (d (n "multiversx-sc") (r "^0.49.0-alpha.3") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.49.0-alpha.3") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.49.0-alpha.3") (d #t) (k 2)))) (h "1vk3va1va3pracgvg109m132y2a9ghbzczapbbyxw2sxxbnnf0mv")))

(define-public crate-multiversx-wegld-swap-sc-0.49.0-alpha.4 (c (n "multiversx-wegld-swap-sc") (v "0.49.0-alpha.4") (d (list (d (n "multiversx-sc") (r "^0.49.0-alpha.4") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.49.0-alpha.4") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.49.0-alpha.4") (d #t) (k 2)))) (h "1mdqad30dznnvnahnlk6kq8mn17k487cfk69vlh4nhqfzvqa55fh")))

(define-public crate-multiversx-wegld-swap-sc-0.48.1 (c (n "multiversx-wegld-swap-sc") (v "0.48.1") (d (list (d (n "multiversx-sc") (r "^0.48.1") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.48.1") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.48.1") (d #t) (k 2)))) (h "0ynhkgsk34g4fmkn0s8sf6fz1x9yn1v3l0fnvq9pj91iwj55dq0i")))

(define-public crate-multiversx-wegld-swap-sc-0.49.0 (c (n "multiversx-wegld-swap-sc") (v "0.49.0") (d (list (d (n "multiversx-sc") (r "^0.49.0") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.49.0") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.49.0") (d #t) (k 2)))) (h "06x6a5yifgngpirbj1xv8sbkif4jby7s7x6ixzcpdk18820bc5kd")))

(define-public crate-multiversx-wegld-swap-sc-0.50.0 (c (n "multiversx-wegld-swap-sc") (v "0.50.0") (d (list (d (n "multiversx-sc") (r "^0.50.0") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.50.0") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.50.0") (d #t) (k 2)))) (h "0x3vradr93kcfrkz7zv3bzdgnplxpz2dldg2iqlmvrwq52mimac9")))

(define-public crate-multiversx-wegld-swap-sc-0.50.1 (c (n "multiversx-wegld-swap-sc") (v "0.50.1") (d (list (d (n "multiversx-sc") (r "^0.50.1") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.50.1") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.50.1") (d #t) (k 2)))) (h "12k0nzqj5zlhwgpc5wjavpb2hxggdbmzc9hm1s41zj1vryrxkfy8")))

(define-public crate-multiversx-wegld-swap-sc-0.50.2 (c (n "multiversx-wegld-swap-sc") (v "0.50.2") (d (list (d (n "multiversx-sc") (r "^0.50.2") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.50.2") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.50.2") (d #t) (k 2)))) (h "1gyqriap25rsdkiq0j0kq65d2190dslzw75rdnx3wlh3zp20asah")))

(define-public crate-multiversx-wegld-swap-sc-0.50.3 (c (n "multiversx-wegld-swap-sc") (v "0.50.3") (d (list (d (n "multiversx-sc") (r "^0.50.3") (d #t) (k 0)) (d (n "multiversx-sc-modules") (r "^0.50.3") (d #t) (k 0)) (d (n "multiversx-sc-scenario") (r "^0.50.3") (d #t) (k 2)))) (h "1kij3rm69kr766mn26c193rkf1086sk30xa0c8spzq09w8zamlvj")))

