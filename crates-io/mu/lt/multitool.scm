(define-module (crates-io mu lt multitool) #:use-module (crates-io))

(define-public crate-multitool-0.0.1 (c (n "multitool") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rillrate") (r "^0.36.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.20.0") (d #t) (k 0)))) (h "0f5i9lypbrwj89j1lw1hg87gx9vxygjl8iiwrylz8gvjpc2lzpnv")))

