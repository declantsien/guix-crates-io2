(define-module (crates-io mu lt multiversx-sc-codec-derive) #:use-module (crates-io))

(define-public crate-multiversx-sc-codec-derive-0.17.0 (c (n "multiversx-sc-codec-derive") (v "0.17.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11k7yd13sw74chs59666s1y0cx0kl1xcvssdqf50cpzaqhlsn6h8") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-multiversx-sc-codec-derive-0.17.1 (c (n "multiversx-sc-codec-derive") (v "0.17.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yi6i8rd3z6kc3dla2hm9k9xhpgrjpk0xh9914a1czrna4nh0xp9") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-multiversx-sc-codec-derive-0.17.2 (c (n "multiversx-sc-codec-derive") (v "0.17.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0c2ss2jrspr0h660c2pnf2a0czpchfgsl6nm1z37z9pxh07r5mis") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-multiversx-sc-codec-derive-0.18.0 (c (n "multiversx-sc-codec-derive") (v "0.18.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vlw15z74hjas8j0gszzfiiilbf2w3kylcryjlm4bw7wci9pkaqj") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-multiversx-sc-codec-derive-0.18.1 (c (n "multiversx-sc-codec-derive") (v "0.18.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b5jiyvqiw0ha0r0lbdwgz6n2ah45djvq1a8qn5qzn6n5ivc0yws") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-multiversx-sc-codec-derive-0.18.2 (c (n "multiversx-sc-codec-derive") (v "0.18.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dbdb0m3dyvigwrz901mci53rjiaki3m4sdj2slfvf2302706i7l") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-multiversx-sc-codec-derive-0.18.3 (c (n "multiversx-sc-codec-derive") (v "0.18.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "18m5zdjii77fawg3c0gdhgnf6qzdw8nidval5ycj1lyaz51kpc6k") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-multiversx-sc-codec-derive-0.18.4 (c (n "multiversx-sc-codec-derive") (v "0.18.4") (d (list (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.76") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (d #t) (k 0)))) (h "1zj037cg1k52nhzy71kifbljj65gxcdz03147iihaggx3xj6hn9r") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-multiversx-sc-codec-derive-0.18.5 (c (n "multiversx-sc-codec-derive") (v "0.18.5") (d (list (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.78") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (d #t) (k 0)))) (h "13hsv1f7582zlqpcff68dam9l1321jrqikhricnn0x6cdc544fgm") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-multiversx-sc-codec-derive-0.18.6 (c (n "multiversx-sc-codec-derive") (v "0.18.6") (d (list (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.78") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (d #t) (k 0)))) (h "00qbgfnwr7vp3cpdw0aj16cqiik83g15kxbcinf6b57w6x5ls733") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-multiversx-sc-codec-derive-0.18.7 (c (n "multiversx-sc-codec-derive") (v "0.18.7") (d (list (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.78") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (d #t) (k 0)))) (h "0c41gfh5ds1zb4vnvhz1221y39dc3b1x4m3djm7lppkmx5wjrjax") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-multiversx-sc-codec-derive-0.18.8 (c (n "multiversx-sc-codec-derive") (v "0.18.8") (d (list (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.78") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (d #t) (k 0)))) (h "04480y54c079phvlzby4135f8pa2cx385l9pvnfj0zx8xji2sczk") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-multiversx-sc-codec-derive-0.19.0 (c (n "multiversx-sc-codec-derive") (v "0.19.0") (d (list (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.82") (d #t) (k 0)) (d (n "quote") (r "=1.0.36") (d #t) (k 0)) (d (n "syn") (r "=2.0.61") (d #t) (k 0)))) (h "054cmbqppa7yv2fcdvqgar9yhsn6nihly3cg1gd1kg505l2ahwng") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

