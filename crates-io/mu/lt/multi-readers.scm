(define-module (crates-io mu lt multi-readers) #:use-module (crates-io))

(define-public crate-multi-readers-0.1.0 (c (n "multi-readers") (v "0.1.0") (h "0ahz8hlkm7h044lxjim61lm7jvcawcf9g5lqf2wyn3diai7kw7a4") (y #t)))

(define-public crate-multi-readers-0.1.1 (c (n "multi-readers") (v "0.1.1") (h "1l685ia463jwbv8gsrcgaa87993x5zd17k9xl811p6sdr6wr0ysk")))

(define-public crate-multi-readers-0.1.2 (c (n "multi-readers") (v "0.1.2") (h "13wiypp2ycqb8rbr6airxdcrxrhh8yggw4az2h8scccba9az6ww4")))

(define-public crate-multi-readers-0.1.3 (c (n "multi-readers") (v "0.1.3") (h "1z4gcxjgxmag88c5gb5fs30l4dfcqdacqvhb8iny2xiqavglf05g")))

(define-public crate-multi-readers-0.1.4 (c (n "multi-readers") (v "0.1.4") (h "12ywf3sbm66riwzxs1zq01x0yzkhrpix2fx3fd43x1i47qab649a")))

(define-public crate-multi-readers-0.1.5 (c (n "multi-readers") (v "0.1.5") (d (list (d (n "tokio") (r "^1.37.0") (o #t) (d #t) (k 0)))) (h "17h7n7xjh0ldw45plsal29ki79r849km45912c8dffblpnnsnkzl") (f (quote (("async" "tokio"))))))

(define-public crate-multi-readers-0.1.6 (c (n "multi-readers") (v "0.1.6") (d (list (d (n "tokio") (r "^1.37.0") (o #t) (d #t) (k 0)))) (h "171pprsily361h2qm4518mii928fcygyhkbds5mllbd1z10k32nw") (f (quote (("async" "tokio"))))))

