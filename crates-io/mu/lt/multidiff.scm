(define-module (crates-io mu lt multidiff) #:use-module (crates-io))

(define-public crate-multidiff-0.0.0 (c (n "multidiff") (v "0.0.0") (d (list (d (n "similar") (r "^2.3") (k 0)))) (h "1ngpmx0awf8smx8vnki9wdbbm6hvspilg1wndi4siknwb7kcdan5") (f (quote (("std") ("default" "std")))) (r "1.56")))

