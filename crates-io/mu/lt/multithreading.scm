(define-module (crates-io mu lt multithreading) #:use-module (crates-io))

(define-public crate-multithreading-0.1.0 (c (n "multithreading") (v "0.1.0") (h "1rmyk9brhmrdwijmxi89wvl247aa92cpp1f2rrybd5yhlxp5176r")))

(define-public crate-multithreading-0.1.1 (c (n "multithreading") (v "0.1.1") (h "0yxmpd6skg4l97d9vv9dzm08rv9hv44671nrvy1lbylwfd37p3ss")))

(define-public crate-multithreading-0.1.2 (c (n "multithreading") (v "0.1.2") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "123pc9ssikmdp13xr9rz7mj4shjdyrfnvqw2qr115m0wxi9fy2kp")))

(define-public crate-multithreading-0.1.3 (c (n "multithreading") (v "0.1.3") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "03pfdcx7r9g8mlh9f2zpkkprvn010ragqq5p7pabskm4r7jyspd5")))

(define-public crate-multithreading-0.1.4 (c (n "multithreading") (v "0.1.4") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "0jjdni7hf8hbmmy69f9b7148g45cxlbqkswnx63s2xjv98vbk1if")))

(define-public crate-multithreading-0.2.0 (c (n "multithreading") (v "0.2.0") (h "1q3m3kr2ijraikl9r6g3f4ddd48kq8vlsfxy3qm4l2v2dsw41hpc")))

