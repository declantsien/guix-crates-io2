(define-module (crates-io mu lt multiview-estimator) #:use-module (crates-io))

(define-public crate-multiview-estimator-0.1.0 (c (n "multiview-estimator") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2") (d #t) (k 0)) (d (n "ofps") (r "^0.1") (d #t) (k 0)) (d (n "opencv") (r "^0.62") (f (quote ("clang-runtime"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0phlmk3g2sl86mqb5z7gc3bk6rmqy875yln0cvzdpv06966gjjc7")))

