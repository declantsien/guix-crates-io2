(define-module (crates-io mu lt multiversx-sc-wasm-adapter) #:use-module (crates-io))

(define-public crate-multiversx-sc-wasm-adapter-0.39.0 (c (n "multiversx-sc-wasm-adapter") (v "0.39.0") (d (list (d (n "multiversx-sc") (r "=0.39.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1vfyjlg4h48pxk4g9h08l8bv8wla2fvljd0b1pj9qpxlpsqjn95w") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.39.1 (c (n "multiversx-sc-wasm-adapter") (v "0.39.1") (d (list (d (n "multiversx-sc") (r "=0.39.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "06y78253fhm62v61inrmshiglk8fjwnk47h91pawci8hq3j64hnf") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.39.2 (c (n "multiversx-sc-wasm-adapter") (v "0.39.2") (d (list (d (n "multiversx-sc") (r "=0.39.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0zi9db8fz4brdcf2yh869sqn41qfdy18ilc7h446qnl7ggdrdmwr") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.39.3 (c (n "multiversx-sc-wasm-adapter") (v "0.39.3") (d (list (d (n "multiversx-sc") (r "=0.39.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0lkayiw7qj4nfwp7c5pbg0mq94bfgvy5kx5jxf34ki7mx0s4n1xy") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.39.4 (c (n "multiversx-sc-wasm-adapter") (v "0.39.4") (d (list (d (n "multiversx-sc") (r "=0.39.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0i8hvpnk2hag36k610zljq9q4avramm3bdnvmshpwnnx3j1zlsgf") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.39.5 (c (n "multiversx-sc-wasm-adapter") (v "0.39.5") (d (list (d (n "multiversx-sc") (r "=0.39.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "02bnnhmgp87983ysy002vzj2fwx1jd4ibnsq95ip9bajksrxapdk") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.39.6 (c (n "multiversx-sc-wasm-adapter") (v "0.39.6") (d (list (d (n "multiversx-sc") (r "=0.39.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "10r25yh5hzgnxsls67qy135qsblznrlf2412zr3phz51lsr9f3ys") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.39.7 (c (n "multiversx-sc-wasm-adapter") (v "0.39.7") (d (list (d (n "multiversx-sc") (r "=0.39.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0k59ana3ff24236hvg6bm847lamgx4q7hmzcr32lzsh5c5397w2r") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.39.8 (c (n "multiversx-sc-wasm-adapter") (v "0.39.8") (d (list (d (n "multiversx-sc") (r "=0.39.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "15a0gk1656svh0p8x13qmm1nahzpdslm9kck2lcci3j9y5afwpxg") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.40.0 (c (n "multiversx-sc-wasm-adapter") (v "0.40.0") (d (list (d (n "multiversx-sc") (r "=0.40.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0hn1401aazcjrbma0x912pgn0w4paqi7llrhyy8z6gld0zm061bk") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.40.1 (c (n "multiversx-sc-wasm-adapter") (v "0.40.1") (d (list (d (n "multiversx-sc") (r "=0.40.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0nhvvz7v10n0d448x68cg58k9p8yc7w6i3wp4w01x71ibkygq529") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.41.0 (c (n "multiversx-sc-wasm-adapter") (v "0.41.0") (d (list (d (n "multiversx-sc") (r "=0.41.0") (d #t) (k 0)))) (h "0cligp6jvbi6fb6mby50mi3nixqffnmzaa8flqznmsfbnxvzcv9y") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.41.1 (c (n "multiversx-sc-wasm-adapter") (v "0.41.1") (d (list (d (n "multiversx-sc") (r "=0.41.1") (d #t) (k 0)))) (h "0jc2qlc2ba622v9wps4zhsla474cdkvi04pij8fvrywca0wj91bh") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.41.2 (c (n "multiversx-sc-wasm-adapter") (v "0.41.2") (d (list (d (n "multiversx-sc") (r "=0.41.2") (d #t) (k 0)))) (h "1k3i5imhnz2sxd6qz3sbv2x1nysvanzv9kc94gncf56dly0lv1cj") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.41.3 (c (n "multiversx-sc-wasm-adapter") (v "0.41.3") (d (list (d (n "multiversx-sc") (r "=0.41.3") (d #t) (k 0)))) (h "0xran9bamxhhjh2kmrx8xf62ylycf3667f46pqfyn7p7dmi2f8wk") (f (quote (("vm-validate-token-identifier") ("vm-esdt-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-multiversx-sc-wasm-adapter-0.42.0 (c (n "multiversx-sc-wasm-adapter") (v "0.42.0") (d (list (d (n "multiversx-sc") (r "=0.42.0") (d #t) (k 0)))) (h "00g54kzh2f23kxfnp9kp3fwkibv5qmknj26d9iai512cc4vh26wm")))

(define-public crate-multiversx-sc-wasm-adapter-0.43.0 (c (n "multiversx-sc-wasm-adapter") (v "0.43.0") (d (list (d (n "multiversx-sc") (r "=0.43.0") (d #t) (k 0)))) (h "0rqa7x2sbrgamc3g15jynrixg9q2rq1jh03x9x3zpmfr2q1sz7pw")))

(define-public crate-multiversx-sc-wasm-adapter-0.43.1 (c (n "multiversx-sc-wasm-adapter") (v "0.43.1") (d (list (d (n "multiversx-sc") (r "=0.43.1") (d #t) (k 0)))) (h "1qi2sdpl28frlhff7lsxbv6x9bi2df0j1dvy5l9qp2jb9ikv4r1y")))

(define-public crate-multiversx-sc-wasm-adapter-0.43.2 (c (n "multiversx-sc-wasm-adapter") (v "0.43.2") (d (list (d (n "multiversx-sc") (r "=0.43.2") (d #t) (k 0)))) (h "0b4lw899nycvmkw85lj2bfk45p1lxll0zwpn2nh5md1yzhnz4ndh")))

(define-public crate-multiversx-sc-wasm-adapter-0.43.3 (c (n "multiversx-sc-wasm-adapter") (v "0.43.3") (d (list (d (n "multiversx-sc") (r "=0.43.3") (d #t) (k 0)))) (h "0wg50652miypvj9nlj523biibra986fr1vvggyi7xv4gjrxwwvwz")))

(define-public crate-multiversx-sc-wasm-adapter-0.43.4 (c (n "multiversx-sc-wasm-adapter") (v "0.43.4") (d (list (d (n "multiversx-sc") (r "=0.43.4") (d #t) (k 0)))) (h "17pilglc77sgzkc8f0nb748kqv28342h9acr83g2xpl0pk8j3rs0")))

(define-public crate-multiversx-sc-wasm-adapter-0.43.5 (c (n "multiversx-sc-wasm-adapter") (v "0.43.5") (d (list (d (n "multiversx-sc") (r "=0.43.5") (d #t) (k 0)))) (h "1265hg7dzn17synhpdv3j7yp6a18l9v3ncc9vzqssisvbdad9vrb")))

(define-public crate-multiversx-sc-wasm-adapter-0.44.0 (c (n "multiversx-sc-wasm-adapter") (v "0.44.0") (d (list (d (n "multiversx-sc") (r "=0.44.0") (d #t) (k 0)))) (h "03fhc2zjv72dpv5mchkxalgmx1ycal2m338p2561fxxz2053p8i5")))

(define-public crate-multiversx-sc-wasm-adapter-0.45.0 (c (n "multiversx-sc-wasm-adapter") (v "0.45.0") (d (list (d (n "multiversx-sc") (r "=0.45.0") (d #t) (k 0)))) (h "1gmwmdh4qpja8xx14vpiq3k8rmr3yi97ldqmfm1827gn1hqxb44d")))

(define-public crate-multiversx-sc-wasm-adapter-0.45.1 (c (n "multiversx-sc-wasm-adapter") (v "0.45.1") (d (list (d (n "multiversx-sc") (r "=0.45.1") (d #t) (k 0)))) (h "1fkq28kczkqzc2y1vpmmq9py3p5kpaph56wgvj1bvsl0qf68pyss")))

(define-public crate-multiversx-sc-wasm-adapter-0.45.2 (c (n "multiversx-sc-wasm-adapter") (v "0.45.2") (d (list (d (n "multiversx-sc") (r "=0.45.2") (d #t) (k 0)))) (h "0i9yk32341y9blzgawjs6kq2i78zbc8aipgx1c99yga1smh9caf4")))

(define-public crate-multiversx-sc-wasm-adapter-0.46.0 (c (n "multiversx-sc-wasm-adapter") (v "0.46.0") (d (list (d (n "multiversx-sc") (r "=0.46.0") (d #t) (k 0)))) (h "1a7vk83qhcrvcqsbm02fqj2nnla0rz8l0a3g5fzv29qahs5sgrxc")))

(define-public crate-multiversx-sc-wasm-adapter-0.46.1 (c (n "multiversx-sc-wasm-adapter") (v "0.46.1") (d (list (d (n "multiversx-sc") (r "=0.46.1") (d #t) (k 0)))) (h "0y4ykn42vcswg4a47b8ndaf7pb28z98zh3q1iskaamns006g8ycm")))

(define-public crate-multiversx-sc-wasm-adapter-0.47.0 (c (n "multiversx-sc-wasm-adapter") (v "0.47.0") (d (list (d (n "multiversx-sc") (r "=0.47.0") (d #t) (k 0)))) (h "175m67v5smiqx35bf2f23jynpngiv9qqwqixvy80c92ia67zhxkp")))

(define-public crate-multiversx-sc-wasm-adapter-0.47.1 (c (n "multiversx-sc-wasm-adapter") (v "0.47.1") (d (list (d (n "multiversx-sc") (r "=0.47.1") (d #t) (k 0)))) (h "04qjn72n9db0kh5sz8023xnnq8d0xzxrbmywv43j0mah5riv0czj")))

(define-public crate-multiversx-sc-wasm-adapter-0.47.2 (c (n "multiversx-sc-wasm-adapter") (v "0.47.2") (d (list (d (n "multiversx-sc") (r "=0.47.2") (d #t) (k 0)))) (h "10fb9m3pw8d4w32faj9q73f7m5p7gflb3bvwgzdyvvr626sg1xxm")))

(define-public crate-multiversx-sc-wasm-adapter-0.47.3 (c (n "multiversx-sc-wasm-adapter") (v "0.47.3") (d (list (d (n "multiversx-sc") (r "=0.47.3") (d #t) (k 0)))) (h "0flh4156hik1b9gglzzvwyhk76xw8bsfsnlq4h0mim19xq3qdmql")))

(define-public crate-multiversx-sc-wasm-adapter-0.47.4 (c (n "multiversx-sc-wasm-adapter") (v "0.47.4") (d (list (d (n "multiversx-sc") (r "=0.47.4") (d #t) (k 0)))) (h "07nq1pj3nwr322baqz9577lafzqvbws9hk6xmmdzvrps9nrh0j9h")))

(define-public crate-multiversx-sc-wasm-adapter-0.47.5 (c (n "multiversx-sc-wasm-adapter") (v "0.47.5") (d (list (d (n "multiversx-sc") (r "=0.47.5") (d #t) (k 0)))) (h "0wgs26wxn0q4bjjhmkrhngas15dx2gymkz65crlv9f390wry1rp7")))

(define-public crate-multiversx-sc-wasm-adapter-0.47.6 (c (n "multiversx-sc-wasm-adapter") (v "0.47.6") (d (list (d (n "multiversx-sc") (r "=0.47.6") (d #t) (k 0)))) (h "0h9gd7fnbcxwpxmk1giv3fs06d21jk8bbakjxdk9bvwkn4fa9b6c")))

(define-public crate-multiversx-sc-wasm-adapter-0.47.7 (c (n "multiversx-sc-wasm-adapter") (v "0.47.7") (d (list (d (n "multiversx-sc") (r "=0.47.7") (d #t) (k 0)))) (h "1q92v4vsf4qh682j96xa300hk46gsrlkgdx5n6sk588z5gs6fdqy")))

(define-public crate-multiversx-sc-wasm-adapter-0.47.8 (c (n "multiversx-sc-wasm-adapter") (v "0.47.8") (d (list (d (n "multiversx-sc") (r "=0.47.8") (d #t) (k 0)))) (h "0yn1xpkgqzlwk3qirb2pgg5l4hrhbll62z9zgws2g3dc1yf0ilbk")))

(define-public crate-multiversx-sc-wasm-adapter-0.48.0-alpha.1 (c (n "multiversx-sc-wasm-adapter") (v "0.48.0-alpha.1") (d (list (d (n "multiversx-sc") (r "=0.48.0-alpha.1") (d #t) (k 0)))) (h "0f3affvqgd54kh98z4jc6a2a7lj4883xk9j73qp962h5m2i007in")))

(define-public crate-multiversx-sc-wasm-adapter-0.48.0 (c (n "multiversx-sc-wasm-adapter") (v "0.48.0") (d (list (d (n "multiversx-sc") (r "=0.48.0") (d #t) (k 0)))) (h "05nnamq1bj8x01b90gy1cvd3mh17rh6s1x6kk8pam0rcqzdiiggn")))

(define-public crate-multiversx-sc-wasm-adapter-0.49.0-alpha.2 (c (n "multiversx-sc-wasm-adapter") (v "0.49.0-alpha.2") (d (list (d (n "multiversx-sc") (r "=0.49.0-alpha.2") (d #t) (k 0)))) (h "1i0z75cx1dkc7izc1nkc5g7f5n43a7n9x7j5z07y0h04fhvc6l1k")))

(define-public crate-multiversx-sc-wasm-adapter-0.49.0-alpha.3 (c (n "multiversx-sc-wasm-adapter") (v "0.49.0-alpha.3") (d (list (d (n "multiversx-sc") (r "=0.49.0-alpha.3") (d #t) (k 0)))) (h "0hkkmkfqqb0b0401cn2bxkyg4ihalzy15j6mfvy1cg4fh6q24l2c")))

(define-public crate-multiversx-sc-wasm-adapter-0.49.0-alpha.4 (c (n "multiversx-sc-wasm-adapter") (v "0.49.0-alpha.4") (d (list (d (n "multiversx-sc") (r "=0.49.0-alpha.4") (d #t) (k 0)))) (h "0pmypdmb66234pqs2j32672kr0v4fclzn820rbbb4gsb5518lsmg")))

(define-public crate-multiversx-sc-wasm-adapter-0.48.1 (c (n "multiversx-sc-wasm-adapter") (v "0.48.1") (d (list (d (n "multiversx-sc") (r "=0.48.1") (d #t) (k 0)))) (h "1g0frcn3p1gjpdgyl62g9hiyx94cj5m1n63w48jb9ylrmclhvi94")))

(define-public crate-multiversx-sc-wasm-adapter-0.49.0 (c (n "multiversx-sc-wasm-adapter") (v "0.49.0") (d (list (d (n "multiversx-sc") (r "=0.49.0") (d #t) (k 0)))) (h "1y5xd2cqq61y51rjnn4i8br5z14cvrvl28sndwjk9zn063c7sp83")))

(define-public crate-multiversx-sc-wasm-adapter-0.50.0 (c (n "multiversx-sc-wasm-adapter") (v "0.50.0") (d (list (d (n "multiversx-sc") (r "=0.50.0") (d #t) (k 0)))) (h "0z8z0y2xwih6vy8q1vw9nxp4hihjxl39rlbswh5kddqym4qz19s2")))

(define-public crate-multiversx-sc-wasm-adapter-0.50.1 (c (n "multiversx-sc-wasm-adapter") (v "0.50.1") (d (list (d (n "multiversx-sc") (r "=0.50.1") (d #t) (k 0)))) (h "1b0921f6wgbhfbd9nccxjmi3pm029rdhi72mf83hsnqn03sjcgan")))

(define-public crate-multiversx-sc-wasm-adapter-0.50.2 (c (n "multiversx-sc-wasm-adapter") (v "0.50.2") (d (list (d (n "multiversx-sc") (r "=0.50.2") (d #t) (k 0)))) (h "1mwn8vl867qwcyls4vmrwxk3cx44cbx695zb5d9z4iq1pl1lbs76")))

(define-public crate-multiversx-sc-wasm-adapter-0.50.3 (c (n "multiversx-sc-wasm-adapter") (v "0.50.3") (d (list (d (n "multiversx-sc") (r "=0.50.3") (d #t) (k 0)))) (h "1zdcwy2z24vxgzfh810nivlv4i6kipv741fk90jbns6fmy3f4l23")))

