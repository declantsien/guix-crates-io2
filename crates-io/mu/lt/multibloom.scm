(define-module (crates-io mu lt multibloom) #:use-module (crates-io))

(define-public crate-multibloom-0.1.0 (c (n "multibloom") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)))) (h "1wlh99ajvsnrs1hzbqjm4mdll6mqjmygyksibx9adym264a4g4qn")))

(define-public crate-multibloom-0.1.1 (c (n "multibloom") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)))) (h "07ydmdg2im8sy6jqikk8mdf9swkxrzlfzaj5vq5n8vgxc7q4y45w")))

(define-public crate-multibloom-0.2.0 (c (n "multibloom") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "twox-hash") (r "^1.0.0") (d #t) (k 2)))) (h "108nyn11cgf7d1hqqmj7b9w4z2c1kb8bw6fz71v75b77h4w2l69s")))

