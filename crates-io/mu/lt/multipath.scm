(define-module (crates-io mu lt multipath) #:use-module (crates-io))

(define-public crate-multipath-0.1.0 (c (n "multipath") (v "0.1.0") (h "18yx5l02dq3wyz4ah28pfczsdsmrg9c1skpkc34nhlbz7vin5yxl")))

(define-public crate-multipath-1.0.0 (c (n "multipath") (v "1.0.0") (h "0z98jr758gxaxjx5l7vikw4w4vz4rlc9irxj9gs1a6fv1fi9p4k2")))

