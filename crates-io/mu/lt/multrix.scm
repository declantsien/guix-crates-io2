(define-module (crates-io mu lt multrix) #:use-module (crates-io))

(define-public crate-multrix-0.1.0 (c (n "multrix") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0r0pdadawgfmzc4fima80qx06041z5djnggvmvm4hlbqjknn78q1") (y #t)))

(define-public crate-multrix-0.1.1 (c (n "multrix") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "17pjbaq7s9hplg5x8bls917iycbmdk90490lb0p9jqml4szy768j") (y #t)))

(define-public crate-multrix-0.1.2 (c (n "multrix") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1a28hfzvrfnbmanqvar5ixphxi1lzhvk72jidj2b9jvfv4ab6ghq")))

