(define-module (crates-io mu lt multiversx-sc-modules) #:use-module (crates-io))

(define-public crate-multiversx-sc-modules-0.39.0 (c (n "multiversx-sc-modules") (v "0.39.0") (d (list (d (n "multiversx-sc") (r "^0.39.0") (d #t) (k 0)))) (h "1d2ig8zra8ad35wcgmc2ghdj4pjd529iwmnqg7bx9jrnrznqlb5v") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.39.1 (c (n "multiversx-sc-modules") (v "0.39.1") (d (list (d (n "multiversx-sc") (r "^0.39.1") (d #t) (k 0)))) (h "0lqlbhgzwpxy9gblmbh6c613z286y6r1bk63f9dyirzrafjk6hnk") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.39.2 (c (n "multiversx-sc-modules") (v "0.39.2") (d (list (d (n "multiversx-sc") (r "^0.39.2") (d #t) (k 0)))) (h "05d8lxw08zghlyh9ir8ink8vs93prarvz2xbxprilx2l5lhvv6rf") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.39.3 (c (n "multiversx-sc-modules") (v "0.39.3") (d (list (d (n "multiversx-sc") (r "^0.39.3") (d #t) (k 0)))) (h "05ibqs4x606wghnqnwcgfcmp1pfzlmzdff730wa2z9isr3kqs9mr") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.39.4 (c (n "multiversx-sc-modules") (v "0.39.4") (d (list (d (n "multiversx-sc") (r "^0.39.4") (d #t) (k 0)))) (h "1wv1hk4nbrwpivbmx84np493fk207xnk1s0crav5420wwfiq88pi") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.39.5 (c (n "multiversx-sc-modules") (v "0.39.5") (d (list (d (n "multiversx-sc") (r "^0.39.5") (d #t) (k 0)))) (h "1kasjf95kyqsh33rsvmml88g8s219b0gh53gibzqljq9f2dims08") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.39.6 (c (n "multiversx-sc-modules") (v "0.39.6") (d (list (d (n "multiversx-sc") (r "^0.39.6") (d #t) (k 0)))) (h "19xldwihw1i21hc4pviay5y8zwdw0xvc0i38cnrzvxjhqyy9f6ym") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.39.7 (c (n "multiversx-sc-modules") (v "0.39.7") (d (list (d (n "multiversx-sc") (r "^0.39.7") (d #t) (k 0)))) (h "1f3rg68ci3nv3nv7aih80ralyq6461ly67s056hp1ifkzs2jbnc9") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.39.8 (c (n "multiversx-sc-modules") (v "0.39.8") (d (list (d (n "multiversx-sc") (r "^0.39.8") (d #t) (k 0)))) (h "1495j4z37fx6h0siwy5b2nsvh0dl3l4rb4nq057li5kkyc30pjyv") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.40.0 (c (n "multiversx-sc-modules") (v "0.40.0") (d (list (d (n "multiversx-sc") (r "^0.40.0") (d #t) (k 0)))) (h "1raa9ahsr5wcq9hfzmp6mswq65mq8wdabwp9azxdifypvwgvf99q") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.40.1 (c (n "multiversx-sc-modules") (v "0.40.1") (d (list (d (n "multiversx-sc") (r "^0.40.1") (d #t) (k 0)))) (h "0yf0708afqqs3kanx9lyaqbymslxfv8fh6ricpa0668vy1kdjdrw") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.41.0 (c (n "multiversx-sc-modules") (v "0.41.0") (d (list (d (n "multiversx-sc") (r "^0.41.0") (d #t) (k 0)))) (h "163vr2ka3j2y8r7610s8rjdkhw9rw6cybha8jpyqmvl55j5801yx") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.41.1 (c (n "multiversx-sc-modules") (v "0.41.1") (d (list (d (n "multiversx-sc") (r "^0.41.1") (d #t) (k 0)))) (h "18yhayam3hv6dba3kfd4jv9j7wkyzqgz7cf1n38y5mabv7h16bxz") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.41.2 (c (n "multiversx-sc-modules") (v "0.41.2") (d (list (d (n "multiversx-sc") (r "^0.41.2") (d #t) (k 0)))) (h "1qw570y0z1ag6wfil8y5jnljfs7nl8nhgqhzr52xara14l3ayf3m") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.41.3 (c (n "multiversx-sc-modules") (v "0.41.3") (d (list (d (n "multiversx-sc") (r "^0.41.3") (d #t) (k 0)))) (h "04i3w3an25pk7bszappdzl4jds6sxs7sw34q4bbdzgw86wi2aj3h") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.42.0 (c (n "multiversx-sc-modules") (v "0.42.0") (d (list (d (n "multiversx-sc") (r "^0.42.0") (d #t) (k 0)))) (h "10m247k3xsv5d967nf8sb8nn8abkxz2c6l8ac0l12m5cbylqwyh8") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.43.0 (c (n "multiversx-sc-modules") (v "0.43.0") (d (list (d (n "multiversx-sc") (r "^0.43.0") (d #t) (k 0)))) (h "1hvh7k7s735r6j1pw3qvlmd09xzba6ynm3s1438h6rmmrcmlbj49") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.43.1 (c (n "multiversx-sc-modules") (v "0.43.1") (d (list (d (n "multiversx-sc") (r "^0.43.1") (d #t) (k 0)))) (h "1p1cwrds083rq134s6idrixzpry2p4g93s2ga2651pjy5m7b5msk") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.43.2 (c (n "multiversx-sc-modules") (v "0.43.2") (d (list (d (n "multiversx-sc") (r "^0.43.2") (d #t) (k 0)))) (h "04b35j1nlznlyf0hjypdkj3g39rsrd5vx4wpgfylbiybhlszwm9v") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.43.3 (c (n "multiversx-sc-modules") (v "0.43.3") (d (list (d (n "multiversx-sc") (r "^0.43.3") (d #t) (k 0)))) (h "0x7d5rzrd87c2k2hrlwyx4bhb3capags25m8dwa9gjghbds61hg5") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.43.4 (c (n "multiversx-sc-modules") (v "0.43.4") (d (list (d (n "multiversx-sc") (r "^0.43.4") (d #t) (k 0)))) (h "04270lliig7jpgw0y6pvrbl15kfp8hinv0awgg9wlwjhzr42bp3m") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.43.5 (c (n "multiversx-sc-modules") (v "0.43.5") (d (list (d (n "multiversx-sc") (r "^0.43.5") (d #t) (k 0)))) (h "165wwk2dcr7zbr0iyfvm8kf2rrzm4smvwdgz3rk8cnfh2hf2m825") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.44.0 (c (n "multiversx-sc-modules") (v "0.44.0") (d (list (d (n "multiversx-sc") (r "^0.44.0") (d #t) (k 0)))) (h "0gc9c8nw9jsgz3cqhjq7rmv2ncjh0y7wfv5bkh62rmv61hqjsm5v") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.45.0 (c (n "multiversx-sc-modules") (v "0.45.0") (d (list (d (n "multiversx-sc") (r "^0.45.0") (d #t) (k 0)))) (h "0zs8f691wcsyj00qx7nbf3qw0m3c08bbkgh68x28hq8nmv9gv1f2") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.45.1 (c (n "multiversx-sc-modules") (v "0.45.1") (d (list (d (n "multiversx-sc") (r "^0.45.1") (d #t) (k 0)))) (h "0a8viw33wgzjs1kcia4p8qfnh3p9lq8dipvnqwpiyrjjvafpbk3m") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.45.2 (c (n "multiversx-sc-modules") (v "0.45.2") (d (list (d (n "multiversx-sc") (r "^0.45.2") (d #t) (k 0)))) (h "0xf40qq0030w03kw04p5wa061rn32qm2v5d871yka141r25kz0x5") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.46.0 (c (n "multiversx-sc-modules") (v "0.46.0") (d (list (d (n "multiversx-sc") (r "^0.46.0") (d #t) (k 0)))) (h "1h7h4iqh79i21r4xqq2wg4pqbkpz5df506wnp7vnjaq389vfp220") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.46.1 (c (n "multiversx-sc-modules") (v "0.46.1") (d (list (d (n "multiversx-sc") (r "^0.46.1") (d #t) (k 0)))) (h "0pw8931avwv69hiq9x2jkchj7xb4v985ybqyk1szyc76jnxglgy6") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.47.0 (c (n "multiversx-sc-modules") (v "0.47.0") (d (list (d (n "multiversx-sc") (r "^0.47.0") (d #t) (k 0)))) (h "1s35x7dickdxqncrnd70aqwnylmsd1v98iidak8i5xwgcllqs50y") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.47.1 (c (n "multiversx-sc-modules") (v "0.47.1") (d (list (d (n "multiversx-sc") (r "^0.47.1") (d #t) (k 0)))) (h "10zwfnd49ycaz24c0gifvhgzy3chr2xvv3zp968gsasrij9fn0ws") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.47.2 (c (n "multiversx-sc-modules") (v "0.47.2") (d (list (d (n "multiversx-sc") (r "^0.47.2") (d #t) (k 0)))) (h "0pm5ssbndq3mxw07wykxkmpd3f71ab14lq9a72qrg01dyfhk0nkb") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.47.3 (c (n "multiversx-sc-modules") (v "0.47.3") (d (list (d (n "multiversx-sc") (r "^0.47.3") (d #t) (k 0)))) (h "0kgnll679v9yh7byxlszf6ndggj5l8hizxd1aanwzilrb1dx62ih") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.47.4 (c (n "multiversx-sc-modules") (v "0.47.4") (d (list (d (n "multiversx-sc") (r "^0.47.4") (d #t) (k 0)))) (h "154i3bqb9y38rl6f1l9skqw399chfhg1km8jfm30hlk2sq842z8q") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.47.5 (c (n "multiversx-sc-modules") (v "0.47.5") (d (list (d (n "multiversx-sc") (r "^0.47.5") (d #t) (k 0)))) (h "0p0p2bdl7l3i3azv2ksqsj5pyb5dqx8mixvwyx76rj0mfgqsrk73") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.47.6 (c (n "multiversx-sc-modules") (v "0.47.6") (d (list (d (n "multiversx-sc") (r "^0.47.6") (d #t) (k 0)))) (h "0a6qn8a1jvq0sy9c9z4labyk8lxg8mhai2363yjkza6r1qdf1sda") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.47.7 (c (n "multiversx-sc-modules") (v "0.47.7") (d (list (d (n "multiversx-sc") (r "^0.47.7") (d #t) (k 0)))) (h "0lygmakfq3s0fv59h1ys1fjvhxizf8nk4mkx97xqcfn60ljbalhl") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.47.8 (c (n "multiversx-sc-modules") (v "0.47.8") (d (list (d (n "multiversx-sc") (r "^0.47.8") (d #t) (k 0)))) (h "0kcsx7krbaz0y4xd9m6k12hac4sg95yhnys5kw60wxc82m8xgc1n") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.48.0-alpha.1 (c (n "multiversx-sc-modules") (v "0.48.0-alpha.1") (d (list (d (n "multiversx-sc") (r "^0.48.0-alpha.1") (d #t) (k 0)))) (h "1535j6vcflf65zrkskkwwpjf47qnf5m7j65f9pm46jw35fj1ykia") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.48.0 (c (n "multiversx-sc-modules") (v "0.48.0") (d (list (d (n "multiversx-sc") (r "^0.48.0") (d #t) (k 0)))) (h "153ccv1gcw0skn3jhx35ls0z8b45yc9bq86rqff41azw18c7ll2f") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.49.0-alpha.2 (c (n "multiversx-sc-modules") (v "0.49.0-alpha.2") (d (list (d (n "multiversx-sc") (r "^0.49.0-alpha.2") (d #t) (k 0)))) (h "110z0rrscfi9ihzcxdmlxvw6bwvh603ny4riwpr1fclqs3xzngq2") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.49.0-alpha.3 (c (n "multiversx-sc-modules") (v "0.49.0-alpha.3") (d (list (d (n "multiversx-sc") (r "^0.49.0-alpha.3") (d #t) (k 0)))) (h "0iq4pmidcc9xl8i00nmyrc6hvhjhapchh7w4pdnvcdm9ifhcrgka") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.49.0-alpha.4 (c (n "multiversx-sc-modules") (v "0.49.0-alpha.4") (d (list (d (n "multiversx-sc") (r "^0.49.0-alpha.4") (d #t) (k 0)))) (h "06hvk7y4yv19m2ly1gs19n9kxnlvg8gnf4rs08dlb30cmwhj9dfx") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.48.1 (c (n "multiversx-sc-modules") (v "0.48.1") (d (list (d (n "multiversx-sc") (r "^0.48.1") (d #t) (k 0)))) (h "171cb59sxzl76v4wx4m2dcd247krlzv920h8d9q43wl11c0f5j8p") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.49.0 (c (n "multiversx-sc-modules") (v "0.49.0") (d (list (d (n "multiversx-sc") (r "^0.49.0") (d #t) (k 0)))) (h "08cn9wzlpwa3y7s0pbi73vlbd1cg855qllgp4z27dkq70r37nrvn") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.50.0 (c (n "multiversx-sc-modules") (v "0.50.0") (d (list (d (n "multiversx-sc") (r "^0.50.0") (d #t) (k 0)))) (h "1bn11bhjymxdywd03h5j25ykmcs076c6km4cng3h2nzcml84d0i5") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.50.1 (c (n "multiversx-sc-modules") (v "0.50.1") (d (list (d (n "multiversx-sc") (r "^0.50.1") (d #t) (k 0)))) (h "0dbv39kiikmzf0m3hfinnqqahyz722pdqhh8wix9g80s3bpbwxqf") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.50.2 (c (n "multiversx-sc-modules") (v "0.50.2") (d (list (d (n "multiversx-sc") (r "^0.50.2") (d #t) (k 0)))) (h "13ps82v5fi1v12lrfglfv17wnc70rrjprnb02p5bfgd0kaapkv7b") (f (quote (("alloc" "multiversx-sc/alloc"))))))

(define-public crate-multiversx-sc-modules-0.50.3 (c (n "multiversx-sc-modules") (v "0.50.3") (d (list (d (n "multiversx-sc") (r "^0.50.3") (d #t) (k 0)))) (h "19bsy4kqx370nb7w8br025z9mbm9h3md3ggd0kyavixdvl88r6kz") (f (quote (("alloc" "multiversx-sc/alloc"))))))

