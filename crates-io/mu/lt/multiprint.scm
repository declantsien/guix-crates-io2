(define-module (crates-io mu lt multiprint) #:use-module (crates-io))

(define-public crate-multiprint-0.1.0 (c (n "multiprint") (v "0.1.0") (h "0i326kfgbkrxc03ga02cdwg8243zj0w4xmr5rnv63n1rx4lr2abj")))

(define-public crate-multiprint-0.1.1 (c (n "multiprint") (v "0.1.1") (h "02q9hjyfycqi099irm8fpl15b6kwgb5i9ia3cajqmr225lkd5abv")))

(define-public crate-multiprint-0.1.2 (c (n "multiprint") (v "0.1.2") (h "0y0ckbxjqghknvsvf32czajk8yxfjvxqg4qwlcw8d909rf686brg")))

(define-public crate-multiprint-0.1.3 (c (n "multiprint") (v "0.1.3") (h "09szb1rjcn1ci9vpfpyrjaqk6dqc92ws281xfvq43wq8vsfaxa3m")))

