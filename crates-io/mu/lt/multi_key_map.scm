(define-module (crates-io mu lt multi_key_map) #:use-module (crates-io))

(define-public crate-multi_key_map-0.1.0 (c (n "multi_key_map") (v "0.1.0") (h "1v8cikdhrdz1r9pm0jf4lywzrw3qj0a0xwq1ab2lj2jz8cablxnq")))

(define-public crate-multi_key_map-0.2.0 (c (n "multi_key_map") (v "0.2.0") (h "0vbbknq9cmfalx5mv8dgrlzbag0iqkwz97l07aim7gg2lw9is33b")))

(define-public crate-multi_key_map-0.3.0 (c (n "multi_key_map") (v "0.3.0") (h "0bydbh0ip669pszbrnvjgs0244amwmglkqzz6i7hbkqpiq9k3lr7")))

