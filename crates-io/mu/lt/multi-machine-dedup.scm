(define-module (crates-io mu lt multi-machine-dedup) #:use-module (crates-io))

(define-public crate-multi-machine-dedup-0.2.0 (c (n "multi-machine-dedup") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tree_magic_mini") (r "^3.0.3") (f (quote ("with-gpl-data"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1vc3fyx4qwb8sir9x8mjk0cx7jvv7hnm5fkvqc3dbfkhdnb0sr1l")))

