(define-module (crates-io mu lt multimarkov) #:use-module (crates-io))

(define-public crate-multimarkov-0.1.0 (c (n "multimarkov") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0ivwk9gg5bdb6z0xfrm09y1msabzhr8wxqv0p1zpiykg0a0ww7w1")))

(define-public crate-multimarkov-0.2.0 (c (n "multimarkov") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "000ajyqjqd874ybyl4dxvn2102iiy2q0wpnlrxh20drgnq9x5pw2")))

(define-public crate-multimarkov-0.2.1 (c (n "multimarkov") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0bqnjhnc5xjshcwrqn5893skjv1gps8mgkdxhcaybr5l9rgf7dl3")))

(define-public crate-multimarkov-0.3.0 (c (n "multimarkov") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1mmyqidxbiq1cslvcfkg2kly4ga7i4sbkfnf2dh65bcp8cgiv3pj") (y #t)))

(define-public crate-multimarkov-0.3.1 (c (n "multimarkov") (v "0.3.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1l7w1wdyhk2mrvhmj7yy7phas4a8ri91z76hg032486zpb418x2f") (y #t)))

(define-public crate-multimarkov-0.3.2 (c (n "multimarkov") (v "0.3.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1ym9jpj447sip2w4zppb21xix18xqiz8w4535wci7sbl7pqd38n8")))

(define-public crate-multimarkov-0.4.0 (c (n "multimarkov") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1383gizygvdmlxnldgcj5z08pjv6fbvjhf7zj2jan6vnkp7qr3hw")))

