(define-module (crates-io mu lt multi-git) #:use-module (crates-io))

(define-public crate-multi-git-0.1.0 (c (n "multi-git") (v "0.1.0") (d (list (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "0x082ifmnglbs6ppcm06c78nph5bf90rxjyh59vcdn854rk18h0a")))

