(define-module (crates-io mu lt multipart-nickel) #:use-module (crates-io))

(define-public crate-multipart-nickel-0.1.0 (c (n "multipart-nickel") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "multipart") (r "^0.10") (f (quote ("hyper" "server"))) (k 0)) (d (n "nickel") (r "^0.9") (d #t) (k 0)))) (h "09zhxnbrmbgiymq61x0rcdlh7lag3ifblqhc6ncchg279m33j2ps")))

(define-public crate-multipart-nickel-0.2.0 (c (n "multipart-nickel") (v "0.2.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "multipart") (r "^0.11") (f (quote ("hyper" "server"))) (k 0)) (d (n "nickel") (r "^0.9") (d #t) (k 0)))) (h "0lhhd1kqyv5pq0045xcfs0i068hl32c8vcn40z5id7xc2mqv36cg")))

(define-public crate-multipart-nickel-0.3.0 (c (n "multipart-nickel") (v "0.3.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "multipart") (r "^0.13") (f (quote ("hyper" "server"))) (k 0)) (d (n "nickel") (r "^0.10") (d #t) (k 0)))) (h "1544s67d0n3nv27mik7k6qv4yvzskjgq9bz4nprhwc7c8ik5d7g7")))

(define-public crate-multipart-nickel-0.3.1 (c (n "multipart-nickel") (v "0.3.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "multipart") (r "^0.13") (f (quote ("hyper" "server"))) (k 0)) (d (n "nickel") (r "^0.10") (d #t) (k 0)))) (h "1dnjzwfm7nl05mspjfw4ql6l3s65xplkk2cqbyxrv1bjwshyf4zi")))

