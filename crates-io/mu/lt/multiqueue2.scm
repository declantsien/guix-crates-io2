(define-module (crates-io mu lt multiqueue2) #:use-module (crates-io))

(define-public crate-multiqueue2-0.1.0 (c (n "multiqueue2") (v "0.1.0") (d (list (d (n "atomic_utilities") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0k110lk8nqbkd9xxwk9lkivbnza9jbb4i2hwxsd31xn9prpfv9fl")))

(define-public crate-multiqueue2-0.1.1 (c (n "multiqueue2") (v "0.1.1") (d (list (d (n "atomic_utilities") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0zd9p4igphrkd9askzj54ayfcck4a4zxnh68whia18wr9zjlw7vx")))

(define-public crate-multiqueue2-0.1.2 (c (n "multiqueue2") (v "0.1.2") (d (list (d (n "atomic_utilities") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1xcs2xlvns6w6s8yp65mv3zv7i64ksh913z09w4546anxdkyfz8m")))

(define-public crate-multiqueue2-0.1.3 (c (n "multiqueue2") (v "0.1.3") (d (list (d (n "atomic_utilities") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.9") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0rifw274ndkp4pw6kz93dh3b3mfb5mzlipz18sw79agykwjxpnx9")))

(define-public crate-multiqueue2-0.1.4 (c (n "multiqueue2") (v "0.1.4") (d (list (d (n "atomic_utilities") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.9") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1dizy0qmyihli9n3bnhw3mawcqlwn9hjdh1mj8wgai2lqzdjp3dn")))

(define-public crate-multiqueue2-0.1.5 (c (n "multiqueue2") (v "0.1.5") (d (list (d (n "atomic_utilities") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.9") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0spd80h03n13dfnf3pzm79qhyd1s7y4rryy9x07rms38yiiv7z8f")))

(define-public crate-multiqueue2-0.1.6 (c (n "multiqueue2") (v "0.1.6") (d (list (d (n "atomic_utilities") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.9") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "106m9fas862h2f6kn4yarmyf8qq0lfwvlsky4zsr1mm5y1ykhf3s")))

(define-public crate-multiqueue2-0.1.7 (c (n "multiqueue2") (v "0.1.7") (d (list (d (n "atomic_utilities") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.30") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.5.1") (d #t) (k 0)) (d (n "time") (r "^0.2.23") (d #t) (k 0)))) (h "07biq9abav37dz8zmv00ihg4cavizd7chx85gpfakapxbpj2273l")))

