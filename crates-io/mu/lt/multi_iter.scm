(define-module (crates-io mu lt multi_iter) #:use-module (crates-io))

(define-public crate-multi_iter-0.1.0 (c (n "multi_iter") (v "0.1.0") (h "0m7yl4m0d21y8sis71aqxalwahm5x1n37k325fb7sin80j7w79x9") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-multi_iter-0.1.1 (c (n "multi_iter") (v "0.1.1") (h "0553zs32bbhv6ldzwsajb4hd21sfizqhfxik4dszz2i10mkxcp4b") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-multi_iter-0.1.2 (c (n "multi_iter") (v "0.1.2") (h "0jjiskcnn89sglvvrk3k28kp1hzcfccrym2ls1kzwmgkxaz15bkm") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-multi_iter-0.1.3 (c (n "multi_iter") (v "0.1.3") (h "0982f7f12hkprd3957bhbhljhi24kk2bvcf34gxn20714rwg7dca") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-multi_iter-0.1.4 (c (n "multi_iter") (v "0.1.4") (h "1f08gkv0qj07sbnyvj58lyns62i1r0z1hzpylym0srihlsbp7az2") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-multi_iter-0.1.5 (c (n "multi_iter") (v "0.1.5") (h "1fw1m3frjr1cci9gancykipl0ig3qj7c38k2bnlr06j3wvcy31gc") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-multi_iter-0.1.6 (c (n "multi_iter") (v "0.1.6") (h "14b7q5nlbx3h46968y9dgwj89ikhkzvkfbbx22czp8gdqa5ycfdl") (f (quote (("unstable") ("std") ("default" "std"))))))

