(define-module (crates-io mu lt multihashes) #:use-module (crates-io))

(define-public crate-multihashes-0.0.0 (c (n "multihashes") (v "0.0.0") (h "1cnbnsc9wd1grdqbhq0a2wp2isvrd0kw9s5wd75jx210ac4i28p8") (y #t)))

(define-public crate-multihashes-0.0.1-multihashes (c (n "multihashes") (v "0.0.1-multihashes") (h "0hwv0mzssglpj1j1r5xb20k4x0609wh49v42briq7z633xh2g2n6") (y #t)))

(define-public crate-multihashes-0.0.0-- (c (n "multihashes") (v "0.0.0--") (h "1zrn9lmvysqrpmb7zfz1ixsagjchvz9l9hxm40fzsnabd7ly9r54") (y #t)))

