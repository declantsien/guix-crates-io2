(define-module (crates-io mu lt multiset) #:use-module (crates-io))

(define-public crate-multiset-0.0.1 (c (n "multiset") (v "0.0.1") (h "0rv2fkhw8ddam0jw1s0i6zyg1ir8khx0c4n3y3av28sb17k5hjqh")))

(define-public crate-multiset-0.0.2 (c (n "multiset") (v "0.0.2") (h "1yk3053w12jj61vwygd70mgdpvszfn6087jswq4lfiy014jwikm8")))

(define-public crate-multiset-0.0.3 (c (n "multiset") (v "0.0.3") (h "0c8fh4d5pqv1xf42j9r4pfj3vnnk3d42jv6mpdlq1jvxmw9av7cb")))

(define-public crate-multiset-0.0.4 (c (n "multiset") (v "0.0.4") (h "0cx8ijqfa2qa4hq82dx73pr73a0zrbpj4ry4cmj79ji8jdrybh6q")))

(define-public crate-multiset-0.0.5 (c (n "multiset") (v "0.0.5") (h "0p6v3lxsci7v7amcsjvn0c49dpsi50ciidxqp1n9jl6kvp4ki1yf")))

