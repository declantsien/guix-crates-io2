(define-module (crates-io mu lt multiconf) #:use-module (crates-io))

(define-public crate-multiconf-0.1.0 (c (n "multiconf") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "0bl3grgfr3y4nrcc7hy73n4bg5z88js6va1nyicsdpncfy4fa82y")))

(define-public crate-multiconf-0.1.1 (c (n "multiconf") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "0yzxg5pyz9ghgrkggq2av058wlvcnac6fcmp3216yqy8qkl3gm14")))

(define-public crate-multiconf-0.1.2 (c (n "multiconf") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "14clmvwylc7pxpa0gliwkxadkv1jvghbr4zcrrf4g40y48spyqq0")))

(define-public crate-multiconf-0.1.3 (c (n "multiconf") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "0y8pwydnjs686jaz9syf9vd78zvbx5crpaqlfa1apjm5dbf8lb4j")))

(define-public crate-multiconf-0.1.4 (c (n "multiconf") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "0fqsb1lvf6yh92m38ab6cilkrdcnpjciggxzrbyahl34ffs86f37")))

(define-public crate-multiconf-0.1.5 (c (n "multiconf") (v "0.1.5") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "1g3f7n2z3d2iwjkb9nriirpb9lhyfsdhz4r56camz9zjwpcm41bq")))

(define-public crate-multiconf-0.1.6 (c (n "multiconf") (v "0.1.6") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "0z4lfqj879pj7xv8sbx9msr31nwvh0nmwzmamp38x4xwnhdqaz7s")))

(define-public crate-multiconf-0.1.7 (c (n "multiconf") (v "0.1.7") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "0ch732pczx7j8mhd34ar7510ww8fdf2a2bfl4addzzl1x19dx33n")))

(define-public crate-multiconf-0.1.8 (c (n "multiconf") (v "0.1.8") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "08wwz8fmpwfdvpv5qlcjd8fk465k124a1qn02zk215y7y5nv5b4i")))

(define-public crate-multiconf-0.1.9 (c (n "multiconf") (v "0.1.9") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "0ih832affkj1mgncxlpig0zfsjm35j2zpjyzy3mwmpc3sv836ky7")))

(define-public crate-multiconf-0.1.10 (c (n "multiconf") (v "0.1.10") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "1kdlhqnv09a17sj2x1cv1965hax1zck7526dqm0kh0jcyfy0znsb")))

(define-public crate-multiconf-0.1.11 (c (n "multiconf") (v "0.1.11") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "0b3qq41gdnm419ir8kyixw3d6i520qbdc84k0dvgmgak0268cjq6")))

