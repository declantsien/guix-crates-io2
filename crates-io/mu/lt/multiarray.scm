(define-module (crates-io mu lt multiarray) #:use-module (crates-io))

(define-public crate-multiarray-0.1.0 (c (n "multiarray") (v "0.1.0") (d (list (d (n "anyrange") (r "^0.1.0") (d #t) (k 0)))) (h "0cyd3k7rwws1a9vim295mziszph299q8rzv8v9i4z5jp13bzlrz9")))

(define-public crate-multiarray-0.1.1 (c (n "multiarray") (v "0.1.1") (d (list (d (n "anyrange") (r "^0.1.0") (d #t) (k 0)))) (h "1smrab9xq5gxc8hyvw5gw9hb1vwk7vsxyj173zql2isqr6n23gdf")))

(define-public crate-multiarray-0.1.2 (c (n "multiarray") (v "0.1.2") (d (list (d (n "anyrange") (r "^0.1.0") (d #t) (k 0)))) (h "01g5l3rx80w0829g36jl9f7p5c3maby2ybs191q2i5krarxs4fra")))

(define-public crate-multiarray-0.1.3 (c (n "multiarray") (v "0.1.3") (d (list (d (n "anyrange") (r "^0.1.0") (d #t) (k 0)))) (h "0crsd7lh7hs1k61sc919jyl7z8wb3z8s9q344pqyn0md2a380bav")))

