(define-module (crates-io mu lt multipeek) #:use-module (crates-io))

(define-public crate-multipeek-0.1.0 (c (n "multipeek") (v "0.1.0") (h "0rbdkv8584b674rp0q0wv933wq44863n7vk5l1cf8sw8nyz8rb89")))

(define-public crate-multipeek-0.1.1 (c (n "multipeek") (v "0.1.1") (h "1c2argb9hd7m4952f0xdvr9g7g5cf591ms8j2dwqm0zpsixv4v7d")))

(define-public crate-multipeek-0.1.2 (c (n "multipeek") (v "0.1.2") (h "18mnjwyda0crk9rx74jmwcvp1g1nx173by6bk0w8qz5fqbqiqsqx")))

