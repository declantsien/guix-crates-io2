(define-module (crates-io mu lt multi_mut) #:use-module (crates-io))

(define-public crate-multi_mut-0.1.0 (c (n "multi_mut") (v "0.1.0") (h "1bv30cvag685wrhrfvvfasa11v7c5l1hy6a4rbhqvlvcj7jyjnsx")))

(define-public crate-multi_mut-0.1.1 (c (n "multi_mut") (v "0.1.1") (h "127d3n8pl2v757lf9v3y5i2pic9ydfiabjnxycigzgfj5x93niva")))

(define-public crate-multi_mut-0.1.2 (c (n "multi_mut") (v "0.1.2") (h "0l8kslglya9b9nsvqr94s0ic2vilnsh73m9757dkwn8za4p9yhcx")))

(define-public crate-multi_mut-0.1.3 (c (n "multi_mut") (v "0.1.3") (h "1knyzfs2gniwgb41bk2ws1sv2jmzgj5bmwcn7a2c2yjmwn3g6vc1")))

