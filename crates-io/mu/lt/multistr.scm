(define-module (crates-io mu lt multistr) #:use-module (crates-io))

(define-public crate-multistr-0.1.0 (c (n "multistr") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0sl57rjsj1lk9mpv65r3gf565kx0yfj9jyg6jf2dq51zgy6z432q")))

(define-public crate-multistr-0.2.0 (c (n "multistr") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0g46khp6hn4x9i8a15fbz2zk4ssy6pr9av3gbcyncfx3bs9f3cjp")))

(define-public crate-multistr-0.2.1 (c (n "multistr") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "1vrvfbfp79w759s2ls1146gd44ik69664whsbdm1z61rvn2i53cl") (f (quote (("inclusive_range"))))))

(define-public crate-multistr-0.3.0 (c (n "multistr") (v "0.3.0") (d (list (d (n "bow") (r "^0.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "extra-default") (r "^0.1") (d #t) (k 0)) (d (n "len-trait") (r "^0.2") (f (quote ("collections"))) (k 0)) (d (n "push-trait") (r "^0.3") (f (quote ("collections"))) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "1l2r6wwv6asd6jrrixl04ihhb2bvs13m7vdz5g7hrm1b46nv8ipn") (f (quote (("inclusive_range"))))))

(define-public crate-multistr-0.4.0 (c (n "multistr") (v "0.4.0") (d (list (d (n "bow") (r "^0.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "extra-default") (r "^0.1") (d #t) (k 0)) (d (n "len-trait") (r "^0.2") (f (quote ("collections"))) (k 0)) (d (n "push-trait") (r "^0.4") (f (quote ("collections"))) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 0)))) (h "0bqpjpy23r5i36nl3wblpmajxgsfmb150pdjd9a60g84d9lmxyrd") (f (quote (("inclusive_range"))))))

(define-public crate-multistr-0.5.0 (c (n "multistr") (v "0.5.0") (d (list (d (n "bow") (r "^1.0") (d #t) (k 0)) (d (n "extra-default") (r "^0.2") (d #t) (k 0)) (d (n "len-trait") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "push-trait") (r "^0.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0nkyril2vxs9z6wbfp8sb646l1qim5rand009x8i624bbvwybiws") (f (quote (("inclusive_range"))))))

(define-public crate-multistr-0.5.1 (c (n "multistr") (v "0.5.1") (d (list (d (n "bow") (r "^1.0") (d #t) (k 0)) (d (n "extra-default") (r "^0.2") (d #t) (k 0)) (d (n "len-trait") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "push-trait") (r "^0.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0z8vcdd1fgsd4and9s5l97g2k4bd8bdcp9ymrs2p77mpjbcwk830") (f (quote (("inclusive_range"))))))

(define-public crate-multistr-0.5.2 (c (n "multistr") (v "0.5.2") (d (list (d (n "bow") (r "^1.0") (d #t) (k 0)) (d (n "extra-default") (r "^0.2") (d #t) (k 0)) (d (n "len-trait") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "push-trait") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0b8ynqggrplk6rdy9ggjpdidl8n115ziqzj3i84z58kc9wdnxywh") (f (quote (("inclusive_range"))))))

(define-public crate-multistr-0.5.3 (c (n "multistr") (v "0.5.3") (d (list (d (n "bow") (r "^1.0") (d #t) (k 0)) (d (n "extra-default") (r "^0.2") (d #t) (k 0)) (d (n "len-trait") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "push-trait") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0wj0zvdgm5i1zs24w4szpfqs42jbwfyw73d6qk0f77gm794dj548") (f (quote (("inclusive_range"))))))

(define-public crate-multistr-0.5.4 (c (n "multistr") (v "0.5.4") (d (list (d (n "bow") (r "^1.0") (d #t) (k 0)) (d (n "extra-default") (r "^0.2") (d #t) (k 0)) (d (n "len-trait") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "push-trait") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "01ighianrpf2j07yc5ffvh6799whvz3fql5lgpwi4pa7c22h2zn0") (f (quote (("inclusive_range"))))))

