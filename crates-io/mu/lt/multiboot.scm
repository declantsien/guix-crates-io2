(define-module (crates-io mu lt multiboot) #:use-module (crates-io))

(define-public crate-multiboot-0.0.1 (c (n "multiboot") (v "0.0.1") (h "03566rki7vrxxngfvgl8v26piba05scsg09mp29gcy3gfyf02ci0")))

(define-public crate-multiboot-0.0.2 (c (n "multiboot") (v "0.0.2") (h "0hnj4k6jqh8pxg6fici024x050ls0h7lkdybr7r71fzcry3scz64")))

(define-public crate-multiboot-0.0.3 (c (n "multiboot") (v "0.0.3") (h "0n7r65hifyhdpa6lhzdqsy22lhd5kvwsc3n412nxf2wgzrbkhbxv")))

(define-public crate-multiboot-0.0.4 (c (n "multiboot") (v "0.0.4") (h "09knj7xbmjqfs300gwncz70px5ms4kzjafqvjjd9qkqh1jawb9yv")))

(define-public crate-multiboot-0.0.5 (c (n "multiboot") (v "0.0.5") (h "18w2kbwq74f8nkan20c213b204ynfy4f6j277i4ynizff2mv8l5j")))

(define-public crate-multiboot-0.1.0 (c (n "multiboot") (v "0.1.0") (h "08r80l7hbr01012b0k7qrnip83ca351qxdb5hfi7r376yph7dn6h")))

(define-public crate-multiboot-0.1.1 (c (n "multiboot") (v "0.1.1") (h "0z4k9n5qvb5hrvz87ilfqivkpndfs7mganz2ylgbr1b8zbkzzha1")))

(define-public crate-multiboot-0.2.0 (c (n "multiboot") (v "0.2.0") (h "0k3a1vgg8wk36v2cwzv925lgn6zhfvrkhlg50cya7n93vwh0r2s8")))

(define-public crate-multiboot-0.3.0 (c (n "multiboot") (v "0.3.0") (h "0hsxw84aw2sjfy3fqi8x8ih9qd6p3d5d0bgycviak3hj9wfkapkl")))

(define-public crate-multiboot-0.4.0 (c (n "multiboot") (v "0.4.0") (h "1bzsbilycd5if4rakx93azyzxbiqh70qgs3ldimb3qds6wy21r89") (y #t)))

(define-public crate-multiboot-0.6.0 (c (n "multiboot") (v "0.6.0") (d (list (d (n "paste") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1bgmwa2hwh9gscp32amvp2csfysbs6c2a8dqdrkwjyzgmmjjqi0g")))

(define-public crate-multiboot-0.7.0 (c (n "multiboot") (v "0.7.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "11grfl409hqjlxyk16ff2rsi48ixdfn2969kbvq1kw90nc36rrmd")))

(define-public crate-multiboot-0.8.0 (c (n "multiboot") (v "0.8.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0vnm7fyfh5cwc8pycnnmn9lcs850ncz1w8i2bi9dmxdwnyvx6ypq")))

