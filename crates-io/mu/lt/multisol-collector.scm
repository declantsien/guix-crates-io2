(define-module (crates-io mu lt multisol-collector) #:use-module (crates-io))

(define-public crate-multisol-collector-1.0.0 (c (n "multisol-collector") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "multisol-structs") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "06m14fffrisfcc5k26yqvvp1gmjpaxg2418qxjml5wx34p67i2bw")))

(define-public crate-multisol-collector-1.0.1 (c (n "multisol-collector") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "multisol-structs") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1npfz3vg9fkpvhrpjxk57anl54lfsvqvffpjglibd5h42kyb7d01")))

(define-public crate-multisol-collector-1.0.2 (c (n "multisol-collector") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "multisol-structs") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "12fsgrmyz6syq5390ql80b367kgma63ay779pyfd88iz8q8149is")))

(define-public crate-multisol-collector-1.1.0 (c (n "multisol-collector") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "multisol-structs") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0gpmx19j955f816cvxh28np3grigignj5z29ss4ksbw9npkc6k9s")))

