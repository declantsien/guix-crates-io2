(define-module (crates-io mu lt multiscale-truchet) #:use-module (crates-io))

(define-public crate-multiscale-truchet-0.0.1 (c (n "multiscale-truchet") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "09ggbm7143v1r7sa1wahb3iv43zrlxl0xgbpn851wvs3dyxm87wm")))

