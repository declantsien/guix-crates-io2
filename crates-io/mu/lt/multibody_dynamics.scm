(define-module (crates-io mu lt multibody_dynamics) #:use-module (crates-io))

(define-public crate-multibody_dynamics-0.1.0 (c (n "multibody_dynamics") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1iq88mrpwhdyh3m19y2hag067lmkn9agwhrr7xdxgnzz1nzrygi8")))

(define-public crate-multibody_dynamics-0.1.1 (c (n "multibody_dynamics") (v "0.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "19p1ajqwy5lki598wm1r45jr6y6vj8mc0bq2l6k99m1c13mbxlas")))

(define-public crate-multibody_dynamics-0.1.2 (c (n "multibody_dynamics") (v "0.1.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1pliv6s8hzahr8wbk4rrary55df1k3a6ps1qkj58vmy6mfwcx49w")))

