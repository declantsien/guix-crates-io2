(define-module (crates-io mu lt multisock) #:use-module (crates-io))

(define-public crate-multisock-0.1.0 (c (n "multisock") (v "0.1.0") (h "15nz15f2aabwb3ls93jfd732kz5qa2y5ls54jlmivsp6rqiv43kc")))

(define-public crate-multisock-0.1.1 (c (n "multisock") (v "0.1.1") (h "1y6blwb7b8w47qi734vbayg586v24k960abbrpj2rrlxqhn970wv")))

(define-public crate-multisock-1.0.0 (c (n "multisock") (v "1.0.0") (h "0hpkavwdzz8n70rz4kf71zfi4hj2xnzxys4n6pp7718zlnahpc09")))

