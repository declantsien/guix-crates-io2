(define-module (crates-io mu lt multiboot2-host) #:use-module (crates-io))

(define-public crate-multiboot2-host-0.1.0 (c (n "multiboot2-host") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1qvjyja3vxcwbl7nz53g6m9dw8jrdj4fldqk848ib6chdkrzkd6d") (f (quote (("hvm"))))))

(define-public crate-multiboot2-host-0.1.1 (c (n "multiboot2-host") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "17xjnjhb97j57z4awm230g6dinl12ihs6434k4y3iis449gfbils") (f (quote (("hvm"))))))

(define-public crate-multiboot2-host-0.2.0 (c (n "multiboot2-host") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1lrpgwl6np63lif1xly0anhhw8sjg84rv6gsmcy2vclvhb1r4ld2") (f (quote (("hvm"))))))

(define-public crate-multiboot2-host-0.2.1 (c (n "multiboot2-host") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0pbprynvg5j86hcbqcl75nmynccm27ik7q7z38qgrrsxwvwwy2r0") (f (quote (("hvm"))))))

