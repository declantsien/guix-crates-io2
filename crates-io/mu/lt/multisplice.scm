(define-module (crates-io mu lt multisplice) #:use-module (crates-io))

(define-public crate-multisplice-0.1.0 (c (n "multisplice") (v "0.1.0") (h "0kxrfm0agnimkdnm4s0fg171vhdv8ilkqc853p7xcm6whrwywh6y")))

(define-public crate-multisplice-0.2.0 (c (n "multisplice") (v "0.2.0") (h "1dapmxhrk1md90rzdcq793ky2v481nzsrrz3y6ppn7glb7jha88x")))

(define-public crate-multisplice-0.3.0 (c (n "multisplice") (v "0.3.0") (h "1imvn9p54x3i8kzw7xssh05bcddxnadrn7gkinp37s7pa8wagydi")))

