(define-module (crates-io mu lt multi-eq) #:use-module (crates-io))

(define-public crate-multi-eq-0.1.0 (c (n "multi-eq") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "19434v52qh8dmgx2674wvvbkyaclhp02lcja448gxd2kfkb00652")))

(define-public crate-multi-eq-0.1.1 (c (n "multi-eq") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "0mmmnldapkq0wrdcp5s67bjrchybx7bpczwm9jiwkw2a8dxci9ba")))

