(define-module (crates-io mu lt multi-structs) #:use-module (crates-io))

(define-public crate-multi-structs-0.0.1 (c (n "multi-structs") (v "0.0.1") (h "1lrypq92yhgbi6rp27ismf8zny3d67rz9hsgzq1sgpnk4s27kijv") (f (quote (("example_generated") ("default"))))))

(define-public crate-multi-structs-0.0.2 (c (n "multi-structs") (v "0.0.2") (h "1qvvy491a99abrkinipxmnlqiwqz47cycvwfnaq5qli3akish3fi") (f (quote (("example_generated") ("default"))))))

(define-public crate-multi-structs-0.1.0 (c (n "multi-structs") (v "0.1.0") (h "1932cz0yhpj5j606nbpgh9d260fmbfzmr9r0cls0rr88kzs2alcl") (f (quote (("example_generated") ("default"))))))

(define-public crate-multi-structs-0.1.1 (c (n "multi-structs") (v "0.1.1") (h "0ag9s09awr4yd8q11kbwfmkd6qlmq1389c6dpimdrjv6i5zapaab") (f (quote (("example_generated") ("default"))))))

