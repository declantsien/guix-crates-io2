(define-module (crates-io mu lt multimixer-128) #:use-module (crates-io))

(define-public crate-multimixer-128-0.1.0 (c (n "multimixer-128") (v "0.1.0") (d (list (d (n "criterion") (r "=0.5.1") (d #t) (k 2)) (d (n "crunchy") (r "=0.2.2") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)))) (h "1gi0ywxpfn27727ijq12jdfqylv1x9fdhchmx9crbcm7lg5ml0z3") (f (quote (("internal"))))))

(define-public crate-multimixer-128-0.1.1 (c (n "multimixer-128") (v "0.1.1") (d (list (d (n "criterion") (r "=0.5.1") (d #t) (k 2)) (d (n "crunchy") (r "=0.2.2") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1yxyj8sy1pk249x9ns6yiwqibd311xf0r27z15zfd7x5gs65rlk8") (f (quote (("internal"))))))

(define-public crate-multimixer-128-0.1.2 (c (n "multimixer-128") (v "0.1.2") (d (list (d (n "criterion") (r "=0.5.1") (d #t) (k 2)) (d (n "crunchy") (r "=0.2.2") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0p61nkj4fw5d5chn54n7xkx5xc95hdaxhw34m04v1d9zkm4xlwzn") (f (quote (("internal"))))))

(define-public crate-multimixer-128-0.1.3 (c (n "multimixer-128") (v "0.1.3") (d (list (d (n "criterion") (r "=0.5.1") (d #t) (k 2)) (d (n "crunchy") (r "=0.2.2") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1dlz5nv0nvg2lw3qd8786slpb1mqy1xzqrw9512iljc7h53gykji") (f (quote (("internal"))))))

(define-public crate-multimixer-128-0.1.4 (c (n "multimixer-128") (v "0.1.4") (d (list (d (n "criterion") (r "=0.5.1") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "=0.6.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\", target_arch = \"aarch64\", target_arch = \"loongarch64\"))") (k 2)) (d (n "crunchy") (r "=0.2.2") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0ynwzylj026s977hinl423728jvs7sycxgf5s24qxzv4i2r15p1h") (f (quote (("internal"))))))

