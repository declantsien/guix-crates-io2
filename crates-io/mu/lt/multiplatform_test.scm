(define-module (crates-io mu lt multiplatform_test) #:use-module (crates-io))

(define-public crate-multiplatform_test-0.0.0 (c (n "multiplatform_test") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "058infs3v3jqd9jd7kgwr3afzp37sxyk8bw9lcyrqzgdkkmd9771")))

(define-public crate-multiplatform_test-0.1.0 (c (n "multiplatform_test") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1n6gm8lr4y7r109r60sq5x4z57miipnbagf9s1ih26s2qmhv4c0c")))

