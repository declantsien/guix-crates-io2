(define-module (crates-io mu lt multi-consumer-stream) #:use-module (crates-io))

(define-public crate-multi-consumer-stream-0.1.0 (c (n "multi-consumer-stream") (v "0.1.0") (d (list (d (n "atm-async-utils") (r "^0.1.1") (d #t) (k 2)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "ordermap") (r "^0.3.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "09xv4x8hjavciykbcna7sggwckbmg4h9r0l877cnmq6l8vsrcy5d")))

(define-public crate-multi-consumer-stream-0.2.0 (c (n "multi-consumer-stream") (v "0.2.0") (d (list (d (n "atm-async-utils") (r "^0.1.1") (d #t) (k 2)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "ordermap") (r "^0.3.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "107ddig46fgfv2v077gkfyicyba9phr9b46bhda5imzb6k985d4r")))

(define-public crate-multi-consumer-stream-0.2.1 (c (n "multi-consumer-stream") (v "0.2.1") (d (list (d (n "atm-async-utils") (r "^0.1.1") (d #t) (k 2)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "ordermap") (r "^0.3.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "1s55xlg1xi33afypj45hg7h1w87pzm7s7q6ciflqxssl76gzbr4p")))

(define-public crate-multi-consumer-stream-0.3.0 (c (n "multi-consumer-stream") (v "0.3.0") (d (list (d (n "atm-async-utils") (r "^0.1.1") (d #t) (k 2)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "ordermap") (r "^0.3.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "0hdbw7rpl5rn5b2dllgik2pr1wbk7bdwqr80n904mvrd6slk13nx")))

(define-public crate-multi-consumer-stream-0.4.0 (c (n "multi-consumer-stream") (v "0.4.0") (d (list (d (n "atm-async-utils") (r "^0.2.0") (d #t) (k 2)) (d (n "futures") (r "^0.2.0-alpha") (d #t) (k 2)) (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "indexmap") (r "^0.4.1") (d #t) (k 0)))) (h "1xb5s61sbld464pvs1c17mn83dxgr4l5r9a02s2l1c435fzm5wn4")))

