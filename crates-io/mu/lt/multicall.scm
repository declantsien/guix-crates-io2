(define-module (crates-io mu lt multicall) #:use-module (crates-io))

(define-public crate-multicall-0.1.0 (c (n "multicall") (v "0.1.0") (h "0cw2447q46jcgvhh91g1z6cshap04fwl2d87xsjw701x81f5g49m")))

(define-public crate-multicall-0.1.1 (c (n "multicall") (v "0.1.1") (h "0b6ls4rnnqyprch4fqvfgar03al7yv4ny2n70zbvjlawvfwdf1c5") (y #t)))

(define-public crate-multicall-0.1.2 (c (n "multicall") (v "0.1.2") (h "0p6ipwikw7c6yab1vj0av4vlqhdk8asdryjdlxnxii5a7xp88vsb")))

(define-public crate-multicall-0.1.3 (c (n "multicall") (v "0.1.3") (h "12hi8ksaz9g12qdljs0ivgnvwhcf78cjpzwf9qa9dfxfi2vqm34a")))

(define-public crate-multicall-0.1.4 (c (n "multicall") (v "0.1.4") (h "18shw5cm81xp6vmv25dz7xdz78h7gmfwz1jsq5bpx4hcgnhnp3s2")))

