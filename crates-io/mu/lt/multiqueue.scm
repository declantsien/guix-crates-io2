(define-module (crates-io mu lt multiqueue) #:use-module (crates-io))

(define-public crate-multiqueue-0.1.0 (c (n "multiqueue") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "16alns624d6wkinbjakc29g380mhfvsfqqiaadbm92840pg35bnp") (y #t)))

(define-public crate-multiqueue-0.1.1 (c (n "multiqueue") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1ppwj5nis0mzz2z9zd5ksvfyfqppb9hnncagj8mc27g93kk9k4hq")))

(define-public crate-multiqueue-0.2.1 (c (n "multiqueue") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "02f1g2rygz0vqz7pb4ykjac5bwii3210rg20kqirwagk5aj8a60a")))

(define-public crate-multiqueue-0.3.0 (c (n "multiqueue") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1qsbg1him6j366qp6awl0068inrgmczbgp86dcv9a8382pply015")))

(define-public crate-multiqueue-0.3.1 (c (n "multiqueue") (v "0.3.1") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "198jy9v6z722p0cc5p83qifyxgbgk9szp7gjzxbagxyyp1q1pp0l")))

(define-public crate-multiqueue-0.3.2 (c (n "multiqueue") (v "0.3.2") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1zbkkv7r94ygy2fcg2vd5v99wmbif6rqqi5vgszrqrhn6lznfna0")))

