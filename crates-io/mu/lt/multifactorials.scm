(define-module (crates-io mu lt multifactorials) #:use-module (crates-io))

(define-public crate-multifactorials-0.1.0 (c (n "multifactorials") (v "0.1.0") (h "0mqfqys8z36c4iirvc9kc06j6l6bpv9x6qmmn689lmwcv1fqqh5g")))

(define-public crate-multifactorials-0.2.0 (c (n "multifactorials") (v "0.2.0") (h "07zl5lycnij4900wijl592dszxafdxkdwmrfpyvlibhyxijgv2is")))

(define-public crate-multifactorials-0.3.0 (c (n "multifactorials") (v "0.3.0") (h "00y339p44d3i0lvafdnmq5w4zs99h0chijcb1w51ciy46w1j71x4")))

