(define-module (crates-io mu lt multiversx-chain-vm-executor) #:use-module (crates-io))

(define-public crate-multiversx-chain-vm-executor-0.1.0 (c (n "multiversx-chain-vm-executor") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "enumset") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1hs7r4dl2wyf41174i4qm8bj73k7igfkcqv7b6abcbk88x4z253y")))

(define-public crate-multiversx-chain-vm-executor-0.2.0 (c (n "multiversx-chain-vm-executor") (v "0.2.0") (h "17vfdn0ciqh9lp24n8a0qi5fpfqm2nlvz9izmvjmmd940vx7545m")))

