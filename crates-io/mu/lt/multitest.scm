(define-module (crates-io mu lt multitest) #:use-module (crates-io))

(define-public crate-multitest-0.0.0 (c (n "multitest") (v "0.0.0") (h "00g146gqy0p95l528dmq7pbnrbkxx8zp7z159v18aq6954v9g81d")))

(define-public crate-multitest-0.0.1 (c (n "multitest") (v "0.0.1") (h "0nbqasvh8na64dm87qvf1rxkdf0jvn3sg4m4p7p997vxj9jn4xh3")))

(define-public crate-multitest-0.0.2 (c (n "multitest") (v "0.0.2") (h "1y5kdfvmqjnzf1jfysaxppldbyqfwnfrq26npwz2z1w94gfnb368")))

