(define-module (crates-io mu lt multisql-cli) #:use-module (crates-io))

(define-public crate-multisql-cli-0.0.0 (c (n "multisql-cli") (v "0.0.0") (d (list (d (n "console") (r "^0") (d #t) (k 0)) (d (n "dialoguer") (r "^0") (d #t) (k 0)) (d (n "indicatif") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "multisql") (r "^0.0.2") (d #t) (k 0)))) (h "0pll05p3ndlx8pg3mlla5sgy4ysp7p2lvn85yzj329j9jhxs1zgq")))

(define-public crate-multisql-cli-0.0.2 (c (n "multisql-cli") (v "0.0.2") (d (list (d (n "cli-table") (r "^0") (d #t) (k 0)) (d (n "console") (r "^0") (d #t) (k 0)) (d (n "dialoguer") (r "^0") (d #t) (k 0)) (d (n "indicatif") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "multisql") (r "^0.0.3") (d #t) (k 0)))) (h "0hrw0fwxjw6d0ib6ldxmll2jv57clmdpmvidkxavdxknarz8238l")))

(define-public crate-multisql-cli-0.0.3 (c (n "multisql-cli") (v "0.0.3") (d (list (d (n "cli-table") (r "^0") (d #t) (k 0)) (d (n "console") (r "^0") (d #t) (k 0)) (d (n "dialoguer") (r "^0") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "indicatif") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "multisql") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0323rnn91ln12dlssdqzgzd6fq55sym3zsg1ch6biafwkcf7ra4m")))

(define-public crate-multisql-cli-0.2.0 (c (n "multisql-cli") (v "0.2.0") (d (list (d (n "cli-table") (r "^0") (d #t) (k 0)) (d (n "console") (r "^0") (d #t) (k 0)) (d (n "dialoguer") (r "^0") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "indicatif") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "multisql") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)))) (h "0avhvdcc4r57am13qsikmqp63g20kk28mwgk465ask9r0y47vdc5")))

