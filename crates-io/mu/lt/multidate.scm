(define-module (crates-io mu lt multidate) #:use-module (crates-io))

(define-public crate-multidate-0.1.0 (c (n "multidate") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (k 0)) (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.2") (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("std" "derive" "help"))) (k 0)))) (h "14vcgbw3m1mk3h5fzl9am7hcf3zg1rqq07qbllm1kng2x9pqd5kf")))

(define-public crate-multidate-0.2.0 (c (n "multidate") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (k 0)) (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.2") (k 0)) (d (n "clap") (r "^4.3.3") (f (quote ("std" "derive" "help"))) (k 0)))) (h "0bpba3k454bkga6cvw3nmbq4f21fr2hgyah4rw3gmhliaa2lr749")))

(define-public crate-multidate-0.2.1 (c (n "multidate") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (k 0)) (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (k 0)) (d (n "clap") (r "^4.3.9") (f (quote ("std" "derive" "help"))) (k 0)))) (h "0sj6hllrvlqlh1b34zk72qg0gld602hj2azd27w4pfv65yqa6gn1")))

(define-public crate-multidate-0.2.2 (c (n "multidate") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.86") (k 0)) (d (n "chrono") (r "^0.4.38") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.9.0") (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("std" "derive" "help" "error-context"))) (k 0)))) (h "1qpmq4q6c62cn7kjx8f6m2wzkxjp17mx5bv4azhxw4aady74q6gh")))

