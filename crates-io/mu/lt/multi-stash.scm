(define-module (crates-io mu lt multi-stash) #:use-module (crates-io))

(define-public crate-multi-stash-0.1.0 (c (n "multi-stash") (v "0.1.0") (h "0dqm9rjck1xzzv7pgxakh71ss1hfz3z8s1yvahp63v26nifvm4gp")))

(define-public crate-multi-stash-0.2.0 (c (n "multi-stash") (v "0.2.0") (h "03s12sf633n02mcqcv2yxdx545lwc127hsic3n774khznv29lnk8")))

