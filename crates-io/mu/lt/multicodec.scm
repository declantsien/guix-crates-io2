(define-module (crates-io mu lt multicodec) #:use-module (crates-io))

(define-public crate-multicodec-0.1.0 (c (n "multicodec") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.2") (d #t) (k 0)))) (h "1vbr6fmhlvp7cwlwhx71zcbll9g2dbvx4xj8jiinzb1crrcab1zj")))

