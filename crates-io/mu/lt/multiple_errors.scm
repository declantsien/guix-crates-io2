(define-module (crates-io mu lt multiple_errors) #:use-module (crates-io))

(define-public crate-multiple_errors-1.0.0 (c (n "multiple_errors") (v "1.0.0") (h "095gkqq2lj0xryvw2j926w3qpcdgad772cqckn05rlda8gbw7cy5")))

(define-public crate-multiple_errors-1.1.0 (c (n "multiple_errors") (v "1.1.0") (h "0c10js5w64dghn76g4l523adp7j3d879lrcsdazcjdax03hi77s4")))

