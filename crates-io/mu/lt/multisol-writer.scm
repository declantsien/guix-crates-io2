(define-module (crates-io mu lt multisol-writer) #:use-module (crates-io))

(define-public crate-multisol-writer-1.0.0 (c (n "multisol-writer") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "multisol-structs") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1aiq8zrfrnv784xjrqgsk6420lcbj7398x9s9ap5kw7mljvsfr8r")))

(define-public crate-multisol-writer-1.0.1 (c (n "multisol-writer") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "multisol-structs") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "08ypc9pi741wix604yczrc8j2dphxjkqyw48mg3103mka732fc0r")))

(define-public crate-multisol-writer-1.0.2 (c (n "multisol-writer") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "multisol-structs") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1s449l5ai8ibf2549g0wl33i0hmnbnvrk15yfvbhvird2rnxl42d")))

(define-public crate-multisol-writer-1.1.0 (c (n "multisol-writer") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "multisol-structs") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1lc64ls3ml99wvpdxcsssjmzfi5lwk4mr1lcry62h7p049kwqw62")))

