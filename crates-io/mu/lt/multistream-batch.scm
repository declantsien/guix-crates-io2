(define-module (crates-io mu lt multistream-batch) #:use-module (crates-io))

(define-public crate-multistream-batch-0.1.0 (c (n "multistream-batch") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)))) (h "092hxppdcynnjx5174zn0x0b8fzjd8wfaak9af2rfqw9vr54srii") (f (quote (("default" "crossbeam-channel"))))))

(define-public crate-multistream-batch-0.1.1 (c (n "multistream-batch") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)))) (h "1ss6xphqcb09mjwkw8k14pcp81b5lwvmfn0mh38c3w1lr6sa5plr") (f (quote (("default" "crossbeam-channel"))))))

(define-public crate-multistream-batch-0.1.2 (c (n "multistream-batch") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)))) (h "0l1sxvng98mv2lpz02zmykdp9463scpy161h7bbprdx6b8yb28gc") (f (quote (("default" "crossbeam-channel"))))))

(define-public crate-multistream-batch-1.0.0 (c (n "multistream-batch") (v "1.0.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)))) (h "0sdskrxhin9dfpzls5pf37iz8g5w3dx17wdiv5k7vw7jz79bil6s") (f (quote (("default" "crossbeam-channel"))))))

(define-public crate-multistream-batch-1.0.1 (c (n "multistream-batch") (v "1.0.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)))) (h "0sm9mb7yhqrn1h013y77wr7s8frsqp3m63q6cyldwbw3zszajm92") (f (quote (("default" "crossbeam-channel"))))))

(define-public crate-multistream-batch-1.1.0 (c (n "multistream-batch") (v "1.1.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)))) (h "1l1nrwn0wd3xxskjjkgq06d5k7h9zrzirhhd3f5klg9pbmdk4jay") (f (quote (("default" "crossbeam-channel"))))))

(define-public crate-multistream-batch-1.2.0 (c (n "multistream-batch") (v "1.2.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)))) (h "0vlzghr6ypmp00vvs1wjkpicls9c7zigfav2rj1pqg82g3bd6lrv") (f (quote (("default" "crossbeam-channel"))))))

