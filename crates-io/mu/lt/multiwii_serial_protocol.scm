(define-module (crates-io mu lt multiwii_serial_protocol) #:use-module (crates-io))

(define-public crate-multiwii_serial_protocol-0.1.0 (c (n "multiwii_serial_protocol") (v "0.1.0") (d (list (d (n "packed_struct") (r "^0.1.0") (d #t) (k 0)) (d (n "packed_struct_codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1hh4pg4v067lq5s3y0ijii0zlld62acfbyllssxvy3ii52v64p5b") (f (quote (("std") ("no_std") ("default" "std"))))))

(define-public crate-multiwii_serial_protocol-0.1.1 (c (n "multiwii_serial_protocol") (v "0.1.1") (d (list (d (n "packed_struct") (r "^0.2") (d #t) (k 0)) (d (n "packed_struct_codegen") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0h5fkjqk4ij0zn9jjn6jfvrrl2ikrba01j726ffby0dgi1dxnyw0") (f (quote (("std") ("no_std") ("default" "std"))))))

