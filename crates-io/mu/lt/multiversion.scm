(define-module (crates-io mu lt multiversion) #:use-module (crates-io))

(define-public crate-multiversion-0.1.0 (c (n "multiversion") (v "0.1.0") (d (list (d (n "atomic") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rp30f7g7ppj2n7rghv7jxz7gzyqi50z1wjnaia0q9pwn5q5pqsc")))

(define-public crate-multiversion-0.1.1 (c (n "multiversion") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05sdsdxy403glpz7ml8zji3cgl3irr73g9imcd8760d1pgjpq97l")))

(define-public crate-multiversion-0.2.0 (c (n "multiversion") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "066mjqjw6z086nawirncrw8p756lb8ii3s00d9cf3kbvam2c8kp9")))

(define-public crate-multiversion-0.3.0 (c (n "multiversion") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1j4kslk14ig9qilai189ksv0xs4cx5bgl1hqlqa7w7cszl4jycg5")))

(define-public crate-multiversion-0.4.0 (c (n "multiversion") (v "0.4.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0fq3y2sljjrpj0jlra398npdjl39bnp4cmhcai5mwqji9pdq7fkm") (f (quote (("runtime_dispatch") ("default" "runtime_dispatch"))))))

(define-public crate-multiversion-0.5.0 (c (n "multiversion") (v "0.5.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "04l79x5ibsvnc2yz53iq74xrjq8ybzdaqylpnqq0n2r0xc2f8b84") (f (quote (("runtime_dispatch") ("default" "runtime_dispatch"))))))

(define-public crate-multiversion-0.5.1 (c (n "multiversion") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "06sdghqvdrbf8wgmkj4143imswqzf37f14kyyr0a32lfdb6wphd6") (f (quote (("runtime_dispatch") ("default" "runtime_dispatch"))))))

(define-public crate-multiversion-0.6.0 (c (n "multiversion") (v "0.6.0") (d (list (d (n "multiversion-macros") (r "^0.1") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "0hsvni6dvvzqvf4d76fgz01jg7jvp4akb41flklwghpyczgap2cd") (f (quote (("std" "multiversion-macros/std") ("default" "std"))))))

(define-public crate-multiversion-0.6.1 (c (n "multiversion") (v "0.6.1") (d (list (d (n "multiversion-macros") (r "^0.6.1") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "0wsk9ykdqjd6c54rf5l6yb6ps4hx409cda8aa875xk6k7lm9cp02") (f (quote (("std" "multiversion-macros/std") ("default" "std"))))))

(define-public crate-multiversion-0.7.0 (c (n "multiversion") (v "0.7.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "multiversion-macros") (r "^0.7.0") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "target-features") (r "^0.1") (d #t) (k 0)))) (h "0pclwx6niq8g3dy00ild8kpcsy6xbigw5fl0ng198m9dvgcn3601") (f (quote (("std" "multiversion-macros/std") ("default" "std"))))))

(define-public crate-multiversion-0.7.1 (c (n "multiversion") (v "0.7.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "multiversion-macros") (r "^0.7.1") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "target-features") (r "^0.1") (d #t) (k 0)))) (h "1xz8yy00jcpr22zc4m7azafvbdia3p88cc2pwlss4715wbnpxa76") (f (quote (("std" "multiversion-macros/std") ("default" "std"))))))

(define-public crate-multiversion-0.7.2 (c (n "multiversion") (v "0.7.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "multiversion-macros") (r "^0.7.2") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "target-features") (r "^0.1") (d #t) (k 0)))) (h "1l9b8swj5mw7xi4z138rx62agfzb9g1dcbmz574w4i2ivvd4bnlc") (f (quote (("std" "multiversion-macros/std") ("default" "std"))))))

(define-public crate-multiversion-0.7.3 (c (n "multiversion") (v "0.7.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "multiversion-macros") (r "^0.7.3") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "target-features") (r "^0.1") (d #t) (k 0)))) (h "0al7yrf489lqzxx291sx9566n7slk2njwlqrxbjhqxk1zvbvkixj") (f (quote (("std" "multiversion-macros/std") ("default" "std"))))))

(define-public crate-multiversion-0.7.4 (c (n "multiversion") (v "0.7.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "multiversion-macros") (r "^0.7.4") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "target-features") (r "^0.1") (d #t) (k 0)))) (h "0hm1y7dhdbam2yvaxmxzd0bj7gv777y0zn82jjzx0fhxl5hi31f4") (f (quote (("std" "multiversion-macros/std") ("default" "std"))))))

