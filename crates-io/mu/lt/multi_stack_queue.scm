(define-module (crates-io mu lt multi_stack_queue) #:use-module (crates-io))

(define-public crate-multi_stack_queue-0.1.0 (c (n "multi_stack_queue") (v "0.1.0") (h "13p8z26m2pdqc3j0svbrrg7ywv34kc88as67g4vn5yjg7nyhkah7")))

(define-public crate-multi_stack_queue-0.2.0 (c (n "multi_stack_queue") (v "0.2.0") (h "0n9r30nvpv9g81p916jhf8xn5sxdbzkijzfh65v708l4hqd8ccpb")))

(define-public crate-multi_stack_queue-0.2.1 (c (n "multi_stack_queue") (v "0.2.1") (h "1xf2n24ll5syipjl4ifav6jr1s6x12bzqb4c4md0i734v7rs3xjw")))

(define-public crate-multi_stack_queue-0.2.2 (c (n "multi_stack_queue") (v "0.2.2") (h "03yx9lijs8lipmdni13i7hfmjvhjjsk5ky1ikgr8mvcz9md1qlrg")))

(define-public crate-multi_stack_queue-0.3.0 (c (n "multi_stack_queue") (v "0.3.0") (h "06fszb8kiwya1rng8i9lzymk0k064xli6n6c4ps5c9rh53733cy7")))

