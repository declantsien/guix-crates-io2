(define-module (crates-io mu lt multi_dim_point) #:use-module (crates-io))

(define-public crate-multi_dim_point-0.1.0 (c (n "multi_dim_point") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0xwgpwzxm2hnz9c24ba3kxfgpyi3yrrkdfzrpwqxvngrdc4sg0j7")))

(define-public crate-multi_dim_point-0.2.0 (c (n "multi_dim_point") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0333qrysydmbqcp9vg6nwxfi696r0q4kazw210bl13zv00b275lq")))

(define-public crate-multi_dim_point-0.3.0 (c (n "multi_dim_point") (v "0.3.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "01plsrisadx3424xwhbmhn5rgix15cyvgrs8id3w9izb0nqad67s")))

