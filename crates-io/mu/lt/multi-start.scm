(define-module (crates-io mu lt multi-start) #:use-module (crates-io))

(define-public crate-multi-start-0.1.0 (c (n "multi-start") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "0q7s2b4ifj9zcngnis542qff86vhhagrc9wbpnrlbaka4wn1zm16")))

(define-public crate-multi-start-0.1.1 (c (n "multi-start") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "1kqzm2ldwikmqqdgww76nqvn9b94z7dy9g3zk4d32i8d9h77y8xr")))

(define-public crate-multi-start-0.1.2 (c (n "multi-start") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "1sz7p38avf4nk9gm7r89iih9m9n6s240vlwszs2wc49pq5zlhqz9")))

