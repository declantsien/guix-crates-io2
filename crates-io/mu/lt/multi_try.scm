(define-module (crates-io mu lt multi_try) #:use-module (crates-io))

(define-public crate-multi_try-0.1.0 (c (n "multi_try") (v "0.1.0") (h "13k5v1ncbchh0dclcw9ksvmyprhid7sy29qfs405qmbc7vdfcx8f")))

(define-public crate-multi_try-0.2.0 (c (n "multi_try") (v "0.2.0") (h "0761vc70hzgsf1dhcvxh2d2q1h4izp6ciglf1b7sh9sxdnfisib3") (f (quote (("nightly"))))))

(define-public crate-multi_try-0.3.0 (c (n "multi_try") (v "0.3.0") (h "12pfd1qb7jnj6ihyfvszcdh4dbjjc1w649rfyj6106azmgl5c8ml")))

