(define-module (crates-io mu lt multichannel) #:use-module (crates-io))

(define-public crate-multichannel-0.1.0 (c (n "multichannel") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.19") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0md775nj8zpg8h3rk87m6iv9ifsyhrkzi66ii03kkmrpr16fx8c7")))

(define-public crate-multichannel-0.2.0 (c (n "multichannel") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.19") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.13.2") (f (quote ("union"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "108rrll00067wg9mqjvd8mjqi5y4pcrd1b6agk0dbi0bpp20njd3")))

