(define-module (crates-io mu lt multihash-derive-impl) #:use-module (crates-io))

(define-public crate-multihash-derive-impl-0.1.0 (c (n "multihash-derive-impl") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0h1hbha4kk3rsdsnkkxbr3f256xs8zpcdkqyhxcqccyvibh8b1nk")))

