(define-module (crates-io mu lt multi_containers) #:use-module (crates-io))

(define-public crate-multi_containers-0.1.0 (c (n "multi_containers") (v "0.1.0") (h "1hil8fxj8rm5amqwdqy5vizg201fbcvfrhv6b3zga2xy7mhazh85")))

(define-public crate-multi_containers-0.1.1 (c (n "multi_containers") (v "0.1.1") (h "1q3l1shbflvpn9n2lzp29zw6wg43wwbk662yqgfpdhm88gpnl5d1")))

(define-public crate-multi_containers-0.1.2 (c (n "multi_containers") (v "0.1.2") (h "1nx4vc80iq0accr7251jddkwdzfwg0msyz1qn4kamm6gz7ywz8ns")))

(define-public crate-multi_containers-0.1.3 (c (n "multi_containers") (v "0.1.3") (h "0f8x074c2bxs05nv04hjk2d3mnnbm4075cjp690njw86l56y4ar6")))

(define-public crate-multi_containers-0.2.0 (c (n "multi_containers") (v "0.2.0") (h "0pbkv6k5n4gwahf8siwd3m5iv0rmn67sxnph22ax8bv8d0l11f0a")))

(define-public crate-multi_containers-0.2.1 (c (n "multi_containers") (v "0.2.1") (h "0gd9d80wbnrq2w6yc9vxd565d7pxk0qwzh8dlcx3f0kkd9nabi0x")))

