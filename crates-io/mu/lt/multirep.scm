(define-module (crates-io mu lt multirep) #:use-module (crates-io))

(define-public crate-multirep-0.1.0 (c (n "multirep") (v "0.1.0") (h "0yl96vjvf3pndpsm24j6z8045n5qxx5hfyxc59s9iz6ngdh7ymy8") (y #t)))

(define-public crate-multirep-0.2.0 (c (n "multirep") (v "0.2.0") (h "0ws127gr0n4xlc5v322dls6mbp1nnwxhz63wgivvqc93zchk9j49")))

(define-public crate-multirep-0.2.1 (c (n "multirep") (v "0.2.1") (h "12bvvhrvkhywg5shxrc3k1d4m8mbch9a6z6awxnbvdi31kh21vvs")))

