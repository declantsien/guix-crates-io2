(define-module (crates-io mu lt multiprocessing) #:use-module (crates-io))

(define-public crate-multiprocessing-0.1.0 (c (n "multiprocessing") (v "0.1.0") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "nix") (r "^0") (d #t) (k 0)))) (h "0icfr07zbw1fn743r7xnr6l2l60rf2n7q9mdfhgsfi6p5k6llvb7")))

(define-public crate-multiprocessing-0.1.1 (c (n "multiprocessing") (v "0.1.1") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "nix") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0mqy1i3nwh2y9si21kzqhphvgq4ddn3nmgvwz6gxfb9dd5rq3chr")))

