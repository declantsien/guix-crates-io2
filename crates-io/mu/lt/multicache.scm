(define-module (crates-io mu lt multicache) #:use-module (crates-io))

(define-public crate-multicache-0.1.0 (c (n "multicache") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)))) (h "1gl86s6bcqrhnaxl3l7rs0m9xjdrymbqgckx0knvgx7996mmzvj2")))

(define-public crate-multicache-0.1.1 (c (n "multicache") (v "0.1.1") (d (list (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)))) (h "12qh6nivpj590fwar2n9ssyqczyxvkhdc2mwzv094ayga9j1qdwa")))

(define-public crate-multicache-0.1.2 (c (n "multicache") (v "0.1.2") (d (list (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)))) (h "05hi42zkmc2xicixipc25l74j2igbzfm4l0z6kwrvkcnsvfl6dvs")))

(define-public crate-multicache-0.1.3 (c (n "multicache") (v "0.1.3") (d (list (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)))) (h "086chc855ji2q7p4vplzjs9gzwynjvzcjwcv0q5w5hx2l4q4czd6")))

(define-public crate-multicache-0.2.0 (c (n "multicache") (v "0.2.0") (d (list (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)))) (h "0pjpadk7vdvmq69cjjvl3d9bswywc0scf9z3b32bi4cr8rp1r9qg")))

(define-public crate-multicache-0.3.0 (c (n "multicache") (v "0.3.0") (d (list (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)))) (h "0xvqgl444ikzpvl89lchg702ynhi7xxzdfvagg3q3jm35cfrwihk")))

(define-public crate-multicache-0.3.1 (c (n "multicache") (v "0.3.1") (d (list (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)))) (h "1vqh9vhb19mg93lz5g1s6a162gigi8fbmsdi0hcp0xakc2ffs1vw")))

(define-public crate-multicache-0.4.0 (c (n "multicache") (v "0.4.0") (d (list (d (n "linked-hash-map") (r "^0.4.2") (d #t) (k 0)))) (h "0jicj7qd1c0b7q53w4gq2w6j47hk10npsx8x8mfi0vwyyrax5siy")))

(define-public crate-multicache-0.4.1 (c (n "multicache") (v "0.4.1") (d (list (d (n "linked-hash-map") (r "^0.5.0") (d #t) (k 0)))) (h "1zd9lyq0bqmh51rgm50apa9ll6knjah9z4n28qzy5yh20c6n7v7p")))

(define-public crate-multicache-0.5.0 (c (n "multicache") (v "0.5.0") (d (list (d (n "linked-hash-map") (r "^0.5.0") (d #t) (k 0)))) (h "0a2yi6a3nkcjcvpjlrhw0vrh7xy3y4ym31ssf8sqflc5w835cql1")))

(define-public crate-multicache-0.6.0 (c (n "multicache") (v "0.6.0") (d (list (d (n "linked-hash-map") (r "^0.5.0") (d #t) (k 0)))) (h "0957zx19rjrq6fpp1h3gf8d9zffkqjszxlcfcd2wr7w934nmb1i6")))

(define-public crate-multicache-0.6.1 (c (n "multicache") (v "0.6.1") (d (list (d (n "linked-hash-map") (r "^0.5.0") (d #t) (k 0)))) (h "0wms17fg6d4rhjlrhh6g7r1fhcs4rfxx20w7m059h4h81960g1jh")))

