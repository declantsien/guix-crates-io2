(define-module (crates-io mu lt multilingual-demo) #:use-module (crates-io))

(define-public crate-multilingual-demo-0.1.0 (c (n "multilingual-demo") (v "0.1.0") (d (list (d (n "handlebars") (r "^3.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "warp") (r "^0.2") (f (quote ("tls"))) (d #t) (k 0)))) (h "02sbilc3bgxlzm50gr06lx8l5qlg87scg1k22s0zhvbih9kj23wc")))

(define-public crate-multilingual-demo-0.1.1 (c (n "multilingual-demo") (v "0.1.1") (d (list (d (n "handlebars") (r "^3.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "warp") (r "^0.2") (f (quote ("tls"))) (d #t) (k 0)))) (h "16iw4znfgvnlhl0d16ci5bmr9c446k4m8kjyvqrv2asysq217a7j")))

