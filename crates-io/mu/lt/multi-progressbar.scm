(define-module (crates-io mu lt multi-progressbar) #:use-module (crates-io))

(define-public crate-multi-progressbar-0.1.0 (c (n "multi-progressbar") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "1v6dc1l0gky888gi70dm2qwbw5mww3gc5wzhd1d2smcbaswfxc9q") (s 2) (e (quote (("bin" "dep:clap"))))))

