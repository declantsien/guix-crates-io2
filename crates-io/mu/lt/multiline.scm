(define-module (crates-io mu lt multiline) #:use-module (crates-io))

(define-public crate-multiline-0.1.0 (c (n "multiline") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "cotton") (r "^0.0.1") (d #t) (k 0)) (d (n "multistream-batch") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "1h04sb812ii1qrz7kvpk61i4syh5qv18njwznkp6xic9bdgpp7bi")))

(define-public crate-multiline-0.1.1 (c (n "multiline") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "cotton") (r "^0.0.1") (d #t) (k 0)) (d (n "multistream-batch") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "1bvcwysxad036vqlg4vwkh2lkayhcc62azjghzdyrkysm0giaibh")))

