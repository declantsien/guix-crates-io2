(define-module (crates-io mu lt multisol-structs) #:use-module (crates-io))

(define-public crate-multisol-structs-1.0.0 (c (n "multisol-structs") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "13z5j8svpdzzlhn767rzv6xvrl3kyd2bmqrlh9c6h3dq02ca5ssj")))

(define-public crate-multisol-structs-1.0.1 (c (n "multisol-structs") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0llvs7b9s9ig942dyp0rvw3wfzr7jfgvfk9ly98dczx5xv3ip481")))

(define-public crate-multisol-structs-1.0.2 (c (n "multisol-structs") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1h7qcg2va644ar8rbvvj1bqvyshpdrzf9vchr1n52vj17fr338a8")))

(define-public crate-multisol-structs-1.1.0 (c (n "multisol-structs") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1myw6z603xyafx59ah1qyabjlsialy1jwlz2vpi7yry616k3x22s")))

