(define-module (crates-io mu lt multiinput) #:use-module (crates-io))

(define-public crate-multiinput-0.0.2 (c (n "multiinput") (v "0.0.2") (d (list (d (n "hid-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 0)))) (h "0hrmk3x2j53y382hnrbvvykxlfwwnv1kcl9g433068arnqhyna2a")))

(define-public crate-multiinput-0.0.3 (c (n "multiinput") (v "0.0.3") (d (list (d (n "hid-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 0)))) (h "1xhfx0pfrlylgx57wj5naka0xxz2mc0n3wd9z6v1cwrn7qv7gcfx")))

(define-public crate-multiinput-0.0.4 (c (n "multiinput") (v "0.0.4") (d (list (d (n "hid-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 0)))) (h "0my8cycj5va7s22kbs6lf7h0p1d0nwskzr2bm560h7czzpq16wkr")))

(define-public crate-multiinput-0.0.5 (c (n "multiinput") (v "0.0.5") (d (list (d (n "hid-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 0)))) (h "182z6lgd9dd6dgw1rj7lksw1b2qdryhxm4wsq22qfh4zic3qv9y9")))

(define-public crate-multiinput-0.0.6 (c (n "multiinput") (v "0.0.6") (d (list (d (n "hid-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 0)))) (h "0m0ga2kqx3q1y9dk0bzy3glrkdc88pd65y295wb2k2jr29zsg0ja")))

(define-public crate-multiinput-0.0.7 (c (n "multiinput") (v "0.0.7") (d (list (d (n "hid-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.0") (d #t) (k 0)))) (h "1ska5jq5j1w03508nyxjpal0ng7j1fjwcyyy09wn8i87nk68i0fa")))

(define-public crate-multiinput-0.0.8 (c (n "multiinput") (v "0.0.8") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser" "basetsd" "hidpi" "winnt" "libloaderapi" "fileapi" "hidsdi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1pnkwwxrs2wmsva3hp5dx4gmcm1x1zq7aj0l3iwc9khr2bk3kgk1")))

(define-public crate-multiinput-0.0.9 (c (n "multiinput") (v "0.0.9") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser" "basetsd" "hidpi" "winnt" "libloaderapi" "fileapi" "hidsdi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1jck5xr7vsnhr3nv5hchlkaiy6i3y04sx667nqgwrwx974f44bl5")))

(define-public crate-multiinput-0.0.10 (c (n "multiinput") (v "0.0.10") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser" "basetsd" "hidpi" "winnt" "libloaderapi" "fileapi" "hidsdi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0z1jkdlkgp5prkw3qh4qa2x3xvzzhkc5f1mrkyvggg8y484ncsai")))

(define-public crate-multiinput-0.0.11 (c (n "multiinput") (v "0.0.11") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser" "basetsd" "hidpi" "winnt" "libloaderapi" "fileapi" "hidsdi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15pqgscqdkiyfqyk8a2mh5095yvh7v3df9z91c8c6harzdnr30qa")))

(define-public crate-multiinput-0.0.12 (c (n "multiinput") (v "0.0.12") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser" "basetsd" "hidpi" "winnt" "libloaderapi" "fileapi" "hidsdi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19jkav6q1s24rsxn3zj6lrnqjiycd7shny3vqyz7pd71rcf19nm8")))

(define-public crate-multiinput-0.0.13 (c (n "multiinput") (v "0.0.13") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser" "basetsd" "hidpi" "winnt" "libloaderapi" "fileapi" "hidsdi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lngvqmhvp862jfnymp8aa3921438qjwnk51vaz666xvvrhwqrfc")))

(define-public crate-multiinput-0.0.14 (c (n "multiinput") (v "0.0.14") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser" "basetsd" "hidpi" "winnt" "libloaderapi" "fileapi" "hidsdi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0399mj5ds8kw2fx90wrl0wh9yv68j28r6hlzzw85dlm2bkarac5h")))

(define-public crate-multiinput-0.0.15 (c (n "multiinput") (v "0.0.15") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser" "basetsd" "hidpi" "winnt" "libloaderapi" "fileapi" "hidsdi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wd2v2kf094in4d9bl18xrby5fs6wpsm8985w47vs9y4fxy6frz3")))

(define-public crate-multiinput-0.1.0 (c (n "multiinput") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser" "basetsd" "hidpi" "winnt" "libloaderapi" "fileapi" "hidsdi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0bvf2dm3fgsarpycbgx8ssh6v6k6vx46ivp7mhs15k50rd8mrwak")))

