(define-module (crates-io mu lt multiwii-serial-protocol-2) #:use-module (crates-io))

(define-public crate-multiwii-serial-protocol-2-0.1.2 (c (n "multiwii-serial-protocol-2") (v "0.1.2") (d (list (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "packed_struct") (r "^0.3") (d #t) (k 0)) (d (n "packed_struct_codegen") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "16xhz5lm3pzqxb45qzv0nisjhlca9l8660xp1ak2g3bjkd3q62ls") (f (quote (("std") ("no_std") ("default" "std")))) (y #t)))

