(define-module (crates-io mu lt multitask) #:use-module (crates-io))

(define-public crate-multitask-0.0.1 (c (n "multitask") (v "0.0.1") (h "0x549vhxhxz75fc72zzphjbxcsii6g77d9d0rc78nvdwglv2xbgl")))

(define-public crate-multitask-0.1.0 (c (n "multitask") (v "0.1.0") (d (list (d (n "async-io") (r "^0.1.1") (d #t) (k 2)) (d (n "async-task") (r "^3.0.0") (d #t) (k 0)) (d (n "blocking") (r "^0.4.6") (d #t) (k 2)) (d (n "concurrent-queue") (r "^1.1.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.2.4") (d #t) (k 0)) (d (n "parking") (r "^1.0.3") (d #t) (k 2)) (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "040qyynx9lwnxiphc690kf096rii7is0sfz4w38safq3ngsvmgkv")))

(define-public crate-multitask-0.2.0 (c (n "multitask") (v "0.2.0") (d (list (d (n "async-io") (r "^0.1.1") (d #t) (k 2)) (d (n "async-task") (r "^3.0.0") (d #t) (k 0)) (d (n "blocking") (r "^0.4.6") (d #t) (k 2)) (d (n "concurrent-queue") (r "^1.1.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.2.4") (d #t) (k 0)) (d (n "parking") (r "^1.0.3") (d #t) (k 2)))) (h "1d55m9jxhyg1m73c31xmq438xaz8r3r134bp17vvbkbx3qkkb760")))

