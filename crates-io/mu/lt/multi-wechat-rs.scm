(define-module (crates-io mu lt multi-wechat-rs) #:use-module (crates-io))

(define-public crate-multi-wechat-rs-0.1.0 (c (n "multi-wechat-rs") (v "0.1.0") (d (list (d (n "ntapi") (r "^0.3.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "tlhelp32" "psapi" "winreg" "winbase" "winuser" "securitybaseapi" "impl-default" "processthreadsapi" "handleapi"))) (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (t "cfg(windows)") (k 1)))) (h "1aaqknzgd93nm3c0fcyc9ips2ryxj6a08d257wnmbznlkffy5vnr")))

