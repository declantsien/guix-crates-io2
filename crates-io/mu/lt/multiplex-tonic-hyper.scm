(define-module (crates-io mu lt multiplex-tonic-hyper) #:use-module (crates-io))

(define-public crate-multiplex-tonic-hyper-0.1.0 (c (n "multiplex-tonic-hyper") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "http-body") (r "^0.4.5") (d #t) (k 2)) (d (n "hyper") (r "^0.14.20") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 2)) (d (n "tokio") (r "^1.20") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "tonic") (r "^0.8") (d #t) (k 2)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "1z427ixnfcdh1qk9h371v5g9p3jh73zs574lq2m4d96005w1abq0")))

