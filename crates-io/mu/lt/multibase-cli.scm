(define-module (crates-io mu lt multibase-cli) #:use-module (crates-io))

(define-public crate-multibase-cli-0.1.0 (c (n "multibase-cli") (v "0.1.0") (d (list (d (n "multibase") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "052g227s63dnvbk73sv3q3ycmxx5sak49qfilhrpv8cylv54a4z5")))

(define-public crate-multibase-cli-0.1.1 (c (n "multibase-cli") (v "0.1.1") (d (list (d (n "multibase") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0w4lmkhvbh3qd283rwcd3kcghjb58lx9spj9akv1c68ikq3yi1ld")))

