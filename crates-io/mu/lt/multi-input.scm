(define-module (crates-io mu lt multi-input) #:use-module (crates-io))

(define-public crate-multi-input-0.0.1 (c (n "multi-input") (v "0.0.1") (d (list (d (n "hid-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 0)))) (h "0r3qhrnpviasys329l4p2g58p4pmja5zk40yvdvapc3dw0ldrlsl")))

