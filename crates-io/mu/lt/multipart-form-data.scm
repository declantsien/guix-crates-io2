(define-module (crates-io mu lt multipart-form-data) #:use-module (crates-io))

(define-public crate-multipart-form-data-0.1.0 (c (n "multipart-form-data") (v "0.1.0") (d (list (d (n "isahc") (r "^1.6") (k 2)) (d (n "multipart-boundary") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0n0ihk7nahys8v3g2ay738hgqgbxavxkbws4c7as7incfkyxh75w")))

