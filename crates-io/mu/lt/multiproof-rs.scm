(define-module (crates-io mu lt multiproof-rs) #:use-module (crates-io))

(define-public crate-multiproof-rs-0.1.0 (c (n "multiproof-rs") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "1nf3szdw55r2kwkxdhfaq0n021v53wmnmcyhb370sx7a8x9zq3fp")))

(define-public crate-multiproof-rs-0.1.2 (c (n "multiproof-rs") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "02lhk2vsqn67wn2xp5xpba2wjv3y2bvymy3jw9way5rxkv5h6amd")))

(define-public crate-multiproof-rs-0.1.3 (c (n "multiproof-rs") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0mhskr7g4b5g3273imgn7ni8j6rx5n35hkb64v2f5c3xs8ncv58l")))

(define-public crate-multiproof-rs-0.1.4 (c (n "multiproof-rs") (v "0.1.4") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "1yl6akk4mn5a397pfj8ch5mjw3n6rjmhx48sld39bcl49wsdpgwf")))

(define-public crate-multiproof-rs-0.1.5 (c (n "multiproof-rs") (v "0.1.5") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "1l1zv161qdja30bs8r877i8v1k0imghbv3aqli4j1k4jrhcs794x")))

(define-public crate-multiproof-rs-0.1.6 (c (n "multiproof-rs") (v "0.1.6") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "1ikyz27n60sq02cqnv7wh2cl7rcpl2yxbgj0fhazgbg6yqicsb6n")))

(define-public crate-multiproof-rs-0.1.7 (c (n "multiproof-rs") (v "0.1.7") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "1gdf1spms2dpj4w646h0h4chqawrxrz10rncf80zqkjjh014fl8c")))

(define-public crate-multiproof-rs-0.1.8 (c (n "multiproof-rs") (v "0.1.8") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0fzxvq0r17x9kg7ynqbkr7m0r6h2xnzkb2g0drb8183bkj5p8jxv")))

(define-public crate-multiproof-rs-0.1.9 (c (n "multiproof-rs") (v "0.1.9") (d (list (d (n "arbitrary") (r "^0.4.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rlp") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0jinram9lqjyvp4fxi36bygd5c3jbimbc8mkgxp1kvk4fmjg0byl")))

