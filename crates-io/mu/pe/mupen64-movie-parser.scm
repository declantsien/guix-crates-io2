(define-module (crates-io mu pe mupen64-movie-parser) #:use-module (crates-io))

(define-public crate-mupen64-movie-parser-1.0.0 (c (n "mupen64-movie-parser") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0r67iwk16x4gdgqscswqsyggxcfg7x56lhlqyk4dvzcyn5cnl7hp") (y #t)))

(define-public crate-mupen64-movie-parser-1.0.1 (c (n "mupen64-movie-parser") (v "1.0.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ahycj15ankl6dy359ymkpwbpcyqp6crc8fzj3wp1wzmsgpnmy0i")))

(define-public crate-mupen64-movie-parser-2.0.0 (c (n "mupen64-movie-parser") (v "2.0.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1069safwqnqz46i3qgrlnfp83fcysp3ssky5ns2zlf9kq4h4kdaf")))

