(define-module (crates-io mu pe mupen64plus-sys) #:use-module (crates-io))

(define-public crate-mupen64plus-sys-0.1.0 (c (n "mupen64plus-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1kyfspmqhr8j565wmmilqj5jicri6qyl3k8cc5fy4999jr45yyi2")))

(define-public crate-mupen64plus-sys-0.2.0 (c (n "mupen64plus-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0jz4xbj4rvp6wrqp7i90k64ff0pap263s67mqj6kh0fvx9f8fx4p")))

