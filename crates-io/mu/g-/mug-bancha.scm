(define-module (crates-io mu g- mug-bancha) #:use-module (crates-io))

(define-public crate-mug-bancha-0.0.0 (c (n "mug-bancha") (v "0.0.0") (h "0pv31cpaybcsg0vdmax91i5ij0dqaid59jjsqxbz7idl6zmhhrnh") (y #t)))

(define-public crate-mug-bancha-0.0.1-discontinued (c (n "mug-bancha") (v "0.0.1-discontinued") (h "04j5q87kwfpbc3sblc5a6pjlj319n9yz8a9akfcs3ibcg79vxw54")))

