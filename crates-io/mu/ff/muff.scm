(define-module (crates-io mu ff muff) #:use-module (crates-io))

(define-public crate-muff-0.1.0 (c (n "muff") (v "0.1.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 0)))) (h "12cp83yq3404x56a1zrylgpps175sw4l8ilv36mxs9bniz1ps63y")))

(define-public crate-muff-0.1.1 (c (n "muff") (v "0.1.1") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 0)))) (h "0igf83pvs0kwnn3xhhyg3bsm5qqfpcl5d3imckp0g4aghvi694y1")))

(define-public crate-muff-0.1.2 (c (n "muff") (v "0.1.2") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 0)))) (h "0v71imwfab5shfl8x5arvbw88c9wq5n9r8d95af4nz65a1jgx3sj")))

(define-public crate-muff-0.1.3 (c (n "muff") (v "0.1.3") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 0)))) (h "0pkk8q9pwdrlms65zjar0wg3rv86sxmdqjljnl9dlm0k3dccnj01")))

(define-public crate-muff-0.1.4 (c (n "muff") (v "0.1.4") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 0)))) (h "1v8cf642f74gciwqzb99q41zrhnzx1cmj8iy0nn5g9v084gsyrns")))

