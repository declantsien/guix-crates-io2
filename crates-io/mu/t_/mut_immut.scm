(define-module (crates-io mu t_ mut_immut) #:use-module (crates-io))

(define-public crate-mut_immut-0.1.0 (c (n "mut_immut") (v "0.1.0") (h "1rcy92i2xb4y5nlqdpv1ppf62h76mbgb14d69nvv6g2f9w3cfh4n") (y #t)))

(define-public crate-mut_immut-0.2.0 (c (n "mut_immut") (v "0.2.0") (h "1nsymmpnp9wylp9nv5znypnw69k3ypl2vlfb77rz4678lr4p62j1") (y #t)))

(define-public crate-mut_immut-0.2.1 (c (n "mut_immut") (v "0.2.1") (h "1cii3mclwjllwkyjh4nhcp5dydbkprsha9rnfdwprzl5x5pprlv0") (y #t)))

