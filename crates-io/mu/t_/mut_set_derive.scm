(define-module (crates-io mu t_ mut_set_derive) #:use-module (crates-io))

(define-public crate-mut_set_derive-0.1.2 (c (n "mut_set_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "046kdjyyw5bkkg84ka4amq5h6c1bzfhjinrv6ppscmi476b9znwb")))

(define-public crate-mut_set_derive-0.2.0 (c (n "mut_set_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0cigsbj2dm2k3f2vnfx3psgqr1bpy6b2jmll655zx50g9jaa83jl")))

(define-public crate-mut_set_derive-0.2.1 (c (n "mut_set_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "1w51vhsfvx3dwd7ski5c5nmpjhxzjvrjxgi6qik812h6iwp8mzhn")))

(define-public crate-mut_set_derive-0.2.2 (c (n "mut_set_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "1jfw5z6yav26hyii4lzms62wcdy67k3552cmb7p90fdw35n4bwzz")))

(define-public crate-mut_set_derive-0.2.3 (c (n "mut_set_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0d9dr7bkwb9157fdaznb8w0mra63hilhas6zkxs2gczhnfq9p8zz")))

(define-public crate-mut_set_derive-0.2.4 (c (n "mut_set_derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0hzakz054iq85xfi42f8sc3w24xl9nh89l0w5swkb0ci47yaf0av")))

(define-public crate-mut_set_derive-0.2.5 (c (n "mut_set_derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "1pch2zvxxnzp3rl51cznfy8lv1b7jlm31iidb5rksaskhg7y8r30")))

(define-public crate-mut_set_derive-0.2.6 (c (n "mut_set_derive") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "1yr7bqsg58v6vkz906plh54z72245x18l0ackb8krfdms5ba9f4r")))

(define-public crate-mut_set_derive-0.2.7 (c (n "mut_set_derive") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "13il3idh73nlc0n3y9w6dvr12cdb3j50gknsaqav96w5z70p2ayz")))

(define-public crate-mut_set_derive-0.2.8 (c (n "mut_set_derive") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "06p554lws026bgvjzdhb5i4vwjxhjs3273vmxdiyvkqivn6980y0")))

(define-public crate-mut_set_derive-0.2.9 (c (n "mut_set_derive") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0a65vg1996z3qj48cbx0z3pq9vzcam84pd38icvfbjrngdjpqads")))

(define-public crate-mut_set_derive-0.3.0 (c (n "mut_set_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0pwwj8q5k20n5jdqxzpiy9kjiis2d5y766p7d6sbkkpnx32b49f4")))

(define-public crate-mut_set_derive-0.3.1 (c (n "mut_set_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0gz1m7s8bc9bxkbqxgi8xq8sfjy4fppqfa8r8jc3612gcassqqhx")))

(define-public crate-mut_set_derive-0.3.2 (c (n "mut_set_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "00d4qsp0q3r6902dsq10cdp5sbwchb85ihnq3wi9g85gvn8q4vsd")))

(define-public crate-mut_set_derive-0.3.3 (c (n "mut_set_derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0vf6xnbys15zikq914x66w36amcfn64gsfmdr7pgik1x0pz03185")))

(define-public crate-mut_set_derive-0.3.4 (c (n "mut_set_derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "17c1c782z9yl1aqcf1rqfkb2nwwm4mnsd4c584crfyqpc0ic5zwm")))

(define-public crate-mut_set_derive-0.3.5 (c (n "mut_set_derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "1ysv0kpi244hhz7xgfgdlzby42qqvqch5k2rdhy16x3rm0rq023y")))

