(define-module (crates-io mu t_ mut_set) #:use-module (crates-io))

(define-public crate-mut_set-0.1.0 (c (n "mut_set") (v "0.1.0") (h "0cfn350006k2nki8scaq0b2vrcg1fd6w2wdm446k93j9r17khfyb")))

(define-public crate-mut_set-0.1.1 (c (n "mut_set") (v "0.1.1") (h "1dgclzm2mhgvv1vpxr3rb8ag734vn4ha00p47nl3yarg3as15l59")))

(define-public crate-mut_set-0.1.2 (c (n "mut_set") (v "0.1.2") (h "0w4l859gacznghkdjyks2r9yvypdjf8fzfhv8siw8bv1nnar4zzb")))

(define-public crate-mut_set-0.2.0 (c (n "mut_set") (v "0.2.0") (h "1f3isdv6zjxw7afgja193948a64r0xwwd0v2qr7jj0j8xqkdyq7f")))

(define-public crate-mut_set-0.2.1 (c (n "mut_set") (v "0.2.1") (h "1kfgzyq3q1v65sqlkn2iaznik6dv6il9jcrb6a7xsfal6a7jf20d")))

(define-public crate-mut_set-0.2.2 (c (n "mut_set") (v "0.2.2") (h "0ygf3zrb15fi8rrv2b1y4n0m5vq3yiaalg0w4l663miy8xdz9y4b")))

(define-public crate-mut_set-0.2.3 (c (n "mut_set") (v "0.2.3") (h "0vm99hxz5zalywjkcvp53jbci1brcq0w3skkgswxc5372pxlprz2")))

(define-public crate-mut_set-0.2.4 (c (n "mut_set") (v "0.2.4") (h "1cm7xf8f8s3jcscdbbz5yv6hqr3b92dfbwlk19q2k1gk6mxsfkn8")))

(define-public crate-mut_set-0.2.5 (c (n "mut_set") (v "0.2.5") (h "1brvz08aj50qans7nlcp240kvdm1angy6qzr75dbl50aaw25p6f6")))

(define-public crate-mut_set-0.2.6 (c (n "mut_set") (v "0.2.6") (h "0izvk7gi8f91mgqbwgwb94101nalk8rm6k1wxj7sbb7gnnis8f78")))

(define-public crate-mut_set-0.2.7 (c (n "mut_set") (v "0.2.7") (h "08xd3mdknivdrcvlpmpi7qxjzyj5d6vvjz7k0y4cfbxcfmfd348v")))

(define-public crate-mut_set-0.2.8 (c (n "mut_set") (v "0.2.8") (h "0wabcdj9i418l2bx2ya3s07l4dxkiqv6c49adgirzqb3qa09cfqz")))

(define-public crate-mut_set-0.2.9 (c (n "mut_set") (v "0.2.9") (h "1bkpb91pb8c79axkn7fnqgl4mngc2x2cp2swn1ghhxp0rs258b5d")))

(define-public crate-mut_set-0.3.0 (c (n "mut_set") (v "0.3.0") (h "0paq32i6i1m21kwx36ljwvgd041j99in9gpzfd2j4wfvpdfj4cki")))

(define-public crate-mut_set-0.3.1 (c (n "mut_set") (v "0.3.1") (h "1g69r2f65gn7y2q0mipx8kinf5k0ragcy5h8aqc5r4mi386lcan8")))

(define-public crate-mut_set-0.3.2 (c (n "mut_set") (v "0.3.2") (h "0kml91b7j8q59hhcw4k5qcnvlb6kw3ydas1kyilkfcgld524sghg")))

(define-public crate-mut_set-0.3.3 (c (n "mut_set") (v "0.3.3") (h "0jj9al6gc4yx1r5pzs1mgkanjmbh6id89yk8aylpsqin5jfwn2l9")))

(define-public crate-mut_set-0.3.4 (c (n "mut_set") (v "0.3.4") (h "1kipmx2m8bx0pmx1mh3zmvclqv7ky11ysd57iggamki1qfllwqlv")))

(define-public crate-mut_set-0.3.5 (c (n "mut_set") (v "0.3.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0gjivcs3ywiwrxls5wq5bps7297nvlggib0p6zmlh23z4lfhv4bc")))

