(define-module (crates-io mu t_ mut_static) #:use-module (crates-io))

(define-public crate-mut_static-0.1.0 (c (n "mut_static") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "076xyvjbfjwrlq98qr93jsgri320yrjvx6fqaa6hcxyd3ism3mnx")))

(define-public crate-mut_static-1.0.0 (c (n "mut_static") (v "1.0.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "16nq25b3jmbc45ikf82nqrb5qm7206gb2ixji8w3833c8x8wyncl")))

(define-public crate-mut_static-1.0.1 (c (n "mut_static") (v "1.0.1") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "0rr3lhkkdzwjcplm95y85mw2373ni8vv7fjvpzgf2cifrg1sdiim")))

(define-public crate-mut_static-2.0.0 (c (n "mut_static") (v "2.0.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "1s9mfilxiszrs7a7c67imcw9a34pvqzv8f4sgy5f6jl44xjic9ra")))

(define-public crate-mut_static-3.0.0 (c (n "mut_static") (v "3.0.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "0nm6yx6zzwwh1avkck86yjdsmcfy5l001m8k89440070y42s3il5")))

(define-public crate-mut_static-4.0.0 (c (n "mut_static") (v "4.0.0") (d (list (d (n "error-chain") (r "^0.9") (k 0)))) (h "0rznxzmqhn5bgxhqsjgcx37rdlchrj5p02l037a8q0p86xh2kwcw")))

(define-public crate-mut_static-5.0.0 (c (n "mut_static") (v "5.0.0") (d (list (d (n "error-chain") (r "^0.10") (k 0)))) (h "0rkg6d34kxsgzz206zn054f2gfsb38brlz54pcc3ckls73mxi314")))

