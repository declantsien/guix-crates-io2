(define-module (crates-io mu t_ mut_guard) #:use-module (crates-io))

(define-public crate-mut_guard-0.1.0 (c (n "mut_guard") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0yr89mz4l2m24v27bdijjnynaxfycb5yh2bdabg398hw7mr56q26")))

