(define-module (crates-io mu t_ mut_family) #:use-module (crates-io))

(define-public crate-mut_family-0.1.0 (c (n "mut_family") (v "0.1.0") (h "1l9l18crjsly6qw392l6vdj0pjy87x833flckir7gk96sjgv9q0r") (r "1.65.0")))

(define-public crate-mut_family-0.1.1 (c (n "mut_family") (v "0.1.1") (h "0k3kd3dg0f4xp04zqqvyspwas13ikya2403c7kyahjqhr8dl96i5") (r "1.65.0")))

(define-public crate-mut_family-0.1.2 (c (n "mut_family") (v "0.1.2") (d (list (d (n "sealed") (r "^0.4") (d #t) (k 0)))) (h "19zlsn5j952505hdwflwmk7idap3psr83h31775g8vxrsqrkbqx3") (r "1.65.0")))

(define-public crate-mut_family-0.1.3 (c (n "mut_family") (v "0.1.3") (d (list (d (n "sealed") (r "^0.4") (d #t) (k 0)))) (h "1hpq1mjh91hz04vcgg41f6jwaa8plr7d64hlzqzkdllj5c5l0xgz") (r "1.65.0")))

(define-public crate-mut_family-0.1.4 (c (n "mut_family") (v "0.1.4") (d (list (d (n "sealed") (r "^0.4") (d #t) (k 0)))) (h "0wd6187myssvzi5hd3hxrxcmqwac4aa0g7lpi012902xxb4yqapd") (r "1.65.0")))

(define-public crate-mut_family-0.1.5 (c (n "mut_family") (v "0.1.5") (d (list (d (n "sealed") (r "^0.4") (d #t) (k 0)))) (h "0bbv5j1s6p0svjk82wil1x1w1940y1gi9j95555v8yzy790j9w5h") (r "1.65.0")))

(define-public crate-mut_family-0.1.6 (c (n "mut_family") (v "0.1.6") (d (list (d (n "sealed") (r "^0.4") (d #t) (k 0)))) (h "0bywn02hixlz44yq6x31mqppq8z8vqvjqwlnjq1vvsz9pxs97dnh") (r "1.65.0")))

(define-public crate-mut_family-0.1.7 (c (n "mut_family") (v "0.1.7") (d (list (d (n "sealed") (r "^0.4") (d #t) (k 0)))) (h "1711v69m5lgms123v9hsyp98mzmjkh78c37sb6izd44yl9fvnr0x") (r "1.65.0")))

(define-public crate-mut_family-0.1.8 (c (n "mut_family") (v "0.1.8") (d (list (d (n "sealed") (r "^0.4") (d #t) (k 0)))) (h "092z3acr982j3zszfdjc5mp72cgsamhhj7d7ic68fh6gqqk07jq8") (r "1.65.0")))

(define-public crate-mut_family-0.1.9 (c (n "mut_family") (v "0.1.9") (d (list (d (n "sealed") (r "^0.4") (d #t) (k 0)))) (h "19jmrgqqxwrjjhcmzz83pxk8pd3mxlkbxxbjdbp3fqx5z2n1yhxn") (r "1.65.0")))

(define-public crate-mut_family-0.1.10 (c (n "mut_family") (v "0.1.10") (d (list (d (n "sealed") (r "^0.4") (d #t) (k 0)))) (h "15wr2qcxs92xgpkqr11d63lnahzbizl455crfnzh6ydwpzllx78q") (r "1.65.0")))

(define-public crate-mut_family-0.1.11 (c (n "mut_family") (v "0.1.11") (d (list (d (n "sealed") (r "^0.4") (d #t) (k 0)))) (h "1843nfwpcfqjbg7689b6c418apfn9lrwd0vxldrws6n34awc95s2") (r "1.65.0")))

(define-public crate-mut_family-0.1.12 (c (n "mut_family") (v "0.1.12") (d (list (d (n "sealed") (r "^0.4") (d #t) (k 0)))) (h "0dp3qfcp285ypvblbpgizjw0i50nwfbhrpv6qhr9ii5mmbfjlarv") (r "1.65.0")))

(define-public crate-mut_family-0.1.13 (c (n "mut_family") (v "0.1.13") (d (list (d (n "sealed") (r "^0.4") (d #t) (k 0)))) (h "0nv03l0a358i6af4swr4qd0n4gxh6ip926p5wdlb59kb328pzajb") (r "1.65.0")))

(define-public crate-mut_family-0.1.14 (c (n "mut_family") (v "0.1.14") (d (list (d (n "sealed") (r "^0.4") (d #t) (k 0)))) (h "01ajkfzf6gzpjckggmwb91ljha37j74srpf17hp4a2lvig49idp2") (r "1.65.0")))

