(define-module (crates-io mu ds muds-derive) #:use-module (crates-io))

(define-public crate-muds-derive-0.1.0 (c (n "muds-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ky7gyh77naa1j91ggqb1jyqk8ly9vd5h20d1aqd2gvn9m5k3lz3") (r "1.60")))

(define-public crate-muds-derive-0.1.1 (c (n "muds-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11f36jwhjl8jiipwaddghpz8srw99f17g5pd5h4ibkqw1ayr5dng") (r "1.60")))

