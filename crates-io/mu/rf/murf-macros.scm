(define-module (crates-io mu rf murf-macros) #:use-module (crates-io))

(define-public crate-murf-macros-0.1.0 (c (n "murf-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0yml83sfnzkix9ygycaapkyhpm28lnhc9402zx8dxib72793ab9w") (f (quote (("debug"))))))

