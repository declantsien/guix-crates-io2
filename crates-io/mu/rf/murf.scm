(define-module (crates-io mu rf murf) #:use-module (crates-io))

(define-public crate-murf-0.1.0 (c (n "murf") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "murf-macros") (r "^0.1") (d #t) (k 0)) (d (n "murf-macros") (r "^0.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0mkmr8n7q8xdsabmvi0ljin493qmx9a939xxvzdx2xzgdvzcarci")))

