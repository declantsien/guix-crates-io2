(define-module (crates-io mu ti mutiny) #:use-module (crates-io))

(define-public crate-mutiny-0.1.0 (c (n "mutiny") (v "0.1.0") (d (list (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "psutil") (r "*") (d #t) (k 0)) (d (n "term") (r "*") (d #t) (k 0)))) (h "1kzxm1cyksxwcr3yj2644nk59n90kmqzy5x2fx7h3kq2zkmj011h")))

(define-public crate-mutiny-0.2.0 (c (n "mutiny") (v "0.2.0") (d (list (d (n "docopt") (r "^0.6.39") (d #t) (k 0)) (d (n "docopt_macros") (r "^0.6.39") (d #t) (k 0)) (d (n "log") (r "^0.2.4") (d #t) (k 0)) (d (n "psutil") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "simple_logger") (r "^0.1.0") (d #t) (k 0)))) (h "191kakcbr88nyshcjj9903svyrzx39awjhwjishjaf6hrmrpxb7c")))

