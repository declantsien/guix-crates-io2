(define-module (crates-io mu tu mutual_strip) #:use-module (crates-io))

(define-public crate-mutual_strip-0.1.0 (c (n "mutual_strip") (v "0.1.0") (h "0d7i31vjc51qkh1l3788yg7hgc45l7nvzrj965gq2ni0d5f36nas")))

(define-public crate-mutual_strip-0.1.1 (c (n "mutual_strip") (v "0.1.1") (h "11cpnj5n24prrn5fzp78kif12v4rnrxk8dbwwbckp06yxhmjisna")))

