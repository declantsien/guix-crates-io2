(define-module (crates-io mu tu mutually_exclusive_features) #:use-module (crates-io))

(define-public crate-mutually_exclusive_features-0.0.1 (c (n "mutually_exclusive_features") (v "0.0.1") (h "0ba25qskhidj9iypcndvdyci1jg6qivz2aj7yxr67p43d5yp46pc") (y #t) (r "1.56.0")))

(define-public crate-mutually_exclusive_features-0.0.2 (c (n "mutually_exclusive_features") (v "0.0.2") (h "1aszrpmcqf7znfgmgy2lkvc0j6nh79zmxvwzxviyxfripwm1q3cl") (y #t) (r "1.56.0")))

(define-public crate-mutually_exclusive_features-0.0.3 (c (n "mutually_exclusive_features") (v "0.0.3") (h "03mavdaw06y4pwha3q80vz5mhy3cv09mxn31zjvknxqh0sqc00kd") (r "1.56.0")))

(define-public crate-mutually_exclusive_features-0.1.0 (c (n "mutually_exclusive_features") (v "0.1.0") (h "0xw55kqw8y8y1q5lys9132vm3zlmw8nxz5bkzxrgj56k8mj1wkp9") (r "1.56.0")))

