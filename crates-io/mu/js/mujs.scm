(define-module (crates-io mu js mujs) #:use-module (crates-io))

(define-public crate-mujs-0.0.1 (c (n "mujs") (v "0.0.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0qda16qb7g20acc7ixwq42rlalymc7vbvdiapcxwapq1sdzrai6z") (y #t)))

(define-public crate-mujs-0.0.2 (c (n "mujs") (v "0.0.2") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "062a1lqkm08mjcacjb0h81yli955a5niv08dviy0sfj6xgsgx932")))

(define-public crate-mujs-0.0.3 (c (n "mujs") (v "0.0.3") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0hl1paxqxl83g183s19r765ca8m2grar02x1ba6fj1jdb36nhgil")))

