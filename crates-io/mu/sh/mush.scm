(define-module (crates-io mu sh mush) #:use-module (crates-io))

(define-public crate-mush-0.1.0 (c (n "mush") (v "0.1.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libssh2-sys") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "0lnni9ajnvimfq6smmz1ji4xnfrd9ymsbl4vhz5gxbq1hh4q660j") (y #t)))

(define-public crate-mush-0.1.1 (c (n "mush") (v "0.1.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libssh2-sys") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "0jy79b6gih38d0ji5ffi4f5bqmdccnjdar3qp3dlwi8kys2y57g1") (y #t)))

(define-public crate-mush-0.1.2 (c (n "mush") (v "0.1.2") (h "0q5d92xlqx6fxygsvzq7bbiw4f8yxbiygh7q982yqa0bzq9kh1zp")))

