(define-module (crates-io mu sh mushin) #:use-module (crates-io))

(define-public crate-mushin-0.1.0 (c (n "mushin") (v "0.1.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0ar0j2jdmhnk1z48xx5h6rzxn1c9gh3sgvyggkaykq8apgnql7nj")))

(define-public crate-mushin-0.1.1 (c (n "mushin") (v "0.1.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "12y0asw6bkqm56kp38f03mzras583215sj7j4l3r838ig52bjyid")))

(define-public crate-mushin-0.2.0 (c (n "mushin") (v "0.2.0") (d (list (d (n "arrayfire") (r "^3.8") (d #t) (k 0)))) (h "0pd9gfw8n37szlzdbal3r85fvzcpgwqghzxxwp8fn45vg6cxxjkc")))

(define-public crate-mushin-0.3.0 (c (n "mushin") (v "0.3.0") (d (list (d (n "arrayfire") (r "^3.8") (d #t) (k 0)))) (h "1aqrdrmr29yi5xrdvxixailnkdhfijl0ddj78xd95labl4k9l8ck")))

(define-public crate-mushin-0.4.0 (c (n "mushin") (v "0.4.0") (d (list (d (n "arrayfire") (r "^3.8") (d #t) (k 0)))) (h "04gac1ycg9i57zjshpvcqcndanzlvkry3x208jsc8rwxcvc2z9f5")))

(define-public crate-mushin-0.5.0 (c (n "mushin") (v "0.5.0") (d (list (d (n "arrayfire") (r "^3.8") (d #t) (k 0)))) (h "0jsrn6cc4yy6mch55d1x5nnsahqdhcly0xl2i2a6rfxd5h5cxai0") (f (quote (("nn") ("default" "nn"))))))

