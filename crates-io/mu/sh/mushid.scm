(define-module (crates-io mu sh mushid) #:use-module (crates-io))

(define-public crate-mushid-0.0.1 (c (n "mushid") (v "0.0.1") (d (list (d (n "crc32c") (r "^0.6") (d #t) (k 0)) (d (n "mac_address") (r "^1.1.4") (d #t) (k 0)))) (h "02bhzgwv9zjwxwsk1s0gc8a5xmfxym9jj9ijihk80p875wx8n6ys")))

(define-public crate-mushid-0.0.2 (c (n "mushid") (v "0.0.2") (d (list (d (n "crc32c") (r "^0.6") (d #t) (k 0)) (d (n "mac_address") (r "^1.1.4") (d #t) (k 0)))) (h "1lc449x6fgj0w9wpar0wyrfipw4l74akq0wbbwhldfzn1mh0n2w9")))

