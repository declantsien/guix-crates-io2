(define-module (crates-io mu sh mushin_derive) #:use-module (crates-io))

(define-public crate-mushin_derive-0.1.0 (c (n "mushin_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19qzkbnkl00zjbssb4kjjwaxp038cjx0f4p59qsmnxz2c5w9cb30")))

(define-public crate-mushin_derive-0.1.1 (c (n "mushin_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1c3bnyrann9xm4fjzn7rpkgys3dvw3yrlplh9q380wsb81qqzxpj")))

