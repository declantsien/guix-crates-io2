(define-module (crates-io mu gl mugle_miner_plugin) #:use-module (crates-io))

(define-public crate-mugle_miner_plugin-4.0.0 (c (n "mugle_miner_plugin") (v "4.0.0") (d (list (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0p2k2zpgqskwpf28kr2jfg2a20l1v901vhdgikgyal4277jlmad5")))

