(define-module (crates-io mu gl mugle_miner_util) #:use-module (crates-io))

(define-public crate-mugle_miner_util-4.0.0 (c (n "mugle_miner_util") (v "4.0.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)))) (h "1q78xcz85wx5bjnpqh65p1dnw0kafpv83plgsspi9yvjjm1a01jk")))

