(define-module (crates-io mu lm mulm) #:use-module (crates-io))

(define-public crate-mulm-1.0.0 (c (n "mulm") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fs2") (r "^0.4.2") (d #t) (k 0)) (d (n "maildir") (r "^0.4.2") (d #t) (k 0)) (d (n "mailparse") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1li2a4dl1hfch5xzcc76kf06rnai0j7r4311gkj0vmigpnlhp0i8")))

(define-public crate-mulm-1.0.1 (c (n "mulm") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fs2") (r "^0.4.2") (d #t) (k 0)) (d (n "maildir") (r "^0.4.2") (d #t) (k 0)) (d (n "mailparse") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0gjb4lsx2q68npgbgjh356vkiidzqqgiz0h9n4a4agca65sgx2zd")))

(define-public crate-mulm-1.0.2 (c (n "mulm") (v "1.0.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fs2") (r "^0.4.2") (d #t) (k 0)) (d (n "maildir") (r "^0.4.2") (d #t) (k 0)) (d (n "mailparse") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "07i11yzqahcvlaz25d3xrvwaxzdmybx2gij3ci346rl0d13dk71w")))

(define-public crate-mulm-1.0.3 (c (n "mulm") (v "1.0.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fs2") (r "^0.4.2") (d #t) (k 0)) (d (n "maildir") (r "^0.4.2") (d #t) (k 0)) (d (n "mailparse") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1k9i6bc9w3b0369pxmj2kilz57cjf7dap6kwr8zc65mn8qwm11zv")))

(define-public crate-mulm-1.1.0 (c (n "mulm") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fs2") (r "^0.4.2") (d #t) (k 0)) (d (n "maildir") (r "^0.4.2") (d #t) (k 0)) (d (n "mailparse") (r "^0.12.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "182hll8j6xmcisypaz2flk8y1khnj2f1jl1rlljqwccs5bxi52qf")))

(define-public crate-mulm-1.1.1 (c (n "mulm") (v "1.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fs2") (r "^0.4.2") (d #t) (k 0)) (d (n "maildir") (r "^0.4.2") (d #t) (k 0)) (d (n "mailparse") (r "^0.12.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1zd97nviw8vkhsbk7k2il5igf9zy9girk8h8ny0h4vwd5sm4li69")))

