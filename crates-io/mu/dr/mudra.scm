(define-module (crates-io mu dr mudra) #:use-module (crates-io))

(define-public crate-mudra-0.1.0 (c (n "mudra") (v "0.1.0") (h "1pxb26glgd7b51h25aakyr5r4qyjdjjxy66xim86d2d7kaqz93fh")))

(define-public crate-mudra-0.1.1 (c (n "mudra") (v "0.1.1") (h "0pprisl401ix7zp1869wxal229w7hf25wndyq3dinab7mc4yqfax")))

(define-public crate-mudra-0.1.2 (c (n "mudra") (v "0.1.2") (h "09lri2naag4c62hal263x3iid46wn77azwslrhql2y9rq1nzgjfq")))

(define-public crate-mudra-0.1.3 (c (n "mudra") (v "0.1.3") (h "0lvs14xr8wg5w0p34d6dr189444iwb0k7d4xns9cfjshk8f7a75s")))

(define-public crate-mudra-0.1.4 (c (n "mudra") (v "0.1.4") (h "1jz2kff6i7f9c3llh104z99nd6plsgb9xsbj0w0mpn57crc0bpsd")))

(define-public crate-mudra-0.1.5 (c (n "mudra") (v "0.1.5") (h "1gy8312gaiibzc9q3mbg7n38a12dbdkm1j8argy2fr59qvg9p04p")))

(define-public crate-mudra-0.1.6 (c (n "mudra") (v "0.1.6") (h "1wpmgim94psrcwxya1aclwfs5nmhm2ba4nnnz0axfmvvyqwrrp1s")))

(define-public crate-mudra-0.1.7 (c (n "mudra") (v "0.1.7") (h "1f0fb96lgjqzjf0wzwc77flj0fnkvihqynkcaqwa35z410hs4bd1")))

(define-public crate-mudra-0.1.8 (c (n "mudra") (v "0.1.8") (h "0qjjwi23ijp8gzpfysvian63nxwcd7nhbccj7yqsm2dv1a73yh3f")))

(define-public crate-mudra-0.1.9 (c (n "mudra") (v "0.1.9") (h "0bjdws8ly6imrwk6f1194msbsj53dlhj9f0i245px7pdnwm5syir")))

(define-public crate-mudra-0.1.10 (c (n "mudra") (v "0.1.10") (h "06jm6a73029cm05a958gwymf4g0ixmn0ch72dx9rsnlkbh2lx15w")))

(define-public crate-mudra-0.1.11 (c (n "mudra") (v "0.1.11") (h "0g5vq9vhzxlw8ajjassdfbp3y19b7vvj9zyld04awpf6v770rqwh")))

