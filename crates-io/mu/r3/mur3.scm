(define-module (crates-io mu r3 mur3) #:use-module (crates-io))

(define-public crate-mur3-0.1.0 (c (n "mur3") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "06xvdwybg2f0cb5knwz92smc32ri0dkwmv4hqgj8vdi13sg4ibwp")))

