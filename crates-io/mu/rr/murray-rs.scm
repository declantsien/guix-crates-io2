(define-module (crates-io mu rr murray-rs) #:use-module (crates-io))

(define-public crate-murray-rs-0.1.0 (c (n "murray-rs") (v "0.1.0") (d (list (d (n "httpmock") (r "^0.7.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive" "strum_macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gr86mqac717knn0rxvivgiap20mvl5b6vc2zhvh1gwxs6g8xr26")))

