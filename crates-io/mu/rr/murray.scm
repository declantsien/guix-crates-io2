(define-module (crates-io mu rr murray) #:use-module (crates-io))

(define-public crate-murray-0.1.0 (c (n "murray") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1jz66vcfyf47rj8sd9gkr8pgfi4k6fx3xad7mjln8x5v9hsgdcms")))

(define-public crate-murray-0.1.1 (c (n "murray") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0arldvzcbiljs5c8qddiynnnbvq6lyimkj9178bq2q3zb8ksy20j")))

(define-public crate-murray-0.2.0 (c (n "murray") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1dkampxdi6gbkl30l7gys0dajvygnpyhbin913b3x5kiki7bxl5f")))

(define-public crate-murray-0.3.0 (c (n "murray") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0asykli6fj0jv1hsh30qg759jjknv5bngaxc8a83m6bg0h0m06d6")))

(define-public crate-murray-0.4.0 (c (n "murray") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "03xxya7zhiar5s87p35mshwvzwqbryc4k33xfafvdswchs3kgyjf")))

(define-public crate-murray-0.4.1 (c (n "murray") (v "0.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0wx7hv5nqili1n8i6784pqy56481ij4cydrflw5vcydf50mh5pim")))

(define-public crate-murray-0.4.2 (c (n "murray") (v "0.4.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0jbbsnnnam7j1i7xjdd27w25d6avrs9rnbp6595fd7cmd0kdqjsb")))

