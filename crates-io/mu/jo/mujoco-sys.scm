(define-module (crates-io mu jo mujoco-sys) #:use-module (crates-io))

(define-public crate-mujoco-sys-0.1.0 (c (n "mujoco-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "dirs") (r "^3.0") (d #t) (k 1)))) (h "0g8qc48nbma4lcjiyvky7ann0biq5kdh9wr7hbgdyhkgqj334dq3") (y #t) (l "mujoco200")))

(define-public crate-mujoco-sys-0.1.1 (c (n "mujoco-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "dirs") (r "^3.0") (d #t) (k 1)))) (h "193kzzd2ln75wb8ry4xn089bx7d08558938d0ir31vvxp4zkz0bc") (y #t) (l "mujoco200")))

(define-public crate-mujoco-sys-0.1.2 (c (n "mujoco-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "dirs") (r "^3.0") (d #t) (k 1)) (d (n "dirs") (r "^3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0zaa39h7z1magiqx9f0815q61vf3zl79gdy8z4hmmdx1f0h0mrxc") (f (quote (("mj-render") ("default" "mj-render")))) (y #t) (l "mujoco200")))

(define-public crate-mujoco-sys-0.1.3 (c (n "mujoco-sys") (v "0.1.3") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 1)) (d (n "dirs") (r "^3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0l7r556131nplq2amm81h1xls657wa20jzv3y5gqqdyn7hin3937") (f (quote (("mj-render") ("default" "mj-render")))) (y #t) (l "mujoco200")))

(define-public crate-mujoco-sys-0.1.4 (c (n "mujoco-sys") (v "0.1.4") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 1)) (d (n "dirs") (r "^3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "088kiy8xkakfmvix6vl8c07qj3a9xq6sr557dc715g1p4qr4qpxs") (f (quote (("mj-render") ("default" "mj-render")))) (y #t) (l "mujoco200")))

(define-public crate-mujoco-sys-0.0.0 (c (n "mujoco-sys") (v "0.0.0") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 1)) (d (n "dirs") (r "^3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "15cjbrdr8g6yks9ib26y9dbcq1vxa7xyk59xy22lmbdl0n42kv5g") (f (quote (("mj-render") ("default" "mj-render")))) (l "mujoco200")))

(define-public crate-mujoco-sys-0.0.1 (c (n "mujoco-sys") (v "0.0.1") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 1)) (d (n "dirs") (r "^3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1lgmr86l1vr6vzysr1mv3lvvqq1rmkrj70qwazpqs0cb3yhjp2bg") (f (quote (("mj-render") ("default" "mj-render")))) (l "mujoco200")))

