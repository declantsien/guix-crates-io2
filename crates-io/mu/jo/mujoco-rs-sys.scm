(define-module (crates-io mu jo mujoco-rs-sys) #:use-module (crates-io))

(define-public crate-mujoco-rs-sys-0.0.2 (c (n "mujoco-rs-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "dirs") (r "^3.0") (d #t) (k 1)) (d (n "dirs") (r "^3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1hffcfig7c0zya52m8nhffjjvm8yxh9mfal2np7ibmsa7zsyxg45") (f (quote (("mj-render") ("default" "mj-render")))) (l "mujoco")))

(define-public crate-mujoco-rs-sys-0.0.3 (c (n "mujoco-rs-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "dirs") (r "^4.0") (d #t) (k 1)) (d (n "dirs") (r "^4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1yssm0n49bqfsis0i81faln5nw8kdiqbxwpqvrl4mw0p01b6955j") (f (quote (("mj-render") ("default" "mj-render")))) (l "mujoco")))

(define-public crate-mujoco-rs-sys-0.0.4 (c (n "mujoco-rs-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "dirs") (r "^4.0") (d #t) (k 1)) (d (n "dirs") (r "^4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0dbgkjcm340yb1i26x63p4dfj81sjli63cm98y3j4qang0ihl3g2") (f (quote (("mj-render") ("default" "mj-render")))) (l "mujoco")))

