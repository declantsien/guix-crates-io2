(define-module (crates-io mu jo mujoco-rust) #:use-module (crates-io))

(define-public crate-mujoco-rust-0.0.2 (c (n "mujoco-rust") (v "0.0.2") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mujoco-rs-sys") (r "^0.0.2") (k 0)))) (h "1sh60wni80z3dgj4199c41v08vjb407c6ip3g3rxa35ak73p0qvn") (f (quote (("mj-render" "mujoco-rs-sys/mj-render") ("default" "mj-render"))))))

(define-public crate-mujoco-rust-0.0.3 (c (n "mujoco-rust") (v "0.0.3") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mujoco-rs-sys") (r "^0.0.2") (k 0)))) (h "138snpcyw89c4xlk86wc5h76r4pjklf6x1qw48fwmfkrs5zgdmis") (f (quote (("mj-render" "mujoco-rs-sys/mj-render") ("default" "mj-render"))))))

(define-public crate-mujoco-rust-0.0.4 (c (n "mujoco-rust") (v "0.0.4") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mujoco-rs-sys") (r "^0.0.4") (k 0)) (d (n "nalgebra") (r "^0.31.4") (d #t) (k 0)))) (h "0x4yd4h4dlhycv8f3bvg5z2wp8qc3g063imarkq6rz7z12ps939r") (f (quote (("mj-render" "mujoco-rs-sys/mj-render") ("default" "mj-render"))))))

(define-public crate-mujoco-rust-0.0.6 (c (n "mujoco-rust") (v "0.0.6") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "~5.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mujoco-rs-sys") (r "^0.0.4") (k 0)) (d (n "nalgebra") (r "^0.32.0") (d #t) (k 0)))) (h "0scqgsb08gdgqr7q77x27c3an8bl8qhv4v3xxc8v28103nyzxqvj") (f (quote (("mj-render" "mujoco-rs-sys/mj-render") ("default" "mj-render"))))))

