(define-module (crates-io mu nk munkres) #:use-module (crates-io))

(define-public crate-munkres-0.0.1 (c (n "munkres") (v "0.0.1") (d (list (d (n "bit-vec") (r "*") (d #t) (k 0)))) (h "0fcadcys2p6lkrj8l5qjcji1rwx0g47sgw55mcq4gcbf5qn5vm7s")))

(define-public crate-munkres-0.0.2 (c (n "munkres") (v "0.0.2") (d (list (d (n "bit-vec") (r "*") (d #t) (k 0)))) (h "0i3jz695rivm8i044g838jjghz50c0znfxahkwf0mh9qb2syx4rs")))

(define-public crate-munkres-0.1.0 (c (n "munkres") (v "0.1.0") (d (list (d (n "fixedbitset") (r "^0.1") (d #t) (k 0)))) (h "09dh4n99pc7l0b738a5n7gzg7z88dm9g93hpvlmbfc6vy9k0fhvf")))

(define-public crate-munkres-0.2.0 (c (n "munkres") (v "0.2.0") (d (list (d (n "fixedbitset") (r "^0.1") (d #t) (k 0)))) (h "0ixg4caj5kgnw6a7v3g3z4v072x8knfc9nfni81m8lw81dmxlrm0")))

(define-public crate-munkres-0.3.0 (c (n "munkres") (v "0.3.0") (d (list (d (n "fixedbitset") (r "^0.1") (d #t) (k 0)))) (h "1ry38qdqpxj1lq47vs3zaa0zb3wjz5qz8l3jagi31y5d2cw05jl4")))

(define-public crate-munkres-0.4.0 (c (n "munkres") (v "0.4.0") (d (list (d (n "fixedbitset") (r "^0.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)))) (h "0ivhlihbq7zm0q3v6gmgbgnvbghqa0vilw65aa3wrlxhgccichb7")))

(define-public crate-munkres-0.5.0 (c (n "munkres") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fixedbitset") (r "^0.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)))) (h "17xkvigdqcs04kgpbddx046i0cdqhyi0v1xfrvzxx1a5yspz4a58")))

(define-public crate-munkres-0.5.1 (c (n "munkres") (v "0.5.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fixedbitset") (r "^0.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)))) (h "00sz5r0qll72sdkwicra2qikj58i2nr6hr3jhgmhw241z7silmbx")))

(define-public crate-munkres-0.5.2 (c (n "munkres") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fixedbitset") (r "^0.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (d #t) (k 0)))) (h "1kir2cwsp9s1qm8wrp8h6a67raf40zl88j1nahymkhab94ajbkkl")))

