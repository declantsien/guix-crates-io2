(define-module (crates-io mu ge mugen-air) #:use-module (crates-io))

(define-public crate-mugen-air-0.0.1 (c (n "mugen-air") (v "0.0.1") (d (list (d (n "indoc") (r "^1.0.7") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)))) (h "15pn7f963m4k5a8a27z3dvmp2cw518ll77xmh62kvs0q46yf1fv6")))

