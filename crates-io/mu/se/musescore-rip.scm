(define-module (crates-io mu se musescore-rip) #:use-module (crates-io))

(define-public crate-musescore-rip-0.1.0 (c (n "musescore-rip") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "svg2pdf") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vl14vmyh83h90vhzz60s2ica709ykvzpy48lf3wjhw0abck946v")))

