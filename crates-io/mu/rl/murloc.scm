(define-module (crates-io mu rl murloc) #:use-module (crates-io))

(define-public crate-murloc-0.1.0 (c (n "murloc") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bzz4h8ykg2h04mdx3inzp3apzcgn7h7kac9nwgl0msrwiv6bi1l")))

(define-public crate-murloc-0.1.1 (c (n "murloc") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12b1mqzqzfpjz458cq9i9dibjs1729fw565xn0294yq0m5hzybm1")))

(define-public crate-murloc-0.1.2 (c (n "murloc") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n0y3lggwlvkwa042c09ldrkkiqiv7r0hdmxpkn4ga8a11pjpnyg")))

