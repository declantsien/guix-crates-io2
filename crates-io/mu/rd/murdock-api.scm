(define-module (crates-io mu rd murdock-api) #:use-module (crates-io))

(define-public crate-murdock-api-1.0.0 (c (n "murdock-api") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "04b5v2p90wwzb1qx8j4hzzkbjyr5hcsdmjg14c0qdj3madjknn31")))

