(define-module (crates-io mu tv mutview) #:use-module (crates-io))

(define-public crate-mutview-0.1.0 (c (n "mutview") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0l68ln2f13bx0a814cqxxx4jsjgrqnk08xfgn30w69ybbm7spg4k")))

(define-public crate-mutview-0.1.1 (c (n "mutview") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1j5zfc42c0fyiwymx6byc1c3swjfzh4awqmdl8ar5g8rlgcwyph6")))

(define-public crate-mutview-0.1.2 (c (n "mutview") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xwb0w54yhplrssf5wprsfvy1872ai5n00285dbrz3dq9dn0bv7y")))

