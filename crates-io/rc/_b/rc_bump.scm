(define-module (crates-io rc _b rc_bump) #:use-module (crates-io))

(define-public crate-rc_bump-0.1.0 (c (n "rc_bump") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.14") (f (quote ("collections"))) (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1awid68fjza98sjhvmc0y5ncwgfcdb5wx54ylbs0abl25a8590lr")))

(define-public crate-rc_bump-0.1.1 (c (n "rc_bump") (v "0.1.1") (d (list (d (n "bumpalo") (r "^3.14") (f (quote ("collections"))) (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0xph99kl0ipbvjamndwkf3l9liapmn7wa7rgdqw7ab7kf4kw5gk7")))

(define-public crate-rc_bump-0.1.2 (c (n "rc_bump") (v "0.1.2") (d (list (d (n "bumpalo") (r "^3.14") (f (quote ("collections"))) (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0bckp9zdxgxbs4zjazp4dsk99qrh9ic49afyalsc9jmvlad6ds6c")))

