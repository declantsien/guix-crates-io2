(define-module (crates-io rc u- rcu-clean) #:use-module (crates-io))

(define-public crate-rcu-clean-0.1.0 (c (n "rcu-clean") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1zz8qy89ga88x28zg53qna7y9h1b7c34wz90xybxbppnvbwcm20y")))

(define-public crate-rcu-clean-0.1.1 (c (n "rcu-clean") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "141v63878w24h5rsyi685p0dzqsnnm1gng41g2zqjhiiclygqrh2")))

(define-public crate-rcu-clean-0.1.2 (c (n "rcu-clean") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0ww441in6c0ffx0blqgm7rx6idm7ydca12s9z82ynb622rc8g6h7")))

(define-public crate-rcu-clean-0.1.3 (c (n "rcu-clean") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1mvl66pgc7jlzvcvkzzgxcs39cvizxjv3g0ijfc6jr54yrk74wvj")))

(define-public crate-rcu-clean-0.1.4 (c (n "rcu-clean") (v "0.1.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "045vd3d3f07f8pajhpb1qc34rzxzbm6bs0qilmdmihp9n2vw1b5b")))

(define-public crate-rcu-clean-0.1.5 (c (n "rcu-clean") (v "0.1.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0vnb6axdk5xz5ccdn8bljnyv39ghdzsvldzxb5cspmskv6w0zz8g")))

(define-public crate-rcu-clean-0.1.6 (c (n "rcu-clean") (v "0.1.6") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0p7gcdsc4g8b7wwym9g046wj647khcanhhh7pm76xl7iybd2j1vv")))

(define-public crate-rcu-clean-0.1.7 (c (n "rcu-clean") (v "0.1.7") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "09j6a8skimb3qy85nkgi10m37wfs4fxjj3yhmhisbcx9k041qy14")))

(define-public crate-rcu-clean-0.1.8 (c (n "rcu-clean") (v "0.1.8") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "162mi74mkxxijkfvy9wdd7bm7s8ian1cv7fgyvyd8sp3lzzl27f3")))

