(define-module (crates-io rc hu rchunks) #:use-module (crates-io))

(define-public crate-rchunks-0.1.0 (c (n "rchunks") (v "0.1.0") (h "0ad0f8b3nn193hrakdmk3mx4kkk9rb9n5947br5nrhk86ngzvqmw")))

(define-public crate-rchunks-0.1.1 (c (n "rchunks") (v "0.1.1") (h "1nqhdl5k69z6m13fl48aqg7sqz577q2xj0xk4qxj6a98wb52w0jd")))

(define-public crate-rchunks-0.1.2 (c (n "rchunks") (v "0.1.2") (h "1cdv6sbmacxvfax0mq0yd6k479hi2n6bms229y0z93pcpvaxs453")))

(define-public crate-rchunks-0.1.3 (c (n "rchunks") (v "0.1.3") (h "10rfq2k5rqspsiib1cdp47n0kv443w4qc6f4ijkc0liddf5d59jp")))

(define-public crate-rchunks-0.1.4 (c (n "rchunks") (v "0.1.4") (h "1r8q7rwypf052zhs6xrf80pqwnn4yzdi10yqqvnvy5iqlyay49hc")))

