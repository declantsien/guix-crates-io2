(define-module (crates-io rc at rcatt) #:use-module (crates-io))

(define-public crate-rcatt-0.1.0 (c (n "rcatt") (v "0.1.0") (h "0gw1sxlsndppx0zh0r4simzkyi5znszhdyxqf4mzc7r6m8ijpa5k")))

(define-public crate-rcatt-0.2.0 (c (n "rcatt") (v "0.2.0") (h "0lr7pd9vkxnir431rf51hjbi3rcfc9zz25z9ghjgr5ibpb4a1kz7")))

