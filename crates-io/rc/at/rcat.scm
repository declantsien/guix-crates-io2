(define-module (crates-io rc at rcat) #:use-module (crates-io))

(define-public crate-rcat-2.0.0 (c (n "rcat") (v "2.0.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)))) (h "0ikmiqfm2z4lf4wf7ipy7x5qaslq7ahlflzwcdmvbh0vq7vp5pwa")))

(define-public crate-rcat-2.0.1 (c (n "rcat") (v "2.0.1") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)))) (h "16yc4dn3r13wa5b087qcgy01v3v9k6dk2s09jr7mz1ljgnn56994")))

(define-public crate-rcat-2.0.2 (c (n "rcat") (v "2.0.2") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)))) (h "0vca1b1n9x0hqp958vb8k23v7pmsy4cclkxnr11g9vc9f2i3xw4q")))

(define-public crate-rcat-2.0.3 (c (n "rcat") (v "2.0.3") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)))) (h "0gf84l97z87n8r443klgqw7qsrywpbfgks48ikz4m9x07wv5vs8b")))

(define-public crate-rcat-2.0.4 (c (n "rcat") (v "2.0.4") (d (list (d (n "clap") (r "^2.30") (d #t) (k 0)))) (h "183sdvanafz6mda9yar0yvdhl2sdfw25f6gl1819gq02zxghx5gc")))

