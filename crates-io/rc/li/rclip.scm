(define-module (crates-io rc li rclip) #:use-module (crates-io))

(define-public crate-rclip-0.1.0 (c (n "rclip") (v "0.1.0") (h "0jmqrm0rvqzyqz7vfwbmn1f24wb6nxq6k7b8yw5m5gd7z0df089q")))

(define-public crate-rclip-0.2.0 (c (n "rclip") (v "0.2.0") (h "16rzzpdl87a8hfjcfifaahc0gkpa1a0781q73l9m2i2bcmdwzs78")))

