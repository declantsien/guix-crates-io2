(define-module (crates-io rc li rclio) #:use-module (crates-io))

(define-public crate-rclio-0.0.1 (c (n "rclio") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "rprompt") (r "^2.0") (d #t) (k 0)) (d (n "rtoolbox") (r "^0.0") (d #t) (k 0)))) (h "09k745h4mn7dkswdjfy0c8hkrfdzg1271rqqxx5qidrnrbb6jfjl")))

(define-public crate-rclio-0.0.2 (c (n "rclio") (v "0.0.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "rprompt") (r "^2.0") (d #t) (k 0)) (d (n "rtoolbox") (r "^0.0") (d #t) (k 0)))) (h "02arf5v3qllyfkl3ayhnyx6fw61xf2zjbnij66vmx5mbn88gyyja")))

(define-public crate-rclio-0.0.3 (c (n "rclio") (v "0.0.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "rprompt") (r "^2.0") (d #t) (k 0)) (d (n "rtoolbox") (r "^0.0") (d #t) (k 0)))) (h "1rfqb47n14j8pxgggqlpkz7853jcsrr7zqpch6mgn1riipzyz09p")))

