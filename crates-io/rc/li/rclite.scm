(define-module (crates-io rc li rclite) #:use-module (crates-io))

(define-public crate-rclite-0.1.0 (c (n "rclite") (v "0.1.0") (h "1xfdx5ingw5ssj7d20pa1ws97832cab6mvcmflhdn7isn0h6lcjc") (y #t)))

(define-public crate-rclite-0.1.1 (c (n "rclite") (v "0.1.1") (h "137q3bd49q95f5p2rvmwg6sk6f2sivibgs5lcphplq3d9ka3ll62") (y #t)))

(define-public crate-rclite-0.1.2 (c (n "rclite") (v "0.1.2") (h "06b1h89psbq5y4lkb051alm5j8ah8fj1rchv9ygbq2jld78blghh") (y #t)))

(define-public crate-rclite-0.1.3 (c (n "rclite") (v "0.1.3") (h "1vycl7bfcc6zl4vhfca0lx4xwms8z8kgr4h3w1hwqabd25g4f9v3") (r "1.47")))

(define-public crate-rclite-0.1.4 (c (n "rclite") (v "0.1.4") (h "0kfksvwamb5sb3jm8m1zyi6p94hc0g5d1fs7bhyrmjh0i4y6bwm7") (r "1.47")))

(define-public crate-rclite-0.1.5 (c (n "rclite") (v "0.1.5") (h "13jpjzlz167qxfxm1b67rd936cmypsr2d5awg73qf5mw2h79rrdl") (f (quote (("small")))) (r "1.47")))

(define-public crate-rclite-0.2.0 (c (n "rclite") (v "0.2.0") (h "10sqfrp0lsmjq82xq29gnxvk025mjhykyn4h1ff62qayfy25nhcc") (f (quote (("usize-for-small-platforms") ("default" "usize-for-small-platforms")))) (r "1.47")))

(define-public crate-rclite-0.2.1 (c (n "rclite") (v "0.2.1") (h "0my24yzwnnzh4xk694zyg9fw9fywbnzm87hsr9f9srp9llprxngl") (f (quote (("usize-for-small-platforms") ("default" "usize-for-small-platforms")))) (r "1.47")))

(define-public crate-rclite-0.2.2 (c (n "rclite") (v "0.2.2") (d (list (d (n "branches") (r "^0.1.0") (d #t) (k 0)))) (h "1fjphcjn4ijmimqif4ha3y140d77k06294yn9iqw4ly3siid8cg4") (f (quote (("usize-for-small-platforms") ("default" "usize-for-small-platforms")))) (r "1.47")))

(define-public crate-rclite-0.2.3 (c (n "rclite") (v "0.2.3") (d (list (d (n "branches") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)))) (h "1j1vhnd5wnacb3w86hnrsaijgxi8fld3cxwkrfa03d2pgn8a4s4j") (f (quote (("usize-for-small-platforms") ("default" "usize-for-small-platforms")))) (r "1.47")))

(define-public crate-rclite-0.2.4 (c (n "rclite") (v "0.2.4") (d (list (d (n "branches") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)))) (h "1fa379adiwincgywigi341zd969d3459ljq71n5ymwwficp0r7zf") (f (quote (("usize-for-small-platforms") ("default" "usize-for-small-platforms")))) (r "1.47")))

