(define-module (crates-io rc li rclipd) #:use-module (crates-io))

(define-public crate-rclipd-0.1.0 (c (n "rclipd") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.9") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "xcb") (r "^0.9.0") (f (quote ("xfixes"))) (d #t) (k 0)))) (h "0425jjb339g4vxh36n7a3ga9i3kvflgi937grs9jmc65ndbz9qk0")))

(define-public crate-rclipd-0.1.1 (c (n "rclipd") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.9") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "xcb") (r "^0.9.0") (f (quote ("xfixes"))) (d #t) (k 0)))) (h "1b16v14vv5hpj1vmbdy0j905rk91qvh1z3mn0b088iqkf6a69xc5")))

(define-public crate-rclipd-0.2.0 (c (n "rclipd") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "xcb") (r "^0.10.1") (f (quote ("xfixes"))) (d #t) (k 0)))) (h "1as9fz8nj5kkid5mxkyq502zkghmk9lg9n3xkz4z4ck5363bllzz")))

