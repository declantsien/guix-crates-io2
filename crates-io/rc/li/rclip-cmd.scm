(define-module (crates-io rc li rclip-cmd) #:use-module (crates-io))

(define-public crate-rclip-cmd-0.0.1 (c (n "rclip-cmd") (v "0.0.1") (h "0mz09y8d2gpfhd15ivbp3vjklbj47gkpzjm3zlgdmvbg0ha5r5w0") (y #t)))

(define-public crate-rclip-cmd-0.0.2 (c (n "rclip-cmd") (v "0.0.2") (h "13q5fzxrivv6mwf1wxdirxlzmqm642saxhijwvp2p10d4w1ck5my") (y #t)))

(define-public crate-rclip-cmd-0.0.3 (c (n "rclip-cmd") (v "0.0.3") (h "07rh8lflyhm19i41495l90zma7yahj4af77hskscbpjxm5lp36x7") (y #t)))

(define-public crate-rclip-cmd-0.0.4 (c (n "rclip-cmd") (v "0.0.4") (h "1cahfrvny6i3s4855z4f1nlgfmp5fip4vygwss70krna2pnsb2a1")))

