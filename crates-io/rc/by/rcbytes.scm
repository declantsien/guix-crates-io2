(define-module (crates-io rc by rcbytes) #:use-module (crates-io))

(define-public crate-rcbytes-1.2.1 (c (n "rcbytes") (v "1.2.1") (d (list (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1k143s282vp449dw1zi9hp79d855xxhybjkyb965gnshzf04rr04") (f (quote (("std") ("default" "std"))))))

(define-public crate-rcbytes-1.2.2 (c (n "rcbytes") (v "1.2.2") (d (list (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0lwckxlml08vidd79q7f96dhh77q7hxl52a7dcfjn4k1ncbm1zkd") (f (quote (("std") ("default" "std"))))))

