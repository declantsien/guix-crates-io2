(define-module (crates-io rc #{4o}# rc4ok) #:use-module (crates-io))

(define-public crate-rc4ok-0.1.0 (c (n "rc4ok") (v "0.1.0") (d (list (d (n "criterion") (r "=0.5.1") (d #t) (k 2)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)))) (h "07670vnk1h1yw24sklg1ai9gpigznslipwh0a8m07vbx6nfvwmzf")))

