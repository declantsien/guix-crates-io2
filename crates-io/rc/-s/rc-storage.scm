(define-module (crates-io rc -s rc-storage) #:use-module (crates-io))

(define-public crate-rc-storage-0.1.0 (c (n "rc-storage") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.6.0") (d #t) (k 0)) (d (n "actix-form-data") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (d #t) (k 0)) (d (n "storage-list") (r "^1.0.0") (d #t) (k 0)) (d (n "sys-mount") (r "^1.5.1") (k 0)) (d (n "temp-file") (r "^0.1.7") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "1gc9bsi6p01inp7sdpam9vgwdj64jnkf486i7y5y6ifgndx3p312")))

(define-public crate-rc-storage-0.1.1 (c (n "rc-storage") (v "0.1.1") (d (list (d (n "actix-files") (r "^0.6.0") (d #t) (k 0)) (d (n "actix-form-data") (r "^0.6.2") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.17") (d #t) (k 0)) (d (n "storage-list") (r "^1.0.0") (d #t) (k 0)) (d (n "sys-mount") (r "^1.5.1") (k 0)) (d (n "temp-file") (r "^0.1.7") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "0xscifka103v29sn55w42wsjd1kkbld1fds9x9fh84v1bh8l36ph")))

