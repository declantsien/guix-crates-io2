(define-module (crates-io rc -s rc-slice2) #:use-module (crates-io))

(define-public crate-rc-slice2-0.3.1 (c (n "rc-slice2") (v "0.3.1") (h "0yb37w1knwwslkapn4slpiqva9ks94q3fcbybgmw068q259a0jx5")))

(define-public crate-rc-slice2-0.4.0 (c (n "rc-slice2") (v "0.4.0") (d (list (d (n "smallvec") (r "^1.11.0") (o #t) (d #t) (k 0)))) (h "0jkizq3987k2lj8q7hqv9xszykmij65x3azy5ldix7lfx3c9rqq0") (s 2) (e (quote (("smallvec" "dep:smallvec"))))))

(define-public crate-rc-slice2-0.4.1 (c (n "rc-slice2") (v "0.4.1") (d (list (d (n "smallvec") (r "^1.11.0") (o #t) (d #t) (k 0)))) (h "0s39hrs2rlaqsr4rrkk5avabw5jb848w9ihhdfkggp2y6c9axm4c") (s 2) (e (quote (("smallvec" "dep:smallvec"))))))

