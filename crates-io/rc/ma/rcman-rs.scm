(define-module (crates-io rc ma rcman-rs) #:use-module (crates-io))

(define-public crate-rcman-rs-0.1.0 (c (n "rcman-rs") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1ch4a9zfydnar1gj5mr4bj97hp8l5j630hrsg81yr6pyv9g1j22x") (y #t)))

(define-public crate-rcman-rs-0.1.1 (c (n "rcman-rs") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0q3nkhbwp0ckxg543lkzdvzai6y7grm9amdjpjykljbbavc8qqs2") (y #t)))

(define-public crate-rcman-rs-0.1.2 (c (n "rcman-rs") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "04ynvxvc3aw2fm5xy7wgwklsf3x4k23f3b5ag0hya6n84b7nyf8j") (y #t)))

(define-public crate-rcman-rs-0.1.3 (c (n "rcman-rs") (v "0.1.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0q4bmi5bx1xaf83hpnpf0vmkjlw5yvk2a0j6ibixhjw33p89yw8r") (y #t)))

