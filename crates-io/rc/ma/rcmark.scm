(define-module (crates-io rc ma rcmark) #:use-module (crates-io))

(define-public crate-rcmark-0.1.0 (c (n "rcmark") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libcmark-sys") (r "^0.1") (d #t) (k 0)))) (h "0kvgyfbr2arrwlrkfw41ys997gxr8yryr8x8p6q3ncdvm83vs0b8")))

