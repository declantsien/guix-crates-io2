(define-module (crates-io rc sh rcshell) #:use-module (crates-io))

(define-public crate-rcshell-0.0.1-alpha.1 (c (n "rcshell") (v "0.0.1-alpha.1") (d (list (d (n "anyhow") (r "^1.0.79") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "built") (r "^0.7.1") (f (quote ("git2" "chrono"))) (d #t) (k 1)) (d (n "clap") (r "^4.4") (f (quote ("env" "unicode"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (f (quote ("termios"))) (d #t) (k 0)))) (h "1m0w5m46hws1fqhdvp3q5pnshjvbqxdajk9msb9pyfw2c5shc209")))

(define-public crate-rcshell-0.0.1-alpha.2 (c (n "rcshell") (v "0.0.1-alpha.2") (d (list (d (n "anyhow") (r "^1.0.79") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "built") (r "^0.7.1") (f (quote ("git2" "chrono"))) (d #t) (k 1)) (d (n "clap") (r "^4.4") (f (quote ("env" "unicode"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (f (quote ("termios"))) (d #t) (k 0)))) (h "03rsr1sf65xzy7yq04y2mx352qspff5j86fdig461mrrs4knrlwd")))

(define-public crate-rcshell-0.0.1-alpha.3 (c (n "rcshell") (v "0.0.1-alpha.3") (d (list (d (n "anyhow") (r "^1.0.79") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "built") (r "^0.7.1") (f (quote ("git2" "chrono"))) (d #t) (k 1)) (d (n "clap") (r "^4.4") (f (quote ("env" "unicode"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.10") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (f (quote ("termios"))) (d #t) (k 0)))) (h "19asyh424vkfbqp7wfn8akki2ajkfyfmkay6rwjf7lksvlmik9p3")))

