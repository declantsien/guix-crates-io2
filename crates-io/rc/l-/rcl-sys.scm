(define-module (crates-io rc l- rcl-sys) #:use-module (crates-io))

(define-public crate-rcl-sys-0.0.0 (c (n "rcl-sys") (v "0.0.0") (h "1g9zgcql606a270x9g8lsjc7zj1nhnjxwj0pgi2zf6qrjlmxj5sm")))

(define-public crate-rcl-sys-0.0.2 (c (n "rcl-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "15kkgdr2fihafwf4h80gf0wjhnb0p0ch3y35vx73zg5lnpj476dh")))

