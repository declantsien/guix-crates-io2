(define-module (crates-io rc ra rcrawl) #:use-module (crates-io))

(define-public crate-rcrawl-1.0.0 (c (n "rcrawl") (v "1.0.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "0g0073kfj38zf1p6dr6p1cqv06vlpzm5nm6f5hqpyk8imcdyr68d")))

(define-public crate-rcrawl-1.0.1 (c (n "rcrawl") (v "1.0.1") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "1ngyyx06ybwwf18wpl1mn1vj2jdfpap7jnrsa0a5j7zzagxjnfxr")))

(define-public crate-rcrawl-1.0.2 (c (n "rcrawl") (v "1.0.2") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "1ijqcad9zg5863hgb8p6xg4b362y463icfck10v0xwnlvkr2pb0w")))

(define-public crate-rcrawl-1.1.0 (c (n "rcrawl") (v "1.1.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "13spzbvp7ff66pl8ynmkc3zigfl905v9b4jgifnmjpr9b1m7xdr6")))

(define-public crate-rcrawl-1.1.1 (c (n "rcrawl") (v "1.1.1") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "1m25wkwgi47avyzbfg9c8gin5s4k86va9jpnhpi1w89x495xnc8s")))

(define-public crate-rcrawl-1.1.2 (c (n "rcrawl") (v "1.1.2") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "19dk847hva7yy39kgsiqzzzmip43gdp7wfaq36wbm8jv3gps9flv")))

(define-public crate-rcrawl-1.1.3 (c (n "rcrawl") (v "1.1.3") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "1w6gm8q48ggscq62n08ifbcf18bsxxsgdsc0hz8vniklivc1dkk9")))

(define-public crate-rcrawl-1.1.4 (c (n "rcrawl") (v "1.1.4") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "0mnqwr6378fa0ls995wc76p1vwfi6l8cjyids922ikccafxbn2bd")))

(define-public crate-rcrawl-1.1.5 (c (n "rcrawl") (v "1.1.5") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "1wwgjhiiq7b9zzhksd0vc6xrq2573dki73dy0pnd35bq98r0sasv")))

(define-public crate-rcrawl-1.1.6 (c (n "rcrawl") (v "1.1.6") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)))) (h "0nljd7c4fi7syjh4kw46r1sbdff7ynh670bh0wa60bbxwwqb0mah")))

(define-public crate-rcrawl-1.1.7 (c (n "rcrawl") (v "1.1.7") (d (list (d (n "clap") (r ">=2.33.0, <2.34.0") (d #t) (k 0)))) (h "1q23ngl95g7f39rkb9h11fz5n6bm769pshk9bl9lf4wpiycm9zf4")))

(define-public crate-rcrawl-1.2.0 (c (n "rcrawl") (v "1.2.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1hw0z1cq45fm7pr1mgvf0xg1ipyxks25sjgf58f8ixdwh2v2y96i")))

(define-public crate-rcrawl-1.2.1 (c (n "rcrawl") (v "1.2.1") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "150rzbb1dz940gawbd413p8m88wmbyfqykr35apb07lirgqafmhy")))

