(define-module (crates-io rc md rcmd-core) #:use-module (crates-io))

(define-public crate-rcmd-core-0.1.0 (c (n "rcmd-core") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)) (d (n "zip-extensions") (r "^0.6") (d #t) (k 0)))) (h "0hccmcx9cwrv321l9g90936qmg575rv4zz32fc7jccrfi85zmjm0")))

