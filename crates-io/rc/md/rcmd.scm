(define-module (crates-io rc md rcmd) #:use-module (crates-io))

(define-public crate-rcmd-0.1.0 (c (n "rcmd") (v "0.1.0") (d (list (d (n "err-derive") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1fz8i1f2igigphs823vxgirm1lshmi5rbjic4ym2c5az2pgi1lik")))

