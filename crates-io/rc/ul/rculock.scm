(define-module (crates-io rc ul rculock) #:use-module (crates-io))

(define-public crate-rculock-0.1.0 (c (n "rculock") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)))) (h "0q0w7lvsnpwpncikikslylf58gmmmfrwv23xl988ki7551jm2adh") (y #t)))

(define-public crate-rculock-0.1.1 (c (n "rculock") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)))) (h "15z3skqvj30zbhzmkkk8wildqf232l7vx9hsvjnnx4jp9q6bgqg2") (y #t)))

(define-public crate-rculock-0.1.2 (c (n "rculock") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)))) (h "1wghwyr0jca3dcf438mrnk83j2j36jpraplav0jxbm2qlblqmgy7")))

