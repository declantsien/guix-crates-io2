(define-module (crates-io rc lr rclrust-msg-core) #:use-module (crates-io))

(define-public crate-rclrust-msg-core-0.0.1 (c (n "rclrust-msg-core") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0cmm0dsrr95rg2nx54mxgna80cvjc5vrl5l8mr4kgshad6jzykka") (y #t)))

