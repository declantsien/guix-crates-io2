(define-module (crates-io rc lr rclrust) #:use-module (crates-io))

(define-public crate-rclrust-0.0.0 (c (n "rclrust") (v "0.0.0") (h "1jiiyfpldswmy2jcsyyc0gnd4k302h1zr5y9sbmg1kfy37q8f13v")))

(define-public crate-rclrust-0.0.2 (c (n "rclrust") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rcl-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "rclrust-msg") (r "^0.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1w7jnvq60bmkpgr8bq2kmsfm7y3cl0lgzdv59h8ygapr9s11fm3p")))

