(define-module (crates-io rc lr rclrs) #:use-module (crates-io))

(define-public crate-rclrs-0.1.0 (c (n "rclrs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 1)))) (h "1k1snvz9w1pc5f1y3dcrnbjd3smyyw9ibpkrf34d9sm5hw0f810l")))

(define-public crate-rclrs-0.2.0 (c (n "rclrs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "rosidl_runtime_rs") (r "^0.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1wnvpdpdn1dzpa0clxdw49bd4r9zc5q63xb6p96362y7ag4xadbq")))

(define-public crate-rclrs-0.3.0 (c (n "rclrs") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "rosidl_runtime_rs") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1sw9amjvaq2w142lwbcwnkjxwxxd2kdi6rdycn3is6b5iia3x5di") (r "1.63")))

(define-public crate-rclrs-0.3.1 (c (n "rclrs") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "rosidl_runtime_rs") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "17lyl9m08r9gy74qb1ykfs7mx30aqsqg066h5y7d54ggn5ymz9wl") (r "1.63")))

(define-public crate-rclrs-0.4.0 (c (n "rclrs") (v "0.4.0") (d (list (d (n "ament_rs") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rosidl_runtime_rs") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0wzx5ywx46mnaypfiiazybdas0fdn8c5ckw91a2cl906d0pgj49f") (f (quote (("dyn_msg" "ament_rs" "libloading")))) (r "1.63")))

(define-public crate-rclrs-0.4.1 (c (n "rclrs") (v "0.4.1") (d (list (d (n "ament_rs") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rosidl_runtime_rs") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0aavp21fqxwkxjkcpf50w9j2lxn5c3wvwpmzgfh8r9wi7i5cf29z") (f (quote (("generate_docs" "rosidl_runtime_rs/generate_docs") ("dyn_msg" "ament_rs" "libloading") ("default")))) (r "1.63")))

