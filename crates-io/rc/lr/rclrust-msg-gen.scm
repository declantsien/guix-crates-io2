(define-module (crates-io rc lr rclrust-msg-gen) #:use-module (crates-io))

(define-public crate-rclrust-msg-gen-0.0.2 (c (n "rclrust-msg-gen") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15jhzzr29w6msxav9xahqskxnchqybcbyiizp4rvzi69yb9pfss5")))

