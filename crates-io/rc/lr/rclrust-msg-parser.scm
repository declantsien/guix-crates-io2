(define-module (crates-io rc lr rclrust-msg-parser) #:use-module (crates-io))

(define-public crate-rclrust-msg-parser-0.0.1 (c (n "rclrust-msg-parser") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1") (d #t) (k 0)) (d (n "rclrust-msg-types") (r "^0.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10zpr4ins0ycky1lcilscm4qjaizlybx28yr4m2w2nh1vvbxn4nz") (y #t)))

