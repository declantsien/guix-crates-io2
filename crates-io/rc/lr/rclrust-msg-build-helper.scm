(define-module (crates-io rc lr rclrust-msg-build-helper) #:use-module (crates-io))

(define-public crate-rclrust-msg-build-helper-0.0.1 (c (n "rclrust-msg-build-helper") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rclrust-msg-parser") (r "^0.0.1") (d #t) (k 0)) (d (n "rclrust-msg-types") (r "^0.0.1") (d #t) (k 0)))) (h "072mnammkfjra2igkchfhzlqv5kmw054icky8xhfs3qi7sd46gxm") (y #t)))

