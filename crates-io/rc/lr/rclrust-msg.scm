(define-module (crates-io rc lr rclrust-msg) #:use-module (crates-io))

(define-public crate-rclrust-msg-0.0.1 (c (n "rclrust-msg") (v "0.0.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "rclrust-msg-build-helper") (r "^0.0.1") (d #t) (k 1)) (d (n "rclrust-msg-core") (r "^0.0.1") (d #t) (k 0)) (d (n "rclrust-msg-parser") (r "^0.0.1") (d #t) (k 1)) (d (n "rclrust-msg-types") (r "^0.0.1") (d #t) (k 1)) (d (n "sailfish") (r "^0.3") (d #t) (k 1)))) (h "02vp1s6hz0a5q52pzaklwgxp13ncxs0qxvb1nmkxmiv0i4x127sx")))

(define-public crate-rclrust-msg-0.0.2 (c (n "rclrust-msg") (v "0.0.2") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "rclrust-msg-gen") (r "^0.0.2") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0mwcbip4wq7ykq0h3l7bl8644wfkirhhchccipdm28a370lgb2va")))

