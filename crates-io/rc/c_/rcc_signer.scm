(define-module (crates-io rc c_ rcc_signer) #:use-module (crates-io))

(define-public crate-rcc_signer-0.1.0 (c (n "rcc_signer") (v "0.1.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.1") (d #t) (k 0)) (d (n "k256") (r "^0.11.3") (f (quote ("ecdsa" "keccak256"))) (d #t) (k 0)) (d (n "serialport") (r "^4.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "1b8j0iwmjyhaamv81zdk5wwrj0szkx1dgwj37ilg5n7ywiz7k0ap")))

(define-public crate-rcc_signer-0.1.1 (c (n "rcc_signer") (v "0.1.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.1") (d #t) (k 0)) (d (n "k256") (r "^0.11.3") (f (quote ("ecdsa" "keccak256"))) (d #t) (k 0)) (d (n "serialport") (r "^4.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "1kvh9mm6rday3p69f00cswwrynaqdj7vv639c2plzbv64wlql5m7")))

