(define-module (crates-io rc lo rclog) #:use-module (crates-io))

(define-public crate-rclog-0.1.1-alpha.1 (c (n "rclog") (v "0.1.1-alpha.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.20") (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "1pggwj6aabg19xhkk9a6byhx0pqx3q10whdixxyvvkff8jxiqfmk")))

(define-public crate-rclog-0.1.1 (c (n "rclog") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.20") (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "06abwcry80r46hgn3vglrvbri8valyjqpxb13ljdwgdfh7ssdcrm")))

(define-public crate-rclog-0.1.1-alpha.2 (c (n "rclog") (v "0.1.1-alpha.2") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.20") (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "1jc01a5nzs4q126frbcbd4242wzzlikhzr8b0f5fdmz92h0x9jq3")))

(define-public crate-rclog-0.1.1-beta.1 (c (n "rclog") (v "0.1.1-beta.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.20") (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "1cxbn5zhw6wxnzrgp05g66n9lxrfwdxzig8zrwwy3j9a8i2vldk3")))

(define-public crate-rclog-0.1.2-alpha.1 (c (n "rclog") (v "0.1.2-alpha.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.20") (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "1b38zf3h14sbq0dpf9dy8w1pjkb3p06hf289yn5mi1qb209p2wb2")))

(define-public crate-rclog-0.1.2-beta.1 (c (n "rclog") (v "0.1.2-beta.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.20") (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "04s675cw2x9yrcnacj9kvvf64dacbbrn2b2svriihzzsg7rwkdcs")))

(define-public crate-rclog-0.1.2 (c (n "rclog") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.20") (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "0n11krb3151adypw07hwxpgqnv5j2bls7hsbmjf3anl3ppnzgxnx")))

(define-public crate-rclog-0.1.3-alpha.1 (c (n "rclog") (v "0.1.3-alpha.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.20") (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "04sizji0rbfr212584frvc7lpy3r0sv9a5x89zn1y8ymwng7cvwd")))

(define-public crate-rclog-0.1.3 (c (n "rclog") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.20") (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "0knahgwqyra05q2wzqdj1bbscja3nqwwz27lc3hjq6c6pbp0vs7n")))

