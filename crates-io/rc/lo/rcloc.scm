(define-module (crates-io rc lo rcloc) #:use-module (crates-io))

(define-public crate-rcloc-0.6.2 (c (n "rcloc") (v "0.6.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0khl2g0vdfd8c8ss81xgw29w0b5zd9imi4igmrbshymsvscak8f7")))

