(define-module (crates-io rc lo rclone) #:use-module (crates-io))

(define-public crate-rclone-0.1.1 (c (n "rclone") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "19z1b8nlm2w4d0yj8liprhd6n5vmkjdgplvwqlj306jc0pwi0lak")))

