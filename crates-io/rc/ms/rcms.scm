(define-module (crates-io rc ms rcms) #:use-module (crates-io))

(define-public crate-rcms-0.1.0 (c (n "rcms") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "17313wpi6ngdkv7cs9a7nm3gjmnlxp4ys5spdy9ij1afalkyws2h")))

