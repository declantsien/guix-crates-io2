(define-module (crates-io rc sv rcsv) #:use-module (crates-io))

(define-public crate-rcsv-0.1.0 (c (n "rcsv") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt" "fileapi" "handleapi" "memoryapi" "std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0629x55bnv8hb1l0g5gq7pv3bjjsfxs6wg4msdf4b56wmngx0yqz")))

