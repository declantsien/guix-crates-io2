(define-module (crates-io rc nb rcnb-rs) #:use-module (crates-io))

(define-public crate-rcnb-rs-0.1.0 (c (n "rcnb-rs") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 2)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1d08zy1nnfq2wmsjlxf0i8xwssj5qvhsw4crzwqchn29aacyi7b4")))

