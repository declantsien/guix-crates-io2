(define-module (crates-io rc a9 rca9539) #:use-module (crates-io))

(define-public crate-rca9539-0.1.0 (c (n "rca9539") (v "0.1.0") (d (list (d (n "bitmaps") (r "^3.1.0") (k 0)) (d (n "cortex-m") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)) (d (n "spin") (r "^0.9.2") (o #t) (d #t) (k 0)))) (h "1ifaq97nai6rfdnk0wds1n28s9nl2f91w5j09spq0h6lzval8r6y") (f (quote (("example") ("default" "example")))) (y #t)))

(define-public crate-rca9539-0.1.1 (c (n "rca9539") (v "0.1.1") (d (list (d (n "bitmaps") (r "^3.1.0") (k 0)) (d (n "cortex-m") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)) (d (n "spin") (r "^0.9.2") (o #t) (d #t) (k 0)))) (h "16lqpxxlzk9sfvgnmnkm1swl8nqr307j42sp6fkpcjhv3sdl8yki") (f (quote (("example") ("default" "example")))) (y #t)))

