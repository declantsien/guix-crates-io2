(define-module (crates-io rc re rcrefcell) #:use-module (crates-io))

(define-public crate-rcrefcell-1.0.0 (c (n "rcrefcell") (v "1.0.0") (h "0i2y2hw81zk78znpy0635y26z9dhswh3ywyhfkxdsp2hbqb6c69n")))

(define-public crate-rcrefcell-1.0.1 (c (n "rcrefcell") (v "1.0.1") (h "1xvmj3kxagpzkl4z3alljncj3cbdv2mpchdwwpg9lkx714db9pns")))

(define-public crate-rcrefcell-1.0.2 (c (n "rcrefcell") (v "1.0.2") (h "0mdn8fk2k123ymnm1xd19nmbbaawbzjm7874xf21k51fha4g34nc")))

(define-public crate-rcrefcell-1.0.3 (c (n "rcrefcell") (v "1.0.3") (h "1dk15ywjxh0pr5l66x3mmdslvfijmac1v925l4zpia29q009znia")))

(define-public crate-rcrefcell-1.0.4 (c (n "rcrefcell") (v "1.0.4") (h "0y8cvwgv01vhdfpi6pv5g4r8c74pil9dcbxv7qqd0p8g3hg266kk")))

(define-public crate-rcrefcell-1.0.5 (c (n "rcrefcell") (v "1.0.5") (h "04h4nc30w0zknl424xvp5ki46a633mav0d82khkfdjx47ndlcgib")))

(define-public crate-rcrefcell-1.0.6 (c (n "rcrefcell") (v "1.0.6") (h "0qcq0528mskjglr4w4xiyhwh951zslw5mv5695pq49gfyg5dad0q")))

(define-public crate-rcrefcell-1.0.7 (c (n "rcrefcell") (v "1.0.7") (h "0zwkv8pcw86wp41w620hmigbqcwmng47q69k9fz5rihk96iz0dzx")))

