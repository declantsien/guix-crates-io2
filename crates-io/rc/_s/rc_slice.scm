(define-module (crates-io rc _s rc_slice) #:use-module (crates-io))

(define-public crate-rc_slice-0.1.0 (c (n "rc_slice") (v "0.1.0") (h "0ckm5sh2gl9l0c22nskkia42br26wir6r4158vhflr6ag1rjwdyn") (y #t)))

(define-public crate-rc_slice-0.2.0 (c (n "rc_slice") (v "0.2.0") (h "0js6jrx66y5iv6znzs9qhv7jh3r0j33xr1gc0h9bf8zs8915wail") (y #t)))

(define-public crate-rc_slice-0.3.0 (c (n "rc_slice") (v "0.3.0") (h "10n33wb45vaqx3kyjlszc4y8x2ivplc6v1nwzfx4xhsgra27w7xz") (y #t)))

(define-public crate-rc_slice-0.4.0 (c (n "rc_slice") (v "0.4.0") (h "1cflgdqr9zf9iph89yb1q18617l09jwavzmi2f8mxbspfa15a7ds") (y #t)))

