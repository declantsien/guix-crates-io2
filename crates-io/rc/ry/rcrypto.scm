(define-module (crates-io rc ry rcrypto) #:use-module (crates-io))

(define-public crate-rcrypto-0.1.0 (c (n "rcrypto") (v "0.1.0") (h "1iqwgdnm1vvcyky2ml0yza0v1457i7ws34v0z4v49bnwnc554gp6")))

(define-public crate-rcrypto-0.1.1 (c (n "rcrypto") (v "0.1.1") (d (list (d (n "rmath") (r "^0.1.1") (d #t) (k 0)))) (h "1jbi77kaxxgxb4yhlg3wlj6p2v1iz0nmpgxil4yfyhyqzfrinfmp")))

(define-public crate-rcrypto-0.1.2 (c (n "rcrypto") (v "0.1.2") (d (list (d (n "rmath") (r "^0.1.4") (d #t) (k 0)))) (h "039afnjhg0rlhk3hilwipcjfw6g56mr22dh24kagpigs56pvhxxk")))

(define-public crate-rcrypto-0.2.0 (c (n "rcrypto") (v "0.2.0") (d (list (d (n "rmath") (r "^0.1.5") (d #t) (k 0)))) (h "1mjkf59gbv04397rhywbn4jn4n0m3dj1dyyr9v8wayvj2cj7z6w6")))

