(define-module (crates-io rc ry rcrypt) #:use-module (crates-io))

(define-public crate-rcrypt-0.1.0 (c (n "rcrypt") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bcrypt") (r "^0.10.1") (d #t) (k 0)))) (h "0763c1vhrd73vm30kmw92s7h8d9b2lxhi8wdvmvm4vlzzwsjfmrz")))

(define-public crate-rcrypt-0.1.1 (c (n "rcrypt") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bcrypt") (r "^0.10.1") (d #t) (k 0)))) (h "042fhiqd70dn7f4sn9iacjiknvrhza9n4scg1pw5yfmmyz4p0b58")))

(define-public crate-rcrypt-0.2.0 (c (n "rcrypt") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bcrypt") (r "^0.10.1") (d #t) (k 0)))) (h "0zpy75zp0h4wckf5f2npj3xdj2ay458qdyfil0kfind3wfjics2d")))

(define-public crate-rcrypt-0.3.0 (c (n "rcrypt") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bcrypt") (r "^0.10.1") (d #t) (k 0)))) (h "12li1swdd3s9i75xdr5rx8nhrr3jqww36vg560qhka8ylrwa4gxk")))

(define-public crate-rcrypt-0.4.0-alpha.1 (c (n "rcrypt") (v "0.4.0-alpha.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "blowfish") (r "^0.9") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)))) (h "0zlkp1gymkij4l8znxfmffnjvwjzdalgazkd71sq4gycbrz221jn")))

(define-public crate-rcrypt-0.4.0 (c (n "rcrypt") (v "0.4.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "blowfish") (r "^0.9") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)))) (h "07ifdm6kl16zj5wr1v6bi2w4vqd7zkk4c122yb33qlhb5yzw6w4w")))

