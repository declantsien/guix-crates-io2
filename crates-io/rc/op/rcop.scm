(define-module (crates-io rc op rcop) #:use-module (crates-io))

(define-public crate-rcop-0.1.0 (c (n "rcop") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prettytable") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0jkw8fpfh3l5sykhhb1sbhrp57r1i16s5kj9h53pm8m070cs50mr")))

(define-public crate-rcop-0.1.2 (c (n "rcop") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prettytable") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0675h8mfy07l77pd1kqziid15imslk4621vlg76i5j9z382wvavf")))

(define-public crate-rcop-0.1.3 (c (n "rcop") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prettytable") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0sjc6q6x8ddv376v0pskxdpfq1rjm6cz7hqs4rnj47vks6d2ywvk")))

