(define-module (crates-io rc om rcom) #:use-module (crates-io))

(define-public crate-rcom-0.1.0 (c (n "rcom") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.25") (d #t) (k 0)) (d (n "clap") (r "^2.19.3") (d #t) (k 0)) (d (n "mio") (r "^0.6.2") (d #t) (k 0)) (d (n "serial") (r "^0.3.4") (d #t) (k 0)) (d (n "termios") (r "^0.2.2") (d #t) (k 0)))) (h "0a798jalqn447f8iim3xyr2qz0g86rhrs0p8cm4mjp8sclvvz7s5")))

(define-public crate-rcom-0.1.1 (c (n "rcom") (v "0.1.1") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "serial") (r "^0.3") (d #t) (k 0)) (d (n "termios") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0sv4awlfq91zqmxq3gycggnwpyawp8i5g3a24fd728cxn2jix4c3")))

(define-public crate-rcom-0.2.0 (c (n "rcom") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "mio") (r "^0.6.21") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0p3d59wfp7697cii0aiwbabkaxnx8zz8ifxj9cw1qy4rbxcx1rh5")))

