(define-module (crates-io rc om rcomplex) #:use-module (crates-io))

(define-public crate-rcomplex-0.1.0 (c (n "rcomplex") (v "0.1.0") (h "0r41qqvlfhqx1c03khr3n654ab7qxz8wdp1mwvbh3s7snc5hd8fn")))

(define-public crate-rcomplex-0.1.1 (c (n "rcomplex") (v "0.1.1") (h "1v73g8fcmqn1k08y2jgixawh9ml8mp5jh6dvkz630hkrrd6zhycl")))

(define-public crate-rcomplex-0.1.2 (c (n "rcomplex") (v "0.1.2") (h "0c867n8f2hj78lkwax2b8i9scrjl3r9fq3yy5isqdmvhgzymb1gz")))

