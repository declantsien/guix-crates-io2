(define-module (crates-io rc om rcommunity_macros) #:use-module (crates-io))

(define-public crate-rcommunity_macros-0.0.1 (c (n "rcommunity_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "05a76nnnhfmhpip3sw6ja4084xslkhqzmqfbjmzpw86y1mkr0h06")))

(define-public crate-rcommunity_macros-0.0.2 (c (n "rcommunity_macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (d #t) (k 0)))) (h "0inzp3mp5088vz4j0cwdszifhnnzi4f7hcr75rrcgf7nrwi4fggq")))

