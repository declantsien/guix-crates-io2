(define-module (crates-io rc om rcommunity_core) #:use-module (crates-io))

(define-public crate-rcommunity_core-0.0.1 (c (n "rcommunity_core") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1shw0jcysihkqjm4mrab9s7bjzyb290xw6358xxq76mzhjvk7wlz")))

(define-public crate-rcommunity_core-0.0.2 (c (n "rcommunity_core") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.19.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "01zs2ij9djycwcb170dha2iv5yh0gf07qriiy7r6qxraxz9af3kz")))

(define-public crate-rcommunity_core-0.0.3 (c (n "rcommunity_core") (v "0.0.3") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.19.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jdf45ywq6dbzxh09hba6kdf1kcx3rv5vas0ys99xs3xsn73zgcn")))

