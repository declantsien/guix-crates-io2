(define-module (crates-io rc ud rcudnn) #:use-module (crates-io))

(define-public crate-rcudnn-1.5.0 (c (n "rcudnn") (v "1.5.0") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "coaster") (r "^0.1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "rcudnn-sys") (r "^0.1.0") (d #t) (k 0)))) (h "00ksp4kkxvg6m2h6433fz3db9nq3jia0avlaa6h1hpdrcq5q0mdm") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-rcudnn-1.6.0 (c (n "rcudnn") (v "1.6.0") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "coaster") (r "^0.1") (f (quote ("native" "cuda"))) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "rcudnn-sys") (r "^0.2.0") (d #t) (k 0)))) (h "04pvx7xqw3sqx7x2wacin4gzg0qf5kv4vykh1a1sx5k43x8mj636") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-rcudnn-1.7.0 (c (n "rcudnn") (v "1.7.0") (d (list (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "coaster") (r "^0.1") (f (quote ("native" "cuda"))) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rcudnn-sys") (r "^0.4") (d #t) (k 0)))) (h "1s94v1hzga54m2ypdldhrigp9lqhm3qa3x7kmv1pgy52pi870a0q") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-rcudnn-1.8.0 (c (n "rcudnn") (v "1.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rcudnn-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1b06qrhfgq18xmk5ckjkgrnadb084bl4qvn8f9vhca3jma78nv6z")))

