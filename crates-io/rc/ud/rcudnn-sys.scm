(define-module (crates-io rc ud rcudnn-sys) #:use-module (crates-io))

(define-public crate-rcudnn-sys-0.1.0 (c (n "rcudnn-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.25.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0bfbhbbj8xi7q90f0f1sxxxf7g8dbjahmjbyf6w8593m2574wsy1")))

(define-public crate-rcudnn-sys-0.2.0 (c (n "rcudnn-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1l2rrry51ib0wygamh9w23qdc2p5hf59594vj7vpz5am6xrvx6il")))

(define-public crate-rcudnn-sys-0.4.0 (c (n "rcudnn-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1xni1zjrrirkpjjkvn9ag85yrk6r2pk374k8vhfxvmxnc8d69kzn") (f (quote (("generate" "bindgen") ("default" "generate")))) (l "cudnn")))

(define-public crate-rcudnn-sys-0.5.0 (c (n "rcudnn-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0aqk2q57bfw4yspdsmixf3jzrgaynq4jzyvc1sabxybx9sw5wmn2") (f (quote (("generate" "bindgen") ("default")))) (l "cudnn")))

