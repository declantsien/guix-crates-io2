(define-module (crates-io rc s- rcs-module) #:use-module (crates-io))

(define-public crate-RCS-Module-0.1.0 (c (n "RCS-Module") (v "0.1.0") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "time") (r "^0.1.12") (d #t) (k 0)))) (h "122h9g8nxjai4mxzss812rs44438sayk8bza7vr8zy6l31sy41nk") (r "1.56")))

