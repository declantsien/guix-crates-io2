(define-module (crates-io rc on rcon) #:use-module (crates-io))

(define-public crate-rcon-0.0.2 (c (n "rcon") (v "0.0.2") (d (list (d (n "podio") (r "^0") (d #t) (k 0)))) (h "0ijdpns84hdclfr16g0wlpsv45iyc4fd4gmr47vyk914svnjw02d")))

(define-public crate-rcon-0.0.3 (c (n "rcon") (v "0.0.3") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "podio") (r "^0.1") (d #t) (k 0)))) (h "084376awk38znnbaxzkic3y8bk3574my3p7n90llhq8nalfgfgga")))

(define-public crate-rcon-0.0.4 (c (n "rcon") (v "0.0.4") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "podio") (r "^0.1") (d #t) (k 0)))) (h "0xlqcvqlk6mhb06dmrv4nq3mp0c37pnspc6i5px5lq3kwlzwbq3v")))

(define-public crate-rcon-0.0.5 (c (n "rcon") (v "0.0.5") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "podio") (r "^0.1") (d #t) (k 0)))) (h "11fqqqnrz6s841abvyl6cb4x32f13l7adcyicwkslz6lhkcg34aa")))

(define-public crate-rcon-0.1.0 (c (n "rcon") (v "0.1.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "podio") (r "^0.1") (d #t) (k 0)))) (h "08n77wszf4462xmn5gm7rh2bjw3f93ks39mxck41xmxdmigrvlba")))

(define-public crate-rcon-0.2.0 (c (n "rcon") (v "0.2.0") (d (list (d (n "err-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("tcp" "io-util" "dns"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("macros"))) (d #t) (k 2)))) (h "1n63y09a8kzrvpnb5cm1s3jicanchv5mxnsyv6bwg4kzsn6vk1l0")))

(define-public crate-rcon-0.2.1 (c (n "rcon") (v "0.2.1") (d (list (d (n "err-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("tcp" "io-util" "dns" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros"))) (d #t) (k 2)))) (h "06gzn3f0hmpd163ciql916jh0520dz7qzg726aj3fzvf6nlip92j") (f (quote (("delay") ("default" "delay"))))))

(define-public crate-rcon-0.3.0 (c (n "rcon") (v "0.3.0") (d (list (d (n "err-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("tcp" "io-util" "dns" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros"))) (d #t) (k 2)))) (h "002i7l4sxphya3q7ncnh2ns6rbdnkqmb4h98fk67rn20dvp7h7kj") (f (quote (("default"))))))

(define-public crate-rcon-0.3.1 (c (n "rcon") (v "0.3.1") (d (list (d (n "err-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("tcp" "io-util" "dns" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "09g8iaa8xk5q1r270cjcl7qnvp0q470c9kv4rfn6ldbgm0jd401k") (f (quote (("default"))))))

(define-public crate-rcon-0.4.0 (c (n "rcon") (v "0.4.0") (d (list (d (n "err-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("net" "io-util" "time" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0l470m4xm95mqyw59xly9k2lfpfpmllh4wbcfqbm3wah8992z63x") (f (quote (("default"))))))

(define-public crate-rcon-0.5.0 (c (n "rcon") (v "0.5.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("async-io"))) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "err-derive") (r "^0.3.0") (d #t) (k 0)))) (h "023m8kina89nnq6r73aai9idc207gb41bq8r2146s1i3lchp9whg")))

(define-public crate-rcon-0.5.1 (c (n "rcon") (v "0.5.1") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "err-derive") (r "^0.3.0") (d #t) (k 0)))) (h "118ax5fvy7p4v7dsrn3nqmxr7rw3dc17lm478w49k8v46686ynj6")))

(define-public crate-rcon-0.5.2 (c (n "rcon") (v "0.5.2") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "err-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1rkihy95grrlw76d07pipj88w1aznhl3my2c5px91gc6dwadszvb")))

(define-public crate-rcon-0.6.0 (c (n "rcon") (v "0.6.0") (d (list (d (n "async-std") (r "^1.9.0") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "err-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 2)) (d (n "tokio") (r "^1.10.1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "0ys18zr00ydgz73xlzk3xg0migil4kczc9xg9dpps7h43ksap2v1") (f (quote (("rt-tokio" "tokio/net" "tokio/time") ("rt-async-std" "async-std") ("default"))))))

