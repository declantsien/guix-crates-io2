(define-module (crates-io rc on rcon-shell) #:use-module (crates-io))

(define-public crate-rcon-shell-0.1.0 (c (n "rcon-shell") (v "0.1.0") (d (list (d (n "directories") (r "^2.0.2") (d #t) (k 0)) (d (n "rcon") (r "^0.2.1") (d #t) (k 0)) (d (n "rpassword") (r "^4.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (d #t) (k 0)))) (h "0s0zsw9v9xzlnfp7xdmlzr2894qzxrpxaqs92x12blgyfcsbq3p0")))

(define-public crate-rcon-shell-0.1.1 (c (n "rcon-shell") (v "0.1.1") (d (list (d (n "directories") (r "^2.0.2") (d #t) (k 0)) (d (n "rcon") (r "^0.2.1") (d #t) (k 0)) (d (n "rpassword") (r "^4.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (d #t) (k 0)))) (h "1rpsp3nlsymph83rwgr50fl63g59lilnhhqb7lfvdpd994wgs828")))

