(define-module (crates-io rc on rconn) #:use-module (crates-io))

(define-public crate-rconn-0.1.0 (c (n "rconn") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r9hywrsygrlfa558k75hza97b2aj5dwnfkbqh6f6a6n7wlsn9l8") (y #t)))

(define-public crate-rconn-0.1.1 (c (n "rconn") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "016a2bsqk499sxwzplcibm0mm32rv73yyyr9wk47mam7m0vy6z0d") (y #t)))

(define-public crate-rconn-0.1.2 (c (n "rconn") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qj4lghx2q19mcx22wc7vs0lngbxr44dbwvfh2r180jcg5rdzghb")))

(define-public crate-rconn-0.2.0 (c (n "rconn") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05gjfcfin7dfzlyfv8gldh8k7avij3g192h2b11hn3x0qxn4941f")))

(define-public crate-rconn-0.3.0 (c (n "rconn") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03kdaz6n82v051sl5w5mvhk3c5hsxa59zjafgsnzrlzmkzdzkf9g") (y #t)))

(define-public crate-rconn-0.3.1 (c (n "rconn") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k5f4zmzphzw24dfb4lwc5xnac442j8z9pivm2i56aq010fj2cnb") (y #t)))

(define-public crate-rconn-0.3.2 (c (n "rconn") (v "0.3.2") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0khc9mwsw296b9ahaig8r4145lr0jhv5b55xj75c3f00fy3nx28d") (y #t)))

(define-public crate-rconn-0.3.3 (c (n "rconn") (v "0.3.3") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11a8sydrxpfahnlacif89jhs842ij84dzgy9yf2vasl013xqgjy4")))

(define-public crate-rconn-0.3.4 (c (n "rconn") (v "0.3.4") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1blz6jzg3r1a4kv347dz7kkyhbwxfijzk9ipzbqn3s7nv4j4r7x5")))

(define-public crate-rconn-0.3.5 (c (n "rconn") (v "0.3.5") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z24xrw1fi2vg09q1wjy2l8h0qb1smw8cgqizxz61hxnj1w2a8c5") (y #t)))

(define-public crate-rconn-0.3.6 (c (n "rconn") (v "0.3.6") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k8xwb719asp6lvpy3klg2ib15nmp97rpfngnihpz3773yvrbc7j")))

