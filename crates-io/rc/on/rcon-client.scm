(define-module (crates-io rc on rcon-client) #:use-module (crates-io))

(define-public crate-rcon-client-0.1.2 (c (n "rcon-client") (v "0.1.2") (d (list (d (n "bytes") (r "1.1.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "1.0.*") (d #t) (k 0)))) (h "09n7dcnpd62bzydh8wn4c3mbzr3m0f1a1pgkp2q55n904m1iw0gy")))

