(define-module (crates-io rc c- rcc-cosmos) #:use-module (crates-io))

(define-public crate-rcc-cosmos-0.1.0 (c (n "rcc-cosmos") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "cosmos-sdk-proto") (r "^0.15.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rcc-trait-chain") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1a86vw44gg19892sw7z7kaq8q7qy7jzfpn9dn77ch83jyd98qmk4")))

