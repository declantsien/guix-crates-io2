(define-module (crates-io rc c- rcc-aptos) #:use-module (crates-io))

(define-public crate-rcc-aptos-0.1.0 (c (n "rcc-aptos") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bcs") (r "^0.1.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rcc-trait-chain") (r "^0.1.0") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "12qwyb7p3sfbwr8n12yd8c1dz0r0f627hisag15jw59ya2c0jkw9") (f (quote (("default" "address32") ("address32") ("address20"))))))

