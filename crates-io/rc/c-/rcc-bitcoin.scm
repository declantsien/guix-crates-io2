(define-module (crates-io rc c- rcc-bitcoin) #:use-module (crates-io))

(define-public crate-rcc-bitcoin-0.1.0 (c (n "rcc-bitcoin") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.29.2") (f (quote ("no-std"))) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "191y8dhndx5cimx4g4cadhh851r7jy800xhfbw6zfbbwbwhg9iv9")))

