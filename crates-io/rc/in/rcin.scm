(define-module (crates-io rc in rcin) #:use-module (crates-io))

(define-public crate-rcin-0.1.0 (c (n "rcin") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1z1q3v0fb14zkxm89f9lc5vbyg28rikg25namw0881f0f4i7m3d1")))

(define-public crate-rcin-0.1.1 (c (n "rcin") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "159gvqb7ilkzv1r2xv3763nhvyrj3gn7nrqairv1ss3sf2dv1pc7")))

(define-public crate-rcin-0.2.0 (c (n "rcin") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1mzp1m21k3gm3bl6ykqx70zijimg2vw0j2ii5s8ilj5mkxbw5p86")))

(define-public crate-rcin-0.2.1 (c (n "rcin") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1cbhcahjjdyx7xx2h7i2135wwhn20a6w8nnynm62wa0c11ap065s")))

