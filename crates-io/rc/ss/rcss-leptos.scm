(define-module (crates-io rc ss rcss-leptos) #:use-module (crates-io))

(define-public crate-rcss-leptos-0.2.0 (c (n "rcss-leptos") (v "0.2.0") (d (list (d (n "leptos") (r "^0.6.0") (d #t) (k 0)) (d (n "leptos_meta") (r "^0.6.0") (d #t) (k 0)) (d (n "rcss") (r "^0.2.0") (d #t) (k 0)) (d (n "rcss-layers") (r "^0.2.0") (d #t) (k 0)))) (h "0h7jsjlghwb84dzbqbwhiwiycyn9hdm5fc5wkpsk4ksagiba2qz7")))

