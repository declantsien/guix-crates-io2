(define-module (crates-io rc ss rcss-layers) #:use-module (crates-io))

(define-public crate-rcss-layers-0.2.0 (c (n "rcss-layers") (v "0.2.0") (d (list (d (n "rcss") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "12hflsklkrd8gbyw1cmzlqxq92yj04280nzsf49cq0ap8q2rc3yy") (f (quote (("default" "rcss_enable")))) (s 2) (e (quote (("rcss_enable" "dep:rcss"))))))

