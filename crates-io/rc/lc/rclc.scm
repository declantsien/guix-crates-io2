(define-module (crates-io rc lc rclc) #:use-module (crates-io))

(define-public crate-rclc-0.8.3 (c (n "rclc") (v "0.8.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rcalc_lib") (r "^0.9") (d #t) (k 0)))) (h "04xdygzbq5kg1963pc30srvbri4i7482nka3zj4iw2bhn2hlbl9n")))

(define-public crate-rclc-0.8.4 (c (n "rclc") (v "0.8.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rcalc_lib") (r "^0.9") (d #t) (k 0)))) (h "0g5hfrfqmrgzfnr0v93nz1a2d2gbwv66kh8vgx4bqz38c908pcqk")))

(define-public crate-rclc-0.8.5 (c (n "rclc") (v "0.8.5") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rcalc_lib") (r "^0.9") (d #t) (k 0)))) (h "13vb958ry963f3sq44wr8x907mfi9fsh4lc7zz2b0ddxcfas9qz4")))

(define-public crate-rclc-0.8.6 (c (n "rclc") (v "0.8.6") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rcalc_lib") (r "^0.9") (d #t) (k 0)))) (h "170xzkbxpq06r0rc1kiw9wa181s8jjkk88m5h1dnygcj97ycs434")))

(define-public crate-rclc-0.8.7 (c (n "rclc") (v "0.8.7") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rcalc_lib") (r "^0.9") (d #t) (k 0)))) (h "03218r722lrkzrsmxb913pvh6p20wc934znnr6p4h2jpmvj61a91")))

(define-public crate-rclc-1.0.0 (c (n "rclc") (v "1.0.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rcalc_lib") (r "^1") (d #t) (k 0)))) (h "07ydcifn09vrrjvzi7z2y1cjj2w93p3rv151xygk8m7w14qkqkrk")))

