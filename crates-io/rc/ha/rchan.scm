(define-module (crates-io rc ha rchan) #:use-module (crates-io))

(define-public crate-rchan-0.2.2 (c (n "rchan") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0dql9mivdzfw73p1g4ibl4id6rll3viijs20ny1y9610ijgrxsg8")))

(define-public crate-rchan-0.2.3 (c (n "rchan") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0hyl2y4mh8yqk064zh7rmzpg102hzhrqdglc1vyirb6laxyrxxsf")))

(define-public crate-rchan-0.3.0 (c (n "rchan") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0p3zyx71zkcpf49cqc3c0jnpfqxymqf8bk7ax5gsgnk909yfkarp")))

