(define-module (crates-io rc cp rccp) #:use-module (crates-io))

(define-public crate-rccp-0.1.6 (c (n "rccp") (v "0.1.6") (h "01xrbh4dn8bk24q3nxl2g9s7mpkkybrgai63wbd0pdj9zhgyhckp")))

(define-public crate-rccp-0.1.8 (c (n "rccp") (v "0.1.8") (h "1zzmrqhi2jdskxc3kjd72qzsh312fydvbvvchz7iam3s0p16nkmw")))

(define-public crate-rccp-0.1.9 (c (n "rccp") (v "0.1.9") (h "1xkraqa6skvc6k987pvwxcrizd5a85g4b5r8k58c8daa74ijmax7")))

(define-public crate-rccp-0.2.0 (c (n "rccp") (v "0.2.0") (h "0ixhc0n8jp46yxwkykx8s99m0qgipfmm0n0fzwwyxa19bn693srz")))

(define-public crate-rccp-0.2.1 (c (n "rccp") (v "0.2.1") (h "0ka3v25icmkr5mzagilg3zbcjqbb669pvb1y5ky7ajm9xvhccqnb")))

