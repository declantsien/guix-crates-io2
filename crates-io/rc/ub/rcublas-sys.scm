(define-module (crates-io rc ub rcublas-sys) #:use-module (crates-io))

(define-public crate-rcublas-sys-0.1.0 (c (n "rcublas-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.25.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0fnn4l03538vmjmbvmvz6angmpa9hnxrpxll83s328l8wzjh6w9c")))

(define-public crate-rcublas-sys-0.1.1 (c (n "rcublas-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.25.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1an2bbs6171nsgkdklfwvbrrmgfyhi9d1kr8g5s59xhg5vnmixl4")))

(define-public crate-rcublas-sys-0.2.0 (c (n "rcublas-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1w5gyhhqs8grh1cdn212iwf1vf8ifjl4sgy8p9y6f0jf23ncnli3") (l "cublas")))

(define-public crate-rcublas-sys-0.4.0 (c (n "rcublas-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1kxi4m3rij57ivmasjkvf7y2va3yg59vp2ny3dl15w9ly432v7pp") (f (quote (("generate" "bindgen") ("default" "generate")))) (l "cublas")))

(define-public crate-rcublas-sys-0.5.0 (c (n "rcublas-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1rc81wrd39n00n2jxi1mdmqyapvwhrb58jbsrir3d0b5gkvwmiy5") (f (quote (("generate" "bindgen") ("default")))) (l "cublas")))

