(define-module (crates-io rc ub rcublas) #:use-module (crates-io))

(define-public crate-rcublas-0.2.0 (c (n "rcublas") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.35") (o #t) (d #t) (k 0)) (d (n "coaster") (r "^0.1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rcublas-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0503wnfnvgbza1shs6b5h8gj7wqwsai29s2ysxd8i4ii926fc6c7") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-rcublas-0.3.0 (c (n "rcublas") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0.35") (o #t) (d #t) (k 0)) (d (n "coaster") (r "^0.1.0") (f (quote ("cuda" "native"))) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rcublas-sys") (r "^0.2") (d #t) (k 0)))) (h "19a5skkfiynkyxp512xgap8hqz5f27nxwv3k0y7fbwgrr36hk2wz") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-rcublas-0.5.0 (c (n "rcublas") (v "0.5.0") (d (list (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "coaster") (r "^0.1") (f (quote ("cuda" "native"))) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rcublas-sys") (r "^0.4") (d #t) (k 0)))) (h "1k7kfj4qm8dmrmx5j7s6j0gi49x5qva1hf634hlyp6svj0vmxs4k") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-rcublas-0.6.0 (c (n "rcublas") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rcublas-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bw03x871m8svq61i2fflw8qi9wyk3pdipm2b4qsw7818hk61mnj")))

