(define-module (crates-io rc me rcmerkle) #:use-module (crates-io))

(define-public crate-rcmerkle-0.1.0 (c (n "rcmerkle") (v "0.1.0") (d (list (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "0zifgf2k0hpy4i814dhl1ymywy9bw58g36r8fp9sr0wp6450w1x2")))

(define-public crate-rcmerkle-0.1.1 (c (n "rcmerkle") (v "0.1.1") (d (list (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "0qbkz5p69kskbkrdfy4bc2rn2g00glq1hkis2m0dwar2dzap4ddi")))

