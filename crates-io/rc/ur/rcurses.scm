(define-module (crates-io rc ur rcurses) #:use-module (crates-io))

(define-public crate-rcurses-0.1.0 (c (n "rcurses") (v "0.1.0") (d (list (d (n "ncurses") (r "^5.91.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0l1y3ah97w7p95g6jyj9g4hah4f68zm2f72wdflcvxkiydrngxrf") (y #t)))

