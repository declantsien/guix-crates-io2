(define-module (crates-io rc al rcalc) #:use-module (crates-io))

(define-public crate-rcalc-0.1.0 (c (n "rcalc") (v "0.1.0") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "0rj7yhp025bh02khyar8n9nikjacicy2f83d0fz85ki7vhfl8x3f")))

(define-public crate-rcalc-0.1.1 (c (n "rcalc") (v "0.1.1") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "0nva3viijf10whgbd4mfmx55gd8y9gls6axp38wr767dmw583156")))

(define-public crate-rcalc-0.1.2 (c (n "rcalc") (v "0.1.2") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "0bir5cpq2fgkv8nshsgh5m5g4ymzf3z94gayibdx6n3flk367rcx")))

