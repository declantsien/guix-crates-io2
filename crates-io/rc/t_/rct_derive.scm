(define-module (crates-io rc t_ rct_derive) #:use-module (crates-io))

(define-public crate-rct_derive-0.1.0 (c (n "rct_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0zx5854mpzzhhbcpxnb4xgwws8xgp2yi1kpcsnynqi7xnxmhs9b0") (f (quote (("default"))))))

