(define-module (crates-io rc ro rcron) #:use-module (crates-io))

(define-public crate-rcron-1.0.1 (c (n "rcron") (v "1.0.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "cron") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0wvm8sz8n96n816zand99abdxak9xpvi6zhwilrfhg57bg2lzcnw")))

(define-public crate-rcron-1.0.2 (c (n "rcron") (v "1.0.2") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "cron") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ma6hcb35il2zdvyzsi2hs3r9aa8b2qfkjpqxfx3vpvnaxp2qibm")))

(define-public crate-rcron-1.0.3 (c (n "rcron") (v "1.0.3") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "cron") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0g4q8bjlhwrya458myzlykqxdbwy358r57l69dddf0jzsj21a90d")))

(define-public crate-rcron-1.1.0 (c (n "rcron") (v "1.1.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "cron") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0yh8j4g6482gxsq69pfd9wvxkrqrjh17225asmd433nmvl1ggiss")))

(define-public crate-rcron-1.1.1 (c (n "rcron") (v "1.1.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "cron") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "00d82r3bpnylvar8n30vghhyv5srcqdh17w5bsa5cynszw1cr33k")))

(define-public crate-rcron-1.1.2 (c (n "rcron") (v "1.1.2") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "cron") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "02hl8h9bwzh4bkawfj97szvgk3crbms6idiixi5aghvrrq1v12mw")))

(define-public crate-rcron-1.1.3 (c (n "rcron") (v "1.1.3") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "cron") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "13iw9v22k6y6vwibinkldkgy2frfl1a2iq0bvjqd4jjc6fkzyy9i")))

(define-public crate-rcron-1.2.0 (c (n "rcron") (v "1.2.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "cron") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1zy3pzlxyffgi6p07dxl7cb516qmg0jnphwyvcvxzv7s68hc4xww")))

(define-public crate-rcron-1.2.1 (c (n "rcron") (v "1.2.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "cron") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0a6ahy2sfc8fhb5qmzpygr2l8r2cw5a25di4z1r85230x5swdk9h")))

(define-public crate-rcron-1.2.2 (c (n "rcron") (v "1.2.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cwp7jyg51n6rh4qv55rhknzhgsxspv7i8z0xl44c4xhdfr4wx79")))

