(define-module (crates-io rc rt rcrt1) #:use-module (crates-io))

(define-public crate-rcrt1-1.0.0 (c (n "rcrt1") (v "1.0.0") (d (list (d (n "goblin") (r "^0.4") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ascxkrx7ljyxl60pn8pib4ckkaqnjjxfw3iwghq5mc0bwjh01v0")))

(define-public crate-rcrt1-2.0.0 (c (n "rcrt1") (v "2.0.0") (d (list (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0p6nwk76xmjq6mbwijaf7sximacv8c2xmqdc651i6w7im5l1mf10") (r "1.56")))

(define-public crate-rcrt1-2.1.0 (c (n "rcrt1") (v "2.1.0") (d (list (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cd56mphqm5pcjmz704v0ghjfk29p237vzzxmvxmdgywz04b9wbw") (r "1.56")))

(define-public crate-rcrt1-2.2.0 (c (n "rcrt1") (v "2.2.0") (d (list (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bii9qkkvmc0q29blhi7v1rsdrgzasqarkfm4lk7py38dcbglfq6") (r "1.56")))

(define-public crate-rcrt1-2.3.0 (c (n "rcrt1") (v "2.3.0") (d (list (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ic342ahm3dcgi77m858al4cq7njc578vg2j1ah4qj01g5bqz0fv") (r "1.56")))

(define-public crate-rcrt1-2.4.0 (c (n "rcrt1") (v "2.4.0") (d (list (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qw0v75h42rp8icdrv7skr8lsak0wqc89b5yl65jir535jkrk2jg") (r "1.56")))

(define-public crate-rcrt1-2.5.0 (c (n "rcrt1") (v "2.5.0") (d (list (d (n "goblin") (r "^0.6") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08cl7sc5gzjlwxi33ig4r2y5wfi4dsfhhcyaigqchw3rpnig8ilf") (r "1.56")))

