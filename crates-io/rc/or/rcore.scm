(define-module (crates-io rc or rcore) #:use-module (crates-io))

(define-public crate-rcore-0.1.0 (c (n "rcore") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rair-env") (r "^0.1") (d #t) (k 0)) (d (n "rair-io") (r "^0.1") (d #t) (k 0)) (d (n "rtrees") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0p6mq9479cgh6k8lhqfnxrdj5pvypkihy6nyywblgj0v5zim05d5")))

