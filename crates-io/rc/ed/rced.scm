(define-module (crates-io rc ed rced) #:use-module (crates-io))

(define-public crate-rced-0.0.0 (c (n "rced") (v "0.0.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1k475b1rhimvifg5wi18zppcakv1g36zmkh05sxpgp5fc16vzanf") (y #t)))

(define-public crate-rced-0.0.1 (c (n "rced") (v "0.0.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0mad5gapk5jfcjn9cpz1qzf44qgfdcj8h63ggrhz8nqciaypjd13") (y #t)))

(define-public crate-rced-0.0.2 (c (n "rced") (v "0.0.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "023i8vkfhwcrrsl3saaf8961aa9l84zj6pf7829z1pp1jzmx95p2") (y #t)))

(define-public crate-rced-0.0.3 (c (n "rced") (v "0.0.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "15vfy5pbd34qjah5v6k0sgsnr71pv8v8cz3pp0pq8mapi10k30ic") (y #t)))

(define-public crate-rced-0.0.4 (c (n "rced") (v "0.0.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0vvqn75lqprxynvcaf8c945s097c426zayyfgwqirypb1f5010yg") (y #t)))

(define-public crate-rced-0.0.5 (c (n "rced") (v "0.0.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1ddfc4vp3gfrlk4xw7z9qp0xs4ibcpxfl72islbbg0iav3l70lq5") (y #t)))

(define-public crate-rced-0.0.6 (c (n "rced") (v "0.0.6") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "10ir7qkpn56v3d0x8yx1y62m9mfjg6jq7zyczhl1j6vyrmw08v3h") (y #t)))

(define-public crate-rced-0.0.7 (c (n "rced") (v "0.0.7") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0f1a1g6q0c2dd764a85i0akd0fzb6kvz5bsg8lwfh4aw3kmg1pc4") (y #t)))

(define-public crate-rced-0.0.8 (c (n "rced") (v "0.0.8") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1lqqk8nw7ibzr87zw30irq7xhkn8jyl370idmjn9cvy1rvbc1g01")))

