(define-module (crates-io rc mi rcmixed) #:use-module (crates-io))

(define-public crate-rcmixed-0.0.1 (c (n "rcmixed") (v "0.0.1") (d (list (d (n "aes-soft") (r "^0.3") (d #t) (k 0)) (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "1ssfrjif1bvwi24bdami96xvw51y70pm9fvcvak616j52vmfrlvk")))

(define-public crate-rcmixed-0.0.2 (c (n "rcmixed") (v "0.0.2") (d (list (d (n "aes-soft") (r "^0.3") (d #t) (k 0)) (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "1xx9v18328h5lcigwcgg76d83hzhd6lw4pf6krp6vs7zgq2g7zfs")))

