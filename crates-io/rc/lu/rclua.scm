(define-module (crates-io rc lu rclua) #:use-module (crates-io))

(define-public crate-rclua-0.0.1 (c (n "rclua") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.97") (d #t) (k 1)) (d (n "libc") (r "^0.2.154") (d #t) (k 0)))) (h "0czgxm46h26nnpf07gcwq694vqgpsxgj49c4ym1rmgwf8zawiqqd")))

(define-public crate-rclua-0.0.2 (c (n "rclua") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.97") (d #t) (k 1)) (d (n "libc") (r "^0.2.154") (d #t) (k 0)))) (h "0iany2pxmw0b872yn26nap8iw7yqpgsqfk82an021lsriq6ilckw")))

(define-public crate-rclua-1.0.0 (c (n "rclua") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.97") (d #t) (k 1)) (d (n "libc") (r "^0.2.154") (d #t) (k 0)))) (h "1cnxrs9jglmy2vqh2r815wicjcj20gp6zkbj7222nlnambgp06q0")))

(define-public crate-rclua-1.0.1 (c (n "rclua") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.97") (d #t) (k 1)) (d (n "libc") (r "^0.2.154") (d #t) (k 0)))) (h "0prlb6bnj4h6291i48z1hl9fwg7izp8i664jcnr84mmi6yapv3qr")))

