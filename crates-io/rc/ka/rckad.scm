(define-module (crates-io rc ka rckad) #:use-module (crates-io))

(define-public crate-rckad-0.0.1 (c (n "rckad") (v "0.0.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "042n6kb48hkzrasvw23sip288fm253d8pgbk24yras17i4b70z0s")))

(define-public crate-rckad-0.0.2 (c (n "rckad") (v "0.0.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "0d1vnkyz1mxa3xfardznm4kz26qk5cwv1kycqk87ddjr78v5ms42")))

(define-public crate-rckad-0.0.3 (c (n "rckad") (v "0.0.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "0vf48zq4gs0kqvsrakbi3x6mnl72njhllx1kv6n6s6ch2n3jc7hn")))

(define-public crate-rckad-0.0.4 (c (n "rckad") (v "0.0.4") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "0bkrqh685103ml35w0lm121n2l32h3cvddw4rgbh8kbqs9rbkb8j")))

(define-public crate-rckad-0.0.5 (c (n "rckad") (v "0.0.5") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "107pss06mdhzk6hngpja9a3r6armkcfja3171rvp692mhczrknkx")))

(define-public crate-rckad-0.0.6 (c (n "rckad") (v "0.0.6") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "1925cfn4lm1i039s9hr8i5yay1f703frgwdypvwsp12zzwj43y2j")))

(define-public crate-rckad-0.1.0 (c (n "rckad") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "blake3") (r "^0.3") (k 0)) (d (n "postcard") (r "^0.5") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 0)))) (h "1bp4p339bl6anh0awf7y197m0s162lh3ja26pxzxa5zhz5bzk5n9")))

