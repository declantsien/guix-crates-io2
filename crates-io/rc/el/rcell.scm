(define-module (crates-io rc el rcell) #:use-module (crates-io))

(define-public crate-rcell-0.0.0 (c (n "rcell") (v "0.0.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0n47db3r58aad4qap4b1bmhaax69mszyfsqja2rprqk06ja1vy5a")))

(define-public crate-rcell-0.1.0 (c (n "rcell") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0rnqfhvfvrakplprqpfvcmy5shpr7j749c4v3cxiqc5cgqjxb3gl")))

(define-public crate-rcell-0.2.0 (c (n "rcell") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0bd9p6y53l9m2rwx6ws3mzibgppyf4bbj2419w7k0j675jzvh024")))

(define-public crate-rcell-0.3.0 (c (n "rcell") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "16hvajhvjpd31xalypais3xd9glykrwaahwa5wg4crad19pinnvh")))

(define-public crate-rcell-0.4.0 (c (n "rcell") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1z8aqhdixq4ad9p97lsbsx3jabqj348lx87yrm4zqv19j1m60vf1")))

(define-public crate-rcell-0.5.0 (c (n "rcell") (v "0.5.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0963gg6y26sji6chhwpbi0j0445cvn4q6xcld783nmisvdqh8ngp")))

(define-public crate-rcell-1.0.0 (c (n "rcell") (v "1.0.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1af4pglf26x5pgrhal5cw4nn2zbpcwii2blpbj8rhhxh6gpvhq9x")))

(define-public crate-rcell-1.1.0 (c (n "rcell") (v "1.1.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "05sa46j894kdk7ghy25cfq7b6a3gk9h3rmqzvnhw5g5l35lyxsww")))

(define-public crate-rcell-1.1.1 (c (n "rcell") (v "1.1.1") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "10mkr7560r1z16rjkjm4yd2bvln8bg54mws63wy3ijy15ijl6yvf")))

(define-public crate-rcell-1.1.2 (c (n "rcell") (v "1.1.2") (d (list (d (n "parking_lot") (r ">=0.11, <=0.12") (d #t) (k 0)))) (h "13gbvql3zc7k27r5dkiyhqggz8307pfxw9j50n74nn9v6jkq2nnz")))

(define-public crate-rcell-1.1.3 (c (n "rcell") (v "1.1.3") (d (list (d (n "sharded_mutex") (r "^0.2") (d #t) (k 0)))) (h "0f7iscpsl0cnr6zsp6z397b7kl4m2hd4vsafhf0whjarj3llmh62")))

(define-public crate-rcell-2.0.0-pre0 (c (n "rcell") (v "2.0.0-pre0") (d (list (d (n "sharded_mutex") (r "^0.4.0") (d #t) (k 0)))) (h "0qviggzwp1dpq98m2hkg8ihqc1rkf9lh0v1dbvwjm7mjcb2zlsag")))

(define-public crate-rcell-2.0.0 (c (n "rcell") (v "2.0.0") (h "08ycymcz5za1wh3xpxc59b7gw3pa3v288md36ryra9vcx4cgg0ny") (f (quote (("sync") ("default" "sync"))))))

