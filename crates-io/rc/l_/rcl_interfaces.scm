(define-module (crates-io rc l_ rcl_interfaces) #:use-module (crates-io))

(define-public crate-rcl_interfaces-1.2.1 (c (n "rcl_interfaces") (v "1.2.1") (d (list (d (n "builtin_interfaces") (r "^1.2.1") (d #t) (k 0)) (d (n "rosidl_runtime_rs") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "02abpdvhdhcx0xkp3kbidv0h42a09qyc6h268hxk2jk9fh9grfa2") (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde-big-array" "rosidl_runtime_rs/serde" "builtin_interfaces/serde"))))))

