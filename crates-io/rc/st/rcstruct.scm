(define-module (crates-io rc st rcstruct) #:use-module (crates-io))

(define-public crate-rcstruct-0.1.0 (c (n "rcstruct") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)))) (h "1hld8xhjdxg0i5jbp6b07pdzcz772rgdp3q9igr7r4fdaz1vpg0a")))

(define-public crate-rcstruct-0.1.1 (c (n "rcstruct") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)))) (h "0qgn5ympcja9q0kzq57pl3r98yq6sckjn0l84f46msy4p7jpmbxr")))

(define-public crate-rcstruct-0.1.2 (c (n "rcstruct") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)))) (h "00apy6mxyhywjffpr80ky4s9pg2cjsjfyd8c55j45p5k3in9sxs3")))

(define-public crate-rcstruct-0.1.3 (c (n "rcstruct") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)))) (h "07f374a6iw162qw682iy3rpfxa6zyhzpbjdalyc0m3bjd7rswq81")))

(define-public crate-rcstruct-0.1.4 (c (n "rcstruct") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)))) (h "1z2pl029h0n8yilnqhny387vy3a05rvsqh101j52kczqc998zjl7")))

(define-public crate-rcstruct-0.2.0 (c (n "rcstruct") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)))) (h "1myqsqznp42n7scp8pzsfs3gqqncsm3fjxgsfv0ixzn5dj5ywqf4")))

