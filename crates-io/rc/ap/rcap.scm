(define-module (crates-io rc ap rcap) #:use-module (crates-io))

(define-public crate-rcap-0.1.0 (c (n "rcap") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1vz30yqdg1v9ybc68gpp4gmlrq8jq9dv2isyqz0j229sk9laz2cc")))

(define-public crate-rcap-0.1.1 (c (n "rcap") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1qidlb8f3m85s8fhfvxjvadx4g84rxxf23nh5n5snif2g4n6pp6g")))

(define-public crate-rcap-0.1.2 (c (n "rcap") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "102rwbvx8l8433cv9kav756rvir7skzjaa6xqrhlkzg17iw6n3iq")))

(define-public crate-rcap-0.1.3 (c (n "rcap") (v "0.1.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1mymrblmjq8qgzrg7fbgb5dmppc30pa4j4ipdzzm7sny5hk7h30x")))

(define-public crate-rcap-0.1.4 (c (n "rcap") (v "0.1.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1ajq04w9i5y9c9ga63g6yp6s29gxncwz776zhim8jrvc9aaj1rl4")))

