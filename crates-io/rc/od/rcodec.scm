(define-module (crates-io rc od rcodec) #:use-module (crates-io))

(define-public crate-rcodec-1.0.0 (c (n "rcodec") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "pl-hlist") (r "^1.0.0") (d #t) (k 0)))) (h "06mi0fs2wza0p939y0j4aj0fnrc8hp20zw07c3499486226h03bf")))

(define-public crate-rcodec-1.0.1 (c (n "rcodec") (v "1.0.1") (d (list (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "pl-hlist") (r "^1.0") (d #t) (k 0)))) (h "1vykrs9jgbaxmbzr17b2q2rzz5ifnb4dn19zb7q65gqqgkfb1xmr")))

