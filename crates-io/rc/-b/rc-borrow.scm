(define-module (crates-io rc -b rc-borrow) #:use-module (crates-io))

(define-public crate-rc-borrow-1.0.0 (c (n "rc-borrow") (v "1.0.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "19107jwh878awifb58nwcknkjcy2yxs76r0r0vx2q1702945dbh3") (f (quote (("std") ("default" "erasable" "std")))) (y #t)))

(define-public crate-rc-borrow-1.1.0 (c (n "rc-borrow") (v "1.1.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1k63f6jbrqsjlx0dn551hw1fayvhpj8mii3ffwzcscqi4sszzjlg") (f (quote (("std") ("default" "erasable" "std"))))))

(define-public crate-rc-borrow-1.2.0 (c (n "rc-borrow") (v "1.2.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1g6ccqspqszb90fk4vvxs75pszzaj7y5w350vn816ldw2b1digdp") (f (quote (("std") ("default" "erasable" "std"))))))

(define-public crate-rc-borrow-1.2.1 (c (n "rc-borrow") (v "1.2.1") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0l7xl1qid17anbn36xy06ppcy40pdxfkdd24z6j7x450rspz0m0f") (f (quote (("std") ("default" "erasable" "std"))))))

(define-public crate-rc-borrow-1.3.0 (c (n "rc-borrow") (v "1.3.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1l5ha7h5m8jk9y3p4sydj7s0003m64b1y6injycqvmr6ff8kqvgp") (f (quote (("std") ("default" "erasable" "std"))))))

(define-public crate-rc-borrow-1.4.0 (c (n "rc-borrow") (v "1.4.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0mszljysiqjrrinms54mbb9klb5s83plixw6khr1x9nphv2vf3dg") (f (quote (("std") ("default" "erasable" "std"))))))

