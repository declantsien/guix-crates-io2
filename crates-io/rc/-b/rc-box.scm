(define-module (crates-io rc -b rc-box) #:use-module (crates-io))

(define-public crate-rc-box-1.0.0 (c (n "rc-box") (v "1.0.0") (d (list (d (n "erasable") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1r9s5whj70ffilj4gx6yf17mg0k38ld3f9vsk5676v7kdlfpaf3n") (f (quote (("default" "erasable"))))))

(define-public crate-rc-box-1.1.0 (c (n "rc-box") (v "1.1.0") (d (list (d (n "erasable") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "slice-dst") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "1yj1b8vnnf31ilbxy2f9977lgcmahmckprhdyp1cl6569q60wdci") (f (quote (("default" "erasable"))))))

(define-public crate-rc-box-1.1.1 (c (n "rc-box") (v "1.1.1") (d (list (d (n "erasable") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "slice-dst") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "115qyny9sn3ljg676ddrnqlpq94y6hf8zcilmlxv5nrmmxkfc721") (f (quote (("default" "erasable"))))))

(define-public crate-rc-box-1.2.0 (c (n "rc-box") (v "1.2.0") (d (list (d (n "erasable") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "slice-dst") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "unsize") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1n1zb14isg05apb21n8h4b0bm58kvrdc5aydq8q402dzx9chfsg0") (f (quote (("default" "erasable"))))))

