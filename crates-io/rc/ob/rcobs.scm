(define-module (crates-io rc ob rcobs) #:use-module (crates-io))

(define-public crate-rcobs-0.1.0 (c (n "rcobs") (v "0.1.0") (d (list (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "1wzx11ykl8wrjb7914p3w93dbfq8zrvr0gmmpg81wjd70fi1ik27") (f (quote (("std") ("default" "std"))))))

(define-public crate-rcobs-0.1.1 (c (n "rcobs") (v "0.1.1") (d (list (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "0lm206p86ihr2fkxxd2bxjr13k8fsindfhx01blmb0bg6z7c9k86") (f (quote (("std") ("default" "std"))))))

