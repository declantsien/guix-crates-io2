(define-module (crates-io rc tr rctree) #:use-module (crates-io))

(define-public crate-rctree-0.0.1 (c (n "rctree") (v "0.0.1") (h "1iyz8q4b4c6pycvark6abds20l23qdl4dyyyrayw4phpnzayjwa3") (y #t)))

(define-public crate-rctree-0.1.0 (c (n "rctree") (v "0.1.0") (h "06c7v5l27va8qkaa3glvj4w933j1ccjyvmc91k06v2h33prnbhnn") (y #t)))

(define-public crate-rctree-0.1.1 (c (n "rctree") (v "0.1.1") (h "0dgy9w76j66l00hfk37x7f4423zy52viznh7br28cin1fripnqcq") (y #t)))

(define-public crate-rctree-0.2.0 (c (n "rctree") (v "0.2.0") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "0r9bp7bx3d8dbrgck9fdb90ki8fphj66ad2a01nj5fpbim72kidq")))

(define-public crate-rctree-0.2.1 (c (n "rctree") (v "0.2.1") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "0jsz0sicjz7lgrij0z2lnmbdfjx25kk5n6mbsvhbr5bg0pqf05qq")))

(define-public crate-rctree-0.2.2 (c (n "rctree") (v "0.2.2") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "1rz033fxqg207way4xlivp83xssji03qhwg2j0islbswa00kj2ys")))

(define-public crate-rctree-0.3.0 (c (n "rctree") (v "0.3.0") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "0c4ald1576v4fkh8p997zf7gsbq25hi75y461kr8wmm0fhbpjrbk")))

(define-public crate-rctree-0.3.1 (c (n "rctree") (v "0.3.1") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "0a57yv9ygb5xrrqipdq1kkcqkv2ks5ybhbqa5mzb0y8lrz5i5z5n")))

(define-public crate-rctree-0.3.2 (c (n "rctree") (v "0.3.2") (d (list (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "1nkzx3h9w7ha8sncq3iwn20dbh59hnvpyb8y4fvn5d1j6n912g8r")))

(define-public crate-rctree-0.3.3 (c (n "rctree") (v "0.3.3") (h "1a54z2b850albiqx9vw009p9xg363vqzh1ybkwb89zn8375jk7my")))

(define-public crate-rctree-0.4.0 (c (n "rctree") (v "0.4.0") (h "1s92472lniqn2c0b5ln8ssl014x0raiyzkk0hagrvsd6far2iq4s")))

(define-public crate-rctree-0.5.0 (c (n "rctree") (v "0.5.0") (h "0kvzahkwriawhjjb08ai7rfi77px7rpx5h83hjcx6dccyxzf4hiv")))

(define-public crate-rctree-0.6.0 (c (n "rctree") (v "0.6.0") (h "1sd6vsa5p3j27v6f1v0l0afl3hn4an1jr3psky3024gcmdk7hgp0")))

