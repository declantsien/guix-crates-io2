(define-module (crates-io rc -u rc-u8-reader) #:use-module (crates-io))

(define-public crate-rc-u8-reader-1.0.0 (c (n "rc-u8-reader") (v "1.0.0") (h "1amcj58pmaqfz3316j9lg6lqw5j98sx1xipizhi134bcg59lf1pa") (f (quote (("nightly"))))))

(define-public crate-rc-u8-reader-1.0.1 (c (n "rc-u8-reader") (v "1.0.1") (h "1mzd2fwgwnds8kvzb53gxqa9akmwhjjic4m0iq2lnflgxgx2h9zk") (f (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2.0.0 (c (n "rc-u8-reader") (v "2.0.0") (h "0qfzyrjqsq6h8g1g1slxsfgf25p75wzsx45dxpxm35vilnafdazq") (f (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2.0.1 (c (n "rc-u8-reader") (v "2.0.1") (h "0by6csyg8ns7iv356xrbl9bjgh1yvhjvjsk68xx6xv94dg4cyjc4") (f (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2.0.2 (c (n "rc-u8-reader") (v "2.0.2") (h "0ydzvnf73q56q16vp6fqn41lwg376xl6qk7aa6aj7n3fcw3s45gz") (f (quote (("nightly")))) (y #t)))

(define-public crate-rc-u8-reader-2.0.3 (c (n "rc-u8-reader") (v "2.0.3") (h "15yahippxhwiqlc774ms8xi8s365dkl38m3k4yb5y12k020kch0b") (f (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2.0.4 (c (n "rc-u8-reader") (v "2.0.4") (d (list (d (n "debug-helper") (r "^0.1") (d #t) (k 0)))) (h "1g8csxrp6px4am64ijz5ibqfijf50dj948wbaji5wvx69xp7wj17") (f (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2.0.5 (c (n "rc-u8-reader") (v "2.0.5") (d (list (d (n "debug-helper") (r "^0.1") (d #t) (k 0)))) (h "05q63llpa2s0g3z4lbkj486xjccpn79czsa479ccz38xlhsxxjj1") (f (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2.0.6 (c (n "rc-u8-reader") (v "2.0.6") (d (list (d (n "debug-helper") (r "^0.2") (d #t) (k 0)))) (h "1mxp5r6if04ibadh4pxsn22hxp9n7zflq43yv8z280wzi71mshx0") (f (quote (("nightly")))) (y #t)))

(define-public crate-rc-u8-reader-2.0.7 (c (n "rc-u8-reader") (v "2.0.7") (d (list (d (n "debug-helper") (r "^0.2") (d #t) (k 0)))) (h "10w9mfi75dcg832hdrvym7wrc32a2688p4d42a18n015j0wdhah2") (f (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2.0.8 (c (n "rc-u8-reader") (v "2.0.8") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "1sd87qw3s0a83qmdcibgzq95pchql9p08wwy0slcpgs53s5kdkqw") (f (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2.0.9 (c (n "rc-u8-reader") (v "2.0.9") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "0c5k5vhkwwyw57y0x1jykz0h4lqmgkz47hagqhv6xwlzacdswqlp") (f (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2.0.10 (c (n "rc-u8-reader") (v "2.0.10") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "1h7sxhq50jhkj7x6r0ly3369l7w6cvndl7sagggaznbaxpnbkypa") (f (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2.0.11 (c (n "rc-u8-reader") (v "2.0.11") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "0k311kaygdqlzy3k71043r9wxymi4rk05j7r5gxvjscmlzisiiyq") (f (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2.0.12 (c (n "rc-u8-reader") (v "2.0.12") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "0bw4ji3nwk8nrm3nvbq8jw5rg44j1plpdklsgra1zm3bk62qbl5a") (f (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2.0.13 (c (n "rc-u8-reader") (v "2.0.13") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "15sddzia7wlp5dhczc5bjdr94pfrdp8yaxsypdxi4r9y2zfbpxrz")))

(define-public crate-rc-u8-reader-2.0.14 (c (n "rc-u8-reader") (v "2.0.14") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "0s8zvcl655ism4j8xhx7qa2vw3i2a1c3ywmcgl5jc1vggrl42aw1")))

(define-public crate-rc-u8-reader-2.0.15 (c (n "rc-u8-reader") (v "2.0.15") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (k 0)))) (h "0imhivh4p0ib9vlk5d4bd01aqv2ssv47y7zbpl1j39qjfsaslq3j")))

(define-public crate-rc-u8-reader-2.0.16 (c (n "rc-u8-reader") (v "2.0.16") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (k 0)))) (h "0havzvikj3gx6kdghnz6c1cdfkri3g4zsawfwmkns7ycxmhg73qs")))

