(define-module (crates-io rc #{5-}# rc5-cipher) #:use-module (crates-io))

(define-public crate-rc5-cipher-0.1.0 (c (n "rc5-cipher") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1na77is4i95ciiiysswmjgvqll8raix2xffjrpfzsy4mpkh9bm3d")))

(define-public crate-rc5-cipher-0.1.1 (c (n "rc5-cipher") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1gvwnlp8acgl5mm087xk6mlz9b0ydxsxmh012rs7whxn2fh13ryc")))

(define-public crate-rc5-cipher-0.1.2 (c (n "rc5-cipher") (v "0.1.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "19zqipmzbx6p9iqq2wsdmn01cm5q7zp7ba1356rkgyb1giamx6w7")))

