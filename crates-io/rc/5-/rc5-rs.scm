(define-module (crates-io rc #{5-}# rc5-rs) #:use-module (crates-io))

(define-public crate-rc5-rs-0.1.0 (c (n "rc5-rs") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-bytes") (r "^0.4") (d #t) (k 0)) (d (n "secrecy") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01ds4kkyzpbqz80jaiadsk2jwji95cksqlbadm6w2cx698cvma7x")))

(define-public crate-rc5-rs-0.1.1 (c (n "rc5-rs") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-bytes") (r "^0.4") (d #t) (k 0)) (d (n "secrecy") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qwvaiyq5va7cj5i67f26bn0wqd7ynp3szgsnfxvkrv4v1c7al92")))

(define-public crate-rc5-rs-0.1.2 (c (n "rc5-rs") (v "0.1.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-bytes") (r "^0.4") (d #t) (k 0)) (d (n "secrecy") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zvynfdjhaqivjmwncv68iv3rhvh7nsiyyihbmcga71dykfbn4y0")))

