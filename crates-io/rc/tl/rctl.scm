(define-module (crates-io rc tl rctl) #:use-module (crates-io))

(define-public crate-rctl-0.0.1 (c (n "rctl") (v "0.0.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "number_prefix") (r "^0.2") (d #t) (k 0)) (d (n "sysctl") (r "^0.2") (d #t) (k 0)) (d (n "users") (r "^0.7") (d #t) (k 0)))) (h "134r89k993420ka8bl81bwzjncgshi27g8mjrrh8qran6k90bh65")))

(define-public crate-rctl-0.0.2 (c (n "rctl") (v "0.0.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "number_prefix") (r "^0.2") (d #t) (k 0)) (d (n "sysctl") (r "^0.2") (d #t) (k 0)) (d (n "users") (r "^0.7") (d #t) (k 0)))) (h "1xshg7j8b9d5ngpnn09hq3cpax5hf4i3isgzz2j6zmkv3yabcdqq")))

(define-public crate-rctl-0.0.3 (c (n "rctl") (v "0.0.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "number_prefix") (r "^0.2") (d #t) (k 0)) (d (n "sysctl") (r "^0.2") (d #t) (k 0)) (d (n "users") (r "^0.7") (d #t) (k 0)))) (h "1g4lrgk75390di4szi7d8v1gvv87wn25rjkxd6x63ygaw89blp3q")))

(define-public crate-rctl-0.0.4 (c (n "rctl") (v "0.0.4") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "number_prefix") (r "^0.2") (d #t) (k 0)) (d (n "sysctl") (r "^0.2") (d #t) (k 0)) (d (n "users") (r "^0.7") (d #t) (k 0)))) (h "189s0c6qkldnmfdqj4qdr832b37qdxq8xfgk7n9ig39yac8h3qa7")))

(define-public crate-rctl-0.0.5 (c (n "rctl") (v "0.0.5") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "number_prefix") (r "^0.2") (d #t) (k 0)) (d (n "sysctl") (r "^0.2") (d #t) (k 0)) (d (n "users") (r "^0.7") (d #t) (k 0)))) (h "0lwsg1w70vm5gs4sgamx9kqm4cmazpjp3fk8i68192p9dymw3adr")))

(define-public crate-rctl-0.1.0 (c (n "rctl") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "number_prefix") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "sysctl") (r "^0.3") (d #t) (k 0)) (d (n "users") (r "^0.9.1") (d #t) (k 0)))) (h "0k172hr6gvqq80d2saa460k01j8y0amqddf0d36ij3laz28bhk0l") (f (quote (("serialize" "serde" "serde_json"))))))

(define-public crate-rctl-0.2.0 (c (n "rctl") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "number_prefix") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "sysctl") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "19ijg8j2fwb8x504qka3562wg01mvh4amddc2b3y8kpdkiqd5gy6") (f (quote (("serialize" "serde" "serde_json"))))))

