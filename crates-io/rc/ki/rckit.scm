(define-module (crates-io rc ki rckit) #:use-module (crates-io))

(define-public crate-rckit-0.1.0 (c (n "rckit") (v "0.1.0") (d (list (d (n "clap") (r "^2.23.0") (d #t) (k 0)) (d (n "redis") (r "^0.9.0") (d #t) (k 0)))) (h "1i9wfm35ij40wy4lq88ycqhmrhmw6whzpgrhr2singjpyyimhl20")))

