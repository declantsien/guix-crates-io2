(define-module (crates-io wf c- wfc-rs) #:use-module (crates-io))

(define-public crate-wfc-rs-0.1.0 (c (n "wfc-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1v6i034bdv9m22y44zggxr5qqbxpaps4jhpnj9q7lhw6nc0kxqwa") (l "wfc")))

(define-public crate-wfc-rs-0.2.0 (c (n "wfc-rs") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vzqyzjx5nark7njvrpxg8ad6xp98v3paqpjrlp2pl5nasf1jwcz") (l "wfc")))

(define-public crate-wfc-rs-0.3.0 (c (n "wfc-rs") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12pz3mllip9qh64vb1mw2jsw2p5i2h2ygs2vlc7qmsv5x7f9cgp6") (l "wfc")))

(define-public crate-wfc-rs-0.3.1 (c (n "wfc-rs") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zlv7gy9iwhbslpfxn29mmnlkbj0s61z0asd2l3vqgya798bh5iy") (l "wfc")))

(define-public crate-wfc-rs-0.3.2 (c (n "wfc-rs") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1w3b9f1y8mjg394g2zbx36npicg13zgmnh83smbs8g6sjxbab5dg") (l "wfc")))

(define-public crate-wfc-rs-0.4.0 (c (n "wfc-rs") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18j565x5wlxaz3gwsyd07whrn2alkxnpmv894mrl6ls1y2vv8za0") (l "wfc")))

(define-public crate-wfc-rs-0.4.1 (c (n "wfc-rs") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r2hq4a4lnxqyzzrx9kpy8y5p6mwvdk0lx2j1027y9xxnb51ll8p") (l "wfc")))

(define-public crate-wfc-rs-0.4.2 (c (n "wfc-rs") (v "0.4.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "151r3i27kdl8kjyrzc01pz2bpxna12f5prqi3s2j7aq2fccnq8rn") (l "wfc")))

(define-public crate-wfc-rs-0.5.0 (c (n "wfc-rs") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hwli2nzfl463ky6nfwwf3jqhhqq4c65jq0205zhiqa9889chgs2") (l "wfc")))

(define-public crate-wfc-rs-0.5.1 (c (n "wfc-rs") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ki86lq6n64g9br3740icdk67z65qdh7y0gvr261a27x3apy4dsx") (l "wfc")))

(define-public crate-wfc-rs-0.5.2 (c (n "wfc-rs") (v "0.5.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xjhsqjb0sh2yj38bnvf81ad7iw5p08whyhdb2rrcxyvwxzpi04a") (l "wfc")))

(define-public crate-wfc-rs-0.5.3 (c (n "wfc-rs") (v "0.5.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k3i9hjdifg01gfvfb0jxx174nkgv5qy45zvd1353my3mllj529z") (l "wfc")))

(define-public crate-wfc-rs-0.6.0 (c (n "wfc-rs") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "001b4j90cmx04p8j5ggi70v2vk18301gphmdwq0iljghkd8mnynr") (l "wfc")))

(define-public crate-wfc-rs-0.6.1 (c (n "wfc-rs") (v "0.6.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jjn2zg3vil03gfpx85sf0zwyy5586acwnjwzmvdjvfilfmnqkjh") (l "wfc")))

