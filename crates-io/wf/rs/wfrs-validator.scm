(define-module (crates-io wf rs wfrs-validator) #:use-module (crates-io))

(define-public crate-wfrs-validator-0.0.2 (c (n "wfrs-validator") (v "0.0.2") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.2") (d #t) (k 0)))) (h "0bf1nzg09aqxi0c0zhm10vvnmgj9swnh1waa0ckmy2frrxax08cf")))

(define-public crate-wfrs-validator-0.0.3 (c (n "wfrs-validator") (v "0.0.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.3") (d #t) (k 0)))) (h "0ihlac6x1inzzkzlv7mrmbc4hjms0vck8ljj0mxk3y4gdbfdj13d")))

(define-public crate-wfrs-validator-0.0.4 (c (n "wfrs-validator") (v "0.0.4") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.4") (d #t) (k 0)))) (h "0k1shf246hwq638i5hx1346zl2v6x67zxbahr5kj5h4bsci9hvzz")))

(define-public crate-wfrs-validator-0.0.5 (c (n "wfrs-validator") (v "0.0.5") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.5") (d #t) (k 0)))) (h "1ri1569wb9k9yqc6jbdsx61pkpf4hzlwiss2m05kl7vd8r3hkbh7")))

(define-public crate-wfrs-validator-0.0.6 (c (n "wfrs-validator") (v "0.0.6") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.6") (d #t) (k 0)))) (h "1s33h7b6sm8ggiji3dhmm39bx1889z8262q1zj8kraii1qaha7z8")))

(define-public crate-wfrs-validator-0.0.7 (c (n "wfrs-validator") (v "0.0.7") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.7") (d #t) (k 0)))) (h "1qf6zzyhgdcqfys55270wkz0334w6qnhphfak704pq8bg5cqnar2")))

(define-public crate-wfrs-validator-0.0.8 (c (n "wfrs-validator") (v "0.0.8") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.8") (d #t) (k 0)))) (h "07iiqjps07328i7922rx46ifm9nckii5s129xv1qf3mhpw0j09i4")))

(define-public crate-wfrs-validator-0.0.9 (c (n "wfrs-validator") (v "0.0.9") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.9") (d #t) (k 0)))) (h "1m1ylzvdhzxmfz1r9pnx7z2r59wnsc8apxnv4gc1yv5v7w867lw9")))

(define-public crate-wfrs-validator-0.0.10 (c (n "wfrs-validator") (v "0.0.10") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.10") (d #t) (k 0)))) (h "0v86l73kgrpx2m3vvysmj95agbmmpidghfdgxirdb5bw7mwgj7ss")))

(define-public crate-wfrs-validator-0.0.11 (c (n "wfrs-validator") (v "0.0.11") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.11") (d #t) (k 0)))) (h "044gcd4bn59pjgc1wa2y3klg96s9h2ydflhv70nsp2nnjkk5kl26")))

(define-public crate-wfrs-validator-0.0.12 (c (n "wfrs-validator") (v "0.0.12") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.12") (d #t) (k 0)))) (h "1hs4l3ngmw9c508md5qhpr513kgp63mb87y5v4iw6sdp9q4zis7j")))

(define-public crate-wfrs-validator-0.0.13 (c (n "wfrs-validator") (v "0.0.13") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.13") (d #t) (k 0)))) (h "1qdmys5bxaqm3q273qjrhksbsncqz6isw0zsc14xqdyqp0bwj6sh")))

(define-public crate-wfrs-validator-0.0.14 (c (n "wfrs-validator") (v "0.0.14") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.14") (d #t) (k 0)))) (h "0nkcgxnmap40msrbsv2rzsa4lscck5q0acg3h10bshy2v147njml")))

(define-public crate-wfrs-validator-0.0.15 (c (n "wfrs-validator") (v "0.0.15") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.15") (d #t) (k 0)))) (h "12p90vq9bf7nyg2mr2jrmn56dxsjwlkqvxi38cisrrvq20z0hdb3")))

(define-public crate-wfrs-validator-0.0.16 (c (n "wfrs-validator") (v "0.0.16") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.16") (d #t) (k 0)))) (h "10w3lqll3c7nvnsycqj4gh3gf1kg6fa63hg3gphxc27g5ryhpzzb")))

(define-public crate-wfrs-validator-0.0.17 (c (n "wfrs-validator") (v "0.0.17") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.17") (d #t) (k 0)))) (h "15hwkfyjxi4ml8ijafxxcgf7bv4xflddvdda8jwq781gi63sfmsp")))

(define-public crate-wfrs-validator-0.0.18 (c (n "wfrs-validator") (v "0.0.18") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.0.18") (d #t) (k 0)))) (h "0hy622xg7m22llablaa69q4ipgzmscl711h2yy5rna3dhcyk970k")))

(define-public crate-wfrs-validator-0.20.0 (c (n "wfrs-validator") (v "0.20.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.20.0") (d #t) (k 0)))) (h "0w6xgn4kzrh1ywmbvpb2gqhqv259ipimgb2i4r19kwha2jblllfg")))

(define-public crate-wfrs-validator-0.20.1 (c (n "wfrs-validator") (v "0.20.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.20.1") (d #t) (k 0)))) (h "1kxmq0xs61lqv1rhvdhd1mfzim3l0xjdrii75kh157hcsi9ik7nm")))

(define-public crate-wfrs-validator-0.20.2 (c (n "wfrs-validator") (v "0.20.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wfrs-model") (r "^0.20.2") (d #t) (k 0)))) (h "1m5rpmzdni2av1ccvbik6k9rr6r948qyrywczg8kqg1wkin0za9m")))

