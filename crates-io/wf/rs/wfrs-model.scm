(define-module (crates-io wf rs wfrs-model) #:use-module (crates-io))

(define-public crate-wfrs-model-0.0.2 (c (n "wfrs-model") (v "0.0.2") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "0yvp8aab92246vdpd405f7vc0g7y9ypn367f3mbgdrqx57zq3fic")))

(define-public crate-wfrs-model-0.0.3 (c (n "wfrs-model") (v "0.0.3") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "15j9qxii9gkbkrc2la1irj62biqfls5hpn6crzrh80x68a2ni9mm")))

(define-public crate-wfrs-model-0.0.4 (c (n "wfrs-model") (v "0.0.4") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "0sm3l75xblffq7i9b32rn7hmz9zr73fqpbaamzixcw972w1ks2i3")))

(define-public crate-wfrs-model-0.0.5 (c (n "wfrs-model") (v "0.0.5") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "0rshf3iagznkp7s4ii831mjsdvx4i0rnr7dw8n7f7sxjkh02wl5v")))

(define-public crate-wfrs-model-0.0.6 (c (n "wfrs-model") (v "0.0.6") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "0zbkfvy76ir894mi733dlf3k02aj256yl5djddqg4987nim6z1m7")))

(define-public crate-wfrs-model-0.0.7 (c (n "wfrs-model") (v "0.0.7") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "1bwyrvfvhnia8bl9nd9mkx1x0378h15aq8mggwy5p1f6z7a3d4jl")))

(define-public crate-wfrs-model-0.0.8 (c (n "wfrs-model") (v "0.0.8") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "04545arw3xqyz6jgp7sjiqjakjg44r9n554bjzlqg1x4zy655z4z")))

(define-public crate-wfrs-model-0.0.9 (c (n "wfrs-model") (v "0.0.9") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "12ldmh57lwn93zp62gcrq20l1bmr1x29h05wdw0xddqc961qkrkp")))

(define-public crate-wfrs-model-0.0.10 (c (n "wfrs-model") (v "0.0.10") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "1yxcxrb6i7c3h3zzg39ijmaww5c70n30ipgb66ry9m1z43kzx41h")))

(define-public crate-wfrs-model-0.0.11 (c (n "wfrs-model") (v "0.0.11") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "0s0hf7bk75wl93z529miffnp0sly5w65fdssja1h3mz8j9d9m0g4")))

(define-public crate-wfrs-model-0.0.12 (c (n "wfrs-model") (v "0.0.12") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "0qhg72hf69d8ijmncbwck344bqw4hd7fvr36n56wr34znf1kcgrk")))

(define-public crate-wfrs-model-0.0.13 (c (n "wfrs-model") (v "0.0.13") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "1slrsl1nd3cbg8vamyjm0jma3d9pxs1aqh6kgs6lxqvszy04v54v")))

(define-public crate-wfrs-model-0.0.14 (c (n "wfrs-model") (v "0.0.14") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "0l0s8slynz3w5g8d5jdrcbbgpil89rv1ni4i73wlk8053gjy64an")))

(define-public crate-wfrs-model-0.0.15 (c (n "wfrs-model") (v "0.0.15") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "091hianiisl43n1fbq56vbl7p8ni04j3rn4c2vxi885b2vkp986g")))

(define-public crate-wfrs-model-0.0.16 (c (n "wfrs-model") (v "0.0.16") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "08a6026rx4173ldz308xw3jd4wyiy3czj9lv1y5jn8gp5p0bavfm")))

(define-public crate-wfrs-model-0.0.17 (c (n "wfrs-model") (v "0.0.17") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "01x0hsvlcjjqm4gr788i4p5hqklhzghigff2y1aidyc6ma533cv0")))

(define-public crate-wfrs-model-0.0.18 (c (n "wfrs-model") (v "0.0.18") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "0cnhbig1mwimp8dz5lvaqmib20ix9prrsfrmwqvkavpi6zqcbids")))

(define-public crate-wfrs-model-0.20.0 (c (n "wfrs-model") (v "0.20.0") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "145c0g5lqjs9zyyh7phdqm7n7091cn04r16jq2mv1h6g0rqbk2h0")))

(define-public crate-wfrs-model-0.20.1 (c (n "wfrs-model") (v "0.20.1") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "1dkpr9zw17wdm1nc9i9zd6wrvj80rpkpzjdpipscj7ix654n4gv7")))

(define-public crate-wfrs-model-0.20.2 (c (n "wfrs-model") (v "0.20.2") (d (list (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)))) (h "1j7dpxf0w7s2c0q2xgf19i1aickxz99zinniaz36m4hwzqzqpzzg")))

