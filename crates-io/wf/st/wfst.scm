(define-module (crates-io wf st wfst) #:use-module (crates-io))

(define-public crate-wfst-0.1.0 (c (n "wfst") (v "0.1.0") (d (list (d (n "bincode") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0inqn1y2qz1q9c8fx9v3plbd3k44rivffym2qdd2z5fcss9i7c5s")))

(define-public crate-wfst-0.2.0 (c (n "wfst") (v "0.2.0") (d (list (d (n "bincode") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1rg9y6qbb6sadqh3m2ays9x7m5hghpgggv7bwgms11ipnj6psk3g")))

(define-public crate-wfst-0.3.0 (c (n "wfst") (v "0.3.0") (d (list (d (n "bincode") (r "^0.6.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1adsz9fymkmqxa6vf87zvjaff4ybpc02w0wb93vdb7415s45l3lg")))

(define-public crate-wfst-0.4.0 (c (n "wfst") (v "0.4.0") (d (list (d (n "bincode") (r "^0.6.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "057g3vbkvvafifci749cn67wgg284gh7pbhy5c8h9pvb9j0rvjwd")))

(define-public crate-wfst-0.5.0 (c (n "wfst") (v "0.5.0") (d (list (d (n "bincode") (r "^0.6.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0k1ykl859windx51x3anzli4igxi6nd9ipnf8cvr5mgp13qh79l3")))

(define-public crate-wfst-0.6.0 (c (n "wfst") (v "0.6.0") (d (list (d (n "bincode") (r "^0.6.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0z8qni6x5cq7irf7kfw6n3ycjy56j3zkrm66rd4kqfjh2pl48win")))

