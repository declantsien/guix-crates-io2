(define-module (crates-io wf st wfst4str) #:use-module (crates-io))

(define-public crate-wfst4str-1.0.1 (c (n "wfst4str") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rustfst") (r "^0.9.0") (d #t) (k 0)))) (h "1jp1396s576sa4q581cq97g1jk6xss1yfj5ddg5ilp3ra0f5qzm1")))

(define-public crate-wfst4str-1.0.2 (c (n "wfst4str") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rustfst") (r "^0.9.0") (d #t) (k 0)))) (h "1yl3v1iy7vlq61k7dxg5jsdd11f0wk7nicw7amhfvc8dd2d3i7vr")))

(define-public crate-wfst4str-1.0.4 (c (n "wfst4str") (v "1.0.4") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rustfst") (r "^0.9.0") (d #t) (k 0)))) (h "10inipd6rm4z5ijg2jkikd431rlzr84xbqrf8nfih1mb5rj3g1xa")))

