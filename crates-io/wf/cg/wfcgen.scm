(define-module (crates-io wf cg wfcgen) #:use-module (crates-io))

(define-public crate-wfcgen-0.1.0 (c (n "wfcgen") (v "0.1.0") (d (list (d (n "fastwfc") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "10p1057jgckqwlxcydirm918bbn0hr2hvzklfzlij6rwnrx2zf72")))

