(define-module (crates-io wf bu wfbuf) #:use-module (crates-io))

(define-public crate-wfbuf-0.1.0 (c (n "wfbuf") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "^0.1") (d #t) (k 0)))) (h "1782zyyy40hcc9lgr0jb4nmf11mhdawhzrr499pn8pf5yrzaxv40")))

(define-public crate-wfbuf-0.1.1 (c (n "wfbuf") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "^0.1") (d #t) (k 0)))) (h "0i3scrwq7iqxmhxlprm8jldm83z77wixbghvv3bi5c99qx6nv09h")))

