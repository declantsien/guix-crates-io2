(define-module (crates-io wf c_ wfc_voxel) #:use-module (crates-io))

(define-public crate-wfc_voxel-0.1.0 (c (n "wfc_voxel") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0z5j78xan6ciirkqxz165brzqj5nhfcqzqf93m40zpr7ac0d9clv")))

(define-public crate-wfc_voxel-0.1.1 (c (n "wfc_voxel") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0lw18qqakgj81jm51b5d44wgr19mxrjf2pqwwvjd02y0zlig8jsc")))

(define-public crate-wfc_voxel-0.1.2 (c (n "wfc_voxel") (v "0.1.2") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dwr464d4lgpxpyasqcfyp88k3md0gm492gyznhj3ixmjn8nwv2d")))

(define-public crate-wfc_voxel-0.1.3 (c (n "wfc_voxel") (v "0.1.3") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0h5fd3xvkc70wi2c2dikc0pvzgz2xhkj4mbk28h11vxsj6yb8bf5")))

(define-public crate-wfc_voxel-0.1.4 (c (n "wfc_voxel") (v "0.1.4") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1x79s2x5d8wna7541zxyqw4bg2wjylan2pkfcidwz6xs2xsap214")))

(define-public crate-wfc_voxel-0.1.5 (c (n "wfc_voxel") (v "0.1.5") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1cw1d9yhqxar8bc329mb6m6lgyzn9r8kwjw51jdrc5q1gj8v211a")))

(define-public crate-wfc_voxel-0.2.0 (c (n "wfc_voxel") (v "0.2.0") (d (list (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15n5jifz8np3a084in65fvap5bjrwq5cn15xk7qy7yq574fgijln")))

