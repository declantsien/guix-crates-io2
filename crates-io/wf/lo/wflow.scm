(define-module (crates-io wf lo wflow) #:use-module (crates-io))

(define-public crate-wflow-0.1.0 (c (n "wflow") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli_printer") (r "^0.2.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "1g0sjw9wriymqgdykg0mcd8wgy9xbdiq9jxmkgh53d9lw5gqy2fq")))

