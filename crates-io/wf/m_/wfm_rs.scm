(define-module (crates-io wf m_ wfm_rs) #:use-module (crates-io))

(define-public crate-wfm_rs-0.1.0 (c (n "wfm_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0sy1l1c2x11z3y818swzxl2jwl13pbwfd11alqkp3x8vfhprvi65")))

(define-public crate-wfm_rs-0.1.1 (c (n "wfm_rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1z1yvjcg7nbfm2pyw9gx8z458ljy99gawmk1g8mf5av4j50r06f0")))

