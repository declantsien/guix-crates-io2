(define-module (crates-io wf qu wfqueue) #:use-module (crates-io))

(define-public crate-wfqueue-0.1.0 (c (n "wfqueue") (v "0.1.0") (d (list (d (n "cache-padded") (r "^1") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 2)) (d (n "loom") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0fw3hw33pb8vinr9ikf15j9xbbr86bad0df6i840jqnlqxiyndrf") (y #t)))

(define-public crate-wfqueue-0.1.1 (c (n "wfqueue") (v "0.1.1") (d (list (d (n "cache-padded") (r "^1") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 2)) (d (n "loom") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "12h541l85qnkvn9ml49x1k7jka69vjkp19cip20ssp5s0zbjnc5q") (y #t)))

(define-public crate-wfqueue-0.2.0 (c (n "wfqueue") (v "0.2.0") (d (list (d (n "cache-padded") (r "^1") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 2)) (d (n "loom") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "per-thread-object") (r "^0.4") (d #t) (k 0)))) (h "17i16i1k0ccjdnhbhk4flijaph5rh5gd3qmhz8i181lrzq75qh9a")))

(define-public crate-wfqueue-0.2.1 (c (n "wfqueue") (v "0.2.1") (d (list (d (n "cache-padded") (r "^1") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.2") (d #t) (k 2)) (d (n "loom") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "per-thread-object") (r "^0.4") (d #t) (k 0)))) (h "0nsb7l97zbpiyjrdizby7vx83xfv7xrcmv4qfkcvg6xczgih0gp7")))

