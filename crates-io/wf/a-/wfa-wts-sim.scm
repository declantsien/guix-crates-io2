(define-module (crates-io wf a- wfa-wts-sim) #:use-module (crates-io))

(define-public crate-wfa-wts-sim-0.1.0 (c (n "wfa-wts-sim") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mockstream") (r "^0.0.3") (d #t) (k 0)))) (h "0zw9v1qzc0pvclx19xhd4s7l261q3ma39763a6qqw6mlsdgp1c4g")))

