(define-module (crates-io wf ir wfir) #:use-module (crates-io))

(define-public crate-wfir-0.1.0 (c (n "wfir") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.16") (d #t) (k 0)))) (h "1zjh1fz6ldc6nra6r5plhzhv9gqiv6smmfsm50q6mdc2jayswryl")))

(define-public crate-wfir-0.1.1 (c (n "wfir") (v "0.1.1") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.16") (d #t) (k 0)))) (h "03kvd8m7jbdz6ninc71md8is6fzdjcjsk04p531m5slbiac0f8mv")))

(define-public crate-wfir-0.1.2 (c (n "wfir") (v "0.1.2") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.16") (d #t) (k 0)))) (h "0v3hvrdr5h0xd5y69l7cdbvriixj4p34w7likzgsnblq28vrbg9s")))

