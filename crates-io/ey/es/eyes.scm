(define-module (crates-io ey es eyes) #:use-module (crates-io))

(define-public crate-eyes-1.0.0 (c (n "eyes") (v "1.0.0") (h "1zi5xsbvc79nn1157xi99k0r6w0l949x9yz3ln8dk3izvyv0ps4p") (y #t)))

(define-public crate-eyes-1.0.1 (c (n "eyes") (v "1.0.1") (h "1n70pn4zyfc40c34dyqinb8vv1vs3irzwp2iw6svvmbnf1blaxfc") (y #t)))

(define-public crate-eyes-1.1.0 (c (n "eyes") (v "1.1.0") (h "0lb9idcq9n6j67jmyp0by9z5cwrl8jlp5ayklcwvdj5avgzfvz5n") (y #t)))

(define-public crate-eyes-1.1.1 (c (n "eyes") (v "1.1.1") (h "0ip2czd4d2ysmxab9d3w5aj1w2gfsnby8nigm4l4ndv8aly7f1ac") (y #t)))

(define-public crate-eyes-1.1.2 (c (n "eyes") (v "1.1.2") (h "1qmxphizcs8ny3adnym2lifjhsspbc9hqsg1i6iy8mx1j4vhc37r")))

(define-public crate-eyes-1.2.0 (c (n "eyes") (v "1.2.0") (h "18q65j07va7ln82gk1vcgc8sb8hgdlrkz3vdp1kvccqy4xda8bbj")))

(define-public crate-eyes-1.2.1 (c (n "eyes") (v "1.2.1") (h "02x6z6g0dj7n38r6s8y4a5g3x454x13c2rk71fszkvk8n916ki10")))

(define-public crate-eyes-1.3.0 (c (n "eyes") (v "1.3.0") (h "11dr4p4pmb4ybp343w9vyxpxlsy708skfcrbkmp257qq770x57zs")))

