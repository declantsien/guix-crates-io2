(define-module (crates-io ey ec eyecite) #:use-module (crates-io))

(define-public crate-eyecite-0.0.0 (c (n "eyecite") (v "0.0.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "prepush-hook" "run-cargo-test" "run-cargo-clippy" "run-cargo-fmt"))) (k 2)) (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "daachorse") (r "^0.4.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reporters-db") (r "^0.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "100d1swrf73fd1nzcnnjl0ajmdhp0zlsixj53ifc6my59ky9c9p9")))

