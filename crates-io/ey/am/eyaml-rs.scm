(define-module (crates-io ey am eyaml-rs) #:use-module (crates-io))

(define-public crate-eyaml-rs-0.1.1 (c (n "eyaml-rs") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml" "color" "suggestions"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "yaml-rust") (r ">=0.4.1") (d #t) (k 0)))) (h "0zgbxjjp6z19kqvvaydn7l9mh1rvsbpv1qq74x9dbzvq5l96zm6i")))

