(define-module (crates-io ey re eyre-span) #:use-module (crates-io))

(define-public crate-eyre-span-0.1.0 (c (n "eyre-span") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1pf5fzix43q4sn5bimw3ysw715fn2xa0ia8hcx22431lddmfklw6") (f (quote (("default" "tracing-error"))))))

