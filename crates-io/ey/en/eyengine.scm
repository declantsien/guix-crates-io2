(define-module (crates-io ey en eyengine) #:use-module (crates-io))

(define-public crate-eyengine-0.1.0 (c (n "eyengine") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "legion") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 1)) (d (n "wgpu") (r "^0.12") (f (quote ("spirv"))) (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)))) (h "1iwrk3y7qvjr56wnhl73jc3vmvmjaa8s2pki4rlcbggsqdv42d37")))

