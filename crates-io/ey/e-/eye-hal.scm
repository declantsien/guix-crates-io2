(define-module (crates-io ey e- eye-hal) #:use-module (crates-io))

(define-public crate-eye-hal-0.1.0 (c (n "eye-hal") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 2)) (d (n "uvc") (r "^0.2.0") (f (quote ("vendor"))) (o #t) (d #t) (k 0)) (d (n "v4l") (r "^0.12.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0avshv20iav27mjcaj0zn6f1slq4qw70408k1v75wjrm1lifva3b") (f (quote (("plat-uvc" "uvc"))))))

(define-public crate-eye-hal-0.2.0 (c (n "eye-hal") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "openpnp_capture") (r "^0.2.4") (d #t) (t "cfg(target_os=\"macos\")") (k 0)) (d (n "openpnp_capture_sys") (r "^0.4.0") (d #t) (t "cfg(target_os=\"macos\")") (k 0)) (d (n "uvc") (r "^0.2.0") (f (quote ("vendor"))) (d #t) (t "cfg(target_os=\"windows\")") (k 0)) (d (n "v4l") (r "^0.14.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1l46k0rxkjgyiq6yn4zdfnsfg7zdhjmzi8m91jss1j6qp45lpscf")))

