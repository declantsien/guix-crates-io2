(define-module (crates-io ey tz eytzinger) #:use-module (crates-io))

(define-public crate-eytzinger-1.0.0 (c (n "eytzinger") (v "1.0.0") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "14dcwpbznbiyi8hy96hpcmf1b28sk1rz53vbxvl6vr2s00w8xclc")))

(define-public crate-eytzinger-1.0.1 (c (n "eytzinger") (v "1.0.1") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "06q52nm38596jaiv7nb6qhv1nyj1v61p52d5l82l14cbg0i5ij1z") (f (quote (("branchless"))))))

(define-public crate-eytzinger-1.1.0 (c (n "eytzinger") (v "1.1.0") (d (list (d (n "nohash-hasher") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "nonmax") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0n1lm15h7hrpi3793h7pfh3bkp3rlc9qlildiy1l7i5dacahxmpv") (f (quote (("heap-permutator-sparse" "nohash-hasher") ("heap-permutator" "nonmax") ("default" "heap-permutator" "heap-permutator-sparse") ("branchless"))))))

(define-public crate-eytzinger-1.1.1 (c (n "eytzinger") (v "1.1.1") (d (list (d (n "nohash-hasher") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "nonmax") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0jy3c98n98jshxh1vw39v8xq25wd9xgdfpbq4m5k28vb2p73bvlc") (f (quote (("heap-permutator-sparse" "nohash-hasher") ("heap-permutator" "nonmax") ("default" "heap-permutator" "heap-permutator-sparse") ("branchless"))))))

