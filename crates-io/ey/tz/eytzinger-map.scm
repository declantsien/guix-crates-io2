(define-module (crates-io ey tz eytzinger-map) #:use-module (crates-io))

(define-public crate-eytzinger-map-0.1.0 (c (n "eytzinger-map") (v "0.1.0") (d (list (d (n "eytzinger") (r "^1") (d #t) (k 0)))) (h "1sb5zxrnr06vphbgl865kgddkgdydq7a62r7rwn3v8crb26bbsbw")))

(define-public crate-eytzinger-map-0.1.1 (c (n "eytzinger-map") (v "0.1.1") (d (list (d (n "eytzinger") (r "^1") (d #t) (k 0)))) (h "1jmvszd7ym52yqakdks1jlanjkjgrmwfhdk0dfqwz30f9y52b2rb")))

