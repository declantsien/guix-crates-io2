(define-module (crates-io pi de pide-rs) #:use-module (crates-io))

(define-public crate-pide-rs-0.2.0 (c (n "pide-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1rybh1q3c9kwxaw3kxzq6yb1y8xi579vn422mr0csmrvv9ipykkc")))

(define-public crate-pide-rs-0.2.1 (c (n "pide-rs") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1m5spv42mdkb669qa14gkc8nyq34xp191ijny80mld8c3ssl3dfp")))

(define-public crate-pide-rs-0.2.2 (c (n "pide-rs") (v "0.2.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0wqfblaqj348w7b1191qfd54wd1rb7dvlc9b885i2hg7i1qkx83b")))

