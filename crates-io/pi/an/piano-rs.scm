(define-module (crates-io pi an piano-rs) #:use-module (crates-io))

(define-public crate-piano-rs-0.1.0 (c (n "piano-rs") (v "0.1.0") (d (list (d (n "rodio") (r "^0.5") (d #t) (k 0)) (d (n "rustbox") (r "^0.9") (d #t) (k 0)))) (h "1dqkrm2lxaa2iz32c792njp1cb6fa86gs2kyasrmjha6yczmalp0")))

