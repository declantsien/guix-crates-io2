(define-module (crates-io pi an piano_keyboard) #:use-module (crates-io))

(define-public crate-piano_keyboard-0.1.0 (c (n "piano_keyboard") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 2)) (d (n "png") (r "^0.14.0") (d #t) (k 2)))) (h "1n663cv9x8sx89xsp0ysyc21hz3d8p637ynyc495gc1s8xs5wmcf")))

(define-public crate-piano_keyboard-0.1.1 (c (n "piano_keyboard") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 2)) (d (n "png") (r "^0.14.0") (d #t) (k 2)))) (h "0h3fcpw6kih63knnswgg71rxhgbvkxckxbzhk77nvf03vb9bak4j")))

(define-public crate-piano_keyboard-0.2.2 (c (n "piano_keyboard") (v "0.2.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 2)) (d (n "png") (r "^0.14.0") (d #t) (k 2)))) (h "1w3adqk3zaqbiggl0mwc0cpngnkshhrckyy596jp3rqa1x17ym46")))

(define-public crate-piano_keyboard-0.2.3 (c (n "piano_keyboard") (v "0.2.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 2)) (d (n "png") (r "^0.14.0") (d #t) (k 2)))) (h "1ixynwmls14k4bd2k2b5lr1b466nm79mng5xdfhrqfc2ynvbqfji")))

