(define-module (crates-io pi _o pi_ordmap) #:use-module (crates-io))

(define-public crate-pi_ordmap-0.1.0 (c (n "pi_ordmap") (v "0.1.0") (h "0w6ijgbkj19i5nkwprnjb4sryy5z7wfyi6813d80cmn3syd204n9")))

(define-public crate-pi_ordmap-0.2.0 (c (n "pi_ordmap") (v "0.2.0") (h "15hs5s35hp02a586l793nz503g4i28ni3l74m8zwdrsr28zbbny1")))

(define-public crate-pi_ordmap-0.2.1 (c (n "pi_ordmap") (v "0.2.1") (h "1cxsxjynm5vdq91rh39y99yqlh3px69znhd8bp28zc0smr7lyw6c")))

(define-public crate-pi_ordmap-0.2.2 (c (n "pi_ordmap") (v "0.2.2") (h "0yrrffv5qa8f7gibyy2d2l60ip7c0j0lbsi77ngs6isz91lk2ldq")))

