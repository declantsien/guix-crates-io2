(define-module (crates-io pi zz pizzicato) #:use-module (crates-io))

(define-public crate-pizzicato-0.1.0 (c (n "pizzicato") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "noise") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rsmpeg") (r "^0.7") (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cm4g7as5bv2g1x93cxxhw4f423lk7bsw0kzsfn54zb8rd9vhiby")))

