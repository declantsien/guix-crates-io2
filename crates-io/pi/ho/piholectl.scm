(define-module (crates-io pi ho piholectl) #:use-module (crates-io))

(define-public crate-piholectl-0.1.0 (c (n "piholectl") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "pi-hole-api") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0vg4i2x2gr4hdv0qfynaqksxyg076x66lypyqvrmdy5jglwzqaiz")))

(define-public crate-piholectl-0.2.0 (c (n "piholectl") (v "0.2.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "pi-hole-api") (r "^0.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "05r73jfy353nhi7cw5jsmarmpq3sr65qf9sr97lpaw5sgl86cdl9")))

