(define-module (crates-io pi ho pihole-influx-agent) #:use-module (crates-io))

(define-public crate-pihole-influx-agent-0.1.0 (c (n "pihole-influx-agent") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "blocking" "json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0cggn6p4bhghsl6h7zqfjnsf5dzzvrlb37fz13ibrw1z3q6kgcad") (y #t)))

(define-public crate-pihole-influx-agent-0.1.1 (c (n "pihole-influx-agent") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "blocking" "json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14rrq5g7jdnrr36461hsmsx4byl4rn1kis39n7rh465ww8q4bghm")))

