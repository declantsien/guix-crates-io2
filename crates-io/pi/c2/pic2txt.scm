(define-module (crates-io pi c2 pic2txt) #:use-module (crates-io))

(define-public crate-pic2txt-1.0.0 (c (n "pic2txt") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Media_Ocr" "Graphics_Imaging" "Storage_Streams" "Foundation"))) (d #t) (k 0)))) (h "083aia6kl5jg53bf9469mdgkzcqvwgaj69d85sma1qdaippgk7cw")))

