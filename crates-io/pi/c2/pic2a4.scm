(define-module (crates-io pi c2 pic2a4) #:use-module (crates-io))

(define-public crate-pic2a4-0.1.0 (c (n "pic2a4") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "printpdf") (r "^0.5") (f (quote ("embedded_images"))) (d #t) (k 0)))) (h "0zjwc0kifxrnfnpqw18am8w67862zaqvvj7i24jkim049vqvvxpj")))

