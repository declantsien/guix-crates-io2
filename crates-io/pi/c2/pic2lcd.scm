(define-module (crates-io pi c2 pic2lcd) #:use-module (crates-io))

(define-public crate-pic2lcd-0.1.0 (c (n "pic2lcd") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "116fi4i6f8zi689i0g79x0bpcf9wy2zb21ns94wjxin40128cdpn")))

