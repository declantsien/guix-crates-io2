(define-module (crates-io pi za pizarra) #:use-module (crates-io))

(define-public crate-pizarra-0.6.1 (c (n "pizarra") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0jgpf42p1rfddgcq69zppssq3jzbj70kmi6bvdax5s3bvlzvfygn")))

(define-public crate-pizarra-0.6.2 (c (n "pizarra") (v "0.6.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "098ia8rm196j7cz4wn6wnza307hnlj77d5hcmvwak1q1sxix2ym1")))

(define-public crate-pizarra-0.6.3 (c (n "pizarra") (v "0.6.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0fqnf23gznxzg4qs3p5dkizvapd07w9v0fa994q9k1h1gxnw6cip")))

(define-public crate-pizarra-0.6.4 (c (n "pizarra") (v "0.6.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "16wq190yd17l4h40qfv0lvfi0yv00izyb6mcxhyzqqkdn2bbkn5a")))

(define-public crate-pizarra-0.6.5 (c (n "pizarra") (v "0.6.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1yimndslv4kc8j0g5w5xbi1bxjkyspk5acrgjp8p8pni6vkng3k5")))

(define-public crate-pizarra-0.7.0 (c (n "pizarra") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "02qjx7gl438054qy9qcbbyz2a389wrci9crjy7fm78s1am10n8ff")))

(define-public crate-pizarra-0.7.1 (c (n "pizarra") (v "0.7.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1dkm7winr9v293zpfgfrgaav9j16161hd6dsq0nsycf2cd36njkm")))

(define-public crate-pizarra-0.7.2 (c (n "pizarra") (v "0.7.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0cz1jrk2v6qh85xgxkc6q91kk0fmrwvg6jmmsfj57hladk86krkh")))

(define-public crate-pizarra-0.7.3 (c (n "pizarra") (v "0.7.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "15bcndj6g0w3rls484xy9cxczyr2245b07dydndww8517r78nvwy")))

(define-public crate-pizarra-0.8.0 (c (n "pizarra") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1lc75h77wwdzx0lkgq2b06lg2p7ldb0hfjhabark0x4zafhm4n2f")))

(define-public crate-pizarra-0.8.1 (c (n "pizarra") (v "0.8.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "009ngg0lpivb8gxh51i4g3piaggvg1s6mrrcnm0aq5amccjynji8")))

(define-public crate-pizarra-0.8.2 (c (n "pizarra") (v "0.8.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0w8fjv21wg1csrqsffns87f0rhwfys6fqpxhzp92pya171wkylq5")))

(define-public crate-pizarra-0.9.0 (c (n "pizarra") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "02z4gjsy80ihhgvd7wl38i2ic6kw739d2frc387dsrwyqxfbn5q6")))

(define-public crate-pizarra-0.9.1 (c (n "pizarra") (v "0.9.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1qbhpkvlijxfdc1nrrwdmx1xwmg36lq3hhj71n83lrf9gffnvy62")))

(define-public crate-pizarra-0.9.2 (c (n "pizarra") (v "0.9.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0ghi69cnafqa7rcy7l4jdlqs4zymcwjnn23is7hrngm1c8kg00sm")))

(define-public crate-pizarra-0.9.3 (c (n "pizarra") (v "0.9.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0naprf8372dsq5amf71is26f0k41mkbnfj60ivm4sv0rg5ik4wyh")))

(define-public crate-pizarra-0.10.0 (c (n "pizarra") (v "0.10.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "087h5lc5r30iyl2m64wbyglnwb4bhihsqwsn8rlqw27gwi2h7gyl")))

(define-public crate-pizarra-0.11.0 (c (n "pizarra") (v "0.11.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "02l4y0ggqnsiii0c6j58h8mn11rdmahyhv21hkb3id5j0qld7nas")))

(define-public crate-pizarra-1.0.0 (c (n "pizarra") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1ia8xpysn3zv1f5lm3yybv6iagwspnijf7zjzqb9d2ir9prxdkyx")))

(define-public crate-pizarra-1.1.0 (c (n "pizarra") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0ihr9vriw87yfgmr8s5kv72kqlrm6rw8yz9d1bfmzgxla79qan4z")))

(define-public crate-pizarra-1.1.1 (c (n "pizarra") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0yqna0jpwphw627h5biw372l7kkjr1d596k41b16xl1yk49spfrl")))

(define-public crate-pizarra-1.1.2 (c (n "pizarra") (v "1.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "08f0ir8wwr2cch9l3x4qiyzx3bsib8fm7cwzv226a6dim2drkf58")))

(define-public crate-pizarra-1.1.3 (c (n "pizarra") (v "1.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1f6vy62qr507ynq4g8afzmdmx4014r5sf7c0y04rjqigmyanzwbp")))

(define-public crate-pizarra-1.1.4 (c (n "pizarra") (v "1.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1jlci5d9684w74g8552fj4m7c9pxxna51kx2w78g5q0vk30bjd0m")))

(define-public crate-pizarra-1.1.5 (c (n "pizarra") (v "1.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0mf2b6l6j47jh9flka8h58b1zz1w2mabw03w7gg00w3gzrc6gz3r")))

(define-public crate-pizarra-1.1.6 (c (n "pizarra") (v "1.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1pgyllrb2ak8dbnv52qy6alhrfkr041wnpvmlz4ra6nbhhkgq8rz")))

(define-public crate-pizarra-1.2.0 (c (n "pizarra") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0rvcx013s3yrxsbb2a97avmkn7qgyspbqipc4zjirq09dbr0phml")))

(define-public crate-pizarra-1.2.1 (c (n "pizarra") (v "1.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1rxwakwg6wihdpg6vwzg087y6cmyclagqc4iphx1saqab9xjizqx")))

(define-public crate-pizarra-1.3.0 (c (n "pizarra") (v "1.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "12nv6dh61bnzh4yn5mfh7y5yy87d453p2mzrl636dm5ahsx1l00p")))

(define-public crate-pizarra-1.4.0 (c (n "pizarra") (v "1.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1xa1ncqdy19qc7jmiy5bifgy0n0l6fbs10vz4spj77mql4azkycl")))

(define-public crate-pizarra-1.5.0 (c (n "pizarra") (v "1.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "13d4gk5qv2mz4i65bg107mn8qyb2y36idk9wrwdgn1p2a1bi4qvv")))

(define-public crate-pizarra-1.6.0 (c (n "pizarra") (v "1.6.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1n81d9mq8yw86cifm9lr63rk8yw9c2ybwz0xk8iwndpmk6l60s85")))

(define-public crate-pizarra-1.7.0 (c (n "pizarra") (v "1.7.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "10q3pqiaj48vhhwvpilb4a5w0p8cy6c6dffklvwqx9bd8gi41gzg")))

(define-public crate-pizarra-1.7.1 (c (n "pizarra") (v "1.7.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "18indwazrzw374n1kvnsyxbnbidfh8q0qk9mww9igxlc79xg5zj9")))

(define-public crate-pizarra-1.8.0 (c (n "pizarra") (v "1.8.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "03xgc2mhxsiypnqc58dzs3m6llianb2bx6b9vkags41cgq0g8ss2")))

(define-public crate-pizarra-1.9.0 (c (n "pizarra") (v "1.9.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "084q574ddk82knpn4s48bidi1zjbxrjm9d616cmr53p147c8jrk4")))

(define-public crate-pizarra-1.10.0 (c (n "pizarra") (v "1.10.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "028zprapdijirmnl9mqa4gkfd0ga6nyqq0n40db67h6a3799dbn5")))

(define-public crate-pizarra-1.10.1 (c (n "pizarra") (v "1.10.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1047ijv1qj3bwdcaph6fdydfjfaqw6il5bqfi8vz2mg0lja8xbr4")))

(define-public crate-pizarra-2.0.0 (c (n "pizarra") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "16i4z1j4kk1fc9660asq9wpca2xl0vgrbsv15hnai2x59dfcr54y")))

(define-public crate-pizarra-2.0.1 (c (n "pizarra") (v "2.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "02qgrffjpa110z0ajv4gs5rgx0lln8sycl9fzf8zgy27njjdh03n")))

(define-public crate-pizarra-2.0.2 (c (n "pizarra") (v "2.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "16s3wnq9bxr15idhhcrq8l2z6pp8lls13cj95mzl0x6kbacj1xay")))

(define-public crate-pizarra-2.0.3 (c (n "pizarra") (v "2.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "14qr0xwcrjhsxlqnvlsklbcswr1srndaq7lbp781li10kdgynb9n")))

(define-public crate-pizarra-2.0.4 (c (n "pizarra") (v "2.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1q0z629raq2c2jn84p0nqdwia04lkr9b3dn1r9xh4g82g751ii3s")))

(define-public crate-pizarra-3.0.0 (c (n "pizarra") (v "3.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rstar") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)))) (h "1v5l9npzvgkq9g7zv1isvszzi2mdwl9wk57k6f543kv5113slij8")))

