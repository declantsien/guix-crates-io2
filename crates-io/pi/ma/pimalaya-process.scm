(define-module (crates-io pi ma pimalaya-process) #:use-module (crates-io))

(define-public crate-pimalaya-process-0.0.1 (c (n "pimalaya-process") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m76y3gjl1snpav9mlx5s8n7y95sg3qj7blvpr9wqwf0hwipinm0")))

(define-public crate-pimalaya-process-0.0.2 (c (n "pimalaya-process") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0skjwblg9xin55zbw2ncdm7ld9skdm7bn8nfjzizn3igpx9ji3cd")))

(define-public crate-pimalaya-process-0.0.3 (c (n "pimalaya-process") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("io-util" "process"))) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("macros" "rt"))) (k 2)))) (h "1w2cqf55hw5d1191yj6nihm413g3gyf0cp3lmc9c9qafn1hlfqsi")))

(define-public crate-pimalaya-process-0.0.4 (c (n "pimalaya-process") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("io-util" "process"))) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("macros" "rt"))) (k 2)))) (h "1dffx3yj0lzpnkvysqlmsqakybxil8y493qycc1971xisnn189hx")))

(define-public crate-pimalaya-process-0.0.5 (c (n "pimalaya-process") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("io-util" "process"))) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("macros" "rt"))) (k 2)))) (h "072xpbyxybqd38yqyj5isz9fad7d7xgxcg310r6lwk9knqd1x78s")))

(define-public crate-pimalaya-process-0.1.0 (c (n "pimalaya-process") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("io-util" "process"))) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("macros" "rt"))) (k 2)))) (h "19k2hg41v558xxj74qkjzfdjxz8dxhrhqgpx5dcw4rm0ir31qkf8")))

