(define-module (crates-io pi ma pimalaya-oauth2) #:use-module (crates-io))

(define-public crate-pimalaya-oauth2-0.0.1 (c (n "pimalaya-oauth2") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oauth2") (r "^4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1d17nbykvpq868sxvm05axyj19xb8clsp0xrw4cq6gdqqxih096p")))

(define-public crate-pimalaya-oauth2-0.0.2 (c (n "pimalaya-oauth2") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oauth2") (r "^4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "18gqng9vwcikqzi65qb4wq4fcsbdv3ijfir4z97v3dk25ckc9gn9")))

(define-public crate-pimalaya-oauth2-0.0.3 (c (n "pimalaya-oauth2") (v "0.0.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oauth2") (r "^4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1s4c2y586rly0x35jyf27yyk5pjjmsznkxs6pla4yc2nkvdb6a3a")))

(define-public crate-pimalaya-oauth2-0.0.4 (c (n "pimalaya-oauth2") (v "0.0.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oauth2") (r "^4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("io-util" "net"))) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("macros" "rt"))) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1jkjkcn11z4pz96b73d7ngsalp6dmq1ypwps23b2wbsvq0k16yy4")))

(define-public crate-pimalaya-oauth2-0.1.0 (c (n "pimalaya-oauth2") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oauth2") (r "^4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("io-util" "net"))) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("macros" "rt"))) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "11s9azn7i2s0rz0sdwachsk7pr4xfxxj7fh1k4ymwjpgn4x2zfy7")))

