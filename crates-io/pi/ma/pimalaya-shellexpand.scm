(define-module (crates-io pi ma pimalaya-shellexpand) #:use-module (crates-io))

(define-public crate-pimalaya-shellexpand-0.1.0 (c (n "pimalaya-shellexpand") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "shellexpand_native") (r "^3.1") (f (quote ("full"))) (k 0) (p "shellexpand")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rmlpiwfhigdlyz3jhqini82vpx58n8wdi2n6lq811rx7hp2l04m")))

(define-public crate-pimalaya-shellexpand-0.1.1 (c (n "pimalaya-shellexpand") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "shellexpand_native") (r "^3.1") (f (quote ("full"))) (k 0) (p "shellexpand")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ba8vjnv1aayz5cq9iyn964mczkag47ng60liykbwxisbw1169w5")))

