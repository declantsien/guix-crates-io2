(define-module (crates-io pi ma pimalaya-pomodoro) #:use-module (crates-io))

(define-public crate-pimalaya-pomodoro-0.0.3 (c (n "pimalaya-pomodoro") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ixcb344ixmijj5c6nxd8x5pg9xca91a4q2ggqh2r165crcfv991") (f (quote (("tcp-client" "client") ("tcp-binder" "server") ("tcp" "tcp-binder" "tcp-client") ("server") ("default" "server" "client" "tcp") ("client"))))))

(define-public crate-pimalaya-pomodoro-0.1.0 (c (n "pimalaya-pomodoro") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mock_instant") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d6fwmjfli16x3szx1ih97z4p3qa7yny86hnxl6l1pbvai5q1xhc") (f (quote (("tcp-client" "client") ("tcp-binder" "server") ("tcp" "tcp-binder" "tcp-client") ("server") ("default" "server" "client" "tcp") ("client"))))))

