(define-module (crates-io pi ma pimalaya-secret) #:use-module (crates-io))

(define-public crate-pimalaya-secret-0.0.1 (c (n "pimalaya-secret") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pimalaya-keyring") (r "=0.0.1") (d #t) (k 0)) (d (n "pimalaya-oauth2") (r "=0.0.2") (d #t) (k 0)) (d (n "pimalaya-process") (r "=0.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q1ipfhn5vbj3qmahbyr58f5drhvv3zwvn08lrbzkirscn2zb1dm")))

(define-public crate-pimalaya-secret-0.0.2 (c (n "pimalaya-secret") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pimalaya-keyring") (r "=0.0.4") (d #t) (k 0)) (d (n "pimalaya-process") (r "=0.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07zhvy6n89wf491am8ccfanqhj7yizb2xn1n79gw57zcwn2dqf4k")))

(define-public crate-pimalaya-secret-0.0.3 (c (n "pimalaya-secret") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pimalaya-keyring") (r "=0.0.4") (d #t) (k 0)) (d (n "pimalaya-process") (r "=0.0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("macros" "rt"))) (k 2)))) (h "1r4rl088gnlnw3pqrn35dnc2vwl5qcj06x9y7b6jb09z910idz6y")))

(define-public crate-pimalaya-secret-0.0.4 (c (n "pimalaya-secret") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pimalaya-keyring") (r "=0.0.4") (d #t) (k 0)) (d (n "pimalaya-process") (r "=0.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("macros" "rt"))) (k 2)))) (h "1l8c19z92ga90vlbajngsd2zl6qzvm9yjwfg2nr7nckpx6pmr5qg")))

(define-public crate-pimalaya-secret-0.0.5 (c (n "pimalaya-secret") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pimalaya-keyring") (r "=0.0.5") (d #t) (k 0)) (d (n "pimalaya-process") (r "=0.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("macros" "rt"))) (k 2)))) (h "1xip019wlps2pgmy7yv8r5q53ss7igb9f1f2h2n3kpfrx3b3svcn")))

(define-public crate-pimalaya-secret-0.1.0 (c (n "pimalaya-secret") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "keyring-lib") (r "=0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "process-lib") (r "=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("macros" "rt"))) (k 2)))) (h "0jwd4b46yab701sgfcjfqjnmz8ar9250nm8ml144nwap5f92mpb2")))

