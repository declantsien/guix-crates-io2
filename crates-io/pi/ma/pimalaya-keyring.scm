(define-module (crates-io pi ma pimalaya-keyring) #:use-module (crates-io))

(define-public crate-pimalaya-keyring-0.0.1 (c (n "pimalaya-keyring") (v "0.0.1") (d (list (d (n "keyring") (r "^2.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "157yanji96py8mvq4zwlwwplkr21bv6zp3jy9abb95qqzxgyspxf")))

(define-public crate-pimalaya-keyring-0.0.2 (c (n "pimalaya-keyring") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "keyring") (r "^2.0") (d #t) (k 0)) (d (n "keyring") (r "^2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02snf0drcwp2rl4vmxfa3g7z5zgh0gvyff56brvdq7jxw3z3hl51")))

(define-public crate-pimalaya-keyring-0.0.3 (c (n "pimalaya-keyring") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "keyring") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "123czvj5w8hjqwnmqq5q6gf63c55czq7zsn4jcalcix9wpsfsp3h")))

(define-public crate-pimalaya-keyring-0.0.4 (c (n "pimalaya-keyring") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "keyring") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d51vnl7p0zggkkpfghpch8vnwj7sjcpr7knhb3a62bwsn4j3xyf")))

(define-public crate-pimalaya-keyring-0.0.5 (c (n "pimalaya-keyring") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "keyring") (r "^2.0.4") (f (quote ("linux-no-secret-service"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00yvz90l6hrvbzcyvhba5svarh3xcpyqrnixp1x692nfz6k7syxc")))

(define-public crate-pimalaya-keyring-0.1.0 (c (n "pimalaya-keyring") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "keyring_native") (r "^2.0.4") (f (quote ("linux-no-secret-service"))) (k 0) (p "keyring")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15h08sa862znh31x2gsavl5xzlr8x0aaz8in9k844zy8mw086x3x")))

