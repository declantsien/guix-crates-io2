(define-module (crates-io pi am piam-types) #:use-module (crates-io))

(define-public crate-piam-types-0.0.0 (c (n "piam-types") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aws-sigv4") (r "^0.48.0") (d #t) (k 0)) (d (n "aws-smithy-checksums") (r "^0.48.0") (d #t) (k 0)) (d (n "cidr") (r "^0.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "06xwrpvijfcdr1c5q4zlk5mg39ckj13snxjs8hajl8xdl1mbalg9") (f (quote (("s3-policy") ("all-policy" "s3-policy"))))))

