(define-module (crates-io pi _a pi_async_file) #:use-module (crates-io))

(define-public crate-pi_async_file-0.1.1 (c (n "pi_async_file") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pi_async") (r "^0.1") (d #t) (k 0)))) (h "01k8c7c10250bqsc35c9rsqaak59y5xp5i92h76dvvb2cq9cfnrz")))

(define-public crate-pi_async_file-0.1.2 (c (n "pi_async_file") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pi_async") (r "^0.1") (d #t) (k 0)))) (h "0rxf1mlab63bqj7b12h3pfjasv7nxsjvf11rpgwk988xlrp7pjh8")))

(define-public crate-pi_async_file-0.1.3 (c (n "pi_async_file") (v "0.1.3") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pi_async") (r "^0.1") (d #t) (k 0)))) (h "19xy97ks5jk6w984znwpfrr0q4p7kb4k1qx4dy8hf2m23fz9bhvn")))

(define-public crate-pi_async_file-0.1.5 (c (n "pi_async_file") (v "0.1.5") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pi_async") (r "^0.1") (d #t) (k 0)))) (h "06s4jlwnffkmzyc0gvhw5f7jimbhd83zjn3x1g5g18bgbbpfalfp")))

(define-public crate-pi_async_file-0.1.8 (c (n "pi_async_file") (v "0.1.8") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pi_async") (r "^0.2") (d #t) (k 0)))) (h "0mpgdqlk2vxgbz497cb5nsdwa7qsdd8yq37408skwqyqi5zld3xv")))

(define-public crate-pi_async_file-0.1.9 (c (n "pi_async_file") (v "0.1.9") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pi_async") (r "^0.2") (d #t) (k 0)))) (h "0zxvrfbnk1kn3g06pf9clqfi6bmffzqmg2ld7b63b752hky99lyp")))

(define-public crate-pi_async_file-0.2.0 (c (n "pi_async_file") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pi_async") (r "^0.2") (d #t) (k 0)))) (h "10f6hkn0dblqwlww1dgrmby25vysikdx1sm04s8fqyn1n9knxwa5")))

(define-public crate-pi_async_file-0.2.1 (c (n "pi_async_file") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pi_async") (r "^0.2") (d #t) (k 0)))) (h "0n69knn2g50b6a7sa1mbcrd3smiq0y820n4p257icq9af3qfhq5h")))

(define-public crate-pi_async_file-0.3.0 (c (n "pi_async_file") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pi_async") (r "^0.3") (d #t) (k 0)))) (h "0q76j61gazrl23xn74qavfk5ild3x88qs7ir5qq57c2fnylmg55g")))

(define-public crate-pi_async_file-0.4.0 (c (n "pi_async_file") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pi_async") (r "^0.5") (d #t) (k 0)))) (h "1z1i2j101d6ga7hpsbhp0a24qs64jvlp4fkhw0jk9x9lfvsd9vb9")))

(define-public crate-pi_async_file-0.5.0 (c (n "pi_async_file") (v "0.5.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pi-async-rt") (r "^0.1") (d #t) (k 0)))) (h "0f4icl1dsbva74cmpgzbjzskiq1hnk44j5chdvkplg22h6cniij5")))

(define-public crate-pi_async_file-0.6.0 (c (n "pi_async_file") (v "0.6.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "normpath") (r "^1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pi-async-rt") (r "^0.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (k 0)))) (h "1f8gqm274b1ip6yvd8413h9dpsvyrjwpixssy96p7nwdj02gw5wx")))

(define-public crate-pi_async_file-0.6.1 (c (n "pi_async_file") (v "0.6.1") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "normpath") (r "^1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pi-async-rt") (r "^0.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (k 0)))) (h "0yxqxrv780qd0cb13b0hvpm4i524pdspiapqya3qz0f3w3yzxxhn")))

(define-public crate-pi_async_file-0.6.2 (c (n "pi_async_file") (v "0.6.2") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "normpath") (r "^1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pi-async-rt") (r "^0.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (k 0)))) (h "03n0a9bj25qllmnbgnqnw4mlci7r473hhmj3w7vf3i0lxq7h0w3d")))

(define-public crate-pi_async_file-0.6.3 (c (n "pi_async_file") (v "0.6.3") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "normpath") (r "^1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pi-async-rt") (r "^0.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (k 0)))) (h "06hhw257w61s01gdvxyirmkmqiifwci3z1j40jx197k0s66da780")))

(define-public crate-pi_async_file-0.6.5 (c (n "pi_async_file") (v "0.6.5") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "normpath") (r "^1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pi-async-rt") (r "^0.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (k 0)))) (h "03f7ck3acvqj3qz60p3dpc53h10ybldwkxjkprbn8lzap4izx9i2")))

(define-public crate-pi_async_file-0.6.6 (c (n "pi_async_file") (v "0.6.6") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "normpath") (r "^1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pi-async-rt") (r "^0.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (k 0)))) (h "0lq23v701j3f38j8l0wcd0197b8jw5h725l3xfgfdy8ck0kdiixr")))

