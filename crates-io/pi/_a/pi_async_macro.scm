(define-module (crates-io pi _a pi_async_macro) #:use-module (crates-io))

(define-public crate-pi_async_macro-0.1.1 (c (n "pi_async_macro") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05dlfx9p99n5dfsk5x51ns8fs2ddxwxv5l291pljm3qjbhxdnsjf")))

(define-public crate-pi_async_macro-0.1.3 (c (n "pi_async_macro") (v "0.1.3") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0by6q8gxf6lq4m1my94ym0xlsyi29djwdljzch410wy639n6my9i")))

