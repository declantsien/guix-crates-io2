(define-module (crates-io pi _a pi_adler32) #:use-module (crates-io))

(define-public crate-pi_adler32-0.1.0 (c (n "pi_adler32") (v "0.1.0") (d (list (d (n "adler32") (r "^1.2") (d #t) (k 0)))) (h "1g28vyx87nw9bdqdlsh8lfawi1j0kh943ngv5fpr3j282qywws8s")))

(define-public crate-pi_adler32-0.1.1 (c (n "pi_adler32") (v "0.1.1") (d (list (d (n "adler32") (r "^1.2") (d #t) (k 0)))) (h "1nn191nanxs3nx69k7aynzhkjm2iapk4239hhnm0xr8z3v0j3qnw")))

