(define-module (crates-io pi _a pi_append_vec) #:use-module (crates-io))

(define-public crate-pi_append_vec-0.1.0 (c (n "pi_append_vec") (v "0.1.0") (d (list (d (n "pi_arr") (r "^0.11") (d #t) (k 0)) (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0yq2x0bsr28904ylqlsvarn35j1gvk78kxdn6m730awj89iagj87")))

(define-public crate-pi_append_vec-0.1.1 (c (n "pi_append_vec") (v "0.1.1") (d (list (d (n "pi_arr") (r "^0.11") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1p7zi75nvlmr0jqry3fsa9fcwywv9b0rdlc4jns9xwqm6rwg8d42")))

(define-public crate-pi_append_vec-0.1.2 (c (n "pi_append_vec") (v "0.1.2") (d (list (d (n "pi_arr") (r "^0.11") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0hlb59nssqnyzdxfz1zd85wknxjz8pbnwi5dy7ksnsfhqdbpvayv")))

(define-public crate-pi_append_vec-0.1.3 (c (n "pi_append_vec") (v "0.1.3") (d (list (d (n "pi_arr") (r "^0.11") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "02mg8m4fkakarszl99xd1z733pvfc0spgflxdp790ibm7w67v11m")))

(define-public crate-pi_append_vec-0.1.4 (c (n "pi_append_vec") (v "0.1.4") (d (list (d (n "pi_arr") (r "^0.11") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0xz9nacnblw37y8fx1bm7zikxdw7irzcm24jspbnb6dlwy59x080")))

(define-public crate-pi_append_vec-0.1.5 (c (n "pi_append_vec") (v "0.1.5") (d (list (d (n "pi_arr") (r "^0.11") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "10ds5ylg7fg6nqif97kybl1ss4masrg35whzfy7i1rb9fzi6qfx5")))

(define-public crate-pi_append_vec-0.1.6 (c (n "pi_append_vec") (v "0.1.6") (d (list (d (n "pi_arr") (r "^0.11") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "06bvcp3vvhcj27ngkv3ka9yyx0xf1ggshyz7xxd34q1xhkz4llg0")))

(define-public crate-pi_append_vec-0.2.0 (c (n "pi_append_vec") (v "0.2.0") (d (list (d (n "pi_arr") (r "^0.12") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0xdh14z86f2a7wb1bhrlqag690w5jm2s5aksx1vmqlhg0bs479cz")))

(define-public crate-pi_append_vec-0.3.0 (c (n "pi_append_vec") (v "0.3.0") (d (list (d (n "pi_arr") (r "^0.17") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "087819rx3zpd55wz7hf2725wif6wa765pys97a2j8j7838jpcc09")))

(define-public crate-pi_append_vec-0.3.1 (c (n "pi_append_vec") (v "0.3.1") (d (list (d (n "pi_arr") (r "^0.17") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0zgnh8y90bmxd9p24fkds8ic6kk03h81pi80qi7pg0s02fqn782y")))

(define-public crate-pi_append_vec-0.3.2 (c (n "pi_append_vec") (v "0.3.2") (d (list (d (n "pi_arr") (r "^0.17") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0fllqzmprdaw54cvlhnwn3wqg6m7nx8ga8ax0k0s32k47kfbad2b")))

(define-public crate-pi_append_vec-0.3.3 (c (n "pi_append_vec") (v "0.3.3") (d (list (d (n "pi_arr") (r "^0.17") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1acbpx2sqq1602ngvp7wyd11k7439zalv1hs73hn7br4jsm50bn6")))

(define-public crate-pi_append_vec-0.3.4 (c (n "pi_append_vec") (v "0.3.4") (d (list (d (n "pi_arr") (r "^0.17") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1jl9s77wk29ars49a58f7xk9p9cwx89sh306wqb15vnxx9m53b5x")))

(define-public crate-pi_append_vec-0.3.5 (c (n "pi_append_vec") (v "0.3.5") (d (list (d (n "pi_arr") (r "^0.17") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1dcgs73yydqs5901k5i50ghg13a3v99sgi1ckzwrxbr5gi2xab5m")))

(define-public crate-pi_append_vec-0.3.6 (c (n "pi_append_vec") (v "0.3.6") (d (list (d (n "pi_arr") (r "^0.17") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0yjx5bg2sv93ni1g6zifl8f5zv3cpzv5yw6q0aqj7mny20m7vvkr")))

(define-public crate-pi_append_vec-0.3.7 (c (n "pi_append_vec") (v "0.3.7") (d (list (d (n "pi_arr") (r "^0.18") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1nyjaa8psag7rfqh2ccdj8f8815q98a34m8mcwhv4izh8d06y1k1")))

(define-public crate-pi_append_vec-0.3.8 (c (n "pi_append_vec") (v "0.3.8") (d (list (d (n "pi_arr") (r "^0.18") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1r7ixnhzwqzqqgqbfckkp613739crp07zkwsf8cba6fgk5697rwj")))

(define-public crate-pi_append_vec-0.3.9 (c (n "pi_append_vec") (v "0.3.9") (d (list (d (n "pi_arr") (r "^0.18") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1nviawm91g1gsfynqkbiymvwyr5nd8gw0zfjkz1zwwxiavp2595d")))

(define-public crate-pi_append_vec-0.3.10 (c (n "pi_append_vec") (v "0.3.10") (d (list (d (n "pi_arr") (r "^0.18") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "18fi2qv39rxkyw3f5dnwsw78dhs1zl3z1wj3x6jj7vd89mpa9d00")))

(define-public crate-pi_append_vec-0.3.11 (c (n "pi_append_vec") (v "0.3.11") (d (list (d (n "pi_arr") (r "^0.18") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0kzminsn6r1v7sf7ginm81hdfyf0lfz0nidvrfmaz1xy2fbgpmnz")))

(define-public crate-pi_append_vec-0.4.1 (c (n "pi_append_vec") (v "0.4.1") (d (list (d (n "pi_arr") (r "^0.18") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0bwqhn5bhv27jzq1am0v3zyr9qwz9l4837ywgh6f9x86xb2gsizj")))

(define-public crate-pi_append_vec-0.4.2 (c (n "pi_append_vec") (v "0.4.2") (d (list (d (n "pi_arr") (r "^0.18") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "19i2lx0kylchw576jq8wmm1m78yis16zqhg2bk2f2p2rlzvda27z")))

