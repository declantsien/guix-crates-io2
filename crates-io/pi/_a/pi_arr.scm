(define-module (crates-io pi _a pi_arr) #:use-module (crates-io))

(define-public crate-pi_arr-0.1.0 (c (n "pi_arr") (v "0.1.0") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0w7ji0xy5i75xs48xmm1hhdsm4ax02601qmb3576q8j8zqpgjqgv")))

(define-public crate-pi_arr-0.1.1 (c (n "pi_arr") (v "0.1.1") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0glb03pqcj7nnh44za1m520lrdwkxzdb20q96jgssvjdg5srlid5")))

(define-public crate-pi_arr-0.2.1 (c (n "pi_arr") (v "0.2.1") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1nqhvxz65nksxfxrbg0040z79s3wvnxi5w9mzb6hamcp0d4hhfdp")))

(define-public crate-pi_arr-0.2.2 (c (n "pi_arr") (v "0.2.2") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "03h480ykfw5nm74z5nsrymp6sjkq6ll41ridnnqm75p8hl4m3kg0")))

(define-public crate-pi_arr-0.3.0 (c (n "pi_arr") (v "0.3.0") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "12mpml6bwnixiaj87d21dr8wdq8iccdqawf0mpczjl56zk11lnzb")))

(define-public crate-pi_arr-0.4.0 (c (n "pi_arr") (v "0.4.0") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0xbjyrld0ai1h6jdnkvagz4ly7kbhpla9prr6cbwb3bvx741ijfb")))

(define-public crate-pi_arr-0.4.1 (c (n "pi_arr") (v "0.4.1") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0ai1ig1pfhcc5vd4hc6fqg4ddmi07c5gfn6v1r51i8g872191kyz")))

(define-public crate-pi_arr-0.5.0 (c (n "pi_arr") (v "0.5.0") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "02qkrnr3z376mjjxrq7hma5i099r130ahqlbhfpz24md1iwg8gc6")))

(define-public crate-pi_arr-0.6.0 (c (n "pi_arr") (v "0.6.0") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0ildvqazvnm5719acn0m9cziabrwd18ihira41f7c9rhls2735yi")))

(define-public crate-pi_arr-0.7.0 (c (n "pi_arr") (v "0.7.0") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "12b97kfnyq5qb1a9acldq1c4j6mkgyk6v8ww6nk2k7f07hvdnacz")))

(define-public crate-pi_arr-0.8.0 (c (n "pi_arr") (v "0.8.0") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0hd78xpyi7lpbvvjjy2qryd7il60apfzkm5gvhd41hzy7rm0m063")))

(define-public crate-pi_arr-0.8.1 (c (n "pi_arr") (v "0.8.1") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0wahjg098czpil43ds73r81i7bvgprfbbr2aiq18gjv6jvafyizp")))

(define-public crate-pi_arr-0.8.2 (c (n "pi_arr") (v "0.8.2") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0ph7dqzyn7pih7g2bfkrvmah58yxvc19fzzlfys69ypckvqbsxva")))

(define-public crate-pi_arr-0.9.0 (c (n "pi_arr") (v "0.9.0") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0iwj31qgxq5vhc0j250az1ryvkb51z6cplhwcinfdmh8nqdkfvb8")))

(define-public crate-pi_arr-0.10.0 (c (n "pi_arr") (v "0.10.0") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0r9dhjxfcm8j6i769mxgnz49lbpbqcas9a78k5fj6jngd01s9fih")))

(define-public crate-pi_arr-0.11.0 (c (n "pi_arr") (v "0.11.0") (d (list (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1niy49wf7x6x4dg2x2idjscf7zbvxk791a8m64g91zyy6l838jjp")))

(define-public crate-pi_arr-0.11.1 (c (n "pi_arr") (v "0.11.1") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1b4xqj4qcpyh7y0w588g3a38jjpgd1vhrg37zjgff4xh65fqrdsk")))

(define-public crate-pi_arr-0.11.2 (c (n "pi_arr") (v "0.11.2") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0ggncvjijrkmaa2ppaycmszh390qxp2p6d1ryylp3aswb8324apc")))

(define-public crate-pi_arr-0.12.0 (c (n "pi_arr") (v "0.12.0") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0l86dnkpk4928kbq0v89s3j5mzcy2h6nsq4c8nvj5iqjk0zny8cs")))

(define-public crate-pi_arr-0.12.1 (c (n "pi_arr") (v "0.12.1") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0l2fkvfkrfv8vx4yn7g3h119bhgxr0sdpaljpvhl4mbd8b5cypxf")))

(define-public crate-pi_arr-0.13.0 (c (n "pi_arr") (v "0.13.0") (d (list (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0kamll52mjr5gzgmhna40p7x557f0wmn4pnfn8379hhvdxwnra0j")))

(define-public crate-pi_arr-0.13.1 (c (n "pi_arr") (v "0.13.1") (d (list (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1aaz8f3lwya5m70rxasxwxs4rrq4fm5zqywnhzpx5ybj7jpd0phn")))

(define-public crate-pi_arr-0.13.2 (c (n "pi_arr") (v "0.13.2") (h "0spqlmrya5hifaw0rdsz0jb3a7w5riivbc229jq44c178hjxid1b")))

(define-public crate-pi_arr-0.14.0 (c (n "pi_arr") (v "0.14.0") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)))) (h "1h5nsfdkw492rypq5hjs4i7y8h0n2m7phrflqyrjhnqs9hdpl70p")))

(define-public crate-pi_arr-0.14.1 (c (n "pi_arr") (v "0.14.1") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)))) (h "1k8swg26xhmzf8fja0q202svy83w0ghh4yls6pbr5ijg4n53v4vv")))

(define-public crate-pi_arr-0.14.2 (c (n "pi_arr") (v "0.14.2") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "183ip04y8ha0apd5n88kx1ssl3r30vd17gs4gpc9d9wjrahdfxf8")))

(define-public crate-pi_arr-0.14.3 (c (n "pi_arr") (v "0.14.3") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0d8bq3269l41zynizi77fxy6l7w2p6p3vpwaiadhv6pqnlq54nda")))

(define-public crate-pi_arr-0.15.0 (c (n "pi_arr") (v "0.15.0") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "03adsl49jrxyqis2lyx1jp979mbvmkc2qrb8baim6hp0804x8lz7")))

(define-public crate-pi_arr-0.15.1 (c (n "pi_arr") (v "0.15.1") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "08aczf1anfd5gnyl1plp70lz2bhbk7kh3nqr7hm49l8lzz6dz24f")))

(define-public crate-pi_arr-0.16.0 (c (n "pi_arr") (v "0.16.0") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1c6ag8qfwbdxyns3lknfg6xgjz6pigfkgb23xvjfvaqdlcn4y56s")))

(define-public crate-pi_arr-0.17.0 (c (n "pi_arr") (v "0.17.0") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0fk1sky1p3661w8z1fvvjjrj30g69zr1rx98vwz7kg9l5bk7r3vg")))

(define-public crate-pi_arr-0.17.1 (c (n "pi_arr") (v "0.17.1") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0lkh8zss2c6ax1mfmdk0irqrr51hhygsgrkms8pc31x9wbx9cv42")))

(define-public crate-pi_arr-0.18.0 (c (n "pi_arr") (v "0.18.0") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0lm276vn4i8b1qxl8hdvczzghnzk2n4b9fahbp3rmv42j1zzg8g5")))

(define-public crate-pi_arr-0.18.1 (c (n "pi_arr") (v "0.18.1") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0zsws3g54g37456xbjv6wr6339rll8vfniyafdr5kfqjwwfycak7")))

(define-public crate-pi_arr-0.18.2 (c (n "pi_arr") (v "0.18.2") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "11rcdvd61vsrf152f006hfl1m2i65ld20sm0wwflj14y5dhcsz8y")))

(define-public crate-pi_arr-0.18.3 (c (n "pi_arr") (v "0.18.3") (d (list (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "04xhw2l2qaixd888myi41kx8rls4pxxg1y958vl4p0y2vlixpp01")))

