(define-module (crates-io pi _a pi_any) #:use-module (crates-io))

(define-public crate-pi_any-0.1.0 (c (n "pi_any") (v "0.1.0") (h "0r9hfxi7j290ljvmfp6k7c4al3v9rawhvrkx05psaw56hlsfxc4h")))

(define-public crate-pi_any-0.1.1 (c (n "pi_any") (v "0.1.1") (h "1prkipv3k1nz3vblbixlsj1586scczr2kdalncsm3ili3p32ksqi")))

