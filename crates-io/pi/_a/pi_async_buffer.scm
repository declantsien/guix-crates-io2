(define-module (crates-io pi _a pi_async_buffer) #:use-module (crates-io))

(define-public crate-pi_async_buffer-0.1.0 (c (n "pi_async_buffer") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_async") (r "^0.3") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.3") (d #t) (k 2)))) (h "09hdpcvvzwhrpsa1xzlbrlivgwsfrp8iy6ppd3fryb9ni33p02nv")))

(define-public crate-pi_async_buffer-0.2.0 (c (n "pi_async_buffer") (v "0.2.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_async") (r "^0.5") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.4") (d #t) (k 2)))) (h "1cqr9vy61p968dxgrb6cfypxwmkarwj5vk0yyg2nks7dl5db187x")))

(define-public crate-pi_async_buffer-0.3.0 (c (n "pi_async_buffer") (v "0.3.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_async") (r "^0.5") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.4") (d #t) (k 2)))) (h "0ska9wkpwwp3gdqv1slfpnh2jsbxwj72lvznyxpvdc4qa33xwsag")))

(define-public crate-pi_async_buffer-0.4.0 (c (n "pi_async_buffer") (v "0.4.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_async") (r "^0.5") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.4") (d #t) (k 2)))) (h "1hwm0m5j0sqmlxl6qqdh3lfajwqp5mni6y04m4v2nggxz8bbphbn")))

(define-public crate-pi_async_buffer-0.4.1 (c (n "pi_async_buffer") (v "0.4.1") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_async") (r "^0.5") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.4") (d #t) (k 2)))) (h "0ykv6bkqqadxxmppcvdhfwr3awgw4qw50mgc64m3aamqf1cyh3w8")))

(define-public crate-pi_async_buffer-0.4.2 (c (n "pi_async_buffer") (v "0.4.2") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_async") (r "^0.5") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.4") (d #t) (k 2)))) (h "0qvz97s5hinga6gk86vi92bjmkf06f5yhbhp3d89yz6pqznkgsij")))

(define-public crate-pi_async_buffer-0.4.3 (c (n "pi_async_buffer") (v "0.4.3") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_async") (r "^0.5") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.4") (d #t) (k 2)))) (h "1hk2zvj19rgr42xs2h2fjcfjipgan2jqadh5f7rs1w3gwqg287h6")))

(define-public crate-pi_async_buffer-0.4.4 (c (n "pi_async_buffer") (v "0.4.4") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_async") (r "^0.5") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.4") (d #t) (k 2)))) (h "1k88dz7h9ya0m9c15b40nviwdx7b9z2ydga3k3c93z0q3wn6whl4")))

(define-public crate-pi_async_buffer-0.5.0 (c (n "pi_async_buffer") (v "0.5.0") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_async") (r "^0.5") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.4") (d #t) (k 2)))) (h "06gb1jh2b2pizw9vlmgd7cnas5lkkagxk0bdxklwpd3nh6yi8zjb")))

(define-public crate-pi_async_buffer-0.6.0 (c (n "pi_async_buffer") (v "0.6.0") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_async") (r "^0.5") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.4") (d #t) (k 2)))) (h "1l4syajncg7llv4awsq1iqsbfnzw4ggpmnqdpimkmw6zb4bkrbb4")))

(define-public crate-pi_async_buffer-0.6.1 (c (n "pi_async_buffer") (v "0.6.1") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_async") (r "^0.5") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.4") (d #t) (k 2)))) (h "0xbyp02kh74grxpqzknx71ck11l5c0s8gb45216a4af1v5xzhid7")))

(define-public crate-pi_async_buffer-0.7.0 (c (n "pi_async_buffer") (v "0.7.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi-async-rt") (r "^0.1") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.5") (d #t) (k 2)))) (h "0r4bspvsyspsc0bcq9z1s4qj0441fwa0bxs8vwjiwsmfd2z13kk1")))

