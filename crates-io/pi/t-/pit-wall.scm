(define-module (crates-io pi t- pit-wall) #:use-module (crates-io))

(define-public crate-pit-wall-0.1.0 (c (n "pit-wall") (v "0.1.0") (h "0nbdg8c9cc08w08a30bjkrpw7p7ww2cnlh4gdkpzfk09x27q972l")))

(define-public crate-pit-wall-0.2.0 (c (n "pit-wall") (v "0.2.0") (h "1iyjrpbla3qky9xph45fddk6grq8hxhbdp871l655liha41m5b9s")))

(define-public crate-pit-wall-0.3.0 (c (n "pit-wall") (v "0.3.0") (h "09rjyfjsjrgrc0m8qp1ad96gf32f5jvii5vjwgryxcl2mzs3g8bb")))

(define-public crate-pit-wall-0.4.0 (c (n "pit-wall") (v "0.4.0") (h "1jss4m9zr2xvshkh35wmc98qp4fbl66dzni3b37rqigiizbpan3m")))

(define-public crate-pit-wall-0.4.1 (c (n "pit-wall") (v "0.4.1") (h "1gnpsq30p4nimmanc3rvd3k36f791qrvq4b9l36d4i8zk07fc1lz")))

(define-public crate-pit-wall-0.4.2 (c (n "pit-wall") (v "0.4.2") (d (list (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0vvckaqlp89rka7yj693maj8ms8y5rlr9lfc77mv9wgblmskb6az")))

(define-public crate-pit-wall-0.4.3 (c (n "pit-wall") (v "0.4.3") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1qnixb1b644x8p3v4rgl82l20mkilkg289g17hkv8f31awlcwyil")))

