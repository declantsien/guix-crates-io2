(define-module (crates-io pi p- pip-upgrade) #:use-module (crates-io))

(define-public crate-pip-upgrade-0.1.0 (c (n "pip-upgrade") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "spinoff") (r "^0.7.0") (d #t) (k 0)))) (h "0mxiv7pym9a98nrxinrd7amddv4j805gv0zpr89xsdvx595ifqgn")))

(define-public crate-pip-upgrade-0.1.1 (c (n "pip-upgrade") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "spinoff") (r "^0.7.0") (d #t) (k 0)))) (h "1vwbk2p9nbvyrgm48rlhwbbgz68f7dchzjkmkgrgpb9sczhsfyas")))

(define-public crate-pip-upgrade-0.2.0 (c (n "pip-upgrade") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "spinoff") (r "^0.7.0") (d #t) (k 0)))) (h "0idi2rfrdj4hpc9yr51jyahjiilwfl17dj0xhykdj82gdr3rq9rx")))

(define-public crate-pip-upgrade-0.2.1 (c (n "pip-upgrade") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "spinoff") (r "^0.7.0") (d #t) (k 0)))) (h "06vw9wzpd2j5dzg48zkvlsln6nbxxsd1qfhsbyi2nyj2q3pmxbn4")))

