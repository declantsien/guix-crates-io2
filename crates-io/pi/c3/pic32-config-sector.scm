(define-module (crates-io pi c3 pic32-config-sector) #:use-module (crates-io))

(define-public crate-pic32-config-sector-0.1.0 (c (n "pic32-config-sector") (v "0.1.0") (h "0rpssn9dwgpi17wlb03wvg9jj0fgc50s4bh7sqpy28jw8h3r2f9g")))

(define-public crate-pic32-config-sector-0.1.1 (c (n "pic32-config-sector") (v "0.1.1") (h "14hwf82v34vypy6wan5h48xsn4l3dryids94s55vdl9jvzswwkzh")))

(define-public crate-pic32-config-sector-0.1.2 (c (n "pic32-config-sector") (v "0.1.2") (h "0mabmydha3k8rmw9ahlrrxi2r44jvk1iy19hjlvxcm3k3b7zqrpg")))

(define-public crate-pic32-config-sector-0.2.0 (c (n "pic32-config-sector") (v "0.2.0") (h "1644m0plazhxxaa0vzp1vbap6vi4xfli780px7vyf4b0z6ps48dd")))

(define-public crate-pic32-config-sector-0.3.0 (c (n "pic32-config-sector") (v "0.3.0") (h "080lv7mmqn597ky9j6r8g084dasaiipxlgqbl61hymqm35px7py5")))

