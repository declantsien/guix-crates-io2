(define-module (crates-io pi c3 pic32mx2xx) #:use-module (crates-io))

(define-public crate-pic32mx2xx-0.1.0 (c (n "pic32mx2xx") (v "0.1.0") (d (list (d (n "mips-mcu") (r "^0.1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "16r7a59v3xg191jlcrx7dwq7xz4l21hwsiqfwflpk9cslxxp6nsn") (f (quote (("rt") ("pic32mx2xxfxxxd") ("pic32mx2xxfxxxb"))))))

(define-public crate-pic32mx2xx-0.1.1 (c (n "pic32mx2xx") (v "0.1.1") (d (list (d (n "mips-mcu") (r "^0.1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0kyfg91hh53a8q7lq9jaxqjsfm03qlbfqardmvk5if6s4m3id7sp") (f (quote (("rt") ("pic32mx2xxfxxxd") ("pic32mx2xxfxxxb"))))))

(define-public crate-pic32mx2xx-0.1.2 (c (n "pic32mx2xx") (v "0.1.2") (d (list (d (n "mips-mcu") (r "^0.1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0srwsvqyp33qlsrx67qw68llr7602n2lil0sa2a1h932llyzr51d") (f (quote (("rt") ("pic32mx2xxfxxxd") ("pic32mx2xxfxxxb"))))))

(define-public crate-pic32mx2xx-0.2.0 (c (n "pic32mx2xx") (v "0.2.0") (d (list (d (n "mips-mcu") (r "^0.1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0n6r9imk4cd1hbgcs06jig99qly70r7bb6rfdp8j26g2zcq0kxxj") (f (quote (("rt") ("pic32mx2xxfxxxd") ("pic32mx2xxfxxxc") ("pic32mx2xxfxxxb"))))))

(define-public crate-pic32mx2xx-0.3.0 (c (n "pic32mx2xx") (v "0.3.0") (d (list (d (n "mips-mcu") (r "^0.1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "114mqalsraf9nbsylrn5lxc3gamnnjxnk8jjbi10pryskin6zj5r") (f (quote (("rt") ("pic32mx2xxfxxxd") ("pic32mx2xxfxxxc") ("pic32mx2xxfxxxb") ("pic32mx2x4fxxxd") ("pic32mx2x4fxxxb") ("pic32mx1xxfxxxd") ("pic32mx1xxfxxxc") ("pic32mx1xxfxxxb") ("pic32mx1x4fxxxd") ("pic32mx1x4fxxxb"))))))

(define-public crate-pic32mx2xx-0.4.0 (c (n "pic32mx2xx") (v "0.4.0") (d (list (d (n "mips-mcu") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "11pyh1y7ybs0djd45hh9pmllh338w0gpky7cgj0yq7aasky85kby") (f (quote (("rt") ("pic32mx2xxfxxxd") ("pic32mx2xxfxxxc") ("pic32mx2xxfxxxb") ("pic32mx2x4fxxxd") ("pic32mx2x4fxxxb") ("pic32mx1xxfxxxd") ("pic32mx1xxfxxxc") ("pic32mx1xxfxxxb") ("pic32mx1x4fxxxd") ("pic32mx1x4fxxxb"))))))

(define-public crate-pic32mx2xx-0.4.1 (c (n "pic32mx2xx") (v "0.4.1") (d (list (d (n "mips-mcu") (r "^0.2.0") (d #t) (k 0)) (d (n "mips-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00k1s49nfhz61c8vnkx0nccmlsx2afrj4db3smvxnync6q6h906p") (f (quote (("rt" "mips-rt") ("pic32mx2xxfxxxd") ("pic32mx2xxfxxxc") ("pic32mx2xxfxxxb") ("pic32mx2x4fxxxd") ("pic32mx2x4fxxxb") ("pic32mx1xxfxxxd") ("pic32mx1xxfxxxc") ("pic32mx1xxfxxxb") ("pic32mx1x4fxxxd") ("pic32mx1x4fxxxb"))))))

(define-public crate-pic32mx2xx-0.4.2 (c (n "pic32mx2xx") (v "0.4.2") (d (list (d (n "mips-mcu") (r "^0.2.0") (d #t) (k 0)) (d (n "mips-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0zqgcjswdkvbqacz2jqx6wym18lijyvzf1sixsv7jlsjf2badjj0") (f (quote (("rt" "mips-rt") ("pic32mx2xxfxxxd") ("pic32mx2xxfxxxc") ("pic32mx2xxfxxxb") ("pic32mx2x4fxxxd") ("pic32mx2x4fxxxb") ("pic32mx1xxfxxxd") ("pic32mx1xxfxxxc") ("pic32mx1xxfxxxb") ("pic32mx1x4fxxxd") ("pic32mx1x4fxxxb"))))))

(define-public crate-pic32mx2xx-0.5.0 (c (n "pic32mx2xx") (v "0.5.0") (d (list (d (n "mips-mcu") (r "^0.2.0") (d #t) (k 0)) (d (n "mips-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0k2ksyg8ch9a1wadxl0yk4dyqab7dmr11dk5c0wvg9bgwm21biqx") (f (quote (("rt" "mips-rt") ("pic32mx2xxfxxxd") ("pic32mx2xxfxxxc") ("pic32mx2xxfxxxb") ("pic32mx2x4fxxxd") ("pic32mx2x4fxxxb") ("pic32mx1xxfxxxd") ("pic32mx1xxfxxxc") ("pic32mx1xxfxxxb") ("pic32mx1x4fxxxd") ("pic32mx1x4fxxxb"))))))

(define-public crate-pic32mx2xx-0.6.0 (c (n "pic32mx2xx") (v "0.6.0") (d (list (d (n "mips-mcu") (r "^0.2.0") (d #t) (k 0)) (d (n "mips-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1crc9b18sqj59l2vy1njv2hwabzfwffi4szzk4x3w057d9kjjg03") (f (quote (("rt" "mips-rt") ("pic32mx2xxfxxxd") ("pic32mx2xxfxxxc") ("pic32mx2xxfxxxb") ("pic32mx2x4fxxxd") ("pic32mx2x4fxxxb") ("pic32mx1xxfxxxd") ("pic32mx1xxfxxxc") ("pic32mx1xxfxxxb") ("pic32mx1x4fxxxd") ("pic32mx1x4fxxxb"))))))

(define-public crate-pic32mx2xx-0.7.0 (c (n "pic32mx2xx") (v "0.7.0") (d (list (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "mips-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "08gqmh245w2vkkgk4jp9ybaq8hsnxil44q0832m9sxvscim0v73r") (f (quote (("rt" "mips-rt") ("pic32mx2xxfxxxd") ("pic32mx2xxfxxxc") ("pic32mx2xxfxxxb") ("pic32mx2x4fxxxd") ("pic32mx2x4fxxxb") ("pic32mx1xxfxxxd") ("pic32mx1xxfxxxc") ("pic32mx1xxfxxxb") ("pic32mx1x4fxxxd") ("pic32mx1x4fxxxb")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

