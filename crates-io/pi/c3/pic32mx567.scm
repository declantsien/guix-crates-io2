(define-module (crates-io pi c3 pic32mx567) #:use-module (crates-io))

(define-public crate-pic32mx567-0.1.0 (c (n "pic32mx567") (v "0.1.0") (d (list (d (n "mips-mcu") (r "^0.2.0") (d #t) (k 0)) (d (n "mips-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0n0bapq7cnsm8kiwa6srwq0d4k6lqhx5wgh67kwx9zv2hjdg6l4h") (f (quote (("rt" "mips-rt") ("pic32mx695fxxxl"))))))

