(define-module (crates-io pi c3 pic32mx470) #:use-module (crates-io))

(define-public crate-pic32mx470-0.1.0 (c (n "pic32mx470") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "mips-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1bqv90cdgr2aj6mjwjh7fd6jbmvlyid8cbv98y8wkm7yqgjpwckl") (f (quote (("rt" "mips-rt") ("pic32mx47xfxxxl") ("pic32mx37xfxxxl"))))))

