(define-module (crates-io pi pv pipvwmdrn) #:use-module (crates-io))

(define-public crate-pipvwmdrn-0.1.3 (c (n "pipvwmdrn") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.6") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1gnzfjlkwmnwjyff39x2a5b49xphwdxa9swbf3d9zxw74m95dbha")))

(define-public crate-pipvwmdrn-0.1.31 (c (n "pipvwmdrn") (v "0.1.31") (d (list (d (n "clap") (r "^4.4.6") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "00frbmka36b959022xlmw714b7njl49s3vgdq3aipg3s1f53c7m5")))

