(define-module (crates-io pi c8 pic8259) #:use-module (crates-io))

(define-public crate-pic8259-0.10.0 (c (n "pic8259") (v "0.10.0") (d (list (d (n "x86_64") (r "^0.14.2") (f (quote ("instructions" "inline_asm"))) (k 0)))) (h "0dxqf2r6rxh9kc7gnfmjzrxvabzvgql4hji69b6m6a8aqmjplvfc")))

(define-public crate-pic8259-0.10.1 (c (n "pic8259") (v "0.10.1") (d (list (d (n "x86_64") (r "^0.14.2") (f (quote ("instructions" "inline_asm"))) (k 0)))) (h "18cimh2hnwngc1ifp8srr482vgv8akkl391snzww0czfhc6r5k08")))

(define-public crate-pic8259-0.10.2 (c (n "pic8259") (v "0.10.2") (d (list (d (n "x86_64") (r "^0.14.2") (f (quote ("instructions"))) (k 0)))) (h "1792wrssflh66rbc2yh35i8rn1m7lhf087czcja6xqg22ksj3v14") (f (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-pic8259-0.10.3 (c (n "pic8259") (v "0.10.3") (d (list (d (n "x86_64") (r "^0.14.2") (f (quote ("instructions"))) (k 0)))) (h "1j49q76s0s0x51c2h4rpw879pnazvw4jclaaazdw7h0gghc947kn") (f (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-pic8259-0.10.4 (c (n "pic8259") (v "0.10.4") (d (list (d (n "x86_64") (r "^0.14.2") (f (quote ("instructions"))) (k 0)))) (h "157183xan3mwg2rk52gi8qw91z1v267p71c6jcbhn7nv05dlp16b") (f (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-pic8259-0.11.0 (c (n "pic8259") (v "0.11.0") (d (list (d (n "x86_64") (r "^0.15.0") (f (quote ("instructions"))) (k 0)))) (h "08nv6dyb49rp4329nq344ndiivsxhmkgvrs7grsmy5ib55nainb2") (f (quote (("stable") ("nightly") ("default" "nightly"))))))

