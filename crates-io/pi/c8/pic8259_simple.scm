(define-module (crates-io pi c8 pic8259_simple) #:use-module (crates-io))

(define-public crate-pic8259_simple-0.1.0 (c (n "pic8259_simple") (v "0.1.0") (d (list (d (n "cpuio") (r "^0.2.0") (d #t) (k 0)))) (h "0q9dmmm53z35gq19ap6h1yq2hzwswyvymqf3q29mn80dzj8n38d0")))

(define-public crate-pic8259_simple-0.1.1 (c (n "pic8259_simple") (v "0.1.1") (d (list (d (n "cpuio") (r "^0.2.0") (d #t) (k 0)))) (h "0wjwlcds67wxh5mk6k3d78m7sp9qg43bxnkc3d9ai3c223yv4r6w")))

(define-public crate-pic8259_simple-0.2.0 (c (n "pic8259_simple") (v "0.2.0") (d (list (d (n "cpuio") (r "^0.3.0") (d #t) (k 0)))) (h "0lp9kgqaihsaqbd3xwh3br2fyxcd4gzpsyzn2n0byncfzfbm8amg")))

