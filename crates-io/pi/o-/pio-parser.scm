(define-module (crates-io pi o- pio-parser) #:use-module (crates-io))

(define-public crate-pio-parser-0.1.0 (c (n "pio-parser") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19.6") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.6") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "pio") (r "^0.1.0") (d #t) (k 0)))) (h "0p7dkx64n6kj69cdr83i4wbn7c823zfxr8ryr1hpwndv9aqpnkjn")))

(define-public crate-pio-parser-0.2.0 (c (n "pio-parser") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.19.6") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.6") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "pio") (r "^0.2.0") (d #t) (k 0)))) (h "1p0j1gk2jhsqwmgscvkdv55cad2jx9rmq476qyjnzwfsqgzajgh3")))

(define-public crate-pio-parser-0.2.1 (c (n "pio-parser") (v "0.2.1") (d (list (d (n "lalrpop") (r "^0.19.6") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.6") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "pio") (r "^0.2.1") (d #t) (k 0)))) (h "01ibhkbzdmk0wqah8aps6h4kj0nhb3pd3srd2bqnrpnx8dj5mqjp")))

(define-public crate-pio-parser-0.2.2 (c (n "pio-parser") (v "0.2.2") (d (list (d (n "lalrpop") (r "^0.19.6") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.6") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "pio") (r "^0.2.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (f (quote ("unicode"))) (k 1)))) (h "0syxm3rnjgcicn5wqv67dimksnla54ayy1vjzj6zkbkrh8mjqlvp")))

