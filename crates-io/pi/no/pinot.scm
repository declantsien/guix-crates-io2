(define-module (crates-io pi no pinot) #:use-module (crates-io))

(define-public crate-pinot-0.1.0 (c (n "pinot") (v "0.1.0") (h "0ys1c64amf9h0ni8j6nq4mjmqigs5clc5pw48yw9x4k6iagnxkv1")))

(define-public crate-pinot-0.1.1 (c (n "pinot") (v "0.1.1") (h "1wby6vj58c28yh5z2k5ilfv2j4yl5vlghbfljdan9naw44mzcld9")))

(define-public crate-pinot-0.1.2 (c (n "pinot") (v "0.1.2") (h "05qd4g29jmzi29dq1fv20ihy9c9fzx7hybviik6n3k9rix41afjk")))

(define-public crate-pinot-0.1.3 (c (n "pinot") (v "0.1.3") (h "153di76kx7xb4mp2i14zg1wkz3mclivv4c77mbhh7af553yqnsnh")))

(define-public crate-pinot-0.1.4 (c (n "pinot") (v "0.1.4") (h "13zh0g7d47a3szi2z4q2p3q8yph2ipw7q5gnsxvk34l44h6yjfpy")))

(define-public crate-pinot-0.1.5 (c (n "pinot") (v "0.1.5") (h "16l044217rshhmr89ml0ppi4s86v6mhq5kx3lhac8djhz0zh38vb")))

