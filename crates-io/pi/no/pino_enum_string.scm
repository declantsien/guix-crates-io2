(define-module (crates-io pi no pino_enum_string) #:use-module (crates-io))

(define-public crate-pino_enum_string-0.1.0 (c (n "pino_enum_string") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "14kmkcg3zyawdm4qa9gwlcclgll409vz8fa3nfis9m2vxd2fvigs")))

