(define-module (crates-io pi no pino_utils) #:use-module (crates-io))

(define-public crate-pino_utils-0.1.0 (c (n "pino_utils") (v "0.1.0") (d (list (d (n "pino_enum_string") (r "^0.1.0") (d #t) (k 0)))) (h "183129v41xq2bg2ykg8jflika5627wpdpjfpc58crwz3gz1lvz8y")))

(define-public crate-pino_utils-0.1.1 (c (n "pino_utils") (v "0.1.1") (d (list (d (n "pino_deref") (r "^0.1") (d #t) (k 0)) (d (n "pino_enum_string") (r "^0.1") (d #t) (k 0)))) (h "1qmh2v2lchh7rawfjcg584cyqkl45lvjhwdjgfq2c92h7i390brx")))

