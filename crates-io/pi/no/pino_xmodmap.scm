(define-module (crates-io pi no pino_xmodmap) #:use-module (crates-io))

(define-public crate-pino_xmodmap-0.1.0 (c (n "pino_xmodmap") (v "0.1.0") (h "0dlj5xqh3qd9lkpgn69xk5py7s5c2p7xgsvjqnjcr9ya3w08skn1")))

(define-public crate-pino_xmodmap-0.2.0 (c (n "pino_xmodmap") (v "0.2.0") (h "1c460nc5mnfm5by0qbs4l370sl839afcf78kq3ffrgijmng48pvs")))

