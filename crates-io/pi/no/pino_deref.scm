(define-module (crates-io pi no pino_deref) #:use-module (crates-io))

(define-public crate-pino_deref-0.1.0 (c (n "pino_deref") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "12gjdrbhgw2ragrpwrpp18pi0cm1nj26n2szyzp003y6gf77mv9d")))

(define-public crate-pino_deref-0.1.1 (c (n "pino_deref") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "029j478v8hjg230hn68dm2gia8fcakyas0rglclsd92q37vkhl3d")))

