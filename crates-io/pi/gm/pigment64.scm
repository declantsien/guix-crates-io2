(define-module (crates-io pi gm pigment64) #:use-module (crates-io))

(define-public crate-pigment64-0.2.2 (c (n "pigment64") (v "0.2.2") (d (list (d (n "clap") (r "^4.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "png") (r "^0.17.9") (d #t) (k 0)))) (h "1dan27cbj5zl1amjz8cv9icp40h75zmpkiyccn0zvhwyvx65y9f5")))

(define-public crate-pigment64-0.3.0 (c (n "pigment64") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "png") (r "^0.17.9") (d #t) (k 0)))) (h "072sx01fm4xxbf0das6mj2v6v9092dnz48is5fcz5iy9b7q7mklp")))

(define-public crate-pigment64-0.4.0 (c (n "pigment64") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)))) (h "084sq1njr6df816xymf1pvk6nhhi6s7ns3kzhjdsv0ns6grvyj4a") (y #t)))

(define-public crate-pigment64-0.4.1 (c (n "pigment64") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)))) (h "1qja2i3q9dl74sw3zmhrgl88m929j0ha75h611b3vlmsjinizx3j")))

(define-public crate-pigment64-0.4.2 (c (n "pigment64") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)))) (h "14wjbk05f7jgxszhi127q0csc1v1adsaghlyjhqck3z67520rygx")))

(define-public crate-pigment64-0.4.3 (c (n "pigment64") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)))) (h "1ggrrjp6jrjzf8aiwqqkl883fyvyfqjfbl74vsvsssj0mlr6yv84")))

(define-public crate-pigment64-0.4.4 (c (n "pigment64") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "png") (r "^0.17.13") (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.2") (d #t) (k 0)))) (h "1zcw747b7ar5f4rmafkm0743xfgxyh582q5nkqsir93k1j2h49sl") (y #t)))

(define-public crate-pigment64-0.4.5 (c (n "pigment64") (v "0.4.5") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "png") (r "^0.17.13") (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.2") (d #t) (k 0)))) (h "135imkp3y6krjjhgkwc7njgjsnxpprillxlylkv8l1gy8kg1nsf1")))

