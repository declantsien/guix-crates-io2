(define-module (crates-io pi mo pimoroni-trackball-driver) #:use-module (crates-io))

(define-public crate-pimoroni-trackball-driver-0.1.0 (c (n "pimoroni-trackball-driver") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1dic9a6zbvxqmk6gwl1vpv73129gx2f8jsc7l7dl6fa3i7ridbaa")))

(define-public crate-pimoroni-trackball-driver-0.1.1 (c (n "pimoroni-trackball-driver") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0n0lm40iwxk5gjbvx7bbdhs13gxyyz1afciw9sy0rlirqnysnsmn")))

