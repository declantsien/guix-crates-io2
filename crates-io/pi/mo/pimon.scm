(define-module (crates-io pi mo pimon) #:use-module (crates-io))

(define-public crate-pimon-0.1.0 (c (n "pimon") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "pi-hole-api") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (d #t) (k 0)) (d (n "tui") (r "^0.9.5") (d #t) (k 0)))) (h "01b94wwal9x0qc2yjq7h2k3z82rrxvj77rqllnj57hy5i84vd2bb")))

(define-public crate-pimon-0.2.0 (c (n "pimon") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pi-hole-api") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (d #t) (k 0)) (d (n "tui") (r "^0.14.0") (d #t) (k 0)))) (h "0axsqjig60ici2mqn16idmg3m65ksdg1zbpcibqnsr3fvvyrpgs2")))

(define-public crate-pimon-0.3.0 (c (n "pimon") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "pi-hole-api") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)))) (h "0bh6vpns68albmdjbrdf5yli7kf4d0xv8sn1i831lmr04h22ydpi")))

