(define-module (crates-io pi gr pigrabbit) #:use-module (crates-io))

(define-public crate-pigrabbit-0.1.0 (c (n "pigrabbit") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1bln0k3gi27a7ki4mdrbsp7zkmwvwgyqgiindz9x477dh6v9yz5r") (y #t)))

(define-public crate-pigrabbit-0.1.1 (c (n "pigrabbit") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1486a89cw7qk5x815jra1y9jcj9rpibiq45c773qmr4lis2x4dbq") (y #t)))

(define-public crate-pigrabbit-0.1.2 (c (n "pigrabbit") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("full"))) (d #t) (k 2)))) (h "155n26bk1xyy3g149sg82hynxdvzyf2dg0vn8s2a1bg4l4f0wxp8") (y #t)))

(define-public crate-pigrabbit-0.1.3 (c (n "pigrabbit") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0mqg3wf88q5880py7y1c4jm1l3lkx4khzlaz5cki2rgcl3082zdf") (y #t)))

(define-public crate-pigrabbit-0.1.4 (c (n "pigrabbit") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0kz21zmr20rbmalkhblbczr9rbiddwydgjnkw098pbqlv093wcff")))

