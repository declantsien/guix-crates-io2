(define-module (crates-io pi d_ pid_control) #:use-module (crates-io))

(define-public crate-pid_control-0.5.0 (c (n "pid_control") (v "0.5.0") (h "1pnm9c7z2vxs293030i3ij18yq4ilrk7ip89zk1sc6r0n33c88rk")))

(define-public crate-pid_control-0.5.1 (c (n "pid_control") (v "0.5.1") (h "1l7plisy7jvj6f7rskvk3fvx4657wpll1i5y6yl400gd3mnvdssv")))

(define-public crate-pid_control-0.6.0 (c (n "pid_control") (v "0.6.0") (h "1i9z8ghc15xq8wsq41915hgrlraglv85s0zvbc163wxbg660lv5z")))

(define-public crate-pid_control-0.7.0 (c (n "pid_control") (v "0.7.0") (h "0184m5vbbrfc2m787kds721if93kjq04fayqsak9x72y70y35z5j")))

(define-public crate-pid_control-0.7.1 (c (n "pid_control") (v "0.7.1") (h "12a586ljzw3s9hjjm5f5qib3d5q63mx4gipmp4b1g5ljq81r6575")))

(define-public crate-pid_control-0.7.2 (c (n "pid_control") (v "0.7.2") (h "0w9z8gqr14c85cjaz2qr79bgap49qcn61mz1p3l50hcfchr95wgi")))

