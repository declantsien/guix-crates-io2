(define-module (crates-io pi xt pixterm) #:use-module (crates-io))

(define-public crate-pixterm-0.1.0 (c (n "pixterm") (v "0.1.0") (d (list (d (n "ansipix") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1piszqflsnm720knvv4h1c0isk7c7hz6y0221ckf3yf2z2bd8hqm")))

(define-public crate-pixterm-0.1.1 (c (n "pixterm") (v "0.1.1") (d (list (d (n "ansipix") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "03fiqzcvdpqvn3ddpbjj04zm25bn3srnzizjkm0lmdrlwa3d86zx")))

(define-public crate-pixterm-0.1.2 (c (n "pixterm") (v "0.1.2") (d (list (d (n "ansipix") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0ymn3csn3712d3ykzxx8iz9cfxr6jqk717h6ja2l255yxghwamsv")))

(define-public crate-pixterm-0.2.0 (c (n "pixterm") (v "0.2.0") (d (list (d (n "ansipix") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ishxgw5b8xdm68s6qam9va68mxy3355044is6dcy8d8p8flxaqd")))

(define-public crate-pixterm-0.2.1 (c (n "pixterm") (v "0.2.1") (d (list (d (n "ansipix") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sr99f4bqpx9kpwi0q5dji8qi8i9p9h6pmhlzyqb172wl6jlsk43")))

(define-public crate-pixterm-0.3.0 (c (n "pixterm") (v "0.3.0") (d (list (d (n "ansipix") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fzl70g2099p40hl6gdkzz9vbgkypy3i5zfhsz9bmwmplp6509kb")))

(define-public crate-pixterm-0.4.0 (c (n "pixterm") (v "0.4.0") (d (list (d (n "ansipix") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d4n987nf5xvmb7jjn0zc56vx5a2iwr6jkqx8p7ilgx65qlsjdh9")))

