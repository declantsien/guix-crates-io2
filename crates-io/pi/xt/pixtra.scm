(define-module (crates-io pi xt pixtra) #:use-module (crates-io))

(define-public crate-pixtra-0.1.0 (c (n "pixtra") (v "0.1.0") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "01yipiyzg4zsry7w8184a1wn3b9zbs88nx6p3ialjmdsjdx0qbk9")))

(define-public crate-pixtra-0.1.1 (c (n "pixtra") (v "0.1.1") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "1sxpv398770j5x62rs7wmambw4j45nhlj43alcgnvsj76vybbqck")))

(define-public crate-pixtra-0.1.2 (c (n "pixtra") (v "0.1.2") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "10zdwid86qfiimak28j1ivcnw2cdm83h536c67v1hgp0mz3250aa")))

(define-public crate-pixtra-0.1.3 (c (n "pixtra") (v "0.1.3") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "036p3klvk5xmw6gvarww5ji8h2013nldshf79c55ajc545x4kvl3")))

(define-public crate-pixtra-0.1.4 (c (n "pixtra") (v "0.1.4") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "1lldsqqn37q7dwmjb43akp6pq6m9263cqn262r3ycmvavg2jazzj")))

(define-public crate-pixtra-0.1.5 (c (n "pixtra") (v "0.1.5") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "1dwzjz7a8kl0m6cxnrjj8hcylhiha603xarrmmb7f3dfi8rj7vpf")))

(define-public crate-pixtra-0.2.1 (c (n "pixtra") (v "0.2.1") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "1h97z14m3js8kym0qxjwv07176i5acs9l91plwwahlmc8xq2mf7m") (f (quote (("pixtra"))))))

(define-public crate-pixtra-0.2.2 (c (n "pixtra") (v "0.2.2") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "040z431psd735byzldxfnisdf25q25vh01ic2fkq0iaribp9f8vr") (f (quote (("pixtra"))))))

(define-public crate-pixtra-0.2.3 (c (n "pixtra") (v "0.2.3") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0y9ly31g5hk4j89c6b3n3fxs93mz9k09ljqg37688q1zln9i88ap") (f (quote (("pixtra"))))))

(define-public crate-pixtra-0.2.4 (c (n "pixtra") (v "0.2.4") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1h5zmqcpn5k8jmb124jpyz0vspnnkf9b28mmgpdx95l2fz7jy5mf") (f (quote (("pixtra"))))))

(define-public crate-pixtra-0.2.5 (c (n "pixtra") (v "0.2.5") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gycpzlhws967nwp28an2nj49qzvsfimlkb9bc8qbir0iwpcvras") (f (quote (("pixtra"))))))

