(define-module (crates-io pi nt pinto) #:use-module (crates-io))

(define-public crate-pinto-0.1.0 (c (n "pinto") (v "0.1.0") (h "170g1ms6wx4kfba627s5zkr5jnldqgch09mk2xgq1kbv4wkksqan")))

(define-public crate-pinto-0.2.0 (c (n "pinto") (v "0.2.0") (h "0qn34sfvk93qhxjy5nfjc69sxcmmdmccf8kawzly5l6l5m7m86mf")))

(define-public crate-pinto-0.3.0 (c (n "pinto") (v "0.3.0") (h "1rf5dn7cj3kllm43yskvr71xaf97wk4hm7pwmnd8gj3vadmsm872")))

(define-public crate-pinto-0.4.0 (c (n "pinto") (v "0.4.0") (h "1dga2nspl0z0lg3km2qp6ls2saqwh7vz83xbh0aj00w3lz06amm6")))

(define-public crate-pinto-0.5.0 (c (n "pinto") (v "0.5.0") (h "07xga2bz637zxkr0wwfm70jdi9y4wjar6flzaans6cch7m38x9j1")))

(define-public crate-pinto-0.6.0 (c (n "pinto") (v "0.6.0") (h "0j2r7zlpnh788ldvlxf10pglgsyyvrq6jxpv041bhhq2vg971c95")))

(define-public crate-pinto-0.6.1 (c (n "pinto") (v "0.6.1") (h "040c0dpimwfl40mirrsx7mspd43fcrgql7in4498xgvv04yf7dhm")))

