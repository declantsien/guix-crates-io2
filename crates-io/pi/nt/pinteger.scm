(define-module (crates-io pi nt pinteger) #:use-module (crates-io))

(define-public crate-pinteger-0.1.0 (c (n "pinteger") (v "0.1.0") (h "0wb78a5m6ddgggbnbnq7kb0pkzjlf5lwnywn69335pdqi6h8fl3j")))

(define-public crate-pinteger-0.1.1 (c (n "pinteger") (v "0.1.1") (h "1rygnmrpmn6lr75jnry1y2d8bxkv8bs200vy79jxnm1mv1k52g6j") (f (quote (("std") ("nightly") ("default" "std"))))))

