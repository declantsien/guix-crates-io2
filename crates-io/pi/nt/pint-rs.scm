(define-module (crates-io pi nt pint-rs) #:use-module (crates-io))

(define-public crate-pint-rs-0.1.0 (c (n "pint-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.3") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)))) (h "0mgysrvnix5kqi3pbaza944flrxznxh1xd1kwpdiddgywx2gj4m6")))

(define-public crate-pint-rs-0.1.1 (c (n "pint-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.3") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (d #t) (k 0)))) (h "0m16fkhkgcib87flhi91325qnm28xzrn72rk0hdxym00vsvryih3")))

(define-public crate-pint-rs-0.1.2 (c (n "pint-rs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.3") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (d #t) (k 0)))) (h "16zqc4mmdm18hy56rahmw7hlqj8hrr526lkmsbpbhr76g2vbzczg")))

(define-public crate-pint-rs-0.1.3 (c (n "pint-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (d #t) (k 0)))) (h "1syx5xp38nrw1qpgrq30gyg7ymz1d8ngczhwxay8qsi4smipk7ac")))

