(define-module (crates-io pi nt pint) #:use-module (crates-io))

(define-public crate-pint-0.1.0 (c (n "pint") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0563zli5dvcmwyl9dk90h762jrkrlbis7gvqq1nw7ssk88yy7xpg")))

