(define-module (crates-io pi -s pi-search) #:use-module (crates-io))

(define-public crate-pi-search-0.1.0 (c (n "pi-search") (v "0.1.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)))) (h "1g9jryk9rfg9frx4jab4471lwrvlckig6j7ncf6zxs6h3vxkzrly")))

(define-public crate-pi-search-0.2.0 (c (n "pi-search") (v "0.2.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)))) (h "1k95mr6j9bilnxripix24y3fwhzi8pwrhhccv8xwhxq4bb0jd196")))

(define-public crate-pi-search-0.2.1 (c (n "pi-search") (v "0.2.1") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)))) (h "1l75w06ha1ciz17yymkwv9gnpcprds2ijzd6bqk5nf30qhadbbz1")))

