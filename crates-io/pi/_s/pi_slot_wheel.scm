(define-module (crates-io pi _s pi_slot_wheel) #:use-module (crates-io))

(define-public crate-pi_slot_wheel-0.1.0 (c (n "pi_slot_wheel") (v "0.1.0") (d (list (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0nq7r9j5zm0mfcjw7i1lad6lh4favbj2q2p6z4s34vqzvk9fq0gk")))

(define-public crate-pi_slot_wheel-0.2.0 (c (n "pi_slot_wheel") (v "0.2.0") (d (list (d (n "pi_slot_deque") (r "^0.2") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "164xayav9xsmmav4dclrfrqk5b45mbcwkdabh4cglbx4c5jlg2h8")))

