(define-module (crates-io pi _s pi_sparialtree) #:use-module (crates-io))

(define-public crate-pi_sparialtree-0.4.0 (c (n "pi_sparialtree") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "parry2d") (r "^0.13") (d #t) (k 0)) (d (n "parry3d") (r "^0.13") (d #t) (k 0)) (d (n "pcg_rand") (r "^0.13") (d #t) (k 2)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_slotmap") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0xcss8sd98ci9lvzwzb97mp9x71a85k690q3znjbcdc8ilp1xf8p")))

