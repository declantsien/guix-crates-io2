(define-module (crates-io pi _s pi_slot) #:use-module (crates-io))

(define-public crate-pi_slot-0.1.0 (c (n "pi_slot") (v "0.1.0") (d (list (d (n "pi_arr") (r "^0.6") (d #t) (k 0)) (d (n "pi_key_alloter") (r "^0.3") (d #t) (k 0)) (d (n "pi_null") (r "^0.2") (d #t) (k 0)))) (h "1h8s8wcg4abq5wjb99qs4zlc40n0j13mhdjb163iziyqlglvn8w1")))

(define-public crate-pi_slot-0.1.1 (c (n "pi_slot") (v "0.1.1") (d (list (d (n "pi_arr") (r "^0.11") (d #t) (k 0)) (d (n "pi_key_alloter") (r "^0.4") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1wm2qn4l20xfrn464f71b2fr0cm2jf2klfy9y27wqbsan0j4k3k7")))

(define-public crate-pi_slot-0.1.4 (c (n "pi_slot") (v "0.1.4") (d (list (d (n "pi_arr") (r "^0.17") (d #t) (k 0)) (d (n "pi_key_alloter") (r "^0.4") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1czrz8v6b73gqs1bs0i1lpz0x3mqx1cjsldyp80066jy54bflyyy")))

(define-public crate-pi_slot-0.1.6 (c (n "pi_slot") (v "0.1.6") (d (list (d (n "pi_append_vec") (r "^0.3") (d #t) (k 0)) (d (n "pi_arr") (r "^0.17") (d #t) (k 0)) (d (n "pi_key_alloter") (r "^0.4") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1nwh7m36bsnaf2fm8h34r62x7n95zk9j2hwcsj4ap0s4hpdlzipy")))

(define-public crate-pi_slot-0.1.7 (c (n "pi_slot") (v "0.1.7") (d (list (d (n "pi_append_vec") (r "^0.3") (d #t) (k 0)) (d (n "pi_arr") (r "^0.17") (d #t) (k 0)) (d (n "pi_key_alloter") (r "^0.4") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0s8vx5azkhjlwckwzl3hv30w8wybk0g86mlw22i1kx9nzb97131p")))

(define-public crate-pi_slot-0.1.8 (c (n "pi_slot") (v "0.1.8") (d (list (d (n "pi_append_vec") (r "^0.3") (d #t) (k 0)) (d (n "pi_arr") (r "^0.18") (d #t) (k 0)) (d (n "pi_key_alloter") (r "^0.4") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1iiha81pn4fj45i3vf3gyp0d2mgjam0f7gmi7qqzwfrqaji5mrbr")))

(define-public crate-pi_slot-0.2.0 (c (n "pi_slot") (v "0.2.0") (d (list (d (n "pi_append_vec") (r "^0.4") (d #t) (k 0)) (d (n "pi_arr") (r "^0.18") (d #t) (k 0)) (d (n "pi_key_alloter") (r "^0.5") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "0zyrz56w63h19dzzyn11llkiq9w2m3rlkwrzxjd246f1k0j4a5bh")))

