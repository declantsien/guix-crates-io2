(define-module (crates-io pi _s pi_slot_deque) #:use-module (crates-io))

(define-public crate-pi_slot_deque-0.1.0 (c (n "pi_slot_deque") (v "0.1.0") (d (list (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1sqdk07gg6qlm6q977x9qbgnzal9z0isdb953n541m4lqb9w2ph5")))

(define-public crate-pi_slot_deque-0.1.1 (c (n "pi_slot_deque") (v "0.1.1") (d (list (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0hz0k3amknhsj8pbb45svvz2nh8h4hg47v8chn78mn5zhwnaaasx")))

(define-public crate-pi_slot_deque-0.1.3 (c (n "pi_slot_deque") (v "0.1.3") (d (list (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1mgld32pvcl0ladw5sdi63w9f7qdcsin1p1vn8a6y8lda8h7r2ad")))

(define-public crate-pi_slot_deque-0.2.0 (c (n "pi_slot_deque") (v "0.2.0") (d (list (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0lapy0jsl01bs7xdggj0hfg1808f4iibf2nqbghp1h1s2g15h68m")))

(define-public crate-pi_slot_deque-0.2.1 (c (n "pi_slot_deque") (v "0.2.1") (d (list (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0dxddwfc2ydx7yxcpkghkkgvwwbj85awp1bgqdc5any4190l530c")))

