(define-module (crates-io pi _s pi_share) #:use-module (crates-io))

(define-public crate-pi_share-0.1.0 (c (n "pi_share") (v "0.1.0") (h "063w6zffv4cwlwpy9f6rkraxyyspvcbfyachma6jqvrnmgsmcczj") (f (quote (("rc") ("defualt"))))))

(define-public crate-pi_share-0.1.1 (c (n "pi_share") (v "0.1.1") (h "00rr82jb96rv3ljisyf9ja5wyfj87xg58mgk0kpap06lvag07jwz") (f (quote (("rc") ("defualt"))))))

(define-public crate-pi_share-0.1.2 (c (n "pi_share") (v "0.1.2") (h "0hmpqfri71w58fl3jc8swq639vp4fsqglz8fbck3nqa1pc3fzfaa") (f (quote (("rc") ("defualt"))))))

(define-public crate-pi_share-0.1.3 (c (n "pi_share") (v "0.1.3") (h "066fxjlqv01w6rj9bm0j2462a6a6c24wv80wqhy0hgnh8yrl5jj4") (f (quote (("rc") ("defualt"))))))

(define-public crate-pi_share-0.2.0 (c (n "pi_share") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1b15ah4rk28vrsacxa5wj2p2m4lra2asnasmimr89vkpvcykh7mk") (f (quote (("rc") ("defualt"))))))

(define-public crate-pi_share-0.3.0 (c (n "pi_share") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0j8gqmzjcx6m1rl54yd8fna1bfap9f0xfgb11vjl2iahs449wclh") (f (quote (("rc") ("defualt"))))))

(define-public crate-pi_share-0.4.0 (c (n "pi_share") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0hpzs7bfwaq7x614c00rc7kykskn25w7dhad84jb1fn7ls3c24dq") (f (quote (("rc") ("defualt"))))))

(define-public crate-pi_share-0.4.1 (c (n "pi_share") (v "0.4.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "01s0l40aybfyiaswwkqj850bql22f8z15d4bjc5qfkcsyvhiyp06") (f (quote (("rc") ("defualt"))))))

(define-public crate-pi_share-0.4.2 (c (n "pi_share") (v "0.4.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "16zdm62nvhf54frfcy816rrkqalr004ayw3bjpk55jkci2xphs4f") (f (quote (("serial") ("rc"))))))

(define-public crate-pi_share-0.4.3 (c (n "pi_share") (v "0.4.3") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0dr3yw3gnsczvcnr063qdd2qialf16b6csrk12ig1wkpdhbrbc8q") (f (quote (("serial") ("rc"))))))

(define-public crate-pi_share-0.4.4 (c (n "pi_share") (v "0.4.4") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0w46zd9rd42lm6v99hly4cnygrf1rny38g03a4jg2lawghc8rg2v") (f (quote (("serial") ("rc"))))))

(define-public crate-pi_share-0.4.5 (c (n "pi_share") (v "0.4.5") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0bf0nb63a7i6lpj0s71m870c0q235xjl30qq4lirdr6c2wp59gp3") (f (quote (("serial") ("rc"))))))

(define-public crate-pi_share-0.4.6 (c (n "pi_share") (v "0.4.6") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1j9l3nr3mql9dmxm73535nqi9wrka9qc0p0dva4d9dcww9mpdjr3") (f (quote (("serial") ("rc"))))))

(define-public crate-pi_share-0.4.7 (c (n "pi_share") (v "0.4.7") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1bzbkisdpv6ibmkhn4q6gf3pr83ds1z8psq1k4csddm83nbq4dgd") (f (quote (("serial") ("rc"))))))

(define-public crate-pi_share-0.4.8 (c (n "pi_share") (v "0.4.8") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0bdzq98vjwcgs8l3f8r068d9qfby5jckfgcxl4w36rg12wnca2dl") (f (quote (("serial") ("rc")))) (y #t)))

(define-public crate-pi_share-0.4.9 (c (n "pi_share") (v "0.4.9") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1xvpqglgz1cdr2akva22dq2rlw4rc2w10llxigrf4hphc1zgfsk7") (f (quote (("serial") ("rc"))))))

(define-public crate-pi_share-0.4.10 (c (n "pi_share") (v "0.4.10") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1m8khci25jh5ir8a7xislip0c4q658f404g9jc0jfwb1k26570im") (f (quote (("serial") ("rc"))))))

(define-public crate-pi_share-0.4.11 (c (n "pi_share") (v "0.4.11") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "14y77rb2nrhhhdah39c6zx1iv2pgid8bz0n1vzpvl932m749la3z") (f (quote (("serial") ("rc"))))))

(define-public crate-pi_share-0.4.12 (c (n "pi_share") (v "0.4.12") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "03sck5kgqp8pb587hyjya07sry9g24xd254pjsfwas3ymykdij96") (f (quote (("serial") ("rc"))))))

(define-public crate-pi_share-0.4.13 (c (n "pi_share") (v "0.4.13") (h "04wkf9ybr12xndavxcm8xa7y7xr308y1ar9jdyf8bqqiq2iafraf") (f (quote (("serial") ("rc"))))))

(define-public crate-pi_share-0.4.14 (c (n "pi_share") (v "0.4.14") (h "0bbgxwnzr1xcv4sb5by51z7sh45zaahssmwy4x9m9dr3mr9963yg") (f (quote (("serial") ("rc"))))))

(define-public crate-pi_share-0.4.15 (c (n "pi_share") (v "0.4.15") (h "0a9lyswk9ar7c6735rk154hz7zwg518rc8cavdwma0ak3lqwfz82") (f (quote (("serial") ("rc"))))))

