(define-module (crates-io pi xi pixiee) #:use-module (crates-io))

(define-public crate-pixiee-0.1.0 (c (n "pixiee") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cherrygh") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.4") (d #t) (k 0)))) (h "0q4fv67v8k3qb398kxr7yb0gl2zs5k42agdg38kny2yfn2bn0fc5")))

(define-public crate-pixiee-0.1.1 (c (n "pixiee") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cherrygh") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.4") (d #t) (k 0)))) (h "1l4rc90r6f1p1k1dwqsfqkh8mfpgjrkg3c3kvj043k6md09mnvbl")))

