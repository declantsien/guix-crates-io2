(define-module (crates-io pi xi pixiv-rust) #:use-module (crates-io))

(define-public crate-pixiv-rust-0.1.0 (c (n "pixiv-rust") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1fy0bzsxfhg77p1j6519lkipb3bhiasqlkphwh7j232myqmbdczj")))

