(define-module (crates-io pi xi pixiv) #:use-module (crates-io))

(define-public crate-pixiv-0.1.0 (c (n "pixiv") (v "0.1.0") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "1k8g37vi68c7wjxyh240lr2lgwkbnjgvbzrla8cfh7pbqkh0w2f2")))

(define-public crate-pixiv-0.1.1 (c (n "pixiv") (v "0.1.1") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.11") (d #t) (k 0)))) (h "1wpiwzv2ywi7lzwd4flj13zwhcyyir5cr53v6ipzz6kp544srkvm")))

(define-public crate-pixiv-0.1.2 (c (n "pixiv") (v "0.1.2") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "kankyo") (r "~0.2") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.11") (d #t) (k 0)))) (h "13a752ym0ji3lkx751i3hnpdjra9yipr58x74i7w866hhid99irc")))

(define-public crate-pixiv-0.1.3 (c (n "pixiv") (v "0.1.3") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "kankyo") (r "~0.2") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.11") (d #t) (k 0)))) (h "1pilgh0z1hxqnf2y8c3kkd82pi5z03flmvwf1znp70vr7ds208y5")))

(define-public crate-pixiv-0.1.4 (c (n "pixiv") (v "0.1.4") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "kankyo") (r "~0.2") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.11") (d #t) (k 0)))) (h "0408p0xy7hviaq7hkpyasxa145yv3amkyp1kdyhiv1adnhzbanyn")))

(define-public crate-pixiv-0.2.0 (c (n "pixiv") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "kankyo") (r "~0.2") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.11") (d #t) (k 0)))) (h "1chkaxnbpwpzva03kf3masiqarygdq52qsfkmpir96kdh682rz3g")))

