(define-module (crates-io pi xi pixiv-rs) #:use-module (crates-io))

(define-public crate-pixiv-rs-0.1.0 (c (n "pixiv-rs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1s9m1i80223z572iqw3inq0a3w4xkg7zj8s8q2w9szja57d1ykpp") (f (quote (("rustls" "reqwest/rustls"))))))

