(define-module (crates-io pi ec piecetable) #:use-module (crates-io))

(define-public crate-piecetable-0.1.0 (c (n "piecetable") (v "0.1.0") (h "06ipfhimsnb4gm4mz7f1zkkp30gypxz89xp2s5763ib5xa3mdqli")))

(define-public crate-piecetable-0.2.0 (c (n "piecetable") (v "0.2.0") (h "09b2j6366x8qvqnc2q7v31n4hy6bx4x0974dbajp7wlr9fz0z5rm")))

(define-public crate-piecetable-0.2.1 (c (n "piecetable") (v "0.2.1") (h "0yc9ixnlk4k44pxi9nzy9k1jlidg3ipvkkp3xbax9p54md9wr782")))

