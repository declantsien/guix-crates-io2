(define-module (crates-io pi ec pieces) #:use-module (crates-io))

(define-public crate-pieces-0.1.0 (c (n "pieces") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "0m0jmd9sj5sv0jhfhnwh41flljsn6isc5f5hd47jaz3np4cy4icv")))

(define-public crate-pieces-0.1.1 (c (n "pieces") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "0xh424c2dsyig00sb5gakp3lfai08f2d0d52fxd5wfg677srg8ik")))

(define-public crate-pieces-0.2.0 (c (n "pieces") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)))) (h "0dkgn31awp5b83xyaqxk0d6kvpns5j25qfhw5whfcflx3ailaa3k")))

