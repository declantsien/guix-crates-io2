(define-module (crates-io pi ec piechart) #:use-module (crates-io))

(define-public crate-piechart-0.1.0 (c (n "piechart") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "149dkg34l42gly933j5nwp4sblbxhz2yh5brsnbif6mfzv7w7lmr")))

(define-public crate-piechart-0.1.1 (c (n "piechart") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "0dkwkga3j2v13dlg5j4a3n7hdhmr0qp1w9q2ihdy1s40jp3vsc1q")))

(define-public crate-piechart-0.1.2 (c (n "piechart") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "05r98546yf3m9j2q8p7j0jsbmz0l9w8in17gpg82rqzawcna5nf9")))

(define-public crate-piechart-1.0.0 (c (n "piechart") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("std" "suggestions" "color"))) (o #t) (k 0)))) (h "12a0rnnzan4g8zx6sfb6m1jzcgq3yijf32cjdh35qa4yhxr06wvr") (f (quote (("cli" "clap"))))))

