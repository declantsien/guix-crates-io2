(define-module (crates-io pi ec piece_table_rs) #:use-module (crates-io))

(define-public crate-piece_table_rs-0.1.0 (c (n "piece_table_rs") (v "0.1.0") (h "1xxsly9pqylighfa9c8wpgzh9z2272aw2s04dq0qsj0q0zif1scd") (y #t)))

(define-public crate-piece_table_rs-0.1.1 (c (n "piece_table_rs") (v "0.1.1") (h "1l0rj1k4vzycfwxhk4r84ylm2q23p9w4gs0rvfi21frrixin6sz9") (y #t)))

(define-public crate-piece_table_rs-0.1.2 (c (n "piece_table_rs") (v "0.1.2") (h "0rqnkxln2ifshzg47zbfkir9nrhfl5gxasl9z6jxgiiiiqxrv22p") (y #t)))

(define-public crate-piece_table_rs-0.1.3 (c (n "piece_table_rs") (v "0.1.3") (h "08jn53gv246pb86jsm7kmpm7cqy7mvl519qdlzx5lvnbkq57d4ag") (y #t)))

(define-public crate-piece_table_rs-0.1.4 (c (n "piece_table_rs") (v "0.1.4") (h "0pq1hiaijhr1x43f5dy75cbn49r7d4n06qq4sgddix69phh8sd6l")))

