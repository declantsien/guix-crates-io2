(define-module (crates-io pi ec piecrust-macros) #:use-module (crates-io))

(define-public crate-piecrust-macros-0.1.0 (c (n "piecrust-macros") (v "0.1.0") (d (list (d (n "piecrust-uplink") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.34") (d #t) (k 0)) (d (n "syn") (r "^2.0.44") (f (quote ("full"))) (d #t) (k 0)))) (h "1m9ayx9a48hdwd72l9zvxasvy5syiv32d65xgl5ccvz609n23kps")))

