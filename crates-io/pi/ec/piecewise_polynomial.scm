(define-module (crates-io pi ec piecewise_polynomial) #:use-module (crates-io))

(define-public crate-piecewise_polynomial-0.1.0 (c (n "piecewise_polynomial") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "arbitrary") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)))) (h "017ml4gyvrfpkkn4jcpxrdydyqag6vpjlnqc1dsdizjp292hvpiy")))

