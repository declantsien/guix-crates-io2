(define-module (crates-io pi ec piecewise-linear) #:use-module (crates-io))

(define-public crate-piecewise-linear-0.1.0 (c (n "piecewise-linear") (v "0.1.0") (d (list (d (n "geo") (r "^0.12.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0irmbdxxyv8f8vwbvpxma6bsy1yz3r9gnl0cvz83n81ksf31yinn") (f (quote (("use-serde" "serde" "geo/use-serde") ("default"))))))

(define-public crate-piecewise-linear-0.2.0 (c (n "piecewise-linear") (v "0.2.0") (d (list (d (n "geo") (r "^0.26.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09gkh284bhmmxr1ycm69d3a6ac2wprgfcklh38zqm027gi33aq13") (f (quote (("use-serde" "serde" "geo/use-serde") ("default"))))))

