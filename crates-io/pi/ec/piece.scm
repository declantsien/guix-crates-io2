(define-module (crates-io pi ec piece) #:use-module (crates-io))

(define-public crate-piece-0.1.0 (c (n "piece") (v "0.1.0") (h "1rf5x7gfkma8my8kqgfzixf5mwi78arvzg0fk2sywz62gdxk7c68")))

(define-public crate-piece-0.1.1 (c (n "piece") (v "0.1.1") (h "1s7hp9n1w65cvaccsnwsmn00h42jssipzy9l25z7n2dx39sxaym8")))

(define-public crate-piece-0.1.2 (c (n "piece") (v "0.1.2") (h "1s7davcjl7c4bs9s8qcl5s358dd2rg42kzwqs37jy5l2xhfz62yb")))

(define-public crate-piece-0.1.3 (c (n "piece") (v "0.1.3") (h "1gzrv2fi3ngdhcrbx8n7zsq7f40ihn3iyidb0s8pc91ld1s1sflf")))

(define-public crate-piece-0.2.0 (c (n "piece") (v "0.2.0") (h "11vvbcjsb6pbkyl7l2ywczgqhhiqk4xjjd87pa68ijqw5nmix6s4")))

(define-public crate-piece-0.2.1 (c (n "piece") (v "0.2.1") (d (list (d (n "allocator-api2") (r "^0.2.15") (o #t) (k 0)))) (h "09mqh9368sdpjg92m1flwci99drb1c3ccyifw1grnwxlsww4yhcq") (f (quote (("stable" "allocator-api2") ("default") ("all" "vec" "boxed")))) (s 2) (e (quote (("vec" "allocator-api2?/alloc") ("boxed" "allocator-api2?/alloc"))))))

