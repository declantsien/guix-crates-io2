(define-module (crates-io pi ec piecrust-uplink) #:use-module (crates-io))

(define-public crate-piecrust-uplink-0.1.0-alpha (c (n "piecrust-uplink") (v "0.1.0-alpha") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1l4b3kazvmpqiv9cmw24j997cvq1ldmirjdqcc03m6csz1kkvnsf") (f (quote (("std") ("default" "std"))))))

(define-public crate-piecrust-uplink-0.1.0 (c (n "piecrust-uplink") (v "0.1.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1w9wnv6hbi46lcz0zhw94qmzyjgvbc2sahdhapgci29pg615q1sl") (f (quote (("std") ("default" "std"))))))

(define-public crate-piecrust-uplink-0.2.0 (c (n "piecrust-uplink") (v "0.2.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1wngyla7jnsrx4xn73nmlabhm8zglxpyipxixna2ifb0wcl0h9r9") (f (quote (("std") ("default" "std"))))))

(define-public crate-piecrust-uplink-0.3.0 (c (n "piecrust-uplink") (v "0.3.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1h2hh7g957q4sb5x37ymswwjhk4zh02digirmwmpk2cvx50bq7p0") (f (quote (("std") ("default" "std"))))))

(define-public crate-piecrust-uplink-0.4.0 (c (n "piecrust-uplink") (v "0.4.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)))) (h "0d25qk6blwlij00y4nfp1p3m9jk6pj6iampnp1dv4qdzf2vb2fnd") (f (quote (("std") ("default" "std"))))))

(define-public crate-piecrust-uplink-0.5.0 (c (n "piecrust-uplink") (v "0.5.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)))) (h "1kscjrmql5hnlnb2i9izwx1w48m04sq9bwla8nhj81wxrgmipvy2") (f (quote (("std") ("default" "std"))))))

(define-public crate-piecrust-uplink-0.1.1 (c (n "piecrust-uplink") (v "0.1.1") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1hks7syr2knqa6q4pwk6661avlhaigsh3pc014ilhn2vkx290ff7") (f (quote (("std") ("default" "std"))))))

(define-public crate-piecrust-uplink-0.6.0 (c (n "piecrust-uplink") (v "0.6.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)))) (h "0jr49dm22rslrf748x1yz5pcr4f51z5kxh0ikwd6dm71sqxp6m7f") (f (quote (("debug") ("abi"))))))

(define-public crate-piecrust-uplink-0.6.1 (c (n "piecrust-uplink") (v "0.6.1") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)))) (h "0wmcm63if43a1ac8cx6kqqjkrpwsqvh1gayjf16kjbyixp1g0k3m") (f (quote (("debug") ("abi"))))))

(define-public crate-piecrust-uplink-0.7.0 (c (n "piecrust-uplink") (v "0.7.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)))) (h "08dns0wrard8n7csnsqy6pw7nxksr9pvmm3n1xink5swdrrq44a5") (f (quote (("debug") ("abi"))))))

(define-public crate-piecrust-uplink-0.7.1 (c (n "piecrust-uplink") (v "0.7.1") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)))) (h "1qw8y8y6zqhrd3nixp7hyqccn93vqa8wawcyvalr9r1l7snkh075") (f (quote (("debug") ("abi"))))))

(define-public crate-piecrust-uplink-0.8.0-rc.0 (c (n "piecrust-uplink") (v "0.8.0-rc.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)))) (h "0qwfpa5vwrjh5pnpkzyizaqcmg3j1h8p8v4psx3hjgd85h9mqr1l") (f (quote (("debug") ("abi"))))))

(define-public crate-piecrust-uplink-0.8.0 (c (n "piecrust-uplink") (v "0.8.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)))) (h "1bc75kxa3v99nq2l69c20iixm7qrwwbnl9cq4ik1r0xgah3qvb10") (f (quote (("debug") ("abi"))))))

(define-public crate-piecrust-uplink-0.9.0-rc.0 (c (n "piecrust-uplink") (v "0.9.0-rc.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)))) (h "0bwn3bafavhfwkpiqgybx7v6fxgcqg6imx1hhihsd3w2qaqm4qra") (f (quote (("debug") ("abi"))))))

(define-public crate-piecrust-uplink-0.9.0 (c (n "piecrust-uplink") (v "0.9.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)))) (h "1s313gdprg9hd2xw47j0a1kvk11jl4wfqp8z5270fjz8bpvpda7p") (f (quote (("debug") ("abi"))))))

(define-public crate-piecrust-uplink-0.10.0-rc.0 (c (n "piecrust-uplink") (v "0.10.0-rc.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)))) (h "1d5rm1r1vcdyzm1iv92hdfwi4qvkfmik7i4vq63k7z5272incr9k") (f (quote (("debug") ("abi"))))))

(define-public crate-piecrust-uplink-0.10.0 (c (n "piecrust-uplink") (v "0.10.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc"))) (k 0)))) (h "1d3ycnx42g11d9f2inch0bc4kwfrhxbilirq21w7157q4caz78ap") (f (quote (("debug") ("abi"))))))

(define-public crate-piecrust-uplink-0.11.0-rc.0 (c (n "piecrust-uplink") (v "0.11.0-rc.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc" "validation"))) (k 0)))) (h "1slgcyf0s3nhadg7gnn0aw5q3pfcar4zr22inflyr1sfnc24bzzc") (f (quote (("debug") ("abi")))) (y #t)))

(define-public crate-piecrust-uplink-0.11.0-rc.1 (c (n "piecrust-uplink") (v "0.11.0-rc.1") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc" "validation"))) (k 0)))) (h "1k8v6fd3hbdjy9cbc66a0nzpm6lsmy5r1xvpg4v9iw0xr9njlcl3") (f (quote (("debug") ("abi")))) (y #t)))

(define-public crate-piecrust-uplink-0.11.0-rc.2 (c (n "piecrust-uplink") (v "0.11.0-rc.2") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc" "validation"))) (k 0)))) (h "083rzwspz0qb41l9s4awmgs2c6f1s0b1nbaq47bifw25d30v0743") (f (quote (("debug") ("abi"))))))

(define-public crate-piecrust-uplink-0.11.0 (c (n "piecrust-uplink") (v "0.11.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc" "validation"))) (k 0)))) (h "1jmd1gwgihwk8592mh9vaxlxnn6cmxp8q2sqpf2c5153rvqnnlrr") (f (quote (("debug") ("abi"))))))

(define-public crate-piecrust-uplink-0.12.0 (c (n "piecrust-uplink") (v "0.12.0") (d (list (d (n "bytecheck") (r "^0.6") (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("size_32" "alloc" "validation"))) (k 0)))) (h "12cd21hbr4xq0gih3pfslckfkn16w4xyk0jfd7l51jj06cq6f5b1") (f (quote (("debug") ("abi"))))))

