(define-module (crates-io pi th pithy) #:use-module (crates-io))

(define-public crate-pithy-0.1.0 (c (n "pithy") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1ksn2by8pr2vmc1g9cw5i74gvpr0c4gwr2m90bccvnc4ayijn8q6")))

(define-public crate-pithy-0.1.1 (c (n "pithy") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1gmzig7g57si39pf9m1443gv1dyx2s2a01d82yjijbhnmkp5kmrc")))

(define-public crate-pithy-0.1.2 (c (n "pithy") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "033fj1454gk7227gg8ksfpny7wcql3d2avh0pwp0n45jacnk3zb0")))

(define-public crate-pithy-0.1.3 (c (n "pithy") (v "0.1.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "00n76sjzl78v3v5ai2f8z44p6bpsh0wzp1a9f1hcpqg06qg4apfn")))

(define-public crate-pithy-0.1.4 (c (n "pithy") (v "0.1.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "04nlys1fvcwxbcbyds39wasik0jzvwiphp6ng8y0jjvqvr9rifhw") (y #t)))

(define-public crate-pithy-0.1.5 (c (n "pithy") (v "0.1.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "04bf19mgd5amfgxv91vppp9lqiar2vd2f13bfgig62sf7s7vl9b2")))

(define-public crate-pithy-0.1.6 (c (n "pithy") (v "0.1.6") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1a6xfhpiwwx7ikp6jqp28aqqpacj62fzv6m7cs6c6ncp931q84wj")))

(define-public crate-pithy-0.1.7 (c (n "pithy") (v "0.1.7") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0gxyq3wlshf9510g3c81prbrv5wvphdq6mh4scaj553c4qkd1yns")))

