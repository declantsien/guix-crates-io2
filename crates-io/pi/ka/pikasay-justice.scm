(define-module (crates-io pi ka pikasay-justice) #:use-module (crates-io))

(define-public crate-pikasay-justice-0.1.0 (c (n "pikasay-justice") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "colour") (r "^0.6.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1vzff1qyff601yqq39pxly4an84w8r5x0ingmsxwdkj2qz0nayvg")))

