(define-module (crates-io pi ka pika) #:use-module (crates-io))

(define-public crate-pika-0.0.0 (c (n "pika") (v "0.0.0") (h "0n40gx24pknw6mdfpvy9j6ck6k8cvyzrfajfhmnxz10bkyb3wl6j")))

(define-public crate-pika-0.1.1 (c (n "pika") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "mac_address") (r "^1.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0fk8xfvicj0g91rbp59hnqg7cffxikxpgi92nz2jdqyhcas1fl6w")))

(define-public crate-pika-0.1.2 (c (n "pika") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "mac_address") (r "^1.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1l11kcxi3hd73lc4z1cxz48nrqiqivibys6vxv0pjbx66g52vmxj")))

(define-public crate-pika-0.1.3 (c (n "pika") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "mac_address") (r "^1.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1cn7hwpk6mwk10mv3qf2a7gg3vdgvzlbxmaalbnr678ql7iwas0c")))

