(define-module (crates-io pi ka pikasay) #:use-module (crates-io))

(define-public crate-pikasay-0.1.0 (c (n "pikasay") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0zpniz44ssv8y81fhajyg21l1zvdmjj4rcrg8nccpcf7d6xrka99")))

(define-public crate-pikasay-0.2.0 (c (n "pikasay") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1q7931ws12mrfzgvizkvgfk6fi0srxnn2g7a4l4lp0gwaawxk5cp")))

