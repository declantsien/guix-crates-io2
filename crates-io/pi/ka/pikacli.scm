(define-module (crates-io pi ka pikacli) #:use-module (crates-io))

(define-public crate-pikacli-0.1.0 (c (n "pikacli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1yxy5axw7k2ggd2dj2cvc5gn5ml49jm32v8y02qcdg8q52wm72n3")))

