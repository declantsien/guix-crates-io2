(define-module (crates-io pi ka pika-id) #:use-module (crates-io))

(define-public crate-pika-id-0.1.0 (c (n "pika-id") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "mac_address") (r "^1.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1gf0wmbksvvy4n8lsrl5f726j7gc86067wc8gmj2m7dxqh7whhji")))

(define-public crate-pika-id-0.1.1 (c (n "pika-id") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "mac_address") (r "^1.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1q3980cavjq7v0ma03p7c6g309krzvrbds2sa9rsddg3wsxzs74n")))

