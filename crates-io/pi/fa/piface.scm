(define-module (crates-io pi fa piface) #:use-module (crates-io))

(define-public crate-piface-0.1.0 (c (n "piface") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "eui48") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\", target_os = \"freebsd\"))") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xc5pc4ak4jq8sxnsbqg8vq1ab7lslzp454y4qjabs8q9l05amb0")))

(define-public crate-piface-0.1.1 (c (n "piface") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "eui48") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\", target_os = \"freebsd\"))") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f1bg9b64sliikmkgnqdybjxms89bazgxzyb38n9r5m40yhpvlsx")))

