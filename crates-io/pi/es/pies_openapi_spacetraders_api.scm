(define-module (crates-io pi es pies_openapi_spacetraders_api) #:use-module (crates-io))

(define-public crate-pies_openapi_spacetraders_api-2.0.0 (c (n "pies_openapi_spacetraders_api") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1nbgnsfg74dfdrd06mlyjacbkr4ghkffs54la2fi8fdj047mg33g") (y #t)))

(define-public crate-pies_openapi_spacetraders_api-2023.6.24 (c (n "pies_openapi_spacetraders_api") (v "2023.6.24") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "093l3xig5r1q18gz4hgz8326f1lp2ywn0lgdxwbpdzx6dd2daq72")))

(define-public crate-pies_openapi_spacetraders_api-2023.9.30 (c (n "pies_openapi_spacetraders_api") (v "2023.9.30") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1sgxr7m3b2nw8q3cmcycx6iqdxggs8wq0y1h49980631ldlmjwfs")))

