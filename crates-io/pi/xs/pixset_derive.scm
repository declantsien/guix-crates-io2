(define-module (crates-io pi xs pixset_derive) #:use-module (crates-io))

(define-public crate-pixset_derive-0.0.1 (c (n "pixset_derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "01mqli9g3zwxw4idxj0qz4dj8i9my44p8ayj8sk2jqm88r84z1n8")))

(define-public crate-pixset_derive-0.0.2 (c (n "pixset_derive") (v "0.0.2") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "06ggn1y386014zsngrlg4iacv0k73nkcm8an7r5r0x7r4pim2xkl")))

(define-public crate-pixset_derive-0.0.3 (c (n "pixset_derive") (v "0.0.3") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0kymd61gg7cgy2irp5vmsm15hk6hi98gzsmarbzsbf54i087lynw")))

(define-public crate-pixset_derive-0.0.4 (c (n "pixset_derive") (v "0.0.4") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1v2hg4cxqqfi5m23rpi9303c782nmi6bm2l4iaq2jll9wrhqrv9d")))

(define-public crate-pixset_derive-0.0.5 (c (n "pixset_derive") (v "0.0.5") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1dhj9jxahxmrm1xnyzn80zb898jsxps77n0lygl63pc1z14yw86k")))

