(define-module (crates-io pi xs pixset) #:use-module (crates-io))

(define-public crate-pixset-0.0.1 (c (n "pixset") (v "0.0.1") (d (list (d (n "pixset_derive") (r "^0.0.1") (d #t) (k 0)))) (h "0fm0hyqkl6lprsyf6acp6k406wrsgavs7vivxckfwz4an48k4dhv")))

(define-public crate-pixset-0.0.2 (c (n "pixset") (v "0.0.2") (d (list (d (n "pixset_derive") (r "^0.0.2") (d #t) (k 0)))) (h "0mv0zbjlc6d2m4n4c7a0q85arhx39am8l3wpa0jkw2nn117j3rcs")))

(define-public crate-pixset-0.0.3 (c (n "pixset") (v "0.0.3") (d (list (d (n "pixset_derive") (r "^0.0.3-pre") (d #t) (k 0)))) (h "115awkm52kbl8zx11zpnf0jhgpdn981qvl37dzi7k0y237m24bq8")))

(define-public crate-pixset-0.0.4 (c (n "pixset") (v "0.0.4") (d (list (d (n "pixset_derive") (r "^0.0.4-pre") (d #t) (k 0)))) (h "0qlzkxjp4lfdp03iyhhvcq0a5vqsbxjn3sl1k19dhgz2vdq0hpjd")))

(define-public crate-pixset-0.0.5 (c (n "pixset") (v "0.0.5") (d (list (d (n "pixset_derive") (r "^0.0.5") (d #t) (k 0)))) (h "1kbl62igz0v887s0ia86073rlcjagq83zk8rlxxq3l6xsqk61j2q")))

(define-public crate-pixset-0.0.6 (c (n "pixset") (v "0.0.6") (d (list (d (n "pixset_derive") (r "^0.0.5") (d #t) (k 0)))) (h "0pl7bjnv5kysp9p8kg64pmm5q9hjjns6fbnxbin4lnq16zfbni86")))

