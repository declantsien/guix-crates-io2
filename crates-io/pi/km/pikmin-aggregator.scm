(define-module (crates-io pi km pikmin-aggregator) #:use-module (crates-io))

(define-public crate-pikmin-aggregator-0.1.0 (c (n "pikmin-aggregator") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mysql") (r "^15.0") (f (quote ("ssl"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)))) (h "16zxlniw64mppaak0ayb5y0ipv5dv09y0hjf4qy9bp4d9pmq3q3k")))

