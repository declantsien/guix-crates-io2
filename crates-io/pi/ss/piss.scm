(define-module (crates-io pi ss piss) #:use-module (crates-io))

(define-public crate-piss-0.1.0 (c (n "piss") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "177br37szs860n99y2cqylv2nicgs3j9a1l6b030vzdranfrknah")))

(define-public crate-piss-0.1.1 (c (n "piss") (v "0.1.1") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1l1zab40jlmbm6kr0hwyarxkdfh7m05qwj1qsnm23wp6d7a52gx3")))

(define-public crate-piss-0.1.2 (c (n "piss") (v "0.1.2") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1c4jd1hzamqai8nwiz04si6hv65cjqzqip6ydzyzw9q1n4hbawxb")))

