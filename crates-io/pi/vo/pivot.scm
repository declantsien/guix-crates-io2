(define-module (crates-io pi vo pivot) #:use-module (crates-io))

(define-public crate-pivot-0.1.0 (c (n "pivot") (v "0.1.0") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "wat") (r "^1.0.29") (d #t) (k 0)))) (h "1flrlvnhmdsw0g63zla94c6y1sglvidw6hdibgfiz46132inry7k")))

