(define-module (crates-io pi vo pivotal-tracker-changelog) #:use-module (crates-io))

(define-public crate-pivotal-tracker-changelog-0.1.0 (c (n "pivotal-tracker-changelog") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1spgjn6birg4wf8r14wp0ajg0mmxbslzac07qczp0rajdp566nkk")))

