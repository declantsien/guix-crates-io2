(define-module (crates-io pi _c pi_cancel_timer) #:use-module (crates-io))

(define-public crate-pi_cancel_timer-0.1.0 (c (n "pi_cancel_timer") (v "0.1.0") (d (list (d (n "pcg_rand") (r "^0.13") (d #t) (k 2)) (d (n "pi_ext_heap") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_wheel") (r "^0.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1k0z0vv2rblxvpjs7jmsgj2in3nays6fylkkvvgzva4vlh6pw93c")))

(define-public crate-pi_cancel_timer-0.1.1 (c (n "pi_cancel_timer") (v "0.1.1") (d (list (d (n "pcg_rand") (r "^0.13") (d #t) (k 2)) (d (n "pi_ext_heap") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_wheel") (r "^0.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0yfs5qgvcrll4i1chcl31z25scmak21h075ll2h1kc7yw37423k5")))

(define-public crate-pi_cancel_timer-0.2.0 (c (n "pi_cancel_timer") (v "0.2.0") (d (list (d (n "pcg_rand") (r "^0.13") (d #t) (k 2)) (d (n "pi_ext_heap") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.2") (d #t) (k 0)) (d (n "pi_slot_wheel") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0pdq6wvb3a9zqvnbh638a0f96vid6rh9yjbgvxi5hvir4d8i3i9x")))

