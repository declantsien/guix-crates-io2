(define-module (crates-io pi _c pi_compress) #:use-module (crates-io))

(define-public crate-pi_compress-0.1.0 (c (n "pi_compress") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)))) (h "1ah5na1p4pnnxq8p640838cwzbdnirdrgxyhc0vjcxjxl1274ca2")))

