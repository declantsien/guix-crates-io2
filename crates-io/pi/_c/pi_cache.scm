(define-module (crates-io pi _c pi_cache) #:use-module (crates-io))

(define-public crate-pi_cache-0.1.1 (c (n "pi_cache") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0bfjmihbs5ynz0c8rvww6p01xhs99jk0yqd7sc9shvj4jcp4jnlm")))

(define-public crate-pi_cache-0.1.2 (c (n "pi_cache") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "15jn1qvijq1dz3g5d5zjks3nxwqiwqwh71nhagvgazqa3rhjp4n5")))

(define-public crate-pi_cache-0.1.3 (c (n "pi_cache") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0ga7fd4k1zaw7lsfqy1nhjz8f24pdiq8jddifhswk8qxy8967z15")))

(define-public crate-pi_cache-0.1.4 (c (n "pi_cache") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0w27iy8c97cv5a53fdn66b4r9ydmxrlqvcsmc5iyp0mn8gxh59yr")))

(define-public crate-pi_cache-0.1.5 (c (n "pi_cache") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1hky3pax0z1sjm0jjk7nkrixfc9vdhrxcaaiyyq257j4k2gwlzq9")))

(define-public crate-pi_cache-0.2.0 (c (n "pi_cache") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1hfsjp0s7560vgdd2k9kjzpk549cv77gi2zhjr5w9vyaiw6i6d68")))

(define-public crate-pi_cache-0.2.1 (c (n "pi_cache") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1fsb4m7k0phxn5nynw5kvv00gaqlh2hh5q9ry3kcdvd6w65hwiwa")))

(define-public crate-pi_cache-0.2.2 (c (n "pi_cache") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0y1l19mkqk7wmj2zllahiqi0imj7asaajv1gw29jzfaqyyma4wg6")))

(define-public crate-pi_cache-0.2.3 (c (n "pi_cache") (v "0.2.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0wf6p0cni7kk0fbn3va148cbs50rcafw27r4nqzm4k20wkj5dwy4")))

(define-public crate-pi_cache-0.2.4 (c (n "pi_cache") (v "0.2.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1hz4zmirvkr61vqjv8bxm8ry5smzza7ps8gh8dm7x2s8gw7qwas9")))

(define-public crate-pi_cache-0.2.5 (c (n "pi_cache") (v "0.2.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1n4lp88f3b8m4rnkamxf3vzmcjpivs60cdgbhvgx30407f0nfqxc")))

(define-public crate-pi_cache-0.3.0 (c (n "pi_cache") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pcg_rand") (r "^0.13") (d #t) (k 2)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0mfh4dwlqz15pyp7py9yqsxawxrvh07afjh3mbrh96adfgvhnfih")))

(define-public crate-pi_cache-0.3.1 (c (n "pi_cache") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pcg_rand") (r "^0.13") (d #t) (k 2)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.2") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0aw3ynxdzyxzrb3qm703dlkg36ip7jrnfs3xrzxfhwvbl229gy3b")))

(define-public crate-pi_cache-0.3.2 (c (n "pi_cache") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pcg_rand") (r "^0.13") (d #t) (k 2)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_print_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.2") (d #t) (k 0)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1kncqwj40yzi55g7mmdlzbpq6sh0dazs0ybzv0vs7p9gq9c8fw0y")))

(define-public crate-pi_cache-0.4.0 (c (n "pi_cache") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pcg_rand") (r "^0.13") (d #t) (k 2)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_print_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "0y251bgvwmwlj8fdkfpnd3p7y0kjw1nwgrmjjyqg1ax8rvgf9q9x")))

(define-public crate-pi_cache-0.4.3 (c (n "pi_cache") (v "0.4.3") (d (list (d (n "TinyUFO") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "pcg_rand") (r "^0.13") (d #t) (k 2)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_print_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1qphis1kzb4inzpm3q2hp6rx4jmcbldy9qbg4zhn5sd4cyg75fkl")))

