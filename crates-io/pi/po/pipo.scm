(define-module (crates-io pi po pipo) #:use-module (crates-io))

(define-public crate-pipo-0.1.0 (c (n "pipo") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0mwagsbpvckmlgpc76g6i23nq3kpb3dg0kkixzsv1ibiaafvycs7")))

(define-public crate-pipo-0.1.1 (c (n "pipo") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0c5z39q3m2bcmnr7j53ca8vjy5fzd24vmlwqrcgr8h0733xvr2va")))

