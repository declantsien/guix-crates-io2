(define-module (crates-io pi ca picam) #:use-module (crates-io))

(define-public crate-picam-0.1.0 (c (n "picam") (v "0.1.0") (h "0n61xmihaawni646qa1sj4nbv4icxnk4jkrgg74qb6ic8z1c4qh5") (y #t)))

(define-public crate-picam-0.1.1 (c (n "picam") (v "0.1.1") (h "06c0809b9qrq02h8p7brsagrwm93a9q8v5cnnjz9kiv7lgbdxha0") (y #t)))

