(define-module (crates-io pi ca picasso-core) #:use-module (crates-io))

(define-public crate-picasso-core-0.1.0 (c (n "picasso-core") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "purrmitive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.3") (d #t) (k 0)))) (h "1gk2dvfv0qjshp8vcifhd1c6ldbzn7plm5mvrg1k3d3kjhd61lx9") (f (quote (("default"))))))

