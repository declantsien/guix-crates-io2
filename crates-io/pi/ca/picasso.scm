(define-module (crates-io pi ca picasso) #:use-module (crates-io))

(define-public crate-picasso-0.1.0 (c (n "picasso") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "picasso-core") (r "^0.1") (d #t) (k 0)))) (h "16blkwk57cdd5cgr8s30q7xl6f0jbfnqnv9yv91izfpday6x7a8w") (f (quote (("default"))))))

