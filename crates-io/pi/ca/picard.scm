(define-module (crates-io pi ca picard) #:use-module (crates-io))

(define-public crate-picard-0.0.1 (c (n "picard") (v "0.0.1") (d (list (d (n "picard-core") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1wrf1gv48pqhrbgf05yfcwrpyp4k0g537zvkvawphdavwxg7z2ik")))

