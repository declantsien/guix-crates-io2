(define-module (crates-io pi ca picard-core) #:use-module (crates-io))

(define-public crate-picard-core-0.0.1 (c (n "picard-core") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1cs8y7hbzgqcvhmpdvz10b82gbz7yksmaabwcfxhh93g7naxn06q")))

