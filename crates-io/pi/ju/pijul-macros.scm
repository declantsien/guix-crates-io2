(define-module (crates-io pi ju pijul-macros) #:use-module (crates-io))

(define-public crate-pijul-macros-0.1.0 (c (n "pijul-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gmrjgr03ajami96vq2ylwrqz7nbw75vlpz5545gy9r59iky0w73")))

(define-public crate-pijul-macros-0.2.0 (c (n "pijul-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nvcm23w6s9059cm2jcks80anmjzffsb2sr22s9a16qfhm7k2s5v")))

(define-public crate-pijul-macros-0.3.0 (c (n "pijul-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03dyaahwp8d8ddajqjs6g3vq86hra3ckqjxcicb1wzrchkl581wx")))

(define-public crate-pijul-macros-0.4.0 (c (n "pijul-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "052pwsj072rd4l62c87x0yc86ldm385lmmz19nffarcs84brhk76")))

(define-public crate-pijul-macros-0.5.0 (c (n "pijul-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r3hivav1mzpzdpk2rj8flhl4vgp2r85gdi0kw5x8r8bv0y79cy2")))

