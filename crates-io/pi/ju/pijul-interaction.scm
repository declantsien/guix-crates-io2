(define-module (crates-io pi ju pijul-interaction) #:use-module (crates-io))

(define-public crate-pijul-interaction-0.0.1 (c (n "pijul-interaction") (v "0.0.1") (d (list (d (n "dialoguer") (r "^0.10.4") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "duplicate") (r "^1.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (f (quote ("improved_unicode"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "pijul-config") (r "^0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0vv1z0flsgvf09y18sv2jbk9sn0rllka15wdskpv9ziydp9xr1h7")))

