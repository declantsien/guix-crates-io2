(define-module (crates-io pi ju pijul-repository) #:use-module (crates-io))

(define-public crate-pijul-repository-0.0.1 (c (n "pijul-repository") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "libpijul") (r "^1.0.0-beta.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pijul-config") (r "^0.0.1") (d #t) (k 0)) (d (n "rlimit") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.7") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "06vl6id0amw1pxfhj6m34mvr171czikph8g21xg34k7c263maiq4")))

