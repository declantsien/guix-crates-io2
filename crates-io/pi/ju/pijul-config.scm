(define-module (crates-io pi ju pijul-config) #:use-module (crates-io))

(define-public crate-pijul-config-0.0.1 (c (n "pijul-config") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (f (quote ("editor"))) (k 0)) (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "whoami") (r "^1.4") (k 0)))) (h "19xdcqyw2lh4r8rs5dfjzqf27ajfld2gimw07bhvnjixz0gplylq")))

