(define-module (crates-io pi xo pixops) #:use-module (crates-io))

(define-public crate-pixops-0.0.0 (c (n "pixops") (v "0.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "pix") (r "^0.4") (d #t) (k 0)))) (h "01vrpxlqg4ic9v0w6f7jfrvyxdf6k6ycpqz7z6vqk65wqv3gymqk") (f (quote (("use-simd") ("default" "use-simd"))))))

(define-public crate-pixops-0.0.1 (c (n "pixops") (v "0.0.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "pix") (r "^0.6") (d #t) (k 0)))) (h "0hihx1fqyg6lixdw8j1295ah0rv66vsc5ks8vmzhyqgl86yjngr9") (f (quote (("use-simd") ("default" "use-simd"))))))

(define-public crate-pixops-0.0.2 (c (n "pixops") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pix") (r "^0.7.0") (d #t) (k 0)))) (h "0shdwhbb8sc0bmxrj19f5hpksw0gd80g0bv8h1i6lgiswvics3dn") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-pixops-0.1.0 (c (n "pixops") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pix") (r "^0.8.0") (d #t) (k 0)))) (h "0slbfaydplmwf1i1cv6741xbalwzmmy46wq6q9f6k4957dpyw9jy") (f (quote (("simd") ("default" "simd"))))))

