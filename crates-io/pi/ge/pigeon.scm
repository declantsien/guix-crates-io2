(define-module (crates-io pi ge pigeon) #:use-module (crates-io))

(define-public crate-pigeon-0.1.0 (c (n "pigeon") (v "0.1.0") (d (list (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "0kn74irv650k6aqqv5hggxqg7ag9j7gnkcp2cd20miz3hbbf33ah")))

(define-public crate-pigeon-0.1.1 (c (n "pigeon") (v "0.1.1") (d (list (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "16fjx135p1aday6z6md426zjmzf55c3kgxafklaygyv9vbm93kmv")))

(define-public crate-pigeon-0.2.0 (c (n "pigeon") (v "0.2.0") (d (list (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "096wiivvm61pmwv1jklj9sygqcn1lwzx4sd5b482pi0rycqs4pzf")))

(define-public crate-pigeon-0.3.0 (c (n "pigeon") (v "0.3.0") (d (list (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "1dwz84r7swhj7lx3qd49wxn7qpk06zdcjyaqdq05xyx5mmmk5z3w")))

(define-public crate-pigeon-0.3.1 (c (n "pigeon") (v "0.3.1") (d (list (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "16s4aiqvw8zpblbh5f16fzm8c4hriwajjjj894prykajkfha50s3")))

(define-public crate-pigeon-0.3.2 (c (n "pigeon") (v "0.3.2") (d (list (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "0ir7qiwspwbli28f6c132z3k7ayssr9716axqxy7gr6mm54y6pdc")))

