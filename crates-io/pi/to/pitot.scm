(define-module (crates-io pi to pitot) #:use-module (crates-io))

(define-public crate-pitot-0.0.1 (c (n "pitot") (v "0.0.1") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "inotify") (r "^0.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serial") (r "^0.3.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "ws") (r "^0.7.1") (d #t) (k 0)))) (h "1azm07sgwpqwhmigk15drvqrlyj2z8pdpa21iw3mpv9hd5inw4s3")))

