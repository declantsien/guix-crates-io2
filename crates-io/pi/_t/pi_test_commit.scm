(define-module (crates-io pi _t pi_test_commit) #:use-module (crates-io))

(define-public crate-pi_test_commit-0.1.0 (c (n "pi_test_commit") (v "0.1.0") (h "1ciwilqs85jzva6kq7l8prcihb313jgmj88szn2hq725fsxf2xlq")))

(define-public crate-pi_test_commit-0.2.0 (c (n "pi_test_commit") (v "0.2.0") (h "1z68wgnkfizjqi7mkglvi8xx4gwgx9vf29m6i7hmxdqbb80f5w49")))

(define-public crate-pi_test_commit-0.3.0 (c (n "pi_test_commit") (v "0.3.0") (h "17hn7n1kb1g605fwsy0v1z3j3khw66ax2xl9zqgp3xs511kcmy3v")))

(define-public crate-pi_test_commit-0.4.0 (c (n "pi_test_commit") (v "0.4.0") (h "17hsnbb2ilgaxa5i7y432jwx44jfmcnnnwfr24qbf83l6lrcq3fq")))

(define-public crate-pi_test_commit-0.5.0 (c (n "pi_test_commit") (v "0.5.0") (h "0b7xdf0j9hjgpw9k6aymk7jvi3m4zd3aih9hamvd5zd5hczb1f5j")))

(define-public crate-pi_test_commit-0.5.1 (c (n "pi_test_commit") (v "0.5.1") (h "0yr26mj6hmipclzgwz48q91ac9b258v6rkhzldbkxqksvx4320nl")))

(define-public crate-pi_test_commit-0.5.2 (c (n "pi_test_commit") (v "0.5.2") (h "1ia22j38jc2vgm3qpkj494w702pq5jcgs5xhgiwhabi9z8z35gk3")))

