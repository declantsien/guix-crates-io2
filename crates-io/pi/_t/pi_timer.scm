(define-module (crates-io pi _t pi_timer) #:use-module (crates-io))

(define-public crate-pi_timer-0.1.0 (c (n "pi_timer") (v "0.1.0") (d (list (d (n "pcg_rand") (r "^0.13") (d #t) (k 2)) (d (n "pi_ext_heap") (r "^0.1") (d #t) (k 0)) (d (n "pi_wheel") (r "^0.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)))) (h "0wygm835a9mb6f034l87ywwsg2pxaf9zq2g3an9qcpvskcns5hla")))

