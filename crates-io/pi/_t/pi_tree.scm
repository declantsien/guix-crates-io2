(define-module (crates-io pi _t pi_tree) #:use-module (crates-io))

(define-public crate-pi_tree-0.1.0 (c (n "pi_tree") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_slotmap") (r "^0.1") (d #t) (k 0)))) (h "0l1hdnyf9bi1xc7zmaa491x0i97af4d2p224rngmdk5z860g7m1r")))

