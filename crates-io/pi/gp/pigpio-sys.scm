(define-module (crates-io pi gp pigpio-sys) #:use-module (crates-io))

(define-public crate-pigpio-sys-0.1.0 (c (n "pigpio-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)))) (h "152a2anp0qa8h3rc6p17vmhv8kfa8l44zbpzr42126rr22l49d9n")))

(define-public crate-pigpio-sys-0.1.1 (c (n "pigpio-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "0f0crc7samqx6xf9lfmb6mqzk2lmf3vqfgb10m3kdbjl65jhrrd4")))

