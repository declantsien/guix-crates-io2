(define-module (crates-io pi nc pincli) #:use-module (crates-io))

(define-public crate-pincli-0.1.0 (c (n "pincli") (v "0.1.0") (h "0qpqn6mvki2pjls8ddhhzj8szqk7xv4dnscw7401377jqn3hi3az")))

(define-public crate-pincli-0.1.1 (c (n "pincli") (v "0.1.1") (h "0nw10dj04xc96hpmsc9hi71d7y9qvvvq3habf9mwjfxhsd5kjdwg")))

