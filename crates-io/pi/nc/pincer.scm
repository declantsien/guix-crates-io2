(define-module (crates-io pi nc pincer) #:use-module (crates-io))

(define-public crate-pincer-0.1.0 (c (n "pincer") (v "0.1.0") (h "02a0pn5h3dgy4g6yhjdrd41bkdqlx0hwbi8xg30d401nmx3v3vmf")))

(define-public crate-pincer-1.0.0 (c (n "pincer") (v "1.0.0") (h "0ijag0kbzf27v73ackn3vx21cxj1l342ysns40yhlsnc8jqx14xd")))

(define-public crate-pincer-2.0.0 (c (n "pincer") (v "2.0.0") (h "1msc6bwz9qd1m0kmxam47cv4bp6lhjscjz7zl6fg27yyf2hl388d")))

