(define-module (crates-io pi nc pinch) #:use-module (crates-io))

(define-public crate-pinch-0.1.0 (c (n "pinch") (v "0.1.0") (d (list (d (n "handlebars") (r "^4.1.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1lkz859k2krhyr45gdzaim52y90zi8mvdph7i5nmwznpia98wqs6")))

