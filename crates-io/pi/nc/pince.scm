(define-module (crates-io pi nc pince) #:use-module (crates-io))

(define-public crate-pince-0.1.0 (c (n "pince") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ws3387bzx724zfcihasfmi3vwdifz5ifl33gh28ica9zj1zwggn")))

