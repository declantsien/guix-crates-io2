(define-module (crates-io pi _p pi_phf_map) #:use-module (crates-io))

(define-public crate-pi_phf_map-0.1.0 (c (n "pi_phf_map") (v "0.1.0") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_wy_rng") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "187ri1sxp70r1503lcvwybc4fwvzdiqq8blr8na60lzhs969vr2q")))

(define-public crate-pi_phf_map-0.2.0 (c (n "pi_phf_map") (v "0.2.0") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_wy_rng") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "182shw9g9cqrf5x437xldnws3b01yrf4kfaks2k3v27nnqm9gqmd")))

(define-public crate-pi_phf_map-0.2.1 (c (n "pi_phf_map") (v "0.2.1") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_wy_rng") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0phzj64b6syv65wc0icy04hxyhbimrhn1vky3w62b96xvp26wpx6")))

(define-public crate-pi_phf_map-0.2.2 (c (n "pi_phf_map") (v "0.2.2") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_wy_rng") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "00h830pdv6r23aixzdngg1za3vzv74b2d99wpavn8y84ssvhll3z")))

(define-public crate-pi_phf_map-0.2.3 (c (n "pi_phf_map") (v "0.2.3") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_wy_rng") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "03zb77b2nly3r0bypfj8i83f5whp928shjxl11v87b5rhlnvgbn1")))

