(define-module (crates-io pi _p pi_proc_macros) #:use-module (crates-io))

(define-public crate-pi_proc_macros-0.1.0 (c (n "pi_proc_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1jiy1mrpjggzdgmbyp2c0vzx14206jjmr358ry6kbs7bbicp9g9g")))

