(define-module (crates-io pi _p pi_pointer) #:use-module (crates-io))

(define-public crate-pi_pointer-0.1.0 (c (n "pi_pointer") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pi_slab") (r "^0.1") (d #t) (k 0)))) (h "1k3bk89wn07g6cjka2s0ingq6ggqyfrq65ks858nncfyc3ycdzja")))

