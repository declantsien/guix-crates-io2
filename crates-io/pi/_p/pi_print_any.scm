(define-module (crates-io pi _p pi_print_any) #:use-module (crates-io))

(define-public crate-pi_print_any-0.1.0 (c (n "pi_print_any") (v "0.1.0") (h "0m2gpli4bzc4b0kara6sssbvy6691l618i4d5pgn02ms9pyj18j8")))

(define-public crate-pi_print_any-0.1.1 (c (n "pi_print_any") (v "0.1.1") (h "08m0n3xa18m23gih5nzbqp7xk42ys0zq7jq8icc732cv3sick990")))

(define-public crate-pi_print_any-0.1.2 (c (n "pi_print_any") (v "0.1.2") (h "0jlayycfj5yqsl4hq681089j34fi3h8xiz2ygd8ll26akasdvvlf")))

