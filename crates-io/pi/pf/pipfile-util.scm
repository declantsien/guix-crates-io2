(define-module (crates-io pi pf pipfile-util) #:use-module (crates-io))

(define-public crate-pipfile-util-0.1.1 (c (n "pipfile-util") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0355v93xna1nllrpgfxsf0xw20hax3vcgi6hy9rc4922iw3r4g3j")))

