(define-module (crates-io pi pf pipfile-diff) #:use-module (crates-io))

(define-public crate-pipfile-diff-0.1.0 (c (n "pipfile-diff") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dgxhjbs51w2a97jvw0328f8vnivh9y4hm1nw3gd56wgvacc5p5s") (y #t)))

