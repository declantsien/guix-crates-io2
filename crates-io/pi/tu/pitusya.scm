(define-module (crates-io pi tu pitusya) #:use-module (crates-io))

(define-public crate-pitusya-0.0.0 (c (n "pitusya") (v "0.0.0") (h "0ihn9ihcjz4hi82jc8zh0d1r9a6kd8bhlmq7l4l9a37kcd5kyyl0")))

(define-public crate-pitusya-0.0.1 (c (n "pitusya") (v "0.0.1") (h "1crvvjz7izqv300471rb13k6f1hckg1rlc6nyx0ja4dj41q3d430")))

(define-public crate-pitusya-1.0.0 (c (n "pitusya") (v "1.0.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)))) (h "1ghks8ndvf74xvx3v94y77jjacn3gkhjylyc9qm1nhqaw90ip3fv")))

(define-public crate-pitusya-1.0.1 (c (n "pitusya") (v "1.0.1") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)))) (h "1m4gsayyb5r4mpbrzln43lkshcm3ary2dzndqp42hbvshvqlvg1x")))

(define-public crate-pitusya-1.0.2 (c (n "pitusya") (v "1.0.2") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "llvm-sys") (r "^160.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)))) (h "0ffk74fqh0lqs79nqqk595kar58s13nvnx90zfnnscz2sqlb34p7")))

(define-public crate-pitusya-1.0.4 (c (n "pitusya") (v "1.0.4") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "llvm-sys") (r "^160.1.3") (d #t) (k 0)) (d (n "pitusyastd") (r "^0.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)))) (h "0mc53xwggivibwncjva660zj3gldgbls7rfx6f612gys2zjk39sf")))

