(define-module (crates-io pi tu pitufo) #:use-module (crates-io))

(define-public crate-pitufo-1.0.0 (c (n "pitufo") (v "1.0.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0izk8dzmfv76jyya99zx79nm56rivq80hpa2hc0cn17x7qrznfib")))

(define-public crate-pitufo-1.1.0 (c (n "pitufo") (v "1.1.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("arbitrary_precision"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "09lp27bm2rgcmqwl0nkrsi5rdcr29fza527bjkj9070x0mnnrgx9")))

