(define-module (crates-io pi x- pix-win-loop) #:use-module (crates-io))

(define-public crate-pix-win-loop-0.1.0 (c (n "pix-win-loop") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pixels") (r "^0.13") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "web-time") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.29") (f (quote ("rwh_05" "x11" "wayland" "wayland-dlopen" "wayland-csd-adwaita"))) (k 0)))) (h "1j4lwd82lg99nxz5zibv23flnc3sl2p7r00xrhms3hiq4yjx7822") (f (quote (("winit-event-loop-spawn") ("default"))))))

(define-public crate-pix-win-loop-0.2.0 (c (n "pix-win-loop") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pixels") (r "^0.13") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "web-time") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.29") (f (quote ("rwh_05" "x11" "wayland" "wayland-dlopen" "wayland-csd-adwaita"))) (k 0)))) (h "0m2qdzw24z3ixmc281fs2fdrcbq9sqcf1s4mzdfx5qarcg1h36pd")))

(define-public crate-pix-win-loop-0.3.0 (c (n "pix-win-loop") (v "0.3.0") (d (list (d (n "pixels") (r "^0.13") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "win-loop") (r "^0.1") (f (quote ("rwh_05"))) (d #t) (k 0)))) (h "0n8fpr5in5jv38gm17n678cmi54dcclhaycgm6jkk5rfjgygf6sb")))

(define-public crate-pix-win-loop-0.4.0 (c (n "pix-win-loop") (v "0.4.0") (d (list (d (n "pixels") (r "^0.13") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "win-loop") (r "^0.3") (f (quote ("rwh_05"))) (d #t) (k 0)))) (h "0fmxm2ckfbavdp3avk43pnfljk43avsxwmi99992c9k9rg9jhy0j")))

