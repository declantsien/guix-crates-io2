(define-module (crates-io pi x- pix-brcode) #:use-module (crates-io))

(define-public crate-pix-brcode-0.1.0 (c (n "pix-brcode") (v "0.1.0") (d (list (d (n "emv-qrcps") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f3qywndxpywhm1sasxmhmr1zdggpqqw51838q90ii9ahib5jcb8")))

