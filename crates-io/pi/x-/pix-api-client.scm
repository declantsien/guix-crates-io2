(define-module (crates-io pi x- pix-api-client) #:use-module (crates-io))

(define-public crate-pix-api-client-0.2.1 (c (n "pix-api-client") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "arc-swap") (r "^1.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pix-brcode") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "190hv3dg1pl7v2cgs3562rckg1h9d31ih2z56cyb1jx61pq3ap1m") (f (quote (("default"))))))

