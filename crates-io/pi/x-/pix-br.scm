(define-module (crates-io pi x- pix-br) #:use-module (crates-io))

(define-public crate-pix-br-0.1.0 (c (n "pix-br") (v "0.1.0") (d (list (d (n "image-base64-wasm") (r "^0.6.0") (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.0.4") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1sb4pmxx6mbj6w8qfzhp6bac9cfkqiv6f6qa6hd0qx2vnqsp0dx4")))

