(define-module (crates-io pi co picorv32) #:use-module (crates-io))

(define-public crate-picorv32-0.1.0 (c (n "picorv32") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)))) (h "05cz7v608bi264w8h2faxfsibx9nsawgj69yprg1y9vl0y9jvl25") (f (quote (("inline-asm"))))))

(define-public crate-picorv32-0.1.1 (c (n "picorv32") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 1)))) (h "0chha5sb2f58glqix9fmbvh6qfs8pd3x83sk02l34dd8zi0n80c4") (f (quote (("inline-asm"))))))

(define-public crate-picorv32-0.1.2 (c (n "picorv32") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 1)))) (h "03ysffvffx100a7zxgiqil4gf5zgfknfjds6vmcsw0xsyi3rzq9q") (f (quote (("inline-asm") ("const-fn" "bare-metal/const-fn"))))))

(define-public crate-picorv32-0.1.3 (c (n "picorv32") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 1)))) (h "005favpvlvjjb3ds55dnjsx5jg2i8r6zza5wf1y2f4ksq0rggrrw") (f (quote (("interrupts-qregs") ("inline-asm") ("const-fn" "bare-metal/const-fn"))))))

