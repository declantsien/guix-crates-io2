(define-module (crates-io pi co pico-sys) #:use-module (crates-io))

(define-public crate-pico-sys-0.0.1 (c (n "pico-sys") (v "0.0.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1q5pg0ms6szz6b5h26h4k40zb76zbwwjgyigac4wly9qngdj4yl5")))

