(define-module (crates-io pi co pico-args) #:use-module (crates-io))

(define-public crate-pico-args-0.1.0 (c (n "pico-args") (v "0.1.0") (h "17zb3ydiq7bw8jjkkcf4654rcjw5v6va654l1pima3wi9lvzrpgl")))

(define-public crate-pico-args-0.2.0 (c (n "pico-args") (v "0.2.0") (h "0b7jyz62yfim18yvvqvp7dk21zjak547vzannih47kq588ajbkig")))

(define-public crate-pico-args-0.3.0 (c (n "pico-args") (v "0.3.0") (h "0rw6f1g8ig28snd0jdnh9s9izwghmdz4fc318fxpwa3i6w7jpqr2")))

(define-public crate-pico-args-0.3.1 (c (n "pico-args") (v "0.3.1") (h "1k78bmc3k8jcy7dw7gdkhn0v4wx1k6b6w1ihwgd44pd06jwg3l9s")))

(define-public crate-pico-args-0.3.2 (c (n "pico-args") (v "0.3.2") (h "13zs8z2ayk9m02ns8ckgj41v1xq2p5dlz5mxwd4h6gy4x9n86wba") (f (quote (("eq-separator") ("default" "eq-separator"))))))

(define-public crate-pico-args-0.3.3 (c (n "pico-args") (v "0.3.3") (h "0ii0xx5y9jk85azfzq5221b5vlrc8nlgb83w7lschrj93y5yw7hv") (f (quote (("eq-separator") ("default" "eq-separator"))))))

(define-public crate-pico-args-0.3.4 (c (n "pico-args") (v "0.3.4") (h "1c9qyv3aykm8bzapiqq2gznp7cyk04py92xzghfqcp25fggv9f98") (f (quote (("short-space-opt") ("eq-separator") ("default" "eq-separator"))))))

(define-public crate-pico-args-0.4.0 (c (n "pico-args") (v "0.4.0") (h "1mlm8pwwdwj2c8h3qwh7ai7zvbkjqxgjhaj73j3spqa51717406p") (f (quote (("short-space-opt") ("eq-separator") ("default" "eq-separator"))))))

(define-public crate-pico-args-0.4.1 (c (n "pico-args") (v "0.4.1") (h "0zd6nmfgzxzb98wdcpxckxr0ckdh0phidz67zjzy042sijwzwykx") (f (quote (("short-space-opt") ("eq-separator") ("default" "eq-separator") ("combined-flags"))))))

(define-public crate-pico-args-0.4.2 (c (n "pico-args") (v "0.4.2") (h "0s646i0pbcck300rqldb21m151zxp66m3mdskha063blrfbcv2yv") (f (quote (("short-space-opt") ("eq-separator") ("default" "eq-separator") ("combined-flags"))))))

(define-public crate-pico-args-0.5.0 (c (n "pico-args") (v "0.5.0") (h "05d30pvxd6zlnkg2i3ilr5a70v3f3z2in18m67z25vinmykngqav") (f (quote (("short-space-opt") ("eq-separator") ("default") ("combined-flags"))))))

