(define-module (crates-io pi co picoquic) #:use-module (crates-io))

(define-public crate-picoquic-0.1.0 (c (n "picoquic") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "picoquic-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1fqn6g51nnsbfnnlyjn99b749xxz0llml4nc2in4mcazddqdcyfg")))

(define-public crate-picoquic-0.1.1 (c (n "picoquic") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "picoquic-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1ggjczl1a68zw5s71hm37xxkyj1vzsg1hpz3lkpwqmqnhrkz52f8")))

