(define-module (crates-io pi co pico-bytes) #:use-module (crates-io))

(define-public crate-pico-bytes-0.1.0 (c (n "pico-bytes") (v "0.1.0") (h "10ih1gkmxwab6hkbf0mvdbpi4ra9vp3f619w6nzkch81k301qq8n")))

(define-public crate-pico-bytes-0.1.1 (c (n "pico-bytes") (v "0.1.1") (h "0h12xwnx7nnylrmrxdh5i9k8h9zggi0p1y88g34fm2b3vg1ra56z")))

