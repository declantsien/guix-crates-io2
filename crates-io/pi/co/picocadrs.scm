(define-module (crates-io pi co picocadrs) #:use-module (crates-io))

(define-public crate-picocadrs-0.1.0 (c (n "picocadrs") (v "0.1.0") (d (list (d (n "rlua") (r "^0.19.4") (d #t) (k 0)))) (h "08613fvwfgnirn825kqpdl6bc6kipi4z3d9pw24lgm7nqym6m1gb")))

(define-public crate-picocadrs-0.1.1 (c (n "picocadrs") (v "0.1.1") (d (list (d (n "rlua") (r "^0.19.4") (d #t) (k 0)))) (h "1cgh97y5l9j22g9xcf44ymcjpj2ygg6673hw603nl402z3llbaf3")))

(define-public crate-picocadrs-0.2.0 (c (n "picocadrs") (v "0.2.0") (d (list (d (n "rlua") (r "^0.19.4") (d #t) (k 0)))) (h "0p2kxpqrc75hj1wy3s4x0zsik83v1b2nj28b7i4ayk2qv9hm5a8m")))

(define-public crate-picocadrs-1.0.0 (c (n "picocadrs") (v "1.0.0") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "rlua") (r "^0.19.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "13x50fbnm51ahxrpis56iwrbml8z5mgdm0pgia040f1zldr1jqxa")))

(define-public crate-picocadrs-1.0.1 (c (n "picocadrs") (v "1.0.1") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "rlua") (r "^0.19.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "14bwyd7a15kap4r4642hnpb7k6k7i79x7dl51hhlizrm5v2q8vbh")))

(define-public crate-picocadrs-1.0.2 (c (n "picocadrs") (v "1.0.2") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "rlua") (r "^0.19.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0c81y8ssfmr05hsgihn1p3lsbg1jxshvai3rhaspmxfjxyncil6x")))

(define-public crate-picocadrs-1.0.3 (c (n "picocadrs") (v "1.0.3") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "rlua") (r "^0.19.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "02v4z039bh1jyb49zz0m292zvj28g5vnzpwc57q0dq887jpczsrv")))

