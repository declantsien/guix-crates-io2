(define-module (crates-io pi co picolog) #:use-module (crates-io))

(define-public crate-picolog-1.0.0 (c (n "picolog") (v "1.0.0") (d (list (d (n "log") (r "^0.4.20") (f (quote ("std"))) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 2)))) (h "17sf2byc87i2pwx7k96vzgycdcvr8wp891ha7ic621jgh0z1lkn1")))

(define-public crate-picolog-1.0.1 (c (n "picolog") (v "1.0.1") (d (list (d (n "log") (r "^0.4.20") (f (quote ("std"))) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 2)))) (h "1w2vvxpw723i5cd72g7rf17fxq5s0ybl4ssg9fj3fw66lk4x4ggw")))

