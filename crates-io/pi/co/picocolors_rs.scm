(define-module (crates-io pi co picocolors_rs) #:use-module (crates-io))

(define-public crate-picocolors_rs-0.0.1 (c (n "picocolors_rs") (v "0.0.1") (h "03j0sjdr7h045pbsf6hch7wfnis4qsd4q1rxydpm033g3wfck1hx")))

(define-public crate-picocolors_rs-0.1.0 (c (n "picocolors_rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "13lynywq2zfjqkydpmqjfxzbzhvjxyba82b9hwwzkziv5zwsdzm5")))

