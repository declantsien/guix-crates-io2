(define-module (crates-io pi co pico-serial) #:use-module (crates-io))

(define-public crate-pico-serial-0.1.0 (c (n "pico-serial") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 1)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 1)) (d (n "tokio-serial") (r "^5.4.3") (d #t) (k 0)))) (h "1p1375g1snyv52vizybzp8dqy366rvl6jcdvd8lwkzjypawvhrqq")))

