(define-module (crates-io pi co pico-common) #:use-module (crates-io))

(define-public crate-pico-common-0.1.1 (c (n "pico-common") (v "0.1.1") (d (list (d (n "enum-iterator") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0yq1f6h0xj9cfniy53ql670a4rk29wrqal1afg3a7aaw223xhjgb")))

(define-public crate-pico-common-0.1.2 (c (n "pico-common") (v "0.1.2") (d (list (d (n "enum-iterator") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1iq9x8rxjkqqm3yby3ky1pvzkw5gm9y17qb56qdnk4bjs5swsar0")))

(define-public crate-pico-common-0.1.4 (c (n "pico-common") (v "0.1.4") (d (list (d (n "enum-iterator") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1kq75p398mhj7j3b7bssn3qqcww2zh58fd2xzsa843n6z7m43igx")))

(define-public crate-pico-common-0.2.0 (c (n "pico-common") (v "0.2.0") (d (list (d (n "enum-iterator") (r "^0.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1djwf6f95zijmn0c3ma2cfshhi58mkm12896x3k3khqlvi84k1hs")))

(define-public crate-pico-common-0.2.1 (c (n "pico-common") (v "0.2.1") (d (list (d (n "enum-iterator") (r "^0.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "09sk4k2pdc8v85bkcwdi7qcf5awyv5myj3jf418vl8xc39kg2w6x")))

(define-public crate-pico-common-0.3.0 (c (n "pico-common") (v "0.3.0") (d (list (d (n "enum-iterator") (r "^0.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "18jx9hsl3jqxxka29nx7g2v2x4aqp3hzmzf2dgbyyr741h9g1dix")))

(define-public crate-pico-common-0.3.1 (c (n "pico-common") (v "0.3.1") (d (list (d (n "enum-iterator") (r "^0.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1wj1wfyrdp137p9p3qh20dk6a7qnzphylmi27018gpqfrm09nfmq")))

