(define-module (crates-io pi co picoquic-sys) #:use-module (crates-io))

(define-public crate-picoquic-sys-0.1.0 (c (n "picoquic-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "0ym4sv8wagn95n2z4by67ig0pvql02wrk89m6xbp7idsjfxxi5i3")))

