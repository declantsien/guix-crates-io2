(define-module (crates-io pi co picoborgrev) #:use-module (crates-io))

(define-public crate-picoborgrev-0.1.0 (c (n "picoborgrev") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "05kid9w40ny32f9dxx4mfmiw6mkdhi47v9jxgrxr2v2z3njjx35i")))

(define-public crate-picoborgrev-0.1.1 (c (n "picoborgrev") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1b0vbn5zxxw2k8vnd5bnbwipkdisbfsqn5h9hflzfzwhd0i6y3la")))

