(define-module (crates-io pi co picohttpparser-sys) #:use-module (crates-io))

(define-public crate-picohttpparser-sys-1.0.0 (c (n "picohttpparser-sys") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05wgkp5ggnq9bwb9pgv4q3j3n0p3qw53zp0naph6jqbxfq0819g7") (f (quote (("sse4") ("default" "sse4"))))))

