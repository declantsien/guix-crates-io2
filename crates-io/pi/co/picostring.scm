(define-module (crates-io pi co picostring) #:use-module (crates-io))

(define-public crate-picostring-0.1.0 (c (n "picostring") (v "0.1.0") (h "1a608kshq8cisp8nkfwwsmzznnswsj7572y5ix9il1vd2z4h1kk8")))

(define-public crate-picostring-0.2.0 (c (n "picostring") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "134w22i702m120xdlymhwvqjpmczzlnqfg9r1xag01khnd5wrk67") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-picostring-0.3.1 (c (n "picostring") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "06siz5bv2khzgd6bq957a0kyqz56bkkcnykq9lgx9143ry2ck3bw") (s 2) (e (quote (("serde" "dep:serde"))))))

