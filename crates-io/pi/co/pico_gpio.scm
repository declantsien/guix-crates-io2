(define-module (crates-io pi co pico_gpio) #:use-module (crates-io))

(define-public crate-pico_gpio-0.1.0 (c (n "pico_gpio") (v "0.1.0") (d (list (d (n "readformat") (r "^0.1.2") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "0nqqj4dc9r0jwxm50npxlknh6jyad4vigl73wf99va90bbs68icq")))

(define-public crate-pico_gpio-0.1.1 (c (n "pico_gpio") (v "0.1.1") (d (list (d (n "readformat") (r "^0.1.2") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "14qpfljr2lx3g73ily19sib3n54hgchi3ncilraaxdxag59xchca")))

(define-public crate-pico_gpio-0.1.2 (c (n "pico_gpio") (v "0.1.2") (d (list (d (n "readformat") (r "^0.1.2") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "07b6c1vxv0168cpdzmn98j40n9ybcr0c4fviijymvysxisnzgyz3")))

(define-public crate-pico_gpio-0.1.3 (c (n "pico_gpio") (v "0.1.3") (d (list (d (n "readformat") (r "^0.1.2") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "0l4ac1d7iz50rm2hm5q6vbd4y1r98j1w73mj82ayb2kfs6mna9yk")))

