(define-module (crates-io pi co picoslon) #:use-module (crates-io))

(define-public crate-picoslon-0.1.0 (c (n "picoslon") (v "0.1.0") (d (list (d (n "async-scoped") (r "^0.7.1") (f (quote ("use-tokio"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)) (d (n "tarantool") (r "^4.0.0") (f (quote ("picodata"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.10") (d #t) (k 0)))) (h "0xrid60wm0s3a11mj3z4dr04s9ljwxyyssqq4gmbnflgpgf4bhqs")))

