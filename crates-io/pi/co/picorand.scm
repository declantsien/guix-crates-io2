(define-module (crates-io pi co picorand) #:use-module (crates-io))

(define-public crate-picorand-0.0.0 (c (n "picorand") (v "0.0.0") (h "0wqpckg7b2y691zkf9lr3q1h0vwp1qwpdnnaflbg1r1c84k1p2qz")))

(define-public crate-picorand-0.1.0 (c (n "picorand") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 2)))) (h "0jhraf7mds61g6fw7phbi4cykr9381n89widf5r7z8adbq696xbr")))

(define-public crate-picorand-0.1.1 (c (n "picorand") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 2)))) (h "12064586nqybancjg3mrzlrla7bvs8f3gnh11vicf215km8k1z5n")))

(define-public crate-picorand-0.1.2 (c (n "picorand") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 2)))) (h "19h24kz8kdvgi5k2r7bx60brk5c1px7vabigj98xlassz50aqspb")))

