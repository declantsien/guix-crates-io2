(define-module (crates-io pi co picodata-rmp-serde) #:use-module (crates-io))

(define-public crate-picodata-rmp-serde-1.0.0 (c (n "picodata-rmp-serde") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rmp") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.133") (d #t) (k 2)))) (h "02s1s7g3h85i5fp0j3dkfji7kkna399bhbg4cg0sjh915jc9n7xl")))

