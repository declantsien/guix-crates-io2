(define-module (crates-io pi co pico-device) #:use-module (crates-io))

(define-public crate-pico-device-0.1.1 (c (n "pico-device") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log-derive") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pico-common") (r "^0.1.1") (d #t) (k 0)) (d (n "pico-driver") (r "^0.1.1") (d #t) (k 0)))) (h "1wkmy1qkp1r73c3dap37qm0c3wi3z3rw892shfszyd02ssmzlf8m")))

(define-public crate-pico-device-0.1.2 (c (n "pico-device") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log-derive") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pico-common") (r "^0.1.2") (d #t) (k 0)) (d (n "pico-driver") (r "^0.1.2") (d #t) (k 0)))) (h "1d822g6f5msbk210i8ycx1v36rcm6jqi6138mdz9p0z0234qsj2i")))

(define-public crate-pico-device-0.1.4 (c (n "pico-device") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log-derive") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pico-common") (r "^0.1.4") (d #t) (k 0)) (d (n "pico-driver") (r "^0.1.4") (d #t) (k 0)))) (h "0v5i04qklks1hlzx9fd784n3z81glc9xssca6zn6j15zxg8drq0b")))

(define-public crate-pico-device-0.2.0 (c (n "pico-device") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pico-common") (r "^0.2.0") (d #t) (k 0)) (d (n "pico-driver") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (d #t) (k 0)))) (h "0rgaca9x6b48kny1papxym6sfs60s0k0gxcd8nwp566fj4mcqdnj")))

(define-public crate-pico-device-0.2.1 (c (n "pico-device") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pico-common") (r "^0.2.1") (d #t) (k 0)) (d (n "pico-driver") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (d #t) (k 0)))) (h "1mpr9accz48dy03f6cqchrvmh2gjzj9fdjm25da0wszjv8cvf674")))

(define-public crate-pico-device-0.3.0 (c (n "pico-device") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pico-common") (r "^0.3.0") (d #t) (k 0)) (d (n "pico-driver") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (d #t) (k 0)))) (h "0fvw9qkm150rryivi7qy6yphy19p8vbvvq9sgcckry3hhd7pijca")))

(define-public crate-pico-device-0.3.1 (c (n "pico-device") (v "0.3.1") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pico-common") (r "^0.3.1") (d #t) (k 0)) (d (n "pico-driver") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (d #t) (k 0)))) (h "19ln7airc0h2jgaxzvncsws34272ia56chqhixi5idqijmmbyscq")))

