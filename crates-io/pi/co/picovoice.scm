(define-module (crates-io pi co picovoice) #:use-module (crates-io))

(define-public crate-picovoice-0.0.1 (c (n "picovoice") (v "0.0.1") (d (list (d (n "pv_porcupine") (r "^1.9") (d #t) (k 0)) (d (n "pv_rhino") (r "^1.6") (d #t) (k 0)))) (h "0bmhdwp8lrrr9y6n7kfxdx0m0vf5nkvi6ylj1qj0v48ji0cnv4i0")))

(define-public crate-picovoice-1.1.0 (c (n "picovoice") (v "1.1.0") (d (list (d (n "pv_porcupine") (r "^1.9") (d #t) (k 0)) (d (n "pv_rhino") (r "^1.6") (d #t) (k 0)))) (h "0bpq86mbh4dlrc6xwpjy6x7m5kw3swwizlzjncrr21d29inz00bm")))

(define-public crate-picovoice-2.0.0 (c (n "picovoice") (v "2.0.0") (d (list (d (n "pv_porcupine") (r "^2.0.0") (d #t) (k 0)) (d (n "pv_rhino") (r "^2.0.0") (d #t) (k 0)))) (h "1vpfpdwr20vagbx9wza58hnmqf4ldzr8bgl938qii2vai3gv3si9")))

(define-public crate-picovoice-2.0.1 (c (n "picovoice") (v "2.0.1") (d (list (d (n "pv_porcupine") (r "^2.0.1") (d #t) (k 0)) (d (n "pv_rhino") (r "^2.0.1") (d #t) (k 0)))) (h "0l51nrchzvw5y6hwxmhcffrxmgvq2g4637bkcq6jd62cdimsdfsg")))

(define-public crate-picovoice-2.1.0 (c (n "picovoice") (v "2.1.0") (d (list (d (n "pv_porcupine") (r "^2.1.0") (d #t) (k 0)) (d (n "pv_rhino") (r "^2.1.0") (d #t) (k 0)))) (h "1wp88d4wb3677q5a4j0l28c32v0l54g78ixv79zh6nmr7q8v5z1l")))

(define-public crate-picovoice-2.1.1 (c (n "picovoice") (v "2.1.1") (d (list (d (n "pv_porcupine") (r "^2.1.0") (d #t) (k 0)) (d (n "pv_rhino") (r "^2.1.0") (d #t) (k 0)))) (h "0n2z3r1xqf4bbc5xj9pyi9fh5xc6q91c802wgygmlbr2jilb1j8s")))

(define-public crate-picovoice-2.1.2 (c (n "picovoice") (v "2.1.2") (d (list (d (n "pv_porcupine") (r "^2.1.2") (d #t) (k 0)) (d (n "pv_rhino") (r "^2.1.2") (d #t) (k 0)))) (h "1m66hb40hqn0gd8m504cx08qjnv3sclpn973zi0zzz1xf82bglfv")))

(define-public crate-picovoice-2.1.3 (c (n "picovoice") (v "2.1.3") (d (list (d (n "pv_porcupine") (r "^2.1.4") (d #t) (k 0)) (d (n "pv_rhino") (r "^2.1.3") (d #t) (k 0)))) (h "1xgqfdkf0wl0l5vybg98rqqsqh5i49y1fc49m0wwdbriiiym5v67")))

(define-public crate-picovoice-2.1.4 (c (n "picovoice") (v "2.1.4") (d (list (d (n "pv_porcupine") (r "^2.1.5") (d #t) (k 0)) (d (n "pv_rhino") (r "^2.1.4") (d #t) (k 0)))) (h "1hnpp852nfb79rcw1jvjbnpxsj2wy4wpi0s82jskw2kzahbd3rim")))

(define-public crate-picovoice-2.1.5 (c (n "picovoice") (v "2.1.5") (d (list (d (n "pv_porcupine") (r "^2.1.6") (d #t) (k 0)) (d (n "pv_rhino") (r "^2.1.5") (d #t) (k 0)))) (h "1xhdnxzk6mm62dlf7pws4axr4jam1449yrrsfhd66z8hvxwfvlhc")))

(define-public crate-picovoice-2.1.6 (c (n "picovoice") (v "2.1.6") (d (list (d (n "pv_porcupine") (r "=2.1.6") (d #t) (k 0)) (d (n "pv_rhino") (r "=2.1.5") (d #t) (k 0)))) (h "1sfa0rxg0lrw2swimbsfq3b627475ic9shr4pm4v8kqx6kga8hh0")))

(define-public crate-picovoice-2.1.7 (c (n "picovoice") (v "2.1.7") (d (list (d (n "pv_porcupine") (r "=2.1.6") (d #t) (k 0)) (d (n "pv_rhino") (r "=2.1.6") (d #t) (k 0)))) (h "1wxcgpavj0s2mr1fh6wd4mhbrzv7mvr3ba3c7x7jgkdx6qdzach7")))

(define-public crate-picovoice-2.1.8 (c (n "picovoice") (v "2.1.8") (d (list (d (n "pv_porcupine") (r "=2.1.6") (d #t) (k 0)) (d (n "pv_rhino") (r "=2.1.7") (d #t) (k 0)))) (h "0swz6pk0gabcwx1lk6pl4frg6lv6719r4w407xjml9ahlb32bm0p")))

(define-public crate-picovoice-2.1.9 (c (n "picovoice") (v "2.1.9") (d (list (d (n "pv_porcupine") (r "=2.1.8") (d #t) (k 0)) (d (n "pv_rhino") (r "=2.1.8") (d #t) (k 0)))) (h "1badgdlr88pqq28vy5pyw2algqjc795zzm0a7xikj0l3wwbsd23g")))

(define-public crate-picovoice-2.2.0 (c (n "picovoice") (v "2.2.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "pv_porcupine") (r "=2.2.0") (d #t) (k 0)) (d (n "pv_rhino") (r "=2.2.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "01kvkmz2bkk3m82bszplxmihmc10i1d23sa2db4vwwyvjgb7awrm")))

(define-public crate-picovoice-2.2.1 (c (n "picovoice") (v "2.2.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "pv_porcupine") (r "=2.2.1") (d #t) (k 0)) (d (n "pv_rhino") (r "=2.2.1") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0983x8zjj1p9qk27z0pb9b23fp6ph7y9nxi1pgcks1izcmkpr9xz")))

(define-public crate-picovoice-3.0.0 (c (n "picovoice") (v "3.0.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "pv_porcupine") (r "=3.0.0") (d #t) (k 0)) (d (n "pv_rhino") (r "=3.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "101i92x88bs46h8j0cixfr14k0d1n7357li1b1vhl2xg70nlxgps")))

(define-public crate-picovoice-3.0.1 (c (n "picovoice") (v "3.0.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "pv_porcupine") (r "=3.0.0") (d #t) (k 0)) (d (n "pv_rhino") (r "=3.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "1zmf9j2x5s5y55ymx5if5knqd2f3b5rqiyq46m4jay3r7idsyciy")))

(define-public crate-picovoice-3.0.2 (c (n "picovoice") (v "3.0.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "pv_porcupine") (r "=3.0.1") (d #t) (k 0)) (d (n "pv_rhino") (r "=3.0.1") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "13l297fm9qr44z57m2mn9cal1fcd835m6x1jg5q2216cwjw5xnr5")))

(define-public crate-picovoice-3.0.3 (c (n "picovoice") (v "3.0.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "pv_porcupine") (r "=3.0.2") (d #t) (k 0)) (d (n "pv_rhino") (r "=3.0.2") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "1iqzcgiaqq81mkv1xh3xz7v7jcsqgyd5v6glr3sg2d1ivkj90d8y")))

