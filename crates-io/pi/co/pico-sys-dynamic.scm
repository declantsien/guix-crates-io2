(define-module (crates-io pi co pico-sys-dynamic) #:use-module (crates-io))

(define-public crate-pico-sys-dynamic-0.2.0 (c (n "pico-sys-dynamic") (v "0.2.0") (d (list (d (n "libloading") (r "^0.6") (d #t) (k 0)))) (h "16zdc9vqv6raqjji925q2lldr9xi0m0p0m7xy2lcpn34jaqxvbff")))

(define-public crate-pico-sys-dynamic-0.2.1 (c (n "pico-sys-dynamic") (v "0.2.1") (d (list (d (n "libloading") (r "^0.6") (d #t) (k 0)))) (h "0f8i9ql5ryrrbh8mnw2i9q877pkblkpk5rlvip4xha2knm7s110w")))

(define-public crate-pico-sys-dynamic-0.3.0 (c (n "pico-sys-dynamic") (v "0.3.0") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1b53fw12zd8r1622w7xs3zjxxphrbx2wcjgr89aggxbxa6yiq0sy")))

(define-public crate-pico-sys-dynamic-0.3.1 (c (n "pico-sys-dynamic") (v "0.3.1") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1hyb2a99r926xpxagmcgyc5265imnh29964kzllzary5q20wf5x0")))

