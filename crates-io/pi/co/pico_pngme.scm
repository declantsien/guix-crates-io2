(define-module (crates-io pi co pico_pngme) #:use-module (crates-io))

(define-public crate-pico_pngme-0.1.0 (c (n "pico_pngme") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)))) (h "1l3aw5wfi64isk3zx6x7vvqnjxnlvzz0sihws3a81xfxah9fsjj6")))

