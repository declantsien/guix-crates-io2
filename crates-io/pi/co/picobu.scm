(define-module (crates-io pi co picobu) #:use-module (crates-io))

(define-public crate-picobu-1.0.1 (c (n "picobu") (v "1.0.1") (d (list (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "hotwatch") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0gikk4sq1air3lhfa0wd277wwcl0l2kl8arms9p4n45b2b55m64d")))

(define-public crate-picobu-1.0.2 (c (n "picobu") (v "1.0.2") (d (list (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "hotwatch") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1vryx7a427nkdfmgrp5q6wny7d7dp6h9d8wi0b318wrpiw0bk3hr")))

(define-public crate-picobu-1.0.3 (c (n "picobu") (v "1.0.3") (d (list (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "hotwatch") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0qkvhha7jlzqshisq1mfpsxalnhzwkzkig8zyivx291faii3f45a")))

(define-public crate-picobu-1.0.4 (c (n "picobu") (v "1.0.4") (d (list (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "hotwatch") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0xqa6wpp869zflyk2adprwhj8hw441r8fybz6p87y28433j8w97s")))

