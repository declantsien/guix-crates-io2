(define-module (crates-io pi co picolo) #:use-module (crates-io))

(define-public crate-picolo-0.1.0 (c (n "picolo") (v "0.1.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "0i4s9dfpcg2ghiccrjlg6zqpmc64bx9ajwhjhn89z49gdar59ygv")))

(define-public crate-picolo-0.1.1 (c (n "picolo") (v "0.1.1") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "1q2zk4jv9rbk1ixl2qbyzp33h42i4xd10yrv7vkkpc9rwa7x7mrg")))

(define-public crate-picolo-0.1.2 (c (n "picolo") (v "0.1.2") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "0pzz7a3snhw90ygj52bn7vd3hb0rlja0c8vq7m9mgay3j9n9y4c7")))

(define-public crate-picolo-0.1.3 (c (n "picolo") (v "0.1.3") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "07bafvlizvapwx2jnzmmwazd385n7sambr1caxgrqxbbpjq43qin")))

(define-public crate-picolo-0.1.4 (c (n "picolo") (v "0.1.4") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "0wgda4fhzmx0h50j3n3zn658i1mvdcd1jx7bk5lbbjzrb3rc6skz")))

(define-public crate-picolo-0.2.0 (c (n "picolo") (v "0.2.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "1km4rqlrh26mcmylknryx35mahph2j2xkga33klnxsm3hxyk278s")))

(define-public crate-picolo-0.2.1 (c (n "picolo") (v "0.2.1") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "0kqkcpnv4jjd7j4p5zk6zww7d0zn45vxsfn04i3iibvimll6rn4j")))

(define-public crate-picolo-0.2.2 (c (n "picolo") (v "0.2.2") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "0lq308qy9i09y3b3mclh946iri74si0zx5aaa71vd3ykaldqb3xk")))

(define-public crate-picolo-0.2.3 (c (n "picolo") (v "0.2.3") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "1rzhya4knkxq9yr14g7jz6sn4isjdlj7pilcb94lgbvpbwrwd6jw")))

(define-public crate-picolo-0.2.4 (c (n "picolo") (v "0.2.4") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "1zgllm4sh8wlk5bl10yy2f5lf5wrfnak7zvaj4mnxpbyi6vaw4jq")))

(define-public crate-picolo-0.2.5 (c (n "picolo") (v "0.2.5") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "1p2pl8v5xv8k9k54144mp8pvg1r41c23pq07q8lc6gwdccxbw6yd")))

(define-public crate-picolo-0.3.0 (c (n "picolo") (v "0.3.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "1fp6r4vwg0dyhp7fwnmi5kz75p8mvc3m7sjwk289b829msr1q8vg")))

(define-public crate-picolo-0.3.1 (c (n "picolo") (v "0.3.1") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "0r6wbhc255fvbk7hw68vvsvbrcal7j71ss05xl5s8hwciafgy755")))

(define-public crate-picolo-0.3.2 (c (n "picolo") (v "0.3.2") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "0kcqs0l4hjvbbnd8gpxy9fwf8qqy8skd9qra6zq5rvs57cl4r47f")))

(define-public crate-picolo-0.3.3 (c (n "picolo") (v "0.3.3") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "0n36629hd7hylxlrk3ihysd0yknicqdfwxr7hynis9s1bisp9m8g")))

(define-public crate-picolo-0.3.4 (c (n "picolo") (v "0.3.4") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "16j6rlzsj29q1ndvcx2yymd73sqgqhk5bamjqr68brqkwhc0fn7r")))

(define-public crate-picolo-0.3.5 (c (n "picolo") (v "0.3.5") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "104almz82xrbxr8cjw90lq1mrrlsjv6l0hb54kvdl41q7rk42yjy")))

(define-public crate-picolo-0.3.6 (c (n "picolo") (v "0.3.6") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "1cm8qq67lz71ya2hybbxsq5cynjhd87m2wykasgxjk2wfjgrm0kq")))

(define-public crate-picolo-0.3.7 (c (n "picolo") (v "0.3.7") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "0f4hhj534vn2ip5xpq52mp21hj06b20w0kzbq4lm8plalff6mzhk")))

