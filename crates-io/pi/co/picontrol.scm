(define-module (crates-io pi co picontrol) #:use-module (crates-io))

(define-public crate-picontrol-0.1.0 (c (n "picontrol") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "1xy90rj8s6i2ddiksbqbx5lvllgy9q7fx3fhrzcwql3ml3cifvlz")))

(define-public crate-picontrol-0.1.1 (c (n "picontrol") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "10hafrv2fvv7ssznw833x13wmmm8xgi5z092sy0422xsch1afccv")))

(define-public crate-picontrol-0.2.0 (c (n "picontrol") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "03f4lv9j88qyq1zffj8fackb6hwvlf6ys2l9v7pfarkpnsy94hbx")))

(define-public crate-picontrol-0.2.1 (c (n "picontrol") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "068s8q895k6yfg3azidcm9dsv2xgbi9zx0vqykqgp4f5pxpnh2w8")))

