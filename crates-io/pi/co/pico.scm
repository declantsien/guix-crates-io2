(define-module (crates-io pi co pico) #:use-module (crates-io))

(define-public crate-pico-0.0.1 (c (n "pico") (v "0.0.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "pico-sys") (r "*") (d #t) (k 0)))) (h "0fscx1dny3hq8q2v3pi4dbiz4i5vfzq4il46vzyd5ydns942lrnn")))

(define-public crate-pico-0.0.2 (c (n "pico") (v "0.0.2") (d (list (d (n "pico-sys") (r "*") (d #t) (k 0)))) (h "03rdrxmwpkybgnmf5h3vg6xxnc9ppr1ycf17ci0ig2cd00dy33m9")))

