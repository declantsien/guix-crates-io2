(define-module (crates-io pi he pihex) #:use-module (crates-io))

(define-public crate-pihex-0.1.0 (c (n "pihex") (v "0.1.0") (d (list (d (n "clap") (r "^2.21") (d #t) (k 0)))) (h "0n811qw9vibk5ng6w9jhshn4al9djnwm1h8zmnmhd6pxfvz1i8wx")))

(define-public crate-pihex-0.1.1 (c (n "pihex") (v "0.1.1") (d (list (d (n "clap") (r "^2.21") (d #t) (k 0)))) (h "1gaa20mv8q7vdv17b4y2krp4cf1swws17idjimdgidh8l36icd5g")))

(define-public crate-pihex-0.1.2 (c (n "pihex") (v "0.1.2") (d (list (d (n "clap") (r "^2.23.0") (d #t) (k 0)))) (h "1r91382djmaf5lkxrh9y2a7m3a9g021ilvra6z8j4pb2593zqfxx")))

(define-public crate-pihex-0.1.3 (c (n "pihex") (v "0.1.3") (d (list (d (n "clap") (r "^2.23.0") (d #t) (k 0)))) (h "1k9a42p6b2pi1ljwyl6xdvnf5120w5nnfzy6zhl13cpp4psi5shr")))

(define-public crate-pihex-0.1.4 (c (n "pihex") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "04yq1pap43vc68kv2cqb4gawdxq1rdq5wbqyrjfi5gcpsddy95fh")))

(define-public crate-pihex-0.1.5 (c (n "pihex") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1q5h85s790z5vxwidj00g86m174y2n80095ksqr0rmrk01qgp9rq")))

(define-public crate-pihex-0.1.6 (c (n "pihex") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0lgzb26xcwwh0a5m348byx9frmsr94ikvncykwm1phjwxy4xhgrz")))

(define-public crate-pihex-0.1.7 (c (n "pihex") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1ra01zfjvmkgk00vgh4pw4b5habhw6yai2hxsw3i59ydmpkxiyc2")))

(define-public crate-pihex-0.1.8 (c (n "pihex") (v "0.1.8") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "10fif9gjr6ys7dzb5492gg9s70carh7jjjjzqaspyx2v3xl0w7ah")))

