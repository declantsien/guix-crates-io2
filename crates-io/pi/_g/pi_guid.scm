(define-module (crates-io pi _g pi_guid) #:use-module (crates-io))

(define-public crate-pi_guid-0.1.0 (c (n "pi_guid") (v "0.1.0") (d (list (d (n "pi_time") (r "^0.1") (d #t) (k 0)))) (h "05j65p22k5gpqbyzb2b6vmwkz34qvs73m4j49d9d57m8blq7ykzc")))

(define-public crate-pi_guid-0.1.1 (c (n "pi_guid") (v "0.1.1") (d (list (d (n "pi_time") (r "^0.1") (d #t) (k 0)))) (h "00am92baqlcvr2jiapc3z8bm7wr82h52mwv70kycvkp9i3myd7ci")))

(define-public crate-pi_guid-0.1.2 (c (n "pi_guid") (v "0.1.2") (d (list (d (n "pi_time") (r "^0.3.0") (d #t) (k 0)))) (h "13nf9bw80hhg340jkbaih7aca01s7x0qhjmay1vgf38vgfxzvkjk")))

(define-public crate-pi_guid-0.1.3 (c (n "pi_guid") (v "0.1.3") (d (list (d (n "pi_time") (r "^0.3.0") (d #t) (k 0)))) (h "17rc91ni3062vd9zfcv8n2dn64wi8nz86yia3clqkzgyppv6pgxs")))

