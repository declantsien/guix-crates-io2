(define-module (crates-io pi _g pi_guid64) #:use-module (crates-io))

(define-public crate-pi_guid64-0.1.0 (c (n "pi_guid64") (v "0.1.0") (d (list (d (n "pi_time") (r "^0.1") (d #t) (k 0)))) (h "05cv5jvag47j1xck93rmwbgdzbyiyxq592p92ckxwywj13bk7ygx")))

(define-public crate-pi_guid64-0.1.1 (c (n "pi_guid64") (v "0.1.1") (d (list (d (n "pi_time") (r "^0.1") (d #t) (k 0)))) (h "1asc7x6mlcgklndcqfcs7l4b9zzjq48wix0qma1b71xi01l3yxd1")))

(define-public crate-pi_guid64-0.1.2 (c (n "pi_guid64") (v "0.1.2") (d (list (d (n "pi_time") (r "^0.1") (d #t) (k 0)))) (h "0mg0dbc3znnxc8nf45snk7qg6j5lvp2yyrnjdsbn7aa0n43jzp11")))

