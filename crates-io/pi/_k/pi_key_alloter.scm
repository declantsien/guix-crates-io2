(define-module (crates-io pi _k pi_key_alloter) #:use-module (crates-io))

(define-public crate-pi_key_alloter-0.1.2 (c (n "pi_key_alloter") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1j9c6szvx7ry05dm4n0ysl45x1qjncm4qr3ws72yih6xvlnll61q") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.2.0 (c (n "pi_key_alloter") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0l1lvnaqk7x95bxf31s16xindc9pghnawax8v3s6vxah9lvzqsfg") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.2.1 (c (n "pi_key_alloter") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0y4mvn4aif6gn72b2xslky6my6v2jynkl3k1rs93m905jx6ydzmz") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.3.0 (c (n "pi_key_alloter") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1s79ifzll0cm0cnky08624fks5izx9c20414qs9nvcbxhfmpzl53") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.3.1 (c (n "pi_key_alloter") (v "0.3.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0yp81lw1frd432ia0hfk1y5yv31izqbym695wp8w2k43bbh1957f") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.4.0 (c (n "pi_key_alloter") (v "0.4.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1y9w5zbfxj95146ch0pzmjf5n9936ng71alv3rf9wsm97nx5b48g") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.4.1 (c (n "pi_key_alloter") (v "0.4.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "15i04zkq8swrgmwc8fiamk178cnaqrzd9zpidyrfx0aaaish79kg") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.4.2 (c (n "pi_key_alloter") (v "0.4.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0m277ngiiyl9a5ygy0fbr2hrlz9gf9hzbdvrknd3pyf1sv2vdx6l") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.4.3 (c (n "pi_key_alloter") (v "0.4.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0jpq2lb3ppifjv0b8hjkpp3vawxlp8xqc8w49xkxi82yqw73124w") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.4.4 (c (n "pi_key_alloter") (v "0.4.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "05kf5np0ww061wkz11svbikr0gwsf27nsnssdyhagc4r9a0fdr58") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.4.5 (c (n "pi_key_alloter") (v "0.4.5") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1mr78gaq18fljrcv1wcp7f3vpfazrgab0bblirsm5v78j300iav2") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.4.6 (c (n "pi_key_alloter") (v "0.4.6") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1px99zzdqdgh0pwsl0jc989f646r17cw2bkhbrg7v17zvyh84drv") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.4.7 (c (n "pi_key_alloter") (v "0.4.7") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0dbjll596nfwj9z6w4gy7w8c65c07nswf2y6g95gzjdd6wfplvjz") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.4.9 (c (n "pi_key_alloter") (v "0.4.9") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0pwn1z4dycv7d3c8cxsp2ir9dca34pxjl91s0m83kzm29pjjyqsd") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.4.10 (c (n "pi_key_alloter") (v "0.4.10") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "11nc4jvxqszrijq2dbzgggypg74zz7m7l0c3wqjnwk8xhkp883n2") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.5.0 (c (n "pi_key_alloter") (v "0.5.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "05lxrhrxyi6w05phxdx9vb50x15dzyiq2v2jz89r029g6389qx4a") (f (quote (("rc"))))))

(define-public crate-pi_key_alloter-0.5.1 (c (n "pi_key_alloter") (v "0.5.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1b37q21k6savdpm3bacyxflv4yjf59cl89s06lss6n63i5a0skvi") (f (quote (("rc"))))))

