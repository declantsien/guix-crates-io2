(define-module (crates-io pi et piet-svg) #:use-module (crates-io))

(define-public crate-piet-svg-0.0.8 (c (n "piet-svg") (v "0.0.8") (d (list (d (n "piet") (r "^0.0.8") (d #t) (k 0)) (d (n "piet-test") (r "^0.0.8") (d #t) (k 2)) (d (n "svg") (r "^0.6") (d #t) (k 0)))) (h "1vg6cpqlyfc1jir2ya0c5cc2rg5bfa7jb3gxdagdk50lmyij88sv")))

(define-public crate-piet-svg-0.0.9 (c (n "piet-svg") (v "0.0.9") (d (list (d (n "piet") (r "^0.0.9") (d #t) (k 0)) (d (n "piet-test") (r "^0.0.9") (d #t) (k 2)) (d (n "svg") (r "^0.6") (d #t) (k 0)))) (h "17qwpyir411gjwpw85n5ksxbgi1gq8wp2s41pm1dcwlf62c7r166")))

(define-public crate-piet-svg-0.0.10 (c (n "piet-svg") (v "0.0.10") (d (list (d (n "piet") (r "^0.0.10") (d #t) (k 0)) (d (n "piet-test") (r "^0.0.10") (d #t) (k 2)) (d (n "svg") (r "^0.6") (d #t) (k 0)))) (h "1l26bavqq84fzhic0lcd80q0qisk66qwsrhmsnavrkp8p3pdcwr2")))

(define-public crate-piet-svg-0.0.11 (c (n "piet-svg") (v "0.0.11") (d (list (d (n "piet") (r "^0.0.11") (d #t) (k 0)) (d (n "piet-test") (r "^0.0.11") (d #t) (k 2)) (d (n "svg") (r "^0.7.1") (d #t) (k 0)))) (h "1qj3pclalrd3nb7b1xmj7djx5j2sdcziirv19q4lgnybfrgwqq2g")))

(define-public crate-piet-svg-0.0.12 (c (n "piet-svg") (v "0.0.12") (d (list (d (n "piet") (r "^0.0.12") (d #t) (k 0)) (d (n "piet-test") (r "^0.0.12") (d #t) (k 2)) (d (n "svg") (r "^0.7.1") (d #t) (k 0)))) (h "0rpwrcsw1703vya0y9hp0f5rwk2c895l8qdy105j90c3vgpkk7d4")))

(define-public crate-piet-svg-0.0.13 (c (n "piet-svg") (v "0.0.13") (d (list (d (n "piet") (r "^0.0.13") (d #t) (k 0)) (d (n "piet") (r "^0.0.13") (f (quote ("samples"))) (d #t) (k 2)) (d (n "svg") (r "^0.8.0") (d #t) (k 0)))) (h "1h7gl8m1vq43gibyfpcdj803fq0q88pxv0afza7krkccz2r8zf3b")))

(define-public crate-piet-svg-0.1.0 (c (n "piet-svg") (v "0.1.0") (d (list (d (n "piet") (r "^0.1.0") (d #t) (k 0)) (d (n "piet") (r "^0.1.0") (f (quote ("samples"))) (d #t) (k 2)) (d (n "svg") (r "^0.8.0") (d #t) (k 0)))) (h "0jvxrjz8xms6kgzmnqz566cgf4m9mlwf9asih06caksbws0ilnfd")))

(define-public crate-piet-svg-0.2.0-pre1 (c (n "piet-svg") (v "0.2.0-pre1") (d (list (d (n "piet") (r "^0.2.0-pre1") (d #t) (k 0)) (d (n "piet") (r "^0.2.0-pre1") (f (quote ("samples"))) (d #t) (k 2)) (d (n "svg") (r "^0.8.0") (d #t) (k 0)))) (h "0wph5s9j1m496xw4a16qvfq3bmr1is9f5v0z679cbizpjv4wy36r")))

(define-public crate-piet-svg-0.2.0-pre2 (c (n "piet-svg") (v "0.2.0-pre2") (d (list (d (n "piet") (r "^0.2.0-pre2") (d #t) (k 0)) (d (n "piet") (r "^0.2.0-pre2") (f (quote ("samples"))) (d #t) (k 2)) (d (n "svg") (r "^0.8.0") (d #t) (k 0)))) (h "14ccvwg5ry2j9018mn2xi2ph3vnqhmc6dqwvnnvwpd4zm6hmpk9d")))

(define-public crate-piet-svg-0.2.0-pre3 (c (n "piet-svg") (v "0.2.0-pre3") (d (list (d (n "piet") (r "^0.2.0-pre3") (d #t) (k 0)) (d (n "piet") (r "^0.2.0-pre3") (f (quote ("samples"))) (d #t) (k 2)) (d (n "svg") (r "^0.8.0") (d #t) (k 0)))) (h "18ga0asy8qj6c8nv3ma754s60dg7j71gynj8wy1v5y9pl4v3ccdl")))

(define-public crate-piet-svg-0.2.0-pre4 (c (n "piet-svg") (v "0.2.0-pre4") (d (list (d (n "piet") (r "^0.2.0-pre4") (d #t) (k 0)) (d (n "piet") (r "^0.2.0-pre4") (f (quote ("samples"))) (d #t) (k 2)) (d (n "svg") (r "^0.8.0") (d #t) (k 0)))) (h "1vg2rryywldd6nyxgj9a0lv5i9x9agf4cd5jxf1gacq8paq454az")))

(define-public crate-piet-svg-0.2.0-pre5 (c (n "piet-svg") (v "0.2.0-pre5") (d (list (d (n "piet") (r "^0.2.0-pre5") (d #t) (k 0)) (d (n "piet") (r "^0.2.0-pre5") (f (quote ("samples"))) (d #t) (k 2)) (d (n "svg") (r "^0.8.0") (d #t) (k 0)))) (h "11mbj9ygz5ag9yf1zhqi2k5cg67jhip4y682hhg4q77qg6bbjrm5")))

(define-public crate-piet-svg-0.2.0-pre6 (c (n "piet-svg") (v "0.2.0-pre6") (d (list (d (n "piet") (r "=0.2.0-pre6") (d #t) (k 0)) (d (n "piet") (r "=0.2.0-pre6") (f (quote ("samples"))) (d #t) (k 2)) (d (n "svg") (r "^0.8.0") (d #t) (k 0)))) (h "0ki67g01xxhalpihrf9ax0f3ciz6ylsgvq46rmjyxj4ync2q8h8d")))

(define-public crate-piet-svg-0.2.0 (c (n "piet-svg") (v "0.2.0") (d (list (d (n "piet") (r "^0.2.0") (d #t) (k 0)) (d (n "piet") (r "^0.2.0") (f (quote ("samples"))) (d #t) (k 2)) (d (n "svg") (r "^0.8.0") (d #t) (k 0)))) (h "0m04w8accsbqh6ysbc3hc9dryj98yllm677ag0n3d0ysnqsrvcsf")))

(define-public crate-piet-svg-0.3.0 (c (n "piet-svg") (v "0.3.0") (d (list (d (n "piet") (r "^0.3.0") (d #t) (k 0)) (d (n "piet") (r "^0.3.0") (f (quote ("samples"))) (d #t) (k 2)) (d (n "svg") (r "^0.8.0") (d #t) (k 0)))) (h "1awfjlrybbnx3s8ip64fk65xc2va5lz2ayq33yhmysr1jpbjs7xk")))

(define-public crate-piet-svg-0.4.0 (c (n "piet-svg") (v "0.4.0") (d (list (d (n "piet") (r "^0.4.0") (d #t) (k 0)) (d (n "piet") (r "^0.4.0") (f (quote ("samples"))) (d #t) (k 2)) (d (n "svg") (r "^0.9.0") (d #t) (k 0)))) (h "1m3whg31mfklzxnnymgai29ljfs7qfvqi990qsck801zhnzw0r45")))

(define-public crate-piet-svg-0.5.0-pre1 (c (n "piet-svg") (v "0.5.0-pre1") (d (list (d (n "piet") (r "=0.5.0-pre1") (d #t) (k 0)) (d (n "piet") (r "=0.5.0-pre1") (f (quote ("samples"))) (d #t) (k 2)) (d (n "svg") (r "^0.10.0") (d #t) (k 0)))) (h "0zd9i28aq4i4w0n5d3sj5yg065pxcs2c9ndcy9iqslf8zzgkv4g7")))

(define-public crate-piet-svg-0.5.0 (c (n "piet-svg") (v "0.5.0") (d (list (d (n "piet") (r "=0.5.0") (d #t) (k 0)) (d (n "piet") (r "=0.5.0") (f (quote ("samples"))) (d #t) (k 2)) (d (n "svg") (r "^0.10.0") (d #t) (k 0)))) (h "0y840irap08w01jwrbq5w1vm62n5kbnild9hqj7sr6603a72gwdq")))

(define-public crate-piet-svg-0.6.0 (c (n "piet-svg") (v "0.6.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "evcxr_runtime") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "font-kit") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (f (quote ("png"))) (k 0)) (d (n "piet") (r "=0.6.0") (d #t) (k 0)) (d (n "piet") (r "=0.6.0") (f (quote ("samples"))) (d #t) (k 2)) (d (n "rustybuzz") (r "^0.4.0") (d #t) (k 0)) (d (n "svg") (r "^0.10.0") (d #t) (k 0)))) (h "1sg8lx5x8cb727417lrn70x0cflqj1vq351m85xi4fa8viw0ad8m") (f (quote (("evcxr" "evcxr_runtime") ("default"))))))

(define-public crate-piet-svg-0.6.1 (c (n "piet-svg") (v "0.6.1") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "evcxr_runtime") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "font-kit") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("png"))) (k 0)) (d (n "piet") (r "=0.6.1") (d #t) (k 0)) (d (n "rustybuzz") (r "^0.4.0") (d #t) (k 0)) (d (n "svg") (r "^0.10.0") (d #t) (k 0)) (d (n "piet") (r "=0.6.1") (f (quote ("samples"))) (d #t) (k 2)))) (h "1mvxlmlr9gw45phz8jk9ipnqx8bazcq46racjiwf0hrl5asb47s0") (f (quote (("evcxr" "evcxr_runtime") ("default"))))))

(define-public crate-piet-svg-0.6.2 (c (n "piet-svg") (v "0.6.2") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "evcxr_runtime") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "font-kit") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("png"))) (k 0)) (d (n "piet") (r "=0.6.2") (d #t) (k 0)) (d (n "rustybuzz") (r "^0.4.0") (d #t) (k 0)) (d (n "svg") (r "^0.10.0") (d #t) (k 0)) (d (n "piet") (r "=0.6.2") (f (quote ("samples"))) (d #t) (k 2)))) (h "0nfrsshgkjqbhn6ahjamqv9jd7iwfz3z3a61a9v0h9nry04y4s4b") (f (quote (("evcxr" "evcxr_runtime") ("default"))))))

