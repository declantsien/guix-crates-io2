(define-module (crates-io pi et pietro-test-crate) #:use-module (crates-io))

(define-public crate-pietro-test-crate-0.1.0 (c (n "pietro-test-crate") (v "0.1.0") (h "0hcdy3xv322j8733zcp9zgfzahdlhvkw47mrdcdjas8lygr0660i")))

(define-public crate-pietro-test-crate-0.2.0 (c (n "pietro-test-crate") (v "0.2.0") (h "0zdj5k140hqwi8b24zzqks49p7vakbb3wcvrj3hij3w9yil651ks")))

(define-public crate-pietro-test-crate-0.3.0 (c (n "pietro-test-crate") (v "0.3.0") (h "0pxp6pvq0zz38a9xlfx122faf2w12cswy8rkrr38lcc6z0xgs9l4")))

(define-public crate-pietro-test-crate-0.4.0 (c (n "pietro-test-crate") (v "0.4.0") (h "06h91h3i41f2g8myldfw3byw5vb5pgqr409ca4vayqgpz60ys3sf")))

(define-public crate-pietro-test-crate-0.5.0 (c (n "pietro-test-crate") (v "0.5.0") (h "1j80w642lh3c3x8aya6hqhiphjgg23phq4bdfq5q0p1h57wimxi6")))

(define-public crate-pietro-test-crate-0.6.0 (c (n "pietro-test-crate") (v "0.6.0") (h "1i5b6gyjd32mdvmsnc1a3clff0i5afln0y325mhnqixyfniajirm")))

(define-public crate-pietro-test-crate-0.7.0 (c (n "pietro-test-crate") (v "0.7.0") (h "1zppgh15zqy9mpcv4x9lwbgjcdcngphi7r3qafn9ab9rzi3jcnjk")))

