(define-module (crates-io pi et piet-test) #:use-module (crates-io))

(define-public crate-piet-test-0.0.1 (c (n "piet-test") (v "0.0.1") (d (list (d (n "kurbo") (r "^0.2.0") (d #t) (k 0)) (d (n "piet") (r "^0.0.1") (d #t) (k 0)))) (h "05i9r1306ip83p96bnqg4pkna4v4waaas8pd1c97ldbprnr8dvrx")))

(define-public crate-piet-test-0.0.2 (c (n "piet-test") (v "0.0.2") (d (list (d (n "kurbo") (r "^0.2.1") (d #t) (k 0)) (d (n "piet") (r "^0.0.2") (d #t) (k 0)))) (h "196l65hvyx8id2yz8kiwd0c13x6pn14ggqv3jp3invl9xdzxa2iw")))

(define-public crate-piet-test-0.0.3 (c (n "piet-test") (v "0.0.3") (d (list (d (n "piet") (r "^0.0.3") (d #t) (k 0)))) (h "0p8kiw635b8vsgp6pvdkflnrgzws65br74qm6hnv0qx8qap2vyzx")))

(define-public crate-piet-test-0.0.4 (c (n "piet-test") (v "0.0.4") (d (list (d (n "piet") (r "^0.0.4") (d #t) (k 0)))) (h "0m7g80mhn8j4447h1dnmq1y9rjgibi57imcdpb59161bdjcxyinf")))

(define-public crate-piet-test-0.0.5 (c (n "piet-test") (v "0.0.5") (d (list (d (n "piet") (r "^0.0.5") (d #t) (k 0)))) (h "1giar006vvfj393dppzcma0jg1nql5m0aclrmrvqzldsg5c22v2f")))

(define-public crate-piet-test-0.0.6 (c (n "piet-test") (v "0.0.6") (d (list (d (n "piet") (r "^0.0.6") (d #t) (k 0)))) (h "1nzzh9009alidkm7vkm7qzi26276nx0zh0zvhfmhy3088c4jqln6")))

(define-public crate-piet-test-0.0.7 (c (n "piet-test") (v "0.0.7") (d (list (d (n "piet") (r "^0.0.7") (d #t) (k 0)))) (h "1hvgzki5mcva3cwmbvva64axq08zpx0hpym8vgcy36sis6mncbvl")))

(define-public crate-piet-test-0.0.8 (c (n "piet-test") (v "0.0.8") (d (list (d (n "piet") (r "^0.0.8") (d #t) (k 0)))) (h "1fd1baimbf75iwc40n9dyj3v5z2m4512n4ik79nhvdli4gh1b8j1")))

(define-public crate-piet-test-0.0.9 (c (n "piet-test") (v "0.0.9") (d (list (d (n "piet") (r "^0.0.9") (d #t) (k 0)))) (h "15kkiqrcb1fz6i2skv8fzvffr8k2hkjb001rq12lgr6kcg43hmb5")))

(define-public crate-piet-test-0.0.10 (c (n "piet-test") (v "0.0.10") (d (list (d (n "piet") (r "^0.0.10") (d #t) (k 0)))) (h "0xix4r3a1qv8vv1pzq1g6iy27kphfxp9vkgvr1df61az1bq5s85l")))

(define-public crate-piet-test-0.0.11 (c (n "piet-test") (v "0.0.11") (d (list (d (n "piet") (r "^0.0.11") (d #t) (k 0)))) (h "1a4cm7b5m5ax6av0md8k8m9q1rpzpr97dw0mihcyy68rl91rd7a3")))

(define-public crate-piet-test-0.0.12 (c (n "piet-test") (v "0.0.12") (d (list (d (n "piet") (r "^0.0.12") (d #t) (k 0)))) (h "1970qrg2rp10jj36m6rzy0v7c7llahgymj8pwnr13clz1x5kjlyh")))

