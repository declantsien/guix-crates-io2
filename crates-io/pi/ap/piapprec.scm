(define-module (crates-io pi ap piapprec) #:use-module (crates-io))

(define-public crate-piapprec-0.1.0 (c (n "piapprec") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.0") (d #t) (k 0)))) (h "1mvwb5zcx4kvzlmd88jd6mg4x4kw675in5x00ahfhdm4kq3qlkzq")))

(define-public crate-piapprec-0.1.1 (c (n "piapprec") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.0") (d #t) (k 0)))) (h "0nhklirqqc83lb3b1bsyw72v6vf7lvbsdab9y2s72gi3n8ps7iij")))

