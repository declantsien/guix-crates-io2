(define-module (crates-io pi na pinar) #:use-module (crates-io))

(define-public crate-pinar-0.1.0 (c (n "pinar") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "napi-sys") (r "^0.1") (d #t) (k 0) (p "pinar-napi-sys")) (d (n "pinar-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "1.*") (o #t) (d #t) (k 0)))) (h "1wyps70wc815n38k9g1x0kdd7yagsqx34jjy6p6bdm3aqd3aijq9") (f (quote (("pinar-serde" "serde" "serde_derive" "pinar-derive") ("default" "pinar-serde"))))))

