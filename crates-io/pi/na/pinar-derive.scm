(define-module (crates-io pi na pinar-derive) #:use-module (crates-io))

(define-public crate-pinar-derive-0.1.0 (c (n "pinar-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)))) (h "0kbyi9f4hgzja8w54qrg06dd48adw3d62agkijbpjzzyb6wsx0ji")))

(define-public crate-pinar-derive-0.2.0 (c (n "pinar-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits" "clone-impls" "visit" "visit-mut" "derive"))) (d #t) (k 0)))) (h "0hgqvn3bc32rpmvcj61y2ljpcr3xgc283f4z2nh9zwslz4jddggz")))

