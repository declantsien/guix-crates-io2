(define-module (crates-io pi na pinarcmutex) #:use-module (crates-io))

(define-public crate-pinarcmutex-0.1.0 (c (n "pinarcmutex") (v "0.1.0") (d (list (d (n "tokio") (r "^1.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "0j9mbfkb9lv8sjih7y39xjwypffh92qbysy2aiqhl28dary69lcl") (r "1.49")))

(define-public crate-pinarcmutex-0.1.1 (c (n "pinarcmutex") (v "0.1.1") (d (list (d (n "tokio") (r "^1.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "177gbzzczn0i87q495y67niy6ld6f3mrqhx82mkrr3fkc5a9lw5r") (r "1.49")))

