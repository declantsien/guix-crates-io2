(define-module (crates-io pi na pinata_ipfs) #:use-module (crates-io))

(define-public crate-pinata_ipfs-0.1.0 (c (n "pinata_ipfs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("multipart"))) (d #t) (k 0)))) (h "0fsjcp0drn5jjh1swga7kz0d0x82a9s7ff7di4ap5jyy4xi7n6nn")))

(define-public crate-pinata_ipfs-0.1.1 (c (n "pinata_ipfs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("multipart"))) (d #t) (k 0)))) (h "1yx75qx886mh6iakcc3lf4n5m2287d4bbs3pzra3gmjf7x77z142")))

(define-public crate-pinata_ipfs-0.1.2 (c (n "pinata_ipfs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("multipart"))) (d #t) (k 0)))) (h "1xc97wschxjjkg4s72v081m1b8jh78i4ximmy7sl44m3wr6bk3qm")))

(define-public crate-pinata_ipfs-0.1.3 (c (n "pinata_ipfs") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("multipart"))) (d #t) (k 0)))) (h "0pynlrha3mih2921mmw3xqhkp02m2p03ldpvqmm7r98w6cv0a3gw")))

(define-public crate-pinata_ipfs-0.1.4 (c (n "pinata_ipfs") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("multipart"))) (d #t) (k 0)))) (h "0iz97vz6qgm7z9rncrnc88fav57ywzvn8d2kmhj30852scbk5y4s")))

(define-public crate-pinata_ipfs-0.1.5 (c (n "pinata_ipfs") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("multipart"))) (d #t) (k 0)))) (h "09f4qq7f0f6jnxkfbcmyr4va5mxpnva55lkwz0zwadrpz9g6w4k6")))

(define-public crate-pinata_ipfs-0.1.6 (c (n "pinata_ipfs") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("multipart"))) (d #t) (k 0)))) (h "0pr8b989sm1i5pvgn8yyl95z9z056lps0z3yl8lpb6jnd24bfmcp")))

(define-public crate-pinata_ipfs-0.1.7 (c (n "pinata_ipfs") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("multipart"))) (d #t) (k 0)))) (h "10bq46nkvz0dp34w9h4fc2knh3sj3blnglkslysr1530az887vaj")))

(define-public crate-pinata_ipfs-0.1.8 (c (n "pinata_ipfs") (v "0.1.8") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("multipart"))) (d #t) (k 0)))) (h "1fsmaai3n0shz1n3g9i3igjjr50fh651v3xbxc9lvg261kw2yf84")))

(define-public crate-pinata_ipfs-0.1.9 (c (n "pinata_ipfs") (v "0.1.9") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("multipart"))) (d #t) (k 0)))) (h "1883d3pi0zs7d3r0v5ayp4vwvm6q5wzfv85qhns3kvh1qmc36pcb")))

