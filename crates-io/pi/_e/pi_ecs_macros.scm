(define-module (crates-io pi _e pi_ecs_macros) #:use-module (crates-io))

(define-public crate-pi_ecs_macros-0.1.0 (c (n "pi_ecs_macros") (v "0.1.0") (d (list (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vxqscwhhswbal1rmvj0vqf0xnbs3yx8gnxcr059fn91ikalvwqn")))

