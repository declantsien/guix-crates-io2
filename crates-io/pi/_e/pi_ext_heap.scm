(define-module (crates-io pi _e pi_ext_heap) #:use-module (crates-io))

(define-public crate-pi_ext_heap-0.1.0 (c (n "pi_ext_heap") (v "0.1.0") (h "0hiwxksaxjyys562gy11rkz1i9sgwc9dlqw8l79d77wl8rygpp5c")))

(define-public crate-pi_ext_heap-0.1.1 (c (n "pi_ext_heap") (v "0.1.1") (h "0ci9jpsfsc2mb886apiij09q4ba1yy5gw98ggc0rr89ikd37sqw9")))

