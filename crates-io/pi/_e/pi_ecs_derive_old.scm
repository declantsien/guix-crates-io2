(define-module (crates-io pi _e pi_ecs_derive_old) #:use-module (crates-io))

(define-public crate-pi_ecs_derive_old-0.1.0 (c (n "pi_ecs_derive_old") (v "0.1.0") (d (list (d (n "pi_ecs_old") (r "^0.1") (d #t) (k 0)) (d (n "pi_map") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g05p8dz97893cvq8cgg59k9dr3w6ipyix464jb7z3fm7fpra19f")))

