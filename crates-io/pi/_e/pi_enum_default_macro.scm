(define-module (crates-io pi _e pi_enum_default_macro) #:use-module (crates-io))

(define-public crate-pi_enum_default_macro-0.1.0 (c (n "pi_enum_default_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qgl716imqahiz45lyyqp22hffksr4m8ifmvrgwl8h5iskykjf8a")))

