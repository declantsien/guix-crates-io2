(define-module (crates-io pi ny pinyin-order) #:use-module (crates-io))

(define-public crate-pinyin-order-0.1.0 (c (n "pinyin-order") (v "0.1.0") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "regex") (r "^0.2") (d #t) (k 1)))) (h "1aq2x1368gins85cpkrwd2vjscqqhqhvpc960rbd2nwddip1bmnk")))

(define-public crate-pinyin-order-0.1.1 (c (n "pinyin-order") (v "0.1.1") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "regex") (r "^0.2") (d #t) (k 1)))) (h "014v26k8bq5gy6ymbd4wy7j6la0bd72yzrswrxik1jpng8d2c0zz")))

(define-public crate-pinyin-order-0.1.2 (c (n "pinyin-order") (v "0.1.2") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "regex") (r "^0.2") (d #t) (k 1)))) (h "1asy9274hnk5s8l4qw5ksg5hqlinhgf81sw8kxma73ngpbh8m2a3")))

