(define-module (crates-io pi ny pinyin) #:use-module (crates-io))

(define-public crate-pinyin-0.0.1 (c (n "pinyin") (v "0.0.1") (d (list (d (n "phf") (r "~0.7.3") (d #t) (k 0)) (d (n "phf_codegen") (r "~0.7.3") (d #t) (k 1)) (d (n "regex") (r "~0.1.8") (d #t) (k 0)))) (h "08svf6k4a3737ajyb96jawnhs1pm89al7dkn1j19xydn6ricwjiq")))

(define-public crate-pinyin-0.0.2 (c (n "pinyin") (v "0.0.2") (d (list (d (n "phf") (r "~0.7.3") (d #t) (k 0)) (d (n "phf_codegen") (r "~0.7.3") (d #t) (k 1)) (d (n "regex") (r "~0.1.8") (d #t) (k 0)))) (h "1fric7mlh8zwzivvs13kpwx5ffw6k15fvrdfdxk58bmnnhy2rzs5")))

(define-public crate-pinyin-0.0.4 (c (n "pinyin") (v "0.0.4") (d (list (d (n "phf") (r "~0.7.3") (d #t) (k 0)) (d (n "phf_codegen") (r "~0.7.3") (d #t) (k 1)) (d (n "regex") (r "~0.1.8") (d #t) (k 0)))) (h "1xpwhhha5wqamchgk0zai5l3rcif541c2r3bzy2s7m5ijskwc665") (f (quote (("unstable"))))))

(define-public crate-pinyin-0.0.5 (c (n "pinyin") (v "0.0.5") (d (list (d (n "phf") (r "~0.7.3") (d #t) (k 0)) (d (n "phf_codegen") (r "~0.7.3") (d #t) (k 1)) (d (n "regex") (r "~0.1.8") (d #t) (k 0)))) (h "0qgyf4rzriyish963z4nqg9jkchfxzdj7bdib9rx795mfjqzirc3") (f (quote (("unstable"))))))

(define-public crate-pinyin-0.0.6 (c (n "pinyin") (v "0.0.6") (d (list (d (n "phf") (r "~0.7.3") (d #t) (k 0)) (d (n "phf_codegen") (r "~0.7.3") (d #t) (k 1)) (d (n "regex") (r "~0.1.8") (d #t) (k 0)))) (h "036v8sklnf6q07c7lh5kx1hyxk6gs4xb9ri75lkx3nz0sl41c6vf") (f (quote (("unstable"))))))

(define-public crate-pinyin-0.1.0 (c (n "pinyin") (v "0.1.0") (d (list (d (n "phf") (r "~0.7.3") (d #t) (k 0)) (d (n "phf_codegen") (r "~0.7.3") (d #t) (k 1)) (d (n "regex") (r "~0.1.8") (d #t) (k 0)) (d (n "regex") (r "~0.1.8") (d #t) (k 1)))) (h "02mi5bnqs52vvx7dc4jdbr4fzsyd0fps1i164dw7f3a810pi7264") (f (quote (("unstable"))))))

(define-public crate-pinyin-0.2.0 (c (n "pinyin") (v "0.2.0") (d (list (d (n "phf") (r "~0.7.3") (d #t) (k 0)) (d (n "phf_codegen") (r "~0.7.3") (d #t) (k 1)) (d (n "regex") (r "~0.1.8") (d #t) (k 0)) (d (n "regex") (r "~0.1.8") (d #t) (k 1)))) (h "1p3r9j55nw9q4q4m0kh39j63dxmgwrayibc26aflh7p3va7r0ks6") (f (quote (("unstable"))))))

(define-public crate-pinyin-0.3.0 (c (n "pinyin") (v "0.3.0") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 1)))) (h "0hxp60270z46lqkw2k3vzsgmc972h3mmg4mx0al25zn25nsls38x") (f (quote (("unstable"))))))

(define-public crate-pinyin-0.4.0 (c (n "pinyin") (v "0.4.0") (h "16dc6c5jj41bdwxkncd5ifxghxlb3879vpn4y2xw5lg89rjdjjsv") (f (quote (("unstable"))))))

(define-public crate-pinyin-0.5.0 (c (n "pinyin") (v "0.5.0") (h "1f1i3x0nsm3qp1515448v7iph27icwdy7vzdm1xihbz33kywwwac") (f (quote (("unstable"))))))

(define-public crate-pinyin-0.6.0 (c (n "pinyin") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "010irv84rbmzia11di82a3b4bbcnjmi3536km55c53grxa2dbbfk") (f (quote (("unstable"))))))

(define-public crate-pinyin-0.7.0 (c (n "pinyin") (v "0.7.0") (h "132s89n6h6bs0h8nxw6ks4gjihzs17snqzg8asyl7s1gm8nmcw3s") (f (quote (("with_tone_num_end") ("with_tone_num") ("with_tone") ("plain") ("heteronym") ("default" "compat" "plain" "with_tone" "with_tone_num" "with_tone_num_end" "heteronym") ("compat" "plain" "with_tone" "with_tone_num" "heteronym"))))))

(define-public crate-pinyin-0.8.0 (c (n "pinyin") (v "0.8.0") (h "0n9s3j09cf6j18v2rps8bxppjs7lv5342c0v4la8zs8cgyb6idzc") (f (quote (("with_tone_num_end") ("with_tone_num") ("with_tone") ("plain") ("heteronym") ("default" "compat" "plain" "with_tone" "with_tone_num" "with_tone_num_end" "heteronym") ("compat" "plain" "with_tone" "with_tone_num" "heteronym"))))))

(define-public crate-pinyin-0.9.0 (c (n "pinyin") (v "0.9.0") (h "1i30n9gpzf3k6hmm9sjrma6pfmkhgbrpvx82w19438xgwcv27l9v") (f (quote (("with_tone_num_end") ("with_tone_num") ("with_tone") ("plain") ("heteronym") ("default" "compat" "plain" "with_tone" "with_tone_num" "with_tone_num_end" "heteronym") ("compat" "plain" "with_tone" "with_tone_num" "heteronym"))))))

(define-public crate-pinyin-0.10.0 (c (n "pinyin") (v "0.10.0") (h "1h2glzd6iaii0vh4wchhl5n8l1pbx4fm596fl0ww46kas0f63whn") (f (quote (("with_tone_num_end") ("with_tone_num") ("with_tone") ("plain") ("heteronym") ("default" "compat" "plain" "with_tone" "with_tone_num" "with_tone_num_end" "heteronym") ("compat" "plain" "with_tone" "with_tone_num" "heteronym"))))))

