(define-module (crates-io pi ny pinyin-tool) #:use-module (crates-io))

(define-public crate-pinyin-tool-0.1.0 (c (n "pinyin-tool") (v "0.1.0") (d (list (d (n "jieba-rs") (r "^0.4") (d #t) (k 0)) (d (n "pinyin") (r "^0.7") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.14") (d #t) (k 0)))) (h "1yibg7mdqr2b6bzhghgb5x22g152wi8bf7qn1zjbpl58mbrniv8z")))

(define-public crate-pinyin-tool-0.1.1 (c (n "pinyin-tool") (v "0.1.1") (d (list (d (n "jieba-rs") (r "^0.4") (d #t) (k 0)) (d (n "pinyin") (r "^0.7") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.14") (d #t) (k 0)))) (h "04fwv8n7b0nj8qfl3gqxzzhmmwsd3qhlyc70sjwfzixs8hcr2c5p")))

(define-public crate-pinyin-tool-0.1.2 (c (n "pinyin-tool") (v "0.1.2") (d (list (d (n "jieba-rs") (r "^0.4") (d #t) (k 0)) (d (n "pinyin") (r "^0.7") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.14") (d #t) (k 0)))) (h "0g0i3nllcbmlg42vqv43cp64vhp7gllljxgwmq3kh86gaakb0gbr")))

(define-public crate-pinyin-tool-0.1.3 (c (n "pinyin-tool") (v "0.1.3") (d (list (d (n "jieba-rs") (r "^0.4") (d #t) (k 0)) (d (n "pinyin") (r "^0.7") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.14") (d #t) (k 0)))) (h "0j7s8mh1h9bsbxmyp1ksygw8whxag5ka809xyj8a2vk3d70bd7hd")))

