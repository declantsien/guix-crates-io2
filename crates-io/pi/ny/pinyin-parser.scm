(define-module (crates-io pi ny pinyin-parser) #:use-module (crates-io))

(define-public crate-pinyin-parser-0.1.0 (c (n "pinyin-parser") (v "0.1.0") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0icqy4nninjri93m1r439cfm41aayh72fjcgsczp5ck801q98n7b")))

(define-public crate-pinyin-parser-0.1.1 (c (n "pinyin-parser") (v "0.1.1") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1lz2f5ss0x51wzw0sgh38djbybpl65bv0ivkhysj0c6dr9c9pavj")))

(define-public crate-pinyin-parser-0.1.2 (c (n "pinyin-parser") (v "0.1.2") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1z4allyhrks682ja8l0mxifmwy6i1ik93lwqywqa2bz8bpkdigfi")))

(define-public crate-pinyin-parser-0.1.3 (c (n "pinyin-parser") (v "0.1.3") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0x8mh7fy7as3m677wya18dhjvjrczkq5ca1b74145338d8pzanrr")))

(define-public crate-pinyin-parser-0.1.4 (c (n "pinyin-parser") (v "0.1.4") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1pjchxrzmca8i0rxw0dca6ci19anbr1xyfdir465b4c1a2d1z9x9")))

(define-public crate-pinyin-parser-0.1.5 (c (n "pinyin-parser") (v "0.1.5") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1qy36ljr3zk6m6zwrjg9rd5lak67dq56qy6ckagjafdvp61p0qvx")))

(define-public crate-pinyin-parser-0.1.6 (c (n "pinyin-parser") (v "0.1.6") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0wvyyryv25fzfn3z6wiipw54qg9xp62i7843lal76jd3zvgcsgvi")))

(define-public crate-pinyin-parser-0.1.7 (c (n "pinyin-parser") (v "0.1.7") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "08x5gmigi83a2fbhnmqd4p7845wswidspnrx9sb420b9v9mj6fij")))

(define-public crate-pinyin-parser-0.1.8 (c (n "pinyin-parser") (v "0.1.8") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0fyzy9d6bi1nyhxlkn5m527j2xic0163fwvf3db5ffdvlhfh4x7i")))

(define-public crate-pinyin-parser-0.1.9 (c (n "pinyin-parser") (v "0.1.9") (d (list (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0iq57yrv8apmqnj56rn71z10acm2qaf30cmkf7a645mcf1gpmsq8")))

