(define-module (crates-io pi ny pinyin-translator) #:use-module (crates-io))

(define-public crate-pinyin-translator-0.1.0 (c (n "pinyin-translator") (v "0.1.0") (h "153sf67nfs5b0h9dxrv9pdlr4vrahp0agbib77w5cvny17w4m225")))

(define-public crate-pinyin-translator-0.1.1 (c (n "pinyin-translator") (v "0.1.1") (h "03jbblr2in3klimmmb4s0rdcd41iw31lv4dg9yzmicjblwd0wczw")))

(define-public crate-pinyin-translator-0.2.0 (c (n "pinyin-translator") (v "0.2.0") (h "1jf4s7qw6fsjgg79d1k37xi2krynpfr6rabz5frpvavvdhipg7sc")))

(define-public crate-pinyin-translator-0.2.1 (c (n "pinyin-translator") (v "0.2.1") (h "0n3vqm52hnsx4h9p0wrcm4rv76crygfldzplykh4cakx3ssjjhbv")))

(define-public crate-pinyin-translator-0.2.2 (c (n "pinyin-translator") (v "0.2.2") (h "0di5h597lzqd6lp2szn2gvbqnawx72qg2yxssi4vr4zpzcb9zdxr")))

(define-public crate-pinyin-translator-0.2.3 (c (n "pinyin-translator") (v "0.2.3") (d (list (d (n "cbindgen") (r "^0.24.3") (d #t) (k 1)))) (h "1zaj64gk1jyk21f6sc6vhggf2a463hynqkq1y91izrbwlf7lh7b7")))

