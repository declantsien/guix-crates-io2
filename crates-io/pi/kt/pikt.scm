(define-module (crates-io pi kt pikt) #:use-module (crates-io))

(define-public crate-pikt-0.1.0 (c (n "pikt") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pikchr-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0501nc0spj2m65wa5ljq0bk1y538ss7v620kn2lfl8facdm6x8hx")))

