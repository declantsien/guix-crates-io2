(define-module (crates-io pi tc pitch_shift) #:use-module (crates-io))

(define-public crate-pitch_shift-1.0.0 (c (n "pitch_shift") (v "1.0.0") (d (list (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 2)) (d (n "realfft") (r "^3.0.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.0") (d #t) (k 0)))) (h "02xjxiqfq7vhp2hqq7czr2qx1jw1l10r6fz2pjqn033nghanfdgw")))

