(define-module (crates-io pi tc pitch) #:use-module (crates-io))

(define-public crate-pitch-0.1.0 (c (n "pitch") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)))) (h "184w8jg0qx7lridwcg7na6qdhc5bnvqc5nf67wywd20jclf2jpya")))

(define-public crate-pitch-0.1.1 (c (n "pitch") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)))) (h "1mj9nd597nblmqk17hlyny4dw062nglbbhky13xxjvxfyqdh71wi")))

(define-public crate-pitch-0.1.2 (c (n "pitch") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)))) (h "0598bhvkwrrd76b7z166j3wny1bp0rlm7v28s0mw3hl2r51h7axz")))

(define-public crate-pitch-0.1.3 (c (n "pitch") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)))) (h "0imvz8ji51ziw692shz6wgs9mb71cfq2sz0i8lz6bcclrx37mdsc")))

