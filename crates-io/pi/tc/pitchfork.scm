(define-module (crates-io pi tc pitchfork) #:use-module (crates-io))

(define-public crate-pitchfork-0.0.1 (c (n "pitchfork") (v "0.0.1") (d (list (d (n "abscissa_core") (r "^0.3.0") (d #t) (k 0)) (d (n "abscissa_core") (r "^0.3.0") (f (quote ("testing"))) (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "14b05s8pgcjyq73wzn75m5lw28k0pp1ff171k76mxidnjmcnz9qx") (y #t)))

