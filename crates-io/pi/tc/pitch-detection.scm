(define-module (crates-io pi tc pitch-detection) #:use-module (crates-io))

(define-public crate-pitch-detection-0.1.0 (c (n "pitch-detection") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rustfft") (r "^3.0.0") (k 0)))) (h "1zsrzgziwfipvydmy48d08zz10504ipbgcx58hr27zay0694g0s3")))

(define-public crate-pitch-detection-0.2.0 (c (n "pitch-detection") (v "0.2.0") (d (list (d (n "num-complex") (r "^0.3") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rustfft") (r "^4.0.0") (k 0)))) (h "0bq0rb5wfrh4k9jmq0l7zvpaf03cq6y795a0bpxggk3zd7qwrkqg")))

(define-public crate-pitch-detection-0.3.0 (c (n "pitch-detection") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "rustfft") (r "^6.0.1") (k 0)))) (h "14mzvx1kwawv3nab0i3qqbl3xmvwn1psc0f5jxhcsipxzh6yg1p0")))

