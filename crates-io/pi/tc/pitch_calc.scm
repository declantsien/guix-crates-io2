(define-module (crates-io pi tc pitch_calc) #:use-module (crates-io))

(define-public crate-pitch_calc-0.7.0 (c (n "pitch_calc") (v "0.7.0") (h "0gqi3gz1dmnpxp8k7amp1imwbw740ybkrhhxffyqvr99accam8cp")))

(define-public crate-pitch_calc-0.8.0 (c (n "pitch_calc") (v "0.8.0") (h "055nklrbrm4xq3w8ml483nb063an810m4hrs71qb6dr49gvqjsz5")))

(define-public crate-pitch_calc-0.8.1 (c (n "pitch_calc") (v "0.8.1") (d (list (d (n "rustc-serialize") (r "^0.1.1") (d #t) (k 0)))) (h "0vdfj62x2k31g1n8m1sx3x6wr5jmf4y20ap13l4b4811dmqc0mfi")))

(define-public crate-pitch_calc-0.8.2 (c (n "pitch_calc") (v "0.8.2") (d (list (d (n "rustc-serialize") (r "^0.2.10") (d #t) (k 0)))) (h "0vprp0bgwfy8gayqbgcbh9x5v1c69x246x1r6sndj8q8w6mj06qz")))

(define-public crate-pitch_calc-0.8.3 (c (n "pitch_calc") (v "0.8.3") (d (list (d (n "rand") (r "^0.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.12") (d #t) (k 0)))) (h "06nmg8km7x12ahwa5x2qsiwxfp8xkyyfqgridyix7bcpwjqw3pqp")))

(define-public crate-pitch_calc-0.9.0 (c (n "pitch_calc") (v "0.9.0") (d (list (d (n "rand") (r "^0.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.12") (d #t) (k 0)))) (h "1x1rd8r49xqnsk08qb9b83rkrplc971pddhn8kdr3sqj3b7mwnly")))

(define-public crate-pitch_calc-0.9.1 (c (n "pitch_calc") (v "0.9.1") (d (list (d (n "rand") (r "^0.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.12") (d #t) (k 0)))) (h "1zx21jxin6d9i9hv7n7x69yh7hmdqn8rlzzflv99a8mwzmk1sb6l")))

(define-public crate-pitch_calc-0.9.2 (c (n "pitch_calc") (v "0.9.2") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0gag1mljh6k0dsblvlqjx18an6gb9xaw7kp9sd5adgbkqbffpix6")))

(define-public crate-pitch_calc-0.9.3 (c (n "pitch_calc") (v "0.9.3") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "124z1gl6x7r9cc2zkcdgawcgl3dxp7cp5j62rzh19km3qx7r4y08")))

(define-public crate-pitch_calc-0.9.4 (c (n "pitch_calc") (v "0.9.4") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "09flj4109j80m9ds36vc8x800jxd4l50ih52g5zdkrd2z2bjdvwk")))

(define-public crate-pitch_calc-0.9.5 (c (n "pitch_calc") (v "0.9.5") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0imfapvr51axn2qvk6yfa2y7xcw17sla8zw8xr2rzf55ri6cvvnf")))

(define-public crate-pitch_calc-0.9.6 (c (n "pitch_calc") (v "0.9.6") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0drglfb01ndfrqnlppl4c4vq44ziq1lfj4p4ai9k3msb5v6w73rq")))

(define-public crate-pitch_calc-0.9.7 (c (n "pitch_calc") (v "0.9.7") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "09gcx7s7bl4kwhifn9cxbqrcrf42xizz9sl2r2gl55xrgw5is2sr")))

(define-public crate-pitch_calc-0.10.0 (c (n "pitch_calc") (v "0.10.0") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1p2aqgf3rdvk79f5mlcrmnh1mn949l98pkfd6f89pxq6njxwg4hb")))

(define-public crate-pitch_calc-0.11.0 (c (n "pitch_calc") (v "0.11.0") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1hav521fmhzbp6g2qdnwvhnvi0d7m59miakp8fcdqmmb8qhxc5h3") (f (quote (("serde_serialization" "serde" "serde_json"))))))

(define-public crate-pitch_calc-0.11.1 (c (n "pitch_calc") (v "0.11.1") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1gr468scrab0yb9p8ldpdlvxs1n01nqhrri5xypi50z2qm5zx932") (f (quote (("serde_serialization" "serde" "serde_json"))))))

(define-public crate-pitch_calc-0.12.0 (c (n "pitch_calc") (v "0.12.0") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 2)))) (h "1y26bamnwp1yvxbln6pi4xgmsxcviwjkxlzzakwp16cyzzbhaw1q")))

