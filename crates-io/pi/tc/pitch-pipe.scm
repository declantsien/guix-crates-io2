(define-module (crates-io pi tc pitch-pipe) #:use-module (crates-io))

(define-public crate-pitch-pipe-0.1.0 (c (n "pitch-pipe") (v "0.1.0") (d (list (d (n "circular-buffer") (r "^0.1.7") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "one-euro-rs") (r "^0.2.0") (d #t) (k 0)))) (h "17rqa57p6n2082xz63dgn30fggdkpvakil0jjbcf6jq0k9n27z3v")))

(define-public crate-pitch-pipe-0.1.1 (c (n "pitch-pipe") (v "0.1.1") (d (list (d (n "circular-buffer") (r "^0.1.7") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "one-euro-rs") (r "^0.2.0") (d #t) (k 0)))) (h "0cyv6i521y23b81wd3cjv9g54j8v71hh7lqhn6bxmxlzb0k3sz42")))

(define-public crate-pitch-pipe-0.1.2 (c (n "pitch-pipe") (v "0.1.2") (d (list (d (n "circular-buffer") (r "^0.1.7") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "one-euro-rs") (r "^0.2.0") (d #t) (k 0)))) (h "1f5f42k6sf27qq3nzwz3dir741dfnkzv1rbx26gx2aw9axdqygdi")))

