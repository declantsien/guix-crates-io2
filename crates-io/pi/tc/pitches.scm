(define-module (crates-io pi tc pitches) #:use-module (crates-io))

(define-public crate-pitches-0.1.0 (c (n "pitches") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.3.0") (d #t) (k 0)))) (h "1n3nc5gbpd35qyr4habm8sqnvw7jp9r38gcqndyz9ificlsqvlbp")))

