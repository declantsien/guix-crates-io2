(define-module (crates-io pi on pion) #:use-module (crates-io))

(define-public crate-Pion-0.0.0 (c (n "Pion") (v "0.0.0") (h "0q81ngk2xbb4zf61v19h6vwb36g0fkshzrw3zhka1ll2kjzyhd7i") (y #t)))

(define-public crate-Pion-0.0.1 (c (n "Pion") (v "0.0.1") (h "0351y5rkz0qhdi8q33d8knr4xj12a4q63b2ldsc0131zik2yq1dg")))

(define-public crate-Pion-0.0.2 (c (n "Pion") (v "0.0.2") (h "1p52slnljrcaprak0l9wz1a2c3sdn24a3wr1wkzbs23zmiyjrn3x")))

