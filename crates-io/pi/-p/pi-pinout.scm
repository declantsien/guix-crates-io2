(define-module (crates-io pi -p pi-pinout) #:use-module (crates-io))

(define-public crate-pi-pinout-0.1.0 (c (n "pi-pinout") (v "0.1.0") (h "0is9g957jahzfham662pyam9bbfbvfa249yddlgf60viqr5kgnc8")))

(define-public crate-pi-pinout-0.1.1 (c (n "pi-pinout") (v "0.1.1") (h "1mdgnl8b9sy9pkkfs989nvn0ypmx0gc29lfzxcn492yyrar24c25")))

(define-public crate-pi-pinout-0.1.2 (c (n "pi-pinout") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "06ffgjry5kyfzzg6h725nir1zij5my4rar2b16zlbqgvpq8sh10z")))

(define-public crate-pi-pinout-0.1.3 (c (n "pi-pinout") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i3jkb0xga6wyw58afm22vaily5xi7py09h6nv0dwgn7f390sv9k")))

