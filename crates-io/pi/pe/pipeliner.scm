(define-module (crates-io pi pe pipeliner) #:use-module (crates-io))

(define-public crate-pipeliner-0.1.1 (c (n "pipeliner") (v "0.1.1") (h "1m8n4mcapvzm18djdckdwcll48719bx7palkqnd0ky6vgl4vwp66")))

(define-public crate-pipeliner-1.0.0 (c (n "pipeliner") (v "1.0.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)))) (h "01lw333v0bfd1zdq5n3bmcc9h413gg21z5yaff99f634si2wppi4")))

(define-public crate-pipeliner-1.0.1 (c (n "pipeliner") (v "1.0.1") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)))) (h "0xpym8m88zkfcjbsncf6pqx4p0zsysm734rc8d1i1c4rnignak2g")))

