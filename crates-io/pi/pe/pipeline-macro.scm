(define-module (crates-io pi pe pipeline-macro) #:use-module (crates-io))

(define-public crate-pipeline-macro-0.1.0 (c (n "pipeline-macro") (v "0.1.0") (h "0afd2mihvf9bzrvwfasv3hzxxzgijcllfc2fdihp45wnp4b1ikam")))

(define-public crate-pipeline-macro-0.1.1 (c (n "pipeline-macro") (v "0.1.1") (h "0cqm6s325gwwnmgvf3nsaicr0snzg39ywk32vlk438nga7m6ci29")))

(define-public crate-pipeline-macro-0.1.2 (c (n "pipeline-macro") (v "0.1.2") (h "0ych52abw9rvkzrqi3hlv2wrvrrwh9prfrg3ksi9lmwrpdii6g6l")))

(define-public crate-pipeline-macro-0.1.3 (c (n "pipeline-macro") (v "0.1.3") (h "1zb8p1917j0ghm01jm2la0wpw6k4aqg6am08j0cb9mdbv8aagk1x")))

