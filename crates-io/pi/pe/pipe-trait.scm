(define-module (crates-io pi pe pipe-trait) #:use-module (crates-io))

(define-public crate-pipe-trait-0.1.0 (c (n "pipe-trait") (v "0.1.0") (h "006sk5jhnhyl2w41v25q7ilazj9a583jak357b27b1cizmf5ahwq")))

(define-public crate-pipe-trait-0.1.1 (c (n "pipe-trait") (v "0.1.1") (h "1nxijfr6pj8xjhaj1pk4jcxxzh73ibpfqs9ks2pi26jl3cnmxmbc")))

(define-public crate-pipe-trait-0.1.2 (c (n "pipe-trait") (v "0.1.2") (h "1nijg048mf8yjv7qgg6wq7lyb5hmiwfzp846wzkhj93zmzanpi6x")))

(define-public crate-pipe-trait-0.1.3 (c (n "pipe-trait") (v "0.1.3") (h "05adiqlh92mzc55xkrm17r27ha39nnm027m9pg07pajz8pjgg0ns")))

(define-public crate-pipe-trait-0.1.5 (c (n "pipe-trait") (v "0.1.5") (h "1z9pb0xmif34aww7zgv2ss72wp1cl6l4mk1i5s18rn0kraf7ffwg")))

(define-public crate-pipe-trait-0.1.6 (c (n "pipe-trait") (v "0.1.6") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)))) (h "0c5zpi19j76gaq8ypjs6rr89j0jbjyfr91s7rvk865bzwjhbgfjf")))

(define-public crate-pipe-trait-0.1.7 (c (n "pipe-trait") (v "0.1.7") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)))) (h "15076br4p9j08j3kijal3aqh39k9gms2wvrmqralybm5w6rpza5f")))

(define-public crate-pipe-trait-0.1.8 (c (n "pipe-trait") (v "0.1.8") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)))) (h "1fpp34pd4ci52g0x45i89rn1xw8v4li8nydj6vzirsxxvf5q84f4")))

(define-public crate-pipe-trait-0.1.9 (c (n "pipe-trait") (v "0.1.9") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)))) (h "0s4xrl3p5vf28d1l0ldjzs2q5xc5yznip8iv4wgaf5i5d65rs0iv")))

(define-public crate-pipe-trait-0.1.10 (c (n "pipe-trait") (v "0.1.10") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)))) (h "03zdkjfqwjasiymhxi4mzb3bk5icvvijmrhp48gdcmjfr3gagn3c")))

(define-public crate-pipe-trait-0.1.11 (c (n "pipe-trait") (v "0.1.11") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)))) (h "0nn3bjq8mk9210w44yrxv0cjamq5anskmixkih8pkn7m0f4hcp1n")))

(define-public crate-pipe-trait-0.2.0 (c (n "pipe-trait") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)))) (h "1g3g89khm7679ff62vyvy9my6m25vg2ckkfqk027a9cbs88w210x")))

(define-public crate-pipe-trait-0.2.1 (c (n "pipe-trait") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.7") (d #t) (k 2)))) (h "1q0c0y8al01ca3qi9jw05fg1ppy15fanc1z6wg0gspls6g73ck3x")))

(define-public crate-pipe-trait-0.3.0 (c (n "pipe-trait") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 2)))) (h "1f42q8hhp4fqznlcy0cqsvfz9n8930p62krxips7nz8bfpc62ddc")))

(define-public crate-pipe-trait-0.3.1 (c (n "pipe-trait") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 2)))) (h "112kkmldaxa65yn72ldazgpfyqj17bgp3dg7gh6ij2bvrvqlyy8z")))

(define-public crate-pipe-trait-0.3.2 (c (n "pipe-trait") (v "0.3.2") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 2)))) (h "1cx5vv9sg8iv1d9c8lnicw5v9bv44ffy0ckk1dm28k77c5ajzn9z")))

(define-public crate-pipe-trait-0.3.3 (c (n "pipe-trait") (v "0.3.3") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)))) (h "14xzhwzw6ggc02q3hmw9y500wmj7xky29wqyzl5i15m2qj1awpi5") (y #t)))

(define-public crate-pipe-trait-0.4.0 (c (n "pipe-trait") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)))) (h "1ql988sa4appx60q0wfmn1d6i2qrk7vadvw4zsp600wzwp4ixgn1")))

