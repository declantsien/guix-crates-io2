(define-module (crates-io pi pe pipelines) #:use-module (crates-io))

(define-public crate-pipelines-0.1.0 (c (n "pipelines") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 2)) (d (n "flate2") (r "^0.2.19") (d #t) (k 2)) (d (n "humansize") (r "^1.0.1") (d #t) (k 2)) (d (n "log") (r "^0.3.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.0") (d #t) (k 2)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 2)))) (h "1dhbr8j21hrg8lmpxf3r7ck9p5hmlbsi6a85s1j9n15h4p5yx4j4")))

(define-public crate-pipelines-0.4.0 (c (n "pipelines") (v "0.4.0") (d (list (d (n "chan") (r "^0.1.19") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 2)) (d (n "flate2") (r "^0.2.19") (d #t) (k 2)) (d (n "humansize") (r "^1.0.1") (d #t) (k 2)) (d (n "log") (r "^0.3.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.0") (d #t) (k 2)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 2)))) (h "0rihaqjf3xb884vqs0hkrkmn41n0i8a5am0lsh5dqf6ar9gd1795") (f (quote (("default" "chan"))))))

