(define-module (crates-io pi pe pipeline-script) #:use-module (crates-io))

(define-public crate-pipeline-script-0.1.0 (c (n "pipeline-script") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.17") (d #t) (k 0)) (d (n "ssh-rs") (r "^0.5.0") (f (quote ("scp"))) (d #t) (k 0)))) (h "1l8cdv5k5y6pxprsc4fhyhxhqfrbwwlgqi4372qdh1am6fainbvb")))

(define-public crate-pipeline-script-0.2.0 (c (n "pipeline-script") (v "0.2.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "scanner-rust") (r "^2.0.17") (d #t) (k 0)) (d (n "ssh-rs") (r "^0.5.0") (f (quote ("scp"))) (d #t) (k 0)))) (h "03zysykjiz3lln52ry5mxrqwx1bizq87831fzqp5hdvilhgrv6g8")))

