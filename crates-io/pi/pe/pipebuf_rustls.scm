(define-module (crates-io pi pe pipebuf_rustls) #:use-module (crates-io))

(define-public crate-pipebuf_rustls-0.21.0 (c (n "pipebuf_rustls") (v "0.21.0") (d (list (d (n "pipebuf") (r "^0.2") (d #t) (k 0)) (d (n "rustls") (r "^0.21.1") (d #t) (k 0)))) (h "1lmxicjgqfvc1vv90bnrwkasx1rwir02c5ajnkwii8zq4fgb6i6c")))

(define-public crate-pipebuf_rustls-0.23.0 (c (n "pipebuf_rustls") (v "0.23.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pipebuf") (r "^0.3.1") (d #t) (k 0)) (d (n "pprof") (r "^0.13") (f (quote ("criterion" "flamegraph"))) (d #t) (k 2)) (d (n "rustls") (r "^0.23.4") (k 0)) (d (n "rustls") (r "^0.23.4") (f (quote ("std" "ring"))) (k 2)) (d (n "rustls-pemfile") (r "^2.1.2") (d #t) (k 2)))) (h "02ir6c602yc91by66s1j6zym5xsn59ds6pi9y95bb63znqv8brca") (f (quote (("unbuffered") ("default" "buffered") ("buffered" "rustls/std"))))))

