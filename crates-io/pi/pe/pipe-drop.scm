(define-module (crates-io pi pe pipe-drop) #:use-module (crates-io))

(define-public crate-pipe-drop-0.0.0 (c (n "pipe-drop") (v "0.0.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1i433xybns93c72zaixrk9xyzv82vx0h2mnrg549nvh51065axq3") (y #t)))

