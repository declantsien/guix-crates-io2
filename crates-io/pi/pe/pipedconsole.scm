(define-module (crates-io pi pe pipedconsole) #:use-module (crates-io))

(define-public crate-pipedconsole-0.0.1 (c (n "pipedconsole") (v "0.0.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "std" "errhandlingapi" "processthreadsapi" "winbase" "minwinbase" "handleapi" "namedpipeapi" "consoleapi" "processenv" "fileapi"))) (d #t) (k 0)))) (h "0fy27rq8viiv5dmbs4zhdfahdqp91hrrx7vcpdhwzww6i6savvh6") (y #t)))

(define-public crate-pipedconsole-0.0.2 (c (n "pipedconsole") (v "0.0.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "std" "errhandlingapi" "processthreadsapi" "winbase" "minwinbase" "handleapi" "namedpipeapi" "consoleapi" "processenv" "fileapi"))) (d #t) (k 0)))) (h "116vr210i7m5zzlwvcxqjbnrsl1j6wdhbhflry5dkv46slcpp77l") (y #t)))

(define-public crate-pipedconsole-0.0.4 (c (n "pipedconsole") (v "0.0.4") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "std" "errhandlingapi" "processthreadsapi" "winbase" "minwinbase" "handleapi" "namedpipeapi" "consoleapi" "processenv" "fileapi"))) (d #t) (k 0)))) (h "1j3k9gb76xk6b37ix3mqnza7p4svc3fr709yj6bg3fi2zwf76hz9") (y #t)))

(define-public crate-pipedconsole-0.1.0 (c (n "pipedconsole") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "std" "errhandlingapi" "processthreadsapi" "winbase" "minwinbase" "handleapi" "namedpipeapi" "consoleapi" "processenv" "fileapi"))) (d #t) (k 0)))) (h "1mzv99b6hy0zmy2ilgrhjl11gig9lvljw7pca03lb03phf058bak") (y #t)))

(define-public crate-pipedconsole-0.1.2 (c (n "pipedconsole") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "std" "errhandlingapi" "processthreadsapi" "winbase" "minwinbase" "handleapi" "namedpipeapi" "consoleapi" "processenv" "fileapi"))) (d #t) (k 0)))) (h "0ca5fp6rx3dh8mj8qfbagc0vy1dzndqpsl73wq1nqvsiwnnnanhw") (y #t)))

(define-public crate-pipedconsole-0.1.3 (c (n "pipedconsole") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "std" "errhandlingapi" "processthreadsapi" "winbase" "minwinbase" "handleapi" "namedpipeapi" "consoleapi" "processenv" "fileapi"))) (d #t) (k 0)))) (h "107prmfm85y26f2ldvwb56fmlnviwjpjf1m1sdyw0kr065nsdpr7")))

(define-public crate-pipedconsole-0.1.4 (c (n "pipedconsole") (v "0.1.4") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "std" "errhandlingapi" "processthreadsapi" "winbase" "minwinbase" "handleapi" "namedpipeapi" "consoleapi" "processenv" "fileapi"))) (d #t) (k 0)))) (h "0szsri7dxjv8isc0hydjjv1rzbz174jyp7aqbrhs7rp9954slva5")))

(define-public crate-pipedconsole-0.2.0 (c (n "pipedconsole") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "std" "errhandlingapi" "processthreadsapi" "winbase" "minwinbase" "handleapi" "namedpipeapi" "consoleapi" "processenv" "fileapi"))) (d #t) (k 0)))) (h "0pxyl4phjrpbyxrwrps1k1fi0b7fjd4r6kd7dj8ppqqbhqxibfrz")))

(define-public crate-pipedconsole-0.2.3 (c (n "pipedconsole") (v "0.2.3") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "std" "errhandlingapi" "processthreadsapi" "winbase" "minwinbase" "handleapi" "namedpipeapi" "consoleapi" "processenv" "fileapi"))) (d #t) (k 0)))) (h "0m6y7768hwf24i73w7k462g7lpjwg0gkjs8m9s8i22zn38hx0ba7")))

(define-public crate-pipedconsole-0.3.0 (c (n "pipedconsole") (v "0.3.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "std" "errhandlingapi" "processthreadsapi" "winbase" "minwinbase" "handleapi" "namedpipeapi" "consoleapi" "processenv" "fileapi"))) (d #t) (k 0)))) (h "0nscp21v83zm5px90a2px8y2bpg5833ikqda9k37iw5fbx2ilrj4")))

(define-public crate-pipedconsole-0.3.1 (c (n "pipedconsole") (v "0.3.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "std" "errhandlingapi" "processthreadsapi" "winbase" "minwinbase" "handleapi" "namedpipeapi" "consoleapi" "processenv" "fileapi"))) (d #t) (k 0)))) (h "0lr164gfzpv3psjcnv8l308vq8nqk7l8hq14lgp9xrsqzn9lg3ps")))

(define-public crate-pipedconsole-0.3.2 (c (n "pipedconsole") (v "0.3.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "std" "errhandlingapi" "processthreadsapi" "winbase" "minwinbase" "handleapi" "namedpipeapi" "consoleapi" "processenv" "fileapi"))) (d #t) (k 0)))) (h "0yyjag3n1mlxw58n14nlzmaa19dvwvf51fwfak980a55r4rj06k4")))

