(define-module (crates-io pi pe pipe_udp_server) #:use-module (crates-io))

(define-public crate-pipe_udp_server-0.7.1 (c (n "pipe_udp_server") (v "0.7.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "sha256") (r "^1.1.1") (d #t) (k 0)) (d (n "sntpc") (r "^0.3.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.22") (f (quote ("rt" "macros" "net"))) (k 0)))) (h "12xrwlzrhzn7s559lb74d075qhib610k0kvh5kyywqvlp6f1n927")))

