(define-module (crates-io pi pe pipers) #:use-module (crates-io))

(define-public crate-pipers-1.0.0 (c (n "pipers") (v "1.0.0") (h "156ifkpizknrp9x08a1zgf6f8dxx5adg5czin96zlyrz0jjklaam") (y #t)))

(define-public crate-pipers-1.0.1 (c (n "pipers") (v "1.0.1") (h "09nc0i9v0rpvip9jrwkszakf9c10qy6lyr14sr0p4xl44ppc8pzf") (y #t)))

