(define-module (crates-io pi pe pipeawesome2) #:use-module (crates-io))

(define-public crate-pipeawesome2-0.1.3 (c (n "pipeawesome2") (v "0.1.3") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "graphviz-rust") (r "^0.1.2") (d #t) (k 0)) (d (n "peg") (r "^0.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1iqz5pq1j4ci1lix0p43k72ysws2grwz186p92xpmhv4g6bpx2xb")))

