(define-module (crates-io pi pe pipebuf_websocket) #:use-module (crates-io))

(define-public crate-pipebuf_websocket-0.1.0 (c (n "pipebuf_websocket") (v "0.1.0") (d (list (d (n "embedded-websocket") (r "^0.8") (d #t) (k 0)) (d (n "httparse") (r "^1.4") (k 0)) (d (n "pipebuf") (r "^0.2") (d #t) (k 0)))) (h "10gnqgif6i4p4vgm82s2zvkh0mgsrpqzn8fh1mn9a4c45kyy9x2g")))

(define-public crate-pipebuf_websocket-0.1.1 (c (n "pipebuf_websocket") (v "0.1.1") (d (list (d (n "embedded-websocket") (r "^0.8") (d #t) (k 0)) (d (n "httparse") (r "^1.4") (k 0)) (d (n "pipebuf") (r "^0.2") (d #t) (k 0)))) (h "05yfd1b6d8cbzsbbp4jwm0sb9i00da6aqqpxgrkmbsgb0j8p7kbp")))

(define-public crate-pipebuf_websocket-0.2.0 (c (n "pipebuf_websocket") (v "0.2.0") (d (list (d (n "embedded-websocket") (r "^0.8") (d #t) (k 0)) (d (n "httparse") (r "^1.4") (k 0)) (d (n "pipebuf") (r "^0.3") (d #t) (k 0)))) (h "1j016f9ch0wlhb6g1crchi4mv03f1q6qfp77hpjizwlxa0z007a0")))

