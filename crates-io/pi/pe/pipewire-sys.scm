(define-module (crates-io pi pe pipewire-sys) #:use-module (crates-io))

(define-public crate-pipewire-sys-0.1.0 (c (n "pipewire-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)))) (h "0hn2ilfnj6wfwwxmc3x246bvnm0r1apxrb20kf2jvj9jwd1kqda3")))

(define-public crate-pipewire-sys-0.2.1 (c (n "pipewire-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "spa_sys") (r "^0") (d #t) (k 0) (p "libspa-sys")) (d (n "system-deps") (r "^2.0") (d #t) (k 1)))) (h "1ns6rvmlc7xk7l07jxjkmidw1yy9p7s8qcif8kkrcl98bpcdjfrd") (l "pipewire-0.3")))

(define-public crate-pipewire-sys-0.2.2 (c (n "pipewire-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "spa_sys") (r "^0") (d #t) (k 0) (p "libspa-sys")) (d (n "system-deps") (r "^3.0") (d #t) (k 1)))) (h "0a6j607wcdn9l1w650w3b8a8gh9wvskwirfn2rhk2psa5h74hdwj") (l "pipewire-0.3")))

(define-public crate-pipewire-sys-0.3.0 (c (n "pipewire-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "spa_sys") (r "^0") (d #t) (k 0) (p "libspa-sys")) (d (n "system-deps") (r "^3.0") (d #t) (k 1)))) (h "0d91bqyi6n1xijrq65sriyil01k1bgwyf9ap8h6ficfd5p16zz1s") (l "pipewire-0.3")))

(define-public crate-pipewire-sys-0.4.0 (c (n "pipewire-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "spa_sys") (r "^0") (d #t) (k 0) (p "libspa-sys")) (d (n "system-deps") (r "^3.0") (d #t) (k 1)))) (h "0nbhp27gv23w5c9znw7dwgvfgn0vdwjqyfp85iw0qn9z461wvals") (l "pipewire-0.3")))

(define-public crate-pipewire-sys-0.4.1 (c (n "pipewire-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "spa_sys") (r "^0") (d #t) (k 0) (p "libspa-sys")) (d (n "system-deps") (r "^3.0") (d #t) (k 1)))) (h "0jdr3vdj1phch69wd5l7kqjl3dbbpmlny41mngdzgziskzpsajlv") (l "pipewire-0.3")))

(define-public crate-pipewire-sys-0.5.0 (c (n "pipewire-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "spa_sys") (r "^0") (d #t) (k 0) (p "libspa-sys")) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "1r0fh7yazisxv4iqgbrghy6mgsa9rj91hv84hqbv1lqzx9i44v9z") (l "pipewire-0.3")))

(define-public crate-pipewire-sys-0.6.0 (c (n "pipewire-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.64") (k 1)) (d (n "spa_sys") (r "^0") (d #t) (k 0) (p "libspa-sys")) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "08r19mahd0cjg23z5pnvph599syxs0f2dh7x48wsmdkzvgp90lm9") (l "pipewire-0.3")))

(define-public crate-pipewire-sys-0.7.0 (c (n "pipewire-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.66") (k 1)) (d (n "spa_sys") (r "^0") (d #t) (k 0) (p "libspa-sys")) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "018lky6r2bckg74qp7hab9wn8cqq14l9gwslrq2bz2akh6rr0ryn") (l "pipewire-0.3")))

(define-public crate-pipewire-sys-0.7.1 (c (n "pipewire-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.66") (f (quote ("runtime"))) (k 1)) (d (n "spa_sys") (r "^0") (d #t) (k 0) (p "libspa-sys")) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "025kva7y0bil6f3cd2k25j61n9xmfihcpdp0qwrar5szzv7y4n6m") (l "pipewire-0.3")))

(define-public crate-pipewire-sys-0.7.2 (c (n "pipewire-sys") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.66") (f (quote ("runtime"))) (k 1)) (d (n "spa_sys") (r "^0") (d #t) (k 0) (p "libspa-sys")) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "0r4z0farzflycgfp6x7z65h57np4l1qnpj4r8z5lcwkkgd70h349") (l "pipewire-0.3")))

(define-public crate-pipewire-sys-0.8.0 (c (n "pipewire-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime"))) (k 1)) (d (n "spa_sys") (r "^0.8") (d #t) (k 0) (p "libspa-sys")) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "04hiy3rl8v3j2dfzp04gr7r8l5azzqqsvqdzwa7sipdij27ii7l4") (l "pipewire-0.3")))

