(define-module (crates-io pi pe pipe_serve) #:use-module (crates-io))

(define-public crate-pipe_serve-0.4.2 (c (n "pipe_serve") (v "0.4.2") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.22") (f (quote ("rt" "macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1mmslm57k8203l081qkbxfm5g32m075gq2x2qyvk3npw6b30c7n7")))

(define-public crate-pipe_serve-0.4.3 (c (n "pipe_serve") (v "0.4.3") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.22") (f (quote ("rt" "macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1yf59snxjmwd2q74lx5gghw2cydg43ixkzi7n3ipabs48pihvrbh")))

