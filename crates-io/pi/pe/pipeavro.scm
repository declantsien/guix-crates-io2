(define-module (crates-io pi pe pipeavro) #:use-module (crates-io))

(define-public crate-pipeavro-0.1.0 (c (n "pipeavro") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "avro-rs") (r "^0.13.0") (f (quote ("snappy"))) (d #t) (k 0)) (d (n "pipebase") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16ccgzhpylndcqqkk2hzz5pcq2narg0r5k6ssvgnwr0hn6d695g6")))

(define-public crate-pipeavro-0.1.1 (c (n "pipeavro") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "avro-rs") (r "^0.13.0") (f (quote ("snappy"))) (d #t) (k 0)) (d (n "pipebase") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hzjca65baznjf8pknhlhkk526akdz3pl1vg6pbafzgq9qvc44w4")))

(define-public crate-pipeavro-0.1.2 (c (n "pipeavro") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "avro-rs") (r "^0.13.0") (f (quote ("snappy"))) (d #t) (k 0)) (d (n "pipebase") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hlxbwz9cz1gz0iy6z5lfy5w2hzxrfqhi80lcbdgpd8kcrls0b2q")))

(define-public crate-pipeavro-0.1.3 (c (n "pipeavro") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "avro-rs") (r "^0.13.0") (f (quote ("snappy"))) (d #t) (k 0)) (d (n "pipebase") (r "^0.1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18fbcxvr6wi613jqi0ypl6zn56d1fyk8mirbc5q60biizcm3fyjn")))

(define-public crate-pipeavro-0.1.4 (c (n "pipeavro") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "avro-rs") (r "^0.13.0") (f (quote ("snappy"))) (d #t) (k 0)) (d (n "pipebase") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y6dhma1j6xzi8a59lszcxygrmwfwz71saq5b77bi08qp5akqgfs")))

