(define-module (crates-io pi pe pipecsv) #:use-module (crates-io))

(define-public crate-pipecsv-0.1.0 (c (n "pipecsv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pipebase") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nzhnfinf3d9f2a8laa56qmqvs6mjl5cirgqi5wc4fw1j0awi61b")))

(define-public crate-pipecsv-0.1.1 (c (n "pipecsv") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pipebase") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a33k04x47503i8i8llpdvn7qglrzri3rzb60p7fkiymbc9j3yly")))

(define-public crate-pipecsv-0.1.2 (c (n "pipecsv") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pipebase") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10x19wv3fbd9wxr8idyif7399cklki8lycg04qgr5x099ap6vh9g")))

(define-public crate-pipecsv-0.1.3 (c (n "pipecsv") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pipebase") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10kwfkxn36qqvsy7fs2lk6pbsfzq5zs19ww1h1as0md8iylxlky7")))

(define-public crate-pipecsv-0.1.4 (c (n "pipecsv") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "pipebase") (r "^0.1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "12v6njxi2b92js057ha4l24f0sl2p2j4dc8rw8a7b0q2s6nq1p1b")))

(define-public crate-pipecsv-0.1.5 (c (n "pipecsv") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "pipebase") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "030ini2hdp85aii9c2ajafifi9fvss2lpmmi5piy02h33dsh7v7m")))

