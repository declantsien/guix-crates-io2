(define-module (crates-io pi pe pipeline_for_networking) #:use-module (crates-io))

(define-public crate-pipeline_for_networking-0.1.0 (c (n "pipeline_for_networking") (v "0.1.0") (h "1ss7fx3mfwy3ipzklk84kwqpl6k7z7hi1vfk3n0zdabr20h1xfz1")))

(define-public crate-pipeline_for_networking-0.1.1 (c (n "pipeline_for_networking") (v "0.1.1") (h "1drk85im74mirv2himccd0hy6ggbzqzrdryymarxbpb85sdw93mn")))

