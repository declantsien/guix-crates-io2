(define-module (crates-io pi pe pipetee) #:use-module (crates-io))

(define-public crate-pipetee-1.0.0 (c (n "pipetee") (v "1.0.0") (h "1qyi9hfpnii0ndcrihwkpigixascwj3rhnpzpy54f26pzri5wbi5")))

(define-public crate-pipetee-1.0.1 (c (n "pipetee") (v "1.0.1") (h "0ggibakq6hn7xancv17wyz6y6wyh4vafz4b1swqqrv4d2x0glyb9") (r "1.65")))

