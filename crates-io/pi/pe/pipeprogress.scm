(define-module (crates-io pi pe pipeprogress) #:use-module (crates-io))

(define-public crate-pipeprogress-2020.6.0 (c (n "pipeprogress") (v "2020.6.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.14.2") (d #t) (k 0)))) (h "1z18kcv52m402iynrq6m09979fzw2c88wwdlfkx9v2g3708d6nfn")))

(define-public crate-pipeprogress-2020.6.1 (c (n "pipeprogress") (v "2020.6.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.14.2") (d #t) (k 0)))) (h "031pf5gr0qy576kawk4sv4i771z800yb1fzj67s5grpp86mbfzjq")))

(define-public crate-pipeprogress-2020.6.4 (c (n "pipeprogress") (v "2020.6.4") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.14.2") (d #t) (k 0)))) (h "0gqmi5h61wqj7d2c9hiz1mcy6hakpw9m9mykr43a0lg36ikffm5v")))

(define-public crate-pipeprogress-2023.3.0 (c (n "pipeprogress") (v "2023.3.0") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "1hh0x6alq4dr1aj831n1piarra96gjca9gjg1rj4g8c4xbvjsp21")))

