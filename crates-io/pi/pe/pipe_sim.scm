(define-module (crates-io pi pe pipe_sim) #:use-module (crates-io))

(define-public crate-pipe_sim-0.1.0 (c (n "pipe_sim") (v "0.1.0") (h "1wsqhwrp4fn90yd3qzvialh2ly180kry2napxhblkjl8q3g51y0w")))

(define-public crate-pipe_sim-0.1.1 (c (n "pipe_sim") (v "0.1.1") (h "00w8xcydpp7vdf7szipkvl56i98ij7i80hphs128rdz5827gsrh5")))

(define-public crate-pipe_sim-0.1.2 (c (n "pipe_sim") (v "0.1.2") (h "0nxxl683wwb0lv712c1lw77xfdrpncykfs1ssjns7438l2w21zr6")))

(define-public crate-pipe_sim-0.1.3 (c (n "pipe_sim") (v "0.1.3") (h "0fyvikk3jzifqk8qxxv2c430y4zyhnkcjpmnf5xk0kpnarw6jj43")))

(define-public crate-pipe_sim-0.2.1 (c (n "pipe_sim") (v "0.2.1") (h "1m0w5sqwh3lh98kcz0xdb9i89hdv1d1l1g9kbpm62n6l4knmgb32")))

(define-public crate-pipe_sim-0.2.2 (c (n "pipe_sim") (v "0.2.2") (h "06089rnxxn46bi97amnxrjii6xwl56sicm1aifvkjbjixgb8d7m3")))

(define-public crate-pipe_sim-0.2.3 (c (n "pipe_sim") (v "0.2.3") (h "0i7igck78amhpl03y3j363hqay2ki367za85isif68m3xz977nv2")))

