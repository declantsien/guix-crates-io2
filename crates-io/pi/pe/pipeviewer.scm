(define-module (crates-io pi pe pipeviewer) #:use-module (crates-io))

(define-public crate-pipeviewer-1.0.0 (c (n "pipeviewer") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.14.2") (d #t) (k 0)))) (h "15dkq38alcc2rgp4hhszz84i7yjknv12zfb7zcpq3l7g3jvyr1c8")))

(define-public crate-pipeviewer-1.0.1 (c (n "pipeviewer") (v "1.0.1") (d (list (d (n "clap") (r "^3.2.5") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "1158xzalla6l0hhk43zbd0bx4sffxvf3ykammxs65r3s7qn6fjya")))

