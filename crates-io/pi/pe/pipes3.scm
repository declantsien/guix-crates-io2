(define-module (crates-io pi pe pipes3) #:use-module (crates-io))

(define-public crate-pipes3-0.1.5 (c (n "pipes3") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "pipebase") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "s3") (r "^0.4.0") (d #t) (k 0) (p "aws-sdk-s3")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1rzchw9a5fyd9w0k32kw2dv3vhwgnfbvq8ic60fm6aijbrb9j12h")))

