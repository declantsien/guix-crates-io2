(define-module (crates-io pi pe pipe_mock_serve) #:use-module (crates-io))

(define-public crate-pipe_mock_serve-0.7.1 (c (n "pipe_mock_serve") (v "0.7.1") (d (list (d (n "actix-multipart") (r "^0.6.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "async-stream") (r "^0.3.4") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.27") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.27") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1lc4gckv3fblgx21lhca9fzbawd5b5qsnbrxn42ghdfkgjp5yycj")))

