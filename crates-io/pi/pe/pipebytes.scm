(define-module (crates-io pi pe pipebytes) #:use-module (crates-io))

(define-public crate-pipebytes-0.1.0 (c (n "pipebytes") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "pipebase") (r "^0.1.0") (d #t) (k 0)))) (h "1q2v6fghqa9ssbfsakkas157rynngrlmqvyrqv823kq797xkfi25")))

(define-public crate-pipebytes-0.1.1 (c (n "pipebytes") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "pipebase") (r "^0.1.1") (d #t) (k 0)))) (h "095pqzm4fgvix1j6q8gy3kz5yhlnp6lpqhglymwkm31kyadsdvky")))

(define-public crate-pipebytes-0.1.2 (c (n "pipebytes") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "pipebase") (r "^0.1.2") (d #t) (k 0)))) (h "1c67ixsfi69rq7slk1yx38v0ypdbp3d0q1zvxc5nrgcgsdwjp3qp")))

(define-public crate-pipebytes-0.1.3 (c (n "pipebytes") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "pipebase") (r "^0.1.2") (d #t) (k 0)))) (h "1gczw4qhrlmr0wkbpmq9a9cndzwj5yf3q61j2b44vmgd82fnvjn4")))

(define-public crate-pipebytes-0.1.4 (c (n "pipebytes") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "pipebase") (r "^0.1.9") (d #t) (k 0)))) (h "0pk8i8285a95hlhifjr7wsfr4d8kas54x2acyx75kc4alb295h8a")))

(define-public crate-pipebytes-0.1.5 (c (n "pipebytes") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "pipebase") (r "^0.2.0") (d #t) (k 0)))) (h "11mwx53q0r1q8ana68zkhxsph03yvwgkn1wzbqqramyy6kq8gswl")))

