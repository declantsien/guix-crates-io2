(define-module (crates-io pi pe pipesqs) #:use-module (crates-io))

(define-public crate-pipesqs-0.1.5 (c (n "pipesqs") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "pipebase") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqs") (r "^0.4.0") (d #t) (k 0) (p "aws-sdk-sqs")) (d (n "tokio") (r "^1.6.1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "13bikmgib1cbz24j7dkw0m9qk2yppihgwaz06vxhg8g5q5vfv2sh")))

