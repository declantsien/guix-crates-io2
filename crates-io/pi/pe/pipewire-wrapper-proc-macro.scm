(define-module (crates-io pi pe pipewire-wrapper-proc-macro) #:use-module (crates-io))

(define-public crate-pipewire-wrapper-proc-macro-0.1.0 (c (n "pipewire-wrapper-proc-macro") (v "0.1.0") (d (list (d (n "pipewire-wrapper-macro-impl") (r "^0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)))) (h "00qqibgj4piscrv7a2qw9w5zl1lq0nknh4ghy6rp1xhcn4aak9h2")))

(define-public crate-pipewire-wrapper-proc-macro-0.1.1 (c (n "pipewire-wrapper-proc-macro") (v "0.1.1") (d (list (d (n "pipewire-wrapper-macro-impl") (r "^0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)))) (h "1fnksbxmsr2qhf2ry9s7wcns0rrn997s2zdrvdicxh2ysigf2q16")))

(define-public crate-pipewire-wrapper-proc-macro-0.1.2 (c (n "pipewire-wrapper-proc-macro") (v "0.1.2") (d (list (d (n "pipewire-wrapper-macro-impl") (r "^0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)))) (h "0k50fkpj7k3lyp4xw9b66q66mlg05g5k79r2ij4qp28v3j2c9hk6")))

