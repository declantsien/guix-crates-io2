(define-module (crates-io pi pe pipebased) #:use-module (crates-io))

(define-public crate-pipebased-0.1.0 (c (n "pipebased") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "pipebased_common") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "17m9x5z6zgqg1wlfv7wn427cic07vkbvny0za2rnvf46svi2azxg")))

