(define-module (crates-io pi pe pipewire-wrapper-macro-impl) #:use-module (crates-io))

(define-public crate-pipewire-wrapper-macro-impl-0.1.0 (c (n "pipewire-wrapper-macro-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1ckalxnqfbakv3zfwns319wj3w4pk6pbkdngwj47zqaj1v0ihlwx")))

(define-public crate-pipewire-wrapper-macro-impl-0.1.1 (c (n "pipewire-wrapper-macro-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "04q2sgv8xpryv3d6pg34bmr0qdx0mya15b6sbn1y62krfhqms7qv")))

(define-public crate-pipewire-wrapper-macro-impl-0.1.2 (c (n "pipewire-wrapper-macro-impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0ib9zczim2gd88pbywnw4fyn2a6dcm1xydjkjygc5drdarvnfas8")))

