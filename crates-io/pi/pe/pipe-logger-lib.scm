(define-module (crates-io pi pe pipe-logger-lib) #:use-module (crates-io))

(define-public crate-pipe-logger-lib-1.0.0 (c (n "pipe-logger-lib") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "0pnvlamz6cfjgf013cdxalsg6sn33kf9yzl6g5gyll6443jddmd5")))

(define-public crate-pipe-logger-lib-1.0.1 (c (n "pipe-logger-lib") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "0vh9y1x7802wsvf535lr626qz2acdlm9ffw18z9mfk5wqr5x4n8x")))

(define-public crate-pipe-logger-lib-1.0.2 (c (n "pipe-logger-lib") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "03q84aslwsszrjqwm9gz6jizk8ww0cf0k8lyn751zb0c4n5j8mz6")))

(define-public crate-pipe-logger-lib-1.0.3 (c (n "pipe-logger-lib") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "16l7bflh6h2bah4vnwfj9v07frrx4dm1577ag9nfybqrmy9zdx1n")))

(define-public crate-pipe-logger-lib-1.1.0 (c (n "pipe-logger-lib") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "07yy6hgv59szh0n55q8zfnxanq1vx8lkf8k3c79rziw1319pazqn")))

(define-public crate-pipe-logger-lib-1.1.1 (c (n "pipe-logger-lib") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "14sk134p18a9n3fzjzkglzgd10nm0gbdxc0dqa595v6bfbi102sh")))

(define-public crate-pipe-logger-lib-1.1.2 (c (n "pipe-logger-lib") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "1mqjj0ybb0n411sr0szsr706pqykr1l2hq8c005ry1gpimisgnb0")))

(define-public crate-pipe-logger-lib-1.1.3 (c (n "pipe-logger-lib") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "1qbmdskal1dhk5w6ac2xby944m4vn09320js8x8lh2bjwszvqb61")))

(define-public crate-pipe-logger-lib-1.1.4 (c (n "pipe-logger-lib") (v "1.1.4") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "0x6g8f6pnln8my77mfhm6nnnqqv3c8d57vnfs8p7k9mk21iws8q1")))

(define-public crate-pipe-logger-lib-1.1.5 (c (n "pipe-logger-lib") (v "1.1.5") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "03ir42dcy0b88xvlarg4663nz85rss8cyc1g5jn3lpbdj2h9d1c0")))

(define-public crate-pipe-logger-lib-1.1.6 (c (n "pipe-logger-lib") (v "1.1.6") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "05pviid1kg2wi7khggsbbxyik99rs4q4vgh4k55as0q5d6almph8")))

(define-public crate-pipe-logger-lib-1.1.7 (c (n "pipe-logger-lib") (v "1.1.7") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "025asvgjbwh3d9whb9y5jq8ygs1d2af6g7b3gpvrilwyk55c8x1f")))

(define-public crate-pipe-logger-lib-1.1.8 (c (n "pipe-logger-lib") (v "1.1.8") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "0p0shrfkbbwg95amswrzmqlsm68fzq1pcn12g83vl1a07lzy3cja")))

(define-public crate-pipe-logger-lib-1.1.9 (c (n "pipe-logger-lib") (v "1.1.9") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "0h311cx77iq0x3ajy0fllygz3v4y5m5ybni7drdh6qkqhk0xncwj")))

(define-public crate-pipe-logger-lib-1.1.10 (c (n "pipe-logger-lib") (v "1.1.10") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "05z835zklhjj37n4aq15fx61q2rj8xy0wvm86i68x7dh0yn84kqb")))

(define-public crate-pipe-logger-lib-1.1.11 (c (n "pipe-logger-lib") (v "1.1.11") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "1kvhxgmj8584byl5f8fv34l2gn7cv805xha6yavdkxkvkq77zvi8")))

(define-public crate-pipe-logger-lib-1.1.12 (c (n "pipe-logger-lib") (v "1.1.12") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "1q8hrp67dvqhkmdxc37csd56is49cip7jgdiws5ykcxg2169hg0b")))

(define-public crate-pipe-logger-lib-1.1.13 (c (n "pipe-logger-lib") (v "1.1.13") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "182w4h4xjszxhnj61sinl8kwp46vms2gvwdl3asca4nfx6fx4k9d")))

(define-public crate-pipe-logger-lib-1.1.14 (c (n "pipe-logger-lib") (v "1.1.14") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "1s1q493yk1b2zr4abclm3ir5i5v2xarkf4a7l1ybylx1m3il04n2")))

(define-public crate-pipe-logger-lib-1.1.15 (c (n "pipe-logger-lib") (v "1.1.15") (d (list (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "1p55b8nqkz02gnvvmy79mb24pjjix2fqhr504zmr2gvkw6bkpmcw")))

(define-public crate-pipe-logger-lib-1.1.16 (c (n "pipe-logger-lib") (v "1.1.16") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("clock"))) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "17gg1kwb5f7pa1z9bxjnzdvmw8ckig1xw226mw23fcpwsad1liah") (r "1.56")))

(define-public crate-pipe-logger-lib-1.1.17 (c (n "pipe-logger-lib") (v "1.1.17") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("clock"))) (k 0)) (d (n "path-absolutize") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "xz2") (r "^0.1.5") (d #t) (k 0)))) (h "0pljaq20pc8zpg4q18sdsjr5rndh7ydh3lpz4y9ijaqs1y7vic6m") (r "1.61")))

