(define-module (crates-io pi pe pipebuilder_mock) #:use-module (crates-io))

(define-public crate-pipebuilder_mock-0.1.0 (c (n "pipebuilder_mock") (v "0.1.0") (d (list (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "pipebuilder_common") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 0)))) (h "1j7pw7ffvpp6fxvc7jkhxvymavh2i7xrg80z6ckl1wmzmvzf9c75")))

