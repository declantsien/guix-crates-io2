(define-module (crates-io pi pe pipe_watcher) #:use-module (crates-io))

(define-public crate-pipe_watcher-1.0.0 (c (n "pipe_watcher") (v "1.0.0") (d (list (d (n "ipipe") (r "^0.4.1") (k 0)))) (h "17kkv1jz9c2j7cb31vfb7hkwrbkvl9abn63dapdbqlnq28dgbday")))

(define-public crate-pipe_watcher-1.0.1 (c (n "pipe_watcher") (v "1.0.1") (d (list (d (n "ipipe") (r "^0.7.3") (k 0)))) (h "0g4ap06fq9q27pvlc9gvja5iybyr93fbq65y6nvdcr2yvj49wq2n")))

(define-public crate-pipe_watcher-2.0.0 (c (n "pipe_watcher") (v "2.0.0") (d (list (d (n "getch") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "ipipe") (r "^0.7.3") (k 0)))) (h "0l8v7hv9c94k34shzzfacbzx5qg22ipk6bb9aizdm37jqaz0yzvk")))

(define-public crate-pipe_watcher-2.0.1 (c (n "pipe_watcher") (v "2.0.1") (d (list (d (n "getch") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "ipipe") (r "^0.7.3") (k 0)))) (h "05ff35h1s59n3bg6c5lziymlgh8gd8597ak36mqsi5i7pwi9dss4")))

(define-public crate-pipe_watcher-2.0.2 (c (n "pipe_watcher") (v "2.0.2") (d (list (d (n "getch") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "ipipe") (r "^0.7.4") (k 0)))) (h "1hzk46ikzm4878i9bkvv3nrx0fga8v2lsy678bhxaqxlb3n46sfg")))

(define-public crate-pipe_watcher-2.0.3 (c (n "pipe_watcher") (v "2.0.3") (d (list (d (n "getch") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "ipipe") (r "^0.7.4") (k 0)))) (h "1qw2awq68pn846ymbjr406szj1r6g4c94d3yddjld6xbn85c6n6b")))

(define-public crate-pipe_watcher-2.0.4 (c (n "pipe_watcher") (v "2.0.4") (d (list (d (n "getch") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "ipipe") (r "^0.7.4") (k 0)))) (h "0d3s1m7f8y8y4rpd7p5brc917v5228j7449qkacvynpkayz3s685") (f (quote (("default" "getch"))))))

(define-public crate-pipe_watcher-2.0.5 (c (n "pipe_watcher") (v "2.0.5") (d (list (d (n "getch") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "ipipe") (r "^0.7.4") (k 0)))) (h "0zl99cfyrm7v59sivcky9zv9p8zlbssafbg281ldhcbnlcyf2b42") (f (quote (("default" "getch"))))))

(define-public crate-pipe_watcher-2.0.6 (c (n "pipe_watcher") (v "2.0.6") (d (list (d (n "getch") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "ipipe") (r "^0.7.4") (k 0)))) (h "0wp47abp2rssj5gyajzfakng8isddhjnprlwlaj9cdbkcmi0fnk9") (f (quote (("default" "getch"))))))

(define-public crate-pipe_watcher-2.1.0 (c (n "pipe_watcher") (v "2.1.0") (d (list (d (n "getch") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "ipipe") (r "^0.7.4") (k 0)))) (h "0jp925kb9840kpxgzl3vgbw4j4zb9bzgzd1mzdaayhfb4jrkvd67") (f (quote (("default" "getch"))))))

(define-public crate-pipe_watcher-2.1.1 (c (n "pipe_watcher") (v "2.1.1") (d (list (d (n "getch") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "ipipe") (r "^0.8") (k 0)))) (h "0cwmwd66ri8fs1dsqqrmx2bl5k72n15wbak9wxks9lgdc0d3b6lg") (f (quote (("default" "getch"))))))

(define-public crate-pipe_watcher-2.1.2 (c (n "pipe_watcher") (v "2.1.2") (d (list (d (n "getch") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "ipipe") (r "^0.8") (k 0)))) (h "0zh4s5xryrml8iz1cgywab3ip1a2j8b8hdzvzjhj8pdg8n7z8kcf") (f (quote (("default" "getch"))))))

