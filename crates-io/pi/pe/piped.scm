(define-module (crates-io pi pe piped) #:use-module (crates-io))

(define-public crate-piped-0.0.0 (c (n "piped") (v "0.0.0") (d (list (d (n "reqwest") (r "^0.11.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "07wl61gw1x3jixpww8l9qkcxdbl0w7j03ajhbqpv26z39f8hav1q") (y #t)))

(define-public crate-piped-0.0.1 (c (n "piped") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1gvfmhc4469almhf2505v4ff6g2n0kvfih3mfi1fzw0px49iygyn")))

(define-public crate-piped-0.0.2 (c (n "piped") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "01dh7d2h3xxmh17s0cpb0s0bsqy4a3y2ppayxqahc6hfwfrzd7n2")))

(define-public crate-piped-0.0.3 (c (n "piped") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0wwlkxsylk34w454ywkx6dqkf5s13dd5yczsndhkd1zpgaz0krr1")))

(define-public crate-piped-0.0.4 (c (n "piped") (v "0.0.4") (d (list (d (n "reqwest") (r "^0.11") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "13h59k0yjxbly78ac8mvi00g2wxk8zfs5i1578g82if8rhvi68cc")))

