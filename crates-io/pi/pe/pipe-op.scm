(define-module (crates-io pi pe pipe-op) #:use-module (crates-io))

(define-public crate-pipe-op-0.0.1 (c (n "pipe-op") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "191pdzilav2z66jaivv21nbx04ng33cplvi2c7gxn2ihhcyisfy6")))

