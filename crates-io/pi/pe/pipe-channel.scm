(define-module (crates-io pi pe pipe-channel) #:use-module (crates-io))

(define-public crate-pipe-channel-1.0.0 (c (n "pipe-channel") (v "1.0.0") (d (list (d (n "nix") (r "^0.6.0") (d #t) (k 0)))) (h "11rcla8nycqlkwmfj3s2lcjrvdk4p85hc32c927zhagc24d6m5ll")))

(define-public crate-pipe-channel-1.0.1 (c (n "pipe-channel") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.13") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)))) (h "1fi7xkd4zsi9l2zrzfwyipd8ijjf4crryamii9ph59z7x3nm1ilb")))

(define-public crate-pipe-channel-1.1.0 (c (n "pipe-channel") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.13") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)))) (h "0y8x86b3y3s1q1rsr04aiaygi0vpii6nc6lg66f1j4q7zl25sxh7")))

(define-public crate-pipe-channel-1.1.1 (c (n "pipe-channel") (v "1.1.1") (d (list (d (n "libc") (r "^0.2.13") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)))) (h "0f2a97ladms9w4caqf5zd2swxar05g4bjzbwz792lhamyw557pbn")))

(define-public crate-pipe-channel-1.1.2 (c (n "pipe-channel") (v "1.1.2") (d (list (d (n "libc") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.8.1") (d #t) (k 0)))) (h "13dil3j59xdp4mv57qxch6b5rz88569556jppad07017ravhd6wn")))

(define-public crate-pipe-channel-1.2.0 (c (n "pipe-channel") (v "1.2.0") (d (list (d (n "libc") (r "^0.2.22") (d #t) (k 0)) (d (n "nix") (r "^0.8.1") (d #t) (k 0)))) (h "0d3gyidvsmrh9rkvjz64w8n4rj49l7jsn4s5zjh0zrhqc9cl8fw3")))

(define-public crate-pipe-channel-1.2.1 (c (n "pipe-channel") (v "1.2.1") (d (list (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "1xsap9hqsd6wkh7wj4v3zb2sbbvcgzis7757ymk6n0d7ral1m4k1")))

(define-public crate-pipe-channel-1.2.2 (c (n "pipe-channel") (v "1.2.2") (d (list (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "1b7lq6qrafda24c62l5q6vr0aaj1cvqp8nl7q98hdnvlkg7ax2x2")))

(define-public crate-pipe-channel-1.3.0 (c (n "pipe-channel") (v "1.3.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "07yvpvgmslmmadkz5dhn4as5x1qxc6g29bs1iqv8zjnp4x4zsf82")))

