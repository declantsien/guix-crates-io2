(define-module (crates-io pi pe pipederive) #:use-module (crates-io))

(define-public crate-pipederive-0.1.0 (c (n "pipederive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (f (quote ("full"))) (d #t) (k 0)))) (h "0cp738hv5vdfhna833am8jqmnd16pgxa7a2ybf0c754k1b7661dq")))

(define-public crate-pipederive-0.1.1 (c (n "pipederive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (f (quote ("full"))) (d #t) (k 0)))) (h "1n2j7pngkyvhprkxprqn7b2rsjz9vp0v2vdkix1a4bnxl31jdmmi")))

(define-public crate-pipederive-0.1.2 (c (n "pipederive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (f (quote ("full"))) (d #t) (k 0)))) (h "02rf5vf14zl30b5qsc8d9cg1x5zwrbjpzsyb0milq3l1c87nhczk")))

(define-public crate-pipederive-0.1.3 (c (n "pipederive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (f (quote ("full"))) (d #t) (k 0)))) (h "1vvrkgyq0ad6dljrlmb3wzf3fcgp49yfidvsjiv07dz75nx7vk6p")))

(define-public crate-pipederive-0.1.4 (c (n "pipederive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (f (quote ("full"))) (d #t) (k 0)))) (h "19c8ficsysffhn0rf5xapxfvm7d3y6mjmnarbxlahr42ir4d6f04")))

(define-public crate-pipederive-0.2.0 (c (n "pipederive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (f (quote ("full"))) (d #t) (k 0)))) (h "014bpy4cxvq79mz5ld6x77lji3wif48bbqqac2m40p36mfxryhxk")))

(define-public crate-pipederive-0.2.1 (c (n "pipederive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (f (quote ("full"))) (d #t) (k 0)))) (h "0i2kbl8hlb4cylz6dk1q5b516v6n1i85kcbfpq9ksmd866b6bbg7")))

