(define-module (crates-io pi pe pipedynamodb) #:use-module (crates-io))

(define-public crate-pipedynamodb-0.1.5 (c (n "pipedynamodb") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "dynamodb") (r "^0.4.0") (d #t) (k 0) (p "aws-sdk-dynamodb")) (d (n "pipebase") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0wjhpwss99y0bkl2wvcb3kapi09bybq9qrf2g7a8rl37g0ap6v0r")))

