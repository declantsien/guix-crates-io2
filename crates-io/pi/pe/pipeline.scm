(define-module (crates-io pi pe pipeline) #:use-module (crates-io))

(define-public crate-pipeline-0.4.0 (c (n "pipeline") (v "0.4.0") (h "1rc316g1bkxndd493g89qnbfk4jp1bslai434s0gfwbvpr5ziqmq")))

(define-public crate-pipeline-0.4.1 (c (n "pipeline") (v "0.4.1") (h "0x5zf7pzigqa05wcpdaqv5nl6v6kf5yb5265lmk2qg1fh0f0rk9h")))

(define-public crate-pipeline-0.5.0 (c (n "pipeline") (v "0.5.0") (h "1h67m0z74y75xifx9aspq1dbf707nqf09j8pibmrcab3z83ncnyi")))

