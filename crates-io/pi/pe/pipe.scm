(define-module (crates-io pi pe pipe) #:use-module (crates-io))

(define-public crate-pipe-0.0.1 (c (n "pipe") (v "0.0.1") (d (list (d (n "resize-slice") (r "^0.0") (d #t) (k 0)))) (h "16fvqm6wq0cwyj6y4pwrihwh164lsngzsq78fi06kk0drmxpjwzf")))

(define-public crate-pipe-0.0.2 (c (n "pipe") (v "0.0.2") (h "1r9b4gxazklvb2psn7x7whkzq1mcdmxzq8zny7fhhs5ll1m0v297") (f (quote (("bench"))))))

(define-public crate-pipe-0.0.3 (c (n "pipe") (v "0.0.3") (h "1n9h3wihwm778xiahxq7vxsqmg37rlkcs28yarqgnxgw555bvj4w") (f (quote (("bench"))))))

(define-public crate-pipe-0.1.0 (c (n "pipe") (v "0.1.0") (d (list (d (n "readwrite") (r "^0.1") (o #t) (d #t) (k 0)))) (h "01ihfrbfr10w4lw9kqa3qi6jmn4i1nh8v9j45j7wgshi8r9zyig3") (f (quote (("bidirectional" "readwrite") ("bench"))))))

(define-public crate-pipe-0.2.0 (c (n "pipe") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "os_pipe") (r "^0.8.1") (d #t) (k 2)) (d (n "readwrite") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "13vzvy7cykhcijmp6zc5idb0ja54jfdb5xg7wksjlg4n0skn7xyr") (f (quote (("unstable-doc-cfg") ("bidirectional" "readwrite"))))))

(define-public crate-pipe-0.3.0 (c (n "pipe") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9.0") (d #t) (k 2)) (d (n "readwrite") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0v0h61q0zx49bgv40k19fjdj3khcppk5l34wvysr2s855q21xldw") (f (quote (("unstable-doc-cfg") ("bidirectional" "readwrite"))))))

(define-public crate-pipe-0.4.0 (c (n "pipe") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9.0") (d #t) (k 2)) (d (n "readwrite") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0xcd3mf7rg925iwcy0yaa0cl6fd9kqax8n2cvxkbjzi1v8kqyyqw") (f (quote (("unstable-doc-cfg") ("bidirectional" "readwrite"))))))

