(define-module (crates-io pi pe pipebuf_mio) #:use-module (crates-io))

(define-public crate-pipebuf_mio-0.1.0 (c (n "pipebuf_mio") (v "0.1.0") (d (list (d (n "mio") (r "^0.8") (f (quote ("net"))) (d #t) (k 0)) (d (n "pipebuf") (r "^0.2") (d #t) (k 0)))) (h "0hf7lkrdx2a7hxysn4v7iws0kz2i6lhhdamx4vkr6x6vaircf5kh")))

(define-public crate-pipebuf_mio-0.1.1 (c (n "pipebuf_mio") (v "0.1.1") (d (list (d (n "mio") (r "^0.8") (f (quote ("net"))) (d #t) (k 0)) (d (n "pipebuf") (r "^0.2") (d #t) (k 0)))) (h "1waamm6b39ccv990q2rhnjjmgsp0sri3rji3ih861bsdh7d55si8")))

(define-public crate-pipebuf_mio-0.2.0 (c (n "pipebuf_mio") (v "0.2.0") (d (list (d (n "mio") (r "^0.8") (f (quote ("net"))) (d #t) (k 0)) (d (n "pipebuf") (r "^0.3") (d #t) (k 0)))) (h "15yfbmz7va7vsns9526h0kqgd6w3baf6cqmx0qgqlaag7x6wzza6")))

(define-public crate-pipebuf_mio-0.2.1 (c (n "pipebuf_mio") (v "0.2.1") (d (list (d (n "mio") (r "^0.8") (f (quote ("net"))) (d #t) (k 0)) (d (n "pipebuf") (r "^0.3") (d #t) (k 0)))) (h "1psczzh62fn4bwj9msnhl44xxra3bv8ny4f93gypz9zj5l311i1r")))

