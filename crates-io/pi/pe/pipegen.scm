(define-module (crates-io pi pe pipegen) #:use-module (crates-io))

(define-public crate-pipegen-0.1.0 (c (n "pipegen") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sp5zjb2rf5nvbsfb4lbspxlkw392s453xgmzghhjim7a8mlbv92")))

(define-public crate-pipegen-0.1.1 (c (n "pipegen") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bclc6l9p64yfbk0rl40hnd9dg5azvdrjdaz2sb5qw6gspchay23")))

(define-public crate-pipegen-0.1.2 (c (n "pipegen") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pw0lzgicgigxd84mkdcip926nzchxmx9qx939jnsvif5fnmnylz")))

(define-public crate-pipegen-0.1.3 (c (n "pipegen") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "06mhr7wn7qhdjnf46n4wxz3pdi8flni6hvpjkf97kwf59c1jymry")))

(define-public crate-pipegen-0.1.4 (c (n "pipegen") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vn313m6vqbrxq0pjci1lm80dsspvxflpaz6rq1bzzq6v5n6521b")))

(define-public crate-pipegen-0.1.5 (c (n "pipegen") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cdm84hpqdk70vvrvm3g0lmham297glff1mj0lm342mzqxjdgyy2")))

(define-public crate-pipegen-0.1.6 (c (n "pipegen") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kxcky7y49n3i6wsa16xwwh2bdi62zq986gisf36qlvg1xy5r2yd")))

(define-public crate-pipegen-0.1.7 (c (n "pipegen") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "064dj5hc410ljxi9qxys046v6g9x50v767aip1w8xn55w17a2a42")))

(define-public crate-pipegen-0.1.8 (c (n "pipegen") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c2znvrkshswyphw7jmcvqg8lr3p0hh6ynffy46jr95jdvcsv0qp")))

(define-public crate-pipegen-0.1.9 (c (n "pipegen") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pvlpsbmiscfmavzvlm31k3v7zhvszibhjzajyydravnp8a30xxz")))

(define-public crate-pipegen-0.1.10 (c (n "pipegen") (v "0.1.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xf24yvjq1j10qqdblsvh7wfkfbm08lpd6zllhg26lf7jhcn8l5d")))

(define-public crate-pipegen-0.1.11 (c (n "pipegen") (v "0.1.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i97fvh4yj57n2zrm97k6m5anqg31sx831mvcjsk7p1bzkwaf6dq")))

(define-public crate-pipegen-0.1.12 (c (n "pipegen") (v "0.1.12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zsa1yqqf5h1fag08v2xx2ncswm0c5z32sl8kqijcq98ikz1nm5r")))

(define-public crate-pipegen-0.1.13 (c (n "pipegen") (v "0.1.13") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bvfchxy6b01vdaah7a7lz14kbvypb17mvk3xgra5zclksrghsc9")))

(define-public crate-pipegen-0.1.14 (c (n "pipegen") (v "0.1.14") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "042s086r2050z7bzrp0ayqgz4cpr2hjmm5fxwsgk3bf9jn0h2p7p")))

(define-public crate-pipegen-0.1.15 (c (n "pipegen") (v "0.1.15") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rwxcl701gzwdhj13v69l0bhmxcq8nzl8qq19brzdky3n59kh2hi")))

(define-public crate-pipegen-0.1.16 (c (n "pipegen") (v "0.1.16") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "15kc6ri0c4xzdzzgrppc9103mm9mfcn8nw69bd7gh7zyjqxxp9bz")))

(define-public crate-pipegen-0.1.17 (c (n "pipegen") (v "0.1.17") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nnxxg3v68c9hpnp195kx489gd27v4nyg1zay1z071pklzf2205x")))

(define-public crate-pipegen-0.2.0 (c (n "pipegen") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l61fnrw457m53fyvgkg52xh5qgf61bacdda8cjsy2aq51nafbyj")))

(define-public crate-pipegen-0.2.1 (c (n "pipegen") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bp4vf04ck612iyr2xllq05zamg5fabm6ig55dbj4mmdxh4i68wp")))

(define-public crate-pipegen-0.2.2 (c (n "pipegen") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "07975dc7cjn1cpnlh8j5f459kjfph3sfad5990lvz47qcljnd7aw")))

