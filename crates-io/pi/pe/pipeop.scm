(define-module (crates-io pi pe pipeop) #:use-module (crates-io))

(define-public crate-pipeop-0.1.0 (c (n "pipeop") (v "0.1.0") (h "0152is3dk0i46fxngk19m7hcqs91brqq41gmvgknvvqs71ldsa2g")))

(define-public crate-pipeop-0.1.1 (c (n "pipeop") (v "0.1.1") (h "09k4by6b3r7mj6fqk18k4k8dy8wcc6h7pm5r0yxn95x25xzw48qg")))

(define-public crate-pipeop-0.1.2 (c (n "pipeop") (v "0.1.2") (h "0n5741pwnkvkhp35hvh6d7mwnwfs6x1ml91izg4da7ak27mljhxl")))

(define-public crate-pipeop-0.1.3 (c (n "pipeop") (v "0.1.3") (h "0iip0l3jdr9wp55xhzlhk0m5880saz2g533xx6mhpyr6dph77h2c")))

(define-public crate-pipeop-0.1.4 (c (n "pipeop") (v "0.1.4") (h "1xmw71779yfq8b451p1k5xkyxpgavhlvfw37560y57m169dprg9f")))

(define-public crate-pipeop-0.1.5 (c (n "pipeop") (v "0.1.5") (h "1m3ww59v08xp3ay3xkz4qccicyllxkramw0fpkgsxyb9dhgp7xcv")))

(define-public crate-pipeop-0.1.6 (c (n "pipeop") (v "0.1.6") (h "1ky5x9qc2lvdaf2s9pphrmk73sm8cbm6i12j016n7xl7qvgzb6wv")))

(define-public crate-pipeop-0.1.7 (c (n "pipeop") (v "0.1.7") (h "1za6dz1dqgwvxil1wmbhb6xbdhw2kd40jicb1sij2j9x6rq4h1mb")))

(define-public crate-pipeop-0.1.8 (c (n "pipeop") (v "0.1.8") (h "0qwb653iqn5n5gnz0g8a101kgxihid1f2d21ml6cw5wmvch4r2vm")))

(define-public crate-pipeop-0.2.0 (c (n "pipeop") (v "0.2.0") (h "0b05ascq2v50yf6bk2zids5vhb1rm1cgfhljc280ag97rksiwdjs")))

