(define-module (crates-io pi pe pipeviewer-exercise) #:use-module (crates-io))

(define-public crate-pipeviewer-exercise-0.1.0 (c (n "pipeviewer-exercise") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "1cg3pa6283bb3s552jhhnldq6ap8bcdjm8rhdinky72acm6pl5m5")))

