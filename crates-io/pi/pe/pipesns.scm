(define-module (crates-io pi pe pipesns) #:use-module (crates-io))

(define-public crate-pipesns-0.1.5 (c (n "pipesns") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "pipebase") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sns") (r "^0.4.0") (d #t) (k 0) (p "aws-sdk-sns")) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "09r8qyz23pnw04fxivjmy8l0flipcarqxm2jign61yzr9gql2hkw")))

