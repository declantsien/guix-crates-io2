(define-module (crates-io pi pe pipedream) #:use-module (crates-io))

(define-public crate-pipedream-0.0.1 (c (n "pipedream") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6.2") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "toolbelt") (r "^0.0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "06rmw813mh4w2j1qrjc9136azlwzqmww5jmkasi0l3wx49s8wd76")))

