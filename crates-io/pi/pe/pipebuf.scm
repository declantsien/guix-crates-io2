(define-module (crates-io pi pe pipebuf) #:use-module (crates-io))

(define-public crate-pipebuf-0.2.0 (c (n "pipebuf") (v "0.2.0") (h "1gw4ym5iqrkzbaqw986099wisnklvmy1ra6vcrfffjn07dgjigm0") (f (quote (("std") ("static") ("default" "std") ("alloc"))))))

(define-public crate-pipebuf-0.2.1 (c (n "pipebuf") (v "0.2.1") (h "1y2558vnajq8by88j7wqm22i7dq665v2q54kdkivp0y6ql169hsh") (f (quote (("std") ("static") ("default" "std") ("alloc"))))))

(define-public crate-pipebuf-0.3.0 (c (n "pipebuf") (v "0.3.0") (h "0csqvs990p3pk1rpnryrbcv6dib36qryx0d3ygxcngh4f6xvdl4d") (f (quote (("std") ("static") ("default" "std") ("alloc"))))))

(define-public crate-pipebuf-0.3.1 (c (n "pipebuf") (v "0.3.1") (h "1219mbr0bbq5bi6n5ygg422cj8s9nlfj16246zgapja0xzvpzidi") (f (quote (("std") ("static") ("default" "std") ("alloc"))))))

