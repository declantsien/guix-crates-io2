(define-module (crates-io pi pe pipefile) #:use-module (crates-io))

(define-public crate-pipefile-0.1.0 (c (n "pipefile") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j56ysdx4ckibqjhc78wf4i32nin9h36x37x6s0gfpbhw7synq3x") (f (quote (("default"))))))

(define-public crate-pipefile-0.1.1 (c (n "pipefile") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yd2a1bfcig4b9y4ma1084zcn04l91lzzaczbwd6xf40pzyvp7s0") (f (quote (("default"))))))

