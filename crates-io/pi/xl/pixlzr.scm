(define-module (crates-io pi xl pixlzr) #:use-module (crates-io))

(define-public crate-pixlzr-0.1.0 (c (n "pixlzr") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "1hpwhxdxyb818rrmsgqxjhbw4c80kbcw7jcv8ps7l6zcagr5l3ml")))

(define-public crate-pixlzr-0.1.1 (c (n "pixlzr") (v "0.1.1") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "08ff132r01jsfbf0qwm1d1ilxw50w29sgx2i5x1xzgi6x81la34k")))

(define-public crate-pixlzr-0.2.0 (c (n "pixlzr") (v "0.2.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "00z6rq0bd7p19kpg2hbywx7fywxajq6fxrj50vawx9bs3xgra5i1")))

(define-public crate-pixlzr-0.2.1 (c (n "pixlzr") (v "0.2.1") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "0fplyidrb42z51hn79bi7c0xyr7b64cjyyvv4i0krkyny65n4n8q")))

(define-public crate-pixlzr-0.2.2 (c (n "pixlzr") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "qoi") (r "^0.4") (d #t) (k 0)))) (h "1cwrf1pmwkpbyr3vncif6p9a6wnrpaw6zi95nq3dpai41cpg3pim")))

(define-public crate-pixlzr-0.3.0 (c (n "pixlzr") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fast_image_resize") (r "^2.7.3") (d #t) (k 0)) (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "qoi") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0mmipi29j0kb1inwiaj7ya9zp9qkh1ms0jh11335xcqvxrn5x47b") (f (quote (("default" "image-rs" "cli")))) (s 2) (e (quote (("image-rs" "dep:image" "dep:palette") ("cli" "dep:clap"))))))

