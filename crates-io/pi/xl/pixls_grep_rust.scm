(define-module (crates-io pi xl pixls_grep_rust) #:use-module (crates-io))

(define-public crate-pixls_grep_rust-0.1.0 (c (n "pixls_grep_rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1b5m6agi5dnwh4kdcn3mim6j1ymrn6dfd9jahh5h9zidgbgdjgsx")))

