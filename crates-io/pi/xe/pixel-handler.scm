(define-module (crates-io pi xe pixel-handler) #:use-module (crates-io))

(define-public crate-pixel-handler-1.0.1 (c (n "pixel-handler") (v "1.0.1") (d (list (d (n "ggez") (r "^0.9.3") (d #t) (k 0)))) (h "1nniqv0mvz3n00wxxl8dandxrvhpn982283dxmb7hlkk2j5cxdw5") (y #t)))

(define-public crate-pixel-handler-0.1.2 (c (n "pixel-handler") (v "0.1.2") (d (list (d (n "ggez") (r "^0.9.3") (d #t) (k 0)))) (h "0cy1biyilmiif5gvy5bxnc5ybf2d18wm5gsmhlh8jrhjwr3dnsis")))

(define-public crate-pixel-handler-0.1.3 (c (n "pixel-handler") (v "0.1.3") (d (list (d (n "ggez") (r "^0.9.3") (d #t) (k 0)))) (h "00a7gkiadm1jrs24bwyv1xvjik045pv9fj6azilfy6d4p8kipxyy")))

(define-public crate-pixel-handler-0.1.4 (c (n "pixel-handler") (v "0.1.4") (d (list (d (n "ggez") (r "^0.9.3") (d #t) (k 0)))) (h "16akas5n37i2m7q5rkxdy2ikyvg43q023j9mx7n5m8nzh5rszhv3")))

(define-public crate-pixel-handler-0.1.5 (c (n "pixel-handler") (v "0.1.5") (d (list (d (n "ggez") (r "^0.9.3") (d #t) (k 0)))) (h "01cvycav1r2wnmrr0390vd6ph85kllj48v6ygjjdhjarjfc85wy6")))

(define-public crate-pixel-handler-0.1.6 (c (n "pixel-handler") (v "0.1.6") (d (list (d (n "ggez") (r "^0.9.3") (d #t) (k 0)))) (h "0gd2ni3k26xj0k00g805wl1gci01x2prkg56rfbkf04rns1n9f0f")))

(define-public crate-pixel-handler-0.1.61 (c (n "pixel-handler") (v "0.1.61") (d (list (d (n "ggez") (r "^0.9.3") (d #t) (k 0)))) (h "0whywn1scpfiwgj82pyy3ldbfmkqzp6fr492n673ldmj1q5g9lgx")))

(define-public crate-pixel-handler-0.1.62 (c (n "pixel-handler") (v "0.1.62") (d (list (d (n "ggez") (r "^0.9.3") (d #t) (k 0)))) (h "18vy202hxah5vff3wdf0nwljjykafibjwddchkv2r9zwlsn0ky92")))

