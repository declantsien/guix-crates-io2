(define-module (crates-io pi xe pixel_formats) #:use-module (crates-io))

(define-public crate-pixel_formats-0.1.0 (c (n "pixel_formats") (v "0.1.0") (d (list (d (n "bitfrob") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wb8j351vdg2gbm0d8xg7bmkk9ivjb6a2r9rczgs0kmskbxgghbi") (f (quote (("default" "bytemuck" "bitfrob"))))))

(define-public crate-pixel_formats-0.1.1 (c (n "pixel_formats") (v "0.1.1") (d (list (d (n "bitfrob") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fast-srgb8") (r "^1.0.0") (d #t) (k 0)))) (h "0wyvna9i080wmhk8jn6nmgly7idm12izhvxsdy3ib5scq85y3vjj") (f (quote (("default" "bytemuck" "bitfrob"))))))

(define-public crate-pixel_formats-0.1.2 (c (n "pixel_formats") (v "0.1.2") (d (list (d (n "bitfrob") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fast-srgb8") (r "^1.0.0") (d #t) (k 0)))) (h "0qfdi496wp5s5c3p8axfrk73zhgw94pp3y9ysva8vp7ql65hffmz") (f (quote (("default" "bytemuck" "bitfrob"))))))

(define-public crate-pixel_formats-0.1.3 (c (n "pixel_formats") (v "0.1.3") (d (list (d (n "bitfrob") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fast-srgb8") (r "^1.0.0") (d #t) (k 0)))) (h "0m5dnw6d16pw61wrnrzfnripm5qpms3f20m1jcgj4h8534xzh1g0") (f (quote (("default" "bytemuck" "bitfrob"))))))

(define-public crate-pixel_formats-0.1.4 (c (n "pixel_formats") (v "0.1.4") (d (list (d (n "bitfrob") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fast-srgb8") (r "^1.0.0") (d #t) (k 0)))) (h "03wkq71bxxxrc6pl9pshc2srw0x4hzwrwp2sqd2xm5z63337ra71") (f (quote (("default" "bytemuck" "bitfrob"))))))

(define-public crate-pixel_formats-0.1.5 (c (n "pixel_formats") (v "0.1.5") (d (list (d (n "bitfrob") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fast-srgb8") (r "^1.0.0") (d #t) (k 0)))) (h "1zcq87acc20xbdrbnfjg4p5pvi0fn3v6wpsfbk9xwq49jqp2ncbq") (f (quote (("default" "bytemuck" "bitfrob"))))))

