(define-module (crates-io pi xe pixelmatch) #:use-module (crates-io))

(define-public crate-pixelmatch-0.1.0 (c (n "pixelmatch") (v "0.1.0") (d (list (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (o #t) (d #t) (k 0)))) (h "0r4pg508dh33wnbn72y4imick8w05xcgapmr5pqzpgnb5957f5ab") (f (quote (("build-binary" "structopt"))))))

