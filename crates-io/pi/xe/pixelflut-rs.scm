(define-module (crates-io pi xe pixelflut-rs) #:use-module (crates-io))

(define-public crate-pixelflut-rs-0.0.1 (c (n "pixelflut-rs") (v "0.0.1") (d (list (d (n "custom_error") (r "^1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11") (d #t) (k 2)) (d (n "tokio") (r "^0.3") (f (quote ("rt-multi-thread" "io-util" "net" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("macros"))) (d #t) (k 2)))) (h "08gn32jhx09i04lc9h8bkxx057gllzq8nya9bpmjnxn1nl7xsy3z")))

(define-public crate-pixelflut-rs-0.1.0 (c (n "pixelflut-rs") (v "0.1.0") (d (list (d (n "custom_error") (r "^1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11") (d #t) (k 2)) (d (n "tokio") (r "^0.3") (f (quote ("rt-multi-thread" "io-util" "net" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("macros"))) (d #t) (k 2)))) (h "0fyajsgclsh50ymk1lypbx21qsldqjgwal5jxaslvyvd1zb3dj0h")))

(define-public crate-pixelflut-rs-0.1.1 (c (n "pixelflut-rs") (v "0.1.1") (d (list (d (n "custom_error") (r ">=1.8.0, <2.0.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "simple_logger") (r ">=1.11.0, <2.0.0") (d #t) (k 2)) (d (n "tokio") (r ">=0.3.0, <0.4.0") (f (quote ("rt-multi-thread" "io-util" "net" "sync"))) (d #t) (k 0)) (d (n "tokio") (r ">=0.3.0, <0.4.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "0496alq8kq8ywdv4zn0zcrbdcsxm7hdmcvkr12myvl0f1gx5mkp6")))

(define-public crate-pixelflut-rs-0.2.0 (c (n "pixelflut-rs") (v "0.2.0") (d (list (d (n "custom_error") (r "^1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt-multi-thread" "io-util" "net" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "063fz9m7gf859ccffznnkvgyc2h0rrnf7sk986ayiagqmjbsmjsd")))

