(define-module (crates-io pi xe pixelpwnr-render) #:use-module (crates-io))

(define-public crate-pixelpwnr-render-0.0.1 (c (n "pixelpwnr-render") (v "0.0.1") (d (list (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.16") (d #t) (k 0)) (d (n "gfx_text") (r "^0.29") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "old_school_gfx_glutin_ext") (r "^0.28") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "0mqsg9bn24pja2x0nsq282ixp7glhr8jkv03apaimm9m2fpxbik4")))

