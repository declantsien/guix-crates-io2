(define-module (crates-io pi xe pixelize) #:use-module (crates-io))

(define-public crate-pixelize-0.1.0 (c (n "pixelize") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 0)))) (h "1ph8jcxasj90sawr8749h36n6x4r9kaajg4a6zmwkn56y939m4pb")))

(define-public crate-pixelize-0.1.1 (c (n "pixelize") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 0)))) (h "1n2qd8m5xzylj5p4mf3yk04jmbpddl9a0wxywp5i2z05cbbqm40r")))

(define-public crate-pixelize-0.1.2 (c (n "pixelize") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 0)))) (h "08n9ll27xf5y4avindcacba2m4wg5gvzi9k19k98wxz07v0m49i0")))

(define-public crate-pixelize-0.1.3 (c (n "pixelize") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 0)))) (h "0k3lx162kqm8y5pi5plazsdmpgzascnfxjhbvkg6gjg0x2gldyn0")))

(define-public crate-pixelize-0.2.0 (c (n "pixelize") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 0)))) (h "14823ybiizxm4bp817xm3xil82r14pbfij22v1hz3rdppmrh4jkr")))

(define-public crate-pixelize-0.2.1 (c (n "pixelize") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 0)))) (h "1sh5m7khv9db193lfpbfgjyzdg100gi0bsf18z75zxd7ax2dcf74")))

(define-public crate-pixelize-0.3.0 (c (n "pixelize") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 0)))) (h "00wcq6r6qir9mcrxk31gg621ipvhfrxflafwdg00995l16hxskl3")))

(define-public crate-pixelize-0.3.1 (c (n "pixelize") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 0)))) (h "15n3iknkrbbwjp12497lnhrrkzhp61nb064ad2z59mr5lms6lv6p")))

