(define-module (crates-io pi xe pixels-cli) #:use-module (crates-io))

(define-public crate-pixels-cli-0.1.0 (c (n "pixels-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "webp") (r "^0.2.6") (d #t) (k 0)))) (h "1m5pf22sy2gbhxnh0ppmx7x68rh8w4ygwrpcxhqdq6an1jvp339l")))

(define-public crate-pixels-cli-0.1.1 (c (n "pixels-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "webp") (r "^0.2.6") (d #t) (k 0)))) (h "13y3f1bk5s37v3xxvnl2kqy188fy5p95j0qaybwbga08wchi2i6m")))

(define-public crate-pixels-cli-0.1.2 (c (n "pixels-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.6") (d #t) (k 0)))) (h "0w18jzicc0ql7vlmv6qd7k0j5q4dbwpaaa2wkr8p4g84xb04kw5k")))

