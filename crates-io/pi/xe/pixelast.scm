(define-module (crates-io pi xe pixelast) #:use-module (crates-io))

(define-public crate-pixelast-0.1.0 (c (n "pixelast") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dnjpg8vmky2xzsq3ijsxav3pzpi55pwxn441cqydpcpf6fxc43l")))

