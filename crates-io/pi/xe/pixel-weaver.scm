(define-module (crates-io pi xe pixel-weaver) #:use-module (crates-io))

(define-public crate-pixel-weaver-0.1.0 (c (n "pixel-weaver") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.18.0") (f (quote ("swizzle"))) (d #t) (k 0)) (d (n "rusty-ppm") (r "^0.3.0") (d #t) (k 0)) (d (n "simple-canvas") (r "^0.1.1") (d #t) (k 0)))) (h "1nkp25cjp6iia591s8kmr7w6vc1szy4r4payiizwhv854pmq30n0")))

(define-public crate-pixel-weaver-0.1.1 (c (n "pixel-weaver") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.18.0") (f (quote ("swizzle"))) (d #t) (k 0)) (d (n "rusty-ppm") (r "^0.3.0") (d #t) (k 0)) (d (n "simple-canvas") (r "^0.1.1") (d #t) (k 0)))) (h "00nidlqaj0pf447dpm4a8xfrf62wx54481fhilvrwipfqxg4l0g8")))

(define-public crate-pixel-weaver-0.1.2 (c (n "pixel-weaver") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.18.0") (f (quote ("swizzle"))) (d #t) (k 0)) (d (n "rusty-ppm") (r "^0.3.0") (d #t) (k 0)) (d (n "simple-canvas") (r "^0.1.1") (d #t) (k 0)))) (h "0fc6diidsf6ypqs3sg2xnpf4zs6ahpgizq3k1j5h4qzwwmaqi6y4")))

