(define-module (crates-io pi xe pixel-game-lib-macros) #:use-module (crates-io))

(define-public crate-pixel-game-lib-macros-0.1.0 (c (n "pixel-game-lib-macros") (v "0.1.0") (d (list (d (n "litrs") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0krg8zhk5i65f2v4mps2zvllgwf0j41kv0004h53h2z0h9754pv6") (f (quote (("hot-reloading-assets")))) (r "1.77.0")))

