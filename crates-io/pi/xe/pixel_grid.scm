(define-module (crates-io pi xe pixel_grid) #:use-module (crates-io))

(define-public crate-pixel_grid-0.1.0 (c (n "pixel_grid") (v "0.1.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.26") (d #t) (k 0)) (d (n "glutin") (r "^0.18") (d #t) (k 0)) (d (n "grid_2d") (r "^0.11") (d #t) (k 0)))) (h "1fzs53wczlgfkx6rxf09cx9jwwzi2921kmxgcwb01pwqy1wsqv07")))

(define-public crate-pixel_grid-0.2.0 (c (n "pixel_grid") (v "0.2.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.26") (d #t) (k 0)) (d (n "glutin") (r "^0.18") (d #t) (k 0)) (d (n "grid_2d") (r "^0.11") (d #t) (k 0)))) (h "01zqdmgbr258qjsjz8bf1l4ndmp4r3x52isg2s5zcb26c1pmgh3s")))

(define-public crate-pixel_grid-0.3.0 (c (n "pixel_grid") (v "0.3.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.26") (d #t) (k 0)) (d (n "glutin") (r "^0.18") (d #t) (k 0)) (d (n "grid_2d") (r "^0.11") (d #t) (k 0)))) (h "1a9cdxh15ijhr87gi0mrid2k8bh9bbp0s1810ll88mxrs6k0z0sv")))

(define-public crate-pixel_grid-0.3.1 (c (n "pixel_grid") (v "0.3.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.28") (d #t) (k 0)) (d (n "glutin") (r "^0.19") (d #t) (k 0)) (d (n "grid_2d") (r "^0.11") (d #t) (k 0)))) (h "0brygp1sjksbn1kzhwajzh8n4h2pw43zaspw1mn1mygpngabla7m")))

(define-public crate-pixel_grid-0.4.0 (c (n "pixel_grid") (v "0.4.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.28") (d #t) (k 0)) (d (n "glutin") (r "^0.19") (d #t) (k 0)) (d (n "grid_2d") (r "^0.11") (d #t) (k 0)))) (h "173qs1xiaqpz1iw8bd4pbi89vlh0hmbjw29nfqpyzdia86qmcr5w")))

(define-public crate-pixel_grid-0.4.1 (c (n "pixel_grid") (v "0.4.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.28") (d #t) (k 0)) (d (n "glutin") (r "^0.19") (d #t) (k 0)) (d (n "grid_2d") (r "^0.12") (d #t) (k 0)))) (h "0wx0c7gjq3rrifmfrxbxg7vvkbv6qbqf0dw2vpylimnlgciwyyx1")))

(define-public crate-pixel_grid-0.5.0 (c (n "pixel_grid") (v "0.5.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.16") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.29") (d #t) (k 0)) (d (n "glutin") (r "^0.19") (d #t) (k 0)) (d (n "grid_2d") (r "^0.12") (d #t) (k 0)))) (h "0g04s9bppcx9yf55bdsp3qvldhn82jnwjv1q8sg1jwvdlhlmz5cs")))

