(define-module (crates-io pi xe pixel-sig) #:use-module (crates-io))

(define-public crate-pixel-sig-0.2.0 (c (n "pixel-sig") (v "0.2.0") (d (list (d (n "amcl_wrapper") (r "^0.3.5") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1yf4blwh6mlj1qf4zr3z9cfcr9gaf8jbb17ikvj3ggw9qc7y2730")))

(define-public crate-pixel-sig-0.3.0 (c (n "pixel-sig") (v "0.3.0") (d (list (d (n "amcl_wrapper") (r "^0.3.5") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1aqzxqpy2ww4csc87482hz19hfjvx69ifkw0d17853h7x23rjkwd") (f (quote (("default" "VerkeyG2") ("VerkeyG2") ("VerkeyG1"))))))

(define-public crate-pixel-sig-0.4.0 (c (n "pixel-sig") (v "0.4.0") (d (list (d (n "amcl_wrapper") (r "^0.3") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "secret_sharing") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "071f55zm4xq2imggx2rfzk7lzfl4nzlkz7ap2gh6frp2v8xjzxk2") (f (quote (("default" "VerkeyG2") ("VerkeyG2") ("VerkeyG1"))))))

