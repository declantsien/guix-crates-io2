(define-module (crates-io pi xe pixel_fight) #:use-module (crates-io))

(define-public crate-pixel_fight-0.1.0 (c (n "pixel_fight") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.50") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "speedy2d") (r "^1.8") (d #t) (k 0)))) (h "1lp15qnd7cqy1fxsn2lby7wrfg017zgq7hdxm1nva5s3gv1yvfad")))

