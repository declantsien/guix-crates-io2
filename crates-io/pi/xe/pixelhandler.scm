(define-module (crates-io pi xe pixelhandler) #:use-module (crates-io))

(define-public crate-PixelHandler-1.0.0 (c (n "PixelHandler") (v "1.0.0") (d (list (d (n "ggez") (r "^0.9.3") (d #t) (k 0)))) (h "1746sa8qhs4kh7ba6wm40bs5jb5zgmym0cs118yhcahm68vls68q") (y #t)))

(define-public crate-PixelHandler-1.0.1 (c (n "PixelHandler") (v "1.0.1") (d (list (d (n "ggez") (r "^0.9.3") (d #t) (k 0)))) (h "1p66nb8628gj35hzb6i580sw35rriq93l0a3ha2ykjcdcz3hwvzm") (y #t)))

