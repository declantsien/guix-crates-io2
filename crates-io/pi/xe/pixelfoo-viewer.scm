(define-module (crates-io pi xe pixelfoo-viewer) #:use-module (crates-io))

(define-public crate-pixelfoo-viewer-0.1.0 (c (n "pixelfoo-viewer") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("std" "help" "usage" "suggestions" "error-context" "derive"))) (k 0)) (d (n "piston") (r "^0.53.2") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.43.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.82.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.128.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.71.0") (d #t) (k 0)))) (h "0lyclfgz4qbp0g959wm45wpicinj4ir7cmhbi0knhhh3va6cpskl")))

(define-public crate-pixelfoo-viewer-0.1.1 (c (n "pixelfoo-viewer") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("std" "help" "usage" "suggestions" "error-context" "derive"))) (k 0)) (d (n "piston") (r "^0.53.2") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.43.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.82.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.128.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.71.0") (d #t) (k 0)))) (h "1dy783czgypsw71h7x0l8hcc8xvq9jid5qfih356j06rzh4x7i0r")))

