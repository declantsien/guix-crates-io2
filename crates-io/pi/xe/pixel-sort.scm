(define-module (crates-io pi xe pixel-sort) #:use-module (crates-io))

(define-public crate-pixel-sort-0.1.0 (c (n "pixel-sort") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "imageproc") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s24l5faicqamk1i3l9qcjwfw87xg434w0zd7mw38jb4wxhmxmyy") (f (quote (("default"))))))

