(define-module (crates-io pi xe pixel-canvas) #:use-module (crates-io))

(define-public crate-pixel-canvas-0.1.0 (c (n "pixel-canvas") (v "0.1.0") (d (list (d (n "glium") (r "^0.26.0-alpha3") (d #t) (k 0)))) (h "1zi6bf0gd6h7whgbj9n7qwz1q35m2k1gnygn704vpkkj6m70scgi")))

(define-public crate-pixel-canvas-0.2.0 (c (n "pixel-canvas") (v "0.2.0") (d (list (d (n "glium") (r "^0.26.0-alpha3") (d #t) (k 0)))) (h "1vjx17a6ns1v61dp9clvf4y0q43di79w8jnzj6s4ak4yim8xj1ai")))

(define-public crate-pixel-canvas-0.2.1 (c (n "pixel-canvas") (v "0.2.1") (d (list (d (n "glium") (r "^0.26.0") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.2") (d #t) (k 2)))) (h "0s7z8fn6r9qlh576h40paw9cgfwij4yfxisdcwmhal07vhq4r1bm")))

(define-public crate-pixel-canvas-0.2.2 (c (n "pixel-canvas") (v "0.2.2") (d (list (d (n "glium") (r "^0.28.0") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.4") (d #t) (k 2) (p "packed_simd_2")) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.2") (d #t) (k 2)))) (h "0v394pqpiij3gla3zdpgcqf8knzszmrmj5qanz3bn9bim2nsmbpc")))

(define-public crate-pixel-canvas-0.2.3 (c (n "pixel-canvas") (v "0.2.3") (d (list (d (n "glium") (r "^0.31.0") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.6") (d #t) (k 2) (p "packed_simd_2")) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "00i30xkz77vzcnpri242fmxja02zdabam4qqngg5b26yk6w903jk")))

