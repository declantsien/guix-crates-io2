(define-module (crates-io pi xe pixel-fix-rs) #:use-module (crates-io))

(define-public crate-pixel-fix-rs-0.1.0 (c (n "pixel-fix-rs") (v "0.1.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "spade") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zzibsif6b5bfnl77j8rg5v812wgdfzfawfm70lf1kijpia14h8v") (r "1.72")))

