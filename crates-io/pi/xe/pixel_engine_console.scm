(define-module (crates-io pi xe pixel_engine_console) #:use-module (crates-io))

(define-public crate-pixel_engine_console-0.1.0 (c (n "pixel_engine_console") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "pixel_engine") (r "^0.8.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("unicode-linebreak"))) (k 0)))) (h "0nbvbhzxbgqxb1jlwzs11dszw7i1wlwxfv4802k14f9pj2qb30vr") (f (quote (("warn") ("trace") ("off") ("info") ("error") ("default" "info") ("debug"))))))

