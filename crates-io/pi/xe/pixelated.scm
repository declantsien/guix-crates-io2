(define-module (crates-io pi xe pixelated) #:use-module (crates-io))

(define-public crate-pixelated-0.1.0 (c (n "pixelated") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.15") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)))) (h "0ffwpw4316mk630prl8px9g5xbs4cm0m375sx1yf88p5wqcm2imw") (r "1.76.0")))

(define-public crate-pixelated-0.1.1 (c (n "pixelated") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.15") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)))) (h "1l2q4m4c9c5x91r00ifk3qq36xg0m5qkry859h6slbxkywjn8sln") (r "1.76.0")))

(define-public crate-pixelated-0.2.0 (c (n "pixelated") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 0)))) (h "08hf3b6k8yh72xd3gz049h8ywkn3j4f3dii5lm6swnpcqg1qzw4j") (r "1.76.0")))

