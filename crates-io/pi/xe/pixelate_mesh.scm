(define-module (crates-io pi xe pixelate_mesh) #:use-module (crates-io))

(define-public crate-pixelate_mesh-0.1.0 (c (n "pixelate_mesh") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_pbr" "bevy_sprite" "bevy_ui" "bevy_gltf" "png"))) (k 0)))) (h "0b26yf47aqrpn2pnl4b8avlkka5k4wip36w0j8j4f24lbdca3v84")))

(define-public crate-pixelate_mesh-0.2.0 (c (n "pixelate_mesh") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_pbr" "bevy_sprite" "bevy_ui" "bevy_gltf" "png" "tonemapping_luts"))) (k 0)))) (h "0g3kxkv8pkpf9kyng1lvw477p17zn06yrfwwr31yjzphmsj1csdh")))

(define-public crate-pixelate_mesh-0.3.0 (c (n "pixelate_mesh") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_pbr" "bevy_sprite" "bevy_ui" "bevy_gltf" "png" "tonemapping_luts"))) (k 0)))) (h "0x5ankq7819xlzj3pq1fxxyg6wmwgqj8x1pkvdz8dfgcwa3h4n0m")))

