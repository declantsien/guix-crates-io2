(define-module (crates-io pi xe pixel) #:use-module (crates-io))

(define-public crate-pixel-0.1.0 (c (n "pixel") (v "0.1.0") (h "0mfpmk8l9145zd2ld1dlsbfvkhg6jhh5mhwdrx896c4bakrmvis0")))

(define-public crate-pixel-0.1.1 (c (n "pixel") (v "0.1.1") (h "0cdgl82frxbrfij0py27q9im6fhjw60wpzbr0sbmvv22nnzdzlbm")))

(define-public crate-pixel-0.1.2 (c (n "pixel") (v "0.1.2") (h "1mx8yanvzxwpx3qgg4dpp9lsgc1xzr04nfzinya8k4kb5449abwj")))

(define-public crate-pixel-0.1.3 (c (n "pixel") (v "0.1.3") (h "1xl4fv3ssv6c2nlni7fflyfw9bnyx2zx513zaprd7yfwhrzakpp2")))

