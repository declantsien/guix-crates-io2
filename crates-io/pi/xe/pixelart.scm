(define-module (crates-io pi xe pixelart) #:use-module (crates-io))

(define-public crate-pixelart-0.1.0 (c (n "pixelart") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "imageproc") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0h91223x88fy54xs78g2jzvh7fr4wyr7v6qq92xnk1xbqb9l1xis")))

(define-public crate-pixelart-0.1.1 (c (n "pixelart") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "imageproc") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "1wk5wgyraqq49vrdndy1d4y0hfa1wb143r2qn460frzhmr6mqrwi")))

