(define-module (crates-io pi xe pixels_primitives) #:use-module (crates-io))

(define-public crate-pixels_primitives-0.1.0 (c (n "pixels_primitives") (v "0.1.0") (d (list (d (n "pixels") (r "^0.9.0") (d #t) (k 2)) (d (n "winit") (r "^0.26.1") (d #t) (k 2)) (d (n "winit_input_helper") (r "^0.12.0") (d #t) (k 2)))) (h "0q6y9l38x7gng29dab2gx79az5xcfc10q5j75zcmbipr4zs9f1hi")))

(define-public crate-pixels_primitives-0.1.1 (c (n "pixels_primitives") (v "0.1.1") (d (list (d (n "pixels") (r "^0.9.0") (d #t) (k 2)) (d (n "winit") (r "^0.26.1") (d #t) (k 2)) (d (n "winit_input_helper") (r "^0.12.0") (d #t) (k 2)))) (h "1gyb7fphi3nczvid7w04630qxwh2wf3555kvz4gyj0869i8xh92k")))

