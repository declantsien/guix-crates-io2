(define-module (crates-io pi xe pixel_map) #:use-module (crates-io))

(define-public crate-pixel_map-0.1.0 (c (n "pixel_map") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1jbciy40lj4xdw3vf48hmmlk7zw2n67pg651k7bb7zvl3dk44rgk")))

(define-public crate-pixel_map-0.2.0 (c (n "pixel_map") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (o #t) (k 0)))) (h "12m4b704asc2kx1py4qh164hs396pvcl28yq5yn1rz4xs360gc3j") (s 2) (e (quote (("serde" "dep:serde" "glam/serde"))))))

(define-public crate-pixel_map-0.3.0 (c (n "pixel_map") (v "0.3.0") (d (list (d (n "bevy_math") (r "^0.11.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (o #t) (k 0)))) (h "0yw2fsyba11pb8ffqb3x26a1lviy0awvda9f3q5j0j7vkpaclnx4") (s 2) (e (quote (("serialize" "dep:serde" "bevy_math/serialize"))))))

