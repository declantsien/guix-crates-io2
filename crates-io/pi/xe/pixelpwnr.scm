(define-module (crates-io pi xe pixelpwnr) #:use-module (crates-io))

(define-public crate-pixelpwnr-0.0.1 (c (n "pixelpwnr") (v "0.0.1") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0ibibg5lh1jqvjsma3jjj3mwjmg498qbiv58ffkw3iwjakxsa30f")))

