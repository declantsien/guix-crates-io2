(define-module (crates-io pi xe pixels-u32) #:use-module (crates-io))

(define-public crate-pixels-u32-0.1.0 (c (n "pixels-u32") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 2)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "pixels") (r "^0.7.0") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 2)))) (h "06agyb3lr7p1ldkq179p9jx1pm9lwagbb614l1ixncah4q4kcqh4")))

(define-public crate-pixels-u32-0.2.0 (c (n "pixels-u32") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "pixels") (r "^0.8.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.0") (d #t) (k 2)))) (h "10qqrgw7385qgqd132fwv2yy2nkada6cmfsn03skzhjaizzjn7n2")))

(define-public crate-pixels-u32-0.3.0 (c (n "pixels-u32") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "bytemuck") (r "^1.8.0") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 2)))) (h "11375db0w0kqhlwh7p45si4ggpaiasjcp0kf97pbhc0jf8f1cs6x")))

