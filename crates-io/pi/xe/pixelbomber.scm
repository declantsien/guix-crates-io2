(define-module (crates-io pi xe pixelbomber) #:use-module (crates-io))

(define-public crate-pixelbomber-0.1.0 (c (n "pixelbomber") (v "0.1.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0yqq403m3ir87qkp6r8imkn08q67fgz9w73grybgq1d87699kifp")))

(define-public crate-pixelbomber-0.1.1 (c (n "pixelbomber") (v "0.1.1") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0yn36nkpaanrknh2cil5w55qxqc8jzj0aq2dm0fx0sgwkaqn6d44")))

(define-public crate-pixelbomber-0.1.2 (c (n "pixelbomber") (v "0.1.2") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1a7ijxm54q3sp9smk3104gc1didvzkwbrrxwxa2q9byi7l5d70h7")))

(define-public crate-pixelbomber-0.2.0 (c (n "pixelbomber") (v "0.2.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "020sgz9qw5jap9yfhcr2wrkk6s5snhdbd40sa1grfsh692n4w2fi")))

(define-public crate-pixelbomber-0.2.1 (c (n "pixelbomber") (v "0.2.1") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0dq7ji7nsm2f487ydf695qka89zh286ri1bkh35mh8m2ymsbc9av")))

(define-public crate-pixelbomber-0.2.2 (c (n "pixelbomber") (v "0.2.2") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1j7gd5fdawkb1dhvfzqy2hyahp8f6453p4vqgpnd1dmywkwb0plc")))

(define-public crate-pixelbomber-0.2.3 (c (n "pixelbomber") (v "0.2.3") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1m75jg6ircvyr7gp064l6k0ahfxqas3ky56k3fdmcqwnlj35hywh")))

(define-public crate-pixelbomber-0.3.0 (c (n "pixelbomber") (v "0.3.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "15c78cp2mranqn9r93q6q021aq4d1nckg7hcrwpwsqqvirzm0vvl")))

(define-public crate-pixelbomber-0.4.0 (c (n "pixelbomber") (v "0.4.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "14dj9016skw7y8sas0afbvnwbfpi090h65239xmn0zpl9yxiw1kh")))

(define-public crate-pixelbomber-0.4.1 (c (n "pixelbomber") (v "0.4.1") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "09z2pisq3jkdbj1fpcwv0s97lcwm7nsjyhbn2007y2jy7xsr6p51")))

(define-public crate-pixelbomber-0.5.0 (c (n "pixelbomber") (v "0.5.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0vffh3lx6pmy5cjr3292lg2rxk1jpk3kisbd7zw45vcp4hy80112")))

(define-public crate-pixelbomber-0.5.1 (c (n "pixelbomber") (v "0.5.1") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1a2fjhanm16hia0v99cwzwxalcz7964nrazn3yf65fnizxi7zgxi")))

(define-public crate-pixelbomber-0.5.2 (c (n "pixelbomber") (v "0.5.2") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1as5k1bcbgnqffmp8b84q5dm0ar3c1jyfiimbwz1ib4cb41ljlbb")))

(define-public crate-pixelbomber-0.5.3 (c (n "pixelbomber") (v "0.5.3") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0j9b7r0mv06gdri5fa86ms08ckxq87nza61gh5mqxizrk8cwmy0j")))

(define-public crate-pixelbomber-0.5.4 (c (n "pixelbomber") (v "0.5.4") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0da3nj00y5qwjcscs2jiw2250p6n9970ap43n6chr9xj1zbwc5qz")))

(define-public crate-pixelbomber-0.5.5 (c (n "pixelbomber") (v "0.5.5") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1zh8n2b8fmgz3j6j27i3zill1kx6yavk7875q9s9xpdjc6vbpj9b")))

(define-public crate-pixelbomber-0.5.6 (c (n "pixelbomber") (v "0.5.6") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0z2cc0d5jis572qf19sj4dhw2q4ilk9317pnxbcqdrn38y9rjjs9")))

(define-public crate-pixelbomber-0.5.7 (c (n "pixelbomber") (v "0.5.7") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1v4plzq01qw8dbwgf3f8hrc7r03k2f1v6nkl45x7wgnflivwq9ir")))

(define-public crate-pixelbomber-0.6.0 (c (n "pixelbomber") (v "0.6.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0539yi6mv08p93y28fv0704cn09scbd9vd66q7dsprankhnkr8x7")))

