(define-module (crates-io pi xe pixel_engine_draw) #:use-module (crates-io))

(define-public crate-pixel_engine_draw-0.1.0 (c (n "pixel_engine_draw") (v "0.1.0") (d (list (d (n "image") (r "^0.23.5") (d #t) (k 0)))) (h "109waz0f4878s4m8zcz6nh5nc3dc43p534n3q3dhx6zf62qshzy8")))

(define-public crate-pixel_engine_draw-0.2.0 (c (n "pixel_engine_draw") (v "0.2.0") (d (list (d (n "image") (r "^0.23.5") (d #t) (k 0)))) (h "1spyr8ba1wz6nf0gwmqjmmn7cg2s5i6x0d69gjq4yx1hxy7wjjg7")))

(define-public crate-pixel_engine_draw-0.3.0 (c (n "pixel_engine_draw") (v "0.3.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0fwz2iai4kwlhs6nf8wmx00nclwzrvhq7rf8bzjbm4y10id0mqcz")))

(define-public crate-pixel_engine_draw-0.4.0 (c (n "pixel_engine_draw") (v "0.4.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "13vrf878j7fv24rvb1z538vwp5rhv0896b1nn4x03b21ypb046ci")))

(define-public crate-pixel_engine_draw-0.5.1 (c (n "pixel_engine_draw") (v "0.5.1") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (f (quote ("parking_lot" "std" "alloc"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "slab") (r "^0.4.6") (d #t) (k 0)))) (h "051frkg6w7vg5wn8d2yzvic3227l6hiag5y81h0m1n868iyjwl71")))

(define-public crate-pixel_engine_draw-0.5.2 (c (n "pixel_engine_draw") (v "0.5.2") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (f (quote ("parking_lot" "std" "alloc"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "slab") (r "^0.4.6") (d #t) (k 0)))) (h "1ynv6rml6wdylayfnhid29r5j7gd6854s8ka4sfrn5rsz9by57n1")))

(define-public crate-pixel_engine_draw-0.5.3 (c (n "pixel_engine_draw") (v "0.5.3") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (f (quote ("parking_lot" "std" "alloc"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "slab") (r "^0.4.7") (d #t) (k 0)))) (h "1ax3a8r04708ihznsjqq1vy1x2m7qgfcy8vs8i981rf1vzybh28n")))

(define-public crate-pixel_engine_draw-0.5.4 (c (n "pixel_engine_draw") (v "0.5.4") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (f (quote ("parking_lot" "std" "alloc"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "slab") (r "^0.4.7") (d #t) (k 0)))) (h "13nl5d58978hz1rvszfk7r7iici61bz2fk0f2a762xq2ss94nclk")))

(define-public crate-pixel_engine_draw-0.7.0 (c (n "pixel_engine_draw") (v "0.7.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (f (quote ("parking_lot" "std" "alloc"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "slab") (r "^0.4.7") (d #t) (k 0)))) (h "0jyx74hv2qvajckmyk9vrbsiaiwz9q0znvc4gas78aqvzkzwgwqz")))

