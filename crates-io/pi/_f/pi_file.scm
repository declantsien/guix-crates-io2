(define-module (crates-io pi _f pi_file) #:use-module (crates-io))

(define-public crate-pi_file-0.1.0 (c (n "pi_file") (v "0.1.0") (d (list (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "npnc") (r "^0.2") (d #t) (k 0)) (d (n "pi_atom") (r "^0.1") (d #t) (k 0)))) (h "1qz8n99n0786f65rr2k2yq1zz8jbpn3959331wh0sjvwwv3p7cvm")))

(define-public crate-pi_file-0.2.0 (c (n "pi_file") (v "0.2.0") (d (list (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "npnc") (r "^0.2") (d #t) (k 0)) (d (n "pi_atom") (r "^0.2") (d #t) (k 0)))) (h "18l82mxdi003dj7941fqjh9csfnzdwwgcbiq2jgjgc32q6zy76x1")))

(define-public crate-pi_file-0.2.1 (c (n "pi_file") (v "0.2.1") (d (list (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "npnc") (r "^0.2") (d #t) (k 0)) (d (n "pi_atom") (r "^0.5") (d #t) (k 0)))) (h "10ddwkaf90dq1pw77qkgkzk2f45pk8hayc81clmy6wlha4w9gqib")))

