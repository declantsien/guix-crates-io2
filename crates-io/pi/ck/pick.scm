(define-module (crates-io pi ck pick) #:use-module (crates-io))

(define-public crate-pick-0.1.0 (c (n "pick") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0cdh9jmyj5b7bhls449gvq1s016xl3km5l1c6mwy8azlklrir88c")))

(define-public crate-pick-0.1.1 (c (n "pick") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1dv5s4v3hlyv73afpgpkvdrc2chk9k2155s85l1la9smkaj38h0c")))

