(define-module (crates-io pi ck picker) #:use-module (crates-io))

(define-public crate-picker-0.1.0 (c (n "picker") (v "0.1.0") (d (list (d (n "nativefiledialog-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0p8vkb4aaj3z8nrgizfpdnfkhcmg3zi4136ph8av1n1226zxrz4c")))

(define-public crate-picker-0.1.1 (c (n "picker") (v "0.1.1") (d (list (d (n "nativefiledialog-sys") (r "^0.1.0") (d #t) (k 0)))) (h "103q1d706xh8wz89lk8407aga19jzx03sl7lm8b7a3g5mi04vn22")))

