(define-module (crates-io pi ck pickasay) #:use-module (crates-io))

(define-public crate-pickasay-0.1.0 (c (n "pickasay") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "170m4nd275awnlsxklv6mahyxs04p70adjx65s9w4x6dznpqr72j")))

