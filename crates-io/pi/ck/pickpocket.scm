(define-module (crates-io pi ck pickpocket) #:use-module (crates-io))

(define-public crate-pickpocket-0.1.0 (c (n "pickpocket") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1pi81fd62nggsnmrkmijbwblrw3ncyp9nhi97wd4s53p3r0g6wzz")))

(define-public crate-pickpocket-0.1.1 (c (n "pickpocket") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0f03agnrpbn4r7wfvyig1hzkql1sh5lyll3mj3s5qav002jr0r5v")))

