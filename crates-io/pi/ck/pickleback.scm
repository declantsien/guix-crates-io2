(define-module (crates-io pi ck pickleback) #:use-module (crates-io))

(define-public crate-pickleback-0.1.0 (c (n "pickleback") (v "0.1.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "lifeguard") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "smallmap") (r "^1.4.2") (d #t) (k 0)))) (h "1k32v6g5wvwb3r8gqg6vy5r7l0h6f89alzmd1zw6sw900a7m91gm")))

