(define-module (crates-io pi ra pirates_macro_lib) #:use-module (crates-io))

(define-public crate-pirates_macro_lib-0.1.0 (c (n "pirates_macro_lib") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0kgdksjicp5j6008w4virkhjmxb2fjdf0wmw0rghwvvzzx9sjvqs")))

