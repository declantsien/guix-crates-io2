(define-module (crates-io pi ra pirate) #:use-module (crates-io))

(define-public crate-pirate-0.1.0 (c (n "pirate") (v "0.1.0") (h "18g9dcw4966g6js5bm66j307nyrvki626pfn6pza4mga6apg7p50")))

(define-public crate-pirate-0.2.0 (c (n "pirate") (v "0.2.0") (h "139z5hzginnfwd8hrp9f7w7v1kxx0kybkb7qn7qyhyy2v3a5q66x")))

(define-public crate-pirate-1.0.0 (c (n "pirate") (v "1.0.0") (h "0kvdjszm5q1azlbhjh52hgwdbizc2vdrv9jyi913zsgiq4x6va49")))

