(define-module (crates-io pi nk pinky-swear) #:use-module (crates-io))

(define-public crate-pinky-swear-0.0.0 (c (n "pinky-swear") (v "0.0.0") (h "1fdvb348yprazbxnnpj9l5n0i1fgvj2kw41pfw1hh9vdc04qsjfw")))

(define-public crate-pinky-swear-0.1.0 (c (n "pinky-swear") (v "0.1.0") (d (list (d (n "futures") (r "= 0.3.0-alpha.19") (o #t) (d #t) (k 0) (p "futures-core-preview")) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "0yspjnivfk7l550ki37jy76g50f0dwhwkinfisbgrajj8nmg4qca") (y #t)))

(define-public crate-pinky-swear-0.1.1 (c (n "pinky-swear") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "1a2kz4gc84b3hnq57am72p0qr94y3jd24sa16iwmi0njahp1lwlr") (f (quote (("futures")))) (y #t)))

(define-public crate-pinky-swear-0.2.0 (c (n "pinky-swear") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "1g8a6fyj8k3af32p88gzyliih7h6fdjbrhn7y5wrv0m816gyb9bk") (y #t)))

(define-public crate-pinky-swear-0.2.1 (c (n "pinky-swear") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "0kx768n3wlk3fdi8kpywca57ncq8y73m434lqs5153a7a80wcgvn") (y #t)))

(define-public crate-pinky-swear-0.2.2 (c (n "pinky-swear") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "03l5cljf0nv3b4bkivn7ck1z61qp68mrvz2x30na9jb7pvwpn9si") (y #t)))

(define-public crate-pinky-swear-0.3.0 (c (n "pinky-swear") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "1j1mr6ja0drbl543nac1iz40sb17i30d1pnlqrv541rfxaia2ksb") (y #t)))

(define-public crate-pinky-swear-0.3.1 (c (n "pinky-swear") (v "0.3.1") (d (list (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "08ivipkcn8zwrfzr9z2xsnbmmi3cbz3jqqz6gymnk06yh87wc3r4") (y #t)))

(define-public crate-pinky-swear-0.4.0 (c (n "pinky-swear") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "1zb3j4wzr8x3r4z0a9j5q5k2fhx62h471sq9yxp9wwik6ggcj8s5") (y #t)))

(define-public crate-pinky-swear-0.4.1 (c (n "pinky-swear") (v "0.4.1") (d (list (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "0zrzl29dclalncymy3w52f9jhxfxgv25kfzzzz912n3zxzayxbjr") (y #t)))

(define-public crate-pinky-swear-0.4.2 (c (n "pinky-swear") (v "0.4.2") (d (list (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "0j5h3nd49wryddn7lbw16ncmnnz3q8704v1q1j4ilx1imjqmhkqn")))

(define-public crate-pinky-swear-1.0.0 (c (n "pinky-swear") (v "1.0.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "040dzplchv85bca27yl09a799rhrckyq12sjhqgs21x0mq6g1bi7")))

(define-public crate-pinky-swear-1.1.0 (c (n "pinky-swear") (v "1.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0paajcdcdcnb9l9znw0hhqp0dv2bjsmp0g7hqlmqwdxcc51964vk")))

(define-public crate-pinky-swear-1.1.1 (c (n "pinky-swear") (v "1.1.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0lnpvb2dw9r0skh2s77yk5y74ph4xwdzzgq1rbdqlv4ficzvrkgl")))

(define-public crate-pinky-swear-1.1.2 (c (n "pinky-swear") (v "1.1.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1dva178bsaxijj4gq84j5dqz26aags51krabm1ihysf4wkk7rnqh")))

(define-public crate-pinky-swear-1.1.3 (c (n "pinky-swear") (v "1.1.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1gfsg0jqi72wx4xzyglxc2axg5r2fj6m9bqipfyj7x4z4bidjnwr")))

(define-public crate-pinky-swear-1.2.0 (c (n "pinky-swear") (v "1.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "024zhnc1srb43niimdnpz08q89pg6yphf7zr1mcyr214sapqi5gs") (y #t)))

(define-public crate-pinky-swear-1.3.0 (c (n "pinky-swear") (v "1.3.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0zjnz3w9qds4bcd8yl2lhkzbayrs009zq65q45sw4mgpi509ph48")))

(define-public crate-pinky-swear-1.3.1 (c (n "pinky-swear") (v "1.3.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0j06qi92iaranzhpj66b3xrnaakarfnpgr3bw3ypfvnbfnksppfg")))

(define-public crate-pinky-swear-1.4.0 (c (n "pinky-swear") (v "1.4.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0j4p9bq96jcc45gaz129isk74a7s65888y3p0c0mnjwx70861fkl")))

(define-public crate-pinky-swear-1.5.0 (c (n "pinky-swear") (v "1.5.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1jgzpqzd7gb1vzbafjmqj72knlhddkqya2qc8fn9xd7n241l12nd")))

(define-public crate-pinky-swear-1.6.0 (c (n "pinky-swear") (v "1.6.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1qifg2a2ciiqiqjcri3s9q3d14zn4042c04gd81dlmnigz3xqh30")))

(define-public crate-pinky-swear-1.6.1 (c (n "pinky-swear") (v "1.6.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0ilqg32892dr34s8x2i8fm83z7v178qjks1lpd9qzv3yhbq96jlh")))

(define-public crate-pinky-swear-1.7.0 (c (n "pinky-swear") (v "1.7.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1rq595vk8g1mm5y6brb0gpxmx61wm2gwfimac5sv17amk161vn7b")))

(define-public crate-pinky-swear-2.0.0 (c (n "pinky-swear") (v "2.0.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "03zxs5zwkbzb748c76pi1grqdni42jsh796mb1w716f9qaia2wji")))

(define-public crate-pinky-swear-2.1.0 (c (n "pinky-swear") (v "2.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1aw9906f4z1y6rp7ypjpd308rwz0cmavk392jb8bk9na5df5b3hn")))

(define-public crate-pinky-swear-2.2.0 (c (n "pinky-swear") (v "2.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1m8f21zjzqhghhgafimpq92pca4qsvsznrgc1zq3vl9dyazv6gs7")))

(define-public crate-pinky-swear-2.3.0 (c (n "pinky-swear") (v "2.3.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0gl1b2hz327i9l0x91hyj26fk0mqwdgyh3yww6zf0jvvbkndzm3x")))

(define-public crate-pinky-swear-2.3.1 (c (n "pinky-swear") (v "2.3.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0391wljlcvl7y3d5dg603xiyii5v15mzd85qdkf51z5z33rswrzb")))

(define-public crate-pinky-swear-3.0.0 (c (n "pinky-swear") (v "3.0.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1qyal600sqw5lprc8x456sr5b8nzq8pwlx0iqfbk26npz8zfk425")))

(define-public crate-pinky-swear-4.0.0 (c (n "pinky-swear") (v "4.0.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "012md12m4v1n9ffjq4q1r3aj0aynm86ssshb7bmin598z96hc7q7")))

(define-public crate-pinky-swear-4.0.1 (c (n "pinky-swear") (v "4.0.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1pyvxm1dy1ssjh06is400wmmg3anj6mh877z4xxvgsqjjjw2c7jw")))

(define-public crate-pinky-swear-4.1.0 (c (n "pinky-swear") (v "4.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0a91h878rh9n6yda7khwf0k3w4zd7c8fv5v33ylshpyihx5pzmv9")))

(define-public crate-pinky-swear-4.2.0 (c (n "pinky-swear") (v "4.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "02xhjk186ixa9a3xcdnwx3xwwyr06cwwmw00ricvp66lc038gbpb")))

(define-public crate-pinky-swear-4.2.1 (c (n "pinky-swear") (v "4.2.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1i72ywbb192c9z66gqjhkzkajbg8cjsiixcd5625cfrrs15w05lp")))

(define-public crate-pinky-swear-4.2.2 (c (n "pinky-swear") (v "4.2.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1d7mnfmgdk750cbrfncxq0vscsw0rfa52l3zqx8ncg1ads75xvxi")))

(define-public crate-pinky-swear-4.2.3 (c (n "pinky-swear") (v "4.2.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "08swl2yhwvqb1j3nay4n13aa2ryd0pijc46hx03gmhhm5xpj1mzs")))

(define-public crate-pinky-swear-4.3.0 (c (n "pinky-swear") (v "4.3.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0iz0j4cjkdia3qkrymija9saykwbmkxpq7kakg8fyl1jg1a8jj51")))

(define-public crate-pinky-swear-4.3.1 (c (n "pinky-swear") (v "4.3.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0ahgkw3sd7qc8im7qhb8v9p54sacyxrf2pmyn90xsbyik65wzra0")))

(define-public crate-pinky-swear-4.3.2 (c (n "pinky-swear") (v "4.3.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "184qk7mbvx7qkcxhckqnaqndhfdmxxq0xd2jgfbr9sgxchvkry0f") (y #t)))

(define-public crate-pinky-swear-4.3.3 (c (n "pinky-swear") (v "4.3.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "11n5ci2hvqpw0czj1rg7ykl13fhrylnlgb38w2w9754gc2shaywg")))

(define-public crate-pinky-swear-4.4.0 (c (n "pinky-swear") (v "4.4.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1hadqhfbwhn78rlqn20yka9bg7n51blkqkjfccw06l71z2kcvy4v")))

(define-public crate-pinky-swear-5.0.0 (c (n "pinky-swear") (v "5.0.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "flume") (r "^0.9") (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1n8nvpqq5kflf2qpbphfch4mywqb227v5d33j494a4xsswn0d5ny") (y #t)))

(define-public crate-pinky-swear-5.0.1 (c (n "pinky-swear") (v "5.0.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "flume") (r "^0.9") (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0nwiq2lfxrm28vck13h27sjjnl4fig7hn7mdzl60vxkrn3sid8ny")))

(define-public crate-pinky-swear-5.1.0 (c (n "pinky-swear") (v "5.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "flume") (r "^0.10") (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "03cx1jbvjhy00hf7520wxgfd9833lmap5l8kz7sh8ip2p07kdc3p")))

(define-public crate-pinky-swear-5.2.0 (c (n "pinky-swear") (v "5.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "flume") (r "^0.10") (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0a4jbgzw06d8vq3ky5dr7clbl99sa0w0nqcjgbf0xv02vjjr97is") (r "1.56.0")))

(define-public crate-pinky-swear-5.2.1 (c (n "pinky-swear") (v "5.2.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "flume") (r "^0.10") (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0vmrvcwn1jjx9zdr7mcb6446mzgv0nf2jlcfp4375i2iw883r98w") (r "1.56.0")))

(define-public crate-pinky-swear-6.0.0 (c (n "pinky-swear") (v "6.0.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "flume") (r "^0.10") (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "0p1dycqslcm3yv0vzv7l5i1gv445p7chr2sc6rsasjsypnndjrv3") (r "1.56.0")))

(define-public crate-pinky-swear-6.1.0 (c (n "pinky-swear") (v "6.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "flume") (r "^0.10") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1b7j4yd75ix3xkl231swzakdlkk0ih3ljlz8nmfjkgx4lxxbd56q") (r "1.56.0")))

(define-public crate-pinky-swear-4.5.0 (c (n "pinky-swear") (v "4.5.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "1a4i89kckwc9v0dkavmmkz9lg16l1wc9gq4m8xmmi1gswkay7bf5")))

(define-public crate-pinky-swear-6.2.0 (c (n "pinky-swear") (v "6.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "flume") (r "^0.11") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (k 0)))) (h "089apchwf54sjcvgigf906ivzlrqchkbslhid0d0bjhkskmf7ykc") (r "1.56.0")))

