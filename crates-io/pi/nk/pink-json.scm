(define-module (crates-io pi nk pink-json) #:use-module (crates-io))

(define-public crate-pink-json-0.4.0 (c (n "pink-json") (v "0.4.0") (d (list (d (n "ryu") (r "^1.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "0cjjgkprvy7c5da7bysd26hjbvz8g6hz4pi5sfizdd6h502h2in1") (f (quote (("std" "serde/std") ("default" "custom-error-messages") ("custom-error-messages"))))))

