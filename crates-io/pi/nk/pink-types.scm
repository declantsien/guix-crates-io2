(define-module (crates-io pi nk pink-types) #:use-module (crates-io))

(define-public crate-pink-types-0.1.0 (c (n "pink-types") (v "0.1.0") (d (list (d (n "scale") (r "^3.3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.3") (f (quote ("derive"))) (o #t) (k 0)))) (h "0acl82dh4ybqxcy7g5a3s1s04h3gw4v8h9z1c3jwy1dmscbcxgqc") (f (quote (("std" "scale/std" "scale-info/std") ("default" "scale/std"))))))

(define-public crate-pink-types-0.1.2-dev.0 (c (n "pink-types") (v "0.1.2-dev.0") (d (list (d (n "scale") (r "^3.3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.3") (f (quote ("derive"))) (k 0)))) (h "0ry0xhd9cq9gkhiy7wzcnkrrn3h4qv825hp3jcfdgjabqgrbmxcq") (f (quote (("std" "scale/std" "scale-info/std") ("default" "scale/std"))))))

(define-public crate-pink-types-0.1.2 (c (n "pink-types") (v "0.1.2") (d (list (d (n "scale") (r "^3.3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.3") (f (quote ("derive"))) (k 0)))) (h "16vi6mda2llfnnm4xamggjmsx7a55lfss9k73jwcbdhpqw9pc4pz") (f (quote (("std" "scale/std" "scale-info/std") ("default" "scale/std"))))))

