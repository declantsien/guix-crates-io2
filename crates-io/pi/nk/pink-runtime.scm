(define-module (crates-io pi nk pink-runtime) #:use-module (crates-io))

(define-public crate-pink-runtime-0.1.0 (c (n "pink-runtime") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "rustyline") (r "^10.1.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0yxr0nzyp05f0lxvh4ivzgb75pgk9d3j5mbl1ymwc519jfpwx881")))

(define-public crate-pink-runtime-0.1.1 (c (n "pink-runtime") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (f (quote ("pattern"))) (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "rustyline") (r "^10.1.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "036b036h6fva968c2mrbh1bwwwq1ky084gngmaf2k6djdhyz848z")))

