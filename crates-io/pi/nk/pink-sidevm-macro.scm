(define-module (crates-io pi nk pink-sidevm-macro) #:use-module (crates-io))

(define-public crate-pink-sidevm-macro-0.1.0 (c (n "pink-sidevm-macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "insta") (r "^1.14.0") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt-snippet") (r "^0.1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1izhpqy6cmzb6fknwd2j8kkijnic36nh12710w9fg2kznmshpr4d")))

(define-public crate-pink-sidevm-macro-0.1.1 (c (n "pink-sidevm-macro") (v "0.1.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "insta") (r "^1.14.0") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt-snippet") (r "^0.1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1qzyz5qki43nfjf3y7sn75i9bvnimbhmi77k13c38hmqjlzr2rah")))

