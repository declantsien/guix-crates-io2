(define-module (crates-io pi nk pink-kv-session) #:use-module (crates-io))

(define-public crate-pink-kv-session-0.1.0 (c (n "pink-kv-session") (v "0.1.0") (d (list (d (n "scale") (r "^3") (d #t) (k 2) (p "parity-scale-codec")))) (h "0cbbsbybp859kzsnfl98a21kbw91qlgisf24km3v10gvzk47dpn2")))

(define-public crate-pink-kv-session-0.2.0 (c (n "pink-kv-session") (v "0.2.0") (d (list (d (n "scale") (r "^3") (d #t) (k 2) (p "parity-scale-codec")))) (h "147pbjspq94s16h9rq1zk65xcj11rs4579b3wmdaq8pff3k9b99x")))

