(define-module (crates-io pi nk pink_log) #:use-module (crates-io))

(define-public crate-pink_log-0.1.0 (c (n "pink_log") (v "0.1.0") (d (list (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1zfgl1z957610mkck17bjrd42x1hasq2q5y2wiifiq0xysvrjcbv") (y #t)))

(define-public crate-pink_log-0.1.1 (c (n "pink_log") (v "0.1.1") (d (list (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0bxyiqdi7idr7clrq7kw0axwhbzlaibs52lai9wwp6g1m5yjix41")))

