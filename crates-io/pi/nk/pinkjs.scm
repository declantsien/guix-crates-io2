(define-module (crates-io pi nk pinkjs) #:use-module (crates-io))

(define-public crate-pinkjs-0.1.0 (c (n "pinkjs") (v "0.1.0") (d (list (d (n "drink") (r "^0.8.5") (d #t) (k 0)) (d (n "drink-pink-runtime") (r "^1.2.13") (d #t) (k 0)) (d (n "hex_fmt") (r "^0.3.0") (d #t) (k 0)) (d (n "ink") (r "^4.3.0") (d #t) (k 0)) (d (n "phat_js") (r "^0.2.8") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "1f7r66biz8kgfv8q462jwrn3isf4gn5ha0iindvgy00c3zyxvvqq")))

