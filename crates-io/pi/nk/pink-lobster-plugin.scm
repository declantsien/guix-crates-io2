(define-module (crates-io pi nk pink-lobster-plugin) #:use-module (crates-io))

(define-public crate-pink-lobster-plugin-0.1.0 (c (n "pink-lobster-plugin") (v "0.1.0") (d (list (d (n "pink-lobster") (r "^0.1.0") (d #t) (k 0)))) (h "046q9alh2201yd8xkazqsfpl6722drm24bkancy8105nvzypc9sd")))

(define-public crate-pink-lobster-plugin-0.1.1 (c (n "pink-lobster-plugin") (v "0.1.1") (d (list (d (n "pink-lobster") (r "^0.1.0") (d #t) (k 0)))) (h "0as5arw1c2pw0h3ng9nd74rmhpg20n2na3hlqqqswdcchi9h23pm")))

(define-public crate-pink-lobster-plugin-0.1.2 (c (n "pink-lobster-plugin") (v "0.1.2") (d (list (d (n "pink-lobster") (r "^0.1.2") (d #t) (k 0)))) (h "0q77yyjyqnrzyk5nsasnlsp9wy3jhc7srnzwg4vwh8ywzqx4s2sw")))

(define-public crate-pink-lobster-plugin-0.2.0 (c (n "pink-lobster-plugin") (v "0.2.0") (d (list (d (n "pink-lobster") (r "^0.2.0") (d #t) (k 0)))) (h "0kg7y4dhzzg1cm3ihqfsz4n1bl74sqii0mxirxd1ddvyhfxjqxyh")))

