(define-module (crates-io pi nk pink-trombone) #:use-module (crates-io))

(define-public crate-pink-trombone-0.1.0 (c (n "pink-trombone") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 2)))) (h "1n5zz0szzp6rm9cb434ra91skvv9h574v490gc5jbf530j8k6f20")))

(define-public crate-pink-trombone-0.2.0 (c (n "pink-trombone") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 2)))) (h "0kc4snzjibbh3lw1qly75knz2asfkdkdakiwqfmgbhbkaq7f73q6")))

(define-public crate-pink-trombone-0.2.1 (c (n "pink-trombone") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 2)))) (h "0c1bwmycvvx8v8pyhgfkhsw5hpfm55118ja7gagnx0v2pk394v1z")))

