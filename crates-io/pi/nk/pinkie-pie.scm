(define-module (crates-io pi nk pinkie-pie) #:use-module (crates-io))

(define-public crate-pinkie-pie-0.1.0 (c (n "pinkie-pie") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "terminal_color_builder") (r "^0.1.1") (d #t) (k 0)))) (h "0fk4ygi5yi4jbvw2ml6lqj9vhbzsvmbwsc1f6r1vlk5zkksl4qp8") (y #t)))

