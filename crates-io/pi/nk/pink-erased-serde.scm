(define-module (crates-io pi nk pink-erased-serde) #:use-module (crates-io))

(define-public crate-pink-erased-serde-0.3.23 (c (n "pink-erased-serde") (v "0.3.23") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.63") (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0yj39i82bza93aj5yqa1lnlis48qyxq4vd1d50p5yarffjnwy4z3") (f (quote (("unstable-debug") ("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.31")))

