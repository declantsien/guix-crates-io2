(define-module (crates-io pi nk pink-lobster) #:use-module (crates-io))

(define-public crate-pink-lobster-0.1.0 (c (n "pink-lobster") (v "0.1.0") (h "1g67nly75v3ydcvc5fd9mm1639x66ky8prv1xpymb97d9v78x0j5")))

(define-public crate-pink-lobster-0.1.1 (c (n "pink-lobster") (v "0.1.1") (h "0v3anfcphgxasryaxn6n6m8ibmlm883sshziirwdnv135bnqj0zf")))

(define-public crate-pink-lobster-0.1.2 (c (n "pink-lobster") (v "0.1.2") (h "0zgz5rliy18h5r7spc547qahzdxj24frcfgzkpdk1gr4hka88pyp")))

(define-public crate-pink-lobster-0.2.0 (c (n "pink-lobster") (v "0.2.0") (h "03937y3iy9xm00analfx273l6xbg4yx2gqky5j1p3z4zbzcc56c9")))

