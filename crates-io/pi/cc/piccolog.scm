(define-module (crates-io pi cc piccolog) #:use-module (crates-io))

(define-public crate-piccolog-1.0.0 (c (n "piccolog") (v "1.0.0") (d (list (d (n "log") (r ">= 0.4.4, < 0.4.9") (f (quote ("std"))) (d #t) (k 0)))) (h "1v14rvzlp1kqqi2l6gv035mx5wh8wia2dwk9978czj50vbl2baja")))

(define-public crate-piccolog-1.0.1 (c (n "piccolog") (v "1.0.1") (d (list (d (n "log") (r ">= 0.4.4, < 0.4.9") (f (quote ("std"))) (d #t) (k 0)))) (h "0jbp5j3kjc5094cyjhw08hm6qxa5wddkinnajchhch40xfd6xc4h")))

(define-public crate-piccolog-1.0.2 (c (n "piccolog") (v "1.0.2") (d (list (d (n "log") (r ">=0.4.4, <0.4.11") (f (quote ("std"))) (d #t) (k 0)))) (h "0xr37asyrqrha6lmx4igg3yvbbm7j6a106bfcy0xw7vw982w493x")))

(define-public crate-piccolog-1.0.3 (c (n "piccolog") (v "1.0.3") (d (list (d (n "log") (r ">=0.4.4, <0.4.15") (f (quote ("std"))) (d #t) (k 0)))) (h "171ncvs94zldn3j479zjfhn4rq8w4kkcrfm2f2nflhsy2b7zd43j")))

