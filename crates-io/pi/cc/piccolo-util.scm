(define-module (crates-io pi cc piccolo-util) #:use-module (crates-io))

(define-public crate-piccolo-util-0.3.0 (c (n "piccolo-util") (v "0.3.0") (d (list (d (n "gc-arena") (r "^0.5.0") (f (quote ("allocator-api2" "hashbrown"))) (d #t) (k 0)) (d (n "piccolo") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ffb83phla1vkvkl7w3ycaxld20xbz90qi22842dwgsdjcgk5axd") (f (quote (("default" "serde"))))))

(define-public crate-piccolo-util-0.3.1 (c (n "piccolo-util") (v "0.3.1") (d (list (d (n "gc-arena") (r "^0.5.0") (f (quote ("allocator-api2" "hashbrown"))) (d #t) (k 0)) (d (n "piccolo") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nbyvl11hxhxfgqkavma5jw604dm8gng6jdkpb1jd3xyldp1qaxq") (f (quote (("default" "serde"))))))

