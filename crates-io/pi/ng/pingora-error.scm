(define-module (crates-io pi ng pingora-error) #:use-module (crates-io))

(define-public crate-pingora-error-0.1.0 (c (n "pingora-error") (v "0.1.0") (h "0lr6xh1y9bn16y1w8kfbi9nkcxwmpc61m48wcf8jd777a2vv3b9d")))

(define-public crate-pingora-error-0.1.1 (c (n "pingora-error") (v "0.1.1") (h "0bg6qjh5zmikbdvk3dwxbx8ciwafdicgkp90hm8rj989llqb9q9h")))

(define-public crate-pingora-error-0.2.0 (c (n "pingora-error") (v "0.2.0") (h "0w0bh4jdqhwpslam44djahfiqdz6cgnxfdzhsgnszs9f6i65vawz")))

