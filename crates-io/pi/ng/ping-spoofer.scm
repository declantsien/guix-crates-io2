(define-module (crates-io pi ng ping-spoofer) #:use-module (crates-io))

(define-public crate-ping-spoofer-1.1.0 (c (n "ping-spoofer") (v "1.1.0") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.24") (d #t) (k 0)))) (h "0q05qzgvsijjwsva4m6d5dsg2h9fizv4an9y9zppw5vzs1d3zbfq")))

(define-public crate-ping-spoofer-1.2.0 (c (n "ping-spoofer") (v "1.2.0") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.24") (d #t) (k 0)))) (h "0fvs9ws5zkjcl0chgql5bh21zh46df51hqkpdzgf9br7kpzcr8ja")))

(define-public crate-ping-spoofer-1.2.1 (c (n "ping-spoofer") (v "1.2.1") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.24") (d #t) (k 0)))) (h "1wfr2mrh70zdbpcaxx08sbji4jm6975fgq3pnf5x0vjqsh6c2lky")))

