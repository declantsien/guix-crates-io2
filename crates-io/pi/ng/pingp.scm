(define-module (crates-io pi ng pingp) #:use-module (crates-io))

(define-public crate-pingp-0.1.0 (c (n "pingp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "future-utils") (r "^0.12.1") (d #t) (k 0)) (d (n "is-url") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "1amp9xmyx65zwgbi76d628mkbr5pqkydbh51a9kvd3h6q17s0k31")))

