(define-module (crates-io pi ng ping-pong-cell) #:use-module (crates-io))

(define-public crate-ping-pong-cell-0.1.0 (c (n "ping-pong-cell") (v "0.1.0") (h "0awckfgq56cws8hyp39288dis70q74nchdns1fh1j0b9m7a7r1mn")))

(define-public crate-ping-pong-cell-0.1.1 (c (n "ping-pong-cell") (v "0.1.1") (h "0wmvql6nrfkzlsxl2zv133b0ifrpfwjkbjs3hh4nanvvl2xkpvdy")))

(define-public crate-ping-pong-cell-0.1.2 (c (n "ping-pong-cell") (v "0.1.2") (h "1mxfxbd4xhs2b7s7230440jc57iqa9ar3k1gishvmlgv4qbmvhlm")))

