(define-module (crates-io pi ng pingkeeper) #:use-module (crates-io))

(define-public crate-pingkeeper-3.0.0 (c (n "pingkeeper") (v "3.0.0") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "pipeliner") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0nn3mjgbzi5wsi6zsv0wyxbs3w4x0f7bcm03nnzj27rc8i9jilpc")))

(define-public crate-pingkeeper-3.0.1 (c (n "pingkeeper") (v "3.0.1") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "pipeliner") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1xgihrbvznll7wjbcrb61hlqk8ig0nzrm4ywivjk6gicisk21qlw")))

(define-public crate-pingkeeper-3.0.2 (c (n "pingkeeper") (v "3.0.2") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "pipeliner") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0ah24zv4wmin2y42igiaiyvga3yczcmlq9razyv14qgmm7m7gp43")))

(define-public crate-pingkeeper-3.1.0 (c (n "pingkeeper") (v "3.1.0") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "pipeliner") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1vgjp886j6w9ba7sranqnpjvqb2bpgq658l03iy7g9hfpgqadxa1")))

(define-public crate-pingkeeper-3.1.1 (c (n "pingkeeper") (v "3.1.1") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "pipeliner") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0qg22f6125kza4rzyb2vwp4vcd92r06xaj8jdxlajp1pqgbsd8sm")))

(define-public crate-pingkeeper-3.2.0 (c (n "pingkeeper") (v "3.2.0") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "pipeliner") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "124w06vjwawn4ql7iab9s870nrkl2fi8ckbs871s5wh2ifxbkvf5")))

