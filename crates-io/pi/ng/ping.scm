(define-module (crates-io pi ng ping) #:use-module (crates-io))

(define-public crate-ping-0.1.0 (c (n "ping") (v "0.1.0") (h "039s7n5b8qmzhdc8n3amzr8jsb1b0h6mkjcbanki0m5ds3y5fhav")))

(define-public crate-ping-0.2.0 (c (n "ping") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)))) (h "1mwhi1yxz3j0sy6b4hx520ch4rimxn7qsq3jmw4ysv7k22xdc8bm")))

(define-public crate-ping-0.3.0 (c (n "ping") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gfks51ii4gpnrxl2xprw9r0j52m3s8pqs9dib0lcyifcw0sky8j")))

(define-public crate-ping-0.4.0 (c (n "ping") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xkb1i1adkbl237m5yh80vwqvrmndbdsm1cl7psc2kw900f4s139")))

(define-public crate-ping-0.4.1 (c (n "ping") (v "0.4.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h8iyphd5c6k609635ja813isyplnzrlz8hgp0pfrb2v39xahw33")))

(define-public crate-ping-0.5.9 (c (n "ping") (v "0.5.9") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1v87vflqa3mg2k81c5z1ca3xjafmwb45knbmqh3rwqzw60n2ixk6") (y #t)))

(define-public crate-ping-0.5.0 (c (n "ping") (v "0.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "052q591kcyk9056ckwmq39p9d0kp8m5bfrasp77mxwsbb3q8lp4q")))

(define-public crate-ping-0.5.1 (c (n "ping") (v "0.5.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d74gda73nypwb9d2d7wmngaakxm1vf9hg5w5gij2g1kr77an3sh")))

(define-public crate-ping-0.5.2 (c (n "ping") (v "0.5.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13afixj4glxac4xbjrc9ddx36591cainnp5xzj2fqfw4lvsy2bhj")))

