(define-module (crates-io pi ng pingora-limits) #:use-module (crates-io))

(define-public crate-pingora-limits-0.1.0 (c (n "pingora-limits") (v "0.1.0") (d (list (d (n "ahash") (r "^0") (d #t) (k 0)) (d (n "dashmap") (r "^5") (d #t) (k 2)) (d (n "dhat") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "0vg63wgh0ych3ryhlkskp267gc396gk4cs99n3gm3salkpzykra3") (f (quote (("dhat-heap"))))))

(define-public crate-pingora-limits-0.1.1 (c (n "pingora-limits") (v "0.1.1") (d (list (d (n "ahash") (r ">=0.8.9") (d #t) (k 0)) (d (n "dashmap") (r "^5") (d #t) (k 2)) (d (n "dhat") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "1fxjid97c4n3wmid772kyzmcraq7wyznk1ygz4gcnqajlywjsd3q") (f (quote (("dhat-heap"))))))

(define-public crate-pingora-limits-0.2.0 (c (n "pingora-limits") (v "0.2.0") (d (list (d (n "ahash") (r ">=0.8.9") (d #t) (k 0)) (d (n "dashmap") (r "^5") (d #t) (k 2)) (d (n "dhat") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "03arx73xvlax0aq8vn04znqplpy41r6jj6afav4dhr18v1fmp8r4") (f (quote (("dhat-heap"))))))

