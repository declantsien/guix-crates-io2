(define-module (crates-io pi ng ping-fox) #:use-module (crates-io))

(define-public crate-ping-fox-0.1.0 (c (n "ping-fox") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "more-asserts") (r "^0.3") (d #t) (k 2)) (d (n "pnet_packet") (r "^0.31") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "12xcq3whr1mwi73rv78rpysl7m6pwzmhd5z38fjw2cj0zhpn95z5")))

