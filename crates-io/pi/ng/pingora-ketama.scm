(define-module (crates-io pi ng pingora-ketama) #:use-module (crates-io))

(define-public crate-pingora-ketama-0.1.0 (c (n "pingora-ketama") (v "0.1.0") (d (list (d (n "crc32fast") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "csv") (r "^1.2") (d #t) (k 2)) (d (n "dhat") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0kiya13g2qqfcn451mdfv78894fkfh71dyx2vrahcpa8l02b84cx") (f (quote (("heap-prof"))))))

(define-public crate-pingora-ketama-0.1.1 (c (n "pingora-ketama") (v "0.1.1") (d (list (d (n "crc32fast") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "csv") (r "^1.2") (d #t) (k 2)) (d (n "dhat") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0dl7za7yw8292d049r74kx6sj5fdnx3n1ba4mknbadmygd2nacl2") (f (quote (("heap-prof"))))))

(define-public crate-pingora-ketama-0.2.0 (c (n "pingora-ketama") (v "0.2.0") (d (list (d (n "crc32fast") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "csv") (r "^1.2") (d #t) (k 2)) (d (n "dhat") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1cvlxkd26q2b0c6fk8cy01qwincq2ksn5srkqv1q546lj7gq85vj") (f (quote (("heap-prof"))))))

