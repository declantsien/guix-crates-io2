(define-module (crates-io pi ng pingall) #:use-module (crates-io))

(define-public crate-pingall-0.1.0 (c (n "pingall") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1b1jq7f5pblpwgg08km8r8qmhdnh0h0la0v5cgi912j4shkbmm3m")))

(define-public crate-pingall-0.1.1 (c (n "pingall") (v "0.1.1") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1f7c96xz124hcxylh6x62x1ysqc4sx7jjfnis5n21wq5y1m78ys1")))

(define-public crate-pingall-0.1.2 (c (n "pingall") (v "0.1.2") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1d5lcbxp5d7388qwmams3wjapxy1x97fld8scbficy570isrq43h")))

(define-public crate-pingall-0.2.0 (c (n "pingall") (v "0.2.0") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "process" "rt"))) (d #t) (k 0)))) (h "101ih4dxcnl874rvcqz9n6vk81dfdc5pcfvg7iizwffykhrfnv8d")))

(define-public crate-pingall-0.2.1 (c (n "pingall") (v "0.2.1") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "process" "rt"))) (d #t) (k 0)))) (h "0zx80xk75jwk7n0c8pb8nk0sid2b9h74nzmxk5rgd3v2rjvwrc81")))

(define-public crate-pingall-0.2.2 (c (n "pingall") (v "0.2.2") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "process" "rt"))) (d #t) (k 0)))) (h "132srhlmsdyq92y4fvksqvqm9sxywphsff4439ddbv9aq3ibbpsj")))

(define-public crate-pingall-0.3.0 (c (n "pingall") (v "0.3.0") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "surge-ping") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "process" "rt"))) (d #t) (k 0)))) (h "1ikdwf1s379ivnp4kjvlnfxzyqjkx8gdl9pm6gdjd0112awrl73y")))

(define-public crate-pingall-0.3.1 (c (n "pingall") (v "0.3.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "surge-ping") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "process" "rt"))) (d #t) (k 0)))) (h "0rdqjk2mgvr0bdvj4vs12mzinpglc2w5k25rqakzf0hqr1s85n0k")))

(define-public crate-pingall-0.3.2 (c (n "pingall") (v "0.3.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)) (d (n "ping") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "process" "rt"))) (d #t) (k 0)))) (h "0a0amgbvpp3fj4m86sc5id8sj5b34ihzcifdbc9k22dch10fhpcc")))

(define-public crate-pingall-0.3.3 (c (n "pingall") (v "0.3.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)) (d (n "tiny-ping") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "process" "rt"))) (d #t) (k 0)))) (h "18ksjz43vly5qwfnys2v1njbnj7hd63c836qysn1lq5sin3wcqfc")))

(define-public crate-pingall-0.3.4 (c (n "pingall") (v "0.3.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "pico-args") (r "^0.5") (d #t) (k 0)) (d (n "tiny-ping") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "process" "rt"))) (d #t) (k 0)))) (h "1anrkvfmc811k254cpx92pns95q1bii22ib18cmyxg7p360wj5zy")))

(define-public crate-pingall-0.3.5 (c (n "pingall") (v "0.3.5") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "pico-args") (r "^0.5") (d #t) (k 0)) (d (n "tiny-ping") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "process" "rt"))) (d #t) (k 0)))) (h "1rkxcy0krlj5m27y8wxnpppzkgdldjw615l78r79fwpmdd86a3s7")))

(define-public crate-pingall-0.4.1 (c (n "pingall") (v "0.4.1") (d (list (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "pico-args") (r "^0.5") (d #t) (k 0)) (d (n "tiny-ping") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "process" "rt"))) (d #t) (k 0)))) (h "0rygahdwgd2m38aym3siw1r24lnsmfgjr8ybjxkqv4n4zxp6gd8i")))

