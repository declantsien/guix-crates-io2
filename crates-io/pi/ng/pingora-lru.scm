(define-module (crates-io pi ng pingora-lru) #:use-module (crates-io))

(define-public crate-pingora-lru-0.1.0 (c (n "pingora-lru") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0") (d #t) (k 0)) (d (n "hashbrown") (r "^0") (d #t) (k 0)) (d (n "lru") (r "^0") (d #t) (k 2)) (d (n "parking_lot") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "1vr98hfbc1wf1fjmjgcl58n4332h34lz5c9b0a31sxfq6ag80hnl")))

(define-public crate-pingora-lru-0.1.1 (c (n "pingora-lru") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0") (d #t) (k 0)) (d (n "hashbrown") (r "^0") (d #t) (k 0)) (d (n "lru") (r "^0") (d #t) (k 2)) (d (n "parking_lot") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "11mp3s3yrd7fbnb0c3rhdjc6jfpq759fhxx2ml0rrzp1p5qqk1i5")))

(define-public crate-pingora-lru-0.2.0 (c (n "pingora-lru") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0") (d #t) (k 0)) (d (n "hashbrown") (r "^0") (d #t) (k 0)) (d (n "lru") (r "^0") (d #t) (k 2)) (d (n "parking_lot") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "1jgcdhgq7zbw5nimrwshl49sqa73fplzrxbvcllyksall8lli00q")))

