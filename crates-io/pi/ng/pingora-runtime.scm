(define-module (crates-io pi ng pingora-runtime) #:use-module (crates-io))

(define-public crate-pingora-runtime-0.1.0 (c (n "pingora-runtime") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net"))) (d #t) (k 2)))) (h "1jqc5f3cwkjhr1pj2izkhwl20h75f8ym962i1a6rx04dha47w8z0")))

(define-public crate-pingora-runtime-0.1.1 (c (n "pingora-runtime") (v "0.1.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net"))) (d #t) (k 2)))) (h "1fggzwiyjfqrb04h3zwvhrcpka1n3nml0hf651y79vnb7y1zj9p8")))

(define-public crate-pingora-runtime-0.2.0 (c (n "pingora-runtime") (v "0.2.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net"))) (d #t) (k 2)))) (h "1slrnjcqi7zhc0lfa3y9g6x5i4vwb9hihlvnclgwn70bzl7j5cy8")))

