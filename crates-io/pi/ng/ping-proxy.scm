(define-module (crates-io pi ng ping-proxy) #:use-module (crates-io))

(define-public crate-ping-proxy-0.1.0 (c (n "ping-proxy") (v "0.1.0") (d (list (d (n "buf-view") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "signal-hook-tokio") (r "^0.3.1") (f (quote ("futures-v0_3"))) (d #t) (k 0)) (d (n "socket2") (r "^0.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jk1gkfnpi1c1lsgncymrngaavk7ca1rxaamdg05islz2m20lir5")))

