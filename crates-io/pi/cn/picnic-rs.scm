(define-module (crates-io pi cn picnic-rs) #:use-module (crates-io))

(define-public crate-picnic-rs-0.1.0 (c (n "picnic-rs") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0lwfc0i8x33zcrgi53yzf5a2nsm4lpynp37jjqf0py8qyyayv466")))

