(define-module (crates-io pi dd piddiy) #:use-module (crates-io))

(define-public crate-piddiy-0.1.0 (c (n "piddiy") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1qmy45dxjcx4bmnxdkgsak1c6hjjkngarph5dfdfd156qxs1gvfg")))

(define-public crate-piddiy-0.1.1 (c (n "piddiy") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0am943sy7xzp77ycnfs77s44hs6ymf5k2x3v355sr091imqs6q7p")))

