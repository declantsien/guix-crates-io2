(define-module (crates-io pi d1 pid1-exe) #:use-module (crates-io))

(define-public crate-pid1-exe-0.1.0 (c (n "pid1-exe") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("std" "derive" "help"))) (k 0)) (d (n "pid1") (r "^0.1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)))) (h "137r1r3ncgnyiz4m0halfm1a7ygnfxxbgnrsyvpw06igcnrh1bgb")))

(define-public crate-pid1-exe-0.1.1 (c (n "pid1-exe") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("std" "derive" "help"))) (k 0)) (d (n "pid1") (r "^0.1.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)))) (h "1rbdx2np910n8r6p4xhqbh3l54fbq3ip7ga2n87x9hmzsz3nlabw")))

(define-public crate-pid1-exe-0.1.2 (c (n "pid1-exe") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("std" "derive" "help"))) (k 0)) (d (n "pid1") (r "^0.1.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)))) (h "0qv982pwlr62z1c09ciin8lalqcgz56385dw5acqy5waiwp5ivwf")))

(define-public crate-pid1-exe-0.1.3 (c (n "pid1-exe") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("std" "derive" "help"))) (k 0)) (d (n "pid1") (r "^0.1.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)))) (h "0g9k8vmjqv5s3mzbcgkqgxbpjxp2myv0c544cfq7gj1m3mzcskaf")))

