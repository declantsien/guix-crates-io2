(define-module (crates-io pi d1 pid1) #:use-module (crates-io))

(define-public crate-pid1-0.1.0 (c (n "pid1") (v "0.1.0") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("process" "signal"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07xax72m751kwxbkq29ws75zlb0l3i24g9qlcc4v7i1560cii27p")))

(define-public crate-pid1-0.1.1 (c (n "pid1") (v "0.1.1") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("process" "signal"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0x0lz3zidzj4004yngxz46igq59vzq7lfcrhn24vwjhqbvi4lkyl")))

