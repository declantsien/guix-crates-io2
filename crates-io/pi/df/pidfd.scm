(define-module (crates-io pi df pidfd) #:use-module (crates-io))

(define-public crate-pidfd-0.1.0 (c (n "pidfd") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17bzg6ccxhi2p7zxq7rcwnb0jkjn1amjf6yvgnw4iv30i78sdi3r") (y #t)))

(define-public crate-pidfd-0.2.0 (c (n "pidfd") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1njs8sd7r492fv2b61g6v4qzbm4ibgc7xbrpn6dimp8ns6ib3zdx") (f (quote (("waitid")))) (y #t)))

(define-public crate-pidfd-0.2.1 (c (n "pidfd") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "099syy3h2wnzsjdjdabkywixmnbfvcnqf68y5d4ncyn63h0daavi") (f (quote (("waitid")))) (y #t)))

(define-public crate-pidfd-0.2.2 (c (n "pidfd") (v "0.2.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 0)))) (h "1k9n19gx8hgp2k1cqx1kpxlwdp2f3zdbr4yahcipybkv7fysy04i") (f (quote (("waitid")))) (y #t)))

(define-public crate-pidfd-0.2.3 (c (n "pidfd") (v "0.2.3") (d (list (d (n "fd-reactor") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hfjac1k1n98hvfkhaq92anmpa3srnb5hkn5nkg2gbc521rm49w5") (f (quote (("waitid"))))))

(define-public crate-pidfd-0.2.4 (c (n "pidfd") (v "0.2.4") (d (list (d (n "fd-reactor") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0254c9dznk64rdil1lfff11nw5xrywdiwg7z49345laqvjfc9laz") (f (quote (("waitid"))))))

