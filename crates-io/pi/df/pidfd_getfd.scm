(define-module (crates-io pi df pidfd_getfd) #:use-module (crates-io))

(define-public crate-pidfd_getfd-0.1.0 (c (n "pidfd_getfd") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "pidfd") (r "^0.2.4") (d #t) (k 0)))) (h "10l42rb6vdrz025w2nmlgxjgxy5c692lb6r0wrzslm22cg32f3ka")))

(define-public crate-pidfd_getfd-0.2.0 (c (n "pidfd_getfd") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.98") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "pidfd") (r "^0.2.4") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "15zrgffsvr996fn0sk4m4wl3sx7b9i2f98pr0lh01l0f4db0k0h9") (f (quote (("nightly") ("default"))))))

(define-public crate-pidfd_getfd-0.2.1 (c (n "pidfd_getfd") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.98") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "pidfd") (r "^0.2.4") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0dzqvkk2kk7zv6cqdxhvjvi6hmhdqhbg93p8nhrf7gxi2ax2k1wx") (f (quote (("nightly") ("default"))))))

(define-public crate-pidfd_getfd-0.2.2 (c (n "pidfd_getfd") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.98") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "pidfd") (r "^0.2.4") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "063c3hwzw9kkbxalh1mlz5gcmizi4jaw1g132smryx8dfppgkcg7") (f (quote (("nightly") ("default"))))))

