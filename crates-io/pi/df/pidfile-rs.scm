(define-module (crates-io pi df pidfile-rs) #:use-module (crates-io))

(define-public crate-pidfile-rs-0.1.0 (c (n "pidfile-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15ilrpm0cg48mw3hn7249rgg78xf9cqimcwvcs6sp6p6y39hmxav")))

(define-public crate-pidfile-rs-0.1.1 (c (n "pidfile-rs") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0039k1p08n4csychnkyfnhgw266mhai1qr67zrb8w8d77x139zca")))

(define-public crate-pidfile-rs-0.1.2 (c (n "pidfile-rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 2)) (d (n "system-deps") (r "^5") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x01h9m9rznd7vblnq10l375jxpq7mcncx7im6qpj6yq3ldb8ngw")))

(define-public crate-pidfile-rs-0.2.0 (c (n "pidfile-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 2)) (d (n "system-deps") (r "^5") (d #t) (t "cfg(not(any(target_os = \"dragonfly\", target_os = \"freebsd\")))") (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w1ikb3nl01prz2c677f8g6r0bv994mm17vqpvhy9b5lmyldbvxx")))

