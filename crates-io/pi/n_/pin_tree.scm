(define-module (crates-io pi n_ pin_tree) #:use-module (crates-io))

(define-public crate-pin_tree-0.1.0 (c (n "pin_tree") (v "0.1.0") (h "094i0asqvli68bqp0d0jpcdc0kzwg1pzpiypn98c5ns0wz3y3r9g")))

(define-public crate-pin_tree-0.2.0 (c (n "pin_tree") (v "0.2.0") (h "08yrghya8gp6lflbn46dgvyq91ixb213snnigrpb5shb0d3cb4b3")))

