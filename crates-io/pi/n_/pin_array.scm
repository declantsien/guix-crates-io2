(define-module (crates-io pi n_ pin_array) #:use-module (crates-io))

(define-public crate-pin_array-0.1.0 (c (n "pin_array") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1bjkpqk6l6951rxya11cdvw2m0addl7nppgsl9jsj5wi0frp8ydq")))

(define-public crate-pin_array-0.1.1 (c (n "pin_array") (v "0.1.1") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "06ld9hmr60jhn84ayiqr8xh2hnaqgcqq8y239jd3cj7pp7zxsx18")))

