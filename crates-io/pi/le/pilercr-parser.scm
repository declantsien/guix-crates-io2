(define-module (crates-io pi le pilercr-parser) #:use-module (crates-io))

(define-public crate-pilercr-parser-1.0.0 (c (n "pilercr-parser") (v "1.0.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0jimpfwikp8212q5r34cwm0pdq6qgakxcnshkpf3b0x9kba3d6rd")))

(define-public crate-pilercr-parser-1.0.1 (c (n "pilercr-parser") (v "1.0.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "02lbkb33z39hs2d1lwkjc0nd4cpivrwa50ywjcxw80s36hy3a8ws")))

(define-public crate-pilercr-parser-1.0.2 (c (n "pilercr-parser") (v "1.0.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "01c1hrb76xw70lmm4z52dhhwynwaycqsprgrgr7540kbiq0h6v8y")))

(define-public crate-pilercr-parser-1.1.0 (c (n "pilercr-parser") (v "1.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1nycha5shq3md6v618xrfnfni40s0ivli4zgpm3qf421ba349ihl")))

