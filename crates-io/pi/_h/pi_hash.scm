(define-module (crates-io pi _h pi_hash) #:use-module (crates-io))

(define-public crate-pi_hash-0.1.0 (c (n "pi_hash") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "twox-hash") (r "^1") (d #t) (k 0)))) (h "074f6hqcpf9wkyk605g4835av1zj7b1s2j4xf0hz450wkjcx1jda") (f (quote (("xxhash"))))))

(define-public crate-pi_hash-0.1.1 (c (n "pi_hash") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "twox-hash") (r "^1") (d #t) (k 0)))) (h "1pb4mwzyz2j5b7995g7jcbn46yc7dvc77dnn1bj0fzax647y0fcf") (f (quote (("xxhash"))))))

