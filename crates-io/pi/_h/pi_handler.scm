(define-module (crates-io pi _h pi_handler) #:use-module (crates-io))

(define-public crate-pi_handler-0.1.3 (c (n "pi_handler") (v "0.1.3") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "pi_atom") (r "^0.1") (d #t) (k 0)) (d (n "pi_gray") (r "^0.1") (d #t) (k 0)))) (h "0a1wzwpz0qzyixaj9fz77s6kkxg3x93cyf75ingv9r64mc4zkl6p")))

(define-public crate-pi_handler-0.1.5 (c (n "pi_handler") (v "0.1.5") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_atom") (r "^0.1") (d #t) (k 0)) (d (n "pi_gray") (r "^0.1") (d #t) (k 0)))) (h "067bfskk90z0gvh3bis822fi2l3kgqg3b15dyzq2lbl2bvhbwsqr")))

(define-public crate-pi_handler-0.1.6 (c (n "pi_handler") (v "0.1.6") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_atom") (r "^0.2") (d #t) (k 0)) (d (n "pi_gray") (r "^0.1") (d #t) (k 0)))) (h "0h6m25qx7i9n0v8biqpmdjgvlzs6dm79nl8fglrc1kljpgs0ynlv")))

(define-public crate-pi_handler-0.1.7 (c (n "pi_handler") (v "0.1.7") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pi_atom") (r "^0.5") (d #t) (k 0)) (d (n "pi_gray") (r "^0.1") (d #t) (k 0)))) (h "0w1cnnkw6rax91cx4j0840rsp6p9q67jh2jn2mzn4dm8ma9nz8sc")))

