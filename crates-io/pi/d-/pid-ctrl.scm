(define-module (crates-io pi d- pid-ctrl) #:use-module (crates-io))

(define-public crate-pid-ctrl-0.1.0 (c (n "pid-ctrl") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d6wxygbfp2clsakp41lk6nzcigj1kbmf0fgnaji186hlyp9h4w4") (y #t)))

(define-public crate-pid-ctrl-0.1.1 (c (n "pid-ctrl") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pq095d33wm660f6pf9axdkml2y7zhfjw3pd28ayhmjrwncbavzn") (y #t)))

(define-public crate-pid-ctrl-0.1.2 (c (n "pid-ctrl") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kh2cca6pvkl9ws1p8qysq5clig6z8yck4j5s8fycsfpgnk174vi") (y #t)))

(define-public crate-pid-ctrl-0.1.3 (c (n "pid-ctrl") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mqgdkd3yd8xk4x2yf1crw71wxjhr7arnqzkr4y8nmyb84k7d8nq") (y #t)))

(define-public crate-pid-ctrl-0.1.4 (c (n "pid-ctrl") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jh2v1h1is4sjfzb3pdl4k8kjyr2cnqmn4xr3p0ja9h6a4rc7r6d")))

