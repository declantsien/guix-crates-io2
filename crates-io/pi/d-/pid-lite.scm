(define-module (crates-io pi d- pid-lite) #:use-module (crates-io))

(define-public crate-pid-lite-1.0.0 (c (n "pid-lite") (v "1.0.0") (h "04ax9k8sddvhh7yh5mgsi0xyyba2qd8b8l6p0b6v489v6zsy09rk")))

(define-public crate-pid-lite-1.1.0 (c (n "pid-lite") (v "1.1.0") (h "0b9hq4iq9b84frr1zm5v3zqc9z2x3r263ifjzvnlaa1n8ky6q0nq") (f (quote (("std") ("default" "std"))))))

(define-public crate-pid-lite-1.1.1 (c (n "pid-lite") (v "1.1.1") (h "1pv0vifrj0smha7glf5zkpv5vi7mn6wg97h8gbci52zwwx0kjmln") (f (quote (("std") ("default" "std"))))))

(define-public crate-pid-lite-1.1.2 (c (n "pid-lite") (v "1.1.2") (d (list (d (n "defmt") (r "^0.2") (o #t) (d #t) (k 0)))) (h "18zxb4v2l2fy3hvzyax3g7n4r1mb70f71b16p4g1axz4ncxsw87p") (f (quote (("std") ("default" "std"))))))

(define-public crate-pid-lite-1.2.0 (c (n "pid-lite") (v "1.2.0") (d (list (d (n "defmt") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0gx7vwi9jjbg2cvkkpzmpxd5vaywj4fnqwqpkihnmzqzrs0ganjj") (f (quote (("std") ("default" "std"))))))

