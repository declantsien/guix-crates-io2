(define-module (crates-io pi d- pid-allocator) #:use-module (crates-io))

(define-public crate-pid-allocator-0.1.0 (c (n "pid-allocator") (v "0.1.0") (d (list (d (n "spin") (r "^0.9.0") (d #t) (k 0)))) (h "1aqdpjhm4ccakp091sa88jmd7qi5429820hxap7ax9yqk229gmhq")))

(define-public crate-pid-allocator-0.1.1 (c (n "pid-allocator") (v "0.1.1") (d (list (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "0x9zvdllvjzal5dbnaq5qbyq6i56f6l9g9azqykwinvrc2i2dy9i")))

(define-public crate-pid-allocator-0.1.2 (c (n "pid-allocator") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "04kk51dl4n20p19lz3cqfddpm879dgn8ldm2l3nrz9p75ng801sz")))

(define-public crate-pid-allocator-0.1.3 (c (n "pid-allocator") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "1sv00q1bzrz56iqivkqym7ffksf6h4c93xnjn96ybppbni5b74a7")))

(define-public crate-pid-allocator-0.1.4 (c (n "pid-allocator") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "06n25dnf7raf324ms7hvzhyla2lakikm12h2h9a8h4hyq3c6vzya")))

(define-public crate-pid-allocator-0.1.5 (c (n "pid-allocator") (v "0.1.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "0dxsnsqqjx78mib8d74gv428rbc10vz4mrc5fm8wcq6fj12672vs")))

