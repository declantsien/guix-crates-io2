(define-module (crates-io pi d- pid-set) #:use-module (crates-io))

(define-public crate-pid-set-0.1.0 (c (n "pid-set") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "03s0rjb7q3d71s7yc6pp5j27vmpl3c3sjspgbmabczdnxw4j7bw9")))

(define-public crate-pid-set-0.1.1 (c (n "pid-set") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0hmbwanmzjrl95pnwk1xghxm6lizg9p6yxyjzhb2hcbr512jwiqc")))

(define-public crate-pid-set-0.1.3 (c (n "pid-set") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0xp6m2z3dkfb2wmjl4j2asdqrg565h89w2n677nx9h4z1phhzbi0")))

