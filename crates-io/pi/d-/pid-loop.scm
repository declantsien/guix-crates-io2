(define-module (crates-io pi d- pid-loop) #:use-module (crates-io))

(define-public crate-pid-loop-0.0.1 (c (n "pid-loop") (v "0.0.1") (h "19zhngg618fs9862ixh7fby7lv7qakv70k82qvs6qmg4m0y6h1r2")))

(define-public crate-pid-loop-0.0.2 (c (n "pid-loop") (v "0.0.2") (h "18vwyjicy5937wvpi63z3mp1qs4vgd7klf6950rq8aypxa7i8waw")))

(define-public crate-pid-loop-0.0.3 (c (n "pid-loop") (v "0.0.3") (h "0znq2y6f9hwl8i43xqpfl990wipm3s7h1g5wi0dk1hn6wc8507kk")))

(define-public crate-pid-loop-0.0.4 (c (n "pid-loop") (v "0.0.4") (h "0cfplscfvmhpz7xkaj4jf07m1bkd0w8dhjph2rjm4bq8v5x5wbm7")))

(define-public crate-pid-loop-0.0.5 (c (n "pid-loop") (v "0.0.5") (h "1y8j5cbgwk47ymhlc73z3zj7ncmyh0f6hx7vnkdk5m9gndg8fnfv")))

(define-public crate-pid-loop-0.0.6 (c (n "pid-loop") (v "0.0.6") (h "177j6v28w1d5vy2025k9k6ixcapm8npqnvsnw90qrrxi9wx0ig9d")))

