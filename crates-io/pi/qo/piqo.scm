(define-module (crates-io pi qo piqo) #:use-module (crates-io))

(define-public crate-piqo-0.1.0 (c (n "piqo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "gix-discover") (r "^0.22.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (f (quote ("editor"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0nhnn5zfj72hzdfmsw83m5f6xspaqfgpdcqknw57irk38lwvw2j8")))

(define-public crate-piqo-0.1.1 (c (n "piqo") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "gix-discover") (r "^0.22.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (f (quote ("editor"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "08gwdbgdwj39vd4cghy5n52scwmna2l0gr4qqpm41f4xk6818j3b")))

