(define-module (crates-io pi ll pillow-templates) #:use-module (crates-io))

(define-public crate-pillow-templates-0.3.0 (c (n "pillow-templates") (v "0.3.0") (d (list (d (n "handlebars") (r "^4.3.6") (d #t) (k 0)) (d (n "pillow-fs") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tera") (r "^1.17.1") (d #t) (k 0)))) (h "1ngzhkn3sjkpxl0ql6pm76jzcj8l6ga765ylysng01z0i5padb8k")))

(define-public crate-pillow-templates-0.4.1 (c (n "pillow-templates") (v "0.4.1") (d (list (d (n "handlebars") (r "^4.3.6") (d #t) (k 0)) (d (n "pillow-fs") (r "^0.4.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tera") (r "^1.17.1") (d #t) (k 0)))) (h "0jhfqb8zqrn1i8w6riad29vbi37iirlgs2l03ndhmk6hc8q2qjag")))

