(define-module (crates-io pi ll pillar) #:use-module (crates-io))

(define-public crate-pillar-0.1.0 (c (n "pillar") (v "0.1.0") (d (list (d (n "nix") (r "^0.20.0") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)))) (h "08p053m75a4s8qnqws3ihh751v4ifmax3m2bnc5bx2hxyri7snyb")))

(define-public crate-pillar-0.1.1 (c (n "pillar") (v "0.1.1") (d (list (d (n "nix") (r "^0.20.0") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)))) (h "0mzx3g83can4sizb8hr5mxhz98kyjh9fgyf1d46rnqn62r7cqm87")))

(define-public crate-pillar-0.1.2 (c (n "pillar") (v "0.1.2") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (t "cfg(target_family = \"unix\")") (k 0)))) (h "0bpfyw8j821jl5n0yxrrn85a1wkf0pyzqy0cqz3p201wkcfkwqdf")))

