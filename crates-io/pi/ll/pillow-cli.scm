(define-module (crates-io pi ll pillow-cli) #:use-module (crates-io))

(define-public crate-pillow-cli-0.1.0 (c (n "pillow-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sl5kbc2qfa1rdlya7i5wy61j2cnrnz1r762r96zj06gp7aaqxf1")))

