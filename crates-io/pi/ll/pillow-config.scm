(define-module (crates-io pi ll pillow-config) #:use-module (crates-io))

(define-public crate-pillow-config-0.1.0 (c (n "pillow-config") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0ll6ykg608dzjsjjzvklg12zi393hi0fg6kpncpqqdhdfrxbnir7")))

