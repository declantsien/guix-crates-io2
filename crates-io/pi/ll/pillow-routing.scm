(define-module (crates-io pi ll pillow-routing) #:use-module (crates-io))

(define-public crate-pillow-routing-0.3.0 (c (n "pillow-routing") (v "0.3.0") (d (list (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "pillow-env") (r "^0.3.0") (d #t) (k 0)) (d (n "pillow-http") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "19w2asqkwxib2nkawagyf074qz17j3vaw8sk2djwpfvc7gznnfwk")))

(define-public crate-pillow-routing-0.4.1 (c (n "pillow-routing") (v "0.4.1") (d (list (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "pillow-env") (r "^0.4.1") (d #t) (k 0)) (d (n "pillow-http") (r "^0.4.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1901ki74hm1v88jzygdi6qpz8kpmbplr5ilk6lj7bldgf4rjzl1h")))

(define-public crate-pillow-routing-0.4.2 (c (n "pillow-routing") (v "0.4.2") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "pillow-env") (r "^4.1.2") (d #t) (k 0)) (d (n "pillow-http") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03l56bz59m83nk27ic8rr1k7rq6rnr8b44pzms0jj8nh3lbqq8s7")))

