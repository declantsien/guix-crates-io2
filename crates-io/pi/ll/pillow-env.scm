(define-module (crates-io pi ll pillow-env) #:use-module (crates-io))

(define-public crate-pillow-env-0.3.0 (c (n "pillow-env") (v "0.3.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "0lxnsppj3m7ns8bqbihb5z95q0swmga0rz4xi17b2mhlanr0lvyh")))

(define-public crate-pillow-env-0.4.0 (c (n "pillow-env") (v "0.4.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "1wkmhmr4gnhwqm5sa8hb56bxinbiyv4lfpc53s8174y3w44xsd5i")))

(define-public crate-pillow-env-0.4.1 (c (n "pillow-env") (v "0.4.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "0rfxscbmvjh05k2fdbd4ybvrn6r78bxzsfk4xhj08ganhy6adgfw")))

(define-public crate-pillow-env-4.1.2 (c (n "pillow-env") (v "4.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "0f6wxkb7yivl6hphphqcwa3zgsn46ddsi103174vp4ad20j1fa52")))

