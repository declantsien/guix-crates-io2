(define-module (crates-io pi ll pillow_macros) #:use-module (crates-io))

(define-public crate-pillow_macros-0.4.1 (c (n "pillow_macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "188ildh992c5wwwvdgmm0j9p23zdr06z0mrwpxvc4yijm0w6zgw3")))

