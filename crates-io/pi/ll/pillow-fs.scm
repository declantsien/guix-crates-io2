(define-module (crates-io pi ll pillow-fs) #:use-module (crates-io))

(define-public crate-pillow-fs-0.3.0 (c (n "pillow-fs") (v "0.3.0") (h "17cqmfpqndvfjjy0zfgyavfd6khmy4y12qasqfzrv05dznslgngb")))

(define-public crate-pillow-fs-0.4.0 (c (n "pillow-fs") (v "0.4.0") (h "1c0s0ynsl0vvn4yb9r7j603nkpgm3wwypbjfbfv1k8ysbcb8djfj")))

(define-public crate-pillow-fs-0.4.1 (c (n "pillow-fs") (v "0.4.1") (h "09wik0ar0qh4jw9k5pzh6k9sc1yiy293m1fnpvphy2h3qnlljv0p")))

(define-public crate-pillow-fs-4.2.0 (c (n "pillow-fs") (v "4.2.0") (d (list (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)))) (h "1bla85qlbbn6pqgsv9hqswcyfv5zj32n1w7k5jmv1mzml44l0vwh")))

