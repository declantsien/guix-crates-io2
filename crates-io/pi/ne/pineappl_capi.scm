(define-module (crates-io pi ne pineappl_capi) #:use-module (crates-io))

(define-public crate-pineappl_capi-0.1.0 (c (n "pineappl_capi") (v "0.1.0") (d (list (d (n "pineappl") (r "^0.1.0") (d #t) (k 0)))) (h "1lqyxfcmxmsy0vx3y74jflinbwy1bfxpc9q72cniaz9rl0mps07x")))

(define-public crate-pineappl_capi-0.2.0 (c (n "pineappl_capi") (v "0.2.0") (d (list (d (n "pineappl") (r "^0.2.0") (d #t) (k 0)))) (h "1dpyp9y9caaqq3m6q7nd3wyqh5s9n2cjbggshpap73dd4xmavdpc")))

(define-public crate-pineappl_capi-0.3.0 (c (n "pineappl_capi") (v "0.3.0") (d (list (d (n "pineappl") (r "^0.3.0") (d #t) (k 0)))) (h "1kx3m059w97lxc0mjjh735122p0fshaqqa3fn8lnbgx9m7cr6cv6")))

(define-public crate-pineappl_capi-0.4.1 (c (n "pineappl_capi") (v "0.4.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pineappl") (r "^0.4.1") (d #t) (k 0)))) (h "0li94f4l7gaddmjgirccc7lnb1xskk9m7xa5h58w2rs6s2mqjyla")))

(define-public crate-pineappl_capi-0.5.0 (c (n "pineappl_capi") (v "0.5.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.5.0") (d #t) (k 0)))) (h "1cwiilw546xf0w91s2m8irqyyq1phhma9hra1w7vsx8w63avi9jw") (f (quote (("capi"))))))

(define-public crate-pineappl_capi-0.5.1 (c (n "pineappl_capi") (v "0.5.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.5.1") (d #t) (k 0)))) (h "1x8c2v1374y081rl0ncnwgwb9c9f4arj7gak638p6z62brdamqn5") (f (quote (("capi"))))))

(define-public crate-pineappl_capi-0.5.2 (c (n "pineappl_capi") (v "0.5.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.5.2") (d #t) (k 0)))) (h "17dhf0p6zcmmcv5by83rpjhdwx2yga5a0ygz8sr9w7wqizkavyn0") (f (quote (("capi"))))))

(define-public crate-pineappl_capi-0.5.3 (c (n "pineappl_capi") (v "0.5.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.5.3") (d #t) (k 0)))) (h "0j7cxnv8iv275zlsfid6wd65iy2p4dfwnsl0yancfr82j3m5jr6r") (f (quote (("capi"))))))

(define-public crate-pineappl_capi-0.5.4 (c (n "pineappl_capi") (v "0.5.4") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.5.4") (d #t) (k 0)))) (h "1lis87pi0l1b4l3qvsz8crls1wyby76xj3iyhazh1vzgyz2xd3dc") (f (quote (("capi"))))))

(define-public crate-pineappl_capi-0.5.5 (c (n "pineappl_capi") (v "0.5.5") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.5.5") (d #t) (k 0)))) (h "0kl2ii38c9znzcvv6rfzlal1w8859kfwi4ykadc5in381269giqc") (f (quote (("capi")))) (r "1.56.1")))

(define-public crate-pineappl_capi-0.5.6 (c (n "pineappl_capi") (v "0.5.6") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.5.6") (d #t) (k 0)))) (h "0a3si5bcryazzm6niiqs6z57kk9991fvzwm1q1667vfifn5q77c3") (f (quote (("capi")))) (r "1.56.1")))

(define-public crate-pineappl_capi-0.5.7 (c (n "pineappl_capi") (v "0.5.7") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.5.7") (d #t) (k 0)))) (h "03my9jnm14ksbxay64mc6hh88dad7c095pnxn1r1awvcbbfz5zkb") (f (quote (("capi")))) (r "1.56.1")))

(define-public crate-pineappl_capi-0.5.8 (c (n "pineappl_capi") (v "0.5.8") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.5.8") (d #t) (k 0)))) (h "0i43n79q0ici4az32vvsw4xgd5vz5w42b3akdann2ww8fgj6pssx") (f (quote (("capi")))) (r "1.56.1")))

(define-public crate-pineappl_capi-0.5.9 (c (n "pineappl_capi") (v "0.5.9") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.5.9") (d #t) (k 0)))) (h "04kffnrxad9qcj6qgs7b7sw3nls7jdkrxdnqwf9dpqcsjj20743l") (f (quote (("capi")))) (r "1.56.1")))

(define-public crate-pineappl_capi-0.6.0-alpha.1 (c (n "pineappl_capi") (v "0.6.0-alpha.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.6.0-alpha.1") (d #t) (k 0)))) (h "0h3682hg2zmv8vnvzq96j807bnrc0c5wg8dkqj4h720kkk4a2l2z") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.6.0-alpha.2 (c (n "pineappl_capi") (v "0.6.0-alpha.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.6.0-alpha.2") (d #t) (k 0)))) (h "0vm588fgqrgigrlkq6hn724f0i20xv7axrkmsihp4hsmkg6riv63") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.6.0-alpha.3 (c (n "pineappl_capi") (v "0.6.0-alpha.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.6.0-alpha.3") (d #t) (k 0)))) (h "16zq8mn22bfw5bzkm0cq53w71ni60yj5pvv0bw5yizxkr3ki08pb") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.6.0-alpha.4 (c (n "pineappl_capi") (v "0.6.0-alpha.4") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.6.0-alpha.4") (d #t) (k 0)))) (h "0fw2bcyzmfsp9312nw78nmvq06as0lld4vbfjm9cnl5w4hvn9a3b") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.6.0-alpha.5 (c (n "pineappl_capi") (v "0.6.0-alpha.5") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.6.0-alpha.5") (d #t) (k 0)))) (h "1r2xaxdnbndiczdsbnisx509zjsa46k2f9pvmrgg7ilw3kbg5cfm") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.6.0-alpha.9 (c (n "pineappl_capi") (v "0.6.0-alpha.9") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.6.0-alpha.9") (d #t) (k 0)))) (h "123zw61wy1xby9iv2337v301wnrxsfm9170d9zaq247plrghziq4") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.6.0-alpha.10 (c (n "pineappl_capi") (v "0.6.0-alpha.10") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.6.0-alpha.10") (d #t) (k 0)))) (h "1qrhvx5hzmfyphl0fh2b98bwbzmzd9ckq3g80qxbnpdcg8blwmk5") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.6.0-alpha.11 (c (n "pineappl_capi") (v "0.6.0-alpha.11") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.6.0-alpha.11") (d #t) (k 0)))) (h "1fx0ff7g6svgs8aqzpd34c1yly7l1yjbyla12540jy02j6b1pgd6") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.6.0-alpha.17 (c (n "pineappl_capi") (v "0.6.0-alpha.17") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.6.0-alpha.17") (d #t) (k 0)))) (h "00whab776dsqhk4rdfpamidr830kqm1wk3bcjr4j0crkb40iyc9l") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.6.0-alpha.18 (c (n "pineappl_capi") (v "0.6.0-alpha.18") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.6.0-alpha.18") (d #t) (k 0)))) (h "09b2y92r90aax8z5wvsj6ia7li5vgv3a6k5y2z4lsrblb2ad5nm3") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.6.0 (c (n "pineappl_capi") (v "0.6.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.6.0") (d #t) (k 0)))) (h "09602gj62izkhd667z16d3mrpbpky0q27k10xsckdb0prjfxnd4f") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.6.1 (c (n "pineappl_capi") (v "0.6.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "^0.6.1") (d #t) (k 0)))) (h "1gq3krfkk8zvwc0af6wd2g0a96vl853zywxss7qn0dajsh07ls95") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.6.2 (c (n "pineappl_capi") (v "0.6.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "=0.6.2") (d #t) (k 0)))) (h "1npra7jglpz97f658xvzx0pxf0x2bx9lvmpplmy49si93drws9h3") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.6.3 (c (n "pineappl_capi") (v "0.6.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "=0.6.3") (d #t) (k 0)))) (h "0lzynxbzb2xbc1rvc9zl8jrmx9wrlmd7llmdx1ik9bfgnx8fww4i") (f (quote (("capi")))) (r "1.64.0")))

(define-public crate-pineappl_capi-0.7.0 (c (n "pineappl_capi") (v "0.7.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "=0.7.0") (d #t) (k 0)))) (h "136wyl7gscdjf4a8qc0l5r85qscd24zsnpvlmlnjns8ihpsrcbh2") (f (quote (("capi")))) (r "1.70.0")))

(define-public crate-pineappl_capi-0.7.1-rc.2 (c (n "pineappl_capi") (v "0.7.1-rc.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "=0.7.1-rc.2") (d #t) (k 0)))) (h "1ydrfgq36kzi1x7jdnhgqphx2xyl4zp5p3mqslxz4d5bkarcmz7b") (f (quote (("capi")))) (r "1.70.0")))

(define-public crate-pineappl_capi-0.7.1 (c (n "pineappl_capi") (v "0.7.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "=0.7.1") (d #t) (k 0)))) (h "1vvizsmsayadh2mqbzzls6x192p7vpikhqxci1m2pvr488fbpczm") (f (quote (("capi")))) (r "1.70.0")))

(define-public crate-pineappl_capi-0.7.2 (c (n "pineappl_capi") (v "0.7.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "=0.7.2") (d #t) (k 0)))) (h "1slpmhn22j4r0flr1i8ijniqbj26hn3bapf88k86hwfqw16c338m") (f (quote (("capi")))) (r "1.70.0")))

(define-public crate-pineappl_capi-0.7.3 (c (n "pineappl_capi") (v "0.7.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "=0.7.3") (d #t) (k 0)))) (h "1mz8aiz4019w89hfpiaq272vqid5r03sa1w2kh56ph7x1wwrx5ik") (f (quote (("capi")))) (r "1.70.0")))

(define-public crate-pineappl_capi-0.7.4-rc.1 (c (n "pineappl_capi") (v "0.7.4-rc.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "=0.7.4-rc.1") (d #t) (k 0)))) (h "01z2r3zw8gyr3slxc4vwklblrmspnk76dynf1ivxddijn8iy2dnm") (f (quote (("capi")))) (r "1.70.0")))

(define-public crate-pineappl_capi-0.7.4 (c (n "pineappl_capi") (v "0.7.4") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pineappl") (r "=0.7.4") (d #t) (k 0)))) (h "1d992696xhk36gfn0yhszjpgzd3wpq6l9ql2n27zdn15pd3vfqzh") (f (quote (("capi")))) (r "1.70.0")))

