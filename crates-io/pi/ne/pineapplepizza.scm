(define-module (crates-io pi ne pineapplepizza) #:use-module (crates-io))

(define-public crate-pineapplepizza-0.1.0 (c (n "pineapplepizza") (v "0.1.0") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "1b83br7m6ky6xfv2mrapl51qm8x0zq5lfn4gp0g3kjpqsd7wd92k")))

(define-public crate-pineapplepizza-0.1.1 (c (n "pineapplepizza") (v "0.1.1") (d (list (d (n "nom") (r "^4.1") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "14m3s2kvszwli33jqdjx00k11iz7x966gd5j5d1mxb492zksbwql")))

(define-public crate-pineapplepizza-0.1.2 (c (n "pineapplepizza") (v "0.1.2") (d (list (d (n "nom") (r "^4.1") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0fpp958n72nxa21mhyh53hj2cx3hkwy4wigqcmcq404w4dmfs0nw")))

