(define-module (crates-io pi ne pineappl_applgrid) #:use-module (crates-io))

(define-public crate-pineappl_applgrid-0.1.0 (c (n "pineappl_applgrid") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "0diivd49khlacyr6zdkzyf72l69nv73j4b0sw8bjmy6kml5ww1iz") (r "1.56.1")))

(define-public crate-pineappl_applgrid-0.2.0 (c (n "pineappl_applgrid") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "0cpmwaikcvs969b4knz6q55rnqdiixszgkgrfdkdr5dz0nj5s0aq") (r "1.56.1")))

(define-public crate-pineappl_applgrid-0.5.9 (c (n "pineappl_applgrid") (v "0.5.9") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "04avc4f0k3qi5q25jyflw9pvz6pndgjdx1ca4h01ca9nhyqc7xpi") (r "1.56.1")))

(define-public crate-pineappl_applgrid-0.6.0-alpha.1 (c (n "pineappl_applgrid") (v "0.6.0-alpha.1") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "1p5gw7cpgi2ydmf8qs4p94iic0sxn9km2wa8wk8rlyl0gsq7ykgl") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.6.0-alpha.2 (c (n "pineappl_applgrid") (v "0.6.0-alpha.2") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "1nipq59wzzkic0slk2xxw2dsli77dl25ar4r2qsanhv2j4i8lb89") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.6.0-alpha.3 (c (n "pineappl_applgrid") (v "0.6.0-alpha.3") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "1rx5f46308a25rcqf2618cdnhxjk1f3wn70vc5i2b5g6s8ga0449") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.6.0-alpha.4 (c (n "pineappl_applgrid") (v "0.6.0-alpha.4") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "16cqc63byhxwhbj1x0jfzyak4qs2q0ncn3x4k251hh6km5yk8cpg") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.6.0-alpha.5 (c (n "pineappl_applgrid") (v "0.6.0-alpha.5") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "12a84krx5dbd2j55qghv71lq7h4gvgry45v9h0k08kz0h2ivz3s3") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.6.0-alpha.9 (c (n "pineappl_applgrid") (v "0.6.0-alpha.9") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "11jwz0jky856y67dw7nf37kvzicz0c395hra2qz2cybwlha072cm") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.6.0-alpha.10 (c (n "pineappl_applgrid") (v "0.6.0-alpha.10") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "1f5nbihyayp8jpds6wwrwv43yhbpfld812pa62jm3lhvzdz6acan") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.6.0-alpha.11 (c (n "pineappl_applgrid") (v "0.6.0-alpha.11") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "083bp811nz7dr88q0x547vxb2hmbgaxayssyjdwh651if8cvjhyv") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.6.0-alpha.17 (c (n "pineappl_applgrid") (v "0.6.0-alpha.17") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "18njzcvbdn386d37d1bsfva438bm4492dlxrccgdsc2px8q9bzw6") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.6.0-alpha.18 (c (n "pineappl_applgrid") (v "0.6.0-alpha.18") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "0bk1548f4v9y9ry20p4r2pz0c53dvr9cqdr8iy45xn8abxklr88b") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.6.0 (c (n "pineappl_applgrid") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "10wr993khhqyx0xfrr0dadq3pa5s375s3k2cb642qf8dmlh77kr7") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.6.1 (c (n "pineappl_applgrid") (v "0.6.1") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "1cwd9zz68mb1j9wcw7hsmi1hpxyj3yqji3p4qrk153bck9g7sd54") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.6.2 (c (n "pineappl_applgrid") (v "0.6.2") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "1kyy8adkwjsg82wfw9hyriv3zsshir80c9l2b38c4803d6jwmvbr") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.6.3 (c (n "pineappl_applgrid") (v "0.6.3") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1l0r263czqrkknhwhf46rwq9jqaw0r2cnp9sai9i80b3c4wa1vkf") (r "1.64.0")))

(define-public crate-pineappl_applgrid-0.7.0 (c (n "pineappl_applgrid") (v "0.7.0") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "189ac1jf97w8rxy24vkbpkdckhxbm2vcjrqbpgbpyja76sgjx12m") (r "1.70.0")))

(define-public crate-pineappl_applgrid-0.7.1-rc.2 (c (n "pineappl_applgrid") (v "0.7.1-rc.2") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0wzrihvrlbji5yqyfcmqicvrb171pc0afbaljjcxka8sl3c3q0g6") (r "1.70.0")))

(define-public crate-pineappl_applgrid-0.7.1 (c (n "pineappl_applgrid") (v "0.7.1") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "02zpbdxr99z9ai54cf1nqf58n58rm8vn2w46v436zvni9r1pw4mq") (r "1.70.0")))

(define-public crate-pineappl_applgrid-0.7.2 (c (n "pineappl_applgrid") (v "0.7.2") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0z64disnzir77mvs8m0vn3ill408dg7mzg0cwpc1fypz3mxy0qxw") (r "1.70.0")))

(define-public crate-pineappl_applgrid-0.7.3 (c (n "pineappl_applgrid") (v "0.7.3") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1jcb84lslibgica0k16ld1j70j1cdqa73vbivnlrv63555lcpphq") (r "1.70.0")))

(define-public crate-pineappl_applgrid-0.7.4-rc.1 (c (n "pineappl_applgrid") (v "0.7.4-rc.1") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1k8x4nbih07m5bxsja4isa2w75il0gdbkk69pg90mp499ybm38pf") (r "1.70.0")))

(define-public crate-pineappl_applgrid-0.7.4 (c (n "pineappl_applgrid") (v "0.7.4") (d (list (d (n "cc") (r "^1.0.49") (d #t) (k 1)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "055py5cy28xchfkn22xn8b7ilrv3hj84xxzzkhi5gp4pnwbzgnzw") (f (quote (("static")))) (r "1.70.0")))

