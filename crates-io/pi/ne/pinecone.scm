(define-module (crates-io pi ne pinecone) #:use-module (crates-io))

(define-public crate-pinecone-0.1.0 (c (n "pinecone") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.100") (f (quote ("derive" "alloc"))) (k 0)))) (h "0x071x6pk805fbpms9qknx7pbp8cmvjgffsd72l4nh36gj0jf5cl") (f (quote (("use-std") ("defaults"))))))

(define-public crate-pinecone-0.2.0 (c (n "pinecone") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.6.3") (f (quote ("nightly" "inline-more" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive" "alloc"))) (k 0)))) (h "0i66zy1dx45f3qv5a2cwn435xv59gpcc7jgq1qpgcrycaxqa2ag8") (f (quote (("use-std" "serde/std") ("defaults"))))))

(define-public crate-pinecone-0.2.1 (c (n "pinecone") (v "0.2.1") (d (list (d (n "hashbrown") (r "^0.6.3") (f (quote ("nightly" "inline-more" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive" "alloc"))) (k 0)))) (h "160jpklq1qccwqhxcp9bwrhjgkni89a8wrfy17r8v70d7i0d2yb4") (f (quote (("use-std" "serde/std") ("defaults"))))))

(define-public crate-pinecone-0.2.2 (c (n "pinecone") (v "0.2.2") (d (list (d (n "hashbrown") (r "^0.6.3") (f (quote ("nightly" "inline-more" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive" "alloc"))) (k 0)))) (h "0n20azwjy1s91k3787nxq30r24vp8zypc505dbp1cnaz70hzss3p") (f (quote (("use-std" "serde/std") ("defaults"))))))

(define-public crate-pinecone-0.2.3 (c (n "pinecone") (v "0.2.3") (d (list (d (n "hashbrown") (r "^0.6.3") (f (quote ("nightly" "inline-more" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive" "alloc"))) (k 0)))) (h "089f6zqq4rf945ykf8d6fhwb84pm8vfak3w97azb8px67wfp6a6l") (f (quote (("use-std" "serde/std") ("defaults"))))))

(define-public crate-pinecone-0.2.4 (c (n "pinecone") (v "0.2.4") (d (list (d (n "hashbrown") (r "^0.6.3") (f (quote ("nightly" "inline-more" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive" "alloc"))) (k 0)))) (h "1dr7zi4bn4zlrmkwki0naa5zy6bp58176175qbhvzk5j2x2imx04") (f (quote (("use-std" "serde/std") ("defaults"))))))

(define-public crate-pinecone-0.2.5 (c (n "pinecone") (v "0.2.5") (d (list (d (n "hashbrown") (r "^0.11.2") (f (quote ("nightly" "inline-more" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive" "alloc"))) (k 0)))) (h "1yyrjgbpync0g3skz6sig003khqcd126vdvg1nyhyriniimx8zi7") (f (quote (("use-std" "serde/std") ("defaults"))))))

