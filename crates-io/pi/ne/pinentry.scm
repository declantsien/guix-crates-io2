(define-module (crates-io pi ne pinentry) #:use-module (crates-io))

(define-public crate-pinentry-0.0.0 (c (n "pinentry") (v "0.0.0") (h "07yq94d4jpl3iicm2cckz4z1ccp2il3fz6qbqzc7rqn7n0fvq9lk")))

(define-public crate-pinentry-0.1.0 (c (n "pinentry") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "secrecy") (r "^0.6") (d #t) (k 0)) (d (n "which") (r "^3.1") (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1ks1j51q6kprqm4kcavnn1chz2g4awgw0i6hfc8fd6akasg2pgrn")))

(define-public crate-pinentry-0.2.0 (c (n "pinentry") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "secrecy") (r "^0.7") (d #t) (k 0)) (d (n "which") (r "^4") (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "09qkbhaasj7bhyihb13fyvx0c2k8midmcw5s679cpyxpxb8bas6z")))

(define-public crate-pinentry-0.3.0 (c (n "pinentry") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "secrecy") (r "^0.7") (d #t) (k 0)) (d (n "which") (r "^4") (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "0899l6d0jx8rmrmpxf6mhj9x4bvfvcnyggq07mpz23n4fxp6l9m8")))

(define-public crate-pinentry-0.4.0 (c (n "pinentry") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "secrecy") (r "^0.8") (d #t) (k 0)) (d (n "which") (r "^4") (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1h9q582sz7091yx0nm6ihq3n6nssb0gj42mpdg0335lxmww7kq6k")))

(define-public crate-pinentry-0.5.0 (c (n "pinentry") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "secrecy") (r "^0.8") (d #t) (k 0)) (d (n "which") (r "^4") (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1j4grhhn4q3f4dycnkxr0lcvlv417ysnvs2fm0mmwsmyd2ybi9dz")))

