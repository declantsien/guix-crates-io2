(define-module (crates-io pi ne pinenote_dbus_service) #:use-module (crates-io))

(define-public crate-pinenote_dbus_service-0.2.1-dev (c (n "pinenote_dbus_service") (v "0.2.1-dev") (d (list (d (n "dbus") (r "^0.9.6") (d #t) (k 0)) (d (n "dbus-crossroads") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "1cbd23afpw8z271fmz4a9vx0lnxdm3543advjxh2ayywkzsp5cjb")))

(define-public crate-pinenote_dbus_service-0.2.1-dev2 (c (n "pinenote_dbus_service") (v "0.2.1-dev2") (d (list (d (n "dbus") (r "^0.9.6") (d #t) (k 0)) (d (n "dbus-crossroads") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "15vy8v72v1ald3988vbhdplb2jfpg6gjcc0gfpjqsmbiqwc1ayiw")))

(define-public crate-pinenote_dbus_service-0.2.1-dev3 (c (n "pinenote_dbus_service") (v "0.2.1-dev3") (d (list (d (n "dbus") (r "^0.9.6") (d #t) (k 0)) (d (n "dbus-crossroads") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "nix") (r ">=0.25.0") (d #t) (k 0)))) (h "05qnzhrbz9apr4ql2nlmz09j4y5g8mli2iawkn0lamv3jb6x50mp")))

(define-public crate-pinenote_dbus_service-0.2.2-dev1 (c (n "pinenote_dbus_service") (v "0.2.2-dev1") (d (list (d (n "cargo-deb") (r "^2.1.0") (d #t) (k 0)) (d (n "dbus") (r "^0.9.6") (d #t) (k 0)) (d (n "dbus-crossroads") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "nix") (r ">=0.25.0") (d #t) (k 0)))) (h "0b7jygpmp03jgqiwa5nd13mhy13rv7h7fyia4556r40789x33xaj")))

