(define-module (crates-io pi ne pineappl_fastnlo) #:use-module (crates-io))

(define-public crate-pineappl_fastnlo-0.1.0 (c (n "pineappl_fastnlo") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "1vpi3rhsix8c7rb0hb8qxa1kdfs67yys7g5qv49lxds86bdn089m")))

(define-public crate-pineappl_fastnlo-0.2.0 (c (n "pineappl_fastnlo") (v "0.2.0") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "0czl3a9fgxq36p83dvrysslc25f5cnay6bba3hgr6c02xpzcbqlm") (r "1.56.1")))

(define-public crate-pineappl_fastnlo-0.5.9 (c (n "pineappl_fastnlo") (v "0.5.9") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "12f5zpgas8fv2ii1bp1rma80d5h49m8m0wyyj125i17vgif89fxi") (r "1.56.1")))

(define-public crate-pineappl_fastnlo-0.6.0-alpha.1 (c (n "pineappl_fastnlo") (v "0.6.0-alpha.1") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "0f7jmfyx3ynxikv9szlqmbpkz2c53xhpa4wgd7znjsci1i4hqybw") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.6.0-alpha.2 (c (n "pineappl_fastnlo") (v "0.6.0-alpha.2") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "0lnlh2bgyf7dc1rrlvzx09372f8yp5c17k1y44r5yalnlviz0661") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.6.0-alpha.3 (c (n "pineappl_fastnlo") (v "0.6.0-alpha.3") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "0rhxd25q3g5wjv5vj4d6aivvvqrddhlj9im24fcwlc4kn7rwwj7s") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.6.0-alpha.4 (c (n "pineappl_fastnlo") (v "0.6.0-alpha.4") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "1h5ld44npw3f41rg0prdb6vp865yk33z9bdpw6qykvcmfyxi105z") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.6.0-alpha.5 (c (n "pineappl_fastnlo") (v "0.6.0-alpha.5") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "1hz55xckr8l5rd4j65c4dkr6169k33salcasyq5w16z5v0kqp1rf") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.6.0-alpha.9 (c (n "pineappl_fastnlo") (v "0.6.0-alpha.9") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "129iwjxfng9nq4iry5zm1qwkvl5mm156r0jk4fzp565gq21ap4xz") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.6.0-alpha.10 (c (n "pineappl_fastnlo") (v "0.6.0-alpha.10") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "1kw0kd29prc515z859rl7z1nglsc8sl2cbp83524hhja5z5d96bx") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.6.0-alpha.11 (c (n "pineappl_fastnlo") (v "0.6.0-alpha.11") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "0mcns3y4r85rnvirqivx99lw7y62qgrk08fgch13395wsggmp1k8") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.6.0-alpha.17 (c (n "pineappl_fastnlo") (v "0.6.0-alpha.17") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)))) (h "0yxwjqzs1vkklzyall66djs7sa39n4yzwh0vk7wpbyi552rxc857") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.6.0-alpha.18 (c (n "pineappl_fastnlo") (v "0.6.0-alpha.18") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0kjr5x2fhidw7rayfm1ip06sdk7sjpa1aq0pyx29p30czyg0wlcr") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.6.0 (c (n "pineappl_fastnlo") (v "0.6.0") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1djyi56l8rq98b1pldlz658ync36wh9zx5q595crhgpwn5m2hxzc") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.6.1 (c (n "pineappl_fastnlo") (v "0.6.1") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "00ri2v1ck48lfz49q7fda5a8s4dfmb1y82abgqfwvcci1dqknkbn") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.6.2 (c (n "pineappl_fastnlo") (v "0.6.2") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "01s4nyl4qk8k5j1l3avawf9kgb5p09hbzdlww5i4i5y8bfcaylnp") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.6.3 (c (n "pineappl_fastnlo") (v "0.6.3") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1byf32b21bfgkd7rq5zic3i07fhgr20ilkdy9v8mw8b8z9xiaqnh") (r "1.64.0")))

(define-public crate-pineappl_fastnlo-0.7.0 (c (n "pineappl_fastnlo") (v "0.7.0") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "02gwaggq35h4ybqb99mx522j0av6kbar2lf5y8z8fxpcwxdx8zcb") (r "1.70.0")))

(define-public crate-pineappl_fastnlo-0.7.1-rc.2 (c (n "pineappl_fastnlo") (v "0.7.1-rc.2") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "04xgqn7x51kqp65jl74720yb5cfpa8xgd3hzl07yiiynr6k9bpgj") (r "1.70.0")))

(define-public crate-pineappl_fastnlo-0.7.1 (c (n "pineappl_fastnlo") (v "0.7.1") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gl8hq29qigvcx1kivcaxx1m5zis2sakhfinxsjpkhm2ahdv9a1z") (r "1.70.0")))

(define-public crate-pineappl_fastnlo-0.7.2 (c (n "pineappl_fastnlo") (v "0.7.2") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1mb54algl354qg3j69m7qhswhsi949cqk6ylbkb6z80hkays7c4w") (r "1.70.0")))

(define-public crate-pineappl_fastnlo-0.7.3 (c (n "pineappl_fastnlo") (v "0.7.3") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "03nd8yxrwh9vqidkizjggy9c9c3jcpd9gpykyfkpihn9ibmshsx3") (r "1.70.0")))

(define-public crate-pineappl_fastnlo-0.7.4-rc.1 (c (n "pineappl_fastnlo") (v "0.7.4-rc.1") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1v7m1ljp5bamv0gh4j5ms6d4mvf2s18w9bj1n8bjh8018lvnvrjq") (r "1.70.0")))

(define-public crate-pineappl_fastnlo-0.7.4 (c (n "pineappl_fastnlo") (v "0.7.4") (d (list (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1k1midb0k2hmzagmja2z6djrg8mll6p9caxm2qrsjxzipzmg2mdz") (f (quote (("static")))) (r "1.70.0")))

