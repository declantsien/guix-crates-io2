(define-module (crates-io pi ne pinecone-client) #:use-module (crates-io))

(define-public crate-pinecone-client-0.1.0 (c (n "pinecone-client") (v "0.1.0") (h "1nah7pr0jx3rm8a5zj62wiiy36ayiq05indc8fcl5rn3gsyym1lf") (y #t)))

(define-public crate-pinecone-client-0.1.1 (c (n "pinecone-client") (v "0.1.1") (h "1q318jmnw14x6dvbx2hdfgsmv170r74kng41fj71fhi0j2n33cdr") (y #t)))

(define-public crate-pinecone-client-0.1.2 (c (n "pinecone-client") (v "0.1.2") (h "17kxkaaq159mnc82864dr1kyv2xy03zgxjwy3nlbvgmn4lns7b23") (y #t)))

(define-public crate-pinecone-client-0.1.3 (c (n "pinecone-client") (v "0.1.3") (h "19al1qwbkxcj6k6pxaapl6gj95hp117a7zkhwp0mksa5y4b68q3f") (y #t)))

