(define-module (crates-io pi ne pine64-bl602-freertos-riscv-ram) #:use-module (crates-io))

(define-public crate-pine64-bl602-freertos-riscv-ram-0.1.0+1.6.11-1-g66bb28da (c (n "pine64-bl602-freertos-riscv-ram") (v "0.1.0+1.6.11-1-g66bb28da") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)))) (h "1374wfzj9ckbbn8h9kjc4affni30hb99cal449q6sp3b620hb5is") (y #t) (l "pine64-bl602-freertos-riscv-ram")))

(define-public crate-pine64-bl602-freertos-riscv-ram-0.1.1+1.6.11-1-g66bb28da (c (n "pine64-bl602-freertos-riscv-ram") (v "0.1.1+1.6.11-1-g66bb28da") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)))) (h "1c5hvcj8iyk759sw6frj4wcpxlcrw0q4mqw0bbcfc2m38sgbyvn1") (y #t) (l "pine64-bl602-freertos-riscv-ram")))

(define-public crate-pine64-bl602-freertos-riscv-ram-0.1.2+1.6.11-1-g66bb28da (c (n "pine64-bl602-freertos-riscv-ram") (v "0.1.2+1.6.11-1-g66bb28da") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)))) (h "0ch5n6fhyi2srj4ll0ldrdqq66m4s7kl0i9gfqlinvrj7d0510v0") (y #t) (l "pine64-bl602-freertos-riscv-ram")))

