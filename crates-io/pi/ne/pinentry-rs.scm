(define-module (crates-io pi ne pinentry-rs) #:use-module (crates-io))

(define-public crate-pinentry-rs-0.1.0 (c (n "pinentry-rs") (v "0.1.0") (d (list (d (n "secstr") (r "^0.3.0") (d #t) (k 0)))) (h "06bm0g5ynl6kiysgafcsnxxga05z6rnihs8v1yqcnnjfldac7c0a")))

(define-public crate-pinentry-rs-0.1.1 (c (n "pinentry-rs") (v "0.1.1") (d (list (d (n "secstr") (r "^0.4.0") (d #t) (k 0)))) (h "0414mhdch675q4pld9rxh4kzxjiiiin2labi01pfmnqif8c1fa7c")))

(define-public crate-pinentry-rs-0.1.2 (c (n "pinentry-rs") (v "0.1.2") (d (list (d (n "secstr") (r "^0.4.0") (d #t) (k 0)))) (h "1k5wzkmahi5hi27481a911s17bbqp9w79nn0096rw3ak779mn710")))

(define-public crate-pinentry-rs-0.2.0 (c (n "pinentry-rs") (v "0.2.0") (d (list (d (n "secstr") (r "^0.5.0") (d #t) (k 0)))) (h "11if39v9wpqb55y6xjrijh7ih0k4fp5vn3ij5sbbpxpmhmgcqh2d")))

