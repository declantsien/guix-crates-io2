(define-module (crates-io pi xg pixglyph) #:use-module (crates-io))

(define-public crate-pixglyph-0.1.0 (c (n "pixglyph") (v "0.1.0") (d (list (d (n "ttf-parser") (r "^0.18") (d #t) (k 0)))) (h "12zx71y4qxjbnjp9yvsgzrnqm09vi5zmikfdr0ay6pvijg9svvwy")))

(define-public crate-pixglyph-0.2.0 (c (n "pixglyph") (v "0.2.0") (d (list (d (n "ttf-parser") (r "^0.19") (d #t) (k 0)))) (h "13wg9dp7l6a2h3fnh9lngfy952mccsqdlnnq3hyfcs363zr92xgn")))

(define-public crate-pixglyph-0.3.0 (c (n "pixglyph") (v "0.3.0") (d (list (d (n "ttf-parser") (r "^0.20") (d #t) (k 0)))) (h "04m7l6y49nimb8pw9x6mqxjqcy248p2c705q4n0v6z8r9jnziq72")))

