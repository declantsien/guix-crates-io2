(define-module (crates-io pi ro pirox) #:use-module (crates-io))

(define-public crate-pirox-0.0.2 (c (n "pirox") (v "0.0.2") (d (list (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "1mr936n421fnspdav3lv1wf681ca8mqjm1h3x0c7kbpil818vqyf")))

(define-public crate-pirox-0.0.3 (c (n "pirox") (v "0.0.3") (d (list (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "09iakl8y7zckh9hn0f740dfgnd77xpy6n9h3950gj7bafih459v0")))

