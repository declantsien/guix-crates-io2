(define-module (crates-io pi xm pixman) #:use-module (crates-io))

(define-public crate-pixman-0.1.0 (c (n "pixman") (v "0.1.0") (d (list (d (n "drm-fourcc") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pixman-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1dji0jgvhj63fnyndzp769svhn63lcvihp297bjg857c1gd28jnj") (f (quote (("default")))) (s 2) (e (quote (("drm-fourcc" "dep:drm-fourcc")))) (r "1.65.0")))

