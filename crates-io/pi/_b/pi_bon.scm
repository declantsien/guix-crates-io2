(define-module (crates-io pi _b pi_bon) #:use-module (crates-io))

(define-public crate-pi_bon-0.1.0 (c (n "pi_bon") (v "0.1.0") (d (list (d (n "pi_data_view") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "19s587vs3589dlszqaahskx85w1f8c5k06vkyg1arcrvcwhc67k6")))

(define-public crate-pi_bon-0.2.0 (c (n "pi_bon") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "pi_data_view") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "075mqqwzbchqzqh50xwlhim4rssj8xg0z85xf4m111kgsw6v2jvx")))

(define-public crate-pi_bon-0.1.1 (c (n "pi_bon") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "pi_data_view") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0lhpnxbqyibndsknncf4q7h32icywsxfjnl9pgqxbh7y1maf348i")))

(define-public crate-pi_bon-0.1.3 (c (n "pi_bon") (v "0.1.3") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "pi_data_view") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1r1lmqzqilziprqzz7sp4yh66fdywdcx3bh75ih9hbyl2llz1ad7")))

(define-public crate-pi_bon-0.2.1 (c (n "pi_bon") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "pi_data_view") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "01w8j8glbahmj04wb4aj7d614l6lrclcy604jpp7kcr9mnr31rsb")))

(define-public crate-pi_bon-0.2.2 (c (n "pi_bon") (v "0.2.2") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "031vrsi2x8wsh7wcqdncs1hn8qq9b0jyhniqg5j2cr5lk53mbqhs")))

(define-public crate-pi_bon-0.2.3 (c (n "pi_bon") (v "0.2.3") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0gdwifb8vs7m5nd8cjcrs6gd6760skpifqxr9934jiza1hc3l8gn")))

