(define-module (crates-io pi nn pinned) #:use-module (crates-io))

(define-public crate-pinned-0.1.0 (c (n "pinned") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.24") (f (quote ("std" "async-await"))) (k 0)) (d (n "rustversion") (r "^1.0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0nsrxs49dhjjz1gvg0pvac2rcidnwwd8l99y7vhwym2yv5xh4ad8") (r "1.60.0")))

