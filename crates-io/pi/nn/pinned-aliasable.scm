(define-module (crates-io pi nn pinned-aliasable) #:use-module (crates-io))

(define-public crate-pinned-aliasable-0.1.0 (c (n "pinned-aliasable") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 2)))) (h "1qnzqz5np70f6gh5w31w2vqzsylpzikgkh4a64pnqzc34qi66j77")))

(define-public crate-pinned-aliasable-0.1.1 (c (n "pinned-aliasable") (v "0.1.1") (d (list (d (n "pin-project") (r "^1.0.7") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 2)))) (h "080ibjq91dm77a2m9p372l3l6hrpshkdsbw6bbcvdp9ax825kfbk")))

(define-public crate-pinned-aliasable-0.1.2 (c (n "pinned-aliasable") (v "0.1.2") (d (list (d (n "pin-project") (r "^1.0.7") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 2)))) (h "1vla4shmxyq028icdwji8zk1qkzddj3gb7syzm3qpfwr951zp7x0")))

(define-public crate-pinned-aliasable-0.1.3 (c (n "pinned-aliasable") (v "0.1.3") (d (list (d (n "pin-project") (r "^1.0.7") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 2)))) (h "0yqbhlmiqn3xwvr1s0a8akxb82bf5vmggwy1kav07vghkgl9l3sx")))

