(define-module (crates-io pi nn pinned_sync) #:use-module (crates-io))

(define-public crate-pinned_sync-0.0.1 (c (n "pinned_sync") (v "0.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "03153ccv1h2wc637b3maxxqbihbhl5bp0z2mk7vsm1zh22hgaz4n")))

