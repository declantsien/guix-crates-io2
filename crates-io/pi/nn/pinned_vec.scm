(define-module (crates-io pi nn pinned_vec) #:use-module (crates-io))

(define-public crate-pinned_vec-0.1.0 (c (n "pinned_vec") (v "0.1.0") (h "0ic2wwl97wcl1mc340nvbid9c8ssagvi4hxaczxkxddm1y4qhhvi")))

(define-public crate-pinned_vec-0.1.1 (c (n "pinned_vec") (v "0.1.1") (h "12il2y234k9r4i3v9yh7qpavrwdhi48b057z96hb0bv2j8nxi2i6")))

