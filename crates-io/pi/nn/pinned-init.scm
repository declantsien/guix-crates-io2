(define-module (crates-io pi nn pinned-init) #:use-module (crates-io))

(define-public crate-pinned-init-0.0.0 (c (n "pinned-init") (v "0.0.0") (d (list (d (n "pin-project") (r "^1.0.0") (d #t) (k 0)) (d (n "pinned-init-macro") (r "=0.0.0") (d #t) (k 0)))) (h "1q81kp1gzam6v646q5wmw885q904b44xnfpskrr47px5zngyppbn") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-pinned-init-0.0.1 (c (n "pinned-init") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0djzd7jg06hy45dc5lfzdll32s5ppqqzji28f2mkckn42cihj871") (f (quote (("std") ("never_type") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-pinned-init-0.0.2 (c (n "pinned-init") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "11y0xfvaczk1y26ngna59z7b219hdjw3c9k6qfcz7yjrlq1yn7yr") (f (quote (("std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-pinned-init-0.0.3 (c (n "pinned-init") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pinned-init-macro") (r "=0.0.1") (d #t) (k 0)))) (h "19w9w2mnmy6si2pv00yj80bhqcach5mjhrf8jgmwj55rhw41ag9r") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-pinned-init-0.0.4 (c (n "pinned-init") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pinned-init-macro") (r "=0.0.2") (d #t) (k 0)))) (h "0qxh8rk57h8aaqf89n3bij9vplsqvgf7mdvy44wgazslsh26zbh4") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-pinned-init-0.0.5 (c (n "pinned-init") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pinned-init-macro") (r "=0.0.3") (d #t) (k 0)))) (h "1hgw4a4ynpa57d57wk0mxxi1vrlfayyq5vj009yd52f4vl60pfsm") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-pinned-init-0.0.6 (c (n "pinned-init") (v "0.0.6") (d (list (d (n "compiletest_rs") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pinned-init-macro") (r "=0.0.4") (d #t) (k 0)))) (h "17899aij40k3gmhjv4nrv5x4dri9ncy1am8ivkrwbsb1zxm1ji41") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-pinned-init-0.0.7 (c (n "pinned-init") (v "0.0.7") (d (list (d (n "compiletest_rs") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pinned-init-macro") (r "=0.0.5") (d #t) (k 0)))) (h "1plwyfrmrlrvqv6q4mc1fi56x2z6fz60q89czxbpbv3gh082c3fj") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

