(define-module (crates-io pi nn pinned-bucket) #:use-module (crates-io))

(define-public crate-pinned-bucket-0.1.0 (c (n "pinned-bucket") (v "0.1.0") (h "06yhndhj94k9v75nzwdi069x1kv0lx0a6awkkmnq74388zjv7r7g")))

(define-public crate-pinned-bucket-0.1.1 (c (n "pinned-bucket") (v "0.1.1") (h "19792sblqmsi3vy44xf9sb013hkgsgy8h4xfw5xin1hjsncqfd5s")))

(define-public crate-pinned-bucket-0.1.2 (c (n "pinned-bucket") (v "0.1.2") (h "0q2964b7f3nc3cafb8x10sybgj9pmcbig37kwng1r5a7p6w7pnll")))

(define-public crate-pinned-bucket-0.1.3 (c (n "pinned-bucket") (v "0.1.3") (h "0i1qr6wvy65cmjixfiywwddbvmh9qmckk5d2vcnwh680z56ci01n")))

(define-public crate-pinned-bucket-0.1.4 (c (n "pinned-bucket") (v "0.1.4") (h "0gylcp3i436mjl7z53kv3z9iy3mzm8m9nf0wzz48wnlfj0pff1k3")))

(define-public crate-pinned-bucket-0.1.5 (c (n "pinned-bucket") (v "0.1.5") (h "0bwq7i5732sk338x6pg5i0g0cp5qm5bh2lj0ylfirw6sdclwjm64")))

(define-public crate-pinned-bucket-0.2.0 (c (n "pinned-bucket") (v "0.2.0") (h "0nynf6ypbagy5xf36crv3q46f2fipvjnbl65j1nwmkm9adwb337d")))

(define-public crate-pinned-bucket-0.2.1 (c (n "pinned-bucket") (v "0.2.1") (h "0szcnhn604prk0cl7f4h0s4mxzhyn8320v58h64a72b9f6zrz2yy")))

(define-public crate-pinned-bucket-0.2.2 (c (n "pinned-bucket") (v "0.2.2") (h "0q9wfcn56k7jny9sczwf5l9g16n6wc6jh3dsgzz93v4zi8cmqf89")))

(define-public crate-pinned-bucket-0.3.0 (c (n "pinned-bucket") (v "0.3.0") (h "105vax84739jvfam43ncj9npan03ajrqlljrj6cbzzrb1n5rapm0")))

