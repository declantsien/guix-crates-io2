(define-module (crates-io pi nn pinned-mutex) #:use-module (crates-io))

(define-public crate-pinned-mutex-0.1.0 (c (n "pinned-mutex") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1.1") (d #t) (k 2)))) (h "1xw29kkjs00a2hgj4lbkaa0gz8553dpf7fw863nqk5ixr7q5g2zn") (r "1.60")))

(define-public crate-pinned-mutex-0.2.0 (c (n "pinned-mutex") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1.1") (d #t) (k 2)))) (h "198d3nns5iabvwshkrd8wr506arv144g8w1mnwij8hz2mz0vmkin") (r "1.60")))

(define-public crate-pinned-mutex-0.3.0 (c (n "pinned-mutex") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1.1") (d #t) (k 2)))) (h "0hf3s1splhpahggnjwhgkv87rci8vjwma9lpcdm9fv7xzqc9hl5j") (r "1.60")))

