(define-module (crates-io pi nn pinned-repos) #:use-module (crates-io))

(define-public crate-pinned-repos-0.1.0 (c (n "pinned-repos") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vnysvpyabgy00lvpk1xmky8i4f5jgv6dl9xybxdac8q8z6jlmb7")))

