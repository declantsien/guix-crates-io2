(define-module (crates-io pi nn pinned-init-macro) #:use-module (crates-io))

(define-public crate-pinned-init-macro-0.0.0 (c (n "pinned-init-macro") (v "0.0.0") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k1l5s40vcxinnxrj17vwpv80sr79qk0hpwg0mhncq7bxkj9digk") (y #t)))

(define-public crate-pinned-init-macro-0.0.1 (c (n "pinned-init-macro") (v "0.0.1") (h "0g0jgf7mhrghwbdwqbla2is2qrbcv2nm4bghfaw86c8b7xqnz964")))

(define-public crate-pinned-init-macro-0.0.2 (c (n "pinned-init-macro") (v "0.0.2") (h "0mkz71wbyvrfcxzjjv7jm9p45x52x3myfyd8614qzzmasdpdpghd")))

(define-public crate-pinned-init-macro-0.0.3 (c (n "pinned-init-macro") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0dfi1qll6g8z4fv4p3ail020mgsgwkpxm94cyqprwnp6p3whsjhf")))

(define-public crate-pinned-init-macro-0.0.4 (c (n "pinned-init-macro") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "04dzrcm8m2w55rwrlnmd4k6bpf972xy39b01f3vlzr5akz8649vd")))

(define-public crate-pinned-init-macro-0.0.5 (c (n "pinned-init-macro") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1m4b2daypxs0rarbsqapvjfawc5sk4xgh1qmdk2ccd331v7kz6jn")))

