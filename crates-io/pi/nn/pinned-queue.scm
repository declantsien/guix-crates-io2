(define-module (crates-io pi nn pinned-queue) #:use-module (crates-io))

(define-public crate-pinned-queue-0.1.0 (c (n "pinned-queue") (v "0.1.0") (h "0x7i19pk3gi8s0qvzj50hd03gh7c2f1jmbsdqxpf9n7s91zd8llb")))

(define-public crate-pinned-queue-0.1.1 (c (n "pinned-queue") (v "0.1.1") (h "0yaxc121ynpc29wl2ygzingmpay3b97xfmq1py5pq8qzqzanwhx2")))

