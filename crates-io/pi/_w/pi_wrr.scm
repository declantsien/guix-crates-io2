(define-module (crates-io pi _w pi_wrr) #:use-module (crates-io))

(define-public crate-pi_wrr-0.1.1 (c (n "pi_wrr") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1.9") (d #t) (k 2)) (d (n "quanta") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "st3") (r "^0.4") (d #t) (k 2)))) (h "1hh33r2yqy14ipfb0blicyh0v20nnjhmkdakcy6nf90r58vl7kch")))

(define-public crate-pi_wrr-0.1.0 (c (n "pi_wrr") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1.9") (d #t) (k 2)) (d (n "quanta") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "st3") (r "^0.4") (d #t) (k 2)))) (h "0j951jchx2j89w7vc57382z7chq3hh6y98y8vpswiipl9072lz5q")))

