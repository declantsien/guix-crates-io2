(define-module (crates-io pi _w pi_weight_task) #:use-module (crates-io))

(define-public crate-pi_weight_task-0.1.0 (c (n "pi_weight_task") (v "0.1.0") (d (list (d (n "pi_cancel_timer") (r "^0.1") (d #t) (k 0)) (d (n "pi_ext_heap") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "pi_slot_wheel") (r "^0.1") (d #t) (k 0)) (d (n "pi_timer") (r "^0.1") (d #t) (k 0)) (d (n "pi_weight") (r "^0.1") (d #t) (k 0)) (d (n "pi_wheel") (r "^0.1") (d #t) (k 0)) (d (n "pi_wy_rng") (r "^0.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "066yfgifddhp9bi2nbp8lc84mz55n6mpamx58k5ns73q7i50xvsv")))

