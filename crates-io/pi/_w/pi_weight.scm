(define-module (crates-io pi _w pi_weight) #:use-module (crates-io))

(define-public crate-pi_weight-0.1.0 (c (n "pi_weight") (v "0.1.0") (d (list (d (n "pcg_rand") (r "^0.13") (d #t) (k 2)) (d (n "pi_ext_heap") (r "^0.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)))) (h "0bmysl3bvqx4c1ivgz3l4y5rhnz08cm1alydh4a3b3sylkglq7yr")))

