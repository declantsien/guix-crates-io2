(define-module (crates-io pi _w pi_wtree) #:use-module (crates-io))

(define-public crate-pi_wtree-0.1.0 (c (n "pi_wtree") (v "0.1.0") (d (list (d (n "pi_dyn_uint") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 2)))) (h "085gzldzmr0p6zfw335rpc60lfdxc0cr4x4h2vf9vs5aizm3khay")))

