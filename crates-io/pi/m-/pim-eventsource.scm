(define-module (crates-io pi m- pim-eventsource) #:use-module (crates-io))

(define-public crate-pim-eventsource-0.1.0 (c (n "pim-eventsource") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)) (d (n "sled") (r "^0.34.3") (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "00h7jccdzbhsmijpb090k7jzis0lilzk54h7aiwf3grgx47b4gyk")))

