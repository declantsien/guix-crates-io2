(define-module (crates-io pi e_ pie_format) #:use-module (crates-io))

(define-public crate-pie_format-1.0.0 (c (n "pie_format") (v "1.0.0") (d (list (d (n "png") (r "^0.17.7") (d #t) (k 0)))) (h "1f1i3rw92ma44sn61slrddbq62mjg9b5hgr4qscwinjbp33s58av")))

(define-public crate-pie_format-1.0.1 (c (n "pie_format") (v "1.0.1") (d (list (d (n "png") (r "^0.17.7") (d #t) (k 0)))) (h "0w2pmhd3fqx690zm44y8gm0phk6p0647hr3an8dd5jw7w41ys6yn")))

