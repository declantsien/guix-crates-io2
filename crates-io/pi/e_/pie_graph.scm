(define-module (crates-io pi e_ pie_graph) #:use-module (crates-io))

(define-public crate-pie_graph-0.0.1 (c (n "pie_graph") (v "0.0.1") (d (list (d (n "hashlink") (r "^0.8") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "slotmap") (r "^1") (d #t) (k 0)))) (h "00sb5il3mh5xyjsv4gs7mvfm8mp7qqjj5cwfbnryp8brsvqf3qrs") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "slotmap/serde" "hashlink/serde_impl"))))))

