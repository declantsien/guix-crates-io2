(define-module (crates-io pi nf pinfish) #:use-module (crates-io))

(define-public crate-pinfish-0.1.0-alpha (c (n "pinfish") (v "0.1.0-alpha") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "pinfish-macros") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "argh") (r "^0.1.8") (d #t) (k 2)))) (h "04xkpk0pfrwgvkr5yhrl54mjaf611561i07x60b00a627mgbqnhv")))

(define-public crate-pinfish-0.1.0-alpha1 (c (n "pinfish") (v "0.1.0-alpha1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "pinfish-macros") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "argp") (r "^0.1") (d #t) (k 2)))) (h "0gf6kkyb40v18windsrzlh31a5vdih4h0l9l330lymxariwr0q4r")))

(define-public crate-pinfish-0.1.0-alpha.2 (c (n "pinfish") (v "0.1.0-alpha.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "pinfish-macros") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "argp") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "198rryc96i0w40l7l09jxjinjdg2cghch8wf89nrw3wanags5qp7")))

(define-public crate-pinfish-0.0.1 (c (n "pinfish") (v "0.0.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "pinfish-macros") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "argp") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0i2v6v27x6mlnlbcr8lg3ndr4spmkv60f85favx99nc8rz437d39")))

