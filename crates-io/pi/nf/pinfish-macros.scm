(define-module (crates-io pi nf pinfish-macros) #:use-module (crates-io))

(define-public crate-pinfish-macros-0.1.0-alpha (c (n "pinfish-macros") (v "0.1.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sivzjgq1ha3njr15dwr3ihnc28cv2pdw7ws1g0mb7ssf4jn7lga")))

