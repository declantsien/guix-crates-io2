(define-module (crates-io pi _n pi_new) #:use-module (crates-io))

(define-public crate-pi_new-0.1.1 (c (n "pi_new") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1v46zg5rhw7fyx98mjx55n0n630yrcm6q5qmwpy00kvdb98rb9n6")))

(define-public crate-pi_new-0.1.4 (c (n "pi_new") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1s4pg0rq4j6qrh5i1c345y6s267y7qgklbkznh09r88crcyk1ifn")))

(define-public crate-pi_new-0.1.5 (c (n "pi_new") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0nyf2b2jwqslfawk8zqldxflj9y4svn6jj6xm0hvg5yyp5cx2xad")))

(define-public crate-pi_new-1.1.6 (c (n "pi_new") (v "1.1.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1yg6misnyd8f2q3b3yv2ydl78dyclan0mg7ms5jl85qj15yv5apk")))

(define-public crate-pi_new-1.1.7 (c (n "pi_new") (v "1.1.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0wg3ga6y2ikicjx8k0m0ps9hrwgkp999lvi4nidj5rr4v4qy71wh")))

(define-public crate-pi_new-1.1.8 (c (n "pi_new") (v "1.1.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10fcymd90kr5lqkwnmn1x7gv70iczk0cmchppnd3xlfvxa0aaml2")))

(define-public crate-pi_new-0.1.0 (c (n "pi_new") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "02mkqgr2n1hwpbn9fkj1pkhjidr6mj6bmdkz1ijxqnlxhmpmarbd")))

(define-public crate-pi_new-0.1.2 (c (n "pi_new") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1aqdlyzyaaggqd8alp98q3kagy1xrb902j2hza26c72fcrpnp8s0")))

(define-public crate-pi_new-0.1.6 (c (n "pi_new") (v "0.1.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1mx6rrdp5bvdhinlrq31z4lcdnaqqmzm53njh0j6x8sqa8cbkh3v")))

(define-public crate-pi_new-0.1.7 (c (n "pi_new") (v "0.1.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1nd8ix1lg2r69nmhwxwb0x1jhl6zaxnrsk16b61k51icp09igbyv")))

