(define-module (crates-io pi _n pi_null) #:use-module (crates-io))

(define-public crate-pi_null-0.1.0 (c (n "pi_null") (v "0.1.0") (h "1ydvvqnrgp4b4saz4xm76g2r1fvvh2dvxvqh9jdz58rqk253f97b")))

(define-public crate-pi_null-0.2.0 (c (n "pi_null") (v "0.2.0") (h "0cx37nchsnn1wp70g19d0c76hznlg0x39slcrq3vw8basb1hns38") (y #t) (r "1.70")))

(define-public crate-pi_null-0.2.1 (c (n "pi_null") (v "0.2.1") (h "1gvkn6rpk89xdd68fimzf8fqp5c8w1zzspb7160gawn7a2q862ad") (y #t) (r "1.72")))

(define-public crate-pi_null-0.1.2 (c (n "pi_null") (v "0.1.2") (h "03anhc8hiqyv7czmwd11nz0nf2a474nl0brs6fxihkcq12qk0a89")))

(define-public crate-pi_null-0.1.3 (c (n "pi_null") (v "0.1.3") (h "0am42cvlfj9d9xyyrwf4z1cjs2kddfa3yxk7h304ijx7gpxn2v0v")))

(define-public crate-pi_null-0.1.4 (c (n "pi_null") (v "0.1.4") (h "02jka598773r6419jk98ymik8602vvd8m5bfwnjnyp8r7j00ggij")))

(define-public crate-pi_null-0.1.5 (c (n "pi_null") (v "0.1.5") (h "0c5iiqbkydr64pl42kj1rkll4fhhv61gnvrj2mky9qns85gaxg37")))

(define-public crate-pi_null-0.1.6 (c (n "pi_null") (v "0.1.6") (h "0mz4fr53kz80rd8n2dcdc0wc5ji2zlykqjs4nqrj59z7dmmnr3f3")))

(define-public crate-pi_null-0.1.7 (c (n "pi_null") (v "0.1.7") (h "118wskggvc44qcr5lbvvamg49cgxd3hlir1q5wvkzhr6zvkfl9ks")))

(define-public crate-pi_null-0.1.8 (c (n "pi_null") (v "0.1.8") (h "0h3m2d79i4c29wb2kkr60mx7r51kij18x8yps7b1h76cx9acvc2s")))

(define-public crate-pi_null-0.1.9 (c (n "pi_null") (v "0.1.9") (h "1pb201r6xia730sk5522rzflqcg2fis3rwkcnhwppw57d2j7y8mf")))

(define-public crate-pi_null-0.1.10 (c (n "pi_null") (v "0.1.10") (h "0fc96xjamnv2xhnjiz9r3hinynr5vvfh62dh0cqiq0i000f77lbm")))

(define-public crate-pi_null-0.1.11 (c (n "pi_null") (v "0.1.11") (h "0swba1zpklb0zqr194m9fijzs23xcj965hv8n1amfph2p8xfka0r")))

