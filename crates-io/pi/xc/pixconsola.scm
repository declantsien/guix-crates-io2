(define-module (crates-io pi xc pixconsola) #:use-module (crates-io))

(define-public crate-pixconsola-0.1.0 (c (n "pixconsola") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "enum-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)) (d (n "transpose") (r "^0.2") (d #t) (k 0)))) (h "16lan8g9la3iqmlcyllqxkvzq8m8505v13w04jf5ys7yd5r2iw7l")))

