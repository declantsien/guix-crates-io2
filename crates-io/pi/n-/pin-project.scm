(define-module (crates-io pi n- pin-project) #:use-module (crates-io))

(define-public crate-pin-project-0.1.0 (c (n "pin-project") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.9") (f (quote ("proc-macro" "parsing" "printing" "full"))) (k 0)))) (h "10m8y3z2gmjljw94zqf7vbh8ifa84ir07zjadwwrj6wz8dm86nv3") (y #t)))

(define-public crate-pin-project-0.1.1 (c (n "pin-project") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.9") (f (quote ("proc-macro" "parsing" "printing" "full"))) (k 0)))) (h "0s12anppdkxg9r7z0f1m0ykz4qx6bdm0p7lf31a52k3n36lw1m95")))

(define-public crate-pin-project-0.1.2 (c (n "pin-project") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.9") (f (quote ("proc-macro" "parsing" "printing" "full"))) (k 0)))) (h "0wsnvwppl69fvyiabjd8a9aaw3paijy4nk9cq7755p8vmrnnma1i")))

(define-public crate-pin-project-0.1.3 (c (n "pin-project") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("proc-macro" "parsing" "printing" "full"))) (k 0)))) (h "06hb28ax9yvjd3vmjwdy4afgd3myq5hn5784j1iagavqn5af6vdi")))

(define-public crate-pin-project-0.1.4 (c (n "pin-project") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("proc-macro" "parsing" "printing" "clone-impls" "full"))) (k 0)))) (h "1p9a9rfyczw8715b7sqckw1il3nc14cn32a04qc2ppzkf75jw9y7")))

(define-public crate-pin-project-0.1.5 (c (n "pin-project") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("proc-macro" "parsing" "printing" "clone-impls" "full"))) (k 0)))) (h "04wwhvwa0rivqhi2yx53m8x9jfrywmbj7npniiqnb3mr9x2p5a1j")))

(define-public crate-pin-project-0.1.6 (c (n "pin-project") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("proc-macro" "parsing" "printing" "clone-impls" "full"))) (k 0)))) (h "07w7hlyg60y3h890ndwrsj9mhplfm95rb5as9i5w5k9ab21bmjx3") (f (quote (("unsafe_variants") ("unsafe_fields") ("default" "unsafe_fields"))))))

(define-public crate-pin-project-0.1.7 (c (n "pin-project") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("proc-macro" "parsing" "printing" "clone-impls" "full"))) (k 0)))) (h "018d1h05c7dj11v0ibaw7igsvj04yk1ri1wjr00746svk53khqqx") (f (quote (("unsafe_variants") ("unsafe_fields") ("default" "unsafe_fields"))))))

(define-public crate-pin-project-0.1.8 (c (n "pin-project") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("proc-macro" "parsing" "printing" "clone-impls" "full"))) (k 0)))) (h "0yivpzb42yl2hyfq7mpvbv5j9xwshpha9rf1wfsa0ld9x61899cs") (f (quote (("unsafe_variants") ("unsafe_fields") ("project_attr" "syn/visit-mut") ("default" "project_attr" "unsafe_fields"))))))

(define-public crate-pin-project-0.2.0 (c (n "pin-project") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("proc-macro" "parsing" "printing" "clone-impls" "full"))) (k 0)))) (h "0mxkb57q925nzk09acy6kr0ml5pcf773pbcbwmf7y96csyygxzf0") (f (quote (("unsafe_variants") ("unsafe_fields") ("project_attr" "syn/visit-mut") ("default" "project_attr"))))))

(define-public crate-pin-project-0.2.1 (c (n "pin-project") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("proc-macro" "parsing" "printing" "clone-impls" "full"))) (k 0)))) (h "0l4xbdavi26agfdzqw6kcvxwsx74hb2hc408dcgxjylgcxyjsl7c") (f (quote (("unsafe_variants") ("unsafe_fields") ("project_attr" "syn/visit-mut") ("default" "project_attr"))))))

(define-public crate-pin-project-0.2.2 (c (n "pin-project") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("proc-macro" "parsing" "printing" "clone-impls" "full"))) (k 0)))) (h "1g6s07ka72jd9w18qmanzbczxhxsvgpa75qbmfqz30717msc2wvy") (f (quote (("unsafe_variants") ("unsafe_fields") ("project_attr" "syn/visit-mut") ("default" "project_attr"))))))

(define-public crate-pin-project-0.3.0 (c (n "pin-project") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("proc-macro" "parsing" "printing" "clone-impls" "full"))) (k 0)))) (h "1vnsqmlzh1yrwqx64hmn7dnzh1salslk740w1kahsl39w3w69dl7") (f (quote (("project_attr" "syn/visit-mut") ("default" "project_attr"))))))

(define-public crate-pin-project-0.3.1 (c (n "pin-project") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0hcx7h3gcnyklc9qbyhbivrzppz9zi3xp8dhbpilgfqhgw7rqb07") (f (quote (("project_attr" "syn/visit-mut") ("default" "project_attr"))))))

(define-public crate-pin-project-0.3.2 (c (n "pin-project") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0nrqqdy038hryabpz1fqc3rgy1axhms9c0pp8x3cc42y2g8q0zzk") (f (quote (("project_attr" "syn/visit-mut") ("default" "project_attr"))))))

(define-public crate-pin-project-0.3.3 (c (n "pin-project") (v "0.3.3") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1h6yz0vjx0am5ngryk2nrqfic6kc6mzmwv03l4s4qqiw9g1zfg9k") (f (quote (("project_attr" "syn/visit-mut") ("default" "project_attr")))) (y #t)))

(define-public crate-pin-project-0.3.4 (c (n "pin-project") (v "0.3.4") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full"))) (d #t) (k 0)))) (h "16nrl6rhrqi3741lvl7s2jdkilk37jf6i8iywwdyjgcnaa3jc517") (f (quote (("project_attr" "syn/visit-mut") ("default" "project_attr"))))))

(define-public crate-pin-project-0.4.0-alpha.1 (c (n "pin-project") (v "0.4.0-alpha.1") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.0-alpha.1") (k 0)))) (h "0ihdja10v0l3apv21mgg1a6dg59g7v0fkh08d8nn86h8dpka45gv") (f (quote (("renamed" "pin-project-internal/renamed") ("project_attr" "pin-project-internal/project_attr"))))))

(define-public crate-pin-project-0.4.0-alpha.2 (c (n "pin-project") (v "0.4.0-alpha.2") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.0-alpha.2") (k 0)))) (h "1gk60imb44y94jipnxdxxxqpq2zs3785w6ll07yw8i5lrrd8r6kp") (f (quote (("renamed" "pin-project-internal/renamed") ("project_attr" "pin-project-internal/project_attr"))))))

(define-public crate-pin-project-0.3.5 (c (n "pin-project") (v "0.3.5") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nfw93iqa3fls9xn9izbyzdycbqmx2xsikp9bpbvhlsrs79mc4d1") (f (quote (("project_attr" "syn/visit-mut") ("default" "project_attr"))))))

(define-public crate-pin-project-0.4.0-alpha.3 (c (n "pin-project") (v "0.4.0-alpha.3") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.0-alpha.3") (k 0)))) (h "17gp4bvn4kcydhamsjy4ckhi2lbvyra3kpnvpnbcva8hk0znmwm2") (f (quote (("renamed" "pin-project-internal/renamed") ("project_attr" "pin-project-internal/project_attr"))))))

(define-public crate-pin-project-0.4.0-alpha.4 (c (n "pin-project") (v "0.4.0-alpha.4") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.0-alpha.4") (k 0)))) (h "0fq2bw4d3d7kk26xn0pds7mnizfwpw4k8wqvk5y37ggy98zy1sp4") (f (quote (("renamed" "pin-project-internal/renamed") ("project_attr" "pin-project-internal/project_attr"))))))

(define-public crate-pin-project-0.4.0-alpha.5 (c (n "pin-project") (v "0.4.0-alpha.5") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.0-alpha.5") (k 0)))) (h "1xr1x8nssmalvsrzqq8b64hkjdrvdixwm02b7935ajyi59mdvry6") (f (quote (("renamed" "pin-project-internal/renamed") ("project_attr" "pin-project-internal/project_attr"))))))

(define-public crate-pin-project-0.4.0-alpha.6 (c (n "pin-project") (v "0.4.0-alpha.6") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.0-alpha.6") (k 0)))) (h "0mqg1a9s6360inhnjmxvx731yj7815mhxb6bb1r38c103j88v1p1") (f (quote (("renamed" "pin-project-internal/renamed") ("project_attr" "pin-project-internal/project_attr"))))))

(define-public crate-pin-project-0.4.0-alpha.7 (c (n "pin-project") (v "0.4.0-alpha.7") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.0-alpha.7") (k 0)))) (h "0dmig0xq3l4byqw08rw6bil3snf1ai4pgsvx0kpc1dh7yxiq9593") (f (quote (("renamed" "pin-project-internal/renamed") ("project_attr" "pin-project-internal/project_attr"))))))

(define-public crate-pin-project-0.4.0-alpha.8 (c (n "pin-project") (v "0.4.0-alpha.8") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.0-alpha.8") (k 0)))) (h "1blraniynq25c3gh6g26f2vysz927ivk00kiffcl60568jnwhw4l") (f (quote (("renamed" "pin-project-internal/renamed") ("project_attr" "pin-project-internal/project_attr"))))))

(define-public crate-pin-project-0.4.0-alpha.9 (c (n "pin-project") (v "0.4.0-alpha.9") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.0-alpha.9") (k 0)))) (h "01g0a47p3wznxhcm8c9gr1fanim92v0bbazn41fb1k0hphpxndap") (f (quote (("renamed" "pin-project-internal/renamed") ("project_attr" "pin-project-internal/project_attr"))))))

(define-public crate-pin-project-0.4.0-alpha.10 (c (n "pin-project") (v "0.4.0-alpha.10") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.0-alpha.10") (k 0)))) (h "1x9q9lblbib3hr41828ihr86zkrrndmk7pqhrxn09hww8g301arp") (f (quote (("renamed" "pin-project-internal/renamed") ("project_attr" "pin-project-internal/project_attr"))))))

(define-public crate-pin-project-0.4.0-alpha.11 (c (n "pin-project") (v "0.4.0-alpha.11") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.0-alpha.11") (k 0)))) (h "18wvpwc219jpa4xgms23f5snyfllbvdczy3p8rjzn2if1c9qrkm3") (f (quote (("renamed" "pin-project-internal/renamed") ("project_attr" "pin-project-internal/project_attr"))))))

(define-public crate-pin-project-0.4.0-beta.1 (c (n "pin-project") (v "0.4.0-beta.1") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.0-beta.1") (k 0)))) (h "130zhxx5gysawdvs3j5rzfs0iba63yaipbhhysfjr5bblqx836qi")))

(define-public crate-pin-project-0.4.0 (c (n "pin-project") (v "0.4.0") (d (list (d (n "compiletest") (r "^0.3.21") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.0") (k 0)))) (h "190rwjdbm0q7lx2kxrng97xcw3kmhgppwnimn3jrk6c96ziplc1f") (y #t)))

(define-public crate-pin-project-0.4.1 (c (n "pin-project") (v "0.4.1") (d (list (d (n "compiletest") (r "= 0.3.22") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.1") (k 0)))) (h "0paca5p496i8f6c2zac4jgv3q29hlr4m75x1ipp4bxdw4x7wpb9w") (y #t)))

(define-public crate-pin-project-0.4.2 (c (n "pin-project") (v "0.4.2") (d (list (d (n "compiletest") (r "= 0.3.22") (f (quote ("stable" "tmp"))) (d #t) (k 2) (p "compiletest_rs")) (d (n "pin-project-internal") (r "= 0.4.2") (k 0)))) (h "1sva1qgvlsf3w10imccyp37j9bwdfd4dh326q3n31bkrb7m5d49x") (y #t)))

(define-public crate-pin-project-0.4.3 (c (n "pin-project") (v "0.4.3") (d (list (d (n "pin-project-internal") (r "= 0.4.3") (k 0)))) (h "0979hn6y0yjwxq1lr8d0bd87qr3pi632q7iylllabch5rmfra0qq") (y #t)))

(define-public crate-pin-project-0.4.4 (c (n "pin-project") (v "0.4.4") (d (list (d (n "pin-project-internal") (r "= 0.4.4") (k 0)))) (h "140dpxrfzwid9zzgngwmylc20r4s2nbh0xm13z7k2ll67s7wab2n") (y #t)))

(define-public crate-pin-project-0.4.5 (c (n "pin-project") (v "0.4.5") (d (list (d (n "pin-project-internal") (r "= 0.4.5") (k 0)))) (h "1vcnzz5f5nbx826r1xicyqzrqw4lygzn7rb8z2ikhhsf5c2fgz65") (y #t)))

(define-public crate-pin-project-0.4.6 (c (n "pin-project") (v "0.4.6") (d (list (d (n "pin-project-internal") (r "= 0.4.6") (k 0)))) (h "0sg434b2szi9xm2yw19i6pma0knycq9vk7q66jslqvi1qx303fcl") (y #t)))

(define-public crate-pin-project-0.4.7 (c (n "pin-project") (v "0.4.7") (d (list (d (n "pin-project-internal") (r "= 0.4.7") (k 0)))) (h "1id81czw12yih4xdlg4xs6lj7f6sccmxg03vsfi0rxi1zz2a3z3m") (y #t)))

(define-public crate-pin-project-0.4.8 (c (n "pin-project") (v "0.4.8") (d (list (d (n "pin-project-internal") (r "= 0.4.8") (k 0)))) (h "0b3pfbqfy3hhgq62zvbw7m0a4d55mrgnll9w8l9jymyrm1is813q") (y #t)))

(define-public crate-pin-project-0.4.9 (c (n "pin-project") (v "0.4.9") (d (list (d (n "pin-project-internal") (r "= 0.4.9") (k 0)))) (h "18i66hjxg93ch1ayh18r7lxrvshaq1ahqhwkqxcwb4k2xrg7yskg") (y #t)))

(define-public crate-pin-project-0.4.10 (c (n "pin-project") (v "0.4.10") (d (list (d (n "pin-project-internal") (r "= 0.4.10") (k 0)))) (h "0qq010591r7ahn97pxdar7aabnchhdfjrlj1z1k5mh484vadrqrn") (y #t)))

(define-public crate-pin-project-0.4.11 (c (n "pin-project") (v "0.4.11") (d (list (d (n "pin-project-internal") (r "= 0.4.11") (k 0)))) (h "0vkwnhvyfcn8cdd4slnzlda6h5l0wb6xfbqpvk82azagghiia9zl") (y #t)))

(define-public crate-pin-project-0.4.12 (c (n "pin-project") (v "0.4.12") (d (list (d (n "pin-project-internal") (r "= 0.4.12") (k 0)))) (h "16hay3izmilcnqshsr1s0phalj6kwad2w4bz69cisc1jlddnila8") (y #t)))

(define-public crate-pin-project-0.4.13 (c (n "pin-project") (v "0.4.13") (d (list (d (n "pin-project-internal") (r "= 0.4.13") (k 0)))) (h "0parsaiim2nis1cq50d6rsq0w8lcfsyln8y7k2agjhmvnnzvzhw2") (y #t)))

(define-public crate-pin-project-0.4.14 (c (n "pin-project") (v "0.4.14") (d (list (d (n "pin-project-internal") (r "= 0.4.14") (k 0)))) (h "1whni6c7hi3dz2jgs7z90c345q339l4v39ckrsam589ww770ggib") (y #t)))

(define-public crate-pin-project-0.4.15 (c (n "pin-project") (v "0.4.15") (d (list (d (n "pin-project-internal") (r "= 0.4.15") (k 0)))) (h "0gf67a32vc6ql7fzb9fjdihsx1z3653xy5h2w1zi26all2sdll1w") (y #t)))

(define-public crate-pin-project-0.4.16 (c (n "pin-project") (v "0.4.16") (d (list (d (n "pin-project-internal") (r "= 0.4.16") (k 0)))) (h "1lb32r1nhzgxw5k8zcizz6jvg021z6dfs3kdm76jqll99v5q1m41") (y #t)))

(define-public crate-pin-project-0.4.17 (c (n "pin-project") (v "0.4.17") (d (list (d (n "pin-project-internal") (r "= 0.4.17") (k 0)))) (h "14bpkzj4q3hqbkdvz2klqnqyl8zz36rhwx6g835hxrimwzp3mjgd") (y #t)))

(define-public crate-pin-project-0.4.18 (c (n "pin-project") (v "0.4.18") (d (list (d (n "pin-project-internal") (r "=0.4.18") (k 0)))) (h "18wqmp5zrq3x5dxvfd1dmpcx8gsdx02ngz1saly9wa7bfx5bfvqc") (y #t)))

(define-public crate-pin-project-0.4.19 (c (n "pin-project") (v "0.4.19") (d (list (d (n "pin-project-internal") (r "=0.4.19") (k 0)))) (h "12kywjhycm2dyz6js9fqsaaky12gk3prfd37iagq8w1y9b7ilfms") (y #t)))

(define-public crate-pin-project-0.4.20 (c (n "pin-project") (v "0.4.20") (d (list (d (n "pin-project-internal") (r "=0.4.20") (k 0)))) (h "069rhbdy323xxbxxqq3n47mylnd10qx36qdw36xi5l9pj3zp6lz7") (y #t)))

(define-public crate-pin-project-0.4.21 (c (n "pin-project") (v "0.4.21") (d (list (d (n "pin-project-internal") (r "=0.4.21") (k 0)))) (h "0qq5d0kj2r2lyvis4crs54nafszyyr2s0mgqpmw1pi1awl61fi5h") (y #t)))

(define-public crate-pin-project-0.4.22 (c (n "pin-project") (v "0.4.22") (d (list (d (n "pin-project-internal") (r "=0.4.22") (k 0)))) (h "05wwxy46j9z27ibbiisjqk0rivf0z00h4al1f92mwjp9pz6sdqqj") (y #t)))

(define-public crate-pin-project-0.4.23 (c (n "pin-project") (v "0.4.23") (d (list (d (n "pin-project-internal") (r "=0.4.23") (k 0)))) (h "1aj8ivjr7bw3dmnp53zj10a0fx6ij3lqx7vx94p38ydfybzk6i6a") (y #t)))

(define-public crate-pin-project-1.0.0-alpha.1 (c (n "pin-project") (v "1.0.0-alpha.1") (d (list (d (n "pin-project-internal") (r "=1.0.0-alpha.1") (k 0)))) (h "1dv0d6qiamgdgajv30jm9s9rv4hpxgamq2s84rvfizla9nsa5p90")))

(define-public crate-pin-project-0.4.24 (c (n "pin-project") (v "0.4.24") (d (list (d (n "pin-project-internal") (r "=0.4.24") (k 0)))) (h "01bjlxd56laazv5qh2bzpr9izwisk5xlvxbwpqvk91gzzdyav3zl") (y #t)))

(define-public crate-pin-project-0.4.25 (c (n "pin-project") (v "0.4.25") (d (list (d (n "pin-project-internal") (r "=0.4.25") (k 0)))) (h "0kql3ydwla063iwl9rm0jkhgil9v7cbq4995b4axjjw59022i7ib") (y #t)))

(define-public crate-pin-project-0.4.26 (c (n "pin-project") (v "0.4.26") (d (list (d (n "pin-project-auxiliary-macro") (r "^0") (d #t) (k 2)) (d (n "pin-project-internal") (r "=0.4.26") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "05yczgmhnqk8cr3951fgk5c9fwj09apqcbs58szcjggfppbdzyqk") (y #t)))

(define-public crate-pin-project-0.4.27 (c (n "pin-project") (v "0.4.27") (d (list (d (n "pin-project-auxiliary-macro") (r "^0") (d #t) (k 2)) (d (n "pin-project-internal") (r "=0.4.27") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "05cfd6sp0ydkdfw7b3c8s1sws0xmhclylam9icnkvsiq9glwiyrg") (y #t)))

(define-public crate-pin-project-1.0.0 (c (n "pin-project") (v "1.0.0") (d (list (d (n "pin-project-auxiliary-macro") (r "^0") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.0.0") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1mfmk9g63d0bi8m5ary7ylnj111dd7sf7wysp7hpww2gi1rbmn7p") (y #t)))

(define-public crate-pin-project-1.0.1 (c (n "pin-project") (v "1.0.1") (d (list (d (n "pin-project-auxiliary-macro") (r "^0") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.0.1") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0h9qyx9napp34j5d7jk11xnlbirmdgxklkh7sxcxjq2gfhwdhhgf") (y #t)))

(define-public crate-pin-project-1.0.2 (c (n "pin-project") (v "1.0.2") (d (list (d (n "pin-project-auxiliary-macro") (r "^0") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.0.2") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "19qw2nm2kk38v9j16nsm8j3fkh0g8pjq0k4cplx7i2f4q8vj5k4w")))

(define-public crate-pin-project-1.0.3 (c (n "pin-project") (v "1.0.3") (d (list (d (n "pin-project-internal") (r "=1.0.3") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1rvmv48ynnnh526ylxhinyy9ypw5113p8qan6ijvmmma753810ss")))

(define-public crate-pin-project-1.0.4 (c (n "pin-project") (v "1.0.4") (d (list (d (n "pin-project-internal") (r "=1.0.4") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1wm2gjmgcsglw8yqka67fhvcj7zj1fh6yfw6awlal5wza1l0pdwm")))

(define-public crate-pin-project-1.0.5 (c (n "pin-project") (v "1.0.5") (d (list (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.0.5") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0qqbmxna4b49p80qnhlbnzvdi2q6p22l8da4y5vl8717j2xqxyln")))

(define-public crate-pin-project-1.0.6 (c (n "pin-project") (v "1.0.6") (d (list (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.0.6") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "19hva0wx1zm1v4n5vp4qdbljj6n9jpd7l63mwldfj1l8frclh5xw")))

(define-public crate-pin-project-0.4.28 (c (n "pin-project") (v "0.4.28") (d (list (d (n "pin-project-internal") (r "=0.4.28") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "03rx11vd15rwj2g3c5x79f1154fykpag88fj1hgda6ciqnsr50ci") (y #t)))

(define-public crate-pin-project-1.0.7 (c (n "pin-project") (v "1.0.7") (d (list (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.0.7") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1964rh32hiy5v2ircli8wv8fxq9h2nkgfalda6j407040v0rql67")))

(define-public crate-pin-project-1.0.8 (c (n "pin-project") (v "1.0.8") (d (list (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.0.8") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "020c5nyj7qpyqn6xwy3hw6vgm31mifq9g7p916n5vihc480chssp")))

(define-public crate-pin-project-0.4.29 (c (n "pin-project") (v "0.4.29") (d (list (d (n "pin-project-internal") (r "=0.4.29") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "ryu") (r "=1.0.6") (d #t) (k 2)) (d (n "serde_json") (r "=1.0.72") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "02f93m6qq0hiq7mmf9wqn7kph4nw9lk9jd06zzlpjx8k666w25cn") (r "1.34")))

(define-public crate-pin-project-1.0.9 (c (n "pin-project") (v "1.0.9") (d (list (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.0.9") (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "13s3h5yg338bgkvs070f6dnnf3lnc2y6lkphrxh82j08wly128hn") (r "1.37")))

(define-public crate-pin-project-1.0.10 (c (n "pin-project") (v "1.0.10") (d (list (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.0.10") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "0pm7qfsczqnb3d6y530zk6xng239m0cagijbg124xbrvmmwkibaq") (r "1.37")))

(define-public crate-pin-project-0.4.30 (c (n "pin-project") (v "0.4.30") (d (list (d (n "pin-project-internal") (r "=0.4.30") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "ryu") (r "=1.0.6") (d #t) (k 2)) (d (n "serde_json") (r "=1.0.72") (d #t) (k 2)) (d (n "toml") (r "=0.5.8") (d #t) (k 2)) (d (n "trybuild") (r "=1.0.53") (d #t) (k 2)))) (h "0nlxmsiq39bc73iryh92yslrp2jzlkdjjxd7rv5sjzpflljgkw1y") (r "1.34")))

(define-public crate-pin-project-1.0.11 (c (n "pin-project") (v "1.0.11") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.0.11") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "0q522b4byy73cky74lx8g5fl9pm4dib5vlx2wh0vxzwcqj1kw83q") (r "1.37")))

(define-public crate-pin-project-1.0.12 (c (n "pin-project") (v "1.0.12") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.0.12") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "1k3f9jkia3idxl2pqxamszwnl89dk52fa4jqj3p7zmmwnq4scadd") (r "1.37")))

(define-public crate-pin-project-1.1.0 (c (n "pin-project") (v "1.1.0") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.67") (d #t) (k 2)))) (h "1bgfkh430jzm0xdr4xw1y62120pk18kd0wxgdqv1xawyf5v78nn9") (r "1.56")))

(define-public crate-pin-project-1.1.1 (c (n "pin-project") (v "1.1.1") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.67") (d #t) (k 2)))) (h "1gdrlz72lh0vzgnnwvlv8wk12wlcwnvq0khv1qmpm433hbfqy4vf") (r "1.56")))

(define-public crate-pin-project-1.1.2 (c (n "pin-project") (v "1.1.2") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.1.2") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.67") (d #t) (k 2)))) (h "0hl83z8p0rq4jld71avk9154vv6zicapz0qdrd28j2mi9nyd42h3") (r "1.56")))

(define-public crate-pin-project-1.1.3 (c (n "pin-project") (v "1.1.3") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "pin-project-internal") (r "=1.1.3") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.67") (d #t) (k 2)))) (h "08k4cpy8q3j93qqgnrbzkcgpn7g0a88l4a9nm33kyghpdhffv97x") (r "1.56")))

(define-public crate-pin-project-1.1.4 (c (n "pin-project") (v "1.1.4") (d (list (d (n "pin-project-internal") (r "=1.1.4") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1q07737j774zxffdrypncdsrc5zx7dffw6l4dzanni9c8jhc80h3") (r "1.56")))

(define-public crate-pin-project-1.1.5 (c (n "pin-project") (v "1.1.5") (d (list (d (n "pin-project-internal") (r "=1.1.5") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1cxl146x0q7lawp0m1826wsgj8mmmfs6ja8q7m6f7ff5j6vl7gxn") (r "1.56")))

