(define-module (crates-io pi n- pin-init) #:use-module (crates-io))

(define-public crate-pin-init-0.1.0 (c (n "pin-init") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pin-init-internal") (r "=0.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 2)))) (h "1jjzqklwq99hyxlch4awls30irimy9d6xa86xz35rxr6vxv65dlc") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-pin-init-0.1.1 (c (n "pin-init") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pin-init-internal") (r "=0.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 2)))) (h "06712wiknl9wkqszfjnxic9m38qklff6sqdwk3sa5yi20inabjr4") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pin-init-0.2.0 (c (n "pin-init") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pin-init-internal") (r "=0.2.0") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 2)))) (h "0526badzg8nijdx4kkl0brylss8kqx9fqmn465xi94pphslxszqw") (f (quote (("default" "alloc" "alloc_pin_with") ("alloc_try_pin_with" "alloc") ("alloc_pin_with" "alloc") ("alloc"))))))

