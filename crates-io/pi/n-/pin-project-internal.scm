(define-module (crates-io pi n- pin-project-internal) #:use-module (crates-io))

(define-public crate-pin-project-internal-0.4.0-alpha.1 (c (n "pin-project-internal") (v "0.4.0-alpha.1") (d (list (d (n "lazy_static") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0qvp82zq0l5i4k2mhlhfg81vmm0m3xgzklv0ryadgkq1dl2xx21z") (f (quote (("renamed" "proc-macro-crate" "serde" "lazy_static") ("project_attr" "syn/visit-mut"))))))

(define-public crate-pin-project-internal-0.4.0-alpha.2 (c (n "pin-project-internal") (v "0.4.0-alpha.2") (d (list (d (n "lazy_static") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0-alpha") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p9s9af8b4p2c8cm0rj7b4xc655g0a0lqjgcbv6rg277883b7rk5") (f (quote (("renamed" "proc-macro-crate" "serde" "lazy_static") ("project_attr" "syn/visit-mut"))))))

(define-public crate-pin-project-internal-0.4.0-alpha.3 (c (n "pin-project-internal") (v "0.4.0-alpha.3") (d (list (d (n "lazy_static") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0-alpha") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d7g3i03pk1bz91dnzdx7vv8l923dx1gklafc9ik661406860k4x") (f (quote (("renamed" "proc-macro-crate" "serde" "lazy_static") ("project_attr" "syn/visit-mut"))))))

(define-public crate-pin-project-internal-0.4.0-alpha.4 (c (n "pin-project-internal") (v "0.4.0-alpha.4") (d (list (d (n "lazy_static") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0-alpha") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1siy5jr9v35p5dlkpg66931xqjnlk7yh7fi545nnylxkkbk3rdii") (f (quote (("renamed" "proc-macro-crate" "serde" "lazy_static") ("project_attr" "syn/visit-mut"))))))

(define-public crate-pin-project-internal-0.4.0-alpha.5 (c (n "pin-project-internal") (v "0.4.0-alpha.5") (d (list (d (n "lazy_static") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^0.4.0-alpha") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lrgmzlkxybdag7xw5za7pg04p8g6zzll8xg4619c8npzz8hgglc") (f (quote (("renamed" "proc-macro-crate" "serde" "lazy_static") ("project_attr" "syn/visit-mut"))))))

(define-public crate-pin-project-internal-0.4.0-alpha.6 (c (n "pin-project-internal") (v "0.4.0-alpha.6") (d (list (d (n "lazy_static") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "serde") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ppa85na3zjsdkxxpzn3402cns4k7cksb9q334a31j45645w1sgd") (f (quote (("renamed" "proc-macro-crate" "serde" "lazy_static") ("project_attr" "syn/visit-mut"))))))

(define-public crate-pin-project-internal-0.4.0-alpha.7 (c (n "pin-project-internal") (v "0.4.0-alpha.7") (d (list (d (n "lazy_static") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "serde") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ipvmw6qm21wg8vc8fi7a3hj66qkqrzn5g0b4vwn10j9hsb618n6") (f (quote (("renamed" "proc-macro-crate" "serde" "lazy_static") ("project_attr" "syn/visit-mut"))))))

(define-public crate-pin-project-internal-0.4.0-alpha.8 (c (n "pin-project-internal") (v "0.4.0-alpha.8") (d (list (d (n "lazy_static") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "serde") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p2iy925g95v6b468y2rsnk016nnirrgpzaxxkj4imvyhfc4msvp") (f (quote (("renamed" "proc-macro-crate" "serde" "lazy_static") ("project_attr" "syn/visit-mut"))))))

(define-public crate-pin-project-internal-0.4.0-alpha.9 (c (n "pin-project-internal") (v "0.4.0-alpha.9") (d (list (d (n "lazy_static") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1frl869k7sc0bjv195na62shw521ycjp2949vkj08mq7a0zkh3qh") (f (quote (("renamed" "proc-macro-crate" "serde" "lazy_static") ("project_attr" "syn/visit-mut"))))))

(define-public crate-pin-project-internal-0.4.0-alpha.10 (c (n "pin-project-internal") (v "0.4.0-alpha.10") (d (list (d (n "lazy_static") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ccdd4kwz0ksd04hklxf7927dbvfz5av0dmkjjgdhwhwhf757rrj") (f (quote (("renamed" "proc-macro-crate" "serde" "lazy_static") ("project_attr" "syn/visit-mut"))))))

(define-public crate-pin-project-internal-0.4.0-alpha.11 (c (n "pin-project-internal") (v "0.4.0-alpha.11") (d (list (d (n "lazy_static") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i79jz7p2l8c0ism27q3rnqmi21cqil51w2l2n6bx5852y20lqpy") (f (quote (("renamed" "proc-macro-crate" "serde" "lazy_static") ("project_attr" "syn/visit-mut"))))))

(define-public crate-pin-project-internal-0.4.0-beta.1 (c (n "pin-project-internal") (v "0.4.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "14ybq83y3l05kb395jcicsx5i489rjllr7376jijchx7rmq83y4f")))

(define-public crate-pin-project-internal-0.4.0 (c (n "pin-project-internal") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1la5qm2mkvaw4q5a1wpj8fgnqxc4rz447vgr1hsshn0m2j7la8jm")))

(define-public crate-pin-project-internal-0.4.1 (c (n "pin-project-internal") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1gs8kd6ark47g327433b4j99s19dml4a1gp5mbf188j68vvmaqj4")))

(define-public crate-pin-project-internal-0.4.2 (c (n "pin-project-internal") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1npkq8kyby96dxwspp3yslmsk8g0ygic35gv0a1kvxbvrpzmydqs")))

(define-public crate-pin-project-internal-0.4.3 (c (n "pin-project-internal") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0763yy4l9k71c2kacgzxnq0ms4yv7jvnzfpd6jnxz4b4408sgrq8")))

(define-public crate-pin-project-internal-0.4.4 (c (n "pin-project-internal") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "07kzm7bll952zyz31a1yv18bhrpc79xqc92f3z35rsqlgqsvr991")))

(define-public crate-pin-project-internal-0.4.5 (c (n "pin-project-internal") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1wxb9j5z5dqc9ikiawqm8b283dzm6a3xldg7czv3awn23irb8i3n")))

(define-public crate-pin-project-internal-0.4.6 (c (n "pin-project-internal") (v "0.4.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0mc3dk62z4nn1f3lx9d4avi4vfcr5c7xsn41p8y6srghjgw95jj4")))

(define-public crate-pin-project-internal-0.4.7 (c (n "pin-project-internal") (v "0.4.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1mlhdaab54yrag80fgcsm9c8vygfprs81ivfb83n3kna9r7csi35")))

(define-public crate-pin-project-internal-0.4.8 (c (n "pin-project-internal") (v "0.4.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0gwvrd8y9x1zfhwqw9qg3l2yi1969ad58aldqq839krfbyj24lrq")))

(define-public crate-pin-project-internal-0.4.9 (c (n "pin-project-internal") (v "0.4.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "16nrp90357zi6j177g9s5pihnk1nv9nw0yqif8k6in4hww647249")))

(define-public crate-pin-project-internal-0.4.10 (c (n "pin-project-internal") (v "0.4.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0scr26l6v7g3nq69gm1lx2x5a9p231v8whbaw2b15zvpqmm39mzl")))

(define-public crate-pin-project-internal-0.4.11 (c (n "pin-project-internal") (v "0.4.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0iww722gawm18m86wc606d50n6jiys8lirg4iblk9xwdsvxfmaxg")))

(define-public crate-pin-project-internal-0.4.12 (c (n "pin-project-internal") (v "0.4.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "17fxdm3mq94ikyjj51c04az88g97fjpfzbr3kbngwlpfxlxr9wxk")))

(define-public crate-pin-project-internal-0.4.13 (c (n "pin-project-internal") (v "0.4.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0qwgs8xhlc5x5vngh2hqmg6s66nzp22v09jm04n5c67vvi4n9gyc")))

(define-public crate-pin-project-internal-0.4.14 (c (n "pin-project-internal") (v "0.4.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1ikd953zvn8dyhnan3r4sqs36lhj2iwzhgl6m62skr8h3b2rwy0v")))

(define-public crate-pin-project-internal-0.4.15 (c (n "pin-project-internal") (v "0.4.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0yw3r5h5bfhb617d76grysy48av0kkzbhisdzmx7fxjc3nnfa9vc")))

(define-public crate-pin-project-internal-0.4.16 (c (n "pin-project-internal") (v "0.4.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1srgwycdsxapmdba5np3rvnw2cfy33w5sjxihnv1kjzw3vqrcad8")))

(define-public crate-pin-project-internal-0.4.17 (c (n "pin-project-internal") (v "0.4.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0h3ym8qya4h0nkrq9hrnhcx1kfbcsc7w82dysqxwkd553c4b53g5")))

(define-public crate-pin-project-internal-0.4.18 (c (n "pin-project-internal") (v "0.4.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "030kwijkm4y3irg9z18p091knd3hy21l0rwc8sahnih0zlazvsz1")))

(define-public crate-pin-project-internal-0.4.19 (c (n "pin-project-internal") (v "0.4.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0rgjncsri7bs4kvvjyqsq4v856zdbycscdpfnhpf18viic28hkhr")))

(define-public crate-pin-project-internal-0.4.20 (c (n "pin-project-internal") (v "0.4.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0vdya7f3msv7bz41di9iir3y06n4wzymqshxg9071hykjd4b9d0h")))

(define-public crate-pin-project-internal-0.4.21 (c (n "pin-project-internal") (v "0.4.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1fpaqwg2qrjiy54dpmiznynn19sgwadkyg0ss1znfz0d7k77dgds")))

(define-public crate-pin-project-internal-0.4.22 (c (n "pin-project-internal") (v "0.4.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1xxac6f3ip45zqbfcmmk748ywjw9sbavz1fcswvqgn3rrx2zs3va")))

(define-public crate-pin-project-internal-0.4.23 (c (n "pin-project-internal") (v "0.4.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "07s66r7c6dirkvcjhqsndaf3qmvwl45c28dgypyk38797rf823ic")))

(define-public crate-pin-project-internal-1.0.0-alpha.1 (c (n "pin-project-internal") (v "1.0.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1999xrmj3gkkr8k7148d6zn8r0mc64dyxnwm3ymhj9s8pp04lszd")))

(define-public crate-pin-project-internal-0.4.24 (c (n "pin-project-internal") (v "0.4.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1bjqs6pvr9apmx3irfm0nqzy8mbqrz358y4rjwb5mjnkpn9x5ii4")))

(define-public crate-pin-project-internal-0.4.25 (c (n "pin-project-internal") (v "0.4.25") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1apdj55mw8i0v7ic474jg9549siwj45mcw0rsz1s9ikbcx9b7j68")))

(define-public crate-pin-project-internal-0.4.26 (c (n "pin-project-internal") (v "0.4.26") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "13nc5nl0cyfplyrr7ns1ryqi39apgm118b9mrx93ab33kwrb2by8")))

(define-public crate-pin-project-internal-0.4.27 (c (n "pin-project-internal") (v "0.4.27") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "158q986q00s1pz1akazdcjkvkbdcbdhfw5azw8g3mgkadgjjmbb5")))

(define-public crate-pin-project-internal-1.0.0 (c (n "pin-project-internal") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0ayg36x05xd0jigc4d0q4plyjbyz0ir9wkhr1kg4k5phcszg5ggk")))

(define-public crate-pin-project-internal-1.0.1 (c (n "pin-project-internal") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "11igpgxqnqlfim2zrmy8k82dr9s9yqfhim4400sgysxnjjjzz941")))

(define-public crate-pin-project-internal-1.0.2 (c (n "pin-project-internal") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.44") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0pwy3m32scf3ypjb9ai151lmaa27vyj06lc64i28l0r31fzx5s7q")))

(define-public crate-pin-project-internal-1.0.3 (c (n "pin-project-internal") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.44") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0flbvqy34ng3y21s8zqqm4glj62p66xyqpqwphaksi3kixmw9g5p")))

(define-public crate-pin-project-internal-1.0.4 (c (n "pin-project-internal") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.44") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0wcy3n9w856aw27f6ns2ipd2yac7jbl0n3pmn0cyhb7jjdimm8na")))

(define-public crate-pin-project-internal-1.0.5 (c (n "pin-project-internal") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "02scl9dsvlg5nnlxcfb6x9d1jnhbmkvl32x1s95zgijq6np6k1km")))

(define-public crate-pin-project-internal-1.0.6 (c (n "pin-project-internal") (v "1.0.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1ra9j3q9yiwz3agfmr07chrlz31dzqxlw91z10dysmp832ck5454")))

(define-public crate-pin-project-internal-0.4.28 (c (n "pin-project-internal") (v "0.4.28") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.44") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0pianl8ma0ihhara39swdddpa3hrv08k5iv46b9dkrhb6006gqiv")))

(define-public crate-pin-project-internal-1.0.7 (c (n "pin-project-internal") (v "1.0.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0vs289my2262ziwxj60mnzr2k41ibga73z8yddah1dc34l9m1ja8")))

(define-public crate-pin-project-internal-1.0.8 (c (n "pin-project-internal") (v "1.0.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "12gkrjd7xzwa3vlhpzxbxlbwisi2dw8l1q62v867zkhl7lbfi3vf")))

(define-public crate-pin-project-internal-0.4.29 (c (n "pin-project-internal") (v "0.4.29") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.44") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0ami799a794z5a8h190f20vqzwby0kkbqnwxkpadkvhrf1168j84") (r "1.34")))

(define-public crate-pin-project-internal-1.0.9 (c (n "pin-project-internal") (v "1.0.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0xiyp9jgfk93950b33j4pilg3k0zlhfaqmnk47bpdpwkxrpzanmr")))

(define-public crate-pin-project-internal-1.0.10 (c (n "pin-project-internal") (v "1.0.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1frnrghhxzkwn636q3s2917r8fgxd98dnx7jz983d7525c4nyjvl")))

(define-public crate-pin-project-internal-0.4.30 (c (n "pin-project-internal") (v "0.4.30") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.44") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "07p6mwz6kz317a6n3p93yk4llj939ihqdz7d1mwl7gmyx468s745") (r "1.34")))

(define-public crate-pin-project-internal-1.0.11 (c (n "pin-project-internal") (v "1.0.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0x2c20v0hw44sdz026hm164yvhcax426vlq128v4acxkw5ssy3vi")))

(define-public crate-pin-project-internal-1.0.12 (c (n "pin-project-internal") (v "1.0.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0maa6icn7rdfy4xvgfaq7m7bwpw9f19wg76f1ncsiixd0lgdp6q6") (r "1.37")))

(define-public crate-pin-project-internal-1.1.0 (c (n "pin-project-internal") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "01wggqhdy0wm1ycwaq44ayip7bb7smyj9075wv2ynd42j9q7ch1r") (r "1.56")))

(define-public crate-pin-project-internal-1.1.1 (c (n "pin-project-internal") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1irnjk0bf7w24m8qjzckvmpaabg85n2ygdjgacny3qq3nc8z9zni") (r "1.56")))

(define-public crate-pin-project-internal-1.1.2 (c (n "pin-project-internal") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "173d7wxb4qaijwddsx6g0fn7d3hq5i0db61kn5qw8kp9rhp0fbpc") (r "1.56")))

(define-public crate-pin-project-internal-1.1.3 (c (n "pin-project-internal") (v "1.1.3") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "01a4l3vb84brv9v7wl71chzxra2kynm6yvcjca66xv3ij6fgsna3") (r "1.56")))

(define-public crate-pin-project-internal-1.1.4 (c (n "pin-project-internal") (v "1.1.4") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "141nmib9lqbisrf8rz6fayy6l4fiw2r547h6af6npiy9c0mh8v16") (r "1.56")))

(define-public crate-pin-project-internal-1.1.5 (c (n "pin-project-internal") (v "1.1.5") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.25") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0r9r4ivwiyqf45sv6b30l1dx282lxaax2f6gl84jwa3q590s8f1g") (r "1.56")))

