(define-module (crates-io pi n- pin-cell) #:use-module (crates-io))

(define-public crate-pin-cell-0.1.0 (c (n "pin-cell") (v "0.1.0") (h "08kxbph1n009a41dn9q77vh14vblhxmqad30d17vg5nka3dh031i")))

(define-public crate-pin-cell-0.1.1 (c (n "pin-cell") (v "0.1.1") (h "01x1gdcp2l8hf5ajclacmla4rmi37qncs4j5chhwzg8d6q3kx8cc")))

(define-public crate-pin-cell-0.2.0 (c (n "pin-cell") (v "0.2.0") (h "0yqa32vv9knsg6vm1ix8zlv9bk85wp49qzab2s021y65sgmw9x71")))

