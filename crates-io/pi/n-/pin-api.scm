(define-module (crates-io pi n- pin-api) #:use-module (crates-io))

(define-public crate-pin-api-0.1.0 (c (n "pin-api") (v "0.1.0") (h "1l5w8jcnmn3vnwh8h8wc3id8pzx8g5f2wmv6n6v1js6bjr81jv40") (f (quote (("std") ("default-features" "std"))))))

(define-public crate-pin-api-0.1.1 (c (n "pin-api") (v "0.1.1") (h "0llcmm6dqabfihiyrpn4m973w5zrscxrdfsb5q7py7ibdbsqy9px") (f (quote (("std") ("default" "std"))))))

(define-public crate-pin-api-0.1.2 (c (n "pin-api") (v "0.1.2") (h "1zcdabx3ndc08p23i8d5qbbl1micqi35sf91c3zgich6hy8qa8f0") (f (quote (("std") ("nightly") ("default" "std" "nightly"))))))

(define-public crate-pin-api-0.1.3 (c (n "pin-api") (v "0.1.3") (h "16d9q5an0c4y6jda4yrwjcc0dg5gn8ipl241sn9r7kw7383fkvgq") (f (quote (("std") ("nightly") ("default" "std" "nightly"))))))

(define-public crate-pin-api-0.2.0 (c (n "pin-api") (v "0.2.0") (h "10rsahmmh0wg851y49ycbqsb7ln5z5nv0phhajqkblah8lxgi78c") (f (quote (("std") ("nightly") ("default" "std" "nightly"))))))

(define-public crate-pin-api-0.2.1 (c (n "pin-api") (v "0.2.1") (h "1bgp6hf3wyb5wkixxdchya3j80ha7vlpvq763zg9n9ysvgkfy1j8") (f (quote (("std") ("nightly") ("default" "std" "nightly"))))))

