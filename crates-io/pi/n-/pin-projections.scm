(define-module (crates-io pi n- pin-projections) #:use-module (crates-io))

(define-public crate-pin-projections-0.1.0 (c (n "pin-projections") (v "0.1.0") (h "127zx4lh7fnk7swslbjh8jzvqyzjq8cg3jdm92nd5jsmv2865s8l")))

(define-public crate-pin-projections-0.1.1 (c (n "pin-projections") (v "0.1.1") (h "164vyzwr3sjr5jk6hvxrwy6md9abs1dfqkdkdr6f01pchvg10hin")))

(define-public crate-pin-projections-0.2.0 (c (n "pin-projections") (v "0.2.0") (h "19nzvhvp3x2i1847y5hq98yg4ig7pjw91agsbwhp67a289vmr21w")))

(define-public crate-pin-projections-0.3.0 (c (n "pin-projections") (v "0.3.0") (h "0b73kdql5aa3825caxvx5sr6fcbsc6sx7h74010x6ar8bbl7ypcb")))

(define-public crate-pin-projections-0.3.1 (c (n "pin-projections") (v "0.3.1") (h "1bwpi141vkq3pk2972v0lh47h7l2sak8qxnw3jb67w9vanagpyn0")))

(define-public crate-pin-projections-0.4.0 (c (n "pin-projections") (v "0.4.0") (h "09a143llv47xcqr50hsvc2vbcnx6crgpdjh48jh397m5qz24g6ab")))

