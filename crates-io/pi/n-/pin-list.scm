(define-module (crates-io pi n- pin-list) #:use-module (crates-io))

(define-public crate-pin-list-0.1.0 (c (n "pin-list") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 2)) (d (n "pinned-aliasable") (r "^0.1.3") (d #t) (k 0)))) (h "0pkcsl9frrdq3l242b8xn4hpvf90bgyr4azzb3w5d2wlb96li4gy") (f (quote (("std" "alloc") ("alloc")))) (r "1.56.0")))

