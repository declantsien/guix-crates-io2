(define-module (crates-io pi n- pin-project-lite) #:use-module (crates-io))

(define-public crate-pin-project-lite-0.1.0 (c (n "pin-project-lite") (v "0.1.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "021lvlmrw1846p3955115kak0v5bn4sy48vw2lp40fzsvkz627sg") (y #t)))

(define-public crate-pin-project-lite-0.1.1 (c (n "pin-project-lite") (v "0.1.1") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "149rf3qk5gdvx5ryns3vw2cihr2bff6zn6gfja3cxqz6l2y6rbzh") (y #t)))

(define-public crate-pin-project-lite-0.1.2 (c (n "pin-project-lite") (v "0.1.2") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "08m4jk0pzr43jnf52xizz0my4grcl3plhq5ziq1jyibjpfw2x0p8") (y #t)))

(define-public crate-pin-project-lite-0.1.3 (c (n "pin-project-lite") (v "0.1.3") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0j1j4k1asqs4pbjbqi2zpm0npi1wljyl990xw2m96ps4f12harkj") (y #t)))

(define-public crate-pin-project-lite-0.1.4 (c (n "pin-project-lite") (v "0.1.4") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1bljczwz9yyb6jskjhbkilcbdg7v1mhfwzp2mxknzf7v1isl8y13") (y #t)))

(define-public crate-pin-project-lite-0.1.5 (c (n "pin-project-lite") (v "0.1.5") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0zznprlm9yhagpbxj6c1xqhb3ns894f1gxq8c47jwjbqppp5wl7p") (y #t)))

(define-public crate-pin-project-lite-0.1.6 (c (n "pin-project-lite") (v "0.1.6") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1bghrjpn93ndvk9zm07002wssyb2jri9am050bbsgww43nhjvwwx") (y #t)))

(define-public crate-pin-project-lite-0.1.7 (c (n "pin-project-lite") (v "0.1.7") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "05dp09xswfb18i2jmlvzkb0pd0fin9s3m64fgyksg6161zqxnai8") (y #t)))

(define-public crate-pin-project-lite-0.1.8 (c (n "pin-project-lite") (v "0.1.8") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1cy6va4w4ksqvwyjx58a5z8848wr1iny3byvnbzpc1p7y2j4kwvi") (y #t)))

(define-public crate-pin-project-lite-0.1.9 (c (n "pin-project-lite") (v "0.1.9") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1w0ryy3x3b63hqqby5kkhxkqw6djp4qxpq6cv066l58yg6blirsg") (y #t)))

(define-public crate-pin-project-lite-0.1.10 (c (n "pin-project-lite") (v "0.1.10") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "159wz66z3r4dbvxqykrykshwsycbmqyznfdmjyn848ahazkdjmg5") (y #t)))

(define-public crate-pin-project-lite-0.1.11 (c (n "pin-project-lite") (v "0.1.11") (d (list (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0srgdb3vkx7ppcww1qr7a67c7n84y01lq35j9g44z4h1z8x145y9") (y #t)))

(define-public crate-pin-project-lite-0.2.0 (c (n "pin-project-lite") (v "0.2.0") (d (list (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "070klqy200alrhxb79fxarrrrn0vbwg95dmqw9062vhqxibky1kb") (y #t)))

(define-public crate-pin-project-lite-0.2.1 (c (n "pin-project-lite") (v "0.2.1") (d (list (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1n22ljlhvy6zvb19jr2gh5604vhh9cnwwlp3q9a9kpycakbl6rz3") (y #t)))

(define-public crate-pin-project-lite-0.2.2 (c (n "pin-project-lite") (v "0.2.2") (d (list (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0g6ain5m0lnhz53h9brwj9ybw42lsl2hv9dp832bxkm40jvxx5k1") (y #t)))

(define-public crate-pin-project-lite-0.2.3 (c (n "pin-project-lite") (v "0.2.3") (d (list (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "05ssxwwima5sa12w3rsdgn2za2axslg4z67lfd868k2srjkf0dms") (y #t)))

(define-public crate-pin-project-lite-0.2.4 (c (n "pin-project-lite") (v "0.2.4") (d (list (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "09x8chns8apal89pndqdrr0c2nv8jw6nmi8hl38acjbc6sprg5j3") (y #t)))

(define-public crate-pin-project-lite-0.2.5 (c (n "pin-project-lite") (v "0.2.5") (d (list (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "12jlqmyw8sy3d5jc8ri4d3gg9z7wyl6rzjr2qz8kw0sb5r293x0c")))

(define-public crate-pin-project-lite-0.1.12 (c (n "pin-project-lite") (v "0.1.12") (d (list (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0xx7f3wzc8ydvd1v2mmrxfypjchp52bphrirf08phbq8ba8n8yr5")))

(define-public crate-pin-project-lite-0.2.6 (c (n "pin-project-lite") (v "0.2.6") (d (list (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "01g96zxghb33s1vsjmjpn9l3a2nxdqj7glf9lhq7q5wjkhjiy3nw")))

(define-public crate-pin-project-lite-0.2.7 (c (n "pin-project-lite") (v "0.2.7") (d (list (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0hwl8iyx3h9i3i3jr2vqj07nf4ay1v1w1ga29cbjmdd6d4fd2ccd")))

(define-public crate-pin-project-lite-0.2.8 (c (n "pin-project-lite") (v "0.2.8") (d (list (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "0v2c5ds2jqr84q0nc94dfhv8fs7lachl9sarf9992b66gkkzp072") (r "1.37")))

(define-public crate-pin-project-lite-0.2.9 (c (n "pin-project-lite") (v "0.2.9") (d (list (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "05n1z851l356hpgqadw4ar64mjanaxq1qlwqsf2k05ziq8xax9z0") (r "1.37")))

(define-public crate-pin-project-lite-0.2.10 (c (n "pin-project-lite") (v "0.2.10") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "once_cell") (r "=1.14") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde") (r "=1.0.156") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "toml") (r "=0.5.9") (d #t) (k 2)) (d (n "trybuild") (r "=1.0.67") (d #t) (k 2)))) (h "0mwx9wpkzsammdsygb73lil8ba3yap30b9b2r3q5y7lj059d4h2c") (r "1.37")))

(define-public crate-pin-project-lite-0.2.11 (c (n "pin-project-lite") (v "0.2.11") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "once_cell") (r "=1.14") (d #t) (k 2)) (d (n "proc-macro2") (r "=1.0.65") (d #t) (k 2)) (d (n "quote") (r "=1.0.30") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde") (r "=1.0.156") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "toml") (r "=0.5.9") (d #t) (k 2)) (d (n "trybuild") (r "=1.0.67") (d #t) (k 2)))) (h "070ivlvdiih2m0kri7n7hbpnhhvm7axnv246870d01v64h8ncl9c") (r "1.37")))

(define-public crate-pin-project-lite-0.2.12 (c (n "pin-project-lite") (v "0.2.12") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "once_cell") (r "=1.14") (d #t) (k 2)) (d (n "proc-macro2") (r "=1.0.65") (d #t) (k 2)) (d (n "quote") (r "=1.0.30") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde") (r "=1.0.156") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "toml") (r "=0.5.9") (d #t) (k 2)) (d (n "trybuild") (r "=1.0.67") (d #t) (k 2)))) (h "018a7yg2zjcfby4832yw7s9091mgy6syfm369fjpfykjy45ipk0j") (r "1.37")))

(define-public crate-pin-project-lite-0.2.13 (c (n "pin-project-lite") (v "0.2.13") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "once_cell") (r "=1.14") (d #t) (k 2)) (d (n "proc-macro2") (r "=1.0.65") (d #t) (k 2)) (d (n "quote") (r "=1.0.30") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde") (r "=1.0.156") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "toml") (r "=0.5.9") (d #t) (k 2)) (d (n "trybuild") (r "=1.0.67") (d #t) (k 2)))) (h "0n0bwr5qxlf0mhn2xkl36sy55118s9qmvx2yl5f3ixkb007lbywa") (r "1.37")))

(define-public crate-pin-project-lite-0.2.14 (c (n "pin-project-lite") (v "0.2.14") (d (list (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "00nx3f04agwjlsmd3mc5rx5haibj2v8q9b52b0kwn63wcv4nz9mx") (r "1.37")))

