(define-module (crates-io pi n- pin-utils) #:use-module (crates-io))

(define-public crate-pin-utils-0.1.0-alpha.1 (c (n "pin-utils") (v "0.1.0-alpha.1") (h "1bdyixkadzfl1iaxm3402a3cy91lb8pgy78716i2avl56h02qwq0")))

(define-public crate-pin-utils-0.1.0-alpha.2 (c (n "pin-utils") (v "0.1.0-alpha.2") (h "1bhdb1b3v0sm6sv9qj9nnj179af8c7z8navka75r84wys27a96ab")))

(define-public crate-pin-utils-0.1.0-alpha.3 (c (n "pin-utils") (v "0.1.0-alpha.3") (h "1czc8j2p4qzyma9pb6d4d37mg5j34ybrwwia4ladxhhcwiwjbm22")))

(define-public crate-pin-utils-0.1.0-alpha.4 (c (n "pin-utils") (v "0.1.0-alpha.4") (h "11xmyx00n4m37d546by2rxb8ryxs12v55cc172i3yak1rqccd52q")))

(define-public crate-pin-utils-0.1.0 (c (n "pin-utils") (v "0.1.0") (h "117ir7vslsl2z1a7qzhws4pd01cg2d3338c47swjyvqv2n60v1wb")))

