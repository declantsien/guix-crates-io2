(define-module (crates-io pi n- pin-queue) #:use-module (crates-io))

(define-public crate-pin-queue-0.1.0 (c (n "pin-queue") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (f (quote ("std"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 2)))) (h "1bk6qkr3rf3pn69q2w8ad9qs99vyan1xpfhc871dgsbbbg7scybd") (f (quote (("default" "critical-section")))) (y #t) (s 2) (e (quote (("parking_lot" "dep:parking_lot") ("critical-section" "dep:critical-section"))))))

(define-public crate-pin-queue-0.2.0 (c (n "pin-queue") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (f (quote ("std"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 2)))) (h "17b9216xki5zjxxzr83y8za2dx3ykckff8xnhgpjdiyi6w7shqii") (f (quote (("default" "critical-section")))) (y #t) (s 2) (e (quote (("parking_lot" "dep:parking_lot") ("critical-section" "dep:critical-section"))))))

