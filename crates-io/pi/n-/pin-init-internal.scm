(define-module (crates-io pi n- pin-init-internal) #:use-module (crates-io))

(define-public crate-pin-init-internal-0.1.0 (c (n "pin-init-internal") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nm04ljmbh2w2j6dmqp35qjx6l3wsaznb5d0rqgvdr8mq1qhsk1y")))

(define-public crate-pin-init-internal-0.2.0 (c (n "pin-init-internal") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0n1idzqr56nl77h1np7mphj809f07w9cw5yi5d9ad3n7hjna6l51")))

