(define-module (crates-io pi ct picturs) #:use-module (crates-io))

(define-public crate-picturs-0.0.0 (c (n "picturs") (v "0.0.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)))) (h "18a2mbmy5y0q37pgnqlilz9dn8af214xmfhr6ipmn9ljkr5p51c4")))

