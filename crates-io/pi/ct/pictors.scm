(define-module (crates-io pi ct pictors) #:use-module (crates-io))

(define-public crate-pictors-0.1.0 (c (n "pictors") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "092h6hkhgp88kxhnrrycbsw05k4msqgydzd5l7j99b2lii845kif")))

(define-public crate-pictors-0.1.1 (c (n "pictors") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "060sjqqh04wgbb3nykjxbllhm8aah9y5mvdfir1aa9b45say01cj")))

