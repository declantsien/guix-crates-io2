(define-module (crates-io pi -c pi-compression) #:use-module (crates-io))

(define-public crate-pi-compression-3.1.4 (c (n "pi-compression") (v "3.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fv23dyz76qfb3l87lkplh7kh9ggpfdlg4w3wy4g1jpwmqqvvxwc")))

