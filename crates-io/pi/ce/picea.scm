(define-module (crates-io pi ce picea) #:use-module (crates-io))

(define-public crate-picea-0.1.0 (c (n "picea") (v "0.1.0") (d (list (d (n "picea-macro-tools") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)) (d (n "speedy2d") (r "^2.0.0") (d #t) (k 2)))) (h "06m4ih0si5wwy37aj4ndmvalxqfhgsw6m8rn17yvdb3lg19h561g")))

