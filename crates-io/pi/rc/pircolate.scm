(define-module (crates-io pi rc pircolate) #:use-module (crates-io))

(define-public crate-pircolate-0.1.0 (c (n "pircolate") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)))) (h "06ngkhykkv7yflqbs5g2fs4vx3m4hcrs90q45yzbjz9iajad1kbx")))

(define-public crate-pircolate-0.2.0 (c (n "pircolate") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "13v5h1nriddv4r8707rq3hwazc5x4x3825f43hmf0n2dzn2783i0")))

(define-public crate-pircolate-0.2.1 (c (n "pircolate") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "1jlxxgh132i6abf5py84ibdp5m9yinldpb0xrvxla0la2bs0z3vf")))

