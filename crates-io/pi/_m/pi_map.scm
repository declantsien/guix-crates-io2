(define-module (crates-io pi _m pi_map) #:use-module (crates-io))

(define-public crate-pi_map-0.1.0 (c (n "pi_map") (v "0.1.0") (d (list (d (n "pi_hash") (r "^0.1") (d #t) (k 0)))) (h "1wksyhbqpqjpgxj46nk2jgx20mpm7v14yjahiv88i06bdfq2ibmm")))

(define-public crate-pi_map-0.1.1 (c (n "pi_map") (v "0.1.1") (d (list (d (n "pi_hash") (r "^0.1") (d #t) (k 0)))) (h "1q5wxxyc3dw3ml4xw8qfvlm48pkj72vwap3i51zlbrcgm80pz48m")))

(define-public crate-pi_map-0.1.2 (c (n "pi_map") (v "0.1.2") (d (list (d (n "pi_hash") (r "^0.1") (d #t) (k 0)))) (h "12a6kpji6kq2pv4a6s5wph6drmimxcqxjhjmyg206bdf2l2n6a7k")))

(define-public crate-pi_map-0.1.3 (c (n "pi_map") (v "0.1.3") (d (list (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "1l4kh13i2ksj4lckh822d37kqicmmf98ixqy91kgcd0rhphnad0r")))

(define-public crate-pi_map-0.1.4 (c (n "pi_map") (v "0.1.4") (d (list (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "0yls0w0rksdqc2b2v9mxxiikgqvyxcj29lvfj1zxkjrwwis9vvmk")))

(define-public crate-pi_map-0.1.5 (c (n "pi_map") (v "0.1.5") (d (list (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "1f265s0wz792mnvz82j74bs9izzrzzpd1cv7fzsd8xxj5qv46s21")))

(define-public crate-pi_map-0.2.0 (c (n "pi_map") (v "0.2.0") (d (list (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_null") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "0rb76ij6vjfr08vgaj8gzfc7cknf2w7d8s37jrssgjhq28vwwr9y") (y #t)))

(define-public crate-pi_map-0.2.1 (c (n "pi_map") (v "0.2.1") (d (list (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "059z1vzhcd8b1qad5kqh4r8kqcqc2z5n4c79mic06p3pmh3nzc78")))

