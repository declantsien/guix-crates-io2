(define-module (crates-io pi _d pi_dirty) #:use-module (crates-io))

(define-public crate-pi_dirty-0.1.0 (c (n "pi_dirty") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_map") (r "^0.1") (d #t) (k 0)))) (h "0fwb3v7av9gjyiky9n6500p4581vi6166p15dhzb3wzcbbzpnwgb")))

(define-public crate-pi_dirty-0.1.1 (c (n "pi_dirty") (v "0.1.1") (d (list (d (n "derive_deref") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_map") (r "^0.1") (d #t) (k 0)))) (h "1xjappjy3pxh5zvrzzp97z2a8ic01qmh9c5rhg5rafwc5vjqbsms")))

(define-public crate-pi_dirty-0.2.0 (c (n "pi_dirty") (v "0.2.0") (d (list (d (n "derive_deref") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_map") (r "^0.2") (d #t) (k 0)))) (h "08ajxpcr3kn7r9z4nf96qarzk8ia9wcsl0mgm5qn6s4vxy2bhlcs") (y #t)))

(define-public crate-pi_dirty-0.1.2 (c (n "pi_dirty") (v "0.1.2") (d (list (d (n "derive_deref") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_map") (r "^0.2") (d #t) (k 0)))) (h "14cy6jmaz00mi0ajwas1j63ffcg66kd0yz6j119sl1npvfcwmgjc")))

