(define-module (crates-io pi _d pi_debug_info) #:use-module (crates-io))

(define-public crate-pi_debug_info-0.1.0 (c (n "pi_debug_info") (v "0.1.0") (d (list (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (o #t) (d #t) (k 0)))) (h "0pp3bnwpr5mayc123ixfj13wwjnibz1dxdf0jxsxycgwbbh4rnb8") (f (quote (("wasm-bindgen" "web-sys") ("print") ("default"))))))

