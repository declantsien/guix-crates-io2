(define-module (crates-io pi _d pi_data_view) #:use-module (crates-io))

(define-public crate-pi_data_view-0.1.0 (c (n "pi_data_view") (v "0.1.0") (h "1shv0i6blspihxciwjs8fmah39kr6hlqfn31jc7j83dybs8cgvbz")))

(define-public crate-pi_data_view-0.1.1 (c (n "pi_data_view") (v "0.1.1") (h "03p85xqwyyda9rghhc0q2j23x1sbv6cvf4f5m5v0b4b1n9rll6hn")))

