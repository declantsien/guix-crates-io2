(define-module (crates-io pi _d pi_densevec) #:use-module (crates-io))

(define-public crate-pi_densevec-0.1.0 (c (n "pi_densevec") (v "0.1.0") (d (list (d (n "pi_map") (r "^0.1") (d #t) (k 0)))) (h "0x087ny5nzfca703j7qswm6bdrnfapwqim9c2kpbqr2jkrvq38n6")))

(define-public crate-pi_densevec-0.1.1 (c (n "pi_densevec") (v "0.1.1") (d (list (d (n "pi_map") (r "^0.1") (d #t) (k 0)))) (h "1h6a43smg4sx3id1n97s652fjagwa8xs2p31w3g8kcxnp2d1rv5p")))

(define-public crate-pi_densevec-0.1.2 (c (n "pi_densevec") (v "0.1.2") (d (list (d (n "pi_map") (r "^0.1") (d #t) (k 0)))) (h "13qcyw03l36y903pvlwwl2g1pql1v1zi1in13fik7jyrdi73fpgn")))

(define-public crate-pi_densevec-0.2.0 (c (n "pi_densevec") (v "0.2.0") (d (list (d (n "pi_map") (r "^0.2") (d #t) (k 0)))) (h "18hvrvksk4z7zzbh1h3qj9fsw6idqy1xv429h3904mal52kf09dw") (y #t)))

(define-public crate-pi_densevec-0.1.3 (c (n "pi_densevec") (v "0.1.3") (d (list (d (n "pi_map") (r "^0.2") (d #t) (k 0)))) (h "0j5y8f99jbd1wss7lvyc1r0m7rqpwdsac42pvbxvwly6gj3grskd")))

