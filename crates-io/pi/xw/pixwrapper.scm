(define-module (crates-io pi xw pixwrapper) #:use-module (crates-io))

(define-public crate-pixwrapper-0.1.0 (c (n "pixwrapper") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.54") (d #t) (k 1)))) (h "1b9pdf9ildw1ajb232wwwkzvkxknnjb624gdjygr3jh2bacsdy46")))

(define-public crate-pixwrapper-0.1.1 (c (n "pixwrapper") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.54") (d #t) (k 1)))) (h "0dpm2ygcmcyv415f730aprvkl750kzybr9ds80fp85ya265ayk4x")))

(define-public crate-pixwrapper-0.1.2 (c (n "pixwrapper") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.54") (d #t) (k 1)))) (h "0x5amkld3r66djkvlyprci8wbdq8xg62rdllx21y4n8mg3m6hxgg")))

(define-public crate-pixwrapper-0.1.3 (c (n "pixwrapper") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0.54") (d #t) (k 1)))) (h "04if56d8h6v49ny5gdzmn2z090ggywhngg4nxyhlzn5pf5rm2b3n")))

