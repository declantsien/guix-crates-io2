(define-module (crates-io pi ta pitaya-go-bsp) #:use-module (crates-io))

(define-public crate-pitaya-go-bsp-0.1.0 (c (n "pitaya-go-bsp") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6, <0.8") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 2)) (d (n "nrf52840-hal") (r "^0.13.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "panic-rtt-target") (r "^0.1.2") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "rtt-target") (r "^0.3.1") (f (quote ("cortex-m"))) (d #t) (k 2)))) (h "0rj3mbwvyjdrzspzb0c1xbjzpxpav5y27v3dc43lb0wb8z4xvpxv") (f (quote (("rt" "nrf52840-hal/rt") ("default" "rt"))))))

