(define-module (crates-io pi ta pitaya-go) #:use-module (crates-io))

(define-public crate-pitaya-go-0.0.1 (c (n "pitaya-go") (v "0.0.1") (d (list (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "nrf52840-hal") (r "^0.11") (d #t) (k 0)) (d (n "nrf52840-pac") (r "^0.9.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2") (d #t) (k 2)))) (h "1kzm0xxr4p83scha4flnrbdz0p1r4xigalyfgw3n5fflm29jwwqk") (f (quote (("rt" "nrf52840-hal/rt") ("default" "rt"))))))

(define-public crate-pitaya-go-0.0.2 (c (n "pitaya-go") (v "0.0.2") (d (list (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "nrf52840-hal") (r "^0.11") (d #t) (k 0)) (d (n "nrf52840-pac") (r "^0.9.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2") (d #t) (k 2)))) (h "1gna4hzjl15qbrs8r2pl84i5ssx0dsrw7mvj7rh17303d1jfnfzv") (f (quote (("rt" "nrf52840-hal/rt") ("default" "rt"))))))

