(define-module (crates-io pi ko piko) #:use-module (crates-io))

(define-public crate-piko-0.1.0 (c (n "piko") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "gfx-hal") (r "^0.5") (d #t) (k 0)))) (h "12l25h0qh5z51kvjn0xlzqzazjgvsy7m7z4nqh7vlvw5mrg2g9ny")))

