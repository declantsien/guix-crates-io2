(define-module (crates-io pi kc pikchr-sys) #:use-module (crates-io))

(define-public crate-pikchr-sys-0.1.0 (c (n "pikchr-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1g5vba74qqbbikq0klqjhf644fmdydhll5f8a0nkfd1d14lcchrv")))

(define-public crate-pikchr-sys-0.1.1 (c (n "pikchr-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0gjmkllad1c1d00arcvz3w27nyp2q9nwc3pms6lpk21y8c26rsp7")))

