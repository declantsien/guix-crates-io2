(define-module (crates-io pi kc pikchr-cli) #:use-module (crates-io))

(define-public crate-pikchr-cli-0.1.2 (c (n "pikchr-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pikchr") (r "^0.1.2") (d #t) (k 0)))) (h "0lh2qcbwpihaigf9h9psgn3qsmh6czqmnkllcg4qgm81cmic785k")))

