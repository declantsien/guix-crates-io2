(define-module (crates-io pi kc pikchr) #:use-module (crates-io))

(define-public crate-pikchr-0.1.0 (c (n "pikchr") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jjy15lsqsmzpahf39a36f5dd1p844xrpwsd5hn94mh4sbazy2sh")))

(define-public crate-pikchr-0.1.1 (c (n "pikchr") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lm6924k84jdwsyjf029r2xwz23dmm19mryb51jaj9q29f9n001w")))

(define-public crate-pikchr-0.1.2 (c (n "pikchr") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jybhjps3b7rksv3brzrcwflmbq0j9yp6yb8skckw51fa6rd09jf")))

(define-public crate-pikchr-0.1.3 (c (n "pikchr") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vn91nmxpp3b9pbp19mds6n4clq0xzii10i4rli4xb6zl1qb8c5l")))

