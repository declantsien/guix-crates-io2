(define-module (crates-io pi er pierce) #:use-module (crates-io))

(define-public crate-pierce-0.1.0 (c (n "pierce") (v "0.1.0") (h "1xgbl81zyl75vjljwiwnr0q1dlhal0gyx9gwbd1qnhwsvk17jb09")))

(define-public crate-pierce-0.2.0 (c (n "pierce") (v "0.2.0") (h "1jz2717h7qy8bkq065kzmi0k0s2svi5s8x0q04rmnd07h8xy7fip")))

(define-public crate-pierce-0.3.0 (c (n "pierce") (v "0.3.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "1xqblnav48byhhqasa1n16r6jlhmn3axggrk3bjh9r0n4pwmjd7p")))

