(define-module (crates-io pi er pieralyzer) #:use-module (crates-io))

(define-public crate-pieralyzer-0.1.0 (c (n "pieralyzer") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jig3srj8ljcq2qiqy1a2cq9wm662bw1hzxc2d3pm55d3xr6h5kd")))

(define-public crate-pieralyzer-0.1.1 (c (n "pieralyzer") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08rm2l0kwj886kip6a34hlzjsbw30n8n72f1qa9k7xh31dk1jgza")))

(define-public crate-pieralyzer-0.1.2 (c (n "pieralyzer") (v "0.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04fkcsgy35mw1aqblqiskvv91dg3a34akhgsg9qmf6d89xcy39mk")))

