(define-module (crates-io pi st piston-shaders) #:use-module (crates-io))

(define-public crate-piston-shaders-0.0.0 (c (n "piston-shaders") (v "0.0.0") (d (list (d (n "piston-shaders_graphics2d") (r "^0.0.0") (d #t) (k 0)))) (h "1zpfg0qg6jikqq4i6dk6wc3b2hjs3db0jv7vp379czzhk1ga522q")))

(define-public crate-piston-shaders-0.1.0 (c (n "piston-shaders") (v "0.1.0") (d (list (d (n "piston-shaders_graphics2d") (r "^0.1.0") (d #t) (k 0)))) (h "058w1b8izr3mn3j91lfv17l7xa1hnv9560rrm0p6xqav9pw8z93s")))

(define-public crate-piston-shaders-0.2.0 (c (n "piston-shaders") (v "0.2.0") (d (list (d (n "piston-shaders_graphics2d") (r "^0.2.0") (d #t) (k 0)))) (h "0rr1nlspg979q4wx2a5la3kfk6sm6frp9v25lbk51pah85z73k2v")))

(define-public crate-piston-shaders-0.3.0 (c (n "piston-shaders") (v "0.3.0") (d (list (d (n "piston-shaders_graphics2d") (r "^0.3.0") (d #t) (k 0)))) (h "1jnif3bnyv2jh3limzr9hy0f8njdwy4wkqn5cz4x2mfr9r2pcyci")))

(define-public crate-piston-shaders-0.3.1 (c (n "piston-shaders") (v "0.3.1") (d (list (d (n "piston-shaders_graphics2d") (r "^0.3.1") (d #t) (k 0)))) (h "11nsvnny4z550x2hzjkkk1kb5s8gj2v1d8339mj1vm0bfg24fmcg")))

(define-public crate-piston-shaders-0.4.0 (c (n "piston-shaders") (v "0.4.0") (d (list (d (n "piston-shaders_graphics2d") (r "^0.4.0") (d #t) (k 0)))) (h "1ifs3mc5k20ijvv5s8jqxdvfzjk9fyhzf6isyid8pa4g7kzn70w5")))

