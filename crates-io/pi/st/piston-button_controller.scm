(define-module (crates-io pi st piston-button_controller) #:use-module (crates-io))

(define-public crate-piston-button_controller-0.1.0 (c (n "piston-button_controller") (v "0.1.0") (d (list (d (n "piston_window") (r "^0.63.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0zhla6ph997441insra29d8qllx19cx5iwgx547sfwi2b2mdm6la")))

(define-public crate-piston-button_controller-0.1.1 (c (n "piston-button_controller") (v "0.1.1") (d (list (d (n "piston_window") (r "^0.63.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "06nhss9cn2c3z35004zr4z2yyhgixli2fxswyi7qfy5fi2l3zli1")))

(define-public crate-piston-button_controller-0.1.2 (c (n "piston-button_controller") (v "0.1.2") (d (list (d (n "piston_window") (r "^0.63.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.41.0") (d #t) (k 2)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0xk52lxgrq7a698mcdihibib2fklzin4k0hql149dhqils5nrky3")))

(define-public crate-piston-button_controller-0.2.0 (c (n "piston-button_controller") (v "0.2.0") (d (list (d (n "piston_window") (r "^0.65.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.18.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.42.0") (d #t) (k 2)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "1fx7cw47izzg8wwy4ayr60vk1mx358fs0j5xc7p11bf9ax569p3a")))

(define-public crate-piston-button_controller-0.3.0 (c (n "piston-button_controller") (v "0.3.0") (d (list (d (n "piston_window") (r "^0.70.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.19.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.44.0") (d #t) (k 2)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "1nrywxzj5wyiq6scr451y0n0sfsh3kf43r2yx556l3d3cl4v1w6l")))

(define-public crate-piston-button_controller-0.4.0 (c (n "piston-button_controller") (v "0.4.0") (d (list (d (n "piston_window") (r "^0.73.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.20.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.46.0") (d #t) (k 2)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "1f411qw11mhv50w5qxzkpwwf1pn8m0yvbcbbc82sx27g8yllfg5h")))

(define-public crate-piston-button_controller-0.5.0 (c (n "piston-button_controller") (v "0.5.0") (d (list (d (n "piston_window") (r "^0.80.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.21.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.50.0") (d #t) (k 2)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "02azxcn0q1l3fb55yxxr03gpsfp10hl9hf69r4hxbwpyv2kgh728")))

(define-public crate-piston-button_controller-0.6.0 (c (n "piston-button_controller") (v "0.6.0") (d (list (d (n "piston_window") (r "^0.83.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.22.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.51.0") (d #t) (k 2)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0xvcdpvzhy4xycw6178gpnsljb1wf7x40hvdcnfrgzlrqji8kad4")))

(define-public crate-piston-button_controller-0.7.0 (c (n "piston-button_controller") (v "0.7.0") (d (list (d (n "piston_window") (r "^0.85.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.23.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.52.0") (d #t) (k 2)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0hp556fa74xi10x6xr1f02hsws44zab9qypzhfsmmxfv25yaxra3")))

(define-public crate-piston-button_controller-0.8.0 (c (n "piston-button_controller") (v "0.8.0") (d (list (d (n "piston_window") (r "^0.88.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.24.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.54.0") (d #t) (k 2)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "193w4x17i0qmp948cn94sag067rzaq04bqcf8602mr7z8cymv4ph")))

(define-public crate-piston-button_controller-0.9.0 (c (n "piston-button_controller") (v "0.9.0") (d (list (d (n "piston_window") (r "^0.93.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.25.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.56.0") (d #t) (k 2)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "0pi81aqyhkfw59dkrgj63f3nsw0slmiyzvz14zsnz3q27iyp32n1")))

(define-public crate-piston-button_controller-0.10.0 (c (n "piston-button_controller") (v "0.10.0") (d (list (d (n "piston_window") (r "^0.96.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.26.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.60.0") (d #t) (k 2)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "1dflwxqaplgm9302n84h7dnw27b8pjz7n37lwl7chs8fl4mbnn5k")))

(define-public crate-piston-button_controller-0.11.0 (c (n "piston-button_controller") (v "0.11.0") (d (list (d (n "piston") (r "^0.47.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.32.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.65.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.27.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.61.0") (d #t) (k 2)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "0p3n5sd20wx2jc1ii95w9rhk9xw3g72gp3j90z8nqr42rk65krkv")))

(define-public crate-piston-button_controller-0.12.0 (c (n "piston-button_controller") (v "0.12.0") (d (list (d (n "piston") (r "^0.48.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.32.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.65.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.62.0") (d #t) (k 2)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "1hc7ybny0pygqzydipnbzw1rdmccq5ql7n17p7yg72b29b8dm1w4")))

(define-public crate-piston-button_controller-0.13.0 (c (n "piston-button_controller") (v "0.13.0") (d (list (d (n "piston") (r "^0.51.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.36.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.72.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.65.0") (d #t) (k 2)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "0bszlh1zlqij3hzf09gm7bpkim29kbg6rri705azy69g2fnhali0")))

