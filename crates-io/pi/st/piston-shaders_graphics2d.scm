(define-module (crates-io pi st piston-shaders_graphics2d) #:use-module (crates-io))

(define-public crate-piston-shaders_graphics2d-0.0.0 (c (n "piston-shaders_graphics2d") (v "0.0.0") (h "00af6a52gr341yv8ak729a0gyclj5sn625ng48rii3xxj4jzzcnh")))

(define-public crate-piston-shaders_graphics2d-0.1.0 (c (n "piston-shaders_graphics2d") (v "0.1.0") (h "1pkx272l2mv7iikmxhbv6rh11w2ip4ips496999rzajk42ai8dqq")))

(define-public crate-piston-shaders_graphics2d-0.2.0 (c (n "piston-shaders_graphics2d") (v "0.2.0") (h "0czcbvnk8aayclqc5kkbls5v0wgv7nc0l7d4ihfqc697hfds9501")))

(define-public crate-piston-shaders_graphics2d-0.2.1 (c (n "piston-shaders_graphics2d") (v "0.2.1") (h "03mh2imqg8q5nyykczvx1qkp7i9q4vnmdvfm0f68v4jgc01jzrv3")))

(define-public crate-piston-shaders_graphics2d-0.3.0 (c (n "piston-shaders_graphics2d") (v "0.3.0") (h "0i05946pmsrqdvpsv0g6fhvj84ays4xrmwcvm4m29m1pdg2pfcxw")))

(define-public crate-piston-shaders_graphics2d-0.3.1 (c (n "piston-shaders_graphics2d") (v "0.3.1") (h "1dhh9bv4q19gdnj9d1nqq0yrvzs6gcn0c5j1p1f3xzyzq7d1gg4p")))

(define-public crate-piston-shaders_graphics2d-0.4.0 (c (n "piston-shaders_graphics2d") (v "0.4.0") (h "16j8gb3j530jln8y1wn3bbfjyzabdls67axs0bzxfvrbip8g8d9s")))

