(define-module (crates-io pi st piston-gfx_texture) #:use-module (crates-io))

(define-public crate-piston-gfx_texture-0.0.6 (c (n "piston-gfx_texture") (v "0.0.6") (d (list (d (n "gfx") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.3.9") (d #t) (k 0)) (d (n "piston-texture") (r "^0.0.1") (d #t) (k 0)))) (h "0q5sx34pd41jkig8hlbsr38w8rgpamz9w1hyvkbfm6lqp1mfrxsg")))

(define-public crate-piston-gfx_texture-0.0.7 (c (n "piston-gfx_texture") (v "0.0.7") (d (list (d (n "gfx") (r "^0.6.0") (d #t) (k 0)) (d (n "image") (r "^0.3.10") (d #t) (k 0)) (d (n "piston-texture") (r "^0.0.1") (d #t) (k 0)))) (h "0d48wbbp84c04xxd1zy1qgw8w7csfz20pvmrvbpyl8ak4b5kf9hw")))

(define-public crate-piston-gfx_texture-0.1.0 (c (n "piston-gfx_texture") (v "0.1.0") (d (list (d (n "gfx") (r "^0.6.1") (d #t) (k 0)) (d (n "image") (r "^0.3.10") (d #t) (k 0)) (d (n "piston-texture") (r "^0.1.0") (d #t) (k 0)))) (h "12yhnmlqa1vaqx7l7vy1bnasidfj47kll5836zczhbm1xjz49jz9")))

(define-public crate-piston-gfx_texture-0.2.0 (c (n "piston-gfx_texture") (v "0.2.0") (d (list (d (n "gfx") (r "^0.6.1") (d #t) (k 0)) (d (n "image") (r "^0.3.11") (d #t) (k 0)) (d (n "piston-texture") (r "^0.2.0") (d #t) (k 0)))) (h "0qqgk01dsxp5czbhb15z25dmjbxzbq0s0fgga26npfwigbfa2gl7")))

(define-public crate-piston-gfx_texture-0.3.0 (c (n "piston-gfx_texture") (v "0.3.0") (d (list (d (n "gfx") (r "^0.7.0") (d #t) (k 0)) (d (n "image") (r "^0.3.11") (d #t) (k 0)) (d (n "piston-texture") (r "^0.2.0") (d #t) (k 0)))) (h "09divk0m3bm7rna24j4zk9s9wg2yapj0h423640w5q0hfq2j2wii")))

(define-public crate-piston-gfx_texture-0.4.0 (c (n "piston-gfx_texture") (v "0.4.0") (d (list (d (n "gfx") (r "^0.7.0") (d #t) (k 0)) (d (n "image") (r "^0.4.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.2.0") (d #t) (k 0)))) (h "1ygvhjvkhcvnk9vn7nvq2ys9y5nx8dzrnglzc5mlplr1mlgng9iy")))

(define-public crate-piston-gfx_texture-0.5.0 (c (n "piston-gfx_texture") (v "0.5.0") (d (list (d (n "gfx") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.4.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.2.0") (d #t) (k 0)))) (h "1gx56w97vf0s95ipj12y9lx09hq323z2yi9pnmgzysr2341dy1aj")))

(define-public crate-piston-gfx_texture-0.6.0 (c (n "piston-gfx_texture") (v "0.6.0") (d (list (d (n "gfx") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.5.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.2.0") (d #t) (k 0)))) (h "1ac3shbp89ymxsdhp6k1zag5x7al6a1mlm2x2gyahmyvy78xjllc")))

(define-public crate-piston-gfx_texture-0.6.1 (c (n "piston-gfx_texture") (v "0.6.1") (d (list (d (n "gfx") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.5.4") (d #t) (k 0)) (d (n "piston-texture") (r "^0.2.0") (d #t) (k 0)))) (h "0nalawcpcyx1m897sh2c6231w9hdgcra3221ll4wr0szcrq9g4w8")))

(define-public crate-piston-gfx_texture-0.7.0 (c (n "piston-gfx_texture") (v "0.7.0") (d (list (d (n "gfx") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.6.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.3.0") (d #t) (k 0)))) (h "003sqq391q1ha9z542nx17r0lx13i2aaby027521v2kpy25rwysl")))

(define-public crate-piston-gfx_texture-0.8.0 (c (n "piston-gfx_texture") (v "0.8.0") (d (list (d (n "gfx") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.6.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.3.0") (d #t) (k 0)))) (h "0l8x2vwxbiiv4yr00n4183dwzyss7qnhjcxqcyq7016a6a9sw5nf")))

(define-public crate-piston-gfx_texture-0.9.0 (c (n "piston-gfx_texture") (v "0.9.0") (d (list (d (n "gfx") (r "^0.9.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.6.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.3.0") (d #t) (k 0)))) (h "1wwqscdl98g9wy7y68lcvsvpqip2b28j4rc4spfvlqwa4sxfljw6")))

(define-public crate-piston-gfx_texture-0.10.0 (c (n "piston-gfx_texture") (v "0.10.0") (d (list (d (n "gfx") (r "^0.9.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.7.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.3.0") (d #t) (k 0)))) (h "1gskxrc4hijn79mvzpd00lgrzx4pf9n0j26arpsgimgkh2x3vnzd")))

(define-public crate-piston-gfx_texture-0.11.0 (c (n "piston-gfx_texture") (v "0.11.0") (d (list (d (n "gfx") (r "^0.9.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.7.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.3.0") (d #t) (k 0)))) (h "06gv2i7fhzgj3y2xn28byr8fvm6w6b2gzsmcdysfmsvm2jc19wd7")))

(define-public crate-piston-gfx_texture-0.12.0 (c (n "piston-gfx_texture") (v "0.12.0") (d (list (d (n "gfx") (r "^0.10.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.7.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.4.0") (d #t) (k 0)))) (h "1qj8dxm5l17fx3b205p39jg2v4hnpb0rjabdyzllsxx1y6c55j1m")))

(define-public crate-piston-gfx_texture-0.13.0 (c (n "piston-gfx_texture") (v "0.13.0") (d (list (d (n "gfx") (r "^0.10.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.8.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.4.0") (d #t) (k 0)))) (h "0s2km3alhxdgksrjz11zp0ry8m0n2g1an7zk0cjc6kzg1y60r3vg")))

(define-public crate-piston-gfx_texture-0.14.0 (c (n "piston-gfx_texture") (v "0.14.0") (d (list (d (n "gfx") (r "^0.10.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.9.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.4.0") (d #t) (k 0)))) (h "1x42ryld2saxv4jr6m3brfn4njwl6lfc8srn3hh2xpz2xln3dm42")))

(define-public crate-piston-gfx_texture-0.15.0 (c (n "piston-gfx_texture") (v "0.15.0") (d (list (d (n "gfx") (r "^0.11.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.10.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.4.0") (d #t) (k 0)))) (h "0f27xpmz217n9qns9jsnlsx49qn5zc6lqssimw51kiq7bhcg5a8n")))

(define-public crate-piston-gfx_texture-0.16.0 (c (n "piston-gfx_texture") (v "0.16.0") (d (list (d (n "gfx") (r "^0.12.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.4.0") (d #t) (k 0)) (d (n "image") (r "^0.10.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.4.0") (d #t) (k 0)))) (h "0bhajxrl5p0fya0bwpp2m5q6sinya90kyvf2v0w7vh3l20w7kkwr")))

(define-public crate-piston-gfx_texture-0.17.0 (c (n "piston-gfx_texture") (v "0.17.0") (d (list (d (n "gfx") (r "^0.12.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.4.0") (d #t) (k 0)) (d (n "image") (r "^0.10.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.5.0") (d #t) (k 0)))) (h "07vb5kmhalf3r81id20czghy4nyhiaw7j2l1qjnpa83aga2ipdcx")))

(define-public crate-piston-gfx_texture-0.18.0 (c (n "piston-gfx_texture") (v "0.18.0") (d (list (d (n "gfx") (r "^0.12.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.4.0") (d #t) (k 0)) (d (n "image") (r "^0.10.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.5.0") (d #t) (k 0)))) (h "0y41igqlcwpakmv6dw541ilivjffb3a9i4abvwvl3w24f7mj507p")))

(define-public crate-piston-gfx_texture-0.19.0 (c (n "piston-gfx_texture") (v "0.19.0") (d (list (d (n "gfx") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.10.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.5.0") (d #t) (k 0)))) (h "0m8r4gpvnkmb72gi082hgyixabsh9lzaznar8vz7v8qlnflwi9jm")))

(define-public crate-piston-gfx_texture-0.20.0 (c (n "piston-gfx_texture") (v "0.20.0") (d (list (d (n "gfx") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.12.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.5.0") (d #t) (k 0)))) (h "16jlrg7hqzlhch3w3m2i0immab0srinfac1zipqyifzfly5ira6l")))

(define-public crate-piston-gfx_texture-0.21.0 (c (n "piston-gfx_texture") (v "0.21.0") (d (list (d (n "gfx") (r "^0.14.0") (d #t) (k 0)) (d (n "image") (r "^0.12.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.5.0") (d #t) (k 0)))) (h "01la0a8pbd3ggkn3d9adnpy6givgmypbnqlk3xyqmky19p2371vz")))

(define-public crate-piston-gfx_texture-0.21.1 (c (n "piston-gfx_texture") (v "0.21.1") (d (list (d (n "gfx") (r "^0.14.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.6.0") (d #t) (k 0)) (d (n "image") (r "^0.12.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.5.0") (d #t) (k 0)))) (h "1244qi3pqpz0n2nr8wbp7bsz3nvnfzh992h6sgk69lbr30ifqif2")))

(define-public crate-piston-gfx_texture-0.22.0 (c (n "piston-gfx_texture") (v "0.22.0") (d (list (d (n "gfx") (r "^0.14.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.6.0") (d #t) (k 0)) (d (n "image") (r "^0.12.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.5.0") (d #t) (k 0)))) (h "00hrzmgwlsvr7lw3jn7niq70qywpaibpgky7l67iyr6njbpigp33")))

(define-public crate-piston-gfx_texture-0.22.1 (c (n "piston-gfx_texture") (v "0.22.1") (d (list (d (n "gfx") (r "^0.14.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.6.0") (d #t) (k 0)) (d (n "image") (r "^0.12.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.5.0") (d #t) (k 0)))) (h "0bp6jqphpdnhks42s87bks54qxxspil6rrcxq6q6yy45nzrcfhrv")))

(define-public crate-piston-gfx_texture-0.23.0 (c (n "piston-gfx_texture") (v "0.23.0") (d (list (d (n "gfx") (r "^0.15.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7.0") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.5.0") (d #t) (k 0)))) (h "12air400w5nkl7kx75k5w8r1w44c9chd19nhwrvcx8mxjbgp4vkd")))

(define-public crate-piston-gfx_texture-0.24.0 (c (n "piston-gfx_texture") (v "0.24.0") (d (list (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7.0") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.5.0") (d #t) (k 0)))) (h "1mgss5f5pb03mv7gpn76grr0zf0qpb7ks6lw8gdvs9q43jkddpw7")))

(define-public crate-piston-gfx_texture-0.25.0 (c (n "piston-gfx_texture") (v "0.25.0") (d (list (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7.0") (d #t) (k 0)) (d (n "image") (r "^0.14.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.5.0") (d #t) (k 0)))) (h "1p3jz7qfjcg3ak1fk6vw25k9axrk97vckhqaszi9jzfn5cm3ljdf")))

(define-public crate-piston-gfx_texture-0.26.0 (c (n "piston-gfx_texture") (v "0.26.0") (d (list (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7.0") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.5.0") (d #t) (k 0)))) (h "1zqkpqq584iqfm6wfggl6i8b8wl0lxrdqq94wr3xjk61inmfl1nv")))

(define-public crate-piston-gfx_texture-0.27.0 (c (n "piston-gfx_texture") (v "0.27.0") (d (list (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7.0") (d #t) (k 0)) (d (n "image") (r "^0.16.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.5.0") (d #t) (k 0)))) (h "1w55qyd6vsnvqrpdslx9xcdq45a89rim9rgra415n3ijv6842akm")))

(define-public crate-piston-gfx_texture-0.28.0 (c (n "piston-gfx_texture") (v "0.28.0") (d (list (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7.0") (d #t) (k 0)) (d (n "image") (r "^0.16.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.6.0") (d #t) (k 0)))) (h "1bgm0ws45c8fxsf16zfxcr54c9njcpkigzjdgzrysl97bbpb5cdh")))

(define-public crate-piston-gfx_texture-0.29.0 (c (n "piston-gfx_texture") (v "0.29.0") (d (list (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7.0") (d #t) (k 0)) (d (n "image") (r "^0.17.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.6.0") (d #t) (k 0)))) (h "1hgi750855xm8rbbjavdmbrq3y7y4rf1jkvz32hwjrgw619ca80b")))

(define-public crate-piston-gfx_texture-0.30.0 (c (n "piston-gfx_texture") (v "0.30.0") (d (list (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7.0") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.6.0") (d #t) (k 0)))) (h "0f6yhir4m8xpcb81bs71s9hwgafgjx8qxn1vrz48gawldvkpfcyg")))

(define-public crate-piston-gfx_texture-0.31.0 (c (n "piston-gfx_texture") (v "0.31.0") (d (list (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.6.0") (d #t) (k 0)))) (h "1sdggzjwfrn6cj5icyavdq8q0xn0z3fs3ihwxj9d3is1s4f5zbyx")))

(define-public crate-piston-gfx_texture-0.32.0 (c (n "piston-gfx_texture") (v "0.32.0") (d (list (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.6.0") (d #t) (k 0)))) (h "1a6m7aia85sj07w1bjpfcm7xb7f7zz88fqka1j7ykqxpcnlzd9c3")))

(define-public crate-piston-gfx_texture-0.33.0 (c (n "piston-gfx_texture") (v "0.33.0") (d (list (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.6.0") (d #t) (k 0)))) (h "1a2jdah4jaklrkxblz1yap6nlh97012bwp0fhsa4047jd2axvbxf")))

(define-public crate-piston-gfx_texture-0.34.0 (c (n "piston-gfx_texture") (v "0.34.0") (d (list (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.6.0") (d #t) (k 0)))) (h "1pqzdy9saqh5spqw7w080j2gicw6r62vrsaamsqk7p420q90hy7l")))

(define-public crate-piston-gfx_texture-0.35.0 (c (n "piston-gfx_texture") (v "0.35.0") (d (list (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.6.0") (d #t) (k 0)))) (h "0ri04df9v1nb3lr2isqckbwqf8fk6h4rv6640ambvlbzpkx7lc66")))

(define-public crate-piston-gfx_texture-0.36.0 (c (n "piston-gfx_texture") (v "0.36.0") (d (list (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.6.0") (d #t) (k 0)))) (h "0p92wx9faa1s6kp791fgfkqdjwlb72fx13hkfp3np9isd08kh1kr")))

(define-public crate-piston-gfx_texture-0.37.0 (c (n "piston-gfx_texture") (v "0.37.0") (d (list (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.22.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.6.0") (d #t) (k 0)))) (h "033myrp14hvz0dhjmgms1qa3bck3r71cn6c9qgw8krv2cnspxg4i")))

(define-public crate-piston-gfx_texture-0.38.0 (c (n "piston-gfx_texture") (v "0.38.0") (d (list (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.22.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.7.0") (d #t) (k 0)))) (h "0gg4zarsagg8z5vzzd5cpfh7hr6z8qchzrm6xsn5a703synvy6yl")))

(define-public crate-piston-gfx_texture-0.39.0 (c (n "piston-gfx_texture") (v "0.39.0") (d (list (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.22.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.8.0") (d #t) (k 0)))) (h "0zxd1aa4xg3b6rwjw94rc666aq9nbxp0cnr5yk0mqnba1kwy5slj")))

(define-public crate-piston-gfx_texture-0.40.0 (c (n "piston-gfx_texture") (v "0.40.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9.1") (d #t) (k 0)) (d (n "image") (r "^0.22.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.8.0") (d #t) (k 0)))) (h "1nr5awdgk3njfvfanszrv4gxz93f6skid1c8yijswccygripchqz")))

(define-public crate-piston-gfx_texture-0.41.0 (c (n "piston-gfx_texture") (v "0.41.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9.1") (d #t) (k 0)) (d (n "image") (r "^0.23.0") (d #t) (k 0)) (d (n "piston-texture") (r "^0.8.0") (d #t) (k 0)))) (h "0z699bgwyv5llkb604ajm50fqrxnlayaakrgghcp95qgcabi33fy")))

(define-public crate-piston-gfx_texture-0.42.0 (c (n "piston-gfx_texture") (v "0.42.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9.1") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "piston-texture") (r "^0.8.0") (d #t) (k 0)))) (h "1pzh1imcxmcarpwfyccvidq6r6cq81s64yv6d2qk95pl5cadz6qq")))

(define-public crate-piston-gfx_texture-0.43.0 (c (n "piston-gfx_texture") (v "0.43.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9.1") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "piston-texture") (r "^0.8.0") (d #t) (k 0)))) (h "0achqrl04jw5p7wh7g327wn1wdzbm8qn421jdkk6s5lyyd3yhkvv")))

(define-public crate-piston-gfx_texture-0.44.0 (c (n "piston-gfx_texture") (v "0.44.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9.1") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "piston-texture") (r "^0.9.0") (d #t) (k 0)))) (h "1b44dgz2kb73kd3fcgncymsm3yw4j3y1lw4p8r753y26c2mzl091")))

(define-public crate-piston-gfx_texture-0.45.0 (c (n "piston-gfx_texture") (v "0.45.0") (d (list (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9.1") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "piston-texture") (r "^0.9.0") (d #t) (k 0)))) (h "01kd17ciaamkjry7k09nqd9ljcgng26m10vnfika2d751w4flshn")))

