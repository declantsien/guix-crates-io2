(define-module (crates-io pi st piston-button_tracker) #:use-module (crates-io))

(define-public crate-piston-button_tracker-0.1.0 (c (n "piston-button_tracker") (v "0.1.0") (d (list (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)))) (h "0bhfy00z1c5l0sh89b9dqqj0p33d82dynsi1j6zs472n9745jpdy")))

(define-public crate-piston-button_tracker-0.2.0 (c (n "piston-button_tracker") (v "0.2.0") (d (list (d (n "pistoncore-input") (r "^0.18.0") (d #t) (k 0)))) (h "1g5qmxx5nsa6za30aqgnxa4cyzd9x5vpgzy5xc7kdbikxas69mbs")))

(define-public crate-piston-button_tracker-0.3.0 (c (n "piston-button_tracker") (v "0.3.0") (d (list (d (n "pistoncore-input") (r "^0.19.0") (d #t) (k 0)))) (h "0wckn45pigj6qikycycws8dmqgbwx0p3nllxqn89r8c4v26dycm4")))

(define-public crate-piston-button_tracker-0.4.0 (c (n "piston-button_tracker") (v "0.4.0") (d (list (d (n "pistoncore-input") (r "^0.20.0") (d #t) (k 0)))) (h "15w4c0gzarvhkkar0i6gjpn3qv8s1xb133kfq0hh3vq3dvab0n4y")))

(define-public crate-piston-button_tracker-0.5.0 (c (n "piston-button_tracker") (v "0.5.0") (d (list (d (n "pistoncore-input") (r "^0.21.0") (d #t) (k 0)))) (h "14chsfixjink10pgplykg30jgz95c7gwik6vsahnwc0hdcafbx1l")))

(define-public crate-piston-button_tracker-0.6.0 (c (n "piston-button_tracker") (v "0.6.0") (d (list (d (n "pistoncore-input") (r "^0.22.0") (d #t) (k 0)))) (h "0ifgfb9a1v9mcr34530h3xvkqnhz68j8fprdbfwf5gl7v60rb8h5")))

(define-public crate-piston-button_tracker-0.7.0 (c (n "piston-button_tracker") (v "0.7.0") (d (list (d (n "pistoncore-input") (r "^0.23.0") (d #t) (k 0)))) (h "1fixp3g2hcyvga42jnsv0mmcw75wpclhlxii1bkf6m89pi3jr2dk")))

(define-public crate-piston-button_tracker-0.8.0 (c (n "piston-button_tracker") (v "0.8.0") (d (list (d (n "pistoncore-input") (r "^0.24.0") (d #t) (k 0)))) (h "0gw1bchkja1ww50wpyrd44sj20zk2y1nvifrg2wbhx858d9srdlz")))

(define-public crate-piston-button_tracker-0.9.0 (c (n "piston-button_tracker") (v "0.9.0") (d (list (d (n "pistoncore-input") (r "^0.24.0") (d #t) (k 0)))) (h "13m6b8p6nm60hllapvmzzyqny0mmw5a20ci4sdbhsymzbsx96k2n")))

(define-public crate-piston-button_tracker-0.10.0 (c (n "piston-button_tracker") (v "0.10.0") (d (list (d (n "pistoncore-input") (r "^0.25.0") (d #t) (k 0)))) (h "018b7p0d88pqjvaiai3b22izmgcfbpc9kmkjza84pls8piyxpgb5")))

(define-public crate-piston-button_tracker-0.11.0 (c (n "piston-button_tracker") (v "0.11.0") (d (list (d (n "pistoncore-input") (r "^0.26.0") (d #t) (k 0)))) (h "0g5add2jf9q9da5ff3254slhdilizci55nl55c0amj95wcy0k2qp")))

(define-public crate-piston-button_tracker-0.12.0 (c (n "piston-button_tracker") (v "0.12.0") (d (list (d (n "pistoncore-input") (r "^0.27.0") (d #t) (k 0)))) (h "0wa57r55dj4yyd27zdzc72fg4xq5pp0d3bcjdnpm6miaw33nlb27")))

(define-public crate-piston-button_tracker-0.13.0 (c (n "piston-button_tracker") (v "0.13.0") (d (list (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)))) (h "18hg89cyzvcdh6sd62nfzb3j9hw0dizyf1m4da5adn5gbk6zpz6n")))

(define-public crate-piston-button_tracker-0.14.0 (c (n "piston-button_tracker") (v "0.14.0") (d (list (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)))) (h "0vyb77n9ic24ln440c11n16qy7pmaw3vsci5k4iwb3g3j8iny659")))

