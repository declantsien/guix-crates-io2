(define-module (crates-io pi st piston2d-glow_wrap) #:use-module (crates-io))

(define-public crate-piston2d-glow_wrap-0.1.0 (c (n "piston2d-glow_wrap") (v "0.1.0") (d (list (d (n "glow") (r "^0.12.0") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.7") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web_sys") (r "~0.3.60") (f (quote ("WebGlUniformLocation"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0) (p "web-sys")))) (h "1mxjv5lb8kws6zdjmhh4kvlp60f51nxlbn4f4prmvl2hgnycyhn9")))

