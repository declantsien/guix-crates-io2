(define-module (crates-io pi st piston_mix_economy) #:use-module (crates-io))

(define-public crate-piston_mix_economy-0.1.0 (c (n "piston_mix_economy") (v "0.1.0") (d (list (d (n "piston-timer_controller") (r "^0.6.0") (d #t) (k 2)) (d (n "piston_window") (r "^0.53.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "19ci999hczzjxrkdhli2mfx978yyx8ppprmihr5a809wnnbvlhgi")))

(define-public crate-piston_mix_economy-0.2.0 (c (n "piston_mix_economy") (v "0.2.0") (d (list (d (n "piston-timer_controller") (r "^0.6.0") (d #t) (k 2)) (d (n "piston_window") (r "^0.53.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "0v5pg8mcacww631rrrnr3gp9chffj936q6sabbh9z5y079my7jmy")))

(define-public crate-piston_mix_economy-0.2.1 (c (n "piston_mix_economy") (v "0.2.1") (d (list (d (n "piston-timer_controller") (r "^0.21.0") (d #t) (k 2)) (d (n "piston_window") (r "^0.120.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0h3j2i53j1df1y8axbpzxdiz3w3k8n8bds9bk37f64f7y0blm5iw")))

