(define-module (crates-io pi st piston3d-gfx_voxel) #:use-module (crates-io))

(define-public crate-piston3d-gfx_voxel-0.0.5 (c (n "piston3d-gfx_voxel") (v "0.0.5") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.5.2") (d #t) (k 0)) (d (n "image") (r "^0.3.9") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.0.6") (d #t) (k 0)))) (h "0azy2klli6g9jqgci464pm3478cmq7myac9d98w7d7x3c4fy72ba")))

(define-public crate-piston3d-gfx_voxel-0.1.0 (c (n "piston3d-gfx_voxel") (v "0.1.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.6") (d #t) (k 0)) (d (n "image") (r "^0.3") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.1") (d #t) (k 0)))) (h "0035j2k96xww8aqsd67nr0cphcvvjxc8ldzdy90li4nf1k5p24zy")))

(define-public crate-piston3d-gfx_voxel-0.1.1 (c (n "piston3d-gfx_voxel") (v "0.1.1") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.6") (d #t) (k 0)) (d (n "image") (r "^0.3") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.1") (d #t) (k 0)))) (h "18cr1jnncgdy8hy489s9f0z1wa0953npbd4xcbvzgsxbvmdxx4wm")))

(define-public crate-piston3d-gfx_voxel-0.2.0 (c (n "piston3d-gfx_voxel") (v "0.2.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.7.0") (d #t) (k 0)) (d (n "image") (r "^0.3") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.3.0") (d #t) (k 0)))) (h "117js0adwd4srqhqdf51ggxrd3smkjylxkqmg9a3f933lz31djs0")))

(define-public crate-piston3d-gfx_voxel-0.3.0 (c (n "piston3d-gfx_voxel") (v "0.3.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.7.0") (d #t) (k 0)) (d (n "image") (r "^0.4.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.4.0") (d #t) (k 0)))) (h "0yl73psp6zc1n2svf0zz51kk2n7vl5g8lawhkv4klxc6bg0pipnw")))

(define-public crate-piston3d-gfx_voxel-0.4.0 (c (n "piston3d-gfx_voxel") (v "0.4.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.4.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.5.0") (d #t) (k 0)))) (h "0n3gj92vk96l7382gcs02i6jzv1d9iyygp3c96adf3xzlvhdhlcq")))

(define-public crate-piston3d-gfx_voxel-0.5.0 (c (n "piston3d-gfx_voxel") (v "0.5.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.5.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.6.0") (d #t) (k 0)))) (h "1nd2zz4f1pmqb656a0hb1l29x3ms1a2gkyl96bgajb5q6s2gzsi6")))

(define-public crate-piston3d-gfx_voxel-0.6.0 (c (n "piston3d-gfx_voxel") (v "0.6.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.6.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.7.0") (d #t) (k 0)))) (h "13byx7c9hbv2wbr7zbb4ivz42g7hlg0y0bwvi95b6hhjfaqch459")))

(define-public crate-piston3d-gfx_voxel-0.7.0 (c (n "piston3d-gfx_voxel") (v "0.7.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.6.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.8.0") (d #t) (k 0)))) (h "0djcqw1q33bwa5bnm8vvylq553ps7w8cz70a00wp7xdmj8cg35k8")))

(define-public crate-piston3d-gfx_voxel-0.8.0 (c (n "piston3d-gfx_voxel") (v "0.8.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.9.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.14.0") (d #t) (k 0)))) (h "0lv7xi8q0kiifi7m9zr35hl0l94iasc52h0x84x4asrmzsz8nhnb")))

(define-public crate-piston3d-gfx_voxel-0.9.0 (c (n "piston3d-gfx_voxel") (v "0.9.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.11.0") (d #t) (k 0)) (d (n "image") (r "^0.10.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.15.0") (d #t) (k 0)))) (h "0zg1l9aqh1akx2dblsyddcwx0kvahljqn2ngrd6zy21inqbpp11w")))

(define-public crate-piston3d-gfx_voxel-0.10.0 (c (n "piston3d-gfx_voxel") (v "0.10.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.12.0") (d #t) (k 0)) (d (n "image") (r "^0.10.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.16.0") (d #t) (k 0)))) (h "068fklxjdf1ac511sw6d81ggq47f611qjd2jnffh9sd8fhvh75xw")))

(define-public crate-piston3d-gfx_voxel-0.11.0 (c (n "piston3d-gfx_voxel") (v "0.11.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.12.0") (d #t) (k 0)) (d (n "image") (r "^0.10.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.17.0") (d #t) (k 0)))) (h "0zqdl628bhyw4f4bxz79p1jpb59khwfzdsc8imgdac4j19gpnfy0")))

(define-public crate-piston3d-gfx_voxel-0.12.0 (c (n "piston3d-gfx_voxel") (v "0.12.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.12.0") (d #t) (k 0)) (d (n "image") (r "^0.10.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.18.0") (d #t) (k 0)))) (h "0ya3qaiq44jhz7cnjiqcvlz3knvdgyv44w96mgm85q4rfa38n5hi")))

(define-public crate-piston3d-gfx_voxel-0.13.0 (c (n "piston3d-gfx_voxel") (v "0.13.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.12.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.20.0") (d #t) (k 0)))) (h "1wk3cq29nd80wihzf6bchknflmpv83zvdz8yryjb69b4ljdha0k6")))

(define-public crate-piston3d-gfx_voxel-0.14.0 (c (n "piston3d-gfx_voxel") (v "0.14.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.14.0") (d #t) (k 0)) (d (n "image") (r "^0.12.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.21.0") (d #t) (k 0)))) (h "0zch20rxsfbarrl6ndgqrzkp28y7yrhpnksxf0pfgny1haw2avdf")))

(define-public crate-piston3d-gfx_voxel-0.15.0 (c (n "piston3d-gfx_voxel") (v "0.15.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.14.0") (d #t) (k 0)) (d (n "image") (r "^0.12.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.22.0") (d #t) (k 0)))) (h "04nx0y36hci9snvhs81zqylylkdd8p4xkzz1is3qajyf1mp8yqp0")))

(define-public crate-piston3d-gfx_voxel-0.16.0 (c (n "piston3d-gfx_voxel") (v "0.16.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.15.1") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.23.0") (d #t) (k 0)))) (h "0nr7gvjvkf72pcfjs74rda54yy2rb83risb3b80bvxa974qkb5p0")))

(define-public crate-piston3d-gfx_voxel-0.17.0 (c (n "piston3d-gfx_voxel") (v "0.17.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "image") (r "^0.13.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.24.0") (d #t) (k 0)))) (h "15v2av51phb3k2n8bqmx1wpb6sl0s8s6fav715bjqzlswba5mcss")))

(define-public crate-piston3d-gfx_voxel-0.18.0 (c (n "piston3d-gfx_voxel") (v "0.18.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "image") (r "^0.14.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.25.0") (d #t) (k 0)))) (h "0j4hwq0bpp1w05ir199l58mscg1dbw38jznzc3m3xyr4rz3lz4rg")))

(define-public crate-piston3d-gfx_voxel-0.19.0 (c (n "piston3d-gfx_voxel") (v "0.19.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.26.0") (d #t) (k 0)))) (h "0ly2av178s8pn0w026jy7f9l49l5m6jzwnadi3msz3ws71z1rhwq")))

(define-public crate-piston3d-gfx_voxel-0.20.0 (c (n "piston3d-gfx_voxel") (v "0.20.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "image") (r "^0.16.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.27.0") (d #t) (k 0)))) (h "117mdp3gy9bpgn51z5shvmh8cd0biichhi87qdsk1r5r1xhyqpmg")))

(define-public crate-piston3d-gfx_voxel-0.21.0 (c (n "piston3d-gfx_voxel") (v "0.21.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "image") (r "^0.16.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.28.0") (d #t) (k 0)))) (h "1b62cqw17kwz1b0mxfz56sl47xq8vfx23g79j2nlbf8qs8zb2n38")))

(define-public crate-piston3d-gfx_voxel-0.22.0 (c (n "piston3d-gfx_voxel") (v "0.22.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "image") (r "^0.17.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.29.0") (d #t) (k 0)))) (h "1cfdvw4zpvqfcfalfinkv60n32vwa0kabs65mi9868331f3fd7ck")))

(define-public crate-piston3d-gfx_voxel-0.23.0 (c (n "piston3d-gfx_voxel") (v "0.23.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.30.0") (d #t) (k 0)))) (h "1y3cqnncl3wigmmrm29s5qf1ssczq56caffm38k3llh9vi82wqp3")))

(define-public crate-piston3d-gfx_voxel-0.24.0 (c (n "piston3d-gfx_voxel") (v "0.24.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.31.0") (d #t) (k 0)))) (h "0k3whjvab3q7xhrggqwm9q6iqdbnbzannfijqbld70hxzack4c9q")))

(define-public crate-piston3d-gfx_voxel-0.25.0 (c (n "piston3d-gfx_voxel") (v "0.25.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.32.0") (d #t) (k 0)))) (h "0s5qiiz7blafp736fd53mlv7blrf4xa8j8386cs3hp7vyi97ca7k")))

(define-public crate-piston3d-gfx_voxel-0.26.0 (c (n "piston3d-gfx_voxel") (v "0.26.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.34.0") (d #t) (k 0)))) (h "1x3f5v0blkkj01lqzfnzzyn040r1slkg44pzs0yvn0dwq9gw8275")))

(define-public crate-piston3d-gfx_voxel-0.27.0 (c (n "piston3d-gfx_voxel") (v "0.27.0") (d (list (d (n "array") (r "^0.0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.17.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (f (quote ("png"))) (k 0)) (d (n "piston-gfx_texture") (r "^0.35.0") (d #t) (k 0)))) (h "05d6m5a5f55zaxz7lv833bic64kpppnc80p12zrrnrzrlkmh16nx")))

