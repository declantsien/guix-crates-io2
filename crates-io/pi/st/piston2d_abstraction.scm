(define-module (crates-io pi st piston2d_abstraction) #:use-module (crates-io))

(define-public crate-piston2d_abstraction-0.1.0 (c (n "piston2d_abstraction") (v "0.1.0") (d (list (d (n "piston2d_abstraction_proc_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.120.0") (d #t) (k 0)))) (h "1jr3hanadgm3d62xijqrmy559gi0w1p8krmx4nnmsk9h9f5l5yjh")))

(define-public crate-piston2d_abstraction-0.1.1 (c (n "piston2d_abstraction") (v "0.1.1") (d (list (d (n "piston2d_abstraction_proc_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.120.0") (d #t) (k 0)))) (h "0bl5x4rdag0rkkbi8xdj374sb17f6yxhdiv9p4wman2x19hvimvm")))

(define-public crate-piston2d_abstraction-0.1.2 (c (n "piston2d_abstraction") (v "0.1.2") (d (list (d (n "piston2d_abstraction_proc_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.120.0") (d #t) (k 0)))) (h "0p33nkcz15fc60cman2jbv9j8jv4wvwqaw0fijlb8y6nzv4q5gjk")))

(define-public crate-piston2d_abstraction-0.1.3 (c (n "piston2d_abstraction") (v "0.1.3") (d (list (d (n "piston2d_abstraction_proc_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.120.0") (d #t) (k 0)))) (h "1nflir1qc3avamjxf3l2z2kijxnl6yxf4m39xwh0c7z6zbp7ykb4")))

(define-public crate-piston2d_abstraction-0.1.4 (c (n "piston2d_abstraction") (v "0.1.4") (d (list (d (n "piston2d_abstraction_proc_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.120.0") (d #t) (k 0)))) (h "1rs52l4xbp5l3cprlfdiigx5rqb3qrwv751arsbf6as8cn4fx69j")))

(define-public crate-piston2d_abstraction-0.1.5 (c (n "piston2d_abstraction") (v "0.1.5") (d (list (d (n "piston2d_abstraction_proc_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.120.0") (d #t) (k 0)))) (h "0lyqwmvkim30gvhb7qrd3wk5r60msvlrd271n2syynjr92rz0r7c")))

(define-public crate-piston2d_abstraction-0.1.6 (c (n "piston2d_abstraction") (v "0.1.6") (d (list (d (n "piston2d_abstraction_proc_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 0)))) (h "1cbwqp9rx4c8g1x2mgbm54w0rxskdp45qkgdgbraxxgcznbwysw4")))

(define-public crate-piston2d_abstraction-0.1.7 (c (n "piston2d_abstraction") (v "0.1.7") (d (list (d (n "piston2d_abstraction_proc_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.120.0") (d #t) (k 0)))) (h "0p6lkhhy5kjs10lkkjqh7s7vlf00a55m8rg8wcq0ch68wc4m7m0c")))

(define-public crate-piston2d_abstraction-0.1.8 (c (n "piston2d_abstraction") (v "0.1.8") (d (list (d (n "lerp") (r "^0.4.0") (d #t) (k 0)) (d (n "piston2d_abstraction_proc_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.120.0") (d #t) (k 0)))) (h "0mz2vl1x148wap7jh39f4z7cyrx5afh9ckxpa8nz79dbwkammmzz")))

