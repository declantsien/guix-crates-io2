(define-module (crates-io pi st pistoncore-input) #:use-module (crates-io))

(define-public crate-pistoncore-input-0.0.0 (c (n "pistoncore-input") (v "0.0.0") (h "0qflp6zrha219ng38k47b54zk654dmv4xw20c9p24bnbzdf0vvnh")))

(define-public crate-pistoncore-input-0.0.1 (c (n "pistoncore-input") (v "0.0.1") (h "06riwr3m41ijqrwsbhxj49bzlc8sp36jhrsbb8dhzp783v9jccax")))

(define-public crate-pistoncore-input-0.0.5 (c (n "pistoncore-input") (v "0.0.5") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1c6sp7xqy7bziyw9pb740jrbd3k78sw8s2rsyx305j8bj102wd9a")))

(define-public crate-pistoncore-input-0.0.6 (c (n "pistoncore-input") (v "0.0.6") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0ci107y5vj3rd8pzdhqzzhsrqmp27dxs5fsjqfngs7056nxsym59")))

(define-public crate-pistoncore-input-0.0.7 (c (n "pistoncore-input") (v "0.0.7") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0r7ylq82c7lmfzpfvhsaqjcmsl0crfbn2a9l7vhkazf0szcz6jad")))

(define-public crate-pistoncore-input-0.0.8 (c (n "pistoncore-input") (v "0.0.8") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "num") (r "^0.1.21") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1n9dhcp824d7fciw823pnkwnz15br2rmn9qm6gfjb4mi0cxqn0l8")))

(define-public crate-pistoncore-input-0.0.9 (c (n "pistoncore-input") (v "0.0.9") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "num") (r "^0.1.21") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1fcmw72598waxn52hncy8rgixv7q5lzy9av8fc8is1vd88j25n4y")))

(define-public crate-pistoncore-input-0.1.0 (c (n "pistoncore-input") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.1.21") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "1am6flynvn83rg7iakvlhb1fa81msrq173227i7lybrg5gfgp13m")))

(define-public crate-pistoncore-input-0.2.0 (c (n "pistoncore-input") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "num") (r "^0.1.25") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "04n2g8l3630dapbsywrgz3lzzivlfi5papbql2p2v5wq2bjj6rsk")))

(define-public crate-pistoncore-input-0.3.0 (c (n "pistoncore-input") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "num") (r "^0.1.25") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "0m8bpsy39rpk6bqw2hgcmnri2gn5yqma1cm4v0lxsaq799pph6jl")))

(define-public crate-pistoncore-input-0.3.1 (c (n "pistoncore-input") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "num") (r "^0.1.25") (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "1736z9cfw3c3jjzsppd43y2ha3j3zkdzyq9hc68n9g2mpk5rswcl")))

(define-public crate-pistoncore-input-0.4.0 (c (n "pistoncore-input") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "1klr1lwxv9x7i0wxq2wf5z3qyg76kyimmm65qh9p44bi353n7qlw")))

(define-public crate-pistoncore-input-0.5.0 (c (n "pistoncore-input") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "184pmx3jhc0j6qd7x9x220g7mnjp0g43xi5mjwkk0qdh519sbn7s")))

(define-public crate-pistoncore-input-0.5.1 (c (n "pistoncore-input") (v "0.5.1") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1cdgfh5yagkybgy1sv26mvb2z4j64rvjf0f3xqzpy5syfv37jbd5")))

(define-public crate-pistoncore-input-0.6.0 (c (n "pistoncore-input") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0vqlq4zg3vbblmw5h1zlbxbm2gv77rn9rk07sl3dycmagkblprcs")))

(define-public crate-pistoncore-input-0.7.0 (c (n "pistoncore-input") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1y495rm7b5vrpxfxxjb4306x9iiyrncbl89i9i8lb4b1xs1fgkl7")))

(define-public crate-pistoncore-input-0.8.0 (c (n "pistoncore-input") (v "0.8.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1y3l6yvw16krc86vsb8551l9w96hxacg5kaajxbnkbf0c169xglr")))

(define-public crate-pistoncore-input-0.8.1 (c (n "pistoncore-input") (v "0.8.1") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0ssl0r9y6gzw9qplpnwv2v5bxkyh3qi22qlv0ab7r3qh85q7lsip")))

(define-public crate-pistoncore-input-0.9.0 (c (n "pistoncore-input") (v "0.9.0") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "06abgr8lyqb3za36hzaq6pqsh0lhb4s82qcvmphlwg0jhhi26f82")))

(define-public crate-pistoncore-input-0.10.0 (c (n "pistoncore-input") (v "0.10.0") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1b7mn9ypbc9psi99p0pbrkjsc6zfxhsiwqafpj1q04k3qs5f5dv6")))

(define-public crate-pistoncore-input-0.11.0 (c (n "pistoncore-input") (v "0.11.0") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0x6ajvaxlrm409l41fqvh9j46g4p6n3wi7hkmn5mhif38qpyypqv")))

(define-public crate-pistoncore-input-0.12.0 (c (n "pistoncore-input") (v "0.12.0") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "19d9s6i49ihwk6hp9waz3m83z0bq0bxy16nakkybsyxi66k9jv7m")))

(define-public crate-pistoncore-input-0.13.0 (c (n "pistoncore-input") (v "0.13.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0jc5ajfifj348m52wdy2vx5kbwdgz1nq00zcc2qjc9laxz360lc4")))

(define-public crate-pistoncore-input-0.14.0 (c (n "pistoncore-input") (v "0.14.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0a5zrqlbkps1zspfzhwi962hr9afm0f032g2glhcx81i1sk8354h")))

(define-public crate-pistoncore-input-0.15.0 (c (n "pistoncore-input") (v "0.15.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0v8xdm4xghiaw1nm592g85rbd737b9s0q7cmbqnx615jl9l9dxr2")))

(define-public crate-pistoncore-input-0.16.0 (c (n "pistoncore-input") (v "0.16.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1qj9mqjd8vn35ql5ci2v47jz50p8c30azwn5p8l15fxa1rxfzxp7")))

(define-public crate-pistoncore-input-0.17.0 (c (n "pistoncore-input") (v "0.17.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "021nd46gga1a8b1iag4k0djnxh2c0q7v7lfmghnbirirjdfg0832")))

(define-public crate-pistoncore-input-0.17.1 (c (n "pistoncore-input") (v "0.17.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "09dzzs1jx9ivyqca5wba6xkfqnxa0zwj27y3xlhzd1vrvx7nbi0c")))

(define-public crate-pistoncore-input-0.18.0 (c (n "pistoncore-input") (v "0.18.0") (d (list (d (n "bitflags") (r "^0.8.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1xcxyp6xzcryp8cv42kbr6yrq9963gh9l9cssxg2qhaf6hn56dmb")))

(define-public crate-pistoncore-input-0.19.0 (c (n "pistoncore-input") (v "0.19.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "03vdavm4imkizli6zzr7sg4qryxqybymgjv8vlmx8z0r00fg4ljw")))

(define-public crate-pistoncore-input-0.20.0 (c (n "pistoncore-input") (v "0.20.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0rnbzdwv5p1x9499q9q6a9ipa7hsb79s0rx0dbqygpz10d5z9zn7")))

(define-public crate-pistoncore-input-0.21.0 (c (n "pistoncore-input") (v "0.21.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0wj8ysm10mnfz2sb5yyxm51vi5q3m4m9bjlpm2mbhibb1gcdvfj3")))

(define-public crate-pistoncore-input-0.22.0 (c (n "pistoncore-input") (v "0.22.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0drhgk5drikr368dqzmj1hi7k1djbsbdcnl1nrzfwirws732k5js")))

(define-public crate-pistoncore-input-0.23.0 (c (n "pistoncore-input") (v "0.23.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0isfz6lsqydiavjzs7jrqd15bs69m68p24imdycihflf8v3s65fz")))

(define-public crate-pistoncore-input-0.24.0 (c (n "pistoncore-input") (v "0.24.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bq5lkw0x10fdiycl30g8jpq1naz4wscm816cklwifn78bi2qq8c")))

(define-public crate-pistoncore-input-0.25.0 (c (n "pistoncore-input") (v "0.25.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1cavsjbl965dk0amj9b17bj7szrp37j2ihs8ska8705wk4kbmg4j")))

(define-public crate-pistoncore-input-0.25.1 (c (n "pistoncore-input") (v "0.25.1") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0c7iw7w3y2f6bb87cjvj1d2vy6jvy47dn2vq45mrn7p27iwpqwva")))

(define-public crate-pistoncore-input-0.26.0 (c (n "pistoncore-input") (v "0.26.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0sapsmazdmnhiwplvc07q727hwwiz4d8bwj6a63lwdr6kwxagpgp")))

(define-public crate-pistoncore-input-0.27.0 (c (n "pistoncore-input") (v "0.27.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "04yn3sal8byfbh4mdrr0rswv21k4hxks4ndy88kjqnr61rv5ffaw")))

(define-public crate-pistoncore-input-0.28.0 (c (n "pistoncore-input") (v "0.28.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1rrcz9px098m3nx98gvrvzirfdp3vg03cblfkcrp4wnvswc0hwq5")))

(define-public crate-pistoncore-input-0.28.1 (c (n "pistoncore-input") (v "0.28.1") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0yjzrm0k5g8sn0gy9ayknigb06b3i5rawlkbf632mzj939nmfgxx")))

(define-public crate-pistoncore-input-1.0.0 (c (n "pistoncore-input") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0c5gv4aqj5ajmlp14i8f9l4q77s6x5xfccfsn833y8fqppkl3fs4")))

(define-public crate-pistoncore-input-1.0.1 (c (n "pistoncore-input") (v "1.0.1") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1w3ilidwr7dxlx3bwlppa160y98ci86aa2as8kym9i8nxgbgwxr9")))

