(define-module (crates-io pi st piston-graphics_api_version) #:use-module (crates-io))

(define-public crate-piston-graphics_api_version-0.1.0 (c (n "piston-graphics_api_version") (v "0.1.0") (h "0n0bnsxzhglfbcbh3hdz4wg6433zl1c1kdpiib4hzl520by7984a")))

(define-public crate-piston-graphics_api_version-0.2.0 (c (n "piston-graphics_api_version") (v "0.2.0") (h "1b5p6s45jqv057lpbxkiq3yrdjjhvcynmi2vjf8292rf0yh4hky5")))

(define-public crate-piston-graphics_api_version-1.0.0 (c (n "piston-graphics_api_version") (v "1.0.0") (h "064dmyxnhyy1gn14dcp6s214085zyz3s52mrg9qiwrqgl3bw041c")))

(define-public crate-piston-graphics_api_version-1.0.1 (c (n "piston-graphics_api_version") (v "1.0.1") (h "1v8vpln90zmlr400szw2dpjy61w181blafavv3c6g1537qg401iv")))

