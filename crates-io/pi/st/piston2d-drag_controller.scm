(define-module (crates-io pi st piston2d-drag_controller) #:use-module (crates-io))

(define-public crate-piston2d-drag_controller-0.0.1 (c (n "piston2d-drag_controller") (v "0.0.1") (d (list (d (n "pistoncore-event") (r "^0.0.1") (d #t) (k 0)))) (h "00qkcvm0apivgjlr71jrfjqylh2vmjc3f9w9607f8q946m8qv1nr")))

(define-public crate-piston2d-drag_controller-0.0.3 (c (n "piston2d-drag_controller") (v "0.0.3") (d (list (d (n "pistoncore-event") (r "^0.1.3") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.9") (d #t) (k 0)))) (h "1gawipxmm9zkg3p3ym938zlf5k5lwa6811q6lc6rrwaykx28x4z6")))

(define-public crate-piston2d-drag_controller-0.1.0 (c (n "piston2d-drag_controller") (v "0.1.0") (d (list (d (n "pistoncore-event") (r "^0.1.4") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)))) (h "0xikalb4ygv3f71mc4padvz65pd6vh9vjhhawh595id12br0jwp3")))

(define-public crate-piston2d-drag_controller-0.2.0 (c (n "piston2d-drag_controller") (v "0.2.0") (d (list (d (n "pistoncore-event") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)))) (h "0f68qpy5msliwk28v2a9bxjy4m91mgk83v1d7akln2bz8x3z62wz")))

(define-public crate-piston2d-drag_controller-0.3.0 (c (n "piston2d-drag_controller") (v "0.3.0") (d (list (d (n "pistoncore-event") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.2.0") (d #t) (k 0)))) (h "1i6vwlwcnnfwc2k8p6mj37skmma688lbwcfpcxh0i0xwy83yi2hi")))

(define-public crate-piston2d-drag_controller-0.4.0 (c (n "piston2d-drag_controller") (v "0.4.0") (d (list (d (n "pistoncore-event") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.2.0") (d #t) (k 0)))) (h "0pc41y3flhwks0qinp096ipligmx79w1khxllcj41ch4za2953am")))

(define-public crate-piston2d-drag_controller-0.5.0 (c (n "piston2d-drag_controller") (v "0.5.0") (d (list (d (n "pistoncore-input") (r "^0.3.0") (d #t) (k 0)))) (h "0p2pady2vkhxak9anipy90rn0g3w4akjkq8m5czkrr94aa7qf1xj")))

(define-public crate-piston2d-drag_controller-0.6.0 (c (n "piston2d-drag_controller") (v "0.6.0") (d (list (d (n "pistoncore-input") (r "^0.4.0") (d #t) (k 0)))) (h "0wqls2jb1nrhwl51l3ylq55yzj26mf3kbsj3fba6q17kv3zk5ym1")))

(define-public crate-piston2d-drag_controller-0.7.0 (c (n "piston2d-drag_controller") (v "0.7.0") (d (list (d (n "pistoncore-input") (r "^0.5.0") (d #t) (k 0)))) (h "1cybi4mmll0q6lzzla6dkg549wk3nmjakcxybvsdkzfxpxyd6daa")))

(define-public crate-piston2d-drag_controller-0.8.0 (c (n "piston2d-drag_controller") (v "0.8.0") (d (list (d (n "pistoncore-input") (r "^0.6.0") (d #t) (k 0)))) (h "0w5f3ni9ks3b190md5dv7d4hyxgylrvdkkkydf6asipm3p0041fl")))

(define-public crate-piston2d-drag_controller-0.9.0 (c (n "piston2d-drag_controller") (v "0.9.0") (d (list (d (n "pistoncore-input") (r "^0.7.0") (d #t) (k 0)))) (h "038gyqhwkrbfcmky91542ynkkj6jp25k2yjmxks8gy76w8aqwwn0")))

(define-public crate-piston2d-drag_controller-0.10.0 (c (n "piston2d-drag_controller") (v "0.10.0") (d (list (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)))) (h "1faj8qsh72l4dkh0l6ihap3camsm1ca9wk7pf55hq225j632yxvd")))

(define-public crate-piston2d-drag_controller-0.11.0 (c (n "piston2d-drag_controller") (v "0.11.0") (d (list (d (n "pistoncore-input") (r "^0.9.0") (d #t) (k 0)))) (h "05xvnja44nk02najmj5mp0x93xrwh28p0ldi5k1bdqznbbz1nn28")))

(define-public crate-piston2d-drag_controller-0.12.0 (c (n "piston2d-drag_controller") (v "0.12.0") (d (list (d (n "pistoncore-input") (r "^0.10.0") (d #t) (k 0)))) (h "00x9ab22mw8m9i71symh1md3lsjswky7mpy1h72f2xpwcckvgq4h")))

(define-public crate-piston2d-drag_controller-0.13.0 (c (n "piston2d-drag_controller") (v "0.13.0") (d (list (d (n "pistoncore-input") (r "^0.12.0") (d #t) (k 0)))) (h "0725z505khwwv384nlj7yjyz3q1szppbqmfk5rx56fnwsj0lanjs")))

(define-public crate-piston2d-drag_controller-0.14.0 (c (n "piston2d-drag_controller") (v "0.14.0") (d (list (d (n "pistoncore-input") (r "^0.13.0") (d #t) (k 0)))) (h "02cq0b982pl6krznvvlj5sxr2p95k62pls4r5a7bfbbksnim13x1")))

(define-public crate-piston2d-drag_controller-0.15.0 (c (n "piston2d-drag_controller") (v "0.15.0") (d (list (d (n "pistoncore-input") (r "^0.14.0") (d #t) (k 0)))) (h "0nhj8wd24y6b5psrxhj06ggqrlai5lc11cx6w776pvqw1iqk1rpb")))

(define-public crate-piston2d-drag_controller-0.16.0 (c (n "piston2d-drag_controller") (v "0.16.0") (d (list (d (n "pistoncore-input") (r "^0.15.0") (d #t) (k 0)))) (h "01fc5y24azw8xqq5w5zsskfjdhky5a6nnh428xhb1jsm2gblkww0")))

(define-public crate-piston2d-drag_controller-0.17.0 (c (n "piston2d-drag_controller") (v "0.17.0") (d (list (d (n "pistoncore-input") (r "^0.16.0") (d #t) (k 0)))) (h "02na4h4jzx5zc4n04hq2jx28pq44by8myysg4fq0hpd08x55b57c")))

(define-public crate-piston2d-drag_controller-0.18.0 (c (n "piston2d-drag_controller") (v "0.18.0") (d (list (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)))) (h "0rwdxr0p8s9ad3ch6b36mkx8c2kralq9ny2w8igl0j6qail6xvrh")))

(define-public crate-piston2d-drag_controller-0.19.0 (c (n "piston2d-drag_controller") (v "0.19.0") (d (list (d (n "pistoncore-input") (r "^0.18.0") (d #t) (k 0)))) (h "00pp45h6zg1niihsxx25i694iibz7hbr176na04r3pjvhppmfm95")))

(define-public crate-piston2d-drag_controller-0.20.0 (c (n "piston2d-drag_controller") (v "0.20.0") (d (list (d (n "pistoncore-input") (r "^0.19.0") (d #t) (k 0)))) (h "1y115v91kw65r6h3sf9gly653w0fqr4s4pkyf6p5kxg9igkr3yvw")))

(define-public crate-piston2d-drag_controller-0.21.0 (c (n "piston2d-drag_controller") (v "0.21.0") (d (list (d (n "pistoncore-input") (r "^0.20.0") (d #t) (k 0)))) (h "0l28g3w72k37v80yy631q4nn8ij5zrsphpq1p8mgvqpj9kwx86dk")))

(define-public crate-piston2d-drag_controller-0.22.0 (c (n "piston2d-drag_controller") (v "0.22.0") (d (list (d (n "pistoncore-input") (r "^0.21.0") (d #t) (k 0)))) (h "11fhw9dvdyi6zrv6r78dg97h0kxdf17m6qpwpn6k627k329mcfby")))

(define-public crate-piston2d-drag_controller-0.23.0 (c (n "piston2d-drag_controller") (v "0.23.0") (d (list (d (n "pistoncore-input") (r "^0.22.0") (d #t) (k 0)))) (h "03lnv8szqnhmaqk6mql6ma45qiw2klmqvw1zmkyg8xrd3ivriaj4")))

(define-public crate-piston2d-drag_controller-0.24.0 (c (n "piston2d-drag_controller") (v "0.24.0") (d (list (d (n "pistoncore-input") (r "^0.23.0") (d #t) (k 0)))) (h "18jg4gaz7ylidq59d38ab3bf1nywn9wfg36ihsrwig08127ydiib")))

(define-public crate-piston2d-drag_controller-0.25.0 (c (n "piston2d-drag_controller") (v "0.25.0") (d (list (d (n "pistoncore-input") (r "^0.24.0") (d #t) (k 0)))) (h "0cbrvlxq072hsmsaf3kzvmi6ylg9yv347vf5fdlbz863sndbh0bj")))

(define-public crate-piston2d-drag_controller-0.26.0 (c (n "piston2d-drag_controller") (v "0.26.0") (d (list (d (n "pistoncore-input") (r "^0.25.0") (d #t) (k 0)))) (h "16hmgp418ddjl5nr6545kcls6gnl2ljh50m2qdb4czf3na99vwqh")))

(define-public crate-piston2d-drag_controller-0.27.0 (c (n "piston2d-drag_controller") (v "0.27.0") (d (list (d (n "pistoncore-input") (r "^0.26.0") (d #t) (k 0)))) (h "0w0h07crs4vg470ipp8v0w71k48h5qxmb8hmis4a88vnj8f3wi9i")))

(define-public crate-piston2d-drag_controller-0.28.0 (c (n "piston2d-drag_controller") (v "0.28.0") (d (list (d (n "pistoncore-input") (r "^0.27.0") (d #t) (k 0)))) (h "1s1hi8bal3gy94v3vvmsc4vz6ca303zfgsncd026k6knlyhgc0pw")))

(define-public crate-piston2d-drag_controller-0.29.0 (c (n "piston2d-drag_controller") (v "0.29.0") (d (list (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)))) (h "1mpy3cg5nlm8ai9xvf2v3n5v9rk9657cn1sxiak30sq25nnkypc8")))

(define-public crate-piston2d-drag_controller-0.30.0 (c (n "piston2d-drag_controller") (v "0.30.0") (d (list (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)))) (h "04y4klfwn3lr358wmvl2m6lzbxbwhsdrxc63r59vpx3j88d2ix9g")))

