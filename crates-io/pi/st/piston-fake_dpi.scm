(define-module (crates-io pi st piston-fake_dpi) #:use-module (crates-io))

(define-public crate-piston-fake_dpi-0.1.0 (c (n "piston-fake_dpi") (v "0.1.0") (d (list (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.43.0") (d #t) (k 0)))) (h "1sng3xrbr13idy3amvg553wg206hrfhbdgzr08v2480a51gqvjii")))

(define-public crate-piston-fake_dpi-0.2.0 (c (n "piston-fake_dpi") (v "0.2.0") (d (list (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.44.0") (d #t) (k 0)))) (h "00sljrmyg20dr088yyvc5m3scqhvbjwsqn6aymfsdhc2z0qylgc3")))

(define-public crate-piston-fake_dpi-0.3.0 (c (n "piston-fake_dpi") (v "0.3.0") (d (list (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.45.0") (d #t) (k 0)))) (h "0yxcic0z34jryg0vyqvcfdi4ggbl0f0md8pganrgb5yyrdd56s1k")))

(define-public crate-piston-fake_dpi-0.4.0 (c (n "piston-fake_dpi") (v "0.4.0") (d (list (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.46.0") (d #t) (k 0)))) (h "1365xyziql96y37i5p44mrlw4gb5x99j3hvdw2mwbvs9wvgr5s0v")))

(define-public crate-piston-fake_dpi-0.5.0 (c (n "piston-fake_dpi") (v "0.5.0") (d (list (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.47.0") (d #t) (k 0)))) (h "1p7skjwj62lf8gjckzfmlgi7p1gclml6ca84m6nf2ap6f31pgrn3")))

(define-public crate-piston-fake_dpi-0.6.0 (c (n "piston-fake_dpi") (v "0.6.0") (d (list (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^1.0.0") (d #t) (k 0)))) (h "01fwfcv356aasygh7iawd4iwh0kacrr5cl3gm396cjndzf92x1bl")))

