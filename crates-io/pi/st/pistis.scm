(define-module (crates-io pi st pistis) #:use-module (crates-io))

(define-public crate-pistis-0.1.0 (c (n "pistis") (v "0.1.0") (h "144v373wdwry5k8v27gqz77p985hk9b72pvw0g57s6xslwvqi6mg") (y #t) (r "1.59")))

(define-public crate-pistis-0.0.0 (c (n "pistis") (v "0.0.0") (h "1hqdsnfk3xq2v267ih9mdfksa1w6pc7giz69c5agjwpc46dja2fz") (r "1.59")))

