(define-module (crates-io pi st piston) #:use-module (crates-io))

(define-public crate-piston-0.0.0 (c (n "piston") (v "0.0.0") (h "1b40z7zj48rbhi6ba3kh72kc9fshgbca50a7jvk6v7kqnkin1abk")))

(define-public crate-piston-0.0.1 (c (n "piston") (v "0.0.1") (d (list (d (n "pistoncore-event") (r "^0.0.6") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.5") (d #t) (k 0)) (d (n "quack") (r "^0.0.6") (d #t) (k 0)))) (h "0hrpfjynb98icps9vgxjiy9vqx2wi22nksyncj6v5ljc0cgahy5y")))

(define-public crate-piston-0.0.5 (c (n "piston") (v "0.0.5") (d (list (d (n "pistoncore-event") (r "^0.0.8") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.7") (d #t) (k 0)) (d (n "quack") (r "^0.0.10") (d #t) (k 0)))) (h "0yhir2h1spjmaai5h1jk9ppqj3gflg1v7vv0dn42gc8mqgxz8shg")))

(define-public crate-piston-0.0.6 (c (n "piston") (v "0.0.6") (d (list (d (n "pistoncore-event") (r "^0.0.9") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.8") (d #t) (k 0)) (d (n "quack") (r "^0.0.12") (d #t) (k 0)))) (h "0adk3c45rrzyblgq062w7pi1sf5h7h8708dcjwrb6cv82vrid6ns")))

(define-public crate-piston-0.0.7 (c (n "piston") (v "0.0.7") (d (list (d (n "pistoncore-event") (r "^0.0.10") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.12") (d #t) (k 0)) (d (n "quack") (r "^0.0.13") (d #t) (k 0)))) (h "16l305q9wmiw4616kzbafw3lzy1n07sv6bqmpn471ad28c11wv36")))

(define-public crate-piston-0.1.0 (c (n "piston") (v "0.1.0") (d (list (d (n "pistoncore-event") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.9") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.0") (d #t) (k 0)))) (h "0xhf5jkvw2qxxfq6z442dhq962vbjl8zhaacl9c3hq91djsib1jn")))

(define-public crate-piston-0.1.1 (c (n "piston") (v "0.1.1") (d (list (d (n "pistoncore-event") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.9") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.0") (d #t) (k 0)))) (h "0xgnn6yynifvi0kjq15iiqd0wp6chfrcgymdb86brqv1sf5hd6x2")))

(define-public crate-piston-0.1.2 (c (n "piston") (v "0.1.2") (d (list (d (n "pistoncore-event") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.9") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.0") (d #t) (k 0)))) (h "05jllbcqjch2izf331azlwz3gdyfw26drq0rvb4r2ik548dcnpd8")))

(define-public crate-piston-0.1.3 (c (n "piston") (v "0.1.3") (d (list (d (n "pistoncore-event") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.9") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.1") (d #t) (k 0)))) (h "0cv7czp7k87cdpp4l1g1bjz1anczc64g96ahl3kwk54x19klwc6a")))

(define-public crate-piston-0.1.4 (c (n "piston") (v "0.1.4") (d (list (d (n "pistoncore-event") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.0") (d #t) (k 0)))) (h "1lm45bi7psmafqcslv8dmkjjfpq7kzv92j7dyqmh9qrw37hs4z34")))

(define-public crate-piston-0.1.5 (c (n "piston") (v "0.1.5") (d (list (d (n "pistoncore-event") (r "^0.1.4") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.4") (d #t) (k 0)))) (h "0znyg576wkzfpxb22hxqnfsiwya03imf75in3ssiz1if54d5f9d1")))

(define-public crate-piston-0.2.0 (c (n "piston") (v "0.2.0") (d (list (d (n "pistoncore-event") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.4") (d #t) (k 0)))) (h "03gi9z45yx22992yzjbhngkryyr4na4nxf1vp3wb01v60yk1h6kp")))

(define-public crate-piston-0.2.1 (c (n "piston") (v "0.2.1") (d (list (d (n "pistoncore-event") (r "^0.2.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.5") (d #t) (k 0)))) (h "1x73rd1mkwaldki6g1qjh13z6765j1b02rbxgl7i19pqpkp75mxc")))

(define-public crate-piston-0.2.2 (c (n "piston") (v "0.2.2") (d (list (d (n "pistoncore-event") (r "^0.2.2") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.6") (d #t) (k 0)))) (h "13ic78dc66qh327mdvrcnyx367f5mpmwbfp3lxrijwcyd5vc7886")))

(define-public crate-piston-0.3.0 (c (n "piston") (v "0.3.0") (d (list (d (n "pistoncore-event") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.2.0") (d #t) (k 0)))) (h "02yhpilb0nz0x6niz75mz4xb0lrnrpw5b4cgqlmzj6419h3yvx0g")))

(define-public crate-piston-0.4.0 (c (n "piston") (v "0.4.0") (d (list (d (n "pistoncore-event") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.3.0") (d #t) (k 0)))) (h "0d3jr12vpgs8swl6mzngsffdsyffsva7bi3g73sslyv7df1pdaif")))

(define-public crate-piston-0.4.1 (c (n "piston") (v "0.4.1") (d (list (d (n "pistoncore-event") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.3.0") (d #t) (k 0)))) (h "1nj23gsf7fzs15n6s7kf0h49m04p4fcyi6mq39yfcakjmsbc7smy")))

(define-public crate-piston-0.5.0 (c (n "piston") (v "0.5.0") (d (list (d (n "pistoncore-event_loop") (r "^0.5.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.4.0") (d #t) (k 0)))) (h "13mq3l7asf15z3nzcb7z6bjaidzx70cnqq0zq66rjw6m0szvpp1n")))

(define-public crate-piston-0.6.0 (c (n "piston") (v "0.6.0") (d (list (d (n "pistoncore-event_loop") (r "^0.6.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.5.0") (d #t) (k 0)))) (h "02z5pyq0qrkvdbbq6afjmslz6c7nzx1dnysqqqk453s090vplj5a")))

(define-public crate-piston-0.7.0 (c (n "piston") (v "0.7.0") (d (list (d (n "pistoncore-event_loop") (r "^0.7.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.6.0") (d #t) (k 0)))) (h "1n8ymx5pk578m42vcp9j9d2vyrkdzk0grv9i9p3kn5q20ynrgcb3")))

(define-public crate-piston-0.8.0 (c (n "piston") (v "0.8.0") (d (list (d (n "pistoncore-event_loop") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.7.0") (d #t) (k 0)))) (h "01bqldy9w3jxm0hdxa6a7rljv7rmg1fwglinh7idhvxnjzzcnb3z")))

(define-public crate-piston-0.9.0 (c (n "piston") (v "0.9.0") (d (list (d (n "pistoncore-event_loop") (r "^0.9.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.8.0") (d #t) (k 0)))) (h "095kwwjc651sw8r6602dywh3b3jw9mbics3q2qxkgmzf1pd4v4a6")))

(define-public crate-piston-0.10.0 (c (n "piston") (v "0.10.0") (d (list (d (n "pistoncore-event_loop") (r "^0.10.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.5.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.9.0") (d #t) (k 0)))) (h "1n41rki9s0yszgqh6jk7ywxahxj02zh3wpixg9ik71pqd2qvxw40")))

(define-public crate-piston-0.10.1 (c (n "piston") (v "0.10.1") (d (list (d (n "pistoncore-event_loop") (r "^0.10.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.5.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.9.1") (d #t) (k 0)))) (h "1jvj5ylvv4v69h6hl4pn1q0wskgl59wb10w6hi3wmqjbwaa7h9f0")))

(define-public crate-piston-0.11.0 (c (n "piston") (v "0.11.0") (d (list (d (n "pistoncore-event_loop") (r "^0.11.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.6.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.10.0") (d #t) (k 0)))) (h "0w6gsgb0pdp3abk3isn3b0qfs5fgwcjjv03fhp2i9i0fa4sa5r38")))

(define-public crate-piston-0.12.0 (c (n "piston") (v "0.12.0") (d (list (d (n "pistoncore-event_loop") (r "^0.12.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.7.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.11.0") (d #t) (k 0)))) (h "0lkr62j4vzdqjacv99qbdch03v2qpxsjy0sny7cpnsfh7yqb5yn2")))

(define-public crate-piston-0.13.0 (c (n "piston") (v "0.13.0") (d (list (d (n "pistoncore-event_loop") (r "^0.13.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.12.0") (d #t) (k 0)))) (h "184lcnw681jiimvxpd5is0gfpxnbn2vav26mjy7mm5mq53ix031j")))

(define-public crate-piston-0.13.1 (c (n "piston") (v "0.13.1") (d (list (d (n "pistoncore-event_loop") (r "^0.13.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.12.0") (d #t) (k 0)))) (h "0gcrsxgq4ya0nwiw3233y1l0c58akw1x3v2zagvl08ymwagxgfd4")))

(define-public crate-piston-0.14.0 (c (n "piston") (v "0.14.0") (d (list (d (n "pistoncore-event_loop") (r "^0.14.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.12.1") (d #t) (k 0)))) (h "0647qic9jsi0z5lbc1ipdpy1rxhc9ry9byw74b6jqm2nzgkkshvn")))

(define-public crate-piston-0.15.0 (c (n "piston") (v "0.15.0") (d (list (d (n "pistoncore-event_loop") (r "^0.14.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.12.1") (d #t) (k 0)))) (h "1nhbzh6abj15x58g0j3h36pdk8gkyyzjqx1dmq42zsq7ba2sqmhh") (y #t)))

(define-public crate-piston-0.15.1 (c (n "piston") (v "0.15.1") (d (list (d (n "pistoncore-event_loop") (r "^0.15.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.13.0") (d #t) (k 0)))) (h "0ppb62sv89zpblxjpw15hh62k53vazvx0x9xfswc5k6fsdap2xkr")))

(define-public crate-piston-0.16.0 (c (n "piston") (v "0.16.0") (d (list (d (n "pistoncore-event_loop") (r "^0.16.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.14.0") (d #t) (k 0)))) (h "13737xkk9fbylygibz685kyz1a3khnmmmsrfsf9lhc6z45cw6lnf")))

(define-public crate-piston-0.17.0 (c (n "piston") (v "0.17.0") (d (list (d (n "pistoncore-event_loop") (r "^0.17.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.14.0") (d #t) (k 0)))) (h "1vr5gbzdbayjwvf4slw0ivw9g4c9yk4172wwjgpi7mlgkzylhdci")))

(define-public crate-piston-0.18.0 (c (n "piston") (v "0.18.0") (d (list (d (n "pistoncore-event_loop") (r "^0.18.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.15.0") (d #t) (k 0)))) (h "1f13wjvab5y5xycxl5bxq15y902ss2vipg5xsbdxhn3ck38g6xpg")))

(define-public crate-piston-0.19.0 (c (n "piston") (v "0.19.0") (d (list (d (n "pistoncore-event_loop") (r "^0.19.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.9.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.16.0") (d #t) (k 0)))) (h "1kirk7adl72lzzpjcd1ya9rqbryxscamif9hpz8brpdggxlsg8f5")))

(define-public crate-piston-0.20.0 (c (n "piston") (v "0.20.0") (d (list (d (n "pistoncore-event_loop") (r "^0.20.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.10.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.17.0") (d #t) (k 0)))) (h "08jdclizqihbxl1z4ib4wv11lwfr643xnrhkv744267qjrciizgx")))

(define-public crate-piston-0.20.1 (c (n "piston") (v "0.20.1") (d (list (d (n "pistoncore-event_loop") (r "^0.20.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.10.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.17.0") (d #t) (k 0)))) (h "1slprkdss79gdvny82y5sys7dx6wdn0xp90wilzwm89lq4fsjhzf")))

(define-public crate-piston-0.21.0 (c (n "piston") (v "0.21.0") (d (list (d (n "pistoncore-event_loop") (r "^0.21.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.11.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.18.0") (d #t) (k 0)))) (h "1i40ij7723v02ig49p1z6r8xqdpc1gcm3lysv4zwwhyq6wllc2l5")))

(define-public crate-piston-0.22.0 (c (n "piston") (v "0.22.0") (d (list (d (n "pistoncore-event_loop") (r "^0.22.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.12.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.19.0") (d #t) (k 0)))) (h "00fd13jjpk6nw8nr7zblhqc6scsdgxwgk6s11yng9ni4kk5hpk48")))

(define-public crate-piston-0.22.1 (c (n "piston") (v "0.22.1") (d (list (d (n "pistoncore-event_loop") (r "^0.22.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.12.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.19.0") (d #t) (k 0)))) (h "1gr5bmln8s58bws1hq89n1wvsjbp7cazbsdpci153msrlmwm3xjz")))

(define-public crate-piston-0.23.0 (c (n "piston") (v "0.23.0") (d (list (d (n "pistoncore-event_loop") (r "^0.23.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.12.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.20.0") (d #t) (k 0)))) (h "13rf5wj9iwxhsc9nc8r467ymy2hsachsl12z703k8yhpka403c3i")))

(define-public crate-piston-0.24.0 (c (n "piston") (v "0.24.0") (d (list (d (n "pistoncore-event_loop") (r "^0.24.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.13.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.21.0") (d #t) (k 0)))) (h "0jpc4cgwdzghpaxiqdb5y1s9js49mdxy660c78351xgn4ggn9wcn")))

(define-public crate-piston-0.25.0 (c (n "piston") (v "0.25.0") (d (list (d (n "pistoncore-event_loop") (r "^0.25.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.14.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.22.0") (d #t) (k 0)))) (h "0n0ks74h0h0bjkh7afqp2cknl4yqn8wakb5a5a36yb9my991a0hp")))

(define-public crate-piston-0.25.1 (c (n "piston") (v "0.25.1") (d (list (d (n "pistoncore-event_loop") (r "^0.25.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.14.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.22.1") (d #t) (k 0)))) (h "0qh2qi4njlrpr0i1bp64g0kfgqy9fsz2qb6m84mn4v3vw0pspr8y")))

(define-public crate-piston-0.26.0 (c (n "piston") (v "0.26.0") (d (list (d (n "pistoncore-event_loop") (r "^0.26.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.14.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.23.0") (d #t) (k 0)))) (h "06xfc45ay6kp4pi83454gcbnfv9gai14ccimgz2i9hcw1y6kij0n")))

(define-public crate-piston-0.27.0 (c (n "piston") (v "0.27.0") (d (list (d (n "pistoncore-event_loop") (r "^0.27.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.15.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.24.0") (d #t) (k 0)))) (h "09fii0lq110h3kk9v8vj44dhnz708pa5fz20qfi3vhv24psb8jwl")))

(define-public crate-piston-0.28.0 (c (n "piston") (v "0.28.0") (d (list (d (n "pistoncore-event_loop") (r "^0.28.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.16.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.25.0") (d #t) (k 0)))) (h "0hrjhdndqm26h4hndfan5z1bgidjmx6m7r07mpkicdcx93xs2g4v")))

(define-public crate-piston-0.29.0 (c (n "piston") (v "0.29.0") (d (list (d (n "pistoncore-event_loop") (r "^0.29.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.16.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.25.0") (d #t) (k 0)))) (h "0mv28nfdv5v1jnyyz6mlb2cbxvndypk55jh4ha0fy2fdpblz6fpq")))

(define-public crate-piston-0.30.0 (c (n "piston") (v "0.30.0") (d (list (d (n "pistoncore-event_loop") (r "^0.30.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.26.0") (d #t) (k 0)))) (h "1xrbv7hrirr2ay9h9a1j9vy9vmslg4pb2spcp1h9h0d6lsfm8dwr")))

(define-public crate-piston-0.31.0 (c (n "piston") (v "0.31.0") (d (list (d (n "pistoncore-event_loop") (r "^0.31.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.26.0") (d #t) (k 0)))) (h "0q9l7ymvb4sa0s27i6lp8yvn4faxrgsjjm9m4r4v09v86njwdj37")))

(define-public crate-piston-0.31.1 (c (n "piston") (v "0.31.1") (d (list (d (n "pistoncore-event_loop") (r "^0.31.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.26.0") (d #t) (k 0)))) (h "0wyga052gd8faw4n3r2qf89w1gzs2x9b1aclqniaq1gci37pr42g")))

(define-public crate-piston-0.31.2 (c (n "piston") (v "0.31.2") (d (list (d (n "pistoncore-event_loop") (r "^0.31.2") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.26.1") (d #t) (k 0)))) (h "1s4b6pcq1nkha31rqyv07k01px9n29whvy5g0224z89s05ad3bl1")))

(define-public crate-piston-0.31.3 (c (n "piston") (v "0.31.3") (d (list (d (n "pistoncore-event_loop") (r "^0.31.3") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.26.2") (d #t) (k 0)))) (h "0lb1v1ybbaabyyswwr6ahbjpk5ap0vmca7wy6rafnh4nxc628bxd")))

(define-public crate-piston-0.31.4 (c (n "piston") (v "0.31.4") (d (list (d (n "pistoncore-event_loop") (r "^0.31.4") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.26.2") (d #t) (k 0)))) (h "0d97av4mj5d9anyw5xq50252zxfqak82jiljzwnwfi0ma0q5b3ac")))

(define-public crate-piston-0.32.0 (c (n "piston") (v "0.32.0") (d (list (d (n "pistoncore-event_loop") (r "^0.32.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.18.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.27.0") (d #t) (k 0)))) (h "1lmfp6s99zrqghn8xb2zzw2sk72izcf24c7kqyxikvdiq3221ysc")))

(define-public crate-piston-0.33.0 (c (n "piston") (v "0.33.0") (d (list (d (n "pistoncore-event_loop") (r "^0.33.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.19.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.28.0") (d #t) (k 0)))) (h "1kvj1gb78q5x4pwiz21vw6pyryr7xc9770phrk9fnw35g4h5b63r")))

(define-public crate-piston-0.34.0 (c (n "piston") (v "0.34.0") (d (list (d (n "pistoncore-event_loop") (r "^0.34.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.19.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.29.0") (d #t) (k 0)))) (h "0kc2n27mhjz7gl6igbgwfhmld6fwvc93k0bcdnq2ih0r77qbb9rh")))

(define-public crate-piston-0.35.0 (c (n "piston") (v "0.35.0") (d (list (d (n "pistoncore-event_loop") (r "^0.35.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.20.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.30.0") (d #t) (k 0)))) (h "15qgnz2vr8sbfjhb2wl8fh1nzcnijrsi09cqr84zf7vk5c13apjq")))

(define-public crate-piston-0.36.0 (c (n "piston") (v "0.36.0") (d (list (d (n "pistoncore-event_loop") (r "^0.36.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.20.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.31.0") (d #t) (k 0)))) (h "0cpn5k2iz9nhp306skbk1llnzwyqqi11fmv8h7h3r57df143v4pg")))

(define-public crate-piston-0.37.0 (c (n "piston") (v "0.37.0") (d (list (d (n "pistoncore-event_loop") (r "^0.37.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.21.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.32.0") (d #t) (k 0)))) (h "1888h4rgw0r701x39b7vax4wnw3finxlaarrajk70zma4y120dai")))

(define-public crate-piston-0.38.0 (c (n "piston") (v "0.38.0") (d (list (d (n "pistoncore-event_loop") (r "^0.38.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.22.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.33.0") (d #t) (k 0)))) (h "16pcgjxb8adqvvw38llysav2979b3xgwzls1vy4xdwrpcv17gw0r")))

(define-public crate-piston-0.39.0 (c (n "piston") (v "0.39.0") (d (list (d (n "pistoncore-event_loop") (r "^0.39.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.23.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.34.0") (d #t) (k 0)))) (h "02f9yr1382x98izdi0mdlb7aa12n6pqjwjxbj2wr8ab66rmb2ahi")))

(define-public crate-piston-0.40.0 (c (n "piston") (v "0.40.0") (d (list (d (n "pistoncore-event_loop") (r "^0.40.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.23.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.35.0") (d #t) (k 0)))) (h "19r3hdp2xsrl1w2pwfp64miwhzn0qxv416mn99jcdxsnw2agq2a2")))

(define-public crate-piston-0.41.0 (c (n "piston") (v "0.41.0") (d (list (d (n "pistoncore-event_loop") (r "^0.41.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.24.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.36.0") (d #t) (k 0)))) (h "0hd60bvm6p9rc5p7s40y93fjkk8xnnrdx3ck43712igmgszqghx6")))

(define-public crate-piston-0.42.0 (c (n "piston") (v "0.42.0") (d (list (d (n "pistoncore-event_loop") (r "^0.42.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.24.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.37.0") (d #t) (k 0)))) (h "0rrixsc5p8j4137sxzzs0dr4rv5q0nxmxi9zg4gi7233ky3b3ryc")))

(define-public crate-piston-0.43.0 (c (n "piston") (v "0.43.0") (d (list (d (n "pistoncore-event_loop") (r "^0.43.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.25.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.38.0") (d #t) (k 0)))) (h "15pajrp18l3l5nbv0maih3qy49psff3ig34hh5mbjnc6mh641bwh")))

(define-public crate-piston-0.44.0 (c (n "piston") (v "0.44.0") (d (list (d (n "pistoncore-event_loop") (r "^0.44.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.25.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.39.0") (d #t) (k 0)))) (h "0f86miiifc6d03f540i29c1whgvalzirm52ly09fpim2yb3p3vf0")))

(define-public crate-piston-0.45.0 (c (n "piston") (v "0.45.0") (d (list (d (n "pistoncore-event_loop") (r "^0.45.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.25.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.40.0") (d #t) (k 0)))) (h "119238dj43093nna8z1y77p0l3cwss5a220g3bhmfynyg98wggsd")))

(define-public crate-piston-0.46.0 (c (n "piston") (v "0.46.0") (d (list (d (n "pistoncore-event_loop") (r "^0.46.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.26.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.41.0") (d #t) (k 0)))) (h "0074ii588ysqcg9zldyxk4adri46z3yimvh3xwdgwshiap21b4h2")))

(define-public crate-piston-0.47.0 (c (n "piston") (v "0.47.0") (d (list (d (n "pistoncore-event_loop") (r "^0.47.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.27.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.42.0") (d #t) (k 0)))) (h "02icm9g5fvzxs9jdkn6jw4mwbxpy7s2aqwfwwfsb0n0iap75h27x")))

(define-public crate-piston-0.48.0 (c (n "piston") (v "0.48.0") (d (list (d (n "pistoncore-event_loop") (r "^0.48.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.43.0") (d #t) (k 0)))) (h "05gv15bp0disgyyw8z60j215ylg6icivbnpvg5sd1zfmz3zxyrgv")))

(define-public crate-piston-0.49.0 (c (n "piston") (v "0.49.0") (d (list (d (n "pistoncore-event_loop") (r "^0.49.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.44.0") (d #t) (k 0)))) (h "1y0rbw92mzagqmwk79wv9axq0m7aid0s0d5cppyzh33wrxhdl3xj")))

(define-public crate-piston-0.50.0 (c (n "piston") (v "0.50.0") (d (list (d (n "pistoncore-event_loop") (r "^0.50.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.28.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.45.0") (d #t) (k 0)))) (h "06f7s679lmz1a80cws3p4r2i6jcy4n88194c82vpn582xfakq2ya")))

(define-public crate-piston-0.51.0 (c (n "piston") (v "0.51.0") (d (list (d (n "pistoncore-event_loop") (r "^0.51.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.46.0") (d #t) (k 0)))) (h "0nvfj95p1yxb8fcsjhz7ak5y19jywrss5dxw271y8q1fzvi15yw7")))

(define-public crate-piston-0.52.0 (c (n "piston") (v "0.52.0") (d (list (d (n "pistoncore-event_loop") (r "^0.52.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.47.0") (d #t) (k 0)))) (h "19kh4r1aqyk8jwk37jppc04f8gz1xf0iiw2kxm5z1xd106n97jvn")))

(define-public crate-piston-0.52.1 (c (n "piston") (v "0.52.1") (d (list (d (n "pistoncore-event_loop") (r "^0.52.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.47.0") (d #t) (k 0)))) (h "0xcbm98i2p96rkaig5kiqz8zbfy9hx74slp45yf7wkqm9qkdl3ra")))

(define-public crate-piston-0.53.0 (c (n "piston") (v "0.53.0") (d (list (d (n "pistoncore-event_loop") (r "^0.53.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.47.0") (d #t) (k 0)))) (h "0x84f74nj2qr2xs0lm0nyr6kg6wldcr701lhyfz1jdv0xgjbjv8b")))

(define-public crate-piston-0.53.1 (c (n "piston") (v "0.53.1") (d (list (d (n "pistoncore-event_loop") (r "^0.53.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.47.1") (d #t) (k 0)))) (h "1rnnb0zry19sd37r5mwsg6lq3s4a1sr3y24zgkr80viiz703nj3z")))

(define-public crate-piston-0.53.2 (c (n "piston") (v "0.53.2") (d (list (d (n "pistoncore-event_loop") (r "^0.53.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.47.1") (d #t) (k 0)))) (h "09p50rqzg9fk60as84i0bdilnq6xqddvc3xw0d0vwgh6w63m8ikf")))

(define-public crate-piston-0.54.0 (c (n "piston") (v "0.54.0") (d (list (d (n "pistoncore-event_loop") (r "^0.54.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^1.0.0") (d #t) (k 0)))) (h "1nrjgcknw57pf3j1m15ah4l5q0yj189cvincl6rpx9kvl9rbzz76")))

(define-public crate-piston-0.55.0 (c (n "piston") (v "0.55.0") (d (list (d (n "pistoncore-event_loop") (r "^0.55.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^1.0.0") (d #t) (k 0)))) (h "150la6syvsyqvqlh9ld1w782xh40m24pvx8sq2w9zcrgfw088zjv") (f (quote (("async" "pistoncore-event_loop/async"))))))

(define-public crate-piston-1.0.0 (c (n "piston") (v "1.0.0") (d (list (d (n "pistoncore-event_loop") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^1.0.0") (d #t) (k 0)))) (h "0gg62n8mlplwvcyn73rzg397788xdmsiv04jhj5cmpz3wsinqx2z") (f (quote (("async" "pistoncore-event_loop/async"))))))

