(define-module (crates-io pi st piston_window_game) #:use-module (crates-io))

(define-public crate-piston_window_game-0.1.0 (c (n "piston_window_game") (v "0.1.0") (d (list (d (n "gfx") (r "^0.6.3") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.4.0") (d #t) (k 0)) (d (n "piston2d-gfx_graphics") (r "^0.1.22") (d #t) (k 0)) (d (n "piston_window") (r "^0.3.0") (d #t) (k 0)))) (h "0nxgik5lz8gc8l49dzsayyis6011nxds216nw0vhzss1nhhd88dj") (y #t)))

