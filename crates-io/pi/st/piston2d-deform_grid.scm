(define-module (crates-io pi st piston2d-deform_grid) #:use-module (crates-io))

(define-public crate-piston2d-deform_grid-0.1.0 (c (n "piston2d-deform_grid") (v "0.1.0") (d (list (d (n "piston2d-graphics") (r "^0.31.0") (d #t) (k 0)))) (h "1n5kikxmkq9b025vq9pla9kgshsbhcvdwak1mydpp32gf4b58k6j")))

(define-public crate-piston2d-deform_grid-0.2.0 (c (n "piston2d-deform_grid") (v "0.2.0") (d (list (d (n "piston2d-graphics") (r "^0.32.0") (d #t) (k 0)))) (h "1qmg01zynpd3qmiqixmlw4pjrz892d31yawfhzbdvjgyv1hy878a")))

(define-public crate-piston2d-deform_grid-0.3.0 (c (n "piston2d-deform_grid") (v "0.3.0") (d (list (d (n "piston2d-graphics") (r "^0.33.0") (d #t) (k 0)))) (h "1xlvbmld1szafjhhff23dqxnwvpiixzvlc6zgrw6rvsfp1xks3xp")))

(define-public crate-piston2d-deform_grid-0.4.0 (c (n "piston2d-deform_grid") (v "0.4.0") (d (list (d (n "piston2d-graphics") (r "^0.34.0") (d #t) (k 0)))) (h "0w7ss6pvl4939cfjh23vblj16pxg6v6i1af7gqmsq9g867adp2xv")))

(define-public crate-piston2d-deform_grid-0.5.0 (c (n "piston2d-deform_grid") (v "0.5.0") (d (list (d (n "piston2d-graphics") (r "^0.35.0") (d #t) (k 0)))) (h "1b5b30ywihylmb5vhbbz9ns16qxg7bj5a7xc23bw248w8nilb4qg")))

(define-public crate-piston2d-deform_grid-0.6.0 (c (n "piston2d-deform_grid") (v "0.6.0") (d (list (d (n "piston2d-graphics") (r "^0.36.0") (d #t) (k 0)))) (h "0hl11ycz6fy5c1sdi7f99h9xr6mxlzwxr8hwqv0mir72jdq3m66f")))

(define-public crate-piston2d-deform_grid-0.7.0 (c (n "piston2d-deform_grid") (v "0.7.0") (d (list (d (n "piston2d-graphics") (r "^0.37.0") (d #t) (k 0)))) (h "1fwf93lxgx0672ng7q7ssz07cx2ssf5qlvgs42ks36bjz048icmr")))

(define-public crate-piston2d-deform_grid-0.8.0 (c (n "piston2d-deform_grid") (v "0.8.0") (d (list (d (n "piston2d-graphics") (r "^0.38.0") (d #t) (k 0)))) (h "0xfsqmzv34piiqxf5p73jk2wp5slz125sh4r86hn33giranai1mg")))

(define-public crate-piston2d-deform_grid-0.9.0 (c (n "piston2d-deform_grid") (v "0.9.0") (d (list (d (n "piston2d-graphics") (r "^0.39.0") (d #t) (k 0)))) (h "1rl0dg2gzm06m9galk59c3pbs7rshca82q1pri7j4bf6wivzm9zl")))

(define-public crate-piston2d-deform_grid-0.10.0 (c (n "piston2d-deform_grid") (v "0.10.0") (d (list (d (n "piston2d-graphics") (r "^0.40.0") (d #t) (k 0)))) (h "0vgy80kiakgcjir4095vfy82ksh6mcy2706y20qjf024599idg1y")))

(define-public crate-piston2d-deform_grid-0.11.0 (c (n "piston2d-deform_grid") (v "0.11.0") (d (list (d (n "piston2d-graphics") (r "^0.41.0") (d #t) (k 0)))) (h "1fxisb3idyv1f7gdkk04kf7cycfjsj1rs6dkvdvpl8caf514dmhh")))

(define-public crate-piston2d-deform_grid-0.12.0 (c (n "piston2d-deform_grid") (v "0.12.0") (d (list (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)))) (h "0pqbzwj5rl6dq64zkm506pwgqxlfv44rnrirvxcqp3kfxh0r4gb7")))

(define-public crate-piston2d-deform_grid-0.13.0 (c (n "piston2d-deform_grid") (v "0.13.0") (d (list (d (n "piston2d-graphics") (r "^0.43.0") (d #t) (k 0)))) (h "0f5vm0yf5a5imwa6adi1sy4fircssdyqwgbladkrnhrgidxlmjpz")))

(define-public crate-piston2d-deform_grid-0.14.0 (c (n "piston2d-deform_grid") (v "0.14.0") (d (list (d (n "piston2d-graphics") (r "^0.44.0") (d #t) (k 0)))) (h "1sfsy3265d3yqrprzlxq9kn67fqg09xjpwdi2b00hyifhdl3mq1s")))

