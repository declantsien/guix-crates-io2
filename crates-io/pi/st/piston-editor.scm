(define-module (crates-io pi st piston-editor) #:use-module (crates-io))

(define-public crate-piston-editor-0.1.0 (c (n "piston-editor") (v "0.1.0") (h "1l7lyjicgif6d66awsmkv1as46y4nxq0gr4m3ml15hq0c2nvdhbi")))

(define-public crate-piston-editor-0.2.0 (c (n "piston-editor") (v "0.2.0") (h "0kb52fff9vgv7m8yjznk00zk8dyl0yd57dv26k18r00aq2mhd3m3")))

(define-public crate-piston-editor-0.2.1 (c (n "piston-editor") (v "0.2.1") (h "1acjbqibkd91hxld6kpskb35vgsi17bafbnq9z66yfk3012xrqzg")))

(define-public crate-piston-editor-0.2.2 (c (n "piston-editor") (v "0.2.2") (h "0d3cgs35jj98dvc94pvifvl1drz9fmriw7shqfji6z05xwfpc266")))

(define-public crate-piston-editor-0.2.3 (c (n "piston-editor") (v "0.2.3") (h "0jc2jra407ncnwkn9hdiqpz4vvfz56gr4bppdp591vsvpz8lrp49")))

(define-public crate-piston-editor-0.2.4 (c (n "piston-editor") (v "0.2.4") (h "1a70ghdvcsm3s9by9086m6p9n72wwpgzfqy73k8r0z0fkyml37g7")))

(define-public crate-piston-editor-0.3.0 (c (n "piston-editor") (v "0.3.0") (h "08snghbp12s1qg6hkzrnk19s90p91vvin5958zdli4sb2k8x2rd1")))

