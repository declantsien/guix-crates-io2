(define-module (crates-io pi st piston2d-shapes) #:use-module (crates-io))

(define-public crate-piston2d-shapes-0.1.0 (c (n "piston2d-shapes") (v "0.1.0") (d (list (d (n "piston2d-graphics") (r "^0.7.1") (d #t) (k 0)))) (h "1219fvnn9gv9zysglh2qlq0wgiqfpzyczcs2v46vhmziw8rzdcpa")))

(define-public crate-piston2d-shapes-0.2.0 (c (n "piston2d-shapes") (v "0.2.0") (d (list (d (n "piston2d-graphics") (r "^0.8.0") (d #t) (k 0)))) (h "140b1gds5vxjjc7lz9nvbvcj1hnh7qhmp9f1nzyf316rv72wp85r")))

(define-public crate-piston2d-shapes-0.3.0 (c (n "piston2d-shapes") (v "0.3.0") (d (list (d (n "piston2d-graphics") (r "^0.9.0") (d #t) (k 0)))) (h "1vwwxx7hylzj3783cyp55dh0sdrrxx8k7jry4ya1l35qk3hvmiqy")))

(define-public crate-piston2d-shapes-0.4.0 (c (n "piston2d-shapes") (v "0.4.0") (d (list (d (n "piston2d-graphics") (r "^0.10.0") (d #t) (k 0)))) (h "0gzq4dzqdxhhh1a1qs0l15pvvnc4mrmxx41cy6qp294kkq2hr7bp")))

(define-public crate-piston2d-shapes-0.5.0 (c (n "piston2d-shapes") (v "0.5.0") (d (list (d (n "piston2d-graphics") (r "^0.11.0") (d #t) (k 0)))) (h "041pdx3qcjranm08jmnar24n36c0k9cclaz9c2ciflcjvf3jazzf")))

(define-public crate-piston2d-shapes-0.6.0 (c (n "piston2d-shapes") (v "0.6.0") (d (list (d (n "piston2d-graphics") (r "^0.12.0") (d #t) (k 0)))) (h "041ldpfigkx022k2x8ag5jvc9gmdcd2jdh4z4mi7yc25brhfqsni")))

(define-public crate-piston2d-shapes-0.7.0 (c (n "piston2d-shapes") (v "0.7.0") (d (list (d (n "piston2d-graphics") (r "^0.13.0") (d #t) (k 0)))) (h "0ifvznss4q8nzl8ng58axvdgwwpr031vvaddwgnsqhpk7sb4hcz6")))

(define-public crate-piston2d-shapes-0.8.0 (c (n "piston2d-shapes") (v "0.8.0") (d (list (d (n "piston2d-graphics") (r "^0.14.0") (d #t) (k 0)))) (h "1ir347baldxyxqaba11d1bhwhkvsykpyh9vc6bf77kijl13gils0")))

(define-public crate-piston2d-shapes-0.9.0 (c (n "piston2d-shapes") (v "0.9.0") (d (list (d (n "piston2d-graphics") (r "^0.15.0") (d #t) (k 0)))) (h "0p7iqkiv6vjjqhvjqyhc2i0il7m9m45xm0fyasjyap5x2fhij25s")))

(define-public crate-piston2d-shapes-0.10.0 (c (n "piston2d-shapes") (v "0.10.0") (d (list (d (n "piston2d-graphics") (r "^0.16.0") (d #t) (k 0)))) (h "08j6sfy6y6xxlsxv5h6vrlvyj9xsaivkjcx7hmc7j7k984zicsly")))

(define-public crate-piston2d-shapes-0.11.0 (c (n "piston2d-shapes") (v "0.11.0") (d (list (d (n "piston2d-graphics") (r "^0.17.0") (d #t) (k 0)))) (h "1b1wl9n85z943fscxv5l58g014kk0n56j1zwzarq3krcqk81bnp4")))

(define-public crate-piston2d-shapes-0.12.0 (c (n "piston2d-shapes") (v "0.12.0") (d (list (d (n "piston2d-graphics") (r "^0.18.0") (d #t) (k 0)))) (h "1gm6vmnzr0fp569psipw851ixf74cbspn7g42pmvl2grm2vx8k9h")))

(define-public crate-piston2d-shapes-0.13.0 (c (n "piston2d-shapes") (v "0.13.0") (d (list (d (n "piston2d-graphics") (r "^0.19.0") (d #t) (k 0)))) (h "1p40gal0v1i1jg9gz7ik109bgrjrg7dym4ydvwqv5sbn3wjpyb2j")))

(define-public crate-piston2d-shapes-0.14.0 (c (n "piston2d-shapes") (v "0.14.0") (d (list (d (n "piston2d-graphics") (r "^0.20.0") (d #t) (k 0)))) (h "0q46ihdiamiv406md1w0c7fwys2vri1qgs80fswgyqxc3f5s9d58")))

(define-public crate-piston2d-shapes-0.15.0 (c (n "piston2d-shapes") (v "0.15.0") (d (list (d (n "piston2d-graphics") (r "^0.21.0") (d #t) (k 0)))) (h "131b2hc9xcwgax8yfl3jmz02j051lx7a8amryfm57yw4xdmr2qz3")))

(define-public crate-piston2d-shapes-0.16.0 (c (n "piston2d-shapes") (v "0.16.0") (d (list (d (n "piston2d-graphics") (r "^0.22.0") (d #t) (k 0)))) (h "0fj539w58gkijhlf9vbnhcrankpvr7rk3b62by5djp57y8v2gjya")))

(define-public crate-piston2d-shapes-0.17.0 (c (n "piston2d-shapes") (v "0.17.0") (d (list (d (n "piston2d-graphics") (r "^0.23.0") (d #t) (k 0)))) (h "0ljc3qzq532damjxz9cnk7afyzlrjx1icbpcghlmp065vi6pf5xg")))

(define-public crate-piston2d-shapes-0.18.0 (c (n "piston2d-shapes") (v "0.18.0") (d (list (d (n "piston2d-graphics") (r "^0.24.0") (d #t) (k 0)))) (h "01dc1g1dddk6bazn0fp08m1vdqy5rvb216kwjd7wjysavy87akkh")))

(define-public crate-piston2d-shapes-0.19.0 (c (n "piston2d-shapes") (v "0.19.0") (d (list (d (n "piston2d-graphics") (r "^0.25.0") (d #t) (k 0)))) (h "1arpzgkzb456j8cvvdmkvvzxpb20aakycpl18w0nnxgccfy9yk02")))

(define-public crate-piston2d-shapes-0.20.0 (c (n "piston2d-shapes") (v "0.20.0") (d (list (d (n "piston2d-graphics") (r "^0.26.0") (d #t) (k 0)))) (h "068r7jns9nxcnkggp1fjd549c5ymfk0blfkq7m737pzp438ibb4l")))

(define-public crate-piston2d-shapes-0.21.0 (c (n "piston2d-shapes") (v "0.21.0") (d (list (d (n "piston2d-graphics") (r "^0.27.0") (d #t) (k 0)))) (h "1r9yb7irm8c90mh7idm1zhw84vlczv3yrgnqsfrvwcis8i3ps92f")))

(define-public crate-piston2d-shapes-0.22.0 (c (n "piston2d-shapes") (v "0.22.0") (d (list (d (n "piston2d-graphics") (r "^0.28.0") (d #t) (k 0)))) (h "000q5620h5w902nw1lp2l498gz4fqkjv3qmdigi1lka9bwfil9km")))

(define-public crate-piston2d-shapes-0.23.0 (c (n "piston2d-shapes") (v "0.23.0") (d (list (d (n "piston2d-graphics") (r "^0.29.0") (d #t) (k 0)))) (h "05jxhf9pgj5135zjwbvljjvliyy2gc307x93pfjfyqc444qihp1y")))

(define-public crate-piston2d-shapes-0.24.0 (c (n "piston2d-shapes") (v "0.24.0") (d (list (d (n "piston2d-graphics") (r "^0.30.0") (d #t) (k 0)))) (h "1a9f1khl1l4qbafda4q3qpbybv68662hb3x9vpn5mjlpb32zdhjn")))

(define-public crate-piston2d-shapes-0.25.0 (c (n "piston2d-shapes") (v "0.25.0") (d (list (d (n "piston2d-graphics") (r "^0.31.0") (d #t) (k 0)))) (h "1clgi92wb23nqkgrv6bb01in2q7ircsk9dc1sk51v2n6xmdv30xb")))

(define-public crate-piston2d-shapes-0.26.0 (c (n "piston2d-shapes") (v "0.26.0") (d (list (d (n "piston2d-graphics") (r "^0.32.0") (d #t) (k 0)))) (h "1mwjvkkl0ffmrcaknp2gvyww8cdx78dmnliv6w3zndsdj6i52a2y")))

(define-public crate-piston2d-shapes-0.27.0 (c (n "piston2d-shapes") (v "0.27.0") (d (list (d (n "piston2d-graphics") (r "^0.33.0") (d #t) (k 0)))) (h "1dzr7bqg8r0rr87cq18250f4599hlv8akzcrygrpkir2s5rpkpy8")))

(define-public crate-piston2d-shapes-0.28.0 (c (n "piston2d-shapes") (v "0.28.0") (d (list (d (n "piston2d-graphics") (r "^0.34.0") (d #t) (k 0)))) (h "1sni69jcdbd9kgbzy5ksmnsy3iff0hw053w15fspw9kh8z82lb1f")))

(define-public crate-piston2d-shapes-0.29.0 (c (n "piston2d-shapes") (v "0.29.0") (d (list (d (n "piston2d-graphics") (r "^0.35.0") (d #t) (k 0)))) (h "1zdzj3jbj9bs9gf5g0bpf9fcympryxp37i69a5p3vjgf5jp5czpf")))

(define-public crate-piston2d-shapes-0.30.0 (c (n "piston2d-shapes") (v "0.30.0") (d (list (d (n "piston2d-graphics") (r "^0.36.0") (d #t) (k 0)))) (h "1rryfxdcnkiyhwzxjab8p698r8nrsbcx3bggjgg1a5ayj13fjxsf")))

(define-public crate-piston2d-shapes-0.31.0 (c (n "piston2d-shapes") (v "0.31.0") (d (list (d (n "piston2d-graphics") (r "^0.37.0") (d #t) (k 0)))) (h "03bapn7lb9pbj3f4kkq1485x3zfaf3klygidp3pvnyrv9ysvbmvg")))

(define-public crate-piston2d-shapes-0.32.0 (c (n "piston2d-shapes") (v "0.32.0") (d (list (d (n "piston2d-graphics") (r "^0.38.0") (d #t) (k 0)))) (h "0jj62jngn6h91i1qfsmhx8br8yc71r435f5hywmi7881np6q99c6")))

(define-public crate-piston2d-shapes-0.33.0 (c (n "piston2d-shapes") (v "0.33.0") (d (list (d (n "piston2d-graphics") (r "^0.39.0") (d #t) (k 0)))) (h "0wz7iyvy9xpq9pqdwqaig16dd9zwviq29iawji9ljbnxrd3acyhh")))

(define-public crate-piston2d-shapes-0.34.0 (c (n "piston2d-shapes") (v "0.34.0") (d (list (d (n "piston2d-graphics") (r "^0.40.0") (d #t) (k 0)))) (h "0r28a9r30hwdpjbfn5vx2fs1n7m8ha8jxxgg80kr6wrs34ap87wm")))

(define-public crate-piston2d-shapes-0.35.0 (c (n "piston2d-shapes") (v "0.35.0") (d (list (d (n "piston2d-graphics") (r "^0.41.0") (d #t) (k 0)))) (h "1q8icslpkajybb3ljr1nm9m4yxv1fsry2b39xjc23gjr5d92jlrj")))

(define-public crate-piston2d-shapes-0.36.0 (c (n "piston2d-shapes") (v "0.36.0") (d (list (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)))) (h "1skm02bh8bb2j5cxaaydxih594vyyxdbamgc66mkqaamrhgbfny2")))

(define-public crate-piston2d-shapes-0.38.0 (c (n "piston2d-shapes") (v "0.38.0") (d (list (d (n "piston2d-graphics") (r "^0.44.0") (d #t) (k 0)))) (h "1kfih9s2z17ahg5izfcnd8bvp5ywz3h1dnfd1165g9cyg2b30m0q")))

