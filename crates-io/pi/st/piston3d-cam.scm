(define-module (crates-io pi st piston3d-cam) #:use-module (crates-io))

(define-public crate-piston3d-cam-0.0.1 (c (n "piston3d-cam") (v "0.0.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "pistoncore-event") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "vecmath") (r "^0.0.3") (d #t) (k 0)))) (h "04qws5sqpdf3p7nxwkqldksp9fa9ryd65r9r70yayifk9clppwyw")))

(define-public crate-piston3d-cam-0.0.4 (c (n "piston3d-cam") (v "0.0.4") (d (list (d (n "quaternion") (r "^0.0.5") (d #t) (k 0)) (d (n "vecmath") (r "^0.0.5") (d #t) (k 0)))) (h "1l3y62wfrl4y1n1q9ndp1n7w7jg2rjx6lsx0q3z8yasrk79d65rc")))

(define-public crate-piston3d-cam-0.0.5 (c (n "piston3d-cam") (v "0.0.5") (d (list (d (n "quaternion") (r "^0.0.5") (d #t) (k 0)) (d (n "vecmath") (r "^0.0.5") (d #t) (k 0)))) (h "188m9aq4f5s0c4lhqpl02zp0lbmmrm411ldknw0g8335hr2203cv")))

(define-public crate-piston3d-cam-0.0.7 (c (n "piston3d-cam") (v "0.0.7") (d (list (d (n "quaternion") (r "^0.0.6") (d #t) (k 0)) (d (n "vecmath") (r "^0.0.22") (d #t) (k 0)))) (h "1xgyrg3gxy2wiiccx7f1bdpamjra4fmp6kwr2ps6ddx1nrn4dv6a")))

(define-public crate-piston3d-cam-0.0.8 (c (n "piston3d-cam") (v "0.0.8") (d (list (d (n "quaternion") (r "^0.0.7") (d #t) (k 0)) (d (n "vecmath") (r "^0.0.23") (d #t) (k 0)))) (h "0vwz9ky04gyg14lxn7khimj50a784h465k7cks7bw8pc4y1f1qsj")))

(define-public crate-piston3d-cam-0.1.0 (c (n "piston3d-cam") (v "0.1.0") (d (list (d (n "quaternion") (r "^0.1.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.1.0") (d #t) (k 0)))) (h "0ydajs4z1b2msq6smxpx0crri4kdqhm69fnlhpkjqas95hmzpzc8")))

(define-public crate-piston3d-cam-0.2.0 (c (n "piston3d-cam") (v "0.2.0") (d (list (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "11xqz2p564wx00bxi6hd903ayasj0w4dg1vawkhwcf4250fskn2z")))

(define-public crate-piston3d-cam-0.3.0 (c (n "piston3d-cam") (v "0.3.0") (d (list (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "02jj5hgvjngh86ymndyyi4rxlpsqphxbc3nfjcqrxl6409604lv1")))

(define-public crate-piston3d-cam-0.4.0 (c (n "piston3d-cam") (v "0.4.0") (d (list (d (n "quaternion") (r "^0.4.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "082inzbjh7v1igadfjky6jqwzwc19rrzlg9w2dnbiwbw9fjbb6y2")))

(define-public crate-piston3d-cam-0.5.0 (c (n "piston3d-cam") (v "0.5.0") (d (list (d (n "quaternion") (r "^0.4.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "1wp5vx18wgd119rzsq109i7pam1893kaiq4hxcqjyh4v5mz032r0")))

(define-public crate-piston3d-cam-0.6.0 (c (n "piston3d-cam") (v "0.6.0") (d (list (d (n "quaternion") (r "^1.0.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "0aafpc10hqsmxkhg0prybaya8r5cx9jp452vkqxm5s88v2qbmfwl")))

