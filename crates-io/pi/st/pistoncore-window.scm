(define-module (crates-io pi st pistoncore-window) #:use-module (crates-io))

(define-public crate-pistoncore-window-0.0.0 (c (n "pistoncore-window") (v "0.0.0") (d (list (d (n "pistoncore-current") (r "^0.0.0") (d #t) (k 0)) (d (n "pistoncore-event_loop") (r "^0.0.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.0") (d #t) (k 0)))) (h "0bqbjcs0r89ghsx46dmnrf04ygaddwp4p22gc6d6jcaypgddlpcx")))

(define-public crate-pistoncore-window-0.0.1 (c (n "pistoncore-window") (v "0.0.1") (d (list (d (n "pistoncore-current") (r "^0.0.1") (d #t) (k 0)) (d (n "pistoncore-event_loop") (r "^0.0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.1") (d #t) (k 0)))) (h "1iw87gv91cnrj7ppqxbgrcvj8sxs7cfih5kaqy58yz776lbc8mm6")))

(define-public crate-pistoncore-window-0.0.2 (c (n "pistoncore-window") (v "0.0.2") (d (list (d (n "pistoncore-event_loop") (r "^0.0.3") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "quack") (r "^0.0.1") (d #t) (k 0)))) (h "10493wayandcsy4bcrg4jpdma5kzpb7r1xln2gl92xg894bvdkh7")))

(define-public crate-pistoncore-window-0.0.3 (c (n "pistoncore-window") (v "0.0.3") (d (list (d (n "pistoncore-event_loop") (r "^0.0.4") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "quack") (r "^0.0.2") (d #t) (k 0)))) (h "0d1m3zgsvg6hair3q35p612nvbmyn2k27zfvziq46dniv7h7iak6")))

(define-public crate-pistoncore-window-0.0.4 (c (n "pistoncore-window") (v "0.0.4") (d (list (d (n "pistoncore-event_loop") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "quack") (r "^0.0.3") (d #t) (k 0)))) (h "1j34qd0k23zplxx7l7r8ri0kwgqlxc0k7jkprgmxmncf8lznfh1c")))

(define-public crate-pistoncore-window-0.0.5 (c (n "pistoncore-window") (v "0.0.5") (d (list (d (n "pistoncore-event_loop") (r "^0.0.6") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "quack") (r "^0.0.6") (d #t) (k 0)))) (h "0j17amm7dk6jzv7dl50vfq6j454r0djzdysqx2wxvq4dvz8g916s")))

(define-public crate-pistoncore-window-0.0.7 (c (n "pistoncore-window") (v "0.0.7") (d (list (d (n "pistoncore-event_loop") (r "^0.0.10") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "quack") (r "^0.0.10") (d #t) (k 0)))) (h "10q1jr8ib3isb2xccgcbfiq59rf2nq4ypkbbi2rg4807r39yj6l5")))

(define-public crate-pistoncore-window-0.0.8 (c (n "pistoncore-window") (v "0.0.8") (d (list (d (n "pistoncore-event_loop") (r "^0.0.11") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "quack") (r "^0.0.12") (d #t) (k 0)))) (h "0awj7b3i0m60qwkzkd7a3nwm5ka1q2b3c27wqgbnq6v8g3yx4vpa")))

(define-public crate-pistoncore-window-0.0.11 (c (n "pistoncore-window") (v "0.0.11") (d (list (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "quack") (r "^0.0.12") (d #t) (k 0)))) (h "1bbhmfy55pycs81pd9qycsw7vcyih0rrj272iibl8kqfkz361lxq")))

(define-public crate-pistoncore-window-0.0.12 (c (n "pistoncore-window") (v "0.0.12") (d (list (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "quack") (r "^0.0.13") (d #t) (k 0)))) (h "0j15jf2niig64w7c8awgm376qfwl6pmkq15kly2vaz18ix0nfjv8")))

(define-public crate-pistoncore-window-0.0.13 (c (n "pistoncore-window") (v "0.0.13") (d (list (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "quack") (r "^0.0.13") (d #t) (k 0)))) (h "1fl6sibs63bq8r2crb4p606i2p8q83h6p0db7r367a0spv25ys3j")))

(define-public crate-pistoncore-window-0.0.14 (c (n "pistoncore-window") (v "0.0.14") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.7") (d #t) (k 0)) (d (n "quack") (r "^0.0.13") (d #t) (k 0)))) (h "1hfyw8s82z02zbr3dyh92qab0mhr3w2q1cn4kzafhd8xszli2dbi")))

(define-public crate-pistoncore-window-0.0.15 (c (n "pistoncore-window") (v "0.0.15") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.7") (d #t) (k 0)) (d (n "quack") (r "^0.0.13") (d #t) (k 0)))) (h "0y2klryi4d90q275kg93gsjixr7ly1lf6hzd3dvxvzsd8y70bkq2")))

(define-public crate-pistoncore-window-0.1.0 (c (n "pistoncore-window") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.9") (d #t) (k 0)))) (h "0sh18i4f7f8c68d333kr94hjh52wzlxdizc9vcakymbd6cr7rsfk")))

(define-public crate-pistoncore-window-0.1.1 (c (n "pistoncore-window") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.9") (d #t) (k 0)))) (h "0i4ph4zg4z1j76rx1ahz1nkdbql02agnv3w9q55lsi4r4xp9mk2m")))

(define-public crate-pistoncore-window-0.1.2 (c (n "pistoncore-window") (v "0.1.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.9") (d #t) (k 0)))) (h "181yv33yhay81bll4x2c0xak12hvjgzvcs84zdgzj6z4b8hhvy75")))

(define-public crate-pistoncore-window-0.1.3 (c (n "pistoncore-window") (v "0.1.3") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)))) (h "0ysqs9hzk1qhhdqw4m79cxc2z9s791w45n0y28qcswj58q9pwcm7")))

(define-public crate-pistoncore-window-0.1.4 (c (n "pistoncore-window") (v "0.1.4") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.1.0") (d #t) (k 0)))) (h "1fcgh9xpamskg7rfvbpjgq4ipb7j6h6yvh10c2ywxkhn0hydvf9j")))

(define-public crate-pistoncore-window-0.1.5 (c (n "pistoncore-window") (v "0.1.5") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.1.0") (d #t) (k 0)))) (h "1iapmi8x32dj09addnarh0cjfzk68pk9bh6qvzbai17c3cp7hals")))

(define-public crate-pistoncore-window-0.1.6 (c (n "pistoncore-window") (v "0.1.6") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.1.0") (d #t) (k 0)))) (h "1p4sci8zspxaw4cd71gkalvjp3h9i8rizp5l0vllyi5i4vfq3h34")))

(define-public crate-pistoncore-window-0.2.0 (c (n "pistoncore-window") (v "0.2.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.2.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.1.0") (d #t) (k 0)))) (h "0clmdrmq94azv944f872imrjrzlg2f3g63a8023hc54d6nj44fym")))

(define-public crate-pistoncore-window-0.3.0 (c (n "pistoncore-window") (v "0.3.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.2.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.0") (d #t) (k 0)))) (h "0fi5armx352v8x4n1ar0g8kdqrz4pl4zkhfj6f5v4fclw167r74z")))

(define-public crate-pistoncore-window-0.4.0 (c (n "pistoncore-window") (v "0.4.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.3.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.0") (d #t) (k 0)))) (h "1jnd2ha6wwhl3b5hfz39lldh4dmnmm0mbv98kgmfsbdyb8rnbvd0")))

(define-public crate-pistoncore-window-0.5.0 (c (n "pistoncore-window") (v "0.5.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.3.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.0") (d #t) (k 0)))) (h "16p7wa8p0f9g2mam83l4wdq8khyf9q7hlj8gkmfh33aiygmpvbhf")))

(define-public crate-pistoncore-window-0.6.0 (c (n "pistoncore-window") (v "0.6.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.4.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.0") (d #t) (k 0)))) (h "1yl41p5bb3s0gp8848f5jdbi4sm0v9q55d8aqidxbm3fnikan2vd")))

(define-public crate-pistoncore-window-0.7.0 (c (n "pistoncore-window") (v "0.7.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.4.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.0") (d #t) (k 0)))) (h "1b14dmv2asn875lb8i39jgisc4w5sc9yxz31snyxfy7mnghnp0wj")))

(define-public crate-pistoncore-window-0.8.0 (c (n "pistoncore-window") (v "0.8.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.4.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.0") (d #t) (k 0)))) (h "1izw33r6jww7vlis80sdfpd6dfrhl5yvqv9mzw7s95s393p1cvmq")))

(define-public crate-pistoncore-window-0.9.0 (c (n "pistoncore-window") (v "0.9.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.5.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.0") (d #t) (k 0)))) (h "02b4d4hkbsgkasd35ayqpqwmx6j6wzcbfa03amfrq84mf2pn73zy")))

(define-public crate-pistoncore-window-0.9.1 (c (n "pistoncore-window") (v "0.9.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.5.1") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "0dd343n8rmkqdqafm9ipw0zrvlw0licq0wp60q50yrnv6b6cl3ri")))

(define-public crate-pistoncore-window-0.10.0 (c (n "pistoncore-window") (v "0.10.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.6.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "182s7690xp4jz33a0i31dghjci1gjnf2gh3bxbdwqa41xlasjlka")))

(define-public crate-pistoncore-window-0.11.0 (c (n "pistoncore-window") (v "0.11.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.7.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "0q0l8g1b8gsdhw78p77539xs054iq5bb0lg21xqkz0x6bax16s1q")))

(define-public crate-pistoncore-window-0.12.0 (c (n "pistoncore-window") (v "0.12.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "137n1l95i4h6k6pswdfv973mvwdpw0svlvsf4qkzvnfkvqaw568j")))

(define-public crate-pistoncore-window-0.12.1 (c (n "pistoncore-window") (v "0.12.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "09pwy2sb2ixwpmih2i1x9w2m0wgyj8qfkjjw8aazij8klzxr7al0")))

(define-public crate-pistoncore-window-0.13.0 (c (n "pistoncore-window") (v "0.13.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "18w289n8w8lhfgpbgxbl40yg7w6qi81kypri8hgxr87ngyvgwjcv")))

(define-public crate-pistoncore-window-0.14.0 (c (n "pistoncore-window") (v "0.14.0") (d (list (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "17f2lisg0cqjsx33mpk82dbfvjin744n7k671aw7mgy35ck1r2x6")))

(define-public crate-pistoncore-window-0.15.0 (c (n "pistoncore-window") (v "0.15.0") (d (list (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "0kjy7j05sh2v0rh5hg78x9243jqfhxrba4px3amd4mqkx9jvbv4j")))

(define-public crate-pistoncore-window-0.16.0 (c (n "pistoncore-window") (v "0.16.0") (d (list (d (n "pistoncore-input") (r "^0.9.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "1rv60ya6rb4am30krw1hsaz9s6zgh51vkpzr7j9mg56jzs0bcl77")))

(define-public crate-pistoncore-window-0.17.0 (c (n "pistoncore-window") (v "0.17.0") (d (list (d (n "pistoncore-input") (r "^0.10.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "03r1w6i40b71ff7fm9qcj0r6y58q38dq37118j5gyb9zy9dfplq1")))

(define-public crate-pistoncore-window-0.18.0 (c (n "pistoncore-window") (v "0.18.0") (d (list (d (n "pistoncore-input") (r "^0.11.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "118kd38k0l9gii2v8ijrb3g9s6m4bwc1w5dxg8baigwzs1g27z1r")))

(define-public crate-pistoncore-window-0.19.0 (c (n "pistoncore-window") (v "0.19.0") (d (list (d (n "pistoncore-input") (r "^0.12.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "0ib38vbifd9p2nwkxd0574hs2rifs49miw823774gdsqa7xbdny8")))

(define-public crate-pistoncore-window-0.20.0 (c (n "pistoncore-window") (v "0.20.0") (d (list (d (n "pistoncore-input") (r "^0.12.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "1wb6dmkgkgi11slw3cpidasg6ma2frc28dq0b24isf4bybac5c9d")))

(define-public crate-pistoncore-window-0.21.0 (c (n "pistoncore-window") (v "0.21.0") (d (list (d (n "pistoncore-input") (r "^0.13.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "0rx160afmcc1qxvz5aaxr5r51m0lx84c0vxfwzg2cadgwivxchp2")))

(define-public crate-pistoncore-window-0.22.0 (c (n "pistoncore-window") (v "0.22.0") (d (list (d (n "pistoncore-input") (r "^0.14.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "0a4a1xxc4ixjr4n6cxfy3dw29lsq3vhki6q2yv8prkkwjfyxay38")))

(define-public crate-pistoncore-window-0.22.1 (c (n "pistoncore-window") (v "0.22.1") (d (list (d (n "pistoncore-input") (r "^0.14.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "01lkdngk456j6kr58fh21nm0n5l9g7qc1qswdsmipkk8y4a5w4km")))

(define-public crate-pistoncore-window-0.23.0 (c (n "pistoncore-window") (v "0.23.0") (d (list (d (n "pistoncore-input") (r "^0.14.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "1mh003z0qkc18z0hgj212za6mv1affid7z1n37zrx1cicli1jl84")))

(define-public crate-pistoncore-window-0.24.0 (c (n "pistoncore-window") (v "0.24.0") (d (list (d (n "pistoncore-input") (r "^0.15.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "1vvbrqf61sjchygj9b31g6hy8v37w9iaw8lpa9kzpv7x6qd0by36")))

(define-public crate-pistoncore-window-0.25.0 (c (n "pistoncore-window") (v "0.25.0") (d (list (d (n "pistoncore-input") (r "^0.16.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "0sv5jnnbrxly0qvnq1i25yrpyhs1ndwji58f5lir9s4129dyvjjh")))

(define-public crate-pistoncore-window-0.26.0 (c (n "pistoncore-window") (v "0.26.0") (d (list (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "09nnxq0b4lrm0ldilhni4k9nj1yr15ga8l1zb8riawjbd9imm842")))

(define-public crate-pistoncore-window-0.26.1 (c (n "pistoncore-window") (v "0.26.1") (d (list (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "0mbls7iyzk8h1c0igp8p4f3fga0mbd50r0bblasxa9yivjhawl4h")))

(define-public crate-pistoncore-window-0.26.2 (c (n "pistoncore-window") (v "0.26.2") (d (list (d (n "pistoncore-input") (r "^0.17.1") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "1h6w4b8i70q6pajlrmpvgsisximc0g8bmxz2bb34y5kgl09853r9")))

(define-public crate-pistoncore-window-0.27.0 (c (n "pistoncore-window") (v "0.27.0") (d (list (d (n "pistoncore-input") (r "^0.18.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "1k2jv8xarpgg243g71vvbbvl99vbzqfn9yl7pzf824pwq83d35i5")))

(define-public crate-pistoncore-window-0.28.0 (c (n "pistoncore-window") (v "0.28.0") (d (list (d (n "pistoncore-input") (r "^0.19.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.2.1") (d #t) (k 0)))) (h "1p0xgd33qkmy01nkrs8l0gzz42rb0ayvnirsbz56892434z93k9z")))

(define-public crate-pistoncore-window-0.29.0 (c (n "pistoncore-window") (v "0.29.0") (d (list (d (n "pistoncore-input") (r "^0.19.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.3.0") (d #t) (k 0)))) (h "1a1s6qqq64yj8xa3glrx6h3ws37jmdg9zzqh0jz9xrghp7l5s5ry")))

(define-public crate-pistoncore-window-0.30.0 (c (n "pistoncore-window") (v "0.30.0") (d (list (d (n "pistoncore-input") (r "^0.20.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.3.0") (d #t) (k 0)))) (h "1vdbjf69gj9qp9q4vzpf345lqvi47blin9zdciq6wp4l133wka9j")))

(define-public crate-pistoncore-window-0.31.0 (c (n "pistoncore-window") (v "0.31.0") (d (list (d (n "pistoncore-input") (r "^0.20.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.3.0") (d #t) (k 0)))) (h "0ri4a8gq52abhfsnjww5b9i97lhrz48229aign2vawqpw6v8sq62")))

(define-public crate-pistoncore-window-0.32.0 (c (n "pistoncore-window") (v "0.32.0") (d (list (d (n "pistoncore-input") (r "^0.21.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.3.0") (d #t) (k 0)))) (h "0rw50f3k28g05i4g8gkwkmabkkc83li1ry7w2g29k0wgyc6sxza8")))

(define-public crate-pistoncore-window-0.33.0 (c (n "pistoncore-window") (v "0.33.0") (d (list (d (n "pistoncore-input") (r "^0.22.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.3.0") (d #t) (k 0)))) (h "0fhxh9xv89mzsfi8ws07ra5lac3h83w9qnx0gzpfhlhnxy4fk9h9")))

(define-public crate-pistoncore-window-0.34.0 (c (n "pistoncore-window") (v "0.34.0") (d (list (d (n "pistoncore-input") (r "^0.23.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.3.0") (d #t) (k 0)))) (h "013qhvq5fwkbrmpfzgw6jdd9ng8zfs1dqhjmplf7v6d7fsjwgkrr")))

(define-public crate-pistoncore-window-0.35.0 (c (n "pistoncore-window") (v "0.35.0") (d (list (d (n "pistoncore-input") (r "^0.23.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.3.0") (d #t) (k 0)))) (h "0h5lpq0amwpni2grlaa6x568nnppqr45wy5xrp40flnd59bgdfav")))

(define-public crate-pistoncore-window-0.36.0 (c (n "pistoncore-window") (v "0.36.0") (d (list (d (n "pistoncore-input") (r "^0.24.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.3.0") (d #t) (k 0)))) (h "00ghbw0yav74nmkx7gdj262q1qj2a7pzv3pzwqckyca460fak8pz")))

(define-public crate-pistoncore-window-0.37.0 (c (n "pistoncore-window") (v "0.37.0") (d (list (d (n "pistoncore-input") (r "^0.24.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.3.0") (d #t) (k 0)))) (h "1p8x9g1qp7lrbfklcnxrdb8d9hlzw13zk2xc6xkkxd1xcv838fc6")))

(define-public crate-pistoncore-window-0.38.0 (c (n "pistoncore-window") (v "0.38.0") (d (list (d (n "pistoncore-input") (r "^0.25.0") (d #t) (k 0)) (d (n "shader_version") (r "^0.3.0") (d #t) (k 0)))) (h "08m4bm8v2zbmwv69ndr4qnc485y3z9bsps8avcgk0nvjyx1qqzx0")))

(define-public crate-pistoncore-window-0.39.0 (c (n "pistoncore-window") (v "0.39.0") (d (list (d (n "piston-graphics_api_version") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.25.0") (d #t) (k 0)))) (h "044c8njggn63b5b4fnnq4mb9nvilr3d5mrgn2brbls04z8wc85sr")))

(define-public crate-pistoncore-window-0.40.0 (c (n "pistoncore-window") (v "0.40.0") (d (list (d (n "piston-graphics_api_version") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.25.1") (d #t) (k 0)))) (h "0kdgqfisgms3pnnb6360fp5zcjh8jqyhzwsa0w66aj0n4s599yhy")))

(define-public crate-pistoncore-window-0.41.0 (c (n "pistoncore-window") (v "0.41.0") (d (list (d (n "piston-graphics_api_version") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.26.0") (d #t) (k 0)))) (h "0l9lwyfsr2jzg7c3rrw7hdhqndn4dhq5d6n30sz4pj25c1xjfg6b")))

(define-public crate-pistoncore-window-0.42.0 (c (n "pistoncore-window") (v "0.42.0") (d (list (d (n "piston-graphics_api_version") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.27.0") (d #t) (k 0)))) (h "17l9lx4ia453sygl910jsyzkvzh4q9rcn7db2s4fpb7dirr6kl74")))

(define-public crate-pistoncore-window-0.43.0 (c (n "pistoncore-window") (v "0.43.0") (d (list (d (n "piston-graphics_api_version") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)))) (h "0rhpzxik3j3bj7nklyd8d6lfzrmq3rhvzppgnbvyyqya42m6abi9")))

(define-public crate-pistoncore-window-0.44.0 (c (n "pistoncore-window") (v "0.44.0") (d (list (d (n "piston-graphics_api_version") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)))) (h "18qy3nnpb9jczvkiyzzznamck0pzgiyi6073jrkldnci6b3in10q")))

(define-public crate-pistoncore-window-0.45.0 (c (n "pistoncore-window") (v "0.45.0") (d (list (d (n "piston-graphics_api_version") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.28.1") (d #t) (k 0)))) (h "1y9hxbxf4i5a3gl3cw40pf1xzzvd7l09lgp445zl3crdp6hp6yz9")))

(define-public crate-pistoncore-window-0.46.0 (c (n "pistoncore-window") (v "0.46.0") (d (list (d (n "piston-graphics_api_version") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)))) (h "1a7lfx5znn9b1padhkiyan4l291h66mw5ivcnhfp00cz5yfsk5n1")))

(define-public crate-pistoncore-window-0.47.0 (c (n "pistoncore-window") (v "0.47.0") (d (list (d (n "piston-graphics_api_version") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)))) (h "0civsnfr4syz27cj2mlbh0xiafy60xkd5c7wng6k2hl97lw34jdv")))

(define-public crate-pistoncore-window-0.47.1 (c (n "pistoncore-window") (v "0.47.1") (d (list (d (n "piston-graphics_api_version") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.1") (d #t) (k 0)))) (h "1gshbq5a12xkrl301lvhypqahn6wzq1260vyqx1k3hfgx6s64afn")))

(define-public crate-pistoncore-window-1.0.0 (c (n "pistoncore-window") (v "1.0.0") (d (list (d (n "piston-graphics_api_version") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^1.0.1") (d #t) (k 0)))) (h "1la33wcvqkidm3xk5pip8yi38pryn6sjj0ljncvml8f8f75g1yi6")))

