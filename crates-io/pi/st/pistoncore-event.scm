(define-module (crates-io pi st pistoncore-event) #:use-module (crates-io))

(define-public crate-pistoncore-event-0.0.0 (c (n "pistoncore-event") (v "0.0.0") (d (list (d (n "pistoncore-current") (r "^0.0.0") (d #t) (k 0)) (d (n "pistoncore-event_loop") (r "^0.0.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.0") (d #t) (k 0)))) (h "0r6f850pw38kgcdsvggwdcwyf32dr94id0i9swjp82kp4620yr7l")))

(define-public crate-pistoncore-event-0.0.1 (c (n "pistoncore-event") (v "0.0.1") (d (list (d (n "pistoncore-current") (r "^0.0.1") (d #t) (k 0)) (d (n "pistoncore-event_loop") (r "^0.0.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.1") (d #t) (k 0)))) (h "1h1lg8jqxmnkg7xzxpv16fmh5fapbmyjh64rv6jrb6gikn95gbkj")))

(define-public crate-pistoncore-event-0.0.2 (c (n "pistoncore-event") (v "0.0.2") (d (list (d (n "pistoncore-event_loop") (r "^0.0.3") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.2") (d #t) (k 0)))) (h "1p0m9swpzlial3d6cfnklklk5vx9kqmwh4yns9cj50wky1z6mi2b")))

(define-public crate-pistoncore-event-0.0.3 (c (n "pistoncore-event") (v "0.0.3") (d (list (d (n "pistoncore-event_loop") (r "^0.0.3") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.2") (d #t) (k 0)))) (h "09fprvqlcvxjkfycpz2wl8wpah5p1v6bjfgabw17wd0b0sg6dric")))

(define-public crate-pistoncore-event-0.0.4 (c (n "pistoncore-event") (v "0.0.4") (d (list (d (n "pistoncore-event_loop") (r "^0.0.4") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.3") (d #t) (k 0)))) (h "1yyihvv1vz2zbcrk4rvjjk72mm4i6qcbjq72mw86pnk1bg5f0ivw")))

(define-public crate-pistoncore-event-0.0.5 (c (n "pistoncore-event") (v "0.0.5") (d (list (d (n "pistoncore-event_loop") (r "^0.0.4") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.3") (d #t) (k 0)))) (h "110as0q3z581a6s1vyqpi1d3rddyrnpn4129ryr6pn7rd7qfhi1b")))

(define-public crate-pistoncore-event-0.0.6 (c (n "pistoncore-event") (v "0.0.6") (d (list (d (n "pistoncore-event_loop") (r "^0.0.7") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.5") (d #t) (k 0)))) (h "0lvqjlysmqqzjp2d53g0mhbcb16dqjr69dv4c5vpkv45b51m5dnz")))

(define-public crate-pistoncore-event-0.0.8 (c (n "pistoncore-event") (v "0.0.8") (d (list (d (n "pistoncore-event_loop") (r "^0.0.10") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.7") (d #t) (k 0)))) (h "0nhy39qagcz4901i77n583bxa0qvwnyxv1jcps7bgvcbcqiibvgr")))

(define-public crate-pistoncore-event-0.0.9 (c (n "pistoncore-event") (v "0.0.9") (d (list (d (n "pistoncore-event_loop") (r "^0.0.11") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.8") (d #t) (k 0)))) (h "078cr5d55n176kk66yyz9zqh966kl4hkbl21jx3lzbpc31i1m22x")))

(define-public crate-pistoncore-event-0.0.10 (c (n "pistoncore-event") (v "0.0.10") (d (list (d (n "pistoncore-event_loop") (r "^0.0.13") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.12") (d #t) (k 0)))) (h "0m4hbiq9r696rm10kgmc4a0y1014by78qyz1g64pmdqi33nbii4m")))

(define-public crate-pistoncore-event-0.0.12 (c (n "pistoncore-event") (v "0.0.12") (d (list (d (n "pistoncore-event_loop") (r "^0.0.17") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.7") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.15") (d #t) (k 0)))) (h "1h26j667gllxhhk9c95f97hk4p2aai0srykqii1kz95dhnf47qcp")))

(define-public crate-pistoncore-event-0.1.0 (c (n "pistoncore-event") (v "0.1.0") (d (list (d (n "pistoncore-event_loop") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.9") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.0") (d #t) (k 0)))) (h "0ncy2f09jrzmrzqac31sn4sh2wxpj7mdxpkk25150z3jgpl7n5y9")))

(define-public crate-pistoncore-event-0.1.3 (c (n "pistoncore-event") (v "0.1.3") (d (list (d (n "pistoncore-event_loop") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.0.9") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.0") (d #t) (k 0)))) (h "1vry050592dxz9zj6xf9wxi7ib2j9fsnqqg3x3pack1ripjmj159")))

(define-public crate-pistoncore-event-0.1.4 (c (n "pistoncore-event") (v "0.1.4") (d (list (d (n "pistoncore-event_loop") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.0") (d #t) (k 0)))) (h "1x15fvlyx6rp65w4dzbvz64zid0sch0cswqmpqq1bv7bgsdqqwi5")))

(define-public crate-pistoncore-event-0.2.0 (c (n "pistoncore-event") (v "0.2.0") (d (list (d (n "pistoncore-event_loop") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.4") (d #t) (k 0)))) (h "0pb0hrbfrp9kimvz6c9r0s9nlq9akzaq7p7kkavgfr17h1vdm7sy")))

(define-public crate-pistoncore-event-0.2.1 (c (n "pistoncore-event") (v "0.2.1") (d (list (d (n "pistoncore-event_loop") (r "^0.2.1") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.5") (d #t) (k 0)))) (h "00b0cgs93jcd4rf6ph0g4v0rmmjgl7f8ag3727kw06sp72sf5p5d")))

(define-public crate-pistoncore-event-0.2.2 (c (n "pistoncore-event") (v "0.2.2") (d (list (d (n "pistoncore-event_loop") (r "^0.2.2") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.6") (d #t) (k 0)))) (h "1hbn6qwwi1n75c39rccjbyvdaip069bh9msjrkdhfrhac5dz8xgx")))

(define-public crate-pistoncore-event-0.3.0 (c (n "pistoncore-event") (v "0.3.0") (d (list (d (n "pistoncore-event_loop") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.2.0") (d #t) (k 0)))) (h "08qz6hh0y74nsd02j3mg5s75b4rdc7wzmyjzvav7mmvdf35aq3rh")))

(define-public crate-pistoncore-event-0.4.0 (c (n "pistoncore-event") (v "0.4.0") (d (list (d (n "pistoncore-event_loop") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.3.0") (d #t) (k 0)))) (h "0xz0rqw6cfk232dyvi5cqdhzd4y0zbcg30mlznmbj6q1q2lqzfpl")))

