(define-module (crates-io pi st piston2d_abstraction_proc_macros) #:use-module (crates-io))

(define-public crate-piston2d_abstraction_proc_macros-0.1.0 (c (n "piston2d_abstraction_proc_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "14frjb004bb0ghss529r6sa0rrwycwzws2dvbq7z7d8m95xsp8d6")))

(define-public crate-piston2d_abstraction_proc_macros-0.1.1 (c (n "piston2d_abstraction_proc_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "14qaajciw60c5hni94wh7q9dflx8f8v4snjasykgh7zbvnlbl0z1")))

(define-public crate-piston2d_abstraction_proc_macros-0.1.2 (c (n "piston2d_abstraction_proc_macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "1lzlwkpci96rqczy8yd1z48k0lnyqhxi6zm020873wfn3w6zsh16")))

