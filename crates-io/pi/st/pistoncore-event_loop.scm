(define-module (crates-io pi st pistoncore-event_loop) #:use-module (crates-io))

(define-public crate-pistoncore-event_loop-0.0.0 (c (n "pistoncore-event_loop") (v "0.0.0") (d (list (d (n "pistoncore-current") (r "^0.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.1") (d #t) (k 0)))) (h "1hsbl0778b02l9ayw82vr5x7b133qgygni0d70jx8zjdfmx54iiy")))

(define-public crate-pistoncore-event_loop-0.0.1 (c (n "pistoncore-event_loop") (v "0.0.1") (d (list (d (n "pistoncore-current") (r "^0.0.1") (d #t) (k 0)) (d (n "time") (r "^0.1.1") (d #t) (k 0)))) (h "171jpl1602ljhkc893gr29qbgaqx9q96p3nycicrn1f6324s4xhg")))

(define-public crate-pistoncore-event_loop-0.0.2 (c (n "pistoncore-event_loop") (v "0.0.2") (d (list (d (n "pistoncore-current") (r "^0.0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.3") (d #t) (k 0)))) (h "0as865dxiyymbpzhx8l2y81br80844dhl394d7n9wjw7fkxjwqzh")))

(define-public crate-pistoncore-event_loop-0.0.3 (c (n "pistoncore-event_loop") (v "0.0.3") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)) (d (n "quack") (r "^0.0.1") (d #t) (k 0)))) (h "0ikr1qiqjlrq9sq1h7gr9i3niym2f9bbag4blgffrjs1a8jyn6jd")))

(define-public crate-pistoncore-event_loop-0.0.4 (c (n "pistoncore-event_loop") (v "0.0.4") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)) (d (n "quack") (r "^0.0.2") (d #t) (k 0)))) (h "15ck8q98rna74axz31dgjxdwax3lpsymvkckpxwg4c1b7fm37fi2")))

(define-public crate-pistoncore-event_loop-0.0.5 (c (n "pistoncore-event_loop") (v "0.0.5") (d (list (d (n "clock_ticks") (r "^0.0.2") (d #t) (k 0)) (d (n "quack") (r "^0.0.3") (d #t) (k 0)))) (h "0jhlxr93f2kcycvnn4jagrmnbz9lh652122klm4ii3z3ccvflxml")))

(define-public crate-pistoncore-event_loop-0.0.6 (c (n "pistoncore-event_loop") (v "0.0.6") (d (list (d (n "clock_ticks") (r "^0.0.2") (d #t) (k 0)) (d (n "quack") (r "^0.0.6") (d #t) (k 0)))) (h "1pzi3mdznkm8sxpminsj2yy0pdwq7awyp0wdp8cx26sg9f3rz2yf")))

(define-public crate-pistoncore-event_loop-0.0.7 (c (n "pistoncore-event_loop") (v "0.0.7") (d (list (d (n "clock_ticks") (r "^0.0.2") (d #t) (k 0)) (d (n "quack") (r "^0.0.6") (d #t) (k 0)))) (h "13krak6qwg5dsb8957lri8dl39g2an48qdwjyvqqprzfnkj0s8ax")))

(define-public crate-pistoncore-event_loop-0.0.10 (c (n "pistoncore-event_loop") (v "0.0.10") (d (list (d (n "clock_ticks") (r "^0.0.2") (d #t) (k 0)) (d (n "quack") (r "^0.0.10") (d #t) (k 0)))) (h "0cs0mmahd13lrvssg4v4kli8wx476nrikrkw44n5ny9c4syp24qs")))

(define-public crate-pistoncore-event_loop-0.0.11 (c (n "pistoncore-event_loop") (v "0.0.11") (d (list (d (n "clock_ticks") (r "^0.0.2") (d #t) (k 0)) (d (n "quack") (r "^0.0.12") (d #t) (k 0)))) (h "1gangv6blj2zr2cqwkksrbf9lbpl6q05xgkcah26k3priyzqg3lw")))

(define-public crate-pistoncore-event_loop-0.0.13 (c (n "pistoncore-event_loop") (v "0.0.13") (d (list (d (n "clock_ticks") (r "^0.0.2") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.12") (d #t) (k 0)) (d (n "quack") (r "^0.0.13") (d #t) (k 0)))) (h "1g1vxxx1cjmfkdyqs65xg9s5yibky3jskhrbn4032wzxl7f1syhs")))

(define-public crate-pistoncore-event_loop-0.0.17 (c (n "pistoncore-event_loop") (v "0.0.17") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.0.15") (d #t) (k 0)))) (h "0cwsg64lm4rikw3ss1h86rwgcrbdll7af5yva6irjs9qkhz3nsdw")))

(define-public crate-pistoncore-event_loop-0.0.18 (c (n "pistoncore-event_loop") (v "0.0.18") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.0") (d #t) (k 0)))) (h "0ljgm336b4phnlzymdhrdixalavxx5g2cnppk0y18d7h9jv35s1j")))

(define-public crate-pistoncore-event_loop-0.1.0 (c (n "pistoncore-event_loop") (v "0.1.0") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.0") (d #t) (k 0)))) (h "0cjabdlvr0r299a4var1lp6flwpgz1d0zql03p8a6n70qpjp3z0v")))

(define-public crate-pistoncore-event_loop-0.1.5 (c (n "pistoncore-event_loop") (v "0.1.5") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.0.3") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.0") (d #t) (k 0)))) (h "1aqnc0byf4k8p2ydmvqs41351gx591sbkg7ms32xg40iw6fn1xwa")))

(define-public crate-pistoncore-event_loop-0.1.6 (c (n "pistoncore-event_loop") (v "0.1.6") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.0") (d #t) (k 0)))) (h "0k2axl6ldjks93fw2757l9z7pb68294qss5h0mxbf4p8cy1ldxfk")))

(define-public crate-pistoncore-event_loop-0.2.0 (c (n "pistoncore-event_loop") (v "0.2.0") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.0") (d #t) (k 0)))) (h "0harvbpqimpp4vhbvkq81gzbfzj0hfllkm3jmv2zv63bvr18abb3")))

(define-public crate-pistoncore-event_loop-0.2.1 (c (n "pistoncore-event_loop") (v "0.2.1") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.5") (d #t) (k 0)))) (h "1bhqaqrz5c5dp1qy3fplg6z04p3n2vki73j98h0nbr9wj0926p0j")))

(define-public crate-pistoncore-event_loop-0.2.2 (c (n "pistoncore-event_loop") (v "0.2.2") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.1.6") (d #t) (k 0)))) (h "1jp3vr2h80zidwb3i907nxqfp8i93lxywj1w8a1vfpmwfcikmj2c")))

(define-public crate-pistoncore-event_loop-0.3.0 (c (n "pistoncore-event_loop") (v "0.3.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.2.0") (d #t) (k 0)))) (h "1n5kvzkwbrdhi4gnvs430nsdzy7s0696pixcv4zzwv1rm0x5lri7")))

(define-public crate-pistoncore-event_loop-0.4.0 (c (n "pistoncore-event_loop") (v "0.4.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.3.0") (d #t) (k 0)))) (h "0rlhjvb2q8x0320bqzl88grvai3zaxw10in8ls53dzyi05gkcqxb")))

(define-public crate-pistoncore-event_loop-0.5.0 (c (n "pistoncore-event_loop") (v "0.5.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.4.0") (d #t) (k 0)))) (h "0s36bzmpg7y2j11ygxf17276jihjlhfr4npwml6rzvsh7isj7b9x")))

(define-public crate-pistoncore-event_loop-0.6.0 (c (n "pistoncore-event_loop") (v "0.6.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.5.0") (d #t) (k 0)))) (h "1j3xh5l0qxmwrf95d89c923rp7fvh7lmqpx3q4045lf3fk81g16s")))

(define-public crate-pistoncore-event_loop-0.7.0 (c (n "pistoncore-event_loop") (v "0.7.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.6.0") (d #t) (k 0)))) (h "1v6ff0qlrp031naky8i843fx30wy6g8sln7q1h265jwqqa51gdcw")))

(define-public crate-pistoncore-event_loop-0.8.0 (c (n "pistoncore-event_loop") (v "0.8.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.7.0") (d #t) (k 0)))) (h "1h1n7vvqj5aq4pi38rs4g8gcn1a3brp85xm3aklhjnskcml4wgjn")))

(define-public crate-pistoncore-event_loop-0.9.0 (c (n "pistoncore-event_loop") (v "0.9.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.4.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.8.0") (d #t) (k 0)))) (h "15j60m9l6masciy4197vjj0bz928h3k548qzd5dcdr1m6kmkrjbb")))

(define-public crate-pistoncore-event_loop-0.10.0 (c (n "pistoncore-event_loop") (v "0.10.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.5.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.9.0") (d #t) (k 0)))) (h "1vk6qmlxip339pa394362r7ijkj1nvh5clfxffr17z9hj695bjxr")))

(define-public crate-pistoncore-event_loop-0.10.1 (c (n "pistoncore-event_loop") (v "0.10.1") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.1.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.5.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.9.1") (d #t) (k 0)))) (h "0rak0ynryj3j0k65fgvpv0x4i0rdybzwnqap8z4jnp68kcdbdmi4")))

(define-public crate-pistoncore-event_loop-0.11.0 (c (n "pistoncore-event_loop") (v "0.11.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.6.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.10.0") (d #t) (k 0)))) (h "0mm5hr1llp9i5vh4fgwbrmfcwj6485kiqlqy8mwvppq5a7imp0yg")))

(define-public crate-pistoncore-event_loop-0.12.0 (c (n "pistoncore-event_loop") (v "0.12.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.7.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.11.0") (d #t) (k 0)))) (h "1zwjn7hzb28sgsfgmznqfnw8y6p501mb0i79dx45s59cfjk64hxv")))

(define-public crate-pistoncore-event_loop-0.13.0 (c (n "pistoncore-event_loop") (v "0.13.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.12.0") (d #t) (k 0)))) (h "1yglq3wiz7smslllm1007g9qvv4vnwnam2hrcra0wn7xblnqw2i2")))

(define-public crate-pistoncore-event_loop-0.13.1 (c (n "pistoncore-event_loop") (v "0.13.1") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 0)) (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.12.0") (d #t) (k 0)))) (h "0f6iqf0vricnijmj81xr0gbisx5imiyj04x7wpsa9n6pm1aqklcq")))

(define-public crate-pistoncore-event_loop-0.14.0 (c (n "pistoncore-event_loop") (v "0.14.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1cknk88rjqfx5mxllwvrqgq2p2c1inxw9k55sg8l49bd0lcd1yq7")))

(define-public crate-pistoncore-event_loop-0.15.0 (c (n "pistoncore-event_loop") (v "0.15.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.13.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "0922ms1kix9vvrp73fgnx7zv0fzlw5d6k3rsjvbmphvjyhd86z5h")))

(define-public crate-pistoncore-event_loop-0.16.0 (c (n "pistoncore-event_loop") (v "0.16.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.14.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1mmqcqqvacg4m9j1zhk3gl6h6mxdcimipbagkai6ghdmihf2z5nk")))

(define-public crate-pistoncore-event_loop-0.17.0 (c (n "pistoncore-event_loop") (v "0.17.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.14.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "05hf4nqkj1s6wc5zm3gydcwd35myj2lq2n2pbncgfh2imc8gz2qb")))

(define-public crate-pistoncore-event_loop-0.18.0 (c (n "pistoncore-event_loop") (v "0.18.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.8.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.15.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1mlda2nglj48z1q7a7i8q9y9f5q515hcix1n800zr8r7ai3i6809")))

(define-public crate-pistoncore-event_loop-0.19.0 (c (n "pistoncore-event_loop") (v "0.19.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.9.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.16.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1fid0sbwbd71acd7i55na162x780hf4c4xp7giyhcbs86aisf8c9")))

(define-public crate-pistoncore-event_loop-0.20.0 (c (n "pistoncore-event_loop") (v "0.20.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.10.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.17.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1vvaydhwi4sqgdmcajcf0zl7k18cvxaiw9fdl42slnrmgmy177nb")))

(define-public crate-pistoncore-event_loop-0.20.1 (c (n "pistoncore-event_loop") (v "0.20.1") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.10.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.17.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "0h3fbik9igpy2dksxmr54ixfrrc7wz1mzjnyrll18blff1wh3ah0")))

(define-public crate-pistoncore-event_loop-0.21.0 (c (n "pistoncore-event_loop") (v "0.21.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.11.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.18.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "0dyxsz1rnp0lmj3wr7rr8fggkkbqj7nljxx9vqs73aqvvfwd6bqs")))

(define-public crate-pistoncore-event_loop-0.22.0 (c (n "pistoncore-event_loop") (v "0.22.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.12.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.19.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1xlcvnpn856ijwspbyizmdhmz83cm8kwb439anxbhkz7q960zi35")))

(define-public crate-pistoncore-event_loop-0.22.1 (c (n "pistoncore-event_loop") (v "0.22.1") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.12.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.19.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1lmrrmv16hrqii60k0nshg0a7xcaznar3shm5f6vfxc0sf2g4sc2")))

(define-public crate-pistoncore-event_loop-0.23.0 (c (n "pistoncore-event_loop") (v "0.23.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.12.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.20.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "0xny3vmrhsczfb94apry0vp7x77jw48mp3vwkpdap17bwsb3vi37")))

(define-public crate-pistoncore-event_loop-0.24.0 (c (n "pistoncore-event_loop") (v "0.24.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.13.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.21.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "0hrjyfhf1dv6xbczsyzz0h8i3v5nws9c90rfs8czl8nbnsn6z4vl")))

(define-public crate-pistoncore-event_loop-0.25.0 (c (n "pistoncore-event_loop") (v "0.25.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.14.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.22.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "0h95jj7lgpcbsjv3bcqkf2ck8abwhw9xbd8z4y5w0qjadywmc9bn")))

(define-public crate-pistoncore-event_loop-0.25.1 (c (n "pistoncore-event_loop") (v "0.25.1") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.14.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.22.1") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "08wv4kxcr74c0idwzwwi15kl0md3f2qggs3lhczi4vhmc3q6g52c")))

(define-public crate-pistoncore-event_loop-0.26.0 (c (n "pistoncore-event_loop") (v "0.26.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.14.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.23.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "04r2dvxiidffgmw7yd7nzwaj23zma902nvw1f111x24zl1g847z8")))

(define-public crate-pistoncore-event_loop-0.27.0 (c (n "pistoncore-event_loop") (v "0.27.0") (d (list (d (n "piston-viewport") (r "^0.2.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.15.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.24.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1s9gp9d51y7w51845np4a2qg33kyvcqi7vmz03n18898zi1ig2rz")))

(define-public crate-pistoncore-event_loop-0.28.0 (c (n "pistoncore-event_loop") (v "0.28.0") (d (list (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.16.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.25.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1wk79mcadcxmhyy91j3nnm5p5ss3xqai0fn2kvm0vm92i6z97csr")))

(define-public crate-pistoncore-event_loop-0.29.0 (c (n "pistoncore-event_loop") (v "0.29.0") (d (list (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.16.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.25.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1famihn918mxxqdca242lmafykr6b3847v0qr6avxv0np1hg3k67")))

(define-public crate-pistoncore-event_loop-0.30.0 (c (n "pistoncore-event_loop") (v "0.30.0") (d (list (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.26.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1fk6zb38bw6qxhcbdlqqzs5mxj7r25x0w16x3m03k5qz8vmvxl62")))

(define-public crate-pistoncore-event_loop-0.31.0 (c (n "pistoncore-event_loop") (v "0.31.0") (d (list (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.26.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1prwj00ah5zpb4dqcqgq9b7lbx2q5kcljpl1hcyaih7wad518kni")))

(define-public crate-pistoncore-event_loop-0.31.1 (c (n "pistoncore-event_loop") (v "0.31.1") (d (list (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.26.0") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1jdsgcnkn88g4mrzy0vad0ddpa32gjxxwsjfbskklbf7sdghbvls")))

(define-public crate-pistoncore-event_loop-0.31.2 (c (n "pistoncore-event_loop") (v "0.31.2") (d (list (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.26.1") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1745r6svnwm17af8kd8srdqkaqcrir6qckgpx5y74kz2gzpybg7p")))

(define-public crate-pistoncore-event_loop-0.31.3 (c (n "pistoncore-event_loop") (v "0.31.3") (d (list (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.26.2") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1ddiv7mr2b3ss0j86fwj0h4zr83i7rdnrr5rsjm0fmwjlzfwqx6w")))

(define-public crate-pistoncore-event_loop-0.31.4 (c (n "pistoncore-event_loop") (v "0.31.4") (d (list (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.17.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.26.2") (d #t) (k 0)))) (h "0hckp12pr7id74zmszar246hwngrf6g6q3qkcfy22zc4bziqvqr9")))

(define-public crate-pistoncore-event_loop-0.32.0 (c (n "pistoncore-event_loop") (v "0.32.0") (d (list (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.18.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.27.0") (d #t) (k 0)))) (h "1rbq0zy8bmg53i57dxxpf0bzqxi26yf396aa6q1vb8vla97nja9f")))

(define-public crate-pistoncore-event_loop-0.33.0 (c (n "pistoncore-event_loop") (v "0.33.0") (d (list (d (n "piston-viewport") (r "^0.3.0") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.19.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.28.0") (d #t) (k 0)))) (h "0g18rgg5wr978mfj9x64abgw3a4dam2ap5glpzwi8hds77kw646l")))

(define-public crate-pistoncore-event_loop-0.34.0 (c (n "pistoncore-event_loop") (v "0.34.0") (d (list (d (n "pistoncore-input") (r "^0.19.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.29.0") (d #t) (k 0)))) (h "1dcivki3qah75j43yyp34a7qkhhg4c3d8h1aq3bvgfx11bx5a85y")))

(define-public crate-pistoncore-event_loop-0.35.0 (c (n "pistoncore-event_loop") (v "0.35.0") (d (list (d (n "pistoncore-input") (r "^0.20.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.30.0") (d #t) (k 0)))) (h "1w7ygbdi4xn6rlc8041siqsrva7w064v3j9aqhqg1w06vapch63h")))

(define-public crate-pistoncore-event_loop-0.36.0 (c (n "pistoncore-event_loop") (v "0.36.0") (d (list (d (n "pistoncore-input") (r "^0.20.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.31.0") (d #t) (k 0)))) (h "0ldl4240v7v5yd7vra2fjbszxyj0awck622pnmbqj2x932j3qbpa")))

(define-public crate-pistoncore-event_loop-0.37.0 (c (n "pistoncore-event_loop") (v "0.37.0") (d (list (d (n "pistoncore-input") (r "^0.21.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.32.0") (d #t) (k 0)))) (h "1dn3sqfqzyyir9igl19m71vdpmlpygbjw6wq07z0g1kk6s3yn6pl")))

(define-public crate-pistoncore-event_loop-0.38.0 (c (n "pistoncore-event_loop") (v "0.38.0") (d (list (d (n "pistoncore-input") (r "^0.22.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.33.0") (d #t) (k 0)))) (h "01rgimc4hbq58yhyqpg02c0cib0jhwifayz1zllj0s7clkkgrdvy")))

(define-public crate-pistoncore-event_loop-0.39.0 (c (n "pistoncore-event_loop") (v "0.39.0") (d (list (d (n "pistoncore-input") (r "^0.23.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.34.0") (d #t) (k 0)))) (h "0h4z1qzk506kmd5b2d32xa48nhy0sni6nf1m9m9v91yi67a9nf01")))

(define-public crate-pistoncore-event_loop-0.40.0 (c (n "pistoncore-event_loop") (v "0.40.0") (d (list (d (n "pistoncore-input") (r "^0.23.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.35.0") (d #t) (k 0)))) (h "028907cvx484s1bi739z2i4mxn7b312yiar4z3y7z62vhzw02lbl")))

(define-public crate-pistoncore-event_loop-0.41.0 (c (n "pistoncore-event_loop") (v "0.41.0") (d (list (d (n "pistoncore-input") (r "^0.24.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.36.0") (d #t) (k 0)))) (h "0rziy23zb1yrqks094bpbqrannn6rnnmgvh0fkia2zr56imh8icc")))

(define-public crate-pistoncore-event_loop-0.42.0 (c (n "pistoncore-event_loop") (v "0.42.0") (d (list (d (n "pistoncore-input") (r "^0.24.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.37.0") (d #t) (k 0)))) (h "06l4sfg4n178kzbzi336mcbdrgqy7nddrpxdghp07bi5rnc47yrf")))

(define-public crate-pistoncore-event_loop-0.43.0 (c (n "pistoncore-event_loop") (v "0.43.0") (d (list (d (n "pistoncore-input") (r "^0.25.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.38.0") (d #t) (k 0)))) (h "1sjxk911n9p0f2vzyjvns3p84s80nwj53q2drcq3qk7ydxqxfxaa")))

(define-public crate-pistoncore-event_loop-0.44.0 (c (n "pistoncore-event_loop") (v "0.44.0") (d (list (d (n "pistoncore-input") (r "^0.25.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.39.0") (d #t) (k 0)))) (h "079sy4yw9lsf77cp448ps472r96cmli7yb50l9m721cnqv20r805")))

(define-public crate-pistoncore-event_loop-0.45.0 (c (n "pistoncore-event_loop") (v "0.45.0") (d (list (d (n "pistoncore-input") (r "^0.25.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.40.0") (d #t) (k 0)))) (h "0jpvg804rgwh9a3h2zwfjhr0a6wq6q8avacvb04ygp68gxq7sagp")))

(define-public crate-pistoncore-event_loop-0.46.0 (c (n "pistoncore-event_loop") (v "0.46.0") (d (list (d (n "pistoncore-input") (r "^0.26.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.41.0") (d #t) (k 0)))) (h "07bl7xxf96cjk8ksksv7sq9xx1j76qmiq0qy7xc0ib6dnndmgnbb")))

(define-public crate-pistoncore-event_loop-0.47.0 (c (n "pistoncore-event_loop") (v "0.47.0") (d (list (d (n "pistoncore-input") (r "^0.27.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.42.0") (d #t) (k 0)))) (h "1b8m1r0nqm1s1hf0k36b7gy9wvvwi1zz517yknrjqhxxhhnks5lj")))

(define-public crate-pistoncore-event_loop-0.48.0 (c (n "pistoncore-event_loop") (v "0.48.0") (d (list (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.43.0") (d #t) (k 0)))) (h "14fas1q2k2cvsiggpy6nly7xpa37qi2k1ziwpxbw7yvyzzzb0spc")))

(define-public crate-pistoncore-event_loop-0.49.0 (c (n "pistoncore-event_loop") (v "0.49.0") (d (list (d (n "pistoncore-input") (r "^0.28.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.44.0") (d #t) (k 0)))) (h "1h9ij9vx42xg39198yxdlpk842pli5jqm2kwswiv3bqqcji0fwsm")))

(define-public crate-pistoncore-event_loop-0.50.0 (c (n "pistoncore-event_loop") (v "0.50.0") (d (list (d (n "pistoncore-input") (r "^0.28.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.45.0") (d #t) (k 0)))) (h "0i7629jf89bcbgzqv09jbh4k1yv0kamaswq8xdgqndhdnz7bd2na")))

(define-public crate-pistoncore-event_loop-0.51.0 (c (n "pistoncore-event_loop") (v "0.51.0") (d (list (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.46.0") (d #t) (k 0)))) (h "1gk1nd2sz1zknrv0sd3fpz7xwmdx2h6cblyjz34mpkpzaiwwrp4p")))

(define-public crate-pistoncore-event_loop-0.52.0 (c (n "pistoncore-event_loop") (v "0.52.0") (d (list (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.47.0") (d #t) (k 0)))) (h "00ivg1l4sz12v0vaf2yvnly8mrhls7r521haz8znf75xn73w3by1")))

(define-public crate-pistoncore-event_loop-0.53.0 (c (n "pistoncore-event_loop") (v "0.53.0") (d (list (d (n "pistoncore-input") (r "^1.0.0") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.47.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)))) (h "17kf2chq2y9v9pf4fap18c95rjxj959zssn07j46yz0xs2p5avcj")))

(define-public crate-pistoncore-event_loop-0.53.1 (c (n "pistoncore-event_loop") (v "0.53.1") (d (list (d (n "pistoncore-input") (r "^1.0.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.47.1") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)))) (h "1rkpxfp0ch37jg73j1cix6haqsbpwcaqcd4l6sr0nhqjy0xnpn21")))

(define-public crate-pistoncore-event_loop-0.54.0 (c (n "pistoncore-event_loop") (v "0.54.0") (d (list (d (n "pistoncore-input") (r "^1.0.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^1.0.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)))) (h "12rwc4sf432q3ipn3012vgpydb6hxf3kaaarrc6fnw7xvy6rjzsp")))

(define-public crate-pistoncore-event_loop-0.55.0 (c (n "pistoncore-event_loop") (v "0.55.0") (d (list (d (n "pistoncore-input") (r "^1.0.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^1.0.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "0pz2x5yhmgg3y8y8nj60zviwch105q499pw1p61n8zn76k2mxp21") (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-pistoncore-event_loop-1.0.0 (c (n "pistoncore-event_loop") (v "1.0.0") (d (list (d (n "pistoncore-input") (r "^1.0.1") (d #t) (k 0)) (d (n "pistoncore-window") (r "^1.0.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "0lndvx17rn9m09n2scn8fd0kf9ck5za18wlvs3nrch852s6513m2") (s 2) (e (quote (("async" "dep:tokio"))))))

