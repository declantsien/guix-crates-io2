(define-module (crates-io pi st piston-viewport) #:use-module (crates-io))

(define-public crate-piston-viewport-0.0.0 (c (n "piston-viewport") (v "0.0.0") (h "19g5yfiwfz9r2v7r4s11kwnqvpclh72z0hyqj4h298ki8ygk4hwm")))

(define-public crate-piston-viewport-0.0.1 (c (n "piston-viewport") (v "0.0.1") (h "1r7r469q7crcjgijan1526zdymc95r8lwpzs9mj3y61vzvc66zzg")))

(define-public crate-piston-viewport-0.0.2 (c (n "piston-viewport") (v "0.0.2") (d (list (d (n "piston-float") (r "^0.0.1") (d #t) (k 0)))) (h "145vr60z7624krkr2hcybavbv7c1wyscc1cyy6s9hcillbgm1y71")))

(define-public crate-piston-viewport-0.0.3 (c (n "piston-viewport") (v "0.0.3") (d (list (d (n "piston-float") (r "^0.0.2") (d #t) (k 0)))) (h "0yd8npxy4dna63wkzanx816lz9b7ay0gwkcyd1v6h05z6l6ql4lz")))

(define-public crate-piston-viewport-0.1.0 (c (n "piston-viewport") (v "0.1.0") (d (list (d (n "piston-float") (r "^0.1.0") (d #t) (k 0)))) (h "01pfndra5i6dgjn75k40kvkzd8mh1lb505vqjc40g0x2lzzrqr7n")))

(define-public crate-piston-viewport-0.2.0 (c (n "piston-viewport") (v "0.2.0") (d (list (d (n "piston-float") (r "^0.2.0") (d #t) (k 0)))) (h "1rx4sclpsljbi9zb92ig0v2362np220r3ksagcwslawh7wq58qld")))

(define-public crate-piston-viewport-0.3.0 (c (n "piston-viewport") (v "0.3.0") (d (list (d (n "piston-float") (r "^0.3.0") (d #t) (k 0)))) (h "0hv3v28pkp2rraf7322zv2i47w03qc1nr23dr42cd7gx72l4hmcw")))

(define-public crate-piston-viewport-0.4.0 (c (n "piston-viewport") (v "0.4.0") (d (list (d (n "piston-float") (r "^0.3.0") (d #t) (k 0)))) (h "04cgg7y6fn1bsrzgbnj7v71fn462fk2781qx88czzzjvxhvfyk4g")))

(define-public crate-piston-viewport-0.5.0 (c (n "piston-viewport") (v "0.5.0") (d (list (d (n "piston-float") (r "^0.3.0") (d #t) (k 0)))) (h "049n07r87ww24mvkzcp0w7k6z7d3zp08wrnsgkaydavxbycxv5kx")))

(define-public crate-piston-viewport-1.0.0 (c (n "piston-viewport") (v "1.0.0") (d (list (d (n "piston-float") (r "^1.0.0") (d #t) (k 0)))) (h "16378hcy41b7x3zj2z4har0wq6fl4r62kf9p106jjl8hg2dv3aq1")))

(define-public crate-piston-viewport-1.0.1 (c (n "piston-viewport") (v "1.0.1") (d (list (d (n "piston-float") (r "^1.0.0") (d #t) (k 0)))) (h "12l9lyij7hp8vqhwrfnvb13hnnxszs8m38x6mqpd8c4v4pa3ik8j")))

(define-public crate-piston-viewport-1.0.2 (c (n "piston-viewport") (v "1.0.2") (d (list (d (n "piston-float") (r "^1.0.0") (d #t) (k 0)))) (h "00f0i8xqv1als60bhg7z62ac08cncm3jnrnqpg6xj7fpw25azv31")))

