(define-module (crates-io pi st pistoncore-current) #:use-module (crates-io))

(define-public crate-pistoncore-current-0.0.0 (c (n "pistoncore-current") (v "0.0.0") (h "1n96j610dxl0668h3c2x2ic6w8d0jfpf5q9vcqpj9w3cwj8am7a4")))

(define-public crate-pistoncore-current-0.0.1 (c (n "pistoncore-current") (v "0.0.1") (h "0sxc9sn3fj9sqbcpbkxj64p3kwi83mg9fgxp529k0xcbp5gzna34")))

(define-public crate-pistoncore-current-0.0.2 (c (n "pistoncore-current") (v "0.0.2") (h "0iw1q7gzxdxw234psq67l2kxzmf6dcyabl22b2lzxh12l75b1ym6")))

(define-public crate-pistoncore-current-0.0.3 (c (n "pistoncore-current") (v "0.0.3") (h "1frvqynwzsiiwxly41bci2q4zvknvpg5b4xgdhs2wihfwf4nlq64")))

