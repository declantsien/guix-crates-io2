(define-module (crates-io pi st piston_rs) #:use-module (crates-io))

(define-public crate-piston_rs-0.1.0 (c (n "piston_rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0ph0hf4kxs0caiy71pnkzacsf8d4z6zf508xzrd2d5580q2ml03z")))

(define-public crate-piston_rs-0.2.0 (c (n "piston_rs") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0az9ay9kg6mpgjigr5i7h72jia2lkgdhcnyz2sshk0lm1hnigm4p")))

(define-public crate-piston_rs-0.2.1 (c (n "piston_rs") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0kamhgbdm9iach2xflgyvzcsrfjfn3ffhrz0zdxpyfg71p01r8d2")))

(define-public crate-piston_rs-0.2.2 (c (n "piston_rs") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "00y7jhj6dyfhxhm1rp6sirj25hgyknc3mg84gg8bhv9jwh86cqp5")))

(define-public crate-piston_rs-0.2.3 (c (n "piston_rs") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "07rkg82a0i095x5kgdia2wmgypiy8zk5jhv3dqbzc92746642kp6")))

(define-public crate-piston_rs-0.3.0 (c (n "piston_rs") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "03lizfyybp07gcpps77afp8vkpq8nn6bsarv082fynkx3ciq1im1")))

(define-public crate-piston_rs-0.4.0 (c (n "piston_rs") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "14k1ay5faaf1z815lx79wqdlz7m9gjns7932s1syicq9vd3xj74v")))

(define-public crate-piston_rs-0.4.1 (c (n "piston_rs") (v "0.4.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "15gxg46gkv823cbwkw935ca31gsxsicsppfn5a2kvkgaqdddxz98")))

(define-public crate-piston_rs-0.4.2 (c (n "piston_rs") (v "0.4.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1sj3maaqxjqbmizbs7483p5v0b9dzq064zdk9zf0m74j5p0qczrk")))

(define-public crate-piston_rs-0.4.3 (c (n "piston_rs") (v "0.4.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0yqj8fw68pb00da199nm57lbmxyk6663mgsxmx7878gf9xwl8shm")))

