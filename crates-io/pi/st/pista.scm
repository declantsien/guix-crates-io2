(define-module (crates-io pi st pista) #:use-module (crates-io))

(define-public crate-pista-0.1.0 (c (n "pista") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.55") (d #t) (k 0)) (d (n "tico") (r "^1.0.0") (d #t) (k 0)))) (h "15cxma10w519w0fg2ha9aqfwhwy5kr1mhj87xrr3gs84fmjx6r7f")))

(define-public crate-pista-0.1.1 (c (n "pista") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.55") (d #t) (k 0)) (d (n "tico") (r "^1.0.0") (d #t) (k 0)))) (h "0lkm5pf50qxskqxvrnjx0vq1q17nw7sc6k3y6pjjcppj1193a5nc")))

(define-public crate-pista-0.1.2 (c (n "pista") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.55") (d #t) (k 0)) (d (n "tico") (r "^1.0.0") (d #t) (k 0)))) (h "0b4yfhn43lscqhrlalwy1vrbwga8gwi08g9n77g7pb14sn0bzq6h")))

(define-public crate-pista-0.1.3 (c (n "pista") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.55") (d #t) (k 0)) (d (n "tico") (r "^1.0.0") (d #t) (k 0)))) (h "12gl142b25c4wf3pj1ndg0jb69my6b4bghq98lc66srrfggkgzkx")))

(define-public crate-pista-0.1.5 (c (n "pista") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.55") (d #t) (k 0)) (d (n "tico") (r "^1.0.0") (d #t) (k 0)))) (h "0f1d8xz8ar7ph05zgp5camgxp8a7h06h475ld9klwyxg2rhrwfjx")))

