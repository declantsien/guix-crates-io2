(define-module (crates-io pi st piston-music) #:use-module (crates-io))

(define-public crate-piston-music-0.1.0 (c (n "piston-music") (v "0.1.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.18.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.8.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.7.0") (d #t) (k 0)))) (h "1kbd41hpyii9xp6wvk4336p9g79bzhpw59z0ybbnqk5xwzpvgz5r")))

(define-public crate-piston-music-0.1.1 (c (n "piston-music") (v "0.1.1") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.18.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.8.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.7.0") (d #t) (k 0)))) (h "0ass26d1ncpgq08r5v8db9i3cg04apaj4lx7a0bzsmsrbmw6vdw1")))

(define-public crate-piston-music-0.2.0 (c (n "piston-music") (v "0.2.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.20.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.9.1") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.7.0") (d #t) (k 0)))) (h "1n6sv1a2yi7nvpd1x6l5w6wa187fm6vwg69hfrkdbv4ra1xqhnli")))

(define-public crate-piston-music-0.3.0 (c (n "piston-music") (v "0.3.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.10.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.8.0") (d #t) (k 0)))) (h "11lxhnz49hjwr45qbbj2z5vpn4cpkwrjw5s8h0k4ffarw44mp3rq")))

(define-public crate-piston-music-0.4.0 (c (n "piston-music") (v "0.4.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.31.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.11.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.9.0") (d #t) (k 0)))) (h "068srgfc089gmj8xpadk9dyjml3z5kqrjn8annydd943ji7p8hv8")))

(define-public crate-piston-music-0.5.0 (c (n "piston-music") (v "0.5.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.32.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.12.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.10.0") (d #t) (k 0)))) (h "12zmjl7gfzc7kjqk2cpc477kqbllh75nhspygl0zbk160zcb0mvb")))

(define-public crate-piston-music-0.6.0 (c (n "piston-music") (v "0.6.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.34.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.13.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.12.0") (d #t) (k 0)))) (h "1g60wdlzz6jy0mfrlm0mh3xyri0fyx3q3ksiqvr9ip9ympm1jzff")))

(define-public crate-piston-music-0.7.0 (c (n "piston-music") (v "0.7.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.35.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.14.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.13.0") (d #t) (k 0)))) (h "1lam6w3n5zcyfki3dw4wp3y45jpl95b5wawjl71xz2whns2vy3np")))

(define-public crate-piston-music-0.8.0 (c (n "piston-music") (v "0.8.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.41.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.15.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.15.0") (d #t) (k 0)))) (h "0r7fhrhakciy55nj54c2k7xxkrzhp9cy2ihkpy315zy8sb2pgwzw")))

(define-public crate-piston-music-0.9.0 (c (n "piston-music") (v "0.9.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.43.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.18.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.17.0") (d #t) (k 0)))) (h "0yi3m6kc3079hdf6vlz7b2yypwg7m332rj3z0cbyfx9qf0brc205")))

(define-public crate-piston-music-0.10.0 (c (n "piston-music") (v "0.10.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.47.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.19.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.18.0") (d #t) (k 0)))) (h "0prj9dd6ix8jkn90m7d73sx442v5wisj8mfwdnn22kwp9symxzyd")))

(define-public crate-piston-music-0.11.0 (c (n "piston-music") (v "0.11.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.50.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.21.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.21.0") (d #t) (k 0)))) (h "0r07ggzbmzlqpim3vx744bmzjzdmbrkg4jjgc95f7yj6ashq689a")))

(define-public crate-piston-music-0.12.0 (c (n "piston-music") (v "0.12.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.53.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.22.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.22.0") (d #t) (k 0)))) (h "0dj0r6ikl5pxx56f3c5c0f6iz56agcz9qlldq93rma9n2w6991f7")))

(define-public crate-piston-music-0.13.0 (c (n "piston-music") (v "0.13.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.53.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.22.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.22.0") (d #t) (k 0)))) (h "0mpj8klydz8qj4kz3aw2j1wfrhya20qg25272k96c9p61kki8dis")))

(define-public crate-piston-music-0.14.0 (c (n "piston-music") (v "0.14.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.53.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.23.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.23.0") (d #t) (k 0)))) (h "1d5iqhc6qjf48ij9hc50pgs9yjaig7pcpy7bakn12p92al4zk7xp")))

(define-public crate-piston-music-0.15.0 (c (n "piston-music") (v "0.15.0") (d (list (d (n "current") (r "^0.1.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.53.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.23.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.23.0") (d #t) (k 0)))) (h "1jc5inhpigixh5qwsz9g8nhkf87bmy5szzxyqgi30za5lx7cds52")))

(define-public crate-piston-music-0.16.0 (c (n "piston-music") (v "0.16.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.54.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.24.0") (d #t) (k 0)) (d (n "sdl2_mixer") (r "^0.24.0") (d #t) (k 0)))) (h "07pvzdzh6lx8yl5c89jrjfnm3993ky1f18jwvvcbw68pk6g2k2yc")))

(define-public crate-piston-music-0.17.0 (c (n "piston-music") (v "0.17.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.60.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.27.2") (f (quote ("mixer"))) (k 0)))) (h "0n5wsc02b0xw7favh3kbnd31bvl15sf7zm8pa3yp78kxl5xm988x")))

(define-public crate-piston-music-0.18.0 (c (n "piston-music") (v "0.18.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.62.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.28.0") (f (quote ("mixer"))) (k 0)))) (h "1vskmfb0b79ivd4p52wyaxrshzbvirmz6j873bynsgqv2gvjpb5l")))

(define-public crate-piston-music-0.19.0 (c (n "piston-music") (v "0.19.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.64.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.29.0") (f (quote ("mixer"))) (k 0)))) (h "11wxcsv7138ik8mc7ykpqz1dx7rv758yj657fl8r0qs2d4dm5dkr")))

(define-public crate-piston-music-0.20.0 (c (n "piston-music") (v "0.20.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.66.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.30.0") (f (quote ("mixer"))) (k 0)))) (h "123q5fb2j6mdwr6234rvj60b8xgpa53p9xq5gy6fiwn515628mmm")))

(define-public crate-piston-music-0.21.0 (c (n "piston-music") (v "0.21.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.66.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.30.0") (f (quote ("mixer"))) (k 0)))) (h "0fnd1lfb3mgmjcq497crnydfldk3yzi07x6s5s384gxrrrkzd8hd")))

(define-public crate-piston-music-0.23.0 (c (n "piston-music") (v "0.23.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.69.1") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.43.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.30.0") (f (quote ("mixer"))) (k 0)))) (h "0nf69k2jzdnwrignnsjwqy913588sb3p0m34syv6sms0s567f9nd")))

(define-public crate-piston-music-0.24.0 (c (n "piston-music") (v "0.24.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.74.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.47.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31.0") (f (quote ("mixer"))) (k 0)))) (h "1ap7gbpx76fbx884fa1vzn4sdxfc8k40k6navk866968n96jxa62")))

(define-public crate-piston-music-0.25.0 (c (n "piston-music") (v "0.25.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.86.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.53.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.1") (f (quote ("mixer"))) (k 0)))) (h "1ks9bhxi743wiccvfyyljc24l5i2lgy74yndzsw5k2qdi7rkd4vk")))

(define-public crate-piston-music-0.25.1 (c (n "piston-music") (v "0.25.1") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.86.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.53.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.1") (f (quote ("mixer"))) (k 0)))) (h "1b15rv32hjbw2v538hc71805cqlzlw60lb87qj3zvb1kyiw6vkhg")))

(define-public crate-piston-music-0.26.0 (c (n "piston-music") (v "0.26.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.110.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.65.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.0") (f (quote ("mixer"))) (k 0)))) (h "0iphhwlcrmhlklg2yfaw2hxfgrpirrhhyg10njbhabw9872wgw3d")))

