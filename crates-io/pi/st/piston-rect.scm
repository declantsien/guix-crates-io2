(define-module (crates-io pi st piston-rect) #:use-module (crates-io))

(define-public crate-piston-rect-0.1.0 (c (n "piston-rect") (v "0.1.0") (d (list (d (n "piston-float") (r "^0.2.0") (d #t) (k 0)))) (h "1a3jmjn2fbky2w7j1bbagb88psa8403xm3wpa6z8jjfg68ly2abg")))

(define-public crate-piston-rect-0.2.0 (c (n "piston-rect") (v "0.2.0") (d (list (d (n "piston-float") (r "^0.3.0") (d #t) (k 0)))) (h "10qsran415prfnsvqr919dd27bv58mpm1hdbpwv4wypavxzhqcry")))

(define-public crate-piston-rect-0.3.0 (c (n "piston-rect") (v "0.3.0") (d (list (d (n "piston-float") (r "^1.0.0") (d #t) (k 0)))) (h "11ja8fjhsvcw25lxhqyf5zm0np2pgf17cb590skdp1s62ir5g8i5")))

