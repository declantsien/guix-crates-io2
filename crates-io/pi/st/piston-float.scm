(define-module (crates-io pi st piston-float) #:use-module (crates-io))

(define-public crate-piston-float-0.0.0 (c (n "piston-float") (v "0.0.0") (h "0q3bga150c3z1s56dpd9wfn9fhjx3rbd1rcivjyaz82bli76v7g8")))

(define-public crate-piston-float-0.0.1 (c (n "piston-float") (v "0.0.1") (h "14mgkkmvx1q12pwkqn198n7lqdrpzbd92q2cbc3w4x3kq4bbjh18")))

(define-public crate-piston-float-0.0.2 (c (n "piston-float") (v "0.0.2") (h "1bshq7kp3mj3xldif7913lg7k3nbax6kh1a5q3f6ah4k8smyiarx")))

(define-public crate-piston-float-0.1.0 (c (n "piston-float") (v "0.1.0") (h "0bri7sbgi48sscl4srlkihpj7r9gn0m6a1k4k5xj36y9fk6a8zry")))

(define-public crate-piston-float-0.2.0 (c (n "piston-float") (v "0.2.0") (h "1lf3cjwwdzaidiqxmvmysj5gr0ih3vi7b281r89rq1lbrzqp450r")))

(define-public crate-piston-float-0.3.0 (c (n "piston-float") (v "0.3.0") (h "169i7rhi986m2s5x7v993cn1k1rin15jwlb66bvbrm7g82kc6n5h")))

(define-public crate-piston-float-1.0.0 (c (n "piston-float") (v "1.0.0") (h "0r35aasycms79hf2vf1ap40kkp8ywgl4hmfkf762dq8jwd3vw07r")))

(define-public crate-piston-float-1.0.1 (c (n "piston-float") (v "1.0.1") (h "142m26f610d7xk5znrk8gw5mkiqgia9lzf4j1jaqy3pqvi1vyy5d")))

