(define-module (crates-io pi st piston_meta_search) #:use-module (crates-io))

(define-public crate-piston_meta_search-0.1.0 (c (n "piston_meta_search") (v "0.1.0") (d (list (d (n "piston_meta") (r "^0.11.0") (d #t) (k 0)) (d (n "range") (r "^0.1.1") (d #t) (k 0)))) (h "0i2fgsv4cg8hwhm3wj29r8r45sdps1bfh451y0kas8ca8n99jk5p")))

(define-public crate-piston_meta_search-0.2.0 (c (n "piston_meta_search") (v "0.2.0") (d (list (d (n "piston_meta") (r "^0.12.0") (d #t) (k 0)) (d (n "range") (r "^0.1.1") (d #t) (k 0)))) (h "0aglx6by7jl4pa0kxns79wbjfg7bdfp1jhyrjplz78fa2wa6cr9x")))

(define-public crate-piston_meta_search-0.3.0 (c (n "piston_meta_search") (v "0.3.0") (d (list (d (n "piston_meta") (r "^0.13.0") (d #t) (k 0)) (d (n "range") (r "^0.2.0") (d #t) (k 0)))) (h "16mn7pgqzfssp44660lmbha5jyyxy9h8xqnnkj3s1sr00rxgw97p")))

(define-public crate-piston_meta_search-0.4.0 (c (n "piston_meta_search") (v "0.4.0") (d (list (d (n "piston_meta") (r "^0.14.0") (d #t) (k 0)) (d (n "range") (r "^0.2.0") (d #t) (k 0)))) (h "1pg4xr02kffbkncy2jp0pf3hg8vz4y128yfs90apm38181y23fcq")))

(define-public crate-piston_meta_search-0.5.0 (c (n "piston_meta_search") (v "0.5.0") (d (list (d (n "piston_meta") (r "^0.15.0") (d #t) (k 0)) (d (n "range") (r "^0.2.0") (d #t) (k 0)))) (h "05idcgzsz6h0kb2472pnn6yjchskvxc0gkdks2hisc9a58an7bwv")))

(define-public crate-piston_meta_search-0.6.0 (c (n "piston_meta_search") (v "0.6.0") (d (list (d (n "piston_meta") (r "^0.16.0") (d #t) (k 0)) (d (n "range") (r "^0.2.0") (d #t) (k 0)))) (h "0kpl1qk31p255crbq6c28p9ixs7jaijddnf7s2f3zr6nh6i0qzff")))

(define-public crate-piston_meta_search-0.7.0 (c (n "piston_meta_search") (v "0.7.0") (d (list (d (n "piston_meta") (r "^0.19.0") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)))) (h "1bzvi11q7r7i5sdrbrv7nklf2387ni6jb8ivshd3271ylaq39ywm")))

(define-public crate-piston_meta_search-0.8.0 (c (n "piston_meta_search") (v "0.8.0") (d (list (d (n "piston_meta") (r "^0.20.0") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)))) (h "0rx55gx7mjb99c3klrpmjgxkb566kgnajjnjmvysi4ylszwlg6ay")))

(define-public crate-piston_meta_search-0.9.0 (c (n "piston_meta_search") (v "0.9.0") (d (list (d (n "piston_meta") (r "^0.21.0") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)))) (h "1akjb88l1m38j2d9600w7n4mh9vpbiygkqjqxc24niw4p8i7127f")))

(define-public crate-piston_meta_search-0.10.0 (c (n "piston_meta_search") (v "0.10.0") (d (list (d (n "piston_meta") (r "^0.22.0") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)))) (h "0byaz1kr6gvn8zcd0fy9gksinj30a91yzz17dmddxsy45qdqv1h5")))

(define-public crate-piston_meta_search-0.11.0 (c (n "piston_meta_search") (v "0.11.0") (d (list (d (n "piston_meta") (r "^0.24.0") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)))) (h "1bsqbhbk16nj9h5zm4d3dmdkgfky1rfbngrp18hd796hldm0l1zd")))

(define-public crate-piston_meta_search-0.12.0 (c (n "piston_meta_search") (v "0.12.0") (d (list (d (n "piston_meta") (r "^0.25.0") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)))) (h "11s8xvzq06pcjn2zpzrmg0bw37axfh14d1na1zn5lrq6nnj3z703")))

(define-public crate-piston_meta_search-0.13.0 (c (n "piston_meta_search") (v "0.13.0") (d (list (d (n "piston_meta") (r "^0.26.0") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)))) (h "0pxkb8scf9s3majbkyx6s78g0dnlbc84m5zjhwjlrqphnwgnbip4")))

(define-public crate-piston_meta_search-0.13.1 (c (n "piston_meta_search") (v "0.13.1") (d (list (d (n "piston_meta") (r "^0.26.2") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)))) (h "0964ddqb7kanhmdbfig7nyv65dn08clls49704cwqm7kswgqdi0z")))

(define-public crate-piston_meta_search-0.14.0 (c (n "piston_meta_search") (v "0.14.0") (d (list (d (n "piston_meta") (r "^0.27.0") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)))) (h "1k1hayxw2misnzwyfqwfx4qkqj4dps9yahsl74bip03giqz28d37")))

(define-public crate-piston_meta_search-0.15.0 (c (n "piston_meta_search") (v "0.15.0") (d (list (d (n "piston_meta") (r "^0.28.0") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)))) (h "1j0ffgsy25pyycv5jbpxq7ji6mqkc24a1gwvwqp3ng2gp3idhwiv")))

(define-public crate-piston_meta_search-0.16.0 (c (n "piston_meta_search") (v "0.16.0") (d (list (d (n "piston_meta") (r "^0.29.0") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)))) (h "0bria7kikqx134zfvsi2q9nikpfgv7d8al8ny4sw5k176vwfgd0x")))

(define-public crate-piston_meta_search-0.17.0 (c (n "piston_meta_search") (v "0.17.0") (d (list (d (n "piston_meta") (r "^0.30.0") (d #t) (k 0)) (d (n "range") (r "^0.3.1") (d #t) (k 0)))) (h "17h6mfqic3chj83gmq87f95722lhir11vj190cg4vs0nhjc35hrx")))

(define-public crate-piston_meta_search-0.18.0 (c (n "piston_meta_search") (v "0.18.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "06z0m7asjzyfa33gi4q11lf20d4kxrmg3mab7sx2x8yld43mgdir")))

(define-public crate-piston_meta_search-0.19.0 (c (n "piston_meta_search") (v "0.19.0") (d (list (d (n "piston_meta") (r "^2.0.0") (d #t) (k 0)))) (h "1sl1mnyzgh8zcq5dmicmyyyzni8wq03wfpahlmng5a4da2wrnw22")))

