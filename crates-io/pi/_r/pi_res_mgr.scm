(define-module (crates-io pi _r pi_res_mgr) #:use-module (crates-io))

(define-public crate-pi_res_mgr-0.1.0 (c (n "pi_res_mgr") (v "0.1.0") (d (list (d (n "pi_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.1") (f (quote ("rc"))) (d #t) (k 0)) (d (n "pi_slot_deque") (r "^0.1") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1ypncrlbf2aa6rsa2skhikgas9pdfg3aadk6yrp7a35j0j691jai")))

