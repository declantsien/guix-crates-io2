(define-module (crates-io pi _r pi_rsync) #:use-module (crates-io))

(define-public crate-pi_rsync-0.1.0 (c (n "pi_rsync") (v "0.1.0") (d (list (d (n "adler32") (r "^1.2") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)))) (h "0jc1913whvly8329w8jkh58ndqgdpijvsp74j2aaf1905ys8jbkc")))

