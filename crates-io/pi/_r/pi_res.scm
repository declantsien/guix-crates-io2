(define-module (crates-io pi _r pi_res) #:use-module (crates-io))

(define-public crate-pi_res-0.1.0 (c (n "pi_res") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pi_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_atom") (r "^0.1") (d #t) (k 0)) (d (n "pi_deque") (r "^0.1") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1") (d #t) (k 0)) (d (n "pi_lru") (r "^0.1") (d #t) (k 0)) (d (n "pi_share") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "pi_slab") (r "^0.1") (d #t) (k 0)))) (h "0zv52l06njscgp8vhanj0wzdd17sb5dy1nyjf9dvx79yc9w251g3") (f (quote (("rc" "pi_share/rc") ("default" "pi_share"))))))

