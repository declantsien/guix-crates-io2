(define-module (crates-io pi _r pi_rt_file) #:use-module (crates-io))

(define-public crate-pi_rt_file-0.1.0 (c (n "pi_rt_file") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "pi_async") (r "^0.1.0") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.1.5") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1.1") (f (quote ("xxhash"))) (d #t) (k 0)))) (h "0av1f07x4wsbyspgrpf2fllywz1bvhws3slb5cy2gw1q2g421p60")))

(define-public crate-pi_rt_file-0.1.1 (c (n "pi_rt_file") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "pi_async") (r "^0.1.0") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.1.5") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1.1") (f (quote ("xxhash"))) (d #t) (k 0)))) (h "09ilyv2p7cvkp1x6nbgy5b3wp81vr1r2wyaxa0lkh59y1swh96gq")))

(define-public crate-pi_rt_file-0.2.0 (c (n "pi_rt_file") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "pi_async") (r "^0.2") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.1.5") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1.1") (f (quote ("xxhash"))) (d #t) (k 0)))) (h "0asajkkdn35yl2qkvvralg5dp3rvw74ykw53b186mgjrhpihhnr9")))

(define-public crate-pi_rt_file-0.2.1 (c (n "pi_rt_file") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "pi_async") (r "^0.5") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.4") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1.1") (f (quote ("xxhash"))) (d #t) (k 0)))) (h "1sn3zg4r7l4bzlrjrzrg0qa9p7yk8f4rikpb4gyc2k1kayyzd251")))

(define-public crate-pi_rt_file-0.3.0 (c (n "pi_rt_file") (v "0.3.0") (d (list (d (n "async-lock") (r "^2.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "pi-async-rt") (r "^0.1") (d #t) (k 0)) (d (n "pi_async_file") (r "^0.5") (d #t) (k 0)) (d (n "pi_hash") (r "^0.1.1") (f (quote ("xxhash"))) (d #t) (k 0)))) (h "15p2gpmyw7c35b8dbgyagnsc2p3xp229a264bbhaba88dq7w3fmy")))

