(define-module (crates-io pi np pinpoint) #:use-module (crates-io))

(define-public crate-pinpoint-0.1.0 (c (n "pinpoint") (v "0.1.0") (h "1zg0plcjrxbl70m8ailfv4w272pbj85hqgid592kmx93vwzyjnrm") (f (quote (("pinned"))))))

(define-public crate-pinpoint-0.1.1 (c (n "pinpoint") (v "0.1.1") (h "1s16j6cbixnfrwd9q0zg14q830z11rd5xh8f1y4inw3ql6qcb68h") (f (quote (("pinned"))))))

(define-public crate-pinpoint-0.1.2 (c (n "pinpoint") (v "0.1.2") (h "1s4c1lajiw0hv74lpiylf9nrcvdjr22hqfiqfkzcabshja21l7ax") (f (quote (("slice_of_cells") ("pinned"))))))

