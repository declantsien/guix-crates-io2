(define-module (crates-io pi gl piglog) #:use-module (crates-io))

(define-public crate-piglog-1.0.0 (c (n "piglog") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "1i6d85pr1pbxpf87xg5bg3h3ip0hqh40aw4n2z26sxvm65dnikpr")))

(define-public crate-piglog-1.1.0 (c (n "piglog") (v "1.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0w1igkqzd2rz8y0gxz4rrb65jf8sli06nh4x5mcv2gqqvhkydc10")))

(define-public crate-piglog-1.2.0 (c (n "piglog") (v "1.2.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "040rmfc1nsqfg5ig2fcpgiza8myi47634q08mhrlchmqr1xkixrc")))

(define-public crate-piglog-1.3.0 (c (n "piglog") (v "1.3.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0a4z53va1b7ibvfgp3s7fpfcf3k4qia60q3hv7y56w8wcngn9yiz")))

(define-public crate-piglog-1.3.1 (c (n "piglog") (v "1.3.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "1xd1j7wcsbllg92nsl7z31vg1d8jpqrbhmni78l611gx2izymz9h")))

(define-public crate-piglog-1.3.2 (c (n "piglog") (v "1.3.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "01qzgff75668jpmabgzwzcq58k594cxvn4fa69rfpnk5rybnrygw")))

(define-public crate-piglog-1.4.0 (c (n "piglog") (v "1.4.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0z879sb6aq65hin7qxq8xyg1clhvxzrq69bzfa41x1a083rdhcp2") (s 2) (e (quote (("clap_derive" "dep:clap"))))))

(define-public crate-piglog-1.4.1 (c (n "piglog") (v "1.4.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "05d8pagfi1g6z686kq1d9v753lg7qlab6ywb28a163n8cjmbq6c5") (s 2) (e (quote (("clap_derive" "dep:clap"))))))

