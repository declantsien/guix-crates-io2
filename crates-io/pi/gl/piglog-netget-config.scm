(define-module (crates-io pi gl piglog-netget-config) #:use-module (crates-io))

(define-public crate-piglog-netget-config-1.0.0 (c (n "piglog-netget-config") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "netget") (r "^0.1.0") (d #t) (k 0)))) (h "0n9x3ngf0jjm2fmys1jg6z4acgbjyniiibl1m352zz287b96ngzj") (y #t)))

(define-public crate-piglog-netget-config-1.0.1 (c (n "piglog-netget-config") (v "1.0.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "netget") (r "^0.1.0") (d #t) (k 0)))) (h "0aw7zgm9va6bngngfn22hwx7k45p9dz40qdir06jy4hywcwgzy4z")))

