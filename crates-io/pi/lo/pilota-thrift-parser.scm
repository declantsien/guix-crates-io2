(define-module (crates-io pi lo pilota-thrift-parser) #:use-module (crates-io))

(define-public crate-pilota-thrift-parser-0.1.0 (c (n "pilota-thrift-parser") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1l78rbwkczwbp6b5g6s2c3fk0xxznjrzsnkchz76imcs3k6av19m")))

(define-public crate-pilota-thrift-parser-0.1.1 (c (n "pilota-thrift-parser") (v "0.1.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0xsz5vzz20nf33b8dfih0mwvf2g2b21vmqzyxj3315cxzd0h0p49")))

(define-public crate-pilota-thrift-parser-0.2.0 (c (n "pilota-thrift-parser") (v "0.2.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1gvwmwrx5qfv750b1b2w63mnfkvkymmh0048316pxjd5c133cwls")))

(define-public crate-pilota-thrift-parser-0.3.0 (c (n "pilota-thrift-parser") (v "0.3.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0n6flg0aw86rn0k69l3wj77dnaq3f9na98nfsxgl0nwlqi1nfn92")))

(define-public crate-pilota-thrift-parser-0.4.0 (c (n "pilota-thrift-parser") (v "0.4.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "01rxvy7xqhsl17jrnkbwd3lgnx0531fv55dr95wij0ja2zmlyh0r")))

(define-public crate-pilota-thrift-parser-0.4.1 (c (n "pilota-thrift-parser") (v "0.4.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0spi97dbizrggx7l2ag3ca5k37bl8kqpvhpn05iv3hb35zs7wakg")))

(define-public crate-pilota-thrift-parser-0.4.2 (c (n "pilota-thrift-parser") (v "0.4.2") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1w19f3cj7wydb1bq2yqxwh2s46brm3n774vbmrwva0gbqp4x2j32")))

(define-public crate-pilota-thrift-parser-0.9.0 (c (n "pilota-thrift-parser") (v "0.9.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "12wp7iia6ga34dj63x2m1hfc3qfj67filhf2ry2k4vi15cqd70a7")))

(define-public crate-pilota-thrift-parser-0.10.0 (c (n "pilota-thrift-parser") (v "0.10.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0r5i64id3vki7cl7cll1hd8rs43x4yq6r15yg5k21bi68xb9s7x5")))

(define-public crate-pilota-thrift-parser-0.11.0 (c (n "pilota-thrift-parser") (v "0.11.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0x0d24k59lkkmfsrrdg4xbqsk96zpixn3hzlvhg4pshdj994h2gj")))

(define-public crate-pilota-thrift-parser-0.11.1 (c (n "pilota-thrift-parser") (v "0.11.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0dxfg3mvcnf0cz2gmicmdgm5i1qlad8p53l62qgi3jszyarwv120")))

