(define-module (crates-io pi lo pilosaurus) #:use-module (crates-io))

(define-public crate-Pilosaurus-0.1.0 (c (n "Pilosaurus") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16vgrmq4lqw437zypfdz6mjfqxll0nwi3c4his5r31gnizz40m88") (y #t)))

(define-public crate-Pilosaurus-0.1.1 (c (n "Pilosaurus") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1msnw71zk6b4s1xs7mf3pxqiqvkmwgs3dq19chmsf63g237h2yk3") (y #t)))

