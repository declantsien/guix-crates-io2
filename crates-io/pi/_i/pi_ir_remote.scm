(define-module (crates-io pi _i pi_ir_remote) #:use-module (crates-io))

(define-public crate-pi_ir_remote-0.1.0 (c (n "pi_ir_remote") (v "0.1.0") (d (list (d (n "gpio-cdev") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "stoppable_thread") (r ">=0.2.1, <0.3.0") (d #t) (k 0)))) (h "0ykj55nahdx7ifnc5y8cq6hn1lffv9c850iscln49pwxgp0nk8r7")))

