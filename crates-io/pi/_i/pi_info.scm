(define-module (crates-io pi _i pi_info) #:use-module (crates-io))

(define-public crate-pi_info-0.1.0 (c (n "pi_info") (v "0.1.0") (h "1hc6g7h3raf4ynkyx6afr3v1m72h86mdangvb8m9n76lrn2p43h1")))

(define-public crate-pi_info-0.1.1 (c (n "pi_info") (v "0.1.1") (h "0fkxvylfwaxqy8had9c7bvr5p1swz6q4an5mqg1n0wq56qqlw1i7")))

(define-public crate-pi_info-0.1.2 (c (n "pi_info") (v "0.1.2") (h "0447rkb7yf6020py03xqdl0kq5wrd8w4wb9nczb9cdm22bn6di9w")))

(define-public crate-pi_info-0.1.3 (c (n "pi_info") (v "0.1.3") (h "1pibq8wmzwrs27nqsm0z2c3x8523iwf89pjys6llp6h94vslf0ya")))

