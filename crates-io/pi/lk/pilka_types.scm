(define-module (crates-io pi lk pilka_types) #:use-module (crates-io))

(define-public crate-pilka_types-0.6.0 (c (n "pilka_types") (v "0.6.0") (h "1n6rh2y61dky4ps6cmibj8k179q33a5nr95gnf7i3pp62g86wi8n")))

(define-public crate-pilka_types-0.7.0 (c (n "pilka_types") (v "0.7.0") (d (list (d (n "bytemuck") (r "^1.7.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dlabkxlcwyps2wkkq2z20zl9gi6bc0nq1hlmj8ch8jld7awddcf")))

(define-public crate-pilka_types-0.7.1 (c (n "pilka_types") (v "0.7.1") (d (list (d (n "bytemuck") (r "^1.7.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jjwk1fzzqzlckyd4rkjyl33s0s4xf720s2v52as8i9rravgksj8")))

(define-public crate-pilka_types-0.7.10 (c (n "pilka_types") (v "0.7.10") (d (list (d (n "bytemuck") (r "^1.7.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lhb3g9mhad36glvlkr4r15x7i50lwvpqxx82pv3wkz39mdd3kxl")))

