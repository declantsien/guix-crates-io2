(define-module (crates-io pi lk pilka_incremental) #:use-module (crates-io))

(define-public crate-pilka_incremental-0.1.0 (c (n "pilka_incremental") (v "0.1.0") (d (list (d (n "pilka_ash") (r "^0.1.1") (d #t) (k 0)) (d (n "pilka_winit") (r "^0.1.1") (d #t) (k 0)))) (h "181qgg5sd0g42dajqv9w78mpzasrxawwx3jf1xjb6qf3r4xaq80s")))

(define-public crate-pilka_incremental-0.2.3 (c (n "pilka_incremental") (v "0.2.3") (d (list (d (n "pilka_ash") (r "^0.2.3") (d #t) (k 0)) (d (n "pilka_winit") (r "^0.2.3") (d #t) (k 0)))) (h "0phh7zqkcqpb0yx6jafxxc1w5xz1m77ffpvb6j3g65ir7hrz4ma1")))

(define-public crate-pilka_incremental-0.2.4 (c (n "pilka_incremental") (v "0.2.4") (d (list (d (n "pilka_ash") (r "^0.2.4") (d #t) (k 0)) (d (n "pilka_winit") (r "^0.2.3") (d #t) (k 0)))) (h "1f00410rxx3kv4vjjg6j7la6nqlvs2rlrxr2kizpgjrgcaksw87b")))

(define-public crate-pilka_incremental-0.3.1 (c (n "pilka_incremental") (v "0.3.1") (d (list (d (n "pilka_ash") (r "^0.3.1") (d #t) (k 0)) (d (n "pilka_winit") (r "^0.3.1") (d #t) (k 0)))) (h "16mlivjrm62l6grmsm6qqbr2hvkmyqfaq25570w4fms85wb9lwhp")))

(define-public crate-pilka_incremental-0.3.3 (c (n "pilka_incremental") (v "0.3.3") (d (list (d (n "pilka_ash") (r "^0.3.1") (d #t) (k 0)) (d (n "pilka_winit") (r "^0.3.1") (d #t) (k 0)))) (h "04xzxp9i720mkhz1q4c6y7drllwc5l8qmcxsqdmxq0ryd4k9sb0k")))

(define-public crate-pilka_incremental-0.4.2 (c (n "pilka_incremental") (v "0.4.2") (d (list (d (n "pilka_ash") (r "^0.3.1") (d #t) (k 0)) (d (n "pilka_winit") (r "^0.3.1") (d #t) (k 0)))) (h "0lf1zv90j02bfrvj8v8vm1ycb3056irsh4b8mjngs5q2qdwcnr9f")))

(define-public crate-pilka_incremental-0.4.3 (c (n "pilka_incremental") (v "0.4.3") (d (list (d (n "pilka_ash") (r "^0.4.3") (d #t) (k 0)) (d (n "pilka_winit") (r "^0.3.1") (d #t) (k 0)))) (h "0rj58wp7dp47y1fhg2073cb2p47pmyxk2ylxbk0rxxaccbs7y0hr")))

(define-public crate-pilka_incremental-0.4.4 (c (n "pilka_incremental") (v "0.4.4") (d (list (d (n "pilka_ash") (r "^0.4.3") (d #t) (k 0)) (d (n "pilka_winit") (r "^0.3.1") (d #t) (k 0)))) (h "10yfgvi62b8fgfsmaxwyqlm44f960vjd2pvfwaifvw6x5i1d338w")))

(define-public crate-pilka_incremental-0.4.5 (c (n "pilka_incremental") (v "0.4.5") (d (list (d (n "pilka_ash") (r "^0.4.3") (d #t) (k 0)) (d (n "pilka_winit") (r "^0.3.1") (d #t) (k 0)))) (h "0w5mh86ay0spya6s7353lq4bxv37h1hn2brkzcslphdp2m2csdp7")))

(define-public crate-pilka_incremental-0.5.0 (c (n "pilka_incremental") (v "0.5.0") (d (list (d (n "pilka_ash") (r "^0.5.0") (d #t) (k 0)) (d (n "pilka_winit") (r "^0.5.0") (d #t) (k 0)))) (h "1mh7q3a9md3305qi31h2h12axmaw4prndqbcbnbaf2jssdlqnbys")))

(define-public crate-pilka_incremental-0.5.3 (c (n "pilka_incremental") (v "0.5.3") (d (list (d (n "pilka_ash") (r "^0.5.0") (d #t) (k 0)) (d (n "pilka_winit") (r "^0.5.0") (d #t) (k 0)))) (h "16cxgkzigs0cx80rmwra1zzyjg7k44vcdqvi886fglx41ff67xxh")))

