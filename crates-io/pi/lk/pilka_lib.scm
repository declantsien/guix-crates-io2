(define-module (crates-io pi lk pilka_lib) #:use-module (crates-io))

(define-public crate-pilka_lib-0.1.0 (c (n "pilka_lib") (v "0.1.0") (d (list (d (n "pilka_dyn") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "pilka_incremental") (r "^0.1.0") (d #t) (k 0)))) (h "0w1m6gg64diizirhgns6khwbr13p924pp0pbchclrvsal3dws6vn") (f (quote (("dynamic" "pilka_dyn"))))))

(define-public crate-pilka_lib-0.2.3 (c (n "pilka_lib") (v "0.2.3") (d (list (d (n "pilka_dyn") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "pilka_incremental") (r "^0.2.3") (d #t) (k 0)))) (h "1nh66s78v1fv6yzpn16l1dbfr35g3l6yzpwyyqsqzzc51mpjv6zk") (f (quote (("dynamic" "pilka_dyn"))))))

(define-public crate-pilka_lib-0.2.4 (c (n "pilka_lib") (v "0.2.4") (d (list (d (n "pilka_dyn") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "pilka_incremental") (r "^0.2.4") (d #t) (k 0)))) (h "0fnf6xxvr5gj6j5dsk65dizdfjrxmdybfyfn3q40md0vli8a2qp9") (f (quote (("dynamic" "pilka_dyn"))))))

(define-public crate-pilka_lib-0.3.1 (c (n "pilka_lib") (v "0.3.1") (d (list (d (n "pilka_dyn") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "pilka_incremental") (r "^0.3.1") (d #t) (k 0)))) (h "0vhydygyfdla09fkm4zm5x7fwr7bk65mg78rm4lggcwm8wrknqa4") (f (quote (("dynamic" "pilka_dyn"))))))

(define-public crate-pilka_lib-0.4.2 (c (n "pilka_lib") (v "0.4.2") (d (list (d (n "pilka_dyn") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "pilka_incremental") (r "^0.4.2") (d #t) (k 0)))) (h "14b0y3vdf1k9mcigdjmqziv4xlj0idfiyk0lbrs7pqvbzcpdkbb3") (f (quote (("dynamic" "pilka_dyn"))))))

(define-public crate-pilka_lib-0.4.3 (c (n "pilka_lib") (v "0.4.3") (d (list (d (n "pilka_dyn") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "pilka_incremental") (r "^0.4.2") (d #t) (k 0)))) (h "08yms1pl493rf7v6yjrw6gihlfcbfh2gz7ncb913x6qd2n5shxf2") (f (quote (("dynamic" "pilka_dyn"))))))

(define-public crate-pilka_lib-0.5.0 (c (n "pilka_lib") (v "0.5.0") (d (list (d (n "pilka_dyn") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "pilka_incremental") (r "^0.5.0") (d #t) (k 0)))) (h "120qrdfj22av3k879fr5fdf70q0rm1gdxsxz9mvyfcv0bfih1ds2") (f (quote (("dynamic" "pilka_dyn"))))))

