(define-module (crates-io pi lk pilka_winit) #:use-module (crates-io))

(define-public crate-pilka_winit-0.1.0 (c (n "pilka_winit") (v "0.1.0") (d (list (d (n "winit") (r "^0.23.0") (d #t) (k 0)))) (h "1csqvns572019h4vkcrvkjpr1q3am0nlsjxwca5fxh44izll1ybz")))

(define-public crate-pilka_winit-0.1.1 (c (n "pilka_winit") (v "0.1.1") (d (list (d (n "winit") (r "^0.23.0") (d #t) (k 0)))) (h "0k5jf0b34v04dkr0ms811zj9nlqxj5kax1rkg0cwibqmzqhwpb4n")))

(define-public crate-pilka_winit-0.2.3 (c (n "pilka_winit") (v "0.2.3") (d (list (d (n "winit") (r "^0.23.0") (d #t) (k 0)))) (h "034kbpzv4533058lza6r83vlb4wn59gyky24kgs0s0v6h039wfby")))

(define-public crate-pilka_winit-0.3.1 (c (n "pilka_winit") (v "0.3.1") (d (list (d (n "winit") (r "^0.23.0") (d #t) (k 0)))) (h "1q9jj8ciyfwv50rz0fwsz7z2fw7hwl74ly3g3yq3bwb1xy8bdrq6")))

(define-public crate-pilka_winit-0.5.0 (c (n "pilka_winit") (v "0.5.0") (d (list (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "1lg4p0h6n6c2ib54alwqkc76a59kwl3a5dilns682qhmm22i8byv")))

