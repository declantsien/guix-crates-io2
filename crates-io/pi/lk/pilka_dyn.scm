(define-module (crates-io pi lk pilka_dyn) #:use-module (crates-io))

(define-public crate-pilka_dyn-0.1.0 (c (n "pilka_dyn") (v "0.1.0") (d (list (d (n "pilka_incremental") (r "^0.1.0") (d #t) (k 0)))) (h "0sbvf7z1zys1s3z0bzjgpc465zr1rnnbs71z6rpfc6yn54ps32j3")))

(define-public crate-pilka_dyn-0.2.3 (c (n "pilka_dyn") (v "0.2.3") (d (list (d (n "pilka_incremental") (r "^0.2.3") (d #t) (k 0)))) (h "1k7ffi05j8dd2wsgqndbjcnc54r5nqpb8napfd1jhs7gb2l6q669")))

(define-public crate-pilka_dyn-0.3.1 (c (n "pilka_dyn") (v "0.3.1") (d (list (d (n "pilka_incremental") (r "^0.3.1") (d #t) (k 0)))) (h "05wzsf1sjm24c7jxplayicj9lykym0m8sibfgfl3bj8yqy63kk9z")))

(define-public crate-pilka_dyn-0.4.2 (c (n "pilka_dyn") (v "0.4.2") (d (list (d (n "pilka_incremental") (r "^0.4.2") (d #t) (k 0)))) (h "0n4why3gqfszql921f8f7myicgxdf2irw5c1ppydnav8ykizk1lp")))

(define-public crate-pilka_dyn-0.4.3 (c (n "pilka_dyn") (v "0.4.3") (d (list (d (n "pilka_incremental") (r "^0.4.2") (d #t) (k 0)))) (h "06222kzrna3lxqb2pd1aa2f11h42xkpr78nkr85xz1br6hj18dm3")))

(define-public crate-pilka_dyn-0.5.0 (c (n "pilka_dyn") (v "0.5.0") (d (list (d (n "pilka_incremental") (r "^0.5.0") (d #t) (k 0)))) (h "1ynn0wldnpn8ql0xnfggbm87qyawamw4x7ilfanp8jb8dq26fk43")))

