(define-module (crates-io pi _l pi_local_timer) #:use-module (crates-io))

(define-public crate-pi_local_timer-0.1.0 (c (n "pi_local_timer") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.0") (d #t) (k 0)) (d (n "pi_dyn_uint") (r "^0.1") (d #t) (k 0)) (d (n "pi_heap") (r "^0.1") (d #t) (k 0)) (d (n "pi_time") (r "^0.1") (d #t) (k 0)))) (h "0pi12673gklnzlb6avwfgqdv65jv86xwv1xpfxx44mxx8wxyzy4i")))

