(define-module (crates-io pi _l pi_link_list) #:use-module (crates-io))

(define-public crate-pi_link_list-0.1.0 (c (n "pi_link_list") (v "0.1.0") (d (list (d (n "derive-deref-rs") (r "^0.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_print_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_slotmap") (r "^0.1") (d #t) (k 2)))) (h "0z5zhg646dyj7sl73mlxza79nghzklvil87rac2zd3y487m5qm53")))

(define-public crate-pi_link_list-0.1.1 (c (n "pi_link_list") (v "0.1.1") (d (list (d (n "derive-deref-rs") (r "^0.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_print_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_slotmap") (r "^0.1") (d #t) (k 2)))) (h "076knb2hn3hf9mppc3l29lx547vpk27nwqdpch9zan1swjxlf1gy")))

(define-public crate-pi_link_list-0.1.3 (c (n "pi_link_list") (v "0.1.3") (d (list (d (n "derive-deref-rs") (r "^0.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_print_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_slotmap") (r "^0.1") (d #t) (k 2)))) (h "0hxc50wxd2dvkdp8c1b3b4c3h5yjvazannr99gvlk25wf0jzyjxr")))

(define-public crate-pi_link_list-0.1.4 (c (n "pi_link_list") (v "0.1.4") (d (list (d (n "derive-deref-rs") (r "^0.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_print_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_slotmap") (r "^0.1") (d #t) (k 2)))) (h "1xdx31zj3rggg7c9fcpgy24b5c6xhisvj0hdmd9dcqr3k9x4mkxy")))

(define-public crate-pi_link_list-0.1.5 (c (n "pi_link_list") (v "0.1.5") (d (list (d (n "derive-deref-rs") (r "^0.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_print_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_slotmap") (r "^0.1") (d #t) (k 2)))) (h "1j5kg2fnp63j6n9zgj0sqlfqx806bj7bprxl0ffg5xwbif9xpxr2")))

(define-public crate-pi_link_list-0.1.6 (c (n "pi_link_list") (v "0.1.6") (d (list (d (n "derive-deref-rs") (r "^0.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_print_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_slotmap") (r "^0.1") (d #t) (k 2)))) (h "188qic9dcjpjdpbfnhycq20629bhbpxwk20mij9nxlnlsbkcjwjc")))

(define-public crate-pi_link_list-0.1.7 (c (n "pi_link_list") (v "0.1.7") (d (list (d (n "derive-deref-rs") (r "^0.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_print_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_slotmap") (r "^0.1") (d #t) (k 2)))) (h "1i9da7hcg29vi86xrgrpdmlfggkwaj3wwd8jm6fnhngcf48wn6mx")))

(define-public crate-pi_link_list-0.1.8 (c (n "pi_link_list") (v "0.1.8") (d (list (d (n "derive-deref-rs") (r "^0.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_print_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_slotmap") (r "^0.1") (d #t) (k 2)))) (h "0wf6bcshzymmcccxpq6mpg6lah4214z70115rywrdh7x3d4pn4k9")))

(define-public crate-pi_link_list-0.1.9 (c (n "pi_link_list") (v "0.1.9") (d (list (d (n "derive-deref-rs") (r "^0.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "pi_null") (r "^0.1") (d #t) (k 0)) (d (n "pi_print_any") (r "^0.1") (d #t) (k 0)) (d (n "pi_slotmap") (r "^0.1") (d #t) (k 2)))) (h "0k1iyk8xa1pmvkjhw1vrzz3vnfdczywizmj6y2wfgz9058nrjj2s")))

