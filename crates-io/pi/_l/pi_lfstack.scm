(define-module (crates-io pi _l pi_lfstack) #:use-module (crates-io))

(define-public crate-pi_lfstack-0.1.2 (c (n "pi_lfstack") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)))) (h "1z7dxyswhagmpg29plfh91j87prscs23bw8m1qmqr8693dm9n9x7")))

(define-public crate-pi_lfstack-0.1.3 (c (n "pi_lfstack") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)))) (h "0y1wylim3pqdjd10v6amjawr8k0azqbscb5kb44hgdp2c9vwlpac")))

(define-public crate-pi_lfstack-0.1.5 (c (n "pi_lfstack") (v "0.1.5") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)))) (h "1pd92qr9w12k6wh157bysplhvl4x0z0nc5xxbfbkxzai0k5v9qfy")))

