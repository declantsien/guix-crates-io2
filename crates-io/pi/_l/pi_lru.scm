(define-module (crates-io pi _l pi_lru) #:use-module (crates-io))

(define-public crate-pi_lru-0.1.0 (c (n "pi_lru") (v "0.1.0") (d (list (d (n "pi_deque") (r "^0.1") (d #t) (k 0)) (d (n "pi_slab") (r "^0.1") (d #t) (k 0)))) (h "0d835xz6l4470xghknpqb3yqsy1ljba98jn6yi8fn49zdy4920fv")))

