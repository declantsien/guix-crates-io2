(define-module (crates-io pi _l pi_listener) #:use-module (crates-io))

(define-public crate-pi_listener-0.1.0 (c (n "pi_listener") (v "0.1.0") (d (list (d (n "im") (r "^13.0.0") (d #t) (k 0)) (d (n "pi_share") (r "^0.1") (d #t) (k 0)))) (h "038cjzsiy3yg4c0ii1vxprc2ikb1ynq2vkrr08sr0cw5fqdm1yy4")))

(define-public crate-pi_listener-0.2.0 (c (n "pi_listener") (v "0.2.0") (d (list (d (n "im") (r "^13.0.0") (d #t) (k 0)) (d (n "pi_share") (r "^0.4") (d #t) (k 0)))) (h "1hn9179wbvyd58ksdhirabrczxgdbixbr9hv5la82q6hsxjhab4g")))

