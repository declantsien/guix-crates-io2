(define-module (crates-io pi ed pied-piper) #:use-module (crates-io))

(define-public crate-pied-piper-0.1.0 (c (n "pied-piper") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "1zwjvfqmd8j5xddnmq2725kg2xm402d6nfnvarj4yqsipx04r34p")))

