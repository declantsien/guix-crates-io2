(define-module (crates-io pi pi piping) #:use-module (crates-io))

(define-public crate-piping-0.1.0 (c (n "piping") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "19vpc7ns8vz3fz4baizh146cdd4497n8fdx5hz21pmjbm8zvxsd3")))

(define-public crate-piping-0.1.1 (c (n "piping") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0xmlwvib44zys3l29v76qc1vqnf7wy58ply6nff6fhvgj8vbf4pc")))

(define-public crate-piping-0.1.2 (c (n "piping") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0hq43bkp03n8j3scr76jraxws9f8733sp99g2bihmjng91762vy9")))

(define-public crate-piping-0.1.3 (c (n "piping") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1hk1npr2j6rf0x834j4jq7pfi4is1arfi52lyc11wybwr7ingrq2")))

(define-public crate-piping-0.1.4 (c (n "piping") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1alyg2qvii4zrgz7cm6vn1mgi2gr9i53rsffqnpmq58g0x7ih7ai")))

(define-public crate-piping-1.0.0 (c (n "piping") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "00qrl4mlc8sdpkqq5ic4g6i0l0fad91zwb74mx9bqrwvbi155y2l")))

