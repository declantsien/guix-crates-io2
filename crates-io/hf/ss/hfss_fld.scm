(define-module (crates-io hf ss hfss_fld) #:use-module (crates-io))

(define-public crate-hfss_fld-0.1.0 (c (n "hfss_fld") (v "0.1.0") (d (list (d (n "colour") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1hbli22hkgn1mcwawjw8shan4g27rxs6xxqb9dxr6vxy2pxbwlaw")))

(define-public crate-hfss_fld-0.1.1 (c (n "hfss_fld") (v "0.1.1") (d (list (d (n "colour") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "182gvx6npay9zfasi5hjwgkbywnzbbznwa4knnylfs2w0x15ldsv")))

(define-public crate-hfss_fld-0.1.2 (c (n "hfss_fld") (v "0.1.2") (d (list (d (n "colour") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0zsv6kcg0m87ikagm710rwq0vipn474szi3cvq31zp0d3nmn5aa6")))

(define-public crate-hfss_fld-0.1.3 (c (n "hfss_fld") (v "0.1.3") (d (list (d (n "colour") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0ymw7lxahamdz173xin08x49w9cm3v40zfwx41wjlkcdnsvkidzk")))

(define-public crate-hfss_fld-0.1.4 (c (n "hfss_fld") (v "0.1.4") (d (list (d (n "colour") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "09xw91pcv36k7hzsi47xjzziv5y50g26sl0yqlylbs1fw99jlacw")))

