(define-module (crates-io hf tw hftwo) #:use-module (crates-io))

(define-public crate-hftwo-0.1.0 (c (n "hftwo") (v "0.1.0") (d (list (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "08q0yrrr9klm5ryaqbjxc23wxy3ha4pcyh1kb4rky6z1ilbygzgk") (s 2) (e (quote (("defmt-03" "dep:defmt"))))))

(define-public crate-hftwo-0.1.1 (c (n "hftwo") (v "0.1.1") (d (list (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0drzjnbcf20hpvr1bcrflzgp3a7ifwzm77b7wcn31sxyh7d1fcba") (s 2) (e (quote (("defmt-03" "dep:defmt"))))))

(define-public crate-hftwo-0.1.2 (c (n "hftwo") (v "0.1.2") (d (list (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qn33h46cjj7q8qpq3ygri0v1ifyj0s2rrk6qjml6lx7vkd522nz") (s 2) (e (quote (("defmt-03" "dep:defmt"))))))

