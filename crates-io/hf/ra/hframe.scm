(define-module (crates-io hf ra hframe) #:use-module (crates-io))

(define-public crate-hframe-0.1.0 (c (n "hframe") (v "0.1.0") (d (list (d (n "egui") (r "^0.25.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Window" "Document" "HtmlBodyElement" "console" "HtmlHeadElement" "Navigator"))) (d #t) (k 0)))) (h "049lc8ix540nsflksi67vcmx2yc2jr73pb3s2nfiqql6d0g7xbk0") (r "1.73")))

(define-public crate-hframe-0.1.1 (c (n "hframe") (v "0.1.1") (d (list (d (n "egui") (r "^0.25.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Window" "Document" "HtmlBodyElement" "console" "HtmlHeadElement" "Navigator"))) (d #t) (k 0)))) (h "1l5l10kn7xskvghn9rngk4zw6zyk7jb8vlnlh54x8q4hqwdy37v9") (r "1.73")))

(define-public crate-hframe-0.2.0 (c (n "hframe") (v "0.2.0") (d (list (d (n "egui") (r "^0.25.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Window" "Document" "HtmlBodyElement" "console" "HtmlHeadElement" "Navigator"))) (d #t) (k 0)))) (h "14n4p9fl968jjjpjffbhyfbv0yhgaf8lhyb03hcxi2ncjg980igj") (r "1.73")))

(define-public crate-hframe-0.3.0 (c (n "hframe") (v "0.3.0") (d (list (d (n "egui") (r "^0.25.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Window" "Document" "HtmlBodyElement" "console" "HtmlHeadElement" "Navigator" "Element" "NodeList"))) (d #t) (k 0)))) (h "1fmq42sx4bmmfjrmw667v2h5dv0ch4x47qjabwqcjqhy7zc6fi07") (r "1.73")))

(define-public crate-hframe-0.4.0 (c (n "hframe") (v "0.4.0") (d (list (d (n "egui") (r "^0.25.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Window" "Document" "HtmlBodyElement" "console" "HtmlHeadElement" "Navigator" "Element" "NodeList" "HtmlElement" "CssStyleDeclaration"))) (d #t) (k 0)))) (h "1fd8c7mqn8cbivnlarmd7cc9vhcwgqz7fib71x5cqpkclr16hhl8") (r "1.73")))

(define-public crate-hframe-0.4.1 (c (n "hframe") (v "0.4.1") (d (list (d (n "egui") (r ">=0.25.0, <1.0.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("Window" "Document" "HtmlBodyElement" "console" "HtmlHeadElement" "Navigator" "Element" "NodeList" "HtmlElement" "CssStyleDeclaration"))) (d #t) (k 0)))) (h "0zqzg1vxhdc850ca3y0c2jfr2q18fd22xij942923nyy5m7qgx1k") (r "1.73")))

