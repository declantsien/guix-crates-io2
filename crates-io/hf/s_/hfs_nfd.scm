(define-module (crates-io hf s_ hfs_nfd) #:use-module (crates-io))

(define-public crate-hfs_nfd-0.1.0 (c (n "hfs_nfd") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0xmzc83g2rvfldy0g9l3ca5kz7k3j57a14bx7nppvwzrxdbs8zli")))

(define-public crate-hfs_nfd-0.1.1 (c (n "hfs_nfd") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "07rcsbckr5ddsd54xm1wk2v15i5k99i3d2axl1km5hlk7l5156y2")))

(define-public crate-hfs_nfd-1.0.0 (c (n "hfs_nfd") (v "1.0.0") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "18424n3jy6fqkszdwahacam2b600xlrbr8b3qx1k095m8hpaac5k")))

(define-public crate-hfs_nfd-1.1.0 (c (n "hfs_nfd") (v "1.1.0") (d (list (d (n "ahash") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1gr5cnmyl6ihajr5869ljh7lwx9snx6mqx6nxa98vf831g3bjjnc")))

(define-public crate-hfs_nfd-2.0.0 (c (n "hfs_nfd") (v "2.0.0") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1qsv5vpqc7rzlbnbkbp564jm7dn8y30nb5fd5j9isz34ysm3vwfa") (f (quote (("bench"))))))

