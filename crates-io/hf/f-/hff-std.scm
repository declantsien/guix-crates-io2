(define-module (crates-io hf f- hff-std) #:use-module (crates-io))

(define-public crate-hff-std-0.1.0 (c (n "hff-std") (v "0.1.0") (h "1v1n35jfg7mfz67ya5c4j6ksdkqbzj6ps67dnqiaigbzq4gak6cl")))

(define-public crate-hff-std-0.2.0 (c (n "hff-std") (v "0.2.0") (d (list (d (n "hff-core") (r "^0.2.0") (d #t) (k 0)))) (h "0wnf1li5i9qrsssx306rbps85bvs0mnlmv8xj5j6byljif3p0lpd")))

(define-public crate-hff-std-0.2.1 (c (n "hff-std") (v "0.2.1") (d (list (d (n "hff-core") (r "^0.2.1") (d #t) (k 0)))) (h "15x3l8k9399f982173y1pxqsgi1z1mx0g42vzji32m2qpv60gv1f")))

(define-public crate-hff-std-0.2.2 (c (n "hff-std") (v "0.2.2") (d (list (d (n "hff-core") (r "^0.2.1") (d #t) (k 0)))) (h "08ckfvsr6ssbyglidsviqf20dz2hgvgya1r1z2lklamqdjmjbrif")))

(define-public crate-hff-std-0.2.3 (c (n "hff-std") (v "0.2.3") (d (list (d (n "hff-core") (r "^0.2.1") (d #t) (k 0)))) (h "0wvhvrznn98ld31wk2rrmakdbxzrwjnq13ar7chsmbv2qsqbsb0a")))

(define-public crate-hff-std-0.2.4 (c (n "hff-std") (v "0.2.4") (d (list (d (n "hff-core") (r "^0.2.2") (d #t) (k 0)))) (h "0wig5c3lb0yqdy8wb1qn4a1lmyh0daq7zn70gxnqrvcnrwr2wa0b")))

(define-public crate-hff-std-0.3.2 (c (n "hff-std") (v "0.3.2") (d (list (d (n "hff-core") (r "^0.3.1") (d #t) (k 0)))) (h "06mj2gy2hjp77hr0sily27f7c1zzchjr6k3klsi203ddz9a6ignd")))

(define-public crate-hff-std-0.3.3 (c (n "hff-std") (v "0.3.3") (d (list (d (n "hff-core") (r "^0.3.1") (d #t) (k 0)))) (h "0r5zmq15wi7wjlz1a771h7rmnmg8myxiiv0sgcvy5bflnfsjnycs")))

(define-public crate-hff-std-0.3.4 (c (n "hff-std") (v "0.3.4") (d (list (d (n "hff-core") (r "^0.3.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "130bfmgyvyxhfvaaljgj7zwddcxff10zlss9dh6wncbmxwc19mq8") (f (quote (("default")))) (s 2) (e (quote (("compression" "dep:xz2"))))))

(define-public crate-hff-std-0.3.5 (c (n "hff-std") (v "0.3.5") (d (list (d (n "hff-core") (r "^0.3.2") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0k9gmf1mpg7bi1a14k3098y0nwxqjwppr3b9ffhk43ifkjxsphqm") (f (quote (("default")))) (s 2) (e (quote (("compression" "dep:xz2"))))))

(define-public crate-hff-std-0.3.6 (c (n "hff-std") (v "0.3.6") (d (list (d (n "hff-core") (r "^0.3.3") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0ffjjv5hzvgvs372icmgqjl3sln3jq3w1pg8gcrf6a2mpngh6hgj") (f (quote (("default")))) (s 2) (e (quote (("compression" "dep:xz2" "hff-core/compression"))))))

(define-public crate-hff-std-0.3.7 (c (n "hff-std") (v "0.3.7") (d (list (d (n "hff-core") (r "^0.3.3") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0yalb9g9lhlmk6gd85lfgyjlywm1jpnrqw3hbgg0mxy1srhhrvkj") (f (quote (("default")))) (s 2) (e (quote (("compression" "dep:xz2" "hff-core/compression"))))))

(define-public crate-hff-std-0.4.1 (c (n "hff-std") (v "0.4.1") (d (list (d (n "hff-core") (r "^0.4.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "1m3965qci239ysm94i7bbzlv81xp9ja67xar90hqn8mp1y3y1chc") (f (quote (("default")))) (s 2) (e (quote (("compression" "dep:xz2" "hff-core/compression"))))))

(define-public crate-hff-std-0.4.2 (c (n "hff-std") (v "0.4.2") (d (list (d (n "hff-core") (r "^0.4.2") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0gnpkg3bbmmxwb843j3qhvn4iciwbwrd46y426d755a76dabac7q") (f (quote (("default")))) (s 2) (e (quote (("compression" "dep:xz2" "hff-core/compression"))))))

(define-public crate-hff-std-0.5.0 (c (n "hff-std") (v "0.5.0") (d (list (d (n "hff-core") (r "^0.5.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "00dii5y6x1gdm2yxjkgcxv0fxwz7x618j0yiazapw4mfch4vs0f8") (f (quote (("default")))) (s 2) (e (quote (("compression" "dep:xz2" "hff-core/compression"))))))

(define-public crate-hff-std-0.5.3 (c (n "hff-std") (v "0.5.3") (d (list (d (n "hff-core") (r "^0.5.3") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0vv81d4b674i45ggy4vfg0j1fy8fviisgq3n7jlw24rfkk0lgwl7") (f (quote (("default")))) (s 2) (e (quote (("compression" "dep:xz2" "hff-core/compression"))))))

(define-public crate-hff-std-0.5.4 (c (n "hff-std") (v "0.5.4") (d (list (d (n "hff-core") (r "^0.5.4") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0r8m230agxz3y38g5fidyk2mhg59mqqzdgxjw2kq581asmvfs9d5") (f (quote (("default")))) (s 2) (e (quote (("compression" "dep:xz2" "hff-core/compression"))))))

(define-public crate-hff-std-0.6.0 (c (n "hff-std") (v "0.6.0") (d (list (d (n "hff-core") (r "^0.6.0") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "1vq7byha339yifjfjq3jbzwcr7pygsnc30xva4nw9ql6kb0brhjv") (f (quote (("default")))) (s 2) (e (quote (("compression" "dep:xz2" "hff-core/compression"))))))

(define-public crate-hff-std-0.6.1 (c (n "hff-std") (v "0.6.1") (d (list (d (n "hff-core") (r "^0.6.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0q92z1g4l0rr7v3hlpgp9gnskziypdvf4h533ds76my3xv18wn4d") (f (quote (("default")))) (s 2) (e (quote (("compression" "dep:xz2" "hff-core/compression"))))))

(define-public crate-hff-std-0.6.3 (c (n "hff-std") (v "0.6.3") (d (list (d (n "hff-core") (r "^0.6.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0yxh25s7igpv49nd9861pv1ss7ak93bimc75kg8lkjazhcwvwfc6") (f (quote (("default")))) (s 2) (e (quote (("compression" "dep:xz2" "hff-core/compression"))))))

