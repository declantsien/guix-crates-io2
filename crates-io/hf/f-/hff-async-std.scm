(define-module (crates-io hf f- hff-async-std) #:use-module (crates-io))

(define-public crate-hff-async-std-0.1.1 (c (n "hff-async-std") (v "0.1.1") (d (list (d (n "hff-core") (r "^0.2.0") (d #t) (k 0)) (d (n "hff-std") (r "^0.2.0") (d #t) (k 0)))) (h "0qgyb5l723m27nv8kla5a518hm6z97w1jamb8javafk971mgfdg4")))

(define-public crate-hff-async-std-0.1.2 (c (n "hff-async-std") (v "0.1.2") (d (list (d (n "hff-core") (r "^0.2.2") (d #t) (k 0)) (d (n "hff-std") (r "^0.2.4") (d #t) (k 0)))) (h "1mn90q4x1ws66l3lam7x1v26sl6486zygc6nqvq38qdk6vpgjlby")))

(define-public crate-hff-async-std-0.3.1 (c (n "hff-async-std") (v "0.3.1") (d (list (d (n "hff-core") (r "^0.3.1") (d #t) (k 0)) (d (n "hff-std") (r "^0.3.1") (d #t) (k 0)))) (h "09n441rmnrzr4fd9pvhffprbak4b6d87qsc6mz6igzdr1sv3gk16")))

(define-public crate-hff-async-std-0.3.2 (c (n "hff-async-std") (v "0.3.2") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.3.3") (d #t) (k 0)) (d (n "hff-std") (r "^0.3.6") (d #t) (k 0)))) (h "0nas6whh3bfpn9m4qlmimvjkg2rsgnv4yaw85hbvwbsyjaacdsgj")))

(define-public crate-hff-async-std-0.3.3 (c (n "hff-async-std") (v "0.3.3") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.3.3") (d #t) (k 0)) (d (n "hff-std") (r "^0.3.6") (d #t) (k 0)))) (h "19pkq3npjj3sfjq34fa12gmkn2qsa7y8vqm7yxwga8qlmksmscd8")))

(define-public crate-hff-async-std-0.4.1 (c (n "hff-async-std") (v "0.4.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.4.0") (d #t) (k 0)) (d (n "hff-std") (r "^0.4.0") (d #t) (k 0)))) (h "0d1cc6ddhnvq9qsgqmb7hnkgc8j4z5c8j9sfwy7f5zis2m00zsfw")))

(define-public crate-hff-async-std-0.4.2 (c (n "hff-async-std") (v "0.4.2") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.4.2") (d #t) (k 0)) (d (n "hff-std") (r "^0.4.2") (d #t) (k 0)))) (h "0jlpzj47hbn405ysra1n3yrcv80n2ma4d7fv73r2pprqa8yncxwr")))

(define-public crate-hff-async-std-0.5.0 (c (n "hff-async-std") (v "0.5.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.5.0") (d #t) (k 0)) (d (n "hff-std") (r "^0.5.0") (d #t) (k 0)))) (h "170ln5d14a1psrfj3cb7gk0mwn6g8c3yi8dxwcxgnl52xsrn1b4a")))

(define-public crate-hff-async-std-0.5.3 (c (n "hff-async-std") (v "0.5.3") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.5.3") (d #t) (k 0)) (d (n "hff-std") (r "^0.5.3") (d #t) (k 0)))) (h "0zfip280qmgfhdfbamr1z17dgzwghxjjc04pf2ms8p0xl8802pmm")))

(define-public crate-hff-async-std-0.5.4 (c (n "hff-async-std") (v "0.5.4") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.5.4") (d #t) (k 0)) (d (n "hff-std") (r "^0.5.4") (d #t) (k 0)))) (h "01l81kydcl51z0f8k14kbnv4k5d99hdjbakwbfqw7ic44vdxc027")))

(define-public crate-hff-async-std-0.6.0 (c (n "hff-async-std") (v "0.6.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.6.0") (d #t) (k 0)) (d (n "hff-std") (r "^0.6.0") (d #t) (k 0)))) (h "07lma6hxcsinzi4zmkfmmxfg2lwl28fjpb651wz2f3r8rq60algn")))

(define-public crate-hff-async-std-0.6.1 (c (n "hff-async-std") (v "0.6.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.6.1") (d #t) (k 0)) (d (n "hff-std") (r "^0.6.1") (d #t) (k 0)))) (h "01y1gi91cjpvch1ms0f9vlcmx807f50c0ach2801rznacsr6kxwm")))

(define-public crate-hff-async-std-0.6.3 (c (n "hff-async-std") (v "0.6.3") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.6.1") (d #t) (k 0)) (d (n "hff-std") (r "^0.6.1") (d #t) (k 0)))) (h "112lpfapzdimgndg05s9h9bi19w208fn5kxfl7yyjy7vgm84cxn1")))

