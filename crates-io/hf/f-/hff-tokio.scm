(define-module (crates-io hf f- hff-tokio) #:use-module (crates-io))

(define-public crate-hff-tokio-0.1.1 (c (n "hff-tokio") (v "0.1.1") (d (list (d (n "hff-core") (r "^0.2.0") (d #t) (k 0)) (d (n "hff-std") (r "^0.2.0") (d #t) (k 0)))) (h "0kh2niapg027q3py4ghpyn6gdzxavkxai45ip6ia11vr94xkq0gm")))

(define-public crate-hff-tokio-0.1.2 (c (n "hff-tokio") (v "0.1.2") (d (list (d (n "hff-core") (r "^0.2.2") (d #t) (k 0)) (d (n "hff-std") (r "^0.2.4") (d #t) (k 0)))) (h "05syy3s30kbwf3zamvcxacs82phq3bzcd36bzarkvjvp14ms8kad")))

(define-public crate-hff-tokio-0.3.1 (c (n "hff-tokio") (v "0.3.1") (d (list (d (n "hff-core") (r "^0.3.1") (d #t) (k 0)) (d (n "hff-std") (r "^0.3.1") (d #t) (k 0)))) (h "0jfaj81lkk05ipdmq4n7wbijx4bidl08frl5p85d1g91cc563csn")))

(define-public crate-hff-tokio-0.3.2 (c (n "hff-tokio") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.3.3") (d #t) (k 0)) (d (n "hff-std") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d2j3saqm39dxiw0cl236m7wyg233d50jsv6jnw5k2ap6sdycizl")))

(define-public crate-hff-tokio-0.3.3 (c (n "hff-tokio") (v "0.3.3") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.3.3") (d #t) (k 0)) (d (n "hff-std") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12d3sifki2kpgbpyqicsync3nw0nb60xnppnv505q012m0vhy0fp")))

(define-public crate-hff-tokio-0.4.1 (c (n "hff-tokio") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.4.0") (d #t) (k 0)) (d (n "hff-std") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kbrpwab16zw512w2m89yj387804q7bdr6x3dv6aqnynk7yr7mw3")))

(define-public crate-hff-tokio-0.4.2 (c (n "hff-tokio") (v "0.4.2") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.4.2") (d #t) (k 0)) (d (n "hff-std") (r "^0.4.2") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pg8xzcsxkzrh2wms3i95l449cn623y5fdb8wm1w0sz3ndhmi5v1")))

(define-public crate-hff-tokio-0.5.0 (c (n "hff-tokio") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.5.0") (d #t) (k 0)) (d (n "hff-std") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cmw5143d06r2fr6xrsfyayppppzxyjc3kkm1xfjq14fps2g9nj4")))

(define-public crate-hff-tokio-0.5.3 (c (n "hff-tokio") (v "0.5.3") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.5.3") (d #t) (k 0)) (d (n "hff-std") (r "^0.5.3") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i4369m2yk1d2qlffd0yfrxd3b0ccp3x3jlk71xk8ghs4kkqygzn")))

(define-public crate-hff-tokio-0.5.4 (c (n "hff-tokio") (v "0.5.4") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.5.4") (d #t) (k 0)) (d (n "hff-std") (r "^0.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mcq5rg00q9j54xsdcz67r54w8bimz2wlfinybp9pr1qpxksaxp1")))

(define-public crate-hff-tokio-0.6.0 (c (n "hff-tokio") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.6.0") (d #t) (k 0)) (d (n "hff-std") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "172630r6smjg5w3v628jcxw5q30zc1h3bn4yfg48nwdbqysalzgb")))

(define-public crate-hff-tokio-0.6.1 (c (n "hff-tokio") (v "0.6.1") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.6.1") (d #t) (k 0)) (d (n "hff-std") (r "^0.6.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jd7ind6l73l2kbqp61x8v49vav36l7n9nkgv0rshzr7ks6w9ac2")))

(define-public crate-hff-tokio-0.6.3 (c (n "hff-tokio") (v "0.6.3") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "hff-core") (r "^0.6.1") (d #t) (k 0)) (d (n "hff-std") (r "^0.6.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03w9gs49smjjq4yrnf4h3qm91746h39mii8x4p7y8rahhkaalwr6")))

