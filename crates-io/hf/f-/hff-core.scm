(define-module (crates-io hf f- hff-core) #:use-module (crates-io))

(define-public crate-hff-core-0.1.0 (c (n "hff-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1k5h1001ha0sgsww3rckwnnilgzac41d68164q65hwzyi73frb8v")))

(define-public crate-hff-core-0.2.0 (c (n "hff-core") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0m10sh19087r141z5iaygzxxn2s7ypb5bp2gcyl9nwiiyr3yxh7n")))

(define-public crate-hff-core-0.2.1 (c (n "hff-core") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "13xy3djrp2b9d7f511wd7pakvvyza5m36mi15aphgcp67vzhdz8f")))

(define-public crate-hff-core-0.2.2 (c (n "hff-core") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "05zgp8xx62pxmya71yikazyfcf2qjgwdj7zz6b0j09wca7zs1bgs")))

(define-public crate-hff-core-0.3.1 (c (n "hff-core") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1fvgscl5asqpr1fc4g6ksn1d9ws64ws1g1qq8a54c6yx0ia0m1mp")))

(define-public crate-hff-core-0.3.2 (c (n "hff-core") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "16zrf7xf8d4dg7jhjkmifrrcpyzf35m861jc3gpm3p291ani8xba")))

(define-public crate-hff-core-0.3.3 (c (n "hff-core") (v "0.3.3") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0zkn5h3qffz1p1r481rrrwrmchvk24qdl3m44pp0gs88dx6jwk3m") (s 2) (e (quote (("compression" "dep:xz2"))))))

(define-public crate-hff-core-0.3.4 (c (n "hff-core") (v "0.3.4") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "11jxrki7hg3hihqs5lg82sr07gx8nm7kl009c71276vw5826nf8r") (s 2) (e (quote (("compression" "dep:xz2"))))))

(define-public crate-hff-core-0.4.1 (c (n "hff-core") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "08rysppxj3sncr8i264wsnqn49lwvcbvsd1m8jzan0wfim1nf8h4") (s 2) (e (quote (("compression" "dep:xz2"))))))

(define-public crate-hff-core-0.4.2 (c (n "hff-core") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0ccrzbd0khlpcmbf4f1k8yhchnpvzfmsx245q352mfas7brn3k83") (s 2) (e (quote (("compression" "dep:xz2"))))))

(define-public crate-hff-core-0.5.0 (c (n "hff-core") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0rraiz0zaajjb8rzn3yzqy9i766mci8ky6cj9kazlcz2x9ll9qzh") (s 2) (e (quote (("compression" "dep:xz2"))))))

(define-public crate-hff-core-0.5.2 (c (n "hff-core") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "1jwlmbl0zv47qk13zcndcp9y43ii6qab9kaclz4jfibrhm13rfav") (s 2) (e (quote (("compression" "dep:xz2"))))))

(define-public crate-hff-core-0.5.3 (c (n "hff-core") (v "0.5.3") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "15yrh45nkvzsxyi7g5pf9p9sipi9n9cgl8dns241fzk521b82lx5") (s 2) (e (quote (("compression" "dep:xz2"))))))

(define-public crate-hff-core-0.5.4 (c (n "hff-core") (v "0.5.4") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "09900jqz6bjxng0z9zf2jv977xfy93yz0r80xnibnwlhilhv3ybg") (s 2) (e (quote (("compression" "dep:xz2"))))))

(define-public crate-hff-core-0.6.0 (c (n "hff-core") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0p9598qhmc7kh2lriri4pfkglncs9i5m1x1vkrpvrgcwah9xna2k") (s 2) (e (quote (("compression" "dep:xz2"))))))

(define-public crate-hff-core-0.6.1 (c (n "hff-core") (v "0.6.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "09xb8110dg4n2ybqv5h3bbxcan96qik3kv23l0vp9qdcw4w3zi65") (s 2) (e (quote (("compression" "dep:xz2"))))))

(define-public crate-hff-core-0.6.3 (c (n "hff-core") (v "0.6.3") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0vsiqrr2y8cfjfx4s0hmzwi3np690q65akhz08q237zzzprqsr69") (s 2) (e (quote (("compression" "dep:xz2"))))))

