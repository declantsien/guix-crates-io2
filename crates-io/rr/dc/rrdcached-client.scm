(define-module (crates-io rr dc rrdcached-client) #:use-module (crates-io))

(define-public crate-rrdcached-client-0.1.0 (c (n "rrdcached-client") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)))) (h "1822k8hxajg5hhiwwl7jjjvh17s466fvqkl7imgc8ffhiz1ar3br")))

(define-public crate-rrdcached-client-0.1.1 (c (n "rrdcached-client") (v "0.1.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)))) (h "03h2l3z0zkvgj3fdlj0zmiq9m4jdfp06wc6kjmrlkdh10gpcy7bc")))

