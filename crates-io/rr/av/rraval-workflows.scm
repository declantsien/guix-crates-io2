(define-module (crates-io rr av rraval-workflows) #:use-module (crates-io))

(define-public crate-rraval-workflows-0.1.0 (c (n "rraval-workflows") (v "0.1.0") (h "1589crln7wacqbm4nr0kkz4rlzv9zywpaf543wi74bdbhzz7cmn2")))

(define-public crate-rraval-workflows-0.1.1 (c (n "rraval-workflows") (v "0.1.1") (h "0cildgpa9vpl3n99djx9yidsc6vigc98b41m4flw3b84vgkhjadr")))

