(define-module (crates-io rr w_ rrw_macro) #:use-module (crates-io))

(define-public crate-rrw_macro-0.1.0 (c (n "rrw_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pqrpa7jq67c4gb9kjmkwsljlvd3kvikyasv1pp77abwi0bcvz6n")))

(define-public crate-rrw_macro-0.1.1 (c (n "rrw_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qbnn2k11i712cjrjs8fi90zqh1zq54a5g3fk41fj6n24rc97vdr")))

