(define-module (crates-io rr ul rrules) #:use-module (crates-io))

(define-public crate-rrules-0.1.0 (c (n "rrules") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "082rnlhhhs037qyfc2vfdnawk9g48dgvccln39n740aza0n4gyx7")))

(define-public crate-rrules-0.1.1 (c (n "rrules") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "1fbzxb2qz5hnbrpkxz47i86rhdp6n1lg8nbcxcs6cfr5jcfnw7gs")))

(define-public crate-rrules-0.1.2 (c (n "rrules") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "0dcima9ps48rgf3d3lcvr9snm1p9ymggmq9cv7ljgczlznfzc4hm")))

(define-public crate-rrules-0.2.0 (c (n "rrules") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "10fvf8ib92z554r8m4pm6hwkhvfdpghkmnvqngm5svry0y4rx30k")))

(define-public crate-rrules-0.2.1 (c (n "rrules") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0rf9xfxjpwzklz3gaiwncfwmz09nwp2isidmwf4s7xr6006g0v4v")))

(define-public crate-rrules-0.2.2 (c (n "rrules") (v "0.2.2") (d (list (d (n "cargo-sonar") (r "^0.14.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1b4rv4s446asqff2y4yfi2km0wall5kn87ml7fhva1ajlha5ha3g")))

(define-public crate-rrules-0.2.3 (c (n "rrules") (v "0.2.3") (d (list (d (n "cargo-sonar") (r "^0.14.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0qzgzvps8bhnz1mra7iw4p6cbdzw8i40g8x89nmxzzfaz7zgrf80")))

