(define-module (crates-io rr es rres) #:use-module (crates-io))

(define-public crate-rres-0.1.0 (c (n "rres") (v "0.1.0") (d (list (d (n "drm") (r "^0.6") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "lexopt") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.15") (f (quote ("stderr" "colors"))) (k 0)))) (h "07il5qfzfi79hl4brswhxd51r3sxpxbjm0in78z8b24y61i54d29")))

(define-public crate-rres-0.1.1 (c (n "rres") (v "0.1.1") (d (list (d (n "drm") (r "^0.6") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "lexopt") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.15") (f (quote ("stderr" "colors"))) (k 0)))) (h "0rk4if2lmhfy781df27g3p7xmm94vspzm2cfa3s1bh6z2d6hj5wp")))

(define-public crate-rres-0.1.2 (c (n "rres") (v "0.1.2") (d (list (d (n "drm") (r "^0.6") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "lexopt") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (f (quote ("stderr" "colors"))) (k 0)))) (h "0kbz7waid38a4wq75frmash3bbj8396266sp3lf5kd0f20bgj0hp")))

(define-public crate-rres-0.1.3 (c (n "rres") (v "0.1.3") (d (list (d (n "drm") (r "^0.7") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "lexopt") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0") (f (quote ("stderr" "colors"))) (k 0)))) (h "14idx062hdbyswfwq2ji694dcmspzg8glaa3aridwlr9k5bpmb0w")))

(define-public crate-rres-0.1.4 (c (n "rres") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "drm") (r "^0.9") (d #t) (k 0)) (d (n "lexopt") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0") (f (quote ("stderr" "colors"))) (k 0)))) (h "0j3w2p0kwg7jijwjsgfygfdsv73s2zv30yvhl0z3dkj91wy84sw4")))

(define-public crate-rres-0.1.5 (c (n "rres") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "drm") (r "^0.9") (d #t) (k 0)) (d (n "lexopt") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0") (f (quote ("stderr" "colors"))) (k 0)))) (h "18ydv23qxxzlv7vc4z0szlk5dbzrhfpfm6ym78n3px1d2hp08q5n")))

