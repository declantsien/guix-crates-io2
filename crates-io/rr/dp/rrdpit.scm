(define-module (crates-io rr dp rrdpit) #:use-module (crates-io))

(define-public crate-rrdpit-0.0.2 (c (n "rrdpit") (v "0.0.2") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0i53cx7rx8lash14g3v0w277cfq2631plk1dxy63471l9jl0jmds")))

(define-public crate-rrdpit-0.0.3 (c (n "rrdpit") (v "0.0.3") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1n2zam23fqdpwn1zbv4wb206cl14af08g9dixhvjw26nfhgjlb8m")))

(define-public crate-rrdpit-0.0.4 (c (n "rrdpit") (v "0.0.4") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.17") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0i6fzijmiawdp84p64wk41dx4pz9sq6bda10mg1f16sj6bamj4ms")))

