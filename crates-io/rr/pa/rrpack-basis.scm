(define-module (crates-io rr pa rrpack-basis) #:use-module (crates-io))

(define-public crate-rrpack-basis-0.40.0 (c (n "rrpack-basis") (v "0.40.0") (d (list (d (n "rill-derive") (r "^0.40.0") (d #t) (k 0)) (d (n "rill-engine") (r "^0.40.0") (o #t) (d #t) (k 0)) (d (n "rill-protocol") (r "^0.40.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "vectorize") (r "^0.2.0") (d #t) (k 0)))) (h "0qiz8vpyhkahfcm6a404n8bh22j80jymxcziy42pmch3hvf5bzh8") (f (quote (("engine" "rill-engine") ("default" "engine"))))))

(define-public crate-rrpack-basis-0.41.0 (c (n "rrpack-basis") (v "0.41.0") (d (list (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "rill-derive") (r "^0.41.0") (d #t) (k 0)) (d (n "rill-engine") (r "^0.41.0") (o #t) (d #t) (k 0)) (d (n "rill-protocol") (r "^0.41.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "vectorize") (r "^0.2.0") (d #t) (k 0)))) (h "012zkv7cha270fd3ar7sph3n0mfr4sgb401wy7d26ifrsj39c4w9") (f (quote (("engine" "rill-engine") ("default" "engine"))))))

