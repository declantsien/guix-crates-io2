(define-module (crates-io rr pl rrplug_proc) #:use-module (crates-io))

(define-public crate-rrplug_proc-2.0.0 (c (n "rrplug_proc") (v "2.0.0") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "06xdim0xgz9zzd90qlpkdc7h30ix4ydrrckdh8is8wllgza6si16")))

(define-public crate-rrplug_proc-2.1.0 (c (n "rrplug_proc") (v "2.1.0") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0ashqhb65picq4d011qsvxnmzs4wpg0fw9ya3ql7imxk3g9283vs")))

(define-public crate-rrplug_proc-2.1.1 (c (n "rrplug_proc") (v "2.1.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0rg6s8796b1vrqhmyjlrv7rzgig5a3miapnsgm5hyaa4jflcy4f0")))

(define-public crate-rrplug_proc-3.0.0 (c (n "rrplug_proc") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "192c2d4p0kn44373xhfidv8mg860fqa308mp3lw14wmbrnz723ck")))

(define-public crate-rrplug_proc-4.0.0 (c (n "rrplug_proc") (v "4.0.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1l1qw3wrvjylrp5558x3hp4vmh93vj0bc44n6qqx1crvjqakl0mv")))

