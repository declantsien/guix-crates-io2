(define-module (crates-io rr pl rrpl) #:use-module (crates-io))

(define-public crate-rrpl-0.1.0 (c (n "rrpl") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "14b72f0pnrfdgh4cv92bl8wkcxvrhanjxmc2h0p9k8byxlv8gwv1")))

