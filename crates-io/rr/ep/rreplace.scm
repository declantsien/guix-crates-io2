(define-module (crates-io rr ep rreplace) #:use-module (crates-io))

(define-public crate-rreplace-0.1.0 (c (n "rreplace") (v "0.1.0") (h "0xpw1mf3lpxqa03fr8vavwrgikhdwkair2n5w92y1dd7zmr9vqmg")))

(define-public crate-rreplace-0.1.1 (c (n "rreplace") (v "0.1.1") (h "1srqb38v3qn8gzr0l3zqzbhvbk0yncxcqgf76v6g1lplww8gv1d1") (y #t)))

(define-public crate-rreplace-0.1.2 (c (n "rreplace") (v "0.1.2") (h "0rlqfx6clbplpr5pnly2qp0kbkbk8g0y05hc1s9cj8l0fh6y6sq0") (y #t)))

(define-public crate-rreplace-0.1.3 (c (n "rreplace") (v "0.1.3") (h "1dby9w8020jrdbbfdsbma4apg6y358kjl91rw04436bvqmzj4i5i") (y #t)))

(define-public crate-rreplace-0.1.4 (c (n "rreplace") (v "0.1.4") (h "1d7yjxj2ick08z87fybmdw18p7hc1as1nr1rrk2r5vlv3dgxhbr3") (y #t)))

(define-public crate-rreplace-0.1.5 (c (n "rreplace") (v "0.1.5") (h "1arrns4660wmq09ssdcf4sgyvv4mx4n4gqb72i99h4fkc3f5r1ic") (y #t)))

