(define-module (crates-io rr ai rrain) #:use-module (crates-io))

(define-public crate-rrain-0.1.0 (c (n "rrain") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json" "blocking" "stream"))) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textplots") (r "^0.8") (d #t) (k 0)))) (h "0p0bp02szw27456lgjykqsrvbnmcdx39jd2yxdbvrkcr5kfidwax")))

