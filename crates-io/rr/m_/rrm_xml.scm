(define-module (crates-io rr m_ rrm_xml) #:use-module (crates-io))

(define-public crate-rrm_xml-0.0.1-alpha.0 (c (n "rrm_xml") (v "0.0.1-alpha.0") (d (list (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "16lrigv1f848kxh3s01xx4hbzyyfwka0virb9b6msj337yna2zv7") (r "1.58.1")))

(define-public crate-rrm_xml-0.0.1-alpha.6 (c (n "rrm_xml") (v "0.0.1-alpha.6") (d (list (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0v33k36i35hbv0f7rb2g1bxv9904j3z4gwlnn3gqniwax2bppmwn") (r "1.58.1")))

(define-public crate-rrm_xml-0.0.1-alpha.7 (c (n "rrm_xml") (v "0.0.1-alpha.7") (d (list (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0qz20r3kdm98q3an82xjh782qp4b1sqljj5j4pccir7i1q42rnsm")))

