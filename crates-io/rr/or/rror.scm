(define-module (crates-io rr or rror) #:use-module (crates-io))

(define-public crate-rror-0.1.0 (c (n "rror") (v "0.1.0") (h "1i7ckzg0bkncdqk8ybxjq2xmpmzmw3zi5p4d722lly820956inqa")))

(define-public crate-rror-0.1.1 (c (n "rror") (v "0.1.1") (h "1s0pb23m9fl61vwi0y59n8b84jb66ikbgq3ffiij26jcdxmg69a7")))

