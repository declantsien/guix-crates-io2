(define-module (crates-io rr sy rrsync) #:use-module (crates-io))

(define-public crate-rrsync-0.0.0 (c (n "rrsync") (v "0.0.0") (h "1ysa2qp6bn9x3f9nsw768663x8l6zww7c5sgl5dypbbvakazv3xz") (y #t)))

(define-public crate-rrsync-0.1.0 (c (n "rrsync") (v "0.1.0") (d (list (d (n "cdchunking") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (f (quote ("termcolor" "atty" "humantime"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1x1ysfqcgvbvllx037r7waqc485sv3grk5s9lm4pnsm3an47wsvg") (y #t)))

