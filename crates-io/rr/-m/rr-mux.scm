(define-module (crates-io rr -m rr-mux) #:use-module (crates-io))

(define-public crate-rr-mux-0.1.0 (c (n "rr-mux") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "17a30msl8bsxbib965afz1cdpsqipi9cp0ykwj6zp4qn9dkimqmc")))

(define-public crate-rr-mux-0.2.0 (c (n "rr-mux") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "08kajg3h2vyz5sqg1h6q2gvdxf10x5vzsr7jwjz8ri11lnm2cp71")))

(define-public crate-rr-mux-0.3.0 (c (n "rr-mux") (v "0.3.0") (d (list (d (n "derive_builder") (r "^0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1vwqkz79mdm8pk0xfcwqcwmns05ijjs9d7nyjpj4c9i49bggfnid")))

(define-public crate-rr-mux-0.4.0 (c (n "rr-mux") (v "0.4.0") (d (list (d (n "derive_builder") (r "^0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0q0sv1w71amlncfmvj1k8dnd5pcdy3r1c74wch0y4g5y3767km3b")))

(define-public crate-rr-mux-0.5.0 (c (n "rr-mux") (v "0.5.0") (d (list (d (n "derive_builder") (r "^0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1cjhjlsfdrbkxlsqqymd8726p5qxg1fynrp2jj32m13spjq55nwc")))

(define-public crate-rr-mux-0.6.0 (c (n "rr-mux") (v "0.6.0") (d (list (d (n "derive_builder") (r "^0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1xify1kmz1hj9kpvaqq93fqybd7md3x4kr5ykik4z8gkkrlbr86b")))

(define-public crate-rr-mux-0.7.0 (c (n "rr-mux") (v "0.7.0") (d (list (d (n "derive_builder") (r "^0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "034kqpxlprw1z5hy70l1c4p6ijz1aja7nmv9r7r44al0b6dkdsyc")))

(define-public crate-rr-mux-0.7.1 (c (n "rr-mux") (v "0.7.1") (d (list (d (n "derive_builder") (r "^0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "07vj4dhm0ddb42mn5xxk3xfj6gzzvh78qvmalfizc4yj8apgchnz")))

(define-public crate-rr-mux-0.8.0 (c (n "rr-mux") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1.19") (d #t) (k 0)) (d (n "derive_builder") (r "^0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "07ngc7rq7l269yhqn87j3s06yac6kvpvxrd4lc6jhkirwv0r8ayx")))

(define-public crate-rr-mux-0.9.0 (c (n "rr-mux") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1.19") (d #t) (k 0)) (d (n "derive_builder") (r "^0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "072m7niwrffsq78b7zvr0vw5cl06a1pmylv3caaqrb1gsffxgwkc")))

(define-public crate-rr-mux-0.10.0 (c (n "rr-mux") (v "0.10.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1zqllw083dyv4hr9gi8b6n3ql3qhfkl3pl1m2hjibgbgwyd1ndxn")))

(define-public crate-rr-mux-0.10.1 (c (n "rr-mux") (v "0.10.1") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1gvhcr6zi1dd822f993xfx2sxf93qjb9as2k2wj3za3x2m3xpdd0")))

