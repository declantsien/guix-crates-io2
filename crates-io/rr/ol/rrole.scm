(define-module (crates-io rr ol rrole) #:use-module (crates-io))

(define-public crate-rrole-0.1.1 (c (n "rrole") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quicli") (r "^0.3") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.32.0") (d #t) (k 0)) (d (n "rusoto_sts") (r "^0.32.0") (d #t) (k 0)))) (h "18z5s5climq2sjk8659vh7yr086sf3xz6b2zfbhmbsbz6081pbad")))

(define-public crate-rrole-0.1.2 (c (n "rrole") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quicli") (r "^0.3") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.32.0") (d #t) (k 0)) (d (n "rusoto_sts") (r "^0.32.0") (d #t) (k 0)))) (h "064hq02x480sj6b3qv8fjk5ii1l6nk700z5s6digy1npqshiiv21")))

