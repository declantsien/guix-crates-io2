(define-module (crates-io sh ec shecret) #:use-module (crates-io))

(define-public crate-shecret-0.1.0 (c (n "shecret") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "14ig6dc9nznapvri0pzrijhgw0al59hhji4mgi9wz1lp4l7dm5d8")))

(define-public crate-shecret-0.1.1 (c (n "shecret") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "0v5hvsb6sbqislmnp5ik3qpyjm837l919p6fficfy3iabp2y1hxn")))

(define-public crate-shecret-0.1.2 (c (n "shecret") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "0hzn9j16mz74cgcikb171kqanp82zwb11g5422shg32p15qzvg8r")))

(define-public crate-shecret-0.1.3 (c (n "shecret") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "027a26a83cs0w1vj9r3ali9n1c7qj8p5p76pzmsdcdakc36n4w9k")))

(define-public crate-shecret-0.1.4 (c (n "shecret") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "19s6bymy82ilhz4yyqz82ds9rdp35yhwgl1p2xmkm4mvf1sc5k6n")))

(define-public crate-shecret-0.1.5 (c (n "shecret") (v "0.1.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "00670sqz10vdm0pgmpmry170iqcg77crw7sl5k04gh8kszhsb9x9")))

(define-public crate-shecret-0.1.6 (c (n "shecret") (v "0.1.6") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "00d3nrk4ypmdgxrdxa9chq1yra1vjgp4km38vwqmmalxnj8s0lr2")))

(define-public crate-shecret-0.2.0 (c (n "shecret") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "1karkah2g0kdzg0bn7w2lph5l8pzdg3i28f5q01sjzd2hw1vaqnv")))

(define-public crate-shecret-0.3.0 (c (n "shecret") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "1pf3hmn32xilrgxqg7kdrj9nwfzk0dsakqs3s0v7i05d6nqnlbhz")))

(define-public crate-shecret-1.0.0 (c (n "shecret") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "0ahz61s33jv4ips817k8f97g39i395cdw7640bkyn8ifbm9clgxb")))

