(define-module (crates-io sh t4 sht4x) #:use-module (crates-io))

(define-public crate-sht4x-0.1.0 (c (n "sht4x") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "fixed") (r "^1.20.0") (d #t) (k 0)) (d (n "sensirion-i2c") (r "^0.2") (d #t) (k 0)))) (h "0ckgcqm21dbbnnmk8pipfyqz7fp76r5s7k5msy7lbyay1cq27dbh") (s 2) (e (quote (("defmt" "dep:defmt"))))))

