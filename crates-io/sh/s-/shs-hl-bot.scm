(define-module (crates-io sh s- shs-hl-bot) #:use-module (crates-io))

(define-public crate-shs-hl-bot-0.0.0 (c (n "shs-hl-bot") (v "0.0.0") (h "0xxik5l4w7frpnida10cvxcyhac2ylj51320mgq0h24iqfhwr65a")))

(define-public crate-shs-hl-bot-0.0.1 (c (n "shs-hl-bot") (v "0.0.1") (h "1x35mj8f4jznhp3r6l0smddp3hgzhw98nfh17pbhfl9110rixq3k")))

