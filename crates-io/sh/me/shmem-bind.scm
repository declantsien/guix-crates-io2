(define-module (crates-io sh me shmem-bind) #:use-module (crates-io))

(define-public crate-shmem-bind-0.1.0 (c (n "shmem-bind") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1nbh4xldsrbbvgw6vqyayfk5aq8khlsqw77nk4al461gywx72l79")))

(define-public crate-shmem-bind-0.1.1 (c (n "shmem-bind") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "198wklm0b1r9zm1vvw66vp95rc8ni3wsdcr6vajvqaxp47fk0cxh")))

(define-public crate-shmem-bind-0.1.2 (c (n "shmem-bind") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1hnrrg7a8s99m69bh1jil12bzvg0fsqvihb6v4z79lc2s54j5nk3")))

(define-public crate-shmem-bind-0.1.3 (c (n "shmem-bind") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "01l5ndvwzwv7slp2171q0nawjwbd536niaj6zn3xgkfd75xk21pw")))

