(define-module (crates-io sh me shmem) #:use-module (crates-io))

(define-public crate-shmem-0.1.0 (c (n "shmem") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (d #t) (k 0)))) (h "0r686cfr2bavxjilj2lr3sll6chk5gci0ia9dkm99gxqbxkz3z2z")))

(define-public crate-shmem-0.2.0 (c (n "shmem") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (d #t) (t "cfg(unix)") (k 0)))) (h "0d0piscxa37hicx78grxr0zqjvbd2kw455gaa3mqfia96k67y64j")))

