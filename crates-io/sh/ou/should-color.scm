(define-module (crates-io sh ou should-color) #:use-module (crates-io))

(define-public crate-should-color-0.1.0 (c (n "should-color") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 2)))) (h "0paxsv0alx1nzlmvqg4y1xxr2fp74qipvw9j9y6cr0kw4f4hmd29")))

(define-public crate-should-color-0.2.0 (c (n "should-color") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 2)))) (h "0fw079incblyc0hahsbprd8s6d6lxrsvg82qycjcbvvjfkji90m2") (f (quote (("no_color") ("default" "no_color" "clicolor" "clicolor_force") ("clicolor_force") ("clicolor"))))))

(define-public crate-should-color-0.2.1 (c (n "should-color") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 2)))) (h "0x2pcjkkcgc34bps0q2gnys3x3xkk1ask0r2kzmjwai597qqb391") (f (quote (("no_color") ("default" "no_color" "clicolor" "clicolor_force") ("clicolor_force") ("clicolor"))))))

(define-public crate-should-color-0.3.0 (c (n "should-color") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 2)))) (h "06y4gjdpyvag0cqvvvjhxxbiqidls9h7pj64c122g5zlkv6kqlbd") (f (quote (("no_color") ("default" "no_color" "clicolor" "clicolor_force") ("clicolor_force") ("clicolor"))))))

(define-public crate-should-color-0.4.0 (c (n "should-color") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("color" "derive" "std"))) (o #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)))) (h "0w11wl0crx4d6z9jnp3bayaa3vqmf20vz1cf2pipsydd9s7bp8qs") (f (quote (("no_color") ("default" "no_color" "clicolor" "clicolor_force") ("clicolor_force") ("clicolor"))))))

(define-public crate-should-color-0.5.0 (c (n "should-color") (v "0.5.0") (d (list (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("color" "derive" "std"))) (o #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("cargo" "derive"))) (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)))) (h "0rkqyikm7y4p81ayv3qs2c5y0n6y61qqc0zlabbj1jpwijfi8pp5") (f (quote (("stream" "atty") ("no_color") ("default" "no_color" "clicolor" "clicolor_force" "stream") ("clicolor_force") ("clicolor"))))))

(define-public crate-should-color-0.5.1 (c (n "should-color") (v "0.5.1") (d (list (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("color" "derive" "std"))) (o #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("cargo" "derive"))) (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 2)))) (h "0hggc5bnmd599hqsf0gn288zq99aij6pa8qakzzd8k8c5nk9nxnq") (f (quote (("stream" "atty") ("no_color") ("default" "no_color" "clicolor" "clicolor_force" "stream") ("clicolor_force") ("clicolor"))))))

(define-public crate-should-color-0.5.2 (c (n "should-color") (v "0.5.2") (d (list (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("color" "derive" "std"))) (o #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("cargo" "derive"))) (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "document-features") (r "^0.2.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 2)))) (h "0f3q8pvanplahzza2ablc4vyrxkhg46ks4ckbfvyhnvlhxhw4l0h") (f (quote (("no_color") ("default" "clicolor" "clicolor_force" "no_color" "stream") ("clicolor_force") ("clicolor")))) (s 2) (e (quote (("stream" "dep:atty") ("clap" "dep:clap"))))))

