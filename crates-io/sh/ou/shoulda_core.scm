(define-module (crates-io sh ou shoulda_core) #:use-module (crates-io))

(define-public crate-shoulda_core-0.1.0 (c (n "shoulda_core") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1njsn0507b0iyp5ylr7qsvr2gcznbkzi4habdfgzv2kjxpb0m2i2")))

(define-public crate-shoulda_core-0.1.1 (c (n "shoulda_core") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1hd1p8x1pijdv97ms7jr48fd6kvwn7ds5r3iq7f2riji2a2spp93")))

(define-public crate-shoulda_core-0.1.2 (c (n "shoulda_core") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "0g411995wyzwv14l201skskm8hpn20ak22kqz4a1pfkbx43cl133")))

(define-public crate-shoulda_core-0.1.3 (c (n "shoulda_core") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "0wp8qbr4w3fzik5fvjnc71dzbq7zyh14vn9gbph03c6h0aspfk9v")))

(define-public crate-shoulda_core-0.1.4 (c (n "shoulda_core") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1vylhfx98sa54y2adrxifibj43dljmngxfk73dkmaajbvvgb0hmj")))

(define-public crate-shoulda_core-0.1.5 (c (n "shoulda_core") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "0ilzizk9pxb341kv0mf63jikli3pd9440n6d16ki87h35lbpl1jb")))

(define-public crate-shoulda_core-0.1.6 (c (n "shoulda_core") (v "0.1.6") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1qpm4bs99ds54kqvvqiyn76mxx27b72i4y449g3k60x1r87kvvry")))

(define-public crate-shoulda_core-0.1.7 (c (n "shoulda_core") (v "0.1.7") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1jqaqamk3m49arnq751g3a19kfjbw3d7z98l87riy7aa9nlh0nww")))

(define-public crate-shoulda_core-0.1.8 (c (n "shoulda_core") (v "0.1.8") (d (list (d (n "const_env") (r "^0.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "0wz46g659kdnbrzws509yj77rp8dkfh9wsprrjhv9m2q60mv4fd9")))

(define-public crate-shoulda_core-0.1.9 (c (n "shoulda_core") (v "0.1.9") (d (list (d (n "const_env") (r "^0.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1dp6ylay8r2pc28kz6hclm7r2xfs0840bvjp1s1sqh8fvc1xgfly")))

(define-public crate-shoulda_core-0.1.10 (c (n "shoulda_core") (v "0.1.10") (d (list (d (n "const_env") (r "^0.1.2") (d #t) (k 0)) (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1mr57l91nlnkn41hx3hjlb4bkhrw2p6y7mmi2ai8xn49yql0iq00")))

(define-public crate-shoulda_core-0.1.11 (c (n "shoulda_core") (v "0.1.11") (d (list (d (n "const_env") (r "^0.1.2") (d #t) (k 0)) (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "0bixfaai2ar89i6ijmad8bnvfahmlmzr3hf8lqvc0pgi9qql7jxi")))

(define-public crate-shoulda_core-0.1.12 (c (n "shoulda_core") (v "0.1.12") (d (list (d (n "const_env") (r "^0.1.2") (d #t) (k 0)) (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1cqjs55grljsq2sq5mdqbz9h91r9pp2fj03c0rgmngkpgszba0sh")))

(define-public crate-shoulda_core-0.1.13 (c (n "shoulda_core") (v "0.1.13") (d (list (d (n "const_env") (r "^0.1.2") (d #t) (k 0)) (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "1k4zam3vmq8xj1diy8sbmhxglrb65k5bxwmlnraa5l6mlhv060jh")))

(define-public crate-shoulda_core-0.2.0 (c (n "shoulda_core") (v "0.2.0") (d (list (d (n "const_env") (r "^0.1.2") (d #t) (k 0)) (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "05y2fszly85v473hwvhff1w7qvagzfx4ffak1ycgfzxmfbh521fa")))

(define-public crate-shoulda_core-0.2.1 (c (n "shoulda_core") (v "0.2.1") (d (list (d (n "const_env") (r "^0.1.2") (d #t) (k 0)) (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "09ayn3hmpp6b8vg1rmjna5h4gm2nfk8i4fyrpjjl7g9xvi4a8xmm")))

