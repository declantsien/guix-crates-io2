(define-module (crates-io sh ou shoulds) #:use-module (crates-io))

(define-public crate-shoulds-0.1.0 (c (n "shoulds") (v "0.1.0") (h "1n2ba6xa6xnvjy6vj5jnjzmc5sa5amjrwr5rdpx64mb3kad88yvj")))

(define-public crate-shoulds-0.1.1 (c (n "shoulds") (v "0.1.1") (h "0hxxl507jq80bpav813asgw56igx9pbp85475c83j7z58xh5610n")))

(define-public crate-shoulds-0.1.2 (c (n "shoulds") (v "0.1.2") (h "0dlvwk5mp3savn9phn9vwwh04gidwdkhcz7jyw8i7l5fhhcyp0k2")))

(define-public crate-shoulds-0.1.3 (c (n "shoulds") (v "0.1.3") (h "1lyysywc14kjnpx91z6r154ff86jgvvkn3skrbrdiyxi5r0qicsr")))

(define-public crate-shoulds-0.1.4 (c (n "shoulds") (v "0.1.4") (h "0z2z7cskysrfqv6z33h8nkz3kysb582nrc0l8b3wj9a0qja4mrrz")))

(define-public crate-shoulds-0.1.5 (c (n "shoulds") (v "0.1.5") (h "1xpmzf4mlgkx8sa0z1v9g9isfws7j03n1sv1c69nryywm518f4id")))

(define-public crate-shoulds-0.1.6 (c (n "shoulds") (v "0.1.6") (h "01kh8493z96358cs805d11r0f1cfm8pax01q9cc9azipikplpic3")))

