(define-module (crates-io sh ou shoulda_macro) #:use-module (crates-io))

(define-public crate-shoulda_macro-0.1.0 (c (n "shoulda_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1p9xbsvk4dsy043x6k23bws7wy86igasli7qrijwgq9kjz3xvsq9")))

(define-public crate-shoulda_macro-0.1.1 (c (n "shoulda_macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zwaqafqjjm5agg9wg0nzkarxflh66d22wgjn88ib9mg9nibqx3f")))

(define-public crate-shoulda_macro-0.1.2 (c (n "shoulda_macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mihnylhzmpwlx96aixc47r0g6xvixbjkyzwpw3afgwiyzazl0ym")))

(define-public crate-shoulda_macro-0.1.3 (c (n "shoulda_macro") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03axmx0064d726jf1003ybz2dk802am8xcjxrgpcss7n3pxk0yyk")))

(define-public crate-shoulda_macro-0.1.4 (c (n "shoulda_macro") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19gf4a7a7sw6jwc80mgsqc3laksmvjzh709kigmf5bhvsfg4bq70")))

(define-public crate-shoulda_macro-0.1.5 (c (n "shoulda_macro") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0l6l3by3mza1gjgzsjm5642v77nllr496d9k85yc0k33pvxiv008")))

(define-public crate-shoulda_macro-0.1.6 (c (n "shoulda_macro") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fa50lpj3vrz91lvsgps8zdr0893fbccim025xhxpx61vw8r8x6c") (f (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1.7 (c (n "shoulda_macro") (v "0.1.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vnmdf43ylrw5hk7h5rl71c1pqj4p7b36y6shwsr3v1176nnfvyf") (f (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1.8 (c (n "shoulda_macro") (v "0.1.8") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v8j1rs5ni6wr19avy1vj5n5fzcvrh84hhyv5yapcrwdm6nm3lqi") (f (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1.9 (c (n "shoulda_macro") (v "0.1.9") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d4rz3s8sxfhxcsyayyqxr81mxndp4r0ldy619sk1p9wp3fi03fg") (f (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1.10 (c (n "shoulda_macro") (v "0.1.10") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ns2ybpdspjyq5v6idrczdijxfwx9lqdmznimsn5lr05b0pz2fzv") (f (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1.11 (c (n "shoulda_macro") (v "0.1.11") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13ckang5glrhbaz1gqlpwqj65nirqz7n0il8kkjjq8xi9jm3wrxr") (f (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1.12 (c (n "shoulda_macro") (v "0.1.12") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l4zbaff6vicfd5bl2c718c9sjfqw6f7wi3054kqkky2amdaw4xj") (f (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1.13 (c (n "shoulda_macro") (v "0.1.13") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s07bvbg5vphrpkijpzsafsygml8ishhfvcjj0yafg5dpnnc9bj3") (f (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.2.0 (c (n "shoulda_macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07ajvsqc8s406d52pmdkxs7iygbl8i61hgj5c5mqgf0vlyljnknx") (f (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.2.1 (c (n "shoulda_macro") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rnvq0m0jyrd4viycwrvmr9a81dlnk4h910vpqbx0b7v8pslvqjy")))

