(define-module (crates-io sh ou shoulda) #:use-module (crates-io))

(define-public crate-shoulda-0.1.0 (c (n "shoulda") (v "0.1.0") (d (list (d (n "shoulda_core") (r "^0.1.0") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1ykyvcgbf0jhr3xy7jxym242949v7b5q8y0gl3j63d2ygafbcdd2")))

(define-public crate-shoulda-0.1.1 (c (n "shoulda") (v "0.1.1") (d (list (d (n "shoulda_core") (r "^0.1.0") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0rh0zwxxq8037ggk4hy02cx2fdclmr4j1q8mccqdprp5vmnk04zy")))

(define-public crate-shoulda-0.1.2 (c (n "shoulda") (v "0.1.2") (d (list (d (n "shoulda_core") (r "^0.1.2") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.2") (d #t) (k 0)))) (h "0xgj3k2rrbgdxybxhjfgz8sx221nacn8vf64d4pzbz51vxk9ls0f")))

(define-public crate-shoulda-0.1.3 (c (n "shoulda") (v "0.1.3") (d (list (d (n "shoulda_core") (r "^0.1.3") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.3") (d #t) (k 0)))) (h "06n2z39vx4ag795xmc6n7x6y9pdysdypys60jgqpi3a3pa0w1jbx")))

(define-public crate-shoulda-0.1.4 (c (n "shoulda") (v "0.1.4") (d (list (d (n "shoulda_core") (r "^0.1.4") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.4") (d #t) (k 0)))) (h "173cik0ykm27i6nrdhdf79x7yj8rcybrg7p2aisbfip53c8pg9hv")))

(define-public crate-shoulda-0.1.5 (c (n "shoulda") (v "0.1.5") (d (list (d (n "shoulda_core") (r "^0.1.5") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.5") (d #t) (k 0)))) (h "051i8pprp225vc8hi9crj65n4q9r23riyi1bp1mnxgfch75mpbh1")))

(define-public crate-shoulda-0.1.6 (c (n "shoulda") (v "0.1.6") (d (list (d (n "shoulda_core") (r "^0.1.6") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.6") (d #t) (k 0)))) (h "1b5cg0niycwh0bd519qqidprgayi47930sdk966gzgwhdg2nnlqc") (f (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1.7 (c (n "shoulda") (v "0.1.7") (d (list (d (n "shoulda_core") (r "^0.1.7") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.7") (d #t) (k 0)))) (h "0ad8if9329rlls5yc3zzisjsjqc18c57jghrxklq4b76diabkc8j") (f (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1.8 (c (n "shoulda") (v "0.1.8") (d (list (d (n "shoulda_core") (r "^0.1.8") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.8") (d #t) (k 0)))) (h "1p1x5pmg6v7iq2dzbipl8d4br7z69d1y4y9gvaxi5r41vyw9dz6p") (f (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1.9 (c (n "shoulda") (v "0.1.9") (d (list (d (n "shoulda_core") (r "^0.1.9") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.9") (d #t) (k 0)))) (h "0bq3pjichw5awzfnmckkk0ia7bi36d8ycb18ixp8idng5ra3nmd5") (f (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1.10 (c (n "shoulda") (v "0.1.10") (d (list (d (n "shoulda_core") (r "^0.1.9") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.9") (d #t) (k 0)))) (h "1a4jcf6flha600793n78fkspyb1ir35bzgf16vq8lqcx11b08d83") (f (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1.11 (c (n "shoulda") (v "0.1.11") (d (list (d (n "shoulda_core") (r "^0.1.11") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.11") (d #t) (k 0)))) (h "19wvz34j5ryiyn4wkjifw0886dk7z0d99bii9adxjfy91ih15k3v") (f (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1.12 (c (n "shoulda") (v "0.1.12") (d (list (d (n "shoulda_core") (r "^0.1.12") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.12") (d #t) (k 0)))) (h "1iaj1mqjn735w86dgaic1i7g4yqij7fi1xq5bs68wcn42kw3v44d") (f (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1.13 (c (n "shoulda") (v "0.1.13") (d (list (d (n "shoulda_core") (r "^0.1.13") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.1.13") (d #t) (k 0)))) (h "0zj9dql2jc15bpc4nvr7klpb95i8hqglm06xzngrxxc43ar2ybwd") (f (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.2.0 (c (n "shoulda") (v "0.2.0") (d (list (d (n "shoulda_core") (r "^0.2.0") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.2.0") (d #t) (k 0)))) (h "0v73caiccvvwcnh2mfh39y9w8qjb7krrsd9bx4ivkvcx68sydh4x") (f (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.2.1 (c (n "shoulda") (v "0.2.1") (d (list (d (n "shoulda_core") (r "^0.2.0") (d #t) (k 0)) (d (n "shoulda_macro") (r "^0.2.0") (d #t) (k 0)))) (h "01lhd2dh6wdrydlyghc1wa1lr2bd0slh00sxrhpzk5nzik0fincs")))

