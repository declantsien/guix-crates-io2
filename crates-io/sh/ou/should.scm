(define-module (crates-io sh ou should) #:use-module (crates-io))

(define-public crate-should-0.1.0 (c (n "should") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "1q4jd8f0877p7p9am8292mah8mprggcmhc0yrxz56q44yyzymn7k")))

(define-public crate-should-0.2.0 (c (n "should") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "0c34ccy4nf5vff16shw8dxdccar9ffa4b0cbq93pyczry8y76siq")))

(define-public crate-should-0.3.0 (c (n "should") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "1zp61y11yd6zxdr81zlwn7w2bpbmnn09irdr7jhd10my4p8avjj0")))

(define-public crate-should-0.4.0 (c (n "should") (v "0.4.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "14l080agh1yp6pjb658zw5cjlyg0kjlbpgbdjnhfll1xjc24qy98")))

(define-public crate-should-0.4.1 (c (n "should") (v "0.4.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1753b9hdchxb4nc9gpnl6lcq26fxqajqsmmw7l0i7b9khbnf7dyl")))

(define-public crate-should-0.4.2 (c (n "should") (v "0.4.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1jqimp8hx8m5ma8p18dgpxr5l71xjflgfr8z0hjwpf837ybb0wwa")))

