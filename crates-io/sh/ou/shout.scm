(define-module (crates-io sh ou shout) #:use-module (crates-io))

(define-public crate-shout-0.1.0 (c (n "shout") (v "0.1.0") (d (list (d (n "shout-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0i96xanfc1jkrjj3jl3fp981qpcffc1gj7vjihm7rm4viv3vynj3")))

(define-public crate-shout-0.1.1 (c (n "shout") (v "0.1.1") (d (list (d (n "shout-sys") (r "^0.1.1") (d #t) (k 0)))) (h "11n9vnzcld2kaf2yxk4q4i53y4ppyv2za0spf3iwg8w4yxldyq8h")))

(define-public crate-shout-0.1.2 (c (n "shout") (v "0.1.2") (d (list (d (n "shout-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1njp2bf1zwljpaby5ihjmlnxp5pck5jlfmzl84dmncmkzazggwfa")))

(define-public crate-shout-0.1.3 (c (n "shout") (v "0.1.3") (d (list (d (n "shout-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0fr2sd7ikggqs5h7w910n9l1yi209ryx48ax08501z2a83iz4sww")))

(define-public crate-shout-0.1.4 (c (n "shout") (v "0.1.4") (d (list (d (n "shout-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0zyxpfvp9842rm86fn1jl5f4qhfzcknq6hfwlihfnipapn0622q1")))

(define-public crate-shout-0.1.5 (c (n "shout") (v "0.1.5") (d (list (d (n "shout-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0lydxjzn878r253dnk1bhrannrqj576b0zf431b1j40k550577zp")))

(define-public crate-shout-0.1.6 (c (n "shout") (v "0.1.6") (d (list (d (n "shout-sys") (r "^0.1.1") (d #t) (k 0)))) (h "029cqzsyyahy7bv504l9lmvg45n18g3nlgs6yhfrpy2vm52yygwv")))

(define-public crate-shout-0.2.0 (c (n "shout") (v "0.2.0") (d (list (d (n "shout-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1p3xfqmyp5vnz6na3q3s2wg3p2mwibdwnbl6llsmijbm2k2iy8iy")))

(define-public crate-shout-0.2.1 (c (n "shout") (v "0.2.1") (d (list (d (n "shout-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1ixf1yb6dkvdqv2108jsjrdqxz3rl71kadrrzmpq43a761g82lns")))

