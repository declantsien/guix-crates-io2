(define-module (crates-io sh if shiftreg-spi) #:use-module (crates-io))

(define-public crate-shiftreg-spi-0.1.0 (c (n "shiftreg-spi") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1.2") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (f (quote ("std"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^1.0.0-rc.3") (d #t) (k 0)))) (h "11s972bhywfa84blx7dbqgq8lfj2z8q3cxnhiyrb4m251avlkzak")))

