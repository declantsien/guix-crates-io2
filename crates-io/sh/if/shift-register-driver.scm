(define-module (crates-io sh if shift-register-driver) #:use-module (crates-io))

(define-public crate-shift-register-driver-0.1.0 (c (n "shift-register-driver") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1a7z8qw5s1h3s304jzcf922g497p55fc5lf6s2fwfp7gildgcwza")))

(define-public crate-shift-register-driver-0.1.1 (c (n "shift-register-driver") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1998d68833x6a1fvqfvzz34hmilj0a3b6f5ala3189fi23sawwb3")))

