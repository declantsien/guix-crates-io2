(define-module (crates-io sh if shift_or_euc) #:use-module (crates-io))

(define-public crate-shift_or_euc-0.1.0 (c (n "shift_or_euc") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "136swz5csn4s5smfbbrf5yi4dnf6fp0wsx4x9fah762vd2jdwc7r")))

