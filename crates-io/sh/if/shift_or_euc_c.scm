(define-module (crates-io sh if shift_or_euc_c) #:use-module (crates-io))

(define-public crate-shift_or_euc_c-0.1.0 (c (n "shift_or_euc_c") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "shift_or_euc") (r "^0.1.0") (d #t) (k 0)))) (h "1g2sl35wabhyfsb4jjhwi52rkk1qw06bhn7gv145ri38ia6c07n8")))

