(define-module (crates-io sh if shifted_vec) #:use-module (crates-io))

(define-public crate-shifted_vec-0.1.0 (c (n "shifted_vec") (v "0.1.0") (h "14k3cy5srqr6vqfpflffgn9jq1ra4gsa5cd1gnffqbcm4kd52sd1")))

(define-public crate-shifted_vec-0.1.1 (c (n "shifted_vec") (v "0.1.1") (h "1fwsqx1n5kiqs3zy7g7i9c212grnz4wc29gpj19z0v0rkv4zp2zw")))

(define-public crate-shifted_vec-0.1.2 (c (n "shifted_vec") (v "0.1.2") (h "1i1zqs2kccp99k5dbkkymdh8l3jr7xgwkib11ph5s9qrcrldy3fy")))

