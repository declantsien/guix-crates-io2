(define-module (crates-io sh ut shuttle-warp) #:use-module (crates-io))

(define-public crate-shuttle-warp-0.12.0-rc1 (c (n "shuttle-warp") (v "0.12.0-rc1") (d (list (d (n "shuttle-runtime") (r "^0.12.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "194j8mgvabw8afsp704rx6v2p75wlvjbpyv6i89fv5mjxywnlk32")))

(define-public crate-shuttle-warp-0.12.0 (c (n "shuttle-warp") (v "0.12.0") (d (list (d (n "shuttle-runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0p99garl2smqi27ld6cfg3llviddyrmaaaqr9kknihcfiqr2zj28")))

(define-public crate-shuttle-warp-0.13.0 (c (n "shuttle-warp") (v "0.13.0") (d (list (d (n "shuttle-runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "03sghkgf58qmkkddjjfabvwidhxi5sfqbhggg28hwimr1m91bcy9")))

(define-public crate-shuttle-warp-0.14.0 (c (n "shuttle-warp") (v "0.14.0") (d (list (d (n "shuttle-runtime") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0cbhsfl394xl053h8p39iy5p2fmsvngj9hwp5hiqhqx6qchyq2yr")))

(define-public crate-shuttle-warp-0.15.0 (c (n "shuttle-warp") (v "0.15.0") (d (list (d (n "shuttle-runtime") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0qm57qn871c0sfq96xsz54k23msv3dsiz5g5afazlqcj2hafffkp")))

(define-public crate-shuttle-warp-0.16.0 (c (n "shuttle-warp") (v "0.16.0") (d (list (d (n "shuttle-runtime") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1kp7kchnf01mzssw2zarmymwv8n6py99h2l3vj2xq0jd5pw2mkkn")))

(define-public crate-shuttle-warp-0.17.0 (c (n "shuttle-warp") (v "0.17.0") (d (list (d (n "shuttle-runtime") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1xclvzvmav4gxv3dz2410989kx77q5j09fxgffxipk9xq651fp5n")))

(define-public crate-shuttle-warp-0.18.0 (c (n "shuttle-warp") (v "0.18.0") (d (list (d (n "shuttle-runtime") (r "^0.18.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1g5lx65nbdw20l855glp7v523pbqj6h0ycy4lfc6ggsxnspykf5h")))

(define-public crate-shuttle-warp-0.19.0 (c (n "shuttle-warp") (v "0.19.0") (d (list (d (n "shuttle-runtime") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1x1jgp48zjlazcjzl76k7azlyqg9yb231h41dr70ldqqny356hxf")))

(define-public crate-shuttle-warp-0.20.0 (c (n "shuttle-warp") (v "0.20.0") (d (list (d (n "shuttle-runtime") (r "^0.20.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0a84b96xi676j7qvcyf33jwlkmwg60w1ljql6gva788z143v1big")))

(define-public crate-shuttle-warp-0.21.0 (c (n "shuttle-warp") (v "0.21.0") (d (list (d (n "shuttle-runtime") (r "^0.21.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0pz6hqzhvbzc7gflrn4cbvi37w80nkdnfvkpkdqc0gssw1nfi6i3")))

(define-public crate-shuttle-warp-0.22.0 (c (n "shuttle-warp") (v "0.22.0") (d (list (d (n "shuttle-runtime") (r "^0.22.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "087a0jh00i3w8093gyvrpr3lgvif3x1m3cik632k1hz54rsk1nn4")))

(define-public crate-shuttle-warp-0.23.0 (c (n "shuttle-warp") (v "0.23.0") (d (list (d (n "shuttle-runtime") (r "^0.23.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1hs2wx9m83mzx8knd03spmlhq6azisibbrmvk2q6v8xysa9ny27l")))

(define-public crate-shuttle-warp-0.24.0 (c (n "shuttle-warp") (v "0.24.0") (d (list (d (n "shuttle-runtime") (r "^0.24.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0r253zqgwvplrbml368fi82cp484y2ikllzgl96cb8skv5xwmb89")))

(define-public crate-shuttle-warp-0.25.0 (c (n "shuttle-warp") (v "0.25.0") (d (list (d (n "shuttle-runtime") (r "^0.25.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1d70wkf3vsdfi79m53fc8hm07iyqqyqaxfb06gkvl2vr7q3lz728")))

(define-public crate-shuttle-warp-0.26.0 (c (n "shuttle-warp") (v "0.26.0") (d (list (d (n "shuttle-runtime") (r "^0.26.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0m95lq6paivwiay1ncs6933hni3dhfxh1ql32z42i433q3wbkvcb")))

(define-public crate-shuttle-warp-0.27.0 (c (n "shuttle-warp") (v "0.27.0") (d (list (d (n "shuttle-runtime") (r "^0.27.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1jxjwvxlkf57qa12va8yab22h2q1hbbgqc08ig6fncdiy48hmp74")))

(define-public crate-shuttle-warp-0.28.0 (c (n "shuttle-warp") (v "0.28.0") (d (list (d (n "shuttle-runtime") (r "^0.28.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0mnb0qdmam4d49jv3aygn5da1agf0dvsy8n6bmy89lhpcbfwicl8")))

(define-public crate-shuttle-warp-0.29.0 (c (n "shuttle-warp") (v "0.29.0") (d (list (d (n "shuttle-runtime") (r "^0.29.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1yqrsqzdr4hyam349wjm2jqb6j3kqjkb96jbwgvg1754ql08gbr0")))

(define-public crate-shuttle-warp-0.30.0 (c (n "shuttle-warp") (v "0.30.0") (d (list (d (n "shuttle-runtime") (r "^0.30.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0gdskpn5vblv1zkrqj2x88lvcrz90ny2bz39bv99mrs78cpvaiiw")))

(define-public crate-shuttle-warp-0.30.1 (c (n "shuttle-warp") (v "0.30.1") (d (list (d (n "shuttle-runtime") (r "^0.30.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1drkwq7hyhd7l7zlqp4vhcyl3qx7ar8zchcx9igbrp2nc524lm88")))

(define-public crate-shuttle-warp-0.31.0 (c (n "shuttle-warp") (v "0.31.0") (d (list (d (n "shuttle-runtime") (r "^0.31.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "198wbcyqgha6bf6y07jhyxa4gn8w504xqc7r9yjv86aggzxdd30s")))

(define-public crate-shuttle-warp-0.32.0 (c (n "shuttle-warp") (v "0.32.0") (d (list (d (n "shuttle-runtime") (r "^0.32.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "12i093ap7nn308xwgfn5wyiqmhplswwawzyi7qpcxc0asr5k3qhg")))

(define-public crate-shuttle-warp-0.33.0 (c (n "shuttle-warp") (v "0.33.0") (d (list (d (n "shuttle-runtime") (r "^0.33.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1zylrbmbxvaj2rzs3hk9867pas305szi66rwdmwpsbbhsp13rx7p")))

(define-public crate-shuttle-warp-0.34.0 (c (n "shuttle-warp") (v "0.34.0") (d (list (d (n "shuttle-runtime") (r "^0.34.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "07bism2jvnm7dw9wwg29rwdddhs7gy8gkvka8m9d0c1f007c7kyx")))

(define-public crate-shuttle-warp-0.34.1 (c (n "shuttle-warp") (v "0.34.1") (d (list (d (n "shuttle-runtime") (r "^0.34.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "16ycqwbkb2fll5bmj1qlap822bf1q486hjy0p06qyy9hd2qyknky")))

(define-public crate-shuttle-warp-0.35.0 (c (n "shuttle-warp") (v "0.35.0") (d (list (d (n "shuttle-runtime") (r "^0.35.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0rdl8ad7wzp3s6nywr5lfc6032rd4rwv1ay19299wcnrayh5fgs2")))

(define-public crate-shuttle-warp-0.35.1 (c (n "shuttle-warp") (v "0.35.1") (d (list (d (n "shuttle-runtime") (r "^0.35.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1q7pn7x8ww6d5xdyibnv9zch37anx0qira18y2g7jyfdq4hgqbxh")))

(define-public crate-shuttle-warp-0.35.2 (c (n "shuttle-warp") (v "0.35.2") (d (list (d (n "shuttle-runtime") (r "^0.35.2") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "05h5nrdy6333c91l296xk499chj2v1fps7kdmvi04kf1s51fwy63")))

(define-public crate-shuttle-warp-0.36.0 (c (n "shuttle-warp") (v "0.36.0") (d (list (d (n "shuttle-runtime") (r "^0.36.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0cf3ldqjvc4917qy52d0dz552mqlqd4abh6zz2zdqn89dwynmkpq")))

(define-public crate-shuttle-warp-0.37.0 (c (n "shuttle-warp") (v "0.37.0") (d (list (d (n "shuttle-runtime") (r "^0.37.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "10wk0mgldlyy5hd1hxwfnvbqvaym2glw7m37f3400d9bab0c6f7s")))

(define-public crate-shuttle-warp-0.38.0 (c (n "shuttle-warp") (v "0.38.0") (d (list (d (n "shuttle-runtime") (r "^0.38.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "11a5j22q432bpssnfy5zn2x79sga47hl79r0cnkrssxfc50w7q8q")))

(define-public crate-shuttle-warp-0.39.0 (c (n "shuttle-warp") (v "0.39.0") (d (list (d (n "shuttle-runtime") (r "^0.39.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1wk68zc0pg2w8jivy063v5sjcyrp6lxp7s4gizg5rci1r6a0sqd9")))

(define-public crate-shuttle-warp-0.40.0 (c (n "shuttle-warp") (v "0.40.0") (d (list (d (n "shuttle-runtime") (r "^0.40.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1dldrg0spq8zcfxcmq02zxkhfhz9qpbcfvdkhvxi301sajzaw50j")))

(define-public crate-shuttle-warp-0.41.0 (c (n "shuttle-warp") (v "0.41.0") (d (list (d (n "shuttle-runtime") (r "^0.41.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0isq8acrwcw2zvvyhv2fwx39c6hwa6ikbllp576n7qwck740bv9f")))

(define-public crate-shuttle-warp-0.42.0 (c (n "shuttle-warp") (v "0.42.0") (d (list (d (n "shuttle-runtime") (r "^0.42.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0pcscgn86ab656fx4idfb0s0k0l9wbcgmv38mgbgk1kildhgmvii")))

(define-public crate-shuttle-warp-0.43.0 (c (n "shuttle-warp") (v "0.43.0") (d (list (d (n "shuttle-runtime") (r "^0.43.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1jvqqfcr1086zfakm33kbdpyvin876pklkdg4c0laz54b7rh7v51")))

(define-public crate-shuttle-warp-0.44.0 (c (n "shuttle-warp") (v "0.44.0") (d (list (d (n "shuttle-runtime") (r "^0.44.0") (k 0)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1x8lkl5lrgz49px1kjhfpby331162rnh4zd51hwrvrlg0bff223d")))

(define-public crate-shuttle-warp-0.45.0 (c (n "shuttle-warp") (v "0.45.0") (d (list (d (n "shuttle-runtime") (r "^0.45.0") (k 0)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "114dgzy5k80yamd0qb8f54jy4m7n02vcq9jd7rkrknll8gympmm1")))

