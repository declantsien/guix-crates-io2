(define-module (crates-io sh ut shuttle-axum) #:use-module (crates-io))

(define-public crate-shuttle-axum-0.12.0-rc1 (c (n "shuttle-axum") (v "0.12.0-rc1") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "025i3vy70cbs845hqskmynw1dbssc2d4zxia8sspdqna15kqx920")))

(define-public crate-shuttle-axum-0.12.0 (c (n "shuttle-axum") (v "0.12.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0kp4442n2gi2p10827bh8aisaxzhmggr4kan48z647vxq7wamk4i")))

(define-public crate-shuttle-axum-0.13.0 (c (n "shuttle-axum") (v "0.13.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0qzcip6ckkyzfsnlfwasxy8py8pab5mr90cd5dsgkqiz7lwc1g1v")))

(define-public crate-shuttle-axum-0.14.0 (c (n "shuttle-axum") (v "0.14.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0claxmcvl6dx04wi38k7r6pfyrfqzzp2na1464195b8mbypf7g07")))

(define-public crate-shuttle-axum-0.15.0 (c (n "shuttle-axum") (v "0.15.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1aifxa00yhzp6h54z9izgm3g6dz37p6k3w01nin6gjsr0k6f8qyb")))

(define-public crate-shuttle-axum-0.16.0 (c (n "shuttle-axum") (v "0.16.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1klxy7chfkg1xrd9rd7d1l2n5i0rs3d45x8qd9ia46s6jyxbyxw0")))

(define-public crate-shuttle-axum-0.17.0 (c (n "shuttle-axum") (v "0.17.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0m2vj7wr64vl2lf36hq0szd970xabh9r6qrv57wm4ky2rs4kfrr6")))

(define-public crate-shuttle-axum-0.18.0 (c (n "shuttle-axum") (v "0.18.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.18.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "09f7ir6x9xs2pzisf9pmn46xgb7pz7l8b0qgy4h8haz01l3ml7w5")))

(define-public crate-shuttle-axum-0.19.0 (c (n "shuttle-axum") (v "0.19.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1vn2p6whkzzk2mg7irifa0xy9h76c122bz0q7c4j8mg2b0hap7ri")))

(define-public crate-shuttle-axum-0.20.0 (c (n "shuttle-axum") (v "0.20.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.20.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "00r9bk5pc3jas4ghbmh66dgv95fr6v466v09ydnxw7f95kjb0s0h")))

(define-public crate-shuttle-axum-0.21.0 (c (n "shuttle-axum") (v "0.21.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.21.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0r4zfl0b4wnn9fr7vvjjr6val0bpqj8jdcbiq4k064p39d8gg93b")))

(define-public crate-shuttle-axum-0.22.0 (c (n "shuttle-axum") (v "0.22.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.22.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1mb5p9081f5rrfibsrz38df6zh21kdc167pmzk8cxdvx4imd9h2b")))

(define-public crate-shuttle-axum-0.23.0 (c (n "shuttle-axum") (v "0.23.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.23.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0z8hhbls32ci95n31y0hca9d1fmymjm82h1rsys0jn6956drafx2")))

(define-public crate-shuttle-axum-0.24.0 (c (n "shuttle-axum") (v "0.24.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.24.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "01rh876bk9dbrb6pqdxq92sbk00w5qm01hyiysddnr1yan0vn8qg")))

(define-public crate-shuttle-axum-0.25.0 (c (n "shuttle-axum") (v "0.25.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.25.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0b3dp5hkwx6l8m5bir6l1dqzhrhh4fijixja3hq48k613yz0lgsz")))

(define-public crate-shuttle-axum-0.26.0 (c (n "shuttle-axum") (v "0.26.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.26.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0nlknl8abgy8szj0x3zahh3k3y32yiq852ffx8xmkp96mmmvk7sh")))

(define-public crate-shuttle-axum-0.27.0 (c (n "shuttle-axum") (v "0.27.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.27.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0mchjmyp15rkxrlr141n6jd18nyq0369b64nih0pagxjin62jpkc")))

(define-public crate-shuttle-axum-0.28.0 (c (n "shuttle-axum") (v "0.28.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.28.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1agyzpwsf6r3f7c1fn6jln7035mi5qgqvqp0ymd0bwh9f2zm07ag")))

(define-public crate-shuttle-axum-0.29.0 (c (n "shuttle-axum") (v "0.29.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.29.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1p27hikycqsmw7qnvnm5k7ap56zw1ibays44nq3c27k8kjm2mvav")))

(define-public crate-shuttle-axum-0.30.0 (c (n "shuttle-axum") (v "0.30.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0nz6985klv3wgxpf37b5cxn28889mcba5iqh3w9vvmg4m18cw70a")))

(define-public crate-shuttle-axum-0.30.1 (c (n "shuttle-axum") (v "0.30.1") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0pzpmrqrwllifkffk1gd34mifpc1hjnj0m6v7vl8sqxjfw0gy133")))

(define-public crate-shuttle-axum-0.31.0 (c (n "shuttle-axum") (v "0.31.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.31.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0l8lfrcy8jv5ncawvj2jy93anmv7z3lg2xy80s361nn8zmz91rgn")))

(define-public crate-shuttle-axum-0.32.0 (c (n "shuttle-axum") (v "0.32.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.32.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0r76zzmic7gdvbbfiimbh64j2ig713kpvgnhw97jbzi1sdgyav6j")))

(define-public crate-shuttle-axum-0.33.0 (c (n "shuttle-axum") (v "0.33.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.33.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1hd4a9fvv5b7llazrnar4zd1n1qhl1g063m426ajszr0diwpqv1w")))

(define-public crate-shuttle-axum-0.34.0 (c (n "shuttle-axum") (v "0.34.0") (d (list (d (n "axum") (r "^0.6.10") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.34.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "09z8k9p9cqa27svq5ayqi5ygwcvlv22f1dv9jhp47ypksaj9ybm4")))

(define-public crate-shuttle-axum-0.34.1 (c (n "shuttle-axum") (v "0.34.1") (d (list (d (n "axum") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "axum-0-7") (r "^0.7.1") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.34.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0vla0r8iwpz3f844xxd8lwdc5xw59vrfcbvlxs7xaw1frldiihn9") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-7" "dep:axum-0-7"))))))

(define-public crate-shuttle-axum-0.35.0 (c (n "shuttle-axum") (v "0.35.0") (d (list (d (n "axum") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "axum-0-7") (r "^0.7.1") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.35.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1jmy9a7sk2jrz8y9vhfa1dnp8i7gkm0zgl43zq702qx3pjzbalyj") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-7" "dep:axum-0-7"))))))

(define-public crate-shuttle-axum-0.35.1 (c (n "shuttle-axum") (v "0.35.1") (d (list (d (n "axum") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "axum-0-7") (r "^0.7.1") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.35.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1hwgi2yg1q7amivbxqcnnpdz29y1i72ib4dhp0303c7fmvcwha3x") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-7" "dep:axum-0-7"))))))

(define-public crate-shuttle-axum-0.35.2 (c (n "shuttle-axum") (v "0.35.2") (d (list (d (n "axum") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "axum-0-7") (r "^0.7.1") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.35.2") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "18g3d715hn419lgcjmml47yd0kin1a9s7hvq81jpfdj9wsc232lb") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-7" "dep:axum-0-7"))))))

(define-public crate-shuttle-axum-0.36.0 (c (n "shuttle-axum") (v "0.36.0") (d (list (d (n "axum") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "axum-0-6") (r "^0.6.13") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.36.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1lgbydgrd4aiy5z71fxfp2h8q0f8dz6jgv05j95h8wsgp607c0xi") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-6" "dep:axum-0-6"))))))

(define-public crate-shuttle-axum-0.37.0 (c (n "shuttle-axum") (v "0.37.0") (d (list (d (n "axum") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "axum-0-6") (r "^0.6.13") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.37.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "08gkc7macfd7p99zz1ki1p9h1ri4jb01nmk8f5j7caavb981psmr") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-6" "dep:axum-0-6"))))))

(define-public crate-shuttle-axum-0.38.0 (c (n "shuttle-axum") (v "0.38.0") (d (list (d (n "axum") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "axum-0-6") (r "^0.6.13") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.38.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0bxz9mc4lng2wbfb2p62qsh7vqw8fvzz3rw8kns2mligm22yahia") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-6" "dep:axum-0-6"))))))

(define-public crate-shuttle-axum-0.39.0 (c (n "shuttle-axum") (v "0.39.0") (d (list (d (n "axum") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "axum-0-6") (r "^0.6.13") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.39.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "13yxc0jp18p6vqv96k3p87myd83x1pznmdahwrlsiwh43vqjcid1") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-6" "dep:axum-0-6"))))))

(define-public crate-shuttle-axum-0.40.0 (c (n "shuttle-axum") (v "0.40.0") (d (list (d (n "axum") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "axum-0-6") (r "^0.6.13") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.40.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "04h94q0rvq8kw8nxb6ch211217yh5ywhwcxd9pb0y3ivka941kdv") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-6" "dep:axum-0-6"))))))

(define-public crate-shuttle-axum-0.41.0 (c (n "shuttle-axum") (v "0.41.0") (d (list (d (n "axum") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "axum-0-6") (r "^0.6.13") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.41.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "121y92h028dd96v2r3vwfnhqphfccxnp5mw48s78iid0rlvhaxab") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-6" "dep:axum-0-6"))))))

(define-public crate-shuttle-axum-0.42.0 (c (n "shuttle-axum") (v "0.42.0") (d (list (d (n "axum") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "axum-0-6") (r "^0.6.13") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.42.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0xibsnh1h7j11a85jz7r44vfp4kmpv4arabbv5v8hc66vrmdmdcj") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-6" "dep:axum-0-6"))))))

(define-public crate-shuttle-axum-0.43.0 (c (n "shuttle-axum") (v "0.43.0") (d (list (d (n "axum") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "axum-0-6") (r "^0.6.13") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.43.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "169p0a69pxdw542wv1mqvi1644ifsdrq4fk7kb66bwp20xgdm66d") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-6" "dep:axum-0-6"))))))

(define-public crate-shuttle-axum-0.44.0 (c (n "shuttle-axum") (v "0.44.0") (d (list (d (n "axum") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "axum-0-6") (r "^0.6.13") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.44.0") (k 0)))) (h "0p7r18bv0ircj57grwdf3kb9lkxm4jp3n41mc230x9js5vw6jd1r") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-6" "dep:axum-0-6"))))))

(define-public crate-shuttle-axum-0.45.0 (c (n "shuttle-axum") (v "0.45.0") (d (list (d (n "axum") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "axum-0-6") (r "^0.6.13") (o #t) (d #t) (k 0) (p "axum")) (d (n "shuttle-runtime") (r "^0.45.0") (k 0)))) (h "1030gqz8iymdsq7pr9b5wlm52dsqx3hm4amib6c20qff2rflchif") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum-0-6" "dep:axum-0-6"))))))

