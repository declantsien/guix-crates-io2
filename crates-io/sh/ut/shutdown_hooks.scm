(define-module (crates-io sh ut shutdown_hooks) #:use-module (crates-io))

(define-public crate-shutdown_hooks-0.0.0 (c (n "shutdown_hooks") (v "0.0.0") (h "1hjfl4c3928k9yqci1057y8h21s4hi5x4fhc0h5613qvddyv852r")))

(define-public crate-shutdown_hooks-0.0.1 (c (n "shutdown_hooks") (v "0.0.1") (h "029237qd2472py1kiyl56xf0ns25vzmzay9mixj3d20c6rwy0x08")))

(define-public crate-shutdown_hooks-0.0.2 (c (n "shutdown_hooks") (v "0.0.2") (h "1xhbf7p72kip6r73bfyzsv8q9wrrqlrnb6yb26hli59n08mf0a2i")))

(define-public crate-shutdown_hooks-0.0.3 (c (n "shutdown_hooks") (v "0.0.3") (h "1ynvh4mh24wld2lrvmy1i4wklwyklh7291abwl0ihxkp721b786k")))

(define-public crate-shutdown_hooks-0.1.0 (c (n "shutdown_hooks") (v "0.1.0") (h "0igsvfzgglsc75ankmdpa2ysqccrlrdkjvwrjaf424y9pvnssmv0")))

