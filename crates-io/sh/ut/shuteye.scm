(define-module (crates-io sh ut shuteye) #:use-module (crates-io))

(define-public crate-shuteye-0.0.1 (c (n "shuteye") (v "0.0.1") (h "1ap4x7w1n0dfza01gl7g8hbb8v192l6q27wjhg0glncww09dqrhj")))

(define-public crate-shuteye-0.0.2 (c (n "shuteye") (v "0.0.2") (h "0n4j13gd74mry704klgcmd27vq3injpq2ml8vljizffvvk573hsm")))

(define-public crate-shuteye-0.0.3 (c (n "shuteye") (v "0.0.3") (h "1y8qi4592ar9hhd5hxywdbsc6f35dl5s046wimp5nh0ymav3j27v")))

(define-public crate-shuteye-0.0.4 (c (n "shuteye") (v "0.0.4") (h "01hkhwa8xq4xkaj7a4bsd1iwj28s0jvn2sc0j03i6xx7hix9lb4y")))

(define-public crate-shuteye-0.0.5 (c (n "shuteye") (v "0.0.5") (h "0wknkha3g0wrlawxwpbqp0r5rakwl99j1ir8k1my8sq7dkgwx7b3")))

(define-public crate-shuteye-0.1.0 (c (n "shuteye") (v "0.1.0") (h "0vrj22v1c9fyb8w8wprb5av8hgl8s6z2arq2dvd1ymrnfvl2jmzw")))

(define-public crate-shuteye-0.1.1 (c (n "shuteye") (v "0.1.1") (h "094nm2jpby3c624zyfbc67vzdwp1jgdm2lwg65g6r6c4shndia9v")))

(define-public crate-shuteye-0.2.0 (c (n "shuteye") (v "0.2.0") (h "0wh1yclczzlg2fqri55vv5210r6irhvz27r8y1px1bdsj6403big")))

(define-public crate-shuteye-0.3.0 (c (n "shuteye") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1mqc7lrfsqpyskijr1gfqf1xinz3lfm3g8syykiz913f1va0zp12")))

(define-public crate-shuteye-0.3.1 (c (n "shuteye") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "17piww5s65mpq3250f0ni37dpybfhm94nbav4vzrzg1yjq61xkqy")))

(define-public crate-shuteye-0.3.2 (c (n "shuteye") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1z4fq0z31ng9pqc6fg14x46alzqh14807qxqb86q0wdiw19z9r1y")))

(define-public crate-shuteye-0.3.3 (c (n "shuteye") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1ksjj99l3h93sjv545n406d98968d2r2872m9a2yl8gbyllp58ic")))

