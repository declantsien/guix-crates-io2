(define-module (crates-io sh ut shutdown-barrier) #:use-module (crates-io))

(define-public crate-shutdown-barrier-0.1.0 (c (n "shutdown-barrier") (v "0.1.0") (d (list (d (n "loom") (r "^0.5.6") (d #t) (t "cfg(loom)") (k 0)))) (h "1i2c7pc8f2w80bz0il45idwl58vay9p9pyfbmq88fz298dcgia76") (y #t)))

(define-public crate-shutdown-barrier-1.0.0 (c (n "shutdown-barrier") (v "1.0.0") (h "018ck5vlfycyzlgks8lhdgf9r2mffalkjfpiwc0nljyswsadqby1") (y #t)))

(define-public crate-shutdown-barrier-2.0.0 (c (n "shutdown-barrier") (v "2.0.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1ib5zvwl467pl4j6vrz9wbhwwa8fs8n37rz5hzg9gh3s9h0maa7n") (y #t)))

(define-public crate-shutdown-barrier-3.0.0 (c (n "shutdown-barrier") (v "3.0.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1890mwpkw3qjhjfw0q94k4hacm0kv75bwx73yi8xfynn00cl3gdg") (y #t)))

