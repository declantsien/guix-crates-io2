(define-module (crates-io sh ut shuttle-tide) #:use-module (crates-io))

(define-public crate-shuttle-tide-0.12.0-rc1 (c (n "shuttle-tide") (v "0.12.0-rc1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0-rc1") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1jj2bsx234vdf2zdd0fwj8kyahcy9kcw9s8zbdg7r03hr40wy8p2")))

(define-public crate-shuttle-tide-0.12.0 (c (n "shuttle-tide") (v "0.12.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "19apza2ji15xzqlvzmghicmx678j49n7pxy508y23j34dn2ls67h")))

(define-public crate-shuttle-tide-0.13.0 (c (n "shuttle-tide") (v "0.13.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1cps3n82h54xnbr6y4a51agqkl0x27imavd2gghhw76dd79knhpr")))

(define-public crate-shuttle-tide-0.14.0 (c (n "shuttle-tide") (v "0.14.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.14.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0p68nizzidq9skykgjvzds28rjddak5zaq1mabqvigy91jrbnffp")))

(define-public crate-shuttle-tide-0.15.0 (c (n "shuttle-tide") (v "0.15.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.15.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0976zb87qmjg73257jx5xqk6xg6yp2wrkv7bqz4dy1ghkdkfgaks")))

(define-public crate-shuttle-tide-0.16.0 (c (n "shuttle-tide") (v "0.16.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.16.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "155vg54ad66f2k59bwfqi53py3rpg80ayvd0zwb0vd55ss07rwvj")))

(define-public crate-shuttle-tide-0.17.0 (c (n "shuttle-tide") (v "0.17.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.17.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0wfyljnw2a1ccq0hdl4y4ij4fl14vbrfw184qf57wvbfcxwdr2h9")))

(define-public crate-shuttle-tide-0.18.0 (c (n "shuttle-tide") (v "0.18.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.18.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0jagz6zxssy50pqqzzr3fz2kiicg1cmd825xr8v809nsw68pc2v8")))

(define-public crate-shuttle-tide-0.19.0 (c (n "shuttle-tide") (v "0.19.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.19.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0i3sq5ykmvrf7ywjxpslqzjwgc3c7ypzvsbhplj33sqxy2k6lvql")))

(define-public crate-shuttle-tide-0.20.0 (c (n "shuttle-tide") (v "0.20.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.20.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0p8r6fpr84zwgxrqswfs3kqmfw2q0b1z3xq5hcc0lqn5kf0balmf")))

(define-public crate-shuttle-tide-0.21.0 (c (n "shuttle-tide") (v "0.21.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.21.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1y1rfbgxhsscpyqrmvfmzg60i4jiq9sjacyr4iys3gl36r6y6xpw")))

(define-public crate-shuttle-tide-0.22.0 (c (n "shuttle-tide") (v "0.22.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.22.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0p5rryz72x48kf780vd52rbpqfay44m4635835jiivyjr6dq6kq4")))

(define-public crate-shuttle-tide-0.23.0 (c (n "shuttle-tide") (v "0.23.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.23.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1i98vvfg1qw19w470h5knwrhlxibjmcwsxbh0aflhqhq20bd80bx")))

(define-public crate-shuttle-tide-0.24.0 (c (n "shuttle-tide") (v "0.24.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.24.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1n1b0rm7kpgw5rxwajlx00pilcqj2b8hqkh8hmlj53nk4c0d7p1l")))

(define-public crate-shuttle-tide-0.25.0 (c (n "shuttle-tide") (v "0.25.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.25.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "16rhdwijmacvcynr2hk0pf4z7jf18x0wm3mdpk8abqzlzpd6lcmh")))

(define-public crate-shuttle-tide-0.26.0 (c (n "shuttle-tide") (v "0.26.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.26.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "17qc7xwj9l9gwa2b3bj5frlxy56bsyg31pgbskyzzwgzdw08045n")))

(define-public crate-shuttle-tide-0.27.0 (c (n "shuttle-tide") (v "0.27.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.27.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0iqr5nb71bzzvgkiafyg6qli0yh73iih57najd7rl003bhnflkzn")))

(define-public crate-shuttle-tide-0.28.0 (c (n "shuttle-tide") (v "0.28.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.28.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1zry0y5ksxxrw70gq6pwid6rwlha54lhjv1wdbj86n7lvk8pf084")))

(define-public crate-shuttle-tide-0.29.0 (c (n "shuttle-tide") (v "0.29.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.29.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "13abw7vakn05yb5fx70nz9r087l36agbdfy9nhfflhv2532kz3g4")))

(define-public crate-shuttle-tide-0.30.0 (c (n "shuttle-tide") (v "0.30.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "08iwqy9raqwks4dmamhx5gmpjczlbv42zh04cf6z7s8drchjqbii")))

(define-public crate-shuttle-tide-0.30.1 (c (n "shuttle-tide") (v "0.30.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.1") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1n34isq5hgsqbsq9dypmbyyis1bqy7rvadya8hcv85gjrh4pqbkm")))

(define-public crate-shuttle-tide-0.31.0 (c (n "shuttle-tide") (v "0.31.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.31.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0ffczlzsyn07p09f16kp81vhbs9872240b3b5xfrk12y2hy52v9j")))

(define-public crate-shuttle-tide-0.32.0 (c (n "shuttle-tide") (v "0.32.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.32.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "10hyvjadxwdhjm4bbc43z42fw2gwkyq47v9ckxfp8zryza35s1fi")))

(define-public crate-shuttle-tide-0.33.0 (c (n "shuttle-tide") (v "0.33.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.33.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0sbhkvzzhz9f4pfixsw0gmcqx4jjy4ydw95g0kmqgdlv3ihpwgql")))

(define-public crate-shuttle-tide-0.34.0 (c (n "shuttle-tide") (v "0.34.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.34.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0ncy1jnjji9frhdkyxgn21z99jijdi6s0zd6af8rlw7bly68m57b")))

(define-public crate-shuttle-tide-0.34.1 (c (n "shuttle-tide") (v "0.34.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.34.1") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0dg8qap1c2dscrj4sdrgmxj3lfhfggisrn8389mlgjwsj3rj75mi")))

(define-public crate-shuttle-tide-0.35.0 (c (n "shuttle-tide") (v "0.35.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1zag0vz6s06n075yagdfwb8hbwb9lb7lx0cijkbyg382xf850p6p")))

(define-public crate-shuttle-tide-0.35.1 (c (n "shuttle-tide") (v "0.35.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.1") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1cx93ldqib6d7kb7qwrrzrz3sb02m3g2c7qgvzwsh418lh5n6a6m")))

(define-public crate-shuttle-tide-0.35.2 (c (n "shuttle-tide") (v "0.35.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.2") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1wnh6xg57ggz90axq06ah7kyfi1cmzzdj73r57fbmcj5barrfr3y")))

(define-public crate-shuttle-tide-0.36.0 (c (n "shuttle-tide") (v "0.36.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.36.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1qyvnw06rxidhwvgj1i3mqsi9j38x08mnpy0h7d1s8xv6gcgv8xm")))

(define-public crate-shuttle-tide-0.37.0 (c (n "shuttle-tide") (v "0.37.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.37.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0n2q16xpb7llbynhyvz9m4sqnpf0hkd9n8656fzzrya9i6hf2y59")))

(define-public crate-shuttle-tide-0.38.0 (c (n "shuttle-tide") (v "0.38.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.38.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1jlpwfv7dfdf09xgnznnxsz636f5nxs2yky4mm6szkrlw67ax4pq")))

(define-public crate-shuttle-tide-0.39.0 (c (n "shuttle-tide") (v "0.39.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.39.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "16grwjhyar93m9skxi4qf26rdynzdfcpj0mfdvcknksdy8xy93iv")))

(define-public crate-shuttle-tide-0.40.0 (c (n "shuttle-tide") (v "0.40.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.40.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0wc5i32r3bd4fmj3hfdbgbp6z5mmva72mq214kmxkw3amd7f8qw3")))

(define-public crate-shuttle-tide-0.41.0 (c (n "shuttle-tide") (v "0.41.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.41.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0yclzv7j6hkycinqhlad53lsm2qg4dkjhv4m601vr416gfvdjxvz")))

(define-public crate-shuttle-tide-0.42.0 (c (n "shuttle-tide") (v "0.42.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.42.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "08sr6j3hpfki1dfd2gl2llfszlrf38rz4jw42v59sfsq29qzpgy3")))

(define-public crate-shuttle-tide-0.43.0 (c (n "shuttle-tide") (v "0.43.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.43.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "17gx8495dn38anfpzl0pyisrf16m8f52lqm0vzrp9c4pa7k6c9kp")))

(define-public crate-shuttle-tide-0.44.0 (c (n "shuttle-tide") (v "0.44.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.44.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "06axggw7yjfz6ddw85p7rrx373rsb298qjvbac4csj15m2d94p66")))

(define-public crate-shuttle-tide-0.45.0 (c (n "shuttle-tide") (v "0.45.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.45.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "1bi4ps18v1jfvmfhqzv3z6yzq8fn4x8b6qjh4mcd4xdad2hpz1vz")))

