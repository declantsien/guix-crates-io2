(define-module (crates-io sh ut shut_up) #:use-module (crates-io))

(define-public crate-shut_up-0.1.0 (c (n "shut_up") (v "0.1.0") (h "0bh37m3cay3yax4x0xjbss4vvw4by8piha55ibszny5pi08bc0x0")))

(define-public crate-shut_up-0.1.1 (c (n "shut_up") (v "0.1.1") (h "0yji8y0gk4wc592zv68dcybsv9llz0kh18bjwbpmyy6q32gdy9kw")))

