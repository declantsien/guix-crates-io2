(define-module (crates-io sh ut shuttle-secrets) #:use-module (crates-io))

(define-public crate-shuttle-secrets-0.6.0 (c (n "shuttle-secrets") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.6.0") (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt"))) (d #t) (k 0)))) (h "006vsi6m1whrvyaxqaswgys53i9ydkrb6qfwfmpmdrxm2ml6gmgr")))

(define-public crate-shuttle-secrets-0.7.0 (c (n "shuttle-secrets") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.7.0") (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt"))) (d #t) (k 0)))) (h "0m0j5fn7mm7sd072aw5hkbs30fxrkcbksl0vhlba731k9s9v20qg")))

(define-public crate-shuttle-secrets-0.7.1 (c (n "shuttle-secrets") (v "0.7.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.7.1") (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt"))) (d #t) (k 0)))) (h "07c7qlhp5sd50g07mzpy7vbx62q2kmq27kznna3c2wdbmvs9h79d")))

(define-public crate-shuttle-secrets-0.7.2 (c (n "shuttle-secrets") (v "0.7.2") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.7.2") (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt"))) (d #t) (k 0)))) (h "0c9zl0aasd3scb2hslpfyv7cbb31ismk9r5br3a335aqzg86zagz")))

(define-public crate-shuttle-secrets-0.8.0 (c (n "shuttle-secrets") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.8.0") (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt"))) (d #t) (k 0)))) (h "1d4ggsscsarh4ahmwys39m0k0qrg5klypmcc6gslnsll4vbky64i")))

(define-public crate-shuttle-secrets-0.9.0 (c (n "shuttle-secrets") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.9.0") (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt"))) (d #t) (k 0)))) (h "1smcfl19rk4m7c6bdivzagi3f66rxjp038dan6vab0yahczldzbj")))

(define-public crate-shuttle-secrets-0.10.0 (c (n "shuttle-secrets") (v "0.10.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.10.0") (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt"))) (d #t) (k 0)))) (h "10q1jlhn4vf08xis209ak9wrdg0j9fzs0gczd25gk62ccd6ijy3d")))

(define-public crate-shuttle-secrets-0.11.0 (c (n "shuttle-secrets") (v "0.11.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.11.0") (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt"))) (d #t) (k 0)))) (h "1g7z92j2qsqvm362i4byshs9gsv5q9ip42la8xpvzd8dggdq79cc")))

(define-public crate-shuttle-secrets-0.12.0-rc1 (c (n "shuttle-secrets") (v "0.12.0-rc1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.12.0-rc1") (k 0)))) (h "16w7kyihgvq7dprphrngh4nz2dzh9l0brj9mxcgn8jvzrd87xcvk")))

(define-public crate-shuttle-secrets-0.12.0 (c (n "shuttle-secrets") (v "0.12.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.12.0") (k 0)))) (h "0x3c16lwzy7dx1xk4s8mvqfm2s74rysjiwq14rsc3na9m4r0qrqj")))

(define-public crate-shuttle-secrets-0.13.0 (c (n "shuttle-secrets") (v "0.13.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.13.0") (k 0)))) (h "1g5if0rs7ac0d2634afks539kykkzpgrg037pbdanzwz2fi5yrlp")))

(define-public crate-shuttle-secrets-0.14.0 (c (n "shuttle-secrets") (v "0.14.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.14.0") (k 0)))) (h "0j2gygs6iwyawvsvs0xvygbij7spizqrakn7csgc9m5nnwqw0fdp")))

(define-public crate-shuttle-secrets-0.15.0 (c (n "shuttle-secrets") (v "0.15.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.15.0") (k 0)))) (h "14a0vwz51cf0il3rq33w5q342dm9621iy37axipcp8vxj780c6kn")))

(define-public crate-shuttle-secrets-0.16.0 (c (n "shuttle-secrets") (v "0.16.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.16.0") (k 0)))) (h "0wd0b4fpbsacjxnp5j8hp1z2lnxw4wfqm9amka49p7libyx4ykbx")))

(define-public crate-shuttle-secrets-0.17.0 (c (n "shuttle-secrets") (v "0.17.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.17.0") (k 0)))) (h "0aqx6rrakb9z1lkq9i21dsh6gjsp1xm2yvj8a175f8a3jksc4qcr")))

(define-public crate-shuttle-secrets-0.18.0 (c (n "shuttle-secrets") (v "0.18.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.18.0") (k 0)))) (h "1f0bglm0idlqh0pd6r5m8k11nsjsr3dhzb4jlv0qik4mzgrh42bm")))

(define-public crate-shuttle-secrets-0.19.0 (c (n "shuttle-secrets") (v "0.19.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.19.0") (k 0)))) (h "0529j1vkjqpsjggdrv2hj7kx8ginkkjwx02nm1csp4509jlqiznb")))

(define-public crate-shuttle-secrets-0.20.0 (c (n "shuttle-secrets") (v "0.20.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.20.0") (d #t) (k 0)))) (h "1bdh4q6p5pw9ydf8rb3sd986dji1gd9vghnlg86ygaiks2q6amis")))

(define-public crate-shuttle-secrets-0.21.0 (c (n "shuttle-secrets") (v "0.21.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.21.0") (d #t) (k 0)))) (h "0wmacc5c82bd03j9v9srgbfr518gwphywm2dvsh14s2nhvm7gixn")))

(define-public crate-shuttle-secrets-0.22.0 (c (n "shuttle-secrets") (v "0.22.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.22.0") (d #t) (k 0)))) (h "0p20blzz5jlj2g6rx42gcqjys9qs7mikzcf18spxqkdcb2c5qzaq")))

(define-public crate-shuttle-secrets-0.23.0 (c (n "shuttle-secrets") (v "0.23.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.23.0") (d #t) (k 0)))) (h "17acm1nca4sa2va1bc1gqp3clc9rhq6rw9idf71yy7r8f6g4li6r")))

(define-public crate-shuttle-secrets-0.24.0 (c (n "shuttle-secrets") (v "0.24.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.24.0") (d #t) (k 0)))) (h "18h8ms30yj69awc68ga6ln47mx4zbgssi151mbfjp042c73pnkm2")))

(define-public crate-shuttle-secrets-0.25.0 (c (n "shuttle-secrets") (v "0.25.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.25.0") (d #t) (k 0)))) (h "1ycjxj1jjnphdzr91h0zv7i3f8ih2q49q241lsp1hvfwng3mlj4a")))

(define-public crate-shuttle-secrets-0.26.0 (c (n "shuttle-secrets") (v "0.26.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.26.0") (d #t) (k 0)))) (h "0g4qk4s0m6g5gh3911s386qw5dn5xs8jjsb4jlyz41w0kpnlsyck")))

(define-public crate-shuttle-secrets-0.27.0 (c (n "shuttle-secrets") (v "0.27.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.27.0") (d #t) (k 0)))) (h "0mq5iiz7si37d3ij6mj666rq4hzmjss0simi3w2fihjxzrmhkkis")))

(define-public crate-shuttle-secrets-0.28.0 (c (n "shuttle-secrets") (v "0.28.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.28.0") (d #t) (k 0)))) (h "16b044dr819bvfhvdycw8xkyas69ch5ip0k2b15h7dmcrvraxvq4")))

(define-public crate-shuttle-secrets-0.29.0 (c (n "shuttle-secrets") (v "0.29.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.29.0") (d #t) (k 0)))) (h "1bcrnbqw97hl6d76v60alhmjsv49j4ya4rjns6nlc66jh2yrrzcy")))

(define-public crate-shuttle-secrets-0.30.0 (c (n "shuttle-secrets") (v "0.30.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.30.0") (d #t) (k 0)))) (h "16a4dslc76wz0wyp8sjafpdibbdy54ff2di7rpk27wbpd23b99br")))

(define-public crate-shuttle-secrets-0.30.1 (c (n "shuttle-secrets") (v "0.30.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.30.1") (d #t) (k 0)))) (h "1yb2ghv7dfywywbjlmk31rv1y8jgjgs60ynilbskm7m8ap93kdzc")))

(define-public crate-shuttle-secrets-0.31.0 (c (n "shuttle-secrets") (v "0.31.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.31.0") (d #t) (k 0)))) (h "0lzc9mfj5nx26nc6qbi1d4b5bb3s9nr63hkgw0klv9if8d7hbgp9")))

(define-public crate-shuttle-secrets-0.32.0 (c (n "shuttle-secrets") (v "0.32.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.32.0") (d #t) (k 0)))) (h "1q23d0h9gqk9133a74gd2d18ndd18xdv8pz836icr2yafrjr6a53")))

(define-public crate-shuttle-secrets-0.33.0 (c (n "shuttle-secrets") (v "0.33.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.33.0") (d #t) (k 0)))) (h "0rapgh88ym7kfc4rqiz9d077vvr2rp7dbdvcp6kwg69aag7km6a1")))

(define-public crate-shuttle-secrets-0.34.0 (c (n "shuttle-secrets") (v "0.34.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.34.0") (d #t) (k 0)))) (h "1qrfwjgmx1qxdfk9lh9vra8bymal4rfqifbzivi31f63cmx2w6pb")))

(define-public crate-shuttle-secrets-0.34.1 (c (n "shuttle-secrets") (v "0.34.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.34.1") (d #t) (k 0)))) (h "1915rr6ylgpdhzd22lp37gydwcxld1zzg5jra5p649jmj6djdq7h")))

(define-public crate-shuttle-secrets-0.35.0 (c (n "shuttle-secrets") (v "0.35.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.35.0") (d #t) (k 0)))) (h "0amac7cqs4100pm39pjhl6dz983dmr0ybq62l9qxzs2da09n6kkq")))

(define-public crate-shuttle-secrets-0.35.1 (c (n "shuttle-secrets") (v "0.35.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.35.1") (d #t) (k 0)))) (h "1m1djpyj4m3g7fxlz8dgi3v341zfdpgns47yw2j2nyw1z04bamil")))

(define-public crate-shuttle-secrets-0.35.2 (c (n "shuttle-secrets") (v "0.35.2") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.35.2") (d #t) (k 0)))) (h "1asmy795ym60qcrzavkmzvpfiajfx1mfkdyk0w540dy99nmjiwwm")))

(define-public crate-shuttle-secrets-0.36.0 (c (n "shuttle-secrets") (v "0.36.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.36.0") (d #t) (k 0)))) (h "1dbgqw68jffvkgxylxxppcnj69yxjdnm8hkqg8bnl0avwgnry75p")))

(define-public crate-shuttle-secrets-0.37.0 (c (n "shuttle-secrets") (v "0.37.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.37.0") (d #t) (k 0)))) (h "0nb13689pjf75ic0wjxx448flfwi9pnl8shg4jxf8nk49mhbiq2r")))

(define-public crate-shuttle-secrets-0.38.0 (c (n "shuttle-secrets") (v "0.38.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.38.0") (d #t) (k 0)))) (h "0ssm47629c3qqkhgj61b8z0wrafhm642kd32mg6b5zha66c3vpbv")))

(define-public crate-shuttle-secrets-0.39.0 (c (n "shuttle-secrets") (v "0.39.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.39.0") (d #t) (k 0)))) (h "0q8v48xq280856dhm7a1gv1bsv7gg6rizcib7nxh4jc76m3qzg7h")))

(define-public crate-shuttle-secrets-0.40.0 (c (n "shuttle-secrets") (v "0.40.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.40.0") (d #t) (k 0)))) (h "028xn43ch0yxddqbfi6ksmbbxgy9c422vmb4qq6b9q1kgl1slv28")))

(define-public crate-shuttle-secrets-0.41.0 (c (n "shuttle-secrets") (v "0.41.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.41.0") (d #t) (k 0)))) (h "034bag0c84gmmklr98g9xbrxisa16dh988lip3jp5dmc0m13k7ad")))

(define-public crate-shuttle-secrets-0.42.0 (c (n "shuttle-secrets") (v "0.42.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shuttle-service") (r "<=0.44.0") (d #t) (k 0)))) (h "08b99m53mnvbxavma9b373fav9rlf74zv9fwdyk02xdfzihiqinz")))

