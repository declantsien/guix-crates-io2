(define-module (crates-io sh ut shuttle-metadata) #:use-module (crates-io))

(define-public crate-shuttle-metadata-0.1.0 (c (n "shuttle-metadata") (v "0.1.0") (h "158pj2y1dk98mwrp8kr11gprfpj5vimwsxpqbydk08jrkrrryppd")))

(define-public crate-shuttle-metadata-0.25.0 (c (n "shuttle-metadata") (v "0.25.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.25.0") (d #t) (k 0)))) (h "0a9iznf40n2y6svz5z405c2q1nxmqgsaghvxravw2853pkzhijzd")))

(define-public crate-shuttle-metadata-0.26.0 (c (n "shuttle-metadata") (v "0.26.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.26.0") (d #t) (k 0)))) (h "1d18cm3lh3hd261wlrd3bvf9srcdyb7fmnm6zqxg5yympic54383")))

(define-public crate-shuttle-metadata-0.27.0 (c (n "shuttle-metadata") (v "0.27.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.27.0") (d #t) (k 0)))) (h "00pzhxpxifq5n4f94xz6p7i13ga2d9f33yb960bsddjd9j03i5ds")))

(define-public crate-shuttle-metadata-0.28.0 (c (n "shuttle-metadata") (v "0.28.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.28.0") (d #t) (k 0)))) (h "07cznghzf3f9sz1bawq8p370ly69fnkhrw4q7psrr5fvvyrnrr2h")))

(define-public crate-shuttle-metadata-0.29.0 (c (n "shuttle-metadata") (v "0.29.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.29.0") (d #t) (k 0)))) (h "1929dy9r0qn1bch6mmy473zjpxsmdxb64w16f96jqsp0qyxf1zjr")))

(define-public crate-shuttle-metadata-0.30.0 (c (n "shuttle-metadata") (v "0.30.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.30.0") (d #t) (k 0)))) (h "0kzx2rdcbnzjpbf7dz7vq135p2kpw9g2kkpcs5flylf4j5xxy9m4")))

(define-public crate-shuttle-metadata-0.30.1 (c (n "shuttle-metadata") (v "0.30.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.30.1") (d #t) (k 0)))) (h "1kpmd3998r2r9f5ax65b01dnxgbpnvy3mcjx3ja9rfliciq4m4yq")))

(define-public crate-shuttle-metadata-0.31.0 (c (n "shuttle-metadata") (v "0.31.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.31.0") (d #t) (k 0)))) (h "1b6rbpa3n939zf687866g4ddna3q7i4y77ilww2m0q80qrg34j6p")))

(define-public crate-shuttle-metadata-0.32.0 (c (n "shuttle-metadata") (v "0.32.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.32.0") (d #t) (k 0)))) (h "0scc9rwpdyb36767s1zd1g9bg26m1jvc7saqcfvzv80s2j0iqwc7")))

(define-public crate-shuttle-metadata-0.33.0 (c (n "shuttle-metadata") (v "0.33.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.33.0") (d #t) (k 0)))) (h "0p86ckhr6qm7iz23x4vxj6aphwp79xi0hcqryb6g8mhcnjfwx6mz")))

(define-public crate-shuttle-metadata-0.34.0 (c (n "shuttle-metadata") (v "0.34.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.34.0") (d #t) (k 0)))) (h "1jvz8aaplzznipa0spkyx260fw8sphv7sad2lvz33zs8rz34dc2n")))

(define-public crate-shuttle-metadata-0.34.1 (c (n "shuttle-metadata") (v "0.34.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.34.1") (d #t) (k 0)))) (h "1kl57xcbwp09mnnp9jmn98imkxfyjyz15xl81mrjq955vf9bk919")))

(define-public crate-shuttle-metadata-0.35.0 (c (n "shuttle-metadata") (v "0.35.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.35.0") (d #t) (k 0)))) (h "0r83krf4dahzdx1a2g4fc42c3ghzx47rjcabfk7ayd8332dd6ayp")))

(define-public crate-shuttle-metadata-0.35.1 (c (n "shuttle-metadata") (v "0.35.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.35.1") (d #t) (k 0)))) (h "1w6prmbzbal08mpgib040g47pxc22f2hrw77cvxbp9hbi4qcmyg5")))

(define-public crate-shuttle-metadata-0.35.2 (c (n "shuttle-metadata") (v "0.35.2") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.35.2") (d #t) (k 0)))) (h "0xlyjwivnylm8p0vfm074yv52zm4msxwy5blih6h28d4ibk31w2j")))

(define-public crate-shuttle-metadata-0.36.0 (c (n "shuttle-metadata") (v "0.36.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.36.0") (d #t) (k 0)))) (h "1slvpilbia2d0w410sc5536gwk8gadk95n71vj0my8nn7pqmigp5")))

(define-public crate-shuttle-metadata-0.37.0 (c (n "shuttle-metadata") (v "0.37.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.37.0") (d #t) (k 0)))) (h "1ppglq8cphbxmn2r7p7b1vn9vnzp0rmbkj7i4hrkjzglvqhyvs9w")))

(define-public crate-shuttle-metadata-0.38.0 (c (n "shuttle-metadata") (v "0.38.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.38.0") (d #t) (k 0)))) (h "0shw20cbgx7bfwwznxnljggfg70l1wixb7hxdrwrnpjvikqbahg8")))

(define-public crate-shuttle-metadata-0.39.0 (c (n "shuttle-metadata") (v "0.39.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.39.0") (d #t) (k 0)))) (h "07adqvs4g239h8nn3znd3m57bba0155whscxc1sgxd8jaji9m4zb")))

(define-public crate-shuttle-metadata-0.40.0 (c (n "shuttle-metadata") (v "0.40.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.40.0") (d #t) (k 0)))) (h "1p6nrmcfbma077dar1fr2h66spx6midn966p7amhgf814abbg796")))

(define-public crate-shuttle-metadata-0.41.0 (c (n "shuttle-metadata") (v "0.41.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.41.0") (d #t) (k 0)))) (h "0ri5aadns9p6fs58mh0arby7ssasg3zz8kp3dwm3skif2wq25l86")))

(define-public crate-shuttle-metadata-0.42.0 (c (n "shuttle-metadata") (v "0.42.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "shuttle-service") (r "<=0.44.0") (d #t) (k 0)))) (h "03424mp8pmd1yhi3aha4c55xbi2isj7wrkmn2d05i0crgwlpzqvv")))

