(define-module (crates-io sh ut shuttle-salvo) #:use-module (crates-io))

(define-public crate-shuttle-salvo-0.12.0-rc1 (c (n "shuttle-salvo") (v "0.12.0-rc1") (d (list (d (n "salvo") (r "^0.37.5") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0r4fbs1psildijbv5fhxgqkvac761w4pw02zzqym73hq2s1lqqvr")))

(define-public crate-shuttle-salvo-0.12.0 (c (n "shuttle-salvo") (v "0.12.0") (d (list (d (n "salvo") (r "^0.37.5") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "05aqnxfgbzvk150yjz1h330mx5yx1aljvv0695q3vprqjsvz9ap9")))

(define-public crate-shuttle-salvo-0.13.0 (c (n "shuttle-salvo") (v "0.13.0") (d (list (d (n "salvo") (r "^0.37.5") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0frgz88r7aiqax63206qnx0dn45jmqf7qxhxy7g9xc242yjd2ic7")))

(define-public crate-shuttle-salvo-0.14.0 (c (n "shuttle-salvo") (v "0.14.0") (d (list (d (n "salvo") (r "^0.37.5") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1w2281c929mp8pl7x5b9pyz7sigybjmr58n3swkali6n13bwdjw6")))

(define-public crate-shuttle-salvo-0.15.0 (c (n "shuttle-salvo") (v "0.15.0") (d (list (d (n "salvo") (r "^0.37.5") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1r9hkf37aswywyxwi9hdls9dsv5hhs3ixnq5za58fjf527vx6c6c")))

(define-public crate-shuttle-salvo-0.16.0 (c (n "shuttle-salvo") (v "0.16.0") (d (list (d (n "salvo") (r "^0.37.5") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0l0jw1ghd1l9rgb090hc2xjk0ach3h0hvk5s8k1f7ylrjb3r44aw")))

(define-public crate-shuttle-salvo-0.17.0 (c (n "shuttle-salvo") (v "0.17.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0iab6mcqf6bnmg30cyjkf9yj14yf84bhv3p15bc4l3fmidii83k8")))

(define-public crate-shuttle-salvo-0.18.0 (c (n "shuttle-salvo") (v "0.18.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.18.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1bhhjf89h52lh4ibp1gm8zi5cksik684f4xn4fvwh71x9w5qya39")))

(define-public crate-shuttle-salvo-0.19.0 (c (n "shuttle-salvo") (v "0.19.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0y3aa8cxa3blg4h67fx9wvdnzxl0mzknca0qfhm5cbnm3zs4y330")))

(define-public crate-shuttle-salvo-0.20.0 (c (n "shuttle-salvo") (v "0.20.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.20.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1fw0m0vg2jywzz2aizgr6gjdrbdn1cg2v0gny0r8h3wqxrll7d0k")))

(define-public crate-shuttle-salvo-0.21.0 (c (n "shuttle-salvo") (v "0.21.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.21.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1j0n3hcdwvizzh03v3aqyb464kx8zsza5waf506qy75lfxr8k9yf")))

(define-public crate-shuttle-salvo-0.22.0 (c (n "shuttle-salvo") (v "0.22.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.22.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0d5p5c4078ys2ksf4fhnrngbmmi76im1rf7k1vayshz245f5xq0i")))

(define-public crate-shuttle-salvo-0.23.0 (c (n "shuttle-salvo") (v "0.23.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.23.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0r17vhmci942msjhwzg9qsqrvhx1w1ccm7b1l8crr19950bndrl5")))

(define-public crate-shuttle-salvo-0.24.0 (c (n "shuttle-salvo") (v "0.24.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.24.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1xcjkwwx40aq7zkq7a9rlmv173aqpqd39xcm4znyqdw6ljpjvjph")))

(define-public crate-shuttle-salvo-0.25.0 (c (n "shuttle-salvo") (v "0.25.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.25.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0mjjfyvs1qqkcqahyhn515rz3k580gpf72i0rw44y2p7xdn6fsda")))

(define-public crate-shuttle-salvo-0.26.0 (c (n "shuttle-salvo") (v "0.26.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.26.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1vhkj2w447h9bw9rghj1jpbb9kbfc8a96ya73mmz4ifwwfpqr3dk")))

(define-public crate-shuttle-salvo-0.27.0 (c (n "shuttle-salvo") (v "0.27.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.27.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1q0kdgrg50vxk6r7k8pz2wbcfb0nrrgh6dr5plblsphnp61jypny")))

(define-public crate-shuttle-salvo-0.28.0 (c (n "shuttle-salvo") (v "0.28.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.28.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0ds1biw627ybq2dcl8zvqzwadqaapdq7cjq8rgcgygymjzvsy2nn")))

(define-public crate-shuttle-salvo-0.29.0 (c (n "shuttle-salvo") (v "0.29.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.29.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1nhdj2q5vi2gxhhpx34icwbmyw6nkqv01wz63j0fg0pdcwrq28sy")))

(define-public crate-shuttle-salvo-0.30.0 (c (n "shuttle-salvo") (v "0.30.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1q5avw6pjqvbmc27gj7zdhspwzjrh203k0lsd4ncdpajz9v9gii9")))

(define-public crate-shuttle-salvo-0.30.1 (c (n "shuttle-salvo") (v "0.30.1") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0dpcibmc2njgicpdw3pkc9860hlf1rxzj3qw6lg2rvpd9vjpmnj9")))

(define-public crate-shuttle-salvo-0.31.0 (c (n "shuttle-salvo") (v "0.31.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.31.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "00wxxn9803mg97glcl1mdp6y7zjmy29mxal2536kjk2vpwnqk4y1")))

(define-public crate-shuttle-salvo-0.32.0 (c (n "shuttle-salvo") (v "0.32.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.32.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1cx1ybdi46am3wwz71kms95kxgqm32ycdq57gh1ng3xf5pp3scdl")))

(define-public crate-shuttle-salvo-0.33.0 (c (n "shuttle-salvo") (v "0.33.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.33.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1w7nhbkg21rs55bfy0gh9r55s02gfhrx36b8905clbsf3xxmqsyd")))

(define-public crate-shuttle-salvo-0.34.0 (c (n "shuttle-salvo") (v "0.34.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.34.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0zp1cddn5szihs72n8h2vzfn45kskvvrv71l0ipjgv9b2y7172av")))

(define-public crate-shuttle-salvo-0.34.1 (c (n "shuttle-salvo") (v "0.34.1") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.34.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0hw9ncgggh1m60yg21pr9khr7q6xwccyjrg9wk01c0l8d450fn8b")))

(define-public crate-shuttle-salvo-0.35.0 (c (n "shuttle-salvo") (v "0.35.0") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "17h865mmmn50lvc4i1r05jdkj94im6yfbjvx03ccbmdgqrwi7qmb")))

(define-public crate-shuttle-salvo-0.35.1 (c (n "shuttle-salvo") (v "0.35.1") (d (list (d (n "salvo") (r "^0.41.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0s6ca6cssk44yhwycfj37fq3pjb7cpz2yicha8nyx1y50bcygjfk")))

(define-public crate-shuttle-salvo-0.35.2 (c (n "shuttle-salvo") (v "0.35.2") (d (list (d (n "salvo") (r "^0.63.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.2") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0789a8k4g3ms6zz9n0d5zqjph3gbgf188923rrqsapam5k2s10mv")))

(define-public crate-shuttle-salvo-0.36.0 (c (n "shuttle-salvo") (v "0.36.0") (d (list (d (n "salvo") (r "^0.63.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.36.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0hgfblgs8j9bd0ccl1fisfmhhnn517p229zjhzlhq893cg02znn8")))

(define-public crate-shuttle-salvo-0.37.0 (c (n "shuttle-salvo") (v "0.37.0") (d (list (d (n "salvo") (r "^0.63.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.37.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "07qf53aqcc54fwrd9fmqj8fz913d5nk7nx3p1fz9msba5qqdvpjq")))

(define-public crate-shuttle-salvo-0.38.0 (c (n "shuttle-salvo") (v "0.38.0") (d (list (d (n "salvo") (r "^0.63.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.38.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1napalsik50lgb6ki2aj40nrv9ln42m3j9kiw1pdbkpgl7bhp0zn")))

(define-public crate-shuttle-salvo-0.39.0 (c (n "shuttle-salvo") (v "0.39.0") (d (list (d (n "salvo") (r "^0.63.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.39.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1m5i3ppbyc2krm55gxd1ria5c54y5v55vps9a9ga0m30bcm6mp2n")))

(define-public crate-shuttle-salvo-0.40.0 (c (n "shuttle-salvo") (v "0.40.0") (d (list (d (n "salvo") (r "^0.63.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.40.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1vvfn5ki9lixawn792rg0s43alxb8xasi1py9c7g0mkj6g0mz9k2")))

(define-public crate-shuttle-salvo-0.41.0 (c (n "shuttle-salvo") (v "0.41.0") (d (list (d (n "salvo") (r "^0.63.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.41.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0zps18yxscy33974my8mfmy7xrdylznbkrzqrf7jbpifjzlqgi5g")))

(define-public crate-shuttle-salvo-0.42.0 (c (n "shuttle-salvo") (v "0.42.0") (d (list (d (n "salvo") (r "^0.63.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.42.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "17zfzsp6b4rcnff5f6wx1hhf401m89532v2kj5wgbb7mwldhq2s1")))

(define-public crate-shuttle-salvo-0.43.0 (c (n "shuttle-salvo") (v "0.43.0") (d (list (d (n "salvo") (r "^0.63.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.43.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "130z5qdl6b9yv7rymwrrvxqvn5077ka3jb34yxhi2pc5z3k497sv")))

(define-public crate-shuttle-salvo-0.44.0 (c (n "shuttle-salvo") (v "0.44.0") (d (list (d (n "salvo") (r "^0.63.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.44.0") (k 0)))) (h "1z5785kq1lb5cz5s5yih455md80z327jwvykqvd6dkc0zl52hnps")))

(define-public crate-shuttle-salvo-0.45.0 (c (n "shuttle-salvo") (v "0.45.0") (d (list (d (n "salvo") (r "^0.63.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.45.0") (k 0)))) (h "19xbp93x3pp79xbhg2idncbj5h1s05mb9y4rjd77vdrrzpp004pz")))

