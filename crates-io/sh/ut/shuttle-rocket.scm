(define-module (crates-io sh ut shuttle-rocket) #:use-module (crates-io))

(define-public crate-shuttle-rocket-0.12.0-rc1 (c (n "shuttle-rocket") (v "0.12.0-rc1") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1v27pcjj022jjv4p3cizfsh23ar12mw2188kvsbfv90i9s70x256")))

(define-public crate-shuttle-rocket-0.12.0 (c (n "shuttle-rocket") (v "0.12.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1f4hy95pjmwm30gwh6470h0fz0g82s09sjhynd8x1v04m4v17k7f")))

(define-public crate-shuttle-rocket-0.13.0 (c (n "shuttle-rocket") (v "0.13.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1f2s1zds9f9wc4vjyl9r4w8gzzzr6mimbixg13gzrwkjqx5p1jja")))

(define-public crate-shuttle-rocket-0.14.0 (c (n "shuttle-rocket") (v "0.14.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0r1my296yk4x3vk8mxiyzni9gqxy3rfx2yqsyiwqcvq3hi5x6rh4")))

(define-public crate-shuttle-rocket-0.15.0 (c (n "shuttle-rocket") (v "0.15.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0n96qc74arl7lf53gjyakgqa8wiqklvy40pawlf443a1lkf8ci6y")))

(define-public crate-shuttle-rocket-0.16.0 (c (n "shuttle-rocket") (v "0.16.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1gqqrqh26mqcpnx3swfygic42221v30ndd2knsyc1cqy3zil355x")))

(define-public crate-shuttle-rocket-0.17.0 (c (n "shuttle-rocket") (v "0.17.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1n49shrsdbw7cbsly11mzdlnkndfh93q5k50kpzw2rwcjhj1kw1x")))

(define-public crate-shuttle-rocket-0.18.0 (c (n "shuttle-rocket") (v "0.18.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.18.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0cv8zaab3k4d2xr2w30ydxkx70dr3haz5rk2h3q8gh4qrkcary7g")))

(define-public crate-shuttle-rocket-0.19.0 (c (n "shuttle-rocket") (v "0.19.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "17fjd7dkky8rkyp4la8yvnwk6g7zbf0ymh3cynfc0lmxsnymkvfi")))

(define-public crate-shuttle-rocket-0.20.0 (c (n "shuttle-rocket") (v "0.20.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.20.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0c7daj05xxrqqcvprq4x129qq36k5snxr25qc8ky5laa7drzyiaw")))

(define-public crate-shuttle-rocket-0.21.0 (c (n "shuttle-rocket") (v "0.21.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.21.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0z69ap9031mdvhbis3n9rfiinv6gyw7574iy5cr6i5h0ib2vf602")))

(define-public crate-shuttle-rocket-0.22.0 (c (n "shuttle-rocket") (v "0.22.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.22.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ypz4hwj68iqhryqsck6sqmiihbjnkxgj3j83hrq86wpljc4qmwv")))

(define-public crate-shuttle-rocket-0.23.0 (c (n "shuttle-rocket") (v "0.23.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.23.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ynf70h041awidq380hmhdf5z2ni2h16jhqllk85c22amxqx7hyd")))

(define-public crate-shuttle-rocket-0.24.0 (c (n "shuttle-rocket") (v "0.24.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.24.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1nyjw5zgrd7myjx4qxcynb47nj0h3w9plm8m6dzm0c65q7qjyy66")))

(define-public crate-shuttle-rocket-0.25.0 (c (n "shuttle-rocket") (v "0.25.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.25.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0jxr3c5q89pnibgxi1kbmxn2bdja5zslc1p4rbvvxyqaf9nj8vjg")))

(define-public crate-shuttle-rocket-0.26.0 (c (n "shuttle-rocket") (v "0.26.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.26.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1j94ycjxkpvlzc8djnm0wxwqi6da3k1rxhh0lv9qhsjdm02x6jah")))

(define-public crate-shuttle-rocket-0.27.0 (c (n "shuttle-rocket") (v "0.27.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.27.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0sikp9chg0cp18jc5cqm0xfyycrsa3r0l96y97z7gcq115ym4903")))

(define-public crate-shuttle-rocket-0.28.0 (c (n "shuttle-rocket") (v "0.28.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.28.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1qm06pwrx9asf2ynaqm2ncahca1x1kvlqh9sb34rrfqnmkjjhvd3")))

(define-public crate-shuttle-rocket-0.29.0 (c (n "shuttle-rocket") (v "0.29.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.29.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0ik6cl9l8xg6z8kkwsqhpmnyl8aavcdqjqrgb90j0szg6v2whnr3")))

(define-public crate-shuttle-rocket-0.30.0 (c (n "shuttle-rocket") (v "0.30.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0jpym1plns3xr24mj5b7alz3zkc1lrd09cdwbigzgvwnslsqb2zl")))

(define-public crate-shuttle-rocket-0.30.1 (c (n "shuttle-rocket") (v "0.30.1") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0kfcp8kg4chshnfxwnpldkvh3m8wdm8lsjrgvh150d7b6fln7f7z")))

(define-public crate-shuttle-rocket-0.31.0 (c (n "shuttle-rocket") (v "0.31.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.31.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "03f4kms4lq1r1g8jjdnljlar321d634ycz26s77z359wh8nnq5w5")))

(define-public crate-shuttle-rocket-0.32.0 (c (n "shuttle-rocket") (v "0.32.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.32.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "09zd2wmk0ivqp1hzy7nb3bvi475pr4kddpl648kdb7013d6wp860")))

(define-public crate-shuttle-rocket-0.33.0 (c (n "shuttle-rocket") (v "0.33.0") (d (list (d (n "rocket") (r "^0.5.0-rc.4") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.33.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1f1w24kk5lyhm91amr6sz0isnkw77mrqigl8fnc5gkglgg80r4nn")))

(define-public crate-shuttle-rocket-0.34.0 (c (n "shuttle-rocket") (v "0.34.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.34.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0fm3i7h0blhi9cj6zn34l4xw47q37gphdiw8x4fyb2wsgibc2gvc")))

(define-public crate-shuttle-rocket-0.34.1 (c (n "shuttle-rocket") (v "0.34.1") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.34.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1fkzppckc2p599mkal0cbi10kvg0m0h4i7pn6w8pzz9k6hf712gz")))

(define-public crate-shuttle-rocket-0.35.0 (c (n "shuttle-rocket") (v "0.35.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1x9xagpx67lqxbr6vmps43h1mjfgrv1x3nb7307vfgxid20p4qfg")))

(define-public crate-shuttle-rocket-0.35.1 (c (n "shuttle-rocket") (v "0.35.1") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0gy4ai5hkgv1pkz0ffq0gbmrykygycz3h1adwfgz8hfibk8534q3")))

(define-public crate-shuttle-rocket-0.35.2 (c (n "shuttle-rocket") (v "0.35.2") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.2") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1rwcv9rj4wx2r6lgf8v2zsgh5j98vbnfa7agmw7lhd94ibq54s2n")))

(define-public crate-shuttle-rocket-0.36.0 (c (n "shuttle-rocket") (v "0.36.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.36.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "09jhhwh79yjvv6hjxxyclri9mwm26z8qrxjv1ibhrdvgcbzi5qpa")))

(define-public crate-shuttle-rocket-0.37.0 (c (n "shuttle-rocket") (v "0.37.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.37.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "05yyx6vn5qd7sf7vp03szbx09kwilxpq7qbar4ihh7gg5irnja98")))

(define-public crate-shuttle-rocket-0.38.0 (c (n "shuttle-rocket") (v "0.38.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.38.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1q4wcmxpliqcfyyszxinkaad0mz240hcyqyk0qyx06yxfxzli3rr")))

(define-public crate-shuttle-rocket-0.39.0 (c (n "shuttle-rocket") (v "0.39.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.39.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "05xlgf6mgvwz2brjih5b0np5n8iizpmp9g0153dcxar31f4iihhs")))

(define-public crate-shuttle-rocket-0.40.0 (c (n "shuttle-rocket") (v "0.40.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.40.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0w6zxxqzk8jw1v5zlg0y6ggqnqvifnz7smk00hkla61ix4njafjh")))

(define-public crate-shuttle-rocket-0.41.0 (c (n "shuttle-rocket") (v "0.41.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.41.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1z957wjp3blzp7wp72yf07nks0hwg13r7djiaq5psiyp5w0vr4gp")))

(define-public crate-shuttle-rocket-0.42.0 (c (n "shuttle-rocket") (v "0.42.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.42.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0dc1laf295ppn50q3n30862k32xjnlrl7skcp7nk273h6aq2j9pi")))

(define-public crate-shuttle-rocket-0.43.0 (c (n "shuttle-rocket") (v "0.43.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.43.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1sg7dpsr3gvjq3m72yn7q5sxkznicn6zn24kkpbbrrkxrdh4wj7x")))

(define-public crate-shuttle-rocket-0.44.0 (c (n "shuttle-rocket") (v "0.44.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.44.0") (k 0)))) (h "16y83mwnmwx9xlylx7gxpca3qs9s7kzi8jz0wb412chjp7la0ip5")))

(define-public crate-shuttle-rocket-0.45.0 (c (n "shuttle-rocket") (v "0.45.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.45.0") (k 0)))) (h "0v6gsd1c96wadv65s87bpg77jifw32lgg68bc70azadvnv4i8w3d")))

