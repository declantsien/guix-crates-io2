(define-module (crates-io sh ut shuttle-sdk) #:use-module (crates-io))

(define-public crate-shuttle-sdk-0.1.0 (c (n "shuttle-sdk") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shuttle-core") (r "^0.3.1") (d #t) (k 0)))) (h "146wxhp8yh4l0dbax8d2yx5glmx7rg0fh8kisl0ymwy7mbkv7jvn")))

