(define-module (crates-io sh ut shuttle-poise) #:use-module (crates-io))

(define-public crate-shuttle-poise-0.12.0-rc1 (c (n "shuttle-poise") (v "0.12.0-rc1") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1dzddj3vrbp3fnp737g9387g1yni90gri2fpk9hj6nziwa3dw5qg")))

(define-public crate-shuttle-poise-0.12.0 (c (n "shuttle-poise") (v "0.12.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0qz7zp6kdwgzk071ji4hb114fk7wkznyghjr1pkgfmqgj6fzc6f6")))

(define-public crate-shuttle-poise-0.13.0 (c (n "shuttle-poise") (v "0.13.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0vl3vqmjg0kbdsymm5xapzqsy2r84wf2lh81n0s6df72rlpq683g")))

(define-public crate-shuttle-poise-0.14.0 (c (n "shuttle-poise") (v "0.14.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1i140bi9zb4cfpndbmhl5irhl506s13znflaf6nmwhjlhnz7qbgz")))

(define-public crate-shuttle-poise-0.15.0 (c (n "shuttle-poise") (v "0.15.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0q2gix6y29mqb4zz3ddlfhnxgmkiq03s0m0fjmcb0v1dbpjphk35")))

(define-public crate-shuttle-poise-0.16.0 (c (n "shuttle-poise") (v "0.16.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "03237d6j6x08lsci06258rv71x6sv7xhhfa6m3isvg3s2gpypj62")))

(define-public crate-shuttle-poise-0.17.0 (c (n "shuttle-poise") (v "0.17.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0q5174b0qynsvd62zmb8rgb2hwjf5rg18sj5f2ds2z0adzq1jcw7")))

(define-public crate-shuttle-poise-0.18.0 (c (n "shuttle-poise") (v "0.18.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.18.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "02mjkxby7j8in49wgy4mw8maifjpanfbih2hm8lblg0x83iy3k4d")))

(define-public crate-shuttle-poise-0.19.0 (c (n "shuttle-poise") (v "0.19.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1s4pnfyinhslqsydkhss9wrsnshbpvz8q4x4401kv719xpz47sm7")))

(define-public crate-shuttle-poise-0.20.0 (c (n "shuttle-poise") (v "0.20.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.20.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0rk9rkwzrj2vw4xhals5j3xji8hf647b8sk6p7fn4fdahhw8qh2k")))

(define-public crate-shuttle-poise-0.21.0 (c (n "shuttle-poise") (v "0.21.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.21.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "16398gha7fbzivddkwga3fhgn75gv2dj3j4jwy2hg5jy97vbnavg")))

(define-public crate-shuttle-poise-0.22.0 (c (n "shuttle-poise") (v "0.22.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.22.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1nm1aixzaicwxyfvhhxv0ihkb1gkzi5qkb3ndr96q9qpnfs76k3j")))

(define-public crate-shuttle-poise-0.23.0 (c (n "shuttle-poise") (v "0.23.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.23.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "15hx5ns08da8byd6x6mzzpbwphy02zxk97mpwx04s8blknykb5nh")))

(define-public crate-shuttle-poise-0.24.0 (c (n "shuttle-poise") (v "0.24.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.24.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ag9dqym2fwp1mn1x14icfdl8ndwpi5sc20v4wrw3sab66f9a78d")))

(define-public crate-shuttle-poise-0.25.0 (c (n "shuttle-poise") (v "0.25.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.25.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0zb8f8mjz07kbywgwlgbwpznvx7qangndyda4fbl5nfs10db4dxx")))

(define-public crate-shuttle-poise-0.26.0 (c (n "shuttle-poise") (v "0.26.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.26.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1myl3a7b843bpr5mz9hdlkscmiabd3z21327knx0n9hplianrq8b")))

(define-public crate-shuttle-poise-0.27.0 (c (n "shuttle-poise") (v "0.27.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.27.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1wiicwx7pd2bz8z35zalpwzcrpf8wivv8512c0ig3mspl41hdi1s")))

(define-public crate-shuttle-poise-0.28.0 (c (n "shuttle-poise") (v "0.28.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.28.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0262hy42njsk3zmvqwj0i6cx6dq3cqihggzzh6lhr3gd65i1w6gn")))

(define-public crate-shuttle-poise-0.29.0 (c (n "shuttle-poise") (v "0.29.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.29.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "00mr0hhx0b1xsrd82q22ni07zz974a7c517n3g46bsbx2dlv4xs8")))

(define-public crate-shuttle-poise-0.30.0 (c (n "shuttle-poise") (v "0.30.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0jhcj9k7n0kcg3xaz8pd3s8y3assz3ir3xhscv67pkv6553c3hry")))

(define-public crate-shuttle-poise-0.30.1 (c (n "shuttle-poise") (v "0.30.1") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1h632k255iha1izhvnspzmfbdb1i58r8ws3y6nbdc4m5syzb04zr")))

(define-public crate-shuttle-poise-0.31.0 (c (n "shuttle-poise") (v "0.31.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.31.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "164j3z796qva1j0fva5ijm6a3mh8kmm0cdcram9kyqvhynaqznjl")))

(define-public crate-shuttle-poise-0.32.0 (c (n "shuttle-poise") (v "0.32.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.32.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "090lqqdv75bvs2674gfcnvy6k2lgzi5v54al8blrnvrikmjqy23z")))

(define-public crate-shuttle-poise-0.33.0 (c (n "shuttle-poise") (v "0.33.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.33.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0bk1m0dh3zxdg8m76a9w6w0m2xmwl0dnbxm28q7j71mxdh375bbm")))

(define-public crate-shuttle-poise-0.34.0 (c (n "shuttle-poise") (v "0.34.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.34.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "00hcy1789fy0jyf8q1szncyc6xzxwk7czz22a5nnq89hb4ysdxqi")))

(define-public crate-shuttle-poise-0.34.1 (c (n "shuttle-poise") (v "0.34.1") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.34.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0kq53nkmlc5p07j7nczqwipprgqg67r5h3cl9j4gg30i75mvqgkb")))

(define-public crate-shuttle-poise-0.35.0 (c (n "shuttle-poise") (v "0.35.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0wraamvrxpxr883a8x3z2q5nrhfbg4xcpihjcfmhnb0kn7cywrqa")))

(define-public crate-shuttle-poise-0.35.1 (c (n "shuttle-poise") (v "0.35.1") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "090czvlkhvvfiaizjvkw79ncx7mypd9m1x3nhcgvmqf5lfrwim09")))

(define-public crate-shuttle-poise-0.35.2 (c (n "shuttle-poise") (v "0.35.2") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.2") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "11l1ymy7xcfwih399is2r169ak8gzmx69h1g42dswhvjzs6dzz5d")))

(define-public crate-shuttle-poise-0.36.0 (c (n "shuttle-poise") (v "0.36.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.36.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0bwfkdg4hq6pxlq03n8sb1slwj714411jh4jv8hqy37mgwxy8cxc")))

(define-public crate-shuttle-poise-0.37.0 (c (n "shuttle-poise") (v "0.37.0") (d (list (d (n "poise") (r "^0.5.2") (d #t) (k 0)) (d (n "shuttle-runtime") (r "<=0.40.0") (k 0)))) (h "1dnl7nr73lg6b25vvpa8jj3573j8chw95vnagi4hh8v1zxjy58mf")))

