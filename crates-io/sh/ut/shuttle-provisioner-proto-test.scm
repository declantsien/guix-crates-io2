(define-module (crates-io sh ut shuttle-provisioner-proto-test) #:use-module (crates-io))

(define-public crate-shuttle-provisioner-proto-test-0.1.0 (c (n "shuttle-provisioner-proto-test") (v "0.1.0") (d (list (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.0") (d #t) (k 1)))) (h "147syychfa3n4gswbk2ls0naxk0lfy5cb0i7k72lpnji7knn3zq4")))

