(define-module (crates-io sh ut shutdown_server) #:use-module (crates-io))

(define-public crate-shutdown_server-0.1.0 (c (n "shutdown_server") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "macros"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "1wdyvpdyvd09k6w06arfpy6bv6gp8yn14ncdwlmgd3cj4d49pgrv")))

