(define-module (crates-io sh ut shuttle-thruster) #:use-module (crates-io))

(define-public crate-shuttle-thruster-0.12.0-rc1 (c (n "shuttle-thruster") (v "0.12.0-rc1") (d (list (d (n "shuttle-runtime") (r "^0.12.0-rc1") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0a4q54nj42vmn6mn4kbnjgmv6kp49z1kvzkb6dn72cqkm1b09xj4")))

(define-public crate-shuttle-thruster-0.12.0 (c (n "shuttle-thruster") (v "0.12.0") (d (list (d (n "shuttle-runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "122avxw85rcqf7lh2hc776lrrzvsx4s5bg4rqxdi9mywyljwj43w")))

(define-public crate-shuttle-thruster-0.13.0 (c (n "shuttle-thruster") (v "0.13.0") (d (list (d (n "shuttle-runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0n7j63hlgi0736znk9zsxpqvdy3867vr36rb3ln2yzblqy7vp6g8")))

(define-public crate-shuttle-thruster-0.14.0 (c (n "shuttle-thruster") (v "0.14.0") (d (list (d (n "shuttle-runtime") (r "^0.14.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "03lm1j1asw91ia2101hxm8y7y2jz679vq4f92s94gd0qkibnrnnm")))

(define-public crate-shuttle-thruster-0.15.0 (c (n "shuttle-thruster") (v "0.15.0") (d (list (d (n "shuttle-runtime") (r "^0.15.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1flk8jk7cg0gjqvqbnrm22cy1svjr3k5yqkblzbdj51734wdzplj")))

(define-public crate-shuttle-thruster-0.16.0 (c (n "shuttle-thruster") (v "0.16.0") (d (list (d (n "shuttle-runtime") (r "^0.16.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1wgrs3y88gcg8rmyiaixig2qvc8dbf412hpa8cz5zza3g9r2bsrk")))

(define-public crate-shuttle-thruster-0.17.0 (c (n "shuttle-thruster") (v "0.17.0") (d (list (d (n "shuttle-runtime") (r "^0.17.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "10x8xq0d89zmcaxw9mxflcfdvgq0ip1pwd6834qidk0lcqsh1hnh")))

(define-public crate-shuttle-thruster-0.18.0 (c (n "shuttle-thruster") (v "0.18.0") (d (list (d (n "shuttle-runtime") (r "^0.18.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "01wjppmlfzrqflfhyc4dqkj9mq7q1s90yxlgpak17mj2py9il2hv")))

(define-public crate-shuttle-thruster-0.19.0 (c (n "shuttle-thruster") (v "0.19.0") (d (list (d (n "shuttle-runtime") (r "^0.19.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0syvr7z7aiawalw54h5k649zgmhagzghcmxcsysn5as29imr8a28")))

(define-public crate-shuttle-thruster-0.20.0 (c (n "shuttle-thruster") (v "0.20.0") (d (list (d (n "shuttle-runtime") (r "^0.20.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1hnwpyjbibiaaix2g2yldhvilav6j2q26w49yw07gfd26ixa2g9r")))

(define-public crate-shuttle-thruster-0.21.0 (c (n "shuttle-thruster") (v "0.21.0") (d (list (d (n "shuttle-runtime") (r "^0.21.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0xw979mkhs3080j1ha4pv0g6vni7w5rmzfhfig1zh5bl3f2j38ml")))

(define-public crate-shuttle-thruster-0.22.0 (c (n "shuttle-thruster") (v "0.22.0") (d (list (d (n "shuttle-runtime") (r "^0.22.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0rsbgmkh9k2sbcxbyx0h17jvxhbg1yzbzg2dkc14l98b6i6cz9v6")))

(define-public crate-shuttle-thruster-0.23.0 (c (n "shuttle-thruster") (v "0.23.0") (d (list (d (n "shuttle-runtime") (r "^0.23.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0nwvw3yfc79wh5fw4ir5wff6axlgq4c29slng6120ah720z1n2l3")))

(define-public crate-shuttle-thruster-0.24.0 (c (n "shuttle-thruster") (v "0.24.0") (d (list (d (n "shuttle-runtime") (r "^0.24.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1qihzhlskqbqh35s4drp6ij7ymjrnxnrxz9phsi1xavj1wqx152x")))

(define-public crate-shuttle-thruster-0.25.0 (c (n "shuttle-thruster") (v "0.25.0") (d (list (d (n "shuttle-runtime") (r "^0.25.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0r1gk6ccwqmfxy5r9np07cxzx1iv4x6y5x50gfczlqznijfb032m")))

(define-public crate-shuttle-thruster-0.26.0 (c (n "shuttle-thruster") (v "0.26.0") (d (list (d (n "shuttle-runtime") (r "^0.26.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1rfp03k8gwcz9v2dz0w3qra2x1q6n0a12p3yb3zq8sqf19qpijx6")))

(define-public crate-shuttle-thruster-0.27.0 (c (n "shuttle-thruster") (v "0.27.0") (d (list (d (n "shuttle-runtime") (r "^0.27.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1g8ism2a8mlpd2x2m8zkaf56jb1gnfdci4zivzxzl9gc2icii9h4")))

(define-public crate-shuttle-thruster-0.28.0 (c (n "shuttle-thruster") (v "0.28.0") (d (list (d (n "shuttle-runtime") (r "^0.28.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1kfnfmq4ikj704428xjqknmxk92yqy3qb8g0829k4v0pm31vwc3h")))

(define-public crate-shuttle-thruster-0.29.0 (c (n "shuttle-thruster") (v "0.29.0") (d (list (d (n "shuttle-runtime") (r "^0.29.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "08qyv3080dsnby9s9azn143gfp3xhv24mdyj41i16dc7hhsn09va")))

(define-public crate-shuttle-thruster-0.30.0 (c (n "shuttle-thruster") (v "0.30.0") (d (list (d (n "shuttle-runtime") (r "^0.30.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "148x3ph49gnd8hji3z53qnwhzxsig0r4kybvfw91lhfmdfm7qs47")))

(define-public crate-shuttle-thruster-0.30.1 (c (n "shuttle-thruster") (v "0.30.1") (d (list (d (n "shuttle-runtime") (r "^0.30.1") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1cmcdb51i97wca6v1rp8v5sa20w0agd6k1b1fbrfdczwjdy2m39j")))

(define-public crate-shuttle-thruster-0.31.0 (c (n "shuttle-thruster") (v "0.31.0") (d (list (d (n "shuttle-runtime") (r "^0.31.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "07kxrp4qy0p6wrwgpvfn7czi52zklsak6bqlxf0w7s3jcq5sf1vf")))

(define-public crate-shuttle-thruster-0.32.0 (c (n "shuttle-thruster") (v "0.32.0") (d (list (d (n "shuttle-runtime") (r "^0.32.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1k0vbli0wfjpf4lmy28h1rijsz57bw9ab73yp027qx2vkwcsra43")))

(define-public crate-shuttle-thruster-0.33.0 (c (n "shuttle-thruster") (v "0.33.0") (d (list (d (n "shuttle-runtime") (r "^0.33.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0x1bcwn6g3k9d1zm53ysqw77m6fazcrm61d93ncvvc9p0mx670dw")))

(define-public crate-shuttle-thruster-0.34.0 (c (n "shuttle-thruster") (v "0.34.0") (d (list (d (n "shuttle-runtime") (r "^0.34.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "13pbjxmyyhmq9dal55bnyhhlfbhvmz0jv34qka34jxdzxnyj91zf")))

(define-public crate-shuttle-thruster-0.34.1 (c (n "shuttle-thruster") (v "0.34.1") (d (list (d (n "shuttle-runtime") (r "^0.34.1") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "02aamvgzffsf8pwv22ys3vhl36s0xwg1xrn1gbapyvjmc4ygh1rl")))

(define-public crate-shuttle-thruster-0.35.0 (c (n "shuttle-thruster") (v "0.35.0") (d (list (d (n "shuttle-runtime") (r "^0.35.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0aprcank7xpm3ignhlsrw416ryr911p5nd6jgirnd6mqfjcv73b0")))

(define-public crate-shuttle-thruster-0.35.1 (c (n "shuttle-thruster") (v "0.35.1") (d (list (d (n "shuttle-runtime") (r "^0.35.1") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1kmgnx3mh5wlaqkxmzddp4488jdy2m2wr0mxl775b3jjn7y2nkyf")))

(define-public crate-shuttle-thruster-0.35.2 (c (n "shuttle-thruster") (v "0.35.2") (d (list (d (n "shuttle-runtime") (r "^0.35.2") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1jlwbkip7r03q3pgqyiml6gfx50jdx2b962x33mhdkqc8lqzpd12")))

(define-public crate-shuttle-thruster-0.36.0 (c (n "shuttle-thruster") (v "0.36.0") (d (list (d (n "shuttle-runtime") (r "^0.36.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1cxgfsil9m340d345l8ilsvmkcy4pjsyak3zan2g4zpnxz2aq07d")))

(define-public crate-shuttle-thruster-0.37.0 (c (n "shuttle-thruster") (v "0.37.0") (d (list (d (n "shuttle-runtime") (r "^0.37.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "04fmk8m53aj301hin9d2m4g25dw8zh3jx1r7gbl6x074icdpvzb5")))

(define-public crate-shuttle-thruster-0.38.0 (c (n "shuttle-thruster") (v "0.38.0") (d (list (d (n "shuttle-runtime") (r "^0.38.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0cw8bnzqj9cjlk1c7vb8ysbil01sq4l8sn1cg02zizb5ld6ilpnp")))

(define-public crate-shuttle-thruster-0.39.0 (c (n "shuttle-thruster") (v "0.39.0") (d (list (d (n "shuttle-runtime") (r "^0.39.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "10aqzzg4y52ck6rgbjr29x9m13f3bm7b19927v5yiywg0dg7da9n")))

(define-public crate-shuttle-thruster-0.40.0 (c (n "shuttle-thruster") (v "0.40.0") (d (list (d (n "shuttle-runtime") (r "^0.40.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "14i7krgddz73rxyvgxx8d8zdxzm0rjv56z9ja31c52y7vgn052wv")))

(define-public crate-shuttle-thruster-0.41.0 (c (n "shuttle-thruster") (v "0.41.0") (d (list (d (n "shuttle-runtime") (r "^0.41.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "12db6w5wm8gl2sdaqcv2dzi9fsv96acgqpq9wxxg57sg26skja0i")))

(define-public crate-shuttle-thruster-0.42.0 (c (n "shuttle-thruster") (v "0.42.0") (d (list (d (n "shuttle-runtime") (r "^0.42.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0zp52701mgqifwrr5ndn06by4pzxapspm5m4k0xppssmkry4c06n")))

(define-public crate-shuttle-thruster-0.43.0 (c (n "shuttle-thruster") (v "0.43.0") (d (list (d (n "shuttle-runtime") (r "^0.43.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1pjqzv83jxw614b1cl253lybpviq9375ympr4fj9hc572jgh0vys")))

(define-public crate-shuttle-thruster-0.44.0 (c (n "shuttle-thruster") (v "0.44.0") (d (list (d (n "shuttle-runtime") (r "^0.44.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)))) (h "0p1kapknhmw6y1agf6zn6r616l5y3jkz9l93d1p4n18n7wk09awd")))

(define-public crate-shuttle-thruster-0.45.0 (c (n "shuttle-thruster") (v "0.45.0") (d (list (d (n "shuttle-runtime") (r "^0.45.0") (k 0)) (d (n "thruster") (r "^1.3.0") (d #t) (k 0)) (d (n "thruster") (r "^1.3.0") (f (quote ("hyper_server"))) (d #t) (k 2)))) (h "0sc7vkfxq1nw3inmd1q7x5hxw45a531hhkwslw5wswh6npggi4ff")))

