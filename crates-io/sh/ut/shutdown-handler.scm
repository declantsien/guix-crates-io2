(define-module (crates-io sh ut shutdown-handler) #:use-module (crates-io))

(define-public crate-shutdown-handler-0.1.0 (c (n "shutdown-handler") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal" "sync" "rt"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "signal-hook") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "178pp2nb34if85ls0ic171lj01k7av7fsnal7l6rg8zylgj42dgy")))

(define-public crate-shutdown-handler-0.1.1 (c (n "shutdown-handler") (v "0.1.1") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal" "sync" "rt"))) (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0kgyjky6w58dfpkr1wbpkxl68hydi64qys76l4m2pmdsf9bwg9yy")))

