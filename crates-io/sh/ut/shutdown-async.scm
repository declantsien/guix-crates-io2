(define-module (crates-io sh ut shutdown-async) #:use-module (crates-io))

(define-public crate-shutdown-async-0.0.0 (c (n "shutdown-async") (v "0.0.0") (d (list (d (n "tokio") (r "^1.27.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1q0ajc9cp927yk659hgi84201b9pwql2vkas8yr6v4vag1g5h4na")))

(define-public crate-shutdown-async-0.1.0 (c (n "shutdown-async") (v "0.1.0") (d (list (d (n "tokio") (r "^1.27.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0a4hf0n7hbw0vyh00hf06wk7ayzf7gigx9fkdjdq13xlirgzbx4i")))

(define-public crate-shutdown-async-0.1.1 (c (n "shutdown-async") (v "0.1.1") (d (list (d (n "tokio") (r "^1.27.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1zs8lgmll7x6vmfl6c8z9w8ijckqzazr8vbcv3nqprp7pmlrwyg2")))

