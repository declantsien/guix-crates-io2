(define-module (crates-io sh ut shuttle-actix-web) #:use-module (crates-io))

(define-public crate-shuttle-actix-web-0.12.0-rc1 (c (n "shuttle-actix-web") (v "0.12.0-rc1") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1wbqnvfm6d761cr1zr1nsd13c9s8nxxracyq3xpis255rnagwic3")))

(define-public crate-shuttle-actix-web-0.12.0 (c (n "shuttle-actix-web") (v "0.12.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0p5qk6x5y0gm9rss1n9psagxvlkiy9s1140qn7idyair1h0ms4kp")))

(define-public crate-shuttle-actix-web-0.13.0 (c (n "shuttle-actix-web") (v "0.13.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1rwrxfwhbxww833zn07g1afpms54dfr0hd3v11cdd9fm0nznzqb1")))

(define-public crate-shuttle-actix-web-0.14.0 (c (n "shuttle-actix-web") (v "0.14.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1qn7l6zk9dvx7m6zr3zlwj5kpni3bclzizrr1qxsc796vp3mhpp7")))

(define-public crate-shuttle-actix-web-0.15.0 (c (n "shuttle-actix-web") (v "0.15.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0fcfq989hr47jc3fcj0bzzgnq04i78lwxfq07qsnyxik9871qj5w")))

(define-public crate-shuttle-actix-web-0.16.0 (c (n "shuttle-actix-web") (v "0.16.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0q2m15l96xkyq7y8hvvfgwm95v2aqd0lsk5w2yrv4cfl52rxar1s")))

(define-public crate-shuttle-actix-web-0.17.0 (c (n "shuttle-actix-web") (v "0.17.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1v30n5b3fh3ayhxxm3r6aybxzb6fjklwb8zd4x11vrqn5l7fkgjn")))

(define-public crate-shuttle-actix-web-0.18.0 (c (n "shuttle-actix-web") (v "0.18.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.18.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "15syvfjb4axj0zrrw5m1pyaagx4ccfwd20frgifahfjiljkkk39i")))

(define-public crate-shuttle-actix-web-0.19.0 (c (n "shuttle-actix-web") (v "0.19.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "10vrsv04mgkxrpvcd2w4zjdyhxxpa5ay3w8bhs2ynxk4ih0db3p5")))

(define-public crate-shuttle-actix-web-0.20.0 (c (n "shuttle-actix-web") (v "0.20.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.20.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1hwc6iyhdcw8yj5rhwqdy7dwhh2dfsgqzzjh373879s0cz8c8swx")))

(define-public crate-shuttle-actix-web-0.21.0 (c (n "shuttle-actix-web") (v "0.21.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.21.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "196rqlv9pbk3xg19nhnssg1bbd7vb26qzhkcikxk61xzx4sayim3")))

(define-public crate-shuttle-actix-web-0.22.0 (c (n "shuttle-actix-web") (v "0.22.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.22.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ysh546nypshlfajmhawlxd6a1lgrkvdkaycvg9kks9lm8flpwwh")))

(define-public crate-shuttle-actix-web-0.23.0 (c (n "shuttle-actix-web") (v "0.23.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.23.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0szns8m9q1gpcn0nhq2yryq80872nb264wn0id09m25kmpvphr9d")))

(define-public crate-shuttle-actix-web-0.24.0 (c (n "shuttle-actix-web") (v "0.24.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.24.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1hbzv38h9pm2x6rmm5yhr0f1sr9dnfnwfvq4ddiznd50hnzw258z")))

(define-public crate-shuttle-actix-web-0.25.0 (c (n "shuttle-actix-web") (v "0.25.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.25.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0bj6h16f27g12kpwgvnjfi4255x5qwf339pn4zs5mwkqixy7ip4c")))

(define-public crate-shuttle-actix-web-0.26.0 (c (n "shuttle-actix-web") (v "0.26.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.26.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "04152wmj7n84cywhf07mbwvhaav086qw1lrzw23lpg9ynvl632j5")))

(define-public crate-shuttle-actix-web-0.27.0 (c (n "shuttle-actix-web") (v "0.27.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.27.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "10j099wlkl1cfxgnv180nw1dpbiijhjfws31vais1zlh6qwhr2zr")))

(define-public crate-shuttle-actix-web-0.28.0 (c (n "shuttle-actix-web") (v "0.28.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.28.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1cvpcfmx5s699g4hn48i6y2dd6v8g2hs791i9cj3rxhsmgj20a02")))

(define-public crate-shuttle-actix-web-0.29.0 (c (n "shuttle-actix-web") (v "0.29.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.29.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "13n706892ihzmi9mg6nlafbx4vqcf6hlcxw9fkaf41ygq02wqsj1")))

(define-public crate-shuttle-actix-web-0.30.0 (c (n "shuttle-actix-web") (v "0.30.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "01pczv3frkmzzhdjnppf69xbkni87kr8zxwj3n7z5wqaaiqhwp51")))

(define-public crate-shuttle-actix-web-0.30.1 (c (n "shuttle-actix-web") (v "0.30.1") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1jqfk281gnhll16x6387qqlkw2cva51sx5l0f4q9533ky13xfl7h")))

(define-public crate-shuttle-actix-web-0.31.0 (c (n "shuttle-actix-web") (v "0.31.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.31.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "03sryhn2xgqxvbw02w3zg295pmsxsa4z4rwpiqwryah8hhb53wnm")))

(define-public crate-shuttle-actix-web-0.32.0 (c (n "shuttle-actix-web") (v "0.32.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.32.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0pj7aqalllnj78zy38s810ln9xjjwa358i84jx81jb9imzsqasfv")))

(define-public crate-shuttle-actix-web-0.33.0 (c (n "shuttle-actix-web") (v "0.33.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.33.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0fg53grds8qfn5hnpw2bbncbi5swfbfyjg1w78l8x3b6yjw6gy6z")))

(define-public crate-shuttle-actix-web-0.34.0 (c (n "shuttle-actix-web") (v "0.34.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.34.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0mj4x0spa1ank2rl25vaqjfsk8q2dadkhpr7srxa6xk8b1y4l0k5")))

(define-public crate-shuttle-actix-web-0.34.1 (c (n "shuttle-actix-web") (v "0.34.1") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.34.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0nkbwrgs588gvh4iladc7qm96jcbxqlmfjq26jmlhzk3p5qm0995")))

(define-public crate-shuttle-actix-web-0.35.0 (c (n "shuttle-actix-web") (v "0.35.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0hfffbzclyizc5xmlbx6hv5wccfycwqq6nbbj1cw3wjjif1f6xxj")))

(define-public crate-shuttle-actix-web-0.35.1 (c (n "shuttle-actix-web") (v "0.35.1") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1i6w65yaf8j41v41jqvk5qsnmi6v47f3d28772yb78xfbq0vba3w")))

(define-public crate-shuttle-actix-web-0.35.2 (c (n "shuttle-actix-web") (v "0.35.2") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.2") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "05m5myjjyi9p3ras3bf56pkj002h2g3441m539hk02avh7wigvq3")))

(define-public crate-shuttle-actix-web-0.36.0 (c (n "shuttle-actix-web") (v "0.36.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.36.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1zw0d994mj0l257k2apiidvwi2r1qzxblyzbfq5y7n64k3289lz4")))

(define-public crate-shuttle-actix-web-0.37.0 (c (n "shuttle-actix-web") (v "0.37.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.37.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0dhrih8ikaa874nx707cdjcw6jspfh1h0ylcqjfykfs3x453a30a")))

(define-public crate-shuttle-actix-web-0.38.0 (c (n "shuttle-actix-web") (v "0.38.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.38.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1qz40b36cgj4sxcnh4jnvsgwsas1cchpm6xx186gjac75xiwsymb")))

(define-public crate-shuttle-actix-web-0.39.0 (c (n "shuttle-actix-web") (v "0.39.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.39.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "16hwa6kknbmry4k8d9vlc8h43balnyj8d5al93lcnffvc1xr06c8")))

(define-public crate-shuttle-actix-web-0.40.0 (c (n "shuttle-actix-web") (v "0.40.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.40.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "14swkmw7b0xnrrqyv560y9n4pkphk5fl4f5b17zhphfc0za37vhl")))

(define-public crate-shuttle-actix-web-0.41.0 (c (n "shuttle-actix-web") (v "0.41.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.41.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0hphz5r0iza1y871zjsnr1dma1gxvx56w5arhggbvnzgmxdwav1g")))

(define-public crate-shuttle-actix-web-0.42.0 (c (n "shuttle-actix-web") (v "0.42.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.42.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0zbpdf26v90g8hwd5rr1c76llj6672zml5nhd1nqqv06dd05rk56")))

(define-public crate-shuttle-actix-web-0.43.0 (c (n "shuttle-actix-web") (v "0.43.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.43.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1zzpf6zxihybl8nllnr7vsx9hk319xlcyjgs9cpw7cq9djqm3l6n")))

(define-public crate-shuttle-actix-web-0.44.0 (c (n "shuttle-actix-web") (v "0.44.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.44.0") (k 0)))) (h "132wk5vq56cfvq2qvmi1407w11dv0j1fh4wxqig18j535s6srvqv")))

(define-public crate-shuttle-actix-web-0.45.0 (c (n "shuttle-actix-web") (v "0.45.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.45.0") (k 0)))) (h "0zw1ca4zzxlxiz0bfxvykfh7b4rhxxypqbcfijrkj919j090h9kk")))

