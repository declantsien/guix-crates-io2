(define-module (crates-io sh ut shutil) #:use-module (crates-io))

(define-public crate-shutil-0.1.0 (c (n "shutil") (v "0.1.0") (h "1bnd6j1ib8dmkjq42k5gi3lyiv3p5f2fkz79d44kcn6ckxmz3jvk")))

(define-public crate-shutil-0.1.1 (c (n "shutil") (v "0.1.1") (h "01fgf92dhncclynwkkrgbr70czn2a61bjy7ymmx44rfmrxcfxy6j")))

(define-public crate-shutil-0.1.2 (c (n "shutil") (v "0.1.2") (h "1qd7lcx23c40jqvjkjxndnf4l9gvhrf31w6advprqpjn1lb1yv3j")))

