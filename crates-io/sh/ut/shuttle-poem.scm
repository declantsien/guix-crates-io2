(define-module (crates-io sh ut shuttle-poem) #:use-module (crates-io))

(define-public crate-shuttle-poem-0.12.0-rc1 (c (n "shuttle-poem") (v "0.12.0-rc1") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0dw4kvgfv1wrmf5m43wg307k2hw943xc7fxmavx7wv7c81rjkr07")))

(define-public crate-shuttle-poem-0.12.0 (c (n "shuttle-poem") (v "0.12.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1mb9ig0idpfm2d3z8q4x21fl8j5snrn7v0q8xzkvkzh1qp0ddmw2")))

(define-public crate-shuttle-poem-0.13.0 (c (n "shuttle-poem") (v "0.13.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0lmb80yws3zlrp7bbp60z20115frmmngqcdy8hyrl460gvx3xaan")))

(define-public crate-shuttle-poem-0.14.0 (c (n "shuttle-poem") (v "0.14.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "05rn0r12v7m8vsa84iyrn2l571w6l4nf3vwl6lyz6c8lj7q1w32c")))

(define-public crate-shuttle-poem-0.15.0 (c (n "shuttle-poem") (v "0.15.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "02hxv6iyvjpkj2pbwwq80155lgf931k7w1nik5ignsndq8waadcv")))

(define-public crate-shuttle-poem-0.16.0 (c (n "shuttle-poem") (v "0.16.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1l8dwzya09aaqnswqwcp99mhnacvnrp2f40kg224ar0xk4032gyg")))

(define-public crate-shuttle-poem-0.17.0 (c (n "shuttle-poem") (v "0.17.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0mr3w24927qfirg8d0fapdiqci4bc7qyry6bpp0aqdz1q20jbp2c")))

(define-public crate-shuttle-poem-0.18.0 (c (n "shuttle-poem") (v "0.18.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.18.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0j0g4i6x4j19kxvxw4ypmncx4yc2im8qic0d6wrvlifhfv4wk4ka")))

(define-public crate-shuttle-poem-0.19.0 (c (n "shuttle-poem") (v "0.19.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "13zx0rmi256agdgx2mq30nqkl8nf1cg1djf5aqxlbcsjda2g8j08")))

(define-public crate-shuttle-poem-0.20.0 (c (n "shuttle-poem") (v "0.20.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.20.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0pbrgf6r3zn161izbpw1ar8sfh25zbpqa4qsiaixzz75057v9phi")))

(define-public crate-shuttle-poem-0.21.0 (c (n "shuttle-poem") (v "0.21.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.21.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "13lfccy4gk9mw4fpx2s82a8xbnhfz94dflq2w4827xhzws66w6m4")))

(define-public crate-shuttle-poem-0.22.0 (c (n "shuttle-poem") (v "0.22.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.22.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0w8f54505bypw43bmhcjz4kry8d86yhjir7c6fqnczp0bf0mkbb5")))

(define-public crate-shuttle-poem-0.23.0 (c (n "shuttle-poem") (v "0.23.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.23.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0xskcpgwsqvcfmcp56y81d7k4z6f67hj0x3v8jlzczmy8db3rwzn")))

(define-public crate-shuttle-poem-0.24.0 (c (n "shuttle-poem") (v "0.24.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.24.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0wdz3wv1xg26604c0mjsyf2sfqwl1fk8l3gk5yhanyqpqcghrjfd")))

(define-public crate-shuttle-poem-0.25.0 (c (n "shuttle-poem") (v "0.25.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.25.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0yrzhax2s585k3sxhwp31a4g6f1wj4s3cs5dai6616j4zvlkfl1n")))

(define-public crate-shuttle-poem-0.26.0 (c (n "shuttle-poem") (v "0.26.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.26.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "18a14mbhlg1m5x068lankdi23jbfdwgqg8qmkc9z3d58357bv7rq")))

(define-public crate-shuttle-poem-0.27.0 (c (n "shuttle-poem") (v "0.27.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.27.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1r7bwcb28f0kn2qhsspw5kwwwma0q71791ajcmb3pzy1l830gmn3")))

(define-public crate-shuttle-poem-0.28.0 (c (n "shuttle-poem") (v "0.28.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.28.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "17yn935ffnhi60glkjcxhps6f3p4i233v7kmws0rqvmjb3c4pp92")))

(define-public crate-shuttle-poem-0.29.0 (c (n "shuttle-poem") (v "0.29.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.29.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1qmwx90wdxxb3mrj8mh6myq7bzqh89rssr6ib0v9048zhdw49has")))

(define-public crate-shuttle-poem-0.30.0 (c (n "shuttle-poem") (v "0.30.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0swf18kl48rw2by2021l891h39lfyw7n4d2xj4x5z5hay17rbv5z")))

(define-public crate-shuttle-poem-0.30.1 (c (n "shuttle-poem") (v "0.30.1") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.30.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1qjbdqahj9m1xi4k4rprdl3w0nd1yydqg9ak75pk8pv81l5h8fz9")))

(define-public crate-shuttle-poem-0.31.0 (c (n "shuttle-poem") (v "0.31.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.31.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "05dj3fhf1f4vxlif9yl6hhqy27qr7kn0z4ffms8p0bqxfv20sqfs")))

(define-public crate-shuttle-poem-0.32.0 (c (n "shuttle-poem") (v "0.32.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.32.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "06rv8fmdff8pmxgxmnyz5nip99bqm3dqxpir8z2jq5lrbxi80bfl")))

(define-public crate-shuttle-poem-0.33.0 (c (n "shuttle-poem") (v "0.33.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.33.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1027fdzvmppsrzkncylzgi9s7f22qly8n96rhz4jrgpbk6rsq0gw")))

(define-public crate-shuttle-poem-0.34.0 (c (n "shuttle-poem") (v "0.34.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.34.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "06zhi2bc4130ns70fc57jp2agl885630g6hpzr82hd8c8xxyxsj5")))

(define-public crate-shuttle-poem-0.34.1 (c (n "shuttle-poem") (v "0.34.1") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.34.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1yd8dzvivldljwc4lwd944855wb3z3x5r41d6xixkgyvx7qfyz26")))

(define-public crate-shuttle-poem-0.35.0 (c (n "shuttle-poem") (v "0.35.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0r80r03viaxhy263msq79zwxhjvjr53rfancl4b6rar46ql623yi")))

(define-public crate-shuttle-poem-0.35.1 (c (n "shuttle-poem") (v "0.35.1") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.1") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ra0333gi09flkxsr5qablfzyy31jfwgb80wripljmwhs5ppmyxg")))

(define-public crate-shuttle-poem-0.35.2 (c (n "shuttle-poem") (v "0.35.2") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.35.2") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1yim55mkk0d298z8hffh44vp53cashfmx9vya2j3k0axzzz3v5sm")))

(define-public crate-shuttle-poem-0.36.0 (c (n "shuttle-poem") (v "0.36.0") (d (list (d (n "poem") (r "^1.3.55") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.36.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0bwvdcc28c3ahinqcxxw4yl6qkw4qhp1hfjmy79wl8k819xaqib3")))

(define-public crate-shuttle-poem-0.37.0 (c (n "shuttle-poem") (v "0.37.0") (d (list (d (n "poem") (r "^2.0.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.37.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ghn0r38k51n9skzhgfgalsp8zxa3cnnvdlszamkh7zq49hc6zhi")))

(define-public crate-shuttle-poem-0.38.0 (c (n "shuttle-poem") (v "0.38.0") (d (list (d (n "poem") (r "^2.0.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.38.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ybhclxxq36m1bnmry6dk18ikl58hq760fig0w3shxcbrxzhd6v6")))

(define-public crate-shuttle-poem-0.39.0 (c (n "shuttle-poem") (v "0.39.0") (d (list (d (n "poem") (r "^2.0.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.39.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1wp2jshn486ji5h5p62nxa1k09s87cdxic5i4ygh6kd6j4p8mrrg")))

(define-public crate-shuttle-poem-0.40.0 (c (n "shuttle-poem") (v "0.40.0") (d (list (d (n "poem") (r "^2.0.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.40.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1zpjb5qmw211l65gqxrkjsk22lk4b52g14n3d84yp799nkxqnygb")))

(define-public crate-shuttle-poem-0.41.0 (c (n "shuttle-poem") (v "0.41.0") (d (list (d (n "poem") (r "^2.0.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.41.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0xdq02jlqn2pd60z6nxy7ihvhv7dnrp5cb7zkn9hwi9y23mgx01k")))

(define-public crate-shuttle-poem-0.42.0 (c (n "shuttle-poem") (v "0.42.0") (d (list (d (n "poem") (r "^2.0.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.42.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "09r2lmpnhayy5afzx2fhiigy72pv3ij9pa51jghi8sy3jd5qz0r9")))

(define-public crate-shuttle-poem-0.43.0 (c (n "shuttle-poem") (v "0.43.0") (d (list (d (n "poem") (r "^2.0.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.43.0") (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1h5ff1im4pkijmsz27id94nfs8vmxlv72dka72ijybsdi28as53i")))

(define-public crate-shuttle-poem-0.44.0 (c (n "shuttle-poem") (v "0.44.0") (d (list (d (n "poem") (r "^3.0.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.44.0") (k 0)))) (h "1c79rpqdcng6sji3cq98wm34kj9qyl2z0pkkcdpl086i8q1isbps")))

(define-public crate-shuttle-poem-0.45.0 (c (n "shuttle-poem") (v "0.45.0") (d (list (d (n "poem") (r "^3.0.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.45.0") (k 0)))) (h "0i1l152d4cnzbj2a77whjs3c0xkliwxag3x397qvqnh1csykpfdh")))

