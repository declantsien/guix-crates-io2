(define-module (crates-io sh ut shuttle-qdrant) #:use-module (crates-io))

(define-public crate-shuttle-qdrant-0.36.0 (c (n "shuttle-qdrant") (v "0.36.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.36.0") (d #t) (k 0)))) (h "15gq9dwiv6vfq59fsmcdahh0i9gi4lzrg72i68j0vv53q299w0b6") (y #t)))

(define-public crate-shuttle-qdrant-0.37.0 (c (n "shuttle-qdrant") (v "0.37.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.37.0") (d #t) (k 0)))) (h "0rpzcyr8y85zz64nz6ijhbwqjm0g7984znvf7mjpny4ixszwxlx6") (y #t)))

(define-public crate-shuttle-qdrant-0.37.1 (c (n "shuttle-qdrant") (v "0.37.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.37.0") (d #t) (k 0)))) (h "1wmcj1jda7l8rskgdxx43cs4pwapcz9gx5sl49c93imhcfiv85xv")))

(define-public crate-shuttle-qdrant-0.38.0 (c (n "shuttle-qdrant") (v "0.38.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.38.0") (d #t) (k 0)))) (h "01qzv1411awf6qq68x67x5bb65s8nnyfgwps5sd8xmp1fl7pnxxg")))

(define-public crate-shuttle-qdrant-0.39.0 (c (n "shuttle-qdrant") (v "0.39.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shuttle-service") (r "^0.39.0") (d #t) (k 0)))) (h "1m2dmn7r08b0h9wdvbpavf7sv812li89zxsbiagjn9xs79gchjsq")))

(define-public crate-shuttle-qdrant-0.40.0 (c (n "shuttle-qdrant") (v "0.40.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.40.0") (d #t) (k 0)))) (h "0y3rbw96k6x7gs06hbqykhlsibrhkqf5p7l4ds8c1bh4lczhdx73")))

(define-public crate-shuttle-qdrant-0.41.0 (c (n "shuttle-qdrant") (v "0.41.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.41.0") (d #t) (k 0)))) (h "16y6hl8dxjcmn9prqjigc7mggd07k17n428yfz4026rxmk5l8r38")))

(define-public crate-shuttle-qdrant-0.42.0 (c (n "shuttle-qdrant") (v "0.42.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.42.0") (d #t) (k 0)))) (h "1hqq44cavpk10l1aq6ydi17qf9vd574689g6fj5cifs3bz1q5spd")))

(define-public crate-shuttle-qdrant-0.43.0 (c (n "shuttle-qdrant") (v "0.43.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.43.0") (d #t) (k 0)))) (h "0l5gfnahdixf6c9xmknr5bj50wmd487x256fkyf42lvwsa82rwrb")))

(define-public crate-shuttle-qdrant-0.44.0 (c (n "shuttle-qdrant") (v "0.44.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.44.0") (d #t) (k 0)))) (h "0qglrmjy0yry5b78jmwmrmh6wi8krb1gnmh6hdpwmdcgvvd7g63w")))

(define-public crate-shuttle-qdrant-0.45.0 (c (n "shuttle-qdrant") (v "0.45.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "qdrant-client") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.45.0") (d #t) (k 0)))) (h "1b5d7qy46vq6pcmrs98wcga7vrz17zhzy384ykax50qdslxjfgcx")))

