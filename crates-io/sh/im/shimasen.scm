(define-module (crates-io sh im shimasen) #:use-module (crates-io))

(define-public crate-shimasen-0.1.0 (c (n "shimasen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02da964w8ljj38ahx6rayc96isiq0wmf7sqzf67ja7iy6s1vqvj5")))

(define-public crate-shimasen-0.2.0 (c (n "shimasen") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1qji23dnv8z31q793z70yi1wcib4kk97aq9hrypkdl2imyikq3ij")))

(define-public crate-shimasen-0.3.0 (c (n "shimasen") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14bkcnnb6ygvrkwcmzqdpvk8nif8cxh4q1fladwbw92i0lzsv7zc")))

(define-public crate-shimasen-0.4.0 (c (n "shimasen") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1hdymhyw67bywghii0c14b3zycqw2h7j2ij2lnasasmvb4rpmvdx")))

(define-public crate-shimasen-0.5.0 (c (n "shimasen") (v "0.5.0") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "mail-parser") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1apklr155s0yc95s8vw9bcv7135lga3h2j84lwka5xs94q5zs4al")))

