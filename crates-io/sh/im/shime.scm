(define-module (crates-io sh im shime) #:use-module (crates-io))

(define-public crate-shime-0.1.0 (c (n "shime") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.0") (d #t) (k 0)))) (h "1p0w44b7ijvawfama1jrj3h3ks51nr3khdjyk5jjm47izwjj918i") (y #t)))

(define-public crate-shime-0.0.1 (c (n "shime") (v "0.0.1") (d (list (d (n "dirs") (r "^5.0.0") (d #t) (k 0)))) (h "1fh52w4r4d3l0pa4kcyj6kwhgn2njviaawxl36qwy8wh29bmgy2r")))

(define-public crate-shime-0.0.2 (c (n "shime") (v "0.0.2") (d (list (d (n "ansi-colors") (r "^0.3.0") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "git_info") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1418mm9mjywmp7j3rm41wb69zgaaihpls7ph64g3k7gz5xzbf7nc")))

(define-public crate-shime-0.0.3 (c (n "shime") (v "0.0.3") (d (list (d (n "ansi-colors") (r "^0.3.0") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "git_info") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)) (d (n "tsu") (r "^1.0.1") (d #t) (k 0)))) (h "0r4rd56i1fqwgp9c9kd3vqmmzm6cl95pyscl21lc7cwm3prqr9w1")))

(define-public crate-shime-0.0.4 (c (n "shime") (v "0.0.4") (d (list (d (n "ansi-colors") (r "^0.3.0") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "git_info") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)) (d (n "tsu") (r "^1.0.1") (d #t) (k 0)))) (h "1k3p7xn8756vb9mricwsp0w3i3p5r72n566g2ygrg0amff96xprc")))

