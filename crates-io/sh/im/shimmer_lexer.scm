(define-module (crates-io sh im shimmer_lexer) #:use-module (crates-io))

(define-public crate-shimmer_lexer-0.0.0 (c (n "shimmer_lexer") (v "0.0.0") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shimmer_meta") (r "^0") (d #t) (k 0)) (d (n "winnow") (r "^0.6") (d #t) (k 0)))) (h "0r876p8wah48wg5wvzl4bdrjhx1288a727zqwpyv7i0pfyyr0fkm") (f (quote (("debug" "winnow/debug"))))))

