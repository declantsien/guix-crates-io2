(define-module (crates-io sh im shimmer_meta) #:use-module (crates-io))

(define-public crate-shimmer_meta-0.0.0 (c (n "shimmer_meta") (v "0.0.0") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.6") (d #t) (k 0)))) (h "0418c4mspjfiijn6zpxm77z1z1h42dhlp1mysvdbjr27fkxclc2g")))

