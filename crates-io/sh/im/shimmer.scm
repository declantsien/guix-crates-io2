(define-module (crates-io sh im shimmer) #:use-module (crates-io))

(define-public crate-shimmer-0.0.0 (c (n "shimmer") (v "0.0.0") (d (list (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "shimmer_lexer") (r "^0") (d #t) (k 0)) (d (n "shimmer_meta") (r "^0") (d #t) (k 0)))) (h "0hm78qzdka9s2f1gd1rpvb2s2hfsk00z5c02s3wnlsfwzms56kg2")))

