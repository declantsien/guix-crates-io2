(define-module (crates-io sh oo shoogah_macros) #:use-module (crates-io))

(define-public crate-shoogah_macros-0.1.0 (c (n "shoogah_macros") (v "0.1.0") (d (list (d (n "as_bool") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "073fha9b4xxc5fqpsg0il1acbpw5whqi59i57j39psjgpsf8qpbq")))

(define-public crate-shoogah_macros-0.2.0 (c (n "shoogah_macros") (v "0.2.0") (d (list (d (n "as_bool") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00qy35srqiryk9pxsyzpaq26x2hxdadbymar5ckapfh1z2mdrhsc")))

(define-public crate-shoogah_macros-0.3.0 (c (n "shoogah_macros") (v "0.3.0") (d (list (d (n "as_bool") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dy3prhv6f3smm892k8m7ifa4lnxvqpiivw218f7jzd7vlj7cwad")))

