(define-module (crates-io sh oo shook) #:use-module (crates-io))

(define-public crate-shook-0.0.0 (c (n "shook") (v "0.0.0") (h "1xdj1j4inlip5pd09613gh9hcjzy6r5x51xg4yq1dwnbbal8i404")))

(define-public crate-shook-0.1.0 (c (n "shook") (v "0.1.0") (d (list (d (n "windows-sys") (r "^0.52") (d #t) (k 0)))) (h "0dp53knpc97jvkd5qqs126afr801g2vgf9izdj02pdmrmi274qr2") (r "1.77.1")))

(define-public crate-shook-0.1.1 (c (n "shook") (v "0.1.1") (h "16i6il175qgzz0jmj3bv86b6rcxz53i2g6y5vqb8yqinxz7wlcbq") (r "1.77.1")))

