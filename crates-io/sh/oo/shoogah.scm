(define-module (crates-io sh oo shoogah) #:use-module (crates-io))

(define-public crate-shoogah-0.1.0 (c (n "shoogah") (v "0.1.0") (d (list (d (n "as_bool") (r "^0.1") (d #t) (k 0)) (d (n "shoogah_macros") (r "^0.1") (d #t) (k 0)))) (h "0j0jzqrjs3k1a1gp6j3lknkvw3zn35n4zwasyn7dvjw2zsd59a0f")))

(define-public crate-shoogah-0.1.1 (c (n "shoogah") (v "0.1.1") (d (list (d (n "as_bool") (r "^0.1") (d #t) (k 0)) (d (n "shoogah_macros") (r "^0.1") (d #t) (k 0)))) (h "0visggzk60v9vnb8fgsb2iz7lbl5h9w6095s7gmiqx38iqg7z0v1")))

(define-public crate-shoogah-0.2.0 (c (n "shoogah") (v "0.2.0") (d (list (d (n "as_bool") (r "^0.1") (d #t) (k 0)) (d (n "shoogah_macros") (r "^0.2") (d #t) (k 0)))) (h "0jwm0f5mhw782b3wp8yzwi79a9k6bmrs4qd0mbaas1lcl1dqyp66")))

(define-public crate-shoogah-0.2.1 (c (n "shoogah") (v "0.2.1") (d (list (d (n "as_bool") (r "^0.1") (d #t) (k 0)) (d (n "shoogah_macros") (r "^0.2") (d #t) (k 0)))) (h "09pd256gdikps0fc0np7qich3mbjawraqzhlsin2lvgjm6r5apv4")))

(define-public crate-shoogah-0.2.2 (c (n "shoogah") (v "0.2.2") (d (list (d (n "as_bool") (r "^0.1") (d #t) (k 0)) (d (n "shoogah_macros") (r "^0.2") (d #t) (k 0)))) (h "1dr3bg6likcw15fxgc39s2lpiakr1k1rw1m8vdd113za13ddi127")))

(define-public crate-shoogah-0.3.0 (c (n "shoogah") (v "0.3.0") (d (list (d (n "as_bool") (r "^0.1") (d #t) (k 0)) (d (n "shoogah_macros") (r "^0.3") (d #t) (k 0)))) (h "0dbcjr6b70rk9x0by05sf6i6qqxk3940xyjahlk986p3z85yq882")))

