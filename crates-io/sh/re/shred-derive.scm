(define-module (crates-io sh re shred-derive) #:use-module (crates-io))

(define-public crate-shred-derive-0.1.0 (c (n "shred-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0dhjb3b51da0h01ylgs8k7lgdq3ishzzk9lfpbkla63vpjipg88s")))

(define-public crate-shred-derive-0.1.1 (c (n "shred-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0hj9wkxlh0njy5xg0bn40d48avrq7v3vi1bkkhcbvmdcncjszniw")))

(define-public crate-shred-derive-0.1.2 (c (n "shred-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "157n5j3vli0xbgivkgpx41xzwffw6rfc4h4x88q2695ch6jfhb2s")))

(define-public crate-shred-derive-0.2.0 (c (n "shred-derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0hndppfsbshc5saafaw6j985vax0s9lp2j66rzlqnck1xfp23vr1")))

(define-public crate-shred-derive-0.3.0 (c (n "shred-derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0jc0hrvkz8fz5yq5h72wcwfb1xn6bsavr8qjswngx4vf7f8r9a54")))

(define-public crate-shred-derive-0.4.0 (c (n "shred-derive") (v "0.4.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "09kbxzwbvcxnpqskhd6dnq3w848w3lg2fga261fxh0acnrsw9fcb")))

(define-public crate-shred-derive-0.5.0-alpha1 (c (n "shred-derive") (v "0.5.0-alpha1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0d6qnykk8cxy6q4998jkw0g9d14pdbzq4ylsb352sw1mjf727p9q")))

(define-public crate-shred-derive-0.5.0-alpha2 (c (n "shred-derive") (v "0.5.0-alpha2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0phaidyr2hra780gvpmahvyikv0ii5dbmw1xq2q1ggin3kdvzsi8")))

(define-public crate-shred-derive-0.5.0 (c (n "shred-derive") (v "0.5.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0v63gbbzxcrn3c18c1r0lpyismwf9jigl46x16cyzijhdkncfrjb")))

(define-public crate-shred-derive-0.5.1 (c (n "shred-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "18ci0vqs6swpjq3kqp83xmrpbjniys8w5bx7mcj30b9hwpjk9kwz")))

(define-public crate-shred-derive-0.6.0 (c (n "shred-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "106v6ywznkl1hzdbv5grsdlpdwq6dksc3a4f6wn9gk3dcgm4b271")))

(define-public crate-shred-derive-0.6.1 (c (n "shred-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1my2nxnrbviyxw232hvmq6awkz86avvas7p032p94cljzl1mq94c")))

(define-public crate-shred-derive-0.6.2 (c (n "shred-derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1pvzfjhii1chn6b07mfi9bvxpybcpnasmsxx3l4vy7vmya071wx1")))

(define-public crate-shred-derive-0.6.3 (c (n "shred-derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "0bsa0kf9v723xhv9v175wlkvcbxdxnpnmavn8ajl2phmplv4qh6m")))

(define-public crate-shred-derive-0.7.0 (c (n "shred-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "139smd9rrfcmfnm2czaj12084zgsh4ybcgk620f3ch4bdxw0nxk9")))

