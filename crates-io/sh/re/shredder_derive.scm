(define-module (crates-io sh re shredder_derive) #:use-module (crates-io))

(define-public crate-shredder_derive-0.1.0 (c (n "shredder_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bg9hdagxrd0d4n1y3pww2d7fcmw9sagi36pi11xgs7iv2jbcvgj")))

(define-public crate-shredder_derive-0.1.1 (c (n "shredder_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pb9vxsvxj440p1rdmdp6v5xlr0ykivc64jp7gi0ipqkb1pifmgw")))

(define-public crate-shredder_derive-0.2.0 (c (n "shredder_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "05qar25zd01r5ll7gv3yf8vwmkinvfqbhp806c2xggr2nzg2mw8q")))

