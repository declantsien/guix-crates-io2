(define-module (crates-io sh re shrev) #:use-module (crates-io))

(define-public crate-shrev-0.1.0 (c (n "shrev") (v "0.1.0") (d (list (d (n "shred") (r "^0.4") (d #t) (k 0)))) (h "0kqwq3iwpr6fnnnwi2d7b280z0z8210fdvdhsxmwpgr3a4q3bnsx")))

(define-public crate-shrev-0.1.1 (c (n "shrev") (v "0.1.1") (d (list (d (n "shred") (r "^0.4") (d #t) (k 0)))) (h "05iqa0n6igxvm3i5gmj06dxb4w40araibfm5sybybk8v0p7b976j")))

(define-public crate-shrev-0.2.0 (c (n "shrev") (v "0.2.0") (h "11x8b06s8xkppvb178rysg6knnig0zr93s1lqvns9hv3i6w1lkvc")))

(define-public crate-shrev-0.3.0 (c (n "shrev") (v "0.3.0") (h "0n3fj2l0dz98n0brxpf8fl8bxy51vdx358s05s53xdcvalsdjpgf")))

(define-public crate-shrev-0.3.1 (c (n "shrev") (v "0.3.1") (h "0b682qsvhp4xxg2zxvf3qck6c08hzim2yai5mnbyfd8rnm1qjlnl")))

(define-public crate-shrev-0.3.2 (c (n "shrev") (v "0.3.2") (h "136i4g077vs528kywqmrx69d6z5p2w44313nv2zzscgicr2fdnsf")))

(define-public crate-shrev-0.3.3 (c (n "shrev") (v "0.3.3") (h "1p48fhff3r0mn4j4ybazkv37knv7wj75vpfzlri9znif841zwpj4")))

(define-public crate-shrev-0.4.0 (c (n "shrev") (v "0.4.0") (h "0a61ayh9fwk5xqg6w6bz55vacl4idks4i3nyx3m7x3v146ms8wjy")))

(define-public crate-shrev-0.5.0 (c (n "shrev") (v "0.5.0") (h "12mwp82fka9c56d0d6iddgfra69ylfamhky52g2iw19awxmzjz35")))

(define-public crate-shrev-0.6.0 (c (n "shrev") (v "0.6.0") (h "1yr0i798ljkiclbznzb5xlzc1fixafh5iwf8jk4qk329s3mz1kzh")))

(define-public crate-shrev-0.7.0 (c (n "shrev") (v "0.7.0") (h "019dkgys4xmfghfrq7wf7jqrycppwaip92khfac5x6v54d4wj412")))

(define-public crate-shrev-0.8.0 (c (n "shrev") (v "0.8.0") (h "0n67bddpff72pwwh35z9rmz90dlwbpz4mcbxsfphn8jh14ns4g88")))

(define-public crate-shrev-0.8.1 (c (n "shrev") (v "0.8.1") (h "01qykmg0vx7kdi2k6bjy1fhnc7fhgy396dg71dzdqzsfykc9k8rn")))

(define-public crate-shrev-0.8.2 (c (n "shrev") (v "0.8.2") (h "0ks9fwss2c8l147s4hbmv3ggm25khx6a2pp42a1hagx0sl5vrg39")))

(define-public crate-shrev-1.0.0-alpha1 (c (n "shrev") (v "1.0.0-alpha1") (d (list (d (n "derivative") (r "^1.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 0)))) (h "0mvdczam4il06nyjpblawwh2fx1s6phl0cb7gc1nqkshabndnkva") (y #t)))

(define-public crate-shrev-1.0.0 (c (n "shrev") (v "1.0.0") (d (list (d (n "derivative") (r "^1.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 0)))) (h "1ri6lpz5v0c8dvzh552v8aixqf8rxnwnp73k9fjy7ah3axh722c0")))

(define-public crate-shrev-1.0.1 (c (n "shrev") (v "1.0.1") (d (list (d (n "derivative") (r "^1.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 0)))) (h "10m4kp6mh5d3hs3a6rhgk7cgn21lvcap5xdcrv1cvcx4c1pysq7c")))

(define-public crate-shrev-1.1.0 (c (n "shrev") (v "1.1.0") (h "1aysynh51rbyxp4hn8800jir4gpvpvgnjzjb0k3sadg3qg7rikcc")))

(define-public crate-shrev-1.1.1 (c (n "shrev") (v "1.1.1") (h "0zs3zw5inn151jyxh17szbz91zd7nx9ry1jbbdrrvbq3gq0jwxdm")))

(define-public crate-shrev-1.1.3 (c (n "shrev") (v "1.1.3") (h "06hbw5hxgyg4qnmwmf6bm3hi7pjdm7jm0d533ilvzwfw5wik7sm5")))

