(define-module (crates-io sh to shtola-markdown) #:use-module (crates-io))

(define-public crate-shtola-markdown-0.1.0 (c (n "shtola-markdown") (v "0.1.0") (d (list (d (n "comrak") (r "^0.6.2") (d #t) (k 0)) (d (n "shtola") (r "^0.1.0") (d #t) (k 0)))) (h "14wajkpy1pwn93bv7l0zwcqq7g3qld8xg4vrlar73701yaahds5x")))

(define-public crate-shtola-markdown-0.1.1 (c (n "shtola-markdown") (v "0.1.1") (d (list (d (n "comrak") (r "^0.6.2") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.1") (d #t) (k 2)) (d (n "shtola") (r "^0.1.1") (d #t) (k 0)))) (h "1mifdyscbnpfr2b7dbdcicc5gw17ryfi5sv4cfdl7jg2sz8r2bq9")))

