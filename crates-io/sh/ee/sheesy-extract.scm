(define-module (crates-io sh ee sheesy-extract) #:use-module (crates-io))

(define-public crate-sheesy-extract-1.0.1 (c (n "sheesy-extract") (v "1.0.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "sheesy-types") (r "^1.0.1") (d #t) (k 0)))) (h "12n5yzwan3mmalwhq1657jvq7j3hdwnvqrs2ylkyhzicjx9ws05j") (y #t)))

(define-public crate-sheesy-extract-2.0.0 (c (n "sheesy-extract") (v "2.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "sheesy-types") (r "^2.0.0") (d #t) (k 0)))) (h "1bil51xga7i7y4rzkfbs72xh7k9zrjg0wwk8rq1g2fpz3sw000ij") (y #t)))

(define-public crate-sheesy-extract-3.0.0 (c (n "sheesy-extract") (v "3.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "sheesy-types") (r "^3.0.0") (d #t) (k 0)))) (h "100x3bj9vp95xk3q89543y90ncx938hdhf688gzxmphh8vy9l2sn") (y #t)))

