(define-module (crates-io sh ee sheet) #:use-module (crates-io))

(define-public crate-sheet-0.1.0 (c (n "sheet") (v "0.1.0") (h "1qvm7y5pl98l730y6bgczwi0m5jj1zb1fhffb9rzp82d5bqr27k1") (y #t)))

(define-public crate-sheet-0.0.0 (c (n "sheet") (v "0.0.0") (h "11gpz94cs60bmdib8d10hrcadw19xdzqkzm1lq5g5i89w450v9hq")))

