(define-module (crates-io sh ee sheety) #:use-module (crates-io))

(define-public crate-sheety-0.1.0 (c (n "sheety") (v "0.1.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1qnk40971xhf61xai4av8hzaf4rqisjp4rhbhlgm9c1158iibhsw")))

