(define-module (crates-io sh ee sheety-cli) #:use-module (crates-io))

(define-public crate-sheety-cli-0.1.0 (c (n "sheety-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sheety") (r "^0.1.0") (d #t) (k 0)))) (h "07gw2cn1iwxi3nllnwcykh7w1b3i0vg7ivdivrsjfvgankhlddds")))

