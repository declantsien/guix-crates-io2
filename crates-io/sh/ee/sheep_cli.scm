(define-module (crates-io sh ee sheep_cli) #:use-module (crates-io))

(define-public crate-sheep_cli-0.2.0 (c (n "sheep_cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "sheep") (r "^0.2.1") (d #t) (k 0)))) (h "0jixjm9nqx4kz84bv1cmyzn4jz0vlx18cspvdp2c9acpz3hw9zh0")))

(define-public crate-sheep_cli-0.3.0 (c (n "sheep_cli") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "oxipng") (r "^2.2") (d #t) (k 0)) (d (n "png") (r "^0.15") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "sheep") (r "^0.3.0") (d #t) (k 0)))) (h "1qahzib32nqi610klk2bwvj08dlq59202y3rmg8lzc8bzvh3syjl")))

