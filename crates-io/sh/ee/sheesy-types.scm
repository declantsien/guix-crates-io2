(define-module (crates-io sh ee sheesy-types) #:use-module (crates-io))

(define-public crate-sheesy-types-1.0.1 (c (n "sheesy-types") (v "1.0.1") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "088pf2yih2d03k0iryrlsknqyfqc9fqaa385gqkjx5szkk1icg8j") (y #t)))

(define-public crate-sheesy-types-2.0.0 (c (n "sheesy-types") (v "2.0.0") (d (list (d (n "atty") (r "^0.2.6") (d #t) (k 0)) (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "mktemp") (r "^0.3.1") (d #t) (k 0)))) (h "05aryxq3f07r2im13r7p3dz74n5prfnwp8prh7w54f91iy690adk") (y #t)))

(define-public crate-sheesy-types-3.0.0 (c (n "sheesy-types") (v "3.0.0") (d (list (d (n "atty") (r "^0.2.6") (d #t) (k 0)) (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "mktemp") (r "^0.3.1") (d #t) (k 0)))) (h "1553ldfdg830vk34y17c16kjf6nkwkbnrzad5av50wdg39sa9bdy") (y #t)))

