(define-module (crates-io sh ee sheep) #:use-module (crates-io))

(define-public crate-sheep-0.2.1 (c (n "sheep") (v "0.2.1") (d (list (d (n "image") (r "^0.20") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0hb89s04b1hvvl0c8wxcba8wk7b15gm5qpsvpw9cxl27h3ymqyk4") (f (quote (("default" "amethyst") ("amethyst" "serde" "serde_derive"))))))

(define-public crate-sheep-0.3.0 (c (n "sheep") (v "0.3.0") (d (list (d (n "image") (r "^0.20") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "twox-hash") (r "^1.4.2") (d #t) (k 0)))) (h "065hsyz3gbxkf5x661k7f0761crj7asim1d6qvm2xpmwjcx2kpxj") (f (quote (("default" "amethyst") ("amethyst" "serde" "serde_derive"))))))

