(define-module (crates-io sh ow showie) #:use-module (crates-io))

(define-public crate-showie-1.0.0 (c (n "showie") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "1qcyflf83lx292r2jhn2cjplp9d9r7lvlm67h6dd28y4q1wgdl6v")))

(define-public crate-showie-1.0.1 (c (n "showie") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "1zldzamxj83w61z1dqmplzl8yh98milw4l85qr7js46q1h3rm8r7")))

