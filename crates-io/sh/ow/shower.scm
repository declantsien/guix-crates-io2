(define-module (crates-io sh ow shower) #:use-module (crates-io))

(define-public crate-shower-0.1.0 (c (n "shower") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "unindent") (r "^0.2.1") (d #t) (k 0)))) (h "16wlgqi595653c9ifxg4w48rvwgxw35alwnhrn78zf1sg616c56d")))

