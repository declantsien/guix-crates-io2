(define-module (crates-io sh ow show-path) #:use-module (crates-io))

(define-public crate-show-path-0.1.0 (c (n "show-path") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lm942c1y4438k8w6f8fl0s2a1k752yvcb1fapss0y7r4zn5cagg")))

(define-public crate-show-path-0.1.1 (c (n "show-path") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "0akkbgk7rfidzbz1a8c3nkacs97yr5ybp7mi5c4jzblx0sxnkrfb")))

