(define-module (crates-io sh ow show-option) #:use-module (crates-io))

(define-public crate-show-option-0.1.0 (c (n "show-option") (v "0.1.0") (d (list (d (n "lazy_format") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "lazy_format") (r "^2.0.0") (d #t) (k 2)))) (h "0bjy1mc5pyzfl4s09azb86116msk41ckyi9dhhpm7np0ykzp48i6") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:lazy_format"))))))

