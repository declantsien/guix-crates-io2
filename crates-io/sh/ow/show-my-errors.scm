(define-module (crates-io sh ow show-my-errors) #:use-module (crates-io))

(define-public crate-show-my-errors-0.1.0 (c (n "show-my-errors") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "112r637r6hqfj7g0xmshz7vf3nmvx065pvb1sqnfp30sz7g5ash1")))

(define-public crate-show-my-errors-0.1.1 (c (n "show-my-errors") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "1hx23r907ndjclx1rc9pij03h1faakgizkvx5ccj4c2a0zalklm2")))

(define-public crate-show-my-errors-0.1.2 (c (n "show-my-errors") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "1y9xpwlyg7672phsjcbhlwxm03n0xmg85yhbcpwzm6bnsjxfp70k") (y #t)))

(define-public crate-show-my-errors-0.1.3 (c (n "show-my-errors") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0zhccnv85f1iknwlzyaxv97i4aaiflpxrh3hmj750n7hnycqwli6")))

