(define-module (crates-io sh ow show-simplex) #:use-module (crates-io))

(define-public crate-show-simplex-0.9.0 (c (n "show-simplex") (v "0.9.0") (d (list (d (n "noise") (r "^0.5.1") (d #t) (k 0)) (d (n "schedule_recv") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.13") (d #t) (k 0)))) (h "1xrb4jnzd6av0cgrgqbnwfyppj6d7sfqjlj58803k8y9ynfbj12l")))

