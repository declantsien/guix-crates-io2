(define-module (crates-io sh ow show-bytes) #:use-module (crates-io))

(define-public crate-show-bytes-0.1.0 (c (n "show-bytes") (v "0.1.0") (h "1q1468ci43q1yyzba9w8kmqiw2mdzcwajfivc7hlldj12x11kxpa")))

(define-public crate-show-bytes-0.1.1 (c (n "show-bytes") (v "0.1.1") (h "11rhh7idiaaqzkaaqw8nkss85wvwd1svhvkxl42nlm9lx94q40xf")))

(define-public crate-show-bytes-0.2.0 (c (n "show-bytes") (v "0.2.0") (h "1f7pg68xh7jzj5858iil4gj1wyqgy98xdmm2vdarkldjd8lb073q")))

