(define-module (crates-io sh ow show-image-macros) #:use-module (crates-io))

(define-public crate-show-image-macros-0.8.0 (c (n "show-image-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0c8p34rkj65dq3zdm4d1abx1f1jl913x13mxx17z8lsxp34n5y61")))

(define-public crate-show-image-macros-0.8.1 (c (n "show-image-macros") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1j7y7vxwbr0rnal8n6y8126pr1hnhm5myj1lm4ni1gxhj88qyw3r")))

(define-public crate-show-image-macros-0.8.3 (c (n "show-image-macros") (v "0.8.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "show-image") (r "^0.8.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "04ncd47x6dz6wjsnzsv7wqj5sp0byp3bsgq44l76v2ffwriaa0xf")))

(define-public crate-show-image-macros-0.9.0 (c (n "show-image-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0glnaxbgkmfjvg5viyrs48ciskm9anjqbvnyrrnrs7n5hl2a4dkb")))

(define-public crate-show-image-macros-0.10.0 (c (n "show-image-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0pqj2c59jyxhycyp0rirg7pfs23qavbj5qisrqhkgnayqhr7vd04")))

(define-public crate-show-image-macros-0.11.0 (c (n "show-image-macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1a5h5ycy1znshqclj6c79csxicds7818g1vfhys7s6flammbfkys")))

(define-public crate-show-image-macros-0.12.0 (c (n "show-image-macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0gy40pq5ydxwcw5zw4hj9icb5956iq32812al7n8hz0g14bxn4z8")))

(define-public crate-show-image-macros-0.12.1 (c (n "show-image-macros") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1r4dd5s0g18d2c0hypsg53khl9dqcxgvclh7xk2xxry3xz8l5yy9")))

(define-public crate-show-image-macros-0.12.2 (c (n "show-image-macros") (v "0.12.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "19jv8bkn33lz9dap0jnqkj6m1cm9ziwkwhr7n1581fr86jpqzx09")))

(define-public crate-show-image-macros-0.12.3 (c (n "show-image-macros") (v "0.12.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "19fyw003zlqgmwmf9ar9y326ilv66kzngn30ak4j3ccbwahpp0v1")))

