(define-module (crates-io sh ow show) #:use-module (crates-io))

(define-public crate-show-0.1.0 (c (n "show") (v "0.1.0") (h "0dlrs8wdsgwizfl5cr4n0d82xpk5bbnc3ssh4iwdinnb3bh6r47i")))

(define-public crate-show-0.1.1 (c (n "show") (v "0.1.1") (h "065lfjylfmh2a2gmdl4g7d02l75k5w4hc3w3x6wg37k565p46bxd")))

