(define-module (crates-io sh ow show-direnv-diff) #:use-module (crates-io))

(define-public crate-show-direnv-diff-0.1.0 (c (n "show-direnv-diff") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (d #t) (k 0)) (d (n "json") (r "^0.12.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "1dw79y43628w4diq8aplf48nzj1cws0832gpxxawy23lrmkqv8f2")))

