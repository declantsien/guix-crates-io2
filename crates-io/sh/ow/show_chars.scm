(define-module (crates-io sh ow show_chars) #:use-module (crates-io))

(define-public crate-show_chars-0.1.0 (c (n "show_chars") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "fl2rust") (r "^0") (d #t) (k 1)) (d (n "fltk") (r "^1") (d #t) (k 0)) (d (n "fltk-table") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1b8f6j8blqn3l78gfs94zw4868ns698cigjrjdnr0pk5nk2bcwsw") (r "1.70.0")))

