(define-module (crates-io sh ot shotwellvfs) #:use-module (crates-io))

(define-public crate-shotwellvfs-0.1.0 (c (n "shotwellvfs") (v "0.1.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "fuse") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "sqlite") (r "^0.23") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0bidan3jf2cqpfj5p08dl117fiy6z0jcc30vy3088ig9kmp60j6h")))

