(define-module (crates-io sh ot shotgun) #:use-module (crates-io))

(define-public crate-shotgun-1.1.6 (c (n "shotgun") (v "1.1.6") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.15") (f (quote ("png_codec"))) (k 0)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "x11") (r "^2.15") (f (quote ("xlib" "xrandr"))) (d #t) (k 0)))) (h "1g1hvbknmxpb7pf9jlk2465kmprbphpsn82gw7xgpmk5jdcskd6y")))

(define-public crate-shotgun-1.2.0 (c (n "shotgun") (v "1.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.19") (f (quote ("png_codec"))) (k 0)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib" "xrandr"))) (d #t) (k 0)))) (h "1zsjaq5zda3b5scg3arv2krllg8wj5f1v2ga9x57ncg645qb77q4")))

(define-public crate-shotgun-2.0.0 (c (n "shotgun") (v "2.0.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.19") (f (quote ("png_codec"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib" "xrandr"))) (d #t) (k 0)))) (h "1pc6x5knh5b02k4w94vay9kw2hpakwk3nxb5qr767dn023lxqhny")))

(define-public crate-shotgun-2.0.1 (c (n "shotgun") (v "2.0.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.19") (f (quote ("png_codec"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib" "xrandr"))) (d #t) (k 0)))) (h "07gv9cva53fvsgfin467njff7abw69fpf24x0a5ihdmiicgnlr1m")))

(define-public crate-shotgun-2.1.0 (c (n "shotgun") (v "2.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.21") (f (quote ("png_codec" "pnm"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib" "xrandr"))) (d #t) (k 0)))) (h "1hz65s56sjqpwg7whna5h5xgm01ikarb5kwvih07rcrdgxk4ppvg")))

(define-public crate-shotgun-2.2.0 (c (n "shotgun") (v "2.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.22") (f (quote ("png_codec" "pnm"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib" "xrandr"))) (d #t) (k 0)))) (h "17jk6xy7fhj6iiz5rjsyprr8xhvn7w24l8fc6rxmrrwjjz5wjl5i")))

(define-public crate-shotgun-2.2.1 (c (n "shotgun") (v "2.2.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png" "pnm"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib" "xrandr"))) (d #t) (k 0)))) (h "00sn3iz7x3x69snpcxb0syppvmv04wv4wxzd0990v2z6sq4dqydc")))

(define-public crate-shotgun-2.3.0 (c (n "shotgun") (v "2.3.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png" "pnm"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib" "xrandr"))) (d #t) (k 0)))) (h "03vqg7xii5i3slhqbigill18c10p4cvlzdl5h06284iyp3jyqzka")))

(define-public crate-shotgun-2.3.1 (c (n "shotgun") (v "2.3.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png" "pnm"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib" "xrandr"))) (d #t) (k 0)))) (h "1yafmkhkzarfld1q8cfad8r4mwwcnz99b3cgw3a5fb3kv3d30bil")))

(define-public crate-shotgun-2.4.0 (c (n "shotgun") (v "2.4.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (f (quote ("png" "pnm"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.21.0") (f (quote ("xlib" "xrandr"))) (d #t) (k 0)))) (h "1j4khqsbmnqfr7r5dwzyckf782vaqa3rb891cfr1pq2ix0il5z5q")))

(define-public crate-shotgun-2.5.0 (c (n "shotgun") (v "2.5.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (f (quote ("png" "pnm"))) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "x11rb") (r "^0.11.1") (f (quote ("randr"))) (d #t) (k 0)))) (h "0jc4119k5glr0y5fnqjq7laqnix6f5baa9kwpvqw9jbpm8awvgs5")))

(define-public crate-shotgun-2.5.1 (c (n "shotgun") (v "2.5.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (f (quote ("png" "pnm"))) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "x11rb") (r "^0.11.1") (f (quote ("randr"))) (d #t) (k 0)))) (h "05gfp75kg9011qavs4m7fz0v4dsbpr71pa8bv74lis89zbajv7zj")))

