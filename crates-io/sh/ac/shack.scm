(define-module (crates-io sh ac shack) #:use-module (crates-io))

(define-public crate-shack-0.1.0 (c (n "shack") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.38") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 0)))) (h "0h1r6zrmyj5vinl717dyiisxdm0gsca9i0m85jzmv2khmjmyi4g5")))

(define-public crate-shack-0.1.1 (c (n "shack") (v "0.1.1") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.38") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 0)))) (h "05hdgs5p978dlml9c25gjjj4yxy5qwzly0bs9aqz3zz6l2j93wp3")))

