(define-module (crates-io sh er sherlog) #:use-module (crates-io))

(define-public crate-sherlog-0.8.0 (c (n "sherlog") (v "0.8.0") (d (list (d (n "cairo-rs") (r "^0.9.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "gdk") (r "^0.13.2") (d #t) (k 0)) (d (n "gio") (r "^0.9.1") (d #t) (k 0)) (d (n "glib") (r "^0.10.3") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "gtk") (r "^0.9.2") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(windows)") (k 1)) (d (n "zip") (r "^0.5.9") (f (quote ("deflate"))) (k 0)))) (h "1i8vggl8gzmcfwp39r7n3kzxcj3408qcx83m6vrv5sfymq8agipw")))

