(define-module (crates-io sh er sheroz) #:use-module (crates-io))

(define-public crate-sheroz-0.1.0 (c (n "sheroz") (v "0.1.0") (d (list (d (n "cipher_magma") (r "^0.3") (d #t) (k 0)) (d (n "tick_counter") (r "^0.3") (d #t) (k 0)))) (h "073ifwq1cshx7c2jlqw7rbhb0xfklvp6z7k0ppy9dclp248r4kax")))

