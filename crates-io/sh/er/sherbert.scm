(define-module (crates-io sh er sherbert) #:use-module (crates-io))

(define-public crate-sherbert-0.1.0 (c (n "sherbert") (v "0.1.0") (h "1xw4ax8crfc3ar4d6a8ch14q8fnxd22zcznrbb7hqvyjm0jbfaj8")))

(define-public crate-sherbert-0.2.0 (c (n "sherbert") (v "0.2.0") (h "1yk7c59hm446lhsq437ndj8jr17dxjfp0cx7q6whkjnd1y1zhbcm")))

