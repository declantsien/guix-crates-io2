(define-module (crates-io sh oj shoji) #:use-module (crates-io))

(define-public crate-shoji-0.0.0 (c (n "shoji") (v "0.0.0") (h "0c9srvz3z719s21ckjrxpa1igfmzkhmdvddd1bq3yyflj43w7gdx")))

(define-public crate-shoji-0.0.1 (c (n "shoji") (v "0.0.1") (d (list (d (n "generational-arena") (r "^0.2") (d #t) (k 0)))) (h "0b4np1zpsj99p6gaqfxjxrfmqwjc02jkk9cm1inhq3pz5a0vj74l")))

(define-public crate-shoji-0.0.2 (c (n "shoji") (v "0.0.2") (d (list (d (n "generational-arena") (r "^0.2") (d #t) (k 0)))) (h "15vv0nsyrbmg1wrgxzzjw76yzjsd1zl3b9m6m0kpy54krsfrd7vj")))

(define-public crate-shoji-0.0.3 (c (n "shoji") (v "0.0.3") (d (list (d (n "generational-arena") (r "^0.2") (d #t) (k 0)))) (h "1xbp597ji4akrymyqxhhx7wzhmxkvmzbyw36hjdy5yffdbxlppxf")))

(define-public crate-shoji-0.0.4 (c (n "shoji") (v "0.0.4") (d (list (d (n "generational-arena") (r "^0.2") (d #t) (k 0)))) (h "1r9pqf5sg0dcqp0p0invcvy9dlflrihy41qsf6ciryxg8wz6bnxc")))

(define-public crate-shoji-0.0.5 (c (n "shoji") (v "0.0.5") (d (list (d (n "generational-arena") (r "^0.2") (d #t) (k 0)))) (h "1drmv34hqv41d0vrrc7phk0ffn9p2dsxfbz333ccy9qqkq4b0bhi")))

(define-public crate-shoji-0.0.6 (c (n "shoji") (v "0.0.6") (d (list (d (n "generational-arena") (r "^0.2") (d #t) (k 0)))) (h "0cc0f3jy03bswwi0k649272sij03d4i9jz48c7nn1b1bp7i116wn")))

(define-public crate-shoji-0.1.0 (c (n "shoji") (v "0.1.0") (d (list (d (n "generational-arena") (r "^0.2") (d #t) (k 0)))) (h "051vhfxmbahbf7mq9qmy61vz4yld711nxaw9fmyqgj3x24xkfy4y")))

