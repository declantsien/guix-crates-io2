(define-module (crates-io sh ra shrapnel) #:use-module (crates-io))

(define-public crate-shrapnel-0.1.0-alpha.29 (c (n "shrapnel") (v "0.1.0-alpha.29") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1zsl9bi14zy4401zj1p4ny1bnf1kwwkf90gs3k9flzvhrrg2qfsl")))

