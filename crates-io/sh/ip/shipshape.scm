(define-module (crates-io sh ip shipshape) #:use-module (crates-io))

(define-public crate-shipshape-0.1.0 (c (n "shipshape") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13.5") (d #t) (k 0)) (d (n "hyper-socket") (r "^0.2.0") (d #t) (k 0)))) (h "1j1f1519rrxadxwir93bfcr283bqv6ray0cnwgzy5ndsbi950lv2")))

(define-public crate-shipshape-0.1.1 (c (n "shipshape") (v "0.1.1") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13.5") (d #t) (k 0)) (d (n "hyper-socket") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0lvxr38y2a692yshyrjmmly1svshr0xj35fb623vnhiapcmpgygv")))

