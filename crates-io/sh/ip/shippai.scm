(define-module (crates-io sh ip shippai) #:use-module (crates-io))

(define-public crate-shippai-0.1.0 (c (n "shippai") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0l55f9admzlj7kqyr6wj5q8qd7dh9s2x7bf6j2g05zd6ckbgwh1d")))

(define-public crate-shippai-0.1.1 (c (n "shippai") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "1plfwizxqnr2q7f9lycyh6623j7aq41nzfav8dyqb5fc15mxfhn3")))

(define-public crate-shippai-0.2.0 (c (n "shippai") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "shippai_derive") (r "^0.2") (d #t) (k 0)))) (h "192qnbwy4ywrbcaz0wlxddw9xbiwngfryz89nr76kz95ylxzf1cf")))

(define-public crate-shippai-0.2.1 (c (n "shippai") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "shippai_derive") (r "^0.2") (d #t) (k 0)))) (h "0nf087m5a3dnnajbiqhs01l9hw4l36qm4d1v2p6ik3q6cn2xd1fp")))

(define-public crate-shippai-0.2.2 (c (n "shippai") (v "0.2.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "shippai_derive") (r "^0.2") (d #t) (k 0)))) (h "140vw0ly8s5q9x66h9vc0q8hnaf0bmbrgf9nkl3xij863ssaxyf6")))

(define-public crate-shippai-0.2.3 (c (n "shippai") (v "0.2.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "shippai_derive") (r "^0.2") (d #t) (k 0)))) (h "1pjyicf1w58jn3fxz6x62hmym9k7r9wavvgxcwfrdslmk4g2kf3b")))

(define-public crate-shippai-0.2.4 (c (n "shippai") (v "0.2.4") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "shippai_derive") (r "^0.2") (d #t) (k 0)))) (h "0x4f4k214mdarrj8qz69c8vaqn5kcikkka42md2jzjrqgahcxdps")))

(define-public crate-shippai-0.3.0 (c (n "shippai") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "shippai_derive") (r "^0.3") (d #t) (k 0)))) (h "1xw6w40hpfhm3yq42k73f0plk6v748g5k5ivl45bdgcxswljnpbl")))

(define-public crate-shippai-0.3.1 (c (n "shippai") (v "0.3.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "shippai_derive") (r "^0.3") (d #t) (k 0)))) (h "1pd1b3viaxayvsjbgg1pm0hxmq86lrn8pym1pn7w13h010biks2j")))

(define-public crate-shippai-0.3.2 (c (n "shippai") (v "0.3.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "shippai_derive") (r "^0.3") (d #t) (k 0)))) (h "0rjsx1dnv4dk7hh25aqzpijy2k64jnqjmnf2sl9j0g83gqkx5sab")))

