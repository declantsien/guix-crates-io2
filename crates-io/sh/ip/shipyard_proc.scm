(define-module (crates-io sh ip shipyard_proc) #:use-module (crates-io))

(define-public crate-shipyard_proc-0.1.0 (c (n "shipyard_proc") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1258f2w21bvv3qnyslw0q0na8r0k31pdzvxm5rsn5nyha313gyr8")))

(define-public crate-shipyard_proc-0.2.0 (c (n "shipyard_proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "07lgi9qakdy1a52ylwbdx3wvv69fagbh21q1ifk9psw29miv9ln6")))

(define-public crate-shipyard_proc-0.2.1 (c (n "shipyard_proc") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0xsy9073fs6rbpr6f5ardc5jqnl86kiwiqq4rb0l0d3lkabwwh21")))

(define-public crate-shipyard_proc-0.2.2 (c (n "shipyard_proc") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bplff3bs6jmkwsllirvpzd0clg5lyzvx0ill1l7r96s3r0ws5p0")))

(define-public crate-shipyard_proc-0.3.0-alpha.1 (c (n "shipyard_proc") (v "0.3.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0jaj89rmr6lli1fzqdmkzhkgz9v7vmh51kzij0vqj0ipc8sxg973")))

(define-public crate-shipyard_proc-0.3.0-alpha.2 (c (n "shipyard_proc") (v "0.3.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "13l6xa3q42lv2bn1fvd14ncl38jm17hf8iab670f1ghj9wiaqp8g")))

(define-public crate-shipyard_proc-0.3.0 (c (n "shipyard_proc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "137h2lyp049wpyfcqnjkbvz6gk353drvbkxmk20lcbjqp7s4gf1y")))

