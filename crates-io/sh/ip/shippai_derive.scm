(define-module (crates-io sh ip shippai_derive) #:use-module (crates-io))

(define-public crate-shippai_derive-0.2.0 (c (n "shippai_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0pg02b1b6qkcfvf3lcgdfh5nrnrj0vx5bi4l8ycffhyjpl7hz1ph")))

(define-public crate-shippai_derive-0.2.1 (c (n "shippai_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0xn3spqcrnwwd431p4784qj2afy6l88zdr39x0xr3brm9my98m3x")))

(define-public crate-shippai_derive-0.2.2 (c (n "shippai_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0qa9kfhk0d41hfkd3ff6fq2mqvjn90rqbhwfj29cczxff1hbva67")))

(define-public crate-shippai_derive-0.2.3 (c (n "shippai_derive") (v "0.2.3") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)) (d (n "synstructure") (r "^0.8") (d #t) (k 0)))) (h "1rw5kn5vzh6alba0c0q6wn9kkxn7gn6fpcqjffm0fr0f7l2a203x")))

(define-public crate-shippai_derive-0.2.4 (c (n "shippai_derive") (v "0.2.4") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)) (d (n "synstructure") (r "^0.8") (d #t) (k 0)))) (h "0wgmmmgd66w4zcq6yklmcv8i7gw769dshlhqhxnhiz3vaxm33qq7")))

(define-public crate-shippai_derive-0.3.0 (c (n "shippai_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)) (d (n "synstructure") (r "^0.8") (d #t) (k 0)))) (h "0wh6m5c8pdvgc1hbapbjfgjq2s0dyscpgab6pkcf8a54jlb8fss0")))

(define-public crate-shippai_derive-0.3.1 (c (n "shippai_derive") (v "0.3.1") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)) (d (n "synstructure") (r "^0.8") (d #t) (k 0)))) (h "1cl2l6w58pr27b4m73ig68zgwz2x7a3g6qrz2c3mr637rwwvfvgl")))

(define-public crate-shippai_derive-0.3.2 (c (n "shippai_derive") (v "0.3.2") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)) (d (n "synstructure") (r "^0.8") (d #t) (k 0)))) (h "1bi280f95imz7jmbjlhhs3dg7js91h6z2rav6grhmfa4jg1vnmbm")))

