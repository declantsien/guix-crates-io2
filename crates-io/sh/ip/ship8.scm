(define-module (crates-io sh ip ship8) #:use-module (crates-io))

(define-public crate-ship8-0.1.0 (c (n "ship8") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (f (quote ("unsafe_textures"))) (d #t) (k 0)))) (h "09fnfvx8n4v6f419ralrqvqlblikwz7yq9rb9wd6vlszqb7yny3n") (y #t)))

(define-public crate-ship8-0.1.1 (c (n "ship8") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (f (quote ("unsafe_textures"))) (d #t) (k 0)))) (h "16mb046mxb326nk0926k5vzkk37pn2njjavqzwac8fhanl1p66r9") (y #t)))

(define-public crate-ship8-0.1.2 (c (n "ship8") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (f (quote ("unsafe_textures"))) (d #t) (k 0)))) (h "0npgszlwq1kw3slwlijhs3mdhdj16pzw2djainhgzfaajmmr6h6z") (y #t)))

(define-public crate-ship8-0.1.4 (c (n "ship8") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (f (quote ("unsafe_textures"))) (d #t) (k 0)))) (h "1csvf8543ai8gkddcssafldgrb9pj1gdplzq06f4vljd7crh6bws") (y #t)))

(define-public crate-ship8-0.1.5 (c (n "ship8") (v "0.1.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (f (quote ("unsafe_textures"))) (d #t) (k 0)))) (h "0np6b2l6x5afwsqar4dmxs2vyrynn71zybqh581rbka7ab92njh8") (y #t)))

(define-public crate-ship8-0.2.0 (c (n "ship8") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (f (quote ("unsafe_textures"))) (d #t) (k 0)))) (h "0znviyq81zx1mgwvrcic2fwgh94hxx9bvydy5702dlhwwam36zlv") (y #t)))

