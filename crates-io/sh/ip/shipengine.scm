(define-module (crates-io sh ip shipengine) #:use-module (crates-io))

(define-public crate-shipengine-0.0.1 (c (n "shipengine") (v "0.0.1") (h "1k5yy3fpkl82w9vilqgn5bdlsnrl38i5p6zp1xg0x8417y9jp4vq")))

(define-public crate-shipengine-0.0.2 (c (n "shipengine") (v "0.0.2") (h "0yrd5jq4jmqwdxiwnpngsqr9bhchvsnsfmsmdrivwgbfv6znbjl8")))

