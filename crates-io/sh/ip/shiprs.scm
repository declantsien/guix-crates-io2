(define-module (crates-io sh ip shiprs) #:use-module (crates-io))

(define-public crate-shiprs-0.1.0 (c (n "shiprs") (v "0.1.0") (h "0l0fj7smxn2fz9ni08kqs3i17bq3hz67w5hlsy8yl3v4sxfivchg")))

(define-public crate-shiprs-0.1.1 (c (n "shiprs") (v "0.1.1") (h "14j7frq4v2j0sp2vldkmc49nhl81b8v2fzln2fian8bmdd67lgqm")))

