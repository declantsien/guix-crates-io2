(define-module (crates-io sh ib shiba-preview) #:use-module (crates-io))

(define-public crate-shiba-preview-0.0.0 (c (n "shiba-preview") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "open") (r "^3.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "wry") (r "^0.21.1") (d #t) (k 0)))) (h "0wkr77b2jrav6j4hk8jb4yd8d1siilqrzhfkpj30fwwqy6c4h2yz")))

