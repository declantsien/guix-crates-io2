(define-module (crates-io sh ib shibuichi) #:use-module (crates-io))

(define-public crate-shibuichi-0.0.1 (c (n "shibuichi") (v "0.0.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "15qyhdh98brsd9sxlbma0i77msf9nfg4wgqn65s7z5pzd92zkzm8")))

(define-public crate-shibuichi-0.1.0 (c (n "shibuichi") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1639zfkv09z15y5bad2qlkwrdiwd7352mjg003cfyryzdqhxgg83")))

(define-public crate-shibuichi-0.1.1 (c (n "shibuichi") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "16l3gbj2xfy8xc3bjk3aqxh13v41i53i6l2zb750qy3yzf480cf3")))

(define-public crate-shibuichi-0.1.2 (c (n "shibuichi") (v "0.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1pwxhqz50qgw7hmgflrcg57gz8ziyn0gdpd9fq7y5iisbwjqh2yb") (f (quote (("vendored-openssl" "git2/vendored-openssl") ("vendored-libgit2" "git2/vendored-libgit2") ("default"))))))

(define-public crate-shibuichi-0.1.3 (c (n "shibuichi") (v "0.1.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "git2") (r "^0.16") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0bca6m0k09ygckydanvzq18655a21bysvd3vsdrn3dgwh2i05fmx") (f (quote (("vendored-openssl" "git2/vendored-openssl") ("vendored-libgit2" "git2/vendored-libgit2") ("default"))))))

(define-public crate-shibuichi-0.1.4 (c (n "shibuichi") (v "0.1.4") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "git2") (r "^0.16") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1lnabwk9ja3x4fiq2q2z954d08pg002mbmlg9zjvwd27y0q1ggs2") (f (quote (("vendored-openssl" "git2/vendored-openssl") ("vendored-libgit2" "git2/vendored-libgit2") ("default"))))))

(define-public crate-shibuichi-0.1.5 (c (n "shibuichi") (v "0.1.5") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "03rs30hvx8fsqjc1brxy14cg7vhs3dm0hqznlbay0va2wx8kmpaz") (f (quote (("vendored-openssl" "git2/vendored-openssl") ("vendored-libgit2" "git2/vendored-libgit2") ("default"))))))

(define-public crate-shibuichi-0.1.6 (c (n "shibuichi") (v "0.1.6") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0anqjxryamrj44na3x0bvx6mc032727l7g8fxrnwj6yn0a280qfz") (f (quote (("vendored-openssl" "git2/vendored-openssl") ("vendored-libgit2" "git2/vendored-libgit2") ("default"))))))

(define-public crate-shibuichi-0.1.7 (c (n "shibuichi") (v "0.1.7") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0lffa8h4a12a6nwm99jwb1jvgd7nqdxrknr4cpgphlab3hikrnqg") (f (quote (("vendored-openssl" "git2/vendored-openssl") ("vendored-libgit2" "git2/vendored-libgit2") ("default"))))))

