(define-module (crates-io sh ib shiba) #:use-module (crates-io))

(define-public crate-shiba-0.1.0 (c (n "shiba") (v "0.1.0") (h "09jgvvy6mbpay2cp9rqwl70kvcpbg2r6dzwdphzax3l2chyg6bx9")))

(define-public crate-shiba-0.1.1 (c (n "shiba") (v "0.1.1") (h "0qpgiry9zcf6xyk7lfggfjand7dynf3wgd0dw9yy1nxgrb9rksa6")))

