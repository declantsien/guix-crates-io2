(define-module (crates-io sh ib shibboleth) #:use-module (crates-io))

(define-public crate-shibboleth-0.1.0 (c (n "shibboleth") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sqlite") (r "^0.25.0") (d #t) (k 0)) (d (n "zoea") (r "^0.0.8") (d #t) (k 0)))) (h "0v82qxxk0sms6d70gp6m4yn5ihz3ppmsk81yrvzkxr72sx71xdw0") (y #t)))

(define-public crate-shibboleth-0.1.1 (c (n "shibboleth") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sqlite") (r "^0.25.0") (d #t) (k 0)) (d (n "zoea") (r "^0.0.8") (d #t) (k 0)))) (h "1i9fv6ldkwfq12idf1x1vd01m601ahdx81304a2qf27mxz682ma5") (y #t)))

(define-public crate-shibboleth-0.1.2 (c (n "shibboleth") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.25.0") (d #t) (k 0)) (d (n "zoea") (r "^0.0.8") (d #t) (k 0)))) (h "0jzr3hgwhwd92s3z3qpkx6fi9vx15ciywfqmpz35m9c2zvh8yyjs") (y #t)))

(define-public crate-shibboleth-0.1.3 (c (n "shibboleth") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.25.0") (d #t) (k 0)) (d (n "zoea") (r "^0.0.8") (d #t) (k 0)))) (h "05zfzh7qm7mv1ygdxgkw16s3ds47193q2bhdrp995pc5xxm5l1hn") (y #t)))

