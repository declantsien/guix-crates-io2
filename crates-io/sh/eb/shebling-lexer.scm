(define-module (crates-io sh eb shebling-lexer) #:use-module (crates-io))

(define-public crate-shebling-lexer-0.0.0 (c (n "shebling-lexer") (v "0.0.0") (d (list (d (n "miette") (r "^5.9.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "shebling-ast") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "08p6zxlw9lyn2sjh8kp2a47l4jnmrymsmv1rmfzh0kac7xpgmag2")))

