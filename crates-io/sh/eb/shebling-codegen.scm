(define-module (crates-io sh eb shebling-codegen) #:use-module (crates-io))

(define-public crate-shebling-codegen-0.0.0 (c (n "shebling-codegen") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "11jlqph77k1sqswqb8rc4q81fwc0343sfgwl9rjq846c4bjm8rjx")))

