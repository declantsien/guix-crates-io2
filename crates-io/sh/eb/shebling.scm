(define-module (crates-io sh eb shebling) #:use-module (crates-io))

(define-public crate-shebling-0.0.0 (c (n "shebling") (v "0.0.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "shebling-codegen") (r "=0.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1a87f57gm3cjls4m0cl51dkhfz114pab6h23392fjzk2cdq0m5ia")))

