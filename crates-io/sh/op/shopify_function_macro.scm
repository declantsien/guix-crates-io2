(define-module (crates-io sh op shopify_function_macro) #:use-module (crates-io))

(define-public crate-shopify_function_macro-0.1.0 (c (n "shopify_function_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zyjxsv1zmw98x0m2hb3v2vi59if0nvc0b9zjv8lffv53grhzmhk")))

(define-public crate-shopify_function_macro-0.2.0 (c (n "shopify_function_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rbz1yd89w20b2pxbqdnrgq7pxf7nrfn07svgrjac2b19b0jp0l6")))

(define-public crate-shopify_function_macro-0.2.1 (c (n "shopify_function_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qg7g2caf41gs78w7h6133pj6y8lqfdnrz0rpg91h4nmr2jm8hw4")))

(define-public crate-shopify_function_macro-0.2.2 (c (n "shopify_function_macro") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p75cwnk4yvc8m4prly45y3pi55fcaq576x3amzyxylbb6nqbxx8")))

(define-public crate-shopify_function_macro-0.2.3 (c (n "shopify_function_macro") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dls9c8i7p2sa28kmj45ql8q2wirlhs03k2qq3c7xcpansclhz94")))

(define-public crate-shopify_function_macro-0.2.4 (c (n "shopify_function_macro") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h3p3dic4jz4wvgfh00miczng7z8akk8348ms020ndplyl1s0mik")))

(define-public crate-shopify_function_macro-0.2.5 (c (n "shopify_function_macro") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m4ik0j33xib9k38b5a12b2cwwwhmpbfxsfbgmkq4mphgm5lvd76")))

(define-public crate-shopify_function_macro-0.3.0 (c (n "shopify_function_macro") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lw4l71knj62m21hqzcn60r5rlb17pyw6rh0kdgkmfpxkamg7cq8")))

(define-public crate-shopify_function_macro-0.4.0 (c (n "shopify_function_macro") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zjvshf9nc9fb4a098j4jid2gg6g7vk7p8sh63mfgbfn4vmlvxrg")))

(define-public crate-shopify_function_macro-0.5.0 (c (n "shopify_function_macro") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12kpgln31596lqb7hsva5wb2gx3sqbvlf6yzfim914lv95fnh0f9")))

(define-public crate-shopify_function_macro-0.6.0 (c (n "shopify_function_macro") (v "0.6.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zv2bdhp4fdb82rkk6yw9d905f0wpswjnnbd38844zncxfqia9ww")))

(define-public crate-shopify_function_macro-0.7.0 (c (n "shopify_function_macro") (v "0.7.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "033h1g80wcmxv1j4xhs2c0832kq85w94nr05g6wy5y7lj4m5qhkj")))

(define-public crate-shopify_function_macro-0.7.1 (c (n "shopify_function_macro") (v "0.7.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "graphql_client_codegen") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rj3ylbsllv9l7n4679mrxqlr0m4ja4dp9pajlwpc555awy4g956") (y #t)))

(define-public crate-shopify_function_macro-0.8.0 (c (n "shopify_function_macro") (v "0.8.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "graphql_client_codegen") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15i4h5nhbv4wc5zp0hwalnzbd2lv5hknpqxw71q2fi6c8fsfpy2h")))

