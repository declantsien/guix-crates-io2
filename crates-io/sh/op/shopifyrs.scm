(define-module (crates-io sh op shopifyrs) #:use-module (crates-io))

(define-public crate-shopifyrs-0.0.1 (c (n "shopifyrs") (v "0.0.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac-sha") (r "^0.6") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0sn6ayry21p2bsjxd2f72b90d204v3ap6zbwpyyl77ks0186xlml")))

(define-public crate-shopifyrs-0.0.2 (c (n "shopifyrs") (v "0.0.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac-sha") (r "^0.6") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "04lc9vii2kq6zyf7jx4yrnzzs4h3apfc72i2ysv7mfiz2999l6y6")))

(define-public crate-shopifyrs-0.0.3 (c (n "shopifyrs") (v "0.0.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac-sha") (r "^0.6") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0i43hhzsbnaahq2yxxgwazf4c0dk486mvdc8iwvgka5dlrgz9mr6")))

