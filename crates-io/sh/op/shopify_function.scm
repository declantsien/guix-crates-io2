(define-module (crates-io sh op shopify_function) #:use-module (crates-io))

(define-public crate-shopify_function-0.1.0 (c (n "shopify_function") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.13.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0kvhk7avgv04hggyqbw2ff4xxpijnjzkwf234nky1hmld1y5vn70")))

(define-public crate-shopify_function-0.2.0 (c (n "shopify_function") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.2.0") (d #t) (k 0)))) (h "11i1dnshw1jk0cqaiaisz8agrk17i2rhzy7kq0b3j6vhkijr8773")))

(define-public crate-shopify_function-0.2.1 (c (n "shopify_function") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.2.1") (d #t) (k 0)))) (h "1946k9jg0gdwwj1gfxb8ydy7z5wgkghl6857vx05g2d2ycc989np")))

(define-public crate-shopify_function-0.2.2 (c (n "shopify_function") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.2.2") (d #t) (k 0)))) (h "1vrydyc2f50hyawjq56n0pylf37ns60adksrz2304xgfnfrimqx3")))

(define-public crate-shopify_function-0.2.3 (c (n "shopify_function") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.2.3") (d #t) (k 0)))) (h "13yglazifyvpnq2zmsqbzn9h1c515k75hyi7xws5fzl31i2n5asi")))

(define-public crate-shopify_function-0.2.4 (c (n "shopify_function") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.2.4") (d #t) (k 0)))) (h "0assmffg8pq3fwi2kfl065nzyndpk2pqbwiba3m0anrl0pfplipl")))

(define-public crate-shopify_function-0.2.5 (c (n "shopify_function") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "graphql_client") (r "^0.13.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1yaqxxipy6ppd9smqq1rdycl3pql6yhfbgc8j0sb4g30m5hdfkdf")))

(define-public crate-shopify_function-0.3.0 (c (n "shopify_function") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "graphql_client") (r "^0.13.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.3.0") (d #t) (k 0)))) (h "0k9l0f2w43lkaq2x5z7cvb01d3mas1pn54wfxlypy43w4lgagixw")))

(define-public crate-shopify_function-0.4.0 (c (n "shopify_function") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "graphql_client") (r "^0.13.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.4.0") (d #t) (k 0)))) (h "1659raddjrb4cdjcdjc5yvj9w5qncziflpxaanlfb9vv1yh774i4")))

(define-public crate-shopify_function-0.5.0 (c (n "shopify_function") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "graphql_client") (r "^0.13.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.5.0") (d #t) (k 0)))) (h "0i008hfcskjypvjici2qgr3z61s5sdq7avyf4j75h7zq2k34y6xl")))

(define-public crate-shopify_function-0.6.0 (c (n "shopify_function") (v "0.6.0") (d (list (d (n "graphql_client") (r "^0.13.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.6.0") (d #t) (k 0)))) (h "0qg66l0qq4x1vlfgvzk86j8wbnrzdkmfn5b3nq28c3xbskrhbj23")))

(define-public crate-shopify_function-0.7.0 (c (n "shopify_function") (v "0.7.0") (d (list (d (n "graphql_client") (r "^0.13.0") (d #t) (k 2)) (d (n "ryu") (r "^1") (f (quote ("small"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.7.0") (d #t) (k 0)))) (h "1qy5kfcs9wg6w0jskxjp27v5f12hd9lj13wy25xjzj2d6f7dl6xc")))

(define-public crate-shopify_function-0.7.1 (c (n "shopify_function") (v "0.7.1") (d (list (d (n "graphql_client") (r "^0.14.0") (d #t) (k 2)) (d (n "graphql_client_codegen") (r "^0.14.0") (d #t) (k 2)) (d (n "ryu") (r "^1") (f (quote ("small"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.7.1") (d #t) (k 0)))) (h "12byibl3wi2iqkrrxrgzmiry97lpsnw2k90adz33g4cqlij02kci") (y #t)))

(define-public crate-shopify_function-0.8.0 (c (n "shopify_function") (v "0.8.0") (d (list (d (n "graphql_client") (r "^0.14.0") (d #t) (k 2)) (d (n "graphql_client_codegen") (r "^0.14.0") (d #t) (k 2)) (d (n "ryu") (r "^1") (f (quote ("small"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shopify_function_macro") (r "^0.8.0") (d #t) (k 0)))) (h "12hq8m1qzxqaix412jk1n7b5w6lapg2a0wmppc9865mj31h160b1")))

