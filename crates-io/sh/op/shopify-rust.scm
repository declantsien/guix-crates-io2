(define-module (crates-io sh op shopify-rust) #:use-module (crates-io))

(define-public crate-shopify-rust-0.1.0 (c (n "shopify-rust") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)) (d (n "textnonce") (r "^1.0.0") (d #t) (k 0)))) (h "1v6vjknwdi6z9ahfsihzvhx7sanp53wlsm703bbqzw2xa516hcfh")))

