(define-module (crates-io sh un shunt) #:use-module (crates-io))

(define-public crate-shunt-0.1.0 (c (n "shunt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("term"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0wmidhc7zcpw4rsp8l05irdqp4n5nz9rmr0rikb6rclbi1hfx2hl")))

(define-public crate-shunt-0.2.0 (c (n "shunt") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("term"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0znl9lrg38ycn8s41l67fj2s665a0r8xb6xqfvvd6brzkmk28g91")))

(define-public crate-shunt-0.3.0 (c (n "shunt") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("term"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0318nak9l3j7lx5dw3dfm3bgn488a0vk96mpr3ha582zrq1xad82")))

(define-public crate-shunt-0.4.0 (c (n "shunt") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("term"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0v0wkqw91crvs16ljrspvxkf3bz5jj6l98wm2ry6cb0z4fqzzylq")))

