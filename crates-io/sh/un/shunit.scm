(define-module (crates-io sh un shunit) #:use-module (crates-io))

(define-public crate-shunit-0.1.1 (c (n "shunit") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "yaserde") (r "^0.8") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.8") (d #t) (k 0)))) (h "05501f7q8fy54qlw0a0yq9c74z9ir4isgwsd9i9iys7kxaa99gpc") (y #t)))

(define-public crate-shunit-0.2.0 (c (n "shunit") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "yaserde") (r "^0.8") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.8") (d #t) (k 0)))) (h "0mqilvy99nadg7qcbyynxhw20dgj7wgixnpnin1748pmpsx0y67w")))

