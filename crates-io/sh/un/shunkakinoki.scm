(define-module (crates-io sh un shunkakinoki) #:use-module (crates-io))

(define-public crate-shunkakinoki-0.1.0 (c (n "shunkakinoki") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0604h89qmhp91djhhndqniancwwgf9h2cgb62cp5bpydqi9384p2")))

(define-public crate-shunkakinoki-0.1.1 (c (n "shunkakinoki") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "098ffg2wvl26hql5lhi0wi4ixv0a24l2hidhm12pfd5cwv7ygzh7")))

(define-public crate-shunkakinoki-2.7.0 (c (n "shunkakinoki") (v "2.7.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1m4k9lf9kvc30xn36yy1bv4jxp1mg5zicl5cqspha0qrbymz8m59")))

(define-public crate-shunkakinoki-2.8.0 (c (n "shunkakinoki") (v "2.8.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1ms0glzvylky6vbz72pbxb99plk4a541g1q4ypz500s4dmnkhs5g")))

(define-public crate-shunkakinoki-2.11.0 (c (n "shunkakinoki") (v "2.11.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1j88gj20mqcf2lfkzqiq73zxvaq10llmhrw5i20sj8qyq84x6jr5")))

(define-public crate-shunkakinoki-2.11.1 (c (n "shunkakinoki") (v "2.11.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "09n59kjj32jf7c0bbzgyylpix97c0jwk4ybwb46g7pxw4b0xkz0x")))

(define-public crate-shunkakinoki-2.11.2 (c (n "shunkakinoki") (v "2.11.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1fh609g3m0jp91yw1pbm9wwm8sjq8j8xqxlkrdhc1g4xg185i6z8")))

(define-public crate-shunkakinoki-2.11.3 (c (n "shunkakinoki") (v "2.11.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1a0030cx5h1hb34j6v65k12w263v5v8vwbp37zp0abavcd44g2sd")))

(define-public crate-shunkakinoki-2.11.4 (c (n "shunkakinoki") (v "2.11.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1zwz5xg1mafr424hh4sz12hkm9wy61ps2w1ii9lnjbd84dv0gq5r")))

(define-public crate-shunkakinoki-2.12.0 (c (n "shunkakinoki") (v "2.12.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0499mnpfh4kypwz6f2ffax0ixay80dyp3b73ndlnizp45f1bnkaj")))

(define-public crate-shunkakinoki-3.0.0 (c (n "shunkakinoki") (v "3.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1bwf20vivinnf9xy2nfnc6iw0kva9qgr71cl0dkyiwyf20bydr2w")))

(define-public crate-shunkakinoki-4.0.0 (c (n "shunkakinoki") (v "4.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0p2gn15c4ib8wxbl8m66wi9l9v60pjp320lc8sfgmwjg8qgcnwx7")))

(define-public crate-shunkakinoki-4.0.1 (c (n "shunkakinoki") (v "4.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0phrxymyma06sa8v8rr2v3x78zfzgvjykh8nds88fhc5i12x0655")))

(define-public crate-shunkakinoki-4.0.2 (c (n "shunkakinoki") (v "4.0.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0pq9dydmwwbr7mjc50g438ymcl2sgxa3whg8x02g8wrqgi9zbjgw")))

(define-public crate-shunkakinoki-4.0.3 (c (n "shunkakinoki") (v "4.0.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1n81lryxjb7b48qwvvj1g1frpfs778y2pal8rqcskw9n4qi8iqbn")))

(define-public crate-shunkakinoki-4.0.4 (c (n "shunkakinoki") (v "4.0.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "18dar68nqwk6z9iwcxdq1zdg90gfj3d4am0sxrjjlyd75gfrjndx")))

(define-public crate-shunkakinoki-4.0.5 (c (n "shunkakinoki") (v "4.0.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "121il6fiqk6fnqay0n2wp2yb5kksr4bay0ggyi66c4fjqry5w6jh")))

(define-public crate-shunkakinoki-4.0.6 (c (n "shunkakinoki") (v "4.0.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1r6gchp2nasa7r7mjg45qprc8v6xybffwqymxif257j22j61v6c1")))

(define-public crate-shunkakinoki-4.1.0 (c (n "shunkakinoki") (v "4.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "11595lm03rib3k9dny1d3sp6ij8siscfvllsshkpnhz0widgjdbw")))

(define-public crate-shunkakinoki-4.2.0 (c (n "shunkakinoki") (v "4.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "11cdfnsik787qmq9571rxh2qwbsns753ks8q9f6a7944vxnqw08g")))

(define-public crate-shunkakinoki-4.2.1 (c (n "shunkakinoki") (v "4.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0mzljzw01269xxk1yr2jj4ra9zk3f8h8gnx037y7ig4pgc8cbwly")))

(define-public crate-shunkakinoki-4.2.2 (c (n "shunkakinoki") (v "4.2.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0s0diqa3n04sdzygiv3ijv1s1ab9a1hna18cvzfhiy5m8zdcjynv")))

(define-public crate-shunkakinoki-4.3.0 (c (n "shunkakinoki") (v "4.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0im9a1ga2z2gk09msjbabx9cb8k63i5z29gzhrhw4bf7ncd2qxzi")))

(define-public crate-shunkakinoki-4.4.0 (c (n "shunkakinoki") (v "4.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0x8jqj8wqc7p1lr464jvjavy1g2nsh2clc9h4d5m1y24s0n56l4y")))

(define-public crate-shunkakinoki-4.5.0 (c (n "shunkakinoki") (v "4.5.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "182aj02x9r71ys2xhgkdjyhi0yhmynzan306305nw949b3mv0i69")))

(define-public crate-shunkakinoki-4.5.1 (c (n "shunkakinoki") (v "4.5.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "17i63qiarxxk57wkl6bplsrf6c645kl8a4xqjaz09265w64rp5ss")))

(define-public crate-shunkakinoki-4.5.2 (c (n "shunkakinoki") (v "4.5.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1p3ldnq5jf5abqgry1lybhqsvv3g0zwg66s915k4pp770fy1lcwf")))

(define-public crate-shunkakinoki-4.6.0 (c (n "shunkakinoki") (v "4.6.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.13") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "14cwyzvpxqqi9hzvjrszlnw1phxlpzi9wgdrypayd1lp16ljclmr")))

(define-public crate-shunkakinoki-4.6.1 (c (n "shunkakinoki") (v "4.6.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0h9va66fkwd60qs8nyx6yk6hi8zmqjv2kdrjg8bxzzm66dms704s")))

(define-public crate-shunkakinoki-4.6.2 (c (n "shunkakinoki") (v "4.6.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1va4qz0fb9d2h7vn7bnpdsl9wnm8ryhbq83ab1sc0kljnb0zc62d")))

(define-public crate-shunkakinoki-4.6.3 (c (n "shunkakinoki") (v "4.6.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0853d0493d3hgidkf9ibd4gp8y3vq0mg6080c59yfh7cn4m3m32c")))

(define-public crate-shunkakinoki-4.7.0 (c (n "shunkakinoki") (v "4.7.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1ggzk8w1vl4j57g8vgghq36swwgan8518wnybpdkw83gyzw3fq0x")))

(define-public crate-shunkakinoki-4.7.1 (c (n "shunkakinoki") (v "4.7.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "14xvzb94kdprrlngpja0i8y95mnkw1j2f7hs8zg3fwhfc4pcac55")))

(define-public crate-shunkakinoki-4.7.2 (c (n "shunkakinoki") (v "4.7.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "08mlpbc54d3mns8wfw0aaphzlgdzk0z88ys45br9smlgr4anf2c8")))

(define-public crate-shunkakinoki-4.7.3 (c (n "shunkakinoki") (v "4.7.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "185d7l1f79pxqfbx0sydfn3ijlnwqd3w6h4dq6xin6nqqgbjzmyq")))

(define-public crate-shunkakinoki-4.8.0 (c (n "shunkakinoki") (v "4.8.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0vn80j64ldc8dfaqprjiid6w2fnq09zffp9vym49lv1ma7a0gb6q")))

(define-public crate-shunkakinoki-4.9.0 (c (n "shunkakinoki") (v "4.9.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0kps66jyks76icfkn2vzv81if3zn1qfdk4l1fcdhn0ns11j7ls2k")))

(define-public crate-shunkakinoki-4.10.0 (c (n "shunkakinoki") (v "4.10.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1f36fhp5rx4q67gvzfgbbk2874yi59whjng6kdwrp58pdqk2xy99")))

(define-public crate-shunkakinoki-4.11.0 (c (n "shunkakinoki") (v "4.11.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1lvmv5gmgn0g40z7wdb1krh5k089l3zwjrgznn63n3bzyslwv7x7")))

(define-public crate-shunkakinoki-4.12.0 (c (n "shunkakinoki") (v "4.12.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "00sca4wzhn4s8k2lcpds98jwnab91z0c8png1ni0fx80cxjrgd1c")))

(define-public crate-shunkakinoki-4.12.1 (c (n "shunkakinoki") (v "4.12.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "17f93qvxvd975hq0q50j3nq203ilnqqcwlm84i0xq7bh3dnwlrhn")))

(define-public crate-shunkakinoki-4.12.2 (c (n "shunkakinoki") (v "4.12.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1swy7nnnhr8w4k8ngpdrsp87fk12406p4yv323x0734kliqgq24f")))

(define-public crate-shunkakinoki-4.13.0 (c (n "shunkakinoki") (v "4.13.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1zjvfav7z062chw6ad6iv7ylhsmxpymx3iv0g8v1g6l24h6svsry")))

(define-public crate-shunkakinoki-4.14.0 (c (n "shunkakinoki") (v "4.14.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "11xhvrsvww06lxs5g3igh5cc6fy5ak7ncz2sr5qi2y2jap3q28ky")))

(define-public crate-shunkakinoki-4.14.1 (c (n "shunkakinoki") (v "4.14.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1ib9gqv5xakh1rjxhh2fh2vvj8w67ws74byzx7nx2lbrrc1kjy8n")))

(define-public crate-shunkakinoki-4.15.0 (c (n "shunkakinoki") (v "4.15.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "17scpww1ic13jq2k9rjnndbbik8k3j83nkiapx47lyw7hc5za6yb")))

(define-public crate-shunkakinoki-4.16.0 (c (n "shunkakinoki") (v "4.16.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1r3svgals5kkagqpp1y87s6hq910d3qwww9p90fq416z3p5i9van")))

(define-public crate-shunkakinoki-4.17.0 (c (n "shunkakinoki") (v "4.17.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0qfi859sbvxnfkkkx77cp9qclnrlrf0q7ra7lp4a3bs0r8ka049l")))

(define-public crate-shunkakinoki-4.17.1 (c (n "shunkakinoki") (v "4.17.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1174hq08v42q3k9ml1ygnl6q2hqlgf4ai6fd4s3hkp8vppm4p4sn")))

(define-public crate-shunkakinoki-4.17.2 (c (n "shunkakinoki") (v "4.17.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "17q5zzy1vcai4bpci6giv9mfmdq0q3h447ba4qifjknaj9gbl4rx")))

(define-public crate-shunkakinoki-4.17.3 (c (n "shunkakinoki") (v "4.17.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1a4v3r1mhw5dfd4ggrsrc41p0zqmxmarmkz8dk96vx1qxn0cj5s5")))

(define-public crate-shunkakinoki-4.17.4 (c (n "shunkakinoki") (v "4.17.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0d23nfk210f3jh452i0cw4nzrxbsyclbvfnn3gddkvys5kx85009")))

(define-public crate-shunkakinoki-4.17.5 (c (n "shunkakinoki") (v "4.17.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0awj1f9agrx4i5mhar1g4fdfq1ac7rvc3cprgjznn999hb1m380l")))

(define-public crate-shunkakinoki-4.17.6 (c (n "shunkakinoki") (v "4.17.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1grn84fyjbcsaw058cwwhm6mm73fizx86yyrqakkyqhipzlx54g4")))

(define-public crate-shunkakinoki-4.18.0 (c (n "shunkakinoki") (v "4.18.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0dp2yr4xdc9mz6ci4hhlrpacxbcd1lh1aybsq061jrqg06m52ca2")))

(define-public crate-shunkakinoki-4.19.0 (c (n "shunkakinoki") (v "4.19.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "12372mgdgv4qzfw7cldjnz8r7506b6cs5w217fkzi23bryd41ygm")))

(define-public crate-shunkakinoki-4.19.1 (c (n "shunkakinoki") (v "4.19.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1jbqfdm7az7g01f8ccfr3wnn7iq16pqqadpb2gsaqf5fd1l8rd49")))

(define-public crate-shunkakinoki-4.19.2 (c (n "shunkakinoki") (v "4.19.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0rxb3l90qlwr26p8fww7x981nj51vg5gba9y3jqgpw4v95f08mmb")))

(define-public crate-shunkakinoki-4.19.3 (c (n "shunkakinoki") (v "4.19.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0ch3vkny3k9jzy8f386qkfli1kbb8lph1v9sxwr8pbwg299phik6")))

(define-public crate-shunkakinoki-4.19.4 (c (n "shunkakinoki") (v "4.19.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0ca1djlmzj6v0rsr1gnjdblncd84xlcpsif2qra2i7vi77isv99l")))

(define-public crate-shunkakinoki-4.19.5 (c (n "shunkakinoki") (v "4.19.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1vx13vhaa8rziyhwic8235wmw6s76bcc1fnaad4j628lhfnhpgxm")))

(define-public crate-shunkakinoki-4.19.6 (c (n "shunkakinoki") (v "4.19.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0c0qa8v685k9pwg0jl3m4ng16jj24yl6gb30297wxb6hkhgyv79f")))

(define-public crate-shunkakinoki-4.19.7 (c (n "shunkakinoki") (v "4.19.7") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "06cax6cy2mbcb6hf6q9dw6vzjzlkl6q7ajzr7r1hfn47lgynk1jb")))

(define-public crate-shunkakinoki-4.19.8 (c (n "shunkakinoki") (v "4.19.8") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0av1548n07glppi1ln7nrzpm9bnqir1d0hrg3ds13dzlxqamgnyn")))

(define-public crate-shunkakinoki-4.19.9 (c (n "shunkakinoki") (v "4.19.9") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0dgrvs4sgnq94qw11g6v10ddzza2r5p035hkxqq1akjba27gphjl")))

(define-public crate-shunkakinoki-4.19.10 (c (n "shunkakinoki") (v "4.19.10") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "174v09vyznphichvc60rw7y1z8d5mpxi1fv0k3wpz3xqym7rg2rf")))

(define-public crate-shunkakinoki-4.19.11 (c (n "shunkakinoki") (v "4.19.11") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1p4mnw0f44w5sdrq4brj6vpqxm79ls4kd9djb53mcj8xry6l79py")))

(define-public crate-shunkakinoki-4.20.0 (c (n "shunkakinoki") (v "4.20.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "05sqqg0wrf7zpfn3gni088rdnnq62gniv1j8fj2v6rk2yvzvlnis")))

(define-public crate-shunkakinoki-4.20.1 (c (n "shunkakinoki") (v "4.20.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0ihf6adgqg4z6cm43gsqf37lgg2pm2j6f483q4sb02h2k67fiiga")))

(define-public crate-shunkakinoki-4.20.2 (c (n "shunkakinoki") (v "4.20.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1mwcz8f25gzhrgb2azs0k3v52mzfdb9xr4g5gfvl6p8ayibvmmiq")))

(define-public crate-shunkakinoki-4.20.3 (c (n "shunkakinoki") (v "4.20.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "16r55mvqbv1b321r6ygxwpiyr1lzfzjp7xmwyy8vsdainz40arvc")))

(define-public crate-shunkakinoki-4.20.4 (c (n "shunkakinoki") (v "4.20.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0jyzz44k61041hmf415352pmfy2dfn5wi700n11g0k9vvanc5hac")))

(define-public crate-shunkakinoki-4.20.5 (c (n "shunkakinoki") (v "4.20.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0438xjksma09n7xyn3fs95d2wlhi4fbp6ax0vcmpsakcggxlfp60")))

(define-public crate-shunkakinoki-4.20.6 (c (n "shunkakinoki") (v "4.20.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0xj4pqfxc3kdpj93jk0zkprnrp79hk750900brs061fnv89jn8gj")))

(define-public crate-shunkakinoki-4.20.7 (c (n "shunkakinoki") (v "4.20.7") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1pbdq74dcmqchdh1w85hv2zpra7jjdh4xg52ms26xx7wsyj05zlx")))

(define-public crate-shunkakinoki-4.20.8 (c (n "shunkakinoki") (v "4.20.8") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "07faf11nvyihvmzvccb1dhg7si1a5c6n9lphqwkn19z4pvl6n025")))

(define-public crate-shunkakinoki-4.21.0 (c (n "shunkakinoki") (v "4.21.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0bzd3xrcbwns9abdq2d6v5sjx1bn50nix7sqijclw05002jfhcis")))

(define-public crate-shunkakinoki-4.21.1 (c (n "shunkakinoki") (v "4.21.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1m2xlkm4pz6ikyhh7dyhmchg1b4ah37xmjrhaih1ai3ip7r03fhy")))

(define-public crate-shunkakinoki-4.21.2 (c (n "shunkakinoki") (v "4.21.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "06r3vv7l3n3vzm1lr1rs1sqph9s6rwvjhmwnvyhrc93byg4mp91g")))

(define-public crate-shunkakinoki-4.22.0 (c (n "shunkakinoki") (v "4.22.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "193dw357l11sac8449wahn6pc61l4bl6jrzagjyy16xzqh56jpyc")))

(define-public crate-shunkakinoki-4.22.1 (c (n "shunkakinoki") (v "4.22.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1f3xwd6jxx0r34kscsiy6shxcxcln282l3r5zaxlakybwsy4adqj")))

(define-public crate-shunkakinoki-4.22.2 (c (n "shunkakinoki") (v "4.22.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1gds7gjq3f9g28rx72cmjci0gy44dqjpy7h1cyf6slx7a8q0k5s0")))

(define-public crate-shunkakinoki-4.22.3 (c (n "shunkakinoki") (v "4.22.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "09cjd9n1pcciq7rw5d8sgjwm2xw16ih180d5hs7yysdpqfxqhzm7")))

(define-public crate-shunkakinoki-4.22.4 (c (n "shunkakinoki") (v "4.22.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1rdrk3dzhlxddz9q4a8avfb9m1k5ij490a125p8dvp68argbidbb")))

(define-public crate-shunkakinoki-4.22.5 (c (n "shunkakinoki") (v "4.22.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0aikf6jn4hzb7bnswpq63fc9nf2d3ql25r33z86jbpgd23v1sf76")))

(define-public crate-shunkakinoki-4.22.6 (c (n "shunkakinoki") (v "4.22.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "10pm424kaiigp690g4p73sa164c3plfr1brsy3ln6gr7xzfgnl58")))

(define-public crate-shunkakinoki-4.22.7 (c (n "shunkakinoki") (v "4.22.7") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0vh0ndqwq28l4ngmrfkf3c70hz765gyr5ky31hn3xpp8b41nahlb")))

(define-public crate-shunkakinoki-4.22.8 (c (n "shunkakinoki") (v "4.22.8") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0mlnjpcz7vq1a9pwynvcx6ncf8axr9bcc69gskj0s0m822mwzcdy")))

(define-public crate-shunkakinoki-4.22.9 (c (n "shunkakinoki") (v "4.22.9") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0ln5px6515dl527s8cfm7923g3drbhca57abw2rxp746a23w1hsr")))

(define-public crate-shunkakinoki-4.22.10 (c (n "shunkakinoki") (v "4.22.10") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1017c9vj8pby89af88ymmbnjvqxmx1ma4hzhx1qqrbmlc0y0gilj")))

(define-public crate-shunkakinoki-4.22.11 (c (n "shunkakinoki") (v "4.22.11") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1800q4m4zbw71a055c66m8gi60fcx99zdz6ij9sx99w7spyigmc4")))

(define-public crate-shunkakinoki-4.22.12 (c (n "shunkakinoki") (v "4.22.12") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1wzmjpvacz5cgrhy3dd9vq31b8lagx4cphlc0jy79a7skhj7sxdy")))

(define-public crate-shunkakinoki-4.22.13 (c (n "shunkakinoki") (v "4.22.13") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1fvbmrgx0hpd70792yw4rfk56g0z84cyw4fb3m1fkhw6ksqpf7px")))

(define-public crate-shunkakinoki-4.22.14 (c (n "shunkakinoki") (v "4.22.14") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0csr0dd3mmj34q22f0xd24fkgpvl50wy2r9nmzs2wv7ywpaycqcf")))

(define-public crate-shunkakinoki-4.22.15 (c (n "shunkakinoki") (v "4.22.15") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "04c027ykkp6wbihn95a2vnq7rapdacz9zppxiipsfaj043s4b3bg")))

(define-public crate-shunkakinoki-4.22.16 (c (n "shunkakinoki") (v "4.22.16") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0x8sindpwf97lz67k5jk1zmcq9jwn8ayinya7lfi751scl0vy8km")))

(define-public crate-shunkakinoki-4.22.17 (c (n "shunkakinoki") (v "4.22.17") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1y2rrx4514p41d36cgxhnr0gsackzdqa2vbhh3p50f05acq0cssw")))

(define-public crate-shunkakinoki-4.22.18 (c (n "shunkakinoki") (v "4.22.18") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0cqhi9qxjyw36g7zdjn1s05fqkymih5k2zhvfk87p1cpxyx1263c")))

(define-public crate-shunkakinoki-4.22.19 (c (n "shunkakinoki") (v "4.22.19") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "168k4p93nyz1j1hncy9pyv3clyhin8mfl1kqp5hninkvk8njw0rl")))

(define-public crate-shunkakinoki-4.22.20 (c (n "shunkakinoki") (v "4.22.20") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1q95j39yh5d85anhqbb5a66sqffjm468khg83j0sk24via12y8ic")))

(define-public crate-shunkakinoki-4.22.21 (c (n "shunkakinoki") (v "4.22.21") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0y7yq2ka9dhsz66hhq1qfvqg75iwyfcg3mcdfq0p2802d0h867pi")))

(define-public crate-shunkakinoki-4.22.22 (c (n "shunkakinoki") (v "4.22.22") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0lv9aghr03m8mbwlgbd22sgb48ncivscixxfz80ms3awkz113dd3")))

(define-public crate-shunkakinoki-4.22.23 (c (n "shunkakinoki") (v "4.22.23") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1jn57qidv9hl9ayrzzrqbsr9radhs1m3ad6h7rhr80a017q21qqp")))

(define-public crate-shunkakinoki-4.22.24 (c (n "shunkakinoki") (v "4.22.24") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0yqg5y6hi9l348hr27bw4fspjqmrqy3hka84iqafba2aal67bkbv")))

(define-public crate-shunkakinoki-4.23.0 (c (n "shunkakinoki") (v "4.23.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1766j2rm5qk1f1pk6r0wra5gcpyyh11kzkmrfwr0c4h2pyvmqlw1")))

(define-public crate-shunkakinoki-4.23.1 (c (n "shunkakinoki") (v "4.23.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "09ny7s10399qf0wkrb4x2axy2z8wj2nd02xsa8nfahp4d668y28h")))

(define-public crate-shunkakinoki-4.23.2 (c (n "shunkakinoki") (v "4.23.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0qsi77lcr0q52fszc89kcaiqvf9wc16xnjb3m53bjygv1kfjycfc")))

(define-public crate-shunkakinoki-4.23.3 (c (n "shunkakinoki") (v "4.23.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0k6fyzgp5r0mzrw2ydlndwjk3mvagfa21x20jwn0jvxcfxpg08r9")))

(define-public crate-shunkakinoki-4.23.4 (c (n "shunkakinoki") (v "4.23.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0lr63y0cjp9b4415a8h0bcrda4gzb6xvdrxfi8x6235zr2kkb2yg")))

(define-public crate-shunkakinoki-4.23.5 (c (n "shunkakinoki") (v "4.23.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1j80lgnyzc7klbr8ijzc4dvs4cg4hnp72g17ggq2hsys4agbl0d2")))

(define-public crate-shunkakinoki-4.23.6 (c (n "shunkakinoki") (v "4.23.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "18w7izipcggrib3v0awy33ihgbsnmz5rjgidkk34qsslx4nwkgwk")))

(define-public crate-shunkakinoki-4.23.7 (c (n "shunkakinoki") (v "4.23.7") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0120kxqmvg3ky4vzps4awpsnm49qmki5nnpnzkr91nbib2ywzz0c")))

(define-public crate-shunkakinoki-4.23.8 (c (n "shunkakinoki") (v "4.23.8") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1cqamic2schjpj1n1c4srzjlx2ilfim6lgarzfpnqqlzs98djs30")))

(define-public crate-shunkakinoki-4.23.9 (c (n "shunkakinoki") (v "4.23.9") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1l8fisyh37qq1c81sm3g5jrv0z3hfkazfnm42i241wmxk8igc3i8")))

(define-public crate-shunkakinoki-4.23.10 (c (n "shunkakinoki") (v "4.23.10") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0nf599fk9vbw4hzrfyra88vq0nvrm4afql6l6xx47354aqkh75yx")))

(define-public crate-shunkakinoki-4.23.11 (c (n "shunkakinoki") (v "4.23.11") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1qyjnxz878yqjg3qmiin61dvkk0cf7izd3mg5acm7b8dcv75hpmf")))

(define-public crate-shunkakinoki-4.23.12 (c (n "shunkakinoki") (v "4.23.12") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1xwncfzsa6lma1r40hmbzzykv6als0h7w0w6gcfcj18a9f0z5drd")))

(define-public crate-shunkakinoki-4.23.13 (c (n "shunkakinoki") (v "4.23.13") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1a9b1g2cmxqx4mbv5g5bpz6xry779gdmbvnw7sxqg573r7n6p42x")))

(define-public crate-shunkakinoki-4.23.14 (c (n "shunkakinoki") (v "4.23.14") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0ji46aj5ccmc1gf2308i1qq8f6as18qy14xbzgamxkz3czvylkjw")))

(define-public crate-shunkakinoki-4.23.15 (c (n "shunkakinoki") (v "4.23.15") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0c2ax25j70hwkknj7kzh9kmgnl3gbwvnw6pllbr4wqpcaiixbc7m")))

(define-public crate-shunkakinoki-4.23.16 (c (n "shunkakinoki") (v "4.23.16") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1wv0m55wa56kkhqc2khi4410p4ajs4p0mkizlp858k8fmafy7pmz")))

(define-public crate-shunkakinoki-4.23.17 (c (n "shunkakinoki") (v "4.23.17") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0pckfyk4q9a8nw07yj06djzf672kb0kvj24f79fk4crzw5n56ra6")))

(define-public crate-shunkakinoki-4.23.18 (c (n "shunkakinoki") (v "4.23.18") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0jpykikzpj0d8i4f997p47n8sqc66ffynbw1k8cab69l0yqnaddx")))

(define-public crate-shunkakinoki-4.23.19 (c (n "shunkakinoki") (v "4.23.19") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0xq1mjlkfngc3zqy113z7s58gc699w5zmv4kd7pv2gf5kcscih3a")))

(define-public crate-shunkakinoki-4.23.20 (c (n "shunkakinoki") (v "4.23.20") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0kqigda7mrjyl487n2rn1x683gnakn74r9g69pnna0wwqvnjsbjw")))

(define-public crate-shunkakinoki-4.23.21 (c (n "shunkakinoki") (v "4.23.21") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "16vz00xgk586yl9vb9r18wv6750qgq4z8wi6j99183p5h3yf4bjb")))

(define-public crate-shunkakinoki-4.23.22 (c (n "shunkakinoki") (v "4.23.22") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "047rhqz21fm6nc8phfnpvkw48ksk9cl7rbvjk9p7r4cz88rw0qg1")))

(define-public crate-shunkakinoki-4.23.23 (c (n "shunkakinoki") (v "4.23.23") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0vwx6ch96wh0nakr9wrdcwxwd1f7pp9n7rlax3nm9ai95qi2129q")))

(define-public crate-shunkakinoki-4.23.24 (c (n "shunkakinoki") (v "4.23.24") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0iiyr52a2hqmrgmp39s9x08z5wd07hs1416989zfxyq6k5qfxlh5")))

(define-public crate-shunkakinoki-4.23.25 (c (n "shunkakinoki") (v "4.23.25") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0afs852wzpahgrd52l777w54nk26d9daszdfzna22ysyij5mk448")))

(define-public crate-shunkakinoki-4.23.26 (c (n "shunkakinoki") (v "4.23.26") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0j10hlv5kiz3km0ns5irhgbsnsibimlngvqaxk362phi974abhl6")))

(define-public crate-shunkakinoki-4.23.27 (c (n "shunkakinoki") (v "4.23.27") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1flpihv5maxkri7ybsl24bnp927k247q3fyg8whvkr6pk6iqz01v")))

(define-public crate-shunkakinoki-4.23.28 (c (n "shunkakinoki") (v "4.23.28") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "13jl42jww1lp8ah912qnc7k4m8lp98pnk1bjqa398h048n1lxiyw")))

(define-public crate-shunkakinoki-4.23.29 (c (n "shunkakinoki") (v "4.23.29") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0p6fz4v60bp1az3z2104s96s7wcg3qahh5lggxf3lzlr1ljk7hcl")))

(define-public crate-shunkakinoki-4.23.30 (c (n "shunkakinoki") (v "4.23.30") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1iw9r6lg54fkg8jdy80v1zw2gl1ygz09pvwljnqr9jbkbz9m72wf")))

(define-public crate-shunkakinoki-4.23.31 (c (n "shunkakinoki") (v "4.23.31") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0n65smxfd8mrkwhhlq51si003cki3lbw9zx03ff5dp8gf9bzlmw5")))

(define-public crate-shunkakinoki-4.23.32 (c (n "shunkakinoki") (v "4.23.32") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0j88sp8813paq5krq67i47zvc3gnqpjq49wvqar9kbxf738na4rs")))

(define-public crate-shunkakinoki-4.23.33 (c (n "shunkakinoki") (v "4.23.33") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0zvj97jnmxyfx6wnpbn19knbaz4gb49p8mgfsizj26jjlc6plrxc")))

(define-public crate-shunkakinoki-4.23.34 (c (n "shunkakinoki") (v "4.23.34") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "031pzwzfizc9bdgis3cgcqcl5qy616v8cv9p8a65h84dfyahrhxs")))

(define-public crate-shunkakinoki-4.23.35 (c (n "shunkakinoki") (v "4.23.35") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1bqh66vyvi28xxs7zb8qxr3d723r1zzfn7n13c08cw74kc3480fy")))

(define-public crate-shunkakinoki-4.23.36 (c (n "shunkakinoki") (v "4.23.36") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "05xyhrr1q1z30c9vgp8hwwzq7mkfljf2l7n97iff2pc0kl55050f")))

(define-public crate-shunkakinoki-4.23.37 (c (n "shunkakinoki") (v "4.23.37") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0z0v2jlsn0j45zwxl6hjdyrmxb1b75r8lcm6ywrpc6gjwaq8qjcr")))

(define-public crate-shunkakinoki-4.23.38 (c (n "shunkakinoki") (v "4.23.38") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1dmkj9raxg86mj2s0fvv62d5gc09h3v6bp626dkxil0dr3cg0vc2")))

(define-public crate-shunkakinoki-4.23.39 (c (n "shunkakinoki") (v "4.23.39") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "144wr0cj41rq84yysh3a473n6j3m53rpkgqhca4h5rvlldxmdbdp")))

(define-public crate-shunkakinoki-4.23.40 (c (n "shunkakinoki") (v "4.23.40") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0qc3qqmbxmqmgywcqfyr6wci9ncs29irabdsf20y1xcixyn8y281")))

(define-public crate-shunkakinoki-4.23.41 (c (n "shunkakinoki") (v "4.23.41") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0fanamgwclpdghppdcmkphixvka9snmpfvsv9irzc3wnpmfqp3w9")))

(define-public crate-shunkakinoki-4.23.42 (c (n "shunkakinoki") (v "4.23.42") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "19hipw136m39lsq4bi1ply4z2g9i6p5xc5f7wbl56f5kq3ak5hdj")))

(define-public crate-shunkakinoki-4.23.43 (c (n "shunkakinoki") (v "4.23.43") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1bq8lrla783vn8zxam3a8s1ic0svi6djv1qh7nsmm8dmm23ny6dg")))

(define-public crate-shunkakinoki-4.23.44 (c (n "shunkakinoki") (v "4.23.44") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "103fg7m1yv18sdn4r59wi10akqlnng0haz51jq3jf184ba6xbfbg")))

(define-public crate-shunkakinoki-4.23.45 (c (n "shunkakinoki") (v "4.23.45") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "18kvl7kq281q4h8bqamj7vz70wxjmbyiham0hnpllk6gbb237sma")))

(define-public crate-shunkakinoki-4.23.46 (c (n "shunkakinoki") (v "4.23.46") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0kqsjkd857fgj11bsjfdfnrz9p2s8qrmcrjk8nrydnqpn5x0hhcw")))

(define-public crate-shunkakinoki-4.23.47 (c (n "shunkakinoki") (v "4.23.47") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "16pcljn0p5r9bkh050wyq8isk2h9g7nwdj4d0qys2qg7nd0s58h1")))

(define-public crate-shunkakinoki-4.23.48 (c (n "shunkakinoki") (v "4.23.48") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "17l6rvrrv1zfdd6d182p75whngmhs13pjwl2j1zcdkmq8ch3xsc7")))

(define-public crate-shunkakinoki-4.23.49 (c (n "shunkakinoki") (v "4.23.49") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0951cw6bcv6pydlxb0zqzl3nw2lsig9waz8ch3n1df35irfl1cps")))

(define-public crate-shunkakinoki-4.23.50 (c (n "shunkakinoki") (v "4.23.50") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "085hsyl6850qpl8yf5h0v7c8n30s1bkrp4qp98d8z99y7sx8x56s")))

(define-public crate-shunkakinoki-4.23.51 (c (n "shunkakinoki") (v "4.23.51") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0i6hx7nk95gxigk1iiwyz4hh5hrkf6ifqm9gyrr42lmq047ksz5s")))

(define-public crate-shunkakinoki-4.23.52 (c (n "shunkakinoki") (v "4.23.52") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "09f3asadj5sbhkzsyxfsmy7x8nrcxypc15c20mvxrslvgq8gb454")))

(define-public crate-shunkakinoki-4.23.53 (c (n "shunkakinoki") (v "4.23.53") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1fr8gjgm0hwfc56k0fsmy33vanmvsl6lrrbc8wp6acczzdnippcj")))

(define-public crate-shunkakinoki-4.23.54 (c (n "shunkakinoki") (v "4.23.54") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0hhds1s60pxlwlw7jgmfxnxppm05kl2ly2iv9g7lq7mramhmfzz7")))

(define-public crate-shunkakinoki-4.23.55 (c (n "shunkakinoki") (v "4.23.55") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1lwdj3qcnq2agzjl2qh65wpjwz0zlxvrzikhc93j4pspd22szdkr")))

(define-public crate-shunkakinoki-4.23.56 (c (n "shunkakinoki") (v "4.23.56") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0xj7mk0wkkwkdx2yr30x0d7aw3cz9zrib9b863x2dyrb2pj602kg")))

(define-public crate-shunkakinoki-4.23.57 (c (n "shunkakinoki") (v "4.23.57") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1sb5616iw4h5fpfdiflp48ff2pmqw7i3q8k79rcyaqky6hvb66v4")))

(define-public crate-shunkakinoki-4.23.58 (c (n "shunkakinoki") (v "4.23.58") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0hyvk0b37ybvwmkfxy1bjl984pz2m473mq7fgjpkiw9qfxnngnmb")))

(define-public crate-shunkakinoki-4.23.59 (c (n "shunkakinoki") (v "4.23.59") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0v98kv8vky9w6hxb1zh21rnlsh8sk69srnvfaikj4v5z9a7fjjb5")))

(define-public crate-shunkakinoki-4.23.60 (c (n "shunkakinoki") (v "4.23.60") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0cjpnbsj7an2kxz3yx0wwry8l782yqysbfhq4h31yf56x9fpdghg")))

(define-public crate-shunkakinoki-4.23.61 (c (n "shunkakinoki") (v "4.23.61") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1qa59w034s7nb37bslff5ab6b0jss93i0iqxd7g8xz6fvrxiqgqa")))

(define-public crate-shunkakinoki-4.23.62 (c (n "shunkakinoki") (v "4.23.62") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "16j0aybw9mgi1mvzv07m61xf9ibsl91f77ba901w3cj08mgjc24p")))

(define-public crate-shunkakinoki-4.23.63 (c (n "shunkakinoki") (v "4.23.63") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0cczvh31q3k97hc6afapia5n951sw89s4q0j5v2fj7ad913g32pg")))

(define-public crate-shunkakinoki-4.23.64 (c (n "shunkakinoki") (v "4.23.64") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1ryc928pxc5wfsy6jjrlghf6agwc3jm4xq46r89r24b4jd553b5j")))

(define-public crate-shunkakinoki-4.23.65 (c (n "shunkakinoki") (v "4.23.65") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1g276i9rh1vhj2mrq2phlc5n6ghybjw594yfhjwy3klpnz6wqgmn")))

(define-public crate-shunkakinoki-4.23.66 (c (n "shunkakinoki") (v "4.23.66") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0k2ms8p3iq13mgayb1m2a6injnxmh2a0xd17p799mdwvhyvp8pal")))

(define-public crate-shunkakinoki-4.23.67 (c (n "shunkakinoki") (v "4.23.67") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1kx3dkwlg4cjdy8irsd5fblmqmp367srmc3mjglvrymm339mj3a2")))

(define-public crate-shunkakinoki-4.23.68 (c (n "shunkakinoki") (v "4.23.68") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1shr2vkpi19mmlvk9h5mw629x9l4126ljxxphn12zf26xfh2n4r7")))

(define-public crate-shunkakinoki-4.23.69 (c (n "shunkakinoki") (v "4.23.69") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1kn8amdnqyashhgypwn50k622dyfi252q2i9r7y7w0bz8r5qvjfr")))

(define-public crate-shunkakinoki-4.23.70 (c (n "shunkakinoki") (v "4.23.70") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0fm558i736invh3h8qvf844magjlbzgzxfy4almqpprz3c22mv2d")))

(define-public crate-shunkakinoki-4.23.71 (c (n "shunkakinoki") (v "4.23.71") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1v8c8m5zzl3lcdh3995lhv4bzfq8af9mav4r5jxx68x9lz1fn62h")))

(define-public crate-shunkakinoki-4.23.72 (c (n "shunkakinoki") (v "4.23.72") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1ckvfh74j29grlhcldn8yvnif6w8cjrnnld6dz24bwixs3dg38r6")))

(define-public crate-shunkakinoki-4.23.73 (c (n "shunkakinoki") (v "4.23.73") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "19i7179lx2ylsljfdgxwyms5l0wh8d12nsw0dawh7zr7m673hjcx")))

(define-public crate-shunkakinoki-4.23.74 (c (n "shunkakinoki") (v "4.23.74") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0m87zqm77ki9a25ixmf7ymw2fh7xmmqxhf93yqbagx6f39y2haww")))

(define-public crate-shunkakinoki-4.23.75 (c (n "shunkakinoki") (v "4.23.75") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0ly1j9qacflajcfplgfvk45i8f4s05vlimndh3x8iqfs4sb14sb6")))

(define-public crate-shunkakinoki-4.23.76 (c (n "shunkakinoki") (v "4.23.76") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "187jka3cw1x98byw9csnn1jpwvcq989jsg9iylnc58wq2cbbiwxf")))

(define-public crate-shunkakinoki-4.23.77 (c (n "shunkakinoki") (v "4.23.77") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1vqbxln3whxih1gq8kps1hln68i85v79r8108m118sx0kx0qvqcd")))

(define-public crate-shunkakinoki-4.23.78 (c (n "shunkakinoki") (v "4.23.78") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0lmk48qdhrllhk7yq6i8405pmx1abxsxr488lfkij7qmkc6fk3s7")))

(define-public crate-shunkakinoki-4.23.79 (c (n "shunkakinoki") (v "4.23.79") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1w03wd3268n4dx7nblbkxmfa9lfvws3bnbh7gm7shr8klw0xcdp3")))

(define-public crate-shunkakinoki-4.23.80 (c (n "shunkakinoki") (v "4.23.80") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1nnwrdngqd78yk23wrs76m76720pr87qs7fm4b56ywzbyqrwj1yx")))

(define-public crate-shunkakinoki-4.23.81 (c (n "shunkakinoki") (v "4.23.81") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0fma6n5s5sl5qxja2v2qhrpic2nnb32y4phwx36mfbyp4y3ad8w2")))

(define-public crate-shunkakinoki-4.23.82 (c (n "shunkakinoki") (v "4.23.82") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "12a6y83vdhc42bq36hsp3hdixa7b9k4jyyhiwjwi6pcx62d7w087")))

(define-public crate-shunkakinoki-4.23.83 (c (n "shunkakinoki") (v "4.23.83") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1kckxjfk0xj3hrp5f0v7pi70xrm3r3833s8z6f35ah75dbg4k4i2")))

(define-public crate-shunkakinoki-4.23.84 (c (n "shunkakinoki") (v "4.23.84") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "08ra498klll9r27aac231nfi9iij5i96b2wqz2gg4zvmdnz40vq5")))

(define-public crate-shunkakinoki-4.23.85 (c (n "shunkakinoki") (v "4.23.85") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "02bd9ngkj8w19xp3ghm5inqcb0j2vd97amiq9v5jybrzadccz8zl")))

(define-public crate-shunkakinoki-4.23.86 (c (n "shunkakinoki") (v "4.23.86") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "10nywg0ilpjczm51fxjq0hk8p7bxsrwv52pl2nhhmbvky34awmmz")))

(define-public crate-shunkakinoki-4.23.87 (c (n "shunkakinoki") (v "4.23.87") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0dljfbbkrzbvx3qhm8hfrkqv7y46ikn9pz5a357vjn89zwzxdn86")))

(define-public crate-shunkakinoki-4.23.88 (c (n "shunkakinoki") (v "4.23.88") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0h847nf3hnjhy1cyf027h14207q0w89819qd1rcjz980y5l535hw")))

(define-public crate-shunkakinoki-4.23.89 (c (n "shunkakinoki") (v "4.23.89") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "015xdg5lm3b09hhdsxp1xq45ckq4q6kzg82j62pkkzhf123pszhi")))

(define-public crate-shunkakinoki-4.23.90 (c (n "shunkakinoki") (v "4.23.90") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1sj2v906yzxxddz5873k05vmbapsmwnasvh31cdn4igz18axyjb6")))

(define-public crate-shunkakinoki-4.23.91 (c (n "shunkakinoki") (v "4.23.91") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0rvmsddg68qlxfp7kc9iaf06wnyymnmb0mhxs380w03lp4lvjq49")))

(define-public crate-shunkakinoki-4.23.92 (c (n "shunkakinoki") (v "4.23.92") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "010zlms4drzq2sp81vh5zw1driqmwlvss2z15fpr8csgfjf45gvx")))

(define-public crate-shunkakinoki-4.23.93 (c (n "shunkakinoki") (v "4.23.93") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0x30ylfd4lk8lk2zidpjzxik8pafaqpq6z8hqbikq9x33pscgajd")))

(define-public crate-shunkakinoki-4.23.94 (c (n "shunkakinoki") (v "4.23.94") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1cawcxp8g9722qsyx85ynqzsk42w14zh6sn6nn5q5a2d9rqhbiw7")))

(define-public crate-shunkakinoki-4.23.95 (c (n "shunkakinoki") (v "4.23.95") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "14455wf3q1443b71sg0vhhj420rgn3rd4khpajjs1x9dji3gg3k4")))

(define-public crate-shunkakinoki-4.23.96 (c (n "shunkakinoki") (v "4.23.96") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0lzxchmsfvvi5cakd7wfsrfmqs0j8jbwgggsrm58cr278g7wxkyw")))

(define-public crate-shunkakinoki-4.23.97 (c (n "shunkakinoki") (v "4.23.97") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0c1j32312gc7m5x7q2fd5qzrs805scixwraydh2mfp1jm1v6dnxh")))

(define-public crate-shunkakinoki-4.23.98 (c (n "shunkakinoki") (v "4.23.98") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "19fflcdy1vk60jz79g86kyfml1ljsvvz97pfibk36zqbhzw56gh2")))

(define-public crate-shunkakinoki-4.23.99 (c (n "shunkakinoki") (v "4.23.99") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1app4zwlwlny6wc62igg5jj08734pjzq9va9fqvrhar7h6q5mvx2")))

(define-public crate-shunkakinoki-4.23.100 (c (n "shunkakinoki") (v "4.23.100") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "0sfm5l8nmjib7vi46wmaqqkfv9mm9cm7ak0gffv128vh2agkjws0")))

(define-public crate-shunkakinoki-4.23.101 (c (n "shunkakinoki") (v "4.23.101") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 0)))) (h "1mk3m7f8scbm903fpcdgqd57d5ka78hw8wv4l0rl7kvvm825p72y")))

(define-public crate-shunkakinoki-4.23.102 (c (n "shunkakinoki") (v "4.23.102") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1qiyvf9n54qx0073232f8pp22qqjbf7wy595z7wahg8jyyci1d60")))

(define-public crate-shunkakinoki-4.23.103 (c (n "shunkakinoki") (v "4.23.103") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1mdfwfv0q3hx570vr6rqbhmyq4i9i80d5lbr5nr9qbx8zwqkpsj2")))

(define-public crate-shunkakinoki-4.23.104 (c (n "shunkakinoki") (v "4.23.104") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1zjmk9nq3jwr63s8pagx7nx2b2h9q460iy8wsd7garpvcs4kbbj1")))

(define-public crate-shunkakinoki-4.23.105 (c (n "shunkakinoki") (v "4.23.105") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "0p15601qqy2csmpvqzdswl12c456j77n68y2s9zkp30yrqjv622p")))

(define-public crate-shunkakinoki-4.23.106 (c (n "shunkakinoki") (v "4.23.106") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1n2ph99mh5y474ywwpiq0pcsdch31cccg14510f0amgr0dsgzpg2")))

(define-public crate-shunkakinoki-4.23.107 (c (n "shunkakinoki") (v "4.23.107") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "0ncn9i488k8qgy1g9vi3w9i6lc3qb4xmzcgq4y3a08rlik0fl0al")))

(define-public crate-shunkakinoki-4.23.108 (c (n "shunkakinoki") (v "4.23.108") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "19jixydrf75lzqqxssyp9rvmvwfba3fl4c738s5dzqb73b80shzz")))

(define-public crate-shunkakinoki-4.23.109 (c (n "shunkakinoki") (v "4.23.109") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1h8gzzzwph05z8gvwppl81fd7hqlmbz52qm6s1h7snhhhwd4csjb")))

(define-public crate-shunkakinoki-4.23.110 (c (n "shunkakinoki") (v "4.23.110") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1r5iwx8flgn8c3kcbzj5c2fgcdk79gzp7728fhdp61wmwjx2m65p")))

(define-public crate-shunkakinoki-4.23.111 (c (n "shunkakinoki") (v "4.23.111") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "13jhnqqqlfiyd5wk5ydf60d0pzqbgawcz9b5w95n9hmrf7c39648")))

(define-public crate-shunkakinoki-4.23.112 (c (n "shunkakinoki") (v "4.23.112") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1nvf9ky0zl0g7yny7ryx95hgyd2dxjc96cmsy8dshjqakmg5h5ih")))

(define-public crate-shunkakinoki-4.23.113 (c (n "shunkakinoki") (v "4.23.113") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1l8vcj3ikfpppb8z3fqpdcq11q14fda37lvb9h8zq4zf03ldn38l")))

(define-public crate-shunkakinoki-4.23.114 (c (n "shunkakinoki") (v "4.23.114") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "0560zgaffvbpl64l0yaqqgyrhd095y5jksyh7hqvf4d0zr06x0i0")))

(define-public crate-shunkakinoki-4.23.115 (c (n "shunkakinoki") (v "4.23.115") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "00zabdfj464g74qb1l411fsbyxv9k8p3qr9rwmm6hx4ckfdhs3rg")))

(define-public crate-shunkakinoki-4.23.116 (c (n "shunkakinoki") (v "4.23.116") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1niag1p7qs9mh96lv3xh85v8cv6nlksaldq2rx26scs6m8jq5j88")))

(define-public crate-shunkakinoki-4.23.117 (c (n "shunkakinoki") (v "4.23.117") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "085saxh77n3dc8j6cbiqw3g95cs9cli2lr36f7scfjvmdgigvkdi")))

(define-public crate-shunkakinoki-4.23.118 (c (n "shunkakinoki") (v "4.23.118") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "0lgrfv38cgqqms3ngn7c0zzmh189413wfy3jgghb72ygwby6kz67")))

(define-public crate-shunkakinoki-4.23.119 (c (n "shunkakinoki") (v "4.23.119") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "19p5ycjc95sz2jmx1qq013f7hp1qx0vpphdn56076ygp0sz9dk24")))

(define-public crate-shunkakinoki-4.23.120 (c (n "shunkakinoki") (v "4.23.120") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "0dh74af14x8ymry2vqwnfqqk2b094v9221s3328gwbc13zs8dbq2")))

(define-public crate-shunkakinoki-4.23.121 (c (n "shunkakinoki") (v "4.23.121") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1rsag6xykkrbggxcwl82qhc1sdc78fn5an59par2v9yaaj70vfg6")))

(define-public crate-shunkakinoki-4.23.122 (c (n "shunkakinoki") (v "4.23.122") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "08cx33xs5fn2655lz8fasliwn85dywj25dymnqfdjc4hh24wni1z")))

(define-public crate-shunkakinoki-4.23.123 (c (n "shunkakinoki") (v "4.23.123") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "0qx33hcbg27dxy7g8k6jvws687rmhs6q56pgpdbclfjygrxa0jsc")))

(define-public crate-shunkakinoki-4.23.124 (c (n "shunkakinoki") (v "4.23.124") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "19i8hdbqipl6hb878kb62zh6phw17b27qw4wq24wkmz6j20kiwjd")))

(define-public crate-shunkakinoki-4.23.125 (c (n "shunkakinoki") (v "4.23.125") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1cxbjdv71qs3490smicwd3ka60anrwzvvylmdrsmsds6lxdxbj42")))

(define-public crate-shunkakinoki-4.23.126 (c (n "shunkakinoki") (v "4.23.126") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "062vdh0w67a1kc0wlsi8rwbf28017lv7cxlrmwm978hrymg0wyrw")))

(define-public crate-shunkakinoki-4.23.127 (c (n "shunkakinoki") (v "4.23.127") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "05nxr57vzc2ymfb5qlhvzs8mn7q0mq5iwbhw4zafyf0cw71igaj2")))

(define-public crate-shunkakinoki-4.23.128 (c (n "shunkakinoki") (v "4.23.128") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "0fmlh8yg98brcqjba53w6vxh6jzsym7dzaxjidzskn75ks75nn77")))

(define-public crate-shunkakinoki-4.23.129 (c (n "shunkakinoki") (v "4.23.129") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "0l708127a4i5vz8vl8nazri0h2mx520fp4sagzjimn13281mhajk")))

(define-public crate-shunkakinoki-4.23.130 (c (n "shunkakinoki") (v "4.23.130") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1509kaffqywmxfs5055mdzcf7mjzf5qfk8nla8y22wbz6gc5s40i")))

(define-public crate-shunkakinoki-4.23.131 (c (n "shunkakinoki") (v "4.23.131") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "11mw3s2gbgf8kka42gnwag33zrfhl34ym92kghcgpgi6r6wh7sv4")))

(define-public crate-shunkakinoki-4.23.132 (c (n "shunkakinoki") (v "4.23.132") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "01k0s5knvxajljf9awvdni4yswgb9c6dwgilsrfly7bv6lp8hck2")))

(define-public crate-shunkakinoki-4.23.133 (c (n "shunkakinoki") (v "4.23.133") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1vlayc52mca17p8k9i4vqygnn9rnkczv1rdpwbhgbm364hh56gb3")))

(define-public crate-shunkakinoki-4.23.134 (c (n "shunkakinoki") (v "4.23.134") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "0yf7y6ray6hg4wr0jkvd61jy1y80i4i0x016hx7dgzwprkz5zlls")))

(define-public crate-shunkakinoki-4.28.0 (c (n "shunkakinoki") (v "4.28.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "semantic-release-rust") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "185grhv42kns6nhwkzhpbqs40rzrbn2nhk0sczsidqqpx0chlzqj")))

(define-public crate-shunkakinoki-4.28.1 (c (n "shunkakinoki") (v "4.28.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "semantic-release-rust") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "0sg25h07llmh7dq3kvnb98ipvkd1aq12pmk5aj3cjfr6hypicy2h")))

(define-public crate-shunkakinoki-4.29.0 (c (n "shunkakinoki") (v "4.29.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "semantic-release-rust") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1y4g6i0dnmrmxjafmxzfbj4jr0hdqj7pmy6vg0x580718in148qq")))

(define-public crate-shunkakinoki-4.29.1 (c (n "shunkakinoki") (v "4.29.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "semantic-release-rust") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "0s62g7b93mdrhk2i2f4kqadi8yjf4fcm5smwql2x0v7zwbnl8frd")))

(define-public crate-shunkakinoki-4.29.2 (c (n "shunkakinoki") (v "4.29.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "semantic-release-rust") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "0lchd67drkb2vlld2b5gsxfrr8bf89zl27ppfzkv1sdmgsw619k5")))

(define-public crate-shunkakinoki-4.29.3 (c (n "shunkakinoki") (v "4.29.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "semantic-release-rust") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1crq3rmcx5yifyxsjc5x629d1rgqmfy23w3d3fsqnvw41vmzylk1")))

(define-public crate-shunkakinoki-4.30.0 (c (n "shunkakinoki") (v "4.30.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "semantic-release-rust") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1q89hkdskb0c1gj0rjpqp9zlv7l9lyvjr6j062vni0bpml9jy06c")))

(define-public crate-shunkakinoki-4.30.1 (c (n "shunkakinoki") (v "4.30.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "semantic-release-rust") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1ncal5mjkg1f3wxpks2wj85p9l11vswlnvl8mn73irhy761x0vsr")))

(define-public crate-shunkakinoki-4.30.2 (c (n "shunkakinoki") (v "4.30.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "semantic-release-rust") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "1yrp6cj34q2xm2gfzsnanqdkr5ilc20s1y93ymjrs1ggs9r3j6l8")))

(define-public crate-shunkakinoki-4.30.3 (c (n "shunkakinoki") (v "4.30.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "self_update") (r "^0.15") (d #t) (k 0)) (d (n "semantic-release-rust") (r "^1.0.0-alpha.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "webbrowser") (r "^0.5.4") (d #t) (k 0)))) (h "01myhad6nky3fpps0rcx3mwjjnlfvsqlfymc3lm9fdz3p3q07xw9")))

