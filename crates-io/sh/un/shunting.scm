(define-module (crates-io sh un shunting) #:use-module (crates-io))

(define-public crate-shunting-0.0.3 (c (n "shunting") (v "0.0.3") (d (list (d (n "dylib") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "lexers") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "1szbrpgfz60aj5b2isjszhrpiml5ziin23r4clrvlkh2w6rqkr4h") (f (quote (("dynlink-eval" "dylib") ("default" "dynlink-eval"))))))

(define-public crate-shunting-0.0.4 (c (n "shunting") (v "0.0.4") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "dylib") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "lexers") (r "^0.0.8") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rustyline") (r "^3.0.0") (d #t) (k 0)))) (h "0s1rqnbwqzx2h7rf8nypkmiy0267l6z4y0m3xiqsk58lr7mim1hz") (f (quote (("dynlink-eval" "dylib") ("default" "dynlink-eval"))))))

(define-public crate-shunting-0.0.5 (c (n "shunting") (v "0.0.5") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "dylib") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "lexers") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rustyline") (r "^3.0.0") (d #t) (k 0)))) (h "1j0r716l8y2i8j097h5gd3qiylif3186s7i9gargf3vkz987km07") (f (quote (("dynlink-eval" "dylib") ("default" "dynlink-eval"))))))

(define-public crate-shunting-0.0.6 (c (n "shunting") (v "0.0.6") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "dylib") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "lexers") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "081fwzlpln908ahqq0c9g72f5lfzr2n9zry7dgqplz1hk1zjf7iy") (f (quote (("dynlink-eval" "dylib") ("default" "dynlink-eval"))))))

(define-public crate-shunting-0.0.7 (c (n "shunting") (v "0.0.7") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "dylib") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "lexers") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "18nsz5icwdy0l0kcks18w5mba6fy8p60ws0v9cfk6vhs39nkhdjg") (f (quote (("dynlink-eval" "dylib") ("default" "dynlink-eval"))))))

(define-public crate-shunting-0.0.8 (c (n "shunting") (v "0.0.8") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "dylib") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "lexers") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "0myjj5yh5z89hvm9g52m0787xac2ss5zf2hys3wls6a4r6bhzmfh") (f (quote (("dynlink-eval" "dylib") ("default" "dynlink-eval"))))))

(define-public crate-shunting-0.0.9 (c (n "shunting") (v "0.0.9") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 2)) (d (n "dylib") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "lexers") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "1d2ci78hdxh09lh3dpz0p0xkc69qknspr4g14abiiymfvygxxsda") (f (quote (("dynlink-eval" "dylib") ("default" "dynlink-eval"))))))

(define-public crate-shunting-0.0.10 (c (n "shunting") (v "0.0.10") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 2)) (d (n "dylib") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "lexers") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)))) (h "1b6y7d9h0qjxmqmj46m3f0h7lzf9mf83py0pp9zqlf4r6graz26c") (f (quote (("dynlink-eval" "dylib") ("default" "dynlink-eval"))))))

(define-public crate-shunting-0.0.11 (c (n "shunting") (v "0.0.11") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 2)) (d (n "dylib") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "lexers") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 2)))) (h "1x8abj97z9jv35bxnb8x8dr3fwdph9dhp4afbpf7h54j5rdawq8p") (f (quote (("dynlink-eval" "dylib") ("default" "dynlink-eval"))))))

(define-public crate-shunting-0.0.12 (c (n "shunting") (v "0.0.12") (d (list (d (n "dylib") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 2)) (d (n "lexers") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 2)))) (h "1w79y509la767r0l00j3pc6k9gfhlgix0vfx7dsfzs1mx8jaw7c0") (f (quote (("dynlink-eval" "dylib") ("default" "dynlink-eval"))))))

(define-public crate-shunting-0.1.0 (c (n "shunting") (v "0.1.0") (d (list (d (n "home") (r "^0.5") (d #t) (k 2)) (d (n "lexers") (r "^0.1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 2)))) (h "01bnhyrdgy7wm85r5p6glm2c7awvviymzra2sxnin7swaxx4mlvj")))

(define-public crate-shunting-0.1.1 (c (n "shunting") (v "0.1.1") (d (list (d (n "home") (r "^0.5") (d #t) (k 2)) (d (n "lexers") (r "^0.1") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 2)))) (h "073g36i36kpza8s38qh9n6hvl2yndv8b4cldjqss34mj92kj9sg4")))

(define-public crate-shunting-0.1.2 (c (n "shunting") (v "0.1.2") (d (list (d (n "home") (r "^0.5") (d #t) (k 2)) (d (n "lexers") (r "^0.1") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 2)))) (h "0kyp9idqskprsfk9adfawbw13v7rcrl9b6l4l9abz7ymjl6j3g66")))

