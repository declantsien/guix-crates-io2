(define-module (crates-io sh #{11}# sh1107) #:use-module (crates-io))

(define-public crate-sh1107-0.1.0 (c (n "sh1107") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "either") (r "^1.8.0") (k 0)) (d (n "embedded-hal-async") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (k 0)) (d (n "itertools") (r "^0.10.3") (k 0)))) (h "07g1ijgaphv4h33s2yiaw27dgix4qr3h2ssycgrbz53xpdnnhgzn")))

