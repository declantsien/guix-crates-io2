(define-module (crates-io sh rs shrs_output_capture) #:use-module (crates-io))

(define-public crate-shrs_output_capture-0.0.1 (c (n "shrs_output_capture") (v "0.0.1") (d (list (d (n "shrs") (r "^0.0.1") (d #t) (k 0)))) (h "0jd52k73hi6vs6gqa84hs6r1idlh7cvv8pfvlnzx1wpwnw8q4dbs")))

(define-public crate-shrs_output_capture-0.0.2 (c (n "shrs_output_capture") (v "0.0.2") (d (list (d (n "shrs") (r "^0.0.2") (d #t) (k 0)))) (h "0g3pypgwgbagnd30d3irwk1nn85r0rs7dlcskmlh53gkhr2pvicy")))

(define-public crate-shrs_output_capture-0.0.3 (c (n "shrs_output_capture") (v "0.0.3") (d (list (d (n "shrs") (r "^0.0.3") (d #t) (k 0)))) (h "1dymvzgp5ipxh18pmg0rhg16fhqhrw55s2lcf722453yv1sz045l")))

(define-public crate-shrs_output_capture-0.0.4 (c (n "shrs_output_capture") (v "0.0.4") (d (list (d (n "shrs") (r "^0.0.4") (d #t) (k 0)))) (h "03dp563gp5r6l08qdv95dyg6mzk6i01qg2dswi4p49dayzn5kc3l")))

(define-public crate-shrs_output_capture-0.0.5 (c (n "shrs_output_capture") (v "0.0.5") (d (list (d (n "shrs") (r "^0.0.5") (d #t) (k 0)))) (h "1g8xdnfnba6shm77yrs2vgivdqkjb7ypjwww9hd4nhvjbx88jvdj")))

