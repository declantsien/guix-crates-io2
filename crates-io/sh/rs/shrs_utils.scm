(define-module (crates-io sh rs shrs_utils) #:use-module (crates-io))

(define-public crate-shrs_utils-0.0.1 (c (n "shrs_utils") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "ropey") (r "^1.6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00bwfpzrl2cw2mvg0kgs4z71y26xwb9wh85lk7ss0dpwhpcq5nrh")))

(define-public crate-shrs_utils-0.0.2 (c (n "shrs_utils") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "ropey") (r "^1.6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0lksia2gz18ij7vhpjvp5005al4zm6yb0g73bf37i4q412gw0x7g")))

(define-public crate-shrs_utils-0.0.3 (c (n "shrs_utils") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "ropey") (r "^1.6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "09y1nqyfw3j6xxg0grm6xh1bgd3wsn508iv1wc75v82v35p4n77a")))

(define-public crate-shrs_utils-0.0.4 (c (n "shrs_utils") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "ropey") (r "^1.6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "13ak84r0rwxa5drnz5m9bq90hhyi3gpz4n16lpsd0iwck1y868h9")))

(define-public crate-shrs_utils-0.0.5 (c (n "shrs_utils") (v "0.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "ropey") (r "^1.6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0yn9yzr5gwvgsmbig2csilp75zd6qw1lbsds4mjbs3zxx39jcfph")))

