(define-module (crates-io sh rs shrs_insulter) #:use-module (crates-io))

(define-public crate-shrs_insulter-0.0.1 (c (n "shrs_insulter") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shrs") (r "^0.0.1") (d #t) (k 0)))) (h "1im8pk6b0bqcgy6llcp0hbmr8m9l1xji28kvkm0lr39dsk4yfara")))

