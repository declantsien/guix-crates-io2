(define-module (crates-io sh rs shrs_completion) #:use-module (crates-io))

(define-public crate-shrs_completion-0.0.3 (c (n "shrs_completion") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "shrs") (r "^0.0.3") (d #t) (k 0)) (d (n "ssh2-config") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1nr39km6q3qcfs9andyqxhrbsq0whg1s3qrrm0pvp82jazfvak2h")))

(define-public crate-shrs_completion-0.0.4 (c (n "shrs_completion") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "shrs") (r "^0.0.4") (d #t) (k 0)) (d (n "ssh2-config") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "08644fmcrcp4flxd926a6hd5nkniqjddafqq951067p14fh9zgw6")))

(define-public crate-shrs_completion-0.0.5 (c (n "shrs_completion") (v "0.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "shrs") (r "^0.0.5") (d #t) (k 0)) (d (n "ssh2-config") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0l14yvh46b5v0x1bj1lir8ii8qww7b5k7q8gcpx935q4nh9ydp2y")))

