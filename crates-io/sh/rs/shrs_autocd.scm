(define-module (crates-io sh rs shrs_autocd) #:use-module (crates-io))

(define-public crate-shrs_autocd-0.0.2 (c (n "shrs_autocd") (v "0.0.2") (d (list (d (n "shrs") (r "^0.0.2") (d #t) (k 0)))) (h "0yhqqq29pqgnqk5x6qkabh5sxqwf7sf3b9xbvny4ybazbrwp1h30")))

(define-public crate-shrs_autocd-0.0.3 (c (n "shrs_autocd") (v "0.0.3") (d (list (d (n "shrs") (r "^0.0.3") (d #t) (k 0)))) (h "17h0kf3da9vjxm8vaa4n771slj1mmablrg94a41lsk8ar6dd6c72")))

(define-public crate-shrs_autocd-0.0.4 (c (n "shrs_autocd") (v "0.0.4") (d (list (d (n "shrs") (r "^0.0.4") (d #t) (k 0)))) (h "0gzpc2swrcagfhw7ixm48iy0ypbjyy8pf4qf9p8gpdyvhwg9s4is")))

(define-public crate-shrs_autocd-0.0.5 (c (n "shrs_autocd") (v "0.0.5") (d (list (d (n "shrs") (r "^0.0.5") (d #t) (k 0)))) (h "0sig830x4wbgqbnjsfs3klb5k16hzmsbsww98k6gf309fsq8sdzx")))

