(define-module (crates-io sh rs shrs_file_logger) #:use-module (crates-io))

(define-public crate-shrs_file_logger-0.0.4 (c (n "shrs_file_logger") (v "0.0.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "shrs") (r "^0.0.4") (d #t) (k 0)))) (h "1k1xy0vaf8x84ix5iwzd37diyy4hv9idlxab7pngas8np34idc8f")))

(define-public crate-shrs_file_logger-0.0.5 (c (n "shrs_file_logger") (v "0.0.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "shrs") (r "^0.0.5") (d #t) (k 0)))) (h "1a1cay7lpindvqnlb70jw7a8q1afkc37z5kvqsw8z2q0w8q8rwzh")))

