(define-module (crates-io sh rs shrs_vi) #:use-module (crates-io))

(define-public crate-shrs_vi-0.0.1 (c (n "shrs_vi") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ykqfrqi4q8zzp48l4fzwkffy1c24bi7f9z2ff5aph77m4fcb2i8")))

(define-public crate-shrs_vi-0.0.2 (c (n "shrs_vi") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jif7wkwiac59143gpiwa63fizif1b1dj3l9icih9rfi916vb51z")))

(define-public crate-shrs_vi-0.0.3 (c (n "shrs_vi") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "08ywqprhjgrhjbgz9r2zfdxpahk1xqwzhi65dn9jsk6szrhdgi4l")))

(define-public crate-shrs_vi-0.0.4 (c (n "shrs_vi") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hjs6zmsf2xkdlmxzaz7g3w31cyvgbn4w0xwn46d8n4qck28iidp")))

(define-public crate-shrs_vi-0.0.5 (c (n "shrs_vi") (v "0.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.8") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0878kqirhp4shczxgvxms6wy26nq8fxkwmnw5h6m3prl614fr2l3")))

