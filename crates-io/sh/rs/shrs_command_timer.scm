(define-module (crates-io sh rs shrs_command_timer) #:use-module (crates-io))

(define-public crate-shrs_command_timer-0.0.1 (c (n "shrs_command_timer") (v "0.0.1") (d (list (d (n "shrs") (r "^0.0.1") (d #t) (k 0)))) (h "0sdbgx2j3q71l599yhyr39ar5753rh5mppw253gqm6w7830s7wb3")))

(define-public crate-shrs_command_timer-0.0.2 (c (n "shrs_command_timer") (v "0.0.2") (d (list (d (n "shrs") (r "^0.0.2") (d #t) (k 0)))) (h "04mwn352q646pb1gxhpg5swzjggzx8l1r03k8cz6hf71wjyy0bzj")))

(define-public crate-shrs_command_timer-0.0.3 (c (n "shrs_command_timer") (v "0.0.3") (d (list (d (n "shrs") (r "^0.0.3") (d #t) (k 0)))) (h "1mg1sgm3flbg8wj7iabi9siqzv6z90b6r6bkpcgbb33p1rmnqll8")))

(define-public crate-shrs_command_timer-0.0.4 (c (n "shrs_command_timer") (v "0.0.4") (d (list (d (n "shrs") (r "^0.0.4") (d #t) (k 0)))) (h "1qkd8nch8b38qm3rzvd4f7p04bm8qbcw0v1y7v4vnwxyzkl3qrb6")))

(define-public crate-shrs_command_timer-0.0.5 (c (n "shrs_command_timer") (v "0.0.5") (d (list (d (n "shrs") (r "^0.0.5") (d #t) (k 0)))) (h "12g3rfz3xbzxi5cm4sviyi6wawvcsqjgs0ih0vswcw76x3wah7ay")))

