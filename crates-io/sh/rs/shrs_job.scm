(define-module (crates-io sh rs shrs_job) #:use-module (crates-io))

(define-public crate-shrs_job-0.0.1 (c (n "shrs_job") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("fs" "term" "process" "signal"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1i6mkzpn062vjpy9pm3gy6k8np5ljqgsi6x1j8l9hhjr87qypraa")))

(define-public crate-shrs_job-0.0.2 (c (n "shrs_job") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("fs" "term" "process" "signal"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bsli1yypri2an8k5j161p1glqsbvpn6bcgwscdd06n4ymfgxyjn")))

(define-public crate-shrs_job-0.0.3 (c (n "shrs_job") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("fs" "term" "process" "signal"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1d75x9w1zhn8xbld42l1zlswzd8rx8q9w9hh674wyqw0qpdq0061")))

(define-public crate-shrs_job-0.0.4 (c (n "shrs_job") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("fs" "term" "process" "signal"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lavjp872dvh8qgsizaa3p3k4m3l9h1sh5gq0lgii16cqxjg4zqi")))

(define-public crate-shrs_job-0.0.5 (c (n "shrs_job") (v "0.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("fs" "term" "process" "signal"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14l1nshwvcxhr8n5wl257nrcy7lfrafly1k0xawzdf0h5glx7r6p")))

