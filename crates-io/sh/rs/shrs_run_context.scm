(define-module (crates-io sh rs shrs_run_context) #:use-module (crates-io))

(define-public crate-shrs_run_context-0.0.2 (c (n "shrs_run_context") (v "0.0.2") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "shrs") (r "^0.0.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1bv12zxrbpqb7kk6zng1lxs3w1wb1niqb5qh3zsqk45vwi9yjgsn")))

(define-public crate-shrs_run_context-0.0.3 (c (n "shrs_run_context") (v "0.0.3") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "shrs") (r "^0.0.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "1k7mkxsa5z9ishdvg82sxliix6q9a7743maywn9jsp82fd3hnscn")))

(define-public crate-shrs_run_context-0.0.4 (c (n "shrs_run_context") (v "0.0.4") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "shrs") (r "^0.0.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "03w4dldfplfjinaa1gpjpmj0dm791g6g3wx62cby0xar4fbifgf1")))

(define-public crate-shrs_run_context-0.0.5 (c (n "shrs_run_context") (v "0.0.5") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "shrs") (r "^0.0.5") (f (quote ("serde"))) (d #t) (k 0)))) (h "005nsms52vnn36bsy1a7iadwnr7s87gc3zpxb2rxbdq9ygry25v2")))

