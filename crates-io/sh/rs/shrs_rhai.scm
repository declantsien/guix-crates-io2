(define-module (crates-io sh rs shrs_rhai) #:use-module (crates-io))

(define-public crate-shrs_rhai-0.0.4 (c (n "shrs_rhai") (v "0.0.4") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "rhai") (r "^1.16") (d #t) (k 0)) (d (n "shrs") (r "^0.0.4") (d #t) (k 0)))) (h "0rlyp7g4kb5y7pag3xs2dd2wz9wv22lpf4w8ibsal0a2949kahcy")))

(define-public crate-shrs_rhai-0.0.5 (c (n "shrs_rhai") (v "0.0.5") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "rhai") (r "^1.16") (d #t) (k 0)) (d (n "shrs") (r "^0.0.5") (d #t) (k 0)))) (h "0yvbwi9y9f7w7ygi600pdj8xr3p67km1ra4mjjx04c9bvqm62a0j")))

