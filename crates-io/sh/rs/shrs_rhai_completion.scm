(define-module (crates-io sh rs shrs_rhai_completion) #:use-module (crates-io))

(define-public crate-shrs_rhai_completion-0.0.5 (c (n "shrs_rhai_completion") (v "0.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "rhai") (r "^1.16") (d #t) (k 0)) (d (n "shrs") (r "^0.0.5") (d #t) (k 0)) (d (n "ssh2-config") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rkf2nip9wrpjdxirvvzawrs7ps8gkirk5hrm614wf3kcx2higbb")))

