(define-module (crates-io sh rs shrs_mux) #:use-module (crates-io))

(define-public crate-shrs_mux-0.0.1 (c (n "shrs_mux") (v "0.0.1") (d (list (d (n "shrs") (r "^0.0.1") (d #t) (k 0)))) (h "1han51j9m93hni32bn8bycm6ragf8qmlshj17k0rgp0zv662f2lk")))

(define-public crate-shrs_mux-0.0.3 (c (n "shrs_mux") (v "0.0.3") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shrs") (r "^0.0.3") (d #t) (k 0)))) (h "0kx0x10r4sf7j5ax4m9q91hfyacwnx9mlazvz4i9x9phbcpvlr73")))

(define-public crate-shrs_mux-0.0.4 (c (n "shrs_mux") (v "0.0.4") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "openssh") (r "^0.10.3") (d #t) (k 0)) (d (n "shrs") (r "^0.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 0)))) (h "0zl2ifpqk6wmaizqp7fj5wzxlfgvlqj949abwli7hy1r4xj7nd4v")))

(define-public crate-shrs_mux-0.0.5 (c (n "shrs_mux") (v "0.0.5") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "openssh") (r "^0.10.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rustpython-parser") (r "^0.3.1") (d #t) (k 0)) (d (n "shrs") (r "^0.0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 0)))) (h "1c03y310ynikvz73gx5qaqm1slhnf6x6cv52s3ksyxpiqjyycy93")))

