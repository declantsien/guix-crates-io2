(define-module (crates-io sh aw shawshank) #:use-module (crates-io))

(define-public crate-shawshank-0.1.0 (c (n "shawshank") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "owning_ref") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)))) (h "0j2pijwr7aaxkkn27rjcq0ayi44mxcx6cyab738aakwinfr1sbcz") (f (quote (("unstable"))))))

(define-public crate-shawshank-0.2.0 (c (n "shawshank") (v "0.2.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "owning_ref") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)))) (h "184sk3mx186389pyf8935k4iajk0qk0j1bz7w3cd7pmikab76gq5") (f (quote (("unstable")))) (y #t)))

(define-public crate-shawshank-0.2.1 (c (n "shawshank") (v "0.2.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "owning_ref") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)))) (h "0agzjfsqx0w11w0h6iyknlcw8r8w5irqxmsf8w1g9f6j6b220gwi") (f (quote (("unstable"))))))

(define-public crate-shawshank-0.2.2 (c (n "shawshank") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "skeptic") (r "^0.10") (d #t) (k 1)) (d (n "skeptic") (r "^0.10") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (d #t) (k 0)))) (h "13p6xf8mw4d1gj9yc4cga47lywfril09d0ldz9az8hbb0a5l7w5w") (f (quote (("unstable"))))))

(define-public crate-shawshank-0.2.3 (c (n "shawshank") (v "0.2.3") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (d #t) (k 0)))) (h "1anzmcqgfjcld54f1iqysdga47lfxcm3pnw168gflnqsy7i2qwjm") (f (quote (("unstable"))))))

