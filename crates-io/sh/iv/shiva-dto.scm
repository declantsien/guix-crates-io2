(define-module (crates-io sh iv shiva-dto) #:use-module (crates-io))

(define-public crate-shiva-dto-0.1.0 (c (n "shiva-dto") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0glgmjipzwx9hapg4aymnism0xhnvz99sxjr7mrc584xndni2dci")))

(define-public crate-shiva-dto-0.1.1 (c (n "shiva-dto") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1wbzlnp8kbf8phyfv9q4aqlfzv0nm06p6jgnh53gdzc469lpps12")))

(define-public crate-shiva-dto-0.2.0 (c (n "shiva-dto") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1an03ymr7qrjza3xpaqlcx42rcf479r3qkaf3xfvys6aisikf8bs")))

(define-public crate-shiva-dto-0.3.0 (c (n "shiva-dto") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0y07kdljnp5b9vq3v1l5ygxfwi8xn6923y5l1rcjlgf8hildmnbx")))

(define-public crate-shiva-dto-0.4.0 (c (n "shiva-dto") (v "0.4.0") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0r9zk7a7rcfahkvif19r3xw61xbnkns44qv169hnxfyyarj4g25m")))

(define-public crate-shiva-dto-0.4.1 (c (n "shiva-dto") (v "0.4.1") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0pp9hg2w96ic6mmgpbv3axvqw348ljsbg6z7gwys00mwrknpdxk1")))

(define-public crate-shiva-dto-0.4.2 (c (n "shiva-dto") (v "0.4.2") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0rp5cfmsqinvnmfidg0ypzfb22qqimfbzc76rhylmzqgjql1kins")))

(define-public crate-shiva-dto-0.4.3 (c (n "shiva-dto") (v "0.4.3") (d (list (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)))) (h "0jsqwz62x9v7nhh0kmpvmxmlzvkb5k8nm7i2gnxsxkcihs0438hv")))

(define-public crate-shiva-dto-0.4.4 (c (n "shiva-dto") (v "0.4.4") (d (list (d (n "rustc-serialize") (r "^0.3.23") (d #t) (k 0)))) (h "0mmch8kvr1wgh1y87gy2bwmgc4zvbw7zdi7rh3ny04clixqk96w6")))

(define-public crate-shiva-dto-0.5.0 (c (n "shiva-dto") (v "0.5.0") (d (list (d (n "serde") (r "^0.9.13") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.13") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.10") (d #t) (k 0)))) (h "1pdd862za7dmqd8c0hk52g0b4vi6il1mz0hv09gp8qnlmf1208b6")))

(define-public crate-shiva-dto-0.6.0 (c (n "shiva-dto") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1b2a8pvpb390w1p8w0p87n437q8n3d8ar7z1wsr9f5rfqxpcgzc7")))

(define-public crate-shiva-dto-0.7.0 (c (n "shiva-dto") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hzif51vc3asg87xkbs37q1dad3y1k3rg86fnml0a4gy2swhndgn")))

