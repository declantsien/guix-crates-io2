(define-module (crates-io sh iv shiv-transform) #:use-module (crates-io))

(define-public crate-shiv-transform-0.1.0-alpha (c (n "shiv-transform") (v "0.1.0-alpha") (d (list (d (n "glam") (r "^0.21") (d #t) (k 0)) (d (n "shiv") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "1rqxl96wxcsraz3q1dyamy5130np3cxgdrlcm893xy4n8bc16hx0")))

(define-public crate-shiv-transform-0.1.0-alpha2 (c (n "shiv-transform") (v "0.1.0-alpha2") (d (list (d (n "glam") (r "^0.21") (d #t) (k 0)) (d (n "shiv") (r "^0.1.0-alpha2") (d #t) (k 0)))) (h "05vdzalkyi8bq2jf22jiazmhn12ibzwal63vzhxrvn75s07zbw39") (y #t)))

(define-public crate-shiv-transform-0.1.0-alpha.3 (c (n "shiv-transform") (v "0.1.0-alpha.3") (d (list (d (n "glam") (r "^0.22") (d #t) (k 0)) (d (n "shiv") (r "^0.1.0-alpha.3") (f (quote ("hierarchy"))) (d #t) (k 0)))) (h "18dx5ksgxhgm75bqsbbqrry9xxnrwfdrp5isx74n6ll05hbl98mp") (f (quote (("default"))))))

(define-public crate-shiv-transform-0.1.0-alpha.4 (c (n "shiv-transform") (v "0.1.0-alpha.4") (d (list (d (n "glam") (r "^0.22") (d #t) (k 0)) (d (n "shiv") (r "^0.1.0-alpha.3") (f (quote ("hierarchy"))) (d #t) (k 0)))) (h "1a002v3l6j0l2ca32w8bwynxk8g6b5yhbq0vmyacillv38m2a46r") (f (quote (("default"))))))

