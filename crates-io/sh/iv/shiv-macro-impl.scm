(define-module (crates-io sh iv shiv-macro-impl) #:use-module (crates-io))

(define-public crate-shiv-macro-impl-0.1.0-alpha3 (c (n "shiv-macro-impl") (v "0.1.0-alpha3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ng6zm0vrmadi4mckx5387cykp7n6r64wiw40npsvi49h5dnvksj") (y #t)))

(define-public crate-shiv-macro-impl-0.1.0-alpha.3 (c (n "shiv-macro-impl") (v "0.1.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1524gbxrb8xviia6zjxcwxjaigx8skpzq4fsl21mw5q8s9ll7v9k")))

