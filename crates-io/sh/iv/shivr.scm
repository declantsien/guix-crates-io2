(define-module (crates-io sh iv shivr) #:use-module (crates-io))

(define-public crate-shivr-0.1.0 (c (n "shivr") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0cyzgsxj5mk0i833jjhqzca1n478frs7dwb96fk7qgmdaigblxw5") (y #t)))

(define-public crate-shivr-0.1.1 (c (n "shivr") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1bai396a7laj8f97j56alqksycvw15bycvczlmyy4cv9fvv8n9vl")))

(define-public crate-shivr-0.1.2 (c (n "shivr") (v "0.1.2") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0frbg2vprlv0dr5bhi7h77ilp2iarm9l6bw810gb629g6sk68fr6")))

(define-public crate-shivr-0.1.3 (c (n "shivr") (v "0.1.3") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1f2iw950a3vilmv4x49dhhd8nhs9d53m2qwj1vs51fmakwky4kk5")))

(define-public crate-shivr-0.1.4 (c (n "shivr") (v "0.1.4") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "03swa6sdiacfcpm0yz441w198yzv88124w61idq43qnmkch0lbhy")))

(define-public crate-shivr-0.1.5 (c (n "shivr") (v "0.1.5") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0zhf6bc6r9pfkjw8x3q25nl53kzj630c4br3ah4bdv8g2par17ad")))

