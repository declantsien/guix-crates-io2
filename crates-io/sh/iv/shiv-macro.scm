(define-module (crates-io sh iv shiv-macro) #:use-module (crates-io))

(define-public crate-shiv-macro-0.1.0-alpha (c (n "shiv-macro") (v "0.1.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0iv2xnacm31nlc1ygnd7y8nbbk4p4vxdr8yj52dlrldzwiddnnid")))

(define-public crate-shiv-macro-0.1.0-alpha2 (c (n "shiv-macro") (v "0.1.0-alpha2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x5ywzr0pgarnbkw0brgx4vblllz7gdbh5rq0ijmajpbp5s9im5z") (y #t)))

(define-public crate-shiv-macro-0.1.0-alpha3 (c (n "shiv-macro") (v "0.1.0-alpha3") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "shiv-macro-impl") (r "^0.1.0-alpha3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00x2144x0a55p97llsq191fa81mxwghm19sx30c50p41sma5q8dl") (y #t)))

(define-public crate-shiv-macro-0.1.0-alpha.3 (c (n "shiv-macro") (v "0.1.0-alpha.3") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "shiv-macro-impl") (r "^0.1.0-alpha.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gn58zifcx5driay9fmw5kpy3k2s4cbncxprsp5l643fy9a859pz")))

