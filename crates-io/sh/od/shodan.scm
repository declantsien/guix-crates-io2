(define-module (crates-io sh od shodan) #:use-module (crates-io))

(define-public crate-shodan-0.1.0 (c (n "shodan") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10.0") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.1") (d #t) (k 0)))) (h "17jwvhba6ywy6nzraq783iyj5x4sdrnrkbaa252bfy73r5iwprb4")))

