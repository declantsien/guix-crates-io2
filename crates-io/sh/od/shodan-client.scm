(define-module (crates-io sh od shodan-client) #:use-module (crates-io))

(define-public crate-shodan-client-0.0.1 (c (n "shodan-client") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1l8l85v9s5kzw2ravn3mi9vj0spmdm4hrzvyf2lh2gi5kylcnh7p")))

(define-public crate-shodan-client-0.0.2 (c (n "shodan-client") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "0is30jv3yw433xi2ap0586ikhnfjkk7lvk12kyzj109zdi4x7snz")))

(define-public crate-shodan-client-0.1.0 (c (n "shodan-client") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1c0z8sz3i1cyppmdkqhpcb9g7x72n8hidqvak6ssqdfviam4f6pd")))

(define-public crate-shodan-client-0.1.1 (c (n "shodan-client") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1la91j354d1nw0c6d6zpx4bc1sqwn6g6cwhv1bd9x47phl488y1r")))

