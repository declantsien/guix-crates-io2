(define-module (crates-io sh uu shuuro) #:use-module (crates-io))

(define-public crate-shuuro-0.5.0 (c (n "shuuro") (v "0.5.0") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0b7xyb24ahryjnmap5xbxvgf59g9hyjq1vfsakrrs5h7f9wvciwv")))

(define-public crate-shuuro-0.5.1 (c (n "shuuro") (v "0.5.1") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ya9bhmvfd3i82dmmqq0x304ki7355hfi6g4yk1kc3n0gdpgjg5y")))

(define-public crate-shuuro-0.5.2 (c (n "shuuro") (v "0.5.2") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1g7l8c5yk5s717rspbnzas4pv65bxmjhmxgnpb6g5489k1c669f7")))

(define-public crate-shuuro-0.5.3 (c (n "shuuro") (v "0.5.3") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1403k7gk8hybljkgi1mr1193946yiwc6pb13n4sc65v99vjrbyg7")))

(define-public crate-shuuro-0.5.4 (c (n "shuuro") (v "0.5.4") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pm9v36x5dq6n278ic7kfdad0sn2ckdd4f6ph8r1jx5r3xp1376c")))

(define-public crate-shuuro-0.5.5 (c (n "shuuro") (v "0.5.5") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j9fpay0hpy7crhxdnxnvb8c7c1jfdjx4xh7zrypjdsz6zwxflfj")))

(define-public crate-shuuro-0.5.6 (c (n "shuuro") (v "0.5.6") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01ykpydk3y90y302n8g9i7xcm2k2wqcj6mcvynwxy2an776ya57b")))

(define-public crate-shuuro-0.5.7 (c (n "shuuro") (v "0.5.7") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1iphny7aj823ihdxdxdsk24l80zc7mpav00l91bg8n1xqhxiv7pg")))

(define-public crate-shuuro-0.5.8 (c (n "shuuro") (v "0.5.8") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s342vh91xs58aq4p9injyf5cbbs3qa1wm16r5qzf8ri4z2xfb3w")))

(define-public crate-shuuro-0.5.81 (c (n "shuuro") (v "0.5.81") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "049ky8fw1rn4ckicaj1nh2n4x2gc43lahfd5g57hdwsvmldndcgl")))

(define-public crate-shuuro-0.5.82 (c (n "shuuro") (v "0.5.82") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rx60qgp74j8w5x3wq6z7k78m0hv3r5gz239khxh1v95b8ml4c3j")))

(define-public crate-shuuro-0.5.83 (c (n "shuuro") (v "0.5.83") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bs3aa36s8vp1gsgvq5l8fsxng0lvqfcvycnwqc64agl0i81d4xb")))

(define-public crate-shuuro-0.5.84 (c (n "shuuro") (v "0.5.84") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dv18w4xrc4di3gyq5wb3pz9h2hi1hsr2if56gwnq1574d7y5v24")))

(define-public crate-shuuro-0.5.85 (c (n "shuuro") (v "0.5.85") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hwq7z8qwrv6bfc9srlpdqqdlx9yl7xpqngasmyk6gn5rx9mddrc")))

(define-public crate-shuuro-0.5.86 (c (n "shuuro") (v "0.5.86") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1agjk47llwhi2s23pwfkgdsyfsv0aa4dqmbsg63bhd9v9xp8wn91")))

(define-public crate-shuuro-0.5.87 (c (n "shuuro") (v "0.5.87") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xhixl732vlh20bwnvxv6gbglpy999p12d468zlsk5ib0l1rbj37")))

(define-public crate-shuuro-0.5.88 (c (n "shuuro") (v "0.5.88") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qngjgrbryqjwaxc2ghj7xfifjjkvc84h1vgjlcp94jm63sxwkn8")))

(define-public crate-shuuro-0.5.89 (c (n "shuuro") (v "0.5.89") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08mvj92zn46n8raadmd8yxykyikfsb1j6ckbck02vbxznqqw6hzh")))

(define-public crate-shuuro-0.5.90 (c (n "shuuro") (v "0.5.90") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16zb3mlc6lki3rgfmq5c9ydrwl095xan98vbvrniwbal59ziq0nl")))

(define-public crate-shuuro-0.5.91 (c (n "shuuro") (v "0.5.91") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k2n2l27pl93i77h6id87iqh4blf740viajcb6ycdji503csg1xn")))

(define-public crate-shuuro-0.5.92 (c (n "shuuro") (v "0.5.92") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f9fr8kwd1x9x3izz7rrzrj54h7241kzyhicqxwc109nfav4dap0")))

(define-public crate-shuuro-0.5.93 (c (n "shuuro") (v "0.5.93") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hhpaa8ylkz9ids25nbvax9gs0rv27msym5plicmy708g37xybyk")))

(define-public crate-shuuro-0.5.94 (c (n "shuuro") (v "0.5.94") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "084m1q3fji1vmrjm17dhmjib6z7jgdbzwhm7pm09q9pphhdkhkcp")))

(define-public crate-shuuro-0.5.95 (c (n "shuuro") (v "0.5.95") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z82p2liv9kdd3vxx3mnmf46xjfdmlr2igh01lz0xbiczh4v5nj8") (f (quote (("standard") ("shuuro8") ("shuuro12") ("default"))))))

(define-public crate-shuuro-0.6.0 (c (n "shuuro") (v "0.6.0") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "113ncqb3wg4cw9lf8cnq5r9rdi5yb804ww8k9yr2w0h571mwp17p") (f (quote (("standard") ("shuuro8") ("shuuro12") ("default"))))))

(define-public crate-shuuro-0.7.0 (c (n "shuuro") (v "0.7.0") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "091xa3rlinn167bvg27pb812a6401rs23kfqp8j83xr0pjzw7ysl") (f (quote (("standard") ("shuuro8") ("shuuro12") ("default"))))))

(define-public crate-shuuro-0.8.0 (c (n "shuuro") (v "0.8.0") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06lc536jyajawp34a84hl76hqf5ild9jbgy3c4lqnsn2z993i2y2") (f (quote (("standard") ("shuuro8") ("shuuro12") ("default"))))))

(define-public crate-shuuro-0.8.1 (c (n "shuuro") (v "0.8.1") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0983bg901cf1maxy4mldnnhsvfbhg306i5abgq07jca849wd041g") (f (quote (("standard") ("shuuro8") ("shuuro12") ("default"))))))

