(define-module (crates-io sh an shank_render) #:use-module (crates-io))

(define-public crate-shank_render-0.0.1 (c (n "shank_render") (v "0.0.1") (d (list (d (n "prettyplease") (r "^0.1.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.12") (d #t) (k 0)))) (h "1pxcschlmki8lkgdwcw0k1szgbai2mi0j4h14hj71lbh9h912bhl")))

(define-public crate-shank_render-0.1.0 (c (n "shank_render") (v "0.1.0") (d (list (d (n "prettyplease") (r "^0.1.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.1.0") (d #t) (k 0)))) (h "1amw8rhn7vrf1kh8vgzq45jaz8in7zrx5p9pgb2j393lrgj1xr4c")))

(define-public crate-shank_render-0.2.0 (c (n "shank_render") (v "0.2.0") (d (list (d (n "prettyplease") (r "^0.1.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.2.0") (d #t) (k 0)))) (h "0xfhkhq4b8ynypx44a8bcpcyaq6x78xwxxbi1qv84i8zm90izpnf")))

(define-public crate-shank_render-0.2.1 (c (n "shank_render") (v "0.2.1") (d (list (d (n "prettyplease") (r "^0.1.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.2.1") (d #t) (k 0)))) (h "0s9x7fhh5swjazci1j8llb8gv6cvbdglv21a8vxpsxjc7iv0ngz6")))

(define-public crate-shank_render-0.3.0 (c (n "shank_render") (v "0.3.0") (d (list (d (n "prettyplease") (r "^0.1.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.3.0") (d #t) (k 0)))) (h "00vjb1ylc6jb5inrx0cqn02fs268x7s3rrl17cdk3slmvp3ajbjs")))

(define-public crate-shank_render-0.3.1-alpha.4 (c (n "shank_render") (v "0.3.1-alpha.4") (d (list (d (n "prettyplease") (r "^0.1.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.3.1-alpha.4") (d #t) (k 0)))) (h "0r9ljypm75vayqyk9aq42map5ff5i0l8sc18bni23n6acahw6acp")))

(define-public crate-shank_render-0.3.1-alpha.5 (c (n "shank_render") (v "0.3.1-alpha.5") (d (list (d (n "prettyplease") (r "^0.1.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.3.1-alpha.5") (d #t) (k 0)))) (h "0rjil7is1kian3ynjvbvm8i33cpyy3g7l9h56ilnkvcrkw06kybk")))

(define-public crate-shank_render-0.4.0 (c (n "shank_render") (v "0.4.0") (d (list (d (n "prettyplease") (r "^0.1.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.4.0") (d #t) (k 0)))) (h "0sn12k063r660pca9fngq9881v3x172h22mhhsz2fyg21xhgxdpj")))

(define-public crate-shank_render-0.4.1 (c (n "shank_render") (v "0.4.1") (d (list (d (n "prettyplease") (r "^0.1.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.4.1") (d #t) (k 0)))) (h "025kmdpazgy8sc7b5c4mj0vb6arrd8by2y3pj7kigidndj3qf7fv")))

(define-public crate-shank_render-0.4.2 (c (n "shank_render") (v "0.4.2") (d (list (d (n "prettyplease") (r "^0.1.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.4.2") (d #t) (k 0)))) (h "0ph7nd3hh2yllhjivxp1w68hvyihzpmj4n6wi3w8k4c0c6x7a48j")))

