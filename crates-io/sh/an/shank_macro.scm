(define-module (crates-io sh an shank_macro) #:use-module (crates-io))

(define-public crate-shank_macro-0.0.0 (c (n "shank_macro") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "1l8d0pmfv1pgky03dl3ri087bids3rg65w2lzcpi65wyxfgqsd2w")))

(define-public crate-shank_macro-0.0.1 (c (n "shank_macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "1vv0f0x9ksg20405iqh45rm0j2rbg1k53mz1qmh2kcvxpz1z0q28")))

(define-public crate-shank_macro-0.0.2 (c (n "shank_macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0mhxm6h3xlx7gi20jngq74avj41dj4x1s62ryczhmimc6ssx69qa")))

(define-public crate-shank_macro-0.0.3 (c (n "shank_macro") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "01cii9jfbmrjczhfzcw749xg58b98pikvq6l6w5579pjwy5m79zr")))

(define-public crate-shank_macro-0.0.4 (c (n "shank_macro") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0gajfcdfx3hgf991hk304ymgfqaqby98kzf74wka1kj35drbzcy0")))

(define-public crate-shank_macro-0.0.5 (c (n "shank_macro") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "1ldzsb1fq4f2d28j495ha8n2lz9bx3r0i8pmb34xyhx998akbn2x")))

(define-public crate-shank_macro-0.0.6 (c (n "shank_macro") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "1v3m0lapxq311fr5shn9w7nn134vkm63p53vx25clpr6l27zwc38")))

(define-public crate-shank_macro-0.0.7 (c (n "shank_macro") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0472avv285k9m4bi7jrrjjhmdh085kfvnambb10fc2m2abla2v0d")))

(define-public crate-shank_macro-0.0.8 (c (n "shank_macro") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "17silz5ki8q7ab870v7ws8vb9vjy2w04qzq0i1nf6i88apms3f3a")))

(define-public crate-shank_macro-0.0.9 (c (n "shank_macro") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "1as42v193dxrv3xzxyb09274axpj4b9j3fsdgwblpdh7w0ma0hqv")))

(define-public crate-shank_macro-0.0.10 (c (n "shank_macro") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "09k9531aidmp3mlmbv5r4dm3qxsx3l3p9z5n5iifp16zymn7v8xk")))

(define-public crate-shank_macro-0.0.11 (c (n "shank_macro") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.11") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "1j2fzkigzxdvw20lyv6wn2mvfy7irlgiavnck3d4pdz8l4i7v4k3")))

(define-public crate-shank_macro-0.0.12 (c (n "shank_macro") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.0.12") (d #t) (k 0)) (d (n "shank_render") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "02pkr4y7476m7rzcdphlkykcgqd87dvik6npsdmg44m05gmdd61l")))

(define-public crate-shank_macro-0.1.0 (c (n "shank_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "shank_render") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0h2d44s7vl0ffcyj44v6rzwr2hmdp7g40x6rj3lv3aml9jqvk8g4")))

(define-public crate-shank_macro-0.2.0 (c (n "shank_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.2.0") (d #t) (k 0)) (d (n "shank_render") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "064l95gvgygydmc5ih7wrpw12kfikdb1rm80y13cy62v1ks4zfv2")))

(define-public crate-shank_macro-0.2.1 (c (n "shank_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.2.1") (d #t) (k 0)) (d (n "shank_render") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "1wmlg3kz0fkiql6hbv9sva96f5crd9v6kkqc4zr27jnw30fw77ma")))

(define-public crate-shank_macro-0.3.0 (c (n "shank_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.3.0") (d #t) (k 0)) (d (n "shank_render") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0m3p96vjasmdpfgghgqbz48aqbqmydqix6v76cigc59fq1lz1gla")))

(define-public crate-shank_macro-0.3.1-alpha.4 (c (n "shank_macro") (v "0.3.1-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.3.1-alpha.4") (d #t) (k 0)) (d (n "shank_render") (r "^0.3.1-alpha.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "18byq1kmx0plbac1q01zbxiqh82aixqpa5azbvw1qashfr3x3qpz")))

(define-public crate-shank_macro-0.3.1-alpha.5 (c (n "shank_macro") (v "0.3.1-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.3.1-alpha.5") (d #t) (k 0)) (d (n "shank_render") (r "^0.3.1-alpha.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0kkcfvnlklr5488k5l6zp8fifxc52pi6cc0d3vy6n1k7dnp9jd55")))

(define-public crate-shank_macro-0.4.0 (c (n "shank_macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.3.1-alpha.5") (d #t) (k 0)) (d (n "shank_render") (r "^0.3.1-alpha.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "1a17wls26iq42q1z62wq9a8qhnw8hj23mc5m8svbx99gdyl8kn2a")))

(define-public crate-shank_macro-0.4.1 (c (n "shank_macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.4.1") (d #t) (k 0)) (d (n "shank_render") (r "^0.4.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0gry0n8flkndrq3z37g88k4mh9lwqahwiy6v37496l4q3rp5isj9")))

(define-public crate-shank_macro-0.4.2 (c (n "shank_macro") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "shank_macro_impl") (r "^0.4.2") (d #t) (k 0)) (d (n "shank_render") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "1kj452l5rl1iia84y405cy05jywf0nah2839v91y1ggfz12jdgx9")))

