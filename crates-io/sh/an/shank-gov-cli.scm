(define-module (crates-io sh an shank-gov-cli) #:use-module (crates-io))

(define-public crate-shank-gov-cli-0.3.0 (c (n "shank-gov-cli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_gov_idl") (r "^0.3.0") (d #t) (k 0)))) (h "1bh95qz48lzqrbiyr0fkhgi50cfi55xnjwcjiv8y23hazak4qscv")))

(define-public crate-shank-gov-cli-0.3.1 (c (n "shank-gov-cli") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_gov_idl") (r "^0.3.0") (d #t) (k 0)))) (h "19ij7i1d5diw1gmkrjb00yadn571rap74haadmfw6mvjrq1hiykf")))

(define-public crate-shank-gov-cli-0.3.2 (c (n "shank-gov-cli") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_gov_idl") (r "^0.3.1") (d #t) (k 0)))) (h "1sx1av3fhb3xzg07y9xwsxd0l6q9gvdi5ard12rxqvcsri5a5532")))

(define-public crate-shank-gov-cli-0.3.3 (c (n "shank-gov-cli") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_gov_idl") (r "^0.3.1") (d #t) (k 0)))) (h "1kvj1ffxv7pgh99jbgaw9dwfkjirhb419sx634fv2cpg7rxp9c47")))

(define-public crate-shank-gov-cli-0.3.4 (c (n "shank-gov-cli") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_gov_idl") (r "^0.3.1") (d #t) (k 0)))) (h "01b8l9gmn6hy5z9irdrcb0r0rk2mdmwwfh2zwp7nid1c3lvcrnsd")))

