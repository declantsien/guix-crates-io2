(define-module (crates-io sh an shank-cli) #:use-module (crates-io))

(define-public crate-shank-cli-0.0.0 (c (n "shank-cli") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.0.0") (d #t) (k 0)))) (h "0f4id6sdhvnp864xkc9ms8qzwwk8rgrda8459ymxh40kgjx4q5r4")))

(define-public crate-shank-cli-0.0.1 (c (n "shank-cli") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.0.1") (d #t) (k 0)))) (h "1my5w9af82qgbiv2rihy2lg22pbwzzzdf8naqvwyk51d9sgz493c")))

(define-public crate-shank-cli-0.0.2 (c (n "shank-cli") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.0.2") (d #t) (k 0)))) (h "18narkzwwzc467ggm7fx2mg9rxnmh0lnizz1ci617kwb5c49brqr")))

(define-public crate-shank-cli-0.0.3 (c (n "shank-cli") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.0.3") (d #t) (k 0)))) (h "0hbza2s0nvw4jkqahi3g06r5718ip9l417y1pj8zw98vxg9m1azf")))

(define-public crate-shank-cli-0.0.4 (c (n "shank-cli") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.0.4") (d #t) (k 0)))) (h "1g16i3vs3c0fgp13criskn0rlwxprswag98cq18ylhd6rv0qg5g4")))

(define-public crate-shank-cli-0.0.5 (c (n "shank-cli") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.0.5") (d #t) (k 0)))) (h "00iyp4zbsccvx2p5i5k68ijrzsihn03yccn9qn5s4pnzakga7n90")))

(define-public crate-shank-cli-0.0.6 (c (n "shank-cli") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.0.6") (d #t) (k 0)))) (h "0c7w4gvn9csnr23vkm3qrc01d26i90viyr685c03gq3f0xmmhzcp")))

(define-public crate-shank-cli-0.0.7 (c (n "shank-cli") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.0.7") (d #t) (k 0)))) (h "0724ynqfpdr04maf4zqsa226bsh477pjs8zdjmz5a63phhrkbz5b")))

(define-public crate-shank-cli-0.0.8 (c (n "shank-cli") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.0.8") (d #t) (k 0)))) (h "0k1qss858cwlawyx84gl9h1v4r19x4rmxwmj5bd2lafajc0v6syy")))

(define-public crate-shank-cli-0.0.9 (c (n "shank-cli") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.0.9") (d #t) (k 0)))) (h "1jlp02njli82a3y34kkm82mgj07zh1jwdj3fkg7gqkd9blmjn6jd")))

(define-public crate-shank-cli-0.0.10 (c (n "shank-cli") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.0.10") (d #t) (k 0)))) (h "1lq87k6kczwxn6jvj165hxd76g0fz5z85997v5xx37lgmbndwdrm")))

(define-public crate-shank-cli-0.0.11 (c (n "shank-cli") (v "0.0.11") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.0.11") (d #t) (k 0)))) (h "1wrn082cjqrgx9p79rm44iachl2lary7wm7i3lyp0m0g1xhapxv2")))

(define-public crate-shank-cli-0.0.12 (c (n "shank-cli") (v "0.0.12") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.0.12") (d #t) (k 0)))) (h "0wcf5d52hl8psb2khijqlm3w1mjp73531s7b81l3ig2gcmw8dgyw")))

(define-public crate-shank-cli-0.1.0 (c (n "shank-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.1.0") (d #t) (k 0)))) (h "088jr5idw8nlgf533p2xcli40qikyrvd6wbxcb30fw5hz6v5anda")))

(define-public crate-shank-cli-0.2.0 (c (n "shank-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.2.0") (d #t) (k 0)))) (h "10lrrc083dkc065m5fmhrxxsb07nxk268y10fdv60wlh6wsqns8q")))

(define-public crate-shank-cli-0.2.1 (c (n "shank-cli") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.2.1") (d #t) (k 0)))) (h "00i812cr7ivy335dnqzfaza4w1vgcg72qcfr7yvkfx7wli4imlci")))

(define-public crate-shank-cli-0.3.0 (c (n "shank-cli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.3.0") (d #t) (k 0)))) (h "1vnr5yd6fwsnc38zgfdfvvfiv9zizs4jf8xylg3bybgiifn6k7bx")))

(define-public crate-shank-cli-0.3.1-alpha.4 (c (n "shank-cli") (v "0.3.1-alpha.4") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.3.1-alpha.4") (d #t) (k 0)))) (h "1whsc423jkb179921j8zd4c808dfkscaklhw3gh64l6yx5dgf4hm")))

(define-public crate-shank-cli-0.3.1-alpha.5 (c (n "shank-cli") (v "0.3.1-alpha.5") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.3.1-alpha.5") (d #t) (k 0)))) (h "0cdpjq5c01iszfk4ynv36v4m4xfkwv2ligmhp5k4m7cvgifgwpk1")))

(define-public crate-shank-cli-0.4.0 (c (n "shank-cli") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.3.1-alpha.5") (d #t) (k 0)))) (h "1c72vdyb4vps4zh78fr9clw9irvyi5zlxzbg6g1qlfqaxrhahd7m")))

(define-public crate-shank-cli-0.4.1 (c (n "shank-cli") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.4.1") (d #t) (k 0)))) (h "1gz9sgrw7mafdxz65nrlrm6x11pdv2z5hnp742d3ds338h5zrx5b")))

(define-public crate-shank-cli-0.4.2 (c (n "shank-cli") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "shank_idl") (r "^0.4.2") (d #t) (k 0)))) (h "07dvapz4iqwnb5wp1wrn8ipci75af0c7g20k5a2mijjphhnndnpz")))

