(define-module (crates-io sh an shank) #:use-module (crates-io))

(define-public crate-shank-0.0.0 (c (n "shank") (v "0.0.0") (d (list (d (n "shank_macro") (r "^0.0.0") (d #t) (k 0)))) (h "1kikn88hxw2d98r0lqy849p35cbp5j45hsal4kmzaqhq6zynz93w")))

(define-public crate-shank-0.0.1 (c (n "shank") (v "0.0.1") (d (list (d (n "shank_macro") (r "^0.0.1") (d #t) (k 0)))) (h "0rw1512lr7jdc34igp59zig4yxm385lfmbwdqlz66l6w1k1jn7i4")))

(define-public crate-shank-0.0.2 (c (n "shank") (v "0.0.2") (d (list (d (n "shank_macro") (r "^0.0.2") (d #t) (k 0)))) (h "0pg9af3inxgbphf7qkc89dg134z3i42s6bp62ngyc73xfhbddkx9")))

(define-public crate-shank-0.0.3 (c (n "shank") (v "0.0.3") (d (list (d (n "shank_macro") (r "^0.0.3") (d #t) (k 0)))) (h "007jgb0w27jbx6k69vd6xah78ir6lra7fapz33kq2w66qjmgiiv2")))

(define-public crate-shank-0.0.4 (c (n "shank") (v "0.0.4") (d (list (d (n "shank_macro") (r "^0.0.4") (d #t) (k 0)))) (h "1n42kap5gwbwpfnss0qz2rk6ay1fq1p1ipyia3cizgbav75cd9l5")))

(define-public crate-shank-0.0.5 (c (n "shank") (v "0.0.5") (d (list (d (n "shank_macro") (r "^0.0.5") (d #t) (k 0)))) (h "06qcihbshps9rxgfjj8m16ypayhyi0mcm58zwk5jpx76jyr8d6bs")))

(define-public crate-shank-0.0.6 (c (n "shank") (v "0.0.6") (d (list (d (n "shank_macro") (r "^0.0.6") (d #t) (k 0)))) (h "1wjrqkvj8cfk0hhx21ngy6q5nxabmsg4l23sq84bwm9cv9s33qlp")))

(define-public crate-shank-0.0.7 (c (n "shank") (v "0.0.7") (d (list (d (n "shank_macro") (r "^0.0.7") (d #t) (k 0)))) (h "1q9bsy5xb3bm4csjjphgc2y5yj59qiv73vizk64w2l8404xr0gci")))

(define-public crate-shank-0.0.8 (c (n "shank") (v "0.0.8") (d (list (d (n "shank_macro") (r "^0.0.8") (d #t) (k 0)))) (h "068qz3waknw2ssyzczb6jq0a9sjy1kii98128s2x46hlb9nw0d99")))

(define-public crate-shank-0.0.9 (c (n "shank") (v "0.0.9") (d (list (d (n "shank_macro") (r "^0.0.9") (d #t) (k 0)))) (h "1q45gsyyaa7fk3g62wgwdn12mp9zcj7x9922a1nzzahqpry6ak5m")))

(define-public crate-shank-0.0.10 (c (n "shank") (v "0.0.10") (d (list (d (n "shank_macro") (r "^0.0.10") (d #t) (k 0)))) (h "08bv0fd3cv4b5fmgcgm93hakmkq3b5ziwial10qi3hwck6wdgc4h")))

(define-public crate-shank-0.0.11 (c (n "shank") (v "0.0.11") (d (list (d (n "shank_macro") (r "^0.0.11") (d #t) (k 0)))) (h "19blb7sljfq9w0nyy2f58433cjf78jafh4pk72mqibcmbrdmcgmn")))

(define-public crate-shank-0.0.12 (c (n "shank") (v "0.0.12") (d (list (d (n "shank_macro") (r "^0.0.12") (d #t) (k 0)))) (h "1gkh7d8mjwyn8xnh0m2x46x8dzzw6v730c8bfmvwgd5859a01723")))

(define-public crate-shank-0.1.0 (c (n "shank") (v "0.1.0") (d (list (d (n "shank_macro") (r "^0.1.0") (d #t) (k 0)))) (h "00wri7k1sw7m70931pddigwhh9p3zvxxk5wqm866wm8fpwd68pn3")))

(define-public crate-shank-0.2.0 (c (n "shank") (v "0.2.0") (d (list (d (n "shank_macro") (r "^0.2.0") (d #t) (k 0)))) (h "0hg4pvlkm9wrvbkgx9pf14hsgksp15g2s0fdfd4k2spgjyxqf8l7")))

(define-public crate-shank-0.2.1 (c (n "shank") (v "0.2.1") (d (list (d (n "shank_macro") (r "^0.2.1") (d #t) (k 0)))) (h "1ayvbydv31209p74g1q623afznqfwgisjrvkdy9lmnf01x8lgijb")))

(define-public crate-shank-0.3.0 (c (n "shank") (v "0.3.0") (d (list (d (n "shank_macro") (r "^0.3.0") (d #t) (k 0)))) (h "0ddmrb5c0zmplks4gf7f8ffijpq9ibvyw997aadbd4yl29b3kjap")))

(define-public crate-shank-0.3.1-alpha.4 (c (n "shank") (v "0.3.1-alpha.4") (d (list (d (n "shank_macro") (r "^0.3.1-alpha.4") (d #t) (k 0)))) (h "1m0ngkzfsp46c7569pnah192b8c1kips9yxx52p7glsa72nxl66m")))

(define-public crate-shank-0.3.1-alpha.5 (c (n "shank") (v "0.3.1-alpha.5") (d (list (d (n "shank_macro") (r "^0.3.1-alpha.5") (d #t) (k 0)))) (h "15412j9inpngf6py6qg9qnqbw9rmdsswpk66dgsm2lv3cwxh57l5")))

(define-public crate-shank-0.4.0 (c (n "shank") (v "0.4.0") (d (list (d (n "shank_macro") (r "^0.3.1-alpha.5") (d #t) (k 0)))) (h "0k1w4nsdg91ivbns3yv52fpgh6x7gnq81arqph1c962mps02hv01")))

(define-public crate-shank-0.4.1 (c (n "shank") (v "0.4.1") (d (list (d (n "shank_macro") (r "^0.4.1") (d #t) (k 0)))) (h "0n02k3kn65kjh0mhlrpbf0ybwn02kq56x0gm49lzsjc409a565lp")))

(define-public crate-shank-0.4.2 (c (n "shank") (v "0.4.2") (d (list (d (n "shank_macro") (r "^0.4.2") (d #t) (k 0)))) (h "1m9ijs63ypm5kdbq9fc6ajpx0qhwxphhym957dhwxm4kaj2r9n13")))

