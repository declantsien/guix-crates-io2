(define-module (crates-io sh ri shrimple-parser) #:use-module (crates-io))

(define-public crate-shrimple-parser-0.0.1 (c (n "shrimple-parser") (v "0.0.1") (h "1l44fnkzv2i8vw3cf2hzgp4r964yq2zfkl4nvvcwbcjm98wz6cbi")))

(define-public crate-shrimple-parser-0.0.2 (c (n "shrimple-parser") (v "0.0.2") (h "0gpv2i988sj65mr0ihw7nwjrjnfyy5nzx2dxafzpcy9vxcg2q06x") (f (quote (("nightly"))))))

(define-public crate-shrimple-parser-0.0.3 (c (n "shrimple-parser") (v "0.0.3") (h "0vlb0l1dw1dr59j4gc3n5cbjc7n6xr5hdlxaz68f5l9fbnc6an17") (f (quote (("nightly"))))))

