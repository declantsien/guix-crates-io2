(define-module (crates-io sh ri shrimplang) #:use-module (crates-io))

(define-public crate-shrimplang-0.1.0 (c (n "shrimplang") (v "0.1.0") (d (list (d (n "logos") (r "^0.13") (d #t) (k 0)) (d (n "rustyline") (r "^12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.46") (d #t) (k 0)))) (h "1a9k4wrmrp0f5kjw78jfv9iq5p2k0s55xmzpfb8dksf7ldw5k81g") (f (quote (("with-file-history") ("default" "with-file-history") ("debug"))))))

