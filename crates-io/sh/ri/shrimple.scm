(define-module (crates-io sh ri shrimple) #:use-module (crates-io))

(define-public crate-shrimple-0.1.0 (c (n "shrimple") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mlua") (r "^0.9") (f (quote ("lua54"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1gck51la5rhszqjkz99qv6rm9izm9bzwfp5qjkkfrprzf7c2wqls")))

(define-public crate-shrimple-0.2.0 (c (n "shrimple") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mlua") (r "^0.9") (f (quote ("lua54"))) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "shrimple-localhost") (r "^4") (d #t) (k 0)) (d (n "shrimple-parser") (r "^0.0.2") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)))) (h "1gg1a6r0qk16mv41qahyxw400h51v4lw8nd21nk8bvvgmfa9pi7m")))

