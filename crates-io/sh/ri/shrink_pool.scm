(define-module (crates-io sh ri shrink_pool) #:use-module (crates-io))

(define-public crate-shrink_pool-0.9.0 (c (n "shrink_pool") (v "0.9.0") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)))) (h "0b9q1w35xfzik6izzlrs3q4dasvinpdxr3qb5a9vqp6m0jlph3q9")))

(define-public crate-shrink_pool-0.9.1 (c (n "shrink_pool") (v "0.9.1") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)))) (h "0gs4yg0grxbz4hkwvllyf8w9dmw1jzpb1q1pvf6mi3f4ij4vhrs7")))

(define-public crate-shrink_pool-1.0.0 (c (n "shrink_pool") (v "1.0.0") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)))) (h "0jyj4q4fd5r8s11m33px1q0v1fmnq4amjizqjc7xj7wz8aaxmrqv")))

