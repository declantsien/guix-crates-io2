(define-module (crates-io sh ri shrimple-localhost) #:use-module (crates-io))

(define-public crate-shrimple-localhost-1.0.0 (c (n "shrimple-localhost") (v "1.0.0") (h "1chms1i8rg44kks3hiwqa3gjiykqgs1a3wv5v6jw0f7d4lr2yj3k")))

(define-public crate-shrimple-localhost-2.0.0 (c (n "shrimple-localhost") (v "2.0.0") (h "1bas3mcwy1hsq97nsiqjjs5fmza2xlzmvb09mxi77ijrr7rxd16l")))

(define-public crate-shrimple-localhost-2.1.0 (c (n "shrimple-localhost") (v "2.1.0") (h "13nz0diw9xyd2gzbacrkdk9qp84xmw4z86jqagmr3w0mbhf3545n")))

(define-public crate-shrimple-localhost-3.0.0 (c (n "shrimple-localhost") (v "3.0.0") (h "1ll8mvw6dmqaljh1hz7l2rm54diqb9yschi9ghnx3716vihg8sk1")))

(define-public crate-shrimple-localhost-4.0.0 (c (n "shrimple-localhost") (v "4.0.0") (h "136a522kd7g7aylxrzvdvb8km8vgrs5kwxwyw4yvknf19zps78zb")))

