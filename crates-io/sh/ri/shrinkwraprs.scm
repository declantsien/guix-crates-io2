(define-module (crates-io sh ri shrinkwraprs) #:use-module (crates-io))

(define-public crate-shrinkwraprs-0.0.1 (c (n "shrinkwraprs") (v "0.0.1") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "075z1x1xsnhc28s4mrfaf30q8n2804jppcqh0845zxgmwd3f0cp3") (f (quote (("strict"))))))

(define-public crate-shrinkwraprs-0.0.2 (c (n "shrinkwraprs") (v "0.0.2") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "16fbv5590pp2rjg9vj3vx777g2f2899g3fagng9nawdmyvvgk27g") (f (quote (("strict"))))))

(define-public crate-shrinkwraprs-0.1.0 (c (n "shrinkwraprs") (v "0.1.0") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0bdlqd2jgxs4xi4kn7xpnl0cr6z84xv2isfymxaxj1ag7ydfrkaw") (f (quote (("strict"))))))

(define-public crate-shrinkwraprs-0.1.1 (c (n "shrinkwraprs") (v "0.1.1") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "00s7pif0m5114pbi7jnxabbhkkjqzmbghy63k9p1qjdsdpgic7n0") (f (quote (("strict"))))))

(define-public crate-shrinkwraprs-0.2.0 (c (n "shrinkwraprs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "09my2g863kg8im8www6ggzan9555yr8p19a5ysidp6775zqrlvfq") (f (quote (("strict"))))))

(define-public crate-shrinkwraprs-0.2.1 (c (n "shrinkwraprs") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "1176v3g04ryrkp4xvbx8z566zy31p9ydcwzz4qajvjmjj1xh8pvx") (f (quote (("strict") ("std") ("default" "std"))))))

(define-public crate-shrinkwraprs-0.2.2 (c (n "shrinkwraprs") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "16nivnh98iy58m8ra2av7yya8zn5kdg2r8zbrs3gc1g2f82wcybd") (f (quote (("strict") ("std") ("default" "std"))))))

(define-public crate-shrinkwraprs-0.2.3 (c (n "shrinkwraprs") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "1iyxbxscfr59vmi79qlf17r49ggxaqdnpfaf1s7y1sfbjvg5ysc3") (f (quote (("strict") ("std") ("default" "std"))))))

(define-public crate-shrinkwraprs-0.3.0 (c (n "shrinkwraprs") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("default" "extra-traits"))) (d #t) (k 0)))) (h "0cd33zc4aad9fxr3gn55wqgcl71fza50cazy0svdydi32i26fgp6") (f (quote (("strict") ("std") ("default" "std"))))))

