(define-module (crates-io sh ri shrink) #:use-module (crates-io))

(define-public crate-shrink-0.2.0 (c (n "shrink") (v "0.2.0") (d (list (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1rljj18mwqrf0s10y44msc4n5ch92a2nw1qcp0djfmddi093sbmn")))

