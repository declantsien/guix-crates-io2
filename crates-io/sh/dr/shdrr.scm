(define-module (crates-io sh dr shdrr) #:use-module (crates-io))

(define-public crate-shdrr-0.1.0 (c (n "shdrr") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.5") (d #t) (k 0)))) (h "18r49zfpgvqci9i443gxcsg782la64218wymlkpfs5a973m7h7d2")))

(define-public crate-shdrr-0.1.1 (c (n "shdrr") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.5") (d #t) (k 0)))) (h "0d4yxpijnpzihjib1wdmq9ziz8x91bw86p5airjhbsm2y48qmlvy")))

(define-public crate-shdrr-0.1.3 (c (n "shdrr") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.5") (d #t) (k 0)))) (h "0fy029laq598nm5sv6002sh7lzyy529zrkmi9m45q7d926sw4ij3")))

