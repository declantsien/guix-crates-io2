(define-module (crates-io sh io shiori_hglobal) #:use-module (crates-io))

(define-public crate-shiori_hglobal-0.1.0 (c (n "shiori_hglobal") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0h4qlw5wkccphc79f26v8z8lm2nk2k02765r3z59j61nsb9245vz")))

(define-public crate-shiori_hglobal-0.1.1 (c (n "shiori_hglobal") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "15d1c8lj7zqm8qb45yy4w1hgl8dvzgrk1153qqiidd6l8wv302yj")))

(define-public crate-shiori_hglobal-0.1.2 (c (n "shiori_hglobal") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "local-encoding") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "06n45afw379kcxmx03hzy1m4n00vv9pph3x2n3labdbmal7j9fa8")))

(define-public crate-shiori_hglobal-0.3.0 (c (n "shiori_hglobal") (v "0.3.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winbase" "stringapiset"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fph01wbc6gqjl1x3k6m2rkxagzx87ghzs1n3jc23fxw1bmnqjd2")))

