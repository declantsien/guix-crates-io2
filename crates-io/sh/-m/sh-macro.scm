(define-module (crates-io sh -m sh-macro) #:use-module (crates-io))

(define-public crate-sh-macro-0.1.0 (c (n "sh-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0nr8mdijcadqs4sjdcprs2f3gb9xs03g8qibqymswpxmhk5apja7")))

(define-public crate-sh-macro-0.1.1 (c (n "sh-macro") (v "0.1.1") (d (list (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "172nf24plfpl88fzvmf9fm1wy7kmgqv4s4k5gcxvlak25mqyh4jj")))

(define-public crate-sh-macro-0.2.0 (c (n "sh-macro") (v "0.2.0") (d (list (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0k3njf8qjhdsr2s5fn6x13r4ha9cb327vq7439jv3311arild59n")))

(define-public crate-sh-macro-0.2.1 (c (n "sh-macro") (v "0.2.1") (d (list (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1143vz5r0gkd4fhg0zdvdrr0c3mzd49jbiqfrakr04rbibiignhn")))

