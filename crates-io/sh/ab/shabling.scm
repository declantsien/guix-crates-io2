(define-module (crates-io sh ab shabling) #:use-module (crates-io))

(define-public crate-shabling-0.0.0 (c (n "shabling") (v "0.0.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0b57jl23bbgh335m5kd1d0sjp08qdjbhgadr2ypgxmg6cb0j7dc5") (y #t)))

(define-public crate-shabling-0.0.1 (c (n "shabling") (v "0.0.1") (h "0ss1dl5jnhgjxj31fgzrsa3lqfb07wyn4bkrd3g37sr9cwax7ga8") (y #t)))

