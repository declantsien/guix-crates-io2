(define-module (crates-io sh ak shakespeare) #:use-module (crates-io))

(define-public crate-shakespeare-0.0.0 (c (n "shakespeare") (v "0.0.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "shakespeare-macro") (r "=0.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "0kz231295gkpfi7j1dfdy1qgskrs5xnf60brg7j4szkzh1zqpyyj")))

(define-public crate-shakespeare-0.0.1 (c (n "shakespeare") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "shakespeare-macro") (r "=0.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.91") (d #t) (k 2)))) (h "0b8n703lgiy8sxs8291mpr96f94r4590d292nr0bigpfm0kis55q")))

