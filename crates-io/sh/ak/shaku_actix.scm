(define-module (crates-io sh ak shaku_actix) #:use-module (crates-io))

(define-public crate-shaku_actix-0.1.0 (c (n "shaku_actix") (v "0.1.0") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "shaku") (r ">=0.5.0, <0.7.0") (f (quote ("thread_safe"))) (d #t) (k 0)))) (h "12x8zpz8zmsrilhdggazc02h6f4krx9i3qrmrp9c3hn0p89rgyi5")))

(define-public crate-shaku_actix-0.1.1 (c (n "shaku_actix") (v "0.1.1") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "shaku") (r ">=0.5.0, <0.7.0") (f (quote ("thread_safe"))) (d #t) (k 0)))) (h "14mnir5xpk40dpd88711742mfdk799ahjlhfmbq4g0rm74rymbk2")))

(define-public crate-shaku_actix-0.2.0 (c (n "shaku_actix") (v "0.2.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "shaku") (r ">=0.5.0, <0.7.0") (f (quote ("thread_safe"))) (d #t) (k 0)))) (h "0mqmg8vs9h78wb8jnli03fx24v3cfpnwrkms3hpy6ka7pbgr9gxl")))

