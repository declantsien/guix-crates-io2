(define-module (crates-io sh ak shaku_derive) #:use-module (crates-io))

(define-public crate-shaku_derive-0.1.0 (c (n "shaku_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0rh0clqx54waif1m4qh1ci9jw5p78cn4kb0489jja0fpygw53xjc")))

(define-public crate-shaku_derive-0.2.0 (c (n "shaku_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ad7d7zqqb0ryly9vawyj2gqdm0mpm3rqsxbyf1qw9pnrhyihbii")))

(define-public crate-shaku_derive-0.3.0 (c (n "shaku_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1xs36v3q0vp1zj4mqrjsmhwwz5mh65hwaljqam4ry5rshqmxnj3v")))

(define-public crate-shaku_derive-0.3.1 (c (n "shaku_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0g7x41paz9xxqv3h7q77f4f2nhy6mq83anm1klzrra554pv99s8x")))

(define-public crate-shaku_derive-0.4.0 (c (n "shaku_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "12lwsbk9wm6cy5lyk7ka02si6czrflqwj03hamvmnsakgqxnv124")))

(define-public crate-shaku_derive-0.4.1 (c (n "shaku_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "10vz5ga1hlyhrcj31c7dldjgl72b22w79y5ssb778502gyw9iqpr")))

(define-public crate-shaku_derive-0.5.0 (c (n "shaku_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "088a8j7m2m4gn6j497qxh3vgq3104z2psrk3y48qk9j7ljzfhw6r")))

(define-public crate-shaku_derive-0.6.0 (c (n "shaku_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "153lyacv9v4ryn6na2zq92ciigggs8bgx9mvraqza5b6hby6sb4l")))

(define-public crate-shaku_derive-0.6.1 (c (n "shaku_derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1p57mw871z5pd8i7q72amklmwj5wsdbqdr614lc9r2wm3wgpjksj")))

