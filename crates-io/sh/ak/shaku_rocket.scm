(define-module (crates-io sh ak shaku_rocket) #:use-module (crates-io))

(define-public crate-shaku_rocket-0.1.0 (c (n "shaku_rocket") (v "0.1.0") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "shaku") (r "~0.1.0") (d #t) (k 0)))) (h "0wda5khb6bjhd1nd10fr8x4kh5qxracfrskr1sl4qmgl2dy6qkvx")))

(define-public crate-shaku_rocket-0.2.0 (c (n "shaku_rocket") (v "0.2.0") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "shaku") (r "~0.2.0") (d #t) (k 0)))) (h "0zzfxx1b40s5j1fbid6miicw8ysnbzzg8fn6x478rfv7pznxxls7")))

(define-public crate-shaku_rocket-0.3.0 (c (n "shaku_rocket") (v "0.3.0") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "shaku") (r "~0.3.0") (d #t) (k 0)))) (h "1b51bfk8q16gigjiwgf2l0gf0mj14wm1cck2ni0bhf0xplwfkzqh")))

(define-public crate-shaku_rocket-0.3.1 (c (n "shaku_rocket") (v "0.3.1") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "shaku") (r "~0.3.1") (d #t) (k 0)))) (h "1dklmqvqwz2dqhp9xr1yav35hq60lr3chq9dx9rjnl0gm2k44pg7")))

(define-public crate-shaku_rocket-0.4.0 (c (n "shaku_rocket") (v "0.4.0") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "shaku") (r "~0.4.0") (f (quote ("thread_safe"))) (d #t) (k 0)))) (h "053ki8qpjhajgaz1a60pm0nzhqwcps7i6mxd41dlgh75232kiffa")))

(define-public crate-shaku_rocket-0.5.0 (c (n "shaku_rocket") (v "0.5.0") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "shaku") (r "~0.5.0") (f (quote ("thread_safe"))) (d #t) (k 0)))) (h "1n2dwgrslp3c920vhysnjhi4013n6b9kl78vzrh041vvh8jm0hii")))

(define-public crate-shaku_rocket-0.5.1 (c (n "shaku_rocket") (v "0.5.1") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "shaku") (r ">=0.5.0, <0.7.0") (f (quote ("thread_safe"))) (d #t) (k 0)))) (h "0034izahxmqmczld7afi1fvszmznfhqs6mz3300lvq06clvrh1ly")))

(define-public crate-shaku_rocket-0.6.0 (c (n "shaku_rocket") (v "0.6.0") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "shaku") (r ">=0.5.0, <0.7.0") (f (quote ("thread_safe"))) (d #t) (k 0)))) (h "01sx3s4a96p96k9dlr1fq53rzjv7r6ld92gdy5fi2y6ysd9y15pb")))

(define-public crate-shaku_rocket-0.7.0-rc.1 (c (n "shaku_rocket") (v "0.7.0-rc.1") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "shaku") (r ">=0.5.0, <0.7.0") (f (quote ("thread_safe"))) (d #t) (k 0)))) (h "064xghncjgx3z1kqfgahba71kgk69wmnyk9xxzirslvbv7z50k64")))

(define-public crate-shaku_rocket-0.7.0 (c (n "shaku_rocket") (v "0.7.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "shaku") (r ">=0.5.0, <0.7.0") (f (quote ("thread_safe"))) (d #t) (k 0)))) (h "0xahkfc9pha3sh68mbsgng4zj02jjpbpxhyg1wgg0dlx0am7z32d")))

