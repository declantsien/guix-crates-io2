(define-module (crates-io sh ak shakshuka) #:use-module (crates-io))

(define-public crate-shakshuka-0.1.0 (c (n "shakshuka") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "1zk14i29wzk9anvx1ifh4lxar9qj3lpa7r7dphr7lh7rfgqa8kga")))

(define-public crate-shakshuka-0.2.0 (c (n "shakshuka") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)))) (h "0dm17simdsj3s1pay2n34qp06if9rx84x6vxrp3lxpij1jg441qy")))

