(define-module (crates-io sh ak shaku) #:use-module (crates-io))

(define-public crate-shaku-0.1.0 (c (n "shaku") (v "0.1.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "shaku_derive") (r "~0.1.0") (o #t) (d #t) (k 0)))) (h "1ylqlmvm0wmjz6xb0zvgm35j5nvfq64ihykjxf30lxn157m1bvgl") (f (quote (("thread_safe") ("derive" "shaku_derive") ("default" "thread_safe" "derive"))))))

(define-public crate-shaku-0.2.0 (c (n "shaku") (v "0.2.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "shaku_derive") (r "~0.2.0") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0cidaag8v298wfjvmy6awyvyzigj5062l1wci9jpd2ng67fdmd5i") (f (quote (("thread_safe") ("derive" "shaku_derive") ("default" "thread_safe" "derive"))))))

(define-public crate-shaku-0.3.0 (c (n "shaku") (v "0.3.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "shaku_derive") (r "~0.3.0") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1n58w7qchwfq3k9icb0750nwn9gkyn8kvx8kbf6wb2ra0hr7rfm6") (f (quote (("thread_safe") ("derive" "shaku_derive") ("default" "thread_safe" "derive"))))))

(define-public crate-shaku-0.3.1 (c (n "shaku") (v "0.3.1") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "shaku_derive") (r "~0.3.1") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1k01f1bvpcm3m2z3kmg62i31x07ahvw158j2bgz8limgwvghr4fx") (f (quote (("thread_safe") ("derive" "shaku_derive") ("default" "thread_safe" "derive"))))))

(define-public crate-shaku-0.4.0 (c (n "shaku") (v "0.4.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "shaku_derive") (r "~0.4.0") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ibd3xhx3knddxrkaxhpxpmrn22x6j126284ppmnzz715gh9dzlm") (f (quote (("thread_safe") ("derive" "shaku_derive") ("default" "thread_safe" "derive"))))))

(define-public crate-shaku-0.4.1 (c (n "shaku") (v "0.4.1") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "shaku_derive") (r "~0.4.1") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "13ccwczwiaajdzl305dnw9v3k4h48nmrc214fclv3s35hm3c2p86") (f (quote (("thread_safe") ("derive" "shaku_derive") ("default" "thread_safe" "derive"))))))

(define-public crate-shaku-0.5.0 (c (n "shaku") (v "0.5.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "shaku_derive") (r "~0.5.0") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0sj6cq9zmrm8xj3vw21wyjgl299c7c5dya6cf3d4sqmarf9hn4w6") (f (quote (("thread_safe") ("derive" "shaku_derive") ("default" "thread_safe" "derive"))))))

(define-public crate-shaku-0.6.0 (c (n "shaku") (v "0.6.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "shaku_derive") (r "~0.6.0") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "10nmvxy7jcjw4pk9iwcng898wfsdh3bzic52nlf1l77pwnw25kga") (f (quote (("thread_safe") ("derive" "shaku_derive") ("default" "thread_safe" "derive"))))))

(define-public crate-shaku-0.6.1 (c (n "shaku") (v "0.6.1") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "shaku_derive") (r "~0.6.0") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "19cjf6qzm7l93xpg6vnnc635sjxs7i1igkh2533y55504mlsfk9k") (f (quote (("thread_safe") ("derive" "shaku_derive") ("default" "thread_safe" "derive"))))))

