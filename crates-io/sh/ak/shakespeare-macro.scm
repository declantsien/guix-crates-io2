(define-module (crates-io sh ak shakespeare-macro) #:use-module (crates-io))

(define-public crate-shakespeare-macro-0.0.0 (c (n "shakespeare-macro") (v "0.0.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "structmeta") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "03fyggsm8ncxlj0jq0rvy5figs9fznr3wlfgk97bh653p250gl0m")))

(define-public crate-shakespeare-macro-0.0.1 (c (n "shakespeare-macro") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0fhw65bj9ac28a4q8wjpd6daz1npk3cidpxb5xcvkfq36mbwb58r")))

