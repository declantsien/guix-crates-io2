(define-module (crates-io sh ak shakejo) #:use-module (crates-io))

(define-public crate-shakejo-0.0.0 (c (n "shakejo") (v "0.0.0") (h "1ya8xi4xsymsl7r8nxcy8sf6jyjqa574i1qbkh33hrpcd53kajk9")))

(define-public crate-shakejo-0.0.1 (c (n "shakejo") (v "0.0.1") (d (list (d (n "blake2") (r "^0.10") (k 0)) (d (n "chacha20poly1305") (r "^0.10") (k 0)) (d (n "hkdf") (r "^0.12") (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "snow") (r "^0.9") (d #t) (k 2)) (d (n "x25519-dalek") (r "^2") (f (quote ("static_secrets"))) (k 0)))) (h "1f4z6y2gvas7b79by50p7ybnpkaxj4ixi9k1v1a8s5ivharxr1dp")))

