(define-module (crates-io sh ak shakti_riscv_hal) #:use-module (crates-io))

(define-public crate-shakti_riscv_hal-0.1.0 (c (n "shakti_riscv_hal") (v "0.1.0") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "riscv") (r "^0.10") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (d #t) (k 0)) (d (n "tock-registers") (r "^0.9.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.2") (d #t) (k 0)))) (h "08x3d5dfqp3908w0l83mawzyr1x1mi3gxmrliyd1ppig1j5b4dql")))

(define-public crate-shakti_riscv_hal-0.1.2 (c (n "shakti_riscv_hal") (v "0.1.2") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "riscv") (r "^0.10") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (d #t) (k 0)) (d (n "tock-registers") (r "^0.9.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.2") (d #t) (k 0)))) (h "182y922rc7ng61jgj5hahz4m4zrkmcqdfcknmmjcf8cvzg3j2iz8")))

