(define-module (crates-io sh ak shakmaty) #:use-module (crates-io))

(define-public crate-shakmaty-0.0.1 (c (n "shakmaty") (v "0.0.1") (h "1j8qnmaj3afw61q3bkcw5gr7xksyznc1zqyv2sbqgj7cby81l6a0") (y #t)))

(define-public crate-shakmaty-0.0.2 (c (n "shakmaty") (v "0.0.2") (d (list (d (n "arrayvec") (r "^0.3") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "0w7j95d5y4awzh6x4grn27hhg5qdp0149kz5d5k2l1f6p4ps8fpm")))

(define-public crate-shakmaty-0.0.3 (c (n "shakmaty") (v "0.0.3") (d (list (d (n "arrayvec") (r "^0.3") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "1yggan24gxyr1vgpb757ai9gpmmg9s8jglsr4cdgh50x53963z74")))

(define-public crate-shakmaty-0.0.4 (c (n "shakmaty") (v "0.0.4") (d (list (d (n "arrayvec") (r "^0.3") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "0aagnyqzwdfzaxkn3fba00nsxdpd3mk1r547gmvih1jcpk4axyrd")))

(define-public crate-shakmaty-0.0.5 (c (n "shakmaty") (v "0.0.5") (d (list (d (n "arrayvec") (r "^0.4.0") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "01ax4gyjngg8bz1gl2v9mhsyhv25d3adsnvmilc52vdh8g73avmg")))

(define-public crate-shakmaty-0.0.6 (c (n "shakmaty") (v "0.0.6") (d (list (d (n "arrayvec") (r "^0.4.0") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "1cqkgpxxh3b4sxwln37z67vqpl7n90lzcbasnc505rj9x8hb9zqg")))

(define-public crate-shakmaty-0.0.7 (c (n "shakmaty") (v "0.0.7") (d (list (d (n "arrayvec") (r "^0.4.0") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "0062b3rs2mqw2i0gz7flk6wp09zjbf8blmcgmy1yxl5886xjdhmw")))

(define-public crate-shakmaty-0.0.8 (c (n "shakmaty") (v "0.0.8") (d (list (d (n "arrayvec") (r "^0.4.0") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "06h3yg7pps9s0akk4lny6izbmsp1cyhi1xbn9m37jf17q2chybn6")))

(define-public crate-shakmaty-0.0.9 (c (n "shakmaty") (v "0.0.9") (d (list (d (n "arrayvec") (r "^0.4.0") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "1hhjqq93sn2i0m2casqknglgr234lsbpfsl0vj8ggbpw944jwlmm")))

(define-public crate-shakmaty-0.0.10 (c (n "shakmaty") (v "0.0.10") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "btoi") (r "^0.1") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "0dm7ss87cifzf40ng1f5iy9fwmsslfpcd630xbcf0a8694s7x1by")))

(define-public crate-shakmaty-0.1.0 (c (n "shakmaty") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "btoi") (r "^0.1") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "10349w2ph46cs1bp5wik0fsnsia2ss0pd66hh07pl6y5rj5k2ksl")))

(define-public crate-shakmaty-0.2.0 (c (n "shakmaty") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.1") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1kwab0ysd2wvvfr5sb6p7012by0vslhmz98cr5k1k82zrplqrhw1")))

(define-public crate-shakmaty-0.3.0 (c (n "shakmaty") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.1") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0y44dpv2nag6vgl1r8jncsbsjr9q85z8vz6hpa37ijciaz3iqm64")))

(define-public crate-shakmaty-0.4.0 (c (n "shakmaty") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.1") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0hh9a49yjx9kqh0jnpp594rbv931s6y1i237iqi1frm1bf0y4hbp")))

(define-public crate-shakmaty-0.4.1 (c (n "shakmaty") (v "0.4.1") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.1") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0ly7xvsz3ii7451hd0r0rx7nl3ykh807yp9c39i0v1j4y9h2mwvk")))

(define-public crate-shakmaty-0.4.2 (c (n "shakmaty") (v "0.4.2") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.1") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1bga1z04iaa665x85h940pfp1mz7l7821fwi2jk6h104dhk9pzzh")))

(define-public crate-shakmaty-0.5.0 (c (n "shakmaty") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.1") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "18f39z9f3q6kl8mzfjx1ypmnhy1a34vaxrmcgjym10w2x15dsgyz")))

(define-public crate-shakmaty-0.5.1 (c (n "shakmaty") (v "0.5.1") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.1") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1jq9fz0yjynxp8605kv2wdq7vmw0j1glllxhqg5c7636jxx2jbc5")))

(define-public crate-shakmaty-0.6.0 (c (n "shakmaty") (v "0.6.0") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.1") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "10qznqmsdssl1wm5bkv3aimkq4f6wsmlczh5cjds4423s538wx2h")))

(define-public crate-shakmaty-0.6.1 (c (n "shakmaty") (v "0.6.1") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.1") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0difgdwlhnrfvi4fvfnlajdd4saw6ad58d7lqnk6ph0xmi44ydln")))

(define-public crate-shakmaty-0.6.2 (c (n "shakmaty") (v "0.6.2") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.2") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0mf6ikbhn5z6xqy38gzc876kzz5dq28pc6kr6yrb5ic6dgnb8l6h")))

(define-public crate-shakmaty-0.6.3 (c (n "shakmaty") (v "0.6.3") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.3") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1h90hyk476b3d637abka9n5d4zdv5rk443r1kxj9sd6bk3izizx6")))

(define-public crate-shakmaty-0.6.4 (c (n "shakmaty") (v "0.6.4") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.3") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0xqn6fk02nw1bjf2z8klzj3g7adqwmyz5zfb00fic2b7qhwc0n8q")))

(define-public crate-shakmaty-0.6.5 (c (n "shakmaty") (v "0.6.5") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.3") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "08577cgnwpchry9za0c1as5va1gh9nazcwfya1nzxbr5njva7ccx")))

(define-public crate-shakmaty-0.6.6 (c (n "shakmaty") (v "0.6.6") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.3") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1bcn1hgp1j919fr21j880bk72n4lnmf54qaarmbcpawwlip2s5zh")))

(define-public crate-shakmaty-0.6.7 (c (n "shakmaty") (v "0.6.7") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.3") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0srib9w8xmzsydw0n7fa0qqz10y02vvisjbssjm9n8rz455nmpns")))

(define-public crate-shakmaty-0.7.0 (c (n "shakmaty") (v "0.7.0") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.3") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "14l8yjwgvhsxfac9mkdimx45qlja6w6dsi2vhazqyps6a0ksjfbl")))

(define-public crate-shakmaty-0.7.1 (c (n "shakmaty") (v "0.7.1") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.3") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0kpcxnsp3rgb3070pkc5f38iwdjbriihsmdadx5i8zg18asl5azd")))

(define-public crate-shakmaty-0.7.2 (c (n "shakmaty") (v "0.7.2") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.3") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0gpvrckdks1ynjbzj9ixvfl7zxmz1ywv0xpj39fkx2f4z0gjy65d")))

(define-public crate-shakmaty-0.8.0 (c (n "shakmaty") (v "0.8.0") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "184f0j2w7czfamibf4jwkicglq18j06bp9z2245kjx6bxh7d416i")))

(define-public crate-shakmaty-0.8.1 (c (n "shakmaty") (v "0.8.1") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0qg716lfk2dq9zjd5kjnsc4b91g9wp9s2iqadfj2bawkyxnjx9qh")))

(define-public crate-shakmaty-0.9.0 (c (n "shakmaty") (v "0.9.0") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1083jhwd0kamrr5iqv5c85ww5b14hr1g0q2sh5bg0znmh4i121g3")))

(define-public crate-shakmaty-0.10.0 (c (n "shakmaty") (v "0.10.0") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1qzm61bi03m9y4mwaghb8h0bq2b77k4fs8w6qci31z2yijyp9c9y")))

(define-public crate-shakmaty-0.11.0 (c (n "shakmaty") (v "0.11.0") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "195ln5sb14adfks09dgry34zqlrjcjk1x0kdg8fvq7m2gbiqm2d1")))

(define-public crate-shakmaty-0.11.1 (c (n "shakmaty") (v "0.11.1") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0qk3sazcsf5xbpyxhclqqd3swzkf6b4zbjba612vx2qv7brb0dr3")))

(define-public crate-shakmaty-0.11.2 (c (n "shakmaty") (v "0.11.2") (d (list (d (n "arrayvec") (r "^0.4") (f (quote ("use_union"))) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "09s9mq00paa1w6cc3i6c792wpcq6jb6rb9r6vmc7djjbzys9bc2n")))

(define-public crate-shakmaty-0.12.0 (c (n "shakmaty") (v "0.12.0") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1ny8is9chhxzsy3s5wkldyvjzb5iv1k8l4pncy3a0kxymaaasq3d")))

(define-public crate-shakmaty-0.13.0 (c (n "shakmaty") (v "0.13.0") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "121pf0g9w5n8lv9chq119d5mxn66qnn84hv63ywg5xj5b6bgznlr")))

(define-public crate-shakmaty-0.13.1 (c (n "shakmaty") (v "0.13.1") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "09gq8rdc7j01k5arf2ijbvdkw82b5h0k7h59bcqx6kgiszv2vz0v")))

(define-public crate-shakmaty-0.14.0 (c (n "shakmaty") (v "0.14.0") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1inca4n4cx541kpakq5q6lns2w3nxkwb33zi5733q9wnspr6w6vx")))

(define-public crate-shakmaty-0.14.1 (c (n "shakmaty") (v "0.14.1") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0i2p8skhr3jl2q6whr8fvh0sr8jiv5i38py3aqx0fgfsj45rxpqq")))

(define-public crate-shakmaty-0.15.0 (c (n "shakmaty") (v "0.15.0") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "00lzapl9sckp3a1rrvcx4f36wagrwgd1n0xgxq1105y63i7ksv64")))

(define-public crate-shakmaty-0.15.1 (c (n "shakmaty") (v "0.15.1") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0fxah0aikrjz5knnc00isf1cif39924irzsminrhk7xr2vvz3q24")))

(define-public crate-shakmaty-0.15.2 (c (n "shakmaty") (v "0.15.2") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "08sl02ppf3c91950l5d5pjrqg0hgylcas8li1ihhhzxj5wcwdyfy")))

(define-public crate-shakmaty-0.15.3 (c (n "shakmaty") (v "0.15.3") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "024va2af4q2b1fvf04gigvq9lls6rr6fpcxk4pl4hrfgzv35khbj")))

(define-public crate-shakmaty-0.16.0 (c (n "shakmaty") (v "0.16.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "08l07hr1k2h1m50ysldcqcpffdpcps1rxwgay5hqcnwm5g212mlr")))

(define-public crate-shakmaty-0.16.1 (c (n "shakmaty") (v "0.16.1") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1iwz0hdjykyzdawgnv6zlsb5627b5llqlr1g5vn1p876lg37vwkp")))

(define-public crate-shakmaty-0.16.2 (c (n "shakmaty") (v "0.16.2") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "024iwsfxc19g84yi6w1wjbyfxvrmlw5dymyj1i9hnpij93yzgy58")))

(define-public crate-shakmaty-0.16.3 (c (n "shakmaty") (v "0.16.3") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "13gf9jy1g971j0c3m1x7x4h5gs1zvvhi37g6fh3mnv7365s8h512")))

(define-public crate-shakmaty-0.16.4 (c (n "shakmaty") (v "0.16.4") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0k1rljsj9h32qdg5mlh1h53z7l7flpphap9wsxqnfx75i8wy9a83")))

(define-public crate-shakmaty-0.17.0 (c (n "shakmaty") (v "0.17.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1lvspid44b8svd18k2nh9zw926dsmab92sb6w8pyrm9yv9xdl6sw")))

(define-public crate-shakmaty-0.17.1 (c (n "shakmaty") (v "0.17.1") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "180k5xjs818ndsdpcbrgn25hmg17q7yv6dpj80kfkb6kc7g8r2z0")))

(define-public crate-shakmaty-0.17.2 (c (n "shakmaty") (v "0.17.2") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0dkijayiwv05ljl8c68c1y72x5a0xl2hm1d1cdf02rqgcaw1a0by")))

(define-public crate-shakmaty-0.18.0 (c (n "shakmaty") (v "0.18.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "19w9b0j4zy42h25787mny0ah1lqx8sf31xkyk4xr19bs5ddxp07m") (f (quote (("variant") ("default"))))))

(define-public crate-shakmaty-0.19.0 (c (n "shakmaty") (v "0.19.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0lbz8y8vw3jyjp8nsymljcgzxycnhxk9ls3i6dd923n1cp37cbsx") (f (quote (("variant") ("default"))))))

(define-public crate-shakmaty-0.20.0 (c (n "shakmaty") (v "0.20.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0xxl6as50c2jgn3i91wnmbsm2n9alkymaq6vnrb46ym5mjgdm3c9") (f (quote (("variant") ("step") ("default"))))))

(define-public crate-shakmaty-0.20.1 (c (n "shakmaty") (v "0.20.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0prbj89m1fw4j04krp9q06048bby162v6757v75p57mw98xfp1j6") (f (quote (("variant") ("step") ("default"))))))

(define-public crate-shakmaty-0.20.2 (c (n "shakmaty") (v "0.20.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1l11q0djq6x4snj6myl9jaimc3r193qb1d11m91yfzmis1bhf82m") (f (quote (("variant") ("step") ("default")))) (r "1.56")))

(define-public crate-shakmaty-0.20.3 (c (n "shakmaty") (v "0.20.3") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1xfzrs7phanbvvwrwgcz6v89vbk4h3d8hgqhch070693g7ap8vip") (f (quote (("variant") ("step") ("default")))) (r "1.56")))

(define-public crate-shakmaty-0.20.4 (c (n "shakmaty") (v "0.20.4") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0xigx408qi6kv1x2y52pkd89qkbj18y34413ss3dyas2m2rnrza8") (f (quote (("variant") ("step") ("default")))) (r "1.56")))

(define-public crate-shakmaty-0.20.5 (c (n "shakmaty") (v "0.20.5") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "0cpx3mnm6zl9nbgl6k895bk7afaqafhb4gvbvais9c66b956k5ag") (f (quote (("variant") ("step") ("default")))) (r "1.56")))

(define-public crate-shakmaty-0.20.6 (c (n "shakmaty") (v "0.20.6") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1am3zvhkvsd4401j21m5wn69mmhzk1ni2aqhr9yacdk0gx1bi5xa") (f (quote (("variant") ("step") ("default")))) (r "1.56")))

(define-public crate-shakmaty-0.20.7 (c (n "shakmaty") (v "0.20.7") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1425x4akq8lmdvj77bqyqhwpn42340ippqcfwiz686ddppqxlkp9") (f (quote (("variant") ("step") ("default")))) (r "1.56")))

(define-public crate-shakmaty-0.21.0 (c (n "shakmaty") (v "0.21.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1rnwm1qd4k8rw93yyi4zphsz16jz33sa7gjca9yp8jw3p0jh54ql") (f (quote (("variant") ("step") ("default")))) (r "1.56")))

(define-public crate-shakmaty-0.21.1 (c (n "shakmaty") (v "0.21.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1f6gg4xbqf05qqjy2ivq8d6gnk9py79ln89lkvrfvz6p6mw02dhx") (f (quote (("variant") ("step") ("default")))) (r "1.56")))

(define-public crate-shakmaty-0.21.2 (c (n "shakmaty") (v "0.21.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "100fy8q8j7n5i7r6vf54r1zj7gkqpfc0mkjawr6dvmlj7f3rlrf5") (f (quote (("variant") ("step") ("default")))) (r "1.60")))

(define-public crate-shakmaty-0.21.3 (c (n "shakmaty") (v "0.21.3") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1s11lxn17hyx3hssmx9v2ql20yj0c241pwvsfc7jwrjcwh36qkhz") (f (quote (("variant") ("step") ("default")))) (r "1.60")))

(define-public crate-shakmaty-0.21.4 (c (n "shakmaty") (v "0.21.4") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "1sk5qp92nfqwk4hwwqcidr9z643kgn9xb72ad2scpyg267i31kbn") (f (quote (("variant") ("step") ("default")))) (r "1.60")))

(define-public crate-shakmaty-0.22.0 (c (n "shakmaty") (v "0.22.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)))) (h "00r0lx2ggfv7rijy6k7v0br08gfivyn4dg83cww0ydsyl15dc88c") (f (quote (("variant") ("step") ("std" "alloc") ("default" "std") ("alloc")))) (r "1.61")))

(define-public crate-shakmaty-0.23.0 (c (n "shakmaty") (v "0.23.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (o #t) (d #t) (k 0)))) (h "19798csnc78zpdm31f744hw5rniz04d1lgm5mkbhirrycdfn7kgl") (f (quote (("variant") ("step") ("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("nohash-hasher" "dep:nohash-hasher")))) (r "1.62")))

(define-public crate-shakmaty-0.23.1 (c (n "shakmaty") (v "0.23.1") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (o #t) (d #t) (k 0)))) (h "17s27xa2sqjlg8y3qsiqji0wn3d1lr185vy67zklwgm9dym82pi9") (f (quote (("variant") ("step") ("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("nohash-hasher" "dep:nohash-hasher")))) (r "1.62")))

(define-public crate-shakmaty-0.24.0 (c (n "shakmaty") (v "0.24.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1aprci1p91cgd5cfxv8kqp2jhcz6h6fk50y9hc6aydawfq3yccqc") (f (quote (("variant") ("step") ("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("nohash-hasher" "dep:nohash-hasher")))) (r "1.63")))

(define-public crate-shakmaty-0.25.0 (c (n "shakmaty") (v "0.25.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0c7dylqn54c59q6w0fha2gx4wwisdllan5207axm34k7blizf8rl") (f (quote (("variant") ("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("nohash-hasher" "dep:nohash-hasher")))) (r "1.63")))

(define-public crate-shakmaty-0.26.0 (c (n "shakmaty") (v "0.26.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1cyg23r3h8qlsb8l8ydl3zsqqn8xxca3bv7q18dh3b3adg9798kx") (f (quote (("variant") ("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("nohash-hasher" "dep:nohash-hasher")))) (r "1.70")))

(define-public crate-shakmaty-0.27.0 (c (n "shakmaty") (v "0.27.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "btoi") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_with") (r "^3.6.1") (d #t) (k 2)))) (h "04sx7arbjjj2ggf2ad4d04zkgwji5canirfy5c5l2nrdjg9j08ws") (f (quote (("variant") ("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("nohash-hasher" "dep:nohash-hasher")))) (r "1.75")))

