(define-module (crates-io sh ak shakmaty-uci) #:use-module (crates-io))

(define-public crate-shakmaty-uci-0.1.0 (c (n "shakmaty-uci") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (k 0)) (d (n "nom_permutation") (r "^0.1.0") (f (quote ("permutation_opt"))) (k 0)) (d (n "shakmaty") (r "^0.27.0") (k 0)))) (h "1y5f0c3ywjqqp8fggh82x5isrlv4zfji8clr7bjwqy7zdjhxl00q") (f (quote (("std" "nom/std" "shakmaty/std") ("default" "std"))))))

