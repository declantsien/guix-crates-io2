(define-module (crates-io sh ak shaku_axum) #:use-module (crates-io))

(define-public crate-shaku_axum-0.1.0 (c (n "shaku_axum") (v "0.1.0") (d (list (d (n "axum") (r "^0.3") (d #t) (k 0)) (d (n "shaku") (r ">=0.5.0, <0.7.0") (f (quote ("thread_safe"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "09nvlx73n2qbxprfqm5lppn0nv9qnfpd527w9ba6g13jkzjnrbzv")))

(define-public crate-shaku_axum-0.2.0 (c (n "shaku_axum") (v "0.2.0") (d (list (d (n "axum") (r "^0.4") (d #t) (k 0)) (d (n "shaku") (r ">=0.5.0, <0.7.0") (f (quote ("thread_safe"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "11qb5c7c5j2dl45y4744fij7rb6szl7h9jnnmknacfd6kq7m2fm4")))

(define-public crate-shaku_axum-0.3.0 (c (n "shaku_axum") (v "0.3.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "shaku") (r ">=0.5.0, <0.7.0") (f (quote ("thread_safe"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "04qv38m2wn45vbq6vdwx2394kqdz76g7wwi44jry9nqpv6yp7h03")))

(define-public crate-shaku_axum-0.4.0 (c (n "shaku_axum") (v "0.4.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "shaku") (r ">=0.5.0, <0.7.0") (f (quote ("thread_safe"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "00asnkay8nar91q34q9id6v064s3l0w8h397qkjvdp48sr44yvl1")))

(define-public crate-shaku_axum-0.5.0 (c (n "shaku_axum") (v "0.5.0") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "shaku") (r ">=0.5.0, <0.7.0") (f (quote ("thread_safe"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1nqayv7w27jcdhvr11v7n71g9kyfz614zy80l424w6izzrx5dn0y")))

