(define-module (crates-io sh as shasum) #:use-module (crates-io))

(define-public crate-shasum-0.4.0 (c (n "shasum") (v "0.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1cb7cnc236m400hdxfw588yv006xza3h016c2fy0krzi1y7ks2pf")))

(define-public crate-shasum-0.5.0 (c (n "shasum") (v "0.5.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0bn7vqklqiipv0ryl990chsvzvp9r7rp161hqcb7b75sx6awg32r")))

(define-public crate-shasum-0.5.1 (c (n "shasum") (v "0.5.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1634141jrzzr1cd078yzqkhnz33bzknbpy70dqnxhmzsnpcygpx5")))

(define-public crate-shasum-0.6.0 (c (n "shasum") (v "0.6.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "02w129hgl92bg4b8vq20imr65ya84lqbgax56yz2sa52c1ln0gav") (y #t)))

(define-public crate-shasum-0.6.1 (c (n "shasum") (v "0.6.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "14cgq2c564pycnkjvm427lj7kll7awm8pfci289rh4h8wc4sr25y")))

(define-public crate-shasum-0.7.0 (c (n "shasum") (v "0.7.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1yzdzixcfz9m87f7yacxkcpkzv90l7qkbdkar8rdrw4yf2dksfy9")))

