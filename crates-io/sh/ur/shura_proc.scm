(define-module (crates-io sh ur shura_proc) #:use-module (crates-io))

(define-public crate-shura_proc-0.1.0 (c (n "shura_proc") (v "0.1.0") (d (list (d (n "syn") (r "^1.0.105") (d #t) (k 0)))) (h "091dqjqd0ndsqz0hgbivlj3v3diyn88xlq33871rhps3b3g7l4jn")))

(define-public crate-shura_proc-0.2.0 (c (n "shura_proc") (v "0.2.0") (d (list (d (n "const-fnv1a-hash") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0wsphfyfd5dlaq9kcds9r8v623xskk4scdiwiswiz123qvmfhdl6")))

(define-public crate-shura_proc-0.3.0 (c (n "shura_proc") (v "0.3.0") (d (list (d (n "const-fnv1a-hash") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "08vlsd0nd7zf1ymxb9hmhzqlyqcpd4xq8spgdksc9cpdrhdbq2v7") (f (quote (("physics"))))))

(define-public crate-shura_proc-0.4.0 (c (n "shura_proc") (v "0.4.0") (d (list (d (n "const-fnv1a-hash") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "10sccnyl12i6rjjsav7hyzc8337k4k6zqxnfvn44ahhvp65kmd1d") (f (quote (("physics"))))))

