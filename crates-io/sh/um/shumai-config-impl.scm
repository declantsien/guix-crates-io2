(define-module (crates-io sh um shumai-config-impl) #:use-module (crates-io))

(define-public crate-shumai-config-impl-0.1.0 (c (n "shumai-config-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f8qjm2nbccvw7pc1lsqq0p134vag64li40d2svp88javk97h80h")))

(define-public crate-shumai-config-impl-0.1.1 (c (n "shumai-config-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12i5lpn2z330znq2rj5qsaim78kcs4h75x68wvrdnsmhivj4bxb3")))

(define-public crate-shumai-config-impl-0.1.2 (c (n "shumai-config-impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qndyzxvh3bam840gp8csf3j1klsji8g3sp3f94wxwzhakqg4aqw")))

(define-public crate-shumai-config-impl-0.1.3 (c (n "shumai-config-impl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z4xx2zv5xqs1g3rivnpm0vgnw36lrdv9vc5fviwpqxg3qyxq9yn")))

(define-public crate-shumai-config-impl-0.1.4 (c (n "shumai-config-impl") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q9ayw5izmxmlkrac3ng7wc79ixdkzdfsg9hhb8gld2kryn03gjr")))

(define-public crate-shumai-config-impl-0.1.5 (c (n "shumai-config-impl") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cl2sv3kqg3a6prx2gqny1nh0cnjwnqqzzxdirzdyjxq58ihc18i")))

(define-public crate-shumai-config-impl-0.1.6 (c (n "shumai-config-impl") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vkmrp0gxijs7wk5caz9q2i18s8rcvf5g1ii1cl9387jxbxsnlim")))

(define-public crate-shumai-config-impl-0.2.0 (c (n "shumai-config-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nilm2lpbpc1vqbgwhd1727wxwfqzg7ni2faawmads0zavfymyr3")))

(define-public crate-shumai-config-impl-0.2.1 (c (n "shumai-config-impl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xsxbz45zl1ch5lyfdy9zr1fci1jhi1c4q74iyfkykl74qm77h7g")))

(define-public crate-shumai-config-impl-0.2.2 (c (n "shumai-config-impl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yhgxwk64y2xk6i7sc37xk8g70mwlxk2s06n0mlm9srdfqaj14ja")))

