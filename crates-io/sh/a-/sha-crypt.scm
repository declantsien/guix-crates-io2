(define-module (crates-io sh a- sha-crypt) #:use-module (crates-io))

(define-public crate-sha-crypt-0.1.0 (c (n "sha-crypt") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "subtle") (r "^2") (o #t) (k 0)))) (h "1a73dr4d8ak33n9zlvy10k1g3zgcv0qxq4s78zrzywwyqlw3j5h6") (f (quote (("std") ("include_simple" "rand" "std" "subtle") ("default" "include_simple"))))))

(define-public crate-sha-crypt-0.2.0 (c (n "sha-crypt") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "subtle") (r "^2") (o #t) (k 0)))) (h "014s5sgqmycc2nfgd3v70f73rr549yj2prl33khqapirfkck3ks4") (f (quote (("std") ("simple" "rand" "std" "subtle") ("default" "simple"))))))

(define-public crate-sha-crypt-0.2.1 (c (n "sha-crypt") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "subtle") (r "=2.4") (o #t) (k 0)))) (h "12710whwndbk9rs6f21s8y9x98cq3jmqc7g581rwd1ifqi8vm38w") (f (quote (("std") ("simple" "rand" "std" "subtle") ("default" "simple"))))))

(define-public crate-sha-crypt-0.3.0 (c (n "sha-crypt") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "subtle") (r ">=2, <2.4") (o #t) (k 0)))) (h "1az8r5cdpd3w6qn8s5vd8ccz3grsm6wqfxfi8a5c64xpal682zrz") (f (quote (("std") ("simple" "rand" "std" "subtle") ("default" "simple"))))))

(define-public crate-sha-crypt-0.3.1 (c (n "sha-crypt") (v "0.3.1") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "subtle") (r ">=2, <2.5") (o #t) (k 0)))) (h "12lwjyyg1b463hvhdc0r083884ym6lnxbva70nzrnpxw94gnlzh6") (f (quote (("std") ("simple" "rand" "std" "subtle") ("default" "simple"))))))

(define-public crate-sha-crypt-0.3.2 (c (n "sha-crypt") (v "0.3.2") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 0)) (d (n "subtle") (r ">=2, <2.5") (o #t) (k 0)))) (h "00jc5mwar2csjh5hzgpqrd1mydzilfkgyia3lmq0zav0vlv8farq") (f (quote (("std") ("simple" "rand" "std" "subtle") ("default" "simple"))))))

(define-public crate-sha-crypt-0.4.0 (c (n "sha-crypt") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 0)) (d (n "subtle") (r ">=2, <2.5") (o #t) (k 0)))) (h "0wi097aw77awx8k9ib3a2jx5gnjybzyhq5md7lbsfam9x203w6gh") (f (quote (("std") ("simple" "rand" "std" "subtle") ("default" "simple")))) (r "1.56")))

(define-public crate-sha-crypt-0.5.0 (c (n "sha-crypt") (v "0.5.0") (d (list (d (n "base64ct") (r "^1.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 0)) (d (n "subtle") (r "^2") (o #t) (k 0)))) (h "11mjg1n4hl945m15xzany4w9wpwpk8qjykvm5pa130wdf84r1rw8") (f (quote (("std") ("simple" "rand" "std" "subtle") ("default" "simple")))) (r "1.60")))

(define-public crate-sha-crypt-0.6.0-pre.0 (c (n "sha-crypt") (v "0.6.0-pre.0") (d (list (d (n "base64ct") (r "^1.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sha2") (r "=0.11.0-pre.3") (k 0)) (d (n "subtle") (r "^2") (o #t) (k 0)))) (h "08krwn68s9kxkqmwf14mg54mdhvp6aa0l375w661z5vghp5xk4dr") (f (quote (("std") ("simple" "rand" "std" "subtle") ("default" "simple")))) (r "1.72")))

