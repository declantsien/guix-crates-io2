(define-module (crates-io sh a- sha-to-hex) #:use-module (crates-io))

(define-public crate-sha-to-hex-1.0.0 (c (n "sha-to-hex") (v "1.0.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)))) (h "1lavp15ldi9ik84i44qss8nab64f8k64b454rafnkyp7rdfr5kzg")))

(define-public crate-sha-to-hex-1.0.1 (c (n "sha-to-hex") (v "1.0.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)))) (h "1614y6501gaxr2f57k4j4y9k2ifd2wqwrb8v3js6b8ri73a86f0j")))

(define-public crate-sha-to-hex-1.0.2 (c (n "sha-to-hex") (v "1.0.2") (d (list (d (n "const-hex") (r "^1") (d #t) (k 0)) (d (n "ring") (r "^0.17") (d #t) (k 0)))) (h "1fzgaxnf2rbsk235yxwa5zdwcsyadb20mczimp12075kspp0my4s")))

(define-public crate-sha-to-hex-1.1.0 (c (n "sha-to-hex") (v "1.1.0") (d (list (d (n "const-hex") (r "^1") (d #t) (k 0)) (d (n "ring") (r "^0.17") (d #t) (k 0)))) (h "1i6qxcpi5l07b7p0gm7ijxvgq2ncnsrwk7aggf9w7k9n3l91m6d0")))

