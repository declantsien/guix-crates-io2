(define-module (crates-io sh ru shrust) #:use-module (crates-io))

(define-public crate-shrust-0.0.1 (c (n "shrust") (v "0.0.1") (d (list (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)))) (h "0qr8png9nfmp2rdqnzb4c4f4v1y23knx15ykxqpgvjrni7xa52cy")))

(define-public crate-shrust-0.0.2 (c (n "shrust") (v "0.0.2") (d (list (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)))) (h "0swnyl4sf4dvndsca4hlvgxz60db208gljj9ynivmm2idy7kiki0")))

(define-public crate-shrust-0.0.3 (c (n "shrust") (v "0.0.3") (d (list (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)))) (h "1wbmr891jdp6q7v72aqsnc4s8m3hv9m1x72ryvz3ddqhrgwrcz8v") (y #t)))

(define-public crate-shrust-0.0.4 (c (n "shrust") (v "0.0.4") (d (list (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)))) (h "1fd9vrn0i9wlrf6d32fr68c3j2vi9gfyhdckzd7a6n4ni4rfsiwf")))

(define-public crate-shrust-0.0.5 (c (n "shrust") (v "0.0.5") (d (list (d (n "prettytable-rs") (r "^0.7") (d #t) (k 0)))) (h "0bwl2km9089s546b2qkwg6bjc8zw0i6wn8mqcia9dbhrnkmb3ca5")))

(define-public crate-shrust-0.0.6 (c (n "shrust") (v "0.0.6") (d (list (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "0jsgpw8hv9vrljywnihc3biqp08ipdjchwp26a9bda77khbndhcy")))

(define-public crate-shrust-0.0.7 (c (n "shrust") (v "0.0.7") (d (list (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "1h8jgni71ffwk3ahp14hzxiwhlfn3k38by0h0s9adh5w21m86fc8")))

