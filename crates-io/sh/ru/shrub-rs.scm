(define-module (crates-io sh ru shrub-rs) #:use-module (crates-io))

(define-public crate-shrub-rs-0.1.0 (c (n "shrub-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1qlpswq03kqwip1xpw8cgrh963alvm5r61dmvmphzb97c3bghcic")))

(define-public crate-shrub-rs-0.1.1 (c (n "shrub-rs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dnhk14yr4q0g267cdznfs68vazid8bld1cwx5hx78k5ardnqqjl")))

(define-public crate-shrub-rs-0.1.2 (c (n "shrub-rs") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hfj3rxzvrz67ldsxgq5nmsiffqqm80pw355990pyhni9ih2sk82")))

(define-public crate-shrub-rs-0.2.0 (c (n "shrub-rs") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "yaml-merge-keys") (r "^0.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1a1yjg2qh7ix0n9b1vm98pv40rxikpjln65i59cy5vrgb7ddy2yx")))

(define-public crate-shrub-rs-0.3.0 (c (n "shrub-rs") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "yaml-merge-keys") (r "^0.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1fq7n97nmfajxq2196g34q9qwsmx5s4nw8jpx2q465fff5n5hb8s")))

(define-public crate-shrub-rs-0.3.1 (c (n "shrub-rs") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "yaml-merge-keys") (r "^0.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0z28vwqrcfivb875fjiigy0brb8idyxqi39kihnhql6z3alrsia9")))

(define-public crate-shrub-rs-0.3.2 (c (n "shrub-rs") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "yaml-merge-keys") (r "^0.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "11hk7h0ix50wjr7zl7w5snr0k8q2iphmr27q5jl5m7a3ka4478yx")))

(define-public crate-shrub-rs-0.3.3 (c (n "shrub-rs") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "yaml-merge-keys") (r "^0.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "00p9si93qb5ndszxkivpcd0qglskinvxrdhza8bdzmyw9q9qcprc")))

(define-public crate-shrub-rs-0.3.7 (c (n "shrub-rs") (v "0.3.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)) (d (n "yaml-merge-keys") (r "^0.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "04asjhii2z31bzjzrl9ddrl8ckbr23z0w8mpbrajm2mihr5mxk0q")))

(define-public crate-shrub-rs-0.4.1 (c (n "shrub-rs") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)) (d (n "yaml-merge-keys") (r "^0.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0b6fc983wadvdv4ch1gpha4p7993cmnqpa4q6cfkf2l5lapv076k")))

(define-public crate-shrub-rs-0.4.2 (c (n "shrub-rs") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)) (d (n "yaml-merge-keys") (r "^0.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0p66akvwzap47fi77ixj95zhyyi8y08a5wkqfpmbq7064qlw3rbg")))

(define-public crate-shrub-rs-0.5.0 (c (n "shrub-rs") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)) (d (n "yaml-merge-keys") (r "^0.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "068r616sd80fkhsr2hhskji6dg510vdj7k9lxji99379rlzn9778")))

(define-public crate-shrub-rs-0.5.1 (c (n "shrub-rs") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)) (d (n "yaml-merge-keys") (r "^0.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0d9n5mlm0hdhzzp7gmp2p2rik84x9d9msmawi5zxndyjsy4119k2")))

(define-public crate-shrub-rs-0.5.2 (c (n "shrub-rs") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)) (d (n "yaml-merge-keys") (r "^0.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0mw8z2qvgmb8b5gfi40nmybgpr1qfgzplcpx9yms9xpcl7x1v2jc")))

