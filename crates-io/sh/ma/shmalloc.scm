(define-module (crates-io sh ma shmalloc) #:use-module (crates-io))

(define-public crate-shmalloc-0.1.0 (c (n "shmalloc") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.9") (f (quote ("const_mut_refs"))) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "0mhs2svswjrwwygl31brbffxa3gsbfww2lnx0vryakrgiyczxgwp") (y #t)))

(define-public crate-shmalloc-0.1.1 (c (n "shmalloc") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.9") (f (quote ("const_mut_refs"))) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "0nrxpa1bz6dr61h3l4pam2ppp5zxvixdckxyl0rxn0xqq63h0bxf")))

(define-public crate-shmalloc-0.2.0 (c (n "shmalloc") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.9") (f (quote ("const_mut_refs"))) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "1zzw7lfja39ar2dd17fz4yhfd0rmsrbjy9p7qr33b2s81a2bm2lp") (y #t)))

(define-public crate-shmalloc-0.2.1 (c (n "shmalloc") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.9") (f (quote ("const_mut_refs"))) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "09yr6l54m2vrdbb1ws23s2a5iagr7by5vll47mhm8pc0dz0qjwk8")))

(define-public crate-shmalloc-0.2.2 (c (n "shmalloc") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.9") (f (quote ("const_mut_refs"))) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "06zfi9gifmkr7f3bwkl3wrh8i1yyihzsq8rnh1jzlsvp7ssx6nw7")))

