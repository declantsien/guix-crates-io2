(define-module (crates-io sh s_ shs_core) #:use-module (crates-io))

(define-public crate-shs_core-0.1.0 (c (n "shs_core") (v "0.1.0") (d (list (d (n "criterion") (r "~0.2.10") (d #t) (k 2)) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libsodium-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.0") (d #t) (k 0)))) (h "1p5afa0wc2nirgvfd1v3q55gnqk18f49jyswimwplk5g3iplx6ms")))

(define-public crate-shs_core-0.2.0 (c (n "shs_core") (v "0.2.0") (d (list (d (n "criterion") (r "~0.2.10") (d #t) (k 2)) (d (n "libsodium-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.0") (d #t) (k 0)))) (h "15z4ph3ygf20fdp9sncsw82mp733l116qx9i6nrfsjk12qhpyqhy")))

(define-public crate-shs_core-0.2.1 (c (n "shs_core") (v "0.2.1") (d (list (d (n "criterion") (r "~0.2.10") (d #t) (k 2)) (d (n "libsodium-sys") (r "^0.2") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "09f7rz9vpds3mqf4i2lwmspzlj3q1756kzk4lwafmzz65c0z9iiy")))

(define-public crate-shs_core-0.2.2 (c (n "shs_core") (v "0.2.2") (d (list (d (n "criterion") (r "~0.2.10") (d #t) (k 2)) (d (n "libsodium-sys") (r "^0.2") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "1qa41f7zwpygkh5hj0r9xrl1rhzg57803l8hygqkz6rjgrkkypd7")))

(define-public crate-shs_core-0.2.3 (c (n "shs_core") (v "0.2.3") (d (list (d (n "criterion") (r "~0.2.10") (d #t) (k 2)) (d (n "libsodium-sys") (r "^0.2") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "0cwkhdpjkzijhx1lddrknjhbdcqzri6f22vf5kjldbvlljivrp52")))

(define-public crate-shs_core-0.3.0 (c (n "shs_core") (v "0.3.0") (d (list (d (n "criterion") (r "~0.2.10") (d #t) (k 2)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "ssb-crypto") (r "^0.1") (d #t) (k 0)))) (h "01gjpd97zm5j8zfvkryz9x7xmnri6jyxrddc1hpr8pc9cgpxy4i4")))

