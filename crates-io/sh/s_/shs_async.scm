(define-module (crates-io sh s_ shs_async) #:use-module (crates-io))

(define-public crate-shs_async-0.1.0 (c (n "shs_async") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "pseudo-async-io") (r "^0.1") (d #t) (k 2)) (d (n "shs_core") (r "^0.2") (d #t) (k 0)))) (h "153rf20lalsybg12ich313h31alg3lv8xfkv943rhpvml4gy4mkd")))

(define-public crate-shs_async-0.2.0 (c (n "shs_async") (v "0.2.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "readwrite") (r "^0.1") (d #t) (k 2)) (d (n "shs_core") (r "^0.3.0") (d #t) (k 0)) (d (n "ssb-crypto") (r "^0.1.0") (d #t) (k 0)))) (h "1ik9lyxyn86j81pzqy873azx4pzvpyn0r8c9s1mv43a1i1kgplil")))

(define-public crate-shs_async-0.3.0 (c (n "shs_async") (v "0.3.0") (d (list (d (n "async-ringbuffer") (r "^0.5") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.14") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 2)) (d (n "readwrite") (r "^0.1") (d #t) (k 2)) (d (n "shs_core") (r "^0.3.0") (d #t) (k 0)) (d (n "ssb-crypto") (r "^0.1.2") (d #t) (k 0)))) (h "13wbqx91n18zd3wyk23dqcv699iv0argjjh1vxypg2w20bn94ssy")))

