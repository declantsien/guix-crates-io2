(define-module (crates-io sh at shattuck) #:use-module (crates-io))

(define-public crate-shattuck-0.1.0 (c (n "shattuck") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "070hiw6sjngq3hdj6zrjh40cg6mp9bzf70559yb1wx1vfk7lwx8d")))

