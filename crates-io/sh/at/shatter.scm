(define-module (crates-io sh at shatter) #:use-module (crates-io))

(define-public crate-shatter-0.1.0 (c (n "shatter") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1wn51vradx4fy9w6dyx1vz073b2mxsl038fj31jfcvnpzy9jyzdr")))

(define-public crate-shatter-0.1.1 (c (n "shatter") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0d0qwchvq2zp01s3gil8jnankpjaljfgp61p0zk11yvyq22aiq8k")))

(define-public crate-shatter-0.1.2 (c (n "shatter") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0xs119kz668kknn5fkzvqnyhzkjfhpfjglvg08x9ly05z0bd6wjc")))

