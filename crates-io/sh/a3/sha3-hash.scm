(define-module (crates-io sh a3 sha3-hash) #:use-module (crates-io))

(define-public crate-sha3-hash-0.1.0 (c (n "sha3-hash") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "0bf3pmcv3nx1zp998qsqnsr0v415awlyk0qp66qlvabl76m553a9") (y #t)))

(define-public crate-sha3-hash-0.1.1 (c (n "sha3-hash") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.126") (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("alloc"))) (k 2)) (d (n "sha3") (r "^0.9.1") (k 0)))) (h "1kmmyr51mz04cv9w2idfv4wr0hais2l1sn6mzsam7p2z9hrf59r6") (y #t)))

(define-public crate-sha3-hash-0.1.2 (c (n "sha3-hash") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.126") (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("alloc"))) (k 2)) (d (n "sha3") (r "^0.9.1") (k 0)))) (h "1l9v4achq2k6lyj60x58mgjj4wgkc3208ld23xkwpwsddiiqw0nc") (y #t)))

(define-public crate-sha3-hash-0.1.3 (c (n "sha3-hash") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.126") (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("alloc"))) (k 2)) (d (n "sha3") (r "^0.9.1") (k 0)))) (h "12r941b2ykggx8agzxw23qljxa62hrxs3bsgwyha6f0crap3cawj") (y #t)))

(define-public crate-sha3-hash-0.1.4 (c (n "sha3-hash") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.126") (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("alloc"))) (k 2)) (d (n "sha3") (r "^0.9.1") (k 0)))) (h "033wbgil6m4cc62ww7wk8hcchxhvbi1x4r04jzf75kyq5cg82ag8") (y #t)))

