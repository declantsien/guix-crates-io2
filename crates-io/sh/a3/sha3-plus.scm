(define-module (crates-io sh a3 sha3-plus) #:use-module (crates-io))

(define-public crate-sha3-plus-0.8.1 (c (n "sha3-plus") (v "0.8.1") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "byte-tools") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "05rkkdfiq5yb2w5r12mdyh8wfq6z4hk87bz18x7ms6dphfcjih20") (f (quote (("std" "digest/std") ("default" "std")))) (y #t)))

