(define-module (crates-io sh a3 sha3-const) #:use-module (crates-io))

(define-public crate-sha3-const-0.1.0 (c (n "sha3-const") (v "0.1.0") (h "13mz3wjx9cwhx3v6fgcxksixq3qw4xwv8j0jf7fdnx6ib3ajb5mg")))

(define-public crate-sha3-const-0.1.1 (c (n "sha3-const") (v "0.1.1") (h "0hy3ryvls57zdmnfcjws69qjjw8l3ckzi1z72bvshyb4hzg78ixh")))

