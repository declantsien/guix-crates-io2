(define-module (crates-io sh a3 sha3) #:use-module (crates-io))

(define-public crate-sha3-0.0.0 (c (n "sha3") (v "0.0.0") (h "0hqzz81kdgxrfjbybfr76w0fn66imqicwxlwzdkj2knhsdr9vfnv") (y #t)))

(define-public crate-sha3-0.1.0 (c (n "sha3") (v "0.1.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.1") (d #t) (k 2)) (d (n "digest") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "0d8311rfzmf4vfc10d5bpz6mi9sh3q5ff41bnp09k7gs7f7rlwbj")))

(define-public crate-sha3-0.2.0 (c (n "sha3") (v "0.2.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.1") (d #t) (k 2)) (d (n "digest") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "0c45kwsd4f05pyfq2ncxiip64q83c6k5c29h259jiq3f5zk2izv6")))

(define-public crate-sha3-0.3.0 (c (n "sha3") (v "0.3.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.2") (d #t) (k 2)) (d (n "digest") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "042fxskszr8ci1nq5plqmg4lag16x2704p0zbv1bl9n8y5zqm60z")))

(define-public crate-sha3-0.4.0 (c (n "sha3") (v "0.4.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "0lxzsmdm801kz4kjbz7mql6c6zn31rqyyh46yai0lfawzgiv530h")))

(define-public crate-sha3-0.4.1 (c (n "sha3") (v "0.4.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "0336587dj326373a7q6j2nlfv6gzrd7yz1alpbivq7zmln32grs7")))

(define-public crate-sha3-0.5.0 (c (n "sha3") (v "0.5.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4.0") (d #t) (k 2)) (d (n "digest") (r "^0.5.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "18wf8fshmihimqh0612w11di0zrg9zgcbjvm8sb0hllsrjphy0mn")))

(define-public crate-sha3-0.5.1 (c (n "sha3") (v "0.5.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "0la95gs037g5rrkiyn6wizry2lh0akaly3y82xkc8l38zbasw74f")))

(define-public crate-sha3-0.5.2 (c (n "sha3") (v "0.5.2") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (t "cfg(target_endian = \"big\")") (k 0)) (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "0q1cwg6ci6v524vk2xm5n9jq8z8dwh6nqrdszpj5dcn6fp4wxirx")))

(define-public crate-sha3-0.5.3 (c (n "sha3") (v "0.5.3") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "0620xazz09pry1s6s0pyc9y9n37aw9jgiaz2bdm8rz0ghy3v7zz9")))

(define-public crate-sha3-0.6.0 (c (n "sha3") (v "0.6.0") (d (list (d (n "block-buffer") (r "^0.2") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "186nimd5rv3xfvsk3ravlqaaq1q5c5idmkq907398sm5nq2mjh16")))

(define-public crate-sha3-0.7.0 (c (n "sha3") (v "0.7.0") (d (list (d (n "block-buffer") (r "^0.3.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 2)))) (h "13b1i06yqn214gf889h60dm0pv4hy53z9wvvdzqg78zxx99ihh8g") (y #t)))

(define-public crate-sha3-0.7.1 (c (n "sha3") (v "0.7.1") (d (list (d (n "block-buffer") (r "^0.3.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 2)))) (h "1i8mydx1mzixxc2ffchlljr7wjhal3xag6ncbagyabm875g2iv13")))

(define-public crate-sha3-0.7.2 (c (n "sha3") (v "0.7.2") (d (list (d (n "block-buffer") (r "^0.3.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 2)))) (h "0r0dw6ivz0rdpiln2j1p3q8jy4zv5ngrif7d06d24arfy780c8w7")))

(define-public crate-sha3-0.7.3 (c (n "sha3") (v "0.7.3") (d (list (d (n "block-buffer") (r "^0.3.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)))) (h "1g1hfm3r5wwcfbacv3lyagb90ijngzq3vdb2nnghnaflkvswwkdn")))

(define-public crate-sha3-0.8.0 (c (n "sha3") (v "0.8.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "byte-tools") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "16mwckx6zcjp16mv7mjjig65k2qbja2bhbslnbkyyr5kr89c2g8s") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-sha3-0.8.1 (c (n "sha3") (v "0.8.1") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "byte-tools") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0fh54kvg4f46w72bd6h2gqbm989hff7g7zary15r6ayfhd0fb99l") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-sha3-0.8.2 (c (n "sha3") (v "0.8.2") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "byte-tools") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "1kryhpjafv3k1x9fj13psrqhrvkjigswm564kfapqfifg87bq9nx") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-sha3-0.9.0 (c (n "sha3") (v "0.9.0") (d (list (d (n "block-buffer") (r "^0.8") (d #t) (k 0)) (d (n "byte-tools") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0s4914fv3lk9hkrfsg8b1q5gczimvrvksbs255mspd3v660cyndq") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-sha3-0.9.1 (c (n "sha3") (v "0.9.1") (d (list (d (n "block-buffer") (r "^0.9") (f (quote ("block-padding"))) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "02d85wpvz75a0n7r2da15ikqjwzamhii11qy9gqf6pafgm0rj4gq") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-sha3-0.10.0 (c (n "sha3") (v "0.10.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)))) (h "1ganwfqyj89j56bg5ig8980bg0b1a6jiaa3cx70fh1pl3kikby9i") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-sha3-0.10.1 (c (n "sha3") (v "0.10.1") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)))) (h "11hclx8ijnlx82dyd0bh9hi629zb3vqjfsyaqlgk1dl7dhazh6w8") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-sha3-0.10.2 (c (n "sha3") (v "0.10.2") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)))) (h "0b1ijjcv4sxhq77spdgx8h2x96yr4a0w1dv1b8whz6gccq1lhc8a") (f (quote (("std" "digest/std") ("reset") ("default" "std"))))))

(define-public crate-sha3-0.10.3 (c (n "sha3") (v "0.10.3") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)))) (h "1sdnj7ilrg2kdsg6h862xia364qd11xa4hkyr8v7h05igr8452ac") (f (quote (("std" "digest/std") ("reset") ("default" "std"))))))

(define-public crate-sha3-0.10.4 (c (n "sha3") (v "0.10.4") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)))) (h "0mhg8zq5p7im8kd4pkbvsq99a83s6pjp5fs1nz14gsl9s97g7vga") (f (quote (("std" "digest/std") ("reset") ("default" "std"))))))

(define-public crate-sha3-0.10.5 (c (n "sha3") (v "0.10.5") (d (list (d (n "digest") (r "^0.10.4") (d #t) (k 0)) (d (n "digest") (r "^0.10.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)))) (h "1abvvc2pgkai3r31zsis40ln15qxp33s28ik90mrdbm12vm4p472") (f (quote (("std" "digest/std") ("reset") ("oid" "digest/oid") ("default" "std"))))))

(define-public crate-sha3-0.10.6 (c (n "sha3") (v "0.10.6") (d (list (d (n "digest") (r "^0.10.4") (d #t) (k 0)) (d (n "digest") (r "^0.10.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "keccak") (r "^0.1") (d #t) (k 0)))) (h "1jcijr37gyail16p0g1vljjbkrsmbk0lpf4q0l4dqnwjmqzw7w5x") (f (quote (("std" "digest/std") ("reset") ("oid" "digest/oid") ("default" "std"))))))

(define-public crate-sha3-0.10.7 (c (n "sha3") (v "0.10.7") (d (list (d (n "digest") (r "^0.10.4") (d #t) (k 0)) (d (n "digest") (r "^0.10.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "keccak") (r "^0.1.3") (d #t) (k 0)))) (h "130r2nnm2vzx9yzm99dqwa6b1ssgy9rznsx32irm41rk68dbphjl") (f (quote (("std" "digest/std") ("reset") ("oid" "digest/oid") ("default" "std") ("asm" "keccak/asm"))))))

(define-public crate-sha3-0.10.8 (c (n "sha3") (v "0.10.8") (d (list (d (n "digest") (r "^0.10.4") (d #t) (k 0)) (d (n "digest") (r "^0.10.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "keccak") (r "^0.1.4") (d #t) (k 0)))) (h "0q5s3qlwnk8d5j34jya98j1v2p3009wdmnqdza3yydwgi8kjv1vm") (f (quote (("std" "digest/std") ("reset") ("oid" "digest/oid") ("default" "std") ("asm" "keccak/asm"))))))

(define-public crate-sha3-0.11.0-pre.0 (c (n "sha3") (v "0.11.0-pre.0") (d (list (d (n "digest") (r "=0.11.0-pre.3") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "keccak") (r "^0.1.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (o #t) (k 0)))) (h "1as1d97imw4pvwnlzv2p1xdrfaa9shzg2syha0pih5dji87sm05b") (f (quote (("std" "digest/std") ("reset") ("oid" "digest/oid") ("default" "oid" "std") ("asm" "keccak/asm")))) (r "1.71")))

(define-public crate-sha3-0.11.0-pre.1 (c (n "sha3") (v "0.11.0-pre.1") (d (list (d (n "digest") (r "=0.11.0-pre.4") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "keccak") (r "^0.1.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (o #t) (k 0)))) (h "08ikbflw4lkkslvs33hi7dq755qh8axhklmlgf2vvimzpf7dmx93") (f (quote (("std" "digest/std") ("reset") ("oid" "digest/oid") ("default" "oid" "std") ("asm" "keccak/asm")))) (r "1.71")))

(define-public crate-sha3-0.11.0-pre.2 (c (n "sha3") (v "0.11.0-pre.2") (d (list (d (n "digest") (r "=0.11.0-pre.7") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "keccak") (r "=0.2.0-pre.0") (d #t) (k 0)))) (h "01k8gnhhj5aynmi9ghf8zqwr76cc3md34ghv98qb6cqi6r7b9v4w") (f (quote (("zeroize" "digest/zeroize") ("std" "digest/std") ("reset") ("oid" "digest/oid") ("default" "oid" "std") ("asm" "keccak/asm")))) (r "1.71")))

(define-public crate-sha3-0.11.0-pre.3 (c (n "sha3") (v "0.11.0-pre.3") (d (list (d (n "digest") (r "=0.11.0-pre.8") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "keccak") (r "=0.2.0-pre.0") (d #t) (k 0)))) (h "1fc5fwjdydd0wf79s385hd14wnc6vy43wk7id8yplr3sk2wh4b7k") (f (quote (("zeroize" "digest/zeroize") ("std" "digest/std") ("reset") ("oid" "digest/oid") ("default" "oid" "std") ("asm" "keccak/asm")))) (r "1.71")))

