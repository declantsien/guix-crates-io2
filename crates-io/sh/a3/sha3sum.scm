(define-module (crates-io sh a3 sha3sum) #:use-module (crates-io))

(define-public crate-sha3sum-0.1.0 (c (n "sha3sum") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "191p93svj7xwxb5nnb6a1x7kk8xfpj3ws38jd7747icxx2gk1zn7") (y #t)))

(define-public crate-sha3sum-0.1.1 (c (n "sha3sum") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "11kacwfxrd1cch0mv5sxbz1v9x5c1k73nzz3c2z4kl7zv2idza2y") (y #t)))

(define-public crate-sha3sum-0.1.2 (c (n "sha3sum") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "0yszbxqyiq2brqpxgckhsc0h884s66hff7x6181xwjnxvgl0xmsh") (y #t)))

(define-public crate-sha3sum-0.1.3 (c (n "sha3sum") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "0nngbyb1bn84n05v9kid2a413nh12bxpxbbf0v51iwl41wa7lh93") (y #t)))

(define-public crate-sha3sum-0.1.4 (c (n "sha3sum") (v "0.1.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "0jncw53mj0ypab41lfpsdi5l1d8fa8bwq6zg1p5j0ajcmnicddqf") (y #t)))

(define-public crate-sha3sum-0.1.5 (c (n "sha3sum") (v "0.1.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "0xdjs15n785afwfrbvp3bh7qxzxci0sr40qiff8j6pb35lzqiayv")))

(define-public crate-sha3sum-1.0.0 (c (n "sha3sum") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "07s3i2pjx6p8dhalaga33h5lsjnwkyhv4qwx2b61hpqs0qdnh8zh")))

(define-public crate-sha3sum-1.0.1 (c (n "sha3sum") (v "1.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "117rbkzd0gfr429bwl1m90hymvl3s272zrkzyhh70fk6vk4scm7l")))

(define-public crate-sha3sum-1.1.0 (c (n "sha3sum") (v "1.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "07qi0hjrr5jv8nfdqhjhc4ryp09c0szc8li4yz5cy3lqzcacpw2m")))

(define-public crate-sha3sum-1.2.0 (c (n "sha3sum") (v "1.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0aqclyl7mndl5z2ncvac5p7vady9d8m0pr76a9ayn5ph8ib4zp8y")))

(define-public crate-sha3sum-1.2.1 (c (n "sha3sum") (v "1.2.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0ra90jv1532v11hjzkcnpwpbg51s06pylra40indca1nagyan0i8")))

(define-public crate-sha3sum-1.2.2 (c (n "sha3sum") (v "1.2.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0isakphg5mhmwhiaynl0mbv4ijmmg9c53byx1v7da3glyaldvhfh")))

