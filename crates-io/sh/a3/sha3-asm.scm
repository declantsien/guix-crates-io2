(define-module (crates-io sh a3 sha3-asm) #:use-module (crates-io))

(define-public crate-sha3-asm-0.1.0 (c (n "sha3-asm") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0pm1qwxra3aqw4w7f7fqm0hl6ws7z4814xxlbqcnpmssnfk1vims") (r "1.64")))

(define-public crate-sha3-asm-0.1.1 (c (n "sha3-asm") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0h1vh973lvw7r28n0wp2kn4g9a0bz6ayj68rpc4gyfr5c7c7zdd9") (r "1.64")))

