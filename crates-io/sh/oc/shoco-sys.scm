(define-module (crates-io sh oc shoco-sys) #:use-module (crates-io))

(define-public crate-shoco-sys-0.1.0 (c (n "shoco-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)))) (h "1isw7djm6ah9hxgx4nddyaxcasrr39jfm4k1p850xmiqbiahnfg7")))

(define-public crate-shoco-sys-1.0.0 (c (n "shoco-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)))) (h "1fwrrs2j78ksl0l673j4pnb87a2j2h7h8qjzibkqgwm89lgfnvm7")))

