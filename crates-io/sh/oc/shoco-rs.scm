(define-module (crates-io sh oc shoco-rs) #:use-module (crates-io))

(define-public crate-shoco-rs-0.1.0 (c (n "shoco-rs") (v "0.1.0") (d (list (d (n "shoco-sys") (r "^0.1.0") (d #t) (k 0)))) (h "12qinajii2c2zdf2pkrlw0wrimg6wn3zyam2jas3dzn7fx5pdsls")))

(define-public crate-shoco-rs-0.1.1 (c (n "shoco-rs") (v "0.1.1") (d (list (d (n "shoco-sys") (r "^1") (d #t) (k 0)))) (h "0bdcs76sjymnf0rp8q094ys7n7lh9256z4iy4liavjjlfmx8wb2f")))

