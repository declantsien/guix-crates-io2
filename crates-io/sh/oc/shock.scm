(define-module (crates-io sh oc shock) #:use-module (crates-io))

(define-public crate-shock-0.1.0 (c (n "shock") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std"))) (k 0)) (d (n "bstr") (r "^1.6.2") (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.7.7") (f (quote ("parse"))) (k 0)))) (h "0iyjricqrjps4khjl9cgqcracvsyinh4vlq5yqv0655c1bpl2wyj")))

(define-public crate-shock-0.1.1 (c (n "shock") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std"))) (k 0)) (d (n "bstr") (r "^1.6.2") (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8.0") (f (quote ("parse"))) (k 0)))) (h "0w7nfkzbn90ppdr837yfrnp33kyc2a4qq77lx4x8vbswykipsxv6")))

(define-public crate-shock-0.1.2 (c (n "shock") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std"))) (k 0)) (d (n "bstr") (r "^1.6.2") (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8.1") (f (quote ("parse"))) (k 0)))) (h "0nkvvkw6rzgh7jf1938g0wbsnipm057f2213wkwjq82dav30c75i")))

(define-public crate-shock-0.1.3 (c (n "shock") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std"))) (k 0)) (d (n "bstr") (r "^1.6.2") (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8.1") (f (quote ("parse"))) (k 0)))) (h "03j4vqj6pm7ay8qil3m1dr6mw646q35q91akzw84z8ncy3gck5s7")))

(define-public crate-shock-0.1.4 (c (n "shock") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std"))) (k 0)) (d (n "bstr") (r "^1.7.0") (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8.6") (f (quote ("parse"))) (k 0)))) (h "0lwz2icbpd3qnjzypmkax81d8jczsywmmk6nmgv780r58ppvpj1j")))

(define-public crate-shock-0.1.5 (c (n "shock") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std"))) (k 0)) (d (n "bstr") (r "^1.7.0") (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8.6") (f (quote ("parse"))) (k 0)))) (h "11kq2s77iq10f724yxa43is6chl2f6lzpdaz98m3b77rz1q3ap2p")))

(define-public crate-shock-0.1.6 (c (n "shock") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std"))) (k 0)) (d (n "bstr") (r "^1.8.0") (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8.8") (f (quote ("parse"))) (k 0)))) (h "1ymhv8hkkgzc719m53l9kchy4acbdddjn9fjmndi3w44zj1vqd3b")))

(define-public crate-shock-0.1.7 (c (n "shock") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.78") (f (quote ("std"))) (k 0)) (d (n "bstr") (r "^1.9.0") (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8.8") (f (quote ("parse"))) (k 0)))) (h "0g80ij9l00lnwhibhk00cx08psrs2kd07s41m2a69hyndv4ig6l8")))

(define-public crate-shock-0.1.8 (c (n "shock") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.79") (f (quote ("std"))) (k 0)) (d (n "bstr") (r "^1.9.0") (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8.9") (f (quote ("parse"))) (k 0)))) (h "08i8lbw67xa533m07h1kvm1yfj7ifih5jdy1dvimh6rf5q75my90")))

(define-public crate-shock-0.1.9 (c (n "shock") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.80") (f (quote ("std"))) (k 0)) (d (n "bstr") (r "^1.9.1") (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8.10") (f (quote ("parse"))) (k 0)))) (h "168rgvizpjjh1l7pm71qg4b5rdc89xaf7c5ynxdjqg3543gd2cdx")))

(define-public crate-shock-0.1.10 (c (n "shock") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.81") (f (quote ("std"))) (k 0)) (d (n "bstr") (r "^1.9.1") (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8.12") (f (quote ("parse"))) (k 0)))) (h "133bsf39964ggrf9iw8mm2qsy1f4kiwyh2c4b9m3v5a83l55hh8r")))

(define-public crate-shock-0.1.11 (c (n "shock") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.82") (f (quote ("std"))) (k 0)) (d (n "bstr") (r "^1.9.1") (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8.12") (f (quote ("parse"))) (k 0)))) (h "145sr10drkgnfcfqd2y5llcc7d25dgg0zj97r81r0nnv54wb3f5h")))

