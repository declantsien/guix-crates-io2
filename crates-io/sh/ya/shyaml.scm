(define-module (crates-io sh ya shyaml) #:use-module (crates-io))

(define-public crate-shyaml-0.3.0 (c (n "shyaml") (v "0.3.0") (d (list (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.8") (d #t) (k 0)) (d (n "tempdir") (r "~0") (d #t) (k 0)))) (h "0bh5x45bbwgyyyvggsr780rwpiawjzh0v9j0pz2j6l1gkp7bbcqj")))

(define-public crate-shyaml-0.4.0 (c (n "shyaml") (v "0.4.0") (d (list (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.8") (d #t) (k 0)) (d (n "tempdir") (r "~0") (d #t) (k 0)))) (h "11i68r5i0rg47rb94rzy7wcy1apbkddkgsd6977kfgfc0i6v2444")))

(define-public crate-shyaml-0.4.1 (c (n "shyaml") (v "0.4.1") (d (list (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.8") (d #t) (k 0)) (d (n "tempdir") (r "~0") (d #t) (k 0)))) (h "1l8d94wz9skqk41h099b4s62479pp381gq8pa9gkrrm3ai1wy375")))

