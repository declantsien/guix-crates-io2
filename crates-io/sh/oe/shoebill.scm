(define-module (crates-io sh oe shoebill) #:use-module (crates-io))

(define-public crate-shoebill-0.1.1 (c (n "shoebill") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1n3vkgqc06kw7wd0fgpf8rig882j4f8l3b7hh1cxqy7w61k2qgxq") (y #t)))

(define-public crate-shoebill-0.1.3 (c (n "shoebill") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0i2iwxp7vid62hh93gh5f7nlwgsag9mmkvvsnngh3zk4zsx154fx") (y #t)))

(define-public crate-shoebill-0.1.4 (c (n "shoebill") (v "0.1.4") (d (list (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "00s11h73s79xfl1yl45rhs4zj3xb318ahl3jjrm4v6h2bkdihkg0")))

(define-public crate-shoebill-0.1.5 (c (n "shoebill") (v "0.1.5") (d (list (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "10ysf6mgx7hm4cccwi87q90pjs21000dv1vqh4zv1nnzb6fxxwya")))

