(define-module (crates-io sh oe shoelace) #:use-module (crates-io))

(define-public crate-shoelace-0.1.0 (c (n "shoelace") (v "0.1.0") (h "1rgn9fn2i25n84g7fk6ld915w72fglhij8ii9p59g293f1dm2qk7")))

(define-public crate-shoelace-0.1.1 (c (n "shoelace") (v "0.1.1") (h "1jz1p2v32x9jibib7b7v9gdf3k0dxf62g2bkci06596i8xpnwr3j")))

(define-public crate-shoelace-0.1.2 (c (n "shoelace") (v "0.1.2") (h "0mvy7vpfx2d70kcay1ps7ih1vqmk0madavcq52sy1bzbmyd8llsh")))

(define-public crate-shoelace-0.1.3 (c (n "shoelace") (v "0.1.3") (h "0ch3i9mii19jvlbchhb4avf0llgdcgiqcjw2n8bb3rfgcdpcwaxh")))

