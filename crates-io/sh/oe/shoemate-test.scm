(define-module (crates-io sh oe shoemate-test) #:use-module (crates-io))

(define-public crate-shoemate-test-0.1.0 (c (n "shoemate-test") (v "0.1.0") (h "1i08fijxwh0yfv3s5hvn8grrizxvfs7gwpz240crxlp1vv56bmak") (r "1.64.0")))

(define-public crate-shoemate-test-0.1.1 (c (n "shoemate-test") (v "0.1.1") (h "1czb3yrqrzs6z23qr3kc7s68vv8dc4010bgalj8k1vb5r114z4l9") (r "1.64.0")))

(define-public crate-shoemate-test-0.1.2 (c (n "shoemate-test") (v "0.1.2") (h "1a39529ila7lv4jwmqsjyhq4bir66y3v571xzxbd60b2c6w5dvcc") (r "1.64.0")))

(define-public crate-shoemate-test-0.1.3 (c (n "shoemate-test") (v "0.1.3") (h "0didaaiiqvfcs569czlv64lrpai3cxc0drsy9vxy3bf876r34mxi") (r "1.64.0")))

(define-public crate-shoemate-test-0.1.4 (c (n "shoemate-test") (v "0.1.4") (h "0pfwh2jggiwb36rdww4dkp9m04aj8gxf3yiyk8pi3dp53sl8dp4m") (r "1.64.0")))

(define-public crate-shoemate-test-0.1.5 (c (n "shoemate-test") (v "0.1.5") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "19f7gix1v7hlh2zy4s7rpgpr0aa47dhzj6la8ci3m7c55r5fzz88") (r "1.64.0")))

(define-public crate-shoemate-test-0.1.6 (c (n "shoemate-test") (v "0.1.6") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "opendp") (r "^0.6.0") (d #t) (k 0)))) (h "14f7gf8zb7jd1wp0gdkp84p4qr27gmd8jh2r5b31n23fkcmjsygj") (r "1.64.0")))

(define-public crate-shoemate-test-0.1.7 (c (n "shoemate-test") (v "0.1.7") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "opendp") (r "^0.6.0") (d #t) (k 0)))) (h "1yffj4a05rrgswv1a35xnwql4v35yz467dnryxcsbxl9cgyvy95m") (r "1.64.0")))

