(define-module (crates-io sh uc shuc) #:use-module (crates-io))

(define-public crate-shuc-0.1.0 (c (n "shuc") (v "0.1.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0x3kiw0l6y2jdwl001qhr8crk0rbaj8w7qhr8ynm9xfmg59yjqc7")))

(define-public crate-shuc-0.1.1 (c (n "shuc") (v "0.1.1") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1fmaqfkw2l0513sx5dxrspkwsnd1d60270nsgclnigqvrlm4id2q") (y #t)))

(define-public crate-shuc-0.1.2 (c (n "shuc") (v "0.1.2") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "066xdcgdpga654r5wlss266wz0dp6lpw7vq77h6rwnx186gcniy4")))

