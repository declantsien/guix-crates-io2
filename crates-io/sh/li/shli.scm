(define-module (crates-io sh li shli) #:use-module (crates-io))

(define-public crate-shli-0.0.1 (c (n "shli") (v "0.0.1") (d (list (d (n "termion") (r "^1") (d #t) (k 0)))) (h "113m1ayxgs6b24zdbf48r49i6sbl2xvmqccjhnis8y4bd04zd8hw")))

(define-public crate-shli-0.1.0 (c (n "shli") (v "0.1.0") (d (list (d (n "termion") (r "^1") (d #t) (k 0)))) (h "08q2m2qk0apqqziy6vp4pgnqzlj9lqwncxlm64vf1pwylr93630r")))

(define-public crate-shli-0.1.1 (c (n "shli") (v "0.1.1") (d (list (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0j57i74b6svfqbfjinnbz0iqi8zchpm66vkjcm92s9arpn0b3f1l")))

(define-public crate-shli-0.2.0 (c (n "shli") (v "0.2.0") (d (list (d (n "termion") (r "^1") (d #t) (k 0)))) (h "09mg0jxkgdav2s97783k5dwkga3qs2m8fd7zwn7id86hzvxazq1j")))

(define-public crate-shli-0.3.0 (c (n "shli") (v "0.3.0") (d (list (d (n "termion") (r "^1") (d #t) (k 0)))) (h "1yri864kfadrqp516v798apgw1dyxdkknkgivraq3bhsmfbizh0f")))

(define-public crate-shli-0.4.0 (c (n "shli") (v "0.4.0") (d (list (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0ichw6ais5wvclxhcgf7bdf1ri0f7y6fibsgmhjmc6jysvcwcq9v")))

(define-public crate-shli-0.4.1 (c (n "shli") (v "0.4.1") (d (list (d (n "termion") (r "^1") (d #t) (k 0)))) (h "1hm30wjvx60vgv3aslxqqhdz1bhdamcg7w0gmwh5r91fvfrn7cyi")))

