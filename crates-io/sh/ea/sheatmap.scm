(define-module (crates-io sh ea sheatmap) #:use-module (crates-io))

(define-public crate-sheatmap-0.1.0 (c (n "sheatmap") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rstar") (r "^0.7.1") (d #t) (k 0)))) (h "1pswrgl81pz2j4p3kbvs6705p1ky6q03ffhb8zhnij8fjv1sykhd")))

