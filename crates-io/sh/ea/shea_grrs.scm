(define-module (crates-io sh ea shea_grrs) #:use-module (crates-io))

(define-public crate-shea_grrs-0.1.0 (c (n "shea_grrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1vj1nrp7xqbligq5fc3x2fb57q5zp9x63i9za31h8clqnnl93q90")))

