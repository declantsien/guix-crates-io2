(define-module (crates-io sh m_ shm_ring) #:use-module (crates-io))

(define-public crate-shm_ring-0.1.0 (c (n "shm_ring") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.141") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "0gch1m50xabg4hwa5nw53q2rsxifi52danjm5zry175fh87nwvka")))

(define-public crate-shm_ring-0.2.0 (c (n "shm_ring") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.141") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "0ybp6wq09746gx0yf5584z0ax8b5s3wm0digws9l8bmx65b5rlhb") (f (quote (("avx2"))))))

(define-public crate-shm_ring-0.2.1 (c (n "shm_ring") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.141") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "1jn9ymi0hybsf002kls5b93xwymcb83611c25qiq3srj5swnywsw") (f (quote (("avx2"))))))

(define-public crate-shm_ring-0.2.2 (c (n "shm_ring") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.141") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "020dibm5953322yr9cx9pfw111kplv5nz8m9in1b2mmd1vg1siip") (f (quote (("avx2"))))))

(define-public crate-shm_ring-0.2.3 (c (n "shm_ring") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.141") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "0cjapaxqwhs3hfrzxhpn9mfrnskgbhlkpzpz7jrfapy4izlzr44p") (f (quote (("avx2"))))))

