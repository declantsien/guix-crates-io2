(define-module (crates-io sh m_ shm_open_anonymous) #:use-module (crates-io))

(define-public crate-shm_open_anonymous-1.0.0 (c (n "shm_open_anonymous") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0scyccpcka5mqjwlxrainmwzilgws6b60dm45vfv115acdx3j507")))

(define-public crate-shm_open_anonymous-1.0.1 (c (n "shm_open_anonymous") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0h1069yxffsck6kn81qkcrc72lj7psvr3dznibkm9vjqhn3f5y80")))

