(define-module (crates-io sh ro shroom) #:use-module (crates-io))

(define-public crate-shroom-0.0.1 (c (n "shroom") (v "0.0.1") (d (list (d (n "lalrpop") (r "^0.16.3") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16.3") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1hps12y380ib4s55abyf0whl39fqkd9p7475m146ay36ri1alb8z")))

