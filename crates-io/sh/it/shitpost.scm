(define-module (crates-io sh it shitpost) #:use-module (crates-io))

(define-public crate-shitpost-1.0.0 (c (n "shitpost") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "shitpost_markov") (r "^1.0.0") (d #t) (k 1)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 1)))) (h "1sz0yk5h5ill1j0gjbm5fhv4p9f36rfs43izshxvg0z2n1a9kcm9")))

(define-public crate-shitpost-1.0.1 (c (n "shitpost") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "shitpost_markov") (r "^1.0.1") (d #t) (k 1)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 1)))) (h "0llw9gri6l6gw40jmp8piaxh0mmc0ffx07byd295p58nqyyy0yzq")))

(define-public crate-shitpost-1.0.2 (c (n "shitpost") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "shitpost_markov") (r "^1.0.1") (d #t) (k 1)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 1)))) (h "0wxf685gv5vv8ak8nff6033567c5z418098gn4qwkj6rfk2d5ghx")))

