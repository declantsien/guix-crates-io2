(define-module (crates-io sh it shitpic) #:use-module (crates-io))

(define-public crate-shitpic-0.1.0 (c (n "shitpic") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)))) (h "0ikwny9xw5pvlnyqmm9zdjm8kk2843436gz6kacbp64w5g041034")))

(define-public crate-shitpic-0.2.0 (c (n "shitpic") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clio") (r "^0.3.5") (f (quote ("clap-parse" "http-ureq"))) (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)))) (h "1h5cjxdiayh4mdl2giv28728xnbhfwa2sbw6zwfjl0h3fiilb358")))

