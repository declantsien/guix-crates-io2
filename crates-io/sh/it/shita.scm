(define-module (crates-io sh it shita) #:use-module (crates-io))

(define-public crate-shita-0.1.0 (c (n "shita") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "11c7511cx3546vwz2c54bff9qphdgz1i7ggb34bi6hrqmglpvkp8")))

(define-public crate-shita-0.1.1 (c (n "shita") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "0q5b39bziqz0lrlrzwiw6hma7dbv6wxay3j2gx6mgh0xld1m3ynx")))

