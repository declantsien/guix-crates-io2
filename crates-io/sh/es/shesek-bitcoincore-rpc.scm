(define-module (crates-io sh es shesek-bitcoincore-rpc) #:use-module (crates-io))

(define-public crate-shesek-bitcoincore-rpc-0.10.0 (c (n "shesek-bitcoincore-rpc") (v "0.10.0") (d (list (d (n "bitcoincore-rpc-json") (r "^0.10.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "14llvngwqg63jz7sabjv4c7gs7c7cbcg45942zvv0p6kl93wgvyh")))

(define-public crate-shesek-bitcoincore-rpc-0.10.1 (c (n "shesek-bitcoincore-rpc") (v "0.10.1") (d (list (d (n "jsonrpc") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shesek-bitcoincore-rpc-json") (r "^0.10.0") (d #t) (k 0)))) (h "0mvk8fi4ppifd6n5a0i7c5iyhr7iq2nfyx63418im032magyyx2a")))

(define-public crate-shesek-bitcoincore-rpc-0.11.0 (c (n "shesek-bitcoincore-rpc") (v "0.11.0") (d (list (d (n "jsonrpc") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shesek-bitcoincore-rpc-json") (r "^0.11.0") (d #t) (k 0)))) (h "0zyrzml3vh0llhvpqz5pkm78k9dq7hyd4i1j5hvf012hi86yhlw9")))

