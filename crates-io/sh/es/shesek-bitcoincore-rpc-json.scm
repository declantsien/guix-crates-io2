(define-module (crates-io sh es shesek-bitcoincore-rpc-json) #:use-module (crates-io))

(define-public crate-shesek-bitcoincore-rpc-json-0.10.0 (c (n "shesek-bitcoincore-rpc-json") (v "0.10.0") (d (list (d (n "bitcoin") (r "^0.23") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1cpy8pgvz08jjh4lkxjr6gx5jykgfiji3bg6dh0nvrv1dgnnlv29")))

(define-public crate-shesek-bitcoincore-rpc-json-0.11.0 (c (n "shesek-bitcoincore-rpc-json") (v "0.11.0") (d (list (d (n "bitcoin") (r "^0.23") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1d9md9zz69zpy1ap1g8sylxb4ac7h8qgx3dpgjswsfqrh9p325s6")))

