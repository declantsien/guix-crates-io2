(define-module (crates-io sh es shess) #:use-module (crates-io))

(define-public crate-shess-0.1.0 (c (n "shess") (v "0.1.0") (h "1yxggqbfv3ppf31vkqichi3ya1cskn4yvllf6sii8i6my91dlrql")))

(define-public crate-shess-0.1.1 (c (n "shess") (v "0.1.1") (d (list (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "glucose") (r "^0.2.1") (d #t) (k 0)))) (h "0ijhyl1vjla73ilp9kpk92q58j7d8fqpyypmd8r2gw1n6r7ji02f") (f (quote (("terminal") ("full" "terminal" "discord") ("discord") ("default"))))))

(define-public crate-shess-0.1.2 (c (n "shess") (v "0.1.2") (d (list (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "glucose") (r "^0.2.1") (d #t) (k 0)))) (h "03cqplxh4wc6lbm19dghfcjr3hcdwzkc8bvpcg9ql68kalqnv7n1") (f (quote (("terminal") ("full" "terminal" "discord") ("discord") ("default"))))))

(define-public crate-shess-0.1.3 (c (n "shess") (v "0.1.3") (d (list (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "glucose") (r "^0.2.1") (d #t) (k 0)))) (h "1axz3dq0gbsl3g7j8c5c1kw1wqxqxxqf2s5cp806qwirwcqp8wyl") (f (quote (("terminal") ("full" "terminal" "discord") ("discord") ("default"))))))

(define-public crate-shess-0.1.4 (c (n "shess") (v "0.1.4") (d (list (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "glucose") (r "^0.2.1") (d #t) (k 0)))) (h "0qsv1x2wvaxcz0v8vsdw7lrqyk9qc3ghgsgcd5h3qq17vz7y2fyv") (f (quote (("terminal") ("full" "terminal" "discord") ("discord") ("default"))))))

(define-public crate-shess-0.1.5 (c (n "shess") (v "0.1.5") (d (list (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "glucose") (r "^0.2.1") (d #t) (k 0)))) (h "0w398h6c8a5qaci2vcn2208cklhz7r5wimz88rhfniyfb5bam65r") (f (quote (("terminal") ("full" "terminal" "discord") ("discord") ("default"))))))

(define-public crate-shess-0.1.6 (c (n "shess") (v "0.1.6") (d (list (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "glucose") (r "^0.2.1") (d #t) (k 0)))) (h "0hkqzrxjvlk4jixj2k5wj0ayfsn4a5lbz1kcmnxgygcr4j90j5v4") (f (quote (("terminal") ("full" "terminal" "discord") ("discord") ("default"))))))

(define-public crate-shess-0.1.7 (c (n "shess") (v "0.1.7") (d (list (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "glucose") (r "^0.2.2") (d #t) (k 0)))) (h "0rskcn477dpjfqm785p2hk2bcr58fi7hdl08fjsxc2lxacdb3li9") (f (quote (("terminal") ("full" "terminal" "discord") ("discord") ("default"))))))

(define-public crate-shess-0.1.8 (c (n "shess") (v "0.1.8") (d (list (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "glucose") (r "^0.2.2") (d #t) (k 0)))) (h "0ddlhn6gl0g5c7lh10milbxb9n9cs9yzx3w491vf33hvbl8wyl8v") (f (quote (("terminal") ("full" "terminal" "discord") ("discord") ("default"))))))

(define-public crate-shess-0.1.9 (c (n "shess") (v "0.1.9") (d (list (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "glucose") (r "^0.2.2") (d #t) (k 0)))) (h "03gyqls8dpw6a3lnczjwspir4qfm0mcgzxf8azg2jfrj5fz4v462") (f (quote (("terminal") ("full" "terminal" "discord") ("discord") ("default"))))))

(define-public crate-shess-0.1.10 (c (n "shess") (v "0.1.10") (d (list (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "glucose") (r "^0.2.2") (d #t) (k 0)))) (h "0aifmwdfgmw6v6blsvv07izgdgb7aj63f6614x0pr4wgfpdnkldy") (f (quote (("terminal") ("full" "terminal" "discord") ("discord") ("default"))))))

(define-public crate-shess-0.1.11 (c (n "shess") (v "0.1.11") (d (list (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "glucose") (r "^0.2.2") (d #t) (k 0)))) (h "07d0hwj9d2k5ipj3xp9alr0nwfvks3inhj5vrxfpb8spj7vy1jfh") (f (quote (("terminal") ("full" "terminal" "discord") ("discord") ("default"))))))

(define-public crate-shess-0.1.12 (c (n "shess") (v "0.1.12") (d (list (d (n "fructose") (r "^0.3.3") (d #t) (k 0)) (d (n "glucose") (r "^0.2.2") (d #t) (k 0)))) (h "13gh5k11yw1rh3vlfrxvqz96jnl6zw8h61nd58w7hj58ciipm2vf") (f (quote (("terminal") ("full" "terminal" "discord") ("discord") ("default"))))))

