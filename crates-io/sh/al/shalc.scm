(define-module (crates-io sh al shalc) #:use-module (crates-io))

(define-public crate-shalc-0.1.0 (c (n "shalc") (v "0.1.0") (d (list (d (n "slog") (r "^2.5.2") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "slog-stdlog") (r "^4.0.0") (d #t) (k 0)))) (h "19dx8v5mqdf7xw816rjg1z4i0adz185a1p6cb3131lsc1cckhjry")))

