(define-module (crates-io sh al shalrath) #:use-module (crates-io))

(define-public crate-shalrath-0.1.0 (c (n "shalrath") (v "0.1.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0h9yry99fj8cv9h7a6s54b2c0dnvf26hszmc4mpcfc8x20arqsq7") (f (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.1.1 (c (n "shalrath") (v "0.1.1") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "054krxk1vxpxiidk5z2dy1zpnhla9s93z0xg5x52xmy3d2b91161") (f (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.1.2 (c (n "shalrath") (v "0.1.2") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wsv3ag87ykqlqy4vzn2cahs2y8yx4619z4rgff7f54rpcvxkg92") (f (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.1.3 (c (n "shalrath") (v "0.1.3") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05xaam8lwzyvlrz681awk6vaxinwkcm9i3iv97n0r9f0bg1mdsm2") (f (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.1.4 (c (n "shalrath") (v "0.1.4") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m1qivdva05nyp65qm7m11jqxbap34bv73c22iiprdmy9pw77z9h") (f (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.2.0 (c (n "shalrath") (v "0.2.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12w2g98g2hsh47ymi8536r57vjabrjni5brhvm3b7k10wyap2bpr") (f (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.2.1 (c (n "shalrath") (v "0.2.1") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00dyw4swlf0pbx4rgk9mnhkspdjrpqg5nplbr7l430klyircr1yq") (f (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.2.3 (c (n "shalrath") (v "0.2.3") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0h9sw01ag9jndhvwq3r5x35d0b3bv8rnwyj9yfc78ahsg8qf38bz") (f (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.2.4 (c (n "shalrath") (v "0.2.4") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xfafhvg7xx0qh0f29srq7zaxvfvmlidzwsy8jshsxh65jhww3qs") (f (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.2.5 (c (n "shalrath") (v "0.2.5") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "145r7b9gr2ps21h06biwqfn2fx7gb4pdwgkxra19rxzgcw3bgvgn") (f (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.2.6 (c (n "shalrath") (v "0.2.6") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rwldl9yz8xgcbjr69p4lffksg4qzbq665npbpwz0lp3vff6syin") (f (quote (("non_foss_tests") ("default" "serde"))))))

