(define-module (crates-io sh al shallow-debug) #:use-module (crates-io))

(define-public crate-shallow-debug-0.1.0 (c (n "shallow-debug") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)))) (h "1hqqhx2hvk6yz4ipa60dd8s9fc8gh83hxz18pzsc2mmb8k23cpmc") (r "1.60.0")))

