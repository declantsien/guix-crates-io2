(define-module (crates-io sh al shale) #:use-module (crates-io))

(define-public crate-shale-0.1.0 (c (n "shale") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1id61528fnk6rgzn4a527rmarlqk4k90qp0qd3282am3h4mrjnad")))

(define-public crate-shale-0.1.1 (c (n "shale") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lru") (r "^0.8.1") (d #t) (k 0)))) (h "01kad73637zbs29j93cn99fvahalpd3fy4jgydgm7s4dlnj90cb4")))

(define-public crate-shale-0.1.2 (c (n "shale") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lru") (r "^0.8.1") (d #t) (k 0)))) (h "1lx4w6gkr08lr1bi9bpf5azp5p7pp708ys3jlf38k9f0mb1p7d1g")))

(define-public crate-shale-0.1.3 (c (n "shale") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lru") (r "^0.8.1") (d #t) (k 0)))) (h "084kks4gi424w8zbk7z1g7h4gr0jjbhj5w28pxlr8d2ci2qvis8l")))

(define-public crate-shale-0.1.4 (c (n "shale") (v "0.1.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lru") (r "^0.8.1") (d #t) (k 0)))) (h "0nq636n58a0blrr9mdvqwhl4fifqslcbxhg6n3xal2as4mlgvfvn")))

(define-public crate-shale-0.1.5 (c (n "shale") (v "0.1.5") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lru") (r "^0.8.1") (d #t) (k 0)))) (h "05bc5ynpwnqjc1cn8vlqvcrqblr7a2c9fwvm5gxw478s02drnh1p")))

(define-public crate-shale-0.1.6 (c (n "shale") (v "0.1.6") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lru") (r "^0.8.1") (d #t) (k 0)))) (h "17fh5p4smfz2wmpixcqw172igmgpw3fmn27hr6m89mid54h00xcv")))

(define-public crate-shale-0.1.7 (c (n "shale") (v "0.1.7") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lru") (r "^0.8.1") (d #t) (k 0)))) (h "1nn1qs20a1j7fby4k795cdzyvj5p4p6vm0vqpq41wjzmx92xv8z3")))

(define-public crate-shale-0.1.9 (c (n "shale") (v "0.1.9") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lru") (r "^0.12.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1sairayvf2iyiy6migmiwmpzil92allm4rnbxdmbkdl95b5v5p8k")))

