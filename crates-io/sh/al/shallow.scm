(define-module (crates-io sh al shallow) #:use-module (crates-io))

(define-public crate-shallow-0.0.0 (c (n "shallow") (v "0.0.0") (h "0qlj811gbz3zqg6v3vc5n5bhqcyjzlwd777xqnpz34amz85fkcaz") (f (quote (("default"))))))

(define-public crate-shallow-0.0.1 (c (n "shallow") (v "0.0.1") (h "1d4zq4mig46p0mvzra6c5skxjz4d1xkq600zqslm5c4i8qzrkxwc") (f (quote (("default"))))))

(define-public crate-shallow-0.0.2 (c (n "shallow") (v "0.0.2") (h "1vnjhqgr06c6dky7sdsba7fckflfybvsdzm3am7cxpdl7dp8sf7l") (f (quote (("default"))))))

(define-public crate-shallow-0.1.0 (c (n "shallow") (v "0.1.0") (h "01rzdy9l1cfbi6axpvk9d04yl1hfhdw8ifliag3aal0w7qx23wn9") (f (quote (("default"))))))

(define-public crate-shallow-0.1.1 (c (n "shallow") (v "0.1.1") (h "0iq8l1xqq1807axlkczbqv2fm4hccmw2cbiziwmbjy777xf2bbg2") (f (quote (("default"))))))

(define-public crate-shallow-0.2.0 (c (n "shallow") (v "0.2.0") (h "00xcvm2sc6xjbijm0v75haw5ahfvgi6n3r2lfz84gjp4w9k9vk62") (f (quote (("default"))))))

