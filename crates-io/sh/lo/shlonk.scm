(define-module (crates-io sh lo shlonk) #:use-module (crates-io))

(define-public crate-shlonk-0.1.0 (c (n "shlonk") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "=0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "08z7zy9cav2ccry2vjw3i5d84xmh4aklwfd06qdp17sjw1rsc2wc")))

(define-public crate-shlonk-0.1.1 (c (n "shlonk") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "=0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "142acqw33wz2ndjhfj24m5kkr8523ry6yvhjy64vai4s0nxmh7yy")))

(define-public crate-shlonk-0.1.2 (c (n "shlonk") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "=0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "0hfhdrlwgmkphgxy0igqk33r5qrwgwbh52ap8mfcz0i0l7h42nbb")))

(define-public crate-shlonk-0.1.3 (c (n "shlonk") (v "0.1.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r ">=0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)))) (h "1mfcpsa54c945cv663r9zslgdgq0igy4h332jx8hw6vqr2k0zkbf")))

(define-public crate-shlonk-0.1.4 (c (n "shlonk") (v "0.1.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r ">=0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)))) (h "1k1xhxmzm7i3ykkadac0c6hpa2chyq4zrxz6i0400qvgk63fxifv")))

(define-public crate-shlonk-0.1.5 (c (n "shlonk") (v "0.1.5") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r ">=0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)))) (h "16xx2590z03p3283fn5fyixzs2j1hia2677x6p25j4axd4j3g6d0")))

(define-public crate-shlonk-0.1.6 (c (n "shlonk") (v "0.1.6") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r ">=0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)))) (h "0smi4xn50bsv6dmxix0lnplsy309gsh51ic9iqmfd5nxwjfgsy6k")))

(define-public crate-shlonk-0.1.7 (c (n "shlonk") (v "0.1.7") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r ">=0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)))) (h "07pykccn4ixqfhvwjfkfwvy19qn96py8pdhq5qxinggqln174671")))

(define-public crate-shlonk-0.1.8 (c (n "shlonk") (v "0.1.8") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r ">=0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)))) (h "0z2zdhn8s0p8bj3lj5baag1cfngvqyn2hd15wqyvjh5vbdpadyw3")))

(define-public crate-shlonk-0.1.9 (c (n "shlonk") (v "0.1.9") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r ">=0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)))) (h "0y6hisd6m30klm3sfwil0wqb7mcvw5hq3bmqzwgm13c38ra6xdx1")))

(define-public crate-shlonk-0.1.10 (c (n "shlonk") (v "0.1.10") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r ">=0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)))) (h "0832s26vlx8mswn81qcx1xxc3ggabdh0s3vlixsr576qqiz6inl9")))

(define-public crate-shlonk-0.1.11 (c (n "shlonk") (v "0.1.11") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r ">=0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)))) (h "1a3jkrnjkr2yb8vgk5qibx2hm36p83pr7rz4y124mxqlnnlhibgw")))

