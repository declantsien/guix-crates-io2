(define-module (crates-io sh r3 shr3) #:use-module (crates-io))

(define-public crate-shr3-0.1.0 (c (n "shr3") (v "0.1.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)))) (h "1fpa8ch1ga0w5p8jan6b4qzmj2r52w4pizvn90kv1rxcjp3iy44z")))

(define-public crate-shr3-1.0.0 (c (n "shr3") (v "1.0.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)))) (h "1408miy70hg4yp893lgcp0a64cwsd1kadi53vilzggfnp9s0rz0s") (f (quote (("default") ("__devmode__"))))))

