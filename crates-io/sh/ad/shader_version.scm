(define-module (crates-io sh ad shader_version) #:use-module (crates-io))

(define-public crate-shader_version-0.0.0 (c (n "shader_version") (v "0.0.0") (h "1qqmpv9gq1bfzsbs8vaygqg6wcj4f2wfvh70z3x17l70as066vb7")))

(define-public crate-shader_version-0.0.1 (c (n "shader_version") (v "0.0.1") (h "1xfiybr2b2cgzhg0kikc102d0qncsghfhs4r52661svs9ac3wj61")))

(define-public crate-shader_version-0.0.2 (c (n "shader_version") (v "0.0.2") (h "08pls1jwc9fj2l2xisq0callq3jw1f9yqp3cx34h75r81wr8md4q")))

(define-public crate-shader_version-0.0.5 (c (n "shader_version") (v "0.0.5") (h "0y756ng24f58amw5m3cnc08fq4idbcqhs4qil9sjxhycv42fy3yx")))

(define-public crate-shader_version-0.0.6 (c (n "shader_version") (v "0.0.6") (h "0mxla6gb3y3ml3kvyihx306asdnrvw4fm4x103qgjcqbsy12yink")))

(define-public crate-shader_version-0.1.0 (c (n "shader_version") (v "0.1.0") (h "0yvqk2vz9rcq5panlz0f1j6q7qn5psyag76xd0cxkpadk0sjxl7g")))

(define-public crate-shader_version-0.2.0 (c (n "shader_version") (v "0.2.0") (h "03lr3cbhjfgrbqawpavd4i5n7f37dxxhw739mxsjm1vga4dl58pz")))

(define-public crate-shader_version-0.2.1 (c (n "shader_version") (v "0.2.1") (h "08cwnnlm7ipg22r1dfzzsml0whb6wja9ls7wgpqmlwfhg47i3n9y")))

(define-public crate-shader_version-0.2.2 (c (n "shader_version") (v "0.2.2") (h "0vq9k7m16ls0g9anl6ihc2y0n41vklykyz8js6i8nfss11cnysj7")))

(define-public crate-shader_version-0.3.0 (c (n "shader_version") (v "0.3.0") (h "1aplphm870mnlfqm61z41d0vydqjlswjkpkl1wy6dx24j71i17m2")))

(define-public crate-shader_version-0.4.0 (c (n "shader_version") (v "0.4.0") (d (list (d (n "piston-graphics_api_version") (r "^0.1.0") (d #t) (k 0)))) (h "0vkjcj0hc8i47wvgh9hn5s9mqh8c29a5pw0m3r84j5bjcn286kg5")))

(define-public crate-shader_version-0.5.0 (c (n "shader_version") (v "0.5.0") (d (list (d (n "piston-graphics_api_version") (r "^0.1.0") (d #t) (k 0)))) (h "1d4yg78v16kmfr2pnxrznsjrgszi7392fm9sjlp6d01k41ps40dm")))

(define-public crate-shader_version-0.6.0 (c (n "shader_version") (v "0.6.0") (d (list (d (n "piston-graphics_api_version") (r "^0.2.0") (d #t) (k 0)))) (h "1yk651xc9irl3pl0rlplypzyzy44d0j03ji0j7hjjdjknwzpi3j7")))

(define-public crate-shader_version-0.7.0 (c (n "shader_version") (v "0.7.0") (d (list (d (n "piston-graphics_api_version") (r "^1.0.0") (d #t) (k 0)))) (h "09hfsgmyzxms8vfvf56pdpsfgdcg03higkia0rzyjkkqfisvzbfz")))

