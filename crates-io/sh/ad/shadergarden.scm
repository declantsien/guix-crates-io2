(define-module (crates-io sh ad shadergarden) #:use-module (crates-io))

(define-public crate-shadergarden-0.2.0 (c (n "shadergarden") (v "0.2.0") (d (list (d (n "ffmpeg-next") (r "^4.4") (d #t) (k 0)) (d (n "glium") (r "^0.30.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "lexpr") (r "^0.2.6") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1jynqyisxdf8jk5il63phy9fnmn7pmhyk4jkql0j6w7ini5rs1hl")))

