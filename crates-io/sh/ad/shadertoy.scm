(define-module (crates-io sh ad shadertoy) #:use-module (crates-io))

(define-public crate-shadertoy-0.1.1 (c (n "shadertoy") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "0hkk7raz653bygx3vrz0s101jn7730gw5s4jbzn7hgpp7625qhpa")))

(define-public crate-shadertoy-0.1.2 (c (n "shadertoy") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "0rznqfd4hybwg6qwyknmb3gyyr9dqrpvycfj6afqbv77g7kz9w7z")))

(define-public crate-shadertoy-0.3.0 (c (n "shadertoy") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "0slgf6cm9qyqnhnllxgwg5q43q7k7k2wpy8i9sf2ymf5q2v5y8ah")))

(define-public crate-shadertoy-0.4.0 (c (n "shadertoy") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "1phg5lz6wflsb21gxz87s0kalvh0lvabwr2xrik53pqddz0xfzql")))

(define-public crate-shadertoy-0.4.1 (c (n "shadertoy") (v "0.4.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "0yklxz14h54fvbbdb061cv9a9nj0f923icazhfwvzspg29qvrky5")))

(define-public crate-shadertoy-0.5.0 (c (n "shadertoy") (v "0.5.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "0004214kyqvs1jd0mj5pv0bxlcgzyjq3871i5wfx5i2is2vrxnny")))

(define-public crate-shadertoy-0.6.0 (c (n "shadertoy") (v "0.6.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)))) (h "1lli9j22ljl4hfdym1xjmwh75q2jzmfk5mw7cmc4x0q2wrn4vjjm")))

(define-public crate-shadertoy-0.6.1 (c (n "shadertoy") (v "0.6.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.18") (d #t) (k 0)))) (h "1fg4ghkdc7qmdv1ycfydkyxlkzhzzn47mcyc2absnqnfrxq6cbzw")))

