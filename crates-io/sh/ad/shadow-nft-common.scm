(define-module (crates-io sh ad shadow-nft-common) #:use-module (crates-io))

(define-public crate-shadow-nft-common-0.1.0 (c (n "shadow-nft-common") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "solana-frozen-abi-macro") (r "=1.14") (d #t) (k 0)))) (h "05ggvgpvviwynk1r5qrxikl4d01b871dvxd4hmaxmsvi7yllkhm7")))

(define-public crate-shadow-nft-common-0.1.1 (c (n "shadow-nft-common") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "solana-frozen-abi-macro") (r "=1.14") (d #t) (k 0)))) (h "1z6pf5lk3vkyciiys70qqjg6hlghpnll8q8c97ffdcm93c5sqy64")))

