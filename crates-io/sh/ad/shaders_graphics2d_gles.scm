(define-module (crates-io sh ad shaders_graphics2d_gles) #:use-module (crates-io))

(define-public crate-shaders_graphics2d_gles-0.1.0 (c (n "shaders_graphics2d_gles") (v "0.1.0") (h "0vdp39bgzrv2wygmnz0cbi2jj5chyas0s2cpys1s1cp9v39dqgq5")))

(define-public crate-shaders_graphics2d_gles-0.2.0 (c (n "shaders_graphics2d_gles") (v "0.2.0") (h "07spqah9k6h2lnqamjijg4g6a03g0pm0374vqi5421fw6wpa9jbn")))

