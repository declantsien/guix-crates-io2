(define-module (crates-io sh ad shader-types) #:use-module (crates-io))

(define-public crate-shader-types-0.1.0 (c (n "shader-types") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (d #t) (k 2)) (d (n "zerocopy") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1pwb8y03zpa9kn2dx9j5p9vhks5ls0gc5d7hc76h0fs1g8pwf4iz")))

(define-public crate-shader-types-0.2.0 (c (n "shader-types") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (d #t) (k 2)))) (h "1xxhkyg25y38dzmvf16gqxb49i3imxv252nzij72yj7bbawplgzk")))

(define-public crate-shader-types-0.2.1 (c (n "shader-types") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (d #t) (k 2)) (d (n "mint") (r "^0.5.5") (o #t) (d #t) (k 0)))) (h "1j3rfwg0yfdmjvq0l53jqzrkbqbpgbapidyz1srprinjyz0j99w4") (f (quote (("std") ("default" "std"))))))

(define-public crate-shader-types-0.2.2 (c (n "shader-types") (v "0.2.2") (d (list (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (d #t) (k 2)) (d (n "glsl-layout") (r "^0.3") (d #t) (k 2)) (d (n "mint") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "std140") (r "^0.2") (d #t) (k 2)))) (h "1pxjmgvz4m2wm68hjdsrw2gidqadq9k79g69bjzldf1d7xrcjzq5") (f (quote (("std") ("default" "std"))))))

