(define-module (crates-io sh ad shader_to_spirv) #:use-module (crates-io))

(define-public crate-shader_to_spirv-0.1.0 (c (n "shader_to_spirv") (v "0.1.0") (d (list (d (n "naga") (r "^0.7.1") (f (quote ("wgsl-in" "glsl-in" "glsl-validate" "spv-out"))) (d #t) (k 0)))) (h "05al855pi50nplbj5x0ymfh5a6al76ja0a8ip9rgqmdg2gbhiggd")))

(define-public crate-shader_to_spirv-0.1.1 (c (n "shader_to_spirv") (v "0.1.1") (d (list (d (n "naga") (r "^0.7.1") (f (quote ("wgsl-in" "glsl-in" "glsl-validate" "spv-out"))) (d #t) (k 0)))) (h "1y7z5d25gd00pzl7lmvc17yvkz506pragkmcasjhm5lghx9a52sw")))

