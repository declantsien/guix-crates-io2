(define-module (crates-io sh ad shade_runner) #:use-module (crates-io))

(define-public crate-shade_runner-0.1.0 (c (n "shade_runner") (v "0.1.0") (d (list (d (n "color-backtrace") (r "^0.1") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "shaderc") (r "^0.5") (d #t) (k 0)) (d (n "spirv-reflect") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.12") (d #t) (k 0)))) (h "1mkrd93pgj6q8nwbcwimvck9szcvasmnbxjdakc693icgzica5bz")))

(define-public crate-shade_runner-0.1.1 (c (n "shade_runner") (v "0.1.1") (d (list (d (n "color-backtrace") (r "^0.1") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "shaderc") (r "^0.5") (d #t) (k 0)) (d (n "spirv-reflect") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.12") (d #t) (k 0)))) (h "0274slmjlggrh1cgsky3ahawp1l4p7rd5f6d74mkwsr2bv56bych")))

(define-public crate-shade_runner-0.1.2 (c (n "shade_runner") (v "0.1.2") (d (list (d (n "color-backtrace") (r "^0.1") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "shaderc") (r "^0.5") (d #t) (k 0)) (d (n "spirv-reflect") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.13") (d #t) (k 0)))) (h "0c4hilmv7n745xy0kb3ijyf534x0rap898phml9gm5p7g4034y7z")))

(define-public crate-shade_runner-0.2.0 (c (n "shade_runner") (v "0.2.0") (d (list (d (n "color-backtrace") (r "^0.1") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "spirv-reflect") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.14") (d #t) (k 0)))) (h "18kzbzq244qgkaw2fdphwxlwfvss4paf7d473s33xf1a7j4x8k4n")))

(define-public crate-shade_runner-0.3.0 (c (n "shade_runner") (v "0.3.0") (d (list (d (n "color-backtrace") (r "^0.1") (d #t) (k 2)) (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "spirv-reflect") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.16") (d #t) (k 0)))) (h "0h27czaxsmd84dlq55zbz1sqfs87lp6phrzv4wdkrxw64690qlpl")))

