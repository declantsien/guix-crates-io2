(define-module (crates-io sh ad shader-translator) #:use-module (crates-io))

(define-public crate-shader-translator-0.1.0 (c (n "shader-translator") (v "0.1.0") (d (list (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "simon") (r "^0.3") (d #t) (k 0)))) (h "0gg8jdnlqhc75gvrls44jdnxsaadxianbbxs2wcbz3babfviy4dx")))

(define-public crate-shader-translator-0.2.0 (c (n "shader-translator") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "meap") (r "^0.4") (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)))) (h "1g7s27v6wblygskml0vbs85baccwmfzjb6fy1nxg80g8y2mv3wnj")))

(define-public crate-shader-translator-0.3.0 (c (n "shader-translator") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "meap") (r "^0.4") (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)))) (h "08zs0pr684za42xnrhl1z3jn1izm704bhvhja195ydjh7plhl9cl")))

(define-public crate-shader-translator-0.4.0 (c (n "shader-translator") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "meap") (r "^0.4") (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)))) (h "0jakrwm83pshrgfr0svc4z71ml94afzv2ak8z79rilspv93bga7r")))

