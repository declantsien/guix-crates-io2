(define-module (crates-io sh ad shaderc) #:use-module (crates-io))

(define-public crate-shaderc-0.1.0 (c (n "shaderc") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "068kkb5jd1rhvh55dhwbdxh58fg7p16zn0m23h1nl9rs09776ab4")))

(define-public crate-shaderc-0.1.1 (c (n "shaderc") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jf2sikg88gph89ryhvwbalivmj8il2zrhmbid5n2inr567c9aq9")))

(define-public crate-shaderc-0.1.2 (c (n "shaderc") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05b91qapsz20kgkvld48z09dzk8h0ca3hbmjw4rwl8wrck2ziz5b")))

(define-public crate-shaderc-0.2.0 (c (n "shaderc") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rifbkmqscshhjv2v71053i8g6b2akhwcxqdl2yki1dvih995znz") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.0 (c (n "shaderc") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rwq7fkhwsjpfpfgzyky385hcq0z14i1gvpdj01jfqgqigm1x911") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.1 (c (n "shaderc") (v "0.3.1") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r1xv3d3grici22nsqwqahzzkqlzhlq49w2ibzmkc0xbrz0vpdql") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.2 (c (n "shaderc") (v "0.3.2") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pjq0kk9b0p95dav1png58ps3aqaizrxycwfyciqaaziigcw9ayk") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.3 (c (n "shaderc") (v "0.3.3") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13id7gbh5c729inzavj5h6g837qg6gp7imdqd4716k881lv2jdd3") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.4 (c (n "shaderc") (v "0.3.4") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "006ii292h2ppiyn4cgplqy04xbs5vb1ys23w8bb0fg8lkfcyzwz9") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.5 (c (n "shaderc") (v "0.3.5") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08cff5c90bpaxvnr9xfxc6q9mdpbn28qryr4mzk56df7ipc8z21f") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.6 (c (n "shaderc") (v "0.3.6") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16fg1b9yarafmjj2gb0ncvqlbd1n3i6131l9qgydb6yj0fh7628x") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.7 (c (n "shaderc") (v "0.3.7") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bhwfyjd2inqkm18kwcp2vkk4vl1xb69ch0fpvh16im1jpjnb7bs") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.8 (c (n "shaderc") (v "0.3.8") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r05jkiib54x3d7h3cpmjljxny3cxl3hgv8qmslx6jhqmmz39xj7") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.9 (c (n "shaderc") (v "0.3.9") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nc07gskxbfn2ymn1l7i8nw1vcklqf3ypbg3w6d98fl2z7vjndf7") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.10 (c (n "shaderc") (v "0.3.10") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fzs1n49wa73d4k3znx5glnccncs8d6h0q9p051qzy81yknccmpw") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.11 (c (n "shaderc") (v "0.3.11") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "039wvs35lwryb4mrhmb80mkxdm2f6qvjw69x511zg4laq5fcmr4l") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.12 (c (n "shaderc") (v "0.3.12") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jqv2vjqbz3azsgwnd3jdgblyx1w8dqv5539pzmqzsridg3b01qb") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.13 (c (n "shaderc") (v "0.3.13") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nr8n2fypff6jvmmr79j8piq1bbqwz9vfrb70cvgf8g91cm63rd2") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.14 (c (n "shaderc") (v "0.3.14") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sf31q0azpddi0rd977jiwrs3x74k89v4jvrdr6dhgy3xj6xbhmm") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.15 (c (n "shaderc") (v "0.3.15") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0p75p5qdd785kz84dzwszy3xibxcaz2bqf4srfp6vskdr38d848y") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.3.16 (c (n "shaderc") (v "0.3.16") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0as86jrpd2phw8sl7p4nbsdiz1f2nk0kp6i6x556fxrp5lz2qbfq") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc"))))))

(define-public crate-shaderc-0.5.0 (c (n "shaderc") (v "0.5.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.5.0") (f (quote ("check-inverted-no-defaults"))) (d #t) (k 0)))) (h "0yzh41g0q2995rsm3yl46d2vksxb2bmk4rayz9rpavl0fznzv7g6") (f (quote (("default" "shaderc-sys/inverted-no-defaults") ("check-inverted-no-defaults" "shaderc-sys/check-inverted-no-defaults") ("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.5.1 (c (n "shaderc") (v "0.5.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.5.1") (f (quote ("check-inverted-no-defaults"))) (d #t) (k 0)))) (h "08icj2p4aslxws78c7bjzih6920wxx6fxd08g4xk84iym11zzvdp") (f (quote (("default" "shaderc-sys/inverted-no-defaults") ("check-inverted-no-defaults" "shaderc-sys/check-inverted-no-defaults") ("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.6.0 (c (n "shaderc") (v "0.6.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0mqggx56yg63f72xs4w0mj8rl3cggr68djblbi6d77kn9v04jl7w") (f (quote (("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.6.1 (c (n "shaderc") (v "0.6.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.6.1") (d #t) (k 0)))) (h "1d1m2rx1sijcv6z0675g9k3229vx5kyixba7m1a306zf08sbqjka") (f (quote (("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.6.2 (c (n "shaderc") (v "0.6.2") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.6.2") (d #t) (k 0)))) (h "10c25927qrgsa01bxrszklw9n3zhg2sz0jpvdg6a6zrdvww4jd7d") (f (quote (("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.6.3 (c (n "shaderc") (v "0.6.3") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.6.3") (d #t) (k 0)))) (h "1s9bwa3a5jwacyjcc71fj676jrsz4rz6lwv6p9fdm6qbw6maxf2h") (f (quote (("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.7.0 (c (n "shaderc") (v "0.7.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1154zk1a1g1bnxk7ry0mk9gsy5x163l4n1fma2fgqrw63y6wpw03") (f (quote (("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.7.1 (c (n "shaderc") (v "0.7.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.7.1") (d #t) (k 0)))) (h "1l15g3j27j5150xwpg405rl75cgbzhznn3pfp5k7d5x0ag0nz2ck") (f (quote (("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.7.2 (c (n "shaderc") (v "0.7.2") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.7.2") (d #t) (k 0)))) (h "0xy5p84p5kgsfipdigryk6v7das3agn4lh8c70hgldrvlmapk8qw") (f (quote (("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.7.3 (c (n "shaderc") (v "0.7.3") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.7.3") (d #t) (k 0)))) (h "1sk7pibwaai32c99pmp7bm2mz3f92iqmd67hrnc8al6k9apqmnjq") (f (quote (("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.7.4 (c (n "shaderc") (v "0.7.4") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.7.4") (d #t) (k 0)))) (h "180i25sgip8xl43snsj82rh256b8fn6737fpv1876584p1bwgqnn") (f (quote (("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.8.0 (c (n "shaderc") (v "0.8.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.8.0") (d #t) (k 0)))) (h "1vkbbnf53xc35r0wil46y7dim8zv1g23p8ihjmv245l659hgxrl0") (f (quote (("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.8.1 (c (n "shaderc") (v "0.8.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.8.1") (d #t) (k 0)))) (h "093q2aayfilhq0a30falznbwqqyd9h2p3pb4z6pcw8524vm1193p") (f (quote (("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.8.2 (c (n "shaderc") (v "0.8.2") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.8.2") (d #t) (k 0)))) (h "0hrl1w3zf9apwl0m4p3w52b4pgyn2gnhp8lfg0453nx0hwkzbkii") (f (quote (("build-from-source" "shaderc-sys/build-from-source"))))))

(define-public crate-shaderc-0.8.3 (c (n "shaderc") (v "0.8.3") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shaderc-sys") (r "^0.8.3") (d #t) (k 0)))) (h "0yrvwq0xqlc8c7hlhng9pfz3p39mwg5k296ic2xhg1m1ml9pkq17") (f (quote (("prefer-static-linking" "shaderc-sys/prefer-static-linking") ("build-from-source" "shaderc-sys/build-from-source"))))))

