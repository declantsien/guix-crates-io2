(define-module (crates-io sh ad shader-reflect) #:use-module (crates-io))

(define-public crate-shader-reflect-0.2.0 (c (n "shader-reflect") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "spirq") (r "^0.6.0") (d #t) (k 0)))) (h "1kj8jd5mc7v975gn1x19nhhs369i4jvj0a847hgv7w50ygca9p3w")))

(define-public crate-shader-reflect-0.2.1 (c (n "shader-reflect") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "spirq") (r "^0.6.1") (d #t) (k 0)))) (h "0wdwjnpx4i2ga945yx9dw8nygvxrh25ihvvpjri0wg4yyv8n6434")))

(define-public crate-shader-reflect-0.2.2 (c (n "shader-reflect") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "spirq") (r "^0.6.3") (d #t) (k 0)))) (h "1vnsgdkn8kh8pdrr86hrklv9v0jwmwkwpfvxy5c7lklgq0fghrvw")))

(define-public crate-shader-reflect-0.2.3 (c (n "shader-reflect") (v "0.2.3") (d (list (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "spirq") (r "^0.6.4") (d #t) (k 0)))) (h "1brwplvkxgrxzsxgkh9wxhvxsxs3qpbliq38nz4xnkfzfh5l15gq")))

(define-public crate-shader-reflect-0.2.4 (c (n "shader-reflect") (v "0.2.4") (d (list (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "spirq") (r "^1.1.1") (d #t) (k 0)))) (h "0pldd77w695l4fhpzfn6wp44nd36kba64999ybwkmfhkhbpry8xi")))

(define-public crate-shader-reflect-0.2.6 (c (n "shader-reflect") (v "0.2.6") (d (list (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "spirq") (r "^1.1.3") (d #t) (k 0)))) (h "05jpqcxa9cyvsvlldrsyv6rsxv0y85d3z7q8jnbi92hn5g0qsi39")))

(define-public crate-shader-reflect-0.2.7 (c (n "shader-reflect") (v "0.2.7") (d (list (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "spirq") (r "^1.2.0") (d #t) (k 0)))) (h "069c0gh10x6xb62zlwv0fpcvkbq33dhahldnzb35v0szgcjws3wk")))

