(define-module (crates-io sh ad shader-prepper) #:use-module (crates-io))

(define-public crate-shader-prepper-0.1.0 (c (n "shader-prepper") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7.11") (d #t) (k 0)))) (h "1kf79fl6gdbbbkzscnlryq3zazjbci0hqdf5qwvyz4rp93y6m41z")))

(define-public crate-shader-prepper-0.1.1 (c (n "shader-prepper") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "0hwywbdz078j6x82zhxm6qpnrmmw2y1565px45k2llih35lnjv5x")))

(define-public crate-shader-prepper-0.1.2 (c (n "shader-prepper") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "1knhw36n63vvlc5jjyvbz3vvclx49k8nh88wn1p898zrpx9i5rqm")))

(define-public crate-shader-prepper-0.2.0 (c (n "shader-prepper") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "03jw7qlr1y2pinxw7bnqd2h6h5r3rryqfjlsai6a8kr50g231x8r")))

(define-public crate-shader-prepper-0.3.0-pre.1 (c (n "shader-prepper") (v "0.3.0-pre.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "173jvnjbyh6v348s188g97gaj2bigvj3s2rya0pxy3b0wmap534r") (f (quote (("gl_compiler" "regex" "lazy_static"))))))

(define-public crate-shader-prepper-0.3.0-pre.2 (c (n "shader-prepper") (v "0.3.0-pre.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03laajhzbqnsiz15pq01assa9fy6zmmr0i40qnxy1ppb1l53h8s9") (f (quote (("gl_compiler" "regex" "lazy_static") ("default"))))))

(define-public crate-shader-prepper-0.3.0-pre.3 (c (n "shader-prepper") (v "0.3.0-pre.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hf0m40jpvlgc9sznhy68cxm671bacmdnk8lk2lj3fw0hf4wix1j") (f (quote (("gl_compiler" "regex" "lazy_static") ("default"))))))

