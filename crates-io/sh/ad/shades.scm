(define-module (crates-io sh ad shades) #:use-module (crates-io))

(define-public crate-shades-0.1.0 (c (n "shades") (v "0.1.0") (h "0nrmy49qkyh33r2xh46hffr6707sn3sir4banzf3yhvacf5dprcv") (f (quote (("fun-call"))))))

(define-public crate-shades-0.2.0 (c (n "shades") (v "0.2.0") (h "1mkw48ggql92ldi2rzbxvp51802mdxrvlznj2891xzj0595xs399") (f (quote (("fun-call"))))))

(define-public crate-shades-0.3.0 (c (n "shades") (v "0.3.0") (h "1qrzacb949w8as0fkdwkc175gnwgl6bqk6hjw6kvabcwhl8ci8k1") (f (quote (("fun-call"))))))

(define-public crate-shades-0.3.1 (c (n "shades") (v "0.3.1") (h "1n7czwjzy671177d0wf41waqs34i361mzaix0jr1ixh7ha1d3rnj") (f (quote (("fun-call"))))))

(define-public crate-shades-0.3.2 (c (n "shades") (v "0.3.2") (h "1yh7q4aldsnnc98sxar4isxcp5a80nhr43vba1dfg9cm0ql386mh") (f (quote (("fun-call"))))))

(define-public crate-shades-0.3.3 (c (n "shades") (v "0.3.3") (h "1is2zvakihxp10kbnxqngkjh10jjq1g3gx828wlx0vl4p90lypkb") (f (quote (("fun-call"))))))

(define-public crate-shades-0.3.4 (c (n "shades") (v "0.3.4") (h "00drzm7ff920iibav1kyx41yw2p1qdw3ibnir39k1rnl6wp0ckgh") (f (quote (("fun-call"))))))

(define-public crate-shades-0.3.5 (c (n "shades") (v "0.3.5") (h "12hvqbhrp4n0a218qn96adcxcmxbg0qzpv8902k9sv9z2i5fzgw4") (f (quote (("fun-call"))))))

(define-public crate-shades-0.3.6 (c (n "shades") (v "0.3.6") (h "0qc3wqrsgfsx73bb4b2x43z21p75pmh5xya4xg8x9g2rx3l6z3a5") (f (quote (("fun-call"))))))

(define-public crate-shades-0.4.0 (c (n "shades") (v "0.4.0") (d (list (d (n "shades-edsl") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1mdk9ag5a8ipjljjd5s0h2qz3hc681x6ficr7ziqg0d7fy5b4l0s") (f (quote (("edsl" "shades-edsl") ("default" "edsl"))))))

