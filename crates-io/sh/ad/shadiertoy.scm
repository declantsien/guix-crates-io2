(define-module (crates-io sh ad shadiertoy) #:use-module (crates-io))

(define-public crate-shadiertoy-0.1.0 (c (n "shadiertoy") (v "0.1.0") (d (list (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.24") (d #t) (k 0)) (d (n "glutin") (r "^0.16") (d #t) (k 0)) (d (n "image") (r "^0.19") (d #t) (k 0)))) (h "0m8gfq8hmdy7jcfr5rgb9ail5la51y0lv40brc2sxvdqqpj2ndkh")))

