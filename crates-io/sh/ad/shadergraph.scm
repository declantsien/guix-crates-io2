(define-module (crates-io sh ad shadergraph) #:use-module (crates-io))

(define-public crate-shadergraph-0.1.0 (c (n "shadergraph") (v "0.1.0") (d (list (d (n "ffmpeg-next") (r "^4.4") (d #t) (k 0)) (d (n "glium") (r "^0.30.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "lexpr") (r "^0.2.6") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0cr7lz4w6mx87gdxn8dmm5ikjdablc4x6pw7jq2sm1jfyi7rn0qp")))

