(define-module (crates-io sh ad shader-crusher) #:use-module (crates-io))

(define-public crate-shader-crusher-0.1.1 (c (n "shader-crusher") (v "0.1.1") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "entropy") (r "^0.3.0") (d #t) (k 0)) (d (n "glsl") (r "^4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "regex") (r "~1.3") (d #t) (k 0)))) (h "00qx6lv0ciz67sgh6j9wk0p65ym1466mwnzb698cky3msv441gxr")))

(define-public crate-shader-crusher-0.1.2 (c (n "shader-crusher") (v "0.1.2") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "entropy") (r "^0.3.0") (d #t) (k 0)) (d (n "glsl") (r "~4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "regex") (r "~1.4") (d #t) (k 0)))) (h "1zy163ldp38yapxw4fwfz6jrrmsrmv59m5rm584fmn42i76x53dr")))

(define-public crate-shader-crusher-0.1.3 (c (n "shader-crusher") (v "0.1.3") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "entropy") (r "^0.3.0") (d #t) (k 0)) (d (n "glsl") (r "~4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "regex") (r "~1.4") (d #t) (k 0)))) (h "0zczj9sfh6yvd2s2s5kx7zlg3s85rahx09wrlkx2nzv2jcbll59z")))

(define-public crate-shader-crusher-0.2.0 (c (n "shader-crusher") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "entropy") (r "^0.3.0") (d #t) (k 0)) (d (n "glsl") (r "~4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1c9x2z6r0k0xzpxhs1f0v3s7dqwjx6rc3hkgl48l5yhpd14szmc8")))

(define-public crate-shader-crusher-0.3.0-alpha (c (n "shader-crusher") (v "0.3.0-alpha") (d (list (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "entropy") (r "^0.4.1") (d #t) (k 0)) (d (n "glsl") (r "^5.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.123") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0xmql9y67hn2qx10v2ri62zm8l872qgi7drla4wl7dmw0qp1w0s0")))

(define-public crate-shader-crusher-0.3.1-alpha (c (n "shader-crusher") (v "0.3.1-alpha") (d (list (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "entropy") (r "^0.4.1") (d #t) (k 0)) (d (n "glsl") (r "^6.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.123") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0mgxlr0yfm2mkb34dyng8dpipmwb8xfwgsas9py6hnbzs05b7pnb") (y #t)))

(define-public crate-shader-crusher-0.3.2-dev (c (n "shader-crusher") (v "0.3.2-dev") (d (list (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "entropy") (r "^0.4.1") (d #t) (k 0)) (d (n "glsl") (r "^6.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.123") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "01g7cdixdhvk29vqfqyzmpysq0zc8ygikx5mmr663v1rx6hc0bl2") (y #t)))

(define-public crate-shader-crusher-0.3.6-dev (c (n "shader-crusher") (v "0.3.6-dev") (d (list (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "entropy") (r "^0.4.1") (d #t) (k 0)) (d (n "glsl") (r "^6.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.123") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1gan3b7qhw8bl9m86jxls9r75ayhq8jyv1ln7nlp72yiyzvls7n3") (y #t)))

(define-public crate-shader-crusher-0.3.9-alpha (c (n "shader-crusher") (v "0.3.9-alpha") (d (list (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "entropy") (r "^0.4.1") (d #t) (k 0)) (d (n "glsl") (r "^6.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.123") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "06gmvj1yb3b8vqnnrmcrmcp9nhblr1klab41611sqavm9ljf31vw")))

(define-public crate-shader-crusher-0.3.10-alpha (c (n "shader-crusher") (v "0.3.10-alpha") (d (list (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "entropy") (r "^0.4.1") (d #t) (k 0)) (d (n "glsl") (r "^6.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.123") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1dc1yd5lyyf0a1akf6m00h3j7wcm00sn47lp058jdjringp3r474")))

(define-public crate-shader-crusher-0.3.11-alpha (c (n "shader-crusher") (v "0.3.11-alpha") (d (list (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "entropy") (r "^0.4.1") (d #t) (k 0)) (d (n "glsl") (r "^6.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.123") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1cjsp3gfm96rmbn91bgqfxz6whc1a22jmn8293ipcwc11g6jzyc6") (y #t)))

(define-public crate-shader-crusher-0.3.12-alpha (c (n "shader-crusher") (v "0.3.12-alpha") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "entropy") (r "^0.4.1") (d #t) (k 0)) (d (n "glsl") (r "^6.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "09x5a9782cfblmdfilspqczqw12g914xiha86pmhk22dsajjn9mz")))

(define-public crate-shader-crusher-0.4.0-alpha (c (n "shader-crusher") (v "0.4.0-alpha") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "entropy") (r "^0.4.1") (d #t) (k 0)) (d (n "glsl") (r "^6.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1dljvl6i7wazyj43ckmhcpxfpychqc7qrk9j5bcsrhbcg39s2qw8")))

(define-public crate-shader-crusher-0.4.1-alpha (c (n "shader-crusher") (v "0.4.1-alpha") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "entropy") (r "^0.4.1") (d #t) (k 0)) (d (n "glsl") (r "^6.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0r4m6b797yixhwl8y7qi9vdpbhs7y16gnqh8y9ibwnpjr09q4fjy")))

