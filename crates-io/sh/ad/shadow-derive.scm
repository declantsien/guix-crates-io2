(define-module (crates-io sh ad shadow-derive) #:use-module (crates-io))

(define-public crate-shadow-derive-0.2.0 (c (n "shadow-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "05kp1yqfn15xkxrycxqszwcvas13fnmkymk80lshsi311ysrw75i")))

(define-public crate-shadow-derive-0.2.1 (c (n "shadow-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1pchvjxy1nkpvw58x5hbqxbnxb8izvnkzks621lwck2xgdz91yr0")))

