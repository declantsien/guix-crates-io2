(define-module (crates-io sh ad shadow-drive-app) #:use-module (crates-io))

(define-public crate-shadow-drive-app-0.0.0 (c (n "shadow-drive-app") (v "0.0.0") (h "1ibiawhz2rkqjayrjvmw3f4q9l2dws23a3c6k0c83zm7vlf8g55k")))

(define-public crate-shadow-drive-app-0.6.2 (c (n "shadow-drive-app") (v "0.6.2") (h "05dqcinmr33354jg7j4k8yr9m65my1d99id1396irbn5h5s4kbzb")))

(define-public crate-shadow-drive-app-0.6.3 (c (n "shadow-drive-app") (v "0.6.3") (h "0clrfgxl6xjf57vnh4x1gjw1ar63qzlr00x78vnqkdhsk1jc0g71")))

(define-public crate-shadow-drive-app-0.7.0 (c (n "shadow-drive-app") (v "0.7.0") (h "1i9s2vqqx21wm9xxa727nxzgmyxma9sz8mnbkbv0hblwk49bvvj6")))

(define-public crate-shadow-drive-app-0.7.1 (c (n "shadow-drive-app") (v "0.7.1") (h "12612xfr99981rjlgcx2zdzhddym8cr2v80zj1qbmlgw6g923m97")))

