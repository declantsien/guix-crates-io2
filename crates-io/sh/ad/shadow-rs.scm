(define-module (crates-io sh ad shadow-rs) #:use-module (crates-io))

(define-public crate-shadow-rs-0.1.0 (c (n "shadow-rs") (v "0.1.0") (h "1yk1rb8gxg3fv9gn547c82r6n6j83nk0n23js3s319dkmyhy9i2g")))

(define-public crate-shadow-rs-0.2.0 (c (n "shadow-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0dcnxld2wg8ym77n0v0iyfx06hlbidmg50jr84dhbvsd7sc30hps")))

(define-public crate-shadow-rs-0.2.1 (c (n "shadow-rs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0a5hn9xskb4k6nba3w6wfakbd0sr04kfj8w0cw9hqhyihpdi3d2w")))

(define-public crate-shadow-rs-0.3.0 (c (n "shadow-rs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "1gkfv8j5vlq9295bq43fhagkfwx393kjx5609439jsvp7cyibn97")))

(define-public crate-shadow-rs-0.3.1 (c (n "shadow-rs") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0npxk0h1f8msryp9a4rd49wpgh30s4s3b22lz9ib95cy29agzbfa")))

(define-public crate-shadow-rs-0.3.2 (c (n "shadow-rs") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0527y71lg7mmmir6hw8m215naz0hm3nfind24f8j8rjdf7ql3xbc")))

(define-public crate-shadow-rs-0.3.3 (c (n "shadow-rs") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "09h9w8l8vhsisgi6sn6jig2q0941zad4kkn4llasplvwyxwrqbd1")))

(define-public crate-shadow-rs-0.3.4 (c (n "shadow-rs") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0xivhmxhj7f6bs227qb09mz7l49gp6h8fs5hbq3scv9zlm46grdk")))

(define-public crate-shadow-rs-0.3.5 (c (n "shadow-rs") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0i6awbrhyvfa4am9s26bhxgn20d4phqg4x5cdwbb3idqzgapv0za")))

(define-public crate-shadow-rs-0.3.7 (c (n "shadow-rs") (v "0.3.7") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0v33r9xxrq4i9yadhacycbd05bs5lb16hhyh9bh7wl63a0f4k07v")))

(define-public crate-shadow-rs-0.3.8 (c (n "shadow-rs") (v "0.3.8") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "1aiwg61ypwjfqc3ra785jlza4yhr4hxycjgvvp5xfjdxxfh40yjr")))

(define-public crate-shadow-rs-0.3.9 (c (n "shadow-rs") (v "0.3.9") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "08jsprdgk82z82hr1ajin9ahv3062g7sd9mzv0pcdpcrlalxdvd7")))

(define-public crate-shadow-rs-0.3.10 (c (n "shadow-rs") (v "0.3.10") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "023871jsp9gfyw3dgg76bki1kvivs90a2hg99y3j92q4illm0psb")))

(define-public crate-shadow-rs-0.3.11 (c (n "shadow-rs") (v "0.3.11") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "1ldndnnmq1crrvq5a2i6yvdb3km8dnrz0fgbw0q45rr1cmd7417j")))

(define-public crate-shadow-rs-0.3.12 (c (n "shadow-rs") (v "0.3.12") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0ajw1bd0cxnwv8hkahppx0518m73ip2ab0qpzig5hli5v7cfh55c")))

(define-public crate-shadow-rs-0.3.13 (c (n "shadow-rs") (v "0.3.13") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "1qicvlaghzgfppv41gwih9g737r03x0i62c2z3ms9k1i57mdg7j5")))

(define-public crate-shadow-rs-0.3.14 (c (n "shadow-rs") (v "0.3.14") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "1yg24mwn87hhwhm7l6ajy6hi3i46p65g0afikyscxj5a6qfrjam4")))

(define-public crate-shadow-rs-0.3.15 (c (n "shadow-rs") (v "0.3.15") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "175f3mhvlw2mpgmbxwnvxws2jh4psdfy806a4vk23qqnvjmnc6af")))

(define-public crate-shadow-rs-0.3.16 (c (n "shadow-rs") (v "0.3.16") (d (list (d (n "chrono") (r ">=0.4.13, <0.5.0") (d #t) (k 0)) (d (n "git2") (r ">=0.13.8, <0.14.0") (d #t) (k 0)))) (h "0fdjx18v8n81i26fi13k20xn5782qmdql0ac4b8kx7zivmgpldxn") (y #t)))

(define-public crate-shadow-rs-0.3.17 (c (n "shadow-rs") (v "0.3.17") (d (list (d (n "chrono") (r ">=0.4.13, <0.5.0") (d #t) (k 0)) (d (n "git2") (r ">=0.13.8, <0.14.0") (d #t) (k 0)))) (h "0gx5711vgw112h86i8rf17yjkd5s46xwz86qx1jbyv5v28g9kqik")))

(define-public crate-shadow-rs-0.3.18 (c (n "shadow-rs") (v "0.3.18") (d (list (d (n "chrono") (r ">=0.4.13, <0.5.0") (d #t) (k 0)) (d (n "git2") (r ">=0.13.8, <0.14.0") (d #t) (k 0)))) (h "0p9hq8gxi3687ij3w76j4q86wbjd85j2awhm1z2nlf58f6zmxvmq")))

(define-public crate-shadow-rs-0.3.19 (c (n "shadow-rs") (v "0.3.19") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0pfmr2hrxgcfffki7ryrnl8xwx1wxrg3hbr1gqpf9zb9438yhwxl")))

(define-public crate-shadow-rs-0.3.20 (c (n "shadow-rs") (v "0.3.20") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "11xb2vx10xfmry5hdl82gv7b5dmb4s90cs9gb3ipfrm2zicrvadz")))

(define-public crate-shadow-rs-0.4.0 (c (n "shadow-rs") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0gzakkcqj9nzgm1kg3i54nm3m8cip05nzcndwv37y8qnh1fpfzmb")))

(define-public crate-shadow-rs-0.4.1 (c (n "shadow-rs") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0bdmp4hm70618ihzdadjx2yxcb5vfph42f34ax5a8sbg7afamqn3")))

(define-public crate-shadow-rs-0.5.0 (c (n "shadow-rs") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0ahmm8drhzvjdgahc5pz9f0j1vyw196837l4dcnia3bd1znbq4im")))

(define-public crate-shadow-rs-0.5.1 (c (n "shadow-rs") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "11rywsd06n4q29kmb6whpylsi6vrhsm73j75q98jk9iphb66xvmm")))

(define-public crate-shadow-rs-0.5.2 (c (n "shadow-rs") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0dc4v29rm16avs2wb72hx63sqnyaag97sm9b015mq8bvvg185qh4")))

(define-public crate-shadow-rs-0.5.3 (c (n "shadow-rs") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0y3h3cq703v8cyiniah7din4lva7f5yk7mh60nc4vvzpjlahhzb7")))

(define-public crate-shadow-rs-0.5.4 (c (n "shadow-rs") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "035b8z9p33n7s2g874ynvbk7v3fbmmwzazhh10cm3h5wdqsydp35")))

(define-public crate-shadow-rs-0.5.5 (c (n "shadow-rs") (v "0.5.5") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "0w885w4ywlwpw872j3hwv4my4bv4sxapq8pbhxm2i3qg68i6dpdp")))

(define-public crate-shadow-rs-0.5.6 (c (n "shadow-rs") (v "0.5.6") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)))) (h "1wjacnqkyg5rlg4kx41j2kkac66zsly4prgqr3f8nkzsvmzkzsx1")))

(define-public crate-shadow-rs-0.5.7 (c (n "shadow-rs") (v "0.5.7") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (k 0)))) (h "09vi2dzmak0j9ax78dmvl3339cgf0hq74c34l3sxb25j79ppknbk")))

(define-public crate-shadow-rs-0.5.8 (c (n "shadow-rs") (v "0.5.8") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (k 0)))) (h "1nzcniw33k25sjpw4lgn917dpx1hln5apqi2lqx245w1pls65ls8")))

(define-public crate-shadow-rs-0.5.9 (c (n "shadow-rs") (v "0.5.9") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (k 0)))) (h "00k7szhxyyg7v2c5j3fl22kxqg78k0bnn019s0y66jqgrzkyyyhy")))

(define-public crate-shadow-rs-0.5.10 (c (n "shadow-rs") (v "0.5.10") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (k 0)))) (h "1wcnjryk7wa8g4xvvfnzrnmrli3360ys53j6nidvq8rw3nhdygnr")))

(define-public crate-shadow-rs-0.5.11 (c (n "shadow-rs") (v "0.5.11") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (k 0)))) (h "1x7limv2niqbvdhqgp0qlcm934nm76dsa8342gmk9fxswsldaxlf")))

(define-public crate-shadow-rs-0.5.12-pre (c (n "shadow-rs") (v "0.5.12-pre") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "0zjky8gzzckh66iswf81hv1rv1nh4glvphfd2jrw6qd8mvvwrpnq") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.12 (c (n "shadow-rs") (v "0.5.12") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "0xdqlzj4m6msyxpv3sqwvbmgfib7hsg97ri8x8859bvnaz0k9lz5") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.13 (c (n "shadow-rs") (v "0.5.13") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "0caayyd87lwkc3iaj4gh8w646wsmx9h35j7jb9l5096lp6il8p8d") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.14 (c (n "shadow-rs") (v "0.5.14") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "09kwsqlzpgapdk97c9i46pap2rrjw25jclp9lj8fczksshs5g0zj") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.15 (c (n "shadow-rs") (v "0.5.15") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "00aghvw2rlw6fwf8x9l12zw634fhd69sl61s7ajfwm2a60qyxb4g") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.16 (c (n "shadow-rs") (v "0.5.16") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "1cnq1hadl4sh7jmlldfyc7yjh2554wn78ixxhdw9xrx7ml1zxzd5") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.17 (c (n "shadow-rs") (v "0.5.17") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "0ygzyv0mnsdq5gadqz7im5bjm9hawrzqxjpzmm5myvp93a9jqghl") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.18 (c (n "shadow-rs") (v "0.5.18") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "10m74d1xs7432sa6pcqvzrnf6j2gkjn6zfmzfixf6nqbbh3i3h0c") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.19 (c (n "shadow-rs") (v "0.5.19") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "0vp7xaq4dl8fn9vdad7pny7wvb1x3qcrsnrnbj9ldrdy4af2n4xx") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.20 (c (n "shadow-rs") (v "0.5.20") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "1rwr8qhyf3fiqxxqa1lxdmsvwd3vla35r2admpdfph6md5ixiljd") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.21 (c (n "shadow-rs") (v "0.5.21") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "0f4jbymv6kbjgk5cfs3kc3mkwnw6yakvrycqjcmpm4937f9a0zhw") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.22 (c (n "shadow-rs") (v "0.5.22") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "0xp8sjh675avnx17vgqpmk5y61r5fb9qzvjd18jn2jv400vl6xvz") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.23 (c (n "shadow-rs") (v "0.5.23") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "1kn6n5ns7awqarzhw4ysz7rsxm19lbsdk8bgqpycn2fjli04c1dv") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.24 (c (n "shadow-rs") (v "0.5.24") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "1qldaq7z09s6v0qp2yfhkrhvwprnjr4my6ngmidl2xj1v0ql9ln2") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.5.25 (c (n "shadow-rs") (v "0.5.25") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "0193qj586xpbz2c1aa7g3vpxfvhz1z251wl488m0ygrxvrxhw3hi") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.0 (c (n "shadow-rs") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "1f0n115wnjv74khm8ijcl6mnx1cyaknjkm7xcamvbzjmmd1lcs3d") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.1 (c (n "shadow-rs") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "1zx6ch2flykvhgvq3ngaqmwa1jkd1r33cw2vcqdsbsnkgd99sir0") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.2 (c (n "shadow-rs") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "0sps996yhchz43zfk9zysjpirz638s1hbhspp998bk6v9l6k7j10") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.3 (c (n "shadow-rs") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (o #t) (k 0)))) (h "19cszgq9arn52wcs3ja4ymqcrm4hcd4z7hg02ld07n87hpswfn4y") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.4 (c (n "shadow-rs") (v "0.6.4") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)))) (h "0rjx46d4j299zgjgzmk50654in5mvc9nj1vjfrjs74cky2fbb0bp") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.5 (c (n "shadow-rs") (v "0.6.5") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)))) (h "1r929dy4db7xhxf2daj6c5zc6np4prs1h6v8mgwamy2lqm03zkw1") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.6 (c (n "shadow-rs") (v "0.6.6") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)))) (h "0w7qsg4y6gxy8jzc62pph0l1frhjfdihly99wlc9ziz64x4833p4") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.7 (c (n "shadow-rs") (v "0.6.7") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)))) (h "0r46zsq388zrk3rgaxh4a8lf37rbqljfjh8y59x713qagndcbsqn") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.8 (c (n "shadow-rs") (v "0.6.8") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)))) (h "17pli9s721fa1ndx1n9dcnaxm9qj4r5g9bx3r73idjamzp6k6aj4") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.9 (c (n "shadow-rs") (v "0.6.9") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)))) (h "1wk337nl00ha20v43mijgmbjpngv0h7vgyabcpdfn4jzimwrr518") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.10 (c (n "shadow-rs") (v "0.6.10") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)))) (h "1ggc44l1n71a19p0ndmicb0nbwj6s4hd78xbjv2shz21l4dn9l6n") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.11 (c (n "shadow-rs") (v "0.6.11") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)))) (h "0ixd6lvxk2xc3iry79xs1nff83ykdby79kkfi5rxpfnv5pvkyc5q") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.12 (c (n "shadow-rs") (v "0.6.12") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)))) (h "0hrhnx5ipkx02acxkx4r0955ab80qkjym3q9wags23sbm452z2z3") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.6.13 (c (n "shadow-rs") (v "0.6.13") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)))) (h "1dp5f4ff2bgdc6agb5lk9q5qw1892kv0i4z8ggi0xs27wp74mail") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.7.0 (c (n "shadow-rs") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)))) (h "1q7l8wmxwms12g1jpclaaqbh6dw2yrclggz7m4xi935a3yzmwynm") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.7.1 (c (n "shadow-rs") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)))) (h "1b8gj94rzfjcl5rbd9isfb5sj84xvc5jfjjcgva233qdfx10k89w") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.7.2 (c (n "shadow-rs") (v "0.7.2") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)))) (h "0cmvmy2w27i5sws1118r3hi6skkwxi5s9x7lqs7aq71qb0y0m47p") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.8.0 (c (n "shadow-rs") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)))) (h "0rbmpjv74lyv8d0m8c5ssl4zzyghkjdq52l6l8lrfsfihr7b82vr") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.8.1 (c (n "shadow-rs") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)))) (h "0iy4f6yzag52qr93hjhm2l8crxhjblr91nffbdd8zzw0xchfjdc9") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.9.0 (c (n "shadow-rs") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.13.20") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)))) (h "02gpjyf79irrxa5v22sgylwvi6az4g19y56j00zsrzbbv74wmaiq") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.10.0 (c (n "shadow-rs") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)))) (h "08dyp6lb7mbkbrjjdppcy50qs06ag2f478lw03fx5y1rsvf9klmb") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.10.1 (c (n "shadow-rs") (v "0.10.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)))) (h "07b6rw7rcjp5igaswxm8w8ggmc2n54vs9xyv5klqqmsh0yx4gnky") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.11.0 (c (n "shadow-rs") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)))) (h "0jr920fa3al3x29dsd0plj9fr6wnbx3z821rvbs53s89d7irhzpl") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.12.0 (c (n "shadow-rs") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)))) (h "1wyzpb505dxwq9vg36sgjybh1i1g9yv7gl9sq7fdhj2f1bd866zd") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.13.0 (c (n "shadow-rs") (v "0.13.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tz-rs") (r "^0.6.11") (d #t) (k 0)))) (h "11lzziqz1fgd3ggr221ri7gk3gvyc40ajvzc0f9vfdbi46qqcz26") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.13.1 (c (n "shadow-rs") (v "0.13.1") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tz-rs") (r "^0.6.11") (d #t) (k 0)))) (h "1k2i2rgh69ba5597mnqzm5v2xpl8m70ya69snnbpnbhh1k9lp84j") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.14.0 (c (n "shadow-rs") (v "0.14.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tz-rs") (r "^0.6.11") (d #t) (k 0)))) (h "1zy5y5k99waan3q9i0gacgbljhdvmj09j26sw5k9i5xpm9x0xlrs") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.15.0 (c (n "shadow-rs") (v "0.15.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tz-rs") (r "^0.6.11") (d #t) (k 0)))) (h "0vciyw2vyz237bjfsaq0r447n2ib3f2b53wwxy9map5j7kn725x8") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.16.0 (c (n "shadow-rs") (v "0.16.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.2.7") (f (quote ("local"))) (k 0)))) (h "14nj0gx3v1r0xs1lmd09phlv5z5f9p80rl9d6b8kw6il999ry3bh") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.16.1 (c (n "shadow-rs") (v "0.16.1") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.3.1") (f (quote ("local"))) (k 0)))) (h "17x9m99b40c420ddh5frimch2xf7a73pbnk1cj70vjdm2nnrklxj") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.16.2 (c (n "shadow-rs") (v "0.16.2") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.4.3") (f (quote ("local" "std"))) (k 0)))) (h "0xdfjz97lk6bw1kzp92fb7j7p2m5g0alkji4j2764k4dfcd6ny0w") (f (quote (("default" "git2"))))))

(define-public crate-shadow-rs-0.16.3 (c (n "shadow-rs") (v "0.16.3") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.4.4") (f (quote ("local" "std"))) (o #t) (k 0)))) (h "0kpzi2flxrza9b85h969ahr2d8k5p6jh2m5sbrr4ym0qhk3a03lc") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.17.0 (c (n "shadow-rs") (v "0.17.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.4.4") (f (quote ("local" "std"))) (o #t) (k 0)))) (h "05q2478nhyk99z8r7z1dn8krx4dghdg3qv7pg1brgcxjrqf4hid6") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.17.1 (c (n "shadow-rs") (v "0.17.1") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.4.4") (f (quote ("local" "std"))) (o #t) (k 0)))) (h "0fzms87ysmaz3yr6n5qj4lam3aazhlq2si7vxfkxbnyi88cyaygn") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.18.0 (c (n "shadow-rs") (v "0.18.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.5.0") (o #t) (k 0)))) (h "1yi2x3mvd0q4vxf69vzrq1a31r1vsy8kn2i4gl1djkam4qvf7y4b") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.19.0 (c (n "shadow-rs") (v "0.19.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.5.0") (o #t) (k 0)))) (h "07a7rxgzirbfiyjxxs3knnx0ykrnavw40nfyzplv9vahb1wiwh3c") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.20.0 (c (n "shadow-rs") (v "0.20.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.5.0") (o #t) (k 0)))) (h "1mk6nx4bngvczn209rj8fc4blvg7dln0pln5l9ki4ii2lz90pacd") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.20.1 (c (n "shadow-rs") (v "0.20.1") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.16.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.5.0") (o #t) (k 0)))) (h "07ak117sw2hdm63q2aia605ldg36gi8qsl4y0pav7yic3scvjhh9") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.21.0 (c (n "shadow-rs") (v "0.21.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.16.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.5.0") (o #t) (k 0)))) (h "10x5fy46hpp3wz0lzpr5f3magh485bhq4j1jap7h0c47bymhfzs2") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.22.0 (c (n "shadow-rs") (v "0.22.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.17.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.5.0") (o #t) (k 0)))) (h "0x6d7r01p071y6ya6dmfcam2cidqcsjdzb1689pjvxr961myyyqm") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.23.0 (c (n "shadow-rs") (v "0.23.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.17.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.5.0") (o #t) (k 0)))) (h "0013fhbpgravpbwidrqgghg6dysacy09zf4cxjs0pzan8xq3h1cp") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.24.0 (c (n "shadow-rs") (v "0.24.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.5.0") (o #t) (k 0)))) (h "1liazgj1hj5vs1yg0j5yq4551zldms7x5crg3zqwf5fl0vs412gq") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.24.1 (c (n "shadow-rs") (v "0.24.1") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.5.0") (o #t) (k 0)))) (h "0xqcw27hf8ybxlzhnnx5k0dvb5lq6yydsjv6yrfilky9y6pqq6gr") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.25.0 (c (n "shadow-rs") (v "0.25.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.5.0") (o #t) (k 0)))) (h "1jv0zz8w6zsxjqmxlpsclrv36gliyrrcl6qhr86qb83lf5pq8pb1") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.26.0 (c (n "shadow-rs") (v "0.26.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.6.0") (f (quote ("local"))) (o #t) (k 0)) (d (n "winnow") (r "^0.5.31") (d #t) (k 2)))) (h "077ijsyw13a89z2r8a1a8qmk1misd2xyycmq2q8fx61d2viv3347") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.26.1 (c (n "shadow-rs") (v "0.26.1") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.6.0") (f (quote ("local"))) (o #t) (k 0)) (d (n "winnow") (r "^0.5.33") (d #t) (k 2)))) (h "15xrlf66f10773k9kwpvz8909akfjspyy0yy9ss665wrfs15qp1y") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.27.0 (c (n "shadow-rs") (v "0.27.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.6.0") (f (quote ("local"))) (o #t) (k 0)) (d (n "winnow") (r "^0.6") (d #t) (k 2)))) (h "09damnkwd7m0bsd7nqy9ymcjzhvpd69mmha2wnckfy69ca1gb46w") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.27.1 (c (n "shadow-rs") (v "0.27.1") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.6.0") (f (quote ("local"))) (o #t) (k 0)) (d (n "winnow") (r "^0.6") (d #t) (k 2)))) (h "0a01fm2mc7gkq5c5lsgm2l8jvl3b2gvrggvybsqinsblpbbcnq3r") (f (quote (("default" "git2" "tzdb"))))))

(define-public crate-shadow-rs-0.28.0 (c (n "shadow-rs") (v "0.28.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (o #t) (k 0)) (d (n "is_debug") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("formatting" "local-offset" "parsing"))) (d #t) (k 0)) (d (n "tzdb") (r "^0.6.0") (f (quote ("local"))) (o #t) (k 0)) (d (n "winnow") (r "^0.6") (d #t) (k 2)))) (h "1pw38f3v1d3afg1i5y23f7jkrhj84hlnw7xd8c2n93zfv9mm2x8x") (f (quote (("default" "git2" "tzdb"))))))

