(define-module (crates-io sh ad shadow-drive-py) #:use-module (crates-io))

(define-public crate-shadow-drive-py-0.0.0 (c (n "shadow-drive-py") (v "0.0.0") (h "0qkiyp0vyd42b4skgndbyxhlj2dda5g6wn4g8265swj47ainn70j")))

(define-public crate-shadow-drive-py-0.6.2 (c (n "shadow-drive-py") (v "0.6.2") (d (list (d (n "concat-arrays") (r "^0.1.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "shadow-drive-sdk") (r "^0.6.2") (d #t) (k 0)) (d (n "tokio") (r "^1.14.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-scoped") (r "^0.2.0") (d #t) (k 0)))) (h "1yaipi7p7dq1x4g4wgrjffi5ns4sdwij19kdmdsa7j8yrip4hkxa")))

