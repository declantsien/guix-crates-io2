(define-module (crates-io sh ad shadow-clone) #:use-module (crates-io))

(define-public crate-shadow-clone-0.0.1 (c (n "shadow-clone") (v "0.0.1") (h "05wl1m5bx4w0jk0ahq918rxjxl27mfdz5n0d81sdph8rx3mrkxhx")))

(define-public crate-shadow-clone-0.1.0 (c (n "shadow-clone") (v "0.1.0") (h "0ycpw2ry0j5ybcd0r20ihashz3pm7n8qvkpc4cx4kgb4wl0ygs47")))

(define-public crate-shadow-clone-0.1.1 (c (n "shadow-clone") (v "0.1.1") (h "0g601b0n3v07j849xpqxk9b4xwy7ac7cqm9zm8gaywwv8vaqqxmd")))

(define-public crate-shadow-clone-1.0.0 (c (n "shadow-clone") (v "1.0.0") (h "1qj6356y106cpmxbgksxj278bpk89902b9ij9045y8y8ijdz2hn2")))

(define-public crate-shadow-clone-1.1.0 (c (n "shadow-clone") (v "1.1.0") (h "0rympgl6ds2zizxidildkllsdgnviwgrgpds9w5bkq8av2xjzl4x")))

(define-public crate-shadow-clone-1.2.0 (c (n "shadow-clone") (v "1.2.0") (h "1y2j9zc5226rlcpng2cv8p970vzhyqp73b1ab5bz7qw624awhpl8")))

(define-public crate-shadow-clone-1.2.1 (c (n "shadow-clone") (v "1.2.1") (h "1psazsas6qa9a4p1n91a97ibja2a3zzs664rdn8h1lk5c4pxdqwr")))

