(define-module (crates-io sh ad shades-edsl) #:use-module (crates-io))

(define-public crate-shades-edsl-0.1.0 (c (n "shades-edsl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "1xf5j17gsipglxhnk11ngi3bd8l833ssb8dsxn8gpjj6r0gahwx7")))

