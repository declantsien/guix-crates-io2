(define-module (crates-io sh ad shaderc-sys) #:use-module (crates-io))

(define-public crate-shaderc-sys-0.4.0 (c (n "shaderc-sys") (v "0.4.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12yyspfzh8v7id23wk6lki353dqddz1xcpf5cjy32f6mi06v1lh8") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc")))) (y #t) (l "shaderc")))

(define-public crate-shaderc-sys-0.4.1 (c (n "shaderc-sys") (v "0.4.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ph59rxbjz6fyiv6ch9bw6crgr1kvjk495fcki9q5dc9fhcgsjy4") (f (quote (("default" "build-native-shaderc") ("build-native-shaderc")))) (y #t) (l "shaderc")))

(define-public crate-shaderc-sys-0.5.0 (c (n "shaderc-sys") (v "0.5.0") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0iycadfrj7y520bksyqd844480bphix75wshx68gbbjkp8j2mcwp") (f (quote (("inverted-no-defaults") ("check-inverted-no-defaults") ("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.5.1 (c (n "shaderc-sys") (v "0.5.1") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0d3fibbrqibx9smqawa7w1iqm3x63m8fvxjickbdd99mw7nh8g8s") (f (quote (("inverted-no-defaults") ("check-inverted-no-defaults") ("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.6.0 (c (n "shaderc-sys") (v "0.6.0") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1i7xpg6jm1wqq80ax50rpp49ahq5b5igcwdcnwdpl268r5x651g7") (f (quote (("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.6.1 (c (n "shaderc-sys") (v "0.6.1") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qizzaamrxn32fiwbvq0jwlcksmx3adwg8ag1jdlqswkxfplm1wn") (f (quote (("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.6.2 (c (n "shaderc-sys") (v "0.6.2") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a657ifn0x0cb510d2w37qx3ljhajws48kp5nnr8qy885dqmq1rh") (f (quote (("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.6.3 (c (n "shaderc-sys") (v "0.6.3") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zkksh93i8lxm0aj85fz12qrb9x37x88gdgskm68hck75p3df4jv") (f (quote (("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.7.0 (c (n "shaderc-sys") (v "0.7.0") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1afy1cklyccly78fhbwvqg20fwzqi38ipa1k4242zf240bw7b4f8") (f (quote (("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.7.1 (c (n "shaderc-sys") (v "0.7.1") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gf80yz2hsk3d9ny7g7rch49iai8cd8xbzwh1rwgbsclhi1818rc") (f (quote (("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.7.2 (c (n "shaderc-sys") (v "0.7.2") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17qarrarj8s94cpwj28763rqzxdwqzsfcxixc7xz4gal9pdn4sfs") (f (quote (("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.7.3 (c (n "shaderc-sys") (v "0.7.3") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yfd338chxak27387zdqbhdwika3br4760r5cmx03wi5pp06xmwb") (f (quote (("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.7.4 (c (n "shaderc-sys") (v "0.7.4") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fwqzrg40f1vq4s1h65c5gls247m4k4vjil8llrhv0z5zijk8vx3") (f (quote (("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.8.0 (c (n "shaderc-sys") (v "0.8.0") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 1)))) (h "1gyhr242ivy0wkfmbrr7h84i3lff8p58mfy0zc53s5zqa634k51p") (f (quote (("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.8.1 (c (n "shaderc-sys") (v "0.8.1") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 1)))) (h "1668zyxl4l4kj5sxki3md02bvx3g8sxrlh1hjizak86qyyfw70d2") (f (quote (("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.8.2 (c (n "shaderc-sys") (v "0.8.2") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 1)))) (h "0r6gainx6v0ak6yrc9kmnnx5v4yzml2746blrppxdzzwzwwq93qy") (f (quote (("build-from-source")))) (l "shaderc")))

(define-public crate-shaderc-sys-0.8.3 (c (n "shaderc-sys") (v "0.8.3") (d (list (d (n "cmake") (r "^0.1.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 1)))) (h "1xzpf9i3k2r845xv86bvy9h4j09dr93qbjir1wq9c8g21wj0s4kk") (f (quote (("prefer-static-linking") ("build-from-source")))) (l "shaderc")))

