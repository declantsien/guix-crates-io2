(define-module (crates-io sh ad shadercrab) #:use-module (crates-io))

(define-public crate-shadercrab-0.1.0 (c (n "shadercrab") (v "0.1.0") (d (list (d (n "glium") (r "^0.31.0") (d #t) (k 0)))) (h "1y8rdz64bsnniqaywf4grqjxclyxmvc28hm5qh0z33srmar695z6")))

(define-public crate-shadercrab-0.1.1 (c (n "shadercrab") (v "0.1.1") (d (list (d (n "glium") (r "^0.31.0") (d #t) (k 0)))) (h "0is2px0f0birqfra39k78v3ngddm69zg11w1zccybva2809vk4yg")))

(define-public crate-shadercrab-0.1.2 (c (n "shadercrab") (v "0.1.2") (d (list (d (n "glium") (r "^0.31.0") (d #t) (k 0)))) (h "0fwgm101l1s50w8q1j62c7mzfwkmv897m61vq99dpchv1c0wdzlb")))

