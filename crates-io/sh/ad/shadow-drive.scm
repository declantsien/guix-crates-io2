(define-module (crates-io sh ad shadow-drive) #:use-module (crates-io))

(define-public crate-shadow-drive-0.0.0 (c (n "shadow-drive") (v "0.0.0") (h "18wpfssqpzyycw9d0yh265wqcbhmvzsmdkwqwivh2hpdglvchxww")))

(define-public crate-shadow-drive-0.6.3 (c (n "shadow-drive") (v "0.6.3") (h "0cbicl22m8jir26cc5m6nc36gyffjpnfb9p26ws2900b09kkqyh3")))

(define-public crate-shadow-drive-0.7.0 (c (n "shadow-drive") (v "0.7.0") (h "050k1znyw3gyy2dakr5rf5g12fcc4vwq293nl7jqky9bkkqxyyf5")))

(define-public crate-shadow-drive-0.7.1 (c (n "shadow-drive") (v "0.7.1") (h "0bbqpjylfppn5jr524ikm1mmbwv855nqvqfy2cr4gv2c2zxw4xrs")))

