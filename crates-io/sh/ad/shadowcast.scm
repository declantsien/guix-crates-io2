(define-module (crates-io sh ad shadowcast) #:use-module (crates-io))

(define-public crate-shadowcast-0.1.0 (c (n "shadowcast") (v "0.1.0") (d (list (d (n "direction") (r "^0.13") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "03n17s48cg408ln0ib9b4zydjm64lmazhnddx1x9cnsvvi1dm8xb")))

(define-public crate-shadowcast-0.1.1 (c (n "shadowcast") (v "0.1.1") (d (list (d (n "direction") (r "^0.13") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)))) (h "0pjchyv3b8x7qf94qpjk2gq9990m04da081qsy3alm587a7pzan8")))

(define-public crate-shadowcast-0.2.0 (c (n "shadowcast") (v "0.2.0") (d (list (d (n "direction") (r "^0.14") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)))) (h "1h7lf2x484v1ji6z5l0a6v8j32kar29ihbi04j1zg72hd0sdncw9")))

(define-public crate-shadowcast-0.3.0 (c (n "shadowcast") (v "0.3.0") (d (list (d (n "direction") (r "^0.15") (d #t) (k 0)) (d (n "grid_2d") (r "^0.8") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)))) (h "1ld74ssy4swh2fv1yk10yihm0bifylzvjkyhg1mbarjih7w820ih")))

(define-public crate-shadowcast-0.4.0 (c (n "shadowcast") (v "0.4.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "04vw4rg7nimfww681sjd4mlqhl9j6389mrgvcl3fajy2g7zz8gj4")))

(define-public crate-shadowcast-0.5.0 (c (n "shadowcast") (v "0.5.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.12") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0lhnmzx0vwlz465h186drnsjyv2fbk17aw4krkxb7c7g9v5lg2ad") (f (quote (("serialize" "direction/serialize" "coord_2d/serialize"))))))

(define-public crate-shadowcast-0.5.1 (c (n "shadowcast") (v "0.5.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.12") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1wrs3fpw0813nhy6y10j1cmcnjfv6a3lv2a2dgbppzmqkjdixb8d") (f (quote (("serialize" "direction/serialize" "coord_2d/serialize"))))))

(define-public crate-shadowcast-0.5.2 (c (n "shadowcast") (v "0.5.2") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.12") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0b2mpscrfpxn4fv0sl09y2s1zfbj0q4f6c6mllzbxqbjjp9k7jsg") (f (quote (("serialize" "direction/serialize" "coord_2d/serialize"))))))

(define-public crate-shadowcast-0.5.3 (c (n "shadowcast") (v "0.5.3") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.12") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "10rna4ps2k4kh2i4aydn804yqaf08sigj8v35jqamy6vkis7wvfa") (f (quote (("serialize" "direction/serialize" "coord_2d/serialize"))))))

(define-public crate-shadowcast-0.5.4 (c (n "shadowcast") (v "0.5.4") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.12") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "17qias645j9ih1mlvqbbig0lnz2rr4d8b3xdarcn9k2xw31z4sg1") (f (quote (("serialize" "serde" "direction/serialize" "coord_2d/serialize"))))))

(define-public crate-shadowcast-0.5.5 (c (n "shadowcast") (v "0.5.5") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.12") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1k2mzcvr5ba12x9yyq6mmbs97krlq2i4sh3v1870agh1ghhsaypy") (f (quote (("serialize" "serde" "direction/serialize" "coord_2d/serialize"))))))

(define-public crate-shadowcast-0.5.6 (c (n "shadowcast") (v "0.5.6") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.12") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "00in962ilb5rznidyzqgh8grlw6ggakz8c2k644cacdcqjmxrv51") (f (quote (("serialize" "serde" "direction/serialize" "coord_2d/serialize"))))))

(define-public crate-shadowcast-0.6.0 (c (n "shadowcast") (v "0.6.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1w2kkfiyc00nnkpgirbv0vwmvxv3jk3vpx84qm8f38w3svwdm0d0") (f (quote (("serialize" "serde" "direction/serialize" "coord_2d/serialize"))))))

(define-public crate-shadowcast-0.6.1 (c (n "shadowcast") (v "0.6.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "15kmv9hq1wisjnc3cihagpibvkrmj6zb52idrk8bn4915mgi3kmw") (f (quote (("serialize" "serde" "direction/serialize" "coord_2d/serialize"))))))

(define-public crate-shadowcast-0.7.0 (c (n "shadowcast") (v "0.7.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1jrpl9rnjf5d0marfhcqw6k9vrbxhix7frg2gy2q2qnnnwfrasf6") (f (quote (("serialize" "serde" "direction/serialize" "coord_2d/serialize"))))))

(define-public crate-shadowcast-0.7.1 (c (n "shadowcast") (v "0.7.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1nsf2m3czj376s8naq0mf0xrkyjwpzqbmmla86vcwcbxl7lbnfgz") (f (quote (("serialize" "serde" "direction/serialize" "coord_2d/serialize"))))))

(define-public crate-shadowcast-0.8.0 (c (n "shadowcast") (v "0.8.0") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "direction") (r "^0.18") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0knvg1r6y4a56gsqzzjq6cnqs622p93107n1s1292idp7r3r6nv6") (f (quote (("serialize" "serde" "direction/serialize" "coord_2d/serialize"))))))

(define-public crate-shadowcast-0.8.1 (c (n "shadowcast") (v "0.8.1") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "direction") (r "^0.18") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1mbjq32hq9zv6f21lck2szabksjldcj4k1nmw5sggzwlpk7sc624") (f (quote (("serialize" "serde" "direction/serialize" "coord_2d/serialize"))))))

