(define-module (crates-io sh el shell-rs) #:use-module (crates-io))

(define-public crate-shell-rs-0.1.0 (c (n "shell-rs") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "nc") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1gdks4gvv7z0bq1d8spv8wxz8sbnry73gwm7p4rh1zxd7xgmzqj6")))

(define-public crate-shell-rs-0.1.1 (c (n "shell-rs") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "nc") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "17x8k4cmq0fv78xjj8jmx7rm57py4igccxbg1jzjapxc2nl2qs7w")))

(define-public crate-shell-rs-0.1.2 (c (n "shell-rs") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "nc") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0g5547zddjr7s8jiqjnipnkalh5nbw5z5h4qjczp9hmm5xdq203r")))

(define-public crate-shell-rs-0.1.3 (c (n "shell-rs") (v "0.1.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "nc") (r "^0.7.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1mh4yzkqb550scgmkhs9qw64d1m5x8vws2psmiqfc43yggwnai99")))

(define-public crate-shell-rs-0.2.1 (c (n "shell-rs") (v "0.2.1") (d (list (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "nc") (r "^0.7.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0nlga61zqslmgzj1hbwvp96n7y36q1pcx97ivrr591vd38fzssgp")))

(define-public crate-shell-rs-0.2.2 (c (n "shell-rs") (v "0.2.2") (d (list (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "nc") (r "^0.7.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "0srx81pn93d3k7qqzcr74770dmfkhdaf8q4m2mm4l090hlc3zkk3")))

(define-public crate-shell-rs-0.2.3 (c (n "shell-rs") (v "0.2.3") (d (list (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "nc") (r "^0.7.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "0yy1x9c0xpw4i2xbgv7i4i1ax9npfknc9lld7wfkv7nf4xsbbsa9") (f (quote (("std") ("default" "std"))))))

(define-public crate-shell-rs-0.2.4 (c (n "shell-rs") (v "0.2.4") (d (list (d (n "blake2b_simd") (r "^0.5.11") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "nc") (r "^0.7.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.6") (d #t) (k 0)))) (h "0lpczbiznh8b4p6d0hj8m64i6m9d5xy87mc6kfmd8jx31xna84vr") (f (quote (("std") ("default" "std"))))))

(define-public crate-shell-rs-0.2.5 (c (n "shell-rs") (v "0.2.5") (d (list (d (n "blake2b_simd") (r "^0.5.11") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "nc") (r "^0.7.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.6") (d #t) (k 0)))) (h "1ykbq11fcw9kygdpjnlbp9r433asy0xbis0kmbhsyxgxs6hick7y") (f (quote (("std") ("default" "std"))))))

(define-public crate-shell-rs-0.2.6 (c (n "shell-rs") (v "0.2.6") (d (list (d (n "blake2b_simd") (r "^1.0.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "nc") (r "^0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0hz4cd36cqzqgnvbnjh1jgfgij3l90cffg8mxg1g27pvn97q8ych") (f (quote (("std") ("default" "std"))))))

