(define-module (crates-io sh el shelter-block) #:use-module (crates-io))

(define-public crate-shelter-block-0.1.0 (c (n "shelter-block") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "blake3") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "multibase") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7") (d #t) (k 0)) (d (n "xid") (r "^1.0") (d #t) (k 0)))) (h "1mj0r7y4qx1nics9rb2gr3jn4xdamrbz3sga42pi1ihvxfp91krj") (r "1.66")))

