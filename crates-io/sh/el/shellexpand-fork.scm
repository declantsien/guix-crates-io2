(define-module (crates-io sh el shellexpand-fork) #:use-module (crates-io))

(define-public crate-shellexpand-fork-2.1.1 (c (n "shellexpand-fork") (v "2.1.1") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)))) (h "1vl55m9y5zj4zsglf8iirfs44vqavnx7fqgyimykgsh05kbrd6sv")))

(define-public crate-shellexpand-fork-2.1.2 (c (n "shellexpand-fork") (v "2.1.2") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "1aj3v91c87mh42asgbbp51c92pnl6bjfyivvz4k8sdfz19cn07ih")))

