(define-module (crates-io sh el shell2batch) #:use-module (crates-io))

(define-public crate-shell2batch-0.1.0 (c (n "shell2batch") (v "0.1.0") (h "083vfhl94vrbjqa75g7nwqrlnvwkq3q73sd0xy7ng03isg507w1b") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.1 (c (n "shell2batch") (v "0.1.1") (h "1l0b9ypanqx14c6pwb4hm4cidhaylxkwmiya8567pg4532qg7c2s") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.2 (c (n "shell2batch") (v "0.1.2") (d (list (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0wxdgwh4b43xd4s1d1jwv1385nwd4kc8q97hf5llx0azjgp43b53") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.3 (c (n "shell2batch") (v "0.1.3") (d (list (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0h1kzqw4fh3yj7rwcc9nwia81wfjjw4zafy9wkvhk5mh6fysvqa0") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.4 (c (n "shell2batch") (v "0.1.4") (d (list (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1b8jphp8jib0sspngc78f7155qdgpvfv2j9wgmnsmly4ixfsd7z6") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.5 (c (n "shell2batch") (v "0.1.5") (d (list (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "076s6g4zm12nvix4ihhmplcwl5ali18ps76hba26gcli6a260akg") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.6 (c (n "shell2batch") (v "0.1.6") (d (list (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "117y20apin9xv2a1ay96ablq0hahc0969z4hs0nyblsyn16wrpim") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.7 (c (n "shell2batch") (v "0.1.7") (d (list (d (n "regex") (r "^0.2.3") (d #t) (k 0)))) (h "0fs61nsx3vldq6ai15lgf59jaajh47z7wghy92mfza48jz40xqmf") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.8 (c (n "shell2batch") (v "0.1.8") (d (list (d (n "regex") (r "^0.2.3") (d #t) (k 0)))) (h "0mk199yqm6c6lprwji0x2jyvnrzl6nbry3vmil4bpp1869100n1y") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.9 (c (n "shell2batch") (v "0.1.9") (d (list (d (n "regex") (r "^0.2.3") (d #t) (k 0)))) (h "0l5xgdimll7rnkzvya5kp40kishcm1680pian4m8c8rpnzh1qspm") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.10 (c (n "shell2batch") (v "0.1.10") (d (list (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "1mnlqakhnw02niq221d0f8psnl9yyl6hp7imfkv3s9cgl7wfnpnc") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.11 (c (n "shell2batch") (v "0.1.11") (d (list (d (n "regex") (r "^0.2.6") (d #t) (k 0)))) (h "1axa9difdidw8x2x9551h0vk3623s8jizprhrlwmwajlgp915gds") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.12 (c (n "shell2batch") (v "0.1.12") (d (list (d (n "regex") (r "^0.2.6") (d #t) (k 0)))) (h "0i7y8zp94w0mnyyssbh9cgpc1iip5bmb24jj2mhgyc6v8321v3ni") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.13 (c (n "shell2batch") (v "0.1.13") (d (list (d (n "regex") (r "^0.2.7") (d #t) (k 0)))) (h "0qdvzy1cn13dkigarz4c1f0qg280gayvdyc6aklwmjbr8ciysaww") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.14 (c (n "shell2batch") (v "0.1.14") (d (list (d (n "regex") (r "^0.2.7") (d #t) (k 0)))) (h "1v4mh3ry15gn669khay53vgwx1k0cyy270gy05j9ss1kmykly5gp") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.15 (c (n "shell2batch") (v "0.1.15") (d (list (d (n "regex") (r "^0.2.8") (d #t) (k 0)))) (h "0h416wg3nd4nvgsi055cfas5b53mm0ilmn2sampnxc1xbda14mqy") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.16 (c (n "shell2batch") (v "0.1.16") (d (list (d (n "regex") (r "^0.2.9") (d #t) (k 0)))) (h "0rk3czlk6mfi7bwf72b3p5045zvmck4mmriksmfw6f84jjcw2mv5") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.17 (c (n "shell2batch") (v "0.1.17") (d (list (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "1gn1h69fl1g6vw6fkj5i7cf86g6zshfs3f281vppbzsvrq6z645h") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.18 (c (n "shell2batch") (v "0.1.18") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1gsysr9wdg3ps25b7zycffyhmh3ib9azc4xrwvds3b5i29i6njli") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.19 (c (n "shell2batch") (v "0.1.19") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0k7rzjdcpmzn7llcfqyg6fwvf6b90a03pdkj0wg1q93s6kvgvcza") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.20 (c (n "shell2batch") (v "0.1.20") (d (list (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1rvbf0pm96y1sg09mbcwd5l52qdzgkzbig1fcqfl191x8k1qwnx7") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.21 (c (n "shell2batch") (v "0.1.21") (d (list (d (n "regex") (r "^1.0.1") (d #t) (k 0)))) (h "111gc28g7yk8a1fi8lk2j7igcbnhakcn13phdffl5kc60rrdrqpz") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.22 (c (n "shell2batch") (v "0.1.22") (d (list (d (n "regex") (r "^1.0.2") (d #t) (k 0)))) (h "1wy7nflmz5kd8wnv0xsv64pjcpl91wy6phzczvzz15ssfxa6fv1f") (f (quote (("default"))))))

(define-public crate-shell2batch-0.1.23 (c (n "shell2batch") (v "0.1.23") (d (list (d (n "regex") (r "^1.0.2") (d #t) (k 0)))) (h "0j6yy2zamv5xg4czb1f2ilbylwrlnbr1cx5s1llw8pk231gq3d6j") (f (quote (("default"))))))

(define-public crate-shell2batch-0.2.0 (c (n "shell2batch") (v "0.2.0") (d (list (d (n "regex") (r "^1.0.4") (d #t) (k 0)))) (h "0gvjjx5arn3qia2cc44jz9hkk1szhws36xxzl8f16l06ckll8wq9") (f (quote (("default"))))))

(define-public crate-shell2batch-0.2.1 (c (n "shell2batch") (v "0.2.1") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "1hm8p9xk0apaylg8qf5qdz7aa7zihhw7wg582whdcdrsx60xpx1z") (f (quote (("default"))))))

(define-public crate-shell2batch-0.2.2 (c (n "shell2batch") (v "0.2.2") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "0c81lc0zf8xxa39yjl97mgd7ipqi6hnrv203szk92svpp6h2xlfn") (f (quote (("default"))))))

(define-public crate-shell2batch-0.2.3 (c (n "shell2batch") (v "0.2.3") (d (list (d (n "regex") (r "^1.0.6") (d #t) (k 0)))) (h "0ciqkhgwnc8c7gcai1aidxicmb8ib895h47wqxiwz8abbr6r4zpf") (f (quote (("default"))))))

(define-public crate-shell2batch-0.2.4 (c (n "shell2batch") (v "0.2.4") (d (list (d (n "regex") (r "^1.0.6") (d #t) (k 0)))) (h "11nk6dii24nfrg91y77yca22pmq5h7hb292g9hc6yccy5m8win0k")))

(define-public crate-shell2batch-0.2.5 (c (n "shell2batch") (v "0.2.5") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "05ggm8jbvvp3hbsfp5441qcaq32qim6f8j4492z19z7hx87c6kdm")))

(define-public crate-shell2batch-0.2.6 (c (n "shell2batch") (v "0.2.6") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "0ilx07s1f2f7dpfyl6xcdycsq43fd19ngi2abi5wsa7r8n0r3dmd")))

(define-public crate-shell2batch-0.2.7 (c (n "shell2batch") (v "0.2.7") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1qzs949dgjkgf1f1f0d75xydk68ypz2v3i8hza0iq5wr2xa102i0")))

(define-public crate-shell2batch-0.3.0 (c (n "shell2batch") (v "0.3.0") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "0h1p7s6fimgkr4c04h1c987493sm35dzbcq0mgcgbhp8xx566rvb")))

(define-public crate-shell2batch-0.3.1 (c (n "shell2batch") (v "0.3.1") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1iyp689lxl9zc7x839n91bj5lv36qn73467y4iz6f4f9hc9vbv78")))

(define-public crate-shell2batch-0.4.0 (c (n "shell2batch") (v "0.4.0") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1vwxcqbrg204bz9rs5mh0yavx59cmkxdgyw3jwz9d2mr4bcm943n")))

(define-public crate-shell2batch-0.4.1 (c (n "shell2batch") (v "0.4.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0sr6ijpidphs66fymn745c619iry544k6jpfhvdp6v4h5cscwypp")))

(define-public crate-shell2batch-0.4.2 (c (n "shell2batch") (v "0.4.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0r9zfhxmqnrnyk7g590333szfpsjijs2wfwy7ish240w6pp54nhq")))

(define-public crate-shell2batch-0.4.3 (c (n "shell2batch") (v "0.4.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0zz1sajylw79k86bn8fc812658jm2vrrayxlsby495z95cgqi9gp")))

(define-public crate-shell2batch-0.4.4 (c (n "shell2batch") (v "0.4.4") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1a84368dm9mqnaw35r8yb3slqgs71w3y17kqnkygylka3w7frx0l")))

(define-public crate-shell2batch-0.4.5 (c (n "shell2batch") (v "0.4.5") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "03i4nsadm8wd9wmd1k0j1jpaspl3vw1wi9i173cbwnhwi867s31s")))

