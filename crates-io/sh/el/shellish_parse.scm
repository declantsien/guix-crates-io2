(define-module (crates-io sh el shellish_parse) #:use-module (crates-io))

(define-public crate-shellish_parse-1.0.0 (c (n "shellish_parse") (v "1.0.0") (h "1wbf437yp4abpqa2c4b56n5yrhbn6nzjw9709jb4fw4a48yx0dzr")))

(define-public crate-shellish_parse-2.0.0 (c (n "shellish_parse") (v "2.0.0") (h "0y1irnmc66y9wr4wdfqgfv7amdfz4fbrqb48k8jxf734ln7glhb6")))

(define-public crate-shellish_parse-2.1.0 (c (n "shellish_parse") (v "2.1.0") (h "06wvd6lflwak2mc6vxsbszjc0zwbsqa3i01ajd2cvxqlf19wdmn5")))

(define-public crate-shellish_parse-2.2.0 (c (n "shellish_parse") (v "2.2.0") (h "0wwrsx76n76b285ll71rp69hln7kl6xkdf9pdxb2h6k8ml9bjaac")))

(define-public crate-shellish_parse-2.2.0-1 (c (n "shellish_parse") (v "2.2.0-1") (h "1axzp584ar3wbmzz7vv71ls13qwi8wfrnhshwq0crxjlrqmqh7fc")))

