(define-module (crates-io sh el shelf) #:use-module (crates-io))

(define-public crate-shelf-0.1.0 (c (n "shelf") (v "0.1.0") (h "1d2nl2dvxnrcdgnvrc2sa41rzi5hn2mv9baajibqq264cwh8zswr")))

(define-public crate-shelf-0.1.1 (c (n "shelf") (v "0.1.1") (h "0ix1ciszr10mk9fvznpv01nc3znav2hcyb08hiy7jrsl6k3xc69j")))

(define-public crate-shelf-0.2.0 (c (n "shelf") (v "0.2.0") (h "0ckhk35y3v4d3i54sf4y7kj60bjbkf6ddabvs7hpjdwd4l147352")))

(define-public crate-shelf-0.2.1 (c (n "shelf") (v "0.2.1") (h "0zcg3gv6x162zqkhclsqs0cq1pkcfizy5lns6p1g7f7bcxddwz8c")))

(define-public crate-shelf-0.2.2 (c (n "shelf") (v "0.2.2") (h "14lgaia19fa1wvggskv4gnzq8wryc5jcjams0rf05b0kg2gk0nkr")))

