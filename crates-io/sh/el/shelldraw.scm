(define-module (crates-io sh el shelldraw) #:use-module (crates-io))

(define-public crate-shelldraw-0.1.0 (c (n "shelldraw") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0npgs08hvi88f51zhkn2f6cwj6hakm342650d7rbncfd5pw22ki8")))

