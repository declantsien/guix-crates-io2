(define-module (crates-io sh el shell_helper) #:use-module (crates-io))

(define-public crate-shell_helper-0.1.0 (c (n "shell_helper") (v "0.1.0") (h "199y4z4gx581agf0dh180q8ll3b0x8vcdwn5svc0vm8papmzi1xs")))

(define-public crate-shell_helper-0.2.0 (c (n "shell_helper") (v "0.2.0") (h "192f1i8d2m4cjidnj8zyckvidl997shc4z6dsq15nv3cwdp25bas")))

(define-public crate-shell_helper-0.2.1 (c (n "shell_helper") (v "0.2.1") (h "0gfbq2jsr4jka21p8amb419lgx6q98kiyjzs1b6989g0z57c33z7")))

(define-public crate-shell_helper-0.2.2 (c (n "shell_helper") (v "0.2.2") (h "0ymjw2rmb6lripwc9x3lbw2amlbm10m5gchipw6268qjg455jgy9")))

(define-public crate-shell_helper-0.2.3 (c (n "shell_helper") (v "0.2.3") (h "0zm927mzdm7w7b3xniz8qvbjhswq90fm97afcsa2p6bsv1b1xf90")))

(define-public crate-shell_helper-0.2.4 (c (n "shell_helper") (v "0.2.4") (h "09k81b9dz60dry7ra2jk90jypszrxcj6nn0bd39nfh6y2f8g6bhr")))

(define-public crate-shell_helper-0.2.5 (c (n "shell_helper") (v "0.2.5") (h "0dk4nnbqwk7fdlwl02s40yh8nx8ab3j54npxmp6jyrwl8s1pfbmf")))

(define-public crate-shell_helper-0.2.6 (c (n "shell_helper") (v "0.2.6") (h "1wljg8434bw82j9r8779xc8sl5phabvdrgsq1bw4gr3g2v1pva0r")))

(define-public crate-shell_helper-0.2.7 (c (n "shell_helper") (v "0.2.7") (h "1bd0lswzicg2ks7wcp242hsc54hj7rddf8qgaaj2m0d71vsmqci0")))

(define-public crate-shell_helper-0.2.8 (c (n "shell_helper") (v "0.2.8") (h "01ahmmcvh3lf1m5qnzxis2a65gkmsb3n22yi6839i8g545j5dggy")))

(define-public crate-shell_helper-0.2.9 (c (n "shell_helper") (v "0.2.9") (d (list (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)))) (h "17prywjvs8nn872xlsbwyq2jp0594dph49j0x3baz4vd2fvkg8bl")))

(define-public crate-shell_helper-0.2.10 (c (n "shell_helper") (v "0.2.10") (d (list (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)))) (h "1vigj8yw0p8lfrx4f6fx16v5v4l20jziylbs3qzpjqqnjzwhi6di")))

(define-public crate-shell_helper-0.2.11 (c (n "shell_helper") (v "0.2.11") (d (list (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)))) (h "1kmhk2nj45vpl3yw55006whbr5pnap0ahb238pbxbs0fm8a84ghq")))

(define-public crate-shell_helper-0.2.12 (c (n "shell_helper") (v "0.2.12") (d (list (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)))) (h "0g0m6ywdv99h0r213mfd4xsag1gihbi3a3dcfwpylzgjgm0zpw29")))

