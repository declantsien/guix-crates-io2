(define-module (crates-io sh el shell_creator) #:use-module (crates-io))

(define-public crate-shell_creator-0.1.0 (c (n "shell_creator") (v "0.1.0") (h "1knhzky6qqc2p8msbspq67sba8mn3yi9vkijbnby6hkll2rlsdfd")))

(define-public crate-shell_creator-0.1.1 (c (n "shell_creator") (v "0.1.1") (h "15gfpycfdzkbnhv44rb2l4qsa7gmq3dkg0kfy3bh31062k9mrn7a")))

