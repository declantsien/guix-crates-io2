(define-module (crates-io sh el shelter-storage) #:use-module (crates-io))

(define-public crate-shelter-storage-0.1.0 (c (n "shelter-storage") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "moka") (r "^0.11") (d #t) (k 0)) (d (n "orion") (r "^0.17") (d #t) (k 0)) (d (n "redis") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1zd70nv4cd19j00kwp0pj8ifnc6vzv40q4hy4x8h8ha6minarq1j") (r "1.66")))

