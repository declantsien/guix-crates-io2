(define-module (crates-io sh el shellharden) #:use-module (crates-io))

(define-public crate-shellharden-3.1.0 (c (n "shellharden") (v "3.1.0") (h "04bdfy409zgqddmfr6q9irmc8xkv6jdn7w4as7plbgpfiaqa4qv6")))

(define-public crate-shellharden-3.2.0 (c (n "shellharden") (v "3.2.0") (h "0qjkvd2f3gbhgmypzbjcs27cxfpc2qdbmwg17vvn1kxsp1f4iwc7")))

(define-public crate-shellharden-4.0.0 (c (n "shellharden") (v "4.0.0") (h "1bry0jgb1nwqsk5yqgvmk6j9cya8hjf24xw6p886mv7f0c6i7nj9")))

(define-public crate-shellharden-4.0.1 (c (n "shellharden") (v "4.0.1") (h "1dvnlkdn5nb55wwacw3aga0skdqds72br90pkz05kn5px6w0w10s")))

(define-public crate-shellharden-4.1.0 (c (n "shellharden") (v "4.1.0") (h "0k217iji5caxky6ajzkz0rhps012nvknwfk9xkhjnbjy3mpv6307")))

(define-public crate-shellharden-4.1.1 (c (n "shellharden") (v "4.1.1") (h "1msvarcr118x89nrw23zfbrv0cz4skl8ivh7yjkpwmgpf4p5mad2")))

(define-public crate-shellharden-4.1.2 (c (n "shellharden") (v "4.1.2") (h "06hlzg4xkd13k8nrwqmir8azlkgm6k4bz7lr25nmk1f120ayird7")))

(define-public crate-shellharden-4.1.3 (c (n "shellharden") (v "4.1.3") (h "1wjrgx56znpwxwn30lr9cgilla6fwrc05k98lc2v6w7d8mb58gw0")))

(define-public crate-shellharden-4.2.0 (c (n "shellharden") (v "4.2.0") (h "065gk16am0nmmp4wpqvjdp3rv3fx4v8rbd9adyl9vizm3nigqfv4")))

(define-public crate-shellharden-4.3.0 (c (n "shellharden") (v "4.3.0") (h "19h8w5axa49rzfvc70cjdym323pd7zh98iqhn46qq4sv1448l8gi")))

(define-public crate-shellharden-4.3.1 (c (n "shellharden") (v "4.3.1") (h "0wc2rj0d6s4m8l8p105fp58zici517kc12xmbkq6yibnv02bdv2k")))

