(define-module (crates-io sh el shellexpand-utils) #:use-module (crates-io))

(define-public crate-shellexpand-utils-0.1.0 (c (n "shellexpand-utils") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1") (f (quote ("full"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05b20vbv9gx23w7ylmdq5sx3bwsa1yfr4fg794q0sx3jbb8i8hdl")))

(define-public crate-shellexpand-utils-0.2.0 (c (n "shellexpand-utils") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1") (f (quote ("full"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g41z3vc3s715g01j3i1xqw8j2n315bvq58m2yhngp60a3xqjdxb")))

(define-public crate-shellexpand-utils-0.2.1 (c (n "shellexpand-utils") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1") (f (quote ("full"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1x9rwkrkhdaylw9sh2029bpfn5v3pcb61v20pv8ki6sjjcv8ynn1")))

