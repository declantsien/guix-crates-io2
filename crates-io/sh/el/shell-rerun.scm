(define-module (crates-io sh el shell-rerun) #:use-module (crates-io))

(define-public crate-shell-rerun-0.1.0 (c (n "shell-rerun") (v "0.1.0") (d (list (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)))) (h "10k7ssa5nd7vc8bdla5xjr13p80mx28g8z1g70xlp0s3yqhwd2cr")))

(define-public crate-shell-rerun-0.1.1 (c (n "shell-rerun") (v "0.1.1") (d (list (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)))) (h "13cbb7y8xbyyaqyknli4xyh5whv5xb0766rkpv6pwjfzws1mjg8a")))

(define-public crate-shell-rerun-0.1.2 (c (n "shell-rerun") (v "0.1.2") (d (list (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)))) (h "08xilajqlxg5169465hibm0sj56bbgdnar57h2i760fkab0qf3p5")))

