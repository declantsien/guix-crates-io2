(define-module (crates-io sh el shelfie) #:use-module (crates-io))

(define-public crate-shelfie-1.0.0 (c (n "shelfie") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)))) (h "11qayk30ry9y683vp9fjsmp7xaqbcwvy2w5na57x9n90bbm9skqi") (f (quote (("logging" "env_logger" "log") ("default"))))))

(define-public crate-shelfie-1.0.1 (c (n "shelfie") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)))) (h "1piv2ss2f33i4kly81n18cn9fsjvdafcjygmcvxsyhskihc2d7gb") (f (quote (("logging" "env_logger" "log") ("default"))))))

