(define-module (crates-io sh el shell-escape) #:use-module (crates-io))

(define-public crate-shell-escape-0.1.0 (c (n "shell-escape") (v "0.1.0") (h "0cc43bcwf3x4hhcf1hgq25dhbjppmny7vfjbri11sjp0cl17amgi")))

(define-public crate-shell-escape-0.1.1 (c (n "shell-escape") (v "0.1.1") (h "1g5rzx32kg469z4j8szcmz3vn6cgp0q2kaimbcc1rbvld2y06hn3")))

(define-public crate-shell-escape-0.1.2 (c (n "shell-escape") (v "0.1.2") (h "010fwdix482kg8pmlhlrbn9acm5pvqiwaxa8nsxmkrqynv0vqcmk")))

(define-public crate-shell-escape-0.1.3 (c (n "shell-escape") (v "0.1.3") (h "1y2fp2brv639icv4a0fdqs1zhlrxq8qbz27ygfa86ifmh5jcjp6x")))

(define-public crate-shell-escape-0.1.4 (c (n "shell-escape") (v "0.1.4") (h "1fgs1iyx3b124b7njjmhfn9q5ipmhxrafavh8mxbfl9a9zk162hp")))

(define-public crate-shell-escape-0.1.5 (c (n "shell-escape") (v "0.1.5") (h "0kqq83dk0r1fqj4cfzddpxrni2hpz5i1y607g366c4m9iyhngfs5")))

