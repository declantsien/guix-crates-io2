(define-module (crates-io sh el shellcolor) #:use-module (crates-io))

(define-public crate-shellcolor-0.1.0 (c (n "shellcolor") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1sl5igzdz63h8fm3icxdsssv3lckf74aymslbxndj0vs48fn59mi")))

(define-public crate-shellcolor-0.2.0 (c (n "shellcolor") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0adsg838j0jkycq9frxnbm1wd167ym42cvmaaz0w8d7gnbh79hwn")))

(define-public crate-shellcolor-0.2.1 (c (n "shellcolor") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1lcl41aw4ipkihp4x3jdhzmc741qrvk7vmlj72fryigw7a8g20xb")))

