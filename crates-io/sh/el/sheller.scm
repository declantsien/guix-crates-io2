(define-module (crates-io sh el sheller) #:use-module (crates-io))

(define-public crate-sheller-0.0.1 (c (n "sheller") (v "0.0.1") (h "0bgmxkwi4dmy1kc1qjqjrhklvcdzyp04fd8s18ipalc4gh7dc8nj")))

(define-public crate-sheller-0.0.2 (c (n "sheller") (v "0.0.2") (h "18kv9iax1a7pg191hb7ki9idswib5aqj90g0cww9xprm6by6n9zs")))

(define-public crate-sheller-0.1.0 (c (n "sheller") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "01d0m6ky2xq392qasfmfv3afsnv67mvihhjib7grqzx0vdnxpfwg")))

(define-public crate-sheller-0.1.1 (c (n "sheller") (v "0.1.1") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0b8rqblap9y7zmhz39p7n58s75bfivz846p00rs48apar65b2m3c")))

(define-public crate-sheller-0.1.2 (c (n "sheller") (v "0.1.2") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "00ac3l1ch6j421qw2w73z4y51pw2ki8syc3n7jdaak10q2yw5zd4")))

(define-public crate-sheller-0.2.0 (c (n "sheller") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0rgy2j2nnkk7zqhdh98s5yr65s5cph8ac2qldwiwfy63cxdw26ba")))

(define-public crate-sheller-0.2.1 (c (n "sheller") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "08zi65g3g7cbgmafbllmqn1ng52avj253gp9zcgypghc1p33q0y2")))

(define-public crate-sheller-0.3.0 (c (n "sheller") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0rnyzqs1dhn7wljwa28503j5vphvnqxbsirfvf8jdphi6h6p2fkb")))

(define-public crate-sheller-0.4.0 (c (n "sheller") (v "0.4.0") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1jsmwzkq5k0qfsb0aldz82isfrvl8pn5dwhnw87d2i36vaxjpqpz")))

(define-public crate-sheller-0.5.0 (c (n "sheller") (v "0.5.0") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1x5hssaxfg9cbv3s1n0fsh6616vqxfaiywinfrmp887s8h05ycm1")))

(define-public crate-sheller-0.5.1 (c (n "sheller") (v "0.5.1") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0flh8bjx38xj6ja62k6qhjnd7g2n9yjqvdg1ryndh21b69hfazpl")))

(define-public crate-sheller-0.5.2 (c (n "sheller") (v "0.5.2") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0sa3p0ns1b453k29s6jiyv6sfwx8c5xxzcfpgjsz05rsfb8z9nmv")))

(define-public crate-sheller-0.5.3 (c (n "sheller") (v "0.5.3") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "013r5ds1z3cpmx1zrphy2s0mybyn680d78j34jsf63ibyfgv1j50")))

(define-public crate-sheller-0.5.4 (c (n "sheller") (v "0.5.4") (d (list (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1bk0cjzk9s4crvns706vjlvrvgxi0h4s9sp7mdrg7s24vmh88bx3")))

