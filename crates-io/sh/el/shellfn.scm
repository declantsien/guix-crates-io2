(define-module (crates-io sh el shellfn) #:use-module (crates-io))

(define-public crate-shellfn-0.1.0 (c (n "shellfn") (v "0.1.0") (d (list (d (n "shellfn-attribute") (r "^0.1.0") (d #t) (k 0)) (d (n "shellfn-core") (r "^0.1.0") (d #t) (k 0)))) (h "0mmc8bp13acgn79kj0x70gb4fviarf723v8rdj5jbhkrvjls3ylp")))

(define-public crate-shellfn-0.1.1 (c (n "shellfn") (v "0.1.1") (d (list (d (n "shellfn-attribute") (r "^0.1.1") (d #t) (k 0)) (d (n "shellfn-core") (r "^0.1.1") (d #t) (k 0)))) (h "052v2lsg91b8mgacjdy0i4wdqmypyjq86q4j664c8rlbim8lkvf9")))

