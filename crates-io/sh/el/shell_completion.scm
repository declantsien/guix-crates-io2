(define-module (crates-io sh el shell_completion) #:use-module (crates-io))

(define-public crate-shell_completion-0.0.1 (c (n "shell_completion") (v "0.0.1") (h "1f25hh058mds2i1z8yhhlbbdwgm21cdzk67r4vqfd12jqinv236h")))

(define-public crate-shell_completion-0.0.2 (c (n "shell_completion") (v "0.0.2") (h "0b04q56g5glvlvi49x83gjp95zrdl02if91yc6834dh44lcpr4vk")))

