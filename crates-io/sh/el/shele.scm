(define-module (crates-io sh el shele) #:use-module (crates-io))

(define-public crate-shele-0.1.0 (c (n "shele") (v "0.1.0") (h "10an6cg6kaz97ll0fk1ki54qllcnijcjph60j90cnr0cshlc115f")))

(define-public crate-shele-0.3.0 (c (n "shele") (v "0.3.0") (h "1ynj2nib1js4y73miihnyzmmm88s5qazm4vwb2wkqb0qrsvvv03r")))

(define-public crate-shele-0.3.1 (c (n "shele") (v "0.3.1") (h "1zcl63gi6vczxf81k7kbia5yjrnvkbmhfv4jjgigk7zac71pbrcm")))

(define-public crate-shele-0.4.0 (c (n "shele") (v "0.4.0") (h "0f3ldd3g2y8aiy6w9qp8qrjab1sl31nspj88xyf3aavdqyl2im5a")))

