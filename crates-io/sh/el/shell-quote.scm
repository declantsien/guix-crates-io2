(define-module (crates-io sh el shell-quote) #:use-module (crates-io))

(define-public crate-shell-quote-0.1.0 (c (n "shell-quote") (v "0.1.0") (h "1q0d1hg5w84kv4px4ikp2rly7zsd7b5gvknxj7bd7y3a7ql7bfag")))

(define-public crate-shell-quote-0.2.0 (c (n "shell-quote") (v "0.2.0") (h "01akashy8xnbbcl7fifvpd8ks5jav5qw477f1gyx3fbg13wfsdsg")))

(define-public crate-shell-quote-0.3.0 (c (n "shell-quote") (v "0.3.0") (h "0mr9sjx5ny8yjmarvr0xadndrvcx01aw7s7cfw4k051zdbjb2cdb")))

(define-public crate-shell-quote-0.3.1 (c (n "shell-quote") (v "0.3.1") (h "0jx8mczndk931iac2apr538xskb2kkb5rihkj4yp5rym5y409afp")))

(define-public crate-shell-quote-0.3.2 (c (n "shell-quote") (v "0.3.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1cqws6lzk1qzvryg2vy5gjjmvl56s1q3gn4ibx26n8d5pqv342yh")))

(define-public crate-shell-quote-0.4.0 (c (n "shell-quote") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1cj1irfab09vrlw9qbi9x4lwijxmag1h4lk7m8awj5hz8p9ay6da")))

(define-public crate-shell-quote-0.5.0 (c (n "shell-quote") (v "0.5.0") (d (list (d (n "bstr") (r "^1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1ggwjanysdln7fhv82rvx0pzg6hb1kwkkbl9lqzxpicbz78pi7m0") (f (quote (("default" "bstr"))))))

