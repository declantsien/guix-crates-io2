(define-module (crates-io sh el shelly) #:use-module (crates-io))

(define-public crate-shelly-0.1.0 (c (n "shelly") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)))) (h "106v6l6j6vc5hf8x6fdhkfb2i75svxz0a4d7qjkp1kx7irfzn0q4")))

(define-public crate-shelly-0.2.0 (c (n "shelly") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.1.0") (d #t) (k 0)))) (h "02895wawylbz6fkcxv2wnjva8054pschp9xhz6542p7w2rwsqc7d")))

(define-public crate-shelly-0.2.1 (c (n "shelly") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.1.0") (d #t) (k 0)))) (h "1lrzvpcyxrbq0i4qcm41s06fk3dahggb32jvf6z8rfmk9gj2j5vh")))

(define-public crate-shelly-0.2.2 (c (n "shelly") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.1.0") (d #t) (k 0)))) (h "0zfwlx7v051sn2j67djhm9pmkqh8qp0byik6995glqinmlm1q0zd")))

(define-public crate-shelly-0.3.0 (c (n "shelly") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1852qq4n118w7ij0dg644ni1zgv11qkdkw6g0xkr2jysg9zm9a69")))

