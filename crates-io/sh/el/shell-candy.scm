(define-module (crates-io sh el shell-candy) #:use-module (crates-io))

(define-public crate-shell-candy-0.1.0 (c (n "shell-candy") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "1jmjmkh76di73akga0766c82b3ddhi892i1y0fa97nshnbskf0qf")))

(define-public crate-shell-candy-0.1.1 (c (n "shell-candy") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "1x2qw1axsm8a7x9xmhqcxl4x8rxmnqly20f2cirql9awl3skypkp")))

(define-public crate-shell-candy-0.1.2 (c (n "shell-candy") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "0kzpn6x1mynwbsp6z4q79x3fgscpgc53kh7nxiv41qxlh2ds7hmq")))

(define-public crate-shell-candy-0.1.3 (c (n "shell-candy") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "1dbv8q2ya573chvs1qnj87qah3plhyrca144chgs9ihinzxr5347")))

(define-public crate-shell-candy-0.2.0 (c (n "shell-candy") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "1hh9nb3nbz28j5wyribpq39b61hq17x9nnjsi075y4vkhnwn4ggm")))

(define-public crate-shell-candy-0.2.1 (c (n "shell-candy") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "0860a8m69infz9xqnvdpdhxa4fwldqcirp78bg3gc08yc2s309bb")))

(define-public crate-shell-candy-0.2.3 (c (n "shell-candy") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "03j140f9nngndibhgd740ia486xb5g4c2vafvxlzp6m2yj31zyfr")))

(define-public crate-shell-candy-0.3.0 (c (n "shell-candy") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "0zkw863rmcz8ahnmfb18n2k2pbdpsqaf34x3hplrjj8z3rn2ivfc")))

(define-public crate-shell-candy-0.4.0 (c (n "shell-candy") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "12xw0x332npprs96d7znyz1xpkbsqkv4psxj30wr61bih627dy4w")))

