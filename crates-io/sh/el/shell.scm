(define-module (crates-io sh el shell) #:use-module (crates-io))

(define-public crate-shell-0.0.1 (c (n "shell") (v "0.0.1") (h "0yz1s5dddyj21dgm6ibdl0hy37a4ay6fbcmyh4rkbv38k9l886xn")))

(define-public crate-shell-0.2.0 (c (n "shell") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "term") (r "*") (d #t) (k 0)))) (h "1yqm9zzvrl9x8yjl4ia0lv9bz84nrv3wzl2064sznwqr68gy46v1")))

(define-public crate-shell-0.3.0 (c (n "shell") (v "0.3.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "term") (r "*") (d #t) (k 0)))) (h "055jq6362vnz58h6j75ihzxjmd80m3wvf0ifikgd2w0irq2p6yy8")))

(define-public crate-shell-0.3.1 (c (n "shell") (v "0.3.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "0zs47z1zg0sxwsrxgzmp8is09qs638j36qq0k6xs6fy4k6v41gga")))

(define-public crate-shell-0.3.2 (c (n "shell") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1dw9sfvvcnjdj6g29j30bjlm0glacihhyxdvr41dy3zi45r299k0")))

