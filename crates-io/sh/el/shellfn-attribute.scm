(define-module (crates-io sh el shellfn-attribute) #:use-module (crates-io))

(define-public crate-shellfn-attribute-0.1.0 (c (n "shellfn-attribute") (v "0.1.0") (d (list (d (n "darling") (r "^0.8.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "shellfn-core") (r "^0.1.0") (d #t) (k 0)) (d (n "shellwords") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0i5drw65fsl2nvjllidk17gpa13sdms707mjg2kndpi6jrp74l2r")))

(define-public crate-shellfn-attribute-0.1.1 (c (n "shellfn-attribute") (v "0.1.1") (d (list (d (n "darling") (r "^0.8.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "shellfn-core") (r "^0.1.1") (d #t) (k 0)) (d (n "shellwords") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v5wr7smm7z3pi1xdvrzp843spgrm2swjbg76zfxkb0dk8pxmi72")))

