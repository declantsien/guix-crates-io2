(define-module (crates-io sh el shell-hist) #:use-module (crates-io))

(define-public crate-shell-hist-0.1.0 (c (n "shell-hist") (v "0.1.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0vj9zw76qfw639w831napw6d9jc5yiqg0im28vh361psmrqiw3b4")))

(define-public crate-shell-hist-0.2.0 (c (n "shell-hist") (v "0.2.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0b8djhrwcgn75w6zg60qi2iw6fqlm5lbfafhcid6gx3ifi3j1sh6")))

