(define-module (crates-io sh el shell-history-cleaner) #:use-module (crates-io))

(define-public crate-shell-history-cleaner-0.3.1 (c (n "shell-history-cleaner") (v "0.3.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0c8fid8p0b0gs8al3fy3infyg3cfdna8fmbl6dnnmxjig76ln51d")))

