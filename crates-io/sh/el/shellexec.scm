(define-module (crates-io sh el shellexec) #:use-module (crates-io))

(define-public crate-shellexec-0.1.0 (c (n "shellexec") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "virtual-memory") (r "^0.1.0") (d #t) (k 0)))) (h "03044k3z113pfyiwgmix7n0b4hc3xa12r2fcib2c8wv67cvapcwi")))

(define-public crate-shellexec-0.1.1 (c (n "shellexec") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "virtual-memory") (r "^0.1.1") (d #t) (k 0)))) (h "1wmk0d8mqpk4pdn5vd28gay114yb36xw9klp17ahw4lisd9xfmwd")))

