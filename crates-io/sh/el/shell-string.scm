(define-module (crates-io sh el shell-string) #:use-module (crates-io))

(define-public crate-shell-string-0.1.1 (c (n "shell-string") (v "0.1.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "116xhar16iqwj12gb2rdw32gccw4qdypdzx7b6cn4zbd33pyxfg0")))

(define-public crate-shell-string-0.1.2 (c (n "shell-string") (v "0.1.2") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "14g5xa0ifpawis2555vkc8jcyli9gfc4g7bq4fybn52a0s8wayvy")))

(define-public crate-shell-string-0.1.3 (c (n "shell-string") (v "0.1.3") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1xfnr5zgfakdczi39ny00974k2kb023zsgzz2c2cms7y4jwfmcp9")))

(define-public crate-shell-string-0.1.4 (c (n "shell-string") (v "0.1.4") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1wrkf3nrkgzi59ap8w48apkqasib613mqvbxrw1y9wp77z8nbm1d")))

(define-public crate-shell-string-0.1.5 (c (n "shell-string") (v "0.1.5") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0404xd8wqq77329h7kvghhhxa0jvr537649clf7vhryvd1rgbqrg")))

(define-public crate-shell-string-0.2.0 (c (n "shell-string") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "10ci85mjkczfb53xpgb505s5r3dy51lmr2xvsf8d5i00xgdj8pgd")))

(define-public crate-shell-string-0.2.1 (c (n "shell-string") (v "0.2.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "17s8gn4rzd9ibww5y0nsaq0ipzl0ifzvmijj7j0bz03v80jby9gw")))

(define-public crate-shell-string-0.2.2 (c (n "shell-string") (v "0.2.2") (d (list (d (n "itertools") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "nom") (r ">=6.0.0, <7.0.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.14, <0.4.0") (d #t) (k 0)))) (h "0fhivqid6lx2hq0hsv48hwbr6mfjgn4p1h16ii090wjc1vc7nhlj")))

(define-public crate-shell-string-0.2.3 (c (n "shell-string") (v "0.2.3") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1jiwdvyw3m8g3ygiasrl9kk5xsjcb9isk2s20ss1qisqf7dd515l")))

(define-public crate-shell-string-0.3.0 (c (n "shell-string") (v "0.3.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1slx1dj9kvmn11015205rp98vsrk7r7cw3y78b85grm9pp3lz44x")))

(define-public crate-shell-string-0.3.1 (c (n "shell-string") (v "0.3.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0cqy021dxhw3blz33c4hnbp7afynyylqc9x7h9sxr746cgd6vvy0")))

(define-public crate-shell-string-0.3.2 (c (n "shell-string") (v "0.3.2") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0iwz6h93wig1hz1sxhfmwcjzbbcll0d6azy25ls4xzsxi6grxa9f")))

(define-public crate-shell-string-0.3.3 (c (n "shell-string") (v "0.3.3") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0mkzzk670nclih7gz8k6w1qb4a1gz92pqxxxflgvgqik1ilmgbq8")))

(define-public crate-shell-string-0.3.4 (c (n "shell-string") (v "0.3.4") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1d0jd9xzzzz01cj19xxzrffya64k08c3yplycng495rl4f76h6bd")))

(define-public crate-shell-string-0.3.5 (c (n "shell-string") (v "0.3.5") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0mrkji2m1jbyph7jbn2g1ys06qcs7rwn15ncpwdjzsmdh12kalrx")))

(define-public crate-shell-string-0.3.6 (c (n "shell-string") (v "0.3.6") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0d101yn0y4fvcqgpn5d2fy1s1z25m4ha1wgyyrg5ki0wycxv57qn")))

(define-public crate-shell-string-0.4.0 (c (n "shell-string") (v "0.4.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1zxzq09gc2gavyb0r185c70v7nxnfiminvxlr37yqm1bnk4s6hyd")))

(define-public crate-shell-string-0.4.1 (c (n "shell-string") (v "0.4.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "11vjknbr22h33lisc9qsm4gihay01csjmgsq0q49my1dz16k7sx6")))

(define-public crate-shell-string-0.4.2 (c (n "shell-string") (v "0.4.2") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1dffh89ncr0z978amfcwmxm28sdqvb7pqc32b2b2rah0lmrafzaj")))

