(define-module (crates-io sh el shellexpand) #:use-module (crates-io))

(define-public crate-shellexpand-0.1.0 (c (n "shellexpand") (v "0.1.0") (h "1nbaf7qq8cini4hz8ljnj899xnk4iz9gzb6fdbgswiaz1qqvajjg")))

(define-public crate-shellexpand-1.0.0 (c (n "shellexpand") (v "1.0.0") (h "0r3mfdvcjda7j57iz5g6aa9q918v3819n8702252gza2j5d5nyny")))

(define-public crate-shellexpand-1.1.0 (c (n "shellexpand") (v "1.1.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "0jjdlhaj54mw7vwhaa35rwqj0xsiwm0xwsl2qwb7784f078aw8rs")))

(define-public crate-shellexpand-1.1.1 (c (n "shellexpand") (v "1.1.1") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)))) (h "1dd6rcq423jcf1wipbwymfqv6z3yvqhx9wmaxsdiyhbvvknpjzic")))

(define-public crate-shellexpand-2.0.0 (c (n "shellexpand") (v "2.0.0") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)))) (h "00y6kf5nvrpmbsyyxic1r1w4yqr0lkz5dxjnsdj99bws58k24aws")))

(define-public crate-shellexpand-2.1.0 (c (n "shellexpand") (v "2.1.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)))) (h "0a981ynym0hipnvkd93ihszdszjqs0cslj5ilzsdv19d3f1vggc3")))

(define-public crate-shellexpand-2.1.2 (c (n "shellexpand") (v "2.1.2") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "1r0i1r2r3dv0rc82xc5vhxmwl3zbvblf91sgmwls0k8chiv81k3w")))

(define-public crate-shellexpand-3.0.0 (c (n "shellexpand") (v "3.0.0") (d (list (d (n "bstr") (r "^1.0.0-pre.2") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (o #t) (d #t) (k 0)) (d (n "os_str_bytes") (r "^6") (o #t) (d #t) (k 0)))) (h "1qc0d1zsaha7hphmyp0323zbndby5c6hrz9r9i44sab5lvg7s76x") (f (quote (("tilde" "dirs") ("path" "bstr" "os_str_bytes") ("full" "default" "path") ("default" "base-0" "tilde") ("base-0"))))))

(define-public crate-shellexpand-3.1.0 (c (n "shellexpand") (v "3.1.0") (d (list (d (n "bstr") (r "^1.0.0-pre.2") (o #t) (d #t) (k 0)) (d (n "dirs") (r ">=4, <6") (o #t) (d #t) (k 0)) (d (n "os_str_bytes") (r ">=5, <7") (o #t) (d #t) (k 0)))) (h "0jz1i14ziz8gbyj71212s7dqrw6q96f25i48zkmy66fcjhxzl0ys") (f (quote (("tilde" "dirs") ("path" "bstr" "os_str_bytes") ("full-msrv-1-51" "full-msrv-1-31" "path") ("full-msrv-1-31" "base-0" "tilde") ("full" "full-msrv-1-51") ("default" "base-0" "tilde") ("base-0"))))))

