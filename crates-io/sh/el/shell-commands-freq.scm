(define-module (crates-io sh el shell-commands-freq) #:use-module (crates-io))

(define-public crate-shell-commands-freq-0.1.0 (c (n "shell-commands-freq") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vbi26zyg7y4cj5142hbkm9vy7g00c5i1hjjlpnb2dzfmk9dh0rh")))

(define-public crate-shell-commands-freq-0.1.1 (c (n "shell-commands-freq") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g68a84jmb5nsg6py868ldgv9z97w63jvrdk2qr7qggc1y14cwl0")))

