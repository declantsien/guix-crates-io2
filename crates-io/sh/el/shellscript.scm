(define-module (crates-io sh el shellscript) #:use-module (crates-io))

(define-public crate-shellscript-0.1.0 (c (n "shellscript") (v "0.1.0") (h "19k8hxwxaqyanx8apsddd7y5vzb4qliq9s3x4c2y204a28mvcypa")))

(define-public crate-shellscript-0.2.0 (c (n "shellscript") (v "0.2.0") (h "0bp9yp4n009ff0klckr14irh5738278c4v0sjj0j197r3bw8cwqm")))

(define-public crate-shellscript-0.2.1 (c (n "shellscript") (v "0.2.1") (h "0f2gq4zrw63c9wp5bmjrwiiiqq5zmawy9jn9pa1gd33yq2gp370i")))

(define-public crate-shellscript-0.3.0 (c (n "shellscript") (v "0.3.0") (h "0ixmnwc2zgb47n6fcg3yh7qgm3bvj88dfck9ngj2ikplxi56walm")))

(define-public crate-shellscript-0.3.1 (c (n "shellscript") (v "0.3.1") (h "1i10dnk6fcbqhp3y2x8ga9zr1gc6v2dm98d116b213bzm5zx1h0m")))

