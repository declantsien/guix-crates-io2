(define-module (crates-io sh el shell2bin) #:use-module (crates-io))

(define-public crate-shell2bin-0.1.0 (c (n "shell2bin") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "19ydrz2bwx7h4r36h5brdp8fw7p7dh7nfbjr7ys6gr143qdjbgwp")))

(define-public crate-shell2bin-2.0.0 (c (n "shell2bin") (v "2.0.0") (d (list (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0f83c10lv04lwzf9bs2l5v9chisv48z1ir4dmrsjfhwhqk37aspv")))

