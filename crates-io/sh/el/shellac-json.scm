(define-module (crates-io sh el shellac-json) #:use-module (crates-io))

(define-public crate-shellac-json-0.2.0 (c (n "shellac-json") (v "0.2.0") (d (list (d (n "capnp") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shellac-codec") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1rvssrnd5nd3z69ln8jfkn5c5xp9dpniia3zmp72mkrga368bzas")))

