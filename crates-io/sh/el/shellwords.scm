(define-module (crates-io sh el shellwords) #:use-module (crates-io))

(define-public crate-shellwords-0.1.0 (c (n "shellwords") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1zp5s88gnk3lwrk9d2hw9qsyg5ik93fg0ikq9jd8jmbc05n8c6jp")))

(define-public crate-shellwords-1.0.0 (c (n "shellwords") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "1xfhnl4xdra8dyyfjkc1fl4yk5dcsgfdi057c1pd48zy1sdhwpv8")))

(define-public crate-shellwords-1.1.0 (c (n "shellwords") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1rna1yky1g9kbaavqgxljp74h07grq9n9yayxm483a4r8sm1brc9")))

