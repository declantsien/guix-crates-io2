(define-module (crates-io sh el shell-daddy) #:use-module (crates-io))

(define-public crate-shell-daddy-0.1.1 (c (n "shell-daddy") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "05ksqrak0zqkbxw2xid4rg8vgmps899xm62nsabs87j47qp63h87")))

(define-public crate-shell-daddy-0.1.2 (c (n "shell-daddy") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "18clpx9771kxc2c6am2v8ayq8g80r090ssg71jp5vpi0p0k05yc4")))

(define-public crate-shell-daddy-0.1.3 (c (n "shell-daddy") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "171my6d7fn2ayja6qddrq2cd6djyckwcjrw9jcawylfzk8906vmk")))

