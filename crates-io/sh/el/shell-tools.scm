(define-module (crates-io sh el shell-tools) #:use-module (crates-io))

(define-public crate-shell-tools-0.1.0 (c (n "shell-tools") (v "0.1.0") (h "1j2fz5dipinr9nbjavdvggaap6wmmxvkkxxrdc7fj3i3rjjx3svv")))

(define-public crate-shell-tools-0.1.1 (c (n "shell-tools") (v "0.1.1") (h "0v6nbsa4c8k5siv7x1k647jynawyzis6g5idn95vyimj0d0kmb95")))

(define-public crate-shell-tools-0.1.2 (c (n "shell-tools") (v "0.1.2") (h "1sawa91iic6cgn5qxbbagx31ay4rfl3xpy2prp17k462iv69fzyx")))

(define-public crate-shell-tools-0.1.3 (c (n "shell-tools") (v "0.1.3") (h "1yplhpfbhici3mcnj59rb4bf9j4fjjmfcb6jazczz4qdf1mpvgi2")))

(define-public crate-shell-tools-0.1.4 (c (n "shell-tools") (v "0.1.4") (h "1r0ysfin9v9iqaah7yi5zhwl3x40xdd1998634syyxz4jj8w5dvq")))

(define-public crate-shell-tools-0.1.5 (c (n "shell-tools") (v "0.1.5") (h "1vbxwga54pq59c2jr1rkby8j3vjw65big34208744zskbwbjd9js")))

