(define-module (crates-io sh el shellac-codec) #:use-module (crates-io))

(define-public crate-shellac-codec-0.2.0 (c (n "shellac-codec") (v "0.2.0") (d (list (d (n "capnp") (r "^0.10") (d #t) (k 0)) (d (n "capnpc") (r "^0.10") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yg6jk3jqrzpx1s8w89vhghss62zwm9i5gsqw7q8ry968nfbgynb")))

