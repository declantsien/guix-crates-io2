(define-module (crates-io sh el shellcheck-sarif) #:use-module (crates-io))

(define-public crate-shellcheck-sarif-0.2.11 (c (n "shellcheck-sarif") (v "0.2.11") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.11") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "069ph5kqnr5l1xwigbbg49i56v2q7y4bir6bz9z27x30fnwv6ahx")))

(define-public crate-shellcheck-sarif-0.2.12 (c (n "shellcheck-sarif") (v "0.2.12") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.12") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1kgpqy2fl68r5q1ypnyhwhp2ybn2ybfhqxbfxh2dlpd26z2l9d27")))

(define-public crate-shellcheck-sarif-0.2.14 (c (n "shellcheck-sarif") (v "0.2.14") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.14") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0dfgpxlrf9jz1g6g3zvglac81kwl4s2270jgx9x0vr1zcsqyw53w")))

(define-public crate-shellcheck-sarif-0.2.15 (c (n "shellcheck-sarif") (v "0.2.15") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.15") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1brdnsdh04zgwraly7f6rmbzr081rh9j9ah9rl20j0jyfn5xzkbi")))

(define-public crate-shellcheck-sarif-0.2.17 (c (n "shellcheck-sarif") (v "0.2.17") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.17") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "074ja9j4w0mndr2izlf2v6kvf2f4b7dliacz0m0iknjxdi9jkg04")))

(define-public crate-shellcheck-sarif-0.2.19 (c (n "shellcheck-sarif") (v "0.2.19") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.19") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "19wdchiscj8lzf21ls6v3z8awjfzicd5x8syklak9avmpwh6b7y5")))

(define-public crate-shellcheck-sarif-0.2.20 (c (n "shellcheck-sarif") (v "0.2.20") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.20") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0qjafdsmcivhdikdjij5c36hw5jci7g6m1b4s02g4higy5pkb9qs")))

(define-public crate-shellcheck-sarif-0.2.21 (c (n "shellcheck-sarif") (v "0.2.21") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.7") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.21") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "167hd2bc3h92kxkjwapq57fv4d4rd0smyikai4zalfni8k20n8gx")))

(define-public crate-shellcheck-sarif-0.2.22 (c (n "shellcheck-sarif") (v "0.2.22") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.22") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0kl52w5k6dxd0pip6bpj493xflr1mnw0vgyf2xmb9ycbb4gsxqa4")))

(define-public crate-shellcheck-sarif-0.2.24 (c (n "shellcheck-sarif") (v "0.2.24") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.24") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "13j6w91r2wjqjxl55i6390qb6gi7la6119086bjywkignigz6a5a")))

(define-public crate-shellcheck-sarif-0.2.25 (c (n "shellcheck-sarif") (v "0.2.25") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.2.25") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1pqicjlvmnz0iymy3j8rgr3bqi37pjlnd1s5nrpdf71k3ng3pvvq")))

(define-public crate-shellcheck-sarif-0.3.0 (c (n "shellcheck-sarif") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.10") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.0") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1s2yvva1ihwm1qmhv3hbr1pdl0jgqk2csdc051wzzbdh5yjqw3b1")))

(define-public crate-shellcheck-sarif-0.3.1 (c (n "shellcheck-sarif") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.1") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0ll1q5mxz9gha4sa79848zsbrjd05j0v4vaf1pq3sf73c9m0lzl3")))

(define-public crate-shellcheck-sarif-0.3.2 (c (n "shellcheck-sarif") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.2") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0r6g2gnazr6wmk7q271z7wp1g1s4h99nib6063kjn83s03q79pqc")))

(define-public crate-shellcheck-sarif-0.3.3 (c (n "shellcheck-sarif") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.3") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0k1ds8yr6c652jqnd9izdy2l63mwszwiq36xb6byrxwn8dhv5wdh")))

(define-public crate-shellcheck-sarif-0.3.4 (c (n "shellcheck-sarif") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.4") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1x6i1fwqpab1z27yz2g5raslx27lg5s50xjb9yw4zzfdd2qf24s3")))

(define-public crate-shellcheck-sarif-0.3.5 (c (n "shellcheck-sarif") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.5") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1357zhpwrszgyd52q96a9dz5f5abzckliiwjw5w0sda2f1q8770m")))

(define-public crate-shellcheck-sarif-0.3.6 (c (n "shellcheck-sarif") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.6") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "13ln7zjnlm8vxq53sdikj76cdrl3idx05cywr1c6q4mph805vd9c")))

(define-public crate-shellcheck-sarif-0.3.7 (c (n "shellcheck-sarif") (v "0.3.7") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.3.7") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1sq4p6r6j9r7kzscy6q76hm55gdh1d3v61ka75hkz0abflck84p4")))

(define-public crate-shellcheck-sarif-0.4.0 (c (n "shellcheck-sarif") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.4.0") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0pnyr7cwggmby78ar2lpiswdxzy5jrnc06fnmcmadinhfqv5j31s")))

(define-public crate-shellcheck-sarif-0.4.1 (c (n "shellcheck-sarif") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.4.1") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0kg3d07fwy1vrn04aaln9db0x8lc6cr185bl095gc3ghvy4mm1sw")))

(define-public crate-shellcheck-sarif-0.4.2 (c (n "shellcheck-sarif") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-sarif") (r "^0.4.2") (f (quote ("shellcheck-converters"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0s6yn1qzpql09q585dlanmq164j3rk43rwx7ls1dy7nsi5hgznll")))

