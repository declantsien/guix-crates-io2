(define-module (crates-io sh el shell-hooks) #:use-module (crates-io))

(define-public crate-shell-hooks-0.1.0 (c (n "shell-hooks") (v "0.1.0") (d (list (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "0cg5j0igsgl4sw22yld973v5b6vrw9z36f2i7dq954mbc3mcp49i")))

(define-public crate-shell-hooks-0.1.2 (c (n "shell-hooks") (v "0.1.2") (d (list (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "1gy1aidqj8pcsiyx7v736c2g3s0ad0br07xra8pdjmram19z27rp")))

