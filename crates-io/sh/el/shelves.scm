(define-module (crates-io sh el shelves) #:use-module (crates-io))

(define-public crate-shelves-0.1.0 (c (n "shelves") (v "0.1.0") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "15iqxggcf63lq2wlb0hj7q930ay3k57hqlchcd8634dgbxnfv3ff") (f (quote (("slab-storage" "slab"))))))

(define-public crate-shelves-0.2.0 (c (n "shelves") (v "0.2.0") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1n6znb11by6ikkzflxhwy7v1lryx33ds7lp563ynz5gzdwd48r37") (f (quote (("slab-storage" "slab"))))))

(define-public crate-shelves-0.2.1 (c (n "shelves") (v "0.2.1") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0b9kjszk3xc8gm9bm76xyrb3gjz2xibp1wfpmzj5a0swy43zhw3v") (f (quote (("slab-storage" "slab"))))))

(define-public crate-shelves-0.2.2 (c (n "shelves") (v "0.2.2") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "10f5q18m9xmh5nq7hcf8hfs52m0vma7z18bv55n702fnj3zlwsfz") (f (quote (("slab-storage" "slab"))))))

(define-public crate-shelves-0.2.3 (c (n "shelves") (v "0.2.3") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1d4p6yif6cmvq6zrkxbdhsx30ssc6m6yvljnh6nsp7zxgn11pc2a") (f (quote (("slab-storage" "slab"))))))

(define-public crate-shelves-0.2.4 (c (n "shelves") (v "0.2.4") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0l0vzms269xc4a8br4fx2qvin0w350m1grwy8v6pwjnlq9ymwp2z") (f (quote (("slab-storage" "slab"))))))

(define-public crate-shelves-0.2.5 (c (n "shelves") (v "0.2.5") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1wz48rx8dcfi768zggilkpn7fwn9lkdsbkmmpl52cfj47infim2h") (f (quote (("slab-storage" "slab"))))))

(define-public crate-shelves-0.2.6 (c (n "shelves") (v "0.2.6") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "18gwmyykrgijp1bdiir72lfn1h3ypsq1kl8dzch71vnhh8jaaj67") (f (quote (("slab-storage" "slab"))))))

(define-public crate-shelves-0.2.7 (c (n "shelves") (v "0.2.7") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "02gjwmqzl8jqwnz1saxg9pd8l6v0f75b1rc4q1506j0312rk3y4c") (f (quote (("slab-storage" "slab"))))))

(define-public crate-shelves-1.0.0 (c (n "shelves") (v "1.0.0") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1g95i4a550w6i7pk2xjazi3zmqkivk1fc49k9sx6v7sg4pjv4mcq") (f (quote (("slab-storage" "slab"))))))

(define-public crate-shelves-1.1.0 (c (n "shelves") (v "1.1.0") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "05xnf3iknlq994zv2zkr1dn1n79ni7n72jxchbaj2pgxd330k6zp") (f (quote (("slab-storage" "slab"))))))

(define-public crate-shelves-1.2.0 (c (n "shelves") (v "1.2.0") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0pg8m9fi7f6fymx1hbwipd65jwdwx1ma1l2mhk8z1mpgfm90z2iz") (f (quote (("slab-storage" "slab")))) (s 2) (e (quote (("serde" "dep:serde"))))))

