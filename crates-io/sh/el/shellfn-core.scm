(define-module (crates-io sh el shellfn-core) #:use-module (crates-io))

(define-public crate-shellfn-core-0.1.0 (c (n "shellfn-core") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "1yb7czy3rbqgg4a6xylq8jfhr80sdjrd32aqjmxflr6xi3fzbmqi")))

(define-public crate-shellfn-core-0.1.1 (c (n "shellfn-core") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "1mxz5i06j5baqb3w2j7kb46y42nivzdfqb6832bdq1j1gq34r7b5")))

