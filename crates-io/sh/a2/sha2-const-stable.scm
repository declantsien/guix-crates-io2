(define-module (crates-io sh a2 sha2-const-stable) #:use-module (crates-io))

(define-public crate-sha2-const-stable-0.1.0 (c (n "sha2-const-stable") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "proptest") (r "^0.9.6") (d #t) (k 2)) (d (n "sha2") (r "^0.8.1") (d #t) (k 2)))) (h "1n9yz02vfyf6sp5hlnlzz497b2j49pvhiwpzh8xqjjh92579s5sz")))

