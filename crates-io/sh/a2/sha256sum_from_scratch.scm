(define-module (crates-io sh a2 sha256sum_from_scratch) #:use-module (crates-io))

(define-public crate-sha256sum_from_scratch-0.1.1 (c (n "sha256sum_from_scratch") (v "0.1.1") (d (list (d (n "sha_256_scratch") (r "^0.1.1") (d #t) (k 0)))) (h "1z3w3y0pjx6sshp8wb8gqdpwwhf89zxi94s3x56p3is6v9v0mm12")))

(define-public crate-sha256sum_from_scratch-0.1.2 (c (n "sha256sum_from_scratch") (v "0.1.2") (d (list (d (n "sha_256_scratch") (r "^0.1.2") (d #t) (k 0)))) (h "1rpmfb9qa180lghmljwnkd7w6rmsfy220xxrz7p2fw6a5vijj625")))

