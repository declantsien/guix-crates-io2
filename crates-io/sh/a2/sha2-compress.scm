(define-module (crates-io sh a2 sha2-compress) #:use-module (crates-io))

(define-public crate-sha2-compress-0.1.0 (c (n "sha2-compress") (v "0.1.0") (d (list (d (n "fixed-array") (r "^0.1.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.0") (d #t) (k 0)))) (h "11241zz09gfr9md2yj8jnk0kh5wqnr6d4llxrbmh9ps90bjpf9sy")))

(define-public crate-sha2-compress-0.1.1 (c (n "sha2-compress") (v "0.1.1") (d (list (d (n "fixed-array") (r "^0.1.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.0") (d #t) (k 0)))) (h "15r8vj593ysmqz4gjddfam5g8lrqk9p8k3ilfw7ly8mhm0l7zk5j")))

(define-public crate-sha2-compress-0.1.2 (c (n "sha2-compress") (v "0.1.2") (d (list (d (n "fixed-array") (r "^0.1.1") (d #t) (k 0)) (d (n "uints") (r "^0.5.1") (d #t) (k 0)))) (h "1sha3w8fp3mimv376lbmq19vad9ic28yrs09pz3pi57x8a52wqrs")))

(define-public crate-sha2-compress-0.2.0 (c (n "sha2-compress") (v "0.2.0") (d (list (d (n "fixed-array") (r "^0.1.1") (d #t) (k 0)) (d (n "uints") (r "^0.6.0") (d #t) (k 0)))) (h "0z668wjyll6n8kn7z35d4fnscq4xcknyh0gy39cy07cmyj9mc89c")))

(define-public crate-sha2-compress-0.2.1 (c (n "sha2-compress") (v "0.2.1") (d (list (d (n "fixed-array") (r "^0.1.1") (d #t) (k 0)) (d (n "uints") (r "^0.6.1") (d #t) (k 0)))) (h "02m60ginhyq2nmzm71lzkasszg9yanf7a36nwbfbh4lx6wf0payv")))

(define-public crate-sha2-compress-0.3.0 (c (n "sha2-compress") (v "0.3.0") (d (list (d (n "fixed-array") (r "^0.2.0") (d #t) (k 0)) (d (n "uints") (r "^0.7.0") (d #t) (k 0)))) (h "1h0dyi6if6v4r7ps3d9yylam37mf7fs4vasdsg7l6fkz5yaj7pfk")))

(define-public crate-sha2-compress-0.3.1 (c (n "sha2-compress") (v "0.3.1") (d (list (d (n "fixed-array") (r "^0.2.0") (d #t) (k 0)) (d (n "uints") (r "^0.7.0") (d #t) (k 0)))) (h "02x5slfyn2l5l57016mdwdrljsr6zivx9a8dq10nsfwwk2l8c59r")))

(define-public crate-sha2-compress-0.3.2 (c (n "sha2-compress") (v "0.3.2") (d (list (d (n "fixed-array") (r "^0.2.0") (d #t) (k 0)) (d (n "uints") (r "^0.7.0") (d #t) (k 0)))) (h "1gbd7v5fjvmcydqvw70h6ksaaqcv9v3m4p39kklx8kw2cwkksjl7")))

(define-public crate-sha2-compress-0.3.3 (c (n "sha2-compress") (v "0.3.3") (d (list (d (n "fixed-array") (r "^0.2.0") (d #t) (k 0)) (d (n "uints") (r "^0.7.0") (d #t) (k 0)))) (h "0w8g4xl9n3ih5jr0nzck48hf56z2fl99xfkl1jcpdvf1z35kddyh")))

(define-public crate-sha2-compress-0.4.0 (c (n "sha2-compress") (v "0.4.0") (d (list (d (n "fixed-array") (r "^0.2.0") (d #t) (k 0)) (d (n "uints") (r "^0.8.0") (d #t) (k 0)))) (h "0c4q266spi85jz2jmywnganrvzwcv09dlzb342n1ax7ydydk31yf")))

(define-public crate-sha2-compress-0.4.1 (c (n "sha2-compress") (v "0.4.1") (d (list (d (n "fixed-array") (r "^0.2.0") (d #t) (k 0)) (d (n "uints") (r "^0.8.1") (d #t) (k 0)))) (h "0kvnz37yq2hm7ik6c4bzs8rqxhg2mpgi0q5h3fmkq40cshacmriw")))

(define-public crate-sha2-compress-0.5.0 (c (n "sha2-compress") (v "0.5.0") (d (list (d (n "fixed-array") (r "^0.3.0") (d #t) (k 0)) (d (n "uints") (r "^0.9.0") (d #t) (k 0)))) (h "11n3x39h5qzzsf57pphwsrm95mfqlrf61v7pjmllawf22f6fr737")))

(define-public crate-sha2-compress-0.6.0 (c (n "sha2-compress") (v "0.6.0") (d (list (d (n "fixed-array") (r "^0.4.0") (d #t) (k 0)) (d (n "uints") (r "^0.10.0") (d #t) (k 0)))) (h "0bg07ysvvd7p9ryczf95jniv6z477vzrgnak823wf7m5767hmaag")))

(define-public crate-sha2-compress-0.7.0 (c (n "sha2-compress") (v "0.7.0") (d (list (d (n "fixed-array") (r "^0.4.0") (d #t) (k 0)) (d (n "uints") (r "^0.11.0") (d #t) (k 0)))) (h "195ys962rrwgha1xs8jvip7xk1ndkql9wd8f0v2ncj64lwcxjg5d")))

(define-public crate-sha2-compress-0.7.1 (c (n "sha2-compress") (v "0.7.1") (d (list (d (n "fixed-array") (r "^0.4.0") (d #t) (k 0)) (d (n "uints") (r "^0.11.1") (d #t) (k 0)))) (h "1m3mmkphhh930yn8mz5ww8a64sxig1wnwi27lxfqfg04pprhp4d1")))

(define-public crate-sha2-compress-0.7.2 (c (n "sha2-compress") (v "0.7.2") (d (list (d (n "fixed-array") (r "^0.4.1") (d #t) (k 0)) (d (n "uints") (r "^0.11.2") (d #t) (k 0)))) (h "1plfqhqbv6fjz7rkm2szqk3wvss2hxhbrvzxshrr0j12xa6jpiyc")))

