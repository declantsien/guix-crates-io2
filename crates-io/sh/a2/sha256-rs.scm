(define-module (crates-io sh a2 sha256-rs) #:use-module (crates-io))

(define-public crate-sha256-rs-0.1.0 (c (n "sha256-rs") (v "0.1.0") (h "0pjc9zhxip2gwkbwxqbk4mz2k6fpqdd623qgp4lxn7lv375hbm5m")))

(define-public crate-sha256-rs-1.0.0 (c (n "sha256-rs") (v "1.0.0") (h "12salgmx03mwhq14wf3as4x61i0325h9li2wgyhclacmihhvfvvb")))

(define-public crate-sha256-rs-1.0.1 (c (n "sha256-rs") (v "1.0.1") (h "0cmc9g3sbi72n5ppjnvq4zji4saajvqajpphdmq3180miir2phyv")))

(define-public crate-sha256-rs-1.0.2 (c (n "sha256-rs") (v "1.0.2") (h "18jaxj53mab2bnbc7rccnl4cf5ya0lhghk42c2f9ak8mb2z443c2")))

