(define-module (crates-io sh a2 sha256) #:use-module (crates-io))

(define-public crate-sha256-0.1.0 (c (n "sha256") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "hmac") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 0)))) (h "0pj2ingwsw5b9k7s248dy4da1kqkkyclhh1zjm8cmbrmjc5f5fpj") (y #t)))

(define-public crate-sha256-1.0.0 (c (n "sha256") (v "1.0.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "1pwklwj049zv5830rphzd380y8mnkbi60ds050v6kfrkljp89pq1")))

(define-public crate-sha256-1.0.1 (c (n "sha256") (v "1.0.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "0x92043zww3y1i19igfhkfvay4jfi08ckqm9dqqv9kc7zb1hbs51")))

(define-public crate-sha256-1.0.2 (c (n "sha256") (v "1.0.2") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (k 0)))) (h "0jik6ca4gk0wsby31qgj92aj50dz0c2dqiqws58jnx2l6gnkdwvp")))

(define-public crate-sha256-1.0.3 (c (n "sha256") (v "1.0.3") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (k 0)))) (h "0vhldqp4gq8n36py349m932c72qkfwwah1jyvrcx70f0jvssg12f")))

(define-public crate-sha256-1.1.0 (c (n "sha256") (v "1.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (k 0)))) (h "0bxqv40k8zrcvyy7v229jm66lpi8kns625rps6dijrpsp950kmkv")))

(define-public crate-sha256-1.1.1 (c (n "sha256") (v "1.1.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (k 0)))) (h "0hd1qv43jfhy5m1syq8wm7wzbzgr2fpi9bb6q27w250whxkxnd73")))

(define-public crate-sha256-1.1.2 (c (n "sha256") (v "1.1.2") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (k 0)))) (h "106ifg0ygxyp6f814213qrby4ab6wgwlggl27pl5f696czqnk09j")))

(define-public crate-sha256-1.1.3 (c (n "sha256") (v "1.1.3") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (k 0)))) (h "07sgd3x23hylf84rq6684sdnsclf583i3dp952pa9hxsw9fqp7sz")))

(define-public crate-sha256-1.1.4 (c (n "sha256") (v "1.1.4") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (k 0)))) (h "0a18fxi6531cdd5k9mh8aj6qs66fv3235wpa00q70h89pk0pba88")))

(define-public crate-sha256-1.1.5 (c (n "sha256") (v "1.1.5") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (k 0)))) (h "10n74xfl49s264kd3jh4rhs2233xpgdk2i52q45glh67skj6f4yc")))

(define-public crate-sha256-1.2.0 (c (n "sha256") (v "1.2.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.54") (o #t) (k 0)) (d (n "sha2") (r "^0.10.6") (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1j70prwpa5j1qaxq441b7405fwpv9pa1gj7xixllnp33zm41a7xi") (f (quote (("native_openssl" "openssl"))))))

(define-public crate-sha256-1.2.1 (c (n "sha256") (v "1.2.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.54") (o #t) (k 0)) (d (n "sha2") (r "^0.10.6") (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "13l0rd5ykcbdg6bpxy6ri32lf4p7j3vn40x6flx6vagj0qwzk57q") (f (quote (("native_openssl" "openssl"))))))

(define-public crate-sha256-1.2.2 (c (n "sha256") (v "1.2.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.54") (o #t) (k 0)) (d (n "sha2") (r "^0.10.6") (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "14j7fwxavvljxfx5z11v1h1mi7gz83144cx542n953br1h5p0vrq") (f (quote (("native_openssl" "openssl"))))))

(define-public crate-sha256-1.3.0 (c (n "sha256") (v "1.3.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.54") (o #t) (k 0)) (d (n "sha2") (r "^0.10.6") (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "070nnq00q85ncmggnbz085wp0g85caj4mrkvwv2vb7rdpvmxapkz") (f (quote (("native_openssl" "openssl"))))))

(define-public crate-sha256-1.4.0 (c (n "sha256") (v "1.4.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.54") (o #t) (k 0)) (d (n "sha2") (r "^0.10.6") (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util" "fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "11ik6fwggprzs6s18vqi37b9qmhckf9qnhzz2jnwx32qi2pci5bq") (f (quote (("native_openssl" "openssl") ("default" "async") ("async" "tokio"))))))

(define-public crate-sha256-1.5.0 (c (n "sha256") (v "1.5.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.54") (o #t) (k 0)) (d (n "sha2") (r "^0.10.6") (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util" "fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1c35j1z4inpz7fwa6vy0xf3arffz5mykyj8nlc50g8sgj5m8y9qq") (f (quote (("native_openssl" "openssl") ("default" "async") ("async" "tokio"))))))

