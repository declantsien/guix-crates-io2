(define-module (crates-io sh a2 sha256fast) #:use-module (crates-io))

(define-public crate-sha256fast-0.0.0 (c (n "sha256fast") (v "0.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1khixbdkddcsh5g5sc6lnw89zwa9kdvphi3hf1ja6yk98kqfwc1w") (f (quote (("portable") ("default")))) (y #t)))

(define-public crate-sha256fast-0.0.1 (c (n "sha256fast") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)))) (h "085v3kp1mgc8hfngp1p7ialsba2781f00k91j884a8hjmpwi78kb") (f (quote (("portable") ("default")))) (y #t)))

(define-public crate-sha256fast-0.0.2 (c (n "sha256fast") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)))) (h "180mgqb0fjr3a2w5izblhcmnydh68znci889xd0svydy823c12f9") (f (quote (("portable") ("default")))) (y #t)))

(define-public crate-sha256fast-0.0.3 (c (n "sha256fast") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)))) (h "03fhiiq790axq5wgz3mpw0raw29463gm1by7xmm17715j4h4539g") (f (quote (("portable") ("default"))))))

