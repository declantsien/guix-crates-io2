(define-module (crates-io sh a2 sha256sum) #:use-module (crates-io))

(define-public crate-sha256sum-0.1.0 (c (n "sha256sum") (v "0.1.0") (d (list (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0qslvwp7xj2jbzcm7df99mxvav3nkgyygmv4ddn2xzqjgprwkniv")))

