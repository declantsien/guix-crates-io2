(define-module (crates-io sh a2 sha2-derive) #:use-module (crates-io))

(define-public crate-sha2-derive-0.1.0 (c (n "sha2-derive") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "sha2-derive-proc-macro") (r "^0.1") (d #t) (k 0)))) (h "0b4m6xbnwlblhzdaasypm1hfj9wdl9bm6yffj61fa6vc87r80c8s")))

(define-public crate-sha2-derive-0.1.1 (c (n "sha2-derive") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "sha2-derive-proc-macro") (r "^0.1") (d #t) (k 0)))) (h "0myvri43869gj120zqk2390dgsrmddhnbdha9cf4gzz7ijx718h3")))

(define-public crate-sha2-derive-0.1.2 (c (n "sha2-derive") (v "0.1.2") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "sha2-derive-proc-macro") (r "^0.1") (d #t) (k 0)))) (h "1zmzgpa21q0rdxpdrj8r47ww2n92qh3nj5m25xsznlmqp0fngbqv")))

