(define-module (crates-io sh a2 sha2-derive-proc-macro) #:use-module (crates-io))

(define-public crate-sha2-derive-proc-macro-0.1.0 (c (n "sha2-derive-proc-macro") (v "0.1.0") (h "0g7h38hv86lvn8hbmhkf13sb9fq81s1as726s7i3s08xcvidl0bf")))

(define-public crate-sha2-derive-proc-macro-0.1.1 (c (n "sha2-derive-proc-macro") (v "0.1.1") (h "1dzsw2w4g8crk943alhqb1i9w6fsjpymy82dbn999ifjwih3iawc")))

(define-public crate-sha2-derive-proc-macro-0.1.2 (c (n "sha2-derive-proc-macro") (v "0.1.2") (h "01aaxwpsdf84j7kman7whx9ja1bafcyjb48y2j43j3fm6a0ha9zg")))

(define-public crate-sha2-derive-proc-macro-0.1.3 (c (n "sha2-derive-proc-macro") (v "0.1.3") (h "06ny56m4g85xxac60i56g2ndqwfgipmdaag614j5bbb9la49b6rx")))

(define-public crate-sha2-derive-proc-macro-0.1.4 (c (n "sha2-derive-proc-macro") (v "0.1.4") (h "11a79yaybb6nj5pikix7a6qnqhmnhnk0ndm2556cv21m5c74k32r")))

(define-public crate-sha2-derive-proc-macro-0.1.5 (c (n "sha2-derive-proc-macro") (v "0.1.5") (h "1s6b91hqayhaasv0sn12ivz1qbia5g6y8c6x2rv4d47x9c1glyv5")))

(define-public crate-sha2-derive-proc-macro-0.1.6 (c (n "sha2-derive-proc-macro") (v "0.1.6") (h "1x0np7bg0qb83c5zz3y91wknifm7zmwdd6n467k1z1jlj1nypbqa")))

