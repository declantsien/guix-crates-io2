(define-module (crates-io sh a2 sha2-asm) #:use-module (crates-io))

(define-public crate-sha2-asm-0.1.0 (c (n "sha2-asm") (v "0.1.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1xhxngw3kwx1b5drs1f30sxikdkn4nrg1mc9nng6kfnbrqrmpxpr")))

(define-public crate-sha2-asm-0.2.0 (c (n "sha2-asm") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1j2z4cc0wahrk24a9j2j68fw8v7h58ymc6rgjkmfaddvmsqkhacj")))

(define-public crate-sha2-asm-0.2.1 (c (n "sha2-asm") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1si549g26614l5id2641rbg2c6wqd8n4l50mbq722x57wziypcrr")))

(define-public crate-sha2-asm-0.3.0 (c (n "sha2-asm") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "09id310ngbcv98bww7ns1zmilcagnzhqx2s2skpmf23lzl890c9y")))

(define-public crate-sha2-asm-0.4.0 (c (n "sha2-asm") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "generic-array") (r "^0.10") (d #t) (k 0)))) (h "0pyz0acv4ga40kdqmn837rav04jx13xnxw6acykg27ji0q2qb0ap")))

(define-public crate-sha2-asm-0.5.0 (c (n "sha2-asm") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0a4mgs07yfnlvh4gqrzd7a9hv4p017786i0b6dly80axs98wclgb")))

(define-public crate-sha2-asm-0.5.1 (c (n "sha2-asm") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0kyw2raj0cqi3xan1gc4zfcpcap5x1fnlnffb0ilrfh8957xsnhx")))

(define-public crate-sha2-asm-0.5.2 (c (n "sha2-asm") (v "0.5.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0y245rzdh4azgdmjmjb7bavsf8p18rn067fy7r1msgxsigxqmfla")))

(define-public crate-sha2-asm-0.5.3 (c (n "sha2-asm") (v "0.5.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0k3qwv2yl8fyi2i6cprfb8d874ii5kmcmckgnjkwnz2ac9fayyl1")))

(define-public crate-sha2-asm-0.5.4 (c (n "sha2-asm") (v "0.5.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0y4n8r4362y2fa6p2j0dgny4zfi194gdf01l6j850n9vf8ha3kwj")))

(define-public crate-sha2-asm-0.5.5 (c (n "sha2-asm") (v "0.5.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "192xi35b9qp2ph4fv584z8gy8mr9bsxkbfvb9q9z40k5pqjz5hm7")))

(define-public crate-sha2-asm-0.6.0 (c (n "sha2-asm") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "03j9whmxs758gqcy44zx3n3d3z59jnnhkn3x3kxwg2s916qhbqzy")))

(define-public crate-sha2-asm-0.6.1 (c (n "sha2-asm") (v "0.6.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "08rp21zv96n8cnwcix177xkdw43zx8dqgfjfwd0gly9hvrl7lfaw")))

(define-public crate-sha2-asm-0.6.2 (c (n "sha2-asm") (v "0.6.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1spwvfy7v9s2bfjizpfy634dmnar0k155ikrlkirhlyinmpif9xz")))

(define-public crate-sha2-asm-0.6.3 (c (n "sha2-asm") (v "0.6.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0kp480744vkwg3fqx98379nsdw1lzzzimd88v0qgpqqic03afyzj")))

(define-public crate-sha2-asm-0.6.4 (c (n "sha2-asm") (v "0.6.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ay1vai08d802avl41r0s6r1nrcnzv7jnj5xna34d03mc56j2idq")))

