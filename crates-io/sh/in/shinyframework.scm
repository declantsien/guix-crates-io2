(define-module (crates-io sh in shinyframework) #:use-module (crates-io))

(define-public crate-shinyframework-0.1.0 (c (n "shinyframework") (v "0.1.0") (h "1vb3hpchaxzfdbhc08mjc0ry4gdbbl424886i8lry0fl5f6nssah")))

(define-public crate-shinyframework-0.1.1 (c (n "shinyframework") (v "0.1.1") (d (list (d (n "shinyframework_application") (r "^0.1.1") (d #t) (k 0)) (d (n "shinyframework_common") (r "^0.1.1") (d #t) (k 0)) (d (n "shinyframework_configuration") (r "^0.1.1") (d #t) (k 0)) (d (n "shinyframework_jobs") (r "^0.1.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.86") (d #t) (k 2)))) (h "0qlhark9jxzv8pg7q7w4vwcvjazzk4bypzlwky6mf85gbaim6g4j")))

(define-public crate-shinyframework-0.1.2 (c (n "shinyframework") (v "0.1.2") (d (list (d (n "shinyframework_application") (r "^0.1.2") (d #t) (k 0)) (d (n "shinyframework_common") (r "^0.1.2") (d #t) (k 0)) (d (n "shinyframework_configuration") (r "^0.1.2") (d #t) (k 0)) (d (n "shinyframework_jobs") (r "^0.1.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.86") (d #t) (k 2)))) (h "1kbf8gslvwbvq1hlg63cbms4s3inysqk4i1hl610ia4x5v0h4z42")))

