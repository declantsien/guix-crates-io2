(define-module (crates-io sh in shine-ecs-macro) #:use-module (crates-io))

(define-public crate-shine-ecs-macro-0.2.0 (c (n "shine-ecs-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "147dkva2xdykxqkzh0hacbc5qi3h7vddz9q5hcbmgvc7220k23hq")))

