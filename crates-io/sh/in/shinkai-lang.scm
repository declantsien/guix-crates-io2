(define-module (crates-io sh in shinkai-lang) #:use-module (crates-io))

(define-public crate-shinkai-lang-0.1.0 (c (n "shinkai-lang") (v "0.1.0") (h "0dj9c2ha471m443hgkn6dr3vwlswkkb1igjb1bgv7djx1j1ahw8q") (y #t)))

(define-public crate-shinkai-lang-0.1.1 (c (n "shinkai-lang") (v "0.1.1") (h "1glp5m35sxkyhprw6f2nzw6s6m4aarblipqpy4g32n6c8fna1b0i") (y #t)))

(define-public crate-shinkai-lang-0.1.2 (c (n "shinkai-lang") (v "0.1.2") (h "0vr1xxk6yc1ia83slndgaqlhpp0sgdgcipw887nzjcc34zbccx8h") (y #t)))

