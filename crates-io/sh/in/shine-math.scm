(define-module (crates-io sh in shine-math) #:use-module (crates-io))

(define-public crate-shine-math-0.2.0 (c (n "shine-math") (v "0.2.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "shine-testutils") (r "^0.2.0") (d #t) (k 2)))) (h "1agkcrclvz1jcd0d5v3f9a7dzfc6d5l71pzg71bi2bnzvkz6kbr6")))

