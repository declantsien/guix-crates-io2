(define-module (crates-io sh in shine-testutils) #:use-module (crates-io))

(define-public crate-shine-testutils-0.1.0 (c (n "shine-testutils") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "1715x1hk8y1j10xyc2vkxirgl6q257sd430p8dx5jw6y1b6rzmc8")))

(define-public crate-shine-testutils-0.2.0 (c (n "shine-testutils") (v "0.2.0") (d (list (d (n "actix-files") (r "^0.1.0-beta") (d #t) (k 0)) (d (n "actix-rt") (r "^0.2") (d #t) (k 0)) (d (n "actix-session") (r "^0.1.0-beta") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shine-gltf") (r "^0.2.0") (d #t) (k 0)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "0vsl1jg5wd0yb0p5y881q6kn3fw4j4mbdlv5b6q0a9kihcg31dd7")))

