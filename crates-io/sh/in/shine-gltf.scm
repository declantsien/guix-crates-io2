(define-module (crates-io sh in shine-gltf) #:use-module (crates-io))

(define-public crate-shine-gltf-0.2.0 (c (n "shine-gltf") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shine-gltf-macro") (r "^0.2.0") (d #t) (k 0)))) (h "02kvvprcq3kbbqr1fwf9qx38pxigf1ms1jj7g6ngqnw1lky7pm6y") (f (quote (("default") ("KHR_materials_pbrSpecularGlossiness"))))))

