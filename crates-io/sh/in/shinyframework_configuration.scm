(define-module (crates-io sh in shinyframework_configuration) #:use-module (crates-io))

(define-public crate-shinyframework_configuration-0.1.0 (c (n "shinyframework_configuration") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.53") (d #t) (k 0)))) (h "16y3x7765cdiwf3k1mykkwngwwf1fb937ch02vp2x7ddaqgnir7z")))

(define-public crate-shinyframework_configuration-0.1.1 (c (n "shinyframework_configuration") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.53") (d #t) (k 0)))) (h "0yd2ip27dg5k3chdb5awmpyzcz3ylkhwkqnj9fr2mvrxacfqmi7l")))

(define-public crate-shinyframework_configuration-0.1.2 (c (n "shinyframework_configuration") (v "0.1.2") (d (list (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1w7zb0gv77hxzdngb6lg05i3vj3znljd94h701g311wwhs86mj9x")))

