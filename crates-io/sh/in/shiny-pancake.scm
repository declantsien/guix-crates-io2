(define-module (crates-io sh in shiny-pancake) #:use-module (crates-io))

(define-public crate-shiny-pancake-0.1.0 (c (n "shiny-pancake") (v "0.1.0") (d (list (d (n "piston_window") (r "^0.57.0") (d #t) (k 0)))) (h "1kvrhjn2gdml7jc00z3ab1hxrr557fic1hhpj2kgr4yvs58h9icy")))

(define-public crate-shiny-pancake-0.2.0 (c (n "shiny-pancake") (v "0.2.0") (d (list (d (n "clap") (r "^2.19.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.57.0") (d #t) (k 0)))) (h "0ml5sks7gxm8bfz7hgisjbip7k3b71fg96fqcbk0y8741yrnnclh")))

(define-public crate-shiny-pancake-0.3.0 (c (n "shiny-pancake") (v "0.3.0") (d (list (d (n "clap") (r "^2.19.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.57.0") (d #t) (k 0)))) (h "083blzsyl1805mi2niwdrj4frjxmscidafhmayl3hsr5wqazhpci")))

(define-public crate-shiny-pancake-0.4.0 (c (n "shiny-pancake") (v "0.4.0") (d (list (d (n "clap") (r "^2.19.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.58.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.33.0") (o #t) (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.37.0") (o #t) (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.24.0") (d #t) (k 0)))) (h "0pghib1f5p5shzdxrccbvvqaw9qmwrja8914bg9r4slif7jmzgxd") (f (quote (("sdl2-backend" "pistoncore-sdl2_window") ("glutin-backend" "pistoncore-glutin_window") ("default" "glutin-backend"))))))

(define-public crate-shiny-pancake-1.0.0 (c (n "shiny-pancake") (v "1.0.0") (d (list (d (n "clap") (r "^2.19.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.58.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.33.0") (o #t) (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.37.0") (o #t) (d #t) (k 0)) (d (n "pistoncore-window") (r "^0.24.0") (d #t) (k 0)))) (h "10jxczs0bsaxwrxii76m6c9xa3ghjq259yznmg26g6jsfsf4j081") (f (quote (("sdl2-backend" "pistoncore-sdl2_window") ("glutin-backend" "pistoncore-glutin_window") ("default" "glutin-backend"))))))

