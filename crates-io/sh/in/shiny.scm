(define-module (crates-io sh in shiny) #:use-module (crates-io))

(define-public crate-shiny-0.1.1 (c (n "shiny") (v "0.1.1") (h "1xgvby364b4b5l48h36q1d75ndpr723vkm7cwvcck5gzw47bmn67")))

(define-public crate-shiny-0.1.2 (c (n "shiny") (v "0.1.2") (h "082xdlahgzhx5l2hz7xpcz54zm7s028zg3igvl44l52ah26cjwdz")))

(define-public crate-shiny-0.1.3 (c (n "shiny") (v "0.1.3") (h "1v5bdsi2p9mqn1yn7yh0lj8ksmn3b0s5jmz5c4dzvrpf99zk9fdk")))

