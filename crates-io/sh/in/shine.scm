(define-module (crates-io sh in shine) #:use-module (crates-io))

(define-public crate-shine-0.1.0 (c (n "shine") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "shine-ecs") (r "^0.1.0") (d #t) (k 0)) (d (n "shine-store") (r "^0.2.0") (d #t) (k 0)))) (h "1hyhzn5p4hdvjwfi7pmm2r7zxwmycsmih4n8dgjxkwfbyd9fmlfl")))

