(define-module (crates-io sh in shine-graph-macro) #:use-module (crates-io))

(define-public crate-shine-graph-macro-0.1.1 (c (n "shine-graph-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.6") (f (quote ("full"))) (d #t) (k 0)))) (h "0yfvjyd826bpyw8lfisdjnqgm8mjsyl48jjg2iabjwgkjpnx9qc0")))

(define-public crate-shine-graph-macro-0.2.0 (c (n "shine-graph-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0aw5kinyxqnpj7gg4v3w1a8q2yjqgx8x89vyj81bidrx4rfg8fa4")))

