(define-module (crates-io sh in shine-store) #:use-module (crates-io))

(define-public crate-shine-store-0.1.0 (c (n "shine-store") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)))) (h "0y2jcchhhxxk46gd0kf4b4giy4ydccmsz2zw0y7kql1hpzcdf4zb")))

(define-public crate-shine-store-0.1.1 (c (n "shine-store") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)))) (h "0myqc9xqxiq4i6m2kxay2j4cx79s11sil5faqa53nx2qc7xnf4sr")))

(define-public crate-shine-store-0.2.0 (c (n "shine-store") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "permutohedron") (r "^0.2.4") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "shine-testutils") (r "^0.1.0") (d #t) (k 2)))) (h "1bq8zhjj2n42qzb51nlx8n20q0bdi5bhzr857bmqbgwai94fm8ad")))

