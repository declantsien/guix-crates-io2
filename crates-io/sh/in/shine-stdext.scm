(define-module (crates-io sh in shine-stdext) #:use-module (crates-io))

(define-public crate-shine-stdext-0.2.0 (c (n "shine-stdext") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "shine-testutils") (r "^0.2.0") (d #t) (k 2)))) (h "0k9pialds6mwx0y4v6nskw094zc5bhjf5wliigff5wawr99q8x1z")))

