(define-module (crates-io sh in shine-input) #:use-module (crates-io))

(define-public crate-shine-input-0.2.0 (c (n "shine-input") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gilrs") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.19") (d #t) (k 0)))) (h "19g80ryr9bb0b3yg89zv3f931k0vlki8ay4yy95y2n5myvcinb8i")))

(define-public crate-shine-input-0.2.1 (c (n "shine-input") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gilrs") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.18") (d #t) (k 0)))) (h "0y5cj06d500m4dmxz9pfyfainz4vww3py0rbfnsgj6w4xlhp3r0w")))

