(define-module (crates-io sh in shine-shard) #:use-module (crates-io))

(define-public crate-shine-shard-0.2.0 (c (n "shine-shard") (v "0.2.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "shine-ecs") (r "^0.2.0") (d #t) (k 0)) (d (n "shine-ecs-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "shine-graph") (r "^0.2.0") (d #t) (k 0)) (d (n "shine-math") (r "^0.2.0") (d #t) (k 0)) (d (n "shine-stdext") (r "^0.2.0") (d #t) (k 0)) (d (n "shine-testutils") (r "^0.2.0") (d #t) (k 2)))) (h "1ylqkrljyj3lk68fc53nlwhzr5aj5gmz9lvr2nyj3vhl84dmlxc3")))

