(define-module (crates-io sh in shingles) #:use-module (crates-io))

(define-public crate-shingles-0.1.0 (c (n "shingles") (v "0.1.0") (h "0x821vq8w3wzy0jaakmm3b0hp63d5g75f0s0mrgwgb5vhx6lxxhz")))

(define-public crate-shingles-0.1.1 (c (n "shingles") (v "0.1.1") (h "182w5fvvjn6ihybyk2hgrq815rxfyln6gr1vhzaxxyyxw55qifvj")))

