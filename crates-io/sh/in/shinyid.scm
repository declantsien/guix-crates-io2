(define-module (crates-io sh in shinyid) #:use-module (crates-io))

(define-public crate-shinyid-0.1.0 (c (n "shinyid") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0kbwp2ggakfp0i5rpzxxvgmqcm4wb577cq9yshdp9d8xx5ls2qm8")))

(define-public crate-shinyid-0.2.0 (c (n "shinyid") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0b6bzlqyk70nl1jkgpp0qcvhrfka973apj8y1vay75m9adjz91f5")))

