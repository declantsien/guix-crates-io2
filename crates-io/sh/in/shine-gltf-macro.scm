(define-module (crates-io sh in shine-gltf-macro) #:use-module (crates-io))

(define-public crate-shine-gltf-macro-0.2.0 (c (n "shine-gltf-macro") (v "0.2.0") (d (list (d (n "inflections") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1mcawglzl270dghnr496kcnrpcrlbsdkx2i3mhalmjmi5n0bw4qi")))

