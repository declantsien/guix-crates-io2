(define-module (crates-io sh in shinyframework_common) #:use-module (crates-io))

(define-public crate-shinyframework_common-0.1.0 (c (n "shinyframework_common") (v "0.1.0") (h "0ihk52nwdzmjlmbwp1jxqm64q002p7raz0910ldqy83dbnhm4fjm")))

(define-public crate-shinyframework_common-0.1.1 (c (n "shinyframework_common") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (d #t) (k 0)) (d (n "shinyframework_configuration") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("io"))) (d #t) (k 0)))) (h "1qwvhyrky3jlydfrgha7758yqfn5g83c5g42pzffkg07d0ni8m69")))

(define-public crate-shinyframework_common-0.1.2 (c (n "shinyframework_common") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "shinyframework_configuration") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("io"))) (d #t) (k 0)))) (h "04k47xc17732wdxviji5r8xlccnhzgbdsqg6anlbwj82w42msp5k")))

