(define-module (crates-io sh en shentong) #:use-module (crates-io))

(define-public crate-shentong-0.5.5 (c (n "shentong") (v "0.5.5") (h "048hhss47y47cbzqiwiwmcbv25c3ydaaal0mmrvfqxxyvi2y968s") (f (quote (("aq_unstable")))) (y #t)))

(define-public crate-shentong-0.5.6 (c (n "shentong") (v "0.5.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "oracle_procmacro") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 2)))) (h "03nyvxxzmzz1hx139dmw96g1yjd29bld7as6jkwi15yqiynw87v4") (f (quote (("aq_unstable")))) (y #t)))

(define-public crate-shentong-0.5.7 (c (n "shentong") (v "0.5.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "oracle_procmacro") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 2)))) (h "185s1c3q46xgpk4x1jgnrf9rba566mivxrzak52g0074zw6g6dnh") (f (quote (("aq_unstable"))))))

(define-public crate-shentong-0.5.8 (c (n "shentong") (v "0.5.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "oracle_procmacro") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 2)))) (h "1vp7gw8jix17c6a5bnhnl98wqkxbjq9bdwcmp7hk3i5ga4ad079f") (f (quote (("aq_unstable"))))))

