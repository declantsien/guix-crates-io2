(define-module (crates-io sh en shen-nbt5) #:use-module (crates-io))

(define-public crate-shen-nbt5-0.4.0 (c (n "shen-nbt5") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0031f1kf89zsrf6k5m4gnb6afl6bykrlbfx8m12qrd5xdqfyd2j3") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-shen-nbt5-0.4.1 (c (n "shen-nbt5") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10nv93a4ih5qypb7m9rllv88qimi5nb0v49r200w9qq32yzyngf0") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-shen-nbt5-0.4.2 (c (n "shen-nbt5") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wg6p58qp5vnrc3wv2abfdycbi8iyv6sxwgw0wv1zgi58w6s6lfk") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-shen-nbt5-0.4.3 (c (n "shen-nbt5") (v "0.4.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xndblqh5c0nkrhb54g3n6vwc9lhnyiq4bppy9rb2r8zsd8pw2gm") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-shen-nbt5-0.4.4 (c (n "shen-nbt5") (v "0.4.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18dh071nf4qyzwgz3zfcqbck0k2sjgbki9y6klbx39qkvf60fyh2") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

