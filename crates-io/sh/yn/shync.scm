(define-module (crates-io sh yn shync) #:use-module (crates-io))

(define-public crate-shync-0.1.0 (c (n "shync") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)))) (h "1cn3qpwwkfmk9dlyy8z4dmqn29aj8bb59yzs8mlmcq65lnin9lvf")))

(define-public crate-shync-0.1.1 (c (n "shync") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)))) (h "1w9xk62ng75qc24ja1lsb4qdnwrmi420igfsf44g122823f09jr5")))

