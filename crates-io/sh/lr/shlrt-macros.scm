(define-module (crates-io sh lr shlrt-macros) #:use-module (crates-io))

(define-public crate-shlrt-macros-0.0.1 (c (n "shlrt-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "1sdgm5sn70f1v5hfamwlb8zlshgsvzk0vxnmf39i999174hn8q8y")))

