(define-module (crates-io sh tc shtcx) #:use-module (crates-io))

(define-public crate-shtcx-0.1.0 (c (n "shtcx") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "tui") (r "^0.8") (d #t) (k 2)))) (h "0cgvvg19lxz9awzxqbg2hikq76ybg94665a4r37igxwb02m3g43a") (f (quote (("default"))))))

(define-public crate-shtcx-0.9.0 (c (n "shtcx") (v "0.9.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "tui") (r "^0.8") (d #t) (k 2)))) (h "030m5vx850rsjmiac9pp9lci20xb7ayabm3aiwgndqr0dl1sh951") (f (quote (("default"))))))

(define-public crate-shtcx-0.10.0 (c (n "shtcx") (v "0.10.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "tui") (r "^0.8") (d #t) (k 2)))) (h "1ipx81bf3z9q16splj3rch8badv7j0f2s0y2bah52pphggzk6hcy") (f (quote (("default"))))))

(define-public crate-shtcx-0.11.0 (c (n "shtcx") (v "0.11.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "termion") (r "^1.5") (d #t) (k 2)) (d (n "tui") (r "^0.8") (d #t) (k 2)))) (h "0kcskr30als34ypxhbdmfgrxi9s239hlw926wa9caslvy0215ggm") (f (quote (("default"))))))

(define-public crate-shtcx-1.0.0 (c (n "shtcx") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)) (d (n "termion") (r "^4.0") (d #t) (k 2)) (d (n "tui") (r "^0.19") (d #t) (k 2)))) (h "17r6m5ipxgcdn282n6h9gxrxam3gpv72idd5c28y3ikqdipdc99c") (f (quote (("default"))))))

