(define-module (crates-io sh tc shtc1) #:use-module (crates-io))

(define-public crate-shtc1-0.1.0 (c (n "shtc1") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)))) (h "0wvq8rrcxkxhhg0119q1lrnc327jrpar3knpdlhh7kxp144j90d7")))

