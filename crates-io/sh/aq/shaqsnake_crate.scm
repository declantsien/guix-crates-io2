(define-module (crates-io sh aq shaqsnake_crate) #:use-module (crates-io))

(define-public crate-shaqsnake_crate-0.1.0 (c (n "shaqsnake_crate") (v "0.1.0") (h "0j7l68z4lq3izjrg9v012n5rw37azfbrg4im024palssk81b94la")))

(define-public crate-shaqsnake_crate-0.1.1 (c (n "shaqsnake_crate") (v "0.1.1") (h "1dpyy9h9kdf79l9i3xpsbmj4x1l5a1vk51gdp17vq4kdpw8rd0cl")))

