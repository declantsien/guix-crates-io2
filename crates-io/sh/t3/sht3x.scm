(define-module (crates-io sh t3 sht3x) #:use-module (crates-io))

(define-public crate-sht3x-0.1.1 (c (n "sht3x") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.1.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)))) (h "0ska6prk0vfvmyy5z19vfqxa2ycd4c2cmx57frykab9p5xlfryh2")))

