(define-module (crates-io sh t3 sht31) #:use-module (crates-io))

(define-public crate-sht31-0.1.0 (c (n "sht31") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-svc") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "18shbbbgyh32671fkh7ir79a2jvxv3s5g2r329ssaw8gasd923k8") (f (quote (("esp32-fix"))))))

(define-public crate-sht31-0.1.1 (c (n "sht31") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-svc") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "12rdlmmlmd8ww2shl12vshhgi5ilw58zc8h776knr2xqw3lzkxvj") (f (quote (("esp32-fix"))))))

(define-public crate-sht31-0.2.0 (c (n "sht31") (v "0.2.0") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "0d30is11nvjzqsk4cr5a50jacwaj10c68ikdskfjicf840fvqq05") (f (quote (("esp32-fix")))) (y #t) (s 2) (e (quote (("thiserror" "dep:thiserror"))))))

(define-public crate-sht31-0.2.1 (c (n "sht31") (v "0.2.1") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "1jvgnl9pifb2cra1qpr9qn95wzm4jglry0865p38dns9dqdcxbj4") (s 2) (e (quote (("thiserror" "dep:thiserror"))))))

(define-public crate-sht31-0.3.0 (c (n "sht31") (v "0.3.0") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "18fcz1rijhf3m553k1hgi9bjzl91kav7lvr6f6x1al854wjcsa15") (s 2) (e (quote (("thiserror" "dep:thiserror"))))))

