(define-module (crates-io sh ar shared-resource-ipc) #:use-module (crates-io))

(define-public crate-shared-resource-ipc-0.1.0 (c (n "shared-resource-ipc") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0cb1fxiabxcf7176n6lpj20nfhb15r1rdzarjv01vs63rdvh03fw")))

