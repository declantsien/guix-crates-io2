(define-module (crates-io sh ar sharedvec) #:use-module (crates-io))

(define-public crate-sharedvec-0.1.2 (c (n "sharedvec") (v "0.1.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0wrhpb485q8s19ffp2rblvkmsnx71cyiggvj2rc15c8nviavaygk")))

(define-public crate-sharedvec-0.2.0 (c (n "sharedvec") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1kcxcq9zczblkz64y5x7054h1rjz4frqblx5qky1ccysickp5az2")))

