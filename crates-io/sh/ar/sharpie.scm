(define-module (crates-io sh ar sharpie) #:use-module (crates-io))

(define-public crate-sharpie-0.1.0 (c (n "sharpie") (v "0.1.0") (d (list (d (n "pem") (r "^1.1.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "base64") (r "^0.21.0") (d #t) (k 2)))) (h "0kzjnpnl5pvc6wypgv5bjms4g7kjdgzm7675pnm0ymdwpvfp380v")))

(define-public crate-sharpie-0.1.1 (c (n "sharpie") (v "0.1.1") (d (list (d (n "pem") (r "^1.1.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "base64") (r "^0.21.0") (d #t) (k 2)))) (h "1mwcccvvyqhy62w4p6m5vs7f2dxfvwzn9kq2zgvw5g2a5ld17dfy")))

(define-public crate-sharpie-0.1.2 (c (n "sharpie") (v "0.1.2") (d (list (d (n "pem") (r "^1.1.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "base64") (r "^0.21.0") (d #t) (k 2)))) (h "1vfqkl4fqy9050cg3vjq1kpd1997ff4vislnz5qf3xsm3wcnd3yz")))

(define-public crate-sharpie-0.2.0 (c (n "sharpie") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "pem") (r "^1.1.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "base64") (r "^0.21.0") (d #t) (k 2)))) (h "0cldb7fiff4x67wbyj5qbkybifkjzmxji3xg4m13zz1gr9frjflf") (f (quote (("default" "base64")))) (s 2) (e (quote (("base64" "dep:base64"))))))

