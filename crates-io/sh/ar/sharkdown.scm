(define-module (crates-io sh ar sharkdown) #:use-module (crates-io))

(define-public crate-sharkdown-0.1.0 (c (n "sharkdown") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0c4kd1z2f2aj3g5w465l7h2hp6qqwcn5pqm3kbv8myc7zi6qac1n")))

(define-public crate-sharkdown-0.1.1 (c (n "sharkdown") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1kq13mb2r258ycbn5cbx49li4sjwbhdxmzgv98r1dh1i682mvlca")))

