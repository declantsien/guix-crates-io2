(define-module (crates-io sh ar sharp_lr35902_assembler) #:use-module (crates-io))

(define-public crate-sharp_lr35902_assembler-0.1.0 (c (n "sharp_lr35902_assembler") (v "0.1.0") (h "1a71sbdf7lwwr54lmbwfq7flglwlsfgwar89hidwgaiwafn72rbq")))

(define-public crate-sharp_lr35902_assembler-0.1.1 (c (n "sharp_lr35902_assembler") (v "0.1.1") (h "1yr4i2vhiqabigh0gq0nnbbmmasrm3w88l3bdpnw2ciflndglns6")))

