(define-module (crates-io sh ar shared-iter) #:use-module (crates-io))

(define-public crate-shared-iter-0.1.0 (c (n "shared-iter") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1ml9lpvvjv4lprlsksxswvq4yv3hfw8ivfp3sq9zpqff256wki5i")))

(define-public crate-shared-iter-0.1.1 (c (n "shared-iter") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0f02gbzvv6yajp6296q240ljrmc2fnd0wsvxm6718qczr2zizzml")))

(define-public crate-shared-iter-0.2.0 (c (n "shared-iter") (v "0.2.0") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0nxyy2hcq7a7l4f8fjk9gzg7gc13kr4c86iv6wn84yx8dyjx9q8s")))

