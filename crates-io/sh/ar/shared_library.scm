(define-module (crates-io sh ar shared_library) #:use-module (crates-io))

(define-public crate-shared_library-0.1.0 (c (n "shared_library") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "05czbzpb1qkj3z9a7w5n6r95zd96xqbjc9wmq7pxwgi6ywm2ila3")))

(define-public crate-shared_library-0.2.0 (c (n "shared_library") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1iqsdbyral3jdfsqa7kf2b267lk5y1nwmyy7dki862bbrc5bzlrr") (y #t)))

(define-public crate-shared_library-0.2.1 (c (n "shared_library") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n2c36c40d57m8rm3acg8540v0yg9wj7slkm517lpyrna8alz3bi") (y #t)))

(define-public crate-shared_library-0.2.2 (c (n "shared_library") (v "0.2.2") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03pggrxrfxr12crnxkmv8m7jmjggil401ndj040pj31b1xcf703a") (y #t)))

(define-public crate-shared_library-0.1.1 (c (n "shared_library") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1787jdjh3njsifrb2r9xc9f29dmfnkgw73xd7q91w21p4lqfxwhp")))

(define-public crate-shared_library-0.1.2 (c (n "shared_library") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cafchhz08d53p6hxhmfm2g2sj7rxl06762xxmqkbb7xcj007avn")))

(define-public crate-shared_library-0.1.3 (c (n "shared_library") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15jml7syx0dqfw52066vjrqdi1zsgvd3ri8wckd3vxbb53vk1bs7")))

(define-public crate-shared_library-0.1.4 (c (n "shared_library") (v "0.1.4") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jgha31i1dmp15c7qsmqd4f5mizsynw2l88nvq91ma2iybgrv5z0")))

(define-public crate-shared_library-0.1.5 (c (n "shared_library") (v "0.1.5") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16m7617x89nmpi1ilzhahpr3bidn0x18zldnnl7p3lngdxmi417v")))

(define-public crate-shared_library-0.1.6 (c (n "shared_library") (v "0.1.6") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "131zmwry9sypvzc01nzvcn23fzxc6iffp8bf2ahfjfv1gsqx09qj")))

(define-public crate-shared_library-0.1.7 (c (n "shared_library") (v "0.1.7") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vlwa3388apsz70d24dqq370qx0islncfjkyzln5a922h78gj8kq")))

(define-public crate-shared_library-0.1.8 (c (n "shared_library") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hc9s8dskh0ni3wgwzsvgqlwbbc8aj647pn6gk6dgn74ih4vym42")))

(define-public crate-shared_library-0.1.9 (c (n "shared_library") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04fs37kdak051hm524a360978g58ayrcarjsbf54vqps5c7px7js")))

