(define-module (crates-io sh ar shared-local-state) #:use-module (crates-io))

(define-public crate-shared-local-state-0.1.0 (c (n "shared-local-state") (v "0.1.0") (h "052xc4paacnyl520n3plzw1d1l82yvqf8sg269jq1l19rl286p40")))

(define-public crate-shared-local-state-0.1.1 (c (n "shared-local-state") (v "0.1.1") (h "09g8cx1xxcqm52l5rymv3yns5hpiswm7p9g8l707sv54n5ywjg6m")))

(define-public crate-shared-local-state-0.1.4 (c (n "shared-local-state") (v "0.1.4") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0aa9pfdv55a25smq45bv32454c0ay7jx3gsss4p7flajyjrcql1s")))

