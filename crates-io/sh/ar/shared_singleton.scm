(define-module (crates-io sh ar shared_singleton) #:use-module (crates-io))

(define-public crate-shared_singleton-0.1.0 (c (n "shared_singleton") (v "0.1.0") (h "17ngrz1nd2525k3vxf197agv5wq5y05s7ic7jmpk7hp0s3j4jjcf")))

(define-public crate-shared_singleton-0.2.0 (c (n "shared_singleton") (v "0.2.0") (d (list (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "1srsgrh38cc9ncrvjq51jijfk0il2zb5chkjm3cdknqrv4qcwgrs") (y #t) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-shared_singleton-0.2.1 (c (n "shared_singleton") (v "0.2.1") (d (list (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "1c6081vq7av8ja7b5p0j43lf0b94ryfh8qqjglhnxyk9xm5kkjih") (s 2) (e (quote (("tokio" "dep:tokio"))))))

