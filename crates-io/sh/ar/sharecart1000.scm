(define-module (crates-io sh ar sharecart1000) #:use-module (crates-io))

(define-public crate-sharecart1000-1.0.0 (c (n "sharecart1000") (v "1.0.0") (d (list (d (n "rust-ini") (r "^0.11") (d #t) (k 0)))) (h "0aw24v6sir00cj55ib55k0xv0rw0nci2j1ch4w4cz8ydvyk15wxg")))

(define-public crate-sharecart1000-1.0.1 (c (n "sharecart1000") (v "1.0.1") (d (list (d (n "rust-ini") (r "^0.11") (d #t) (k 0)))) (h "06nh8k74hmr3bcagd48apdsgxmf6p875l06gpy73xiw2n7f38cb2")))

