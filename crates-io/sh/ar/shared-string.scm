(define-module (crates-io sh ar shared-string) #:use-module (crates-io))

(define-public crate-shared-string-0.1.0 (c (n "shared-string") (v "0.1.0") (h "04njscmv45ni7idzrfr7k5cfk0jg5j9kpjz5pqmw92rn1q02ay4b")))

(define-public crate-shared-string-0.1.1 (c (n "shared-string") (v "0.1.1") (h "1aapkfa6wzvxn48dw4fl2pw9a3xpji0anx1ff79v51b6bsfg61rr")))

(define-public crate-shared-string-0.1.2 (c (n "shared-string") (v "0.1.2") (h "0hnsydpdz77dyhj558h101v48b71vfn6qgy4mnmp7jmanx2kgpd3")))

(define-public crate-shared-string-0.1.3 (c (n "shared-string") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "01092c1r1gnqrhrcjbl32gzryn1sr56aqvj4hsvalyv6bwvy04yv")))

(define-public crate-shared-string-0.1.4 (c (n "shared-string") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1zgvcy1p84p2zxqp73irk8hgrx74b1m0gvj6nx4gk102lzscriyb")))

(define-public crate-shared-string-0.1.5 (c (n "shared-string") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0z2q4b9gp0w0hphf1skkkvd980acia0nqxfzf2m4azvzyrvc5hal")))

(define-public crate-shared-string-0.1.6 (c (n "shared-string") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0rczi5qqcq21hc4d4lzmv6jn3cs1cakclgx4dijr5x0h81x8r7x0")))

(define-public crate-shared-string-0.1.7 (c (n "shared-string") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "19rw431fz2ywhygsiki4kwqwq824i0ac4wldk7fwk9175csblmch")))

