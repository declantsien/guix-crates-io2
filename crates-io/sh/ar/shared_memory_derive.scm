(define-module (crates-io sh ar shared_memory_derive) #:use-module (crates-io))

(define-public crate-shared_memory_derive-0.10.0 (c (n "shared_memory_derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "shared_memory") (r "0.*") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0fnx4lsc8isicxxw175amzhgm75fz19051k99sqg1qjb63qi8ykn")))

