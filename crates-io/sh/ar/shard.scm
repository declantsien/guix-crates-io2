(define-module (crates-io sh ar shard) #:use-module (crates-io))

(define-public crate-shard-0.1.0 (c (n "shard") (v "0.1.0") (h "168drv99ml81g0zaq0gk052sh832azk5lgady9h90gjhr94akg1p")))

(define-public crate-shard-0.1.1 (c (n "shard") (v "0.1.1") (h "0kjgji79j4c6dvifrkxdak7rr4mx8n26ipv9cq8ynsbb5v5d35kq")))

(define-public crate-shard-0.1.2 (c (n "shard") (v "0.1.2") (h "1nhvgha182myzaf541c45cs8dcmpslh8mr780lnqgmh9a8m27wby")))

