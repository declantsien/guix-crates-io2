(define-module (crates-io sh ar sharded_rwlock) #:use-module (crates-io))

(define-public crate-sharded_rwlock-0.0.0 (c (n "sharded_rwlock") (v "0.0.0") (d (list (d (n "parking_lot") (r ">=0.11, <=0.12") (d #t) (k 0)))) (h "0b4cp7hd0vxp2pdwy4yndf117d5n3y77n01gcz7cjpq5pal1fh4a")))

