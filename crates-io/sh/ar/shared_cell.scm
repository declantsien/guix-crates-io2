(define-module (crates-io sh ar shared_cell) #:use-module (crates-io))

(define-public crate-shared_cell-0.1.0 (c (n "shared_cell") (v "0.1.0") (d (list (d (n "async_main") (r "^0.4") (f (quote ("futures"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pasts") (r "^0.14") (d #t) (k 2)) (d (n "tokio") (r "^1.30") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "traitful") (r "^0.2") (d #t) (k 0)) (d (n "whisk") (r "^0.12") (d #t) (k 2)))) (h "1ciqfvb8nzlgq01qmqb3rc3jjkrcyz14q1mlys36v526748miny3") (f (quote (("default") ("alloc")))) (y #t)))

(define-public crate-shared_cell-0.1.1 (c (n "shared_cell") (v "0.1.1") (d (list (d (n "async_main") (r "^0.4") (f (quote ("futures"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pasts") (r "^0.14") (d #t) (k 2)) (d (n "tokio") (r "^1.30") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "traitful") (r "^0.2") (d #t) (k 0)) (d (n "whisk") (r "^0.12") (d #t) (k 2)))) (h "027n6rjn749fcsdap2j7ym808ablb0kl4zbpcgn08cxfzp0ccpc9") (f (quote (("default") ("alloc")))) (y #t)))

(define-public crate-shared_cell-0.2.0 (c (n "shared_cell") (v "0.2.0") (d (list (d (n "async_main") (r "^0.4") (f (quote ("futures"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pasts") (r "^0.14") (d #t) (k 2)) (d (n "tokio") (r "^1.30") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "whisk") (r "^0.12") (d #t) (k 2)))) (h "1bw1pqa9yf01in7hckpldmir1ag3sx0y9znndbvizm2xvx3acxqr") (f (quote (("default") ("alloc")))) (y #t)))

(define-public crate-shared_cell-0.3.0 (c (n "shared_cell") (v "0.3.0") (d (list (d (n "async_main") (r "^0.4") (f (quote ("futures"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pasts") (r "^0.14") (d #t) (k 2)) (d (n "tokio") (r "^1.30") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "whisk") (r "^0.12") (d #t) (k 2)))) (h "09j1kczcwqpnk1acjy4i97yj9gbkppgaydcm0p71299p638rrjz7")))

(define-public crate-shared_cell-0.4.0 (c (n "shared_cell") (v "0.4.0") (d (list (d (n "async_main") (r "^0.4") (f (quote ("futures"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pasts") (r "^0.14") (d #t) (k 2)) (d (n "tokio") (r "^1.30") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "whisk") (r "^0.12") (d #t) (k 2)))) (h "1fxajd68ziphfd4q2m4780dwllhbpbr8bzm6vzx45777hng1kgpg")))

(define-public crate-shared_cell-0.5.0 (c (n "shared_cell") (v "0.5.0") (d (list (d (n "async_main") (r "^0.4") (f (quote ("futures"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pasts") (r "^0.14") (d #t) (k 2)) (d (n "tokio") (r "^1.30") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "whisk") (r "^0.12") (d #t) (k 2)))) (h "0yd9n3ixawnyn23mqcwqqd3vdh2qhk2c09750dxbbzywvijj78rc")))

