(define-module (crates-io sh ar shared_resources) #:use-module (crates-io))

(define-public crate-shared_resources-0.1.0 (c (n "shared_resources") (v "0.1.0") (d (list (d (n "atomic_refcell") (r "^0.1.13") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)))) (h "0swy0wrhnc7hb7ryh3jrsbw40sqprq667m5ykr1q7igslz8qs51m")))

