(define-module (crates-io sh ar shared_error) #:use-module (crates-io))

(define-public crate-shared_error-0.1.0 (c (n "shared_error") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "slog") (r "^2.7") (f (quote ("max_level_trace" "nested-values"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0cms5fvgm8vhzxw0qmw79yz9xr8qcfg3z5fcfsv3hvc0i9lxw6m7")))

