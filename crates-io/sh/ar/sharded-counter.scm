(define-module (crates-io sh ar sharded-counter) #:use-module (crates-io))

(define-public crate-sharded-counter-0.1.0 (c (n "sharded-counter") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "0kjfarc5g4icbvhvmck970rzwi64y9wgplgii11jbzpqap3s7byb")))

