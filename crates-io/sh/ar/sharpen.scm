(define-module (crates-io sh ar sharpen) #:use-module (crates-io))

(define-public crate-sharpen-0.0.1 (c (n "sharpen") (v "0.0.1") (h "1h0mf48w3awh9q3jriz5r130pv897p8q874yr7h8rjiz8yzhfrx4")))

(define-public crate-sharpen-0.0.2 (c (n "sharpen") (v "0.0.2") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)))) (h "05w482a3hip4xjzxg048k3jdiaaa64gnycrd17kf6sl8x5q1iysp")))

(define-public crate-sharpen-0.0.3 (c (n "sharpen") (v "0.0.3") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)))) (h "0zb90kvn3ngrqgy4vbx2plgrz73fr08fhsz8xs5r72iq13k8x93g")))

(define-public crate-sharpen-0.0.4 (c (n "sharpen") (v "0.0.4") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)))) (h "07cja8vpnk1j7hawcwzb4qgs83zlvv09zwf17hacb5m4dgbx0ps3")))

(define-public crate-sharpen-0.0.5 (c (n "sharpen") (v "0.0.5") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)))) (h "07a167zkq9npqrk3qxpvwjrnasrjldwwkrqskhhm00fg2snz7bg4")))

(define-public crate-sharpen-0.1.0 (c (n "sharpen") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)))) (h "1yfyrpn5d70v0bamkd2slkhzy62cdd22qcmv83zbrz1kzb0llpvl")))

