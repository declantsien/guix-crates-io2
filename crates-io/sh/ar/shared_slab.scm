(define-module (crates-io sh ar shared_slab) #:use-module (crates-io))

(define-public crate-shared_slab-0.0.1 (c (n "shared_slab") (v "0.0.1") (h "1qkd3jxxz4lg7wf5v53gln2z4b59zw8xz6kf5vndxq5h54qp2jm2")))

(define-public crate-shared_slab-0.1.0 (c (n "shared_slab") (v "0.1.0") (h "057y3mb1c4qsngqvjjgg0w1cshm7hqb9zq51cby841syrdvr1xdd")))

