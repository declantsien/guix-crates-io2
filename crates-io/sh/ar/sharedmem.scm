(define-module (crates-io sh ar sharedmem) #:use-module (crates-io))

(define-public crate-sharedmem-0.1.0 (c (n "sharedmem") (v "0.1.0") (d (list (d (n "memmap") (r "^0.5") (d #t) (k 0)))) (h "0i0h9k5fyh4yvci6aw908i9kd2y1xw2pcm63qva2wadz42kp8mgs") (y #t)))

(define-public crate-sharedmem-0.1.1 (c (n "sharedmem") (v "0.1.1") (d (list (d (n "memmap") (r "^0.5") (d #t) (k 0)))) (h "1scz34mgsnck3m43ybc5ml4x6zrrvqg5qxivzhw3mqlnhmx6y22d")))

