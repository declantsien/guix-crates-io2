(define-module (crates-io sh ar shardize-core) #:use-module (crates-io))

(define-public crate-shardize-core-0.1.0 (c (n "shardize-core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0f6fj8xnw1v6mjlc6zcsypfblgkrf8y1jdhb6gg5517qf7w3kdvj")))

