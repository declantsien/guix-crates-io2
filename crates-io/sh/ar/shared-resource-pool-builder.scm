(define-module (crates-io sh ar shared-resource-pool-builder) #:use-module (crates-io))

(define-public crate-shared-resource-pool-builder-0.1.0 (c (n "shared-resource-pool-builder") (v "0.1.0") (d (list (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0y7l5jwqiby6rd37inkkr6fvxc40g852zi81a6crziw57dwylgmy")))

(define-public crate-shared-resource-pool-builder-0.2.0 (c (n "shared-resource-pool-builder") (v "0.2.0") (d (list (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1ix53z7dh2jkhs5f2ivq45ymwsfq3f3bxkny1smxkx0dfmlhprbi")))

(define-public crate-shared-resource-pool-builder-0.2.1 (c (n "shared-resource-pool-builder") (v "0.2.1") (d (list (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "03qd064fhx03j0xl02sfnf0j7apxxpsb9z01dm7rkd3zs2xxh3kl")))

