(define-module (crates-io sh ar sharky) #:use-module (crates-io))

(define-public crate-sharky-0.1.0 (c (n "sharky") (v "0.1.0") (d (list (d (n "sharks") (r "^0.4") (d #t) (k 0)))) (h "135dq97zdhhzr6xdmis1vg7lhgz74ibl363allmakbqfvjnshkii") (r "1.66")))

(define-public crate-sharky-0.1.1 (c (n "sharky") (v "0.1.1") (d (list (d (n "sharks") (r "^0.4") (d #t) (k 0)))) (h "1b45ak4xhz25qjc4gdy5xmxmcaa10cm74528ijvd0h1mfnbaih9s") (r "1.66")))

(define-public crate-sharky-0.1.2 (c (n "sharky") (v "0.1.2") (d (list (d (n "sharks") (r "^0.4") (d #t) (k 0)))) (h "16i0hdy1zcg3hj3djm2n4zp53ycmjqdblyghx2p74qrw36j3kb7y") (r "1.66")))

