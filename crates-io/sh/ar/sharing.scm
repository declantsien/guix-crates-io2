(define-module (crates-io sh ar sharing) #:use-module (crates-io))

(define-public crate-sharing-0.0.0 (c (n "sharing") (v "0.0.0") (h "16sg6s37vgvryj890kbam27spm9dnnmlbnigwhzclh6pj094y3rv")))

(define-public crate-sharing-0.1.0 (c (n "sharing") (v "0.1.0") (d (list (d (n "chacha20") (r "^0.2.1") (d #t) (k 2)) (d (n "gf") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3.2") (d #t) (k 0)))) (h "05xh78f20j3w2d4k9w97fqw6cxrrpiarbk9qa6cyqfgwb7d21svs")))

(define-public crate-sharing-0.1.1 (c (n "sharing") (v "0.1.1") (d (list (d (n "chacha20") (r "^0.2.1") (d #t) (k 2)) (d (n "gf") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3.2") (d #t) (k 0)))) (h "1yg086mmb9p541v4ccmn9gymaqmh3d56hlj867g6xrif8k7cb423")))

(define-public crate-sharing-0.1.2 (c (n "sharing") (v "0.1.2") (d (list (d (n "chacha20") (r "^0.2.1") (d #t) (k 2)) (d (n "gf") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3.2") (d #t) (k 0)))) (h "0pjm196rp4cd120faq6klcl9kl8j5jy6zmkpgga3avllknhq9msa")))

