(define-module (crates-io sh ar shared_bytes) #:use-module (crates-io))

(define-public crate-shared_bytes-0.1.0-beta.4 (c (n "shared_bytes") (v "0.1.0-beta.4") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "06cyfrw3nmjmriv1y1wvgwb6243sb04z6j05yag3rblaqa6fcvlk") (f (quote (("unstable_feature_unsafe_optimizations") ("unstable_feature_no_unsafe_optimizations"))))))

