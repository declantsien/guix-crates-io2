(define-module (crates-io sh ar sharded-log) #:use-module (crates-io))

(define-public crate-sharded-log-0.0.0 (c (n "sharded-log") (v "0.0.0") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fault-injection") (r "^1.0.1") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "0xlg07760pm4zyi9ndqd2p7ihc90p3r5zd96mmf25knh5f9f13jr")))

(define-public crate-sharded-log-0.0.1 (c (n "sharded-log") (v "0.0.1") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fault-injection") (r "^1.0.1") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)))) (h "18vj27pfcclpnnh4b7qqcl7r6zz3bkfw5bccj5kplv585fnnpzq0")))

