(define-module (crates-io sh ar shared_hashmap) #:use-module (crates-io))

(define-public crate-shared_hashmap-0.1.0 (c (n "shared_hashmap") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("process"))) (d #t) (k 2)) (d (n "raw_sync") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "shared_memory") (r "^0.12.4") (d #t) (k 0)))) (h "1idy2ibh80in2wwa4hvkv2drq0w7dhfzrn55cl82z0jb4ginsp32")))

(define-public crate-shared_hashmap-0.1.1 (c (n "shared_hashmap") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("process"))) (d #t) (k 2)) (d (n "raw_sync") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "shared_memory") (r "^0.12.4") (d #t) (k 0)))) (h "1ihmh6hmhiaf15lbnlz3hag4wrgjr24h2z48ijw6lv3l07cywrvp")))

(define-public crate-shared_hashmap-0.1.2 (c (n "shared_hashmap") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("process"))) (d #t) (k 2)) (d (n "raw_sync") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "shared_memory") (r "^0.12.4") (d #t) (k 0)))) (h "0cvim67d5hm4bw6w9naqhsm5k8sf68cvlgs086iqfkff00acw724")))

