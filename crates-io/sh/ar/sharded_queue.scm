(define-module (crates-io sh ar sharded_queue) #:use-module (crates-io))

(define-public crate-sharded_queue-1.0.0 (c (n "sharded_queue") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "147x3nim72ry96m5sbb2v26ksc7v4z0zdmfx506wiy1hgzgnl2k4")))

(define-public crate-sharded_queue-1.0.1 (c (n "sharded_queue") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "1yil8ia7a3d44i51rsva20c9dc2n6hk6q7jl4c2i2sm435l5af5s")))

(define-public crate-sharded_queue-1.0.2 (c (n "sharded_queue") (v "1.0.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "180bn7rwd35l1d05p58h5n52sylyz05vrfd1l15wiyjx9p8vc393")))

(define-public crate-sharded_queue-1.0.3 (c (n "sharded_queue") (v "1.0.3") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "03m1isjwxr7j42hxph9r2bf71vbmm3llnm68vjlfsj9mxvdgdz4q")))

(define-public crate-sharded_queue-1.0.4 (c (n "sharded_queue") (v "1.0.4") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "0apdzsb4jfxam8z84vqggxiai81g18xaz2iclvrkfgmxcgggf0p5")))

(define-public crate-sharded_queue-1.0.5 (c (n "sharded_queue") (v "1.0.5") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "1fs9m669dw5l3n2wpiif1f6v2cf4c0rwi6mgm2cdf8gpv9c0djyc")))

(define-public crate-sharded_queue-1.0.6 (c (n "sharded_queue") (v "1.0.6") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "1i9x69z2c26l35c296ypvb0z7h60qg1y30d97zl5413rf1v969ir")))

(define-public crate-sharded_queue-0.0.1 (c (n "sharded_queue") (v "0.0.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "0b9lv3h1y89393azza5kl314p86sxb45ckwnwq79bs1359qkqdia") (y #t)))

(define-public crate-sharded_queue-1.0.7 (c (n "sharded_queue") (v "1.0.7") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "153v77b70kxx77jk9yd27cg9wlp19c0llbmjzzydp850k3zpq86w")))

(define-public crate-sharded_queue-1.0.8 (c (n "sharded_queue") (v "1.0.8") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "0yfarh1h53knjymhmk1xi38x8pk4ydsb49d2dyrhx9w2d6h5k96i")))

(define-public crate-sharded_queue-1.0.9 (c (n "sharded_queue") (v "1.0.9") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "0ljfmbwxvb37syf541ypqachsqxgcgc0f2ha855lgvapa4i64liz")))

(define-public crate-sharded_queue-1.0.10 (c (n "sharded_queue") (v "1.0.10") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "16frqx8q2pmshm29pplknp34wsszgcgr0lwriywj7zpsrx1i6hii")))

(define-public crate-sharded_queue-1.0.11 (c (n "sharded_queue") (v "1.0.11") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "1fy3g45z5a7h3nsqavvl66g41v5bggh6c8m589z3q9f0qcal394j")))

(define-public crate-sharded_queue-1.0.12 (c (n "sharded_queue") (v "1.0.12") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "0nawhnya2xmwzrwr7w1iv8parzbm18awa09r5j1jippl3m0h3gbm")))

(define-public crate-sharded_queue-1.0.13 (c (n "sharded_queue") (v "1.0.13") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "04xvrbally1i2fwai2fwz3098wgy9yybd6lj92hahwc12l8vrrix")))

(define-public crate-sharded_queue-1.0.14 (c (n "sharded_queue") (v "1.0.14") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "1xb80hpf8x14hc4h0xh6kas3qhsgm6d99l58mqcjry0damak2c4a")))

(define-public crate-sharded_queue-1.0.15 (c (n "sharded_queue") (v "1.0.15") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "185mhjlxfxgz222jj5n1pdd9crsqmqyw014fak1lxmgz52cg45km")))

(define-public crate-sharded_queue-1.0.16 (c (n "sharded_queue") (v "1.0.16") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "1mj5rq3fvvcax0pxyg94740dj9xaqyiggxi0mdbf8z4gvq8mdzrb")))

(define-public crate-sharded_queue-1.0.17 (c (n "sharded_queue") (v "1.0.17") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "0jrr35508bdgfg6fhcawra6d5zvbr48kpihlbrnjb005a31irx2p")))

(define-public crate-sharded_queue-1.0.18 (c (n "sharded_queue") (v "1.0.18") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "0024gjl99xqqg10a5640lmir26gnc1f681ksmghlpvk4mwnsmhp6")))

(define-public crate-sharded_queue-1.0.19 (c (n "sharded_queue") (v "1.0.19") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "0078is9r1a501x7mjliiij82djr8wqkd3ifda7hgphz084rxfhlq")))

(define-public crate-sharded_queue-1.0.20 (c (n "sharded_queue") (v "1.0.20") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "07hyxf658c1cwf93jnbx9szn0wnrbaq2nfaajzjvq8bmh5lsaq9b")))

(define-public crate-sharded_queue-1.0.22 (c (n "sharded_queue") (v "1.0.22") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "0k6vckfgmssvqpq0jlwmgf3mnmf85g5vpzlb84c7zyzp20iy826f")))

(define-public crate-sharded_queue-1.0.23 (c (n "sharded_queue") (v "1.0.23") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "058bqpfvjdf2wapq7f5fap28a666klv4rfpqz2ypns46axajbryq")))

(define-public crate-sharded_queue-1.0.24 (c (n "sharded_queue") (v "1.0.24") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "1b0igqjgw86h0vc29k63s894sc9yanzvg50d8j9j02mhh50dsmva")))

(define-public crate-sharded_queue-1.0.25 (c (n "sharded_queue") (v "1.0.25") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "1ll3a4ywqx7zly1v3z9gbj9vh65dma9xmsdckrhdkcwdc34mjpyi")))

(define-public crate-sharded_queue-1.0.26 (c (n "sharded_queue") (v "1.0.26") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.16") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "1c5mn65ipn7yg0g7j333hw43jpan7b8znx47p1d020njj6cr1mvk")))

(define-public crate-sharded_queue-1.0.27 (c (n "sharded_queue") (v "1.0.27") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.16") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "1cg54mxn4bnyjqp98cq2v189s3d04v8jrkmw5dwm59ms24n9yx64")))

(define-public crate-sharded_queue-2.0.0 (c (n "sharded_queue") (v "2.0.0") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.16") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "1pzk1542xhja661v3bqhkjrm3kz28zzswqvcjz3f572wnazwr2bv")))

(define-public crate-sharded_queue-2.0.1 (c (n "sharded_queue") (v "2.0.1") (d (list (d (n "concurrent-queue") (r "^2.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.16") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 2)))) (h "1d7sw5686y9jr5xb8nhpi71krfh7hrlmf16ibbkn09fqigi9njd3")))

