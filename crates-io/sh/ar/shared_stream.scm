(define-module (crates-io sh ar shared_stream) #:use-module (crates-io))

(define-public crate-shared_stream-0.1.0 (c (n "shared_stream") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("executor"))) (k 2)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1gc9idd22vavb4hswvx1wa5lnyvfb7jxv2f5c3xwpwqsgpc1jwgh")))

(define-public crate-shared_stream-0.1.1 (c (n "shared_stream") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (f (quote ("executor"))) (k 2)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "01xak55fzslsklwcyi8ixgxxybzlvb4mh5jc0lymd1m6akm2jl1w")))

(define-public crate-shared_stream-0.2.0 (c (n "shared_stream") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("executor"))) (k 2)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1ngpzyppbcys1ybwfm0jzwa5z7akja8fqhgkcjh553mjqhxm0k0y")))

(define-public crate-shared_stream-0.2.1 (c (n "shared_stream") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (f (quote ("executor"))) (k 2)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0y9faqrdsw56fjdnkrp26bp78p40mwcchd316w4vb87sbq4nq75a")))

