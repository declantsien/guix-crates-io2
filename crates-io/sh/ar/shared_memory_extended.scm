(define-module (crates-io sh ar shared_memory_extended) #:use-module (crates-io))

(define-public crate-shared_memory_extended-0.13.0 (c (n "shared_memory_extended") (v "0.13.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("fs" "mman"))) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "raw_sync") (r "^0.1") (d #t) (k 2)) (d (n "win-sys") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0vq8xlxlllfhmgqdbgi588a4d8dh21vrcl0x8y2lzrivkb77wk80") (f (quote (("logging" "log") ("default"))))))

