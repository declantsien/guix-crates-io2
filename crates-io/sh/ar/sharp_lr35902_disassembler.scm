(define-module (crates-io sh ar sharp_lr35902_disassembler) #:use-module (crates-io))

(define-public crate-sharp_lr35902_disassembler-0.1.0 (c (n "sharp_lr35902_disassembler") (v "0.1.0") (h "06y0gnwqc1rq9bbigdwf2922iv7c53429p9dqxh9lvi8lgi918bj")))

(define-public crate-sharp_lr35902_disassembler-0.1.1 (c (n "sharp_lr35902_disassembler") (v "0.1.1") (h "0pxkdw3nizhz8lk16k8h8ygwbdnr4a87xyrd3ibvfxwi3c27aka6")))

