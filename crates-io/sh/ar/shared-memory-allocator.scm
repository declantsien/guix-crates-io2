(define-module (crates-io sh ar shared-memory-allocator) #:use-module (crates-io))

(define-public crate-shared-memory-allocator-0.1.0 (c (n "shared-memory-allocator") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.127") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0k5m7lcq7f5cvjfjd22cqqp0x6mkl5255h5zh2msw2iwxng4lplq")))

