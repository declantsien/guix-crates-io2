(define-module (crates-io sh ar shared-mutex) #:use-module (crates-io))

(define-public crate-shared-mutex-0.1.0 (c (n "shared-mutex") (v "0.1.0") (h "07gwwnjkcnv1cjws3s9pzrm3xdcfmwy1al6lq9i486g89jkvk3ri")))

(define-public crate-shared-mutex-0.2.0 (c (n "shared-mutex") (v "0.2.0") (d (list (d (n "scoped-pool") (r "^0.1") (d #t) (k 2)))) (h "0s4whjpp6d8xk5c2wpqj6z2kxs6xvfjn7v58bd897mpiwhcj7l4i")))

(define-public crate-shared-mutex-0.2.1 (c (n "shared-mutex") (v "0.2.1") (d (list (d (n "scoped-pool") (r "^0.1") (d #t) (k 2)))) (h "0w2ww0pnpnwwk4sfj95fjx1whsh2q18xnyf328azwrqi0hbb3bxl")))

(define-public crate-shared-mutex-0.2.2 (c (n "shared-mutex") (v "0.2.2") (d (list (d (n "scoped-pool") (r "^0.1") (d #t) (k 2)))) (h "122wb3xya9j6y311blx1bpp2qnr406z5n8fgl90wdhzrsaxx79yf")))

(define-public crate-shared-mutex-0.2.3 (c (n "shared-mutex") (v "0.2.3") (d (list (d (n "scoped-pool") (r "^0.1") (d #t) (k 2)))) (h "0rnvw5g4lr5jn83ins4j9jrbn3phcf7884hcrwrfmf9g68b0d9jm")))

(define-public crate-shared-mutex-0.3.0 (c (n "shared-mutex") (v "0.3.0") (d (list (d (n "poison") (r "^0.1") (d #t) (k 0)) (d (n "scoped-pool") (r "^0.1") (d #t) (k 2)))) (h "0ydb7di52dwgmgcbd7z74ppnk8whdp1sgikg8s9jazxabyyf0w23")))

(define-public crate-shared-mutex-0.3.1 (c (n "shared-mutex") (v "0.3.1") (d (list (d (n "poison") (r "^0.1") (d #t) (k 0)) (d (n "scoped-pool") (r "^0.1") (d #t) (k 2)))) (h "0mqmi6298ngv6wzqjmj14rbv8yibxzx2i0kxlp784xmy3wfz4cmn")))

