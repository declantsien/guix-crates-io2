(define-module (crates-io sh ar shard-ecs) #:use-module (crates-io))

(define-public crate-shard-ecs-0.1.0 (c (n "shard-ecs") (v "0.1.0") (d (list (d (n "const_fn_assert") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0ndsb1a5333ly1diylyc5f6gmg427j1r0jz0pnxxqmpyiicfxaam")))

(define-public crate-shard-ecs-0.1.1 (c (n "shard-ecs") (v "0.1.1") (d (list (d (n "const_fn_assert") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0id6hxrzib4p06jgdp0418nwr7g3q61yhqb4k8148g56blr4n6n2")))

(define-public crate-shard-ecs-0.1.2 (c (n "shard-ecs") (v "0.1.2") (d (list (d (n "const_fn_assert") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "16mxw8irb772a4dj2xn0b0l69wwpgg0gk19ja3ay9g7xzyj0m4ps") (f (quote (("const_type_name") ("const_fn_panic"))))))

(define-public crate-shard-ecs-0.2.0 (c (n "shard-ecs") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1wfripw9n0cfzf6yfgzckxk8ynfy21kxzmbq8m3q2c2sl5v9ym4p")))

(define-public crate-shard-ecs-0.2.1 (c (n "shard-ecs") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0hayiav51yf5qnk3vzml42y21y83fc0mrxmm632l7r43qgw33ii5")))

(define-public crate-shard-ecs-0.2.2 (c (n "shard-ecs") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1gj5fsr74cyjciyrr6wbb26vrps64kgb84rxwmqy3i0lzq59vafn")))

(define-public crate-shard-ecs-0.2.3 (c (n "shard-ecs") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0lxv0m7l5rls8rpvlhb8jgsif9a059ksgj95kii0bvm7041xmzcn")))

(define-public crate-shard-ecs-0.2.4 (c (n "shard-ecs") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0kqdpb9anw5aqwblwz8agykh5lvvg2mqi9c90j5qvxkbffnx9ncx")))

(define-public crate-shard-ecs-0.2.5 (c (n "shard-ecs") (v "0.2.5") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0drdink0328aidkn82zyrpjrrpgqd3mfsbvr40zdx0yfgyyi2cr6")))

(define-public crate-shard-ecs-0.2.6 (c (n "shard-ecs") (v "0.2.6") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "shard_ecs_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1arqgiicj17q4hr2zhcakrn0g40mkyfcypbjmzvf5f8wl1q59a0x") (f (quote (("derive" "shard_ecs_derive"))))))

(define-public crate-shard-ecs-0.2.7 (c (n "shard-ecs") (v "0.2.7") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "shard_ecs_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1yl75026a0jmkzdhgdngwslwv6fa19xcg0qmcx0ysyrqbp3g6nmn") (f (quote (("derive" "shard_ecs_derive"))))))

