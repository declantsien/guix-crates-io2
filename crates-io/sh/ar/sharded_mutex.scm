(define-module (crates-io sh ar sharded_mutex) #:use-module (crates-io))

(define-public crate-sharded_mutex-0.1.0 (c (n "sharded_mutex") (v "0.1.0") (d (list (d (n "parking_lot") (r ">=0.11, <=0.12") (d #t) (k 0)))) (h "1xdmj4kmryql1g7fq4xyznrxz3m3b8mpcmrwlm7pl8akq4cdz094")))

(define-public crate-sharded_mutex-0.2.0 (c (n "sharded_mutex") (v "0.2.0") (d (list (d (n "parking_lot") (r ">=0.11, <=0.12") (d #t) (k 0)))) (h "05dmh4ndjzbd8i9nis2wiz1k704k0nanhw8nx88184v026xa5h16")))

(define-public crate-sharded_mutex-0.3.0 (c (n "sharded_mutex") (v "0.3.0") (d (list (d (n "assoc_static") (r "^0.1.1") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.12") (d #t) (k 0)))) (h "1jg3i9bc9b2s75153981iy7a9b50djfqyg2hngrsc4qrrddbyzkw") (y #t)))

(define-public crate-sharded_mutex-0.3.1 (c (n "sharded_mutex") (v "0.3.1") (d (list (d (n "assoc_static") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.12") (d #t) (k 0)))) (h "0ghibda2zjkkk94vx7hp01ka8lximdp7ka2jdj30ib1c9kl62gfa")))

(define-public crate-sharded_mutex-0.4.0 (c (n "sharded_mutex") (v "0.4.0") (d (list (d (n "assoc_static") (r "^1.0.0-pre0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.12") (d #t) (k 0)))) (h "08s6036h5q0vwpfjkrxb978blvz60vv8hwfaqzvw28yqm00yhcaa")))

(define-public crate-sharded_mutex-0.5.0 (c (n "sharded_mutex") (v "0.5.0") (d (list (d (n "assoc_static") (r "^1.0.0-pre0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.12") (d #t) (k 0)))) (h "0l6031asb55nbby243qcxfm2kbm3hxdfpvy1pmjh7331ylr181zk")))

(define-public crate-sharded_mutex-0.5.1 (c (n "sharded_mutex") (v "0.5.1") (d (list (d (n "assoc_static") (r "^1.0.0-pre0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "0xm5jmmrgv014v8953ngv7qjvs8v4795c770dswwvg1f4k387wg7")))

(define-public crate-sharded_mutex-0.6.0 (c (n "sharded_mutex") (v "0.6.0") (d (list (d (n "assoc_static") (r "^1.0.0-pre0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "1dzz52cpwxyww17mhzvaddlsds6wcc3g9l0gpyx8q4b30ykdbdix")))

(define-public crate-sharded_mutex-0.7.0 (c (n "sharded_mutex") (v "0.7.0") (d (list (d (n "assoc_static") (r "^1.0.0-pre0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "0g4grxyrlkk3ycfwd17fx1d9niajvgrsvg2ykwcmlqlbdvyadv1f")))

(define-public crate-sharded_mutex-0.8.0 (c (n "sharded_mutex") (v "0.8.0") (d (list (d (n "assoc_static") (r "^1.0.0-pre0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "0xzpk218b0n2ddch5gf4xbh0ics8zdw108qxiy6bhk9zlm84hrs5")))

(define-public crate-sharded_mutex-0.8.1 (c (n "sharded_mutex") (v "0.8.1") (d (list (d (n "assoc_static") (r "^1.0.0-pre0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "1g82v9r7y9wby2qxnj4i7yjixh5h2f05cgqg8n896bpmxx3ahnf7")))

(define-public crate-sharded_mutex-0.9.0 (c (n "sharded_mutex") (v "0.9.0") (d (list (d (n "assoc_static") (r "^1.0.0-pre0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "187il88rxwb7l3vail6wlrgk518ic68kalacjqm2q46ysys74h3d")))

(define-public crate-sharded_mutex-0.9.1 (c (n "sharded_mutex") (v "0.9.1") (d (list (d (n "assoc_static") (r "^1.0.0-pre0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "1jzmz2siq5vvad376by2slvrzrv739si48q9ng6cp4zc4s7c5n7h")))

(define-public crate-sharded_mutex-0.10.0 (c (n "sharded_mutex") (v "0.10.0") (d (list (d (n "assoc_static") (r "^1.0.0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "0fjgw1fg2fgzwj37pa62gp87hk6b8jix35hv3cbk25y36dyx94n9")))

(define-public crate-sharded_mutex-0.11.0 (c (n "sharded_mutex") (v "0.11.0") (d (list (d (n "assoc_static") (r "^1.0.0") (d #t) (k 0)) (d (n "assoc_threadlocal") (r "^0.0.1") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "00mryjvvzj510q499d940hr717r6v3c4x14w91baylyz05kdz6c9")))

(define-public crate-sharded_mutex-1.0.0 (c (n "sharded_mutex") (v "1.0.0") (d (list (d (n "assoc_static") (r "^1.0.0") (d #t) (k 0)) (d (n "assoc_threadlocal") (r "^0.0.1") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "05fy50lqn7akklh68630wm42fc8zamxhjxzk36cgq47vnz4ba8q2")))

(define-public crate-sharded_mutex-1.1.0 (c (n "sharded_mutex") (v "1.1.0") (d (list (d (n "assoc_static") (r "^1.0.0") (d #t) (k 0)) (d (n "assoc_threadlocal") (r "^0.0.1") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11, <=0.13") (d #t) (k 0)))) (h "0w5k3pn57z1zp8ansy7jbcfi7mc0xklq01xn2d5f1p9gxai46qxi") (f (quote (("default" "align_narrow") ("align_wide") ("align_none") ("align_narrow") ("align_cacheline"))))))

(define-public crate-sharded_mutex-1.2.0 (c (n "sharded_mutex") (v "1.2.0") (d (list (d (n "assoc_static") (r "^1.0.0") (d #t) (k 0)) (d (n "assoc_threadlocal") (r "^0.0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "04y48ljzawclvj33kgyw575ip28mjw0vn1ma7qh7z8rpwbkmfmw4") (f (quote (("default" "align_narrow") ("align_wide") ("align_none") ("align_narrow") ("align_cacheline")))) (r "1.56.1")))

(define-public crate-sharded_mutex-1.2.1 (c (n "sharded_mutex") (v "1.2.1") (d (list (d (n "assoc_static") (r "^1.0.0") (d #t) (k 0)) (d (n "assoc_threadlocal") (r "^0.0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0c5v0xwvnip18gvrpjgclb9qwna9jdqq7qbx50l6y2ys94cp0vcj") (f (quote (("default" "align_narrow") ("align_wide") ("align_none") ("align_narrow") ("align_cacheline")))) (r "1.56.1")))

(define-public crate-sharded_mutex-1.2.2 (c (n "sharded_mutex") (v "1.2.2") (d (list (d (n "assoc_static") (r "^1.0.0") (d #t) (k 0)) (d (n "assoc_threadlocal") (r "^0.0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0pgp3hj6d8dhhnxdd8mczdwlvbl37fs3lm1p3hgin7kgpxdpmdyz") (f (quote (("default" "align_narrow") ("align_wide") ("align_none") ("align_narrow") ("align_cacheline")))) (r "1.56.1")))

