(define-module (crates-io sh ar shared-rc) #:use-module (crates-io))

(define-public crate-shared-rc-0.1.0 (c (n "shared-rc") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "unsize") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0bn4z1lsxyzf9drrgp4q3gdqa00ypf4fgkwjzipfq391xrllwp12") (f (quote (("std")))) (y #t) (s 2) (e (quote (("unsize" "dep:unsize") ("static_assertions" "dep:static_assertions")))) (r "1.56.0")))

(define-public crate-shared-rc-0.1.1 (c (n "shared-rc") (v "0.1.1") (d (list (d (n "unsize") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0jv9033qx6hkzy2ypad18za25z22hyynq56c27wn8jw5j19qgfl6") (f (quote (("std") ("static_assertions")))) (y #t) (s 2) (e (quote (("unsize" "dep:unsize")))) (r "1.56.0")))

