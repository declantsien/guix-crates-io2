(define-module (crates-io sh ar shared) #:use-module (crates-io))

(define-public crate-shared-0.1.0 (c (n "shared") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)))) (h "0sz07vr0p546hj97dzm8bv4sr137d8blmv3csdaqdzg06qwb456r")))

(define-public crate-shared-0.1.1 (c (n "shared") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)))) (h "1qzg4krqjxyslih7xyqwcvzc72x4qfl4g2px0y9ll961k75bxs9c")))

(define-public crate-shared-0.1.2 (c (n "shared") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)))) (h "09fl9z1k5bs569v8kvpxsclwa7gw8zccjnl6asgfjfidfzk19cq9")))

