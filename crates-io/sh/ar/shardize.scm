(define-module (crates-io sh ar shardize) #:use-module (crates-io))

(define-public crate-shardize-0.1.0 (c (n "shardize") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shardize-core") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "180j5ivf3adcf2ks14drr0dvnnkh46xayl6l4bd6xndy9qr5545g")))

