(define-module (crates-io sh ar shared_arena) #:use-module (crates-io))

(define-public crate-shared_arena-0.1.0 (c (n "shared_arena") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "19kyinx3dhfd3dxfplbpk7pvr1nbg8897skw102an8c0328br4xi")))

(define-public crate-shared_arena-0.8.0 (c (n "shared_arena") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "1k190scj565k4l2010difv2813h13nwjah0y8579xrny5hmcaxnz")))

(define-public crate-shared_arena-0.8.1 (c (n "shared_arena") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "1fld67zqsxp4km32h4nmpx69qwcf4hpq3l4clr9c95n8hrnfxm8m")))

(define-public crate-shared_arena-0.8.2 (c (n "shared_arena") (v "0.8.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0ljc0jqwfmx1yrg4zqnwl2wcgbcw2k4377lgarn2mzpqq00xml7s")))

(define-public crate-shared_arena-0.8.3 (c (n "shared_arena") (v "0.8.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0aj543b2gkxj4yzykv3lgw2nvgwhvkaz9mv0xxc67j1799wai4dk")))

(define-public crate-shared_arena-0.8.4 (c (n "shared_arena") (v "0.8.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0x22h2c3f75fzpjj383096jgihmmid53334q2gmi638gzvqdhfs5")))

