(define-module (crates-io sh ar shared_jwt) #:use-module (crates-io))

(define-public crate-shared_jwt-0.1.0 (c (n "shared_jwt") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "00r1ws3c3srp9qsb9p379299663scwqmkv9q6w04ivd4vcs12v19") (y #t)))

(define-public crate-shared_jwt-0.1.1 (c (n "shared_jwt") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0m82zrn7aa14n221mr9wnysswjz8ddjsxpmq1yv6hc8avggwx7qz") (y #t)))

