(define-module (crates-io sh ar shared_child) #:use-module (crates-io))

(define-public crate-shared_child-0.1.0 (c (n "shared_child") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "1qa4zkv9qpnq85y8qmmyhqcd72qzcckzq3lwzs4kv5nhlpsmj0ng")))

(define-public crate-shared_child-0.2.0 (c (n "shared_child") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "085k6abhilh7051gggljxxj68zwvx87f6rsb34x47wki7jap0b33")))

(define-public crate-shared_child-0.2.1 (c (n "shared_child") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "1k0ldvk9p1l0b38bwd3a29ylmwbj60c37hnqzh0hljmyin93i6q9")))

(define-public crate-shared_child-0.3.0 (c (n "shared_child") (v "0.3.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "0121rv4awc3lqx71d7yl8dn4fxr59bl2nm348iw63gcdcc9iir5r")))

(define-public crate-shared_child-0.3.1 (c (n "shared_child") (v "0.3.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "1dc33wz0747k87370fhl634cjg01yysx1y308fcpxyp3d4lgxq6p")))

(define-public crate-shared_child-0.3.2 (c (n "shared_child") (v "0.3.2") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "0kkv2b1s6jqdbaqhlhcivs7xj2xz7cpiqc9m5aygjnj7nf1y9mdw")))

(define-public crate-shared_child-0.3.3 (c (n "shared_child") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("synchapi" "winbase" "winerror" "winnt"))) (d #t) (k 0)))) (h "0j2grl1qb0pwb75zm08agpsllrli1h4gqya0dfw1qfjvcmwls9by")))

(define-public crate-shared_child-0.3.4 (c (n "shared_child") (v "0.3.4") (d (list (d (n "libc") (r "^0.2.42") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("synchapi" "winbase" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lmjmr7931dr9cpalw2n7ss4i9mnl7285j2dygxflk9y80xczswc")))

(define-public crate-shared_child-0.3.5 (c (n "shared_child") (v "0.3.5") (d (list (d (n "libc") (r "^0.2.42") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("synchapi" "winbase" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0np6g5fmixncky9c0j5xhqxrp1rkxqnmx5vj7spq652vavazgsbb")))

(define-public crate-shared_child-1.0.0 (c (n "shared_child") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.42") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("synchapi" "winbase" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1vyaw3ka1kyyfbb57vfdi8zn6794sl1mprrsybz3f89wmmcldndh")))

