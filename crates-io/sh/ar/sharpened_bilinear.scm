(define-module (crates-io sh ar sharpened_bilinear) #:use-module (crates-io))

(define-public crate-sharpened_bilinear-1.0.0 (c (n "sharpened_bilinear") (v "1.0.0") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "lexopt") (r "^0.3") (d #t) (k 0)))) (h "1qb2hxqnhpb4wa9gzbpppdv874azgr9lwjkqlm2lbvrgd02iz63f")))

