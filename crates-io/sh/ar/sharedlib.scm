(define-module (crates-io sh ar sharedlib) #:use-module (crates-io))

(define-public crate-sharedlib-0.2.0 (c (n "sharedlib") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "03fxyvlz8j7x84gia026zb4j9r305mhyzmz8bjxzzbrwsrx60y29")))

(define-public crate-sharedlib-1.0.0 (c (n "sharedlib") (v "1.0.0") (d (list (d (n "define_error") (r "^1.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1zdg61bk26xfwznshr6rlxzcp4nx7lb92jc5f386181kkqsqnxzv")))

(define-public crate-sharedlib-3.0.0 (c (n "sharedlib") (v "3.0.0") (d (list (d (n "define_error") (r "^4") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1kn0ynz7f8rawxy2w5wz4q1n80fq9bwskkxm351jm31c1kpzhqkn")))

(define-public crate-sharedlib-3.1.0 (c (n "sharedlib") (v "3.1.0") (d (list (d (n "define_error") (r "^4") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1n2sn36cnbv6f9c62ajz0xmdg4nrb9r3pz11z7rv6fz6g2asnn6d")))

(define-public crate-sharedlib-4.0.0 (c (n "sharedlib") (v "4.0.0") (d (list (d (n "error-chain") (r "^0.4.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "16nnjh1v486rpjqk5b4dk4r175crk3ahmd32adyn5vjm72sk27w1")))

(define-public crate-sharedlib-5.0.0 (c (n "sharedlib") (v "5.0.0") (d (list (d (n "error-chain") (r "^0.4.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1kgg9y4y1qx8dykk67bd6jb89dyscbdl1d9g94pjblfjl0871vv6")))

(define-public crate-sharedlib-6.0.0 (c (n "sharedlib") (v "6.0.0") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1iwjl0jr7rjlba09bf5bi0xzn3g4z3lsjg6jd6y1kwqgkasb90ji")))

(define-public crate-sharedlib-7.0.0 (c (n "sharedlib") (v "7.0.0") (d (list (d (n "error-chain") (r "^0.9") (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0b6z6rap4i92h40jnaq102si8lvyvlgb7ls88jqd45ij2anvf6lf")))

