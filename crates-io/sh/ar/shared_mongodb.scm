(define-module (crates-io sh ar shared_mongodb) #:use-module (crates-io))

(define-public crate-shared_mongodb-0.1.0 (c (n "shared_mongodb") (v "0.1.0") (d (list (d (n "mongodb") (r "^2.1.0") (d #t) (k 0)))) (h "05dagl2rl0phnfqngysajwb72c000rmwhrjvvp9j44i7v7xjqdxw")))

(define-public crate-shared_mongodb-0.1.1 (c (n "shared_mongodb") (v "0.1.1") (d (list (d (n "actix-web") (r "^3") (d #t) (k 2)) (d (n "mongodb") (r "^2.1.0") (d #t) (k 0)))) (h "12kfjlgiksgpr3lxvlrd9zxyx1maas5xfcm2scdrsbc0wlvb3q19")))

(define-public crate-shared_mongodb-0.1.2 (c (n "shared_mongodb") (v "0.1.2") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "actix-web") (r "^3") (d #t) (k 2)) (d (n "mongodb") (r "^2.1.0") (d #t) (k 0)))) (h "1r7rfnb4r5j4mkdvj3sg85zng97zmiy7332bj2a9rz4da8pn1ars")))

(define-public crate-shared_mongodb-0.1.3 (c (n "shared_mongodb") (v "0.1.3") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "actix-web") (r "^3") (d #t) (k 2)) (d (n "mongodb") (r "^2.2.1") (f (quote ("async-std-runtime"))) (k 0)))) (h "1wckdykdrzrz2zgl0ljkjh566f2bn3cwpjh3pf5xp62qdrv8szfr")))

(define-public crate-shared_mongodb-0.1.4 (c (n "shared_mongodb") (v "0.1.4") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "actix-web") (r "^3") (d #t) (k 2)) (d (n "mongodb") (r "^2.2.1") (f (quote ("async-std-runtime"))) (k 0)))) (h "1x9d3m68dmhlapvkm8c14rzfh071am0gnhlfs0qjz4hbcimp7hrl")))

(define-public crate-shared_mongodb-0.1.5 (c (n "shared_mongodb") (v "0.1.5") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "actix-web") (r "^3") (d #t) (k 2)) (d (n "mongodb") (r "^2.2.1") (f (quote ("async-std-runtime"))) (k 0)))) (h "1qgxz54jfg844fq75fmn8scknzxyzslnw697h5wv2gp096jxj9vd")))

(define-public crate-shared_mongodb-0.1.6 (c (n "shared_mongodb") (v "0.1.6") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "actix-web") (r "^3") (d #t) (k 2)) (d (n "mongodb") (r "^2.2.1") (f (quote ("async-std-runtime"))) (k 0)))) (h "1alfhls7fxzl7c8z1yrn318m20ylw6dydwi79w2s2vxmy83nvcjp")))

(define-public crate-shared_mongodb-0.1.7 (c (n "shared_mongodb") (v "0.1.7") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 2)) (d (n "actix-web") (r "^3") (d #t) (k 2)) (d (n "mongodb") (r "^2.2.1") (f (quote ("async-std-runtime"))) (k 0)) (d (n "tokio") (r "^1.29.1") (d #t) (k 0)))) (h "0kx72plccl0ylzinbzqw13b02n35j8s6hg0i3ggvkcmpjmfks8cd")))

