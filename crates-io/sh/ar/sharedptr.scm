(define-module (crates-io sh ar sharedptr) #:use-module (crates-io))

(define-public crate-sharedptr-0.1.0 (c (n "sharedptr") (v "0.1.0") (h "0r31s7ba35ycrdsi1jxa443yavanmnd5q81xfv2f45gy623i5b6x")))

(define-public crate-sharedptr-0.1.1 (c (n "sharedptr") (v "0.1.1") (h "1ll80rhjwc3fav62h5ssx5l1g2xgi1rrw6zivm321bdgnb20nbvf")))

(define-public crate-sharedptr-0.2.0 (c (n "sharedptr") (v "0.2.0") (h "0m8v1v0bfccqbvqas3p2n8bbwvrc88giccl1inv6bqy6hkipwlhh")))

(define-public crate-sharedptr-0.3.0 (c (n "sharedptr") (v "0.3.0") (h "1x8p5yn6wjaqgjd83kz7gzqpbg04dyw1ghqagxfrghxnrrd8zxdj") (y #t)))

(define-public crate-sharedptr-0.3.1 (c (n "sharedptr") (v "0.3.1") (h "193bxvhrf4yg45mf22qm8k2bsg2x5ki8255yghxkvx7w2f393lzx") (y #t)))

(define-public crate-sharedptr-0.3.2 (c (n "sharedptr") (v "0.3.2") (h "1c7i547byp50qxz2vqhrpnd82gdspdrlhy4rcilz63mwbdrn39im")))

(define-public crate-sharedptr-0.3.3 (c (n "sharedptr") (v "0.3.3") (h "1qjk71skcqrla379xdlk927qgplp7rgqz1b7f0lib4q7dnvr05l6") (y #t)))

(define-public crate-sharedptr-0.3.4 (c (n "sharedptr") (v "0.3.4") (h "1ad6zambbvzvy0gv1xsci5qabxhn1k1phpsxpyaq79z17hc6fdb9")))

