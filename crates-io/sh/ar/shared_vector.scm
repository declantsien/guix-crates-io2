(define-module (crates-io sh ar shared_vector) #:use-module (crates-io))

(define-public crate-shared_vector-0.1.0 (c (n "shared_vector") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1935bk5jb9g8qv0xsg0v9byvgi4pzhbh44r449in5hp6jfp22mdz") (y #t)))

(define-public crate-shared_vector-0.1.1 (c (n "shared_vector") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0c6g2ikms8ybppbv8zd87syzkbp42pxrjgxmpg8z9v9vibnfm5gz")))

(define-public crate-shared_vector-0.2.0 (c (n "shared_vector") (v "0.2.0") (d (list (d (n "allocator-api2") (r "^0.2.4") (d #t) (k 0)) (d (n "blink-alloc") (r "^0.2.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "12yv5hidvs8ax9aivh0x6iqmb79gywn94h30l14268573jy97das") (f (quote (("std" "allocator-api2/std") ("nightly" "allocator-api2/nightly" "blink-alloc/nightly") ("default" "std"))))))

(define-public crate-shared_vector-0.3.0 (c (n "shared_vector") (v "0.3.0") (d (list (d (n "allocator-api2") (r "^0.2.4") (d #t) (k 0)) (d (n "blink-alloc") (r "^0.2.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0aa72jlbw2h9ag4hjmdank84pbrmsxkgb3k32kjcnyjzh9mvgsm1") (f (quote (("std" "allocator-api2/std") ("nightly" "allocator-api2/nightly" "blink-alloc/nightly") ("default" "std"))))))

(define-public crate-shared_vector-0.3.1 (c (n "shared_vector") (v "0.3.1") (d (list (d (n "allocator-api2") (r "^0.2.4") (d #t) (k 0)) (d (n "blink-alloc") (r "^0.2.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1b6r2bsy2z7wg2hjvcwvxvspb673b219h098s0zxdjlkc0l467hl") (f (quote (("std" "allocator-api2/std") ("nightly" "allocator-api2/nightly" "blink-alloc/nightly") ("default" "std"))))))

(define-public crate-shared_vector-0.4.0 (c (n "shared_vector") (v "0.4.0") (d (list (d (n "allocator-api2") (r "^0.2.4") (d #t) (k 0)) (d (n "blink-alloc") (r "^0.2.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1grdjhq6x5z3c17wga8552yjfyac3jyam2xpbj6cwslhxh35ifam") (f (quote (("std" "allocator-api2/std") ("nightly" "allocator-api2/nightly" "blink-alloc/nightly") ("default" "std"))))))

(define-public crate-shared_vector-0.4.1 (c (n "shared_vector") (v "0.4.1") (d (list (d (n "allocator-api2") (r "^0.2.4") (d #t) (k 0)) (d (n "blink-alloc") (r "^0.2.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1snzy10jf07iidrr2ibla44fzrvwrqafq28xqrx4g65ibsjr86gi") (f (quote (("std" "allocator-api2/std") ("nightly" "allocator-api2/nightly" "blink-alloc/nightly") ("default" "std"))))))

(define-public crate-shared_vector-0.4.2 (c (n "shared_vector") (v "0.4.2") (d (list (d (n "allocator-api2") (r "^0.2.4") (d #t) (k 0)) (d (n "blink-alloc") (r "^0.2.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0a03gdyrc4l39q74dzifrmjn047w5zi1xhglgslfxcnhl02x5fzz") (f (quote (("std" "allocator-api2/std") ("nightly" "allocator-api2/nightly" "blink-alloc/nightly") ("default" "std"))))))

(define-public crate-shared_vector-0.4.3 (c (n "shared_vector") (v "0.4.3") (d (list (d (n "allocator-api2") (r "^0.2.4") (d #t) (k 0)) (d (n "blink-alloc") (r "^0.2.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0md668rill3l66x4g7zv9d7jww3vq9akizmqlz2m6khd4f0mdfd7") (f (quote (("std" "allocator-api2/std") ("nightly" "allocator-api2/nightly" "blink-alloc/nightly") ("default" "std"))))))

(define-public crate-shared_vector-0.4.4 (c (n "shared_vector") (v "0.4.4") (d (list (d (n "allocator-api2") (r "^0.2.4") (d #t) (k 0)) (d (n "blink-alloc") (r "^0.2.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1zcw72zikbm2drzfmccgvyv2dh7gha0727qhk7a9ki9b6i13zpzy") (f (quote (("std" "allocator-api2/std") ("nightly" "allocator-api2/nightly" "blink-alloc/nightly") ("default" "std"))))))

