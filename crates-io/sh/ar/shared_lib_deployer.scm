(define-module (crates-io sh ar shared_lib_deployer) #:use-module (crates-io))

(define-public crate-shared_lib_deployer-1.1.0 (c (n "shared_lib_deployer") (v "1.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "07kqzqfj4g8gm1p8kvri83wrh9yggfhj5q7xr1mwq3qhpw794jfr")))

(define-public crate-shared_lib_deployer-1.2.1 (c (n "shared_lib_deployer") (v "1.2.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0a9h7qvdgmbrw7f7b44plbkl7wiin2m6xsz5rncpvnjz3yshkha2")))

(define-public crate-shared_lib_deployer-1.2.2 (c (n "shared_lib_deployer") (v "1.2.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "02z6c1pza8firsgw7y8s64glwcxdl2zm7dcskr47wxn0lv23k178")))

(define-public crate-shared_lib_deployer-1.3.0 (c (n "shared_lib_deployer") (v "1.3.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1ca1darg1shvdcpj4fcap1rk9z33pp5i6phlyfrshm8w0fd6sf9y")))

(define-public crate-shared_lib_deployer-1.4.0 (c (n "shared_lib_deployer") (v "1.4.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "079byjmviw7xmrba7v8zqczq38dxzazq518l327dlf6l2y3plf1p")))

(define-public crate-shared_lib_deployer-1.5.0 (c (n "shared_lib_deployer") (v "1.5.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0n5j7rkj7x8j9gw9n15712zxzmg7ziva2v2c6p7cgdwzaasbn1zd")))

(define-public crate-shared_lib_deployer-1.5.1 (c (n "shared_lib_deployer") (v "1.5.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1xf4ywvgql2mc1hshisadbwxj7ii056lv0xam92dc3srfx3h7xsw")))

