(define-module (crates-io sh ar shareable) #:use-module (crates-io))

(define-public crate-shareable-0.0.1 (c (n "shareable") (v "0.0.1") (h "026b35i45ngkbnhmys7bwk6c30330wkira0awh0kqm999nxzidrk")))

(define-public crate-shareable-0.0.5 (c (n "shareable") (v "0.0.5") (h "1n4rmyl68q96w633nzbn455p83q2xssvd41g0v7zvyzzzvx57ksm")))

(define-public crate-shareable-0.1.0 (c (n "shareable") (v "0.1.0") (h "0m2490ii0kh3lmx9r1iinxhipsapwd4ijws3b0scc000vikf7nlm")))

(define-public crate-shareable-0.1.1 (c (n "shareable") (v "0.1.1") (h "1iwkscaw5d5yjqzh8h0rldwrwpwhlvw4kmyqdc9dfsil1ack08sh")))

