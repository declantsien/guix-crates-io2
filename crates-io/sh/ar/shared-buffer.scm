(define-module (crates-io sh ar shared-buffer) #:use-module (crates-io))

(define-public crate-shared-buffer-0.1.0 (c (n "shared-buffer") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.6.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "122dgqdpgm6gpr83q22x7f16hc0g1m29c4hbjhj79vr02a6mm48w") (r "1.64.0")))

(define-public crate-shared-buffer-0.1.1 (c (n "shared-buffer") (v "0.1.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.6.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "00q2xn5vnpdqb9pah694bwr6ifiqqir75j6ls4kbayj13b8vrrwy") (r "1.64.0")))

(define-public crate-shared-buffer-0.1.2 (c (n "shared-buffer") (v "0.1.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.6.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "13v6yi059xi34b28dsg8h8c2srd9l1shkh43lr4bs762hiwri94n") (r "1.64.0")))

(define-public crate-shared-buffer-0.1.3 (c (n "shared-buffer") (v "0.1.3") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.6.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "15frnhjq221micviq1za53viqaa5f8wfdcqns0yziqk1xq11dxic") (r "1.64.0")))

(define-public crate-shared-buffer-0.1.4 (c (n "shared-buffer") (v "0.1.4") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.6.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "05jbp0v6yq3s25ykf9i0r7wfbh8pxmskj794mbkmfafmp8srijgn") (r "1.64.0")))

