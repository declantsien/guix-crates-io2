(define-module (crates-io sh ar sharks) #:use-module (crates-io))

(define-public crate-sharks-0.1.0 (c (n "sharks") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1r0hwak5i7fziw5m4aplwib6igmh2xlzjfphbh2rqx70k4w7qm2m")))

(define-public crate-sharks-0.1.1 (c (n "sharks") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0h2gr8adfqblmw6acdk369ia1spr0ykfhwikbfvzh5fb6r3zhnjh")))

(define-public crate-sharks-0.2.0 (c (n "sharks") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1sdnlpvkyb9pp6m8l6nzf7y332jqwv3xxbr0h70d2bff2k96hmm3")))

(define-public crate-sharks-0.3.0 (c (n "sharks") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0v03n4qwrbjqpjhrslyglhccvss7kihs3p4gzfaps4bsidz80wa2")))

(define-public crate-sharks-0.3.1 (c (n "sharks") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1wqzsygvfkg4nlrcqgx9wxddhslxlapdq46kz00wrvhirz97w5rw")))

(define-public crate-sharks-0.3.2 (c (n "sharks") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "13x4583j6gvwxxkl3yrcf6bsl38fq8gsx52cq0vib6lg454rh8ny")))

(define-public crate-sharks-0.3.3 (c (n "sharks") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1czc3nndbxjjfy36913wnwfz3bx7ziy1w642z22pnq3qq9xhjyj4")))

(define-public crate-sharks-0.4.0 (c (n "sharks") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (k 0)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "08lphhhwsmakmfxm79p5kvqplcdigcb50z04plz2rnxv5yh8qd91") (f (quote (("std" "rand/std") ("default" "std"))))))

(define-public crate-sharks-0.4.1 (c (n "sharks") (v "0.4.1") (d (list (d (n "arbitrary") (r "^0.4.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (k 0)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "1charll841wj5bqg47gih67l1mr560h6vqgxilxrbi7d3iqsh4sb") (f (quote (("std" "rand/std") ("fuzzing" "std" "arbitrary") ("default" "std"))))))

(define-public crate-sharks-0.4.2 (c (n "sharks") (v "0.4.2") (d (list (d (n "arbitrary") (r "^0.4.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (k 0)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "0321fj8b29m1c6n6zfcmpspmh55qb2l7qrxk5yk7rjgkx6nqccw8") (f (quote (("std" "rand/std") ("fuzzing" "std" "arbitrary") ("default" "std"))))))

(define-public crate-sharks-0.4.3 (c (n "sharks") (v "0.4.3") (d (list (d (n "arbitrary") (r "^0.4.7") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0zs9m0xr5agg5yv98rkahi46xprm16d9vh69w22hjl1mlqfdkbnw") (f (quote (("std" "rand/std" "rand/std_rng") ("fuzzing" "std" "arbitrary") ("default" "std"))))))

(define-public crate-sharks-0.5.0 (c (n "sharks") (v "0.5.0") (d (list (d (n "arbitrary") (r "^0.4.7") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "zeroize") (r "^1.2.0") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "0lw770kbh3zhgxaaqq63sfan2blmzf1rzm5sn6gl4blabyaiwawh") (f (quote (("zeroize_memory" "zeroize") ("std" "rand/std" "rand/std_rng") ("fuzzing" "std" "arbitrary") ("default" "std" "zeroize_memory"))))))

