(define-module (crates-io sh ar shared_slice) #:use-module (crates-io))

(define-public crate-shared_slice-0.0.1 (c (n "shared_slice") (v "0.0.1") (h "0mncwf0jzg1mqfwvarrk6ph54g8zv49j1i9h80p2njlkqgd7x7j3")))

(define-public crate-shared_slice-0.0.2 (c (n "shared_slice") (v "0.0.2") (h "1yc6cfmzq8ccz4qk5jgcciwwfslfvvgs6cba3iz1yqlkd68is45m")))

(define-public crate-shared_slice-0.0.3 (c (n "shared_slice") (v "0.0.3") (h "016r6xgn38wqhijcnqhwm91m9p3laksynq5br7sli035q8lixzky")))

(define-public crate-shared_slice-0.0.4 (c (n "shared_slice") (v "0.0.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1p79rc9kfy140l0gk8gbi6wiq0r5k4wdlxp2959f02amf95zxnh7") (f (quote (("unstable"))))))

