(define-module (crates-io sh ar sharp_lr35902_cpu) #:use-module (crates-io))

(define-public crate-sharp_LR35902_cpu-0.1.0 (c (n "sharp_LR35902_cpu") (v "0.1.0") (h "08s1nrvgwll935bnjp1s70hgadkyvsxav8sm1wqsf1a23h8kjd58")))

(define-public crate-sharp_LR35902_cpu-0.1.2 (c (n "sharp_LR35902_cpu") (v "0.1.2") (h "1d66bw11airi7mzk7hyydpwmi4rmk3n0acm9zz76r0zpgi0sc65f")))

