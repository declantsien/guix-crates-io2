(define-module (crates-io sh ar shared_lru) #:use-module (crates-io))

(define-public crate-shared_lru-0.1.0 (c (n "shared_lru") (v "0.1.0") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0ghav7n8fn4lazqdayd1na9mrk64hm5cdr2y6ki4yg24fr7ff9nz")))

(define-public crate-shared_lru-0.1.1 (c (n "shared_lru") (v "0.1.1") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)))) (h "0f1wbfpnmcydyl4z62nb9rv2bm0kjaxvwfxj45d31554y1045fhv") (f (quote (("default"))))))

(define-public crate-shared_lru-0.1.2 (c (n "shared_lru") (v "0.1.2") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)))) (h "1msildx5h626gq3xcxl840h7zzmf364wv9bqcyjzpzqxjsb88dvd") (f (quote (("default"))))))

(define-public crate-shared_lru-0.1.3 (c (n "shared_lru") (v "0.1.3") (d (list (d (n "dashmap") (r "^4.0.2") (d #t) (k 0)) (d (n "lru") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)))) (h "05c5g69yqh2di7pwh391vf3kyb0rqs4hbyscadqxcb8ja0dddncb") (f (quote (("default"))))))

(define-public crate-shared_lru-0.1.4 (c (n "shared_lru") (v "0.1.4") (d (list (d (n "dashmap") (r "^4.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "lru") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)))) (h "1kb3s7z536b7gvz5rgy3sg4acnj8jznnc4l1fxg7ki9iljlfhw2d") (f (quote (("default"))))))

(define-public crate-shared_lru-0.1.5 (c (n "shared_lru") (v "0.1.5") (d (list (d (n "dashmap") (r "^4.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "lru") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (o #t) (d #t) (k 0)))) (h "16z1j3j7hh5fgy7xj72rr9dhb2dhzjzly8w0xf78iyricb435i02") (f (quote (("default"))))))

