(define-module (crates-io sh ar shared-ids) #:use-module (crates-io))

(define-public crate-shared-ids-0.10.0 (c (n "shared-ids") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mfq3fk10zyanhh0qfxvamzhmh5bdxvmi2z66r42w6fm0vk9yii7")))

(define-public crate-shared-ids-0.11.0 (c (n "shared-ids") (v "0.11.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16dzff91cqfsf2bqjwzk1q9py1v080k9lgqia1sp829kn9m871k7")))

