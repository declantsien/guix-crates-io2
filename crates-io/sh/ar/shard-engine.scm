(define-module (crates-io sh ar shard-engine) #:use-module (crates-io))

(define-public crate-shard-engine-0.0.0 (c (n "shard-engine") (v "0.0.0") (h "0zhz9m1395wa6a8glai5hwqi4rnxgflp1wjyaqkhxly88jnir7k4") (y #t)))

(define-public crate-shard-engine-0.0.1-discontinued (c (n "shard-engine") (v "0.0.1-discontinued") (h "14d73lcw4r7aixbd3gsv2ibfqkwbvqfa0ka4y3ddcn823k9b4djs")))

