(define-module (crates-io sh po shpool_pty) #:use-module (crates-io))

(define-public crate-shpool_pty-0.1.0 (c (n "shpool_pty") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r ">=0.2.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0waw8rcgs55yb3s0fd2gsq9958lldxavwyr2jch3jaybwkhia5wy") (f (quote (("unstable") ("travis" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default") ("debug"))))))

(define-public crate-shpool_pty-0.2.0 (c (n "shpool_pty") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r ">=0.2.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fmwsiqbn6bfgw9rqprcwppq51ia63pjsv8g4axg3lspbd5y24kw") (f (quote (("unstable") ("travis" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default") ("debug"))))))

(define-public crate-shpool_pty-0.2.1 (c (n "shpool_pty") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r ">=0.2.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0i9il5ism3zj8mkapgss4msilas73aisnid9r79hk2x7vw1c9bmz") (f (quote (("unstable") ("travis" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default") ("debug"))))))

(define-public crate-shpool_pty-0.3.0 (c (n "shpool_pty") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r ">=0.2.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0h302d5f8xlbvkblliz414kfyyzkjlj6dp1q3hh82bffix7rm163") (f (quote (("unstable") ("travis" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default") ("debug"))))))

(define-public crate-shpool_pty-0.3.1 (c (n "shpool_pty") (v "0.3.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r ">=0.2.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1dcsjv2kcnly2q749qza23xmirpnzavqwbgs5gwx2i2084syryq1") (f (quote (("unstable") ("travis" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default") ("debug"))))))

