(define-module (crates-io sh ep shepherd) #:use-module (crates-io))

(define-public crate-shepherd-0.1.0 (c (n "shepherd") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (f (quote ("termination"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.4") (d #t) (k 0)))) (h "05bba36127jv3f8wxpp6ypnrlzx9jr165c3kjxyz4zivmjy3mn1w")))

(define-public crate-shepherd-0.1.1 (c (n "shepherd") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (f (quote ("termination"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.4") (d #t) (k 0)))) (h "1ib6icgxamhccyqz2d8ha2fcfdhg68gf025hrkl96vy6mps5l68r")))

(define-public crate-shepherd-0.1.2 (c (n "shepherd") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (f (quote ("termination"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.4") (d #t) (k 0)))) (h "1ynwj9salds91rc0ascdp4g3ilz16s4zmz4i3sk98fvrnfig3a2x") (y #t)))

(define-public crate-shepherd-0.1.3 (c (n "shepherd") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (f (quote ("termination"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.4") (d #t) (k 0)))) (h "0vq6mpyiqjr372q57mpj2qi494y88a2x6xmzz3cg8wq8inycsbyd")))

(define-public crate-shepherd-0.1.4 (c (n "shepherd") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (f (quote ("termination"))) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.4") (d #t) (k 0)))) (h "0m1mzj0y3ns56yib9piimn7gb582my6nia19syqnnn6llcc8hxfv")))

(define-public crate-shepherd-0.1.5 (c (n "shepherd") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.4") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (f (quote ("termination"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "0hfri69n5w400znyzrfvjj1xrc8gq4jiccafvcsni7g2f7x1yl2g")))

