(define-module (crates-io sh ep shepplogan) #:use-module (crates-io))

(define-public crate-shepplogan-1.0.0 (c (n "shepplogan") (v "1.0.0") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ij4r8p71ivxmp73kk7m47mwii7d0b4x7pnskxf1fa28sr6zzsqp") (f (quote (("slow_impl") ("parallel" "rayon") ("default"))))))

(define-public crate-shepplogan-1.0.1 (c (n "shepplogan") (v "1.0.1") (d (list (d (n "image") (r "^0.20.1") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1mgyyb3py9p076dxgzixr34b6g2fik9yx5bwvqz1fnzdqjzmmqpq") (f (quote (("slow_impl") ("parallel" "rayon") ("default"))))))

