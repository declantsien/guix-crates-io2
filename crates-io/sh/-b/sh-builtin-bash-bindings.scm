(define-module (crates-io sh -b sh-builtin-bash-bindings) #:use-module (crates-io))

(define-public crate-sh-builtin-bash-bindings-0.1.0 (c (n "sh-builtin-bash-bindings") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0w76iwrinm0pab9zi41nx2p6i2420kmhdjl5847cb94gngfhn3vk")))

