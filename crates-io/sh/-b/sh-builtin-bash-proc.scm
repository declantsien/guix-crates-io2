(define-module (crates-io sh -b sh-builtin-bash-proc) #:use-module (crates-io))

(define-public crate-sh-builtin-bash-proc-0.1.0 (c (n "sh-builtin-bash-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sh-builtin-common-util") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0j1gjwfb35b2z7kxd93bw5dgc4iygxrajvxccdlh34hjpshikwd3")))

