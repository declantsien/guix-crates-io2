(define-module (crates-io sh -b sh-builtin-common-util) #:use-module (crates-io))

(define-public crate-sh-builtin-common-util-0.1.0 (c (n "sh-builtin-common-util") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0c6l561hiiyq1g3zqfc5k7j9ch75lczxnir848fxz7jn11wgv6gl")))

