(define-module (crates-io sh -b sh-builtin-bash) #:use-module (crates-io))

(define-public crate-sh-builtin-bash-0.1.0 (c (n "sh-builtin-bash") (v "0.1.0") (d (list (d (n "sh-builtin-bash-bindings") (r "^0.1.0") (d #t) (k 0)) (d (n "sh-builtin-bash-proc") (r "^0.1.0") (d #t) (k 0)))) (h "0p3kh9vcxzqchmph864mrdf1nfh0impavfg46pwgxmhxsc9fy0kc")))

