(define-module (crates-io sh av shave) #:use-module (crates-io))

(define-public crate-shave-0.0.0 (c (n "shave") (v "0.0.0") (h "1a36agayskmkkih5b4wslhk314sww3g7kh7l0zfsb7gzj6k1jqqq")))

(define-public crate-shave-0.1.0 (c (n "shave") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (f (quote ("std"))) (k 0)) (d (n "fantoccini") (r "^0.20.0-rc.7") (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("tcp"))) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1.0.114") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.2") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("process" "time"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "test-util"))) (k 2)))) (h "02vasid9xm46a8qjdm0pj3wcm4mw87jgzgrgzb3saw4mpfp44981") (r "1.63")))

(define-public crate-shave-0.2.0 (c (n "shave") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (f (quote ("std"))) (k 0)) (d (n "fantoccini") (r "^0.20.0-rc.7") (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("tcp"))) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1.0.114") (f (quote ("std"))) (k 0)) (d (n "tempfile") (r "^3.2") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("process" "time"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "test-util"))) (k 2)))) (h "0n0jaqjwksmxjlv4n666j34mnyfh8s8j3yaskd054j3z9am453iz") (r "1.63")))

