(define-module (crates-io sh av shavee_pam) #:use-module (crates-io))

(define-public crate-shavee_pam-0.7.3 (c (n "shavee_pam") (v "0.7.3") (d (list (d (n "pamsm") (r "^0.5.3") (f (quote ("libpam"))) (d #t) (k 0)) (d (n "shavee_core") (r "^0.7.3") (d #t) (k 0) (p "shavee_core")))) (h "09m5p5jk3n620gxgkq9iw8m24znqwfl79hac8mmpndpj2pxjh39n") (f (quote (("yubikey") ("file") ("default" "file" "yubikey"))))))

(define-public crate-shavee_pam-0.7.5 (c (n "shavee_pam") (v "0.7.5") (d (list (d (n "pamsm") (r "^0.5.3") (f (quote ("libpam"))) (d #t) (k 0)) (d (n "shavee_core") (r "^0.7.5") (d #t) (k 0) (p "shavee_core")))) (h "03cw8jq0kafkjcnbjabj4yxb6skf8wf6ga5wly81n3fbabykqryv") (f (quote (("yubikey") ("file") ("default" "file" "yubikey"))))))

(define-public crate-shavee_pam-1.0.0 (c (n "shavee_pam") (v "1.0.0") (d (list (d (n "pamsm") (r "^0.5.3") (f (quote ("libpam"))) (d #t) (k 0)) (d (n "shavee_core") (r "^1.0.0") (d #t) (k 0) (p "shavee_core")))) (h "1ahzd2gs21icp4300gfhg1x1zpfm7gxdsr0bdm3hip9p6cfjifwz") (f (quote (("yubikey") ("file") ("default" "file" "yubikey"))))))

(define-public crate-shavee_pam-1.0.1 (c (n "shavee_pam") (v "1.0.1") (d (list (d (n "pamsm") (r "^0.5") (f (quote ("libpam"))) (d #t) (k 0)) (d (n "shavee_core") (r "^1.0.1") (d #t) (k 0) (p "shavee_core")))) (h "0xzmgn0fqvhcms40mmsn47vlb7ll1qibnmcx2zi3hcc8m15zam1m") (f (quote (("yubikey") ("file") ("default" "file" "yubikey"))))))

