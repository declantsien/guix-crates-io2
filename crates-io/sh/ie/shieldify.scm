(define-module (crates-io sh ie shieldify) #:use-module (crates-io))

(define-public crate-shieldify-0.1.0 (c (n "shieldify") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.11") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "03raihmzl35x3pka0a0dbbn36bw2p3iy52z321nrzw52cqymg6yy")))

(define-public crate-shieldify-0.1.1 (c (n "shieldify") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.11") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0y2ksgnmy1g1ahhgvp8pwcg0npfhhz20bvlsmn70xisj3l8b1gc8")))

(define-public crate-shieldify-0.1.2 (c (n "shieldify") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.11") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "129k819r1ry5y5m9qqgrvj5ysrrmm1xsyzvgh1zidfar4p3dzb7w")))

(define-public crate-shieldify-0.1.3 (c (n "shieldify") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.11") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1s09r05cyqlbc9by6mnd7kk3clymz3id2l8ifv83ipkm4p1wjgql")))

