(define-module (crates-io sh ie shield-sim7000) #:use-module (crates-io))

(define-public crate-shield-sim7000-1.0.0-alpha.1 (c (n "shield-sim7000") (v "1.0.0-alpha.1") (d (list (d (n "at-commands") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.2") (d #t) (k 0)))) (h "00221szhf8bm0ljg78s0kmzi1rvn40n8na66mzjl4nw2smr1l9wq")))

(define-public crate-shield-sim7000-1.0.0-alpha.2 (c (n "shield-sim7000") (v "1.0.0-alpha.2") (d (list (d (n "at-commands") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.2") (d #t) (k 0)))) (h "1bzdvk7akm76bn9r680j3blh44yb9zypmsgrwb3rwbd8fkpcv024")))

