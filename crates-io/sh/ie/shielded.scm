(define-module (crates-io sh ie shielded) #:use-module (crates-io))

(define-public crate-shielded-0.1.0 (c (n "shielded") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "ring") (r "^0.14") (d #t) (k 0)))) (h "0yc5qljilivs617qmzq580plzv9kmmnazl2ax71hcpj4z0avb6bp")))

(define-public crate-shielded-0.1.1 (c (n "shielded") (v "0.1.1") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "ring") (r "^0.16") (d #t) (k 0)))) (h "1qny8srmn9scjpjzad0f7ilgjdn66npwar0cnk4kxkb43hcr9lpv")))

(define-public crate-shielded-0.1.2 (c (n "shielded") (v "0.1.2") (d (list (d (n "quickcheck") (r "^1") (k 2)) (d (n "ring") (r "^0.17") (d #t) (k 0)))) (h "12ciiyfaxkl92vpp076da72h6537w9ws7m53j4j8by62wsw5lpbf")))

