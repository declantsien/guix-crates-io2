(define-module (crates-io sh ie shield-maker) #:use-module (crates-io))

(define-public crate-shield-maker-0.1.0 (c (n "shield-maker") (v "0.1.0") (d (list (d (n "ab_glyph") (r "^0.2.18") (d #t) (k 0)) (d (n "css-color-parser") (r "^0.1.2") (d #t) (k 0)))) (h "034kvfsqdgd531inam2ypi4vdis4q75pps6xjg5kcnsqb2szadci")))

