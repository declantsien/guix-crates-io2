(define-module (crates-io sh i_ shi_history) #:use-module (crates-io))

(define-public crate-shi_history-0.3.2 (c (n "shi_history") (v "0.3.2") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlite") (r "^0.30.4") (d #t) (k 0)) (d (n "tabled") (r "^0.12.0") (d #t) (k 0)))) (h "078pz8i9mnhi637ip3k83g7r9rxs5hhqm90nyqja7qyjxs1ky5xr")))

(define-public crate-shi_history-0.4.0 (c (n "shi_history") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlite") (r "^0.30.4") (d #t) (k 0)) (d (n "tabled") (r "^0.12.0") (d #t) (k 0)))) (h "1k3fcmq90fi8ry0nypc1cf4h4dy048ascnsdrsvli88d580w9kfk")))

(define-public crate-shi_history-0.5.0 (c (n "shi_history") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlite") (r "^0.30.4") (d #t) (k 0)))) (h "0h3a9gwnvnxdflhk528s7vcqp6syynlq54vsa54ycdz91n59459k")))

(define-public crate-shi_history-0.6.0 (c (n "shi_history") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlite") (r "^0.34.0") (d #t) (k 0)))) (h "1p8zm4b4wq3wq2zf187r0nykndkir08d5awfx8cz3rz319bd3dzx")))

