(define-module (crates-io sh e- she-elgamal) #:use-module (crates-io))

(define-public crate-she-elgamal-0.0.10 (c (n "she-elgamal") (v "0.0.10") (d (list (d (n "ec-pairing") (r "^0.0.10") (k 0)) (d (n "jub-jub") (r "^0.0.10") (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (k 2)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (k 0)) (d (n "zkstd") (r "^0.0.10") (k 0)))) (h "0qxhw6z8v0l4y6rjih7hhbi62yg19k25axjm6j9mngvbnjgk0xd1")))

(define-public crate-she-elgamal-0.0.11 (c (n "she-elgamal") (v "0.0.11") (d (list (d (n "ec-pairing") (r "^0.0.11") (k 0)) (d (n "jub-jub") (r "^0.0.14") (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (k 0)) (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6.4") (k 2)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (k 0)) (d (n "zkstd") (r "^0.0.12") (k 0)))) (h "0cxyrp85gydrq3sngffjcp3ibk4bm2293if3i5igwvs3g4qb3769")))

(define-public crate-she-elgamal-0.0.12 (c (n "she-elgamal") (v "0.0.12") (d (list (d (n "jub-jub") (r "^0.0.20") (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (k 2)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (k 0)) (d (n "zkstd") (r "^0.0.22") (k 0)))) (h "1sf8y6drb7651nfwjcbgj2fsldwpi212f6r7isr1kk6sqhiq09by")))

