(define-module (crates-io sh og shogi_official_kifu) #:use-module (crates-io))

(define-public crate-shogi_official_kifu-0.1.0 (c (n "shogi_official_kifu") (v "0.1.0") (d (list (d (n "shogi_core") (r "^0.1") (f (quote ("alloc"))) (k 0)) (d (n "shogi_legality_lite") (r "^0.1") (f (quote ("alloc"))) (k 0)) (d (n "shogi_usi_parser") (r "=0.1.0") (d #t) (k 2)))) (h "0f7nqimsaa9lwpl5hz15djhczljc0k11mzrv3nn290wcfffp967l") (f (quote (("std" "shogi_core/std" "shogi_legality_lite/std") ("kansuji") ("default" "kansuji" "std")))) (r "1.60")))

(define-public crate-shogi_official_kifu-0.1.1 (c (n "shogi_official_kifu") (v "0.1.1") (d (list (d (n "shogi_core") (r "^0.1") (f (quote ("alloc"))) (k 0)) (d (n "shogi_legality_lite") (r "^0.1.1") (f (quote ("alloc"))) (k 0)) (d (n "shogi_usi_parser") (r "=0.1.0") (d #t) (k 2)))) (h "1h6hhwmj73qdscqrcp2chy54wxphbny1wg9gigpcj9yc510s06hr") (f (quote (("std" "shogi_core/std" "shogi_legality_lite/std") ("kansuji") ("default" "kansuji" "std")))) (r "1.60")))

(define-public crate-shogi_official_kifu-0.1.2 (c (n "shogi_official_kifu") (v "0.1.2") (d (list (d (n "shogi_core") (r "^0.1") (f (quote ("alloc"))) (k 0)) (d (n "shogi_legality_lite") (r "^0.1.2") (f (quote ("alloc"))) (k 0)) (d (n "shogi_usi_parser") (r "=0.1.0") (d #t) (k 2)))) (h "1qhglcyvpjkfk6il1fvls1czj13nvpr2njh0z6rzkbqjyw9hjx8n") (f (quote (("std" "shogi_core/std" "shogi_legality_lite/std") ("kansuji") ("default" "kansuji" "std")))) (r "1.60")))

