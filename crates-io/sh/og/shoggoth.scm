(define-module (crates-io sh og shoggoth) #:use-module (crates-io))

(define-public crate-shoggoth-0.0.1 (c (n "shoggoth") (v "0.0.1") (h "044mc9b78s0lnadhqz86dcwsz44g4wc5kh8a3yai4rnbrdbhsvfa")))

(define-public crate-shoggoth-0.0.2 (c (n "shoggoth") (v "0.0.2") (h "10i0rsi6ag7in28ygcvmwnlgcrnr1hwmnlcq6zzbirlnbkm3is8a")))

(define-public crate-shoggoth-0.0.3 (c (n "shoggoth") (v "0.0.3") (h "04f0aparg789hgph6pxnkm3vk62zdiknjdxynp58d6sjj54484n5")))

(define-public crate-shoggoth-0.0.4 (c (n "shoggoth") (v "0.0.4") (h "05llivc37075dclr94s04npcdmfpzwci4h55kq070fpvm7v1bxf1")))

(define-public crate-shoggoth-0.0.5 (c (n "shoggoth") (v "0.0.5") (h "07mry5dsamxfz4mqs2id9qpjlb9l0lp66jannwz4b2nzlrdikqrn")))

(define-public crate-shoggoth-0.0.6 (c (n "shoggoth") (v "0.0.6") (d (list (d (n "unify") (r "*") (d #t) (k 0)))) (h "0machhdz91dwz9chj9k78f0gjhjq40p6gh2l553x6hlx006wyswl")))

(define-public crate-shoggoth-0.0.7 (c (n "shoggoth") (v "0.0.7") (d (list (d (n "unify") (r "*") (d #t) (k 0)))) (h "15fqzzxzakh75n5g3kx93d4m0fj4r237jalbfw90ys5yqy0plm7k")))

(define-public crate-shoggoth-0.0.8 (c (n "shoggoth") (v "0.0.8") (d (list (d (n "unify") (r "*") (d #t) (k 0)))) (h "1gxvak60r8zmdwladg4lxfchlv30wv4x6wjcy2k48br4imbh07bh")))

(define-public crate-shoggoth-0.0.9 (c (n "shoggoth") (v "0.0.9") (d (list (d (n "unify") (r "*") (d #t) (k 0)))) (h "0lc9vkfzbmqq1yq8nivz5nklb7i3kgh2n365cwplg0qnc7x5ki96")))

(define-public crate-shoggoth-0.0.10 (c (n "shoggoth") (v "0.0.10") (h "05hxi2a2aja64jmp4yjvl71wqpyqcc1i86kjgpk4xj4p6n0zz8s7")))

(define-public crate-shoggoth-0.0.11 (c (n "shoggoth") (v "0.0.11") (h "06akvrb4bklwp9wm9s2qfcblynw2h2xdw8ziishdzp4fxy0cfs1b")))

(define-public crate-shoggoth-0.0.12 (c (n "shoggoth") (v "0.0.12") (h "0iiy83p0ypx2vzhhngjbyrdq2m63wjp2k4qis8bfsvng545p4xx1")))

(define-public crate-shoggoth-0.0.13 (c (n "shoggoth") (v "0.0.13") (h "00nbla5aih074qs4dgzf3zc4ggydcfkd4zfjvzsb89y2fnmmsh2g")))

(define-public crate-shoggoth-0.0.14 (c (n "shoggoth") (v "0.0.14") (h "00kkhqbb4aqd6wficflknn2kfbyklzhcgy3vsfvrhn313yr70783")))

(define-public crate-shoggoth-0.0.15 (c (n "shoggoth") (v "0.0.15") (h "101dwqxqwb6p7wpf163wcffrhsqrh8askavjlg4ihdpxqry6x74n")))

(define-public crate-shoggoth-0.0.16 (c (n "shoggoth") (v "0.0.16") (h "1ky82xhnbv4knkwc8n74hn6d9hw97g9csskcb6n6n4pzwh5a7cf4")))

(define-public crate-shoggoth-0.0.17 (c (n "shoggoth") (v "0.0.17") (d (list (d (n "shoggoth_macros") (r "*") (d #t) (k 0)))) (h "0fgip2n299kidspclryndia2nkqm5154bszzl41gqfn7b1qci71m")))

(define-public crate-shoggoth-0.0.18 (c (n "shoggoth") (v "0.0.18") (d (list (d (n "shoggoth_macros") (r "*") (d #t) (k 0)))) (h "1wca4gk07wz8xay0vf4dzcy3bf1rf7cpd2pbi4xj97c54ki6nyh5")))

(define-public crate-shoggoth-0.0.19 (c (n "shoggoth") (v "0.0.19") (d (list (d (n "shoggoth_macros") (r "*") (d #t) (k 0)))) (h "18sl73khana7l7dbffcjjkaglqdpdxsq5njyijq67pncrmk1z1i4")))

