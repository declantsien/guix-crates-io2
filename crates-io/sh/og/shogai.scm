(define-module (crates-io sh og shogai) #:use-module (crates-io))

(define-public crate-shogai-1.0.0 (c (n "shogai") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1crwsajqlljmakpy5jkyxg9i3hvh82dhnljwgx5sz8rmfra56vxn")))

(define-public crate-shogai-1.0.1 (c (n "shogai") (v "1.0.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1z533jfhfkzf5p1kg9xffp9b4a2awbyqq4jq1lc1qbnln6likcjb")))

(define-public crate-shogai-2.0.0 (c (n "shogai") (v "2.0.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1lyx8v3pwwmabhqv7b5amgx7w3m3g7zlacy32n4lzhy63p5hw7d2")))

