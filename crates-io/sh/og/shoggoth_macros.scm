(define-module (crates-io sh og shoggoth_macros) #:use-module (crates-io))

(define-public crate-shoggoth_macros-0.0.17 (c (n "shoggoth_macros") (v "0.0.17") (h "1hcsrjf7bylsbdz5cri42b8wyzm9kpcwbzzqgqwy6rjyhwrx7i06")))

(define-public crate-shoggoth_macros-0.0.18 (c (n "shoggoth_macros") (v "0.0.18") (h "1h649d9xy7crbsf1sclkvq688n6ncsxph3r0l82hm1v907ph3n28")))

(define-public crate-shoggoth_macros-0.0.19 (c (n "shoggoth_macros") (v "0.0.19") (h "1psb33cb1i371f7iinh99q6348xiqylvr2hbahy0905v6m01lgz4")))

