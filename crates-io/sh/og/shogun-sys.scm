(define-module (crates-io sh og shogun-sys) #:use-module (crates-io))

(define-public crate-shogun-sys-0.1.0 (c (n "shogun-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0ckh9gd1y0h6mpipcw81z3qy27acx96qngscjhrs5wkds6afa8yq")))

