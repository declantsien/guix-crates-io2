(define-module (crates-io sh og shogiutil) #:use-module (crates-io))

(define-public crate-shogiutil-0.1.0 (c (n "shogiutil") (v "0.1.0") (h "0n4gaw4bd0a2340gzl03sn5g76c0nbd6lqwdb3rmf5lbs9xrjpjh")))

(define-public crate-shogiutil-0.2.0 (c (n "shogiutil") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0bswkzc8f2azka781yhy3dl6yj72yymnmzqlwddbb60ncxz0lf6b")))

(define-public crate-shogiutil-0.3.0 (c (n "shogiutil") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "01ns59nkn60y4bhzfw8l5a41rhcv4fq7mhjr1fwm62m0yxd41dfp")))

(define-public crate-shogiutil-0.3.1 (c (n "shogiutil") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1cnv4ywaaz13wv1ld74zajazgxml6lk4v3ywzdr9wbi31rq7qv1p")))

(define-public crate-shogiutil-0.4.0 (c (n "shogiutil") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "011x6rhc2988li4098v1ndzkvyavgn46q39s404dg2k5hafadm1y")))

(define-public crate-shogiutil-0.4.1 (c (n "shogiutil") (v "0.4.1") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0kxr91jvinckj3y99nrqbxrmivg1d74ixsgg3dlclcmf6kkw5g4k")))

(define-public crate-shogiutil-0.5.0 (c (n "shogiutil") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "19lcifp2g825a2xd5db456i2v460i3yryfdw74hcpn07g1272gm0")))

(define-public crate-shogiutil-0.5.1 (c (n "shogiutil") (v "0.5.1") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "186d0gw3np5zkqn1apw5qs2ask4zpvkzfmaal9sgb769xm9r1nwm")))

(define-public crate-shogiutil-0.5.2 (c (n "shogiutil") (v "0.5.2") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "15ddgk29qxaxvky47q6s9dwpyw5a9fhkxbpiygz11cnffpqnsp0a")))

(define-public crate-shogiutil-0.5.3 (c (n "shogiutil") (v "0.5.3") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "04552fspqyi1gqypjy9kc7ghrvb3gyisffl9fkk44fpm1jrswrnh")))

(define-public crate-shogiutil-0.5.4 (c (n "shogiutil") (v "0.5.4") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "04wvg65awcphgxvbhxlzi8nckg53afbvdxbpsjzs854rl50xglbx")))

(define-public crate-shogiutil-0.6.0 (c (n "shogiutil") (v "0.6.0") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1rzhnsbc4qq0g1qmmvxxji5g1sb3wazld9rzjapfm8qf8bwzdxzl")))

(define-public crate-shogiutil-0.7.0 (c (n "shogiutil") (v "0.7.0") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0n10g0dv0vm5x08ak7y4ygqc3knmw0xbbr3x1fl0127zsdihb8dc")))

