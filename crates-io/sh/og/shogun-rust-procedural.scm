(define-module (crates-io sh og shogun-rust-procedural) #:use-module (crates-io))

(define-public crate-shogun-rust-procedural-0.1.0 (c (n "shogun-rust-procedural") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dhv340xg0dfcsy57n068zs7yljzqwpa9l5n06v6lqp5mrslkgky")))

(define-public crate-shogun-rust-procedural-0.1.1 (c (n "shogun-rust-procedural") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shogun-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15ndfydqpakahid1m4r1p3yjmxmk40p672qnw2i02w6qq8gqg54m")))

