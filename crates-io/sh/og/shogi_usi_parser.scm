(define-module (crates-io sh og shogi_usi_parser) #:use-module (crates-io))

(define-public crate-shogi_usi_parser-0.1.0 (c (n "shogi_usi_parser") (v "0.1.0") (d (list (d (n "shogi_core") (r "^0.1.1") (k 0)))) (h "0qlcxy6a8xjq734r1gvypj2s7ayr9bhkgci3jmfbrw2w0nrdm6pk") (f (quote (("std" "alloc" "shogi_core/std") ("default" "std") ("alloc" "shogi_core/alloc")))) (r "1.60")))

