(define-module (crates-io sh og shogi-img) #:use-module (crates-io))

(define-public crate-shogi-img-0.1.0 (c (n "shogi-img") (v "0.1.0") (d (list (d (n "image") (r "^0.24.8") (f (quote ("png"))) (k 0)) (d (n "imageproc") (r "^0.23.0") (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "shogi_core") (r "^0.1.5") (d #t) (k 0)))) (h "0xbmq9if4cgcbr008d3d894m9p2pwgcjf00d3hn4ijz9x7s9lz1g")))

(define-public crate-shogi-img-0.2.0 (c (n "shogi-img") (v "0.2.0") (d (list (d (n "image") (r "^0.24.8") (f (quote ("png"))) (k 0)) (d (n "imageproc") (r "^0.23.0") (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "shogi_core") (r "^0.1.5") (d #t) (k 0)))) (h "0llv5rmp5ddclvq5rqsqc716indihfg1i2n8s02ryfb5l3fj10kh")))

