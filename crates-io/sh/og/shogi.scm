(define-module (crates-io sh og shogi) #:use-module (crates-io))

(define-public crate-shogi-0.1.0 (c (n "shogi") (v "0.1.0") (h "1qbsh77mki9mqh10cqywmzrkzhp8jkxrig0z9a50mjq1l2dwz08s")))

(define-public crate-shogi-0.2.0 (c (n "shogi") (v "0.2.0") (d (list (d (n "itertools") (r "^0.5.9") (d #t) (k 0)))) (h "0icsiw2399rcdsrdjhgymf280sgdh54djcfq3lqh17r0lafdhq1y")))

(define-public crate-shogi-0.3.0 (c (n "shogi") (v "0.3.0") (d (list (d (n "itertools") (r "^0.5.9") (d #t) (k 0)))) (h "1s3fjzqjnw2pqfmkfvk107wf79fjhkjnbrxr72ry38daps8gscym")))

(define-public crate-shogi-0.4.0 (c (n "shogi") (v "0.4.0") (d (list (d (n "itertools") (r "^0.5.9") (d #t) (k 0)))) (h "1a33ykajvnymy3p11cisijs4pshy9zxhg72m6l62knrxh5vmh2by")))

(define-public crate-shogi-0.5.0 (c (n "shogi") (v "0.5.0") (d (list (d (n "itertools") (r "^0.5.9") (d #t) (k 0)))) (h "10hfkhmsnsw3cx8x4v640g1kqzkjz2dzg4f6b1s85j54j47xhzrz")))

(define-public crate-shogi-0.6.0 (c (n "shogi") (v "0.6.0") (d (list (d (n "bitintr") (r "^0.1.17") (d #t) (k 0)) (d (n "itertools") (r "^0.5.9") (d #t) (k 0)))) (h "14j5388hygx60hi3p62gl40352k49rnw5jz73bqbsfyclrdrkdjh")))

(define-public crate-shogi-0.7.0 (c (n "shogi") (v "0.7.0") (d (list (d (n "bitintr") (r "^0.1.18") (d #t) (k 0)) (d (n "itertools") (r "^0.6.0") (d #t) (k 0)))) (h "1dq7a0nkzxhnf4kycm520s3y638g0yqhzrmcyi9i73bfmpd1wl3z")))

(define-public crate-shogi-0.8.0 (c (n "shogi") (v "0.8.0") (d (list (d (n "bitintr") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "0jk34kfsw9vm1h90rnq6yr5v0c7pdlasfyks19hf3sk9sa7pwsyi")))

(define-public crate-shogi-0.9.0 (c (n "shogi") (v "0.9.0") (d (list (d (n "bitintr") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)))) (h "165fjchpw4yka9jipm7hadlal170prpymd6cx68cmbk455wam7yp")))

(define-public crate-shogi-0.10.0 (c (n "shogi") (v "0.10.0") (d (list (d (n "bitintr") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)))) (h "1mxn590mlhp4gi4c22wq4j0f4sh3qh6zmas5qvbh25zdcqp8r1fg")))

(define-public crate-shogi-0.10.1 (c (n "shogi") (v "0.10.1") (d (list (d (n "bitintr") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)))) (h "16jm9p046jxgf6b3nj5mbmhac6zxy4ksc5589yacb78ssm1jjjlm")))

(define-public crate-shogi-0.10.2 (c (n "shogi") (v "0.10.2") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "0xjgvhvvpn4f3jsnnwjpa24dasc689argi378h2k23fzh5iijfja")))

(define-public crate-shogi-0.10.3 (c (n "shogi") (v "0.10.3") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "18jklksy8wiynca2kgk9fmn1xg7d0a10asj9a1sh1ffgxgbxw53s")))

(define-public crate-shogi-0.11.0 (c (n "shogi") (v "0.11.0") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i8cwh9k0nm96ps34zaydvc71acwiv349z775j1zn1cdfa01707w")))

(define-public crate-shogi-0.12.0 (c (n "shogi") (v "0.12.0") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1635jnncbfxrn3iyx9gmzsxa3kqm6jfw8lx4p77pxv8m85gn8jks")))

(define-public crate-shogi-0.12.1 (c (n "shogi") (v "0.12.1") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zjr2zs036ldwwbc6na3w3x6y9a1ck328a4i9yif6gfnb8634swk")))

(define-public crate-shogi-0.12.2 (c (n "shogi") (v "0.12.2") (d (list (d (n "bitintr") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ixcb7dfkq51w6bsgyz1nvi3x1mj238vq78kidk1b2i49xw91qn9")))

