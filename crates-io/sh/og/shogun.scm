(define-module (crates-io sh og shogun) #:use-module (crates-io))

(define-public crate-shogun-0.1.0 (c (n "shogun") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "shogun-rust-procedural") (r "^0.1.0") (d #t) (k 0)))) (h "08xhzsw5gc10d912m2z04x2y85s7ymp1kv4634f1lrgwy77p257p")))

(define-public crate-shogun-0.1.1 (c (n "shogun") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "shogun-rust-procedural") (r "^0.1.1") (d #t) (k 0)) (d (n "shogun-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1phsk2ys61iaxxslqv54mnl7rhn4252avp37qscrc9c51knwcl3i")))

