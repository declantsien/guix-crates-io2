(define-module (crates-io sh og shogi_legality_lite) #:use-module (crates-io))

(define-public crate-shogi_legality_lite-0.1.0 (c (n "shogi_legality_lite") (v "0.1.0") (d (list (d (n "shogi_core") (r "^0.1.3") (k 0)))) (h "0i6dj1770843y91syxp1dn7n0ib2frd1ynjz2cywcg3dqrafimmm") (f (quote (("std" "alloc" "shogi_core/std") ("default" "std") ("alloc" "shogi_core/alloc")))) (r "1.60")))

(define-public crate-shogi_legality_lite-0.1.1 (c (n "shogi_legality_lite") (v "0.1.1") (d (list (d (n "shogi_core") (r "^0.1.3") (k 0)) (d (n "shogi_usi_parser") (r "=0.1.0") (k 2)))) (h "05jgz7vavi3ampnbljsgy0krs5r4i2bwdlypzbwrl9lja1zkn6sa") (f (quote (("std" "alloc" "shogi_core/std") ("default" "std") ("alloc" "shogi_core/alloc")))) (r "1.60")))

(define-public crate-shogi_legality_lite-0.1.2 (c (n "shogi_legality_lite") (v "0.1.2") (d (list (d (n "shogi_core") (r "^0.1.3") (k 0)) (d (n "shogi_usi_parser") (r "=0.1.0") (k 2)))) (h "0zh079iql9dyrghy1cycl77a54yb97gs51v35rrvfysp0qlf2vbh") (f (quote (("std" "alloc" "shogi_core/std") ("default" "std") ("alloc" "shogi_core/alloc")))) (r "1.60")))

