(define-module (crates-io sh og shogi_core) #:use-module (crates-io))

(define-public crate-shogi_core-0.1.0 (c (n "shogi_core") (v "0.1.0") (h "1n71q8bvxjyfrabk8gy2fslkm7fdh9584smr5wg2kk69bjwrnhxp") (f (quote (("std" "alloc") ("ord") ("hash") ("default" "std") ("alloc")))) (r "1.60")))

(define-public crate-shogi_core-0.1.1 (c (n "shogi_core") (v "0.1.1") (h "0d3syivqpm3fg98sm6vq2nm02cs4kf7giilg6x91q9vjyny9s77v") (f (quote (("std" "alloc") ("ord") ("hash") ("default" "std") ("alloc")))) (r "1.60")))

(define-public crate-shogi_core-0.1.2 (c (n "shogi_core") (v "0.1.2") (h "0fkh66gaqw3as141fibqjgzvp0wyvva3rcnfc1r32qbjmsfs2pgi") (f (quote (("std" "alloc") ("ord") ("hash") ("experimental") ("default" "std") ("alloc")))) (r "1.60")))

(define-public crate-shogi_core-0.1.3 (c (n "shogi_core") (v "0.1.3") (h "01khjvda2k5q05r8szv16dln59wdq9svy3bldnnw6y2fiplw073z") (f (quote (("std" "alloc") ("ord") ("hash") ("experimental") ("default" "std") ("alloc")))) (r "1.60")))

(define-public crate-shogi_core-0.1.4 (c (n "shogi_core") (v "0.1.4") (h "0bfgxys4j7y53cq858i3hb3z7ngas7imd4fdqjx5iyvxcq3rrjgh") (f (quote (("std" "alloc") ("ord") ("hash") ("experimental") ("default" "std") ("alloc")))) (r "1.60")))

(define-public crate-shogi_core-0.1.5 (c (n "shogi_core") (v "0.1.5") (h "055aj60wb1iqj8w7jcd3i9pljjn963as3ga6cc5vyrkwzzj7s2ix") (f (quote (("std" "alloc") ("ord") ("hash") ("experimental") ("default" "std") ("alloc")))) (r "1.60")))

