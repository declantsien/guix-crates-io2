(define-module (crates-io sh or shortest-brainfuck) #:use-module (crates-io))

(define-public crate-shortest-brainfuck-0.1.0 (c (n "shortest-brainfuck") (v "0.1.0") (d (list (d (n "brainfrick") (r "^1.1.1") (d #t) (k 0)))) (h "04aqazw2hvqhck50a35b107rircp7izlgzsdh7fwq0f3074ac1r5")))

(define-public crate-shortest-brainfuck-0.2.0 (c (n "shortest-brainfuck") (v "0.2.0") (h "1riv00im8bgl9spxzz1wyqi6bdvpnfv7janv03ma62bq1fv86v9h")))

