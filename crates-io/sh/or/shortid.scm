(define-module (crates-io sh or shortid) #:use-module (crates-io))

(define-public crate-shortid-1.0.0 (c (n "shortid") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (f (quote ("v1"))) (d #t) (k 2)))) (h "0zhfmrbd0lbsgv4ychf6jgz7vrv47a8ial15z6mfmixxa4kgl603")))

(define-public crate-shortid-1.0.1 (c (n "shortid") (v "1.0.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (f (quote ("v1"))) (d #t) (k 2)))) (h "071zh03jrcaywkac0z3ipyhd6m70453ygsp4jw8j7s273c8bix04")))

(define-public crate-shortid-1.0.2 (c (n "shortid") (v "1.0.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (f (quote ("v1"))) (d #t) (k 2)))) (h "0q2gkycrmr5r9gfbbwpbs4xfarzmnshwsavz5y12215c72yqadbp")))

(define-public crate-shortid-1.0.3 (c (n "shortid") (v "1.0.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (f (quote ("v1"))) (d #t) (k 2)))) (h "0cdgq46qy2jzdby2kmnbscvvycir8yqn5yvgqf9v0akx88rfx3dq")))

(define-public crate-shortid-1.0.4 (c (n "shortid") (v "1.0.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (f (quote ("v1"))) (d #t) (k 2)))) (h "0s0crkf56ia65rcpghajb25q2zlw85myaf06y0v0n5978rylpcln")))

(define-public crate-shortid-1.0.5 (c (n "shortid") (v "1.0.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (f (quote ("v1"))) (d #t) (k 2)))) (h "1wc97adkgkcapxan36i958jnsnxllqwlrzlp9vp0w4y584vx79b0")))

(define-public crate-shortid-1.0.6 (c (n "shortid") (v "1.0.6") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v1"))) (d #t) (k 2)))) (h "1vd66y7l73d3y2a29cfzj0zimm7vbjxn74ibl2k2cgsdsmgvkw8s")))

