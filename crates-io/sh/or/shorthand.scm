(define-module (crates-io sh or shorthand) #:use-module (crates-io))

(define-public crate-shorthand-0.1.0 (c (n "shorthand") (v "0.1.0") (d (list (d (n "from_map") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "075ascw8fyilja2jfb73h025sqg2g924xhh8plbc9xyqamfif6jk")))

(define-public crate-shorthand-0.1.1 (c (n "shorthand") (v "0.1.1") (d (list (d (n "from_map") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0cc9y5y2jb7k86qdxm61q0288xiy2ypjwcvhy482c8fqhpwpfks7")))

