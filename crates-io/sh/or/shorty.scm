(define-module (crates-io sh or shorty) #:use-module (crates-io))

(define-public crate-shorty-0.1.0 (c (n "shorty") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nanoid") (r "^0.2") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1dxj930v4jwyx9k2xkcnqbinhp61l0c6hdzwr1yq1hhf0xszv5c2")))

(define-public crate-shorty-0.2.1 (c (n "shorty") (v "0.2.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nanoid") (r "^0.2") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0v7wfdmqmh9h54cspfd4b3w57nm8453qf41qggyh2cc52cd9vxzn")))

(define-public crate-shorty-0.3.0 (c (n "shorty") (v "0.3.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nanoid") (r "^0.2") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0vgx4z7ps1qk1qyn1knbw1y71r785a1dhwgr1dkxm0kf7hmb6cvz")))

(define-public crate-shorty-0.4.0 (c (n "shorty") (v "0.4.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nanoid") (r "^0.2") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0f9k87wp0nbnlnch7h7d7ih94hmmr0cz224lkzwa2nw2d3y5rpkl")))

(define-public crate-shorty-0.5.0 (c (n "shorty") (v "0.5.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nanoid") (r "^0.2") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1c79gca0w2wqp5pxq1cpr2hsiv8lgg9990nvpgnig6dpmakd2x06")))

(define-public crate-shorty-0.5.1 (c (n "shorty") (v "0.5.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nanoid") (r "^0.2") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "115c39xkwsa4dfbl7p154pi4dy3qil2ndb6jpj9xnz1knwzl20sf")))

(define-public crate-shorty-0.5.2 (c (n "shorty") (v "0.5.2") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nanoid") (r "^0.2") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1ry54x1chzcp8hlvrkkl93shmc12j0pk96s46vifvbi7801nhqb5")))

(define-public crate-shorty-0.5.3 (c (n "shorty") (v "0.5.3") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nanoid") (r "^0.2") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1k3wa0wiym83xnryvak8kc5dd8arxmjra4pamz7a7z07bvli273y")))

(define-public crate-shorty-0.5.4 (c (n "shorty") (v "0.5.4") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nanoid") (r "^0.2") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0sa89amm5m9bk3h7gv6z7c7rd0nv5wnvz83cwhhgxzvxs0g0icqq")))

