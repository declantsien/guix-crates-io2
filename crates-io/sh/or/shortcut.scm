(define-module (crates-io sh or shortcut) #:use-module (crates-io))

(define-public crate-shortcut-1.0.0 (c (n "shortcut") (v "1.0.0") (h "021gq1malrrq5lxly16rfbyjhnm6gfw70i77idyxyyggz7ml0625")))

(define-public crate-shortcut-1.1.0 (c (n "shortcut") (v "1.1.0") (h "02lmk2dwgbz7xb7y42bq97lpv185fr813rzkzv03imj6n9vfxzjp")))

(define-public crate-shortcut-2.0.0 (c (n "shortcut") (v "2.0.0") (h "06y0l7xgkmgj4x5lgb5jr519g51hjhigf43jrxn9wav968alwmxy")))

(define-public crate-shortcut-2.1.0 (c (n "shortcut") (v "2.1.0") (h "1x0r00ghklgg1w4jdzbwxyl9kd8vgslq3nwd3zcx79hdf1fpq60k")))

(define-public crate-shortcut-3.1.0 (c (n "shortcut") (v "3.1.0") (h "1vnygf2qp9l41l9jidyqv38jyf8rrmqn0rhgfrvasgf458c63h19")))

(define-public crate-shortcut-3.2.0 (c (n "shortcut") (v "3.2.0") (h "0zlk59zxw0q45fqi83hkw702190xfcayzwkmsb4804qfn1dpi1zy")))

(define-public crate-shortcut-3.2.1 (c (n "shortcut") (v "3.2.1") (h "0mq01fpp2627gmkwr0fkxs9n8mbxbv5ykp1y5x28zmg3x7k1429r")))

(define-public crate-shortcut-3.3.0 (c (n "shortcut") (v "3.3.0") (h "0gqmr7n3dllpas2nnq0bynj8q8xwfnbkpk4vqhmas402cgp8wycl")))

(define-public crate-shortcut-3.3.1 (c (n "shortcut") (v "3.3.1") (h "0sg7ybxrksjqc67rzfzs0xnb37flc97wzgz6c4chrblcay1ff055")))

(define-public crate-shortcut-3.3.2 (c (n "shortcut") (v "3.3.2") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "05g3z95khg9fvgi4xyym1q3zkgka12npdjbn6mhvr02gcq81p5x2")))

(define-public crate-shortcut-4.0.0 (c (n "shortcut") (v "4.0.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1g1f3l2cjwvj4yc1balj7x9s5wrfy23pajcsdgw9yxs4sikqfbyn")))

(define-public crate-shortcut-4.0.1 (c (n "shortcut") (v "4.0.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1a2qmxwac0y4ivdby4fw7xaar9jl70mjbqwvvrzd7dh9250gdnss")))

(define-public crate-shortcut-4.1.0 (c (n "shortcut") (v "4.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0ydkksxd5ivwhvpj7ngjlafzzhb1yk9m7qvpp6y383sf94jrfb9s")))

(define-public crate-shortcut-4.1.2 (c (n "shortcut") (v "4.1.2") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0sm8jgvvkifrqdj4r4kw8gfy1f0akchdv54gqqazgy8d6xvszd33")))

(define-public crate-shortcut-4.1.3 (c (n "shortcut") (v "4.1.3") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0rfzzhpjvr1rinl0q1abpimxazm7g6z0nnjrvnh2165hqzng1yj7")))

(define-public crate-shortcut-4.1.4 (c (n "shortcut") (v "4.1.4") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "16199m04j96lwwzbcvjdyjsx4v9f2lhaa23m3gp5m2kvwhv56fms")))

