(define-module (crates-io sh or short-text) #:use-module (crates-io))

(define-public crate-short-text-0.1.0 (c (n "short-text") (v "0.1.0") (h "14rijdjrly6iikddns561p50y00wmmvwqdpkf92hs6rpj1r5kg59") (y #t)))

(define-public crate-short-text-0.1.1 (c (n "short-text") (v "0.1.1") (h "1d65r0ixa8h9gnpw25addj76dbjgbk0sr1gikpb0kxzr0ck3jgn9") (y #t)))

