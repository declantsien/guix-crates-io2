(define-module (crates-io sh or shortscale) #:use-module (crates-io))

(define-public crate-shortscale-0.1.0 (c (n "shortscale") (v "0.1.0") (h "1qq4mcy4ql9ab2di2nci1xh9c084iikswzp6fqj2idxf1ya1dk7c")))

(define-public crate-shortscale-1.0.0 (c (n "shortscale") (v "1.0.0") (h "1x0j917l7j9y3skxwikjb255lp0lnj1mprba6kn01dgh6al9lpbs")))

(define-public crate-shortscale-1.0.1 (c (n "shortscale") (v "1.0.1") (h "0zf6blhi9bfaabq09hxldklalry9avg912vwvad53p4zykgq9s3d")))

(define-public crate-shortscale-1.0.2 (c (n "shortscale") (v "1.0.2") (h "14y91bs40l3jqx5s62x1vk911i33dcm6pknv8dvvl128flz7wwwq")))

(define-public crate-shortscale-1.0.3 (c (n "shortscale") (v "1.0.3") (h "1dprm6vm7c09hvnc94hjrgwajd5qhfz6f25m6saxxk82dvlx0biy")))

(define-public crate-shortscale-1.1.0 (c (n "shortscale") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0ikg01a2qxy636g280vn6db4yii8jj7d8sc9pn7k7amm7qf7gwzm")))

(define-public crate-shortscale-1.1.1 (c (n "shortscale") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1p0qc5q20njg5dzlnnnfs2jp57y7balv7xs3lzq5ab79wqa1h2av")))

(define-public crate-shortscale-1.2.0 (c (n "shortscale") (v "1.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "09nib0pllwqc87vfxfwyksn8pdwmw0f2l9xvlmm6g74fa2im13nl")))

(define-public crate-shortscale-1.3.0 (c (n "shortscale") (v "1.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0phnwqgmwn52gsn2sq4wq4b9lf8bkrvv2pvaa7vq3hkn4avygr9x")))

(define-public crate-shortscale-1.3.1 (c (n "shortscale") (v "1.3.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1w8bpk18msga87x2y2lj2i3x8a3313hclnj15bz59p0y5vvac92w")))

(define-public crate-shortscale-1.3.2 (c (n "shortscale") (v "1.3.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "16avd4ynd7sv0scx0hi53zdb83xmk18gr0x6fzjyjdyhapdm9h2d")))

(define-public crate-shortscale-1.3.3 (c (n "shortscale") (v "1.3.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0f0p2q9x4wmwf4kvrm4kchr6k6a2z20jnp2kkbyiylcpagccgcxd")))

(define-public crate-shortscale-1.4.0 (c (n "shortscale") (v "1.4.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0jpsy344vfri0pw7j2y2i4kxnbjbklcpzs4vxi34nlgb6skz6kmm")))

(define-public crate-shortscale-1.5.0 (c (n "shortscale") (v "1.5.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "08hmnrkb4mklqlb1b33d0qk15p3spj8fxxfrdxk9smkkx8cl36qf")))

