(define-module (crates-io sh or shortestpath) #:use-module (crates-io))

(define-public crate-shortestpath-0.1.0 (c (n "shortestpath") (v "0.1.0") (h "0zg02cqqa2l9l5q62rj9vzj4j1zg1625xky6987jgalivd99g5kl")))

(define-public crate-shortestpath-0.2.0 (c (n "shortestpath") (v "0.2.0") (h "0pgqn5wqm70qispqbq1pnxkgjj68ni9dzapxp44803j7zvamcr0y")))

