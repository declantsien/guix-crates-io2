(define-module (crates-io sh or shortcuts) #:use-module (crates-io))

(define-public crate-shortcuts-0.1.0 (c (n "shortcuts") (v "0.1.0") (h "0glm911qsp2cdryxprx0ssvwwvlc3f8f4k0rz80vyb8714wsmfvr")))

(define-public crate-shortcuts-0.1.1 (c (n "shortcuts") (v "0.1.1") (h "1nyfykir7qbkylmfyjv0ncm33py2vdhwis3rar5yrdmxwg4rqcz1")))

(define-public crate-shortcuts-0.1.2 (c (n "shortcuts") (v "0.1.2") (h "1fxd8gyzih9l34gj9b69lhz1y20gb09j9nh37sx7yrr5337hlgj1")))

