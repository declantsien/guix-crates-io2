(define-module (crates-io sh or shortlang) #:use-module (crates-io))

(define-public crate-shortlang-0.4.0 (c (n "shortlang") (v "0.4.0") (d (list (d (n "az") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "miette") (r "^5.10.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "rug") (r "^1.22.0") (f (quote ("integer" "float"))) (k 0)))) (h "1rzikdpai9wkv5dk4q5hmq9gwnvjild52ayj96x6nmlpd2lc7pnh")))

