(define-module (crates-io sh or shorty-conf) #:use-module (crates-io))

(define-public crate-shorty-conf-0.1.0 (c (n "shorty-conf") (v "0.1.0") (h "1h4h5dqp9jmlcmv5hhiyjp8v4lwb0k4h46pm1yynqihj0b0jzisb")))

(define-public crate-shorty-conf-0.2.1 (c (n "shorty-conf") (v "0.2.1") (h "0fl6fddhakd42kv8bk8901f6kj1wwrpllanjzqzirlzxnypcp267")))

(define-public crate-shorty-conf-0.3.0 (c (n "shorty-conf") (v "0.3.0") (h "16acbm229s6gq51rd80fa332qjd28z427s790vmgnnqmsz2va9rj")))

(define-public crate-shorty-conf-0.4.0 (c (n "shorty-conf") (v "0.4.0") (h "110z11g81yd3kf2253bw0zvpd14sxz8rf3d6fpzq9h356sqfnpks")))

(define-public crate-shorty-conf-0.5.0 (c (n "shorty-conf") (v "0.5.0") (h "1624szki5kk9z3igq2x0ypmcrdwp60ibbjzw252wsc7x6fyv9vwv")))

(define-public crate-shorty-conf-0.5.1 (c (n "shorty-conf") (v "0.5.1") (h "0h0wgz0m1cr3gyvpmww7saakp23z5symyj9qi8rwzpzrnp1w9008")))

(define-public crate-shorty-conf-0.5.2 (c (n "shorty-conf") (v "0.5.2") (h "0y3270x66l5a5ab0y9dww9fh38sj6yhdi9dy325xdr11lkgd2rqr")))

(define-public crate-shorty-conf-0.5.3 (c (n "shorty-conf") (v "0.5.3") (h "1y2jigx7m7izzfx7l1in4gf02b9bcxzrd1p8khcvpnb9z46911dp")))

(define-public crate-shorty-conf-0.5.4 (c (n "shorty-conf") (v "0.5.4") (h "0b8ami4mhsrdzzrp6s51rs7cjsvfrh1qh8438zrb87v6g7jc5viz")))

