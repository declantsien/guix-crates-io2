(define-module (crates-io sh or short-crypt) #:use-module (crates-io))

(define-public crate-short-crypt-1.0.0 (c (n "short-crypt") (v "1.0.0") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.3") (d #t) (k 0)) (d (n "crc-any") (r "^1.0.0") (d #t) (k 0)))) (h "0793ab6lmhrwnpbkcq32sbm31x5rr8p553hiyxkabvvwq2rqjmp7")))

(define-public crate-short-crypt-1.0.1 (c (n "short-crypt") (v "1.0.1") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.3") (d #t) (k 0)) (d (n "crc-any") (r "^1.0.0") (d #t) (k 0)))) (h "0w6wbng0d6l00ig91d8qxg6mpdfi4b2p5c6d9ai54fkmg02pr0hh")))

(define-public crate-short-crypt-1.0.2 (c (n "short-crypt") (v "1.0.2") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.3") (d #t) (k 0)) (d (n "crc-any") (r "^1.0.0") (d #t) (k 0)))) (h "1sjab5qrvrpz20qp07814m52qg7mi06n9ic47ab6gvzdjiw0z0ja")))

(define-public crate-short-crypt-1.0.3 (c (n "short-crypt") (v "1.0.3") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.3") (d #t) (k 0)) (d (n "crc-any") (r "^1.0.0") (d #t) (k 0)))) (h "0imca4yvz8ri012dfg3mg8646i8zj9i6xaqs5kmvyjb2j0kv2l10")))

(define-public crate-short-crypt-1.0.4 (c (n "short-crypt") (v "1.0.4") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.3") (d #t) (k 0)) (d (n "crc-any") (r "^1.0.0") (d #t) (k 0)))) (h "1824i3hcbicni8j1hmllj7gawd0hrg92q1vdkmvzfiaxkan3mb28")))

(define-public crate-short-crypt-1.0.5 (c (n "short-crypt") (v "1.0.5") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.3") (d #t) (k 0)) (d (n "crc-any") (r "^1.0.0") (d #t) (k 0)))) (h "1p25dl9c5ag1fw6syx5zp352sp6rwp7l2n326sds61p9lqj1vxsf")))

(define-public crate-short-crypt-1.0.6 (c (n "short-crypt") (v "1.0.6") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.7") (d #t) (k 0)) (d (n "crc-any") (r "^1.0.0") (d #t) (k 0)))) (h "0nj9zf1qrfqgyp1adjdviz9zansy0ai3f4k601365hjs5lrx2hbn")))

(define-public crate-short-crypt-1.0.7 (c (n "short-crypt") (v "1.0.7") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.7") (d #t) (k 0)) (d (n "crc-any") (r "^1.0.0") (d #t) (k 0)))) (h "1dzlh5cl27ablj6j24az2nbpzqf731g0da7ndaxq27qx9275piaj")))

(define-public crate-short-crypt-1.0.8 (c (n "short-crypt") (v "1.0.8") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.7") (d #t) (k 0)) (d (n "crc-any") (r "^2") (d #t) (k 0)))) (h "17b93bn8i4jzmy9l0vpzlnfv30wpknvg3l67kfscw3ymmfk4f6yl")))

(define-public crate-short-crypt-1.0.9 (c (n "short-crypt") (v "1.0.9") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.7") (d #t) (k 0)) (d (n "crc-any") (r "^2") (d #t) (k 0)) (d (n "debug-helper") (r "^0.1") (d #t) (k 0)))) (h "157j41fllapim3k9scl84gmryhrxb6ysbnzii3q82hh9bfxxgrx2")))

(define-public crate-short-crypt-1.0.10 (c (n "short-crypt") (v "1.0.10") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.7") (d #t) (k 0)) (d (n "crc-any") (r "^2") (d #t) (k 0)) (d (n "debug-helper") (r "^0.2") (d #t) (k 0)))) (h "02s028paxs4yxlgmwp19w6lyv8z7wqpbpq6x7bsr6b9kh7ymmv0x")))

(define-public crate-short-crypt-1.0.11 (c (n "short-crypt") (v "1.0.11") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.7") (d #t) (k 0)) (d (n "crc-any") (r "^2") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "02hmd1zplrszjwz7gvgw1imhvjmcwzixa1143v1j7831ljj8wssd")))

(define-public crate-short-crypt-1.0.12 (c (n "short-crypt") (v "1.0.12") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.7") (d #t) (k 0)) (d (n "crc-any") (r "^2") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "0n9n1dx8865k300mhbxdsfbq5s4l50q4q7ps3n85shm6iwc4qlrp")))

(define-public crate-short-crypt-1.0.13 (c (n "short-crypt") (v "1.0.13") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.7") (d #t) (k 0)) (d (n "crc-any") (r "^2") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "14n5drmdnzq06raj0vmkawi7fjny64mcipfgqihrk7x6c456wn70")))

(define-public crate-short-crypt-1.0.14 (c (n "short-crypt") (v "1.0.14") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.7") (d #t) (k 0)) (d (n "crc-any") (r "^2") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "1cqafx10m7dhv7fibsch502yrhimsnn61ynmsbb9k8zhzkpxrfkm")))

(define-public crate-short-crypt-1.0.15 (c (n "short-crypt") (v "1.0.15") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.7") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "0fiqsci9kbzrylscjkxr1mwpdkdmawcwjhxg9nmnm7nw7fkzprrw")))

(define-public crate-short-crypt-1.0.16 (c (n "short-crypt") (v "1.0.16") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.7") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "11h40z93ai2mrz4w91ycqz2h3n6hdm8bwffmlvjm8xdyd5rg2yjq")))

(define-public crate-short-crypt-1.0.17 (c (n "short-crypt") (v "1.0.17") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.1.7") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "1k6kngwwrdpw8sdnqv7s3icf2cmfnfvc63xg7s0m9b8r1cdmszjr")))

(define-public crate-short-crypt-1.0.18 (c (n "short-crypt") (v "1.0.18") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "1m5m25nffpsqpmv54v5ddfv31n1bca75279d7w31hm5v44d8pb29")))

(define-public crate-short-crypt-1.1.0 (c (n "short-crypt") (v "1.1.0") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0k3vam9dfiiqpc9j9rz6sj7i06a93ilkjhxpnjv6j971nlm09ivc") (f (quote (("std" "debug-helper") ("default" "std")))) (y #t)))

(define-public crate-short-crypt-1.0.19 (c (n "short-crypt") (v "1.0.19") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "1v9xkpkdap739dk6da05jwli12gx9if1f8w1q9ry8z1dhvv6zw2r")))

(define-public crate-short-crypt-1.0.20 (c (n "short-crypt") (v "1.0.20") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "0sg4zvflihfsqkmhypm9qflr589k6s4gw5dzvnf1qzwj90jx7adf")))

(define-public crate-short-crypt-1.0.21 (c (n "short-crypt") (v "1.0.21") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "1vk6vnw3lz3v1g542mf586gi5gqfi8aaj1q7nqb48lr4y71b34q4")))

(define-public crate-short-crypt-1.0.22 (c (n "short-crypt") (v "1.0.22") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "0ckl2z7mg65jnp6icfg8j18p34vlph6k67qbnpc21q7mg2sjc8v7")))

(define-public crate-short-crypt-1.0.23 (c (n "short-crypt") (v "1.0.23") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "0jivhg4m3ckp5qph38y4k3v6nzjx8zhwj1lixlaqzg1j53jawprg")))

(define-public crate-short-crypt-1.0.24 (c (n "short-crypt") (v "1.0.24") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "0r7kih2n5hg3iwdiisdvkn83p6wl83f70za49wb8y15vxy1kgc62")))

(define-public crate-short-crypt-1.0.25 (c (n "short-crypt") (v "1.0.25") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "0py5k5h95xh4nwmiyp45mgqb27v04kslzi2rrw3d8xaizqi75vjl")))

(define-public crate-short-crypt-1.0.26 (c (n "short-crypt") (v "1.0.26") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "1zrfcnx2ff23rkdxky773ahxyj0qbb4q4dknvdkq9qp30vx0yffr")))

(define-public crate-short-crypt-1.0.27 (c (n "short-crypt") (v "1.0.27") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^1.4") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "0wfq0ph8959dp7cr45bf6swim9fq3q2wiavicv0mmsy9vlxayfi2")))

(define-public crate-short-crypt-1.0.28 (c (n "short-crypt") (v "1.0.28") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64-url") (r "^2") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (d #t) (k 0)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "1yfps8x352jfbc04ar3rzsjds8fzxgnppdimb7i7bfn5zxqgpnff") (r "1.56")))

