(define-module (crates-io sh or shor) #:use-module (crates-io))

(define-public crate-shor-1.0.0 (c (n "shor") (v "1.0.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "18wsas6dl67bgm2spy6lwpdnizag9x6q1wzhizyvfwg2h0qd6a6p") (y #t)))

(define-public crate-shor-1.0.1 (c (n "shor") (v "1.0.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1x5nb4pxmbdykkrlqi1ll0zmfdg85iix4j6jryzq28pfhsh9n3ni") (y #t)))

(define-public crate-shor-1.0.2 (c (n "shor") (v "1.0.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "12i3br8r5s1ibszlkdxc9r7i7klg02w42kacn8w8s3i39vk89v1m") (y #t)))

(define-public crate-shor-1.0.3 (c (n "shor") (v "1.0.3") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1hvhqrw5as1wdyzb1l0qny3nq9vj3jyjnd67d6w3p7hkchd53bpx") (y #t)))

(define-public crate-shor-1.0.4 (c (n "shor") (v "1.0.4") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0fimph32lsvva7dyi5dxyk2sv9xlmhb12yz0d57p74s1vgjx3a90") (y #t)))

(define-public crate-shor-1.0.5 (c (n "shor") (v "1.0.5") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0da8an9xqyvl9hi105mks73kzgayl65ikj5zan7c6zzqa4nfnrc7") (y #t)))

(define-public crate-shor-0.0.1 (c (n "shor") (v "0.0.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1j42n4bxsygszfpl9zqji94df7lr4bcghywqyr1y3lx2gzdchrli") (y #t)))

(define-public crate-shor-0.0.2 (c (n "shor") (v "0.0.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1jgijasz4lrcvzbcs07np3xrslylh4lh400zzl9m3avky9769w25")))

