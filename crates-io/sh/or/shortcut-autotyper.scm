(define-module (crates-io sh or shortcut-autotyper) #:use-module (crates-io))

(define-public crate-shortcut-autotyper-0.1.0 (c (n "shortcut-autotyper") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b4vk1ldn0m0k3vrp4h2xy77vq5qipc1b4z44iw1v63r8d1fn4c1")))

