(define-module (crates-io sh or short_print) #:use-module (crates-io))

(define-public crate-short_print-0.1.0 (c (n "short_print") (v "0.1.0") (h "0lzbfs1z81hs7l3hn7a1sq77052gl54d9mjmvg907y7hw08ym6zp")))

(define-public crate-short_print-0.1.1 (c (n "short_print") (v "0.1.1") (h "00ikb2mg7g43nq09x8bsmmcrm24yjg94abcjjs49isdp4lnmr01d")))

