(define-module (crates-io sh or shortcut_assert_fs) #:use-module (crates-io))

(define-public crate-shortcut_assert_fs-0.1.0 (c (n "shortcut_assert_fs") (v "0.1.0") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 0)) (d (n "camino") (r "^1.1") (d #t) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "include_dir") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1v2d2l4lyxv8hcaagcx3qlgpgkjvdlsx9id8v16qgbhpr50szcxm")))

