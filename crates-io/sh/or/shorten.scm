(define-module (crates-io sh or shorten) #:use-module (crates-io))

(define-public crate-shorten-0.1.0 (c (n "shorten") (v "0.1.0") (h "13k3093ly35iv1s67g5sbjj70pdv0yh1ylwqvqimcw9mbzff27z8")))

(define-public crate-shorten-0.1.1 (c (n "shorten") (v "0.1.1") (h "12lsrc0pmiclqm9wsgwz1zq9fjij92m8p9sd905w54nynd7jvs0n")))

(define-public crate-shorten-0.1.2 (c (n "shorten") (v "0.1.2") (h "03188sj3wcn0f0n954zbvjn6vjlp5yl8bmy4bprspb4dk648mnsv")))

(define-public crate-shorten-0.1.3 (c (n "shorten") (v "0.1.3") (h "0pi6x4b5c8b80s7j5a4q2mabdvn9a5ax2jxxcwq0ppdhrclf8mk0")))

(define-public crate-shorten-0.1.4 (c (n "shorten") (v "0.1.4") (h "14y03bp5g8d44zyzl2rcy7nfm8vard34q4c9krirkvf357cdi7jh") (y #t)))

(define-public crate-shorten-0.1.5 (c (n "shorten") (v "0.1.5") (h "00r4wybbvf9wqkadgkdgrzbl4csbspj1qvbdnb0c7af2b79l8k3w")))

(define-public crate-shorten-0.2.0 (c (n "shorten") (v "0.2.0") (h "00pc2m6ra68j826lm0mcjffcz5xlk4aij3skcwkvc68qgmya3xb6")))

