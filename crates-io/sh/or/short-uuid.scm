(define-module (crates-io sh or short-uuid) #:use-module (crates-io))

(define-public crate-short-uuid-0.1.0 (c (n "short-uuid") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0acf9glpjsn2ig1g947hwad1ivfyyiy6f2d0dvk7x576hf5g20p5") (f (quote (("std") ("default" "std"))))))

(define-public crate-short-uuid-0.1.1 (c (n "short-uuid") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1cd946q37in7pga8rkys4r6z2wn0m0jvn3574a878j56g1klq1ig") (f (quote (("std") ("default" "std"))))))

(define-public crate-short-uuid-0.1.2 (c (n "short-uuid") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "01ci83653pzrz4m1sk88ccw2skjjwkzs25ij44s3865lvrgq0ms2") (f (quote (("std") ("default" "std"))))))

