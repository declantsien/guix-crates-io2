(define-module (crates-io sh or shortcutd) #:use-module (crates-io))

(define-public crate-shortcutd-0.1.0 (c (n "shortcutd") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "evdev-shortcut") (r "^0.1.2") (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "zbus") (r "^3.13.1") (f (quote ("tokio"))) (k 0)))) (h "05ckpgrnqvjd52ag6dxwmnf3nccgypp5wmxdaxjwr1p2p17kd5ky")))

(define-public crate-shortcutd-0.2.0 (c (n "shortcutd") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "evdev-shortcut") (r "^0.1.4") (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "zbus") (r "^3.13.1") (f (quote ("tokio"))) (k 0)))) (h "1xyzmik4ckhdw2vk8vcb1rsy30fn9w426fcl2q8xgiqh9x6c525g")))

