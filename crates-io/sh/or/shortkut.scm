(define-module (crates-io sh or shortkut) #:use-module (crates-io))

(define-public crate-shortkut-0.1.0 (c (n "shortkut") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "difflib") (r "^0.4.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "minreq") (r "^2.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "termimad") (r "^0.11.1") (d #t) (k 0)))) (h "1kr15x656wvzwn3aqjn6il438q3fkck70qv5z2kz41x4hgzpk9ms")))

