(define-module (crates-io sh or shortlist) #:use-module (crates-io))

(define-public crate-shortlist-0.1.0 (c (n "shortlist") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0169hz7hlf7vy33580cjl7ng20dxnh6q9a44xf93r1wrcw7809c7")))

(define-public crate-shortlist-0.1.1 (c (n "shortlist") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0yv3fr7l41klshligr8a679i8rwpws4k03vz3az5z3mvxnzdk0pa")))

(define-public crate-shortlist-0.1.2 (c (n "shortlist") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0vz0c3c58fskyvx3wpjiijkm2gi9d40yfa429y9vjx3arnv3100y")))

(define-public crate-shortlist-0.2.0 (c (n "shortlist") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0qaip0wmhlsx1g3hk1lsg01ml7s7hs8l2kafh3wfmcswj14vgi3a")))

