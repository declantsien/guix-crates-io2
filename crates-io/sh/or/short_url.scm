(define-module (crates-io sh or short_url) #:use-module (crates-io))

(define-public crate-short_url-0.1.0 (c (n "short_url") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0cjkkqi5js22x8xnrsrrvpasf7f3w8i4zihhp435xp28dfryb8zm")))

(define-public crate-short_url-1.0.0 (c (n "short_url") (v "1.0.0") (h "0wnxwmbxlnc9cz0yg9bpd7np9gxy802i7nkl487lan69zi3nq8ni")))

