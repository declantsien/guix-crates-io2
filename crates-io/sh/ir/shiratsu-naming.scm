(define-module (crates-io sh ir shiratsu-naming) #:use-module (crates-io))

(define-public crate-shiratsu-naming-0.1.0 (c (n "shiratsu-naming") (v "0.1.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "05qn1frszcsbxz7jx7xp654i6ps0k5334q22h780yn2n0rj42fig")))

(define-public crate-shiratsu-naming-0.1.1 (c (n "shiratsu-naming") (v "0.1.1") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "1ycgsp8sdkpkvzak0i42rjajvckww7ysw0pv2wy9nxhhvrg7scgn")))

(define-public crate-shiratsu-naming-0.1.2 (c (n "shiratsu-naming") (v "0.1.2") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "12mm7bad1l2n081svrz31178m3199ld47ri3k4065cqb9p5ivqhk")))

(define-public crate-shiratsu-naming-0.1.3 (c (n "shiratsu-naming") (v "0.1.3") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0hrdfmmb2qwxfj3h0mq8ih174cjlhlwa24nm4p1r4q04ywpg2cqs")))

(define-public crate-shiratsu-naming-0.1.4 (c (n "shiratsu-naming") (v "0.1.4") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "18p6b1v5jm4h1rk0s5l61z25vdqbm9kp9m9dzvs5azk904p3q4yx")))

(define-public crate-shiratsu-naming-0.1.5 (c (n "shiratsu-naming") (v "0.1.5") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "1aiym1m5kd5j3paqd39d5j9156vp5j2qnic9a0z2s9a63j5pi8x2")))

(define-public crate-shiratsu-naming-0.1.6 (c (n "shiratsu-naming") (v "0.1.6") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "061zpc7x98j5bhcq8cq3049h0p85pd9s1n0y2zniafy1d7m98b8k")))

(define-public crate-shiratsu-naming-0.1.7 (c (n "shiratsu-naming") (v "0.1.7") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "185b4023ss77q1p3nwwwv6kzx13j30x235njz291m0v3cqfci67f")))

