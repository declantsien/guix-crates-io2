(define-module (crates-io sh ir shiru) #:use-module (crates-io))

(define-public crate-shiru-0.1.0 (c (n "shiru") (v "0.1.0") (h "1rid2q9379b56bhvrlfi64dngnh9fpki627bb7ldb7l1kkzajcv1")))

(define-public crate-shiru-0.1.1 (c (n "shiru") (v "0.1.1") (h "1mqr2wsx9n2122iwcpaf51pmyini2q3jl4yvhv2xj5pqjgnvbznq")))

(define-public crate-shiru-0.1.3 (c (n "shiru") (v "0.1.3") (h "0knjy02n6njfps4agayzc00q072sz273gn4av9r8vxqjdmx791ba")))

(define-public crate-shiru-0.1.4 (c (n "shiru") (v "0.1.4") (h "0fzahxxhphysp7xyk53w1y860r0w8h6x2wx4g5rx1z6lm35k1n2h")))

(define-public crate-shiru-0.1.5 (c (n "shiru") (v "0.1.5") (h "1rc3d1qq93gh8ggy1i3llqapnlyhx35cxn6r922kwwswx6clakry")))

(define-public crate-shiru-0.1.6 (c (n "shiru") (v "0.1.6") (h "189sgx0rykyfilnwqz7lp0pxqhvyphzx26c3ng77bxw32w2hzvc1")))

(define-public crate-shiru-0.1.7 (c (n "shiru") (v "0.1.7") (h "098akc2f95qvfnmg4h28c8jhcvava65ilg22vl53q8aplnqx6q58")))

