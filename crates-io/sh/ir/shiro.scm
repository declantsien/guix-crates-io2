(define-module (crates-io sh ir shiro) #:use-module (crates-io))

(define-public crate-shiro-0.0.1 (c (n "shiro") (v "0.0.1") (d (list (d (n "base16ct") (r "^0.2.0") (d #t) (k 0)) (d (n "base64ct") (r "^1.6.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "md2") (r "^0.10.2") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "0l73dblpkc8g10wi1wl3z8b4fjh0503m45s52y1lihv2mxpq58n8")))

