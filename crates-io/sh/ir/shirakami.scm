(define-module (crates-io sh ir shirakami) #:use-module (crates-io))

(define-public crate-shirakami-0.1.1 (c (n "shirakami") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1l3h56psh3l9sq9lx24kmpvjyfp7bcsd068v5xkkvc80vv5h2w19")))

(define-public crate-shirakami-0.1.2 (c (n "shirakami") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1bkddnmwdfx7q7ycaw2wjkijb8fi5zq5sgfycsvxsqr3i6nmhy4w")))

