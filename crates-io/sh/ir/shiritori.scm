(define-module (crates-io sh ir shiritori) #:use-module (crates-io))

(define-public crate-shiritori-0.1.0 (c (n "shiritori") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0scixbyi8vwkzzb7l3q2fm6dq5ar7x06a08lmb8bii62md60zcik")))

(define-public crate-shiritori-0.1.1 (c (n "shiritori") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0p8mn9c2r2a4cfc13r6vcnan63hfs1fx6wm4inv2kngyy788c912")))

