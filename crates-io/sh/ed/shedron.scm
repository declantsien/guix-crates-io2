(define-module (crates-io sh ed shedron) #:use-module (crates-io))

(define-public crate-shedron-0.1.1 (c (n "shedron") (v "0.1.1") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "13bwa1w4s974ss5gv5hxz7m9vp2xcn5gjxbvigr80xnr5gvc7bvf") (f (quote (("map-operators-do-single") ("default" "map-operators-do-single"))))))

(define-public crate-shedron-0.1.2 (c (n "shedron") (v "0.1.2") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0h0al7fswbnzz3d1zzx5yb1irj6qcs0vhn4pvalnczqb28h6wxzy") (f (quote (("map-operators-do-single") ("default" "map-operators-do-single"))))))

(define-public crate-shedron-0.1.3 (c (n "shedron") (v "0.1.3") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dmylvsycr6yg0m2adj8ckhvazcj9gvlcrgxlm4r9k24mr7v7dc1") (f (quote (("map-operators-do-single") ("default" "map-operators-do-single"))))))

(define-public crate-shedron-0.1.4 (c (n "shedron") (v "0.1.4") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1mb9nglglbvs1gcmg4cbpqsdmppclsya8l2han6bjrdhscsch9qx") (f (quote (("map-operators-do-single") ("default" "map-operators-do-single"))))))

