(define-module (crates-io sh a1 sha1-checked) #:use-module (crates-io))

(define-public crate-sha1-checked-0.10.0 (c (n "sha1-checked") (v "0.10.0") (d (list (d (n "digest") (r "^0.10.7") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "sha1") (r "^0.10.6") (f (quote ("compress"))) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (k 0)))) (h "08s4h1drgwxzfn1mk11rn0r9i0rbjra1m0l2c0fbngij1jn9kxc9") (f (quote (("std" "digest/std" "sha1/std") ("oid" "digest/oid" "sha1/oid") ("default" "oid" "std")))) (s 2) (e (quote (("zeroize" "dep:zeroize")))) (r "1.72")))

