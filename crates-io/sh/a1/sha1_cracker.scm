(define-module (crates-io sh a1 sha1_cracker) #:use-module (crates-io))

(define-public crate-sha1_cracker-0.1.0 (c (n "sha1_cracker") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "16cj6hclv5hvmf7r7zdd20adgxjfl4z8r1i9cppkix72sm4rnnyp")))

