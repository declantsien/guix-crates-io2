(define-module (crates-io sh a1 sha1dir) #:use-module (crates-io))

(define-public crate-sha1dir-0.1.0 (c (n "sha1dir") (v "0.1.0") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "038h7zfspwlpvk0gs97jxa54gvcsqhc34x9m514xn7s6cb5cwqh8")))

(define-public crate-sha1dir-0.1.1 (c (n "sha1dir") (v "0.1.1") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1rq84b5l83vlf9xg5f1vabpayd7v30pw95mhhblc2pk7hi5nygxs")))

(define-public crate-sha1dir-0.1.2 (c (n "sha1dir") (v "0.1.2") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "046v6a04k0skv0h554zhmppmwqc5x535rj6ydnanvrj30x323c2h")))

(define-public crate-sha1dir-0.1.3 (c (n "sha1dir") (v "0.1.3") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1pwdmhv2zvg8k4gf9dw8m35pyzvvf9m6rbr0yp81wbkvhrk7xns5")))

(define-public crate-sha1dir-1.0.0 (c (n "sha1dir") (v "1.0.0") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0fhp3xsn41cpmkfw73430mhsrqn26gc5ixfi0mr6z8bcrfahix6b")))

(define-public crate-sha1dir-1.0.1 (c (n "sha1dir") (v "1.0.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive" "std" "suggestions"))) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)))) (h "0cci1c6jcis2lcn3cfbkks96zfvz7mf7wrk7i05xc95g9m749ykl")))

(define-public crate-sha1dir-1.0.2 (c (n "sha1dir") (v "1.0.2") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive" "std" "suggestions"))) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)))) (h "1g9q9dx5ka76a8i973jdc46wcnm8qrpnc6gsqqv9kf5wlcdkbmq7")))

(define-public crate-sha1dir-1.0.3 (c (n "sha1dir") (v "1.0.3") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive" "std" "suggestions"))) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)))) (h "1ibbfvc4h8cmkrqf2i285y36cs1cwprka81pzw0w4m9cif61wb64")))

(define-public crate-sha1dir-1.0.4 (c (n "sha1dir") (v "1.0.4") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("deprecated" "derive" "std" "suggestions"))) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)))) (h "1m1bmcajikdyxz02dbg5dq6m0gv80qz69jkba8xk7hnn7hy79qrf")))

(define-public crate-sha1dir-1.0.5 (c (n "sha1dir") (v "1.0.5") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("deprecated" "derive" "std" "suggestions"))) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)))) (h "1zsim01i9lxxr2p4c4cgajd3q8qc3glcibacs5hd9mzpy94x67w5")))

(define-public crate-sha1dir-1.0.6 (c (n "sha1dir") (v "1.0.6") (d (list (d (n "clap") (r "^4") (f (quote ("deprecated" "derive"))) (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)))) (h "1zhr8vjsiz6cvm0hcq3gbvy763hac771q74jbcqdyr1bbpmgw41n")))

(define-public crate-sha1dir-1.0.7 (c (n "sha1dir") (v "1.0.7") (d (list (d (n "clap") (r "^4") (f (quote ("deprecated" "derive"))) (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)))) (h "10xq9wsrmr9di469q6dsbq277f35bppwiamffj763zc2kpgdbw76")))

(define-public crate-sha1dir-1.0.8 (c (n "sha1dir") (v "1.0.8") (d (list (d (n "clap") (r "^4") (f (quote ("deprecated" "derive"))) (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)))) (h "19ndm724pp0giz56gzrnh9wxk5qraydv87qj40mkbzcndyvzr8w7")))

(define-public crate-sha1dir-1.0.9 (c (n "sha1dir") (v "1.0.9") (d (list (d (n "clap") (r "^4") (f (quote ("deprecated" "derive"))) (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)))) (h "1ww2zpka0lg26xv8p0zfcvx696xkna3x1fxsdvyxnjxrhrylchvi")))

(define-public crate-sha1dir-1.0.10 (c (n "sha1dir") (v "1.0.10") (d (list (d (n "clap") (r "^4") (f (quote ("deprecated" "derive"))) (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)))) (h "1xi4z3k5xxq4qmapbrphmm9nn8d3j8h8kyhr9wmh9zcrindgy4hm")))

