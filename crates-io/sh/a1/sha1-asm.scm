(define-module (crates-io sh a1 sha1-asm) #:use-module (crates-io))

(define-public crate-sha1-asm-0.1.0 (c (n "sha1-asm") (v "0.1.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1kc78x08bzr4ba8rhhsvsld1arycbz7m47q21d6brzx1gx4wx9g7")))

(define-public crate-sha1-asm-0.2.0 (c (n "sha1-asm") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "032sy6mqiwivc6jpmc5rc2sr6vaysyyr0dzdgmvwr99cjk5i2s46")))

(define-public crate-sha1-asm-0.2.1 (c (n "sha1-asm") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1qz4kknlqbij6i9p36y0a9p4y5lls378j0164ry448rhzp8anb13")))

(define-public crate-sha1-asm-0.3.0 (c (n "sha1-asm") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "1fg6bbikm3b2rk91d04nkc3r7kahyfq92vb6l3iixbwbyac5w322")))

(define-public crate-sha1-asm-0.4.0 (c (n "sha1-asm") (v "0.4.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0ks5p52z31fwbzkrn4zi4grchbjjba4w8d86r614rqg8kdghhqak")))

(define-public crate-sha1-asm-0.4.1 (c (n "sha1-asm") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0mwq7amdicsqd27npb58rmi299z4hq2cjx70jv6fg89npfnp1yxc")))

(define-public crate-sha1-asm-0.4.2 (c (n "sha1-asm") (v "0.4.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "054l2i52mndkxk0k2f6sci2vwbqhy6nm13vgin4vjr520wy3kkml")))

(define-public crate-sha1-asm-0.4.3 (c (n "sha1-asm") (v "0.4.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1i1i8viy6y30mv9v5hwhg9w6b722qkyh9c6n8bn4d27jpv14pg0s")))

(define-public crate-sha1-asm-0.4.4 (c (n "sha1-asm") (v "0.4.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1z5vdimd7l0vmr2p7kjibi0rghf5frb1ld0gzdkxrxfmkllf5nmr")))

(define-public crate-sha1-asm-0.5.0 (c (n "sha1-asm") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1p4pz4vkqvq9bzlx4qmyc21kpw27d1s1ykw0xizmd8fh1wkb5wgm")))

(define-public crate-sha1-asm-0.5.1 (c (n "sha1-asm") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1b7ab7f4n87pqdmbl1a5jrc2axf27pvbndsz9qiwwgxw01qlygan")))

(define-public crate-sha1-asm-0.5.2 (c (n "sha1-asm") (v "0.5.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "08af3yv0wl8ljp4964whz2yms22pb0svf05gp3inpy778mvr99ib")))

(define-public crate-sha1-asm-0.5.3 (c (n "sha1-asm") (v "0.5.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0asqxlxf5li7ac9mi49qj890rzsfb5px5ynzmqq12z5nz2xcwsi8")))

