(define-module (crates-io sh a1 sha1) #:use-module (crates-io))

(define-public crate-sha1-0.0.1 (c (n "sha1") (v "0.0.1") (h "1831pd102z8g787mdsa77c6nwdfx5myalgp6n3f9rzs9kavzq99p")))

(define-public crate-sha1-0.0.2 (c (n "sha1") (v "0.0.2") (h "19lcdddg7x31rb9him8mwy5xszwk08ha8psw3n9h21lpwc060m98")))

(define-public crate-sha1-0.0.3 (c (n "sha1") (v "0.0.3") (h "07ja50i0ixq2gcpggm2nv3hyrdwflrfi4fgvmlm3v25zkamsaq5m")))

(define-public crate-sha1-0.0.5 (c (n "sha1") (v "0.0.5") (h "03jnacmz0ah7ir5dnzfljxk1795la31gjdjhv1wlyzssf1w09ddc")))

(define-public crate-sha1-0.0.6 (c (n "sha1") (v "0.0.6") (h "14dha7nl7sp9q4w4bn6gw345vdgjjpd2c742k2z047idrvwdiwmw")))

(define-public crate-sha1-0.0.7 (c (n "sha1") (v "0.0.7") (h "1prm8mm3n51raif3h8bz2xf96gxkn1p1y0kvqsngq9n3bxz93v4k")))

(define-public crate-sha1-0.0.8 (c (n "sha1") (v "0.0.8") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "0nxvxvpws0sgsba6gr2vglr3iybzwmcd6b3ndsq0q90dbbjb9gd3")))

(define-public crate-sha1-0.1.0 (c (n "sha1") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "16l25j7m5k4bija1pmymml7jz5vcr3iqscndailp5gnsn4q43g5g")))

(define-public crate-sha1-0.1.1 (c (n "sha1") (v "0.1.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "0g550pjhbqx66mrqp4dbvhxxb6jfp21j95d62d10w51lb06s81x3")))

(define-public crate-sha1-0.2.0 (c (n "sha1") (v "0.2.0") (d (list (d (n "openssl") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0p09zfhd27z6yr5in07gfjcx345010rw51ivlcf14364x3hv2c6c")))

(define-public crate-sha1-0.3.0 (c (n "sha1") (v "0.3.0") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0xssknzbhx420z3c9a9bflfp3i4xalrq5fmdl2cxbiwf693nwppk")))

(define-public crate-sha1-0.4.0 (c (n "sha1") (v "0.4.0") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "094rfb410rw85n47sngmpf0kpp9r1x6igw8v5hd0xfvhzb7x4glk")))

(define-public crate-sha1-0.5.0 (c (n "sha1") (v "0.5.0") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "17iddyfrbsp4g8kkvqwqrdnfjwjd3c190cgbmcxvkjy79v79h5hp") (f (quote (("std"))))))

(define-public crate-sha1-0.6.0 (c (n "sha1") (v "0.6.0") (d (list (d (n "openssl") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "03gs2q4m67rn2p8xcdfxhip6mpgahdwm12bnb3vh90ahv9grhy95") (f (quote (("std"))))))

(define-public crate-sha1-0.6.1 (c (n "sha1") (v "0.6.1") (d (list (d (n "sha1_smol") (r "^1.0.0") (d #t) (k 0)))) (h "0w1p0s9060cv1vlgfa5c93kjksmvzjjc8j780lns3jj5fk4hbnn1") (f (quote (("std" "sha1_smol/std") ("serde" "sha1_smol/serde"))))))

(define-public crate-sha1-0.10.0 (c (n "sha1") (v "0.10.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "sha1-asm") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1slbhy12bdpzs73fgvgghmlgfvrghknlvg9rzjgnijsbp6gj5k04") (f (quote (("std" "digest/std") ("force-soft") ("default" "std") ("compress") ("asm" "sha1-asm"))))))

(define-public crate-sha1-0.10.1 (c (n "sha1") (v "0.10.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "sha1-asm") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0bw56hxajrgb3pjg0cr5xrvmx0jna39564iw2p14ama5cmzlwzy7") (f (quote (("std" "digest/std") ("force-soft") ("default" "std") ("compress") ("asm" "sha1-asm"))))))

(define-public crate-sha1-0.10.2 (c (n "sha1") (v "0.10.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "sha1-asm") (r "^0.5") (o #t) (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0fmnrik1qpyhijcdkkfk1bgk1651xs8ig2k0kd7xsi589hd7iwjw") (f (quote (("std" "digest/std") ("force-soft") ("default" "std") ("compress") ("asm" "sha1-asm"))))))

(define-public crate-sha1-0.10.3 (c (n "sha1") (v "0.10.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "sha1-asm") (r "^0.5") (o #t) (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "08yhl7f5r144mv2hdmcvadqda7r0731hiypcpmgjzqalpdr71fcs") (f (quote (("std" "digest/std") ("force-soft") ("default" "std") ("compress") ("asm" "sha1-asm"))))))

(define-public crate-sha1-0.10.4 (c (n "sha1") (v "0.10.4") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "sha1-asm") (r "^0.5") (o #t) (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0jbmbkzqxk5w5xiz8v0srfs95kq0dcc390xabh4b68g9hfx6jrq0") (f (quote (("std" "digest/std") ("force-soft") ("default" "std") ("compress") ("asm" "sha1-asm"))))))

(define-public crate-sha1-0.10.5 (c (n "sha1") (v "0.10.5") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "digest") (r "^0.10.4") (d #t) (k 0)) (d (n "digest") (r "^0.10.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "sha1-asm") (r "^0.5") (o #t) (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "18zb80sxn31kxdpl1ly6w17hkrvyf08zbxnpy8ckb6f3h3f96hph") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("force-soft") ("default" "std") ("compress") ("asm" "sha1-asm"))))))

(define-public crate-sha1-0.10.6 (c (n "sha1") (v "0.10.6") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "digest") (r "^0.10.7") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "sha1-asm") (r "^0.5") (o #t) (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1fnnxlfg08xhkmwf2ahv634as30l1i3xhlhkvxflmasi5nd85gz3") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("loongarch64_asm") ("force-soft") ("default" "std") ("compress") ("asm" "sha1-asm"))))))

(define-public crate-sha1-0.11.0-pre.0 (c (n "sha1") (v "0.11.0-pre.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "digest") (r "=0.11.0-pre.3") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "sha1-asm") (r "^0.5") (o #t) (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0l38w20nj2zifspjf4qy3hi5pj2yhvpk2hccm936lj74ic18ci61") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("loongarch64_asm") ("force-soft") ("default" "oid" "std") ("compress") ("asm" "sha1-asm")))) (r "1.71")))

(define-public crate-sha1-0.11.0-pre.1 (c (n "sha1") (v "0.11.0-pre.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "digest") (r "=0.11.0-pre.4") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "sha1-asm") (r "^0.5") (o #t) (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1rjzjc384qr978b1f16spn7kimaib5x77m1a8g3b87zzd6w146bk") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("loongarch64_asm") ("force-soft") ("default" "oid" "std") ("compress") ("asm" "sha1-asm")))) (r "1.71")))

(define-public crate-sha1-0.11.0-pre.2 (c (n "sha1") (v "0.11.0-pre.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "digest") (r "=0.11.0-pre.7") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "07zbjnjwaz80lzps00alxh9qyqhva7fvqgl4l637smbkv26x87ih") (f (quote (("zeroize" "digest/zeroize") ("std" "digest/std") ("oid" "digest/oid") ("force-soft") ("default" "oid" "std")))) (r "1.72")))

(define-public crate-sha1-0.11.0-pre.3 (c (n "sha1") (v "0.11.0-pre.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "digest") (r "=0.11.0-pre.8") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "1lzynia231mkbkf04q776diq07xr82l4gk8wiiqj7whnp66dx19q") (f (quote (("zeroize" "digest/zeroize") ("std" "digest/std") ("oid" "digest/oid") ("force-soft") ("default" "oid" "std")))) (r "1.72")))

