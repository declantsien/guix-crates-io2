(define-module (crates-io sh a1 sha1-hasher) #:use-module (crates-io))

(define-public crate-sha1-hasher-0.0.1 (c (n "sha1-hasher") (v "0.0.1") (h "1kmqw0w0byr9ciklh3144k127y4b67w0rzazsmwk862m5j05b3kx") (y #t)))

(define-public crate-sha1-hasher-0.0.2 (c (n "sha1-hasher") (v "0.0.2") (h "1gh3684skjgsl7ma9a7lxz44rxx5513dj65wnbzpxyn9pw3dyc7g") (y #t)))

(define-public crate-sha1-hasher-0.0.3 (c (n "sha1-hasher") (v "0.0.3") (h "0n2dyralgdsigbws5m192v1v2n4vswdpqfg9zpizrz6axhwdjjlk") (y #t)))

(define-public crate-sha1-hasher-0.0.4 (c (n "sha1-hasher") (v "0.0.4") (h "15fjcd16y4swcdkwvsym2mwlhv8q3yf4r2j6n2i6j94rvbn0sc4a") (y #t)))

(define-public crate-sha1-hasher-0.0.5 (c (n "sha1-hasher") (v "0.0.5") (h "1i3a4ri6byzsqrd93cpgxlxhk1r3rhv3jjcjhlpl8ngflal7iyq9") (y #t)))

(define-public crate-sha1-hasher-0.0.6 (c (n "sha1-hasher") (v "0.0.6") (h "1lphrxx22imagbsajxngqshnxwl07myz703vvimpqiar3rrdicv7") (y #t)))

(define-public crate-sha1-hasher-0.1.0 (c (n "sha1-hasher") (v "0.1.0") (h "1xmz4f1gqm9s1yh8x8qb03fkl29y10dq3k1xmz0azl1yhd7as7wg") (y #t)))

