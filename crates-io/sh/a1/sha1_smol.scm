(define-module (crates-io sh a1 sha1_smol) #:use-module (crates-io))

(define-public crate-sha1_smol-1.0.0 (c (n "sha1_smol") (v "1.0.0") (d (list (d (n "openssl") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "04nhbhvsk5ms1zbshs80iq5r1vjszp2xnm9f0ivj38q3dhc4f6mf") (f (quote (("std"))))))

