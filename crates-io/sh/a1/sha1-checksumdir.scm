(define-module (crates-io sh a1 sha1-checksumdir) #:use-module (crates-io))

(define-public crate-sha1-checksumdir-0.1.0 (c (n "sha1-checksumdir") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "14xfhys4h9ins47hkv2cx1fnbqhvfd1ff3587q2v4h4l38anp2v8")))

