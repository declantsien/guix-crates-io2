(define-module (crates-io sh a1 sha1-macros) #:use-module (crates-io))

(define-public crate-sha1-macros-1.0.0 (c (n "sha1-macros") (v "1.0.0") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1b1vwnawlqpb2hkgbflb42rsmsl5l9w7ca3axg76kwkrb7n8mhzx")))

