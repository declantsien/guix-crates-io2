(define-module (crates-io sh a_ sha_256_scratch) #:use-module (crates-io))

(define-public crate-sha_256_scratch-0.1.1 (c (n "sha_256_scratch") (v "0.1.1") (h "1icfinslxx6yvm90gz05l88jlqdihizp68r0nw4x32mjnxl09ppm")))

(define-public crate-sha_256_scratch-0.1.2 (c (n "sha_256_scratch") (v "0.1.2") (h "0yyza3p062nrssf5raavy3xhsinza1lq6s9hhdkxqvwrb7vkfd00")))

