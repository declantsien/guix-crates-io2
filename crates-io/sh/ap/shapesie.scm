(define-module (crates-io sh ap shapesie) #:use-module (crates-io))

(define-public crate-shapesie-0.1.0 (c (n "shapesie") (v "0.1.0") (d (list (d (n "inquire") (r "^0.6.0") (f (quote ("date" "editor"))) (d #t) (k 0)))) (h "0r3jdkp0hlbh13wc2akdadjcn9qpjp3zx25md6z7sa90q0b6wpz8")))

(define-public crate-shapesie-0.2.0 (c (n "shapesie") (v "0.2.0") (d (list (d (n "inquire") (r "^0.6.0") (f (quote ("date" "editor"))) (d #t) (k 0)))) (h "01dly7q9j0gwqqgiz758695cm26azkhyxqazw2j8r60kq6hql20k")))

(define-public crate-shapesie-0.2.1 (c (n "shapesie") (v "0.2.1") (d (list (d (n "inquire") (r "^0.6.0") (f (quote ("date" "editor"))) (d #t) (k 0)))) (h "15180p4cwmhdpxhnqj1x7r12l3lkwhap9s2cjx7gbl758si7pk6w")))

(define-public crate-shapesie-0.2.2 (c (n "shapesie") (v "0.2.2") (d (list (d (n "inquire") (r "^0.6.0") (f (quote ("date" "editor"))) (d #t) (k 0)))) (h "0sqrgihmb81vx9ikardz1ar46mz9ckh7xfsxgzlznqfp1zvh5vh5")))

(define-public crate-shapesie-0.2.3 (c (n "shapesie") (v "0.2.3") (d (list (d (n "inquire") (r "^0.6.0") (f (quote ("date" "editor"))) (d #t) (k 0)))) (h "1ihq57hxnqvjszpnnphnzwy3rx2asiv8pmmp1mx66f1kajhyaz39")))

(define-public crate-shapesie-0.2.4 (c (n "shapesie") (v "0.2.4") (d (list (d (n "inquire") (r "^0.6.0") (f (quote ("date" "editor"))) (d #t) (k 0)))) (h "1vnj59aydc1wyrys9qr91f4z5cvwh2h43f5s3l91095l0qs4a53d")))

(define-public crate-shapesie-0.3.0 (c (n "shapesie") (v "0.3.0") (d (list (d (n "inquire") (r "^0.6.0") (f (quote ("date" "editor"))) (d #t) (k 0)))) (h "1j92w8lzqsxd8f3chkk55jrm20i5bwxh43lfchvn418gkkl54i3k")))

(define-public crate-shapesie-1.0.0 (c (n "shapesie") (v "1.0.0") (d (list (d (n "inquire") (r "^0.6.0") (f (quote ("date" "editor"))) (d #t) (k 0)))) (h "12r7afdl3m4myiw9fw54km8p5b0vdvkr1di8pbm92jys0xj1n2dx")))

