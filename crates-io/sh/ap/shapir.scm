(define-module (crates-io sh ap shapir) #:use-module (crates-io))

(define-public crate-shapir-0.0.1 (c (n "shapir") (v "0.0.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "1pmw2nncg2rsypybfsqsl38q0x0kicswwr3y14y3j0lkx1p47b6g")))

(define-public crate-shapir-0.0.2 (c (n "shapir") (v "0.0.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "00xaxshs6d0ackbaacxjcbf93xr4kzn19v44dav525ac5isbvdhg")))

(define-public crate-shapir-0.0.3 (c (n "shapir") (v "0.0.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "17svc6bfj7pl4al4c2k2hdn3ksljpryxikgk5ybzajyzmja32i0p")))

(define-public crate-shapir-0.0.4 (c (n "shapir") (v "0.0.4") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "0sybxr74sx2nhaq0k0l5gsz3mb1z650rrr3avbvf4k7d21cs4gja")))

(define-public crate-shapir-0.0.5 (c (n "shapir") (v "0.0.5") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "1j9dvg22ka4n6vnzqp9qx5pnq3xxbvkchvy3iq22xsjq8nysxk8q")))

(define-public crate-shapir-0.0.6 (c (n "shapir") (v "0.0.6") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "md5") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "0hw80hhpxdk6wjglp5avak3kpv0ykz7l6jj522r0g4rs69cnrh6c")))

(define-public crate-shapir-0.0.7 (c (n "shapir") (v "0.0.7") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "md5") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "06ni664rqq3af5d2v48snrzsxsmlkalyx4nmya1wr2y6njbd8na6")))

(define-public crate-shapir-0.0.8 (c (n "shapir") (v "0.0.8") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "md5") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1zmy3bmaxcrqci2j2bshzfxz9s63gr9m5j06k8jamlla5f3hjcvj")))

(define-public crate-shapir-0.1.0 (c (n "shapir") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "md5") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "00k8l3fqipr60vmngaagdn6ihr58s73973s45cwifvpb03qdpqxg")))

(define-public crate-shapir-0.1.1 (c (n "shapir") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "md5") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "00c3cdal2b3s6imbnkkli9jrm33bsvi53wxjiz14ncgq1glii12b")))

(define-public crate-shapir-0.1.2 (c (n "shapir") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "md5") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0pqcs53hyp779k0zwgf4n0gancjdaxm8sa4ql6gihx6agrb1hxmf")))

(define-public crate-shapir-0.1.3 (c (n "shapir") (v "0.1.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "md5") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "1vw3988jyl27nlpnada6vcigwawjscnnf0f3i06bxw2wl8hw0jwn")))

(define-public crate-shapir-0.2.0 (c (n "shapir") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "md5") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "189xvhravi2i38wrsphb7391bn1wbjzhklq4a2skxh6dadnrmjlr")))

(define-public crate-shapir-0.2.1 (c (n "shapir") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "email") (r "^0.0.13") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "md5") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "1nxxzidw2wrrp4fwyaly661m6alnmdlfn3mzp5l9jy3g5rdn9j4n")))

(define-public crate-shapir-0.3.0 (c (n "shapir") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "email") (r "^0.0.18") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "14vrjjzwf5pmrnw70578ap44l378diz527p04xv7h34xjijkfr3v")))

(define-public crate-shapir-0.3.1 (c (n "shapir") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "email") (r "^0.0.18") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "02in4id34iplryvsbaph7xk349hd2xbm3ssbb63664krdmp0l9hd")))

(define-public crate-shapir-0.4.0 (c (n "shapir") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "email") (r "^0.0.18") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "0g0q4mzwk1im3wdrixja5dx3lw7g1zaz9larx4qxjjgvw0w0gkxq")))

(define-public crate-shapir-0.5.0 (c (n "shapir") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "email") (r "^0.0.20") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "md5") (r "^0.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1ddl7dk8dmfrg3yny4yb2rv9d4rygqvq8nddgqrpyb4q1bjx602s")))

