(define-module (crates-io sh ap shape-convex) #:use-module (crates-io))

(define-public crate-shape-convex-0.0.0 (c (n "shape-convex") (v "0.0.0") (d (list (d (n "shape-core") (r "^0.1.12") (d #t) (k 0)))) (h "19lj5zcg2866gmgfyc8vw2s4h0zdapmq7dchh8a79c1fahgvdgnf") (f (quote (("default"))))))

(define-public crate-shape-convex-0.0.1 (c (n "shape-convex") (v "0.0.1") (d (list (d (n "shape-core") (r "^0.1.12") (d #t) (k 0)))) (h "17xn9h09blhsia5vyaw9q6rzcmczqfjj6dzsgbvi4qhnj72klfr0") (f (quote (("default"))))))

