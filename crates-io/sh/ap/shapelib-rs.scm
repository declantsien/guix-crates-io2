(define-module (crates-io sh ap shapelib-rs) #:use-module (crates-io))

(define-public crate-shapelib-rs-0.1.0 (c (n "shapelib-rs") (v "0.1.0") (d (list (d (n "asciiz") (r "^0.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)))) (h "17v3r4qciyd88vk92rzgsmg36xhknpr3mc9q21zrq63d7yf315rn") (y #t)))

(define-public crate-shapelib-rs-0.1.1 (c (n "shapelib-rs") (v "0.1.1") (d (list (d (n "asciiz") (r "^0.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)))) (h "0qqkmpc43i7ym6ll4in9h2ib5x220gi18b9y4p4nn2q0qp7azjw3") (y #t)))

(define-public crate-shapelib-rs-0.2.0 (c (n "shapelib-rs") (v "0.2.0") (d (list (d (n "asciiz") (r "^0.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)))) (h "0wc53pf5n4xmh4hfzyn8960r6aac26n2adsskns09vjfzvv7kwcv") (y #t)))

(define-public crate-shapelib-rs-0.2.1 (c (n "shapelib-rs") (v "0.2.1") (d (list (d (n "asciiz") (r "^0.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)))) (h "039nn94rzjxgbb7xy8xm1jlm0cbcqk536nn1wwq3cabi6s2lvgvf") (y #t)))

(define-public crate-shapelib-rs-0.2.2 (c (n "shapelib-rs") (v "0.2.2") (d (list (d (n "asciiz") (r "^0.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)))) (h "0dnb71vc1a8ihmdfsbwdyj82f0jx1cqhmwvwlzij7v8gybhdccad")))

