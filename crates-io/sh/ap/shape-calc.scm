(define-module (crates-io sh ap shape-calc) #:use-module (crates-io))

(define-public crate-shape-calc-0.1.0 (c (n "shape-calc") (v "0.1.0") (h "1jgvs8sqbrbgi4kqz6l2jydnjkkznazf3s8k6cxa34dr24z4l6py")))

(define-public crate-shape-calc-0.1.1 (c (n "shape-calc") (v "0.1.1") (h "0wpqfw5xyby7fc0a2whc9k151q8c2zmrkcjp0vxc1x30p4id15d5")))

(define-public crate-shape-calc-0.1.2 (c (n "shape-calc") (v "0.1.2") (h "16nqmrzm8gz0f092glrnp1hp5and4635nl51vdhpmwxgyqb1wlks")))

(define-public crate-shape-calc-0.1.3 (c (n "shape-calc") (v "0.1.3") (h "144ks9ni4n4jw4pfnrxydk0arzk0py2sll6swy8vc2yahrq3i0cs")))

