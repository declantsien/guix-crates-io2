(define-module (crates-io sh ap shape-svg) #:use-module (crates-io))

(define-public crate-shape-svg-0.0.0 (c (n "shape-svg") (v "0.0.0") (d (list (d (n "shape-core") (r "0.0.*") (d #t) (k 0)))) (h "1z8ykj683qbr3dl31w6bb6ag15ba98vnlwvpnylx63w1drlz9bsi") (f (quote (("default"))))))

(define-public crate-shape-svg-0.0.1 (c (n "shape-svg") (v "0.0.1") (d (list (d (n "shape-core") (r "0.0.*") (d #t) (k 0)))) (h "10apx184c3mwl83csq0nj68rss4pidcn9r3hf7afxn08pv5a0i5c") (f (quote (("default"))))))

(define-public crate-shape-svg-0.0.2 (c (n "shape-svg") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shape-core") (r "0.0.*") (d #t) (k 0)))) (h "0cx07fxvj0g6y2imslx3a81s1mhhzv3gldqykwv7z4lxiwy6z08p") (f (quote (("default"))))))

(define-public crate-shape-svg-0.0.3 (c (n "shape-svg") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "shape-core") (r "0.1.*") (d #t) (k 0)))) (h "1wwbmqw5n2hap2b5jhx869z6air9vm0vxw1jq39m7x7nd0z6pp65") (f (quote (("default"))))))

(define-public crate-shape-svg-0.0.4 (c (n "shape-svg") (v "0.0.4") (d (list (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.159") (d #t) (k 0)) (d (n "shape-core") (r "0.1.*") (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)))) (h "10hf1z8k1rjk9kgm7y4lwdmivqphbdnaqaazbp7kh1w53wvn36r8") (f (quote (("default"))))))

(define-public crate-shape-svg-0.0.5 (c (n "shape-svg") (v "0.0.5") (d (list (d (n "shape-core") (r "^0.1.10") (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)))) (h "10vv6hrxjfrmi2cbyz5jip575q9qk9awk7jlk4cymlmjb99ypy8w") (f (quote (("default"))))))

(define-public crate-shape-svg-0.0.6 (c (n "shape-svg") (v "0.0.6") (d (list (d (n "shape-core") (r "^0.1.12") (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)))) (h "093rg9y32b1mmabvqf8yzphxsrnd1d9cr154mlfy6qa0zkcgqn8j") (f (quote (("default"))))))

(define-public crate-shape-svg-0.0.7 (c (n "shape-svg") (v "0.0.7") (d (list (d (n "shape-core") (r "^0.1.12") (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)))) (h "03qkx5zf5dzk3bpxrbb30rg7yb8bm7yx56256jihicwgq5isjcp0") (f (quote (("default"))))))

(define-public crate-shape-svg-0.0.8 (c (n "shape-svg") (v "0.0.8") (d (list (d (n "shape-core") (r "0.1.*") (d #t) (k 0)) (d (n "svg") (r "^0.14.0") (d #t) (k 0)))) (h "08p1mbpnfpqdwvxydd30mfkld02n0sqzinjqhlclhj3bcw2vjp5h") (f (quote (("default"))))))

(define-public crate-shape-svg-0.0.9 (c (n "shape-svg") (v "0.0.9") (d (list (d (n "shape-core") (r "0.1.*") (d #t) (k 0)) (d (n "svg") (r "^0.14.0") (d #t) (k 0)))) (h "1k51dvk2n5sc49d6g46jy1bix1d2rzcxx27ypqy7ay13b05234i3") (f (quote (("default"))))))

