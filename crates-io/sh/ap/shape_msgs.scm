(define-module (crates-io sh ap shape_msgs) #:use-module (crates-io))

(define-public crate-shape_msgs-4.2.3 (c (n "shape_msgs") (v "4.2.3") (d (list (d (n "geometry_msgs") (r "^4.2.3") (d #t) (k 0)) (d (n "rosidl_runtime_rs") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "std_msgs") (r "^4.2.3") (d #t) (k 0)))) (h "0ink8i31yqli31wzbqqg9vkpnza2mq6bblhphg05rdphk11ncxbb") (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde-big-array" "rosidl_runtime_rs/serde" "std_msgs/serde" "geometry_msgs/serde"))))))

