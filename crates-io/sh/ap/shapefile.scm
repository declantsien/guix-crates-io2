(define-module (crates-io sh ap shapefile) #:use-module (crates-io))

(define-public crate-shapefile-0.0.1 (c (n "shapefile") (v "0.0.1") (h "1gd2g3yml29k9www9q9fcggv61rqil74bwlgwwnlikbi06hmdqvf")))

(define-public crate-shapefile-0.0.2 (c (n "shapefile") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)))) (h "1ysc9yp34qsj559bf9rkzvnfjfqlbzffry5ibfywr86nmphc5iih")))

(define-public crate-shapefile-0.0.3 (c (n "shapefile") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.0.2") (d #t) (k 0)))) (h "0f08p9hv2vcahqndbsgkj6zwjh9v5bai8s50174a3mgbw42vq379")))

(define-public crate-shapefile-0.0.4 (c (n "shapefile") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.0.3") (d #t) (k 0)) (d (n "geo-types") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "1i16xvd7x0vck1szpvnvlvx0g5fphs0y4l4fwgzhc12x3p7dvkmx")))

(define-public crate-shapefile-0.0.5 (c (n "shapefile") (v "0.0.5") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.0.4") (d #t) (k 0)) (d (n "geo-types") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "16l4yy8229rw4j617pfqrj9wn94y9f88p1wjf7wdcvxwipwzdy8n")))

(define-public crate-shapefile-0.1.0 (c (n "shapefile") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.0.4") (d #t) (k 0)) (d (n "geo-types") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "1bhpmlmadypipzqq2p59kws44x4p298542hzk8291qi2smxk2ljr")))

(define-public crate-shapefile-0.1.1 (c (n "shapefile") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.0.4") (d #t) (k 0)) (d (n "geo-types") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "10m4vczfki0hrvfby7gzpfbr3dgcvhqsiakny4z1yrl5yzd72lya")))

(define-public crate-shapefile-0.2.0 (c (n "shapefile") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.0.4") (d #t) (k 0)) (d (n "geo-types") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "0k4lrrapxzwqmr6a33cy5pjl0xx633mk84isy1nabnqhjmsv0vdn")))

(define-public crate-shapefile-0.2.1 (c (n "shapefile") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.0.4") (d #t) (k 0)) (d (n "geo-types") (r ">=0.4.0, <0.7.0") (o #t) (d #t) (k 0)))) (h "1vsz8lnbakivp93cyph6qfrqi8d2g5ksizp59dd1ap3ifb0cbxhn")))

(define-public crate-shapefile-0.2.2 (c (n "shapefile") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.0.4") (d #t) (k 0)) (d (n "geo-types") (r ">=0.4.0, <0.8.0") (o #t) (d #t) (k 0)))) (h "0vwdjdw23l9ik3crdq3q073nnkhyvlngdrbvjm1gyx1jiap7r8vj")))

(define-public crate-shapefile-0.3.0 (c (n "shapefile") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.2.0") (d #t) (k 0)) (d (n "geo-types") (r ">=0.4.0, <0.8.0") (o #t) (d #t) (k 0)))) (h "139laq7337pjha4kay49r5jch6lb87pv25l988ian504si6br579")))

(define-public crate-shapefile-0.4.0 (c (n "shapefile") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.3.0") (d #t) (k 0)) (d (n "geo-types") (r ">=0.4.0, <0.8.0") (o #t) (d #t) (k 0)))) (h "065ylhjcl9i85agfq9fff590v01pa6d0x8j0ml1acw1v1ncdqxwg")))

(define-public crate-shapefile-0.5.0 (c (n "shapefile") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.4.0") (d #t) (k 0)) (d (n "geo-types") (r ">=0.4.0, <0.8.0") (o #t) (d #t) (k 0)))) (h "1bs85jqqdc0vy7xvsh8w6l5yh92yl5m91x3fjlhbbrq4cxsg7rcn")))

(define-public crate-shapefile-0.6.0 (c (n "shapefile") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.5.0") (d #t) (k 0)) (d (n "geo-types") (r ">=0.4.0, <0.8.0") (o #t) (d #t) (k 0)))) (h "0g05nc7fiyr7yi4kx4n6804rhn32p70x56kp0afha0r5jcp4gmbr")))

