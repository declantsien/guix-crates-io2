(define-module (crates-io sh ap shape-contour) #:use-module (crates-io))

(define-public crate-shape-contour-0.2.0 (c (n "shape-contour") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "shapelib-rs") (r "^0.2") (d #t) (k 0)))) (h "0178fn69j11z5r586p6plgid922nf0wlv3xqwn7wwsh98fpzkg7d") (y #t)))

(define-public crate-shape-contour-0.2.1 (c (n "shape-contour") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "shapelib-rs") (r "^0.2") (d #t) (k 0)))) (h "0fdc1qhb8dns8a5h6kpiy4w4qcp6l57cjlxf8davvznlr70c1cg6") (y #t)))

(define-public crate-shape-contour-0.2.2 (c (n "shape-contour") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "shapelib-rs") (r "^0.2") (d #t) (k 0)))) (h "0zjfs9waiarhgc0szagfi0zwsladsd15n9pcm8zd4cy1sk92gvbz")))

