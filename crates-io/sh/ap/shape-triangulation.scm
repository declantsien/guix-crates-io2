(define-module (crates-io sh ap shape-triangulation) #:use-module (crates-io))

(define-public crate-shape-triangulation-0.0.0 (c (n "shape-triangulation") (v "0.0.0") (d (list (d (n "shape-core") (r "^0.1.3") (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1v3ikr18m84zkp3gqa8ppzvzdg1aj5fknfmzl2jhanp7091ddjzb") (f (quote (("default"))))))

(define-public crate-shape-triangulation-0.1.0 (c (n "shape-triangulation") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "delaunator") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "shape-core") (r "^0.1.8") (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (o #t) (d #t) (k 0)))) (h "0flfbrwcqp5rbl3hr7bdxplvzpqcp2sadrhij53w6vfscgfz4b1x") (f (quote (("default"))))))

(define-public crate-shape-triangulation-0.1.1 (c (n "shape-triangulation") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "delaunator") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "shape-core") (r "^0.1.12") (d #t) (k 0)) (d (n "shape-svg") (r "^0.0.7") (o #t) (d #t) (k 0)))) (h "1fklv2miif1fjrvn8cbhw9jd1g5nnql57bsjlv3xs72krx6g3f9v") (f (quote (("default"))))))

