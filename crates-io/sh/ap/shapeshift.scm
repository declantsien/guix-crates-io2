(define-module (crates-io sh ap shapeshift) #:use-module (crates-io))

(define-public crate-shapeshift-0.1.0 (c (n "shapeshift") (v "0.1.0") (d (list (d (n "clap") (r "^2.22") (d #t) (k 0)) (d (n "hyper") (r "^0.10.9") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "05vynay6llx939pq4p89snkjmraaz0283bwk1qr12y45cv84mjwj")))

