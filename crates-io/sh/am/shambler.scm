(define-module (crates-io sh am shambler) #:use-module (crates-io))

(define-public crate-shambler-0.2.0 (c (n "shambler") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.32.5") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "shalrath") (r "^0.2.6") (d #t) (k 0)) (d (n "usage") (r "^1.4.0") (f (quote ("rayon"))) (d #t) (k 0)))) (h "1clg9jwnrcxvwyqqxs0lj2j1032x5aqvvbsx0nqnnmd6hw6ihyn5")))

