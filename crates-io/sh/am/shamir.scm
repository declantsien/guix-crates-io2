(define-module (crates-io sh am shamir) #:use-module (crates-io))

(define-public crate-shamir-1.0.0 (c (n "shamir") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "05dl25qqh4hvfp53pplzrwpw8azgdahh9h8q26ljk56wv8mjd18x")))

(define-public crate-shamir-1.0.1 (c (n "shamir") (v "1.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1clwr0kdcly1fmwy42i7hckdfvkhvrzhkzh8xniisq20vsr2x1la")))

(define-public crate-shamir-1.0.2 (c (n "shamir") (v "1.0.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1mwc2p681rrbibsbrhg7ly9idv1cjp71sbcvs015p4rvdzzx7506")))

(define-public crate-shamir-2.0.0 (c (n "shamir") (v "2.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "05z3vx89xrv6qgsybqfx65jmyf9j3js6zm5mb2gzj3pcrh54wczk")))

