(define-module (crates-io sh am shamir_secret_sharing) #:use-module (crates-io))

(define-public crate-shamir_secret_sharing-0.1.0 (c (n "shamir_secret_sharing") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "15l85hryw8l1js9xnpbfwqa9cqs05fv5iy9lkf9r344c9crzjgh4")))

(define-public crate-shamir_secret_sharing-0.1.1 (c (n "shamir_secret_sharing") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "14k5zdj7xrcm4dz6dffrf813rkwv2690af642c9sgas9xcdnnvh8")))

