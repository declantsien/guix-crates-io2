(define-module (crates-io sh am shamir_file_secret_sharing) #:use-module (crates-io))

(define-public crate-shamir_file_secret_sharing-1.0.0 (c (n "shamir_file_secret_sharing") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0m0n5vpa5z482196x9gm57ayi87jzq6wpnzsw1hf2bxsl7xdv175") (r "1.60")))

