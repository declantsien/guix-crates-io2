(define-module (crates-io sh am shame) #:use-module (crates-io))

(define-public crate-shame-0.0.0 (c (n "shame") (v "0.0.0") (d (list (d (n "derive-where") (r "^1") (d #t) (k 0)) (d (n "impls") (r "^1") (d #t) (k 2)) (d (n "pub-fields") (r "^0.1") (d #t) (k 0)))) (h "0ylvbxmwrcfp0218mfrxpfby7w7l7gvj0waqk1aqimwfl3jg8lax")))

(define-public crate-shame-0.0.1 (c (n "shame") (v "0.0.1") (d (list (d (n "derive-where") (r "^1") (d #t) (k 0)) (d (n "derive_everything") (r "^0.1") (d #t) (k 0)) (d (n "impls") (r "^1") (d #t) (k 2)) (d (n "pub-fields") (r "^0.1") (d #t) (k 0)))) (h "1466bkxff62av2p0d6wmcqc3ip2dpvki8x76kqprjmii1a56cnp3")))

