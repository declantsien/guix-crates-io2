(define-module (crates-io sh uf shuffle) #:use-module (crates-io))

(define-public crate-shuffle-0.1.0 (c (n "shuffle") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "13baz28dprpb3hn54jkpjmw70gmy1hvv924v6v9cssr51b6gq1jb")))

(define-public crate-shuffle-0.1.1 (c (n "shuffle") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "01p70p9jwzi9lc611y26kcd9dn595nlngjvrdd547r6s2yg7ynkl")))

(define-public crate-shuffle-0.1.2 (c (n "shuffle") (v "0.1.2") (d (list (d (n "bitvec") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1lb6bg4g8cgqiaczvvz7g92bgy7p5nl0a30rcy3frlc18q3h3lcq")))

(define-public crate-shuffle-0.1.3 (c (n "shuffle") (v "0.1.3") (d (list (d (n "bitvec") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0xhm3bmxywbr27qvj4b4naykd9q0h7frc71z3zwhcc5gvmhi6np3")))

(define-public crate-shuffle-0.1.6 (c (n "shuffle") (v "0.1.6") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0g7nzy5s5s7qgcy1p983g0fvqp3vxgm1kxjbb4hj1jvp4xzg9zap")))

(define-public crate-shuffle-0.1.7 (c (n "shuffle") (v "0.1.7") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1cjfms6cd8y720nbq35f8s4pkq4h5znf0b0rlqi6wwp0df0bxv1g")))

