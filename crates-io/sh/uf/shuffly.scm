(define-module (crates-io sh uf shuffly) #:use-module (crates-io))

(define-public crate-shuffly-0.1.0 (c (n "shuffly") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1hsb65imhinwmvhb20zp1xqcgl1r21klmiyf0y6f4l7f299ka7wp")))

(define-public crate-shuffly-0.1.1 (c (n "shuffly") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0w35p9wb10ahk7av886blxqk11ammfawzhb0ygbnggxlgwmr5mmn")))

(define-public crate-shuffly-0.1.2 (c (n "shuffly") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "00rqbamrsrflykk48wpzlhl9m2s7w5s3vxh8bwspmrm7n3pl8bv1")))

(define-public crate-shuffly-0.1.3 (c (n "shuffly") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0r3a1h8d0ypsh2cx86w8sh3hiv38109as7zalix8pqzp0s5l3pif")))

