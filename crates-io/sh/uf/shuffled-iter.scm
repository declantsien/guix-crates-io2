(define-module (crates-io sh uf shuffled-iter) #:use-module (crates-io))

(define-public crate-shuffled-iter-0.1.0 (c (n "shuffled-iter") (v "0.1.0") (d (list (d (n "bit-vec") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1zbyys9xqnid5cbh77x9yacfn4kr2jw34sbv017nn94k4qai1iak")))

(define-public crate-shuffled-iter-0.2.0 (c (n "shuffled-iter") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "14izvzrmm9dq4jjll96j4ia5an19if92m2n74jg99j12hm8mczax")))

