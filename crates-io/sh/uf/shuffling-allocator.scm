(define-module (crates-io sh uf shuffling-allocator) #:use-module (crates-io))

(define-public crate-shuffling-allocator-1.0.0 (c (n "shuffling-allocator") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "02rk46njg8yswbvp1v5r35cvkq4ny0nbdsgzhimccrdkpyp78a5b")))

(define-public crate-shuffling-allocator-1.1.0 (c (n "shuffling-allocator") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1pibf7db5y21ly81siby0s9pkxpgxplivdpr7h5crgs8ngbvnjkn")))

(define-public crate-shuffling-allocator-1.1.1 (c (n "shuffling-allocator") (v "1.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("synchapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1y47z5kzws0i6ip0qpm3aakmxqpbzba9j33lmaznxlbk8d2hm344")))

(define-public crate-shuffling-allocator-1.1.2 (c (n "shuffling-allocator") (v "1.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("synchapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "15n4dx86xvldq0p9vl5fbfc3k8g2rg2nzcjadw0dk2c4m5zrgsaf")))

