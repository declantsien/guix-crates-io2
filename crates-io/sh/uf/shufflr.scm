(define-module (crates-io sh uf shufflr) #:use-module (crates-io))

(define-public crate-shufflr-0.1.0 (c (n "shufflr") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0vpqjmsa9xsw16vnxx6fzsnrss90r7xx4b5k825d7jklzvdma62j")))

(define-public crate-shufflr-0.1.1 (c (n "shufflr") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1n25nh53p27k2iqr22w3x7b5sa7cdxcfwvlyvca9ppdik4bzfamq")))

(define-public crate-shufflr-0.1.2 (c (n "shufflr") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0l3hb372pi1a1mcxqhar1rngcwqg49fbsnpx282fj4b8b9dg2rm4")))

