(define-module (crates-io sh t- sht-colour) #:use-module (crates-io))

(define-public crate-sht-colour-0.1.0 (c (n "sht-colour") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1f5znz056921y6s7ihmxg6m8ww2ajy3c2dnr3n1d51lq3ia28zy7")))

