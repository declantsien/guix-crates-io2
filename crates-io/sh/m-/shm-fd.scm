(define-module (crates-io sh m- shm-fd) #:use-module (crates-io))

(define-public crate-shm-fd-0.2.0 (c (n "shm-fd") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.109") (d #t) (k 0)) (d (n "memfile") (r "^0.2.1") (d #t) (k 0)))) (h "0l0nxngvicq5d2kdzmrwa7v96fvkgpjiyma6f14syqphf0sfia0n")))

(define-public crate-shm-fd-0.4.0 (c (n "shm-fd") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.109") (o #t) (d #t) (k 0)) (d (n "memfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1yggsqx72rn4v8v9i6x3b1rs0x57l0bi8mmmm7dfmpzc9hmnbagc") (f (quote (("std"))))))

(define-public crate-shm-fd-0.5.0 (c (n "shm-fd") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.109") (o #t) (d #t) (k 0)) (d (n "memfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0961hdpbdr5q2c7cz02p6zvm03l44if8lnysz253rgq3nhcjca2b") (f (quote (("std"))))))

