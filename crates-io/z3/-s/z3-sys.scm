(define-module (crates-io z3 -s z3-sys) #:use-module (crates-io))

(define-public crate-z3-sys-0.1.0 (c (n "z3-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "va_list") (r "^0.0.1") (d #t) (k 0)))) (h "164v430777qfhz9qvi9vb9mqki70q5jyf306hmajpyzb7ff3db89")))

(define-public crate-z3-sys-0.2.0 (c (n "z3-sys") (v "0.2.0") (h "1dfqyx9g2jjkal5kmh9b2hl89p062c5q4xy5vsg9mr2yhnx0md76")))

(define-public crate-z3-sys-0.3.0 (c (n "z3-sys") (v "0.3.0") (h "0gcngw03wjqmjv807618b152l96hqqzlifipnw2cvns243987dmr")))

(define-public crate-z3-sys-0.4.0 (c (n "z3-sys") (v "0.4.0") (h "00255l78bkzxz0l6y6z0l9qg366d39j91mnb1al6z71l28vr1zqx")))

(define-public crate-z3-sys-0.5.0 (c (n "z3-sys") (v "0.5.0") (h "0cfy6y0zy3g6576fph6c48js3n1cmkgf98rfpw7n5hmb3v1v8mxf")))

(define-public crate-z3-sys-0.6.0 (c (n "z3-sys") (v "0.6.0") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0cr8dhs7f42vi9810989amasw2b15lv8bpr5i674wjfib52rg1r4") (f (quote (("static-link-z3" "cmake")))) (l "z3")))

(define-public crate-z3-sys-0.6.1 (c (n "z3-sys") (v "0.6.1") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1bhimqgb7amcyg16p4rhnf562l2l3ql52qdqpwnx0m6bzahxh0j0") (f (quote (("static-link-z3" "cmake")))) (l "z3")))

(define-public crate-z3-sys-0.6.2 (c (n "z3-sys") (v "0.6.2") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "08as34ky9b9n9wyyjik3hgl991983d11i4lwsvbnvf4fmspkr1p1") (f (quote (("static-link-z3" "cmake")))) (l "z3")))

(define-public crate-z3-sys-0.6.3 (c (n "z3-sys") (v "0.6.3") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1q0xh5iqar5nvwyww31inz6yj7zrj7yw6h5lzx0kx4ylzfjqp8dg") (f (quote (("static-link-z3" "cmake")))) (l "z3")))

(define-public crate-z3-sys-0.7.0 (c (n "z3-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0pz7kkmb9qmri1dglak5043wa2rskswpxkmjq4lbc6ihp64jysx0") (f (quote (("static-link-z3" "cmake")))) (l "z3")))

(define-public crate-z3-sys-0.7.1 (c (n "z3-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "19l63517glil9wj5gp0gxmmd7hyn5g5nc02j9jcz2jxyb2ydr0hw") (f (quote (("static-link-z3" "cmake")))) (l "z3")))

(define-public crate-z3-sys-0.8.0 (c (n "z3-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.66.0") (f (quote ("runtime"))) (k 1)) (d (n "cmake") (r "^0.1.49") (o #t) (d #t) (k 1)))) (h "106d86abhw8yc8g8r8ahswgzd5p94i1cz33gwdv4csx2npwb0cg8") (f (quote (("static-link-z3" "cmake")))) (l "z3")))

(define-public crate-z3-sys-0.8.1 (c (n "z3-sys") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.66.0") (f (quote ("runtime"))) (k 1)) (d (n "cmake") (r "^0.1.49") (o #t) (d #t) (k 1)))) (h "1c7l61a2zvsy2l5gyagaahi5d0kad3ab0jag80mz9qqdpkyp1kyp") (f (quote (("static-link-z3" "cmake")))) (l "z3")))

