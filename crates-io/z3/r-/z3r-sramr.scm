(define-module (crates-io z3 r- z3r-sramr) #:use-module (crates-io))

(define-public crate-z3r-sramr-0.1.1 (c (n "z3r-sramr") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1lvyjjhxnrqf7av9hax9q0gify8vpyp21akwk0x506hs7l712cbz")))

(define-public crate-z3r-sramr-0.2.0 (c (n "z3r-sramr") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "17lqc53bw22a8ny84wq5vc81xqvwbchv4vba21dm2gqbjypi0pvb")))

(define-public crate-z3r-sramr-0.2.3 (c (n "z3r-sramr") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0w390nx7nfn3iw92xwn0820212vyczkay36asvq12jfza9l2swrp")))

