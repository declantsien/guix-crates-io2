(define-module (crates-io z3 tr z3tracer) #:use-module (crates-io))

(define-public crate-z3tracer-0.1.0 (c (n "z3tracer") (v "0.1.0") (d (list (d (n "smt2parser") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "1bl98ihhfy0npmr9g5jybkd7lpfjfxz4701j6qxymmavw2yddyi4")))

(define-public crate-z3tracer-0.2.0 (c (n "z3tracer") (v "0.2.0") (d (list (d (n "smt2parser") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "1qzh3zkan2aqvnhp202gipsl1w0621n6vm3cc75wzrmhlxnvz3fz")))

(define-public crate-z3tracer-0.3.0 (c (n "z3tracer") (v "0.3.0") (d (list (d (n "smt2parser") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "0i2l1r0kwjz2s6p0xgaazlbbxhzd3k7gjm5m1l2iid5xc9ac0ak6")))

(define-public crate-z3tracer-0.3.1 (c (n "z3tracer") (v "0.3.1") (d (list (d (n "smt2parser") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "0fnkl8skb4x2gywis8c65rczdx16fc2m1s6dv0bknrhaqcjms20q")))

(define-public crate-z3tracer-0.3.2 (c (n "z3tracer") (v "0.3.2") (d (list (d (n "smt2parser") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "00w9hzwb7dsaf1vci5kw137w5r7sbxyxw8ld6mb5cnf46rmd7r9q")))

(define-public crate-z3tracer-0.4.0 (c (n "z3tracer") (v "0.4.0") (d (list (d (n "smt2parser") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1cqplqxp6x7v15r39irswv0n6q8rhvsjmqr8m62mrc86pxr3sqfv")))

(define-public crate-z3tracer-0.5.0 (c (n "z3tracer") (v "0.5.0") (d (list (d (n "smt2parser") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1vvbncwblnhc34masljib40mcasy837r0gl3gggs1zyavbblfkdd")))

(define-public crate-z3tracer-0.6.0 (c (n "z3tracer") (v "0.6.0") (d (list (d (n "smt2parser") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0gbcppdgvg3c8ng4s6b3n5s07qvry9pqxys77797p5r8vdyb7di8")))

(define-public crate-z3tracer-0.7.0 (c (n "z3tracer") (v "0.7.0") (d (list (d (n "smt2parser") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1impz82w8cnrr25f8ydrjyg1zh0nyi0ajqasc1il0hzjwc7rmlzh")))

(define-public crate-z3tracer-0.8.0 (c (n "z3tracer") (v "0.8.0") (d (list (d (n "smt2parser") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1q5nj0w8yasfc8sc33nhi40mmvx6mbmwly6d02agl3rh9cyjzdap")))

(define-public crate-z3tracer-0.9.0 (c (n "z3tracer") (v "0.9.0") (d (list (d (n "smt2parser") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1bjwgjgy4k3gnfhkrxidigrzxm0j3jkgp9jxrhmyx72hw5s28m3m")))

(define-public crate-z3tracer-0.10.0 (c (n "z3tracer") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "smt2parser") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1591n4c7x0rhi5h6im2akg4l1igcpxiqjdhbbs6w1n0z039qz6zw")))

(define-public crate-z3tracer-0.11.0 (c (n "z3tracer") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "multiset") (r "^0.0.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "smt2parser") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1mp36ngvynsnr130maiymwwfmkm3zy6ikcvxw5cb5l110djanp4q") (f (quote (("report" "plotters" "petgraph" "multiset"))))))

(define-public crate-z3tracer-0.11.1 (c (n "z3tracer") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "multiset") (r "^0.0.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "smt2parser") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1fyja0qdp3ad5qyzgkxqxcb01567a57b4fpx0vagjyjsxg0zxhbb") (f (quote (("report" "plotters" "petgraph" "multiset"))))))

(define-public crate-z3tracer-0.11.2 (c (n "z3tracer") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "multiset") (r "^0.0.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "smt2parser") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "043wlzjnn1vrqj6crvkxhij1ql2m8vq484x3nfrvcf96qsxdk2fy") (f (quote (("report" "plotters" "petgraph" "multiset"))))))

