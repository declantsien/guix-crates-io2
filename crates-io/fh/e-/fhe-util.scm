(define-module (crates-io fh e- fhe-util) #:use-module (crates-io))

(define-public crate-fhe-util-0.1.0-beta.0 (c (n "fhe-util") (v "0.1.0-beta.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.8.1") (f (quote ("prime"))) (d #t) (k 0) (p "num-bigint-dig")) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zrnq074w08jxib7g7m7hfya37l6s09nyka5xbkvcm7amwbkh1mz") (y #t)))

(define-public crate-fhe-util-0.1.0-beta.1 (c (n "fhe-util") (v "0.1.0-beta.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.8.1") (f (quote ("prime"))) (d #t) (k 0) (p "num-bigint-dig")) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1daij9sjk98p6ybs10gmh74va3zpfxps5dh7z03r3jahq1na4n3a") (y #t)))

(define-public crate-fhe-util-0.1.0-beta.3 (c (n "fhe-util") (v "0.1.0-beta.3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.8.1") (f (quote ("prime"))) (d #t) (k 0) (p "num-bigint-dig")) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "185sd7j6cg7057qiws7399d0zl9xjy14f164hhk167qzp6d80bxb") (y #t)))

(define-public crate-fhe-util-0.1.0-beta.4 (c (n "fhe-util") (v "0.1.0-beta.4") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8.1") (f (quote ("prime"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18831qr06i3nd2gqd5br3nwfbqpidvym0nbmbd134r107jl9d8d8") (y #t)))

(define-public crate-fhe-util-0.1.0-beta.5 (c (n "fhe-util") (v "0.1.0-beta.5") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8.4") (f (quote ("prime"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1h421iai3mx1lgzz9kdskg4bnqvc6wawj13i9v7hmj2qvcpqlzqq") (y #t)))

(define-public crate-fhe-util-0.1.0-beta.6 (c (n "fhe-util") (v "0.1.0-beta.6") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8.4") (f (quote ("prime"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04hbclh4v4whgf6bvydvkigac8szpm3h00fbjfl2jc3448hn5d81") (y #t)))

(define-public crate-fhe-util-0.1.0-beta.7 (c (n "fhe-util") (v "0.1.0-beta.7") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8.4") (f (quote ("prime"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19ng9k4s5jrf5dqlz9rcc7x7k5ikmz2z999cmjgi2y2gapgkdd0j") (y #t)))

(define-public crate-fhe-util-0.1.0-beta.8 (c (n "fhe-util") (v "0.1.0-beta.8") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8.4") (f (quote ("prime"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qim9d0glcfc9g806bcd8dssm09abp10p89fj7pmw7wkfq8akfsf") (r "1.73")))

