(define-module (crates-io fh e- fhe-traits) #:use-module (crates-io))

(define-public crate-fhe-traits-0.1.0-beta.0 (c (n "fhe-traits") (v "0.1.0-beta.0") (h "07d1j8rwanh9ryss71fp9fgahdrsnj4ylr603484qyla82yns89x") (y #t)))

(define-public crate-fhe-traits-0.1.0-beta.1 (c (n "fhe-traits") (v "0.1.0-beta.1") (h "0dr0bwffv1gj8mhgz9n7459zdrdb19vdvw7wdfhvq9pycnv0f383") (y #t)))

(define-public crate-fhe-traits-0.1.0-beta.3 (c (n "fhe-traits") (v "0.1.0-beta.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hwsjw4613npvjy7w5pcnfq5hm9mljrv8lsm2wcaa0drm48fndmf") (y #t)))

(define-public crate-fhe-traits-0.1.0-beta.4 (c (n "fhe-traits") (v "0.1.0-beta.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pklmmxsv74644rf7048adff6xil9ig8ihp7k723akv0ydsjr7rc") (y #t)))

(define-public crate-fhe-traits-0.1.0-beta.5 (c (n "fhe-traits") (v "0.1.0-beta.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01hc051q3sdnbljglbfk5wgkinxkn2pladw3aacmn3ykvqk7yg6d") (y #t)))

(define-public crate-fhe-traits-0.1.0-beta.6 (c (n "fhe-traits") (v "0.1.0-beta.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1pby9xbzfwkgrvbkmyyix5l5ldakfsjq97mh3xvxwshsmc8x0gh4") (y #t)))

(define-public crate-fhe-traits-0.1.0-beta.7 (c (n "fhe-traits") (v "0.1.0-beta.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18pqy56sbg6vjgrz3kcbcl3abk1bfzwwzvg1mjzy4fhm373ldk33") (y #t)))

(define-public crate-fhe-traits-0.1.0-beta.8 (c (n "fhe-traits") (v "0.1.0-beta.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03jz2wyv9vlihh0xymf17bj1j5lrvsla5i37i5vq1c9r8pcy0395") (r "1.73")))

