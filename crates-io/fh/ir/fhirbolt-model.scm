(define-module (crates-io fh ir fhirbolt-model) #:use-module (crates-io))

(define-public crate-fhirbolt-model-0.1.0 (c (n "fhirbolt-model") (v "0.1.0") (d (list (d (n "fhirbolt-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("arbitrary_precision"))) (d #t) (k 0)))) (h "09c2kbgy9lqpvziwcr2knlh59jk4h6n0dq9g1mgigylqr6jd03zn") (f (quote (("r4b") ("r4"))))))

(define-public crate-fhirbolt-model-0.1.1 (c (n "fhirbolt-model") (v "0.1.1") (d (list (d (n "fhirbolt-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("arbitrary_precision"))) (d #t) (k 0)))) (h "0b2iygdl86mid1q4m2yj5izhlqhpnmd4bff5f6dg3mlfk10v0n1i") (f (quote (("r4b") ("r4"))))))

(define-public crate-fhirbolt-model-0.1.2 (c (n "fhirbolt-model") (v "0.1.2") (d (list (d (n "fhirbolt-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("arbitrary_precision"))) (d #t) (k 0)))) (h "1sdndmaksylm9a9vd4l9q2mqa86pmn35cvh2dvh6xaq7rqsajjrx") (f (quote (("r4b") ("r4"))))))

(define-public crate-fhirbolt-model-0.1.3 (c (n "fhirbolt-model") (v "0.1.3") (d (list (d (n "fhirbolt-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("arbitrary_precision"))) (d #t) (k 0)))) (h "1qn3x1rb3r92nb6rfzipqqgws9ls9j2as0006p16az86k4vgnnb7") (f (quote (("r4b") ("r4"))))))

(define-public crate-fhirbolt-model-0.2.0 (c (n "fhirbolt-model") (v "0.2.0") (d (list (d (n "fhirbolt-shared") (r "^0.2.0") (d #t) (k 0)))) (h "1k6ym6i6fv422ayhadi44mq1f1rq87nja7cw2sxddcyil24a46v6") (f (quote (("r4b") ("r4"))))))

(define-public crate-fhirbolt-model-0.2.1 (c (n "fhirbolt-model") (v "0.2.1") (d (list (d (n "fhirbolt-shared") (r "^0.2.0") (d #t) (k 0)))) (h "06arlqn0irlc6isjb53sps6clxqx1xcqw0jlq6d9hd0wk857wxi0") (f (quote (("r4b") ("r4"))))))

(define-public crate-fhirbolt-model-0.2.2 (c (n "fhirbolt-model") (v "0.2.2") (d (list (d (n "fhirbolt-shared") (r "^0.2.0") (d #t) (k 0)))) (h "0hs84fzl5pdhzg73hgxfgkvqgp5nxy588wy7a92xp9dyxw5l2smm") (f (quote (("r4b") ("r4"))))))

(define-public crate-fhirbolt-model-0.3.0 (c (n "fhirbolt-model") (v "0.3.0") (d (list (d (n "fhirbolt-shared") (r "^0.3.0") (d #t) (k 0)))) (h "0n3zyib4f9pbfxgj3mak9a8nlyr5cgf4a9dgj5sy45ldrfamylr9") (f (quote (("r5") ("r4b") ("r4"))))))

(define-public crate-fhirbolt-model-0.4.0 (c (n "fhirbolt-model") (v "0.4.0") (d (list (d (n "fhirbolt-shared") (r "^0.4.0") (d #t) (k 0)))) (h "0nzdgcvzavn0sf0q8g6bbfy23yzd5bdivl6nbnna71520q3s51x2") (f (quote (("r5") ("r4b") ("r4"))))))

