(define-module (crates-io fh ir fhir-resource-r5) #:use-module (crates-io))

(define-public crate-fhir-resource-r5-0.0.2 (c (n "fhir-resource-r5") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fhir-rs") (r "^0.0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1ihm75m9h0qwl36ys6msg05d7bhvbjrv2bv1ywxa84dl4y5vifj9")))

(define-public crate-fhir-resource-r5-0.0.3 (c (n "fhir-resource-r5") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fhir-rs") (r "^0.0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "02lafkz8q4hkizxpnjpmzdx6wyf4bc2w9iraqnc2n63nhvbkzz7f")))

