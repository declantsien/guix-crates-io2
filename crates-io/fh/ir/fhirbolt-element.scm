(define-module (crates-io fh ir fhirbolt-element) #:use-module (crates-io))

(define-public crate-fhirbolt-element-0.2.0 (c (n "fhirbolt-element") (v "0.2.0") (d (list (d (n "fhirbolt-shared") (r "^0.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)))) (h "1w531605m3krsnv45zp2miw40b4446bzpznfn4xhan6f0rh3kl54")))

(define-public crate-fhirbolt-element-0.2.1 (c (n "fhirbolt-element") (v "0.2.1") (d (list (d (n "fhirbolt-shared") (r "^0.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)))) (h "1ahmv8pjnwwj38020qqdrm5gqps1i59v8zkci06b32iqzmhnj3ip")))

(define-public crate-fhirbolt-element-0.2.2 (c (n "fhirbolt-element") (v "0.2.2") (d (list (d (n "fhirbolt-shared") (r "^0.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)))) (h "0ks1mz09q7s84x1kfblhw9bna14nyizi1sbvs7m9g5j6ddivslyq")))

(define-public crate-fhirbolt-element-0.3.0 (c (n "fhirbolt-element") (v "0.3.0") (d (list (d (n "fhirbolt-shared") (r "^0.3.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)))) (h "1vcj70qhs8bnv41bqf89j2521i64p1r52rdndfk79g9zmcc7b6i5")))

(define-public crate-fhirbolt-element-0.4.0 (c (n "fhirbolt-element") (v "0.4.0") (d (list (d (n "fhirbolt-shared") (r "^0.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)))) (h "1dyj4d0l8c9zv89dsnyf42qsbz380wirgg7g107rm6if82n8l6r6")))

