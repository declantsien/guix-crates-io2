(define-module (crates-io fh ir fhirbolt-shared) #:use-module (crates-io))

(define-public crate-fhirbolt-shared-0.1.0 (c (n "fhirbolt-shared") (v "0.1.0") (h "1sqrgkxrj562v885pr5xbivv9z66jxk360p7hc6xix3mq3rbnq0a")))

(define-public crate-fhirbolt-shared-0.1.1 (c (n "fhirbolt-shared") (v "0.1.1") (h "132vwsjjjlnmyvpi19a82bcq8kp7kgb2ksjpbzj724g1j417pqxl")))

(define-public crate-fhirbolt-shared-0.1.2 (c (n "fhirbolt-shared") (v "0.1.2") (h "0g3cn6snjp362j7nddysvzbk62k9ff3w20w1fy7p22vzvdsdv8jm")))

(define-public crate-fhirbolt-shared-0.1.3 (c (n "fhirbolt-shared") (v "0.1.3") (h "0bdmw3w83nam8k9ycg3x1ixhrrwjamcfvxa93qakdk5ip20gqc4p")))

(define-public crate-fhirbolt-shared-0.2.0 (c (n "fhirbolt-shared") (v "0.2.0") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "18xhs4qx1napqv6q6li5sb2ihcc3r4ypam0f7pmxjw9crr7a2n8d")))

(define-public crate-fhirbolt-shared-0.2.1 (c (n "fhirbolt-shared") (v "0.2.1") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "0nsvdqr8skbmvpan2ginx29rs0iknxasn621a2xqqzfk0jjw506d")))

(define-public crate-fhirbolt-shared-0.2.2 (c (n "fhirbolt-shared") (v "0.2.2") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "05j6scvvxv3m64vhj7wllndwjrby8cqssp54k79kwjn9qsvn102i")))

(define-public crate-fhirbolt-shared-0.3.0 (c (n "fhirbolt-shared") (v "0.3.0") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "1l8yx1vyghxb68vj10qxzc96b6650i5wan04r7mn3bwnam2m4mfj")))

(define-public crate-fhirbolt-shared-0.4.0 (c (n "fhirbolt-shared") (v "0.4.0") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "0rpwcrfrj3lsh7s20hbhprda4kxkyrr3vjcqn89k90hb5xxl5yqw")))

