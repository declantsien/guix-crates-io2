(define-module (crates-io fh ir fhir-derive) #:use-module (crates-io))

(define-public crate-fhir-derive-0.0.2 (c (n "fhir-derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1bp8cga77b8n5b31fh31fz8mffvwamzlzdskhqr88nqy0r4i0rag")))

(define-public crate-fhir-derive-0.0.3 (c (n "fhir-derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0libc0z5x9nhz4ygmjz1mq6qp2chac5yadyj96lbza2j7x78y56m")))

(define-public crate-fhir-derive-0.0.4 (c (n "fhir-derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1yqzkxjg6gz8qqmnizpagskrsaqhcly4m11jn9ivcnfck7ij6ghf")))

(define-public crate-fhir-derive-0.0.5 (c (n "fhir-derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0v5jm7ia9wlkplh0xpv54s9in5y9c76jyslgnkl94pda06dj6cs9")))

(define-public crate-fhir-derive-0.0.6 (c (n "fhir-derive") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qp3gx65jg2g6c9srmf2056hd1kgrc1cir0sa2vxn2j4qyqv1n7c")))

(define-public crate-fhir-derive-0.0.7 (c (n "fhir-derive") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1x5asn1chlq7f8x1wjnhgyi90njf1vn00ylgilghvlldnzhzhrwf")))

