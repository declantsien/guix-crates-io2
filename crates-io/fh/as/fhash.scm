(define-module (crates-io fh as fhash) #:use-module (crates-io))

(define-public crate-fhash-0.1.0 (c (n "fhash") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 2)) (d (n "frand") (r "^0.7.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (k 0)) (d (n "image") (r "^0.24.6") (f (quote ("png"))) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "155n5wmmvdqdj723vx3wwmjj3qaip4khw6czvzl915wc2vvw5xdy")))

(define-public crate-fhash-0.1.1 (c (n "fhash") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 2)) (d (n "frand") (r "^0.7.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (k 0)) (d (n "image") (r "^0.24.6") (f (quote ("png"))) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "19wh3mj7l5bcpnb51l4gfmx0r6zbmg5cahsh5clf57nnv10ha1xs")))

(define-public crate-fhash-0.1.2 (c (n "fhash") (v "0.1.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 2)) (d (n "frand") (r "^0.7.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (k 0)) (d (n "image") (r "^0.24.6") (f (quote ("png"))) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0fphz0flxznig9pxi96iif116l16q96d4ba6bhjxcsyd4qdd9iyd")))

(define-public crate-fhash-0.2.0 (c (n "fhash") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.7") (d #t) (k 2)) (d (n "frand") (r "^0.8.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 2)))) (h "12c4s0x33c89qyw07jfpd73jl31dxikp24mjy3khy427dhd20pfp")))

(define-public crate-fhash-0.2.1 (c (n "fhash") (v "0.2.1") (d (list (d (n "ahash") (r "^0.8.7") (d #t) (k 2)) (d (n "frand") (r "^0.8.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 2)))) (h "1q9libkrn4jld79awm9bvgwyildiiyibadi9ah0rd9w3hcf8hz21")))

(define-public crate-fhash-0.7.0 (c (n "fhash") (v "0.7.0") (d (list (d (n "ahash") (r "^0.8.7") (d #t) (k 2)) (d (n "frand") (r "^0.8.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 2)))) (h "1jnb6li40mm0lhyj4i36r3ql4f2jp5kzqgzrklnvv5dxa31ibahl") (f (quote (("std") ("default" "std"))))))

(define-public crate-fhash-0.7.1 (c (n "fhash") (v "0.7.1") (d (list (d (n "ahash") (r "^0.8.7") (d #t) (k 2)) (d (n "frand") (r "^0.8.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 2)))) (h "1xz33w61bwq67qay693k3s9irfhybbinh37hv430gh1r014jn4qz") (f (quote (("std") ("default" "std"))))))

