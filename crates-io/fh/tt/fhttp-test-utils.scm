(define-module (crates-io fh tt fhttp-test-utils) #:use-module (crates-io))

(define-public crate-fhttp-test-utils-1.6.1 (c (n "fhttp-test-utils") (v "1.6.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fhttp-core") (r "^1.6.1") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 0)))) (h "04d9p9xki4ik5xwwicrgkgsycmbgpc6xhhavzgg4vmm233kq4x62")))

(define-public crate-fhttp-test-utils-2.0.0 (c (n "fhttp-test-utils") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fhttp-core") (r "^2.0.0") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 0)))) (h "1b6n2nbivf0rl6i2mlabvbb3s2lrjvp55fhwq0sxf4aggb1xkag2")))

