(define-module (crates-io fh tm fhtml) #:use-module (crates-io))

(define-public crate-fhtml-0.1.0 (c (n "fhtml") (v "0.1.0") (d (list (d (n "const_format") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fhtml-macros") (r "^0.1") (d #t) (k 0)))) (h "01j8nachvc978b65qpnx3x28bbmbsfll9x2jx7lykhrpl463h69h") (s 2) (e (quote (("const_format" "dep:const_format"))))))

(define-public crate-fhtml-0.2.0 (c (n "fhtml") (v "0.2.0") (d (list (d (n "const_format") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fhtml-macros") (r "^0.2") (d #t) (k 0)))) (h "01pkjrgcjcyg2qi8is7fc1kjydhzldias85mqs5h5kg64ac6mkqk") (s 2) (e (quote (("const_format" "dep:const_format"))))))

(define-public crate-fhtml-0.2.1 (c (n "fhtml") (v "0.2.1") (d (list (d (n "const_format") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fhtml-macros") (r "^0.2") (d #t) (k 0)))) (h "0wrifss34xiiba7aggdim5b8ggm6id3nbvx50p54xsjfnpav1m8b") (f (quote (("default")))) (s 2) (e (quote (("const_format" "dep:const_format"))))))

(define-public crate-fhtml-0.3.0 (c (n "fhtml") (v "0.3.0") (d (list (d (n "const_format") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fhtml-macros") (r "^0.3") (d #t) (k 0)))) (h "002my9v2wr0jn4jbnvxfymqvgmkk99lciqwhnvcr4ib9vwkz6ww7") (f (quote (("default")))) (s 2) (e (quote (("const-format" "dep:const_format"))))))

(define-public crate-fhtml-0.4.0 (c (n "fhtml") (v "0.4.0") (d (list (d (n "const_format") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fhtml-macros") (r "^0.4") (d #t) (k 0)) (d (n "lazy_format") (r "^2.0.3") (o #t) (d #t) (k 0)))) (h "0h2py6my0q4ksb4c6fz0ndw8jx33asl6bfc741shxnbysdafqkzz") (f (quote (("default")))) (s 2) (e (quote (("const" "dep:const_format"))))))

(define-public crate-fhtml-0.4.1 (c (n "fhtml") (v "0.4.1") (d (list (d (n "const_format") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fhtml-macros") (r "^0.4") (d #t) (k 0)))) (h "00y10v4spw940bddf4f28b6cdxiaag603g3zh8hbcgs0p8iibj9q") (f (quote (("default")))) (s 2) (e (quote (("const" "dep:const_format"))))))

