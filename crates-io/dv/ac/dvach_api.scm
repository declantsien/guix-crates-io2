(define-module (crates-io dv ac dvach_api) #:use-module (crates-io))

(define-public crate-dvach_api-0.0.1 (c (n "dvach_api") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.9.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1hkqsgrd2yy64n9hd1gsy63b487gvzwxii9v5ixnn5x5iv2yz6sw") (y #t)))

(define-public crate-dvach_api-0.0.2 (c (n "dvach_api") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.9.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1nf5hppxybvf0c7hgzgr07qfnbm0azhm6aglckax0ww98zr612wb")))

