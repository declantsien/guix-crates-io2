(define-module (crates-io dv co dvcompute_gpss_cons) #:use-module (crates-io))

(define-public crate-dvcompute_gpss_cons-1.3.3 (c (n "dvcompute_gpss_cons") (v "1.3.3") (d (list (d (n "dvcompute") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute_cons")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "0h89m3yhra9l27f1iar8f06vak5d4z14fp3a0k7g73dwphibz5dq") (f (quote (("default" "cons_mode") ("cons_mode" "dvcompute/cons_mode"))))))

(define-public crate-dvcompute_gpss_cons-1.3.4 (c (n "dvcompute_gpss_cons") (v "1.3.4") (d (list (d (n "dvcompute") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute_cons")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "1r68zk8izgmqk1nqvc3536ms7y3fc7h3zgyhm3maqvlhvahqyrnk") (f (quote (("default" "cons_mode") ("cons_mode" "dvcompute/cons_mode"))))))

(define-public crate-dvcompute_gpss_cons-1.3.5 (c (n "dvcompute_gpss_cons") (v "1.3.5") (d (list (d (n "dvcompute") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute_cons")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "0z76vkcgxaxmd2rsvibg904nlrhaw8jbr6m1rpsmxkykyjmyfw0l") (f (quote (("default" "cons_mode") ("cons_mode" "dvcompute/cons_mode"))))))

