(define-module (crates-io dv co dvcompute_mpi) #:use-module (crates-io))

(define-public crate-dvcompute_mpi-1.3.3 (c (n "dvcompute_mpi") (v "1.3.3") (d (list (d (n "dvcompute_network") (r "^1.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1dsrly13q6h703z17pf6j4a3r4fr893h5smnsm3xqpq8lw1cdj67")))

(define-public crate-dvcompute_mpi-1.3.4 (c (n "dvcompute_mpi") (v "1.3.4") (d (list (d (n "dvcompute_network") (r "^1.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1x32dy7krh3r6cayqxykr4a59yqd8dr2mkix49cpsac6ipalvxc8")))

(define-public crate-dvcompute_mpi-2.0.0 (c (n "dvcompute_mpi") (v "2.0.0") (d (list (d (n "dvcompute_network") (r "^1.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0mcv4ca5ij2faxvj4j4cnmskmga6a2bhn6d5yhzr98p2zdz43bdc")))

