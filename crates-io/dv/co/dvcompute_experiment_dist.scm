(define-module (crates-io dv co dvcompute_experiment_dist) #:use-module (crates-io))

(define-public crate-dvcompute_experiment_dist-1.3.7 (c (n "dvcompute_experiment_dist") (v "1.3.7") (d (list (d (n "dvcompute_dist") (r "^1.3.7") (d #t) (k 0)) (d (n "dvcompute_network") (r "^1.3.3") (d #t) (k 0)) (d (n "dvcompute_results_dist") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "0hq4sn0j9y6y0mnbsaphcxyv6vhn6hdm3rcfrvzpdgsflhl21jvp") (f (quote (("dist_mode") ("default" "dist_mode"))))))

