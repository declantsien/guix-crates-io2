(define-module (crates-io dv co dvcompute_branch) #:use-module (crates-io))

(define-public crate-dvcompute_branch-1.3.3 (c (n "dvcompute_branch") (v "1.3.3") (d (list (d (n "dvcompute_rand_dist") (r "^1.3.3") (d #t) (k 0) (p "dvcompute_rand")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "06y8vzcjb5lqbdncg7pgqiaqqzmxrf1w6jga91skjrxwysgg21dz") (f (quote (("default" "branch_mode") ("branch_wasm_mode" "dvcompute_rand_dist/wasm_mode") ("branch_mode"))))))

(define-public crate-dvcompute_branch-1.3.4 (c (n "dvcompute_branch") (v "1.3.4") (d (list (d (n "dvcompute_rand_dist") (r "^1.3.3") (d #t) (k 0) (p "dvcompute_rand")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "0v0xqbznfwvn2p0xnzy15sdj9vqwj5pd06x0kzh86q0y7mdryf0s") (f (quote (("default" "branch_mode") ("branch_wasm_mode" "dvcompute_rand_dist/wasm_mode") ("branch_mode"))))))

(define-public crate-dvcompute_branch-1.3.5 (c (n "dvcompute_branch") (v "1.3.5") (d (list (d (n "dvcompute_rand_dist") (r "^1.3.3") (d #t) (k 0) (p "dvcompute_rand")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "004ckqyras0f70xsq1rd7k8nvs75zf2xn88cfw48n60rfr1cag6m") (f (quote (("default" "branch_mode") ("branch_wasm_mode" "dvcompute_rand_dist/wasm_mode") ("branch_mode"))))))

(define-public crate-dvcompute_branch-1.3.7 (c (n "dvcompute_branch") (v "1.3.7") (d (list (d (n "dvcompute_rand_dist") (r "^1.3.3") (d #t) (k 0) (p "dvcompute_rand")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "13xxalyk8scbgwycixhx096pl3fqk3qjkidbvcsji9i901l3d4h6") (f (quote (("default" "branch_mode") ("branch_wasm_mode" "dvcompute_rand_dist/wasm_mode") ("branch_mode"))))))

