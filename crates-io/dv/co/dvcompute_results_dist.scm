(define-module (crates-io dv co dvcompute_results_dist) #:use-module (crates-io))

(define-public crate-dvcompute_results_dist-1.3.3 (c (n "dvcompute_results_dist") (v "1.3.3") (d (list (d (n "dvcompute_dist") (r "^1.3.3") (d #t) (k 0)) (d (n "dvcompute_gpss_dist") (r "^1.3.3") (d #t) (k 0)) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "1x44zmljdd4bp34j63d8p1ckfvrrqz2yi441q96sbp8a0fijrzjp") (f (quote (("dist_mode") ("default" "dist_mode"))))))

