(define-module (crates-io dv co dvcompute_results_branch) #:use-module (crates-io))

(define-public crate-dvcompute_results_branch-1.3.3 (c (n "dvcompute_results_branch") (v "1.3.3") (d (list (d (n "dvcompute_dist") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute_branch")) (d (n "dvcompute_gpss_dist") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute_gpss_branch")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "0x7a7wwc8ijsm84fjwvicws8z57g1dzg020n37gqvvqydibphs0m") (f (quote (("default" "branch_mode") ("branch_wasm_mode" "dvcompute_dist/branch_wasm_mode" "dvcompute_gpss_dist/branch_wasm_mode") ("branch_mode" "dvcompute_dist/branch_mode" "dvcompute_gpss_dist/branch_mode"))))))

