(define-module (crates-io dv co dvcompute_gpss_dist) #:use-module (crates-io))

(define-public crate-dvcompute_gpss_dist-1.3.3 (c (n "dvcompute_gpss_dist") (v "1.3.3") (d (list (d (n "dvcompute_dist") (r "^1.3.3") (d #t) (k 0)) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "1di87k1fal50x26wh6f0y8k67ncqsgkx3x4dvmm91fbfmkclcmly")))

(define-public crate-dvcompute_gpss_dist-1.3.4 (c (n "dvcompute_gpss_dist") (v "1.3.4") (d (list (d (n "dvcompute_dist") (r "^1.3.3") (d #t) (k 0)) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "0g0409pgk2wjpd8k73zqkwshfpak1gd2ai8bp12ywkpxf7k82kms")))

(define-public crate-dvcompute_gpss_dist-1.3.5 (c (n "dvcompute_gpss_dist") (v "1.3.5") (d (list (d (n "dvcompute_dist") (r "^1.3.3") (d #t) (k 0)) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "0vl93wf2qr5wsxakii721rka00hzdsjgsarf3v6in9xn06lkxksy")))

