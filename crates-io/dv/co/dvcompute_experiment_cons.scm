(define-module (crates-io dv co dvcompute_experiment_cons) #:use-module (crates-io))

(define-public crate-dvcompute_experiment_cons-1.3.7 (c (n "dvcompute_experiment_cons") (v "1.3.7") (d (list (d (n "dvcompute") (r "^1.3.5") (o #t) (d #t) (k 0) (p "dvcompute_cons")) (d (n "dvcompute_network") (r "^1.3.3") (d #t) (k 0)) (d (n "dvcompute_results") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute_results_cons")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "168lqjxyriz7xm03my51yvy04p1f8wa1pfkfqwz4f3f4zrikr4vq") (f (quote (("default" "cons_mode") ("cons_mode" "dvcompute/cons_mode" "dvcompute_results/cons_mode"))))))

