(define-module (crates-io dv co dvcompute_rand_dist) #:use-module (crates-io))

(define-public crate-dvcompute_rand_dist-1.3.3 (c (n "dvcompute_rand_dist") (v "1.3.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1qs86p7xi4dkm7z38j7jsnxgs0m3awczihixkdx7vlds8c9svai8") (f (quote (("dist_mode") ("default" "dist_mode"))))))

(define-public crate-dvcompute_rand_dist-1.3.4 (c (n "dvcompute_rand_dist") (v "1.3.4") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "120m6a2lvdylj5jrwm50g87pwj9pw0vbx6vk39divhj9xpwdpvdl") (f (quote (("dist_mode") ("default" "dist_mode"))))))

