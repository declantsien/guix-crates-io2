(define-module (crates-io dv co dvcompute) #:use-module (crates-io))

(define-public crate-dvcompute-1.3.3 (c (n "dvcompute") (v "1.3.3") (d (list (d (n "dvcompute_rand") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute_rand")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "027m6vkygyxahlv6fjpl449sgk8dc5jizwkbmmj34hinb4yggxh7") (f (quote (("wasm_mode" "dvcompute_rand/wasm_mode") ("seq_mode" "dvcompute_rand/seq_mode") ("default" "seq_mode"))))))

(define-public crate-dvcompute-1.3.4 (c (n "dvcompute") (v "1.3.4") (d (list (d (n "dvcompute_rand") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute_rand")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "09wzyavygh0kna9572i432yv7vikd0p0q4s3af6scp7niphahm1g") (f (quote (("wasm_mode" "dvcompute_rand/wasm_mode") ("seq_mode" "dvcompute_rand/seq_mode") ("default" "seq_mode"))))))

