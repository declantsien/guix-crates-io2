(define-module (crates-io dv co dvcompute_gpss) #:use-module (crates-io))

(define-public crate-dvcompute_gpss-1.3.3 (c (n "dvcompute_gpss") (v "1.3.3") (d (list (d (n "dvcompute") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "0zys44ajaxyip1d0ysc6a518p846m10aa01s46kwa5vcdghaja86") (f (quote (("wasm_mode" "dvcompute/wasm_mode") ("seq_mode" "dvcompute/seq_mode") ("default" "seq_mode"))))))

(define-public crate-dvcompute_gpss-1.3.4 (c (n "dvcompute_gpss") (v "1.3.4") (d (list (d (n "dvcompute") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "0v964ivkdchh6kylnglpc3rgzvabkdk3lrkcaw5415b3rby63plj") (f (quote (("wasm_mode" "dvcompute/wasm_mode") ("seq_mode" "dvcompute/seq_mode") ("default" "seq_mode"))))))

(define-public crate-dvcompute_gpss-1.3.5 (c (n "dvcompute_gpss") (v "1.3.5") (d (list (d (n "dvcompute") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "0c0ryd6g21rfkanzqvl23sjfi0az1iwgix8c7svawar9h1qf1n5l") (f (quote (("wasm_mode" "dvcompute/wasm_mode") ("seq_mode" "dvcompute/seq_mode") ("default" "seq_mode"))))))

