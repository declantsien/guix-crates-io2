(define-module (crates-io dv co dvcompute_rand) #:use-module (crates-io))

(define-public crate-dvcompute_rand-1.3.3 (c (n "dvcompute_rand") (v "1.3.3") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1afd2ibjqv9niqa5hr6nhmzgvim78nsas0hcgsv7xdldmnkaidxa") (f (quote (("wasm_mode" "rand/wasm-bindgen") ("seq_mode" "rand") ("default" "seq_mode"))))))

