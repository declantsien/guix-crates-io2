(define-module (crates-io dv co dvcompute_experiment) #:use-module (crates-io))

(define-public crate-dvcompute_experiment-1.3.7 (c (n "dvcompute_experiment") (v "1.3.7") (d (list (d (n "dvcompute") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute")) (d (n "dvcompute_results") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute_results")) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "00dkjb89naj33skpwv1a12yvd97wb3wb2q27bg7p9f48pw8zbzba") (f (quote (("wasm_mode" "dvcompute/wasm_mode" "dvcompute_results/wasm_mode") ("seq_mode" "dvcompute/seq_mode" "dvcompute_results/seq_mode") ("default" "seq_mode"))))))

