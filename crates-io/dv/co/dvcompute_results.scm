(define-module (crates-io dv co dvcompute_results) #:use-module (crates-io))

(define-public crate-dvcompute_results-1.3.3 (c (n "dvcompute_results") (v "1.3.3") (d (list (d (n "dvcompute") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute")) (d (n "dvcompute_gpss") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute_gpss")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "1sha904wdz2irxqqqppibfsk8rfyr6a7vcfi36c9clhz5r2pxrlw") (f (quote (("wasm_mode" "dvcompute/wasm_mode" "dvcompute_gpss/wasm_mode") ("seq_mode" "dvcompute/seq_mode" "dvcompute_gpss/seq_mode") ("default" "seq_mode"))))))

