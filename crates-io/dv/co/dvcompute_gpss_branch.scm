(define-module (crates-io dv co dvcompute_gpss_branch) #:use-module (crates-io))

(define-public crate-dvcompute_gpss_branch-1.3.3 (c (n "dvcompute_gpss_branch") (v "1.3.3") (d (list (d (n "dvcompute_dist") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute_branch")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "0ybwk5n13fgkzxmf8g229gdr2az5ic06j7mgzkhg22wk050fah6p") (f (quote (("default" "branch_mode") ("branch_wasm_mode" "dvcompute_dist/branch_wasm_mode") ("branch_mode" "dvcompute_dist/branch_mode"))))))

(define-public crate-dvcompute_gpss_branch-1.3.4 (c (n "dvcompute_gpss_branch") (v "1.3.4") (d (list (d (n "dvcompute_dist") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute_branch")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "058wmwx7ba4didvvw2389xi83kp5c6j78d7f4p58nx48xhwwx3z0") (f (quote (("default" "branch_mode") ("branch_wasm_mode" "dvcompute_dist/branch_wasm_mode") ("branch_mode" "dvcompute_dist/branch_mode"))))))

(define-public crate-dvcompute_gpss_branch-1.3.5 (c (n "dvcompute_gpss_branch") (v "1.3.5") (d (list (d (n "dvcompute_dist") (r "^1.3.3") (o #t) (d #t) (k 0) (p "dvcompute_branch")) (d (n "dvcompute_utils") (r "^1.3.3") (d #t) (k 0)))) (h "1wgmhgprvi6bpbximgrj442l3lrlqiwzjn330frw7jkpc5mjdiby") (f (quote (("default" "branch_mode") ("branch_wasm_mode" "dvcompute_dist/branch_wasm_mode") ("branch_mode" "dvcompute_dist/branch_mode"))))))

