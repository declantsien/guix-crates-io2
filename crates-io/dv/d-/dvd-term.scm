(define-module (crates-io dv d- dvd-term) #:use-module (crates-io))

(define-public crate-dvd-term-0.1.0 (c (n "dvd-term") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ruscii") (r "^0.4.0") (d #t) (k 0)))) (h "1v5jxfx8xs6rvj9gvaw2cchsh26fwqcw4763fvah6p58p4xd6hlr")))

(define-public crate-dvd-term-0.1.1 (c (n "dvd-term") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ruscii") (r "^0.4.0") (d #t) (k 0)))) (h "1hj93knyfpfrkcbffx0fw6x282yjhifszqvv86k26nsnfxm6lg8n")))

(define-public crate-dvd-term-0.1.2 (c (n "dvd-term") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ruscii") (r "^0.4.0") (d #t) (k 0)))) (h "0gk31cswycj3lm8zndivblzslvjnryp0x991s3c2c0w3li7m326z")))

(define-public crate-dvd-term-0.1.22 (c (n "dvd-term") (v "0.1.22") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ruscii") (r "^0.4.0") (d #t) (k 0)))) (h "0hxxmd5xa58nw6r9g9apvgzqvgk9far71ha2lqfkxpmjcgmmym2h")))

(define-public crate-dvd-term-0.1.23 (c (n "dvd-term") (v "0.1.23") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ruscii") (r "^0.4.0") (d #t) (k 0)))) (h "19lxwjykn0zl1vnr800vhzs05ca81pkb1zmfy9rjiankfqm1fpqp")))

