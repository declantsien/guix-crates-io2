(define-module (crates-io dv bv dvbv5) #:use-module (crates-io))

(define-public crate-dvbv5-0.0.0 (c (n "dvbv5") (v "0.0.0") (d (list (d (n "dvbv5-sys") (r "^0.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.46") (d #t) (k 0)))) (h "0p8crcq0yjz4m15fm8l78sr5hsrq78ybfyq255bsq21mqdx1i9vp")))

(define-public crate-dvbv5-0.0.1 (c (n "dvbv5") (v "0.0.1") (d (list (d (n "dvbv5-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)))) (h "057f38hqlaw33g769d2w8mmwhffljc75ga9pvk8qgby3z1jsqp0q")))

(define-public crate-dvbv5-0.1.0 (c (n "dvbv5") (v "0.1.0") (d (list (d (n "dvbv5-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0axwfdxn6nqa4qazzbkkr6kc2fnpzi4mka575x9rng4pi292fxxg")))

(define-public crate-dvbv5-0.1.1 (c (n "dvbv5") (v "0.1.1") (d (list (d (n "dvbv5-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0qm7l32j4m8jv39ji8pp6vkpb1s2i04508j4qzvlycgs69hl9ip3")))

(define-public crate-dvbv5-0.2.0 (c (n "dvbv5") (v "0.2.0") (d (list (d (n "dvbv5-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1ahrcb0y80mbz1dxgansgfsx8y378yqxb6cqjmq4ww8hpjy4nv1v")))

(define-public crate-dvbv5-0.2.1 (c (n "dvbv5") (v "0.2.1") (d (list (d (n "dvbv5-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "03sn8dq6w9qi7gzy0ysjkl583d6fxzmsjpxjifca75dzd7rwz8j0")))

(define-public crate-dvbv5-0.2.2 (c (n "dvbv5") (v "0.2.2") (d (list (d (n "dvbv5-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "06p6w2qdcrh3fpyvidnxz7lscjk7yk5zz0v013lw3vr37hlghnw8")))

(define-public crate-dvbv5-0.2.3 (c (n "dvbv5") (v "0.2.3") (d (list (d (n "dvbv5-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1fvliskgh1ckdhka5fnv2bkvacvj0zqnmyq6l0bwln1xqc4x2aj1") (f (quote (("docs-rs"))))))

(define-public crate-dvbv5-0.2.4 (c (n "dvbv5") (v "0.2.4") (d (list (d (n "dvbv5-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "03lm4dq03caff5nvwpqj71m2v72x27wx84xaqjxz8y58sfsixifb") (f (quote (("docs-rs" "dvbv5-sys/docs-rs"))))))

(define-public crate-dvbv5-0.2.5 (c (n "dvbv5") (v "0.2.5") (d (list (d (n "dvbv5-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0vaabi20fj8q3xh0vmxn58ish3s0gc80n9485gyplkq8la2q832l") (f (quote (("docs-rs" "dvbv5-sys/docs-rs"))))))

(define-public crate-dvbv5-0.2.6 (c (n "dvbv5") (v "0.2.6") (d (list (d (n "dvbv5-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)))) (h "17axd427yjy40d3g6wz0hkl7baxfp0i1p2vrl77ai8c83kc9rw86") (f (quote (("docs-rs" "dvbv5-sys/docs-rs"))))))

