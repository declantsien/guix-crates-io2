(define-module (crates-io dv bv dvbv5-sys) #:use-module (crates-io))

(define-public crate-dvbv5-sys-0.0.0 (c (n "dvbv5-sys") (v "0.0.0") (d (list (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1wwqjpl98x1l68r7ypnbfnjn7nxdlbkpdrwnp67msgji5d7lx81i")))

(define-public crate-dvbv5-sys-0.0.1 (c (n "dvbv5-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0mbzl4apq8ydlcvb1rmrq0dhp5sbmhcssvg0l0rg1n46acr7mm39")))

(define-public crate-dvbv5-sys-0.1.0 (c (n "dvbv5-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0hznyjd665mxywj0dil6jaaqv2jz41kcydki7iabdndrc040gnjv")))

(define-public crate-dvbv5-sys-0.1.1 (c (n "dvbv5-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "191nwc0fhsxdrafwzxhh0lql54nwyfi4vdmq15fk4blcn72xg1w7")))

(define-public crate-dvbv5-sys-0.1.2 (c (n "dvbv5-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0w2zfzbfzcqshmc4w03h2c2ga77ndrblb0538ks65jhyy6brdvm4")))

(define-public crate-dvbv5-sys-0.1.3 (c (n "dvbv5-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1lw6k060fqxjjjhvklsinvlkxywzm5jvj3il017vq2pg9iiljyl8") (f (quote (("docs-rs"))))))

(define-public crate-dvbv5-sys-0.1.4 (c (n "dvbv5-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "11f6l6qvfid7wfd4ar9fl0g77b4yv54kmzj6pglsnxp1rc24k2im") (f (quote (("docs-rs")))) (l "dvbv5")))

(define-public crate-dvbv5-sys-0.2.0 (c (n "dvbv5-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "18vm7f5mjhi82flhfnmvqj1kkf88mkvcg9ziwyi2lg650wpnk2wy") (f (quote (("docs-rs")))) (l "dvbv5")))

(define-public crate-dvbv5-sys-0.2.1 (c (n "dvbv5-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "08bb2nasmf2imwhiiwcpnq0kqc8lyp0973zvk311hwpa99jjain1") (f (quote (("docs-rs")))) (l "dvbv5")))

