(define-module (crates-io dv i- dvi-to-text) #:use-module (crates-io))

(define-public crate-dvi-to-text-0.1.0 (c (n "dvi-to-text") (v "0.1.0") (d (list (d (n "dvi") (r "^0.2.2") (d #t) (k 0)))) (h "02qk6xgbhn3xlpqb8x7fjzpm0slnh1k9zcqiw44cirqr70x4gkd1")))

(define-public crate-dvi-to-text-0.2.0 (c (n "dvi-to-text") (v "0.2.0") (d (list (d (n "dvi") (r "^0.2.2") (d #t) (k 0)))) (h "04qx1677y21750sfs6l8zpax7abxxhzpaiday1mm7nh7g7qclkkc")))

(define-public crate-dvi-to-text-0.2.1 (c (n "dvi-to-text") (v "0.2.1") (d (list (d (n "dvi") (r "^0.2.2") (d #t) (k 0)))) (h "1fsrv8hby8jdd1grfkc8gznj13k8jmix62ra9snwsb5avi90fppc")))

