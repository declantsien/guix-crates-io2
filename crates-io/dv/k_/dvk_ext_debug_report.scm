(define-module (crates-io dv k_ dvk_ext_debug_report) #:use-module (crates-io))

(define-public crate-dvk_ext_debug_report-0.1.0 (c (n "dvk_ext_debug_report") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "dvk") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "shared_library") (r "^0.1.4") (d #t) (k 0)))) (h "0ch5g5wr4d3xh4zv04jijrm2rcw660ay272zxjd3sydiw9p0phxg") (y #t)))

