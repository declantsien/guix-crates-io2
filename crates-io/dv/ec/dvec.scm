(define-module (crates-io dv ec dvec) #:use-module (crates-io))

(define-public crate-dvec-0.1.0 (c (n "dvec") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (o #t) (d #t) (k 0)))) (h "1zy94iapq9m5a8a8dw7rmnkn284gz457n40a65j29sswkcpf5iy0") (f (quote (("serde_" "serde" "serde_derive") ("default")))) (y #t)))

(define-public crate-dvec-0.2.1 (c (n "dvec") (v "0.2.1") (d (list (d (n "dnum") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (o #t) (d #t) (k 0)))) (h "02ajh8gdvf3ma8bbrr33lpff2fhzp63wzqyl9qiszgz821mbhk6x") (f (quote (("serde_" "serde" "serde_derive") ("default")))) (y #t)))

(define-public crate-dvec-0.3.0 (c (n "dvec") (v "0.3.0") (d (list (d (n "dnum") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (o #t) (d #t) (k 0)))) (h "1nagkia9bpzzsf5kkjfnprvndpgy759kllm0ijkr20c5rz2rn2bs") (f (quote (("serde_" "serde" "serde_derive") ("default")))) (y #t)))

