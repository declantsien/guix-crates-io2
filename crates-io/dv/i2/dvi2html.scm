(define-module (crates-io dv i2 dvi2html) #:use-module (crates-io))

(define-public crate-dvi2html-0.1.0 (c (n "dvi2html") (v "0.1.0") (d (list (d (n "dvi") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1zi66sqr092zfy1m88slldzsm40ixczyhlq0kgqqyp9pr0bgvgq1")))

(define-public crate-dvi2html-0.2.0 (c (n "dvi2html") (v "0.2.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "dvi") (r "^0.2.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1z6yhqn3c17bfn3annx0jc9agvvanvayn5rb0av6ll8g35zmc9dd")))

