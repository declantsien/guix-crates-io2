(define-module (crates-io dv us dvush-lib-with-protos) #:use-module (crates-io))

(define-public crate-dvush-lib-with-protos-0.1.0 (c (n "dvush-lib-with-protos") (v "0.1.0") (h "14sn7syg5a73p3n1ysr3a7bfjfsijpn2nkw0dxnk4g9d8dww6yz7") (l "libwithprotos-workaround")))

(define-public crate-dvush-lib-with-protos-0.2.0 (c (n "dvush-lib-with-protos") (v "0.2.0") (h "0ysyqwa2kd6syj0kx81z5g6z1s7j2a5jd95j9i2m0i1gykw8gvd9") (l "libwithprotos-workaround")))

