(define-module (crates-io ku ri kurinji) #:use-module (crates-io))

(define-public crate-kurinji-1.0.0 (c (n "kurinji") (v "1.0.0") (d (list (d (n "bevy") (r "^0.3") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17dymm870vp8qb97y2djrwprrl32znlpayabsdyv80442ppzmw68")))

(define-public crate-kurinji-1.0.1 (c (n "kurinji") (v "1.0.1") (d (list (d (n "bevy") (r "^0.3") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0cnxwc9168im5sp72yvycnhbfndsn2i42vcylwhdv5sx52c47yrp")))

(define-public crate-kurinji-1.0.2 (c (n "kurinji") (v "1.0.2") (d (list (d (n "bevy") (r ">=0.3.0, <0.4.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "ron") (r ">=0.6.2, <0.7.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "06spdac2v048a8m383x4ijiz6ikzsz8wx12xrpyhngnyy9hka4pp")))

(define-public crate-kurinji-1.0.3 (c (n "kurinji") (v "1.0.3") (d (list (d (n "bevy") (r ">=0.3.0, <0.4.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "ron") (r ">=0.6.2, <0.7.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1jcyvnpz64cpwzwp7mw5x711pb7qrfiimf0lic9yy7y940g4iwml")))

(define-public crate-kurinji-1.0.4 (c (n "kurinji") (v "1.0.4") (d (list (d (n "bevy") (r "^0.3") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "190mkx3v3wrjaccy7fj1xqqgcd12905b8h3s2nplk3n7wjirc7ba")))

(define-public crate-kurinji-1.0.5 (c (n "kurinji") (v "1.0.5") (d (list (d (n "bevy") (r "^0.4") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0p1s5784l17rpcbj4bdbzfqf7zyi91bh7f64q98aw6wx6blab4p4")))

