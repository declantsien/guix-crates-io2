(define-module (crates-io ku ri kurisu-derive) #:use-module (crates-io))

(define-public crate-kurisu-derive-0.1.4 (c (n "kurisu-derive") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "derive" "parsing"))) (d #t) (k 0)))) (h "0hyl0jk0vya3b49p5ivbsbb8h8cpjk7cwydqg20rlgmp2y36dwhn") (y #t)))

(define-public crate-kurisu-derive-0.1.5 (c (n "kurisu-derive") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "derive" "parsing"))) (d #t) (k 0)))) (h "0qcyc7gf7q5gi1w3jnapwdrrpz3ihzprxn4zvzf0bik1d98n7yr9")))

(define-public crate-kurisu-derive-0.1.6 (c (n "kurisu-derive") (v "0.1.6") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "derive" "parsing"))) (d #t) (k 0)))) (h "0jv52jaq74skxzd0xhkszajr07w8mrmndk11jsm32n4mkd9vq3b4")))

