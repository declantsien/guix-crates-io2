(define-module (crates-io ku ri kurisu) #:use-module (crates-io))

(define-public crate-kurisu-0.1.5 (c (n "kurisu") (v "0.1.5") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "kurisu-derive") (r "=0.1.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.11") (d #t) (k 0)) (d (n "textwrap") (r "^0.12") (f (quote ("terminal_size"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)) (d (n "url") (r "^2.2") (o #t) (d #t) (k 0)))) (h "073qix4vrj12kkr43vy7x0s6vbwyvyrhzlyvlvi0vhz2lbwlz9p6") (f (quote (("parser_extras" "url") ("default"))))))

(define-public crate-kurisu-0.1.6 (c (n "kurisu") (v "0.1.6") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "kurisu-derive") (r "=0.1.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.11") (d #t) (k 0)) (d (n "textwrap") (r "^0.12") (f (quote ("terminal_size"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)) (d (n "url") (r "^2.2") (o #t) (d #t) (k 0)))) (h "0qiqyv0yixibhnkpw3fx1812ap935i76s6hgk1s05kcwma1sjvpx") (f (quote (("parser_extras" "url") ("default"))))))

