(define-module (crates-io ku ri kurit-devserver) #:use-module (crates-io))

(define-public crate-kurit-devserver-0.1.0 (c (n "kurit-devserver") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.4") (o #t) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (o #t) (d #t) (k 0)) (d (n "sha-1") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "1n9jwb2wiqgk3439ixcr8908ylqkskz5hf80npf2zqw18l0vmncj") (f (quote (("reload" "notify" "sha-1" "base64") ("default"))))))

