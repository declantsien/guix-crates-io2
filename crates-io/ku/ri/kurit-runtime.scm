(define-module (crates-io ku ri kurit-runtime) #:use-module (crates-io))

(define-public crate-kurit-runtime-0.1.0 (c (n "kurit-runtime") (v "0.1.0") (d (list (d (n "deno_core") (r "^0.221.0") (k 0)) (d (n "kurit-js") (r "^0.1.0") (d #t) (k 0)) (d (n "kurit-ops") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (k 0)))) (h "1jcrgnvk875zbh4r26rbsx5kaf6047i616ppwavlvsfqir5g4swq")))

