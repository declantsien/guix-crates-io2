(define-module (crates-io ku ri kurit-ops) #:use-module (crates-io))

(define-public crate-kurit-ops-0.1.0 (c (n "kurit-ops") (v "0.1.0") (d (list (d (n "deno_core") (r "^0.221.0") (k 0)) (d (n "kurit-devserver") (r "^0.1.0") (d #t) (k 0)) (d (n "kurit-template") (r "^0.1.0") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.14") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (k 0)))) (h "012siiz57m3k8jq14dc1s5vny5xirz4qb95flhnmvr6nkfkv54np")))

