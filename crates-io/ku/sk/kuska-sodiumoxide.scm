(define-module (crates-io ku sk kuska-sodiumoxide) #:use-module (crates-io))

(define-public crate-kuska-sodiumoxide-0.2.5-0 (c (n "kuska-sodiumoxide") (v "0.2.5-0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.41") (k 0)) (d (n "libsodium-sys") (r "^0.2.4") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.59") (o #t) (k 0)) (d (n "serde") (r "^1.0.59") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 2)))) (h "126g2i15gfndc5q0y7b2p0znkxldmzym2yvq8ci742r4vnpqw3xf") (f (quote (("use-pkg-config" "libsodium-sys/use-pkg-config") ("std") ("default" "serde" "std") ("benchmarks"))))))

