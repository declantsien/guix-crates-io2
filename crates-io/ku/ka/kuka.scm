(define-module (crates-io ku ka kuka) #:use-module (crates-io))

(define-public crate-kuka-0.1.0 (c (n "kuka") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mm22yhhb3km41sk76gx9k2wbpfhi1c86y3i6iyaaayv688z1gx0")))

