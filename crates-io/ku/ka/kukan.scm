(define-module (crates-io ku ka kukan) #:use-module (crates-io))

(define-public crate-kukan-0.1.0 (c (n "kukan") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1ps763ipli1wipykg8id01d7iinzr8lrv2qa98ngj375vl9j1amm")))

(define-public crate-kukan-0.1.1 (c (n "kukan") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "10x720g6jmyxw6zv6k6ml7y9b9fp7lzczzjvm6kqlwx6i72ad9z0")))

(define-public crate-kukan-0.1.2 (c (n "kukan") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0jnprvmizafzgmh2azsvy5k1nxvxm5npk82q4sfyh5wpyz4v78cc")))

