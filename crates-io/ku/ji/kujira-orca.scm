(define-module (crates-io ku ji kujira-orca) #:use-module (crates-io))

(define-public crate-kujira-orca-0.8.0 (c (n "kujira-orca") (v "0.8.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8.0") (d #t) (k 0)))) (h "0h1hhif4j68lgf32n6x77azy34k7g5f8qi6nxxz9w31i0ghwcyrk")))

(define-public crate-kujira-orca-0.8.1 (c (n "kujira-orca") (v "0.8.1") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "01czzwpjjgs89pnmqj9s6av1a3ihymy83103748mayxsj5y1srg0")))

(define-public crate-kujira-orca-0.8.2 (c (n "kujira-orca") (v "0.8.2") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "17hx8xzwxp5688j435qcayy7d5wj48jphsccgyrb46m0wdy5rcmq")))

(define-public crate-kujira-orca-0.8.3 (c (n "kujira-orca") (v "0.8.3") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "0fb0iizhqckmhlvvx35in6lixqn8zmhcs0knm5zp9xbn9l3bvzl5")))

(define-public crate-kujira-orca-1.0.0 (c (n "kujira-orca") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "kujira-std") (r "^1.0.0") (d #t) (k 0)))) (h "06jkjkx6dm9h5mmhpw66vhhfm50i9hvndiaqksn5ilif1r7ai4rj")))

(define-public crate-kujira-orca-1.1.0 (c (n "kujira-orca") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^2.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0") (d #t) (k 0)) (d (n "kujira-std") (r "^1.1.0") (d #t) (k 0)))) (h "0vmz3pngwj3iv7gfxrq70njxi4p2zgd5v6lyqisv4dyzvnq7kv7s")))

