(define-module (crates-io ku ji kujira-fin) #:use-module (crates-io))

(define-public crate-kujira-fin-0.8.0 (c (n "kujira-fin") (v "0.8.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw20") (r "^1.0") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8.0") (d #t) (k 0)))) (h "0jrpwnqiq8g2n5dzi74c6yd66hhfpg27b77k6ygr7ncfzfvqbzqn")))

(define-public crate-kujira-fin-0.8.1 (c (n "kujira-fin") (v "0.8.1") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw20") (r "^1.0") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "0p8g6jpks26348wqvqxyv79m5kgnjpxskywzpvqdndnipf9iwyi5")))

(define-public crate-kujira-fin-0.8.2 (c (n "kujira-fin") (v "0.8.2") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw20") (r "^1.0") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "0dv8v4vqf4dhcc6jc4c39bzkd7b6xaldsg438mi5mprbi9khvfc9")))

(define-public crate-kujira-fin-0.8.3 (c (n "kujira-fin") (v "0.8.3") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw20") (r "^1.1") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "1iz5kl6m0m5m7x4ssbhp245rm39npbhdx8sr40p8nm7x8j6d5cip")))

(define-public crate-kujira-fin-0.9.0 (c (n "kujira-fin") (v "0.9.0") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw20") (r "^1.1") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "09jcxazf5j85i2f76f80zpsgprxy4ij1z2g7qvq1nipk1jxm243w")))

(define-public crate-kujira-fin-1.0.0 (c (n "kujira-fin") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw20") (r "^1.1") (d #t) (k 0)) (d (n "kujira-std") (r "^1.0.0") (d #t) (k 0)))) (h "09kk9dv9pknw7yqs7vjsw8hj16nn7qvv9y54xzvldd7ihhnlb76p")))

(define-public crate-kujira-fin-1.1.0 (c (n "kujira-fin") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^2.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0") (d #t) (k 0)) (d (n "cw20") (r "^2.0") (d #t) (k 0)) (d (n "kujira-std") (r "^1.1.0") (d #t) (k 0)))) (h "172p7kk8hza8m0i7anqsfqvw9xlajinpvb40ab6b4ajx476ybdbk")))

