(define-module (crates-io ku ji kujira-stable) #:use-module (crates-io))

(define-public crate-kujira-stable-0.8.0 (c (n "kujira-stable") (v "0.8.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "kujira-fin") (r "^0.8.0") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8.0") (d #t) (k 0)))) (h "1b4s25yz12asyshalm8vcz3ms3i9dx2w36m0rb1m7fhhdgc4d95w")))

(define-public crate-kujira-stable-0.8.1 (c (n "kujira-stable") (v "0.8.1") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "kujira-fin") (r "^0.8") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "1q6w7d6wnd0kxvw0v11j4l70rrqj67zwn1zcwjarpvpiy7qxx3nx")))

(define-public crate-kujira-stable-0.8.2 (c (n "kujira-stable") (v "0.8.2") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "kujira-fin") (r "^0.8") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "0w5bkm0pywiwqm86lqzbb08f0ph7akwfppkd9635v4kcrxijhs1d")))

(define-public crate-kujira-stable-0.9.0 (c (n "kujira-stable") (v "0.9.0") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "kujira-fin") (r "^0.8") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "1wg66fwpa50cckly99jxjxbdnifp607hvbhk8r1kjml9xh2m325l") (y #t)))

(define-public crate-kujira-stable-0.9.1 (c (n "kujira-stable") (v "0.9.1") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "kujira-fin") (r "^0.8") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "1rs95ysbb717pmrnwd24h4dalbc1ldf2860zdymn4xhwbfh67iy8")))

(define-public crate-kujira-stable-1.0.0 (c (n "kujira-stable") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "kujira-fin") (r "^1.0.0") (d #t) (k 0)) (d (n "kujira-std") (r "^1.0.0") (d #t) (k 0)))) (h "037krh4sjvlfivnz6qyvszgwk210yam6pgvppw4l49bifnrsng27")))

(define-public crate-kujira-stable-1.1.0 (c (n "kujira-stable") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^2.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0") (d #t) (k 0)) (d (n "kujira-fin") (r "^1.1.0") (d #t) (k 0)) (d (n "kujira-std") (r "^1.1.0") (d #t) (k 0)))) (h "09rly7mksm47vrld4qqmr4xzw3sfvqbgnnqxiv2sws3346f1ncv5")))

