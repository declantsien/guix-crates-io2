(define-module (crates-io ku ji kujira-ghost) #:use-module (crates-io))

(define-public crate-kujira-ghost-0.8.0 (c (n "kujira-ghost") (v "0.8.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8.0") (d #t) (k 0)))) (h "1gvgkqaq3ji9rnhhkqzghrn7ilkxbqksg7q7bfi473zx4rkdkx0q")))

(define-public crate-kujira-ghost-0.8.1 (c (n "kujira-ghost") (v "0.8.1") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "0z0ql4287q3bd2zxv5xy75pywlp3l2xa3ssjcmvhfv2cgrn92yyb")))

(define-public crate-kujira-ghost-0.8.2 (c (n "kujira-ghost") (v "0.8.2") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "0x7r36jawvq2ray965hanaqz8bdbmpwviqhgf5w730lsb342nm8z")))

(define-public crate-kujira-ghost-0.8.3 (c (n "kujira-ghost") (v "0.8.3") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "1ffa81fkkbb7ygcdsn9w3av95gbrvi5iiw78kxzi0xyppfv3gf2s")))

(define-public crate-kujira-ghost-0.8.4 (c (n "kujira-ghost") (v "0.8.4") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "0d6q2dy90kf6m1syjlxmz6d7372682jlhydhp4yskhxrr8d2zwyz")))

(define-public crate-kujira-ghost-0.9.0 (c (n "kujira-ghost") (v "0.9.0") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "0mv2kbmkiz4smj1dr9601db6isfh6az7p5p7bff8g2r92jf00rww") (y #t)))

(define-public crate-kujira-ghost-0.9.1 (c (n "kujira-ghost") (v "0.9.1") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0") (d #t) (k 0)) (d (n "kujira-fin") (r "^0.9") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)))) (h "08zkqar10xphx73iz5znr7zxyk3jzlwh1a9vxlv8bijmrradq40a")))

(define-public crate-kujira-ghost-1.0.0 (c (n "kujira-ghost") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0") (d #t) (k 0)) (d (n "kujira-fin") (r "^1.0.0") (d #t) (k 0)) (d (n "kujira-std") (r "^1.0.0") (d #t) (k 0)))) (h "15x5pswjqns3vk85hcwvlnhni07knil1wpnm5dngigy6ih8nzj8x")))

(define-public crate-kujira-ghost-1.0.1 (c (n "kujira-ghost") (v "1.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0") (d #t) (k 0)) (d (n "kujira-fin") (r "^1.0.0") (d #t) (k 0)) (d (n "kujira-std") (r "^1.0.0") (d #t) (k 0)))) (h "0850308k4pim7z4b4nmgxk63za0b3qdasypridk4dbpfw1g9w339")))

(define-public crate-kujira-ghost-1.1.0 (c (n "kujira-ghost") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^2.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0") (d #t) (k 0)) (d (n "cw-utils") (r "^2.0") (d #t) (k 0)) (d (n "kujira-fin") (r "^1.1.0") (d #t) (k 0)) (d (n "kujira-std") (r "^1.1.0") (d #t) (k 0)))) (h "13f46f8wv2wffijkwzjjnl4r0hbf412rzrskl5mf7l27vx3jpd77")))

