(define-module (crates-io ku ji kujira-bow) #:use-module (crates-io))

(define-public crate-kujira-bow-0.8.0 (c (n "kujira-bow") (v "0.8.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "kujira-fin") (r "^0.8.0") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "02gfzp6v5n7lcnqmmivrqwgc848samv6ihhb2wf4x4v7a2nq1xfb")))

(define-public crate-kujira-bow-0.8.1 (c (n "kujira-bow") (v "0.8.1") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "kujira-fin") (r "^0.8") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0f9dpvhsf2h611w7bww213mnxi4wnf0iwnax0cl9y1i116sl4qw0")))

(define-public crate-kujira-bow-0.8.2 (c (n "kujira-bow") (v "0.8.2") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "kujira-fin") (r "^0.8") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1c9hrag30q2vk81c2fbapy83wlyhgl5iq462q8qyb3iqnxklzzqf")))

(define-public crate-kujira-bow-0.8.3 (c (n "kujira-bow") (v "0.8.3") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "kujira-fin") (r "^0.8") (d #t) (k 0)) (d (n "kujira-std") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ddw7p3bdxmjr8j3l8z2565hq8wnsm8an1pg4z0kcvw9ml7v8wzi")))

(define-public crate-kujira-bow-1.0.0 (c (n "kujira-bow") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5") (d #t) (k 0)) (d (n "kujira-fin") (r "^1.0.0") (d #t) (k 0)) (d (n "kujira-std") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0h37d71nv24nff12n9gmszv1q1mygkzk61wxrqli0s9acdcylajn")))

(define-public crate-kujira-bow-1.1.0 (c (n "kujira-bow") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^2.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0") (d #t) (k 0)) (d (n "kujira-fin") (r "^1.1.0") (d #t) (k 0)) (d (n "kujira-std") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0c3l0z8i9m0m2h1zid6hcm3415f80psgm7y5ra1mrdkr5hddqqlm")))

