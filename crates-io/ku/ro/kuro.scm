(define-module (crates-io ku ro kuro) #:use-module (crates-io))

(define-public crate-kuro-0.1.0 (c (n "kuro") (v "0.1.0") (h "0l7mwhyfzvckczj87p53w6ngqqlxacfr31wsgyw92d56va47abj0") (y #t)))

(define-public crate-kuro-0.0.0 (c (n "kuro") (v "0.0.0") (h "0270gpdbgsbc7qgir1vnaq62h0vnqvm8sx512qlbdi2wphqg13bh")))

