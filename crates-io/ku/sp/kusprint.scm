(define-module (crates-io ku sp kusprint) #:use-module (crates-io))

(define-public crate-kusprint-1.0.0 (c (n "kusprint") (v "1.0.0") (h "186a4xkapip8mhnkwkbs2j74fggy54ngv74sgf84mly63jiw27v5")))

(define-public crate-kusprint-1.0.1 (c (n "kusprint") (v "1.0.1") (h "0ypy6ada6qfkhyfn2plw3556n48aps7n6mn0dddyl6rhpnmsbkyi")))

(define-public crate-kusprint-1.0.2 (c (n "kusprint") (v "1.0.2") (h "0j312ci6sch1xq7rp7m015w72rp9v9nizvgnqj8fsjqw1w54qcrq") (y #t)))

(define-public crate-kusprint-1.0.3 (c (n "kusprint") (v "1.0.3") (h "0mkmxapxihsjyx7k9fj8lf2q4p7lch1fg8c37bpdmxxnbiv6c8x9")))

