(define-module (crates-io ku ru kurumi) #:use-module (crates-io))

(define-public crate-kurumi-0.1.0 (c (n "kurumi") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "glib-build-tools") (r "^0.19") (d #t) (k 1)) (d (n "gtk") (r "^0.8") (f (quote ("v4_12"))) (d #t) (k 0) (p "gtk4")) (d (n "poppler-rs") (r "^0.23") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "1kgcmcgynbw565b93wxc2za7ayydfm9wyiazwzxy6w3q4ji5056l")))

