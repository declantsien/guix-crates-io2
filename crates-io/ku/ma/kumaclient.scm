(define-module (crates-io ku ma kumaclient) #:use-module (crates-io))

(define-public crate-kumaClient-0.1.0 (c (n "kumaClient") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1850qq7mk3z0fdn0gapfnkv263c1p3djbxir9fy62z6nzqmpzzkg") (y #t)))

(define-public crate-kumaClient-0.1.1 (c (n "kumaClient") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0c5aml42xf74r0rw80d9dhi6xvgd2m17hiflrl38hkzpav0x8wwq") (y #t)))

(define-public crate-kumaClient-0.1.2 (c (n "kumaClient") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rust_socketio") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0fd2lllj0yjiq9gdzzqrwq2galjya6d0n47qz52bfx2yjw38kv6h") (y #t)))

