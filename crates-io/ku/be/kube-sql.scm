(define-module (crates-io ku be kube-sql) #:use-module (crates-io))

(define-public crate-kube-sql-0.0.1 (c (n "kube-sql") (v "0.0.1") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17.0") (f (quote ("v1_26"))) (d #t) (k 0)) (d (n "kube") (r "^0.80.0") (f (quote ("runtime" "derive"))) (d #t) (k 0)) (d (n "sqlparser") (r "^0.32.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13m5r2mc2d3jck7ppvmlv3m9nfshr9x91hndvay1l9bjv65dlwp1") (r "1.67.0")))

