(define-module (crates-io ku be kubewatch) #:use-module (crates-io))

(define-public crate-kubewatch-0.9.0 (c (n "kubewatch") (v "0.9.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0sk0wcfqpx212bmmhqq577463fr335r9pr305c7vl2fxsxmk67wd")))

(define-public crate-kubewatch-0.9.1 (c (n "kubewatch") (v "0.9.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0a9j1jbwkjzqnppzv8ns4fx57zaf9yq2l1rba349zrqwgjwhfr10")))

(define-public crate-kubewatch-0.9.2 (c (n "kubewatch") (v "0.9.2") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0xp3j1f96jv7iw2lqqh600r8nvrbfazwb47d4n4vqhxizks833iz")))

