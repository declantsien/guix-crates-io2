(define-module (crates-io ku be kube-conf) #:use-module (crates-io))

(define-public crate-kube-conf-0.1.0 (c (n "kube-conf") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0jdzvj1435d77kzqy764ssijakbixxwmprp6b3a2fiywqikyhaa5")))

(define-public crate-kube-conf-0.2.0 (c (n "kube-conf") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)))) (h "03qdap79a9nagl7sqxpxlwsclpx2h13yq1n0y7f2zlgr8v6i072f")))

