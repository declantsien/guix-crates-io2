(define-module (crates-io ku be kube_quantity_2) #:use-module (crates-io))

(define-public crate-kube_quantity_2-0.6.1 (c (n "kube_quantity_2") (v "0.6.1") (d (list (d (n "k8s-openapi") (r "^0.19.0") (k 0)) (d (n "k8s-openapi") (r "^0.19.0") (f (quote ("v1_27"))) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1pmlyvgxvmbrs8mhb388ln2dyv7jsbvq813s4kh381gvipspabq5") (f (quote (("__check" "k8s-openapi/v1_27"))))))

