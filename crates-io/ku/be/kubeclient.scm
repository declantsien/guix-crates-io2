(define-module (crates-io ku be kubeclient) #:use-module (crates-io))

(define-public crate-kubeclient-0.1.0 (c (n "kubeclient") (v "0.1.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "openssl") (r "^0.9.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.1") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)) (d (n "url_serde") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "1cb808lf0lzk25qnjikmsk37ydn88wmrwiv765x6zrbmwwi3nd6l")))

