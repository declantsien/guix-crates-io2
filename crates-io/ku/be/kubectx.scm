(define-module (crates-io ku be kubectx) #:use-module (crates-io))

(define-public crate-kubectx-1.0.0 (c (n "kubectx") (v "1.0.0") (d (list (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)))) (h "0xz2wkaj82qajx4qjgkhlwy8ifdngz6y8yfjpfarvhn7rnai5kpy")))

(define-public crate-kubectx-1.0.1 (c (n "kubectx") (v "1.0.1") (d (list (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)))) (h "1w49zrqrqb9vybwr31w2v4h4z172v5vv5b3szqcxlwm3n20gpp5v")))

(define-public crate-kubectx-1.0.2 (c (n "kubectx") (v "1.0.2") (d (list (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)))) (h "1ics3lyg6z6wls0by4ibgv5gawww8bc2z5gb0adr4i928m0wg8wc")))

(define-public crate-kubectx-1.0.3 (c (n "kubectx") (v "1.0.3") (d (list (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)))) (h "1m1fdgr1qnj965v6hymy4h9rhd3j1zxi5ipcb0cwx2h7sh4xm714")))

