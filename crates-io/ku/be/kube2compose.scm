(define-module (crates-io ku be kube2compose) #:use-module (crates-io))

(define-public crate-kube2compose-0.1.0 (c (n "kube2compose") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "docker-compose-types") (r "^0.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17.0") (f (quote ("v1_26"))) (d #t) (k 0)) (d (n "kube") (r "^0.78.0") (f (quote ("runtime" "derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("time" "signal" "sync" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "140qlvhnzrk6kr2lwnyqgbbizr9apzp7vl42vf899pa6mk8hd659")))

