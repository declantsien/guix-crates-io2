(define-module (crates-io ku be kubera) #:use-module (crates-io))

(define-public crate-kubera-0.0.1 (c (n "kubera") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1dxxdpiv7qywg23827a31cjqciw7mq32r6hj9bz7xsgq4c7w3240")))

