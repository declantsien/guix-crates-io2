(define-module (crates-io ku be kubectl-sh) #:use-module (crates-io))

(define-public crate-kubectl-sh-0.1.0 (c (n "kubectl-sh") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)))) (h "0d5x20y9ci10i9srfyragk7d2yxac3pyznj12a6slw3q6k8v5ahq") (y #t)))

(define-public crate-kubectl-sh-0.1.1 (c (n "kubectl-sh") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)))) (h "06w447k24clymg6nnxl66r56h21qcaxbf7cqa44mzqv56s4ki78m")))

(define-public crate-kubectl-sh-0.1.2 (c (n "kubectl-sh") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "1q5wks376z52hdxljz501j42kb40252ikb6qikc9dr2d7x271dxm")))

(define-public crate-kubectl-sh-0.2.0 (c (n "kubectl-sh") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "07xw5vgnrb0wyrjc85n632hqjbn6a122kv8zc0nmi4jy83hxalza")))

