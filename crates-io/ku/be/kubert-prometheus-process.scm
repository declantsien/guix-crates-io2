(define-module (crates-io ku be kubert-prometheus-process) #:use-module (crates-io))

(define-public crate-kubert-prometheus-process-0.1.0 (c (n "kubert-prometheus-process") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "procfs") (r "^0.16") (t "cfg(target_os = \"linux\")") (k 0)) (d (n "prometheus-client") (r "^0.22.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "15rv253riaakks84vi4pcqrvfgn6338fzqhgcf0kcsmn1xgz9lj0") (r "1.65")))

