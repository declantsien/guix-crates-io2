(define-module (crates-io ku be kubeless) #:use-module (crates-io))

(define-public crate-kubeless-0.1.0 (c (n "kubeless") (v "0.1.0") (d (list (d (n "actix-web") (r "^0.6.14") (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.4") (k 0)))) (h "19lwwgjhi2vc675hkakmqy4618nlshjp85i98i9kclxqqgnxld5r") (f (quote (("nightly" "prometheus/nightly" "lazy_static/nightly") ("default"))))))

(define-public crate-kubeless-0.1.1 (c (n "kubeless") (v "0.1.1") (d (list (d (n "actix-web") (r "^0.6.14") (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.4") (k 0)))) (h "187jwn6hia7jbzn1ggdzy81ycprz8z3ajwmvf2sg7xafqyd6d5za") (f (quote (("nightly" "prometheus/nightly" "lazy_static/nightly") ("default"))))))

(define-public crate-kubeless-0.1.2 (c (n "kubeless") (v "0.1.2") (d (list (d (n "actix-web") (r "^0.6.14") (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.4") (k 0)))) (h "18f18y6d61hk0ndk3i26kfb0rd4hin6xf6wq8d69ip2qs8hplzsr") (f (quote (("nightly" "prometheus/nightly" "lazy_static/nightly") ("default"))))))

(define-public crate-kubeless-0.1.3 (c (n "kubeless") (v "0.1.3") (d (list (d (n "actix-web") (r "^0.6.14") (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.4") (k 0)))) (h "0qc0c4zbzhv2dhckr5g924l8n7lpcrzw7vmawca1d50fdaai4n35") (f (quote (("nightly" "prometheus/nightly" "lazy_static/nightly") ("default"))))))

