(define-module (crates-io ku be kubectx-rs) #:use-module (crates-io))

(define-public crate-kubectx-rs-1.0.0 (c (n "kubectx-rs") (v "1.0.0") (d (list (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)))) (h "0fjia55k6k0j4fv11sl80m2hv5fqmvka0mh04frl2valfqrknk0s")))

(define-public crate-kubectx-rs-1.0.1 (c (n "kubectx-rs") (v "1.0.1") (h "1v56v74irgarjgicbqy7k1xwl1cwnvjz4rhkkby9x68lw8cniqr3")))

