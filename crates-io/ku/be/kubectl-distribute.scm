(define-module (crates-io ku be kubectl-distribute) #:use-module (crates-io))

(define-public crate-kubectl-distribute-0.1.0 (c (n "kubectl-distribute") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17.0") (f (quote ("v1_20"))) (d #t) (k 0)) (d (n "kube") (r "^0.78.0") (f (quote ("derive" "runtime" "ws"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18fan4gc33j4jz974871b705yg1v19cvnxs0zxs3ivvbvrykp8m5")))

