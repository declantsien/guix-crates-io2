(define-module (crates-io ku be kube-prometheus-stack-crds) #:use-module (crates-io))

(define-public crate-kube-prometheus-stack-crds-0.1.0 (c (n "kube-prometheus-stack-crds") (v "0.1.0") (d (list (d (n "k8s-openapi") (r "^0.15.0") (f (quote ("v1_20"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1z9jjziwlzr1bpsbl9xn97zffic1bqqwk0i4qhcxfh2llsw6fm41")))

