(define-module (crates-io ku be kubert-prometheus-tokio) #:use-module (crates-io))

(define-public crate-kubert-prometheus-tokio-0.1.0 (c (n "kubert-prometheus-tokio") (v "0.1.0") (d (list (d (n "prometheus-client") (r "^0.22") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio-metrics") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1fvczwi5pxr2j445swmi9wh9x93dk194dbsacqq93j48yhyzl0d1") (f (quote (("rt" "tokio/rt" "tokio/time" "tokio-metrics/rt")))) (r "1.65")))

