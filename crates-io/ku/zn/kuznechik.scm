(define-module (crates-io ku zn kuznechik) #:use-module (crates-io))

(define-public crate-kuznechik-0.1.0 (c (n "kuznechik") (v "0.1.0") (d (list (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "00dfg5hcwcl47k37m17lkq14ifj62y5c4ikvj6ibq8wh8k2w61r3")))

(define-public crate-kuznechik-0.1.1 (c (n "kuznechik") (v "0.1.1") (d (list (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "0fgqiqd1wmpiah5986p27mb92q7p1zyjiz644m4bnjxg1rvl1sm8")))

(define-public crate-kuznechik-0.2.0 (c (n "kuznechik") (v "0.2.0") (d (list (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "0a5rj1hgczbgjyzlnvlfmx90gvnjy2rkzkcswq6qipbkclpl8w1k")))

(define-public crate-kuznechik-0.3.0 (c (n "kuznechik") (v "0.3.0") (d (list (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "1w1yjn54f84cw5rbi2ndcgk35jh3hczlpx9b96d0kwd8k4wdcm8w")))

(define-public crate-kuznechik-0.4.0 (c (n "kuznechik") (v "0.4.0") (d (list (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "0lfpp6b9w3g00g6qvwc8drkmk006n4w473cbh7pc3s9snckwnkwk")))

