(define-module (crates-io ku mq kumquat) #:use-module (crates-io))

(define-public crate-kumquat-0.1.0 (c (n "kumquat") (v "0.1.0") (d (list (d (n "syntect") (r "^4.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1fqgilg8qld6yf2z00mfkn8245xdi00s3m5wsbicxn320f4wasc8")))

(define-public crate-kumquat-1.0.0 (c (n "kumquat") (v "1.0.0") (d (list (d (n "syntect") (r "^4.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0qr1sbycv36sk3qcrdf3n17ib1z4vivmm1kl9l2kczl2xa16n4f8")))

(define-public crate-kumquat-1.0.1 (c (n "kumquat") (v "1.0.1") (d (list (d (n "syntect") (r "^4.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1afz2d1ji01fbkfl8d81mvja006ah5g9mnq5ds2ajbv4db9nf5qa")))

