(define-module (crates-io ku on kuon_request_derive) #:use-module (crates-io))

(define-public crate-kuon_request_derive-0.0.0 (c (n "kuon_request_derive") (v "0.0.0") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "kuon") (r "^0.0.22") (d #t) (k 0)) (d (n "kuon_proc_macro_utils") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (d #t) (k 0)))) (h "0xanfwjwk273hca0fawql23d7d46kr3xcfsz72hslw487inpc7d0")))

(define-public crate-kuon_request_derive-0.0.1 (c (n "kuon_request_derive") (v "0.0.1") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "kuon_proc_macro_utils") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (d #t) (k 0)))) (h "1bxpch02pgs5vhp08h58g9c1fa18ljiajjmrs5plidm1gqlm31cr")))

