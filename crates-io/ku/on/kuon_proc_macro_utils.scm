(define-module (crates-io ku on kuon_proc_macro_utils) #:use-module (crates-io))

(define-public crate-kuon_proc_macro_utils-0.0.0 (c (n "kuon_proc_macro_utils") (v "0.0.0") (d (list (d (n "if_chain") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "11p7igghfjkw655k7fp8a14gjj54vydc67kb9nw7rv9373x5vmsc")))

