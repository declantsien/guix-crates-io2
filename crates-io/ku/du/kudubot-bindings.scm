(define-module (crates-io ku du kudubot-bindings) #:use-module (crates-io))

(define-public crate-kudubot-bindings-0.1.0 (c (n "kudubot-bindings") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iqq3bxq0xdn6jxh5b42brd6dqaa41fa90vi7lxsl26hc9zv8mqy")))

(define-public crate-kudubot-bindings-0.1.1 (c (n "kudubot-bindings") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d0p6vkwkrr4bssnbyz9vfdj6vdmb3pyz652ls94vm5qxk1sv7y3")))

(define-public crate-kudubot-bindings-0.14.3 (c (n "kudubot-bindings") (v "0.14.3") (d (list (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "114kvcyf7rnl6plb8fc8qyq9vd55yp8b546ly32rnd0pdflb4vhp")))

(define-public crate-kudubot-bindings-0.15.0 (c (n "kudubot-bindings") (v "0.15.0") (d (list (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1njqagalbmqhm5a2cq8k2qgqwayl33m6nh7bf76yv1vf9iikz4a4")))

(define-public crate-kudubot-bindings-0.16.0 (c (n "kudubot-bindings") (v "0.16.0") (d (list (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hg7wbdk4hwbpzmizr79jb73jgj2gblf8ln0bsbwk8fji3n38b7y")))

(define-public crate-kudubot-bindings-0.16.1 (c (n "kudubot-bindings") (v "0.16.1") (d (list (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z8g2z9in835d8pvqia8gzi4yfs5zg81wwxv2c1c60xm3f9wp93w")))

(define-public crate-kudubot-bindings-0.18.2 (c (n "kudubot-bindings") (v "0.18.2") (d (list (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qiz6p1yc0f033ngndlj5cyfilj6qg43qwsgil5wbncimb10h73p")))

