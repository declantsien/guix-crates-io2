(define-module (crates-io ku ge kugel) #:use-module (crates-io))

(define-public crate-kugel-0.0.1 (c (n "kugel") (v "0.0.1") (d (list (d (n "gl") (r "*") (d #t) (k 0)))) (h "15gjd62rhi9vk5y72wx3200nz1997d88c6g7n885ssi4slxrwq1f")))

(define-public crate-kugel-0.0.2 (c (n "kugel") (v "0.0.2") (d (list (d (n "gl") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0r11fpyizhg3iis6bwavk86hcaphk9hv2jk6vazayvicl4h602p3")))

