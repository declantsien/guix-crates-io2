(define-module (crates-io ku pi kupiakos-foo) #:use-module (crates-io))

(define-public crate-kupiakos-foo-1.0.0 (c (n "kupiakos-foo") (v "1.0.0") (h "14wlpxa4hc4kvwbnswrvi9l9h0n4baq63n72y1hqwa4yfvx50rdl")))

(define-public crate-kupiakos-foo-1.0.1 (c (n "kupiakos-foo") (v "1.0.1") (h "19zwsyf4g6gj8lav91shiapr42q46im3ri1jzaqhvqd5x3hryzmm")))

