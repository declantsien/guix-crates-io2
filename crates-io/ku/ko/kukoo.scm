(define-module (crates-io ku ko kukoo) #:use-module (crates-io))

(define-public crate-kukoo-0.1.0 (c (n "kukoo") (v "0.1.0") (d (list (d (n "clippy-utilities") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.9.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1wahjig837jajpqilw9wwpxyxpdf5anlmhkbkk2ym89rs9zkbrwj")))

