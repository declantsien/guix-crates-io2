(define-module (crates-io ku ba kuba) #:use-module (crates-io))

(define-public crate-kuba-0.1.0 (c (n "kuba") (v "0.1.0") (h "1q2yk43j5bard4lhqwdr75rf9vw2snqp76a0vh5w0p7cps0dgia9")))

(define-public crate-kuba-0.1.1 (c (n "kuba") (v "0.1.1") (h "1i5lzsd0sz7r4ncpzwdh51jxcppg24scji2kaavbsx65hk6wdx7l")))

(define-public crate-kuba-0.1.2 (c (n "kuba") (v "0.1.2") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1ljvv0cckrbb0zp1d0kz7150xiqiql04kl6mjlr68vwnk5px1h10")))

(define-public crate-kuba-0.1.3 (c (n "kuba") (v "0.1.3") (h "1hi06i5bysjl8rkv44mrfhy2mkz7wm32jy6jkjj6rg999smmzxn0")))

