(define-module (crates-io ku bi kubizone-common) #:use-module (crates-io))

(define-public crate-kubizone-common-0.1.0 (c (n "kubizone-common") (v "0.1.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "14iwm32l8hvw34vq0q2j8zpdfmgy17iq6cq8naw7acl878d0ny73")))

(define-public crate-kubizone-common-0.1.1 (c (n "kubizone-common") (v "0.1.1") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a6m3c7vi74aq84xsy8x5b9ycdafbi7ssmasqiw3jsp8agrxh3jy")))

(define-public crate-kubizone-common-0.2.0 (c (n "kubizone-common") (v "0.2.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xxdncmdnvrbagnkcg95ywkcpap6csg3kfgm9wvm03bn1ml0s24g")))

(define-public crate-kubizone-common-0.3.0 (c (n "kubizone-common") (v "0.3.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "1573mbfkj7ngk2w8jhhs8q436nz0x7xjjgd6czlq0il2vy83kp7l")))

(define-public crate-kubizone-common-0.3.1 (c (n "kubizone-common") (v "0.3.1") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wyqqyg2fqi0c6j1wrx0fdvjlblw71lpip1cm99nlp8rpp09ll8n")))

(define-public crate-kubizone-common-0.4.0 (c (n "kubizone-common") (v "0.4.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ws7z33j2hib19hivvfpw5xddx66yp5wz3zv2anzk56m5dyga96d")))

(define-public crate-kubizone-common-0.4.1 (c (n "kubizone-common") (v "0.4.1") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mzkxmp6cpdlvwkyd8nlrwpn5w5ygrbaxfg3bbrjr6bwdmvy8cns")))

(define-public crate-kubizone-common-0.4.2 (c (n "kubizone-common") (v "0.4.2") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w76817f54lwvmy5cz8w3zhywd0qbbk7y2x0p2ffswly07jkg64i")))

(define-public crate-kubizone-common-0.4.3 (c (n "kubizone-common") (v "0.4.3") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "10p8m25vk1zgib2aci23q78n6w0kvijciih0w3ccs6p4m41ya4sj")))

(define-public crate-kubizone-common-0.5.0 (c (n "kubizone-common") (v "0.5.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zwsp21kj2d38pi6as32dlf7bg6cy2a357jyv81xv35b538611q0")))

(define-public crate-kubizone-common-0.6.0 (c (n "kubizone-common") (v "0.6.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01qx1vc762i9qzhr7jz4kfkc4npdbmfdy966q1yq7a436cpxph10")))

(define-public crate-kubizone-common-0.7.0 (c (n "kubizone-common") (v "0.7.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1svkzx94lxfd9xhlvsd7fwkcf5w1dhi4p7mabwac8rwrqwas1kvs")))

(define-public crate-kubizone-common-0.7.1 (c (n "kubizone-common") (v "0.7.1") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vvfhshya9183l6cf9bdlswbknfmw3kw44ryddzh2khlk3fr3zyq")))

(define-public crate-kubizone-common-0.7.2 (c (n "kubizone-common") (v "0.7.2") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02r0ly3q7jv55m9a7dgbdz03dcd9myr34pnr9ywixldlf49aw88c")))

(define-public crate-kubizone-common-0.7.3 (c (n "kubizone-common") (v "0.7.3") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fcj2mdq45g25bd07yl0vkcrrsnksp21izcdalv4j3dysl5b6w7q")))

(define-public crate-kubizone-common-0.7.4 (c (n "kubizone-common") (v "0.7.4") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "076v3dcxr3fgrfr67wi3vvfynn187cfcm3zndkxhynijpbjiq944")))

(define-public crate-kubizone-common-0.7.5 (c (n "kubizone-common") (v "0.7.5") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wam3r8kfizlvj12v621fbjydirmazsvhydrpfb3nzb5zph5i1mx")))

(define-public crate-kubizone-common-0.8.0 (c (n "kubizone-common") (v "0.8.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1grabrsb81cv16ns6kfmd4qzgwdg9bb437nhj69ikkvzjp0ywbj8")))

(define-public crate-kubizone-common-0.9.0 (c (n "kubizone-common") (v "0.9.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gwg8cfdy4ririp7if9iigfaibml3jm49a3z34ziiqvgb157k08r")))

(define-public crate-kubizone-common-0.10.0 (c (n "kubizone-common") (v "0.10.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "159y8cj551kc2p5j22n21hja2ydq6w4bs0r3j0b6ahy4hfj3izw1")))

(define-public crate-kubizone-common-0.10.1 (c (n "kubizone-common") (v "0.10.1") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qf98hxj8yfq3127r86cpdcz2cj6dj6fy1qh53wplbblw4i2zkqs")))

(define-public crate-kubizone-common-0.11.0 (c (n "kubizone-common") (v "0.11.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "066ak1hchfwgr618rzmpma32k84xrqsybs80mmcdjvybjca1vhwi")))

(define-public crate-kubizone-common-0.12.0 (c (n "kubizone-common") (v "0.12.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xs3y2ny5jcxwn9h7x7yrh0dm116wflmwajkxb5rlcdygc55garw")))

(define-public crate-kubizone-common-0.13.0 (c (n "kubizone-common") (v "0.13.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kxfpqhhb2ss98l5fgwdz41chy5kzlhcb3mm0wbamr2jmf91dr9a")))

