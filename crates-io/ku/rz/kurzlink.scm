(define-module (crates-io ku rz kurzlink) #:use-module (crates-io))

(define-public crate-kurzlink-1.0.0 (c (n "kurzlink") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "minijinja") (r "^0.32.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.14") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "13lhvvg5k4pw0lqniyrabdac0m4mdgya6jjqs96xbcqw5czg7xkn")))

