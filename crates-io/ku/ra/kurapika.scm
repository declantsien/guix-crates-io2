(define-module (crates-io ku ra kurapika) #:use-module (crates-io))

(define-public crate-kurapika-0.1.0 (c (n "kurapika") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rsa") (r "^0.6.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0qdbvqb0c27d742bvvp7lyblaggpad9h785791cmxwx7rc56az2w") (y #t)))

(define-public crate-kurapika-0.1.1 (c (n "kurapika") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rsa") (r "^0.6.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "106akrb3hgphwdmcw73r0i93l9qdvjqmh2hghahdfwbkhr21inkz")))

