(define-module (crates-io ku mi kumitateru_pdk) #:use-module (crates-io))

(define-public crate-kumitateru_pdk-0.5.0 (c (n "kumitateru_pdk") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "libloading") (r "0.7.*") (d #t) (k 0)))) (h "0i9gwwkczvb5n6x41yfhc4hz6djm5b2pchawqk5sqs0qdhxv6mk3") (y #t)))

(define-public crate-kumitateru_pdk-0.5.1 (c (n "kumitateru_pdk") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "libloading") (r "0.7.*") (d #t) (k 0)))) (h "0zz92drw5w4m5943xyb6dzxqk18v438cnvsgspckrx6ygvva5x59") (y #t)))

(define-public crate-kumitateru_pdk-0.5.2 (c (n "kumitateru_pdk") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "libloading") (r "0.7.*") (d #t) (k 0)))) (h "1ih5vppnhiwj9zrca35gqlzab3fhmp164b6zyf8mfsj98ppw3j42") (y #t)))

(define-public crate-kumitateru_pdk-0.5.3 (c (n "kumitateru_pdk") (v "0.5.3") (d (list (d (n "clap") (r "2.33.*") (d #t) (k 0)))) (h "10mzi8ziys9m6g4ygj9mi3j6xj0aa3b2zri0yzkzvkwmgwx1hm6z")))

