(define-module (crates-io ku do kudo) #:use-module (crates-io))

(define-public crate-kudo-0.1.0 (c (n "kudo") (v "0.1.0") (h "0mp3jbvrl87xqxmxzbzni6v3xa2gly6gkwdryfd53hhph9b1f6ai")))

(define-public crate-kudo-0.2.0 (c (n "kudo") (v "0.2.0") (h "1ckngbcnjdqdhr5lmfpc192k6s3169h28bqpza8l19p0vpbf3md3")))

(define-public crate-kudo-0.3.0 (c (n "kudo") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1insfgbmr7yl419js9gck07azwk1qy0gj4016kn4jr4bb667figc")))

(define-public crate-kudo-0.4.0 (c (n "kudo") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0522ak73bhnwmn3k7sbw7rb3h9wbymv1p3231m4m0m312ml0dn25")))

