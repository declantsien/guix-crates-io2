(define-module (crates-io ku sa kusabira) #:use-module (crates-io))

(define-public crate-kusabira-0.1.0-alpha0 (c (n "kusabira") (v "0.1.0-alpha0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "system-deps") (r "^6.1") (d #t) (k 0)))) (h "0hilqcicvlrv8nk7r3nsfdhmzgc7z2wvrbb99cdz34lh0wq6xbms")))

(define-public crate-kusabira-0.1.0-alpha1 (c (n "kusabira") (v "0.1.0-alpha1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "system-deps") (r "^6.1") (d #t) (k 0)))) (h "1z326bkad8lvh59p0f1hwv21pqw7kr0xnasgqhmdi811pk5x3qp1")))

(define-public crate-kusabira-0.1.0 (c (n "kusabira") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "system-deps") (r "^6.1") (d #t) (k 0)))) (h "0yj4ldyk2sm486jv9x88fa997a1aif6v0q10bfb4x9kh5hlhzvnr")))

(define-public crate-kusabira-0.1.1 (c (n "kusabira") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "system-deps") (r "^6.1") (d #t) (k 0)))) (h "0xmii3kni6gsbmm78mjp95ah3qr64r7anpld1jyg0xyw6cl2cgr6")))

