(define-module (crates-io ku ai kuaidaili) #:use-module (crates-io))

(define-public crate-kuaidaili-0.1.0 (c (n "kuaidaili") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1mk8c6yv5m5yz0gqzj8l7mkf2i657g34jn8dvrpikj9rc668v74i") (f (quote (("simple") ("default" "simple"))))))

