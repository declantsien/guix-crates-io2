(define-module (crates-io ku bo kubos-app) #:use-module (crates-io))

(define-public crate-kubos-app-0.1.0 (c (n "kubos-app") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "juniper") (r "^0.11") (d #t) (k 2)) (d (n "kubos-system") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "log4rs-syslog") (r "^3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0s8igy4w8hd4fr7mian1zkfla693sdcdnw448cm7dpavxjxzdkv9")))

