(define-module (crates-io ku bo kubos-system) #:use-module (crates-io))

(define-public crate-kubos-system-0.1.0 (c (n "kubos-system") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.3") (d #t) (k 0)) (d (n "log4rs-syslog") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0p3cw25frxmdnr8jybrh4v7087awa5kmsshaaahxszkp2gfbhlj5")))

