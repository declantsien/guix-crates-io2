(define-module (crates-io ku sh kushn) #:use-module (crates-io))

(define-public crate-kushn-0.1.0 (c (n "kushn") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "06k8bgxn6svb900fd0jcspidyfqcajv0bawy340s81q522gv24dv")))

(define-public crate-kushn-0.1.1 (c (n "kushn") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1nzpds2wdsfp8dcz1znblxkrbb3izc4340j4cq4s7hzp8gfim768")))

(define-public crate-kushn-0.1.2 (c (n "kushn") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "03xdcvdpbv07mz54whjjmv5bxpavql5gicjdvgrdr2hrc1jpbnvs")))

(define-public crate-kushn-0.1.3 (c (n "kushn") (v "0.1.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "197q1y47r382a19ga4cpjnbqb1wqns20jbkn57hwsa5glgi08rmw")))

