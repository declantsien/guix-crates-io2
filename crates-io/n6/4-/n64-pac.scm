(define-module (crates-io n6 #{4-}# n64-pac) #:use-module (crates-io))

(define-public crate-n64-pac-0.1.0 (c (n "n64-pac") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.5") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-bitfield") (r "^0.2") (d #t) (k 0)))) (h "1rdh7ysaj7ng3iaa7xpa40yk9fvndr0cmnw7ivahs7z1a9n46ps0")))

(define-public crate-n64-pac-0.1.1 (c (n "n64-pac") (v "0.1.1") (d (list (d (n "num_enum") (r "^0.5") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-bitfield") (r "^0.2") (d #t) (k 0)))) (h "0nlw3rkm0ai41k2cqcx558bxcjp4bq3wlrr32vma5qv549av4rm8")))

(define-public crate-n64-pac-0.2.0 (c (n "n64-pac") (v "0.2.0") (d (list (d (n "num_enum") (r "^0.5") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-bitfield") (r "^0.2") (d #t) (k 0)))) (h "16pxhj25zl0zkjwqhywknzs7ks61ysn1jnzagph1zzzc62qb5hax")))

(define-public crate-n64-pac-0.2.1 (c (n "n64-pac") (v "0.2.1") (d (list (d (n "num_enum") (r "^0.5") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-bitfield") (r "^0.2") (d #t) (k 0)))) (h "1wvrbl4bgjh7nx2jwqzgnx2d36c65pgc97klgl90q8kpc9wpcizj")))

(define-public crate-n64-pac-0.2.2 (c (n "n64-pac") (v "0.2.2") (d (list (d (n "num_enum") (r "^0.5") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-bitfield") (r "^0.2") (d #t) (k 0)))) (h "1qnpfagkxjjcqjrkrvx27xg8i508m2yp3n7gi06qfdcw5pwp6qaj")))

(define-public crate-n64-pac-0.3.0 (c (n "n64-pac") (v "0.3.0") (d (list (d (n "num_enum") (r "^0.5") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-bitfield") (r "^0.2") (d #t) (k 0)))) (h "09n5xq92ivspynmsabpbbdhvy8p23770faqj26qr93xlb1z7jn33") (y #t)))

(define-public crate-n64-pac-0.3.1 (c (n "n64-pac") (v "0.3.1") (d (list (d (n "num_enum") (r "^0.5") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-bitfield") (r "^0.2") (d #t) (k 0)))) (h "05kf96nx5z6b5sjd9bzm8labrdmvajniwzfvhvajsn70xab0dn2a")))

(define-public crate-n64-pac-0.3.2 (c (n "n64-pac") (v "0.3.2") (d (list (d (n "num_enum") (r "^0.5") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-bitfield") (r "^0.2") (d #t) (k 0)))) (h "1jg408x642l24vkqgj1x723gzg0mz5v7y7rzlnb3hj20lp311rzj")))

(define-public crate-n64-pac-0.3.3 (c (n "n64-pac") (v "0.3.3") (d (list (d (n "num_enum") (r "^0.5") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proc-bitfield") (r "^0.2") (d #t) (k 0)))) (h "02aqanl51gpk6v8m1lsfn5bxjibhlyfkgj1anlf8jhni2ak73a47")))

