(define-module (crates-io n6 #{4r}# n64romconvert) #:use-module (crates-io))

(define-public crate-n64romconvert-0.1.0-alpha (c (n "n64romconvert") (v "0.1.0-alpha") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1bdwa0z8bz5drf8j0nbwcvidmn3w53chyvi4drmn4l4ws51bbv84") (y #t)))

(define-public crate-n64romconvert-0.1.1 (c (n "n64romconvert") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "08qn2n1ak3h3sgr5i7465pvjy4533qxmqr47mg4gm2nf0mglpgfg") (y #t)))

(define-public crate-n64romconvert-1.0.0 (c (n "n64romconvert") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "17n2vj7b3hj08yy9q3qrfgqfr1syid45861riafdnj4j5amx9kwg")))

(define-public crate-n64romconvert-1.0.1 (c (n "n64romconvert") (v "1.0.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (k 0)))) (h "0g12s67arf03sl89jp6nx1crqnsjjqjk3dwi5f1wqvnfq6ilxi30") (y #t)))

(define-public crate-n64romconvert-1.0.2 (c (n "n64romconvert") (v "1.0.2") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (k 0)))) (h "1w0jpj9pcqaz5c1f7x48rbcjb9hgq2f07w7ksjg16lv29whl7nil")))

