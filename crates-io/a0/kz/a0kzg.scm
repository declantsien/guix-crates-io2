(define-module (crates-io a0 kz a0kzg) #:use-module (crates-io))

(define-public crate-a0kzg-0.1.0 (c (n "a0kzg") (v "0.1.0") (d (list (d (n "bls12_381") (r "^0.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0zy25c6f7c41iapxpd05hmf356hcgr15fxxr62n914lvh7dwwkw8") (y #t)))

(define-public crate-a0kzg-0.1.1 (c (n "a0kzg") (v "0.1.1") (d (list (d (n "bls12_381") (r "^0.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1j71q198jjvwmcrx77rb57v32cgrg7bax5i8c6r7pc725qaq8nhq") (y #t)))

