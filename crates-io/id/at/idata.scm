(define-module (crates-io id at idata) #:use-module (crates-io))

(define-public crate-idata-0.1.0 (c (n "idata") (v "0.1.0") (h "1v5hwphrmgrqzi4cja0qcm6yw5pcb6hp9hnv2yqqv5a1qf85ya34")))

(define-public crate-idata-0.1.2 (c (n "idata") (v "0.1.2") (h "0xqn3jvpp2vpcs06i55903c86s44ai2xfnjraf3rj6fscc2zj7zc")))

