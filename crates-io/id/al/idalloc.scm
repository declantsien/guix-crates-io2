(define-module (crates-io id al idalloc) #:use-module (crates-io))

(define-public crate-idalloc-0.1.0 (c (n "idalloc") (v "0.1.0") (h "18yy8bglknlgw5k1s3lsvzp457jaaha4r2963gfnlapa5srpig09")))

(define-public crate-idalloc-0.1.1 (c (n "idalloc") (v "0.1.1") (h "1a73z9ir7dwz6q1dsj8xd8xqwijwykz0jgrsiwxm99mnsgvhg7jm")))

