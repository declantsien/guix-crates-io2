(define-module (crates-io id en identified_vec_macros) #:use-module (crates-io))

(define-public crate-identified_vec_macros-0.1.5 (c (n "identified_vec_macros") (v "0.1.5") (d (list (d (n "identified_vec") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1wr9hb4a3whhpfapyz49m8363alhwaagmc8bfl3ncnin52a13bq7") (y #t)))

