(define-module (crates-io id en ident) #:use-module (crates-io))

(define-public crate-ident-0.1.0 (c (n "ident") (v "0.1.0") (h "01xmb4y6l2rprgxm4mz9hi3jlrpaw41zz9yrvdsdm93ds5i714mh")))

(define-public crate-ident-0.1.1 (c (n "ident") (v "0.1.1") (h "168fvc6mkc9rcfrxgryyrxkf3ganhmgii0jir11199vxf6kkc510")))

(define-public crate-ident-0.1.2 (c (n "ident") (v "0.1.2") (h "1l56xc77b81z9hjkmfmmxcj83i03wb7xi36415p56p87n6ygp3k0")))

(define-public crate-ident-0.1.3 (c (n "ident") (v "0.1.3") (h "1srlv8rs3vl9swb1cssi4zfrbbb412iwcd3zby97wf89fq02v7nx")))

(define-public crate-ident-0.1.4 (c (n "ident") (v "0.1.4") (h "14kv90dmj9icbp3111mmgazi1f8ysj834yvckyyz4s6z3yg4j12z")))

(define-public crate-ident-0.2.5 (c (n "ident") (v "0.2.5") (h "079yvm3ri5sc0pm9x2d9y4vrp6pvzmwcp9vgvbpsqp2v05yhw8vs")))

(define-public crate-ident-0.2.6 (c (n "ident") (v "0.2.6") (h "14s8cgiqh6l6fg41i32mjzw4y0kp4g4lnbzmpzrixf2yy6k42pkj")))

(define-public crate-ident-0.2.8 (c (n "ident") (v "0.2.8") (h "0n88zipzbwah4qagw27l42w75dyhh50dpm83r879fnvc7xv8hz10")))

(define-public crate-ident-0.3.9 (c (n "ident") (v "0.3.9") (h "15f5blk9zgnl797kg1n8gwhjwpdxqajhiyv8dpjvcpm35n6swwy0")))

(define-public crate-ident-0.3.10 (c (n "ident") (v "0.3.10") (h "1ailpn0kh2wih1kd1rrn7amxwqkr8rajgf83v7jz4wrcj1fmfhlq")))

