(define-module (crates-io id en identitycard) #:use-module (crates-io))

(define-public crate-identitycard-0.1.0 (c (n "identitycard") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1ikwalvzzg8p29k3lbrkq0hc1vdiw7i7jil9anrwrp2b1j34yqzb")))

(define-public crate-identitycard-0.1.1 (c (n "identitycard") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0c46x3np99yp8ln47lgzafg9ydi97n2p58zglj37fani8iq6q9f5")))

(define-public crate-identitycard-0.1.2 (c (n "identitycard") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1r8v63slncn8zcf4qbbr78g5g2yn0250ifyjbv18ivh2whkwj7jq")))

(define-public crate-identitycard-0.1.4 (c (n "identitycard") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1bphrx3kyj212zl9pzl7dh5bl7n0vnb1lc37lj3pbfglnswiwy5p")))

(define-public crate-identitycard-0.1.5 (c (n "identitycard") (v "0.1.5") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0i2sxvb8a72lzxlyd5dxx35rg252v2lmlzjq4jq7019mp3wc42b6")))

(define-public crate-identitycard-0.1.6 (c (n "identitycard") (v "0.1.6") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1h3rpj9wxasq0zrpi1ackzyw5na1nzk27r77yfvjmqdrqz9w3f6m")))

(define-public crate-identitycard-0.1.7 (c (n "identitycard") (v "0.1.7") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1rm314w6sfwmhdz6yrjqiaz2cbr4kk94hwxpqmpfsdkclwsr6yhv")))

