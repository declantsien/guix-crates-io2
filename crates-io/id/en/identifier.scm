(define-module (crates-io id en identifier) #:use-module (crates-io))

(define-public crate-identifier-0.1.0 (c (n "identifier") (v "0.1.0") (d (list (d (n "identifier_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1c2kcbj0b1w3mdszpx0z7rn14lbf03rbnr0xyc9vws5vib5a9y2q")))

(define-public crate-identifier-0.1.1 (c (n "identifier") (v "0.1.1") (d (list (d (n "identifier_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0c8k27ziixxpr22v5xvsjlz60ly0ykm1hqd0cis31z5f3k55pvww")))

(define-public crate-identifier-0.1.2 (c (n "identifier") (v "0.1.2") (d (list (d (n "identifier_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1jz71lqaprcgwg7a2rbaafkyn082a3hz2ycpwi9z1my2wxg4v8r8")))

(define-public crate-identifier-0.1.3 (c (n "identifier") (v "0.1.3") (d (list (d (n "identifier_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1wv761qg35z2n0978gjlb3cr88vs60grizxyidkdhfcrzxhlav9r")))

