(define-module (crates-io id en ident_case_conversions) #:use-module (crates-io))

(define-public crate-ident_case_conversions-1.0.0 (c (n "ident_case_conversions") (v "1.0.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (o #t) (d #t) (k 0)) (d (n "test-case") (r "^3.2.1") (d #t) (k 2)))) (h "10fqlwxrijx32f4ghvjcg7yly8dlkahvv9yw7fbfgjzvi18c9p3a") (f (quote (("default" "syn"))))))

