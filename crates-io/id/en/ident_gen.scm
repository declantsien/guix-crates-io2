(define-module (crates-io id en ident_gen) #:use-module (crates-io))

(define-public crate-ident_gen-0.1.0 (c (n "ident_gen") (v "0.1.0") (h "0q5c240wacrry95gsmhg4r44afwrxb7gfqv08ijh562sln0w80xj") (y #t)))

(define-public crate-ident_gen-0.1.1 (c (n "ident_gen") (v "0.1.1") (h "0vq79q4dc90qp0yj6g9ygndp39najga5870vdd639cwsbv0ay64z")))

(define-public crate-ident_gen-0.1.2 (c (n "ident_gen") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zg099sijfwj6h5m6nwmakwz954x55fmrxpjv18kjf9ys3h5wgph") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde" "std"))))))

