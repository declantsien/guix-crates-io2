(define-module (crates-io id en identicons-svg) #:use-module (crates-io))

(define-public crate-identicons-svg-0.1.0 (c (n "identicons-svg") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "random_color") (r "^0.6.1") (d #t) (k 0)) (d (n "simple-xml-builder") (r "^1.1.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "1bmnnnzx3ilfzmnzjg6kk7pml9vh4kwqdqnzznrmkpmpcb1y24zr") (s 2) (e (quote (("show-icon" "dep:webbrowser"))))))

