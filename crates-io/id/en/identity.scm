(define-module (crates-io id en identity) #:use-module (crates-io))

(define-public crate-identity-0.0.1 (c (n "identity") (v "0.0.1") (h "1lkwnqxli4yqhvh34gvm9rgdjysy2gxqkc8f97g59f3mwp4x3hx8")))

(define-public crate-identity-0.0.2 (c (n "identity") (v "0.0.2") (h "17idv1ap6b6xzbs9nh6hcdncd75sh732k8kjps09k7pdq52s7p45")))

(define-public crate-identity-0.0.3 (c (n "identity") (v "0.0.3") (h "1gmp98lfr9s09y2w7phsbscdmj1a5s0q5vzck4q0nly01lv3dskr")))

(define-public crate-identity-0.0.4 (c (n "identity") (v "0.0.4") (h "1fqcayi76vhwbn85nqlymzssdsk45hml5m8jr2rawlm38qh8w58w")))

(define-public crate-identity-0.0.5 (c (n "identity") (v "0.0.5") (h "04mx2dg0zzidmk86j28iamw58p19v617qp8igdkwhi6yrxzbp0l9")))

(define-public crate-identity-0.0.6 (c (n "identity") (v "0.0.6") (h "0ngr8r7iv18irg118p923k3bmq0qz7cy7k00z863i42haxf71dqk")))

