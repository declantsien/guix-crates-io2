(define-module (crates-io id en identicon-rs) #:use-module (crates-io))

(define-public crate-identicon-rs-0.1.0 (c (n "identicon-rs") (v "0.1.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "00llbrgrbfjm5vrcms9pil0kv9ycjc51k7f6a2k5w2g4sqp04qfk")))

(define-public crate-identicon-rs-0.1.1 (c (n "identicon-rs") (v "0.1.1") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "16yf0b1a6i1xshp2laicn5xjycv2zlj4hm0w1g439j6lxmf7jnkb")))

(define-public crate-identicon-rs-0.2.0 (c (n "identicon-rs") (v "0.2.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1s085p6sgqrykrn1fq710jn4cq7alj6mw1q95g6fn0i5j1pda09r")))

(define-public crate-identicon-rs-0.2.1 (c (n "identicon-rs") (v "0.2.1") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1n6ai3h90fj235681i0s7f6222z5wcha3bnp38w9w0i20c3rs6jv")))

(define-public crate-identicon-rs-0.2.2 (c (n "identicon-rs") (v "0.2.2") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1mj33rpv7rz8ybar8gw4xgjg25vsd03mj89sh19755prhm7hxrww")))

(define-public crate-identicon-rs-1.0.0 (c (n "identicon-rs") (v "1.0.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "15bhp3ms25i5yx9ky7xckzif8nf2g6y7l35320qpyc5bsvbgi15y")))

(define-public crate-identicon-rs-1.0.1 (c (n "identicon-rs") (v "1.0.1") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "196iqg8mp715ifjs2ml326r4s8ww58dazsvmspx7i110r8mbx86z")))

(define-public crate-identicon-rs-1.0.2 (c (n "identicon-rs") (v "1.0.2") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1ixg092fw59y0i4za3247ikyjg2337p3vgxnzzcr5dbl213y3bzr")))

(define-public crate-identicon-rs-1.2.0 (c (n "identicon-rs") (v "1.2.0") (d (list (d (n "actix-web") (r "^0.7") (d #t) (k 2)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "18bpqi6h328mmgwzjkcnw286bd7d3vpx08hzv8n93m9csxjd3a6w")))

(define-public crate-identicon-rs-1.2.1 (c (n "identicon-rs") (v "1.2.1") (d (list (d (n "actix-web") (r "^0.7") (d #t) (k 2)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0n8gm0gakd9snh2ff7by9v0jcprcwh7j6gckpmlypx1mjggz2hrb")))

(define-public crate-identicon-rs-1.3.0 (c (n "identicon-rs") (v "1.3.0") (d (list (d (n "actix-web") (r "^0.7") (d #t) (k 2)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1jqfq42dk4gxrbzb2p3xp5qv1h91y93y11s6mhfjl20gbpfzaasb")))

(define-public crate-identicon-rs-1.4.0 (c (n "identicon-rs") (v "1.4.0") (d (list (d (n "actix-web") (r "^1.0.0") (d #t) (k 2)) (d (n "image") (r "^0.22.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1s8z973yjakkshlcmzq8bhpgi142318z1hkx6ay1g1awnra9svgb")))

(define-public crate-identicon-rs-1.4.1 (c (n "identicon-rs") (v "1.4.1") (d (list (d (n "actix-web") (r "^1.0.0") (d #t) (k 2)) (d (n "image") (r "^0.22.5") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)))) (h "09im7fpzs3y5mfskjyvl84l9sra35nrcq7k7hzrkd75qi0121xy2")))

(define-public crate-identicon-rs-2.0.0 (c (n "identicon-rs") (v "2.0.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 2)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1n8y43785f9xwh6dy0ji06pnjsr3kybh5llk7l9likyrjqa2hp2w")))

(define-public crate-identicon-rs-2.0.1 (c (n "identicon-rs") (v "2.0.1") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 2)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0552banwg5vjvhhacs4xj93m4pnzlc1jxpwbznsw9prw7fk527yx")))

(define-public crate-identicon-rs-2.0.2 (c (n "identicon-rs") (v "2.0.2") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 2)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 2)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0rs3rrixnv741q4bchijhn5b3vnlic32ckgd8mmc9rvvr97lvs6f")))

(define-public crate-identicon-rs-2.1.0 (c (n "identicon-rs") (v "2.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0sbdnq06wc4qnycbm659clz1syx932al5fglnriwxh22h6k9z721")))

(define-public crate-identicon-rs-2.1.1 (c (n "identicon-rs") (v "2.1.1") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0xdh4vf5mafx6p2fwz90j5silgzcsn5zkqb1ffic1axsvp9wh864")))

(define-public crate-identicon-rs-2.2.0 (c (n "identicon-rs") (v "2.2.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0cj7hjrpfgvy0k2d01asd585z49990fm2wwkq207dm7hw83g2r39")))

(define-public crate-identicon-rs-2.3.0 (c (n "identicon-rs") (v "2.3.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ds53yc2myvw2n8zhxbxpfnvahqd87ng12nha9jxsc6r0mkpvz3v")))

(define-public crate-identicon-rs-3.0.0 (c (n "identicon-rs") (v "3.0.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0zfp47n3m4jbbypbkizhs1w3sf0d20zy9r9wx9p32chnjxn7rcdw")))

(define-public crate-identicon-rs-3.1.0 (c (n "identicon-rs") (v "3.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0cy00jq54xwpfl25y4g3kgscjvf42a0d0r3biv98w9k0ai3qahw6")))

(define-public crate-identicon-rs-3.1.1 (c (n "identicon-rs") (v "3.1.1") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0jja935ksnzhwcjrjcjs3d9iibmvndh8phic9aimllsfv3d5c1ir")))

(define-public crate-identicon-rs-3.1.2 (c (n "identicon-rs") (v "3.1.2") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "handlebars") (r "^4.3") (d #t) (k 1)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0b79pr78r5sxkz64ykv8ps1szjbj61f91ww3gxg398ba0w9ifjw8")))

(define-public crate-identicon-rs-3.1.3 (c (n "identicon-rs") (v "3.1.3") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "handlebars") (r "^4.3") (d #t) (k 1)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "11nf6f96ayrs3ym4dfivzssrc27qms2c6kz6y63rk38w8s69jiwy")))

(define-public crate-identicon-rs-3.1.4 (c (n "identicon-rs") (v "3.1.4") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1gqhbxf9633rf535j9jl6hr9479sxcphh248mrv15yh9vahk8svf")))

(define-public crate-identicon-rs-3.1.5 (c (n "identicon-rs") (v "3.1.5") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "094c75h15116lng3dkxf60iw2r0n7pdaz96kgzmivh4iavwygqk3")))

(define-public crate-identicon-rs-4.0.0 (c (n "identicon-rs") (v "4.0.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1n55hj8ig291797xgbv6qi6xn8z0l3riws0b4v6pafg4qfbap1jq")))

(define-public crate-identicon-rs-4.0.1 (c (n "identicon-rs") (v "4.0.1") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1syvlmkp1jdk29qpd3x9lmlfg32f67ddj33lg8zjmdzibhaxz8m9")))

(define-public crate-identicon-rs-4.0.2 (c (n "identicon-rs") (v "4.0.2") (d (list (d (n "axum") (r "^0.7") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1b783aimlla30f8i3fkvm70z915ifqka1algj0vyyydc1ylmjxfa")))

(define-public crate-identicon-rs-4.0.3 (c (n "identicon-rs") (v "4.0.3") (d (list (d (n "axum") (r "^0.7") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1z98axh7x912cai0fglhvfglp8kypdivval1a57fkb2vm762cw05")))

(define-public crate-identicon-rs-5.0.0 (c (n "identicon-rs") (v "5.0.0") (d (list (d (n "axum") (r "^0.7") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "05zpvd28k9bdg8iv7z6iysrxvd6hgkql0rmhpip264dyfk42cvfy")))

