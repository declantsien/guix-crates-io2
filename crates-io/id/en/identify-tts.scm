(define-module (crates-io id en identify-tts) #:use-module (crates-io))

(define-public crate-identify-tts-0.1.0 (c (n "identify-tts") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.15") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "0yw71s286paiyzjxk0r7k1ix850w8nh1pdvl7xswlgibr0b2if6n")))

