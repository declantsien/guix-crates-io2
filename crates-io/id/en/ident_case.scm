(define-module (crates-io id en ident_case) #:use-module (crates-io))

(define-public crate-ident_case-1.0.0 (c (n "ident_case") (v "1.0.0") (h "1apaj74brasb7wmmp2qm3fqk1i7dyvgdmlki43cjwvv6iqc2d61w")))

(define-public crate-ident_case-1.0.1 (c (n "ident_case") (v "1.0.1") (h "0fac21q6pwns8gh1hz3nbq15j8fi441ncl6w4vlnd1cmc55kiq5r")))

