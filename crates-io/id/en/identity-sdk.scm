(define-module (crates-io id en identity-sdk) #:use-module (crates-io))

(define-public crate-identity-sdk-0.1.0 (c (n "identity-sdk") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "axum-core") (r "^0.3.0") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)))) (h "03sh16msf3dv4mxsibch32zlxmvjmy2rq4pqjf8cl40bs45vaf4g")))

