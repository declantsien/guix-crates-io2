(define-module (crates-io id en identicode) #:use-module (crates-io))

(define-public crate-identicode-0.1.0 (c (n "identicode") (v "0.1.0") (h "12zzg52py61mss8z4899skj06rdlw8c3b3flkh32kib3a7csa8a4")))

(define-public crate-identicode-0.2.0 (c (n "identicode") (v "0.2.0") (h "0lnw5fk0836ayzjvlgq89rr25b95pya8lxcv56n8h613lpbsid3v")))

(define-public crate-identicode-0.3.0 (c (n "identicode") (v "0.3.0") (h "1y27n0rc4wyzl9340dcidbsryy3xv21swwr69ri7194x8gqqn3l0")))

