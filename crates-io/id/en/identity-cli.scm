(define-module (crates-io id en identity-cli) #:use-module (crates-io))

(define-public crate-identity-cli-0.1.0 (c (n "identity-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1sgx7f0sj894arp6fla56lzscvhkqgd07c0xljziih7jmhb027nc")))

