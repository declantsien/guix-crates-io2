(define-module (crates-io id en identity_eddsa_verifier) #:use-module (crates-io))

(define-public crate-identity_eddsa_verifier-0.7.0-alpha.8 (c (n "identity_eddsa_verifier") (v "0.7.0-alpha.8") (d (list (d (n "identity_jose") (r "=0.7.0-alpha.8") (k 0)) (d (n "iota-crypto") (r "^0.23") (f (quote ("std"))) (k 0)))) (h "18d0sknv15y12v2bx20dmdby1czg4pkirq9m6wp7688h7srhbwwn") (f (quote (("ed25519" "iota-crypto/ed25519") ("default" "ed25519")))) (r "1.65")))

(define-public crate-identity_eddsa_verifier-1.0.0-rc.1 (c (n "identity_eddsa_verifier") (v "1.0.0-rc.1") (d (list (d (n "identity_jose") (r "=1.0.0-rc.1") (k 0)) (d (n "iota-crypto") (r "^0.23") (f (quote ("std"))) (k 0)))) (h "0yvjxqf5isbnxcxg0g8sf6l7l5vh73c6fxjxlk0pawxqya5zwr25") (f (quote (("ed25519" "iota-crypto/ed25519") ("default" "ed25519")))) (r "1.65")))

(define-public crate-identity_eddsa_verifier-1.0.0 (c (n "identity_eddsa_verifier") (v "1.0.0") (d (list (d (n "identity_jose") (r "=1.0.0") (k 0)) (d (n "iota-crypto") (r "^0.23") (f (quote ("std"))) (k 0)))) (h "10zkdv6f9j7i7f5dwk4bgqfgifl5grafrlbb991ai6n6majrxfi9") (f (quote (("ed25519" "iota-crypto/ed25519") ("default" "ed25519")))) (r "1.65")))

(define-public crate-identity_eddsa_verifier-1.1.0-alpha.1 (c (n "identity_eddsa_verifier") (v "1.1.0-alpha.1") (d (list (d (n "identity_jose") (r "=1.1.0-alpha.1") (k 0)) (d (n "iota-crypto") (r "^0.23") (f (quote ("std"))) (k 0)))) (h "1vzmnhwvhbka1l3bvh70lybqc4hfnr1xhzgvlbyr7v24p8zq8mzj") (f (quote (("ed25519" "iota-crypto/ed25519") ("default" "ed25519")))) (r "1.65")))

(define-public crate-identity_eddsa_verifier-1.1.0 (c (n "identity_eddsa_verifier") (v "1.1.0") (d (list (d (n "identity_jose") (r "=1.1.0") (k 0)) (d (n "iota-crypto") (r "^0.23") (f (quote ("std"))) (k 0)))) (h "10fs59mxv5ysybc6p3d75f898afgzv3p9xjxzcgkaf2ahgasjzl8") (f (quote (("ed25519" "iota-crypto/ed25519") ("default" "ed25519")))) (r "1.65")))

(define-public crate-identity_eddsa_verifier-1.1.1 (c (n "identity_eddsa_verifier") (v "1.1.1") (d (list (d (n "identity_jose") (r "=1.1.1") (k 0)) (d (n "iota-crypto") (r "^0.23") (f (quote ("std"))) (k 0)))) (h "0cwbrc06l4qf5pi70dddsc59x6gs4vvf2r0y78vmxxkl2vmf3z1f") (f (quote (("ed25519" "iota-crypto/ed25519") ("default" "ed25519")))) (r "1.65")))

(define-public crate-identity_eddsa_verifier-1.0.1 (c (n "identity_eddsa_verifier") (v "1.0.1") (d (list (d (n "identity_jose") (r "=1.0.1") (k 0)) (d (n "iota-crypto") (r "^0.23") (f (quote ("std"))) (k 0)))) (h "0vv1f6rq78422rsakml55cpn50fsfi619vc01mi75iz4kpy90p8h") (f (quote (("ed25519" "iota-crypto/ed25519") ("default" "ed25519")))) (r "1.65")))

(define-public crate-identity_eddsa_verifier-1.2.0 (c (n "identity_eddsa_verifier") (v "1.2.0") (d (list (d (n "identity_jose") (r "=1.2.0") (k 0)) (d (n "iota-crypto") (r "^0.23") (f (quote ("std"))) (k 0)))) (h "1sng4w02qdpf1q4w68zy8mhnnpvcfr3cb4wxhv6nwi9g0naff1w2") (f (quote (("ed25519" "iota-crypto/ed25519") ("default" "ed25519")))) (r "1.65")))

(define-public crate-identity_eddsa_verifier-1.3.0 (c (n "identity_eddsa_verifier") (v "1.3.0") (d (list (d (n "identity_jose") (r "=1.3.0") (k 0)) (d (n "iota-crypto") (r "^0.23") (f (quote ("std"))) (k 0)))) (h "0falrjyr9g2c7hpcjglsp1hi5z1gf831m5g23zfgkx49wdlsa7wm") (f (quote (("ed25519" "iota-crypto/ed25519") ("default" "ed25519")))) (r "1.65")))

