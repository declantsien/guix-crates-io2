(define-module (crates-io id en ident_concat) #:use-module (crates-io))

(define-public crate-ident_concat-0.1.0 (c (n "ident_concat") (v "0.1.0") (h "1j372lanv5zfd8rjc0gcsffb0i0ic0lavbshjsir675f3bk9832a")))

(define-public crate-ident_concat-0.2.0 (c (n "ident_concat") (v "0.2.0") (h "1d7aafp6z05d0r8hn54ib68z5jdfvciwrdiybfs7azraiszqj580")))

(define-public crate-ident_concat-0.2.1 (c (n "ident_concat") (v "0.2.1") (h "14h39axlh04m4xl41d1ckmx5kx324lzm5457vj2d5ca9437cs9hl")))

(define-public crate-ident_concat-0.2.2 (c (n "ident_concat") (v "0.2.2") (h "14j2yas4lflaqw969m2ax89qlggaxv3a8d43028j4lpdyxfjwrhq")))

(define-public crate-ident_concat-0.2.3 (c (n "ident_concat") (v "0.2.3") (h "04nlk0pdnz3kvfid7lghfjld658wwszywnw6y43in4llikxmjvq3")))

(define-public crate-ident_concat-0.3.0 (c (n "ident_concat") (v "0.3.0") (h "0bindr4jj5frvcnggz3gvhw39vb48p91dg4dy1744kbm3qs312lx")))

