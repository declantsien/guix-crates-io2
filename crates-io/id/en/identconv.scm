(define-module (crates-io id en identconv) #:use-module (crates-io))

(define-public crate-identconv-0.1.0 (c (n "identconv") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)))) (h "13h37xgbm72bb749mfyjvc5a6mcgwhb2ycz14hnnddqq0afghx50")))

(define-public crate-identconv-0.1.1 (c (n "identconv") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)))) (h "0a0g1adl7rskj6h0ac4bmhassrzb20vs3scmqagqf5bfax9gjiv5")))

(define-public crate-identconv-0.1.2 (c (n "identconv") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)))) (h "0cskv3jj6hnjg0jqk5cickcxh4q1x220xxdaish0k2r9nxx5yfcf") (y #t)))

(define-public crate-identconv-0.1.3 (c (n "identconv") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)))) (h "084w00as5zbxb8iziqf3n9pixq66jz62kshzxfprqccr51q7zip7")))

(define-public crate-identconv-0.2.0 (c (n "identconv") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "litrs") (r "^0.4") (d #t) (k 0)))) (h "03pr9yhw08a850bfwfin7qq2y94g2vmnmddivvwc2l3pz62k1pq2")))

