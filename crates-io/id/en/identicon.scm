(define-module (crates-io id en identicon) #:use-module (crates-io))

(define-public crate-identicon-0.0.3 (c (n "identicon") (v "0.0.3") (d (list (d (n "image") (r "~0.2") (d #t) (k 0)) (d (n "openssl") (r "~0.2") (d #t) (k 0)))) (h "1a3w5bi2zbqhg3d5rsks0z7751hn5nak6bzqyfq0qy7p5rjgcla4")))

(define-public crate-identicon-0.1.0 (c (n "identicon") (v "0.1.0") (d (list (d (n "image") (r "~0.3") (d #t) (k 0)) (d (n "openssl") (r "~0.6") (d #t) (k 0)))) (h "0zg8mjawy222h9ad0l6l25mqylrn9m1gaqbv252aflip4wk3nhaq")))

(define-public crate-identicon-0.1.1 (c (n "identicon") (v "0.1.1") (d (list (d (n "image") (r "~0.5") (d #t) (k 0)) (d (n "openssl") (r "~0.7") (d #t) (k 0)))) (h "125jmlffixr0sjms1ik6p5j7b9hmgvp9s919x0pv6v7svglbwqwh")))

(define-public crate-identicon-0.2.0 (c (n "identicon") (v "0.2.0") (d (list (d (n "image") (r "^0.23.14") (f (quote ("png"))) (k 0)) (d (n "md-5") (r "^0.9.1") (f (quote ("asm"))) (d #t) (k 0)))) (h "1j61l53qawccjnsm8j8199z75i5js3cvw6c12k5na2lfy4yxbmfh")))

(define-public crate-identicon-0.2.1 (c (n "identicon") (v "0.2.1") (d (list (d (n "image") (r "^0.23.14") (f (quote ("png"))) (k 0)) (d (n "md-5") (r "^0.9.1") (f (quote ("asm"))) (o #t) (d #t) (k 0)))) (h "14p956acfahrwhj0mlfsr3ckkgjljs7spw4l3a1d9az9dpa3pyjh") (f (quote (("default" "build-bin") ("build-bin" "md-5"))))))

