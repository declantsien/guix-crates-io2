(define-module (crates-io id en identity_function) #:use-module (crates-io))

(define-public crate-identity_function-0.1.0 (c (n "identity_function") (v "0.1.0") (h "0flmyy2k951msl325p17psmk4g3c9zjz51qc6xvjn0cz1v2z4h3g")))

(define-public crate-identity_function-0.1.1 (c (n "identity_function") (v "0.1.1") (h "0lzh6lsvnr4j3yxzv67878w57fkvz0yz6vjw9gjz0ab7514w5kkk")))

