(define-module (crates-io id en ident-util) #:use-module (crates-io))

(define-public crate-ident-util-1.0.0 (c (n "ident-util") (v "1.0.0") (h "06klad5r8gnvk6nhk9xgr2pxnj29dz9ab0xs6f63ha0mq6j1846n")))

(define-public crate-ident-util-1.0.1 (c (n "ident-util") (v "1.0.1") (h "0dg5liza6xg88jjaa56zdzgphxxzi8c48n4s8mn4k9f0nxnv42p5")))

