(define-module (crates-io id en identity-diff-derive) #:use-module (crates-io))

(define-public crate-identity-diff-derive-0.5.0 (c (n "identity-diff-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "1nqppyvs7wkfimbd2m2lv9ajksvvhpqy5niy264gvid8smjnrw4w")))

(define-public crate-identity-diff-derive-0.6.0 (c (n "identity-diff-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "0j13jcx5bna3f052lc0zkxnclkzvwy1qh99ldnlfas6llwnkra9f")))

(define-public crate-identity-diff-derive-0.6.1 (c (n "identity-diff-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "1glgkw4xmqghv027lv6gwbphgkp82pqx0dnhkqywy8gml4bnqpgm")))

(define-public crate-identity-diff-derive-0.7.0-alpha.1 (c (n "identity-diff-derive") (v "0.7.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "0r9ak1ki77fh4inv73dv9aadjawyngmw529mckc4ll78nsskgf49")))

(define-public crate-identity-diff-derive-0.7.0-alpha.2 (c (n "identity-diff-derive") (v "0.7.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "0j2zkw57j8fpv1fd1xpbz4azspz6mkmff3x6yya0ab7i79z7fya6")))

(define-public crate-identity-diff-derive-0.7.0-alpha.3 (c (n "identity-diff-derive") (v "0.7.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "107ggpnbgc88hsgwh0fvb1xpkxnv1lj38kxb7s9rrk9zrvgv417i")))

(define-public crate-identity-diff-derive-0.7.0-alpha.4 (c (n "identity-diff-derive") (v "0.7.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "1y5bdgppb9z6fj96jb0s069la65janx51ws4sg082455xjf7a7pn")))

(define-public crate-identity-diff-derive-0.7.0-alpha.5 (c (n "identity-diff-derive") (v "0.7.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "14z89c1qpi49dcaj142ria5rhjiijxy2l3npg5651jp6h4rn382n")))

(define-public crate-identity-diff-derive-0.7.0-alpha.6 (c (n "identity-diff-derive") (v "0.7.0-alpha.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "1zh2z68gjc41vh6qy7a4cyl4ndc4y738l54glsjq59ap94ga84z1")))

(define-public crate-identity-diff-derive-0.6.2 (c (n "identity-diff-derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "0dsgzmyly6x6p326mcmq8km7bq5jifx05fc5lvg9wak1liqln79k")))

(define-public crate-identity-diff-derive-0.6.3-rc.1 (c (n "identity-diff-derive") (v "0.6.3-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "1fgz1kns54zc6xlnssd5q77dn945fn26dcybwg38g9q43yd3sa5d")))

(define-public crate-identity-diff-derive-0.6.3 (c (n "identity-diff-derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "derive"))) (d #t) (k 0)))) (h "1wxbcmqq2y2v8v2fz51qf0v5040m1xr62qgrqh1fj0q6xdrjq3kb")))

