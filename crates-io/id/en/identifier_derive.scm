(define-module (crates-io id en identifier_derive) #:use-module (crates-io))

(define-public crate-identifier_derive-0.1.0 (c (n "identifier_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1w6s3f9ij8gmjjymkk69iqzbdd5fg2mp2f79njj53myjhc39hg30")))

(define-public crate-identifier_derive-0.1.1 (c (n "identifier_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h0ynx9nzhdf8hr1c9c3r9v3rwagwphqqvdnc4l7bjny9i54rizs")))

(define-public crate-identifier_derive-0.1.2 (c (n "identifier_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kb0m5fv9pfvdlfrcm6jx8hc5pjv3rxpc0h5k6bazvhh5ahsm4c3")))

(define-public crate-identifier_derive-0.1.3 (c (n "identifier_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1chqlisqfyhyarx2kzlkbafnd0bn2wpy6yz1iajkqx1bgkf9gyn2")))

