(define-module (crates-io id en identifiers) #:use-module (crates-io))

(define-public crate-identifiers-0.1.0 (c (n "identifiers") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "18iq27gz4ckcy90bm8h3gc7mfanscy9hzi5q546cj5h3pfrwmbj8")))

(define-public crate-identifiers-1.0.0 (c (n "identifiers") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1172n627nqim1x4d0hsqklrwz0g3dkg5gz824pxhfbzn7931izgw")))

(define-public crate-identifiers-1.0.1 (c (n "identifiers") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1yvhjhb52j96919xs4dmgjzxc4sgzkjzx126867xw5185xr6ggm1")))

