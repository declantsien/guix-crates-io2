(define-module (crates-io id ca idcard) #:use-module (crates-io))

(define-public crate-idcard-0.1.1 (c (n "idcard") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13s4hymqvfs3dkkynwxi5m8pjbrq3gk23wjqhg00hzw87y97fdwn")))

(define-public crate-idcard-0.1.2 (c (n "idcard") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "044xsfwlq7ngd5cvj4jc1f42gk21rg3x5nqnnw8r0rhiy5lyv90v")))

(define-public crate-idcard-0.1.3 (c (n "idcard") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15096kmi1aw63xkavhlbc9ss9hihj0rrq45iljvy1m09pp4dzqsl")))

(define-public crate-idcard-0.1.4 (c (n "idcard") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0apbjb8gsrslbw1r5wg4vyb6qn5cckdw6naksqbnhakvdwb9wgfp")))

(define-public crate-idcard-0.2.0 (c (n "idcard") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0f3d3v901yil3hqp2pwh8m4x6g1fdbxn59xf0pzz2gg173pz45by")))

(define-public crate-idcard-0.3.0 (c (n "idcard") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08simx1fnv1p96rkh0659yqdcdrkl05qchdadaqpdrl4c8xiaa94")))

