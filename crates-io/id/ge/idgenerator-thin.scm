(define-module (crates-io id ge idgenerator-thin) #:use-module (crates-io))

(define-public crate-idgenerator-thin-0.1.0 (c (n "idgenerator-thin") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "179vknnf6nbp1gim6vdv660spgn27k0r4iygliqdajf6d4j2w5zy")))

(define-public crate-idgenerator-thin-0.1.1 (c (n "idgenerator-thin") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1kb3j93xdb0jjv0w002vdjfl0rmwb30572dall6avld9jwxi7k4h")))

(define-public crate-idgenerator-thin-0.1.2 (c (n "idgenerator-thin") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "17hcl9m9q71vfn3sccfnlg3vqwq877psvr823l9gknibaxq4nag2")))

(define-public crate-idgenerator-thin-0.2.0 (c (n "idgenerator-thin") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "16ngk05d7b3jipw1qjxmrys88s64qsw8s6x9lxzibk129lc8j8n0")))

(define-public crate-idgenerator-thin-0.2.1 (c (n "idgenerator-thin") (v "0.2.1") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)))) (h "066751q67zzidkrqwfj29lssm1xvm4yw1afq7clgy24rv4j9j5ga")))

