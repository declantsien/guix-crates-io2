(define-module (crates-io id ge idgen) #:use-module (crates-io))

(define-public crate-idgen-0.1.1 (c (n "idgen") (v "0.1.1") (h "00hq3mgwimclxmgmmkfajpx5fk4cffkngyf8crqsgqbdy0lpn3v2")))

(define-public crate-idgen-0.1.2 (c (n "idgen") (v "0.1.2") (h "0jv0ibjbnpandgdbb7rnaibm8llfvm0fwynycbqgb5n7iaw2drfg")))

