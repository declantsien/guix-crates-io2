(define-module (crates-io id er iderive) #:use-module (crates-io))

(define-public crate-iderive-0.1.0 (c (n "iderive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1x194zhkq6dgyqx70wxz5mmfgkksk8nqg9hp7wn76kkjw0spfz9q")))

(define-public crate-iderive-1.0.0 (c (n "iderive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "0ld8c3mbyjjy9799xxj7qsxywh534n0bq4dfjga420zkln42rsqi")))

(define-public crate-iderive-1.1.0 (c (n "iderive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "149nkc3fybrablna594kifqn5wxw9xi5102mrh65lk1l7jdylaim")))

(define-public crate-iderive-1.1.1 (c (n "iderive") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "14jdwsswn48ad8rfllaz9f40klrhf19bhpqqp4gk0parlk0yp6mm")))

(define-public crate-iderive-1.1.2 (c (n "iderive") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0hp62268qyw1ps588mamnm7n76fh7cz5bj3yrn0i9in4r3hnnih6")))

