(define-module (crates-io id #{3u}# id3utile) #:use-module (crates-io))

(define-public crate-id3utile-0.1.0 (c (n "id3utile") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.0.2") (d #t) (k 0)) (d (n "id3") (r "^1.3.0") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1lhhck2i772qgkiv26227z3i9llljq9qszybxdk0rhzhwi16bw6v")))

