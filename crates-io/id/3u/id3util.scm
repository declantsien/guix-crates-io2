(define-module (crates-io id #{3u}# id3util) #:use-module (crates-io))

(define-public crate-id3util-0.1.0 (c (n "id3util") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.0.2") (d #t) (k 0)) (d (n "id3") (r "^1.3.0") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1zmwrlrxy9zmpmysjdq1p5wn20cc4zsr9clwpj5iy8bkn20fcfhp")))

(define-public crate-id3util-0.2.0 (c (n "id3util") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.0.2") (d #t) (k 0)) (d (n "id3") (r "^1.3.0") (d #t) (k 0)) (d (n "mp4ameta") (r "^0.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "19kd80kgp7n4y8dmyn7pmb2cw6f4i4qapvp85g3005px6rvj5axp")))

