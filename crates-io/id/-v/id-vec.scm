(define-module (crates-io id -v id-vec) #:use-module (crates-io))

(define-public crate-id-vec-0.5.0 (c (n "id-vec") (v "0.5.0") (h "1klv13a6653h0aqqmcqwi33zkbxz2y6k7j3gp5zd1ac7d73z9s08")))

(define-public crate-id-vec-0.5.1 (c (n "id-vec") (v "0.5.1") (h "0bs0myjx9ba6p2f0zz4qdyvy0703rv088znymf5rqkdmc2lc4qd7")))

(define-public crate-id-vec-0.5.2 (c (n "id-vec") (v "0.5.2") (h "1lip9s8qmk0wxldh4pxhf8xy5f1cvs27hx6jv6y7b846r57d32q7")))

(define-public crate-id-vec-0.5.3 (c (n "id-vec") (v "0.5.3") (h "08fl47f4bhk2hymzrmq2imyn88jkvhp0nych53gbw44q0ghs0vnd")))

(define-public crate-id-vec-0.5.4 (c (n "id-vec") (v "0.5.4") (h "0rxscd79lm0z3pssc4sdqivw8qlnxr3k34dyr56yy58xb90x5ncr")))

(define-public crate-id-vec-0.5.5 (c (n "id-vec") (v "0.5.5") (h "053j2p3jqzr4swhm3gq6h9fv90rxz3wnih8nwjisg8djips49bm8")))

(define-public crate-id-vec-0.5.6 (c (n "id-vec") (v "0.5.6") (h "000540hvr6sncqly3wmkifa0wa4bzvc2jr3a203fp9rprq929lqf")))

(define-public crate-id-vec-0.5.7 (c (n "id-vec") (v "0.5.7") (h "065pyyndrzs6vs2v5n1m6yzgf6qvymiqw0hw2fdrqh73p7d1d73w")))

