(define-module (crates-io id -d id-derive) #:use-module (crates-io))

(define-public crate-id-derive-0.1.0 (c (n "id-derive") (v "0.1.0") (d (list (d (n "macrotest") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1131wwrckv9qf01xix2np5l8kpwyl958kgs007gc2zpcaz66lhvi")))

