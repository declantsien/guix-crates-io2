(define-module (crates-io id #{-i}# id-ish) #:use-module (crates-io))

(define-public crate-id-ish-0.0.1 (c (n "id-ish") (v "0.0.1") (d (list (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0fmlxi9vm9y27bjkfhl1vfwp3lyy1iznm35d480dga0wg4advpgc") (f (quote (("u128") ("i128"))))))

(define-public crate-id-ish-0.0.2 (c (n "id-ish") (v "0.0.2") (d (list (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1c0608za14qq0gc414akiqa55220875zpwqqfb8ys7328v8jil7g") (f (quote (("with-take-id") ("u128") ("i128") ("default" "i128" "u128" "with-take-id"))))))

(define-public crate-id-ish-0.0.3 (c (n "id-ish") (v "0.0.3") (d (list (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "02q4yrpm914lfb27xcwl0mvp60ppdxnpdqwa9jfw7aq73fimrqyc") (f (quote (("with-take-id") ("u128") ("default" "u128" "with-take-id"))))))

