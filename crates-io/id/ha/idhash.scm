(define-module (crates-io id ha idhash) #:use-module (crates-io))

(define-public crate-idhash-0.1.0 (c (n "idhash") (v "0.1.0") (d (list (d (n "arrow") (r "^5.3") (f (quote ("csv"))) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "12y6is9x3x5fqcd9j63gj2y3g1nk5nk43pwqn0klhfwzyhywdf3a")))

(define-public crate-idhash-0.1.1 (c (n "idhash") (v "0.1.1") (d (list (d (n "arrow") (r "^5.3") (f (quote ("csv"))) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1jbz3kqn7m24wwkbc1h1ki3zvqi4wx9gbkqi4kp2lkw3k8hc9vrj")))

(define-public crate-idhash-0.1.2 (c (n "idhash") (v "0.1.2") (d (list (d (n "arrow") (r "^5.3") (f (quote ("csv"))) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1w5iih7rf964sbpj87aqdgk8pv6shaf3r586bvmh8c3x55d8q5qs")))

(define-public crate-idhash-0.1.3 (c (n "idhash") (v "0.1.3") (d (list (d (n "arrow") (r "^0.6.2") (f (quote ("csv" "io_csv_read"))) (k 0) (p "arrow2")) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0la2mg96kazcklp46g5hcfaf48yysp80qbll5g16l0y01xy98idi")))

(define-public crate-idhash-0.1.4 (c (n "idhash") (v "0.1.4") (d (list (d (n "arrow") (r "^0.7.0") (f (quote ("csv" "io_csv_read"))) (k 0) (p "arrow2")) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1m40fww1z2f4vyqzbbmpi3636bfrmf4irjnz4l3x70nhml4v401b")))

(define-public crate-idhash-0.1.5 (c (n "idhash") (v "0.1.5") (d (list (d (n "arrow") (r "^0.7.0") (f (quote ("csv" "io_csv_read"))) (k 0) (p "arrow2")) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0ba19wr001m5kv2m63b7v2vb05s8gm4v4fmfw5y1n6hra8bs2i5z")))

(define-public crate-idhash-0.2.0 (c (n "idhash") (v "0.2.0") (d (list (d (n "arrow") (r "^0.8.0") (f (quote ("csv" "io_csv_read"))) (k 0) (p "arrow2")) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "122wjfxhwmcxgykbbha5v2k61qwg9dlfqg0d9bk61h2izq83z7q6")))

(define-public crate-idhash-0.3.0 (c (n "idhash") (v "0.3.0") (d (list (d (n "arrow") (r "^0.12.0") (f (quote ("csv" "io_csv_read"))) (k 0) (p "arrow2")) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "19dgshcm8wbzm743arnivllsxzbm818zcjhxx8rb5km470llsspm")))

