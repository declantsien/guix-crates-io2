(define-module (crates-io id ab idable) #:use-module (crates-io))

(define-public crate-idable-0.1.0 (c (n "idable") (v "0.1.0") (h "1a5a62f1264h1nddl09k1s82iirdjgszz1v5y83h3763gwgsyqr4") (f (quote (("for-test"))))))

(define-public crate-idable-0.1.1 (c (n "idable") (v "0.1.1") (h "0jn3aqbsgx1fzvwvdp1nbvlk5m7cj4vgkhwnsxnvq7n0yp38g14k") (f (quote (("for-test"))))))

(define-public crate-idable-0.1.2 (c (n "idable") (v "0.1.2") (h "0g27ym1bc9wl45j590qxsp4jsbp54b6fkah10g8ppdnih9d32rn8") (f (quote (("for-test"))))))

