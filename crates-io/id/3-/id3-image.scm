(define-module (crates-io id #{3-}# id3-image) #:use-module (crates-io))

(define-public crate-id3-image-0.1.0 (c (n "id3-image") (v "0.1.0") (d (list (d (n "id3") (r "^0.3.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.6") (d #t) (k 2)))) (h "0kmk1vbfsrjdn4lbx4bfj6wiyf5nb3xvvsnk5j9cs8xnimrikcb6")))

(define-public crate-id3-image-0.1.1 (c (n "id3-image") (v "0.1.1") (d (list (d (n "id3") (r "^0.3.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.6") (d #t) (k 2)))) (h "0s9bda4gwwm20arjdzgagws74h1plzgsv5drgki6c7dis0pjmbmd")))

(define-public crate-id3-image-0.1.2 (c (n "id3-image") (v "0.1.2") (d (list (d (n "id3") (r "^0.3.0") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (k 0)) (d (n "tempfile") (r "^3.0.6") (d #t) (k 2)))) (h "0ck6la4mhvaqy9czaxhfsqhpb0lczzp20ha43lkhaw71zh5vkc9q")))

(define-public crate-id3-image-0.1.3 (c (n "id3-image") (v "0.1.3") (d (list (d (n "id3") (r "^0.5") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1jmgn7wicn9c9hf91w9p2cbrbnz5ibg0b7i9f2j6rsrfqr9yjmff")))

(define-public crate-id3-image-0.1.4 (c (n "id3-image") (v "0.1.4") (d (list (d (n "id3") (r "^0.5") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1csgw9r2w0kdxs23dmsfz5jrbq2iw7v9qqjzy5acxikfp1ni0qzb")))

(define-public crate-id3-image-0.1.5 (c (n "id3-image") (v "0.1.5") (d (list (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "198xj0ng3q094h7yrrm44m8w36ws9ysyb1lrg0csysb8ryckzsad")))

(define-public crate-id3-image-0.2.0 (c (n "id3-image") (v "0.2.0") (d (list (d (n "id3") (r "^0.6.2") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0hazp4k2ga6fk0i6v1vvc4jwpkyhk9dwl9kiic6qm0wswhdkq0s1")))

(define-public crate-id3-image-0.3.0 (c (n "id3-image") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "id3") (r "^0.6.2") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0mykr0lskgyv1rq71qyp2mbypvcnbz2j3xdyv11yszyrbj831h8f")))

(define-public crate-id3-image-0.3.1 (c (n "id3-image") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "id3") (r "^1.1.2") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "188660fjjaqjc3b8dll06cbi12qjff75lf3mpsln6zi8qzjqrb91")))

