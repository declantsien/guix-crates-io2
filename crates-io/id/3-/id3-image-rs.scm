(define-module (crates-io id #{3-}# id3-image-rs) #:use-module (crates-io))

(define-public crate-id3-image-rs-0.4.0 (c (n "id3-image-rs") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "id3") (r "^1.0.2") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "17pyza8xn75x4gjfvk43v7qbbw0w4jqpq0zhn2v2mjaml98wxq18")))

(define-public crate-id3-image-rs-0.4.1 (c (n "id3-image-rs") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "id3") (r "^1.0.2") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "16g2bbdjc1knlqs90xp98im6xgmzpxv5vkh1mbafy88pqfhvrm47")))

(define-public crate-id3-image-rs-0.4.2 (c (n "id3-image-rs") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 0)) (d (n "id3") (r "^1.2.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1x3jdnbdh1vi274wcm0kwhzxiw7s6dnxshv5lzsb6alp62xvkl8l")))

