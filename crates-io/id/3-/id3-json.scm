(define-module (crates-io id #{3-}# id3-json) #:use-module (crates-io))

(define-public crate-id3-json-0.1.0 (c (n "id3-json") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "id3") (r "^1.5.1") (d #t) (k 0)) (d (n "lexopt") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1dni3ibkb58m4zfkrmvii555x5n2kis2hnyiq5z90rvk821ig7fb")))

(define-public crate-id3-json-0.1.1 (c (n "id3-json") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "id3") (r "^1.5.1") (d #t) (k 0)) (d (n "lexopt") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1v8h2z4alnipcm86r1jpc8sw8y1srrkw4wwz52kky68gks2hr1j1")))

(define-public crate-id3-json-0.1.2 (c (n "id3-json") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "id3") (r "^1.5.1") (d #t) (k 0)) (d (n "lexopt") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "11y7m8wppnrkr1y7b9p9kcr956j2a1m7p9zqhj1ns00639i9i3ab")))

(define-public crate-id3-json-0.2.0 (c (n "id3-json") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "id3") (r "^1.6.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1rjg31gg75sn5dsvyl786ybwm736qjmg3dhrcnyv9kmjz6amig89")))

(define-public crate-id3-json-0.2.1 (c (n "id3-json") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "id3") (r "^1.6.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "18yx8490wh91rnc5dl7prwfyxgb23k7mrqxiddc2gjj960bqk1i3")))

