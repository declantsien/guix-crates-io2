(define-module (crates-io id em idempotent-resource-ops) #:use-module (crates-io))

(define-public crate-idempotent-resource-ops-0.0.1 (c (n "idempotent-resource-ops") (v "0.0.1") (d (list (d (n "time") (r "^0.3.17") (f (quote ("std"))) (k 2)))) (h "0mnvd8rm1b4l63ag8b1qxbfywdzkf72m39rwkll6yqh37c6h5ydm") (f (quote (("std") ("default")))) (r "1.60")))

