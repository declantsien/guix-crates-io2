(define-module (crates-io id -m id-manager) #:use-module (crates-io))

(define-public crate-id-manager-0.0.1 (c (n "id-manager") (v "0.0.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "random-manager") (r "^0.0.1") (d #t) (k 0)) (d (n "ripemd") (r "^0.1.1") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "0bxn786875f2b8ichv2svdxyrnqw1gsrf73nhj1j5i3sa6dsrxpl") (r "1.62")))

(define-public crate-id-manager-0.0.2 (c (n "id-manager") (v "0.0.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "random-manager") (r "^0.0.2") (d #t) (k 0)) (d (n "ripemd") (r "^0.1.3") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "0c76v99wiwzq48crldn0pigzxilxvnsqjlzyw3327v966lh0ijsv") (r "1.66")))

(define-public crate-id-manager-0.0.3 (c (n "id-manager") (v "0.0.3") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "random-manager") (r "^0.0.5") (d #t) (k 0)) (d (n "ripemd") (r "^0.1.3") (d #t) (k 0)) (d (n "whoami") (r "^1.3.0") (d #t) (k 0)))) (h "1581gi519fqnsg2fxnql0lkvqn0bzdisiilgbv97qhqafqg1baxx") (r "1.67")))

