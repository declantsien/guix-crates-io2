(define-module (crates-io id -m id-map) #:use-module (crates-io))

(define-public crate-id-map-0.1.0 (c (n "id-map") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4.0") (d #t) (k 0)))) (h "12vz99q3ay3nllsmklchd1g021xsbpqn5bjqljz35h1yx88fnc70")))

(define-public crate-id-map-0.1.1 (c (n "id-map") (v "0.1.1") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4.0") (d #t) (k 0)))) (h "0l0mm9vp0pky7b3azbbkwwvgvfqclxjpd1wn8aqvq3z0yrawsm5z")))

(define-public crate-id-map-0.1.2 (c (n "id-map") (v "0.1.2") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4.0") (d #t) (k 0)))) (h "0dbkbygnxbdilih3824x8lxkbnqw73da5lm98gbr92spxw8ddqky")))

(define-public crate-id-map-0.1.3 (c (n "id-map") (v "0.1.3") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4.0") (d #t) (k 0)))) (h "1x1i1zzcrx292vzccwmvwixb4gxdndvm1ba7807kvy0y6sb7j8iw")))

(define-public crate-id-map-0.1.4 (c (n "id-map") (v "0.1.4") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4.0") (d #t) (k 0)))) (h "12fsxbrfhcz243njh40hbqaikzqndc08bqccymzanz6xh5rscz36")))

(define-public crate-id-map-0.1.5 (c (n "id-map") (v "0.1.5") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4.0") (d #t) (k 0)))) (h "1ckfd74373i7wmzakq81i6j7pgq3ny6hzkjs4nywq2b6l102qnwg")))

(define-public crate-id-map-0.1.6 (c (n "id-map") (v "0.1.6") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4.0") (d #t) (k 0)))) (h "1m5d1ingwl1ry18jdabxd05vdh8wl36xjy07ar2z9z9635jm4ipv")))

(define-public crate-id-map-0.1.8 (c (n "id-map") (v "0.1.8") (d (list (d (n "id-set") (r "^0.1.2") (d #t) (k 0)))) (h "0isl10zqgm551xyvasp257w3zn2f78gqzi3pshzwknymcgqq1zk4")))

(define-public crate-id-map-0.1.9 (c (n "id-map") (v "0.1.9") (d (list (d (n "id-set") (r "^0.1.3") (d #t) (k 0)))) (h "1n4p80sw6rrym2a1frs1yd60hlb74lkpddq7dxby3y13cifcvriy")))

(define-public crate-id-map-0.1.10 (c (n "id-map") (v "0.1.10") (d (list (d (n "id-set") (r "^0.1.6") (d #t) (k 0)))) (h "03a2c3ljf5rcyj599fdhw3dpkfsib4s7g920i7x9b8xbq5sk1kpp")))

(define-public crate-id-map-0.1.11 (c (n "id-map") (v "0.1.11") (d (list (d (n "id-set") (r "^0.1.9") (d #t) (k 0)))) (h "13mwp91n9s37x03cy1zjncji3skisbmiyyxj122ikajjwxzb7wdq")))

(define-public crate-id-map-0.1.12 (c (n "id-map") (v "0.1.12") (d (list (d (n "id-set") (r "^0.2.0") (d #t) (k 0)))) (h "1h392vhfkjnrz7zbzxbpwazisiqm9p08x2w4nrvs759pigy225w2")))

(define-public crate-id-map-0.1.13 (c (n "id-map") (v "0.1.13") (d (list (d (n "id-set") (r "^0.2.0") (d #t) (k 0)))) (h "08whk4a28036jkynbc72rz9ydkwad06cnnfbs8gpli8h2yvnyn82")))

(define-public crate-id-map-0.1.14 (c (n "id-map") (v "0.1.14") (d (list (d (n "id-set") (r "^0.2.0") (d #t) (k 0)))) (h "1hrn84f5ps0h62fpi17z3z6h5jav4j9h9rv69ihbjnss4jaxdkvm")))

(define-public crate-id-map-0.1.15 (c (n "id-map") (v "0.1.15") (d (list (d (n "id-set") (r "^0.2.0") (d #t) (k 0)))) (h "004sxcgsjypr9lryrdji069nsjdbhxb6farrk6f8sklv9dzr4na4")))

(define-public crate-id-map-0.1.16 (c (n "id-map") (v "0.1.16") (d (list (d (n "id-set") (r "^0.2.0") (d #t) (k 0)))) (h "0502vxrhaldjq4s8z9pgc7h853qmzskz4qcygxlj09kkfba65qcc")))

(define-public crate-id-map-0.2.0 (c (n "id-map") (v "0.2.0") (d (list (d (n "id-set") (r "^0.2.1") (d #t) (k 0)))) (h "12gsyv9h5acf1sa2mh477gf9vi04kyxva0354nph3v72a1igrxlv")))

(define-public crate-id-map-0.2.1 (c (n "id-map") (v "0.2.1") (d (list (d (n "id-set") (r "^0.2.1") (d #t) (k 0)))) (h "1lf6yrsn39l14la0g0g007zvaz2yqfp5rf1wv69mnnlxh5lmb0qb")))

