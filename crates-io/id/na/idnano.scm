(define-module (crates-io id na idnano) #:use-module (crates-io))

(define-public crate-idnano-0.8.0 (c (n "idnano") (v "0.8.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0arvkjawyn51zrk38qmsw90af9aa5pflnwbl7z8nayiqp7fsgiwz")))

(define-public crate-idnano-0.8.1 (c (n "idnano") (v "0.8.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "152k734k7kmqkzig5g8558whd8mggb7b1iy6g5i993xlfjjm18sz")))

(define-public crate-idnano-0.8.2 (c (n "idnano") (v "0.8.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04lv51hmn1g8k9xbdnm60ai1i10p7gb42m396g0mwlmnbnlkf69q") (y #t)))

(define-public crate-idnano-0.8.3 (c (n "idnano") (v "0.8.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1iaj1xclviv162pwq3fqh55bx49y6zw4lrhas8bhk4zp7j8pjid2") (y #t)))

(define-public crate-idnano-0.8.4 (c (n "idnano") (v "0.8.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jnqz7ds4jg1ibdij2zzyc3pch1jfm56pcrhcj5hdsxvdi11afvc")))

