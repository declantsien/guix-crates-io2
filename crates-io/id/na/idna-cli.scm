(define-module (crates-io id na idna-cli) #:use-module (crates-io))

(define-public crate-idna-cli-0.1.0 (c (n "idna-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "idna") (r "^0.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0gd4vx8cjkpzaxbqm7xxpfj7nav5rsx63yrh7jd7x0rq8f37h0k8")))

(define-public crate-idna-cli-0.1.1 (c (n "idna-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "idna") (r "^0.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "07z07am1pinvyiz4jk3z8laidrs3kam1zavnmqn7q3igw0pxfgfk")))

(define-public crate-idna-cli-0.1.2 (c (n "idna-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "idna") (r "^0.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "016mb6ndhsbaliznwrg3ng0gb95v0mqiqrviflywid4lzay2s7vm")))

(define-public crate-idna-cli-0.1.3 (c (n "idna-cli") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "idna") (r "^0.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "18p0dwj80dynin4i0smkh4971m2h9zkcd52va574pwdisks8cpqf")))

(define-public crate-idna-cli-0.1.4 (c (n "idna-cli") (v "0.1.4") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "idna") (r "^0.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0gj0ysi3r0l9pk4cds5mylncskxnv3hvacz871l0kvbjrknscw6c")))

