(define-module (crates-io id ea idealist) #:use-module (crates-io))

(define-public crate-idealist-0.1.0 (c (n "idealist") (v "0.1.0") (h "0wkhnhl5r9ynalxlgmvzv8mfrjf9aikql2xwfd8hv9wvgz7c3fz9") (y #t)))

(define-public crate-idealist-0.1.1 (c (n "idealist") (v "0.1.1") (h "0mslf5750j22zgz2fcks7d0hy37469brmnbg1l7pwrss6a1lf5dn")))

