(define-module (crates-io id ea idea) #:use-module (crates-io))

(define-public crate-idea-0.0.0 (c (n "idea") (v "0.0.0") (h "0lvxwv438l15qmrl5qh7mxczpyr93wbw9vm6f85v83ydmym5sbhr") (y #t)))

(define-public crate-idea-0.0.1 (c (n "idea") (v "0.0.1") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "1sjps4xv929lisk5k78jlh4r986rzipfpxzn7dirz370il8w55sl")))

(define-public crate-idea-0.1.0 (c (n "idea") (v "0.1.0") (d (list (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "1qgwdn1mbfksyndlymmf9g23fhap1kn2mmb15k1wwzmcl0nwzp1d")))

(define-public crate-idea-0.2.0 (c (n "idea") (v "0.2.0") (d (list (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "0l3w5k5bz65qrk6jdiqksgbwinckglaizzkn69pw1kx7bzyiq7n8")))

(define-public crate-idea-0.3.0 (c (n "idea") (v "0.3.0") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1cxcr8a7war3np96f4hlvzxl78wnkyik4pfwpcim29pj9h8lppgw")))

(define-public crate-idea-0.4.0 (c (n "idea") (v "0.4.0") (d (list (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "035msmqxzy68j824nbfbzi6sxdhfvimc5ccnjgkynjva85q9fsvi")))

(define-public crate-idea-0.5.0 (c (n "idea") (v "0.5.0") (d (list (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)))) (h "0444ncqf5k7r5aj9n2dgm9mys30dw1bvy9j2wl1qnlixjshzc0lf") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-idea-0.5.1 (c (n "idea") (v "0.5.1") (d (list (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)))) (h "0xv4hd9mgrwgzfl7cc5nlwyahm9yni5z9dwb3c1z5mqr8h05fm87") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

