(define-module (crates-io id ea idea_crypto) #:use-module (crates-io))

(define-public crate-idea_crypto-0.1.0 (c (n "idea_crypto") (v "0.1.0") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)))) (h "0dfin8h0z5n38i1x1vk1g22i6s9cfq0gmr87lhznpwiv52f87p3n")))

(define-public crate-idea_crypto-0.1.1 (c (n "idea_crypto") (v "0.1.1") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)))) (h "0px1h2pfj4jnir7nhrn66v11ln7gvv3szm1iifjnncinp7ln5c41")))

(define-public crate-idea_crypto-0.1.2 (c (n "idea_crypto") (v "0.1.2") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)))) (h "1brgj4rk6jvbadmmn6xmrwgwq4dcq4dd80v9q9f9mahya35lgq63")))

(define-public crate-idea_crypto-0.1.3 (c (n "idea_crypto") (v "0.1.3") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)))) (h "139m8qn3z11zrjsn2c76qf89rprx9zip36a9pvmz0ahfadcz37f4")))

(define-public crate-idea_crypto-0.2.0 (c (n "idea_crypto") (v "0.2.0") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)))) (h "10zjkdx4zd6l0pf9b86yfj6c2pi5dqnfjinm8brj30nhyxmn6gpr")))

(define-public crate-idea_crypto-0.2.2 (c (n "idea_crypto") (v "0.2.2") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)))) (h "0najflg99qfy9hs1ga8j2pahdvfxdx4469cf18dxpq44bka54ca2")))

(define-public crate-idea_crypto-0.2.3 (c (n "idea_crypto") (v "0.2.3") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)))) (h "0l6kz10ka12yicb364i0iiivqis84k8asmjp37d4vzb4bq4bn9l9")))

(define-public crate-idea_crypto-0.2.4 (c (n "idea_crypto") (v "0.2.4") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)))) (h "03arpk3fz2mbfdqd8gg75037j0dwplp5c44z7jqz0fgd7lqpk15b")))

(define-public crate-idea_crypto-0.2.5 (c (n "idea_crypto") (v "0.2.5") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)))) (h "0rwaqfb1ri97gfwqh22h7a3ibggdvys5x7hpgl7skjd9v2fs6l7p")))

(define-public crate-idea_crypto-0.2.6 (c (n "idea_crypto") (v "0.2.6") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)))) (h "1qxgcd73zljspg569npj2blc3wqdrp7zirj4hx7ciddpwnpd7brn")))

(define-public crate-idea_crypto-0.2.7 (c (n "idea_crypto") (v "0.2.7") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)))) (h "1zm9q91p4y8aim42i3pns0gsa34a0fp5g364gphi08sdna0id2h8")))

(define-public crate-idea_crypto-0.2.8 (c (n "idea_crypto") (v "0.2.8") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)))) (h "0gh6x233xl50xb9jh4l354phaxx8h52yh0ndpcgsfcg5yfwaf97n")))

(define-public crate-idea_crypto-0.2.9 (c (n "idea_crypto") (v "0.2.9") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)))) (h "16s2crwhghhccmffry1d3idsiglvpwamn6csc6fsf54a1mapxg3f")))

