(define-module (crates-io id ea idea_algorithm) #:use-module (crates-io))

(define-public crate-idea_algorithm-0.1.0 (c (n "idea_algorithm") (v "0.1.0") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)))) (h "0yhyk00vq87ncgjbq5ppj497wrzjrzaxnd1wcahl8q3841zi4ggg") (y #t)))

(define-public crate-idea_algorithm-0.1.1 (c (n "idea_algorithm") (v "0.1.1") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)))) (h "06hvcfxzvfx98q9kdg3dp4pim4qfbazw6bvxv5av719qpg2b3z6r") (y #t)))

(define-public crate-idea_algorithm-0.1.2 (c (n "idea_algorithm") (v "0.1.2") (d (list (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 0)))) (h "1aw1n7b5xd70kgr5mq641h6gidgssv6wbfqqh0mhyf0cpmq4rm0v") (y #t)))

