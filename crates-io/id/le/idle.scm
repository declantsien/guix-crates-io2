(define-module (crates-io id le idle) #:use-module (crates-io))

(define-public crate-idle-0.1.0 (c (n "idle") (v "0.1.0") (h "0jkai9ir373r847sx138chrprhbpjawqql1vjwh7a87ds1ybfy0h") (r "1.56.1")))

(define-public crate-idle-0.2.0 (c (n "idle") (v "0.2.0") (h "1lwxmpjzjrl9xkl2q04g6vhwpvrjqrpgknb1jka57nbhvdb4rzp3") (r "1.56.1")))

