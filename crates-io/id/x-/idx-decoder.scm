(define-module (crates-io id x- idx-decoder) #:use-module (crates-io))

(define-public crate-idx-decoder-0.0.1 (c (n "idx-decoder") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18.0") (d #t) (k 0)))) (h "08i2lmbbj3ri2k3vdaaslg9z0xgar2s2b9xak72638c365hwgdjl")))

(define-public crate-idx-decoder-0.0.2 (c (n "idx-decoder") (v "0.0.2") (d (list (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1kd7f4ryg98zxh1pfawmnbv8xj5izinbp51d8wsf1zc2aqzqqqvl")))

