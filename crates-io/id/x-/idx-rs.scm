(define-module (crates-io id x- idx-rs) #:use-module (crates-io))

(define-public crate-idx-rs-0.8.0 (c (n "idx-rs") (v "0.8.0") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "1q7vrskywcr60dl5yvnqwc144z9w8wrml647cp2kvm9iac30gx38")))

(define-public crate-idx-rs-0.8.1 (c (n "idx-rs") (v "0.8.1") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "0vrzmh6r3r7p7py0lrng6jh0lk5dxygid3bb207lz8g478m98lzn")))

(define-public crate-idx-rs-0.9.0 (c (n "idx-rs") (v "0.9.0") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "1h17s4z5iwwmnp2gwyd9x76d8xyggwgv5r9kkgzvkj3p3i29ayfz")))

(define-public crate-idx-rs-0.9.1 (c (n "idx-rs") (v "0.9.1") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "0chgs59slzd54fjad7k9hgpjmzh0sxj3xvi5pbhrmw3ab1vcrsd7")))

(define-public crate-idx-rs-0.9.2 (c (n "idx-rs") (v "0.9.2") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "0iz51jl9ppv2rdw2kaxxq0kfd743vinmzjfq39k1zc9cjmajf1j5")))

(define-public crate-idx-rs-0.9.3 (c (n "idx-rs") (v "0.9.3") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "0ynwcnm1gpakghvs8zz7078nl03nl1yb4j4zbm02v4nr3h4lvm5r")))

(define-public crate-idx-rs-0.9.4 (c (n "idx-rs") (v "0.9.4") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "1813azw3lln4fbg125skli15xq6j7zdqsf4gbd4ksg9mdwiz80pg")))

(define-public crate-idx-rs-0.9.5 (c (n "idx-rs") (v "0.9.5") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "084f91w8v0bg9hwnk8ya7d5nppqxgzbqyr0ll1kp8znvay8wdwjl")))

(define-public crate-idx-rs-0.9.6 (c (n "idx-rs") (v "0.9.6") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0qg36c19l0i7x1v431xqxl3wipkgq82y9mmw6zm58sgd8wc7g3y8")))

(define-public crate-idx-rs-0.9.7 (c (n "idx-rs") (v "0.9.7") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0fxmpwb4myzbcy3zja2q2q5l5fikz4kw6hpa06wp99ab6k5xgyf7")))

(define-public crate-idx-rs-0.9.8 (c (n "idx-rs") (v "0.9.8") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1n9wkykp12aj6z8947n26fdib1flfd2vw4rvcc8xg2akji9kcpim")))

(define-public crate-idx-rs-0.9.9 (c (n "idx-rs") (v "0.9.9") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "056v9fn55zyg1bxs4kczz431qjb31i7lv0mr3lqf8xj5smnla9wp")))

(define-public crate-idx-rs-0.9.10 (c (n "idx-rs") (v "0.9.10") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "17yyia0ksvinx2y19hg8xf2ybwakk1rx0d68673l9nk02333ww2x")))

(define-public crate-idx-rs-0.9.11 (c (n "idx-rs") (v "0.9.11") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "069xf9h9426bmmw6p5xs9g9sfq0yk0p8i2b5yjlmk06vd7gdi9zf")))

(define-public crate-idx-rs-0.9.12 (c (n "idx-rs") (v "0.9.12") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1xxlf9m65gcwib8bi2jaha7ivj99bnnma2clpylx99dqbyhdca9x")))

(define-public crate-idx-rs-0.10.0 (c (n "idx-rs") (v "0.10.0") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "databuffer") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "0l2181kz53q1vka5qc2jhc5ig70ib0sbbr70nn55fxgcgaq9iqp8") (f (quote (("async" "tokio"))))))

