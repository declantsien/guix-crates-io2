(define-module (crates-io id x_ idx_file) #:use-module (crates-io))

(define-public crate-idx_file-0.29.0 (c (n "idx_file") (v "0.29.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "avltriee") (r "^0.42") (d #t) (k 0)) (d (n "file_mmap") (r "^0.15") (d #t) (k 0)))) (h "1xb9pw16qsmp2v1r1ji63jf9dqmindmjixyh9ylv6gs42xprpdhx") (y #t)))

(define-public crate-idx_file-0.30.0 (c (n "idx_file") (v "0.30.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "avltriee") (r "^0.43") (d #t) (k 0)) (d (n "file_mmap") (r "^0.15") (d #t) (k 0)))) (h "14zr92c55kb4npn2mj5r1n3whnwfybwm4s8sg4c5ggvm1fcymwvf") (y #t)))

(define-public crate-idx_file-0.30.1 (c (n "idx_file") (v "0.30.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "avltriee") (r "^0.43") (d #t) (k 0)) (d (n "file_mmap") (r "^0.15") (d #t) (k 0)))) (h "06jmwnl2ip5fhvi7l116npkglasdbsgydymss7fspi22jsw9yr5a") (y #t)))

(define-public crate-idx_file-0.30.2 (c (n "idx_file") (v "0.30.2") (d (list (d (n "avltriee") (r "^0.43") (d #t) (k 0)) (d (n "file_mmap") (r "^0.15") (d #t) (k 0)))) (h "1vziizwmw4bjq9kk9fbcz1cmfz22s0476k8vp450zhcfw4bfw2pv") (y #t)))

(define-public crate-idx_file-0.31.0 (c (n "idx_file") (v "0.31.0") (d (list (d (n "avltriee") (r "^0.44") (d #t) (k 0)) (d (n "file_mmap") (r "^0.15") (d #t) (k 0)))) (h "1nm7fph3m50w44maa6rzjh8nybwpsp04dsckr0nrd69qk9dj8aqi") (y #t)))

(define-public crate-idx_file-0.32.0 (c (n "idx_file") (v "0.32.0") (d (list (d (n "avltriee") (r "^0.45") (d #t) (k 0)) (d (n "file_mmap") (r "^0.15") (d #t) (k 0)))) (h "07n176xmzldvz7pf30z56bvmw9c37gvphdmj73bkxd0przy0aqsf") (y #t)))

(define-public crate-idx_file-0.33.0 (c (n "idx_file") (v "0.33.0") (d (list (d (n "avltriee") (r "^0.46") (d #t) (k 0)) (d (n "file_mmap") (r "^0.15") (d #t) (k 0)))) (h "175k1g5nn3xf4a6wq17kr5zf3k8ihjd5hgscwv6wzjr713x17k5r") (y #t)))

(define-public crate-idx_file-0.34.0 (c (n "idx_file") (v "0.34.0") (d (list (d (n "avltriee") (r "^0.47") (d #t) (k 0)) (d (n "file_mmap") (r "^0.15") (d #t) (k 0)))) (h "0ghin7vwlrvsrj0sg996yp3f6c6hwrwm0lnqw3bf9cn6007cfbia") (y #t)))

(define-public crate-idx_file-0.35.0 (c (n "idx_file") (v "0.35.0") (d (list (d (n "avltriee") (r "^0.47") (d #t) (k 0)) (d (n "file_mmap") (r "^0.16") (d #t) (k 0)))) (h "0j61llrs9zzi0pirzbv55idlgsgxhkyi1352cv99f46mnkisjj7q") (y #t)))

(define-public crate-idx_file-0.35.1 (c (n "idx_file") (v "0.35.1") (d (list (d (n "avltriee") (r "^0.48") (d #t) (k 0)) (d (n "file_mmap") (r "^0.16") (d #t) (k 0)))) (h "0xxjyx8g4lf5xkw75sd001xs0bgp5bd76hka4ik2ia4qlykkr0hq") (y #t)))

(define-public crate-idx_file-0.36.0 (c (n "idx_file") (v "0.36.0") (d (list (d (n "avltriee") (r "^0.49") (d #t) (k 0)) (d (n "file_mmap") (r "^0.16") (d #t) (k 0)))) (h "03j16zn53ag6wnmgyj4f9c0mxighi934dpwd1ms38k3a0kzqzrxh")))

(define-public crate-idx_file-0.37.0 (c (n "idx_file") (v "0.37.0") (d (list (d (n "avltriee") (r "^0.50") (d #t) (k 0)) (d (n "file_mmap") (r "^0.16") (d #t) (k 0)))) (h "0xcqx4mviragclfnfbz05xrn4swwrf5i0lizv1bqh5by0dn45s2q")))

(define-public crate-idx_file-0.37.1 (c (n "idx_file") (v "0.37.1") (d (list (d (n "avltriee") (r "^0.50") (d #t) (k 0)) (d (n "file_mmap") (r "^0.16") (d #t) (k 0)))) (h "0sdz2699c659fgyamra2g2f4c48xz22yy1mcsq6rma4dk4zdldq0")))

(define-public crate-idx_file-0.37.2 (c (n "idx_file") (v "0.37.2") (d (list (d (n "avltriee") (r "^0.50") (d #t) (k 0)) (d (n "file_mmap") (r "^0.17") (d #t) (k 0)))) (h "1lr3447kabg1p51jmgqr664vlwkv7almvnp1578y6j3v7jz6207z")))

(define-public crate-idx_file-0.38.0 (c (n "idx_file") (v "0.38.0") (d (list (d (n "avltriee") (r "^0.51") (d #t) (k 0)) (d (n "file_mmap") (r "^0.17") (d #t) (k 0)))) (h "04jdqygx5my776fhc3lpm1kbswfc3gahy9988yhc08i9gnkxcy2a")))

(define-public crate-idx_file-0.38.1 (c (n "idx_file") (v "0.38.1") (d (list (d (n "avltriee") (r "^0.51") (d #t) (k 0)) (d (n "file_mmap") (r "^0.18") (d #t) (k 0)))) (h "1v6zgl0k557vrcad960mgd2l9zk3spdhm5g44s5vjg73rcgr5b5b")))

(define-public crate-idx_file-0.38.2 (c (n "idx_file") (v "0.38.2") (d (list (d (n "avltriee") (r "^0.51") (d #t) (k 0)) (d (n "file_mmap") (r "^0.18") (d #t) (k 0)))) (h "1ihaj0cd972xks4ir4yn1nibgcf1yikh64ivlb77a7pc76hin9hc")))

(define-public crate-idx_file-0.38.3 (c (n "idx_file") (v "0.38.3") (d (list (d (n "avltriee") (r "^0.51") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)))) (h "02f42jcpgnbav0xcx1hghim47fypax7hwvda3kys398rlx4m4x45")))

(define-public crate-idx_file-0.38.4 (c (n "idx_file") (v "0.38.4") (d (list (d (n "avltriee") (r "^0.51") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)))) (h "1v37av5vzbcr01byrnpdvbl9w6d2xjvjgcl92dcxv2bq8gwl63c6")))

(define-public crate-idx_file-0.39.0 (c (n "idx_file") (v "0.39.0") (d (list (d (n "avltriee") (r "^0.51") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)))) (h "1y6ijsmk2mfmpfh4zjbcj905maqh87fg6ivazv47r2r1cmy26bbq")))

(define-public crate-idx_file-0.40.0 (c (n "idx_file") (v "0.40.0") (d (list (d (n "avltriee") (r "^0.52") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)))) (h "1jg73m9n097c3pa1a5xm432l51lz3p9063yd9kihpr6r67kdiav0")))

(define-public crate-idx_file-0.41.0 (c (n "idx_file") (v "0.41.0") (d (list (d (n "avltriee") (r "^0.53") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)))) (h "0abk4k8b67k43bmv678jvmbvd0ja7n9bai38s8lx283n5kk4a6bc")))

(define-public crate-idx_file-0.42.0 (c (n "idx_file") (v "0.42.0") (d (list (d (n "avltriee") (r "^0.54") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)))) (h "0bfm8wnia35skf9dkjs5hgiza1nvgcx8sg170sganfrdndg2416v")))

(define-public crate-idx_file-0.43.0 (c (n "idx_file") (v "0.43.0") (d (list (d (n "avltriee") (r "^0.55") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "1hv1v1vy8qgkx08q3li42hlzk4m8ivb03fn9pfzlklfrify4fjjp")))

(define-public crate-idx_file-0.44.0 (c (n "idx_file") (v "0.44.0") (d (list (d (n "avltriee") (r "^0.56") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "024ng9fk43wv0mmz5mpnrn50b3k6yhvyx4g8azj140sc36894z92")))

(define-public crate-idx_file-0.45.0 (c (n "idx_file") (v "0.45.0") (d (list (d (n "avltriee") (r "^0.56") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "0a67ykmxiwsq0dlc9mkpcpkmpkjrhijq15sfdyk7bcp2gxfkhjhj")))

(define-public crate-idx_file-0.46.0 (c (n "idx_file") (v "0.46.0") (d (list (d (n "avltriee") (r "^0.57") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "0j5h27xi25yzbgwv277zblwm5ad2yrm4yr7dl87lq5x71y1pmwb0")))

(define-public crate-idx_file-0.46.1 (c (n "idx_file") (v "0.46.1") (d (list (d (n "avltriee") (r "^0.57") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "0zy882p2w122mgxpkbhg9xzr4vl0xbb3ryh9hzcvq4zz012v1dif")))

(define-public crate-idx_file-0.46.2 (c (n "idx_file") (v "0.46.2") (d (list (d (n "avltriee") (r "^0.57") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "01rzdzjy32d1shfw8ijx9r97jv02lb5lnqn0qpvyvng7da9hjkrn")))

(define-public crate-idx_file-0.46.3 (c (n "idx_file") (v "0.46.3") (d (list (d (n "avltriee") (r "^0.57") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "1djmk1hri9fslbbb18zjgsbbsbiw4mx450z3crhm639xac48w1d6")))

(define-public crate-idx_file-0.46.4 (c (n "idx_file") (v "0.46.4") (d (list (d (n "avltriee") (r "^0.57") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "0a2s5sa8319fimsfg0y46gcs8ip5lqg3hy0cik28z2nivgww2fay")))

(define-public crate-idx_file-0.47.0 (c (n "idx_file") (v "0.47.0") (d (list (d (n "avltriee") (r "^0.58.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "0zn87syyd64jgclfrs2m4c5bnl8vfz5sgqcqk47h2k6xfpy8xbpr")))

(define-public crate-idx_file-0.47.1 (c (n "idx_file") (v "0.47.1") (d (list (d (n "avltriee") (r "^0.58.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "18jl469lmylcs91fx7w6b5w9x8rsd9kgb21mwy4qrg1mn9m9nkcw")))

(define-public crate-idx_file-0.48.0 (c (n "idx_file") (v "0.48.0") (d (list (d (n "avltriee") (r "^0.60.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "0m1n7yybskc0pf1c7xbybv4x72cqpbb8xf84p6wvr7cys8pp1s6k")))

(define-public crate-idx_file-0.48.1 (c (n "idx_file") (v "0.48.1") (d (list (d (n "avltriee") (r "^0.61.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "177ggv2a8102kwf01bky0z1zzc8amsjmkk5h89vlp8cgax2fdk8c")))

(define-public crate-idx_file-0.49.0 (c (n "idx_file") (v "0.49.0") (d (list (d (n "avltriee") (r "^0.61.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "1rw7w0g51hvim95i2kc5y3kd6lr4cz13b3vwsbkwl9b70xi2ky13")))

(define-public crate-idx_file-0.50.0 (c (n "idx_file") (v "0.50.0") (d (list (d (n "avltriee") (r "^0.62.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "1k9gsf2h7s6rpfk4z40dhcqm3cdgm5nww7nk5p9azdirj842p8hl")))

(define-public crate-idx_file-0.50.2 (c (n "idx_file") (v "0.50.2") (d (list (d (n "avltriee") (r "^0.63.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "19yy3h67wgamwxlsa9v56cdnrsb5f6bf6hqhz6nyhz6l3gk0fsjj")))

(define-public crate-idx_file-0.51.0 (c (n "idx_file") (v "0.51.0") (d (list (d (n "avltriee") (r "^0.64.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "0116knrf6vcdrwm948i37iy62y87njz2c8sz0ij9rk8x3bkbqi19")))

(define-public crate-idx_file-0.51.1 (c (n "idx_file") (v "0.51.1") (d (list (d (n "avltriee") (r "^0.65.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "0vhljpvhz9gz7xs3vnxl546s0nxm7lvcc2afbriz1kcg9vy58ld8")))

(define-public crate-idx_file-0.52.0 (c (n "idx_file") (v "0.52.0") (d (list (d (n "avltriee") (r "^0.66.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "0krs307jv05lrcyvvmqzdfs57z2xnvq8bgjcd9rybbb7pbr3psms")))

(define-public crate-idx_file-0.53.0 (c (n "idx_file") (v "0.53.0") (d (list (d (n "avltriee") (r "^0.66.1") (d #t) (k 0)) (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "0ff1kxw10m3vnghijy1sa58fgb66n6pwlgpkl654i3f9nffkwdvd")))

(define-public crate-idx_file-0.54.0 (c (n "idx_file") (v "0.54.0") (d (list (d (n "avltriee") (r "^0.67.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "0w8n93hvwc45lybcm0fgdf7ph7hv6xkdrh2s0ymrxhsdx2vxi0nh")))

(define-public crate-idx_file-0.55.0 (c (n "idx_file") (v "0.55.0") (d (list (d (n "avltriee") (r "^0.68.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "0wjkp8a0p4gqpkjd3a8m2jd2zh5cqaxn6vgvn2y9dc4g34y2f7db")))

(define-public crate-idx_file-0.56.0 (c (n "idx_file") (v "0.56.0") (d (list (d (n "avltriee") (r "^0.69.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "0mv702qmz82vazl4nv8fjk95qr2hzbnv5qps9ml2hcsmfhy71vys")))

(define-public crate-idx_file-0.57.0 (c (n "idx_file") (v "0.57.0") (d (list (d (n "avltriee") (r "^0.70.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "1b7p18lfaz3cs4z1hgddbhapgvvpaf15hz765vlnnzy6las2pkdi")))

(define-public crate-idx_file-0.58.0 (c (n "idx_file") (v "0.58.0") (d (list (d (n "avltriee") (r "^0.71.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "0z2ds3pdkldzmfv6cfplj4m8l07vhnx99qz88z95xharbcncnpjw")))

(define-public crate-idx_file-0.59.0 (c (n "idx_file") (v "0.59.0") (d (list (d (n "avltriee") (r "^0.72.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "07wmx9vlgzr8vgh32g9r8vflai5xdiglihm7yhx9kqz4rqp3jf0q")))

(define-public crate-idx_file-0.60.0 (c (n "idx_file") (v "0.60.0") (d (list (d (n "avltriee") (r "^0.73.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "120b1mnsiab7csmia4q3hgdwff5agxnpjyqm3agqphkyz2wf8yja")))

(define-public crate-idx_file-0.61.0 (c (n "idx_file") (v "0.61.0") (d (list (d (n "avltriee") (r "^0.74.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "0rigrxmrdzaq6l2ssdb1g4iczmqzm3pgq4srwc65w737ngv948di")))

(define-public crate-idx_file-0.62.0 (c (n "idx_file") (v "0.62.0") (d (list (d (n "avltriee") (r "^0.75.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "1lfg73gc8f4v9p8d0q95d19dcq7qy16pls8w29iaj2pcli4wv21b")))

(define-public crate-idx_file-0.63.0 (c (n "idx_file") (v "0.63.0") (d (list (d (n "avltriee") (r "^0.76.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "1xdjm7zsiv0qxwv6nhvsq2g9gwij0acfs3cngiczxsmcr72wqwn8")))

(define-public crate-idx_file-0.64.0 (c (n "idx_file") (v "0.64.0") (d (list (d (n "avltriee") (r "^0.77.0") (d #t) (k 0)) (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)))) (h "0v26y5g0s6a877drnka0zvcssy4sihzh8iph2q9s5f7ljxrwxj39")))

