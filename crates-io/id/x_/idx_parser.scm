(define-module (crates-io id x_ idx_parser) #:use-module (crates-io))

(define-public crate-idx_parser-0.1.0 (c (n "idx_parser") (v "0.1.0") (h "19wsa0mrkad6dfk200g41lsxn6qiai9762pr0l47nfvyn9mph2rv") (y #t)))

(define-public crate-idx_parser-0.1.1 (c (n "idx_parser") (v "0.1.1") (h "07f5bcfiqhw3hqd02jrzc0vmffs4xl7642y5xndyvjwn55d0g16b") (y #t)))

(define-public crate-idx_parser-0.2.0 (c (n "idx_parser") (v "0.2.0") (h "1jz2diimr7wryd2p3ax52534z46y0iiwwx3kysmdrjyznv346g0p")))

(define-public crate-idx_parser-0.2.1 (c (n "idx_parser") (v "0.2.1") (h "0i0v0dp0bnvf7zh8rnra6632csgl2wkwriak7mhlrrfw7hs4ypam")))

(define-public crate-idx_parser-0.3.0 (c (n "idx_parser") (v "0.3.0") (h "1shinbpp8azplsdrhkxlck0a51z5a15hbv5x98vz7jdpqh2w6b66")))

