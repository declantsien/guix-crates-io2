(define-module (crates-io id x_ idx_binary) #:use-module (crates-io))

(define-public crate-idx_binary-0.1.0 (c (n "idx_binary") (v "0.1.0") (d (list (d (n "idx_sized") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "various_data_file") (r "^0.1") (d #t) (k 0)))) (h "1zm4906n8dxqryajpld8kb2fhbg6cxw28r4x6ipsnc87jsgzv4c6") (y #t)))

(define-public crate-idx_binary-0.2.0 (c (n "idx_binary") (v "0.2.0") (d (list (d (n "idx_sized") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "various_data_file") (r "^0.2") (d #t) (k 0)))) (h "1mn814043lkzw6mjhzp9358x4yz0xmx758ij8xh5lcfc8slggack") (y #t)))

(define-public crate-idx_binary-0.2.1 (c (n "idx_binary") (v "0.2.1") (d (list (d (n "idx_sized") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "various_data_file") (r "^0.2") (d #t) (k 0)))) (h "02mlkd7nvcw5vdvidal543mwcwv7x10bkk7ifsxhvv8ch9mgxv0p") (y #t)))

(define-public crate-idx_binary-0.3.0 (c (n "idx_binary") (v "0.3.0") (d (list (d (n "idx_sized") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "various_data_file") (r "^0.2") (d #t) (k 0)))) (h "012w7yd1qzw8hihvjx6zq6n3yrzqgc128kmhqxl3mgxyhfwvlq51") (y #t)))

(define-public crate-idx_binary-0.3.1 (c (n "idx_binary") (v "0.3.1") (d (list (d (n "idx_sized") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "various_data_file") (r "^0.3") (d #t) (k 0)))) (h "00wlh7bcn9kz6m9hq9d03vnb1q8wiqr7rkm2rxar9gggylf3rm04") (y #t)))

(define-public crate-idx_binary-0.3.2 (c (n "idx_binary") (v "0.3.2") (d (list (d (n "idx_sized") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "various_data_file") (r "^0.3") (d #t) (k 0)))) (h "1kkarlg4wp056x9hgzb1ny2cd8jmhnp817y7lrngi8iw2hkh1a78") (y #t)))

(define-public crate-idx_binary-0.3.3 (c (n "idx_binary") (v "0.3.3") (d (list (d (n "idx_sized") (r "^0.10") (d #t) (k 0)) (d (n "various_data_file") (r "^0.3") (d #t) (k 0)))) (h "0k8lmsiyl2xxzw5j54fyvrvd9b7xglfcpqiyv0qmix19ab8jq9b6") (y #t)))

(define-public crate-idx_binary-0.3.4 (c (n "idx_binary") (v "0.3.4") (d (list (d (n "idx_sized") (r "^0.11") (d #t) (k 0)) (d (n "various_data_file") (r "^0.3") (d #t) (k 0)))) (h "0jm0gg7p4almwh1mcbafhw629a1wlc1sv2qzxdwn9gj2r6zdk5g0") (y #t)))

(define-public crate-idx_binary-0.3.6 (c (n "idx_binary") (v "0.3.6") (d (list (d (n "idx_sized") (r "^0.11") (d #t) (k 0)) (d (n "various_data_file") (r "^0.3") (d #t) (k 0)))) (h "1jx78d6g10y613d064gvkmr9gxy4i16x3hf4g4lqx2rzcllirx63") (y #t)))

(define-public crate-idx_binary-0.3.7 (c (n "idx_binary") (v "0.3.7") (d (list (d (n "idx_sized") (r "^0.12") (d #t) (k 0)) (d (n "various_data_file") (r "^0.3") (d #t) (k 0)))) (h "1d2rf59bnpkys94h22haqmkz2qw06cx1r29q2q136n2lwblijsn3") (y #t)))

(define-public crate-idx_binary-0.4.0 (c (n "idx_binary") (v "0.4.0") (d (list (d (n "idx_sized") (r "^0.12") (d #t) (k 0)) (d (n "various_data_file") (r "^0.3") (d #t) (k 0)))) (h "17l46v2l37m8gv4p89vws4ry7m3kaf2bjyk46d5wfia1l9gka93f") (y #t)))

(define-public crate-idx_binary-0.5.0 (c (n "idx_binary") (v "0.5.0") (d (list (d (n "idx_sized") (r "^0.12") (d #t) (k 0)) (d (n "various_data_file") (r "^0.4") (d #t) (k 0)))) (h "0gljgdcw7w3f51vsmfifjk2jka9xrm39prilb7y7bv39cyyaicaj") (y #t)))

(define-public crate-idx_binary-0.6.0 (c (n "idx_binary") (v "0.6.0") (d (list (d (n "idx_sized") (r "^0.12") (d #t) (k 0)) (d (n "various_data_file") (r "^0.4") (d #t) (k 0)))) (h "0033j21y91wskfr3pv3r6mz2q28cgg6sm853l3q9gbrk8adrd6jg") (y #t)))

(define-public crate-idx_binary-0.7.0 (c (n "idx_binary") (v "0.7.0") (d (list (d (n "idx_sized") (r "^0.13") (d #t) (k 0)) (d (n "various_data_file") (r "^0.5") (d #t) (k 0)))) (h "0qagk6xkdlnhpsv8nfgwfgldyp2vcick8b519pahfjkdl2390qfj") (y #t)))

(define-public crate-idx_binary-0.7.1 (c (n "idx_binary") (v "0.7.1") (d (list (d (n "idx_sized") (r "^0.13") (d #t) (k 0)) (d (n "various_data_file") (r "^0.6") (d #t) (k 0)))) (h "1n60vfmg4ncqpkmjgbga07dp9lqg57s2c6skd8ldcdl1p56f27g9") (y #t)))

(define-public crate-idx_binary-0.8.0 (c (n "idx_binary") (v "0.8.0") (d (list (d (n "idx_sized") (r "^0.13") (d #t) (k 0)) (d (n "various_data_file") (r "^0.7") (d #t) (k 0)))) (h "0dgpd1n8nsvr64db7ni5fz08gs9b24ss860hz75cdx2v09gmrbki") (y #t)))

(define-public crate-idx_binary-0.8.1 (c (n "idx_binary") (v "0.8.1") (d (list (d (n "idx_sized") (r "^0.14") (d #t) (k 0)) (d (n "various_data_file") (r "^0.7") (d #t) (k 0)))) (h "1pqkrfvg77vpv3w49xy0pg60vpq0apr7ya0bazdn0qi62vfwms0n") (y #t)))

(define-public crate-idx_binary-0.8.2 (c (n "idx_binary") (v "0.8.2") (d (list (d (n "idx_sized") (r "^0.15") (d #t) (k 0)) (d (n "various_data_file") (r "^0.7") (d #t) (k 0)))) (h "0abvjk0a67lm51pbnrnm8yxm7wsp7bjwb3cb6jb7sgjajqwhkfj0") (y #t)))

(define-public crate-idx_binary-0.8.3 (c (n "idx_binary") (v "0.8.3") (d (list (d (n "idx_sized") (r "^0.16") (d #t) (k 0)) (d (n "various_data_file") (r "^0.7") (d #t) (k 0)))) (h "0jgpwsx3vrwjfcy797qay7kpdcq7yp52fwsfk456r18758p89lg1") (y #t)))

(define-public crate-idx_binary-0.8.4 (c (n "idx_binary") (v "0.8.4") (d (list (d (n "idx_sized") (r "^0.17") (d #t) (k 0)) (d (n "various_data_file") (r "^0.7") (d #t) (k 0)))) (h "1wmc5dh0d508q4afzfisl7ak7q5bvhh993r804b02wwgambvm114") (y #t)))

(define-public crate-idx_binary-0.8.5 (c (n "idx_binary") (v "0.8.5") (d (list (d (n "idx_sized") (r "^0.18") (d #t) (k 0)) (d (n "various_data_file") (r "^0.7") (d #t) (k 0)))) (h "161prwbrgph7qvfavxhsy889qfkb07dag9m6k72ngxs64m6cg468") (y #t)))

(define-public crate-idx_binary-0.8.6 (c (n "idx_binary") (v "0.8.6") (d (list (d (n "idx_sized") (r "^0.18") (d #t) (k 0)) (d (n "various_data_file") (r "^0.7") (d #t) (k 0)))) (h "16rxy1x0z21wlddhc9h6ams108ira0m457fqsb51fwnn3m5kvwl1") (y #t)))

(define-public crate-idx_binary-0.9.0 (c (n "idx_binary") (v "0.9.0") (d (list (d (n "idx_sized") (r "^0.19") (d #t) (k 0)) (d (n "various_data_file") (r "^0.8") (d #t) (k 0)))) (h "0ym4iv6755n6if95kdx63040ym667ws4zwyz0cdcvrcnn9fg5bc6") (y #t)))

(define-public crate-idx_binary-0.10.0 (c (n "idx_binary") (v "0.10.0") (d (list (d (n "idx_sized") (r "^0.19") (d #t) (k 0)) (d (n "various_data_file") (r "^0.8") (d #t) (k 0)))) (h "0ndlbhk4yf1j2b92bxrl5sxq2cf6a6nn406qrw623in5c7if80g0") (y #t)))

(define-public crate-idx_binary-0.10.1 (c (n "idx_binary") (v "0.10.1") (d (list (d (n "idx_sized") (r "^0.20") (d #t) (k 0)) (d (n "various_data_file") (r "^0.8") (d #t) (k 0)))) (h "0x0ifyikhp95br42wdyx2fz0vgbgmazd25ql6ffw1w9hf4982n2g") (y #t)))

(define-public crate-idx_binary-0.10.2 (c (n "idx_binary") (v "0.10.2") (d (list (d (n "idx_sized") (r "^0.20") (d #t) (k 0)) (d (n "various_data_file") (r "^0.9") (d #t) (k 0)))) (h "068kc699c93vagv2q4a25whjgvyj5xcj5ms2d2h185k3z4hfix5n") (y #t)))

(define-public crate-idx_binary-0.10.3 (c (n "idx_binary") (v "0.10.3") (d (list (d (n "idx_sized") (r "^0.21") (d #t) (k 0)) (d (n "various_data_file") (r "^0.9") (d #t) (k 0)))) (h "0gcbhcw89yy44rp29v4g50hdd9aj6nv04fvxrbm22k2jr1vrh82f") (y #t)))

(define-public crate-idx_binary-0.10.4 (c (n "idx_binary") (v "0.10.4") (d (list (d (n "idx_sized") (r "^0.21") (d #t) (k 0)) (d (n "various_data_file") (r "^0.9") (d #t) (k 0)))) (h "0cqf4cfl3djbx7sjbyrwyr5vjbwv4ccd21vifrsxj9fqdzhy2yj2") (y #t)))

(define-public crate-idx_binary-0.10.5 (c (n "idx_binary") (v "0.10.5") (d (list (d (n "idx_sized") (r "^0.21") (d #t) (k 0)) (d (n "various_data_file") (r "^0.9") (d #t) (k 0)))) (h "02l3wym595yzbwan5insxhaa9m10zwa48v7slqanki5shpq06z4g") (y #t)))

(define-public crate-idx_binary-0.11.0 (c (n "idx_binary") (v "0.11.0") (d (list (d (n "idx_sized") (r "^0.21") (d #t) (k 0)) (d (n "various_data_file") (r "^0.9") (d #t) (k 0)))) (h "1cl00fqlck1fjj752lz2800vdbl2rzqvx7vq794hnx1c6nfdbiw8") (y #t)))

(define-public crate-idx_binary-0.11.1 (c (n "idx_binary") (v "0.11.1") (d (list (d (n "idx_sized") (r "^0.21") (d #t) (k 0)) (d (n "various_data_file") (r "^0.10") (d #t) (k 0)))) (h "0gnia30h0czbm1v0ra3a4rdk4gs9957m65hcpafq27vzk60cwsyr") (y #t)))

(define-public crate-idx_binary-0.11.2 (c (n "idx_binary") (v "0.11.2") (d (list (d (n "idx_sized") (r "^0.22") (d #t) (k 0)) (d (n "various_data_file") (r "^0.10") (d #t) (k 0)))) (h "1fsqdyyklgpfxylgzci77anfn34s8vr6gygg0jcvw2cpscnhnd4j") (y #t)))

(define-public crate-idx_binary-0.11.3 (c (n "idx_binary") (v "0.11.3") (d (list (d (n "idx_sized") (r "^0.23") (d #t) (k 0)) (d (n "various_data_file") (r "^0.10") (d #t) (k 0)))) (h "0skcbnqb6da2029smx2xmk0bnm2c8p0rhjd4hrkllw3pkxzwbfqp") (y #t)))

(define-public crate-idx_binary-0.12.0 (c (n "idx_binary") (v "0.12.0") (d (list (d (n "idx_sized") (r "^0.24") (d #t) (k 0)) (d (n "various_data_file") (r "^0.10") (d #t) (k 0)))) (h "10kd6a50mw9m3xc7gaijcgq8dfxzmljjhp6h39mjqbzvc9wm6c57") (y #t)))

(define-public crate-idx_binary-0.13.0 (c (n "idx_binary") (v "0.13.0") (d (list (d (n "idx_sized") (r "^0.25") (d #t) (k 0)) (d (n "various_data_file") (r "^0.10") (d #t) (k 0)))) (h "06cb92sas0qp6pfxk3ngvyyd78ziblr8k99h6f6jdy2qpzks3qhr") (y #t)))

(define-public crate-idx_binary-0.13.1 (c (n "idx_binary") (v "0.13.1") (d (list (d (n "idx_sized") (r "^0.26") (d #t) (k 0)) (d (n "various_data_file") (r "^0.10") (d #t) (k 0)))) (h "01ygdlya7gfc42lqfdf7qivf2kgcnb9xj117fh570y3d173fg6w9") (y #t)))

(define-public crate-idx_binary-0.13.2 (c (n "idx_binary") (v "0.13.2") (d (list (d (n "idx_sized") (r "^0.27") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "00wrzxl10say6pzkldz3dilqf7svm3pamvibnxj37zk9abixnqbg") (y #t)))

(define-public crate-idx_binary-0.13.3 (c (n "idx_binary") (v "0.13.3") (d (list (d (n "idx_sized") (r "^0.28") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "0kfjcqhmx50bsijpnviqrd2wdkmn6csdiglf4nyhrwaj5k61gfli") (y #t)))

(define-public crate-idx_binary-0.13.4 (c (n "idx_binary") (v "0.13.4") (d (list (d (n "idx_file") (r "^0.29") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "09bynm2vbaaiaj1b5lh5k555vscsfxwkfaq89wxxc2mldnpnbnca") (y #t)))

(define-public crate-idx_binary-0.13.5 (c (n "idx_binary") (v "0.13.5") (d (list (d (n "idx_file") (r "^0.30") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "02gz65jkssrl6iz6x5ifg2rplv7knj7zgrb7wx4fclxhqq0a8fpr") (y #t)))

(define-public crate-idx_binary-0.14.0 (c (n "idx_binary") (v "0.14.0") (d (list (d (n "idx_file") (r "^0.30") (d #t) (k 0)) (d (n "natord") (r "^1.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "0d4w9r7rfy9965v6m294dqajlhdzcchari5wp3y8wsimgdzbxjbp") (y #t)))

(define-public crate-idx_binary-0.14.1 (c (n "idx_binary") (v "0.14.1") (d (list (d (n "idx_file") (r "^0.31") (d #t) (k 0)) (d (n "natord") (r "^1.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "1va8p7jd1vvcvgyfgmx72sh51q8qk0kv1fnyckblb0r4dff44ac7") (y #t)))

(define-public crate-idx_binary-0.15.0 (c (n "idx_binary") (v "0.15.0") (d (list (d (n "idx_file") (r "^0.31") (d #t) (k 0)) (d (n "natord") (r "^1.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "164ag6cgs5ii6x51hfzafmk78hzmayxfwbfp61rhkbzkl5pn0xx3") (y #t)))

(define-public crate-idx_binary-0.15.1 (c (n "idx_binary") (v "0.15.1") (d (list (d (n "idx_file") (r "^0.32") (d #t) (k 0)) (d (n "natord") (r "^1.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "0b795y3cv0k861d1k5fjhd28g0z1zq1xknghd2b8qx6y0cssvliw") (y #t)))

(define-public crate-idx_binary-0.15.2 (c (n "idx_binary") (v "0.15.2") (d (list (d (n "idx_file") (r "^0.33") (d #t) (k 0)) (d (n "natord") (r "^1.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "0gdxalxx9jx9vr043igcgc7vykcvm83wlnjzlswkk9bk72rfyc0l") (y #t)))

(define-public crate-idx_binary-0.16.0 (c (n "idx_binary") (v "0.16.0") (d (list (d (n "idx_file") (r "^0.34") (d #t) (k 0)) (d (n "natord") (r "^1.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "0vkznwknm8alw23197czp4c37gdiwv9a6bla3nc0s49mk163sb9i") (y #t)))

(define-public crate-idx_binary-0.17.0 (c (n "idx_binary") (v "0.17.0") (d (list (d (n "human-sort") (r "^0.2.2") (d #t) (k 0)) (d (n "idx_file") (r "^0.34") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "0fqd6ahqqqnrwybwmkwqi8c3gpapipc2nixaw3sqac831d34qr1v") (y #t)))

(define-public crate-idx_binary-0.18.0 (c (n "idx_binary") (v "0.18.0") (d (list (d (n "idx_file") (r "^0.34") (d #t) (k 0)) (d (n "natord") (r "^1.0.9") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "1gs8852vdy94irq9dn654iklcdmdvjdl9406flpfr8lfsidjpcak") (y #t)))

(define-public crate-idx_binary-0.18.1 (c (n "idx_binary") (v "0.18.1") (d (list (d (n "idx_file") (r "^0.34") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "1qyfw7smm8ifgaji2xgihdv13zmiiipmg4bprcn7k0x4zxx9siqh") (y #t)))

(define-public crate-idx_binary-0.18.2 (c (n "idx_binary") (v "0.18.2") (d (list (d (n "idx_file") (r "^0.35") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "0rjbjg7bzxvqbq2sim9sjzvryg6dfhq7ihrarhg56pvjg0mm5v1h") (y #t)))

(define-public crate-idx_binary-0.18.3 (c (n "idx_binary") (v "0.18.3") (d (list (d (n "idx_file") (r "^0.36") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "1jj2a1sisfz9yqwzpnrbcm87z14w2ikfz85chqa3mf2w0x6ldgkx")))

(define-public crate-idx_binary-0.19.0 (c (n "idx_binary") (v "0.19.0") (d (list (d (n "idx_file") (r "^0.37") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "1rc3jixkjdsgid674f7w0w0ma9j9g9yaywmhwj3bl99gc75c2d9w")))

(define-public crate-idx_binary-0.19.1 (c (n "idx_binary") (v "0.19.1") (d (list (d (n "idx_file") (r "^0.37") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "1wapwmib4fhai1x2l6xjx3m63cp510frkqpxbaryj4si7y4zgryi")))

(define-public crate-idx_binary-0.19.2 (c (n "idx_binary") (v "0.19.2") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "idx_file") (r "^0.38") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "0sf7kignqs347419ygxgipsmhw9y43mnvpdw6m3hr9hx24fj681y")))

(define-public crate-idx_binary-0.19.3 (c (n "idx_binary") (v "0.19.3") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "idx_file") (r "^0.38") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "0lzpqp4rzwzq5cdq6bcami541x8v6w58qk591kn9kgsdbpr60w94")))

(define-public crate-idx_binary-0.20.0 (c (n "idx_binary") (v "0.20.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "idx_file") (r "^0.38") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "1xcj9ylhyz4g8p9x4g7jrs2nyy5wajgbqa292w360z2zfy44yssp")))

(define-public crate-idx_binary-0.21.0 (c (n "idx_binary") (v "0.21.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "idx_file") (r "^0.39") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "02q1z5ihagw41vs1gfwqa10a4zv5bbfj23bjp65nk2kswk0swn7h")))

(define-public crate-idx_binary-0.22.0 (c (n "idx_binary") (v "0.22.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "idx_file") (r "^0.40") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "1jfbyv6x37xk1bwj6cjvwl1pjx0dg0dbrrdawdpi48k4rfcqrqgz")))

(define-public crate-idx_binary-0.22.1 (c (n "idx_binary") (v "0.22.1") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "idx_file") (r "^0.41") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "06991x1i0h9fzjv6vmd2jcpw3rz7a8zjavsz8250pgnlifxhf16p")))

(define-public crate-idx_binary-0.23.0 (c (n "idx_binary") (v "0.23.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "idx_file") (r "^0.42") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "1j4awf54dq5m8rsdz2jasaa5xbbhqp8f8c50j42m48sv1qzsh6as")))

(define-public crate-idx_binary-0.24.0 (c (n "idx_binary") (v "0.24.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "idx_file") (r "^0.43") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "1qvfq9dvs6rb931nz1qa1is6q6r7p93ahznl4hb32c5ggc1vlg45")))

(define-public crate-idx_binary-0.25.0 (c (n "idx_binary") (v "0.25.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "idx_file") (r "^0.44") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "1vgi3xgi5jg4sn1nisb4q3yc2f9sn4qz0wi8zfg08y4y7xvgnq0i")))

(define-public crate-idx_binary-0.26.0 (c (n "idx_binary") (v "0.26.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "idx_file") (r "^0.45") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "0i5142gd4x8x8h6f287gz8i0igy4r26pgx2wyc8h74d9wpphli0l")))

(define-public crate-idx_binary-0.27.0 (c (n "idx_binary") (v "0.27.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "idx_file") (r "^0.46") (d #t) (k 0)) (d (n "various_data_file") (r "^0.13") (d #t) (k 0)))) (h "01pxwar4lk0xv2p0pp22ksv4v38q4dac71arflgimsxgpsihd9s1")))

(define-public crate-idx_binary-0.27.1 (c (n "idx_binary") (v "0.27.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "idx_file") (r "^0.46") (d #t) (k 0)) (d (n "various_data_file") (r "^0.13") (d #t) (k 0)))) (h "04b5kskqqh1xjs6cnvq6994fk7z6avqy0qq5vvfzpba8ylhfhyys")))

(define-public crate-idx_binary-0.27.2 (c (n "idx_binary") (v "0.27.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "idx_file") (r "^0.46") (d #t) (k 0)) (d (n "various_data_file") (r "^0.13") (d #t) (k 0)))) (h "1vain55pn0rm5fizzrw6y71clhz56zkkaafm2x8y8ckim1hz6iim")))

(define-public crate-idx_binary-0.27.3 (c (n "idx_binary") (v "0.27.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "idx_file") (r "^0.46") (d #t) (k 0)) (d (n "various_data_file") (r "^0.13") (d #t) (k 0)))) (h "1f3zp0bjlbx6fj4g1a91lgqcin4rvciai6g6lnr03r1hgmh79iv1")))

(define-public crate-idx_binary-0.27.4 (c (n "idx_binary") (v "0.27.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "idx_file") (r "^0.46") (d #t) (k 0)) (d (n "various_data_file") (r "^0.13") (d #t) (k 0)))) (h "0nn2yd3lmbbdpwqf05iizdlx3jyw8bqqwzyifb78qd7zhhi6spq5")))

(define-public crate-idx_binary-0.27.5 (c (n "idx_binary") (v "0.27.5") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.46") (d #t) (k 0)) (d (n "various_data_file") (r "^0.13") (d #t) (k 0)))) (h "1dcdbkl9k464558nfmxxyvpg4m2nln2nllsvki1s9q7mvhxwyy6a")))

(define-public crate-idx_binary-0.28.0 (c (n "idx_binary") (v "0.28.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.47.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.13.3") (d #t) (k 0)))) (h "13xwm47a19ziccxl441cs192f2iirbyhqk40xzpi44i38mhyh6ij")))

(define-public crate-idx_binary-0.28.1 (c (n "idx_binary") (v "0.28.1") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.47.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.14.0") (d #t) (k 0)))) (h "0y11mjx8702wcfxmv5s9alm78fqi0l5jvg0621pn9rl840862z41")))

(define-public crate-idx_binary-0.28.2 (c (n "idx_binary") (v "0.28.2") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.48.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.14.0") (d #t) (k 0)))) (h "1a7aa656bqxg3nqpkn4lw3s8agnm786iz3n07dgiqiwh0qr14bpl")))

(define-public crate-idx_binary-0.29.0 (c (n "idx_binary") (v "0.29.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.49.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.14.0") (d #t) (k 0)))) (h "0ynj0r2bdd6b8n536qbg83aqaz4mayq26g7q3hzzq7vj2vvxxm1k")))

(define-public crate-idx_binary-0.29.1 (c (n "idx_binary") (v "0.29.1") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.49.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.15.0") (d #t) (k 0)))) (h "1zn925qj976qrz8v65vgicn0x5wgsljyydic16ziavdfzm1k5kda")))

(define-public crate-idx_binary-0.30.0 (c (n "idx_binary") (v "0.30.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.50.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.16.0") (d #t) (k 0)))) (h "1x8wj2b5xybiz62hz3j5alfrbgzw2g8k29g03xb6inc7y4g36c7s")))

(define-public crate-idx_binary-0.31.0 (c (n "idx_binary") (v "0.31.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.51.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.16.0") (d #t) (k 0)))) (h "1cxcci7qhlhhsf1slx0dcl303rjdayzican4wjakvyzb4z9r9iyi")))

(define-public crate-idx_binary-0.32.0 (c (n "idx_binary") (v "0.32.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.52.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.16.1") (d #t) (k 0)))) (h "1ijd2r1dwbicxr7s3yha04bjf1rnx0ir4qda47d7srlbwxk5vi3z")))

(define-public crate-idx_binary-0.33.0 (c (n "idx_binary") (v "0.33.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.53.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "04hp55kvmy28i375rpvw2ygv6y785nxjjgkp5gvhnjri8xpkkqix")))

(define-public crate-idx_binary-0.33.1 (c (n "idx_binary") (v "0.33.1") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.53.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "0yc91mgr99dycb2vhrxhqx8p2i03m4zlww5i49k16p6209qns8wj")))

(define-public crate-idx_binary-0.33.2 (c (n "idx_binary") (v "0.33.2") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.54.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "150lfg06is9ipq2ixngnc15nmwazk8qhs51cpazfpjpidx7301fi")))

(define-public crate-idx_binary-0.34.0 (c (n "idx_binary") (v "0.34.0") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.55.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "18hafzzbf4cnwvgxxk91pwhblm0y3hjfmc365ard27z92lbpblrx")))

(define-public crate-idx_binary-0.34.1 (c (n "idx_binary") (v "0.34.1") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.55.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "1r3l3m7gbh14m4fq6f3xz44zhcirrw4hgfzg3z4xsh93clxpnk09")))

(define-public crate-idx_binary-0.35.0 (c (n "idx_binary") (v "0.35.0") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.56.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "0nc8agbk7yjnxk0dcrk7xfz1d0f16ab6sbf8n08wflanymb2knhf")))

(define-public crate-idx_binary-0.36.0 (c (n "idx_binary") (v "0.36.0") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.57.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "0mf2x1aq2zpybs5i7jywaa0r06xjv9z1vn5zbqn0v3wgg0m5in0a")))

(define-public crate-idx_binary-0.36.1 (c (n "idx_binary") (v "0.36.1") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.58.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "0611zghn5bwf8rgf5v2jdrd5cgj6adh99dm0cs28pciv45m8qxv6")))

(define-public crate-idx_binary-0.37.0 (c (n "idx_binary") (v "0.37.0") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.59.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "168gh9p8q8yv4y07zjvx37d0li1l99x2lys1y8xra0qgvlf31a8x")))

(define-public crate-idx_binary-0.37.1 (c (n "idx_binary") (v "0.37.1") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.59.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.18.0") (d #t) (k 0)))) (h "0jpw2sjpgdl6ynnv0nsw6hmc672shrsn88jwxkc7jp63zrpb9z1c")))

(define-public crate-idx_binary-0.38.0 (c (n "idx_binary") (v "0.38.0") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.60.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.18.0") (d #t) (k 0)))) (h "1zihdp5v7a0yfvh0c31647ap8f4if4d8nybkhkyqdlr63wiqs8cc")))

(define-public crate-idx_binary-0.38.1 (c (n "idx_binary") (v "0.38.1") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.61.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.18.0") (d #t) (k 0)))) (h "1r8k8nw6shn50djhy876pwm7bjvf1nxlx4080w72p8xv373h5saj")))

(define-public crate-idx_binary-0.38.2 (c (n "idx_binary") (v "0.38.2") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.62.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.18.0") (d #t) (k 0)))) (h "0k652z6wnb5kh7023kzqz5h19j61h3cpdyngrds4k66i1xakn9a1")))

(define-public crate-idx_binary-0.38.3 (c (n "idx_binary") (v "0.38.3") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.63.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.18.0") (d #t) (k 0)))) (h "12y4kzxaasg2q74d14kgkvy6yskcjs93nwcid0jkbbjj52n4xb5r")))

(define-public crate-idx_binary-0.38.4 (c (n "idx_binary") (v "0.38.4") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.64.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.18.0") (d #t) (k 0)))) (h "1n6k7lak1qfa4i9cid3qp2q9lyw6j7aiz4gi87f29smgzxf7lz6y")))

(define-public crate-idx_binary-0.38.5 (c (n "idx_binary") (v "0.38.5") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "idx_file") (r "^0.64.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.18.0") (d #t) (k 0)))) (h "18gv75ic5p6n4zqc1rynqajjspvbgr4435l4gcrngwklqcxrgy5m")))

