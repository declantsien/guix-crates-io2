(define-module (crates-io id po idpool) #:use-module (crates-io))

(define-public crate-idpool-0.9.9 (c (n "idpool") (v "0.9.9") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1m33x52z883cskb3k5n14pmz54izsvzy6f3gdhnar28n58vyisnm") (y #t)))

(define-public crate-idpool-1.0.0 (c (n "idpool") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (k 0)))) (h "0sw500gqs1a6iyypig5c9jznjjji42b2mrv8lcr05mlpwm235v2b") (y #t)))

