(define-module (crates-io id _c id_collections_derive) #:use-module (crates-io))

(define-public crate-id_collections_derive-0.1.0 (c (n "id_collections_derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16xbfh4khz6isrkkdd7kvlw6hicxfj06ahh2g1mvzfg6ca3mizi0")))

