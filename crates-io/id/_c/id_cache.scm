(define-module (crates-io id _c id_cache) #:use-module (crates-io))

(define-public crate-id_cache-0.1.0 (c (n "id_cache") (v "0.1.0") (d (list (d (n "id_collections") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zniwkvq85i3aqfj21lk3vxi05blv9h9g2jp8f4sx6dllmszsagx") (s 2) (e (quote (("serde" "dep:serde" "id_collections/serde"))))))

