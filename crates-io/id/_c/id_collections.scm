(define-module (crates-io id _c id_collections) #:use-module (crates-io))

(define-public crate-id_collections-1.0.0 (c (n "id_collections") (v "1.0.0") (d (list (d (n "id_collections_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1iqv8gk0n30klrccwkvz6p2n6iysiyc12d2skcdzpnlnbsy29hw8")))

(define-public crate-id_collections-1.0.1 (c (n "id_collections") (v "1.0.1") (d (list (d (n "id_collections_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vk9y929cqa33m6hqiqx16sg4pjwx67wk8csz3m3vdxn8ic7nyak")))

