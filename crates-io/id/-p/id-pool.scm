(define-module (crates-io id -p id-pool) #:use-module (crates-io))

(define-public crate-id-pool-0.1.0 (c (n "id-pool") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)))) (h "120isjz3i3wcm27ibzd148y3r4dqyvzig1sb8bx2aviqqk71x83n") (f (quote (("usize") ("u64") ("u32") ("u16") ("default" "usize"))))))

(define-public crate-id-pool-0.2.0 (c (n "id-pool") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (o #t) (d #t) (k 0)))) (h "0qvnc40jdjb84jgbgpl412yhmcprvxr8cskgvq0ahibpn71pmda2") (f (quote (("usize") ("u64") ("u32") ("u16") ("default" "usize"))))))

(define-public crate-id-pool-0.2.1 (c (n "id-pool") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (o #t) (d #t) (k 0)))) (h "1c4wlzqq7r6j9d3c2isjikxbyn3dbr5670bpyvimkhq060p86csy") (f (quote (("usize") ("u64") ("u32") ("u16") ("default" "usize"))))))

(define-public crate-id-pool-0.2.2 (c (n "id-pool") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.138") (o #t) (d #t) (k 0)))) (h "0i5kzpcgifwfbgwxnfp1y234ap0jcwzkbq529bp2323ni96xzl58") (f (quote (("usize") ("u64") ("u32") ("u16") ("default" "usize"))))))

