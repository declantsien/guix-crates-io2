(define-module (crates-io id cu idcurl) #:use-module (crates-io))

(define-public crate-idcurl-0.1.0 (c (n "idcurl") (v "0.1.0") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0zj1a55jdzyf2mgpn6adyc9rf1qx94bsqibl670cmn8ym36wvchs")))

(define-public crate-idcurl-0.1.1 (c (n "idcurl") (v "0.1.1") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1x5bb36f0ji0a77nm4cscqw49p47n8bv031j00qpb0clgaaf8z5l")))

(define-public crate-idcurl-0.2.0 (c (n "idcurl") (v "0.2.0") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1886j0xnkabim66qcihsa1nwvqx9y33gh3i4q21r0cg1g37zxw8w")))

(define-public crate-idcurl-0.2.1 (c (n "idcurl") (v "0.2.1") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1an8zbp4hzj6fxnfb1jmvznz62wp3lnk0n8jy05pjdpy91sikhgl")))

(define-public crate-idcurl-0.2.2 (c (n "idcurl") (v "0.2.2") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0cir9vc3shqflz3967lqarhhmdl5y7wcy34lmnyxyvyc9aqin2dv")))

(define-public crate-idcurl-0.2.3 (c (n "idcurl") (v "0.2.3") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "15a57zgcalp0kmhcgwf9g2bb7y9wkm5yk750kzzawpxm0c2w3fvg")))

(define-public crate-idcurl-0.3.0 (c (n "idcurl") (v "0.3.0") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "0696lvllz8y4hzyqn45qjl9fpnk24vk6x14q2lgi1yynb775c8sk")))

(define-public crate-idcurl-0.3.1 (c (n "idcurl") (v "0.3.1") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "0np0a3fmzd3yx9iawynbb41x31qr72nc7a6kmrvsa5wkdm1ya92r")))

(define-public crate-idcurl-0.3.2 (c (n "idcurl") (v "0.3.2") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "02ngw8sf873hdc6qzsn2khaldxvi6afrl0hs51lfz99fkz6km9yf")))

(define-public crate-idcurl-0.4.0 (c (n "idcurl") (v "0.4.0") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "1wq7mvvg29ddj16wl1qxv42xs77s8bh92hk6azjqqdy3d218ks2y")))

(define-public crate-idcurl-0.4.1 (c (n "idcurl") (v "0.4.1") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "12gy2v70k05947dfj5jnkjn91pixdh0ijhsk42i7fdrb2hnyn4dx")))

(define-public crate-idcurl-0.4.2 (c (n "idcurl") (v "0.4.2") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "0s2yvadlm69xdrzp8gjiwkvvpjb9b2va6rqw26904y094s1rl94y")))

(define-public crate-idcurl-0.4.3 (c (n "idcurl") (v "0.4.3") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "0gaczbprq0zx91kax9c1qsi1kfv7g7x9qy0p063gla91bjx0bvk8")))

(define-public crate-idcurl-0.5.0 (c (n "idcurl") (v "0.5.0") (d (list (d (n "curl-sys") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "0awkijfwb9a5lryxj94z57fbdzrykr90gs864p5mrim0iy8bh6z8")))

