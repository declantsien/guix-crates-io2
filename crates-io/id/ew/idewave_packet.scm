(define-module (crates-io id ew idewave_packet) #:use-module (crates-io))

(define-public crate-idewave_packet-0.1.0 (c (n "idewave_packet") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "085hqlkqk0k3fd4rn4faw9dm778lqmr81cf0q6yfiybgwjcc4anj")))

(define-public crate-idewave_packet-1.0.0 (c (n "idewave_packet") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ddp18ngj4463fkw1lnnfqgy4sr5aqnry34d8xrxz85ls8pwrpxw")))

(define-public crate-idewave_packet-1.0.1 (c (n "idewave_packet") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12l4p97gzdr2qnz4bffscvf8a45c1r4wsy81k5zqhb1idfv6li7l")))

(define-public crate-idewave_packet-1.1.0 (c (n "idewave_packet") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full"))) (d #t) (k 0)))) (h "0p59zzswdh7gf2pd4bk7bbj2vj2vfnrrw20ijh2hrnkddllaszzr")))

(define-public crate-idewave_packet-1.1.1 (c (n "idewave_packet") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full"))) (d #t) (k 0)))) (h "1jx6a8rabb3pri1kws2jyn2ahbgkb6cjmz9pcgliq2p86zgdg442")))

(define-public crate-idewave_packet-1.2.0 (c (n "idewave_packet") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full"))) (d #t) (k 0)))) (h "1j0lbd3n7ghix9p0cps13djbhhmipz155w6bbrn7r4bpxnp0g7r4")))

(define-public crate-idewave_packet-1.2.1 (c (n "idewave_packet") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full"))) (d #t) (k 0)))) (h "1yfglbp3f2dz6n28yfcgzwhwafavy94kl1xjh8hcpm5ngshnz1sl")))

(define-public crate-idewave_packet-1.3.0 (c (n "idewave_packet") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full"))) (d #t) (k 0)))) (h "01nq8i1gxspaq8rl524bg01ynlazhlh972h5y499jxjqwzkzdf1w")))

(define-public crate-idewave_packet-1.3.1 (c (n "idewave_packet") (v "1.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full"))) (d #t) (k 0)) (d (n "tentacli") (r "^8.0.7") (d #t) (k 0)))) (h "0vcar2nlzl115qmhibzpvh2lchmgqzwafnwa5q74qkrwyhijsbhs")))

