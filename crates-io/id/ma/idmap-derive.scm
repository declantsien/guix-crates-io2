(define-module (crates-io id ma idmap-derive) #:use-module (crates-io))

(define-public crate-idmap-derive-0.1.0 (c (n "idmap-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0x33acrg178a0rdh4p53lvic32md30rrmd88xs0h1njnf3i3jrn0")))

(define-public crate-idmap-derive-0.1.1 (c (n "idmap-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "004zgcgjjd491ym551s4b8yjpjhvp1kgc2zr6zxrn9vp0p7z3jyb")))

(define-public crate-idmap-derive-0.1.2 (c (n "idmap-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0w1823war89km16b9z8y3qfig04qqwxwrlishgiffz8fnshlmx73")))

