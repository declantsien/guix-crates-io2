(define-module (crates-io id -c id-card) #:use-module (crates-io))

(define-public crate-id-card-0.1.0 (c (n "id-card") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fz2307wwkkj03xjy7i89hagli2bxhya7brsnq6f92dkvfhz9pzq")))

(define-public crate-id-card-0.1.1 (c (n "id-card") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1lyn5wj2163z42p626nz58k0wspc28jfnb0qljxh0m4mm9lwszng")))

(define-public crate-id-card-0.1.2 (c (n "id-card") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ydjnjc1g9060x3kl20hzs2r8h54daqs41xx74c23lvxpm0l7b9i")))

(define-public crate-id-card-0.1.3 (c (n "id-card") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05d08xh052iq7wvnm089q62vw4hg1agb9ndpdlsqpk84zyd5dxma")))

