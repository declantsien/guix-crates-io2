(define-module (crates-io id io idiom-solitaire) #:use-module (crates-io))

(define-public crate-idiom-solitaire-0.1.0 (c (n "idiom-solitaire") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pathfinding") (r "^2.0") (d #t) (k 0)) (d (n "pinyin") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng" "wasm-bindgen"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zlnkwr2ipmdl8m0dwlb18055kqpsza0acbfxnv68wwr66hbfnka") (f (quote (("default"))))))

