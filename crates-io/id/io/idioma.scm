(define-module (crates-io id io idioma) #:use-module (crates-io))

(define-public crate-idioma-0.0.1 (c (n "idioma") (v "0.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "13cxrkvbs3336757py9j8gwf5c17s3731znnkxy3fm0lq1xzngsp")))

(define-public crate-idioma-0.0.2 (c (n "idioma") (v "0.0.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0jf9r23iczjv6165kr12hxg244y658i8qhnp7h0pc04n831w31rb")))

(define-public crate-idioma-0.0.3 (c (n "idioma") (v "0.0.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "00ws34hh7g7mhb541y7m19628imyv1vmmr449y031vb9c96macbd")))

(define-public crate-idioma-1.0.0 (c (n "idioma") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "08cza29g2afwksab2z1n8zrxjps0qjd011g4mqzwcb935wsl6ar9")))

(define-public crate-idioma-1.1.0 (c (n "idioma") (v "1.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0gl5w88azcihj9i5a9z49rdcx6rfm548flm299l26cgr0mz27aag")))

