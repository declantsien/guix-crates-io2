(define-module (crates-io id _g id_graph_sccs) #:use-module (crates-io))

(define-public crate-id_graph_sccs-0.1.0 (c (n "id_graph_sccs") (v "0.1.0") (d (list (d (n "id_collections") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0wrzfkdk5p45613hpp2j9gnfbibp5rpkab410r63msrc4sspkjl3")))

(define-public crate-id_graph_sccs-0.1.1 (c (n "id_graph_sccs") (v "0.1.1") (d (list (d (n "id_collections") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "08x6ysr6nkfl8jqcf22sh885cg203vl54cpv7zg1lwja5w05msmn")))

