(define-module (crates-io id ba idbag) #:use-module (crates-io))

(define-public crate-idbag-0.1.0 (c (n "idbag") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0v7l9fnjx7v8im4fsr9m37jxhli9d11il1qv7d0irqd6hhrvxf6r") (f (quote (("dev-docs")))) (r "1.56")))

(define-public crate-idbag-0.1.1 (c (n "idbag") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0zq81qik8rz23np013j1gf1z4xybh7sd7hcxwclbyrw4xkpj7ri5") (f (quote (("dev-docs")))) (r "1.56")))

(define-public crate-idbag-0.1.2 (c (n "idbag") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0x33dw1fglbrjz6la3r68yw0jl4pwfispqlkl4zcdyiy0rk8b3kr") (r "1.56")))

