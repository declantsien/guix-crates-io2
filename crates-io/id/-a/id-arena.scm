(define-module (crates-io id -a id-arena) #:use-module (crates-io))

(define-public crate-id-arena-1.0.0 (c (n "id-arena") (v "1.0.0") (h "0rjbwv66dxi87gdd5sbxjvx4wfcm0amrz9lzfasf338ms1nykm7r") (f (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-1.0.1 (c (n "id-arena") (v "1.0.1") (h "01v1rxdbs5vqij3bk2zjj2rb35ip9dib7gzz2sixzb3bv7frw9k2") (f (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-1.0.2 (c (n "id-arena") (v "1.0.2") (h "0kgb6145as6jf6j43giiczs7n1sb227nlrprb3fddl5knc8fwab8") (f (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-2.0.0 (c (n "id-arena") (v "2.0.0") (h "0gl48a2p55yjppi9m37kck8c3bk4d9hw5lpc3imf9zpa7w1m0wis") (f (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-2.0.1 (c (n "id-arena") (v "2.0.1") (h "1i168jw9fix471y751ksni63fs5qwx5xssa20snamc9prc5p6xzp") (f (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-2.1.0 (c (n "id-arena") (v "2.1.0") (d (list (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)))) (h "1qvsbv08wlrmw2ksix2q5ff9776lnk0kj95mgjw4a6qljgmanpqq") (f (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-2.2.0 (c (n "id-arena") (v "2.2.0") (d (list (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)))) (h "0vnvjf7221bj1g895fb210npjl5vxnxzqx6wri59l4q4y2sqj3gh") (f (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-2.2.1 (c (n "id-arena") (v "2.2.1") (d (list (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)))) (h "01ch8jhpgnih8sawqs44fqsqpc7bzwgy0xpi6j0f4j0i5mkvr8i5") (f (quote (("std") ("default" "std"))))))

