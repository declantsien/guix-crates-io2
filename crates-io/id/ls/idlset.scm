(define-module (crates-io id ls idlset) #:use-module (crates-io))

(define-public crate-idlset-0.1.0 (c (n "idlset") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1j12bnllj5h7zh96hvfxsyp85gwj25w1d0cg43r8fspfkrsh4236")))

(define-public crate-idlset-0.1.1 (c (n "idlset") (v "0.1.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0ys9ygc7i69j6h4p9nvz57x1v7svsm16zw2bv4fkqqhrr3wjz18v")))

(define-public crate-idlset-0.1.2 (c (n "idlset") (v "0.1.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0lqlyl6hhwzdarknkbasv4hcwfpph797szmhpb8p0hlgmj090n5j")))

(define-public crate-idlset-0.1.3 (c (n "idlset") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1mgl4w61ig5jwc4cxfi0jqqmj0v5xy0514cz7qx4qyxchi80576m")))

(define-public crate-idlset-0.1.4 (c (n "idlset") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "10ac9vzxw3ib3lmzqy3saa54sq8diiqfla3718bnvldvc9jfb0w6")))

(define-public crate-idlset-0.1.5 (c (n "idlset") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "180ksij7z7gp3fx2xqqqnpgm6drxj340mrdl73819s1z20imk1s2") (f (quote (("use_smallvec"))))))

(define-public crate-idlset-0.1.6 (c (n "idlset") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0rmwdzzlkwh4f9knil0mbmgyigkw0rj8zrxibbc3c41bx1fym8s0") (f (quote (("use_smallvec"))))))

(define-public crate-idlset-0.1.7 (c (n "idlset") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1xfaz3pa2qj74zzyq351j29b990h4y124rv2sw912hpwmna1ydw6") (f (quote (("use_smallvec"))))))

(define-public crate-idlset-0.1.8 (c (n "idlset") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1sria4mlrvlaly50n36hk39ifz5jv37y2gwlyfyaia58ij3b962a") (f (quote (("use_smallvec"))))))

(define-public crate-idlset-0.1.9 (c (n "idlset") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "01bi38r2gfr6nbzcj63s8id6k8ink2nf29zipiqcz1rw1q05wcbl") (f (quote (("use_smallvec"))))))

(define-public crate-idlset-0.1.10 (c (n "idlset") (v "0.1.10") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "1qh25cl86gbjpk0i3w9192k1bivbawsb63qy3v1590grrfh1vxr2") (f (quote (("use_smallvec"))))))

(define-public crate-idlset-0.1.11 (c (n "idlset") (v "0.1.11") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "1bmb9jxa3inqfvpvhfxyafsmklh6ndglvk6lq4i9zzr3d7wi74dk") (f (quote (("use_smallvec"))))))

(define-public crate-idlset-0.1.12 (c (n "idlset") (v "0.1.12") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "02gk83vwsq5ixj3f60vzihi5fm51fz91wbiai6dl41q4qfckxpf0") (f (quote (("use_smallvec" "smallvec") ("tracksize"))))))

(define-public crate-idlset-0.1.13 (c (n "idlset") (v "0.1.13") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "0sjgd9yqnh651bfy84qxdrwlm6qbc4sxhaa20yacjbrw055m8ccw") (f (quote (("use_smallvec" "smallvec") ("tracksize"))))))

(define-public crate-idlset-0.2.0 (c (n "idlset") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (f (quote ("serde"))) (d #t) (k 0)))) (h "0nyzj5ccmb9zv98xqvy011m0wmbasbly6x5g2sw3lb0manbki7hv")))

(define-public crate-idlset-0.2.1 (c (n "idlset") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (f (quote ("serde"))) (d #t) (k 0)))) (h "17rv879yyk2l8c22i955iz8vmr3nvb0kapgn2bff35rdngzi14jp")))

(define-public crate-idlset-0.2.2 (c (n "idlset") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (f (quote ("serde"))) (d #t) (k 0)))) (h "1262rvq8vfd9vypqsxwcky4jk88v8w35zhbcg4waxr8g6z32j6p1")))

(define-public crate-idlset-0.2.3 (c (n "idlset") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (f (quote ("serde"))) (d #t) (k 0)))) (h "1b2fz8cxknyikgpq14bbc0fir621zw2j0vci60dg73g1lm12sqfi")))

(define-public crate-idlset-0.2.4 (c (n "idlset") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (f (quote ("serde"))) (d #t) (k 0)))) (h "1ins76c5wr2hwfpxdxnjnywwdkcgyslr1b8va1g2vcp4bg8mc1rl")))

