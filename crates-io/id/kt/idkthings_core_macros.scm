(define-module (crates-io id kt idkthings_core_macros) #:use-module (crates-io))

(define-public crate-idkthings_core_macros-0.1.0 (c (n "idkthings_core_macros") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full"))) (d #t) (k 0)))) (h "193fmz064x5rwqpy2x7p56kjl4yfk6jrp6gwr1ad7vhkgz34jyzp")))

