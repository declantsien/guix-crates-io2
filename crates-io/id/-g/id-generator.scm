(define-module (crates-io id -g id-generator) #:use-module (crates-io))

(define-public crate-id-generator-0.1.0 (c (n "id-generator") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "100ih8bjx73dm5xy9abrk891nivk4yfk542241qqmpkifjwfkza7")))

(define-public crate-id-generator-0.2.0 (c (n "id-generator") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1q4kanb9q2c4kam1vrvy0lwx7zimzjidxcd68jqnx2ylw1xidib3")))

(define-public crate-id-generator-0.3.0 (c (n "id-generator") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1ngb9qxdrwrpp5f38s6bdzmsva5m69441yyl0ypd2ivah99sqnma")))

