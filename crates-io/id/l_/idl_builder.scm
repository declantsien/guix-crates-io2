(define-module (crates-io id l_ idl_builder) #:use-module (crates-io))

(define-public crate-idl_builder-0.1.0 (c (n "idl_builder") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)))) (h "0lknrd82fznjjika2ccwwgpv73d4s8gf9fz9vgx8cazkrxbwwza0")))

(define-public crate-idl_builder-0.1.1 (c (n "idl_builder") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)))) (h "1afn55sj9cqv5v64hy395kjxhy44y0n57bw495ygvf94gdgbc1lr")))

(define-public crate-idl_builder-0.1.2 (c (n "idl_builder") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)))) (h "1bv5lpsf0c5hv50p775dx6kdwkq5wx4x3pqagb47zxd9xjwl38wr")))

(define-public crate-idl_builder-0.1.3 (c (n "idl_builder") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)))) (h "1isw236za9c47pcmr34jv7c4pbrdd6yhpq2c75ih0krj4znw00mf")))

(define-public crate-idl_builder-0.1.4 (c (n "idl_builder") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)))) (h "0l2sa74cfgb1xc2kl6n5v1fdymdazypkzg5d5c9m18dsscf7vfrw")))

(define-public crate-idl_builder-0.1.5 (c (n "idl_builder") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)))) (h "0xb6mh5bgnbh6izp0syr93k5dgivfnz7cfnc8l1g21sqynji0f8p")))

(define-public crate-idl_builder-0.1.6 (c (n "idl_builder") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)))) (h "0b0gp6hpci7jxazz1a5gjwk228zf04jswa8cd3j1b65y7hv6v7rx")))

