(define-module (crates-io id l_ idl_internal) #:use-module (crates-io))

(define-public crate-idl_internal-0.1.0 (c (n "idl_internal") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wk9c7qpb1j1wv44azn6z7v9qhaprpyv8rmcvkn04bp4h7qk4xrs") (y #t)))

