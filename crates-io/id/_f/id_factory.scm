(define-module (crates-io id _f id_factory) #:use-module (crates-io))

(define-public crate-id_factory-0.1.0 (c (n "id_factory") (v "0.1.0") (h "1dad61wx88bc500b036ac08yjd1f691w2ldq4rpmyk4hdd7jad1g")))

(define-public crate-id_factory-1.0.0 (c (n "id_factory") (v "1.0.0") (h "0jh4kvzavbldmgcznp4nfjwhl20w32ddx6c2iyvvh8gbpmx4k4w1")))

