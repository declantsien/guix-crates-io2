(define-module (crates-io id _l id_loops) #:use-module (crates-io))

(define-public crate-id_loops-0.1.0 (c (n "id_loops") (v "0.1.0") (h "1x53jayrx2hx5d943y6013j55qqpcyqiagkas10m6x8zl2fjvnqm")))

(define-public crate-id_loops-0.1.1 (c (n "id_loops") (v "0.1.1") (h "055fhqcn7mlvjaw2j25hzbcf0v2793hvrcy5szs618igg04rrh8d")))

