(define-module (crates-io id _t id_tree) #:use-module (crates-io))

(define-public crate-id_tree-0.2.3 (c (n "id_tree") (v "0.2.3") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "05rsb0kc52sqbbz8frkj6h23jlscpxfx39g1zqwr4wpmc2fas6hr")))

(define-public crate-id_tree-0.2.4 (c (n "id_tree") (v "0.2.4") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "1j7ah7j0rxrk01b3nyrr3pymkl0krwldis9nzqgp5g3izmj26fkb")))

(define-public crate-id_tree-0.2.5 (c (n "id_tree") (v "0.2.5") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "1jnvhi5q2q9b4mw9gm1h2w8wxjvzn5hyq1jf0pq7laa4v1z286dr")))

(define-public crate-id_tree-0.2.6 (c (n "id_tree") (v "0.2.6") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "0yzab8f9dsvhb74qv7g20gd9pdwjkhmj3206wk0yr8zrg576mpkr")))

(define-public crate-id_tree-0.2.7 (c (n "id_tree") (v "0.2.7") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "1zk2l6v9lsjazi5xf3a4ms9kynka24mvxnnbhr5mc9fmycz1i16y")))

(define-public crate-id_tree-0.2.8 (c (n "id_tree") (v "0.2.8") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "00njmsyqsy0p4acgh3cxgsms206j139rdb9rda8g88ikw7767si6")))

(define-public crate-id_tree-0.2.9 (c (n "id_tree") (v "0.2.9") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "0r9dc5c65cykjg53r3wr89qji5v5qpagfkq11rz4gc4c3k4xmdlr")))

(define-public crate-id_tree-0.2.10 (c (n "id_tree") (v "0.2.10") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "1z0apzbv87fa6n92hflrrikkcayfd4gr95pn552ygn165400vksb")))

(define-public crate-id_tree-0.2.11 (c (n "id_tree") (v "0.2.11") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "0jl4xvxgmhc58l5a8rq92ygw96wy74qmjsll0l6gpdabfhgiysj1")))

(define-public crate-id_tree-0.2.12 (c (n "id_tree") (v "0.2.12") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "1kwwmjy3rdqv7a42w7pqf9kx4w1d5bsi0d8anf8hvaxans9339qa")))

(define-public crate-id_tree-0.2.13 (c (n "id_tree") (v "0.2.13") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "0w1y2iyds4gb0yjrc6fyv5barhd21bfc0kgwcb4r07l1fg133bgm")))

(define-public crate-id_tree-1.0.0 (c (n "id_tree") (v "1.0.0") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "028xg0nvd0b0s6zyxhmp48q528y3hm1s9g2yaxl6ybdf0mnhn6js")))

(define-public crate-id_tree-1.0.1 (c (n "id_tree") (v "1.0.1") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "1pq8nbcahn15qfdiw19gi9bw563nzbxyhys5x8ckcydw2wim0sfi")))

(define-public crate-id_tree-1.0.2 (c (n "id_tree") (v "1.0.2") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "1zbgw9hw87manyj0qrik2jx3kc1hzgxhhk1wmlaipd7pg3hkq8hr")))

(define-public crate-id_tree-1.1.0 (c (n "id_tree") (v "1.1.0") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "0qmgvjqsr17sb7978hpfy3jh49sddsaxkylpg5s3kvqys7d642i5")))

(define-public crate-id_tree-1.1.1 (c (n "id_tree") (v "1.1.1") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "029850isd9lsqffmi6aihy824vh90c0si18sjs724wxmppc2jc3a")))

(define-public crate-id_tree-1.1.2 (c (n "id_tree") (v "1.1.2") (d (list (d (n "snowflake") (r "^1.2.0") (d #t) (k 0)))) (h "0w8z76cdjac5yi3af47lpd4pp3h2p2hzvz5s2s1nkvrxgbabzx2n")))

(define-public crate-id_tree-1.1.3 (c (n "id_tree") (v "1.1.3") (d (list (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "1q81lv1wc8ymn4br7x7fiylmc0ydipglr6wj0n77pgdiljgvdz7c")))

(define-public crate-id_tree-1.2.0 (c (n "id_tree") (v "1.2.0") (d (list (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "0zkkrkziwd4m6wf1lnyvdip40xdc03napy9vmlrzkbkzwf0rnpm3")))

(define-public crate-id_tree-1.3.0 (c (n "id_tree") (v "1.3.0") (d (list (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "0vqldpslzvj295dknbjiy6jqm18556nj7sdm497aqx3i7575l9hc")))

(define-public crate-id_tree-1.4.0 (c (n "id_tree") (v "1.4.0") (d (list (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "1ms9mli1ing2a27cbi3nn40x5vabb75bdwvgj9drrmmgmk0l0a0y")))

(define-public crate-id_tree-1.5.0 (c (n "id_tree") (v "1.5.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "0mxrrarn5m2fws1z7iijkby81rj006h0ksd6ckz9p9bp5inivd8v") (f (quote (("serde_support" "serde" "serde_derive" "snowflake/serde_support"))))))

(define-public crate-id_tree-1.6.0 (c (n "id_tree") (v "1.6.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "1sl7snkx6fjmczyq4a127nbdz17ih8pxn3qmb23nw6v9n3qcvbm8") (f (quote (("serde_support" "serde" "serde_derive" "snowflake/serde_support"))))))

(define-public crate-id_tree-1.7.0 (c (n "id_tree") (v "1.7.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "006x9ll56zg5xbzipx5srz7k0r3qkjq1gjb3mxk4gjzz87wbwcw2") (f (quote (("serde_support" "serde" "serde_derive" "snowflake/serde_support"))))))

(define-public crate-id_tree-1.8.0 (c (n "id_tree") (v "1.8.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "snowflake") (r "^1.3.0") (d #t) (k 0)))) (h "06h7c55k9jnk1yac9fcyk7kqydklzcnlpv944rddx2xysn6xpndw") (f (quote (("serde_support" "serde" "serde_derive" "snowflake/serde_support"))))))

