(define-module (crates-io id co idcontain) #:use-module (crates-io))

(define-public crate-idcontain-0.1.0 (c (n "idcontain") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)))) (h "0dziqlpb44gqc09wp1sbmkkml3vcshb9swd5yqjk72mq7ld78jfg")))

(define-public crate-idcontain-0.2.0 (c (n "idcontain") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)))) (h "0arwfzh7s46vw7786kdr89zq12qc9zx84vkj0li2afjlpk2b8hsf")))

(define-public crate-idcontain-0.3.0 (c (n "idcontain") (v "0.3.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)))) (h "0iysbd0200s3h7pi5wv8jj43s2n70nl0fn2j1qcvw6iykcqi99b2")))

(define-public crate-idcontain-0.4.0 (c (n "idcontain") (v "0.4.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)))) (h "193y2y91afmx6nkw1ng3dh4czm1dpysiskki598vsvmsnzcx2lsb")))

(define-public crate-idcontain-0.6.0 (c (n "idcontain") (v "0.6.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)))) (h "1vjka0gzckrlc5z4225b8j0i7m2rcb3l91878xnsd3f8za5hczkn")))

(define-public crate-idcontain-0.7.1 (c (n "idcontain") (v "0.7.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "1ad0qq28y089k0xjw9mns6inj4bx5x1b5mx5piz4mazn77k1h5dp")))

(define-public crate-idcontain-0.7.2 (c (n "idcontain") (v "0.7.2") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "05apaf5f5c6gvsgvaa74037hrd4qk2jby4083rdwmm8328a67cln")))

(define-public crate-idcontain-0.7.3 (c (n "idcontain") (v "0.7.3") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "10v5wnczcc2kvvbagzp4vqrc62qpdm63kz422ggdqj6ba3s6aadb")))

(define-public crate-idcontain-0.7.4 (c (n "idcontain") (v "0.7.4") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1cd6z78cq06a5znvvgd977i5vdfbsigw2as05fhlkjzg3gyz62dm")))

(define-public crate-idcontain-0.7.5 (c (n "idcontain") (v "0.7.5") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "1m55cblvyd6kvd2bbn82c054q052zr2f0wdm3wsvaxrzf7c0f2ld")))

(define-public crate-idcontain-0.7.6 (c (n "idcontain") (v "0.7.6") (d (list (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "19ymipymmbciw88z1zbsp9sq5dmm68k4v4dxzrcxfpih1pn8b1dc")))

