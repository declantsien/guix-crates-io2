(define-module (crates-io id oc idocker) #:use-module (crates-io))

(define-public crate-idocker-0.1.0 (c (n "idocker") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "shiplift") (r "^0.7.0") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0157xqzfhrr9ci7yigdh3sdddbx9nxi7wbh66ky7wi00fpx8hn7g")))

