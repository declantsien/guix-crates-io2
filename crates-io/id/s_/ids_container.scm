(define-module (crates-io id s_ ids_container) #:use-module (crates-io))

(define-public crate-ids_container-0.1.0 (c (n "ids_container") (v "0.1.0") (h "1fb92ih9sd7sjmwb4hkcjmm39bdixwhn488bgvh54480bd1jj5kv")))

(define-public crate-ids_container-0.1.1 (c (n "ids_container") (v "0.1.1") (h "14w12gln92rxs730cx7lrgkxiycbqyh1my3i71v98ajfcsahimya")))

