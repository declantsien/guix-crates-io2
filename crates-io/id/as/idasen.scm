(define-module (crates-io id as idasen) #:use-module (crates-io))

(define-public crate-idasen-0.1.0 (c (n "idasen") (v "0.1.0") (d (list (d (n "btleplug") (r "^0.5.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)))) (h "1a39bkmgfg5xpams8sb9bldi8nmw442fq91dqjlmwgiy2yqhmxdj")))

(define-public crate-idasen-0.1.1 (c (n "idasen") (v "0.1.1") (d (list (d (n "btleplug") (r "^0.5.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)))) (h "18a53f7zk98mkpl7afxabvw5kcrvkiwph20w6wbrrka4q5sp8jc2")))

(define-public crate-idasen-0.1.2 (c (n "idasen") (v "0.1.2") (d (list (d (n "btleplug") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)))) (h "0hwkz7mmc4ay2sl3p0pa1xlqb4j8l5hqp2chlrmzj056ajxj8l5q")))

(define-public crate-idasen-0.1.3 (c (n "idasen") (v "0.1.3") (d (list (d (n "btleplug") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "winrt") (r "=0.7.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "07pp6icpcnl1fjlggrhkwdkh8cchjxd1l5j681zzwnvxgnfjsb8q")))

(define-public crate-idasen-0.1.4 (c (n "idasen") (v "0.1.4") (d (list (d (n "btleplug") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "winrt") (r "=0.7.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0n1q1glmh0dldsjzinw40hnm4pyxrwdwwp2lspayjvm4gzz7jgn8")))

(define-public crate-idasen-0.2.0 (c (n "idasen") (v "0.2.0") (d (list (d (n "btleplug") (r "^0.9.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "0vmmsfwpl8xgxwydiijjmbih1ph3zky635i61m3dxgdzsna193lr")))

