(define-module (crates-io id as idasen-cli) #:use-module (crates-io))

(define-public crate-idasen-cli-0.1.0 (c (n "idasen-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "idasen") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (d #t) (k 0)))) (h "1rbns0pn6y0706jxik7z45ahv3ici0by0cxg00bic4m95jnn5cml")))

