(define-module (crates-io id id idid) #:use-module (crates-io))

(define-public crate-idid-0.1.0 (c (n "idid") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rev_lines") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "0p59agn8b53jk7qiw1l9n7z6vdfgzkhx72aaapkvfddp8p00jnr1")))

