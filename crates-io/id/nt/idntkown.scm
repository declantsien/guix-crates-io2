(define-module (crates-io id nt idntkown) #:use-module (crates-io))

(define-public crate-idntkown-0.1.0 (c (n "idntkown") (v "0.1.0") (d (list (d (n "alkali") (r "~0.3") (f (quote ("hazmat"))) (d #t) (k 0)) (d (n "capnp") (r "~0.19") (d #t) (k 0)) (d (n "capnpc") (r "~0.19") (d #t) (k 1)) (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "clap") (r "~4.5") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "ntp") (r "~0.5") (d #t) (k 0)) (d (n "pgp") (r "~0.11") (d #t) (k 0)) (d (n "pstream") (r "~0.1") (f (quote ("io-filesystem"))) (d #t) (k 0)) (d (n "rpassword") (r "~7.3") (d #t) (k 0)))) (h "05vzchjdiz0sdvdaakirryfblgbnn07xhsbyga4kq8sl2lz21xs1")))

