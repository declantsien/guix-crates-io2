(define-module (crates-io id l- idl-parser) #:use-module (crates-io))

(define-public crate-idl-parser-0.1.0 (c (n "idl-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "pest") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "07p9dl25z78lpicg1zk3bn1pr5f91wh05c262g17m4yiqlydddlv")))

