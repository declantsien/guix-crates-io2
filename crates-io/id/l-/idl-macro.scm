(define-module (crates-io id l- idl-macro) #:use-module (crates-io))

(define-public crate-idl-macro-0.1.0 (c (n "idl-macro") (v "0.1.0") (d (list (d (n "anchor-syn") (r "^0.24.2") (f (quote ("idl"))) (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "000ikks2crzzvbfckazn7ip2nzwvaianxahh9kvb5mcmz7ayk7p6")))

