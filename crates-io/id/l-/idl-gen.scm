(define-module (crates-io id l- idl-gen) #:use-module (crates-io))

(define-public crate-idl-gen-0.1.0 (c (n "idl-gen") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1k6n31gyhf47gv5y8mm8cdal2qsnfwqm7w14r31v3xbvlnimz7yd")))

