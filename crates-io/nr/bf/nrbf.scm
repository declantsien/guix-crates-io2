(define-module (crates-io nr bf nrbf) #:use-module (crates-io))

(define-public crate-nrbf-0.1.0 (c (n "nrbf") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)) (d (n "const-str") (r "^0.5.7") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34.3") (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17yk2yi8zryp1drcl2r69grkvx79c6i7hv0vj0sjlkpb7rc1l28k") (s 2) (e (quote (("serde" "dep:serde" "rust_decimal/serde"))))))

