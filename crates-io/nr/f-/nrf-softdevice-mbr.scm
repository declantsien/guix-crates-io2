(define-module (crates-io nr f- nrf-softdevice-mbr) #:use-module (crates-io))

(define-public crate-nrf-softdevice-mbr-0.1.1 (c (n "nrf-softdevice-mbr") (v "0.1.1") (h "0988d5fncadx5v52nysbpx9lc72v90i66lpg3f3j35vamxbwmg2y")))

(define-public crate-nrf-softdevice-mbr-0.2.0 (c (n "nrf-softdevice-mbr") (v "0.2.0") (h "05rjp4h67izb1p2gj8m6m51qnr3v5fn1y8bpib252f61n7kk2x7l")))

