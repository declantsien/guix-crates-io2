(define-module (crates-io nr f- nrf-softdevice-s132) #:use-module (crates-io))

(define-public crate-nrf-softdevice-s132-0.1.1 (c (n "nrf-softdevice-s132") (v "0.1.1") (h "1rjr8a8sd0ins4ds8g4mk79y40rw3rnqwrx3xld8vgd6hz5g6jj8")))

(define-public crate-nrf-softdevice-s132-0.1.2 (c (n "nrf-softdevice-s132") (v "0.1.2") (h "12z479v8kbnmiyknvgzzkyfmjami6ax27swgzvydnajw0s1gbi59")))

