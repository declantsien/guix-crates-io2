(define-module (crates-io nr f- nrf-softdevice-s140) #:use-module (crates-io))

(define-public crate-nrf-softdevice-s140-0.1.0 (c (n "nrf-softdevice-s140") (v "0.1.0") (h "04y3xv8krxf8xs1d8jwnq0gh07j8gnlqrxvpfaw20y6x39h7lghy")))

(define-public crate-nrf-softdevice-s140-0.1.1 (c (n "nrf-softdevice-s140") (v "0.1.1") (h "0r2xhazlw6lr0qlv380vx8ps97hzrik344js9pljiz2f4i0qbdm6")))

(define-public crate-nrf-softdevice-s140-0.1.2 (c (n "nrf-softdevice-s140") (v "0.1.2") (h "14sk6lx8dsk548860c3czzl9xn8cpssq2szd2kwzrnpg24g3maay")))

