(define-module (crates-io nr f- nrf-softdevice-macro) #:use-module (crates-io))

(define-public crate-nrf-softdevice-macro-0.1.0 (c (n "nrf-softdevice-macro") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "darling") (r "^0.13.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (d #t) (k 0)))) (h "1sgh9zibzdpynzddjs73lz3j3968vkvd5a7m57x05qhj5wppbw1s")))

