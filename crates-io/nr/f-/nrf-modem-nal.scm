(define-module (crates-io nr f- nrf-modem-nal) #:use-module (crates-io))

(define-public crate-nrf-modem-nal-0.1.0 (c (n "nrf-modem-nal") (v "0.1.0") (d (list (d (n "at-commands") (r "^0.5.1") (d #t) (k 0)) (d (n "embedded-nal") (r "^0.6.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "nrfxlib") (r "^0.6.0") (d #t) (k 0)) (d (n "nrfxlib-sys") (r "^1.5.1") (d #t) (k 0)))) (h "1y68ybfi6x5l9w16dm6yvxwm4kzl55i2b8a4xsqnx455d0kj9fhx")))

(define-public crate-nrf-modem-nal-0.1.1 (c (n "nrf-modem-nal") (v "0.1.1") (d (list (d (n "at-commands") (r "^0.5.1") (d #t) (k 0)) (d (n "embedded-nal") (r "^0.6.0") (d #t) (k 0)) (d (n "ex-log") (r "^0.4") (o #t) (d #t) (k 0) (p "log")) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "nrfxlib") (r "^0.6.0") (d #t) (k 0)) (d (n "nrfxlib-sys") (r "^1.5.1") (d #t) (k 0)))) (h "1z7yrcchs3dbnjh7a4larhb3pcsd3wfxjfvwbmdqsmx7m9p7xsg0") (s 2) (e (quote (("log" "dep:ex-log"))))))

(define-public crate-nrf-modem-nal-0.1.2 (c (n "nrf-modem-nal") (v "0.1.2") (d (list (d (n "at-commands") (r "^0.5.1") (d #t) (k 0)) (d (n "embedded-nal") (r "^0.6.0") (d #t) (k 0)) (d (n "ex-log") (r "^0.4") (o #t) (d #t) (k 0) (p "log")) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "nrfxlib") (r "^0.6.0") (d #t) (k 0)) (d (n "nrfxlib-sys") (r "^1.5.1") (d #t) (k 0)))) (h "13kz59px1a94cglkmmqik8qa2q50nj2c5sa3rhygjdgdxan02qvc") (s 2) (e (quote (("log" "dep:ex-log"))))))

(define-public crate-nrf-modem-nal-0.1.3 (c (n "nrf-modem-nal") (v "0.1.3") (d (list (d (n "at-commands") (r "^0.5.1") (d #t) (k 0)) (d (n "embedded-nal") (r "^0.6.0") (d #t) (k 0)) (d (n "ex-log") (r "^0.4") (o #t) (d #t) (k 0) (p "log")) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "nrfxlib") (r "^0.6.0") (d #t) (k 0)) (d (n "nrfxlib-sys") (r "^1.5.1") (d #t) (k 0)))) (h "09q4094pflzxnd3pf3djplhx0xihfif3lz8d3svq2iadb0i6bd56") (s 2) (e (quote (("log" "dep:ex-log"))))))

(define-public crate-nrf-modem-nal-0.2.0 (c (n "nrf-modem-nal") (v "0.2.0") (d (list (d (n "at-commands") (r "^0.5.1") (d #t) (k 0)) (d (n "embedded-nal") (r "^0.6.0") (d #t) (k 0)) (d (n "ex-log") (r "^0.4") (o #t) (d #t) (k 0) (p "log")) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "nrfxlib") (r "^0.6.0") (d #t) (k 0)) (d (n "nrfxlib-sys") (r "^1.5.1") (d #t) (k 0)))) (h "08y1gf2kagg7b0j3k1qlfxvfm946z0xc1lna2vi79z0ihhirja42") (s 2) (e (quote (("log" "dep:ex-log"))))))

