(define-module (crates-io nr f- nrf-smartled) #:use-module (crates-io))

(define-public crate-nrf-smartled-0.1.0 (c (n "nrf-smartled") (v "0.1.0") (d (list (d (n "nrf52810-hal") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "nrf52832-hal") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)))) (h "0656sw8c3fndwyw187xapif08r0q7212rp6j972pz4gzd8hh68jv") (f (quote (("52840" "nrf52840-hal") ("52832" "nrf52832-hal") ("52810" "nrf52810-hal"))))))

(define-public crate-nrf-smartled-0.2.0 (c (n "nrf-smartled") (v "0.2.0") (d (list (d (n "nrf52810-hal") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "nrf52832-hal") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)))) (h "02rc9l0sip85lbn1cddaw2k9fynppq5dbhl0ln6cawx2nndkrcj4") (f (quote (("52840" "nrf52840-hal") ("52832" "nrf52832-hal") ("52810" "nrf52810-hal"))))))

(define-public crate-nrf-smartled-0.3.0 (c (n "nrf-smartled") (v "0.3.0") (d (list (d (n "nrf52810-hal") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "nrf52832-hal") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)))) (h "0af0a0jfyxq1mjlb79jydmnxpimsldpz68jgbrgkcvdnrayf5374") (f (quote (("52840" "nrf52840-hal") ("52832" "nrf52832-hal") ("52810" "nrf52810-hal"))))))

(define-public crate-nrf-smartled-0.4.0 (c (n "nrf-smartled") (v "0.4.0") (d (list (d (n "nrf52810-hal") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "nrf52832-hal") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)))) (h "18xwfbpd3vs2wm41aqm12py6vl6mlfrk8vxps3a0piig3j4p558v") (f (quote (("52840" "nrf52840-hal") ("52832" "nrf52832-hal") ("52810" "nrf52810-hal"))))))

(define-public crate-nrf-smartled-0.5.0 (c (n "nrf-smartled") (v "0.5.0") (d (list (d (n "nrf52810-hal") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "nrf52832-hal") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)))) (h "1xxqd90jhfsgc6fajby3wjp0ysgcbk2dg62q2bzpsr0959kmdgpq") (f (quote (("52840" "nrf52840-hal") ("52832" "nrf52832-hal") ("52810" "nrf52810-hal"))))))

