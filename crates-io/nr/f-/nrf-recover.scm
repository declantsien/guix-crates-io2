(define-module (crates-io nr f- nrf-recover) #:use-module (crates-io))

(define-public crate-nrf-recover-0.1.0 (c (n "nrf-recover") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "probe-rs") (r "^0.7.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0sxv29rjdxfvzjck6c5pcifffi4gfj4l5jh36hb6792ivjjn2gv5")))

