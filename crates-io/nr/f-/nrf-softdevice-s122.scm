(define-module (crates-io nr f- nrf-softdevice-s122) #:use-module (crates-io))

(define-public crate-nrf-softdevice-s122-0.1.1 (c (n "nrf-softdevice-s122") (v "0.1.1") (h "19fgx5svmvs4qf2y6l7n56q1nfs3dzm8ws3p1mcsjgp5fww135xb")))

(define-public crate-nrf-softdevice-s122-0.1.2 (c (n "nrf-softdevice-s122") (v "0.1.2") (h "0pkbhkw3926k53bbxja21d81ijcxyi023vw9bf0xa7a2i48b5b13")))

