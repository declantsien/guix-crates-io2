(define-module (crates-io nr f- nrf-dfu-target) #:use-module (crates-io))

(define-public crate-nrf-dfu-target-0.1.0 (c (n "nrf-dfu-target") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-storage") (r "^0.3.0") (d #t) (k 0)))) (h "1cw4kdrl3djny3p29kn3fg01hrk2ifkpkxqpc890a4ys4ivb9yn8") (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-nrf-dfu-target-0.1.1 (c (n "nrf-dfu-target") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-storage") (r "^0.3.0") (d #t) (k 0)))) (h "0sjgf1pqpbdsj2d95ji78nvdpzrzk0fs67k5516ad6zf34h95bx2") (s 2) (e (quote (("defmt" "dep:defmt"))))))

