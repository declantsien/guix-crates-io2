(define-module (crates-io nr f- nrf-softdevice-s112) #:use-module (crates-io))

(define-public crate-nrf-softdevice-s112-0.1.1 (c (n "nrf-softdevice-s112") (v "0.1.1") (h "0lcq89vw6vj8ywrlq311r3sz3h05dwn4zqbynr93gmzzffpa6qwh")))

(define-public crate-nrf-softdevice-s112-0.1.2 (c (n "nrf-softdevice-s112") (v "0.1.2") (h "1fafzlahgax2bim60l8rl2zhs9my08a4346z9b9qprmaapcyyp8i")))

