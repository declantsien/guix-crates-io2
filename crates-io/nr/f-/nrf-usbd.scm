(define-module (crates-io nr f- nrf-usbd) #:use-module (crates-io))

(define-public crate-nrf-usbd-0.1.0 (c (n "nrf-usbd") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.8") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1f2i6x30l5lrq18js3lsq16f7jqpiqfdbc5d3na6jpxc660ifnll")))

(define-public crate-nrf-usbd-0.1.1 (c (n "nrf-usbd") (v "0.1.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "critical-section") (r "^0.2.4") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.8") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1icjzl9yx4bqvs5v38d7hgqj0jmjk860sch3gxk3qx08q7a8h3k4")))

(define-public crate-nrf-usbd-0.2.0 (c (n "nrf-usbd") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.8") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1rlmai6yh01zaxp3hgd6smiw8l8x6aica7qwk1jd5i1y1dy91ck6")))

(define-public crate-nrf-usbd-0.3.0 (c (n "nrf-usbd") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (d #t) (k 0)) (d (n "usb-device") (r "^0.3.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "11chzdgyaq51mx6dpj8d5rnkzjfigdvi96dff4i48m0ijhpqdpxf")))

