(define-module (crates-io nr f_ nrf_dfu) #:use-module (crates-io))

(define-public crate-nrf_dfu-2.0.1 (c (n "nrf_dfu") (v "2.0.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 0)))) (h "0wnh5f98ddj3i76svxslac9hk4s3sygj6dc0sxks4csi5ssiv9w9")))

