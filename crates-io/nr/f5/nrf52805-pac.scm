(define-module (crates-io nr f5 nrf52805-pac) #:use-module (crates-io))

(define-public crate-nrf52805-pac-0.1.0 (c (n "nrf52805-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0kwb16fn9f1fxjizsbwi60xrr83j8nvd30igdwl5rx9xqifsdw80") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52805-pac-0.10.0 (c (n "nrf52805-pac") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0393fyzg4mv9ww72n9sdix27nxag8j3rzxbqcfdav7a6yslmf0a8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52805-pac-0.10.1 (c (n "nrf52805-pac") (v "0.10.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "04wpj2ska8yvf2w6q5a67smsjkq1a7xqc7ijgw5vrmjcjxmn7lml") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52805-pac-0.11.0 (c (n "nrf52805-pac") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1nj147phgqr7hhwhd5kic1py1vsphsrv4a8gpw53vi0j4bq6kkgh") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52805-pac-0.12.0 (c (n "nrf52805-pac") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1nh11qnbakrmilvnr5818llz1wpq28wfph5ll7fncajc2kch2kbh") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52805-pac-0.12.1 (c (n "nrf52805-pac") (v "0.12.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1hyfi92vf3yd70k1zxqjfizr5i0zc5c6wg4vrvm018ixyq7k7cl7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52805-pac-0.12.2 (c (n "nrf52805-pac") (v "0.12.2") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0v7s9ffxvqnr35flv6jj68xx19fkina1phvbvvs5k78391v6bnm2") (f (quote (("rt" "cortex-m-rt/device"))))))

