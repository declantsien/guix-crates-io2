(define-module (crates-io nr f5 nrf5340-app-hal) #:use-module (crates-io))

(define-public crate-nrf5340-app-hal-0.14.1 (c (n "nrf5340-app-hal") (v "0.14.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.14.1") (f (quote ("5340-app"))) (k 0)) (d (n "nrf5340-app-pac") (r "^0.10.1") (d #t) (k 0)))) (h "15x3nxllzy6jry1jyjqjs72s6xabnkm5xwy8fm308y430abybni8") (f (quote (("rt" "nrf5340-app-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf5340-app-hal-0.15.0 (c (n "nrf5340-app-hal") (v "0.15.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.15.0") (f (quote ("5340-app"))) (k 0)) (d (n "nrf5340-app-pac") (r "^0.11.0") (d #t) (k 0)))) (h "1qd6hpad0ms8ndk5mmmkha3mxc6r5z7icr7zpmpqxa4ri25s10an") (f (quote (("rt" "nrf5340-app-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf5340-app-hal-0.15.1 (c (n "nrf5340-app-hal") (v "0.15.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.15.1") (f (quote ("5340-app"))) (k 0)) (d (n "nrf5340-app-pac") (r "^0.11.0") (d #t) (k 0)))) (h "06sbmlcilfqjazp9bmlchg31r2v4x0ncjxlyflg548ghhxxl0sy6") (f (quote (("rt" "nrf5340-app-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf5340-app-hal-0.16.0 (c (n "nrf5340-app-hal") (v "0.16.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.16.0") (f (quote ("5340-app"))) (k 0)) (d (n "nrf5340-app-pac") (r "^0.12.2") (d #t) (k 0)))) (h "1wwmhlpkzb9fvn5swpjmilarmfbni99gmakyxxv9xx2zvrlxrsnd") (f (quote (("rt" "nrf5340-app-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf5340-app-hal-0.16.1 (c (n "nrf5340-app-hal") (v "0.16.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.16.1") (f (quote ("5340-app"))) (k 0)) (d (n "nrf5340-app-pac") (r "^0.12.2") (d #t) (k 0)))) (h "084lawxry54zrhihk10ingzp0blmi2nr3zdwzkhg5dsfh24b2z34") (f (quote (("rt" "nrf5340-app-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf5340-app-hal-0.17.0 (c (n "nrf5340-app-hal") (v "0.17.0") (d (list (d (n "nrf-hal-common") (r "=0.17.0") (f (quote ("5340-app"))) (k 0)) (d (n "nrf5340-app-pac") (r "^0.12.2") (d #t) (k 0)))) (h "0ghrdndgy6qs1ghajdjb084k7qb4h0wbgvxj64rf0h073dv39k3w") (f (quote (("rt" "nrf5340-app-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("doc") ("default" "rt" "embedded-hal-02"))))))

(define-public crate-nrf5340-app-hal-0.17.1 (c (n "nrf5340-app-hal") (v "0.17.1") (d (list (d (n "nrf-hal-common") (r "=0.17.1") (f (quote ("5340-app"))) (k 0)) (d (n "nrf5340-app-pac") (r "^0.12.2") (d #t) (k 0)))) (h "03l11kyj1kfvi0a8jqvhjkpf4bmyxwyzkx5anjbvd7bnqizyasff") (f (quote (("rt" "nrf5340-app-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("doc") ("default" "rt" "embedded-hal-02"))))))

(define-public crate-nrf5340-app-hal-0.18.0 (c (n "nrf5340-app-hal") (v "0.18.0") (d (list (d (n "nrf-hal-common") (r "=0.18.0") (f (quote ("5340-app"))) (k 0)) (d (n "nrf5340-app-pac") (r "^0.12.2") (d #t) (k 0)))) (h "170icwfs52y348vfb6n124lc8ky1jgfmcww2iyaq5v6m46qlyygh") (f (quote (("rtic-monotonic" "nrf-hal-common/rtic-monotonic") ("rt" "nrf5340-app-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("doc") ("default" "rt" "embedded-hal-02"))))))

