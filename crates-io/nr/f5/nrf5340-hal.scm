(define-module (crates-io nr f5 nrf5340-hal) #:use-module (crates-io))

(define-public crate-nrf5340-hal-0.0.0 (c (n "nrf5340-hal") (v "0.0.0") (d (list (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "nrf5340-app-pac") (r "^0.1.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "nrf5340-net-pac") (r "^0.1.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.3") (d #t) (k 2)))) (h "101riy2sxn0wg2jbhvkmssp5gnvz63jfxs7zp2n1l3jdma3p0ia0") (f (quote (("net-core" "nrf5340-net-pac") ("app-core" "nrf5340-app-pac"))))))

