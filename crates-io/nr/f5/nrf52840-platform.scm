(define-module (crates-io nr f5 nrf52840-platform) #:use-module (crates-io))

(define-public crate-nrf52840-platform-0.1.1 (c (n "nrf52840-platform") (v "0.1.1") (d (list (d (n "core") (r "^0.1.1") (d #t) (k 2) (p "core-futures-tls")) (d (n "cortex-m-rt") (r "^0.6.11") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-platform") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("async-await"))) (k 2)) (d (n "nrf52840-hal") (r "^0.8.1") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)))) (h "0w4897l4yn74gsvlyil7cs5dzm3ggrlrvxm4y2qd00nrfzxcsbww")))

(define-public crate-nrf52840-platform-0.1.2 (c (n "nrf52840-platform") (v "0.1.2") (d (list (d (n "core") (r "^0.1.1") (d #t) (k 2) (p "core-futures-tls")) (d (n "cortex-m-rt") (r "^0.6.11") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-platform") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("async-await"))) (k 2)) (d (n "nrf52840-hal") (r "^0.8.1") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)))) (h "1wqmllnbrjjlsxn7q4drid9rm94p4gwwmah89nablb25blvcbxnb")))

