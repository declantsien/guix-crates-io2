(define-module (crates-io nr f5 nrf52840) #:use-module (crates-io))

(define-public crate-nrf52840-0.1.0 (c (n "nrf52840") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1c1gna5cgf5f5dxgm0zz3g41lpygw4q1kfdr30h11pk2bavmzam2") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-nrf52840-0.1.1 (c (n "nrf52840") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1kk9w0rds1nlc6a1gwbmmpnxqkz18vch1nsb7l3z7mlrv6wqvjwb") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-nrf52840-0.1.2 (c (n "nrf52840") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "12svjd77ms34is85l3f046rw78rgk5cnw11cqcbg7r084n1jbxlj") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52840-0.1.3 (c (n "nrf52840") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "011nq7f82rv322nw77wzjh7vb3s36m07hn6d05f4kdkld6xaxwq0") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52840-0.2.0 (c (n "nrf52840") (v "0.2.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1h823abxckmzvjvy1qz35mgqzcrffykcsdzq6lvzhc69xgys20ba") (f (quote (("rt" "cortex-m-rt/device"))))))

