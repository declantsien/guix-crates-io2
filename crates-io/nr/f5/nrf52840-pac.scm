(define-module (crates-io nr f5 nrf52840-pac) #:use-module (crates-io))

(define-public crate-nrf52840-pac-0.6.0-beta.1 (c (n "nrf52840-pac") (v "0.6.0-beta.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0zsffvbyi91ddwpyidfc5kp5njp7k6cy1kib1v89a3jk1ghsbff9") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-nrf52840-pac-0.6.0-beta.2 (c (n "nrf52840-pac") (v "0.6.0-beta.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "13mgbknq29vrm3xj1sq0ri5pd6g5i5mjjv6nfgqrd99zp6g20lnx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52840-pac-0.6.0 (c (n "nrf52840-pac") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0nisx6jkix31s109fclk8bv13m6d8r2mcs0i9nzrmsjn1098fx1b") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52840-pac-0.8.0-beta.1 (c (n "nrf52840-pac") (v "0.8.0-beta.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "05809b9fi86w6axx9d9w11333zpvflyh6bbxc5wynygs039dc81v") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52840-pac-0.8.0 (c (n "nrf52840-pac") (v "0.8.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1qnzpx1i5wslx37hyddgl1jrv63gb8yzybzahx9wgrjsxjgb2xg3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52840-pac-0.9.0 (c (n "nrf52840-pac") (v "0.9.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0wrdi8lm7zn19xrirmqarwcnvapn6wlcha1gcms1fqnjmyjq1dz1") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52840-pac-0.10.0 (c (n "nrf52840-pac") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "150jhbavj3lcjgbwdq0prayi30mraqr9nb9byfqn6gh36ah7h9nd") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52840-pac-0.10.1 (c (n "nrf52840-pac") (v "0.10.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1b03674s8zvn2l9lrgvha2v6hp03ylgzzzsrkcjfkh71kk973710") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52840-pac-0.11.0 (c (n "nrf52840-pac") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1kwrw46m23lzbc1j2zmphsrkdq58cf9kd3z2sxgq4fqfj4g9vyly") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52840-pac-0.12.0 (c (n "nrf52840-pac") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "07wbgr4dymg30mbscschyrvjc1ch2crwfbd7sj2apqrddvylph6y") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52840-pac-0.12.1 (c (n "nrf52840-pac") (v "0.12.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1w8prbq3n6ii6bs88sfwrxz0flw316g8bv9hkgf62c970xhpdj91") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52840-pac-0.12.2 (c (n "nrf52840-pac") (v "0.12.2") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0g8vpv8n5l1s7n9d96gi23c47yd1wkm31ymykayfa0myy4v3yw9h") (f (quote (("rt" "cortex-m-rt/device"))))))

