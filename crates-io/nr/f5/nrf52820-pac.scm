(define-module (crates-io nr f5 nrf52820-pac) #:use-module (crates-io))

(define-public crate-nrf52820-pac-0.1.0 (c (n "nrf52820-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1vbfz8i2x1nsrp08algypgj1vn1c1y0s2xig5g5h6hgxpzl93jbn") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52820-pac-0.10.0 (c (n "nrf52820-pac") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1iz0azl9pi7nkz39smqapbvys430kqqp7cjdfc35s2al0l1zf6p6") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52820-pac-0.10.1 (c (n "nrf52820-pac") (v "0.10.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0lbr83c7rly3x6px86zk30h02jkgmrk78jz31h7wj7c14v1413j3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52820-pac-0.11.0 (c (n "nrf52820-pac") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1q192vzzw28n15kxlkmi04jcd4l6fw9d0cmx0s963gy31llhvz6c") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52820-pac-0.12.0 (c (n "nrf52820-pac") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1braxbvwr5w7wlswv4g9szv4l1mh1zwsfmkmlf5gh0zwv6qs9mv2") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52820-pac-0.12.1 (c (n "nrf52820-pac") (v "0.12.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1kv883xjis60ksc4j436cprrhwjvhdh1x109nw6k398jmk45r6xj") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52820-pac-0.12.2 (c (n "nrf52820-pac") (v "0.12.2") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0w8hp1qb9yfgsazvapm8bh97k7akr6rw7sqsmnjijr2yk7ziqyg4") (f (quote (("rt" "cortex-m-rt/device"))))))

