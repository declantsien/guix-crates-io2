(define-module (crates-io nr f5 nrf52833-hal) #:use-module (crates-io))

(define-public crate-nrf52833-hal-0.11.0 (c (n "nrf52833-hal") (v "0.11.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "^0.11.0") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r "^0.9.0") (d #t) (k 0)))) (h "0n8qij10nggzs8jm8vdyly09ig8fvx8y14jy7ii9x7g5hznfadxm") (f (quote (("rt" "nrf52833-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf52833-hal-0.12.0 (c (n "nrf52833-hal") (v "0.12.0") (d (list (d (n "embedded-hal") (r ">=0.2.3, <0.3.0") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.12.0") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r ">=0.9.0, <0.10.0") (d #t) (k 0)))) (h "0cwqnr3k28qa2j5qk7ksrjrkp602i00ldfclpwncrhnc36r6k52j") (f (quote (("rt" "nrf52833-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf52833-hal-0.12.1 (c (n "nrf52833-hal") (v "0.12.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.12.1") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r "^0.9.0") (d #t) (k 0)))) (h "0hraswybnn2q2pvbzw32dr2dji0maydqvqk0bxbhi2a0zl5s4l03") (f (quote (("rt" "nrf52833-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf52833-hal-0.12.2 (c (n "nrf52833-hal") (v "0.12.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.12.2") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r "^0.9.0") (d #t) (k 0)))) (h "1ysf4dnqr6c7z44fbf97jj1ggyskprnpnh7l95npdv1d7yvs6zc1") (f (quote (("rt" "nrf52833-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf52833-hal-0.13.0 (c (n "nrf52833-hal") (v "0.13.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.13.0") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r "^0.9.0") (d #t) (k 0)))) (h "0rbgmfp0kh5vi3cf8svdx4pfzsr6jqji51iml4lr0kq8c06f3maw") (f (quote (("rt" "nrf52833-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf52833-hal-0.14.0 (c (n "nrf52833-hal") (v "0.14.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.14.0") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r "^0.10.1") (d #t) (k 0)))) (h "1l98skgr1g07hasbjpikskpcnr6nkpg4hjr27a202rw5cnsqy3bw") (f (quote (("rt" "nrf52833-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf52833-hal-0.14.1 (c (n "nrf52833-hal") (v "0.14.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.14.1") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r "^0.10.1") (d #t) (k 0)))) (h "0xjzrpmb0am1x7in9d4qx022q41bjzh5xapd4kz9y8wskhk6vxw7") (f (quote (("rt" "nrf52833-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf52833-hal-0.15.0 (c (n "nrf52833-hal") (v "0.15.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.15.0") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r "^0.11.0") (d #t) (k 0)))) (h "104cd6hli55f0m0rfq68m9lx1w19rcvy7qbdagk76v299qck30f3") (f (quote (("rt" "nrf52833-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf52833-hal-0.15.1 (c (n "nrf52833-hal") (v "0.15.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.15.1") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r "^0.11.0") (d #t) (k 0)))) (h "0qvnr20r0c4h0rflf23r5wg5gy77pnm6wdqb7lyw0xfshy8acgvd") (f (quote (("rt" "nrf52833-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf52833-hal-0.16.0 (c (n "nrf52833-hal") (v "0.16.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.16.0") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r "^0.12.2") (d #t) (k 0)))) (h "10b953m1ykxcn0pc5v6d67dzs1sj2yaf0cvrmci16jq11n803m62") (f (quote (("rt" "nrf52833-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf52833-hal-0.16.1 (c (n "nrf52833-hal") (v "0.16.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.16.1") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r "^0.12.2") (d #t) (k 0)))) (h "01mgiladns2305cgzz582ar8x781qpx0k2hjacvxyz7c7f7mm5n1") (f (quote (("rt" "nrf52833-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf52833-hal-0.17.0 (c (n "nrf52833-hal") (v "0.17.0") (d (list (d (n "nrf-hal-common") (r "=0.17.0") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r "^0.12.2") (d #t) (k 0)))) (h "06kngr9sx9dlnbypfx2b7a47ajgxcsxf356vqhsvkg9cb0hqdbgd") (f (quote (("rt" "nrf52833-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("doc") ("default" "rt" "embedded-hal-02"))))))

(define-public crate-nrf52833-hal-0.17.1 (c (n "nrf52833-hal") (v "0.17.1") (d (list (d (n "nrf-hal-common") (r "=0.17.1") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r "^0.12.2") (d #t) (k 0)))) (h "1lxxj3ajpz9qc7ncv14i9k1bx50dlng35iqav9hk3wwsg969wh0h") (f (quote (("rt" "nrf52833-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("doc") ("default" "rt" "embedded-hal-02"))))))

(define-public crate-nrf52833-hal-0.18.0 (c (n "nrf52833-hal") (v "0.18.0") (d (list (d (n "nrf-hal-common") (r "=0.18.0") (f (quote ("52833"))) (k 0)) (d (n "nrf52833-pac") (r "^0.12.2") (d #t) (k 0)))) (h "0v7kj7gh38lfy1v3nb6ip8xr715azqhs08znvbfr4dy9ah7b1yhj") (f (quote (("rtic-monotonic" "nrf-hal-common/rtic-monotonic") ("rt" "nrf52833-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("doc") ("default" "rt" "embedded-hal-02"))))))

