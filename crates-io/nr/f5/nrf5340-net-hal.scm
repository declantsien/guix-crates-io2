(define-module (crates-io nr f5 nrf5340-net-hal) #:use-module (crates-io))

(define-public crate-nrf5340-net-hal-0.15.1 (c (n "nrf5340-net-hal") (v "0.15.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.15.1") (f (quote ("5340-net"))) (k 0)) (d (n "nrf5340-net-pac") (r "^0.11.0") (d #t) (k 0)))) (h "1xik61znyfkn0srjwysw266z4pv9vm25yf3hsvcq8i14sdv3qd9q") (f (quote (("rt" "nrf5340-net-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf5340-net-hal-0.16.0 (c (n "nrf5340-net-hal") (v "0.16.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.16.0") (f (quote ("5340-net"))) (k 0)) (d (n "nrf5340-net-pac") (r "^0.12.2") (d #t) (k 0)))) (h "1c9ycy3sjsh7v6fxslb31fnijpz9rsy4983i9s0dzwsbafsz1afy") (f (quote (("rt" "nrf5340-net-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf5340-net-hal-0.16.1 (c (n "nrf5340-net-hal") (v "0.16.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.16.1") (f (quote ("5340-net"))) (k 0)) (d (n "nrf5340-net-pac") (r "^0.12.2") (d #t) (k 0)))) (h "1am21s80cbxcszwbxnsf05m57zka1h8vs92hd6lg90svzgc2hzav") (f (quote (("rt" "nrf5340-net-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf5340-net-hal-0.17.0 (c (n "nrf5340-net-hal") (v "0.17.0") (d (list (d (n "nrf-hal-common") (r "=0.17.0") (f (quote ("5340-net"))) (k 0)) (d (n "nrf5340-net-pac") (r "^0.12.2") (d #t) (k 0)))) (h "0adpkix9rj1pgn6bc0blr0jccr1sdfrfmj1c2zf20zjc18yd0pzk") (f (quote (("rt" "nrf5340-net-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("doc") ("default" "rt" "embedded-hal-02"))))))

(define-public crate-nrf5340-net-hal-0.17.1 (c (n "nrf5340-net-hal") (v "0.17.1") (d (list (d (n "nrf-hal-common") (r "=0.17.1") (f (quote ("5340-net"))) (k 0)) (d (n "nrf5340-net-pac") (r "^0.12.2") (d #t) (k 0)))) (h "1sjn3jqw0sjv5q12mxr0sc7ncp6dbwjhfs4xkbhxdj9kfgnfjnyf") (f (quote (("rt" "nrf5340-net-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("doc") ("default" "rt" "embedded-hal-02"))))))

(define-public crate-nrf5340-net-hal-0.18.0 (c (n "nrf5340-net-hal") (v "0.18.0") (d (list (d (n "nrf-hal-common") (r "=0.18.0") (f (quote ("5340-net"))) (k 0)) (d (n "nrf5340-net-pac") (r "^0.12.2") (d #t) (k 0)))) (h "19zdr74lfrvwa2lh3f6wgbrw2hx0plk2rgghaq0bzd0550dk8jsa") (f (quote (("rtic-monotonic" "nrf-hal-common/rtic-monotonic") ("rt" "nrf5340-net-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("doc") ("default" "rt" "embedded-hal-02"))))))

