(define-module (crates-io nr f5 nrf52811-hal) #:use-module (crates-io))

(define-public crate-nrf52811-hal-0.12.0 (c (n "nrf52811-hal") (v "0.12.0") (d (list (d (n "embedded-hal") (r ">=0.2.3, <0.3.0") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.12.0") (f (quote ("52811"))) (k 0)) (d (n "nrf52811-pac") (r ">=0.9.1, <0.10.0") (d #t) (k 0)))) (h "0il5cdk7l3rfl5l2diqigj746crhfjdpz7v6hsika26npx3qidhv") (f (quote (("rt" "nrf52811-pac/rt") ("default" "rt"))))))

(define-public crate-nrf52811-hal-0.12.1 (c (n "nrf52811-hal") (v "0.12.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.12.1") (f (quote ("52811"))) (k 0)) (d (n "nrf52811-pac") (r "^0.9.1") (d #t) (k 0)))) (h "1q9c8isgbxdns00nn0867vby7rhn2v04w4bacs3a4nzn55f595n7") (f (quote (("rt" "nrf52811-pac/rt") ("default" "rt"))))))

(define-public crate-nrf52811-hal-0.12.2 (c (n "nrf52811-hal") (v "0.12.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.12.2") (f (quote ("52811"))) (k 0)) (d (n "nrf52811-pac") (r "^0.9.1") (d #t) (k 0)))) (h "0ph6acv2nkxdsrj14ljxhwf25445gymxjzzyzri2hm116xv6z0cs") (f (quote (("rt" "nrf52811-pac/rt") ("default" "rt"))))))

(define-public crate-nrf52811-hal-0.13.0 (c (n "nrf52811-hal") (v "0.13.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.13.0") (f (quote ("52811"))) (k 0)) (d (n "nrf52811-pac") (r "^0.9.1") (d #t) (k 0)))) (h "1b20fzwcvd3nnx8glf84sfkdv4zslmyqxg8lb7vbdkf52qghmsrg") (f (quote (("rt" "nrf52811-pac/rt") ("default" "rt"))))))

(define-public crate-nrf52811-hal-0.14.0 (c (n "nrf52811-hal") (v "0.14.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.14.0") (f (quote ("52811"))) (k 0)) (d (n "nrf52811-pac") (r "^0.10.1") (d #t) (k 0)))) (h "1c3yrbikjgwa7kdbzm5ari4zsi5xc3gfy3ssj7kn2zxw0k5dlnl5") (f (quote (("rt" "nrf52811-pac/rt") ("default" "rt"))))))

(define-public crate-nrf52811-hal-0.14.1 (c (n "nrf52811-hal") (v "0.14.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.14.1") (f (quote ("52811"))) (k 0)) (d (n "nrf52811-pac") (r "^0.10.1") (d #t) (k 0)))) (h "116bzq4z05p7iis4dkkgqxif7wigndsd2bn93186x7x22cbwxlw2") (f (quote (("rt" "nrf52811-pac/rt") ("default" "rt"))))))

(define-public crate-nrf52811-hal-0.15.0 (c (n "nrf52811-hal") (v "0.15.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.15.0") (f (quote ("52811"))) (k 0)) (d (n "nrf52811-pac") (r "^0.11.0") (d #t) (k 0)))) (h "0nwklif1zml4z5fpcx8jwfssi6sbd8l4nfls86whnf9cghs49mrn") (f (quote (("rt" "nrf52811-pac/rt") ("default" "rt"))))))

(define-public crate-nrf52811-hal-0.15.1 (c (n "nrf52811-hal") (v "0.15.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.15.1") (f (quote ("52811"))) (k 0)) (d (n "nrf52811-pac") (r "^0.11.0") (d #t) (k 0)))) (h "06h0553qsj1ns63d2hv7dzlqxfhwkawasz3vhrg0h6ppkvxfgjxw") (f (quote (("rt" "nrf52811-pac/rt") ("default" "rt"))))))

(define-public crate-nrf52811-hal-0.16.0 (c (n "nrf52811-hal") (v "0.16.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.16.0") (f (quote ("52811"))) (k 0)) (d (n "nrf52811-pac") (r "^0.12.2") (d #t) (k 0)))) (h "10fvfig81pdwq75ai99x07ryi804mq8cychqisahwps1c089xjrh") (f (quote (("rt" "nrf52811-pac/rt") ("default" "rt"))))))

(define-public crate-nrf52811-hal-0.16.1 (c (n "nrf52811-hal") (v "0.16.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.16.1") (f (quote ("52811"))) (k 0)) (d (n "nrf52811-pac") (r "^0.12.2") (d #t) (k 0)))) (h "1a0457n77ss6kb54mandfa7a3qnm6wzb4dk8258vcbdvzxva2d0z") (f (quote (("rt" "nrf52811-pac/rt") ("default" "rt"))))))

(define-public crate-nrf52811-hal-0.17.0 (c (n "nrf52811-hal") (v "0.17.0") (d (list (d (n "nrf-hal-common") (r "=0.17.0") (f (quote ("52811"))) (k 0)) (d (n "nrf52811-pac") (r "^0.12.2") (d #t) (k 0)))) (h "16x40xk8p65k1nkfw6l7b6548nwc2qm2j421s0i7a036fyq5rz73") (f (quote (("rt" "nrf52811-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("default" "rt" "embedded-hal-02"))))))

(define-public crate-nrf52811-hal-0.17.1 (c (n "nrf52811-hal") (v "0.17.1") (d (list (d (n "nrf-hal-common") (r "=0.17.1") (f (quote ("52811"))) (k 0)) (d (n "nrf52811-pac") (r "^0.12.2") (d #t) (k 0)))) (h "17y99886yw6z5r0a1aaw9phn1c25nmf2pxdilcwhwdk2x81jj19g") (f (quote (("rt" "nrf52811-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("default" "rt" "embedded-hal-02"))))))

(define-public crate-nrf52811-hal-0.18.0 (c (n "nrf52811-hal") (v "0.18.0") (d (list (d (n "nrf-hal-common") (r "=0.18.0") (f (quote ("52811"))) (k 0)) (d (n "nrf52811-pac") (r "^0.12.2") (d #t) (k 0)))) (h "00gr8xcpz8m0mw640d96gq78ya3lpv4nn830lx3sh1s59wkybckx") (f (quote (("rtic-monotonic" "nrf-hal-common/rtic-monotonic") ("rt" "nrf52811-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("default" "rt" "embedded-hal-02"))))))

