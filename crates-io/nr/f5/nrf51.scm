(define-module (crates-io nr f5 nrf51) #:use-module (crates-io))

(define-public crate-nrf51-0.2.0 (c (n "nrf51") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0zqg5yxf53qmbfqi4fla46d17fzy3xs7d5j3ys2k2nmy4iacb9a7")))

(define-public crate-nrf51-0.2.1 (c (n "nrf51") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "12rm0bj03hsygwzq7ws5bq3zcg0x9vkcn0zj110kyf4cms7jkwl8")))

(define-public crate-nrf51-0.3.0 (c (n "nrf51") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1m7vfi61s5sf559s761yhdvvd30hppd4xhchynfrc556w99chcbr")))

(define-public crate-nrf51-0.4.0 (c (n "nrf51") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1rq98d5z5lx9177ypnpyp9vxp1y4qyyrp7cjbrw5p7k8irays6zq") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-nrf51-0.4.1 (c (n "nrf51") (v "0.4.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.15") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0xl3nsrpyfj83mxkf9rzr9wm0sz5rpya0zyklgid1i2ikal5a8d3") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-nrf51-0.4.2 (c (n "nrf51") (v "0.4.2") (d (list (d (n "bare-metal") (r "^0.1.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0d7d52vwnfhrq97xrmaqv265g3l79ajach28b0v1s817i9m0bq29") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-nrf51-0.5.0 (c (n "nrf51") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0s7yj2xidb5zmm5clcfc05g38gmls3frrs1igy2x6j798gpl1ywq") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt"))))))

(define-public crate-nrf51-0.5.1 (c (n "nrf51") (v "0.5.1") (d (list (d (n "bare-metal") (r "^0.2.3") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1j3ypal391zk60hx0ycx0nkkw9qwxjxxhqh32ikwqlxkcxq72c79") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt"))))))

(define-public crate-nrf51-0.5.2 (c (n "nrf51") (v "0.5.2") (d (list (d (n "bare-metal") (r "^0.2.3") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "11mk45raqv81rnqim215vbf0hlvckpkxgdp444qi1wjsj9na5c42") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt"))))))

(define-public crate-nrf51-0.6.0 (c (n "nrf51") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2.4") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0sgz2q3gx1hcwlx58lampawn3vn6kr41cswr6pywf5fcpjlx8qr0") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt"))))))

(define-public crate-nrf51-0.7.0 (c (n "nrf51") (v "0.7.0") (d (list (d (n "bare-metal") (r "^0.2.5") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0dqklaijprbzwqzkxdlfkyarbdcmx5salx1lam477lnv7hz0dsnn") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt"))))))

(define-public crate-nrf51-0.9.0 (c (n "nrf51") (v "0.9.0") (d (list (d (n "bare-metal") (r "^0.2.5") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "02pq0f54p98cx6q0zxppf5vww9g7zps9g1jmlflz5v9bbjb09y98") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf51-0.10.0 (c (n "nrf51") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2.5") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1zx5072jkq6gpagis657g34lshj123iccq53wagk8jmbga55wz3g") (f (quote (("rt" "cortex-m-rt/device"))))))

