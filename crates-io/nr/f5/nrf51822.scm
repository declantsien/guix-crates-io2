(define-module (crates-io nr f5 nrf51822) #:use-module (crates-io))

(define-public crate-nrf51822-0.1.0 (c (n "nrf51822") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0fdwcd4l0iijx4lqm5kpm9mwjw2giapws9q5rxni9s8nan2an5s1") (y #t)))

(define-public crate-nrf51822-0.1.1 (c (n "nrf51822") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1jk0n7apr1caw2wdwb5fk558jgpywhgfrpyiwpcy2km2dwab7whj") (f (quote (("rt") ("default" "rt"))))))

(define-public crate-nrf51822-0.1.2 (c (n "nrf51822") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.1.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0dfap09fr3cg47c3yp5ikfnxvrlaacmsfnlbqmxal00pfvvkpwvx") (f (quote (("rt") ("default" "rt"))))))

