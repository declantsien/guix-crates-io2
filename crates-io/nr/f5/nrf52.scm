(define-module (crates-io nr f5 nrf52) #:use-module (crates-io))

(define-public crate-nrf52-0.1.0 (c (n "nrf52") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0j1xjascpg8vg3c62p2pnibpqpr3qy4g7ja46vkdjamajlhhzsbh") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-nrf52-0.1.1 (c (n "nrf52") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "01814npwkjgdb0kqfssb0h3br6wagqjs8gl9d447ss6vanqs9fcf") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-nrf52-0.2.0 (c (n "nrf52") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0l2gcpqs0d0z8mj6fxa3byazkhii6mkaypxz8s57rcav05rsxqvn") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-nrf52-0.3.0 (c (n "nrf52") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0mdy3j3hj3wpm94jgqx4whqknafrxm83wdrf3qa91fgzpywhcf7c") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-nrf52-0.4.0 (c (n "nrf52") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1xl6zfvfcsavv45glln3r8bigksrarsc4cbqcc41i3i1jaacsxzc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52-0.4.1 (c (n "nrf52") (v "0.4.1") (d (list (d (n "bare-metal") (r "^0.2.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0ggw7x0hki825hfwr84h9193rvnvrhmy9f3lrwbih0xg722wl3a3") (f (quote (("rt" "cortex-m-rt/device"))))))

