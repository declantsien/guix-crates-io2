(define-module (crates-io nr f5 nrf52805-hal) #:use-module (crates-io))

(define-public crate-nrf52805-hal-0.18.0 (c (n "nrf52805-hal") (v "0.18.0") (d (list (d (n "nrf-hal-common") (r "=0.18.0") (f (quote ("52805"))) (k 0)) (d (n "nrf52805-pac") (r "^0.12.2") (d #t) (k 0)))) (h "07vr4557b7gy4b143ksw5ilv64s2qs4lzkrnbm87khh2xn21fgya") (f (quote (("rtic-monotonic" "nrf-hal-common/rtic-monotonic") ("rt" "nrf52805-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("doc") ("default" "rt" "embedded-hal-02"))))))

