(define-module (crates-io nr f5 nrf52-radio-802154) #:use-module (crates-io))

(define-public crate-nrf52-radio-802154-0.0.1 (c (n "nrf52-radio-802154") (v "0.0.1") (d (list (d (n "cortex-m-semihosting") (r "^0.3") (d #t) (k 0)) (d (n "ieee802154") (r "^0.3") (d #t) (k 0)) (d (n "nrf52840-pac") (r "^0.6") (d #t) (k 0)))) (h "04v746plzxiszwaznic53rkm1lj686w61al436z6zp3l1p2a7y46")))

