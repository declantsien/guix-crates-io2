(define-module (crates-io nr f5 nrf52810-pac) #:use-module (crates-io))

(define-public crate-nrf52810-pac-0.1.0 (c (n "nrf52810-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1pk1mvwgvhcibnf6ny02byramaxhx7576bbp7jysrlfmvfma47nn") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52810-pac-0.1.1 (c (n "nrf52810-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1vhw57kldd9pjxqakr84ffa4qqmagmcnw8kf80ipws87xj3g9fs2") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52810-pac-0.6.0 (c (n "nrf52810-pac") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "07wcpylin7n493avb4favi3f3qfvkjin7wrb2yp82552y1p631zh") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52810-pac-0.8.0 (c (n "nrf52810-pac") (v "0.8.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1cbs9q7xcw08jar8j0xm5xndr175c9aysfzaccx6m75hzvxhn7af") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52810-pac-0.9.0 (c (n "nrf52810-pac") (v "0.9.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1slpcfddxpzvlnlxc0g7p9573048sy37jz53f1iv9hrqlgh10shk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52810-pac-0.10.0 (c (n "nrf52810-pac") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "18ddf48r5fsq5qcgpap67afpr88dz3c3q1k1l55z7h53wpj8nas1") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52810-pac-0.10.1 (c (n "nrf52810-pac") (v "0.10.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "06rd8m657l3yknnxmvp911vlwwgnc6grvplrvrdpl6zvn19xvipl") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52810-pac-0.11.0 (c (n "nrf52810-pac") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1f9kd5b9ahbacv7jh9hsf8ljmxzwrsbjjp6ji6cj8zx9cqbbd0f7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52810-pac-0.12.0 (c (n "nrf52810-pac") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "09pndqa6dabx90af2fchv3c5npc9pwvz63x7mm92hx6vn5sagbqc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52810-pac-0.12.1 (c (n "nrf52810-pac") (v "0.12.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0s7cavw263f588zryq67cgy3qyy0h53l5akhwa472pa8lbcbwws3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52810-pac-0.12.2 (c (n "nrf52810-pac") (v "0.12.2") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0rkn4adb0c62ilzv45c7pikkvjjbh4grmjh6kaxz9a8pmzai4sy2") (f (quote (("rt" "cortex-m-rt/device"))))))

