(define-module (crates-io nr f5 nrf5340-app-pac) #:use-module (crates-io))

(define-public crate-nrf5340-app-pac-0.1.0 (c (n "nrf5340-app-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "07gmznjgywhb4q9irhgd5xhm09d90npcqr7m9y1q8wvzfahncp3l") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf5340-app-pac-0.10.0 (c (n "nrf5340-app-pac") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0r8i0a4fbdmspfbrp886vw9p2672yvcxihkl32j7jivx2l9z22zq") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf5340-app-pac-0.10.1 (c (n "nrf5340-app-pac") (v "0.10.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "133ln2fi9vwbk0ks8vvdjlzc26abzjx5diiwmrrybvbhf2ymlqi8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf5340-app-pac-0.11.0 (c (n "nrf5340-app-pac") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0f903z25ir04qb50f0j9m95mvmy2ss3gdrgzjag7cg74i3r9r7p4") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf5340-app-pac-0.12.0 (c (n "nrf5340-app-pac") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "14gbpzpxl2m07y8fv8knxvpij7jgs3chiysk3py4aggb81nw1qr2") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf5340-app-pac-0.12.1 (c (n "nrf5340-app-pac") (v "0.12.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1b2rb6jlm77mvxz723fg21rnwrqz58hs0zxw9lir2pax6wvakrbn") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf5340-app-pac-0.12.2 (c (n "nrf5340-app-pac") (v "0.12.2") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0yh8iihl6lbpfn44ipr26d6v530slg74h6kwyblhy5fdfd2q523w") (f (quote (("rt" "cortex-m-rt/device"))))))

