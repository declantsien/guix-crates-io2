(define-module (crates-io nr f5 nrf52833-pac) #:use-module (crates-io))

(define-public crate-nrf52833-pac-0.9.0 (c (n "nrf52833-pac") (v "0.9.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "07c0v47pv8bncb5z2isp1p6wz0lzyxlcajmlzbwicdgkr0wfwggd") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52833-pac-0.10.0 (c (n "nrf52833-pac") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0nqdlxyp42vv55f9x52y0wl29g3qz3cnpa9p87z7bc3x5y9k7njp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52833-pac-0.10.1 (c (n "nrf52833-pac") (v "0.10.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0qdldil1z8sssn5yl381mwvl90b3r0fz63h4ih4qxq83b6rbssfd") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52833-pac-0.11.0 (c (n "nrf52833-pac") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "089awvb6r9vnp5gsq3pz7ahqbjdm9hcwa4r1223jiz8ajmnbklkx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52833-pac-0.12.0 (c (n "nrf52833-pac") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "19zckajs5vfcycxrh5j6lfz63v0swm98xl5gyqwnldw9qr80pnxb") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52833-pac-0.12.1 (c (n "nrf52833-pac") (v "0.12.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0pvb7xz0mrf1nwpg4jmb51rsyw0hxfqijvisln875mq6xp16f53a") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52833-pac-0.12.2 (c (n "nrf52833-pac") (v "0.12.2") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0haj1yyj5lmy9597w9wq16f4kj783gw6wyyx2v4csq5kan13bq8h") (f (quote (("rt" "cortex-m-rt/device"))))))

