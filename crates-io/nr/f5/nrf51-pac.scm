(define-module (crates-io nr f5 nrf51-pac) #:use-module (crates-io))

(define-public crate-nrf51-pac-0.10.0 (c (n "nrf51-pac") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "06v3d2b8d0c6lxxlhp80hikabsjjdd919qy8cmrrmwmzkfa9cbg7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf51-pac-0.10.1 (c (n "nrf51-pac") (v "0.10.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0aplcbw1hwd08j232rx3am1hp3qagzpjqca752lz7p2macs2qaws") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf51-pac-0.11.0 (c (n "nrf51-pac") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1vc5jybshsdys0wjgiya6aw75z6j467fqi0zqwnlb65xljhklvab") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf51-pac-0.12.0 (c (n "nrf51-pac") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "17b0fxx0bdhpgzjw25y2jz4p7l18rip2rxkqy70ha0g86wd8px7s") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf51-pac-0.12.1 (c (n "nrf51-pac") (v "0.12.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0nrs7nxj54p37yl4p4q98y0ccqr1scmglap3hs4cjxxdxb19rj1a") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf51-pac-0.12.2 (c (n "nrf51-pac") (v "0.12.2") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "06a2zir7z5b1389wz0i7a5rw5qc378ybv1i064kjwj7fqryihzqk") (f (quote (("rt" "cortex-m-rt/device"))))))

