(define-module (crates-io nr f5 nrf52832-pac) #:use-module (crates-io))

(define-public crate-nrf52832-pac-0.6.0-beta.1 (c (n "nrf52832-pac") (v "0.6.0-beta.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1m6i7w9c3cj91m099bx3wmb93k5xj9hqql52y8f3a2azd6fws0qp") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-nrf52832-pac-0.6.0-beta.2 (c (n "nrf52832-pac") (v "0.6.0-beta.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1np74amz29wc4y3r7y264xydvppi3pfky5d5zdvp45w424vpvay4") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52832-pac-0.6.0 (c (n "nrf52832-pac") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1gwldbaczjrdgxidmiw8llzbpv3bznc50yswqpgxfm4370bj774p") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52832-pac-0.8.0-beta.1 (c (n "nrf52832-pac") (v "0.8.0-beta.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1ir13kwxwyqnv5ax11qpnhbhy5vs1cihkgagpjz7pvkml55xfi3c") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52832-pac-0.8.0 (c (n "nrf52832-pac") (v "0.8.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1yjp14dqrlzyih2cnlks6hs0wf6cir0sbh0k5y3xvg8m8i0lai9w") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52832-pac-0.9.0 (c (n "nrf52832-pac") (v "0.9.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1nsrf893rb8pd8dmc0kjbi314dkjlafh8i83lj90gbjg4y2094kj") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52832-pac-0.10.0 (c (n "nrf52832-pac") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "17aak3s6hn82rshawi338d7gwl2hqr3f9rqdacm4ffqzmrfikhyh") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52832-pac-0.10.1 (c (n "nrf52832-pac") (v "0.10.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1msh4d4azb75rhbwaif0k3lzlpra0gcvcqncac5g957jgk2dxxbn") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52832-pac-0.11.0 (c (n "nrf52832-pac") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0fn3ha9kj4phk2ncvz75gjdimlygaf2mhcpa8dffyjny3i6jhjg6") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52832-pac-0.12.0 (c (n "nrf52832-pac") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "13mgbw6y24fh68d6fawd1z2bhvk6bp10alzdg1v001x3lmj9yb2x") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52832-pac-0.12.1 (c (n "nrf52832-pac") (v "0.12.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0vpj6c6paqxf4p33r72qjwqxxf8g98hi59chc1l9znvmc97dz60l") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52832-pac-0.12.2 (c (n "nrf52832-pac") (v "0.12.2") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1q15p4nsw37ld0497jxjg1s4bkiayhl5c59yh3xlhmn1r62vchh2") (f (quote (("rt" "cortex-m-rt/device"))))))

