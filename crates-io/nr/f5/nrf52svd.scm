(define-module (crates-io nr f5 nrf52svd) #:use-module (crates-io))

(define-public crate-nrf52svd-0.1.0 (c (n "nrf52svd") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "046mklw5gacnvl9v6gs0vkqcgpvzhf0cy83x2c9qwq210bxdkpl2") (f (quote (("rt"))))))

