(define-module (crates-io nr f5 nrf52811-pac) #:use-module (crates-io))

(define-public crate-nrf52811-pac-0.9.0 (c (n "nrf52811-pac") (v "0.9.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "08cvq3dc2f0h0np334vy26a93jasmicx36m2gy1lzq8gkvg2laf1") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52811-pac-0.9.1 (c (n "nrf52811-pac") (v "0.9.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8, <0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0dk0f57c29l074idp3d4x94mfjyn4gphn67p9bjichrg1n9b9xqy") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52811-pac-0.10.0 (c (n "nrf52811-pac") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1jchwgl8i7la9hjxlyk3qn14wq6a4qb7hp9g4k4c45xxzwpyykc9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52811-pac-0.10.1 (c (n "nrf52811-pac") (v "0.10.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1gw5c1rvm70s5ws6ysch18y21nmj8lzmw63wm74dwklj2nd072q8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52811-pac-0.11.0 (c (n "nrf52811-pac") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1khkg4xy3mnvsgkijncdb4bgp5dzvsdcpdidf70iyb555skzk2dy") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52811-pac-0.12.0 (c (n "nrf52811-pac") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "01npxm17bw68nwr6n3kxd6y4h92007251nvygr5l04n18fa3aiv7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52811-pac-0.12.1 (c (n "nrf52811-pac") (v "0.12.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "157rlsd73a1013svvyz3bjw2dbh5b0pg3z7n9mfq3xkjcsp20827") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf52811-pac-0.12.2 (c (n "nrf52811-pac") (v "0.12.2") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "13zxq66vrr92dlqaj5j959mr7csmzjsbkynh17hxabqbxnkv4ya1") (f (quote (("rt" "cortex-m-rt/device"))))))

