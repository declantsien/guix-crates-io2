(define-module (crates-io nr fi nrfind) #:use-module (crates-io))

(define-public crate-nrfind-0.1.0 (c (n "nrfind") (v "0.1.0") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)))) (h "0mbghfz48171ycb09kyqn8b8ky5ds4yld31ddpdq5af3qmgxbnzb")))

(define-public crate-nrfind-0.1.1 (c (n "nrfind") (v "0.1.1") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)))) (h "1ld5mvd4kpd9zrjp53zdj7m32qkcnk712lgxyp94y3wc85wyl8y3")))

(define-public crate-nrfind-0.2.0 (c (n "nrfind") (v "0.2.0") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)))) (h "1c8afjx8yjxafkf09s8pfrrdsvr400q85fc26xrsbzi5g4kg1rkz")))

(define-public crate-nrfind-0.2.1 (c (n "nrfind") (v "0.2.1") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)))) (h "0cg15jfh5jl0r5rqrknmvjv50s5h18nsbaq6vg5g42gh1jzkvl2a")))

(define-public crate-nrfind-1.0.0 (c (n "nrfind") (v "1.0.0") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)))) (h "0vlmx98j9jhb63i8523mnbwri2z1qsrynzbmb7v6b86m9l9v2gb5")))

(define-public crate-nrfind-1.0.1 (c (n "nrfind") (v "1.0.1") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)))) (h "0kp610xzy0kp3nrws3ldvwhj9r6wdlllw7m9g9i4scw9cs70v4k0")))

(define-public crate-nrfind-1.0.2 (c (n "nrfind") (v "1.0.2") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)))) (h "11wazcqz16npqr6070nnr2wpnk4p96pdqw2rjwazvcj12nzqarnd")))

(define-public crate-nrfind-1.0.3 (c (n "nrfind") (v "1.0.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "18kzlfl4gmbyfncsqyl217clbbacjnm8yf8bp9wgh52sq6ddy1bk")))

