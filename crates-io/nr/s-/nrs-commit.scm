(define-module (crates-io nr s- nrs-commit) #:use-module (crates-io))

(define-public crate-nrs-commit-0.1.0 (c (n "nrs-commit") (v "0.1.0") (d (list (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)))) (h "1ys1rplw07gbmiqsjvd3ai9dvj90yhyi6gn07id110h0095vxzrn")))

(define-public crate-nrs-commit-0.2.0 (c (n "nrs-commit") (v "0.2.0") (d (list (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)))) (h "0kc949ayry01x5qpyi95lzpc7ppmahsd7n0s30siqvnmcsrdmz15")))

