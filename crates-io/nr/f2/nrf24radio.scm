(define-module (crates-io nr f2 nrf24radio) #:use-module (crates-io))

(define-public crate-nrf24radio-0.1.0 (c (n "nrf24radio") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "0c489mkbqmwxvjzwg2bqka1903mjgdlfk87729p7fy1pw037bp7j") (f (quote (("log" "defmt") ("default" "log"))))))

