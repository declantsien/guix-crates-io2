(define-module (crates-io nr f2 nrf24l01) #:use-module (crates-io))

(define-public crate-nrf24l01-0.1.0 (c (n "nrf24l01") (v "0.1.0") (d (list (d (n "spidev") (r "^0.3.0") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.1") (d #t) (k 0)))) (h "06a6380mnjfyqvbwxk4mrihv5xgzj8l44rqsinirnchzsp5m75pj")))

(define-public crate-nrf24l01-0.2.0 (c (n "nrf24l01") (v "0.2.0") (d (list (d (n "rppal") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "07zgg2kgnnr3f8ds5nv76gbzdvl718xypfifbwk8phgp631fn9q6") (f (quote (("rpi_accel" "rppal") ("default" "sysfs_gpio"))))))

