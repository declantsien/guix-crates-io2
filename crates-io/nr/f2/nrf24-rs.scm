(define-module (crates-io nr f2 nrf24-rs) #:use-module (crates-io))

(define-public crate-nrf24-rs-0.1.0 (c (n "nrf24-rs") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "08svprlpaz4j86hgn2p9lcixrp8bjfpl1h5n2x66m8jra2jywnzp") (f (quote (("micro-fmt" "ufmt"))))))

(define-public crate-nrf24-rs-0.1.1 (c (n "nrf24-rs") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1hk4ha5rn0wgfz3s1a2v870j7zadl3hy1jzddvlrgbzlkghk3080") (f (quote (("micro-fmt" "ufmt"))))))

