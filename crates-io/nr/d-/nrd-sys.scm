(define-module (crates-io nr d- nrd-sys) #:use-module (crates-io))

(define-public crate-nrd-sys-0.1.0 (c (n "nrd-sys") (v "0.1.0") (d (list (d (n "sysreq") (r "^0") (d #t) (k 1)))) (h "19nxfgfkwj1nirdxkkrfvb9sc2zys981g8az2rn9qfxl30pwn7am")))

(define-public crate-nrd-sys-0.2.0 (c (n "nrd-sys") (v "0.2.0") (d (list (d (n "sysreq") (r "^0") (d #t) (k 1)))) (h "153fb2pd9biqwcrmflcfxkdy83rrhvg4nmlriwf4jdjmzs3bjvn2")))

