(define-module (crates-io nr f9 nrf9160-pac) #:use-module (crates-io))

(define-public crate-nrf9160-pac-0.2.0 (c (n "nrf9160-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1x98cmwzydcal4hskdvz65pr09bg5gmgcxn43w2wmbj3mgy74qbd") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf9160-pac-0.2.1 (c (n "nrf9160-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1ks5gq46l4qxnybip6jhxzknpb02widjjhbw1d0acp276y00l673") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf9160-pac-0.10.0 (c (n "nrf9160-pac") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1qnv269kfql4viakf9nk9dq1kjv0hfyf2ys081yrvbhgvscajz7z") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf9160-pac-0.10.1 (c (n "nrf9160-pac") (v "0.10.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0njkl8bk9v0ljrw2j5ks7y1xhqqgfndgm1hgf00sjljf16740j44") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf9160-pac-0.11.0 (c (n "nrf9160-pac") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "09iryamz8wxarav2r435kindlngw337sr1sslzl2ia5zqmim3ya0") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf9160-pac-0.12.0 (c (n "nrf9160-pac") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1hs1s9fwvyqq9p8c5f9in5fw1rclbpvhgjnbjwwd5ygdmjv8hnwj") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf9160-pac-0.12.1 (c (n "nrf9160-pac") (v "0.12.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "14hvx796qv18yyz06djhb628k66axhfjkvx46pbn8rss25fgpnv4") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf9160-pac-0.12.2 (c (n "nrf9160-pac") (v "0.12.2") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1x48lvxqdfyinhrpss5hrwlxcdlzc7cwlxfi906f112nzd5dfi3k") (f (quote (("rt" "cortex-m-rt/device"))))))

