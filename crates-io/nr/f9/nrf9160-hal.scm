(define-module (crates-io nr f9 nrf9160-hal) #:use-module (crates-io))

(define-public crate-nrf9160-hal-0.10.0 (c (n "nrf9160-hal") (v "0.10.0") (d (list (d (n "cast") (r "^0.2.3") (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "nrf-hal-common") (r "^0.10.0") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.2.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "132hg25wp6y8y4ay0fjilsvkphxmr12jjwgmy1418imahlsbrgly") (f (quote (("rt" "nrf9160-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf9160-hal-0.11.0 (c (n "nrf9160-hal") (v "0.11.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "^0.11.0") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.2.0") (d #t) (k 0)))) (h "1dydp6hycnrnbq3vrkm8k1zv2ghaj3cm3vpvbkz7agmzs10dkpgi") (f (quote (("rt" "nrf9160-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf9160-hal-0.12.0 (c (n "nrf9160-hal") (v "0.12.0") (d (list (d (n "embedded-hal") (r ">=0.2.3, <0.3.0") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.12.0") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "1ph3vslsw9lnvzxwydy45l2njzjyg7b51jw5fxp8y5p0755k8zhr") (f (quote (("rt" "nrf9160-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf9160-hal-0.12.1 (c (n "nrf9160-hal") (v "0.12.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.12.1") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.2.0") (d #t) (k 0)))) (h "058fvycv1iij48ic9k9h825hh60wqwd6qmq19pgzzac976g2cr8y") (f (quote (("rt" "nrf9160-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf9160-hal-0.12.2 (c (n "nrf9160-hal") (v "0.12.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.12.2") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.2.0") (d #t) (k 0)))) (h "1fvhczc7xwl08bmpmz3rin7aqnravdw396lgdcbk5c6xa8x6jf2w") (f (quote (("rt" "nrf9160-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf9160-hal-0.13.0 (c (n "nrf9160-hal") (v "0.13.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.13.0") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.2.0") (d #t) (k 0)))) (h "0zvs3i364s9xyc70b3j676nw492bjlhv5r0ddd9hqygg6x4q8zwf") (f (quote (("rt" "nrf9160-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf9160-hal-0.14.0 (c (n "nrf9160-hal") (v "0.14.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.14.0") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.10.1") (d #t) (k 0)))) (h "1xfw7bjhf4082dmrr8llm77zz5sk7p2iad205rbfbm5f7yyvsg4q") (f (quote (("rt" "nrf9160-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf9160-hal-0.14.1 (c (n "nrf9160-hal") (v "0.14.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.14.1") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.10.1") (d #t) (k 0)))) (h "1g11cvr9m27lmrvjpqh48a92igfjcaj9ni8y0rkw84r5y8yfv2pa") (f (quote (("rt" "nrf9160-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf9160-hal-0.15.0 (c (n "nrf9160-hal") (v "0.15.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.15.0") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.11.0") (d #t) (k 0)))) (h "0i3dpj0px9l8zjc4f6rp1fik2amjawvssinma16j53vh9fhqh76v") (f (quote (("rt" "nrf9160-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf9160-hal-0.15.1 (c (n "nrf9160-hal") (v "0.15.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.15.1") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.11.0") (d #t) (k 0)))) (h "18s8ha0hbswn1z1z9clcb56hcx2c4nkpwhkirdyvk0z71wvcvyb5") (f (quote (("rt" "nrf9160-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf9160-hal-0.16.0 (c (n "nrf9160-hal") (v "0.16.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.16.0") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.12.2") (d #t) (k 0)))) (h "1wqipj4ryyds9jn1r0v7hkiyd4zvi0x4yb4n5h7817m87i45jp4h") (f (quote (("rt" "nrf9160-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf9160-hal-0.16.1 (c (n "nrf9160-hal") (v "0.16.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nrf-hal-common") (r "=0.16.1") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.12.2") (d #t) (k 0)))) (h "0v91bb4r00xad26d33wh078lm8pyfqjs8hvm80izv139kbpf251m") (f (quote (("rt" "nrf9160-pac/rt") ("doc") ("default" "rt"))))))

(define-public crate-nrf9160-hal-0.17.0 (c (n "nrf9160-hal") (v "0.17.0") (d (list (d (n "nrf-hal-common") (r "=0.17.0") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.12.2") (d #t) (k 0)))) (h "1jv013sl8n1sqc28v1ndn46i0v7nyvy40fa8c83s18lw5pbrw15n") (f (quote (("rt" "nrf9160-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("doc") ("default" "rt" "embedded-hal-02"))))))

(define-public crate-nrf9160-hal-0.17.1 (c (n "nrf9160-hal") (v "0.17.1") (d (list (d (n "nrf-hal-common") (r "=0.17.1") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.12.2") (d #t) (k 0)))) (h "1466dlpwcgclaqjrpp43wws54qnk7mja7l42n0z4vh2alz22jb04") (f (quote (("rt" "nrf9160-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("doc") ("default" "rt" "embedded-hal-02"))))))

(define-public crate-nrf9160-hal-0.18.0 (c (n "nrf9160-hal") (v "0.18.0") (d (list (d (n "nrf-hal-common") (r "=0.18.0") (f (quote ("9160"))) (k 0)) (d (n "nrf9160-pac") (r "^0.12.2") (d #t) (k 0)))) (h "03819bmqwdmpd1cp8cfdv4333zixgbbssfhwvv61y6dbh986zfiv") (f (quote (("rtic-monotonic" "nrf-hal-common/rtic-monotonic") ("rt" "nrf9160-pac/rt") ("embedded-hal-02" "nrf-hal-common/embedded-hal-02") ("doc") ("default" "rt" "embedded-hal-02"))))))

