(define-module (crates-io nr f9 nrf91) #:use-module (crates-io))

(define-public crate-nrf91-0.1.0 (c (n "nrf91") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0da88659kj7z84c1wz0725wv899q82w8wigz1m28sjwl5i8c74hb") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-nrf91-0.1.1 (c (n "nrf91") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0ip39zp85fnxhw3nl8iflhlinv7c0pn9ja5k3avakv3dx1x600bj") (f (quote (("rt" "cortex-m-rt/device"))))))

