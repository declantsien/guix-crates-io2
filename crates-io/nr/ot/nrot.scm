(define-module (crates-io nr ot nrot) #:use-module (crates-io))

(define-public crate-nrot-1.0.0 (c (n "nrot") (v "1.0.0") (h "1kx83w00wz9f771zixcnbg88z0xad74rilykbzrvlsnd95ayn4df")))

(define-public crate-nrot-2.0.0 (c (n "nrot") (v "2.0.0") (h "03q9j2z8qbbdmrq58244xy5lbqw32yami1ri8jrig61l0zs5mdz1")))

