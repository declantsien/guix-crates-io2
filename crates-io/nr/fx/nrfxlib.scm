(define-module (crates-io nr fx nrfxlib) #:use-module (crates-io))

(define-public crate-nrfxlib-0.1.0 (c (n "nrfxlib") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "nrf91") (r "^0.1.1") (d #t) (k 0)) (d (n "nrfxlib-sys") (r "^0.1.5") (d #t) (k 0)))) (h "1g707lr47pa51sjz49840dbgn03wcwpl0a3jlig21n4ksd4gixz8")))

(define-public crate-nrfxlib-0.2.1 (c (n "nrfxlib") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "nrf91") (r "^0.1.1") (d #t) (k 0)) (d (n "nrfxlib-sys") (r "^0.2") (d #t) (k 0)))) (h "1bch9gv1fyap9wkgkxn5bi8ygyv0lqa76y5mgdknz2vc5r80d0rs")))

(define-public crate-nrfxlib-0.2.2 (c (n "nrfxlib") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "nrf91") (r "^0.1.1") (d #t) (k 0)) (d (n "nrfxlib-sys") (r "^0.2") (d #t) (k 0)))) (h "1ibgkfbll447a3bfcv837d8v0xkq5wn0lkphwri3p1z6f7d17s6b")))

(define-public crate-nrfxlib-0.3.0 (c (n "nrfxlib") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "nrf91") (r "^0.1.1") (d #t) (k 0)) (d (n "nrfxlib-sys") (r "^1.1.0-rc2") (d #t) (k 0)))) (h "1y9r6f2lfapahk4xxfqqf5pchmx161x9xcbk3awmlyw3kh5hx1cb")))

(define-public crate-nrfxlib-0.4.0 (c (n "nrfxlib") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nrf91") (r "^0.1.1") (d #t) (k 0)) (d (n "nrfxlib-sys") (r "^1.1.0-rc2") (d #t) (k 0)))) (h "1dxs1i3n1c2vm2s5bqb2y9ahycs0q86fibq32hn2bw68x7gnjk9y")))

(define-public crate-nrfxlib-0.5.0 (c (n "nrfxlib") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nrf9160-pac") (r "^0.2.1") (d #t) (k 0)) (d (n "nrfxlib-sys") (r "^1.2.0") (d #t) (k 0)))) (h "0d4sg89mnavpz7pj5dld8sbaw7k9n7mf16kmvpfrs17qq3nzylc7")))

(define-public crate-nrfxlib-0.6.0 (c (n "nrfxlib") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.9.0") (f (quote ("use_spin"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nrf9160-pac") (r "^0.2.1") (d #t) (k 0)) (d (n "nrfxlib-sys") (r "=1.5.1") (d #t) (k 0)))) (h "1lvmp8hprbaixsn1aa5aypdbj330abhcb6q778q8z4pan1wx8k4i")))

