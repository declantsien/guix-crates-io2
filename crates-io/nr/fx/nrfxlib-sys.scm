(define-module (crates-io nr fx nrfxlib-sys) #:use-module (crates-io))

(define-public crate-nrfxlib-sys-0.1.0 (c (n "nrfxlib-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "1gcvj94z7ckqw1s21fx0j80bh93vngyzvlcg6daqvqa4c7g26vjs")))

(define-public crate-nrfxlib-sys-0.1.1 (c (n "nrfxlib-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "0ck5a3cwaqws5lnwyxddv6lf2kdyyhl6809kh4z02nv805hiax41")))

(define-public crate-nrfxlib-sys-0.1.2 (c (n "nrfxlib-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "0s5v2lljfdmdd5cxv9136zmfj2h6gkkm3rc498p2l0skv6fgfjgm")))

(define-public crate-nrfxlib-sys-0.1.3 (c (n "nrfxlib-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "15y955j9f0fkca00rzf1nhnn6slm78h516bkd7mprz429pcwvajk")))

(define-public crate-nrfxlib-sys-0.1.4 (c (n "nrfxlib-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "0ihp3hj2fm6j9zhk9zhywc9wm5flx8kldbwhic0a46s0w988y8f1")))

(define-public crate-nrfxlib-sys-0.1.5 (c (n "nrfxlib-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "13p9s3vr6rx2rnb9xkc1lqlhc9n1dmzk85fy052g8ilazbc6cxbd")))

(define-public crate-nrfxlib-sys-0.1.6 (c (n "nrfxlib-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "110dnz9p2wx0zn8fmp66fwx83qrcnfy5plbpk9vrvhzrfplkq9ly")))

(define-public crate-nrfxlib-sys-0.2.0 (c (n "nrfxlib-sys") (v "0.2.0") (h "029n2n51znqfynzryzhkm33a9xrvsahk6iwa93blvswaziwcfpcl")))

(define-public crate-nrfxlib-sys-1.1.0-rc2+rel1 (c (n "nrfxlib-sys") (v "1.1.0-rc2+rel1") (h "0x0wg665zwswnhdd3m7az7mw14qyiisflvbvrcgp0dhhicbj4b5k")))

(define-public crate-nrfxlib-sys-1.1.0-rc3+rel1 (c (n "nrfxlib-sys") (v "1.1.0-rc3+rel1") (h "0vmkdgrl7a71nx0zrzls76niqkxa7mbzn4wm51qy5rqnkf4xap69")))

(define-public crate-nrfxlib-sys-1.2.0 (c (n "nrfxlib-sys") (v "1.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1v32xdfyp1d9gh7an4sy11qbgigcj8cw68w4q5816zvq9ji3d097")))

(define-public crate-nrfxlib-sys-1.4.2 (c (n "nrfxlib-sys") (v "1.4.2") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0ywvpsdc8x75xmy72if7b2qblr8pnj5gbrfcxvv5khrq38irzp93")))

(define-public crate-nrfxlib-sys-1.5.1-rc1 (c (n "nrfxlib-sys") (v "1.5.1-rc1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0r5j86hq2i513171wzdrl1x69dihys2bvcckawyz47zkfk1x9wbj")))

(define-public crate-nrfxlib-sys-1.5.1 (c (n "nrfxlib-sys") (v "1.5.1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1jcby1ay512wwiy257745pyq1ncdd8qrf698cjhccqabqrd45s7h")))

(define-public crate-nrfxlib-sys-2.1.0 (c (n "nrfxlib-sys") (v "2.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0gqfl77s9lmp0kk5whc2mbc4hssz0r1p8sdpazddzxy53mki3c0f") (f (quote (("llvm-objcopy") ("default" "llvm-objcopy") ("arm-none-eabi-objcopy"))))))

(define-public crate-nrfxlib-sys-2.4.2 (c (n "nrfxlib-sys") (v "2.4.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "llvm-tools") (r "^0.1.1") (o #t) (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1mzdk4hd74gd308zxrnjcy2ynsg5cbv6j0q2v8s5hzdab2g2492m") (f (quote (("default" "llvm-objcopy") ("arm-none-eabi-objcopy")))) (s 2) (e (quote (("llvm-objcopy" "dep:llvm-tools"))))))

