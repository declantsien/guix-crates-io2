(define-module (crates-io nr pc nrpc) #:use-module (crates-io))

(define-public crate-nrpc-0.1.0 (c (n "nrpc") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)))) (h "15hhq9pn4z2rjnax1bq59qvy2y5nyxdlgvzp8w70z42lk8aq2pm9")))

(define-public crate-nrpc-0.2.0 (c (n "nrpc") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)))) (h "1ijs4nxxci5y1n7jqh0pihmknhmwfafqkds1dr9ml0sk5shsjw47")))

(define-public crate-nrpc-0.6.0 (c (n "nrpc") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)))) (h "1kb1zarq851g0r5zqpvx1ndjwvrxhdx55clayv39151rcpmwlhbg")))

(define-public crate-nrpc-0.7.0 (c (n "nrpc") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)))) (h "08z8gbadilwrh55w6s4gz1db4appilwl74sqa2hi2i1p18rb4gym")))

(define-public crate-nrpc-0.8.0 (c (n "nrpc") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)))) (h "0jds82v5bqid8x16nlfdcjwmw6ac3b2yyk8rrb1a3isfqlkk8iqc")))

(define-public crate-nrpc-0.10.0 (c (n "nrpc") (v "0.10.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)))) (h "09jxdz3x0j4dvhqan2ji5xf6mgvvb4iyfc947ikzbiqm2hwhg27b") (f (quote (("server-send") ("default" "client-send" "server-send") ("client-send"))))))

