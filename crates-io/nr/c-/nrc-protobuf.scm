(define-module (crates-io nr c- nrc-protobuf) #:use-module (crates-io))

(define-public crate-nrc-protobuf-2.8.0 (c (n "nrc-protobuf") (v "2.8.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1xqrlgfdyknrq5amj44ahis12z1wa2szc5j16r5lwcgzw7d0zh89") (f (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

