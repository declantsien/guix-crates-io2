(define-module (crates-io nr c- nrc-protobuf-codegen) #:use-module (crates-io))

(define-public crate-nrc-protobuf-codegen-2.8.0 (c (n "nrc-protobuf-codegen") (v "2.8.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "protobuf") (r "= 2.8.0") (d #t) (k 0) (p "nrc-protobuf")))) (h "110kjn3y7hdh7m0x1qhz5li5fnqcr5xwy2an0npafsdn1w7mvns5")))

