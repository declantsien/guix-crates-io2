(define-module (crates-io w- wi w-wiki) #:use-module (crates-io))

(define-public crate-w-wiki-0.1.0 (c (n "w-wiki") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 2)))) (h "03fkddmlsgnkqrac8nhinhnnd1ap3bvk3ylqhh9apajwd4izz0nj")))

(define-public crate-w-wiki-0.1.1 (c (n "w-wiki") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10.6") (f (quote ("json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 2)))) (h "123r4d8fnz966sjjg7fd9zn8mwif5hqrzjiccr1382m9df65332l")))

(define-public crate-w-wiki-0.2.0 (c (n "w-wiki") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "gzip" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1whwc1mc8wgxn4dbpzn10r2vilg37pqdvbwjfmqxbfas7zd6bihi")))

