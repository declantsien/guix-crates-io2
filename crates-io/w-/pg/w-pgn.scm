(define-module (crates-io w- pg w-pgn) #:use-module (crates-io))

(define-public crate-w-pgn-0.1.0 (c (n "w-pgn") (v "0.1.0") (h "1s9wb24bg9rfiywlqksc5wkjnv6b9m73q1scz8dmh52j861dhiv6")))

(define-public crate-w-pgn-0.1.1 (c (n "w-pgn") (v "0.1.1") (h "1v9pjway280w1y129ikwd0mj0bm1080l2w0apkfi2mz2rrficcfi")))

(define-public crate-w-pgn-0.1.2 (c (n "w-pgn") (v "0.1.2") (h "1xba7swixd4d740lfiaaicm3xagidq98wrchjyy6z1vgfiv9zaix")))

