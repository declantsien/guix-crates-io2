(define-module (crates-io kp gr kpgres) #:use-module (crates-io))

(define-public crate-kpgres-0.5.0 (c (n "kpgres") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "kayrx") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "postgres-protocol") (r "^0.5.0") (d #t) (k 0)) (d (n "postgres-types") (r "^0.1.0") (d #t) (k 0)))) (h "14bgsmmhy2fcyfx9avg94fh7m2ar7mrc7gr9r604793jmi65vrdy")))

