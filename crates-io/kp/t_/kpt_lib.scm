(define-module (crates-io kp t_ kpt_lib) #:use-module (crates-io))

(define-public crate-kpt_lib-0.1.0 (c (n "kpt_lib") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "dxf") (r "^0.4.0") (d #t) (k 0)) (d (n "orchestrator") (r "^0.2.0") (d #t) (k 0)) (d (n "ply-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "spade") (r "^1.8.2") (d #t) (k 0)))) (h "077naifgwzsyvqan3gr20i41kmmq3k1m5d9rxv9ywiwqb3d1y088")))

(define-public crate-kpt_lib-0.1.1 (c (n "kpt_lib") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "dxf") (r "^0.4.0") (d #t) (k 0)) (d (n "orchestrator") (r "^0.2.0") (d #t) (k 0)) (d (n "ply-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "spade") (r "^1.8.2") (d #t) (k 0)))) (h "1qb074bpkchk3xii08mp4q1maf4nxd3kgwymjhsbc86m0kfajl6y")))

