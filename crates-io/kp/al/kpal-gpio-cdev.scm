(define-module (crates-io kp al kpal-gpio-cdev) #:use-module (crates-io))

(define-public crate-kpal-gpio-cdev-0.1.0 (c (n "kpal-gpio-cdev") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.2") (d #t) (k 0)) (d (n "kpal-plugin") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14nyys53rawj92qj055wv9ln2rrmpv9hddsryc393yk1wqygdphm")))

(define-public crate-kpal-gpio-cdev-0.2.0 (c (n "kpal-gpio-cdev") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.2") (d #t) (k 0)) (d (n "kpal-plugin") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0cnhprfib5hw3j878diqi1cm83kr6mivamjv7l1x3p9lc3iqalsp")))

(define-public crate-kpal-gpio-cdev-0.2.1 (c (n "kpal-gpio-cdev") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.2") (d #t) (k 0)) (d (n "kpal-plugin") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "089argvijg5z61i3ym6qwfkgk29k85ryw4pgxpsxgv82wd02l6hn")))

(define-public crate-kpal-gpio-cdev-0.2.2 (c (n "kpal-gpio-cdev") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.2") (d #t) (k 0)) (d (n "kpal-plugin") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0a3klv20g7c2985b5pbxnjpvcixyrmqgsls0rwxnklq1g9wlj7m9")))

