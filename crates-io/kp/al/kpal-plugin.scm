(define-module (crates-io kp al kpal-plugin) #:use-module (crates-io))

(define-public crate-kpal-plugin-0.1.0 (c (n "kpal-plugin") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1nd4x3cpxlv4cpfglv7pmqp0bg46ypvcdgxah4vd47nr78gxz9y6")))

(define-public crate-kpal-plugin-0.2.0 (c (n "kpal-plugin") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "multi-map") (r "^1.2") (d #t) (k 0)))) (h "0mh3c11fm7bq19l2dgnknbxm9500mddk0mfb4gf97lh09x3b452v")))

(define-public crate-kpal-plugin-0.2.1 (c (n "kpal-plugin") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "multi-map") (r "^1.2") (d #t) (k 0)))) (h "08iz3sz57i7gqm950h25nvq9681d6lj3hf2ailyhw0kjscxv2das")))

(define-public crate-kpal-plugin-0.2.2 (c (n "kpal-plugin") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "multi-map") (r "^1.2") (d #t) (k 0)))) (h "0srqmdl8cz8845326pfw0a62ynxh1xivjr13r0xlqn5vpbv3f244")))

