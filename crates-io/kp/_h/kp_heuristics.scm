(define-module (crates-io kp _h kp_heuristics) #:use-module (crates-io))

(define-public crate-kp_heuristics-0.1.0 (c (n "kp_heuristics") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1x87923c94pqpc8w1xwzhkydcqgbbayhmk07kpk56x2nr8cb58ad")))

(define-public crate-kp_heuristics-0.1.1 (c (n "kp_heuristics") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09zn123pp3797kihgnwi3bpxb7s9ysmf9jz35fi53a2nyk4wr1aw")))

(define-public crate-kp_heuristics-0.1.2 (c (n "kp_heuristics") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qrlnzv73pn5gzy2x6mj6zm3182d76jx1h1ydh2ba1rpkzxixijs")))

