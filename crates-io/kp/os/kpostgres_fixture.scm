(define-module (crates-io kp os kpostgres_fixture) #:use-module (crates-io))

(define-public crate-kpostgres_fixture-0.1.0 (c (n "kpostgres_fixture") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "dockworker") (r "^0.0.13") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1j245075ynhrrizrn6lkxyiiy5lgn9ywn44phv37zvpkmi9fn3x7") (f (quote (("docker" "dockworker") ("default" "docker"))))))

(define-public crate-kpostgres_fixture-0.2.0 (c (n "kpostgres_fixture") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "dockworker") (r "^0.0.13") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1g22m06j08adp9hmq9slzidfj3fnywjbmfb86ssbyzybppzwhrvl") (f (quote (("docker" "dockworker") ("default" "docker"))))))

