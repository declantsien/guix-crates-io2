(define-module (crates-io kp at kpathsea_sys) #:use-module (crates-io))

(define-public crate-kpathsea_sys-0.1.0 (c (n "kpathsea_sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1npcrdrarxais58z23xfvpr8camsw841ar79w25955vghz63nkzm") (l "kpathsea")))

(define-public crate-kpathsea_sys-0.1.1 (c (n "kpathsea_sys") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1b7sqjfgkw5vrszcgf7l2hdvlbi1n0w212r9f0wb8p1abl1jm52a") (l "kpathsea")))

(define-public crate-kpathsea_sys-0.1.2 (c (n "kpathsea_sys") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0a7zn9l3cqy1dp91kj8f0waniwl2wl38666hcbvss8yy8z09hd13") (l "kpathsea")))

