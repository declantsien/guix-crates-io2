(define-module (crates-io kp at kpathsea) #:use-module (crates-io))

(define-public crate-kpathsea-0.1.0 (c (n "kpathsea") (v "0.1.0") (d (list (d (n "kpathsea_sys") (r "^0.1.0") (d #t) (k 0)))) (h "1bjnm2kvnb9dk100iwvbs7kkhh5s38h9w3k8gzpnv176bpbja324")))

(define-public crate-kpathsea-0.1.1 (c (n "kpathsea") (v "0.1.1") (d (list (d (n "kpathsea_sys") (r "^0.1.0") (d #t) (k 0)))) (h "163m31m7jmk3gbzg95jzzzkbs7xmf6hybfh5cgy0m5h5h48asl8k")))

(define-public crate-kpathsea-0.1.2 (c (n "kpathsea") (v "0.1.2") (d (list (d (n "kpathsea_sys") (r "^0.1.0") (d #t) (k 0)))) (h "1aaafm8bh5s8n188cxdq4bv90kdm6nccspdgw4i4fjpz9m1v8zji")))

(define-public crate-kpathsea-0.1.3 (c (n "kpathsea") (v "0.1.3") (d (list (d (n "kpathsea_sys") (r "^0.1.0") (d #t) (k 0)))) (h "01kbqvj2j0rgqibnq9vm43hciz0i39jn3jcg43i8al1zqvwxcqpb")))

(define-public crate-kpathsea-0.2.0 (c (n "kpathsea") (v "0.2.0") (d (list (d (n "kpathsea_sys") (r "^0.1.0") (d #t) (k 0)) (d (n "which") (r "^2.0.1") (d #t) (k 0)))) (h "1l13ppks1603r0x8pdn53ablrqa6f8znjsra6a2y0i9dpkc1aq80")))

(define-public crate-kpathsea-0.2.1 (c (n "kpathsea") (v "0.2.1") (d (list (d (n "kpathsea_sys") (r "^0.1.0") (d #t) (k 0)) (d (n "which") (r "^2.0.1") (d #t) (k 0)))) (h "07kfh8mvblzw7cbcxk23i07kw4vji599rcwvxqv02jvyi09slxyn")))

(define-public crate-kpathsea-0.2.2 (c (n "kpathsea") (v "0.2.2") (d (list (d (n "kpathsea_sys") (r "^0.1.0") (d #t) (k 0)) (d (n "which") (r "^2.0.1") (d #t) (k 0)))) (h "083yp6mnin9hr6hkypafmi9z6mv09dcvifiv620sy28gfinwxg44")))

(define-public crate-kpathsea-0.2.3 (c (n "kpathsea") (v "0.2.3") (d (list (d (n "kpathsea_sys") (r "^0.1.0") (d #t) (k 0)) (d (n "which") (r "^4.0.1") (d #t) (k 0)))) (h "1f5s15siq4cqwbc69fvfvradvbgcwjgnyl3hwyziqq7n28n2vdwy")))

