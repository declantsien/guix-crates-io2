(define-module (crates-io kp la kplayer-rust-wrap) #:use-module (crates-io))

(define-public crate-kplayer-rust-wrap-1.0.0 (c (n "kplayer-rust-wrap") (v "1.0.0") (h "0qyqivyja332ynv4sag9l98pwdkfhnkg1psyd55cfpxvpxz6qhkn")))

(define-public crate-kplayer-rust-wrap-1.1.1 (c (n "kplayer-rust-wrap") (v "1.1.1") (h "1912mbsp10662cillhzadyykrl5w5051108lb0niwp22g6wzjaz8")))

(define-public crate-kplayer-rust-wrap-1.2.1 (c (n "kplayer-rust-wrap") (v "1.2.1") (h "1i8zdjwxilxd3msa9ixah6l4iv9gmlai5z130prls4qw3jhcsv31")))

(define-public crate-kplayer-rust-wrap-1.2.2 (c (n "kplayer-rust-wrap") (v "1.2.2") (d (list (d (n "protobuf") (r "^2.25.2") (d #t) (k 0)))) (h "1571c6s53y26vxzpl6i1mga4i4b468p570xc7m5lsv5a4fv80kyj")))

(define-public crate-kplayer-rust-wrap-1.4.0 (c (n "kplayer-rust-wrap") (v "1.4.0") (d (list (d (n "protobuf") (r "^2.25.2") (d #t) (k 0)))) (h "0zanl59kzh3dxls8gdg1b081841lap7imnyvsxvdzrxpj7dsh1yp")))

(define-public crate-kplayer-rust-wrap-1.4.1 (c (n "kplayer-rust-wrap") (v "1.4.1") (d (list (d (n "protobuf") (r "^2.25.2") (d #t) (k 0)))) (h "124q401213rgz3habqfdpihgc29y0050hcl4b4mhq5kl9s6rjm8f")))

(define-public crate-kplayer-rust-wrap-1.5.1 (c (n "kplayer-rust-wrap") (v "1.5.1") (d (list (d (n "protobuf") (r "^2.25.2") (d #t) (k 0)))) (h "0bmw85kfkr29b91q7x7si2fk0dk5sqix5hbamw21la1hh6rdwl6c") (y #t)))

(define-public crate-kplayer-rust-wrap-1.5.0 (c (n "kplayer-rust-wrap") (v "1.5.0") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)))) (h "0da1mqcb2pqn917hl1l72faqza3665yvzvydy0g2cvdhlljdrxdh") (y #t)))

(define-public crate-kplayer-rust-wrap-1.5.0-rc1 (c (n "kplayer-rust-wrap") (v "1.5.0-rc1") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)))) (h "0868yxsp1qiy913x55mvf1pnc82cvdw5zbr2pmgk3ng0bi1hp6r0")))

(define-public crate-kplayer-rust-wrap-1.5.6 (c (n "kplayer-rust-wrap") (v "1.5.6") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)))) (h "1lc5abqmcgzjqrfr0kkxsx0k4s4zlln0ggxhpx95vfrpgrircxrd")))

(define-public crate-kplayer-rust-wrap-1.5.6-rc1 (c (n "kplayer-rust-wrap") (v "1.5.6-rc1") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)))) (h "0zr5qnwk96zhpp5sxsv40aifwdawimq4lay6139aqi6cx7v52x43")))

(define-public crate-kplayer-rust-wrap-1.5.6-rc2-10501 (c (n "kplayer-rust-wrap") (v "1.5.6-rc2-10501") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)))) (h "1pb0plq206b25lqf057xksby01jsiazpij4mmn4axrgdc2sb5krr")))

(define-public crate-kplayer-rust-wrap-1.5.7 (c (n "kplayer-rust-wrap") (v "1.5.7") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)))) (h "05vd0ngv067hrf5mwajyn970nm59y12gm7al5zpy4dk967j5wz3z")))

(define-public crate-kplayer-rust-wrap-1.5.8 (c (n "kplayer-rust-wrap") (v "1.5.8") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)))) (h "001jmxrahv6afd7krb68xhb0zhx9dvhp9xrkzkhik4h12nzmpvzb")))

