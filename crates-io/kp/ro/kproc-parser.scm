(define-module (crates-io kp ro kproc-parser) #:use-module (crates-io))

(define-public crate-kproc-parser-0.0.1-alpla.1 (c (n "kproc-parser") (v "0.0.1-alpla.1") (h "194gyd02nm7c52yj5b9w949g16hkz2mq0z24ik32prv93043lh9y")))

(define-public crate-kproc-parser-0.0.1-alpla.2 (c (n "kproc-parser") (v "0.0.1-alpla.2") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)))) (h "108sb76r4xw7cv7x4c8yv5gy2p8zapaq2gjxkjk9cmma0fpiv6pn")))

(define-public crate-kproc-parser-0.0.1-alpla.3 (c (n "kproc-parser") (v "0.0.1-alpla.3") (d (list (d (n "proc-macro2") (r "^1.0.47") (o #t) (d #t) (k 0)))) (h "0lpf5afclk8gfxqzcbb0ygh98975xzd81zvpx56m4ryiwyyhiibz") (s 2) (e (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1-alpla.4 (c (n "kproc-parser") (v "0.0.1-alpla.4") (d (list (d (n "proc-macro2") (r "^1.0.47") (o #t) (d #t) (k 0)))) (h "1xyjbkbdppvw2jmv7jk963sxpvylp5w4f0yf5rqcwwlpl2jikxdb") (s 2) (e (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1-alpla.5 (c (n "kproc-parser") (v "0.0.1-alpla.5") (d (list (d (n "proc-macro2") (r "^1.0.47") (o #t) (d #t) (k 0)))) (h "10g3nczbd73gwq7dng8hkdy8a3m6vi1b9kf54j6dhmm35d7f8adx") (s 2) (e (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1-alpla.6 (c (n "kproc-parser") (v "0.0.1-alpla.6") (d (list (d (n "proc-macro2") (r "^1.0.47") (o #t) (d #t) (k 0)))) (h "0m4fj0jskicxmca6j6md252l9vwvxn5diwinh1dnrjx37x6yivdf") (s 2) (e (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1-alpla.7 (c (n "kproc-parser") (v "0.0.1-alpla.7") (d (list (d (n "proc-macro2") (r "^1.0.47") (o #t) (d #t) (k 0)))) (h "0bk4n4bzyxkf8f3fwfnr5cx76krsw2gzz8l4rnvvi8gpxb1x7g12") (s 2) (e (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1-beta.1 (c (n "kproc-parser") (v "0.0.1-beta.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (o #t) (d #t) (k 0)))) (h "046xj5xjd2saz1gs12lwwxv4ifg074fzihngg0cr74zgi56dr635") (s 2) (e (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1-beta.2 (c (n "kproc-parser") (v "0.0.1-beta.2") (d (list (d (n "proc-macro2") (r "^1.0.47") (o #t) (d #t) (k 0)))) (h "08f2ak9hkd8lvgvsk4x3cyncn7m97g0y00z3kdg26gf41g8a87rg") (s 2) (e (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1-beta.3 (c (n "kproc-parser") (v "0.0.1-beta.3") (d (list (d (n "proc-macro2") (r "^1.0.47") (o #t) (d #t) (k 0)))) (h "1gxf7vkg9zqfzazliqkzdqagy1xwr09vc1cj3x6cb6xx12y5bps4") (f (quote (("builtin_diagnostic")))) (s 2) (e (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1-beta.4 (c (n "kproc-parser") (v "0.0.1-beta.4") (d (list (d (n "proc-macro2") (r "^1.0.47") (o #t) (d #t) (k 0)))) (h "1imirq4crsh6b1z9f88bfjilqkxwl9v03mmy95avwz5zml14zjmg") (f (quote (("builtin_diagnostic")))) (s 2) (e (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1-beta.5 (c (n "kproc-parser") (v "0.0.1-beta.5") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0iiij07vz26c8p4s1vg8pq8q01vnbc8ynwg7wz0kv31sy1sqzrkn") (f (quote (("builtin_diagnostic")))) (s 2) (e (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1-beta.6 (c (n "kproc-parser") (v "0.0.1-beta.6") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0g1cvpx4l58bvf6gbz6vc61sk53a1mm8ji7jxv3kik2pkdd7649m") (f (quote (("builtin_diagnostic")))) (s 2) (e (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1-beta.7 (c (n "kproc-parser") (v "0.0.1-beta.7") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1xzk61a064c74sf0m0nz9f45rx7pi4w972ipd6r5pwrs3vccb8x4") (f (quote (("builtin_diagnostic")))) (s 2) (e (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

