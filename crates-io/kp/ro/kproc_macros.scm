(define-module (crates-io kp ro kproc_macros) #:use-module (crates-io))

(define-public crate-kproc_macros-1.0.0 (c (n "kproc_macros") (v "1.0.0") (d (list (d (n "kmacros_shim") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "036kqkxadkxmda67k1qykp1ja5s00br1pk2bw4hj4vbc5cha0h90")))

(define-public crate-kproc_macros-3.0.0 (c (n "kproc_macros") (v "3.0.0") (d (list (d (n "kmacros_shim") (r "^3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0xxbrrp14mi32jbqzsw59sk6ks7hxlv9f3m33fb4jvlv49nn8nh5")))

(define-public crate-kproc_macros-3.0.1 (c (n "kproc_macros") (v "3.0.1") (d (list (d (n "kmacros_shim") (r "^3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0igpp0cs12s5h7b1qrj703zalx1n3a3290aqyjnll13599191lyg")))

(define-public crate-kproc_macros-3.0.2 (c (n "kproc_macros") (v "3.0.2") (d (list (d (n "kmacros_shim") (r "^3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1b92gp5yhj97d4lm40gf44vwj14l2lr2gla86s42k5lvn782p0al")))

(define-public crate-kproc_macros-4.0.0 (c (n "kproc_macros") (v "4.0.0") (d (list (d (n "kmacros_shim") (r "^4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0li51plc7qssnpq104shv3zmf0lpj0b466jvqxx7agil1xsm2ixb")))

(define-public crate-kproc_macros-5.0.0 (c (n "kproc_macros") (v "5.0.0") (d (list (d (n "kmacros_shim") (r "^5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0z5s9ldqfl66g8a3z55m5kc6y95slra0i9f890ph8xmyghhpqi3g")))

(define-public crate-kproc_macros-5.1.0 (c (n "kproc_macros") (v "5.1.0") (d (list (d (n "kmacros_shim") (r "^5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1xqgsym88flvisqf878cp7vs0kjiy3gvwz185119nv47km50bmyv")))

(define-public crate-kproc_macros-6.0.0 (c (n "kproc_macros") (v "6.0.0") (d (list (d (n "kmacros_shim") (r "^6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "09pqh2d21da4pjvbi67v5r6f2kwv0kf9dck6xijz10lgd0m5ygh8")))

