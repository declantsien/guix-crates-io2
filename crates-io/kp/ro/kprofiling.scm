(define-module (crates-io kp ro kprofiling) #:use-module (crates-io))

(define-public crate-kprofiling-0.1.0 (c (n "kprofiling") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "1j01xvnj621n0sb7azhysslwng5qw3ikjq62mw88xz8nazbx0g24")))

(define-public crate-kprofiling-0.1.1 (c (n "kprofiling") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "17vr778081pbc6hh5dkr8lkv9zp09gk20zw3i2y8jq0fqh5dd33y")))

