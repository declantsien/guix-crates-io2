(define-module (crates-io kp er kperf-rs) #:use-module (crates-io))

(define-public crate-kperf-rs-0.1.0 (c (n "kperf-rs") (v "0.1.0") (d (list (d (n "kperf-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "07r25s0g00w1xdk59f498rsr996nk1cvly0bd2jj9nx26pzsri1h")))

(define-public crate-kperf-rs-0.1.1 (c (n "kperf-rs") (v "0.1.1") (d (list (d (n "kperf-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "183w5krl74v25jxw538y4zgdy77vrsjwzsgkqncrxxwp829haf1x")))

