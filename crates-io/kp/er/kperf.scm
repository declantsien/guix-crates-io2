(define-module (crates-io kp er kperf) #:use-module (crates-io))

(define-public crate-kperf-0.0.0 (c (n "kperf") (v "0.0.0") (h "1i03w3g5943wrh4nrm47iwgs98mn3pm2sck2lb1ibx1dp3kiki5b")))

(define-public crate-kperf-0.1.0 (c (n "kperf") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "kperf-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0n1nhq0rmnv6di1q5x0k5bi061s33zvm1hi7h8d5mnvzdykc3l9n")))

