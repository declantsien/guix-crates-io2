(define-module (crates-io il p- ilp-packet) #:use-module (crates-io))

(define-public crate-ilp-packet-0.2.0 (c (n "ilp-packet") (v "0.2.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "091y7vpgi1f4s5n2zqpfjdb7xpm2s872xbp0w706z6jbsml0lh6c")))

(define-public crate-ilp-packet-0.3.0 (c (n "ilp-packet") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.4") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)))) (h "0kzabn6fvpfzv7b44chkmy4dz2qqc01bn9l4w3nm4qn3j34jsn91")))

