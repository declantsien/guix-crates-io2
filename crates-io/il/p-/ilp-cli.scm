(define-module (crates-io il p- ilp-cli) #:use-module (crates-io))

(define-public crate-ilp-cli-0.1.1-beta.1 (c (n "ilp-cli") (v "0.1.1-beta.1") (d (list (d (n "clap") (r "^2.33.0") (k 0)) (d (n "http") (r "^0.1.18") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (f (quote ("default-tls"))) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tungstenite") (r "^0.9.1") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1k2qs6m5hk2i9wax8rb089s96mvzzqm1gqdcpyzrqxz9kinzcyn9")))

(define-public crate-ilp-cli-0.2.0 (c (n "ilp-cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (k 0)) (d (n "http") (r "^0.1.18") (k 0)) (d (n "reqwest") (r "^0.9.21") (f (quote ("default-tls"))) (k 0)) (d (n "serde") (r "^1.0.101") (k 0)) (d (n "serde_json") (r "^1.0.41") (k 0)) (d (n "tungstenite") (r "^0.9.1") (f (quote ("tls"))) (k 0)) (d (n "url") (r "^2.1.0") (k 0)))) (h "1bdgwvraz792q2hiwb5fwjp28nb0i4kzc3vzl7zab9hnvbmf6srs")))

(define-public crate-ilp-cli-0.3.0 (c (n "ilp-cli") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (k 0)) (d (n "http") (r "^0.1.18") (k 0)) (d (n "reqwest") (r "^0.9.22") (f (quote ("default-tls"))) (k 0)) (d (n "serde") (r "^1.0.101") (k 0)) (d (n "serde_json") (r "^1.0.41") (k 0)) (d (n "thiserror") (r "^1.0.4") (k 0)) (d (n "tungstenite") (r "^0.9.1") (f (quote ("tls"))) (k 0)) (d (n "url") (r "^2.1.0") (k 0)))) (h "0lmycwbwcs4xasv3sprnil52c2r54mm7m90g4x67bd7l1xflbrzr")))

