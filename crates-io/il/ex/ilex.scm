(define-module (crates-io il ex ilex) #:use-module (crates-io))

(define-public crate-ilex-0.5.0 (c (n "ilex") (v "0.5.0") (d (list (d (n "annotate-snippets") (r "^0.10.0") (d #t) (k 0)) (d (n "byteyarn") (r "^0.5") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "ilex-attr") (r "^0.5.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.3") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "rustc_apfloat") (r "^0.2.0") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.5.0") (d #t) (k 0)) (d (n "twie") (r "^0.5") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 0)))) (h "1dhqq49406ldgl2l76zg5i9ily89n08gpv55h9cm243x64gmn0m0")))

