(define-module (crates-io il li illist) #:use-module (crates-io))

(define-public crate-illist-0.0.1 (c (n "illist") (v "0.0.1") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0p3r365j50fvwd62jwqmiik3rzyz4xzi2jgj7xs31h6giqzc5fc2")))

(define-public crate-illist-0.1.0 (c (n "illist") (v "0.1.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0xm58968h58j29smwsn61jx4kflkgb5frp7ackcmgif9v0ril6zw")))

