(define-module (crates-io il li illicit-macro) #:use-module (crates-io))

(define-public crate-illicit-macro-0.9.0 (c (n "illicit-macro") (v "0.9.0") (d (list (d (n "proc-macro-error") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12xmjwhfpabr708k1lbckq6d94sf67psni8wjclb6jwfl0bls9bn")))

(define-public crate-illicit-macro-0.10.0 (c (n "illicit-macro") (v "0.10.0") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1idzsznyy69z2x225af06c3ssr58km7lif75i9kwjlkjjsvd89g9")))

(define-public crate-illicit-macro-1.0.0 (c (n "illicit-macro") (v "1.0.0") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15kxgla6h83gydxsl29vkwixprgikdr5lbzvszk4rhs2zb2gldg7")))

