(define-module (crates-io il li illicit) #:use-module (crates-io))

(define-public crate-illicit-0.9.0 (c (n "illicit") (v "0.9.0") (d (list (d (n "illicit-macro") (r "^0.9.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)))) (h "0l9a8icscql5mnail4hw17jm1aawlamp5znfszc6si8pj4w7d2n6")))

(define-public crate-illicit-0.9.1 (c (n "illicit") (v "0.9.1") (d (list (d (n "illicit-macro") (r "^0.9.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)))) (h "1a8m82sfcrznvbwr5cqmmnplkjxw6jfdcxv0lfkzi8imn7j2hgqd")))

(define-public crate-illicit-0.9.2 (c (n "illicit") (v "0.9.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "illicit-macro") (r "^0.9.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)))) (h "0c0ixacjpf8w4n8myxkqnqbkaxp8djqj1h90a2y42aqmngb1jwmz")))

(define-public crate-illicit-0.10.0 (c (n "illicit") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "illicit-macro") (r "^0.10.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)))) (h "1kmya6pfa9c41rsiklndpd4836bxs6shx3wxr4sdv4l7rxc3wrdz")))

(define-public crate-illicit-1.0.0 (c (n "illicit") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "illicit-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)))) (h "1drk9m40ih0ydavk9ac3zl6gj6j9fb5p52ldp10rgf1a7dixqfnm")))

(define-public crate-illicit-1.1.0 (c (n "illicit") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "illicit-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "insta") (r "^0.16.1") (d #t) (k 2)) (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)))) (h "18r32mnawbiscxjv9x9kyrwls0w8z893s605qlvfisiasmqkdmjd")))

(define-public crate-illicit-1.1.1 (c (n "illicit") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "illicit-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "insta") (r "^0.16.1") (d #t) (k 2)) (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)))) (h "19l4vfjcx04nnavwawn37qz4h8b1x72i4qs5rk9v7c4ijr49wpc2")))

(define-public crate-illicit-1.1.2 (c (n "illicit") (v "1.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "illicit-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "insta") (r "^1.0.0") (d #t) (k 2)) (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)))) (h "1lqh2yiwlrwf5nfjm1n34ld64gyfjkw0vycngsjnqp2m4g9kdcx8")))

