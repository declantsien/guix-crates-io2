(define-module (crates-io il lu illumina_coordinates) #:use-module (crates-io))

(define-public crate-illumina_coordinates-0.9.0 (c (n "illumina_coordinates") (v "0.9.0") (h "0mj0hqyxsjx17p9lpibswi36zpqhdvj9gh0nb0q879sxvhrhqza5")))

(define-public crate-illumina_coordinates-0.9.1 (c (n "illumina_coordinates") (v "0.9.1") (h "16dh8x9dhfzpq4q72mnsi00c102cjiqhg5lnmpgqsrb4p8ynh172")))

