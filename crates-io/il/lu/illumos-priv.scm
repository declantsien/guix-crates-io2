(define-module (crates-io il lu illumos-priv) #:use-module (crates-io))

(define-public crate-illumos-priv-0.1.0 (c (n "illumos-priv") (v "0.1.0") (h "039pcqblyyfgybibgadx1s73mpbc1130jd35d7766q79c3g3ngb5")))

(define-public crate-illumos-priv-0.2.0 (c (n "illumos-priv") (v "0.2.0") (h "0czzwsdqqn3cfnlzim9p9rzmld6jmygfv2qcw2grmdm7s89nylvx")))

