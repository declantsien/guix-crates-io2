(define-module (crates-io il tc iltcme) #:use-module (crates-io))

(define-public crate-iltcme-0.1.0 (c (n "iltcme") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 1)))) (h "1yp2wc08bm0gybvmhxvvpfkjid84ksq023kl70glxwq9sdaa3w5a")))

(define-public crate-iltcme-0.2.0 (c (n "iltcme") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("auto-initialize"))) (d #t) (k 2)))) (h "10wli7cx3l79r9q9wdp5h001lkv8c226n08pbma9hkqqqbxqga5w")))

(define-public crate-iltcme-0.2.2 (c (n "iltcme") (v "0.2.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.2") (f (quote ("auto-initialize"))) (d #t) (k 2)))) (h "119916nmdcv19pv6w9sfkpb1q1lzih1q85byrjbdvwjxxca7xkjk")))

