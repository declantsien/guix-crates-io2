(define-module (crates-io il gd ilgda-ipc) #:use-module (crates-io))

(define-public crate-ilgda-ipc-0.1.0 (c (n "ilgda-ipc") (v "0.1.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "09sj1c2bvj2pc8a0mqs50gghhn721y2lvi2fnfr250pw1sx6cay7") (f (quote (("default")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-ilgda-ipc-0.1.1 (c (n "ilgda-ipc") (v "0.1.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1alad7syyr2kpmyyzsq3la7spq8gsrfmvlfj6riygdazk2wbfzkv") (f (quote (("default")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-ilgda-ipc-0.1.2 (c (n "ilgda-ipc") (v "0.1.2") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "12950kr39sqy4gn78j8blgr7nqwnlwbax7n2lwz1a05mb56n246q") (f (quote (("default")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-ilgda-ipc-0.1.3 (c (n "ilgda-ipc") (v "0.1.3") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "06k7m2y42wy0q39ygmy7ljrcvp75wgw8d82h341xp6y4y31ixx2z") (f (quote (("default")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-ilgda-ipc-0.2.0 (c (n "ilgda-ipc") (v "0.2.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "ipc-channel") (r "^0.18.0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1whk5j7v25234zx4zinmxafqyh0v3c8w5rxdwvnvnr05rixdfpq2")))

(define-public crate-ilgda-ipc-0.2.1 (c (n "ilgda-ipc") (v "0.2.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "heap-array") (r "^0.1.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ipc-channel") (r "^0.18") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1gidyc0wc6d9kgb8vgxdblhk4fq9dkxnk34majdi3dqakxcbhcy7")))

