(define-module (crates-io il da ilda) #:use-module (crates-io))

(define-public crate-ilda-0.0.1 (c (n "ilda") (v "0.0.1") (d (list (d (n "log") (r "0.3.*") (d #t) (k 0)))) (h "0z0zv4nr6zh60xwyvskfggwg10k0zmlz9wan16xpyapxm63gj44z")))

(define-public crate-ilda-0.0.2 (c (n "ilda") (v "0.0.2") (d (list (d (n "image") (r "0.10.*") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)))) (h "1vdwmbr7babcqa0rh7hqxqdayh6x8mrxlz7fcyc4v6fl7wlpn7is")))

(define-public crate-ilda-0.0.3 (c (n "ilda") (v "0.0.3") (d (list (d (n "image") (r "0.10.*") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)))) (h "13bla9i85npx8qmbrm9raic3c82nvxmpfxsjb4fxp9qkdi7xbh97")))

(define-public crate-ilda-0.0.4 (c (n "ilda") (v "0.0.4") (d (list (d (n "image") (r "0.10.*") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)))) (h "10fj2abnhlf5w7sbqi9kvl0f8s47gd32nrr1lj52sfcbcamxrclj")))

(define-public crate-ilda-0.0.5 (c (n "ilda") (v "0.0.5") (d (list (d (n "image") (r "0.10.*") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)))) (h "1cyc0mnwaj0k7nk3vksqimfya4aclrh6f3am7gnb7094lcj4hkqq")))

(define-public crate-ilda-0.0.6 (c (n "ilda") (v "0.0.6") (d (list (d (n "image") (r "0.10.*") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)))) (h "1lvfs12ky6hi98v2d7wbx3dy75s3g8k6dibx51451hs44pyhni1p")))

(define-public crate-ilda-0.2.0 (c (n "ilda") (v "0.2.0") (d (list (d (n "image") (r "0.10.*") (d #t) (k 2)) (d (n "point") (r "^0.3") (d #t) (k 0)))) (h "1ghwg751zjjz33h7vsj11a0q0gxi88dwdrgnh7xca6hh5q9j9c6l")))

