(define-module (crates-io il da ilda-idtf) #:use-module (crates-io))

(define-public crate-ilda-idtf-0.1.0 (c (n "ilda-idtf") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)) (d (n "zerocopy") (r "^0.3") (d #t) (k 0)))) (h "1fr4vgzfqx1kjcqrh9921p7lydzcyvdhmv63n1yf0k3azbb2vsrl")))

