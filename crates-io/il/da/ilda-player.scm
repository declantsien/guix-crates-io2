(define-module (crates-io il da ilda-player) #:use-module (crates-io))

(define-public crate-ilda-player-0.0.1 (c (n "ilda-player") (v "0.0.1") (d (list (d (n "argparse") (r "0.2.*") (d #t) (k 0)) (d (n "ilda") (r "^0.0.6") (d #t) (k 0)) (d (n "lase") (r "^0.0.1") (d #t) (k 0)))) (h "0xb00r1q0f0xs6kcygy7l7azdnqyiqhrlz1i9ccsqc1wi22wzzzd")))

(define-public crate-ilda-player-0.0.2 (c (n "ilda-player") (v "0.0.2") (d (list (d (n "argparse") (r "0.2.*") (d #t) (k 0)) (d (n "ilda") (r "^0.0.6") (d #t) (k 0)) (d (n "lase") (r "^0.0.1") (d #t) (k 0)))) (h "161c5xpqdzl7cnp6il4fwn2ylvl663agi2a88v5kf9kcv2k2l80j")))

