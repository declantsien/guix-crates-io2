(define-module (crates-io il vm ilvm) #:use-module (crates-io))

(define-public crate-ilvm-0.2.0 (c (n "ilvm") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "combine") (r "^3.6.2") (d #t) (k 0)))) (h "133fyn0ljw4pb5rgfrk309zz4ax2p63dn7cb9kfd4xzpbczycqz3")))

(define-public crate-ilvm-0.2.1 (c (n "ilvm") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "combine") (r "^3.6.2") (d #t) (k 0)))) (h "03jkwa3dvwz28j9y13pkjdz55h2xhdawdpmkns5dcfm2iyw62swr")))

