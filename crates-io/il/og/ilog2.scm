(define-module (crates-io il og ilog2) #:use-module (crates-io))

(define-public crate-ilog2-0.0.1 (c (n "ilog2") (v "0.0.1") (h "0s0w6kr7cm9bj329s6hys1fy80z399fvk7iilyxsdv2v756ls9hc")))

(define-public crate-ilog2-0.0.2 (c (n "ilog2") (v "0.0.2") (h "0680gr5d99cccgrw04fnrjyal2y7805qf4siihmdxxldp768j6vb")))

(define-public crate-ilog2-0.1.0 (c (n "ilog2") (v "0.1.0") (h "0s4mdcfzyc9za5i9kpxa25ba050hs25cc3ppk67c23lz9llpas8w")))

(define-public crate-ilog2-0.1.1 (c (n "ilog2") (v "0.1.1") (h "0nnmw4r33d8yq3n7wc5mb7k1c8zv4mayicv856li102z2ybrarsn")))

(define-public crate-ilog2-0.2.0 (c (n "ilog2") (v "0.2.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1c4srq5idakbkkarskf33xbgynlcvsmcghz7j8vrycnckwxfghqc")))

(define-public crate-ilog2-0.2.1 (c (n "ilog2") (v "0.2.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0m16l14kxlwr1kgyfrn23jj1dbf7h86ig5igbikpj9d003c3las2")))

(define-public crate-ilog2-0.2.2 (c (n "ilog2") (v "0.2.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0crd0lwl0qsqkcnmw62zwwla0vm1wgxafp26lisw1846inlgldfa")))

