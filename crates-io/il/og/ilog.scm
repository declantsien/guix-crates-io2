(define-module (crates-io il og ilog) #:use-module (crates-io))

(define-public crate-ilog-0.1.0 (c (n "ilog") (v "0.1.0") (d (list (d (n "num-traits") (r "^0") (d #t) (k 0)))) (h "1xz35a4k77ivkk0mw8c143rfqwls0yj4q3wwgisz70nxqhir8jd3")))

(define-public crate-ilog-0.1.1 (c (n "ilog") (v "0.1.1") (d (list (d (n "num-traits") (r "^0") (d #t) (k 0)))) (h "1ry4vqchx5f0z624w8pii438nilzcfhl1psj99vbjz4w1z351paa")))

(define-public crate-ilog-0.1.2 (c (n "ilog") (v "0.1.2") (h "0c9y5d0jg33yyy1m1yhp0ll9hq8xy9pmks708pva7k9c2h8lgv0a")))

(define-public crate-ilog-0.1.3 (c (n "ilog") (v "0.1.3") (h "04zlfldyy66dmiiqdz4nwlv4m6y8ynp5pdnlr54d1q2j7wvybrxc")))

(define-public crate-ilog-0.1.4 (c (n "ilog") (v "0.1.4") (h "0lz66r05yqjc9k86nwlyj95snlhvnys1xvb528ljjbdyxysqaxvr")))

(define-public crate-ilog-1.0.0 (c (n "ilog") (v "1.0.0") (h "15vx3hgiq1wsm4lj22qli43d0i1wg98lgz3q0pjx918sv8q1bx5i")))

(define-public crate-ilog-1.0.1 (c (n "ilog") (v "1.0.1") (h "0qm3p4v2bqj623ailb2ddcsas7vfbb69q5fsh0xmlq2xb9nqkmm9")))

