(define-module (crates-io il c- ilc-format-energymech) #:use-module (crates-io))

(define-public crate-ilc-format-energymech-0.1.0 (c (n "ilc-format-energymech") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "ilc-base") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "1cjv3png5a77bzwzmyaapngvr6m2s347lbmqwhwlgfn2fd3skrn3")))

(define-public crate-ilc-format-energymech-0.1.1 (c (n "ilc-format-energymech") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "ilc-base") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "08gfbir0di8dvn5nz17haclcc0djjr8q663gi1nwraac03yb4pzj")))

(define-public crate-ilc-format-energymech-0.2.0 (c (n "ilc-format-energymech") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "ilc-base") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "1h67x0ph2v33bd0rkf5nrk9lvn2sbz8vn68xpfpl15ia08wy3ls5")))

