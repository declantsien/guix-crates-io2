(define-module (crates-io il c- ilc-ops) #:use-module (crates-io))

(define-public crate-ilc-ops-0.1.0 (c (n "ilc-ops") (v "0.1.0") (d (list (d (n "blist") (r "^0.0.4") (d #t) (k 0)) (d (n "ilc-base") (r "^0.1.0") (d #t) (k 0)))) (h "1fcc6mj1vch6sg3g7w9sspxd7d3k6gfgxn4rya3h6sdmjl8lak9k")))

(define-public crate-ilc-ops-0.1.1 (c (n "ilc-ops") (v "0.1.1") (d (list (d (n "blist") (r "^0.0.4") (d #t) (k 0)) (d (n "ilc-base") (r "^0.1.0") (d #t) (k 0)))) (h "1d9ssyhqrrdgv0s0p8lmilj201wz8ad72q7b74qv6g2sm4317ng9")))

(define-public crate-ilc-ops-0.1.2 (c (n "ilc-ops") (v "0.1.2") (d (list (d (n "bit-set") (r "^0.3.0") (d #t) (k 0)) (d (n "blist") (r "^0.0.4") (d #t) (k 0)) (d (n "ilc-base") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "0dsmr6z4mswb2b8hramip4zk0adx975v6gkqpiy4r602z7332f2y")))

