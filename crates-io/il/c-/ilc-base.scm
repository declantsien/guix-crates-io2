(define-module (crates-io il c- ilc-base) #:use-module (crates-io))

(define-public crate-ilc-base-0.1.0 (c (n "ilc-base") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "1mhln6v35akh7kv3xakslir6r65s60x4p57gfgdvvagvg5iihzpb")))

(define-public crate-ilc-base-0.1.1 (c (n "ilc-base") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "1yfhrrz4sf1z598wjl5qhrg1k1c8ykaaa8a48a2hfhpq0j3mj9gy")))

(define-public crate-ilc-base-0.1.2 (c (n "ilc-base") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "0qfzphab96gip5dgim6c7aflrqql4kz51rwkjczbpmbsgvcgipmb")))

(define-public crate-ilc-base-0.2.0 (c (n "ilc-base") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "0p8yxfw8jfdiq3ssyl9g61mh8jbdiwq36nxmkkp7a37k93mipli5")))

