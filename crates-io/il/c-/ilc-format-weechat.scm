(define-module (crates-io il c- ilc-format-weechat) #:use-module (crates-io))

(define-public crate-ilc-format-weechat-0.1.0 (c (n "ilc-format-weechat") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "ilc-base") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "0rc16njk6kqy6hjl7c43praprnh5583n2zlhbdwq87pd2rky7vw1")))

(define-public crate-ilc-format-weechat-0.1.1 (c (n "ilc-format-weechat") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "ilc-base") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "13j6n6kdcql46r27bjhrc6nzd59z339b66hasgvk456r92aag1bg")))

(define-public crate-ilc-format-weechat-0.2.0 (c (n "ilc-format-weechat") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "ilc-base") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "00z7zcir0xm56ax85a1z7v4n8km7pvspcrdp9clrm6nlmwwv4cvn")))

