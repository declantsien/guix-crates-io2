(define-module (crates-io il er ilert) #:use-module (crates-io))

(define-public crate-ilert-0.2.1 (c (n "ilert") (v "0.2.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)))) (h "1xj974gaphsbyw0a727265v7mzfqfgf9hgvn5sbl3j7g047dwsf1")))

(define-public crate-ilert-0.2.2 (c (n "ilert") (v "0.2.2") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)))) (h "0q741s39xspqzb09n890h4anl5izi3z6hxjy0gysl2xb6l4ja1kk")))

(define-public crate-ilert-0.2.3 (c (n "ilert") (v "0.2.3") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)))) (h "1qrbqy4nr4mgvdsmh40islkymhlkaw0658dh1iz44vy0i5gs8vjl")))

(define-public crate-ilert-0.3.0 (c (n "ilert") (v "0.3.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "1w7fs48l784f9n4jxfl6zfn54i73rixjgdvfxa90x77svfsvz67j")))

(define-public crate-ilert-1.0.0 (c (n "ilert") (v "1.0.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04ka5sgp99vrahigrb4hir6cv1vy4icq0rs0r3crszmgk8rp14bb")))

(define-public crate-ilert-2.0.0 (c (n "ilert") (v "2.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zm8mc938vzsb6gn9sjy3ks2hwyndwqap3k936dy1g73y6i95y9k")))

(define-public crate-ilert-3.0.0 (c (n "ilert") (v "3.0.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06jxz485ywm1sz2fh4a6k3sfa4jc6d0l4a5cal9pgph6vnghqwrw")))

(define-public crate-ilert-3.1.0 (c (n "ilert") (v "3.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nd9sgjlxngc406030sq75nzlr5r43ykzd08580gsdzi7jz1bgf1")))

(define-public crate-ilert-3.2.0 (c (n "ilert") (v "3.2.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bw1lz6k3adzcsyqjzrvr5j3qvdlmd7ikyw2qsiwp9dkrbqjypyc")))

