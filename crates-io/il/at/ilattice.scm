(define-module (crates-io il at ilattice) #:use-module (crates-io))

(define-public crate-ilattice-0.1.0 (c (n "ilattice") (v "0.1.0") (d (list (d (n "glam") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "morton-encoding") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "111b1i0pcdvhkwmp04a7ig64hvni0rvqf8wfl5c0c3hw75391nzk") (f (quote (("default" "glam"))))))

(define-public crate-ilattice-0.2.0 (c (n "ilattice") (v "0.2.0") (d (list (d (n "glam") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "morton-encoding") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1p7m8zlvxbgrnd3mdfbm2lagz7wx2940rn793pfwzmsn2vkx0mff") (f (quote (("default" "glam"))))))

(define-public crate-ilattice-0.3.0 (c (n "ilattice") (v "0.3.0") (d (list (d (n "glam") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "morton-encoding") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m1jlcyblf51lj9p2knpjqp8ryjm1c7bfwywmg2q9v545rrrz3cf") (f (quote (("default" "glam"))))))

(define-public crate-ilattice-0.4.0 (c (n "ilattice") (v "0.4.0") (d (list (d (n "glam") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "morton-encoding") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16rpcxa5wgh2rqkasrzprikq3ar3an3j84hbhnxflkxdr6hkhb0l") (f (quote (("default" "glam"))))))

