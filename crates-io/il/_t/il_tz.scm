(define-module (crates-io il _t il_tz) #:use-module (crates-io))

(define-public crate-il_tz-0.1.0 (c (n "il_tz") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0n9y9l5bc3mrwpqk2kd7n39gdrppx0d3s1rj2w6vhb4sc31svlrc")))

(define-public crate-il_tz-0.1.1 (c (n "il_tz") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1dm46w7aq50hdjd5s7zhmn2dmf5cnirfkpyypnf24hw65qsd99vh")))

(define-public crate-il_tz-0.1.2 (c (n "il_tz") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "086mn5wsxgmwjf8ywpvkkx2vw9h5xd3bb87944mbfw8raj5ksyvz") (f (quote (("cli" "clap"))))))

(define-public crate-il_tz-0.1.3 (c (n "il_tz") (v "0.1.3") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1g0q13n7kp5c1b3qk4jahf7yfd7463cs65zqhnlak90b3lp13sin") (f (quote (("cli" "clap"))))))

