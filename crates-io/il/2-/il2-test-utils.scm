(define-module (crates-io il #{2-}# il2-test-utils) #:use-module (crates-io))

(define-public crate-il2-test-utils-0.1.0 (c (n "il2-test-utils") (v "0.1.0") (h "0aj16k49blyb4sz8vnbb4igyqlmc2jlcd0vnfwybpc3hpzdz15l0") (y #t)))

(define-public crate-il2-test-utils-0.1.1 (c (n "il2-test-utils") (v "0.1.1") (h "0ra7snv2l2q63s0skc2adjw4dzblih77ra103dy3hvfpsw4h5i3s")))

