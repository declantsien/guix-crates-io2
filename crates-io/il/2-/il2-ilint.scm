(define-module (crates-io il #{2-}# il2-ilint) #:use-module (crates-io))

(define-public crate-il2-ilint-1.1.0 (c (n "il2-ilint") (v "1.1.0") (h "02rzjy5h7jlc3zjwk4fbvv76vzw29kk9rbnp36x686akrvzbqsgk")))

(define-public crate-il2-ilint-1.1.1 (c (n "il2-ilint") (v "1.1.1") (h "0nxm792w1pvj1y6jign4rlssmsjf1sp3yh8z3pk12m7p8wlnyjs9")))

