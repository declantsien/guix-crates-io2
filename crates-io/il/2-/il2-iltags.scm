(define-module (crates-io il #{2-}# il2-iltags) #:use-module (crates-io))

(define-public crate-il2-iltags-1.0.0 (c (n "il2-iltags") (v "1.0.0") (h "0r2zbb966wnjhhy1bhz7j98m626c43h5avj980jbsxwix4106x1f") (y #t)))

(define-public crate-il2-iltags-1.0.1 (c (n "il2-iltags") (v "1.0.1") (h "1037vm09w8ddh1db3x18wjhik8qzpz3z4syar9d17y6k6kwvi73d")))

(define-public crate-il2-iltags-1.1.0 (c (n "il2-iltags") (v "1.1.0") (h "087zfn6hvb7jyw09gagcc57cphxx9gjbr3w54sf46848mzmz46m8")))

(define-public crate-il2-iltags-1.1.1 (c (n "il2-iltags") (v "1.1.1") (h "0zm6l3kwg6kv3rz9hfrglzlqa6a5bjk2mhi2mpsqji7llncj7h6f")))

(define-public crate-il2-iltags-1.2.0 (c (n "il2-iltags") (v "1.2.0") (h "16b5i99f30x80p0b82q9jywsz9748lmkrs3q8dlx365szcl44y87")))

(define-public crate-il2-iltags-1.3.0 (c (n "il2-iltags") (v "1.3.0") (h "0zifzwlph9bmg66cnh49kbvrhf02agqq7kxxkhwjdgyxvgjg7h52")))

(define-public crate-il2-iltags-1.4.0 (c (n "il2-iltags") (v "1.4.0") (h "14pndhnpa66ycszy2km30spskx0kdpr0dm8r31z4p3y1bzv7qgwx")))

