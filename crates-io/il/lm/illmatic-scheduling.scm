(define-module (crates-io il lm illmatic-scheduling) #:use-module (crates-io))

(define-public crate-illmatic-scheduling-0.1.0 (c (n "illmatic-scheduling") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12jyayjnwz8fh25fwn0qwpvzfhf01jlkmssbqvnc6crc50bwd8pb")))

(define-public crate-illmatic-scheduling-0.1.1 (c (n "illmatic-scheduling") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "188aa766gidwh3knx44gbr0s8zhkxmflggjzgxdyqf6slfbym00j")))

(define-public crate-illmatic-scheduling-0.1.2 (c (n "illmatic-scheduling") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06rg50lssqm59b1pcgg2iczp7p8x6wszkfbca1hmckb9q1pb3zp0")))

