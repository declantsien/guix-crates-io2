(define-module (crates-io il #{03}# il0373) #:use-module (crates-io))

(define-public crate-il0373-0.1.1 (c (n "il0373") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 2)))) (h "1vfqabvn9w2dw5xd3hsfvxm1fkpbpvr9p3rmdrfph023rmxpb95k") (f (quote (("test" "embedded-graphics") ("sram") ("graphics" "embedded-graphics") ("default" "graphics"))))))

