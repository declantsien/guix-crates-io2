(define-module (crates-io il i9 ili9163_driver) #:use-module (crates-io))

(define-public crate-ili9163_driver-0.1.0 (c (n "ili9163_driver") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0cxjy8x5rg4mlxyiigw1lkw0fyyn9fr3nk1ff4jcpqk48kmzgzlv")))

(define-public crate-ili9163_driver-0.2.0 (c (n "ili9163_driver") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0bgz4imnck34wazbggpjqq08y1y55kyg6hb1ydwj84g3qqgryyzg")))

(define-public crate-ili9163_driver-0.2.1 (c (n "ili9163_driver") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1680pgm6ngfmmjl26zycq8k06wbkc3z7l0kv2dczhqbdkqdba88p")))

(define-public crate-ili9163_driver-0.2.2 (c (n "ili9163_driver") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "176kv6whq9vf4i0kxz7lgxqihcsc426cjh458s2i8b664chhdivf")))

(define-public crate-ili9163_driver-1.0.0 (c (n "ili9163_driver") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1hp5593jfmlvz1xgn8lsayw2qzcy7dvvihb7nz35r97ypamhqml2")))

