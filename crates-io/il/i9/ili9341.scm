(define-module (crates-io il i9 ili9341) #:use-module (crates-io))

(define-public crate-ili9341-0.1.0 (c (n "ili9341") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)))) (h "14ih0irsw84qaivr1vdy2fd93417a6xrz0sv4mwrmj94fn1fmhfp")))

(define-public crate-ili9341-0.1.1 (c (n "ili9341") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)))) (h "0i2d8k2hvrskx49gv4rznp6nzs7i6r4ar4f362acgimq8cggczlv")))

(define-public crate-ili9341-0.1.2 (c (n "ili9341") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)))) (h "02pval4jxrv040v9xy31qs0a23y4s087971pqprnhyd1kcr5qjxj")))

(define-public crate-ili9341-0.1.3 (c (n "ili9341") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)))) (h "0s41qsxnqc7ryn863rd3rk7wdkgflsf3pw0v3585f2x92j08hvf3")))

(define-public crate-ili9341-0.1.4 (c (n "ili9341") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0b89s11jhdizmgfjf7r4m5yisvc7nk9qimcph7y07i1mbinhx6hz")))

(define-public crate-ili9341-0.1.5 (c (n "ili9341") (v "0.1.5") (d (list (d (n "embedded-graphics") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1hsnfw94gzd1yc6w0lx192gbhy3aszlpl2yskhp8hmhslbgdywjh") (f (quote (("graphics" "embedded-graphics") ("default" "graphics")))) (y #t)))

(define-public crate-ili9341-0.1.6 (c (n "ili9341") (v "0.1.6") (d (list (d (n "embedded-graphics") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1p68yv30lkqb9n7qrc5vfap1g60yc7kidwpxxygx08ckkaqkpkha") (f (quote (("graphics" "embedded-graphics") ("default" "graphics")))) (y #t)))

(define-public crate-ili9341-0.1.7 (c (n "ili9341") (v "0.1.7") (d (list (d (n "embedded-graphics") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0cnj56gxyyd2fwl1s4mmr0n6j4rigp8cdagd8sh9kpjj15qbzqnd") (f (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-ili9341-0.2.0 (c (n "ili9341") (v "0.2.0") (d (list (d (n "embedded-graphics") (r "^0.4.7") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)))) (h "0p2yxz8rbp44agbbp1xzhywf2p7dl7kh94ny2c5fkazcf85vbcdl") (f (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-ili9341-0.3.0-beta.1 (c (n "ili9341") (v "0.3.0-beta.1") (d (list (d (n "embedded-graphics") (r "= 0.6.0-beta.1") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0f3xc7fslqwgxm0a3r1q89ijjzxas66yi8a99m6vnyi80jfkhqh0") (f (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-ili9341-0.3.0 (c (n "ili9341") (v "0.3.0") (d (list (d (n "embedded-graphics") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1v39rbfxrky6lrhdffm01i9rmwhvxsw7mnckpbyxbkqnd3rvw4in") (f (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-ili9341-0.4.0 (c (n "ili9341") (v "0.4.0") (d (list (d (n "display-interface") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "0wpsrq5mfrk28dk0iwmi8aqi4hdvggaaz97h56j1z0czh0hfp3jh") (f (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-ili9341-0.4.1 (c (n "ili9341") (v "0.4.1") (d (list (d (n "display-interface") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "093cy4blyws090zq4a7ckkc7d8igl438fagdyk3236azz0xz5qfx") (f (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-ili9341-0.5.0 (c (n "ili9341") (v "0.5.0") (d (list (d (n "display-interface") (r "^0.4.1") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)))) (h "1af06zqy6mm8r5nk8ndak04kp3f04v9v7w50gnp52l767s97m2aj") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-ili9341-0.6.0 (c (n "ili9341") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7.0") (d #t) (k 2)) (d (n "cortex-m-rtic") (r "^1.0.0") (d #t) (k 2)) (d (n "defmt-rtt") (r "^0.3.0") (d #t) (k 2)) (d (n "display-interface") (r "^0.5") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.6") (d #t) (k 2)) (d (n "stm32f4xx-hal") (r "^0.12.0") (f (quote ("stm32f411"))) (d #t) (k 2)))) (h "02mkknw75pcxbrcz2ncqc7r6l8gzpj0bl8hsb87n6xm0vc2mc46c") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

