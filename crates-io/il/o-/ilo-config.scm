(define-module (crates-io il o- ilo-config) #:use-module (crates-io))

(define-public crate-ilo-config-0.1.0 (c (n "ilo-config") (v "0.1.0") (d (list (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1k1lsvmb3j6j4072q843vxizhpdqj43j6h1nr5w2nb2n6piih9ra")))

(define-public crate-ilo-config-0.2.0 (c (n "ilo-config") (v "0.2.0") (d (list (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1glqjc67i7fzr5kmyim3x47s49d12q3n99srdv799b2wjsn2cx91")))

(define-public crate-ilo-config-0.2.1 (c (n "ilo-config") (v "0.2.1") (d (list (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0gn7ka4f8q6fxjryybd86wn0crfyk3xs3dbyvdb4lkm055d9b9af")))

