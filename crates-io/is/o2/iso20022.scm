(define-module (crates-io is o2 iso20022) #:use-module (crates-io))

(define-public crate-iso20022-0.1.0 (c (n "iso20022") (v "0.1.0") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "1hfsslac6vzfnqj63kax2ajv2i2dskpcq5ha771dmk1pnnh44z5q")))

(define-public crate-iso20022-0.1.1 (c (n "iso20022") (v "0.1.1") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "0lhx1jdv5dz03pqwk9a62n9p4zfdg2c1m75qzbga7b1g9r71syn5")))

(define-public crate-iso20022-0.1.2 (c (n "iso20022") (v "0.1.2") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "0p6wsa4gc6ldsk0q05si1z924cgn4yfv1h56534i8klx15xwzi7f")))

(define-public crate-iso20022-0.1.3 (c (n "iso20022") (v "0.1.3") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "0s61hinc6d4bclqdk2lv3s5bp285s7cbspn4spvdgp7ym1l266ni")))

(define-public crate-iso20022-0.1.4 (c (n "iso20022") (v "0.1.4") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "0zixk8h82fmjcky6w56rarwcybczb67j1dwg831rz48cp1csqwn6")))

(define-public crate-iso20022-0.1.5 (c (n "iso20022") (v "0.1.5") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "1ang3vwqbvyjax7kpj14baq7ddxc6pp55fzml0jyyqcm6x1v71q3")))

(define-public crate-iso20022-0.1.6 (c (n "iso20022") (v "0.1.6") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "12k4x9pvjj7cpqha6vqsrl2vhl62ynq4iz78nrzf7aqfrsm13c3w")))

(define-public crate-iso20022-0.1.7 (c (n "iso20022") (v "0.1.7") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "08m2q6q0hn4h585fcy8avb7iz4jmzqfws45vg6gdq2xfkq9jrhcc")))

(define-public crate-iso20022-0.1.8 (c (n "iso20022") (v "0.1.8") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "1z97w1vd0fvwa7pkh128iw3w570di0v5jkvxy0lmi7nn3lvc8a6l")))

(define-public crate-iso20022-0.1.9 (c (n "iso20022") (v "0.1.9") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "0ws212pv6cpl5dx03wg56ib58zb1nbphr9ds7nl9y6y8ahkxq1k2")))

