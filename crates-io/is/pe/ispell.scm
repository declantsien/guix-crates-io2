(define-module (crates-io is pe ispell) #:use-module (crates-io))

(define-public crate-ispell-0.1.0 (c (n "ispell") (v "0.1.0") (h "1qhj409clwwwag6mqdgqv9wv4iidsnfa6l0q3hlff9w414bjb12d")))

(define-public crate-ispell-0.2.0 (c (n "ispell") (v "0.2.0") (h "02b2055x6j1a5x4aqpj9am7vlir15nkfph5kggd03rc69nbz56bc")))

(define-public crate-ispell-0.3.0 (c (n "ispell") (v "0.3.0") (h "0cakxyglk4wd1v2rflcxb8wp8lppcz9cpygmr7y3f0pikhjnnzj4")))

(define-public crate-ispell-0.3.1 (c (n "ispell") (v "0.3.1") (h "1npj0pdmhn8rx9k1nykg4bk61j91w7921g0qs90jkqhdvd9wbfhw")))

