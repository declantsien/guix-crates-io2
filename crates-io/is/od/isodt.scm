(define-module (crates-io is od isodt) #:use-module (crates-io))

(define-public crate-isodt-0.1.0 (c (n "isodt") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0zqzd96rig3flnbmja4y3x55n8avn0y6h5nyl0n7f6cpm6bwyhpi")))

(define-public crate-isodt-0.1.2 (c (n "isodt") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1885m2f5c8iyzr07kaxzr905cnh6iw7a4s9aad3zky5vd2lhan65")))

(define-public crate-isodt-0.1.3 (c (n "isodt") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1bqsx9b87ig9a3j75z846bhgjn31qf7anjb8idnhisw5wij1yc78")))

