(define-module (crates-io is _s is_slice) #:use-module (crates-io))

(define-public crate-is_slice-0.0.0 (c (n "is_slice") (v "0.0.0") (d (list (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "0xgzyikdjxfynhfprhzc26pb04l5hl635wgf52vyrx3piz6bz0wd")))

(define-public crate-is_slice-0.0.1 (c (n "is_slice") (v "0.0.1") (d (list (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "13by1cs9bxlqas235hhmvwm5mb99z53bhq21rnj2y4j1d5zwbm7w")))

(define-public crate-is_slice-0.0.2 (c (n "is_slice") (v "0.0.2") (d (list (d (n "wtest_basic") (r "~0") (d #t) (k 2)))) (h "0v5x6niz0h9jq8d84ifh11k6xph00inb7317jjij1124f2kkhivj")))

(define-public crate-is_slice-0.1.0 (c (n "is_slice") (v "0.1.0") (d (list (d (n "wtest_basic") (r "~0.1") (d #t) (k 2)))) (h "0x0j6agxffaj7n215sm13f3wznj7dlvg1w5zx7jz1apj5lzq52ki")))

(define-public crate-is_slice-0.1.1 (c (n "is_slice") (v "0.1.1") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0fjmlwzy8an44m8xwbwrhf1308c5jnp0k6pcr7lqf67ifijn0f8h")))

(define-public crate-is_slice-0.2.0 (c (n "is_slice") (v "0.2.0") (d (list (d (n "test_tools") (r "^0.1.5") (d #t) (k 2)))) (h "0w0xmfdppgpq659b05hfc2r0k367jj3ip25rpp1navkh8c77g2v8") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.3.0 (c (n "is_slice") (v "0.3.0") (d (list (d (n "test_tools") (r "^0.1.5") (d #t) (k 2)))) (h "0acj3gl260hqcwddmpcg7vm0gsx746zx72872ij1bszxvwnvzg9b") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.4.0 (c (n "is_slice") (v "0.4.0") (d (list (d (n "test_tools") (r "~0.4.0") (d #t) (k 2)))) (h "0kjgyqzayg20l1nl1v9f7v4hlnszqkb4jh3azhn3gjd0x7n7n5wp") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.5.0 (c (n "is_slice") (v "0.5.0") (d (list (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "04gh1fll79wls5db71hbprizmwcrlinjsc48v4nl3anxzyi9n9nx") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.6.0 (c (n "is_slice") (v "0.6.0") (d (list (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0130609l8627m7x3igj64waavhnpy0wk9f1g7r579x4pr28x2ljr") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.7.0 (c (n "is_slice") (v "0.7.0") (d (list (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "1japcjb8w5chijizzhalfrjn83gczc8jvhxrxrjvmgaaxsrmigjm") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.8.0 (c (n "is_slice") (v "0.8.0") (d (list (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "144cvhjgvvxbg4y6zv6fc152knxb7ykv6cvl9sn59lzcfww3j5r5") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.9.0 (c (n "is_slice") (v "0.9.0") (d (list (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "0pb1mzvhj4ax8qp933qd08zbbg6n4lgaz27rvjxlcw717ky65x8i") (f (quote (("use_alloc" "no_std") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

