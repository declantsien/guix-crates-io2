(define-module (crates-io is _s is_superuser) #:use-module (crates-io))

(define-public crate-is_superuser-1.0.0 (c (n "is_superuser") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("securitybaseapi" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0szfv9h85n4nidrql2y42qhgiinwl6amd076fskgxszb08kv67qk") (y #t)))

(define-public crate-is_superuser-1.0.1 (c (n "is_superuser") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("securitybaseapi" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0r0g2ln3yv0ay38bj3f29rl5s94xjb8av6l08hzvgv8k1rnzbvbi")))

