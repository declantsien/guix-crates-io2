(define-module (crates-io is _s is_sorted) #:use-module (crates-io))

(define-public crate-is_sorted-0.1.0 (c (n "is_sorted") (v "0.1.0") (d (list (d (n "rand") (r "0.4.*") (d #t) (k 2)))) (h "0ffl94lkr7nf5d4d02yif7292wwva9hhjhjyhill9vdynnarvz61") (f (quote (("use_std") ("unstable") ("default" "use_std"))))))

(define-public crate-is_sorted-0.1.1 (c (n "is_sorted") (v "0.1.1") (d (list (d (n "rand") (r "0.4.*") (d #t) (k 2)))) (h "089b75jmic5wgvvywia33pqx1lvf6dfmh03axxr37nrpbi37cwrm") (f (quote (("use_std") ("unstable") ("default" "use_std"))))))

