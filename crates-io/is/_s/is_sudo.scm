(define-module (crates-io is _s is_sudo) #:use-module (crates-io))

(define-public crate-is_sudo-0.0.1 (c (n "is_sudo") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("handleapi" "processthreadsapi" "winnt" "securitybaseapi" "impl-default"))) (d #t) (k 0)))) (h "1fg4km0sf404bv9acycgqq5is7h6lvqr31nqmfqvb3yrbjq5v4q5")))

