(define-module (crates-io is et iset) #:use-module (crates-io))

(define-public crate-iset-0.0.1 (c (n "iset") (v "0.0.1") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1pvg00j47a57i89h54jfbq3arg8x8d9qabgb4bhgzrri1y493jdn")))

(define-public crate-iset-0.0.2 (c (n "iset") (v "0.0.2") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1ncb4223a79bc3v2qrhhvbi7kkl2qpf2d9vr2yskbdbnfqy1sidd")))

(define-public crate-iset-0.0.3 (c (n "iset") (v "0.0.3") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0ji2vfz3ssn53ygim5w947jdzkwykrk5v9p3klmrq2xbby5p8jwj")))

(define-public crate-iset-0.0.4 (c (n "iset") (v "0.0.4") (d (list (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0jj4g85yqc1xyms6cx96qww5gbz2kv9h5xh9f7wng4z9hj529hzh") (f (quote (("dot") ("default" "dot"))))))

(define-public crate-iset-0.0.5 (c (n "iset") (v "0.0.5") (d (list (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11z25fhblky63p3kn24yaii1npwjw22z7k77h97v04k0jg087j62") (f (quote (("dot") ("default" "dot"))))))

(define-public crate-iset-0.1.0 (c (n "iset") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ssp8xr6gn7y65i40bbi65993hmwrfmdi36apbq4lhspjy57hmxh") (f (quote (("dot") ("default" "dot"))))))

(define-public crate-iset-0.1.1 (c (n "iset") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01jgrv766rp79hh7h1rmvbdhhl6nyzir0gh1x6bdazfbjm4sprl3") (f (quote (("dot") ("default" "dot"))))))

(define-public crate-iset-0.2.0 (c (n "iset") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vhai2gby3vfvcirf63m47gaxy04bbalizhrhdi1p9yl2bpbssjr") (f (quote (("std") ("dot" "std") ("default")))) (y #t) (s 2) (e (quote (("serde" "std" "dep:serde"))))))

(define-public crate-iset-0.2.1 (c (n "iset") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ykrm6a2nprqfh3ssrplwsac54jkykammlpv2i7ijglc52l4d0mj") (f (quote (("std") ("dot" "std") ("default")))) (s 2) (e (quote (("serde" "std" "dep:serde"))))))

(define-public crate-iset-0.2.2 (c (n "iset") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0xmcz43xw5kv853sy86rgd9yvxbgbqqpcqj2p4h7pjw0f06nlwfh") (f (quote (("std") ("dot" "std") ("default")))) (s 2) (e (quote (("serde" "std" "dep:serde"))))))

