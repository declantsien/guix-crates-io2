(define-module (crates-io is _m is_main_thread) #:use-module (crates-io))

(define-public crate-is_main_thread-0.1.0 (c (n "is_main_thread") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.64") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"dragonfly\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)) (d (n "objc") (r "^0.2.3") (d #t) (t "cfg(target_os = \"ios\")") (k 0)) (d (n "objc") (r "^0.2.6") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("processthreadsapi" "minwindef"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "04qszm3ab0xhb4h9iwq75ndgirw0jkdrvdagxzjcwgvnp4hhq1nw")))

