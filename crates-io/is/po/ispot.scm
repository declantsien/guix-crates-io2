(define-module (crates-io is po ispot) #:use-module (crates-io))

(define-public crate-ispot-0.2.0 (c (n "ispot") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "plist") (r "^0.4.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rspotify") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vpfrrmcc2zkhl0696l5shzzpskgqqkpf1zjazl44d3aqb22kyyw")))

(define-public crate-ispot-0.2.1 (c (n "ispot") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "plist") (r "^0.4.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rspotify") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jdjd2ky5qqq72g5r4431v0w34a0cfj7033cggrsh478lyy1wwva")))

(define-public crate-ispot-0.3.0 (c (n "ispot") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "plist") (r "^0.4.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rspotify") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1llwy1w62aqzav3nyzldmpbp7bgznd0ng4vckp13hxbgv1rlvf13")))

