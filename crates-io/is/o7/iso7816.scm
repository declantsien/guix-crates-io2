(define-module (crates-io is o7 iso7816) #:use-module (crates-io))

(define-public crate-iso7816-0.0.0-unreleased (c (n "iso7816") (v "0.0.0-unreleased") (h "07kmxk4r1mx4swi4q01vs758gfzwvwlqyfqm588m95sx1njw3dyq")))

(define-public crate-iso7816-0.1.0-alpha.0 (c (n "iso7816") (v "0.1.0-alpha.0") (d (list (d (n "heapless") (r "^0.6") (d #t) (k 0)) (d (n "heapless-bytes") (r "^0.2.0") (d #t) (k 0)))) (h "1vvni8xv4gb5vchqazfaw2zvp9i0b0w2yf5ki5vc31i7apk821rw")))

(define-public crate-iso7816-0.1.0-alpha.1 (c (n "iso7816") (v "0.1.0-alpha.1") (d (list (d (n "delog") (r "^0.1.2") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "05d3pzpirbl9zaci3ydwhl9bpc1bvah6nlgy32rwwfgmpb71mb6m")))

(define-public crate-iso7816-0.1.0 (c (n "iso7816") (v "0.1.0") (d (list (d (n "delog") (r "^0.1.2") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "07f7zz7w79r9mhkw7qvsynqls1lkh7nxx5k9pv45sfqnyqpr88d9")))

(define-public crate-iso7816-0.1.1 (c (n "iso7816") (v "0.1.1") (d (list (d (n "delog") (r "^0.1.2") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "0lpnwfn63hvfwywhvv5q8agybyzwpf8cx5amhsvlk4sh7msarrp7")))

(define-public crate-iso7816-0.1.2 (c (n "iso7816") (v "0.1.2") (d (list (d (n "delog") (r "^0.1.2") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "heapless-bytes") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "036j4xdy56ii1szkr4jwnixrxpsww4c22lw06bm7l7l2kjn77bxk") (f (quote (("std"))))))

