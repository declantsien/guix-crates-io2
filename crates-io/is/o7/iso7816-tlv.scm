(define-module (crates-io is o7 iso7816-tlv) #:use-module (crates-io))

(define-public crate-iso7816-tlv-0.1.0 (c (n "iso7816-tlv") (v "0.1.0") (d (list (d (n "untrusted") (r "^0.7.0") (d #t) (k 0)))) (h "0mpcvz06b1pqds8pk4cf671pzvnmj8c8d1kcrddaf067dgi2jsnx")))

(define-public crate-iso7816-tlv-0.1.1 (c (n "iso7816-tlv") (v "0.1.1") (d (list (d (n "untrusted") (r "^0.7.0") (d #t) (k 0)))) (h "0vsbcsd7q2x9laprqg52a8853y4kc70a9vfdzfzra162m3ckhn6i")))

(define-public crate-iso7816-tlv-0.2.0 (c (n "iso7816-tlv") (v "0.2.0") (d (list (d (n "untrusted") (r "^0.7.0") (d #t) (k 0)))) (h "1ap8byibp95r3ch5vd0gh6dsgznx8ragivglfhrf3phpsrp5vm5b")))

(define-public crate-iso7816-tlv-0.2.1 (c (n "iso7816-tlv") (v "0.2.1") (d (list (d (n "untrusted") (r "^0.7.0") (d #t) (k 0)))) (h "0d0mfsydzkh1ywq2zv8dnaqkmdrwnhpdcqgz4134x4sfyz2wny5f")))

(define-public crate-iso7816-tlv-0.3.0 (c (n "iso7816-tlv") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "untrusted") (r "^0.7.0") (d #t) (k 0)))) (h "183kb49jlnr6himh5dd5bw50zd3pwd1wjbkm4f19m5448bkha3bq")))

(define-public crate-iso7816-tlv-0.4.0 (c (n "iso7816-tlv") (v "0.4.0") (d (list (d (n "rand_core") (r "^0.5.1") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2.1") (d #t) (k 2)) (d (n "untrusted") (r "^0.7.0") (d #t) (k 0)))) (h "034fyw5d77i54axyd418a3pk3jiwai0cbfr0ilqbrzz7qnpjzw9j")))

(define-public crate-iso7816-tlv-0.4.1 (c (n "iso7816-tlv") (v "0.4.1") (d (list (d (n "rand_core") (r "^0.5.1") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2.1") (d #t) (k 2)) (d (n "untrusted") (r "^0.7.0") (d #t) (k 0)))) (h "0am547d5lym9n753lfnmn6p6k220wmjwcnc0yxaq72jw64ckcbw0")))

(define-public crate-iso7816-tlv-0.4.2 (c (n "iso7816-tlv") (v "0.4.2") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2.1") (d #t) (k 2)) (d (n "untrusted") (r "^0.9") (d #t) (k 0)))) (h "10ifspyqw8d7rxpj0n01rr5y702q2mj2i9zlpiph3d9ywq58wp9r")))

(define-public crate-iso7816-tlv-0.4.3 (c (n "iso7816-tlv") (v "0.4.3") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2.1") (d #t) (k 2)) (d (n "untrusted") (r "^0.9") (d #t) (k 0)))) (h "0sh18nxzj5nxsbnkpgr2bjpf1978yz04ai1qlgw55hmlqgpnawyl")))

(define-public crate-iso7816-tlv-0.4.4 (c (n "iso7816-tlv") (v "0.4.4") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2.1") (d #t) (k 2)) (d (n "untrusted") (r "^0.9") (d #t) (k 0)))) (h "0immb18gxx8sycf4k3ks2qxhz8sl8ra5s9wa4a8dccd84j6x4q3n")))

