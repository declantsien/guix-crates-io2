(define-module (crates-io is o7 iso7816_tx) #:use-module (crates-io))

(define-public crate-iso7816_tx-0.1.0 (c (n "iso7816_tx") (v "0.1.0") (d (list (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "0iv9h558md462m628dbz0gz7mgkwx8zy877chbb7yyi6ka0pf7ka")))

(define-public crate-iso7816_tx-0.1.1 (c (n "iso7816_tx") (v "0.1.1") (d (list (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "0hn64wydg82hiwilnw27izbyzbbzfxa7h4s3sd51qpbdi8b5hd4h")))

(define-public crate-iso7816_tx-0.1.2 (c (n "iso7816_tx") (v "0.1.2") (d (list (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "1acx9d9nskcq61hji2fmi3jjrdym4dybplx7fbvwnlb0yp6wrabw")))

