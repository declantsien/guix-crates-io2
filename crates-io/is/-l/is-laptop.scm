(define-module (crates-io is -l is-laptop) #:use-module (crates-io))

(define-public crate-is-laptop-0.1.0 (c (n "is-laptop") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wmi") (r "^0.9") (d #t) (k 0)))) (h "1aj9aw8la0w7l3fa5bw0igc36f1p6f554d4vnlrys4d5s5xmwb7z")))

(define-public crate-is-laptop-0.2.0 (c (n "is-laptop") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "wmi") (r "^0.9") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1qxzzaz9b2cf280avny9lgp80japr1prjl1xcnn46kx3kplf09q5")))

(define-public crate-is-laptop-0.3.0 (c (n "is-laptop") (v "0.3.0") (d (list (d (n "regex") (r "^1.0.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "windows") (r "^0.37.0") (f (quote ("alloc" "Win32_Foundation" "Win32_System_Com" "Win32_System_Wmi" "Win32_Security"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0v10n3f5hw3kvczqdcl4lr8xnf8i3ad980qs27ppa5gvh05r9s1a")))

(define-public crate-is-laptop-0.4.0 (c (n "is-laptop") (v "0.4.0") (d (list (d (n "regex") (r "^1.0.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "windows") (r "^0.37.0") (f (quote ("alloc" "Win32_Foundation" "Win32_System_Com" "Win32_System_Wmi" "Win32_Security"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1asi4b8q9fnra88i2zd9crw15klpbjqgplww98q19x8ccdy474cd")))

(define-public crate-is-laptop-0.4.1 (c (n "is-laptop") (v "0.4.1") (d (list (d (n "regex") (r "^1.0.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "windows") (r "^0.44.0") (f (quote ("Win32_Foundation" "Win32_System_Com" "Win32_System_Wmi" "Win32_Security"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1mw9pawxjaw3kr6gq6pnf7f3lp5lv49ch0f2b39fms1128j2l6gv")))

(define-public crate-is-laptop-0.4.2 (c (n "is-laptop") (v "0.4.2") (d (list (d (n "regex") (r "^1.0.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Com" "Win32_System_Wmi" "Win32_Security"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1gp2jbsz8sw4jsiph01a0vfb86j5sv7kxklspbi2lygw4d1bd6dy")))

(define-public crate-is-laptop-0.4.3 (c (n "is-laptop") (v "0.4.3") (d (list (d (n "regex") (r "^1.0.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Com" "Win32_System_Wmi" "Win32_Security"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0lpb3bkkf9wcqrjbphgkk8fd8ac6v32mvbvqvyp9cllmighwa56a")))

