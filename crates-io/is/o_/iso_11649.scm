(define-module (crates-io is o_ iso_11649) #:use-module (crates-io))

(define-public crate-iso_11649-0.1.1 (c (n "iso_11649") (v "0.1.1") (h "14l2a679afh8hy3rpalj4fdapjb312g18kl62rhjafn20nr1rplq") (r "1.56")))

(define-public crate-iso_11649-0.1.2 (c (n "iso_11649") (v "0.1.2") (h "1gw0jl5a0xhm8zigsnnrw5fnipf37ln9nhw046afm81fvr77rlb7") (r "1.56")))

