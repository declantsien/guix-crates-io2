(define-module (crates-io is o_ iso_iec_7064) #:use-module (crates-io))

(define-public crate-iso_iec_7064-0.1.0 (c (n "iso_iec_7064") (v "0.1.0") (h "19j3c13xrz0jykys959k5qjblbabgjndpq3pm0ygk2wp5h65ng88")))

(define-public crate-iso_iec_7064-0.1.1 (c (n "iso_iec_7064") (v "0.1.1") (h "1xbgxm3l30v42qd034y4wjkw1wgnb2j6k1qvm4l6zcs3z8pfh35m")))

