(define-module (crates-io is o_ iso_currency) #:use-module (crates-io))

(define-public crate-iso_currency-0.1.0 (c (n "iso_currency") (v "0.1.0") (h "15kh976bczh4n0y6h89yfv89cgbqzr62pk2135mvd9y1sz9w8nsx")))

(define-public crate-iso_currency-0.2.0 (c (n "iso_currency") (v "0.2.0") (h "01sq07jv9c64d027pijlbn31phxaa1dvws3vbaz0834af04cv1vm")))

(define-public crate-iso_currency-0.2.1 (c (n "iso_currency") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.107") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 2)))) (h "1zys7n8xm80yva20kri6540rz9fcwjzbh7ykl0grm5dgwmxqyk3a") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.3.0 (c (n "iso_currency") (v "0.3.0") (d (list (d (n "iso_country") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.107") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 2)))) (h "03jpm4a08ssgvznp3afakpjdkh7clhf1wlg6xsxx7db0h0jcxpn9") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.3.1 (c (n "iso_currency") (v "0.3.1") (d (list (d (n "iso_country") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.107") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 2)))) (h "0dbnwra8lpmdk9vjn7cpyfz7sd2clv8v1zjvpivk1ljb0a6f179y") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.3.2 (c (n "iso_currency") (v "0.3.2") (d (list (d (n "iso_country") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.107") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 2)))) (h "0zly9cldwimk4h7m5dy79j145532y419f613hxqvifznm8i4hz13") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.4.0 (c (n "iso_currency") (v "0.4.0") (d (list (d (n "iso_country") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.107") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 2)))) (h "06x1f2ghcnqg638h08ip2d6gir7svhhnqvr0c65hpps8vfy1n0js") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.4.1 (c (n "iso_currency") (v "0.4.1") (d (list (d (n "iso_country") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.107") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 2)))) (h "0z7vrkm0njwg2vq64ik6jvi6vx6xp9wv3ca4062a5fxfagyy4597") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.4.2 (c (n "iso_currency") (v "0.4.2") (d (list (d (n "iso_country") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.107") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 2)))) (h "0sn9ll7mbkjd58pjjiyr5wxr8vagi8qa8xkjjfl2jdyyk9lr0jm8") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.4.3 (c (n "iso_currency") (v "0.4.3") (d (list (d (n "iso_country") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)))) (h "15p0yr7cky50djgcxkg8l49kvi9cpyagfhg1d1is9l1dvksqdkcq") (f (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.4.4 (c (n "iso_currency") (v "0.4.4") (d (list (d (n "iso_country") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06rwxjd5mv8hbsvivvq8imz8mk9a7pscmx3wl13j7j4mps0p3w1k") (f (quote (("with-serde" "serde") ("iterator" "strum") ("default"))))))

