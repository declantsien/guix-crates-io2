(define-module (crates-io is o_ iso_country) #:use-module (crates-io))

(define-public crate-iso_country-0.1.0 (c (n "iso_country") (v "0.1.0") (h "1kk5hxw00smbvnv608ibpj53rrgwh5xd0zl733cnijpfcg2lgfsv")))

(define-public crate-iso_country-0.1.1 (c (n "iso_country") (v "0.1.1") (h "1jdr0snd499zlqza6m6bm6w1fw4ilgbw4slz6zzjn1amqmflm8b3")))

(define-public crate-iso_country-0.1.2 (c (n "iso_country") (v "0.1.2") (h "04iq0fmy5iqf3pv6pw299lxdpf9srrs3ba71kz2m6kx71xbf48wg")))

(define-public crate-iso_country-0.1.3 (c (n "iso_country") (v "0.1.3") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "14ypkgfynmmjhf3i0fq7lhgyf9cv4dyapzdry5jfz6bik8h3w4mn")))

(define-public crate-iso_country-0.1.4 (c (n "iso_country") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0kb251nf4zd97xsg1s378ap5szr4xh4xn7w66rrylj1rimw3wqr0")))

