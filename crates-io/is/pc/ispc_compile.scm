(define-module (crates-io is pc ispc_compile) #:use-module (crates-io))

(define-public crate-ispc_compile-1.0.0 (c (n "ispc_compile") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.47") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1m59d15fhxxnsb4j41nzwagdgc5wnyc0g6dl666j29b6wx6hyxs7")))

(define-public crate-ispc_compile-1.0.1 (c (n "ispc_compile") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.47") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1bfifcimkwib7q15vjyvvckpq7g9w7wjcg0b0b606z6m7jc3sbn1")))

(define-public crate-ispc_compile-1.0.2 (c (n "ispc_compile") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.47") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "14fy6k59b4br61h5y1djzswcngy1xy8r8cghx71fx6fmiimx33jr")))

(define-public crate-ispc_compile-1.0.3 (c (n "ispc_compile") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.46.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.47") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1qdp6h6n42xxalyv01f998hq69ah08rj99b2jkz97n56352d3n6l")))

(define-public crate-ispc_compile-1.0.4 (c (n "ispc_compile") (v "1.0.4") (d (list (d (n "bindgen") (r "^0.46.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.47") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0l6xhv0r6xfqs7pqpzw3nh6vfhh4alm7vjipi6r19fdhrv6jc5s6")))

(define-public crate-ispc_compile-1.0.5 (c (n "ispc_compile") (v "1.0.5") (d (list (d (n "bindgen") (r "^0.46.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.47") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0pp7czigpq6r76x7hs4c17lyil92vzipizg35s91mck8pww64kpx")))

(define-public crate-ispc_compile-1.0.6 (c (n "ispc_compile") (v "1.0.6") (d (list (d (n "bindgen") (r "^0.46.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.47") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1lks9hf0pndjgrsdzgp86d5h08fbsahzjwzyn7rd348shw5s7hih")))

(define-public crate-ispc_compile-1.0.7 (c (n "ispc_compile") (v "1.0.7") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1rcihdffgxak8hrzwvwv2a6jcy3ncz5y5jr4bzpbj473sy7h0wqw")))

(define-public crate-ispc_compile-1.0.8 (c (n "ispc_compile") (v "1.0.8") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "06lwyhvxl8n5bjkk0hjazs2c50hvia7418f41kkvgdcjn71jb0dj")))

(define-public crate-ispc_compile-1.0.9 (c (n "ispc_compile") (v "1.0.9") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "00y0s9nljgvfps1lpg43bkvlp866fyv81g7p2s2yvwp42vhcygr0")))

(define-public crate-ispc_compile-1.0.10 (c (n "ispc_compile") (v "1.0.10") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.5") (d #t) (k 0)))) (h "059aggf8k353n2c8hbb00zzy66nw3v8qh8q3lldpx3q9w72zm44l")))

(define-public crate-ispc_compile-1.0.11 (c (n "ispc_compile") (v "1.0.11") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.5") (d #t) (k 0)))) (h "0jnycc76r2v9xn68ysdyawxqp4kjld9flwbcv1rmh7cnf1mbk8gv")))

(define-public crate-ispc_compile-1.0.12 (c (n "ispc_compile") (v "1.0.12") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.5") (d #t) (k 0)))) (h "03i1pwxq1ak5chx5ljihjiwq2d6aivwrahn6h45hdp8ap4yj8gld")))

(define-public crate-ispc_compile-1.0.13 (c (n "ispc_compile") (v "1.0.13") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.5") (d #t) (k 0)))) (h "169ln8q81yhvsl5sdnhvir0xglk6sff8pg3j3d6nj7aq271kfra6")))

(define-public crate-ispc_compile-1.0.14 (c (n "ispc_compile") (v "1.0.14") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.5") (d #t) (k 0)))) (h "0qg1jinqgp3cib1zggbdb19df4a6qln3c2jzyw1fc89bha9d7015")))

(define-public crate-ispc_compile-1.1.0 (c (n "ispc_compile") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "semver") (r "^1.0.5") (d #t) (k 0)))) (h "1xjwqkfzp91sadwrnqid0xrcv7mk8g4f0y7r1l6w54w4x4p2zr57")))

(define-public crate-ispc_compile-2.0.0 (c (n "ispc_compile") (v "2.0.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.14") (d #t) (k 0)))) (h "08f7jira2922g3im6zx88kdwkc2pagph551v0baiaimyvh5rkm41")))

(define-public crate-ispc_compile-2.0.1 (c (n "ispc_compile") (v "2.0.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.14") (d #t) (k 0)))) (h "1ijyj5z47lq6yvq47af8jir3kc0kl27ikcqbl6npr3f644441cfc")))

