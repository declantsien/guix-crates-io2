(define-module (crates-io is pc ispc_rt) #:use-module (crates-io))

(define-public crate-ispc_rt-1.0.0 (c (n "ispc_rt") (v "1.0.0") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.47") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)))) (h "1rw3531y21fw1wl77j8n1azmm6d318v7ikgfkgwbc39xpdvfv188")))

(define-public crate-ispc_rt-1.0.1 (c (n "ispc_rt") (v "1.0.1") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.47") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)))) (h "07500rbsj92mhr7y77kaa5mvgfc7fsjiy79nvf5b3i7yx8kadaqp")))

(define-public crate-ispc_rt-1.0.2 (c (n "ispc_rt") (v "1.0.2") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.47") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)))) (h "1iicmrzliq39kwpcpai7adk9msayyi1iq0wyqd52x2g7f3d4d8xm")))

(define-public crate-ispc_rt-1.0.3 (c (n "ispc_rt") (v "1.0.3") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10") (d #t) (k 0)))) (h "1yrn21lvlhd0i6mralh43rfnyr7b643d5jp3kwxky9iq7pyaf30i")))

(define-public crate-ispc_rt-1.0.4 (c (n "ispc_rt") (v "1.0.4") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11") (d #t) (k 0)))) (h "0l21bmxdbnkk0zky75k9gv9a4sq693slqa04442p3s6s3gpzz7k6")))

(define-public crate-ispc_rt-1.0.5 (c (n "ispc_rt") (v "1.0.5") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0lqhysb211103jcx0g6229mzqq8if3q1qnz65c9xp04563vci57l")))

(define-public crate-ispc_rt-1.0.6 (c (n "ispc_rt") (v "1.0.6") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1bnpr3g2n8ni7arfvb44rym85plr3dlygz3fzic6gk4zfpj8s0kg")))

(define-public crate-ispc_rt-1.0.7 (c (n "ispc_rt") (v "1.0.7") (d (list (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1fmam5wx4ll174c4brxzx86pd3dyjkk5znvbxz3mx9lhbbw5f6mp")))

(define-public crate-ispc_rt-1.1.0 (c (n "ispc_rt") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0gi1fjiwk8bscsn9m0hfyhdn0xgqjapjkjbzyd3r2kmdg5dmchm7")))

(define-public crate-ispc_rt-2.0.0 (c (n "ispc_rt") (v "2.0.0") (d (list (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)))) (h "1phiibq6fbk214cs92x84z4iw8cd25k9zdk8cfdi6s58dx0200ky")))

(define-public crate-ispc_rt-2.0.1 (c (n "ispc_rt") (v "2.0.1") (d (list (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)))) (h "0y75rbay3hrdrxf6zlafa780qhi2y3gmds727rnf4vzxvl40xly8")))

