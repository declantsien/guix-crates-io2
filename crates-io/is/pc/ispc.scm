(define-module (crates-io is pc ispc) #:use-module (crates-io))

(define-public crate-ispc-0.0.1 (c (n "ispc") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.16.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 0)))) (h "0zqbngpik7nx3xnw5qz9ywy53nar302r9j9cy1sy6kv3sfxscpwi")))

(define-public crate-ispc-0.1.0 (c (n "ispc") (v "0.1.0") (d (list (d (n "aligned_alloc") (r "^0.1.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.16.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.28") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.12") (d #t) (k 0)))) (h "15ldgkflcs6543lxsp26nll5glrrvap8d7vyrj586vv7jwpdww7b")))

(define-public crate-ispc-0.1.1 (c (n "ispc") (v "0.1.1") (d (list (d (n "aligned_alloc") (r "^0.1.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.17.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.28") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.12") (d #t) (k 0)))) (h "167iq6gq780w9pxl8w8ds17g5ygazgyf1cjww83r92ynibhvjciq")))

(define-public crate-ispc-0.2.0 (c (n "ispc") (v "0.2.0") (d (list (d (n "aligned_alloc") (r "^0.1.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.17.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.28") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.12") (d #t) (k 0)))) (h "0b2rwhkw1ilw7kzy701dg03k2blrlr4bymr6b775pm7fzw0wcrmx")))

(define-public crate-ispc-0.2.1 (c (n "ispc") (v "0.2.1") (d (list (d (n "aligned_alloc") (r "^0.1.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.17.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.76") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3.28") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.13") (d #t) (k 0)))) (h "12djci8v71g7d9iabvz1br4wpxbr7p6b67pnrjmnal4cdsx8akjq") (f (quote (("unstable" "clippy"))))))

(define-public crate-ispc-0.3.0 (c (n "ispc") (v "0.3.0") (d (list (d (n "aligned_alloc") (r "^0.1.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.18.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.79") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3.31") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.13") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)) (d (n "semver") (r "^0.2.3") (d #t) (k 0)))) (h "1nyn4lhnjdn3ryj5shw8bf7zr96wmwavkbwdzrdx8jfh8ykivgb8") (f (quote (("unstable" "clippy"))))))

(define-public crate-ispc-0.3.1 (c (n "ispc") (v "0.3.1") (d (list (d (n "aligned_alloc") (r "^0.1.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.19.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.87") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3.35") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)) (d (n "semver") (r "^0.4.0") (d #t) (k 0)))) (h "02aja3yp01gnjhqpsl7mdr86a5hd5dx3hlrsl9v98fkqm7zmdq43") (f (quote (("unstable" "clippy"))))))

(define-public crate-ispc-0.3.2 (c (n "ispc") (v "0.3.2") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.25.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.134") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3.46") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "num_cpus") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "semver") (r "^0.7.0") (d #t) (k 0)))) (h "0chvcsyhfqlilay013867vhfndg1dpy2g14rfjkazj2x7y7g8vfk") (f (quote (("unstable" "clippy"))))))

(define-public crate-ispc-0.3.3 (c (n "ispc") (v "0.3.3") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.30.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.161") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3.54") (d #t) (k 0)) (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "semver") (r "^0.8.0") (d #t) (k 0)))) (h "0ym8x9jzrlgmm6yzaj99cdnkcpz9fm837zqwkbr0lxv5szf8xdzn") (f (quote (("unstable" "clippy"))))))

(define-public crate-ispc-0.3.4 (c (n "ispc") (v "0.3.4") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.37.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.206") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3.54") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0zcmimiqqx54kxyyk7npakik2rglw648gxy5brlrgasavp7781br") (f (quote (("unstable" "clippy"))))))

(define-public crate-ispc-0.3.5 (c (n "ispc") (v "0.3.5") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.37.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.206") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3.54") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "10n8qbmckcklvqf86wk2x12ixiiqpn8cy79jivqqnkjhw6cky45s") (f (quote (("unstable" "clippy"))))))

(define-public crate-ispc-0.3.6 (c (n "ispc") (v "0.3.6") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.38") (d #t) (k 0)) (d (n "clippy") (r "^0.0.212") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3.54") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1ra23jjdy08kij4gwp5y6b4yz8k4pszj34znl8jd28hrpd6qkmqj") (f (quote (("unstable" "clippy"))))))

(define-public crate-ispc-0.3.7 (c (n "ispc") (v "0.3.7") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.38") (d #t) (k 0)) (d (n "clippy") (r "^0.0.212") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3.54") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1jcissyykdp04shm4nvv04ahcnlpbd2pil3m8f8sdr5bgwqmjv2d") (f (quote (("unstable" "clippy"))))))

(define-public crate-ispc-0.3.8 (c (n "ispc") (v "0.3.8") (d (list (d (n "aligned_alloc") (r "^0.1.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.43") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3.55") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0ir318wpvsf2sgsy2z5axngrk85fnr5k6h6mcdxlisxmggw6dq08") (f (quote (("unstable" "clippy"))))))

(define-public crate-ispc-1.0.0 (c (n "ispc") (v "1.0.0") (d (list (d (n "ispc_compile") (r "^1.0.0") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.0") (d #t) (k 0)))) (h "1rhjn2lz1yj2l67vajg14h399a6prqb5l5gr7x2wlfbgavaknhj6")))

(define-public crate-ispc-1.0.1 (c (n "ispc") (v "1.0.1") (d (list (d (n "ispc_compile") (r "^1.0.1") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.1") (d #t) (k 0)))) (h "1krml5bbv6b44ak9vlkcy4hc9c64hbdcsl6f9042iyk38kdz16av")))

(define-public crate-ispc-1.0.2 (c (n "ispc") (v "1.0.2") (d (list (d (n "ispc_compile") (r "^1.0.2") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.1") (d #t) (k 0)))) (h "0w1clqn4ir0hjknlidy542kx4y2wf1dxddpyz0kcwhi2lmj6pich")))

(define-public crate-ispc-1.0.3 (c (n "ispc") (v "1.0.3") (d (list (d (n "ispc_compile") (r "^1.0.3") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.1") (d #t) (k 0)))) (h "0ipja0f3mkmdchw12x9hhh230gh91y5gn2w3vzixry3ya0bizljy")))

(define-public crate-ispc-1.0.4 (c (n "ispc") (v "1.0.4") (d (list (d (n "ispc_compile") (r "^1.0.4") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.2") (d #t) (k 0)))) (h "09mgz5alshn6ksqyp9zczj6pna4psb06cznw7gymvfw0ig79ylbj")))

(define-public crate-ispc-1.0.5 (c (n "ispc") (v "1.0.5") (d (list (d (n "ispc_compile") (r "^1.0.5") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.2") (d #t) (k 0)))) (h "1jrgfj15g1dqsnshsxgvzqgl8042rswi2r4ahp5g171nyiwsajyp")))

(define-public crate-ispc-1.0.6 (c (n "ispc") (v "1.0.6") (d (list (d (n "ispc_compile") (r "^1.0.6") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.2") (d #t) (k 0)))) (h "0fjahyiv0krqvpd14sycmcjayi9hay1gf2qc9fmcjd4xsv9qq5yl")))

(define-public crate-ispc-1.0.7 (c (n "ispc") (v "1.0.7") (d (list (d (n "ispc_compile") (r "^1.0.7") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.3") (d #t) (k 0)))) (h "1kdfnvllybszry4k0vpfwjqz2x3ssnpni0r3qmh127jy1hcgfcmw")))

(define-public crate-ispc-1.0.8 (c (n "ispc") (v "1.0.8") (d (list (d (n "ispc_compile") (r "^1.0.8") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.4") (d #t) (k 0)))) (h "03hfm7df2jaq82rzyzcjs0sv5dsmmd701nm0bk8sg1lqw8bblvvi")))

(define-public crate-ispc-1.0.9 (c (n "ispc") (v "1.0.9") (d (list (d (n "ispc_compile") (r "^1.0.9") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.4") (d #t) (k 0)))) (h "0zd9gima95p92jcmplx1zf9i3vaqmwgiq0pz5f3nr5mz4m8bsy6v")))

(define-public crate-ispc-1.0.10 (c (n "ispc") (v "1.0.10") (d (list (d (n "ispc_compile") (r "^1.0.11") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.5") (d #t) (k 0)))) (h "0snmmpwymhxxc48xyqf9f4j7625n94k4p24hz6n178x6n7ms31r5")))

(define-public crate-ispc-1.0.11 (c (n "ispc") (v "1.0.11") (d (list (d (n "ispc_compile") (r "^1.0.12") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.6") (d #t) (k 0)))) (h "142a2j6vncv2ah6pwxz3r1ah3md0s7ya5n0qfrpawnfjln1alzs6")))

(define-public crate-ispc-1.0.12 (c (n "ispc") (v "1.0.12") (d (list (d (n "ispc_compile") (r "^1.0.13") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.6") (d #t) (k 0)))) (h "1xqs45dksmhbl4p0ciah8jhixi8r4b4khwx6gg0s1v6h36miy6ih")))

(define-public crate-ispc-1.0.13 (c (n "ispc") (v "1.0.13") (d (list (d (n "ispc_compile") (r "^1.0.13") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.7") (d #t) (k 0)))) (h "0rhdsmgcvfy4nih97x8bf7qcqysvza7jmivi35dp3vc5xibnadrg")))

(define-public crate-ispc-1.0.14 (c (n "ispc") (v "1.0.14") (d (list (d (n "ispc_compile") (r "^1.0.14") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.7") (d #t) (k 0)))) (h "080q4yzkzbdn5dma6znw7m29p8zzfp1bmxp9b3bdvskj6w7380n3")))

(define-public crate-ispc-1.1.0 (c (n "ispc") (v "1.1.0") (d (list (d (n "ispc_compile") (r "^1.1.0") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.1.0") (d #t) (k 0)))) (h "19jq4iir8zc1bjgfag1bwwp2hr3ya6d7qpyzmgxy4zpvansln81r")))

(define-public crate-ispc-2.0.0 (c (n "ispc") (v "2.0.0") (d (list (d (n "ispc_compile") (r "^2.0") (d #t) (k 0)) (d (n "ispc_rt") (r "^2.0") (d #t) (k 0)))) (h "0pf11rpigiciv8zl0vdj1a16nzp2ds87gnhcnc7s1bjaxi0invmc")))

(define-public crate-ispc-2.0.1 (c (n "ispc") (v "2.0.1") (d (list (d (n "ispc_compile") (r "^2.0.1") (d #t) (k 0)) (d (n "ispc_rt") (r "^2.0.1") (d #t) (k 0)))) (h "0l2p6akgccq4ikk2ibhmwmj1mgn1pibdx524d73jxq8gnn1i49y8")))

