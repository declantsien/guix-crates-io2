(define-module (crates-io is pc ispc-tex) #:use-module (crates-io))

(define-public crate-ispc-tex-0.1.0 (c (n "ispc-tex") (v "0.1.0") (d (list (d (n "ddsfile") (r "^0.2.3") (d #t) (k 2)) (d (n "image") (r "^0.21.0") (d #t) (k 2)) (d (n "ispc_compile") (r "^1.0.12") (o #t) (d #t) (k 1)) (d (n "ispc_rt") (r "^1.0.6") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.6") (d #t) (k 1)))) (h "1c07nwv0hdn2vw7asr43b8rwjhz4i9fp3vbm15wx96m2abhc9da9") (f (quote (("ispc" "ispc_compile"))))))

