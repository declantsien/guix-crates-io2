(define-module (crates-io is ow isow) #:use-module (crates-io))

(define-public crate-isow-0.2.5 (c (n "isow") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "self_update") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0r04h4x5ib8fj7xkw1c3qhdzkf1sayl2yhp0rskgbmmyyck853yh") (f (quote (("updater" "self_update") ("default" "updater")))) (y #t)))

(define-public crate-isow-0.2.6 (c (n "isow") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "self_update") (r "^0.8") (o #t) (d #t) (k 0)))) (h "17z8da6cxjwnh7lbxxp2n66w6wf363pdgxlmhk9v8jlqhj1lxylg") (f (quote (("updater" "self_update") ("default" "updater")))) (y #t)))

(define-public crate-isow-0.2.7 (c (n "isow") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "self_update") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0qhdqifhz8c48nq7mapzzfvphrxz14w3ncbm5wa6m5wgs57ax95g") (f (quote (("updater" "self_update"))))))

(define-public crate-isow-0.2.11 (c (n "isow") (v "0.2.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "isocal") (r "^0.1") (d #t) (k 0)) (d (n "rbtag") (r "^0.3") (d #t) (k 0)) (d (n "self_update") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0ds5f2vzp3bmfx5z3jr5qg0kjbjb94s9g1sa41pf614dhsjfmiyn") (f (quote (("updater" "self_update"))))))

(define-public crate-isow-0.2.12 (c (n "isow") (v "0.2.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "isocal") (r "^0.1") (d #t) (k 0)) (d (n "rbtag") (r "^0.3") (d #t) (k 0)) (d (n "self_update") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1349px9qd63z2jnzciydb3vd62qlpzdhk4r6w416krvck7h32i7b") (f (quote (("updater" "self_update"))))))

(define-public crate-isow-0.2.13 (c (n "isow") (v "0.2.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "isocal") (r "^0.1") (d #t) (k 0)) (d (n "rbtag") (r "^0.3") (d #t) (k 0)) (d (n "self_update") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0mhp1y2ap3hj9017b4lar1krri3990wv13zq6v8c8swrcm434gmb") (f (quote (("updater" "self_update"))))))

(define-public crate-isow-0.2.14 (c (n "isow") (v "0.2.14") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "isocal") (r "^0.1") (d #t) (k 0)) (d (n "rbtag") (r "^0.3") (d #t) (k 0)) (d (n "self_update") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1wpdnwjyhjl69yhri9vwb3v8jv3la15ncssd31y45pac27zifd6v") (f (quote (("updater" "self_update"))))))

(define-public crate-isow-0.2.15 (c (n "isow") (v "0.2.15") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "isocal") (r "^0.1") (d #t) (k 0)) (d (n "rbtag") (r "^0.3") (d #t) (k 0)) (d (n "self_update") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1fgz5n668lkrrdvg97cyh2zbl5xcj0jl2ssy9bnx1c2n3mfz0283") (f (quote (("updater" "self_update"))))))

(define-public crate-isow-0.3.0 (c (n "isow") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "isocal") (r "^0.1") (d #t) (k 0)) (d (n "rbtag") (r "^0.3") (d #t) (k 0)) (d (n "self_update") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1pyk8v30pvdhch8nf900ffdvcqrdfgj8fj0164hbckk2hpccn0lx") (f (quote (("updater" "self_update"))))))

