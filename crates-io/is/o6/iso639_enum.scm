(define-module (crates-io is o6 iso639_enum) #:use-module (crates-io))

(define-public crate-iso639_enum-0.1.0 (c (n "iso639_enum") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 1)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 1)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)))) (h "079nrfn9khpdbhj847v0xbvchjaljvc9m33h1l6hcqfzprmdca0v")))

(define-public crate-iso639_enum-0.1.1 (c (n "iso639_enum") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 1)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 1)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)))) (h "19imbzhd06jhfk9xk0i1zpg69nm57lac3vj52j335myhcxrn9a5g")))

(define-public crate-iso639_enum-0.2.2 (c (n "iso639_enum") (v "0.2.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 1)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 1)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)))) (h "1sy88w4sy6q3hkb7df4f56lh6n1dadgm9pgdsmcsdhcaa40ih2rr")))

(define-public crate-iso639_enum-0.3.0 (c (n "iso639_enum") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 1)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 1)) (d (n "phf") (r "^0.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)))) (h "0gxg9kak29qnawrnrdx4mzw6nd87kf1q6wrp5imxlh0zs6igz6z2")))

(define-public crate-iso639_enum-0.5.0 (c (n "iso639_enum") (v "0.5.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 1)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 1)) (d (n "phf") (r "^0.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1md0w35fiq3i90ik3cl6pyiq0v09m1m6p656zzf9if3f3wx32zbj")))

(define-public crate-iso639_enum-0.5.1 (c (n "iso639_enum") (v "0.5.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 1)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 1)) (d (n "phf") (r "^0.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sqmvgn4m6xkmg40q4zrwfpszzsh34k9x6c22y4llpw34bgcvn3z")))

(define-public crate-iso639_enum-0.6.0 (c (n "iso639_enum") (v "0.6.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 1)) (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 1)) (d (n "phf") (r "^0.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07xwpsj101ngpd9pgnsqfbz33dlwi1v4374vkinkniizyjkqbd48")))

