(define-module (crates-io is o6 iso639-1) #:use-module (crates-io))

(define-public crate-iso639-1-0.1.0 (c (n "iso639-1") (v "0.1.0") (h "0mpwwcya80rxz53f0mx1y4kykmlpxfz9rkjp17j0fm5hgrzjjpg5")))

(define-public crate-iso639-1-0.1.4 (c (n "iso639-1") (v "0.1.4") (h "1gf90yxv9qdsxynmngcpp3qyp3hv85wsi0qxbilay6wzr89sr257")))

(define-public crate-iso639-1-0.1.5 (c (n "iso639-1") (v "0.1.5") (h "1gkfs4ni0pxm2lf71rk9rplvjijcrnjg9kvrc2ghxw54p027p526")))

(define-public crate-iso639-1-0.1.6 (c (n "iso639-1") (v "0.1.6") (h "0i2zdfldddz2sq6pvkq16jpd3d8f8964iipmkfl9bx1y3rjxcak1")))

(define-public crate-iso639-1-0.1.7 (c (n "iso639-1") (v "0.1.7") (h "0ml9rvyf8p7h9aci8cjqvp2i299is1c9v3mj2s6a244n95aywyh9")))

(define-public crate-iso639-1-0.1.8 (c (n "iso639-1") (v "0.1.8") (h "1iik884qx5l6k13j7vdp3xh5237zvm5kfa97f2hbwr1gwfa2pxrf")))

(define-public crate-iso639-1-0.1.9 (c (n "iso639-1") (v "0.1.9") (h "0gnwj1xfggm66a43hb25r2ybp75jh4m3l3wrff52nknvcpvclvyr")))

(define-public crate-iso639-1-0.1.10 (c (n "iso639-1") (v "0.1.10") (h "03v1jvg40ncd0pas7x91wqgrbikdasr6v82q4i0ngnrpnnsmlb5z")))

(define-public crate-iso639-1-0.1.11 (c (n "iso639-1") (v "0.1.11") (h "1vg9fa2gbdrrvi03ywn3r3ww16s8xlc723xwmkvp3nn8hddcny7y")))

(define-public crate-iso639-1-0.1.12 (c (n "iso639-1") (v "0.1.12") (h "19c7zq5g7rzwinkd4hj5l86g68sjhp84xg39195lgv72v4hf02lj")))

(define-public crate-iso639-1-0.2.0 (c (n "iso639-1") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "1xs7hcy2phwgf36hzvglis1r5yfmsnlr8245cf4mi7nr4mwp58i9")))

(define-public crate-iso639-1-0.2.1 (c (n "iso639-1") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "0rpa5yfzvv79vgwr4070n250r8zsx37fq7ky083i8frcjhscg43k")))

(define-public crate-iso639-1-0.3.0 (c (n "iso639-1") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "17j8dxis9mdymgyjh59vi8jqziyg5nvczf05drz1967881c71pb8")))

(define-public crate-iso639-1-0.4.0 (c (n "iso639-1") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "strum") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (o #t) (d #t) (k 0)))) (h "1971ahpn9hhcdlmwls8167mx99dsbna08241wdw4dcdbb52qv2v1") (s 2) (e (quote (("strum" "dep:strum" "dep:strum_macros"))))))

(define-public crate-iso639-1-0.4.1 (c (n "iso639-1") (v "0.4.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "strum") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (o #t) (d #t) (k 0)))) (h "1dn2h5z48myjplllddrc4q5pb5wy8m01x3h6vvb0lw5jj2zay2hh") (s 2) (e (quote (("strum" "dep:strum" "dep:strum_macros"))))))

