(define-module (crates-io is o6 iso639) #:use-module (crates-io))

(define-public crate-iso639-0.1.0 (c (n "iso639") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 1)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 1)) (d (n "structopt") (r "^0.3.15") (o #t) (d #t) (k 0)))) (h "1rw0776v7akn1dg92kkaf9i7jm53s0f13jahwf36j5k2vg35cqbm")))

