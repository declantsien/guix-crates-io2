(define-module (crates-io is o6 iso639_2) #:use-module (crates-io))

(define-public crate-iso639_2-0.1.2 (c (n "iso639_2") (v "0.1.2") (h "09x7nzrjn1ygp9j8hyqsl5h41rd33g5cs896bzb77nipxav3ds5z")))

(define-public crate-iso639_2-0.1.3 (c (n "iso639_2") (v "0.1.3") (h "119qhy8p2i9d2llh3kzj4g190gyhsrgsf8ryamkcymbd7471ij13")))

(define-public crate-iso639_2-0.1.5 (c (n "iso639_2") (v "0.1.5") (h "0c6gp2150r0n681wyi83psiaa7w5zq887azbhgv40wsfdgm8cg78")))

(define-public crate-iso639_2-0.1.6 (c (n "iso639_2") (v "0.1.6") (h "0gnlj2c53lwbwi2vh47lrmv3h6j47hf0cnldr062vjdzmmdl6cpg")))

(define-public crate-iso639_2-0.1.7 (c (n "iso639_2") (v "0.1.7") (h "0ri2wn7p1bpplrcy333zyp2a26hg2vfi1njmppyyfpplb6g8jrk0")))

(define-public crate-iso639_2-0.1.8 (c (n "iso639_2") (v "0.1.8") (h "199l8f8hz08nmm7j6d7110cacamplf841srsvcn0gn8zw2xiidrl")))

(define-public crate-iso639_2-0.1.9 (c (n "iso639_2") (v "0.1.9") (h "0sw41vh8ibnaii9ndsp0vazhdzh9z9qgpgygrsh87kk4qra0dwwx")))

(define-public crate-iso639_2-0.1.10 (c (n "iso639_2") (v "0.1.10") (h "0s1h398qvk5nv9jbj631hm46vp264s4l2bl5ip0m6bnab8v4iayd")))

(define-public crate-iso639_2-0.1.11 (c (n "iso639_2") (v "0.1.11") (h "0nr34y9ny18mk6yqhv5335yqggr2hd2c2na4kscad4lmiq3dh42j")))

(define-public crate-iso639_2-0.1.12 (c (n "iso639_2") (v "0.1.12") (h "10px50gpb5vrs20785d17122jjsml3f7vs89hp2bm111nz5rcrlw")))

(define-public crate-iso639_2-0.1.13 (c (n "iso639_2") (v "0.1.13") (h "1ww5yvpkq9bd2h87yjgxpl46y5bhpgdgc7jrdicj27vlcff9p60q")))

