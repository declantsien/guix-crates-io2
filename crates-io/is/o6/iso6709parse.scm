(define-module (crates-io is o6 iso6709parse) #:use-module (crates-io))

(define-public crate-iso6709parse-0.1.0 (c (n "iso6709parse") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "latlon") (r "^0.1.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1irsy40di8sg9w18v9k5b1qs738340h3g3wh6qs0gvhi6r4k31ry")))

