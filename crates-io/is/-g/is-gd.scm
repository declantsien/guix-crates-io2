(define-module (crates-io is -g is-gd) #:use-module (crates-io))

(define-public crate-is-gd-0.1.0 (c (n "is-gd") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1q0wygrcrm66dsgy94vrzn66hqn5v1ls24kagz9aqhbasrh96d5p")))

