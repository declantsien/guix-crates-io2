(define-module (crates-io is -g is-glob) #:use-module (crates-io))

(define-public crate-is-glob-0.1.0 (c (n "is-glob") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "16kzsc67aicys0ain31mxim1wiq25mxzavm22a7fz2c9vlryxfkv")))

