(define-module (crates-io is cl isclose) #:use-module (crates-io))

(define-public crate-isclose-0.1.0 (c (n "isclose") (v "0.1.0") (d (list (d (n "euclid") (r "^0.22") (o #t) (k 0)) (d (n "half") (r "^2") (f (quote ("num-traits"))) (o #t) (k 0)) (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0xgdw5amysnmv2619dzf56fvidag2xydgsskra1lxqllfa2m05ar") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "euclid?/std") ("libm" "dep:libm" "euclid?/libm") ("half" "dep:half") ("euclid" "dep:euclid")))) (r "1.60")))

(define-public crate-isclose-0.1.1 (c (n "isclose") (v "0.1.1") (d (list (d (n "euclid") (r "^0.22") (o #t) (k 0)) (d (n "half") (r "^2") (f (quote ("num-traits"))) (o #t) (k 0)) (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "11661cis8nkdjzpznvha6zrnb7zki3r8fq771qdrfcygv06wk3xy") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "euclid?/std") ("libm" "dep:libm" "euclid?/libm") ("half" "dep:half") ("euclid" "dep:euclid")))) (r "1.60")))

