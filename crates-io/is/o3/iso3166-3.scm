(define-module (crates-io is o3 iso3166-3) #:use-module (crates-io))

(define-public crate-iso3166-3-0.1.0 (c (n "iso3166-3") (v "0.1.0") (h "0l1d77m3g0samdz4hcva9y7d3a9fsn0ylng6kdcpgdqa2dl7bbky")))

(define-public crate-iso3166-3-0.2.0 (c (n "iso3166-3") (v "0.2.0") (h "1l4smn2x31zghail0a42b4z3w4a4l6aa4vb58acsnd6lqq9ch4yl")))

(define-public crate-iso3166-3-0.3.0 (c (n "iso3166-3") (v "0.3.0") (h "0v48c6rv8lscrn7qjwbmkqyl7az09hfaa1m4wbafays2j76k11dy")))

