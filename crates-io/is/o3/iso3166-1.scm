(define-module (crates-io is o3 iso3166-1) #:use-module (crates-io))

(define-public crate-iso3166-1-0.1.0 (c (n "iso3166-1") (v "0.1.0") (h "0h0znbmzdhwj8nfi9qdww6kpqk3whiwimlrksxrshqqhhx8g0nzw")))

(define-public crate-iso3166-1-1.0.0 (c (n "iso3166-1") (v "1.0.0") (h "06bybx7bdglwj0nh405svp9l28w7hk0cfmbcbrcy8pqx15il8nqf")))

(define-public crate-iso3166-1-1.0.1 (c (n "iso3166-1") (v "1.0.1") (h "1b5r7sbypj188jcc6smcap1msf0b1qsxxmlqlp6iyim0nbf2mrpc")))

