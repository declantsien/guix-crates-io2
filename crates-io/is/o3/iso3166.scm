(define-module (crates-io is o3 iso3166) #:use-module (crates-io))

(define-public crate-iso3166-1.0.0 (c (n "iso3166") (v "1.0.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1hcm8wgc3yzpz2kzqy1c9d0qgqjkqzr5sw4kfqy0287lhrjz2hp6")))

(define-public crate-iso3166-1.0.1 (c (n "iso3166") (v "1.0.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0wmzyl5pjzrag91x8vj6wawlw9nhfn4mz0z1qkk51nib7jsc8y2p")))

