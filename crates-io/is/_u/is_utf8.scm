(define-module (crates-io is _u is_utf8) #:use-module (crates-io))

(define-public crate-is_utf8-0.1.0-alpha (c (n "is_utf8") (v "0.1.0-alpha") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02c8ghp4gqkplvzh8wyyzcg47jcp1xjpzvng5f9dq5nrilmx23m4")))

(define-public crate-is_utf8-0.1.0 (c (n "is_utf8") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15jpvdr5i2vgiwn1pxw6ckz7cj8bgbp0mc3qf0c9rgjczch3707z")))

(define-public crate-is_utf8-0.1.1 (c (n "is_utf8") (v "0.1.1") (h "0nf14jbgkpsdyf18p4cgbgwyy6s9ksgc8ahlj7v6ms2jiddalgr5")))

(define-public crate-is_utf8-0.1.2 (c (n "is_utf8") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "11nwg71kjnd0c1hkz4h41llcv8mhgxpzrawc0zqjfdp7304krd6f")))

(define-public crate-is_utf8-0.1.3 (c (n "is_utf8") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1lfwmaxkl2q399xpmf0mr02k5kd8r0apdaxfrwyw29d2v7a1lh6n")))

(define-public crate-is_utf8-0.1.4 (c (n "is_utf8") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "12v32711ii47chhhx3w3ypnx3ri0kf0k3hqjdkiai26c6x62gd1h")))

