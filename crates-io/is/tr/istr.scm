(define-module (crates-io is tr istr) #:use-module (crates-io))

(define-public crate-istr-0.1.0 (c (n "istr") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (f (quote ("raw"))) (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1hf6ax8vjm6xjvxdn6i2p12vy5bm0gvd0wgcral83z07xl58x3pn") (f (quote (("cache-utf8" "simdutf8"))))))

