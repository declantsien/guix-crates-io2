(define-module (crates-io is tr istring) #:use-module (crates-io))

(define-public crate-istring-0.1.0 (c (n "istring") (v "0.1.0") (h "0frb3nway9zx69avviflwj5i3ynmbw7svl7z8xpqljr7p0im4871")))

(define-public crate-istring-0.1.1 (c (n "istring") (v "0.1.1") (h "0kllsaf2wn3jypdw91838qbs0dhanp23nf3biwm3iiwp75f1h9b7")))

(define-public crate-istring-0.1.2 (c (n "istring") (v "0.1.2") (h "1lm9sbv5iil45wb51yxz7gwz6ij6mj7nqj184nld9hm3f5vs9cb6")))

(define-public crate-istring-0.1.3 (c (n "istring") (v "0.1.3") (h "12grjsa7d7sqk4xmbvac3bcppgs73cn0f6nh7hkc5rx8cqnmmxn4")))

(define-public crate-istring-0.1.4 (c (n "istring") (v "0.1.4") (h "0zv623krnv3s9ly6drkchxdj1xjjwr90l9m44iwaciv0z38dhqxg")))

(define-public crate-istring-0.1.5 (c (n "istring") (v "0.1.5") (h "0l4d6z34h3ph7d4jhd0bjws3x54z60wz2r4r2lv3bn8vwx5hnn8f")))

(define-public crate-istring-0.1.6 (c (n "istring") (v "0.1.6") (h "0vqhny98asqlafh60dp40bg069i6xkhvdxsmb2ls2jy2z8i50j59")))

(define-public crate-istring-0.1.7 (c (n "istring") (v "0.1.7") (h "06gipjmgkkk4mh0id6iqahbqzvv8djrazgsv9r7r0k7xfbgn44kb")))

(define-public crate-istring-0.1.9 (c (n "istring") (v "0.1.9") (h "142n1zk1bw94rs8m0fw1b2k5l572qvlpqymcxh49skv8iykbqbdh")))

(define-public crate-istring-0.1.10 (c (n "istring") (v "0.1.10") (h "00kz2anq4nshml55a1jwybny80w8fdhymn983x539a9vz43r8pwc")))

(define-public crate-istring-0.1.11 (c (n "istring") (v "0.1.11") (h "0m6m9qcvn485923bbb59qi4dr5bj022f8qixwiacyajcs336xddd")))

(define-public crate-istring-0.1.12 (c (n "istring") (v "0.1.12") (h "1ckwrm93m3qnf314g4xqs83ig73wj7kd8zp7yqzr3m36a5qq2xqc")))

(define-public crate-istring-0.1.13 (c (n "istring") (v "0.1.13") (h "0yz62vh8k0gx1xf6cyc7h3x681408hhw1qfsvn84iz86ad5qp5zc")))

(define-public crate-istring-0.2.0 (c (n "istring") (v "0.2.0") (h "1c05vk82i6hki9y9la6wgbl9cc91kxhi9asdv2c2ahdb3d9fhvwr")))

(define-public crate-istring-0.2.1 (c (n "istring") (v "0.2.1") (h "086ff52gqs7fbd9g0aadwj54zpq2g4ffcczddwan9h0wq61j7yyy")))

(define-public crate-istring-0.2.2 (c (n "istring") (v "0.2.2") (h "06fgx59l3c3p280w8b9ppxl89i2b2hfg7z9yvs5x8la5vn0wlw2c")))

(define-public crate-istring-0.3.0 (c (n "istring") (v "0.3.0") (d (list (d (n "datasize") (r "^0.2.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "15blwhs02a662a2ph6jd7jcy0xh1g2disyyy9z7d47b74awm2xw5") (f (quote (("size" "datasize") ("serialize" "serde"))))))

(define-public crate-istring-0.3.2 (c (n "istring") (v "0.3.2") (d (list (d (n "datasize") (r "^0.2.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1l2djyaaq6a1b015nfa6kl3vd272xd7w498pwvi4ykv3m9n3jgf0") (f (quote (("std") ("size" "datasize") ("serialize" "serde"))))))

(define-public crate-istring-0.3.3 (c (n "istring") (v "0.3.3") (d (list (d (n "datasize") (r "^0.2.13") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0r06a4ixn66zhm6jiilx136slgfs4xy4m0xcjigcjv0dpyl5ryl0") (f (quote (("std") ("size" "datasize") ("serialize" "serde"))))))

(define-public crate-istring-0.3.4 (c (n "istring") (v "0.3.4") (d (list (d (n "datasize") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0fbs0z5snm114ql5xqy5h166nxg02a5x3winsydimg7ckbxwcp47") (f (quote (("std") ("size" "datasize") ("serialize" "serde")))) (s 2) (e (quote (("rkyv" "dep:rkyv"))))))

