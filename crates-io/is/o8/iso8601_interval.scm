(define-module (crates-io is o8 iso8601_interval) #:use-module (crates-io))

(define-public crate-iso8601_interval-0.1.0 (c (n "iso8601_interval") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1ncmdy6brfkvmq2xm5bflbx6h4gcnlm464ad2nqa41lk8mxn8jjb")))

(define-public crate-iso8601_interval-0.2.0 (c (n "iso8601_interval") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1l62569aqxs38jqbbmy1z1l2jprx91zs86rfm850y3v59j09arvl")))

(define-public crate-iso8601_interval-0.3.0 (c (n "iso8601_interval") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1zi61vhf632554cga2h7bfzicfvjrnd70qh4kp7kl7pmb92c94k6")))

