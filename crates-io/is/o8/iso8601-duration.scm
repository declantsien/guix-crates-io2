(define-module (crates-io is o8 iso8601-duration) #:use-module (crates-io))

(define-public crate-iso8601-duration-0.1.0 (c (n "iso8601-duration") (v "0.1.0") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "0r64kbncrq8ac4992663vvs76xcm32jlvccy9chp8h52gzcivdb0")))

(define-public crate-iso8601-duration-0.2.0 (c (n "iso8601-duration") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "04f5i2l9r4k44p822pqy59fmjxpk9yikgbbiq86s2g2x1bvdysm2") (f (quote (("default"))))))

