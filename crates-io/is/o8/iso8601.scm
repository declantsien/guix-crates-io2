(define-module (crates-io is o8 iso8601) #:use-module (crates-io))

(define-public crate-iso8601-0.0.1 (c (n "iso8601") (v "0.0.1") (h "182fy86a9fzxx3kxfjzaypxxlxc5xjxr1yy9pn6p95p8b93r3sm4")))

(define-public crate-iso8601-0.1.0 (c (n "iso8601") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.21") (o #t) (d #t) (k 0)) (d (n "nom") (r "^0.5") (d #t) (k 0)))) (h "1kynq49idcbiijfl5r6bw16z6svvjb2sarycq4x78rn2z44l0whp") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-iso8601-0.1.1 (c (n "iso8601") (v "0.1.1") (d (list (d (n "clippy") (r "~0.0.23") (o #t) (d #t) (k 0)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)))) (h "0xy48qyfmirslaj4dy6n4g8b564jap3cjiql35fmj5vgii7ldp0i") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-iso8601-0.2.0 (c (n "iso8601") (v "0.2.0") (d (list (d (n "clippy") (r "> 0.0.0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "07jx50i57kbf8a27i4wyqdjs5hax5x7jgj71vjpqjmsk9b93pq43") (f (quote (("default"))))))

(define-public crate-iso8601-0.3.0 (c (n "iso8601") (v "0.3.0") (d (list (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "0vvn6f9gv2295ik77nvaz99wzbwz1bmasrd787sz6d9mlwa6ks23") (f (quote (("default"))))))

(define-public crate-iso8601-0.4.0 (c (n "iso8601") (v "0.4.0") (d (list (d (n "nom") (r "^5.0.0") (k 0)))) (h "0n2dcikk0m0np724jjyqvy44hdwyypfki5znjvyava2rg808mq6f") (f (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-iso8601-0.4.1 (c (n "iso8601") (v "0.4.1") (d (list (d (n "nom") (r "^7") (k 0)))) (h "1awzvg2b32p5ghn8fba882aqmj6cz0xx23fd8jlb4wb2pvra6n8a") (f (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-iso8601-0.4.2 (c (n "iso8601") (v "0.4.2") (d (list (d (n "nom") (r "^7") (k 0)))) (h "15nfg6d4qlniw4gk7039s5y07lzgr1dp9snsw63lsxarnyz4zfg5") (f (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-iso8601-0.5.0 (c (n "iso8601") (v "0.5.0") (d (list (d (n "nom") (r "^7") (k 0)))) (h "1l3qf6rbjccsmiizf36z2pi90fi4f2ll2gfrk4a8cschs2rsn89g") (f (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-iso8601-0.5.1 (c (n "iso8601") (v "0.5.1") (d (list (d (n "nom") (r "^7") (k 0)))) (h "1djym0p79kymsxxpbvcfdr37ayiknlky0fpxr4wdriif25gg2si9") (f (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-iso8601-0.6.0 (c (n "iso8601") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "nom") (r "^7") (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cpqkhmky8mz3f4y0byzxrfss4fvpnkniwlxqqlwkcb1k7i4d991") (f (quote (("std" "nom/std") ("default" "std")))) (s 2) (e (quote (("chrono" "dep:chrono" "num-traits"))))))

(define-public crate-iso8601-0.6.1 (c (n "iso8601") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "nom") (r "^7") (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lqif1zp19fjmrbhcdjx0ydnljax3090san5zq8r1x98x9rmsklj") (f (quote (("std" "nom/std") ("default" "std")))) (s 2) (e (quote (("chrono" "dep:chrono" "num-traits"))))))

