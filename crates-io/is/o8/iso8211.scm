(define-module (crates-io is o8 iso8211) #:use-module (crates-io))

(define-public crate-iso8211-0.1.1-alpha.1 (c (n "iso8211") (v "0.1.1-alpha.1") (h "05rp19hczk7gh3kg5rs8imbvldf7kvjyjrah8av04phmmb7xpdwq") (y #t)))

(define-public crate-iso8211-0.1.1-alpha.2 (c (n "iso8211") (v "0.1.1-alpha.2") (h "04a4najlscja780993lsah0y20z7zqzkc3cxpbzi2zlq8as39mhd") (y #t)))

(define-public crate-iso8211-0.1.1-alpha.3 (c (n "iso8211") (v "0.1.1-alpha.3") (h "0dsds30jkla6d6gx454a81z823ljn6i0izjkka209r0z79mc210i") (y #t)))

(define-public crate-iso8211-0.1.1-alpha.4 (c (n "iso8211") (v "0.1.1-alpha.4") (h "0w56xpvi8hyiwg22w3cd20vfzw2jfaad85rqh6x32g9f5wsvjywd") (y #t)))

