(define-module (crates-io is o8 iso8583_parser) #:use-module (crates-io))

(define-public crate-iso8583_parser-0.1.6 (c (n "iso8583_parser") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "emv_tlv_parser") (r "^0.1.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1dzd1nzw65kd43jggsiz5rrz9qv4bqqsndds1bd1cvzmv733mnng")))

(define-public crate-iso8583_parser-0.1.7 (c (n "iso8583_parser") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "emv_tlv_parser") (r "^0.1.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1ij49vs20kh89qril53wd15jhs2js3qj1844kp0r5my78kcnqjv8")))

(define-public crate-iso8583_parser-0.1.8 (c (n "iso8583_parser") (v "0.1.8") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "emv_tlv_parser") (r "^0.1.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1fwimx1hqwyrw761w7gyrfqxm0i41iv2i3vzsrcd5jlx5zl69vcd")))

(define-public crate-iso8583_parser-0.1.9 (c (n "iso8583_parser") (v "0.1.9") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "emv_tlv_parser") (r "^0.1.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1pc2f6kv6piqnqr5ksxk79rlzryh0vgcv9y6k5nldncp8snshpy0")))

(define-public crate-iso8583_parser-0.1.10 (c (n "iso8583_parser") (v "0.1.10") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "emv_tlv_parser") (r "^0.1.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "053nw8dd5l10nvdn4lmh1m29v9sianc9wh1c8swwxkg171pvkfz4")))

