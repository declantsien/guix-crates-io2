(define-module (crates-io is o8 iso8583) #:use-module (crates-io))

(define-public crate-iso8583-0.1.0 (c (n "iso8583") (v "0.1.0") (d (list (d (n "bit-array") (r "^0.4.4") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "0mmpx7b6di03rlbhn7hk9fivsq6gc8qz31md601bpal39rd9x8vg")))

(define-public crate-iso8583-0.1.1 (c (n "iso8583") (v "0.1.1") (d (list (d (n "bit-array") (r "^0.4.4") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.0") (d #t) (k 0)))) (h "1sbary9cgdxrsidrq78f3whfjsfhy2wzb60bx810q0wyvn51n3k0")))

