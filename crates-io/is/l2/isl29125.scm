(define-module (crates-io is l2 isl29125) #:use-module (crates-io))

(define-public crate-isl29125-0.1.0 (c (n "isl29125") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1gbaw3613jrh7ajn19615ym9cbycvy2yknhlp1zhhf4zrvllnz16")))

