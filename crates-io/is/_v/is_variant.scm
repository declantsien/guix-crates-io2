(define-module (crates-io is _v is_variant) #:use-module (crates-io))

(define-public crate-is_variant-0.1.0 (c (n "is_variant") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fk6hdar5mc6xg5p5zwzxbi2yii2issj67d8v4i0sg3x049z946b")))

(define-public crate-is_variant-0.1.1 (c (n "is_variant") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bs8q9mh440ap33nd9wfps5c31ix41dza1nna95wr1wavf0qgniy")))

(define-public crate-is_variant-1.0.0 (c (n "is_variant") (v "1.0.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kkrsinslxhxrgbmp1x02q92xy9dwfvjbf2vnvq9jqsifchy4dyd")))

