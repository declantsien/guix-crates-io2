(define-module (crates-io is ab isabelle-client) #:use-module (crates-io))

(define-public crate-isabelle-client-0.1.0 (c (n "isabelle-client") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "05zg321rcpfmazkqfxbzcq37m376aw2205k4adkcz3v82qp1iq3h")))

