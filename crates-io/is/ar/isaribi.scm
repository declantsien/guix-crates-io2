(define-module (crates-io is ar isaribi) #:use-module (crates-io))

(define-public crate-isaribi-0.1.0 (c (n "isaribi") (v "0.1.0") (d (list (d (n "kagura") (r ">=0.12.0, <0.13.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r ">=0.2.68, <0.3.0") (d #t) (k 0)) (d (n "web-sys") (r ">=0.3.45, <0.4.0") (f (quote ("Element" "HtmlCollection" "HtmlElement" "HtmlStyleElement" "CssRuleList" "CssStyleSheet"))) (d #t) (k 0)))) (h "1aslznhh7dx3xzfcm43hxfmflbvf4fxmm09pwlj4sbss5wvpkg36")))

(define-public crate-isaribi-0.2.0 (c (n "isaribi") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.51") (f (quote ("console" "Document" "Element" "HtmlCollection" "HtmlElement" "HtmlStyleElement" "CssRuleList" "CssStyleSheet" "Window"))) (d #t) (k 0)))) (h "096npbcmkhzf879m3fg7qnq2sf25slcmxmqmxalmdiqkxf27mc8k")))

(define-public crate-isaribi-0.2.1 (c (n "isaribi") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.51") (f (quote ("console" "Document" "Element" "HtmlCollection" "HtmlElement" "HtmlStyleElement" "CssRuleList" "CssStyleSheet" "Window"))) (d #t) (k 0)))) (h "04rk0bpnhz65cxy0dw2zc1scrq2aswy310av6c3j6vbhxvgs0yc2")))

(define-public crate-isaribi-0.2.2 (c (n "isaribi") (v "0.2.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.51") (f (quote ("console" "Document" "Element" "HtmlCollection" "HtmlElement" "HtmlStyleElement" "CssRuleList" "CssStyleSheet" "Window"))) (d #t) (k 0)))) (h "1byifa0p6xrgkwshy5iszixmpb6kyw7zhi136hm1r253kxs8sbi7")))

(define-public crate-isaribi-0.2.3 (c (n "isaribi") (v "0.2.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.51") (f (quote ("console" "Document" "Element" "HtmlCollection" "HtmlElement" "HtmlStyleElement" "CssRuleList" "CssStyleSheet" "Window"))) (d #t) (k 0)))) (h "1hniy420bp9yw8pz56h59jiq8dmn317wknf923qq2vj1igyqfyfw")))

(define-public crate-isaribi-0.2.4 (c (n "isaribi") (v "0.2.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.51") (f (quote ("console" "Document" "Element" "HtmlCollection" "HtmlElement" "HtmlStyleElement" "CssRuleList" "CssStyleSheet" "Window"))) (d #t) (k 0)))) (h "0zyvpbjsgjc1i0jdmqjihskkf46lsfjcmmlpzcfy48dgckhs3brc")))

