(define-module (crates-io is o9 iso9660) #:use-module (crates-io))

(define-public crate-iso9660-0.1.0 (c (n "iso9660") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "fuse") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "md5") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "117vfnhj5mnbfki5rx6nlk2g3lz3d9i25g9adzs9i0nrpd10390d") (f (quote (("nightly") ("default"))))))

(define-public crate-iso9660-0.1.1 (c (n "iso9660") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "fuser") (r "^0.12") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "md5") (r "^0.7") (d #t) (k 2)))) (h "1zx0k9v3ad5rpxnsg65g5y7lplcnyrs2kdbf1ixrrb9hzii6sna0") (f (quote (("nightly") ("default"))))))

