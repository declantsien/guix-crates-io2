(define-module (crates-io is -s is-same) #:use-module (crates-io))

(define-public crate-is-same-0.1.0 (c (n "is-same") (v "0.1.0") (h "1nrxh9nrfj298f3kkvk3q3ay8bljd5cdz090hyrpyw4005kp70sc")))

(define-public crate-is-same-0.2.0 (c (n "is-same") (v "0.2.0") (h "1m0am1ik8sr6qmqk8qvvhzzxfpdxa545l45d8yzdndzrmvg9lmmd")))

(define-public crate-is-same-0.2.1 (c (n "is-same") (v "0.2.1") (h "1r2zxbw27qdvnikqdmhvq0lgyiz5imy8239h5jdb0amv619z50zn")))

