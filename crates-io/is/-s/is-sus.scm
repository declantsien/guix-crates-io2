(define-module (crates-io is -s is-sus) #:use-module (crates-io))

(define-public crate-is-sus-0.1.0 (c (n "is-sus") (v "0.1.0") (h "1x04k52him4wcrkg1yxinlg6h6rnkamak8x7j0jfjisbqg8vd6kh")))

(define-public crate-is-sus-1.0.0 (c (n "is-sus") (v "1.0.0") (h "1ia390jd8s9y2jmnghvjvqpp7nw0xqwmhr81i0z44f0km3i9b0zc")))

