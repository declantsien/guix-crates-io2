(define-module (crates-io is -s is-same-derive) #:use-module (crates-io))

(define-public crate-is-same-derive-0.1.0 (c (n "is-same-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1mjg3ccy2naq59cs9nabi7bgsyl63yn6a4njq9dwc0snqsg8563w")))

(define-public crate-is-same-derive-0.1.1 (c (n "is-same-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1kp5zqxr8s5k05wb8zcv4p10zzimwx60dqh44d62phkif459x7k1")))

