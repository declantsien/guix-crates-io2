(define-module (crates-io is oa isoalloc) #:use-module (crates-io))

(define-public crate-isoalloc-0.1.0 (c (n "isoalloc") (v "0.1.0") (d (list (d (n "libisoalloc-sys") (r "^0.1.0") (d #t) (k 0)))) (h "04zdq76nbdcy1p6ypqyk3a3wyarki0dn2q25ryd68ng0yrzs021s")))

(define-public crate-isoalloc-0.2.0 (c (n "isoalloc") (v "0.2.0") (d (list (d (n "libisoalloc-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0b878dczkig32ix8aqdsnxwsk67fxh08hzldc3yz5nskhcrlhhaz")))

(define-public crate-isoalloc-0.2.1 (c (n "isoalloc") (v "0.2.1") (d (list (d (n "libisoalloc-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0nmpi2lnyyszb0hx6sbaxrnfqff48wn5dlqkrdxcx3plg0cqksi3")))

(define-public crate-isoalloc-0.2.2 (c (n "isoalloc") (v "0.2.2") (d (list (d (n "libisoalloc-sys") (r "^0.2.2") (d #t) (k 0)))) (h "07vfkz4v7xnvhll3z5m49zqfaqnr83mfic0w0dwi6xwyxi63yx3w")))

(define-public crate-isoalloc-0.2.3 (c (n "isoalloc") (v "0.2.3") (d (list (d (n "libisoalloc-sys") (r "^0.2.3") (d #t) (k 0)))) (h "01ccbd5ndznpd3pw8fw9qzfvbrnwdcr4d4z31qbabab1a9bwmiqx")))

(define-public crate-isoalloc-0.2.4 (c (n "isoalloc") (v "0.2.4") (d (list (d (n "libisoalloc-sys") (r "^0.2.4") (d #t) (k 0)))) (h "048xwh7ikph78vciqb1pwgxqabd1rqslbbpf49w7q5ca0nhv0mj4")))

(define-public crate-isoalloc-0.2.5 (c (n "isoalloc") (v "0.2.5") (d (list (d (n "libisoalloc-sys") (r "^0.2.5") (d #t) (k 0)))) (h "16m8lmfdaiqn1l55kk9l3nzw4h4nmpc25xc0pcbjrply0hla2bz7") (f (quote (("userfaultfd") ("tagging") ("sanity") ("neon") ("mte") ("memory_tagging")))) (y #t)))

(define-public crate-isoalloc-0.2.6 (c (n "isoalloc") (v "0.2.6") (d (list (d (n "libisoalloc-sys") (r "^0.2.5") (d #t) (k 0)))) (h "1zck0psvh6mgiqnzb2f2cgzs469mqnbp4ibpc3k46y0nc37zjvfa") (f (quote (("userfaultfd") ("tagging") ("sanity") ("neon") ("mte") ("memory_tagging"))))))

(define-public crate-isoalloc-0.2.7 (c (n "isoalloc") (v "0.2.7") (d (list (d (n "libisoalloc-sys") (r "^0.2.7") (d #t) (k 0)))) (h "0gd050r02kkh4lxil9n8ffq3dly7yxcmv0r4zhy2q23bsj261807") (f (quote (("userfaultfd" "libisoalloc-sys/userfaultfd") ("tagging" "libisoalloc-sys/tagging") ("sanity" "libisoalloc-sys/sanity") ("nothread" "libisoalloc-sys/nothread") ("neon" "libisoalloc-sys/neon") ("mte" "libisoalloc-sys/mte") ("memory_tagging" "libisoalloc-sys/memory_tagging"))))))

(define-public crate-isoalloc-0.2.8 (c (n "isoalloc") (v "0.2.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libisoalloc-sys") (r "^0.2.7") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "14fd1ijlksyjspa5gl67zmd1ncpni9s31ibqx2rihx91r8078f34") (f (quote (("userfaultfd" "libisoalloc-sys/userfaultfd") ("tagging" "libisoalloc-sys/tagging") ("smallmem" "libisoalloc-sys/smallmem") ("sanity" "libisoalloc-sys/sanity") ("nothread" "libisoalloc-sys/nothread") ("neon" "libisoalloc-sys/neon") ("mte" "libisoalloc-sys/mte") ("memory_tagging" "libisoalloc-sys/memory_tagging"))))))

(define-public crate-isoalloc-0.2.9 (c (n "isoalloc") (v "0.2.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libisoalloc-sys") (r "^0.2.9") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)))) (h "0160dcddbvp8ga81nirhphds9k8j4914fqdrsng267bd5rq76p5d") (f (quote (("userfaultfd" "libisoalloc-sys/userfaultfd") ("tagging" "libisoalloc-sys/tagging") ("smallmem" "libisoalloc-sys/smallmem") ("sanity" "libisoalloc-sys/sanity") ("nothread" "libisoalloc-sys/nothread") ("neon" "libisoalloc-sys/neon") ("mte" "libisoalloc-sys/mte") ("memory_tagging" "libisoalloc-sys/memory_tagging"))))))

