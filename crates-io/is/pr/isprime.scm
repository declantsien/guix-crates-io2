(define-module (crates-io is pr isprime) #:use-module (crates-io))

(define-public crate-isprime-0.1.0 (c (n "isprime") (v "0.1.0") (h "1jj7mmkngfrcdx1sjm8a4arv1yhy4fk4ddffm37bab5x3hirqi7g")))

(define-public crate-isprime-0.1.1 (c (n "isprime") (v "0.1.1") (h "0xwlnc1ifhmd55xv4ms3nv3qc556qz3f1z0g1pddyc1adxgifrqr")))

