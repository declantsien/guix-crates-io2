(define-module (crates-io is il isildur) #:use-module (crates-io))

(define-public crate-isildur-0.1.0 (c (n "isildur") (v "0.1.0") (d (list (d (n "crates-index") (r "^0.12") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)))) (h "1ypk843vk7xhnvri9dw4khhb1rrnhzjdzvifrwlwhirckh8v3h69")))

(define-public crate-isildur-0.2.0 (c (n "isildur") (v "0.2.0") (d (list (d (n "crates-index") (r "^0.12") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)))) (h "1zyrg1mypm0c7qn62kknbnb3sm7ra417a4gampkd3vz57z8xl5m6")))

