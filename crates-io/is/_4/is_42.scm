(define-module (crates-io is _4 is_42) #:use-module (crates-io))

(define-public crate-is_42-0.1.0 (c (n "is_42") (v "0.1.0") (h "0frpb0k88ya4s5373qgqyjpkh86liyww3dh9ngqvmdc252vn30pj")))

(define-public crate-is_42-0.1.2 (c (n "is_42") (v "0.1.2") (h "0flwf9khz1p14x8a3rkypgkrwxz1rj00c110jmn9jwnaj2hyw19y")))

