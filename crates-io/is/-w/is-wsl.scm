(define-module (crates-io is -w is-wsl) #:use-module (crates-io))

(define-public crate-is-wsl-0.1.0 (c (n "is-wsl") (v "0.1.0") (d (list (d (n "is-docker") (r "^0.1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "1jxk0ivaf0nhy6425bc6fv002ql48gjgxd5di0vab66wjvq09gca")))

(define-public crate-is-wsl-0.2.0 (c (n "is-wsl") (v "0.2.0") (d (list (d (n "is-docker") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "1h9wl2gw06m2jra9cjhmgaypi6mrxg8i7b432j8f1cq3j4hl71q9")))

(define-public crate-is-wsl-0.3.0 (c (n "is-wsl") (v "0.3.0") (d (list (d (n "is-docker") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "08bgxdb17qcl7ca5hvcj4wmfrm4qcw8dl19x4avczn6qaay89qmq")))

(define-public crate-is-wsl-0.4.0 (c (n "is-wsl") (v "0.4.0") (d (list (d (n "is-docker") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "19bs5pq221d4bknnwiqqkqrnsx2in0fsk8fylxm1747iim4hjdhp")))

