(define-module (crates-io is ta istandy_rs) #:use-module (crates-io))

(define-public crate-istandy_rs-0.1.0 (c (n "istandy_rs") (v "0.1.0") (h "19dl7cyf300q0mh2a0jc54p7hi1g1qncl89765vlgi4mbv6qvhsh") (y #t)))

(define-public crate-istandy_rs-0.1.1 (c (n "istandy_rs") (v "0.1.1") (h "1ha89pvd2n34l7rwn7r7zqmhcr6cggqwbxmg70fzqk162a5pcng6") (y #t)))

