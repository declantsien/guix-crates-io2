(define-module (crates-io is ta istanbul-oxide) #:use-module (crates-io))

(define-public crate-istanbul-oxide-0.0.1 (c (n "istanbul-oxide") (v "0.0.1") (d (list (d (n "indexmap") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "10yw7f99m36pvhj1gj37ymmgqac6406wjwvnwis5v83ar3g38xcq")))

(define-public crate-istanbul-oxide-0.0.2 (c (n "istanbul-oxide") (v "0.0.2") (d (list (d (n "indexmap") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c1f91r8pmvys3j35lchka7y3kdgqgxxakw0xzlw1pd0qyalk92w")))

(define-public crate-istanbul-oxide-0.0.3 (c (n "istanbul-oxide") (v "0.0.3") (d (list (d (n "indexmap") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pznkx47ff0dd1ac8f3k2449208ma43dvpw0nsbjkdrkj3bdirlc")))

(define-public crate-istanbul-oxide-0.0.4 (c (n "istanbul-oxide") (v "0.0.4") (d (list (d (n "indexmap") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "188zins32sps45mrdrdlxgzzfga8xpg9snk8xzhgvmf51nkxlirm")))

(define-public crate-istanbul-oxide-0.0.5 (c (n "istanbul-oxide") (v "0.0.5") (d (list (d (n "indexmap") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1inpksdzdd41i5sai1g6w0bk3w679r2xrrkcsmpmwz90f6rqwdd5")))

(define-public crate-istanbul-oxide-0.0.6 (c (n "istanbul-oxide") (v "0.0.6") (d (list (d (n "indexmap") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q69c7p2n414pnl6ygl1qi76zzlsbnv8snxmvr8hyc6p4cgrkx14")))

(define-public crate-istanbul-oxide-0.0.7 (c (n "istanbul-oxide") (v "0.0.7") (d (list (d (n "indexmap") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ikmbx2825yir0jx1mkf3qhlk2rz1w955l7spw51plffv7b5nc9n")))

(define-public crate-istanbul-oxide-0.0.8 (c (n "istanbul-oxide") (v "0.0.8") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)))) (h "175d62kyg4znaq93rcv17ndpwy83sb85y2045yxvqxkn3wkwcny6")))

(define-public crate-istanbul-oxide-0.0.9 (c (n "istanbul-oxide") (v "0.0.9") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "19nd5qjvbr45grfmma4pfys14h82bamfm587yirmn0nami6189b6")))

(define-public crate-istanbul-oxide-0.0.10 (c (n "istanbul-oxide") (v "0.0.10") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c8ingnadca9l4pw797msbmlqdnzzjb0kpmfy6p8dn6lxc6zlgwf")))

(define-public crate-istanbul-oxide-0.0.11 (c (n "istanbul-oxide") (v "0.0.11") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "01p23bnv7bbg0g4zkq22vpdna30pc8d19z4840iaj26jggq874dy")))

(define-public crate-istanbul-oxide-0.0.12 (c (n "istanbul-oxide") (v "0.0.12") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "120xaqv2qdyr97zihh9pk228jcjmflp0qn88kamiwnibc40s4zs0")))

(define-public crate-istanbul-oxide-0.0.13 (c (n "istanbul-oxide") (v "0.0.13") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "1aqqx4j0y2lyv7cskb43p58287164vha77hi3z2ijyh6779q7ywi")))

(define-public crate-istanbul-oxide-0.0.14 (c (n "istanbul-oxide") (v "0.0.14") (d (list (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b8vazck8bdbkvn64zllznwijn50y681alhn7yh72lxly3bfxdd9")))

(define-public crate-istanbul-oxide-0.0.15 (c (n "istanbul-oxide") (v "0.0.15") (d (list (d (n "indexmap") (r "^1.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zn8w7fy8cxdx67086a95ssci0igm66m058pn8hh4plr9yi0iqfz")))

(define-public crate-istanbul-oxide-0.0.16 (c (n "istanbul-oxide") (v "0.0.16") (d (list (d (n "indexmap") (r "^1.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qlzgsns71xrmxnbzs8qr789dvjiw8c69caacfzp53cdqy9wx956")))

(define-public crate-istanbul-oxide-0.0.17 (c (n "istanbul-oxide") (v "0.0.17") (d (list (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x03x3q5m9kyy71h6773yfw60vlz5y8nb7v6c3mpxhq6x4l9p27a")))

(define-public crate-istanbul-oxide-0.0.18 (c (n "istanbul-oxide") (v "0.0.18") (d (list (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wd6ijjw22v8b1n6ln44yipky5dsqn5wpjafbsjx364vfcijq313")))

(define-public crate-istanbul-oxide-0.0.19 (c (n "istanbul-oxide") (v "0.0.19") (d (list (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "15sr9j054za4xiw0ij6yq78w1vsz2p72g07jhlaxzsci3q20idqj")))

(define-public crate-istanbul-oxide-0.0.20 (c (n "istanbul-oxide") (v "0.0.20") (d (list (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qhpm86iws2wqbrka7vphxfz5r0wpm5mk62j9xsdc93dzrj30d3g")))

