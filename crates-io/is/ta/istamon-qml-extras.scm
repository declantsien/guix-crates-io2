(define-module (crates-io is ta istamon-qml-extras) #:use-module (crates-io))

(define-public crate-istamon-qml-extras-0.1.0-alpha (c (n "istamon-qml-extras") (v "0.1.0-alpha") (d (list (d (n "cpp") (r "^0.5.6") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.6") (d #t) (k 1)) (d (n "qmetaobject") (r "^0.2.5") (d #t) (k 0)) (d (n "qttypes") (r "^0.2.5") (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (d #t) (k 1)))) (h "0m1i3pyl6v4vv8zqs7z0fcc49fs4dhhcmw4wg2mv4kbvgpg9r18x")))

(define-public crate-istamon-qml-extras-0.1.0 (c (n "istamon-qml-extras") (v "0.1.0") (d (list (d (n "cpp") (r "^0.5.6") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.6") (d #t) (k 1)) (d (n "qmetaobject") (r "^0.2.5") (d #t) (k 0)) (d (n "qttypes") (r "^0.2.5") (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (d #t) (k 1)))) (h "0iw4prmkki6y2f9j0pp2nlr35vaf3imcm9118c1c4c3kifp6xd33")))

