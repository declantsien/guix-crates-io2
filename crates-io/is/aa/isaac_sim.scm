(define-module (crates-io is aa isaac_sim) #:use-module (crates-io))

(define-public crate-isaac_sim-0.1.0 (c (n "isaac_sim") (v "0.1.0") (d (list (d (n "autocxx") (r "^0.26") (d #t) (k 0)) (d (n "builder_derive_more") (r "^0.1") (d #t) (k 0)) (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "omniverse") (r "^0.1") (d #t) (k 0)) (d (n "pxr") (r "^0.1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k48srhr33c096yy4ldjs9429dnwki5q10vm999h7ahb3bivksly") (r "1.70")))

(define-public crate-isaac_sim-0.2.0 (c (n "isaac_sim") (v "0.2.0") (d (list (d (n "autocxx") (r "^0.26") (d #t) (k 0)) (d (n "builder_derive_more") (r "^0.1") (d #t) (k 0)) (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "derive_builder") (r "^0.20") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "omniverse") (r "^0.2") (d #t) (k 0)) (d (n "pxr") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02v2ny5sn48cc6haa8dsn0i1rdxm7nnpbdpn6s3p33d1i7zdhfl0") (r "1.70")))

