(define-module (crates-io is -n is-not-capnp) #:use-module (crates-io))

(define-public crate-is-not-capnp-0.0.1 (c (n "is-not-capnp") (v "0.0.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "capnp") (r "^0.17.2") (d #t) (k 0)) (d (n "capnp-rpc") (r "^0.17.0") (d #t) (k 0)))) (h "1s9hnbvpg92r65jmmlhsbb5miq47szg4gp0c9w4rkilqkk564myz")))

