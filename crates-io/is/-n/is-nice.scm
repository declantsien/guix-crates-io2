(define-module (crates-io is -n is-nice) #:use-module (crates-io))

(define-public crate-is-nice-0.0.0 (c (n "is-nice") (v "0.0.0") (h "02xs0r5j9bqlyzc5af8js0mi9h4mv3vgl52iy149c2d4n71amns5")))

(define-public crate-is-nice-0.0.69 (c (n "is-nice") (v "0.0.69") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 0)))) (h "1cd50n8vzb1d7m539nb2bhx7fz9qv7qnjkdhahyicaf2m0m5nnaa")))

