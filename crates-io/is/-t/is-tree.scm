(define-module (crates-io is -t is-tree) #:use-module (crates-io))

(define-public crate-is-tree-0.1.0 (c (n "is-tree") (v "0.1.0") (d (list (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)))) (h "1xp1aghwg86z3w99sw9531aiaihv9zwg50bl4fada42ibm54ja1l")))

(define-public crate-is-tree-0.2.0 (c (n "is-tree") (v "0.2.0") (d (list (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "is-tree-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1d91i92pb0394a9i8zyw9gmxj90hlnhjs93fnsqiyg8nxycaycvx")))

