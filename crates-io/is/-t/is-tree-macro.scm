(define-module (crates-io is -t is-tree-macro) #:use-module (crates-io))

(define-public crate-is-tree-macro-0.1.0 (c (n "is-tree-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1lfahqwq5kd1kcxcsrfq7k94q63p2cvk37fdd8bwv3y93xm90ww3")))

