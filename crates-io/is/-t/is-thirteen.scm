(define-module (crates-io is -t is-thirteen) #:use-module (crates-io))

(define-public crate-is-thirteen-0.1.0 (c (n "is-thirteen") (v "0.1.0") (h "1bsc67z2f5pxpvjmglsil7g4j1ndgr04y4r6ylfnwiqj0v441jgl") (y #t)))

(define-public crate-is-thirteen-0.0.13 (c (n "is-thirteen") (v "0.0.13") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "phf") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)))) (h "1ifv5g121lgw4yc9378l8s9sc642d3v0qbgf3dszg4v626fpxvv0")))

(define-public crate-is-thirteen-0.13.0 (c (n "is-thirteen") (v "0.13.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "phf") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)))) (h "067560ih4c0ccy5ps7pv38rmj8294y3g1ydrbsmffi3laysgyx9b")))

