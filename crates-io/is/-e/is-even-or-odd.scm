(define-module (crates-io is -e is-even-or-odd) #:use-module (crates-io))

(define-public crate-is-even-or-odd-1.0.0 (c (n "is-even-or-odd") (v "1.0.0") (d (list (d (n "is-even") (r "^1") (d #t) (k 0)) (d (n "is-odd") (r "^1") (d #t) (k 0)))) (h "1h8x4zd8xrklq25a67rplq6sid05px703v6dnk49yg81dd49dlx3")))

