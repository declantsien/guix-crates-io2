(define-module (crates-io is -e is-even-u16) #:use-module (crates-io))

(define-public crate-is-even-u16-1.0.0 (c (n "is-even-u16") (v "1.0.0") (h "1l9v6rxw9hlan5rbamm2wbs7aszfkg1qizds6zkhyig260fiinx3")))

(define-public crate-is-even-u16-1.0.1 (c (n "is-even-u16") (v "1.0.1") (h "00y7xs98gml5x4gm9gv64rdqgr6l2rl43ww2v2jcypzaydrq3dnx")))

(define-public crate-is-even-u16-1.0.2 (c (n "is-even-u16") (v "1.0.2") (h "1mdbwbsy51gmjlh46qb9zmygnn9qxr1asphvniqnn8l0h8mmq51v")))

