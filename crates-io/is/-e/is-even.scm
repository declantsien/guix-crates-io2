(define-module (crates-io is -e is-even) #:use-module (crates-io))

(define-public crate-is-even-1.0.0 (c (n "is-even") (v "1.0.0") (d (list (d (n "is-odd") (r "^1.0.0") (d #t) (k 0)))) (h "1l42sf7g9bl3dpnpmpia144jxn0sh9hfm72d49n1xgwxrysmimc3")))

