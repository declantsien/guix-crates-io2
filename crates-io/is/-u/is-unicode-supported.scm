(define-module (crates-io is -u is-unicode-supported) #:use-module (crates-io))

(define-public crate-is-unicode-supported-0.1.0 (c (n "is-unicode-supported") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "0p29c1pm0cpspx8xj86g5h18smm7mk1i38yahsm9yw705ipf5vqw")))

