(define-module (crates-io is -u is-up) #:use-module (crates-io))

(define-public crate-is-up-0.1.0 (c (n "is-up") (v "0.1.0") (h "09p5mnsm1pwn5g3zc0fs66hzvn50kd8ns0bkgdkhvws1rpmklxlh")))

(define-public crate-is-up-0.1.1 (c (n "is-up") (v "0.1.1") (h "0jx9dgy0mqy1499hr7g971k5d526z6l4h3bxhpp5jf8ldpd7b9l0")))

(define-public crate-is-up-0.1.2 (c (n "is-up") (v "0.1.2") (h "18w8vba7gmayy6p7sdc9mapm5bsxg08gvsr083vmlcjdhcbh9dvv")))

