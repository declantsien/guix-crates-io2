(define-module (crates-io is #{-i}# is-impl) #:use-module (crates-io))

(define-public crate-is-impl-0.1.0 (c (n "is-impl") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.19") (f (quote ("compat"))) (d #t) (k 2)) (d (n "futures01") (r "^0.1") (d #t) (k 2) (p "futures")))) (h "1cmmwvfic99gbc59y29lyvvn38bm5ca012grw8lh7xvm4nr69njf")))

(define-public crate-is-impl-0.1.1 (c (n "is-impl") (v "0.1.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.19") (f (quote ("compat"))) (d #t) (k 2)) (d (n "futures01") (r "^0.1") (d #t) (k 2) (p "futures")))) (h "1xi1mj94n2b7x42nnd6ynwafj2jjff6pfd44ql6vgb95x4iw1phd")))

