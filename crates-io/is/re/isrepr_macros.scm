(define-module (crates-io is re isrepr_macros) #:use-module (crates-io))

(define-public crate-isrepr_macros-0.1.0 (c (n "isrepr_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "004b8sp9mrm7aids58ylh5sd6m5bivvh2g7ljnskdg5kvfjcii3v")))

