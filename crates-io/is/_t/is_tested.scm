(define-module (crates-io is _t is_tested) #:use-module (crates-io))

(define-public crate-is_tested-0.1.0 (c (n "is_tested") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "13ic5wkpqbf176clrmw2ciff2vypx0mg32vfa6zm7vkl558nbb6z")))

(define-public crate-is_tested-0.1.1 (c (n "is_tested") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0rd4qf5yp58nlzchyryyl96ya2yqpd931mczl1i0w5l36fx86ln9")))

