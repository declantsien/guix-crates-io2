(define-module (crates-io is _t is_true_rs) #:use-module (crates-io))

(define-public crate-is_true_rs-0.1.0 (c (n "is_true_rs") (v "0.1.0") (h "0rzkzci1c0a6n6jqxp3pl6hgj5x8gfj0y2wfshvjs19jigy3hml9") (y #t)))

(define-public crate-is_true_rs-0.1.1 (c (n "is_true_rs") (v "0.1.1") (h "085n053qbylc3i47jw4i7fyc5n3ml6qrvsxn2nr1b3378svlbfic") (y #t)))

(define-public crate-is_true_rs-0.1.2 (c (n "is_true_rs") (v "0.1.2") (h "0kp74q8nb3qsy6wa317nls82yhbanbpyib07sdlkgs8sv8wgmcbk")))

