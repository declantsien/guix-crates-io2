(define-module (crates-io is _t is_tuesday) #:use-module (crates-io))

(define-public crate-is_tuesday-0.1.0 (c (n "is_tuesday") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "1wfr2299i16c7ibrzk5dyg1ar03rqggqh8fs8dfhl6lckv0syiq3")))

(define-public crate-is_tuesday-0.1.1 (c (n "is_tuesday") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "00xvrh13ws7qaax4hppyw5kiabfmjj4p59y28zk41zz0b9q01qrs")))

