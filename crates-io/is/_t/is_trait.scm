(define-module (crates-io is _t is_trait) #:use-module (crates-io))

(define-public crate-is_trait-0.1.0 (c (n "is_trait") (v "0.1.0") (h "0pry41cg83nr5gf9rgrq6b3sm7n45faqsxsjxadshdv2wgnad0if")))

(define-public crate-is_trait-0.1.1 (c (n "is_trait") (v "0.1.1") (h "1phq8gvzk06bvv9fhw0xa0mmjp962r14q4l0fnfn2dbdc86y42yx")))

(define-public crate-is_trait-0.1.2 (c (n "is_trait") (v "0.1.2") (h "18s57w4sbb9klmi6mc18d9xlpmall6ybaryaka75a0bbfj8d3xry")))

(define-public crate-is_trait-0.1.3 (c (n "is_trait") (v "0.1.3") (h "1xbwsbgph8pbfz0qj317pyv2qamq2z3s29il6z5jvlvr6y860jza")))

(define-public crate-is_trait-0.1.4 (c (n "is_trait") (v "0.1.4") (h "0ww1q7by4qd77zcg6rp0d83xz2882rzbq6694hfbk731z2mnnqx7")))

