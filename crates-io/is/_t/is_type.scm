(define-module (crates-io is _t is_type) #:use-module (crates-io))

(define-public crate-is_type-0.1.0 (c (n "is_type") (v "0.1.0") (h "1r53b0b7b7wz58zgnx6lgmjdlbbk7qfsmgal7pvsmdqjarxv3pmn")))

(define-public crate-is_type-0.2.0 (c (n "is_type") (v "0.2.0") (h "1cn08mi34fzyr9gr7z3vnclab6n6ylpzqpizn9whd4qv0pqwg8p6")))

(define-public crate-is_type-0.2.1 (c (n "is_type") (v "0.2.1") (h "1v7knam6fgymp5nzmg58r5lk33q28pjnv4kh3kmydbxzcyhwvrzy")))

