(define-module (crates-io is _t is_terminal_polyfill) #:use-module (crates-io))

(define-public crate-is_terminal_polyfill-1.70.0 (c (n "is_terminal_polyfill") (v "1.70.0") (h "0018q5cf3rifbnzfc1w1z1xcx9c6i7xlywp2n0fw4limq1vqaizq") (f (quote (("default")))) (r "1.70.0")))

(define-public crate-is_terminal_polyfill-1.48.0 (c (n "is_terminal_polyfill") (v "1.48.0") (d (list (d (n "is-terminal") (r "^0.4.2") (d #t) (k 0)))) (h "09qrvj2dpiawhp33ik2ffq5flkm59zq1mjk1rs9ihd6h9vl2saxm") (f (quote (("default")))) (r "1.48.0")))

