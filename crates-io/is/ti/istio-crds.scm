(define-module (crates-io is ti istio-crds) #:use-module (crates-io))

(define-public crate-istio-crds-0.1.0 (c (n "istio-crds") (v "0.1.0") (d (list (d (n "k8s-openapi") (r "^0.15.0") (f (quote ("v1_20"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1d6mnh6c43hv4khzfkaxpwh6pchbdwcl75j77a9jiwgndn647g1g")))

