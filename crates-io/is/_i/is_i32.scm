(define-module (crates-io is _i is_i32) #:use-module (crates-io))

(define-public crate-is_i32-0.1.0 (c (n "is_i32") (v "0.1.0") (h "1970iz53sw454fcp4141wgcqjp3szck7aqk0cakl3690r9gillhk")))

(define-public crate-is_i32-0.1.1 (c (n "is_i32") (v "0.1.1") (h "017a5lgx8lhax8a6hds3hvj1qvbz3yfdrj30hcg3mgiyscvdv9qq")))

