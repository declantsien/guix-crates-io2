(define-module (crates-io is _i is_in) #:use-module (crates-io))

(define-public crate-is_in-0.1.0 (c (n "is_in") (v "0.1.0") (h "0226wrlppav3pw249viyz4sx2z3v5iblwcmdz4kk0ilpnfnv55hg")))

(define-public crate-is_in-0.1.1 (c (n "is_in") (v "0.1.1") (h "1l127vywgvflzfvnfyjyv52xs9wxmpbqa2ijxyi5p0jg2rd6qj0j")))

(define-public crate-is_in-1.0.0 (c (n "is_in") (v "1.0.0") (h "17ih5v7747q29kv8z4crbhgbdipi2p48r9yfp6sbp3cc34jb33ba")))

