(define-module (crates-io is _e is_effected) #:use-module (crates-io))

(define-public crate-is_effected-0.0.1 (c (n "is_effected") (v "0.0.1") (h "00mppm6h02dpcp1nij5dibmap85cs32zvriadpqkzlxjdnjz8kfn")))

(define-public crate-is_effected-0.1.0 (c (n "is_effected") (v "0.1.0") (d (list (d (n "git2") (r "^0.13.20") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0w6qym3k96rqmq8qrqqqfdvwsp8m2x7gfi2x899bsvkk471vry4r")))

(define-public crate-is_effected-0.2.0 (c (n "is_effected") (v "0.2.0") (d (list (d (n "git2") (r "^0.13.21") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0zxcj9fi6pjf6vaa302bldg13q3i58b42y5wbbn16pndxj8m0j4f")))

(define-public crate-is_effected-0.3.0 (c (n "is_effected") (v "0.3.0") (d (list (d (n "git2") (r "^0.13.22") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1hfd18ipdsfzd6vygrs398n2559f5lx5rxfr6l1gi3vjrxqk8m8z")))

(define-public crate-is_effected-0.4.0 (c (n "is_effected") (v "0.4.0") (d (list (d (n "git2") (r "^0.13.22") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rstest") (r "^0.11.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "16pkj22g9nrcd8bikchf40xmk4blfqpa6hhfd7ygs5hgykkyn84g")))

(define-public crate-is_effected-0.4.1 (c (n "is_effected") (v "0.4.1") (d (list (d (n "git2") (r "^0.13.22") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rstest") (r "^0.11.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1avf1nmpv2xx4wdh66a1bl4wjvgx75vnsiyvflq7lf7q87h4ip7g")))

