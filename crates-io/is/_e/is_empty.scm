(define-module (crates-io is _e is_empty) #:use-module (crates-io))

(define-public crate-is_empty-0.1.0 (c (n "is_empty") (v "0.1.0") (d (list (d (n "is_empty_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0znpyr5cs965vnx4x839mxgysvjnglh6f2p7fz6pw3r550arjm6h")))

(define-public crate-is_empty-0.2.0 (c (n "is_empty") (v "0.2.0") (d (list (d (n "is_empty_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "08a0vws47i7fvj0lb6chkihr0795nf9mapc7553qjlznnxa634qa")))

