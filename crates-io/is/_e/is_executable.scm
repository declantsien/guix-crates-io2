(define-module (crates-io is _e is_executable) #:use-module (crates-io))

(define-public crate-is_executable-0.1.0 (c (n "is_executable") (v "0.1.0") (d (list (d (n "diff") (r "^0.1.10") (d #t) (k 2)))) (h "1a754crfyc5987kv4fp2382zmvgdq1v2n4cwdv2a3fi7nksn0rf1")))

(define-public crate-is_executable-0.1.2 (c (n "is_executable") (v "0.1.2") (d (list (d (n "diff") (r "^0.1.10") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0xy516afjh79a0d53j9v4w5mgi2s0r6f6qynnyz8g0dwi8xmab9h")))

(define-public crate-is_executable-1.0.0 (c (n "is_executable") (v "1.0.0") (d (list (d (n "diff") (r "^0.1.10") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0vchfjjgq2913bnyddwk61vnv4zgissjqpp5jlcx1vg6si8s09hl")))

(define-public crate-is_executable-1.0.1 (c (n "is_executable") (v "1.0.1") (d (list (d (n "diff") (r "^0.1.10") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1j59iqaxcgax0qll30rarpcr7y3dpkl38iv4mlkfcxbvsv3cv6ps")))

