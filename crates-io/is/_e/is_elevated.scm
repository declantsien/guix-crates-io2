(define-module (crates-io is _e is_elevated) #:use-module (crates-io))

(define-public crate-is_elevated-0.1.0 (c (n "is_elevated") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("securitybaseapi" "processthreadsapi"))) (d #t) (k 0)))) (h "1chadmqzmgwancpz1hc2643dakgpxvyfpnh95h2x74sghmizlx78")))

(define-public crate-is_elevated-0.1.1 (c (n "is_elevated") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("securitybaseapi" "processthreadsapi"))) (d #t) (k 0)))) (h "1qn0vsvzi96r3i95ang83dmrk38cjbmpvwqk4bipi804qlvppffp")))

(define-public crate-is_elevated-0.1.2 (c (n "is_elevated") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("securitybaseapi" "processthreadsapi"))) (d #t) (k 0)))) (h "1dgr8azqq0d5xfyjrv8pjzyz914vmljrbjsx064ffqyvyl7hd6aj")))

