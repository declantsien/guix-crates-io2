(define-module (crates-io is _e is_empty_derive) #:use-module (crates-io))

(define-public crate-is_empty_derive-0.1.0 (c (n "is_empty_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "040n9xhaah88yi3n4qb7dzjg1x01j39avbdnghp8izpgxh4k146q")))

