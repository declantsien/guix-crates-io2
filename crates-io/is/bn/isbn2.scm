(define-module (crates-io is bn isbn2) #:use-module (crates-io))

(define-public crate-isbn2-0.4.0 (c (n "isbn2") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.5.2") (k 0)) (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "indexmap") (r "^1.6.1") (o #t) (k 0)) (d (n "quick-xml") (r "^0.20.0") (o #t) (d #t) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1n4w63sppbgpcql97ivs0rs23268w6gzzn10qfcfwaz1v6n05v1j") (f (quote (("serialize" "serde") ("runtime-ranges" "quick-xml" "indexmap"))))))

