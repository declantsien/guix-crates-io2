(define-module (crates-io is bn isbnid) #:use-module (crates-io))

(define-public crate-isbnid-0.1.0 (c (n "isbnid") (v "0.1.0") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "0y9b2fnfviyrj2d3339zmh7vlqylrzagzal1n88qaxhmrbpn1nx8")))

(define-public crate-isbnid-0.1.2 (c (n "isbnid") (v "0.1.2") (d (list (d (n "regex") (r "0.1.*") (d #t) (k 0)))) (h "1ia9lmrk7xijc71v92i7nk8hkyi3wlp4h065x9f635sjjzdvss08")))

(define-public crate-isbnid-0.1.3 (c (n "isbnid") (v "0.1.3") (d (list (d (n "regex") (r "0.2.*") (d #t) (k 0)))) (h "1ii37d3qihm1mdxic9b86bppwjxv8szfyx4riggvjkk1gq112mqq")))

