(define-module (crates-io is bn isbn) #:use-module (crates-io))

(define-public crate-isbn-0.1.0 (c (n "isbn") (v "0.1.0") (h "1j35m9bihg7bslcwih6sll55clazfsmlkk11mpp4cd9rn8ydrh8b")))

(define-public crate-isbn-0.2.0 (c (n "isbn") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "roxmltree") (r "^0.4") (d #t) (k 1)))) (h "0nqdssvac3ij5j6y8q9xbfk2b8852ycd51g45ib1xig62dv1i7hj")))

