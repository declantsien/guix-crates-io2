(define-module (crates-io is la isla-cat) #:use-module (crates-io))

(define-public crate-isla-cat-0.1.0 (c (n "isla-cat") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "14mmpa5pnzd754a1i93xl5hz6wclp48dspbsi6wzbrli9jr83h3h")))

(define-public crate-isla-cat-0.2.0 (c (n "isla-cat") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.19.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1sshzb3jsnb1jagmzg0nwvk9l8czi9f76zyxvn0hckap746p2d0k")))

