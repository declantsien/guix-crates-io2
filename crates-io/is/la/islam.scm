(define-module (crates-io is la islam) #:use-module (crates-io))

(define-public crate-islam-0.1.1 (c (n "islam") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1xynq6ql7y1wylfj85ds07bjnds87k8mgk454ahcbhin3n374v1v")))

(define-public crate-islam-0.1.2 (c (n "islam") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "183p5nflg4am79q8sjhgrxla4gzfhdggajxxgrf392g4v1ygg46g")))

(define-public crate-islam-0.1.3 (c (n "islam") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "13kzrgwqf7ldawv3ai29h5maxd5qivwhsl8nvbd5671v3mmn36lv")))

(define-public crate-islam-0.1.4 (c (n "islam") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1691r1gz5a9vmvqyhmvjsa76357i35bca21v1c0qddn39gr6swds")))

(define-public crate-islam-0.1.5 (c (n "islam") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0z8449dplm7cd4ah8paj6y6yjv9dvrpfxsyzxfk3cr1v3k5lp9yy")))

(define-public crate-islam-0.2.0 (c (n "islam") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros" "formatting" "local-offset"))) (d #t) (k 0)))) (h "0qj5vqnwzw75hxvhs7rzdvyxksm003w64vn26w79kv2wvz3bq7xm")))

(define-public crate-islam-1.0.0 (c (n "islam") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros" "formatting" "local-offset"))) (d #t) (k 0)))) (h "02pfh3qaa9g8b49i4plhykdb7vfd93x1ng76y8bbm9bfdzf2xjlb")))

(define-public crate-islam-2.0.0 (c (n "islam") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zgaadfwnzk6rhvv9zfbm2dqzc7my1harnbmdyy5v8cjmynbz8q1")))

(define-public crate-islam-3.0.0 (c (n "islam") (v "3.0.0") (d (list (d (n "chrono") (r "^0.4.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0ihqrd4b5ki8fq48hqawrl9fjlvn1q9g7xpdjz7rjj8vy1fbhbyr")))

(define-public crate-islam-3.1.0 (c (n "islam") (v "3.1.0") (d (list (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1407ka8ykxipacdrvghhlvk023hsja3znsma5c225dbmjdixm548")))

(define-public crate-islam-3.2.0 (c (n "islam") (v "3.2.0") (d (list (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "17xv2dlxvgrfcskhmni7n3vfi14frslfcxhza9jlkvx307ppsfga")))

