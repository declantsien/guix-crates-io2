(define-module (crates-io is la island_map_generator_bevy) #:use-module (crates-io))

(define-public crate-island_map_generator_bevy-0.1.0 (c (n "island_map_generator_bevy") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.23.0") (d #t) (k 0)) (d (n "island_map_generator_algo") (r "^0.1.0") (d #t) (k 0)))) (h "1bfda4wid8yxdkkn7npxhfmqkim27ri7adrvgssk2d397ahv09vd")))

