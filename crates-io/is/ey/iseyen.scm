(define-module (crates-io is ey iseyen) #:use-module (crates-io))

(define-public crate-iseyen-0.1.0 (c (n "iseyen") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("time" "rt"))) (d #t) (k 0)))) (h "148hbv6jsbplzd2vndjn01c3l7i2qwxpfrhp4dk66khwl0qsqm3v") (y #t) (r "1.56.1")))

(define-public crate-iseyen-0.1.1 (c (n "iseyen") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("time" "rt"))) (d #t) (k 0)))) (h "13kq2l94fhn2vs1vn7k87d24vzddm01bg1rir2c3fsnaw1419s4w") (y #t) (r "1.56.1")))

(define-public crate-iseyen-0.1.2 (c (n "iseyen") (v "0.1.2") (d (list (d (n "futures-core") (r "^0.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1w7k64isy3cmwkx860hlm6468n2qanndkrmi4c079a0r65il132z") (y #t) (r "1.56.1")))

