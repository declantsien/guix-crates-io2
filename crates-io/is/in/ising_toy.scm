(define-module (crates-io is in ising_toy) #:use-module (crates-io))

(define-public crate-ising_toy-0.2.0 (c (n "ising_toy") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "plotly") (r "^0.8.4") (f (quote ("kaleido"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d5x8ziqjzmm123yjs2qwb6mv4qzaccwb6mykm5sfh7j3x155vid")))

