(define-module (crates-io is in isin) #:use-module (crates-io))

(define-public crate-isin-0.1.0 (c (n "isin") (v "0.1.0") (h "05z7yj4q64l0li39asjssdqilv0n3by8iasc71w49w6hxx631vxn") (y #t)))

(define-public crate-isin-0.1.1 (c (n "isin") (v "0.1.1") (h "0nf3hf5ni35xgdf65cqp3xiw7y4md58xvvd9mmbjl9qjra4j85xh") (y #t)))

(define-public crate-isin-0.1.2 (c (n "isin") (v "0.1.2") (h "1fxfzfyb3bpwwr04lgc95yq7dln8p4877p1b9vzpn2ppbf4wc87g") (y #t)))

(define-public crate-isin-0.1.3 (c (n "isin") (v "0.1.3") (h "0yqvqvn6sb8x0z8bcx7j0h5zk2hszgx2nyfy4v2ni83gx6svv1hk") (y #t)))

(define-public crate-isin-0.1.4 (c (n "isin") (v "0.1.4") (h "10d0dkmmkr9r8lzrji61pwj0i654fa1mr2slklh26dmm6bwn96qn") (y #t)))

(define-public crate-isin-0.1.5 (c (n "isin") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0rrhacs8616mx3syncwz3axpx5z4fih1fc3amgdmmd60xkckvl8k") (y #t)))

(define-public crate-isin-0.1.6 (c (n "isin") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "10zf4jp3z502kjj8ainli6sc9pdx5rrr9gaycxhp3yqdp7hic4a4") (y #t)))

(define-public crate-isin-0.1.7 (c (n "isin") (v "0.1.7") (d (list (d (n "bstr") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "02wwhfcbgcrb2gdb0a389rhs4x2djwwp2wmcr04ac3vj8szlxxz2") (y #t)))

(define-public crate-isin-0.1.8 (c (n "isin") (v "0.1.8") (d (list (d (n "bstr") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "06pnsfssc6r7h62yy0nmpxc70a26zhygg2h1r8azp3zcmgv99b9n") (y #t)))

(define-public crate-isin-0.1.9 (c (n "isin") (v "0.1.9") (d (list (d (n "bstr") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0arwn19v0wyl4pcjhsmsmf3iinwjp57nm31rbiy1gd2aq48kxild") (y #t)))

(define-public crate-isin-0.1.10 (c (n "isin") (v "0.1.10") (d (list (d (n "bstr") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0x3ms0i991pnq5m6wpg1f0c5is2qhc00837dp4apch64b2aqbgzc") (y #t)))

(define-public crate-isin-0.1.11 (c (n "isin") (v "0.1.11") (d (list (d (n "bstr") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0z3xs52jlqq3hq7rdcffk2xkxli9ml23isdfz9f5r8pmrykylh11") (y #t)))

(define-public crate-isin-0.1.12 (c (n "isin") (v "0.1.12") (d (list (d (n "bstr") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1wbsabaqdm1nxp4p54kqmmjqdjf09661vxa49vs2qilslbkgscpg") (y #t)))

(define-public crate-isin-0.1.13 (c (n "isin") (v "0.1.13") (d (list (d (n "bstr") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1ivlrl8h3vvqknhm9vnsp2skxnxqcaji982y25zflgrlll6dbl0a") (y #t)))

(define-public crate-isin-0.1.14 (c (n "isin") (v "0.1.14") (d (list (d (n "bstr") (r "^1.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0n0lcmnaa3xdsqsinn4c6sqh3ng1b7kwyvyqms7ihw4ryp7665b5") (y #t)))

(define-public crate-isin-0.1.15 (c (n "isin") (v "0.1.15") (d (list (d (n "bstr") (r "^1.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1n4vf741vgmskg5cigkrysqp0cf3a2rx191pkjhc3jkv15vsdf0s") (y #t)))

(define-public crate-isin-0.1.16 (c (n "isin") (v "0.1.16") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)))) (h "19lh6rwh593djqhzf55acii4xxn7q3zs38s9yrnvjnbvvhgaj53r") (y #t)))

(define-public crate-isin-0.1.17 (c (n "isin") (v "0.1.17") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "0g9q7ii431rdv747gs0msww0y91nr4bfja0qpz9zxy30apixi4ld") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-isin-0.1.18 (c (n "isin") (v "0.1.18") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "0l6fk89c5b6ik6lq72w5h5q44zkkhjy5yg2rjbs269gwdj3ckqsy") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

