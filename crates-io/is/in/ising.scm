(define-module (crates-io is in ising) #:use-module (crates-io))

(define-public crate-ising-0.2.1 (c (n "ising") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("std" "small_rng"))) (k 0)))) (h "11v5lm0572mhazipxv12iwwvqhhxlwxbv35xmc84dk3zw6gc7all") (y #t)))

(define-public crate-ising-0.2.2 (c (n "ising") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("std" "small_rng"))) (k 0)))) (h "19v29hr70kfl7v0dvd0jj8sg9a1qkrxdjd0gzzzkcfvpadfv4qr7") (y #t)))

(define-public crate-ising-0.2.3 (c (n "ising") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("std" "small_rng"))) (k 0)))) (h "0qgsryqcqzizpsixv9kdpgqvg8wpp4v3nfzhkinmi6a0xbzj7p23")))

