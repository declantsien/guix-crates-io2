(define-module (crates-io is in ising_gui) #:use-module (crates-io))

(define-public crate-ising_gui-0.3.2 (c (n "ising_gui") (v "0.3.2") (d (list (d (n "eframe") (r "^0.22.0") (f (quote ("glow" "default_fonts"))) (k 0)) (d (n "ising") (r "=0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "small_rng"))) (k 0)) (d (n "rfd") (r "^0.11.4") (f (quote ("xdg-portal"))) (k 0)))) (h "0d648kyibsl5gi6hwp8znff0nngqvkrvp6s1bin5mlgwwjhy365y") (y #t)))

(define-public crate-ising_gui-0.3.3 (c (n "ising_gui") (v "0.3.3") (d (list (d (n "eframe") (r "^0.22.0") (f (quote ("glow" "default_fonts"))) (k 0)) (d (n "ising") (r "=0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "small_rng"))) (k 0)) (d (n "rfd") (r "^0.11.4") (f (quote ("xdg-portal"))) (k 0)))) (h "1fj9mhfhcdffxrsijhlk6csbhn37l871c19xg2aaybv8sphj1aji")))

