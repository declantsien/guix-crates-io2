(define-module (crates-io is in ising_lib) #:use-module (crates-io))

(define-public crate-ising_lib-0.1.0 (c (n "ising_lib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "1nln95r7qf7zps24gi2s5i1ljg5yj4vqw58vklr8y4ai29x571y8")))

(define-public crate-ising_lib-0.2.0 (c (n "ising_lib") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "1kgldw62iml6z39ynjxs3gwxh8aidva21gbvfipscqsywqp1g16f")))

(define-public crate-ising_lib-0.3.0 (c (n "ising_lib") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "pbr") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "07b84diq9ixi4z8xbfrz3cdp0dwalinsff60fzwxz8r8fy8l4ba4")))

(define-public crate-ising_lib-1.0.0 (c (n "ising_lib") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vfak9azs9vdk081bl8p1xkl6bi0lb3ikprdm9jbf15j64zdm7wc")))

