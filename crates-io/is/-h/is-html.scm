(define-module (crates-io is -h is-html) #:use-module (crates-io))

(define-public crate-is-html-0.1.0 (c (n "is-html") (v "0.1.0") (h "1gn8dsais1mci3vnaa9m57pr4q4cniciiccsjrgdkcxj3pimx39m")))

(define-public crate-is-html-0.1.1 (c (n "is-html") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15izzvvnjligmh7mcmwxx386kv8mcnicm8i6x5b9py6yww354x6f")))

(define-public crate-is-html-0.1.2 (c (n "is-html") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1xp61gcmbzzlbpzqx2l9a292b5p3psn5459il5vym9y4xn3n8137")))

(define-public crate-is-html-0.1.3 (c (n "is-html") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0z413j8snc1gwd083k96hp9z13k4xcy4n1qmfmbvdd45vf39w2yf")))

