(define-module (crates-io is o4 iso4217) #:use-module (crates-io))

(define-public crate-iso4217-0.1.0 (c (n "iso4217") (v "0.1.0") (h "1l6pnydxs6whmkdhkib8lzwbrdp6hh76jlfgjcsczpmrqrxfxrnz") (y #t)))

(define-public crate-iso4217-0.1.1 (c (n "iso4217") (v "0.1.1") (h "117cci9xzh8x8lgh1501i7g6x502x6ns44fq0gz7fn2g57wrmgjc") (y #t)))

(define-public crate-iso4217-0.2.0 (c (n "iso4217") (v "0.2.0") (h "0mrs6y4ysg5l4671ld09w9qv12flana2cl7xyg0mkacdg9b3r38s") (y #t)))

(define-public crate-iso4217-0.3.0 (c (n "iso4217") (v "0.3.0") (h "1giir7xx4fwl3x6jhaij0idsfhsh5i0svsfydlz3j1jcczq3q9b9") (f (quote (("default" "const-fn") ("const-fn"))))))

(define-public crate-iso4217-0.3.1 (c (n "iso4217") (v "0.3.1") (h "0p4cgjhzqh5mi9f97lnd04wd280jirwva8gqdhvxy48ikw6k8sqn")))

(define-public crate-iso4217-0.3.2 (c (n "iso4217") (v "0.3.2") (h "0c12r05y2hrp9n6yd7bdqpbbw77hy8ld9jz9nmvh2kkhk2ls43hs")))

