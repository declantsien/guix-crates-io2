(define-module (crates-io is op isoperm) #:use-module (crates-io))

(define-public crate-isoperm-0.1.0 (c (n "isoperm") (v "0.1.0") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "1s7vdf07xs2i218z3kd53nwqqaxs19hi31bjb32ba395a4v5vq1c")))

(define-public crate-isoperm-0.1.1 (c (n "isoperm") (v "0.1.1") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "1wwpiw1hb69pdvh3x30vw4i0clqsf09biwh5agwxda646ghg5hpm")))

(define-public crate-isoperm-0.1.2 (c (n "isoperm") (v "0.1.2") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "16gf739jghidb7bdcz44pzx6sn6ad90gw9sihaghcbs2anw6pa6g")))

