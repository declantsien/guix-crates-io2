(define-module (crates-io is si issiz) #:use-module (crates-io))

(define-public crate-issiz-0.1.0 (c (n "issiz") (v "0.1.0") (h "1fn80kj8msnizglp929dknc651nyjxxdvr35r2f87ni7dkv42flk")))

(define-public crate-issiz-0.1.1 (c (n "issiz") (v "0.1.1") (h "02qwn1pqa3cb291w8rq8rcxspdfn6f7zyqfl6x3n5am9a6c55yqr")))

(define-public crate-issiz-0.1.2 (c (n "issiz") (v "0.1.2") (h "118fykk8jz1d4dvrc0m9c6nj63vwyyhc1yhw93nmabaxxjviq7h8")))

(define-public crate-issiz-0.1.3 (c (n "issiz") (v "0.1.3") (h "08wsi88grvmffhrj3a92knblgb6hvyah0lnp2dcix9b1k71bxa17")))

