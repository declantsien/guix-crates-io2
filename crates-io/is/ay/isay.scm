(define-module (crates-io is ay isay) #:use-module (crates-io))

(define-public crate-isay-0.1.0 (c (n "isay") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.2.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.2.5") (d #t) (k 0)))) (h "1x9p6aa0yv0qf32z0jh1rbr9kiyq4lin49fhr6cysbgjlcghwqvs")))

(define-public crate-isay-0.2.0 (c (n "isay") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.2.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.2.5") (d #t) (k 0)))) (h "1jmxb40b6ycn0cmamr486plf1fvx8ad7lp3n78lvss2q9ysw7zm8")))

