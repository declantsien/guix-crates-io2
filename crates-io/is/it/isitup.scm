(define-module (crates-io is it isitup) #:use-module (crates-io))

(define-public crate-isitup-2021.5.21 (c (n "isitup") (v "2021.5.21") (d (list (d (n "dyn-fmt") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "179r8klcpw3c2nv4w8fv1f39lwr0kj246sxibbh1j52iczqcc5g7")))

(define-public crate-isitup-2021.5.25 (c (n "isitup") (v "2021.5.25") (d (list (d (n "dyn-fmt") (r "^0.3.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "13fgaiwb1fvahhvqql5s4kvjv6z9vcxnj3j1hxs6c9i6qyw9hvvj")))

