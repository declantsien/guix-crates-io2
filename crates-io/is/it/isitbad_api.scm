(define-module (crates-io is it isitbad_api) #:use-module (crates-io))

(define-public crate-isitbad_api-0.1.0 (c (n "isitbad_api") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0wx14gvj31rsk1gbkw34qlbr8vda94yhkf7d504p2f2cr82jp8yp") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-isitbad_api-1.0.0 (c (n "isitbad_api") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1fgrad8ss9fpwjyfarcslvlkk46mshd0i20pi78xi3psg46iyyfv") (f (quote (("default") ("async" "tokio"))))))

