(define-module (crates-io is o- iso-tp) #:use-module (crates-io))

(define-public crate-iso-tp-0.1.0-alpha.1 (c (n "iso-tp") (v "0.1.0-alpha.1") (d (list (d (n "async-hal") (r "^0.1.0-alpha.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1xrx8fm3blks2lv47w2b9jqbnvp90dh7ia95mwfj823mkkhm7qk3")))

(define-public crate-iso-tp-0.1.0-alpha.2 (c (n "iso-tp") (v "0.1.0-alpha.2") (d (list (d (n "async-hal") (r "^0.1.0-alpha.8") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0zak2djm1xl1b3l8qij7913mc9rxs0cbvh2l552p16bd58s5yf13") (f (quote (("socket" "async-hal" "futures") ("full" "socket"))))))

(define-public crate-iso-tp-0.1.0-alpha.4 (c (n "iso-tp") (v "0.1.0-alpha.4") (d (list (d (n "async-hal") (r "^0.1.0-alpha.8") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1xm97cq2f6jyxy8fg5bk71j4m6c6kh6w850b2myl02hfg8bd2agp") (f (quote (("socket" "async-hal" "futures") ("full" "socket"))))))

(define-public crate-iso-tp-0.1.0-alpha.5 (c (n "iso-tp") (v "0.1.0-alpha.5") (d (list (d (n "async-hal") (r "^0.1.0-alpha.8") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0pwsw40mjk6cmn313lc1bjbaqzj66if7rlqwv4mwl41igqs7rimd") (f (quote (("transport" "async-hal" "futures") ("socket" "async-hal" "embedded-hal" "futures" "pin-project-lite") ("full" "socket" "transport"))))))

