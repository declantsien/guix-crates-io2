(define-module (crates-io is o- iso-20022-catm) #:use-module (crates-io))

(define-public crate-iso-20022-catm-0.1.0 (c (n "iso-20022-catm") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "validator") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "075kzprp552k0bz5zffqxp35c2k2df3qyvm4w66cxj1p0m5nshkh")))

