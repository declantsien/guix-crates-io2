(define-module (crates-io is o- iso-8601) #:use-module (crates-io))

(define-public crate-iso-8601-0.1.0 (c (n "iso-8601") (v "0.1.0") (d (list (d (n "chrono") (r "~0.4.6") (o #t) (d #t) (k 0)) (d (n "nom") (r "~4.0.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.76") (o #t) (d #t) (k 0)))) (h "15aaaqb7dpfa6vrlv2dahcpj0lidn8jv1agclr7xgn3hcb5r7baz") (f (quote (("chrono-serde" "chrono/serde" "serde"))))))

(define-public crate-iso-8601-0.2.0 (c (n "iso-8601") (v "0.2.0") (d (list (d (n "chrono") (r "~0.4.6") (o #t) (d #t) (k 0)) (d (n "nom") (r "~4.0.0") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "regex") (r "~1.0.5") (d #t) (k 0)) (d (n "serde") (r "~1.0.76") (o #t) (d #t) (k 0)))) (h "1aqkdyx71fhhhzrz6qdgabmj2k7b7vgx8ja82qbr6q1sw140aa9a") (f (quote (("chrono-serde" "chrono/serde" "serde"))))))

(define-public crate-iso-8601-0.3.0 (c (n "iso-8601") (v "0.3.0") (d (list (d (n "chrono") (r "~0.4.6") (o #t) (d #t) (k 0)) (d (n "nom") (r "~4.1.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.76") (o #t) (d #t) (k 0)))) (h "1c19104s2bq49w55pp2sw3vbby7p5canlq52qq3m98b8rdcvcn82") (f (quote (("chrono-serde" "chrono/serde" "serde"))))))

(define-public crate-iso-8601-0.4.0 (c (n "iso-8601") (v "0.4.0") (d (list (d (n "chrono") (r "~0.4.19") (o #t) (d #t) (k 0)) (d (n "nom") (r "~6.2.1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "serde") (r "~1.0.126") (o #t) (d #t) (k 0)))) (h "1hkznzpwym4g929q7nwdimv05njqjshgp67khxhf7gwbbgqdlxdp") (f (quote (("chrono-serde" "chrono/serde" "serde"))))))

(define-public crate-iso-8601-0.4.1 (c (n "iso-8601") (v "0.4.1") (d (list (d (n "chrono") (r "~0.4.19") (o #t) (d #t) (k 0)) (d (n "nom") (r "~6.2.1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "serde") (r "~1.0.126") (o #t) (d #t) (k 0)))) (h "0dkg19s2hpday797mjai79jz0pvi004l5qnbrq6pnyaximapzwjc") (f (quote (("chrono-serde" "chrono/serde" "serde"))))))

