(define-module (crates-io is o- iso-10303-parts) #:use-module (crates-io))

(define-public crate-iso-10303-parts-0.1.0 (c (n "iso-10303-parts") (v "0.1.0") (d (list (d (n "iso-10303") (r "^0.1") (d #t) (k 0)))) (h "1bhj3zrgbfa984ahyp750fzn8anwgw9qv0jrib60qq5dpwi7gmi8")))

(define-public crate-iso-10303-parts-0.2.0 (c (n "iso-10303-parts") (v "0.2.0") (d (list (d (n "iso-10303") (r "^0.2") (d #t) (k 0)))) (h "0k3jjd1x4jc3179yb79796yskwzggihlmgzg7bfgbgyhgjny7kfr")))

(define-public crate-iso-10303-parts-0.3.0 (c (n "iso-10303-parts") (v "0.3.0") (d (list (d (n "iso-10303") (r "^0.3") (d #t) (k 0)))) (h "18cm6kari8plp1mj9q20qsrbfrk75i6zkg9l37bqrzk0l714ixjp")))

(define-public crate-iso-10303-parts-0.4.0 (c (n "iso-10303-parts") (v "0.4.0") (d (list (d (n "iso-10303") (r "^0.4") (d #t) (k 0)))) (h "15q22zdrl7fmqi83wnmz1x5lssw6kn6ip30zfz6izvc8nwzdkd5z")))

(define-public crate-iso-10303-parts-0.5.0 (c (n "iso-10303-parts") (v "0.5.0") (d (list (d (n "iso-10303") (r "^0.5") (d #t) (k 0)))) (h "1ma0lq67mm5wbpqp3pkilrwmldf0gsx4v7ycvnhm5i0pkm2xq7xr")))

