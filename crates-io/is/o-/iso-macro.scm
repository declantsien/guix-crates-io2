(define-module (crates-io is o- iso-macro) #:use-module (crates-io))

(define-public crate-iso-macro-0.0.0 (c (n "iso-macro") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "17jxnlyvwsfzw4kivpa9jkvq93hc5c8n4wfvwlbdsnwf63an91si")))

(define-public crate-iso-macro-0.0.1 (c (n "iso-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0wn1s0rkrxk6vf8akwmnvq16rp3fj9k82xyx69fpjnqd3mncz3y4")))

(define-public crate-iso-macro-0.0.2 (c (n "iso-macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1mdyi07fpwqycdcw210q9xk1cpw931rzpkmgmsi68jnxg0xhprqa")))

(define-public crate-iso-macro-0.0.3 (c (n "iso-macro") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0wj5lq97d1amj3b11xbklfqk15yrbc4f6hybp6863r74bxl3ifab")))

(define-public crate-iso-macro-0.0.4 (c (n "iso-macro") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1jj2xiyzq1d468nvk0zqv43dmgnbgpnnr1gvljswnscwk3g9j83y")))

(define-public crate-iso-macro-0.0.5 (c (n "iso-macro") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "169x045cvmbkmi649njfli8nbkl2phh9hfz64s365q3la8py1lna")))

(define-public crate-iso-macro-0.0.6 (c (n "iso-macro") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "11fy26a2jjblgx2i63ipw013y79k5g48djj8i69q02mdjskhmzhp")))

