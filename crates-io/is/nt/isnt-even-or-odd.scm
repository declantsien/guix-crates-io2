(define-module (crates-io is nt isnt-even-or-odd) #:use-module (crates-io))

(define-public crate-isnt-even-or-odd-0.1.0 (c (n "isnt-even-or-odd") (v "0.1.0") (d (list (d (n "is-even-or-odd") (r "^1.0.0") (d #t) (k 0)))) (h "0p6pfg8i2h9gdckxhfdx8jhgwxm4d1yaz3snd7pkdy6m5k76imlf") (y #t)))

(define-public crate-isnt-even-or-odd-1.0.0 (c (n "isnt-even-or-odd") (v "1.0.0") (d (list (d (n "is-even-or-odd") (r "^1.0.0") (d #t) (k 0)))) (h "1hf9iwv4578ld0naffh66ky1qsj1q033v08l2bpf89m69z9wfjjv") (y #t)))

