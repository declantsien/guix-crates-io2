(define-module (crates-io is -v is-vowel) #:use-module (crates-io))

(define-public crate-is-vowel-0.1.0 (c (n "is-vowel") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.17") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0jy1q3md4y26zckd4nk8r0xk28z6x6igbbyxvwrzdw7dhcyq8p4z")))

