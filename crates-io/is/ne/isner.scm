(define-module (crates-io is ne isner) #:use-module (crates-io))

(define-public crate-isner-0.0.1-alpha.1 (c (n "isner") (v "0.0.1-alpha.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "03v4gf8rhyyhw7xsy32gfxl1q9d7zrsaxxaw5jackn02igynwfbg")))

(define-public crate-isner-0.0.1-alpha.2 (c (n "isner") (v "0.0.1-alpha.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1xipsypi979rlhlrh5wakk1wff8dyzb2ixqvgr16dqfy7k6f83l5")))

