(define-module (crates-io is hm ishmael) #:use-module (crates-io))

(define-public crate-ishmael-0.1.0 (c (n "ishmael") (v "0.1.0") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "17wx936bik0fn83vkw4scsv7ynj2j0zfb8pf0469iksqpd6asay5") (y #t)))

(define-public crate-ishmael-0.1.1 (c (n "ishmael") (v "0.1.1") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0lpim1iyf2bz61l3jpdmv8ik73aw4p2f8iaxilmrddkkvqvsmcdp") (y #t)))

(define-public crate-ishmael-0.1.2 (c (n "ishmael") (v "0.1.2") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "03mix2vmxpskqhg8sxazd41lnidmgksp8frnylqk6ijyasw5937y") (y #t)))

