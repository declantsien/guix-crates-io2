(define-module (crates-io is -o is-odd) #:use-module (crates-io))

(define-public crate-is-odd-1.0.0 (c (n "is-odd") (v "1.0.0") (h "0znihxhxljvgv3v3nvzcppjmbhjxd2srpl37n1mmy47nr55cqhap")))

(define-public crate-is-odd-1.0.1 (c (n "is-odd") (v "1.0.1") (h "0wbzlrs82ll7w7m0831rs6srcmxdf1ia68xjmcrgllajzq1h0ygx")))

(define-public crate-is-odd-1.1.0 (c (n "is-odd") (v "1.1.0") (h "0hwjxnl2nid2ga9zsww7f0wjrnixy2z313nhm33nq9qw28c9ckbp")))

