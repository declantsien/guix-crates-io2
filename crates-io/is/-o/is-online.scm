(define-module (crates-io is -o is-online) #:use-module (crates-io))

(define-public crate-is-online-0.1.0 (c (n "is-online") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.5") (f (quote ("cargo" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1nf7hyjk2mjihia47gjhkv5bc3nkqkl29mw7qhs1dgmgx3pqrfxr")))

