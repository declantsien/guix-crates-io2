(define-module (crates-io is -d is-dark-theme) #:use-module (crates-io))

(define-public crate-is-dark-theme-1.0.0 (c (n "is-dark-theme") (v "1.0.0") (d (list (d (n "core-foundation-sys") (r "^0.8.6") (d #t) (t "cfg(target_vendor = \"apple\")") (k 0)))) (h "1q19v85rapvwn9m1nbq36im4xyx07mh8d8akc4mkiad0rs2nyq32")))

