(define-module (crates-io is -d is-docker) #:use-module (crates-io))

(define-public crate-is-docker-0.1.0 (c (n "is-docker") (v "0.1.0") (h "0k1rayybbh9shhwwy8spkpmlfksmi6gpjf370xc9066wsl4xykhp")))

(define-public crate-is-docker-0.2.0 (c (n "is-docker") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "1cyibrv6817cqcpf391m327ss40xlbik8wxcv5h9pj9byhksx2wj")))

