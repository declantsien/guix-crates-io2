(define-module (crates-io is -d is-digit) #:use-module (crates-io))

(define-public crate-is-digit-0.1.0 (c (n "is-digit") (v "0.1.0") (h "157ajd3qb488vgr64d5r0xjvbp7ykgzprbw0k18z3r5awrv6ncpl") (y #t)))

(define-public crate-is-digit-0.1.1 (c (n "is-digit") (v "0.1.1") (h "16v9iyc8rknh3nk6nvp5hka95z22kfp227nm0slpcx7wqyp8yhhn") (y #t)))

(define-public crate-is-digit-0.1.2 (c (n "is-digit") (v "0.1.2") (h "10lb24c1z58xxfyasdzh21b84y4b7pyzr60mard3ipcaqd9aqfnl")))

