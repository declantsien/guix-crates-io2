(define-module (crates-io is #{31}# is31fl3218) #:use-module (crates-io))

(define-public crate-is31fl3218-0.1.0 (c (n "is31fl3218") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "0inqkmnr125xrfi2j24q38l6s2xngcrf9vpqqf1510v6c3hzfns3")))

(define-public crate-is31fl3218-0.2.0 (c (n "is31fl3218") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "05ihwfrdpy88iis8h75nchs272lls7fkfkmsxy40rz20k9r7hknz")))

(define-public crate-is31fl3218-0.3.0 (c (n "is31fl3218") (v "0.3.0") (d (list (d (n "embedded-hal-async") (r "^1.0") (d #t) (k 0)))) (h "1iyql061nfsj43zbmxir5c1p3zsppdsbsrhpr95bppmbpnk6qgk0")))

