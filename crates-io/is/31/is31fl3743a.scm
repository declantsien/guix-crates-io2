(define-module (crates-io is #{31}# is31fl3743a) #:use-module (crates-io))

(define-public crate-is31fl3743a-0.1.0 (c (n "is31fl3743a") (v "0.1.0") (d (list (d (n "embedded-graphics-core") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0d8j9768rbfcpy7pdbjmamriqky9l7yi2rhiywna9jgllr5shk48") (f (quote (("embedded_graphics" "embedded-graphics-core") ("default" "embedded_graphics"))))))

