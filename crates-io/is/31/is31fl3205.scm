(define-module (crates-io is #{31}# is31fl3205) #:use-module (crates-io))

(define-public crate-is31fl3205-0.0.1 (c (n "is31fl3205") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1k86bsbifrfxl03dg05a8p6ic69k3i4j4mdap8ppc34833aawfii") (f (quote (("default"))))))

