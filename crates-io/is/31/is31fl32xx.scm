(define-module (crates-io is #{31}# is31fl32xx) #:use-module (crates-io))

(define-public crate-is31fl32xx-0.1.0 (c (n "is31fl32xx") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "126xc4r8jqh194qf8lw2xm57r86vinr7fz4w05j8byc52ckw7rar") (f (quote (("default"))))))

