(define-module (crates-io is #{31}# is31fl3743b) #:use-module (crates-io))

(define-public crate-is31fl3743b-0.1.0 (c (n "is31fl3743b") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "1bfifnm1wlm18y2ygb3lsq18fvlmvd2bz9fd6vykpwq1p386np0k") (f (quote (("default" "heapless/x86-sync-pool"))))))

(define-public crate-is31fl3743b-0.1.1 (c (n "is31fl3743b") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (f (quote ("defmt-impl"))) (d #t) (k 0)))) (h "0m8pwi6g9ravxrqsv2iyknkmldy453g7xzr5blfa8blf7wv2jjrp")))

(define-public crate-is31fl3743b-0.1.2 (c (n "is31fl3743b") (v "0.1.2") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "heapless") (r "^0.7.14") (f (quote ("defmt-impl"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0mywmc77yjbszz5c06c6mivx2jzhxsib3hd0vgv46ywbp1a6za2a")))

(define-public crate-is31fl3743b-0.1.3 (c (n "is31fl3743b") (v "0.1.3") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (f (quote ("defmt-impl"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "05z4z2vmsla8777cmzani2yrwjp6dap81j5byxgabcrgny1fg19h")))

