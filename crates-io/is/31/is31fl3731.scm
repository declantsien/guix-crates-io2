(define-module (crates-io is #{31}# is31fl3731) #:use-module (crates-io))

(define-public crate-is31fl3731-0.0.1 (c (n "is31fl3731") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 2)))) (h "0rdp08is28bpgqfmlp9ld9jwbdzag6dckiysmdjg2d777kgk19qn") (f (quote (("scroll_phat_hd") ("rgb_matrix_5x5") ("matrix") ("led_shim") ("keybow_2040") ("charlie_wing") ("charlie_bonnet"))))))

(define-public crate-is31fl3731-0.0.2 (c (n "is31fl3731") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 2)))) (h "13vzp5d4rxyqjg3kzxgpwfqxgkcxjkxlq88v0cbv4r3d4pshxn3v") (f (quote (("scroll_phat_hd") ("rgb_matrix_5x5") ("matrix") ("led_shim") ("keybow_2040") ("charlie_wing") ("charlie_bonnet"))))))

(define-public crate-is31fl3731-0.0.3 (c (n "is31fl3731") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 2)))) (h "15kjnrfz9j010gxfc1kmdbgrizmbgfkwxq08bzc7vw3c1j1d9dk7") (f (quote (("scroll_phat_hd") ("rgb_matrix_5x5") ("matrix") ("led_shim") ("keybow_2040") ("charlie_wing") ("charlie_bonnet"))))))

(define-public crate-is31fl3731-0.0.4 (c (n "is31fl3731") (v "0.0.4") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 2)))) (h "17k3m3jaw26mawryn9yaym93aqzf688h0nic50jqmsd5bmz1bzxc") (f (quote (("scroll_phat_hd") ("rgb_matrix_5x5") ("matrix") ("led_shim") ("keybow_2040") ("charlie_wing") ("charlie_bonnet"))))))

(define-public crate-is31fl3731-1.0.0 (c (n "is31fl3731") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 2)))) (h "1p9qq195c6wig0fsx9zkviik89iis26dnifx3m56a5bpxw1q06mf") (f (quote (("scroll_phat_hd") ("rgb_matrix_5x5") ("matrix") ("led_shim") ("keybow_2040") ("charlie_wing") ("charlie_bonnet"))))))

(define-public crate-is31fl3731-1.0.1 (c (n "is31fl3731") (v "1.0.1") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 2)))) (h "0iigjg5yq67s9cr6h3vvmawgxvr2dv84yyf73an8q2xkiwzla7s9") (f (quote (("scroll_phat_hd") ("rgb_matrix_5x5") ("matrix") ("led_shim") ("keybow_2040") ("charlie_wing") ("charlie_bonnet"))))))

