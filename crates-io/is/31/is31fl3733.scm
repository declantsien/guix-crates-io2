(define-module (crates-io is #{31}# is31fl3733) #:use-module (crates-io))

(define-public crate-is31fl3733-0.5.0 (c (n "is31fl3733") (v "0.5.0") (d (list (d (n "diff-in-place") (r "^0.1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 2)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "lite-async-test") (r "^0.1.3") (d #t) (k 2)))) (h "1fqs25v0zrfbkw03klnppsgashwicqcgyrchmh6lq6pkh6y2z6a7")))

