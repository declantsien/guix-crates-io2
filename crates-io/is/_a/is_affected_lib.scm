(define-module (crates-io is _a is_affected_lib) #:use-module (crates-io))

(define-public crate-is_affected_lib-0.1.1 (c (n "is_affected_lib") (v "0.1.1") (d (list (d (n "git2") (r "^0.13.23") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rstest") (r "^0.11.0") (d #t) (k 2)))) (h "1a5sqnjg445irj7zviyfcyhv7vqdfj5w6i5gndmq8fgrwwjjbv3k")))

