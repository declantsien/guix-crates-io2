(define-module (crates-io is _a is_anagram) #:use-module (crates-io))

(define-public crate-is_anagram-0.1.0 (c (n "is_anagram") (v "0.1.0") (d (list (d (n "ascii") (r "^0.8.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "1g025bx7shbpca7dalf4731s8i6c0v2qcbmds50zj738lja3smz7")))

