(define-module (crates-io is _a is_affected) #:use-module (crates-io))

(define-public crate-is_affected-0.4.1 (c (n "is_affected") (v "0.4.1") (d (list (d (n "git2") (r "^0.13.23") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rstest") (r "^0.11.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0miybr0riddmak3rj4rgz9j75rwm4h9dkra2w2vhh6d8523bdl4h")))

(define-public crate-is_affected-0.4.2 (c (n "is_affected") (v "0.4.2") (d (list (d (n "git2") (r "^0.13.23") (k 0)) (d (n "is_affected_lib") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "00vw19vabp5v6dbsamy2v4gbzz6g5hd6f15z15w6rhz5lyhzg4sg")))

