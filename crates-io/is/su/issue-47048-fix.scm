(define-module (crates-io is su issue-47048-fix) #:use-module (crates-io))

(define-public crate-issue-47048-fix-1.0.0 (c (n "issue-47048-fix") (v "1.0.0") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(all(target_os = \"windows\", target_env = \"gnu\"))") (k 1)))) (h "17ikniachfbx5zxvmydyz3r4hlcsb0jsh388c396nz960hvdib2g") (y #t)))

(define-public crate-issue-47048-fix-0.1.0 (c (n "issue-47048-fix") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(all(target_os = \"windows\", target_env = \"gnu\"))") (k 0)))) (h "1r3s0clvb18knj0iyp6jwv50grzap5n3w33dx55ns0b78l23w0z4")))

(define-public crate-issue-47048-fix-0.1.1 (c (n "issue-47048-fix") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(all(target_os = \"windows\", target_env = \"gnu\"))") (k 0)))) (h "0xfpm8fbrr8w31d8phsv4y6afvklggfgjla6nnyl92crmc9b9cx0")))

(define-public crate-issue-47048-fix-0.1.2 (c (n "issue-47048-fix") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(all(target_os = \"windows\", target_env = \"gnu\"))") (k 0)) (d (n "version-sync") (r "^0.7") (d #t) (k 2)))) (h "00c0f7kxdsnsqlicwj1fv1ljl97gqgvlx8cvyj4c1b452maqbkha")))

(define-public crate-issue-47048-fix-0.1.3 (c (n "issue-47048-fix") (v "0.1.3") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(all(target_os = \"windows\", target_env = \"gnu\"))") (k 0)) (d (n "version-sync") (r "^0.7") (d #t) (k 2)))) (h "0pprwh106xks27aqprwk17xfvj6i4n19pdzvq787l65cbplz0fkc")))

