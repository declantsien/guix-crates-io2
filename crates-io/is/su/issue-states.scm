(define-module (crates-io is su issue-states) #:use-module (crates-io))

(define-public crate-issue-states-0.1.0 (c (n "issue-states") (v "0.1.0") (d (list (d (n "yaml-rust") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1nra6gfn8nk796ravdbp0k7jak855kzmrabpmc7aslwssmk9h9a9")))

(define-public crate-issue-states-0.2.0 (c (n "issue-states") (v "0.2.0") (d (list (d (n "yaml-rust") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0x331gwjwzrin9rv6i9s33hp3vc21yhvnb0ifzvrl9907jmapk6y")))

