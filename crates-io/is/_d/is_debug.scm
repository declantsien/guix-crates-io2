(define-module (crates-io is _d is_debug) #:use-module (crates-io))

(define-public crate-is_debug-1.0.0 (c (n "is_debug") (v "1.0.0") (h "0i2wxz2y37a98vkz41zx4aagcp6k6bh49kgys79n11fcp9lbd5m2")))

(define-public crate-is_debug-1.0.1 (c (n "is_debug") (v "1.0.1") (h "12bwspph88wgmzcyl8dg3s28gph41r9shfq8yzaj564xj7lril86")))

