(define-module (crates-io is -r is-real) #:use-module (crates-io))

(define-public crate-is-real-0.1.1 (c (n "is-real") (v "0.1.1") (h "0yid4w6ailm9i5brrzzpqnnnzfaswnvqhkai6c4cwy4g57qqnifj")))

(define-public crate-is-real-0.1.2 (c (n "is-real") (v "0.1.2") (h "14821cpglxbh02nzb3fx51l22mdwhbl10dxhq4d90b0dhhrxa5gx")))

(define-public crate-is-real-0.1.0 (c (n "is-real") (v "0.1.0") (h "00gg4v9zab5fm0f08j7ygf47zrjfbm35awrivgl2kqj909l5rz2k")))

