(define-module (crates-io is -r is-railway) #:use-module (crates-io))

(define-public crate-is-railway-1.0.0 (c (n "is-railway") (v "1.0.0") (h "0l82vab6nl3rip7my840wxbzqcywinqiwbwqlwp1b3fc2yc9j4cd")))

(define-public crate-is-railway-1.0.1 (c (n "is-railway") (v "1.0.1") (h "1vnlx5l5pfpcw95bb87wxg6ig438q8kypmzy1jhijbgns749hrln")))

