(define-module (crates-io is -r is-rtl) #:use-module (crates-io))

(define-public crate-is-rtl-0.1.0 (c (n "is-rtl") (v "0.1.0") (h "1h5r44vc1509y77dw1ch2qsfd42sf5qvzw4k9fhbssm0kz4f8ysp") (y #t)))

(define-public crate-is-rtl-0.1.1 (c (n "is-rtl") (v "0.1.1") (h "12l95a1qwz17yvm5zhiyvglka26iikas9x8i4f9ckc57zpyfmdr1")))

