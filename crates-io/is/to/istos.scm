(define-module (crates-io is to istos) #:use-module (crates-io))

(define-public crate-istos-0.0.1 (c (n "istos") (v "0.0.1") (h "0rginb30dy7w4nkjy2hnsc84lm39gldm9nwh7mb4281j6aqw6k5h")))

(define-public crate-istos-0.0.2 (c (n "istos") (v "0.0.2") (h "1ihspfvcxwi416zjy7s6zix0mgm7aapf5gfi4kf1xyrvk12yprxw")))

(define-public crate-istos-0.0.3 (c (n "istos") (v "0.0.3") (h "011r7q1j435dp8ycc8x0f90jphg0ysyg1p87nd4861n7pkqab7zb")))

