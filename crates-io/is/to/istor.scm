(define-module (crates-io is to istor) #:use-module (crates-io))

(define-public crate-istor-0.1.0 (c (n "istor") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ipaddress") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0hppis179c8ll4yfanhq0g9lh468ci1cq5sbqp9a46bn06nyjf62")))

(define-public crate-istor-0.1.1 (c (n "istor") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ipaddress") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "03lwxh4m84xp7z22gh7sh3x9dc75fbb39rs37x10m54ds339r3f2")))

(define-public crate-istor-0.1.2 (c (n "istor") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ipaddress") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0yc44szv09c5ynbpwfvws0csfzvmi81w7dc2g2w6xvwz6wbz4grb")))

(define-public crate-istor-0.2.0 (c (n "istor") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "16w2klm7pcmrl9kp3pg5ns3vyq87gw98zh8pk6f13vx4hgxdhmkk")))

(define-public crate-istor-0.2.1 (c (n "istor") (v "0.2.1") (d (list (d (n "minparse") (r "^0.1.2") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0452217wh6jzzws7zl4zk23jch4zcx09bdkgj21ghrpgkzid5ldp")))

