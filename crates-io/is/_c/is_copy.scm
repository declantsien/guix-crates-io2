(define-module (crates-io is _c is_copy) #:use-module (crates-io))

(define-public crate-is_copy-0.1.0 (c (n "is_copy") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0g0w8glih71f88158slf3hz4d632d6cw8yrxw3lbh1lsvvazkjcb")))

(define-public crate-is_copy-0.1.1 (c (n "is_copy") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0sr0d819vn1ssphy5cghf6gq84ccw3mfj0iva7vys7r7i6a67304")))

(define-public crate-is_copy-0.1.2 (c (n "is_copy") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1c75wdc0br0kc2p7j7c0g6symvdkgg1nbd79fgl22flk7k8660dw")))

(define-public crate-is_copy-0.1.3 (c (n "is_copy") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "19kl33l7wya1fhjbmzgnzdc6jplyz0brhiw7pzlv2jw9pxgkima0")))

(define-public crate-is_copy-0.1.4 (c (n "is_copy") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1l0l1m6lyp294nzfc8f9c9k7rgq6awxq0r9s33q9619rhffczr18")))

(define-public crate-is_copy-0.1.5 (c (n "is_copy") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0x26p0s3d8y4sgnj6qasvfri3zb3ffi8vf9m8dpcb989v12gs5n0")))

(define-public crate-is_copy-0.1.6 (c (n "is_copy") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dev_util") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1gcdx72di8rq860q5f4ahk3vl6yyr09lvw6by4zk80rgz4ggdrsb")))

