(define-module (crates-io is _c is_ci) #:use-module (crates-io))

(define-public crate-is_ci-0.0.0 (c (n "is_ci") (v "0.0.0") (h "1xa27pg02lgxrlg70n6lr0cvn93yixy60ww1mkm5nrlnsycxbr9h")))

(define-public crate-is_ci-1.0.0 (c (n "is_ci") (v "1.0.0") (h "0949lhxdlvaplsfi980ncs7z8dcafbmdxaf74q02rpa5vqzbpdpn")))

(define-public crate-is_ci-1.0.1 (c (n "is_ci") (v "1.0.1") (h "1i7v275g230f20gyrq3rkffgcmvv60bgl3zw2rbfzzp87k8b524r")))

(define-public crate-is_ci-1.1.0 (c (n "is_ci") (v "1.1.0") (h "10f6mqfscfxgr2zq478ijgx94bqfnggzajzgvr3cgn0mj5yb201m")))

(define-public crate-is_ci-1.1.1 (c (n "is_ci") (v "1.1.1") (h "1ywra2z56x6d4pc02zq24a4x7gvpixynh9524icbpchbf9ydwv31")))

(define-public crate-is_ci-1.2.0 (c (n "is_ci") (v "1.2.0") (h "0ifwvxmrsj4r29agfzr71bjq6y1bihkx38fbzafq5vl0jn1wjmbn")))

