(define-module (crates-io is _c is_close) #:use-module (crates-io))

(define-public crate-is_close-0.1.0 (c (n "is_close") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0q2x72zk06r2brvla080drbpq4g1z7d7gyn0p0l7swxn4ny4ia1r")))

(define-public crate-is_close-0.1.1 (c (n "is_close") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1cgdmx7cw283g0splif3l8994wl9s5y0pw624nkx6fdgbk9lcw9r")))

(define-public crate-is_close-0.1.2 (c (n "is_close") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0d5mgdy85kpdhcaawsfcxirp14hwzmkg64hfwhwaamad3bc5kig6")))

(define-public crate-is_close-0.1.3 (c (n "is_close") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "18m8g5ly4kn3m2svmcddliscm2rhfbmhjvyf1ldffwa8y40rxm64")))

