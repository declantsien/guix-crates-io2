(define-module (crates-io is _c is_chinese) #:use-module (crates-io))

(define-public crate-is_chinese-1.0.0 (c (n "is_chinese") (v "1.0.0") (h "1qkkpak11wjb23q44mh6gg05ycc9vbpz8j1bck1rgrvm1pbjcpl9")))

(define-public crate-is_chinese-1.0.1 (c (n "is_chinese") (v "1.0.1") (h "0br8xgziw6wjqz8dph1bcy65a7xys2xsypib3gggxcfb7fsv1d3f")))

(define-public crate-is_chinese-1.0.2 (c (n "is_chinese") (v "1.0.2") (h "0mf0fkxsc711bbldhz5hd9dr885wxar0b8ackql9c2bmlcy74a64")))

(define-public crate-is_chinese-1.0.3 (c (n "is_chinese") (v "1.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0n1kibf733hcsr5k709z8525l9ab32mgjp341d3mp3qb4b545hzi")))

