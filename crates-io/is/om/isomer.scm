(define-module (crates-io is om isomer) #:use-module (crates-io))

(define-public crate-isomer-0.0.0 (c (n "isomer") (v "0.0.0") (d (list (d (n "crossbeam-skiplist") (r "^0.1.1") (d #t) (k 0)) (d (n "dashu") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1ix88096piybfja5mim580m7h77bgfi9y8lpq1n44rw4mlaimmba") (f (quote (("default"))))))

