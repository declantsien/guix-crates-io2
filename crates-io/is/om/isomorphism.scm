(define-module (crates-io is om isomorphism) #:use-module (crates-io))

(define-public crate-isomorphism-0.1.0 (c (n "isomorphism") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.42") (o #t) (d #t) (k 0)))) (h "1dfz24071zm3hk435cp78mqxxfdjra62g8b1a1shrynanyzc6qyb")))

(define-public crate-isomorphism-0.1.1 (c (n "isomorphism") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.42") (o #t) (d #t) (k 0)))) (h "0gg08wa993n0vf2z2qfh2wzpfpv0g6v3361c4swfz2frf2gxpsnv")))

(define-public crate-isomorphism-0.1.2 (c (n "isomorphism") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.42") (o #t) (d #t) (k 0)))) (h "086zhr60n89h0163gxxd03pwbmzgq4jq8rq8035bj5fvmaf2bf2x")))

(define-public crate-isomorphism-0.1.3 (c (n "isomorphism") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.42") (o #t) (d #t) (k 0)))) (h "1sl8ap8rfr0fzyrf13m4yxk1hr9jkzf4y48lqia2qv5zm88mipkz")))

