(define-module (crates-io is om isomagic) #:use-module (crates-io))

(define-public crate-isomagic-0.1.0 (c (n "isomagic") (v "0.1.0") (d (list (d (n "dot_vox") (r "^3") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "image") (r "^0") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)) (d (n "structopt-derive") (r "^0") (d #t) (k 0)))) (h "0ryfgxwjv422c6ziqymmbjjmz07gp68am7j0rhsfdm3qpn8wjmiq")))

(define-public crate-isomagic-0.1.1 (c (n "isomagic") (v "0.1.1") (d (list (d (n "dot_vox") (r "^3") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "image") (r "^0") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)) (d (n "structopt-derive") (r "^0") (d #t) (k 0)))) (h "0c7gk25722qhsm0c9a7l385r7m4ncl5zb57b0d9xq10xazwydgvk")))

(define-public crate-isomagic-0.1.2 (c (n "isomagic") (v "0.1.2") (d (list (d (n "dot_vox") (r "^3") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "image") (r "^0") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)) (d (n "structopt-derive") (r "^0") (d #t) (k 0)))) (h "0jpb3mhbiay2dk299c0827a5x8xy26h16a8nwl3ab2i7z95yhg8r")))

(define-public crate-isomagic-0.1.3 (c (n "isomagic") (v "0.1.3") (d (list (d (n "dot_vox") (r "^3") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "image") (r "^0") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)) (d (n "structopt-derive") (r "^0") (d #t) (k 0)))) (h "01z4ifi133sfrw9kydf6b47glq9j418ds0s7s9a0n6fs97wh66rg")))

