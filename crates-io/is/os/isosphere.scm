(define-module (crates-io is os isosphere) #:use-module (crates-io))

(define-public crate-isosphere-0.1.0 (c (n "isosphere") (v "0.1.0") (d (list (d (n "claims") (r "^0.7.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rubedo") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.1.0") (d #t) (k 0)) (d (n "velcro") (r "^0.5.4") (d #t) (k 0)))) (h "1m1h1y75g86sl8h3416dls76hyf8x1is6wxdqrsm9ycaqdy66sk5") (f (quote (("reasons"))))))

(define-public crate-isosphere-0.1.1 (c (n "isosphere") (v "0.1.1") (d (list (d (n "claims") (r "^0.7.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rubedo") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "utoipa") (r "^4.2.0") (d #t) (k 0)) (d (n "velcro") (r "^0.5.4") (d #t) (k 0)))) (h "00pg4wqcbq7nmvlnlay0fn5njalpnsp00h1xbm8c9pqz7jr4c55y") (f (quote (("reasons"))))))

