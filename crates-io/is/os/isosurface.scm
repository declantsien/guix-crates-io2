(define-module (crates-io is os isosurface) #:use-module (crates-io))

(define-public crate-isosurface-0.0.1 (c (n "isosurface") (v "0.0.1") (d (list (d (n "cgmath") (r "^0.15.0") (d #t) (k 2)) (d (n "glium") (r "^0.18.0") (d #t) (k 2)) (d (n "num") (r "^0.1.40") (d #t) (k 2)))) (h "0vvwk3x9rkkx4j9j4zls1j1dv0xpwy8h1z0awx2f2srfnydkv7hv")))

(define-public crate-isosurface-0.0.2 (c (n "isosurface") (v "0.0.2") (d (list (d (n "cgmath") (r "^0.15.0") (d #t) (k 2)) (d (n "glium") (r "^0.18.0") (d #t) (k 2)) (d (n "num") (r "^0.1.40") (d #t) (k 2)))) (h "11bq5k619dy437gj83nsshh1l0w9phl186zwwzrzkak1386fkmmy")))

(define-public crate-isosurface-0.0.3 (c (n "isosurface") (v "0.0.3") (d (list (d (n "cgmath") (r "^0.15.0") (d #t) (k 2)) (d (n "glium") (r "^0.18.0") (d #t) (k 2)))) (h "15yrijhx9cs19gbrn9ryyib3m12d6d6fcxxns9nnkqzdamhk07vq")))

(define-public crate-isosurface-0.0.4 (c (n "isosurface") (v "0.0.4") (d (list (d (n "cgmath") (r "^0.15.0") (d #t) (k 2)) (d (n "glium") (r "^0.17.0") (d #t) (k 2)) (d (n "glium_text_rusttype") (r "^0.2.0") (d #t) (k 2)))) (h "0m6w441lws8wd6amprp5gachqi1h5syyv4vh0ppm5vchai9z9jm4")))

(define-public crate-isosurface-0.1.0-alpha.0 (c (n "isosurface") (v "0.1.0-alpha.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glium") (r "^0.26") (d #t) (k 2)) (d (n "glium_text_rusttype") (r "^0.3") (d #t) (k 2)))) (h "17mlnjsg03wppw29fcz7cndv37krpldnm1ays1a9s5qy85h4n3qg")))

