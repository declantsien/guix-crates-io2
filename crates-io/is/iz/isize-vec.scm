(define-module (crates-io is iz isize-vec) #:use-module (crates-io))

(define-public crate-isize-vec-0.1.0 (c (n "isize-vec") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1a86i6x54m42lkcydj9wfk5rnwx35jqqnf545yl19414895ldbcg")))

(define-public crate-isize-vec-0.1.1 (c (n "isize-vec") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "091y4z1llh2vhx40p9if3f295d5lk252fy2p5lx2rld5qdjsw11a")))

