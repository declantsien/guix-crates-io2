(define-module (crates-io is l- isl-rs) #:use-module (crates-io))

(define-public crate-isl-rs-0.1.0 (c (n "isl-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.133") (d #t) (k 0)))) (h "0samvz4hh8v42k7m2r2n4ji5zb34jqr9v9lxdqs04nc4v9iddpva") (y #t)))

(define-public crate-isl-rs-0.1.1 (c (n "isl-rs") (v "0.1.1") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)))) (h "1v8yysbvn5iw9v28908klq0gwfri011bc0xnsr6ri70b9d4x397h")))

(define-public crate-isl-rs-0.1.2 (c (n "isl-rs") (v "0.1.2") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)))) (h "0wzm71nsd3x6hrhadqpj436fr8a6vrmmd2n28m3ap67xz80rv01j")))

(define-public crate-isl-rs-0.1.3 (c (n "isl-rs") (v "0.1.3") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)))) (h "09iws6037b333b64947qibpa5czis29zbnprwgzx2j7zapih355x")))

