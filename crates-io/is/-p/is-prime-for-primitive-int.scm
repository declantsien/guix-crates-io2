(define-module (crates-io is -p is-prime-for-primitive-int) #:use-module (crates-io))

(define-public crate-is-prime-for-primitive-int-0.1.0 (c (n "is-prime-for-primitive-int") (v "0.1.0") (d (list (d (n "fast-modulo") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "0qgrs91aq6yqg6xy22i3j8rbymc2csabz1pipp89nsswip2a1cyh")))

(define-public crate-is-prime-for-primitive-int-0.2.0 (c (n "is-prime-for-primitive-int") (v "0.2.0") (d (list (d (n "fast-modulo") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "05hp9jlvj6rky8whrs9n49mlvbcwhlm5hl8l4q8zacwvsz12hwv9")))

(define-public crate-is-prime-for-primitive-int-0.3.0 (c (n "is-prime-for-primitive-int") (v "0.3.0") (d (list (d (n "fast-modulo") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "1frrv8fq2366g69svfnr7slg01zh2sxai1v90xp2ngf1kcaqcqqk")))

(define-public crate-is-prime-for-primitive-int-0.3.1 (c (n "is-prime-for-primitive-int") (v "0.3.1") (d (list (d (n "fast-modulo") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "11brb3vir99s28s4zjwrr2r3rv1w2cwnna97qkdlvfa07qashhli")))

(define-public crate-is-prime-for-primitive-int-0.4.0 (c (n "is-prime-for-primitive-int") (v "0.4.0") (d (list (d (n "fast-modulo") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "0afaqb5fz0j6sj6j6vmkmmrxfx6mwlbvv723ywixvp9qvm9597n6")))

(define-public crate-is-prime-for-primitive-int-0.5.0 (c (n "is-prime-for-primitive-int") (v "0.5.0") (d (list (d (n "fast-modulo") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "13way7msbi1fkg7vndi0sf48ddjr3l3jh3yscy7kp1ivikjhn8l2")))

(define-public crate-is-prime-for-primitive-int-0.5.1 (c (n "is-prime-for-primitive-int") (v "0.5.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "0k2jaj711yd84crc5vwqqm3f767mk6cjrjcrhsblhmqmw4ng1j45")))

