(define-module (crates-io is ot isotest) #:use-module (crates-io))

(define-public crate-isotest-0.1.0-dev.0 (c (n "isotest") (v "0.1.0-dev.0") (d (list (d (n "assert-panic") (r "^1") (d #t) (k 2)) (d (n "derive_more") (r "^0") (d #t) (k 2)))) (h "1ffmz5ggycdvwc32clc1c8v32bn8wmzphsm95m5gbzwfvhisnc3v")))

(define-public crate-isotest-0.1.0-dev.1 (c (n "isotest") (v "0.1.0-dev.1") (d (list (d (n "assert-panic") (r "^1") (d #t) (k 2)) (d (n "derive_more") (r "^0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 2)))) (h "172mpqpkgpnnicxwnw24fn7bjgq4pk7r5ycab1vk6v363gavzr09") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-isotest-0.1.0-dev.2 (c (n "isotest") (v "0.1.0-dev.2") (d (list (d (n "assert-panic") (r "^1") (d #t) (k 2)) (d (n "derive_more") (r "^0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 2)))) (h "012pi59ib7bbsdpkvyzp2f687dfash0skg8sskmf44v59f366p8q") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-isotest-0.1.0-dev.3 (c (n "isotest") (v "0.1.0-dev.3") (d (list (d (n "assert-panic") (r "^1") (d #t) (k 2)) (d (n "derive_more") (r "^0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 2)))) (h "0b12g3r6sc6lpki9gds2ld4xnr1y1hpkkwpm9kihyhdrjfsm0qmx") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-isotest-0.1.0-dev.4 (c (n "isotest") (v "0.1.0-dev.4") (d (list (d (n "assert-panic") (r "^1") (d #t) (k 2)) (d (n "derive_more") (r "^0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 2)))) (h "1pfrsindv87bfsxpyj87glrb34mn6yy28hrr1il0hrnlz5q85brm") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-isotest-0.1.0 (c (n "isotest") (v "0.1.0") (d (list (d (n "assert-panic") (r "^1") (d #t) (k 2)) (d (n "derive_more") (r "^0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 2)))) (h "1y5f6wkmciv105wmr9a5khaa0p78mmrldspl4753zzqyqz0b52l6") (f (quote (("default" "async") ("async" "futures"))))))

