(define-module (crates-io is ol isolate) #:use-module (crates-io))

(define-public crate-isolate-0.1.0 (c (n "isolate") (v "0.1.0") (h "0bz1rssm7ljc3xkg5v2qjvy48bk9bhxgm0j1sfs6iy4s1fim79gc")))

(define-public crate-isolate-0.1.1 (c (n "isolate") (v "0.1.1") (d (list (d (n "docopt") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "unshare") (r "^0.2.0") (d #t) (k 0)))) (h "0hm66jiyhhd5xc5rsq2mdzgflzky1h6bj2hjylnlihh6s3vgdm4n")))

(define-public crate-isolate-0.1.2 (c (n "isolate") (v "0.1.2") (d (list (d (n "docopt") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "unshare") (r "^0.2.0") (d #t) (k 0)))) (h "1np18q0zsr8phjpm94dy6v84cgabvf21zil85xx81kw598wr0g0b")))

