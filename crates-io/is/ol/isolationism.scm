(define-module (crates-io is ol isolationism) #:use-module (crates-io))

(define-public crate-isolationism-0.1.2 (c (n "isolationism") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "liquid") (r "^0.26.4") (d #t) (k 0)))) (h "1kpilsmppk2kadzd5agkk6qdrs6kdlljpcy8hz69zv7asqc20w4h")))

(define-public crate-isolationism-0.1.3 (c (n "isolationism") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "liquid") (r "^0.26.4") (d #t) (k 0)))) (h "1py1y749qfqv535xixax2hdjwwgrfqcnazri5946kprljgfd03nw")))

(define-public crate-isolationism-0.1.4 (c (n "isolationism") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "liquid") (r "^0.26.4") (d #t) (k 0)))) (h "16zssl2pkqd9wj7aymnavqpi1170flp384w1cfi6hbb5gwra1xvv")))

(define-public crate-isolationism-0.1.40 (c (n "isolationism") (v "0.1.40") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "liquid") (r "^0.26.4") (d #t) (k 0)))) (h "1vhfb6zdqi5caddx432rxmwi0v52zgvrfqjlwhyfid34zgd10d8v")))

(define-public crate-isolationism-0.1.41 (c (n "isolationism") (v "0.1.41") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "liquid") (r "^0.26.4") (d #t) (k 0)))) (h "1mvjlryp2ddjvk7l6jn6c5vk2ifrvfyp43glr7bf7qpj74xgj20m")))

(define-public crate-isolationism-0.1.42 (c (n "isolationism") (v "0.1.42") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "liquid") (r "^0.26.4") (d #t) (k 0)))) (h "1ad4xh77nf5fmf2zzr6gd5azgxwpwwj46d359l1y5wqzz175ywgz")))

(define-public crate-isolationism-0.1.69420 (c (n "isolationism") (v "0.1.69420") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "liquid") (r "^0.26.4") (d #t) (k 0)))) (h "1v9ic1w02xisx6hf9r93gvirg6f70daz5k007v0alri6aj2pi1dp")))

