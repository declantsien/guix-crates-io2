(define-module (crates-io is ol isolated) #:use-module (crates-io))

(define-public crate-isolated-0.1.0 (c (n "isolated") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.60") (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1fr0qwjghlxrk04asfacrx8phwacmgjr6xx15js38nckcaf467cd")))

(define-public crate-isolated-0.2.0 (c (n "isolated") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3.60") (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1v9yjshir1r9jmd581pgvx5kxwybcplsrirkv0d28ay1y1l5riaf")))

