(define-module (crates-io is ol isolanguage-1) #:use-module (crates-io))

(define-public crate-isolanguage-1-0.1.0 (c (n "isolanguage-1") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16rp70j3rp43yrwkr3xv2c97gm2kgra2hvmcraisdmgz7cikzrwq") (f (quote (("serde_support" "serde") ("default"))))))

(define-public crate-isolanguage-1-0.2.0 (c (n "isolanguage-1") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jpl80w3jqk3pn2hi8712nk9016c4gpfgg599219l7c8lp0qpz9a")))

(define-public crate-isolanguage-1-0.2.1 (c (n "isolanguage-1") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lxkf51bw6ay75akkr26q1mj4kxy1zdc4fpawp15249clavciqnq")))

(define-public crate-isolanguage-1-0.2.2 (c (n "isolanguage-1") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1a4ngaz0x0wy22y5l33g0w69qkzy1kmxsclmhqgcqjbxyb3a1qxn")))

