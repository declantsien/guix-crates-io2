(define-module (crates-io is ol isolation_forest) #:use-module (crates-io))

(define-public crate-isolation_forest-1.1.0 (c (n "isolation_forest") (v "1.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15pfpbqkv5j3dwhqwghl38c0gdwqzxcbj1gb9sv4mbvd5i3j22pb")))

