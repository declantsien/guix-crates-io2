(define-module (crates-io is th istherepie-hello_world) #:use-module (crates-io))

(define-public crate-istherepie-hello_world-0.1.0 (c (n "istherepie-hello_world") (v "0.1.0") (h "1nkgndf19kpchdw8j1pda2ailr1xqjhd54hagql1sa25gy8z8r5v") (y #t)))

(define-public crate-istherepie-hello_world-0.0.1 (c (n "istherepie-hello_world") (v "0.0.1") (h "0wlh87cm73ynpvcwj5xij8dm7frqk0rk3vhx96a4dhj61a4p9yma")))

