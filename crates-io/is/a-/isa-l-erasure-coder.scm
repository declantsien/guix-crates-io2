(define-module (crates-io is a- isa-l-erasure-coder) #:use-module (crates-io))

(define-public crate-isa-l-erasure-coder-0.1.0 (c (n "isa-l-erasure-coder") (v "0.1.0") (d (list (d (n "isa-l") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (k 2)) (d (n "rand_pcg") (r "^0.2") (k 2)))) (h "0fwdz8jh6y3jcmjrhnpf23hkncd7qpaxb77cy1hkvihxah8ixgsy")))

