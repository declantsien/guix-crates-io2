(define-module (crates-io is a- isa-l) #:use-module (crates-io))

(define-public crate-isa-l-0.1.0 (c (n "isa-l") (v "0.1.0") (d (list (d (n "libisal-sys") (r "^0.1") (d #t) (k 0)))) (h "19ggrsc7bq4y728qixmf1vmwrks0qv6wf013qzb96hkpw5v5jmyv")))

(define-public crate-isa-l-0.1.1 (c (n "isa-l") (v "0.1.1") (d (list (d (n "libisal-sys") (r "^0.1") (d #t) (k 0)))) (h "0j3dhn52m15dbnf1vh6gwmk0zbq9rkplbm4lxr8kyy5hirx7i324")))

(define-public crate-isa-l-0.1.2 (c (n "isa-l") (v "0.1.2") (d (list (d (n "libisal-sys") (r "^0.1") (d #t) (k 0)))) (h "0rqa1v0gbb909qb4f6riyzdcjdg8rb0c6jsph1yiqszib7885z0b")))

(define-public crate-isa-l-0.2.0 (c (n "isa-l") (v "0.2.0") (d (list (d (n "libisal-sys") (r "^0.1") (d #t) (k 0)))) (h "1yb7jwc3l0la5p9gbzb9i511z6d8vcwk411l3i946bd8a5fjclhf")))

