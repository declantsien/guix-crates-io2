(define-module (crates-io is _p is_proc_translated) #:use-module (crates-io))

(define-public crate-is_proc_translated-0.1.0 (c (n "is_proc_translated") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0n6gw76a54pp496g7ia5i0qsa8gzgyf1r3bnzj6l0a0csbkj1x8k")))

(define-public crate-is_proc_translated-0.1.1 (c (n "is_proc_translated") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(all(target_os = \"macos\", target_arch = \"x86_64\"))") (k 0)))) (h "0jkzwpsqk9afl87i4azibb6w4zdzpl7pjwpxw120sb2y3qk1f6y9")))

