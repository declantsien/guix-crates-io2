(define-module (crates-io is _p is_prime) #:use-module (crates-io))

(define-public crate-is_prime-0.1.0 (c (n "is_prime") (v "0.1.0") (d (list (d (n "ramp") (r "^0.3.3") (d #t) (k 0)))) (h "1ssklpsvr731b6a61dcvl8961zcmjnm9fkpmvnqzj5301xs8kb3d")))

(define-public crate-is_prime-0.1.1 (c (n "is_prime") (v "0.1.1") (d (list (d (n "ramp") (r "^0.3.3") (d #t) (k 0)))) (h "0jpl5p5rilvjq5wxzw70sygg8w2dpci1iagp67lgvb1a6n94267s")))

(define-public crate-is_prime-0.1.2 (c (n "is_prime") (v "0.1.2") (d (list (d (n "ramp") (r "^0.3.3") (d #t) (k 0)))) (h "0xlha1n09y9s1hj5mv01zjssqd3w271g5h0azacyx5a68aajsrjj")))

(define-public crate-is_prime-0.1.3 (c (n "is_prime") (v "0.1.3") (d (list (d (n "ramp") (r "^0.3.3") (d #t) (k 0)))) (h "0i7wpjbq70gmacf7ah3adz4bshdvh2rj7sxb9ar000j1ricq7kq8")))

(define-public crate-is_prime-1.0.2 (c (n "is_prime") (v "1.0.2") (d (list (d (n "ramp") (r "^0.3.3") (d #t) (k 0)))) (h "14hvdpk8har4sy0b3k2nxydzdvrpswrbjhjirh7rrqk9q9ma3jpn")))

(define-public crate-is_prime-1.0.4 (c (n "is_prime") (v "1.0.4") (d (list (d (n "ramp") (r "^0.5.0") (d #t) (k 0)))) (h "18fxhyq4fzm7z7pkf7464zixhcf70lb50cdqmq3lqd0k8qf5icsh")))

(define-public crate-is_prime-1.0.5 (c (n "is_prime") (v "1.0.5") (d (list (d (n "ramp") (r "^0.5.0") (d #t) (k 0)))) (h "04fnvajscssbc2k44agcbc9d03flmfl5nrbwjdj8pkycvmg8qdna")))

(define-public crate-is_prime-2.0.7 (c (n "is_prime") (v "2.0.7") (d (list (d (n "num-bigint") (r "=0.4") (d #t) (k 0)) (d (n "num-traits") (r "=0.2") (d #t) (k 0)))) (h "0xs5b9jyn17b4qlxcjnmrk72g32vjbbn7y947p5y3p63bn9igdn0")))

(define-public crate-is_prime-2.0.8 (c (n "is_prime") (v "2.0.8") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "07ks0q9kfy75b0p242qr2qbp2939a19cn4mrd39s83j84b67qzcm")))

(define-public crate-is_prime-2.0.9 (c (n "is_prime") (v "2.0.9") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "13w0pinlvvbr2sk7zxh575b7cipb51jbran40s6xwlw6hambifad")))

