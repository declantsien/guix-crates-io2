(define-module (crates-io is le islennyfree) #:use-module (crates-io))

(define-public crate-islennyfree-0.1.0 (c (n "islennyfree") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dioxus") (r "^0.3.2") (d #t) (k 0)) (d (n "dioxus-ssr") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1d6y3g4rc6fdyphang1xpcmfmny1awwq43536xfdn7wjg0rilbz6") (y #t)))

