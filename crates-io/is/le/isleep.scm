(define-module (crates-io is le isleep) #:use-module (crates-io))

(define-public crate-isleep-0.1.0 (c (n "isleep") (v "0.1.0") (d (list (d (n "spin_sleep") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "0ch98la3yg0sissqz6wapq66zd39vym053w7iyygd4bvifrsq79f") (s 2) (e (quote (("accuracy" "dep:spin_sleep"))))))

(define-public crate-isleep-0.2.0 (c (n "isleep") (v "0.2.0") (d (list (d (n "spin_sleep") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "0ihdmj296km8mcgaccxnava0jmah45rksmkjbg3yqsm252agn0p4") (s 2) (e (quote (("accuracy" "dep:spin_sleep"))))))

(define-public crate-isleep-0.2.1 (c (n "isleep") (v "0.2.1") (d (list (d (n "spin_sleep") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "1ixs0flzc200ahxblqj4333vggq9a461fbgcf9yhylkwfws1vh0n") (s 2) (e (quote (("accuracy" "dep:spin_sleep"))))))

(define-public crate-isleep-0.3.0 (c (n "isleep") (v "0.3.0") (d (list (d (n "spin_sleep") (r "^1") (o #t) (d #t) (k 0)))) (h "1ww2cc996x0y172ibdppv9m7dr9737s6vyy4r3z0r5as9srsp3a0") (s 2) (e (quote (("accuracy" "dep:spin_sleep"))))))

(define-public crate-isleep-0.3.1 (c (n "isleep") (v "0.3.1") (d (list (d (n "spin_sleep") (r "^1") (o #t) (d #t) (k 0)))) (h "1ifd8j5v5zapf12pmi6aqs5322zybz1gcd67xbid1nqq7nk5y9by") (s 2) (e (quote (("accuracy" "dep:spin_sleep"))))))

(define-public crate-isleep-0.3.2 (c (n "isleep") (v "0.3.2") (d (list (d (n "spin_sleep") (r "^1") (o #t) (d #t) (k 0)))) (h "0sf454r45zq0jkws36fyl2fxg4nxi492s2g5514ccrr1hfgkk178") (s 2) (e (quote (("accuracy" "dep:spin_sleep"))))))

