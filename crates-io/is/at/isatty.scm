(define-module (crates-io is at isatty) #:use-module (crates-io))

(define-public crate-isatty-0.1.0 (c (n "isatty") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1fc2s980w0k88hynrjh98f80mv1z85l8bizigb88ms1irwa3hvyc")))

(define-public crate-isatty-0.1.1 (c (n "isatty") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "035c85hcacy126l48xh4ds3c2cy53hk4ry6r29wnnh0fvi4aa23l")))

(define-public crate-isatty-0.1.2 (c (n "isatty") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "194zgr0z2akl8kssr51285b60xd5mm5j68rq0g7ln3r2b03vzjgf") (y #t)))

(define-public crate-isatty-0.1.3 (c (n "isatty") (v "0.1.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "03r49scl2c95f6zl112fgsfyviq97lmbwa921wmgx6m9f2vhsl7s")))

(define-public crate-isatty-0.1.4 (c (n "isatty") (v "0.1.4") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1adnzg18ihr1idkihlw0l59sskm8pkwqxd36gk8cirhg1ln7dh9x")))

(define-public crate-isatty-0.1.5 (c (n "isatty") (v "0.1.5") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1ilx1xrjb74bdqp9d23hs1c9bj6whl1bhmi5rvkswbksjhd31j80")))

(define-public crate-isatty-0.1.6 (c (n "isatty") (v "0.1.6") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "18m7g4fqyh2l0fbx8ibj7f168mifb1crsx7cbjcpdfy74qvj6alg")))

(define-public crate-isatty-0.1.7 (c (n "isatty") (v "0.1.7") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "164kw60vhswswrhx7k3ccj9ahbbfygn83d1br0jyz417lhxsa651")))

(define-public crate-isatty-0.1.8 (c (n "isatty") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "fileapi" "minwinbase" "minwindef" "processenv" "winbase" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "08n5lpq9cc610hl7ph9wy5wmaam30rkdqhwd02xdgm0cah9l6ckc")))

(define-public crate-isatty-0.1.9 (c (n "isatty") (v "0.1.9") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "fileapi" "minwinbase" "minwindef" "processenv" "winbase" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1p2yx591kwz2b3f2bv7al9hamhn052zmz9jd969rdv4kzj0q46p3")))

(define-public crate-isatty-0.2.0 (c (n "isatty") (v "0.2.0") (h "0ishrc71yjgvfni8qdk32nzjzgs6zd0v466n7gnyj6zhm5jiiv85")))

