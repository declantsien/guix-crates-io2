(define-module (crates-io is oc isocal) #:use-module (crates-io))

(define-public crate-isocal-0.1.0 (c (n "isocal") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "012q6l3hyc8vz8hpvm8rrc45nfpx3aq3ksizqmk5rnhxf247haf4")))

(define-public crate-isocal-0.1.1 (c (n "isocal") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "11dxgv9w5h9w136g47rjxs5a5rnq0kbw2l6wz9f1j735sb6da580")))

