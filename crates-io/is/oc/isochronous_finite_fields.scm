(define-module (crates-io is oc isochronous_finite_fields) #:use-module (crates-io))

(define-public crate-isochronous_finite_fields-1.0.0 (c (n "isochronous_finite_fields") (v "1.0.0") (h "05yjhs60m5gixc2rzamplww1q9fcfy3y7vlp1l24zra57wshxlcn")))

(define-public crate-isochronous_finite_fields-1.0.1 (c (n "isochronous_finite_fields") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "1vml8a4bziwz1y2bqb0hkczkzj1arv3jw42p4b8fxd9zf550yhwq")))

