(define-module (crates-io sd f- sdf-row-guest) #:use-module (crates-io))

(define-public crate-sdf-row-guest-0.1.0 (c (n "sdf-row-guest") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.24.0") (f (quote ("macros"))) (k 0)))) (h "0fhy0zyif1bw2q5a2gs5dn32hg9596f7ibqcdc4jnxhda009lydl")))

(define-public crate-sdf-row-guest-0.1.1 (c (n "sdf-row-guest") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.24.0") (f (quote ("macros"))) (k 0)))) (h "1hbwf79wv0w1w2lr1xql5hj5wv5v0b1v33lzy8d37in789wq6c43")))

(define-public crate-sdf-row-guest-0.1.2 (c (n "sdf-row-guest") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.24.0") (f (quote ("macros"))) (k 0)))) (h "1an3vh8jn7pq3yk6la9h20nzlzcy4f1xm0506n6ks7i1y0zzi6ny")))

