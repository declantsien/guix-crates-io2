(define-module (crates-io sd f- sdf-df-guest) #:use-module (crates-io))

(define-public crate-sdf-df-guest-0.1.0 (c (n "sdf-df-guest") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "polars") (r "^0.39.2") (f (quote ("lazy" "fmt_no_tty" "temporal"))) (k 0)) (d (n "polars-arrow") (r "^0.39.2") (f (quote ("arrow_rs"))) (k 0)) (d (n "wit-bindgen") (r "^0.24.0") (f (quote ("macros"))) (k 0)))) (h "1ymi8g33syjvhf9w7gdfrnzfqmy3xmvcap2979wlhcbjya87abd6")))

(define-public crate-sdf-df-guest-0.1.1 (c (n "sdf-df-guest") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "polars") (r "^0.39.2") (f (quote ("lazy" "fmt_no_tty" "temporal"))) (k 0)) (d (n "polars-arrow") (r "^0.39.2") (f (quote ("arrow_rs"))) (k 0)) (d (n "wit-bindgen") (r "^0.24.0") (f (quote ("macros"))) (k 0)))) (h "1n30g1h5daf6jjzd2j3j42mp3vfwdysl716g33yrynsalikkkdak")))

(define-public crate-sdf-df-guest-0.2.0 (c (n "sdf-df-guest") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "polars") (r "^0.39.2") (f (quote ("lazy" "fmt_no_tty" "temporal"))) (k 0)) (d (n "polars-arrow") (r "^0.39.2") (f (quote ("arrow_rs"))) (k 0)) (d (n "wit-bindgen") (r "^0.24.0") (f (quote ("macros"))) (k 0)))) (h "0y1mcdvc2qm9cs0j7l69rcgsfzgkbwjx36vqxj1lbq07793l0rjd")))

(define-public crate-sdf-df-guest-0.2.1 (c (n "sdf-df-guest") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "polars") (r "^0.39.2") (f (quote ("lazy" "fmt_no_tty" "temporal" "dtype-i8" "dtype-i16" "dtype-u8" "dtype-u16"))) (k 0)) (d (n "polars-arrow") (r "^0.39.2") (f (quote ("arrow_rs"))) (k 0)) (d (n "wit-bindgen") (r "^0.24.0") (f (quote ("macros"))) (k 0)))) (h "1s0sa8rifjh23qasr5i350q5i0whspqnq8clgf58n16fb7xh3dcb")))

