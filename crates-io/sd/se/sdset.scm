(define-module (crates-io sd se sdset) #:use-module (crates-io))

(define-public crate-sdset-0.1.0 (c (n "sdset") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0k58fzdfdfalgnz6sfa9bkgjbcyx18v264ds68s3b38b1xli0s8l")))

(define-public crate-sdset-0.1.1 (c (n "sdset") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1diq7yk71pyam6gmqylrp7icjjdai9x2a4dj8v49jbwwcravgd30")))

(define-public crate-sdset-0.1.2 (c (n "sdset") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "10dlngz336y3vdbmj4papax0p2sg5ykxahq2vsm0ybgris8sgxhj") (f (quote (("unstable"))))))

(define-public crate-sdset-0.2.0 (c (n "sdset") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1ha8nqhpv6v20fc4m52zgaqm32bwprwm4300mjasw21y00xljall") (f (quote (("unstable"))))))

(define-public crate-sdset-0.2.1 (c (n "sdset") (v "0.2.1") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0d70yraayx9wm5mn57ici0i1dg3f87x3z5c8m45x37z9z4798zwm") (f (quote (("unstable"))))))

(define-public crate-sdset-0.2.2 (c (n "sdset") (v "0.2.2") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0pbvv7nv28rw8fhzfghhmcfj67x4cmpz05126fl3ys5q6lq55dlx") (f (quote (("unstable"))))))

(define-public crate-sdset-0.2.3 (c (n "sdset") (v "0.2.3") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0hrq4qpijc8dd5yirs3q16y6r7azdm6kmgimgdvlv71sm8kdf6nm") (f (quote (("unstable"))))))

(define-public crate-sdset-0.2.4 (c (n "sdset") (v "0.2.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1s7d923qs1p7695ralq9jirpmias1z5nc2w5ksvjprz3slvg435b") (f (quote (("unstable"))))))

(define-public crate-sdset-0.2.5 (c (n "sdset") (v "0.2.5") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0kcvladf5kll7qg8g27jc0s10ihr8wpyi9akdi4srpapb7z5v66v") (f (quote (("unstable"))))))

(define-public crate-sdset-0.2.6 (c (n "sdset") (v "0.2.6") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "05gk4cz18fvpnnd67m9yibkaw04mih1fcrnph6p192hvf4xq5259") (f (quote (("unstable"))))))

(define-public crate-sdset-0.3.0 (c (n "sdset") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1if2gkq3l73lqc26s7k6aimjs780q2iw66d6h3rbyz5185rrm5f6") (f (quote (("unstable"))))))

(define-public crate-sdset-0.3.1 (c (n "sdset") (v "0.3.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fxc7ifvl8gg0974bzc9zc9aj4hqqgq3vxvwlwmgiyrck3j90s47") (f (quote (("unstable"))))))

(define-public crate-sdset-0.3.2 (c (n "sdset") (v "0.3.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zzbygzwhz100c7icqjmz857hl7qhmfib9rnfdvgfqr46xfbvlxy") (f (quote (("unstable"))))))

(define-public crate-sdset-0.3.3 (c (n "sdset") (v "0.3.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1581m43bj1fkyfrdjicp5ng9i4pwy70dk25gcxnpphyn8dvl9lmn") (f (quote (("unstable"))))))

(define-public crate-sdset-0.3.4 (c (n "sdset") (v "0.3.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pyhsvqcb36g9m4sdm4hnrmwgyq6l5dj4kjvn1jwhwf6nn46mk38") (f (quote (("unstable"))))))

(define-public crate-sdset-0.3.5 (c (n "sdset") (v "0.3.5") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hnjxc5wmy62wf329cg7rc68glxmkcmblxkqs2aw7cpndpnv8rin") (f (quote (("unstable"))))))

(define-public crate-sdset-0.3.6 (c (n "sdset") (v "0.3.6") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jb39hyiwxlwkbb5xmb059prn36nhypvn3xlcg2r7rna5fmpmzav") (f (quote (("unstable"))))))

(define-public crate-sdset-0.4.0 (c (n "sdset") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fnrfw18dhlrl7k6ng6h8vri8yq28fwwg6y8fqhpjmw5b3h1zcnb") (f (quote (("unstable"))))))

