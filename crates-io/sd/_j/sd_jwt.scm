(define-module (crates-io sd _j sd_jwt) #:use-module (crates-io))

(define-public crate-sd_jwt-0.0.1 (c (n "sd_jwt") (v "0.0.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "base64-url") (r "^1.4.13") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.2") (d #t) (k 0)) (d (n "josekit") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06v246gblkpmpwrjbdplsfgdj3his7rvk00lqmmx5265wibwb5m1")))

(define-public crate-sd_jwt-0.0.2 (c (n "sd_jwt") (v "0.0.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "base64-url") (r "^1.4.13") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.2") (d #t) (k 0)) (d (n "josekit") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1q9070vxz5hfcmipj2kki7d801rfmzkxz1bzwkyjigvsxkgrgvyr")))

