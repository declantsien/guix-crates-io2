(define-module (crates-io sd sb sdsb) #:use-module (crates-io))

(define-public crate-sdsb-0.0.1 (c (n "sdsb") (v "0.0.1") (d (list (d (n "chain-trans") (r "^1") (d #t) (k 0)) (d (n "serde") (r ">1.0.184") (f (quote ("derive" "std" "alloc"))) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1zwfjavb10pk6yavfka445vhcyia0rg7b6xdabhzw3gdn5jz4iw8")))

