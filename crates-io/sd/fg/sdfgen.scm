(define-module (crates-io sd fg sdfgen) #:use-module (crates-io))

(define-public crate-sdfgen-0.5.1 (c (n "sdfgen") (v "0.5.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)))) (h "037lf5yg9ldn3q6snaq6phd2pdn5x0dl3bbqjhr3lmblz8vh56mg")))

(define-public crate-sdfgen-0.6.0 (c (n "sdfgen") (v "0.6.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)))) (h "09z452i8x4fngc800chihpzi853i96rl5kcfp617lmhgb02825ls")))

(define-public crate-sdfgen-0.6.1 (c (n "sdfgen") (v "0.6.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)))) (h "0f8dz7wv34sj9smi71mj3x6b571wwhiv7gvv8kjm7y70z5f1h71m")))

(define-public crate-sdfgen-0.6.2 (c (n "sdfgen") (v "0.6.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)))) (h "0dcik68av7429hlvg89s8msclp577p33n51wxh3d660hbk6rp8nc")))

(define-public crate-sdfgen-0.6.3 (c (n "sdfgen") (v "0.6.3") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)))) (h "1fk47drk2910m53a0c37lgwfipk1gm43qbviq17qqf6i8ywzw8a9")))

