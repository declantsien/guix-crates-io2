(define-module (crates-io sd mm sdmmc-spi) #:use-module (crates-io))

(define-public crate-sdmmc-spi-0.1.0 (c (n "sdmmc-spi") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "defmt") (r "^0.3.2") (d #t) (k 0)) (d (n "diskio") (r "^0.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "size") (r "^0.4.1") (k 0)) (d (n "switch-hal") (r "^0.4.0") (d #t) (k 0)))) (h "1bad74abwf4zxndbfzvd9l98fvkv6pfyzcjyap2f4lbw6j9svq9v")))

(define-public crate-sdmmc-spi-0.1.1 (c (n "sdmmc-spi") (v "0.1.1") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "defmt") (r "^0.3.2") (d #t) (k 0)) (d (n "diskio") (r "^0.1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "size") (r "^0.4.1") (k 0)) (d (n "switch-hal") (r "^0.4.0") (d #t) (k 0)))) (h "0cbi2h81q6xgsabhp23mpv5l10rxnaq6yv0kmlvzr7d9hlmhbqvz")))

