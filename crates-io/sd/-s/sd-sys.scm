(define-module (crates-io sd -s sd-sys) #:use-module (crates-io))

(define-public crate-sd-sys-0.1.0 (c (n "sd-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ipmhldbg59yy61ik6nwgjzicm221b706p8vrzfirivra477haxh") (l "systemd")))

(define-public crate-sd-sys-0.1.1 (c (n "sd-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0vaklaf56fzc66m5ya7fkq7pz1dlsy1marg1hi001xfbc81md3d2") (l "systemd")))

(define-public crate-sd-sys-1.0.0 (c (n "sd-sys") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1k293nd3pczh5l5y8anclrdi07m9axs2hla485bajx0gvvr71y5w") (l "systemd")))

