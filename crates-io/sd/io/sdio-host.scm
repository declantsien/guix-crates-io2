(define-module (crates-io sd io sdio-host) #:use-module (crates-io))

(define-public crate-sdio-host-0.1.0 (c (n "sdio-host") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)))) (h "0pgwdfiyzl2sdkkcscfvvrq98s6wbb1gcqg38nlwgmsppv484i40")))

(define-public crate-sdio-host-0.2.0 (c (n "sdio-host") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)))) (h "08295ygmkl62vxh0ggwhnw8fivm6vgq6g3nj2m4677k1ikicj824")))

(define-public crate-sdio-host-0.3.0 (c (n "sdio-host") (v "0.3.0") (h "038nkynayp1cqyqhxgw4a1z6lb4y984h7kabhl4ijvqmp6m0arr0")))

(define-public crate-sdio-host-0.4.0 (c (n "sdio-host") (v "0.4.0") (h "1y6s0npmib4ap2h6wvl7f0wg0sb7syyjyank0739w13461y5igik")))

(define-public crate-sdio-host-0.5.0 (c (n "sdio-host") (v "0.5.0") (h "0npq6xqx8nnr485kgl2s7gd27a2l3b8lgklf6a63hk7ykigh4g7r")))

(define-public crate-sdio-host-0.6.0 (c (n "sdio-host") (v "0.6.0") (h "02cwxpp41ha6z3ggyc1jkcs1m4syfp83fjw256m381md00dl2p1m")))

(define-public crate-sdio-host-0.7.0 (c (n "sdio-host") (v "0.7.0") (h "05223kzavvk3rigbd07mbmpd13dqd83cbi4y9f068ay288gq42sv")))

(define-public crate-sdio-host-0.8.0 (c (n "sdio-host") (v "0.8.0") (h "1j44z53bllq7a570klph44lgmkaajl6rx5v99b8r8j4xh9620rpv")))

(define-public crate-sdio-host-0.9.0 (c (n "sdio-host") (v "0.9.0") (h "1qkf5ln8vrn5y1a57ysc0k6msici6sb3lp7mnxawvv0fjp5y4a5k")))

