(define-module (crates-io sd io sdio_sdhc) #:use-module (crates-io))

(define-public crate-sdio_sdhc-0.1.0 (c (n "sdio_sdhc") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.8") (f (quote ("stm32f407"))) (d #t) (k 0)))) (h "1w188dz421vgvjhlgvdjc7qmz1zpz1wiv97rdca3n8p8iirqnmjr") (y #t)))

(define-public crate-sdio_sdhc-0.1.1 (c (n "sdio_sdhc") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.8") (f (quote ("stm32f407"))) (d #t) (k 0)))) (h "1fiv27wn1x8s12nvmx4cnnzgp69j2vha59da819ifhmn0hbcyk16") (y #t)))

(define-public crate-sdio_sdhc-0.1.2 (c (n "sdio_sdhc") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.8") (f (quote ("stm32f407"))) (d #t) (k 0)))) (h "170c05n93h6l1jpwqpvqhj7rq9xs8iby3xb2w9dypn7rmfwag83y")))

(define-public crate-sdio_sdhc-0.2.0 (c (n "sdio_sdhc") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "fat32") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.8") (f (quote ("stm32f407"))) (d #t) (k 0)))) (h "0cvkr5nlhz88x5j60hhj47g87bakca959md87a4w1dlc2np8hkpd") (f (quote (("filesystem" "fat32") ("default"))))))

(define-public crate-sdio_sdhc-0.2.1 (c (n "sdio_sdhc") (v "0.2.1") (d (list (d (n "block_device") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.8") (f (quote ("stm32f407"))) (d #t) (k 0)))) (h "06ih3zljxfwnpyjwpnk04j67aj7kzb8lp22b6mpdr86b1z12p2cp") (f (quote (("filesystem" "block_device") ("default"))))))

(define-public crate-sdio_sdhc-0.2.2 (c (n "sdio_sdhc") (v "0.2.2") (d (list (d (n "block_device") (r "=0.1.0") (o #t) (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.8") (f (quote ("stm32f407"))) (d #t) (k 0)))) (h "152md40wz0rg3zfjj9im6payh4hsjiwajy4nc8ryx0q82gjdk2hz") (f (quote (("filesystem" "block_device") ("default"))))))

(define-public crate-sdio_sdhc-0.2.3 (c (n "sdio_sdhc") (v "0.2.3") (d (list (d (n "block_device") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.8") (f (quote ("stm32f407"))) (d #t) (k 0)))) (h "0j4gxsy2b8m57ixhnrqz8bi9jdcaj3i616kiv12j8z587y47mi18") (f (quote (("filesystem" "block_device") ("default"))))))

