(define-module (crates-io sd c- sdc-parser) #:use-module (crates-io))

(define-public crate-sdc-parser-0.1.0 (c (n "sdc-parser") (v "0.1.0") (d (list (d (n "combine") (r "^3.8.1") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "0nwvh9110c34yypw9xa9rb4a3rnfr7px2q87hzznnwdhb6522c9b")))

(define-public crate-sdc-parser-0.1.1 (c (n "sdc-parser") (v "0.1.1") (d (list (d (n "combine") (r "^3.8.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "1210gaancfwkcmkmgd4kizs33yqcafyzaclmdasxrflvyggvjbnv")))

