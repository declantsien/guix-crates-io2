(define-module (crates-io sd #{-i}# sd-id128) #:use-module (crates-io))

(define-public crate-sd-id128-0.1.0 (c (n "sd-id128") (v "0.1.0") (d (list (d (n "sd-sys") (r "^0.1") (d #t) (k 0)))) (h "0j2ddkcqma7bpzk2k7yzkbhmn1zymfql5dpqf75fy5dpcfnqncs4")))

(define-public crate-sd-id128-0.1.1 (c (n "sd-id128") (v "0.1.1") (d (list (d (n "sd-sys") (r "^0.1") (d #t) (k 0)))) (h "0hrdm0520l9rcdj9d1raw3wh3rddhvk8j8yjnhl0w9ig00ajx1z0")))

(define-public crate-sd-id128-0.1.2 (c (n "sd-id128") (v "0.1.2") (d (list (d (n "sd-sys") (r "^0.1") (d #t) (k 0)))) (h "1a7lx9g8xfgnlwf9lc1cvy4is96ymslmqlmdfcq40hy870a62clq")))

(define-public crate-sd-id128-1.0.0 (c (n "sd-id128") (v "1.0.0") (d (list (d (n "sd-sys") (r "^0.1") (d #t) (k 0)))) (h "08d19g81r9gwwca65bs6v8mrb49plb73kksdfjjpskhw5ysh1nk7") (f (quote (("default" "240") ("240") ("233"))))))

(define-public crate-sd-id128-1.0.1 (c (n "sd-id128") (v "1.0.1") (d (list (d (n "sd-sys") (r "^1.0") (d #t) (k 0)))) (h "0gk5i8jgm46rqzrs2rbhyg3jaz3k25gi4hb13c095jnhmia63h06") (f (quote (("default" "240") ("240") ("233"))))))

