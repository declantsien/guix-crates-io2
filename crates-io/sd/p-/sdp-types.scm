(define-module (crates-io sd p- sdp-types) #:use-module (crates-io))

(define-public crate-sdp-types-0.0.0 (c (n "sdp-types") (v "0.0.0") (h "0gj42d2cnkjff33q6zl4z04lwra593j34xv55qh2vqsmd4sqlkhj")))

(define-public crate-sdp-types-0.1.0 (c (n "sdp-types") (v "0.1.0") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)))) (h "150rk32a523x1f9mr73b42fkbqssjqnla0fxr8908cyi0nkgyisd")))

(define-public crate-sdp-types-0.1.1 (c (n "sdp-types") (v "0.1.1") (d (list (d (n "bstr") (r "^0.2.11") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)))) (h "0lbiplarr9zkxfnklcl7h630svyad12pjw57qjznhnjm6zbizz51")))

(define-public crate-sdp-types-0.1.2 (c (n "sdp-types") (v "0.1.2") (d (list (d (n "bstr") (r "^0.2.11") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)))) (h "0z1l27zw9xrj43qv51rrhcw1q7s4yak2f51kgqfyh28hn7x7ji0f")))

(define-public crate-sdp-types-0.1.3 (c (n "sdp-types") (v "0.1.3") (d (list (d (n "bstr") (r "^0.2.11") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)))) (h "1pa247sqqn0kmd5dx2ff4dgx2748l5kqyp1mv3mjdw6ghrl9yjdf")))

(define-public crate-sdp-types-0.1.4 (c (n "sdp-types") (v "0.1.4") (d (list (d (n "bstr") (r "^0.2.11") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)))) (h "1faadnasm47j396b7zxn6b1ymdx3xkb912x2i3pib7xhwf4p4qhs")))

(define-public crate-sdp-types-0.1.5 (c (n "sdp-types") (v "0.1.5") (d (list (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)))) (h "0vns0hibdc2hp6bnpxx0f407asagxhdch2mmyy0x08p255w4knzq")))

(define-public crate-sdp-types-0.1.6 (c (n "sdp-types") (v "0.1.6") (d (list (d (n "bstr") (r "^1") (k 0)) (d (n "fallible-iterator") (r "^0.3") (d #t) (k 0)))) (h "11phi46999bznp4bzkdql0k9s65vrdsfvxxm64dc49520xxy3h9v") (r "1.65")))

