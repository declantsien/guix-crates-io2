(define-module (crates-io sd p- sdp-rs) #:use-module (crates-io))

(define-public crate-sdp-rs-0.0.1 (c (n "sdp-rs") (v "0.0.1") (d (list (d (n "nom") (r "^7.1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "08s27adw6ldp9ici602rs87mvrn14djwg0s2bp44vwwh4dmkqb59")))

(define-public crate-sdp-rs-0.2.1 (c (n "sdp-rs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "vec1") (r "^1.8.0") (d #t) (k 0)))) (h "12asflxibf47d2jd0jq9yi2v0s5pqvhissd9qp86sl8g4yvd2qq6")))

