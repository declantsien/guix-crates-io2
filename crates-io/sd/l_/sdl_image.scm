(define-module (crates-io sd l_ sdl_image) #:use-module (crates-io))

(define-public crate-sdl_image-0.3.6 (c (n "sdl_image") (v "0.3.6") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl") (r "^0.3.6") (d #t) (k 0)))) (h "1lh1qiv7k76qha4f6l1yl5waqy2m2rmm109jkgdkniv1jhvdv246")))

(define-public crate-sdl_image-0.3.7 (c (n "sdl_image") (v "0.3.7") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl1_2-rs") (r "^0.3.7") (d #t) (k 0)))) (h "0s3add4lj8l3d2kl55zbf5javzff9cdqqgshg7z7xslspicg9c23")))

(define-public crate-sdl_image-0.3.8 (c (n "sdl_image") (v "0.3.8") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl1_2-rs") (r "^0.3.7") (d #t) (k 0)))) (h "1p3da0hsqyhapfb4j2blvay9a14fxpqgg7khcqgf2m82z06kz8h9")))

