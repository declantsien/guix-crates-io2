(define-module (crates-io sd l_ sdl_64) #:use-module (crates-io))

(define-public crate-sdl_64-0.1.0 (c (n "sdl_64") (v "0.1.0") (d (list (d (n "gfx_64") (r "^0.1.0") (d #t) (k 0)) (d (n "log_64") (r "^0.1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.60") (k 1)))) (h "0cvk8l9zfzc41nk0fylypxa557l4sqsyhli3gpq4c4r8wkzcxj56")))

