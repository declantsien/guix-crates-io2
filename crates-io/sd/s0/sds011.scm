(define-module (crates-io sd s0 sds011) #:use-module (crates-io))

(define-public crate-sds011-0.1.0 (c (n "sds011") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "17gg2rbj8sgldlk5j099h58k5kd0pzkn8xxp4lkdkw1ab27hlgl7")))

(define-public crate-sds011-0.1.1 (c (n "sds011") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "1wi1rsp2mgasprk2fdlsg7iwlxpv6ybvzcp7i9z4mvd1bwpq9h62")))

(define-public crate-sds011-0.1.2 (c (n "sds011") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "1da8wgsrix8r7kf00jcdrpgm3jxj11bsfn9syk483q9yhgkwdrv8")))

(define-public crate-sds011-0.1.3 (c (n "sds011") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "1mspw9wvk7a68h9aikcw58n5pfd3mbaazjasgni3ald67hcjd33g")))

(define-public crate-sds011-0.1.4 (c (n "sds011") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "0rplb5dg1d8phg42z32n8pmbi0mrzmixdhb17vk63ybkgallmjk4")))

(define-public crate-sds011-0.2.0 (c (n "sds011") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "1aji610n8zvn5cdgzcqm30cy85x5gyh4ymg4flhk65xf92xyyxxb")))

(define-public crate-sds011-0.2.1 (c (n "sds011") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (k 0)))) (h "0zysapwqzdakxyailnd0m32q196pbcrn03qrpxmzgdv3y0x42ih5")))

