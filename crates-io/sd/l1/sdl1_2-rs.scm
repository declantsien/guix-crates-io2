(define-module (crates-io sd l1 sdl1_2-rs) #:use-module (crates-io))

(define-public crate-sdl1_2-rs-0.3.7 (c (n "sdl1_2-rs") (v "0.3.7") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1.24") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "064zmwsy9qwp6fs8br6d80k1qqyxcgzw1m0as9i1am6ksvfjc9zv")))

