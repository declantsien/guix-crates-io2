(define-module (crates-io sd cx sdcx) #:use-module (crates-io))

(define-public crate-sdcx-0.1.0 (c (n "sdcx") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "parol") (r "^0.25.0") (d #t) (k 1)) (d (n "parol_runtime") (r "^0.20.0") (f (quote ("auto_generation"))) (d #t) (k 0)) (d (n "parol_runtime") (r "^0.20.0") (f (quote ("auto_generation"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 1)))) (h "16jiqxfja5y9j0f6b082y2lp2hijsp6m090dw3dd21fcskh3q9y2")))

