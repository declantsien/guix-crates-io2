(define-module (crates-io sd fe sdfer) #:use-module (crates-io))

(define-public crate-sdfer-0.1.0 (c (n "sdfer") (v "0.1.0") (h "0jzbildx1p2knwj6jh40c9v3ssrgddq54kgk2nhabyxnmpmx17c5")))

(define-public crate-sdfer-0.2.0 (c (n "sdfer") (v "0.2.0") (d (list (d (n "image") (r "^0.24.7") (o #t) (d #t) (k 0)))) (h "1nfk44za4qmqnqnbp8kyzvna98p73764rsmzbg55qshy3bkkrkws")))

(define-public crate-sdfer-0.2.1 (c (n "sdfer") (v "0.2.1") (d (list (d (n "image") (r "^0.24.7") (o #t) (d #t) (k 0)))) (h "1va0q962zpplmw38x9mfz9fvx2qhxcmxsz1c406sf8f7qzmpbz97")))

