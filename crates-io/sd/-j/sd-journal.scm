(define-module (crates-io sd -j sd-journal) #:use-module (crates-io))

(define-public crate-sd-journal-0.1.0 (c (n "sd-journal") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sd-id128") (r "^0.1") (d #t) (k 0)) (d (n "sd-sys") (r "^0.1") (d #t) (k 0)))) (h "133pvxaws004b6f3gkic69n4h5y1kmjvj8v09x5b09waa4xa40m4") (f (quote (("td_chrono" "chrono") ("experimental") ("default" "experimental" "td_chrono"))))))

