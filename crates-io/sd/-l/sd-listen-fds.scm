(define-module (crates-io sd -l sd-listen-fds) #:use-module (crates-io))

(define-public crate-sd-listen-fds-0.1.0 (c (n "sd-listen-fds") (v "0.1.0") (h "198050y4qglcagyhwfiz1nwr1iqlvkdjz2fq6mpmvzrqs0c30kfh")))

(define-public crate-sd-listen-fds-0.2.0 (c (n "sd-listen-fds") (v "0.2.0") (h "0gwvsdcg1qw3qa831yq8k7f1fa8msbhhrm0gapzsy6wwm52ivdcb")))

