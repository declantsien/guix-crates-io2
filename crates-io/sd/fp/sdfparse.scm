(define-module (crates-io sd fp sdfparse) #:use-module (crates-io))

(define-public crate-sdfparse-0.1.0 (c (n "sdfparse") (v "0.1.0") (d (list (d (n "clilog") (r "^0.2.3") (d #t) (k 0)) (d (n "compact_str") (r "^0.6.1") (d #t) (k 0)) (d (n "parsing-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.4.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.0") (d #t) (k 0)))) (h "0ildiydgg98n40hs75a16gz4xvajji06fiqr0j0ynipc29gr8idr")))

(define-public crate-sdfparse-0.1.1 (c (n "sdfparse") (v "0.1.1") (d (list (d (n "clilog") (r "^0.2.3") (d #t) (k 0)) (d (n "compact_str") (r "^0.6.1") (d #t) (k 0)) (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "parsing-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.4.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.0") (d #t) (k 0)))) (h "1cwk10ddl38viz86235qvpn9lmbgcpr3zdv5b2r1k8jid79f1xh7")))

(define-public crate-sdfparse-0.1.2 (c (n "sdfparse") (v "0.1.2") (d (list (d (n "circular") (r "^0.3.0") (d #t) (k 0)) (d (n "clilog") (r "^0.2.3") (d #t) (k 0)) (d (n "compact_str") (r "^0.7.0") (d #t) (k 0)) (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "indenter") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5.4") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 2)))) (h "0pd1qxgp352xq3wyjhs2vd2wwiwi2fq9fzlw9n641mbisg4k0gib") (y #t)))

