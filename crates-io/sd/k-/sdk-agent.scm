(define-module (crates-io sd k- sdk-agent) #:use-module (crates-io))

(define-public crate-sdk-agent-0.1.0 (c (n "sdk-agent") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0aqgxx9ippv983h305lhan79rh6lssw0613bj6rrzdl9xp99s59q") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-sdk-agent-0.2.0 (c (n "sdk-agent") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0l384zmd128sy4ldh90y7hd3a52c3ilv4i1n5h0qwjv1y4w68r04") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-sdk-agent-0.2.1 (c (n "sdk-agent") (v "0.2.1") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1fccqpn62b15y22kzpnwdnrdbrrg5dsp2gqwyddz0sk4i0dzbvwh") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-sdk-agent-0.2.2 (c (n "sdk-agent") (v "0.2.2") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "06ys4jv5w65cgkig89cr8q2x9bpz94mydg1vabmf1nv9ji13qj40") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-sdk-agent-0.0.1 (c (n "sdk-agent") (v "0.0.1") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1vyhjk6hya9shp1wp8n2mbslmgjsq7jp0r6wjz3q0imn9l5xrf2r") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

