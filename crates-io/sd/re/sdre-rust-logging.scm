(define-module (crates-io sd re sdre-rust-logging) #:use-module (crates-io))

(define-public crate-sdre-rust-logging-0.1.0 (c (n "sdre-rust-logging") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1qkfm748nl6v22wwn8s2ipm4przxnwbhb8fibnn5g1z4m01rbxwx") (r "1.69.0")))

(define-public crate-sdre-rust-logging-0.2.0 (c (n "sdre-rust-logging") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "17m0n0zk89ngqsw7xyc1ivli4wisd6c9qkpcy81gzmg37hf7gjvq") (r "1.69.0")))

(define-public crate-sdre-rust-logging-0.2.1 (c (n "sdre-rust-logging") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0888w6br7g73b62w6vr8yhhb9i4gqf9m5r5wdh2m9651yj4nmx8m") (r "1.69.0")))

(define-public crate-sdre-rust-logging-0.2.2 (c (n "sdre-rust-logging") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0iwyfah608z0izmf7c7sphjld3mlva2pdghfrlpzk1zaljzpbdg1") (r "1.69.0")))

(define-public crate-sdre-rust-logging-0.3.0 (c (n "sdre-rust-logging") (v "0.3.0") (d (list (d (n "anstyle") (r "^1.0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "18nr28qw62r37fy99rzpqxqycz62a8z1lz84rfz1bbxg9320lbsg") (r "1.69.0")))

(define-public crate-sdre-rust-logging-0.3.1 (c (n "sdre-rust-logging") (v "0.3.1") (d (list (d (n "anstyle") (r "^1.0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "0s7kjbyg7lg24scnn3v46rv254qg6r5wfqx997q3cs16id5yiyia") (r "1.69.0")))

(define-public crate-sdre-rust-logging-0.3.2 (c (n "sdre-rust-logging") (v "0.3.2") (d (list (d (n "anstyle") (r "^1.0.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1h2aphkvhl0a3pdaw4rzyj17pfcv1vqdz8q64v5b2kkhjj7h5crh") (r "1.69.0")))

