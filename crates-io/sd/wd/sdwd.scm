(define-module (crates-io sd wd sdwd) #:use-module (crates-io))

(define-public crate-sdwd-0.1.0 (c (n "sdwd") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "systemd") (r "^0.3") (d #t) (k 0)))) (h "0hdqlxa9l38cby8y7cwgcmmdmcan3ykl8q5ngdhl395myps0m9v3")))

(define-public crate-sdwd-1.0.0 (c (n "sdwd") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "systemd") (r "^0.4") (d #t) (k 0)))) (h "19l55qyfiwjn3nhds1ddgp6raandlpi6x59vhl0kfqs69lyxpyck")))

