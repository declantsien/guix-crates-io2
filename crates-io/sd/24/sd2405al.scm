(define-module (crates-io sd #{24}# sd2405al) #:use-module (crates-io))

(define-public crate-sd2405al-0.1.0 (c (n "sd2405al") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rtcc") (r "^0.2") (d #t) (k 0)))) (h "0dd56cm7zhbs6xsf8lf6sixmkgashl4dlvqf1pdckb4nlwhxsvya")))

