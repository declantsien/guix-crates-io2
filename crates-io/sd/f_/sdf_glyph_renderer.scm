(define-module (crates-io sd f_ sdf_glyph_renderer) #:use-module (crates-io))

(define-public crate-sdf_glyph_renderer-0.1.0 (c (n "sdf_glyph_renderer") (v "0.1.0") (d (list (d (n "derive-error") (r "~0.0") (d #t) (k 0)) (d (n "freetype-rs") (r "~0.26") (o #t) (d #t) (k 0)))) (h "1rfn21hg8qsqgcjf72p0sln4fd0k5ivsksafp445788c6kz1mr0w") (f (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.1.1 (c (n "sdf_glyph_renderer") (v "0.1.1") (d (list (d (n "derive-error") (r "~0.0") (d #t) (k 0)) (d (n "freetype-rs") (r "~0.26") (o #t) (d #t) (k 0)))) (h "0yf9qgfgh89wav0im6nsjkqmnhhzj6f0yv86d78rx47wxg6xab3a") (f (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.1.2 (c (n "sdf_glyph_renderer") (v "0.1.2") (d (list (d (n "derive-error") (r "~0.0") (d #t) (k 0)) (d (n "freetype-rs") (r "~0.26") (o #t) (d #t) (k 0)))) (h "1ii81fw6mqsqa4m2gl9ybwib0sxlifv0ysx4ivy95l3grrpaam7g") (f (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.1.3 (c (n "sdf_glyph_renderer") (v "0.1.3") (d (list (d (n "derive-error") (r "^0.0.5") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.27.0") (o #t) (d #t) (k 0)))) (h "1md34j1kvd4yxhij6qahsc3hsmz4pysf64dxns4pbaxbaaxp52xb") (f (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.2.0 (c (n "sdf_glyph_renderer") (v "0.2.0") (d (list (d (n "derive-error") (r "^0.0.5") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.28.0") (o #t) (d #t) (k 0)))) (h "1rxhk1dcbx4f058j3gqlgq1229wsj8v2lf1f7664s512aj064fc4") (f (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.3.0 (c (n "sdf_glyph_renderer") (v "0.3.0") (d (list (d (n "derive-error") (r "^0.0.5") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.30.1") (o #t) (d #t) (k 0)))) (h "1v0b3pgn00rpic2rnzrlx1k829q9vs40br9mf7za8y9rxbij1pa9") (f (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.4.0 (c (n "sdf_glyph_renderer") (v "0.4.0") (d (list (d (n "derive-error") (r "^0.0.5") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.31.0") (o #t) (d #t) (k 0)))) (h "050zjvhjbabci37gk5gxn768w18jwnj10jr5kzcgxa610bqswidm") (f (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.5.0 (c (n "sdf_glyph_renderer") (v "0.5.0") (d (list (d (n "derive-error") (r "^0.0.5") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.32.0") (o #t) (d #t) (k 0)))) (h "1qq9znqn4y9vgqagq1cbmd30rc22a3qh5wagi1v622gqgn3g6j0q") (f (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.5.1 (c (n "sdf_glyph_renderer") (v "0.5.1") (d (list (d (n "derive-error") (r "^0.0.5") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.32.0") (o #t) (d #t) (k 0)))) (h "1l7v7hz8z3w30zd2iw2lv9hwh7skp1h2hagl1iv0b6g0nkzvrlca") (f (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-1.0.0 (c (n "sdf_glyph_renderer") (v "1.0.0") (d (list (d (n "freetype-rs") (r "^0.32.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "1fdkwg2ypi9pzrvc50fmi5hvq8sqrib5ic03kr80pql1s4ac21cb") (s 2) (e (quote (("freetype" "dep:freetype-rs")))) (r "1.65")))

(define-public crate-sdf_glyph_renderer-1.0.1 (c (n "sdf_glyph_renderer") (v "1.0.1") (d (list (d (n "freetype-rs") (r "^0.35.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "0kyr0gv3nghpsw6v7hvfpfsn0sf8nlnb2fpvx62ckw7zlkv21wjy") (s 2) (e (quote (("freetype" "dep:freetype-rs")))) (r "1.74.0")))

