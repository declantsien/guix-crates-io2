(define-module (crates-io sd -n sd-notify) #:use-module (crates-io))

(define-public crate-sd-notify-0.1.0-rc.1 (c (n "sd-notify") (v "0.1.0-rc.1") (h "0b37slm8wa8lc4d77sqw6pq44r1cp5lz1xdyzmn0j3jy0m0q1ww7")))

(define-public crate-sd-notify-0.1.0-rc.2 (c (n "sd-notify") (v "0.1.0-rc.2") (h "15ahjrlksakaxks1cm3qvzn02p3yf768cizbsaf7z7w6qzbi27zb")))

(define-public crate-sd-notify-0.1.0 (c (n "sd-notify") (v "0.1.0") (h "0li2la47g0n98cd3y5789xi08cfbnc450djskh5qsfc9n2gnvds0")))

(define-public crate-sd-notify-0.1.1 (c (n "sd-notify") (v "0.1.1") (h "11k4h6ly8v9vdrpvls19b63m48m3dcpfkc89hdzp0hxipcw0ix5f")))

(define-public crate-sd-notify-0.2.0 (c (n "sd-notify") (v "0.2.0") (h "1p7spgz5pzd4r5h6jvwmfx1vap691xbg7yf7jjfzc3v7vdfqqd69")))

(define-public crate-sd-notify-0.3.0 (c (n "sd-notify") (v "0.3.0") (h "1j1ff6hhzcyh3m9c0adxnnjrasnvl1v6qaiv5vj2zgajz0hqml0c")))

(define-public crate-sd-notify-0.4.0 (c (n "sd-notify") (v "0.4.0") (h "0697542127hyrvljkiqc6b6sdsmip3v3jyyaz2fpip2h9b4qbpig")))

(define-public crate-sd-notify-0.4.1 (c (n "sd-notify") (v "0.4.1") (h "0clc887rjdz0796c1lsbwnrgmcis4b30gyy3qb4v8zg0yf03c7k2")))

