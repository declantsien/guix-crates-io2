(define-module (crates-io sd l2 sdl2-unifont) #:use-module (crates-io))

(define-public crate-sdl2-unifont-1.0.0 (c (n "sdl2-unifont") (v "1.0.0") (d (list (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.3.0") (d #t) (k 1)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)))) (h "0zigfcdl3icpikqqn3ih88vi0jjl89w295g0s0frza8zswf8kb53") (f (quote (("plane-1") ("plane-0") ("default" "plane-0"))))))

(define-public crate-sdl2-unifont-1.0.1 (c (n "sdl2-unifont") (v "1.0.1") (d (list (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.3.0") (d #t) (k 1)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)))) (h "1wna4z417k010g3x2rm3n8k5ki2x6x0jsixbdgsdbw57ppx0j3b5") (f (quote (("plane-1") ("plane-0") ("default" "plane-0"))))))

(define-public crate-sdl2-unifont-1.0.2 (c (n "sdl2-unifont") (v "1.0.2") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5.1") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.5.1") (d #t) (k 1)) (d (n "sdl2") (r "^0.34.4") (d #t) (k 0)))) (h "0qahl1hw4j2w9r87vhyg44qarkkyzg1h7ra1mzdhjvlhpp564qc1") (f (quote (("plane-1") ("plane-0") ("default" "plane-0"))))))

