(define-module (crates-io sd l2 sdl2_mt) #:use-module (crates-io))

(define-public crate-sdl2_mt-0.1.0 (c (n "sdl2_mt") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.30.0") (d #t) (k 0)))) (h "0brb8irpapn4q8mk8znzv1cfrkxjcfc5br4lyj3z03g5gw2mq1v8")))

(define-public crate-sdl2_mt-0.2.0 (c (n "sdl2_mt") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "sdl2") (r "^0.30.0") (d #t) (k 0)))) (h "0xzab5c4v6bfphrd8490irfwyvwk9dvnv54nfbvm3la0m0zza2gh")))

