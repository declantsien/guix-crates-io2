(define-module (crates-io sd l2 sdl2_particles) #:use-module (crates-io))

(define-public crate-sdl2_particles-0.1.0 (c (n "sdl2_particles") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("image"))) (d #t) (k 0)))) (h "0vp44hcq7f38zaxbrvsrv5d90l8yaxbdfs5vvgfcrnc1idz239zr")))

(define-public crate-sdl2_particles-0.2.0 (c (n "sdl2_particles") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("image"))) (d #t) (k 0)))) (h "0g8q7x3pywa1a53bwyqzyq7fvwwvj3ndfdz3r9mhhablgqzl0abj")))

(define-public crate-sdl2_particles-0.3.0 (c (n "sdl2_particles") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("image"))) (d #t) (k 0)))) (h "1gz6qsf0gypkq42nh9n8xjvxxyb9055nn6ipb0db2mdrr1cgcbsr") (f (quote (("use-unsafe_textures" "sdl2/unsafe_textures"))))))

