(define-module (crates-io sd l2 sdl2_gfx) #:use-module (crates-io))

(define-public crate-sdl2_gfx-0.0.3 (c (n "sdl2_gfx") (v "0.0.3") (d (list (d (n "sdl2") (r "~0.0.3") (d #t) (k 0)))) (h "1q9893dzp6f937d5hwgvi1r4nz6jb7xavjhfhrsjgrpqzzxsbcwk")))

(define-public crate-sdl2_gfx-0.2.1 (c (n "sdl2_gfx") (v "0.2.1") (d (list (d (n "c_vec") (r "1.0.*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "sdl2") (r "^0.2.1") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.2.1") (d #t) (k 0)))) (h "15lk3220hd9acwn38fspv96sg19j7zl7alknxx1z6lwa33bh0s2j")))

(define-public crate-sdl2_gfx-0.4.0 (c (n "sdl2_gfx") (v "0.4.0") (d (list (d (n "c_vec") (r "1.0.*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "sdl2") (r "^0.4.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0dw11zyidfwjy7ygyjwlgia5m26jy10d4pb3dvrfqv1wdylynvj2")))

(define-public crate-sdl2_gfx-0.5.0 (c (n "sdl2_gfx") (v "0.5.0") (d (list (d (n "c_vec") (r "1.0.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.5") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.5") (d #t) (k 0)))) (h "01wmr2m5m43v6xx5nbrjzyrhcxchn9a7qp2ng02wphlrf8wap2vg")))

(define-public crate-sdl2_gfx-0.6.0 (c (n "sdl2_gfx") (v "0.6.0") (d (list (d (n "c_vec") (r "1.0.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.6") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.6") (d #t) (k 0)))) (h "1n9zzawb7pcx2lc8vfd99im4vcpwvss24ylw39cpzixpfsnkyq38")))

(define-public crate-sdl2_gfx-0.6.2 (c (n "sdl2_gfx") (v "0.6.2") (d (list (d (n "c_vec") (r "1.0.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.8") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.6.2") (d #t) (k 0)))) (h "17kf78j8jybry29jpkaq66p7qkg92awd9cgcd5cbhj4flh1z7g3n")))

(define-public crate-sdl2_gfx-0.6.3 (c (n "sdl2_gfx") (v "0.6.3") (d (list (d (n "c_vec") (r "1.0.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.6.2") (d #t) (k 0)))) (h "1yd5l5xr35khg3y422w74i54i7k5g1yqycn6k72il7a14qcb6fhd")))

(define-public crate-sdl2_gfx-0.7.0 (c (n "sdl2_gfx") (v "0.7.0") (d (list (d (n "c_vec") (r "1.0.*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.10") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.7") (d #t) (k 0)))) (h "1j2p1nj0i6h0164fj0cip90xy7qwrwnb7w9ikyykng3i73r1gnc4")))

(define-public crate-sdl2_gfx-0.8.1 (c (n "sdl2_gfx") (v "0.8.1") (d (list (d (n "c_vec") (r "1.0.*") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.16") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8") (d #t) (k 0)))) (h "0jcvxr6gr8dzxcjnshhcl944kr5rbnk0d4nkcwq8wiyc28djcc85")))

(define-public crate-sdl2_gfx-0.18.0 (c (n "sdl2_gfx") (v "0.18.0") (d (list (d (n "c_vec") (r "1.0.*") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.18") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8") (d #t) (k 0)))) (h "0rsph14n2b6xxk712id93c32h8b8azpqqcwaziylpzfgqq191lw5")))

(define-public crate-sdl2_gfx-0.23.0 (c (n "sdl2_gfx") (v "0.23.0") (d (list (d (n "c_vec") (r "1.0.*") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.23") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.23") (d #t) (k 0)))) (h "0x9ry16wnhjxsky1mc4jx7si284zdk54wqir315an7w1ms8j4phv")))

(define-public crate-sdl2_gfx-0.24.0 (c (n "sdl2_gfx") (v "0.24.0") (d (list (d (n "c_vec") (r "1.0.*") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.24") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.24") (d #t) (k 0)))) (h "1127yyh0n3xpbkplnk1q75p998qfqilwhs3c7y8glxdd1svj1hds")))

