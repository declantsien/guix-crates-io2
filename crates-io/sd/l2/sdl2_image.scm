(define-module (crates-io sd l2 sdl2_image) #:use-module (crates-io))

(define-public crate-sdl2_image-0.0.2 (c (n "sdl2_image") (v "0.0.2") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.0.31") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.0.31") (d #t) (k 0)))) (h "190h7gz2fc9gghp3i6v1gfzvr0v02kqzxzwbla4ls63yy1csck4q")))

(define-public crate-sdl2_image-0.0.33 (c (n "sdl2_image") (v "0.0.33") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.0.33") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.0.33") (d #t) (k 0)))) (h "008aqmvg374ikc4cxm6lnrhp7wna994z83c32dn0mabfs86shkh8")))

(define-public crate-sdl2_image-0.2.0 (c (n "sdl2_image") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.2.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.2.0") (d #t) (k 0)))) (h "044mmgyr779fn5icqn7wzyz0acc0svd5ml33i6nbrz7q7xi6s9rh")))

(define-public crate-sdl2_image-0.2.1 (c (n "sdl2_image") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.2.1") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1wi5ys2yy6v0464avqlp7clyhazv2aryvp8n7s9674q892ad058q")))

(define-public crate-sdl2_image-0.2.2 (c (n "sdl2_image") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.2.1") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1dklirmjkwck8bhhd7lp8lk36fz5v019wimndhmlwww0f4c2zb88") (y #t)))

(define-public crate-sdl2_image-0.2.3 (c (n "sdl2_image") (v "0.2.3") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.5") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.5") (d #t) (k 0)))) (h "11vik1jqkxwycrk5px8qfwq3ng1p5c5cf59n231pik11jvri2zyk")))

(define-public crate-sdl2_image-0.2.5 (c (n "sdl2_image") (v "0.2.5") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.8") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.6") (d #t) (k 0)))) (h "0pyrjw1k25kk3gsi0i3h2nbqizx50ypy4p28ypk03hhksb3vhcdz")))

(define-public crate-sdl2_image-0.3.0 (c (n "sdl2_image") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.6") (d #t) (k 0)))) (h "1bc5acwjfflgz76rn06xbhkmv4i2caqybg80nqnf2z416b9qsk4p")))

(define-public crate-sdl2_image-0.4.0 (c (n "sdl2_image") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.10.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.7.0") (d #t) (k 0)))) (h "0dkjzajx7v51dvh506as35xd3piicx4llw70pfgz9angqimkw7wb")))

(define-public crate-sdl2_image-0.5.0 (c (n "sdl2_image") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.11.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.7.0") (d #t) (k 0)))) (h "09shbcqy5mnb9xrrg1wdz8ba1mjcmi0ddj6fx4fwnlpdz04ac565")))

(define-public crate-sdl2_image-0.6.0 (c (n "sdl2_image") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.12.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.7.0") (d #t) (k 0)))) (h "0nc0mmwkgws9n0fvajvbskfwlpfjrk3jxx32nsxz3h8p95y21am7")))

(define-public crate-sdl2_image-0.7.0 (c (n "sdl2_image") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.13.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8.0") (d #t) (k 0)))) (h "0fwzrdirrqvxad5x9ijk8cgzqxmnzs22br3p0gy5lapnsrjzg3r4")))

(define-public crate-sdl2_image-1.0.0 (c (n "sdl2_image") (v "1.0.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.13.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8.0") (d #t) (k 0)))) (h "0rk1ssg8m09damlmw99mflhcx6lpn443d152q9j0bgfzr9x4k9ig") (y #t)))

(define-public crate-sdl2_image-1.1.0 (c (n "sdl2_image") (v "1.1.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.15.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8.0") (d #t) (k 0)))) (h "06zf6437qysgwpmd0r75lcr082rcwzvdxfia8dvvi1kaac54d1vd") (y #t)))

(define-public crate-sdl2_image-0.16.0 (c (n "sdl2_image") (v "0.16.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.16.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8.1") (d #t) (k 0)))) (h "0gr4xlnqpca805jcmwj7ym80pnn89j4zmv5q6qkns8gdikxcny0z")))

(define-public crate-sdl2_image-0.17.0 (c (n "sdl2_image") (v "0.17.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.17.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8.1") (d #t) (k 0)))) (h "0vpb31yigwvz4h5qy8gr372ixm797dmp162gymq9fvap677n5ybv")))

(define-public crate-sdl2_image-0.18.0 (c (n "sdl2_image") (v "0.18.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.18.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8.1") (d #t) (k 0)))) (h "1rll9kyaczjhc2nija49aw8200wbx9hrx06na8jwj8l92hja1w14")))

(define-public crate-sdl2_image-0.19.0 (c (n "sdl2_image") (v "0.19.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.19.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.19.0") (d #t) (k 0)))) (h "1lc9g9cqxxil22jvlxgiqywp4jwamsir1w8jjaj3lbsmg1agyyah")))

(define-public crate-sdl2_image-0.21.0 (c (n "sdl2_image") (v "0.21.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.21.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.21.0") (d #t) (k 0)))) (h "1w7ylx9mm9mpn43z9r8y0ka2slj003a74859qk3xab07f7p8zkas")))

(define-public crate-sdl2_image-0.22.0 (c (n "sdl2_image") (v "0.22.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "sdl2") (r "^0.22.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.22.0") (d #t) (k 0)))) (h "0qafl5127k5i68csa7hvv3s5dwzh189xjzz8kfgm3db699ks13vn")))

(define-public crate-sdl2_image-0.23.0 (c (n "sdl2_image") (v "0.23.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "sdl2") (r "^0.23.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.23.0") (d #t) (k 0)))) (h "08b2jiz93zql6igaywkkfzqncdidzni1jr2a2s3vv9hr4l541rjc")))

(define-public crate-sdl2_image-0.24.0 (c (n "sdl2_image") (v "0.24.0") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.24.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.24.0") (d #t) (k 0)))) (h "0178bdsys9x68nlz61d5hrbp5gz39k0i1lc0zddqvs6b3lbv8iq0")))

(define-public crate-sdl2_image-0.25.0 (c (n "sdl2_image") (v "0.25.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "sdl2") (r "^0.25.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.25.0") (d #t) (k 0)))) (h "18nd807nvwgjlbi7sb50i625v6wcl240gbydbdlq1l7fzvzwbhyc")))

