(define-module (crates-io sd l2 sdl2-extras) #:use-module (crates-io))

(define-public crate-sdl2-extras-0.0.1 (c (n "sdl2-extras") (v "0.0.1") (d (list (d (n "sdl2") (r "^0.31") (f (quote ("ttf" "image"))) (k 0)))) (h "1gn2wbwm8sjqjfzkgszsy65skz9smwgnsnfprvpy3mwmva0yarg7")))

(define-public crate-sdl2-extras-0.1.0 (c (n "sdl2-extras") (v "0.1.0") (d (list (d (n "floating-duration") (r "^0.1.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (f (quote ("ttf" "image"))) (k 0)) (d (n "specs") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "0qhxyh0m4rfphvqw2bw6izsaz8x40yj42j41vsnfyfnfvxbg9fbv") (f (quote (("fspecs" "specs"))))))

(define-public crate-sdl2-extras-0.1.1 (c (n "sdl2-extras") (v "0.1.1") (d (list (d (n "floating-duration") (r "^0.1.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (f (quote ("ttf" "image"))) (k 0)) (d (n "specs") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "1h8shlc4bh2m8m39d547wbj1zybk8cfyr2ry30a6y24v89b89ssm") (f (quote (("fspecs" "specs"))))))

(define-public crate-sdl2-extras-0.1.2 (c (n "sdl2-extras") (v "0.1.2") (d (list (d (n "floating-duration") (r "^0.1.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (f (quote ("ttf" "image"))) (k 0)) (d (n "specs") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "0wx8d2wy29fpir1q7nzp1fha6l6v0g0lisp7zpryvf95k7gfj2yj") (f (quote (("fspecs" "specs"))))))

(define-public crate-sdl2-extras-0.1.3 (c (n "sdl2-extras") (v "0.1.3") (d (list (d (n "floating-duration") (r "^0.1.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (f (quote ("ttf" "image"))) (k 0)) (d (n "specs") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "0rmvjccn9759g4wq41j8ckxkpvs0l6b7zpmmp4j3g4kgchqvl243") (f (quote (("fspecs" "specs"))))))

(define-public crate-sdl2-extras-0.1.4 (c (n "sdl2-extras") (v "0.1.4") (d (list (d (n "floating-duration") (r "^0.1.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (f (quote ("ttf" "image"))) (k 0)) (d (n "specs") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "0mynvldvf7cn8q28lc9f96nxswhr9k68zddz6g2bn6zrmyplrvi7") (f (quote (("fspecs" "specs"))))))

