(define-module (crates-io sd l2 sdl2_net) #:use-module (crates-io))

(define-public crate-sdl2_net-0.0.1 (c (n "sdl2_net") (v "0.0.1") (d (list (d (n "sdl2") (r "^0.0.24") (d #t) (k 0)))) (h "1dh2fcyvqzswih4firaxbgq7vbi4bcicmhrhpivj0zifrb4vk7g2")))

(define-public crate-sdl2_net-0.0.2 (c (n "sdl2_net") (v "0.0.2") (d (list (d (n "sdl2") (r "^0.0.28") (d #t) (k 0)))) (h "0v1343dzdwmmi3dsw2gggpb8zq19fkaqyksgh7cx2a64sxdz9yfr")))

