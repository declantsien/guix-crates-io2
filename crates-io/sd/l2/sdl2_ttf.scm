(define-module (crates-io sd l2 sdl2_ttf) #:use-module (crates-io))

(define-public crate-sdl2_ttf-0.0.2 (c (n "sdl2_ttf") (v "0.0.2") (d (list (d (n "sdl2") (r "~0.0.3") (d #t) (k 0)))) (h "00jhlq3zf7wn05g8m3xbnxnrzkwlbw7xb7f9wqk9mz6q56wfaqbw")))

(define-public crate-sdl2_ttf-0.0.3 (c (n "sdl2_ttf") (v "0.0.3") (d (list (d (n "sdl2") (r "~0.0.3") (d #t) (k 0)))) (h "1zczipbsp7f9awf6gj89fvhr4l4izsrram8bf8dy79pl6wn5w7m9")))

(define-public crate-sdl2_ttf-0.2.1 (c (n "sdl2_ttf") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.2.1") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.2.1") (d #t) (k 0)))) (h "05byal3jp9yv5yab38jvmhl53nff072rbq0bqcxs6x35bfrnk7vj")))

(define-public crate-sdl2_ttf-0.2.2 (c (n "sdl2_ttf") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.2.1") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.2.1") (d #t) (k 0)))) (h "09rw3vwqqs4km6gsr1l2hd6qj0gq7y8fabf72b0w8rcrrfzmx9sv")))

(define-public crate-sdl2_ttf-0.2.3 (c (n "sdl2_ttf") (v "0.2.3") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.2.1") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1dx2zxa6f6la602lcn2rrg5l20qnai2s9m6gz02l4lr47cf746yj")))

(define-public crate-sdl2_ttf-0.4.0 (c (n "sdl2_ttf") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.4") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.4") (d #t) (k 0)))) (h "15vfqn3shybyg2hihk8sl31hv1yyd2nj1485rwzbaf8z0b4q3dbr")))

(define-public crate-sdl2_ttf-0.5.0 (c (n "sdl2_ttf") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.5") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.5") (d #t) (k 0)))) (h "057g41r7ph69ffilm2wdwxgddq1z66vvww3n8k2nsv6bfnnwnkns")))

(define-public crate-sdl2_ttf-0.6.0 (c (n "sdl2_ttf") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.6") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.6") (d #t) (k 0)))) (h "0nscn21hkw5avqr0iz28i1rf4abrak939qv4xshb1am7bb9ab14d")))

(define-public crate-sdl2_ttf-0.6.1 (c (n "sdl2_ttf") (v "0.6.1") (d (list (d (n "bitflags") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.8") (d #t) (k 0)) (d (n "sdl2-sys") (r "*") (d #t) (k 0)))) (h "1kqws14zrwjcasrmnj1x7b7712aw19iwpwvp4f8zw558pnapn7v2")))

(define-public crate-sdl2_ttf-0.9.0 (c (n "sdl2_ttf") (v "0.9.0") (d (list (d (n "bitflags") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.6") (d #t) (k 0)))) (h "0mcrzljynmj612zcw60xnz1djwhg89xqxbvnyjqq4y0ykqdsawrn")))

(define-public crate-sdl2_ttf-0.9.1 (c (n "sdl2_ttf") (v "0.9.1") (d (list (d (n "bitflags") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.6") (d #t) (k 0)))) (h "0lw62m97zg52h6729z5zf9gvzijr3g36fs8dnlpzqz199l9gy43b")))

(define-public crate-sdl2_ttf-0.10.0 (c (n "sdl2_ttf") (v "0.10.0") (d (list (d (n "bitflags") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.12.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.7.0") (d #t) (k 0)))) (h "0pi1rrgd3mzv2280m7bx41jadnfi72431dbpq93wrxdnracpcla1")))

(define-public crate-sdl2_ttf-0.11.0 (c (n "sdl2_ttf") (v "0.11.0") (d (list (d (n "bitflags") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.12.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1fn5bvhxdr9c31mwzcszzlw8i36hsbisadacyz4v291s5zr9hs3i")))

(define-public crate-sdl2_ttf-0.12.0 (c (n "sdl2_ttf") (v "0.12.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.12.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8.0") (d #t) (k 0)))) (h "12zk4nd0xxqix11s1pn7qakk69m6vgfsmf3xw1jpn84xiixr76iq")))

(define-public crate-sdl2_ttf-0.13.0 (c (n "sdl2_ttf") (v "0.13.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.13.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8.0") (d #t) (k 0)))) (h "0n3j1acrz1h9jly89c642qcflp2ip0f8fhfmnbr540v13wzqgqnp")))

(define-public crate-sdl2_ttf-0.13.1 (c (n "sdl2_ttf") (v "0.13.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.13.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8.0") (d #t) (k 0)))) (h "1hy6fzyc8h6i8q1k4k44miamgdvqk38ixfdjb0vyjdqpiaqq7fh8")))

(define-public crate-sdl2_ttf-0.14.0 (c (n "sdl2_ttf") (v "0.14.0") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.13.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8.0") (d #t) (k 0)))) (h "0vab8j07p924swmlfh80499j4vli9w9hbpc5sj5mqhrp0wrn9vyw")))

(define-public crate-sdl2_ttf-0.15.0 (c (n "sdl2_ttf") (v "0.15.0") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.15.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8.0") (d #t) (k 0)))) (h "115k0smy1dyfy5z1dqb052i93y77scpv4h8g4j8dbb8xwgy38x01")))

(define-public crate-sdl2_ttf-0.16.1 (c (n "sdl2_ttf") (v "0.16.1") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.16.1") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8.1") (d #t) (k 0)))) (h "07q2i7ns0i4zznawsz22fpmrcafgrxwnwq0s3j8sqg0mm88wbr9m")))

(define-public crate-sdl2_ttf-0.16.2 (c (n "sdl2_ttf") (v "0.16.2") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.18.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.8.1") (d #t) (k 0)))) (h "051r3wk4pdr4c9ipv21jf1vr4sqf4zr6nzrxjpfsplzb7kdqkgly")))

(define-public crate-sdl2_ttf-0.19.0 (c (n "sdl2_ttf") (v "0.19.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.19.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.19.0") (d #t) (k 0)))) (h "154fy55k1s9j11d1n783vhpsipr71qdz0mxwc3716qs6glydrf0v")))

(define-public crate-sdl2_ttf-0.21.0 (c (n "sdl2_ttf") (v "0.21.0") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.21") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.21") (d #t) (k 0)))) (h "076ry6k9zdc2mr5hz6zl4ldngz4zn9q9f5mj1576mmr2mdnmlfpb")))

(define-public crate-sdl2_ttf-0.22.0 (c (n "sdl2_ttf") (v "0.22.0") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.22") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.22") (d #t) (k 0)))) (h "05gzjspbd01i6r9g4drsdii93xjv603k2flka5zljc47zwbw4iim")))

(define-public crate-sdl2_ttf-0.23.0 (c (n "sdl2_ttf") (v "0.23.0") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.23") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.23") (d #t) (k 0)))) (h "0zsdfsdvg0xc57jz8d1dqw5m46cck935iv7p9dfwm7mm6ymhyj0x")))

(define-public crate-sdl2_ttf-0.23.1 (c (n "sdl2_ttf") (v "0.23.1") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.23") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.23") (d #t) (k 0)))) (h "0k3c97f6ch4cdf3i0gwc9mgzss6sbd8gg184mwh413jzyza6379h")))

(define-public crate-sdl2_ttf-0.24.0 (c (n "sdl2_ttf") (v "0.24.0") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.24") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.24") (d #t) (k 0)))) (h "1nm5n8yclj7l15b0qcbbnbn258r4hdmc62h12ac2xgn8md99dads")))

(define-public crate-sdl2_ttf-0.25.0 (c (n "sdl2_ttf") (v "0.25.0") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.25") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.25") (d #t) (k 0)))) (h "1haxxdfzg74sb0x0gibqc4rh9za8vpx59kjrpf01h50cy3qls0cw")))

(define-public crate-sdl2_ttf-0.25.1 (c (n "sdl2_ttf") (v "0.25.1") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.25") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.25") (d #t) (k 0)))) (h "10f0qsmrqdwppyrd4fqbrf6d3gbd0swkz5m2niidvc9q33ypq0hl")))

