(define-module (crates-io sd l2 sdl2_timing) #:use-module (crates-io))

(define-public crate-sdl2_timing-0.1.0 (c (n "sdl2_timing") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (f (quote ("gfx" "ttf"))) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)))) (h "15mkw55kx56pkil3ccczcy5gxk11d4q4a8wmpnf3l0zjd6pwfygg")))

(define-public crate-sdl2_timing-0.2.0 (c (n "sdl2_timing") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "sdl2") (r "^0.33") (f (quote ("gfx" "ttf"))) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)))) (h "1x0a424qcj4iryh5qxbyri8y01i7f78c2div9z1vyrkqbkllw02i")))

