(define-module (crates-io sd l2 sdl2-wallpaper) #:use-module (crates-io))

(define-public crate-sdl2-wallpaper-0.1.0 (c (n "sdl2-wallpaper") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "07cmljalm4dg9brdqxnj5lmw3ffp9nm85xdhkmr0bmxgipr5s6fx")))

