(define-module (crates-io sd ns sdns) #:use-module (crates-io))

(define-public crate-sdns-0.1.0 (c (n "sdns") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls" "trust-dns"))) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1sh96f8qabkf22sz14g7hqm4773svw6iwp1mivbcsq8fik0d6fr3") (y #t)))

