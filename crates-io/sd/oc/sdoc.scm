(define-module (crates-io sd oc sdoc) #:use-module (crates-io))

(define-public crate-sdoc-0.2.2 (c (n "sdoc") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "1inzxki01czvfcbcchhhazz50qyjwf2rkz3hv1mclzixrakcwak8")))

(define-public crate-sdoc-0.2.3 (c (n "sdoc") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "0l9x1bzk83df59i5zws21q470y6ify1gz6f9qhv1q1x0lvd9pxc7")))

(define-public crate-sdoc-0.3.0 (c (n "sdoc") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "0rksk38gm1ffmvaqdiml4nmh5drdpdixnnxh1chrwvrxcz7nyhw0")))

(define-public crate-sdoc-0.3.1 (c (n "sdoc") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "04z70my3m9737yirddivr94lznf6kw8f2y89zp7mqkn018i3al9b")))

(define-public crate-sdoc-0.3.2 (c (n "sdoc") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "1arm08ks9nhmbfx73q7n9cb63m94vd2dw42xv5qfzf8rhxhxwcsi")))

(define-public crate-sdoc-0.4.0 (c (n "sdoc") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "04fx8kgy7kqsi4iij7jwvqgisav2hwpc5fh8fb81cwcws7m21aiv")))

(define-public crate-sdoc-0.4.1 (c (n "sdoc") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "1gmnd4vi9lhaz4a3v6f0pc5f8j6pn5d3qy8n848h04xl65yh763l")))

(define-public crate-sdoc-0.4.2 (c (n "sdoc") (v "0.4.2") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "0gw4mfcxra43jlkajlkykdlk8a8r8m1xn7170cyl3gjky87vhqpc")))

(define-public crate-sdoc-0.5.0 (c (n "sdoc") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "06zd62a4l8aipzw9naqipycm8qqvhlq1l8mkwaxdwghfr0svz4na")))

(define-public crate-sdoc-0.7.0 (c (n "sdoc") (v "0.7.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "assert_cli") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "15l77gqlnsw3nms4axh2yrkw9r5hl3f4azd0rx7sfgrdb1207wfq")))

(define-public crate-sdoc-0.7.1 (c (n "sdoc") (v "0.7.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "assert_cli") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1640hvbhv8z7fka0nj9aknghm3l1qds7022kwsngn4qdskqmzckf")))

(define-public crate-sdoc-0.8.0 (c (n "sdoc") (v "0.8.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "assert_cli") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "009g9m1gdlaygrlcmfhy1l7k1ql75zv146dqx5jb8n0bx8kyncx4")))

(define-public crate-sdoc-0.8.1 (c (n "sdoc") (v "0.8.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "assert_cli") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1k9fcjf89q39bk1sqsr47h6mkikpp6wpp65hkfvs12jqgpa8nfwc")))

(define-public crate-sdoc-0.8.2 (c (n "sdoc") (v "0.8.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "assert_cli") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1kya8irzh5zlpnl79h41894xkk1s0m6yk0ihgkp7hqhw7nnc4xsm")))

(define-public crate-sdoc-0.8.3 (c (n "sdoc") (v "0.8.3") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "assert_cli") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0v4jsyf606jhlq3vr0hf44qd617mg6bxcs5f6n5p8j0knd2h8z2b")))

(define-public crate-sdoc-0.8.9 (c (n "sdoc") (v "0.8.9") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "00bv4w86xl1z8x261p9lak6l8iibfk96hilb6iy9ccx3a6drjc06")))

(define-public crate-sdoc-0.8.10 (c (n "sdoc") (v "0.8.10") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0gk0m3jpdw2546qgcyv5awnj02cggll1psnk7iag8xj6rwvvx1zc")))

(define-public crate-sdoc-0.8.11 (c (n "sdoc") (v "0.8.11") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1g9l5wxhwc4m31x78zj4h4hwzdyh6gzzkg9v5nrp7rxw7rmbl5ph")))

