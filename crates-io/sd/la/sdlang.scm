(define-module (crates-io sd la sdlang) #:use-module (crates-io))

(define-public crate-sdlang-0.0.1 (c (n "sdlang") (v "0.0.1") (d (list (d (n "base64") (r "~0.10.1") (d #t) (k 0)) (d (n "chrono") (r "~0.4.6") (d #t) (k 0)) (d (n "itertools") (r "~0.8.0") (d #t) (k 0)) (d (n "pest") (r "~2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "~2.1.0") (d #t) (k 0)))) (h "1hf3ira6skxrqiffqnc8yrxvc0p2051f2lv3x3n0hsmjkgj3z2wd")))

