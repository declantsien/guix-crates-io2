(define-module (crates-io sd c_ sdc_apis) #:use-module (crates-io))

(define-public crate-sdc_apis-0.2.0-dev20230930 (c (n "sdc_apis") (v "0.2.0-dev20230930") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.1") (d #t) (k 0)) (d (n "prost-wkt") (r "^0.4") (d #t) (k 0)) (d (n "prost-wkt-build") (r "^0.4") (d #t) (k 1)) (d (n "prost-wkt-types") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "02c75x3ckl0psnl7x16x378czirp1m607v7513pkbsrjfhrspsi2")))

(define-public crate-sdc_apis-0.2.1-dev20240222 (c (n "sdc_apis") (v "0.2.1-dev20240222") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.1") (d #t) (k 0)) (d (n "prost-wkt") (r "^0.4") (d #t) (k 0)) (d (n "prost-wkt-build") (r "^0.4") (d #t) (k 1)) (d (n "prost-wkt-types") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "1vg6sk1zrfd5dxs7y39mfvk7nqssmbrq1z78cv45mmdvn22z4lhw")))

