(define-module (crates-io sd fu sdfui) #:use-module (crates-io))

(define-public crate-sdfui-0.1.0 (c (n "sdfui") (v "0.1.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0b70ix1x8i9sl6vk9dic34cfzw8ziy0jf74hvflpbdb6f02k8x4g")))

(define-public crate-sdfui-0.1.1 (c (n "sdfui") (v "0.1.1") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "147w2yf3m7aipggvszyr74m7kbddis75dvp7a41lbg9afk65h990")))

