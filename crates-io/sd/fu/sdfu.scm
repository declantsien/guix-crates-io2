(define-module (crates-io sd fu sdfu) #:use-module (crates-io))

(define-public crate-sdfu-0.0.1 (c (n "sdfu") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "vek") (r "^0.9") (o #t) (d #t) (k 0)))) (h "17cissj3rbzv02a53sygs9c3wwmxba7rc4nr7sm12ff2kp2ci42l")))

(define-public crate-sdfu-0.1.0 (c (n "sdfu") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "vek") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1m0bhd6i06799054df38zq7zg8zsq4n1ci4kcvfcryf12vddizy7")))

(define-public crate-sdfu-0.1.1 (c (n "sdfu") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "vek") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0zwd1d65klwx2x23ckyxapvp4w13n6cxsdr7yrisvnvkj7jw2fri")))

(define-public crate-sdfu-0.1.2 (c (n "sdfu") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "vek") (r "^0.9") (o #t) (d #t) (k 0)))) (h "14a3rwr2iw6gx14a8gax2vf5chkkcimh696xk7mxqkxa8195jl9z")))

(define-public crate-sdfu-0.2.0 (c (n "sdfu") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "ultraviolet") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vek") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1cvzbln6krzh033wp9ispgqklrnz9rvyhi74ghlwk9jvxhpnns7c")))

(define-public crate-sdfu-0.3.0 (c (n "sdfu") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "ultraviolet") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "vek") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0p12xdxjdhlfxs988218aiqj2322viw40akf7xa01xx1w2k3pmm9")))

(define-public crate-sdfu-0.3.1-alpha.1 (c (n "sdfu") (v "0.3.1-alpha.1") (d (list (d (n "glam") (r ">=0.18") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.27") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.27") (d #t) (k 2)) (d (n "ultraviolet") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "ultraviolet") (r "^0.8") (d #t) (k 2)) (d (n "vek") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "vek") (r "^0.15") (d #t) (k 2)))) (h "1265d20wxmyaq0ph6267x0va54v9p29g78qhh11p2bkg6az5y9g2")))

