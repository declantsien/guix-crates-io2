(define-module (crates-io sd we sdwebuiapi) #:use-module (crates-io))

(define-public crate-sdwebuiapi-0.0.1 (c (n "sdwebuiapi") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "1af6mpw9c4v5qk22l7b568apvy7nk46v2lfj4pnnrhspy801s85r") (y #t)))

(define-public crate-sdwebuiapi-0.0.2 (c (n "sdwebuiapi") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "0q92p4yz055i70yp56f6x0yb2054zaiqql0z9ha1j7sp4csmdcgb") (y #t)))

(define-public crate-sdwebuiapi-0.0.3 (c (n "sdwebuiapi") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "1cbggqrcxzd78a8h5var54hjr48q1xf6bwrg0pzbm87khdj7027k") (y #t)))

(define-public crate-sdwebuiapi-0.0.4 (c (n "sdwebuiapi") (v "0.0.4") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "0qsc6vz8xdl1ihxmz4lfp2ga8vmnqixrcblwdlh6jyn8w33zy803") (y #t)))

(define-public crate-sdwebuiapi-0.0.5 (c (n "sdwebuiapi") (v "0.0.5") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "0dpiwxdp3ps3i3zlwpdj5sh673bxqi5lqb36a1rsrfqb3i2hm13l")))

(define-public crate-sdwebuiapi-0.0.6 (c (n "sdwebuiapi") (v "0.0.6") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "0sh87h3mkjik397hrn7cyybayqlkycws0n7k4wwcn3rgc6ky77kv")))

