(define-module (crates-io sd b_ sdb_client) #:use-module (crates-io))

(define-public crate-sdb_client-0.6.0 (c (n "sdb_client") (v "0.6.0") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "sdb_core") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "04vbg1skgwr7804xfijm5sldh0k9r39pcais9nbrv3m8rzxar33i")))

