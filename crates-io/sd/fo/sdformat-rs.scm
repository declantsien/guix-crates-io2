(define-module (crates-io sd fo sdformat-rs) #:use-module (crates-io))

(define-public crate-sdformat-rs-0.1.0 (c (n "sdformat-rs") (v "0.1.0") (d (list (d (n "RustyXML") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0dwxppx4rh6hb5qg3z7naay9a56nm8i2lzkd7idcc5pw46k33gn8")))

