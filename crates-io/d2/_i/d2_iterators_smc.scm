(define-module (crates-io d2 _i d2_iterators_smc) #:use-module (crates-io))

(define-public crate-d2_iterators_smc-0.1.0 (c (n "d2_iterators_smc") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "0dlx0hq7dym9kdkimqhljmjfx5hf0a3rvxa876ih5annqsffcz8n")))

(define-public crate-d2_iterators_smc-0.1.1 (c (n "d2_iterators_smc") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "05ldjfsic8ic1w2pn2vyxgz2b0myix2v6wdgfllszrxnjw3r3i02")))

