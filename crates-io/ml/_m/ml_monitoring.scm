(define-module (crates-io ml _m ml_monitoring) #:use-module (crates-io))

(define-public crate-ml_monitoring-0.1.0 (c (n "ml_monitoring") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.1") (d #t) (k 2)) (d (n "kolmogorov_smirnov") (r "^1.1.0") (d #t) (k 0)) (d (n "slice-deque") (r "^0.1.16") (d #t) (k 0)))) (h "1b8wdcmdlahldzj9nkvlfkxvx8ddymr812ky1q190wjz3xy7li5i")))

