(define-module (crates-io ml es mles) #:use-module (crates-io))

(define-public crate-mles-0.1.0 (c (n "mles") (v "0.1.0") (d (list (d (n "mles-utils") (r "^0.1.1") (d #t) (k 0)))) (h "10kvgb0hg629qs3fcn8sm9q5l4h0kiyd4fyrmb6qwbvgmadd7kdc")))

(define-public crate-mles-0.2.0 (c (n "mles") (v "0.2.0") (d (list (d (n "mles-utils") (r "^0.2") (d #t) (k 0)))) (h "1nkr71k9dxiiiv1ay1zc1fgs2gsg356in8b1l1h9lxrsspvf97hl")))

(define-public crate-mles-0.3.0 (c (n "mles") (v "0.3.0") (d (list (d (n "mles-utils") (r "^0.3") (d #t) (k 0)))) (h "09kv53kb1z7gr5sin207mlnyzm938ann47vj1s3whym0rggxh8cg")))

(define-public crate-mles-0.3.1 (c (n "mles") (v "0.3.1") (d (list (d (n "mles-utils") (r "^0.3") (d #t) (k 0)))) (h "1ql0gps80k8dqz1hfn8lyqgsv87qxp5jpcb55wffajvs3zwnma7q")))

(define-public crate-mles-0.4.0 (c (n "mles") (v "0.4.0") (d (list (d (n "mles-utils") (r "^0.3") (d #t) (k 0)))) (h "18b01myaahabdbdng6cgw1s7i4vj5xyy4ir77ck97h3paisvqncm")))

(define-public crate-mles-0.4.1 (c (n "mles") (v "0.4.1") (d (list (d (n "mles-utils") (r "^0.4") (d #t) (k 0)))) (h "0a5llmn6zry5qlrrh94i96innk82ln2pr7c5fbmp46z8g48vqy0z")))

(define-public crate-mles-0.4.2 (c (n "mles") (v "0.4.2") (d (list (d (n "mles-utils") (r "^0.4") (d #t) (k 0)))) (h "1j1454lkhq9lqhzdinzkvgmkm65psclwps7psin23ndb9smd4p4c")))

(define-public crate-mles-0.4.4 (c (n "mles") (v "0.4.4") (d (list (d (n "mles-utils") (r "^0.4") (d #t) (k 0)))) (h "049wq29rhg8i35mcv04aqwpilx0wip5lxzmxqhsb3m1m18j9h85p")))

(define-public crate-mles-0.5.0 (c (n "mles") (v "0.5.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1ml1qh9h5rqld4hvgkkz5pafp2zl75mmnq3p874436h2i0gma4z3")))

(define-public crate-mles-0.5.1 (c (n "mles") (v "0.5.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0d4rqrg7vvhq3mj9zwdr7qxxns01jswcli8yapmzwas4ayjykm28")))

(define-public crate-mles-0.5.2 (c (n "mles") (v "0.5.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0hz81rigqzcdnhi3fhnmq55vcagf8wwsm18x45mng78hh4cx0w0b")))

(define-public crate-mles-0.5.3 (c (n "mles") (v "0.5.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0vk332avj9h56bilqy2qzc09lf8siplw164qn7h3swf51ka4cyg2")))

(define-public crate-mles-0.5.4 (c (n "mles") (v "0.5.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1r1mdvj97wp6vp421acr7p20795xfxp5lm5z7i1bmacysn7xpsk0")))

(define-public crate-mles-0.5.5 (c (n "mles") (v "0.5.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.5") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0wckfw07b0agxyj8qf51gwp9fpnz1nnbzbfpfcw7fkwghqjpbn7b")))

(define-public crate-mles-0.5.6 (c (n "mles") (v "0.5.6") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.5") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "02hiqs46iww8pw3n8fgzrvd7yygr3nnjjbwahbk2y8snksp71hir")))

(define-public crate-mles-0.5.8 (c (n "mles") (v "0.5.8") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.5") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0m9wqid2yvgldg2fpy4nhhr0pkghxq05mycs4cxxd75i865dzr5s")))

(define-public crate-mles-0.6.0 (c (n "mles") (v "0.6.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0is8jy4wdgw9jxnzvm4kih5cgp6i5z1a5ir7w3xdj2mi69hxqnbv")))

(define-public crate-mles-0.6.1 (c (n "mles") (v "0.6.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0a7lj571czbx0kckg5q00sinhazf1zbdznsw8klxw1640a0awxmj")))

(define-public crate-mles-0.6.2 (c (n "mles") (v "0.6.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0n8qb19wgxg98dlrb3f761yvbmmsijr25nxkb14ya6zdqva977iw")))

(define-public crate-mles-0.6.3 (c (n "mles") (v "0.6.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1byk21hy9pvpg7gyq490jyf6k0jpyb4fxgadng2pli5chb1avblv")))

(define-public crate-mles-0.7.0 (c (n "mles") (v "0.7.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.7") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "03m3ywkp7b0pmvdw2fvag6gmp5j3m4i95blraa78bxnrfxsy3afv")))

(define-public crate-mles-0.7.1 (c (n "mles") (v "0.7.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.7") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1j8r4afvbn3v4xdmyks09kk6yadxn1v3cl0jsrcv4yyrsxf26dga")))

(define-public crate-mles-0.7.2 (c (n "mles") (v "0.7.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.7") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1hf94wkk2y8c6kzwwa26m0z1igbix1qn6cf7j69pbaqsvk4z4zvg")))

(define-public crate-mles-0.7.3 (c (n "mles") (v "0.7.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.7") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0jmxx95ik1nss0ymfn6cfk2yh15d52p0k70g12qnbp1xixjjl7yd")))

(define-public crate-mles-0.7.4 (c (n "mles") (v "0.7.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.7") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "04ljs592iqkavajvwnkbjyh57qcf8hbg7bwcf2fcwva5pybsib3c")))

(define-public crate-mles-0.7.5 (c (n "mles") (v "0.7.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.7") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0gs64fbm267dacbrmqajp4snl3zxm53hicddh0bax0vk0551m702")))

(define-public crate-mles-0.8.0 (c (n "mles") (v "0.8.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1m5c9ksi1m6m1j1gx4lsbbd78l73mbq00myn3bchvij5l1xg0y55")))

(define-public crate-mles-0.8.1 (c (n "mles") (v "0.8.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "00g3cwhrh75z982fnsyrwsqlpv99g5x2vjg09s5m951k0gylc246")))

(define-public crate-mles-0.8.2 (c (n "mles") (v "0.8.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0mzwndv26b0f5iyv4j7vadlhhw4b66558lxdi5hxmpcykfln4zii")))

(define-public crate-mles-0.8.3 (c (n "mles") (v "0.8.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "12696wlh9p39l3578p6wrm38yyy9433z6nfbxc2abck70539plgv")))

(define-public crate-mles-0.9.0 (c (n "mles") (v "0.9.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1bz071gbqhd7gm44imrfglbpvp8f1cy3amkmj00n9z0jsgwaqzjq")))

(define-public crate-mles-0.9.1 (c (n "mles") (v "0.9.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1ymy3274bnnfw8467n2a8638r8n23m1h59kcgrrlifm93yrv4wnp")))

(define-public crate-mles-0.9.2 (c (n "mles") (v "0.9.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1adhvw9430shbr4hh8b8g3rca8k4fmkwi0m5nnhgwfs72sypvx89")))

(define-public crate-mles-0.9.3 (c (n "mles") (v "0.9.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1b0xv9n1zsn2xp0rlak97cpim82hz3b9r76h3n3yfhd4w9nnrzr0")))

(define-public crate-mles-0.9.4 (c (n "mles") (v "0.9.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "13zd9wr84knpsr6wfxqaxw93z97p813xwf3wz27dxg3qf8w82mfr")))

(define-public crate-mles-0.9.5 (c (n "mles") (v "0.9.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "065n7vbaac3fzdqr1bs1jrs2hyy3alvgcd86zz0nkhx5fcbaf4c1")))

(define-public crate-mles-0.9.6 (c (n "mles") (v "0.9.6") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1wgg6x12hmal5l33cy4r00j10bix0yqyj64dpipagakj4is5qd9m")))

(define-public crate-mles-0.9.7 (c (n "mles") (v "0.9.7") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1x7bfp49nw3am6nn9fa089cms9nmahk6bc5p0pq89mmssprgbagp")))

(define-public crate-mles-0.9.9 (c (n "mles") (v "0.9.9") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "12vpn1zcl1nbq70vm0bzq1b4s1xyk2bvkh6hipnp9cckdmlmmvbj")))

(define-public crate-mles-0.9.10 (c (n "mles") (v "0.9.10") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1kbagf436crbmyb7s9a6f16xlqv59qphp9g1mxpzd1jzxcww1bkk")))

(define-public crate-mles-0.9.11 (c (n "mles") (v "0.9.11") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "06w1pm8l6jfkzp3zgbllnfj03af1m5746zl850986r39rapprxac")))

(define-public crate-mles-0.9.12 (c (n "mles") (v "0.9.12") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0hv5idsx8dx7543wkvix4q80yl5fqvs8adl6kfnai9yjzci35nqs")))

(define-public crate-mles-1.0.0-alpha (c (n "mles") (v "1.0.0-alpha") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0zg6b24kp43yn3yx7vgq39dfdszvgsbcwyr47w0bv3l07g8ar95g")))

(define-public crate-mles-0.10.0 (c (n "mles") (v "0.10.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.10") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0rkkyy33fx84ybslm38hr5dpz2yqiy1lq85mynj8w21bp1m8rn19")))

(define-public crate-mles-0.11.0 (c (n "mles") (v "0.11.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1yjlvq8ls10j2r2pysqr1c0s62x0789s11f3ipwzvidq07pdy38z")))

(define-public crate-mles-1.0.0-beta (c (n "mles") (v "1.0.0-beta") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "066pwprd2fvhs103dp4mk8xvr8aiq6q8v0i7d4q70fzyc6brvd5q")))

(define-public crate-mles-0.11.1 (c (n "mles") (v "0.11.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0wzhlfyczr1admfw7s80pp8j0ms2c32dbvb3lzkcsn5729xb2ygr")))

(define-public crate-mles-0.11.2 (c (n "mles") (v "0.11.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mles-utils") (r "^0.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0s3d6a5ki0gjpv1w6ifacpsw08pgrbg264zgk4pv00gpfc10ypa7")))

(define-public crate-mles-0.12.0 (c (n "mles") (v "0.12.0") (d (list (d (n "mles-utils") (r "^0.12") (d #t) (k 0)))) (h "0975sd6zgirdp3720rs5x0lv8k0wwvp0i0l32vsz9br75cq63z8d")))

(define-public crate-mles-0.13.0 (c (n "mles") (v "0.13.0") (d (list (d (n "mles-utils") (r "^0.13") (d #t) (k 0)))) (h "0g0qlaa9ddh05bjkzfh0865ayqzaklmmpizj8vyqpndfr0f0mgdq")))

(define-public crate-mles-0.14.0 (c (n "mles") (v "0.14.0") (d (list (d (n "mles-utils") (r "^0.14") (d #t) (k 0)))) (h "06bjnzb14vsw8c00ai5g89fx9mxsw44nm05k4fkljw6pl4p1wm4v")))

(define-public crate-mles-1.0.0-beta2 (c (n "mles") (v "1.0.0-beta2") (d (list (d (n "mles-utils") (r "^1.0.0-beta2") (d #t) (k 0)))) (h "0a4njdryg73zm8hwqp3j94lfm5c70pm6d7cm7g3ivj6frld1g0c6")))

(define-public crate-mles-0.15.0 (c (n "mles") (v "0.15.0") (d (list (d (n "mles-utils") (r "^0.15.0") (d #t) (k 0)))) (h "1bqmdmahyvibzyca9gf9jj539116fpwpsg05v10nws9rvqmvx2d6")))

(define-public crate-mles-0.16.0 (c (n "mles") (v "0.16.0") (d (list (d (n "mles-utils") (r "^0.16") (d #t) (k 0)))) (h "06c19f9yd9qzkxy7vp0b1k5f087zwq1nmdzxmjkjxkvi5l4rp6ay")))

(define-public crate-mles-1.0.0-beta3 (c (n "mles") (v "1.0.0-beta3") (d (list (d (n "mles-utils") (r "^0.16") (d #t) (k 0)))) (h "1jd05ic05g26dvi3s673bl6833h61f4rw9m5cn1myq8kwx71a328")))

(define-public crate-mles-0.17.0 (c (n "mles") (v "0.17.0") (d (list (d (n "mles-utils") (r "^0.17") (d #t) (k 0)))) (h "071268n65vwpdwy9jxpnpg0j1377g229pyxmwx2xfavd1qqgi0sa")))

(define-public crate-mles-1.0.0 (c (n "mles") (v "1.0.0") (d (list (d (n "mles-utils") (r "^1.0") (d #t) (k 0)))) (h "0zb01jwxa4ailgyaa25xpwdxrnp34614912vxybp9j46iq5iighp")))

(define-public crate-mles-1.0.1 (c (n "mles") (v "1.0.1") (d (list (d (n "mles-utils") (r "^1.0") (d #t) (k 0)))) (h "1vzniblv78vdicl1czydrl76di3p5zsamklh7h645615mq2gx1x1")))

(define-public crate-mles-1.0.2 (c (n "mles") (v "1.0.2") (d (list (d (n "mles-utils") (r "^1.0") (d #t) (k 0)))) (h "0b2xky8yz54n7j5hdccyy6fb51a515b3clfhry3iw985rzxq8ig6")))

(define-public crate-mles-1.0.3 (c (n "mles") (v "1.0.3") (d (list (d (n "mles-utils") (r "^1.0") (d #t) (k 0)))) (h "047llbkqkqkp2bnycinyn3lsvmvp1bhrbk0vfsvqlbbajnn19f4x")))

(define-public crate-mles-1.0.4 (c (n "mles") (v "1.0.4") (d (list (d (n "mles-utils") (r "^1.0") (d #t) (k 0)))) (h "0xr6ziwk7v3mhd24q7pmz2is6f48x68n63y8si3q4zsg87j12kmn")))

(define-public crate-mles-1.0.5 (c (n "mles") (v "1.0.5") (d (list (d (n "mles-utils") (r "^1.0") (d #t) (k 0)))) (h "1sib21b3fxrcyl2cr8i79wbrakvkanllhwr3l5dxqkajdj07811l")))

(define-public crate-mles-1.0.6 (c (n "mles") (v "1.0.6") (d (list (d (n "mles-utils") (r "^1.0") (d #t) (k 0)))) (h "1ikvfkfx0f278nr3yhxwiw42rbw7mn8841s4y4f86b1wrc0nb3fn")))

(define-public crate-mles-1.1.0 (c (n "mles") (v "1.1.0") (d (list (d (n "mles-utils") (r "^1.1") (d #t) (k 0)))) (h "1gxb2ab28hkqj1znhq0rpzmc09gqaafjlgq83ywrjrg9sy9v8573")))

(define-public crate-mles-1.1.1 (c (n "mles") (v "1.1.1") (d (list (d (n "mles-utils") (r "^1.1") (d #t) (k 0)))) (h "1fix4va91qyxsm50sgg07frc1i5rd0gn04fn4s9qrvagv3ir06mb")))

(define-public crate-mles-1.1.2 (c (n "mles") (v "1.1.2") (d (list (d (n "mles-utils") (r "^1.1") (d #t) (k 0)))) (h "1qb2xh5nfjyn4h7fm4aarrsn138bniy3jyl6xdgdpv5h280pwhds")))

(define-public crate-mles-1.1.5 (c (n "mles") (v "1.1.5") (d (list (d (n "mles-utils") (r "^1.1") (d #t) (k 0)))) (h "0f9abbmn8xcjiy31rb86jzffvrk28v42nn9rgr4kd7f8a3nyh320")))

(define-public crate-mles-1.1.6 (c (n "mles") (v "1.1.6") (d (list (d (n "mles-utils") (r "^1.1") (d #t) (k 0)))) (h "1vh56hk86il0zddwyc5v9vh34x5zj15q2mv8kv11l2wzrd4zhpah")))

(define-public crate-mles-1.1.7 (c (n "mles") (v "1.1.7") (d (list (d (n "mles-utils") (r "^1.1") (d #t) (k 0)))) (h "1wxb7ks8qyg9zjs1cki9ayxr54hsiiwnyfmcbk7h01lpznpn87g8")))

(define-public crate-mles-2.0.0 (c (n "mles") (v "2.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.7") (f (quote ("tokio"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1.0") (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.24.1") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.9") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.3") (f (quote ("compat"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.4") (d #t) (k 0)))) (h "10zgqvzlxyjz38vnlssvbfmj24xixwz45515lsr70d3xybfb1pfc")))

(define-public crate-mles-2.1.0 (c (n "mles") (v "2.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.9.1") (f (quote ("tokio"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.25") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("compat"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (d #t) (k 0)))) (h "0iv1l2z28czpdjzl3ibiy4v01swy2wcl36in0rv506zx8w1wkd8l")))

(define-public crate-mles-2.2.0 (c (n "mles") (v "2.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.9.1") (f (quote ("tokio"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.25") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("compat"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (d #t) (k 0)))) (h "0247m1c0s4ng2nj3r6hp48qmqpwfp8y2i2wmllwck6qwqb47ip04")))

(define-public crate-mles-2.2.1 (c (n "mles") (v "2.2.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.9.1") (f (quote ("tokio"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.25") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("compat"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (d #t) (k 0)))) (h "1w1vcpfcrc70my9nfk8iv6vyazl1p3krlb03j9dgpwizmz4vcig5")))

(define-public crate-mles-2.2.2 (c (n "mles") (v "2.2.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (k 0)) (d (n "http-types") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.9.1") (f (quote ("tokio"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.25") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("compat"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (d #t) (k 0)))) (h "0sm31r90n6g75wpnpwn8fz2yf5dmxg4m8ib03vqgmczy7ssxri61")))

(define-public crate-mles-2.2.3 (c (n "mles") (v "2.2.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (k 0)) (d (n "http-types") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.9.2") (f (quote ("tokio" "ring"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.25") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("compat"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (d #t) (k 0)))) (h "0j8jal6gf3a8vr3nkz1k4xxi9rdz1yp9i02cjrvrjd0fg8i9z32n")))

(define-public crate-mles-2.2.4 (c (n "mles") (v "2.2.4") (d (list (d (n "async-compression") (r "^0.4.6") (f (quote ("tokio" "brotli"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (k 0)) (d (n "http-types") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.9.2") (f (quote ("tokio" "ring"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.25") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("compat"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (d #t) (k 0)))) (h "0mybjr4c10kmmym4dpnkab52z7fhswb8580ildxhgra2gv8sggcx")))

(define-public crate-mles-2.2.5 (c (n "mles") (v "2.2.5") (d (list (d (n "async-compression") (r "^0.4.6") (f (quote ("tokio" "brotli" "zstd"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (k 0)) (d (n "http-types") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.9.2") (f (quote ("tokio" "ring"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.25") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("compat"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (d #t) (k 0)))) (h "0nnz5gig2lhan20l4bk0kbirz07lsjv6r893q2m7algldnq5gcdw")))

(define-public crate-mles-2.2.6 (c (n "mles") (v "2.2.6") (d (list (d (n "async-compression") (r "^0.4.6") (f (quote ("tokio" "brotli" "zstd"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (k 0)) (d (n "http-types") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.9.2") (f (quote ("tokio" "ring"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.25") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("compat"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (d #t) (k 0)))) (h "1bbx06kk53fxg6hccm2dinkvm4dwr8kqijn6wp777yivg6ywlivs")))

(define-public crate-mles-2.2.7 (c (n "mles") (v "2.2.7") (d (list (d (n "async-compression") (r "^0.4.6") (f (quote ("tokio" "brotli" "zstd"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (k 0)) (d (n "http-types") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.9.2") (f (quote ("tokio" "ring"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.25") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("compat"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (d #t) (k 0)))) (h "0bl8ndnv66ajk67sgkzxmqi5vxd7dvd06q5mlpc9kscxmm5h9l84")))

(define-public crate-mles-2.2.8 (c (n "mles") (v "2.2.8") (d (list (d (n "async-compression") (r "^0.4.6") (f (quote ("tokio" "brotli" "zstd"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (k 0)) (d (n "http-types") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.9.2") (f (quote ("tokio" "ring"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.25") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("compat"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (d #t) (k 0)))) (h "1wcvk0aa4gag9pvphyrks6zacrr1hi33d3ipsddgivxfi77pnb10")))

(define-public crate-mles-2.2.9 (c (n "mles") (v "2.2.9") (d (list (d (n "async-compression") (r "^0.4.6") (f (quote ("tokio" "brotli" "zstd"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (k 0)) (d (n "http-types") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.9.2") (f (quote ("tokio" "ring"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.25") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("compat"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (d #t) (k 0)))) (h "1wp7jv3f3bdfw4wbv75ci5l9fw3mw7hkskniwbcpzr2fb909gr5s")))

(define-public crate-mles-2.2.10 (c (n "mles") (v "2.2.10") (d (list (d (n "async-compression") (r "^0.4.6") (f (quote ("tokio" "brotli" "zstd"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (k 0)) (d (n "http-types") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.9.2") (f (quote ("tokio" "ring"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.25") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("compat"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.6") (d #t) (k 0)))) (h "11lpgd963d5xx57mn5jw8zq09xn15jczcx0dmv0spbhmkcliy9jh")))

