(define-module (crates-io ml fl mlflow) #:use-module (crates-io))

(define-public crate-mlflow-0.1.0 (c (n "mlflow") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "nanorand") (r "^0.4.4") (d #t) (k 2)) (d (n "nanoserde") (r "^0.1.20") (d #t) (k 0)) (d (n "pico-args") (r "^0.3.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "ureq") (r "^1.5.2") (f (quote ("tls"))) (k 0)))) (h "1p352hzq97zpia3galwh45prc27kg7pp5aw951l7zr82gwr5gsx0")))

