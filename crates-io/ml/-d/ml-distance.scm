(define-module (crates-io ml -d ml-distance) #:use-module (crates-io))

(define-public crate-ml-distance-0.1.0 (c (n "ml-distance") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)))) (h "1n94zhadw6cvlync8fvsy4b8iyx4l5dn9c9jfcc5ya26g9q8ic9k")))

(define-public crate-ml-distance-0.2.0 (c (n "ml-distance") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)))) (h "15l56vvdxl0xsklf2har0lhwd77n0g1jivx6hmbzm2snbd99v400")))

(define-public crate-ml-distance-0.2.1 (c (n "ml-distance") (v "0.2.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)))) (h "1817wk910y6ymrsn56pnfnawh6fnw18jmphg4hwpj5mnmgc3b1qm")))

(define-public crate-ml-distance-0.2.3 (c (n "ml-distance") (v "0.2.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)))) (h "1xz5kr74a4z43yph0kqh9k0dp0d4zx90pb66k931pb98frg93lsk")))

(define-public crate-ml-distance-1.0.0 (c (n "ml-distance") (v "1.0.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)))) (h "00w04p0gkb62czg48b6y7vvghhygbg7hkg31749v2fvjwm6ndn0r")))

(define-public crate-ml-distance-1.0.1 (c (n "ml-distance") (v "1.0.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)))) (h "0hx44irrcs9fm8q8sssc50in1lzpgh23yr0jca48cifdxhrdjhqw")))

