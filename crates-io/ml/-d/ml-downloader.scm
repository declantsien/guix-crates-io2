(define-module (crates-io ml -d ml-downloader) #:use-module (crates-io))

(define-public crate-ml-downloader-0.1.0 (c (n "ml-downloader") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "readme-rustdocifier") (r "^0.1.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 2)))) (h "1f22pr3rkkpvxww4zly88fxhg2x0czwi1fcimzm21qlmayddn7v1")))

(define-public crate-ml-downloader-0.1.1 (c (n "ml-downloader") (v "0.1.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "readme-rustdocifier") (r "^0.1.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 2)))) (h "11xql3rcywi07khy3yxky9jsqdg8aqa54xr49hyrhgzk88gfzbr5")))

