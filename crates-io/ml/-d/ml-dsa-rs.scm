(define-module (crates-io ml -d ml-dsa-rs) #:use-module (crates-io))

(define-public crate-ml-dsa-rs-0.1.0 (c (n "ml-dsa-rs") (v "0.1.0") (h "1sbclas0y17c3vac7x0v47lmhp1snpyn1p0d0h9jp5s7w3fmk95b")))

(define-public crate-ml-dsa-rs-0.1.1 (c (n "ml-dsa-rs") (v "0.1.1") (h "0i01qavi4mb432pys9jqiqcdzpfq1qx9b0imx94n5abbg2b6lhn9")))

