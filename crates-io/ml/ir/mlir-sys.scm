(define-module (crates-io ml ir mlir-sys) #:use-module (crates-io))

(define-public crate-mlir-sys-0.1.0 (c (n "mlir-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0xj9s3az2dqgj5mrknwpkr2kp26sbmxaln00rsswdqy3dln5syjj") (l "MLIR")))

(define-public crate-mlir-sys-0.1.1 (c (n "mlir-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1y1z7di5s4wbinp55p4mxh0lxpxsxw8r594lwm54ppgnyhdij7xb") (l "MLIR")))

(define-public crate-mlir-sys-0.1.2 (c (n "mlir-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1nnhf55gn9nnaxjrdhdc034mfpq13m22ky97m8w4dlpnggihh846") (l "MLIR")))

(define-public crate-mlir-sys-0.1.3 (c (n "mlir-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "15n8ll2an6pma6l4jqiqxshd1wlq2g36s1gpqihchnv8l33501xa") (l "MLIR")))

(define-public crate-mlir-sys-0.1.4 (c (n "mlir-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1wnxxjsdly596h8kbf6wj2xnbgg7i9aq1d4ksnrj7f0iipllc2ki") (l "MLIR")))

(define-public crate-mlir-sys-0.2.0 (c (n "mlir-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1cwkl5kg4c05d3iy7li9h9k2zsyj9wp74rv3bam9xxfmi3i2n9n1") (l "MLIR")))

(define-public crate-mlir-sys-0.2.1 (c (n "mlir-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "16k9yq55iyd4k5s3sl0kgcg1k2cv1crkixb0j3ymj9zdj59rmqfm") (l "MLIR")))

(define-public crate-mlir-sys-0.2.2 (c (n "mlir-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "0zqvy65dnj172cjx6s8rsi2h1r10vp5bhpafbk6mkj9vlq35h6qv") (l "MLIR")))

