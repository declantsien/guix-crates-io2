(define-module (crates-io ml o- mlo-chacha20-rs) #:use-module (crates-io))

(define-public crate-mlo-chacha20-rs-0.1.0 (c (n "mlo-chacha20-rs") (v "0.1.0") (h "1riwqdwpxg3f2kdj2p4ilkmgss5hmgjz5szwv1jx0ihx9y29g32l")))

(define-public crate-mlo-chacha20-rs-0.1.1 (c (n "mlo-chacha20-rs") (v "0.1.1") (h "1syykr68n8f368zqbrbmgp1b15kywzq0m5gkv5g28lzim4fnccs4")))

(define-public crate-mlo-chacha20-rs-0.1.2 (c (n "mlo-chacha20-rs") (v "0.1.2") (h "1xiifyl2xyrwk00nknnw6bfhfsjdm01ld5s1ls6lp4dpv6ppxz2k")))

