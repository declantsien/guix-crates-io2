(define-module (crates-io ml og mlog) #:use-module (crates-io))

(define-public crate-mlog-0.0.1 (c (n "mlog") (v "0.0.1") (h "1ys4bm4ijkybwp0hk11k0nkknhws3hckh8jkwd88dhgb7p8yw4yr") (y #t)))

(define-public crate-mlog-0.1.0 (c (n "mlog") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)))) (h "1qv3y88zrbvxc134mjg1di5qkyldwbkj1dshw7bm7z3rzgqhsgpg")))

(define-public crate-mlog-0.2.0 (c (n "mlog") (v "0.2.0") (d (list (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)))) (h "0z9ab7y82k4g810wxiyyhcpk984mnn6c8s48fglc2mqj5nf2dlpj")))

(define-public crate-mlog-0.2.1 (c (n "mlog") (v "0.2.1") (d (list (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)))) (h "01xhc1bmdp20bjg2arcr6bds15vjah7dfqk86dc34a7vs1i51m8f")))

(define-public crate-mlog-0.3.0 (c (n "mlog") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)))) (h "1y5nyl6m1w6i12wc91s7yva08h1fgcmjpdj8sdr9insk5vcgky85")))

(define-public crate-mlog-0.3.1 (c (n "mlog") (v "0.3.1") (d (list (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)))) (h "1lcq6xzbv2d65djy99p9wjbw7an57rdw7mv48cqq431bxw6rgkj5")))

