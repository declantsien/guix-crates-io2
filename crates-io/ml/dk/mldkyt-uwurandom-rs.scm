(define-module (crates-io ml dk mldkyt-uwurandom-rs) #:use-module (crates-io))

(define-public crate-mldkyt-uwurandom-rs-0.1.0 (c (n "mldkyt-uwurandom-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1j65j7jgvx7301cjjka9px752h0s8pxicrg8jmq10h747gy7ivim")))

(define-public crate-mldkyt-uwurandom-rs-0.1.1 (c (n "mldkyt-uwurandom-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1lmyg4k9vwgf5819hiy8shb5dprj1mbpzq4qjlh33kk3sb59id43")))

