(define-module (crates-io ml ua mlua-crc16) #:use-module (crates-io))

(define-public crate-mlua-crc16-1.0.0 (c (n "mlua-crc16") (v "1.0.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "mlua") (r "^0.9.1") (d #t) (k 0)))) (h "04qmz28zm637nqsm9hiciwp5a2ysqs2xqykn0gjj0fjxh6wlp4fq") (f (quote (("luajit52" "mlua/luajit52" "mlua/vendored") ("luajit" "mlua/luajit" "mlua/vendored") ("lua54" "mlua/lua54" "mlua/vendored") ("lua53" "mlua/lua53" "mlua/vendored") ("lua52" "mlua/lua52" "mlua/vendored") ("lua51" "mlua/lua51" "mlua/vendored") ("default" "luajit"))))))

(define-public crate-mlua-crc16-1.0.1 (c (n "mlua-crc16") (v "1.0.1") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "mlua") (r "^0.9.5") (d #t) (k 0)))) (h "126219bv4sq8z4pzmzrplpm6a44g0k4pr87k26mqdy1mazy0arr7") (f (quote (("luajit52" "mlua/luajit52" "mlua/vendored") ("luajit" "mlua/luajit" "mlua/vendored") ("lua54" "mlua/lua54" "mlua/vendored") ("lua53" "mlua/lua53" "mlua/vendored") ("lua52" "mlua/lua52" "mlua/vendored") ("lua51" "mlua/lua51" "mlua/vendored"))))))

