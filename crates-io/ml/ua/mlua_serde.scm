(define-module (crates-io ml ua mlua_serde) #:use-module (crates-io))

(define-public crate-mlua_serde-0.5.0 (c (n "mlua_serde") (v "0.5.0") (d (list (d (n "mlua") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0sjn78id1d1h9xm8zdllmnxb53ld8306b7zks54msw3spzdycz6s") (f (quote (("vendored" "mlua/vendored") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53") ("default"))))))

(define-public crate-mlua_serde-0.5.1 (c (n "mlua_serde") (v "0.5.1") (d (list (d (n "mlua") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1247sbpkx6jjwv6z6cmbg8jpixybkr5lvhlgby4xp3bc6483vyxh") (f (quote (("vendored" "mlua/vendored") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53") ("default"))))))

(define-public crate-mlua_serde-0.6.0 (c (n "mlua_serde") (v "0.6.0") (d (list (d (n "mlua") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "11f5zkw2jxc6fwyk6dbjscn575jkrgqdg8b63li2c9asqqhdsyhf") (f (quote (("vendored" "mlua/vendored") ("luajit" "mlua/luajit") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53") ("lua52" "mlua/lua52") ("lua51" "mlua/lua51") ("default"))))))

