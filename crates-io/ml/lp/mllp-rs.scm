(define-module (crates-io ml lp mllp-rs) #:use-module (crates-io))

(define-public crate-mllp-rs-0.1.0 (c (n "mllp-rs") (v "0.1.0") (h "1xd044dj402hyvdbi2b2d76qps384bq0676vbpkm8m7vn4cj0mxb")))

(define-public crate-mllp-rs-0.1.1 (c (n "mllp-rs") (v "0.1.1") (h "1kncajk0sm5hiijvwmyrgzdqz5zxbwzy568kc4gkhj60pwy43vsa")))

