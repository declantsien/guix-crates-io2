(define-module (crates-io ml ib mlib) #:use-module (crates-io))

(define-public crate-mlib-0.1.0 (c (n "mlib") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "11zsh7fl7b798j75kpgp03b5x56dmvnng9m5z18mn1d2n8zhcw52")))

(define-public crate-mlib-0.1.1 (c (n "mlib") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1qpkvai8gn3mm99fyr2lkp4kkkygvdc7nisavhrixm25qn0yaxl4")))

(define-public crate-mlib-0.1.2 (c (n "mlib") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "04771p15pbqqz6c8nw1qmjwq9j026jxzy2sama86pymk6k67rjql")))

