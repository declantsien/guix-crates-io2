(define-module (crates-io ml zu mlzutil) #:use-module (crates-io))

(define-public crate-mlzutil-0.1.0 (c (n "mlzutil") (v "0.1.0") (d (list (d (n "dns-lookup") (r "^0.9.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.4") (d #t) (k 0)) (d (n "interfaces") (r "^0.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "15ls5h6sg341nlc2pd3lv4fir40nkm8igm3is430i392jyz1nfxa")))

(define-public crate-mlzutil-0.1.1 (c (n "mlzutil") (v "0.1.1") (d (list (d (n "dns-lookup") (r "^0.9.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.4") (d #t) (k 0)) (d (n "interfaces") (r "^0.0.4") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "08k0s1qimj1k91fw9qr032jangjzyx573zq5x9m62qcrrw8v62d4")))

(define-public crate-mlzutil-0.2.0 (c (n "mlzutil") (v "0.2.0") (d (list (d (n "dns-lookup") (r "^1.0.1") (d #t) (k 0)) (d (n "hostname") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "06zqm7kzf3zc3na4iv2r6g91bcmnrhy54bgrsbnrnxrw9sczglqq")))

(define-public crate-mlzutil-0.2.1 (c (n "mlzutil") (v "0.2.1") (d (list (d (n "dns-lookup") (r "^1.0.1") (d #t) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "systemstat") (r "<=0.1.5") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "1h5wxdpwas0i9fg62wdxwjxbr6ydjz2bz84vd6qi6zchv9q3pb2i")))

(define-public crate-mlzutil-0.2.2 (c (n "mlzutil") (v "0.2.2") (d (list (d (n "dns-lookup") (r "^1.0.1") (d #t) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.5") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "0nzcbh285p77ryrwlznxpaargjg8308yl5gicszfgny25wl91ibm")))

(define-public crate-mlzutil-0.3.0 (c (n "mlzutil") (v "0.3.0") (d (list (d (n "dns-lookup") (r "^2.0.2") (d #t) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "systemstat") (r "^0.2.3") (d #t) (k 0)) (d (n "time") (r "=0.3.20") (d #t) (k 0)))) (h "13hj6s4amsa0lcnd904f50b4r21pcmmnzknmqsn5zyfmsp0vlnyv")))

