(define-module (crates-io ml nx mlnx-ofed-librdmacm-sys) #:use-module (crates-io))

(define-public crate-mlnx-ofed-librdmacm-sys-0.0.4 (c (n "mlnx-ofed-librdmacm-sys") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.4") (d #t) (k 0)))) (h "0v8y6r61wxcssbsrdxh3kl8064bs4k8yv431igakarg9ajhi2sac")))

(define-public crate-mlnx-ofed-librdmacm-sys-0.0.5 (c (n "mlnx-ofed-librdmacm-sys") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.5") (d #t) (k 0)))) (h "0slf73hpsgi89fdi3qnnw73ids6p3606rd9i26shg82176mn57y4")))

(define-public crate-mlnx-ofed-librdmacm-sys-0.0.6 (c (n "mlnx-ofed-librdmacm-sys") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.6") (d #t) (k 0)))) (h "0hs12zpvxlrgcv8bq2rvc16sk08di2lzd6xwj01gacmgvq76hxbd")))

