(define-module (crates-io ml nx mlnx-ofed-libibverbs-sys) #:use-module (crates-io))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.0 (c (n "mlnx-ofed-libibverbs-sys") (v "0.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0r81g396z4ayrkph4y2pkblq08h10r0pf0d5wswr95y2jyycsbnd")))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.1 (c (n "mlnx-ofed-libibverbs-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0lhji96llzsnfr28vix5c70fibf72kar08gxqn7gasfvd1hgzsip")))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.2 (c (n "mlnx-ofed-libibverbs-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "03w3g4i3hxajd9sf83kmb9gdfh0acwvz42jy0n1cdlkj975dn88d")))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.3 (c (n "mlnx-ofed-libibverbs-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0i90a0idnd8zbyhf4qk3mg75ipf224m0l2qr37ig3bcbbg5xbhch")))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.4 (c (n "mlnx-ofed-libibverbs-sys") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "15iimy4xqd41jskb783r2sc56a6mq22plmn49bhgr1g2mnj7zw81")))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.5 (c (n "mlnx-ofed-libibverbs-sys") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "089c2mjl6p7pzs4yxydd375zdjaz03a9gras424qs7cvzn1q94pw")))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.6 (c (n "mlnx-ofed-libibverbs-sys") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0ysympapr5cxsbnrbxdqcgxxalbd9k6aipmr0zfg2nd505fqws48")))

