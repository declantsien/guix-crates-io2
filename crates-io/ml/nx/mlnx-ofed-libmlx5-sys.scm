(define-module (crates-io ml nx mlnx-ofed-libmlx5-sys) #:use-module (crates-io))

(define-public crate-mlnx-ofed-libmlx5-sys-0.0.3 (c (n "mlnx-ofed-libmlx5-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libnuma-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.3") (d #t) (k 0)))) (h "059xa76qvs07arnvbbd13haw5spfaakbx0kn7am4yjyyxgfl5avs")))

(define-public crate-mlnx-ofed-libmlx5-sys-0.0.4 (c (n "mlnx-ofed-libmlx5-sys") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libnuma-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.4") (d #t) (k 0)))) (h "01p55kmsdg5nb9za4zps8vl68rdvzryf5q9aj4v3lvldqd96z8gz")))

(define-public crate-mlnx-ofed-libmlx5-sys-0.0.5 (c (n "mlnx-ofed-libmlx5-sys") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libnuma-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.5") (d #t) (k 0)))) (h "0x3769jg2chg5xs4bahzbvc24him32pfcnf1351gl3fnnbyai02a")))

(define-public crate-mlnx-ofed-libmlx5-sys-0.0.6 (c (n "mlnx-ofed-libmlx5-sys") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.6") (d #t) (k 0)))) (h "0wi32d6c3cf479w6lyfwk7yfilnfcygkwlhv56jwmbdrdiai0qs4")))

(define-public crate-mlnx-ofed-libmlx5-sys-0.0.7 (c (n "mlnx-ofed-libmlx5-sys") (v "0.0.7") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.6") (d #t) (k 0)))) (h "0j54g1yqly11nb4acm9j83awj2dqp3g8d02ywzn5xyjkpz2m683f")))

