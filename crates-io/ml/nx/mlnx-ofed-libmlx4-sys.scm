(define-module (crates-io ml nx mlnx-ofed-libmlx4-sys) #:use-module (crates-io))

(define-public crate-mlnx-ofed-libmlx4-sys-0.0.7 (c (n "mlnx-ofed-libmlx4-sys") (v "0.0.7") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.6") (d #t) (k 0)))) (h "0ji1hcdlxm3b9gsyhmdqsddifx9rsfrqrmpk8nzi0vq34imncj6y")))

(define-public crate-mlnx-ofed-libmlx4-sys-0.0.8 (c (n "mlnx-ofed-libmlx4-sys") (v "0.0.8") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.6") (d #t) (k 0)))) (h "0qnc17g4i9d6y8s6vkx20nn5gsmkx62h8hw1c779xgxglis4hkl4")))

(define-public crate-mlnx-ofed-libmlx4-sys-0.0.9 (c (n "mlnx-ofed-libmlx4-sys") (v "0.0.9") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.6") (d #t) (k 0)))) (h "19vcvwl1y0zynh2qx7qrlnm2914d1ww5zj81n1jfcaanvrxybnl5")))

(define-public crate-mlnx-ofed-libmlx4-sys-0.0.10 (c (n "mlnx-ofed-libmlx4-sys") (v "0.0.10") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.6") (d #t) (k 0)))) (h "1gymhpnp040j2lplssf5fimd70hs9fng8wx426zqws72s727l46h")))

