(define-module (crates-io ml sn mlsnd) #:use-module (crates-io))

(define-public crate-mlsnd-0.1.0 (c (n "mlsnd") (v "0.1.0") (d (list (d (n "fastrand") (r "^1") (o #t) (d #t) (k 0)) (d (n "moving-least-squares") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "cargo-release") (r "^0.24") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0hpmw613l97743x24ispxd7sx0grvqhpv1j40ppxnhv8ji9zmigf") (f (quote (("bench" "fastrand" "moving-least-squares"))))))

(define-public crate-mlsnd-0.1.1 (c (n "mlsnd") (v "0.1.1") (d (list (d (n "fastrand") (r "^1") (o #t) (d #t) (k 0)) (d (n "moving-least-squares") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "cargo-release") (r "^0.24") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "04hbnk8sb68glrvxzlmji10j8p9w9c4nkmrsxrgzvd3kd3gqjsaq") (f (quote (("bench" "fastrand" "moving-least-squares"))))))

