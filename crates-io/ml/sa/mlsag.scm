(define-module (crates-io ml sa mlsag) #:use-module (crates-io))

(define-public crate-mlsag-0.1.0 (c (n "mlsag") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "merlin") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "02f10awzk188r7zcvqz9fi9f0y4rcn5zpq6w0xrkr2gqvnrf22jm")))

(define-public crate-mlsag-0.2.0 (c (n "mlsag") (v "0.2.0") (d (list (d (n "curve25519-dalek") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "merlin") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0zbp9d9g7scijkf6wb4cmqa4fmmn9h22s63m81711glikxnzrr6l")))

(define-public crate-mlsag-0.3.0 (c (n "mlsag") (v "0.3.0") (d (list (d (n "curve25519-dalek") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "merlin") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "1wg3zwgkhpnagpxzs36qwc2jrnx0wnwwbc1q269r0lsrh99b13pa")))

