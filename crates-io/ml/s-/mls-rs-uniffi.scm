(define-module (crates-io ml s- mls-rs-uniffi) #:use-module (crates-io))

(define-public crate-mls-rs-uniffi-0.1.0 (c (n "mls-rs-uniffi") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "maybe-async") (r "^0.2.10") (d #t) (k 0)) (d (n "mls-rs") (r "^0.39.0") (d #t) (k 0)) (d (n "mls-rs-core") (r "^0.18.0") (d #t) (k 0)) (d (n "mls-rs-crypto-openssl") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("sync"))) (d #t) (t "cfg(mls_build_async)") (k 0)) (d (n "uniffi") (r "^0.26.0") (d #t) (k 0)) (d (n "uniffi_bindgen") (r "^0.26.0") (d #t) (k 2)))) (h "0z3g8kgschzjnjh1h87j0q0jmclm212w5rdgnykmnmy4f9bygs4p") (r "1.68.2")))

