(define-module (crates-io ml s- mls-rs-crypto-traits) #:use-module (crates-io))

(define-public crate-mls-rs-crypto-traits-0.6.0 (c (n "mls-rs-crypto-traits") (v "0.6.0") (d (list (d (n "mls-rs-core") (r "^0.14.0") (k 0)) (d (n "mockall") (r "^0.11") (o #t) (d #t) (k 0)))) (h "02w8i5mzibn4zxsd8b9m96hj9xmyj3g3wc2lnxzhkxllgn61a1bm") (f (quote (("std" "mls-rs-core/std") ("default" "std")))) (s 2) (e (quote (("mock" "std" "dep:mockall"))))))

(define-public crate-mls-rs-crypto-traits-0.6.1 (c (n "mls-rs-crypto-traits") (v "0.6.1") (d (list (d (n "mls-rs-core") (r "^0.14.0") (k 0)) (d (n "mockall") (r "^0.11") (o #t) (d #t) (k 0)))) (h "13zi6hklnixcxckj036yjddf6bv8m85bvlihmjd4jyj5qgv28im0") (f (quote (("std" "mls-rs-core/std") ("default" "std")))) (s 2) (e (quote (("mock" "std" "dep:mockall"))))))

(define-public crate-mls-rs-crypto-traits-0.7.0 (c (n "mls-rs-crypto-traits") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (t "cfg(mls_build_async)") (k 0)) (d (n "maybe-async") (r "^0.2.7") (d #t) (k 0)) (d (n "mls-rs-core") (r "^0.15.0") (k 0)) (d (n "mockall") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1wb6nhq3kx9c4ibxbcakkhin9pcdk4ajk4ficp3s56agajh5y8ly") (f (quote (("std" "mls-rs-core/std") ("default" "std")))) (s 2) (e (quote (("mock" "std" "dep:mockall"))))))

(define-public crate-mls-rs-crypto-traits-0.8.0 (c (n "mls-rs-crypto-traits") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (t "cfg(mls_build_async)") (k 0)) (d (n "maybe-async") (r "^0.2.7") (d #t) (k 0)) (d (n "mls-rs-core") (r "^0.16.0") (k 0)) (d (n "mockall") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1cpkmn7smlskrgigqf3p0n73azi1f2lg6nmddlag82gqjf40j99v") (f (quote (("std" "mls-rs-core/std") ("default" "std")))) (s 2) (e (quote (("mock" "std" "dep:mockall"))))))

(define-public crate-mls-rs-crypto-traits-0.9.0 (c (n "mls-rs-crypto-traits") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (t "cfg(mls_build_async)") (k 0)) (d (n "maybe-async") (r "^0.2.7") (d #t) (k 0)) (d (n "mls-rs-core") (r "^0.17.0") (k 0)) (d (n "mockall") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1pasxcx1b05h1iv7dkl853ngda6yl1175x77ab5nx6g6591cm5j0") (f (quote (("std" "mls-rs-core/std") ("default" "std")))) (s 2) (e (quote (("mock" "std" "dep:mockall"))))))

(define-public crate-mls-rs-crypto-traits-0.10.0 (c (n "mls-rs-crypto-traits") (v "0.10.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (t "cfg(mls_build_async)") (k 0)) (d (n "maybe-async") (r "^0.2.10") (d #t) (k 0)) (d (n "mls-rs-core") (r "^0.18.0") (k 0)) (d (n "mockall") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0hqx8xa8jbhb43qfmr16maflxvw7mhhxjg7kgazvj4w4xw1hnrxg") (f (quote (("std" "mls-rs-core/std") ("default" "std")))) (s 2) (e (quote (("mock" "std" "dep:mockall"))))))

