(define-module (crates-io ml s- mls-rs-codec-derive) #:use-module (crates-io))

(define-public crate-mls-rs-codec-derive-0.1.0 (c (n "mls-rs-codec-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0aw745mprsj7smav49r1bmx9kvck1i2jn3r5iivfq4w7nqjvygvg")))

(define-public crate-mls-rs-codec-derive-0.1.1 (c (n "mls-rs-codec-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1wxr38cr0g7960ywxy8vhr078wllsbqivw1sph5hsgaxzxhp4ki6")))

