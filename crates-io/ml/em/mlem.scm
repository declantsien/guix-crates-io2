(define-module (crates-io ml em mlem) #:use-module (crates-io))

(define-public crate-mlem-0.1.0 (c (n "mlem") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)))) (h "15sc15y3dggc0ymzf03575ksxii2jllz63ha741pzy9xs0a3s5ra")))

