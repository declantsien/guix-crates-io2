(define-module (crates-io ml -p ml-progress) #:use-module (crates-io))

(define-public crate-ml-progress-0.1.0 (c (n "ml-progress") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "readme-rustdocifier") (r "^0.1.1") (d #t) (k 1)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "04xw87gx9i4ln4an9gz3in2pgvlcp8p4i4nrb8sk4gii3np9kzkq")))

