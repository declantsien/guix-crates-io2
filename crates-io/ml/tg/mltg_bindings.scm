(define-module (crates-io ml tg mltg_bindings) #:use-module (crates-io))

(define-public crate-mltg_bindings-0.0.0 (c (n "mltg_bindings") (v "0.0.0") (d (list (d (n "windows") (r "^0.10.0") (d #t) (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (k 1)))) (h "1gkmvg2bj4dlzxv17ldysjyx2mqj8zv1khjdwwsd60w16j3r3q73")))

(define-public crate-mltg_bindings-0.0.1 (c (n "mltg_bindings") (v "0.0.1") (d (list (d (n "windows") (r "^0.11.0") (d #t) (k 0)) (d (n "windows") (r "^0.11.0") (d #t) (k 1)))) (h "065ssqqh77igarafdvsblq25f2kvkvar22q18jmdks9w3wwjy1yn")))

(define-public crate-mltg_bindings-0.0.2 (c (n "mltg_bindings") (v "0.0.2") (d (list (d (n "windows") (r "^0.13.0") (d #t) (k 0)) (d (n "windows") (r "^0.13.0") (d #t) (k 1)))) (h "1w1silimrh4ks6432r51h790cnblf7skv9yggc98ak1rp2ck4sdz")))

(define-public crate-mltg_bindings-0.0.3 (c (n "mltg_bindings") (v "0.0.3") (d (list (d (n "windows") (r "^0.18.0") (d #t) (k 0)) (d (n "windows") (r "^0.18.0") (d #t) (k 1)))) (h "0i68gc95ajknq5zy2qwkrq49wai3pdy6p9f8rvkxv3haf6d4nlqw")))

(define-public crate-mltg_bindings-0.1.0 (c (n "mltg_bindings") (v "0.1.0") (d (list (d (n "windows") (r "^0.19.0") (d #t) (k 0)) (d (n "windows") (r "^0.19.0") (d #t) (k 1)))) (h "0nyshlss5yyahirfx70825hyn2q1y71inhd8yz6959npx41275gy")))

(define-public crate-mltg_bindings-0.2.0 (c (n "mltg_bindings") (v "0.2.0") (d (list (d (n "windows") (r "^0.20.0") (d #t) (k 0)) (d (n "windows") (r "^0.20.0") (d #t) (k 1)))) (h "0bgcdxz6hlfp3pv2skz0zrh1fmlkxz7ld6k4nrxw1gd9cd6aay47")))

(define-public crate-mltg_bindings-0.2.1 (c (n "mltg_bindings") (v "0.2.1") (d (list (d (n "windows") (r "^0.21.0") (d #t) (k 0)) (d (n "windows") (r "^0.21.0") (d #t) (k 1)))) (h "1sb0d2557h853r1n40dirfmrzaj79ancs4vps52qczbnyxn7bq2v")))

(define-public crate-mltg_bindings-0.2.2 (c (n "mltg_bindings") (v "0.2.2") (d (list (d (n "windows") (r "^0.21.1") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 1)))) (h "0mxab202wdj521glh702ffhdn37lcdiv22ah1nlhc0k0fdnjhrb6")))

