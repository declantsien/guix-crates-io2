(define-module (crates-io ml i_ mli_mep) #:use-module (crates-io))

(define-public crate-mli_mep-0.9.0 (c (n "mli_mep") (v "0.9.0") (d (list (d (n "mli") (r "^0.9") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)))) (h "0hg7azh4yhhcmj7f8wm25flzx5idmpjq61pg0xhrlgja8h37kb7g")))

(define-public crate-mli_mep-0.10.0 (c (n "mli_mep") (v "0.10.0") (d (list (d (n "mli") (r "^0.10") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "03cylik5f22m6cidqdkmh7r7wm7dfgbx7li83zflrjp53cz9b4da")))

(define-public crate-mli_mep-0.10.1 (c (n "mli_mep") (v "0.10.1") (d (list (d (n "mli") (r "^0.10") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1q8rihsj95pl7y6nzvd43dr3pj3wl2vib4vv6c8pqahpy2z9kdz6")))

(define-public crate-mli_mep-0.10.2 (c (n "mli_mep") (v "0.10.2") (d (list (d (n "mli") (r "^0.10") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1vgx2mypwdjifiggbd7bzk8fkpqp7brkk4h8jl5qzmyw03ld3112")))

(define-public crate-mli_mep-0.10.3 (c (n "mli_mep") (v "0.10.3") (d (list (d (n "mli") (r "^0.10") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "00rjbcpq0p4946329lrqzxfkszjc7c0bhjp2jl8zl13hy5h595s4")))

