(define-module (crates-io ml t- mlt-sys) #:use-module (crates-io))

(define-public crate-mlt-sys-0.1.0 (c (n "mlt-sys") (v "0.1.0") (h "0g2m9cvzwf5jzbxk47zx6vilqsys4jc9acm14ks04r640nxdm3f7") (l "mlt")))

(define-public crate-mlt-sys-0.1.1 (c (n "mlt-sys") (v "0.1.1") (d (list (d (n "os_type") (r "^2.0.0") (d #t) (k 1)))) (h "01ymy8mxha6nqpyr6pi5sb2k50ynmkp9icrghkicv9c9ly7vcfyk") (l "mlt")))

