(define-module (crates-io ml x9 mlx9061x) #:use-module (crates-io))

(define-public crate-mlx9061x-0.1.0 (c (n "mlx9061x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "04w86hivsjrl9snllhy6999m69b0rsmzs82jzw457f5rid9kc2y6")))

(define-public crate-mlx9061x-0.2.0 (c (n "mlx9061x") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "smbus-pec") (r "^1") (d #t) (k 0)))) (h "11amakii1v8m85015zpvg1yshp7by14apw6qklz29zsm37l4vxvq")))

(define-public crate-mlx9061x-0.2.1 (c (n "mlx9061x") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "smbus-pec") (r "^1") (d #t) (k 0)))) (h "01260cnjkibvnh2mdlc240x4ri47y0mxpv4yxwah1iybk5qv2bhy")))

(define-public crate-mlx9061x-0.3.0 (c (n "mlx9061x") (v "0.3.0") (d (list (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)) (d (n "smbus-pec") (r "^1") (d #t) (k 0)))) (h "1b1vimlpr4jixsx8v45zk5ifjsymnnmiypb758yhikk6r4mdj8nv") (s 2) (e (quote (("defmt-03" "dep:defmt" "embedded-hal/defmt-03"))))))

