(define-module (crates-io ml su mlsub) #:use-module (crates-io))

(define-public crate-mlsub-0.1.0 (c (n "mlsub") (v "0.1.0") (d (list (d (n "im") (r "~15") (d #t) (k 0)) (d (n "iter-set") (r "~2") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.4.1") (d #t) (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 2)) (d (n "seahash") (r "~4") (d #t) (k 0)) (d (n "small-ord-set") (r "^0.1.1") (d #t) (k 0)))) (h "19sh19pf07j0x68y7f62gyha3mr6rwh5immphx0rjay35i4kyzc9")))

