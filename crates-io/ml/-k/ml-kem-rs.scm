(define-module (crates-io ml -k ml-kem-rs) #:use-module (crates-io))

(define-public crate-ml-kem-rs-0.1.0 (c (n "ml-kem-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "0mhy32mi0wky1lhxmh4p3hm0bxxh0cym1yxd07i760dvhr37hvzv")))

(define-public crate-ml-kem-rs-0.1.1 (c (n "ml-kem-rs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "1h8yqddqwb5y33kkpvca3g0x73s84if232f3lh59l2fwl7zmg429") (f (quote (("ml_kem_768") ("ml_kem_512") ("ml_kem_1024") ("default" "ml_kem_512" "ml_kem_768" "ml_kem_1024")))) (r "1.72")))

(define-public crate-ml-kem-rs-0.1.2 (c (n "ml-kem-rs") (v "0.1.2") (h "01fv81b93b6bvln0jy2jgjznm77k8zq81ja0ry9jdiqkjldqylj7")))

