(define-module (crates-io ml -k ml-kem) #:use-module (crates-io))

(define-public crate-ml-kem-0.0.1 (c (n "ml-kem") (v "0.0.1") (h "1qzbvrsfjd12bw4a07p58fy1kmvc3chv7qf0w29snd9q82agxsb0") (y #t)))

(define-public crate-ml-kem-0.1.0-alpha (c (n "ml-kem") (v "0.1.0-alpha") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "hybrid-array") (r "^0.2.0-rc.7") (f (quote ("extra-sizes"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (k 0)))) (h "0jl3y3hb4k8bgnl5j6ra4j87zbng8n6w2qrkgb6fk7kccnpgxq9h") (f (quote (("std" "sha3/std") ("deterministic") ("default" "std")))) (r "1.74")))

(define-public crate-ml-kem-0.1.0 (c (n "ml-kem") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "hybrid-array") (r "^0.2.0-rc.8") (f (quote ("extra-sizes"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (k 0)))) (h "0qinhkl9bgybv0s22sh83wm086kqfli1ggv7gw6c5bm04dwg433s") (f (quote (("std" "sha3/std") ("deterministic") ("default" "std")))) (r "1.74")))

