(define-module (crates-io ml cr mlcr) #:use-module (crates-io))

(define-public crate-mlcr-0.1.0 (c (n "mlcr") (v "0.1.0") (d (list (d (n "nn") (r "^0.1") (d #t) (k 0)))) (h "1si9x1nx7a555yqd3kyr4i0aihm15wx3j47j684x5rar330b44r9")))

(define-public crate-mlcr-0.2.0 (c (n "mlcr") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "nn") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)))) (h "02agpzii3paplxahah9hwm0b29ipqqcdnlmx0vyzp1cskcppzdzk")))

