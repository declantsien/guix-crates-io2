(define-module (crates-io dt -t dt-tool) #:use-module (crates-io))

(define-public crate-dt-tool-0.1.0 (c (n "dt-tool") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "0a12a67l9ag8rnbkb24i5pfml31062ld0zg8rj9363r3p4plhv67") (y #t)))

(define-public crate-dt-tool-0.2.0 (c (n "dt-tool") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "1j72ns56xgv9jwb3phsbqkl9gvwc1m5dx8asd585hpfvbdng3qbn") (y #t)))

(define-public crate-dt-tool-0.3.0 (c (n "dt-tool") (v "0.3.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rail-lang") (r "^0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "0lc8hxj29dn2blr38rfs3n5r782kg2wbpslqy8w5a0rfip2kgsb0") (y #t)))

(define-public crate-dt-tool-0.3.1 (c (n "dt-tool") (v "0.3.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rail-lang") (r "^0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "1a5c589ydwhrvgh47php9jczxw72ibfrdj6z83fn4isplj14xh8h") (y #t)))

(define-public crate-dt-tool-0.3.2 (c (n "dt-tool") (v "0.3.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rail-lang") (r "^0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "0jyzngrp0iysh6c7rxg5d9jr4r7ar941806ir918kssdrfd9m8il") (y #t)))

(define-public crate-dt-tool-0.3.3 (c (n "dt-tool") (v "0.3.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rail-lang") (r "^0.22") (d #t) (k 0)))) (h "161sbkyipgvr61z10f416x0mravz23hbgks07i25414ssddmrag4") (y #t)))

(define-public crate-dt-tool-0.3.4 (c (n "dt-tool") (v "0.3.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rail-lang") (r "^0.22.2") (d #t) (k 0)))) (h "0i7br8fl1r38k27jj2nq7p1pgl7j1cr0iv1w1nxxgwlxij8gbqgi") (y #t)))

(define-public crate-dt-tool-0.4.0 (c (n "dt-tool") (v "0.4.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rail-lang") (r "^0.23.0") (d #t) (k 0)))) (h "0mj8ndaqf7qjzjz4q95jyp8a7qsz53arv0cni2cgzljnsz5jpsxi") (y #t)))

(define-public crate-dt-tool-0.4.1 (c (n "dt-tool") (v "0.4.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rail-lang") (r "^0.24.0") (d #t) (k 0)))) (h "150cs7wn5ig207az6pq094pqv0m5pqxzcyril729q70ni8v2ia29") (y #t)))

(define-public crate-dt-tool-0.4.2 (c (n "dt-tool") (v "0.4.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rail-lang") (r "^0.24.0") (d #t) (k 0)))) (h "1cm2i4nly19flvmhg6xs0djpa0ydf6dsq7kgvlvrv6fffiy9k367") (y #t)))

(define-public crate-dt-tool-0.5.0 (c (n "dt-tool") (v "0.5.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rail-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0m19lyxhcg0dydxi73j8ksc2kcnicyhxf2xj5jivwwqshpvnlaid") (y #t)))

(define-public crate-dt-tool-0.5.1 (c (n "dt-tool") (v "0.5.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rail-lang") (r "^0.25.1") (d #t) (k 0)))) (h "0vwc0j6yc6kbyzb1jxyqyfr47f0q5myy0x2zvjkmh9sadvsjsi4s") (y #t)))

(define-public crate-dt-tool-0.5.2 (c (n "dt-tool") (v "0.5.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rail-lang") (r "^0.25.1") (d #t) (k 0)))) (h "0gxavlk9zpldv0jf5j1plgd6bv0jvfjf1940q9fgxv603h2cxay9") (y #t)))

(define-public crate-dt-tool-0.5.3 (c (n "dt-tool") (v "0.5.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rail-lang") (r "^0.25.1") (d #t) (k 0)))) (h "0m72zq0qs77picgnhxalla8n4pl86rci3kc4h5xhcxanq84vam7w") (y #t)))

(define-public crate-dt-tool-0.6.0 (c (n "dt-tool") (v "0.6.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rail-lang") (r "^0.28.0") (d #t) (k 0)))) (h "01vaq8yi8xphfiapvpgya8xbf54vmkf3125jy0i6l003xhadkmrv") (y #t)))

(define-public crate-dt-tool-0.7.0 (c (n "dt-tool") (v "0.7.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rail-lang") (r "^0.29.0") (d #t) (k 0)))) (h "1d9jdc541lpgiypyh78hnz18hcn1dy25yjz9sl386inhmr90bf3r") (y #t)))

