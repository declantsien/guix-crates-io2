(define-module (crates-io dt pa dtparse) #:use-module (crates-io))

(define-public crate-dtparse-0.7.0 (c (n "dtparse") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.8") (d #t) (k 0)))) (h "0vczhb7wz0rnynn00zq65fymfnf1c33i1c85piawlnxap3x4mn8k")))

(define-public crate-dtparse-0.8.0 (c (n "dtparse") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.8") (d #t) (k 0)))) (h "1f5npwa3dibcjzi14w8jay0gd9isr7vikmb8m01y75ffdb5zjw4j")))

(define-public crate-dtparse-0.9.0 (c (n "dtparse") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.9") (d #t) (k 0)))) (h "0k68wg84zxbrppasf4g2ychjgdfhj2b1974277kqp4frxb73m8ck")))

(define-public crate-dtparse-0.9.1 (c (n "dtparse") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.9") (d #t) (k 0)))) (h "0rabhndgd8hadj84gaa792mjp07ycchl6a2xkiyvz1lslyi7y2xf")))

(define-public crate-dtparse-1.0.0 (c (n "dtparse") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.9") (d #t) (k 0)))) (h "1p1lgm8g3ngpay888xnfw9rwsjx61daala7hywjrv99g8j2sfcbg")))

(define-public crate-dtparse-1.0.1 (c (n "dtparse") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10") (d #t) (k 0)))) (h "0qjyz5n69s6nd0nd6hpc77g74qvgq6q1wgn1ag5dap7i7n8qicdg")))

(define-public crate-dtparse-1.0.2 (c (n "dtparse") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10.1") (d #t) (k 0)))) (h "1n4dg3v7b4jbv57a06yx0ljxigmmad287h4p7756981l4kzj5mdh")))

(define-public crate-dtparse-1.0.3 (c (n "dtparse") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10.1") (d #t) (k 0)))) (h "1id208k8ffm6s57kifcyf6ihn86r2yw0vich1h9bycln0spf61c2")))

(define-public crate-dtparse-1.1.0 (c (n "dtparse") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10.1") (d #t) (k 0)))) (h "12v0pjhlzy15l6azb7hhs8qaj77b228k1mnvmv5qxk8vlmydl4wp")))

(define-public crate-dtparse-1.2.0 (c (n "dtparse") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10.1") (d #t) (k 0)))) (h "1xh9hasfffnfyv8q9pwr31g63rigfx114qz6xw05wdkzpmfnq9qk")))

(define-public crate-dtparse-1.3.0 (c (n "dtparse") (v "1.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.17.0") (k 0)))) (h "1awhyy58c28fhr5nvvfpikdzraihichkz3w1mzdg7smyffldi4w2")))

(define-public crate-dtparse-1.4.0 (c (n "dtparse") (v "1.4.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (k 0)))) (h "1hq7z13nx44pbkf9jsr7frz62a03c9180x6v1fl91ssh7jibdlyv")))

(define-public crate-dtparse-1.5.0 (c (n "dtparse") (v "1.5.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (k 0)))) (h "00g423c3nw7ffhnvxlryg5hl721aib4b4dq3alpfib8lq3ims8v8")))

(define-public crate-dtparse-2.0.0 (c (n "dtparse") (v "2.0.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.24") (f (quote ("clock"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (k 0)))) (h "0fg8h07m0z38c6i556dfmgnhl18i8w37cl235iyfzc9l3kz7r325")))

