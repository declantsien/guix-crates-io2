(define-module (crates-io dt _a dt_analysis) #:use-module (crates-io))

(define-public crate-dt_analysis-0.0.1 (c (n "dt_analysis") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "053rm4qsp2caz4bpphi40rp4fi443kh3b0ds7hclh9mxls03dbis") (y #t)))

(define-public crate-dt_analysis-0.0.2 (c (n "dt_analysis") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "107a8rdj4gcrlx41wfnkg64ajjf0z7sd9jdyhbzcch4ilrdc6vm3") (y #t)))

(define-public crate-dt_analysis-0.0.3 (c (n "dt_analysis") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0zcmn2s3b77napwzbl80iyq1cx0ybp4d503vkspc7mb4hm7pyn57") (y #t)))

(define-public crate-dt_analysis-0.1.0 (c (n "dt_analysis") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0i4zvrs85x48y18hbs8rrclqp9y0p98cjgyn8k0im0c9vhfjfqis") (y #t)))

