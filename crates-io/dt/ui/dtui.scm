(define-module (crates-io dt ui dtui) #:use-module (crates-io))

(define-public crate-dtui-1.0.0 (c (n "dtui") (v "1.0.0") (d (list (d (n "async-recursion") (r "^1.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("full"))) (d #t) (k 0)) (d (n "tui-tree-widget") (r "^0.19") (d #t) (k 0)) (d (n "zbus") (r "^4.2") (f (quote ("tokio"))) (k 0)) (d (n "zbus_xml") (r "^4.0.0") (d #t) (k 0)))) (h "032qrq75xpiwszabz398ys5g6n3vzx64anc3aqfx7d3zw9idyvj4") (r "1.56")))

