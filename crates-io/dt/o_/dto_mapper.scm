(define-module (crates-io dt o_ dto_mapper) #:use-module (crates-io))

(define-public crate-dto_mapper-0.1.2 (c (n "dto_mapper") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "18nh6phv4narp07a8zqrqrb8bzdqyvlkg8h8nayr7l906jwihkcm")))

(define-public crate-dto_mapper-0.1.3 (c (n "dto_mapper") (v "0.1.3") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0pwf2hx2hjjb248avjyvvq3rc6jmv7vhm6xdg3q98s4xb07y2fy1")))

(define-public crate-dto_mapper-0.2.0 (c (n "dto_mapper") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.20.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)) (d (n "unstringify") (r "^0.1.4") (d #t) (k 0)))) (h "17vqb14fy345af8kvj0251az1alfnv2m4b4dx566sgwkfjsb5sdb")))

