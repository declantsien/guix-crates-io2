(define-module (crates-io dt o_ dto_derive) #:use-module (crates-io))

(define-public crate-dto_derive-0.1.0 (c (n "dto_derive") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.3.19") (f (quote ("stable"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rz7iy70jiq91h1dkm07rjbrgkazivj19kngikwfc0ax72x98v59")))

(define-public crate-dto_derive-0.1.1 (c (n "dto_derive") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "= 0.3.19") (f (quote ("stable"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d42yk582s60mmz2wrjh51a1zx8wqwqh37918ygdxr99kkkqjskm")))

