(define-module (crates-io dt bh dtbh-cli) #:use-module (crates-io))

(define-public crate-dtbh-cli-0.1.0 (c (n "dtbh-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "colored_json") (r "^3.1.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking" "json" "cookies"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.13.0") (d #t) (k 0)))) (h "03rznifaxcnq8nzi6bbd0asl1avf4zhc440d693kiiad4cykjnqd")))

