(define-module (crates-io dt ex dtex) #:use-module (crates-io))

(define-public crate-dtex-0.1.0 (c (n "dtex") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tectonic") (r "^0.14.1") (f (quote ("native-tls-vendored"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1mlfqf9qd64kw65j5lxmvvrmbz4p5jvg6m4j8f47aygzmp06z0jw")))

(define-public crate-dtex-0.1.1 (c (n "dtex") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tectonic") (r "^0.14.1") (f (quote ("native-tls-vendored"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "01j3wv4q0ggaa9s8a922mxf0k98ivqxk2dcs7kz6gqv749p37169")))

(define-public crate-dtex-0.1.2 (c (n "dtex") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tectonic") (r "^0.14.1") (f (quote ("native-tls-vendored"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1k9xxnabb4q65sk39j1c8banb7nnbal1fpvdxachhqq1cws82qx9")))

