(define-module (crates-io dt ed dted2) #:use-module (crates-io))

(define-public crate-dted2-0.1.0 (c (n "dted2") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "07ry8sk6dlk9adhqgxb3nnfcalc9qpha8clnrsmr6v25369g8i58") (y #t)))

(define-public crate-dted2-0.1.1 (c (n "dted2") (v "0.1.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1md6vraijbzkgprjb0f299bss851fqffqh67lji7qf3k7wwrbg13") (y #t)))

(define-public crate-dted2-0.2.0 (c (n "dted2") (v "0.2.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "038f5v9i3a0cfl2b60f38xnj6cvgj2cpvdz5mb5iv6hg01nib09a")))

(define-public crate-dted2-0.2.1 (c (n "dted2") (v "0.2.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "187kgkrbxkjy3fkp5qlbqm5hgik82bjbyb4536k2w0rmnplk8hk2")))

