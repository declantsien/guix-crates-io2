(define-module (crates-io dt ed dted) #:use-module (crates-io))

(define-public crate-dted-0.1.0 (c (n "dted") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "4.*") (d #t) (k 0)))) (h "1vrxck6l2q71h2b6m8qbmkgbxyq4msisb1mvkg3ii23bdcrsdkjv")))

(define-public crate-dted-0.2.0 (c (n "dted") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "4.*") (d #t) (k 0)))) (h "1pjk0n77gpgv72bji09k6ikcbxfrvjf66626dw571hkg73smvnqm")))

