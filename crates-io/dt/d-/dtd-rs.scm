(define-module (crates-io dt d- dtd-rs) #:use-module (crates-io))

(define-public crate-dtd-rs-0.1.0-alpha0 (c (n "dtd-rs") (v "0.1.0-alpha0") (d (list (d (n "dtd-macro") (r "^0.1.0-alpha0") (d #t) (k 0)) (d (n "dtd-parser") (r "^0.1.0-alpha2") (d #t) (k 0)))) (h "1whph8dgph8mpz0gl9migwl3knsqi96ixibwwkybv8fz3yfkpq5f") (f (quote (("trace" "dtd-parser/trace") ("default"))))))

(define-public crate-dtd-rs-0.1.0-alpha2 (c (n "dtd-rs") (v "0.1.0-alpha2") (d (list (d (n "dtd-macro") (r "^0.1.0-alpha2") (d #t) (k 0)) (d (n "dtd-parser") (r "^0.1.0-alpha3") (d #t) (k 0)))) (h "1gp17n62c1ywiwhbl6sgsksd6rh7yv3zj502dspzlcfmm3dils4a") (f (quote (("trace" "dtd-parser/trace") ("default"))))))

