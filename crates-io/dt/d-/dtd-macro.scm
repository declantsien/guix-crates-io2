(define-module (crates-io dt d- dtd-macro) #:use-module (crates-io))

(define-public crate-dtd-macro-0.1.0-alpha0 (c (n "dtd-macro") (v "0.1.0-alpha0") (d (list (d (n "dtd-parser") (r "^0.1.0-alpha2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0pdynia9g1cqmi2p9d2hp7pjzz910p5zfyqaqr1cb9xq4cwn75rs")))

(define-public crate-dtd-macro-0.1.0-alpha1 (c (n "dtd-macro") (v "0.1.0-alpha1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "dtd-parser") (r "^0.1.0-alpha3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1s3gc1rpfyx0lx04hlbwr87wcnsqglj4mlrspasymz19l5sykjwj")))

(define-public crate-dtd-macro-0.1.0-alpha2 (c (n "dtd-macro") (v "0.1.0-alpha2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "dtd-parser") (r "^0.1.0-alpha3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0p62lrmdrn2afb80hf96i1wk2sra5408xq3rsmm7rsgz2z1kazdh")))

