(define-module (crates-io dt z- dtz-config) #:use-module (crates-io))

(define-public crate-dtz-config-0.1.0 (c (n "dtz-config") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("json" "multipart" "rustls-tls" "rustls-tls-webpki-roots"))) (k 0)))) (h "0v8kz4s8xzdy68ji57ldhlhmlchhbhhg8dkd06zr0vvy4xnavsax") (r "1.63")))

(define-public crate-dtz-config-1.0.0 (c (n "dtz-config") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("json" "multipart" "rustls-tls" "rustls-tls-webpki-roots"))) (k 0)))) (h "1ygyjiv47y9zrixn9bczfyz3n45j5w97bc8n40n0hr6c96j05b6h") (r "1.63")))

