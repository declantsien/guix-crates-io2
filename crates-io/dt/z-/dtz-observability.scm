(define-module (crates-io dt z- dtz-observability) #:use-module (crates-io))

(define-public crate-dtz-observability-1.0.0 (c (n "dtz-observability") (v "1.0.0") (d (list (d (n "dtz-config") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json" "multipart" "rustls-tls" "rustls-tls-webpki-roots"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "02harhcjikknnj8g86iylascdw4vl1v7h9z5r4gwgy4z4vfqy4k1") (r "1.63")))

(define-public crate-dtz-observability-1.0.1 (c (n "dtz-observability") (v "1.0.1") (d (list (d (n "dtz-config") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json" "multipart" "rustls-tls" "rustls-tls-webpki-roots"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0zzi7w7a0l6yha80bfmi63a29czlv260yd7wbl4l0wd2ym8j6l6h") (r "1.64")))

