(define-module (crates-io dt z- dtz-billing) #:use-module (crates-io))

(define-public crate-dtz-billing-1.0.0 (c (n "dtz-billing") (v "1.0.0") (d (list (d (n "dtz-config") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json" "multipart" "rustls-tls" "rustls-tls-webpki-roots"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0sab6xaf17j0qvag9f302zh3hj1lgw2bnx67j3lm0491c8x8xizf") (r "1.64")))

