(define-module (crates-io dt z- dtz-containerregistry) #:use-module (crates-io))

(define-public crate-dtz-containerregistry-1.0.0 (c (n "dtz-containerregistry") (v "1.0.0") (d (list (d (n "dtz-config") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json" "multipart" "rustls-tls" "rustls-tls-webpki-roots"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1cvq16vp1fzlrr38ngkykns9lcgdyb9lhzxy3c47z49prx167phz") (r "1.64")))

