(define-module (crates-io dt yp dtypei) #:use-module (crates-io))

(define-public crate-dtypei-0.0.1 (c (n "dtypei") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0kzn307qmmw0nwdccn8adk6ryk3sbnbnkx7fscrpx1n3iyhrqk4s")))

