(define-module (crates-io dt n7 dtn7-codegen) #:use-module (crates-io))

(define-public crate-dtn7-codegen-0.1.0 (c (n "dtn7-codegen") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "09yp9i4mr3222gbfhg95wmnspnla1widylvj5j95i8j0ppacwq8j")))

(define-public crate-dtn7-codegen-0.1.1 (c (n "dtn7-codegen") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "06p9b1j03rinbfcsrdhbwydz9lsysai5mx51bkvxhcgdpp0hqwhw")))

(define-public crate-dtn7-codegen-0.1.2 (c (n "dtn7-codegen") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1prbllczfylbrpasr121cz7lczgzzwzrnhykjx1mhky3pqk7k5if")))

