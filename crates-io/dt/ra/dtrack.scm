(define-module (crates-io dt ra dtrack) #:use-module (crates-io))

(define-public crate-dtrack-0.0.0 (c (n "dtrack") (v "0.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gsyzfx6n3mkl3sfg2sxyj3872vx2dj0cszsvzhip0k6cvi03mgy")))

(define-public crate-dtrack-0.0.1 (c (n "dtrack") (v "0.0.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fmgj74gpxy8g3a3x1nypsj3jhqxpg6hi7xh5biciv596l09yrka")))

(define-public crate-dtrack-0.0.2 (c (n "dtrack") (v "0.0.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05b21c059m47b04hmk8rjfz3jxs8hx3y70q2dq15rjiys32mcz1a")))

(define-public crate-dtrack-1.0.0 (c (n "dtrack") (v "1.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hfa21sfibki18qimfk1n120jgvx46cimpqk9rjpx4c0qhbw4v78")))

