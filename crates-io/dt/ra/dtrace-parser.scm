(define-module (crates-io dt ra dtrace-parser) #:use-module (crates-io))

(define-public crate-dtrace-parser-0.1.8 (c (n "dtrace-parser") (v "0.1.8") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0ymhhpkfpxz17csnv5jp2rldc4l81gjdb6cmjnddlq6hkz1dwa1i")))

(define-public crate-dtrace-parser-0.1.9 (c (n "dtrace-parser") (v "0.1.9") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0z0x01cazzpwww3klglk9blnafjj68idmsk80h17fgqr5m14vgf8")))

(define-public crate-dtrace-parser-0.1.10 (c (n "dtrace-parser") (v "0.1.10") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "016d8awa8d9fr3mdn67vzza8f8cvv012dx9c8a2zyn0x77hyvzjn")))

(define-public crate-dtrace-parser-0.1.11 (c (n "dtrace-parser") (v "0.1.11") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.11.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "18rm2wx2pjr23zprxaa5hbd96mhsib8pip47km9bnhg95w3sxdn1")))

(define-public crate-dtrace-parser-0.1.12 (c (n "dtrace-parser") (v "0.1.12") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.11.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1s22q2v7gl6a3rspsnn3qr2g283x3xfszjip1zr7nl8c3bxr7fsv")))

(define-public crate-dtrace-parser-0.1.13 (c (n "dtrace-parser") (v "0.1.13") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.11.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1n4gwdx3wq1w183l6ff3k94dikk7kylnx3yyq1x5cwzx4n3q3s0z") (y #t)))

(define-public crate-dtrace-parser-0.1.14 (c (n "dtrace-parser") (v "0.1.14") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.11.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0js35m26gqwfbrg5pv3f876b8v4zl1a665if0zmlr7vz7a4i1ldy")))

(define-public crate-dtrace-parser-0.1.15 (c (n "dtrace-parser") (v "0.1.15") (d (list (d (n "pest") (r "^2.5.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.7") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0svsmazmy5rwng7a3ngsc9bcfv0d6iv7nbxa6m9f7z1p28lzd47q")))

(define-public crate-dtrace-parser-0.2.0 (c (n "dtrace-parser") (v "0.2.0") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0as5avbzmyr6p2awql414c5yp2ag0b6gvpq4ilrxzm4cnqz4wwvi")))

