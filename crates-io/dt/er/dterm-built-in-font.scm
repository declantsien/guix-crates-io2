(define-module (crates-io dt er dterm-built-in-font) #:use-module (crates-io))

(define-public crate-dterm-built-in-font-0.3.0 (c (n "dterm-built-in-font") (v "0.3.0") (d (list (d (n "dcolor") (r "^0.3.0") (d #t) (k 1)) (d (n "dimage") (r "^0.3.0") (f (quote ("png"))) (k 1)) (d (n "dvec") (r "^0.3.0") (d #t) (k 0)) (d (n "dvec") (r "^0.3.0") (d #t) (k 1)))) (h "1lcwxdx1pch52i34024vlwdw4f8xz23c6ym7dj2gdpm5627yyxxd") (y #t)))

