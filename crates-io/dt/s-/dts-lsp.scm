(define-module (crates-io dt s- dts-lsp) #:use-module (crates-io))

(define-public crate-dts-lsp-0.1.2 (c (n "dts-lsp") (v "0.1.2") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.20.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)) (d (n "tree-sitter-devicetree") (r "^0.10.0") (d #t) (k 0)))) (h "04am89y5mnv3xvd9x29g5rgl9xgay6rlpfkhkidbvkg92px6bzfy")))

(define-public crate-dts-lsp-0.1.3 (c (n "dts-lsp") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.20.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)) (d (n "tree-sitter-devicetree") (r "^0.10.0") (d #t) (k 0)))) (h "1wjxc3qq4vdwf06biw2rm1lqj6bn2gy390rznyanr38gmr8l4rhi")))

