(define-module (crates-io dt mf dtmf) #:use-module (crates-io))

(define-public crate-dtmf-0.1.0 (c (n "dtmf") (v "0.1.0") (d (list (d (n "microfft") (r "^0.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "1mm2dcpnk858pib4x1gam9p1dd5scvbfr38ms02z3j1x05ig389d")))

(define-public crate-dtmf-0.1.1 (c (n "dtmf") (v "0.1.1") (d (list (d (n "microfft") (r "^0.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "1ncwsd8wjf96ki6fjl1y2n1508k7g3ajzkw9cqkyyjdflpdam1xh")))

(define-public crate-dtmf-0.1.2 (c (n "dtmf") (v "0.1.2") (d (list (d (n "microfft") (r "^0.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "0vzh7arc4lw33nadfkjf0hjgls14zc2idpmjcx43b7p11dwa7k5s")))

(define-public crate-dtmf-0.1.3 (c (n "dtmf") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "goertzel-nostd") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 2)))) (h "12wlcfd1yblq8f569421b9bd1f23hrw0r595nmy76q1z2a4fix5f")))

(define-public crate-dtmf-0.1.4 (c (n "dtmf") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "goertzel-nostd") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 2)))) (h "0vax5wfpm320dmmbn80wf6k215wdb0frj24i3hk28brjyrl2wi3k")))

(define-public crate-dtmf-0.1.5 (c (n "dtmf") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "goertzel-nostd") (r "^0.2") (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 2)))) (h "0zipg7xn2mmhghxd045x2ddk95fc6rwl6wykw73qf50fw0y5v1j2")))

