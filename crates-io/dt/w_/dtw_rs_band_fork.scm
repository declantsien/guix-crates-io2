(define-module (crates-io dt w_ dtw_rs_band_fork) #:use-module (crates-io))

(define-public crate-dtw_rs_band_fork-0.9.5 (c (n "dtw_rs_band_fork") (v "0.9.5") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "05cj1kl6d8kxl8nam5vrvbc0r885yqwdf3sii29bdhdpbi3l7k35")))

(define-public crate-dtw_rs_band_fork-0.9.6 (c (n "dtw_rs_band_fork") (v "0.9.6") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "18ra67z61kng2ydlx530hf6bq4rmbwxpigarbn4w7pmwc3g1gapl")))

(define-public crate-dtw_rs_band_fork-0.9.7 (c (n "dtw_rs_band_fork") (v "0.9.7") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "1x33m6pwnjgp2pbgfg1pqhawqqj8iniy3a4kk5s5mldq6q14i9j5")))

(define-public crate-dtw_rs_band_fork-0.9.8 (c (n "dtw_rs_band_fork") (v "0.9.8") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "03sjldgczwqpp56v580va152r3drb051vd08p6lsa41zv005b34q")))

(define-public crate-dtw_rs_band_fork-0.9.9 (c (n "dtw_rs_band_fork") (v "0.9.9") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "1rnilb6ibksgqb2sjk2qhkwqlb30zxnax844y72lb8qpxz9q02kj")))

(define-public crate-dtw_rs_band_fork-1.0.0 (c (n "dtw_rs_band_fork") (v "1.0.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "0jb31afqj8rvxzjzpaa1k91hdn1kp1k70nlxwk2mlm1xfkpxnm2a")))

(define-public crate-dtw_rs_band_fork-1.0.1 (c (n "dtw_rs_band_fork") (v "1.0.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "0vkp7qfa7r46byg90laplzjc2661mn7w3n9bldjfwcj27wfi93hr")))

