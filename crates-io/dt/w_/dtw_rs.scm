(define-module (crates-io dt w_ dtw_rs) #:use-module (crates-io))

(define-public crate-dtw_rs-0.9.0 (c (n "dtw_rs") (v "0.9.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "07nksckl57nhp9k0zhqhbri020hirjxaywjx7f3pz7i4ph4m203k") (y #t)))

(define-public crate-dtw_rs-0.9.1 (c (n "dtw_rs") (v "0.9.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "16ykx540w5iimhfr7k8prkca2xk9dkcsx1bv19npch18mx6yw5cv")))

(define-public crate-dtw_rs-0.9.2 (c (n "dtw_rs") (v "0.9.2") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "01b9l7750f6rdmdw9a5x5cjjhysvb7fxzxpd5iikdqc1x0wlqx8f")))

(define-public crate-dtw_rs-0.9.3 (c (n "dtw_rs") (v "0.9.3") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "16fq1wivsmd3bdv5nrw501fhql4vcz1jdvd07pm60ka0pclb2qms")))

(define-public crate-dtw_rs-0.9.4 (c (n "dtw_rs") (v "0.9.4") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "1lb4fs6qmfrlaxjj1ffzfimj1ayaaj1vmrb2s7xnjq1yr11bqh1f")))

(define-public crate-dtw_rs-0.9.5 (c (n "dtw_rs") (v "0.9.5") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "0n8byi10bx95gp87j3mwzk89ma341vga4zj50s2qwn1xk6r7axil")))

