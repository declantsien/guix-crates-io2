(define-module (crates-io dt g- dtg-lib) #:use-module (crates-io))

(define-public crate-dtg-lib-4.0.0 (c (n "dtg-lib") (v "4.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ywfg8dljknfxbakmnbvf08d4ji38rffvl4ikayl3v5kidp6p2k8")))

(define-public crate-dtg-lib-4.0.1 (c (n "dtg-lib") (v "4.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1mcs8xz3lmh1hva1l4zy8hx3d5xsjwwjyj7bpghyx2dy52mvpasj")))

(define-public crate-dtg-lib-4.0.2 (c (n "dtg-lib") (v "4.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "13a5l3hsc3djql8rrwl9dr85ih8a29bwaqapzdsv3z04dgax1r3m")))

(define-public crate-dtg-lib-4.0.3 (c (n "dtg-lib") (v "4.0.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "17ipmcfclr7rcx47dkkmx80mgd0xl4fx6mi2590dl5ff9h7h821z")))

(define-public crate-dtg-lib-4.1.0 (c (n "dtg-lib") (v "4.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1f24rh361fwa41hh7kngb0ki9yhmi8zcpnv1mq1qcdb5i36qdsb2")))

(define-public crate-dtg-lib-4.1.1 (c (n "dtg-lib") (v "4.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "04flb84pna5g9hbp016cxw2m56hv61wxmwxqsiylz00kadxr79b0")))

(define-public crate-dtg-lib-4.1.2 (c (n "dtg-lib") (v "4.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "05zvhmhrp9xj1k389rwnw65bpfmpqpd3a5vlf08qn7adjxmqrvmr")))

(define-public crate-dtg-lib-4.1.3 (c (n "dtg-lib") (v "4.1.3") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1s3z6ljlxr65dvsk21vwpbk51yrn1qgky5hfg4fvflybhsm6skbh")))

(define-public crate-dtg-lib-5.0.0 (c (n "dtg-lib") (v "5.0.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.53") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1kyn0mc6wg63ff8rd4zzzhgf23ijqw34sbj5nr7s43z44bp9g78l")))

(define-public crate-dtg-lib-5.1.0 (c (n "dtg-lib") (v "5.1.0") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.2") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.56") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1559r4410z2w7j4gq74mp2vplxc6xdi3976k0xiwgwj25vsnpk6d")))

(define-public crate-dtg-lib-5.2.0 (c (n "dtg-lib") (v "5.2.0") (d (list (d (n "chrono") (r "^0.4.25") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.2") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.56") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0gpfymkv0yrds0rnccy5yzsp5ddfkrprw3qag8n7irc1b5fqpxsw")))

(define-public crate-dtg-lib-5.3.0 (c (n "dtg-lib") (v "5.3.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.2") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.56") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0s6qd297m8lxc0lm4w7ppw60y0lps9xjk95i2nxsi4yhfxw0xlpa")))

(define-public crate-dtg-lib-5.3.1 (c (n "dtg-lib") (v "5.3.1") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.2") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.56") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "07s75b02lmgh56g099mdp9qvbv56rxqvx8dahqsdaissznxdyfpl")))

(define-public crate-dtg-lib-5.4.0 (c (n "dtg-lib") (v "5.4.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.57") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0mlhd9pdnspiibkyrg0w0ngrcn7qkaq4ywj2qxw6m013psagplg2")))

(define-public crate-dtg-lib-5.5.0 (c (n "dtg-lib") (v "5.5.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.57") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0dvgh2skx0i010rpqhcinnkdlxwbsz1dlwzcszrab2b2s9hl9j8q")))

(define-public crate-dtg-lib-5.5.1 (c (n "dtg-lib") (v "5.5.1") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.57") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0f9rxsyf82azarly7r3lf022pq3h3nmhd8d38735apgwbizsz9jd")))

(define-public crate-dtg-lib-5.6.0 (c (n "dtg-lib") (v "5.6.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.57") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1wb3fqdnwzp1fmfnsllf78bd9sjb67500pdnd1yvj8p7rjngq3lj")))

(define-public crate-dtg-lib-5.7.0 (c (n "dtg-lib") (v "5.7.0") (d (list (d (n "bbd-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.57") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0cqz9z5xjdq08wfw6nwgl6d76i1yn6ivkf9n5zdljp22phsvnvid")))

(define-public crate-dtg-lib-5.8.0 (c (n "dtg-lib") (v "5.8.0") (d (list (d (n "bbd-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.57") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1fwnxf713r6vkq967zpc3690dngh0wb9g7zpzdwlrv7v6fv2hvk4")))

(define-public crate-dtg-lib-5.8.1 (c (n "dtg-lib") (v "5.8.1") (d (list (d (n "bbd-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.58") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0x0nm2z7y19l7mvr9hk9rzm4wrmj8m41wpv6xxgbymqxba9z2xxf")))

