(define-module (crates-io dt oa dtoa-short) #:use-module (crates-io))

(define-public crate-dtoa-short-0.1.0 (c (n "dtoa-short") (v "0.1.0") (d (list (d (n "dtoa") (r "^0.4.1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2.3") (d #t) (k 2)))) (h "117p8bnsx7ncc40w555q7hfrc5z3a8nh3acasyz7ifprfyair665")))

(define-public crate-dtoa-short-0.2.0 (c (n "dtoa-short") (v "0.2.0") (d (list (d (n "dtoa") (r "^0.4.1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2.3") (d #t) (k 2)))) (h "0dpnigza4s5vfi754hj2h07rf43v74qwpawsfd9y873w6m67d47p")))

(define-public crate-dtoa-short-0.3.0 (c (n "dtoa-short") (v "0.3.0") (d (list (d (n "dtoa") (r "^0.4.1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2.3") (d #t) (k 2)))) (h "1j2wq1kh7sm9vdd01b7w6q0mzyyvn38q9vazr5bzsqk481xp4vzy")))

(define-public crate-dtoa-short-0.3.1 (c (n "dtoa-short") (v "0.3.1") (d (list (d (n "dtoa") (r "^0.4.1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.2.3") (d #t) (k 2)))) (h "078dj8l83mjs4d6bzfgdv281n58c3fnzr35vn3q1h6kwd4k41386")))

(define-public crate-dtoa-short-0.3.2 (c (n "dtoa-short") (v "0.3.2") (d (list (d (n "dtoa") (r "^0.4.1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.3") (d #t) (k 2)))) (h "1wkn7ziqffq8hj0a411lgn7674ackzdk734ikp230rmp2f2hn0jr")))

(define-public crate-dtoa-short-0.3.3 (c (n "dtoa-short") (v "0.3.3") (d (list (d (n "dtoa") (r "^0.4.1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.4") (d #t) (k 2)))) (h "1mh22nwja3v8922h0hq77c29k1da634lvkn9cvg9xrqhmqlk7q5x")))

(define-public crate-dtoa-short-0.3.4 (c (n "dtoa-short") (v "0.3.4") (d (list (d (n "dtoa") (r "^1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.4") (d #t) (k 2)))) (h "0x4c8hxc041j9a85371gxdmi09q5d2whz05iwxwiq8g4qv1yxb6v")))

