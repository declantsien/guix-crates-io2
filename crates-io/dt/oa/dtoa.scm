(define-module (crates-io dt oa dtoa) #:use-module (crates-io))

(define-public crate-dtoa-0.1.0 (c (n "dtoa") (v "0.1.0") (h "0nfj2jz9wzxyw2vfii2nlxxchq56gz9igd85l93k9pnmzqys7rf0")))

(define-public crate-dtoa-0.1.1 (c (n "dtoa") (v "0.1.1") (h "1c7sfksg80822rjy91g5va88pr05b40ppj8m4iiw5mj2nj7mysmm")))

(define-public crate-dtoa-0.2.0 (c (n "dtoa") (v "0.2.0") (h "19hib98xiwam44g3wa891vsshv3xshrkm1rzxx0mh1la0c4jy9md")))

(define-public crate-dtoa-0.2.1 (c (n "dtoa") (v "0.2.1") (h "12g7wh2iigf9i81s54662kng11hqcdrkl776n7jrbqrg2p45vs59")))

(define-public crate-dtoa-0.2.2 (c (n "dtoa") (v "0.2.2") (h "0g96cap6si1g6wi62hsdk2fnj3sf5vd4i97zj6163j8hhnsl3n0d")))

(define-public crate-dtoa-0.3.0 (c (n "dtoa") (v "0.3.0") (h "1bv58mg3q8dq4417gnfj4di15wfsjhwqprmgn27k9grb9dxdrrc0")))

(define-public crate-dtoa-0.3.1 (c (n "dtoa") (v "0.3.1") (h "0ajxji7v1r7467l023i6p8sq1bs8rrvsc95vfz3635ss6zzjg9c7")))

(define-public crate-dtoa-0.4.0 (c (n "dtoa") (v "0.4.0") (h "0kyvq94y7pk6950ydinp7ihmcffb6j56nzlvc88hk3iggg36kpay")))

(define-public crate-dtoa-0.4.1 (c (n "dtoa") (v "0.4.1") (h "144cazs64lf3fbjx82ky4qd1c6fypgf0dz22jw59jihiswgvgj40")))

(define-public crate-dtoa-0.4.2 (c (n "dtoa") (v "0.4.2") (h "1ashiv223ns5p94ip8r5zxxmv2c3hh0pdsm4rcax4x5m7ly7bhq9")))

(define-public crate-dtoa-0.4.3 (c (n "dtoa") (v "0.4.3") (h "1pc9fkpali122caa7pz9mm0vbijwr1iaby8m64yz26j1xd012c3d")))

(define-public crate-dtoa-0.4.4 (c (n "dtoa") (v "0.4.4") (h "0phbm7i0dpn44gzi07683zxaicjap5064w62pidci4fhhciv8mza")))

(define-public crate-dtoa-0.4.5 (c (n "dtoa") (v "0.4.5") (h "18qycvcp0vaqzw0j784ansbxgs39l54ini9v719cy2cs3ghsjn23")))

(define-public crate-dtoa-0.4.6 (c (n "dtoa") (v "0.4.6") (h "0as2dja7bpv5mnqbji2il9yjggzgh4k27x5shjdxpnlb0bs52j8k")))

(define-public crate-dtoa-0.4.7 (c (n "dtoa") (v "0.4.7") (h "0kpagin5jx9khw1ylardzm9hp1g8k0i87qrkgsrwchfp6hlyvmw8")))

(define-public crate-dtoa-0.4.8 (c (n "dtoa") (v "0.4.8") (h "1c5j0wz118dhrczx6spc5za7dnbfxablr4adyahg9aknrsc9i2an")))

(define-public crate-dtoa-1.0.0 (c (n "dtoa") (v "1.0.0") (h "0h3fx8k1iim5xkbxjfwx4z51xsdbs4qaqxmgxpskdpfw2if7yi63") (r "1.36")))

(define-public crate-dtoa-1.0.1 (c (n "dtoa") (v "1.0.1") (h "10lbz4rkvc0qqsax2x8jp1w3z5qrz01hr4kh4gv0qk8jgb51asbm") (r "1.36")))

(define-public crate-dtoa-1.0.2 (c (n "dtoa") (v "1.0.2") (h "19ijjzwnf4jxgcmz7h9gw7zpywa43zxjqb9rwpqhz5ibpmfagajw") (r "1.36")))

(define-public crate-dtoa-1.0.3 (c (n "dtoa") (v "1.0.3") (h "0mh6v0lsb148kkbyyw1il29q6rhli565ma2n2ywwwfandgs3y1f6") (r "1.36")))

(define-public crate-dtoa-1.0.4 (c (n "dtoa") (v "1.0.4") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1yhvks46dhm9zk46gmbbv1jdilcb07d110v82pq17lfhspifx9pq") (r "1.36")))

(define-public crate-dtoa-1.0.5 (c (n "dtoa") (v "1.0.5") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "04skv0ynkzh59sj44ibi19xraj901kij854il26xzs3xd8ah81y0") (r "1.36")))

(define-public crate-dtoa-1.0.6 (c (n "dtoa") (v "1.0.6") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0s9iccyvvr68g57gnb0ybclm6nw8ypbpkckr8q8pkamcpxkr1l35") (r "1.36")))

(define-public crate-dtoa-1.0.7 (c (n "dtoa") (v "1.0.7") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1kzy2yqhid541238iixylii48445w1gkwp8yv22dxv55z3mvn169") (r "1.36")))

(define-public crate-dtoa-1.0.8 (c (n "dtoa") (v "1.0.8") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "168rdzismkh6b1jlrdnqiiasbgl2a5rryh2sc9lykxpm236q76si") (r "1.36")))

(define-public crate-dtoa-1.0.9 (c (n "dtoa") (v "1.0.9") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0lv6zzgrd3hfh83n9jqhzz8645729hv1wclag8zw4dbmx3w2pfyw") (r "1.36")))

