(define-module (crates-io dt gr dtgrep) #:use-module (crates-io))

(define-public crate-dtgrep-0.1.0 (c (n "dtgrep") (v "0.1.0") (h "1c4wz48sb9hp6rw9sjjaa7zq74icmcpl6rh567igndnnc9cc46rd")))

(define-public crate-dtgrep-0.1.1 (c (n "dtgrep") (v "0.1.1") (h "0y2nzp7jz742hcxsi5scz4ydaxqa69lqk7v6m6w72gwlw1qy5aph")))

(define-public crate-dtgrep-0.1.2 (c (n "dtgrep") (v "0.1.2") (h "11d07m89rjjsm2snbbjki7ws16qfqj3xggy7hd4pgz1dilp049ji")))

