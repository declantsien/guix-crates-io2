(define-module (crates-io dt -c dt-cli) #:use-module (crates-io))

(define-public crate-dt-cli-0.1.0 (c (n "dt-cli") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "dt-core") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0jzryl3xfgrww8dmp6xi7ny52zhfmzcwizryr2vqsyivjbg1799v")))

(define-public crate-dt-cli-0.2.0 (c (n "dt-cli") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "10nfyng5hz1hmw8wxh197skr5bnm3zaa3bbw9c86bp3pqp4mpmd2")))

(define-public crate-dt-cli-0.2.1 (c (n "dt-cli") (v "0.2.1") (d (list (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1zfc4g59jy0kbd1gqx8ds1vqls5jsi7cx6aqdfmh58n6sm94cylz")))

(define-public crate-dt-cli-0.3.0 (c (n "dt-cli") (v "0.3.0") (d (list (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1qdqvli1awj5ifl01d5fzcj8h6ch1g0inb2bwk72f1m9qb0gd70k")))

(define-public crate-dt-cli-0.3.1 (c (n "dt-cli") (v "0.3.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "13kswdanw7mha1md4mizd1wgsmlbhqqyxgpssplib8xsxkl5ybfd")))

(define-public crate-dt-cli-0.4.0 (c (n "dt-cli") (v "0.4.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "189d32kknbzi2y00gqdwcliz01jw9phbf1phb5kaz5pbrfr4sgmv")))

(define-public crate-dt-cli-0.5.0 (c (n "dt-cli") (v "0.5.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0xb7kl4l8qw6x6k4r3phjjkd83f2b7q7blvq5l049s756b6v4a28")))

(define-public crate-dt-cli-0.5.1 (c (n "dt-cli") (v "0.5.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1m3qs7pkm6mc66jk8lig6g3mi8n9mkjxfbrb40fi8imwjbm1nv4q")))

(define-public crate-dt-cli-0.5.2 (c (n "dt-cli") (v "0.5.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.5.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "04kp5f3cab100bdg802sip1nabln9zv49n89s16sm0xf1n4hcayy")))

(define-public crate-dt-cli-0.6.0 (c (n "dt-cli") (v "0.6.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0jqads6g6blb7igrgbqr5phls4zn4azzd72r1h5bps3g4rw9fcy8")))

(define-public crate-dt-cli-0.6.1 (c (n "dt-cli") (v "0.6.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1g2ajfizg4877xwk3ck04qxz62w8yzx4lckbahzg9qwd8g0lqsc7")))

(define-public crate-dt-cli-0.6.2 (c (n "dt-cli") (v "0.6.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1k8ql2ng6sq1jbidzd9bpkcr6m8in4s9vpgsbzgc682whpyrldf7")))

(define-public crate-dt-cli-0.6.3 (c (n "dt-cli") (v "0.6.3") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.6.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0wa9x5nxwa0bn9narzplig0gb2h3zylci7qi4qy5ap41dz5f9m8j")))

(define-public crate-dt-cli-0.7.0 (c (n "dt-cli") (v "0.7.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "17md9z1b66mnx1dshw1xc4krpvrg0m42ddx8f2rnpxfjxjmfhshj")))

(define-public crate-dt-cli-0.7.1 (c (n "dt-cli") (v "0.7.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "03cknyfq9zl0kzlxvp6bfh0d2dmrnzydapwwjnik2ncrw1zm19bd")))

(define-public crate-dt-cli-0.7.2 (c (n "dt-cli") (v "0.7.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.7.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0fnjyq3mdalzbvb822r35i4fws2xdwm40fy2l06cni3qabdh63gh")))

(define-public crate-dt-cli-0.7.3 (c (n "dt-cli") (v "0.7.3") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.7.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0z4gs1x6iyn39lsj40hqv7npph3crw7p0gm94pdhkda2z35rm9ya")))

(define-public crate-dt-cli-0.7.4 (c (n "dt-cli") (v "0.7.4") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.7.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1dsnkg10ipdwnd42xf3lc3bkmksvrn6cqffi5390zw7bsrirsiga")))

(define-public crate-dt-cli-0.7.5 (c (n "dt-cli") (v "0.7.5") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.7.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "19qm2ljcr7sb1aqlpj25wx91xxcwqm5p71rcqlasm1vwq3ir2s03")))

(define-public crate-dt-cli-0.7.6 (c (n "dt-cli") (v "0.7.6") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.7.6") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1wn09wf2a276qwwqwbgpmbqwsgwwj7a0lqkc9cap1lgd57apw387")))

(define-public crate-dt-cli-0.7.7 (c (n "dt-cli") (v "0.7.7") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.7.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "16yr7f296023jlgx3hvmsgwxvbd33a05kccgcijamsjmmjp7vlya")))

(define-public crate-dt-cli-0.7.8 (c (n "dt-cli") (v "0.7.8") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.7.8") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1p3kdl36b2azl2hpg2psxw6rjm8xz2mr2mpsyvl45ip7rwmcnmfk")))

(define-public crate-dt-cli-0.7.9 (c (n "dt-cli") (v "0.7.9") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dt-core") (r "^0.7.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0lhmx8737baxr0n0ynaziggk7iqliv3np9b446ylkczgcarz9sm2")))

