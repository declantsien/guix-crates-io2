(define-module (crates-io dt in dtinfer) #:use-module (crates-io))

(define-public crate-dtinfer-0.1.0 (c (n "dtinfer") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "02nj2gkzpnvmq5hxpk3ch81imb5s90j4aqmqvyllh94n89c7bcyd")))

(define-public crate-dtinfer-0.1.1 (c (n "dtinfer") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0il5l632qgmfm9lng9ca2pmm9cn10ifsabvckk9x1g6zkq35r9dg")))

