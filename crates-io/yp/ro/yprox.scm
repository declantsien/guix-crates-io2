(define-module (crates-io yp ro yprox) #:use-module (crates-io))

(define-public crate-yprox-0.1.0 (c (n "yprox") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("full"))) (d #t) (k 0)))) (h "1wp52b2xf7hhwbnssdvpvdkpjysz2c2v57dy9xrfn7bliagl7z9v")))

(define-public crate-yprox-0.1.1 (c (n "yprox") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0zw5j0dz9hd4zj6spq5r8xgsbsbn7aba5k9jmlpjkb3rxl4dzl8h")))

(define-public crate-yprox-0.2.0 (c (n "yprox") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "1piz4jykfkfks61wfj2glgj1c1py4wfpf71isll477n7xvyfwl69")))

