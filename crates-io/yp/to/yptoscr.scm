(define-module (crates-io yp to yptoscr) #:use-module (crates-io))

(define-public crate-yptoscr-1.1.1 (c (n "yptoscr") (v "1.1.1") (d (list (d (n "bors") (r "^1.1.1") (k 0)) (d (n "const-sha1") (r "^0.2.0") (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "1m3rqqv25dal6ymaxzwipnzvwdg12vlhraa4d2n402v90mdqx36k") (f (quote (("std" "hex/std" "bors/std") ("default" "std") ("alloc" "hex/alloc" "bors/std"))))))

