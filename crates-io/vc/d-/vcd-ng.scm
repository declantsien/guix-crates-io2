(define-module (crates-io vc d- vcd-ng) #:use-module (crates-io))

(define-public crate-vcd-ng-0.1.0 (c (n "vcd-ng") (v "0.1.0") (d (list (d (n "atoi_radix10") (r "^0.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "compact_str") (r "^0.6.1") (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)))) (h "0n4igp5ncag75r33mmp45g9m46wljbvkz1qfvdx4w0cv9v4hly46")))

(define-public crate-vcd-ng-0.1.1 (c (n "vcd-ng") (v "0.1.1") (d (list (d (n "atoi_radix10") (r "^0.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "compact_str") (r "^0.6.1") (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)))) (h "15hwhlrxlmrrm22ivrrkfayh9dmadbbg4sxvknli0sjwm89jzc6a")))

(define-public crate-vcd-ng-0.1.2 (c (n "vcd-ng") (v "0.1.2") (d (list (d (n "atoi_radix10") (r "^0.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "compact_str") (r "^0.6.1") (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)))) (h "0am5rcdwkz9haqcfmhlgs1bkhw1dksqhk12wj9gafg63miwk3xn2")))

