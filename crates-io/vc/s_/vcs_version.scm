(define-module (crates-io vc s_ vcs_version) #:use-module (crates-io))

(define-public crate-vcs_version-0.1.0 (c (n "vcs_version") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("formatting" "macros"))) (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "1z8jl7n9s6pwlw37n38654j1zc3sk9xl1sg0kh6bjndx9fbr7161")))

