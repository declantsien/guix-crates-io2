(define-module (crates-io vc ge vcgencmd) #:use-module (crates-io))

(define-public crate-vcgencmd-0.1.0 (c (n "vcgencmd") (v "0.1.0") (d (list (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "1kdja8ylj9cv1pz9jfj04msnv5zzwk171m95np3bw7af7rnqiyj8")))

(define-public crate-vcgencmd-0.1.1 (c (n "vcgencmd") (v "0.1.1") (d (list (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "0pmgsvrl79sm0rnx8r9478k8fskbml1q1b59y4mzvs1cmvz09b2x")))

(define-public crate-vcgencmd-0.1.2 (c (n "vcgencmd") (v "0.1.2") (d (list (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "06y68mzdp607kdnn3ivxj47v6333bz5ggp3q3qx2ydqpv2jpvrwq")))

(define-public crate-vcgencmd-0.1.3 (c (n "vcgencmd") (v "0.1.3") (d (list (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "04ki4581d6llxkvhp7k2qwkjlh9ab7fvz2v8f5vwpj0lax2hp1w4")))

(define-public crate-vcgencmd-0.1.4 (c (n "vcgencmd") (v "0.1.4") (d (list (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "0q01rp05wvy1dqk4asvn3kk6w7j49psr8bd8rqn4kh8rfxyfqpgr")))

(define-public crate-vcgencmd-0.2.0 (c (n "vcgencmd") (v "0.2.0") (d (list (d (n "bitpat") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (o #t) (d #t) (k 0)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "0mf8gf249bf0ychgif8rk6jgr2051p0s2xq5msakrwm7zpc8qy5x") (f (quote (("serde_support" "serde") ("default"))))))

(define-public crate-vcgencmd-0.2.1 (c (n "vcgencmd") (v "0.2.1") (d (list (d (n "bitpat") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (o #t) (d #t) (k 0)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "1fby2jhhg9ivvzkyj23dkl8zpp9667rvpkcyfjxli95yl79afmbj") (f (quote (("serde_support" "serde") ("default")))) (y #t)))

(define-public crate-vcgencmd-0.2.2 (c (n "vcgencmd") (v "0.2.2") (d (list (d (n "bitpat") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "08h8kslgdqq88m32sw67h89341aip14nzykfp086bdq2zwp148wv") (f (quote (("serde_support" "serde") ("default"))))))

(define-public crate-vcgencmd-0.2.3 (c (n "vcgencmd") (v "0.2.3") (d (list (d (n "bitpat") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "19iif4v5iixwk2vmd386hfpjvg2vlj7xkjyivdvdw1f8zgp9vqrk") (f (quote (("serde_support" "serde") ("default"))))))

(define-public crate-vcgencmd-0.3.0 (c (n "vcgencmd") (v "0.3.0") (d (list (d (n "bitpat") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "0njxb7fz9yg85al32s2rj9kz1784g8ivgdi0cjnghmpx2vd3cbxk") (f (quote (("default"))))))

(define-public crate-vcgencmd-0.3.1 (c (n "vcgencmd") (v "0.3.1") (d (list (d (n "bitpat") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "0lr7aj4q3nz1zwgm4k0g2w46bsi3fvx2cy41qadphfk8s99cshx2") (f (quote (("default"))))))

