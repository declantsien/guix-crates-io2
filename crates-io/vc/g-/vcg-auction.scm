(define-module (crates-io vc g- vcg-auction) #:use-module (crates-io))

(define-public crate-vcg-auction-0.1.0 (c (n "vcg-auction") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 2)))) (h "1h48l8n1m3xphr9zbxwm8wrfj2c0ny2r2qb24fi6lx6gdqa9nba8") (f (quote (("default" "rand")))) (r "1.56.1")))

