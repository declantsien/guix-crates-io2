(define-module (crates-io vc at vcat) #:use-module (crates-io))

(define-public crate-vcat-0.1.0 (c (n "vcat") (v "0.1.0") (d (list (d (n "better-panic") (r "^0.3") (d #t) (k 0)) (d (n "vfile") (r "^0.1") (d #t) (k 0)))) (h "0x3cq9sr8wql2qnrkbaiwfjlacdab3wh8wmsg07v7zcp956zyxhn") (y #t)))

(define-public crate-vcat-0.8.0 (c (n "vcat") (v "0.8.0") (d (list (d (n "better-panic") (r "^0.3") (d #t) (k 0)) (d (n "vfile") (r "^0.8") (d #t) (k 0)))) (h "1kxjgwzmxc1yjq4dmafvy79zl6vi0j5ly0y6j2n6pp3db183crbj") (y #t)))

