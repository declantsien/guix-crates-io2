(define-module (crates-io vc r- vcr-cassette) #:use-module (crates-io))

(define-public crate-vcr-cassette-1.0.0 (c (n "vcr-cassette") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0lyq432ijirs3q0fmw34b98w9br2s5d81skmxf9cd6paqn780j71")))

(define-public crate-vcr-cassette-1.0.1 (c (n "vcr-cassette") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0hj73yrfci37anmgmlwb95zmzj7l2mimbyj7a9g70696lqn94mqh")))

(define-public crate-vcr-cassette-1.0.2 (c (n "vcr-cassette") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "04zcnis2c7hsx30q5xyaadbcprvlhidrmjhf1didfw4p4ql51ahv")))

(define-public crate-vcr-cassette-2.0.0 (c (n "vcr-cassette") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 2)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "1ms9bzkv2xqkb1wbd1bqjg866p48h4d6cz00hkhxnx8zhi9ya952")))

(define-public crate-vcr-cassette-2.0.1 (c (n "vcr-cassette") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 2)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0kam85qyyy2s0vbnlkrs1yjnpnkjca1xhl1hcn37i292jrsm6krn")))

