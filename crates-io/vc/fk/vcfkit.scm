(define-module (crates-io vc fk vcfkit) #:use-module (crates-io))

(define-public crate-vcfkit-0.1.0 (c (n "vcfkit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.41.1") (d #t) (k 0)))) (h "14c4ww6xpy0pc27gi0hkxx8q4d7wlj3n8bg57xq012h010vhxx3f") (y #t) (r "1.68.0")))

(define-public crate-vcfkit-0.1.1 (c (n "vcfkit") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.41.1") (d #t) (k 0)))) (h "16cq2k3xdv8730mpg5imh7ip1qv69a2i54cv3kiplb1xjaa34mkp") (r "1.71.0")))

