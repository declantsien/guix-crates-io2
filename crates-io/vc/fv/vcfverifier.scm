(define-module (crates-io vc fv vcfverifier) #:use-module (crates-io))

(define-public crate-vcfverifier-0.1.0 (c (n "vcfverifier") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "faimm") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.39.5") (d #t) (k 0)))) (h "038wiza80hz6qvvy8w2yjplfizm1sbi469h1c7jdqrl9239jqd3m")))

(define-public crate-vcfverifier-0.1.1 (c (n "vcfverifier") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "faimm") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.39.5") (d #t) (k 0)))) (h "1za82pamwch42gw4snznql76x2h800l8xwmjcrgwk99gygrpsrrg")))

