(define-module (crates-io vc di vcdiff-merger) #:use-module (crates-io))

(define-public crate-vcdiff-merger-1.0.0 (c (n "vcdiff-merger") (v "1.0.0") (d (list (d (n "vcdiff-common") (r "^1") (d #t) (k 0)) (d (n "vcdiff-reader") (r "^1") (d #t) (k 0)) (d (n "vcdiff-writer") (r "^1") (d #t) (k 0)))) (h "19njaj1ww9bk1klygkxq6im27kism7vqw0slvzqsh4an5xgnj9py")))

