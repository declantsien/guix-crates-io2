(define-module (crates-io vc di vcdiff-decoder) #:use-module (crates-io))

(define-public crate-vcdiff-decoder-1.0.0 (c (n "vcdiff-decoder") (v "1.0.0") (d (list (d (n "vcdiff-common") (r "^1") (d #t) (k 0)) (d (n "vcdiff-reader") (r "^1") (d #t) (k 0)))) (h "1nshfs3hjy58vg2v0rf9iafbq7kxc0wh24lq2zx7n3kvzxd9krgf")))

