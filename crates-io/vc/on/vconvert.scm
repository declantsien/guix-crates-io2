(define-module (crates-io vc on vconvert) #:use-module (crates-io))

(define-public crate-vconvert-0.1.0 (c (n "vconvert") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1wbqnl9k0gil0qcsyhvcx9p59vv18dkjrj6ar1vnzq4vzhgpmk6g")))

(define-public crate-vconvert-0.1.1 (c (n "vconvert") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1r9kd46k0kk221vj4pp8a1fh9dxycrpk1hm2nqdjbsxb9y09ji4d")))

