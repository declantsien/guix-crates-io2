(define-module (crates-io vc sg vcsgraph) #:use-module (crates-io))

(define-public crate-vcsgraph-0.1.0 (c (n "vcsgraph") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1xr8n8dpajwzqv5m672233nks1s7qg36yk2j3qyy9gkwjbp7hiyf") (f (quote (("cli" "structopt"))))))

(define-public crate-vcsgraph-0.2.0 (c (n "vcsgraph") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1xqy6pp6njyywdnaaqz9hhpfrm7rflw226bw798gfx953qiqrdjc") (f (quote (("cli" "structopt"))))))

