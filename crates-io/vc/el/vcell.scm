(define-module (crates-io vc el vcell) #:use-module (crates-io))

(define-public crate-vcell-0.1.0 (c (n "vcell") (v "0.1.0") (h "03bswmj8gzr2z3nbbqasl9g5k4lkx6arvzxiib88r4mnmzq9ghj5") (f (quote (("const-fn"))))))

(define-public crate-vcell-0.1.1 (c (n "vcell") (v "0.1.1") (h "0mz6fwn6533qn60x2wwvsa619qz8wym1hdjvwbdgpfcyf9fgfl7a") (y #t)))

(define-public crate-vcell-0.1.2 (c (n "vcell") (v "0.1.2") (h "0g2b99b6zam4idgc8p9inrig75qi775zg579i513lmpympf34vl7") (f (quote (("const-fn"))))))

(define-public crate-vcell-0.1.3 (c (n "vcell") (v "0.1.3") (h "00n0ss2z3rh0ihig6d4w7xp72g58f7g1m6s5v4h3nc6jacdrqhvp") (f (quote (("const-fn"))))))

