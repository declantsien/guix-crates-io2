(define-module (crates-io vc d_ vcd_rust) #:use-module (crates-io))

(define-public crate-vcd_rust-0.0.1 (c (n "vcd_rust") (v "0.0.1") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "strum") (r "^0.19") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bf0v8fnn2vz40pgb52d9b3mrxh5rjkdb03f5cpg5xzl457jrjf7")))

