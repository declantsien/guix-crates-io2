(define-module (crates-io vc d_ vcd_io) #:use-module (crates-io))

(define-public crate-vcd_io-0.1.0 (c (n "vcd_io") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0liwcp71sp0zah0rjgdi6rqwzbdqdix0j7bz0rzb5y9bavzr13gw")))

(define-public crate-vcd_io-0.1.1 (c (n "vcd_io") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hfg7phg0rih55g1bhw4rmbdhmyvqzd3hfcsq48knicpagxgni0d")))

(define-public crate-vcd_io-0.1.2 (c (n "vcd_io") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "073p36i38xffaxnkc4rr1w7xl1mkhm496dn3w0fni1778mnshh8a")))

(define-public crate-vcd_io-0.1.3 (c (n "vcd_io") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02nrbkg63v8zy80419icir0h5bgsqh5wws104yav1c1wi6ayrq4w")))

(define-public crate-vcd_io-0.1.4 (c (n "vcd_io") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1sl0gg8qc45r0q5wq9mkqaxfcqfdwdh3v3xv3x8417h7a53gswx4")))

(define-public crate-vcd_io-0.2.0 (c (n "vcd_io") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0b590sda1lwz17zpkihadvfmxj6dkivbsim4ihij7axq4f33vh6a")))

(define-public crate-vcd_io-0.2.1 (c (n "vcd_io") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zfsalifvgsgmkval0p5df62wlhyrfzahi3qwgqs1nvfhkqj52lh")))

(define-public crate-vcd_io-0.2.2 (c (n "vcd_io") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nfw6qyrhyxxvrhypqwffndmac6mavd3gdr24jg5766wpir270h4")))

(define-public crate-vcd_io-0.2.3 (c (n "vcd_io") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hywjmmpcig3vv7ls5jx81mr3w7nlbrw579xcs66jx744j6gh983")))

(define-public crate-vcd_io-0.2.4 (c (n "vcd_io") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0slpp6kb0aaspkljmsvdffampnd4smf352z01n13g786vn6f6zb1")))

(define-public crate-vcd_io-0.2.5 (c (n "vcd_io") (v "0.2.5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0l78mw04ahl0dv35b22c74ig4lrq27ik42jsj0szj0k7dc01yg9j")))

(define-public crate-vcd_io-0.2.6 (c (n "vcd_io") (v "0.2.6") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c8jff6i3afcddrm9fnxapzxpp96v9v6vd83v4bh2i100sgc92ym")))

(define-public crate-vcd_io-0.2.7 (c (n "vcd_io") (v "0.2.7") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k0b0pfz5wjkfhhzdqhidmsdazmyniw5mdshxmrfjnrnvylf07ap")))

(define-public crate-vcd_io-0.2.8 (c (n "vcd_io") (v "0.2.8") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bc1acrnw8ixdz190n1z2gc4ckqpr6m36wjxi8c0hm9j0p1iyi26")))

(define-public crate-vcd_io-0.2.9 (c (n "vcd_io") (v "0.2.9") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a418xrz6gmq99k98fnwlyp92i4z43fbnfiqqnhmi5cg9skxqjg3")))

(define-public crate-vcd_io-0.2.10 (c (n "vcd_io") (v "0.2.10") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00fjhyrn2lq21yvb0dk9653rkq8xbrahd3akfzdrdvwx7qmcmkvr")))

(define-public crate-vcd_io-0.2.11 (c (n "vcd_io") (v "0.2.11") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "random-string") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06g1xj6cvclnvihgg65kam1fw2aq64m0jsa9bgga8jcw6jcgm7xd") (s 2) (e (quote (("dev" "dep:rand" "dep:random-string"))))))

(define-public crate-vcd_io-0.2.13 (c (n "vcd_io") (v "0.2.13") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "random-string") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ayslw8v4x680sb1q9crcarpgkhc4nad2nbqwbqrnamhv6n2jryy") (s 2) (e (quote (("dev" "dep:rand" "dep:random-string"))))))

(define-public crate-vcd_io-0.2.14 (c (n "vcd_io") (v "0.2.14") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "random-string") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hxz9gv3wpmsx1vd6sc7c8z4b2qv9am4wqpqzxfr77j10qf2nd5d") (s 2) (e (quote (("dev" "dep:rand" "dep:random-string"))))))

(define-public crate-vcd_io-0.3.0 (c (n "vcd_io") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "random-string") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01hff3p00jh53pn4dkgvgvmq0wp5ilmjm4g7y2x3wjs67ahxwi61") (s 2) (e (quote (("dev" "dep:rand" "dep:random-string"))))))

