(define-module (crates-io vc md vcmd) #:use-module (crates-io))

(define-public crate-vcmd-0.0.1 (c (n "vcmd") (v "0.0.1") (h "1k5m6bivc3rsmv53s5mvrxz48i57bj2cz4qq6wwaqlv43zx2zigg")))

(define-public crate-vcmd-0.0.2 (c (n "vcmd") (v "0.0.2") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1xh678iycwqc39lshnrs63s6wzvpiky5shln1j3v8qx8isgqmzra")))

