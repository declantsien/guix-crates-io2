(define-module (crates-io vc tr vctr2) #:use-module (crates-io))

(define-public crate-vctr2-0.1.0 (c (n "vctr2") (v "0.1.0") (h "041njghlpxzcflcva7jx120n786w3p5pndnygps10070506ml9z9")))

(define-public crate-vctr2-0.1.1 (c (n "vctr2") (v "0.1.1") (h "0rr4xg0pn0xjmhcwjxd1sgvlhp7sy4sl82dab0404n3gx57gkk1r")))

(define-public crate-vctr2-0.1.2 (c (n "vctr2") (v "0.1.2") (h "1kkhjhk2c2k86k0bn7plpd2n4vh3a43wcqb2kf4ji1rml99fxz5i")))

(define-public crate-vctr2-0.1.3 (c (n "vctr2") (v "0.1.3") (h "0xsnj7f9wx5acvpsc6klpxv4qxq8yy9r4iwb3azad7a9fpk15rx6")))

(define-public crate-vctr2-0.1.4 (c (n "vctr2") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0960ca37m6smaxlnzrhjrdfxigj2pfhi269014yms53c94g49ghg")))

(define-public crate-vctr2-0.1.5 (c (n "vctr2") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0qdp6nx1ccv9iwypggbz33fhbs04i1g0crkfi4p7akkl08ds768d")))

(define-public crate-vctr2-0.1.6 (c (n "vctr2") (v "0.1.6") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0zg8xhfxc6m4gi12hy07ma16gsw7fnr5kyf182anqwzrcvwb773x")))

(define-public crate-vctr2-0.1.7 (c (n "vctr2") (v "0.1.7") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0pn79a8c26jnkh1s9yf99m39v4z5324r8zcy26ps027firffjlf2")))

(define-public crate-vctr2-0.1.8 (c (n "vctr2") (v "0.1.8") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0jh7s64csgwp7ayjr3036xndlfv91566wwc9mghpnrc0nl4jzki1")))

