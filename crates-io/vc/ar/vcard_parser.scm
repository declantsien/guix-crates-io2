(define-module (crates-io vc ar vcard_parser) #:use-module (crates-io))

(define-public crate-vcard_parser-0.1.0 (c (n "vcard_parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "language-tags") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1jkckdbr673v7y30hgwxx5vagi22lwarhibv2fhdyavs6hq52vsg")))

(define-public crate-vcard_parser-0.1.1 (c (n "vcard_parser") (v "0.1.1") (d (list (d (n "language-tags") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "time") (r "^0.3.16") (f (quote ("parsing" "formatting"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0l0yg2b67mp4vpljg7xlixdpgh1shc5szxz1d0w1psn11gzlbwbz")))

(define-public crate-vcard_parser-0.1.2 (c (n "vcard_parser") (v "0.1.2") (d (list (d (n "language-tags") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "time") (r "^0.3.16") (f (quote ("parsing" "formatting"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1g7ifc7x5qxs5qnqsb67jj9mv7y34mjynv30g0ly37rncb4393s7")))

(define-public crate-vcard_parser-0.2.0 (c (n "vcard_parser") (v "0.2.0") (d (list (d (n "language-tags") (r "^0.3.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("parsing" "formatting"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "00n0knmrp19r11n9911f99h78qcxc2ga47hf3xvagvqdhr00akpg")))

(define-public crate-vcard_parser-0.2.1 (c (n "vcard_parser") (v "0.2.1") (d (list (d (n "language-tags") (r "^0.3.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("parsing" "formatting"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0icqpxx5i3r2nz2si9xij6wkda0xk8grzwm70qaw5jv4ddjzahx1")))

(define-public crate-vcard_parser-0.2.2 (c (n "vcard_parser") (v "0.2.2") (d (list (d (n "language-tags") (r "^0.3.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (f (quote ("parsing" "formatting"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0y4ppp6hvs55bxkkmmimkg1yzdpmn9x0qb6rw6rrfki8k9lhy46w")))

