(define-module (crates-io vc ar vcard_tui) #:use-module (crates-io))

(define-public crate-vcard_tui-0.1.0 (c (n "vcard_tui") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.1.5") (d #t) (k 0)) (d (n "vcard_parser") (r "^0.1.0") (d #t) (k 0)))) (h "18fwxz1bwxsvs4xz3dsmyyfxzaq9hm62b5kb7g57849bfr14hsa5")))

(define-public crate-vcard_tui-0.1.1 (c (n "vcard_tui") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.2.0") (d #t) (k 0)) (d (n "vcard_parser") (r "^0.2.1") (d #t) (k 0)))) (h "0zblwmrrhhk069jxi4r085jmqszbjm5p2v8vadmgmkdszj2bvc5l")))

(define-public crate-vcard_tui-0.1.2 (c (n "vcard_tui") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)) (d (n "vcard_parser") (r "^0.2.2") (d #t) (k 0)))) (h "0vrz8gja643ls5z5hjxdqg5pdgfvxl0q1rjfmyj16gvgd5ghy548")))

