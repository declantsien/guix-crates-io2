(define-module (crates-io vc va vcvars) #:use-module (crates-io))

(define-public crate-vcvars-0.1.0 (c (n "vcvars") (v "0.1.0") (d (list (d (n "filenamify") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "1x5fl1f3wpgpiwii0ndjp8qym1spqvrpcmq5j6lcsjci3kawsigv")))

(define-public crate-vcvars-0.1.1 (c (n "vcvars") (v "0.1.1") (d (list (d (n "filenamify") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "1lgjnraw7psjk2b49ll51n7qs0bgvdl9cjvjqc6d27scbmls3f8f")))

(define-public crate-vcvars-0.1.2 (c (n "vcvars") (v "0.1.2") (d (list (d (n "filenamify") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "1a9gpskqlraz2gvmk689ljhzzflvwp9w2m1ngldczy32vanj72z6")))

(define-public crate-vcvars-0.1.3 (c (n "vcvars") (v "0.1.3") (d (list (d (n "filenamify") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "1kcp1xkpipc3i0avk3n7kxld0ynq0ivi35qq8cl9zsnzcwmxaz19")))

(define-public crate-vcvars-0.1.4 (c (n "vcvars") (v "0.1.4") (d (list (d (n "filenamify") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "0d1z9qcc9sqrbkw2pm55yds5inq72wzw366rg2yrmf1b5mp06bj9")))

(define-public crate-vcvars-0.1.5 (c (n "vcvars") (v "0.1.5") (d (list (d (n "filenamify") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "19inanwmmsxbmrip1grpv2nmf6k6as9vms7wh14byhbkzig8a17k")))

(define-public crate-vcvars-0.1.6 (c (n "vcvars") (v "0.1.6") (d (list (d (n "filenamify") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 2)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "03ihvzh1a76vxvznxnbvrg34s7g90n125w6q6gbgr80xs1ahhgj3")))

