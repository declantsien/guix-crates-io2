(define-module (crates-io vc pk vcpkg_cli) #:use-module (crates-io))

(define-public crate-vcpkg_cli-0.1.0 (c (n "vcpkg_cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.23.2") (d #t) (k 0)) (d (n "vcpkg") (r "^0.0.1") (d #t) (k 0)))) (h "13da98cyz3k5wlb4svssfssy21js7q1kdf5lx61jw7fx1wy7zz12")))

(define-public crate-vcpkg_cli-0.1.2 (c (n "vcpkg_cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.23.2") (d #t) (k 0)) (d (n "vcpkg") (r "^0.1.4") (d #t) (k 0)))) (h "1ymd6arsmkx7d31nixf0kbmc0xzfcgy5yidcw8xhc3fv3f7r9nmx")))

(define-public crate-vcpkg_cli-0.2.1 (c (n "vcpkg_cli") (v "0.2.1") (d (list (d (n "clap") (r "^2.23.2") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2") (d #t) (k 0)))) (h "0qyjkkyji9gwzky7ckxc93qyklzmh7937qgs72mj411bpyykx2fw")))

(define-public crate-vcpkg_cli-0.2.2 (c (n "vcpkg_cli") (v "0.2.2") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.4") (d #t) (k 0)))) (h "1syk3d9xlxny2awk9zlxv2wv74r9vw2cd63mxwh2ibfrxkcvpprm")))

(define-public crate-vcpkg_cli-0.2.3 (c (n "vcpkg_cli") (v "0.2.3") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.6") (d #t) (k 0)))) (h "0ajm3999f4x24wwhnhwg2whx10j8wp98kr412pkhdv2q0fiswq8b")))

(define-public crate-vcpkg_cli-0.2.4 (c (n "vcpkg_cli") (v "0.2.4") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.8") (d #t) (k 0)))) (h "05hyjdqmsd6mjk0fvza4zixyr267wjqxmxc909gll0qd85mswsa8")))

