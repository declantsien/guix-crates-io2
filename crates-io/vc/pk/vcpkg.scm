(define-module (crates-io vc pk vcpkg) #:use-module (crates-io))

(define-public crate-vcpkg-0.0.1 (c (n "vcpkg") (v "0.0.1") (h "1px2r0cpjh4vahpq9brm97k1f96qz36yx44dr3dsmd5r43ahpl1q")))

(define-public crate-vcpkg-0.0.2 (c (n "vcpkg") (v "0.0.2") (h "03ijdcv3m3a9drwzpsh98pwm0grnmlkrl75l020fda6vrs8ll30h")))

(define-public crate-vcpkg-0.1.0 (c (n "vcpkg") (v "0.1.0") (h "0ff55nhh024nfp0vg472iq80i4ddjyl7421bf9znkf9k1l9wvbqf")))

(define-public crate-vcpkg-0.1.1 (c (n "vcpkg") (v "0.1.1") (h "1pjxxab8ivkwh6z8xlz4flz9i7vmb23mk5jqxc6vnm17qd241d6h")))

(define-public crate-vcpkg-0.1.2 (c (n "vcpkg") (v "0.1.2") (h "1b0nfr7qxk10fg6hwgzv0crrd1dig1nzgj65b3kcwz4y4hma5fw1")))

(define-public crate-vcpkg-0.1.3 (c (n "vcpkg") (v "0.1.3") (h "1gl6c75lr9f9gxlzjs1r2184ghxv5nfs9zrvq6qak1r1ij3sl49p")))

(define-public crate-vcpkg-0.1.4 (c (n "vcpkg") (v "0.1.4") (h "12kaaf7hxn614v9iqhbvjjgmvbw9rwd31azhm08xwgs9095fxyil")))

(define-public crate-vcpkg-0.1.5 (c (n "vcpkg") (v "0.1.5") (h "16j0kg14jldryks4dw8pcyja9p0hjb78x9bn211rk58x2l29ib4j")))

(define-public crate-vcpkg-0.1.6 (c (n "vcpkg") (v "0.1.6") (h "0y82lf5z8x73qkxrycvzsxhhkqi5p0f4ndai9lpkj2v0fjqs59d9") (y #t)))

(define-public crate-vcpkg-0.1.7 (c (n "vcpkg") (v "0.1.7") (h "1p3hcjk7912f2smpvijif6gvpa0adfz3h6cwnbx441w0140cmr7b")))

(define-public crate-vcpkg-0.1.8 (c (n "vcpkg") (v "0.1.8") (h "0r18j0bqlj2vkbgj1fsn0mmrz06y54q4wvrz84l0gmp5blp24n6x")))

(define-public crate-vcpkg-0.2.0 (c (n "vcpkg") (v "0.2.0") (h "1k2gzrbsdkx5j3rdw6xmb6s78qvakbw40rp0cxzn1nffw9qgyx6z")))

(define-public crate-vcpkg-0.2.1 (c (n "vcpkg") (v "0.2.1") (h "1bdcylhr5v828xz5vm02lwr433cpiws7h44mvc8cgw2m5bz87h2f")))

(define-public crate-vcpkg-0.2.2 (c (n "vcpkg") (v "0.2.2") (h "02vx93alz6qp31vxd2ms2jdd17naxrkd969127xshy1ixn5ps2ly")))

(define-public crate-vcpkg-0.2.3 (c (n "vcpkg") (v "0.2.3") (h "1003wv63wjg112xk3kyikf3aj5i189sx3jf1pd0wm1cakiwgdl3y")))

(define-public crate-vcpkg-0.2.4 (c (n "vcpkg") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0x1410z41pjydhjlchwv81034pdk32l6bpnbwg0085w173hk7rfb")))

(define-public crate-vcpkg-0.2.5 (c (n "vcpkg") (v "0.2.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1k3iqxswcs77v0z6ghdrmxpz1dvc5p26gf77m1xjzvd51sa7a555")))

(define-public crate-vcpkg-0.2.6 (c (n "vcpkg") (v "0.2.6") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0g8l2gpc889bl4ja7fx0syr4sgj0w6zq63kx5hdkf4ivxg9rdwny")))

(define-public crate-vcpkg-0.2.7 (c (n "vcpkg") (v "0.2.7") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "15dzk1b96q946v9aisbd1bbhi33n93wvgziwh1shmscn1xflbp9k")))

(define-public crate-vcpkg-0.2.8 (c (n "vcpkg") (v "0.2.8") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0s1ijdrsg6917imja2hb07l0z4vbx7ydm8m2i1n9g62fg7r3ki1z")))

(define-public crate-vcpkg-0.2.9 (c (n "vcpkg") (v "0.2.9") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0k6p226prl1g8rns3xi5ak44aki0xx6hmnr3wbqhf7hjaqfy9lam")))

(define-public crate-vcpkg-0.2.10 (c (n "vcpkg") (v "0.2.10") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "132hlmsc4maava91vl4lh677sl1c7vr8ccl53fnr5w41y6dh4m34")))

(define-public crate-vcpkg-0.2.11 (c (n "vcpkg") (v "0.2.11") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1yvrd2b97j4hv5bfhcj3al0dpkbzkdsr6dclxqz3zqm50rhwl2xh")))

(define-public crate-vcpkg-0.2.12 (c (n "vcpkg") (v "0.2.12") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0p9ypqvv55cq2rl0s7g5ca2bkvwn62cdrdb0hm8j0hd2crigznyb")))

(define-public crate-vcpkg-0.2.13 (c (n "vcpkg") (v "0.2.13") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1ajg5vqyhd4n72y6048bpdrmb3ppvsaabhavipjhf6by005f8p02")))

(define-public crate-vcpkg-0.2.14 (c (n "vcpkg") (v "0.2.14") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "169kihgyij8jff0ymql2ss8c60q3xgql7r4j19cbzsglzpr5sibh")))

(define-public crate-vcpkg-0.2.15 (c (n "vcpkg") (v "0.2.15") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "09i4nf5y8lig6xgj3f7fyrvzd3nlaw4znrihw8psidvv5yk4xkdc")))

