(define-module (crates-io vc lo vclock) #:use-module (crates-io))

(define-public crate-vclock-0.1.0 (c (n "vclock") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s0wbw1z9d6z9vn4ify0l69942ixcgb26mmbfimykayikvd8rcmc")))

(define-public crate-vclock-0.2.0 (c (n "vclock") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11f1m015yl8b1d3a9ymvz2kckqdjdadlf79p6lfpdlci6q1v96ns")))

(define-public crate-vclock-0.2.1 (c (n "vclock") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bkx0yraj7gyjswa48z5w8z36rbawb5cspig3whvjznsk45p4nnk")))

(define-public crate-vclock-0.2.2 (c (n "vclock") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yzaaq0h489yvmgl59vw2f4zmhsfpbjgj0jf2bm96mm64hjh96f9") (y #t)))

(define-public crate-vclock-0.2.3 (c (n "vclock") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gdpdscgd5wbfqq9crzw0h6f8z2rrzzdrzf189k73adp10x63b0q")))

(define-public crate-vclock-0.3.0 (c (n "vclock") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ir031y4sx7czl2y9bp078642kjbzx1rwgpyhmwbmd2k97nz1pw1") (y #t)))

(define-public crate-vclock-0.3.1 (c (n "vclock") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0pqhs3bma7nc1b1fmc2aiw0ijqnkszcnhlwbi8lbrx3xqv212ad9")))

(define-public crate-vclock-0.4.0 (c (n "vclock") (v "0.4.0") (d (list (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1q4qkf2k6ivldiahg3qnqy62v87l5cjhwmfq90rlxnag71qx4b93") (s 2) (e (quote (("serde" "dep:serde") ("bigint" "dep:num-bigint"))))))

(define-public crate-vclock-0.4.1 (c (n "vclock") (v "0.4.1") (d (list (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "17d7qh994f3gm3g95n7h1f969npigj2zvsi7flj3m2qgcj8nsjsd") (s 2) (e (quote (("serde" "dep:serde") ("bigint" "dep:num-bigint"))))))

(define-public crate-vclock-0.4.2 (c (n "vclock") (v "0.4.2") (d (list (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fwbsccmbqsxkjvw22fs1h230xa5vrw49dy8bxfvkhya810m9s1d") (s 2) (e (quote (("serde" "dep:serde") ("bigint" "dep:num-bigint"))))))

(define-public crate-vclock-0.4.3 (c (n "vclock") (v "0.4.3") (d (list (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kgd84bqxadpvvr0avmb0a9820rm94rya5faz5j2kba8pq6c418d") (s 2) (e (quote (("serde" "dep:serde") ("bigint" "dep:num-bigint")))) (r "1.60")))

(define-public crate-vclock-0.4.4 (c (n "vclock") (v "0.4.4") (d (list (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12am05npjnan6qc0fhnzdahpiavq2bc7gdxn1j9fn64jn09fvls5") (s 2) (e (quote (("serde" "dep:serde") ("bigint" "dep:num-bigint")))) (r "1.60")))

