(define-module (crates-io vc du vcdump) #:use-module (crates-io))

(define-public crate-vcdump-0.1.0 (c (n "vcdump") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "vcd") (r "^0.5.0") (d #t) (k 0)))) (h "08bgv9z5llac6lj07qkswh439c4jk0l7rfn4r81xhi9s15aijli4")))

(define-public crate-vcdump-0.1.1 (c (n "vcdump") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "vcd") (r "^0.5.0") (d #t) (k 0)))) (h "0ik7vkj4ccb88mw8nq83ksvjlzkvkpvrxsfsqa5w1ivar547jjgw")))

(define-public crate-vcdump-0.1.2 (c (n "vcdump") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "vcd") (r "^0.6") (d #t) (k 0)))) (h "12hvl6i5fmyrgwlplk6229g3z5pkbb8c7p3qzm43clnm493bszqw")))

