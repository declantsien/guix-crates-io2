(define-module (crates-io vc he vcheat) #:use-module (crates-io))

(define-public crate-vcheat-0.1.0 (c (n "vcheat") (v "0.1.0") (h "0n2mbairii3bh4jfmi75cgsrzjxvbjqdy7vkcjxgpdnwlf2nfpgv") (y #t)))

(define-public crate-vcheat-0.1.1 (c (n "vcheat") (v "0.1.1") (h "0igxp75b2bzkr5z9ifby3i6ry5rmgh5mzpnpwv1dclznam0bdvv7") (y #t)))

(define-public crate-vcheat-0.1.2 (c (n "vcheat") (v "0.1.2") (h "1636dy1gc0zmc1bg9kz3m9gl4h2ljb1qrpmfs3z820nj26pgfzfb") (y #t)))

(define-public crate-vcheat-0.1.3 (c (n "vcheat") (v "0.1.3") (h "1x09yqpd2skdj77gb97wkm62y2llh580barfly1glh3nipkakczl") (y #t)))

(define-public crate-vcheat-0.1.4 (c (n "vcheat") (v "0.1.4") (h "1g7v5qv6b9an5v50gwfd6qrm29gsx4nl8kx04zf88jahrp9x9y5w") (y #t)))

(define-public crate-vcheat-0.1.5 (c (n "vcheat") (v "0.1.5") (h "1fxsxpgwpy74hpikg13xq5ai750y3bfnhdnbgx37cjmi93pdnsdl") (y #t)))

(define-public crate-vcheat-0.2.0-alpha (c (n "vcheat") (v "0.2.0-alpha") (h "1agv8qw954606l8ilny1fd8jpzz2qn239k5dx0z1caslz4x5afbf") (y #t)))

(define-public crate-vcheat-0.2.0-beta (c (n "vcheat") (v "0.2.0-beta") (h "1qgqksg3yjp5rcf59ypsz74va87p8nnm7dv645rjyn9vn7rn71a4") (y #t)))

(define-public crate-vcheat-0.2.0 (c (n "vcheat") (v "0.2.0") (h "17vaa25q4gpdiws9dpl4j8vikmijlb28a84557zjm4d95h63xn48") (y #t)))

(define-public crate-vcheat-0.3.0 (c (n "vcheat") (v "0.3.0") (h "0ks0k4hf62v258gjr9b6xk9i5a6ki16lwfhm4qwi52ixxw2w3ljp") (y #t)))

(define-public crate-vcheat-0.3.2 (c (n "vcheat") (v "0.3.2") (h "1nd60p8sdijnp8brgw5hwcnb10mhgc6xlrrg1jlh43d70ksc9rk7") (y #t)))

(define-public crate-vcheat-0.4.0 (c (n "vcheat") (v "0.4.0") (d (list (d (n "unsafe_fn_body") (r "^0.1.0") (d #t) (k 0)))) (h "0p6xs1ba61y3ww03wn57dnlh51by9sz71dcdm4zhj2b4rw4gq5hc")))

(define-public crate-vcheat-0.4.1 (c (n "vcheat") (v "0.4.1") (d (list (d (n "unsafe_fn_body") (r "^0.1.0") (d #t) (k 0)))) (h "1sqb0wa1znizrf3hsxc4rh1n3g8g796ln6fd975d23zbffcsam11")))

(define-public crate-vcheat-0.4.3 (c (n "vcheat") (v "0.4.3") (d (list (d (n "unsafe_fn_body") (r "^0.1.0") (d #t) (k 0)))) (h "0qzvq9jj5dibyysimw265qnwjz92w2m2mw8y6a9kq3dc9f50d0a0")))

(define-public crate-vcheat-0.5.0 (c (n "vcheat") (v "0.5.0") (h "1jb423xqc7lx9lg3q44jzn9371m1w4l8v7pcd5zy6ya85mpr8aza") (y #t)))

(define-public crate-vcheat-0.5.1 (c (n "vcheat") (v "0.5.1") (h "06jjq72fq3jj93j8fv2mq8fidhiwpn2dh1xh7d0qa3gnj2bbxgrr")))

(define-public crate-vcheat-0.5.2 (c (n "vcheat") (v "0.5.2") (h "1nwcnvsnc6xj75dvfaxz34f9wv7bq16ff9fgy3zyf1nxm1cykaf9")))

(define-public crate-vcheat-0.6.0 (c (n "vcheat") (v "0.6.0") (h "1qznb83x4zv2pqqyxc6xdin9q6f6il8ibk11v1dn7qr6h3cz6h5i")))

(define-public crate-vcheat-0.6.1 (c (n "vcheat") (v "0.6.1") (h "0zcia04xbf02c0g4589zgh0scdy639i8l7mc15kilacpjxwqqi2l")))

(define-public crate-vcheat-0.6.2 (c (n "vcheat") (v "0.6.2") (h "13dqzangc1bkwy68p25k49m55gyfwqn4ih5nbzsjzf5nc2q3ggqq")))

(define-public crate-vcheat-0.6.3 (c (n "vcheat") (v "0.6.3") (h "12l89ix7y4s8vs61r2ivbxlmcs93v3i57kh018mrywr37wl88qi9")))

(define-public crate-vcheat-0.6.4 (c (n "vcheat") (v "0.6.4") (h "1wsdijy20h00ivz07s00j8ax07wbac3vyvlaanlszm2kv8qz295i")))

(define-public crate-vcheat-0.6.5 (c (n "vcheat") (v "0.6.5") (h "0b9c9kcf7cpnjl0qighc4v0q0mdnch5nxyhfcydpcif8nzzy94vg")))

