(define-module (crates-io vc f_ vcf_batcher) #:use-module (crates-io))

(define-public crate-vcf_batcher-0.1.0 (c (n "vcf_batcher") (v "0.1.0") (d (list (d (n "bgzip") (r "^0.3.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "0qalq19d7km9p2r6lmv0c1hsxgm4085xy3hzm8fiy1xamxvjyipv")))

(define-public crate-vcf_batcher-0.2.0 (c (n "vcf_batcher") (v "0.2.0") (d (list (d (n "bgzip") (r "^0.3.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "147pn9mcsw3dh53prd64gxf7j1i06pfhf0688dcmfp24vj9hfhcx")))

(define-public crate-vcf_batcher-0.2.1 (c (n "vcf_batcher") (v "0.2.1") (d (list (d (n "bgzip") (r "^0.3.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "0kb3ylpryfbd8zj6hn13mfb4s6srdwrw3zs0mhvcfwli4n7ajl11")))

