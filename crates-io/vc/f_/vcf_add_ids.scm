(define-module (crates-io vc f_ vcf_add_ids) #:use-module (crates-io))

(define-public crate-vcf_add_ids-0.1.0 (c (n "vcf_add_ids") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "vcf") (r "^0.6.1") (d #t) (k 0)))) (h "02h248694isrv1njj38bzglinc4i24schy2bg4lzfb4lhvaqgk06")))

(define-public crate-vcf_add_ids-0.1.1 (c (n "vcf_add_ids") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "vcf") (r "^0.6.1") (d #t) (k 0)))) (h "02zzjfyiwbdgy6vnrmfmhj4gzj0nywnqgc97fjnbfc91iaff2kic")))

