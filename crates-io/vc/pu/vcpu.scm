(define-module (crates-io vc pu vcpu) #:use-module (crates-io))

(define-public crate-vcpu-0.1.0 (c (n "vcpu") (v "0.1.0") (d (list (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fidfacv42pj5qnak31qy8xx7f22r862krkija1gchq99rpx376w")))

(define-public crate-vcpu-0.2.0 (c (n "vcpu") (v "0.2.0") (d (list (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12h0303p16id9bpxvsp1qrlnqmbspm6vrl0p6plj4njl82q8i6gr")))

