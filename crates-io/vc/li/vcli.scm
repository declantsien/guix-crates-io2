(define-module (crates-io vc li vcli) #:use-module (crates-io))

(define-public crate-vcli-0.1.0 (c (n "vcli") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "1qhawhx3q665spkdlp1sigk19xp89pd8jzpp0x2prkm6q7213gpg")))

(define-public crate-vcli-0.1.1 (c (n "vcli") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "1zip8zndnbi0apszwhm1xmvnrmc71ilvs686yn01g0brncr1sn2y")))

(define-public crate-vcli-0.1.2 (c (n "vcli") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "1gpkcmcxcllipqvy57lg90alaqcm6mcqllwc6fdrc9gdh33yljlg")))

(define-public crate-vcli-0.1.3 (c (n "vcli") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "0dxgk762v4rc8i077sggqizva0kdad3dzj7664y1l3i122ad2wfl")))

(define-public crate-vcli-0.1.4 (c (n "vcli") (v "0.1.4") (d (list (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "11i18l0gawz8vmpz387jyzwj8f9ghc2v8r75qy79plsb2nsk45ky")))

