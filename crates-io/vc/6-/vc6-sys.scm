(define-module (crates-io vc #{6-}# vc6-sys) #:use-module (crates-io))

(define-public crate-vc6-sys-0.1.0 (c (n "vc6-sys") (v "0.1.0") (h "1jfqsc6zhy709fvbdc99rwkqb43yxbamknhqxspzxms6nynvk1ah")))

(define-public crate-vc6-sys-0.1.1 (c (n "vc6-sys") (v "0.1.1") (h "06c4xgj2pyi7n1s85z4is49zq7gyidn2sfdqai15ycza9inar9qp")))

(define-public crate-vc6-sys-0.1.2 (c (n "vc6-sys") (v "0.1.2") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "11a34hrx074gj84nlhmzw1vrabh8lxk9bqlwyqc5im6b1basrcx5") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std"))))))

