(define-module (crates-io vc s- vcs-classic-hid) #:use-module (crates-io))

(define-public crate-vcs-classic-hid-0.1.0 (c (n "vcs-classic-hid") (v "0.1.0") (d (list (d (n "hidapi") (r "^1.2.5") (k 0)))) (h "1a117ppa4s42b73vf31hirrvyypn88wiqyyzb5v8lyrm1da6x6p2") (f (quote (("linux-static-libusb" "hidapi/linux-static-libusb") ("linux-static-hidraw" "hidapi/linux-static-hidraw") ("default" "linux-static-hidraw"))))))

