(define-module (crates-io vc st vcstatus) #:use-module (crates-io))

(define-public crate-vcstatus-0.1.0 (c (n "vcstatus") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "01q4vyvrhvlq0lgxcd2mf32jd5f2wf2iyr4drvikhr96hjl02nvy")))

(define-public crate-vcstatus-0.1.1 (c (n "vcstatus") (v "0.1.1") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1b3rwlvnqnaka6bnhy1ymrqc5jvaxv3abn715jmczlgg7cc7zq45")))

