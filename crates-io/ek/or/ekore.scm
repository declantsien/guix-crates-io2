(define-module (crates-io ek or ekore) #:use-module (crates-io))

(define-public crate-ekore-0.1.0 (c (n "ekore") (v "0.1.0") (h "0xwvy590nw362h4rf9zj2kyys2kjn1vc9mhgx57nzcgsnxv3ypm3")))

(define-public crate-ekore-0.0.1 (c (n "ekore") (v "0.0.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1s8lx40874l4qla19wqmf1qgh8qqjzbjnxa51qzgvm81xh7cfla8") (r "1.60.0")))

(define-public crate-ekore-0.0.2 (c (n "ekore") (v "0.0.2") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1h7aklikp94v50n6d2y5lrm5q1c0kkm0i03gls5fs053pcawrk56") (r "1.60.0")))

(define-public crate-ekore-0.0.5 (c (n "ekore") (v "0.0.5") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1w75d2k40x91gf770gnn3xvsbzgcikl9q01kygb0a9ny2ljazp2m") (r "1.60.0")))

(define-public crate-ekore-0.1.1-alpha.0 (c (n "ekore") (v "0.1.1-alpha.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1yml825pzf4c694y7cp59zq0cfmsrqvfhmg6ql22l780c58p7k77") (r "1.60.0")))

(define-public crate-ekore-0.1.1-alpha.1 (c (n "ekore") (v "0.1.1-alpha.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "18h9vl5hpjcp4aq5cda1aplqlxz27ahyhkzi5kw3fkjn5hxykvq5") (r "1.60.0")))

(define-public crate-ekore-0.1.1-alpha.2 (c (n "ekore") (v "0.1.1-alpha.2") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1s8mcanaxcnvhr38a04vkpl6qy9zdyv625ivvbz9vf9mas10kgcl") (r "1.60.0")))

(define-public crate-ekore-0.1.1-alpha.3 (c (n "ekore") (v "0.1.1-alpha.3") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0ig5kxy7gd1sl58sbza14lqwwkr919dwmzniimchz4yixnc2d304") (r "1.60.0")))

(define-public crate-ekore-0.1.1-alpha.4 (c (n "ekore") (v "0.1.1-alpha.4") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "19h4lk0z7jkqc8vlbvw5j1afnrdq5fnv4k73d0qdk633pycszqnz") (r "1.60.0")))

(define-public crate-ekore-0.1.1-alpha.5 (c (n "ekore") (v "0.1.1-alpha.5") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "17hl3ihhn8w6nmhcpyinxxvdnni9h3rixfkcsypbqzk9pzdvikcw") (r "1.60.0")))

(define-public crate-ekore-0.14.4-alpha.1 (c (n "ekore") (v "0.14.4-alpha.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1kva5hj8cz1q0q69xk9h80wpnby9hdxw9ma0gxi730vz4jcc1437") (r "1.60.0")))

