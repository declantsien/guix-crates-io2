(define-module (crates-io ek ey ekey) #:use-module (crates-io))

(define-public crate-ekey-0.1.0 (c (n "ekey") (v "0.1.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "11pyffg4qxpydxrm8gk9z8akfapkq5kg095rh4s9ppaxgc4bhyli")))

(define-public crate-ekey-0.2.0 (c (n "ekey") (v "0.2.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dv042n4j0bd7zlnpbwgya76y2gvz2mn2b4cssx6zj9qirkh7dl4") (f (quote (("std") ("default" "std"))))))

(define-public crate-ekey-0.2.1 (c (n "ekey") (v "0.2.1") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qh6mppp2ij1r4gjj5w66s3ba33nzcyq06c0s49bk9vnd5cq8d0b") (f (quote (("std") ("default" "std"))))))

(define-public crate-ekey-0.3.0 (c (n "ekey") (v "0.3.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "16h2zvp60wkdscmk0x8ngxszj5dk661vccmy04abkhvgm9fzj7d0") (f (quote (("std") ("default" "std"))))))

(define-public crate-ekey-0.4.0 (c (n "ekey") (v "0.4.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vdzcbz0ic5mjzvb58is8n09zcz8bgvknjh8g2r7nshndbmmsv6s") (f (quote (("std") ("default" "std"))))))

(define-public crate-ekey-0.5.0 (c (n "ekey") (v "0.5.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1df2ml49qi4yr3zawcdm6jiyy6mldy1041p7g6g3wxrprcwp2x13") (f (quote (("std") ("default" "std"))))))

(define-public crate-ekey-0.6.0 (c (n "ekey") (v "0.6.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j98rl982gsas90ly1rhnhmj0672wi8f3bdvz8rcc4ldij8vw0ib") (f (quote (("std") ("default" "std"))))))

