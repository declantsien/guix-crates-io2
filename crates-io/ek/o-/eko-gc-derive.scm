(define-module (crates-io ek o- eko-gc-derive) #:use-module (crates-io))

(define-public crate-eko-gc-derive-0.0.1 (c (n "eko-gc-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)) (d (n "synstructure") (r "^0.7.0") (d #t) (k 0)))) (h "140m9s5khcb9svlzlh2av6dw29139fga1sp0gq3vd42v32ls8d0x")))

(define-public crate-eko-gc-derive-0.0.2 (c (n "eko-gc-derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)) (d (n "synstructure") (r "^0.7.0") (d #t) (k 0)))) (h "08vhfj8n4gjli57pp36x56cy39hqkmmca5b2vkddq250j08v3wh3")))

