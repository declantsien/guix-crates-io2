(define-module (crates-io ek id ekiden-grpcio-sys) #:use-module (crates-io))

(define-public crate-ekiden-grpcio-sys-0.2.3 (c (n "ekiden-grpcio-sys") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.27") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0cfn2xr50cali6p3kf7sb1na5f1s3x0wb9dzmk3qihdjvxjgnngl") (f (quote (("secure") ("default")))) (y #t)))

(define-public crate-ekiden-grpcio-sys-0.2.4 (c (n "ekiden-grpcio-sys") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.27") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hqhwkirwxr2cv6hl9fg87zzdyz731bpzhqy9khcdysqx2f34ygd") (f (quote (("secure") ("default"))))))

