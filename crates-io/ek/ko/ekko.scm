(define-module (crates-io ek ko ekko) #:use-module (crates-io))

(define-public crate-ekko-0.1.0 (c (n "ekko") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "socket2") (r "^0.3.15") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19.5") (d #t) (k 0)))) (h "07hxi5lc5j7vqr1rkyxjk52b56x5v9p91g3zd5sfzim614c69zmb") (y #t)))

(define-public crate-ekko-0.1.1 (c (n "ekko") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "socket2") (r "^0.3.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19.5") (d #t) (k 0)))) (h "171bf317d88z67qjqp45viml3sls6glfrsy6i2xqvy0358fd8v67") (y #t)))

(define-public crate-ekko-0.1.2 (c (n "ekko") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "socket2") (r "^0.3.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19.5") (d #t) (k 0)))) (h "1ahwd81lnnzf01738k418wlawvfr80ir4abz8p835wc7x5s9adz6") (y #t)))

(define-public crate-ekko-0.1.3 (c (n "ekko") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "socket2") (r "^0.3.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19.5") (d #t) (k 0)))) (h "15v9cfi0cxsmwpvdydz7vyis2yz7n6ikfrf3zbl2s3ywm3d4h70s") (y #t)))

(define-public crate-ekko-0.2.0 (c (n "ekko") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "socket2") (r "^0.3.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "111iggqid7isxgqxda5kdv9j195xazq08qma82n72bjhbldayrzq") (y #t)))

(define-public crate-ekko-0.2.1 (c (n "ekko") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "socket2") (r "^0.3.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1a4kwrlvlchnrqcllrb84y9awp7ad3dcl4bdg7ladp57asf4cwrj") (y #t)))

(define-public crate-ekko-0.3.0 (c (n "ekko") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "socket2") (r "^0.3.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "186vmv60x0122b8h31gyr5saww5zann3l5zhl8yfmcd5r1sq7lk2")))

(define-public crate-ekko-0.4.0 (c (n "ekko") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "socket2") (r "^0.3.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0c1ipbhia39125cvmf8wx7vnch99908zqjrznlid6kqmzmyi86jn")))

(define-public crate-ekko-0.5.0 (c (n "ekko") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "socket2") (r "^0.4.0") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "193ghahj8kzdw569hqngcr3wzn6aa2qyrnxy1izpxvdcydwxrad4")))

(define-public crate-ekko-0.6.0 (c (n "ekko") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "socket2") (r "^0.4.0") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0d9agpc5n3r434x89af6jl59kzz3njgil1zk1ps8ab9al096jcsp")))

(define-public crate-ekko-0.7.0 (c (n "ekko") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "socket2") (r "^0.4.0") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1r8zqc01apr1inrl55zvkbg77rlsydcxn7hpq1flj2kw8sqprr8g")))

(define-public crate-ekko-0.7.1 (c (n "ekko") (v "0.7.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "socket2") (r "^0.4.2") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "09rf4bmhikh0968jhvzgjw3gfc1nbbnsvbxszm4cfq4kcvbqd9my")))

(define-public crate-ekko-0.7.2 (c (n "ekko") (v "0.7.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "socket2") (r "^0.4.2") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0i0dfsybd624ix3c2sz6bggsq0k8jhwk54m85czbaa1wgdijvc49")))

(define-public crate-ekko-0.7.3 (c (n "ekko") (v "0.7.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1h4jp6h8f0pcg2snimgcwfyiq41rchzr1hzvkmwr8ixm3zxhqawx")))

