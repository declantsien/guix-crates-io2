(define-module (crates-io wm -d wm-daemons) #:use-module (crates-io))

(define-public crate-wm-daemons-0.1.1 (c (n "wm-daemons") (v "0.1.1") (d (list (d (n "clap") (r "~1.2.0") (d #t) (k 0)) (d (n "config") (r "~0.1.2") (d #t) (k 0)) (d (n "xdg-basedir") (r "~0.2.0") (d #t) (k 0)))) (h "1gvymnd7w2qichn8imn09bkkxh3zg10zzdvr6a3blc86ws0sh5cf")))

