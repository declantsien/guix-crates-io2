(define-module (crates-io wm ct wmctl) #:use-module (crates-io))

(define-public crate-wmctl-0.0.41 (c (n "wmctl") (v "0.0.41") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^2.33") (f (quote ("suggestions"))) (k 0)) (d (n "gory") (r "^0.1") (d #t) (k 0)) (d (n "libwmctl") (r "^0.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "witcher") (r "^0.1.19") (d #t) (k 0)))) (h "1a5fd0y16xlb7h8ylybyrfj1v6h1hnhqbm8xfhg9g9br4rn3kkvk")))

(define-public crate-wmctl-0.0.45 (c (n "wmctl") (v "0.0.45") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^2.33") (f (quote ("suggestions"))) (k 0)) (d (n "gory") (r "^0.1") (d #t) (k 0)) (d (n "libwmctl") (r "^0.0.45") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "witcher") (r "^0.1.19") (d #t) (k 0)))) (h "0p34im6rs6jd8zi17wm6spp8sm1k10nxvfg4gga315wa4sjzw5fl")))

