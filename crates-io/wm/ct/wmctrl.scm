(define-module (crates-io wm ct wmctrl) #:use-module (crates-io))

(define-public crate-wmctrl-0.1.3 (c (n "wmctrl") (v "0.1.3") (h "1ygc78j4jbsxra9cxvff38q2lsnrm0gghh22m2cx8lfa3pzsc412")))

(define-public crate-wmctrl-0.1.4 (c (n "wmctrl") (v "0.1.4") (h "0nxxhv5k5srwwkdcipsfqyxgq4n9w8wl8mf61sqj97wd34bmmh7z")))

(define-public crate-wmctrl-0.1.5 (c (n "wmctrl") (v "0.1.5") (h "0476gvcw1i2jgqq010qd75112zcfbanw8f4bgb8b40hvwpmzc2k5")))

(define-public crate-wmctrl-0.1.6 (c (n "wmctrl") (v "0.1.6") (h "0yjg13drr2wbgy93agqysx3mcdh82i94c68xplgqima2zylqbbwd")))

(define-public crate-wmctrl-0.1.7 (c (n "wmctrl") (v "0.1.7") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "164smwvhqi83rcmlq27c4s8c52znvwy8xgi9gy2gzib2bk7qxa0w")))

(define-public crate-wmctrl-0.1.8 (c (n "wmctrl") (v "0.1.8") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0awrwgk77ibyivw04kxkrn2qbyrc2580adxyir4pfmsa64lxkdbl")))

