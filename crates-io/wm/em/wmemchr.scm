(define-module (crates-io wm em wmemchr) #:use-module (crates-io))

(define-public crate-wmemchr-0.1.0 (c (n "wmemchr") (v "0.1.0") (d (list (d (n "autocfg") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "wchar") (r "^0.10") (d #t) (k 2)))) (h "0flb2yxh492xzzs33dwl38dq4h2jkscvbsy34bvmjlgvdfn0mah1") (f (quote (("unstable") ("std") ("default" "std"))))))

