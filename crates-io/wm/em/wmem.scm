(define-module (crates-io wm em wmem) #:use-module (crates-io))

(define-public crate-wmem-0.1.0 (c (n "wmem") (v "0.1.0") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "consoleapi" "handleapi" "processthreadsapi" "memoryapi" "tlhelp32"))) (d #t) (k 0)))) (h "09ni77kxs8dr0jjgw4xzg205fhqff48n4b9i6j4ygn3vfhj0f6rb")))

