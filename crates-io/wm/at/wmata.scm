(define-module (crates-io wm at wmata) #:use-module (crates-io))

(define-public crate-wmata-1.0.0 (c (n "wmata") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0lw9f9jkyzik75rxiwcpykn7y3nd0bldkjk2ydhi5hjlv95fkxva")))

(define-public crate-wmata-2.0.0 (c (n "wmata") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.9.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0d5xai74fxv95i7vw401h8y56w4xv8w4jc8779n9sg5c9wp2z9rd")))

(define-public crate-wmata-2.0.1 (c (n "wmata") (v "2.0.1") (d (list (d (n "reqwest") (r "^0.9.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0k5pr8jkkjrw4sf2fyd0zjpbr3w18k2dw92qgxz0in59zw1fai4r")))

(define-public crate-wmata-3.0.0 (c (n "wmata") (v "3.0.0") (d (list (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "10gcf5xfy5ya1n14bc0hbz3san8863gda0pnnqyk906v291nv9s0")))

(define-public crate-wmata-3.0.1 (c (n "wmata") (v "3.0.1") (d (list (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1xai1hv72clbw3lzwcq1m8263vdjraimv6pg9xgkmk7mrzi2qphx")))

(define-public crate-wmata-4.0.0 (c (n "wmata") (v "4.0.0") (d (list (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1lxq6r35qhnl8cy4mqki4ygix8wzxg6imz7apv1sasvg4ay03b01")))

(define-public crate-wmata-5.0.0 (c (n "wmata") (v "5.0.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1y80m0k7l5dy2g5cd7xhj8sws7jz0m7jq503vphhwyp64wz10z85")))

(define-public crate-wmata-5.0.1 (c (n "wmata") (v "5.0.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "173qfhv8p2nxaik40ly711yww3k5grmszfy3lg27rx727psb24jh")))

(define-public crate-wmata-6.0.0 (c (n "wmata") (v "6.0.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0swdy41mzm35psvdr1gk77xmx326abc550cyj7c7ximafvljm6pb")))

(define-public crate-wmata-7.0.0 (c (n "wmata") (v "7.0.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.0") (d #t) (k 0)))) (h "1987n72wrbdlp9pffm1a1w3m1dklprzj2qhjdn32nsy1qn2gfc9w")))

(define-public crate-wmata-7.0.1 (c (n "wmata") (v "7.0.1") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.0") (d #t) (k 0)))) (h "1lckx6p8r7qxc035npw9a7kwc0848rmdd5znwhpmdi9cx6sqyma7")))

(define-public crate-wmata-7.1.0 (c (n "wmata") (v "7.1.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.0") (d #t) (k 0)))) (h "1ab8ilib6ajfa49p9a8nfqi4rbhaz6x0bdggl70qv4i06yi4lhnh")))

