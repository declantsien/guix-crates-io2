(define-module (crates-io iz ia izia) #:use-module (crates-io))

(define-public crate-izia-0.1.0 (c (n "izia") (v "0.1.0") (d (list (d (n "linefeed") (r "^0.5.3") (d #t) (k 0)) (d (n "zia") (r "^0.1") (d #t) (k 0)))) (h "0d07fgagxzr69661v9b1s7w6ab5d5vwwzxvzsc77l70q7jw1kp70")))

(define-public crate-izia-0.2.0 (c (n "izia") (v "0.2.0") (d (list (d (n "linefeed") (r "^0.5.3") (d #t) (k 0)) (d (n "zia") (r "^0") (d #t) (k 0)))) (h "0dih9klm1gwvsn9ji3x7qvjaag3bjmf2kkmzrvhzh06ilw5sa9hn")))

(define-public crate-izia-0.3.0 (c (n "izia") (v "0.3.0") (d (list (d (n "linefeed") (r "^0.5.3") (d #t) (k 0)) (d (n "zia") (r "^0") (d #t) (k 0)))) (h "10aj2sbrs8a9xscnawkrnxrr9sb7nn7icr12lkz952pwvgfrfvbx")))

(define-public crate-izia-0.4.0 (c (n "izia") (v "0.4.0") (d (list (d (n "linefeed") (r "^0.5.3") (d #t) (k 0)) (d (n "zia") (r "^0.4.0") (d #t) (k 0)))) (h "0dpj10nxx845yhmw7isxi71j1hzw0ivd5qxyhp2x32y83pac5ln9")))

(define-public crate-izia-0.5.0 (c (n "izia") (v "0.5.0") (d (list (d (n "linefeed") (r "^0.5.3") (d #t) (k 0)) (d (n "zia") (r "^0.5.0") (d #t) (k 0)))) (h "0vzlysgs2nsvdgdlw28vrqpfx2yk8a85vgz09gjifnn2jdp9ia7h")))

