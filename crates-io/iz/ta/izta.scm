(define-module (crates-io iz ta izta) #:use-module (crates-io))

(define-public crate-izta-0.1.1 (c (n "izta") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v767jsr8wg9bdx9spf8qxggv31bsn6jdx7jzyjlbqynmlkym2xf")))

(define-public crate-izta-0.1.2 (c (n "izta") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mpnnybr0ifpms53n3fjgf9yg0lgb4l9k9l6fbka17qwkibhihv6")))

