(define-module (crates-io iz an izanami-http) #:use-module (crates-io))

(define-public crate-izanami-http-0.1.0-preview.1 (c (n "izanami-http") (v "0.1.0-preview.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "izanami-service") (r "^0.1.0-preview.2") (d #t) (k 0)) (d (n "izanami-util") (r "^0.1.0-preview.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio-buf") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.7") (d #t) (k 2)))) (h "09ilf2526xdjf2iw9c6cid4psyfdfm6680dkajfb792vlq834z8h") (f (quote (("default" "serde_json")))) (y #t)))

