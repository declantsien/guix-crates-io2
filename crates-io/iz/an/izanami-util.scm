(define-module (crates-io iz an izanami-util) #:use-module (crates-io))

(define-public crate-izanami-util-0.1.0-preview.1 (c (n "izanami-util") (v "0.1.0-preview.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-threadpool") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.7") (d #t) (k 2)))) (h "1vzvbndb85jgd9ymlvx9sqviqz18ks435dpzfidly7gpjvxi6jjq") (y #t)))

(define-public crate-izanami-util-0.1.0-preview.2 (c (n "izanami-util") (v "0.1.0-preview.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.7") (d #t) (k 2)))) (h "14cxh0ai11rnfkga2rh2v5r0nhyq6rf08f9jim12jv44jc9nh4jx") (y #t)))

