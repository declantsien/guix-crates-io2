(define-module (crates-io iz an izanami-service) #:use-module (crates-io))

(define-public crate-izanami-service-0.1.0-preview.1 (c (n "izanami-service") (v "0.1.0-preview.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tower-service") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.7") (d #t) (k 2)))) (h "0kdzij64z42gfj920iaxqnsajz5l2bn7x9iascbilb79h2dg0x6a") (y #t)))

(define-public crate-izanami-service-0.1.0-preview.2 (c (n "izanami-service") (v "0.1.0-preview.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "izanami-util") (r "^0.1.0-preview.2") (d #t) (k 0)) (d (n "tower-service") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.7") (d #t) (k 2)))) (h "15hgg5sv5wr8mjh7zbv6q5k46gw5jz8cpk803mbki2y48xrflszn") (y #t)))

