(define-module (crates-io iz ih izihawa-tantivy-query-grammar) #:use-module (crates-io))

(define-public crate-izihawa-tantivy-query-grammar-0.20.0 (c (n "izihawa-tantivy-query-grammar") (v "0.20.0") (d (list (d (n "combine") (r "^4") (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std" "unicode"))) (k 0)))) (h "0qi0238b697frf00y2grkf4ldq111z46ax1vp3np2zmb5hydv0xk")))

(define-public crate-izihawa-tantivy-query-grammar-0.21.0 (c (n "izihawa-tantivy-query-grammar") (v "0.21.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0m18z77a07k74bgkalp63nkxgslhdw67imrp71kf7zwgva6n5sg3")))

(define-public crate-izihawa-tantivy-query-grammar-0.21.0-dev (c (n "izihawa-tantivy-query-grammar") (v "0.21.0-dev") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1j8xq7kq77mfk7nifqraw6bj31alqhjfghdrlycg132alz051qzc")))

(define-public crate-izihawa-tantivy-query-grammar-0.21.1 (c (n "izihawa-tantivy-query-grammar") (v "0.21.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0pcjj6n32jawr31c5afw7i7qlbf0wvxk4nnr28j1kzgrfx8pf521")))

(define-public crate-izihawa-tantivy-query-grammar-0.22.0 (c (n "izihawa-tantivy-query-grammar") (v "0.22.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "18q3jfz4f976f8pd6kkks695gqqfrgr1kb7v7f425icxqmhp6arq")))

