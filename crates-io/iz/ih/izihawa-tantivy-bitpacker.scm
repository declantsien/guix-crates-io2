(define-module (crates-io iz ih izihawa-tantivy-bitpacker) #:use-module (crates-io))

(define-public crate-izihawa-tantivy-bitpacker-0.4.0 (c (n "izihawa-tantivy-bitpacker") (v "0.4.0") (d (list (d (n "bitpacking") (r "^0.8") (f (quote ("bitpacker1x"))) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1g0fmr9i0lbn6lj458pin0avifpnrir4vxxqc3s2bqa783vrg4qp")))

(define-public crate-izihawa-tantivy-bitpacker-0.4.1 (c (n "izihawa-tantivy-bitpacker") (v "0.4.1") (d (list (d (n "bitpacking") (r "^0.8") (f (quote ("bitpacker1x"))) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0nl28g15m5aaj9z5xd23k713izpzirszmshxdfarfm17lvbr98iy")))

(define-public crate-izihawa-tantivy-bitpacker-0.5.0 (c (n "izihawa-tantivy-bitpacker") (v "0.5.0") (d (list (d (n "bitpacking") (r "^0.8") (f (quote ("bitpacker1x"))) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "15fsdmwjvwyy48a0hwyzz5gmh0d4xsafafpph9y1yk7ilgyfhn5a")))

(define-public crate-izihawa-tantivy-bitpacker-0.6.0 (c (n "izihawa-tantivy-bitpacker") (v "0.6.0") (d (list (d (n "bitpacking") (r "^0.9.2") (f (quote ("bitpacker1x"))) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "14fpz827ydjfyza5rrlrlbb2gwmq6x3pkbv3aaj03z0wnagb3fcs")))

