(define-module (crates-io iz ih izihawa_lru_time_cache) #:use-module (crates-io))

(define-public crate-izihawa_lru_time_cache-0.11.11 (c (n "izihawa_lru_time_cache") (v "0.11.11") (d (list (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "~0.6.5") (d #t) (k 2)) (d (n "sn_fake_clock") (r "~0.4.0") (o #t) (d #t) (k 0)))) (h "1pkx65ylj7y26ndrkz2bcg9lnkrzdn1qapc77zbmlynzcw7pzywr")))

(define-public crate-izihawa_lru_time_cache-0.12.0 (c (n "izihawa_lru_time_cache") (v "0.12.0") (d (list (d (n "instant") (r "^0.1") (f (quote ("inaccurate" "wasm-bindgen"))) (k 0)) (d (n "rand") (r "~0.6.5") (d #t) (k 2)) (d (n "sn_fake_clock") (r "~0.4.0") (o #t) (d #t) (k 0)))) (h "1nv8m7p1b2iw20m6fmhww2j9315wis59lnh4jr3yra3j5hg5czqh")))

