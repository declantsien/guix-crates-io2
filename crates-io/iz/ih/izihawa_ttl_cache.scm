(define-module (crates-io iz ih izihawa_ttl_cache) #:use-module (crates-io))

(define-public crate-izihawa_ttl_cache-0.5.1 (c (n "izihawa_ttl_cache") (v "0.5.1") (d (list (d (n "instant") (r "^0.1") (f (quote ("inaccurate" "wasm-bindgen"))) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "19rb3ky44wcs0pbf9akaq3ysk823xmfidw2zzd07zg57l394s3ff") (f (quote (("stats") ("default"))))))

