(define-module (crates-io iz ih izihawa-ownedbytes) #:use-module (crates-io))

(define-public crate-izihawa-ownedbytes-0.5.0 (c (n "izihawa-ownedbytes") (v "0.5.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "1ncgvpc0vcrs88kbc1d4s43mrn0hiyk9jlz1b9y9ijgpk1qjvqab")))

(define-public crate-izihawa-ownedbytes-0.6.0 (c (n "izihawa-ownedbytes") (v "0.6.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "1xwna4blk42xa8syzbknzg3qpl6xrwq8wh9mbs4riv9ys7is4vvx")))

(define-public crate-izihawa-ownedbytes-0.7.0 (c (n "izihawa-ownedbytes") (v "0.7.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "04pp0rsbykdxwqqskkxxh23y4yda9q55vh421xzwd3hkg2py4pal")))

