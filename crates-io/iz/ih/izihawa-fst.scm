(define-module (crates-io iz ih izihawa-fst) #:use-module (crates-io))

(define-public crate-izihawa-fst-0.4.0 (c (n "izihawa-fst") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0i12iian50y8z7n5li12v04nalvk2zb4nbf06qzyminhimn69aqj")))

