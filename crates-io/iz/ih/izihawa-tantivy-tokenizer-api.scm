(define-module (crates-io iz ih izihawa-tantivy-tokenizer-api) #:use-module (crates-io))

(define-public crate-izihawa-tantivy-tokenizer-api-0.1.0 (c (n "izihawa-tantivy-tokenizer-api") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nk744kmalkvamx2gydw9wyfz9qm15sxycssj1y5vqgbmid58ip5")))

(define-public crate-izihawa-tantivy-tokenizer-api-0.2.0 (c (n "izihawa-tantivy-tokenizer-api") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0580myv5njjqns0155rqmyhw0j6bng9ppasdfilc10fjqnkmzpb8")))

(define-public crate-izihawa-tantivy-tokenizer-api-0.3.0 (c (n "izihawa-tantivy-tokenizer-api") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ymnsan8pfmac87w6q8h07plpwz1blz852cdjpw6qjmy6jwjr596")))

