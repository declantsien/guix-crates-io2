(define-module (crates-io hq -r hq-rs) #:use-module (crates-io))

(define-public crate-hq-rs-0.1.0 (c (n "hq-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hcl-rs") (r "^0.17") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)))) (h "0kjdwci6rpf33fm98zknnl7149j3zvynp0gf4gwiqsy1hpljb2wk")))

(define-public crate-hq-rs-0.2.0 (c (n "hq-rs") (v "0.2.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hcl-rs") (r "^0.17") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)))) (h "1v0fiyx072ylnmqyjvsvimqlyj3y4q12bc6w0i7lqcih426c6pii")))

(define-public crate-hq-rs-0.3.0 (c (n "hq-rs") (v "0.3.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hcl-rs") (r "^0.17") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)))) (h "029qzrbclc7m54v7ag27smpkfz32wkqbaalcxscxdjgnd0zsfpc5")))

(define-public crate-hq-rs-0.4.0 (c (n "hq-rs") (v "0.4.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hcl-rs") (r "^0.17") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)))) (h "0ns47dp7iia13ikb0ngi5nhdl4b122cxpspk0154xnc7wxxfpz4i")))

(define-public crate-hq-rs-0.5.0 (c (n "hq-rs") (v "0.5.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hcl-rs") (r "^0.17") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)))) (h "0qwh82dsjjsh9wfjshckqhskkx45ga0pyn9ap1mcrhvxj3gc7w93")))

