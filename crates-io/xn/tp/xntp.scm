(define-module (crates-io xn tp xntp) #:use-module (crates-io))

(define-public crate-xntp-0.1.0 (c (n "xntp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "1rfj8aphbarqqlddhddx4bdj7jsc0r2p863az71pvbl4gl5y7hnx")))

(define-public crate-xntp-0.1.1 (c (n "xntp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "1b8018q46g97imdwzg59s47vxs6m1lffdg4np5h1cnpiqhqxcbk4")))

(define-public crate-xntp-0.1.2 (c (n "xntp") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "0v8y6z9d7y4wpna0dhbllnlk641ysrb0yv42c1dqjikpdblaf52i")))

(define-public crate-xntp-0.1.3 (c (n "xntp") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "11qa7rxxnrak1b6c1d5pnx3r71dsnscgghl6i3dvi5qa98kjxxj9")))

