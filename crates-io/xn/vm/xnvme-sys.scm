(define-module (crates-io xn vm xnvme-sys) #:use-module (crates-io))

(define-public crate-xnvme-sys-0.0.0 (c (n "xnvme-sys") (v "0.0.0") (h "1k413jr18pi9926wz0yn0j4d1j5cplrfg53f7ywg5zz0vidiabya")))

(define-public crate-xnvme-sys-0.7.3 (c (n "xnvme-sys") (v "0.7.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "1w5vnnfhv4llhirlfkj65swiiqmh8j0dshcsp4ng6skbs77lgls5")))

(define-public crate-xnvme-sys-0.7.4 (c (n "xnvme-sys") (v "0.7.4") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "0l0lp91r3dhxvxs0xfqny0schkvlbfharqry2nqzz995ccp9n1xi")))

