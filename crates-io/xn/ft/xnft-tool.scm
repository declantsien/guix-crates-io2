(define-module (crates-io xn ft xnft-tool) #:use-module (crates-io))

(define-public crate-xnft-tool-0.1.1 (c (n "xnft-tool") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0lzzwq6isi6cv1l0zqssymg0hc4jn98ybkhpf7sh3w65c0gkspyd")))

(define-public crate-xnft-tool-0.1.2 (c (n "xnft-tool") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0phvg9xz3dqdq49n2c94yr37dq551dxr4a35mc661qdhayplx353")))

(define-public crate-xnft-tool-0.1.3 (c (n "xnft-tool") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1g4hg9hm6r7rnrkw65q7hlxxg3a164q1n0x4r12p801jq3dqdl5v")))

(define-public crate-xnft-tool-0.1.1-alpha.1 (c (n "xnft-tool") (v "0.1.1-alpha.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0qdfyf6al1cvhys0v72pr5z587ai25nrq7sa3i44knrywvs95bg6")))

(define-public crate-xnft-tool-0.1.4-alpha.1 (c (n "xnft-tool") (v "0.1.4-alpha.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "09v5kcnn2jvpzy3p6n6sp2skkb1ljd9ilzvyfizi5s1rl5ickz40")))

