(define-module (crates-io b3 #{9w}# b39wc) #:use-module (crates-io))

(define-public crate-b39wc-1.0.0 (c (n "b39wc") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colour") (r "^0.6.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "microprofile") (r "^0.0.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0wp3hlnvagps0i8w89nny1hdsxypm3qybnb8l67mwd1djv67kbsz")))

