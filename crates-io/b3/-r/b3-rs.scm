(define-module (crates-io b3 -r b3-rs) #:use-module (crates-io))

(define-public crate-b3-rs-0.0.0 (c (n "b3-rs") (v "0.0.0") (h "15nmx0lzmv9wpbk48lfm496lbirv8hrik9zr8z3cdbg09a4hmk3a") (f (quote (("std") ("default" "std"))))))

(define-public crate-b3-rs-0.1.0-alpha.1 (c (n "b3-rs") (v "0.1.0-alpha.1") (d (list (d (n "nano-leb128") (r "^0.1.0") (k 0)))) (h "1afsd2ffv2hij10v0h4mmlp0j3hl8gxrq0snip0i26ifn7kxlzqx") (f (quote (("std" "nano-leb128/std") ("default" "std"))))))

(define-public crate-b3-rs-0.1.0-alpha.2 (c (n "b3-rs") (v "0.1.0-alpha.2") (d (list (d (n "nano-leb128") (r "^0.1.0") (k 0)))) (h "04qmgxfxqvw5csdciad6f5dgvz4ih19x79bxh4p0ina8anz8ziaa") (f (quote (("std" "nano-leb128/std") ("default" "std"))))))

(define-public crate-b3-rs-0.1.0 (c (n "b3-rs") (v "0.1.0") (d (list (d (n "nano-leb128") (r "^0.1.0") (k 0)))) (h "0z3q7h06z6hzb4zwbgpwmvwijq30aj7s2c781zjvanvwyfrh6ir4") (f (quote (("std" "nano-leb128/std") ("default" "std"))))))

