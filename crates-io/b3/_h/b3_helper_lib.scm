(define-module (crates-io b3 _h b3_helper_lib) #:use-module (crates-io))

(define-public crate-b3_helper_lib-0.0.1 (c (n "b3_helper_lib") (v "0.0.1") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "easy-hasher") (r "^2.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 0)))) (h "0w3gwlhiigs7izj0h2jaq6i55qf1gvq542fa9hzbpjv9155snry7")))

(define-public crate-b3_helper_lib-0.0.2 (c (n "b3_helper_lib") (v "0.0.2") (d (list (d (n "candid") (r "^0.9.1") (d #t) (k 0)) (d (n "easy-hasher") (r "^2.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.12") (d #t) (k 0)))) (h "0dkl08f8kqdhldcqwxpz6y03dr1yd963q2a3v0jk9xg0ag0l39r2")))

(define-public crate-b3_helper_lib-0.0.3 (c (n "b3_helper_lib") (v "0.0.3") (d (list (d (n "candid") (r "^0.9.1") (d #t) (k 0)) (d (n "easy-hasher") (r "^2.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.12") (d #t) (k 0)))) (h "19z6fraj57rr9xwiz7sx2a3rl0zlzlr3irb4am54fzpkmd7ppc9i")))

(define-public crate-b3_helper_lib-0.0.4 (c (n "b3_helper_lib") (v "0.0.4") (d (list (d (n "candid") (r "^0.9.1") (d #t) (k 0)) (d (n "easy-hasher") (r "^2.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.12") (d #t) (k 0)))) (h "0v7ivw3blwqby8d3vak3bbjas1af74r4pxr1rw86966bzylr9vyc")))

(define-public crate-b3_helper_lib-0.0.5 (c (n "b3_helper_lib") (v "0.0.5") (d (list (d (n "candid") (r "^0.9.1") (d #t) (k 0)) (d (n "easy-hasher") (r "^2.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.12") (d #t) (k 0)))) (h "1by9ziy1grwn8jbvmdn100916kg2868ccp6596mvbdrhwy6brwk4")))

