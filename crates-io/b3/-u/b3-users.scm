(define-module (crates-io b3 -u b3-users) #:use-module (crates-io))

(define-public crate-b3-users-0.1.0 (c (n "b3-users") (v "0.1.0") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.4") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.6.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1wnjxxvyl66bjv6vrz2979wsrrcx2n84pp6hgriw4qf2ks17v78c")))

(define-public crate-b3-users-0.1.1 (c (n "b3-users") (v "0.1.1") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.4") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.6.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "13wjfdv7qj27d18a0xmv6ms8cylzmbh2hkv7f1pawjd6yqrm3v70")))

(define-public crate-b3-users-0.1.2 (c (n "b3-users") (v "0.1.2") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.4") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.6.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0wxhxlh4cyqhn8ibqd2ackcdg7fsrg1sbya10b5vbxirf4r8il0l")))

(define-public crate-b3-users-0.1.3 (c (n "b3-users") (v "0.1.3") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.4") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.6.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "16rzqypnpig5ak62ilgzhlsl5vskyhl21wl4nlkp0hn6nqswf1mh")))

(define-public crate-b3-users-0.1.4 (c (n "b3-users") (v "0.1.4") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.4") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.6.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0xa7yksy65fci1z3bsnd51x1hi3zn0hwi4hqp13v5r1q150dzisf")))

