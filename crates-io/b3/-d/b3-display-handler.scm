(define-module (crates-io b3 -d b3-display-handler) #:use-module (crates-io))

(define-public crate-b3-display-handler-0.1.0 (c (n "b3-display-handler") (v "0.1.0") (h "1a3jikjajhdgsc2sbddfc25xw3qywn78rz44f7xcvynfbplfi67f")))

(define-public crate-b3-display-handler-0.1.1 (c (n "b3-display-handler") (v "0.1.1") (h "1x5lpid7liqsm4iyg0kqlhik4q737vbr6x0zj7yaqii3as51m7ml")))

(define-public crate-b3-display-handler-0.1.2 (c (n "b3-display-handler") (v "0.1.2") (h "0ig3fq9kwzfaxn6zjwsi8f36ddmbs2i34n31yqlac8x0qvppkwkb")))

