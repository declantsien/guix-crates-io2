(define-module (crates-io sf sd sfsdb) #:use-module (crates-io))

(define-public crate-sfsdb-0.1.0 (c (n "sfsdb") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)))) (h "13xfm6k8pvn98abc09g5kk0iabcn1q3qpg2lay5jjq5krv4v4hf5")))

