(define-module (crates-io sf s- sfs-core) #:use-module (crates-io))

(define-public crate-sfs-core-0.1.0 (c (n "sfs-core") (v "0.1.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "noodles-bcf") (r "^0.32") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.22") (d #t) (k 0)) (d (n "noodles-vcf") (r "^0.35") (d #t) (k 0)))) (h "0fc343rdjs52r1lq43pb9m886l7lcvnfb7nawj470nxl31bh3k4s") (r "1.70")))

