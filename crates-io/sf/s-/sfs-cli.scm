(define-module (crates-io sf s- sfs-cli) #:use-module (crates-io))

(define-public crate-sfs-cli-0.1.0 (c (n "sfs-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sfs-core") (r "^0.1") (d #t) (k 0)) (d (n "trycmd") (r "^0.14") (d #t) (k 2)))) (h "1nv0m6c10byp915bmkq2qk66k789mzhi3a7hgnf6w7aaqnq87qba") (r "1.70")))

