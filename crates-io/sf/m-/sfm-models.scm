(define-module (crates-io sf m- sfm-models) #:use-module (crates-io))

(define-public crate-sfm-models-0.0.1 (c (n "sfm-models") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "=1.9.13") (d #t) (k 0)))) (h "155q81adhpaxflair322pza90f03w7bk2b6vrmavwxp2cg103n1i")))

(define-public crate-sfm-models-0.0.2 (c (n "sfm-models") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.8") (d #t) (k 0)))) (h "0mi35zc0xf9vi0a6wb67gwg88bmchv4wn3958gvr1mwya5im9zng")))

(define-public crate-sfm-models-0.0.3 (c (n "sfm-models") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "signal" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0bvkpqafxf4fnyg1xxc2dhag2v3f0krb4gphfqbgrjv112w6clxs")))

(define-public crate-sfm-models-0.0.4 (c (n "sfm-models") (v "0.0.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "signal" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0anz3y5yxidg41lk11iww684h8ss9bg47gk9mbxqapk6kalcslik")))

(define-public crate-sfm-models-0.0.5 (c (n "sfm-models") (v "0.0.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "signal" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0gmgh4b9gd7rq1jw2wq39d2jrpspgcnsa3flygsrcdb0z8snr6j5")))

(define-public crate-sfm-models-0.0.6 (c (n "sfm-models") (v "0.0.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "signal" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1z0yjh5kkv935ldvidbi79n9rmc4vdwy0rs3d9w6a6gdh9vn0ywq")))

(define-public crate-sfm-models-0.0.7 (c (n "sfm-models") (v "0.0.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "signal" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0wbkxwhl6askc0d7vhaqp5mf266smljwwkcl7lm6dmpzaiz4ljxs")))

(define-public crate-sfm-models-0.0.8 (c (n "sfm-models") (v "0.0.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "signal" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "14m13dj3bf9rdm1hinnf5nmjrqyxras0cj4dpfigx29kx5i7r4gw") (y #t)))

(define-public crate-sfm-models-0.0.8-1 (c (n "sfm-models") (v "0.0.8-1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "signal" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1mmm0vrwwvi3wib4ks98my3sz2hs1j018p4mf3pl18826ppmagsm")))

(define-public crate-sfm-models-0.0.9 (c (n "sfm-models") (v "0.0.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "signal" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1dsilzj3asxk2wfgx9i7b6r2bd9x97vx01ka29idnivcv4759cp8")))

(define-public crate-sfm-models-0.0.10 (c (n "sfm-models") (v "0.0.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "signal" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0w915gxm176xx4wc3c0vdm7qbcc75zqwbgbnn5m5sf3x7x9jmd1x")))

(define-public crate-sfm-models-0.0.11 (c (n "sfm-models") (v "0.0.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "signal" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1i6hy1m04zv7q8va0xnbr1wmy46fwjvcv8w8jiyznxsj21vh7hpr")))

(define-public crate-sfm-models-0.0.12 (c (n "sfm-models") (v "0.0.12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "signal" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "10kb33sm8jbzzzlrnjivc47xs96lzfjbyaxmbrjsqqpfyy56gnfw")))

(define-public crate-sfm-models-0.1.0 (c (n "sfm-models") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0y24wz97s74v51zxf8ka4b3d9fcb3243i35z90xys1ljxqjzh2v9")))

(define-public crate-sfm-models-0.1.1 (c (n "sfm-models") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0x4sjzsgqim2c5ncxwvhx5cc75qz0mli32w582sxgvqcs3ij8mj5")))

(define-public crate-sfm-models-0.1.2 (c (n "sfm-models") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "184gpcwjgix1np93w83lmnnh8zgidi8aqfca4gld15pz61disriq")))

