(define-module (crates-io sf sh sfshr) #:use-module (crates-io))

(define-public crate-sfshr-0.1.0 (c (n "sfshr") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simpletcp") (r "^0.5.1") (d #t) (k 0)))) (h "03nwznbdilz9f5g8d54vwcks6v2n9ncnbx5f8z843wykclxn9x1m")))

(define-public crate-sfshr-1.0.0 (c (n "sfshr") (v "1.0.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simpletcp") (r "^1.0.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "049hf5d9cxs47ac0z37j1hri0hcxd31s1k7a2vzmwd1dmd9xrn1j")))

(define-public crate-sfshr-1.1.0 (c (n "sfshr") (v "1.1.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simpletcp") (r "^1.2.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "01k3hxll7aibgkkfd1qhynig4zl1cxpd1xxyb48ln93vwhvk38y1")))

