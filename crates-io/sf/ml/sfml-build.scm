(define-module (crates-io sf ml sfml-build) #:use-module (crates-io))

(define-public crate-sfml-build-0.1.0 (c (n "sfml-build") (v "0.1.0") (h "0w7r5fs25mr2s3z4525dkd2qq4hyy7sb4cmjgwqsk7lnmv2vsxfv")))

(define-public crate-sfml-build-0.2.0 (c (n "sfml-build") (v "0.2.0") (h "0hgqihfggc54imrmgl3z6563iv4a389hwkqvgnbbr5gz78bswdgz")))

(define-public crate-sfml-build-0.3.0 (c (n "sfml-build") (v "0.3.0") (h "0ldi8gmg3w46wp5aib1ws9873ynlrgxapchr60kwjybwgyns30sg")))

(define-public crate-sfml-build-0.4.0 (c (n "sfml-build") (v "0.4.0") (h "12y96sy46sgx06vd1q8npv2daxw34l3p1c82fbi3sn0qmwx8kq2k")))

