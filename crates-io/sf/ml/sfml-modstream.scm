(define-module (crates-io sf ml sfml-modstream) #:use-module (crates-io))

(define-public crate-sfml-modstream-0.1.0 (c (n "sfml-modstream") (v "0.1.0") (d (list (d (n "openmpt-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.12") (d #t) (k 0)))) (h "08j4mq41l0blnfs07z8i77lzsdxr6fl59n08f9gb4iv6r4lh4c0y")))

(define-public crate-sfml-modstream-0.1.1 (c (n "sfml-modstream") (v "0.1.1") (d (list (d (n "openmpt-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.12") (f (quote ("audio"))) (k 0)))) (h "07bpnwk5k3shc64zv72cf6lp2k84k4mij7qy1i5r8ir8r32800d3")))

(define-public crate-sfml-modstream-0.2.0 (c (n "sfml-modstream") (v "0.2.0") (d (list (d (n "openmpt-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.16.0") (f (quote ("audio"))) (k 0)))) (h "134l2n7sp4cnkggacmra27g8iw133bwnb4j0kap4f6pi3gmrggl9")))

(define-public crate-sfml-modstream-0.3.0 (c (n "sfml-modstream") (v "0.3.0") (d (list (d (n "openmpt-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "sfml") (r "^0.18.1") (f (quote ("audio"))) (k 0)))) (h "0vgaz8m8n9y0fzpxc2czaxyi87ckmx1q3k8vq7jiqalgakpjp0w2")))

