(define-module (crates-io sf n- sfn-machine) #:use-module (crates-io))

(define-public crate-sfn-machine-0.1.0 (c (n "sfn-machine") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "199230sgfjj4ax5k8v5gggkjlmi3i6258y1vs94raqcvsqxi9wnx") (r "1.74")))

(define-public crate-sfn-machine-0.1.1 (c (n "sfn-machine") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1wza455i6w4wwnbs9vwvf5kyf3hcrq3q7h2v121x4lvrvclh8vjb") (r "1.74")))

(define-public crate-sfn-machine-0.1.3 (c (n "sfn-machine") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1n75hcdkybc52hpj27aax5d7fidpasqrd5bpdcwyf5j1jd6vfq38") (r "1.74")))

