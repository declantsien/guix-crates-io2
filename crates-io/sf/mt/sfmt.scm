(define-module (crates-io sf mt sfmt) #:use-module (crates-io))

(define-public crate-sfmt-0.1.0 (c (n "sfmt") (v "0.1.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "sfmt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "06q2bmail7m577h8s1svgi5cpwhq6cllvfdg8a6ql0kqyna5hf4r")))

(define-public crate-sfmt-0.2.0 (c (n "sfmt") (v "0.2.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "03z93lbgqwrrrkr51idai715hkay7759pa8jgxr55a30fflr7zlb")))

(define-public crate-sfmt-0.2.1 (c (n "sfmt") (v "0.2.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "05vi0gcqdf1lxl98ff79h9116j0f64xprfwfj8n327hs0y83hd0p")))

(define-public crate-sfmt-0.2.2 (c (n "sfmt") (v "0.2.2") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0fg1jn4vw15cpigxifarlmkcqfgcgiifr44yxkgd1s0gqmqgp4ql")))

(define-public crate-sfmt-0.2.3 (c (n "sfmt") (v "0.2.3") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0d4mniw78c9zqjhvw52icarvc67v946l8k64r5k1kzqkhl9hp6ax")))

(define-public crate-sfmt-0.3.0 (c (n "sfmt") (v "0.3.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.2") (d #t) (k 0)))) (h "1iicqydfmb29azb7mhbm1plb4wvr1gnsx7z5avn8b9k28v00xrdq")))

(define-public crate-sfmt-0.3.1 (c (n "sfmt") (v "0.3.1") (d (list (d (n "packed_simd") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.2") (d #t) (k 0)))) (h "12cvb4c2qnxnwzipcrr2bbc803f5m49y1srksaqm1is0h7rlpl8w")))

(define-public crate-sfmt-0.4.0 (c (n "sfmt") (v "0.4.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.2") (d #t) (k 0)))) (h "04s4v8i5xiq78gcvknngdydlkxal0yrv8a4rhwngljab930iqaxg")))

(define-public crate-sfmt-0.5.0 (c (n "sfmt") (v "0.5.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_core") (r "^0.3") (d #t) (k 0)))) (h "0s0irnin4xw7h52nm0ajz87zqx0b1bf4ldznfmpsdhylxv8nhyli")))

(define-public crate-sfmt-0.6.0 (c (n "sfmt") (v "0.6.0") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "02g39qfj8xf0nyjjwy942b79a2qbn1hd0y81vkak74g1536kk4i6") (f (quote (("thread_rng" "rand") ("default" "thread_rng"))))))

(define-public crate-sfmt-0.7.0 (c (n "sfmt") (v "0.7.0") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)))) (h "0rbmgk9r13j5g0i38lj2g1h9y5mpvy9djz3629bghf34ag0s72g3") (f (quote (("thread_rng" "rand") ("default" "thread_rng"))))))

