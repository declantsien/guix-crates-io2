(define-module (crates-io sf kv sfkv) #:use-module (crates-io))

(define-public crate-sfkv-0.1.0 (c (n "sfkv") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "postcard") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1ap8p7f5lmbwmmcmnri1vhsls7s7map83vipdw5c39v67ynbzx95") (f (quote (("postcard-values" "postcard" "serde") ("default" "postcard-values"))))))

