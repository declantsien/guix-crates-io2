(define-module (crates-io sf ha sfhash) #:use-module (crates-io))

(define-public crate-sfhash-0.1.0 (c (n "sfhash") (v "0.1.0") (h "127y1717xg03q85ipvfmnxw8jh8frqga3vqkc97rjvdbz5qd8qa9")))

(define-public crate-sfhash-0.1.1 (c (n "sfhash") (v "0.1.1") (h "16n1c74ih4jj0217qrwpkjgh1xbkmsxnaw2spm5aq08w6kfij867")))

