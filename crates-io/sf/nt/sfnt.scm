(define-module (crates-io sf nt sfnt) #:use-module (crates-io))

(define-public crate-sfnt-0.1.0 (c (n "sfnt") (v "0.1.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "microbench") (r "^0.3.2") (d #t) (k 2)) (d (n "tarrasque") (r "^0.1.0") (d #t) (k 0)))) (h "0qidbqkh81q6wcvfi0fwza9qcpg8cqj6xmpj4mwym3kvyhvkzq7i")))

(define-public crate-sfnt-0.2.0 (c (n "sfnt") (v "0.2.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "microbench") (r "^0.3.2") (d #t) (k 2)) (d (n "tarrasque") (r "^0.2.0") (d #t) (k 0)))) (h "10z6cbn55i2zhcw24vraccjs5r66l1g6vafsywg33gma2q9n3xc0")))

(define-public crate-sfnt-0.3.0 (c (n "sfnt") (v "0.3.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "microbench") (r "^0.3.2") (d #t) (k 2)) (d (n "tarrasque") (r "^0.3.0") (d #t) (k 0)))) (h "1c9wgagvs3yancjwcbypqgyvbjj26rr3zl8mab3hb8xzk2gv9xfl")))

(define-public crate-sfnt-0.4.0 (c (n "sfnt") (v "0.4.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "microbench") (r "^0.3.2") (d #t) (k 2)) (d (n "tarrasque") (r "^0.3.0") (d #t) (k 0)))) (h "1wqh2g6apir9jz4qzjc85ad3wghmc3j0dgfamwcbxlfynzywq4ga")))

(define-public crate-sfnt-0.5.0 (c (n "sfnt") (v "0.5.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "microbench") (r "^0.3.2") (d #t) (k 2)) (d (n "tarrasque") (r "^0.4.0") (d #t) (k 0)))) (h "11976p0rkwmk7j9s0yna20y93l7d8g9idsmws50xs6gn1lp3l282")))

(define-public crate-sfnt-0.6.0 (c (n "sfnt") (v "0.6.0") (d (list (d (n "microbench") (r "^0.3.2") (d #t) (k 2)) (d (n "tarrasque") (r "^0.4.0") (d #t) (k 0)))) (h "0rs4xhi7qwxcldm6lxxszssxd2cl3b9mr5kpwy544frhpzhfid2a")))

(define-public crate-sfnt-0.7.0 (c (n "sfnt") (v "0.7.0") (d (list (d (n "microbench") (r "^0.3.2") (d #t) (k 2)) (d (n "tarrasque") (r "^0.5.0") (d #t) (k 0)))) (h "0k02rhjqfqf5m1ih2r1hbyjcqyzy9i4l3hdahh54kb92dbpd9vrd")))

(define-public crate-sfnt-0.8.0 (c (n "sfnt") (v "0.8.0") (d (list (d (n "microbench") (r "^0.3") (d #t) (k 2)) (d (n "tarrasque") (r "^0.6") (d #t) (k 0)) (d (n "tarrasque-macro") (r "^0.6") (d #t) (k 0)))) (h "0hrjlvz0xmbzjy8cr96ax80xyjj3kjim4vnmr7mrx08m47dgp0mn")))

(define-public crate-sfnt-0.8.1 (c (n "sfnt") (v "0.8.1") (d (list (d (n "microbench") (r "^0.3") (d #t) (k 2)) (d (n "tarrasque") (r "^0.7") (d #t) (k 0)))) (h "1rrpmplddanzfly9p6jzxd1z21a9clsh7009z6grlrsqr9zmw86h")))

(define-public crate-sfnt-0.9.0 (c (n "sfnt") (v "0.9.0") (d (list (d (n "microbench") (r "^0.3") (d #t) (k 2)) (d (n "tarrasque") (r "^0.8") (d #t) (k 0)))) (h "0drlvzvzgzkrlwkmjlzrzi5ggdhm2pkn9bk4k7rcxqiyxkc9443b")))

(define-public crate-sfnt-0.10.0 (c (n "sfnt") (v "0.10.0") (d (list (d (n "microbench") (r "^0.3") (d #t) (k 2)) (d (n "tarrasque") (r "^0.9") (d #t) (k 0)))) (h "11375szz2irssplw1kkjz8wsz8457p0d9dfxpcv8k6cfbqflz0a8")))

(define-public crate-sfnt-0.11.0 (c (n "sfnt") (v "0.11.0") (d (list (d (n "microbench") (r "^0.3") (d #t) (k 2)) (d (n "tarrasque") (r "^0.9") (d #t) (k 0)))) (h "0xx4jm8dngvq1aqz0z9c23jq32f01g1ajqa8j7qba0gpkxsqwcgw")))

(define-public crate-sfnt-0.12.0 (c (n "sfnt") (v "0.12.0") (d (list (d (n "microbench") (r "^0.3") (d #t) (k 2)) (d (n "tarrasque") (r "^0.10") (d #t) (k 0)))) (h "0y41ymabz0wihmjsxdhcgzbpf2i67q48675pfjkfy9hr2iirbgx4")))

