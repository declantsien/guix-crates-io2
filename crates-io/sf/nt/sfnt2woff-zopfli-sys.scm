(define-module (crates-io sf nt sfnt2woff-zopfli-sys) #:use-module (crates-io))

(define-public crate-sfnt2woff-zopfli-sys-0.0.1 (c (n "sfnt2woff-zopfli-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x2agncm6q6k4rj5rk5m2rs1708avm9qs4cmf3vn63yn8x828vv8")))

