(define-module (crates-io sf in sfink_callgraph_traversal) #:use-module (crates-io))

(define-public crate-sfink_callgraph_traversal-0.1.0 (c (n "sfink_callgraph_traversal") (v "0.1.0") (d (list (d (n "cpython") (r "^0.1") (f (quote ("python27-sys" "extension-module-2-7"))) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "rustyline") (r "^4.0") (d #t) (k 0)))) (h "11pg5x6nqqsriwfrvbj654iqazs9rr6zsr9skrw0bybi02a71m7m")))

(define-public crate-sfink_callgraph_traversal-0.2.0 (c (n "sfink_callgraph_traversal") (v "0.2.0") (d (list (d (n "cpython") (r "^0.1") (f (quote ("python27-sys" "extension-module-2-7"))) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0") (d #t) (k 0)))) (h "00gq9lfpvk9ww5j5jd8k03yp7gqkyh4havbpafcgbffmv6g80gnz")))

(define-public crate-sfink_callgraph_traversal-0.2.1 (c (n "sfink_callgraph_traversal") (v "0.2.1") (d (list (d (n "cpython") (r "^0.1") (f (quote ("python27-sys" "extension-module-2-7"))) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0") (d #t) (k 0)))) (h "102hc3qn8ksnr3q6k95yfwc97vpjabigvzf15758hd6lg8sjfik0")))

