(define-module (crates-io sf rm sfrm) #:use-module (crates-io))

(define-public crate-sfrm-0.1.0 (c (n "sfrm") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "trash") (r "^2.0.4") (d #t) (k 0)))) (h "08hgx5snyl5wwb4p66y8c5sblblyin3ppg1i5mhslqaqh3v5l21i")))

