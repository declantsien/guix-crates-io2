(define-module (crates-io sf rx sfrx) #:use-module (crates-io))

(define-public crate-sfrx-0.1.0 (c (n "sfrx") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "100868gh1v9vdxgr84fb04bghxil3ik4d3cxxv24327fqcrcgf62")))

(define-public crate-sfrx-0.1.1 (c (n "sfrx") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i5fl0l3ck3z96k24aiyvc69444az4lgm9nriirkd0whcgladpnp")))

(define-public crate-sfrx-0.1.2 (c (n "sfrx") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ziszwq0bzkxdkd6nv6173dhg9j65jwivgp5s9nq2zsdhpzd7whr")))

(define-public crate-sfrx-0.1.3 (c (n "sfrx") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aa8kn6l1jzg5iq0yncvg6prs4wqqzv6i1hi2dqf8414mh5jwvrr")))

