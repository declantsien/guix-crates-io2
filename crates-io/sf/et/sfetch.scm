(define-module (crates-io sf et sfetch) #:use-module (crates-io))

(define-public crate-sfetch-1.0.0 (c (n "sfetch") (v "1.0.0") (h "0c3z1xrkmr36af4cs998b82zn0962lhdhmlmbcqxgf28h5izrlkw")))

(define-public crate-sfetch-1.1.0 (c (n "sfetch") (v "1.1.0") (h "0mb6x00c13l546mfks29yn85igf27862imadfgihl4jwfy8wm0l2")))

(define-public crate-sfetch-1.1.1 (c (n "sfetch") (v "1.1.1") (h "1hl2m3imfnvhbpdm4gqan54nh3730hcd0ppqfj7wq7x2fms6lipw")))

(define-public crate-sfetch-1.2.0 (c (n "sfetch") (v "1.2.0") (h "0lff2lig8ny15vrxwbaij08ws51rwqnzfpy9r8l5k5cjkz06j61z")))

(define-public crate-sfetch-1.2.1 (c (n "sfetch") (v "1.2.1") (h "1l7ycfa9wxjm8w22azd6jgpdimj0hdlh35fgswcff0zfgs1s62lq")))

(define-public crate-sfetch-1.2.2 (c (n "sfetch") (v "1.2.2") (h "1fw8hifx7h4cy6yh2kmqvqnvbyp7d58cfz78prcfxz9rykvmlrhn")))

(define-public crate-sfetch-1.2.3 (c (n "sfetch") (v "1.2.3") (h "0dfhh06cilknj5i09iij2jlzqs81nzjfxwfrd7ar2m758ckncimz")))

(define-public crate-sfetch-1.3.0 (c (n "sfetch") (v "1.3.0") (h "1qd2f7x2kwbkck5lbx7kcb44kri0m5jr4vw2w7nsqhzqfayl4xf2")))

(define-public crate-sfetch-1.3.1 (c (n "sfetch") (v "1.3.1") (h "0z36li17bpz6d6g249mwr9092q1pj5dm1dfpcakkfp9yq46jyx92")))

