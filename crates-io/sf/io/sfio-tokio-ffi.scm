(define-module (crates-io sf io sfio-tokio-ffi) #:use-module (crates-io))

(define-public crate-sfio-tokio-ffi-0.5.0 (c (n "sfio-tokio-ffi") (v "0.5.0") (d (list (d (n "oo-bindgen") (r "^0.6.0") (d #t) (k 0)))) (h "0aa5nhs2nx6m9w9xbwqsz1xh14g254249v7pdqcrzq6siiikkymj")))

(define-public crate-sfio-tokio-ffi-0.5.1 (c (n "sfio-tokio-ffi") (v "0.5.1") (d (list (d (n "oo-bindgen") (r "^0.6.0") (d #t) (k 0)))) (h "142dxaw5jrycy98ax4d6f8khhq1acqmblxx0c9hsq04rxvn1s0dk")))

(define-public crate-sfio-tokio-ffi-0.6.0 (c (n "sfio-tokio-ffi") (v "0.6.0") (d (list (d (n "oo-bindgen") (r "^0.6.0") (d #t) (k 0)))) (h "08zxvgfr1spdhnkc6xc0x2zzmlw05hrjkcm6h6w3a5ln5wzqzjcj")))

(define-public crate-sfio-tokio-ffi-0.7.0 (c (n "sfio-tokio-ffi") (v "0.7.0") (d (list (d (n "oo-bindgen") (r "^0.7") (d #t) (k 0)))) (h "06ig3chwsvrqnfsjsd79nsmknfwf8s6ibbg5imf42g6nc28045ja")))

(define-public crate-sfio-tokio-ffi-0.8.0 (c (n "sfio-tokio-ffi") (v "0.8.0") (d (list (d (n "oo-bindgen") (r "^0.8") (d #t) (k 0)))) (h "11i2mly2bs2lnfk35yqx3n0vvvaxjvww31cvfjgdg89g1wsa2r3j")))

(define-public crate-sfio-tokio-ffi-0.9.0 (c (n "sfio-tokio-ffi") (v "0.9.0") (d (list (d (n "oo-bindgen") (r "^0.8") (d #t) (k 0)))) (h "1jzyzqmys2nl56c3i4sbhdvgdqahq16v66iy1dy058i5wn7433gi")))

