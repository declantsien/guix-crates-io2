(define-module (crates-io sf io sfio-tokio-mock-io) #:use-module (crates-io))

(define-public crate-sfio-tokio-mock-io-0.2.0 (c (n "sfio-tokio-mock-io") (v "0.2.0") (d (list (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "11i56fnha1h3b1ds9nin9gd2whqh3ihd7p6rzygf3rk0f5mgm2r5")))

