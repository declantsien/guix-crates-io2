(define-module (crates-io sf io sfio-tracing-ffi) #:use-module (crates-io))

(define-public crate-sfio-tracing-ffi-0.5.0 (c (n "sfio-tracing-ffi") (v "0.5.0") (d (list (d (n "oo-bindgen") (r "^0.6") (d #t) (k 0)))) (h "0ky2dbfysnj7kfi0v3d5nx067b3mm96x9w46pj72wzrxq3mjnn2f")))

(define-public crate-sfio-tracing-ffi-0.7.0 (c (n "sfio-tracing-ffi") (v "0.7.0") (d (list (d (n "oo-bindgen") (r "^0.7") (d #t) (k 0)))) (h "09px68xqdf0mp6kg5jgq3pd3vwnb23599n5jvdfmfn95522dqibr")))

(define-public crate-sfio-tracing-ffi-0.8.0 (c (n "sfio-tracing-ffi") (v "0.8.0") (d (list (d (n "oo-bindgen") (r "^0.8") (d #t) (k 0)))) (h "1d78icx1g3xnyj9r74w150qzc19cvf0jb1ky54r6yfs1vg15m5cl")))

(define-public crate-sfio-tracing-ffi-0.8.1 (c (n "sfio-tracing-ffi") (v "0.8.1") (d (list (d (n "oo-bindgen") (r "^0.8") (d #t) (k 0)))) (h "0p6gj7kk8b2ysc688wqxrvl473w1cwqjr25qfj72jyra9k42kgp1")))

(define-public crate-sfio-tracing-ffi-0.9.0 (c (n "sfio-tracing-ffi") (v "0.9.0") (d (list (d (n "oo-bindgen") (r "^0.8") (d #t) (k 0)))) (h "0dggmn78q640zxxxxyl3pmh39frz0vyw15d3yj3gw3lwrkkmgnca")))

