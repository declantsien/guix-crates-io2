(define-module (crates-io sf io sfio-promise) #:use-module (crates-io))

(define-public crate-sfio-promise-0.1.0 (c (n "sfio-promise") (v "0.1.0") (h "1wn4yh7jqj3zpi4xwgp9nxg9w54hvhk95x6l42lrxi5hqyr36xkc")))

(define-public crate-sfio-promise-0.2.0 (c (n "sfio-promise") (v "0.2.0") (h "1bwsqy6b84ihprn7bchqv3cv9g2ba9qsbkx705j1lmf1rbjpnm4i")))

