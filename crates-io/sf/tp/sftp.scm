(define-module (crates-io sf tp sftp) #:use-module (crates-io))

(define-public crate-sftp-0.1.0 (c (n "sftp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "03a4lx1n5y5b1g2ldiyjkg0hwq22hhslxphzwr60fcahk1qkmn6d")))

(define-public crate-sftp-0.2.0 (c (n "sftp") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "112zi9mn8jwmq9bs3kql147l25flryjmczn7l6wxw8i56ys8wki5")))

(define-public crate-sftp-0.2.1 (c (n "sftp") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "02j0k6hb3jhhwvzvaahafjssg01fp9w1sv54965cv48ii1hmflf7")))

(define-public crate-sftp-0.2.2 (c (n "sftp") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0i4p9vv15lpnmz0i34j3csjs8fzdmpljrx6xbi0g8sx00n7gzwlb")))

