(define-module (crates-io sf o- sfo-result) #:use-module (crates-io))

(define-public crate-sfo-result-0.1.0 (c (n "sfo-result") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (o #t) (d #t) (k 0)))) (h "0al9xh88f5scp1xr3haszj98s1vaf1kh0dwjc55jysss3dn4rvp1") (f (quote (("backtrace" "anyhow/backtrace"))))))

(define-public crate-sfo-result-0.1.1 (c (n "sfo-result") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l4l5c416iqv0l7yk2qci2m7ipkk8x9dmh56lin3jl5aa3r8s35c") (f (quote (("backtrace" "anyhow/backtrace"))))))

(define-public crate-sfo-result-0.2.0 (c (n "sfo-result") (v "0.2.0") (d (list (d (n "log") (r "^0.4.21") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07g0dih0l4m4cdqjcliq3wkgnvn8qrzjc29hqn1i7jpx9idvnrk6") (f (quote (("backtrace"))))))

(define-public crate-sfo-result-0.2.1 (c (n "sfo-result") (v "0.2.1") (d (list (d (n "log") (r "^0.4.21") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "174y0iqp219wvp4mgmdqd21l8pjvj321nl53swmcw8nikh8lixgc") (f (quote (("backtrace"))))))

(define-public crate-sfo-result-0.2.2 (c (n "sfo-result") (v "0.2.2") (d (list (d (n "log") (r "^0.4.21") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1964y6yd5cgkj6h5xpn96l3mj8g3fd1z3b5zs07d7khcdsfp79ka") (f (quote (("backtrace"))))))

(define-public crate-sfo-result-0.2.3 (c (n "sfo-result") (v "0.2.3") (d (list (d (n "log") (r "^0.4.21") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hqi49k9d8pn2mbqn4xplz4jr3fag69dbfihqdill9l61yq93kqz") (f (quote (("backtrace"))))))

(define-public crate-sfo-result-0.2.4 (c (n "sfo-result") (v "0.2.4") (d (list (d (n "log") (r "^0.4.21") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0h9872hx3bjwqbszbb0yd84l1v3jk1lhn3sj2z8rbv1gfa0vf4xk") (f (quote (("backtrace"))))))

