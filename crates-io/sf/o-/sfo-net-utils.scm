(define-module (crates-io sf o- sfo-net-utils) #:use-module (crates-io))

(define-public crate-sfo-net-utils-0.1.0 (c (n "sfo-net-utils") (v "0.1.0") (d (list (d (n "c_linked_list") (r "^1.1.1") (d #t) (k 0)) (d (n "ipconfig") (r "^0.3.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("psapi" "shellapi" "mswsock" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0f4pvfbbvmlh49fhnz9vkd67bkbd7nfqkrjhfgn20cnk5qg5rci0")))

(define-public crate-sfo-net-utils-0.1.1 (c (n "sfo-net-utils") (v "0.1.1") (d (list (d (n "c_linked_list") (r "^1.1.1") (d #t) (k 0)) (d (n "ipconfig") (r "^0.3.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("psapi" "shellapi" "mswsock" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lxc3j6m203n13ayg5f2q0xwrq3s4p46sh2a6w67arc3d7vcl48h")))

