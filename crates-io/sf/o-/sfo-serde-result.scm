(define-module (crates-io sf o- sfo-serde-result) #:use-module (crates-io))

(define-public crate-sfo-serde-result-0.1.0 (c (n "sfo-serde-result") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sfo-result") (r "^0.1") (d #t) (k 0)))) (h "1ynjhnpsl7hmq7psc30yimk40zvsndgwlg9xr3f9q1qrc1nnirz9")))

(define-public crate-sfo-serde-result-0.1.1 (c (n "sfo-serde-result") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sfo-result") (r "^0.1") (d #t) (k 0)))) (h "07nsb53rlrsak83m4i96ypg3qkc27wygi8fcraq6yhw4x3isj5gl")))

(define-public crate-sfo-serde-result-0.1.2 (c (n "sfo-serde-result") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sfo-result") (r "^0.1.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1mhf916l8rhf7irzn4pdny7p4l0x8qxw441yzyf6k67xv7wlsjba")))

(define-public crate-sfo-serde-result-0.1.3 (c (n "sfo-serde-result") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sfo-result") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1qdmfkavccs36h4kc5xdpbq9c6hcj5cicbbj23lx1z1fpk8hkrcz")))

(define-public crate-sfo-serde-result-0.1.4 (c (n "sfo-serde-result") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sfo-result") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0d1ab6pw0slsq1bjzp1gb22fw0vim7bxvwcxzfihbggiqf5v9wml")))

(define-public crate-sfo-serde-result-0.2.0 (c (n "sfo-serde-result") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cn7b06rlwjskjfls4f97m28x0rz7wpzxaayqds7x7swx2s2c3la")))

