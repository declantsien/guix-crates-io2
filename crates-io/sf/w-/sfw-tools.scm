(define-module (crates-io sf w- sfw-tools) #:use-module (crates-io))

(define-public crate-sfw-tools-0.1.0 (c (n "sfw-tools") (v "0.1.0") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy" "run-cargo-fmt"))) (d #t) (k 2)) (d (n "fp-core") (r "^0.1.9") (d #t) (k 0)) (d (n "seahorse") (r "^1.1.1") (d #t) (k 0)))) (h "0f9dhx87404hpbih242785yv2h2zj6g96qy9f1ydqb8gnnr1y72n")))

(define-public crate-sfw-tools-0.1.1 (c (n "sfw-tools") (v "0.1.1") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy" "run-cargo-fmt"))) (d #t) (k 2)) (d (n "fp-core") (r "^0.1.9") (d #t) (k 0)) (d (n "seahorse") (r "^1.1.1") (d #t) (k 0)))) (h "0c0qiq2qmw3k8hdz75gb6s030das0iq8spyaagl8jc5jq6gfiy9p")))

(define-public crate-sfw-tools-0.2.0 (c (n "sfw-tools") (v "0.2.0") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy" "run-cargo-fmt"))) (d #t) (k 2)) (d (n "fp-core") (r "^0.1.9") (d #t) (k 0)) (d (n "seahorse") (r "^1.1.1") (d #t) (k 0)) (d (n "tailcall") (r "^0.1.6") (d #t) (k 0)))) (h "08q32bmq2984mlf3grbq9mwwn7mabymzl38qk3yx2rm7v5zyacqy")))

(define-public crate-sfw-tools-0.2.1 (c (n "sfw-tools") (v "0.2.1") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy" "run-cargo-fmt"))) (d #t) (k 2)) (d (n "fp-core") (r "^0.1.9") (d #t) (k 0)) (d (n "seahorse") (r "^1.1.1") (d #t) (k 0)) (d (n "tailcall") (r "^0.1.6") (d #t) (k 0)))) (h "1awd484h8fmcqhkabj2pjz6h1fcil0bd5mjfbd5jqqybq069lw85")))

(define-public crate-sfw-tools-0.3.0 (c (n "sfw-tools") (v "0.3.0") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy" "run-cargo-fmt"))) (d #t) (k 2)) (d (n "fp-core") (r "^0.1.9") (d #t) (k 0)) (d (n "seahorse") (r "^1.1.1") (d #t) (k 0)) (d (n "tailcall") (r "^0.1.6") (d #t) (k 0)))) (h "15r2xlz1wbnw0cicv01vc5wd26h0zzsifb90pqnf4fpd64p1gk69")))

(define-public crate-sfw-tools-0.4.0 (c (n "sfw-tools") (v "0.4.0") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy" "run-cargo-fmt"))) (d #t) (k 2)) (d (n "fp-core") (r "^0.1.9") (d #t) (k 0)) (d (n "peeking_take_while") (r "^0.1.2") (d #t) (k 0)) (d (n "seahorse") (r "^1.1.1") (d #t) (k 0)) (d (n "tailcall") (r "^0.1.6") (d #t) (k 0)))) (h "0yyg4r0hanp5ac1sa1nbpikiyj5gli3pwkrbs81bkdq07f81w5wh")))

