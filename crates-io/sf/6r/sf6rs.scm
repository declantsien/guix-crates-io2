(define-module (crates-io sf #{6r}# sf6rs) #:use-module (crates-io))

(define-public crate-sf6rs-0.1.0 (c (n "sf6rs") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "scraper") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01z168j9x7ncghd187p2cnb5867ld3mxhwy2jngbrcapvisdb86b")))

(define-public crate-sf6rs-0.3.0 (c (n "sf6rs") (v "0.3.0") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "scraper") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09df6cgn8f9swh873yz3nrs3l2as5rk9q2ywzg7ca0pvagz7fx1q")))

(define-public crate-sf6rs-0.3.2 (c (n "sf6rs") (v "0.3.2") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "scraper") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f19n2mkagpgqq1xhw5v6i075v2vyqrbgw0v2p520ahyqwyqwj4h")))

(define-public crate-sf6rs-0.3.3 (c (n "sf6rs") (v "0.3.3") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "scraper") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0924hdkc31xx6absp8z5adzz9cxialzhy97f9sg9axbaqsz4mv12")))

