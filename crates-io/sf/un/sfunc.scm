(define-module (crates-io sf un sfunc) #:use-module (crates-io))

(define-public crate-sfunc-0.0.1 (c (n "sfunc") (v "0.0.1") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 0)))) (h "0l1jmwv49r5canmk0rf2l224ldrsfxyn72nhzidg1kxcrgisnibp")))

(define-public crate-sfunc-0.0.2 (c (n "sfunc") (v "0.0.2") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)))) (h "1bv8lszp0diy1nln26s5k04zanivj0vwcnx900r5gnrxpkdqrb9g")))

(define-public crate-sfunc-0.0.3 (c (n "sfunc") (v "0.0.3") (d (list (d (n "assert") (r "^0.0.3") (d #t) (k 2)))) (h "0wi9qz6s9ibjlc71zb3fabvqkdyja3zbbscrazn06fj91qkh4lcd")))

(define-public crate-sfunc-0.0.4 (c (n "sfunc") (v "0.0.4") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)))) (h "0rcjzhfhn1kfd2z6gkfpcx64qn02hdcsrwsbarbv7xm6s02w06v2")))

