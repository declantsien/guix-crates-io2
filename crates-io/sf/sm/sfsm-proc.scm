(define-module (crates-io sf sm sfsm-proc) #:use-module (crates-io))

(define-public crate-sfsm-proc-0.1.0 (c (n "sfsm-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "sfsm-base") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1h3bcqj47gaq7k1xgf4kxh7dbqhgsfb5bmr0qxnipx32gpi6xbfx")))

(define-public crate-sfsm-proc-0.1.1 (c (n "sfsm-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "sfsm-base") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "168idgikdswyhhlpvrz26gljd0z12347f1nqb40fw6rg293crz8c")))

(define-public crate-sfsm-proc-0.2.2 (c (n "sfsm-proc") (v "0.2.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "sfsm-base") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1gn5jmfq4hvv715vmc6b8176l5hqk20j2qs8dhs5n7cy451p577w")))

(define-public crate-sfsm-proc-0.2.3 (c (n "sfsm-proc") (v "0.2.3") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "sfsm-base") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "13pw9j3ilx0qcprgkvqpzs4qirv5brbr7am32jja5rxss957fac5")))

(define-public crate-sfsm-proc-0.2.4 (c (n "sfsm-proc") (v "0.2.4") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0jbrr77z5y45z2vpdj031h40c3hlz8nzrqqyrcwadigd686i7265")))

(define-public crate-sfsm-proc-0.2.6 (c (n "sfsm-proc") (v "0.2.6") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "06i1jc6svc2yggssd9268xbg9y703l7q28rhnqq42z343gx25vyl")))

(define-public crate-sfsm-proc-0.2.8 (c (n "sfsm-proc") (v "0.2.8") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "10zrmxvg2x8h62ka7jcmcb5x7qq9mvqx4ddlchgzk8gkq08crg2v")))

(define-public crate-sfsm-proc-0.3.0 (c (n "sfsm-proc") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "sfsm-base") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1g262wi47xw92fjhx174jy9yyblfk80hibad64f8npc4khb6vxrb")))

(define-public crate-sfsm-proc-0.4.0 (c (n "sfsm-proc") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "sfsm-base") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0a0qc9hybmyvig665z7ffprnck32r2979mbgx8z3w977wwa5a4dj") (f (quote (("trace-steps") ("trace-messages") ("trace"))))))

(define-public crate-sfsm-proc-0.4.1 (c (n "sfsm-proc") (v "0.4.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "sfsm-base") (r "^0.4.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0vcj9d45sq0npy6vx24c34g41qgxvm9py8qg63yc4bwnzazdyz08") (f (quote (("trace-steps") ("trace-messages") ("trace"))))))

(define-public crate-sfsm-proc-0.4.2 (c (n "sfsm-proc") (v "0.4.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "sfsm-base") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1m824plrh7xgmhw0zn8rd8a2h7gqf5iq40h7659p2kp12nszaxv3") (f (quote (("trace-steps") ("trace-messages") ("trace"))))))

(define-public crate-sfsm-proc-0.4.3 (c (n "sfsm-proc") (v "0.4.3") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "sfsm-base") (r "^0.4.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0js7xap7r1rlg55955i83m95xhznvl5vqdqr30lnm4mn91973c3b") (f (quote (("trace-steps") ("trace-messages") ("trace"))))))

