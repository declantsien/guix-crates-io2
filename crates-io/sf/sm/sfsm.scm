(define-module (crates-io sf sm sfsm) #:use-module (crates-io))

(define-public crate-sfsm-0.1.0 (c (n "sfsm") (v "0.1.0") (d (list (d (n "sfsm-base") (r "^0.1.0") (d #t) (k 0)) (d (n "sfsm-proc") (r "^0.1.0") (d #t) (k 0)))) (h "1hfh3i5w47sfcc6ga2rba16kdzpbhy7lp7zjl5nj717n3dq04yfp")))

(define-public crate-sfsm-0.1.1 (c (n "sfsm") (v "0.1.1") (d (list (d (n "sfsm-base") (r "^0.1.1") (d #t) (k 0)) (d (n "sfsm-proc") (r "^0.1.1") (d #t) (k 0)))) (h "0541v53crjrcyxbzfhb471x6l5p6p3c4xshs8axgz7md3l0mm4qy")))

(define-public crate-sfsm-0.2.0 (c (n "sfsm") (v "0.2.0") (d (list (d (n "sfsm-base") (r "^0.1.2") (d #t) (k 0)) (d (n "sfsm-proc") (r "^0.2.2") (d #t) (k 0)))) (h "0mmi7pxhc228p331pifzcd25hzsdn0rafrcjg10cssygkgcqqvdr")))

(define-public crate-sfsm-0.2.1 (c (n "sfsm") (v "0.2.1") (d (list (d (n "sfsm-base") (r "^0.1.2") (d #t) (k 0)) (d (n "sfsm-proc") (r "^0.2.2") (d #t) (k 0)))) (h "0iprq51whjcspywylmzivl8b5j7clgllix8avlkiyl2fx0yz8r3d")))

(define-public crate-sfsm-0.2.2 (c (n "sfsm") (v "0.2.2") (d (list (d (n "sfsm-base") (r "^0.1.2") (d #t) (k 0)) (d (n "sfsm-proc") (r "^0.2.3") (d #t) (k 0)))) (h "15bwnbg8m362v25kz4r4ly55vm0326q4n5gqhscx65lz9bppj68b")))

(define-public crate-sfsm-0.2.3 (c (n "sfsm") (v "0.2.3") (d (list (d (n "sfsm-base") (r "^0.1.2") (d #t) (k 0)) (d (n "sfsm-proc") (r "^0.2.4") (d #t) (k 0)))) (h "0y06ba56slqmrr9cgzncwzm98x7i79v209x68d87s5d5fixafbi3")))

(define-public crate-sfsm-0.2.6 (c (n "sfsm") (v "0.2.6") (d (list (d (n "sfsm-base") (r "^0.1.4") (d #t) (k 0)) (d (n "sfsm-proc") (r "^0.2.6") (d #t) (k 0)))) (h "04ky5pdaspwls4pqdbvrlx5w1dgyxahssnvw5df579r9424607bb")))

(define-public crate-sfsm-0.2.8 (c (n "sfsm") (v "0.2.8") (d (list (d (n "sfsm-base") (r "^0.1.6") (d #t) (k 0)) (d (n "sfsm-proc") (r "^0.2.8") (d #t) (k 0)))) (h "1i4fjr6hhzbhzfd59szxhlw4x70rvkp2shych5dnnm65vhwflc6s")))

(define-public crate-sfsm-0.3.0 (c (n "sfsm") (v "0.3.0") (d (list (d (n "sfsm-base") (r "^0.3.0") (d #t) (k 0)) (d (n "sfsm-proc") (r "^0.3.0") (d #t) (k 0)))) (h "1i25mikib1z7qln2152agg86zph4dx5hqp2rnjfa04vpig97max7")))

(define-public crate-sfsm-0.4.0 (c (n "sfsm") (v "0.4.0") (d (list (d (n "sfsm-base") (r "^0.4.0") (d #t) (k 0)) (d (n "sfsm-proc") (r "^0.4.0") (k 0)))) (h "187imzf4gxl2w5l5ap3xyk594551mlx5lib2n1s4pmzq2rjc322i") (f (quote (("trace-steps" "sfsm-proc/trace-steps") ("trace-messages" "sfsm-proc/trace-messages") ("trace" "sfsm-proc/trace"))))))

(define-public crate-sfsm-0.4.1 (c (n "sfsm") (v "0.4.1") (d (list (d (n "sfsm-base") (r "^0.4.1") (d #t) (k 0)) (d (n "sfsm-proc") (r "^0.4.1") (k 0)))) (h "1c1010af3kdykmw68hxwv2k1vv6fsdd8x96gljyrnnn2nb7ya7b8") (f (quote (("trace-steps" "sfsm-proc/trace-steps") ("trace-messages" "sfsm-proc/trace-messages") ("trace" "sfsm-proc/trace"))))))

(define-public crate-sfsm-0.4.2 (c (n "sfsm") (v "0.4.2") (d (list (d (n "sfsm-base") (r "^0.4.2") (d #t) (k 0)) (d (n "sfsm-proc") (r "^0.4.2") (k 0)))) (h "1brz6zdgcrqr5bryldrs1rbnj6vvw8lf4k8ah3bm2lh64gy6iqwk") (f (quote (("trace-steps" "sfsm-proc/trace-steps") ("trace-messages" "sfsm-proc/trace-messages") ("trace" "sfsm-proc/trace"))))))

(define-public crate-sfsm-0.4.3 (c (n "sfsm") (v "0.4.3") (d (list (d (n "sfsm-base") (r "^0.4.3") (d #t) (k 0)) (d (n "sfsm-proc") (r "^0.4.3") (k 0)))) (h "13bdig0cwkwkcf9gsc2kpzsymzmzq0agni2n4ps2m8gbn3lrqa1s") (f (quote (("trace-steps" "sfsm-proc/trace-steps") ("trace-messages" "sfsm-proc/trace-messages") ("trace" "sfsm-proc/trace"))))))

