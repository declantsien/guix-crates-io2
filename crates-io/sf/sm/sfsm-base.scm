(define-module (crates-io sf sm sfsm-base) #:use-module (crates-io))

(define-public crate-sfsm-base-0.1.0 (c (n "sfsm-base") (v "0.1.0") (h "08hj38rw63gymn9cn62kas6c7484ch42wk58jdx7yz4llvg99gdl")))

(define-public crate-sfsm-base-0.1.1 (c (n "sfsm-base") (v "0.1.1") (h "05w1va2slb5appp3bsvziz4z7cg0hfsxrfd83zmhscqzralry1br")))

(define-public crate-sfsm-base-0.1.2 (c (n "sfsm-base") (v "0.1.2") (h "1akdlw29c7m737xy3k851khvqv12qg2lsm68any4hn6l654f3f2c")))

(define-public crate-sfsm-base-0.1.4 (c (n "sfsm-base") (v "0.1.4") (h "0a1kycj8pjflgf6h1p8wr6klfq2kx3dv6a15gn1wy5b49s1sl7kd")))

(define-public crate-sfsm-base-0.1.6 (c (n "sfsm-base") (v "0.1.6") (h "1hw91wc5vq7vb49iqy5brlrjg2gljag8bq8j6wr3c4z1ijajfl18")))

(define-public crate-sfsm-base-0.3.0 (c (n "sfsm-base") (v "0.3.0") (h "0kj4hphzvmx13hvk7k1hdlp5pw66hbv5gikyha4x4iiwjhszg69m")))

(define-public crate-sfsm-base-0.4.0 (c (n "sfsm-base") (v "0.4.0") (h "09hbjvw5lknd8kxm7q125i4ll1rs26lb2mwvrxwjzmkgjvd8f5dx")))

(define-public crate-sfsm-base-0.4.1 (c (n "sfsm-base") (v "0.4.1") (h "06q6fs31xwy2m3nqpg341dnnbjj2yzsz7xzv0zghvzvg5wa93xbr")))

(define-public crate-sfsm-base-0.4.2 (c (n "sfsm-base") (v "0.4.2") (h "12zc759r1pv7pbgnkmknsq28yli92lmaanhvpa1d5xp45z3bnxg0")))

(define-public crate-sfsm-base-0.4.3 (c (n "sfsm-base") (v "0.4.3") (h "1kmzsv51fzqxswf3j6sij5xd2rpr8dninfs1rrx9lvw3n4cf3naw")))

