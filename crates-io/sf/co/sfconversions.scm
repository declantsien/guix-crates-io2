(define-module (crates-io sf co sfconversions) #:use-module (crates-io))

(define-public crate-sfconversions-0.1.0 (c (n "sfconversions") (v "0.1.0") (d (list (d (n "extendr-api") (r "^0.4.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.9") (d #t) (k 0)))) (h "17znkb1306qldfxhmdvkrkjpbywarzfmcr4308g1w16i36v9vrnd")))

(define-public crate-sfconversions-0.2.0 (c (n "sfconversions") (v "0.2.0") (d (list (d (n "extendr-api") (r ">=0.4.0") (d #t) (k 0)) (d (n "geo") (r ">=0.25.0") (d #t) (k 0)) (d (n "geo-types") (r ">=0.6.0") (d #t) (k 0)) (d (n "rstar") (r ">=0.11.0") (d #t) (k 0)))) (h "00qnxd0ny8gmf6cazbzha94vgwbnjcx5v0m0zflfl1s8dlc9hfkd")))

