(define-module (crates-io sf xr sfxr) #:use-module (crates-io))

(define-public crate-sfxr-0.1.0 (c (n "sfxr") (v "0.1.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 2)))) (h "0j18cyryi26v9qqhy229xjih5q38aa0z5lgxvcim02v8q3w7zg0g")))

(define-public crate-sfxr-0.1.1 (c (n "sfxr") (v "0.1.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 2)))) (h "04bsyp4wyhid88vxcnjcgmmy5wsl5zn5mpwwpr5y29y42vvmk0z6")))

(define-public crate-sfxr-0.1.2 (c (n "sfxr") (v "0.1.2") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 2)))) (h "0kg67gnjjfc67nxw12p3336g17a58byryfccv7cwz84n6b0mjc8y")))

(define-public crate-sfxr-0.1.3 (c (n "sfxr") (v "0.1.3") (d (list (d (n "prng") (r "^0.1.1") (d #t) (k 0)) (d (n "rng_trait") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 2)))) (h "05ivdvpvggaqvq2vmnix0fdm21zd1amf66mq6jr5wd5yialhas3l")))

(define-public crate-sfxr-0.1.4 (c (n "sfxr") (v "0.1.4") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.31.0") (d #t) (k 2)))) (h "03ngslv13k43x7yvjmb1xwhndwqz9kcbijmiw5k7gjpvay9r5wm2")))

