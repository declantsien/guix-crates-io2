(define-module (crates-io ue be uebersetzt) #:use-module (crates-io))

(define-public crate-uebersetzt-0.1.0 (c (n "uebersetzt") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.39") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "059bg8g1kdvzmbmj9z8g610jfd7a3804i632gip034isjq702iqv")))

(define-public crate-uebersetzt-0.1.1 (c (n "uebersetzt") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.39") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0n2l1lcymg648gipinkq37g9fkb7j35x8d1qy5h22hh4i2qwfbx8")))

(define-public crate-uebersetzt-0.1.2 (c (n "uebersetzt") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.39") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "10pcwj19wzv582sminwljhca2jq0jh37pbg22ryv55gbvqd1hxfa")))

(define-public crate-uebersetzt-0.1.3 (c (n "uebersetzt") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.39") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "18y0arhjnpa4hb8ra830viqzjc976dysjngjnmqgkjax49m0aa8k")))

