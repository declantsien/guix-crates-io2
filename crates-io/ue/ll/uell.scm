(define-module (crates-io ue ll uell) #:use-module (crates-io))

(define-public crate-uell-0.1.0 (c (n "uell") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.5.0") (f (quote ("boxed"))) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1msg9czq995d345w2zrwmxngcx4vapqq2zg7601y44l6wa15kpj0")))

