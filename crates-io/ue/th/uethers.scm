(define-module (crates-io ue th uethers) #:use-module (crates-io))

(define-public crate-uethers-0.1.0 (c (n "uethers") (v "0.1.0") (d (list (d (n "ethereum") (r "^0.12.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.11.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "11jvm7iyvj3pizc2957ikwg0bhw4lqgcni0nnh6xmmyiyiwmmkcq")))

(define-public crate-uethers-0.1.1 (c (n "uethers") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.11.1") (f (quote ("serde_no_std" "rustc-hex"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1zh29g5z4crb8dh8clsfh456il31az51p8m13p4pzxvd24qfdp2s")))

(define-public crate-uethers-0.2.0 (c (n "uethers") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.11.1") (f (quote ("serde_no_std" "rustc-hex"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0c3f7bi0h6haybwgg6gz1c1qg18lwz4pkhk19g42rqrlz0sj10xb")))

(define-public crate-uethers-0.2.1 (c (n "uethers") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.11.1") (f (quote ("serde_no_std" "rustc-hex"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1kbls16j4nj5lwrkva7m7pcxx9vah4j4pz7bgzkaz3q0z4ysxscp")))

(define-public crate-uethers-0.2.2 (c (n "uethers") (v "0.2.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.11.1") (f (quote ("serde_no_std" "rustc-hex"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1f42b14nhpm11kgj0ihyxyyxqx9qy6bzqyl2m3r04mgdwkv0ssa7")))

(define-public crate-uethers-0.2.3 (c (n "uethers") (v "0.2.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.11.1") (f (quote ("serde_no_std" "rustc-hex"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "10b8ik0fqpmcryf2rymgw1xjyqsgin9bsq5a3rgyl6gs6h8axh2y")))

(define-public crate-uethers-0.3.0 (c (n "uethers") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.11.1") (f (quote ("serde_no_std" "rustc-hex"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1a15cbrm2fchj14jkbdpmls0zxp1sjnb2h4s1s70nr8l0vap7a30")))

(define-public crate-uethers-0.4.0 (c (n "uethers") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.11.1") (f (quote ("serde_no_std" "rustc-hex"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1iy3nv1p56sswp4bi5sk8rz7h79hhgp9vsysx2g6k975i45p6f1j")))

(define-public crate-uethers-0.4.1 (c (n "uethers") (v "0.4.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.11.1") (f (quote ("serde_no_std" "rustc-hex"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0asygfb3lcqld54j487lrzja1jwl2jf0ss1zl53ji9mx92csfmln")))

(define-public crate-uethers-0.5.0 (c (n "uethers") (v "0.5.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.11.1") (f (quote ("serde_no_std" "rustc-hex"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1s3cz92i0lpcfs14fwh0c0qp8hlcybslk5fhf2n33v2al7igbf5d")))

(define-public crate-uethers-0.5.1 (c (n "uethers") (v "0.5.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12.2") (f (quote ("serde_no_std" "rustc-hex"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "ureq") (r "^2.8.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0an1iis7pk0nz01hbi1f5j0qcfwil38s7h1y0bd4h7zhpnw58wyy")))

