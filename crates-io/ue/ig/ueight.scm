(define-module (crates-io ue ig ueight) #:use-module (crates-io))

(define-public crate-ueight-0.1.0 (c (n "ueight") (v "0.1.0") (h "1dgzzjqwiwzwkwhjcv8v15scbwj8wcknjcb1ip5c76sbix2y96p0") (y #t)))

(define-public crate-ueight-0.1.1 (c (n "ueight") (v "0.1.1") (h "041ssg5xz7rb2aaiad7jh3rb76s4lm45c389v6gqgs6s6arlkm8x") (y #t)))

(define-public crate-ueight-0.2.0 (c (n "ueight") (v "0.2.0") (h "0s3ihiv2gs82xgdlgyys7dhbq4g3pbcl46b9snlpak81q14yqq3y") (y #t)))

(define-public crate-ueight-0.2.1 (c (n "ueight") (v "0.2.1") (h "1c8m2j20sz3z18jhllmaj38gii9ihf5x4y39xhq7w2r5f4hp4kcy") (y #t)))

(define-public crate-ueight-0.2.2 (c (n "ueight") (v "0.2.2") (h "15fdy5dw14s33bnphx8fd9ll2n65i2228qh2jiw4hmcqqd6l4vla") (y #t)))

(define-public crate-ueight-0.2.3 (c (n "ueight") (v "0.2.3") (h "0f8m1964mqbxj181k0vlbamw9in721l1j1imgvbg4hq3mnlhbf4l") (y #t)))

(define-public crate-ueight-0.2.4 (c (n "ueight") (v "0.2.4") (h "0i46qi3q4rdpz8ilqw414l328vmzhcjiakliznxf06zf606cw3lx") (y #t)))

(define-public crate-ueight-0.2.5 (c (n "ueight") (v "0.2.5") (h "1difkpp5p5ink3ap7c0pqziwz188bwg6xgm7z53fan95lj5f62pc") (y #t)))

(define-public crate-ueight-0.2.6 (c (n "ueight") (v "0.2.6") (h "0rvkjl2b8a7d32dlvxr3wjrxin55iw5d1w3wkzd0w7rf6i0gx0dy") (y #t)))

(define-public crate-ueight-0.2.7 (c (n "ueight") (v "0.2.7") (h "0lf03c65xzz8k8w5djviifnwir0wh6qqhj7hswdlqghxs2l12ffi")))

