(define-module (crates-io ue nc uenc) #:use-module (crates-io))

(define-public crate-uenc-0.1.1 (c (n "uenc") (v "0.1.1") (d (list (d (n "url") (r "^0.5.7") (d #t) (k 0)))) (h "0aal35704d2kjsnkac9imp7r1rviviq0jq4b2m62dny3l2w9a9v3")))

(define-public crate-uenc-0.1.2 (c (n "uenc") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^0.5.7") (d #t) (k 0)))) (h "0ikdnhav91cgwy8a2vgnqmpll22wm76rk47q851g6mbpb11cprw0")))

