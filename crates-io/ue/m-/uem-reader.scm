(define-module (crates-io ue m- uem-reader) #:use-module (crates-io))

(define-public crate-uem-reader-0.1.0 (c (n "uem-reader") (v "0.1.0") (d (list (d (n "enum-iterator") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "usb-ids") (r "^0.2.5") (d #t) (k 0)))) (h "1y7dblcr44qf41sdvjndi5rfmxpbmrqw5lnbgrczcbfsjf5vrhyj") (f (quote (("std") ("default" "std"))))))

(define-public crate-uem-reader-0.2.0 (c (n "uem-reader") (v "0.2.0") (d (list (d (n "enum-iterator") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "usb-ids") (r "^1.2022") (d #t) (k 0)))) (h "0qhzplqnk19izpgxhc2sk0vv9bsxrwx6nxb4cpfqjwpk3mrqaxzl") (f (quote (("std") ("default" "std"))))))

(define-public crate-uem-reader-0.2.1 (c (n "uem-reader") (v "0.2.1") (d (list (d (n "enum-iterator") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "usb-ids") (r "^1.2022") (d #t) (k 0)))) (h "1ddrm0mk055z84yfzrbiraaa5s776gb7i6xaqq297vaiih7j3cm2") (f (quote (("std") ("default" "std"))))))

