(define-module (crates-io ue fc uefc) #:use-module (crates-io))

(define-public crate-uefc-0.1.0 (c (n "uefc") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.17.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1kbvlsfbfby4jxdcx73q4yfpk4wain7mia2cxih88xvpsl6nkpfw")))

(define-public crate-uefc-0.2.0 (c (n "uefc") (v "0.2.0") (d (list (d (n "indicatif") (r "^0.17.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1q0r59fc497wpx2gnzidy80hg1d9zlhx6b7lbk8zx09pxd40gr60")))

(define-public crate-uefc-0.2.1 (c (n "uefc") (v "0.2.1") (d (list (d (n "indicatif") (r "^0.17.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "184g7wgwk9r6g0mwz0rxbyfp57zhkpjcddjjh1ahxjw74c0ndawk")))

