(define-module (crates-io ue fi uefi-graphics-driver) #:use-module (crates-io))

(define-public crate-uefi-graphics-driver-0.1.0 (c (n "uefi-graphics-driver") (v "0.1.0") (d (list (d (n "embedded-graphics-core") (r "^0.3.3") (d #t) (k 0)) (d (n "uefi") (r "^0.20") (d #t) (k 0)) (d (n "uefi-services") (r "^0.17") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "1z4mzflqsj5jjssbc64pqlb6rpcy5m4qvb2kmn2bncidvxvg3km8") (y #t)))

(define-public crate-uefi-graphics-driver-0.1.1 (c (n "uefi-graphics-driver") (v "0.1.1") (d (list (d (n "embedded-graphics-core") (r "^0.3.3") (d #t) (k 0)) (d (n "uefi") (r "^0.20") (d #t) (k 0)) (d (n "uefi-services") (r "^0.17") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "06szqxqkv52ahkp2whvymk4y5bajgfz1j0whf1h1mq45fpkkbbap")))

