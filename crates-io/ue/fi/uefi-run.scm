(define-module (crates-io ue fi uefi-run) #:use-module (crates-io))

(define-public crate-uefi-run-0.0.0 (c (n "uefi-run") (v "0.0.0") (h "10hig09gj853jd6kssrqw9r9a1blvf5cmz3063wcma9h5anjarq3")))

(define-public crate-uefi-run-0.1.0 (c (n "uefi-run") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "fatfs") (r "^0.3") (d #t) (k 0)))) (h "1q5fl2wkwbjqqywabsrnnyggaw5wrmm081xzbb5cagzv6fk5xfb6")))

(define-public crate-uefi-run-0.2.0 (c (n "uefi-run") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "fatfs") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "03hjhs29f0mjvyadhbwczq3cgqzbc4k9ff1wjb9bw5ydzfm99ldv")))

(define-public crate-uefi-run-0.3.0 (c (n "uefi-run") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "fatfs") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "048vb6byp501mcsdpmzrsmnd2z94lgpjnbggj5slr5p4rilb54fd")))

(define-public crate-uefi-run-0.3.1 (c (n "uefi-run") (v "0.3.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "fatfs") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "1m9yjvg0249wrsf208sv9vis6m60j77k56qljjzcz4jgc18jwg6f")))

(define-public crate-uefi-run-0.3.2 (c (n "uefi-run") (v "0.3.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "fatfs") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "0bfcififkjh1qbcr1qn8yv2krgp8p5v4q8jykrn9969brp8kk5am")))

(define-public crate-uefi-run-0.3.3 (c (n "uefi-run") (v "0.3.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "fatfs") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "1il72r7v20qxsvfbvrw7gw4kk6y0x9wkcfisfmp5j7qdwk63amgz")))

(define-public crate-uefi-run-0.4.0 (c (n "uefi-run") (v "0.4.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "fatfs") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "06x8dnaxrkq0adq5rc5w9y1gxzaq3d1abkxrydqlq3hb8lgfdyaz")))

(define-public crate-uefi-run-0.5.0 (c (n "uefi-run") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "fatfs") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "06lrf7n1swrrra58h5psjrj00nvkwiwwxxr6vcvag0r880lmxzf1")))

(define-public crate-uefi-run-0.6.0 (c (n "uefi-run") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "fatfs") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "0pvlj8cnfnv6hhn74kj0gkyskwi6cmly3rvz0pwdm75cbpvzc339")))

(define-public crate-uefi-run-0.6.1 (c (n "uefi-run") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "fatfs") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "0igv9h01rcbhpxx60djmj2hgrvmml5aynnn8xf6wbxd25cgmjnls")))

