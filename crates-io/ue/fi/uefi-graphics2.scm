(define-module (crates-io ue fi uefi-graphics2) #:use-module (crates-io))

(define-public crate-uefi-graphics2-0.1.0 (c (n "uefi-graphics2") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.8") (d #t) (k 0)) (d (n "uefi") (r "^0.27") (d #t) (k 0)))) (h "0xi7n8srnxn2arnzyynn4i633lza82r1wx5g4p647bmbgzvv10qv")))

(define-public crate-uefi-graphics2-0.1.1 (c (n "uefi-graphics2") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.8") (d #t) (k 0)) (d (n "uefi") (r "^0.28") (d #t) (k 0)))) (h "1xqvp4l37rsdj1bbxsa0g5fqglnm62z3lma63ac0j7cdwn53fyz6")))

(define-public crate-uefi-graphics2-0.1.2 (c (n "uefi-graphics2") (v "0.1.2") (d (list (d (n "embedded-graphics") (r "^0.8") (d #t) (k 0)) (d (n "uefi") (r "^0.28") (d #t) (k 0)))) (h "1y0bbz2m3v8g2kpql945wcsgkz4pymxg4b7ys152di01i5r1z9fl")))

(define-public crate-uefi-graphics2-0.1.3 (c (n "uefi-graphics2") (v "0.1.3") (d (list (d (n "embedded-graphics") (r "^0.8") (d #t) (k 0)) (d (n "uefi") (r "^0.28") (d #t) (k 0)))) (h "0xygml688aahszi68h2569zq9bnl4485accaf0j94fvw4ifapv5p")))

