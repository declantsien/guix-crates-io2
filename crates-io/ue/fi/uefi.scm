(define-module (crates-io ue fi uefi) #:use-module (crates-io))

(define-public crate-uefi-0.1.0 (c (n "uefi") (v "0.1.0") (h "0vwjsi08mnhanqjik0h4i4sg7zrfcf2x3ixklhbq66q4nbsyk1ah") (y #t)))

(define-public crate-uefi-0.1.1 (c (n "uefi") (v "0.1.1") (h "1lap95cd5j1k1qzsixc7z63rsf95ajpzgnr683ikwf7kxz03naln") (y #t)))

(define-public crate-uefi-0.1.2 (c (n "uefi") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0b73dcbswh2qni1l79v7jaz0ym5bkx8bcjcs85y2yxpqd5cvzy0f") (y #t)))

(define-public crate-uefi-0.2.0 (c (n "uefi") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "ucs2") (r "^0.1.0") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0c0p3711fdip8k2vbsz5m5h08ms5scba9c4dp72advf3nj7ppg0k")))

(define-public crate-uefi-0.3.0 (c (n "uefi") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "ucs2") (r "^0.1.0") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.2.0") (d #t) (k 0)))) (h "148902vg8af2v3hqbzi00y8cl243s9b93mcgywi56cqakikac8gn") (f (quote (("logger") ("default") ("alloc"))))))

(define-public crate-uefi-0.3.1 (c (n "uefi") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "ucs2") (r "^0.1.0") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.2.1") (d #t) (k 0)))) (h "16fdc0zya39mrnyni6j9vcbcd2nmwvx4ln2h2qbgdx4sp3war6w9") (f (quote (("logger") ("default") ("alloc"))))))

(define-public crate-uefi-0.3.2 (c (n "uefi") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "ucs2") (r "^0.1.0") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.2.1") (d #t) (k 0)))) (h "063s8sz3dxhc0w02dwf168kg0fs91xv0ws66dq3m3wc0xpr5i0np") (f (quote (("logger") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.4.0 (c (n "uefi") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "ucs2") (r "^0.1.0") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0pfh16qqcdky41zma8yyh7ax9d7p208zbqqlhznsbx3dswvcba45") (f (quote (("logger") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.4.1 (c (n "uefi") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "ucs2") (r "^0.2.0") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0xsg1mzahxnb4yxnn0smfizga72s9r7hnrlcjin6fm794jb0gydq") (f (quote (("logger") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.4.2 (c (n "uefi") (v "0.4.2") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "ucs2") (r "^0.2.0") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.0") (d #t) (k 0)))) (h "056ixcybs7fxcfm91pabif3frisnlqsd6qpzqycinm734y83ngy1") (f (quote (("logger") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.4.3 (c (n "uefi") (v "0.4.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0d2rp63vxbm6y3lsjwz94y0bkcrma70gc5w967qygnc0xzgkviin") (f (quote (("logger") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.4.4 (c (n "uefi") (v "0.4.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.0") (d #t) (k 0)))) (h "185lrl2gl5j3iyqavd9xq9q22bxavssl0884r265i7v3a0l6mv2w") (f (quote (("logger") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.4.5 (c (n "uefi") (v "0.4.5") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.0") (d #t) (k 0)))) (h "01gqfcgjqsfpvwa035k4nwk20dvv1s1i1vply5q9arkh072krn29") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.4.6 (c (n "uefi") (v "0.4.6") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.2") (d #t) (k 0)))) (h "0pahg03d6cbn1g2gqvj4ygmny1hjvg1sqq8128jpvlxsxh1i87xb") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.4.7 (c (n "uefi") (v "0.4.7") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.2") (d #t) (k 0)))) (h "04kyfrzkjrw52h4z5ijhj44afms5b103hzxnmnjqwj62n6ldiz08") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.5.0 (c (n "uefi") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.2") (d #t) (k 0)))) (h "0vp6psrzzsrsrif07alx4b9yd115clb1wyiyfp66y09rs8dilmbc") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.6.0 (c (n "uefi") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.2") (d #t) (k 0)))) (h "1yj2lnclpi41vcc9vkng7qb9bd2pfwsbandxyp8v9b3kw2cg9js3") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.7.0 (c (n "uefi") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.2") (d #t) (k 0)))) (h "1gpik4fg7xiyy7idhwqba3rn9d33jyxb911xsxqw0ra6fh34d2xz") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.8.0 (c (n "uefi") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.2") (d #t) (k 0)))) (h "1m16qnhdqk316z1mz51yrsj9q06znz46y63jx00qjxi24fbnzy21") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.8.1 (c (n "uefi") (v "0.8.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.2") (d #t) (k 0)))) (h "1qmz5mg81g9fxnxwqfbpcldlbcyi214gvxg241z3j85xk19ay7lj") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.9.0 (c (n "uefi") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.2") (d #t) (k 0)))) (h "07gw195799ppj57fnz6jhl8jq774bv4dpb3vyvmwbwqdyj1vw270") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.10.0 (c (n "uefi") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.2") (d #t) (k 0)))) (h "15mhqa0ayczkmyhsk8rd5nglpzqbixranwndlhb4yfb7sqb46yb4") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.11.0 (c (n "uefi") (v "0.11.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.3.2") (d #t) (k 0)))) (h "1x6vqpiydn1p18gk08p2wxf23q7r7jn5v1nvvsrz4wmch0pajc26") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.12.0 (c (n "uefi") (v "0.12.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.4.0") (d #t) (k 0)))) (h "1x79bbvcpz0srbmrallfi1v2j8gn8m07xd8fk8hwdjg8iv7m312g") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.13.0 (c (n "uefi") (v "0.13.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.5.0") (d #t) (k 0)))) (h "04q1h80z34k57sps46xpy0cmxb4mdsqvy8mkcxs6l3d8q7n66wyd") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.14.0 (c (n "uefi") (v "0.14.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.5.0") (d #t) (k 0)))) (h "15klvaxdhmdxnly8dfkyb1rxzk2fhwa6fx9lvzv19ypn0jj9h4x2") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.15.0 (c (n "uefi") (v "0.15.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.6.0") (d #t) (k 0)))) (h "160v6v77lb7bvznjfk2dz2mkm52mghfrc0vn5jmqfkdj9y038imq") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc")))) (y #t)))

(define-public crate-uefi-0.15.1 (c (n "uefi") (v "0.15.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.6.0") (d #t) (k 0)))) (h "01cygzs5j863wxfj0b9iplbzv6qk05hcvj4w4rwikj9i5k5c8d3i") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc")))) (y #t)))

(define-public crate-uefi-0.15.2 (c (n "uefi") (v "0.15.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.6.0") (d #t) (k 0)))) (h "0zfdh2akfl3g59733a11f4rc4a1rk4llpdnd0k5f6vg99hjbwhr0") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.16.0 (c (n "uefi") (v "0.16.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.7.0") (d #t) (k 0)))) (h "07z27g43bb0sg4c74i875zg3s3vir1gabpmcqwy06jvf737kambh") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.16.1 (c (n "uefi") (v "0.16.1") (d (list (d (n "bitflags") (r "^1.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.7.0") (d #t) (k 0)))) (h "1zbkr6i1pk9vjyhsnayz66z9n6g53id3h214cnfzpa736i4yjsbf") (f (quote (("logger") ("ignore-logger-errors") ("exts") ("default") ("alloc"))))))

(define-public crate-uefi-0.17.0 (c (n "uefi") (v "0.17.0") (d (list (d (n "bitflags") (r "^1.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.8.0") (d #t) (k 0)))) (h "1m80ibsg5vdqmpsv8iz79vf690qg7xh583yl5sb60r90b161508n") (f (quote (("panic-on-logger-errors") ("logger") ("exts") ("default" "panic-on-logger-errors") ("alloc"))))))

(define-public crate-uefi-0.18.0 (c (n "uefi") (v "0.18.0") (d (list (d (n "bitflags") (r "^1.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.9.0") (d #t) (k 0)))) (h "0d8zp6rp17l574xdjyjppf6rykwd9lvy6ssm854dsr9xhq07gf07") (f (quote (("panic-on-logger-errors") ("logger") ("exts") ("default" "panic-on-logger-errors") ("alloc"))))))

(define-public crate-uefi-0.19.0 (c (n "uefi") (v "0.19.0") (d (list (d (n "bitflags") (r "^1.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.10.0") (d #t) (k 0)))) (h "0ms7wlmd3jia0sq00cz3q57yzrzd6n67wdc5njggcn5j6js7daf8") (f (quote (("unstable") ("panic-on-logger-errors") ("logger") ("global_allocator") ("default" "panic-on-logger-errors") ("alloc"))))))

(define-public crate-uefi-0.19.1 (c (n "uefi") (v "0.19.1") (d (list (d (n "bitflags") (r "^1.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.10.0") (d #t) (k 0)))) (h "0i6qnhp1j8r1c4h4czdc7ybawkwlg9y2byj2sh70f09jhkppyq7m") (f (quote (("unstable") ("panic-on-logger-errors") ("logger") ("global_allocator") ("default" "panic-on-logger-errors") ("alloc"))))))

(define-public crate-uefi-0.20.0 (c (n "uefi") (v "0.20.0") (d (list (d (n "bitflags") (r "^1.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.11.0") (d #t) (k 0)))) (h "1ca3gj1lhhwqi8np07xym3kkzglb0cqryrfn8r6fs88gfkkxafdb") (f (quote (("unstable") ("panic-on-logger-errors") ("logger") ("global_allocator") ("default" "panic-on-logger-errors") ("alloc"))))))

(define-public crate-uefi-0.21.0 (c (n "uefi") (v "0.21.0") (d (list (d (n "bitflags") (r "^2.1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.12.0") (d #t) (k 0)) (d (n "uefi-raw") (r "^0.1.0") (d #t) (k 0)) (d (n "uguid") (r "^2.0.0") (d #t) (k 0)))) (h "0skbp127n8yh27wrs1410ai3yfh6flljbx7gjn3gnm7dv4i8qmhv") (f (quote (("unstable") ("panic-on-logger-errors") ("logger") ("global_allocator") ("default" "panic-on-logger-errors") ("alloc")))) (r "1.68")))

(define-public crate-uefi-0.22.0 (c (n "uefi") (v "0.22.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.12.0") (d #t) (k 0)) (d (n "uefi-raw") (r "^0.2.0") (d #t) (k 0)) (d (n "uguid") (r "^2.0.0") (d #t) (k 0)))) (h "1f7mqg2pxy7vszxvf3gk2cakfzc0kwnh04yiivabcz1819m9hnjb") (f (quote (("unstable") ("panic-on-logger-errors") ("logger") ("global_allocator") ("default" "panic-on-logger-errors") ("alloc")))) (r "1.68")))

(define-public crate-uefi-0.23.0 (c (n "uefi") (v "0.23.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.12.0") (d #t) (k 0)) (d (n "uefi-raw") (r "^0.2.0") (d #t) (k 0)) (d (n "uguid") (r "^2.0.0") (d #t) (k 0)))) (h "1kax0xkkjzr38b8hxv392mxb232k639aqs2jw93xbg2mjcg34rjc") (f (quote (("unstable") ("panic-on-logger-errors") ("logger") ("global_allocator") ("default" "panic-on-logger-errors") ("alloc")))) (r "1.68")))

(define-public crate-uefi-0.24.0 (c (n "uefi") (v "0.24.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.12.0") (d #t) (k 0)) (d (n "uefi-raw") (r "^0.3.0") (d #t) (k 0)) (d (n "uguid") (r "^2.0.0") (d #t) (k 0)))) (h "040a9mqqsn8mw5lslh5n9wsp010assxjm2zifkdv1gdlhqkfhqrv") (f (quote (("unstable") ("panic-on-logger-errors") ("logger") ("global_allocator") ("default" "panic-on-logger-errors") ("alloc")))) (r "1.68")))

(define-public crate-uefi-0.25.0 (c (n "uefi") (v "0.25.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.12.0") (d #t) (k 0)) (d (n "uefi-raw") (r "^0.4.0") (d #t) (k 0)) (d (n "uguid") (r "^2.1.0") (d #t) (k 0)))) (h "0iwx76x8425qlhj09x5rfl3n0k6jjiyvp46z5xngb1shj22g7f0w") (f (quote (("unstable") ("panic-on-logger-errors") ("logger") ("global_allocator") ("default" "panic-on-logger-errors") ("alloc")))) (r "1.70")))

(define-public crate-uefi-0.26.0 (c (n "uefi") (v "0.26.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.13.0") (d #t) (k 0)) (d (n "uefi-raw") (r "^0.5.0") (d #t) (k 0)) (d (n "uguid") (r "^2.1.0") (d #t) (k 0)))) (h "1ifwa0c2jd03gd6z9q57h07ah4via9mx7bahp1wn8r5493vxksh7") (f (quote (("unstable") ("panic-on-logger-errors") ("logger") ("global_allocator") ("default" "panic-on-logger-errors") ("alloc")))) (r "1.70")))

(define-public crate-uefi-0.27.0 (c (n "uefi") (v "0.27.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.13.0") (d #t) (k 0)) (d (n "uefi-raw") (r "^0.5.1") (d #t) (k 0)) (d (n "uguid") (r "^2.1.0") (d #t) (k 0)))) (h "0m582hwzajh2wn45j7mdb57fd8fj1i2pii7lzqsmgm0jqqs9rvl9") (f (quote (("unstable") ("panic-on-logger-errors") ("logger") ("global_allocator") ("default" "panic-on-logger-errors") ("alloc")))) (r "1.70")))

(define-public crate-uefi-0.28.0 (c (n "uefi") (v "0.28.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "qemu-exit") (r "^3.0.2") (o #t) (d #t) (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-macros") (r "^0.13.0") (d #t) (k 0)) (d (n "uefi-raw") (r "^0.5.2") (d #t) (k 0)) (d (n "uguid") (r "^2.1.0") (d #t) (k 0)))) (h "1yk9db9632dvhdq5syr23z18z5l44lfa3p3dmnd5ilpyr5nsbh59") (f (quote (("unstable") ("panic_handler") ("panic-on-logger-errors") ("logger") ("global_allocator") ("default" "panic-on-logger-errors") ("alloc")))) (s 2) (e (quote (("qemu" "dep:qemu-exit" "panic_handler")))) (r "1.70")))

