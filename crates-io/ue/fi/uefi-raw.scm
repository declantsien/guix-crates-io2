(define-module (crates-io ue fi uefi-raw) #:use-module (crates-io))

(define-public crate-uefi-raw-0.1.0 (c (n "uefi-raw") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.1") (d #t) (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "uguid") (r "^2.0.0") (d #t) (k 0)))) (h "1mbn6yb7klwj8cqbxpm5k66c7dswgwvghcniq2y038m8v261qnrx") (r "1.68")))

(define-public crate-uefi-raw-0.2.0 (c (n "uefi-raw") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "uguid") (r "^2.0.0") (d #t) (k 0)))) (h "09cci1wwa71ca97ndvhlnjvwwwfx7chm72kw1blwgds4x7c0hgnp") (r "1.68")))

(define-public crate-uefi-raw-0.3.0 (c (n "uefi-raw") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "uguid") (r "^2.0.0") (d #t) (k 0)))) (h "0bh848760cbvlspgvc030rd53hszdm4ah38vyjjl2r4w14b2ar32") (r "1.68")))

(define-public crate-uefi-raw-0.4.0 (c (n "uefi-raw") (v "0.4.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "uguid") (r "^2.1.0") (d #t) (k 0)))) (h "1gra296xdfm1lgkn1rjphhrihr3nrn353pblfvrrxa1d23p4bgwb") (r "1.70")))

(define-public crate-uefi-raw-0.5.0 (c (n "uefi-raw") (v "0.5.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "uguid") (r "^2.1.0") (d #t) (k 0)))) (h "1myzgfm9dr3flmlm35wzgl2x0bhj50cvw53q9srvyxyqmngccjl6") (r "1.70")))

(define-public crate-uefi-raw-0.5.1 (c (n "uefi-raw") (v "0.5.1") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "uguid") (r "^2.1.0") (d #t) (k 0)))) (h "1icq6sg1js2x159g8db9phqkkb6mq03ij20f1lwqwasvws82ih3c") (r "1.70")))

(define-public crate-uefi-raw-0.5.2 (c (n "uefi-raw") (v "0.5.2") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "ptr_meta") (r "^0.2.0") (k 0)) (d (n "uguid") (r "^2.1.0") (d #t) (k 0)))) (h "0g1kg0356snw90amdhjwzrlkn9hgi8w54l7xxnybijp8a9pp3a7g") (r "1.70")))

