(define-module (crates-io ue fi uefi-graphics) #:use-module (crates-io))

(define-public crate-uefi-graphics-0.1.0 (c (n "uefi-graphics") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)) (d (n "uefi") (r "^0.5.0") (d #t) (k 0)))) (h "0rqlphgr9zs89m138wvfq5psg27h8alz3sasfygsvw0wz0jvh5qw")))

(define-public crate-uefi-graphics-0.2.0 (c (n "uefi-graphics") (v "0.2.0") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)) (d (n "uefi") (r "^0.6.0") (d #t) (k 0)))) (h "1mznqbd0mmi73pgp7i88idwapmgq25z1mf474v62fcqaakbzz5bi")))

(define-public crate-uefi-graphics-0.3.0 (c (n "uefi-graphics") (v "0.3.0") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)))) (h "0gkls5xvrr9aqj75c5nkarzxlcfp2h1djmkvrx2a1km61f0b6hll") (y #t)))

(define-public crate-uefi-graphics-0.3.1 (c (n "uefi-graphics") (v "0.3.1") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)))) (h "1xbjakrback7v1knxqvrzgx5gv4zwy35dvxwys6iyzdix5xzk1z7")))

(define-public crate-uefi-graphics-0.4.0 (c (n "uefi-graphics") (v "0.4.0") (d (list (d (n "embedded-graphics-core") (r "^0.3.2") (d #t) (k 0)))) (h "0x5gpfgb9dqqjf5armhghijr8hflzx34jqr4s11naryl2kj7wcgm")))

