(define-module (crates-io ue fi uefi-ffi) #:use-module (crates-io))

(define-public crate-uefi-ffi-0.1.0 (c (n "uefi-ffi") (v "0.1.0") (h "1jvqpl8gxha7z8w6ly09jpvg6hmlpkpkjyzqqfmxh4ql0d8xfdfh")))

(define-public crate-uefi-ffi-0.1.1 (c (n "uefi-ffi") (v "0.1.1") (h "0hpvcpl352wmv19bkca1ip678i4cryr0c4byw1mlk6favv31j1sl")))

