(define-module (crates-io ue fi uefi-macros) #:use-module (crates-io))

(define-public crate-uefi-macros-0.2.0 (c (n "uefi-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0pzla7lv771qfrls2cnx2mqm1h2sx4yr1221rxkizi10d1bysj0r")))

(define-public crate-uefi-macros-0.2.1 (c (n "uefi-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "05385fyvp69ndj1a076v178k6kdxznvka1ng1d29mw5yvvqnn0iw")))

(define-public crate-uefi-macros-0.3.0 (c (n "uefi-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0y6qsmfnqlwwdbv3dwk2z4rvl32z5pn4s8nc1kcbadzynnaa66qi")))

(define-public crate-uefi-macros-0.3.1 (c (n "uefi-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0m89308b32pvmdhqiglmnp9pg55mbqkv60fmv21jxisx46qa3m67")))

(define-public crate-uefi-macros-0.3.2 (c (n "uefi-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0isrz7fm9is6rkhy5kb5qzv83pja91h4bi396xw4vs10v66zlsbs")))

(define-public crate-uefi-macros-0.3.3 (c (n "uefi-macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1a71pqlk63l6k4m0viy2gw40bgzz57dkz3qp1lr4mwv1m06a3k1x")))

(define-public crate-uefi-macros-0.4.0 (c (n "uefi-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "1slh40cddmf3m72hmpcypnwaywsavizvji1liyx95zgkanyaa4p0")))

(define-public crate-uefi-macros-0.5.0 (c (n "uefi-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.53") (d #t) (k 2)) (d (n "uefi") (r "^0.12.0") (k 2)))) (h "1cydmyfirqz40hjg21q9rknwmana6jj305jzii22pbxcx1dbh1kh")))

(define-public crate-uefi-macros-0.6.0 (c (n "uefi-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.88") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.56") (d #t) (k 2)) (d (n "uefi") (r "^0.14.0") (k 2)))) (h "14h4f64lfzy7fpr55f0v91304k5a32ar0nz4nkbxx2h3lc9dqfn5")))

(define-public crate-uefi-macros-0.6.1 (c (n "uefi-macros") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.88") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.56") (d #t) (k 2)) (d (n "uefi") (r "^0.15.0") (k 2)))) (h "1x52w6jqa6751gw9ghbxp3dc4ah2y5wb2m1yfxiiarw3pxk1n9x9")))

(define-public crate-uefi-macros-0.7.0 (c (n "uefi-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)) (d (n "uefi") (r "^0.15.2") (k 2)))) (h "1drv8fyyzq4rr4c8fa1xsmg10rg15fx4y3va5s6bgay53f1ig68b")))

(define-public crate-uefi-macros-0.7.1 (c (n "uefi-macros") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)) (d (n "uefi") (r "^0.16.0") (k 2)))) (h "0rj88z40fm3b4c0xl4k702sj2wm4iybva91gzr0wiv5naqif467n")))

(define-public crate-uefi-macros-0.8.0 (c (n "uefi-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)) (d (n "uefi") (r "^0.16.0") (k 2)))) (h "1kr2830rsrbylz5mqn6cfl2vyn3y76pf7w891vldc9w7mir86h3b") (y #t)))

(define-public crate-uefi-macros-0.8.1 (c (n "uefi-macros") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)) (d (n "uefi") (r "^0.17.0") (k 2)))) (h "14wb4qmpw694m5gzdl3r519a3a62wpl869n9lqlyaqd3zbscfg1w")))

(define-public crate-uefi-macros-0.9.0 (c (n "uefi-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)) (d (n "uefi") (r "^0.17.0") (k 2)))) (h "153bx6hgm5df430sza6y2w8jhyx8k11c47795hxf9mwz3m50apr7")))

(define-public crate-uefi-macros-0.10.0 (c (n "uefi-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)) (d (n "uefi") (r "^0.18.0") (k 2)))) (h "1684j8s087x7swrjn3ipvbviynd1ai01z59hy7vrjb9z3629c2gq")))

(define-public crate-uefi-macros-0.11.0 (c (n "uefi-macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)) (d (n "uefi") (r "^0.19.0") (k 2)))) (h "1m5b7x1j3kd4k0r2lwpsfrncda0aw5mi0hg58zrz3f9igc7fpjp0")))

(define-public crate-uefi-macros-0.12.0 (c (n "uefi-macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)) (d (n "uefi") (r "^0.20.0") (k 2)))) (h "15cfs2fgl09miyfhmpls86qb4416xwa4dn9vka5hcp8kivpr8g82") (r "1.68")))

(define-public crate-uefi-macros-0.13.0 (c (n "uefi-macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)))) (h "0pb9jncg4smsiicrbq5wrxfsyyky7xg23mal9a2xphq8r31b39r6") (r "1.70")))

