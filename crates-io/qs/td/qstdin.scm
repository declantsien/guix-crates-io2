(define-module (crates-io qs td qstdin) #:use-module (crates-io))

(define-public crate-qstdin-0.1.0 (c (n "qstdin") (v "0.1.0") (h "0c82ydvb888jihpjk6k4812sxidw3v98cf9p4k0wa7jsyl21999f")))

(define-public crate-qstdin-0.1.1 (c (n "qstdin") (v "0.1.1") (h "0mn5lqq9f37bpn71cmml0h6dk93bv0xj2hq59pllqkllxnxljbld")))

