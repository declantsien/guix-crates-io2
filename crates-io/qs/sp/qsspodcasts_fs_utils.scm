(define-module (crates-io qs sp qsspodcasts_fs_utils) #:use-module (crates-io))

(define-public crate-qsspodcasts_fs_utils-0.1.0 (c (n "qsspodcasts_fs_utils") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1qx217bh1f8qjiv0ifvcjc2v3l07c8zxwvy3b8pjdyp8dd2xw46c") (r "1.70.0")))

(define-public crate-qsspodcasts_fs_utils-0.1.1 (c (n "qsspodcasts_fs_utils") (v "0.1.1") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1456wy5s0nvcsb4sp6nfvfnhisrc2rxds7k4rzxz585fpwjnsxfp") (r "1.70.0")))

