(define-module (crates-io qs sp qsspodcasts_podcast_management) #:use-module (crates-io))

(define-public crate-qsspodcasts_podcast_management-0.1.0 (c (n "qsspodcasts_podcast_management") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "html2text") (r "^0.4.3") (d #t) (k 0)) (d (n "rss") (r "^2.0.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.10.0") (d #t) (k 0)))) (h "0h8dqln8jc8d7549x0r2zyn4qjiw79rsr6n05snf9h9fjhv3x5fl") (r "1.70.0")))

(define-public crate-qsspodcasts_podcast_management-0.1.1 (c (n "qsspodcasts_podcast_management") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "html2text") (r "^0.4.3") (d #t) (k 0)) (d (n "rss") (r "^2.0.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.10.0") (d #t) (k 0)))) (h "031r879a75a178biy8ikkdb218s1mns4vd95h1p1nd42a00jvg7r") (r "1.70.0")))

