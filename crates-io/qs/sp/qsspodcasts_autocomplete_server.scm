(define-module (crates-io qs sp qsspodcasts_autocomplete_server) #:use-module (crates-io))

(define-public crate-qsspodcasts_autocomplete_server-0.1.0 (c (n "qsspodcasts_autocomplete_server") (v "0.1.0") (d (list (d (n "command_management") (r "^0.1.0") (d #t) (k 0) (p "qsspodcasts_command_management")) (d (n "data_transport") (r "^0.1.0") (d #t) (k 0) (p "qsspodcasts_data_transport")) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1079im51gyyd3lrskbl638ky0avm79m2i36aiqdsyxgpi5g5l0a0") (r "1.70.0")))

(define-public crate-qsspodcasts_autocomplete_server-0.1.1 (c (n "qsspodcasts_autocomplete_server") (v "0.1.1") (d (list (d (n "command_management") (r "^0.1.0") (d #t) (k 0) (p "qsspodcasts_command_management")) (d (n "data_transport") (r "^0.1.0") (d #t) (k 0) (p "qsspodcasts_data_transport")) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "070ml1dq997506dympy0amhabi44wvq5sbfklj4pswqgknaf79la") (r "1.70.0")))

