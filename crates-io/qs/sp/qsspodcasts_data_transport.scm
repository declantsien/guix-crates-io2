(define-module (crates-io qs sp qsspodcasts_data_transport) #:use-module (crates-io))

(define-public crate-qsspodcasts_data_transport-0.1.0 (c (n "qsspodcasts_data_transport") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fmzi9jrdvmqij3gnibm1wrp490136bg3hj011bj5mxjjva5w8lg") (r "1.70.0")))

(define-public crate-qsspodcasts_data_transport-0.1.1 (c (n "qsspodcasts_data_transport") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yvhm65jx03mzxjnp9qyd6d2h16g7ilg9gldb60ikbyxjnv30f9n") (r "1.70.0")))

