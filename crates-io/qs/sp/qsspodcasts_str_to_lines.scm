(define-module (crates-io qs sp qsspodcasts_str_to_lines) #:use-module (crates-io))

(define-public crate-qsspodcasts_str_to_lines-0.1.0 (c (n "qsspodcasts_str_to_lines") (v "0.1.0") (d (list (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1hyb7s3gv45gv7vrjrsr83c10bm8b5wqazq8q1zblnasb5j04dq3") (r "1.70.0")))

(define-public crate-qsspodcasts_str_to_lines-0.1.1 (c (n "qsspodcasts_str_to_lines") (v "0.1.1") (d (list (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1kwb7v400zimxmk9rq7nzvm6jwpikm8w3d20iln5z3xdibyywgm1") (r "1.70.0")))

