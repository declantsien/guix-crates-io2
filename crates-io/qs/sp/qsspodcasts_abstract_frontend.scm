(define-module (crates-io qs sp qsspodcasts_abstract_frontend) #:use-module (crates-io))

(define-public crate-qsspodcasts_abstract_frontend-0.1.0 (c (n "qsspodcasts_abstract_frontend") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03r6jl5dgqk04aywlbfyvxr1azl9cbs9i3x0q5nvnrp315yh01ik") (r "1.70.0")))

(define-public crate-qsspodcasts_abstract_frontend-0.1.1 (c (n "qsspodcasts_abstract_frontend") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "117w24wcpnyxkp8qr5y74ak5cb0hxx856yy8c74yjvmx3rg9n4vr") (r "1.70.0")))

