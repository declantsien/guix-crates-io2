(define-module (crates-io qs sp qsspodcasts_path_providing) #:use-module (crates-io))

(define-public crate-qsspodcasts_path_providing-0.1.0 (c (n "qsspodcasts_path_providing") (v "0.1.0") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "mockall") (r "^0.11.2") (d #t) (k 2)) (d (n "podcast_management") (r "^0.1.0") (d #t) (k 0) (p "qsspodcasts_podcast_management")))) (h "0zz9yfrylfvmidz3m2l9v1s3k02x8gikvzhrsy13c7zmx67lmysz") (r "1.70.0")))

(define-public crate-qsspodcasts_path_providing-0.1.1 (c (n "qsspodcasts_path_providing") (v "0.1.1") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "mockall") (r "^0.11.2") (d #t) (k 2)) (d (n "podcast_management") (r "^0.1.0") (d #t) (k 0) (p "qsspodcasts_podcast_management")))) (h "0a23a16xcdhinj1hvz64d4y812fk9an9d06cxwwysfkwzb48cgw6") (r "1.70.0")))

