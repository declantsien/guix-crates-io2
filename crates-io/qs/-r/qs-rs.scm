(define-module (crates-io qs -r qs-rs) #:use-module (crates-io))

(define-public crate-qs-rs-0.1.0 (c (n "qs-rs") (v "0.1.0") (h "1m9hkl4sh83kknrfighpknbi1gzz430m8vz0rhsilwdg9g79bsw0")))

(define-public crate-qs-rs-0.1.1 (c (n "qs-rs") (v "0.1.1") (h "0k5yxn46n7ws2h9410ckzb9s4gsjwygx7dvhjd8jjcgaxcrki8y6")))

(define-public crate-qs-rs-0.1.2 (c (n "qs-rs") (v "0.1.2") (d (list (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "0753cjc6zfkamrin2ckl3gzp99pmdmn2pysakydi7y94d66fvakh")))

(define-public crate-qs-rs-0.1.3 (c (n "qs-rs") (v "0.1.3") (d (list (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "1xflgi4imghd0adsk4mlz27hlmvs6kq6hsbs1n43aydy8h8cvynb") (y #t)))

(define-public crate-qs-rs-0.1.4 (c (n "qs-rs") (v "0.1.4") (d (list (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "03p1hrhbkqpsycjiy5hj8rccxwscjv3fd481bsrwkql7xvrgqc04")))

