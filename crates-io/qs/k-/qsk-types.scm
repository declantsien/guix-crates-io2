(define-module (crates-io qs k- qsk-types) #:use-module (crates-io))

(define-public crate-qsk-types-0.1.0 (c (n "qsk-types") (v "0.1.0") (d (list (d (n "galvanic-assert") (r "~0.8") (d #t) (k 2)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "num") (r "~0.3") (d #t) (k 0)) (d (n "num-derive") (r "~0.3") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "strum") (r "~0.23") (d #t) (k 0)) (d (n "strum_macros") (r "~0.23") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)))) (h "18cqyab9w4yq9q1dq6ihqr1m8bwrrvb7aayi74m3q0nn881pwarr")))

