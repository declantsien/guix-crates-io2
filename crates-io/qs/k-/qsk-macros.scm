(define-module (crates-io qs k- qsk-macros) #:use-module (crates-io))

(define-public crate-qsk-macros-0.1.0 (c (n "qsk-macros") (v "0.1.0") (d (list (d (n "galvanic-assert") (r "~0.8") (d #t) (k 2)) (d (n "proc-macro-error") (r "~1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "qsk-types") (r "~0.1") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "~1.0") (d #t) (k 2)))) (h "1z0b6lcvzzabsb2l47rxy0irl23za7ynvp2qncbg1rjhxn0h6wqw")))

