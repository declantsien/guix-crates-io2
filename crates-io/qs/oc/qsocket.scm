(define-module (crates-io qs oc qsocket) #:use-module (crates-io))

(define-public crate-qsocket-0.1.0 (c (n "qsocket") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.60") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rustls") (r "^0.20.6") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "16xvdy0rwwfhgx4qfz6wfjkrl3i81gkhp36y70rb15km05f3ha7l")))

(define-public crate-qsocket-0.1.1 (c (n "qsocket") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.60") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rustls") (r "^0.20.6") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0r66a2acvfys21bsgdxy4bmpqbslp1rqwkbigya06cfp2m9qxmrq")))

(define-public crate-qsocket-0.1.2 (c (n "qsocket") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.60") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rustls") (r "^0.20.6") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0zz72wqq65dqzn7ryq1jd5hx2c5rcwv650x41kikfzll1f6krj86")))

(define-public crate-qsocket-0.1.3 (c (n "qsocket") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.60") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "chacha20") (r "^0.9.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "httparse") (r "^1.8.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.20.6") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "spake2") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0pvffz4n2s9bhmz42pfnv80lmqn6k6l4h9baib35r5zg5fnclncf")))

