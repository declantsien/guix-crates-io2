(define-module (crates-io qs tr qstring) #:use-module (crates-io))

(define-public crate-qstring-0.1.0 (c (n "qstring") (v "0.1.0") (d (list (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "00bh8hcff5jkqvkxbi6wj4m1d8nmik8ph6mlvczwpwrxdmnl6lzc")))

(define-public crate-qstring-0.1.1 (c (n "qstring") (v "0.1.1") (d (list (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "1br5dv91lnarf2rvmcf85ppfifndmkwzb1lfprk178j28b3jxynh")))

(define-public crate-qstring-0.1.2 (c (n "qstring") (v "0.1.2") (d (list (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "0n1flbz61hwl73avp5g1l0jawkh3hbzjzrphpykacbaj9x81vx4z")))

(define-public crate-qstring-0.2.0 (c (n "qstring") (v "0.2.0") (d (list (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "1y4gi8j0kfb0x60jz37y9wn2q2y6l647nb473m7w0lv6mk8bia78")))

(define-public crate-qstring-0.3.0 (c (n "qstring") (v "0.3.0") (d (list (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "05nlynd3mqr2wmwrn402vx19xsb606gaa433b68ga1ziz65f745r")))

(define-public crate-qstring-0.4.0 (c (n "qstring") (v "0.4.0") (d (list (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "1ccr5rxd0cq9672gpaidq3l4vvzypxs0r1d4ak8hg80x0kz9gzig")))

(define-public crate-qstring-0.5.0 (c (n "qstring") (v "0.5.0") (d (list (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "1b1aaw4qa53sh89lggj67rnam4l7d1mjx5pgji85h1a2jqs9nvin")))

(define-public crate-qstring-0.5.1 (c (n "qstring") (v "0.5.1") (d (list (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "0wz1ldjdq8r1qq86qgx8qva3idysi18mvrmzxbrqyk62k7rpgsvg")))

(define-public crate-qstring-0.5.2 (c (n "qstring") (v "0.5.2") (d (list (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "1m4mqjrq255gp3rsg32402bvyy6mdmg0fijbxywhvkamngczblxl")))

(define-public crate-qstring-0.5.3 (c (n "qstring") (v "0.5.3") (d (list (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "00qch2kiay6376bmsamwawbkd573k4kqasgi1a1m2dfwfll6frdh")))

(define-public crate-qstring-0.6.0 (c (n "qstring") (v "0.6.0") (d (list (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "0sc483h65nb36h7byfslwbs4m6742bcswfw8nmgy54valdbw0pjl")))

(define-public crate-qstring-0.7.0 (c (n "qstring") (v "0.7.0") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)))) (h "0njnmd96x4wb7hcva9g2k3mih59w6rn6hsxzrdkh76cjvz94fw16")))

(define-public crate-qstring-0.7.1 (c (n "qstring") (v "0.7.1") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)))) (h "0n0lrph40w9q5x1lvjgw1jwd4jd2h20ysdxjax1551377jqpfvri")))

(define-public crate-qstring-0.7.2 (c (n "qstring") (v "0.7.2") (d (list (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)))) (h "0khhcpwifis87r5chr4jiv3w1bkswcf226c0yjx809pzbzkglr6l")))

