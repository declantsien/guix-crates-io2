(define-module (crates-io qs he qshell) #:use-module (crates-io))

(define-public crate-qshell-0.1.0 (c (n "qshell") (v "0.1.0") (d (list (d (n "sh-macro") (r "^0.1.0") (d #t) (k 0)))) (h "15g67nddjbmdq4ldip37fgl5m3g27n0rfxfkpvnlnf1dqg3gqsha") (y #t)))

(define-public crate-qshell-0.1.1 (c (n "qshell") (v "0.1.1") (d (list (d (n "sh-macro") (r "^0.1.1") (d #t) (k 0)))) (h "1050488n3h9503anrdij6d6hvmxf5jxlmra7mdx0cfiqnncfvrx1") (y #t)))

