(define-module (crates-io qs or qsort-rs) #:use-module (crates-io))

(define-public crate-qsort-rs-0.1.0 (c (n "qsort-rs") (v "0.1.0") (h "18xvr0cp48x3c1p09z2cv57fjn5v5fpdqlfyqfx1cc8bcx74hkq9")))

(define-public crate-qsort-rs-0.1.1 (c (n "qsort-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ds9igjhxcr7rr5nnhz176dmnhjvc2zrs7iyjs9lvfmpzdgzvlib")))

