(define-module (crates-io qs v- qsv-stats) #:use-module (crates-io))

(define-public crate-qsv-stats-0.3.1 (c (n "qsv-stats") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0jvvjahj4fd1il4pqjplqq5m3bq33yl9w1m9kpimw8yh734i6rwb")))

(define-public crate-qsv-stats-0.3.2 (c (n "qsv-stats") (v "0.3.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14z91qixyvsl96j6rpjp1r198jj7wpd90wbwbva58hy5cavqbil8")))

(define-public crate-qsv-stats-0.3.3 (c (n "qsv-stats") (v "0.3.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "18szmvz4ssrckhzjic4ail2lnbjv8a3isdcs9j7w4sky67h1f9jh")))

(define-public crate-qsv-stats-0.3.4 (c (n "qsv-stats") (v "0.3.4") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "05jxy35cx68162xsaljpw5mkqmdh5v5gfsxmg13vk98wbz2rdjdb")))

(define-public crate-qsv-stats-0.3.5 (c (n "qsv-stats") (v "0.3.5") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1wjizlxfk888rickhr1ywwy10ll7480ks1ds27c0fvw0ps6vybwc")))

(define-public crate-qsv-stats-0.3.6 (c (n "qsv-stats") (v "0.3.6") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "082c12c6kblnj3d1wn2giyq48z0dpa7wiyazqqpdjkzrpvsgikzk")))

(define-public crate-qsv-stats-0.3.7 (c (n "qsv-stats") (v "0.3.7") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1ncdyldys04wfjzhzr5411q3k5kka0kar8qqh2r6rin8r3x6kqvx")))

(define-public crate-qsv-stats-0.4.0 (c (n "qsv-stats") (v "0.4.0") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ksd1k6q36a0p3kgyyy896sp2mjhsb11dsyv42l54apdi4mk5an8") (r "1.62.1")))

(define-public crate-qsv-stats-0.4.1 (c (n "qsv-stats") (v "0.4.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0ghyk5kc7jrpbbrmd9ifik6wj7bi6acws8i1rdi9vfs1xg1hshs2") (r "1.63")))

(define-public crate-qsv-stats-0.4.2 (c (n "qsv-stats") (v "0.4.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "00zm3hpiwnn4zr28cql5njgw63hm3xz5cwg4czr8939mlb2hyvs3") (r "1.65")))

(define-public crate-qsv-stats-0.4.3 (c (n "qsv-stats") (v "0.4.3") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0kyqnqdrr9ffhjrcvq7z8pwnhswnnp08ncs72gclx2ah0bwqs07i") (r "1.65")))

(define-public crate-qsv-stats-0.4.4 (c (n "qsv-stats") (v "0.4.4") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0c89gahkgysfbh8fyw0iv3y5nv82zvxcs6fg1ry874xyaasq333a") (r "1.65")))

(define-public crate-qsv-stats-0.4.5 (c (n "qsv-stats") (v "0.4.5") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0h6rrb74dgvs112szgmrkdfnvvjgsjw78pxw6vyi5x9v2gbcv3hn") (r "1.65")))

(define-public crate-qsv-stats-0.4.6 (c (n "qsv-stats") (v "0.4.6") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "09hvg9iifa96b1dah4v1zv53di0pgh7lq4cr2f556kz1qbxva90h") (r "1.65")))

(define-public crate-qsv-stats-0.5.0 (c (n "qsv-stats") (v "0.5.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1vxwganb7birh403mll65q7waklq6jbvpn7mpfiy58by49b7l9f0") (r "1.66")))

(define-public crate-qsv-stats-0.5.1 (c (n "qsv-stats") (v "0.5.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1pbp6fwjp6c7zy4pxpwk82yakc4fiz6vn45hpb63jm2gk9ni9b6y") (r "1.66")))

(define-public crate-qsv-stats-0.5.2 (c (n "qsv-stats") (v "0.5.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1s3qix8cr9ffkzz5fckz52psiqvq62151dm2l48bzwl34ykp7r3p") (r "1.65")))

(define-public crate-qsv-stats-0.5.3 (c (n "qsv-stats") (v "0.5.3") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "00glz278fhl8q4wxv7y54drjx4wxh2fqq6bibhl31wp9jm3hpbrx") (r "1.65")))

(define-public crate-qsv-stats-0.6.0 (c (n "qsv-stats") (v "0.6.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "193pfv43vrapww5q056wmksaginbgp09b0v5czg72i4h264p2nsf") (r "1.65")))

(define-public crate-qsv-stats-0.7.0 (c (n "qsv-stats") (v "0.7.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1xkn7v49ygg3gia1rw1vp0xnfffxwslcbpmn9p15daxid6509aq4") (r "1.65")))

(define-public crate-qsv-stats-0.8.0 (c (n "qsv-stats") (v "0.8.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mhqqkx2llvbg2xah46282a749lx08wgkhg2jy6vyyx8rvj72bqy")))

(define-public crate-qsv-stats-0.9.0 (c (n "qsv-stats") (v "0.9.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ivpa6v06dkk27ayz3pmqjcpi1y4xzhmq93a2cgmicf8dhdchdkd")))

(define-public crate-qsv-stats-0.10.0 (c (n "qsv-stats") (v "0.10.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04z5gakkp3j3nc5m01qjb4pjjxa036rzc1sz7jdq2cp7hmvhlj31")))

(define-public crate-qsv-stats-0.11.0 (c (n "qsv-stats") (v "0.11.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pffly8sp61q46xmzljfnvvbwgmysx9m3c2g7mcqxrmsc5wwa085")))

(define-public crate-qsv-stats-0.12.0 (c (n "qsv-stats") (v "0.12.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b2zfpyh4ldmqavhn4jl0p50ml4hk59q14k6a42wlc2dxj0sns2b") (r "1.73.0")))

(define-public crate-qsv-stats-0.13.0 (c (n "qsv-stats") (v "0.13.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19p4m4hwi9lk3ilj54xr02zyx1p6vnrqwmf6fksv2f8j31qlxd2p") (r "1.76.0")))

(define-public crate-qsv-stats-0.14.0 (c (n "qsv-stats") (v "0.14.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z6r39zqmv7afspkvmfp06s75wiwi7zswzfyzjf6h3h5kqh8nsna") (r "1.77.1")))

(define-public crate-qsv-stats-0.15.0 (c (n "qsv-stats") (v "0.15.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00ggxqkxbrp4afdwa38qznh9vwwyykjw2dszrjz8hsl7f0dr4xp8") (r "1.77.2")))

(define-public crate-qsv-stats-0.16.0 (c (n "qsv-stats") (v "0.16.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qjwbgi56riylvjiqj3mlfprziba43z48l08jb8qp6q2acxn0rj1") (r "1.77.2")))

(define-public crate-qsv-stats-0.17.0 (c (n "qsv-stats") (v "0.17.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0292wn3dzwzb6pll6cv6v4fbwr6sl66pzmhkv7rd2xvmia9rqaj8") (r "1.78")))

(define-public crate-qsv-stats-0.17.1 (c (n "qsv-stats") (v "0.17.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0szfs0n4hn27sqisgqlcy1cv1gvil7yplchr28xad1qxvkhk0ip2") (r "1.78")))

(define-public crate-qsv-stats-0.17.2 (c (n "qsv-stats") (v "0.17.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "05rsmhmg5wvl1ci1m7052l9dv4f7rf6xshrljv6l56prrjlslkhx") (r "1.78")))

(define-public crate-qsv-stats-0.18.0 (c (n "qsv-stats") (v "0.18.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mgf8523qmf547m6wnf2r1a108f5afl28n2rxm4igqd17dwx3xzk") (r "1.78")))

