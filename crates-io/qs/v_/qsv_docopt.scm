(define-module (crates-io qs v_ qsv_docopt) #:use-module (crates-io))

(define-public crate-qsv_docopt-1.2.0 (c (n "qsv_docopt") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (f (quote ("std" "unicode" "perf"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "09bh4m6yrmpj3i9qqyacp6x6rnslqxz414svcrzpjpqbfgf1lpr5")))

(define-public crate-qsv_docopt-1.2.1 (c (n "qsv_docopt") (v "1.2.1") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "1h19ih8nc3bw9xrc7jkjkpb62rplympjbi7px5vs691bpps01860")))

(define-public crate-qsv_docopt-1.2.2 (c (n "qsv_docopt") (v "1.2.2") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "0gnd4cc3caby3wrbfyiybmqawpj365ki8vh1c2vs372axcvrcrn8")))

(define-public crate-qsv_docopt-1.3.0 (c (n "qsv_docopt") (v "1.3.0") (d (list (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "1zz3si8gli76qkkqxzh89amsazdf57h3dd2lqvz9a5q6pln3sc8j") (r "1.70.0")))

(define-public crate-qsv_docopt-1.4.0 (c (n "qsv_docopt") (v "1.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "0md3d00dswkdyp6mbwp2fdj6jrndvqk4zqpp5f4vra03c0ljmicx") (r "1.71.0")))

(define-public crate-qsv_docopt-1.5.0 (c (n "qsv_docopt") (v "1.5.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "1k6rmbkl77nz297n9p1qk037bpbhkn16qka36dg7zkkz64ra05sv") (r "1.71.0")))

(define-public crate-qsv_docopt-1.6.0 (c (n "qsv_docopt") (v "1.6.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.11") (d #t) (k 0)))) (h "0klmax6s22a5f0lla3hlczx4gxc466yiazmiq9gy6bwas5b1rj78") (r "1.75.0")))

(define-public crate-qsv_docopt-1.7.0 (c (n "qsv_docopt") (v "1.7.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.11") (d #t) (k 0)))) (h "15hbvhfmk3f5a9vgf0x5hlhh8vq0sck7mc9lpk2paxaj17sdm8qy") (r "1.77.0")))

