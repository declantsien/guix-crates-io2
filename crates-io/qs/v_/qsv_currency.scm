(define-module (crates-io qs v_ qsv_currency) #:use-module (crates-io))

(define-public crate-qsv_currency-0.5.0 (c (n "qsv_currency") (v "0.5.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1wyc33a05qh7q640f3wi8gcchdkp0hwim3vdfaj6kj7dpywlzhvf")))

(define-public crate-qsv_currency-0.6.0 (c (n "qsv_currency") (v "0.6.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "17nawk8lag3cs22i3qnn0l2dbwmjm0wsrbl8ycbl4allnwr30jz3")))

