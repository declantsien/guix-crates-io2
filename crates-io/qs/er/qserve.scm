(define-module (crates-io qs er qserve) #:use-module (crates-io))

(define-public crate-qserve-0.1.0 (c (n "qserve") (v "0.1.0") (h "1bndc0111wi40mh5zwr0kwridv0zhfn697c9mykzbd26ixhg0wmp")))

(define-public crate-qserve-0.1.1 (c (n "qserve") (v "0.1.1") (h "1k8kw6jfma3r5l8sppzvjgrp9yc1w0a4grcmyv0wjcs2ng6wdhg2")))

(define-public crate-qserve-0.2.0 (c (n "qserve") (v "0.2.0") (h "1sa5xx15vakrvapxhfsdwqs43whp306j9nc1a23amdjawcrbvfv4")))

(define-public crate-qserve-0.2.1 (c (n "qserve") (v "0.2.1") (h "158mbc6s7arvfqanxxjg022qxrcyr0clqqjspk9ghgz31f30frr2")))

(define-public crate-qserve-0.2.2 (c (n "qserve") (v "0.2.2") (h "0i5jv7fixzgh2xbf3d8xinrkc272vw3dyvk4mdi55jhkc8zx74c3")))

(define-public crate-qserve-0.3.0 (c (n "qserve") (v "0.3.0") (h "1pq7v7bx3kwwj3r25wjlqc10kxrp7qka735ky991bgcrrh5djrqj")))

(define-public crate-qserve-1.0.0 (c (n "qserve") (v "1.0.0") (h "0yik55f3yxi8r9p514004xwklpy1hy81rcnc98qpvizvnww8j4q9")))

(define-public crate-qserve-1.0.1 (c (n "qserve") (v "1.0.1") (h "0q9m6dzkb5lafm49s4fmpim40p60i1wqbiidw5f2s426lx10brbg")))

(define-public crate-qserve-1.1.0 (c (n "qserve") (v "1.1.0") (h "0a4znh1cp6pcs43k9hfx0jbrs84ggg764qlakqcwzbljb43bmxb9")))

(define-public crate-qserve-1.2.0 (c (n "qserve") (v "1.2.0") (h "0sdmcq2jbbwwlsb4jiwr3z58m5f8iccmq3kwvynkjnflm6ivzqm9")))

