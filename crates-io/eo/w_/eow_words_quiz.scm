(define-module (crates-io eo w_ eow_words_quiz) #:use-module (crates-io))

(define-public crate-eow_words_quiz-0.1.0 (c (n "eow_words_quiz") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.4") (d #t) (k 0)))) (h "02pxmzbb19n6432qvyqx3k9y6fjbb6n33f966jl76jciymrmvvkm") (y #t)))

(define-public crate-eow_words_quiz-0.1.1 (c (n "eow_words_quiz") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.4") (d #t) (k 0)))) (h "0sg7sb4cvrgl5057hvgjz0ndvf7vmsl24s9g4zfs9clg6lz33lcn") (y #t)))

(define-public crate-eow_words_quiz-0.1.2 (c (n "eow_words_quiz") (v "0.1.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.4") (d #t) (k 0)))) (h "0w92a5r1ymxccrg588sj73mkpj4b1v680d9j16rhsv2zc19rfbn7") (y #t)))

(define-public crate-eow_words_quiz-0.1.3 (c (n "eow_words_quiz") (v "0.1.3") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.4") (d #t) (k 0)))) (h "1011innk7lcyqmqw4yjvggpy1ynqw3rvv5xmx82ydc705c1x7n28")))

