(define-module (crates-io eo si eosio_bytes) #:use-module (crates-io))

(define-public crate-eosio_bytes-0.2.1 (c (n "eosio_bytes") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "eosio_bytes_derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1bqy78c2kwiwjnawwya037s8p5rdq3af0j2bisq4n1h729zb0dng") (f (quote (("internal-use-only-root-path-is-eosio" "eosio_bytes_derive/internal-use-only-root-path-is-eosio") ("derive" "eosio_bytes_derive") ("default" "derive"))))))

