(define-module (crates-io eo si eosio_bytes_derive) #:use-module (crates-io))

(define-public crate-eosio_bytes_derive-0.2.0 (c (n "eosio_bytes_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1s4n71hgnh778bpxpwg6bywpi62ih2jlnzbdi6lzyr8y9gkw0swq") (f (quote (("internal-use-only-root-path-is-eosio") ("default"))))))

(define-public crate-eosio_bytes_derive-0.2.1 (c (n "eosio_bytes_derive") (v "0.2.1") (d (list (d (n "eosio_bytes") (r "^0.2") (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1nmp0q8vl403jmjwv37i0i11mci69chfrsx5b4q4kd3xz50bixz9") (f (quote (("internal-use-only-root-path-is-eosio") ("default"))))))

