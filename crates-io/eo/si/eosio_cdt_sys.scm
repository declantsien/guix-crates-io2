(define-module (crates-io eo si eosio_cdt_sys) #:use-module (crates-io))

(define-public crate-eosio_cdt_sys-0.2.1 (c (n "eosio_cdt_sys") (v "0.2.1") (h "00g8372zq6dxiq3kdjnxriwzwfra94shvr5ni05a02qj60idgh99")))

(define-public crate-eosio_cdt_sys-0.3.0 (c (n "eosio_cdt_sys") (v "0.3.0") (h "1cdkx4sn6qyr9n6civd9vq526j15abzywbhya8yqdxip0f5dvzf8") (f (quote (("mock") ("default"))))))

