(define-module (crates-io eo si eosio_numstr_macros) #:use-module (crates-io))

(define-public crate-eosio_numstr_macros-0.2.1 (c (n "eosio_numstr_macros") (v "0.2.1") (d (list (d (n "eosio_numstr_macros_impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro-nested") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1lyfnwcjdp25hb5v0hvr4a9gipwi77fmf4h1f19wxk60jbww38z7")))

