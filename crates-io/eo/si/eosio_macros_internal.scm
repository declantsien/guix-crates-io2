(define-module (crates-io eo si eosio_macros_internal) #:use-module (crates-io))

(define-public crate-eosio_macros_internal-0.3.0 (c (n "eosio_macros_internal") (v "0.3.0") (d (list (d (n "eosio") (r ">= 0.2, <= 0.3") (d #t) (k 2)) (d (n "eosio_numstr") (r "^0.3") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1mvmpfj8v6ly0fdzcrii9ql1swvjcyzr5h6qyvjk7nk892yndpir")))

(define-public crate-eosio_macros_internal-0.3.1 (c (n "eosio_macros_internal") (v "0.3.1") (d (list (d (n "eosio") (r ">= 0.2, <= 0.3") (d #t) (k 2)) (d (n "eosio_numstr") (r "^0.3") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "10kbp3lqfpg06swy261jhqa5nfz0b0g7krshgqyqqgg44s96ha63")))

