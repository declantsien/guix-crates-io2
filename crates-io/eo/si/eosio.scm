(define-module (crates-io eo si eosio) #:use-module (crates-io))

(define-public crate-eosio-0.1.0 (c (n "eosio") (v "0.1.0") (d (list (d (n "eosio_macros") (r "^0.1") (d #t) (k 0)) (d (n "eosio_sys") (r "^0.1") (d #t) (k 0)))) (h "14higffcjqyf3dv4fdnv2n5lk34rwyqxx13i434p4awppbyzigaj") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-eosio-0.2.0 (c (n "eosio") (v "0.2.0") (d (list (d (n "eosio_macros") (r "^0.2") (f (quote ("internal"))) (d #t) (k 0)) (d (n "eosio_sys") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03f2j0wnhm8lwl54zn97g6851lny5ry4r3y90xdv6llm0y6lwvyc") (f (quote (("stdweb") ("std") ("default" "std" "contract") ("contract") ("alloc"))))))

(define-public crate-eosio-0.3.0 (c (n "eosio") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "eosio_macros") (r "^0.3") (d #t) (k 0)) (d (n "eosio_numstr") (r "^0.3") (d #t) (k 0)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "0pnyvbzd0vyk6a7g3zwdl9p47nnqdmls8rhjl3d5pbai6b9cnpnh")))

(define-public crate-eosio-0.3.1 (c (n "eosio") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "eosio_macros") (r "^0.3.1") (d #t) (k 0)) (d (n "eosio_numstr") (r "^0.3.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "1sd52pkv1ifadzjqvlb7ssysqpkkhfnm6pyhdh6iidygvbmywb7z")))

