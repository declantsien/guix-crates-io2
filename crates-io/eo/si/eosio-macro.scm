(define-module (crates-io eo si eosio-macro) #:use-module (crates-io))

(define-public crate-eosio-macro-0.1.0 (c (n "eosio-macro") (v "0.1.0") (d (list (d (n "eosio-codegen") (r "^0.1.0") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0bp4n8l7cca90k6qmk3xvnnmbpv5myyds33i6q6yzfzr6k2ryxxn") (f (quote (("std" "eosio-codegen/std") ("default" "std"))))))

(define-public crate-eosio-macro-0.1.1 (c (n "eosio-macro") (v "0.1.1") (d (list (d (n "eosio-codegen") (r "^0.1.1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1zhfl217ws8gx24bb4mh8kzf4wwblwrm99jmh2bwwal9zrh9iw2x") (f (quote (("std" "eosio-codegen/std") ("default" "std"))))))

(define-public crate-eosio-macro-0.1.2 (c (n "eosio-macro") (v "0.1.2") (d (list (d (n "eosio-codegen") (r "^0.1.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0s9g3jd7cdl0lsrz1rrzk3iridbjzgaa1rzfl6jnvrshpznvhpq7") (f (quote (("std" "eosio-codegen/std") ("default" "std"))))))

(define-public crate-eosio-macro-0.2.0 (c (n "eosio-macro") (v "0.2.0") (d (list (d (n "eosio-codegen") (r "^0.2.0") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1rp9byzqfj7i37in5fj1xvs4bhs37cf5r6x9g7mbzvz5kv3pna7z") (f (quote (("std" "eosio-codegen/std") ("default" "std"))))))

(define-public crate-eosio-macro-0.2.2 (c (n "eosio-macro") (v "0.2.2") (d (list (d (n "eosio-codegen") (r "^0.2.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "156srrnshkmslrm01qr2p3kiwxn53qas9z086ardh7nasd7bp484") (f (quote (("std" "eosio-codegen/std") ("default" "std"))))))

(define-public crate-eosio-macro-0.2.3 (c (n "eosio-macro") (v "0.2.3") (d (list (d (n "eosio-codegen") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0z8fz2pg81abnqsdydd9wxbisgpy51dipbrqx327pf33z6spzrcw") (f (quote (("std" "eosio-codegen/std") ("default" "std"))))))

(define-public crate-eosio-macro-0.2.4 (c (n "eosio-macro") (v "0.2.4") (d (list (d (n "eosio-codegen") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1z6sg9zi40zgd5sp2xxpfa1s2j3npr29crylbdq5r0ww7bv23nll") (f (quote (("std" "eosio-codegen/std") ("default" "std"))))))

(define-public crate-eosio-macro-0.2.6 (c (n "eosio-macro") (v "0.2.6") (d (list (d (n "eosio-codegen") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0s1i3x634ac6mwa20j622pr6dkgdrsnzw1k1mpyypwkqk2sq8f2d") (f (quote (("std" "eosio-codegen/std") ("default" "std"))))))

