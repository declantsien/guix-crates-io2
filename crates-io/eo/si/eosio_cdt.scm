(define-module (crates-io eo si eosio_cdt) #:use-module (crates-io))

(define-public crate-eosio_cdt-0.3.0 (c (n "eosio_cdt") (v "0.3.0") (d (list (d (n "eosio") (r "^0.3") (d #t) (k 0)) (d (n "eosio_cdt_sys") (r "^0.3") (d #t) (k 0)))) (h "1rsl9l10a0nfz48l6mw10f44si2an02s9mx7m84k0hanqqgy42d6")))

(define-public crate-eosio_cdt-0.3.1 (c (n "eosio_cdt") (v "0.3.1") (d (list (d (n "eosio") (r "^0.3.1") (d #t) (k 0)) (d (n "eosio_cdt_sys") (r "^0.3") (d #t) (k 0)))) (h "1myyfzqxpznylfwgjkqaw366x33alhd93xsqlq6gj5v0hbmfjpw3")))

