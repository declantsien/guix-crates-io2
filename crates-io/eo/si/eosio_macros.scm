(define-module (crates-io eo si eosio_macros) #:use-module (crates-io))

(define-public crate-eosio_macros-0.1.0 (c (n "eosio_macros") (v "0.1.0") (d (list (d (n "eosio_sys") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0n86icb02rbji52zxssl9fw74nchakv71r31w3zrmph2ffy4nhsh")))

(define-public crate-eosio_macros-0.2.0 (c (n "eosio_macros") (v "0.2.0") (d (list (d (n "eosio_sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "18ii31hfx2kzycm0pggsnqz5v13xnngjjq9r4pa5h6sx9ql1m7ii") (f (quote (("internal"))))))

(define-public crate-eosio_macros-0.3.0 (c (n "eosio_macros") (v "0.3.0") (d (list (d (n "eosio_macros_internal") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ryvs3j2qn3mfg7db5j9cwp37n4y7z5kl7gbghv2nc909vhsyg0x")))

(define-public crate-eosio_macros-0.3.1 (c (n "eosio_macros") (v "0.3.1") (d (list (d (n "eosio_macros_internal") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "117kkz92gcq65rxl5cy1zq0zvmpipd4mv2vnsfxynp2d0k529sg4")))

