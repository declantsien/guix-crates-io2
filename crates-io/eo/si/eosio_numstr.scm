(define-module (crates-io eo si eosio_numstr) #:use-module (crates-io))

(define-public crate-eosio_numstr-0.2.1 (c (n "eosio_numstr") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "110i1hnm778j1miv8gnmq65jwcg5fy39lkc9vbxz8js1nvjj1gvr")))

(define-public crate-eosio_numstr-0.2.2 (c (n "eosio_numstr") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "11yfixy148n62l91lzf2p1irfq8475723b0a67i37if95a0di0ln")))

(define-public crate-eosio_numstr-0.3.0 (c (n "eosio_numstr") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "0chr6xfn399f38djzfhi4g17qggi9an3pxdkx3bhr4nngnd2bd7b")))

