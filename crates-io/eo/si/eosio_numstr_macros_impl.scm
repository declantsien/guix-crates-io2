(define-module (crates-io eo si eosio_numstr_macros_impl) #:use-module (crates-io))

(define-public crate-eosio_numstr_macros_impl-0.2.1 (c (n "eosio_numstr_macros_impl") (v "0.2.1") (d (list (d (n "eosio_numstr") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1hjx3r415jsxxmyschbv7baix0ajrbya1fim138g83z4chngky2g")))

