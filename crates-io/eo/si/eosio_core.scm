(define-module (crates-io eo si eosio_core) #:use-module (crates-io))

(define-public crate-eosio_core-0.2.1 (c (n "eosio_core") (v "0.2.1") (d (list (d (n "eosio_bytes") (r "^0.2") (d #t) (k 0)) (d (n "eosio_numstr") (r "^0.2") (d #t) (k 0)) (d (n "eosio_numstr_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nv5nffqh5f8gb11157ka4xpb3rl1iz0ms4wm0hv8bs88m04d1d4")))

