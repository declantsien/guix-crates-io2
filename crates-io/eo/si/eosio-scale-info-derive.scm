(define-module (crates-io eo si eosio-scale-info-derive) #:use-module (crates-io))

(define-public crate-eosio-scale-info-derive-2.1.2 (c (n "eosio-scale-info-derive") (v "2.1.2") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0f0sjb7xh31p4wgdf0ycgxbafx4i45hjxq4k068lgpqy0zvvv8r1") (f (quote (("docs") ("default" "docs")))) (r "1.56.1")))

(define-public crate-eosio-scale-info-derive-2.1.3 (c (n "eosio-scale-info-derive") (v "2.1.3") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0xwhjn4aj7i3h0n9fwrki6g6yyicxkg794m96r7wzsabdgdpaanw") (f (quote (("docs") ("default" "docs")))) (r "1.56.1")))

