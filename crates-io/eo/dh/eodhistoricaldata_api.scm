(define-module (crates-io eo dh eodhistoricaldata_api) #:use-module (crates-io))

(define-public crate-eodhistoricaldata_api-0.1.0 (c (n "eodhistoricaldata_api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04j4ypyi9qm3lkdjhpc5wzhi5v00qsxc9k4bnzlrgimakxmllhjh")))

(define-public crate-eodhistoricaldata_api-0.1.1 (c (n "eodhistoricaldata_api") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bg35ws38m3rlni6b94dmgcfw4jcvcfvhk3mnp6038wmd5gabjy4")))

(define-public crate-eodhistoricaldata_api-0.2.0 (c (n "eodhistoricaldata_api") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x1f8zzmm2wp5qwjxny141fl5jl2cvljw023g8mm2rl9899fhpwm")))

(define-public crate-eodhistoricaldata_api-0.3.0 (c (n "eodhistoricaldata_api") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)))) (h "1llbvw5p2lj7fs9nwa5bpha0gpyy0ah1myrvny8kc86k0r2q3675")))

(define-public crate-eodhistoricaldata_api-0.3.1 (c (n "eodhistoricaldata_api") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.3") (d #t) (k 2)))) (h "04gl6gpzs371z761saljagcq7dkj5ar2hmk0l6c7av77sk5nldjk") (y #t)))

(define-public crate-eodhistoricaldata_api-0.3.2 (c (n "eodhistoricaldata_api") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-compat-02") (r "^0.1") (d #t) (k 0)) (d (n "tokio-test") (r "^0.3") (d #t) (k 2)))) (h "0dc8i6k02fc686212fcarg8xf1iyq33i5d00hxv5wbs0g3aap6y1")))

(define-public crate-eodhistoricaldata_api-0.3.4 (c (n "eodhistoricaldata_api") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-compat-02") (r "^0.2") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0xx3cypqairdm13qmizmkmczyij95hy05y82abr7vr17533wmy5c")))

(define-public crate-eodhistoricaldata_api-0.3.5 (c (n "eodhistoricaldata_api") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-compat-02") (r "^0.2") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1j3vwi816m4wkkqr9zhddslpqywfwn3aadvjrmv1rrd8c9rj37my")))

(define-public crate-eodhistoricaldata_api-0.4.0 (c (n "eodhistoricaldata_api") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio-compat-02") (r "^0.2") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1zyfbyzcabb3vb37v5hv2gfk9b5kwcfnvzr5vv65sy683m0vrhk7")))

