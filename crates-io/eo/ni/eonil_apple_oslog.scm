(define-module (crates-io eo ni eonil_apple_oslog) #:use-module (crates-io))

(define-public crate-eonil_apple_oslog-0.1.0 (c (n "eonil_apple_oslog") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)))) (h "0l35frjkrhqgk39k1z5ck1zikzk9h20s0jagjqrk7n8z5vvhmlq7")))

(define-public crate-eonil_apple_oslog-0.1.1 (c (n "eonil_apple_oslog") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)))) (h "1vpj9zjd8jf6q9sx2l84l2k9bq7wfdbjilfkwaqs3p4disvf9fpm")))

(define-public crate-eonil_apple_oslog-0.1.2 (c (n "eonil_apple_oslog") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)))) (h "1vj4czwqyfiy8mxv0p7d0klih9ql0k7nhz17hn60plvghlr86p48")))

