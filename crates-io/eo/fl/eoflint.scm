(define-module (crates-io eo fl eoflint) #:use-module (crates-io))

(define-public crate-eoflint-0.1.0 (c (n "eoflint") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "06gs3g1bkm18nkksi6bbh6pqybvhyvpz9k1js4wyg05g0v5cbx57")))

(define-public crate-eoflint-0.1.1 (c (n "eoflint") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "15cj2nf36xvi6nkjwzrdzp083wjsgyr2hb0x5jjaxn9rhxp1gfr0")))

(define-public crate-eoflint-0.2.0 (c (n "eoflint") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1llphhavkvs3ayx4cdih69g54mx53dpwgp6lbvdn9skvf1qy5w9c")))

