(define-module (crates-io eo pl eoplus) #:use-module (crates-io))

(define-public crate-eoplus-1.0.0-RC1 (c (n "eoplus") (v "1.0.0-RC1") (d (list (d (n "antlr-rust") (r "=0.3.0-beta") (d #t) (k 0)))) (h "0d2n7ppv34l3nzzcnw263awsj0rldyd7hb3fvy55jp9clfxwb2kg") (r "1.61.0")))

