(define-module (crates-io eo #{-i}# eo-identifiers) #:use-module (crates-io))

(define-public crate-eo-identifiers-0.1.0 (c (n "eo-identifiers") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1apmvf3n1zq31nr35kzjyvl0p0qy3rl30w0hxw407qjnv6b18vgp") (s 2) (e (quote (("serde" "dep:serde" "chrono/serde"))))))

(define-public crate-eo-identifiers-0.1.1 (c (n "eo-identifiers") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "11gdssl62xs39l1q8rf0jvygwa3gjzkkscqaq4aag33fi0ab7lbg") (s 2) (e (quote (("serde" "dep:serde" "chrono/serde"))))))

