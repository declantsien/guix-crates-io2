(define-module (crates-io lv #{2r}# lv2rs) #:use-module (crates-io))

(define-public crate-lv2rs-0.1.0 (c (n "lv2rs") (v "0.1.0") (d (list (d (n "lv2rs-core") (r "^0.1.1") (d #t) (k 0)))) (h "0gxyy06z8p0z18zyrihrd6zc294wdqqk1qdh3sli4qbl41n9p70x")))

(define-public crate-lv2rs-0.1.1 (c (n "lv2rs") (v "0.1.1") (d (list (d (n "lv2rs-core") (r "^0.1.2") (d #t) (k 0)))) (h "1xmliw7ydg58hnd3s54yl7lx1aad4l4q4zpk3sj3df3hw2cv7pvb")))

(define-public crate-lv2rs-0.2.0 (c (n "lv2rs") (v "0.2.0") (d (list (d (n "lv2rs-core") (r "^0.2.0") (d #t) (k 0)) (d (n "lv2rs-urid") (r "^0.1.0") (d #t) (k 0)))) (h "0nap6h5zdpbxrbkrm62y4r149nbhg0kb1w8bygxskc7dqj0hqraw")))

(define-public crate-lv2rs-0.2.1 (c (n "lv2rs") (v "0.2.1") (d (list (d (n "lv2rs-core") (r "^0.2.0") (d #t) (k 0)) (d (n "lv2rs-urid") (r "^0.1.0") (d #t) (k 0)))) (h "12fr58ma0ynlfdf7llahnz71rq43y171zgrpxbj8jxy0ciq06vp9")))

(define-public crate-lv2rs-0.3.0 (c (n "lv2rs") (v "0.3.0") (d (list (d (n "lv2rs-atom") (r "^0.1.0") (d #t) (k 0)) (d (n "lv2rs-core") (r "^0.3.0") (d #t) (k 0)) (d (n "lv2rs-midi") (r "^0.1.0") (d #t) (k 0)) (d (n "lv2rs-urid") (r "^0.2.0") (d #t) (k 0)))) (h "0sm52w8fddp8bzdd6m5mp3hv23rr0kw1fh105i3x74gbidg21km9")))

(define-public crate-lv2rs-0.3.1 (c (n "lv2rs") (v "0.3.1") (d (list (d (n "lv2rs-atom") (r "^0.1.0") (d #t) (k 0)) (d (n "lv2rs-core") (r "^0.3.0") (d #t) (k 0)) (d (n "lv2rs-midi") (r "^0.1.0") (d #t) (k 0)) (d (n "lv2rs-urid") (r "^0.2.0") (d #t) (k 0)))) (h "02dkcsfvzqpn72pj5b2ahm4kpqllnkj4bvjialcrzl4sc1qqyi8c")))

(define-public crate-lv2rs-0.3.2 (c (n "lv2rs") (v "0.3.2") (d (list (d (n "lv2rs-atom") (r "^0.1.0") (d #t) (k 0)) (d (n "lv2rs-core") (r "^0.3.0") (d #t) (k 0)) (d (n "lv2rs-midi") (r "^0.1.0") (d #t) (k 0)) (d (n "lv2rs-urid") (r "^0.2.0") (d #t) (k 0)))) (h "0cq6mjwr6sjxhrviv1a38snwmdal2ghhrglahj98906lq9bw2ka8")))

