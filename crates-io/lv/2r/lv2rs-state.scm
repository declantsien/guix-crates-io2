(define-module (crates-io lv #{2r}# lv2rs-state) #:use-module (crates-io))

(define-public crate-lv2rs-state-0.0.0 (c (n "lv2rs-state") (v "0.0.0") (h "0fv2mw1q7mk56kg3900wj091rfbcy67js8vflhlks3xyra4z8ch7")))

(define-public crate-lv2rs-state-0.0.1 (c (n "lv2rs-state") (v "0.0.1") (h "0i4p3yjik9kgx97qzxqjh34gshkj5wm7fk435hgns90wmm63i473")))

