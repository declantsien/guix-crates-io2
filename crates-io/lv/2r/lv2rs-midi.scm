(define-module (crates-io lv #{2r}# lv2rs-midi) #:use-module (crates-io))

(define-public crate-lv2rs-midi-0.0.0 (c (n "lv2rs-midi") (v "0.0.0") (h "09j7iq7w325ivb58xzyis1v70daqszh1zz2rhhhgjak6v243353k")))

(define-public crate-lv2rs-midi-0.0.1 (c (n "lv2rs-midi") (v "0.0.1") (h "0g6c0pqg5hdxi4szgswz7zvgd1js8p9qv8sph08hnm2bs3hdah5m")))

(define-public crate-lv2rs-midi-0.1.0 (c (n "lv2rs-midi") (v "0.1.0") (d (list (d (n "lv2rs-atom") (r "^0.1.0") (d #t) (k 0)) (d (n "lv2rs-urid") (r "^0.2.0") (d #t) (k 0)) (d (n "ux") (r "^0.1.3") (d #t) (k 0)))) (h "0v6b8cmbdfsk5dw3q6vl82fmwcb05pjafkbd70q56fadmdgig1in")))

(define-public crate-lv2rs-midi-0.1.2 (c (n "lv2rs-midi") (v "0.1.2") (d (list (d (n "lv2rs-atom") (r "^0.1.0") (d #t) (k 0)) (d (n "lv2rs-urid") (r "^0.2.0") (d #t) (k 0)) (d (n "ux") (r "^0.1.3") (d #t) (k 0)))) (h "0k8qkb43aswccnnzqrh5dxpp3kqrjqg67rcpb46m7q33szh8pw8c")))

