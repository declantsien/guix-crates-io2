(define-module (crates-io lv #{2r}# lv2rs-units) #:use-module (crates-io))

(define-public crate-lv2rs-units-0.0.0 (c (n "lv2rs-units") (v "0.0.0") (h "0zgb72x1h8fqjp4ahswmlg69fbilnfx0p5xq05v0inlm9r16arj0")))

(define-public crate-lv2rs-units-0.0.1 (c (n "lv2rs-units") (v "0.0.1") (h "120yls3lqn653nvq6ccq66br5jw99cdlxvvx3xc7a6ibf0glf0g2")))

