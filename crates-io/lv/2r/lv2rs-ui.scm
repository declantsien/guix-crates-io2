(define-module (crates-io lv #{2r}# lv2rs-ui) #:use-module (crates-io))

(define-public crate-lv2rs-ui-0.0.0 (c (n "lv2rs-ui") (v "0.0.0") (h "12giyc7vvij355mj6nicn77mfnyvkld6v55wp2kfz0jca2w3pfpw")))

(define-public crate-lv2rs-ui-0.0.1 (c (n "lv2rs-ui") (v "0.0.1") (h "03xmrmjajiarw9f6gr204jvbaswyy2mkapmx0bwz2vd3zr773nf1")))

