(define-module (crates-io lv #{2r}# lv2rs-urid) #:use-module (crates-io))

(define-public crate-lv2rs-urid-0.0.0 (c (n "lv2rs-urid") (v "0.0.0") (h "1hxx3vmx8dlaqbdnr289ddw2fl16d7cbc4w0nwxa3ci8jbjm29v2")))

(define-public crate-lv2rs-urid-0.1.0 (c (n "lv2rs-urid") (v "0.1.0") (d (list (d (n "lv2rs-core") (r "^0.2.0") (d #t) (k 0)))) (h "113rvsac4l6b733fxpkb1k3spx1z9fzwm4phx9l41xicn9mw8g0v")))

(define-public crate-lv2rs-urid-0.2.0 (c (n "lv2rs-urid") (v "0.2.0") (d (list (d (n "lv2rs-core") (r "^0.3.0") (d #t) (k 0)))) (h "15ilba3abxm8f7d0mpm6rlzhfxa28z2ab893wb23fdd7gn8il6n4")))

(define-public crate-lv2rs-urid-0.2.1 (c (n "lv2rs-urid") (v "0.2.1") (d (list (d (n "lv2rs-core") (r "^0.3.0") (d #t) (k 0)))) (h "11rsfmgizcd83miq0p1q251cd07kvn9lai257rni1z2xy83fbj3m")))

(define-public crate-lv2rs-urid-0.2.2 (c (n "lv2rs-urid") (v "0.2.2") (d (list (d (n "lv2rs-core") (r "^0.3.0") (d #t) (k 0)))) (h "0hlqdnc5qhcns8ac84yxhn3dix56hmhqz3dkk9cd253bh66f2jk5")))

