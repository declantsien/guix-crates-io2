(define-module (crates-io lv #{2r}# lv2rs-time) #:use-module (crates-io))

(define-public crate-lv2rs-time-0.0.0 (c (n "lv2rs-time") (v "0.0.0") (h "0066g5fhhv9gvwn9gvh6fwiggb8jxlcmzbb7j5wgnvrv8xip624p")))

(define-public crate-lv2rs-time-0.0.1 (c (n "lv2rs-time") (v "0.0.1") (h "10hjqaily51lj5ikils41pczj389xf43lxiwk7ripsh2cwsbln9s")))

