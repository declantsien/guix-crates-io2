(define-module (crates-io lv #{2r}# lv2rs-resize) #:use-module (crates-io))

(define-public crate-lv2rs-resize-0.0.0 (c (n "lv2rs-resize") (v "0.0.0") (h "0rh60428r6zyzaly8r53idb0y3fvvcr90ks5rbad41ljbajrsca6")))

(define-public crate-lv2rs-resize-0.0.1 (c (n "lv2rs-resize") (v "0.0.1") (h "0wi3i0hl6q6z33frf5g8rqnqk4bn4h0kq1n7ryaynzbxi2vhvxfd")))

