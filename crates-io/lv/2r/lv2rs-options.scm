(define-module (crates-io lv #{2r}# lv2rs-options) #:use-module (crates-io))

(define-public crate-lv2rs-options-0.0.0 (c (n "lv2rs-options") (v "0.0.0") (h "0fjc8labp669y41762blmgq4h5fvmxlqffh20562yp1qv602l0h0")))

(define-public crate-lv2rs-options-0.0.1 (c (n "lv2rs-options") (v "0.0.1") (h "1mvbl4p5lxnx16yd72w6v0hjmp69yxllyjqb0mf5lj9qvdmypdd4")))

