(define-module (crates-io lv #{2r}# lv2rs-core) #:use-module (crates-io))

(define-public crate-lv2rs-core-0.1.0 (c (n "lv2rs-core") (v "0.1.0") (h "133nfzhcc6caj6yxg19a8bfdhm3iqb4amfw5yknnkalhy87d0hfj")))

(define-public crate-lv2rs-core-0.1.1 (c (n "lv2rs-core") (v "0.1.1") (h "0rfc15vj8s4b6k7jffh4clghcaaf6kqnghxjdbwvyaav8fzna8ak")))

(define-public crate-lv2rs-core-0.1.2 (c (n "lv2rs-core") (v "0.1.2") (h "0c9f1xvpxr1gllrrag9i1adm71q8x6yix9a16a8z3pd2nzj5f1s5")))

(define-public crate-lv2rs-core-0.2.0 (c (n "lv2rs-core") (v "0.2.0") (h "1lqcmh4rpkg9mjr4mw959hg304bcrkrz20lgx3pz00zhlk7cakka")))

(define-public crate-lv2rs-core-0.3.0 (c (n "lv2rs-core") (v "0.3.0") (h "1ssxjd77jzvgj0dnbb17a8129prwpayx3d2lkfrxlxn64frf0ds6")))

(define-public crate-lv2rs-core-0.3.1 (c (n "lv2rs-core") (v "0.3.1") (h "19g8j2liqhi92jiy245q57ai6ln4cr03a1haj8mq117lq9rh63d6")))

(define-public crate-lv2rs-core-0.3.2 (c (n "lv2rs-core") (v "0.3.2") (h "1bsbw7am7ammmza9aawdbz308p6w0lz57xyb7jz5z0y7gfrvq3df")))

(define-public crate-lv2rs-core-0.3.3 (c (n "lv2rs-core") (v "0.3.3") (h "0xh9slgnkwnyrdxc3jwns3facjdx84d5130pvl9f7bpdfi70hr4x")))

