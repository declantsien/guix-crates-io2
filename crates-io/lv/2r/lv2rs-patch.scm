(define-module (crates-io lv #{2r}# lv2rs-patch) #:use-module (crates-io))

(define-public crate-lv2rs-patch-0.0.0 (c (n "lv2rs-patch") (v "0.0.0") (h "1nvwr2hg7250szfmrzgvrv2qns9kx1vnwbk8q2cmwnngy5yclwpz")))

(define-public crate-lv2rs-patch-0.0.1 (c (n "lv2rs-patch") (v "0.0.1") (h "17alp2xf424rw0bsfyl2i7j07wlr240n67al2pyjc9l8964pgj02")))

