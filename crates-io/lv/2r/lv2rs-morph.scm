(define-module (crates-io lv #{2r}# lv2rs-morph) #:use-module (crates-io))

(define-public crate-lv2rs-morph-0.0.0 (c (n "lv2rs-morph") (v "0.0.0") (h "15ff2lpsf7n1n1jsb8j9az86id1pgm7lhdgh2b9sj5fg2nlrl994")))

(define-public crate-lv2rs-morph-0.0.1 (c (n "lv2rs-morph") (v "0.0.1") (h "0cwgvy7fx2k8k5zn6fjnmq3cjzb8z7n1wyvqvhrfksvsyq1jcwja")))

