(define-module (crates-io lv #{2r}# lv2rs-atom) #:use-module (crates-io))

(define-public crate-lv2rs-atom-0.0.0 (c (n "lv2rs-atom") (v "0.0.0") (h "1pfbncl61n69d8maij9bnnnmm8ll9277z10x7k8yikgh9my4qyrb")))

(define-public crate-lv2rs-atom-0.1.0 (c (n "lv2rs-atom") (v "0.1.0") (d (list (d (n "lv2rs-urid") (r "^0.2.0") (d #t) (k 0)))) (h "1vjfam9d9ww3cnyj00j7sz6lzjd6i0h4drxwyn2k4fgwkyknb7rq")))

(define-public crate-lv2rs-atom-0.1.1 (c (n "lv2rs-atom") (v "0.1.1") (d (list (d (n "lv2rs-urid") (r "^0.2.0") (d #t) (k 0)))) (h "1kn1nimyf4z6qfabr759aid2yvzgpnq59x9d1hibwd16xa2m5p0q")))

(define-public crate-lv2rs-atom-0.1.2 (c (n "lv2rs-atom") (v "0.1.2") (d (list (d (n "lv2rs-urid") (r "^0.2.0") (d #t) (k 0)))) (h "0dp3gsv837mgdbi2jjh42ks9yyh0psjxq0ryv3wln4j0mjvv4j37")))

