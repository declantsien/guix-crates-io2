(define-module (crates-io lv bi lvbitfile2rust-cli) #:use-module (crates-io))

(define-public crate-lvbitfile2rust-cli-0.1.0 (c (n "lvbitfile2rust-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "lvbitfile2rust") (r "^0.1.0") (d #t) (k 0)))) (h "18shkgm9ilk58jwq3a3jl115z6w0rq7fi82y0hdbxjc02vqr1kfc")))

(define-public crate-lvbitfile2rust-cli-0.1.1 (c (n "lvbitfile2rust-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "lvbitfile2rust") (r "^0.1.0") (d #t) (k 0)))) (h "19apzhp0k29lc782d421sfgrnrbi2v827g68qs7jqhk3jgjaidfz")))

