(define-module (crates-io lv bi lvbitfile2rust-macros) #:use-module (crates-io))

(define-public crate-lvbitfile2rust-macros-0.1.0 (c (n "lvbitfile2rust-macros") (v "0.1.0") (d (list (d (n "lvbitfile2rust") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (d #t) (k 0)))) (h "0d77cchxcn4jv47hsa5b59wa1vvdb8by2agngy2hnpnha9b8bwxl")))

(define-public crate-lvbitfile2rust-macros-0.1.1 (c (n "lvbitfile2rust-macros") (v "0.1.1") (d (list (d (n "lvbitfile2rust") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (d #t) (k 0)))) (h "0qhhxzy685n7ycyc9ac75svwb61hhcnhn4iw9wfwplc6m81zdawi")))

