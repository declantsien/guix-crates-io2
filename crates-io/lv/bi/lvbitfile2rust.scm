(define-module (crates-io lv bi lvbitfile2rust) #:use-module (crates-io))

(define-public crate-lvbitfile2rust-0.1.0 (c (n "lvbitfile2rust") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1jn6nqcnc9w64v93i9w3nbkraxnhp7qxm6ib39qgvaqcigzdpkz5")))

(define-public crate-lvbitfile2rust-0.1.1 (c (n "lvbitfile2rust") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0ldj9nx9bfdqpax63rgk44f22qlqjzf9xnfbw7msikm8qiiwcvam")))

