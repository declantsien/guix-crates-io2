(define-module (crates-io lv #{2_}# lv2_raw) #:use-module (crates-io))

(define-public crate-lv2_raw-0.1.0 (c (n "lv2_raw") (v "0.1.0") (d (list (d (n "libc") (r "^0") (d #t) (k 0)))) (h "0c8j9j2xgrazhlnpdsbgl7jnd1g6iwbc9wk01a7vc9bw4rdsa8i4")))

(define-public crate-lv2_raw-0.2.0 (c (n "lv2_raw") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j3apz55c2k99pannzsja3szgrzji0zr2d01v1d7kzk43wiyfg2p")))

