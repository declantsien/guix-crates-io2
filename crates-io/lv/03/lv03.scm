(define-module (crates-io lv #{03}# lv03) #:use-module (crates-io))

(define-public crate-lv03-0.0.1 (c (n "lv03") (v "0.0.1") (h "1mqb9dmq6f3jmvjpsmr77i09a6cb58ypyblwjv1fz2gk2ap10q19")))

(define-public crate-lv03-0.0.2 (c (n "lv03") (v "0.0.2") (h "1rqfyl4061clyk0gl48f81nws07p51jylryi296c7slk7yhsa1mm")))

(define-public crate-lv03-0.0.3 (c (n "lv03") (v "0.0.3") (d (list (d (n "nav-types") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0lami1xs0a3fcl75in18bx04117aq5vf830g277y6m8dzdks0pnr") (f (quote (("nav-types-conversion" "nav-types") ("default"))))))

(define-public crate-lv03-0.0.4 (c (n "lv03") (v "0.0.4") (d (list (d (n "bmp") (r "^0.5.0") (d #t) (k 2)) (d (n "nav-types") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "oorandom") (r "^11.1.2") (d #t) (k 0)))) (h "17alwpg801wrf33cfzdymffzcccgglwl9bsgm0nas3vvm0mbyajj") (f (quote (("nav-types-conversion" "nav-types") ("default"))))))

(define-public crate-lv03-0.0.5 (c (n "lv03") (v "0.0.5") (d (list (d (n "bmp") (r "^0.5.0") (d #t) (k 2)) (d (n "nav-types") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "oorandom") (r "^11.1.2") (d #t) (k 2)))) (h "0zbnlfrz8hk8fcvwzqsxl42v8ldgiz4qjv4r6gy6nzsngpqrc31p") (f (quote (("nav-types-conversion" "nav-types") ("default"))))))

(define-public crate-lv03-0.1.0 (c (n "lv03") (v "0.1.0") (d (list (d (n "bmp") (r "^0.5") (d #t) (k 2)) (d (n "nav-types") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "oorandom") (r "^11.1") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)))) (h "1rscnn41hwn10sc0ai6j8c2n4dpzdngmrf4ijmwdxklmvcf1bsi0") (f (quote (("nav-types-conversion" "nav-types") ("default"))))))

(define-public crate-lv03-0.1.1 (c (n "lv03") (v "0.1.1") (d (list (d (n "bmp") (r "^0.5") (d #t) (k 2)) (d (n "nav-types") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "oorandom") (r "^11.1") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)))) (h "1jlx1q298rbgn43522c5nfh6c06q2gplfyz7rzxch7glja6y0ix7") (f (quote (("nav-types-conversion" "nav-types") ("default"))))))

