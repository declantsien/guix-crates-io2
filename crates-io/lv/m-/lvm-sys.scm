(define-module (crates-io lv m- lvm-sys) #:use-module (crates-io))

(define-public crate-lvm-sys-0.1.0 (c (n "lvm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)))) (h "0y4l0lhbvh838xzv6jnflajzzij5y6vk94gs9pn3c0nwhl94qml6")))

(define-public crate-lvm-sys-0.1.1 (c (n "lvm-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "~0.40") (d #t) (k 1)))) (h "06arzngmq9pjcf9ib1im22qp20a7ccznl21gfsbs801wc1d26jqx")))

