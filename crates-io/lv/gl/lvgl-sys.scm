(define-module (crates-io lv gl lvgl-sys) #:use-module (crates-io))

(define-public crate-lvgl-sys-0.1.0 (c (n "lvgl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1njkcw9gqa2sv408am4009fryyfnd7p3mhd0z2pffxmkc3dndm8z") (l "lvgl")))

(define-public crate-lvgl-sys-0.1.1 (c (n "lvgl-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1ymfxlf5lh4bks7446wkwqiyhiz5l2xxc8pfxlwp9z1pmkyb3cwp") (l "lvgl")))

(define-public crate-lvgl-sys-0.2.0 (c (n "lvgl-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1j98gc130h8mjr9vph63hf4xf58lf7b7l1w73pbhn9lagzz8cpyr") (l "lvgl")))

(define-public crate-lvgl-sys-0.3.0 (c (n "lvgl-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "0vs6siv83s9w7kjisyfwpdbfb3y63dbci63zbj6714wphy8mzab2") (l "lvgl")))

(define-public crate-lvgl-sys-0.3.1 (c (n "lvgl-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1vplwn4xvm8q0fl58fqhm5i9ila0kmrcqf7wml99hk0z7pq0p3kl") (l "lvgl")))

(define-public crate-lvgl-sys-0.3.3 (c (n "lvgl-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "03w44hh9dpr3vrd4yj0hp2c304a8pjkqh66dabnz3daslnd19xd5") (l "lvgl")))

(define-public crate-lvgl-sys-0.4.0 (c (n "lvgl-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "0sf3i6x3xvxlirkfnxz81fq96abv2yqmp4cdasg8fn2xpyd92yjh") (l "lvgl")))

(define-public crate-lvgl-sys-0.5.1 (c (n "lvgl-sys") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "0w09x5h5bq2ak7lm72jqig4ksjg1c5is2qhv8wyrk69dn9bjaqia") (l "lvgl")))

(define-public crate-lvgl-sys-0.5.2 (c (n "lvgl-sys") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "07nmzrpd0rq6jkx0v0l6hgayzsqz4i9klain72adwv6dslh5x3fm") (l "lvgl")))

(define-public crate-lvgl-sys-0.6.1 (c (n "lvgl-sys") (v "0.6.1") (d (list (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "11xd8wjvl9ix9yr4526245fs696qin3ds3sfy27i0f91i5wsvl4j") (f (quote (("use-vendored-config") ("rust_timer") ("drivers")))) (l "lvgl")))

(define-public crate-lvgl-sys-0.6.2 (c (n "lvgl-sys") (v "0.6.2") (d (list (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1hj38rx18pi05yj652gbyhrnlnywv34vjg44a39xyszxla4xi5yn") (f (quote (("use-vendored-config") ("rust_timer") ("drivers")))) (l "lvgl")))

