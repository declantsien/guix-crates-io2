(define-module (crates-io lv #{2-}# lv2-midi) #:use-module (crates-io))

(define-public crate-lv2-midi-1.0.0 (c (n "lv2-midi") (v "1.0.0") (d (list (d (n "lv2-atom") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-core") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-units") (r "^0.1.0") (d #t) (k 2)) (d (n "lv2-urid") (r "^1.0.0") (d #t) (k 0)) (d (n "wmidi") (r "^3.1.0") (o #t) (d #t) (k 0)))) (h "1c9jmh2abc398hs3gv2pf56vd1fgm9kgrqxvwp0ydyc8fw7j8jy9") (f (quote (("host" "lv2-core/host" "lv2-urid/host" "lv2-atom/host") ("default"))))))

(define-public crate-lv2-midi-1.0.1 (c (n "lv2-midi") (v "1.0.1") (d (list (d (n "lv2-atom") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-core") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-units") (r "^0.1.0") (d #t) (k 2)) (d (n "lv2-urid") (r "^1.0.0") (d #t) (k 0)) (d (n "wmidi") (r "^3.1.0") (o #t) (d #t) (k 0)))) (h "1cv4xs1if05alrdgz81sick3hpaan32asklx91lf234kac8y7hal") (f (quote (("host" "lv2-core/host" "lv2-urid/host" "lv2-atom/host") ("default"))))))

(define-public crate-lv2-midi-1.1.0 (c (n "lv2-midi") (v "1.1.0") (d (list (d (n "lv2-atom") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-core") (r "^2.0.0") (d #t) (k 2)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-units") (r "^0.1.0") (d #t) (k 2)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)) (d (n "wmidi") (r "^3.1.0") (o #t) (d #t) (k 0)))) (h "1g4df6af0mwdxhv7h0ixh4d3qx9cmwrdn4kkmvwps7fh41qp7s23")))

(define-public crate-lv2-midi-1.2.0 (c (n "lv2-midi") (v "1.2.0") (d (list (d (n "lv2-atom") (r "^2.0.0") (d #t) (k 0)) (d (n "lv2-core") (r "^3.0.0") (d #t) (k 2)) (d (n "lv2-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "lv2-units") (r "^0.1.3") (d #t) (k 2)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)) (d (n "wmidi") (r "^3.1.0") (o #t) (d #t) (k 0)))) (h "0x0glbrfri1glgcrmvc6i1jfv6azhpqvp4ibk5cihsq3s2yfc8xd")))

