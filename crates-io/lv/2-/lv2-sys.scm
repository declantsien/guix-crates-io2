(define-module (crates-io lv #{2-}# lv2-sys) #:use-module (crates-io))

(define-public crate-lv2-sys-1.0.0 (c (n "lv2-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "01x9qxjx3sgkfdw0wyay3hj97zy34w3wggd2xx08j2y0wgfbn8w9")))

(define-public crate-lv2-sys-1.0.1 (c (n "lv2-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1isf6qc0zx1rx4afhll5fkarv98x3bzblaxk0yfnhvj5z1ag6fac")))

(define-public crate-lv2-sys-1.1.0 (c (n "lv2-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0gcq1779kfrfxfx81vbaf7vfpqhx907j055iq04p9ibi9lhymv8d")))

(define-public crate-lv2-sys-2.0.0 (c (n "lv2-sys") (v "2.0.0") (h "0c4f59mrjyy0z0wf033wp648df0sc6zirrcd6kndqj9nvvkzkl4x")))

