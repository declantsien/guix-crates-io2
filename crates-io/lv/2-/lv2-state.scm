(define-module (crates-io lv #{2-}# lv2-state) #:use-module (crates-io))

(define-public crate-lv2-state-1.0.0 (c (n "lv2-state") (v "1.0.0") (d (list (d (n "lv2-atom") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-core") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^1.0.0") (d #t) (k 0)))) (h "0rqab80wv82gmpnzl70hdqrjnkz4hksckv81apr94kmaqwaiwhgy") (f (quote (("host" "lv2-core/host" "lv2-urid/host" "lv2-atom/host") ("default"))))))

(define-public crate-lv2-state-1.0.1 (c (n "lv2-state") (v "1.0.1") (d (list (d (n "lv2-atom") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-core") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^1.0.0") (d #t) (k 0)))) (h "12v8giqw0p1rxn7mdgldja3ayqlm4xzzwxv6fqbcvnz0x1a0v6xy") (f (quote (("host" "lv2-core/host" "lv2-urid/host" "lv2-atom/host") ("default"))))))

(define-public crate-lv2-state-1.1.0 (c (n "lv2-state") (v "1.1.0") (d (list (d (n "lv2-atom") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-core") (r "^2.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^2.0.0") (d #t) (k 2)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "0m3rlwg3rj3283j5cdpbpk1y2f6wnazq0gfqxi7i52vp84h0y2pl")))

(define-public crate-lv2-state-2.0.0 (c (n "lv2-state") (v "2.0.0") (d (list (d (n "lv2-atom") (r "^2.0.0") (d #t) (k 0)) (d (n "lv2-core") (r "^3.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^2.1.0") (d #t) (k 2)) (d (n "mktemp") (r "^0.4.0") (d #t) (k 2)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "0nm0fc7cb4rkmfsvvr4xbac4qf0j7wl2gws3qrcflx057i2lpsb5")))

