(define-module (crates-io lv #{2-}# lv2-units) #:use-module (crates-io))

(define-public crate-lv2-units-0.1.0 (c (n "lv2-units") (v "0.1.0") (d (list (d (n "lv2-core") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^1.0.0") (d #t) (k 0)))) (h "0gwz4jccw7jf64m4jfgs0vlvcdvq46r17fcmr53nfbhn3b73gr42") (f (quote (("host" "lv2-urid/host") ("default"))))))

(define-public crate-lv2-units-0.1.1 (c (n "lv2-units") (v "0.1.1") (d (list (d (n "lv2-core") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^1.0.0") (d #t) (k 0)))) (h "1j7dw8hlchj4b0kmcmv6ajfq3arp5lzajxxw2jjn8gjk3fskf50x") (f (quote (("host" "lv2-urid/host") ("default"))))))

(define-public crate-lv2-units-0.1.2 (c (n "lv2-units") (v "0.1.2") (d (list (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "05zmsa5qs90zq24dzzxg39i0p0kfgwr45lr2y5yaw7alp8x2s7x6")))

(define-public crate-lv2-units-0.1.3 (c (n "lv2-units") (v "0.1.3") (d (list (d (n "lv2-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "0fdamp3hxdr36hqi1j6y01rz1x17if1ibzr7rr4nrabidw74gf82")))

