(define-module (crates-io lv #{2-}# lv2-urid-derive) #:use-module (crates-io))

(define-public crate-lv2-urid-derive-1.0.0 (c (n "lv2-urid-derive") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "1zcvyg1byg6xk92wn0jfs6539kw6sl3d4m08rrc76lzifym8hsnq")))

(define-public crate-lv2-urid-derive-1.0.1 (c (n "lv2-urid-derive") (v "1.0.1") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0w0zmd0gpwsj2z6d3gkjazplmkcbl6r609sqdhn500pzpdkgb3rg")))

