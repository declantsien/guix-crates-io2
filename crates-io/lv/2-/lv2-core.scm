(define-module (crates-io lv #{2-}# lv2-core) #:use-module (crates-io))

(define-public crate-lv2-core-1.0.0 (c (n "lv2-core") (v "1.0.0") (d (list (d (n "lv2-core-derive") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)))) (h "1yldgpbyl11hvggww74haah6520f3qzr20s7kkmymhfpf2mhrzma") (f (quote (("host") ("default"))))))

(define-public crate-lv2-core-1.0.1 (c (n "lv2-core") (v "1.0.1") (d (list (d (n "lv2-core-derive") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)))) (h "02r3wsc0pcz0hmagrz22jdf7adpqdyma3mncxadqxrw6iqwad17r") (f (quote (("host") ("default"))))))

(define-public crate-lv2-core-2.0.0 (c (n "lv2-core") (v "2.0.0") (d (list (d (n "lv2-core-derive") (r "^2.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "0kkkjiilcl3ks3g2mlg2ppp98kdla4a38xmw5ns393845h4p1cvz")))

(define-public crate-lv2-core-3.0.0 (c (n "lv2-core") (v "3.0.0") (d (list (d (n "lv2-core-derive") (r "^2.1.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "1pj9l15zwqwj2h83f3xfpwxsj70vvhkw52gyzkljafvrbx1h00fm")))

