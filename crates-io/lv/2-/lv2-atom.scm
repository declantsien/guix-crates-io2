(define-module (crates-io lv #{2-}# lv2-atom) #:use-module (crates-io))

(define-public crate-lv2-atom-1.0.0 (c (n "lv2-atom") (v "1.0.0") (d (list (d (n "lv2-core") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-units") (r "^0.1.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^1.0.0") (d #t) (k 0)))) (h "0dfwb7pcp80csr4i0p6s7hab040p5vilhyljp0bdn1435n3r1g85") (f (quote (("host" "lv2-core/host" "lv2-urid/host") ("default"))))))

(define-public crate-lv2-atom-1.0.1 (c (n "lv2-atom") (v "1.0.1") (d (list (d (n "lv2-core") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-units") (r "^0.1.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^1.0.0") (d #t) (k 0)))) (h "0qa92lq77lhz7nxm5v4vpyqx08gzhgb5gpc5rza2jixqx3ymmkv7") (f (quote (("host" "lv2-core/host" "lv2-urid/host") ("default"))))))

(define-public crate-lv2-atom-1.1.0 (c (n "lv2-atom") (v "1.1.0") (d (list (d (n "lv2-core") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-units") (r "^0.1.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^2.0.0") (d #t) (k 2)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "0i088vm59dpgw5z24hsg043dsbslsdza31hg5kibkpbxyknn3yx6") (f (quote (("default" "lv2-core"))))))

(define-public crate-lv2-atom-2.0.0 (c (n "lv2-atom") (v "2.0.0") (d (list (d (n "lv2-core") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "lv2-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "lv2-units") (r "^0.1.3") (d #t) (k 0)) (d (n "lv2-urid") (r "^2.1.0") (d #t) (k 2)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "0wd9rgsn8sag8wyhjccmnn82gx4w1yyiav52nyvk579l21xlw6wm") (f (quote (("default" "lv2-core"))))))

