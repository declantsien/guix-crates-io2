(define-module (crates-io lv #{2-}# lv2-host-minimal) #:use-module (crates-io))

(define-public crate-lv2-host-minimal-0.1.0 (c (n "lv2-host-minimal") (v "0.1.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "lilv-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "lv2-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^2.1.0") (d #t) (k 0)) (d (n "lv2_raw") (r "^0.2.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "0rp1xk9r25d4xg6s1c9dfwrrcqvis3ik73rb8df5q8kc2b31i98k") (y #t)))

(define-public crate-lv2-host-minimal-0.1.1 (c (n "lv2-host-minimal") (v "0.1.1") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "lilv-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "lv2-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^2.1.0") (d #t) (k 0)) (d (n "lv2_raw") (r "^0.2.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "00lir118mmbx55cd5fph7492vw6wbib81ykvwsy0l5lzz2c4z4zk") (y #t)))

(define-public crate-lv2-host-minimal-0.1.2 (c (n "lv2-host-minimal") (v "0.1.2") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "lilv-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "lv2-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^2.1.0") (d #t) (k 0)) (d (n "lv2_raw") (r "^0.2.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "08xx1lszfwzanhbxwpc6mg8w9y5d1z1mzj6iaxvdb2ryfp615byx") (y #t)))

(define-public crate-lv2-host-minimal-0.1.4 (c (n "lv2-host-minimal") (v "0.1.4") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "lilv-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "lv2-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^2.1.0") (d #t) (k 0)) (d (n "lv2_raw") (r "^0.2.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "1svm87s2d5b34lnfc5dwa6rgnal2npyam2kj33kfdwkdjv6h4352")))

