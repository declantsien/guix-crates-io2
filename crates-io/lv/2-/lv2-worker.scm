(define-module (crates-io lv #{2-}# lv2-worker) #:use-module (crates-io))

(define-public crate-lv2-worker-0.1.0 (c (n "lv2-worker") (v "0.1.0") (d (list (d (n "lv2-core") (r "^2.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "0fd3zc8s4qxhq2msl7caiqhq11zcpbz8gpw8d716qpf7xj7py0ak")))

(define-public crate-lv2-worker-0.1.1 (c (n "lv2-worker") (v "0.1.1") (d (list (d (n "lv2-core") (r "^3.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "14crsrnjyarra9ipma6lhaj4gpfadvippzr134nkn0z3y30ip4fj")))

