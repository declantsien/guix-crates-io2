(define-module (crates-io lv #{2-}# lv2-urid) #:use-module (crates-io))

(define-public crate-lv2-urid-1.0.0 (c (n "lv2-urid") (v "1.0.0") (d (list (d (n "lv2-core") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-urid-derive") (r "^1.0.0") (d #t) (k 0)))) (h "14308czn0j3v2j7kzqf1m5j0vzh5fx6p340q6xysqcc34hcaw2mp") (f (quote (("host" "lv2-core/host") ("default"))))))

(define-public crate-lv2-urid-1.0.1 (c (n "lv2-urid") (v "1.0.1") (d (list (d (n "lv2-core") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-urid-derive") (r "^1.0.0") (d #t) (k 0)))) (h "008szwpwf9y36317mgxjysqk3czr07yri0mp280km3f48fnzrk2p") (f (quote (("host" "lv2-core/host") ("default"))))))

(define-public crate-lv2-urid-2.0.0 (c (n "lv2-urid") (v "2.0.0") (d (list (d (n "lv2-core") (r "^2.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "1074mkng597ydcjmbfbprf5mkr3y38a71hz7qgkwxipl9kwxjgig")))

(define-public crate-lv2-urid-2.1.0 (c (n "lv2-urid") (v "2.1.0") (d (list (d (n "lv2-core") (r "^3.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "0s2fcb0nyn54ml6azkbhnnxghy898x1q5vs5qgdznrhy9m20624c")))

