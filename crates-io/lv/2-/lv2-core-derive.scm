(define-module (crates-io lv #{2-}# lv2-core-derive) #:use-module (crates-io))

(define-public crate-lv2-core-derive-1.0.0 (c (n "lv2-core-derive") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "06ghdivk0rhczmvcs0c0xx924swj9anmqrmjfzpzl402s1g42hsj")))

(define-public crate-lv2-core-derive-1.0.1 (c (n "lv2-core-derive") (v "1.0.1") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0k2ik6ki2ghz9q47hyqrjc1i107b17akyl4n8hnpi55ackz5gwnd")))

(define-public crate-lv2-core-derive-2.0.0 (c (n "lv2-core-derive") (v "2.0.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "1w1zb926q49pyg1cqwzvlj1n2b8f56416xz3ibbrns0zcb5fzcwg")))

(define-public crate-lv2-core-derive-2.1.0 (c (n "lv2-core-derive") (v "2.1.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0n7lidr50j6vs6py7c63s82yy081y1v59qmrzqw5h0v0z7v064c1")))

(define-public crate-lv2-core-derive-2.1.1 (c (n "lv2-core-derive") (v "2.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "12w3l41jzargrcywz13hbmaazfw4ix2sljl3601h6jfbdrw8zybv")))

