(define-module (crates-io lv #{2-}# lv2-time) #:use-module (crates-io))

(define-public crate-lv2-time-0.1.0 (c (n "lv2-time") (v "0.1.0") (d (list (d (n "lv2-core") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^1.0.0") (d #t) (k 0)))) (h "1j1v2vfvq0pd7jqd4dqz4fnllgqc128yar74i03cxn8wiz6cy93h") (f (quote (("host" "lv2-urid/host") ("default"))))))

(define-public crate-lv2-time-0.1.1 (c (n "lv2-time") (v "0.1.1") (d (list (d (n "lv2-core") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lv2-urid") (r "^1.0.0") (d #t) (k 0)))) (h "0hsvlph1awqmiw3w9hxgq2kdciisd5kk6j3v2mwhr3f8xv19226a") (f (quote (("host" "lv2-urid/host") ("default"))))))

(define-public crate-lv2-time-0.1.2 (c (n "lv2-time") (v "0.1.2") (d (list (d (n "lv2-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "1ngjc0srdpm0sa6pzj6scgcz2k3q7mnx80jykabq3vyh0qgs9y0n")))

(define-public crate-lv2-time-0.1.3 (c (n "lv2-time") (v "0.1.3") (d (list (d (n "lv2-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "urid") (r "^0.1.0") (d #t) (k 0)))) (h "0wznk17vvn5dph6r47vjwmf7g98pb6ij2fdhizdk95sf2qvkf82c")))

