(define-module (crates-io lp u- lpu-macros) #:use-module (crates-io))

(define-public crate-lpu-macros-0.1.0 (c (n "lpu-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "119pcbalk1wqjhl8acjk9m79f57n2f1qfczb86sz86a9cg8fh1gn")))

(define-public crate-lpu-macros-0.2.0 (c (n "lpu-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1sry8g3ryndwc0gssakarvd5nadakf17664asgkbs9rayg98z83h")))

