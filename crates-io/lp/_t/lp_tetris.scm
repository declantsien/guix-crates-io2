(define-module (crates-io lp _t lp_tetris) #:use-module (crates-io))

(define-public crate-lp_tetris-0.1.0 (c (n "lp_tetris") (v "0.1.0") (d (list (d (n "array2d") (r "^0.2.1") (d #t) (k 0)) (d (n "midir") (r "^0.5.0") (d #t) (k 0)) (d (n "multiinput") (r "^0.0.15") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "14jrc7kcjyrp50cxp60va5smn30jb5s86v3wv394iqh25w32nsy2")))

(define-public crate-lp_tetris-0.1.1 (c (n "lp_tetris") (v "0.1.1") (d (list (d (n "array2d") (r "^0.2.1") (d #t) (k 0)) (d (n "midir") (r "^0.5.0") (d #t) (k 0)) (d (n "multiinput") (r "^0.0.15") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "0bsq7qcmjsjy9hylj8xlj6y2lz5hppnrgc990jsi060wc4zq5lcf")))

(define-public crate-lp_tetris-0.1.2 (c (n "lp_tetris") (v "0.1.2") (d (list (d (n "array2d") (r "^0.2.1") (d #t) (k 0)) (d (n "midir") (r "^0.5.0") (d #t) (k 0)) (d (n "multiinput") (r "^0.0.15") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "0liar9nnwbvvcnmdcmaa38yisp7f4xlrih8xb2py389f70f82ynz")))

(define-public crate-lp_tetris-0.1.3 (c (n "lp_tetris") (v "0.1.3") (d (list (d (n "array2d") (r "^0.2.1") (d #t) (k 0)) (d (n "midir") (r "^0.5.0") (d #t) (k 0)) (d (n "multiinput") (r "^0.0.15") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "0gcv7gkggyc8703cb3qz2kmznz6b72b4va0liznnvqgvpznnlh2w")))

(define-public crate-lp_tetris-0.1.4 (c (n "lp_tetris") (v "0.1.4") (d (list (d (n "array2d") (r "^0.2.1") (d #t) (k 0)) (d (n "midir") (r "^0.5.0") (d #t) (k 0)) (d (n "multiinput") (r "^0.0.15") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "1c6xiz71dkzym693s6jj3hpzzvq57q9rcy8wamnm4wv9cjy39wki")))

(define-public crate-lp_tetris-0.1.5 (c (n "lp_tetris") (v "0.1.5") (d (list (d (n "array2d") (r "^0.2.1") (d #t) (k 0)) (d (n "midir") (r "^0.5.0") (d #t) (k 0)) (d (n "multiinput") (r "^0.0.15") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "1wr5cqqn337sj8mm7rbjwp3dgnjjysqr8sb6wd9a0ws39yv1r4q6")))

(define-public crate-lp_tetris-0.1.6 (c (n "lp_tetris") (v "0.1.6") (d (list (d (n "array2d") (r "^0.2.1") (d #t) (k 0)) (d (n "midir") (r "^0.5.0") (d #t) (k 0)) (d (n "multiinput") (r "^0.0.15") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "10r39b45iql0cb2wnqa2pbb03naa25jvwv5hqacwnq4pj3aqb33z")))

