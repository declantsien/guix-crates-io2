(define-module (crates-io lp -s lp-solvers) #:use-module (crates-io))

(define-public crate-lp-solvers-0.0.1 (c (n "lp-solvers") (v "0.0.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "07lxyh0izar93m62w6q2k65nfyp389dh45zpxwicnwyl2y65cmw0")))

(define-public crate-lp-solvers-0.0.2 (c (n "lp-solvers") (v "0.0.2") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "0yl0vsq99vbcr1gsw2qksr8k3prbrfjdl08xi3cklr2l12gpv8lg") (f (quote (("cplex" "xml-rs"))))))

(define-public crate-lp-solvers-0.0.3 (c (n "lp-solvers") (v "0.0.3") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "1g0g0dlhc24s4jpm2m5a3whkpgwid5lsaxsyj2n3azkri7ysycj1") (f (quote (("cplex" "xml-rs"))))))

(define-public crate-lp-solvers-0.0.4 (c (n "lp-solvers") (v "0.0.4") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "002xd94bagavnwa7hsdjwq49dak14dv9l892q7xr7a4176lychrs") (f (quote (("cplex" "xml-rs"))))))

(define-public crate-lp-solvers-0.0.5 (c (n "lp-solvers") (v "0.0.5") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "1fl8r5nzw4zbjpq2g04pz4yms35jy9m23jlpwa2ipmgw16j535bw") (f (quote (("cplex" "xml-rs"))))))

(define-public crate-lp-solvers-1.0.0 (c (n "lp-solvers") (v "1.0.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "1j1z2786s1xdc2in9b2x130i2piv1h24lgldrnz98vz2mmf9n27z") (f (quote (("cplex" "xml-rs"))))))

(define-public crate-lp-solvers-1.0.1 (c (n "lp-solvers") (v "1.0.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "07nkan0a3rwmrrlvpnhdnmbkzmj624z11f716n9d82wvzyqwnj5j") (f (quote (("cplex" "xml-rs"))))))

