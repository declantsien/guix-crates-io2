(define-module (crates-io lp wa lpwan) #:use-module (crates-io))

(define-public crate-lpwan-0.1.0 (c (n "lpwan") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (k 0)) (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "ieee802154") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "radio") (r "^0.8.1") (d #t) (k 0)) (d (n "radio") (r "^0.8.1") (f (quote ("mock"))) (d #t) (k 2)) (d (n "rand-facade") (r "^0.2.0") (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "simplelog") (r "^0.8.0") (d #t) (k 2)))) (h "0zyl466wrw57cpkjf869vk6xah8zbqyrzfdkrp26dxjp3sw5ilwz") (f (quote (("std" "rand-facade/os_rng" "bytes/std") ("mocks") ("default" "std"))))))

