(define-module (crates-io lp so lpsolve) #:use-module (crates-io))

(define-public crate-lpsolve-0.1.0 (c (n "lpsolve") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lpsolve-sys") (r "^5.5") (d #t) (k 0)))) (h "1kbf16ymk0vdraqqs46sdwv3b4v1scv0q57jq6jvbmwhhwd6fv2i")))

