(define-module (crates-io lp #{58}# lp586x) #:use-module (crates-io))

(define-public crate-lp586x-0.1.0 (c (n "lp586x") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)))) (h "0flsr0jl2szlx46yijwmny5pgrrn0rvcsq5cp7ch6hjq8yr969z6")))

