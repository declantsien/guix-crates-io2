(define-module (crates-io lp -m lp-modeler) #:use-module (crates-io))

(define-public crate-lp-modeler-0.3.0 (c (n "lp-modeler") (v "0.3.0") (h "0zf3blx8wnjclncbfqz7wbf4l28s7n4qp0rrmpasdhmkpfkdic60")))

(define-public crate-lp-modeler-0.3.1 (c (n "lp-modeler") (v "0.3.1") (h "1f1f463h7nw4vrss5w36rx7c452lmpn76fyrb8y93c8xvm2391l9")))

(define-public crate-lp-modeler-0.3.2 (c (n "lp-modeler") (v "0.3.2") (d (list (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "1n1y2smki5bwn56zikisyak58nqcrrwma20hyxk0vsrdmvhdcjmh")))

(define-public crate-lp-modeler-0.3.3 (c (n "lp-modeler") (v "0.3.3") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1gq0pzc7wgq9gssicp4ixmzqsf1y4xy4jwfbwik3xc7wrwbs84c3")))

(define-public crate-lp-modeler-0.4.0 (c (n "lp-modeler") (v "0.4.0") (d (list (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0qqlc7k0v6bd7qyl37kv572c20inw2spkl4pg62fyrah6r6jdghf")))

(define-public crate-lp-modeler-0.4.1 (c (n "lp-modeler") (v "0.4.1") (d (list (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1mh6fdrvhhaqk8d5srirydk4nwxlkvj6lh3flsr6ipqb17h69f1l")))

(define-public crate-lp-modeler-0.4.2 (c (n "lp-modeler") (v "0.4.2") (d (list (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1i1qsz50413jgvhw78wjd41a006mnnsq4vb42d5kb9a4cn0z03ry")))

(define-public crate-lp-modeler-0.4.3 (c (n "lp-modeler") (v "0.4.3") (d (list (d (n "coin_cbc") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "11v1yj0nf7zib5zwqf76f24dq785l9g4g8flm0n4rvbfqlszm5mv")))

(define-public crate-lp-modeler-0.5.0 (c (n "lp-modeler") (v "0.5.0") (d (list (d (n "coin_cbc") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "minilp") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0rfln4kcshcfs6j1ppdl3xkry68lhl8ahiwj8jcv3l0rim9lp7z6") (f (quote (("native_coin_cbc" "coin_cbc"))))))

