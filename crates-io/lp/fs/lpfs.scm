(define-module (crates-io lp fs lpfs) #:use-module (crates-io))

(define-public crate-lpfs-0.1.0 (c (n "lpfs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.65") (d #t) (k 2)))) (h "0ls4fpjacrbd2x3pczjwb28l7d8q458ykr05hmsk86385mckx85k") (f (quote (("task") ("stat") ("pid_stat") ("pid_comm") ("default" "apm" "cpuinfo" "stat" "_self" "task" "pid_stat" "pid_comm") ("cpuinfo") ("apm") ("_self"))))))

(define-public crate-lpfs-0.2.0 (c (n "lpfs") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.65") (d #t) (k 2)))) (h "0jnx1bk5irlwa8vj74zp5kx39isawx3w14smyp3fzzz20gijbgw2")))

