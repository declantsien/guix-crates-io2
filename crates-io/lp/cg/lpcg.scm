(define-module (crates-io lp cg lpcg) #:use-module (crates-io))

(define-public crate-lpcg-0.1.0 (c (n "lpcg") (v "0.1.0") (d (list (d (n "image") (r "^0.24.3") (f (quote ("png"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "07av1ymdvcacimv9qd9fk48bl5wg65hmcngdcpig65h7gpf8qwdn")))

(define-public crate-lpcg-0.2.0 (c (n "lpcg") (v "0.2.0") (d (list (d (n "image") (r "^0.24.3") (f (quote ("png"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0ihibx8bnmm89wp0mnxaqk2snkbkh12r0849jd482pzivc06r9pp")))

(define-public crate-lpcg-0.3.0 (c (n "lpcg") (v "0.3.0") (d (list (d (n "image") (r "^0.24.3") (f (quote ("png"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0gdlpxga2r49zyzg02nwx3rfz03bwiiyk2pq2dpr7017sz0a1qqw")))

