(define-module (crates-io lp -t lp-types) #:use-module (crates-io))

(define-public crate-lp-types-0.0.0 (c (n "lp-types") (v "0.0.0") (d (list (d (n "latexify") (r "^0.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.158") (d #t) (k 0)) (d (n "sprs") (r "^0.11.0") (d #t) (k 0)))) (h "1k1z04gvy7lnlk7nrnjw62shghlvglyp7rhmzsdpr1hys0wgimir") (f (quote (("default"))))))

