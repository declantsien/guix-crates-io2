(define-module (crates-io lp #{50}# lp50xx) #:use-module (crates-io))

(define-public crate-lp50xx-0.0.1 (c (n "lp50xx") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "03n9jzbd2vwkqww48fwss0xv0i3rhk7n3xsz0xr8mkk7qcqjq1pf") (f (quote (("default"))))))

(define-public crate-lp50xx-0.0.2 (c (n "lp50xx") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1lh9kbg5fy4pvhh1803kkaa9lvy8lsr9700zgmn2zilcdmns1lrg") (f (quote (("default"))))))

(define-public crate-lp50xx-0.0.3 (c (n "lp50xx") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "00drbp4ww5dxbx1n45m0y1ab2qhabz0927xjisv6f7vja3cjgnvm") (f (quote (("default"))))))

(define-public crate-lp50xx-0.0.4 (c (n "lp50xx") (v "0.0.4") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0cg54408rv0a7990qksia19yg9ssa6rph044i7kly311bw66rv8f") (f (quote (("default"))))))

