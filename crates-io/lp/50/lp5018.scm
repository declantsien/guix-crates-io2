(define-module (crates-io lp #{50}# lp5018) #:use-module (crates-io))

(define-public crate-lp5018-0.1.0 (c (n "lp5018") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)))) (h "0fwmhp6vbskvdk0qybg07fhjyin5ll60wm3wiw9nxwjxnhylnpr4") (y #t)))

(define-public crate-lp5018-0.1.1 (c (n "lp5018") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)))) (h "1yjq5cg8sqdjgyzzwkp2zdlkz1n7bkbpy487vp3znvy2nwhz3bbx") (y #t)))

(define-public crate-lp5018-0.1.2 (c (n "lp5018") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)))) (h "1d911rd8v1fjq22lx30f2cnmav900axl4rnasm0mi343l58bm0nr")))

(define-public crate-lp5018-0.1.3 (c (n "lp5018") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)))) (h "0aa759f665skm0gn4d1g76i76b8hancs7cp9p0vdynap6qwss1kh")))

(define-public crate-lp5018-0.1.4 (c (n "lp5018") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)))) (h "05fhfn202qvn9qljrdz2pyckylq98xrw4dqs56nsxycy9r59rqk7")))

