(define-module (crates-io lp c4 lpc43xx) #:use-module (crates-io))

(define-public crate-lpc43xx-0.0.0 (c (n "lpc43xx") (v "0.0.0") (d (list (d (n "volatile-register") (r "^0.1.2") (d #t) (k 0)))) (h "1lbvxavs1bynbi8l3aihjsxn4haq56sjlxv0b8xxb0v7chv34zq2")))

(define-public crate-lpc43xx-0.1.0 (c (n "lpc43xx") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1x8f19ryxi206jhx6mhrg467ip3xhaaym744nfxz8mvqp42ds02g") (f (quote (("rt" "cortex-m-rt"))))))

