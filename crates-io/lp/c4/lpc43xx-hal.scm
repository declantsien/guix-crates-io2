(define-module (crates-io lp c4 lpc43xx-hal) #:use-module (crates-io))

(define-public crate-lpc43xx-hal-0.0.0 (c (n "lpc43xx-hal") (v "0.0.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lpc43xx") (r "^0.1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1xclsk66zib0mf4g5zvi6v6q3c27mbnc77z1kir9i02cih35hsyj")))

