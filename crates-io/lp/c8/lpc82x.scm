(define-module (crates-io lp c8 lpc82x) #:use-module (crates-io))

(define-public crate-lpc82x-0.1.0 (c (n "lpc82x") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1l8xyyzczbffidjqk4pdnhszgl75saagwrl5ifr2g91bx55whxl6") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-lpc82x-0.1.1 (c (n "lpc82x") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0s30ivia6sp7n2x44nkkh2yrlm43nm2nzxhgk0663p27g3qk42r0") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-lpc82x-0.1.2 (c (n "lpc82x") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "13i5m9pq8kgrs13rl4ch65s44j63rq89gp0yn4ijplwcygv449q9") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-lpc82x-0.1.3 (c (n "lpc82x") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1gdcwak0nmjx5dz2ngvsysj904w8s1bgjhjpkki4amp0sz7kh7ss") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-lpc82x-0.1.4 (c (n "lpc82x") (v "0.1.4") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "14yp0v95znvlqi4qp431acfp2yjb15qfr1si47q72v1qix5i015w") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-lpc82x-0.1.5 (c (n "lpc82x") (v "0.1.5") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0ywfc28pn4w1r1ihfdbzx826mpy897fmfl40pr30920b6k5b6zvw") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-lpc82x-0.2.0 (c (n "lpc82x") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1lsds9yhv4nxdb1l3ckdn628vsfsj4vlfk02v0scvr2963b0xix1") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-lpc82x-0.2.1 (c (n "lpc82x") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "013iwrvi790ysi9hwfjs94dwyb6rll5h0wknkdnagh3fpsm7m8jc") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-lpc82x-0.3.0 (c (n "lpc82x") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "18qw8awdf1ddzk32nazbv5r1fw82wnnykvj2sr8nlgb3da6y71bl") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-lpc82x-0.3.1 (c (n "lpc82x") (v "0.3.1") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1dhm8k94qrd4ci2lhkzvm1x67ayfdd0lq048rv7fd3d9061yi66m") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-lpc82x-0.3.2 (c (n "lpc82x") (v "0.3.2") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "14dzs5lwv4kslxal4g6g3v0xd8ksz2qnikyl0qrqs3lh15pibls1") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-lpc82x-0.4.0 (c (n "lpc82x") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0mpgd17dj6ydivyp07hjdk318rn1ni67w2097crjzmxpx18xwhvd") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-lpc82x-0.5.0 (c (n "lpc82x") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1zysbakxzaw9y32iqqmkp5b1hnqqrc8ngfbfn275f705m0g2xmpn") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-lpc82x-0.6.0 (c (n "lpc82x") (v "0.6.0") (h "1d8yrk8hmzllbj25vpn2a0c64g487qlcz9akl8apl3adc5hjgj1b")))

