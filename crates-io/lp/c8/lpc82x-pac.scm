(define-module (crates-io lp c8 lpc82x-pac) #:use-module (crates-io))

(define-public crate-lpc82x-pac-0.6.0 (c (n "lpc82x-pac") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0xagkx5sx3xx3z2a1a13dyjcik2brkpjkprkrgbmli1blz5m4maq") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-lpc82x-pac-0.6.1 (c (n "lpc82x-pac") (v "0.6.1") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0sc8gp2hvqyss3jd7qrg6sq0kclhf7ydf9zvyv5k0xqkkjxg8iv4") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-lpc82x-pac-0.6.2 (c (n "lpc82x-pac") (v "0.6.2") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1nj61j2a0d2snj6rsih68vgv93iqh65m14ryhbr2523976pwa2rr") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-lpc82x-pac-0.7.0 (c (n "lpc82x-pac") (v "0.7.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "03fs5qzwzdgr90ix8g8iy1i8n2y2sv3s11y9gkx463a98zwk9yrd") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-lpc82x-pac-0.8.0 (c (n "lpc82x-pac") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1vf9019l53z37y8a589mwsa5ydfmvil5c726kkz0wpbzw4yzv3cg") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-lpc82x-pac-0.8.1 (c (n "lpc82x-pac") (v "0.8.1") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0cxz0xfjb367anmjcya37b130m2z76xhdd009nmsaym8wb7knzvn") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

