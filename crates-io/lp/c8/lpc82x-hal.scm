(define-module (crates-io lp c8 lpc82x-hal) #:use-module (crates-io))

(define-public crate-lpc82x-hal-0.1.0 (c (n "lpc82x-hal") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)) (d (n "lpc82x") (r "^0.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1wipnh9gdvscygmf338dwrqsm0ga0i4z1bqkz2dffyjkp3mi99zy") (f (quote (("rt" "lpc82x/rt"))))))

(define-public crate-lpc82x-hal-0.2.0 (c (n "lpc82x-hal") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lpc82x") (r "^0.4") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "panic-abort") (r "^0.2") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "0p8nd9jdq33xwnrxjzqcsmv8lb7kw33hdpz7dknhh11ayl3jrhn4") (f (quote (("rt" "lpc82x/rt") ("compiletest" "compiletest_rs"))))))

(define-public crate-lpc82x-hal-0.3.0 (c (n "lpc82x-hal") (v "0.3.0") (d (list (d (n "compiletest_rs") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.3") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lpc82x-pac") (r "^0.6") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "panic-abort") (r "^0.3.1") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "1cl7d2xcb960mi9hd4kbr4jnqjygnmis1k3a58p3cr6dpxxaz5gy") (f (quote (("rt" "lpc82x-pac/rt") ("compiletest" "compiletest_rs"))))))

(define-public crate-lpc82x-hal-0.4.0 (c (n "lpc82x-hal") (v "0.4.0") (d (list (d (n "compiletest_rs") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.3") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lpc82x-pac") (r "^0.6") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "0gdxy9w1ngbhy3ax014v8gwjcr446fhyfhcmjy8f46hjyjndjlwr") (f (quote (("rt" "lpc82x-pac/rt") ("compiletest" "compiletest_rs"))))))

(define-public crate-lpc82x-hal-0.5.0 (c (n "lpc82x-hal") (v "0.5.0") (h "1v1n8qcsdkczlhwvlblrj0s1qghjfzc77gq6zinrcqzra1wbsbs3")))

