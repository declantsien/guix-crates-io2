(define-module (crates-io lp c8 lpc845-pac) #:use-module (crates-io))

(define-public crate-lpc845-pac-0.1.0 (c (n "lpc845-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0y3yd7n19qz9f310l6l8cv7afqgfwc539lx14g023flc1a1fcag5") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-lpc845-pac-0.2.0 (c (n "lpc845-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1ylyisvy70jsh0bgr4h8rpfj0wc418vvf88qbn5gh056zvpp9cbw") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-lpc845-pac-0.3.0 (c (n "lpc845-pac") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0d289bq2xd58lg3sh5mwcq6xx51qska02983yj3j13m21b8w2i4a") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-lpc845-pac-0.4.0 (c (n "lpc845-pac") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "05jqgw70pyb22765jk9sgbchdkcbv8l64fm0qm8pfyfwvd8r80g2") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-lpc845-pac-0.4.1 (c (n "lpc845-pac") (v "0.4.1") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0zq1vbrvcc4yl3f51i2hr9y6j00pp4lk9fyqib81rrjw4hh1kcpx") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

