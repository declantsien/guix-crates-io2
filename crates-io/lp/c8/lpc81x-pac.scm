(define-module (crates-io lp c8 lpc81x-pac) #:use-module (crates-io))

(define-public crate-lpc81x-pac-0.1.0 (c (n "lpc81x-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "11byjzdb357xrliqvwqyjhb66hwhlwyz627xwk18cc98j8hpbspm") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-lpc81x-pac-0.2.1 (c (n "lpc81x-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1w3a7zwxxn5vdpnh45ypjwscndvp8sxda9hshrlm9n1lpnr189c8") (f (quote (("rt" "cortex-m-rt/device") ("lpc812") ("lpc811") ("lpc810") ("default" "rt"))))))

