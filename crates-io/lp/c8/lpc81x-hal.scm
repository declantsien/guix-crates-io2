(define-module (crates-io lp c8 lpc81x-hal) #:use-module (crates-io))

(define-public crate-lpc81x-hal-0.1.0 (c (n "lpc81x-hal") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (d #t) (k 2)) (d (n "cortex-m-rtfm") (r "^0.4.3") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lpc81x-pac") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0a0l8cypdswhlbml7scvsh2hm7rm4j2y0vi89iq22mpi0lqh7vs8")))

(define-public crate-lpc81x-hal-0.2.0 (c (n "lpc81x-hal") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (d #t) (k 2)) (d (n "cortex-m-rtfm") (r "^0.4.3") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lpc81x-pac") (r "^0.1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0xy5jckf2lccxpd0hsxpnjwq3igm0lnrq4nzjccs7fm5qaiq2czp")))

