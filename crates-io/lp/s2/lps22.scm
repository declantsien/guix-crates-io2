(define-module (crates-io lp s2 lps22) #:use-module (crates-io))

(define-public crate-lps22-0.1.0 (c (n "lps22") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "hal") (r "^0.2.0") (d #t) (k 0) (p "embedded-hal")))) (h "1xvqf15gsf4pqrzmyx6qjgrynp012ljvy8p42s7gnlbb60gz6n8z")))

