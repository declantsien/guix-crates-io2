(define-module (crates-io lp s2 lps25hb) #:use-module (crates-io))

(define-public crate-lps25hb-0.0.1 (c (n "lps25hb") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "07yf14fh0cfl94n0zyrinsdd99src248a3dfd04midi1wg8zqwrk")))

(define-public crate-lps25hb-0.0.2 (c (n "lps25hb") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "092sflzkmknfninziq65l3kqy4c79nc5jgmqvr90nd53r3frrzvy")))

(define-public crate-lps25hb-0.0.3 (c (n "lps25hb") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0r0akc35c06wwb5gqr9qpgl0chfkibp7pyi9m144mnwqg7m5mvim") (y #t)))

(define-public crate-lps25hb-0.0.4 (c (n "lps25hb") (v "0.0.4") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0nl8m0sw7hzrg04cjchxxxza3z06pvszl9cvq4mvvpf3qygh28md")))

(define-public crate-lps25hb-0.1.0 (c (n "lps25hb") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0qfz6yk9sb3gz463dnpj5jjlcgpdr70fj0qi2w6i40dgl1afnwq4")))

