(define-module (crates-io lp re lprefix) #:use-module (crates-io))

(define-public crate-lprefix-0.1.0 (c (n "lprefix") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "02gwnl2yshv4f5xs028gg7pf8ccmph3mcvm77ws3izwmiw07xj6m") (y #t)))

(define-public crate-lprefix-0.1.1 (c (n "lprefix") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1ikwxmwzrjm3flyyx6l4y07g4dsqmq0zpsayn8anmzhkjdxfpawa")))

