(define-module (crates-io lp c1 lpc11xx-async-hal) #:use-module (crates-io))

(define-public crate-lpc11xx-async-hal-0.1.0 (c (n "lpc11xx-async-hal") (v "0.1.0") (d (list (d (n "core") (r "^0.1.0") (d #t) (k 0) (p "core-futures-stateless")) (d (n "lpc11xx") (r "^0.1") (d #t) (k 0)))) (h "0vn1wqydhzp3pk3rz9jm5zagal5npkzvnk819y32lwf707hkglqd") (f (quote (("rt" "lpc11xx/rt"))))))

(define-public crate-lpc11xx-async-hal-0.1.1 (c (n "lpc11xx-async-hal") (v "0.1.1") (d (list (d (n "core") (r "^0.1.0") (d #t) (k 0) (p "core-futures-stateless")) (d (n "lpc11xx") (r "^0.1.4") (d #t) (k 0)))) (h "16p28rkqy2m6hf59rj89c5q1551qmd2adjn5rmxmqk21jxnf6sra") (f (quote (("rt" "lpc11xx/rt"))))))

(define-public crate-lpc11xx-async-hal-0.1.2 (c (n "lpc11xx-async-hal") (v "0.1.2") (d (list (d (n "core") (r "^0.1.0") (d #t) (k 0) (p "core-futures-stateless")) (d (n "lpc11xx") (r "^0.1.4") (d #t) (k 0)))) (h "0ncxgrrhi1qhlzwih6627slndh3vmjgnkpbsx8mqy3g0pzm84jzk") (f (quote (("rt" "lpc11xx/rt"))))))

(define-public crate-lpc11xx-async-hal-0.1.3 (c (n "lpc11xx-async-hal") (v "0.1.3") (d (list (d (n "core") (r "^0.1.0") (d #t) (k 0) (p "core-futures-stateless")) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "lpc11xx") (r "^0.1.4") (d #t) (k 0)))) (h "0cgsz9f7a3i3vaxy4g3cyvmz4hc08xx485wkwfzppi9hxwd0i1p7") (f (quote (("rt" "lpc11xx/rt"))))))

