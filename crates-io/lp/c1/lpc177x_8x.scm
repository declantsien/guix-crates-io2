(define-module (crates-io lp c1 lpc177x_8x) #:use-module (crates-io))

(define-public crate-lpc177x_8x-0.1.0 (c (n "lpc177x_8x") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0n7lzlz7xr64dvjqq245sbgp7gqlad8g1ihnqnhm82abhbfbrwpk")))

(define-public crate-lpc177x_8x-0.13.1 (c (n "lpc177x_8x") (v "0.13.1") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.6") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "048wc2l724zk4pjs9hxrbfdi93ansbcc2k37y2rjz0snlvy6vi9c")))

(define-public crate-lpc177x_8x-0.13.2 (c (n "lpc177x_8x") (v "0.13.2") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.6") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0ap5zlwhn2js1k034zkdsr3s9l3fqj2bwbn7i94clvhvjywm4f1m")))

(define-public crate-lpc177x_8x-0.13.3 (c (n "lpc177x_8x") (v "0.13.3") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0kvgml5y9cimpi2k8v1vzsb3vrrc48l0i6vnffzzr1spz52xf25i") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc177x_8x-0.13.4 (c (n "lpc177x_8x") (v "0.13.4") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1rg9vf37rgijm5jr9zydilbj5399vgkqks4db7al3xbhxwdnd1rp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc177x_8x-0.13.5 (c (n "lpc177x_8x") (v "0.13.5") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0jaqv6hjjca3lxyyz81sq4bviraqlidal661vkhanqcsya6aijha") (f (quote (("rt" "cortex-m-rt/device"))))))

