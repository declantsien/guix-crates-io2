(define-module (crates-io lp c1 lpc11uxx-hal) #:use-module (crates-io))

(define-public crate-lpc11uxx-hal-0.1.0 (c (n "lpc11uxx-hal") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "lpc11uxx") (r "^0.2") (d #t) (k 0)))) (h "1i3c4q2ayjlws1gr9iqigp0v1lkc5glspfvsnd97l78p2fr1cadn")))

