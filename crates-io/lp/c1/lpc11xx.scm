(define-module (crates-io lp c1 lpc11xx) #:use-module (crates-io))

(define-public crate-lpc11xx-0.1.0 (c (n "lpc11xx") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "00mm2i3ppfi63871a6q3jp4rqis4fybxhmp6zayf1sk6j5wqibd0") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc11xx-0.1.1 (c (n "lpc11xx") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "04lxazd6ny0af9n0lg1a2l86fjy7wqp570y3l25yaa4cirdq48h7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc11xx-0.1.2 (c (n "lpc11xx") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1crisgzzcma5ay6zm7rcbvm52qpa0xvnwyd6n3g3kzzhzsrj9i8i") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc11xx-0.1.4 (c (n "lpc11xx") (v "0.1.4") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1bia4zwy6j4kjp3g5qy4gsq7qqq00bv1qs9k5slwp39wkls2jkkp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc11xx-0.2.0 (c (n "lpc11xx") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "16klk9sadh2ad1fhsd5ig3qhkn9jpch0cnr5m2qxsfyrmngqz1sw") (f (quote (("rt" "cortex-m-rt/device"))))))

