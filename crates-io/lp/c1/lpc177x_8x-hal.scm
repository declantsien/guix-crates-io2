(define-module (crates-io lp c1 lpc177x_8x-hal) #:use-module (crates-io))

(define-public crate-lpc177x_8x-hal-0.1.0 (c (n "lpc177x_8x-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lpc177x_8x") (r "^0.13.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "030wkk5wgv3kxijx09d69raz7qr7zs8w18ayv6h7xmgc739za40y") (f (quote (("rt" "lpc177x_8x/rt") ("default" "LQFP208" "rt") ("LQFP208"))))))

(define-public crate-lpc177x_8x-hal-0.1.1 (c (n "lpc177x_8x-hal") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lpc177x_8x") (r "^0.13.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "026c72vc82vhm6qpyx0ka6fhdq923v1hkc3apqwfwy50vcbr1nyk") (f (quote (("rt" "lpc177x_8x/rt") ("default" "LQFP208" "rt") ("LQFP208"))))))

(define-public crate-lpc177x_8x-hal-0.1.2 (c (n "lpc177x_8x-hal") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lpc177x_8x") (r "^0.13.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1vkj8cjdzgd3rhrywi8if1np0v2d9ff2zclkcdxwndcfa0wy4cs3") (f (quote (("rt" "lpc177x_8x/rt") ("default" "LQFP208" "rt") ("LQFP208"))))))

(define-public crate-lpc177x_8x-hal-0.3.0 (c (n "lpc177x_8x-hal") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lpc177x_8x") (r "^0.13.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1xks0vwh7c14gqjs60qjsaxskmgc9nkpwzb0s86kw755h2yhl5s7") (f (quote (("rt" "lpc177x_8x/rt") ("default" "LQFP208" "rt") ("LQFP208"))))))

(define-public crate-lpc177x_8x-hal-0.4.0 (c (n "lpc177x_8x-hal") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lpc177x_8x") (r "^0.13.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "142xaca412rk9qkjbd6i8x1svc5cdaskyqh3p612zphgqmscfcrp") (f (quote (("rt" "lpc177x_8x/rt") ("default" "LQFP208" "rt") ("LQFP208"))))))

