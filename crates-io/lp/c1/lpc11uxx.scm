(define-module (crates-io lp c1 lpc11uxx) #:use-module (crates-io))

(define-public crate-lpc11uxx-0.1.0 (c (n "lpc11uxx") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1gb605ngjfbj7r99qqvli2qji3bmi4hnxg2qfx723spyqvpsxvjv")))

(define-public crate-lpc11uxx-0.2.0 (c (n "lpc11uxx") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1wh3j04szih4n0qq7dlzx41ynxa6znr3935c7ws0x272mhirgc6a")))

(define-public crate-lpc11uxx-0.3.0 (c (n "lpc11uxx") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0s967dvnyi4kvwfxx7fimjamvm12n857sllcrs58a85fgqvkl1qh") (f (quote (("rt" "cortex-m-rt/device"))))))

