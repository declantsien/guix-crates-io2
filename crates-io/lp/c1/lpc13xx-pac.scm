(define-module (crates-io lp c1 lpc13xx-pac) #:use-module (crates-io))

(define-public crate-lpc13xx-pac-0.2.1 (c (n "lpc13xx-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0awzdy8ss230r5pi0g7f680i7pjrk3l7ixjiw8kg7xj2nicg4acl") (f (quote (("rt" "cortex-m-rt/device") ("lpc1343") ("lpc1342") ("lpc1313") ("lpc1311") ("default" "rt"))))))

