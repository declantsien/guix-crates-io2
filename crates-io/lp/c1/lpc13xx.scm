(define-module (crates-io lp c1 lpc13xx) #:use-module (crates-io))

(define-public crate-lpc13xx-0.0.0 (c (n "lpc13xx") (v "0.0.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "10rch1v5rwakcc6vmg2aizf4m6pbcnf1jbg7pnaw88h6dpzsjk8n") (f (quote (("rt" "cortex-m-rt"))))))

