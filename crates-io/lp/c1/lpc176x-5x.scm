(define-module (crates-io lp c1 lpc176x-5x) #:use-module (crates-io))

(define-public crate-lpc176x-5x-0.1.0 (c (n "lpc176x-5x") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "04yrs3da9yhp3vmbdcgwxilvv8lx7wwwdnf8ckk176klrxaf7pzi") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc176x-5x-0.2.0 (c (n "lpc176x-5x") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0zwbxs6cg1vc99lgpnpm67llx72phri0c53p63z015zbwhyw50cp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc176x-5x-0.2.1 (c (n "lpc176x-5x") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1mcfldvl6i46p2hfky0jf983836d8rj6hlr1fybrwdrvq0djv772") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc176x-5x-0.3.0 (c (n "lpc176x-5x") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1740jic6iiwgc5qk6khrdd61jd95pkrgwn91nxa6km6pk5c1zy33") (f (quote (("rt-no-cortex") ("rt" "rt-no-cortex" "cortex-m-rt/device"))))))

(define-public crate-lpc176x-5x-0.3.1 (c (n "lpc176x-5x") (v "0.3.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1r2d3b6dmg8smq0jsvn9hvpalb55pyb2xf52yj707kbqpi2zc6gy") (f (quote (("rt-no-cortex") ("rt" "rt-no-cortex" "cortex-m-rt/device"))))))

