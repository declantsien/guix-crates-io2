(define-module (crates-io lp c1 lpc1769) #:use-module (crates-io))

(define-public crate-lpc1769-0.0.1 (c (n "lpc1769") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1as9cw4afk5vwdc4jkladlr4lh54i9dkh10mayla9qi1yravksmz") (f (quote (("rt" "cortex-m-rt/device"))))))

