(define-module (crates-io lp #{55}# lp55231) #:use-module (crates-io))

(define-public crate-lp55231-0.1.0 (c (n "lp55231") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1g1n3wwjrrz2py4xcap2acjbqq8pz7hn3pnqyycic1v5cqdwaqjx")))

(define-public crate-lp55231-0.2.0 (c (n "lp55231") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1xk5397nzllq90iny3dcb21cdw4w974fa08yfhiwmprcqvb6rlk7")))

(define-public crate-lp55231-0.2.1 (c (n "lp55231") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "06330yb3kbhl50lzvn9ci9vb1q8vib55b11mqzbxjqnq5wqbpbzp")))

