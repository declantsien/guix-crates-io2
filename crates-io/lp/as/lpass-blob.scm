(define-module (crates-io lp as lpass-blob) #:use-module (crates-io))

(define-public crate-lpass-blob-0.1.0 (c (n "lpass-blob") (v "0.1.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "08w96dixwqba5ah5iia77w1p8czcr9amij2nc146kra9k2wqcdvf")))

