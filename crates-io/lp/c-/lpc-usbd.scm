(define-module (crates-io lp c- lpc-usbd) #:use-module (crates-io))

(define-public crate-lpc-usbd-0.1.0 (c (n "lpc-usbd") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.8") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1ghks6qv523zvzslldakva1054a8s91mywxj4xh7216k14q2rhxq")))

