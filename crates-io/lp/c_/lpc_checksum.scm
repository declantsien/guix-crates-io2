(define-module (crates-io lp c_ lpc_checksum) #:use-module (crates-io))

(define-public crate-lpc_checksum-0.1.0 (c (n "lpc_checksum") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vl1q93sq3hj037csqgcb72wiy4vj8gn5a6xs3w4jj5cj0d8sz3v")))

