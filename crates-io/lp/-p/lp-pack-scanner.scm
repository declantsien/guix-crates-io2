(define-module (crates-io lp -p lp-pack-scanner) #:use-module (crates-io))

(define-public crate-lp-pack-scanner-0.1.0 (c (n "lp-pack-scanner") (v "0.1.0") (h "1fcfsqfp2b1w4mvxkn0r4fgks6f6yyjhkh139bbz1z9m6my927kn")))

(define-public crate-lp-pack-scanner-0.2.0 (c (n "lp-pack-scanner") (v "0.2.0") (h "0dqg2s771w14f2sdvkjdq95x71x4h4xf7s6zakgpsxc0ssi9nrkn")))

(define-public crate-lp-pack-scanner-0.3.0 (c (n "lp-pack-scanner") (v "0.3.0") (h "09jx22jjpd4v4yrqwmjxqxkzgb0lslwc8bnphj431iay5k2j312r")))

(define-public crate-lp-pack-scanner-0.4.0 (c (n "lp-pack-scanner") (v "0.4.0") (h "0nckzvsijk3r55rdzzfhg5r7d84x47ylzyrjhpl15xqr99sg5kp3")))

