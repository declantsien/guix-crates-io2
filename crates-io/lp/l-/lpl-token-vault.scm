(define-module (crates-io lp l- lpl-token-vault) #:use-module (crates-io))

(define-public crate-lpl-token-vault-0.1.1 (c (n "lpl-token-vault") (v "0.1.1") (d (list (d (n "borsh") (r "~0.9.1") (d #t) (k 0)) (d (n "num-derive") (r "~0.3") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "safe-token") (r "^3.3.0") (d #t) (k 0)) (d (n "safecoin-program") (r "^1.10.32") (d #t) (k 0)) (d (n "shank") (r "~0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)))) (h "1zlvb5j3gg6jgqcdc6l737gynpx6y1k6y74ipv5zljfx4n2hnzv9") (f (quote (("test-bpf") ("no-entrypoint"))))))

(define-public crate-lpl-token-vault-0.1.2 (c (n "lpl-token-vault") (v "0.1.2") (d (list (d (n "borsh") (r "~0.9.1") (d #t) (k 0)) (d (n "num-derive") (r "~0.3") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "safe-token") (r "^3.3.0") (d #t) (k 0)) (d (n "safecoin-program") (r "^1.10.34") (d #t) (k 0)) (d (n "shank") (r "~0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)))) (h "1rflbkzhkw1haqgzz2zgsk13c9v2llkwn7iy8zb1w0gx5aly59kk") (f (quote (("test-bpf") ("no-entrypoint"))))))

