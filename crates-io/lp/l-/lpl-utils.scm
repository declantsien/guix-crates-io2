(define-module (crates-io lp l- lpl-utils) #:use-module (crates-io))

(define-public crate-lpl-utils-0.1.0 (c (n "lpl-utils") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "safe-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (o #t) (d #t) (k 0)) (d (n "safecoin-program") (r "^1.13.5") (d #t) (k 0)))) (h "1yv3dxr8zn65vjjiz3gv94kv5cmkzq0r3izgaq087kwd89ksiqg0") (f (quote (("token" "safe-token") ("default" "token"))))))

