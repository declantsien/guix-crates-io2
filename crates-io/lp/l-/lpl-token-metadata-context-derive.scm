(define-module (crates-io lp l- lpl-token-metadata-context-derive) #:use-module (crates-io))

(define-public crate-lpl-token-metadata-context-derive-0.2.1 (c (n "lpl-token-metadata-context-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "13rfps31p20w38mpkb63ykjsrfl9c32vykzy2x85q24xfxx2nkns")))

