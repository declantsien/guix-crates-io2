(define-module (crates-io lp c5 lpc55s6x-pac) #:use-module (crates-io))

(define-public crate-lpc55s6x-pac-0.0.1 (c (n "lpc55s6x-pac") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1zpgv6yqk2f3gs5k0gh5077dnj5mf9ix96b55xqfj8qybsw6g5y3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc55s6x-pac-0.0.2 (c (n "lpc55s6x-pac") (v "0.0.2") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1rn4h4fqis10ddngl2sys5ax98adc6ska389gs0i630l8yli98d6") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc55s6x-pac-0.0.3 (c (n "lpc55s6x-pac") (v "0.0.3") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1i2a2ig1mzv11yrip7lqja12af037ccn02gr305y30amy0c3s4wi") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc55s6x-pac-0.0.4 (c (n "lpc55s6x-pac") (v "0.0.4") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "15zl4nxpcz55liccpj43n9ipbq15fkkaxwia7pdxkid88i2r2k3f") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc55s6x-pac-0.0.5 (c (n "lpc55s6x-pac") (v "0.0.5") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1v07262906z8m4kvqmzjiwv0xxnbw4knnwklxpx13ypjwc1rqy14") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc55s6x-pac-0.0.6 (c (n "lpc55s6x-pac") (v "0.0.6") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1nca1fpxvinyq1ymh5syz495cq0qmlawqy25aijmyrp85kp9yd20") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc55s6x-pac-0.0.7 (c (n "lpc55s6x-pac") (v "0.0.7") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0x5l69wghxxlkxnnw576291s2xwirsk5i0m06lp8jd4dysib71d1") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc55s6x-pac-0.0.8 (c (n "lpc55s6x-pac") (v "0.0.8") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0diqx925nbgnwngb7cgvaz5kmfxan6m625mma54h1ahs7qpsrf97") (f (quote (("rt" "cortex-m-rt/device"))))))

