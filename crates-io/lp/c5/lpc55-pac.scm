(define-module (crates-io lp c5 lpc55-pac) #:use-module (crates-io))

(define-public crate-lpc55-pac-0.1.0 (c (n "lpc55-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0c1swlbpw3c4gbxcrrjvyh72kwnkk4ha8y64w6lbrffhggcrqagc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc55-pac-0.1.1 (c (n "lpc55-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rmkg70y0jsgl64mqwywwdjcrzkj8yajzkhwnas6nylj3lgvavic") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-lpc55-pac-0.2.0 (c (n "lpc55-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "005hicni3yk6fsrs7qx30x1zd876406dx1iihjfpsjzq7qbf0bvl") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc55-pac-0.3.0 (c (n "lpc55-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1ar5dsknwhlylsaj6b0j48yavcbna6avn6jrpib2v8h6sn8pbr7f") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc55-pac-0.4.0 (c (n "lpc55-pac") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1g94485gaza478gk4yzj41d2xq7j1lq6zxraql35av9dvwy55ky3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc55-pac-0.4.1 (c (n "lpc55-pac") (v "0.4.1") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1lv5dnpa3ajam1ksd31jx5wz8xsillam2bw88mhm5bqkscr5n6yg") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc55-pac-0.5.0 (c (n "lpc55-pac") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "14az01hyrl1mbszc1rpk3ns0xf9rz4rp7j5vls17x7lxxnx54j9s") (f (quote (("rt" "cortex-m-rt/device"))))))

