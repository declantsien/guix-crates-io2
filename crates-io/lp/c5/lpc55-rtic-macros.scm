(define-module (crates-io lp c5 lpc55-rtic-macros) #:use-module (crates-io))

(define-public crate-lpc55-rtic-macros-0.5.3 (c (n "lpc55-rtic-macros") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1066d4qkmgg9n79cmrq0snaqv6lfs8pbnc4qg3q0xxwb8pb5hj8w") (f (quote (("homogeneous") ("heterogeneous"))))))

