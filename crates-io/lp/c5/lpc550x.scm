(define-module (crates-io lp c5 lpc550x) #:use-module (crates-io))

(define-public crate-lpc550x-0.1.0 (c (n "lpc550x") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "04hw3wqldy1v4qxxzh4s2sm3vpifyvaaw0rzpbxjxg4mw7ng4md9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc550x-0.2.0 (c (n "lpc550x") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "17ckh8ja2c4siw7kqnndqyb2aq8fg7n8s38syjx76ys7lyybflmw") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-lpc550x-0.3.0 (c (n "lpc550x") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1p0izpi2z5swgha8xkmi3qry663zhxfn3vrzpa4921lrw842i1by") (f (quote (("rt" "cortex-m-rt/device"))))))

