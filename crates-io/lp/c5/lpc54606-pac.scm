(define-module (crates-io lp c5 lpc54606-pac) #:use-module (crates-io))

(define-public crate-lpc54606-pac-0.1.0 (c (n "lpc54606-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0lvibwc71i9d9a9a3x8gmd40g3vw99g81l7xsfd1kpabs4mpwgwm") (f (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

