(define-module (crates-io ym lf ymlfxr) #:use-module (crates-io))

(define-public crate-ymlfxr-0.3.2 (c (n "ymlfxr") (v "0.3.2") (d (list (d (n "bytebuffer") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ulid") (r "^0.3.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0mzs4rhiz30183r0aal9qscw91hc6kx0gy85kwi7z809gg7sfzsp")))

