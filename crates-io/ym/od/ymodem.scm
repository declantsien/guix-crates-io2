(define-module (crates-io ym od ymodem) #:use-module (crates-io))

(define-public crate-ymodem-0.1.0 (c (n "ymodem") (v "0.1.0") (d (list (d (n "crc16") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^2.0") (d #t) (k 2)))) (h "1cmsy4173n5ghq65jid2yibg8cjm887g1kyydwkadvnzwp0yhbg5")))

(define-public crate-ymodem-0.1.1 (c (n "ymodem") (v "0.1.1") (d (list (d (n "crc16") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^2.0") (d #t) (k 2)))) (h "1caxl0qb0va4cq7h082c42fqcf08d449v09i2jw1vyz764yw1ilj")))

