(define-module (crates-io ym lc ymlctx) #:use-module (crates-io))

(define-public crate-ymlctx-0.0.1 (c (n "ymlctx") (v "0.0.1") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.5") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1ckj9cs1h8q6c5lckcj685mddxr2rjm8dhhkv0cv3kgqbzcwyk19")))

(define-public crate-ymlctx-0.0.2 (c (n "ymlctx") (v "0.0.2") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.5") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1d1yaik181hwg143sddarjfy17blakaf25sjkaw8ij9hmaafz97r")))

(define-public crate-ymlctx-0.0.3 (c (n "ymlctx") (v "0.0.3") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.5") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "12n2ncwhaaj5n1hdqfvzn7m641vb2a2c81f4422n3ixmj6hvg1kd")))

(define-public crate-ymlctx-0.0.4 (c (n "ymlctx") (v "0.0.4") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.5") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0sc5xrm99068mhx1k31n18dr5cb328ywyzbg5ix1dd1424qz21sw")))

(define-public crate-ymlctx-0.0.5 (c (n "ymlctx") (v "0.0.5") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.5") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "17dc3bm5ncz68vl3lllnaqy0rf3d7r8s96ydagghsx6l5w2jc9fc")))

(define-public crate-ymlctx-0.0.6 (c (n "ymlctx") (v "0.0.6") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.5") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "08d5gd2hjivjlzhr8ikcxyiy8a0j096sp4vazxdzlqkc80xkn2y3")))

(define-public crate-ymlctx-0.0.7 (c (n "ymlctx") (v "0.0.7") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.5") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0z2829lwzj1qip06raz79vhx9gv640ql1k2vx4h981qi3lkd8d2v")))

(define-public crate-ymlctx-0.0.8 (c (n "ymlctx") (v "0.0.8") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.5") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0f37yzjznrfpfry7g5hyc8xqgchdzygl4w8i1h2rc0ks4r17kdl7")))

(define-public crate-ymlctx-0.0.9 (c (n "ymlctx") (v "0.0.9") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.5") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0pygr4k1dszjp0q2pz21430m95xvv8shc48dp1l5fmydn127pknn")))

(define-public crate-ymlctx-0.0.10 (c (n "ymlctx") (v "0.0.10") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.5") (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0ndns3d64r97vr53kx1pplh8j49ziyy3rm1cmj2ndyppn8ni3z7p")))

(define-public crate-ymlctx-0.1.0 (c (n "ymlctx") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1m8v01k70r6qhzsja7zm1337xq8gk7lvx0sq71xh4ydyl3axki5r") (f (quote (("topyobject" "pyo3") ("default"))))))

(define-public crate-ymlctx-0.1.1 (c (n "ymlctx") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1pd036xar0ihl4hziqdbjsljh4dlc82f7qz2bxjwy3r8wgkidzf3") (f (quote (("topyobject" "pyo3") ("default"))))))

(define-public crate-ymlctx-0.1.2 (c (n "ymlctx") (v "0.1.2") (d (list (d (n "pyo3") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1nk6g6l7drk4v91mh8kf32i0xxk9jv84ilj4jk48iv70z8k87qy5") (f (quote (("topyobject" "pyo3") ("default"))))))

(define-public crate-ymlctx-0.1.3 (c (n "ymlctx") (v "0.1.3") (d (list (d (n "pyo3") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "17nqdmvyifcv6vlh6qvd6kyxwa84rfiwsksyi3n2nql8s7vh4nbb") (f (quote (("topyobject" "pyo3") ("default"))))))

(define-public crate-ymlctx-0.1.4 (c (n "ymlctx") (v "0.1.4") (d (list (d (n "pyo3") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0p5rphm0w7vc87ps0x0w3zglcvghy7v9zindkv4g6wr2j63xbjqk") (f (quote (("topyobject" "pyo3") ("default"))))))

(define-public crate-ymlctx-0.1.5 (c (n "ymlctx") (v "0.1.5") (d (list (d (n "pyo3") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1jzfbbzgwcsarlp1ddbhr56sakbdjzm5cbyqq4yl349vs9fb4bdx") (f (quote (("topyobject" "pyo3") ("default"))))))

(define-public crate-ymlctx-0.1.6 (c (n "ymlctx") (v "0.1.6") (d (list (d (n "pyo3") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0q7m9mym8rclp22wy2lsjqv05n6i5hz460jazm219j08kc0fny6s") (f (quote (("topyobject" "pyo3") ("default"))))))

(define-public crate-ymlctx-0.1.7 (c (n "ymlctx") (v "0.1.7") (d (list (d (n "pyo3") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0l1wgj1hwfvmz3xiy4rd4yh025wi21hik11iagbrin1j0rfnczmk") (f (quote (("topyobject" "pyo3") ("default"))))))

(define-public crate-ymlctx-0.1.8 (c (n "ymlctx") (v "0.1.8") (d (list (d (n "pyo3") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "rpds") (r "^0.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.0") (d #t) (k 0)))) (h "0k36rbzac04wgl2cg9rb6mgv361v8701ps9lh0dcxh9jylr07m5m") (f (quote (("topyobject" "pyo3") ("default"))))))

