(define-module (crates-io ym l_ yml_to_ron) #:use-module (crates-io))

(define-public crate-yml_to_ron-0.1.0 (c (n "yml_to_ron") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5.3") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0p79sqp7ivjqqa5xkiaygv37j1c54sxbd45fcyvx80p1rk2q6ijw")))

