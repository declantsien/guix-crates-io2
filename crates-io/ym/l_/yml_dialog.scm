(define-module (crates-io ym l_ yml_dialog) #:use-module (crates-io))

(define-public crate-yml_dialog-0.2.3 (c (n "yml_dialog") (v "0.2.3") (d (list (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)))) (h "06wwjx5rvj5h36mf4nh198zrsn9jlhzs4f961q47mfxrjhzv9w1i")))

