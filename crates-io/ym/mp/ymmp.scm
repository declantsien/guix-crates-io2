(define-module (crates-io ym mp ymmp) #:use-module (crates-io))

(define-public crate-ymmp-0.1.0 (c (n "ymmp") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1skfyfmr5y59jdnqgqbvc1hqj67a2as2gxgh4y0xjqpi5mp1nnj7")))

(define-public crate-ymmp-0.1.1 (c (n "ymmp") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0098lh7y7jd6hhc5b1cbq77753pkc8r7758gxjh74ycbzrixbk2z")))

(define-public crate-ymmp-0.1.2 (c (n "ymmp") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0l4pr6slancvg4a1h6bzh68igaxq3dgp0bdwyvx8hlvnhqafarlf")))

(define-public crate-ymmp-0.1.3 (c (n "ymmp") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gs0nwqb4hpyygdlg94slxz4sj7awj4siif43sasnybhjjvm5acq")))

(define-public crate-ymmp-0.1.4 (c (n "ymmp") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0qzw72xwyjjkgrlgm6pml22nj127mdyfr9g2p1hm54wq8dnmg98k")))

