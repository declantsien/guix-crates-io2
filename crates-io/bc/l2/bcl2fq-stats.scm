(define-module (crates-io bc l2 bcl2fq-stats) #:use-module (crates-io))

(define-public crate-bcl2fq-stats-0.1.0 (c (n "bcl2fq-stats") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "08c84w9291a1iy9vq0jxmh7v6w54yv5gnj7a125sp63ck9yqagym")))

(define-public crate-bcl2fq-stats-0.1.1 (c (n "bcl2fq-stats") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0igfbm1jcnvmd8mkcxgnrppyn3yac023x233lrbz4wwagyg39qxc")))

