(define-module (crates-io bc ry bcrypt-small) #:use-module (crates-io))

(define-public crate-bcrypt-small-0.1.0 (c (n "bcrypt-small") (v "0.1.0") (d (list (d (n "bcrypt-only") (r "^0.3.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.1.10") (f (quote ("std"))) (d #t) (k 0)))) (h "0fpnrcz09mnd07q39yfwy6i68zfdx7f5657shj8lpppmcg2mdvf7")))

(define-public crate-bcrypt-small-0.1.1 (c (n "bcrypt-small") (v "0.1.1") (d (list (d (n "bcrypt-only") (r "^0.3.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.1.10") (f (quote ("std"))) (d #t) (k 0)))) (h "1zakb2yvi2840ajby4bnzn1vx7g6j33akaa9g5wvyarnxdb61jjg")))

