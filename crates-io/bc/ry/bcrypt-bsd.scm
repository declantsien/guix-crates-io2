(define-module (crates-io bc ry bcrypt-bsd) #:use-module (crates-io))

(define-public crate-bcrypt-bsd-0.1.0 (c (n "bcrypt-bsd") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.31") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "13mbmqxmp638m373nvxw4rvcfga6w20i17m4mj6b5aq2krigy0wl")))

(define-public crate-bcrypt-bsd-0.1.1 (c (n "bcrypt-bsd") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.31") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "1gv2n2fwmpz5qhp8vzhjrdlbvxx8zhmnjxkv6z2x3cw1kbxaw4aw")))

(define-public crate-bcrypt-bsd-0.1.2 (c (n "bcrypt-bsd") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.31") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "158kcwqj261h0r9rxn7sy9f69hiaxpng5gh00qkdhx7faqb5g9v2")))

(define-public crate-bcrypt-bsd-0.1.3 (c (n "bcrypt-bsd") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.31") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "1dhhzki2kign7xrrvdbn54rdj75sfi9i7in62r49x15p4v8k5a5c")))

