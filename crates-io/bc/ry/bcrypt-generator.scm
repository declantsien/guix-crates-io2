(define-module (crates-io bc ry bcrypt-generator) #:use-module (crates-io))

(define-public crate-bcrypt-generator-0.1.0 (c (n "bcrypt-generator") (v "0.1.0") (d (list (d (n "bcrypt") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml" "suggestions" "color"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "0lki3k963gyb3wvjavq283hf7708dd17r4p4ajf2qb18hzrj8nmk")))

