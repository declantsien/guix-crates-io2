(define-module (crates-io bc ry bcrypt-only) #:use-module (crates-io))

(define-public crate-bcrypt-only-0.1.0 (c (n "bcrypt-only") (v "0.1.0") (h "1h0sb3i7558chrdjz78vq2609qq8iv26l88gsvczz77k81fjib1x")))

(define-public crate-bcrypt-only-0.2.0 (c (n "bcrypt-only") (v "0.2.0") (h "0ax4z2kkawikrv6qm492ynl3qrqss8s70qq26wlnw58mbrhf6fg0")))

(define-public crate-bcrypt-only-0.3.0 (c (n "bcrypt-only") (v "0.3.0") (h "1k8pvl7k5g94vymgq3cvxhqy23xklv6p30kir3pxkjld8r971jav") (y #t)))

(define-public crate-bcrypt-only-0.3.1 (c (n "bcrypt-only") (v "0.3.1") (h "1pm8nmccvbd6d0csjj8imawgzrsmlb30q12rngwcl9m5l49lcj8x") (f (quote (("std"))))))

