(define-module (crates-io bc ry bcrypter) #:use-module (crates-io))

(define-public crate-bcrypter-0.1.0 (c (n "bcrypter") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "blowfish") (r "^0.3.0") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1sxj3kvx6by67khld605r9bwlmnzc7vifsj67adr3g3qfb31z30x")))

(define-public crate-bcrypter-0.1.1 (c (n "bcrypter") (v "0.1.1") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "blowfish") (r "^0.3.0") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "16hzvmfpb2s7jinr2hqrkqnmjp9fjigy44s2pgdn2wg9bjcal2np")))

