(define-module (crates-io bc ry bcrypt-tune) #:use-module (crates-io))

(define-public crate-bcrypt-tune-1.0.0 (c (n "bcrypt-tune") (v "1.0.0") (d (list (d (n "bcrypt") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (d #t) (k 0)))) (h "0iq1apnjmdvsjmcwi3ihfpj8mllpma2klngncxyiiswc5y85hjvk")))

(define-public crate-bcrypt-tune-1.1.0 (c (n "bcrypt-tune") (v "1.1.0") (d (list (d (n "bcrypt") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.1.2") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (d #t) (k 0)))) (h "17xspsfqhm5hjhvqpkaf7m2kqfkqm9a8193hpzyrds5d7jdg81s6")))

