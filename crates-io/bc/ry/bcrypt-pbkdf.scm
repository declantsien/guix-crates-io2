(define-module (crates-io bc ry bcrypt-pbkdf) #:use-module (crates-io))

(define-public crate-bcrypt-pbkdf-0.0.1 (c (n "bcrypt-pbkdf") (v "0.0.1") (d (list (d (n "blowfish") (r "^0.3.0") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "seckey") (r "^0.9.1") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "1cyv769jm6hyygrqp86fxxlsj0zjbh091gz2nbwsmqpy4p1hfrp5") (y #t)))

(define-public crate-bcrypt-pbkdf-0.0.2 (c (n "bcrypt-pbkdf") (v "0.0.2") (d (list (d (n "blowfish") (r "^0.3.0") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "byteorder") (r "^1.2.6") (k 0)) (d (n "seckey") (r "^0.9.1") (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "08w8sfnsvkws0ff7bgmdhlckzq4q1rhli7wnpqgang9a14p8l1xm") (y #t)))

(define-public crate-bcrypt-pbkdf-0.0.3 (c (n "bcrypt-pbkdf") (v "0.0.3") (d (list (d (n "blowfish") (r "^0.3.0") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (k 0)) (d (n "clear_on_drop") (r "^0.2.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (k 0)))) (h "01sd69262n504cxfmpdasg8pbza6hlidpppw4nv3vg0h7vw77683") (y #t)))

(define-public crate-bcrypt-pbkdf-0.0.4 (c (n "bcrypt-pbkdf") (v "0.0.4") (d (list (d (n "blowfish") (r "^0.3.0") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (k 0)) (d (n "clear_on_drop") (r "^0.2.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (k 0)))) (h "02cqppa0r9zz6pyy0wvfflwv25an157sfvc06zhp97fsh3wk0s22") (y #t)))

(define-public crate-bcrypt-pbkdf-0.1.0 (c (n "bcrypt-pbkdf") (v "0.1.0") (d (list (d (n "blowfish") (r "^0.4") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.7") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.3") (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "04w366sfzrpx0152bc9g7fx9j5q94pgjg3q3bf3i46ch53b6g3hh")))

(define-public crate-bcrypt-pbkdf-0.2.0 (c (n "bcrypt-pbkdf") (v "0.2.0") (d (list (d (n "blowfish") (r "^0.5") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.8") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.4") (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "1vqcgdhxf0caf577al5l9pg8xnyfrwkzhk4vkzscrjjmhgvss49m")))

(define-public crate-bcrypt-pbkdf-0.2.1 (c (n "bcrypt-pbkdf") (v "0.2.1") (d (list (d (n "blowfish") (r "^0.5") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.8") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.4") (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "1n9hzv12y7hywkyj21mvvf6nxyqsb73f1jfzqy5gxivmqsks7vv1") (f (quote (("std") ("default" "std"))))))

(define-public crate-bcrypt-pbkdf-0.3.0 (c (n "bcrypt-pbkdf") (v "0.3.0") (d (list (d (n "blowfish") (r "^0.6") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.9") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.5") (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "078xmds2lis6whnhrpr4hwq59h5ri52ifa4agbrlkmacqq8b0izj") (f (quote (("std") ("default" "std"))))))

(define-public crate-bcrypt-pbkdf-0.4.0 (c (n "bcrypt-pbkdf") (v "0.4.0") (d (list (d (n "blowfish") (r "^0.7") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.10") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.6.0") (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "1dahg8z2v3cns14cvgc7mz0li2g1vvcxb1c8n5lljq9xdkjbpmbh") (f (quote (("std") ("default" "std"))))))

(define-public crate-bcrypt-pbkdf-0.5.0 (c (n "bcrypt-pbkdf") (v "0.5.0") (d (list (d (n "blowfish") (r "^0.7") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.10") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.7") (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "02b3wcd3j2ix680pa1d5y0x7i1rxhw6zfybhx80d5gmcwkmmfg5c") (f (quote (("std") ("default" "std"))))))

(define-public crate-bcrypt-pbkdf-0.6.0 (c (n "bcrypt-pbkdf") (v "0.6.0") (d (list (d (n "blowfish") (r "^0.7") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.11") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "0sc5zmqwpkqxlx85q7ayym6r4x10kp3p791psjczq3h2nygfaia6") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-bcrypt-pbkdf-0.6.1 (c (n "bcrypt-pbkdf") (v "0.6.1") (d (list (d (n "blowfish") (r "^0.8") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.11") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "1fa1xw5c06rdmvh7wycq8ff25ci6j3j1bcyvwnk87cgyhy71nqhj") (f (quote (("std") ("default" "std"))))))

(define-public crate-bcrypt-pbkdf-0.6.2 (c (n "bcrypt-pbkdf") (v "0.6.2") (d (list (d (n "blowfish") (r "^0.8") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.11") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "zeroize") (r "=1.3") (k 0)))) (h "1ms9c5z90n5szx5nbxrqaihny5fs3sl6a1pm3szr5g86jlxw0f3w") (f (quote (("std") ("default" "std"))))))

(define-public crate-bcrypt-pbkdf-0.7.0 (c (n "bcrypt-pbkdf") (v "0.7.0") (d (list (d (n "blowfish") (r "^0.8") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.11") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "zeroize") (r ">=1, <1.5") (k 0)))) (h "13i2cihh33b3dcmwi89qnnjfb7aw76lk0wy3y682sgmshf121nb8") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-bcrypt-pbkdf-0.7.1 (c (n "bcrypt-pbkdf") (v "0.7.1") (d (list (d (n "blowfish") (r "^0.8") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.11") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.9") (k 0)) (d (n "sha2") (r "^0.9") (k 0)) (d (n "zeroize") (r ">=1, <1.5") (k 0)))) (h "0vd7w2lsnk1hfvw4abrc2rlzrhr1qmjwdd51hsmq7jj7398i19m6") (f (quote (("std") ("default" "std"))))))

(define-public crate-bcrypt-pbkdf-0.7.2 (c (n "bcrypt-pbkdf") (v "0.7.2") (d (list (d (n "blowfish") (r "^0.8") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.10") (k 0)) (d (n "sha2") (r "^0.10") (k 0)) (d (n "zeroize") (r ">=1, <1.5") (o #t) (k 0)))) (h "1m8zdrwxjbkidkfl9ldppc6hpi5plh0sd2my1a62h020r2rnbpjb") (f (quote (("std") ("default" "std"))))))

(define-public crate-bcrypt-pbkdf-0.8.0 (c (n "bcrypt-pbkdf") (v "0.8.0") (d (list (d (n "blowfish") (r "^0.9.1") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)) (d (n "pbkdf2") (r "^0.10.1") (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "zeroize") (r ">=1, <1.6") (o #t) (k 0)))) (h "036rc72xih39wml3x347qywnfac1wzjn3w1ljl9s1ziwpzzalz41") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-bcrypt-pbkdf-0.8.1 (c (n "bcrypt-pbkdf") (v "0.8.1") (d (list (d (n "blowfish") (r "^0.9.1") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)) (d (n "pbkdf2") (r "^0.10.1") (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "zeroize") (r ">=1, <1.6") (o #t) (k 0)))) (h "0x10ficqcla5kzn214bchq71iv9113yrw3ib1f1cgfcwz8zj7vzl") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-bcrypt-pbkdf-0.9.0 (c (n "bcrypt-pbkdf") (v "0.9.0") (d (list (d (n "blowfish") (r "^0.9.1") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)) (d (n "pbkdf2") (r "^0.11") (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1ixjh641hjpsn1hxibd2dm6i36mad95a65hnagpfymngc3dsh1iq") (f (quote (("std") ("default" "std")))) (r "1.57")))

(define-public crate-bcrypt-pbkdf-0.10.0 (c (n "bcrypt-pbkdf") (v "0.10.0") (d (list (d (n "blowfish") (r "^0.9.1") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)) (d (n "pbkdf2") (r "^0.12") (k 0)) (d (n "sha2") (r "^0.10.5") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "18pjhsy3m2v0silsp4mjzz8i92zrpqxk9b059zrnk1w8zvhw5ska") (f (quote (("std") ("default" "alloc" "std") ("alloc")))) (r "1.60")))

(define-public crate-bcrypt-pbkdf-0.11.0-pre.0 (c (n "bcrypt-pbkdf") (v "0.11.0-pre.0") (d (list (d (n "blowfish") (r "^0.9.1") (f (quote ("bcrypt"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.0") (d #t) (k 2)) (d (n "pbkdf2") (r "=0.13.0-pre.0") (k 0)) (d (n "sha2") (r "=0.11.0-pre.3") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0g5n76fc9jhqc7xhqwarsmd1z6l3x7gacaimq5jbnk5jr99s04vz") (f (quote (("std") ("default" "alloc" "std") ("alloc")))) (r "1.72")))

