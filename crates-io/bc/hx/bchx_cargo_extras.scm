(define-module (crates-io bc hx bchx_cargo_extras) #:use-module (crates-io))

(define-public crate-bchx_cargo_extras-0.1.0 (c (n "bchx_cargo_extras") (v "0.1.0") (h "179r6g3lk5zvqc0hjyvkzan4d0j1cvlnxirfq3krhnrmkvn8nvl5")))

(define-public crate-bchx_cargo_extras-0.1.1 (c (n "bchx_cargo_extras") (v "0.1.1") (h "00s49r6fwvb54dfhq0gypfydrpgpymdaasly5ywy7qwmpb32qysa")))

