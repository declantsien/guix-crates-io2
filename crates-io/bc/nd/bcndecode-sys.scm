(define-module (crates-io bc nd bcndecode-sys) #:use-module (crates-io))

(define-public crate-bcndecode-sys-0.1.0 (c (n "bcndecode-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1chain323pg1p27kqwr2r0yhlpcs4fwwmjn61kpryknx9yz9rhkv")))

(define-public crate-bcndecode-sys-0.1.1 (c (n "bcndecode-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0s518il4cxx1fipii2kzycq66kv48x6im49517zxvgv0klpgwr4i")))

