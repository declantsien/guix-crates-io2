(define-module (crates-io bc nd bcndecode) #:use-module (crates-io))

(define-public crate-bcndecode-0.1.0 (c (n "bcndecode") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.26") (d #t) (k 0)))) (h "07l3l8pzrjx02sqynpviqi5hdkw28yjx36jgaxjzxji07r7l19cq")))

(define-public crate-bcndecode-0.2.0 (c (n "bcndecode") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.26") (d #t) (k 2)))) (h "1k9qvahz10xy3pp82pzzi31q623fh639g80m4pm86dwrnvirjnyn") (f (quote (("test"))))))

