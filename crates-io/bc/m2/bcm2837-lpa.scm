(define-module (crates-io bc m2 bcm2837-lpa) #:use-module (crates-io))

(define-public crate-bcm2837-lpa-0.1.0 (c (n "bcm2837-lpa") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1rg13d552knc1imsiv9sqg4sxhq8w8jryf5w0x3q5ah3lngmaj2f") (f (quote (("rt")))) (r "1.61.0")))

(define-public crate-bcm2837-lpa-0.2.0 (c (n "bcm2837-lpa") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0smld5cfa0j438a6py2zvs16cfpnpq35kj147l7mk6wvl1b17k4s") (f (quote (("rt")))) (r "1.65.0")))

(define-public crate-bcm2837-lpa-0.2.1 (c (n "bcm2837-lpa") (v "0.2.1") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1s6361g5xl46fy0brbhqymn1c262hv53dbcq5h415pbgq1gfa5fj") (f (quote (("rt")))) (r "1.65.0")))

(define-public crate-bcm2837-lpa-0.2.2 (c (n "bcm2837-lpa") (v "0.2.2") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "116845bjspb2d2xxb2qnk8aw4z26hk5qhzg2rs7d9qym4rhnc7sy") (f (quote (("rt")))) (r "1.65.0")))

(define-public crate-bcm2837-lpa-0.3.0 (c (n "bcm2837-lpa") (v "0.3.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0jjihw4j6qhng1kx2k2s6nbcqblda9d0bz8631bfc32dd8fq27p8") (f (quote (("rt")))) (r "1.65.0")))

(define-public crate-bcm2837-lpa-0.4.0 (c (n "bcm2837-lpa") (v "0.4.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1bh78r440njlqml7fa8n3q288aawq5gkhn2968ahdln3vxv708lk") (f (quote (("rt")))) (r "1.65.0")))

