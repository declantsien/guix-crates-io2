(define-module (crates-io bc m2 bcm2835-lpa) #:use-module (crates-io))

(define-public crate-bcm2835-lpa-0.1.0 (c (n "bcm2835-lpa") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "14md03xqm0w5h2dq5pszxk818pw2kisadpsinyi0h17rcdqjb0mc") (f (quote (("rt")))) (r "1.61.0")))

(define-public crate-bcm2835-lpa-0.2.0 (c (n "bcm2835-lpa") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1hcqhff3i83q1n42xv88vwvp3c62b78mvddb25rm3kls1l3sr8fq") (f (quote (("rt")))) (r "1.65.0")))

(define-public crate-bcm2835-lpa-0.2.1 (c (n "bcm2835-lpa") (v "0.2.1") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1c1c0qrklgpyc63l5idazxn96zjk3lz3f6sq4fzx35zbg7lajmi5") (f (quote (("rt")))) (r "1.65.0")))

(define-public crate-bcm2835-lpa-0.2.2 (c (n "bcm2835-lpa") (v "0.2.2") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1jy4qrxvf8f661h9lhprh9dkfccaqp3dynm14fr3m7nfqc1az0q8") (f (quote (("rt")))) (r "1.65.0")))

(define-public crate-bcm2835-lpa-0.3.0 (c (n "bcm2835-lpa") (v "0.3.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "08r4sr2ryva7nl9qsscdr8jcjyh6qq7c9i750m9xm4hs81w01cvz") (f (quote (("rt")))) (r "1.65.0")))

(define-public crate-bcm2835-lpa-0.4.0 (c (n "bcm2835-lpa") (v "0.4.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "15rsxanjpf7jgmnjj4pjvv7x60r4bjgxw7jgav53c1z97s60r4cp") (f (quote (("rt")))) (r "1.65.0")))

