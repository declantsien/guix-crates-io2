(define-module (crates-io bc m2 bcm2709-spi) #:use-module (crates-io))

(define-public crate-bcm2709-spi-0.1.0 (c (n "bcm2709-spi") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "11mv7621gbzmz81yxzwdc7isamxlg7bfxz17bj2sdz7rvsimyf6a")))

(define-public crate-bcm2709-spi-0.1.1 (c (n "bcm2709-spi") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1vcdg3j7090g81gpdfv0r1kb4mycbxvzd35q7vk2s8fqhj3dcx7i")))

