(define-module (crates-io bc m2 bcm2711-lpa) #:use-module (crates-io))

(define-public crate-bcm2711-lpa-0.1.0 (c (n "bcm2711-lpa") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "17z6cd745ismdb38v2x2b18s54364xnca6c3niqc352qd3jh8gda") (f (quote (("rt")))) (r "1.61.0")))

(define-public crate-bcm2711-lpa-0.2.0 (c (n "bcm2711-lpa") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0np2zicpnq3s0clrisgd5m9d3rs6yc1hji81h90jhsh19gbi3d1i") (f (quote (("rt")))) (r "1.65.0")))

(define-public crate-bcm2711-lpa-0.2.1 (c (n "bcm2711-lpa") (v "0.2.1") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "11pfd77j9gwhss64jc5pbd11b6m4hgkc7l42zy3yfjspcgfavm00") (f (quote (("rt")))) (r "1.65.0")))

(define-public crate-bcm2711-lpa-0.2.2 (c (n "bcm2711-lpa") (v "0.2.2") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1cpw3h6jjs54kjpkia4z1gnihnfsdibid6bcdjd74zm4pl19bwki") (f (quote (("rt")))) (r "1.65.0")))

(define-public crate-bcm2711-lpa-0.3.0 (c (n "bcm2711-lpa") (v "0.3.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1398hqa54wq89va8bdyn0xdz35zd57dmzn8a58gw44w7ipcpv039") (f (quote (("rt")))) (r "1.65.0")))

(define-public crate-bcm2711-lpa-0.4.0 (c (n "bcm2711-lpa") (v "0.4.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0sgsjd29dsa7c36d7x353zii9ymasd4dfdqw8kmk2f2ry90f3g3n") (f (quote (("rt")))) (r "1.65.0")))

