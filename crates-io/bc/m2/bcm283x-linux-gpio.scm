(define-module (crates-io bc m2 bcm283x-linux-gpio) #:use-module (crates-io))

(define-public crate-bcm283x-linux-gpio-0.2.0 (c (n "bcm283x-linux-gpio") (v "0.2.0") (d (list (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "1l66rnb8gca6ps1knavrcwgh5cqvys2dkfph7x9gd77llh6gq51h")))

