(define-module (crates-io bc m2 bcm283x-linux-gpio-mod) #:use-module (crates-io))

(define-public crate-bcm283x-linux-gpio-mod-0.3.0 (c (n "bcm283x-linux-gpio-mod") (v "0.3.0") (d (list (d (n "nix") (r "^0.24.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0fgl7wnzj3sk514c9b8jxmxv6d9s353sar53qnr8l83vabc45381")))

