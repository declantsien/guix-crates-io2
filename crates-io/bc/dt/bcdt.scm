(define-module (crates-io bc dt bcdt) #:use-module (crates-io))

(define-public crate-bcdt-0.1.0 (c (n "bcdt") (v "0.1.0") (d (list (d (n "time") (r "^0.3.12") (d #t) (k 0)))) (h "0w3n80kvlni9q6cr37sb58rknvmahbapr3v9hnn55mwwvfq64ay7")))

(define-public crate-bcdt-0.1.1 (c (n "bcdt") (v "0.1.1") (d (list (d (n "time") (r "^0.3.12") (d #t) (k 0)))) (h "180chq4bhixghiwi8xf501n5dalfsm61dp1mdcaink74ryiazvhr")))

