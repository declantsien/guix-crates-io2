(define-module (crates-io bc ar bcar) #:use-module (crates-io))

(define-public crate-bcar-0.1.0 (c (n "bcar") (v "0.1.0") (h "14w0rwhzygk4c0asp7rgzhjdax7j3yayabma89j6xhf23h8f0q89")))

(define-public crate-bcar-0.2.0 (c (n "bcar") (v "0.2.0") (h "05dw9va9fk9z7cvrxh5b5ys8zfwjkj3fkjr2yvq9bnranvfiambz")))

(define-public crate-bcar-0.2.1 (c (n "bcar") (v "0.2.1") (h "13zlbhd0b3j5bn3789vkyqhky4vxdvlqwnrid37258kpcdvw9jcs")))

