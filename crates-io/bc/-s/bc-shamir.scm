(define-module (crates-io bc -s bc-shamir) #:use-module (crates-io))

(define-public crate-bc-shamir-0.1.0 (c (n "bc-shamir") (v "0.1.0") (d (list (d (n "bc-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0vbjmg873wai786bmx9cxp43j03s3n3qiqmjv12n3p4p8jw8inri")))

(define-public crate-bc-shamir-0.1.1 (c (n "bc-shamir") (v "0.1.1") (d (list (d (n "bc-crypto") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0fk9m44mlqkyjykmmc1vk2a5m6nwyjpjxff9dnxzj46qig8yy234")))

(define-public crate-bc-shamir-0.1.2 (c (n "bc-shamir") (v "0.1.2") (d (list (d (n "bc-crypto") (r "^0.1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "008kzz536hn2f90p91qpqz24dkybpd3wjh4g3j5831d6lmfls96m")))

(define-public crate-bc-shamir-0.2.0 (c (n "bc-shamir") (v "0.2.0") (d (list (d (n "bc-crypto") (r "^0.2.0") (d #t) (k 0)) (d (n "bc-rand") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "134wbsjk8pwfyzaixpkxp5gb1mk5rx6bb9jznmhzcr6aahvk9mxg")))

(define-public crate-bc-shamir-0.3.0 (c (n "bc-shamir") (v "0.3.0") (d (list (d (n "bc-crypto") (r "^0.3.0") (d #t) (k 0)) (d (n "bc-rand") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1rrhnb2md7mvg524s8blm6nmy12dddrs314p2b9d1ni35igdvanb")))

(define-public crate-bc-shamir-0.3.1 (c (n "bc-shamir") (v "0.3.1") (d (list (d (n "bc-crypto") (r "^0.3") (d #t) (k 0)) (d (n "bc-rand") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0ldyl9mqjm0hnk1sx0gkgbvkcikvjd4476d96w9rmz376b4pfh2a")))

(define-public crate-bc-shamir-0.3.2 (c (n "bc-shamir") (v "0.3.2") (d (list (d (n "bc-crypto") (r "^0.3.0") (d #t) (k 0)) (d (n "bc-rand") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "14m7azrj9i3c1mcshryn38g9gbrspna21vplba57qv09xmwfdd0z")))

