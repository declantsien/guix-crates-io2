(define-module (crates-io bc sv bcsv) #:use-module (crates-io))

(define-public crate-bcsv-0.1.0 (c (n "bcsv") (v "0.1.0") (d (list (d (n "base_core_socialist_values") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "12dkn29xamicb5dw14pslvdq0klnr9905fw4s8lrj34x1fm5mghc")))

(define-public crate-bcsv-0.1.1 (c (n "bcsv") (v "0.1.1") (d (list (d (n "base_core_socialist_values") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1hlxdy3k2y804d3r2pdp3fflan6jy9bqnpfwmz7pci19pxlsns7k")))

(define-public crate-bcsv-0.1.2 (c (n "bcsv") (v "0.1.2") (d (list (d (n "base_core_socialist_values") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1n0lgbdy5llnb53lcyhnim0yxph7k4rja5hmijpzmdrz85c7ksqy")))

(define-public crate-bcsv-0.1.3 (c (n "bcsv") (v "0.1.3") (d (list (d (n "base_core_socialist_values") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0g99lg4qgzgwrv3pbjwfpcwfm4wqcvn89k3fs1vd6gjh1ivh1v9w")))

(define-public crate-bcsv-0.1.4 (c (n "bcsv") (v "0.1.4") (d (list (d (n "base_core_socialist_values") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0808x7pbdyhw54df7fb1nqyfsxm85rzdqib1ll1nlra11i1klncv")))

(define-public crate-bcsv-0.1.5 (c (n "bcsv") (v "0.1.5") (d (list (d (n "base_core_socialist_values") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1g8j6j8yb5i0p1g2wl6hwxl419jzfm44gv9p9pgnhmkqd2jhqghk")))

