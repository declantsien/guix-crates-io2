(define-module (crates-io bc s- bcs-link) #:use-module (crates-io))

(define-public crate-bcs-link-0.1.3 (c (n "bcs-link") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0ymw7cr80b81lx5x6ydif0mbdqay3y10qqsy6bd1cnnsxshm8gyy")))

