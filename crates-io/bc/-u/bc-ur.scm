(define-module (crates-io bc -u bc-ur) #:use-module (crates-io))

(define-public crate-bc-ur-0.1.0 (c (n "bc-ur") (v "0.1.0") (d (list (d (n "dcbor") (r "^0.7.0") (d #t) (k 0)) (d (n "ur") (r "^0.3.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1dqphh2mdvcag8rb9wh12zzbfwxhs11lg15z3q6iymdbf4vjms48")))

(define-public crate-bc-ur-0.1.1 (c (n "bc-ur") (v "0.1.1") (d (list (d (n "dcbor") (r "^0.7.0") (d #t) (k 0)) (d (n "ur") (r "^0.3.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "10sjv1zni8scz67gk5m86431w78yxna5ywxmvm9rfvnhi5rzn3dl")))

(define-public crate-bc-ur-0.1.2 (c (n "bc-ur") (v "0.1.2") (d (list (d (n "dcbor") (r "^0.7.0") (d #t) (k 0)) (d (n "ur") (r "^0.3.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0ahh1h6psfbif59x30hx2hfnwd7dvhpbn8cmancmn6cz66gy4gn0")))

(define-public crate-bc-ur-0.1.3 (c (n "bc-ur") (v "0.1.3") (d (list (d (n "dcbor") (r "^0.7.0") (d #t) (k 0)) (d (n "ur") (r "^0.3.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0hw9c9h6pxh361608iyhw7n52v26x12wyng90m7nyirjinxnvk5l")))

(define-public crate-bc-ur-0.1.4 (c (n "bc-ur") (v "0.1.4") (d (list (d (n "dcbor") (r "^0.7.4") (d #t) (k 0)) (d (n "ur") (r "^0.3.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0gazjsynj1rnv267b2r6qgr49frmp5gwb3ykjld4c1qkpm7gapw9")))

(define-public crate-bc-ur-0.1.5 (c (n "bc-ur") (v "0.1.5") (d (list (d (n "dcbor") (r "^0.7.7") (d #t) (k 0)) (d (n "ur") (r "^0.3.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0qbccgs58ambr43lkpnwa60y2f5rd123bs3s5glhvfzkrqyihlkp")))

(define-public crate-bc-ur-0.1.7 (c (n "bc-ur") (v "0.1.7") (d (list (d (n "dcbor") (r "^0.8.1") (d #t) (k 0)) (d (n "ur") (r "^0.3.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1dfrahqv85mq2yzyv876dz0rvm0qyf7snrgdwh6lahkq14xxbjg1")))

(define-public crate-bc-ur-0.2.0 (c (n "bc-ur") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dcbor") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "ur") (r "^0.3.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "06r3b24s17vyhvjbzz4pvx8aivxzdkrhq3pahvglhn24k7161n6i")))

(define-public crate-bc-ur-0.2.1 (c (n "bc-ur") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dcbor") (r "^0.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "ur") (r "^0.3.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "02vngx0kvlhvbwmkdhvr6xvypm9kana5cclz6gga44lnb2l3l0ak")))

(define-public crate-bc-ur-0.2.2 (c (n "bc-ur") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dcbor") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "ur") (r "^0.3.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1r19lgpxsk7saki6llqzf388bprbbb61v8dsp4ni5s19qb9hsgy5")))

(define-public crate-bc-ur-0.2.3 (c (n "bc-ur") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dcbor") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "ur") (r "^0.3.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0g9v09hwxbk96ilsn9ai8mqcp39i2jk87jxa52ih6ilm1dwmw8kw")))

(define-public crate-bc-ur-0.2.4 (c (n "bc-ur") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dcbor") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "ur") (r "^0.3.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "16bppjrhajvmvfbrxc650s2xc518f2zrmiq35f8vbq3n9ppys7nv")))

(define-public crate-bc-ur-0.3.0 (c (n "bc-ur") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dcbor") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "ur") (r "^0.4.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1n3ldshzxmpyp0wh4vf2aimkzgy7kpspzc6phz4kfqp31r5gsx35")))

(define-public crate-bc-ur-0.3.1 (c (n "bc-ur") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "dcbor") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "ur") (r "^0.4.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "0cxgqq98w1kd24apfdk3j9wgdn1yw5v30m2pf099w72z1j2q648d")))

(define-public crate-bc-ur-0.4.0 (c (n "bc-ur") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "dcbor") (r "^0.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "ur") (r "^0.4.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "1yqjsmmc2h7d2f83i5z1n0jbhw5diq4r0wk6y6vfg9i2rz8qsy12")))

