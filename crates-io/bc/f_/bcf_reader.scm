(define-module (crates-io bc f_ bcf_reader) #:use-module (crates-io))

(define-public crate-bcf_reader-0.1.0 (c (n "bcf_reader") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)))) (h "1v8rpa51qsxhbcji2p15ywhxwfvbs1fqaj9x2jbpid3j7ipc9946") (y #t)))

(define-public crate-bcf_reader-0.1.1 (c (n "bcf_reader") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)))) (h "011jjh54adj7r62v93f1mz5y7xivaklr275hlkg26dggkym77r77") (y #t)))

(define-public crate-bcf_reader-0.1.2 (c (n "bcf_reader") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)))) (h "0grxikylzn3zb0a3hgyh1ky50g5xq87vjgw4x132sq4ia641ndfa") (y #t)))

(define-public crate-bcf_reader-0.1.3 (c (n "bcf_reader") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)))) (h "1isnpzjc9baxkmq5y446fhm8ysa7li00c0gwkbjj46dfmrg2mywd")))

(define-public crate-bcf_reader-0.1.4 (c (n "bcf_reader") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "0nfnjvw0j6ydk06hq5iw75m11dz2gjzc1kwqk71ism7f0xwk8l2g")))

(define-public crate-bcf_reader-0.2.1 (c (n "bcf_reader") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "10j5w3rb32xwfd19ci2fc8xwd2f6yflfrvgbacxrpcw9cm6jby3p") (y #t)))

(define-public crate-bcf_reader-0.2.2 (c (n "bcf_reader") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "0rn56mbhixbnc30lgxm48m6n4mp9j0i6m1b8lcq3vq39m06rn8yi") (f (quote (("zlib-ng-compat" "flate2/zlib-ng-compat") ("zlib" "flate2/zlib")))) (y #t)))

(define-public crate-bcf_reader-0.2.3 (c (n "bcf_reader") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "0g5qnr6ljbnpilimnhyprnpp45g9qgycsf8zyhfh5k5p7ccixzpb") (f (quote (("zlib-ng-compat" "flate2/zlib-ng-compat") ("zlib" "flate2/zlib"))))))

