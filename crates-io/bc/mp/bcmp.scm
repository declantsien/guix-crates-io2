(define-module (crates-io bc mp bcmp) #:use-module (crates-io))

(define-public crate-bcmp-0.1.0 (c (n "bcmp") (v "0.1.0") (d (list (d (n "bytepack") (r "^0.3.1") (d #t) (k 0)))) (h "0p62qjpgykclq95wk7bky61fw3i9nln4yx67cv20crlh1qhh7b06")))

(define-public crate-bcmp-0.2.0 (c (n "bcmp") (v "0.2.0") (d (list (d (n "bytepack") (r "^0.3.1") (d #t) (k 0)))) (h "0cqn33s7z8ysdgnmm2algx30w9chi5f7y9jcxwa0dk1rl60qmlmy")))

(define-public crate-bcmp-0.3.0 (c (n "bcmp") (v "0.3.0") (d (list (d (n "bytepack") (r "^0.3.1") (d #t) (k 0)))) (h "1v12q8s1bha92xi0w6figqawl60wy44f16ynphgrdllfxz2n9nq5")))

(define-public crate-bcmp-0.3.1 (c (n "bcmp") (v "0.3.1") (d (list (d (n "bytepack") (r "^0.3.1") (d #t) (k 0)))) (h "0lnyc4ams9ziskd83bwwplnivajbwi94f50vjj7izzhccrnvi1lm")))

(define-public crate-bcmp-0.3.2 (c (n "bcmp") (v "0.3.2") (d (list (d (n "bytepack") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "15jv2x4xz824k8jpc973khh418a4w6iwqvvjqz231h6kmynccb7g")))

(define-public crate-bcmp-0.4.0 (c (n "bcmp") (v "0.4.0") (d (list (d (n "bytepack") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0wzfw6rbwyv8g5mhrsa76r8inixh673vyx8cdjs3130ma9374lv4")))

(define-public crate-bcmp-0.4.1 (c (n "bcmp") (v "0.4.1") (d (list (d (n "bytepack") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1cncnn2svwshivfqllcvd74x52knfv2g4xdp5v7hg1hajqmcswrd")))

