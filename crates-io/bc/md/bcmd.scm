(define-module (crates-io bc md bcmd) #:use-module (crates-io))

(define-public crate-bcmd-0.1.0 (c (n "bcmd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "assert_fs") (r "^1.1.1") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)))) (h "1x8snhyp9w2djfylx9lmwyksayq33a516cg63qphrqkqfcvnxkb0") (y #t)))

(define-public crate-bcmd-0.1.1 (c (n "bcmd") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "assert_fs") (r "^1.1.1") (d #t) (k 2)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wqlcgrgr5c8vl0ivfr3l8wfvb4qgih7h1n2y6g9r027sp64x4ig")))

