(define-module (crates-io bc #{4p}# bc4py-plot-cli) #:use-module (crates-io))

(define-public crate-bc4py-plot-cli-0.1.0 (c (n "bc4py-plot-cli") (v "0.1.0") (d (list (d (n "bc4py_hash") (r "^0.1.2") (f (quote ("poc" "progress-bar"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "084bn29jq1g6vvzkpp1ycjfr5p6b3p5sjsvaq7bgb77mw4rq1rfz")))

