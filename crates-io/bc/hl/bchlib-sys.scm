(define-module (crates-io bc hl bchlib-sys) #:use-module (crates-io))

(define-public crate-bchlib-sys-0.1.0 (c (n "bchlib-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0wvdfr1zkgzi49x8lr1mj5iim74qa63pgy9d46biwskksgjhpl29")))

(define-public crate-bchlib-sys-0.2.0 (c (n "bchlib-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1wh5jd31368wizdsqmaswa3fxi6law6kpwbrjw501kz8djj91jnz")))

(define-public crate-bchlib-sys-0.2.1 (c (n "bchlib-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "14wcb4fnhdz43bdsl52p36p8w4g2652x49hb0f4xvmxjw9bp000c")))

