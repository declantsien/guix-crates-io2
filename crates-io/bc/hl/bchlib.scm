(define-module (crates-io bc hl bchlib) #:use-module (crates-io))

(define-public crate-bchlib-0.2.0 (c (n "bchlib") (v "0.2.0") (d (list (d (n "bchlib-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0qx8mg7a9blgj5r5z21ihqv5l6i4gsx060df65bh40w18h9am132")))

(define-public crate-bchlib-0.2.1 (c (n "bchlib") (v "0.2.1") (d (list (d (n "bchlib-sys") (r "^0.2.1") (d #t) (k 0)))) (h "13bmsn5f0lrrbb1p09gm283n4nv5w59s6rkg2v70rlh1glh1hi9w")))

