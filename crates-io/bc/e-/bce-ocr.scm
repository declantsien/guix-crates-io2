(define-module (crates-io bc e- bce-ocr) #:use-module (crates-io))

(define-public crate-bce-ocr-2022.6.18-0 (c (n "bce-ocr") (v "2022.6.18-0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.8") (d #t) (k 0)) (d (n "serde_url_params") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0zx4z12bf18933s7m282glz2j240ahgqasasjllz3r7vnzij3wpc")))

