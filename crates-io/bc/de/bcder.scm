(define-module (crates-io bc de bcder) #:use-module (crates-io))

(define-public crate-bcder-0.1.0 (c (n "bcder") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0msd20pn5l2hkza56aqjxilki31wy9ybxcza355rdk6yfak9wg2z") (f (quote (("extra-debug"))))))

(define-public crate-bcder-0.2.0 (c (n "bcder") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.14") (d #t) (k 0)))) (h "0y883mn9b1ajsymvdfwh56irihdfyq1h8bb8bvqppa4fmdfsff1k") (f (quote (("extra-debug"))))))

(define-public crate-bcder-0.2.1 (c (n "bcder") (v "0.2.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.14") (d #t) (k 0)))) (h "1233mnkb33daxslhjj0h22iab6bh9m9syrsl0wcwvavrmbmcj1nm") (f (quote (("extra-debug"))))))

(define-public crate-bcder-0.3.0 (c (n "bcder") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3.15") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.14") (d #t) (k 0)))) (h "03h6islblrm6g4lrdnw837l83zfafmwfk47kll13jmmrgq1pmy9y") (f (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.3.1 (c (n "bcder") (v "0.3.1") (d (list (d (n "backtrace") (r "^0.3.15") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.15") (d #t) (k 0)))) (h "15dlnxnhav0prnnlyb4rj2vpac26vw3a3963gr83grjs59zrbgdw") (f (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.2.2 (c (n "bcder") (v "0.2.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.14") (d #t) (k 0)))) (h "00snaxl7lzfn19680c9wk4h4fa8li11a11n16bnnjm4q74n93k6y") (f (quote (("extra-debug"))))))

(define-public crate-bcder-0.4.0 (c (n "bcder") (v "0.4.0") (d (list (d (n "backtrace") (r "^0.3.15") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "04xbxc54bs857pmylgi9q0j3r6yf9hvnbhixib0i4swxrlbnxaz1") (f (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.4.1 (c (n "bcder") (v "0.4.1") (d (list (d (n "backtrace") (r "^0.3.15") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "03smg3w80zjggz0zxblypqxwwhhjg0xz0vcsdx8m066ys0q3xmmm") (f (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.4.2 (c (n "bcder") (v "0.4.2") (d (list (d (n "backtrace") (r "^0.3.15") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "0xhdyjrxd2lv3ah6qc6kycf0qdb420m8mgw80yhy5n5gmkwkn7hh") (f (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.5.0 (c (n "bcder") (v "0.5.0") (d (list (d (n "backtrace") (r "^0.3.15") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1srx0fnfgcp68p0l8qvpjbx3mkrjzfralm23nhm7yzcrv7zk88jx") (f (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.5.1 (c (n "bcder") (v "0.5.1") (d (list (d (n "backtrace") (r "^0.3.15") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "0wir1xcxr4907rgnwz2l9lkjzpv73y2znkv2zfrq3nkspydfappi") (f (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.6.0 (c (n "bcder") (v "0.6.0") (d (list (d (n "backtrace") (r "^0.3.15") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "0nwwbnc23sh89f6vwghmrh3zlrssfxrymcr9h4cggvjxkamvhvn1") (f (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.6.1 (c (n "bcder") (v "0.6.1") (d (list (d (n "backtrace") (r "^0.3.15") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "0f8fkhrbafc7jsvvlhbsv4mxshbxggvz32h23ddyq0bvwnxc7rsj") (f (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.7.0 (c (n "bcder") (v "0.7.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "0lgahjmpki63b7gwa3qj6m3m1lx64p068w8wj4cx5xwfzfndh1zh")))

(define-public crate-bcder-0.7.1 (c (n "bcder") (v "0.7.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "16nsx3d7bpr68j7gyhdmjbvasav6alxmahrwfa63zvja1pfbgpv9")))

(define-public crate-bcder-0.7.2 (c (n "bcder") (v "0.7.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "1nd23nfxsgwd6ah2227ysbgnnmp7mbp9m1yaya361wssg4cz09mb")))

(define-public crate-bcder-0.7.3 (c (n "bcder") (v "0.7.3") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.1") (d #t) (k 0)))) (h "0ixwrgwg7ggzdk709i35yl8wybs5xw29j6b1nv52bspqj34vw5mz")))

(define-public crate-bcder-0.7.4 (c (n "bcder") (v "0.7.4") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1w3fj0y3fbjb3bd42nl7b22kg0cl6240v69mxf5v7aklcxx789y6")))

