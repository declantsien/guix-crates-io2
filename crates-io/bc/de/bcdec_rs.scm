(define-module (crates-io bc de bcdec_rs) #:use-module (crates-io))

(define-public crate-bcdec_rs-0.1.0 (c (n "bcdec_rs") (v "0.1.0") (h "0705c5i4dbrhlbc561rzy3999ccaskym55wzq850n59csfpvr0g4")))

(define-public crate-bcdec_rs-0.1.1 (c (n "bcdec_rs") (v "0.1.1") (h "0l4163jj0617n8vd4xymhb3v6w1v4c7p5gqf54c658x5r90h2f3q")))

