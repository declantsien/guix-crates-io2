(define-module (crates-io bc -r bc-rand) #:use-module (crates-io))

(define-public crate-bc-rand-0.1.0 (c (n "bc-rand") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.9") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0g4fa20vi6gxszk3x8lcaxpphl4pp6sw84rajmn9256cwqja2vsi")))

(define-public crate-bc-rand-0.1.1 (c (n "bc-rand") (v "0.1.1") (d (list (d (n "getrandom") (r "^0.2.9") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "04c1khdhyh8h0nfc7818lg156ys92kcjj48w2q7vny65gvfwq91h")))

