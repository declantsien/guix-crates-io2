(define-module (crates-io bc d- bcd-numbers) #:use-module (crates-io))

(define-public crate-bcd-numbers-0.1.0 (c (n "bcd-numbers") (v "0.1.0") (h "030vf90kclglsaavlpkjgwqmb20xi60l0llg6h2pkkq710hfawq7")))

(define-public crate-bcd-numbers-1.0.0 (c (n "bcd-numbers") (v "1.0.0") (h "03rdvr4x6mffggllf4g0xxsilwb66qz6x2rlak1qwbqi3bih2zc6")))

(define-public crate-bcd-numbers-1.0.1 (c (n "bcd-numbers") (v "1.0.1") (h "0jkha2snrs1agnc00nla2h7dw8kpb4i6fh3bvqw7xv1v0cfmcgwp")))

(define-public crate-bcd-numbers-1.0.2 (c (n "bcd-numbers") (v "1.0.2") (h "188xkgfs31yi2jn9340cj1annlx5zq3izxgyvjg053gfcrdsw0ny")))

(define-public crate-bcd-numbers-1.0.3 (c (n "bcd-numbers") (v "1.0.3") (h "1vz5nbkmp2pvqaiygx6j272bpzi8pqlzy1xw951a29kqa9whpbi6")))

(define-public crate-bcd-numbers-1.0.4 (c (n "bcd-numbers") (v "1.0.4") (h "0by5hwwgkbsslphnwvc96l5mkfvwfw2aichijxcy5q7k6vjxgqa8")))

(define-public crate-bcd-numbers-1.0.5 (c (n "bcd-numbers") (v "1.0.5") (h "1fl4rszs264zghdfkbr3w4s1nwmwwgjgd5xrfhfp8g2xga5vhh5p")))

(define-public crate-bcd-numbers-1.0.6 (c (n "bcd-numbers") (v "1.0.6") (h "1mjp2h2p7q9ms86s9ba4yn13dmc6ipzabiiayhvdmsc9r4b0hwis")))

(define-public crate-bcd-numbers-1.0.7 (c (n "bcd-numbers") (v "1.0.7") (h "0fz99fydriwaxax1gcqsiwrdmwrv0wzi885h0455f3aijahkfps2")))

(define-public crate-bcd-numbers-1.0.8 (c (n "bcd-numbers") (v "1.0.8") (h "09xhjq34b3mvdjzqp8aajl31zvfc1mwqwba2pxh720zhxgwz7q32")))

(define-public crate-bcd-numbers-1.0.9 (c (n "bcd-numbers") (v "1.0.9") (h "05p5fh51jv0jg3kvn6iv770kbxcpiaw78kidzrs6qwrrss1gg6ph")))

(define-public crate-bcd-numbers-1.0.10 (c (n "bcd-numbers") (v "1.0.10") (h "1dl69iar1yhza27bsb2vpcbaqnjmzqinq75d68q3asspkybwyn3z")))

(define-public crate-bcd-numbers-1.0.11 (c (n "bcd-numbers") (v "1.0.11") (h "071rw4ai96l3zsz9rx117k31s1xbi15js56cmbilnba1q7a34k5v")))

