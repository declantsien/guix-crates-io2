(define-module (crates-io bc h_ bch_addr) #:use-module (crates-io))

(define-public crate-bch_addr-0.1.0 (c (n "bch_addr") (v "0.1.0") (d (list (d (n "bs58") (r "^0.2.2") (f (quote ("check"))) (d #t) (k 0)) (d (n "cash_addr") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0mgsj5vdb2vy96c11nm0l3dafih275fxa55j0rday57pnjvnnpks")))

