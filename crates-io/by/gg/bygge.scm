(define-module (crates-io by gg bygge) #:use-module (crates-io))

(define-public crate-bygge-0.1.0 (c (n "bygge") (v "0.1.0") (d (list (d (n "cargo-lock") (r "^4.0.1") (f (quote ("dependency-tree"))) (k 0)) (d (n "cargo_toml") (r "^0.8.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.3.1") (d #t) (k 0)))) (h "02iqvfz63zxa2x0hp6r85ifam9qp5im2m7d88d5h85yhv7fdll6l")))

(define-public crate-bygge-0.2.0 (c (n "bygge") (v "0.2.0") (d (list (d (n "cargo-lock") (r "^4.0.1") (f (quote ("dependency-tree"))) (k 0)) (d (n "cargo_metadata") (r "^0.10.0") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.8.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.3.1") (d #t) (k 0)))) (h "05s47039mv0qdhbpf20hj38w74yb0mz1izf4rcq6z8f2x00zd2k8")))

