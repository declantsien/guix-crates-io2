(define-module (crates-io by _a by_address) #:use-module (crates-io))

(define-public crate-by_address-1.0.0 (c (n "by_address") (v "1.0.0") (h "1xfv8aamy4khcv5hdhsz5s1yffqz64b283mn7v5ff9zriamq31h7")))

(define-public crate-by_address-1.0.1 (c (n "by_address") (v "1.0.1") (h "06l5cmmdw6izgfafsixhvnwiplc2w67wfpwah0mm41rw294dn223")))

(define-public crate-by_address-1.0.2 (c (n "by_address") (v "1.0.2") (h "0amk4d62xsxy3lsv2ry5id0vl48bkk5gqfzxxkb3763d5hf0pd85")))

(define-public crate-by_address-1.0.3 (c (n "by_address") (v "1.0.3") (h "0zc7hxnsb9w408w8na8c7acrvl1lrlswzgsqxl08jv17k01cbafi")))

(define-public crate-by_address-1.0.4 (c (n "by_address") (v "1.0.4") (h "0kwlq82yj7nhghnzw59pdw5babjdnm7g2p6n21cb8kpbc17p0ig2")))

(define-public crate-by_address-2.0.0 (c (n "by_address") (v "2.0.0") (h "0zzqbjngh863zrh58kb9hjzdb1c7fmvq1ikhl0qyvm370ja0d3hg") (y #t)))

(define-public crate-by_address-2.1.0 (c (n "by_address") (v "2.1.0") (h "0ql9b1krx9117psl6fl2x03l385gkkfvm3lsqzkr2yyqibzzh6ya") (y #t)))

(define-public crate-by_address-1.1.0 (c (n "by_address") (v "1.1.0") (h "1l0km1y8ybwjkdn3nca4bcbk3csymv4hyngjl5lxfkhid0lbm3dz")))

(define-public crate-by_address-1.1.1 (c (n "by_address") (v "1.1.1") (h "1p0gc05j2j53qb1aby1hgz88gz6mgr7pr987bxpzhkvjh25whii0")))

(define-public crate-by_address-1.1.2 (c (n "by_address") (v "1.1.2") (h "16158zv5ncinbr7i8f2qdwk2zrab35l03zby7hbjl1axn330hkwx")))

(define-public crate-by_address-1.2.0 (c (n "by_address") (v "1.2.0") (h "020fbsjsr4v4y2v3jhpzym24fz45lidmwrmci03mq1nljq1k7sg7")))

(define-public crate-by_address-1.2.1 (c (n "by_address") (v "1.2.1") (h "01idmag3lcwnnqrnnyik2gmbrr34drsi97q15ihvcbbidf2kryk4")))

