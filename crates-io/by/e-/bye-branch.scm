(define-module (crates-io by e- bye-branch) #:use-module (crates-io))

(define-public crate-bye-branch-0.1.0 (c (n "bye-branch") (v "0.1.0") (d (list (d (n "requestty") (r "^0.5.0") (d #t) (k 0)))) (h "1c3x7d6b55dwc4gbqiwrfd3scwblqdq6psv4iavkh3815ja9cp1f") (y #t)))

(define-public crate-bye-branch-0.1.1 (c (n "bye-branch") (v "0.1.1") (d (list (d (n "requestty") (r "^0.5.0") (d #t) (k 0)))) (h "19gsiqwb7cpcfrkv78h7h488zhjm1rydbfai1zwbx947fzrz9nxs")))

(define-public crate-bye-branch-0.1.11 (c (n "bye-branch") (v "0.1.11") (d (list (d (n "requestty") (r "^0.5.0") (d #t) (k 0)))) (h "1pnsq2rn6gi2c5vvxs8jl2g9hljqxpsjhl5rxchp259a4nd1glqn")))

(define-public crate-bye-branch-0.1.12 (c (n "bye-branch") (v "0.1.12") (d (list (d (n "requestty") (r "^0.5.0") (d #t) (k 0)))) (h "1wf2h3svnwm55b0p58jjkz78nhpfhipa1h07f04kp7nvq36yl5v8")))

