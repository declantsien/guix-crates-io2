(define-module (crates-io by te bytey) #:use-module (crates-io))

(define-public crate-bytey-0.0.0 (c (n "bytey") (v "0.0.0") (h "06cr8dzmfhm53la6nvzmkaqr1psbvzq6ygwy02avj5jhkd8r30pn")))

(define-public crate-bytey-0.1.0 (c (n "bytey") (v "0.1.0") (d (list (d (n "bytey_byte_buffer") (r "^0.1.0") (d #t) (k 0)))) (h "17cgrvaykm6vm0kzrkn8m7lb5yjhxdf5659j6cssik34y7dh9bjh")))

(define-public crate-bytey-0.1.1 (c (n "bytey") (v "0.1.1") (d (list (d (n "bytey_byte_buffer") (r "^0.1.1") (d #t) (k 0)))) (h "0gsbvp2nq0016g2pcib67bqzzkaiy7szd60c7q4jgm0bsnpgn9wi")))

(define-public crate-bytey-0.2.0 (c (n "bytey") (v "0.2.0") (d (list (d (n "bytey_bincode") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bytey_byte_buffer") (r "^0.2.0") (d #t) (k 0)))) (h "021q473dvl1r1v0n7xqx17zwanrgvicmgkckk13l20grw26w8n5p") (f (quote (("bincode_serialize" "bytey_bincode"))))))

(define-public crate-bytey-0.3.0 (c (n "bytey") (v "0.3.0") (d (list (d (n "bytey_bincode") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytey_byte_buffer") (r "^0.2.1") (d #t) (k 0)) (d (n "bytey_derive") (r "^0.2.0") (d #t) (k 0)))) (h "0nbq3qpa54d40psv91y4dy82ca8i6s4ypm2jv2zi8yp9wjzlh77w") (f (quote (("bincode_serialize" "bytey_bincode"))))))

