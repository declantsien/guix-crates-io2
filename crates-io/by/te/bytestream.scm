(define-module (crates-io by te bytestream) #:use-module (crates-io))

(define-public crate-bytestream-0.1.0 (c (n "bytestream") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (o #t) (d #t) (k 0)))) (h "0bckzmb2raiskckj9fjv93nng0lw9pd0c9lzxhy7ms363a30k51v") (f (quote (("std-types" "byteorder") ("default" "std-types"))))))

(define-public crate-bytestream-0.2.0 (c (n "bytestream") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (o #t) (d #t) (k 0)))) (h "0pzij37cf7q5qwkgqfglb5y0ccrlan3dg8grh5gvc70gyrmldqms") (f (quote (("default" "batteries-included") ("batteries-included" "byteorder"))))))

(define-public crate-bytestream-0.3.0 (c (n "bytestream") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.4") (o #t) (d #t) (k 0)))) (h "0n4rfgarwjsds8byzkaqlslhxffbc38ykbzpsqzssqk53hh4sk8g") (f (quote (("default" "batteries-included") ("batteries-included" "byteorder"))))))

(define-public crate-bytestream-0.4.0 (c (n "bytestream") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4.2") (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "0g864gm7zb7nc3qipqvdjvb54918s3g71qi3ikx1mv24mzs0lbsy") (f (quote (("default" "byteorder"))))))

(define-public crate-bytestream-0.4.1 (c (n "bytestream") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.4.2") (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "0m34m19yzvp8n2qkxk0jbwbdq7kpkg32vvlzyvx6lzbi5a221xq4") (f (quote (("default" "byteorder"))))))

