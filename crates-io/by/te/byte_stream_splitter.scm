(define-module (crates-io by te byte_stream_splitter) #:use-module (crates-io))

(define-public crate-byte_stream_splitter-0.1.0 (c (n "byte_stream_splitter") (v "0.1.0") (h "1g4hq6czms0blnyg7lj5ykg1dcgapip80wwlx8sdp8n17laknbhz")))

(define-public crate-byte_stream_splitter-0.1.1 (c (n "byte_stream_splitter") (v "0.1.1") (h "1v69pxgp2yya0zjli791l85wlhwpqyg95c1h8k0widmqs64kw9dr")))

(define-public crate-byte_stream_splitter-0.1.2 (c (n "byte_stream_splitter") (v "0.1.2") (h "1bsn7w5wmly9i91yna26dymbbrj7cp2hs2cmffdl6k453g34b9ll")))

(define-public crate-byte_stream_splitter-0.1.3 (c (n "byte_stream_splitter") (v "0.1.3") (h "0b648hkcdyqmxzwq2sj9b322flgxi85h0239q8va9hg5aqq0zr8j")))

(define-public crate-byte_stream_splitter-0.1.4 (c (n "byte_stream_splitter") (v "0.1.4") (d (list (d (n "time") (r "0.*") (d #t) (k 0)))) (h "1n6363a7s5bjgd6xllxd2p7dlfx4v7c4fhnm539bcfgyhdgs45h2")))

(define-public crate-byte_stream_splitter-0.2.0 (c (n "byte_stream_splitter") (v "0.2.0") (h "0wbbvdk38czw8scjq3dsv2b95b9fv2x1wzmrzwgy2cd7r23wr4bn")))

