(define-module (crates-io by te bytes_to_type) #:use-module (crates-io))

(define-public crate-bytes_to_type-0.1.0 (c (n "bytes_to_type") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0g954hp5p3nq89m5f8m2zh0z5zi721ks5kbxj2834rlibsmbydfa")))

(define-public crate-bytes_to_type-0.1.1 (c (n "bytes_to_type") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "02ggjdfqac32kryyy89rns0bf7ifr3rgxl5ry4s6l8wmzqxq76pd")))

