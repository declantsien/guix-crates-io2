(define-module (crates-io by te bytestream-rs) #:use-module (crates-io))

(define-public crate-bytestream-rs-0.1.0 (c (n "bytestream-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "1f4pv3vjhg8s3lvfvpia4kvyvn00kd4i7gvzw32lm89xgi5j3v4j")))

(define-public crate-bytestream-rs-0.2.0 (c (n "bytestream-rs") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "0p7k2d760qw9qx8jmzg8k0q68c8liv4l9vk413ia2gk0nb65nznq")))

(define-public crate-bytestream-rs-0.2.1 (c (n "bytestream-rs") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "1rq69a91d0nfbm1yfnl0hp12h80rbnlr83v9pcb1ba5h0762wz9v")))

(define-public crate-bytestream-rs-0.2.2 (c (n "bytestream-rs") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "17fi2010bi4ihq7nsjcqhg3n62jhq5pw42587zjkaajj41r2nmma")))

(define-public crate-bytestream-rs-0.2.3 (c (n "bytestream-rs") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "0jz7kag3zkibga8pkd692fgmsy7q4nxa6xx6jp48a498y9jf1svc")))

(define-public crate-bytestream-rs-0.2.4 (c (n "bytestream-rs") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "1wzwa23ira9b19l6xcjgvhjvfi6yhyagf4v2p4ymf0yrsjil4g10")))

(define-public crate-bytestream-rs-0.2.5 (c (n "bytestream-rs") (v "0.2.5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "035qb0x95n3bz7wnr3kwrqsck8f7js26n97hsd8xcbix85505cw5")))

(define-public crate-bytestream-rs-0.2.6 (c (n "bytestream-rs") (v "0.2.6") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "11kfx6nd0v5lylgl5slr2m34fh9jlkq6cfz2447y0i26y5xx46ks")))

(define-public crate-bytestream-rs-0.3.0 (c (n "bytestream-rs") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytestream_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "1df1lcp1yj8glnjb6mrw8saj1n9b5802r3xim5283jfnr82sqgxw")))

(define-public crate-bytestream-rs-0.3.1 (c (n "bytestream-rs") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytestream_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "0ipkvvs4hizdla9vyjn2yr1wmf7rvp33slhcsbmxjvq4yaqnc79w")))

(define-public crate-bytestream-rs-0.3.2 (c (n "bytestream-rs") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytestream_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "152rhjpxngxnay3jvlk784s4fljrz21hrrg5zy817jh7nwhp3hcj")))

(define-public crate-bytestream-rs-0.3.3 (c (n "bytestream-rs") (v "0.3.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytestream_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "1dpw23gjnpnnk35xn3jfx4dp3h707vh2igm4f0674hbskv36is0y")))

(define-public crate-bytestream-rs-0.3.4 (c (n "bytestream-rs") (v "0.3.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytestream_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "0nirb4di83awcsjf5qgdlf51jb2mr1g2v3dgq1lzgdhjc0frpygd")))

(define-public crate-bytestream-rs-0.3.5 (c (n "bytestream-rs") (v "0.3.5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytestream_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0z0gwxn1f53szjx9nvv54lxpz0qgv57d489qyg4jc2iansga2g4d")))

(define-public crate-bytestream-rs-0.3.6 (c (n "bytestream-rs") (v "0.3.6") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytestream_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0j9yks1jxdsild9q5y4z3v80bg13cl58ihjnilh3jk74qrjxdlw5")))

(define-public crate-bytestream-rs-0.3.8 (c (n "bytestream-rs") (v "0.3.8") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytestream_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wdn8kjsx965xhr9iafdkaih0fskgnylhsdf938pr4ncv7rjk9kj")))

(define-public crate-bytestream-rs-0.4.0 (c (n "bytestream-rs") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytestream_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13vl07kli83nxv8law5km80ki2nlw78hlzamf2iczbnnhhb6ls8s")))

(define-public crate-bytestream-rs-0.4.1 (c (n "bytestream-rs") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytestream_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ym9ryk7gi7pwdb8qdd8zinib3zf6iyrijl9kx71ma0w6cdzp51g")))

