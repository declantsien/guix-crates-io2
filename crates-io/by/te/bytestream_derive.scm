(define-module (crates-io by te bytestream_derive) #:use-module (crates-io))

(define-public crate-bytestream_derive-0.1.0 (c (n "bytestream_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m80zl5aqim4jw8zi5zvlk5h8qrgydl3y3jipy25z8b07n3029fr")))

(define-public crate-bytestream_derive-0.2.0 (c (n "bytestream_derive") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ihrka1dadvr71hmcpvna9h3m7hxpw10vxhbs5j0hd440nzqq4cf")))

(define-public crate-bytestream_derive-0.2.1 (c (n "bytestream_derive") (v "0.2.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "1zm9qygv0642fpqazplfzjbi91axqi4f3sk7dw6s1zd2hpicqcnl")))

(define-public crate-bytestream_derive-0.2.2 (c (n "bytestream_derive") (v "0.2.2") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (d #t) (k 0)))) (h "16jwdw8f3vw8zr2lbwc8mr14fbd48ykfbg2drsqxy9dxvkclppa9")))

