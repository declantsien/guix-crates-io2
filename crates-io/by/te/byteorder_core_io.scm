(define-module (crates-io by te byteorder_core_io) #:use-module (crates-io))

(define-public crate-byteorder_core_io-0.5.3 (c (n "byteorder_core_io") (v "0.5.3") (d (list (d (n "core_io") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "09bji66bymp0vm79pcai1gq4akvcv1ywcwyddij3wv9bqzx4nxk3") (f (quote (("std") ("default" "std"))))))

