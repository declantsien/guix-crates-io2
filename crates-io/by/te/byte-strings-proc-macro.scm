(define-module (crates-io by te byte-strings-proc-macro) #:use-module (crates-io))

(define-public crate-byte-strings-proc-macro-0.1.0 (c (n "byte-strings-proc-macro") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1mmkrp3mq4iwyb4i2xz8nlllhfjr2hvxpp0hy2x413azgnx9yy1v") (f (quote (("proc-macro-hygiene") ("default"))))))

(define-public crate-byte-strings-proc-macro-0.1.1 (c (n "byte-strings-proc-macro") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0mcd5j0s3a9wb5qlvb77cn9ybai5l4d30jchhfcp12cb6dhmy27p") (f (quote (("proc-macro-hygiene") ("default"))))))

