(define-module (crates-io by te bytecmp) #:use-module (crates-io))

(define-public crate-bytecmp-0.4.2 (c (n "bytecmp") (v "0.4.2") (d (list (d (n "bytepack") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0lm17cw30h7kqvss0wkfc2l7469rcrb3nmpg5nxf67w1gflpbm6x")))

(define-public crate-bytecmp-0.5.0 (c (n "bytecmp") (v "0.5.0") (d (list (d (n "bytepack") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1razs4ri7rj8b56i387b16apv44ij56i7d83sx4bc2i9xdxxxb70")))

(define-public crate-bytecmp-0.5.1 (c (n "bytecmp") (v "0.5.1") (d (list (d (n "bytepack") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "02qkrv0i66px27dpda035rp4d6lqw0ngd8n4ncfqyckls9k8zpcv")))

