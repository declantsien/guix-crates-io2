(define-module (crates-io by te bytes-cast-derive) #:use-module (crates-io))

(define-public crate-bytes-cast-derive-0.1.0 (c (n "bytes-cast-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nqzv2q301yhjc11aphwrvj85m1hzyd553mmsrj6cirqvvwnm4yb")))

(define-public crate-bytes-cast-derive-0.1.1 (c (n "bytes-cast-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z08czckwcd3pwhrq9smfv04xflr42l2x2y9ini1n0lizj7hwgmi")))

(define-public crate-bytes-cast-derive-0.1.2 (c (n "bytes-cast-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mbdjp5j1clk5yh61227ymh3z4fr4k3d89sigriy6nn4qvilwl6c")))

(define-public crate-bytes-cast-derive-0.2.0 (c (n "bytes-cast-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wagrd5qi6sw4pcyqsvy4isqp5q723vqnxff5kbf7kaqrvyacw3l")))

