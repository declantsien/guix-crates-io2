(define-module (crates-io by te bytelinebuf) #:use-module (crates-io))

(define-public crate-bytelinebuf-0.1.0 (c (n "bytelinebuf") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (o #t) (d #t) (k 0)))) (h "1ch5zqi682xdsnd1cwvgk1sl87krmys6zh9rvl14qpms9y0w3nj8") (f (quote (("stream" "futures" "pin-project") ("default"))))))

