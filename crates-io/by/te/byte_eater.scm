(define-module (crates-io by te byte_eater) #:use-module (crates-io))

(define-public crate-byte_eater-0.1.0 (c (n "byte_eater") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19q73wjnpfgbr6lv3h5qpgw2y2q8ni99ni4q9rcz9iha38xq06yg") (y #t)))

(define-public crate-byte_eater-0.1.1 (c (n "byte_eater") (v "0.1.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0clh5w79ppcywxb0mb77bc7qx5nr3n1slnbavdpag84bqylngn22")))

