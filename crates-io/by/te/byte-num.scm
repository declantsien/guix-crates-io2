(define-module (crates-io by te byte-num) #:use-module (crates-io))

(define-public crate-byte-num-0.1.0 (c (n "byte-num") (v "0.1.0") (h "05b3yi2sdc3gsng16368zbjgbjn4wvzjnssvrpzs7kxl4wzk45dg") (f (quote (("nightly"))))))

(define-public crate-byte-num-0.1.1 (c (n "byte-num") (v "0.1.1") (h "0rlwsmc355f5akmly9kx6vf659m6dsbvgwvh8bq39140fnxbvh2a") (f (quote (("nightly"))))))

(define-public crate-byte-num-0.1.2 (c (n "byte-num") (v "0.1.2") (h "07hm5g43p984n1cmwkpnavbg239b3vdkrq64lrm01aqf2q5abbcm") (f (quote (("nightly"))))))

(define-public crate-byte-num-0.1.3 (c (n "byte-num") (v "0.1.3") (h "1jzgc5nrxhr9wkhpbjrialr1pv99cd953fzq86n11d2bzdwa3rgc") (f (quote (("nightly"))))))

