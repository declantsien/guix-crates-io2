(define-module (crates-io by te byte-sequence) #:use-module (crates-io))

(define-public crate-byte-sequence-0.1.0 (c (n "byte-sequence") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1i3yrgjyvgg6pz7gsgsahr7ci5n6zhm0wvaylki56d4q42ndwzvl")))

(define-public crate-byte-sequence-0.2.0 (c (n "byte-sequence") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0vxj17c3z2lkyr4924fhh4d19avxxbvcaiqrs8xq66klvzjja8rs")))

(define-public crate-byte-sequence-0.2.1 (c (n "byte-sequence") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1c1s4hdl698vqkkzm4w6gbmrqn3awxgx1lv3hvrabif55qgyk8p3")))

(define-public crate-byte-sequence-0.2.2 (c (n "byte-sequence") (v "0.2.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0w32h5kh4lksfwwyqfga88kz641j32w165ms2lgs1mi0i8c5fk3k")))

