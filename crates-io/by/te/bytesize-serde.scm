(define-module (crates-io by te bytesize-serde) #:use-module (crates-io))

(define-public crate-bytesize-serde-0.1.0 (c (n "bytesize-serde") (v "0.1.0") (d (list (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1rvnynzaim71a77n0ycas0a5h2ig6ffs07pb3xgsgm52b7plw983")))

(define-public crate-bytesize-serde-0.2.0 (c (n "bytesize-serde") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0r1djf34n6wyh0mvldqybrwcvj47b7gacjnl4jl6bfn8s9z346ns")))

(define-public crate-bytesize-serde-0.2.1 (c (n "bytesize-serde") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bytesize") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0y018j9m8kkshaarzn9mvbg9zsmpcq6716avg3lmk236s8pyplc6")))

