(define-module (crates-io by te bytesize) #:use-module (crates-io))

(define-public crate-bytesize-0.0.1 (c (n "bytesize") (v "0.0.1") (h "06c28jv20mxplbdkxwrhrk7bnaxww19ghjj2vfpk5m84jr2if49z")))

(define-public crate-bytesize-0.1.0 (c (n "bytesize") (v "0.1.0") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "0s213padbln67arb39bbzisik0ilw002hx9qm8ln2c228c9rw6xz")))

(define-public crate-bytesize-0.1.1 (c (n "bytesize") (v "0.1.1") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "1qk5x5icwsjgdwi27g75k9cyd8sm11qhq4z94jlp2f4n0aiq0rnn")))

(define-public crate-bytesize-0.1.2 (c (n "bytesize") (v "0.1.2") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)))) (h "0g9lz9vmv2hpqg8cvzgpmc72ls2pb6sbbkz8x3bjnshr04i88yzy")))

(define-public crate-bytesize-0.1.3 (c (n "bytesize") (v "0.1.3") (h "1f30siqkprv6f5imxb6ry9lnq5s883g79qlaps7zlk2rzv2r9mqn")))

(define-public crate-bytesize-0.2.0 (c (n "bytesize") (v "0.2.0") (h "1ca10jbwgb0g451pnqb2ffsd5jqmbjpha8x605dhkpp4p30pv3a3")))

(define-public crate-bytesize-0.2.1 (c (n "bytesize") (v "0.2.1") (h "0wyszjdqp8a3w8kqavs3nrmqwfpa8yyp3kcjfpzij7j52ni03zdj")))

(define-public crate-bytesize-0.2.2 (c (n "bytesize") (v "0.2.2") (h "1s6d0r9gwn230kgxnfkd8vagrf82km16427455j8k6mmnqb5m2ax")))

(define-public crate-bytesize-0.2.3 (c (n "bytesize") (v "0.2.3") (h "0zh1h9zb5inaaq1p2wdyf9dazgdchqc2jv79bfw0jsj41j8wx6dd")))

(define-public crate-bytesize-0.2.4 (c (n "bytesize") (v "0.2.4") (h "11l92fafp8vaj1r5wlanjncm9yka2dwcjimma70mhmk967y9z5s4")))

(define-public crate-bytesize-1.0.0 (c (n "bytesize") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "040hwk5hwvx9ikcs4dmb5wcx7c7nqvqwpd81a7r411lpiyhn0sbi")))

(define-public crate-bytesize-1.0.1 (c (n "bytesize") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nl2j2bi9nk3q564jhyb77947wdv5ch54r14gjv4c59s563qd8c1")))

(define-public crate-bytesize-1.0.2 (c (n "bytesize") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ckpa57i821j0nk131jbj77mmkcacxf8nw2cr8g549sdd2qqvc5p") (y #t)))

(define-public crate-bytesize-1.1.0 (c (n "bytesize") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0w7wmmbcirxp5isza0i1lxq5d7r7f0z1pxbxl5f6s1n5m8vfqn3c")))

(define-public crate-bytesize-1.2.0 (c (n "bytesize") (v "1.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "1rdjrxbdbqr3rg6l02k97d5x99s2vbis3y8w9swa8d7zksbw5z1q") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-bytesize-1.3.0 (c (n "bytesize") (v "1.3.0") (d (list (d (n "serde") (r "^1.0.185") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "toml") (r "^0.7.6") (d #t) (k 2)))) (h "1k3aak70iwz4s2gsjbxf0ws4xnixqbdz6p2ha96s06748fpniqx3") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

