(define-module (crates-io by te byte) #:use-module (crates-io))

(define-public crate-byte-0.2.0 (c (n "byte") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0l5ddazgz3mjwmkc4zh5nksn4p1x9cj1n1f41hlx8ajakj6z3yfd")))

(define-public crate-byte-0.2.1 (c (n "byte") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "132f0y2p56fm52yy82ddcc5znlpsv99gqjsi6vi89xdbq5qnfpld")))

(define-public crate-byte-0.2.2 (c (n "byte") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "1jyrr87qlhbnl0dmxjskhm028psxryhd6ds4aa8rila6zv1yh3az")))

(define-public crate-byte-0.2.3 (c (n "byte") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0jia8fs79kgnky1scqysjjfmjxrnslazjdbyd1975gc85ixl6pk6")))

(define-public crate-byte-0.2.4 (c (n "byte") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "112kx47awcd5lzc4mx1lay7dsmwx81b99j46dnccz1lgk658vzsf")))

(define-public crate-byte-0.2.5 (c (n "byte") (v "0.2.5") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0iiq0hfkzp5zd82p12g4vp961jx0nmmkvij3jvwk24jbzq2s15w9")))

(define-public crate-byte-0.2.6 (c (n "byte") (v "0.2.6") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "00dxx93n0l8x2cg2v7mcq7aksf92ga43x05ryk5bzdichrdlmzbw")))

(define-public crate-byte-0.2.7 (c (n "byte") (v "0.2.7") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "1k57i2h2ahqrcp8vhd1m6z604p52vw2dkp6wz33m627898zapir1")))

