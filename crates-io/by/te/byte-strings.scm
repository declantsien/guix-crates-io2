(define-module (crates-io by te byte-strings) #:use-module (crates-io))

(define-public crate-byte-strings-0.1.0 (c (n "byte-strings") (v "0.1.0") (d (list (d (n "byte-strings-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)))) (h "0286fcv23bsl5vc59zqvc9gwplc1gmvvz7ym1bk0yvzfqpxp1fjk") (f (quote (("proc-macro-hygiene" "byte-strings-proc-macro/proc-macro-hygiene") ("nightly") ("default"))))))

(define-public crate-byte-strings-0.1.1 (c (n "byte-strings") (v "0.1.1") (d (list (d (n "byte-strings-proc-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)))) (h "10mwc01rzb5jpd2iyfpn00rmhpjm0844r36jrmzjy6r1bp4vcxlw") (f (quote (("proc-macro-hygiene" "byte-strings-proc-macro/proc-macro-hygiene") ("default"))))))

(define-public crate-byte-strings-0.1.2 (c (n "byte-strings") (v "0.1.2") (d (list (d (n "byte-strings-proc-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)))) (h "0r8z558m2a6bxl86gmqi8s2hhxn83awcz760lmk35b8b2wphz4ja") (f (quote (("proc-macro-hygiene" "byte-strings-proc-macro/proc-macro-hygiene") ("nightly") ("default"))))))

(define-public crate-byte-strings-0.1.3 (c (n "byte-strings") (v "0.1.3") (d (list (d (n "byte-strings-proc-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)))) (h "1r7z07ynl4bbys77kam01h6ks7inrv919xiwibq2bckdpiqc0bi5") (f (quote (("proc-macro-hygiene" "byte-strings-proc-macro/proc-macro-hygiene") ("nightly") ("default"))))))

(define-public crate-byte-strings-0.2.0-rc1 (c (n "byte-strings") (v "0.2.0-rc1") (d (list (d (n "byte-strings-proc_macros") (r "^0.2.0-rc1") (d #t) (k 0)))) (h "0w76wn6fqm6528f741nyw659s990nb25qxhff4f40a37x2b6nw4y") (f (quote (("ui-tests" "better-docs" "const-friendly") ("const-friendly") ("better-docs")))) (y #t)))

(define-public crate-byte-strings-0.2.0 (c (n "byte-strings") (v "0.2.0") (d (list (d (n "byte-strings-proc_macros") (r "^0.2.0") (d #t) (k 0)))) (h "14qn4ihfd6zmc1lyrr8d0cissssc9sgbjy4vwvjsrzn2jdv4dzyq") (f (quote (("ui-tests" "better-docs" "const-friendly") ("const-friendly") ("better-docs")))) (y #t)))

(define-public crate-byte-strings-0.2.1 (c (n "byte-strings") (v "0.2.1") (d (list (d (n "byte-strings-proc_macros") (r "^0.2.1") (d #t) (k 0)))) (h "1s5rjhfvzahp4550y14qdn6gyyy324pvmwk994szg16j6mmzjp6d") (f (quote (("ui-tests" "better-docs" "const-friendly") ("const-friendly") ("better-docs"))))))

(define-public crate-byte-strings-0.2.2 (c (n "byte-strings") (v "0.2.2") (d (list (d (n "byte-strings-proc_macros") (r "^0.2.2") (d #t) (k 0)))) (h "1qvsx0awhky1rh2bpd8r46i16mjg70xswbwyvm61y7h4w3bfwg4n") (f (quote (("ui-tests" "better-docs" "const-friendly") ("const-friendly") ("better-docs"))))))

(define-public crate-byte-strings-0.3.0 (c (n "byte-strings") (v "0.3.0") (d (list (d (n "byte-strings-proc_macros") (r "=0.3.0") (d #t) (k 0)))) (h "00v1q6sxkbadpv4dncks167p4fjivigvy8v1jzlb17b9azr356nj") (f (quote (("ui-tests" "better-docs") ("const-friendly") ("better-docs")))) (r "1.65.0")))

(define-public crate-byte-strings-0.3.1 (c (n "byte-strings") (v "0.3.1") (d (list (d (n "byte-strings-proc_macros") (r "=0.3.1") (d #t) (k 0)))) (h "16n2xd2i0xbj2q3qdcb0dmr9phza1razmzk2k3jm117b3x9yabh0") (f (quote (("ui-tests" "better-docs") ("const-friendly") ("better-docs")))) (r "1.65.0")))

