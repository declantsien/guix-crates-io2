(define-module (crates-io by te byteserde_types) #:use-module (crates-io))

(define-public crate-byteserde_types-0.1.1 (c (n "byteserde_types") (v "0.1.1") (d (list (d (n "byteserde") (r "^0.1") (d #t) (k 0)) (d (n "byteserde_derive") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "0l55m370s34qkcbmm8gyfxqzfyyp6b1y0ibf0zw86ds6nj0lm85z") (y #t) (r "1.69")))

(define-public crate-byteserde_types-0.2.0 (c (n "byteserde_types") (v "0.2.0") (d (list (d (n "byteserde") (r "^0.2") (d #t) (k 0)) (d (n "byteserde_derive") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "0byy49dijz1a4anf36cp6snz324br1438p17zy85m7msqv154ipd") (y #t) (r "1.69")))

(define-public crate-byteserde_types-0.3.0 (c (n "byteserde_types") (v "0.3.0") (d (list (d (n "byteserde") (r "^0.3.0") (d #t) (k 0)) (d (n "byteserde_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "0yysmsbfj15r5k2khszalp7ddam36l3hazkj221z5fjsixgnrp0s") (y #t) (r "1.69")))

(define-public crate-byteserde_types-0.4.0 (c (n "byteserde_types") (v "0.4.0") (d (list (d (n "byteserde") (r "^0.4.0") (d #t) (k 0)) (d (n "byteserde_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "1n2anjqfpnxzbpy3ppmg99bf69q0npb65wjqqz5igclij3v5gnrm") (y #t) (r "1.69")))

(define-public crate-byteserde_types-0.5.0 (c (n "byteserde_types") (v "0.5.0") (d (list (d (n "byteserde") (r "^0.5.0") (d #t) (k 0)) (d (n "byteserde_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1nwxx86b2vj55czx83a6nag31km6ma9gylgryx0hwlmq5djgh98p") (y #t) (r "1.69")))

(define-public crate-byteserde_types-0.6.0 (c (n "byteserde_types") (v "0.6.0") (d (list (d (n "byteserde") (r "^0.6.0") (d #t) (k 0)) (d (n "byteserde_derive") (r "^0.6.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00yk1f1m44js03jbrd1zh9jh0cdpz7k9f0ylanvcwaia5lhvrh7d") (y #t) (r "1.69")))

(define-public crate-byteserde_types-0.6.1 (c (n "byteserde_types") (v "0.6.1") (d (list (d (n "byteserde") (r "^0.6.1") (d #t) (k 0)) (d (n "byteserde_derive") (r "^0.6.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1r5c0kgv8c5xnfhhw1gar3ivc0p89j5i79jxy35sqp4sp4np3f2d") (y #t) (r "1.69")))

(define-public crate-byteserde_types-0.6.2 (c (n "byteserde_types") (v "0.6.2") (d (list (d (n "byteserde") (r "^0.6.2") (d #t) (k 0)) (d (n "byteserde_derive") (r "^0.6.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0v4pvgdaayxpg16r597rhqhr6cr2awxk8j7f5biq11878gdjlya0") (r "1.69")))

