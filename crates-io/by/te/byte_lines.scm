(define-module (crates-io by te byte_lines) #:use-module (crates-io))

(define-public crate-byte_lines-0.1.0 (c (n "byte_lines") (v "0.1.0") (d (list (d (n "byte_string") (r "^1.0.0") (d #t) (k 0)))) (h "12cld3nf6jqkwlryf2hxdl0300zhj3fca22l202b517jhpb9438g")))

(define-public crate-byte_lines-0.1.1 (c (n "byte_lines") (v "0.1.1") (d (list (d (n "byte_string") (r "^1.0.0") (d #t) (k 0)))) (h "138xwm12nn9b03n7n0aayx0x7hm7s1crj6xwjmv2jnq6zq5sf1m1")))

