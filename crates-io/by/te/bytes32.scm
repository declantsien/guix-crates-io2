(define-module (crates-io by te bytes32) #:use-module (crates-io))

(define-public crate-bytes32-0.1.0 (c (n "bytes32") (v "0.1.0") (h "17p32wwf73f4r1brkpsxgi23k7r4sqzyzpwimzqgf4jhixk9laln")))

(define-public crate-bytes32-0.1.1 (c (n "bytes32") (v "0.1.1") (h "1d4frkvg558lxyjhz4ahrmcncwykbzjil3ha35nvxy7gqzvp3z90")))

(define-public crate-bytes32-0.1.2 (c (n "bytes32") (v "0.1.2") (h "1ha927q9hfn5x2a560mrr54snvczihycv9v2x536nwz851mwhzm2")))

(define-public crate-bytes32-0.1.3 (c (n "bytes32") (v "0.1.3") (h "1knq6scinp9zkppx3vlzij3483gqzk8d917vmkj6qi58wfx9vic8")))

(define-public crate-bytes32-0.1.4-0 (c (n "bytes32") (v "0.1.4-0") (h "12g3n8ifk1v6vhb5h502px2w01yhhxcxq8836y3bxd0hx73lnycq")))

