(define-module (crates-io by te bytemuck_parsing) #:use-module (crates-io))

(define-public crate-bytemuck_parsing-0.1.0 (c (n "bytemuck_parsing") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13") (d #t) (k 0)))) (h "0ac5f9gz0q8ldw0cqhsffhva7zm5546mlmgb7hm2hn3vnwg02iwp")))

(define-public crate-bytemuck_parsing-0.2.0 (c (n "bytemuck_parsing") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.13") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("from" "display"))) (k 0)))) (h "0awh3qza9ddwryqjglhz2lfkjaqm5pbpjr38zxha7ws5vv3ic7fc")))

