(define-module (crates-io by te byte_conv) #:use-module (crates-io))

(define-public crate-byte_conv-0.1.0 (c (n "byte_conv") (v "0.1.0") (h "00q9d9dmg56a2kwwa13mqlii9zcmk8l2lxs4j9hl71rpisw9c7ir")))

(define-public crate-byte_conv-0.1.1 (c (n "byte_conv") (v "0.1.1") (h "102p9cdcj1a2n3g7pdd5jvbpl9gfjp5g6azw4rx16ca9blqp56b4")))

