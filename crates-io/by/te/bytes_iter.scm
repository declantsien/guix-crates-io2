(define-module (crates-io by te bytes_iter) #:use-module (crates-io))

(define-public crate-bytes_iter-0.1.0 (c (n "bytes_iter") (v "0.1.0") (h "0glqyiqic02rblss1641wg8hkf5pzxf17xp204gay7v77kfhb1v9")))

(define-public crate-bytes_iter-0.1.1 (c (n "bytes_iter") (v "0.1.1") (h "0y5pnrqjaaibbzhnzdbq7qxxryh7s7ri3qkc64jm13lhy0kfhznn")))

