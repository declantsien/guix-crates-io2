(define-module (crates-io by te bytemuck) #:use-module (crates-io))

(define-public crate-bytemuck-0.1.0 (c (n "bytemuck") (v "0.1.0") (h "1vsr6k04baqlblx7s5vg9xmnxyiz7qkk7v5016s6ykzyxdns5kzv") (f (quote (("extern_crate_alloc")))) (y #t)))

(define-public crate-bytemuck-0.1.1 (c (n "bytemuck") (v "0.1.1") (h "07kj3s9w86hlhapzch28hsqs37f9c1dhv207xd818j0xrkxhi0py") (f (quote (("extern_crate_alloc")))) (y #t)))

(define-public crate-bytemuck-0.1.2 (c (n "bytemuck") (v "0.1.2") (h "0cqb8v1gcarqdl55lrgp88rlalrxz2qxv3x2fjqj3jzjgvjqjlrv") (f (quote (("extern_crate_alloc")))) (y #t)))

(define-public crate-bytemuck-1.0.0 (c (n "bytemuck") (v "1.0.0") (h "0z4vmibm8ylsn57cmc1prb67by4z995jkfl3zzx87l8b06h5haip") (f (quote (("extern_crate_alloc"))))))

(define-public crate-bytemuck-1.0.1 (c (n "bytemuck") (v "1.0.1") (h "16rzsni2ppfcwkbnh049gjda0wna4hyykl3k540c69dgqls99y94") (f (quote (("extern_crate_alloc"))))))

(define-public crate-bytemuck-1.1.0 (c (n "bytemuck") (v "1.1.0") (h "1xwidlcxvjz2ickvafpc88w4rybdx6l3xxrg5ssi7f40ymn513gy") (f (quote (("extern_crate_alloc"))))))

(define-public crate-bytemuck-1.2.0-alpha.1 (c (n "bytemuck") (v "1.2.0-alpha.1") (h "08c499i1s0nwxfs85jp398qdvzhbv96fvkzpxa4rpfkr3sq02rsm") (f (quote (("extern_crate_alloc"))))))

(define-public crate-bytemuck-1.2.0 (c (n "bytemuck") (v "1.2.0") (h "0cclc9v10hc1abqrxgirg3qbwa3ra3s0dai3xiwv9v4j4bgi7yip") (f (quote (("extern_crate_alloc"))))))

(define-public crate-bytemuck-1.3.0-alpha.0 (c (n "bytemuck") (v "1.3.0-alpha.0") (h "1db114m8s7hjz41rww5zh7pas7k67s3i2wf40psf6j9w0y9mvvwy") (f (quote (("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc")))) (y #t)))

(define-public crate-bytemuck-1.3.0 (c (n "bytemuck") (v "1.3.0") (h "0rf3s6b9izgclz1bqz7vclh7l7vprcy5m22y33jmx930d823c1nl") (f (quote (("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc")))) (y #t)))

(define-public crate-bytemuck-1.3.1 (c (n "bytemuck") (v "1.3.1") (h "1scaac5xbfynzbpvz9yjbmg9ag2jalxfijapwlqh7xldf4li0ynv") (f (quote (("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc"))))))

(define-public crate-bytemuck-1.4.0 (c (n "bytemuck") (v "1.4.0") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "071043n73hwi55z9c55ga4v52v8a7ri56gqja8r98clkdyxns14j") (f (quote (("zeroable_maybe_uninit") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive"))))))

(define-public crate-bytemuck-1.4.1 (c (n "bytemuck") (v "1.4.1") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1b3pc5j3sj73d981470vbxflkx14dpqar49wyx6cbdd3bk4jxaj1") (f (quote (("zeroable_maybe_uninit") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive"))))))

(define-public crate-bytemuck-1.5.0 (c (n "bytemuck") (v "1.5.0") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "18355qn3r9yp7ibg00r688sjx58g2qsjylwyq15w5b41b46asjss") (f (quote (("zeroable_maybe_uninit") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive"))))))

(define-public crate-bytemuck-1.5.1 (c (n "bytemuck") (v "1.5.1") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0n6zqqjkk80j0vyck95gr627anjkrql6cdl3iyx86fsnj0h7xmdy") (f (quote (("zeroable_maybe_uninit") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive"))))))

(define-public crate-bytemuck-1.6.0 (c (n "bytemuck") (v "1.6.0") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1gq26pxzsakibs9bfa7w9x97sb98h6k809r2ch3n6m60sbxlplhv") (f (quote (("zeroable_maybe_uninit") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive")))) (y #t)))

(define-public crate-bytemuck-1.6.1 (c (n "bytemuck") (v "1.6.1") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0fp49s1n4dk8hda6yq8r6zx5d8853v0clmkdfjw5afyzdpab2i8d") (f (quote (("zeroable_maybe_uninit") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive")))) (y #t)))

(define-public crate-bytemuck-1.6.2 (c (n "bytemuck") (v "1.6.2") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "19yhagv02dxhxjjj1npfs6gyscv5z3ib5npxbjigs6vrh35rqdid") (f (quote (("zeroable_maybe_uninit") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive")))) (y #t)))

(define-public crate-bytemuck-1.6.3 (c (n "bytemuck") (v "1.6.3") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "120kghhz9w8x93gclyac5zmqvvka06hkjiw4vmfdkhgrr4aq35af") (f (quote (("zeroable_maybe_uninit") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive")))) (y #t)))

(define-public crate-bytemuck-1.7.0 (c (n "bytemuck") (v "1.7.0") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0dc4i6s9l86h2wb2h4r4njp80lm3a5i3k860p9fph3sdf6mx4rlr") (f (quote (("zeroable_maybe_uninit") ("unsound_ptr_pod_impl") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive")))) (y #t)))

(define-public crate-bytemuck-1.7.1 (c (n "bytemuck") (v "1.7.1") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0jxhavc7nr0v7j0fkmshf3mpvgji1b45ssvhpibabfbblrlr8imn") (f (quote (("zeroable_maybe_uninit") ("unsound_ptr_pod_impl") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive")))) (y #t)))

(define-public crate-bytemuck-1.7.2 (c (n "bytemuck") (v "1.7.2") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0nrcrwj7giny1ds3g67g4y1fpb9h70a8cm4az272pf0xqi3755bj") (f (quote (("zeroable_maybe_uninit") ("unsound_ptr_pod_impl") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive"))))))

(define-public crate-bytemuck-1.7.3 (c (n "bytemuck") (v "1.7.3") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0bvi884940pgp8a3jzx9yll8316894gfz153f1jip3f3p3k8k6a3") (f (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive"))))))

(define-public crate-bytemuck-1.8.0 (c (n "bytemuck") (v "1.8.0") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1b8y2j3mcs3m5p9i12ljnr2nadypjx3qlq01d0ryfwa8qakir18f") (f (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.9.0 (c (n "bytemuck") (v "1.9.0") (d (list (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "00gq5r11rpzvzq6mi1gg0n6p2k4wm3gskc0hpxbbik7s4lhhw7pf") (f (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.9.1 (c (n "bytemuck") (v "1.9.1") (d (list (d (n "bytemuck_derive") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1k02rxlr412d2wfgv7j567g27zj11q66gsxfzx51k761vrdxisnd") (f (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.10.0 (c (n "bytemuck") (v "1.10.0") (d (list (d (n "bytemuck_derive") (r "^1.1") (o #t) (d #t) (k 0)))) (h "12j0vfv746rhjzbr555sxzrfw90s727njwn5sf7dyx62gs8zlgf5") (f (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.11.0 (c (n "bytemuck") (v "1.11.0") (d (list (d (n "bytemuck_derive") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0ddqr3zi5fqcaj49pdganrnjhlwzcr02v77j446icjp7cn47qdx5") (f (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.12.0 (c (n "bytemuck") (v "1.12.0") (d (list (d (n "bytemuck_derive") (r "^1.2") (o #t) (d #t) (k 0)))) (h "1zbib4nd8y3ypprkanr9fccm90z674vmhxwy3s754yhlnijcpy24") (f (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd")))) (y #t)))

(define-public crate-bytemuck-1.12.1 (c (n "bytemuck") (v "1.12.1") (d (list (d (n "bytemuck_derive") (r "^1.2.1") (o #t) (d #t) (k 0)))) (h "1nliallfh3f008ybs9d6ivac2pbvhh3adxdyqa7mk8dmj7j1amrg") (f (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.12.2 (c (n "bytemuck") (v "1.12.2") (d (list (d (n "bytemuck_derive") (r "^1.2.1") (o #t) (d #t) (k 0)))) (h "1n5zj5s8d4qi0arv9glz07lmxn8hawpgg08crlkzkqz6sksi9v2s") (f (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.12.3 (c (n "bytemuck") (v "1.12.3") (d (list (d (n "bytemuck_derive") (r "^1.2.1") (o #t) (d #t) (k 0)))) (h "0zwlaqkrp7r7bnl2n40x9ncpspb93d8xcckar61f54nal7csi8xa") (f (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.12.4 (c (n "bytemuck") (v "1.12.4") (d (list (d (n "bytemuck_derive") (r "^1.2.1") (o #t) (d #t) (k 0)))) (h "10168cwiksjgqwh8n30jsrapnbb0p934dglnia636dmdi7gcqjd5") (f (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd")))) (y #t)))

(define-public crate-bytemuck-1.13.0 (c (n "bytemuck") (v "1.13.0") (d (list (d (n "bytemuck_derive") (r "^1.4") (o #t) (d #t) (k 0)))) (h "14v3ps53drpljf48sfrw2sd8a61zv93n89bbp2q0r228n3md6hf0") (f (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.13.1 (c (n "bytemuck") (v "1.13.1") (d (list (d (n "bytemuck_derive") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1sifp93886b552fwbywmp5f4gysar7z62mhh4y8dh5gxhkkbrzhp") (f (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.14.0 (c (n "bytemuck") (v "1.14.0") (d (list (d (n "bytemuck_derive") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1ik1ma5n3bg700skkzhx50zjk7kj7mbsphi773if17l04pn2hk9p") (f (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("nightly_docs") ("must_cast") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("align_offset") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.14.1 (c (n "bytemuck") (v "1.14.1") (d (list (d (n "bytemuck_derive") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1n9bjrxhngiv0lq05f7kl0jw5wyms4z1vqv7q6a2nks01xh9097d") (f (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("nightly_docs") ("must_cast") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("align_offset") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.14.2 (c (n "bytemuck") (v "1.14.2") (d (list (d (n "bytemuck_derive") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0aylwb0l3zx2c212k2nwik4zmbhw5826y7icav0w2ja9vadxccga") (f (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("nightly_docs") ("must_cast") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("align_offset") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.14.3 (c (n "bytemuck") (v "1.14.3") (d (list (d (n "bytemuck_derive") (r "^1.4") (o #t) (d #t) (k 0)))) (h "17xpdkrnw7vcc72cfnmbs6x1pndrh5naj86rkdb4h6k90m7h7vx2") (f (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("nightly_docs") ("must_cast") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("align_offset") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.15.0 (c (n "bytemuck") (v "1.15.0") (d (list (d (n "bytemuck_derive") (r "^1.4") (o #t) (d #t) (k 0)))) (h "05gxh5i8vhjhr8b7abzla1k74m3khsifr439320s18rmfb2nhvax") (f (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("nightly_docs") ("must_cast") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("align_offset") ("aarch64_simd"))))))

(define-public crate-bytemuck-1.16.0 (c (n "bytemuck") (v "1.16.0") (d (list (d (n "bytemuck_derive") (r "^1.4") (o #t) (d #t) (k 0)))) (h "19dwdvjri09mhgrngy0737965pchm25ix2yma8sgwpjxrcalr0vq") (f (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("nightly_docs") ("must_cast") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("const_zeroed") ("align_offset") ("aarch64_simd"))))))

