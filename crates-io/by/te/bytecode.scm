(define-module (crates-io by te bytecode) #:use-module (crates-io))

(define-public crate-bytecode-0.1.0 (c (n "bytecode") (v "0.1.0") (d (list (d (n "tiny-ansi") (r "^0.1.0") (d #t) (k 0)))) (h "1kycywvv4d6nidgc1bc5ifmh6ybqgml0zx04q32k8sazspgqlggm")))

(define-public crate-bytecode-0.1.1 (c (n "bytecode") (v "0.1.1") (d (list (d (n "tiny-ansi") (r "^0.1.0") (d #t) (k 0)))) (h "17m4v1kqcg5zdm9gxiphnf47qihcliqgnmz5maa0r9fwcyg1whyv")))

(define-public crate-bytecode-0.2.0 (c (n "bytecode") (v "0.2.0") (d (list (d (n "tiny-ansi") (r "^0.1.0") (d #t) (k 0)))) (h "1p6ccyvadxi3ch89zs7nzck1vzpc69zg6yx9w7ipzr718z14j924")))

(define-public crate-bytecode-0.3.0 (c (n "bytecode") (v "0.3.0") (d (list (d (n "tiny-ansi") (r "^0.1.0") (d #t) (k 0)))) (h "0cx3qyilfx56wlfhc85br6mrlpwxvvbf4rgxw985sc2xg7xwns02")))

