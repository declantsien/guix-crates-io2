(define-module (crates-io by te byte_marks) #:use-module (crates-io))

(define-public crate-byte_marks-0.1.0 (c (n "byte_marks") (v "0.1.0") (h "0mn6k0adzf7ryd0vvc3g9wp9sjjajq4hs99d32cinibfk7baywk7") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-byte_marks-0.1.1 (c (n "byte_marks") (v "0.1.1") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.18") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0vzy5pcxc2xbqk32ibrgyp1n4g6yjszsg92ivxyqri9svv65xvn7") (f (quote (("std") ("default_async" "async-std" "futures") ("default" "std"))))))

(define-public crate-byte_marks-0.1.2 (c (n "byte_marks") (v "0.1.2") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.18") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0dh50yqdjw1sdq78dbb28q22cs34ai9bxwmfrvgcn2ryk50nxpqd") (f (quote (("std") ("default_async" "async-std" "futures") ("default" "std"))))))

(define-public crate-byte_marks-0.1.3 (c (n "byte_marks") (v "0.1.3") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.18") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1jhwfddxgs8az2kcwzwxzbr89j815m27339i0fjzkf68rrzrzykx") (f (quote (("std") ("default_async" "async-std" "futures") ("default" "std"))))))

(define-public crate-byte_marks-0.1.4 (c (n "byte_marks") (v "0.1.4") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "17kvaxvzl0baq36amizsj3svbihvqky2yj9cxrv037g3rkn862ja") (f (quote (("std") ("default_async" "async-std") ("default" "std"))))))

(define-public crate-byte_marks-0.1.5 (c (n "byte_marks") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0g25fqhk7v3v6mqyp30si819k2436kgja4x4nqpwdh6lgrqhyg3j")))

(define-public crate-byte_marks-0.1.6 (c (n "byte_marks") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0fl20vy88ma92pwr4q5xdxndvlblyk5pvg4h6bwj5awwg20fpvix")))

(define-public crate-byte_marks-0.1.7 (c (n "byte_marks") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0qbrk9i114y2js6ld5f8hrndkwh7i17g3bddxkrwinlchlzim3fn")))

(define-public crate-byte_marks-0.1.8 (c (n "byte_marks") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1sq7jk370zagj6ism7g6scps2c2ygnlsllcsg569svz3nhx9mi5h")))

(define-public crate-byte_marks-0.1.9 (c (n "byte_marks") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0kbk5c3nys5w5s0w9pfi66jv5pf9c3831hxdwzgfcz5y02ddavfp")))

(define-public crate-byte_marks-0.1.10 (c (n "byte_marks") (v "0.1.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "13cgf2irfapzn4hm2xcxvwj2f2ds9lg1kq79qdz4mg2ylnnkmh49")))

(define-public crate-byte_marks-0.1.11 (c (n "byte_marks") (v "0.1.11") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0mbwgl6a1xgwnm8aw19hj82bi0hyn2070zzsm9179xps4icd58cn")))

