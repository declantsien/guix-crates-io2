(define-module (crates-io by te byte-pool) #:use-module (crates-io))

(define-public crate-byte-pool-0.1.0 (c (n "byte-pool") (v "0.1.0") (d (list (d (n "stable_deref_trait") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "0vvsz3zkx1x5rc9cw1sci289p6kpzniyvf1hx5c48kzxylrxp6p4") (f (quote (("stable_deref" "stable_deref_trait") ("default" "stable_deref"))))))

(define-public crate-byte-pool-0.2.0 (c (n "byte-pool") (v "0.2.0") (d (list (d (n "crossbeam-queue") (r "^0.2.0") (d #t) (k 0)))) (h "1xr3rdc9zx8h1xjn8clzqhc1b4kw132pf8za0s5mfhkpj04nzm2a") (f (quote (("default"))))))

(define-public crate-byte-pool-0.2.1 (c (n "byte-pool") (v "0.2.1") (d (list (d (n "crossbeam-queue") (r "^0.2.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "08g2pbhcm4d79i6kqrjvh32hq2y4f82px6nzpsgqgcf8x81f2hlk") (f (quote (("default"))))))

(define-public crate-byte-pool-0.2.2 (c (n "byte-pool") (v "0.2.2") (d (list (d (n "crossbeam-queue") (r "^0.2.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "0yh96ral0pni02bzm3fhvicp1ixz1hz3c5m03hsyq66mk61fjf0y") (f (quote (("default"))))))

(define-public crate-byte-pool-0.2.3 (c (n "byte-pool") (v "0.2.3") (d (list (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1jphxkfbqyg20swfwy1i6cv4vxgkk4d84za79l4v29xlvc6j7izq") (f (quote (("default"))))))

(define-public crate-byte-pool-0.2.4 (c (n "byte-pool") (v "0.2.4") (d (list (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "0n3iibkqk5lyhwmxz0imwz1gsk4xbvs7q8m6xwjmc2zmi48v5wf2") (f (quote (("default"))))))

