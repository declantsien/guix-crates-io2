(define-module (crates-io by te byte_channel) #:use-module (crates-io))

(define-public crate-byte_channel-0.0.1 (c (n "byte_channel") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "test_futures") (r "^0.0.1") (d #t) (k 2)))) (h "1bm1ys6zi3s9amn1r3q9i6gz41vkx3pdzmxz7vli6gcqnpg4mqq2")))

