(define-module (crates-io by te byte-array-struct) #:use-module (crates-io))

(define-public crate-byte-array-struct-0.1.0 (c (n "byte-array-struct") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1k7a4z18kgplkv66passpykxa9sm83pwn6pn1cvxyd7wpdsppdxc") (f (quote (("serialize" "serde" "serde_json") ("default" "serialize"))))))

(define-public crate-byte-array-struct-0.2.0 (c (n "byte-array-struct") (v "0.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1q63nsg0ai9sh1h2xwabiy8gsmrgjql4389sz6bwn2rq4snll568") (f (quote (("with-serde" "serde" "serde_json") ("default"))))))

