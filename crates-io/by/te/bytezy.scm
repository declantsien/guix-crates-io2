(define-module (crates-io by te bytezy) #:use-module (crates-io))

(define-public crate-bytezy-0.0.1 (c (n "bytezy") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0d4mb3w5f5yrhfa8cd3l4pi0sd3fv15b2v22n1lanb980l5yn0jf")))

