(define-module (crates-io by te byteflow) #:use-module (crates-io))

(define-public crate-byteflow-0.1.0 (c (n "byteflow") (v "0.1.0") (h "0wqd5l7h2gdgn0z57nazgkki4a0126jb97kmaqvhrwhsdcdbqpa4") (y #t)))

(define-public crate-byteflow-0.2.0 (c (n "byteflow") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0zd443nlk6bzxakk1x3x7pwcxgb785kfpwzl7bn06np6yc2zv0ng")))

(define-public crate-byteflow-0.2.1 (c (n "byteflow") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0r4z7lc57pkb13214ph3i1bc3znw1p6fswm60kk6yy4qijmq959i")))

