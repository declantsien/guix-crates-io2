(define-module (crates-io by te byteorder_async) #:use-module (crates-io))

(define-public crate-byteorder_async-1.0.0 (c (n "byteorder_async") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "tokio") (r "^0.2.18") (f (quote ("io-util"))) (o #t) (d #t) (k 0)))) (h "1y6h98nbcq09qyxjlkx1yrxgga6g4zb0q4viwkp0y72gys78ziwc") (f (quote (("tokio_async" "tokio") ("std") ("i128") ("futures_async" "futures") ("default" "std"))))))

(define-public crate-byteorder_async-1.0.1 (c (n "byteorder_async") (v "1.0.1") (d (list (d (n "futures") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "tokio") (r "^0.2.18") (f (quote ("io-util"))) (o #t) (d #t) (k 0)))) (h "0yzjkjshw9cwm7lh92xs5a84wd8mb0lbbgvpnrb7fan7k7xmqhz4") (f (quote (("tokio_async" "tokio") ("std") ("i128") ("futures_async" "futures") ("default" "std"))))))

(define-public crate-byteorder_async-1.1.0 (c (n "byteorder_async") (v "1.1.0") (d (list (d (n "futures") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "tokio") (r "^0.2.18") (f (quote ("io-util"))) (o #t) (d #t) (k 0)))) (h "15l43s3rvsdq6mr84kmg4rl7ks8cckwp72haxy426d0sacrb6aaj") (f (quote (("tokio_async" "tokio") ("std") ("i128") ("futures_async" "futures") ("default" "std"))))))

(define-public crate-byteorder_async-1.2.0 (c (n "byteorder_async") (v "1.2.0") (d (list (d (n "futures") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "tokio") (r "^0.2.18") (f (quote ("io-util"))) (o #t) (d #t) (k 0)))) (h "1nnw1idv122canpnmq4dx977z5n82lzzj69nncwk9n5lj5v5b1a0") (f (quote (("tokio_async" "tokio") ("std") ("i128") ("futures_async" "futures") ("default" "std"))))))

