(define-module (crates-io by te bytemuck_derive) #:use-module (crates-io))

(define-public crate-bytemuck_derive-1.0.0-alpha.2 (c (n "bytemuck_derive") (v "1.0.0-alpha.2") (d (list (d (n "bytemuck") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1w6kvv0b51y3bb80xpk4scnsmkv6adv0q6ld3zl01asr91q513qn")))

(define-public crate-bytemuck_derive-1.0.0-alpha.3 (c (n "bytemuck_derive") (v "1.0.0-alpha.3") (d (list (d (n "bytemuck") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16fdfpc1wa4v09glv21d3qdpzxnpdmgm4f0hsp89y5vim072rddi")))

(define-public crate-bytemuck_derive-1.0.0 (c (n "bytemuck_derive") (v "1.0.0") (d (list (d (n "bytemuck") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1k59b6g2d87nf32qwhp73vng3al0zklxg64iiwf0pkxy74xf5ni8")))

(define-public crate-bytemuck_derive-1.0.1 (c (n "bytemuck_derive") (v "1.0.1") (d (list (d (n "bytemuck") (r ">=1.2.0, <2.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "0m6cwx3bxl326xr81z75dz5vzm87zmznhpikr19wnycz5y65y8cf")))

(define-public crate-bytemuck_derive-1.1.0 (c (n "bytemuck_derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "03nd7lc6ycqmlhb9bna70l5x1aqr5bkcani724fwcnwph4j3hbjn")))

(define-public crate-bytemuck_derive-1.1.1 (c (n "bytemuck_derive") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (d #t) (k 0)))) (h "101whslj53fj6rvca7756mxbh8p5rihr17nc6mixl8ap1hcg9lng")))

(define-public crate-bytemuck_derive-1.2.0 (c (n "bytemuck_derive") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0r00ipqhq53fl3gnwwy15qi22n4p52yg8r5yp1nckla7n0gdz71k") (y #t)))

(define-public crate-bytemuck_derive-1.2.1 (c (n "bytemuck_derive") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1a94pi65wll5ydg6ya0h4c3kng4yk3nyjym20xj4jscglxgiz7hv")))

(define-public crate-bytemuck_derive-1.3.0 (c (n "bytemuck_derive") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1d1j74dgq9b0wx73hvirsyzr3hmi7ip16bfvwc3q0bzic2wk7qjz")))

(define-public crate-bytemuck_derive-1.4.0 (c (n "bytemuck_derive") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "08k31ga1h8cs8p50k489vzc2ig7ldyx5q30z1h5d90sdjy543jhs")))

(define-public crate-bytemuck_derive-1.4.1 (c (n "bytemuck_derive") (v "1.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (d #t) (k 0)))) (h "14m18wy2rd6k7bjs56695x00wm9k1a900mmkw5n71gcysaf5rppx")))

(define-public crate-bytemuck_derive-1.5.0 (c (n "bytemuck_derive") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (d #t) (k 0)))) (h "1cgj75df2v32l4fmvnp25xxkkz4lp6hz76f7hfhd55wgbzmvfnln")))

(define-public crate-bytemuck_derive-1.6.0 (c (n "bytemuck_derive") (v "1.6.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (d #t) (k 0)))) (h "0q1gkhwdg8xaslanwa45cq8c6rw229l2k1iwz80p8cgd7wps7aad")))

(define-public crate-bytemuck_derive-1.6.1 (c (n "bytemuck_derive") (v "1.6.1") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (d #t) (k 0)))) (h "0289v33y8ls2xc6ilmg13ljhixf9jghb4wr043wdimdylprgm71n")))

(define-public crate-bytemuck_derive-1.7.0 (c (n "bytemuck_derive") (v "1.7.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (d #t) (k 0)))) (h "06vn0ds3xdnyaz8dh3sswjr1g5l49gi8h8a1ig9rp9bl8aq93s0y")))

