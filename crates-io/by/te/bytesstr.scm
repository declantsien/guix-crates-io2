(define-module (crates-io by te bytesstr) #:use-module (crates-io))

(define-public crate-bytesstr-1.0.0 (c (n "bytesstr") (v "1.0.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1bkahs3si4rn22h05f8wsbkwr5sywnbgr38d0rdmprr8szs6sr97") (f (quote (("unsafe") ("default"))))))

(define-public crate-bytesstr-1.0.1 (c (n "bytesstr") (v "1.0.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0r63aaf1f77vcfa16zgcs7v2vpm4gsb2gxlrb9rl5l6viwa6i70g") (f (quote (("unsafe") ("default"))))))

(define-public crate-bytesstr-1.0.2 (c (n "bytesstr") (v "1.0.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1n09ir6kmawqbygg04h7diixxf4337xa9sxj3dw15jail55jhmfj") (f (quote (("unsafe") ("default"))))))

