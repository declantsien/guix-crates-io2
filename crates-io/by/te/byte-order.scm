(define-module (crates-io by te byte-order) #:use-module (crates-io))

(define-public crate-byte-order-0.1.0 (c (n "byte-order") (v "0.1.0") (d (list (d (n "docgen") (r "^0.1.2") (d #t) (k 0)))) (h "02vpcv4arxsgj8iylvggr00d0ndhh4qhi8m799gkx0nhm54lhmkb")))

(define-public crate-byte-order-0.2.0 (c (n "byte-order") (v "0.2.0") (h "1cqcrkdk4spza0b7l898p97avv05in99049f9c58xa92znmr8ypg")))

(define-public crate-byte-order-0.3.0 (c (n "byte-order") (v "0.3.0") (h "1x6hkfpsk2mzijfknjcp9hn2zs3s6c0rlq54mmwmcjpk9cza28dh")))

