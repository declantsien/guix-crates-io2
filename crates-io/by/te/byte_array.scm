(define-module (crates-io by te byte_array) #:use-module (crates-io))

(define-public crate-byte_array-0.1.0 (c (n "byte_array") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1zv3kf20skacxfy7yia9bpqnid8si7gyn5748w91d3sdbsgm8sax")))

(define-public crate-byte_array-0.1.1 (c (n "byte_array") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0j0qj2vlimrwc77vaxjhv7jn9i6ihgiwq18yzmd44j5c3mgy7yr3")))

(define-public crate-byte_array-0.1.2 (c (n "byte_array") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0bkbfpgamirz3apapd2a0q2siircdx83bn3hsj9fzk1w7hcra0ks")))

(define-public crate-byte_array-0.1.3 (c (n "byte_array") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "18sv2bai22n1y3m4n2xbnbndm6m0iv2rmlfj7g41rrpl5g0slg1h")))

