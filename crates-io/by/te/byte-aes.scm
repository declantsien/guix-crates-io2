(define-module (crates-io by te byte-aes) #:use-module (crates-io))

(define-public crate-byte-aes-0.1.0 (c (n "byte-aes") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)))) (h "02in2gazfr2iw7a74plbggqh8r70bfbnfgyd8x8x5nac692d7zyk")))

(define-public crate-byte-aes-0.1.1 (c (n "byte-aes") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)))) (h "1zcv5bjhxpfglkdizc9g2v8qj6afpsisq9awirjxwawhibpaxfcq")))

(define-public crate-byte-aes-0.1.2 (c (n "byte-aes") (v "0.1.2") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)))) (h "0islqrwppjlbglbg3s6hvcr8cmrxda5m6ng8yp5iq0nxsgmzn8cg")))

(define-public crate-byte-aes-0.2.0 (c (n "byte-aes") (v "0.2.0") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)))) (h "1nhh9r4pmng8p7n1ywhbjbiiljxsq2z0dw50si6sc2238wiq7h78")))

(define-public crate-byte-aes-0.2.1 (c (n "byte-aes") (v "0.2.1") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)))) (h "1i9hv1xh0kx9m41kq72khvy269wq36lkf0x96kib07pqgfwgd4h3") (y #t)))

(define-public crate-byte-aes-0.2.2 (c (n "byte-aes") (v "0.2.2") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)))) (h "0y2abqszbc5nrkvsndn93ab9rr1ah2rafxcqmxc317bf81gkd0a6")))

