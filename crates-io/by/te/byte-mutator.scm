(define-module (crates-io by te byte-mutator) #:use-module (crates-io))

(define-public crate-byte-mutator-0.1.0 (c (n "byte-mutator") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0555w62i9dmlayrgrkg8rwl5n3jy6bbjmfkvywwzl7bllfcf0507")))

(define-public crate-byte-mutator-0.1.1 (c (n "byte-mutator") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0nnh3x0ai6zpwnfkvqbq274mx7rdiz098yqhh7j9iyksf489ikva")))

(define-public crate-byte-mutator-0.1.2 (c (n "byte-mutator") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1dbkb1wgr2d1gj3cal6qlnd5pichz75ill42m226nh36n7g65mrz")))

(define-public crate-byte-mutator-0.2.0 (c (n "byte-mutator") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "19pi3zhzhq4g4xzpl01y7rb4455g74nbvk9pgcb4s6yz8sa5phlr")))

