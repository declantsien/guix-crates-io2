(define-module (crates-io by te byte_units) #:use-module (crates-io))

(define-public crate-byte_units-0.1.0 (c (n "byte_units") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)))) (h "01cza6d363qbvz3h8gfrn2jzbypdv1661yak5nbxwxaj6p13yzdd")))

(define-public crate-byte_units-0.1.1 (c (n "byte_units") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)))) (h "105hr5mrxbx58k8685jnbjl08iv43msc79g0s30gbg180lqb12lb")))

(define-public crate-byte_units-0.1.2 (c (n "byte_units") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)))) (h "0zi5591b23iib30shgz0rbnma89jk9r609s0n8m0nqhpn9dic4j4")))

(define-public crate-byte_units-0.1.3 (c (n "byte_units") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 0)))) (h "07kmaa712whg7whnivasl4xhhxsqd4c4n8x9skr2n3hiksxfs6cz")))

