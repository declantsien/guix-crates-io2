(define-module (crates-io by te bytey_bincode) #:use-module (crates-io))

(define-public crate-bytey_bincode-0.1.0 (c (n "bytey_bincode") (v "0.1.0") (d (list (d (n "bincode") (r "^2.0.0-beta.2") (d #t) (k 0)) (d (n "bytey_byte_buffer") (r "^0.2.0") (d #t) (k 0)))) (h "03qqqafqvbr2zf32i2bsw7fxsxjrv3n1lffz1f3xl46l6akf7naa")))

(define-public crate-bytey_bincode-0.1.1 (c (n "bytey_bincode") (v "0.1.1") (d (list (d (n "bincode") (r "^2.0.0-beta.2") (d #t) (k 0)) (d (n "bytey_byte_buffer") (r "^0.2.1") (d #t) (k 0)))) (h "14s3dj58qf260pxa1hb8cdn1xnk3c9f6i1rbm5sp8l6yrmhxankj")))

