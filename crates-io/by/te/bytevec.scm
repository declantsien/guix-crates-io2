(define-module (crates-io by te bytevec) #:use-module (crates-io))

(define-public crate-bytevec-0.1.0 (c (n "bytevec") (v "0.1.0") (h "0fz7f7ixma09qjn2ncr70wx9c65kdza0dsvlafd5d3ycnxicinas")))

(define-public crate-bytevec-0.1.1 (c (n "bytevec") (v "0.1.1") (h "0gdyav3w8jxlzlyimbm6hycm3fv8xpprf9zymkksj2hk6xq02f37")))

(define-public crate-bytevec-0.2.0 (c (n "bytevec") (v "0.2.0") (h "0hrb7ilnsa2wia8y881zii8hj55iqs7x2yq7kiv4wl405nnyc7hd")))

