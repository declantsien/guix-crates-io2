(define-module (crates-io by te bytes_chain) #:use-module (crates-io))

(define-public crate-bytes_chain-1.1.7 (c (n "bytes_chain") (v "1.1.7") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "1sj461vsi5s6pgfl7zdkvbygmklrp69z3m4f077wf8af9k14da7m")))

(define-public crate-bytes_chain-1.1.8 (c (n "bytes_chain") (v "1.1.8") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "0k4dnqsn8sihb45s3hxlw8cad151lryss1qbi1s0a0bddrmnm2vx")))

