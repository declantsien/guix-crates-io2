(define-module (crates-io by te bytenum) #:use-module (crates-io))

(define-public crate-bytenum-0.1.0 (c (n "bytenum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jk71n81l5cpbl46wh4hpkgg4470nhrf83qbhmkbqry2gbdvh54l")))

(define-public crate-bytenum-0.1.1 (c (n "bytenum") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1a64dzvjiv1k5gj17bqqrfvj9n8r2hwa98k9vlml2gq5fq479fs4")))

(define-public crate-bytenum-0.1.2 (c (n "bytenum") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jh3fl8dpn5aqhp703k5r7rn744b0c8vabz7j23m6ajk7a7ijakp")))

(define-public crate-bytenum-0.1.3 (c (n "bytenum") (v "0.1.3") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1s19n3jkqn5lkmm1dpsl9ylrjb0r2554grmjy5z24jhavnbyskzr")))

(define-public crate-bytenum-0.1.4 (c (n "bytenum") (v "0.1.4") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13w0fa6ig0rj4psrdmn4dkf86cas3x4ny8lnxgpv4p44fm3ymb8a")))

(define-public crate-bytenum-0.1.5 (c (n "bytenum") (v "0.1.5") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hw362h807pkdwm58m2mmq10bcs6vb33vfqhs33xj7ri136n58zf")))

(define-public crate-bytenum-0.1.6 (c (n "bytenum") (v "0.1.6") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1i1sh5n5yhlk56v81hpfjnckjf65gn5dmp1pnyjxi4cbkwpp71wb")))

(define-public crate-bytenum-0.1.7 (c (n "bytenum") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fpa391ki3xgky5hkifdn9cv0vhmaxqwh1bm6wc2qk1ng52fnd0v")))

(define-public crate-bytenum-0.1.8 (c (n "bytenum") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k4ykfnw9n26lk64xws23q4y7ad9p5fr4mcsllksnyxfm508v8cq")))

(define-public crate-bytenum-0.1.9 (c (n "bytenum") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ii1chid4f4mz15ggh4xaw1k2d2apqmxdk6jxac3psy99nk6lbx2")))

