(define-module (crates-io by te bytebuffer) #:use-module (crates-io))

(define-public crate-bytebuffer-0.1.0 (c (n "bytebuffer") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)))) (h "0f7994vv8vmhnabidrry4qk22fi3088p66nnpvfglmrwj5g6rx7s")))

(define-public crate-bytebuffer-0.1.1 (c (n "bytebuffer") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)))) (h "1yq9j3320090rl3lrcxwbh7ql49chjn66lgclhy6x4fb2r9d2wc6")))

(define-public crate-bytebuffer-0.2.0 (c (n "bytebuffer") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)))) (h "11947bwxgh7w7ycsb1zqqg0fh3s7nw8ip7p906f27wwdfxjwz0rv")))

(define-public crate-bytebuffer-0.2.1 (c (n "bytebuffer") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)))) (h "1wvcazv8q79maf7i6akf0w4nn6a962s8pbmfaa0q526ki49s48p8")))

(define-public crate-bytebuffer-2.0.1 (c (n "bytebuffer") (v "2.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1s9ay9vv7m2mrnlxwjp5c6kbzc1wgrlqjgjap4z65sfhdivaz4aq")))

(define-public crate-bytebuffer-2.1.0 (c (n "bytebuffer") (v "2.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1dpm676khsb2kcggb9wfbvw7v1g6ar8mny9f6ndbhqsqhzl0nqra")))

(define-public crate-bytebuffer-2.1.1 (c (n "bytebuffer") (v "2.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0yrgqx0l54j2p5b8i35g1hzw3k706rlc3dkpgyp6k40ajspnh96f")))

(define-public crate-bytebuffer-2.2.0 (c (n "bytebuffer") (v "2.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "half") (r "^2.3.1") (o #t) (d #t) (k 0)))) (h "0z2pkds7lrfm3v1banifd69br763gb1j3df6rmscvjh8rpvzlyzs") (f (quote (("default")))) (s 2) (e (quote (("half" "dep:half"))))))

