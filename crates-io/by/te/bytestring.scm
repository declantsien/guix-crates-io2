(define-module (crates-io by te bytestring) #:use-module (crates-io))

(define-public crate-bytestring-0.1.0 (c (n "bytestring") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.2") (d #t) (k 0)))) (h "1jj31j9riyjq8is1ip5hbi45nm07xrrfs7gm9yaqdq1zzirrgf8c")))

(define-public crate-bytestring-0.1.1 (c (n "bytestring") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5.2") (d #t) (k 0)))) (h "0dck240lhcpb1gcaza5czanbkx1czjqb2dpwxs7rasfp6sx2m1zy")))

(define-public crate-bytestring-0.1.2 (c (n "bytestring") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)))) (h "178378s64sq7licfl1hl48v9489vfrg3z3d5rb90ir1j8ix10k5j")))

(define-public crate-bytestring-0.1.3 (c (n "bytestring") (v "0.1.3") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)))) (h "0szfb8nv75015xlljdzvzbrhm5gx040yfd4l79ifv5z23v624vcc")))

(define-public crate-bytestring-0.1.4 (c (n "bytestring") (v "0.1.4") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)))) (h "1m7advs49l66s7nj72ic6scx1c1y8ah64k06fj4crxlfymkp89pw")))

(define-public crate-bytestring-0.1.5 (c (n "bytestring") (v "0.1.5") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qzkncgky5p5vsdb9msmfg6d92dcs9idcjcr5nk7inkja7x0az7w")))

(define-public crate-bytestring-1.0.0 (c (n "bytestring") (v "1.0.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ka9gkn2rrxms0d5s32ckpchh06qmgidbh4xw630gfcpkshnnw4h")))

(define-public crate-bytestring-1.1.0 (c (n "bytestring") (v "1.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "12ljqf7v6x094n89vy6s1ab130dy2abxfp5g0vphi204sdgsgdl6")))

(define-public crate-bytestring-1.2.0 (c (n "bytestring") (v "1.2.0") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18b9gbd19h4pgh6mffdrh0c28qs4d7i04is0apilhh8mv5bkxy7p")))

(define-public crate-bytestring-1.3.0 (c (n "bytestring") (v "1.3.0") (d (list (d (n "bytes") (r "^1.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1blscywg9gaw6zdc5hqsf9zwyqiym57q631nk7wc960dfs34i3i3")))

(define-public crate-bytestring-1.3.1 (c (n "bytestring") (v "1.3.1") (d (list (d (n "bytes") (r "^1.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0wpf0c5c72x3ycdb85vznkmcy8fy6ckzd512064dyabbx81h5n3l") (r "1.65")))

