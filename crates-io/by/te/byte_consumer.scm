(define-module (crates-io by te byte_consumer) #:use-module (crates-io))

(define-public crate-byte_consumer-0.1.0 (c (n "byte_consumer") (v "0.1.0") (h "1cjw68gxcvq7j7hcqfff8by1fg33w6sj40426xp3dk9ahw2r0fl9")))

(define-public crate-byte_consumer-0.2.0 (c (n "byte_consumer") (v "0.2.0") (h "1d6zsxd1qqn4j6198v67a7n18y1w7z0dvkcw2sjzfgs00d0mqc4q")))

