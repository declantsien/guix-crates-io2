(define-module (crates-io by te byte_counter) #:use-module (crates-io))

(define-public crate-byte_counter-0.1.0 (c (n "byte_counter") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.171") (d #t) (k 0)))) (h "1bb2bir46izyzg0cfh46x7jp42klhis55w6qadpfs4bkwn55gkdi")))

(define-public crate-byte_counter-0.2.0 (c (n "byte_counter") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.171") (d #t) (k 0)))) (h "0bj9izhcpflvpag24dd19i9vqgar9qm008i3xmg889pqyjcr4ck7")))

(define-public crate-byte_counter-0.3.0 (c (n "byte_counter") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.171") (d #t) (k 0)))) (h "11hvc98rlzw97all6azrcvb9q03ya3xzw4q5zzmqq9kn7bf8p783")))

