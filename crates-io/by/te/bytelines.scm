(define-module (crates-io by te bytelines) #:use-module (crates-io))

(define-public crate-bytelines-1.0.0 (c (n "bytelines") (v "1.0.0") (h "0x9yvd003az0i7y1zbgwh2walzqlk54jg8k41a45cgsabm164p5j")))

(define-public crate-bytelines-1.0.1 (c (n "bytelines") (v "1.0.1") (h "06x2zm0gg8cwxw068qf17l40532xydj81cla8b4pc54zgz74nck2")))

(define-public crate-bytelines-2.0.0 (c (n "bytelines") (v "2.0.0") (h "1l5ppl722q2xvnlv4420hlcglil6rx4bjwb75s4vcsp5m93p26ni")))

(define-public crate-bytelines-2.1.0 (c (n "bytelines") (v "2.1.0") (h "1y999b9qhx5x5bfnwgj4pzcslkdphv2jgf205p6fimzc6j4pi2r1")))

(define-public crate-bytelines-2.2.0 (c (n "bytelines") (v "2.2.0") (h "0pdf39xrl1qdg2hf7zgiri3jjpvr3ak0xmz4rcs3q1hpfphg702b")))

(define-public crate-bytelines-2.2.1 (c (n "bytelines") (v "2.2.1") (h "1nscv011nx8v34fws4cvgh6yjyqfqlla1nv6vlkvjzlz11k4lxjm")))

(define-public crate-bytelines-2.2.2 (c (n "bytelines") (v "2.2.2") (h "0pv1dwbbqws84v31f64g6b3nxi0jbhi59cipwpg6651ys504w7kr")))

(define-public crate-bytelines-2.3.0 (c (n "bytelines") (v "2.3.0") (d (list (d (n "tokio") (r "^1.14") (f (quote ("fs" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 2)))) (h "1i43h3j3yv3az70jywgz69kwziid77x64aghpdbw1yyqa14hi73m")))

(define-public crate-bytelines-2.4.0 (c (n "bytelines") (v "2.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("fs" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 2)))) (h "17x55pg0k30wjqfk8mqbcjh3x98afbx34rj5l7czqdf547isqkvq")))

(define-public crate-bytelines-2.5.0 (c (n "bytelines") (v "2.5.0") (d (list (d (n "futures-util") (r "^0.3") (o #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("fs" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 2)))) (h "1jxacxpb7v0qgh325s5b7mfk90fr63jpr90dar8m47r27imnb5qj") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio" "futures-util"))))))

