(define-module (crates-io by te bytebeat) #:use-module (crates-io))

(define-public crate-bytebeat-0.1.0 (c (n "bytebeat") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)))) (h "1hmildb5ifi4z5593ggdlpp7wdnvscklr5dc4p950qk7x4pjqdal") (f (quote (("default"))))))

(define-public crate-bytebeat-0.2.0 (c (n "bytebeat") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "1ly5ig9nan1akcahvjcnbar67sldlfwj4mbkhkdv5mr305dnm3w7") (f (quote (("default"))))))

(define-public crate-bytebeat-0.2.1 (c (n "bytebeat") (v "0.2.1") (d (list (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "0pl8mpfrprx3xrg9z71ckhxvz3ki7aw6ix246il6wrzzpdxqxwzg") (f (quote (("default"))))))

(define-public crate-bytebeat-0.3.0 (c (n "bytebeat") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "0747pmdq2nbnzpj69ld22fs5fr50galg759m8gn3d88pypbamm5f") (f (quote (("jack" "cpal/jack") ("default"))))))

(define-public crate-bytebeat-0.4.0 (c (n "bytebeat") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)))) (h "0mcdx7lq63k2nxv8j40fqzpahs0l3mqh64ja2azpfhc0sm2yssd7") (f (quote (("jack" "cpal/jack") ("default"))))))

