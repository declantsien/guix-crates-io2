(define-module (crates-io by te byteorder-sgx) #:use-module (crates-io))

(define-public crate-byteorder-sgx-0.0.1 (c (n "byteorder-sgx") (v "0.0.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "sgx_tstd") (r "^1.0.9") (d #t) (t "cfg(not(target_env = \"sgx\"))") (k 0)))) (h "1nzsvy57r81wcpldnf22ldbflydfb8hwjqfwp7djwjma60zms99y") (f (quote (("std") ("i128") ("default" "std"))))))

