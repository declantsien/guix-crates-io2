(define-module (crates-io by te bytez) #:use-module (crates-io))

(define-public crate-bytez-0.1.0 (c (n "bytez") (v "0.1.0") (d (list (d (n "goblin") (r "^0.8.0") (d #t) (k 0)))) (h "07krzrllapz5niqhgsbkza7r3q5xkal1ybl3k4ymr6pbjy1b8p3n")))

(define-public crate-bytez-0.1.1 (c (n "bytez") (v "0.1.1") (d (list (d (n "goblin") (r "^0.8.0") (d #t) (k 0)))) (h "0zmip8zn9xran3c1k7k2dqshpk80jvd3gl5r2kihc0jsqcprac1l")))

