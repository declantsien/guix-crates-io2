(define-module (crates-io by te byte-arena) #:use-module (crates-io))

(define-public crate-byte-arena-0.1.0 (c (n "byte-arena") (v "0.1.0") (h "0ax4gqqz7a2430zhycpknryh3m94jf63bd35jl6bzpd0dps71mga") (f (quote (("zero_headers") ("default" "zero_headers"))))))

(define-public crate-byte-arena-0.1.1 (c (n "byte-arena") (v "0.1.1") (h "1wqzl6f3xagq8zk4pfqw8ya7k4axj7zz65pdyrxqbjmby6kfca4x") (f (quote (("zero_headers") ("default" "zero_headers"))))))

(define-public crate-byte-arena-0.1.2 (c (n "byte-arena") (v "0.1.2") (h "1riv5pnq0xgn3gp630p97n82dv7yvpvyf82wiad5h62mzjkqsxfk") (f (quote (("zero_headers") ("default" "zero_headers"))))))

(define-public crate-byte-arena-0.2.0 (c (n "byte-arena") (v "0.2.0") (h "1ll4hmaj3zdsbnagwvazpwvaqgvhmvznhmxzcl4wnz3nqvbl1bff") (f (quote (("zero_headers") ("default"))))))

