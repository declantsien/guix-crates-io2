(define-module (crates-io by te byte-style-encoder) #:use-module (crates-io))

(define-public crate-byte-style-encoder-0.1.1 (c (n "byte-style-encoder") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.19") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "nom") (r "^6.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1p5z995r2jffa53rpfbcri4gh8n78cs0c2dvi4sldbdl8yvvfvhz")))

