(define-module (crates-io by te byte-enum) #:use-module (crates-io))

(define-public crate-byte-enum-0.1.1 (c (n "byte-enum") (v "0.1.1") (d (list (d (n "byte-enum-derive") (r "^0.1.1") (d #t) (k 0)))) (h "00y7ii8ad99jxpsfa675rhh3af0knrv7jba5w2flkpy9m5yg67fl") (f (quote (("external_doc")))) (y #t)))

(define-public crate-byte-enum-0.1.2 (c (n "byte-enum") (v "0.1.2") (d (list (d (n "byte-enum-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1cmlqw7dy1g5k5f049758zzgd405ic0a8xs2h5wlw3k69fbrmksj") (y #t)))

