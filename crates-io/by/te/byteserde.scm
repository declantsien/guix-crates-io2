(define-module (crates-io by te byteserde) #:use-module (crates-io))

(define-public crate-byteserde-0.1.1 (c (n "byteserde") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "text-diff") (r "^0.4") (d #t) (k 2)))) (h "1lkfmydsp0q8s8fpjgd8lfmkcj9ms5iq21lrglv0hibqyghg9if5") (y #t) (r "1.69")))

(define-public crate-byteserde-0.2.0 (c (n "byteserde") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "text-diff") (r "^0.4") (d #t) (k 2)))) (h "0a48cbb9qz8n95jqksbs3lsy88wnihdcwzd91hgxps77spmlwqni") (y #t) (r "1.69")))

(define-public crate-byteserde-0.3.0 (c (n "byteserde") (v "0.3.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "text-diff") (r "^0.4") (d #t) (k 2)))) (h "1hs96zhxdvxah0lahydl26i7k31g3wn78wvnfly6r2pbhs4y5nwi") (y #t) (r "1.69")))

(define-public crate-byteserde-0.4.0 (c (n "byteserde") (v "0.4.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "text-diff") (r "^0.4") (d #t) (k 2)))) (h "0r9rac89326dqp5ffl494lhfyaaj4sfm6bkdcx77m0d985mjjsfp") (y #t) (r "1.69")))

(define-public crate-byteserde-0.5.0 (c (n "byteserde") (v "0.5.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "text-diff") (r "^0.4") (d #t) (k 2)))) (h "0kjln0pwk6r6qnnspb0jrhxiynpn9vng21pjg13h5ynspdidid9n") (y #t) (r "1.69")))

(define-public crate-byteserde-0.6.0 (c (n "byteserde") (v "0.6.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "text-diff") (r "^0.4") (d #t) (k 2)))) (h "03adv3rdf658pa3yw763khwpw5bqn8grfqjsgdmhyhyvqk0ignhd") (y #t) (r "1.69")))

(define-public crate-byteserde-0.6.1 (c (n "byteserde") (v "0.6.1") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "text-diff") (r "^0.4") (d #t) (k 2)))) (h "0h8c8sl5n8y08w19fn15zk3cs94g4w6vrgyh5y1f07par2s4zhqb") (y #t) (r "1.69")))

(define-public crate-byteserde-0.6.2 (c (n "byteserde") (v "0.6.2") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "text-diff") (r "^0.4") (d #t) (k 2)))) (h "0666fkic7z1434arnlhkgg4qvg7x3n4kykvqb63b2dd8fnsd6kda") (r "1.69")))

