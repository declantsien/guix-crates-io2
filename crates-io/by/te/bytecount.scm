(define-module (crates-io by te bytecount) #:use-module (crates-io))

(define-public crate-bytecount-0.1.1 (c (n "bytecount") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1c50zc4qv7f7vyam9c1iwvc97vabr03r06x1p89wnfydqf86s8q8")))

(define-public crate-bytecount-0.1.2 (c (n "bytecount") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1yn8kh3rbaym9pvqxsnhfdbz92ngbgdzip9w37fpk7nwssjv7z9h")))

(define-public crate-bytecount-0.1.4 (c (n "bytecount") (v "0.1.4") (d (list (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "simd") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "04ylwllxqhfpqkyqdzqx8b3f11y8i951fg4dn00273jp2lcw5qs9") (f (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.1.5 (c (n "bytecount") (v "0.1.5") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "simd") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1v8ab05010jgki5hfks68jwsjsp7zzkqp2k2s0yjg3xr9zdpp7i8") (f (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.1.6 (c (n "bytecount") (v "0.1.6") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "simd") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0jljlpwqzhcaqw1sr4qvpgb6h429ai7vvkvdc55nlwn6r3xhk3qy") (f (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.1.7 (c (n "bytecount") (v "0.1.7") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "simd") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0l2h357m6fi05mdqzsb611h7jizafvwbvd3qc2gy5z210g1vggjb") (f (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.2.0 (c (n "bytecount") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "simd") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1pj9p47gxyw9qx4rrzm57gfdrlv4zzzrs8x7l53fm7b7cchl49xg") (f (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.3.0 (c (n "bytecount") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "simd") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "04zd0xc8qmciwl50gakym4hym5l1rmxrw5n2pn0cfz0r0g2vi7c6") (f (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.3.1 (c (n "bytecount") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "simd") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0i7pzhd9qigy18glwqb2pyrjs84i33haad6zf8j90kn8gv6qa9c8") (f (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.3.2 (c (n "bytecount") (v "0.3.2") (d (list (d (n "criterion") (r "^0.2.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "simd") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1n6bmj66dixkvrm80yxmmln77baj9sw1qahcdv5xnmlz6p7djqgq") (f (quote (("simd-accel" "simd") ("html_report") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.4.0 (c (n "bytecount") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2.4") (k 2)) (d (n "packed_simd") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "13qpy38z5wx0rzcdvr2h0ixbfgi1dbrif068il3hwn3k2mah88mr") (f (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.5.0 (c (n "bytecount") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2.7") (k 2)) (d (n "packed_simd") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "039vrn09fpw1iajsgk52sw5ivzsayvlsxrbwb1wvva11bhrw06f2") (f (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.5.1 (c (n "bytecount") (v "0.5.1") (d (list (d (n "criterion") (r "^0.2.7") (k 2)) (d (n "packed_simd") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0z6a280kiy4kg5v3qw97pbyvwycr17fsm41804i8zpq7nmads3xy") (f (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.6.0 (c (n "bytecount") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "packed_simd") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0vplsx73zncb7mz8x0fs3k0p0rz5bmavj09vjk5nqn4z6fa7h0dh") (f (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.6.1 (c (n "bytecount") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "packed_simd") (r "^0.3.4") (o #t) (d #t) (k 0) (p "packed_simd_2")) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0k3nfbqys7zzg9ax0zf2gsj2xfbypyyz30ykv0k23caxlwxpg6n3") (f (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd")))) (y #t)))

(define-public crate-bytecount-0.6.2 (c (n "bytecount") (v "0.6.2") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "packed_simd") (r "^0.3.4") (o #t) (d #t) (k 0) (p "packed_simd_2")) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0bklbbl5ml9ic18s9kn5iix1grrqc6sypz6hvfn8sjc6zhgv7zkj") (f (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.6.3 (c (n "bytecount") (v "0.6.3") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "packed_simd") (r "^0.3.8") (o #t) (d #t) (k 0) (p "packed_simd_2")) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "173wsvyagflb7ic3hpvp1db6q3dsigr452inslnzmsb3ix3nlrrc") (f (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.6.4 (c (n "bytecount") (v "0.6.4") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "packed_simd") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1rrx6jbc3g50r0ac5rrr7rwqzch27wxbzvagp45wh4y8l81js5dd") (f (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.6.5 (c (n "bytecount") (v "0.6.5") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "packed_simd") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "13651lr31cjslq0dp9lgsbns16sz2rw159c03b0h2yi3nxvj98fi") (f (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd")))) (y #t)))

(define-public crate-bytecount-0.6.6 (c (n "bytecount") (v "0.6.6") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "packed_simd") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "17iym1fi0x58i08i97hn8gr48078b9v8071mgvxjbmsxiya9jvym") (f (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd")))) (y #t)))

(define-public crate-bytecount-0.6.7 (c (n "bytecount") (v "0.6.7") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "packed_simd") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "019j3basq13gzmasbqqlhf4076231aw1v63lbyp27ikgs4sz1rg1") (f (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.6.8 (c (n "bytecount") (v "0.6.8") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1klqfjwn41fwmcqw4z03v6i4imgrf7lmf3b5s9v74hxir8hrps2w") (f (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd"))))))

