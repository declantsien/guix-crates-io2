(define-module (crates-io by te bytestool) #:use-module (crates-io))

(define-public crate-bytestool-0.1.0 (c (n "bytestool") (v "0.1.0") (h "0l0zyyjpvx3k3hydwmc2ji180gxlh5c9l7ryl8nhgaxdahbjy54s") (y #t)))

(define-public crate-bytestool-0.2.0 (c (n "bytestool") (v "0.2.0") (h "0r6nayskyixsy8zjkxb9a1qrh7l0p7ikd6177i0hq59129yfg2qh") (y #t)))

(define-public crate-bytestool-0.3.0 (c (n "bytestool") (v "0.3.0") (h "0751akv1lqbxxk8xldk1bgis23zzkiw9m5ms34gy7dg90byqj2q4") (y #t)))

