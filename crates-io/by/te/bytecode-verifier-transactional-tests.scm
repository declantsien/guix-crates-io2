(define-module (crates-io by te bytecode-verifier-transactional-tests) #:use-module (crates-io))

(define-public crate-bytecode-verifier-transactional-tests-0.1.6 (c (n "bytecode-verifier-transactional-tests") (v "0.1.6") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.1.0") (d #t) (k 2)))) (h "1dab2z5wfc7k4dyi71mi96gr6zpcdw3yqbf12w9qsgzfkd0p0y8x") (y #t)))

(define-public crate-bytecode-verifier-transactional-tests-0.2.0 (c (n "bytecode-verifier-transactional-tests") (v "0.2.0") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.2.0") (d #t) (k 2)))) (h "1c57m1dpxbdsj6wvl2sr47xxq0p4fbkxz3xrfs9vkdpd4kmc0r3v") (y #t)))

(define-public crate-bytecode-verifier-transactional-tests-0.2.1 (c (n "bytecode-verifier-transactional-tests") (v "0.2.1") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.2.0") (d #t) (k 2)))) (h "1dy98cddcb9132089ak12vin946bwrdjm5viv22xmii62nlqic2i") (y #t)))

(define-public crate-bytecode-verifier-transactional-tests-0.3.0 (c (n "bytecode-verifier-transactional-tests") (v "0.3.0") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.3.0") (d #t) (k 2)))) (h "1j7kmljqv17z2gyv808h6bsm4v5rjn34mbkddql51mcnv0lzyr3x") (y #t)))

(define-public crate-bytecode-verifier-transactional-tests-0.3.1 (c (n "bytecode-verifier-transactional-tests") (v "0.3.1") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.3.0") (d #t) (k 2)))) (h "1qfbwddvnzgfbc3flvggh72mxzn6nyb36i1q6laj65hgyypw5xcx") (y #t)))

(define-public crate-bytecode-verifier-transactional-tests-0.3.2 (c (n "bytecode-verifier-transactional-tests") (v "0.3.2") (d (list (d (n "datatest-stable") (r "^0.1.1") (d #t) (k 2)) (d (n "move-transactional-test-runner") (r "^0.3.0") (d #t) (k 2)))) (h "1akxzr7b6fl29njbf6arqsc6nblx0z9169i15x35hdrf0pxy1xf6") (y #t)))

