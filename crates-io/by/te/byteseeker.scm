(define-module (crates-io by te byteseeker) #:use-module (crates-io))

(define-public crate-byteseeker-0.0.0 (c (n "byteseeker") (v "0.0.0") (h "0mb3yb9w7p25zzcfj3nqrxm3ci6wx304mqgl6vmsfmrqkm2jhlsf") (y #t)))

(define-public crate-byteseeker-0.1.0 (c (n "byteseeker") (v "0.1.0") (h "10hap77ksxwskn8lsgwb718c2g441dk2iqa0qfhv68wgzqvgvg70")))

(define-public crate-byteseeker-0.1.1 (c (n "byteseeker") (v "0.1.1") (h "1lvm4ynskwni8cblmnalzisxb0rc5zapw3bng9l3p8m43h9wq9nm")))

(define-public crate-byteseeker-0.1.2 (c (n "byteseeker") (v "0.1.2") (h "090w7z876agg340nvbr3r0ncan70a51rhcv8crznfya1ysfl72k6")))

(define-public crate-byteseeker-0.2.0 (c (n "byteseeker") (v "0.2.0") (h "0gfg7b94ykvisi9v37dr4q3hfq05w4hv4jhyph6avsvxcblilifc")))

(define-public crate-byteseeker-0.2.1 (c (n "byteseeker") (v "0.2.1") (h "1xii6gaj4dibgxzmn0yvhcp7rbf4gzmlp9nhk5mcrrakknqzq4ag")))

