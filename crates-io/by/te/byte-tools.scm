(define-module (crates-io by te byte-tools) #:use-module (crates-io))

(define-public crate-byte-tools-0.1.0 (c (n "byte-tools") (v "0.1.0") (h "0jpsf4ynx3jc3km2y7ngkjmsaqv4m8qf2acxvmbb84d7a9yhdrfb")))

(define-public crate-byte-tools-0.1.1 (c (n "byte-tools") (v "0.1.1") (h "1chqjn1lm8wx871zxw0nry9l5qp1apxfbxkaajqpdj0r1kj088vl")))

(define-public crate-byte-tools-0.1.2 (c (n "byte-tools") (v "0.1.2") (h "1v2vg0mipvp0mcpwafzd2c4bdxk3zyqc4as18z63wx06gkgr4iwp")))

(define-public crate-byte-tools-0.1.3 (c (n "byte-tools") (v "0.1.3") (h "0y8jk7vzxjw5kjvanwhwp1d91qqdgrmi2y42fzlgziq0m2dih689")))

(define-public crate-byte-tools-0.2.0 (c (n "byte-tools") (v "0.2.0") (h "0h2zxygfnn46akmgh8cdp4x6xy4kb0b45rzmj76rxa0j99bk432n")))

(define-public crate-byte-tools-0.3.0 (c (n "byte-tools") (v "0.3.0") (h "10i1pmgcn3p1kym5x72pww195sdlcq6mhisdnpglccp2zpk7j14q")))

(define-public crate-byte-tools-0.3.1 (c (n "byte-tools") (v "0.3.1") (h "1mqi29wsm8njpl51pfwr31wmpzs5ahlcb40wsjyd92l90ixcmdg3")))

