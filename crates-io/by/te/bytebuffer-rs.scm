(define-module (crates-io by te bytebuffer-rs) #:use-module (crates-io))

(define-public crate-bytebuffer-rs-0.3.0 (c (n "bytebuffer-rs") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)))) (h "0xzfd458zhd63cx0h04pvp59ya6pfsn3p7dhy8q2pizwy1692ksb") (y #t)))

(define-public crate-bytebuffer-rs-0.3.1 (c (n "bytebuffer-rs") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "03y9a1407j05fjrxassqasgq67caxmv723xbp0dxbhz83mxsqwgx") (y #t)))

(define-public crate-bytebuffer-rs-0.4.0 (c (n "bytebuffer-rs") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0kja4s3qavjg09wnqk096k4zgw33ks1b37kaspzy7ymz61zhnv5n") (y #t)))

(define-public crate-bytebuffer-rs-1.0.0 (c (n "bytebuffer-rs") (v "1.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0pc9wpqs9p0wgn2mbfihgdbswj1xd4qqj4lb9245p3ya7qlkjj3v") (y #t)))

(define-public crate-bytebuffer-rs-2.0.0 (c (n "bytebuffer-rs") (v "2.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0wm3yk485d182a2m3ihnyw231kbnbhg2n894mphmsipbkx02hk42") (y #t)))

(define-public crate-bytebuffer-rs-2.0.1 (c (n "bytebuffer-rs") (v "2.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "15iypm8ygn573zbs9jfr5aw4a756rp5cwn5dim5z2ksqcwxnx23z")))

