(define-module (crates-io by te bytekey-fix) #:use-module (crates-io))

(define-public crate-bytekey-fix-0.5.0 (c (n "bytekey-fix") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "utf-8") (r "^0.7.5") (d #t) (k 0)))) (h "0i712y26k5f08ihc6nbpmajkl8qzb96vi5v6zzipg5ww6m76wk6h")))

(define-public crate-bytekey-fix-0.5.1 (c (n "bytekey-fix") (v "0.5.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "utf-8") (r "^0.7.5") (d #t) (k 0)))) (h "06d1hdy70j0gvcd1cp74sk0j7z1sjq6fsqj32xqvq4l57b5hypzv")))

