(define-module (crates-io by te bytesutil) #:use-module (crates-io))

(define-public crate-bytesutil-0.1.0 (c (n "bytesutil") (v "0.1.0") (h "1f196icds6h8024lj7hb2yynih53b87ddlfv9dv0d87irbx5k3c1") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytesutil-0.2.0 (c (n "bytesutil") (v "0.2.0") (h "05zayqv2pb4iiw4xkblb4h7rnb77wk8083x9lfmg4fyxl7blxhza") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytesutil-0.3.0 (c (n "bytesutil") (v "0.3.0") (h "151kfla1030kvg7f2hmz0pbzhmnq4ka2dal8d58nigijqgpb32sr") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytesutil-0.4.0 (c (n "bytesutil") (v "0.4.0") (h "06db75qg2bff86lck3sbfnsys83q4mhwwg939siax1vs7n3lhi16") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytesutil-0.5.0 (c (n "bytesutil") (v "0.5.0") (h "1gmf12g0bk0ldn6ljv5w96mnp3m6yb7sfxh6a91kxn1b4cvh9m36") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytesutil-0.6.0 (c (n "bytesutil") (v "0.6.0") (h "0fb84rpdl9vjf11jw43b7d55dzxs9npxyr5z3fqbqkqrqqxn0a7h") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytesutil-0.7.0 (c (n "bytesutil") (v "0.7.0") (h "1fwx4sqk369vd3m2yzanx4ld9ixwva505b69x7v3wfx6nivslagl") (f (quote (("std") ("default" "std"))))))

