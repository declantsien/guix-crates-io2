(define-module (crates-io by te byte-slice) #:use-module (crates-io))

(define-public crate-byte-slice-0.1.0 (c (n "byte-slice") (v "0.1.0") (h "0hw2zqw296dr8sk4ypxsfdz7c4gg1kgv9avwkg74dxbbk74gfpbd")))

(define-public crate-byte-slice-0.1.1 (c (n "byte-slice") (v "0.1.1") (h "1sz4k8m6cv06wz30j6wn4z7jc4hm7z8f7nnj0k34rxv2vn7j2j42")))

(define-public crate-byte-slice-0.1.2 (c (n "byte-slice") (v "0.1.2") (h "1isncg840myxswrhs3yli13h4c0zhj4np9rvzwn4kybj1qz8c2jq")))

(define-public crate-byte-slice-0.1.3 (c (n "byte-slice") (v "0.1.3") (h "01ysxyy6cnsvrcg50zx3hchkvy41mqg1x12h6zvqdvyk2q35rawd")))

(define-public crate-byte-slice-0.1.4 (c (n "byte-slice") (v "0.1.4") (h "02xg02n0xl1l9r3k1jfhj842cgb4j2hxibznc1dngvah2rrhj97n")))

(define-public crate-byte-slice-0.1.5 (c (n "byte-slice") (v "0.1.5") (h "0cvrwjvhkjyyn5hbpd1g30c27c4npfri0k9b8a89dylfmvm4j643")))

(define-public crate-byte-slice-0.1.6 (c (n "byte-slice") (v "0.1.6") (h "0cczl762dg9lkicr5qb7w9i1ycddznnzq1vcksi17fwsnvhnfj9p")))

(define-public crate-byte-slice-0.1.7 (c (n "byte-slice") (v "0.1.7") (h "150c5x2y3xah4v36bxjl6pdy2g54bfipn06c84yj95cwckc4562h")))

(define-public crate-byte-slice-0.1.8 (c (n "byte-slice") (v "0.1.8") (h "07nhgf813dy7hwc8rc8g94n2i0jvyblyp8qfylwnliadv8c53czf")))

(define-public crate-byte-slice-0.1.9 (c (n "byte-slice") (v "0.1.9") (h "036zjdm20l664hk322siyzgm4qi6a19asvdkq2z3flmhxwwiw0av")))

(define-public crate-byte-slice-0.1.10 (c (n "byte-slice") (v "0.1.10") (h "0jgsp3m0w1s5ga7wjxqlb66dy53a9lhbdg0cacfzz1s6l4zvmhmx")))

(define-public crate-byte-slice-0.1.11 (c (n "byte-slice") (v "0.1.11") (h "1wgms9n76mbjmdqxm981qy7qbaw45qnhz0z3riklbxylaadwc31p")))

(define-public crate-byte-slice-0.1.12 (c (n "byte-slice") (v "0.1.12") (h "0kyc66m7g8m91wv13g2sq0cqnjg0g22ifff0xh15c67pn8qc3kzy")))

