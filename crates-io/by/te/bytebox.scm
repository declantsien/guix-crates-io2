(define-module (crates-io by te bytebox) #:use-module (crates-io))

(define-public crate-bytebox-0.1.0 (c (n "bytebox") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gb7y0nn4ask7lrj521pmz9vk9pi0iig3mlrfa91zs0azsn8h0wn") (f (quote (("default" "path")))) (s 2) (e (quote (("path" "dep:dirs"))))))

(define-public crate-bytebox-0.1.1 (c (n "bytebox") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12.1") (o #t) (k 0)) (d (n "bevy") (r "^0.12.1") (k 2)) (d (n "dirs") (r "^5.0.1") (o #t) (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cvp19l5smkvwrx013bfbn50p43izqjyv353xghssf0m1g333abh") (f (quote (("default" "path")))) (s 2) (e (quote (("path" "dep:dirs") ("bevy" "dep:bevy"))))))

