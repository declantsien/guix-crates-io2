(define-module (crates-io by te bytes_parser) #:use-module (crates-io))

(define-public crate-bytes_parser-0.1.0 (c (n "bytes_parser") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0i8idh5gzi7d82ih46fbgk5w9752sclsbh3pz9b9is3fhfvd6z0r")))

(define-public crate-bytes_parser-0.1.1 (c (n "bytes_parser") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "19s19mn2fhv7cfd65v2pxa4wwcsmqr9dinqzqc14n96lhbdkqivq")))

(define-public crate-bytes_parser-0.1.2 (c (n "bytes_parser") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1wff443q7gvdl9dif09lcxkqxfxl7ff88w5rqb04mf6sf6a9lwq2")))

(define-public crate-bytes_parser-0.1.3 (c (n "bytes_parser") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0hrj9wfm6y2f4sk3i020gc8c5an04v8j71973nsjld2x91x2crak")))

(define-public crate-bytes_parser-0.1.4 (c (n "bytes_parser") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0m8lg850yn68f8plm6vs62ccq2nqwd0x9iz65picbv8d14m7amnj")))

(define-public crate-bytes_parser-0.1.5 (c (n "bytes_parser") (v "0.1.5") (d (list (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0qcxzca5v17a01xbkwrgqrjhrwwp02x0vyfva3k40vayw7rlcyp3")))

