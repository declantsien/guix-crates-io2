(define-module (crates-io by te bytestr) #:use-module (crates-io))

(define-public crate-bytestr-0.1.0 (c (n "bytestr") (v "0.1.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (o #t) (d #t) (k 0)))) (h "1vlirjqabz6n66vis3f202900bpxyy5jsfcpbab4v7ncv0h9xarj") (s 2) (e (quote (("serde" "dep:serde"))))))

