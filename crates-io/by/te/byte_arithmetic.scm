(define-module (crates-io by te byte_arithmetic) #:use-module (crates-io))

(define-public crate-byte_arithmetic-0.2.0 (c (n "byte_arithmetic") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "1zd550jqpxi83bq5xw7nacv2zncl71x5rvqj3k4rc7ypkq0v9n1x")))

(define-public crate-byte_arithmetic-0.2.1 (c (n "byte_arithmetic") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "1x2czx3ag9q61s5phc7yqrllmw6kcxc1n7rp8zx968pbc94cd1ga")))

(define-public crate-byte_arithmetic-0.2.2 (c (n "byte_arithmetic") (v "0.2.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cp4mg1q813vaxfxw3x0w19qiz365ibhz1hm6wf4f058gyipbyzr")))

(define-public crate-byte_arithmetic-0.3.0 (c (n "byte_arithmetic") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "000shsgivlddpfvmclxd17l3zzvk5csdqv87k2h50mqp65900zyl")))

(define-public crate-byte_arithmetic-0.3.1 (c (n "byte_arithmetic") (v "0.3.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "08vzimwxqgz9p92ln9qcl87wgp8wric1nm652prqx8d4zjilr62n")))

