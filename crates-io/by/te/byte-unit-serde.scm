(define-module (crates-io by te byte-unit-serde) #:use-module (crates-io))

(define-public crate-byte-unit-serde-0.1.0 (c (n "byte-unit-serde") (v "0.1.0") (d (list (d (n "byte-unit") (r "^5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "include-utils") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0cccgk3ifx9bcd60pfzl86mv3kk1l6prdnlkzhhakpsq865xjmzx") (f (quote (("default"))))))

