(define-module (crates-io by te bytecheck_derive) #:use-module (crates-io))

(define-public crate-bytecheck_derive-0.1.0 (c (n "bytecheck_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dfv8vr3mspfd1jbsinpigx6f11f481zpj185a7awbmd3gd6z5zd")))

(define-public crate-bytecheck_derive-0.2.0 (c (n "bytecheck_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15m0v1qjhchlzim96773xh90a4nbzbqa5ys06wmnh1bhbfayhk5d")))

(define-public crate-bytecheck_derive-0.2.1 (c (n "bytecheck_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hazyzl5mkzixagjaq5lyx2nfqh3gmpksjnk9vrfhd0p3hjvlsda")))

(define-public crate-bytecheck_derive-0.3.0 (c (n "bytecheck_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wai31k8qa116z7f9bh084cg9s6hy58hpfv4gs5zran8pbwfks86")))

(define-public crate-bytecheck_derive-0.4.0 (c (n "bytecheck_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dsbici7v0vsmzy665812id4rrx4v7fp0n6qymbk3jd10mxakyjx")))

(define-public crate-bytecheck_derive-0.4.1 (c (n "bytecheck_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jl4935rrv2yr12j5l4wk5r5qc7hlqza7qpdc6fr0wc039iwp71a")))

(define-public crate-bytecheck_derive-0.5.0 (c (n "bytecheck_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0snagypszv074i7kpg44f43sfy5fvkymqmc40yxibbgbyhhyc5hs")))

(define-public crate-bytecheck_derive-0.5.2 (c (n "bytecheck_derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0grbkwwv5j91n7zrimci6fh4k79flxga3mkjg50jysnyraizi088")))

(define-public crate-bytecheck_derive-0.6.0 (c (n "bytecheck_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z5jc7z1b2fmf84lwzmbjwfigxds55p03p3hmzqmhwa6240ig8qp") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6.1 (c (n "bytecheck_derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vxcx5cml4mgfkim0xrbgvp1clkhrn1z89qiph1yk6d3qyhjmjlf") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6.2 (c (n "bytecheck_derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0w125mih2lz1gdkd3prvf298a0vqgjn539rr6z72wlhpbz5wb50k") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6.3 (c (n "bytecheck_derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lv33k7bw70dx9pxsamscjl621nacx08li1fl8w8c4yzlha5hy7v") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6.4 (c (n "bytecheck_derive") (v "0.6.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zl7v5s23xx5bsv02n13lnjinr3k76q4kvznlc6fffv6wdfnvjg7") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6.5 (c (n "bytecheck_derive") (v "0.6.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02p1443m4i3wp20rkw51b8nsijzj2vn9yp27mdyq97ywdzrdzd63") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6.7 (c (n "bytecheck_derive") (v "0.6.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0006ycn72g788hzfy5zd7gl3hxhqi5dj0q7plrjydnimq693nasa") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6.8 (c (n "bytecheck_derive") (v "0.6.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1w3ln5n9gw55l18dm2lbvibgyvrj0bh7m8nsfhhr2pch5a37rcgd") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6.9 (c (n "bytecheck_derive") (v "0.6.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gxr63mi91rrjzfzcb8pfwsnarp9i2w1n168nc05aq4fx7mpdr8k") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.7.0 (c (n "bytecheck_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nhymi1ln6fpwi2vfm3pgzhabfnr3f3xzf8s43f0gg45f1c8ra7d") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6.10 (c (n "bytecheck_derive") (v "0.6.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r9dlkx5w1p8d5gif2yvn6bz1856yij2fxi4wakq2vxl7ia2a4p3") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6.11 (c (n "bytecheck_derive") (v "0.6.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qdgfqx23gbjp5scbc8fhqc5kd014bpxn8hc9i9ssd8r4rplrv57") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.8.0-pre1 (c (n "bytecheck_derive") (v "0.8.0-pre1") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vyw2ghbzsfnbbf6ahy43zm5qq1c3swpk7glps5xrm17nx1dwp22")))

(define-public crate-bytecheck_derive-0.8.0-pre2 (c (n "bytecheck_derive") (v "0.8.0-pre2") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "118aa1g0li1rmvcw5bag1lx6a05wlsp6gsdvnk1hzazbhwmc5rrd")))

(define-public crate-bytecheck_derive-0.8.0-pre3 (c (n "bytecheck_derive") (v "0.8.0-pre3") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v0aj2k08n8wycjr89xmii0qwf3vm6if5ylhb6g38kgbfdqrdsyd")))

(define-public crate-bytecheck_derive-0.8.0-pre4 (c (n "bytecheck_derive") (v "0.8.0-pre4") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w95ad14a1v08jbrbjsbdh9mvaarsbchhn0z64raxqk28kcmclk9")))

(define-public crate-bytecheck_derive-0.8.0-pre5 (c (n "bytecheck_derive") (v "0.8.0-pre5") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "040rjxnxg68f7h1v4jkf14cmscaarwyi1ism3z7p72pn9vb254dm")))

(define-public crate-bytecheck_derive-0.8.0-pre6 (c (n "bytecheck_derive") (v "0.8.0-pre6") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lbwq12anp33111a4hlv30sfq4fpb1cr6k24ismcgqj7ypljnha1")))

(define-public crate-bytecheck_derive-0.6.12 (c (n "bytecheck_derive") (v "0.6.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ng6230brd0hvqpbgcx83inn74mdv3abwn95x515bndwkz90dd1x") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.8.0-alpha.7 (c (n "bytecheck_derive") (v "0.8.0-alpha.7") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01mn2p89102w8d5bamwdxnkxlasg8kk0dni4rb7xmc21q47a4q90")))

(define-public crate-bytecheck_derive-0.8.0-alpha.9 (c (n "bytecheck_derive") (v "0.8.0-alpha.9") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0008a62qgpab3c0ci904qpiv60vijh2z4yjd5j4fhim83na2w3kl")))

