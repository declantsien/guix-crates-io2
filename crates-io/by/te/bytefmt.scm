(define-module (crates-io by te bytefmt) #:use-module (crates-io))

(define-public crate-bytefmt-0.1.1 (c (n "bytefmt") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0b4h7jqjz68pbmkdsvc5fsx4lfisb164q7hnvz0s06ks5jn78cch")))

(define-public crate-bytefmt-0.1.2 (c (n "bytefmt") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1xhb6ilcs5jf4n8ycfb9bbws9z2wrhj32lqhyzan3d8y2jhdh8cy")))

(define-public crate-bytefmt-0.1.3 (c (n "bytefmt") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rnfd1g1900dad4951xns7ap12ay31m8b3016diqj5ds0y0hpx6b")))

(define-public crate-bytefmt-0.1.4 (c (n "bytefmt") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ycnsffg74mz3l3skjs3npspfjmlcbkx09xnhlbc3p7cb15cx888")))

(define-public crate-bytefmt-0.1.5 (c (n "bytefmt") (v "0.1.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1sxivqk9saxm6pjmvjkvzmf38c5n0g0q4nisqhw1akzczyhmqkp4")))

(define-public crate-bytefmt-0.1.6 (c (n "bytefmt") (v "0.1.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "03r7h7nmnqz10lvflixfrgbhwf4lgb7y8i8qs3rm0sadyjrds37f")))

(define-public crate-bytefmt-0.1.7 (c (n "bytefmt") (v "0.1.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0x1xxlxfbmqhbqmcyjjvifsr46cbw02izlbwvba4f752b7q1l2sr")))

