(define-module (crates-io by te byteorder) #:use-module (crates-io))

(define-public crate-byteorder-0.1.0 (c (n "byteorder") (v "0.1.0") (h "0k2sanwcry5n0zd835vr953xjl3amylsw69bycj3m5yyli4xj7vh")))

(define-public crate-byteorder-0.1.1 (c (n "byteorder") (v "0.1.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0v3051cq4ng551032q2krdwnnvqvkhpdh838j4xf0lk113z7qywv")))

(define-public crate-byteorder-0.2.0 (c (n "byteorder") (v "0.2.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0v29xk01206wdyixss8ki0j5mav2vc93bv97j4q7vjb2snj0kb22")))

(define-public crate-byteorder-0.2.1 (c (n "byteorder") (v "0.2.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1x902gcb4ik5by10h1zd6i11qslssmd5xk3j319dmknbgqmak02n")))

(define-public crate-byteorder-0.2.2 (c (n "byteorder") (v "0.2.2") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1dhn0g6g6l002lywsrdbcc31in0zcbflgs5nwnqdbf9zjgwavsbs")))

(define-public crate-byteorder-0.2.3 (c (n "byteorder") (v "0.2.3") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1zq2n5by9vp75jvcpb7m9ag826jar178j0d4b927pj4r05jxw0zy")))

(define-public crate-byteorder-0.2.4 (c (n "byteorder") (v "0.2.4") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0byyqdirgxm974nxlgm9xyyjkchzc1ra0vzf4gcd4fad8zn2325w")))

(define-public crate-byteorder-0.2.5 (c (n "byteorder") (v "0.2.5") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "14rc5n1bky6vfnw5asw2nz78bp9lx8k3l6khvz8vvd0gddb95f23")))

(define-public crate-byteorder-0.2.6 (c (n "byteorder") (v "0.2.6") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1ccsbcvbangiiaz0hpgziwqm0z665vpsr8pyqg9ywv8ias5416ly")))

(define-public crate-byteorder-0.2.7 (c (n "byteorder") (v "0.2.7") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0zv58hcsnqswwp9aj4bnv4kjap0wjq2b3r9a5rb7lcvfl4szjwdi")))

(define-public crate-byteorder-0.2.8 (c (n "byteorder") (v "0.2.8") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "10yk7514dip2848kjf1z735m51799w4pk92z8gi9pdj56dlk88s7")))

(define-public crate-byteorder-0.2.9 (c (n "byteorder") (v "0.2.9") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1aksgvjfss6q6vxnjb90ljhr04hnbp7rjxay7qq5l92lhz0q0ygb")))

(define-public crate-byteorder-0.2.10 (c (n "byteorder") (v "0.2.10") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1bbvchy4fcsc83pjjqvghhqilz1i12amgg6y4arl4kb7d4maj9k4")))

(define-public crate-byteorder-0.2.11 (c (n "byteorder") (v "0.2.11") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0jvzfki27vz7z7jab8xlvbm5aync4gnfl8mddaq7h92vkvvymabf")))

(define-public crate-byteorder-0.2.12 (c (n "byteorder") (v "0.2.12") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0v7yxnpz59gsc961qvcpjjc0qjdbr049m2n4hd1ng1awfps64fcs")))

(define-public crate-byteorder-0.2.13 (c (n "byteorder") (v "0.2.13") (d (list (d (n "bswap") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "010cqb1k2bi2d85zxnrf602kbmdvnxvsybznrlmiwh24saxsiqyh")))

(define-public crate-byteorder-0.2.14 (c (n "byteorder") (v "0.2.14") (d (list (d (n "bswap") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0bdqk22jphygy8li5f5yv8w82v4bknrqaygiw2d485i5jvc53wm2")))

(define-public crate-byteorder-0.3.0 (c (n "byteorder") (v "0.3.0") (d (list (d (n "bswap") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "03msg61k11ma8jp2nv38xzn4ixw0inxwc0pbf9zxqlh1i5rjbik3")))

(define-public crate-byteorder-0.3.1 (c (n "byteorder") (v "0.3.1") (d (list (d (n "bswap") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "058zfxl7anidi743bqx2lay1va7crhjvcqiklg5k1p4h2fygjq96")))

(define-public crate-byteorder-0.3.2 (c (n "byteorder") (v "0.3.2") (d (list (d (n "bswap") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "082z2pb45nj6z88y7yq0zfzyzsfrak4yham3ypawfjwfizg0zvxf")))

(define-public crate-byteorder-0.3.3 (c (n "byteorder") (v "0.3.3") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "08znk64ycqj8iz5y18g14nj3ynh61sxxr29hzx7q6pfxw98q0yxg")))

(define-public crate-byteorder-0.3.4 (c (n "byteorder") (v "0.3.4") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "01wz47lv2p3slmw4i0dd7m6866f03rad7jhbb3f3nlpbsaqmx1rw")))

(define-public crate-byteorder-0.3.5 (c (n "byteorder") (v "0.3.5") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1idsvlp1h2iyqrnhi1zb9x5mm0ql6lq2a6z6nipp5j03jf4mrhh2")))

(define-public crate-byteorder-0.3.6 (c (n "byteorder") (v "0.3.6") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1s9zvr5bpkvwjv39ai0ax8vs0lmnkzhfsb38yg8aqn696npv6hfs")))

(define-public crate-byteorder-0.3.7 (c (n "byteorder") (v "0.3.7") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "09p8qs1npw5gcz1skwr88dgybs9g4l7pm35pn19sjg56cr41nxdw")))

(define-public crate-byteorder-0.3.8 (c (n "byteorder") (v "0.3.8") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1v2m74m5xvgqw4ms3fjllj7p8nw7mdn5wilah7gayqx1gijw21gz")))

(define-public crate-byteorder-0.3.9 (c (n "byteorder") (v "0.3.9") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0by6lb5d8z033f07v0nrnhyd876gdvihjrfkdk8g5p3cfwi8j2kf")))

(define-public crate-byteorder-0.3.10 (c (n "byteorder") (v "0.3.10") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1jg3gpnycbav11fyshppczpf4arcw426g9zgara2ic4l3zdn7s86")))

(define-public crate-byteorder-0.3.11 (c (n "byteorder") (v "0.3.11") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1m9b5nv172w0xdi10kv26whjfr5x6mv8jhqkbqva6xidvxbis1jr")))

(define-public crate-byteorder-0.3.12 (c (n "byteorder") (v "0.3.12") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "07fizq4syb0v3qdxcxy9a1lwjf7h9k5hn6c4rah05fsxb2l3xn9w")))

(define-public crate-byteorder-0.3.13 (c (n "byteorder") (v "0.3.13") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0xd1vzp1yzw9f9qpm7w3mp9kqxdxwrwzqs4d620n6m4g194smci9")))

(define-public crate-byteorder-0.4.0 (c (n "byteorder") (v "0.4.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0gn5rdk6igl872c5ack9s5asl4ly1lx20wky1sfz3m01ck8mg636") (f (quote (("no-std"))))))

(define-public crate-byteorder-0.4.1 (c (n "byteorder") (v "0.4.1") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1rkiqb6c6psk8mqs6wqhzczvvwp5vq0kf8wzhdafqk2ycrsyvs48") (f (quote (("no-std"))))))

(define-public crate-byteorder-0.4.2 (c (n "byteorder") (v "0.4.2") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "010k8mb3fi4jgbj4nxzggyi7zg2jvm7aqirdyf5c1348h4cb9j4n") (f (quote (("no-std"))))))

(define-public crate-byteorder-0.5.0 (c (n "byteorder") (v "0.5.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0k926lglv8bwwb7zc8c0xx0mkvfdzdy7q19g39n7irkjwi0ipcdv") (f (quote (("no-std"))))))

(define-public crate-byteorder-0.5.1 (c (n "byteorder") (v "0.5.1") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1qnznh7smxwfp217gwfrfk0bamxrac02ahzyqc05nzljalbkmjp4") (f (quote (("no-std"))))))

(define-public crate-byteorder-0.5.2 (c (n "byteorder") (v "0.5.2") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0vn2m7kcx8lq3ga37rqgb49qgzmm7w3b1mak7acq799lnarx0s1y") (f (quote (("std") ("default" "std"))))))

(define-public crate-byteorder-0.5.3 (c (n "byteorder") (v "0.5.3") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0ma8pkyz1jbglr29m1yzlc9ghmv6672nvsrn7zd0yn5jqs60xh8g") (f (quote (("std") ("default" "std"))))))

(define-public crate-byteorder-1.0.0 (c (n "byteorder") (v "1.0.0") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1j0qvrvpmk01v5qkmp5l7gmjvlpxxygivm1w074qb63bxsq7f2f4") (f (quote (("std") ("default" "std"))))))

(define-public crate-byteorder-1.1.0 (c (n "byteorder") (v "1.1.0") (d (list (d (n "quickcheck") (r "^0.4") (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0g9d0cs8czlyf8cdanffilp2342vnrzzxbyf6ab0jpbgfa5p70gz") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.2.0 (c (n "byteorder") (v "1.2.0") (d (list (d (n "quickcheck") (r "^0.4") (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0pxqqzn4wi67d2ydwbkjqd2i3l3fspc3ldvc3fy9w8a0i78bc161") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.2.1 (c (n "byteorder") (v "1.2.1") (d (list (d (n "quickcheck") (r "^0.4") (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "08qdzm6y639swc9crvkav59cp46lmfj84rlsbvcakb9zwyvhaa35") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.2.2 (c (n "byteorder") (v "1.2.2") (d (list (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "11rwmf4rifak70hb0rb3344rxg4xga05h7c0r6chpbg3gvzbvdbk") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.2.3 (c (n "byteorder") (v "1.2.3") (d (list (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1sfj7w3gzdjlbj26lxmqdpf8ri5jlhzxn33n9wp0lss4x43bkh3l") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.2.4 (c (n "byteorder") (v "1.2.4") (d (list (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1yd4ix7rkr6djqs285an6lvpf0dgmjh05iaqra7gxfb2xh4wb2c3") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.2.5 (c (n "byteorder") (v "1.2.5") (d (list (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1i8zsp0kpiy3fvr3f75l1kjph1bjv94cxc8k7rd1syaibzqnzmmn") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.2.6 (c (n "byteorder") (v "1.2.6") (d (list (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "109pz3i8fpbfg1h8zn8izh02hfi71pwqkyqwd5w2wzfxb1c2qjch") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.2.7 (c (n "byteorder") (v "1.2.7") (d (list (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0pgpxnfcsg1s7chdkpbigzl304yc2rbhzar2r6npnh957br8vy4l") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.3.0 (c (n "byteorder") (v "1.3.0") (d (list (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1llv9ywnbciizlk814qfqs2w3nmmlssi5z9889rx50m3q3ab1w30") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.3.1 (c (n "byteorder") (v "1.3.1") (d (list (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1syvclxqjwf6qfq98y3fiy82msjp7q8wh7qkvf9b5pkw585b26d0") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.3.2 (c (n "byteorder") (v "1.3.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1xbwjlmq2ziqjmjvkqxdx1yh136xxhilxd40bky1w4d7hn4xvhx7") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.3.3 (c (n "byteorder") (v "1.3.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0m0dl2z8m63rg803n33qv94ga8n29lxqll155b3n1s93hf7v3237") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.3.4 (c (n "byteorder") (v "1.3.4") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1pkjfhgjnq898g1d38ygcfi0msg3m6756cwv0sgysj1d26p8mi08") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.4.0 (c (n "byteorder") (v "1.4.0") (d (list (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1nrh7y7g8wbbnhgsc16lyx48ssdgahfgfd0dbvjbw760f8i43spz") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.4.1 (c (n "byteorder") (v "1.4.1") (d (list (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "08g8pjv0ywypcsmi4s2lv287qyi0cn798frdqiy8q7zdbpfjp2gl") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.4.2 (c (n "byteorder") (v "1.4.2") (d (list (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0srh0h0594jmsnbvm7n0g8xabhla8lwb3gn8s0fzd7d1snix2i5f") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.4.3 (c (n "byteorder") (v "1.4.3") (d (list (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0456lv9xi1a5bcm32arknf33ikv76p3fr9yzki4lb2897p2qkh8l") (f (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1.5.0 (c (n "byteorder") (v "1.5.0") (d (list (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0jzncxyf404mwqdbspihyzpkndfgda450l0893pz5xj685cg5l0z") (f (quote (("std") ("i128") ("default" "std")))) (r "1.60")))

