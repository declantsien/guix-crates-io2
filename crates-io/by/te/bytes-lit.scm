(define-module (crates-io by te bytes-lit) #:use-module (crates-io))

(define-public crate-bytes-lit-0.0.1 (c (n "bytes-lit") (v "0.0.1") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fn2qvlkayvpsfiaczri5xx0wxvvyz09p6sv379h2ap5ajgly3rb")))

(define-public crate-bytes-lit-0.0.2 (c (n "bytes-lit") (v "0.0.2") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jy3myhx86anxsqawg403mhhz5xi03g4sw57gs889ylq3ql1wrbm")))

(define-public crate-bytes-lit-0.0.3 (c (n "bytes-lit") (v "0.0.3") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xijpvjwp994hhda7jgb7kajdd61va088k0zm4llrn6imfw4kagk") (r "1.63")))

(define-public crate-bytes-lit-0.0.4 (c (n "bytes-lit") (v "0.0.4") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0l229dbms1d187c91vcf14w726z19yjzjxgsj25k8j8s2v51n60w") (r "1.63")))

(define-public crate-bytes-lit-0.0.5 (c (n "bytes-lit") (v "0.0.5") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p82m9q2g56vqwjg71fz7zmra32kn75spzimcgj7clhs44vvznha") (r "1.66")))

