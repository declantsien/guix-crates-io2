(define-module (crates-io by te bytekey2) #:use-module (crates-io))

(define-public crate-bytekey2-0.4.3 (c (n "bytekey2") (v "0.4.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 2)) (d (n "utf-8") (r "^0.7.5") (d #t) (k 0)))) (h "1n0kx9jqwvy8kpxs2ic01qandh575mb0sxcf8m1wqrnmlh3367if")))

(define-public crate-bytekey2-0.4.4 (c (n "bytekey2") (v "0.4.4") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 2)) (d (n "utf-8") (r "^0.7.5") (d #t) (k 0)))) (h "18jkxvxzz5xagz3aifissw5ba1h3fzrv8m4dai9syfs5zssg0mvd")))

(define-public crate-bytekey2-0.4.5 (c (n "bytekey2") (v "0.4.5") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 2)) (d (n "utf-8") (r "^0.7.5") (d #t) (k 0)))) (h "1rdx9w6z6zhlldyb4y1360q1lp2r2zgl6qfls417x18w1rn7dcsi")))

