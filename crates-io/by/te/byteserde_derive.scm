(define-module (crates-io by te byteserde_derive) #:use-module (crates-io))

(define-public crate-byteserde_derive-0.1.0 (c (n "byteserde_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1fbj28bpm8r91rvhh71lldc2159ca8vcjazldl972mxmq3jakq4v") (y #t) (r "1.69")))

(define-public crate-byteserde_derive-0.1.1 (c (n "byteserde_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "06y9hnyr85sj3rma1fa5yjf74bs88ii9a5yag1b5sirc3g45m12c") (y #t) (r "1.69")))

(define-public crate-byteserde_derive-0.2.0 (c (n "byteserde_derive") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1lg2zin8dzbyjz6x7vpjph3zakim5a0fg5s86qmlmp6kw5rhdlzc") (y #t) (r "1.69")))

(define-public crate-byteserde_derive-0.3.0 (c (n "byteserde_derive") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1hkkvs5hzvxxg73jn7s87hrxs8dwy6c1fxwpxd363j9m1zbsd6df") (y #t) (r "1.69")))

(define-public crate-byteserde_derive-0.4.0 (c (n "byteserde_derive") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "08zd14529iqh2b0gjzsbxxh10a8cbbifaskcqh165p3k1y90nnl8") (y #t) (r "1.69")))

(define-public crate-byteserde_derive-0.5.0 (c (n "byteserde_derive") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1569bmib5idkqwdhlsf6qdnml9vbb4whjpw89ahrprb4sk3f05fz") (y #t) (r "1.69")))

(define-public crate-byteserde_derive-0.6.0 (c (n "byteserde_derive") (v "0.6.0") (d (list (d (n "byteserde") (r "^0.6.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "02qqmr96is26ax9v3gds15a1q911l9z8cz553svn2mw18yhpfpfk") (y #t) (r "1.69")))

(define-public crate-byteserde_derive-0.6.1 (c (n "byteserde_derive") (v "0.6.1") (d (list (d (n "byteserde") (r "^0.6.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "08n9vq1kmd25a7zq6y4hjgi1sb14q9dr3d4hmlgkv13qcaf4xvci") (y #t) (r "1.69")))

(define-public crate-byteserde_derive-0.6.2 (c (n "byteserde_derive") (v "0.6.2") (d (list (d (n "byteserde") (r "^0.6.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "13c63168srbx66j64pfb3r1xwirshnc15419dc77if4393ssl5l9") (r "1.69")))

