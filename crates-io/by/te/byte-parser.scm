(define-module (crates-io by te byte-parser) #:use-module (crates-io))

(define-public crate-byte-parser-0.2.0 (c (n "byte-parser") (v "0.2.0") (h "0kyqdnhp4ql1nvb0wj26k8l5njdm84s6k470dl9f1shdn98q270s")))

(define-public crate-byte-parser-0.2.1 (c (n "byte-parser") (v "0.2.1") (h "01bbk8pnc9lbs8ny19qvjzigddwwmxqxfmirgl0ymdsia8gwviiq") (f (quote (("unstable-parse-iter"))))))

(define-public crate-byte-parser-0.2.2 (c (n "byte-parser") (v "0.2.2") (h "1r20h92shfgg52w18s6zkz4yyfsxm87439md9kjq2fc8rr0irynq") (f (quote (("unstable-parse-iter"))))))

(define-public crate-byte-parser-0.2.3 (c (n "byte-parser") (v "0.2.3") (h "03vmlvlrqa1xqlyva6jgv7cmv303v7x49zimj2c9c8mjgla8ncmm") (f (quote (("unstable-parse-iter")))) (r "1.56")))

