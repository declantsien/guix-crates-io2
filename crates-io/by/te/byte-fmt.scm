(define-module (crates-io by te byte-fmt) #:use-module (crates-io))

(define-public crate-byte-fmt-0.1.0 (c (n "byte-fmt") (v "0.1.0") (d (list (d (n "abe") (r "^0.1.0") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "0dwyg2lv1wxv9m7qn4lrnqk1hacbvfjqnzkabl47rbkv2c1jikd9")))

(define-public crate-byte-fmt-0.3.0-rc1 (c (n "byte-fmt") (v "0.3.0-rc1") (d (list (d (n "abe") (r "^0.3.0-rc1") (d #t) (k 0)) (d (n "base64") (r "=0.21") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ruint") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.160") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.6") (d #t) (k 0)) (d (n "tracing") (r "=0.1.37") (f (quote ("release_max_level_info"))) (d #t) (k 0)))) (h "0bgsnnfsqdkkpvn4xizwnwbp5w46b3pq5ffw97q5f5lsx8hxmyhr")))

