(define-module (crates-io by te bytes-stream) #:use-module (crates-io))

(define-public crate-bytes-stream-0.0.1 (c (n "bytes-stream") (v "0.0.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "05ibb6vd2fbdnl3062sw2lwmsm58d7h08wkhk84kjxipah6wsl9j")))

(define-public crate-bytes-stream-0.0.2 (c (n "bytes-stream") (v "0.0.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "17x1nd7s5b8i3wwi04hlr1iad3cx3g8swi6wzhcd4ifgqmns80j9")))

(define-public crate-bytes-stream-0.0.3 (c (n "bytes-stream") (v "0.0.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-test") (r "^0.3.28") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0fhzrx8zjsdj1naacjrbq2slna6m4rcjm1d4prln2hhbaamd4x3c")))

