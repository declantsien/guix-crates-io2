(define-module (crates-io by te byte_reader) #:use-module (crates-io))

(define-public crate-byte_reader-0.1.0 (c (n "byte_reader") (v "0.1.0") (h "0z55vyijxi7alnr1vcrwq1zxnhzkicbm7fkmm77qkxsakh9fkdf2")))

(define-public crate-byte_reader-0.1.1 (c (n "byte_reader") (v "0.1.1") (h "0ny16kr3ig9cdgrq9k8h5v69pmf2cc795xpv147l9vldzpvrx2yv") (f (quote (("location"))))))

(define-public crate-byte_reader-0.1.2 (c (n "byte_reader") (v "0.1.2") (h "0cwy4gbwrq0snnpihp5dzl5nncph1x7r5sarzsd01jns54ar7ayc") (f (quote (("location"))))))

(define-public crate-byte_reader-0.1.3 (c (n "byte_reader") (v "0.1.3") (h "01w86v5qj9jfn74xygy3shlbg7vp49qn7b5icy3n1wnrcp5lxw23") (f (quote (("location"))))))

(define-public crate-byte_reader-0.2.0 (c (n "byte_reader") (v "0.2.0") (h "0kwnm4kmkkl5aq9c4vw603x5dhmmrf2icbw689mancjh0dk8dss3") (f (quote (("location"))))))

(define-public crate-byte_reader-0.3.0 (c (n "byte_reader") (v "0.3.0") (h "1741mddgzcjh79h3kpvj6a4gd9hi0b2bwvsb4ahx844k2yj8l7pl") (f (quote (("location"))))))

(define-public crate-byte_reader-0.3.1 (c (n "byte_reader") (v "0.3.1") (h "00y5c6m5s4aj613gjfsyj47076a7p7dx3qdsn7bn98511sb1nj88") (f (quote (("location"))))))

(define-public crate-byte_reader-0.3.2 (c (n "byte_reader") (v "0.3.2") (h "1m7msmqjab0v0xnvyx9hb1fh9jm18ys5c27gh68bk384fpzypp8z") (f (quote (("location"))))))

(define-public crate-byte_reader-0.3.3 (c (n "byte_reader") (v "0.3.3") (h "1hr1l5xqd0ccdqmm0k375fvqgxzap3iy0lybj2qi8i1n85jaf7nm") (f (quote (("location"))))))

(define-public crate-byte_reader-0.3.4 (c (n "byte_reader") (v "0.3.4") (h "1cb02qp3n37ccvz73vywrdppjll2bsxhz3rsc92hhsqk37j1y16f") (f (quote (("location"))))))

(define-public crate-byte_reader-0.3.5 (c (n "byte_reader") (v "0.3.5") (h "1k1faqpbclcyl4188m8kj7p940cb6qd3cy1i8hcwjnmrgikg8blq") (f (quote (("location"))))))

(define-public crate-byte_reader-0.3.6 (c (n "byte_reader") (v "0.3.6") (h "0fp5y0zg8vkdyjp9dn6nr8iiwi4pxslf8rjd15inziq2xkanmch9") (f (quote (("location")))) (y #t)))

(define-public crate-byte_reader-0.4.0 (c (n "byte_reader") (v "0.4.0") (h "169c565dqfx07j7krcvrkbbdgd7ics1wgnrr362n8kknpgjhixrn") (f (quote (("location"))))))

(define-public crate-byte_reader-0.5.0 (c (n "byte_reader") (v "0.5.0") (h "11rvs85qqpwq7cfvm617y91imbq7i9nfjmlcw8paiwcfhkqmh0fh") (f (quote (("location"))))))

(define-public crate-byte_reader-0.5.1 (c (n "byte_reader") (v "0.5.1") (h "0mr89c1g59glw2z56ymms9lza8lcjh1f33ylyfz0li5g77nm4m1w") (f (quote (("location"))))))

(define-public crate-byte_reader-0.5.2 (c (n "byte_reader") (v "0.5.2") (h "1yl1zppygzvl3mpngkwf0s70as1i24js3viqsq18vzidd76kcnpa") (f (quote (("location"))))))

(define-public crate-byte_reader-1.0.0 (c (n "byte_reader") (v "1.0.0") (h "0vbyjfyyahkrc02af255jz1yykncyj4hl976wf57x1b012n0bkhy") (f (quote (("location"))))))

(define-public crate-byte_reader-1.1.0 (c (n "byte_reader") (v "1.1.0") (h "1d5gzb4nzssixxha0af86grxlkg00vpvbszzmbk5imynw24frqj7") (f (quote (("location"))))))

(define-public crate-byte_reader-1.1.1 (c (n "byte_reader") (v "1.1.1") (h "0rz36dnlggv27svgyd8avqm8q3gmzkirps1f6p34vlzg4dk9vvkp") (f (quote (("location"))))))

(define-public crate-byte_reader-1.1.2 (c (n "byte_reader") (v "1.1.2") (h "1na1vbjzdsq8wi1lmpds4aql2byw78jx7jvv2yvjm2fk1d0psjsh") (f (quote (("location"))))))

(define-public crate-byte_reader-1.2.0 (c (n "byte_reader") (v "1.2.0") (h "119jdjd6jyx80kl12qlgp6jnhixnq6zlns0ndk863jdfqcz1cssj") (f (quote (("text") ("location"))))))

(define-public crate-byte_reader-1.2.1 (c (n "byte_reader") (v "1.2.1") (h "05nlbsxjskgbw6flqd83wrvhbvyk5hdq3k6ilvnk6im4vn3jgg5d") (f (quote (("text") ("location"))))))

(define-public crate-byte_reader-1.2.2 (c (n "byte_reader") (v "1.2.2") (h "0wiv16g1zp555w4k44r7krxskd3gwdcraijlk3cgj95pn9iw6j2w") (f (quote (("text") ("location"))))))

(define-public crate-byte_reader-2.0.0 (c (n "byte_reader") (v "2.0.0") (h "01njrarf5xxzzbp7jhr4l7nz6wz34by9lzgwq3cfd6if0vy1hyh8") (f (quote (("text") ("location"))))))

(define-public crate-byte_reader-2.1.0 (c (n "byte_reader") (v "2.1.0") (h "02n4k7qh7wy456w58ymff61ckcqyi7d5yvc5vi9d9qq2272m2bdv") (f (quote (("text") ("location") ("detached"))))))

(define-public crate-byte_reader-3.0.0 (c (n "byte_reader") (v "3.0.0") (h "1pvnnp4qm211csxgmp41c9cbgnwq5kba5nbf4hqlhsay8psngb0g") (f (quote (("text") ("location"))))))

