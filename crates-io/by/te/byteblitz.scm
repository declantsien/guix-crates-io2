(define-module (crates-io by te byteblitz) #:use-module (crates-io))

(define-public crate-byteblitz-0.1.0 (c (n "byteblitz") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)))) (h "1r10bcdh290cj0i911fk749vj7p5pwp5cgi997a2kya2lvia3m36")))

(define-public crate-byteblitz-1.0.0 (c (n "byteblitz") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03xcbc8c2qjdb19j48dld2f76zbpmaz0n8p76qdmyzqc8681lf1g")))

