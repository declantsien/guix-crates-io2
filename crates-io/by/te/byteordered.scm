(define-module (crates-io by te byteordered) #:use-module (crates-io))

(define-public crate-byteordered-0.1.0 (c (n "byteordered") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1b1a0aj1gpiy6x8zijmixbymxh4ahjm5nm1vrfdb0vck9rcnpngl") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.2.0 (c (n "byteordered") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "0rcrk405iwz72nw666cydl3f2dbhfz5995d8wl0mz9wx04d90nsr") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.2.1 (c (n "byteordered") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1w4dpdns6w32qm4w0257mi0k60dcdy0ngjk300hzv8qn801y09wy") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.3.0 (c (n "byteordered") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "0sqjw84nl97c4dp7n7frzc5xhq5sqw6pkv68dndphygq8iz76vzg") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.3.1 (c (n "byteordered") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "0xxzwqhcmx72ybw9z6b7agcnmhwizx1923lpmy2f9xn99gg5297j") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.4.0 (c (n "byteordered") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "09sypx8mzdjplqh8anfl43jcvlc9hs40a8cm1h2gfcmsncignkam") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.4.1 (c (n "byteordered") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1ag91lvv01sl61zignxn8p0igkgkmaqfsrpnxg1zb1x0vhxza3x9") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.5.0 (c (n "byteordered") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0.0") (f (quote ("i128"))) (d #t) (k 0)))) (h "0dwvv1vld8j58r4m6g685lhcwl0n2pgbxpq7xzijd1a9mgl7ws1j")))

(define-public crate-byteordered-0.6.0 (c (n "byteordered") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.0.0") (f (quote ("i128"))) (d #t) (k 0)))) (h "1rl0cba0yv67h6chm1wvd25ywj64rcswhn8rp9541zzm4jacvwmv")))

