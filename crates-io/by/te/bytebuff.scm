(define-module (crates-io by te bytebuff) #:use-module (crates-io))

(define-public crate-bytebuff-0.1.0 (c (n "bytebuff") (v "0.1.0") (d (list (d (n "marshall_derive") (r "^0.1.0") (d #t) (k 0)))) (h "11nkhg9rhww6q8wsv0m3526ynvkxgk6zbp3rifw9py0jrcrwx5vi")))

(define-public crate-bytebuff-0.1.1 (c (n "bytebuff") (v "0.1.1") (d (list (d (n "marshall_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1grar9g60f7mws1kdvimw4sbwzxlwsx820qxi9hbsbard13hb0wg")))

