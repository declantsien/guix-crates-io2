(define-module (crates-io by te bytepack) #:use-module (crates-io))

(define-public crate-bytepack-0.1.0 (c (n "bytepack") (v "0.1.0") (h "1iayka1jm5bhq0dxs5x1r0cp9pbrmwzlh2fpkapklbn06v4r5k5d")))

(define-public crate-bytepack-0.1.1 (c (n "bytepack") (v "0.1.1") (h "1dsw5fcdlyccbb322a48iz17l1svjfvkgqk4f17x3g8xkxdfil6q")))

(define-public crate-bytepack-0.2.0 (c (n "bytepack") (v "0.2.0") (d (list (d (n "bytepack_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0581a1nm66gyfs84h2ikbm7ym7p2h1805x6lyc2m31gy2qdl5y5q")))

(define-public crate-bytepack-0.3.0 (c (n "bytepack") (v "0.3.0") (d (list (d (n "bytepack_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0qd1ic9p9mvgf1h4pcvlbrs6b6hbjpkf63i079gxdkfnpq05lg6l")))

(define-public crate-bytepack-0.3.1 (c (n "bytepack") (v "0.3.1") (d (list (d (n "bytepack_derive") (r "^0.2") (d #t) (k 0)))) (h "0fzj2jvk4b8z09v1mrbqa636pmdby3fmmwgs3i2fv6jmwqnch8h9")))

(define-public crate-bytepack-0.4.0 (c (n "bytepack") (v "0.4.0") (d (list (d (n "bytepack_derive") (r "^0.2") (d #t) (k 0)))) (h "1r4v4zpgqy15hbxvyv2bhplnz62znvjjzxhk7y13a0cimb5323j0")))

(define-public crate-bytepack-0.4.1 (c (n "bytepack") (v "0.4.1") (d (list (d (n "bytepack_derive") (r "^0.2") (d #t) (k 0)))) (h "1kfxzf7mkp5dcpiiarxdinv9qgm8mlppnimcv623z6qfslyw3lzx")))

