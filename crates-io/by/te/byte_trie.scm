(define-module (crates-io by te byte_trie) #:use-module (crates-io))

(define-public crate-byte_trie-0.1.0 (c (n "byte_trie") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0m7xlpi4pgsalyc3pvjhbg2a5qxsqcscdfy7vxhgghiw6f0j2fiq")))

(define-public crate-byte_trie-0.1.1 (c (n "byte_trie") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0py4571gyhqpzq8jsn0vp241aahfakirf33v98xss81gm5iakfry")))

(define-public crate-byte_trie-0.2.0 (c (n "byte_trie") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1mf58k802f96imz5ahz8g4xaz4yrvgkvzhl5q3fqf5pialvy341a")))

(define-public crate-byte_trie-0.3.0 (c (n "byte_trie") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1d7v1hps8a1bx9wnj86n9lnx2cr1gl3ppmalx83ackk9ikk1iqn6")))

