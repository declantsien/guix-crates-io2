(define-module (crates-io by te byte-slice-cast) #:use-module (crates-io))

(define-public crate-byte-slice-cast-0.1.0 (c (n "byte-slice-cast") (v "0.1.0") (h "1mw54i53rzzsffg67m0ybzvgkr36vka6g9ybdhhpjfvcz9xmx1js")))

(define-public crate-byte-slice-cast-0.2.0 (c (n "byte-slice-cast") (v "0.2.0") (h "0r5ycb6ap8w0vj0x0hszdl60vlna9rpbs8y1zf2hf9shgc8nqd18")))

(define-public crate-byte-slice-cast-0.3.0 (c (n "byte-slice-cast") (v "0.3.0") (h "1wgaigq4xzrpaaik1ja828zi4v2qn4md64c01yglzyz07z3fxmil")))

(define-public crate-byte-slice-cast-0.3.1 (c (n "byte-slice-cast") (v "0.3.1") (h "0xfmpyx3i9r1fgqayhz8dy8syvra0wzhz2wimzgjd05d4jw7kjxn")))

(define-public crate-byte-slice-cast-0.3.2 (c (n "byte-slice-cast") (v "0.3.2") (h "0xrykhvimjy7nz4s843xbfvzbzsrxihl818d9a6iviwf28cbzg3w") (f (quote (("std") ("default" "std"))))))

(define-public crate-byte-slice-cast-0.3.3 (c (n "byte-slice-cast") (v "0.3.3") (h "0vvx2jl37kndrv8chywlgf8arkp3cnw6arcd363ynakwg5kwi3nr") (f (quote (("std") ("default" "std"))))))

(define-public crate-byte-slice-cast-0.3.4 (c (n "byte-slice-cast") (v "0.3.4") (h "1aajgab19dmy7gspfr441axx60v9mmg6s09f01qa3phy5hxry87n") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-byte-slice-cast-0.3.5 (c (n "byte-slice-cast") (v "0.3.5") (h "1lwkgp1ahziy0kc6hrczvgxkgbb4qsprak8x7kik7wfbdf8f79dh") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-byte-slice-cast-1.0.0 (c (n "byte-slice-cast") (v "1.0.0") (h "10dw7dliqn9bbqwp31f3nma34gvp7mj66m8ji7sm93580i5bzhb5") (f (quote (("std") ("default" "std"))))))

(define-public crate-byte-slice-cast-1.1.0 (c (n "byte-slice-cast") (v "1.1.0") (h "1rkji3p0cp66yy0r2g8hv4pryx9j60h1dnnxl2s52dlqdbbrc1ya") (f (quote (("std") ("default" "std"))))))

(define-public crate-byte-slice-cast-1.2.0 (c (n "byte-slice-cast") (v "1.2.0") (h "162618ai9pnsim49lkjpq2yi2b3wssclvqxwwycw8xrbb58wfc0x") (f (quote (("std") ("default" "std"))))))

(define-public crate-byte-slice-cast-1.2.1 (c (n "byte-slice-cast") (v "1.2.1") (h "0zh65170havnnqpd912fgbbplm6fm0dc0v7wcgaf35b02v8gvic7") (f (quote (("std") ("default" "std"))))))

(define-public crate-byte-slice-cast-1.2.2 (c (n "byte-slice-cast") (v "1.2.2") (h "033vv1qddzsj9yfsam4abj55rp60digngcr9a8wgv9pccf5rzb63") (f (quote (("std") ("default" "std"))))))

