(define-module (crates-io by te byte_struct) #:use-module (crates-io))

(define-public crate-byte_struct-0.1.0 (c (n "byte_struct") (v "0.1.0") (d (list (d (n "byte_struct_derive") (r "^0.1.0") (d #t) (k 0)))) (h "117rnhiscp7vhg6xzy4733ramzz5pp1j6pz1617admih93hw4wbd")))

(define-public crate-byte_struct-0.2.0 (c (n "byte_struct") (v "0.2.0") (d (list (d (n "byte_struct_derive") (r "^0.2.0") (d #t) (k 0)))) (h "039kdfdssn1dxwnpczgg7d7sazplhncxnwrzcphpiljax7kfss3k")))

(define-public crate-byte_struct-0.3.0 (c (n "byte_struct") (v "0.3.0") (d (list (d (n "byte_struct_derive") (r "^0.2.0") (d #t) (k 0)))) (h "0hdgkp6q9qpja53qy4cncbw2nbjf4bz7r9kh7r5n88vdl7pw28bz")))

(define-public crate-byte_struct-0.4.0 (c (n "byte_struct") (v "0.4.0") (d (list (d (n "byte_struct_derive") (r "^0.4.0") (d #t) (k 0)))) (h "0m1kjlckvpnxhw83d9x9ks2d1ndgvqa6wy2fal2jhhiyrxjv1b22")))

(define-public crate-byte_struct-0.4.1 (c (n "byte_struct") (v "0.4.1") (d (list (d (n "byte_struct_derive") (r "^0.4.0") (d #t) (k 0)))) (h "1fd38h9rp8yb486dkhfn9kcnmxm7sxjbpgczx3xi1vfcrz1h0dci")))

(define-public crate-byte_struct-0.4.2 (c (n "byte_struct") (v "0.4.2") (d (list (d (n "byte_struct_derive") (r "^0.4.2") (d #t) (k 0)))) (h "03d7aabvmj7fj3rprrs3mza2n5vm5p3ny9ihcv8vbna91gk8a751")))

(define-public crate-byte_struct-0.5.0 (c (n "byte_struct") (v "0.5.0") (d (list (d (n "byte_struct_derive") (r "^0.4.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "07lry1kjvkif1pq0kc7i9mfakpmzb5vw5aasdy84mzdkqik6nx93")))

(define-public crate-byte_struct-0.6.0 (c (n "byte_struct") (v "0.6.0") (d (list (d (n "byte_struct_derive") (r "^0.4.2") (d #t) (k 0)) (d (n "generic-array") (r ">= 0.11") (d #t) (k 0)))) (h "0kjn2wm2647wsc119nwaw319ilyz37jrvwshp5130vad88bjxpiv")))

(define-public crate-byte_struct-0.6.1 (c (n "byte_struct") (v "0.6.1") (d (list (d (n "byte_struct_derive") (r "^0.4.2") (d #t) (k 0)) (d (n "generic-array") (r ">=0.11") (d #t) (k 0)))) (h "0wp76z3skhikkav4ib27yvr116n7skgqcc7xswx75bjqkyazzp1k")))

(define-public crate-byte_struct-0.7.0 (c (n "byte_struct") (v "0.7.0") (d (list (d (n "array-init") (r "^2") (d #t) (k 0)) (d (n "byte_struct_derive") (r "^0.4.2") (d #t) (k 0)))) (h "1vqiiggiyaz86my7mn36qsrxc7m3hbr2amrzhpy422qfmla9kmh8")))

(define-public crate-byte_struct-0.7.1 (c (n "byte_struct") (v "0.7.1") (d (list (d (n "array-init") (r "^2") (d #t) (k 0)) (d (n "byte_struct_derive") (r "^0.4.2") (d #t) (k 0)))) (h "0vx7g6mn3z08ffwn1ypz5wikqmks0c1ancdbf8mvyzmbgjdxxac7")))

(define-public crate-byte_struct-0.8.0 (c (n "byte_struct") (v "0.8.0") (d (list (d (n "array-init") (r "^2") (d #t) (k 0)) (d (n "byte_struct_derive") (r "^0.4.2") (d #t) (k 0)))) (h "16wgnq8x97vc0cqry636c9glvyfid3k5qrb8q3cl60s915i1dgay")))

(define-public crate-byte_struct-0.9.0 (c (n "byte_struct") (v "0.9.0") (d (list (d (n "byte_struct_derive") (r "^0.9.0") (d #t) (k 0)))) (h "1qk18dyvzb55981z4kr5y0irhjqw9rqv2cdpccf8h3fdz55n0rhs")))

