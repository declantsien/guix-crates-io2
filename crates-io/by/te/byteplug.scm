(define-module (crates-io by te byteplug) #:use-module (crates-io))

(define-public crate-byteplug-0.0.1 (c (n "byteplug") (v "0.0.1") (d (list (d (n "gl_generator") (r "^0.5.0") (d #t) (k 1)) (d (n "glutin") (r "^0.24") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "0y103ijnq7qyy1w35vwp782j52ls6jgfqrsfqsnq3s6w9r6xikdq")))

