(define-module (crates-io by te bytes-cast) #:use-module (crates-io))

(define-public crate-bytes-cast-0.1.0 (c (n "bytes-cast") (v "0.1.0") (d (list (d (n "bytes-cast-derive") (r "^0.1") (d #t) (k 0)))) (h "0lna2qz1c5qkc13193wai4x61sdkdi4pis1i8cm2ijbv1hqbm5ii")))

(define-public crate-bytes-cast-0.2.0 (c (n "bytes-cast") (v "0.2.0") (d (list (d (n "bytes-cast-derive") (r "^0.1") (d #t) (k 0)))) (h "0ln4yah8npnbcmd5chipnv4m5sl2dx5jg9zxrkkqgsfb9sd4yhqd")))

(define-public crate-bytes-cast-0.3.0 (c (n "bytes-cast") (v "0.3.0") (d (list (d (n "bytes-cast-derive") (r "^0.2") (d #t) (k 0)))) (h "1s2jnfmnn0mbw1k42pvi9pzwbkha647964lywfh3qw6pj4xyj3d2")))

