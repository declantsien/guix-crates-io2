(define-module (crates-io by te byteorder-pod) #:use-module (crates-io))

(define-public crate-byteorder-pod-0.0.1 (c (n "byteorder-pod") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "pod") (r "^0.4.0") (d #t) (k 0)) (d (n "uninitialized") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "1pz4cgglj3f60n7gb1s4n6y8sicdycmk5n85yv31f7mkynsk6a83") (f (quote (("default" "uninitialized"))))))

(define-public crate-byteorder-pod-0.1.0 (c (n "byteorder-pod") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "pod") (r "^0.5.0") (d #t) (k 0)) (d (n "uninitialized") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "03zwir0axiv1vsphlvjlhpm8lj6g0wfiaqbbrqi5jfg7607nayfi") (f (quote (("default" "uninitialized"))))))

