(define-module (crates-io by te bytestream_io) #:use-module (crates-io))

(define-public crate-bytestream_io-0.1.0 (c (n "bytestream_io") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "coc-rs") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "068ibf8j290rzrszv4fxlm54pkqjmnxyy5ar6lpkhp0gv3b8fy4m")))

(define-public crate-bytestream_io-0.1.1 (c (n "bytestream_io") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "coc-rs") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "1s28cn9w1fjyvbfb9zm1qbi6pi0iq4v949i67swri78x193y15cb")))

(define-public crate-bytestream_io-0.1.2 (c (n "bytestream_io") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "coc-rs") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "0slp93s428jnf50w3v7hnsp0dn3f87yq273fah6vs16i5zgvvn0w")))

(define-public crate-bytestream_io-0.1.4 (c (n "bytestream_io") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "coc-rs") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "1brzpczvl7gb05af1lsbjk85pxzqyz35mgyq3mjld6vcvvy2xzgp")))

(define-public crate-bytestream_io-0.1.5 (c (n "bytestream_io") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "coc-rs") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "1ak7pfc903cmysm29jivn6ard1vjysbcg4gr7d19jnm0lxvbcvsy")))

(define-public crate-bytestream_io-0.1.6 (c (n "bytestream_io") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "coc-rs") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "1xs9fl3xxkwakrbhjyh1i52gk37qmhp9nnnk58c1x5zy3z8q0ssz")))

(define-public crate-bytestream_io-0.1.7 (c (n "bytestream_io") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "logic-long") (r "^0.2.0") (d #t) (k 0)))) (h "12cyp3k3rnnh8p8hvcdwnq6mrl65a45xyvhn270y31khdll99x4q")))

