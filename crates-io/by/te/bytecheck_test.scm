(define-module (crates-io by te bytecheck_test) #:use-module (crates-io))

(define-public crate-bytecheck_test-0.6.5 (c (n "bytecheck_test") (v "0.6.5") (d (list (d (n "bytecheck") (r "^0.6") (k 0)))) (h "0sq0s7d2akmk4lsjm9l38raclkx5xwnh8vxbqk16xw5l6jhd2i00") (f (quote (("verbose" "bytecheck/verbose") ("std" "bytecheck/std") ("default" "std"))))))

