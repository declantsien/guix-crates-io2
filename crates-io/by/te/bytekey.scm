(define-module (crates-io by te bytekey) #:use-module (crates-io))

(define-public crate-bytekey-0.2.0 (c (n "bytekey") (v "0.2.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)))) (h "12rfddzp4w0bhl1dw2wmmjs0gvgghn7n4pplj0qksdh211dknqiw")))

(define-public crate-bytekey-0.2.1 (c (n "bytekey") (v "0.2.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)))) (h "0y0917ng6kjfzmr0mcnmyr7vww3ga22a7sxrm0k1cbiwjh31rqgx")))

(define-public crate-bytekey-0.3.0 (c (n "bytekey") (v "0.3.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0pl7d099sm47lg6q10p93ang3rwiqj3vbpdw9w2bfm6qaaia9h76")))

(define-public crate-bytekey-0.4.0 (c (n "bytekey") (v "0.4.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0kr0iprcpbkcf0rm4685ymq6yw1jhvw9jrx0c7ickd3phnfz5qc9")))

(define-public crate-bytekey-0.4.1 (c (n "bytekey") (v "0.4.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "01fcz6fgr1rf1smgsv470jlkh6dbdf993kfa87b745l93pl8bz8s")))

(define-public crate-bytekey-0.4.2 (c (n "bytekey") (v "0.4.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0dwr3hczc4nrmrdsypxcfsvi01irw2rhp4cqj1dr2k110fj7frgc")))

