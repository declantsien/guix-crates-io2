(define-module (crates-io by te byte-slab) #:use-module (crates-io))

(define-public crate-byte-slab-0.1.0 (c (n "byte-slab") (v "0.1.0") (d (list (d (n "heapless") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.128") (k 0)))) (h "043nhra8wzmx93jkj4m84v0gx36kvwz8fhza9kgdllawgdr8ldrs") (f (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.1.1 (c (n "byte-slab") (v "0.1.1") (d (list (d (n "heapless") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.128") (k 0)))) (h "0wdg4k5wc7rq68fd4abpzh72hv7xv6xfg2mhz5rhwbc5ngmlr4cv") (f (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.1.2 (c (n "byte-slab") (v "0.1.2") (d (list (d (n "heapless") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.128") (k 0)))) (h "0sl2c1bbmi2sdz6l6d941mqnckmbcyxalj5ra84a2fv19g1i2wcl") (f (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.1.3 (c (n "byte-slab") (v "0.1.3") (d (list (d (n "heapless") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.128") (k 0)))) (h "1a5ic8phmvkr2wm67slfb7z4hig46mdjigy6pjfmjijx5g54hrdq") (f (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.1.4 (c (n "byte-slab") (v "0.1.4") (d (list (d (n "defmt") (r "^0.3.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.128") (k 0)))) (h "0vqamgx8q0a35sswjdgfsnya53ylrnphbaqgx6mamnawwmkni9h9") (f (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.1.5 (c (n "byte-slab") (v "0.1.5") (d (list (d (n "defmt") (r "^0.3.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.128") (k 0)))) (h "0ckz6194223fkxipf6j4mmbw1y0ldz9kji06d2nfn2axr8q69vdc") (f (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.2.0 (c (n "byte-slab") (v "0.2.0") (d (list (d (n "defmt") (r "^0.3.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.128") (k 0)))) (h "0crhrfh68a2mx3gfsbg5cdgsc3xkzj77kfwv5y9wiy8flx6k20rr") (f (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.2.1 (c (n "byte-slab") (v "0.2.1") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.128") (k 0)))) (h "126l9hywamcfvc1sp7bydi8hnqh03jwrcfcbnmd7hdrzafs96xp7") (f (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue" "defmt"))))))

(define-public crate-byte-slab-0.2.2 (c (n "byte-slab") (v "0.2.2") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.128") (k 0)))) (h "043kf1zy1x16x1aka9yr2bmik69962dlaqgnl4ibzzqb3nyk6krm") (f (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue" "defmt"))))))

