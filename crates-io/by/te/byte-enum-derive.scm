(define-module (crates-io by te byte-enum-derive) #:use-module (crates-io))

(define-public crate-byte-enum-derive-0.1.0 (c (n "byte-enum-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ykn99jjw58cd2q9512nxxkhbpk4dq2cy7l2jva96kinhlfhxaw7") (f (quote (("external_doc")))) (y #t)))

(define-public crate-byte-enum-derive-0.1.1 (c (n "byte-enum-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j7rf3qgq4bh0rf7pbw8p2apxbyl34r571jnchv690q27f9amd0j") (f (quote (("external_doc")))) (y #t)))

(define-public crate-byte-enum-derive-0.1.2 (c (n "byte-enum-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "150c73mnp05kcwlyrxp0hnb83d4yln73ma6rxd0r86wvac712ki5") (y #t)))

