(define-module (crates-io by te byte-strings-proc_macros) #:use-module (crates-io))

(define-public crate-byte-strings-proc_macros-0.2.0-rc1 (c (n "byte-strings-proc_macros") (v "0.2.0-rc1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "11nv5a0w73y0wgxw3w3qy0ls3dj14s8ygyhbz107f09p3r2n9i00")))

(define-public crate-byte-strings-proc_macros-0.2.0 (c (n "byte-strings-proc_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "0mxlii3gcsqaicpfpbkac9ik2wpfs1invjqnc4fmb2j2949j1kbx")))

(define-public crate-byte-strings-proc_macros-0.2.1 (c (n "byte-strings-proc_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "0ddwimzbc4xfy26fsldpwvcyp4ynlhm54wf09vp4ak3x6bjfj4q9")))

(define-public crate-byte-strings-proc_macros-0.2.2 (c (n "byte-strings-proc_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "1r88qljf3bj6ma0hbxr61xmd1ylj5jq78r330xx4q8wp7mkyhy2f")))

(define-public crate-byte-strings-proc_macros-0.3.0 (c (n "byte-strings-proc_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "056y4ixfixjch3mrjqwcm32i9d77dmcvgp61yd0kbvw4iw5cjh3f")))

(define-public crate-byte-strings-proc_macros-0.3.1 (c (n "byte-strings-proc_macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00xfn3anw5gs8hnss7s013c0m1qggnjagc22zqdwgmlq3zky1xv2")))

