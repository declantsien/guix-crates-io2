(define-module (crates-io by te byte_sha) #:use-module (crates-io))

(define-public crate-byte_sha-1.0.0 (c (n "byte_sha") (v "1.0.0") (d (list (d (n "hex_d_hex") (r "^1.0.0") (d #t) (k 0)))) (h "02vq190y9al1lizdn6bfjsvdq3xyw1zw1ib84ng4k75z4yiqamh8")))

(define-public crate-byte_sha-1.0.1 (c (n "byte_sha") (v "1.0.1") (d (list (d (n "hex_d_hex") (r "^1.0.0") (d #t) (k 0)))) (h "1wh4pwqa9cwn56rc4zh3fp1jg7vfgigmc4jbwfh69yg4bq8i7php")))

