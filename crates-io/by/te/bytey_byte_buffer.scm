(define-module (crates-io by te bytey_byte_buffer) #:use-module (crates-io))

(define-public crate-bytey_byte_buffer-0.1.0 (c (n "bytey_byte_buffer") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "18dj1sygrbq9f9ah729xp9ysa18csvq8smi259yadwi3g7q5mr38")))

(define-public crate-bytey_byte_buffer-0.1.1 (c (n "bytey_byte_buffer") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "09kp1bjkmxbq75g374zwn2dag45d50qqmmjq48l83k8sp37h586f")))

(define-public crate-bytey_byte_buffer-0.2.0 (c (n "bytey_byte_buffer") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1hy1g9yzzhzksqrw8hawmmxn90jqv0i67b7y0lmbpq9lm39ypmjs")))

(define-public crate-bytey_byte_buffer-0.2.1 (c (n "bytey_byte_buffer") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0b57bsgkvqn575rv1hqvagxvw6xnlr9z33lh1jmp7bmxjxknklp2")))

