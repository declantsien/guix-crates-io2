(define-module (crates-io by te bytecoding_derive) #:use-module (crates-io))

(define-public crate-bytecoding_derive-0.0.1 (c (n "bytecoding_derive") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0j9csg631jzyji7iv6z673zd08y6dgz1hszrpk0k5f61sfkldy24")))

(define-public crate-bytecoding_derive-0.1.0 (c (n "bytecoding_derive") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num-iter") (r "^0.1.42") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1x0gids555dnmh5655dhg24dynl0swhys6d4jjqk6bd9c8nnay75")))

