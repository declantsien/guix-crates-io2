(define-module (crates-io by te bytes-varint) #:use-module (crates-io))

(define-public crate-bytes-varint-1.0.0 (c (n "bytes-varint") (v "1.0.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)))) (h "1cs13649s40ga6wyvns9vk51y2k9hvg9k7rxzap85b9zzl8ffsz5")))

(define-public crate-bytes-varint-1.0.1 (c (n "bytes-varint") (v "1.0.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)))) (h "15kg6750svzpp1dnim0dqa9apgqh9gz3hnk5x6791s2ckbxkrrr5")))

(define-public crate-bytes-varint-1.0.2 (c (n "bytes-varint") (v "1.0.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)))) (h "1qzkprvp96mfhmljq74mdq9r8a7v7qdqqlgh7w1jnxlgkip9a924")))

(define-public crate-bytes-varint-1.0.3 (c (n "bytes-varint") (v "1.0.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)))) (h "0brkr2n9cpb99x7fbbcsjrcml42l8ihf2hviqhk9ssrngh685hal")))

