(define-module (crates-io by te bytes-ringbuffer) #:use-module (crates-io))

(define-public crate-bytes-ringbuffer-0.0.0 (c (n "bytes-ringbuffer") (v "0.0.0") (h "1nmaa3pl424hby4khv4l03mxc9h7nnw4sljqz630wihadc6xp2x5")))

(define-public crate-bytes-ringbuffer-0.1.0 (c (n "bytes-ringbuffer") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "12ddspbv886lx8k8v78i14jmkv31397cxj2b09q9rpxlg7gcspkd")))

