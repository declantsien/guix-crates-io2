(define-module (crates-io by te byte_chunk) #:use-module (crates-io))

(define-public crate-byte_chunk-0.1.0 (c (n "byte_chunk") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "0z3q3fl739hsigkrcbm011gkw956nfkawxx65jaxs7yvk9v0dn0k")))

(define-public crate-byte_chunk-0.1.1 (c (n "byte_chunk") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "0w8qank5iia3y7mbm2dvkk651sa2ks8gmaxbcfqaal9mjkp0z77s")))

