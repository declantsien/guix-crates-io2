(define-module (crates-io by te byteio) #:use-module (crates-io))

(define-public crate-byteio-0.0.0 (c (n "byteio") (v "0.0.0") (h "1lx3cw9amb04sql1kygjd9ili9vcsrbmy0piyz0yxmi3zsirdj0w")))

(define-public crate-byteio-0.1.0 (c (n "byteio") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1yi84kqbcllfrr5ymkhnp73v8nhcwxrb0lg88b187pqxmlac6pfl") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-byteio-0.2.0 (c (n "byteio") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "0xpr321gw1mb1dbvpcby3fy31mxw554gy9chvw4x4j2v3r4jdjh2") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-byteio-0.2.1 (c (n "byteio") (v "0.2.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "048rsn7gjsvnjcif54bf09jcp6zdlxxkw727pi3l1bch9zj3hlmw") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-byteio-0.2.2 (c (n "byteio") (v "0.2.2") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1pbbdigya31dhnjvfgfs480vqmig3ipinkq2mww73k8p366fq442") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-byteio-0.2.3 (c (n "byteio") (v "0.2.3") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1bw7c872j8yilgsq3ny1fxjk5rpzxc03d1w07m2h6f5j4nmsla0n") (f (quote (("std") ("default" "std") ("alloc"))))))

