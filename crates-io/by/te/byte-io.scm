(define-module (crates-io by te byte-io) #:use-module (crates-io))

(define-public crate-byte-io-0.1.0 (c (n "byte-io") (v "0.1.0") (h "10gqh5a63hbh997zxnvixwg0qlnm6m27hmpm1i0qgjslsl7bh6f4")))

(define-public crate-byte-io-0.1.1 (c (n "byte-io") (v "0.1.1") (h "19cn67rzk4ana4jc5my4w5k6cv5rd814r9l03ycr2x9vwwczk3x6")))

