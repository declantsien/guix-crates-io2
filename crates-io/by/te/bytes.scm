(define-module (crates-io by te bytes) #:use-module (crates-io))

(define-public crate-bytes-0.0.1 (c (n "bytes") (v "0.0.1") (h "15b8ivajwpqc1v8x7ygmh310af3jmnz3b8cyr4hcva5s2j5a9af9")))

(define-public crate-bytes-0.1.0 (c (n "bytes") (v "0.1.0") (d (list (d (n "iobuf") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.1.2") (d #t) (k 2)))) (h "0f5vnhwcp2srfnv592j65ijm8q7xil9rsxxvf5hjnwylifmmqlpd")))

(define-public crate-bytes-0.1.1 (c (n "bytes") (v "0.1.1") (d (list (d (n "iobuf") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.1.2") (d #t) (k 2)))) (h "162ab5rwigicpl323zlqz2brqq26c42a87fsn3gsz1671k6al0rm")))

(define-public crate-bytes-0.1.2 (c (n "bytes") (v "0.1.2") (d (list (d (n "rand") (r "^0.1.2") (d #t) (k 2)))) (h "0bzc2d032qb5a7cpjwx8mh7a0ww26534481m5xz4d1s720r83dxl")))

(define-public crate-bytes-0.2.0 (c (n "bytes") (v "0.2.0") (d (list (d (n "rand") (r "^0.2.1") (d #t) (k 2)))) (h "19kflzkrxb1rygfy2qgic7h34nywwn09zqprxwxz8bbiczr63ds3")))

(define-public crate-bytes-0.2.1 (c (n "bytes") (v "0.2.1") (d (list (d (n "rand") (r "^0.2.1") (d #t) (k 2)))) (h "1j7ndfqwl531d3alfls63wdh9iqq16aq2ybxhvz9w3vxxg70v97g")))

(define-public crate-bytes-0.2.2 (c (n "bytes") (v "0.2.2") (d (list (d (n "rand") (r "^0.2.1") (d #t) (k 2)))) (h "0cav59sy6681qgf70s12mgchzkmp86kfq9f3ib8rc2hyd5qdqw61")))

(define-public crate-bytes-0.2.3 (c (n "bytes") (v "0.2.3") (d (list (d (n "rand") (r "^0.2.1") (d #t) (k 2)))) (h "1wqzpc3ayl1nll52w5vrb70mzv9dnyvy7bz027qv9vg04h4lv5sh")))

(define-public crate-bytes-0.2.4 (c (n "bytes") (v "0.2.4") (d (list (d (n "rand") (r "^0.3.5") (d #t) (k 2)))) (h "13gdyr5whgdw74nh2iz0qj3pnsihlp5y6b4p2sblk1mxignl8lch")))

(define-public crate-bytes-0.2.5 (c (n "bytes") (v "0.2.5") (d (list (d (n "rand") (r "^0.3.5") (d #t) (k 2)))) (h "1y389fjppmnrbgh18i71drjcmz6z3pacq5cp0yn1hhjqvaic7642")))

(define-public crate-bytes-0.2.6 (c (n "bytes") (v "0.2.6") (d (list (d (n "rand") (r "^0.3.5") (d #t) (k 2)))) (h "17qcgn3nxpz8a2s4gbq9xk5ymh1lh4a549fvd59jrch6czwpq79g")))

(define-public crate-bytes-0.2.7 (c (n "bytes") (v "0.2.7") (d (list (d (n "rand") (r "^0.3.5") (d #t) (k 2)))) (h "0hbkzjcv4p5v8mqjpmcjyjf5vb42ggqig7ls0j6qzy21abf3vaik")))

(define-public crate-bytes-0.2.8 (c (n "bytes") (v "0.2.8") (d (list (d (n "rand") (r "^0.3.5") (d #t) (k 2)))) (h "1f7m5jdi24vgv5as039przlyvvhmicf6hz1dvdmcfsginj589n97")))

(define-public crate-bytes-0.2.9 (c (n "bytes") (v "0.2.9") (d (list (d (n "rand") (r "^0.3.5") (d #t) (k 2)))) (h "1b1kqc296knmixcvr97qidwglgy5bd4kv2w63vvwa2952z1by3ka")))

(define-public crate-bytes-0.2.10 (c (n "bytes") (v "0.2.10") (d (list (d (n "rand") (r "^0.3.5") (d #t) (k 2)))) (h "1vffm3yljl2bhkx2b9wn7z4dslji8wxjch6hy2kii06m7yr8yx7q")))

(define-public crate-bytes-0.2.11 (c (n "bytes") (v "0.2.11") (d (list (d (n "rand") (r "^0.3.5") (d #t) (k 2)))) (h "17lbv6isma560fvdk499w992zhmkiy8p3yqsjv8qb6lacgjsvj4k")))

(define-public crate-bytes-0.3.0 (c (n "bytes") (v "0.3.0") (d (list (d (n "rand") (r "^0.3.5") (d #t) (k 2)))) (h "09vcp9kh12pva2xn2ir79k90v1a0id8f4sdv1abn5ifw2bqsyaf1")))

(define-public crate-bytes-0.4.0 (c (n "bytes") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 2)))) (h "12m10gdkvlwrn7d5p1j102sf3fy3gq5rs3izwvvbsz4kpf2knl46")))

(define-public crate-bytes-0.4.1 (c (n "bytes") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 2)))) (h "15jblvs4524ll4kzli5p2l9bk0p0af544id4z6iy65dfc002l4a6")))

(define-public crate-bytes-0.4.2 (c (n "bytes") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 2)))) (h "16j3lncfb7kdfai18xf5nsnibkb76pbw5pf24ys1g1qxm0yr6h9r")))

(define-public crate-bytes-0.4.3 (c (n "bytes") (v "0.4.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 2)))) (h "0lxm4b7ccmw3a4ap1f3mg39r32kn05lk5yc0hvqx2rsx258vivgr")))

(define-public crate-bytes-0.4.4 (c (n "bytes") (v "0.4.4") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "052wkiz74zpim0p4zbgy23wp3wgpf9mx9vm564il4igljdjz294b")))

(define-public crate-bytes-0.4.5 (c (n "bytes") (v "0.4.5") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1mis6gwz0f585xivh48fxsj2ssrd2gr0q7a21kjf6pfcb1xzja6q")))

(define-public crate-bytes-0.4.6 (c (n "bytes") (v "0.4.6") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1scwpm3k85kr47fkzdwrp332mh23qdpzvqxjr5npz5qqswvv8z8v")))

(define-public crate-bytes-0.4.7 (c (n "bytes") (v "0.4.7") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0n8yqvi3hdipsvayw6g8783rsd9zxqm8pkc9ybslaxgvfv45079g")))

(define-public crate-bytes-0.4.8 (c (n "bytes") (v "0.4.8") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1zwrpz9mfprqsby47z4z9qzp66i8shaqyn56rgqd6mv9ls4jklvx")))

(define-public crate-bytes-0.4.9 (c (n "bytes") (v "0.4.9") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1f2gl8vdgz4qhga480gqz5klwrdjashx986mhfq49s1rwbhbhy71") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-bytes-0.4.10 (c (n "bytes") (v "0.4.10") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0qma2kkfx9pyml60k2wkznkrflig4jg4xbscqdpj95dhak9mpr8c") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-bytes-0.4.11 (c (n "bytes") (v "0.4.11") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1yksbhrhis04k5c4ibm40rzfqv8s8vn2m4dhbqscphh3fv9f7ba0") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-bytes-0.4.12 (c (n "bytes") (v "0.4.12") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0768a55q2fsqdjsvcv98ndg9dq7w2g44dvq1avhwpxrdzbydyvr0") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-bytes-0.5.0 (c (n "bytes") (v "0.5.0") (d (list (d (n "loom") (r "^0.2.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "04kcq8bl2vh27jvv2vbb0h008nxjqs2fvc7bv6zlhygspvwqr72l") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-bytes-0.5.1 (c (n "bytes") (v "0.5.1") (d (list (d (n "loom") (r "^0.2.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "13qwifqmxicki2jdgv7xl3liqibzmr6scznqdmaa738s3mnmirs2") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-bytes-0.5.2 (c (n "bytes") (v "0.5.2") (d (list (d (n "loom") (r "^0.2.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "15fq2gn9zk111dkzm1lj3704c6mpdbi6i3k70g3jckky2ngk318w") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-bytes-0.5.3 (c (n "bytes") (v "0.5.3") (d (list (d (n "loom") (r "^0.2.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0f2z23hm1fjz605bfsiggjdcyqnkms88282ag9ghacmkvqalq00h") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytes-0.5.4 (c (n "bytes") (v "0.5.4") (d (list (d (n "loom") (r "^0.2.13") (d #t) (t "cfg(not(windows))") (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1q9r7si1l8vndg4n2ny2nv833ghp5vyqzk5indb9rmhd5ibaq2hk") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytes-0.5.5 (c (n "bytes") (v "0.5.5") (d (list (d (n "loom") (r "^0.3") (d #t) (t "cfg(loom)") (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0fr1kksxb6xyvm8daqygizv61z4c3rx2sjy3wcb0hzdrzcvg130i") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytes-0.5.6 (c (n "bytes") (v "0.5.6") (d (list (d (n "loom") (r "^0.3") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0f5s7xq6qzmdh22ygsy8v0sp02m51y0radvq4i4y8cizy1lfqk0f") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytes-0.6.0 (c (n "bytes") (v "0.6.0") (d (list (d (n "loom") (r "^0.3") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "05ivrcbgl4f7z2zzm9hbsi8cy66spi70xlm6fp16zsq4ylsvrp70") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1.0.0 (c (n "bytes") (v "1.0.0") (d (list (d (n "loom") (r "^0.4") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0wpsy2jwmbrsn7x6vcd00hw9vvz071lv8nrb25wrspvmkna8w7xd") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1.0.1 (c (n "bytes") (v "1.0.1") (d (list (d (n "loom") (r "^0.4") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0h6h1c8g3yj2b4k8g25gr3246mq985y0kl3z685cs784fr1ww05p") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1.1.0 (c (n "bytes") (v "1.1.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1y70b249m02lfp0j6565b29kviapj4xsl9whamcqwddnp9kjv1y4") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1.2.0 (c (n "bytes") (v "1.2.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "03hk3qsjkafww539rl01j0pxq7nrmcimfwd0crhf2rsy1i5dxczh") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1.2.1 (c (n "bytes") (v "1.2.1") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1nsni0jbx1048inbrarn3hz6zxd000pp0rac2mr07s7xf1m7p2pc") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1.3.0 (c (n "bytes") (v "1.3.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0g1s7b19wa2v5vqjif5d9al9gwxqnv310gv63cmaz88mdf34xcnz") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1.4.0 (c (n "bytes") (v "1.4.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1gkh3fk4fm9xv5znlib723h5md5sxsvbd5113sbxff6g1lmgvcl9") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1.5.0 (c (n "bytes") (v "1.5.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "08w2i8ac912l8vlvkv3q51cd4gr09pwlg3sjsjffcizlrb0i5gd2") (f (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1.6.0 (c (n "bytes") (v "1.6.0") (d (list (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1jf2awc1fywpk15m6pxay3wqcg65ararg9xi4b08vnszwiyy2kai") (f (quote (("std") ("default" "std")))) (r "1.39")))

