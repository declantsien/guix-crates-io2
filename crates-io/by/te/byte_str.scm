(define-module (crates-io by te byte_str) #:use-module (crates-io))

(define-public crate-byte_str-0.1.0 (c (n "byte_str") (v "0.1.0") (h "1gr5i2c5g2qj7nhs27bz0mfa3cdp78hr007avgikdz7bkkyhm421") (f (quote (("no_std")))) (y #t)))

(define-public crate-byte_str-0.1.1 (c (n "byte_str") (v "0.1.1") (h "0p6fmm3l060dd37blcjfw8vdjdw1gg5mnsv8c4sk6ayxvr54vjid") (f (quote (("no_std")))) (y #t)))

(define-public crate-byte_str-0.1.2 (c (n "byte_str") (v "0.1.2") (h "16dw5k9pgfn7f9h3mi7cpw3ncwqxbx6yddpnghh5hgpykldknw5z") (f (quote (("no_std")))) (y #t)))

