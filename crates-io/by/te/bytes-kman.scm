(define-module (crates-io by te bytes-kman) #:use-module (crates-io))

(define-public crate-bytes-kman-0.1.0 (c (n "bytes-kman") (v "0.1.0") (d (list (d (n "derive") (r "^0.1.0") (d #t) (k 0)))) (h "0s86zyfxgm0d1128wp9a2g6qlc2rjw556dxgh26q8v0nckjl31af") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.1 (c (n "bytes-kman") (v "0.1.1") (d (list (d (n "derive") (r "^0.1.0") (d #t) (k 0)))) (h "19qfjymmljdnfc40c74h7q04v3assq37czy63gpp424l9ys85sqc") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.2 (c (n "bytes-kman") (v "0.1.2") (d (list (d (n "bytes-kman-derive") (r "^0.1.0") (d #t) (k 0)))) (h "07p0vks6pn8dhjvri0q12q0d48kal2yd0ac0p4q32qandl3d7c66") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.3 (c (n "bytes-kman") (v "0.1.3") (d (list (d (n "bytes-kman-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0irgbli0blfah42bizvsdfr8gwaywx1b3njzr35ca5cccm2dvqjq") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.4 (c (n "bytes-kman") (v "0.1.4") (d (list (d (n "bytes-kman-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1n2hgqknqkp9wfn5s3ysm9iixwl80b20mdnhpxlcclrs7wdr7rn1") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.5 (c (n "bytes-kman") (v "0.1.5") (d (list (d (n "bytes-kman-derive") (r "^0.1.2") (d #t) (k 0)))) (h "17lzxmrddmdifisic3qkiyx23dxr949l8v4p9glr63isxc6fc5c4") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.6 (c (n "bytes-kman") (v "0.1.6") (d (list (d (n "bytes-kman-derive") (r "^0.1.2") (d #t) (k 0)))) (h "01sm7q97s51n14mfdqhzizv798aq7lj4skhizh71l61l359h4qsf") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.7 (c (n "bytes-kman") (v "0.1.7") (d (list (d (n "bytes-kman-derive") (r "^0.1.3") (d #t) (k 0)))) (h "05xkwb5v5w0vb1m9v7k6gz4aqxhmjg2hxwxi3a1qqx887qr17yll") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.8 (c (n "bytes-kman") (v "0.1.8") (d (list (d (n "bytes-kman-derive") (r "^0.1.3") (d #t) (k 0)))) (h "01mgxjz63kxamdd5znlh3vnqhx6ax4cj3yzvmkxs57rjhvqnca8x") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.9 (c (n "bytes-kman") (v "0.1.9") (d (list (d (n "bytes-kman-derive") (r "^0.1.3") (d #t) (k 0)))) (h "0cswsxzqmvjr8a62kqs2b8mvf605pl5dzij72sf3636pk0g1731b") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.10 (c (n "bytes-kman") (v "0.1.10") (d (list (d (n "bytes-kman-derive") (r "^0.1.3") (d #t) (k 0)))) (h "0ihwqpw1y44i2xh8cv6wli1wjd13zckls82vfbxazcixyk0lvl0i") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.11 (c (n "bytes-kman") (v "0.1.11") (d (list (d (n "bytes-kman-derive") (r "^0.1.4") (d #t) (k 0)))) (h "105kszj4zrsphvdpzcmk46maw2rx7k4cq4vc60ci3qp5n9m7qxr7") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.12 (c (n "bytes-kman") (v "0.1.12") (d (list (d (n "bytes-kman-derive") (r "^0.1.5") (d #t) (k 0)))) (h "1zhy93yly7ikrc5iwxqp4dg4mgkhfwdjlqa61fs7zrma5g3yc0ia") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.13 (c (n "bytes-kman") (v "0.1.13") (d (list (d (n "bytes-kman-derive") (r "^0.1.5") (d #t) (k 0)))) (h "110j74y46m752j6g1dz59svvxpfya7pm512vimb1h2ggb11pk8bf") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.14 (c (n "bytes-kman") (v "0.1.14") (d (list (d (n "bytes-kman-derive") (r "^0.1.5") (d #t) (k 0)))) (h "0wrx6mz433hywjnqf76diwz5ymd0v2w390248rbqaz8qbprqsjhk") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.15 (c (n "bytes-kman") (v "0.1.15") (d (list (d (n "bytes-kman-derive") (r "^0.1.5") (d #t) (k 0)))) (h "0i6s2m566fwrs96lyb0paa0piv97cwhrqmx56qxj1x3rzixpw824") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.16 (c (n "bytes-kman") (v "0.1.16") (d (list (d (n "bytes-kman-derive") (r "^0.1.5") (d #t) (k 0)))) (h "0l6qm9i50x6hx2diqvxjw0v7ydlmiqnkka926jsnscg29zy3c2wd") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.17 (c (n "bytes-kman") (v "0.1.17") (d (list (d (n "bytes-kman-derive") (r "^0.1.5") (d #t) (k 0)))) (h "1sg8qk2sm83i2bn91zdpf2cfyiks82xvjkqznbawpi2p7zmczv2k") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.18 (c (n "bytes-kman") (v "0.1.18") (d (list (d (n "bytes-kman-derive") (r "^0.1.5") (d #t) (k 0)))) (h "182hf9hc1y2b05a3qpf830i4f80b03lz88hcb62s89zvahqnmpzn") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.19 (c (n "bytes-kman") (v "0.1.19") (d (list (d (n "bytes-kman-derive") (r "^0.1.5") (d #t) (k 0)))) (h "1lfla3k9p48kfndz4s69f2c68jhg4hhxykaqzd0kkpwd2l3y37lc") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1.20 (c (n "bytes-kman") (v "0.1.20") (d (list (d (n "bytes-kman-derive") (r "^0.1.5") (d #t) (k 0)))) (h "02a2458vwymh39ckq0hipiac9j158hi37n0b7x29rjbmb7lxqb3c") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.2.0 (c (n "bytes-kman") (v "0.2.0") (d (list (d (n "bytes-kman-derive") (r "^0.2.0") (d #t) (k 0)))) (h "15qnlhq218mhqhifvfp9sg8dvgq4xhq5dzzbvln4l24r79l8h4f9") (f (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.3.0 (c (n "bytes-kman") (v "0.3.0") (d (list (d (n "bytes-kman-derive") (r "^0.3.0") (d #t) (k 0)))) (h "08sg3ir0lsyl1xx7lqy64mwi0gl8nnnpvvxkvaxjhl3hcfaji2kk") (f (quote (("std") ("default" "core" "std") ("core"))))))

