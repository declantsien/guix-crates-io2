(define-module (crates-io by te bytey_derive) #:use-module (crates-io))

(define-public crate-bytey_derive-0.1.0 (c (n "bytey_derive") (v "0.1.0") (d (list (d (n "bytey_byte_buffer") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1w14q75i0mjxi4hypp0zlwjdx5z2972c1r791807drbzdgc0s5a9")))

(define-public crate-bytey_derive-0.2.0 (c (n "bytey_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1qdqrjlx1r1v1sixabx6sdirf02mprfyw1j4dq0xrv6hhxvy4yfq")))

