(define-module (crates-io by te byte_rb) #:use-module (crates-io))

(define-public crate-byte_rb-1.0.0 (c (n "byte_rb") (v "1.0.0") (h "1vj0glsngchzxjmmrjj4q253c41zylcdgjfw5i0gz1q1s5hcd9zr") (y #t)))

(define-public crate-byte_rb-1.0.1 (c (n "byte_rb") (v "1.0.1") (h "08xagk7vz7yqjvychbknpgsyv1xypdh5yyc6lpygfq1vxhrm77rz") (y #t)))

(define-public crate-byte_rb-1.0.2 (c (n "byte_rb") (v "1.0.2") (h "120jw2f11i7mi482psfsr8836f9j3d3cwr0j3svkh62s824zm56y") (y #t)))

(define-public crate-byte_rb-1.0.3 (c (n "byte_rb") (v "1.0.3") (h "1c2nm57p82vzj29ii3w20q4wah105p0ska11p8vphvw6dc9sdycf") (y #t)))

(define-public crate-byte_rb-1.0.4 (c (n "byte_rb") (v "1.0.4") (h "01x7n2fg6z7mw04gcjx2v71aajml9d3wjz928cfrp7nw5apcg4gb")))

