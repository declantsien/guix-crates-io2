(define-module (crates-io by te bytes-expand) #:use-module (crates-io))

(define-public crate-bytes-expand-0.4.12 (c (n "bytes-expand") (v "0.4.12") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "17n951f63777z6pr5jk85aa3z82fg5db96g4msa643rxn1vnk6fv") (f (quote (("i128" "byteorder/i128"))))))

