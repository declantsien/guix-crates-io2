(define-module (crates-io by te bytes-utils) #:use-module (crates-io))

(define-public crate-bytes-utils-0.1.0 (c (n "bytes-utils") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)))) (h "1qhds8awnbknwsbzgw4h5r0yzc4gifyaaj22giczbbz0l6faibsh")))

(define-public crate-bytes-utils-0.1.1 (c (n "bytes-utils") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)))) (h "0m3fvz4pcxwrnf7pzppalmkwdb999534d910b4z14hqwjl94fcaf")))

(define-public crate-bytes-utils-0.1.2 (c (n "bytes-utils") (v "0.1.2") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "either") (r "^1") (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)))) (h "1wcf1lsblzkxdi22xm7i56iingkifwg7i4kajvjgv3mckkps6d0r") (f (quote (("std" "bytes/default" "either/default") ("default" "std"))))))

(define-public crate-bytes-utils-0.1.3 (c (n "bytes-utils") (v "0.1.3") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "either") (r "^1") (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)))) (h "1ffllwr7kglgmn8qhbxpbd5vmczdj9cm603lsang70z2fs03lzg4") (f (quote (("std" "bytes/default" "either/default") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde" "bytes/serde"))))))

(define-public crate-bytes-utils-0.1.4 (c (n "bytes-utils") (v "0.1.4") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "either") (r "^1") (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)))) (h "0dcd0lxfpj367j9nwm7izj4mkib3slg61rg4wqmpw0kvfnlf7bvx") (f (quote (("std" "bytes/default") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde" "bytes/serde"))))))

