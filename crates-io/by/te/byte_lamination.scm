(define-module (crates-io by te byte_lamination) #:use-module (crates-io))

(define-public crate-byte_lamination-0.1.0 (c (n "byte_lamination") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "serde_bare") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "05hb9dy53bha38a7hbcmp5y8h624c0vjykm31vah8jrm83d6amz4") (f (quote (("cbor" "serde_cbor" "serde") ("bare" "serde_bare" "serde") ("all" "zstd" "cbor" "bare"))))))

(define-public crate-byte_lamination-0.1.1 (c (n "byte_lamination") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "serde_bare") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "1zphl1j6yp5gr2ma88c8zmgdlm7vih7289c4qnm4jsf1nx9c5xiy") (f (quote (("cbor" "serde_cbor" "serde") ("bare" "serde_bare" "serde") ("all" "zstd" "cbor" "bare"))))))

(define-public crate-byte_lamination-0.1.2 (c (n "byte_lamination") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "serde_bare") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "0rg49ax0akma8bp2gpca8layy27477kdv5i08ag01gb2nrnm3vlx") (f (quote (("cbor" "serde_cbor" "serde") ("bare" "serde_bare" "serde") ("all" "zstd" "cbor" "bare"))))))

