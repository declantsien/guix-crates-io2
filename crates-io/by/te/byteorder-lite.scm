(define-module (crates-io by te byteorder-lite) #:use-module (crates-io))

(define-public crate-byteorder-lite-0.1.0 (c (n "byteorder-lite") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "15alafmz4b9az56z6x7glcbcb6a8bfgyd109qc3bvx07zx4fj7wg") (f (quote (("std") ("default" "std")))) (r "1.60")))

