(define-module (crates-io by te byteripper) #:use-module (crates-io))

(define-public crate-byteripper-0.1.1 (c (n "byteripper") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "goblin") (r "^0.3.4") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "xfailure") (r "^0.1.0") (d #t) (k 0)))) (h "07l66ywrh40dvjk61km418kvzxfahpkgvf34csnksd0fy9h54wvq")))

(define-public crate-byteripper-0.1.2 (c (n "byteripper") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "goblin") (r "^0.3.4") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lv8kqlaljv083cf9dysadaxvy8krzsp9qv3awcnqiicd7d69791")))

