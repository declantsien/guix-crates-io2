(define-module (crates-io by te bytehash) #:use-module (crates-io))

(define-public crate-bytehash-1.0.0 (c (n "bytehash") (v "1.0.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)))) (h "0pqnab5zwb34kzxqvwf9h0ynb774ljjmjy5d6qhkwa6vizqp92kb") (y #t)))

(define-public crate-bytehash-2.0.0 (c (n "bytehash") (v "2.0.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)))) (h "0gn6jxy08vna5f04rkyrs1vnwxdkhmzvmhq80axahjxl04wl7xl4") (y #t)))

(define-public crate-bytehash-3.0.0 (c (n "bytehash") (v "3.0.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)))) (h "17m7lb9jknf3bhgdaw2fcgn3f17ncvjhh36k2xayx6arhqg0p86q") (y #t)))

(define-public crate-bytehash-0.1.0 (c (n "bytehash") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)))) (h "1baral4r93bphlrdyna48fszic1b8b8pr9223qfgx1wssycy7m40")))

(define-public crate-bytehash-0.2.0 (c (n "bytehash") (v "0.2.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)))) (h "0v40zldcmb46np1069jilc1wl75qh5dh4xkl63q3bbmsfysxb5r7")))

(define-public crate-bytehash-0.2.1 (c (n "bytehash") (v "0.2.1") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)))) (h "1h27vmkq1gfiqcn9pn0yc1zpfnhi6riwrkghcyni5ghish4kvza1")))

(define-public crate-bytehash-0.3.0 (c (n "bytehash") (v "0.3.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)))) (h "0qdv8immnhg8kvd5agx4p8n5aa08v6l88vjds3y571ghdnrfb918")))

