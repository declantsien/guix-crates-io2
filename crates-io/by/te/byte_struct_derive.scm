(define-module (crates-io by te byte_struct_derive) #:use-module (crates-io))

(define-public crate-byte_struct_derive-0.1.0 (c (n "byte_struct_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "1yj11dhcsligjc1g9dw8naml9kfg0bdpckc1zfi8772rif1b5rix")))

(define-public crate-byte_struct_derive-0.2.0 (c (n "byte_struct_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "14nvbhbyhxvz7h1yyv861v05nsqfl5i1lif3c9lw5iyvr95xi5w3")))

(define-public crate-byte_struct_derive-0.4.0 (c (n "byte_struct_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "1h03xxdf91nng117pz5wrv5h01v0wz1h3lh24s1awplwfnac25nh")))

(define-public crate-byte_struct_derive-0.4.2 (c (n "byte_struct_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (d #t) (k 0)))) (h "0bhbnhdgvz1rzgzyx42k4m0712qnfs3in7vxam2c1zhawp6yrdkz")))

(define-public crate-byte_struct_derive-0.9.0 (c (n "byte_struct_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "04arvl1sq2bwl5iahaczywjc5c1545xp491s8xm5f9n8j3hiv307")))

