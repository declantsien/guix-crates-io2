(define-module (crates-io by te byte-unit) #:use-module (crates-io))

(define-public crate-byte-unit-1.0.0 (c (n "byte-unit") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0isrdizbb9vyf9jnl5q0w9b7wm795lqzshskflqn37870w7yn1sy")))

(define-public crate-byte-unit-1.0.1 (c (n "byte-unit") (v "1.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1xg2n85n2agnr8djyj6r8fmc3m6z0x51f40li56hjacdk61j7331")))

(define-public crate-byte-unit-1.0.2 (c (n "byte-unit") (v "1.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0an7crj3v2xn6cf9xlcahf8afa4n1aqlqqlvhksp3qs1jvhqva1w")))

(define-public crate-byte-unit-1.0.3 (c (n "byte-unit") (v "1.0.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1fd7f45dkz4i9f3iv20bqlm98pzvnvd5fyd29c07j2pj4d1yvpx5")))

(define-public crate-byte-unit-1.0.4 (c (n "byte-unit") (v "1.0.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04rd72irszmsjbqw010lwki7l6ha6spss4kd9r5ibgbsb03mzkw0")))

(define-public crate-byte-unit-1.0.5 (c (n "byte-unit") (v "1.0.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0kn9p24wxybydqwg8jhmizv1qn2l9mp1pdblsxanb40fi7v2dagc")))

(define-public crate-byte-unit-1.0.6 (c (n "byte-unit") (v "1.0.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1cx6ap20qs0vindw4lq97i4rrsw0s5zr2fd7gh85vg9a19xr716l")))

(define-public crate-byte-unit-1.0.7 (c (n "byte-unit") (v "1.0.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1n7id1zvfgs7m2mwl9z3h8flr062mqx6zp0p2b8m5yv27y1dvlxb")))

(define-public crate-byte-unit-1.0.8 (c (n "byte-unit") (v "1.0.8") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0c13v01k6b1jca8w8566r8bfmzmf08bn039c7rq1bifnpyj33qfk")))

(define-public crate-byte-unit-1.0.9 (c (n "byte-unit") (v "1.0.9") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0byqd08x67h10n5hp5c47lg73s7wlm4hw80gdhmqhrkni3cbwswz")))

(define-public crate-byte-unit-1.1.0 (c (n "byte-unit") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hflans9qdcsvf9k7mnwyzdl1mpbn2wahn4yimyd5dvk9f47ni2y")))

(define-public crate-byte-unit-2.0.0 (c (n "byte-unit") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "16xa10s4xi9zq30rrj8f4mp5mm3sfx9ybxvf98bqayp6432d5p56")))

(define-public crate-byte-unit-2.1.0 (c (n "byte-unit") (v "2.1.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hdqmz9kmpkdf8zl9fg3rng1mhqw5y2cdw41agnpn5ma0d3vnm37")))

(define-public crate-byte-unit-3.0.0 (c (n "byte-unit") (v "3.0.0") (h "0ybis2l3bzbpfxryvxa3ncb1x5xk1sf1nfkbh2g8ipb78nzak9n1")))

(define-public crate-byte-unit-3.0.1 (c (n "byte-unit") (v "3.0.1") (h "11qyipx8g951wcyvz0d82plwpnncb3ji5wj45n1w8xlpxia9j4wh")))

(define-public crate-byte-unit-3.0.2 (c (n "byte-unit") (v "3.0.2") (h "0glj3q3x980mabpjc1s3bcrr6k0zrxlddrzd5g96859himgakb42")))

(define-public crate-byte-unit-3.0.3 (c (n "byte-unit") (v "3.0.3") (h "0mlh8xngs5izs8nl10ryr1q0x0zql1nql4wsy7cr0x40a2asg538")))

(define-public crate-byte-unit-3.1.0 (c (n "byte-unit") (v "3.1.0") (h "0yx6gci6k7vm6i2giiba5am0ycni4yin4kwn4vghmzrwnyr578s8") (f (quote (("u128") ("std") ("default" "u128"))))))

(define-public crate-byte-unit-3.1.1 (c (n "byte-unit") (v "3.1.1") (h "166ish3qfqkialf3fbhb65r6h24gbgp2gp8hvk666c6f120mm70a") (f (quote (("u128") ("std") ("default" "u128"))))))

(define-public crate-byte-unit-3.1.2 (c (n "byte-unit") (v "3.1.2") (h "1bk0s69v4q0xzsmzbllb3ihv1ds380h1b22j9bb153cfj3ycmjm3") (f (quote (("u128") ("std") ("default" "u128"))))))

(define-public crate-byte-unit-3.1.3 (c (n "byte-unit") (v "3.1.3") (h "0h90pzly6cnjmw6aci2ywdrs67d2037dl9p9yf1hdrqwyaxhsfam") (f (quote (("u128") ("std") ("default" "u128"))))))

(define-public crate-byte-unit-3.1.4 (c (n "byte-unit") (v "3.1.4") (h "1f244x8l68x4s2zcj2lylkjkgp3smjvhwg0rj95ms00ivv4h2ls1") (f (quote (("u128") ("std") ("default" "u128"))))))

(define-public crate-byte-unit-4.0.0 (c (n "byte-unit") (v "4.0.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0ggkhz41cnyp4r3bp3sg3nl57nlidyqvrrbxbjbzzaaz37djdlai") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.1 (c (n "byte-unit") (v "4.0.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0n4a8y67nak76lvhds7n451lqz7rcmfvyxzrlck4mdphzf1b7v6i") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.2 (c (n "byte-unit") (v "4.0.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0lmhyyqwnsbyrasncza36l59kz801rbc3nb30kwf0jiyf8pmfr8z") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.3 (c (n "byte-unit") (v "4.0.3") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1x5g7n7xqwbx43aixc559wfhk4qkqzxf9a9h61b131ql8d2r429h") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.4 (c (n "byte-unit") (v "4.0.4") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1dh3blkld26xydywk6ir6pr79nmndq0q2kdcf80q26d92w0jagm8") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.5 (c (n "byte-unit") (v "4.0.5") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1zycj4s9ya764p177357ddhlacllqy55lrlbbqf0g1l5ng7y56xg") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.6 (c (n "byte-unit") (v "4.0.6") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1l1iqcxgasdgw6waharx2gr2s3ysr1a1cp7sac087f6kxc8pwj7r") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.7 (c (n "byte-unit") (v "4.0.7") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1ggwkmhrsqkr184k2qx0gmxyw05cz0mvh90ffcs4xq1yqxx0rhg6") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.8 (c (n "byte-unit") (v "4.0.8") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0k1zcp911qn5sin93brgpqjc53w1l53n7nx9vj6sivrgv9iqb98v") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.9 (c (n "byte-unit") (v "4.0.9") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0lxi11qf6h1rqr0yhsh7i6755l325qrkv9r4bgismyik531mi1qw") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.10 (c (n "byte-unit") (v "4.0.10") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "09zfw6dv0x3lkg2r3kvn1ar43zzypjvcgm0gzsy9nfhw8w00jlmr") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.11 (c (n "byte-unit") (v "4.0.11") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0hz7zzr6sc9cwwr8z9cygqcsdbrjk23knbzg7drj20wwn1kqxn96") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.12 (c (n "byte-unit") (v "4.0.12") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1n5w327g55ly5r4yjh71rhv2ifw615xdxv8d2rj5nxsbxgk9fc86") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.13 (c (n "byte-unit") (v "4.0.13") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0zxfqic18j1byrxhn7msqbywzx2sp9izs89zksaadmy71rdzqvwm") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.14 (c (n "byte-unit") (v "4.0.14") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0jq1w1xr6403v62ckzblzkbn1varldr5b89fykq9zwb5v86z3swm") (f (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4.0.15 (c (n "byte-unit") (v "4.0.15") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1q4m3icp2qrx0rkb653k3iyv3wxpg5qnwc4ijvc6hyqbd812dv08") (f (quote (("u128") ("std" "serde/std" "alloc") ("serde" "alloc") ("default" "std" "u128") ("alloc")))) (y #t)))

(define-public crate-byte-unit-4.0.16 (c (n "byte-unit") (v "4.0.16") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1j5g46y9mx3g4w9v0k51dickrb6givriqyb0vbbfd9hgy76q6kqk") (f (quote (("u128") ("std" "serde/std" "alloc") ("serde" "alloc") ("default" "std" "u128") ("alloc")))) (y #t)))

(define-public crate-byte-unit-4.0.17 (c (n "byte-unit") (v "4.0.17") (d (list (d (n "serde_dep") (r "^1") (f (quote ("alloc"))) (o #t) (k 0) (p "serde")) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1707hr76fv1bwk5m9kwk1g5cm6l3iwa14afb1jdc1c17ssrx86jq") (f (quote (("u128") ("std" "serde_dep/std" "alloc") ("serde" "alloc" "serde_dep") ("default" "std" "u128") ("alloc"))))))

(define-public crate-byte-unit-4.0.18 (c (n "byte-unit") (v "4.0.18") (d (list (d (n "serde_dep") (r "^1") (f (quote ("alloc"))) (o #t) (k 0) (p "serde")) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1124a4iggq5x85xp4w35mrf5syqyhsd8xapzgxj4hj7008v6fj1k") (f (quote (("u128") ("std" "serde_dep/std" "alloc") ("serde" "alloc" "serde_dep") ("default" "std" "u128") ("alloc"))))))

(define-public crate-byte-unit-4.0.19 (c (n "byte-unit") (v "4.0.19") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0v4v4z8qilnbg0lv16icbbbdq5kjpbp8yw044lszrzdqawhb6y6s") (f (quote (("u128") ("std" "serde/std" "alloc") ("default" "std" "u128") ("alloc")))) (s 2) (e (quote (("serde" "alloc" "dep:serde"))))))

(define-public crate-byte-unit-5.0.0-beta.1 (c (n "byte-unit") (v "5.0.0-beta.1") (d (list (d (n "rust_decimal") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1jan2jc22i1bpnv291xy8fh6mab84sq5g1lkk19blsb4al87q3p0") (f (quote (("u128") ("std" "serde/std" "alloc") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal") ("alloc")))) (s 2) (e (quote (("serde" "alloc" "dep:serde") ("rust_decimal" "dep:rust_decimal")))) (r "1.60")))

(define-public crate-byte-unit-5.0.0 (c (n "byte-unit") (v "5.0.0") (d (list (d (n "rust_decimal") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1kwh2sxws9jwnlclj9n62r4lmh46c1m28d0lbb97rvqhmwsbkyjl") (f (quote (("u128") ("std" "serde/std" "alloc") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal") ("alloc")))) (s 2) (e (quote (("serde" "alloc" "dep:serde") ("rust_decimal" "dep:rust_decimal")))) (r "1.60")))

(define-public crate-byte-unit-5.0.1 (c (n "byte-unit") (v "5.0.1") (d (list (d (n "rust_decimal") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1m1wb6i5gjb70p140i9mns4vkgkbzqlcnxgnl08zfp1pxz7rbs5y") (f (quote (("u128") ("std" "serde/std" "alloc") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal") ("alloc")))) (s 2) (e (quote (("serde" "alloc" "dep:serde") ("rust_decimal" "dep:rust_decimal")))) (r "1.60")))

(define-public crate-byte-unit-5.0.2 (c (n "byte-unit") (v "5.0.2") (d (list (d (n "rust_decimal") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0lkmy2lrfp3lpcjaabj9ljwqy5w5rzq40yz24s6ch5h65avaqrwi") (f (quote (("u128") ("std" "serde/std" "alloc") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal") ("alloc")))) (s 2) (e (quote (("serde" "alloc" "dep:serde") ("rust_decimal" "dep:rust_decimal")))) (r "1.60")))

(define-public crate-byte-unit-5.0.3 (c (n "byte-unit") (v "5.0.3") (d (list (d (n "rust_decimal") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1qvvipsmdj916ca39m82914gqy6fbhqq3g0ymfvn9ygpw29ayh5w") (f (quote (("u128") ("std" "serde/std" "alloc") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal") ("alloc")))) (s 2) (e (quote (("serde" "alloc" "dep:serde") ("rust_decimal" "dep:rust_decimal")))) (r "1.60")))

(define-public crate-byte-unit-5.0.4 (c (n "byte-unit") (v "5.0.4") (d (list (d (n "rust_decimal") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1n09c21wlrpg9qllzhdf78dsr8xmgr07kp5mkxry96gyfq03wp9v") (f (quote (("u128") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde") ("rust_decimal" "dep:rust_decimal")))) (r "1.60")))

(define-public crate-byte-unit-5.1.0 (c (n "byte-unit") (v "5.1.0") (d (list (d (n "rocket") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0z41rspmbn3pf7pmnscjwnxsvmadqmlz6ppdqjxfs278ahkf1lxj") (f (quote (("u128") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal")))) (s 2) (e (quote (("std" "serde?/std" "rust_decimal?/std") ("serde" "dep:serde") ("rust_decimal" "dep:rust_decimal") ("rocket" "dep:rocket" "std")))) (r "1.69")))

(define-public crate-byte-unit-5.1.1 (c (n "byte-unit") (v "5.1.1") (d (list (d (n "rocket") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0wa6yqb3dxfrmbf7wqidnhd8cqi4n3i5pkqgybkhqj3mdxvs1sma") (f (quote (("u128") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal")))) (s 2) (e (quote (("std" "serde?/std" "rust_decimal?/std") ("serde" "dep:serde") ("rust_decimal" "dep:rust_decimal") ("rocket" "dep:rocket" "std")))) (r "1.69")))

(define-public crate-byte-unit-5.1.2 (c (n "byte-unit") (v "5.1.2") (d (list (d (n "rocket") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "1b1hchjm7zk73pzjmq4vlglxmd21kwsf6ljsvphv8qd140ab81fl") (f (quote (("u128") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal")))) (s 2) (e (quote (("std" "serde?/std" "rust_decimal?/std") ("serde" "dep:serde") ("rust_decimal" "dep:rust_decimal") ("rocket" "dep:rocket" "std")))) (r "1.69")))

(define-public crate-byte-unit-5.1.3 (c (n "byte-unit") (v "5.1.3") (d (list (d (n "rocket") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0ihblvhqlb68gsyyvaja946ypig0pk5cw1m5d9a0h3g7dchjgnnb") (f (quote (("u128") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal")))) (s 2) (e (quote (("std" "serde?/std" "rust_decimal?/std") ("serde" "dep:serde") ("rust_decimal" "dep:rust_decimal") ("rocket" "dep:rocket" "std")))) (r "1.69")))

(define-public crate-byte-unit-5.1.4 (c (n "byte-unit") (v "5.1.4") (d (list (d (n "rocket") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "utf8-width") (r "^0.1") (d #t) (k 0)))) (h "0gnnl92szl7d6bxz028n03ifflg96z4xp0lxqc3m8rmjy2yikb1k") (f (quote (("u128") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal")))) (s 2) (e (quote (("std" "serde?/std" "rust_decimal?/std") ("serde" "dep:serde") ("rust_decimal" "dep:rust_decimal") ("rocket" "dep:rocket" "std")))) (r "1.69")))

