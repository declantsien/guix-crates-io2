(define-module (crates-io by te byte_utils) #:use-module (crates-io))

(define-public crate-byte_utils-0.1.0 (c (n "byte_utils") (v "0.1.0") (h "1xm5v2c6sdc0chs8qvy8czm5h3mnn9djjir7g20d46fkagrb5hv0") (y #t)))

(define-public crate-byte_utils-0.0.1 (c (n "byte_utils") (v "0.0.1") (h "0vyqigkjw1xbw8092zqjj00sb2di79gpk46q89ll424awd3j6gp5") (y #t)))

(define-public crate-byte_utils-0.1.1 (c (n "byte_utils") (v "0.1.1") (h "1x7q93ax5byrbd06x6yi6wzc0lccqrslk83rkzv6saaxjiliwq8k") (y #t)))

