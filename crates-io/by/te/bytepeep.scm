(define-module (crates-io by te bytepeep) #:use-module (crates-io))

(define-public crate-bytepeep-0.1.0 (c (n "bytepeep") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "04kll0qgjcl080v922hplcwyzlzcg3w6qz8wybk8p6i665wm2jxr")))

