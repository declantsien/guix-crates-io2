(define-module (crates-io by te byte_buffer) #:use-module (crates-io))

(define-public crate-byte_buffer-0.1.0 (c (n "byte_buffer") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)))) (h "1j80ipaq6xkxlvr37fi1mn1n5839qwglwrjiypyj8dmclhaq68rv")))

(define-public crate-byte_buffer-0.1.1 (c (n "byte_buffer") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)))) (h "1ybsfhzzpf2692axiksklp9bz39qnk2gxp1nivwyyb6js8pgqi2l")))

(define-public crate-byte_buffer-0.1.2 (c (n "byte_buffer") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)))) (h "00y7afqnh2vsn1iyq3wpzpyv7flshyfq8gymzsai9pzlkrdmpl92") (y #t)))

(define-public crate-byte_buffer-0.1.3 (c (n "byte_buffer") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)))) (h "0n2qk6xhx45ir8y7kzrk4wgk3i5iblblpx3qfwxh7rsqlximkj6y")))

