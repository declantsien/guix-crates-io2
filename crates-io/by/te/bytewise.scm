(define-module (crates-io by te bytewise) #:use-module (crates-io))

(define-public crate-bytewise-0.1.0 (c (n "bytewise") (v "0.1.0") (h "1kjczqnbiwhz82c0dddym2pk5ij0bbdkcqhdjyd3wi0b68dnrvx4") (y #t)))

(define-public crate-bytewise-0.2.0 (c (n "bytewise") (v "0.2.0") (h "1dhwrrwjvqd9j85p0wn56nmn7r586i0j41631y1nv103ib9x9lic") (y #t)))

(define-public crate-bytewise-0.3.0 (c (n "bytewise") (v "0.3.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1akp7a5pdpj07b4ygjp4i4rg6ji9pibyl6gc716dyw7rkva6hcwr") (y #t)))

