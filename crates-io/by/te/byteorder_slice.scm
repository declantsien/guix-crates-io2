(define-module (crates-io by te byteorder_slice) #:use-module (crates-io))

(define-public crate-byteorder_slice-0.1.0 (c (n "byteorder_slice") (v "0.1.0") (h "1h9fbw04j50s9pbwhlam84kg3bfi5rkgrz17g6f5w73sdva9xkz2")))

(define-public crate-byteorder_slice-0.2.0 (c (n "byteorder_slice") (v "0.2.0") (d (list (d (n "byteorder") (r "1.*") (o #t) (d #t) (k 0)))) (h "1m217m6n63cyhx9crr6w7zr1zrcxpqchskvvcq659gn3ncg9ykb2") (f (quote (("default") ("byteorder_compat" "byteorder"))))))

(define-public crate-byteorder_slice-0.3.0 (c (n "byteorder_slice") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "1k2bb0zw2fgyrjf6xxvqssvrj1sq958saigppczvl2kwgrmkbh0n")))

(define-public crate-byteorder_slice-1.0.0 (c (n "byteorder_slice") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "00snh7di9zfhckyj95gxb7qdbwxhzxnmawd6gjlqgplyzdlmy8lp")))

(define-public crate-byteorder_slice-2.0.0 (c (n "byteorder_slice") (v "2.0.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "0qx11r3mxnkvycdnbbphs6f2kd3vjhv62ndkmgmhy3gavay04gxw")))

(define-public crate-byteorder_slice-3.0.0 (c (n "byteorder_slice") (v "3.0.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "141i06ka1kj04jn8q7pqbxhylc5r64hj9x7qif79ay3k70q4wa8b")))

