(define-module (crates-io by ew byewlma-macros) #:use-module (crates-io))

(define-public crate-byewlma-macros-0.1.0 (c (n "byewlma-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0drx6pdf5d4kc01hc2k9yjbih93kacd5izd7f3lyiwnc7bf9njqd")))

(define-public crate-byewlma-macros-0.1.1 (c (n "byewlma-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0n7gcply087piyakhyya8l076hjapxrpf6r6j0rp8lpfamzp8ks6")))

(define-public crate-byewlma-macros-0.1.3 (c (n "byewlma-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07viviahdcfql0fas67zny5vik09xniirfwiw06x0qrfmhawhcys")))

