(define-module (crates-io by ti bytify-impl) #:use-module (crates-io))

(define-public crate-bytify-impl-0.0.1 (c (n "bytify-impl") (v "0.0.1") (d (list (d (n "byteorder") (r "~1.2") (d #t) (k 0)) (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.4") (d #t) (k 0)) (d (n "syn") (r "~0.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12nw272zfqka239nmyyqf0fjsijvxfi58v3aaxi191w3ij533wd3") (f (quote (("default-big-endian") ("default"))))))

(define-public crate-bytify-impl-0.0.2 (c (n "bytify-impl") (v "0.0.2") (d (list (d (n "byteorder") (r "~1.2") (d #t) (k 0)) (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.4") (d #t) (k 0)) (d (n "syn") (r "~0.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zd29rdwhl5a6vvr3ncaq2vjg3ggxsbxcnhmwgab6l1j92ks8w4k") (f (quote (("default-big-endian") ("default"))))))

(define-public crate-bytify-impl-0.2.0 (c (n "bytify-impl") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10cslrx92mqgz76l5ndiv5b6mcjk9fs0qgr20yr2p2a96jl8w935") (f (quote (("default-big-endian") ("default"))))))

(define-public crate-bytify-impl-0.3.0 (c (n "bytify-impl") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00ayxsj942m9ffyc75dqmxdbffgivr5cyvi60nfbrzq898his685") (f (quote (("default-big-endian") ("default"))))))

(define-public crate-bytify-impl-0.3.1 (c (n "bytify-impl") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1q5x7v0gpi9bvn4r2xsn1bwrddjlyzg2a82rvh78h2icc1yv2s4z") (f (quote (("default-big-endian") ("default"))))))

