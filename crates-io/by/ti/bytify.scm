(define-module (crates-io by ti bytify) #:use-module (crates-io))

(define-public crate-bytify-0.0.1 (c (n "bytify") (v "0.0.1") (d (list (d (n "byteorder") (r "~1.2") (d #t) (k 2)) (d (n "bytify-impl") (r "= 0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "~0.4") (d #t) (k 0)))) (h "0vbal05sf77rlwwl27jafsn677jvim8p6i84z499x3n8a38rjzwh") (f (quote (("default-big-endian" "bytify-impl/default-big-endian") ("default"))))))

(define-public crate-bytify-0.0.2 (c (n "bytify") (v "0.0.2") (d (list (d (n "byteorder") (r "~1.2") (d #t) (k 2)) (d (n "bytify-impl") (r "= 0.0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "~0.4") (d #t) (k 0)))) (h "08s57ygnalmji928dvpd393cpybif9j8hf6yz2kkb2wc51an8kmn") (f (quote (("default-big-endian" "bytify-impl/default-big-endian") ("default"))))))

(define-public crate-bytify-0.2.0 (c (n "bytify") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)) (d (n "bytify-impl") (r "= 0.2.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "19ck7hn9jlcn9adps5zdanj1xm5k9y1akc4nd6sinp6nkacrrin4") (f (quote (("default-big-endian" "bytify-impl/default-big-endian") ("default"))))))

(define-public crate-bytify-0.3.0 (c (n "bytify") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "bytify-impl") (r "= 0.3.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1jd3wcppndrkdyvrn1k3fj5j2c2vjk7vxz3mpmxfz3a15nk80bl9") (f (quote (("default-big-endian" "bytify-impl/default-big-endian") ("default"))))))

(define-public crate-bytify-0.3.1 (c (n "bytify") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "bytify-impl") (r "= 0.3.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "105vbich72p68m43n6jjglc3kg3rnbr8935hv766f3ghm5py3abl") (f (quote (("default-big-endian" "bytify-impl/default-big-endian") ("default"))))))

(define-public crate-bytify-0.4.0 (c (n "bytify") (v "0.4.0") (d (list (d (n "typewit") (r "^1") (d #t) (k 0)))) (h "07l1m49ji5rhvaqipbvbqj7g0lzkfx5qjf94p3lxjhk4mddsi69p")))

(define-public crate-bytify-0.4.1 (c (n "bytify") (v "0.4.1") (d (list (d (n "typewit") (r "^1") (d #t) (k 0)))) (h "1g6gkslga3sd8745w9cwa5yqca3ckhzkiz674di0syc6sfj2bicj")))

