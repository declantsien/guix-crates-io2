(define-module (crates-io by on byond) #:use-module (crates-io))

(define-public crate-byond-0.1.0 (c (n "byond") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.19") (d #t) (k 0)))) (h "0z2c2yaqs3k9fwv0iaklf968zkq6pl2zm3bilpkal95cq8is2ap7")))

(define-public crate-byond-0.2.0 (c (n "byond") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "0vqr5nrrsmjzbjx66hc5ddh2d3qgdycr9d7kfx8iq9gqw3capidg")))

(define-public crate-byond-0.3.0 (c (n "byond") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "1nxl2jamsb5kl0f6z6j4ir2f57q5xbr3hpfmr8xf2wzyxi3gp170")))

(define-public crate-byond-0.3.1 (c (n "byond") (v "0.3.1") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "00i91acw1i3jmf9pw5zl985viz06lf90m71b6j83zai09rnc6n3v")))

(define-public crate-byond-0.3.2 (c (n "byond") (v "0.3.2") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "0av2xkvrih4rx5ifnfx0vfgykfbdbb1wlsyrslzn1h70nya9r4c1")))

(define-public crate-byond-0.4.0 (c (n "byond") (v "0.4.0") (h "0i52w5iynx26r2zksmprdlq66l3k47zb851f0hnvn62ray4prc2k")))

