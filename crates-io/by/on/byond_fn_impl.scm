(define-module (crates-io by on byond_fn_impl) #:use-module (crates-io))

(define-public crate-byond_fn_impl-0.1.0 (c (n "byond_fn_impl") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b1jia939i7vgx23xarczaszclapcvzzywmh2ryia481q08hgzss") (f (quote (("ffi_v2") ("allow_other_arch"))))))

(define-public crate-byond_fn_impl-0.2.0 (c (n "byond_fn_impl") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r2qdrpkarhp1i4n0icnphn76bdk4nrvwixdxrvfhwa11gr7gc26") (f (quote (("ffi_v2") ("allow_other_arch"))))))

(define-public crate-byond_fn_impl-0.4.0 (c (n "byond_fn_impl") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01833k9l5kcqynd4mj14lprbb8r44b3pqghfg1in9633plkdz99i") (f (quote (("ffi_v2") ("allow_other_arch"))))))

(define-public crate-byond_fn_impl-0.5.1 (c (n "byond_fn_impl") (v "0.5.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02f0cqns3ri5z2d5a5w2an1q4qavsxsn4xvs2jclsbmf4q0k8q81") (f (quote (("ffi_v2") ("allow_other_arch"))))))

