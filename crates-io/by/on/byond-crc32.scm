(define-module (crates-io by on byond-crc32) #:use-module (crates-io))

(define-public crate-byond-crc32-1.0.0 (c (n "byond-crc32") (v "1.0.0") (d (list (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "00i1f8l3v5cwijaywm9alsj1ay79ixvcl4w75q3rm69n3j3ya46d") (f (quote (("std") ("default" "std"))))))

(define-public crate-byond-crc32-1.1.0 (c (n "byond-crc32") (v "1.1.0") (d (list (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "060xvx1hwpw4vss5bcfbfxqar7d4py7d1i16z615h7i6455mvxz8") (f (quote (("std") ("default" "std"))))))

(define-public crate-byond-crc32-1.2.0 (c (n "byond-crc32") (v "1.2.0") (d (list (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "1kbjylqw5dl6q70qx48qd4fhlnw3wbvnhi1q2ndiv85hsijhfxgn") (f (quote (("std") ("default" "std"))))))

(define-public crate-byond-crc32-1.2.1 (c (n "byond-crc32") (v "1.2.1") (d (list (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "06qbfmpm9glx86xdchkdkwg49j69m4r7s4lbwqawad6r2gmq0rks") (f (quote (("std") ("default" "std"))))))

(define-public crate-byond-crc32-2.0.0 (c (n "byond-crc32") (v "2.0.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0hq4fqjd8qf7r8mfci563scwm1fz7v2w1mq2838g5jayscc0p4dm")))

(define-public crate-byond-crc32-3.0.0 (c (n "byond-crc32") (v "3.0.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0g9c0l4fllnvc140rrn1arnq11kpdv2az6k5kwm28ahsys0skdds") (f (quote (("std") ("default" "std"))))))

