(define-module (crates-io by on byondapi-macros) #:use-module (crates-io))

(define-public crate-byondapi-macros-0.1.0 (c (n "byondapi-macros") (v "0.1.0") (d (list (d (n "inventory") (r "^0.3.15") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1qx5h8rccxjq4mhg79wmsf5jvbm6lwaa3qh81h83gil8wawfq5fd")))

(define-public crate-byondapi-macros-0.1.1 (c (n "byondapi-macros") (v "0.1.1") (d (list (d (n "inventory") (r "^0.3.15") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "1ra91pmpqqfhwzl85wwcckc203a5adik4rsaf02b7i9n7wkg49xw")))

(define-public crate-byondapi-macros-0.1.2 (c (n "byondapi-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "16idg4b5c9xrgn8cajx2zry6viai6i8v9cc0cyp4ig2f7rm019pg")))

