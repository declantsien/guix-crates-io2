(define-module (crates-io by to bytom-rust-sdk) #:use-module (crates-io))

(define-public crate-bytom-rust-sdk-0.1.0 (c (n "bytom-rust-sdk") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.3") (d #t) (k 0)) (d (n "http") (r "^0.2.0") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("full"))) (d #t) (k 0)))) (h "1w9kzzlddmp0vqcwjsrs2qqmn2p1m73jlpdf41cpvn4fdj8fs6a5")))

