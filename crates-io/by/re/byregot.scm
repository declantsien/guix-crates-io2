(define-module (crates-io by re byregot) #:use-module (crates-io))

(define-public crate-byregot-0.2.0 (c (n "byregot") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "1gnjbbma9cyb502yv8aayj7b9q55r7jny2r7dr5w7klqy1dav0k3")))

(define-public crate-byregot-0.3.0 (c (n "byregot") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0zfz2x4vr0gwiv973s37apcpcvw6gy8vcsi807a7mj50l62432cp")))

(define-public crate-byregot-0.3.1 (c (n "byregot") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "021iyl5lzf0qjsmy357fhzrzgmaacc2az8aw612h92wfwi3xsj4y")))

(define-public crate-byregot-0.4.0 (c (n "byregot") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gbccbsy0mqkljiwxviis40i6q2syjpjamrny76dc6wpii3mlmpp")))

