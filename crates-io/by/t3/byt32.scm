(define-module (crates-io by t3 byt32) #:use-module (crates-io))

(define-public crate-byt32-0.1.0 (c (n "byt32") (v "0.1.0") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "~3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "16hkc2pjdnbacgwipbb78vfhs7wpmrx9pby89si1g34imxidm76r")))

(define-public crate-byt32-0.1.1 (c (n "byt32") (v "0.1.1") (d (list (d (n "aoko") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0ihqf91mydw0pfqfljgmkar9qcgmixp79yp1pix4jk9bhf0q91wx")))

(define-public crate-byt32-0.1.2 (c (n "byt32") (v "0.1.2") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "04n8h3ny01iz6bayych6ifkb6b07jxir7n4g38nqa399971jf3d5")))

(define-public crate-byt32-0.1.3 (c (n "byt32") (v "0.1.3") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0k4v3hz8xf6zk6d7yl6nmk38a6b6n7xxcmd1any1lxmclv76h19g")))

(define-public crate-byt32-0.1.4 (c (n "byt32") (v "0.1.4") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "ibig") (r "^0.3") (d #t) (k 0)))) (h "1jp8sywfbvmdy7ki0gaa11ysi82m7lwi8gbymnzxw6ra90q4923z")))

(define-public crate-byt32-0.1.5 (c (n "byt32") (v "0.1.5") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "ibig") (r "^0.3") (d #t) (k 0)))) (h "0aczvqnx7j36237hbn79l7r87lyj2a7syy0194vmx3ajjmyrxdmv")))

(define-public crate-byt32-0.1.6 (c (n "byt32") (v "0.1.6") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "ibig") (r "^0.3") (d #t) (k 0)))) (h "02f5psk3ydl0f68z52gx1l11cibl5dvnm067c923zq8v25isp9yl")))

(define-public crate-byt32-0.1.7 (c (n "byt32") (v "0.1.7") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "ibig") (r "^0.3") (d #t) (k 0)))) (h "0y9dkk9xxv3c1pgksmrwmdxlbi5pkx0d88csksc0wdi06hcdynny")))

