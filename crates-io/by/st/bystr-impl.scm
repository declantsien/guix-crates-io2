(define-module (crates-io by st bystr-impl) #:use-module (crates-io))

(define-public crate-bystr-impl-0.1.0 (c (n "bystr-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "042hyfij829kvg9a2cz57g4nqqf3yk3w364dckl01pgjip3lrqrg")))

(define-public crate-bystr-impl-0.1.2 (c (n "bystr-impl") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0rs52d95xn9z7a88fljq41yfnvb4brxc5035472q9cjrsii8zbwv")))

(define-public crate-bystr-impl-0.1.3 (c (n "bystr-impl") (v "0.1.3") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0bfsl5kjmxwf3pvnbv94q3938srxlygh6zlj9ijc8zwx2nxxpvi3")))

