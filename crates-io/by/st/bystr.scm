(define-module (crates-io by st bystr) #:use-module (crates-io))

(define-public crate-bystr-0.1.0 (c (n "bystr") (v "0.1.0") (d (list (d (n "bystr-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "12121drzrd5mwgap0yqbg3gr2h81rlmgdxpk1xq0dnm7k9hdqrj4")))

(define-public crate-bystr-0.1.1 (c (n "bystr") (v "0.1.1") (d (list (d (n "bystr-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "035n7vfi9grdf696vvig4fqvs10x847z7gdn0sp8pd273bycqzhn")))

(define-public crate-bystr-0.1.2 (c (n "bystr") (v "0.1.2") (d (list (d (n "bystr-impl") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "0sr2h86jq5gj4dvi1bs22rpi3jxy402zacfl4vn9nhs21j1ri3r2")))

(define-public crate-bystr-0.1.3 (c (n "bystr") (v "0.1.3") (d (list (d (n "bystr-impl") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "13sv7yql9fp1dqxrazajqpyvab0lp2gi0qb8lnhd1kg4zam7zdny")))

