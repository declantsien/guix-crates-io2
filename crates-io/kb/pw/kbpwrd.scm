(define-module (crates-io kb pw kbpwrd) #:use-module (crates-io))

(define-public crate-kbpwrd-0.1.0 (c (n "kbpwrd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "fs" "time" "macros"))) (d #t) (k 0)))) (h "05cjdvz9ljcyi3k77r9vn91dq4pvzqhd3mxf2q9k0i4s6jd5j1gk")))

(define-public crate-kbpwrd-0.1.1 (c (n "kbpwrd") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "fs" "time" "macros"))) (d #t) (k 0)))) (h "1ykmvlw2l768hvg035n7xahd4c4lk70i7d4m3dr9xdfwzbqxlsjy")))

(define-public crate-kbpwrd-0.1.2 (c (n "kbpwrd") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "fs" "time" "macros"))) (d #t) (k 0)))) (h "1fhsnr0kj4vyrpyfm2a8420v4334gjnrhxbnpac1bpkdcbqw8dx8")))

(define-public crate-kbpwrd-0.1.3 (c (n "kbpwrd") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "fs" "time" "macros"))) (d #t) (k 0)))) (h "1xyamh5k5a6x137csl3pqgqgc5y7646crqs9g159fk3mkmah4jhb")))

(define-public crate-kbpwrd-0.1.4 (c (n "kbpwrd") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "fs" "time" "macros"))) (d #t) (k 0)))) (h "19ignarc3qq7qlb4qccs1khlcp4axs9wzzsk9ad7csyk6rlgjici")))

(define-public crate-kbpwrd-0.1.5 (c (n "kbpwrd") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "fs" "time" "macros"))) (d #t) (k 0)))) (h "05mylyzpzlj05c695d8xwz6vc18nrjrq90wvlhp17zzp3jn6y7lj")))

(define-public crate-kbpwrd-0.1.6 (c (n "kbpwrd") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "fs" "time" "macros"))) (d #t) (k 0)))) (h "09sjnmqcjfawfxhl8d7nm462yhl39qhz3ap4m6l2zl6sxbjrxyg6")))

(define-public crate-kbpwrd-0.1.7 (c (n "kbpwrd") (v "0.1.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "fs" "time" "macros"))) (d #t) (k 0)))) (h "07njw1m90cgknsr57jf9mhxbh035w81k7q3dy2nj9hrm0jfjqk1a")))

(define-public crate-kbpwrd-0.1.8 (c (n "kbpwrd") (v "0.1.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "fs" "time" "macros"))) (d #t) (k 0)))) (h "191f8kiziqhcd750a2vmgdxklx2w6cj1vppqlyz88xkq1d73dbni")))

(define-public crate-kbpwrd-0.1.9 (c (n "kbpwrd") (v "0.1.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "fs" "time" "macros"))) (d #t) (k 0)))) (h "0gfmhbk2ngaxf7p3mjiqf4pnmhqvx7l2ypi451r9sjkw719pbv62")))

