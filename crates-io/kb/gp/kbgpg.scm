(define-module (crates-io kb gp kbgpg) #:use-module (crates-io))

(define-public crate-kbgpg-0.1.0 (c (n "kbgpg") (v "0.1.0") (d (list (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "env_logger") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.10") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)))) (h "07djk3gamdlwgsj4fczz7mxhaavzim45b2m6pfx9wi3df3a449a1")))

(define-public crate-kbgpg-0.1.1 (c (n "kbgpg") (v "0.1.1") (d (list (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "env_logger") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.10") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)))) (h "1p7dyz8vg1il0qq3v7anfh6j470nd0lp438s6i04x1i319j354fp")))

(define-public crate-kbgpg-0.1.2-pre (c (n "kbgpg") (v "0.1.2-pre") (d (list (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "env_logger") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.10") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)))) (h "1xr11ibmpainx9xas4vaqw40r3c8y7r2fcapnh2rdxmcc7avsmv3") (y #t)))

(define-public crate-kbgpg-0.1.2 (c (n "kbgpg") (v "0.1.2") (d (list (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "env_logger") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.10") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)))) (h "08lk6h1v46f9hqp8qav4i84h770a71cwzjs81c0cf2dfll734ds3")))

