(define-module (crates-io kb ar kbar) #:use-module (crates-io))

(define-public crate-kbar-0.0.1 (c (n "kbar") (v "0.0.1") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)))) (h "0rgiv3sl097l9rkngr9mx78da9kx6c1g9k9q4sx56vx8xcg73ncx") (f (quote (("default" "crossterm"))))))

(define-public crate-kbar-0.0.2 (c (n "kbar") (v "0.0.2") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)))) (h "07pw9j9wci9r0440m9w8kg11hd1aj5wl8595ckzx3225rmlhskr9") (f (quote (("default" "crossterm"))))))

(define-public crate-kbar-0.0.3 (c (n "kbar") (v "0.0.3") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)))) (h "0dwz3rfqkms1p2mfg6wq24ndcjvvipzbd78sgskfrxn091l09kc7") (f (quote (("default" "crossterm"))))))

(define-public crate-kbar-0.0.4 (c (n "kbar") (v "0.0.4") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)))) (h "107liy4adz5x9ks4vzg0zgsxxwhx7fq3psq095c7fbd44j4ada1m") (f (quote (("default" "crossterm"))))))

(define-public crate-kbar-0.1.0 (c (n "kbar") (v "0.1.0") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)))) (h "04ycmz2pfgl3srv35swxa0v6z4fvdnsc8ddyqkxfi1qzx9qa4sya") (f (quote (("default" "crossterm"))))))

(define-public crate-kbar-0.1.1 (c (n "kbar") (v "0.1.1") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)))) (h "07ajw9bjkl8b2akcbwalf94dxzm8dcnfax938ki13gl6j5b7sbsg") (f (quote (("default" "crossterm"))))))

(define-public crate-kbar-0.1.2 (c (n "kbar") (v "0.1.2") (d (list (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0q10h62fzmbrbshh2w82xl8pibb617l6hjbvl6nq4f112jk65gx5")))

(define-public crate-kbar-0.1.3 (c (n "kbar") (v "0.1.3") (d (list (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0jzx1x2lda3sg4y1z5lkayvdq3m51f252phvns639q07xql646gg")))

(define-public crate-kbar-0.1.5 (c (n "kbar") (v "0.1.5") (d (list (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0ypnl1xamb3748vrx5q4c0bc752ccnzp9zwxdw3zys5r3hdwivv6")))

