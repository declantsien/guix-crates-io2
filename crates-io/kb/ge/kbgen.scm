(define-module (crates-io kb ge kbgen) #:use-module (crates-io))

(define-public crate-kbgen-0.1.1 (c (n "kbgen") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05j3bbxsxg239xfdyvw0jl6pvqpyy2xw1ajr5wkipdxq62h0qnis")))

