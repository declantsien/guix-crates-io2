(define-module (crates-io kb rd kbrd) #:use-module (crates-io))

(define-public crate-kbrd-0.1.0 (c (n "kbrd") (v "0.1.0") (d (list (d (n "pasts") (r "^0.12") (d #t) (k 0)) (d (n "whisk") (r "^0.5") (f (quote ("pasts"))) (d #t) (k 0)))) (h "1qqmqappygkq0i83pp4khhrbdjasz5vzzrb8h2nrcm7qkcz306f8") (r "1.60")))

