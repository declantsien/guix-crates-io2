(define-module (crates-io kb pa kbparse-lib) #:use-module (crates-io))

(define-public crate-kbparse-lib-0.2.0 (c (n "kbparse-lib") (v "0.2.0") (h "1c8glq5cnbkrq9pbg3p2nqagiw3nxgxjihi3haq3shdfziiaxssd") (y #t)))

(define-public crate-kbparse-lib-0.3.0 (c (n "kbparse-lib") (v "0.3.0") (h "0x99f46f8slda4nayz18617zdqfiq15y5ddy5s9jszaarr49yz5g") (y #t)))

(define-public crate-kbparse-lib-0.3.1 (c (n "kbparse-lib") (v "0.3.1") (h "03ml8d5sqj7n5rf8ff58ql9bmmn1mimyn3v33a8vhpf1bs7cgl37")))

