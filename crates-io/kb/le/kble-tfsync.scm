(define-module (crates-io kb le kble-tfsync) #:use-module (crates-io))

(define-public crate-kble-tfsync-0.1.0 (c (n "kble-tfsync") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "kble-socket") (r "^0.1.0") (f (quote ("stdio" "tungstenite"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0g8zvzmg0223smf7v4ns8dd1xqnz7sb3pxr2dp4vw90sh79dg6rb")))

