(define-module (crates-io rd ns rdns) #:use-module (crates-io))

(define-public crate-rDNS-0.0.1 (c (n "rDNS") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.5") (d #t) (k 0)))) (h "0x0hr2rfdz7rq663n6y0d6vxmv13yks7d8rv3mf84kjl1qqy568y")))

