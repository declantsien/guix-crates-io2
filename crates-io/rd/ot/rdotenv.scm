(define-module (crates-io rd ot rdotenv) #:use-module (crates-io))

(define-public crate-rdotenv-0.1.0 (c (n "rdotenv") (v "0.1.0") (h "17hrx5s5b3a9gy9h3r71v7awyh2g14a6kvvz083s33gylm1xc6nm")))

(define-public crate-rdotenv-0.1.1 (c (n "rdotenv") (v "0.1.1") (h "1ajl4h8fy07dsccib6iagi6xasq3cgq49jsw0516cmi6a4rzwjir")))

(define-public crate-rdotenv-0.1.2 (c (n "rdotenv") (v "0.1.2") (h "18fk6i3krw41zhn9y0im8cf0y831rn1chvyaf40s1yrdcsv68k8y")))

(define-public crate-rdotenv-0.1.3 (c (n "rdotenv") (v "0.1.3") (h "1b8x2c5i47vg434p0jnzl5nh09s1kyfkzcrycc715vsj14llf1ss")))

(define-public crate-rdotenv-0.1.4 (c (n "rdotenv") (v "0.1.4") (h "11pbdmkh2cx57nxs3sbrz1z31xxljyx4k3hwyjppbvfidigsq41c")))

