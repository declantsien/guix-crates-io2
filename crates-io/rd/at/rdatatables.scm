(define-module (crates-io rd at rdatatables) #:use-module (crates-io))

(define-public crate-rdatatables-0.0.1 (c (n "rdatatables") (v "0.0.1") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w1k45w70ijbpykn51nm9bqpwp9gb7k5438v7jkwakz8zj18p7jw")))

(define-public crate-rdatatables-0.0.2 (c (n "rdatatables") (v "0.0.2") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g4lzib6gj69wk7q2jjxhmchlxv5y1vnwxc4s189sjyh60b7yw6b")))

(define-public crate-rdatatables-0.0.3 (c (n "rdatatables") (v "0.0.3") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jwggq2pgc330236dbgzbk8pvcv1987nr843wprqg8ldxmq937kh")))

(define-public crate-rdatatables-0.0.4 (c (n "rdatatables") (v "0.0.4") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k56zr1d9g68zmzfphisc4q992bf31pad3samlgk41d0wvw8iapb")))

(define-public crate-rdatatables-0.0.5 (c (n "rdatatables") (v "0.0.5") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02fil02i7li9pblw11mrp02mp6gci62i593i7wj2qm778b5ix14l")))

(define-public crate-rdatatables-0.0.6 (c (n "rdatatables") (v "0.0.6") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pazys64m4mlprxgvp19ck4s203k5l5w0z0llip180bmlzyirjsl")))

(define-public crate-rdatatables-0.0.7 (c (n "rdatatables") (v "0.0.7") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0klxwx834ipma0ardgz464m96zm1ylhxfnq00yf88xrclik9ixl7")))

(define-public crate-rdatatables-0.0.8 (c (n "rdatatables") (v "0.0.8") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fx8nw5ximg99kjykcw3vwscf8zqi4vs5wzs4z9y36jv874ldkbz")))

(define-public crate-rdatatables-0.0.9 (c (n "rdatatables") (v "0.0.9") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cmq9acy7krjs0yc51gg6b6y33kg8jq66jq3ngwrcc7h819jmrn1")))

(define-public crate-rdatatables-0.1.0 (c (n "rdatatables") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "110b0f14yixzqq3vngqsdcyqr8vnp4mqx7gw50km7avwfxis0gkl")))

