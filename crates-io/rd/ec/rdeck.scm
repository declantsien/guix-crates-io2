(define-module (crates-io rd ec rdeck) #:use-module (crates-io))

(define-public crate-rdeck-0.2.0 (c (n "rdeck") (v "0.2.0") (d (list (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 2)))) (h "13dr77kxsbki4yy34cfh6hv158rh9cz4zdvjyz0c2v93qj9nnin8")))

