(define-module (crates-io rd f_ rdf_translator) #:use-module (crates-io))

(define-public crate-rdf_translator-0.1.0 (c (n "rdf_translator") (v "0.1.0") (d (list (d (n "jsonpath") (r "^0.1.0") (d #t) (k 0)) (d (n "rdf") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.0") (d #t) (k 0)))) (h "163mx9pig513gkmwcg3s5fvbylmvj8j4m41fq63dq04h3g75d0x8")))

(define-public crate-rdf_translator-0.1.1 (c (n "rdf_translator") (v "0.1.1") (d (list (d (n "jsonpath") (r "^0.1.0") (d #t) (k 0)) (d (n "rdf") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.0") (d #t) (k 0)))) (h "1d9msb6mxh9zxlj4b6c2zmvlfjs0ybqc88rilijzp7mcbdwz29yy")))

(define-public crate-rdf_translator-0.1.2 (c (n "rdf_translator") (v "0.1.2") (d (list (d (n "jsonpath") (r "^0.1.0") (d #t) (k 0)) (d (n "rdf") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.0") (d #t) (k 0)))) (h "1rj8glrgs2p29xfcygw9jzj5rmdgpymi59zkqmyliw674phcq3d6")))

(define-public crate-rdf_translator-0.1.3 (c (n "rdf_translator") (v "0.1.3") (d (list (d (n "jsonpath") (r "^0.1.1") (d #t) (k 0)) (d (n "rdf") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "1irzfi5y6yqhjm999jbb254apk6n4a5p2mrncxwr5n39hdlkwip3")))

