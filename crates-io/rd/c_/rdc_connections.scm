(define-module (crates-io rd c_ rdc_connections) #:use-module (crates-io))

(define-public crate-rdc_connections-0.0.1 (c (n "rdc_connections") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 1)) (d (n "winsafe") (r "^0.0.6") (d #t) (k 0)))) (h "0prwbaknnh2pvzdb3bl8zkinxwzwf4kvqdrfcijaxf6jl248a7mh")))

(define-public crate-rdc_connections-0.0.2 (c (n "rdc_connections") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 1)) (d (n "winsafe") (r "^0.0.6") (d #t) (k 0)))) (h "1qy7m9ffvc27i3hkqahi0nja5g421wb1zkhyk9fc42irz9ghk104")))

(define-public crate-rdc_connections-0.0.3 (c (n "rdc_connections") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 1)) (d (n "winsafe") (r "^0.0.6") (d #t) (k 0)))) (h "134wmrbxac81xcmwi6kryxypvfqwqayz7pbbhrq9qs1v7gkq7gz4")))

(define-public crate-rdc_connections-0.0.4 (c (n "rdc_connections") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 1)) (d (n "winsafe") (r "^0.0.6") (d #t) (k 0)))) (h "10bhs7wy7b0731ckg3zqx8g0w88vcqy2d6d2p3qa8vqcgg66p8jr")))

(define-public crate-rdc_connections-0.0.5 (c (n "rdc_connections") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 1)) (d (n "winsafe") (r "^0.0.6") (d #t) (k 0)))) (h "0228xmkng14i4ib48m9z52ljsr9xninra7l2njawfa2v8iprpz22")))

(define-public crate-rdc_connections-0.0.6 (c (n "rdc_connections") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 1)) (d (n "winsafe") (r "^0.0.6") (d #t) (k 0)))) (h "0j0iiy7i4fqk923mcdhp895wpsl9bhdnw6a4n2r4x4mc674khyf9")))

(define-public crate-rdc_connections-0.0.7 (c (n "rdc_connections") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 1)) (d (n "winsafe") (r "^0.0.6") (d #t) (k 0)))) (h "1x3plivll0a4l4g4h3y69szj1zqdjz5i63l4gbsf0jph44vw1jc5")))

