(define-module (crates-io rd ma rdma-core-sys) #:use-module (crates-io))

(define-public crate-rdma-core-sys-0.0.1 (c (n "rdma-core-sys") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "042q0qzbswv3rdl73iv27ibn3rffzhypzszrgc3v9q25g9nip1mn")))

(define-public crate-rdma-core-sys-0.1.0 (c (n "rdma-core-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15kf4nd9xxhf3sl2bwr4i6xgxkz126rw21x2kmsdia1azf16xxs0")))

(define-public crate-rdma-core-sys-0.1.1 (c (n "rdma-core-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14mgjcdhbsgl0c9alfp9hh9cf5s4x0nyk6anvq1f9gv5gjlsajhc")))

(define-public crate-rdma-core-sys-0.1.2 (c (n "rdma-core-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08n6j16b0r8mpgzy552k43ickw55brlaxa5116b4kr6yh65y82ax")))

(define-public crate-rdma-core-sys-0.1.3 (c (n "rdma-core-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15qjiyf2lkk1msx0lr53abdba7v10w5y3c7b00qknaadpg7pvdb1")))

(define-public crate-rdma-core-sys-0.1.4 (c (n "rdma-core-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13ixr49ja6sdkin35kvq34zalqr0h8vjrqp8y1h7chfhm0h0q3ik")))

(define-public crate-rdma-core-sys-0.1.5 (c (n "rdma-core-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dmaah082bdwi44rm465jdgs7nscsfavfyqrs1403p0ciry1c9n1")))

(define-public crate-rdma-core-sys-0.1.6 (c (n "rdma-core-sys") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16w1mgaffdkmj7swz6k6sp8gada55kz8c2qwf4j78lrqac7h4gc8")))

(define-public crate-rdma-core-sys-0.1.7 (c (n "rdma-core-sys") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pcqf0pcxldlzb2zx86w68nfcp4n4jr9r3yavnwr7r8xgapvqc7m")))

(define-public crate-rdma-core-sys-0.1.8 (c (n "rdma-core-sys") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07570svmwxs55d9ji3ys1jb21f47b2jn54wwjxmn56hn7kmfin6j")))

(define-public crate-rdma-core-sys-0.1.9 (c (n "rdma-core-sys") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ai11jd3j7phirry3zgshmsm68m2flg884hjkm27jhm65bc0lw45")))

(define-public crate-rdma-core-sys-0.1.10 (c (n "rdma-core-sys") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pmzjh1qaxx1l8r26n21n0wrz0y30rxcxsdf8kj0nniiqf59pr3s")))

