(define-module (crates-io rd ma rdma-core) #:use-module (crates-io))

(define-public crate-rdma-core-0.0.1 (c (n "rdma-core") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.3.21") (d #t) (k 0)) (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rdma-core-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "rust-extra") (r "^0.0.13") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (k 0)))) (h "101vpmwhvhmfxg2dvg1wz6fxh9hin81wn84kyw1isvjn9l78dww7")))

