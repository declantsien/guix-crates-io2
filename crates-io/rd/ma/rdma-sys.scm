(define-module (crates-io rd ma rdma-sys) #:use-module (crates-io))

(define-public crate-rdma-sys-0.1.0 (c (n "rdma-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "162g60f3nr3ldr4sqsvrxcmxx4f6x8x87f5izss1l45ma1n63p3q")))

(define-public crate-rdma-sys-0.2.0 (c (n "rdma-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1b4lmc45zyikl7pzn19hkqs935vamksfspw2srq4ym0lmw0rckdi")))

(define-public crate-rdma-sys-0.3.0 (c (n "rdma-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19zr4h97imyzpmrq2n45c8syi6kb5z3ry9qlp98yvqfj49cz36gb")))

