(define-module (crates-io rd ir rdir-encoding) #:use-module (crates-io))

(define-public crate-rdir-encoding-0.1.0 (c (n "rdir-encoding") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1lnabwywwhl5i3195x8qdcw1f8c9h89p3x2cl7yyn4zr0ld7j7m7")))

