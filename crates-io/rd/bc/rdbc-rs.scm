(define-module (crates-io rd bc rdbc-rs) #:use-module (crates-io))

(define-public crate-rdbc-rs-0.1.0 (c (n "rdbc-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "async-std") (r "^1.11.0") (f (quote ("attributes" "default"))) (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "09qfyhwi1wddmjhs552lxw880z7a2cvqlg4h3cf41x4zwbc6ck42")))

