(define-module (crates-io rd bc rdbc) #:use-module (crates-io))

(define-public crate-rdbc-0.1.0 (c (n "rdbc") (v "0.1.0") (h "1q0agkwssgg81mf3ma7b7d7rz90wp5gbn7kyad2drn0fjzzcjpri")))

(define-public crate-rdbc-0.1.1 (c (n "rdbc") (v "0.1.1") (h "1irshrz8fgjdar5cl2wd59jl21pmpgypq12hxqcbwdhi3qm2ln0f")))

(define-public crate-rdbc-0.1.2 (c (n "rdbc") (v "0.1.2") (h "0maim1wka5w1xxrv1r5sxkfa1ch5sf1ach126i1a11rkgalzxjza")))

(define-public crate-rdbc-0.1.3 (c (n "rdbc") (v "0.1.3") (h "0almzpbqlsqqi2911gmz7rbz3pwar4cgh13ma59cam7ny6pq1m56")))

(define-public crate-rdbc-0.1.4 (c (n "rdbc") (v "0.1.4") (h "12kkkhm182xv9wlfisxg0s8sbrfjn5yg4168m9gczw1sfzdpdxlb")))

(define-public crate-rdbc-0.1.5 (c (n "rdbc") (v "0.1.5") (h "1fy9f0r9gijsifza88m52vmr5zaii688r855h7jsjd3rkb8hai21")))

(define-public crate-rdbc-0.1.6 (c (n "rdbc") (v "0.1.6") (h "1g50j67ypinv38sd5r1xz9ycd8dm0j83yliqkdsn5vmjbq6qcjkg")))

