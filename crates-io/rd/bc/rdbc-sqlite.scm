(define-module (crates-io rd bc rdbc-sqlite) #:use-module (crates-io))

(define-public crate-rdbc-sqlite-0.1.3 (c (n "rdbc-sqlite") (v "0.1.3") (d (list (d (n "fallible-streaming-iterator") (r "^0.1") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.21.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0vr8lmhlkzcyjflivpa24mhx7k3hg0b1agcljv4sv8q1nfaxasia")))

(define-public crate-rdbc-sqlite-0.1.5 (c (n "rdbc-sqlite") (v "0.1.5") (d (list (d (n "fallible-streaming-iterator") (r "^0.1") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.21.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "19sk34l3qyx8pw3m14xqn6gkn514y23jp4kir01pwkz07ac0iwxc")))

(define-public crate-rdbc-sqlite-0.1.6 (c (n "rdbc-sqlite") (v "0.1.6") (d (list (d (n "fallible-streaming-iterator") (r "^0.1") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.6") (d #t) (k 0)) (d (n "rusqlite") (r "^0.21.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1a70pz5q0y12dwfvgxyh64bamcgvgj4kzic6x2zjvv1y1sgg2wal")))

