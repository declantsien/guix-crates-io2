(define-module (crates-io rd bc rdbc-mysql) #:use-module (crates-io))

(define-public crate-rdbc-mysql-0.1.0 (c (n "rdbc-mysql") (v "0.1.0") (d (list (d (n "mysql") (r "^17.0.0") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.0") (d #t) (k 0)))) (h "1f227z5lf33vj3r8jdb8rbyj2svab91m585h2nqvi0nx1dfllkcx")))

(define-public crate-rdbc-mysql-0.1.1 (c (n "rdbc-mysql") (v "0.1.1") (d (list (d (n "mysql") (r "^17.0.0") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.1") (d #t) (k 0)))) (h "05lzw7xg4snc3dh6rwd93p1sa55wj88gpsn7ril1f52qrzyzqgbr")))

(define-public crate-rdbc-mysql-0.1.2 (c (n "rdbc-mysql") (v "0.1.2") (d (list (d (n "mysql") (r "^17.0.0") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.2") (d #t) (k 0)))) (h "17wdbb49zic8ip88fc52q1ysf9449vzgv21q9xjc2rwqa6qypcxs")))

(define-public crate-rdbc-mysql-0.1.3 (c (n "rdbc-mysql") (v "0.1.3") (d (list (d (n "mysql") (r "^17.0.0") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.3") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)))) (h "0w9qkc2ydqkm4f97i4pks9kscvvwrc6diy9i18n1jvyf27ksq5in")))

(define-public crate-rdbc-mysql-0.1.4 (c (n "rdbc-mysql") (v "0.1.4") (d (list (d (n "mysql") (r "^17.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.19.2") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.4") (d #t) (k 0)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)))) (h "0c2ccz12p22yamdvngch9cjqjxxd24bdfq3afa5if3hwx6kg6m0d")))

(define-public crate-rdbc-mysql-0.1.5 (c (n "rdbc-mysql") (v "0.1.5") (d (list (d (n "mysql") (r "^17.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.19.2") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.5") (d #t) (k 0)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)))) (h "03qyk9za8likzl6lbp9pz3n1jd8k4xsdhnpxlarx4p10sin83sgb")))

(define-public crate-rdbc-mysql-0.1.6 (c (n "rdbc-mysql") (v "0.1.6") (d (list (d (n "mysql") (r "^17.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.19.2") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.6") (d #t) (k 0)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)))) (h "1pli5cm99kymdgfk3hdq3w6506dr5xnx6vxcqcj31z1myagv8jvw")))

