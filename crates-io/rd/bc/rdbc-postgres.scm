(define-module (crates-io rd bc rdbc-postgres) #:use-module (crates-io))

(define-public crate-rdbc-postgres-0.1.0 (c (n "rdbc-postgres") (v "0.1.0") (d (list (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.0") (d #t) (k 0)))) (h "15vdlwrhq8qy5mxrb8q1a11vykp1mlv9k9q8qf4pgk3wfil9a090")))

(define-public crate-rdbc-postgres-0.1.1 (c (n "rdbc-postgres") (v "0.1.1") (d (list (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.1") (d #t) (k 0)))) (h "0gfp2nq4x25dxppi3vfs9zvrvppk7iib3hv2l19cv0dq2ncz6cyg")))

(define-public crate-rdbc-postgres-0.1.2 (c (n "rdbc-postgres") (v "0.1.2") (d (list (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.2") (d #t) (k 0)))) (h "17xhxm3kgygc88j2bnhxafzysla1fc54g21f0qj12bjxdc637chw")))

(define-public crate-rdbc-postgres-0.1.3 (c (n "rdbc-postgres") (v "0.1.3") (d (list (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.3") (d #t) (k 0)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)))) (h "0f2p9z1y8x7ii8gkakc1prx1z9hpxn3911qrjvzlkg5a8fbim36v")))

(define-public crate-rdbc-postgres-0.1.4 (c (n "rdbc-postgres") (v "0.1.4") (d (list (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.4") (d #t) (k 0)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)))) (h "0gnyzkh8y11j3x02gxkylz3g4larjw37pawdwzcw57aqhglllrdi")))

(define-public crate-rdbc-postgres-0.1.5 (c (n "rdbc-postgres") (v "0.1.5") (d (list (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.5") (d #t) (k 0)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)))) (h "0dznhgpkr3w6cpkax4k95kqgs9zcs33s1xxqq3ypyw9rzr95dlz7")))

(define-public crate-rdbc-postgres-0.1.6 (c (n "rdbc-postgres") (v "0.1.6") (d (list (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.6") (d #t) (k 0)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)))) (h "162bjn36604gxllbqpvaf2afmy4w3iq2crx9bd81a8p7ix320324")))

