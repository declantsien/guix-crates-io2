(define-module (crates-io rd bc rdbc-cli) #:use-module (crates-io))

(define-public crate-rdbc-cli-0.1.2 (c (n "rdbc-cli") (v "0.1.2") (d (list (d (n "rdbc") (r "^0.1.2") (d #t) (k 0)) (d (n "rdbc-mysql") (r "^0.1.2") (d #t) (k 0)) (d (n "rdbc-postgres") (r "^0.1.2") (d #t) (k 0)))) (h "1r7az40k59x2hdv2np6r5xa5vdmw4vabbc6js2h0azjf9rhdyaxh")))

(define-public crate-rdbc-cli-0.1.4 (c (n "rdbc-cli") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.4") (d #t) (k 0)) (d (n "rdbc-mysql") (r "^0.1.4") (d #t) (k 0)) (d (n "rdbc-postgres") (r "^0.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^4.1.0") (d #t) (k 0)))) (h "0mmpqsdk2p0nzwqpqazwjs2d9lvvqy4h6dd7isnc5ndniwlcah6a")))

(define-public crate-rdbc-cli-0.1.5 (c (n "rdbc-cli") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.5") (d #t) (k 0)) (d (n "rdbc-mysql") (r "^0.1.5") (d #t) (k 0)) (d (n "rdbc-postgres") (r "^0.1.5") (d #t) (k 0)) (d (n "rdbc-sqlite") (r "^0.1.5") (d #t) (k 0)) (d (n "rustyline") (r "^4.1.0") (d #t) (k 0)))) (h "1q1fh7c7cq4l8hk9vb63sq42q9vm3dfsj5g34pf7nq6vnl6m4glc")))

(define-public crate-rdbc-cli-0.1.6 (c (n "rdbc-cli") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rdbc") (r "^0.1.6") (d #t) (k 0)) (d (n "rdbc-mysql") (r "^0.1.6") (d #t) (k 0)) (d (n "rdbc-postgres") (r "^0.1.6") (d #t) (k 0)) (d (n "rdbc-sqlite") (r "^0.1.6") (d #t) (k 0)) (d (n "rustyline") (r "^4.1.0") (d #t) (k 0)))) (h "1h97yb198dm6qzaabd6y1biyjyy49zz48ns3cc5qf56v5jl5yaax")))

