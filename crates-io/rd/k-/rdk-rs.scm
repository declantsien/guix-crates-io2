(define-module (crates-io rd k- rdk-rs) #:use-module (crates-io))

(define-public crate-rdk-rs-0.1.0 (c (n "rdk-rs") (v "0.1.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "rdk-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09j1jm97q2y00nf7jhnvd91g9zcvg0p7ina88rifr2r0dm36djrc")))

