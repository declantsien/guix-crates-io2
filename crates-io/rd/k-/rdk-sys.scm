(define-module (crates-io rd k- rdk-sys) #:use-module (crates-io))

(define-public crate-rdk-sys-0.1.0 (c (n "rdk-sys") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "04x1srdp9xhz417mx3af5hw64xk943g951vb9709m7blqqbvai5v")))

(define-public crate-rdk-sys-0.1.1 (c (n "rdk-sys") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "0qwyn6a7gpyqh42bkfxb1vm191h2vbwvy8f2a193hcqzkvdap7wc") (f (quote (("conda"))))))

