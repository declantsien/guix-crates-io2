(define-module (crates-io rd s- rds-tensors) #:use-module (crates-io))

(define-public crate-rds-tensors-0.1.0 (c (n "rds-tensors") (v "0.1.0") (h "1l70daakv76jibgf895jkjar1nvwhs3jb83jxh7z94xv3vr95ymz")))

(define-public crate-rds-tensors-0.2.0 (c (n "rds-tensors") (v "0.2.0") (h "1bqpmd44c337s2yp6gqwy66zazvykk8kxnybsyy6i1w1j7ic9g21")))

