(define-module (crates-io rd fa rdfa-wasm) #:use-module (crates-io))

(define-public crate-rdfa-wasm-0.1.1 (c (n "rdfa-wasm") (v "0.1.1") (d (list (d (n "wasm-pack") (r "^0.12.1") (d #t) (k 0)))) (h "1hpc023xfjyplk7ymi7vn0f4q496q0p8j06kkyhjl4mprwfcv76a") (r "1.73")))

(define-public crate-rdfa-wasm-0.1.2 (c (n "rdfa-wasm") (v "0.1.2") (d (list (d (n "graph-rdfa-processor") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.88") (d #t) (k 0)))) (h "11cv5vrpggbr4492js0ql9gxc7r3f40w69kdamp40g0wjml7808r") (r "1.73")))

(define-public crate-rdfa-wasm-0.1.3 (c (n "rdfa-wasm") (v "0.1.3") (d (list (d (n "getrandom") (r "^0.2.10") (f (quote ("js"))) (d #t) (k 0)) (d (n "graph-rdfa-processor") (r "^0.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)))) (h "0pcafi8pk5zf4qrx04qhkl3ppik2ydc67n0fpsy06r4a2g14brjd") (r "1.73")))

