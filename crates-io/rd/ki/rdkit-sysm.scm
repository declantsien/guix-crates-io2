(define-module (crates-io rd ki rdkit-sysm) #:use-module (crates-io))

(define-public crate-rdkit-sysm-0.1.0 (c (n "rdkit-sysm") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "1z7xpifgsx1n4fgnz3jiagwjp2wj0fybblhn9iqkhl101kjnn0z5") (f (quote (("dynamic-linking-from-conda") ("default")))) (y #t)))

(define-public crate-rdkit-sysm-0.1.1 (c (n "rdkit-sysm") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0.69") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.69") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "0v85nr029s36xddfi5dj6xaad1bmnzrlikx3mj7ap8a19wx9s3q5") (f (quote (("dynamic-linking-from-conda") ("default")))) (y #t)))

(define-public crate-rdkit-sysm-0.1.2 (c (n "rdkit-sysm") (v "0.1.2") (d (list (d (n "cxx") (r "^1.0.69") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.69") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "1bg3ir30a25kskb0dm8lj880k1zgazskg0h2gww81zabwcxf39ym") (f (quote (("dynamic-linking-from-conda") ("default")))) (y #t)))

(define-public crate-rdkit-sysm-0.1.3 (c (n "rdkit-sysm") (v "0.1.3") (d (list (d (n "cxx") (r "^1.0.69") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.69") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "0rsrn7f7fxawfckiclln28r2z37fd39wi85fm2bjzb3rx6qh29nk") (f (quote (("dynamic-linking-from-conda") ("default")))) (y #t)))

(define-public crate-rdkit-sysm-0.1.4 (c (n "rdkit-sysm") (v "0.1.4") (d (list (d (n "cxx") (r "^1.0.69") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.69") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "1bv0rlfygfmj4vpzlpisghzs872adjrd45ni0isnzma0qk42gfmy") (f (quote (("dynamic-linking-from-conda") ("default")))) (y #t)))

