(define-module (crates-io rd ki rdkit-sys) #:use-module (crates-io))

(define-public crate-rdkit-sys-0.1.0 (c (n "rdkit-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1ffsnzs1d6xdzdgcl24jzw3acdyk0gcjq5hkmlw4hyk61gv3ww10")))

(define-public crate-rdkit-sys-0.1.1 (c (n "rdkit-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1z9hbqfnxqz96a5892jixw23cfqkfvbjf6bzpl4s81zgw1zqgk4h")))

(define-public crate-rdkit-sys-0.1.2 (c (n "rdkit-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "10zprk25s8gxlm821nsjp5hl2asrjiby0wpbw5jhgkbpmmgnmpqb")))

(define-public crate-rdkit-sys-0.1.3 (c (n "rdkit-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1bqk67fsh5wca36xj4xgrg5anri4b1w2y2a2cjp5jkmg07dy75al")))

(define-public crate-rdkit-sys-0.1.4 (c (n "rdkit-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "00sqxw0z9xifdlcy56nawl307rqr3plycdbfip5lj8zxcjw3b7bx")))

(define-public crate-rdkit-sys-0.1.5 (c (n "rdkit-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1bppclq27lidfgs8ncml5m6hm6znf5vnyx0qaq0wqgyf0jch636p")))

(define-public crate-rdkit-sys-0.1.6 (c (n "rdkit-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)) (d (n "eyre") (r "^0.6") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0b1lin9a71x67b451b6n13131kllnaiv16yq1k4qkxj7sq5n009g")))

(define-public crate-rdkit-sys-0.1.7 (c (n "rdkit-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)) (d (n "eyre") (r "^0.6") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1hwhslfs3yq057c3v4y8al88h03l23ah2xk18c4y18qcss37dc6q")))

(define-public crate-rdkit-sys-0.1.8 (c (n "rdkit-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)) (d (n "eyre") (r "^0.6") (d #t) (k 1)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1pnph70dww8mbg7ichnmi6azrf52jy7dc8dqlx82sg38g3qnvqvp")))

(define-public crate-rdkit-sys-0.1.10 (c (n "rdkit-sys") (v "0.1.10") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)) (d (n "eyre") (r "^0.6") (d #t) (k 1)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0q0qibysvgbma37jk77qj61rdwy8j9dg717mlaahlafbvdv3njcg")))

(define-public crate-rdkit-sys-0.1.11 (c (n "rdkit-sys") (v "0.1.11") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)) (d (n "eyre") (r "^0.6") (d #t) (k 1)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0k4q85vsbjvfln457198ybshcljkwyq66v4prqkislfhklg7ks8c")))

(define-public crate-rdkit-sys-0.1.12 (c (n "rdkit-sys") (v "0.1.12") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)) (d (n "eyre") (r "^0.6") (d #t) (k 1)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wsgf7c54ggjy6lmimzlld1g2qpsrami73k2ck7assg7wnmxpjx9")))

(define-public crate-rdkit-sys-0.2.0 (c (n "rdkit-sys") (v "0.2.0") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1gpb1yq1m1ij0ysd8qkrcbh1633lcl7ahab40n9fbylpngwx7c1l")))

(define-public crate-rdkit-sys-0.2.1 (c (n "rdkit-sys") (v "0.2.1") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)))) (h "0q1xrz9j8cjxg0qf1i6cplirj7fm5zmr1s30c0khxr7rq9kkbnf2")))

(define-public crate-rdkit-sys-0.2.2 (c (n "rdkit-sys") (v "0.2.2") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)))) (h "15dgjw49vr2gpzcb6qyfjy0gcngfi2rjhjkdlgr19pna24ajy52n")))

(define-public crate-rdkit-sys-0.2.3 (c (n "rdkit-sys") (v "0.2.3") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)))) (h "0nf34p6nfyn2hfkpg9608wnmarw446xd8h4lk9d4bjp8s9hi8wp4")))

(define-public crate-rdkit-sys-0.2.4 (c (n "rdkit-sys") (v "0.2.4") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)))) (h "0ray658zrsfh3lvhki4b78ixgx3dl446f11vg8j2680f0knmwdjw")))

(define-public crate-rdkit-sys-0.2.5 (c (n "rdkit-sys") (v "0.2.5") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)))) (h "0iww58x6hd87rng83jbckx4yphg99lphzvkqc28l3mb2l5nyp2y8")))

(define-public crate-rdkit-sys-0.2.6 (c (n "rdkit-sys") (v "0.2.6") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)))) (h "0ydli07l66z3zlgxljc08f5j8xn7jpn4lq4xcn1xsh4dqqh9d6gf")))

(define-public crate-rdkit-sys-0.2.7 (c (n "rdkit-sys") (v "0.2.7") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)))) (h "1p5yf4zmg4pm82bq53mls3gkfv5pj73img631nfv5mid2qmdvfwp")))

(define-public crate-rdkit-sys-0.2.8 (c (n "rdkit-sys") (v "0.2.8") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)))) (h "15xx5s621fqylbl2fnwd4msv7icapfy520p61gdnr6n7qsn3j90c")))

(define-public crate-rdkit-sys-0.2.9 (c (n "rdkit-sys") (v "0.2.9") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)))) (h "0ixvgl1alih3p4kfprmf4nzrlz66sm4g425jdbdbiqqd5dzd22fr")))

(define-public crate-rdkit-sys-0.2.10 (c (n "rdkit-sys") (v "0.2.10") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0") (d #t) (k 1)))) (h "19xwl0sdy17afqz3kj8f3y6az6bmfiwr4i4iqmp21a0fbbgvwxl7")))

(define-public crate-rdkit-sys-0.2.12 (c (n "rdkit-sys") (v "0.2.12") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "0szj7c4yiz5lvzkv4yzwi70kz63wxywy4ys6n73kc17wnigaaxkx") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.2.13 (c (n "rdkit-sys") (v "0.2.13") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "0gijxwp0lzj34hr93w5972i5gmm2v12rrir6fa7lhazak3g2kxzn") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.2.14 (c (n "rdkit-sys") (v "0.2.14") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "1w3i629b9h5ddpgxkx2r4ilplrbpvsb56w40jqwnkpkd1ny5b6as") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.2.15 (c (n "rdkit-sys") (v "0.2.15") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "0syvginv28gb8lc2ki557a3q1q6r66whhpk6x5rdicjk6w6v646q") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.2.16 (c (n "rdkit-sys") (v "0.2.16") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "108c2hlg3vysg5zlj327qg9q7025d56zawyyjhwvf75lna9jxqyh") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.2.17 (c (n "rdkit-sys") (v "0.2.17") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "028whbznim61y3cdbxivj4sws94ivbx192pw5mha4rxbmzdmn9h7") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.2.18 (c (n "rdkit-sys") (v "0.2.18") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "09cbijpm1kkz5jyvcsps7kzivcfx5gdfzy2wn79v82sszrqiw5rw") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.2.19 (c (n "rdkit-sys") (v "0.2.19") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "0x6f6p8711b8s9v3qgbwicv561c3kdck84ivc5pjq6qk0m7cibvb") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.2.20 (c (n "rdkit-sys") (v "0.2.20") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "0x63cf6xn5qgxlf8ah4iadn4p1y5ry0ippaiah5lcp9b1lyf0bqs") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.2.21 (c (n "rdkit-sys") (v "0.2.21") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "08mpd5dpbgblnb9s17s10kywqs6c7056bcgayax0nrl6ji8l422y") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.2.22 (c (n "rdkit-sys") (v "0.2.22") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "0ah5z62w7pgr0igbv1zv97x5ms0bcqsf3sqc1l0jy959jg7jjvll") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.3.0 (c (n "rdkit-sys") (v "0.3.0") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.67") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 1)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "0cvjwf930g9vkd514ld00b3ivikzvryhssiwr0ph9zmciw3w01n1") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.4.0 (c (n "rdkit-sys") (v "0.4.0") (d (list (d (n "cxx") (r "^1.0.109") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.109") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 1)) (d (n "which") (r "^4.4.2") (d #t) (k 1)))) (h "1nj22majb3fmaf54nspc97fk7w5lgjs2k1liviapifdi3kxqb8nm") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.4.1 (c (n "rdkit-sys") (v "0.4.1") (d (list (d (n "cxx") (r "^1.0.109") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.109") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 1)) (d (n "which") (r "^4.4.2") (d #t) (k 1)))) (h "19wfy7971b36j3mngi1s801nfpyn601aail6h5d3dpkvdsib7z5h") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.4.3 (c (n "rdkit-sys") (v "0.4.3") (d (list (d (n "cxx") (r "^1.0.109") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.109") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 1)) (d (n "which") (r "^4.4.2") (d #t) (k 1)))) (h "09jf7w58nnbihgvb4361fxdf3pg65bbfzisan4nig05sc2x48bgg") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.4.4 (c (n "rdkit-sys") (v "0.4.4") (d (list (d (n "cxx") (r "^1.0.109") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.109") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 1)) (d (n "which") (r "^4.4.2") (d #t) (k 1)))) (h "155q8ppd1mqsq2986il771rxp5fxfkzpm9vjcbs7vp2dz2jvk7bc") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.4.6 (c (n "rdkit-sys") (v "0.4.6") (d (list (d (n "cxx") (r "^1.0.109") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.109") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 1)) (d (n "which") (r "^4.4.2") (d #t) (k 1)))) (h "0gzfsrh2v3ar0l79vnhnayiiwz64j5pg854w8cwns8grqzjk02ix") (f (quote (("dynamic-linking-from-conda") ("default"))))))

(define-public crate-rdkit-sys-0.4.7 (c (n "rdkit-sys") (v "0.4.7") (d (list (d (n "cxx") (r "^1.0.109") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.109") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 1)) (d (n "which") (r "^4.4.2") (d #t) (k 1)))) (h "1l1mivj2bry1sa7yz8spvfsixc8vq7g1y8s5kcd9gqaphryjpbzq") (f (quote (("dynamic-linking-from-conda") ("default"))))))

