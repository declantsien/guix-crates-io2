(define-module (crates-io rd ki rdkit-rs) #:use-module (crates-io))

(define-public crate-rdkit-rs-0.1.0 (c (n "rdkit-rs") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "rdkit-ffi") (r "^0.1.0") (d #t) (k 0)))) (h "1h50snlb55h6z0b5qnwi21559bi6p1hs6vhpn3ipvj7zh2i7fl1i")))

