(define-module (crates-io rd ki rdkit) #:use-module (crates-io))

(define-public crate-rdkit-0.1.0 (c (n "rdkit") (v "0.1.0") (d (list (d (n "rdkit-sys") (r "^0.1.12") (d #t) (k 0)))) (h "1d28xsgidizspbp07dl9xrffh5gc4593yiblspgzkz3l0506lb6d")))

(define-public crate-rdkit-0.1.1 (c (n "rdkit") (v "0.1.1") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0mrimn64mm2pw6lxz5zj7ysxpdr345f3ny1lx895djxn4w75a6lk")))

(define-public crate-rdkit-0.2.0 (c (n "rdkit") (v "0.2.0") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0fbymlajh82ga935z8w4z3db72ik89fn2sdaq3nda1yrj11gpc0j")))

(define-public crate-rdkit-0.2.1 (c (n "rdkit") (v "0.2.1") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.4") (d #t) (k 0)))) (h "071ba5h2wf3p0ygbz8gzindf4yijrkq554m0avmhg3whkryl1yk8")))

(define-public crate-rdkit-0.2.2 (c (n "rdkit") (v "0.2.2") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.5") (d #t) (k 0)))) (h "11f40477jmbfq5pbf7s26zbg3kjb4m705nplripijan0yk38fv85")))

(define-public crate-rdkit-0.2.3 (c (n "rdkit") (v "0.2.3") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.6") (d #t) (k 0)))) (h "1cd9jcdiiqidbnb12c9sg80hml7a7077212ih8i5wjd8w7dxqqfa")))

(define-public crate-rdkit-0.2.4 (c (n "rdkit") (v "0.2.4") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.7") (d #t) (k 0)))) (h "0hszn469q10id13l1fxk71zr521ssiy899kghjg65d6b7g34dlfp")))

(define-public crate-rdkit-0.2.5 (c (n "rdkit") (v "0.2.5") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.7") (d #t) (k 0)))) (h "0ajfxrsp3wpbgay2f9mm8n4bfm21bb3258adrjrz2hjbk45kn1v7")))

(define-public crate-rdkit-0.2.6 (c (n "rdkit") (v "0.2.6") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.7") (d #t) (k 0)))) (h "14svgrbn530lp2fjrpyvq6777bbq7y64a1myy4sv0himlxsznja6")))

(define-public crate-rdkit-0.2.7 (c (n "rdkit") (v "0.2.7") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.9") (d #t) (k 0)))) (h "05ywi129wn57gg1qa7idcalk5s4kdjjkmb1mfad7q53gk11vxzz4")))

(define-public crate-rdkit-0.2.8 (c (n "rdkit") (v "0.2.8") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.9") (d #t) (k 0)))) (h "0ippcr3v8gha3p58a04z7fxqrfja1k9argx1f1dc568lka7xz1i8")))

(define-public crate-rdkit-0.2.9 (c (n "rdkit") (v "0.2.9") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.9") (d #t) (k 0)))) (h "10whfrjfm0y63irj0pq42l6ypcj32hq0pcd0in71l08swim2yqj5")))

(define-public crate-rdkit-0.2.10 (c (n "rdkit") (v "0.2.10") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.9") (d #t) (k 0)))) (h "12njlwnhsxgws6m5pqpni4i64j1lhjrgwl9pvjg25y0w76f12shx")))

(define-public crate-rdkit-0.2.11 (c (n "rdkit") (v "0.2.11") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.14") (d #t) (k 0)))) (h "0vbsszcam8ask2xysympnzlzkjw4a13kcwqlz6lrzbcdxcbadkrr")))

(define-public crate-rdkit-0.2.12 (c (n "rdkit") (v "0.2.12") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.15") (d #t) (k 0)))) (h "0bg69gsgk5gvfqrwifwmnrj867swwzp0ybhgj55r4q51yn2ns7py")))

(define-public crate-rdkit-0.2.13 (c (n "rdkit") (v "0.2.13") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.16") (d #t) (k 0)))) (h "0rah1rq7g5prmn9l4jj3rndf33dzx297lj9vk9smmdzqbdvbgri7")))

(define-public crate-rdkit-0.2.15 (c (n "rdkit") (v "0.2.15") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.2.21") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "108ysy4986rxjiqlhks6wf3ph9q2kzb4nw7395g4sq9qxqif67dh")))

(define-public crate-rdkit-0.3.0 (c (n "rdkit") (v "0.3.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "11hh4n31db7gvwk0kpic4jaxgzgcpfp2zcisp7nb38nj9f7zr1gh")))

(define-public crate-rdkit-0.3.1 (c (n "rdkit") (v "0.3.1") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cny3cjh7p8z7q7ywrn3cxq06l1dryam2m2jvjgzd4cffzfhgyrd")))

(define-public crate-rdkit-0.4.0 (c (n "rdkit") (v "0.4.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10wjwnc82qc71wsafvgs9gycdqjy5ymnpanlrgpxlmj3pgb253c7")))

(define-public crate-rdkit-0.4.1 (c (n "rdkit") (v "0.4.1") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "197ngc7qsldcm34hg7pn7wfh360q6wb2glqq6l7awzi3vz1xszx3")))

(define-public crate-rdkit-0.4.2 (c (n "rdkit") (v "0.4.2") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qanszbrzkyz92hxqz4a6j20j8kybdp5b29ja9ax1l48s3jai86y")))

(define-public crate-rdkit-0.4.3 (c (n "rdkit") (v "0.4.3") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0mvyk1cxsi5rdfqi2iksvjlxcgl328lzknih1656dzmb0w4n6b4k")))

(define-public crate-rdkit-0.4.4 (c (n "rdkit") (v "0.4.4") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "16ha20dxqdvlb8hrn93vs5mf72jww70dl965ph3wqbyf21v3qayq")))

(define-public crate-rdkit-0.4.6 (c (n "rdkit") (v "0.4.6") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0507n531mf9h75jbck21s4g3sc9y0q5hzb58iq3drn2m8rksia6j")))

(define-public crate-rdkit-0.4.7 (c (n "rdkit") (v "0.4.7") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rdkit-sys") (r "^0.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0c7pcag6k9yn4s0kzcj7i2lvp7cj14dpszmkkdi0xjvf1favx6rl")))

