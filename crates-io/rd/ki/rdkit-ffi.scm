(define-module (crates-io rd ki rdkit-ffi) #:use-module (crates-io))

(define-public crate-rdkit-ffi-0.1.0+rdkit-2022-09-1-pre (c (n "rdkit-ffi") (v "0.1.0+rdkit-2022-09-1-pre") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0gsaqxnk3hxw535v3hdfixzybpnvp0h8wa5pdk2ipr968nlg7b0x")))

