(define-module (crates-io rd xl rdxl_scaffolding) #:use-module (crates-io))

(define-public crate-rdxl_scaffolding-0.0.1 (c (n "rdxl_scaffolding") (v "0.0.1") (d (list (d (n "rdxl") (r "^0.3") (d #t) (k 0)))) (h "0x6ckn1hx5bw4vz1zzhgnbs0jm9aq1mkg84h3b430z5yfg9rnfaz")))

(define-public crate-rdxl_scaffolding-0.0.2 (c (n "rdxl_scaffolding") (v "0.0.2") (d (list (d (n "rdxl") (r "^0.3") (d #t) (k 0)))) (h "03n1aq9xbv5zza94xnjq8vvpyi5vwxyqpas1xwsnd8igp0zdyd6l")))

(define-public crate-rdxl_scaffolding-0.0.3 (c (n "rdxl_scaffolding") (v "0.0.3") (d (list (d (n "rdxl") (r "^0.3") (d #t) (k 0)))) (h "02sv65xss972w51p64162dna9msf4kynvafkdmfh4r9ibxsj2vp5")))

(define-public crate-rdxl_scaffolding-0.0.4 (c (n "rdxl_scaffolding") (v "0.0.4") (d (list (d (n "rdxl") (r "^0.3.10") (d #t) (k 0)))) (h "1gm1dmnjj1axxvg120v177y5kr96jw2vshs0v9g5w2dz3p7z7knc")))

(define-public crate-rdxl_scaffolding-0.0.5 (c (n "rdxl_scaffolding") (v "0.0.5") (d (list (d (n "rdxl") (r "^0.3.11") (d #t) (k 0)))) (h "1pc6x6dvvvkbg5ifvc3m1qys0kbq2g2pzk3vh1ahn7fixlc6jz35")))

(define-public crate-rdxl_scaffolding-0.0.6 (c (n "rdxl_scaffolding") (v "0.0.6") (d (list (d (n "rdxl") (r "^0.3.11") (d #t) (k 0)))) (h "0spz3phkd1kvy2cnayc4yrb5m87b0f3a7b1p0y42nm5062sh57jd")))

(define-public crate-rdxl_scaffolding-0.0.7 (c (n "rdxl_scaffolding") (v "0.0.7") (d (list (d (n "rdxl") (r "^0.3.12") (d #t) (k 0)))) (h "0nczy7lkdfjmsicaxir1pr3cvpcb40jidgsqrp7s8d6ysbkf886k")))

(define-public crate-rdxl_scaffolding-0.0.8 (c (n "rdxl_scaffolding") (v "0.0.8") (d (list (d (n "rdxl") (r "^0.3.12") (d #t) (k 0)))) (h "0306n3l5b18qnjwhdy3iz1p0bqnxkckvd8qc19zdl50kycr3dzmm") (f (quote (("debug_html"))))))

(define-public crate-rdxl_scaffolding-0.0.9 (c (n "rdxl_scaffolding") (v "0.0.9") (d (list (d (n "rdxl") (r "^0.3.12") (d #t) (k 0)))) (h "1v9fcjnqq2ajx2ga2y0d79ay125k8453zn97w6zgvv06vm1d5si2") (f (quote (("debug_html"))))))

(define-public crate-rdxl_scaffolding-0.0.10 (c (n "rdxl_scaffolding") (v "0.0.10") (d (list (d (n "rdxl") (r "^0.3.12") (d #t) (k 0)))) (h "0bg8hkkwhq90jjpalmcwmr6v7ricw8r1d7gk8bar72xf8a707sx9") (f (quote (("debug_html"))))))

(define-public crate-rdxl_scaffolding-0.0.11 (c (n "rdxl_scaffolding") (v "0.0.11") (d (list (d (n "rdxl") (r "^0.4") (d #t) (k 0)))) (h "181aq5hm7hga90zkg8gyllpkls250nrq0p2nij38s6i4n2ibbx6x") (f (quote (("debug_html"))))))

(define-public crate-rdxl_scaffolding-0.0.12 (c (n "rdxl_scaffolding") (v "0.0.12") (d (list (d (n "rdxl") (r "^0.4.6") (d #t) (k 0)))) (h "1r39ka99qdkiybi1hs9j3jx43fxgcl3kbjpn9a9qa070la1j4gw1") (f (quote (("debug_html"))))))

(define-public crate-rdxl_scaffolding-0.0.13 (c (n "rdxl_scaffolding") (v "0.0.13") (d (list (d (n "rdxl") (r "^0.4.6") (d #t) (k 0)))) (h "132hp26acy184lx5nlnjp071blg7fcf7rjjvzlda1m274gcwxx6s") (f (quote (("debug_html"))))))

(define-public crate-rdxl_scaffolding-0.0.14 (c (n "rdxl_scaffolding") (v "0.0.14") (d (list (d (n "rdxl") (r "^0.4.6") (d #t) (k 0)))) (h "1ih40xglvhm7svqcm37mdb4d8dpfqpmhfzzznvbkkarp5v43pl2x") (f (quote (("debug_html"))))))

(define-public crate-rdxl_scaffolding-0.0.15 (c (n "rdxl_scaffolding") (v "0.0.15") (d (list (d (n "rdxl") (r "^0.4.6") (d #t) (k 0)))) (h "1ndid58axfj60p6hwgb7ia7zj2jb9a0jp7sg5rqbza6philzlnw0") (f (quote (("debug_html"))))))

(define-public crate-rdxl_scaffolding-0.0.16 (c (n "rdxl_scaffolding") (v "0.0.16") (d (list (d (n "rdxl") (r "^0.4.6") (d #t) (k 0)))) (h "0wf6mqgwf10j1vn3y15r3gvwd5h46rmifw0jsmbimmqyah1c21p1") (f (quote (("debug_html"))))))

(define-public crate-rdxl_scaffolding-0.0.17 (c (n "rdxl_scaffolding") (v "0.0.17") (d (list (d (n "rdxl") (r "^0.4.9") (d #t) (k 0)))) (h "0dcsl5vpyd5yzgpyy52f16jwp1k8i1q0mx1ma99cfr8237y50sj0")))

(define-public crate-rdxl_scaffolding-0.0.18 (c (n "rdxl_scaffolding") (v "0.0.18") (d (list (d (n "rdxl") (r "^0.4.10") (d #t) (k 0)))) (h "07chvwqr8sc7smxk66mn4cf7pqyf39faa23yj3lnyv7xv9kgxjkx")))

(define-public crate-rdxl_scaffolding-0.0.19 (c (n "rdxl_scaffolding") (v "0.0.19") (d (list (d (n "rdxl") (r "^0.4.12") (d #t) (k 0)))) (h "1lnp2nwgln9xvgvf58d5v3g38ls0s7xzz25c3m44givv0dhccd7s")))

(define-public crate-rdxl_scaffolding-0.0.20 (c (n "rdxl_scaffolding") (v "0.0.20") (d (list (d (n "rdxl") (r "^0.4.13") (d #t) (k 0)))) (h "1jgf6f9hgakvihh5ckpwd3r5x5szgvyqp0gnrcdc05gzwbvcfgig")))

(define-public crate-rdxl_scaffolding-0.0.21 (c (n "rdxl_scaffolding") (v "0.0.21") (d (list (d (n "mxml") (r "^0.1.2") (d #t) (k 0)) (d (n "rdxl") (r "^0.4.13") (d #t) (k 0)))) (h "1p68kp4akw9zjpmq5fik72l4rxr1ranhk0a6wvc865dwrgs13nzc")))

(define-public crate-rdxl_scaffolding-0.0.22 (c (n "rdxl_scaffolding") (v "0.0.22") (d (list (d (n "mxml") (r "^0.1.2") (d #t) (k 0)) (d (n "rdxl") (r "^0.5.2") (d #t) (k 0)))) (h "088mjd37gccw40jdb4kdkbfdqpy251l6m0k8fi9wl9k6nxwqpqvp")))

(define-public crate-rdxl_scaffolding-0.0.24 (c (n "rdxl_scaffolding") (v "0.0.24") (d (list (d (n "mxml") (r "^0.1.2") (d #t) (k 0)) (d (n "rdxl") (r "^0.5.4") (d #t) (k 0)))) (h "17s8cbpzpznslmlsczqdzb1min9vycy0d5b2y7qz6y8gmdqh4qbk")))

(define-public crate-rdxl_scaffolding-0.1.0 (c (n "rdxl_scaffolding") (v "0.1.0") (d (list (d (n "mxml") (r "^0.1.2") (d #t) (k 0)) (d (n "rdxl") (r "^0.5.5") (d #t) (k 0)))) (h "1wj5383bhmpjj57mdyz09f96vafr0yk03zlksqfw22lsp6wam2gm")))

(define-public crate-rdxl_scaffolding-0.1.1 (c (n "rdxl_scaffolding") (v "0.1.1") (d (list (d (n "mxml") (r "^0.1.2") (d #t) (k 0)) (d (n "rdxl") (r "^0.5.5") (d #t) (k 0)))) (h "04s746nnrhzmf2cd0v3x7vcbq20bv5jx65fln6z0qb8kw18i49sa") (y #t)))

