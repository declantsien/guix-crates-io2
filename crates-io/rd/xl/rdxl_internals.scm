(define-module (crates-io rd xl rdxl_internals) #:use-module (crates-io))

(define-public crate-rdxl_internals-0.0.1 (c (n "rdxl_internals") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gk557ygkzcaqr7g5k7fzcih4gxyif51g1maxzqbanjyw2clazhk") (y #t)))

(define-public crate-rdxl_internals-0.0.2 (c (n "rdxl_internals") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v0bkk65w9h6dsz87vsfz6db75di93g1js075kyi7gm4f509jz16") (y #t)))

(define-public crate-rdxl_internals-0.0.3 (c (n "rdxl_internals") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gdzvvm9ysg33r16wf0kj6la31sa65bl9rpf3xyndna1nry2x4n4") (y #t)))

(define-public crate-rdxl_internals-0.0.4 (c (n "rdxl_internals") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sna6jb9phmqbq991rsw7kxgd6sgwrfqjqrdlgfxhbpzxnzfl6pl") (y #t)))

(define-public crate-rdxl_internals-0.0.5 (c (n "rdxl_internals") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a3rd58fnpzv2cch3vd5qkrkdrlanxsr3xsx1plc0y8hjfdm623l") (y #t)))

(define-public crate-rdxl_internals-0.0.6 (c (n "rdxl_internals") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r23sc1mqq1aamqgcgsgkp9aakr1k0nyih35h04k885l6g64xqv4") (y #t)))

(define-public crate-rdxl_internals-0.0.7 (c (n "rdxl_internals") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l5gp2zh03ipbq4i1vh46d902dmya005hmd2hlpw7k4ds1mp4ng2") (y #t)))

(define-public crate-rdxl_internals-0.0.8 (c (n "rdxl_internals") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "18vxddjbsdnsq69pw2gynvl96sn8j1i62b2vagvfa27fxy5i97ky") (y #t)))

(define-public crate-rdxl_internals-0.0.9 (c (n "rdxl_internals") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "06jh4in4yanrk7x3kxzpbhbd7zij8843ls032cad9wg722bq7amj") (y #t)))

(define-public crate-rdxl_internals-0.0.10 (c (n "rdxl_internals") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "07qwzg17laab4vpfgqj0fh84w3mzqzyvrj4mb97h6ngcn57b45nl") (y #t)))

