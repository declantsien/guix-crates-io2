(define-module (crates-io rd xl rdxl_atom) #:use-module (crates-io))

(define-public crate-rdxl_atom-0.0.1 (c (n "rdxl_atom") (v "0.0.1") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)))) (h "0qb6f90xsxrb80ha0p9pl83wvfd5y0cawzw9zjzzrg0zqcc3868c") (y #t)))

(define-public crate-rdxl_atom-0.0.2 (c (n "rdxl_atom") (v "0.0.2") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)))) (h "18rfyix56zrni5arq0j0pxwln326bb42wx6dm1q230f2v0pwpwg7") (y #t)))

