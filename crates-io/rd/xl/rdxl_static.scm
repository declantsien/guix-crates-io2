(define-module (crates-io rd xl rdxl_static) #:use-module (crates-io))

(define-public crate-rdxl_static-0.0.1 (c (n "rdxl_static") (v "0.0.1") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)))) (h "00kz0g1rpk1gn7j3k8xnk19b36k1j9zsq0ppi3n9msrxy5n4db67")))

(define-public crate-rdxl_static-0.0.2 (c (n "rdxl_static") (v "0.0.2") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)))) (h "0id5cia9dxh0pwdxy38gkp07jq4791bvmwgsxw3pv2mx3nxkv8fm")))

(define-public crate-rdxl_static-0.0.3 (c (n "rdxl_static") (v "0.0.3") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)))) (h "0gc5qmzbcnzri7h5w236sxmiss76wlhbrbg2avkqja1f7amw34fr")))

(define-public crate-rdxl_static-0.0.4 (c (n "rdxl_static") (v "0.0.4") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.1") (d #t) (k 0)))) (h "1zf9bsbgrjq9fx1shkqqmkyzq3h94imw2ilqqy7y5ry8rh1rfgaf")))

(define-public crate-rdxl_static-0.0.5 (c (n "rdxl_static") (v "0.0.5") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.2") (d #t) (k 0)))) (h "0dnp9q4za028s7lgcb4i9rkrwzjq2jynp49xcsf40qj4gj7557gv")))

(define-public crate-rdxl_static-0.0.6 (c (n "rdxl_static") (v "0.0.6") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.2") (d #t) (k 0)))) (h "0ngypk5n69nfwx309asmd90rx0wfrf19if3p8lzbq1f813lx2mlm")))

(define-public crate-rdxl_static-0.0.7 (c (n "rdxl_static") (v "0.0.7") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.2") (d #t) (k 0)))) (h "1h7cj1b11l89c5crr9yz7cx1c79kvpjv47v3xk2z2h45q1vp4f24")))

(define-public crate-rdxl_static-0.0.8 (c (n "rdxl_static") (v "0.0.8") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.3") (d #t) (k 0)))) (h "0d8xizxslnwd53d2hj90cwkg1mr91afc2yh7v0c0d5yaqzzj3spv")))

(define-public crate-rdxl_static-0.0.9 (c (n "rdxl_static") (v "0.0.9") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "129mnc0224ynxsxmmnqz410dp883jhm5rf12wan64kp2c4xdn838")))

(define-public crate-rdxl_static-0.0.10 (c (n "rdxl_static") (v "0.0.10") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01ay6mm8qayb8wz6c6hwg6n2i2c0j3fy0jgz3pppv4mhbbli5izp")))

(define-public crate-rdxl_static-0.0.11 (c (n "rdxl_static") (v "0.0.11") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vhjynimfdda8y821lg6fbnxzhq4ihpsksbdc0mw0p5s2hcvzn78")))

(define-public crate-rdxl_static-0.0.12 (c (n "rdxl_static") (v "0.0.12") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d8wagzr9g14jdr3d4dwxa1n6rybl1izpxcc0nn57sn6imxn36yy")))

(define-public crate-rdxl_static-0.0.13 (c (n "rdxl_static") (v "0.0.13") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19r6i53z8s7rgl1s35fq1dgkq93kcbfc0535i92s9jmpqk8zncnf")))

(define-public crate-rdxl_static-0.0.14 (c (n "rdxl_static") (v "0.0.14") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jc6gg68b3mrhhgg5hq84w2hswny5ml612wxjjl8ai4r7na6fmvp")))

(define-public crate-rdxl_static-0.0.15 (c (n "rdxl_static") (v "0.0.15") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cl3jdam0x4r16phzzkfxp1hn97ss7vjpizai0x85gnppisyn2xd")))

(define-public crate-rdxl_static-0.0.16 (c (n "rdxl_static") (v "0.0.16") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i19r74kk8kykgp670xlhdb00nbmcmb71sm5n2g499y5z7i3bd3y")))

(define-public crate-rdxl_static-0.0.17 (c (n "rdxl_static") (v "0.0.17") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xb7ihpbzg2c07j783jmx8qpgkqvgv824dpf36ipwhrcbkzi70wn")))

(define-public crate-rdxl_static-0.0.18 (c (n "rdxl_static") (v "0.0.18") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "171gak0csiqp1yyy86y3wwphf92pc17kf9ynvc894dlsfrqqdxx2")))

(define-public crate-rdxl_static-0.0.19 (c (n "rdxl_static") (v "0.0.19") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12gpypp51l94g1nkj90q94f1fmygh6ypcwxdayrvx249k64ninjb")))

(define-public crate-rdxl_static-0.0.20 (c (n "rdxl_static") (v "0.0.20") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s7llymn2m9y4ksm2ym9lmhzd7xj0dd8caa681yqp8w1zsbs7l5g")))

(define-public crate-rdxl_static-0.0.21 (c (n "rdxl_static") (v "0.0.21") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fgq76kn4sj9k8y7cp14i6bqlz817d34zwp94c7sxjrcw0hjpadc")))

(define-public crate-rdxl_static-0.0.22 (c (n "rdxl_static") (v "0.0.22") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0js0qcikgi4nian1gxyhxxx4ykh0qw1aw07zqvw3inmkx6ngx2pg")))

(define-public crate-rdxl_static-0.0.23 (c (n "rdxl_static") (v "0.0.23") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gvc65wc2zvhpxp7jaj9xxifjs2p0bxdzg6yls73zgjgzvi62dmg")))

(define-public crate-rdxl_static-0.0.24 (c (n "rdxl_static") (v "0.0.24") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vnq0vi4y326qxr07f98cpl0x87aij8j97a424ql5lwlh2wnfxyd")))

(define-public crate-rdxl_static-0.0.25 (c (n "rdxl_static") (v "0.0.25") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r95kxism5p78xbqnrlsyyiwgay8maz66wz1vlbqnyf28dj3dd9m")))

(define-public crate-rdxl_static-0.0.26 (c (n "rdxl_static") (v "0.0.26") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v2cv9vw8wn577z79icq11f4zjmdk2dnjny50wcijw58fvh6744m")))

(define-public crate-rdxl_static-0.0.27 (c (n "rdxl_static") (v "0.0.27") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zf1bf4008fxh0pbv449bfhw9rzdwm3b229v2cbl8vfafn301jy8")))

(define-public crate-rdxl_static-0.0.28 (c (n "rdxl_static") (v "0.0.28") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19dp1jx5kiff8s9x5l09n0x1d7a0ma6zqj73mvn8yv435zyx779l")))

(define-public crate-rdxl_static-0.0.29 (c (n "rdxl_static") (v "0.0.29") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sfyw88yqj9l834zxfxmx71jh4za73lh375m9cvfa3dljdq577gq")))

(define-public crate-rdxl_static-0.0.30 (c (n "rdxl_static") (v "0.0.30") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "191wndf7k4p9988ky9y5znlh1pdqm1sshz1rk2sjiq6cs6ylnnsr")))

(define-public crate-rdxl_static-0.0.31 (c (n "rdxl_static") (v "0.0.31") (d (list (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09sm0csfxsjz05iq4a1d1azr5xn4h0xlifbw2bnhha3crfkkbmlk")))

(define-public crate-rdxl_static-0.0.32 (c (n "rdxl_static") (v "0.0.32") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hqgrgylfhj1xyj37ah95qciqm485s986i3xhcbjrk94l12z663n")))

(define-public crate-rdxl_static-0.0.33 (c (n "rdxl_static") (v "0.0.33") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pwxvqsaw9qi481h1gw9jb5d2g88c3shwzgfalqj8pcz5zxjczma")))

(define-public crate-rdxl_static-0.0.35 (c (n "rdxl_static") (v "0.0.35") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "115qqv87cswzbwh8f5dmpw07qdv20c1cawjmfwphq3ixv1m4hjxz")))

(define-public crate-rdxl_static-0.0.36 (c (n "rdxl_static") (v "0.0.36") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.11") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fmip5a5kjxgxlvgnl70sy6bk1qbpjvx7ajkw6lynp8xdj4m3103")))

(define-public crate-rdxl_static-0.0.37 (c (n "rdxl_static") (v "0.0.37") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.12") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wbk7gmjhnwn5jldv4mzhc29iay3zrplg7qabwqgh1vw1sj4n0yl")))

(define-public crate-rdxl_static-0.0.38 (c (n "rdxl_static") (v "0.0.38") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "rdxl") (r "^0.5") (d #t) (k 0)) (d (n "rdxl_static_macros") (r "^0.0.13") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g416jgfdly8n8gsa2plavx8zniyc62af097k65418bv4bf726iy")))

