(define-module (crates-io rd xl rdxl) #:use-module (crates-io))

(define-public crate-rdxl-0.1.0 (c (n "rdxl") (v "0.1.0") (h "13fj1mqg7cpk1cm0qj2vf4hvli5x5zzqlj0r02rghar1c1pj26sm") (y #t)))

(define-public crate-rdxl-0.1.1 (c (n "rdxl") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i82d4gnlx9ih7zknyw2q0wdcdv0416q3nr9hphj6586c31fznkm") (y #t)))

(define-public crate-rdxl-0.1.2 (c (n "rdxl") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gavjf03gh2w8qj4jblgjixqyg1x8dmj2irj0axjzq4ymikky815") (y #t)))

(define-public crate-rdxl-0.1.3 (c (n "rdxl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12y82fj37r8fggd6val8ndgqpm8x5q9mgavvz39zgs3mxlj8nx6n") (y #t)))

(define-public crate-rdxl-0.1.4 (c (n "rdxl") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1w0dnrbrrhgx0g0k97lsy7sllg6sfbrf4yw5v94hfxpyblclgd4j") (y #t)))

(define-public crate-rdxl-0.1.5 (c (n "rdxl") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d6is4j6g6b2rd6cvx9y7wzcg7mw50zr746jjcg4z6i41al5m8dz") (y #t)))

(define-public crate-rdxl-0.1.6 (c (n "rdxl") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ihpigsnw74cb2mji8d9h6xrckzjnr4zf93hpimlbb6nkxpygadb") (y #t)))

(define-public crate-rdxl-0.1.7 (c (n "rdxl") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s4piafn3jnx4zggcblwmvycmq8b7wysrx020a0cc3jkbmn1039w") (y #t)))

(define-public crate-rdxl-0.1.8 (c (n "rdxl") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09qzc72zhn5nbpjnwyannv5jhkjj40lncgfgvn3583kzlp9609vn") (y #t)))

(define-public crate-rdxl-0.1.9 (c (n "rdxl") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01dh7790g3rxlvlp4f2wghzdbv6xbfnrgyj964i4wi8pfrqq4ix5") (y #t)))

(define-public crate-rdxl-0.1.10 (c (n "rdxl") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17k7s7gzy8c0in32gzhhbb3k1fkl3ipcqs1i540cxkn937ffxkwp") (y #t)))

(define-public crate-rdxl-0.1.11 (c (n "rdxl") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x8fkbw8w3rhnfmbr65ycwcbj02hjfb1p444j6nr60lnz6fknwvc") (y #t)))

(define-public crate-rdxl-0.1.12 (c (n "rdxl") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k7lxdg2gsvk1f5nzxja5ykvs3lhw1bldbqrg383dfq0mvrlaijc") (y #t)))

(define-public crate-rdxl-0.1.13 (c (n "rdxl") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d74val8r4x1vh533ksik9bwqx9pbbki3bgd30annxg7xdy7qbdw") (y #t)))

(define-public crate-rdxl-0.1.14 (c (n "rdxl") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1svqy2s7snbmvxfhna625i7zb8kr8clmwnxwfjj5z75i6fyjzmqc") (y #t)))

(define-public crate-rdxl-0.1.15 (c (n "rdxl") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04xwii4r14kfk8437xa2s574wdc0ik6p8fcil9grd1iviz1p3ch0") (y #t)))

(define-public crate-rdxl-0.1.16 (c (n "rdxl") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f0f2c7sk81f7zv0pzdvkccadiwhc6mjwxf45qfmirjpk83hk3fv") (y #t)))

(define-public crate-rdxl-0.1.17 (c (n "rdxl") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dfxazs44c5z5i795yf6w530hgb8769s0dbgckamxxr6azs7s18h") (y #t)))

(define-public crate-rdxl-0.1.18 (c (n "rdxl") (v "0.1.18") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d3an2qgq1rb445ag8z2ls2zykg04msdqwf4y7jj46b91i8376zl") (y #t)))

(define-public crate-rdxl-0.1.19 (c (n "rdxl") (v "0.1.19") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z7gnkyimq2rnlilk5l438plmxrf65k8xv8i4cdb5hsvsxsnxdai") (y #t)))

(define-public crate-rdxl-0.2.0 (c (n "rdxl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11ybx6n8w24pyk8sf7snin4sp0rknn7ygf6jfdg46ljqsad2m7ax") (y #t)))

(define-public crate-rdxl-0.2.1 (c (n "rdxl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hrigzk0dr0v8bs95q5m8wh7q5r8jkv8cxpi7nr21g3qwx7cyfbv") (y #t)))

(define-public crate-rdxl-0.2.2 (c (n "rdxl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hkk4y1inb4q9hqlxrxk5ljwnzrv5c6bpjf0hlmw73s8mygsx64f") (y #t)))

(define-public crate-rdxl-0.2.3 (c (n "rdxl") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hcf167kxw7mrn74pyr42hmrslyd681lvxpyxmhshrwspvk7dkf6") (y #t)))

(define-public crate-rdxl-0.2.4 (c (n "rdxl") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fyqkc6a08rycpdfi4aqgj76majh4q6vxdqhkd1m90wj4ang5gjd") (y #t)))

(define-public crate-rdxl-0.3.0 (c (n "rdxl") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06krq7n5q2as9mw0hh8rq5xvjjvsi669bjb5a62rf2d7dq4j34pd") (y #t)))

(define-public crate-rdxl-0.3.1 (c (n "rdxl") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rgkxaq6b9q874fv9ajfx230nh6iqnawdzsq90gjz9d7mw7gnm29") (y #t)))

(define-public crate-rdxl-0.3.2 (c (n "rdxl") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vxvf4fnkcx0nv3l15gjvxss9ljsp6nhmbkkfnhvn8g44av3wp4w") (y #t)))

(define-public crate-rdxl-0.3.3 (c (n "rdxl") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00i9jhlfxybaaj2m5i0cn2h40sq53xqv6migi0lzlfdzjsmbic81") (y #t)))

(define-public crate-rdxl-0.3.4 (c (n "rdxl") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lycj93ph8z5mqs54asx2lqy204sjbxzb3nvgb6j0jmyiyandns0") (y #t)))

(define-public crate-rdxl-0.3.5 (c (n "rdxl") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jcb6rr10i8y9ff1cvcss81gbijl3r5fsl06fb7sgmirzxbhm7b2") (y #t)))

(define-public crate-rdxl-0.3.6 (c (n "rdxl") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03r9h635gssdnwhdlpmf2cbd6qsfykcrlgig6n15cvi3s0371rgi") (y #t)))

(define-public crate-rdxl-0.3.7 (c (n "rdxl") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v2shrk96y6lplgdgpj9ycg9y49h4xgflcacj2hpjx2hlivk4zq9") (y #t)))

(define-public crate-rdxl-0.3.8 (c (n "rdxl") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rn80kyvw5snxrx1f2qg4khf79kbgm4hxf9prqjvsfd883ppqzzh") (y #t)))

(define-public crate-rdxl-0.3.9 (c (n "rdxl") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f38snqrdfgxmvzq3bz8jxmnqgb6s97nkabyhdbcpgpw8cxbgl8a") (y #t)))

(define-public crate-rdxl-0.3.10 (c (n "rdxl") (v "0.3.10") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pbnr0108a8308z9zndfyd8x1fkvvdydiqrk6qhcycwyj7fd378i") (y #t)))

(define-public crate-rdxl-0.3.11 (c (n "rdxl") (v "0.3.11") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jhdxqi0k4gv22b3p2ninfgkq147p0p8vg2aflsv9siwf4xsbkkx") (y #t)))

(define-public crate-rdxl-0.3.12 (c (n "rdxl") (v "0.3.12") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h6isvls9q1xdx90h7644824a02yjfl3r60r1gv35hvi4gpan9pb") (y #t)))

(define-public crate-rdxl-0.3.13 (c (n "rdxl") (v "0.3.13") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f9j9alp5izf9whpwv9qihxzzvvmhmx4b3r9cnqfcxip1mhmlqj0") (y #t)))

(define-public crate-rdxl-0.3.14 (c (n "rdxl") (v "0.3.14") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lfcq97h38jnlmvdamjkcz6pfvvmx7n9cjj18vmi3jkm5c45ya66") (y #t)))

(define-public crate-rdxl-0.3.15 (c (n "rdxl") (v "0.3.15") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gqbw6hrd0q4izy0hb7zq8z4q35czflfmx5c6fp2a9p6d5nbmypw") (y #t)))

(define-public crate-rdxl-0.4.0 (c (n "rdxl") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "175sx07c4wzw6gfzwmn9zgzfl3ahia1rm4g3p5gwaza44ssy615v") (y #t)))

(define-public crate-rdxl-0.4.1 (c (n "rdxl") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pvr03psdyvsgva3jgvziwwh4vd64rvhb1w9ggqz3iq1rb2jm69f") (y #t)))

(define-public crate-rdxl-0.4.2 (c (n "rdxl") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bwdg20qyzpml0zb35019kg1bln6idkpk4lvs2ydfgcndk66k9m3") (y #t)))

(define-public crate-rdxl-0.4.3 (c (n "rdxl") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18zwwsibsk24r3g4z1na46xlscfx28gazx3zzxabj9x4jgqlzcl3") (y #t)))

(define-public crate-rdxl-0.4.4 (c (n "rdxl") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "191w04g5mqxff42xsnwb6czn9l6z3ql042zxh609r1zj42izx5v5") (y #t)))

(define-public crate-rdxl-0.4.5 (c (n "rdxl") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0267iwcmspd6pn1rlq350x0kd5i4a37n2kfdfhm5xdnlzjmy0aaq") (y #t)))

(define-public crate-rdxl-0.4.6 (c (n "rdxl") (v "0.4.6") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1928m8hakdfi3hkixdqwz94l8abx9rqdylk5gjfdg5f3702s3v4v") (y #t)))

(define-public crate-rdxl-0.4.7 (c (n "rdxl") (v "0.4.7") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pfkh065cxy3abq632r7v5123s1hcjvi1kq5wv6xgkmd438ndkwv") (y #t)))

(define-public crate-rdxl-0.4.8 (c (n "rdxl") (v "0.4.8") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j1n75a4i3xbpvcyqnhgaxhn3yhx3fisp7fb2z1p48x28ls0hkv5") (y #t)))

(define-public crate-rdxl-0.4.9 (c (n "rdxl") (v "0.4.9") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6s19cczsxqld4y1xzplb8y4l1mk87mvbyvqbv8v04sxgj25703") (y #t)))

(define-public crate-rdxl-0.4.10 (c (n "rdxl") (v "0.4.10") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wqxd6h4hz9wyj3aancjy11z2wf8sy10ziiv8i2hr7vnhqr2ybcp") (y #t)))

(define-public crate-rdxl-0.4.11 (c (n "rdxl") (v "0.4.11") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08fw2v6w30raqqi6kmlkg45zqkkzk2ms0190nr5ygi9icfzkb0lq") (y #t)))

(define-public crate-rdxl-0.4.12 (c (n "rdxl") (v "0.4.12") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j34dafq0p8r8yv3pwym88wfpa6wi9b24ym22w84q86gyz67bjff") (y #t)))

(define-public crate-rdxl-0.4.13 (c (n "rdxl") (v "0.4.13") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01mp4yqs07wlbn40z9axhl6akwp6cq55hc8gnrszd8y2y1r23cbz") (y #t)))

(define-public crate-rdxl-0.4.14 (c (n "rdxl") (v "0.4.14") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02j2v65ba24zscwv1nk6ar9icrasn4krlv2s1wfzc9zyh9yqwks8") (y #t)))

(define-public crate-rdxl-0.4.15 (c (n "rdxl") (v "0.4.15") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d69l6749216dir301cm9786xcas4n8bma8m7h615x1nzfibag2d") (y #t)))

(define-public crate-rdxl-0.4.16 (c (n "rdxl") (v "0.4.16") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08xca6papc3vf1j6cvm32clsn7yfd77815i2m63axzfi0233x9ll") (y #t)))

(define-public crate-rdxl-0.4.17 (c (n "rdxl") (v "0.4.17") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dkr6c3sp05p938b891ai15b5i6ikpz8x0hlnhg3mipbjskm25ry") (y #t)))

(define-public crate-rdxl-0.5.0 (c (n "rdxl") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12yhl9djm1d4ir2m6vlycr1jrsah1b5z38vgpfd21s46lmyhbyin") (y #t)))

(define-public crate-rdxl-0.5.1 (c (n "rdxl") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gd990d0slrn6c5c7lf7xhnk38dblzzgr5jr724yi3kpsfqdl26g") (y #t)))

(define-public crate-rdxl-0.5.2 (c (n "rdxl") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "055phh4cc7l9xkkq0wh7m7rxnjhh48gvqhvl00y2bff62hn87xzm") (y #t)))

(define-public crate-rdxl-0.5.3 (c (n "rdxl") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0friyyxfcrvkzyf4m0lcvm894xirr3awbms0wmscplwmnabvxdwf") (y #t)))

(define-public crate-rdxl-0.5.4 (c (n "rdxl") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vy1hd34vprw3g0lnjsfgmbd0kna3j14dxdavjzayiic4r3q66ya") (y #t)))

(define-public crate-rdxl-0.5.5 (c (n "rdxl") (v "0.5.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wlkbfkmhz1mij8xsri6ypyr7c0a973pgg7n9qsfzfk445x8a34c") (y #t)))

(define-public crate-rdxl-0.5.6 (c (n "rdxl") (v "0.5.6") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11dcrz96irrfmlr7ql6aq04phi7dafbvpgmgjf64lj90j1gxf343") (y #t)))

(define-public crate-rdxl-0.5.8 (c (n "rdxl") (v "0.5.8") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15zzwj7jmpcrj8yjpy0qhmmhnrdyx9mz05y7lhffc2s85f7b7vhi") (y #t)))

(define-public crate-rdxl-0.5.9 (c (n "rdxl") (v "0.5.9") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sx6fl75m0padrkm7vwjr555729yz7k21dvky4533i9rdzrxhjq8") (y #t)))

(define-public crate-rdxl-0.5.10 (c (n "rdxl") (v "0.5.10") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lkyvj3xkgkgswp9jipgmh40xiciwvjd07w4g374yyvwllgm1q7w") (y #t)))

(define-public crate-rdxl-0.5.11 (c (n "rdxl") (v "0.5.11") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i7f6cbnyg08ihh096db7fi9vh8jzk28km801wbxcvzs0ib5fkxm") (y #t)))

(define-public crate-rdxl-0.5.12 (c (n "rdxl") (v "0.5.12") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pxhsvl3xxaq3gva1s0jzyw91f3797v0nd1ns5drz6m7v4cipl0j") (y #t)))

(define-public crate-rdxl-0.5.13 (c (n "rdxl") (v "0.5.13") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10qfkfnhlr750bnlz3lcyb17rh8920fx71y1ykqsa9r73smiz246") (y #t)))

(define-public crate-rdxl-0.5.14 (c (n "rdxl") (v "0.5.14") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xs8s6kzp6145lgz7cw93r3hncw1xgsna2cs04yi8pjwz8lysg7r") (y #t)))

(define-public crate-rdxl-0.5.15 (c (n "rdxl") (v "0.5.15") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lnra0aridlj8drkw6k3xm3n25pgsddh9am17z8alzf09b1igqyx") (y #t)))

(define-public crate-rdxl-0.5.18 (c (n "rdxl") (v "0.5.18") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06lybdnvl39zjggg4bwzn1qjbz8n7v5jrvsmlm3zl3y0wvdir8kp") (y #t)))

(define-public crate-rdxl-0.5.19 (c (n "rdxl") (v "0.5.19") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02rcv8kinvdw6pdkvr31p0kw9pfkyj0blamwyrfv43v31n4b0sc3") (y #t)))

(define-public crate-rdxl-0.5.20 (c (n "rdxl") (v "0.5.20") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "147qrhw9w07yn93wj7x4gqcgkzlm4shmcvmm37g0wayz2j6ca99j") (y #t)))

(define-public crate-rdxl-0.5.21 (c (n "rdxl") (v "0.5.21") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13xcchaykcciviajiy2dkg8ca494r9s7dq4mbipl2aj84bn7mahi") (y #t)))

(define-public crate-rdxl-0.5.22 (c (n "rdxl") (v "0.5.22") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1naq115qbi5jm766irc9ky6jwlfhi5c4mi3z6gzpj58dpcz8ily0") (y #t)))

(define-public crate-rdxl-0.5.23 (c (n "rdxl") (v "0.5.23") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k9rmldkfjbv92yxy0fra95c7lqy3a0g74vl5r2zd4xjd6r2vw08") (y #t)))

(define-public crate-rdxl-0.5.24 (c (n "rdxl") (v "0.5.24") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g70zck97j4nz8v5k5aj6pzc3k2lv5x6klz0sjj2nvvg94sb87zn") (y #t)))

(define-public crate-rdxl-0.5.25 (c (n "rdxl") (v "0.5.25") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gabbljglk5y4b0acg2fk6z6ks3f9hfb15r3syw5np65nlyij1xd") (y #t)))

(define-public crate-rdxl-0.5.26 (c (n "rdxl") (v "0.5.26") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rdxl_internals") (r "^0.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xdz7snh3d38yic1dvf1pgh4rylhycsgb3whldinsk5hqp5kpj8j") (y #t)))

