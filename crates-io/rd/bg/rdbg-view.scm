(define-module (crates-io rd bg rdbg-view) #:use-module (crates-io))

(define-public crate-rdbg-view-0.1.0 (c (n "rdbg-view") (v "0.1.0") (d (list (d (n "rdbg-client") (r "^0.1.0") (d #t) (k 0)))) (h "1f8jixni4vchr4a40i05xqvc9qr9p8bjw2xjy0zprwf2h2w3vyz6")))

(define-public crate-rdbg-view-0.1.2 (c (n "rdbg-view") (v "0.1.2") (d (list (d (n "rdbg-client") (r "^0.1.2") (d #t) (k 0)))) (h "1d6vf405h2x8ybkqc6vjpwh432nbzgg1lxwnj1s0ksdrn3fxb676")))

(define-public crate-rdbg-view-0.1.3 (c (n "rdbg-view") (v "0.1.3") (d (list (d (n "rdbg-client") (r "^0.1.2") (d #t) (k 0)))) (h "013dv5sl2rflc3gqndrz67rdaa23y0amndrp58jk2yjd3fkni9kq")))

(define-public crate-rdbg-view-0.2.0 (c (n "rdbg-view") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rdbg-client") (r "^0.1.3") (d #t) (k 0)))) (h "17kavhxjjhsw87qkx5v45s6gn218hz9ya7h9c0sh0911awjd8lhg")))

(define-public crate-rdbg-view-0.2.1 (c (n "rdbg-view") (v "0.2.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rdbg-client") (r "^0.1.3") (d #t) (k 0)))) (h "1m0ry951xsy43ndglbjr1n0jbpi2c6mq4bnv8fqdj1vkdlv2zrix")))

(define-public crate-rdbg-view-0.2.2 (c (n "rdbg-view") (v "0.2.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rdbg-client") (r "^0.1.4") (d #t) (k 0)))) (h "0m9c0rjqjjn7yw7xnnqvzpcfmbykgxi93bwbfky73jqrb3krcnys")))

(define-public crate-rdbg-view-0.2.3 (c (n "rdbg-view") (v "0.2.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rdbg-client") (r "^0.1.5") (d #t) (k 0)))) (h "13ddgc6m7pl5d76h2806r60cj8rqd2h18mqgdsvmilrl15khmxlk")))

