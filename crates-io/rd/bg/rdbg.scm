(define-module (crates-io rd bg rdbg) #:use-module (crates-io))

(define-public crate-rdbg-0.1.0 (c (n "rdbg") (v "0.1.0") (h "0n2p2nlwakpi6ycyydc49n0ajn818qcgqai89b0gjygc4rx9b0yh") (f (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.1.1 (c (n "rdbg") (v "0.1.1") (h "1546xps3n183m6naqcq8mavlhl8gxlq6569lvfjc7lm9fxaqdcpp") (f (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.1.2 (c (n "rdbg") (v "0.1.2") (h "0zx3dc9lj7a3aq2xdi8z26fj6x944y8y4a57326hk2in24f9n1hc") (f (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.1.3 (c (n "rdbg") (v "0.1.3") (h "17d8pi303i1216j2sxrn6h1v3nvag07s4b68ka6jzvkqf737pqjx") (f (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.1.4 (c (n "rdbg") (v "0.1.4") (h "12fqlifqbhawaqivw6m1v9vw0y1kv8izaq563bmpb75fswsqp66w") (f (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.1.5 (c (n "rdbg") (v "0.1.5") (h "0zckcl0am2ikprsavkbp71vsnm3ffvgfxs15nk7gx19xrc8mqar2") (f (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.2.0 (c (n "rdbg") (v "0.2.0") (h "1xsyzn1y5v9hlfvvljafbl2ihq3l67zq87icl5qndk7f3cyl6cy7") (f (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.2.1 (c (n "rdbg") (v "0.2.1") (h "0wqix44mskpp2mgm4akw2p6lsvxqlp7l35zxq4m5hddvs4w6qr5q") (f (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

