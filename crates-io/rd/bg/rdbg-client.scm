(define-module (crates-io rd bg rdbg-client) #:use-module (crates-io))

(define-public crate-rdbg-client-0.1.0 (c (n "rdbg-client") (v "0.1.0") (h "09mljv212my96ncx5bal64lphdm13f4yka4i2n4pmcm762s61jds")))

(define-public crate-rdbg-client-0.1.2 (c (n "rdbg-client") (v "0.1.2") (d (list (d (n "rdbg") (r "^0.1.2") (d #t) (k 2)))) (h "148sa490icgy54sshil2k0is234cxw02rqhxnpska6qypm3jby01")))

(define-public crate-rdbg-client-0.1.3 (c (n "rdbg-client") (v "0.1.3") (d (list (d (n "rdbg") (r "^0.1.2") (d #t) (k 2)))) (h "1kr60dlh0fbh09xmrvwav26bzhnq38rlb6hcq16f183yg2vnxh4r")))

(define-public crate-rdbg-client-0.1.4 (c (n "rdbg-client") (v "0.1.4") (d (list (d (n "rdbg") (r "^0.2.0") (d #t) (k 2)))) (h "0pd0gb65yrxjx4kgnxafgyyadl7gpyczz88x5fw2xx37h18wnxiq")))

(define-public crate-rdbg-client-0.1.5 (c (n "rdbg-client") (v "0.1.5") (d (list (d (n "rdbg") (r "^0.2.1") (d #t) (k 2)))) (h "07m7jhzmzyfryc6w1q7gqdiba63zad81sl0yppr8a63ljzrjm52c")))

