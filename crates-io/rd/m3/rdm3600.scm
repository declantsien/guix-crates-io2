(define-module (crates-io rd m3 rdm3600) #:use-module (crates-io))

(define-public crate-rdm3600-0.1.0 (c (n "rdm3600") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "176jxqfjvhkdki74s65mzibja2znj6fs0905ww753kn6al4svgja")))

