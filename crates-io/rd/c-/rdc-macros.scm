(define-module (crates-io rd c- rdc-macros) #:use-module (crates-io))

(define-public crate-rdc-macros-0.1.0 (c (n "rdc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0j0xlaawc214xzk7l3n5vw2xyana2azzyvcdjdnpbc3zqllssy8g")))

