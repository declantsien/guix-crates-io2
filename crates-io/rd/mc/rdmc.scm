(define-module (crates-io rd mc rdmc) #:use-module (crates-io))

(define-public crate-rdmc-0.1.0 (c (n "rdmc") (v "0.1.0") (d (list (d (n "bpaf") (r "^0.9.9") (f (quote ("autocomplete"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "0kms5rss82dyhw1xqp0ylbpil7ik3as6lnmqny8jvk0r0gb73wxc")))

(define-public crate-rdmc-0.1.1 (c (n "rdmc") (v "0.1.1") (d (list (d (n "bpaf") (r "^0.9.9") (f (quote ("autocomplete" "bright-color"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "129ysbq9fmiwaargwk3myvr88jy7dckw4ckqa8wgwlywflgazkly")))

(define-public crate-rdmc-0.1.2 (c (n "rdmc") (v "0.1.2") (d (list (d (n "bpaf") (r "^0.9.9") (f (quote ("autocomplete" "bright-color"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "197l436abbp7fqf8qgcg0s3qkq107xwbmrm9nz7zd6kkla5lsx82")))

