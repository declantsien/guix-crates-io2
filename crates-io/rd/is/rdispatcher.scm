(define-module (crates-io rd is rdispatcher) #:use-module (crates-io))

(define-public crate-rdispatcher-0.0.3 (c (n "rdispatcher") (v "0.0.3") (h "0b6760q9shhakn414ah4ghgl6r1iz20jmpigs40spxskbfhgwp5w")))

(define-public crate-rdispatcher-0.0.4 (c (n "rdispatcher") (v "0.0.4") (h "0vgr5qn78srj2rfh5hyp41lrj56a33qdgq65v10d5lh4y7fvr2s3")))

(define-public crate-rdispatcher-0.0.5 (c (n "rdispatcher") (v "0.0.5") (h "0q4znbgqw1023yd79c89bxi9pf8n1gplp7lxn2n4m4b50k0rfdv8")))

(define-public crate-rdispatcher-0.1.0 (c (n "rdispatcher") (v "0.1.0") (h "0i30k5v794vz6p2hmmygvd1wxrf6lppx6myv4mz03vskb1vci6w1")))

