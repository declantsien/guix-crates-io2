(define-module (crates-io rd ra rdrand) #:use-module (crates-io))

(define-public crate-rdrand-0.0.1 (c (n "rdrand") (v "0.0.1") (h "06dvga06xfas2n3d2vkkc1f7nig0iqq9w8mpf6cb37jb7d6103y4")))

(define-public crate-rdrand-0.0.2 (c (n "rdrand") (v "0.0.2") (h "14ik6sk1lnxsyl68pnqnh8qj3vycsbvczq5q977dmvwih389a7h4")))

(define-public crate-rdrand-0.0.3 (c (n "rdrand") (v "0.0.3") (h "0fdchw70rc4gkr3r4a1zv1ywaxcpg7s8ymj41ybhhk8m7smbq788")))

(define-public crate-rdrand-0.0.4 (c (n "rdrand") (v "0.0.4") (h "1m4faw7bn6aylxprz0qfc77sk8kmqagfb5d7f4pip85zx67qzk18")))

(define-public crate-rdrand-0.0.5 (c (n "rdrand") (v "0.0.5") (h "11i5dhp6fnjvhk4pijmi1pfqpvklszpp63xfkzmbf6wjh56jzxf4")))

(define-public crate-rdrand-0.1.0 (c (n "rdrand") (v "0.1.0") (d (list (d (n "rand") (r "0.1.*") (d #t) (k 0)))) (h "130h2wmlxc01147iv6zw302y70bf2727vj081h3i1mhqsv70xkv1")))

(define-public crate-rdrand-0.1.1 (c (n "rdrand") (v "0.1.1") (d (list (d (n "rand") (r "^0.1") (d #t) (k 0)))) (h "1ygi5mwqw2nzdg82n7hg8j0xks37s5bbc4x35jkw4sx4l2nn19yx")))

(define-public crate-rdrand-0.1.2 (c (n "rdrand") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0095dzghb6n0aql0av6idn7769myx62c2gvnvdy3wj09r3amaqwv")))

(define-public crate-rdrand-0.1.3 (c (n "rdrand") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0x15lvmmn800vgdim02gm3v2lk7l9g29zmrxafr7jc145mh2zrc0")))

(define-public crate-rdrand-0.1.4 (c (n "rdrand") (v "0.1.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "193scxwfh4f4g39cj18x8lwvzp6jh8xxxxj1aph8a99cb8psw8vv")))

(define-public crate-rdrand-0.1.5 (c (n "rdrand") (v "0.1.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "10ydhiczivj8xk26pgajyb8h33c823dqb3xi6jd53p5bl6dbkjpx")))

(define-public crate-rdrand-0.1.6 (c (n "rdrand") (v "0.1.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0a1gskcw270bb00b8scj70hmk2qxvbzkx15psp32xdgj84cpyq0m")))

(define-public crate-rdrand-0.1.7 (c (n "rdrand") (v "0.1.7") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "054cd80mpg6m07ya0d9wz85mcl2m5pgr76v5nqj4d43nw2q8mayp")))

(define-public crate-rdrand-0.2.0 (c (n "rdrand") (v "0.2.0") (d (list (d (n "rand_core") (r "^0.2") (k 0)))) (h "1mzy7c0vwxfixsj74hc679cscfh4xfnn2dfkb3ll205g6avd4bcv")))

(define-public crate-rdrand-0.3.0 (c (n "rdrand") (v "0.3.0") (d (list (d (n "rand_core") (r "^0.3") (k 0)))) (h "14yzlr8l1r93100xlzpz7ny607x0dkvpd0sp4kywx02w4vq0xrqn")))

(define-public crate-rdrand-0.4.0 (c (n "rdrand") (v "0.4.0") (d (list (d (n "rand_core") (r "^0.3") (k 0)))) (h "1cjq0kwx1bk7jx3kzyciiish5gqsj7620dm43dc52sr8fzmm9037") (f (quote (("std") ("default" "std"))))))

(define-public crate-rdrand-0.5.0 (c (n "rdrand") (v "0.5.0") (d (list (d (n "rand_core") (r "^0.4") (k 0)))) (h "0biqbnpgkg3ji9sad04nzhgzwihzr2lfjp1wmwar6117p3s4aac3") (f (quote (("std") ("default" "std"))))))

(define-public crate-rdrand-0.5.1 (c (n "rdrand") (v "0.5.1") (d (list (d (n "rand_core") (r "^0.4") (k 0)))) (h "02dsyibvl1iygkmljr0ld9vjyp525q4mjy5j9yazrrkyvhvw7gvp") (f (quote (("std") ("default" "std"))))))

(define-public crate-rdrand-0.6.0 (c (n "rdrand") (v "0.6.0") (d (list (d (n "rand_core") (r "^0.4") (k 0)))) (h "1ir1d2xwknd9r2dpygpphdc7h7wmfy23kjivbp0n0psinm1gh52h") (f (quote (("std") ("default" "std"))))))

(define-public crate-rdrand-0.5.2 (c (n "rdrand") (v "0.5.2") (d (list (d (n "rand_core") (r "^0.4") (k 0)))) (h "192j1viv70mpz8x4k2kh6j58839vyvx4k8nsrkz4sg6knj2p1pjp") (f (quote (("std") ("default" "std"))))))

(define-public crate-rdrand-0.7.0 (c (n "rdrand") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (k 0)))) (h "0q5k8wby76vhcy183sjddj32hh7pabkyd5fvc0d300lpvp6hc6gx") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-rdrand-0.8.0 (c (n "rdrand") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "057jgl6bzzrcp1yr7xdx78v1v2nn4ma9p9p8vzn629pki8w55pha") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-rdrand-0.8.1 (c (n "rdrand") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "0xm5260sih9bp3lil22kymy44r5029xw56akh3jy8h6b0p5xbq64") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-rdrand-0.8.2 (c (n "rdrand") (v "0.8.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "1fv6w7lzxfrfmw16dymn27s454qkqr1q99zp3ymc2m852r1bccz2") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-rdrand-0.8.3 (c (n "rdrand") (v "0.8.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "0m9nacxrckfh5dpmn5wqrd63llafy382xg5d8znqxb0jhqi9a8fr") (f (quote (("std" "rand_core/std") ("default" "std"))))))

