(define-module (crates-io rd en rdenticon) #:use-module (crates-io))

(define-public crate-rdenticon-0.1.0 (c (n "rdenticon") (v "0.1.0") (d (list (d (n "ril") (r "^0.9") (k 0)) (d (n "sha1_smol") (r "^1") (d #t) (k 0)))) (h "1zcbzna71aavwd6vn1w24r1sgsikf55x9vwa2qxpsp4viby9z5cv") (f (quote (("default" "ril/png"))))))

(define-public crate-rdenticon-0.1.1 (c (n "rdenticon") (v "0.1.1") (d (list (d (n "ril") (r ">=0.9, <0.11") (k 0)) (d (n "sha1_smol") (r "^1") (d #t) (k 0)))) (h "1p2hqbv9bsk3fj7iym1zr3ci7i58aicgs0f3s15h5d4p55k2q8w7") (f (quote (("default" "ril/png"))))))

(define-public crate-rdenticon-0.1.2 (c (n "rdenticon") (v "0.1.2") (d (list (d (n "ril") (r ">=0.9, <0.11") (k 0)) (d (n "sha1_smol") (r "^1") (d #t) (k 0)))) (h "0dzvjyr0wih4lsm5b8smlgq1ws9xn6xag8rs2y10q70nzqsccwzn") (f (quote (("default" "ril/png"))))))

(define-public crate-rdenticon-0.1.3 (c (n "rdenticon") (v "0.1.3") (d (list (d (n "ril") (r ">=0.9, <0.11") (k 0)) (d (n "sha1_smol") (r "^1") (d #t) (k 0)))) (h "07dv5jc1ckf89l0jrf67x88l28rnd93yn8nbsajdxkwif1ihkd8r") (f (quote (("default" "ril/png"))))))

(define-public crate-rdenticon-0.1.4 (c (n "rdenticon") (v "0.1.4") (d (list (d (n "ril") (r ">=0.9, <0.11") (k 0)) (d (n "sha1_smol") (r "^1") (d #t) (k 0)))) (h "0vgbmdiv00qfgnrq2gxch6hlxlqrfvb18hbk0c621rdwb6nadmb3") (f (quote (("default" "ril/png"))))))

