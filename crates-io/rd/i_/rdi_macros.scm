(define-module (crates-io rd i_ rdi_macros) #:use-module (crates-io))

(define-public crate-rdi_macros-0.1.0 (c (n "rdi_macros") (v "0.1.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1vdikjkpf9bsrbkia9hwilj6qxpc9h9q1px785zd8f2przy6ra9r")))

