(define-module (crates-io rd b- rdb-pagination-derive) #:use-module (crates-io))

(define-public crate-rdb-pagination-derive-0.1.1 (c (n "rdb-pagination-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rdb-pagination-core") (r "^0.1") (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0rkcjfnbnpr3krh3fz330gq8x63n11fxscq39lh455169pjxzdnb") (r "1.61")))

