(define-module (crates-io rd b- rdb-pagination) #:use-module (crates-io))

(define-public crate-rdb-pagination-0.1.1 (c (n "rdb-pagination") (v "0.1.1") (d (list (d (n "educe") (r "^0.5") (f (quote ("default"))) (k 2)) (d (n "rdb-pagination-core") (r "^0.1") (k 0)) (d (n "rdb-pagination-derive") (r "^0.1") (o #t) (k 0)))) (h "0bxlvs7r1cfdgq5xwxv5cw09rgxfxknc1cpgc75s6v93c7g9kv8s") (f (quote (("serde" "rdb-pagination-core/serde") ("mysql" "rdb-pagination-core/mysql") ("default" "derive")))) (s 2) (e (quote (("derive" "dep:rdb-pagination-derive")))) (r "1.61")))

