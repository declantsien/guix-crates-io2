(define-module (crates-io rd b- rdb-pagination-core) #:use-module (crates-io))

(define-public crate-rdb-pagination-core-0.1.0 (c (n "rdb-pagination-core") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "150q6yzhx8rn8svmc3bs00xv3sqbdab7gqxhsy46ixx7qvy69hz2") (f (quote (("mysql")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.61")))

(define-public crate-rdb-pagination-core-0.1.1 (c (n "rdb-pagination-core") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1a5ddkmz47qlj2qckbvg8n48v41q55dwwsf31xhckkrhdmbxdhf9") (f (quote (("mysql")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.61")))

