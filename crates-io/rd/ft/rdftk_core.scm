(define-module (crates-io rd ft rdftk_core) #:use-module (crates-io))

(define-public crate-rdftk_core-0.1.0 (c (n "rdftk_core") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.0") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.0") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "0y2bykkp4fm0z9dvyn8nc4gxvrmm3h5s3ahzxl11qjkrmb82jg7k")))

(define-public crate-rdftk_core-0.1.1 (c (n "rdftk_core") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.0") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.0") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "1lai5lfp3ai9c5xnqz2wpsvy4w1rbwbc0p7g00ad8mjqivk1g0cc")))

(define-public crate-rdftk_core-0.1.2 (c (n "rdftk_core") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.0") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.0") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "05gq74ckbhqy0m3l5ch0hxaywd8dh3l1vfxxryafk49dsxn7sj9k")))

(define-public crate-rdftk_core-0.1.4 (c (n "rdftk_core") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "0b4f353cfa3zxy1a3xz07qdvjpnlrhgjj0vd59wxxz9y3pcy2bbk")))

(define-public crate-rdftk_core-0.1.5 (c (n "rdftk_core") (v "0.1.5") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "104mfk96wama34s72g3mivlyk5ck8gxacq2dpv7116ccp0cmfvjh")))

(define-public crate-rdftk_core-0.1.6 (c (n "rdftk_core") (v "0.1.6") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.5") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "1jqqkm6kbwix8bn51j236n867m996wxjsr14hah4hk9ak2c2i732")))

(define-public crate-rdftk_core-0.1.7 (c (n "rdftk_core") (v "0.1.7") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.2") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.5") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "075a0d5hzi8mblfqhb6lfaf1mifwjcn7d1sfch42d8gar97nkdqd")))

(define-public crate-rdftk_core-0.1.8 (c (n "rdftk_core") (v "0.1.8") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.2") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.5") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "1zdixaksm29q1cyillzqq4xvym0d9ni6xjq11f7nrlcdq8sdh6ls")))

(define-public crate-rdftk_core-0.1.9 (c (n "rdftk_core") (v "0.1.9") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.2") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.5") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "1qq80irqsrahi6s0k84kmb002ljc4z12b6jvdsm6j17dn8s1rv5b")))

(define-public crate-rdftk_core-0.1.10 (c (n "rdftk_core") (v "0.1.10") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.2") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.5") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "1piyz0phk91gx195jrif04xrry6xyks1gfrnv0vlvinmla8qzbr1")))

(define-public crate-rdftk_core-0.1.11 (c (n "rdftk_core") (v "0.1.11") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.2") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.5") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "04bbi11v7l0jgq586pcgwgxmxawjrs37k8v9k8hp00nc79gdcabp")))

(define-public crate-rdftk_core-0.1.12 (c (n "rdftk_core") (v "0.1.12") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.2") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.5") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "1v51vgggdd55k0d0r1mns53mpc0m6dd8fihqncvvx5g68ymvgy6v")))

(define-public crate-rdftk_core-0.1.13 (c (n "rdftk_core") (v "0.1.13") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.2") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.5") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "1s6srs0axpgfvyqdjy5gw115im1ljqp5hrfjn7k61hxx3pwz1ls2")))

(define-public crate-rdftk_core-0.1.14 (c (n "rdftk_core") (v "0.1.14") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)) (d (n "unique_id") (r "^0.1.3") (d #t) (k 0)))) (h "0zhcz1rpfnk8h185f03cc235gvza0dd5gqcv23bz7jimkbm6v5v8")))

(define-public crate-rdftk_core-0.2.0 (c (n "rdftk_core") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)) (d (n "unique_id") (r "^0.1") (d #t) (k 0)))) (h "1p3l1nqlc9s3yam5fady8m7j4hcn8138qimc00fza8fj8nspg0ag")))

(define-public crate-rdftk_core-0.2.2 (c (n "rdftk_core") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)) (d (n "unique_id") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "02gk76cay22i2wjvzddh2rdj78k0w9qhg0qnpyvvw8yxb9x70sc4")))

(define-public crate-rdftk_core-0.2.3 (c (n "rdftk_core") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)) (d (n "unique_id") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0j9c7j4pq9pvxn00mkwsm5n85f1rns2avkdglx7nl7phas5hj2n1")))

(define-public crate-rdftk_core-0.2.4 (c (n "rdftk_core") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)) (d (n "unique_id") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0axgx6n5hicxfx36awycj1i6a15ykm0h4habxbyzvi2pgmf0x5sk")))

(define-public crate-rdftk_core-0.3.0-pre (c (n "rdftk_core") (v "0.3.0-pre") (d (list (d (n "bimap") (r "^0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)) (d (n "unique_id") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "14v4bz7azflzdzhsmad73jimfzp9cn6fmmam1ydz7rjr3vbcigf7") (f (quote (("rdf_star") ("n3_formulae") ("default" "chrono_types" "n3_formulae" "rdf_star") ("chrono_types" "chrono"))))))

(define-public crate-rdftk_core-0.3.0 (c (n "rdftk_core") (v "0.3.0") (d (list (d (n "bimap") (r "^0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parameterized") (r "^0.3.1") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)) (d (n "unique_id") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1m0xfpn5gw8rgr1nm8jqr9qfinha0v903bfwnr9598rn82ggjzyx") (f (quote (("rdf_star") ("n3_formulae") ("default" "chrono_types" "n3_formulae" "rdf_star") ("chrono_types" "chrono"))))))

(define-public crate-rdftk_core-0.3.1 (c (n "rdftk_core") (v "0.3.1") (d (list (d (n "bimap") (r "^0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parameterized") (r "^0.3.1") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)) (d (n "unique_id") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "11hhbzr76mi2zq1fmv7y72hi4xhmq2hkhja0d6ldvvb1j8nk9d5k") (f (quote (("rdf_star") ("n3_formulae") ("default" "chrono_types" "n3_formulae" "rdf_star") ("chrono_types" "chrono"))))))

