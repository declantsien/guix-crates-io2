(define-module (crates-io rd ft rdftk_graph) #:use-module (crates-io))

(define-public crate-rdftk_graph-0.1.0 (c (n "rdftk_graph") (v "0.1.0") (d (list (d (n "rdftk_core") (r "^0.1.0") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.0") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.0") (d #t) (k 0)))) (h "1998mxpi7h6gc1wy9xya5sykk1b6fz31l7hmmprbjrklxwp3syjv")))

(define-public crate-rdftk_graph-0.1.1 (c (n "rdftk_graph") (v "0.1.1") (d (list (d (n "rdftk_core") (r "^0.1") (d #t) (k 0)))) (h "12sb85xqb7jvqgn5y0w21w3dkzjhgpmx1imd59hshm4dbkz12s4y")))

