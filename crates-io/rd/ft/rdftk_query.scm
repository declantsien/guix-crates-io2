(define-module (crates-io rd ft rdftk_query) #:use-module (crates-io))

(define-public crate-rdftk_query-0.1.0 (c (n "rdftk_query") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "rdftk_core") (r "^0.1.0") (d #t) (k 0)))) (h "1lm2gnvji3cyw3625v8icvlx038x5y97s0xwp1jpmgv7qrp5832w")))

(define-public crate-rdftk_query-0.1.3 (c (n "rdftk_query") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "rdftk_core") (r "^0.3") (d #t) (k 0)))) (h "1nhfy9x9viv6lwlfgw0y2i4ffs8f5cy2b7ln72n60biqjm619brg")))

