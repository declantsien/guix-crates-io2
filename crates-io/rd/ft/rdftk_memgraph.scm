(define-module (crates-io rd ft rdftk_memgraph) #:use-module (crates-io))

(define-public crate-rdftk_memgraph-0.1.0 (c (n "rdftk_memgraph") (v "0.1.0") (d (list (d (n "rdftk_core") (r "^0.1.0") (d #t) (k 0)) (d (n "rdftk_graph") (r "^0.1.0") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.0") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.0") (d #t) (k 0)))) (h "1v9h1dgjg1cw6piv8xdq44a0vdjrib3l4fpycqwqamvas2akv6dp")))

(define-public crate-rdftk_memgraph-0.1.2 (c (n "rdftk_memgraph") (v "0.1.2") (d (list (d (n "rdftk_core") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)))) (h "0w1mrplyw3i2f7dv5hdcfh5nlzaa3gxsca4ykg856bk0nmbx5gxi")))

(define-public crate-rdftk_memgraph-0.1.3 (c (n "rdftk_memgraph") (v "0.1.3") (d (list (d (n "rdftk_core") (r "^0.1.5") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.4") (d #t) (k 0)))) (h "14s2dcrpxr5wlb97dfcx23kx3v56bfqw6r64kz8xic7ggrmgd7cq")))

(define-public crate-rdftk_memgraph-0.1.4 (c (n "rdftk_memgraph") (v "0.1.4") (d (list (d (n "rdftk_core") (r "^0.1.5") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.2") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.4") (d #t) (k 0)))) (h "0i16vw1pb3dcm4vj2ihxrghypvbipdm3i89dlhmrbc67xvyvfjwp")))

(define-public crate-rdftk_memgraph-0.1.5 (c (n "rdftk_memgraph") (v "0.1.5") (d (list (d (n "rdftk_core") (r "^0.1.7") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.2") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.4") (d #t) (k 0)))) (h "0rd47v4j56dhqlr1b2v7gyymln3zrqbswvhznwcq9rk8qipc7bmi")))

(define-public crate-rdftk_memgraph-0.1.6 (c (n "rdftk_memgraph") (v "0.1.6") (d (list (d (n "rdftk_core") (r "^0.1.7") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.2") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.4") (d #t) (k 0)))) (h "157w09y2zs3kkx7xal9x9wf28s4rv75rf98qzww9bmpamb8hs8g9")))

(define-public crate-rdftk_memgraph-0.1.8 (c (n "rdftk_memgraph") (v "0.1.8") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "rdftk_core") (r "^0.1.11") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.4") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1.7") (d #t) (k 0)))) (h "0j7rx4zaj6wvkp7klxw5rlv4v083hg48xrp5mpvcw3yhijd6zccn")))

(define-public crate-rdftk_memgraph-0.1.9 (c (n "rdftk_memgraph") (v "0.1.9") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "rdftk_core") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)))) (h "1grvpcjx5lvff3s0jvxp23ghyfns8l6adv438r6pd81v1vyjwjcl")))

(define-public crate-rdftk_memgraph-0.1.10 (c (n "rdftk_memgraph") (v "0.1.10") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "rdftk_core") (r "^0.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)))) (h "1a8z9ksm2h2x1can5snf4zndi86kdd5shc0vljykig70bi9mqw81")))

(define-public crate-rdftk_memgraph-0.1.11 (c (n "rdftk_memgraph") (v "0.1.11") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rdftk_core") (r "^0.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)))) (h "1aljpwp70x471x8k4wk2scqxxbyj0dfcw38n7fdkpys03n8aqk63")))

(define-public crate-rdftk_memgraph-0.1.12 (c (n "rdftk_memgraph") (v "0.1.12") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rdftk_core") (r "^0.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)))) (h "0s76p52q8x3vaffr6bw7qgl9z7damyqqrwqsiy96f43nmygghyg9")))

