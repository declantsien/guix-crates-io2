(define-module (crates-io rd ft rdftk_ontology) #:use-module (crates-io))

(define-public crate-rdftk_ontology-0.1.0-pre (c (n "rdftk_ontology") (v "0.1.0-pre") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rdftk_core") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_names") (r "^0.1") (d #t) (k 0)))) (h "1jm951vag2gwrxvyg5895bk0jmsnybaa7k73l0jbriqyx8dppjy7")))

