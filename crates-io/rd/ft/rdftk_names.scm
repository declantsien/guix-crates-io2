(define-module (crates-io rd ft rdftk_names) #:use-module (crates-io))

(define-public crate-rdftk_names-0.1.0 (c (n "rdftk_names") (v "0.1.0") (d (list (d (n "paste") (r "^0.1.12") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.0") (d #t) (k 0)))) (h "09p6ja1a0k7hgsvapj0gcy1vj7ygqcsafdvnzi7mbw1qw67lbwii")))

(define-public crate-rdftk_names-0.1.1 (c (n "rdftk_names") (v "0.1.1") (d (list (d (n "paste") (r "^0.1.12") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.0") (d #t) (k 0)))) (h "1khizjckngrp3d527cf0mc8hxgjdgczla5rmv3xbhww5rz8b4amv")))

(define-public crate-rdftk_names-0.1.2 (c (n "rdftk_names") (v "0.1.2") (d (list (d (n "paste") (r "^0.1.12") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.0") (d #t) (k 0)))) (h "1nbx9ln3hq9lncpmva0f4wi2j7z83zc9n9h1imzc6vhv9f306baa")))

(define-public crate-rdftk_names-0.1.4 (c (n "rdftk_names") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.12") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)))) (h "11a43b4iky5pk8nx1c3wwjlvs23g02x2myanccx20pkvv4nhxsqi")))

(define-public crate-rdftk_names-0.1.5 (c (n "rdftk_names") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.12") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.1") (d #t) (k 0)))) (h "19lpwbl2hbh00w3lbi7b289rr0r3sz8i8anski1gw5yr5v6r88px")))

(define-public crate-rdftk_names-0.1.6 (c (n "rdftk_names") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.1") (d #t) (k 0)))) (h "0zl6siabhk6b53gx4qisdb5cgf8m48zqp55xxz6v6ma2svggnp1i")))

(define-public crate-rdftk_names-0.1.7 (c (n "rdftk_names") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.2") (d #t) (k 0)))) (h "1l4sifxc8b9rwq2slq09my329qqxlnbvazy69g3kikxc6c9zjlja")))

(define-public crate-rdftk_names-0.1.8 (c (n "rdftk_names") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1.4") (d #t) (k 0)))) (h "1ngys7phigz0j7m9zz7wy07qvgwr7r0gdnimhwfiqf9a1frhsjkq")))

(define-public crate-rdftk_names-0.1.9 (c (n "rdftk_names") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.2") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)))) (h "08w5q49ibj3rrl1akqvzasxq6x7vjd506r6zbhi7yj1wazhpmkpk")))

