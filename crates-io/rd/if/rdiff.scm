(define-module (crates-io rd if rdiff) #:use-module (crates-io))

(define-public crate-rdiff-0.1.0 (c (n "rdiff") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "notify") (r "^2.6.1") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1gp4mqcm5p6v4aywmq6ph0qc92q4vvajfy0x69fzpkf5gsraldrk")))

(define-public crate-rdiff-0.1.1 (c (n "rdiff") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "notify") (r "^2.6.1") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "04cqdpyfzfqby08px32nrsyrnlg6pis52fmn94dxldlm3sycq6f4")))

(define-public crate-rdiff-0.1.2 (c (n "rdiff") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "notify") (r "^2.6.1") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1w8gfi3zq789nra8hkl8sikbn6gxq0cf5474f1dwn47k9x4r1j1h")))

