(define-module (crates-io rd ed rdedup-prune) #:use-module (crates-io))

(define-public crate-rdedup-prune-0.1.0 (c (n "rdedup-prune") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "rdedup-lib") (r "^3.1.0") (d #t) (k 0)) (d (n "rson_rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (d #t) (k 0)) (d (n "slog-async") (r "^2.5.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1haw8d0nssmmhamf71h88p7ym8a5457ln43gc4g780c9dwgcbkhv")))

