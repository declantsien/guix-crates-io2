(define-module (crates-io rd ed rdedup-cdc) #:use-module (crates-io))

(define-public crate-rdedup-cdc-0.1.0 (c (n "rdedup-cdc") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0hal9gyn04ad1macvgrg4p9079xz412cvcp1pnbndpmmih79xq7d") (f (quote (("default") ("bench"))))))

