(define-module (crates-io rd xs rdxsort) #:use-module (crates-io))

(define-public crate-rdxsort-0.1.0 (c (n "rdxsort") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "08bpvmqcw54gffahr9rhsgwdhca0gzmhyrjjqs89ysgkfb2bxbl8")))

(define-public crate-rdxsort-0.2.0 (c (n "rdxsort") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0xam91zdqxj99nvkrm66yp0ah8k3yfh5j6dvqkhva77awb52a1br") (f (quote (("unstable"))))))

(define-public crate-rdxsort-0.3.0 (c (n "rdxsort") (v "0.3.0") (d (list (d (n "quicksort") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0sf1a0vk8b32fscn0637rb0q4gzw55j98w6g5flbg6gbf1gkm5km") (f (quote (("unstable"))))))

