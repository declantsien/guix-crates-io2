(define-module (crates-io rd dl rddl) #:use-module (crates-io))

(define-public crate-rddl-0.1.0 (c (n "rddl") (v "0.1.0") (d (list (d (n "pom") (r "^0.9.0") (d #t) (k 0)))) (h "1l0nil6s2plgvdxg8ybqxcq0i1d9k3fp596vd33qhrjvq73fvqlk")))

(define-public crate-rddl-0.1.1 (c (n "rddl") (v "0.1.1") (d (list (d (n "pom") (r "^1.0.1") (d #t) (k 0)))) (h "09gph8n2a43idqjp9yh8nixcxzv8lbjzz9lnrsfdy9vipfh1scib")))

