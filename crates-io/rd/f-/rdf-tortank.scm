(define-module (crates-io rd f- rdf-tortank) #:use-module (crates-io))

(define-public crate-rdf-tortank-0.1.3 (c (n "rdf-tortank") (v "0.1.3") (d (list (d (n "neon") (r "^0.10") (f (quote ("napi-6"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tortank") (r "^0.1.9") (d #t) (k 0)))) (h "0fh0wpbg2wjhvns47vmz5zlm6gxx86g6ad7wylbfxxd1aips259z")))

(define-public crate-rdf-tortank-0.1.4 (c (n "rdf-tortank") (v "0.1.4") (d (list (d (n "neon") (r "^0.10") (f (quote ("napi-6"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tortank") (r "^0.1.9") (d #t) (k 0)))) (h "07mzb4j8jwyn1rhz9r346dzl5yfjld9pw27wij161rv484m9a1dp")))

(define-public crate-rdf-tortank-0.1.5 (c (n "rdf-tortank") (v "0.1.5") (d (list (d (n "neon") (r "^0.10") (f (quote ("napi-6"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tortank") (r "^0.1.9") (d #t) (k 0)))) (h "05drqcm3ix5a0d7512jdzgf8ylb8n1vvi39p44hb5amn5lq334hn")))

(define-public crate-rdf-tortank-0.2.0 (c (n "rdf-tortank") (v "0.2.0") (d (list (d (n "neon") (r "^0.10.1") (f (quote ("napi-6"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tortank") (r "^0.1.13") (d #t) (k 0)))) (h "1lpv3wxhb45nydmbw1bp5v1m8wd9qcs2wl6dwh7zgy46d0r9is92")))

(define-public crate-rdf-tortank-0.2.1 (c (n "rdf-tortank") (v "0.2.1") (d (list (d (n "neon") (r "^0.10.1") (f (quote ("napi-6"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tortank") (r "^0.1.18") (d #t) (k 0)))) (h "1l8j9p55ic080cay9hx16giiix41rwbqs6x2483psxiyb6h56m4b")))

