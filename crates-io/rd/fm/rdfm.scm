(define-module (crates-io rd fm rdfm) #:use-module (crates-io))

(define-public crate-rdfm-0.1.0 (c (n "rdfm") (v "0.1.0") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)))) (h "1jzvqqs5is76385lviicabzszyckyngv238nwrsxjdya3v6rfkvd")))

(define-public crate-rdfm-0.1.1 (c (n "rdfm") (v "0.1.1") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)))) (h "1slyi7jw9ab98m5m4b2ajz5pyhl5vadgw4yykpaia7xd0ci855x4")))

(define-public crate-rdfm-0.1.2 (c (n "rdfm") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0dgbmrccl5nrzajilzdd06xrhb2l9s6m9v945q6bjqp1nrqq9wfa")))

(define-public crate-rdfm-0.1.3 (c (n "rdfm") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1b532dxlmxxh6w0j31fw2z3pbcaygx2gdfnsdipmfsbjdjl3dfr5")))

