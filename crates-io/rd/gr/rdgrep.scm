(define-module (crates-io rd gr rdgrep) #:use-module (crates-io))

(define-public crate-rdgrep-0.1.0 (c (n "rdgrep") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1zz3x36v6a7ardzbymqhzx9jh3xg0wr0g781pqvhi5p9vbrmpgsx")))

