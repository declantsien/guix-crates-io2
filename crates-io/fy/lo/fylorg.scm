(define-module (crates-io fy lo fylorg) #:use-module (crates-io))

(define-public crate-fylorg-0.0.1 (c (n "fylorg") (v "0.0.1") (d (list (d (n "clap") (r "^4.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0xg5r8nff4qq8xxigc4sqahh6v7wrg4p7458k8lk57iyrx97dasc")))

