(define-module (crates-io fy ro fyrox-animation) #:use-module (crates-io))

(define-public crate-fyrox-animation-0.1.0 (c (n "fyrox-animation") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "fyrox-core") (r "^0.27.0") (d #t) (k 0)) (d (n "spade") (r "^2.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.0") (d #t) (k 0)))) (h "0avm6nhqg2n15lwyiil1iwr522b9avmmykksx7w11z0wv8xnz7hb") (r "1.72")))

(define-public crate-fyrox-animation-0.2.0 (c (n "fyrox-animation") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "fyrox-core") (r "^0.28.1") (d #t) (k 0)) (d (n "spade") (r "^2.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "042g9kwz5zg1z9yjr17wyrf48msbdbg2bvw7h7fhrpm7bzvh8yrn") (r "1.72")))

