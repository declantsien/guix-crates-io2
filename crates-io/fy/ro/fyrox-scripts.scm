(define-module (crates-io fy ro fyrox-scripts) #:use-module (crates-io))

(define-public crate-fyrox-scripts-0.1.0 (c (n "fyrox-scripts") (v "0.1.0") (d (list (d (n "fyrox") (r "^0.32.0") (d #t) (k 0)))) (h "1wr46zxp94h9dc5brl6l8jdynjckb4wb5f5k5833hy4maf78f5c9") (r "1.56")))

(define-public crate-fyrox-scripts-0.2.0 (c (n "fyrox-scripts") (v "0.2.0") (d (list (d (n "fyrox") (r "^0.33.0") (d #t) (k 0)))) (h "0ypx9vi30hkfdf0pk77x6kqcxzy0i7xwbjzwp7hg30sza0n8iy5s") (r "1.72")))

(define-public crate-fyrox-scripts-0.3.0 (c (n "fyrox-scripts") (v "0.3.0") (d (list (d (n "fyrox") (r "^0.34.0") (d #t) (k 0)))) (h "0xr6xkds2qglrgx5miw60i68iqqfzw2wl56ggcsi9vrggzv5mjjv") (r "1.72")))

