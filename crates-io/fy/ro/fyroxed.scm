(define-module (crates-io fy ro fyroxed) #:use-module (crates-io))

(define-public crate-fyroxed-0.11.0 (c (n "fyroxed") (v "0.11.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "fyrox") (r "^0.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1xhlgg1fizin69jq4h37vg8z86vl744v65jx8v32fxzvb1q8rcsm") (f (quote (("enable_profiler" "fyrox/enable_profiler")))) (r "1.56")))

(define-public crate-fyroxed-0.12.0 (c (n "fyroxed") (v "0.12.0") (d (list (d (n "fyrox") (r "^0.25") (d #t) (k 0)) (d (n "fyroxed_base") (r "^0.12.0") (d #t) (k 0)))) (h "1ylgc32l0466xbxhsnhfchdpb20kby88zw3nmw85lgpl3liq6ivv") (r "1.56")))

(define-public crate-fyroxed-0.13.0 (c (n "fyroxed") (v "0.13.0") (d (list (d (n "fyrox") (r "^0.26") (d #t) (k 0)) (d (n "fyroxed_base") (r "^0.13.0") (d #t) (k 0)))) (h "01gjvqzh47xkz6yg9xzs76c4qcdbhv5qxckri6fc766akkqj43wa") (r "1.56")))

(define-public crate-fyroxed-0.14.0 (c (n "fyroxed") (v "0.14.0") (d (list (d (n "fyrox") (r "^0.27") (d #t) (k 0)) (d (n "fyroxed_base") (r "^0.14.0") (d #t) (k 0)))) (h "1nwqddymbfajrbyy87g5gyyd16m1y5vlwg0xrc2n5g86a1pmw41z") (r "1.56")))

(define-public crate-fyroxed-0.14.1 (c (n "fyroxed") (v "0.14.1") (d (list (d (n "fyrox") (r "^0.27.1") (d #t) (k 0)) (d (n "fyroxed_base") (r "^0.14.0") (d #t) (k 0)))) (h "1x846kwx4425h9dbw7agzyasv9gg0p0m7jwswjjzrx56a0h76g56") (r "1.56")))

