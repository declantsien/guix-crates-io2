(define-module (crates-io fy ro fyrox-dylib) #:use-module (crates-io))

(define-public crate-fyrox-dylib-0.1.0 (c (n "fyrox-dylib") (v "0.1.0") (d (list (d (n "fyrox-impl") (r "^0.34.0") (d #t) (k 0)))) (h "0bq7jy6byy55prvlh6d1aywzad7sxwnckd3jl9yb6sjjz5i382sw") (f (quote (("mesh_analysis" "fyrox-impl/mesh_analysis") ("gltf_blend_shapes" "fyrox-impl/gltf_blend_shapes") ("gltf" "fyrox-impl/gltf")))) (r "1.72")))

