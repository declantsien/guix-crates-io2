(define-module (crates-io fy ro fyrox-resource) #:use-module (crates-io))

(define-public crate-fyrox-resource-0.3.0 (c (n "fyrox-resource") (v "0.3.0") (d (list (d (n "fyrox-core") (r "^0.19.0") (d #t) (k 0)))) (h "1qi3j829xk0azw1rpv88l231vyww1361zpgkrz62jv85wm8hcynv") (r "1.56")))

(define-public crate-fyrox-resource-0.4.0 (c (n "fyrox-resource") (v "0.4.0") (d (list (d (n "fyrox-core") (r "^0.20.0") (d #t) (k 0)))) (h "1hjs9knqdz3xndvlcwfs3xrklm6hszj47dajbga2v4kcs978syjk") (r "1.56")))

(define-public crate-fyrox-resource-0.5.0 (c (n "fyrox-resource") (v "0.5.0") (d (list (d (n "fyrox-core") (r "^0.21.0") (d #t) (k 0)))) (h "0vwj73cv5fcyxga73mwnm6js535m94i7nx7fsfmwyq9yj7i5hrzi") (r "1.56")))

(define-public crate-fyrox-resource-0.6.0 (c (n "fyrox-resource") (v "0.6.0") (d (list (d (n "fyrox-core") (r "^0.22.0") (d #t) (k 0)))) (h "07ps7kfw7rlgvkl9f6la0ai53drrwsp3iyj03iclaxzgb4sf1h7s") (r "1.56")))

(define-public crate-fyrox-resource-0.7.0 (c (n "fyrox-resource") (v "0.7.0") (d (list (d (n "fyrox-core") (r "^0.23.0") (d #t) (k 0)))) (h "1ps2n2k7frxhb48lzf2vj4s7y3qd3p4v92z4q27nmx2bi91fmrc3") (r "1.56")))

(define-public crate-fyrox-resource-0.8.0 (c (n "fyrox-resource") (v "0.8.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "fyrox-core") (r "^0.24.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09ayg2z75fd14j9d91yylraix7j4m59mh9gsl8fqnjfzzv3vqncy") (r "1.56")))

(define-public crate-fyrox-resource-0.9.0 (c (n "fyrox-resource") (v "0.9.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "fyrox-core") (r "^0.25.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jvhiy52yywwsbp41g728f6404kabmbsk8ghbgwsjgq6rbbnlwvd") (r "1.56")))

(define-public crate-fyrox-resource-0.10.0 (c (n "fyrox-resource") (v "0.10.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "fyrox-core") (r "^0.26.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04xwqgpw432wa9ghdrq0xwb2cmnim3ss3xirh0qvq7pd5885mhxz") (r "1.56")))

(define-public crate-fyrox-resource-0.11.0 (c (n "fyrox-resource") (v "0.11.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "fyrox-core") (r "^0.27.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1fqj50f7cvbhpj9fr2p4jilbhr9xd5l7xgwr61k2vqgahh2w54ds") (r "1.72")))

(define-public crate-fyrox-resource-0.12.0 (c (n "fyrox-resource") (v "0.12.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "fyrox-core") (r "^0.28.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "04csabdsvfv63xffa1j8zqljw6w70jw5iar0k31px6rslp58rz53") (r "1.72")))

