(define-module (crates-io fy ro fyrox-math) #:use-module (crates-io))

(define-public crate-fyrox-math-0.1.0 (c (n "fyrox-math") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rectutils") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "09mzq5mm7pihnqriw8023bcf8zf5mw1ib1g0f7qjbifl5wafc4a8") (r "1.72")))

(define-public crate-fyrox-math-0.2.0 (c (n "fyrox-math") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rectutils") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1caxyb6h7kn0xn8j0vyc2i237x0v891pqx6vmlr3m25528b6wz29") (r "1.72")))

