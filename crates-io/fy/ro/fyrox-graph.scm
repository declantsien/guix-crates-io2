(define-module (crates-io fy ro fyrox-graph) #:use-module (crates-io))

(define-public crate-fyrox-graph-0.1.0 (c (n "fyrox-graph") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "fyrox-core") (r "^0.28.1") (d #t) (k 0)) (d (n "fyrox-resource") (r "^0.12.0") (d #t) (k 0)))) (h "12pfw47zhgb0hi93n7wx7chq4zjv1a0hbgm1rzpx3l4w100xwq2z") (r "1.72")))

