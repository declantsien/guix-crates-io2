(define-module (crates-io fy ro fyrox-template) #:use-module (crates-io))

(define-public crate-fyrox-template-0.1.0 (c (n "fyrox-template") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "00gsy4p5ihk45izc2xd7y1i5gfgxjhhwlj6w1xigc03i8ly4wg38") (r "1.56")))

(define-public crate-fyrox-template-0.2.0 (c (n "fyrox-template") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rgzpp5lmli6mlsvk4i2c24zbr3gibdr9dg7xpzxpjdrbd635w3h") (r "1.56")))

(define-public crate-fyrox-template-0.3.0 (c (n "fyrox-template") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gb1pma805nip3kydni2k304npz7891nqzfhbrlpyibkffi90s3v") (r "1.56")))

(define-public crate-fyrox-template-0.4.0 (c (n "fyrox-template") (v "0.4.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vdas1q1v0lg2wczma3gdv3kkwsz7yz5n83ng0sw6p9ksii6xfbj") (r "1.56")))

(define-public crate-fyrox-template-0.5.0 (c (n "fyrox-template") (v "0.5.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0girmn6qi8245f95xj4s2lk3v1saqkv6d7qqc1c3y8p4dza1wybn") (r "1.56")))

(define-public crate-fyrox-template-0.5.1 (c (n "fyrox-template") (v "0.5.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0zi6sryifr72qjxkzjc187la7lab9jsafyr3982f2spp6li4nrzr") (r "1.56")))

(define-public crate-fyrox-template-0.6.0 (c (n "fyrox-template") (v "0.6.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "09j0mk9nb26v500xzhmpazkrbnf6fz42dbrixs91jj51fvgqdscj") (r "1.56")))

(define-public crate-fyrox-template-0.6.1 (c (n "fyrox-template") (v "0.6.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0dk2s3w99zc157avv1xpyqwf49bl2lxwzyn01jzj5yw35z321fi0") (r "1.56")))

(define-public crate-fyrox-template-0.7.0 (c (n "fyrox-template") (v "0.7.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.8") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "161q93wq3h06xpfb0yac28s65752vrp440qn7drshrl1f3v6krxn") (r "1.56")))

(define-public crate-fyrox-template-0.7.1 (c (n "fyrox-template") (v "0.7.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.8") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hi8rsqi98v9893qf51aynqfxnkjs7bqpnajx525lwqz334mmny6") (r "1.56")))

(define-public crate-fyrox-template-0.7.2 (c (n "fyrox-template") (v "0.7.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.8") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0p4s2z0n54p70x44lmimqkji2c042anjv5iznzll2253m0l9azf1") (r "1.56")))

(define-public crate-fyrox-template-0.8.0 (c (n "fyrox-template") (v "0.8.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.8") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0yix006rqchbhv6ndl0qx2qm7lc29x1klbhg6jknpwrv02pa1s13") (r "1.56")))

(define-public crate-fyrox-template-0.9.0 (c (n "fyrox-template") (v "0.9.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.8") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1p46wa4223s5h9pldj2a4c9v56zvfbfy5h9s424mdkf5arifycpy") (r "1.56")))

(define-public crate-fyrox-template-0.9.1 (c (n "fyrox-template") (v "0.9.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.8") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0pipbrs4r18b8wgyapijadxkpxw29g7ccyyr4h60rw44nxvmghkb") (r "1.56")))

(define-public crate-fyrox-template-0.10.0 (c (n "fyrox-template") (v "0.10.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.8") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1aabbjdvjc7zs3pmavizqdy9l30b4bdcs5w68h5ah2i4p6qnyn1i") (r "1.72")))

(define-public crate-fyrox-template-0.11.0 (c (n "fyrox-template") (v "0.11.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.6") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0dbgspk5pahaqh7zg0gqjdzpcdnw99621d8lpd60gk0y579y97mm") (r "1.72")))

