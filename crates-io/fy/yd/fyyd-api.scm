(define-module (crates-io fy yd fyyd-api) #:use-module (crates-io))

(define-public crate-fyyd-api-0.1.0 (c (n "fyyd-api") (v "0.1.0") (h "0y4n7qcfzcbbw1qhm57b17ga1di389n1qgrl4bavyscdycss6s7z")))

(define-public crate-fyyd-api-0.1.1 (c (n "fyyd-api") (v "0.1.1") (d (list (d (n "fyyd-types") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wi7na53yi2p2a67vv35g8n573r40brb22qq2plrp48hry57awmp")))

(define-public crate-fyyd-api-0.1.2 (c (n "fyyd-api") (v "0.1.2") (d (list (d (n "fyyd-types") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vfp2pkh3if1bqrhcygscjilrfbh2ib333h08a02n2s4zfrgx5hd")))

(define-public crate-fyyd-api-0.1.3 (c (n "fyyd-api") (v "0.1.3") (d (list (d (n "fyyd-types") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0ikjbxq8b4grqww56xlzpw97gpxs6i1fvwa5pilmnxym8im02l49")))

