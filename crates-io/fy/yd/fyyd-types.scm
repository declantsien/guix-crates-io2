(define-module (crates-io fy yd fyyd-types) #:use-module (crates-io))

(define-public crate-fyyd-types-0.1.0 (c (n "fyyd-types") (v "0.1.0") (h "0iiy6whc4pgkss6wfjyk5nxbhdn18jn8vbrmxqbiy42rdx9rc3vi")))

(define-public crate-fyyd-types-0.1.1 (c (n "fyyd-types") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f8md0a4k5l8zgdyhac6f0jjpb0xaxrdqs2v0gry37kpr8zvi0sw")))

(define-public crate-fyyd-types-0.1.2 (c (n "fyyd-types") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x8j14hvnhylfncvh7524h04f204idywi13ynfizd3xqh1zbqp74")))

