(define-module (crates-io fy od fyodor) #:use-module (crates-io))

(define-public crate-fyodor-0.1.0 (c (n "fyodor") (v "0.1.0") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1i6z5j152h6apqrbzphyxz697h4hdd87h7r6yqj2yznsfkx4rba8")))

(define-public crate-fyodor-0.1.2 (c (n "fyodor") (v "0.1.2") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "03kspjl20b42c8qa4yqfhrrnxyq4p3j038rrdhfz7w477q45rfcd")))

(define-public crate-fyodor-0.1.3 (c (n "fyodor") (v "0.1.3") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "14jvkmjys2x1xdmzm8yjx6mrlgnwy9d74k7qmg38s9l7v706y4g8")))

(define-public crate-fyodor-0.2.0 (c (n "fyodor") (v "0.2.0") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1ingqjxcs9gjz9i45pmsjcgzl85i5wv9r9wgmii3psg4az8xhpcf")))

(define-public crate-fyodor-0.2.1 (c (n "fyodor") (v "0.2.1") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0grxwqgq0bszw6vb2m127lazqc2r5ksamgdbkv00728riqmkdpqf")))

(define-public crate-fyodor-0.2.2 (c (n "fyodor") (v "0.2.2") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "06zalrjqn1l6sh4q27ndc24wvf7dbfwjbkxsx1rl9jhdvxyhaky0")))

(define-public crate-fyodor-0.2.3 (c (n "fyodor") (v "0.2.3") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "187wyx7aqczcis6i54k2yjw31bvyixydph00z5ff56y52mb0plq8")))

(define-public crate-fyodor-0.3.0 (c (n "fyodor") (v "0.3.0") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1hg0r386s7yr2q1n6inz7994sgf53n8qxpdnxq2gnwgr2nxa9aa5")))

(define-public crate-fyodor-0.4.0 (c (n "fyodor") (v "0.4.0") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lipsum") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0znp3w34ckkzqavjc64mm7p0dwvqcywkbfa5yhhblszmqa3c8wa3")))

