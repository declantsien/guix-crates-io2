(define-module (crates-io fk l_ fkl_codegen_java) #:use-module (crates-io))

(define-public crate-fkl_codegen_java-0.2.0 (c (n "fkl_codegen_java") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "fkl-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "genco") (r "^0.17.2") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)))) (h "1d9h30s9qiggavla4bcqs8y5pd3x9b5mxbd57wg5dm6wv30n5xw1")))

(define-public crate-fkl_codegen_java-0.2.1 (c (n "fkl_codegen_java") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "fkl-parser") (r "^0.2.1") (d #t) (k 0)) (d (n "genco") (r "^0.17.2") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)))) (h "0qd4fwgl7nn27qky4ryiw139wx6dh59y493wv9paxwb1g1j5ksvs")))

(define-public crate-fkl_codegen_java-0.2.2 (c (n "fkl_codegen_java") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "fkl-parser") (r "^0.2.2") (d #t) (k 0)) (d (n "genco") (r "^0.17.2") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)))) (h "1myfqicwf7s557g3alj0n1l9wm7gwhk9kvqyxws2qalflqkdn7xh")))

(define-public crate-fkl_codegen_java-0.2.3 (c (n "fkl_codegen_java") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "fkl-parser") (r "^0.2.2") (d #t) (k 0)) (d (n "genco") (r "^0.17.2") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)))) (h "0vmaj6aazld4b3bglv3aippf96znnv9b9igc50vzji24vv2xwgy8")))

(define-public crate-fkl_codegen_java-0.3.0 (c (n "fkl_codegen_java") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "fkl-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "genco") (r "^0.17.2") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)))) (h "0r875as05axxbzav200jrfdsp90b590sg10yczaw2mdgdllsh00x")))

(define-public crate-fkl_codegen_java-0.4.0 (c (n "fkl_codegen_java") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "fkl-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "genco") (r "^0.17.2") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)))) (h "0qp4mvvisvkcshnvs7jp08rw92g8fkz776wpg2syg62j0bsh823s")))

