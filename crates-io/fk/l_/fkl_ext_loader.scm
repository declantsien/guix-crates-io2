(define-module (crates-io fk l_ fkl_ext_loader) #:use-module (crates-io))

(define-public crate-fkl_ext_loader-0.4.0 (c (n "fkl_ext_loader") (v "0.4.0") (d (list (d (n "fkl_ext_api") (r "^0.4.0") (d #t) (k 0)) (d (n "fkl_mir") (r "^0.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1dwzcd89ll7r70ii21bcj9yfx9yd9vxihkal2c3bq5awwmy1as6z")))

