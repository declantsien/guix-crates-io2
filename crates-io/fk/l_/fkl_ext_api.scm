(define-module (crates-io fk l_ fkl_ext_api) #:use-module (crates-io))

(define-public crate-fkl_ext_api-0.4.0 (c (n "fkl_ext_api") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fkl_mir") (r "^0.4.0") (d #t) (k 0)))) (h "1pdn29n3i2ia9hm4livfbv83vr8gcqqrgycqwbgr9n8s7zj2bp2s")))

