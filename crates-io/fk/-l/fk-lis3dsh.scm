(define-module (crates-io fk -l fk-lis3dsh) #:use-module (crates-io))

(define-public crate-fk-lis3dsh-0.1.0 (c (n "fk-lis3dsh") (v "0.1.0") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "num_enum") (r "~0.5") (k 0)))) (h "1ra73plgvay1m8f5ywipi05l6h6mkidppl441yr9cfdwydjqm0rl")))

