(define-module (crates-io fk l- fkl-parser) #:use-module (crates-io))

(define-public crate-fkl-parser-0.2.0 (c (n "fkl-parser") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "pest") (r "^2.3.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09a9k115k8kf18019z4c11nrsw892wpl41nmx52jg9acs1imsdhx")))

(define-public crate-fkl-parser-0.2.1 (c (n "fkl-parser") (v "0.2.1") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "pest") (r "^2.3.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kdyk3wjpvd8izmq5ll17bz2pfykd85xhappjczm8rcd2m07mzy3")))

(define-public crate-fkl-parser-0.2.2 (c (n "fkl-parser") (v "0.2.2") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "pest") (r "^2.3.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "060gqx4awr07k605gynzvsbfcrfl2swyra867l3mrasgps850yk0")))

(define-public crate-fkl-parser-0.2.3 (c (n "fkl-parser") (v "0.2.3") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "mockall") (r "^0.11.2") (d #t) (k 2)) (d (n "pest") (r "^2.3.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00glsl0v12aaqjky38787cbf60r6bngih9s5j8zxl0mzlkn11kvw")))

(define-public crate-fkl-parser-0.3.0 (c (n "fkl-parser") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "mockall") (r "^0.11.2") (d #t) (k 2)) (d (n "pest") (r "^2.3.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04v19zg7b9n586qxipbq508kakina3052n2150mf8i6832k506ca")))

(define-public crate-fkl-parser-0.4.0 (c (n "fkl-parser") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "mockall") (r "^0.11.2") (d #t) (k 2)) (d (n "pest") (r "^2.3.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0czringdf64ng22jc2ng0agql6fky6z1qibr7rjrisj4v1gi4749")))

