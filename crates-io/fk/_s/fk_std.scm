(define-module (crates-io fk _s fk_std) #:use-module (crates-io))

(define-public crate-fk_std-0.1.0 (c (n "fk_std") (v "0.1.0") (h "0vygp6ajx79pkw8m3vv2hi6ys37mg8sdc0bjp3mlng8496lhrmvg") (y #t)))

(define-public crate-fk_std-0.1.1 (c (n "fk_std") (v "0.1.1") (h "042ni5jvf55w3nvixcg4pmld3bdqqxhnpks20mvfgxpjn0b8yzcx") (y #t)))

(define-public crate-fk_std-0.1.2 (c (n "fk_std") (v "0.1.2") (h "0xx7456vyr8ia8adphc963fmg9brd42s8jf7pba09hnrivhizi9l") (y #t)))

(define-public crate-fk_std-0.2.2 (c (n "fk_std") (v "0.2.2") (h "139s1qmglgxmq14cgr4cbc54y14hs42pwwxi30br4fic1068axqh") (y #t)))

(define-public crate-fk_std-0.2.3 (c (n "fk_std") (v "0.2.3") (h "17kvr7vcpxbby5xqc918y5ayyfl3pqb3kqgsm8aranw8mmfmxwd1") (y #t)))

(define-public crate-fk_std-0.3.3 (c (n "fk_std") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.85") (d #t) (k 0)))) (h "16d90cgzy56dcifsd8fm051xb999w4aiv6zalgnxr61davh7rqgn") (y #t)))

(define-public crate-fk_std-0.3.4 (c (n "fk_std") (v "0.3.4") (h "12ixxvjv5sggw4sjm7ihs0g4w879dnvgqf5wac1ls3csnv2c4sln") (y #t)))

