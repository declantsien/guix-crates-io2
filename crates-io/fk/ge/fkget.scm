(define-module (crates-io fk ge fkget) #:use-module (crates-io))

(define-public crate-fkget-0.1.0 (c (n "fkget") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1a1riakcggy6nsf5s6j8bhil5ymhgmp0fvipm8lnlz04jfzj5qfg") (y #t)))

(define-public crate-fkget-0.2.0 (c (n "fkget") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1820mwfdcp8l1jcv53wgzzvbipqcka4z5r14fl8gr7n8k0g3pcjr") (y #t)))

(define-public crate-fkget-0.3.0 (c (n "fkget") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1rpyddk7xmwqknklcjm6xxqpwq8rh7zm6r3nmfha7jc9k6wv9k8c") (y #t)))

(define-public crate-fkget-0.4.0 (c (n "fkget") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0f35isdhpninwfwpc814sbdvwafm7bm5vm15067q5akh66b49zgm") (y #t)))

(define-public crate-fkget-0.5.0 (c (n "fkget") (v "0.5.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "02giq8l7hp0a83iwbrd1frkbhxcvkq28wrr99qk2v58v5aamp2r3") (y #t)))

(define-public crate-fkget-0.6.0 (c (n "fkget") (v "0.6.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0n565p33ksp0hm5484l117hgm0rnvppdkrsqzkfwzh3bgkqlsgas") (y #t)))

(define-public crate-fkget-0.7.0 (c (n "fkget") (v "0.7.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rusoto_core") (r "^0.48.0") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.48.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "01p3yx75b5h4hpx6qpv1vch0k50dznzac3vbrvv2qckzjfmswyab")))

(define-public crate-fkget-0.7.1 (c (n "fkget") (v "0.7.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rusoto_core") (r "^0.48.0") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.48.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "008xpaxr7fn7qdf2dnpk2a4hhsyaf0pvl0yp7jrkyx1x31l69i1y")))

