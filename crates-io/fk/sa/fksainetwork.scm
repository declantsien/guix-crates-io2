(define-module (crates-io fk sa fksainetwork) #:use-module (crates-io))

(define-public crate-fksainetwork-0.1.0 (c (n "fksainetwork") (v "0.1.0") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "bincode_derive") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0sxhha5xxccn01zqgsywcm1wcvwn7rly3a4mw779kg99cpkcx4la")))

(define-public crate-fksainetwork-0.1.1 (c (n "fksainetwork") (v "0.1.1") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "bincode_derive") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0va8nri7b0qv8yypiy6qi7zwn0s167zw8fxwfpi5qxp04mkmfbyj")))

(define-public crate-fksainetwork-0.1.2 (c (n "fksainetwork") (v "0.1.2") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "bincode_derive") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "103r7ypiiz27qsvc4aqw3x3vrmi9pd9kc26lfhpl9hn3z8za3hjq")))

(define-public crate-fksainetwork-0.1.3 (c (n "fksainetwork") (v "0.1.3") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "bincode_derive") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fxi6mgs4hadlgnzxglib38cvr0b59sqy0n1bh2lhcmhbz40lj7b")))

(define-public crate-fksainetwork-0.2.0 (c (n "fksainetwork") (v "0.2.0") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "bincode_derive") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "bmp") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0472660na3wwsqw17sidjrkdfpc0615sf37l57kx8xbq72x8iadg")))

