(define-module (crates-io hd fs hdfs) #:use-module (crates-io))

(define-public crate-hdfs-0.0.1 (c (n "hdfs") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3.5") (d #t) (k 1)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "url") (r "^0.2.31") (d #t) (k 0)))) (h "1fanc1wggsvfl0jld6ihb3c03xnfi9r586fi034ynqrpy48kmsia")))

(define-public crate-hdfs-0.0.2 (c (n "hdfs") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3.5") (d #t) (k 1)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "url") (r "^0.2.31") (d #t) (k 0)))) (h "0baq6g1jrwra752cl7sy1sjkpa77cza0qh9fb4ak90vcks1j50c6")))

(define-public crate-hdfs-0.0.3 (c (n "hdfs") (v "0.0.3") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "itertools") (r "^0.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "log") (r "^0.3.2") (d #t) (k 0)) (d (n "url") (r "^0.2.37") (d #t) (k 0)))) (h "0i2fqajrcpd0y8s53qcfigls80730p998hkiq5ji1plqvd6qlgn5")))

(define-public crate-hdfs-0.0.4 (c (n "hdfs") (v "0.0.4") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "itertools") (r "^0.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "log") (r "^0.3.2") (d #t) (k 0)) (d (n "url") (r "^0.2.37") (d #t) (k 0)))) (h "1qvjlghwz9nhrwpy7vnv80b8j5k3fr2npzi743v0rhix3jflyaa6")))

