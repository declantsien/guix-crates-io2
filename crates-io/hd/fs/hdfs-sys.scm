(define-module (crates-io hd fs hdfs-sys) #:use-module (crates-io))

(define-public crate-hdfs-sys-0.0.0 (c (n "hdfs-sys") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 1)) (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0qh5plybzrald19fd4si53zka5bnji53204iakp6mq37q82hc6q2")))

(define-public crate-hdfs-sys-0.0.1 (c (n "hdfs-sys") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 1)) (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.124") (d #t) (k 2)) (d (n "regex") (r "^1.5.5") (d #t) (k 1)))) (h "1zscvlh2bydncx2j55k341hz0wdv58npmsbiqjix718x0jwk8slv")))

(define-public crate-hdfs-sys-0.0.2 (c (n "hdfs-sys") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 1)) (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0ffnszisx8i1sgg6cq7sba3rb5xc55zkx8vpn1jk3n3m4aa3m5y1")))

(define-public crate-hdfs-sys-0.0.3 (c (n "hdfs-sys") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 1)) (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "19c9ilblzsyc0x7qyazm470ay525b7rxykwsjlnpnsdcyf6vd14l") (l "hdfs")))

(define-public crate-hdfs-sys-0.0.4 (c (n "hdfs-sys") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "15gajjxx1n5p6mxh1ffjg8w1m3kf1zklna1wxnl5yh04ny1j9vd3") (f (quote (("hdfs_3_3_2" "hdfs_2_10_1" "hdfs_3_2_3") ("hdfs_3_2_3" "hdfs_2_10_1") ("hdfs_2_10_1") ("default" "hdfs_2_10_1")))) (l "hdfs")))

(define-public crate-hdfs-sys-0.0.5 (c (n "hdfs-sys") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1llbk8hdqi4z9v6j7anrmp2fjh8xsg4g5llwakiizcbwg9nvb67d") (f (quote (("hdfs_3_3_2" "hdfs_2_10_1" "hdfs_3_2_3") ("hdfs_3_2_3" "hdfs_2_10_1") ("hdfs_2_10_1") ("default" "hdfs_2_10_1")))) (l "hdfs")))

(define-public crate-hdfs-sys-0.1.0 (c (n "hdfs-sys") (v "0.1.0") (h "10in400g24pilz2cbdkw3pq8rw3ylkkq4rnxpppnyhqc4r5c9n0c") (f (quote (("hdfs_3_3" "hdfs_3_2") ("hdfs_3_2" "hdfs_3_1") ("hdfs_3_1" "hdfs_3_0") ("hdfs_3_0" "hdfs_2_10") ("hdfs_2_9" "hdfs_2_8") ("hdfs_2_8" "hdfs_2_7") ("hdfs_2_7" "hdfs_2_6") ("hdfs_2_6" "hdfs_2_5") ("hdfs_2_5" "hdfs_2_4") ("hdfs_2_4" "hdfs_2_3") ("hdfs_2_3" "hdfs_2_2") ("hdfs_2_2") ("hdfs_2_10" "hdfs_2_9") ("default" "hdfs_2_2")))) (l "hdfs")))

(define-public crate-hdfs-sys-0.2.0 (c (n "hdfs-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "04ya3vpcjzgavlgsqysq0mvz1ll3il18gywbiglnj1favfhgvp3k") (f (quote (("hdfs_3_3" "hdfs_3_2") ("hdfs_3_2" "hdfs_3_1") ("hdfs_3_1" "hdfs_3_0") ("hdfs_3_0" "hdfs_2_10") ("hdfs_2_9" "hdfs_2_8") ("hdfs_2_8" "hdfs_2_7") ("hdfs_2_7" "hdfs_2_6") ("hdfs_2_6" "hdfs_2_5") ("hdfs_2_5" "hdfs_2_4") ("hdfs_2_4" "hdfs_2_3") ("hdfs_2_3" "hdfs_2_2") ("hdfs_2_2") ("hdfs_2_10" "hdfs_2_9") ("default" "hdfs_2_2")))) (l "hdfs")))

(define-public crate-hdfs-sys-0.3.0 (c (n "hdfs-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "java-locator") (r "^0.1") (d #t) (k 1)))) (h "1js9ki2as456whdwk4v51xd74cisjqzljajdnhk1mmd2zg7dbqik") (f (quote (("vendored") ("hdfs_3_3" "hdfs_3_2") ("hdfs_3_2" "hdfs_3_1") ("hdfs_3_1" "hdfs_3_0") ("hdfs_3_0" "hdfs_2_10") ("hdfs_2_9" "hdfs_2_8") ("hdfs_2_8" "hdfs_2_7") ("hdfs_2_7" "hdfs_2_6") ("hdfs_2_6" "hdfs_2_5") ("hdfs_2_5" "hdfs_2_4") ("hdfs_2_4" "hdfs_2_3") ("hdfs_2_3" "hdfs_2_2") ("hdfs_2_2") ("hdfs_2_10" "hdfs_2_9") ("default" "hdfs_2_6")))) (l "hdfs")))

