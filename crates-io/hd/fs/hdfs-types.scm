(define-module (crates-io hd fs hdfs-types) #:use-module (crates-io))

(define-public crate-hdfs-types-0.1.0 (c (n "hdfs-types") (v "0.1.0") (d (list (d (n "prost") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (f (quote ("codegen" "prost"))) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)) (d (n "valuable") (r "^0.1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0a2aqkn1s2mqs9gw3nx8id5yqi0x0j7hzhh3988z9vls9njx0bvw") (f (quote (("default")))) (s 2) (e (quote (("valuable" "dep:valuable") ("serde" "dep:serde"))))))

