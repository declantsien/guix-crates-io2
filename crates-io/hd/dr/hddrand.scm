(define-module (crates-io hd dr hddrand) #:use-module (crates-io))

(define-public crate-hddrand-0.1.1 (c (n "hddrand") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "size") (r "^0.1.2") (d #t) (k 0)))) (h "16nw18a4i8qx15abgd7b99bavwcsxkqrh6rrzd4s1nhq5hb1d02i")))

(define-public crate-hddrand-0.1.2 (c (n "hddrand") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "size") (r "^0.4.0") (d #t) (k 0)))) (h "0bsnyqs55a2dh1xxwv1nr1apwvcxm7nqa6kwwraka7hjzwk4hkmv")))

