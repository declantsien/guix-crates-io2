(define-module (crates-io hd pa hdpath) #:use-module (crates-io))

(define-public crate-hdpath-0.1.0 (c (n "hdpath") (v "0.1.0") (h "1g6xjyhmq29l62r7f9lg4v270pc4rnp84fzbgkq5amcq7dqp5wwg")))

(define-public crate-hdpath-0.1.1 (c (n "hdpath") (v "0.1.1") (h "0da7zvj49yqfjhcv182m40m31v7z11spsx5cr2cc6kdia399fi01")))

(define-public crate-hdpath-0.2.0 (c (n "hdpath") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "17g69ldbvvcmyw6p8nxcigwphhn37k5awm0z30x7jxwcqv7j0d5f") (f (quote (("with-bitcoin" "bitcoin") ("default"))))))

(define-public crate-hdpath-0.2.1 (c (n "hdpath") (v "0.2.1") (d (list (d (n "bitcoin") (r ">=0.21") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0bl3jd8plc95xi9n0x4c0gl6winmx2p4mh79ycrz439i4m9648j7") (f (quote (("with-bitcoin" "bitcoin") ("default"))))))

(define-public crate-hdpath-0.3.0 (c (n "hdpath") (v "0.3.0") (d (list (d (n "bitcoin") (r ">=0.21") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "080ynaw0qsympc2al8w01dkk20r0hm5xz61rqc0ljn9asjhi2r40") (f (quote (("with-bitcoin" "bitcoin") ("default"))))))

(define-public crate-hdpath-0.3.1 (c (n "hdpath") (v "0.3.1") (d (list (d (n "bitcoin") (r ">=0.21") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0xwahr15g544d4n2v30yi4mkzmw4ikxj3172jicjhqinlkkx57l2") (f (quote (("with-bitcoin" "bitcoin") ("default"))))))

(define-public crate-hdpath-0.4.0 (c (n "hdpath") (v "0.4.0") (d (list (d (n "bitcoin") (r ">=0.21") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0fa9wkjc568898cdqvfbp40495jk133iykkk4608qj92h890xsf2") (f (quote (("with-bitcoin" "bitcoin") ("default"))))))

(define-public crate-hdpath-0.5.0 (c (n "hdpath") (v "0.5.0") (d (list (d (n "bitcoin") (r ">=0.21") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "03lmrqmhh7mvj59qkky0i7y55sm43lj0pbjnxazhmnc9azpzdkvs") (f (quote (("with-bitcoin" "bitcoin") ("default"))))))

(define-public crate-hdpath-0.6.0 (c (n "hdpath") (v "0.6.0") (d (list (d (n "bitcoin") (r ">=0.21") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0cm1ccqgxp5jl1xz0hw4wx9fxmbi283nvm7qvpyfqlh9gahzbbbj") (f (quote (("with-bitcoin" "bitcoin") ("default"))))))

(define-public crate-hdpath-0.6.1 (c (n "hdpath") (v "0.6.1") (d (list (d (n "bitcoin") (r ">=0.27") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "171dqgkyb5s24i97qsd3hwva27fyv53s31npk8rn9wjxv3jhkyys") (f (quote (("with-bitcoin" "bitcoin") ("default"))))))

(define-public crate-hdpath-0.6.2 (c (n "hdpath") (v "0.6.2") (d (list (d (n "bitcoin") (r ">=0.27") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1k0sq7sqf7kz9pk628iwngg1gb1mqkvz6s3lnj0kkkj3z0aidbh9") (f (quote (("with-bitcoin" "bitcoin") ("default"))))))

(define-public crate-hdpath-0.6.3 (c (n "hdpath") (v "0.6.3") (d (list (d (n "bitcoin") (r ">=0.27, <0.30") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0pwk31wlppp5yk003kfi2gg6hvfhg2vigqiwymh2czf1nafvr9fz") (f (quote (("with-bitcoin" "bitcoin") ("default"))))))

