(define-module (crates-io hd c1 hdc1080) #:use-module (crates-io))

(define-public crate-hdc1080-0.1.0 (c (n "hdc1080") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "byteorder") (r "^1") (k 2)) (d (n "i2cdev") (r "^0.4.1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.1") (d #t) (k 2)))) (h "00x5fv52x02nfw2lbnjdm9mli74m22hknlgdsahlb8awgmv31wzw")))

