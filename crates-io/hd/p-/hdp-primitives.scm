(define-module (crates-io hd p- hdp-primitives) #:use-module (crates-io))

(define-public crate-hdp-primitives-0.1.0 (c (n "hdp-primitives") (v "0.1.0") (d (list (d (n "alloy-primitives") (r "^0.6.2") (f (quote ("rlp" "serde"))) (d #t) (k 0)) (d (n "alloy-rlp") (r "^0.3.4") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "1q2w7ndr2bgal30z8cgrf3iqvngvdz6jy7vc0f58v0nxm5bvpglg")))

(define-public crate-hdp-primitives-0.2.0 (c (n "hdp-primitives") (v "0.2.0") (d (list (d (n "alloy-primitives") (r "^0.6.2") (f (quote ("rlp" "serde"))) (d #t) (k 0)) (d (n "alloy-rlp") (r "^0.3.4") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "10nr73dq7il1cj84s053x1fi35rchpwkxr5924c9yar59r6lqi96")))

(define-public crate-hdp-primitives-0.3.0 (c (n "hdp-primitives") (v "0.3.0") (d (list (d (n "alloy-dyn-abi") (r "^0.6.2") (d #t) (k 0)) (d (n "alloy-primitives") (r "^0.6.2") (f (quote ("rlp" "serde"))) (d #t) (k 0)) (d (n "alloy-rlp") (r "^0.3.4") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "15yxm2qyb28wa6hy717gnk3rv9jrjbwzb7gabkv486ka0v0dw097")))

