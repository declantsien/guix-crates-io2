(define-module (crates-io hd p- hdp-provider) #:use-module (crates-io))

(define-public crate-hdp-provider-0.1.0 (c (n "hdp-provider") (v "0.1.0") (d (list (d (n "alloy-primitives") (r "^0.6.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "hdp-primitives") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0p59mzr4axgykxga25i9sgdfn3y844c99yvamcch2hyp66djipiq")))

(define-public crate-hdp-provider-0.2.0 (c (n "hdp-provider") (v "0.2.0") (d (list (d (n "alloy-primitives") (r "^0.6.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "hdp-primitives") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0byfyb8kqn5vw2asz5f2gddkpp2gsskhwb6v3j4c3jxcgqr9lxbl")))

