(define-module (crates-io hd lb hdlbr) #:use-module (crates-io))

(define-public crate-hdlbr-0.1.0 (c (n "hdlbr") (v "0.1.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "handlebars") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)))) (h "1g7n72ip1vrcx45kbwfv91gpvc7jys6ribi1nsswvf2vjyk9glmn")))

(define-public crate-hdlbr-0.1.1 (c (n "hdlbr") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "handlebars") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1lmbcq6r69666868r7c0ni0q216acdxagm3dw5cgagb0xfr8czb8")))

