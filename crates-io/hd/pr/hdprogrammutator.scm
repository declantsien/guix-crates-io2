(define-module (crates-io hd pr hdprogrammutator) #:use-module (crates-io))

(define-public crate-HDprogrammutator-0.1.1 (c (n "HDprogrammutator") (v "0.1.1") (d (list (d (n "hdrepresentation") (r "^0.1.6") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "1sn5z537ir5cw8wdpq0i8z6lj77y66sxsrpv1afwc0vq95jd242i")))

(define-public crate-HDprogrammutator-0.1.2 (c (n "HDprogrammutator") (v "0.1.2") (d (list (d (n "hdrepresentation") (r "^0.1.6") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "0f1isw3iw0h8b3w2hbq6fk4zkf85dky57cnsq2sa4b2na0cd8nlc")))

(define-public crate-HDprogrammutator-0.1.21 (c (n "HDprogrammutator") (v "0.1.21") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "10yraz6sdbn72hihjlvaw4rbg6a09ggzir1bglkrpzhbs7fpny62")))

(define-public crate-HDprogrammutator-0.1.22 (c (n "HDprogrammutator") (v "0.1.22") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "1pwl9060h2wdf8kbn2bb05jy65qib2zlrwhw1i08f0q029032sjv")))

(define-public crate-HDprogrammutator-0.1.23 (c (n "HDprogrammutator") (v "0.1.23") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "0y09wmlzk09nnq6gj6rj3b21342p7fc9yfqvfpwhw0ncprcrb1pd")))

(define-public crate-HDprogrammutator-0.1.24 (c (n "HDprogrammutator") (v "0.1.24") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "05ncqgc7ymxpwi4wksm91m1pb6b4qrjfwwc74lv47899x0scr38r")))

(define-public crate-HDprogrammutator-0.1.25 (c (n "HDprogrammutator") (v "0.1.25") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "1f7562shi6mlja8f965qzcz94w5iqcmjr3pp6yhqq2ackqa8n919")))

(define-public crate-HDprogrammutator-0.1.26 (c (n "HDprogrammutator") (v "0.1.26") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "1nzrv5pslsf88sqdgicswspay4xby83x9knq6qw5ddf23dapfqsy")))

(define-public crate-HDprogrammutator-0.1.261 (c (n "HDprogrammutator") (v "0.1.261") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "0kv7fzca2fwy2k6wak7fzqq3vy8zkhpidr1lnvlc7w5mw3jwmp4q")))

(define-public crate-HDprogrammutator-0.1.262 (c (n "HDprogrammutator") (v "0.1.262") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "05fd56kb40p1sycc1czkp23nfl67qvhpln9mcsi5gmfw5lz6ryv5")))

(define-public crate-HDprogrammutator-0.1.263 (c (n "HDprogrammutator") (v "0.1.263") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "1m6xq6g1271qcnw2g3vnzg4fnkl8hk93dk1wi1883wg890ji44zd")))

(define-public crate-HDprogrammutator-0.1.264 (c (n "HDprogrammutator") (v "0.1.264") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "18544j4khxb3rrps1nz6wb08myb9dia6qsaivfj6hzl5h0spfrs9")))

