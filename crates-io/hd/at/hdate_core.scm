(define-module (crates-io hd at hdate_core) #:use-module (crates-io))

(define-public crate-hdate_core-0.1.0 (c (n "hdate_core") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "1pw4ihwm20yy9hx838m80l9gg97s576rrkil381wj9clz85db1bc")))

(define-public crate-hdate_core-0.1.1 (c (n "hdate_core") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "1la34y3jn9k75f3jq92wlz95fwwk1w0a4hv5b7cwxiyd30hy00i6")))

