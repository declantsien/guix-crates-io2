(define-module (crates-io hd at hdate) #:use-module (crates-io))

(define-public crate-hdate-0.1.0 (c (n "hdate") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "hdate_core") (r "^0.1.0") (d #t) (k 0)))) (h "00xadm41w2wnq3b8kbc09ymhmsw20p6p6q6nd2y7gwc5qnzjx6pl")))

(define-public crate-hdate-0.1.1 (c (n "hdate") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "hdate_core") (r "^0.1.1") (d #t) (k 0)))) (h "0n78sxw39myvd54gp1pgsj5ddq2g9g7y0z7a6pd764k0c80b3ihj")))

