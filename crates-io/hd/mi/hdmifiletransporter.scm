(define-module (crates-io hd mi hdmifiletransporter) #:use-module (crates-io))

(define-public crate-hdmifiletransporter-0.1.0 (c (n "hdmifiletransporter") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.12") (d #t) (k 0)) (d (n "opencv") (r "^0.75.0") (d #t) (k 0)))) (h "0qgkv2jfpcljm3gfv69bi5dy49x74vfvf3xdfz4r1sbf39j2xriv")))

(define-public crate-hdmifiletransporter-0.1.1 (c (n "hdmifiletransporter") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.12") (d #t) (k 0)) (d (n "opencv") (r "^0.75.0") (d #t) (k 0)))) (h "01nns6nsw9rngayv4f9r0ps7zypdrsi1dmfclw1dp0vakc9pyxjv")))

(define-public crate-hdmifiletransporter-0.1.2 (c (n "hdmifiletransporter") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.12") (d #t) (k 0)) (d (n "opencv") (r "^0.75.0") (d #t) (k 0)))) (h "049kibyfn5wqs72gv63h6kl6lfsxzkrxjgl74w60rfys6n7bw6c0")))

