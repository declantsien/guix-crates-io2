(define-module (crates-io hd rh hdrhistogram-c) #:use-module (crates-io))

(define-public crate-hdrhistogram-c-0.1.13 (c (n "hdrhistogram-c") (v "0.1.13") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "05aznjv9nyc7rm769dqlkdmpw90h5im2ja055d5vy2xjs9i4c28w")))

(define-public crate-hdrhistogram-c-0.1.14 (c (n "hdrhistogram-c") (v "0.1.14") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "062n8p46q9y1hdzan6ny5kydfww9890zsigi7jjy294vzb5xcaa7") (y #t)))

(define-public crate-hdrhistogram-c-0.1.15 (c (n "hdrhistogram-c") (v "0.1.15") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0mrcgl1h169f6z8gkxklawh3wkc7k7y553d33hnc270rbbm4y8r8") (y #t)))

(define-public crate-hdrhistogram-c-0.1.16 (c (n "hdrhistogram-c") (v "0.1.16") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0qg69fzqv18kl3ppwjm7p6x504q148r3l79i477jn04ngd21dqdg") (y #t)))

(define-public crate-hdrhistogram-c-0.1.17 (c (n "hdrhistogram-c") (v "0.1.17") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0zli88n7rizca69v6rd08zka61wlxircfzq4d3a0hdcgz2yxsf4j")))

