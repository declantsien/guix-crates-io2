(define-module (crates-io hd rh hdrhist) #:use-module (crates-io))

(define-public crate-hdrhist-0.5.0 (c (n "hdrhist") (v "0.5.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "textplots") (r "^0.2") (d #t) (k 2)))) (h "10xkkcn168m5skbdg2sspfl0y2nxl4b32zg7avngzaipnpgjxcji")))

