(define-module (crates-io hd wa hdwallet_rs) #:use-module (crates-io))

(define-public crate-hdwallet_rs-0.1.0 (c (n "hdwallet_rs") (v "0.1.0") (d (list (d (n "base58") (r "^0.1") (d #t) (k 0)) (d (n "bip0039") (r "^0.10.1") (f (quote ("czech" "french" "italian" "japanese" "korean" "portuguese" "spanish" "chinese-simplified" "chinese-traditional"))) (d #t) (k 0)) (d (n "curve25519-dalek") (r "^3.2.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.4.0") (d #t) (k 0)))) (h "0kppkl2nvq3idfahmgn3kkzmxhswvb6y27cqww5v3dwfrrg798w3")))

