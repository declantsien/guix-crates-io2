(define-module (crates-io hd wa hdwallet-bitcoin) #:use-module (crates-io))

(define-public crate-hdwallet-bitcoin-0.1.0 (c (n "hdwallet-bitcoin") (v "0.1.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "hdwallet") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8.0") (d #t) (k 0)))) (h "13abq1ndrmy4sdrx8avwwi83a5yf3yw3crmrya4nbm4mjw8vk4cl")))

(define-public crate-hdwallet-bitcoin-0.1.1 (c (n "hdwallet-bitcoin") (v "0.1.1") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "hdwallet") (r "^0.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8.0") (d #t) (k 0)))) (h "1w836z9r201dn6sf4w2ywm9ydnk2nlb0pnal8f748p0mickcq281")))

(define-public crate-hdwallet-bitcoin-0.2.1 (c (n "hdwallet-bitcoin") (v "0.2.1") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "hdwallet") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8.0") (d #t) (k 0)))) (h "0v4b8hsylgs8yx4l9hxja19lhafza346n0jp0n7z6avsmfwjv7vk")))

(define-public crate-hdwallet-bitcoin-0.2.2 (c (n "hdwallet-bitcoin") (v "0.2.2") (d (list (d (n "base58") (r "^0.1") (d #t) (k 0)) (d (n "hdwallet") (r "^0.2.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8") (d #t) (k 0)))) (h "1k916m9kkhbkf3s46xp0h2nv9cqxlaip51kf9k9bp1wmccy3d9jg")))

(define-public crate-hdwallet-bitcoin-0.2.5 (c (n "hdwallet-bitcoin") (v "0.2.5") (d (list (d (n "base58") (r "^0.1") (d #t) (k 0)) (d (n "hdwallet") (r "^0.2.5") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8") (d #t) (k 0)))) (h "1wywlbbx8fnv47c1lfhv16i3b9bkifk00ghxi3ks5gv7mlbwb05z")))

(define-public crate-hdwallet-bitcoin-0.3.0 (c (n "hdwallet-bitcoin") (v "0.3.0") (d (list (d (n "base58") (r "^0.1") (d #t) (k 0)) (d (n "hdwallet") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "ripemd160") (r "^0.8") (d #t) (k 0)))) (h "1yk0jwbgq0dqi8hpzba4a2bi00l2qcgcbxarpga6azhn0cz5374n")))

(define-public crate-hdwallet-bitcoin-0.4.0 (c (n "hdwallet-bitcoin") (v "0.4.0") (d (list (d (n "base58") (r "^0.2") (d #t) (k 0)) (d (n "hdwallet") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "ripemd") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "05jg1mn5mbbjg0hl8vb5i7pwkapqskmp4f3ysgbnc74il0sij1ac")))

(define-public crate-hdwallet-bitcoin-0.4.1 (c (n "hdwallet-bitcoin") (v "0.4.1") (d (list (d (n "base58") (r "^0.2") (d #t) (k 0)) (d (n "hdwallet") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "ripemd") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0hlm0lraqla5vwbxw593vbn8v4lmbl32f9hgz7g49bnyhqsk64j4")))

