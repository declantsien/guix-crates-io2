(define-module (crates-io hd wa hdwallet-fc) #:use-module (crates-io))

(define-public crate-hdwallet-fc-0.1.0 (c (n "hdwallet-fc") (v "0.1.0") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.27.0") (d #t) (k 0)) (d (n "hdpath") (r "^0.6") (f (quote ("with-bitcoin"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (d #t) (k 0)))) (h "1dhls2p0mk3g8xn47kilnk4lyps7b8jm688hmikc67k7lw8d13y9")))

(define-public crate-hdwallet-fc-0.2.0 (c (n "hdwallet-fc") (v "0.2.0") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.27.0") (d #t) (k 0)) (d (n "hdpath") (r "^0.6") (f (quote ("with-bitcoin"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (d #t) (k 0)))) (h "1lawwcw8ysgnylpz9345va99zx6qhbdcj48gvha7k4kzs1y6r8jj")))

(define-public crate-hdwallet-fc-0.2.1 (c (n "hdwallet-fc") (v "0.2.1") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.27.0") (d #t) (k 0)) (d (n "hdpath") (r "^0.6") (f (quote ("with-bitcoin"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (d #t) (k 0)))) (h "1jvpa8fyd4gqacz7l3jkc30klqfg2ndbqfdcs3qrv6akdfcrgx0h")))

(define-public crate-hdwallet-fc-0.2.2 (c (n "hdwallet-fc") (v "0.2.2") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.27.0") (d #t) (k 0)) (d (n "hdpath") (r "^0.6") (f (quote ("with-bitcoin"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (d #t) (k 0)))) (h "10r9v5azyyskcaa17zgwgm6infzgydb40jhvqlx4xrhwacvnk3dq")))

(define-public crate-hdwallet-fc-0.2.3 (c (n "hdwallet-fc") (v "0.2.3") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.27.0") (d #t) (k 0)) (d (n "hdpath") (r "^0.6") (f (quote ("with-bitcoin"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (d #t) (k 0)))) (h "0888q5s56a20d8j5bc4a24xnqnza6j79r8w1n699w0dijh47nraz")))

(define-public crate-hdwallet-fc-0.2.4 (c (n "hdwallet-fc") (v "0.2.4") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.27.0") (d #t) (k 0)) (d (n "hdpath") (r "^0.6") (f (quote ("with-bitcoin"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (d #t) (k 0)))) (h "10zvarqqawxc4k72i2mi5bkhplk3sh3xvrbdwzjb4cxqss48mnab")))

(define-public crate-hdwallet-fc-0.2.5 (c (n "hdwallet-fc") (v "0.2.5") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.31.1") (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (f (quote ("check"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "18gavi7aphirfjcp2ybhkrrdl47nxcmiprlvg2a8p9aybgpjfrh1")))

(define-public crate-hdwallet-fc-0.2.6 (c (n "hdwallet-fc") (v "0.2.6") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.31.1") (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (f (quote ("check"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "0m1fjyahrqldq6k7aywllfhfpdqy54fljzn4hldkcbf16mqlpkli")))

(define-public crate-hdwallet-fc-0.2.7 (c (n "hdwallet-fc") (v "0.2.7") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.31.1") (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (f (quote ("check"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "013m5n4qaayr9li0h29iaib4q8rq08fhp0209sk31w685y7vkbqz")))

(define-public crate-hdwallet-fc-0.2.8 (c (n "hdwallet-fc") (v "0.2.8") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.31.1") (d #t) (k 0)) (d (n "bs58") (r "^0.5.0") (f (quote ("check"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "10x9vyk9v2gnf0rvwppibd05wkyx88lgq7d9isf9swwqn0gjks8x")))

