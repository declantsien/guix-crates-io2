(define-module (crates-io hd wa hdwallet) #:use-module (crates-io))

(define-public crate-hdwallet-0.0.0 (c (n "hdwallet") (v "0.0.0") (h "1l1349jcfn5l8vkbdvq100jfzj5dsb36yszrgw8mdjmcq1y41brr")))

(define-public crate-hdwallet-0.0.1 (c (n "hdwallet") (v "0.0.1") (d (list (d (n "numext-fixed-uint") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "ring") (r "^0.14.0-alpha2") (d #t) (k 0)) (d (n "secp256k1") (r "^0.12") (d #t) (k 0)))) (h "1npjylv4clabl8m6p4c2aqhj0kpj5bgp4vfya508f45jwjk3saaw")))

(define-public crate-hdwallet-0.1.0 (c (n "hdwallet") (v "0.1.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "ring") (r "^0.14.0") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8.0") (d #t) (k 2)) (d (n "secp256k1") (r "^0.12") (d #t) (k 0)))) (h "05fjv2lr8nm2d16vsrpn13lvmi29d5vvvgr8x0rjyk803v637hli")))

(define-public crate-hdwallet-0.1.1 (c (n "hdwallet") (v "0.1.1") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "ring") (r "^0.14.0") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8.0") (d #t) (k 2)) (d (n "secp256k1") (r "^0.12") (d #t) (k 0)))) (h "14a3xsx3kka7g0rdxk67y7r46v2k65968cnk5vpbkqjmxh6g92xk")))

(define-public crate-hdwallet-0.2.0 (c (n "hdwallet") (v "0.2.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "ring") (r "^0.14.0") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8.0") (d #t) (k 2)) (d (n "secp256k1") (r "^0.12") (d #t) (k 0)))) (h "002y743dmi44sm91qjprdbzjbxx781m6iizkhip7s9dc21m3chi6")))

(define-public crate-hdwallet-0.2.1 (c (n "hdwallet") (v "0.2.1") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "ring") (r "^0.14.0") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8.0") (d #t) (k 2)) (d (n "secp256k1") (r "^0.12") (d #t) (k 0)))) (h "1lzp41w16sxmahiikq5d3jl0q8sskj7zsmvmpygh7hgnpccry6dg")))

(define-public crate-hdwallet-0.2.2 (c (n "hdwallet") (v "0.2.2") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "ring") (r "^0.14.0") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8.0") (d #t) (k 2)) (d (n "secp256k1") (r "^0.12") (d #t) (k 0)))) (h "1lgsfbk6p10bp0jjdg1ddy5sqnm5iv1ihl683cg9f2ccvw5gfp1b")))

(define-public crate-hdwallet-0.2.3 (c (n "hdwallet") (v "0.2.3") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8.0") (d #t) (k 2)) (d (n "secp256k1") (r "^0.12") (d #t) (k 0)))) (h "1ss3pncdksjrcgwgb1vv6sm10f0gdscrb1309qvj1q7hmc88y0h1")))

(define-public crate-hdwallet-0.2.4 (c (n "hdwallet") (v "0.2.4") (d (list (d (n "base58") (r "^0.1") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8") (d #t) (k 2)) (d (n "secp256k1") (r "^0.16") (d #t) (k 0)))) (h "1kimwn008k00nqw3pw7qcbxbmc356g5a8wqj9rsxrpzrnlkz6njy")))

(define-public crate-hdwallet-0.2.5 (c (n "hdwallet") (v "0.2.5") (d (list (d (n "base58") (r "^0.1") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8") (d #t) (k 2)) (d (n "secp256k1") (r "^0.17") (d #t) (k 0)))) (h "1qgkgbd4r7alk4j7la128giycjlaas2cfmjx937am8l1q71mrc9m")))

(define-public crate-hdwallet-0.3.0 (c (n "hdwallet") (v "0.3.0") (d (list (d (n "base58") (r "^0.1") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.1") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8") (d #t) (k 2)) (d (n "secp256k1") (r "^0.19") (d #t) (k 0)))) (h "1mzdvq5r32zibp21dq31jjpg76w1hd3b4pbp4rf8i56wmmpk4cr0")))

(define-public crate-hdwallet-0.3.1 (c (n "hdwallet") (v "0.3.1") (d (list (d (n "base58") (r "^0.1") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.1") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "ripemd160") (r "^0.8") (d #t) (k 2)) (d (n "secp256k1") (r "^0.21") (d #t) (k 0)))) (h "14q18xhj6z9is3zahhp8c13higcb2r40w405wphxn65y8grrpn4w")))

(define-public crate-hdwallet-0.4.0 (c (n "hdwallet") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "secp256k1") (r "^0.26") (d #t) (k 0)) (d (n "base58") (r "^0.1") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ripemd160") (r "^0.8") (d #t) (k 2)))) (h "0nyg8c84iz302n479f3k23mphpizqjbp9drwvck4vg4x4qmi1fw8")))

(define-public crate-hdwallet-0.4.1 (c (n "hdwallet") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "secp256k1") (r "^0.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "base58") (r "^0.1") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ripemd160") (r "^0.8") (d #t) (k 2)))) (h "19s83jg4pfq1ql2m1s1sd4lyd0v8z5grcla3rm91b94y9iyvl0ss")))

