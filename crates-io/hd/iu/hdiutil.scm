(define-module (crates-io hd iu hdiutil) #:use-module (crates-io))

(define-public crate-hdiutil-0.1.0 (c (n "hdiutil") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "binread") (r "^2.2.0") (d #t) (k 0)) (d (n "camino") (r "^1.1.4") (d #t) (k 0)))) (h "0z4rhdndj06p3nw8w2zk71k851j3rynvwzyslg67z78vf3r6fqn1")))

(define-public crate-hdiutil-0.1.1 (c (n "hdiutil") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "binread") (r "^2.2.0") (d #t) (k 0)) (d (n "camino") (r "^1.1.4") (d #t) (k 0)))) (h "09hpnp3jinawa485jz272ac9lqw316qvkh6szq6r7gjnvspsxwgd")))

