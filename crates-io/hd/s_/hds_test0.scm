(define-module (crates-io hd s_ hds_test0) #:use-module (crates-io))

(define-public crate-hds_test0-0.0.1 (c (n "hds_test0") (v "0.0.1") (h "0qv5f1vq1iz0l6q3109q2562h1gq1ff4m69qq1vqwpj9pvic41qm") (r "1.64.0")))

(define-public crate-hds_test0-0.0.2 (c (n "hds_test0") (v "0.0.2") (h "0kifr7d6vhvsrn2jkfyyc1m85p25p7h2w8pb1cyrwi05m8dlc685") (r "1.64.0")))

(define-public crate-hds_test0-0.0.3 (c (n "hds_test0") (v "0.0.3") (h "0r0nndgjjgn462xnx7i3xwvkk3sy19hnav0x2l45a1p1nbv203xm") (r "1.64.0")))

(define-public crate-hds_test0-0.0.4 (c (n "hds_test0") (v "0.0.4") (h "069zrjpiqvpf0cjywj6bim30bh09dbk0wp5nmb7rz7mdn25a5q98") (r "1.64.0")))

