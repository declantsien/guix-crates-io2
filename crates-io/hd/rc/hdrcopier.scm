(define-module (crates-io hd rc hdrcopier) #:use-module (crates-io))

(define-public crate-hdrcopier-0.1.0 (c (n "hdrcopier") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "0n8qsfj6l5gbhz3sszl70rj5fq9lw234g41hqcrlplva4ny70z5b") (r "1.56")))

(define-public crate-hdrcopier-0.1.1 (c (n "hdrcopier") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "1p7sgy3ysx7ryjcf0hqg8ypxww79r33k49jdajajzsw1zp6rmnps") (r "1.56")))

(define-public crate-hdrcopier-0.2.0 (c (n "hdrcopier") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "0psrg5hhlpwfaxn0wpsx5yi2v154aqwhxyd7lrpcic92mrzl777h") (r "1.56")))

(define-public crate-hdrcopier-0.2.1 (c (n "hdrcopier") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "10fawdzn1fi3625gb4b7dh3khnn0xx6awab59q5zafsv5g5m9xfz") (r "1.56")))

(define-public crate-hdrcopier-0.2.2 (c (n "hdrcopier") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "0vfbxd2bik7ingy10581xdgv5hbxmbyr8z2hafn3780f2k6bil8s") (r "1.56")))

(define-public crate-hdrcopier-0.2.3 (c (n "hdrcopier") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "03zsdba1xh89ld71amn5fchaaq787q303imkwsx9w1bcl5mx6kpk") (r "1.56")))

(define-public crate-hdrcopier-0.3.0 (c (n "hdrcopier") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "1ll1zg49w0spcsf29rh93kbxa94r8ah9g9dn7nf5y6lhff3hbkc0") (r "1.56")))

(define-public crate-hdrcopier-0.3.1 (c (n "hdrcopier") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "00vs0d6l7f7a0cyn9m8fgq131gkhf7iy4vvn805wa2ay3afr73kk") (r "1.56")))

(define-public crate-hdrcopier-0.3.2 (c (n "hdrcopier") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "12l6xf0wpspn95k7pi35d9a9kb49m1q04mb04gpr7nk56qkk2wib") (r "1.56")))

