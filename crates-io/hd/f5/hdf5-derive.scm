(define-module (crates-io hd f5 hdf5-derive) #:use-module (crates-io))

(define-public crate-hdf5-derive-0.5.0 (c (n "hdf5-derive") (v "0.5.0") (d (list (d (n "compiletest_rs") (r "^0.3.18") (f (quote ("stable"))) (d #t) (k 2)) (d (n "hdf5-types") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0bvasz8p3d6jqwmj68433wrck2iqfw0p671xl1vka1b3lyb8ilv0")))

(define-public crate-hdf5-derive-0.5.1 (c (n "hdf5-derive") (v "0.5.1") (d (list (d (n "compiletest_rs") (r "^0.3.18") (f (quote ("stable"))) (d #t) (k 2)) (d (n "hdf5") (r ">= 0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "07y0k3xccrvs9h1h2qqhpzyjfxsw1idn78zd17v09qasbx22rf30")))

(define-public crate-hdf5-derive-0.5.2 (c (n "hdf5-derive") (v "0.5.2") (d (list (d (n "compiletest_rs") (r "^0.3.22") (f (quote ("stable"))) (d #t) (k 2)) (d (n "hdf5") (r ">= 0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0j03i9xa760lsxx71r1ccmp7kz5a6j6iiwqpk1k5s8qzjlhhrcg0")))

(define-public crate-hdf5-derive-0.6.0 (c (n "hdf5-derive") (v "0.6.0") (d (list (d (n "compiletest_rs") (r "^0.3.22") (f (quote ("stable"))) (d #t) (k 2)) (d (n "hdf5") (r ">= 0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0wvina1338i3khzpnxjmjpfpszngd6cwldxvrq3y0x428k950rqn")))

(define-public crate-hdf5-derive-0.6.1 (c (n "hdf5-derive") (v "0.6.1") (d (list (d (n "hdf5") (r ">= 0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "06a8gc5h6v5h80avx0dj03ss6pwf2j9blg18c0frvpkm0bgcij4k")))

(define-public crate-hdf5-derive-0.7.0 (c (n "hdf5-derive") (v "0.7.0") (d (list (d (n "hdf5") (r ">=0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0lpzavg6n0y6rf03vfkif9c3xs3bmmjrjsrrps23w6xmc2h5l2z6")))

(define-public crate-hdf5-derive-0.7.1 (c (n "hdf5-derive") (v "0.7.1") (d (list (d (n "hdf5") (r ">=0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "00ncpdiwra3ffd27x4nq3kq5ddg351hkfgvsfg426m6iz5vbmpbf")))

(define-public crate-hdf5-derive-0.8.0 (c (n "hdf5-derive") (v "0.8.0") (d (list (d (n "hdf5") (r ">=0.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0qy2kmws6k8maqg685z2nxnx75bxsgkjxihrszgpkn5p041zgn0n")))

(define-public crate-hdf5-derive-0.8.1 (c (n "hdf5-derive") (v "0.8.1") (d (list (d (n "hdf5") (r ">=0.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "082849npa2a8r3h3cipyb74mwrmwp301hqah9mcq0s0ylk37m9x5")))

