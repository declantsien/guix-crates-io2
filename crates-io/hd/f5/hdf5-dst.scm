(define-module (crates-io hd f5 hdf5-dst) #:use-module (crates-io))

(define-public crate-hdf5-dst-0.1.0 (c (n "hdf5-dst") (v "0.1.0") (d (list (d (n "dst-container") (r "^0.1") (d #t) (k 0)) (d (n "hdf5") (r "^0.8") (d #t) (k 0)) (d (n "hdf5-dst-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1z8ynsg7q1yq6m78flpykb252pg9412p5k8dvvrcy6hqwbmmg3br")))

(define-public crate-hdf5-dst-0.1.1 (c (n "hdf5-dst") (v "0.1.1") (d (list (d (n "dst-container") (r "^0.1") (d #t) (k 0)) (d (n "hdf5") (r "^0.8") (d #t) (k 0)) (d (n "hdf5-dst-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "09kplzm74yxlyyiq28ghil53ysca8j8ipkq3iabsd3wy41dy61d4")))

