(define-module (crates-io hd f5 hdf5-src) #:use-module (crates-io))

(define-public crate-hdf5-src-0.7.0 (c (n "hdf5-src") (v "0.7.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libz-sys") (r "^1.0.25") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "1qva5fcyk5jjsa9jxcsn1lqswr1ly0vly5li68fpzk8ln4xv4100") (f (quote (("zlib" "libz-sys") ("threadsafe") ("hl") ("deprecated")))) (l "hdf5src")))

(define-public crate-hdf5-src-0.7.1 (c (n "hdf5-src") (v "0.7.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libz-sys") (r "^1.0.25") (f (quote ("static"))) (o #t) (d #t) (k 0)))) (h "17jr2xd03x5jgar37mhh1c66ik6c7yynykxkcp2la3nc2ahj98v4") (f (quote (("zlib" "libz-sys") ("threadsafe") ("hl") ("deprecated")))) (l "hdf5src")))

(define-public crate-hdf5-src-0.8.0 (c (n "hdf5-src") (v "0.8.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libz-sys") (r "^1.0.25") (f (quote ("static"))) (o #t) (k 0)))) (h "1a0q6b35xb6i9pclscnmpd3fis8y34f56ixfib1mqjl5pb40s6zc") (f (quote (("zlib" "libz-sys") ("threadsafe") ("hl") ("deprecated")))) (l "hdf5src")))

(define-public crate-hdf5-src-0.8.1 (c (n "hdf5-src") (v "0.8.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libz-sys") (r "^1.0.25") (f (quote ("static"))) (o #t) (k 0)))) (h "1a172wzpc8ywfb5j9mzyyihfwzq35h6n4gd8yaf53h6x77dr6570") (f (quote (("zlib" "libz-sys") ("threadsafe") ("hl") ("deprecated")))) (l "hdf5src")))

