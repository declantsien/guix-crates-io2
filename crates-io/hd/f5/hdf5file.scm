(define-module (crates-io hd f5 hdf5file) #:use-module (crates-io))

(define-public crate-hdf5file-0.0.1 (c (n "hdf5file") (v "0.0.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0wzcsaa5ikj71ggl4ls88fps0daxfp9fpc3s8r5dz1lji7dl9kxn")))

(define-public crate-hdf5file-0.1.0 (c (n "hdf5file") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "022818mi5vvkdxrp38gj7ymgnq92vxh40mzcb7jl8swkaghm0d5w")))

(define-public crate-hdf5file-0.1.1 (c (n "hdf5file") (v "0.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0frib7kkjwh3faisbsjx9pkbqxhs19dxrslxw25vwha9n1l46xvl")))

