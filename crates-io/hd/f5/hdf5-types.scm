(define-module (crates-io hd f5 hdf5-types) #:use-module (crates-io))

(define-public crate-hdf5-types-0.5.0 (c (n "hdf5-types") (v "0.5.0") (d (list (d (n "ascii") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "14nai0psfadcd9jifcl6qfp4fhsjrmnf356pr4rhm1dsnafdgw8s")))

(define-public crate-hdf5-types-0.5.1 (c (n "hdf5-types") (v "0.5.1") (d (list (d (n "ascii") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "1x83858x0cm58bz0dvmid0cb0inkf4rfpl4q6vbk1zriqihgqrpd")))

(define-public crate-hdf5-types-0.5.2 (c (n "hdf5-types") (v "0.5.2") (d (list (d (n "ascii") (r "^0.9") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "1asg1ak9pyziq9hn6dzbv14hssvdlxfdr25cr1s8vl282np0ah6n")))

(define-public crate-hdf5-types-0.6.0 (c (n "hdf5-types") (v "0.6.0") (d (list (d (n "ascii") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "0h4zx77rd2bndmj52dy7jb6140zkvvzqldgxd5kbnpmbf90z5nm7")))

(define-public crate-hdf5-types-0.6.1 (c (n "hdf5-types") (v "0.6.1") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)))) (h "1fp5jf1yc16p9rnnpyyklazlkk77k1d0j5lyvaq7zzcq10f6a66x")))

(define-public crate-hdf5-types-0.7.0 (c (n "hdf5-types") (v "0.7.0") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)))) (h "17n5zh5vpwxbd7m63rmhnp207l03jjhnwwlvii6rkn47wlvc669s")))

(define-public crate-hdf5-types-0.7.1 (c (n "hdf5-types") (v "0.7.1") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (k 2)))) (h "0bl7g2wdpdpzw6lc8d4rw3g985sq9j23lr6kl452mvxk8vcsa8s5") (f (quote (("const_generics"))))))

(define-public crate-hdf5-types-0.8.0 (c (n "hdf5-types") (v "0.8.0") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0a2v0fsgw0by5lvjna85svn33gsrar9xh1v801775fmfmrlb7db7") (f (quote (("h5-alloc"))))))

(define-public crate-hdf5-types-0.8.1 (c (n "hdf5-types") (v "0.8.1") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.8.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "13s27r4faij3iacwlbmrkzj7za77jiv6x2v3wpzv36dlvz06hwml") (f (quote (("h5-alloc"))))))

