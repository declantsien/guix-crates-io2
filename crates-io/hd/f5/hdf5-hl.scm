(define-module (crates-io hd f5 hdf5-hl) #:use-module (crates-io))

(define-public crate-hdf5-hl-0.1.0 (c (n "hdf5-hl") (v "0.1.0") (d (list (d (n "dst-container") (r "^0.1") (d #t) (k 0)) (d (n "generic-tests") (r "^0.1") (d #t) (k 2)) (d (n "hdf5") (r "^0.8") (d #t) (k 0)) (d (n "hdf5-dst") (r "^0.1.0") (d #t) (k 0)) (d (n "hdf5-hl-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "03vn6clw08gq7ncrxjpp60dx5sf1ixgg651zgg8yiyk6s6m2j75m")))

