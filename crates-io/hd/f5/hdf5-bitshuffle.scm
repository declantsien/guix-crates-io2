(define-module (crates-io hd f5 hdf5-bitshuffle) #:use-module (crates-io))

(define-public crate-hdf5-bitshuffle-0.1.0 (c (n "hdf5-bitshuffle") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "git2") (r "^0.13") (d #t) (k 1)) (d (n "hdf5-src") (r "^0.7") (f (quote ("deprecated" "threadsafe"))) (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.7") (f (quote ("static"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17v3lgzg5zqxizk6ia8hyyg02zafwrnnbyzzcbnpwchv6mf7x505") (l "bitshuffle")))

(define-public crate-hdf5-bitshuffle-0.1.1 (c (n "hdf5-bitshuffle") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "git2") (r "^0.13") (d #t) (k 1)) (d (n "hdf5-src") (r "^0.7") (f (quote ("deprecated" "threadsafe"))) (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.7") (f (quote ("static"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vm8vafr4xgl7zaqhm4aipgpzj79inndsdv7d5qxs4kggw95flmp") (l "bitshuffle")))

(define-public crate-hdf5-bitshuffle-0.2.0 (c (n "hdf5-bitshuffle") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "hdf5-src") (r "^0.7") (f (quote ("deprecated" "threadsafe"))) (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.7") (f (quote ("static"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qgpfb14cffn11d1mxnhjfcqiwii273cbvzlcwpzcy4fv7qgidds") (l "bitshuffle")))

(define-public crate-hdf5-bitshuffle-0.8.0 (c (n "hdf5-bitshuffle") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "hdf5-src") (r "^0.8") (f (quote ("deprecated" "threadsafe"))) (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.8") (f (quote ("static" "deprecated" "threadsafe"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0k76dqpmvfwcji0l6yh92alln83k8yp9hizb5f905lsnzz2qxgq0") (l "bitshuffle")))

(define-public crate-hdf5-bitshuffle-0.9.0 (c (n "hdf5-bitshuffle") (v "0.9.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "hdf5-src") (r "^0.8") (f (quote ("deprecated" "threadsafe"))) (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.8") (f (quote ("static" "deprecated" "threadsafe"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qnphynr11731rx86jlh4vln3lfmbcpp8bpzg3c62bbjxipxbfsv") (l "bitshuffle")))

