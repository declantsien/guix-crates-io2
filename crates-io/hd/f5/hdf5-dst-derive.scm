(define-module (crates-io hd f5 hdf5-dst-derive) #:use-module (crates-io))

(define-public crate-hdf5-dst-derive-0.1.0 (c (n "hdf5-dst-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1agzi6ixlk1avw4nxs4dc62lkqk0npa1jqqrfgracnqxiggav2ca")))

