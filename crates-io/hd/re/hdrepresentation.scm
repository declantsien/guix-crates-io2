(define-module (crates-io hd re hdrepresentation) #:use-module (crates-io))

(define-public crate-hdrepresentation-0.1.0 (c (n "hdrepresentation") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1rghdks7l4a6mqhlgqnqp67d99jakr73arng1nkhcw26xinghby8")))

(define-public crate-hdrepresentation-0.1.1 (c (n "hdrepresentation") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "04mi4sfsaf5mpx9q9pq5v6sjyn8dj445nmajwlvwby58glbjl6zl")))

(define-public crate-hdrepresentation-0.1.2 (c (n "hdrepresentation") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "06dsl5w1j6f65kz7vw90nxvlvs6x1smfk0q3si9qyz381ffrq8zx")))

(define-public crate-hdrepresentation-0.1.3 (c (n "hdrepresentation") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1jdw2f3wik2kz2iii726qn7s9l5d5my9qn4ry6r32szwj0j4s8f0")))

(define-public crate-hdrepresentation-0.1.4 (c (n "hdrepresentation") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1aip3f25q4a4wg6jd0jsl7l7fvimwlw36jmgrxzk6csjddha75yh")))

(define-public crate-hdrepresentation-0.1.5 (c (n "hdrepresentation") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0dx52xilmbjrsn5v2vzb6pazsckv45z8fp8vzgsc1gq0fdkdnq6g")))

(define-public crate-hdrepresentation-0.1.6 (c (n "hdrepresentation") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "16z6zbhsiwdbj2wrw19v6y6hgd6l2m6hchabd6gbgbcmafqkpbnf")))

(define-public crate-hdrepresentation-0.1.7 (c (n "hdrepresentation") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "000b1hs5mijhzb8qx4jcm49khnkqxx03p1bmbggrmg75404kvh7g")))

(define-public crate-hdrepresentation-0.1.8 (c (n "hdrepresentation") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_with") (r "^1.5.1") (d #t) (k 0)))) (h "07zxwbi2fxckf0d72ad3vaikrbn7bacy2mg4d8xlysmc9fxmx5ww")))

(define-public crate-hdrepresentation-0.1.81 (c (n "hdrepresentation") (v "0.1.81") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_with") (r "^1.5.1") (d #t) (k 0)))) (h "0m6zi7qn9xmybxi1bz4qh0y5953y27416xq8xm6vsvgahw689vb7")))

(define-public crate-hdrepresentation-0.1.82 (c (n "hdrepresentation") (v "0.1.82") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-tuple-vec-map") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_with") (r "^1.5.1") (d #t) (k 0)))) (h "111cwyx5hlqqhdqdzd1cj7vdy7fq9z1wvv7zsr3dwj090lfz1b80")))

(define-public crate-hdrepresentation-0.1.83 (c (n "hdrepresentation") (v "0.1.83") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0l0jd6wfbxhc3500x6a71z3b9ml7xdycb2nihkmy4cr8690d1d9c")))

(define-public crate-hdrepresentation-0.1.84 (c (n "hdrepresentation") (v "0.1.84") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "0ag8775sdjzmahs75z1y0rqlidk5wlyqy1271qm768dcqcj5vq8c")))

(define-public crate-hdrepresentation-0.1.85 (c (n "hdrepresentation") (v "0.1.85") (d (list (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "1bcpg67j9577jlnc1jjrpmw6ab82bjnpcir1j9x379rgh6w6xj9p")))

(define-public crate-hdrepresentation-0.1.851 (c (n "hdrepresentation") (v "0.1.851") (d (list (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "13dqy0nb1cm77w4j2cf3hfp967p4n8dahlnq0hxj6y31g1jph0fh")))

(define-public crate-hdrepresentation-0.1.852 (c (n "hdrepresentation") (v "0.1.852") (d (list (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "0x55il7knvkyq06h2i0bb49fh3hgvmzg9qiwp576baj36hp2cqxx")))

