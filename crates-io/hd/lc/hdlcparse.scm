(define-module (crates-io hd lc hdlcparse) #:use-module (crates-io))

(define-public crate-hdlcparse-1.0.0 (c (n "hdlcparse") (v "1.0.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "0vwph4aljj6xky5w90bq7lnk0qv944ys1p11spzx9lclshl0ckh6")))

(define-public crate-hdlcparse-2.0.0 (c (n "hdlcparse") (v "2.0.0") (d (list (d (n "nom") (r "^7.1") (k 0)))) (h "0rl10gwpql0scy9hzrhs04vwcvmvj602prq31zfmf740cvi39rwk") (f (quote (("std" "nom/std") ("default" "std"))))))

