(define-module (crates-io hd lc hdlc) #:use-module (crates-io))

(define-public crate-hdlc-0.1.0 (c (n "hdlc") (v "0.1.0") (h "13g18y9sr6l8m61vzvzjmznnd8djj724mslkp7ifbkdp2xsf3548")))

(define-public crate-hdlc-0.1.1 (c (n "hdlc") (v "0.1.1") (h "003wjx0krdlnm50qj571aw9ghbf9qhrkm65k74jaijkdpgzlgn4z")))

(define-public crate-hdlc-0.1.2 (c (n "hdlc") (v "0.1.2") (h "0r91nznxsz17z8f19vj0ahlyv4jlpcfq83qyj16k65ah1lvlf18w")))

(define-public crate-hdlc-0.1.3 (c (n "hdlc") (v "0.1.3") (h "02d5x1s0zs7rl1mqnyajw1a3vxg7wl5n51czdc83f6qf69qal54s")))

(define-public crate-hdlc-0.2.0 (c (n "hdlc") (v "0.2.0") (h "12p41akwd644dsimmks5ynm06d1cpw841m3jp6j0i6fga919kvx6")))

(define-public crate-hdlc-0.2.1 (c (n "hdlc") (v "0.2.1") (h "1zhklczbhz11cx074h7fyscr240yrh0qmkkny2wkrfwhn57azx13")))

(define-public crate-hdlc-0.2.2 (c (n "hdlc") (v "0.2.2") (h "1mz0kiwpzfpxm04sdyjbaj96fghzrd3ar5sizsxy7kfdqwg8f58m")))

(define-public crate-hdlc-0.2.3 (c (n "hdlc") (v "0.2.3") (h "0ys2wqsd1wv9m99na3x82yjybwk29lhh3wd9rc82yyd1fj51qsq2")))

(define-public crate-hdlc-0.2.4 (c (n "hdlc") (v "0.2.4") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "18qx38wpv863q2kr320f09fi7wymf5wyiwy49s8b2ixamcsp23kg")))

(define-public crate-hdlc-0.2.5 (c (n "hdlc") (v "0.2.5") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1d791yzmd4micrnfxmj6vvvz804dvlyk5p48vgsb9j555abqagxg")))

