(define-module (crates-io hd if hdiff) #:use-module (crates-io))

(define-public crate-hdiff-0.1.0 (c (n "hdiff") (v "0.1.0") (h "1fq3p0xj14zwr9yxqxw54wvxngrbpclnpghkmbwgz47rd4xqqzn8")))

(define-public crate-hdiff-0.1.1 (c (n "hdiff") (v "0.1.1") (h "14vzqdl01pbk53id0cwnclibd07ii60r37b0937n11zjwwds3pxn")))

