(define-module (crates-io hd #{44}# hd44780-hal) #:use-module (crates-io))

(define-public crate-hd44780-hal-0.1.0 (c (n "hd44780-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "18dcnab7v3bkvb3aw0007csy23nqqfxjfyaczkwinwhklqpcx0zs")))

(define-public crate-hd44780-hal-0.1.1 (c (n "hd44780-hal") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0gznsisiaq7hlbxzrwmbrci32h1ydmid5f5hygikpinhx3xmizvd")))

(define-public crate-hd44780-hal-0.1.2 (c (n "hd44780-hal") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1q7jk4sm3xca1gw75yrv644yf5fr0653nwbf112dyrdawh92729j")))

(define-public crate-hd44780-hal-0.1.3 (c (n "hd44780-hal") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1527m2qq326y912zqspwbp99bc5wp94zj5slys506r88bx7q9bh1")))

(define-public crate-hd44780-hal-0.1.4 (c (n "hd44780-hal") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0a7mci4hkxygm8wmxgfd3mbg65qcdb7hpp7f62qim3rzqx5y4d42")))

(define-public crate-hd44780-hal-0.2.0 (c (n "hd44780-hal") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0g9z98aikvr1133wcx7463brj0iwza311f51xqv4akf7p88s8k3g")))

(define-public crate-hd44780-hal-0.2.1 (c (n "hd44780-hal") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0s400yii6xi304z4yc5wajcq1h67b62wrbyh0dcw8yfjfs7lncj0")))

