(define-module (crates-io hd #{44}# hd44780_menu) #:use-module (crates-io))

(define-public crate-hd44780_menu-0.1.0 (c (n "hd44780_menu") (v "0.1.0") (h "094cynyj8ff9yihy803la92j5p5gjsjxpn18wm8nvnjymkr5sn3v")))

(define-public crate-hd44780_menu-0.1.1 (c (n "hd44780_menu") (v "0.1.1") (h "0c9jnjba9iwgnzpysvg636yb1y0gnnfyxzwyadr325j4glqfwn2m")))

