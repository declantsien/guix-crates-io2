(define-module (crates-io hd #{44}# hd44780-ntb) #:use-module (crates-io))

(define-public crate-hd44780-ntb-0.0.4 (c (n "hd44780-ntb") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "sysfs_gpio") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "01ll58s6jgf61ws4p2ksrka95z8wg336vyca2v146p7bhzp9mzbw")))

(define-public crate-hd44780-ntb-0.0.5 (c (n "hd44780-ntb") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "sysfs_gpio") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "03ryf73r6457803f6fkmq71hbg48mm0wh8lscyf4m1nckq922mkn")))

(define-public crate-hd44780-ntb-0.0.6 (c (n "hd44780-ntb") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "sysfs_gpio") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1zraja1489pnrmdd115ynry568limrmk2c47gvwbz1mib75lnan3")))

