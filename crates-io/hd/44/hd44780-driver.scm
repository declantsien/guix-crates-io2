(define-module (crates-io hd #{44}# hd44780-driver) #:use-module (crates-io))

(define-public crate-hd44780-driver-0.2.0 (c (n "hd44780-driver") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "11qn1dwbwm6nnqkgjv5q4wf6r0m7drvs0qwcdv7xw8rlkycyzc4m")))

(define-public crate-hd44780-driver-0.2.1 (c (n "hd44780-driver") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "11q2d0gz527f48l3akdwfpqrcb87bfjsiwgayympvhx7hg5rznx6")))

(define-public crate-hd44780-driver-0.3.0 (c (n "hd44780-driver") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0sf8k81zavrrf5c5p191y6mfhgcika89w1x50pgw2qrgs7d2m5s9")))

(define-public crate-hd44780-driver-0.4.0 (c (n "hd44780-driver") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0fkxhlad3ai82iivb5iiikyair5x5j5w4mrs2glxvmxfvqzv3cma")))

