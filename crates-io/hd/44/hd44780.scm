(define-module (crates-io hd #{44}# hd44780) #:use-module (crates-io))

(define-public crate-hd44780-0.1.0 (c (n "hd44780") (v "0.1.0") (h "19xkfx5xvimg1896ay2bkvb5pr07y7xrbg0zng137hiya8mmack6")))

(define-public crate-hd44780-0.2.0 (c (n "hd44780") (v "0.2.0") (h "1jsbq7bavyfh2m5xz97rp1g15a8b99nxf2a004nlnzvykpgw9xsb")))

(define-public crate-hd44780-0.2.1 (c (n "hd44780") (v "0.2.1") (h "1q7489q74wj7nlmcp63nh59fil13vyz3m1k24qrlq7m4xlvhdan0")))

(define-public crate-hd44780-0.2.2 (c (n "hd44780") (v "0.2.2") (h "16ffh15fwcph2xphrag4pn37v0sv88irwcryjcgwriqzxacfj0ny")))

