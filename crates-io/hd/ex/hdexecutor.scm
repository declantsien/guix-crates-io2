(define-module (crates-io hd ex hdexecutor) #:use-module (crates-io))

(define-public crate-hdexecutor-0.1.1 (c (n "hdexecutor") (v "0.1.1") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "penguincrab") (r "^0.1.62") (d #t) (k 0)))) (h "1zw4gbi0l79gsj7ibak30gzg1halwj63k92syhfbxavvrg3fvaf8")))

(define-public crate-hdexecutor-0.1.10 (c (n "hdexecutor") (v "0.1.10") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "penguincrab") (r "^0.1.62") (d #t) (k 0)))) (h "02hmd8jcm0q3mccwrzi2914j71n5nv3z30mnxgdjacidl22vnb4r")))

(define-public crate-hdexecutor-0.1.11 (c (n "hdexecutor") (v "0.1.11") (d (list (d (n "hdrepresentation") (r "^0.1.852") (d #t) (k 0)) (d (n "penguincrab") (r "^0.1.62") (d #t) (k 0)))) (h "0y42d6zhgnpa7k026ff7kfl1562qyhfa8vl9lvsa6435111n7313")))

