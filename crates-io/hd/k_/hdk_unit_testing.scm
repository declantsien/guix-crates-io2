(define-module (crates-io hd k_ hdk_unit_testing) #:use-module (crates-io))

(define-public crate-hdk_unit_testing-0.1.1 (c (n "hdk_unit_testing") (v "0.1.1") (d (list (d (n "hdk") (r "^0.0.115") (d #t) (k 0)))) (h "01plsv7k6acc86lzmyh7s9di1nm4g74k6k7ianpah44v2jr0q107") (f (quote (("mock" "hdk/mock" "hdk/test_utils") ("default" "mock"))))))

(define-public crate-hdk_unit_testing-0.1.2 (c (n "hdk_unit_testing") (v "0.1.2") (d (list (d (n "hdk") (r "^0.0.122") (d #t) (k 0)))) (h "1ladxbzglsi92w4rdag33fvmyqmn3nqplngbdj3vv99gfi6l45rl") (f (quote (("mock" "hdk/mock" "hdk/test_utils") ("default" "mock"))))))

(define-public crate-hdk_unit_testing-0.1.3 (c (n "hdk_unit_testing") (v "0.1.3") (d (list (d (n "hdk") (r "^0.0.122") (d #t) (k 0)))) (h "1206mgg4jbw0cx3gis2dlpn6r4bl5h9bvcx70jqvzq2n39zd8phv") (f (quote (("mock" "hdk/mock" "hdk/test_utils") ("default" "mock"))))))

(define-public crate-hdk_unit_testing-0.1.4 (c (n "hdk_unit_testing") (v "0.1.4") (d (list (d (n "hdk") (r "^0.0.134") (d #t) (k 0)))) (h "09wml7br7jvpc9b6cqrn2facvcj3lw0cp7zri5h9gnmj2sdk7cnd") (f (quote (("mock" "hdk/mock" "hdk/test_utils") ("default" "mock"))))))

