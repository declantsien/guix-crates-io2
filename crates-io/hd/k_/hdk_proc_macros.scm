(define-module (crates-io hd k_ hdk_proc_macros) #:use-module (crates-io))

(define-public crate-hdk_proc_macros-0.0.33-alpha5 (c (n "hdk_proc_macros") (v "0.0.33-alpha5") (d (list (d (n "hdk") (r "= 0.0.33-alpha5") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x29af4b5f7qjanw8may4665yis7pnhjnfc93b1mscgyk09w8n95")))

(define-public crate-hdk_proc_macros-0.0.33-alpha6 (c (n "hdk_proc_macros") (v "0.0.33-alpha6") (d (list (d (n "hdk") (r "= 0.0.33-alpha6") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rvyzs815mmwrikq19h48p2mfvawa38n8xl35v3p23wwvwssw9kz")))

(define-public crate-hdk_proc_macros-0.0.34-alpha1 (c (n "hdk_proc_macros") (v "0.0.34-alpha1") (d (list (d (n "hdk") (r "= 0.0.34-alpha1") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hqhy5ffb79yb941nwlsrk7wdqpvzv109kgskz3wqx6bacmarf6h")))

(define-public crate-hdk_proc_macros-0.0.35-alpha7 (c (n "hdk_proc_macros") (v "0.0.35-alpha7") (d (list (d (n "hdk") (r "= 0.0.35-alpha7") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yk6qirwrqxkhvdk6zjvd5aw60arnj1vcbfsc9i6jd9298da8vzj")))

(define-public crate-hdk_proc_macros-0.0.37-alpha6 (c (n "hdk_proc_macros") (v "0.0.37-alpha6") (d (list (d (n "hdk") (r "= 0.0.37-alpha6") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "197ix2b1wmjxc1f8h41ay2ayh0gz2sfyw6f7p43g3kyh722wnd6z")))

(define-public crate-hdk_proc_macros-0.0.38-alpha2 (c (n "hdk_proc_macros") (v "0.0.38-alpha2") (d (list (d (n "hdk") (r "= 0.0.38-alpha2") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11fsaj9wfkpx5bdh2yyaf8l91z67lk8yh9kb21xpgqd00kwysgj6")))

(define-public crate-hdk_proc_macros-0.0.38-alpha9 (c (n "hdk_proc_macros") (v "0.0.38-alpha9") (d (list (d (n "hdk") (r "= 0.0.38-alpha9") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "091wn1inx4yim21f0idcimanj2hglki1kbqq8w17lvv8scb69gli")))

(define-public crate-hdk_proc_macros-0.0.38-alpha12 (c (n "hdk_proc_macros") (v "0.0.38-alpha12") (d (list (d (n "hdk") (r "= 0.0.38-alpha12") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vy3zbjjns9gxvg6fvz7xqm475y45jj964744p9mkjkxlhnxd7zw")))

(define-public crate-hdk_proc_macros-0.0.38-alpha13 (c (n "hdk_proc_macros") (v "0.0.38-alpha13") (d (list (d (n "hdk") (r "= 0.0.38-alpha13") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xf23flxvzvdmr65mdjnwp34qrv047gcl1rpdmkvc7bwlw1s33lm")))

(define-public crate-hdk_proc_macros-0.0.38-alpha14 (c (n "hdk_proc_macros") (v "0.0.38-alpha14") (d (list (d (n "hdk") (r "= 0.0.38-alpha14") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08p9kdry5jjp1rvaqrfs3ck0r2g6r3852nipx0g7v2735a61zdmk")))

(define-public crate-hdk_proc_macros-0.0.39-alpha2 (c (n "hdk_proc_macros") (v "0.0.39-alpha2") (d (list (d (n "hdk") (r "= 0.0.39-alpha2") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jcikbzs92k6k8wpbkn990azpdzgc0qig5xfrsq68q510k13ii3h")))

(define-public crate-hdk_proc_macros-0.0.39-alpha4 (c (n "hdk_proc_macros") (v "0.0.39-alpha4") (d (list (d (n "hdk") (r "= 0.0.39-alpha4") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fhm4j27whyssf2asrq24nwxlss2ycwldr4gz3sn1060i2jkwy12")))

(define-public crate-hdk_proc_macros-0.0.40-alpha1 (c (n "hdk_proc_macros") (v "0.0.40-alpha1") (d (list (d (n "hdk") (r "= 0.0.40-alpha1") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dmp7g2bml7qpya3vch38rxy2rs7z8xn08j0wmss84qq592g8492")))

(define-public crate-hdk_proc_macros-0.0.41-alpha4 (c (n "hdk_proc_macros") (v "0.0.41-alpha4") (d (list (d (n "hdk") (r "= 0.0.41-alpha4") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "100vmf9f82a5nm71gcwlza0kgmics0c0njrvvhsf5df1w3i2gncq")))

(define-public crate-hdk_proc_macros-0.0.42-alpha1 (c (n "hdk_proc_macros") (v "0.0.42-alpha1") (d (list (d (n "hdk") (r "= 0.0.42-alpha1") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hxvn2rf0rvl1kqzh5fqq2wwrz706gsqwn8kgvbd7vxwy132dhx0")))

(define-public crate-hdk_proc_macros-0.0.42-alpha2 (c (n "hdk_proc_macros") (v "0.0.42-alpha2") (d (list (d (n "hdk") (r "= 0.0.42-alpha2") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ckp7759rb718cs5f8f0m07wyjdiwjvlcklnnsxa2q71mxcnszwp")))

(define-public crate-hdk_proc_macros-0.0.42-alpha3 (c (n "hdk_proc_macros") (v "0.0.42-alpha3") (d (list (d (n "hdk") (r "= 0.0.42-alpha3") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ipwimwkfyd1cngg1xfp26qcjz7l9wamcfklgvi396wxxs07cl8i")))

(define-public crate-hdk_proc_macros-0.0.42-alpha4 (c (n "hdk_proc_macros") (v "0.0.42-alpha4") (d (list (d (n "hdk") (r "= 0.0.42-alpha4") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0myn6pc5cslk16fg506kagfal864308yp11p9xwhpasqv99nizi8")))

(define-public crate-hdk_proc_macros-0.0.42-alpha5 (c (n "hdk_proc_macros") (v "0.0.42-alpha5") (d (list (d (n "hdk") (r "= 0.0.42-alpha5") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "009xqm0wad3pb04iagd0chv5kw5qwd4lcwfsvv5i5b3bqmp3yvj9")))

(define-public crate-hdk_proc_macros-0.0.43-alpha3 (c (n "hdk_proc_macros") (v "0.0.43-alpha3") (d (list (d (n "hdk") (r "= 0.0.43-alpha3") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pxyi0p0y9njd3rns4f5cqib5x4hikw2gjr838drxg4b2l01lnv1")))

(define-public crate-hdk_proc_macros-0.0.44-alpha1 (c (n "hdk_proc_macros") (v "0.0.44-alpha1") (d (list (d (n "hdk") (r "= 0.0.44-alpha1") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mani6yib0jpwyjap34nxvhf1nimhkdinwqwn6hjf7rvhaqm0kjw")))

(define-public crate-hdk_proc_macros-0.0.44-alpha2 (c (n "hdk_proc_macros") (v "0.0.44-alpha2") (d (list (d (n "hdk") (r "= 0.0.44-alpha2") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x02b4lp20p52hc2si4sxjm9a58arsch7grb6sgzklv7ay6lpi5i")))

(define-public crate-hdk_proc_macros-0.0.44-alpha3 (c (n "hdk_proc_macros") (v "0.0.44-alpha3") (d (list (d (n "hdk") (r "= 0.0.44-alpha3") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0khf4ilar86v1g7j3zvwlii6g21fx4q91nrkqngd20d0l0akrwdx")))

(define-public crate-hdk_proc_macros-0.0.45-alpha1 (c (n "hdk_proc_macros") (v "0.0.45-alpha1") (d (list (d (n "hdk") (r "= 0.0.45-alpha1") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19sq19rf0was6bk1388vqn4bh5rdn5f2c6iyd7hyljr6rsimb4dj")))

(define-public crate-hdk_proc_macros-0.0.46-alpha1 (c (n "hdk_proc_macros") (v "0.0.46-alpha1") (d (list (d (n "hdk") (r "= 0.0.46-alpha1") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01hnx866ppq9qpbsx1849ixda40rrbjrvcmbz3ysnvn2p4aywmy6")))

(define-public crate-hdk_proc_macros-0.0.47-alpha1 (c (n "hdk_proc_macros") (v "0.0.47-alpha1") (d (list (d (n "hdk") (r "= 0.0.47-alpha1") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xrml5s8hq1gyccwq5slvn9w0nwmmnh3r67f69g2kx5iak28m7py")))

(define-public crate-hdk_proc_macros-0.0.48-alpha1 (c (n "hdk_proc_macros") (v "0.0.48-alpha1") (d (list (d (n "hdk") (r "= 0.0.48-alpha1") (d #t) (k 0)) (d (n "proc-macro2") (r "= 0.4.27") (d #t) (k 0)) (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dhhn2cqq8lfcrn51z3cq8bwdp72xcpv3ywkgmpl5hdr5kg1lxqy")))

(define-public crate-hdk_proc_macros-0.0.49-alpha1 (c (n "hdk_proc_macros") (v "0.0.49-alpha1") (d (list (d (n "hdk") (r "=0.0.49-alpha1") (d #t) (k 0)) (d (n "proc-macro2") (r "=0.4.27") (d #t) (k 0)) (d (n "quote") (r "=0.6.11") (d #t) (k 0)) (d (n "syn") (r "=0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s2vp5kf3kykx1r6r8xpkdqchhbzj7c6gic0b4dk71q0ycpil4q6")))

(define-public crate-hdk_proc_macros-0.0.50-alpha4 (c (n "hdk_proc_macros") (v "0.0.50-alpha4") (d (list (d (n "hdk") (r "=0.0.50-alpha4") (d #t) (k 0)) (d (n "proc-macro2") (r "=0.4.27") (d #t) (k 0)) (d (n "quote") (r "=0.6.11") (d #t) (k 0)) (d (n "syn") (r "=0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cc6fpa2gllid6v13cv1aqq3mvmli061w4zi3m7ax4kjs199i26w")))

(define-public crate-hdk_proc_macros-0.0.51-alpha1 (c (n "hdk_proc_macros") (v "0.0.51-alpha1") (d (list (d (n "hdk") (r "=0.0.51-alpha1") (d #t) (k 0)) (d (n "proc-macro2") (r "=0.4.27") (d #t) (k 0)) (d (n "quote") (r "=0.6.11") (d #t) (k 0)) (d (n "syn") (r "=0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18hf9anxyyhq93kwyrn4i066b0x79bk30h8r9nkkk5857nphhg8a")))

(define-public crate-hdk_proc_macros-0.0.52-alpha1 (c (n "hdk_proc_macros") (v "0.0.52-alpha1") (d (list (d (n "hdk") (r "=0.0.52-alpha1") (d #t) (k 0)) (d (n "proc-macro2") (r "=0.4.27") (d #t) (k 0)) (d (n "quote") (r "=0.6.11") (d #t) (k 0)) (d (n "syn") (r "=0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07g07rkvjdky20glnnfaq2s9yvk512ndrglndmx1hxfv0nyj4xpg")))

(define-public crate-hdk_proc_macros-0.0.52-alpha2 (c (n "hdk_proc_macros") (v "0.0.52-alpha2") (d (list (d (n "hdk") (r "=0.0.52-alpha2") (d #t) (k 0)) (d (n "proc-macro2") (r "=0.4.27") (d #t) (k 0)) (d (n "quote") (r "=0.6.11") (d #t) (k 0)) (d (n "syn") (r "=0.15.31") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12r4hzq4101lhb9bssnd4m0j9rc6369hwpxnmyvsx7j7jjn549vm")))

