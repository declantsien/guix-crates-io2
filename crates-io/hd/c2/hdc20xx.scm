(define-module (crates-io hd c2 hdc20xx) #:use-module (crates-io))

(define-public crate-hdc20xx-0.1.0 (c (n "hdc20xx") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "1w7gjn8ficcjb1aywf2ijrx68wbnvcf68ijbj6qr7wyp8c55xcqk")))

