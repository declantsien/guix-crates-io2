(define-module (crates-io hd li hdlibaflexecutor) #:use-module (crates-io))

(define-public crate-hdlibaflexecutor-0.0.1 (c (n "hdlibaflexecutor") (v "0.0.1") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "more-asserts") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "1xjbi05d3cs9fphgzn6vg7qhskw7vy89cjnjjlfq3ffx9npxpx3m") (l "shared")))

(define-public crate-hdlibaflexecutor-0.0.11 (c (n "hdlibaflexecutor") (v "0.0.11") (d (list (d (n "hdrepresentation") (r "^0.1.82") (d #t) (k 0)) (d (n "more-asserts") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "1d2lsp534g5rx6n7xsn0ryb8lc2i16s9i49sryk10vg33np1c5ac") (l "shared")))

