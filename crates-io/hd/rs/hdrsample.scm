(define-module (crates-io hd rs hdrsample) #:use-module (crates-io))

(define-public crate-hdrsample-0.1.0 (c (n "hdrsample") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0laqf70gsnp0zis4wsazs38h4m3b3lg86x1fbrgf0gyw0vb1lgm9")))

(define-public crate-hdrsample-1.0.0 (c (n "hdrsample") (v "1.0.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1xfwyal0gg5lp4jihpw3jv6bvx811dfmaz434pc4qinqz03nw5k8")))

(define-public crate-hdrsample-1.1.0 (c (n "hdrsample") (v "1.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0s4a1yj9xiq107b4a1rrsni82gbm50k0vyn8m5wr5nf65as9f6rj")))

(define-public crate-hdrsample-1.1.1 (c (n "hdrsample") (v "1.1.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1pvzfvvilqgjgji4ccprdw77fkf69nwk21jp2nw9wnimff41sack") (f (quote (("benchmark"))))))

(define-public crate-hdrsample-2.0.0 (c (n "hdrsample") (v "2.0.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "19cafl9vdxm001fb74d4vrcv7cgdb214p6b9inc7x7nqa3jgfnwc") (f (quote (("benchmark"))))))

(define-public crate-hdrsample-2.1.0 (c (n "hdrsample") (v "2.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1fkmklx3z1fjrp801gfs7624vsffycrkzb6zsr99gs9zigf4sia4") (f (quote (("benchmark"))))))

(define-public crate-hdrsample-2.1.1 (c (n "hdrsample") (v "2.1.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0w41hlnx8xgm80ygfg50r59hyya48rpn25mc3504cb2vcwgihzch") (f (quote (("benchmark"))))))

(define-public crate-hdrsample-3.0.0 (c (n "hdrsample") (v "3.0.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "06gpkppf7ffrk2r4rk5mnzgjzag8svpliwra8r637f4wljwccy2g") (f (quote (("benchmark"))))))

(define-public crate-hdrsample-4.0.0 (c (n "hdrsample") (v "4.0.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.17") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1rnlzs7ynh59860f1fkvx9q8m6gmclhpfjjrdxlbr5izlg5iczi8") (f (quote (("bench_private"))))))

(define-public crate-hdrsample-5.0.0 (c (n "hdrsample") (v "5.0.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.17") (o #t) (d #t) (k 0)) (d (n "ieee754") (r "^0.2.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "rug") (r "^0.6.0") (d #t) (k 2)))) (h "17x61sbahijs0j9fy8b2mz960l2c4lxfbcgwmq0549gvr4nh3axm") (f (quote (("serialization" "flate2") ("default" "serialization") ("bench_private"))))))

(define-public crate-hdrsample-5.1.0 (c (n "hdrsample") (v "5.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.17") (o #t) (d #t) (k 0)) (d (n "ieee754") (r "^0.2.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "rug") (r "^0.6.0") (d #t) (k 2)))) (h "0ix998x35r8akl0inpxhzfz224djks488cf281sv9c8j2hn0993m") (f (quote (("serialization" "flate2") ("default" "serialization") ("bench_private"))))))

(define-public crate-hdrsample-6.0.0 (c (n "hdrsample") (v "6.0.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.17") (o #t) (d #t) (k 0)) (d (n "ieee754") (r "^0.2.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "rug") (r "^0.6.0") (d #t) (k 2)))) (h "0bxpg6frfi79yjf6jl1sh4wj8vr1mw66skvnx1wpcqbf62ijaz8n") (f (quote (("serialization" "flate2") ("default" "serialization") ("bench_private"))))))

(define-public crate-hdrsample-6.0.1 (c (n "hdrsample") (v "6.0.1") (d (list (d (n "base64") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.17") (o #t) (d #t) (k 0)) (d (n "ieee754") (r "^0.2.2") (d #t) (k 2)) (d (n "nom") (r "^3.2.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "rug") (r "^0.6.0") (d #t) (k 2)))) (h "07986ndqmmh7vi8lyymwc1rz0ivrbmzyv2ig3r8mfcqh6420fh8v") (f (quote (("serialization" "flate2" "nom" "base64") ("default" "serialization") ("bench_private"))))))

(define-public crate-hdrsample-6.0.2 (c (n "hdrsample") (v "6.0.2") (d (list (d (n "base64") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.17") (o #t) (d #t) (k 0)) (d (n "ieee754") (r "^0.2.2") (d #t) (k 2)) (d (n "nom") (r "^3.2.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "rug") (r "^0.9.1") (d #t) (k 2)))) (h "0ib23dsfcd25k23dhzl4swz7vyk43fz7bcw701vihjml22d4hbvj") (f (quote (("serialization" "flate2" "nom" "base64") ("default" "serialization") ("bench_private"))))))

(define-public crate-hdrsample-6.0.3 (c (n "hdrsample") (v "6.0.3") (d (list (d (n "base64") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.17") (o #t) (d #t) (k 0)) (d (n "ieee754") (r "^0.2.2") (d #t) (k 2)) (d (n "nom") (r "^3.2.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "rug") (r "^0.9.1") (d #t) (k 2)))) (h "1qm21iwl6dwssjcpxymcz5wfk8fbib065700ljmskpipaqmmcp2g") (f (quote (("serialization" "flate2" "nom" "base64") ("default" "serialization") ("bench_private"))))))

(define-public crate-hdrsample-6.0.4 (c (n "hdrsample") (v "6.0.4") (d (list (d (n "base64") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.17") (o #t) (d #t) (k 0)) (d (n "ieee754") (r "^0.2.2") (d #t) (k 2)) (d (n "nom") (r "^3.2.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "rug") (r "^0.9.1") (d #t) (k 2)))) (h "0fnj0xaayy254kczd5n7ix9bgaddl5k48qx9qj93discfv8hvvgw") (f (quote (("serialization" "flate2" "nom" "base64") ("default" "serialization") ("bench_private"))))))

