(define-module (crates-io em ai emailx) #:use-module (crates-io))

(define-public crate-emailx-0.1.0 (c (n "emailx") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "trust-dns") (r "^0.15.0-alpha.1") (k 0)))) (h "0m34gk1gwz1i51hys044k96b4782p6rchz3rnq7adhdpm7a4dyss")))

