(define-module (crates-io em ai email-rs) #:use-module (crates-io))

(define-public crate-email-rs-0.1.0 (c (n "email-rs") (v "0.1.0") (h "0kpkqvp9mzncyrqxl3d0xm6ah1h8jph55ivsv03j9mj1a857nbq1")))

(define-public crate-email-rs-0.1.1 (c (n "email-rs") (v "0.1.1") (h "1gyvrv2njcbqvx0mdijnfr6hgwwqjc8wjrwlhkssldmkjzvjjj5q")))

