(define-module (crates-io em ai email_address) #:use-module (crates-io))

(define-public crate-email_address-0.1.0 (c (n "email_address") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l8hgj0xmb18yc4bnnk0dnqcr0clj5pnwwrbyppb0n5k9ivb0vxw") (f (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-email_address-0.2.0 (c (n "email_address") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zdcqn3sh5k5d2q4hicwp43pjn6jazc3scrfihc5vswym2rypxiz") (f (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-email_address-0.2.1 (c (n "email_address") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13j6s1qz5x13rdc0zrmsjfdqqqdshksrwqmrwnhxyms8rg4vg146") (f (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-email_address-0.2.2 (c (n "email_address") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13wgdmm4dc1cppsgb4vg2af1vl9qa7zac1rraz88wbhp2jxrfqb6") (f (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-email_address-0.2.3 (c (n "email_address") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02a8zrddparkm1y24wrlbqyd7mxx6hn52szn20zlgi404mx2mcxi") (f (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-email_address-0.2.4 (c (n "email_address") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04ni69f0qfydw5knkj9x5sah864h9lcy5hxxphaxn2dw7vc3n5g2") (f (quote (("serde_support" "serde") ("default" "serde_support"))))))

