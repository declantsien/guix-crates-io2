(define-module (crates-io em ai email-encoding) #:use-module (crates-io))

(define-public crate-email-encoding-0.1.0 (c (n "email-encoding") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "164zca4pis56ql0byyrkx1kiaya1y91bl25cj1vlckl2cq8jk436") (r "1.56")))

(define-public crate-email-encoding-0.1.1 (c (n "email-encoding") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (k 0)) (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0hc4ka15w17m8i9dgz8pf2ahsdspzszyar4pgyifmrs3qgfivfbm") (r "1.56")))

(define-public crate-email-encoding-0.1.2 (c (n "email-encoding") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (k 0)) (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "06xljszqdicpys16jghyvr5199pqhjh3zjjl0jmmim94dnw1yzl2") (r "1.56")))

(define-public crate-email-encoding-0.1.3 (c (n "email-encoding") (v "0.1.3") (d (list (d (n "base64") (r "^0.13") (k 0)) (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "165z5fx8ws0d0pb6if0rklmsns02mphxav3rk5jj1q326v319p9l") (r "1.56")))

(define-public crate-email-encoding-0.2.0 (c (n "email-encoding") (v "0.2.0") (d (list (d (n "base64") (r "^0.21") (k 0)) (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0xgg9xn88jaiz83dfjwjd46w83mwm84r2mdqvi4a7xwchywj3yyv") (r "1.57")))

(define-public crate-email-encoding-0.2.1 (c (n "email-encoding") (v "0.2.1") (d (list (d (n "base64") (r "^0.22") (k 0)) (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "12kwk433rsqs78yzd3d7cjpy7wqg5a6wd0b2ipkrwwq6kd260wm8") (r "1.57")))

(define-public crate-email-encoding-0.3.0 (c (n "email-encoding") (v "0.3.0") (d (list (d (n "base64") (r "^0.22") (k 0)) (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0zriz9qgqd0s522sjrmhwngldiyknlp66fghj50f5rzdv8yd7lb0") (r "1.57")))

