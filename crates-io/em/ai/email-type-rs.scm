(define-module (crates-io em ai email-type-rs) #:use-module (crates-io))

(define-public crate-email-type-rs-1.0.0 (c (n "email-type-rs") (v "1.0.0") (d (list (d (n "fake") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "validate") (r "^0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "0ybxh9pwmsx0815grrv598gfsai5r4k4f1c7xkg69kqqb04rpsrx") (f (quote (("utils") ("default"))))))

(define-public crate-email-type-rs-1.0.1 (c (n "email-type-rs") (v "1.0.1") (d (list (d (n "fake") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "validate") (r "^0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "0kxjqpkk6qfpq3shy65si785fqaqzpanccc36f3mpzmzcaks9r5w") (f (quote (("utils") ("default"))))))

