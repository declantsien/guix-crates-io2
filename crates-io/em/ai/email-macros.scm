(define-module (crates-io em ai email-macros) #:use-module (crates-io))

(define-public crate-email-macros-0.0.1 (c (n "email-macros") (v "0.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0rd02fwxs929xmbrmg7rfdgl90lakgdq5n8an5lb07csghk58qwc")))

(define-public crate-email-macros-0.0.2 (c (n "email-macros") (v "0.0.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "15bw0jpdcgfybq9b6ak2mfdwn5c76l92r8gqcj3py0jissgs090g")))

