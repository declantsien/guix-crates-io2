(define-module (crates-io em ai email-notif) #:use-module (crates-io))

(define-public crate-email-notif-1.0.0 (c (n "email-notif") (v "1.0.0") (d (list (d (n "lettre") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1vfgqispwhkiabxa8r8clrr3f66w85flpjhm9vzlw2jx0ismw61w")))

(define-public crate-email-notif-1.0.1 (c (n "email-notif") (v "1.0.1") (d (list (d (n "lettre") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "03bynb832w8872vsbf9zakzy3hlp8nyq98dpjipx53hvvp2jvkx9")))

