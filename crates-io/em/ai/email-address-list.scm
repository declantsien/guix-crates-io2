(define-module (crates-io em ai email-address-list) #:use-module (crates-io))

(define-public crate-email-address-list-0.1.0 (c (n "email-address-list") (v "0.1.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "0s7vyxdif2n9y2nr9yz05g537qk4vp31xcz2a7zb1aczgmzcib3q")))

(define-public crate-email-address-list-0.2.0 (c (n "email-address-list") (v "0.2.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "1kjrlf7fjdvqm7bns5qwbgpjwscz61r2rdj3m9lpghhd49fqzgg1")))

(define-public crate-email-address-list-0.2.1 (c (n "email-address-list") (v "0.2.1") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "1rlzb51sbjhls3bkx9jjhp0arp1p7nqy25w595ga77h5axzai9qx")))

(define-public crate-email-address-list-0.2.5 (c (n "email-address-list") (v "0.2.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "mailparse") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0cz1kygzr11l17j7myf1gqpdg4y3crrlig1nkz5fhi74viirkss3") (f (quote (("mailparse-conversions" "mailparse"))))))

(define-public crate-email-address-list-0.3.0 (c (n "email-address-list") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "mailparse") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1wbkgq7qdmg56h7qgnvnvhll88gvlgz5p1d0gi8z06p1a8cncnwr") (f (quote (("mailparse-conversions" "mailparse"))))))

