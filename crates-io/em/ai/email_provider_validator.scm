(define-module (crates-io em ai email_provider_validator) #:use-module (crates-io))

(define-public crate-email_provider_validator-0.1.0 (c (n "email_provider_validator") (v "0.1.0") (d (list (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)))) (h "13ryb7wh49xbpvqbvkfsy4p84zsblkcr7gnik7m31apaw5qib44m") (f (quote (("free") ("disposable") ("default" "free")))) (y #t)))

(define-public crate-email_provider_validator-0.1.1 (c (n "email_provider_validator") (v "0.1.1") (d (list (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)))) (h "175p6krmqxkl3lrlx7zbzg3jmgqw0f0vzl289sfsf31zzb23qa00") (f (quote (("free") ("disposable") ("default" "free")))) (y #t)))

(define-public crate-email_provider_validator-0.1.2 (c (n "email_provider_validator") (v "0.1.2") (d (list (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)))) (h "0bm37lnwny1pxqp0w72535fjza96b86xx14zl64hajl2ac8vpnqs") (f (quote (("free") ("disposable") ("default" "free"))))))

(define-public crate-email_provider_validator-0.1.3 (c (n "email_provider_validator") (v "0.1.3") (d (list (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "reqwest") (r "^0.12") (f (quote ("rustls-tls" "http2" "blocking"))) (k 1)))) (h "12qzwfnwdlxcdjpy11z3jmr7bs8q6wiffwdch8wm6q1ij9jygdhj") (f (quote (("free") ("disposable") ("default" "free"))))))

