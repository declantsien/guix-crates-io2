(define-module (crates-io em ai email-manager) #:use-module (crates-io))

(define-public crate-email-manager-0.1.0 (c (n "email-manager") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0fiib5s5crajaq0hxhmadyb4h4spbls1bbj1c60arbj6jncfk5wm")))

