(define-module (crates-io em ai email-format) #:use-module (crates-io))

(define-public crate-email-format-0.1.0 (c (n "email-format") (v "0.1.0") (d (list (d (n "buf-read-ext") (r "^0.2") (d #t) (k 0)))) (h "0bmr5422jvhbi1w71b2rjbzgy0i57rk4h9jghfq5mhi8d4v02q6v")))

(define-public crate-email-format-0.1.1 (c (n "email-format") (v "0.1.1") (d (list (d (n "buf-read-ext") (r "^0.2") (d #t) (k 0)))) (h "0wskpkwzcai2b8dh99hxzy5542rg0hcrwvp5vmnw3vzmpbfi0kbr")))

(define-public crate-email-format-0.1.2 (c (n "email-format") (v "0.1.2") (d (list (d (n "buf-read-ext") (r "^0.2") (d #t) (k 0)))) (h "15wckp48zw5q94fvj9n3zqm7sf15ddnghyzb702b9ddk2warv0ds")))

(define-public crate-email-format-0.2.0 (c (n "email-format") (v "0.2.0") (d (list (d (n "buf-read-ext") (r "^0.2") (d #t) (k 0)))) (h "04xrqvjn9vkv5p9bq23hxcn8nmys4qg403d9yp2wscqi07hrpp0a")))

(define-public crate-email-format-0.3.0 (c (n "email-format") (v "0.3.0") (d (list (d (n "buf-read-ext") (r "^0.2") (d #t) (k 0)))) (h "0bilxsa24wkj88b5ii2bpvxw9g9d9mhmg7aqqspm8l5q44h8587s")))

(define-public crate-email-format-0.3.1 (c (n "email-format") (v "0.3.1") (d (list (d (n "buf-read-ext") (r "^0.2") (d #t) (k 0)))) (h "0kmh1mirw0ad66jfyyfl08zbv204p3xlpa6d5xw07hd01irky0n9")))

(define-public crate-email-format-0.3.2 (c (n "email-format") (v "0.3.2") (d (list (d (n "buf-read-ext") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 2)) (d (n "mime_multipart") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "19jwqkplpf1d918lywa6il1702d2rbivffxbj70l7d535vdj4pn5") (f (quote (("default"))))))

(define-public crate-email-format-0.4.0 (c (n "email-format") (v "0.4.0") (d (list (d (n "buf-read-ext") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "12qxd32d00lxkdh7rlflgsn9s8xvc90hqn3hmnjvahd1if5rb7ka") (f (quote (("default"))))))

(define-public crate-email-format-0.4.1 (c (n "email-format") (v "0.4.1") (d (list (d (n "buf-read-ext") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0rz1sq5r29kx3fzgbj4lxdfgfi54xv2cpih4bq6q0582ak77r02l") (f (quote (("default"))))))

(define-public crate-email-format-0.5.0 (c (n "email-format") (v "0.5.0") (d (list (d (n "buf-read-ext") (r "^0.3") (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lettre") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1i1mxnwb8fkwdrrh6v9zl408qaxcfzlcnifpgbf0n68svbbj0wh9") (f (quote (("default"))))))

(define-public crate-email-format-0.6.0 (c (n "email-format") (v "0.6.0") (d (list (d (n "buf-read-ext") (r "^0.3") (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lettre") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0lsd7hdr3m602ax3lnf27c7xcqhs3ivbs1s632yjczdw0749x171") (f (quote (("default"))))))

(define-public crate-email-format-0.7.0 (c (n "email-format") (v "0.7.0") (d (list (d (n "buf-read-ext") (r "^0.3") (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lettre") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "17sm393i29r2cy3jwvlaw5qxg8nba6jlycpllzyfmamaypvq03xy") (f (quote (("default"))))))

(define-public crate-email-format-0.8.0 (c (n "email-format") (v "0.8.0") (d (list (d (n "buf-read-ext") (r "^0.3") (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lettre") (r ">= 0.9.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1fr7jg0d5qgqxsfal2s285gwlb5a1g2d7zljcmbz7f0b881vb3nx") (f (quote (("default"))))))

(define-public crate-email-format-0.8.1 (c (n "email-format") (v "0.8.1") (d (list (d (n "buf-read-ext") (r "^0.3") (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lettre") (r ">=0.9.2, <0.10") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0dy5dy07xyj4ms8dnzj4ga913j3g54nxxnx1qi1idj0npsp5s3xy") (f (quote (("default"))))))

