(define-module (crates-io em ai email-template) #:use-module (crates-io))

(define-public crate-email-template-0.1.0 (c (n "email-template") (v "0.1.0") (d (list (d (n "lettre") (r "^0.10.0-alpha.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0lg5j9ld9cyjp3g7v80h42r1xp8db68a09602i9bwndhfcd0pz28")))

(define-public crate-email-template-0.1.1 (c (n "email-template") (v "0.1.1") (d (list (d (n "lettre") (r "^0.10.0-alpha.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0hh4bw5l764gdqdv3i9dbpsifbzjyhnhvmckrif7wvyfw20m7h3f")))

