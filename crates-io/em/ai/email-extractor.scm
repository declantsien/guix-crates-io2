(define-module (crates-io em ai email-extractor) #:use-module (crates-io))

(define-public crate-email-extractor-0.1.0 (c (n "email-extractor") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (d #t) (k 0)) (d (n "mail_extractor") (r "^0.1.2") (d #t) (k 0)) (d (n "rust-s3") (r "^0.18.9") (d #t) (k 0)))) (h "1kyv1a59aimnpfkimw6cxqys0j0xha4s5bziqna7bpw1bl0f23k9")))

