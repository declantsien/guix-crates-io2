(define-module (crates-io em ai emailvalidation-rs) #:use-module (crates-io))

(define-public crate-emailvalidation-rs-0.1.0 (c (n "emailvalidation-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.7") (f (quote ("rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.131") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1pssk43sbm42cdcxw81xwnp3p093w43g7d2flwjp5apxfxcml8c4")))

(define-public crate-emailvalidation-rs-0.1.1 (c (n "emailvalidation-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.7") (f (quote ("rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.131") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0dplm1yijjnrkq9mc2hx2z92xr3djmy66yyfv6sk070bv4bpg7sb")))

