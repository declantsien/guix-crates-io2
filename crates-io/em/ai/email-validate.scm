(define-module (crates-io em ai email-validate) #:use-module (crates-io))

(define-public crate-email-validate-0.1.0 (c (n "email-validate") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "check-if-email-exists") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.27") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "cowstr") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "minstant") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.8.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0kx605angvkrp1xc9zkbin2y8j383m1y5wx68gxjx4rcvk4q0yxy") (f (quote (("all" "tests_integration")))) (y #t) (s 2) (e (quote (("tests_integration" "dep:tempfile"))))))

(define-public crate-email-validate-0.1.1 (c (n "email-validate") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "check-if-email-exists") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.27") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "cowstr") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "minstant") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.8.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1z1vyckhr05b76sr4nynz9wxbrq3mzn9i7dmfaakzxvw1zz15b1k") (f (quote (("all" "tests_integration")))) (s 2) (e (quote (("tests_integration" "dep:tempfile"))))))

(define-public crate-email-validate-0.1.2 (c (n "email-validate") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "check-if-email-exists") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.27") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "cowstr") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "minstant") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.8.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "090l9yb5xm10944sqrzdf4dpkycp91z8fs669f8w32srq63r9yp9") (f (quote (("all" "tests_integration")))) (s 2) (e (quote (("tests_integration" "dep:tempfile"))))))

(define-public crate-email-validate-0.1.3 (c (n "email-validate") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "check-if-email-exists") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.27") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "cowstr") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "minstant") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.8.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "16rkvl41yz9qj97rgsbb7nymv71v5zk6bx7xdkpishrlq02j2833") (f (quote (("all" "tests_integration")))) (s 2) (e (quote (("tests_integration" "dep:tempfile"))))))

(define-public crate-email-validate-0.1.4 (c (n "email-validate") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "check-if-email-exists") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.27") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "cowstr") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "minstant") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.8.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0j41n82z4nfhasx6qaz2jf00x6i8ghjbmr4sv3y9a64fdfryparh") (f (quote (("all" "tests_integration")))) (s 2) (e (quote (("tests_integration" "dep:tempfile"))))))

(define-public crate-email-validate-0.1.5 (c (n "email-validate") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "check-if-email-exists") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.27") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "cowstr") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "minstant") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.8.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1pchwksabknbqm1ajqv4ajx6z3cvf4gfc2kms9ma9dsn915bdkns") (f (quote (("all" "tests_integration")))) (s 2) (e (quote (("tests_integration" "dep:tempfile"))))))

(define-public crate-email-validate-0.1.6 (c (n "email-validate") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "check-if-email-exists") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.27") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "cowstr") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "minstant") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.8.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "01q5f5iyxa98ylacxb1c41grr6ny9apck0r7l382amqmcwaddbsa") (f (quote (("all" "tests_integration")))) (s 2) (e (quote (("tests_integration" "dep:tempfile"))))))

(define-public crate-email-validate-0.1.7 (c (n "email-validate") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "check-if-email-exists") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.27") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "cowstr") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "minstant") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.8.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1vsbr7rd195y12yr7nimwn29dq5zmniqy3bk2fsrk9s1v96v379k") (f (quote (("all" "tests_integration")))) (s 2) (e (quote (("tests_integration" "dep:tempfile"))))))

(define-public crate-email-validate-0.1.8 (c (n "email-validate") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "check-if-email-exists") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.27") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "cowstr") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "email-address-parser") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "minstant") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.8.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "155cqm934bqz4sva6hfbp1kygi92icd87v6v1y3idl6h9av5zarv") (f (quote (("all" "tests_integration")))) (s 2) (e (quote (("tests_integration" "dep:tempfile"))))))

(define-public crate-email-validate-0.1.9 (c (n "email-validate") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "check-if-email-exists") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.27") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "cowstr") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "email-address-parser") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "minstant") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.8.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "17ibi8fl9y7kjribvycb4hqi1ir3bjyqiqwm4z5brlskb63jrqha") (f (quote (("all" "tests_integration")))) (s 2) (e (quote (("tests_integration" "dep:tempfile"))))))

(define-public crate-email-validate-0.1.10 (c (n "email-validate") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "check-if-email-exists") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.27") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "cowstr") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "email-address-parser") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "minstant") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.8.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1dvm5fs1ybvvmi1sfyi2cz9492g4gjscw81p11f4b3jrfnk8yjmx") (f (quote (("all" "tests_integration")))) (s 2) (e (quote (("tests_integration" "dep:tempfile"))))))

(define-public crate-email-validate-0.1.11 (c (n "email-validate") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "check-if-email-exists") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.27") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "cowstr") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "email-address-parser") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "minstant") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.8.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "05mv2k6gaaz54i0036ifbsy5il9qa5l0f3l52an5q87bk839b2bp") (f (quote (("all" "tests_integration")))) (s 2) (e (quote (("tests_integration" "dep:tempfile"))))))

(define-public crate-email-validate-0.1.12 (c (n "email-validate") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "check-if-email-exists") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.27") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "cowstr") (r "^1.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "email-address-parser") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 0)) (d (n "minstant") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.8.0") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0dbc2g3zsk7w3vw2qj5da21cwimh4wirq4dfkkcah96sfycp7xy5") (f (quote (("all" "tests_integration")))) (s 2) (e (quote (("tests_integration" "dep:tempfile"))))))

