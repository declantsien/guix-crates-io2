(define-module (crates-io em ai email_to_image) #:use-module (crates-io))

(define-public crate-email_to_image-0.1.2 (c (n "email_to_image") (v "0.1.2") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "imageproc") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.9") (d #t) (k 0)) (d (n "zip") (r "^0.2.6") (d #t) (k 0)))) (h "1dmr5y4jam3r2hkf2gp50cdmmc8779xfx9nci257yilslws3bih1")))

