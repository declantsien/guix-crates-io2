(define-module (crates-io em ai email-verifier) #:use-module (crates-io))

(define-public crate-email-verifier-0.1.0 (c (n "email-verifier") (v "0.1.0") (h "1hqih2l8yr1y2wlyns8k6vx1ghlkiakanpsvhj9s37245c2h0bsz")))

(define-public crate-email-verifier-0.1.1 (c (n "email-verifier") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mxlqf6ka0wg4f5bcdn2kjbbqwl3p0n9ixs92pk5dj2fir36f825")))

(define-public crate-email-verifier-0.1.2 (c (n "email-verifier") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jvkhmja228134l5kh2hraxck36hx7cli9725qdrgp8wzvqwyxz9")))

(define-public crate-email-verifier-0.1.3 (c (n "email-verifier") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y4acghg0vn69hxlaywhim8mr80ijfrbarjlqkhn53200cj6yz4h")))

(define-public crate-email-verifier-0.1.4 (c (n "email-verifier") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05lmc4q3d2j5gx5scv81l27n8qih0rvrn703qzm6q1d0d7y5dqc2")))

