(define-module (crates-io em ai email) #:use-module (crates-io))

(define-public crate-email-0.0.1 (c (n "email") (v "0.0.1") (d (list (d (n "encoding") (r "~0.2.3") (d #t) (k 0)))) (h "0m4p1rspbhan25qipaqhm8l4i73gzyzzyczk43brw4f4ryaw939c")))

(define-public crate-email-0.0.2 (c (n "email") (v "0.0.2") (d (list (d (n "encoding") (r "~0.2.3") (d #t) (k 0)))) (h "1pddn90fndpjn179ccc75byivz8x50abbpjsq55agipd9jf2g35s")))

(define-public crate-email-0.0.3 (c (n "email") (v "0.0.3") (d (list (d (n "encoding") (r "~0.2.3") (d #t) (k 0)))) (h "170x1zxcrx41v587bbvq3c3dh5km70gb0hsvbxka3anq40whwl70")))

(define-public crate-email-0.0.4 (c (n "email") (v "0.0.4") (d (list (d (n "encoding") (r "~0.2.6") (d #t) (k 0)))) (h "1hsclqzas6a6pxbl6k7l7b4h4w30li9jlzvlakc2vfpmq9q1n69w")))

(define-public crate-email-0.0.5 (c (n "email") (v "0.0.5") (d (list (d (n "chrono") (r "~0.1.5") (d #t) (k 0)) (d (n "encoding") (r "~0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "~0.1.2") (d #t) (k 0)) (d (n "time") (r "~0.1.3") (d #t) (k 0)))) (h "0kpz24q8j6wav3nbva3hhfx7s2x7cn88zk0rlw38402pg10ad5sv")))

(define-public crate-email-0.0.6 (c (n "email") (v "0.0.6") (d (list (d (n "chrono") (r "~0.1.7") (d #t) (k 0)) (d (n "encoding") (r "~0.2.12") (d #t) (k 0)) (d (n "lazy_static") (r "~0.1.4") (d #t) (k 0)) (d (n "time") (r "~0.1.7") (d #t) (k 0)))) (h "0gag1wh9ayhzwwf10nbpqjg3zrhqhfc9vb51x9wddjhnsrdpdm4f")))

(define-public crate-email-0.0.7 (c (n "email") (v "0.0.7") (d (list (d (n "chrono") (r "~0.1.7") (d #t) (k 0)) (d (n "encoding") (r "~0.2.12") (d #t) (k 0)) (d (n "lazy_static") (r "~0.1.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "~0.1.7") (d #t) (k 0)))) (h "0gfx6gpqghkaq3hn9zzvpdrn0m0xlphjcay798pmidcjlgsa6z0i")))

(define-public crate-email-0.0.8 (c (n "email") (v "0.0.8") (d (list (d (n "chrono") (r "~0.1.7") (d #t) (k 0)) (d (n "encoding") (r "~0.2.12") (d #t) (k 0)) (d (n "lazy_static") (r "~0.1.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "~0.1.7") (d #t) (k 0)))) (h "0j7pk4shjabwy3sir6ff58lajh0grjf23iaka8fdrih3b3a2gii9")))

(define-public crate-email-0.0.9 (c (n "email") (v "0.0.9") (d (list (d (n "chrono") (r "~0.2.1") (d #t) (k 0)) (d (n "encoding") (r "~0.2.12") (d #t) (k 0)) (d (n "lazy_static") (r "~0.1.4") (d #t) (k 0)) (d (n "rand") (r "~0.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "~0.1.7") (d #t) (k 0)))) (h "0353h1wsf0y828fd2aq6jh46fjrn8imd2vp7byf7i63cz5i64r83")))

(define-public crate-email-0.0.10 (c (n "email") (v "0.0.10") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0za3014qar6gzild1spx89iv1nwgff0i23yx5imxha4ccpa2fydg")))

(define-public crate-email-0.0.11 (c (n "email") (v "0.0.11") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "09675ja8qr1vi12208xgsl9ms468f7zk733x2kymr5lib26mwj22")))

(define-public crate-email-0.0.12 (c (n "email") (v "0.0.12") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0zpqvvak7vgziq8mq5h7q1n9jlydvjbii4pr65lkvmyn7mxvp4ss")))

(define-public crate-email-0.0.13 (c (n "email") (v "0.0.13") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "112rvxb21ilz0jbrnzw4pxzai73ggwm4f54pf80295n0dg9065j8")))

(define-public crate-email-0.0.14 (c (n "email") (v "0.0.14") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1cca81c7kflilmy6wvvazzli9xzj7018k5fx83qdb4b114kk76y0") (f (quote (("nightly"))))))

(define-public crate-email-0.0.15 (c (n "email") (v "0.0.15") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "077pq81c9m3ydrkmgxz9xlwhmp5xirr3z0c8jh62zx3mzj45pqgx")))

(define-public crate-email-0.0.16 (c (n "email") (v "0.0.16") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "140yp92bcas33cybl67x0cnpncpw23r8s21276y0mn56ybkfjd5g") (f (quote (("nightly"))))))

(define-public crate-email-0.0.17 (c (n "email") (v "0.0.17") (d (list (d (n "base64") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0krgfwk9iw548f10504zdmpw3w8f1mwyvpq2chz41cwf9vxhsml8") (f (quote (("nightly"))))))

(define-public crate-email-0.0.18 (c (n "email") (v "0.0.18") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1pnl8ig9zdsk39k94gyr0qnxjpkysm6c989z0qjwjg7y3acxs288") (f (quote (("nightly"))))))

(define-public crate-email-0.0.19 (c (n "email") (v "0.0.19") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0jn7j2dz7fiq88288vk926is699fp5b8fm4py5iqphfclm5vbb7s") (f (quote (("nightly"))))))

(define-public crate-email-0.0.20 (c (n "email") (v "0.0.20") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1r12byj69x6bkq0qn1zvi103n1pg5k3w8zym2dgich82pd8rlm4i") (f (quote (("nightly"))))))

(define-public crate-email-0.0.21 (c (n "email") (v "0.0.21") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)))) (h "0gkv0lgs1apmq3w13pj2qr2bxiy42hw3vgi1jsb705l3p01hadk5") (f (quote (("nightly"))))))

