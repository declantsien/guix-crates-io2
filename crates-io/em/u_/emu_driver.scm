(define-module (crates-io em u_ emu_driver) #:use-module (crates-io))

(define-public crate-emu_driver-0.1.0 (c (n "emu_driver") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pixels") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.10") (d #t) (k 0)))) (h "1j100a52lrmb0wviiw5mv9ji20aqc20jzslqbgfqli0gi0czr6hv")))

