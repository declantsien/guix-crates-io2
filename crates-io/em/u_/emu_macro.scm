(define-module (crates-io em u_ emu_macro) #:use-module (crates-io))

(define-public crate-emu_macro-0.1.0 (c (n "emu_macro") (v "0.1.0") (d (list (d (n "em") (r "0.*") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "visit" "fold"))) (d #t) (k 0)))) (h "0vabsz4kn9j3shvxdps9kw3xgjr4n7macyix2a2swx7wq8gn8j38")))

