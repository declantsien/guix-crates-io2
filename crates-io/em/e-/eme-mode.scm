(define-module (crates-io em e- eme-mode) #:use-module (crates-io))

(define-public crate-eme-mode-0.1.0 (c (n "eme-mode") (v "0.1.0") (d (list (d (n "aes") (r "^0.5.0") (d #t) (k 2)) (d (n "block-cipher") (r "^0.8.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.6.1") (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)))) (h "0nhsilf16f76varvhsyg4ps217qb1v7csp70c7w784bz9gbbfqg7") (f (quote (("std" "block-modes/std") ("default" "std"))))))

(define-public crate-eme-mode-0.2.0 (c (n "eme-mode") (v "0.2.0") (d (list (d (n "aes") (r "^0.7.5") (d #t) (k 2)) (d (n "block-modes") (r "^0.8.1") (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "cipher") (r "^0.3.0") (d #t) (k 0)))) (h "1db4f2v76i02f6pip6zai3w1ld5s80r7vaza4fwy9s332va13n0k") (f (quote (("std" "block-modes/std") ("default" "std"))))))

(define-public crate-eme-mode-0.2.1 (c (n "eme-mode") (v "0.2.1") (d (list (d (n "aes") (r "^0.7.5") (d #t) (k 2)) (d (n "block-modes") (r "^0.8.1") (k 0)) (d (n "block-padding") (r "^0.2.1") (d #t) (k 0)) (d (n "cipher") (r "^0.3.0") (d #t) (k 0)))) (h "1ikdqj0mc9kc2l5g03bh69v5k5rlhxcflr1mj893xzcdkq63clqq") (f (quote (("std" "block-modes/std") ("default" "std"))))))

(define-public crate-eme-mode-0.3.0 (c (n "eme-mode") (v "0.3.0") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 2)) (d (n "cipher") (r "^0.4.3") (d #t) (k 0)))) (h "1872b2a4zkkvz0g8x9bwbbxpn2vibz581w399ss80r60v4yphsfk") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("default" "block-padding") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc"))))))

(define-public crate-eme-mode-0.3.1 (c (n "eme-mode") (v "0.3.1") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 2)) (d (n "cipher") (r "^0.4.3") (d #t) (k 0)))) (h "1vs3vwr2c1qn10l232z761ymbyccjs8m86fy6ljxb1hlljy58dh6") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("default" "block-padding") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc"))))))

