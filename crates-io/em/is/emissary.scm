(define-module (crates-io em is emissary) #:use-module (crates-io))

(define-public crate-emissary-0.1.0 (c (n "emissary") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.27") (d #t) (k 0)))) (h "05pn2yd82lgw2lgf451a12r30hkb3rkbgf16cgdjxdmicmr9xpgg")))

