(define-module (crates-io em bi embin) #:use-module (crates-io))

(define-public crate-embin-1.0.0 (c (n "embin") (v "1.0.0") (d (list (d (n "clap") (r "^4.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1b57379j76c15smjmrnpi6r2171pv4rwmk3scbic4bfb7i1y0ndg")))

(define-public crate-embin-1.1.0 (c (n "embin") (v "1.1.0") (d (list (d (n "clap") (r "^4.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0ifwzk9pz8hvc1v60vnrkn0vy1ldcwzp66r42qvf9w2q7g2frc8w")))

(define-public crate-embin-1.1.1 (c (n "embin") (v "1.1.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "147pr829spv5dfiib7mbzr2mymx6xfi8v742l1d1qnmfix6gp1kr")))

(define-public crate-embin-1.1.2 (c (n "embin") (v "1.1.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "10mk3db93ccza1ga8qjm5aqk4xr6yz255qnbl3qn30kv5xdn20b8")))

(define-public crate-embin-1.1.3 (c (n "embin") (v "1.1.3") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1lwykmap6wbzx9gx0d6s3zns04f1gbdaswvpql8lb11hzlm8rfyc")))

(define-public crate-embin-1.1.4 (c (n "embin") (v "1.1.4") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "11ib46ndhadr858jj01fb5lpz5flj1nxrpd098wziw8x3kj46cqx")))

