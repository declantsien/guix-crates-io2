(define-module (crates-io em ac emacs-macros) #:use-module (crates-io))

(define-public crate-emacs-macros-0.1.0 (c (n "emacs-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zhv8dk55v2nfzc2afm4s1sy9aa9d6r18afrwlpqdlqbl016pvps")))

(define-public crate-emacs-macros-0.8.0 (c (n "emacs-macros") (v "0.8.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0szqscsf0jz6kjvb8ps3d1wrpcpl3pw8hl6mys0lm1bq3kdxv2q5")))

(define-public crate-emacs-macros-0.10.0 (c (n "emacs-macros") (v "0.10.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rh41gy6l42wkw7qnjwdj685ghlq4ax5ra8qdq7hygi3w53ky81w")))

(define-public crate-emacs-macros-0.10.2 (c (n "emacs-macros") (v "0.10.2") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sj51yd0hjxs6a61kwhrcjljmanw3widrjlxlkbj86sgxlwdydzb")))

(define-public crate-emacs-macros-0.10.3 (c (n "emacs-macros") (v "0.10.3") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hyxih61laqcz0la4q3c01jqm7p3ifnwnrg62adadnrhs2ri3wfi")))

(define-public crate-emacs-macros-0.11.0 (c (n "emacs-macros") (v "0.11.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0390y8vafxdi334hhgrzvcqjq3n5ckcmvilqcfp8vajjq8irrly6")))

(define-public crate-emacs-macros-0.12.0 (c (n "emacs-macros") (v "0.12.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09140c57law9xdydxp8rhf5396ml39bn6l72km16n0kaqk11vfip")))

(define-public crate-emacs-macros-0.13.0 (c (n "emacs-macros") (v "0.13.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0534y333jrx19bxd6li307l7945y9wcrkalcsmxiq77p0g31r2ca")))

(define-public crate-emacs-macros-0.14.1 (c (n "emacs-macros") (v "0.14.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jmls5l90mc9mgw0czfgxg4j9ahqdyv7afrkgfgsihqp1axbw2xw")))

(define-public crate-emacs-macros-0.15.1 (c (n "emacs-macros") (v "0.15.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xz4scnxrvssa5cb4avw0xyiphzl1ms03zjpxvyc1v0iy471cc8b")))

(define-public crate-emacs-macros-0.17.0 (c (n "emacs-macros") (v "0.17.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qg1dcn5acbirq617qq2fgg9adswif2dnr292s3qnq62wzgnyrb9")))

