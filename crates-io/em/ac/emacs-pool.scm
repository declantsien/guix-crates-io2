(define-module (crates-io em ac emacs-pool) #:use-module (crates-io))

(define-public crate-emacs-pool-0.2.0 (c (n "emacs-pool") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.16") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("io-util" "macros" "net" "process" "rt" "signal" "time"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1chhyw9xahh5cfawmymxlbxs45wkp616qh6l0hjiijyafqxz5qp7")))

