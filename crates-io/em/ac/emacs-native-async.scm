(define-module (crates-io em ac emacs-native-async) #:use-module (crates-io))

(define-public crate-emacs-native-async-0.1.0 (c (n "emacs-native-async") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "emacs") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.6") (d #t) (k 0)))) (h "0i3z8pbnl117rvjphd4njm58wf0vmqy5nnh2djhvllwdqmz9zm4b")))

(define-public crate-emacs-native-async-0.1.1 (c (n "emacs-native-async") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "emacs") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.6") (d #t) (k 0)))) (h "1nb75yhxrnl79231qi8mh0mfqpaprld48sj3b25di6jf5mlbw6wc")))

(define-public crate-emacs-native-async-0.1.2 (c (n "emacs-native-async") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "emacs") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.6") (d #t) (k 0)))) (h "0lcz8m49fhn2kyqh02lrcyvpi7bmchpm6n27vvll6kbk1x7vnc82")))

