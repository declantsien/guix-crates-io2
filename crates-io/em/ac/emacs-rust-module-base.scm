(define-module (crates-io em ac emacs-rust-module-base) #:use-module (crates-io))

(define-public crate-emacs-rust-module-base-0.1.0 (c (n "emacs-rust-module-base") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)) (d (n "hyper") (r "^0.9") (f (quote ("security-framework"))) (k 1)))) (h "0kg03lcfsnafi3phh9w1yxm8xcibkc9q5gdzvv8h4rw48d4r5552")))

