(define-module (crates-io em ac emacs_module_bindings) #:use-module (crates-io))

(define-public crate-emacs_module_bindings-0.6.0 (c (n "emacs_module_bindings") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.29") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05ighlj69f3v43wv7m127kdfg5idgd13avdpxlsyhaasfv510yzx")))

(define-public crate-emacs_module_bindings-0.6.1 (c (n "emacs_module_bindings") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.29") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r22h5n17kdz8jqn1fnga786wfqx6pa28lc70k70cajnbjksh1zl")))

(define-public crate-emacs_module_bindings-0.6.2 (c (n "emacs_module_bindings") (v "0.6.2") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1f8yd6vgfaal58drzjavjg8zjx1gqjby0sf6pqwarynijgjxjidl")))

(define-public crate-emacs_module_bindings-0.7.0 (c (n "emacs_module_bindings") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1sm57in5fnid9ghgwipvbcv8v8g6mi67vp9grzwfwlw3rik252k4")))

