(define-module (crates-io em ac emacs_module) #:use-module (crates-io))

(define-public crate-emacs_module-0.1.0 (c (n "emacs_module") (v "0.1.0") (h "1q4xz04anas8cl9s2fkxygrq4s3j2gnw0hj7gy86jw27qvp8gnrx")))

(define-public crate-emacs_module-0.2.0 (c (n "emacs_module") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)))) (h "1w5x922ra1k89cp9zky6457i8q0wcfja91k51xclps002nrqdbdz")))

(define-public crate-emacs_module-0.2.1 (c (n "emacs_module") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)))) (h "1p0n12j7pygb5z21kvxq4mjw0nvw0al2n0y9fvfmnyzlc6za1z04")))

(define-public crate-emacs_module-0.3.0 (c (n "emacs_module") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)))) (h "1d3n6j3ffjivf5ihh8iksj16bzmbsxpc868gzxrjinzc2msglm9w")))

(define-public crate-emacs_module-0.4.0 (c (n "emacs_module") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.48.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)))) (h "0rzjckpdbhjsna7qp6a43msylmrdhxk5szv1xz4h1k73k3l2jrf2")))

(define-public crate-emacs_module-0.10.0 (c (n "emacs_module") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.48.1") (d #t) (k 1)))) (h "1gf9lz735xbkyir53dyv362drfx3nin5an5cx39kd8q8kjjwix5g")))

(define-public crate-emacs_module-0.12.0 (c (n "emacs_module") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "0h57x1lppd93cbs3nix9z6yxwf3waipnmp53zd7sr5zb8pf2s6ks")))

(define-public crate-emacs_module-0.16.2 (c (n "emacs_module") (v "0.16.2") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)))) (h "0s8qrh0ggjmqr04zkcf7s4ijmpd44rjcag78npnq64jv10lxvsry")))

(define-public crate-emacs_module-0.18.0 (c (n "emacs_module") (v "0.18.0") (d (list (d (n "bindgen") (r "^0.59.0") (o #t) (d #t) (k 1)))) (h "1ypjyyv2ca3vza4sia91ckxamgfk63yd8frkvg3d4ph4fk4pn1mk") (f (quote (("default"))))))

