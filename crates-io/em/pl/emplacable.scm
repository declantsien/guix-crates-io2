(define-module (crates-io em pl emplacable) #:use-module (crates-io))

(define-public crate-emplacable-0.1.0-alpha.1 (c (n "emplacable") (v "0.1.0-alpha.1") (h "1ff0csy676kyc9bjxbg680bvvjipndslxd8cmiw5g6dm4rj0zkmq") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-emplacable-0.1.0-alpha.2 (c (n "emplacable") (v "0.1.0-alpha.2") (h "10577pyqig5b0mhm8504kk7qd9bgbc361skxv8l20xhijwfhy98j") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-emplacable-0.1.0-alpha.3 (c (n "emplacable") (v "0.1.0-alpha.3") (h "1fcgqc0cx7g002ra3lz9pfqnmjvjlxfs6i1fsifk7sbp79q7qdm9") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-emplacable-0.1.0-alpha.4 (c (n "emplacable") (v "0.1.0-alpha.4") (h "1g8g478xhrwff4qiwnpvw91xg9x0iya3708j16p6ayl70k0rkjsr") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-emplacable-0.1.0-alpha.5 (c (n "emplacable") (v "0.1.0-alpha.5") (h "0zh4v31igvhhp0kl8xmza6hnllshh6g3bhrxnyhlbr04hl769618") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-emplacable-0.1.0-alpha.6 (c (n "emplacable") (v "0.1.0-alpha.6") (h "0g19108jsh30q74134c4vcyw6akbz59wx3rv47qadgzr4r4z40n1") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-emplacable-0.1.0-alpha.7 (c (n "emplacable") (v "0.1.0-alpha.7") (h "0v7nxpq6072sg132yxbc2214c3rd0mcvq7bw1fz7g9d492hcl731") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-emplacable-0.1.0-alpha.8 (c (n "emplacable") (v "0.1.0-alpha.8") (h "08g86sdi9525iwrrchhfb0x0pkrd05di039nz0r902wjmwhfn8zd") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-emplacable-0.1.0-alpha.9 (c (n "emplacable") (v "0.1.0-alpha.9") (h "0dpc040iv4klzbdi42kfx82mcf1xx3llnd08dmqm34ww9aay7nsa") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-emplacable-0.1.0-alpha.10 (c (n "emplacable") (v "0.1.0-alpha.10") (h "1ag3rnf67isy5hfxsvc75kl3q6q11z7cvqqwf7pg3nzx7maknvz1") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-emplacable-0.1.0-alpha.11 (c (n "emplacable") (v "0.1.0-alpha.11") (h "0piibr58v9hdv2hvf59fnyvqkbi14rcc4rpnyzaagk0x7jgdidc1") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-emplacable-0.1.0-alpha.12 (c (n "emplacable") (v "0.1.0-alpha.12") (h "0sznlxrnicgm7qlla4hg6pymxq7sbn5bzxhm3v5n4mxrqnsdczf2") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

