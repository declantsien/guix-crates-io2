(define-module (crates-io em er emerald_kernel_user_link) #:use-module (crates-io))

(define-public crate-emerald_kernel_user_link-0.2.1 (c (n "emerald_kernel_user_link") (v "0.2.1") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0dvs5a34h31zkb6vyapnghqs6gz8r0398byrqnb6g7c1v1bvv187") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-emerald_kernel_user_link-0.2.2 (c (n "emerald_kernel_user_link") (v "0.2.2") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0v7vmyzwc0isd1nfcvvs2sq3jlywa8204gkppwfrm2jj1v9wa4ig") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-emerald_kernel_user_link-0.2.3 (c (n "emerald_kernel_user_link") (v "0.2.3") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "15vvm3daq3k4fkn016gh2wd4vb821ixqhbm13ryljajsp3534cam") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-emerald_kernel_user_link-0.2.4 (c (n "emerald_kernel_user_link") (v "0.2.4") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1hragfdjc735v8lk2q5f58h34z8wxw972sg9jcsx869cqm332n6j") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-emerald_kernel_user_link-0.2.5 (c (n "emerald_kernel_user_link") (v "0.2.5") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "01brwkjl723f7961lg238jnw1q24z3lj7826jw1n7v5v351a0akz") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-emerald_kernel_user_link-0.2.6 (c (n "emerald_kernel_user_link") (v "0.2.6") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1shx0h24w6mwx9argg0nphkd1d7prkmb844cljnrxqxband6wzbb") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-emerald_kernel_user_link-0.2.7 (c (n "emerald_kernel_user_link") (v "0.2.7") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "01hksf0wz0yn7af9505zp4xljxd02p9h62yxmskrqx7hw11y8d03") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-emerald_kernel_user_link-0.2.8 (c (n "emerald_kernel_user_link") (v "0.2.8") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1rvlr7nn1yih7l8wyy59c7yfvbwcm97cw25rs3l68d2yaz0gnisn") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-emerald_kernel_user_link-0.2.9 (c (n "emerald_kernel_user_link") (v "0.2.9") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0zd26sssgd71giisfsnyjamg76pc3kavni7dzfwzi2fc0jkia6p7") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-emerald_kernel_user_link-0.2.10 (c (n "emerald_kernel_user_link") (v "0.2.10") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "12gn7la7iyn2hlzjy4i8752fncxs16aaf1l29aa0hc8d8gp353ml") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-emerald_kernel_user_link-0.2.11 (c (n "emerald_kernel_user_link") (v "0.2.11") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0h47q0lxc7hgc1aghjsd8zh5gafazaksvrzvf3v057rdj18i2ncv") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-emerald_kernel_user_link-0.2.12 (c (n "emerald_kernel_user_link") (v "0.2.12") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0n63gd2wilr631din50iw60kbmyc2bxw0ww0a9bdj0vvdzlpjqb3") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

