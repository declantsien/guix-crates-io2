(define-module (crates-io em er emergent) #:use-module (crates-io))

(define-public crate-emergent-0.1.0 (c (n "emergent") (v "0.1.0") (h "168vr451zwhkzqkniyj9fbmm4qcb4nyn0dnvwd2hddzmhx5jq51v")))

(define-public crate-emergent-1.0.0 (c (n "emergent") (v "1.0.0") (h "0icvf2am612q8kywjd9w27m58pngd03pi1m1qmivh1vp72mxnvnf") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.1.0 (c (n "emergent") (v "1.1.0") (h "1ms5cgnzw6qhx2qs1vz2ac7kxm5pm2abhapl2zpg0ianr32icdgx") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.2.0 (c (n "emergent") (v "1.2.0") (h "1gjsy07kn514wy943cafbnmnclbryz5d4wilb63ml79i16z4fl8p") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.2.1 (c (n "emergent") (v "1.2.1") (h "1v2dxns868n3p7rsp2vfxyazzmg5ml16y6iq82x17960mplq6rc6") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.3.0 (c (n "emergent") (v "1.3.0") (h "0p8d7pndling59qfvdkj1z8vjmn4aqqirxq7yqsvrmwqhvy63vp6") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.4.0 (c (n "emergent") (v "1.4.0") (h "1ygakdnskc26k19sv83qy707psd5iqj9ffr274ram4smcw5b233c") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.0 (c (n "emergent") (v "1.5.0") (h "1sa84h5ygk51lw9i6mkqvyq3f60ry0x84v531fcd5kbn41hsc5rc") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.1 (c (n "emergent") (v "1.5.1") (h "0cc1apwjxxn17sfc26zdqaf8lvqgwvajwpn2rpwygxl3f09rk00q") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.2 (c (n "emergent") (v "1.5.2") (h "1qvxnxaiwq6x8kihf5rmspqbz18j6iihrpaqvxhisdqw8960jhac") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.3 (c (n "emergent") (v "1.5.3") (h "1a4qlfqhghkwxk2zdwgv8nnrm2iay8n1qi5i73hav8mbil4qkn2v") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.4 (c (n "emergent") (v "1.5.4") (h "04cxiy4qxfk2y0lbbzc0bmfmidcphsd7k7ppwlsprbmfm5dqi3bn") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.5 (c (n "emergent") (v "1.5.5") (h "1zzs5lxxn46pcisf52w5gbk2863bcz3567ylnlg57wm31gs3fvva") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.6 (c (n "emergent") (v "1.5.6") (h "0h0kf3gh97f46chh6wcwi3hx6sq5vx5pdr88xsiidyfbzmgc8mcq") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.7 (c (n "emergent") (v "1.5.7") (h "1dck0liq4jrwap8npkvjyg8yvq5m9ar16i7rv8iwxmgzyx2c2b1m") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.8 (c (n "emergent") (v "1.5.8") (h "1yhvx427b70yrz4gvnlmaz0ihh1zmp4iwblqa1arpliadc17a913") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.9 (c (n "emergent") (v "1.5.9") (h "0ipr7cfgdrk3riwnb9k1v7vmzp48shf32qv84pha11cfzcdwypsx") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.10 (c (n "emergent") (v "1.5.10") (h "1p6bdg7g5z1vqskpymfzsyvi76zx0hpxnva1xzl4r5jm83yb70k2") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.11 (c (n "emergent") (v "1.5.11") (h "1kg1b42pdrpw5ymf7apihcdwwqhwxh8i3i8a6pk2ja8454rxf3n1") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.12 (c (n "emergent") (v "1.5.12") (h "1xa8x062ggi505wdf1071bn5ywfg26a2202mljdfrkwp9a0mmipn") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.5.13 (c (n "emergent") (v "1.5.13") (h "14xgcz84crbrw0rk04sh5s75nh0xn79b7sb0lwm9v4azanwcmms8") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.6.0 (c (n "emergent") (v "1.6.0") (h "0hl016i25kvb2bzqlnfx7if1781x17am1b1jrw4ph90z97f7qkl5") (f (quote (("scalar64"))))))

(define-public crate-emergent-1.7.0 (c (n "emergent") (v "1.7.0") (h "0qqc45dnpy7pb1l77hj3gm9apqwzsnshhn14wz8bznipcjpnn6p8") (f (quote (("scalar64"))))))

