(define-module (crates-io em er emerald_runtime) #:use-module (crates-io))

(define-public crate-emerald_runtime-0.1.0 (c (n "emerald_runtime") (v "0.1.0") (d (list (d (n "kernel_user_link") (r "^0.2.5") (d #t) (k 0) (p "emerald_kernel_user_link")))) (h "0s670k00f2ihnxd2i1qpa8hyjwwghqg3zqac4ynxkxa4r31hyp9q")))

(define-public crate-emerald_runtime-0.1.1 (c (n "emerald_runtime") (v "0.1.1") (d (list (d (n "kernel_user_link") (r "^0.2.6") (d #t) (k 0) (p "emerald_kernel_user_link")))) (h "05x4d72lc0jrqa8l3f5ab35h1r7hdj9c78frii98bmnad2hlb7ha")))

