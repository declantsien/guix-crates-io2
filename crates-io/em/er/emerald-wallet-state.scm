(define-module (crates-io em er emerald-wallet-state) #:use-module (crates-io))

(define-public crate-emerald-wallet-state-0.1.0 (c (n "emerald-wallet-state") (v "0.1.0") (d (list (d (n "bitcoin") (r "~0.29") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "emerald-vault") (r "^0.32.0") (d #t) (k 0)) (d (n "enum-display-derive") (r "^0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "protobuf") (r "^2.25") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "08g2gqf5fdh2yhaqqw2yjspviaz9caxncpagr2sah95yzywkbkmx")))

