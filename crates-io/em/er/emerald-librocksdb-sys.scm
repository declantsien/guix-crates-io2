(define-module (crates-io em er emerald-librocksdb-sys) #:use-module (crates-io))

(define-public crate-emerald-librocksdb-sys-5.11.3 (c (n "emerald-librocksdb-sys") (v "5.11.3") (d (list (d (n "bindgen") (r "^0.29") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "const-cstr") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "0m1sa7ycsrjpah17hhd49r37y7z9x687247yyvppfs8lnzhsc690") (f (quote (("static") ("default" "static"))))))

