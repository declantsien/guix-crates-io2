(define-module (crates-io em er emerald-rocksdb) #:use-module (crates-io))

(define-public crate-emerald-rocksdb-0.7.0 (c (n "emerald-rocksdb") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.0.0") (d #t) (k 0)))) (h "1274z9lslgbdjn65fpyrdyddv0ivg17j1nxyp3sc6r9kc1ac626x") (f (quote (("valgrind") ("default")))) (y #t)))

(define-public crate-emerald-rocksdb-1.0.0 (c (n "emerald-rocksdb") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1pl8slcw0jdgbq97m188mwyzia6ximqkyjp0z9h0hgpvx062cvg8") (f (quote (("valgrind") ("default")))) (y #t)))

(define-public crate-emerald-rocksdb-0.8.0 (c (n "emerald-rocksdb") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.6.2") (d #t) (k 0)))) (h "0jgkq10wrp0f86373z723mdjq7wf2jmz34qqznb2vwdnhymz1i5g") (f (quote (("valgrind") ("default")))) (y #t)))

(define-public crate-emerald-rocksdb-0.9.0 (c (n "emerald-rocksdb") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.6.2") (d #t) (k 0)))) (h "0zaqfgimx53w7vyzimn4i7lr9nxmb14vzgvlgm4c7gpyqi72rwyn") (f (quote (("valgrind") ("default")))) (y #t)))

(define-public crate-emerald-rocksdb-0.10.0 (c (n "emerald-rocksdb") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-emerald-sys") (r "^1.0.0") (d #t) (k 0)))) (h "113vdnv2drxr6cxn9fp6zzpp7a3yc4hh266ix68w6bcd4mm92jhz") (f (quote (("valgrind") ("default")))) (y #t)))

(define-public crate-emerald-rocksdb-0.8.2 (c (n "emerald-rocksdb") (v "0.8.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.7.1") (d #t) (k 0)))) (h "13y428bbap9i7pzmn77sbq65akk8bajgxyydc75m7k076gwpxf1g") (f (quote (("valgrind") ("default")))) (y #t)))

(define-public crate-emerald-rocksdb-1.8.2 (c (n "emerald-rocksdb") (v "1.8.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.7.1") (d #t) (k 0)))) (h "0rc9nn0821dw0pn812pwcjpizqbgz4pmh7iylp5axq4m931d0jyl") (f (quote (("valgrind") ("default"))))))

(define-public crate-emerald-rocksdb-2.8.2 (c (n "emerald-rocksdb") (v "2.8.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.7.1") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)))) (h "16sp8z030wcia6771arpf7z6vqxssq9yzrpc2m82kl12syb0sxnf") (f (quote (("valgrind") ("default"))))))

(define-public crate-emerald-rocksdb-100.0.0 (c (n "emerald-rocksdb") (v "100.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.7.1") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)))) (h "0ilhzyhj6d60s1pccqb5cihp370nhbyz0ds71niwhj9rv535vws2") (f (quote (("valgrind") ("default"))))))

(define-public crate-emerald-rocksdb-0.10.2 (c (n "emerald-rocksdb") (v "0.10.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.11.3") (d #t) (k 0)))) (h "0z5kyr41vaj3gj7z9la4ymp5kmnzqpm7g1wqh9iska4xc3zxap52") (f (quote (("valgrind") ("default"))))))

(define-public crate-emerald-rocksdb-0.10.3 (c (n "emerald-rocksdb") (v "0.10.3") (d (list (d (n "emerald-librocksdb-sys") (r "^5.11.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fzsrhc2lqlvxp7wdlzwymhx3l3lmmh391y38wqv9pb9yrj2a689") (f (quote (("valgrind") ("default"))))))

