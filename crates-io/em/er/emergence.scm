(define-module (crates-io em er emergence) #:use-module (crates-io))

(define-public crate-emergence-0.1.0 (c (n "emergence") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1gkgrdlxgsq5xyy6gzva9w31qb3p5lkrr7acyn0xizd7bjdqb91h")))

(define-public crate-emergence-0.2.0 (c (n "emergence") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking" "rustls"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0n9ggzqbs141hcypaj49jnhpf72dyljhpyh9gfvq8j9f6amkmkhp")))

(define-public crate-emergence-0.3.0 (c (n "emergence") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking" "rustls"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0fwhsavjnbzxpddm8jhwzshvsrcx567xcffrrj3jrwm2lxws0bh6")))

(define-public crate-emergence-1.0.0 (c (n "emergence") (v "1.0.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "19r4ix5nvak1v9gf0h2gq9yzlb003ymwsxspy5dawjkv1h6wkc4f")))

(define-public crate-emergence-1.0.1 (c (n "emergence") (v "1.0.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "09cj9j5baras3a6syk59i65j6msxshpwagiq4rv7z3vmly64hy9f")))

(define-public crate-emergence-1.0.2 (c (n "emergence") (v "1.0.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0ipp677w2gf51s4k504x24dh8jlhrxfwa3s41h3i7kwdvp0h2mbg")))

(define-public crate-emergence-1.0.3 (c (n "emergence") (v "1.0.3") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1sqvy88dv5cqxn7yjxh2ddbmx548czjlqqiis3k3w18x0w67a2vh")))

(define-public crate-emergence-1.0.4 (c (n "emergence") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1i4dkg5pv48fcwm417x46vby2pn9k04lydpg24inf5in80fx3dfc")))

(define-public crate-emergence-1.1.0 (c (n "emergence") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1gh1kcx9w823pzr77ayg5jyfkdxqw05jkvd6qy70hb4skdw9ncb3")))

(define-public crate-emergence-2.0.0 (c (n "emergence") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "015nm7z94grxlys83nk52g9kswjgw5j4yxyal834pa5by850sr6m")))

