(define-module (crates-io em er emergency_brake) #:use-module (crates-io))

(define-public crate-emergency_brake-0.1.0 (c (n "emergency_brake") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1c5mh58v6m5xnbm3s7y6602lpm9g6jzr2pdxcwqmjds3ni8hcfr8") (s 2) (e (quote (("service_checker" "dep:async-trait" "dep:reqwest" "dep:tokio"))))))

