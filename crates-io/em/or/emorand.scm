(define-module (crates-io em or emorand) #:use-module (crates-io))

(define-public crate-emorand-1.0.0 (c (n "emorand") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.1") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0i1m8azq75rlpvrvfdxmbysz55x1n9wczcqkjs0fyif62i1f984s")))

(define-public crate-emorand-1.0.1 (c (n "emorand") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1fl09icfnxly3dyypi0vlqcfvxfpmcjmbl5zmainr1bjm9xvxcpn")))

