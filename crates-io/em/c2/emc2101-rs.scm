(define-module (crates-io em c2 emc2101-rs) #:use-module (crates-io))

(define-public crate-emc2101-rs-0.1.0 (c (n "emc2101-rs") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.2") (f (quote ("unstable-test"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "fugit") (r "^0.3.6") (d #t) (k 0)))) (h "0lsk01ff9f40wyw3yfhnvikdc8x8ipii1yn73hr2353yi0ghcc2s") (s 2) (e (quote (("defmt" "dep:defmt" "fugit/defmt"))))))

