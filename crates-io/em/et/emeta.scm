(define-module (crates-io em et emeta) #:use-module (crates-io))

(define-public crate-emeta-0.1.0 (c (n "emeta") (v "0.1.0") (d (list (d (n "miniserde") (r "^0.1.24") (d #t) (k 0)) (d (n "speedy") (r "^0.8.2") (d #t) (k 0)))) (h "1a5yd7m8g4wr9j5nkgrvakk3prhxzfgvxk0wwi0qhgzbz79vhrmf")))

(define-public crate-emeta-0.1.1 (c (n "emeta") (v "0.1.1") (d (list (d (n "miniserde") (r "^0.1.24") (d #t) (k 0)) (d (n "speedy") (r "^0.8.2") (d #t) (k 0)))) (h "0s91xvxp9pnbak8sbrr6g87d7p70znpd0jaf17wh8aqhz55chnib")))

