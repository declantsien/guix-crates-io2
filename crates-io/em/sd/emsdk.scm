(define-module (crates-io em sd emsdk) #:use-module (crates-io))

(define-public crate-emsdk-0.1.1 (c (n "emsdk") (v "0.1.1") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)))) (h "1adqvhq4il5n3z21kwawlrpk3bbyr8awdv77h1spvv0lv2nj9v51") (r "1.60.0")))

