(define-module (crates-io em py empy) #:use-module (crates-io))

(define-public crate-empy-1.0.0-alpha (c (n "empy") (v "1.0.0-alpha") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chlorine") (r "^1.0") (d #t) (k 0)))) (h "0f3ksz7macafn1hbr85fjid55hbr8wgz9xfbgvy7fdjv6jf46dlv") (f (quote (("std") ("simd") ("nightly-docs") ("mp1-mp2") ("default" "simd"))))))

(define-public crate-empy-1.0.0-alpha2 (c (n "empy") (v "1.0.0-alpha2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chlorine") (r "^1.0") (d #t) (k 0)))) (h "032dsj0mxcc8bm6v8hm1xzzgp8hzaa97psi7iq00ziyn263fn3c1") (f (quote (("simd") ("mp1-mp2") ("default" "simd"))))))

