(define-module (crates-io em it emit-crash) #:use-module (crates-io))

(define-public crate-emit-crash-0.1.0 (c (n "emit-crash") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0b88rlqa1mb48pyx0svlmn8gr4xj8pr9syv52x48smwrs7rl2p52")))

