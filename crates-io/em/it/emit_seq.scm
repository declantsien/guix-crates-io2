(define-module (crates-io em it emit_seq) #:use-module (crates-io))

(define-public crate-emit_seq-0.1.0 (c (n "emit_seq") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "emit") (r "^0.9.2") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "19qw3zry5y4l8jx0ffzz91cgqir907x8fp76fz6z7zl44ag25nvj")))

(define-public crate-emit_seq-0.2.0 (c (n "emit_seq") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "emit") (r "^0.10") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "008h4nrlgar6f6ychk86hxps0618wpafcqa129k322wjq5qb5nv0")))

(define-public crate-emit_seq-0.3.0 (c (n "emit_seq") (v "0.3.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "emit") (r "^0.10") (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "0xrfmjqwzaq8vkbvz917djlyrh5h47q8y0lbik7v2gq4dgivyx4k")))

