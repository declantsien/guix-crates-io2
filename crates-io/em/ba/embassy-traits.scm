(define-module (crates-io em ba embassy-traits) #:use-module (crates-io))

(define-public crate-embassy-traits-0.0.0 (c (n "embassy-traits") (v "0.0.0") (d (list (d (n "defmt") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0awh7fyz0j6599f3mfby3vbv09gl8mj7jl38simjmx8c7ff86fzj") (f (quote (("std"))))))

(define-public crate-embassy-traits-0.0.1 (c (n "embassy-traits") (v "0.0.1") (d (list (d (n "defmt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0py03593sbqh7j090hm980yqrj05w74h96rwapsi91hsy77xkdhp") (f (quote (("std"))))))

(define-public crate-embassy-traits-0.0.2 (c (n "embassy-traits") (v "0.0.2") (d (list (d (n "defmt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0vbp5c9xjwfy6854hiza6r1s8wlc7s4fmxh6vb1x7aqwbwcjh72h") (f (quote (("std"))))))

