(define-module (crates-io em ba embassy-nrf91) #:use-module (crates-io))

(define-public crate-embassy-nrf91-0.0.0 (c (n "embassy-nrf91") (v "0.0.0") (h "1aqc246qhsb96ksf7cbzddc9snmzv457wddrhgzzdk1v3b0qms69")))

(define-public crate-embassy-nrf91-0.1.0 (c (n "embassy-nrf91") (v "0.1.0") (h "04az55pcs6j1s23dsvajv5naxx1bkfkkvqh8nmnbpf0mnr7fgjr1")))

