(define-module (crates-io em ba embassy-executor-macros) #:use-module (crates-io))

(define-public crate-embassy-executor-macros-0.4.0 (c (n "embassy-executor-macros") (v "0.4.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kzp462dzyg7kls2d2wn8lh41iql4v2qab729fs7x0jjp8jzgc0a") (f (quote (("nightly"))))))

(define-public crate-embassy-executor-macros-0.4.1 (c (n "embassy-executor-macros") (v "0.4.1") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1darihr7r283hyv1g4qznf365rd05c9r9saigb7yjl00z364lidd") (f (quote (("nightly"))))))

