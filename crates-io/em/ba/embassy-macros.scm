(define-module (crates-io em ba embassy-macros) #:use-module (crates-io))

(define-public crate-embassy-macros-0.0.0 (c (n "embassy-macros") (v "0.0.0") (h "0w63dfnycil7r9r12gn1zpbsw0yqyfynmip6sparpbgzrrdlz3xl")))

(define-public crate-embassy-macros-0.1.0 (c (n "embassy-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rwfyrdjixsjsmv3byrj3nngkff599h2c25fnimi43pmdawpzxf8") (f (quote (("rtos-trace-interrupt"))))))

(define-public crate-embassy-macros-0.2.0 (c (n "embassy-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zz0j6av1km81g2v2bsnb0b6i4rlx67dy3wc43gzqw667kiybwmc") (f (quote (("rtos-trace-interrupt"))))))

(define-public crate-embassy-macros-0.2.1 (c (n "embassy-macros") (v "0.2.1") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1949hzyffyr9vkvxx4gbqr8qmhmif9zkc7k0y5ym3z5d67sfxlwl") (f (quote (("rtos-trace-interrupt"))))))

