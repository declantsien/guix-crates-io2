(define-module (crates-io em ba embassy-net-wiznet) #:use-module (crates-io))

(define-public crate-embassy-net-wiznet-0.0.0 (c (n "embassy-net-wiznet") (v "0.0.0") (h "0may472zs60ahfn8gijxya6xkpmdj481gjcc9sszvbklk0hk285v")))

(define-public crate-embassy-net-wiznet-0.1.0 (c (n "embassy-net-wiznet") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embassy-futures") (r "^0.1.0") (d #t) (k 0)) (d (n "embassy-net-driver-channel") (r "^0.2.0") (d #t) (k 0)) (d (n "embassy-time") (r "^0.3.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (d #t) (k 0)))) (h "1i3l88xgwyy87gw4mww997n5j00sqigdyxfc1k2kj63qrfa6hk8i")))

