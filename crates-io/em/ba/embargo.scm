(define-module (crates-io em ba embargo) #:use-module (crates-io))

(define-public crate-embargo-0.0.1 (c (n "embargo") (v "0.0.1") (d (list (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1pxv7a0a6sbi5pqca2b1qp4jz6zgs5800s8ja8v0j48kv9k6rcpc")))

(define-public crate-embargo-0.1.0 (c (n "embargo") (v "0.1.0") (d (list (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1zsb3zh93nplgrsyrk7hn0fzk4wy9p2m1rcllw62gq29sn56i5b6")))

(define-public crate-embargo-0.1.1 (c (n "embargo") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0p88683ghjjgb3ailja3gi44q8ls40vd1jc1hmzk841rh14y3354")))

