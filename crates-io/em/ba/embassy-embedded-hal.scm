(define-module (crates-io em ba embassy-embedded-hal) #:use-module (crates-io))

(define-public crate-embassy-embedded-hal-0.0.0 (c (n "embassy-embedded-hal") (v "0.0.0") (h "00cp9b32wl9lzkvjdybwf4h5dg44ijyak0i8bspin8jzkgh177ih")))

(define-public crate-embassy-embedded-hal-0.1.0 (c (n "embassy-embedded-hal") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1.1") (f (quote ("std"))) (d #t) (k 2)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embassy-futures") (r "^0.1.0") (d #t) (k 0)) (d (n "embassy-sync") (r "^0.5.0") (d #t) (k 0)) (d (n "embassy-time") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal-02") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "^1.0") (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-async") (r "^1.0") (d #t) (k 0)) (d (n "embedded-storage") (r "^0.3.1") (d #t) (k 0)) (d (n "embedded-storage-async") (r "^0.4.1") (d #t) (k 0)) (d (n "futures-test") (r "^0.3.17") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "07cswmwnnkw0fd16pi77xl4g3a9ggpv3k0kv0rii1rh31lwak97c") (f (quote (("std") ("default" "time")))) (s 2) (e (quote (("time" "dep:embassy-time"))))))

