(define-module (crates-io em ba embassy-boot-stm32) #:use-module (crates-io))

(define-public crate-embassy-boot-stm32-0.0.0 (c (n "embassy-boot-stm32") (v "0.0.0") (h "0zbc8a6a7fdp92cj83ravhibwmk6wc4871zgs0anb35bw3miiwpq")))

(define-public crate-embassy-boot-stm32-0.2.0 (c (n "embassy-boot-stm32") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "defmt-rtt") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "embassy-boot") (r "^0.2.0") (d #t) (k 0)) (d (n "embassy-stm32") (r "^0.1.0") (k 0)) (d (n "embassy-sync") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-storage") (r "^0.3.1") (d #t) (k 0)) (d (n "embedded-storage-async") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1zq4951yhllgd37gg8jk942h2kk4zqvk5a5aijl5qc3i045f3f37") (f (quote (("debug" "defmt-rtt")))) (s 2) (e (quote (("log" "dep:log" "embassy-boot/log" "embassy-stm32/log") ("defmt" "dep:defmt" "embassy-boot/defmt" "embassy-stm32/defmt"))))))

