(define-module (crates-io em ba embassy-net-driver-channel) #:use-module (crates-io))

(define-public crate-embassy-net-driver-channel-0.0.0 (c (n "embassy-net-driver-channel") (v "0.0.0") (h "091pn6gi3jw5f0zajnjhaspb1551ab9ff0mip83a9kckn69dga9w")))

(define-public crate-embassy-net-driver-channel-0.1.0 (c (n "embassy-net-driver-channel") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embassy-futures") (r "^0.1.0") (d #t) (k 0)) (d (n "embassy-net-driver") (r "^0.1.0") (d #t) (k 0)) (d (n "embassy-sync") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "1h5w5xyvzwd4ikf2nq96i713xyb4dpg1yd705isdmgii1sa96ydy")))

(define-public crate-embassy-net-driver-channel-0.2.0 (c (n "embassy-net-driver-channel") (v "0.2.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embassy-futures") (r "^0.1.0") (d #t) (k 0)) (d (n "embassy-net-driver") (r "^0.2.0") (d #t) (k 0)) (d (n "embassy-sync") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "0cwr5wrbzp9f6r4qvd7ai3vssjcm3lb7dvjmgnmfy4jngvdb8jjq")))

