(define-module (crates-io em ba embassy-futures) #:use-module (crates-io))

(define-public crate-embassy-futures-0.0.0 (c (n "embassy-futures") (v "0.0.0") (h "06zygw849kd3krp3lbv0ic9g8z4wd7jw3ryshgik8lhjaswxvkv2")))

(define-public crate-embassy-futures-0.1.0 (c (n "embassy-futures") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "1hsf6wq9rkpl4a3dv8xn79c03c4kyh94hy0g6qww0iykcmqkdra7")))

(define-public crate-embassy-futures-0.1.1 (c (n "embassy-futures") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "0ryhyycgi28k0p3i1srfsdk67ai6ixr5pjc8qx51wk3rp5sq11qz")))

