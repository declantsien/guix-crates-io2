(define-module (crates-io em ba embassy-usb-synopsys-otg) #:use-module (crates-io))

(define-public crate-embassy-usb-synopsys-otg-0.1.0 (c (n "embassy-usb-synopsys-otg") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embassy-sync") (r "^0.5.0") (d #t) (k 0)) (d (n "embassy-usb-driver") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "0ji5mibj2758djc0b6wsj3s056xjiad76kgp7xi9xwxwf8pfjsyl")))

