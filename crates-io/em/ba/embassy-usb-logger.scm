(define-module (crates-io em ba embassy-usb-logger) #:use-module (crates-io))

(define-public crate-embassy-usb-logger-0.0.0 (c (n "embassy-usb-logger") (v "0.0.0") (h "0hcf9c4vcs762bbwsg9zwa028mx4g8v001g5x2v894yb0v4d57lx")))

(define-public crate-embassy-usb-logger-0.1.0 (c (n "embassy-usb-logger") (v "0.1.0") (d (list (d (n "embassy-futures") (r "^0.1.0") (d #t) (k 0)) (d (n "embassy-sync") (r "^0.5.0") (d #t) (k 0)) (d (n "embassy-usb") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0b61qdiab90ldypkdsmv2xyl76jn67igm5xdjhrxm5pjpxb7chc0")))

(define-public crate-embassy-usb-logger-0.2.0 (c (n "embassy-usb-logger") (v "0.2.0") (d (list (d (n "embassy-futures") (r "^0.1.0") (d #t) (k 0)) (d (n "embassy-sync") (r "^0.5.0") (d #t) (k 0)) (d (n "embassy-usb") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1xs2lbfcgcdr7ds9mz1i94759588wr9bals7wkr30wmzba8xzvzz")))

