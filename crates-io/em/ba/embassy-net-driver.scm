(define-module (crates-io em ba embassy-net-driver) #:use-module (crates-io))

(define-public crate-embassy-net-driver-0.0.0 (c (n "embassy-net-driver") (v "0.0.0") (h "1pwlx6083173pc7vqzclnd7hs34rwmgkl5s7zf8n3a8x744hdiqg")))

(define-public crate-embassy-net-driver-0.1.0 (c (n "embassy-net-driver") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0vlp59341sj0pcygv2g8qq98rxblgpfvy0wps1awpd6sln2ljsjc")))

(define-public crate-embassy-net-driver-0.2.0 (c (n "embassy-net-driver") (v "0.2.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "07bm2yr5l3zx1rgzqj7ywrrk3rgnf352n4b02gvhh1bni72b6kjj")))

