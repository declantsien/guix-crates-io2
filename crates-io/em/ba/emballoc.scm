(define-module (crates-io em ba emballoc) #:use-module (crates-io))

(define-public crate-emballoc-0.1.0 (c (n "emballoc") (v "0.1.0") (d (list (d (n "spin") (r "^0.9.4") (f (quote ("mutex" "spin_mutex"))) (k 0)))) (h "0fckawjiqr2qc7vyxsscj8la921m97hgjzmmx2askm4404sx5n71") (r "1.62")))

(define-public crate-emballoc-0.1.1 (c (n "emballoc") (v "0.1.1") (d (list (d (n "spin") (r "^0.9.4") (f (quote ("mutex" "spin_mutex"))) (k 0)))) (h "1ng5j2dbjhqhlksm1gpa4gbq5k0b22ad1gxllr9w7l1ykw58hsi9") (r "1.62")))

(define-public crate-emballoc-0.1.2 (c (n "emballoc") (v "0.1.2") (d (list (d (n "spin") (r "^0.9.4") (f (quote ("mutex" "spin_mutex"))) (k 0)))) (h "13c2qsxlhbr9nzam3nf1kafb2nfp6nvj18z39m7inkjmw8bxkvnl") (r "1.62")))

(define-public crate-emballoc-0.1.3 (c (n "emballoc") (v "0.1.3") (d (list (d (n "spin") (r "^0.9.8") (f (quote ("mutex" "spin_mutex"))) (k 0)))) (h "0j4plcawc6gr9wrlan87znvkyaig96y7k2gknb9lwhl4103954rk") (r "1.57")))

(define-public crate-emballoc-0.2.0 (c (n "emballoc") (v "0.2.0") (d (list (d (n "spin") (r "^0.9.8") (f (quote ("mutex" "spin_mutex"))) (k 0)))) (h "1wp98m0dzk56yjsj4yws16cg5xn4p4d1dq38lf8wh0x0244d67rs") (f (quote (("portable_atomic" "spin/portable_atomic")))) (r "1.57")))

