(define-module (crates-io em ba embassy-net-enc28j60) #:use-module (crates-io))

(define-public crate-embassy-net-enc28j60-0.0.0 (c (n "embassy-net-enc28j60") (v "0.0.0") (h "0gz9ifparkkrpwhl9wm542hg38s12mcwsipnn1xa869k2fl60aj0")))

(define-public crate-embassy-net-enc28j60-0.1.0 (c (n "embassy-net-enc28j60") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embassy-futures") (r "^0.1.0") (d #t) (k 0)) (d (n "embassy-net-driver") (r "^0.2.0") (d #t) (k 0)) (d (n "embassy-time") (r "^0.3.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "13a7f3zbjg265x16lv1hdx8bkm6ysxg54xcyp1rcglv712vb3sc6")))

