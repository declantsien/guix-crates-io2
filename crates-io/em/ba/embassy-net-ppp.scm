(define-module (crates-io em ba embassy-net-ppp) #:use-module (crates-io))

(define-public crate-embassy-net-ppp-0.0.0 (c (n "embassy-net-ppp") (v "0.0.0") (h "10bgzbmrwb3h75h02jsayl4hn3haggggj41dmf5kzgy48spab8lj")))

(define-public crate-embassy-net-ppp-0.1.0 (c (n "embassy-net-ppp") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embassy-futures") (r "^0.1.0") (d #t) (k 0)) (d (n "embassy-net-driver-channel") (r "^0.2.0") (d #t) (k 0)) (d (n "embassy-sync") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-io-async") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "ppproto") (r "^0.1.2") (d #t) (k 0)))) (h "1gzl0ib6knb1kgchj4brs5q0ydb01k34b7cdj7rdpg52rg4gird0") (s 2) (e (quote (("log" "dep:log" "ppproto/log") ("defmt" "dep:defmt" "ppproto/defmt"))))))

