(define-module (crates-io em ba embassy-boot-rp) #:use-module (crates-io))

(define-public crate-embassy-boot-rp-0.0.0 (c (n "embassy-boot-rp") (v "0.0.0") (h "1hlpgkbacrg8jn43aphk7swr35xwvpydp27k1wxy5zns0wrqf0zv")))

(define-public crate-embassy-boot-rp-0.2.0 (c (n "embassy-boot-rp") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "defmt-rtt") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "embassy-boot") (r "^0.2.0") (d #t) (k 0)) (d (n "embassy-rp") (r "^0.1.0") (k 0)) (d (n "embassy-sync") (r "^0.5.0") (d #t) (k 0)) (d (n "embassy-time") (r "^0.3.0") (d #t) (k 0)) (d (n "embedded-storage") (r "^0.3.1") (d #t) (k 0)) (d (n "embedded-storage-async") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1hpjq6g7kpmgka7piv6v2lvwa4qaxiq66rll4wsxs0d1qamm72ld") (f (quote (("debug" "defmt-rtt")))) (s 2) (e (quote (("log" "dep:log" "embassy-boot/log" "embassy-rp/log") ("defmt" "dep:defmt" "embassy-boot/defmt" "embassy-rp/defmt"))))))

