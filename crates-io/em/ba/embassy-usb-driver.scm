(define-module (crates-io em ba embassy-usb-driver) #:use-module (crates-io))

(define-public crate-embassy-usb-driver-0.0.0 (c (n "embassy-usb-driver") (v "0.0.0") (h "00xqrb0mg3p08inw2arif1kch80pqmc0srxh8fh5csbiwchra82z")))

(define-public crate-embassy-usb-driver-0.1.0 (c (n "embassy-usb-driver") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0w0r4dyk4clggblgam26xymnlczdn4sll41mcr5if15fiq14ghjg")))

