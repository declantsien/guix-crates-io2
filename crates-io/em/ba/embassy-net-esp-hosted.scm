(define-module (crates-io em ba embassy-net-esp-hosted) #:use-module (crates-io))

(define-public crate-embassy-net-esp-hosted-0.0.0 (c (n "embassy-net-esp-hosted") (v "0.0.0") (h "1qkgdmxhhrhxh3r7a17mr09zs44wg8n275xkav7f49zz6biw72km")))

(define-public crate-embassy-net-esp-hosted-0.1.0 (c (n "embassy-net-esp-hosted") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embassy-futures") (r "^0.1.0") (d #t) (k 0)) (d (n "embassy-net-driver-channel") (r "^0.2.0") (d #t) (k 0)) (d (n "embassy-sync") (r "^0.5.0") (d #t) (k 0)) (d (n "embassy-time") (r "^0.3.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (d #t) (k 0)) (d (n "heapless") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "noproto") (r "^0.1.0") (d #t) (k 0)))) (h "1j896rg85sjpwy7qc1z7brbnf817mx6ndxd1h96y4j4a3m40lbnk")))

