(define-module (crates-io em ba embargoed) #:use-module (crates-io))

(define-public crate-embargoed-0.1.0 (c (n "embargoed") (v "0.1.0") (d (list (d (n "maxminddb") (r "^0.21.0") (f (quote ("mmap"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "0fq26a7x0db3900mbrzffbp61bba5fminj953rnl6pq909h1kl5k")))

(define-public crate-embargoed-0.1.1 (c (n "embargoed") (v "0.1.1") (d (list (d (n "maxminddb") (r "^0.21.0") (f (quote ("mmap"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "0z3hx8rirsiknisp84jw5h5gklpnj3qrsjmzpsk8hdal6gywpff7")))

