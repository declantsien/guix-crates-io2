(define-module (crates-io em li emlib) #:use-module (crates-io))

(define-public crate-emlib-0.0.1 (c (n "emlib") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cortex-m-rt") (r "^0.3.12") (f (quote ("abort-on-panic"))) (d #t) (k 0)))) (h "1yg59avxhmyaabkm3b8fdk2zz61hcjy7n8fh5k1sfhy8gj6xmf6b")))

