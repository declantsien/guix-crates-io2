(define-module (crates-io em oc emocli) #:use-module (crates-io))

(define-public crate-emocli-0.8.0-alpha.0 (c (n "emocli") (v "0.8.0-alpha.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1k5hwbfw5k153b14kdlj21g5ibk3yph8lhyahnnwdj1cd34wh1lx") (y #t)))

(define-public crate-emocli-0.8.0-alpha.1 (c (n "emocli") (v "0.8.0-alpha.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0q38lrc9sx88hhbr4jz5j0l9ac6wmb25fwn76md2r5k9k5ca0iz0") (y #t)))

(define-public crate-emocli-0.8.0-alpha.2 (c (n "emocli") (v "0.8.0-alpha.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0rw1f6mkbj2myag2bjjbyi3cgig1apzhcr34zhngpmcjcvq7vqmi") (y #t)))

(define-public crate-emocli-0.8.0-alpha.3 (c (n "emocli") (v "0.8.0-alpha.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1jp7h69nvm6pd6i10g39v1dfw38kfmf0bwq31rblnqsp381c46yj") (y #t)))

(define-public crate-emocli-0.8.0 (c (n "emocli") (v "0.8.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "192pwian17bfxn2ildl9nch23kswqh3z6drk1gi1j5kmgck0bp4n") (y #t)))

(define-public crate-emocli-0.8.1 (c (n "emocli") (v "0.8.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 1)))) (h "02qmnank4hhxq0saggaafk5yd0z137l5p1ds9ifz6npibcpcly4l") (y #t)))

(define-public crate-emocli-0.8.2-alpha.1 (c (n "emocli") (v "0.8.2-alpha.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 1)))) (h "05v7ch3m5cbmaw8wqd31vbjc9h2r2gr7axwg5xapnfvr47ggpqmv") (y #t)))

(define-public crate-emocli-0.8.3 (c (n "emocli") (v "0.8.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 1)))) (h "1qan62yk10y8q4jqa5v2l03k5civmziphm9sj8g324fapw1cpihd") (y #t)))

(define-public crate-emocli-0.8.4 (c (n "emocli") (v "0.8.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 1)))) (h "0v11jj5rg2gb37xxjx4l4w33lahm5gcqd3hr3c1i3sclfk1hd022") (y #t)))

(define-public crate-emocli-0.8.5 (c (n "emocli") (v "0.8.5") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 1)))) (h "1qd5aqm8incx1apipjziskal38nrcd9drdf4misb9ipak1vj3g2k") (y #t)))

(define-public crate-emocli-0.8.6 (c (n "emocli") (v "0.8.6") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 1)))) (h "1m38dydsg0b81bzwzm2zqiwnlklvrbfs2i52r7c3rh2dyx3jv4bq") (y #t)))

(define-public crate-emocli-0.9.0 (c (n "emocli") (v "0.9.0") (h "06slpq40s4ilwk19h2vzhwxw9gzhq23anszlwia5zfkhcc4hd7md")))

