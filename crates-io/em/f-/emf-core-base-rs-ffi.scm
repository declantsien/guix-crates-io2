(define-module (crates-io em f- emf-core-base-rs-ffi) #:use-module (crates-io))

(define-public crate-emf-core-base-rs-ffi-0.1.0 (c (n "emf-core-base-rs-ffi") (v "0.1.0") (h "1aiixhw5gzy8sjsfxgl5ibkyn7m8birv502p8yxsx9wd67j29zzm") (f (quote (("unwind_internal" "extensions") ("init") ("extensions_all" "unwind_internal") ("extensions"))))))

(define-public crate-emf-core-base-rs-ffi-0.1.1 (c (n "emf-core-base-rs-ffi") (v "0.1.1") (h "138pkr2sad7c02mwqn32mhl7s4yz8w4090kyvdjxy7kcgbazwrpw") (f (quote (("unwind_internal" "extensions") ("init") ("extensions_all" "unwind_internal") ("extensions")))) (y #t)))

(define-public crate-emf-core-base-rs-ffi-0.1.2 (c (n "emf-core-base-rs-ffi") (v "0.1.2") (h "0xn8gqmbi0ps9c8cr9wrfyh6ihm5a21knzdz672h5h6ap6xc14zw") (f (quote (("unwind_internal" "extensions") ("init") ("extensions_all" "unwind_internal") ("extensions"))))))

