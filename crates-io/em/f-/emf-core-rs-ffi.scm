(define-module (crates-io em f- emf-core-rs-ffi) #:use-module (crates-io))

(define-public crate-emf-core-rs-ffi-0.1.0 (c (n "emf-core-rs-ffi") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "12kwq472zhy5h9zbsqnczylvjafykjhal50hwpnl887597hcajs0") (y #t)))

