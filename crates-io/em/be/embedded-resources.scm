(define-module (crates-io em be embedded-resources) #:use-module (crates-io))

(define-public crate-embedded-resources-0.1.0 (c (n "embedded-resources") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "19r6fxyjjivc28hx946669y4xqc47z6i60ddkr9xa45hkshnvf6b") (y #t)))

(define-public crate-embedded-resources-0.1.1 (c (n "embedded-resources") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1jvimfxsalm1n9vsnzzvxbn15gq2fx3nif7fgqh4ajjr9zf05nlv") (y #t)))

(define-public crate-embedded-resources-0.1.2 (c (n "embedded-resources") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1fj8pd1dh1is86v08qscy08kc5dhnd87hngzmgp22viv6lj746mv")))

(define-public crate-embedded-resources-0.1.3 (c (n "embedded-resources") (v "0.1.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0g8jm2fkh8b2mi9fy934bj2203gcrvb072v4cr2n9prl87vjqcdp")))

(define-public crate-embedded-resources-0.1.4 (c (n "embedded-resources") (v "0.1.4") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0ia4fdcnwvqfx0my8v3jhsngfhacdvbfbjhy1yvwhav9vj5mcyhy")))

(define-public crate-embedded-resources-0.1.5 (c (n "embedded-resources") (v "0.1.5") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1ihkf8yjpdf0xadijsb81y3lpjlfrlz6dg9hmhbbiln152f4cl7g")))

