(define-module (crates-io em be embed_dir) #:use-module (crates-io))

(define-public crate-embed_dir-0.1.0 (c (n "embed_dir") (v "0.1.0") (h "1l9djw2ini5dx75w28yg8sp28rx3mkbg9i7mi2yssyr57kga1yy7")))

(define-public crate-embed_dir-0.1.1 (c (n "embed_dir") (v "0.1.1") (h "1ffjyn62x6mh72kh13pfnkva03cc1vc49w20xmdpycq63icr6z5d")))

(define-public crate-embed_dir-0.2.0 (c (n "embed_dir") (v "0.2.0") (h "15j8czzx33jd87l3n0ja8fcm4v13822qwyzddy4xirdikkpajq62")))

