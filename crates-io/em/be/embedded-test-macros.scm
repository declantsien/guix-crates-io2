(define-module (crates-io em be embedded-test-macros) #:use-module (crates-io))

(define-public crate-embedded-test-macros-0.1.0 (c (n "embedded-test-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "07h920svz66drkw0n5in5gh2nbb4m0zclf186h21w6syan1p11gd")))

(define-public crate-embedded-test-macros-0.2.0 (c (n "embedded-test-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1j3zbny5jv6r0scz9wc8n7l4mf7ccavpv2sjdpcbwh781ffnwz73") (f (quote (("embassy"))))))

(define-public crate-embedded-test-macros-0.2.1 (c (n "embedded-test-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "157mrz141nsj87pbgvljgkmwp89yr2q5pzka8qpwp0k8rj0bn6ll") (f (quote (("embassy"))))))

(define-public crate-embedded-test-macros-0.2.2 (c (n "embedded-test-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "050fjs3c7ikw42xyhrwqk3q76kz89igs547va0i2jlzhhma29qh9") (f (quote (("embassy"))))))

(define-public crate-embedded-test-macros-0.3.0 (c (n "embedded-test-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1y765slc1nwgkf15mwfvc5dmg6wmb2r81rxjl318qjhiph4vd1rx") (f (quote (("embassy"))))))

(define-public crate-embedded-test-macros-0.4.0 (c (n "embedded-test-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0wifq044kbgx4vra5whkw1byvgzjjm3ga54zx82430wkbi8ylylg") (f (quote (("embassy"))))))

