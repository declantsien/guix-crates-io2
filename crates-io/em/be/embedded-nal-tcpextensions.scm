(define-module (crates-io em be embedded-nal-tcpextensions) #:use-module (crates-io))

(define-public crate-embedded-nal-tcpextensions-0.1.0 (c (n "embedded-nal-tcpextensions") (v "0.1.0") (d (list (d (n "embedded-nal") (r "^0.6") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)))) (h "13mnn6kdc35l184ldbp0j2ci0w4sb1yjw81qjs6m0wcvq4h1rfb3")))

(define-public crate-embedded-nal-tcpextensions-0.1.1 (c (n "embedded-nal-tcpextensions") (v "0.1.1") (d (list (d (n "embedded-nal") (r "^0.6") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)))) (h "1a4w4v610b4ym6f7w9rk2x6gf95lfki3r80w247sx19940mzdimw")))

(define-public crate-embedded-nal-tcpextensions-0.1.2 (c (n "embedded-nal-tcpextensions") (v "0.1.2") (d (list (d (n "embedded-nal") (r "^0.6") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)))) (h "0c32qg7jfgla6kdqgg9b8rgsisklbn461n48xr9zay23arpp8bhg")))

