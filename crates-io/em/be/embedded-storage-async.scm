(define-module (crates-io em be embedded-storage-async) #:use-module (crates-io))

(define-public crate-embedded-storage-async-0.3.0 (c (n "embedded-storage-async") (v "0.3.0") (d (list (d (n "embedded-storage") (r "^0.3.0") (d #t) (k 0)))) (h "0ycdrplrq3mjqglw99h37krha6rx3yqa4ymx2m1vpsa79vvlmw2z")))

(define-public crate-embedded-storage-async-0.4.0 (c (n "embedded-storage-async") (v "0.4.0") (d (list (d (n "embedded-storage") (r "^0.3.0") (d #t) (k 0)))) (h "0nmkjv1w26snd3asrlk9yllzvqlqph2p9aizhzg0q3b7jjl9fa85")))

(define-public crate-embedded-storage-async-0.4.1 (c (n "embedded-storage-async") (v "0.4.1") (d (list (d (n "embedded-storage") (r "^0.3.1") (d #t) (k 0)))) (h "1k0mrm6xc1mdxr0drxbivqpa1kr1brznb430mbqdbdr34dg7fqqp")))

