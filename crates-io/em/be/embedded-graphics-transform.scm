(define-module (crates-io em be embedded-graphics-transform) #:use-module (crates-io))

(define-public crate-embedded-graphics-transform-0.1.0 (c (n "embedded-graphics-transform") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.7") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.3.3") (d #t) (k 0)))) (h "1hj3lscb90cxj2glsg6fnvdk7vz46xy1fxqp1rj6l62qxwh5zm3v")))

