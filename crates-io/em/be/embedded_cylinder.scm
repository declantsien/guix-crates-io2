(define-module (crates-io em be embedded_cylinder) #:use-module (crates-io))

(define-public crate-embedded_cylinder-1.0.0 (c (n "embedded_cylinder") (v "1.0.0") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00m4l00755sycpdmkdmsv89zch7l9654yarswsjdz631j6v2x96m") (y #t)))

(define-public crate-embedded_cylinder-1.0.1 (c (n "embedded_cylinder") (v "1.0.1") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fszk0a6qcbaqm5km4pbv45k75gp1ii4783dqsjhn0hs5jl6hjxj") (y #t)))

(define-public crate-embedded_cylinder-1.0.2 (c (n "embedded_cylinder") (v "1.0.2") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rm01n1fqk86w3vrinrba7jvfhqzv3wswqs3gsb9lcnj05dmjvml") (y #t)))

(define-public crate-embedded_cylinder-1.0.3 (c (n "embedded_cylinder") (v "1.0.3") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pdgyha4rnpkqsb4m8k8xhns9yxf9s7mc9dj48ypwr0xgbcv0m9z") (y #t)))

(define-public crate-embedded_cylinder-1.0.4 (c (n "embedded_cylinder") (v "1.0.4") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1gicyiv8jwnwv6vfz5mqh7hp9d10kzbhlrzb87ha1slixbkn2ad3") (y #t)))

(define-public crate-embedded_cylinder-1.0.5 (c (n "embedded_cylinder") (v "1.0.5") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1dz27614jhyiwyxkrml2ycv716455b02fq3p8gqnxk87fdidi80z") (y #t)))

