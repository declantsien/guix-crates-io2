(define-module (crates-io em be embedded-multi-page-hmi) #:use-module (crates-io))

(define-public crate-embedded-multi-page-hmi-0.2.0 (c (n "embedded-multi-page-hmi") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 2)))) (h "1lab265yh2jgc2cw1gljb8d8xq4yxxiqyvi4lavf2hk6gj0xkpgi")))

(define-public crate-embedded-multi-page-hmi-0.3.0 (c (n "embedded-multi-page-hmi") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 2)))) (h "1pdgz7xbyfj2vr8cgb3f1jpk2lmk19y2ad2gcj5mqj5q8wpi6vff")))

(define-public crate-embedded-multi-page-hmi-0.3.1 (c (n "embedded-multi-page-hmi") (v "0.3.1") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "async-std") (r "^1.9") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "crossterm") (r "0.21.*") (f (quote ("event-stream"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-timer") (r "^3.0") (d #t) (k 2)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 2)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 2)))) (h "0rpi1s2ylwd7ky6nmyr0kknzhj3j3k5hjhpzlvyhqs8lr50mfqq0")))

