(define-module (crates-io em be embedded-storage) #:use-module (crates-io))

(define-public crate-embedded-storage-0.1.0 (c (n "embedded-storage") (v "0.1.0") (h "05ag3hlvjc8zyn7pwrj7fxar0hfjyc6bzwvglclwiqkc8mhc8yz5")))

(define-public crate-embedded-storage-0.2.0 (c (n "embedded-storage") (v "0.2.0") (h "1jl2zarwsrjbgixsjzgd2s94ansfg52f2a2nyg2yddi5kx7cwgbj")))

(define-public crate-embedded-storage-0.3.0 (c (n "embedded-storage") (v "0.3.0") (h "08y7102xn49m1swig3izw53gkksq635cxjvrwndbzswqvlpplv8m")))

(define-public crate-embedded-storage-0.3.1 (c (n "embedded-storage") (v "0.3.1") (h "0cn0q1mjgv3i67nllivk199dlxmrx662441d0vrn1f5yajcfl7d2")))

