(define-module (crates-io em be embedded_font_generator) #:use-module (crates-io))

(define-public crate-embedded_font_generator-0.1.0 (c (n "embedded_font_generator") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "xflags") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "zune-png") (r "^0.2.0") (d #t) (k 0)))) (h "1kv0s1q0ibam031i72p44dwc278cnza805k7mz13h76c592xq57f") (f (quote (("build_bin" "xflags"))))))

