(define-module (crates-io em be embedded_redis_client) #:use-module (crates-io))

(define-public crate-embedded_redis_client-0.1.0 (c (n "embedded_redis_client") (v "0.1.0") (d (list (d (n "async-std") (r "^1.8") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "redis") (r "^0.19") (f (quote ("connection-manager" "async-std-comp"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0z2j231v8s23bd74ncapkz3adjvlm70l9b4y1kcvi8mp3yz5yd2c")))

