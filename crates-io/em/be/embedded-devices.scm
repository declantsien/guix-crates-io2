(define-module (crates-io em be embedded-devices) #:use-module (crates-io))

(define-public crate-embedded-devices-0.0.1 (c (n "embedded-devices") (v "0.0.1") (d (list (d (n "bondrewd") (r "^0.1.14") (f (quote ("derive"))) (k 0)) (d (n "defmt") (r "^0.3.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-registers") (r "^0.9.1") (d #t) (k 0)))) (h "0gwhqll2qqf888l8q1a6n41g3ipp8sls082836pcp5hi9ry1bcsy") (f (quote (("std") ("default"))))))

