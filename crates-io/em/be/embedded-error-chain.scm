(define-module (crates-io em be embedded-error-chain) #:use-module (crates-io))

(define-public crate-embedded-error-chain-0.1.0 (c (n "embedded-error-chain") (v "0.1.0") (d (list (d (n "macros") (r "^0.1") (d #t) (k 0) (p "embedded-error-chain-macros")))) (h "0h385bvsr914r0zj3qdy7n77bqq5ja04jjh9i46rxcl7qpig3nz6") (f (quote (("std") ("panic-on-overflow") ("nightly") ("default" "panic-on-overflow")))) (y #t)))

(define-public crate-embedded-error-chain-0.1.1 (c (n "embedded-error-chain") (v "0.1.1") (d (list (d (n "embedded-error-chain-macros") (r "^0.1") (d #t) (k 0) (p "embedded-error-chain-macros")))) (h "1x4dmnm4816ds5pcrxsqm3bffcq8k7r5gpvmcmsrjhk4s6x0inhr") (f (quote (("std") ("panic-on-overflow") ("nightly") ("default" "panic-on-overflow"))))))

(define-public crate-embedded-error-chain-1.0.0 (c (n "embedded-error-chain") (v "1.0.0") (d (list (d (n "embedded-error-chain-macros") (r "^1.0") (d #t) (k 0) (p "embedded-error-chain-macros")))) (h "12sm16w09hz8gcbqddcqrn4757p9sjis3wgwvpgpjvz8w7q1p151") (f (quote (("std") ("panic-on-overflow") ("nightly") ("default" "panic-on-overflow"))))))

