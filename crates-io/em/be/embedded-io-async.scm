(define-module (crates-io em be embedded-io-async) #:use-module (crates-io))

(define-public crate-embedded-io-async-0.0.0 (c (n "embedded-io-async") (v "0.0.0") (h "10zbdhswaypa1034jl3lwkmam43650rvgi4rpcckmghx0iv4g2rq")))

(define-public crate-embedded-io-async-0.5.0 (c (n "embedded-io-async") (v "0.5.0") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "embedded-io") (r "^0.5") (d #t) (k 0)))) (h "0544gyzgmjrcpdsw87ril4j0hibanv9dqw0cbar619a9sx57b50k") (f (quote (("std" "alloc" "embedded-io/std") ("alloc" "embedded-io/alloc")))) (s 2) (e (quote (("defmt-03" "dep:defmt-03" "embedded-io/defmt-03"))))))

(define-public crate-embedded-io-async-0.6.0 (c (n "embedded-io-async") (v "0.6.0") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "embedded-io") (r "^0.6") (d #t) (k 0)))) (h "1cb62s14d5w9pdqgqsr5ckifymx6nvgq3p54qzbv525ldxym40yy") (f (quote (("std" "alloc" "embedded-io/std") ("alloc" "embedded-io/alloc")))) (s 2) (e (quote (("defmt-03" "dep:defmt-03" "embedded-io/defmt-03"))))))

(define-public crate-embedded-io-async-0.6.1 (c (n "embedded-io-async") (v "0.6.1") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "embedded-io") (r "^0.6") (d #t) (k 0)))) (h "0gxs1pawhwxgc8fifqynkiifg0nmc58yfnrrk71ahfh7sir9kw1z") (f (quote (("std" "alloc" "embedded-io/std") ("alloc" "embedded-io/alloc")))) (s 2) (e (quote (("defmt-03" "dep:defmt-03" "embedded-io/defmt-03"))))))

