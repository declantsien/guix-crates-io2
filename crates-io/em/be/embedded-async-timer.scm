(define-module (crates-io em be embedded-async-timer) #:use-module (crates-io))

(define-public crate-embedded-async-timer-0.1.0 (c (n "embedded-async-timer") (v "0.1.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "stm32f1xx-hal") (r "^0.5.3") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "stm32l4xx-hal") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "10drq00m8732vbzsh4lynr265n8dfagg1azznr9n09d4j887nlx9") (f (quote (("stm32l4x6" "stm32l4xx-hal/stm32l4x6" "cortex-m") ("stm32f103" "stm32f1xx-hal/stm32f103" "cortex-m") ("default" "stm32f103"))))))

