(define-module (crates-io em be embedded-threadsafe-rp2040) #:use-module (crates-io))

(define-public crate-embedded-threadsafe-rp2040-0.2.3 (c (n "embedded-threadsafe-rp2040") (v "0.2.3") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (d #t) (k 0)) (d (n "embedded-threadsafe") (r "^0.2.3") (d #t) (k 0)) (d (n "rp2040-hal") (r "^0.8.2") (d #t) (k 0)))) (h "17gbsvwhkgarl7q1njdg1mz1vl49rd714bnmbyks411wl4dlqg76") (f (quote (("default"))))))

