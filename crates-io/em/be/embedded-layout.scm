(define-module (crates-io em be embedded-layout) #:use-module (crates-io))

(define-public crate-embedded-layout-0.0.1 (c (n "embedded-layout") (v "0.0.1") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)))) (h "1vy5ily8ynka5wgpbi1nsrsy68rag543415rmvf9c32ggjdcwgq2")))

(define-public crate-embedded-layout-0.0.2 (c (n "embedded-layout") (v "0.0.2") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)))) (h "1wypbqrb3ic1mzvkgdmsr1jw8l1pjbbynyq6cj1hhs4dh17w6nl1")))

(define-public crate-embedded-layout-0.0.3 (c (n "embedded-layout") (v "0.0.3") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.2.0") (d #t) (k 2)))) (h "0n8wnajslyx4vrijcgvh6ds458mab1cvf6n13dv0fvyc9dwd0616")))

(define-public crate-embedded-layout-0.1.0 (c (n "embedded-layout") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.2.0") (d #t) (k 2)))) (h "0jyfj1dvbd7yyzsg9pd7z0vpg0cx7f8bnmrp8ax5ip26x3pjn6dj")))

(define-public crate-embedded-layout-0.2.0 (c (n "embedded-layout") (v "0.2.0") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0") (d #t) (k 2)) (d (n "embedded-layout-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1j0a3snifr9lbq6f5jd412q6rlc6a69sh26f3c96l04llrmw3nvs")))

(define-public crate-embedded-layout-0.3.0 (c (n "embedded-layout") (v "0.3.0") (d (list (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.5.0") (d #t) (k 2)) (d (n "embedded-layout-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1cz88pgfc33zx7h9q9h9l5rldynm3x69szb9z4bmg5jakada0w94") (r "1.61")))

(define-public crate-embedded-layout-0.3.1 (c (n "embedded-layout") (v "0.3.1") (d (list (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.5.0") (d #t) (k 2)) (d (n "embedded-layout-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1fw2ca2fxwabflgsad9miqrr7r15wryyz4mvbf1i344xmxawjg8x") (r "1.61")))

(define-public crate-embedded-layout-0.3.2 (c (n "embedded-layout") (v "0.3.2") (d (list (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.5.0") (d #t) (k 2)) (d (n "embedded-layout-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0s9ka77ifbs86j5jh974ql30p8w096dhh52gz2kpf4q2z6vjb1xh") (r "1.61")))

(define-public crate-embedded-layout-0.4.0 (c (n "embedded-layout") (v "0.4.0") (d (list (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.5.0") (d #t) (k 2)) (d (n "embedded-layout-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "rayon-core") (r "=1.11") (d #t) (k 2)))) (h "1klzc4wbg8sada1s58r3f6hm8x473zhqm69qk08c8ymy8iqj5dr6") (r "1.61")))

(define-public crate-embedded-layout-0.4.1 (c (n "embedded-layout") (v "0.4.1") (d (list (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.5.0") (d #t) (k 2)) (d (n "embedded-layout-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon-core") (r "=1.11") (d #t) (k 2)))) (h "1nmwikmsh6b6h9spqb1pilrd90mv2sfzrpzvwy4kh6fd7bcdcf5x") (r "1.61")))

