(define-module (crates-io em be embedded-hal) #:use-module (crates-io))

(define-public crate-embedded-hal-0.1.0 (c (n "embedded-hal") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.6.0") (d #t) (k 2)))) (h "0wpp8cvl2aayz9vxrbl55cay0ksbmqznbns919w3sia0sdxgpa5h") (f (quote (("unproven" "nb/unstable"))))))

(define-public crate-embedded-hal-0.1.1 (c (n "embedded-hal") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.6.0") (d #t) (k 2)))) (h "0jy53ic63vgdjx71lyvgp1c8s6m31i86xndv16lx5mh259xnzry6") (f (quote (("unproven" "nb/unstable"))))))

(define-public crate-embedded-hal-0.1.2 (c (n "embedded-hal") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.6.0") (d #t) (k 2)))) (h "0rz20vbi0a07rffq2jmns4lkf1axdqmdqknhjqi38y3g36iiyjv8") (f (quote (("unproven" "nb/unstable"))))))

(define-public crate-embedded-hal-0.2.0 (c (n "embedded-hal") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.6.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0lrc8m7wirclnks07hdrg5qhaaj230wgdf7hv0j9v10nxiz74568") (f (quote (("unproven" "nb/unstable"))))))

(define-public crate-embedded-hal-0.2.1 (c (n "embedded-hal") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.6.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0qh3ikpqwzifxv7hy86nmfzzdg6dsv01sl1583xvaklkwivld516") (f (quote (("unproven" "nb/unstable"))))))

(define-public crate-embedded-hal-0.1.3 (c (n "embedded-hal") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.6.0") (d #t) (k 2)))) (h "01lacsg56nci7w5k2h4wiy3qqv29870lb60bv8mhvccpm7jdajwv") (f (quote (("unproven" "nb/unstable" "embedded-hal/unproven"))))))

(define-public crate-embedded-hal-0.2.2 (c (n "embedded-hal") (v "0.2.2") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.6.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0cwc88kanaqprj9c6fycqfcmfjcxh5xarwc83pa180w3719fb04q") (f (quote (("unproven" "nb/unstable"))))))

(define-public crate-embedded-hal-0.2.3-rc.1 (c (n "embedded-hal") (v "0.2.3-rc.1") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.6.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1zcqfqdaiy1kw9kmg1fz2jvsagxsj215bcrfs5f2dj4axb5bibq4") (f (quote (("unproven" "nb/unstable"))))))

(define-public crate-embedded-hal-0.2.3 (c (n "embedded-hal") (v "0.2.3") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.6.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "02qpdjksa1njjgidkhzw9qxiw1i0p0bxcq1d7irafk89anhhhjgf") (f (quote (("unproven" "nb/unstable"))))))

(define-public crate-embedded-hal-1.0.0-alpha.1 (c (n "embedded-hal") (v "1.0.0-alpha.1") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "stm32f3") (r "^0.8") (f (quote ("stm32f303" "rt"))) (d #t) (k 2)))) (h "0mlga4sbwhilnyf9df98my7qvdkl4yxm14b415vzw18hjcg6cpwa")))

(define-public crate-embedded-hal-0.2.4 (c (n "embedded-hal") (v "0.2.4") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.8.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1jszhqfik8kbh6s57iqlzg7dsqc9llvsz4v344amsxn9kvjqr6gs") (f (quote (("unproven" "nb/unstable"))))))

(define-public crate-embedded-hal-1.0.0-alpha.2 (c (n "embedded-hal") (v "1.0.0-alpha.2") (d (list (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.12") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "1sib3bv0wrp83i15rgiy8s71vbi0lply8vk7fbc2l95g9l683hvw")))

(define-public crate-embedded-hal-1.0.0-alpha.3 (c (n "embedded-hal") (v "1.0.0-alpha.3") (d (list (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.12") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "09fpi33h6xngh17d6csmvw3sv18j24wmzfrd8rr4zb8ndl60sm4r")))

(define-public crate-embedded-hal-1.0.0-alpha.4 (c (n "embedded-hal") (v "1.0.0-alpha.4") (d (list (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.12") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "0fv9lk3cxwj9fsqglk715zqav6ks0iklzmd8y4fxljygxamhq9pj")))

(define-public crate-embedded-hal-0.2.5 (c (n "embedded-hal") (v "0.2.5") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.8.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1bbjs66lp5nb3n002i0sr05zwk8lch14qfah88sa5ivvl8zls66v") (f (quote (("unproven" "nb/unstable"))))))

(define-public crate-embedded-hal-0.2.6 (c (n "embedded-hal") (v "0.2.6") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.8.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1faa71mvs9zklyiiy9l5br9f2bwmxwak0br7jb49cr8mzxignv73") (f (quote (("unproven" "nb/unstable"))))))

(define-public crate-embedded-hal-1.0.0-alpha.5 (c (n "embedded-hal") (v "1.0.0-alpha.5") (d (list (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.13") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "1d3dapxpgn65h7f4px8ab8nyy6fpzk8xzk33jm4k0lk6913c0m55")))

(define-public crate-embedded-hal-1.0.0-alpha.6 (c (n "embedded-hal") (v "1.0.0-alpha.6") (d (list (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.14") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "0zjcc4dd66lbm5kad2mn43h78gx5052z2hhmzrnjb6iilxfpc3jp")))

(define-public crate-embedded-hal-0.2.7 (c (n "embedded-hal") (v "0.2.7") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "stm32f30x") (r "^0.8.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1zv6pkgg2yl0mzvh3jp326rhryqfnv4l27h78v7p7maag629i51m") (f (quote (("unproven" "nb/unstable"))))))

(define-public crate-embedded-hal-1.0.0-alpha.7 (c (n "embedded-hal") (v "1.0.0-alpha.7") (d (list (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.14") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "05i2gy51kvbk3205k70f5scv4q2kb6c49bsry8gslwxfvr773k4k")))

(define-public crate-embedded-hal-1.0.0-alpha.8 (c (n "embedded-hal") (v "1.0.0-alpha.8") (d (list (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.14") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "1381h2ys3aq39fq2y8mn0571y89gk69gbsqsn6h4489kzp3vzfn3")))

(define-public crate-embedded-hal-1.0.0-alpha.9 (c (n "embedded-hal") (v "1.0.0-alpha.9") (h "14aczbskjw9h3i4imwnfb8ga4nnr64d30yy0yxjhar7fvwfi16qj")))

(define-public crate-embedded-hal-1.0.0-alpha.10 (c (n "embedded-hal") (v "1.0.0-alpha.10") (h "0fz9wi3sk859xlnxzgnyl3npwx69lhc6h8cvc9pcd4ax7w3lsp7n")))

(define-public crate-embedded-hal-1.0.0-alpha.11 (c (n "embedded-hal") (v "1.0.0-alpha.11") (h "1wiz5ksgv0l1vc5qkwjjdrcfikccphkxf7ap41cv3zmdpjx4wwpp")))

(define-public crate-embedded-hal-1.0.0-rc.1 (c (n "embedded-hal") (v "1.0.0-rc.1") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")))) (h "08hfdhaakic6gx2i6h29f8v5hdwk9pbamf4adcywmf2p0hpvr518")))

(define-public crate-embedded-hal-1.0.0-rc.2 (c (n "embedded-hal") (v "1.0.0-rc.2") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")))) (h "1vrdnbv0b7rzd25hpb9dy3vg5s7mfx0i97wwrxkvk3mws1mfqmry") (s 2) (e (quote (("defmt-03" "dep:defmt-03")))) (r "1.56")))

(define-public crate-embedded-hal-1.0.0-rc.3 (c (n "embedded-hal") (v "1.0.0-rc.3") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")))) (h "084rq88qhbmv89f5j4lk6wzpnzpkbxxzkd25m4f768pxw5wjyh5w") (s 2) (e (quote (("defmt-03" "dep:defmt-03")))) (r "1.60")))

(define-public crate-embedded-hal-1.0.0 (c (n "embedded-hal") (v "1.0.0") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")))) (h "128bb4h3kw8gvz6w7xa0z0j6nrk5jhm3aa7v350clkh0nzz906in") (s 2) (e (quote (("defmt-03" "dep:defmt-03")))) (r "1.60")))

