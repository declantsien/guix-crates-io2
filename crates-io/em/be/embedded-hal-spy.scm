(define-module (crates-io em be embedded-hal-spy) #:use-module (crates-io))

(define-public crate-embedded-hal-spy-0.0.1 (c (n "embedded-hal-spy") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "156acylsp7i3yccv5vm8bixsd9x0sbx65y7b0kckgy61lck4dsi5")))

(define-public crate-embedded-hal-spy-0.0.2 (c (n "embedded-hal-spy") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1zw0as7cwyjqk54xx8zdzagwpmawi89w4g9qv6y4g7mfbpp0k88b")))

(define-public crate-embedded-hal-spy-0.0.3 (c (n "embedded-hal-spy") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1w8l08pc080ci734fs2n6yv700hp4cg7zfx2xpih5zi3vw2i70fm")))

(define-public crate-embedded-hal-spy-0.0.4 (c (n "embedded-hal-spy") (v "0.0.4") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1cxf7slk63pqc2nfrndv490w5krfg6jnnabrfnbmvksrqhswd213")))

(define-public crate-embedded-hal-spy-0.0.5 (c (n "embedded-hal-spy") (v "0.0.5") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1v17v1yi558bli923skw27ymbi2j4kzgmaczzbxi4ylyggwh788i") (f (quote (("embedded_hal_digital_io_legacy_v1"))))))

