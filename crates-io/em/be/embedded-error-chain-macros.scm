(define-module (crates-io em be embedded-error-chain-macros) #:use-module (crates-io))

(define-public crate-embedded-error-chain-macros-0.1.0 (c (n "embedded-error-chain-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0ypgrkp3r5k07wippc8zq4az18khxj8dy1jrjj84aghvn3z7gypm")))

(define-public crate-embedded-error-chain-macros-1.0.0 (c (n "embedded-error-chain-macros") (v "1.0.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "17v3d5lw49nkqs4rhv6lkm7f67a8w3h4x9r2gx4jv6bji1icwlfj")))

