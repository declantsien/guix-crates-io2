(define-module (crates-io em be embedded-storage-inmemory) #:use-module (crates-io))

(define-public crate-embedded-storage-inmemory-0.1.0 (c (n "embedded-storage-inmemory") (v "0.1.0") (d (list (d (n "embedded-storage") (r "^0.3") (d #t) (k 0)) (d (n "embedded-storage-async") (r "^0.4") (d #t) (k 0)))) (h "0qlkjbx55a7bwb95dwwpkz87ccddjcvzh7cm8590b6mn419v4vs8") (f (quote (("nightly"))))))

(define-public crate-embedded-storage-inmemory-0.1.1 (c (n "embedded-storage-inmemory") (v "0.1.1") (d (list (d (n "embedded-storage") (r "^0.3") (d #t) (k 0)) (d (n "embedded-storage-async") (r "^0.4") (d #t) (k 0)))) (h "1jc6d4i26407qmssc3zis36bcr2k1zd7bj8jx4406mma7y9n57ag") (f (quote (("nightly"))))))

