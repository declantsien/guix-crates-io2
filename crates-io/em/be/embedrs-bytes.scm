(define-module (crates-io em be embedrs-bytes) #:use-module (crates-io))

(define-public crate-embedrs-bytes-0.4.6 (c (n "embedrs-bytes") (v "0.4.6") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0mlb2wpmgkcgmbdkm556wk0acpbm6inay0hkfsdysgw6rbxcmikm") (f (quote (("std" "alloc" "iovec") ("nightly" "alloc") ("alloc"))))))

