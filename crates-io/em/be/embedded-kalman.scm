(define-module (crates-io em be embedded-kalman) #:use-module (crates-io))

(define-public crate-embedded-kalman-0.1.0 (c (n "embedded-kalman") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)))) (h "0i5g0904y6qg04v9w9y4b54s4rnyr4bigs9lgpm6j2zz0yb2lkri") (y #t)))

(define-public crate-embedded-kalman-0.2.0 (c (n "embedded-kalman") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)))) (h "1qrxxgp4iyzfwh55djipsih7gsqqcbvqripiisrx7lhwljha6f08") (y #t)))

