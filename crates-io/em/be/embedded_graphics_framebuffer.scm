(define-module (crates-io em be embedded_graphics_framebuffer) #:use-module (crates-io))

(define-public crate-embedded_graphics_framebuffer-0.1.0 (c (n "embedded_graphics_framebuffer") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "framebuffer") (r "^0.3.0") (d #t) (k 0)))) (h "0rh5gln5ajlllnz92jmd72ah998j0v3xm6jmsyh2zzc2ygsc8ply")))

(define-public crate-embedded_graphics_framebuffer-0.1.1 (c (n "embedded_graphics_framebuffer") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "framebuffer") (r "^0.3.0") (d #t) (k 0)))) (h "10fidd67cfn23abhwbr9pnn2s3m2dr9qdq9q6armga7a357czrys")))

