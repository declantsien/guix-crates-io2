(define-module (crates-io em be embedded-qmp6988) #:use-module (crates-io))

(define-public crate-embedded-qmp6988-0.1.0 (c (n "embedded-qmp6988") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4.0") (d #t) (k 2)) (d (n "micromath") (r "^2.1.0") (d #t) (k 0)))) (h "1cc0r0n8v43apfpkhgwlm18rzqawa1ch54fvwivaqy06hxxsbfy0")))

