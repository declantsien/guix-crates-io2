(define-module (crates-io em be embedded-freetype-sys) #:use-module (crates-io))

(define-public crate-embedded-freetype-sys-0.1.0 (c (n "embedded-freetype-sys") (v "0.1.0") (d (list (d (n "cty") (r "^0.1.5") (d #t) (k 0)))) (h "11s3ba7rvr7q47j1ip414wx374i9jijym847j8bq8b8wrc53qnvr")))

(define-public crate-embedded-freetype-sys-0.2.0 (c (n "embedded-freetype-sys") (v "0.2.0") (d (list (d (n "cty") (r "^0.2.0") (d #t) (k 0)))) (h "1sgv2pz4yhqp5717c8aaxmg2hi920y6lngjmqxjkiyrzjqczrg1k")))

