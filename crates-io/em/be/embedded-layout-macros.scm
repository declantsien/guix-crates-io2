(define-module (crates-io em be embedded-layout-macros) #:use-module (crates-io))

(define-public crate-embedded-layout-macros-0.1.0 (c (n "embedded-layout-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bf5y7f5l9j7zmpc5w1bblf908js578dzd229h8gjk2yx378c89m")))

(define-public crate-embedded-layout-macros-0.2.0 (c (n "embedded-layout-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1r5yxgirfr64kpr9s795ml5x0ypz7ia1vq6f6xal80s6znq193hy")))

(define-public crate-embedded-layout-macros-0.2.1 (c (n "embedded-layout-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "07v55vcmrv13w0j9lwql1py30fsj80qkl4lr8x3n7ni6qpdhl7j9")))

(define-public crate-embedded-layout-macros-0.3.0 (c (n "embedded-layout-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1iwr9ihvba6wvhfn3pj6hk929h9gdaqyr2rjcga5lb9ih7966mbx")))

(define-public crate-embedded-layout-macros-0.3.1 (c (n "embedded-layout-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0014sl9lf5d0pa8q3dcnhqfcvfk01bf25dvla9lmpq67whgn4vjg")))

