(define-module (crates-io em be embedded-flight-control) #:use-module (crates-io))

(define-public crate-embedded-flight-control-0.1.0 (c (n "embedded-flight-control") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)))) (h "0pnj8r7ds1gikq4gl7hzy8rs0qg71hbk0r6791pg3z9w5mzm6zsb")))

(define-public crate-embedded-flight-control-0.2.0 (c (n "embedded-flight-control") (v "0.2.0") (d (list (d (n "embedded-flight-core") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)))) (h "0m7d30f52f064f9iq38xsiyf8k05zxdcxkvg1l0kx61i4972hj0j")))

(define-public crate-embedded-flight-control-0.2.1 (c (n "embedded-flight-control") (v "0.2.1") (d (list (d (n "embedded-flight-core") (r "^0.1.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (f (quote ("libm-force"))) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)))) (h "1031cspa6imm24bp45kb7r80pa0a9858zbh7vp51fbcbqylx5wv4")))

