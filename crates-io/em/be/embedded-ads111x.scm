(define-module (crates-io em be embedded-ads111x) #:use-module (crates-io))

(define-public crate-embedded-ads111x-0.1.0 (c (n "embedded-ads111x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1pbsdn78kbixvi1k852vg8id5ydb2796psxp4qh7glhpfvrk9qd9")))

(define-public crate-embedded-ads111x-0.1.1 (c (n "embedded-ads111x") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "14vzycc1ddkfi4h1ygg9bqw0r4gpsrjh8k2r60m2cwv88h9na1hw")))

