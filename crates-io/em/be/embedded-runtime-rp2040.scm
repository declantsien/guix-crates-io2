(define-module (crates-io em be embedded-runtime-rp2040) #:use-module (crates-io))

(define-public crate-embedded-runtime-rp2040-0.1.2 (c (n "embedded-runtime-rp2040") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "embedded-runtime") (r "^0.1.2") (d #t) (k 0)))) (h "03w4g4baci2bnmbdvq2lx3w9ap782z6sv9d1qxba18h44hiyg334") (f (quote (("default"))))))

(define-public crate-embedded-runtime-rp2040-0.2.0 (c (n "embedded-runtime-rp2040") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "embedded-runtime") (r "^0.2.0") (d #t) (k 0)))) (h "1z2ir2hqb6wwx1fsqfh0f0g17b42fp3wskcbcff7yamz6mbw5cgk") (f (quote (("default"))))))

(define-public crate-embedded-runtime-rp2040-0.3.0 (c (n "embedded-runtime-rp2040") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "embedded-runtime") (r "^0.3.0") (d #t) (k 0)))) (h "14qxz4bdh4b154x35jh6nxywnlnpvllkq2dipnrw4dxdpzgmmpyj") (f (quote (("default"))))))

