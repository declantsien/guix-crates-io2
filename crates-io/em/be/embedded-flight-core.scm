(define-module (crates-io em be embedded-flight-core) #:use-module (crates-io))

(define-public crate-embedded-flight-core-0.1.0 (c (n "embedded-flight-core") (v "0.1.0") (d (list (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0g2mbph1j7dcycwzk6w7j5csxxxmdj1ivk5bnjvr0jjv0km00vw6")))

(define-public crate-embedded-flight-core-0.1.1 (c (n "embedded-flight-core") (v "0.1.1") (d (list (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)))) (h "12w8v6m1hdjd5a644lg3ccs8129dqnqpvi169w6pagc11vyklwc5")))

