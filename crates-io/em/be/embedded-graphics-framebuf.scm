(define-module (crates-io em be embedded-graphics-framebuf) #:use-module (crates-io))

(define-public crate-embedded-graphics-framebuf-0.0.1 (c (n "embedded-graphics-framebuf") (v "0.0.1") (d (list (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)))) (h "0mnzv8i8k9rpihyl09a3mysc0zslsnxc2nm80cv3511ajzldjdc0")))

(define-public crate-embedded-graphics-framebuf-0.0.2 (c (n "embedded-graphics-framebuf") (v "0.0.2") (d (list (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)))) (h "1qb1lq77n65byijfp62pf9wnzbymxyl5rb510fv416xvkhk5gsbw")))

(define-public crate-embedded-graphics-framebuf-0.1.0 (c (n "embedded-graphics-framebuf") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)))) (h "1sp4aj6ppgfzlr0ci1ksgkaw1chnvd5x1yix3hdxgmi4sc7zn4ny")))

(define-public crate-embedded-graphics-framebuf-0.2.0 (c (n "embedded-graphics-framebuf") (v "0.2.0") (d (list (d (n "embedded-dma") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)))) (h "0xdk6ma5g47b87m8x3v0xxwf0f8dikkmwl6cd6z8ifwfh6r4iwq3")))

(define-public crate-embedded-graphics-framebuf-0.3.0 (c (n "embedded-graphics-framebuf") (v "0.3.0") (d (list (d (n "embedded-dma") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)))) (h "098nh1ris4l0izf502h4apxdgqrpnxhka7l3q4cbxb42a64brs7z")))

(define-public crate-embedded-graphics-framebuf-0.4.0 (c (n "embedded-graphics-framebuf") (v "0.4.0") (d (list (d (n "embedded-dma") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)))) (h "0y54vrd46mwlcphcjcga5h199x3b79vvbv5zagva697w10h0k7zc")))

(define-public crate-embedded-graphics-framebuf-0.5.0 (c (n "embedded-graphics-framebuf") (v "0.5.0") (d (list (d (n "embedded-dma") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)))) (h "0xmyhmx6976im0skpdhgx1ji014s3sp1sx72s4jgl9w7yqh48d92")))

