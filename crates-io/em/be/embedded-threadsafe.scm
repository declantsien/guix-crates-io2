(define-module (crates-io em be embedded-threadsafe) #:use-module (crates-io))

(define-public crate-embedded-threadsafe-0.2.2 (c (n "embedded-threadsafe") (v "0.2.2") (h "0cpsxb99xcbix9f6nlkkxxgh7zb7a0dfw0flvhdhwmavrv19rzhs") (f (quote (("default"))))))

(define-public crate-embedded-threadsafe-0.2.3 (c (n "embedded-threadsafe") (v "0.2.3") (h "0pla4bynprxdqy4qqiqzig03yclzjrcx6vl74dwlf1vb3rn2cr1i") (f (quote (("default"))))))

