(define-module (crates-io em be embercat) #:use-module (crates-io))

(define-public crate-embercat-0.0.0 (c (n "embercat") (v "0.0.0") (d (list (d (n "chandra") (r "^0.0.0") (k 0)))) (h "1034nnc7pmm1yirflxkk65dqas6sxcjr5k3s8wfnsnjz18f3wfh4") (f (quote (("std" "chandra/std") ("nightly" "chandra/nightly") ("gpu" "chandra/gpu") ("default" "gpu" "std"))))))

