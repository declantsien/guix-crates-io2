(define-module (crates-io em be embedded-nal-async-std) #:use-module (crates-io))

(define-public crate-embedded-nal-async-std-0.1.0 (c (n "embedded-nal-async-std") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12") (d #t) (k 0)) (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "embedded-nal-async") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1sbyxf14f5n298qs1iqhj937cm9y6pk7sx5x4cxfxl6mzbaayaan")))

(define-public crate-embedded-nal-async-std-0.1.1 (c (n "embedded-nal-async-std") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12") (d #t) (k 0)) (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "embedded-nal-async") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0jghvdj7bdxxxcvl1n8zal2i8ij83d81182ncj486f420xmd1x0s")))

