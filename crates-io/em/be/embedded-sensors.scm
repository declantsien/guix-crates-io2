(define-module (crates-io em be embedded-sensors) #:use-module (crates-io))

(define-public crate-embedded-sensors-0.1.0 (c (n "embedded-sensors") (v "0.1.0") (d (list (d (n "ahrs") (r "^0.3.0") (o #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.0") (o #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "nmea0183") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1nqxbcm8bw9qy4n800q9k2l5blnm8fyws80whz7fxh4fvr7g6m64") (f (quote (("ublox" "nmea0183") ("mpu925x" "ahrs" "ak8963" "mpu6500") ("mpu6500" "nalgebra") ("default" "ak8963" "mpu925x" "mpu6500" "ublox") ("ak8963" "nalgebra"))))))

(define-public crate-embedded-sensors-0.1.1 (c (n "embedded-sensors") (v "0.1.1") (d (list (d (n "ahrs") (r "^0.3.0") (o #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.0") (o #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "nmea0183") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0c8ydgwa5597dp13vq2ai1rnb53ynjfipdp4p6f4m070zry44ryv") (f (quote (("ublox" "nmea0183") ("mpu925x" "ahrs" "ak8963" "mpu6500") ("mpu6500" "nalgebra") ("default" "ak8963" "bh1750" "mpu925x" "mpu6500" "ublox") ("bh1750") ("ak8963" "nalgebra"))))))

