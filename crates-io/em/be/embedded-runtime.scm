(define-module (crates-io em be embedded-runtime) #:use-module (crates-io))

(define-public crate-embedded-runtime-0.1.2 (c (n "embedded-runtime") (v "0.1.2") (h "0pw7r8wy2h7ycv7z7ffmr12g61w771p8n2p8lz81wk8lz526h13v") (f (quote (("default"))))))

(define-public crate-embedded-runtime-0.2.0 (c (n "embedded-runtime") (v "0.2.0") (h "1ps7p0hsng7551j1hsi6dyhb57z2605n7xpfnm17yin16civh4rd") (f (quote (("default"))))))

(define-public crate-embedded-runtime-0.3.0 (c (n "embedded-runtime") (v "0.3.0") (h "1vmkm330yddlkhyhl40llshwccypglwgq52l7gx5hb0whj35zxyf") (f (quote (("default"))))))

