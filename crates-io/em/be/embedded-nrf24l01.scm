(define-module (crates-io em be embedded-nrf24l01) #:use-module (crates-io))

(define-public crate-embedded-nrf24l01-0.0.0 (c (n "embedded-nrf24l01") (v "0.0.0") (d (list (d (n "bitfield") (r "^0.13.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "116fqbnfpaypk9jwsppjasldak4k6dqcx3kxxngkci21a94lnmd1")))

(define-public crate-embedded-nrf24l01-0.0.1 (c (n "embedded-nrf24l01") (v "0.0.1") (d (list (d (n "bitfield") (r "^0.13.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "17bgbb8pym5ngz1ycqr4ak94mk6xbk9inksgifakfvaanjv4pj9q")))

(define-public crate-embedded-nrf24l01-0.1.0 (c (n "embedded-nrf24l01") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0w2r3gbzh9pc9mj80xzlc52acplbbqpy3nkljqnzh0ralv8rzd9q")))

(define-public crate-embedded-nrf24l01-0.2.0 (c (n "embedded-nrf24l01") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "0ywr6mvzzi2d7a6cr3sb3lvgpwimgcfqgx78ngs6la5yr0dmxx4y")))

