(define-module (crates-io em be embedded-vintage-fonts) #:use-module (crates-io))

(define-public crate-embedded-vintage-fonts-0.1.0 (c (n "embedded-vintage-fonts") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.7.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0") (d #t) (k 2)))) (h "00ga10pxaz46vvpsyf21j3x2x7wdhgjv5y6dly51l88624gv536h")))

(define-public crate-embedded-vintage-fonts-0.2.0 (c (n "embedded-vintage-fonts") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.5.0") (d #t) (k 2)))) (h "17v96n1jbqnmnc0iyqdlrvpcj4z0rv3sl7gdrd23n7fgnhhx80rp")))

