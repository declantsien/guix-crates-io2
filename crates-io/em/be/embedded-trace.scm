(define-module (crates-io em be embedded-trace) #:use-module (crates-io))

(define-public crate-embedded-trace-0.1.0 (c (n "embedded-trace") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)))) (h "0r53qfsksz6fbc6mv9b52adbg5ksf7cma36082nwqpkyh9157asn")))

(define-public crate-embedded-trace-0.2.0 (c (n "embedded-trace") (v "0.2.0") (d (list (d (n "embedded-hal_0_2") (r "^0.2.7") (o #t) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal_1") (r "^1.0.0-rc.3") (d #t) (k 0) (p "embedded-hal")) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)))) (h "1mx89kq8h2l98ax0ydaigkwl3ir3kmiksis2qkm0gh3ssvm2r3z9")))

(define-public crate-embedded-trace-0.2.1 (c (n "embedded-trace") (v "0.2.1") (d (list (d (n "embedded-hal_0_2") (r "^0.2.7") (o #t) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal_1") (r "^1.0.0-rc.3") (d #t) (k 0) (p "embedded-hal")) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)))) (h "0di01gdc8002lkr19jggss2mrmk84i82w9hd5vi1p2vcc1hgqq1c")))

(define-public crate-embedded-trace-0.2.2 (c (n "embedded-trace") (v "0.2.2") (d (list (d (n "embedded-hal_0_2") (r "^0.2.7") (o #t) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal_1") (r "^1.0.0-rc.3") (d #t) (k 0) (p "embedded-hal")) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)))) (h "012lbbgwafqsx5x413h70jgsnpkg942pimjc11pl6ka07dr5487a")))

