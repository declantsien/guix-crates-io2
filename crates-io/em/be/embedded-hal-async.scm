(define-module (crates-io em be embedded-hal-async) #:use-module (crates-io))

(define-public crate-embedded-hal-async-0.1.0-alpha.0 (c (n "embedded-hal-async") (v "0.1.0-alpha.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)))) (h "1gasrrf9kkv46aphb5y4scxc2klmv3kqc0y46fgnvfnx9116i7qv")))

(define-public crate-embedded-hal-async-0.1.0-alpha.1 (c (n "embedded-hal-async") (v "0.1.0-alpha.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)))) (h "1iay4md2bjdwc2prljy2x7l6g4qcw9x3d2m2anvv5sgxi9rlj8h2")))

(define-public crate-embedded-hal-async-0.1.0-alpha.2 (c (n "embedded-hal-async") (v "0.1.0-alpha.2") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.9") (d #t) (k 0)))) (h "1pnhzqz1nk1g7gcyq301h8608d1jmkxm0k42rcnavh4zyjip1ax3")))

(define-public crate-embedded-hal-async-0.1.0-alpha.3 (c (n "embedded-hal-async") (v "0.1.0-alpha.3") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.9") (d #t) (k 0)))) (h "0mshlsr5z754lm6v00vqw9q3sxk43h37f06zbyw57hh8plqzc4y9")))

(define-public crate-embedded-hal-async-0.2.0-alpha.0 (c (n "embedded-hal-async") (v "0.2.0-alpha.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.9") (d #t) (k 0)))) (h "0sa9bn3kxf1s76x6yxdgb5a9mxv9rg00jcg02mks0pfn10l352k0")))

(define-public crate-embedded-hal-async-0.2.0-alpha.1 (c (n "embedded-hal-async") (v "0.2.0-alpha.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.10") (d #t) (k 0)))) (h "0gq6hgfckv9ghc3mlkz6gimwmn4fq4adlb1islvdwj5glw53fhl0") (r "1.65.0")))

(define-public crate-embedded-hal-async-0.2.0-alpha.2 (c (n "embedded-hal-async") (v "0.2.0-alpha.2") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.11") (d #t) (k 0)))) (h "0a821vs0y7ix671qbz5d5vbw9iyd2rplzimdshjb1v8qggn8b0li") (r "1.65.0")))

(define-public crate-embedded-hal-async-1.0.0-rc.1 (c (n "embedded-hal-async") (v "1.0.0-rc.1") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "embedded-hal") (r "=1.0.0-rc.1") (d #t) (k 0)))) (h "1sfd19rdaxs7pyl9f3c0zd9s85w4yyh9a25hndaj2d25fx8s7a3n") (s 2) (e (quote (("defmt-03" "dep:defmt-03" "embedded-hal/defmt-03")))) (r "1.65.0")))

(define-public crate-embedded-hal-async-1.0.0-rc.2 (c (n "embedded-hal-async") (v "1.0.0-rc.2") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "embedded-hal") (r "=1.0.0-rc.2") (d #t) (k 0)))) (h "0qyx2vfbz4l41svcb6dbgrg8fqvd2wrcvxc5xwbq3cww3yhd8nlv") (s 2) (e (quote (("defmt-03" "dep:defmt-03" "embedded-hal/defmt-03")))) (r "1.65.0")))

(define-public crate-embedded-hal-async-1.0.0-rc.3 (c (n "embedded-hal-async") (v "1.0.0-rc.3") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "embedded-hal") (r "=1.0.0-rc.3") (d #t) (k 0)))) (h "0vb28d84x9cysi24bwk9q12wryppvliv6vyc99hmvczzy8pbl7zs") (s 2) (e (quote (("defmt-03" "dep:defmt-03" "embedded-hal/defmt-03")))) (r "1.75")))

(define-public crate-embedded-hal-async-1.0.0 (c (n "embedded-hal-async") (v "1.0.0") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "110q7zxsis2wl7gwshg86gpq0fgd85la4kfxqv1i7zppprdnhk0c") (s 2) (e (quote (("defmt-03" "dep:defmt-03" "embedded-hal/defmt-03")))) (r "1.75")))

