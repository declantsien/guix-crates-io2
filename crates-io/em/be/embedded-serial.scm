(define-module (crates-io em be embedded-serial) #:use-module (crates-io))

(define-public crate-embedded-serial-0.3.0 (c (n "embedded-serial") (v "0.3.0") (h "082qfpj8ria547x2fm7v3hw5ygrdnz2wi76k5j5b7d9gwnjj94ai")))

(define-public crate-embedded-serial-0.4.0 (c (n "embedded-serial") (v "0.4.0") (h "1axx5dz03yf6nhrd0rjrma6wm008xmb5irj3mjrnkm8mcqryd1v2")))

(define-public crate-embedded-serial-0.5.0 (c (n "embedded-serial") (v "0.5.0") (h "03ji1m803dv7kwzigziaslrcq0xfjndnnrsb70di8cyw8sds1jr3")))

