(define-module (crates-io em be embedded-hal-bus) #:use-module (crates-io))

(define-public crate-embedded-hal-bus-0.1.0-alpha.0 (c (n "embedded-hal-bus") (v "0.1.0-alpha.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)))) (h "08792yz4y53zl0z9l4bgnmdllc7mpzfbamhdg6n7fnpphdw871bg")))

(define-public crate-embedded-hal-bus-0.1.0-alpha.1 (c (n "embedded-hal-bus") (v "0.1.0-alpha.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.9") (d #t) (k 0)))) (h "0k2xf4mkairz6w7hqzfwlwpg1cksalnyvwdqk6cdbgwj8bw81jjw")))

(define-public crate-embedded-hal-bus-0.1.0-alpha.2 (c (n "embedded-hal-bus") (v "0.1.0-alpha.2") (d (list (d (n "critical-section") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.10") (d #t) (k 0)))) (h "0wkq0irzhjwaln8qw94gbgrcdd4m2z0bz9ijj34ayk448f4nkrvf") (f (quote (("std"))))))

(define-public crate-embedded-hal-bus-0.1.0-alpha.3 (c (n "embedded-hal-bus") (v "0.1.0-alpha.3") (d (list (d (n "critical-section") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.11") (d #t) (k 0)))) (h "0m79hw0vbrpm69b091935zjh5fypjza29zzrkf4xxff72xyl0dhb") (f (quote (("std"))))))

(define-public crate-embedded-hal-bus-0.1.0-rc.1 (c (n "embedded-hal-bus") (v "0.1.0-rc.1") (d (list (d (n "critical-section") (r "^1.0") (d #t) (k 0)) (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "embedded-hal") (r "=1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=1.0.0-rc.1") (o #t) (d #t) (k 0)))) (h "0ahvvpwyfmpykaf93a385pszgy6bn3bj9m56kdiakyiyhpr24a36") (f (quote (("std")))) (s 2) (e (quote (("defmt-03" "dep:defmt-03" "embedded-hal/defmt-03" "embedded-hal-async?/defmt-03") ("async" "dep:embedded-hal-async"))))))

(define-public crate-embedded-hal-bus-0.1.0-rc.2 (c (n "embedded-hal-bus") (v "0.1.0-rc.2") (d (list (d (n "critical-section") (r "^1.0") (d #t) (k 0)) (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "embedded-hal") (r "=1.0.0-rc.2") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=1.0.0-rc.2") (o #t) (d #t) (k 0)))) (h "1iqm5ic1g77k1lvmj0qljbhrxca19kx9s0ppz0mv56hmlyi2di8n") (f (quote (("std")))) (s 2) (e (quote (("defmt-03" "dep:defmt-03" "embedded-hal/defmt-03" "embedded-hal-async?/defmt-03") ("async" "dep:embedded-hal-async")))) (r "1.60")))

(define-public crate-embedded-hal-bus-0.1.0-rc.3 (c (n "embedded-hal-bus") (v "0.1.0-rc.3") (d (list (d (n "critical-section") (r "^1.0") (d #t) (k 0)) (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "embedded-hal") (r "=1.0.0-rc.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=1.0.0-rc.3") (o #t) (d #t) (k 0)))) (h "0kqzhdh7ry42xln4y8yf8idk2761cdj5gf3drm526q9lnz52xyqy") (f (quote (("std")))) (s 2) (e (quote (("defmt-03" "dep:defmt-03" "embedded-hal/defmt-03" "embedded-hal-async?/defmt-03") ("async" "dep:embedded-hal-async")))) (r "1.60")))

(define-public crate-embedded-hal-bus-0.1.0 (c (n "embedded-hal-bus") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.0") (d #t) (k 0)) (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "11k8n1869v6fxz1mr6zrrlbv0d0a6bk8dncc87dynfa3x3nydd2p") (f (quote (("std")))) (s 2) (e (quote (("defmt-03" "dep:defmt-03" "embedded-hal/defmt-03" "embedded-hal-async?/defmt-03") ("async" "dep:embedded-hal-async")))) (r "1.60")))

(define-public crate-embedded-hal-bus-0.2.0 (c (n "embedded-hal-bus") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.0") (d #t) (k 0)) (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^1") (k 0)))) (h "0sbng7vvd8pf8hv3djyfs9lr8hcahvgv7gg2kyspsmz852zq0f8d") (f (quote (("std")))) (s 2) (e (quote (("defmt-03" "dep:defmt-03" "embedded-hal/defmt-03" "embedded-hal-async?/defmt-03") ("async" "dep:embedded-hal-async")))) (r "1.60")))

