(define-module (crates-io em be embed-manifest) #:use-module (crates-io))

(define-public crate-embed-manifest-1.0.0 (c (n "embed-manifest") (v "1.0.0") (h "0gq1abjkqb5bjl2ab2lx6xpd3ci183jx4i6p4m1ly757fdb7101h") (r "1.56")))

(define-public crate-embed-manifest-1.1.0 (c (n "embed-manifest") (v "1.1.0") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write_core" "coff"))) (k 0)) (d (n "object") (r "^0.28.3") (f (quote ("read_core" "archive" "coff"))) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1d2qi1cia5bnckral9dqcan9856232r1hhgnwwxvxkbxn2vbini5") (r "1.56")))

(define-public crate-embed-manifest-1.2.0 (c (n "embed-manifest") (v "1.2.0") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write_core" "coff"))) (k 0)) (d (n "object") (r "^0.28.3") (f (quote ("read_core" "archive" "coff"))) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0bba4vfg8lgarpfrfyxpavwx66p5y5pwr5qv0mmg8cga0l292xjf") (r "1.56")))

(define-public crate-embed-manifest-1.2.1 (c (n "embed-manifest") (v "1.2.1") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write_core" "coff"))) (k 0)) (d (n "object") (r "^0.28.3") (f (quote ("read_core" "archive" "coff"))) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1nlkx70hdw2fc031xynh2j867akaxasa0r8r02hxhrkknsxlzhrk") (r "1.56")))

(define-public crate-embed-manifest-1.3.0 (c (n "embed-manifest") (v "1.3.0") (d (list (d (n "object") (r "=0.28.3") (f (quote ("read_core" "coff"))) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0cjbkkak0jiwfzavws39mvm5dnbbdqa1lgi5hsgm8cn2aic4jwky") (r "1.56")))

(define-public crate-embed-manifest-1.3.1 (c (n "embed-manifest") (v "1.3.1") (d (list (d (n "object") (r "^0.29.0") (f (quote ("read_core" "coff"))) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1xq09d9dlrldnhf7kbic88a95ygq44zvhfrqjn4lyy8a1d5mgzs0") (r "1.56")))

(define-public crate-embed-manifest-1.4.0 (c (n "embed-manifest") (v "1.4.0") (d (list (d (n "object") (r "^0.31.1") (f (quote ("read_core" "coff"))) (k 2)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1bjyigz63zifyp2ygynnj7pyns804igyzb2kicfyssqdi5n49ka1") (r "1.56")))

