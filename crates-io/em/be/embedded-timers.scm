(define-module (crates-io em be embedded-timers) #:use-module (crates-io))

(define-public crate-embedded-timers-0.2.0 (c (n "embedded-timers") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "nb") (r "^1.1.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (k 0)))) (h "013c192cdyzpsci166alz65fn1g887xcc11rr0ca15jqgrbbvz13")))

