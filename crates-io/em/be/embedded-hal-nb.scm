(define-module (crates-io em be embedded-hal-nb) #:use-module (crates-io))

(define-public crate-embedded-hal-nb-0.0.0 (c (n "embedded-hal-nb") (v "0.0.0") (h "0k1kbpjgkwvpyblkgq098kaks0q4dv1f69fiysq8qfjxjnxm85rr") (y #t)))

(define-public crate-embedded-hal-nb-1.0.0-alpha.0 (c (n "embedded-hal-nb") (v "1.0.0-alpha.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.15") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "0yk82yd3pfrh4mss98y8l34xn2y727fn0px30a02gicbw1anyglw")))

(define-public crate-embedded-hal-nb-1.0.0-alpha.1 (c (n "embedded-hal-nb") (v "1.0.0-alpha.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.9") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.15") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "0fif8xhx8vg5mh0gpv5jsmbz20zi59a3jgz3smcnixrv1bn601vy")))

(define-public crate-embedded-hal-nb-1.0.0-alpha.2 (c (n "embedded-hal-nb") (v "1.0.0-alpha.2") (d (list (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "=1.0.0-alpha.10") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.15") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "0p067mb08pf07ccbpihwgc11an0x3k55p5hpbh8bqnx9avyzyr8l")))

(define-public crate-embedded-hal-nb-1.0.0-alpha.3 (c (n "embedded-hal-nb") (v "1.0.0-alpha.3") (d (list (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "=1.0.0-alpha.11") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.15") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "1mv0cc8xi36l96xxdy2lwqranqy96rzc272y801hirjli8zlr7m0")))

(define-public crate-embedded-hal-nb-1.0.0-rc.1 (c (n "embedded-hal-nb") (v "1.0.0-rc.1") (d (list (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "=1.0.0-rc.1") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.15") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "1j9rbz2vssk286bymmb1b0in7f8pn21h1ghfrdd1js9w2sq3nzi5")))

(define-public crate-embedded-hal-nb-1.0.0-rc.2 (c (n "embedded-hal-nb") (v "1.0.0-rc.2") (d (list (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "=1.0.0-rc.2") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.15") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "00rp2aqp41xbx8jfjdvjzwrnql902c59ch983sczagyn298k5i37") (r "1.56")))

(define-public crate-embedded-hal-nb-1.0.0-rc.3 (c (n "embedded-hal-nb") (v "1.0.0-rc.3") (d (list (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "=1.0.0-rc.3") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.15") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "1svdccqg1fivyahvmddhjyk3bizq5gmxrc4y730bcx8658qmipiw") (r "1.56")))

(define-public crate-embedded-hal-nb-1.0.0 (c (n "embedded-hal-nb") (v "1.0.0") (d (list (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "stm32f1") (r "^0.15") (f (quote ("stm32f103" "rt"))) (d #t) (k 2)))) (h "01c65qa4hihvmwqkgmy6yrqf3nxb5fqmk7i9jn4q53182j62d97v") (r "1.56")))

