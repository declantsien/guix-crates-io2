(define-module (crates-io em be embedded-can) #:use-module (crates-io))

(define-public crate-embedded-can-0.1.0 (c (n "embedded-can") (v "0.1.0") (d (list (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1sg9xaws8nri4l2iklxgy0dysbw1qhkybl2q9jszg304i0cmr61g")))

(define-public crate-embedded-can-0.2.0 (c (n "embedded-can") (v "0.2.0") (d (list (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0pkxwsqqi00vv5yagkzldn3cn8z8f4zx4m9f8xrs8whm2s7j95iz")))

(define-public crate-embedded-can-0.3.0 (c (n "embedded-can") (v "0.3.0") (d (list (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0dg6bppa14bhsfy625sw23f24q3mc4gxzad098bhs276vihrcw8j")))

(define-public crate-embedded-can-0.4.0 (c (n "embedded-can") (v "0.4.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "18nn643nji9m9albmq61wvh74ppg6c3x0qj6vx6q65bm73l1gn9j")))

(define-public crate-embedded-can-0.4.1 (c (n "embedded-can") (v "0.4.1") (d (list (d (n "nb") (r "^1") (d #t) (k 0)))) (h "0f4lmwdn1rc8764smr6jydf1fyf6vlc8sjgsd3gk5j3sz1byilp9")))

