(define-module (crates-io em be embed-doc-image) #:use-module (crates-io))

(define-public crate-embed-doc-image-0.1.0 (c (n "embed-doc-image") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "parsing" "proc-macro" "printing"))) (k 0)))) (h "0ql3w8aja93li97ifkkwcni7dk9cms0kjh743g2gdrbz51y685fd")))

(define-public crate-embed-doc-image-0.1.1 (c (n "embed-doc-image") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "parsing" "proc-macro" "printing"))) (k 0)))) (h "1jwp7j71y2ja5fjxr976062b21s7y5sx6awx9gvhgj7pkw4949xk")))

(define-public crate-embed-doc-image-0.1.2 (c (n "embed-doc-image") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "parsing" "proc-macro" "printing"))) (k 0)))) (h "1q6pqsv9fqv7rdgkqkmw2b99g9jdqq928hfiawb1kgaal6qn0cid")))

(define-public crate-embed-doc-image-0.1.3 (c (n "embed-doc-image") (v "0.1.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "parsing" "proc-macro" "printing"))) (k 0)))) (h "0pky0bmiwqprbbi0ybzik89s1c4xvcklw0cdvnwx7spak21ycz2j")))

(define-public crate-embed-doc-image-0.1.4 (c (n "embed-doc-image") (v "0.1.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "parsing" "proc-macro" "printing"))) (k 0)))) (h "0vhn8wnbkm61ranfhpmzrs7mbyjqarcrcs6b4lj857bd4f8zadmg")))

