(define-module (crates-io em be embedded-ffi) #:use-module (crates-io))

(define-public crate-embedded-ffi-0.1.0 (c (n "embedded-ffi") (v "0.1.0") (d (list (d (n "cstr_core") (r "^0.1.2") (d #t) (k 0)))) (h "0g47jjb6d8f372s6h9yadqma5vallxf71skw1k7xgc2g4q3xagc8") (f (quote (("alloc" "cstr_core/alloc"))))))

(define-public crate-embedded-ffi-0.1.1 (c (n "embedded-ffi") (v "0.1.1") (d (list (d (n "cstr_core") (r "^0.1.2") (d #t) (k 0)))) (h "0dvipf8mg6vxvnmqfghsk4h3lzk705fqmlk8g3j75ak5nfm30024") (f (quote (("alloc" "cstr_core/alloc"))))))

(define-public crate-embedded-ffi-0.1.2 (c (n "embedded-ffi") (v "0.1.2") (d (list (d (n "cstr_core") (r "^0.1.2") (d #t) (k 0)))) (h "0chgngsk2pmix4zri8gzrykhy60swkq9gwd1j1z41argwjcza57x") (f (quote (("alloc" "cstr_core/alloc"))))))

