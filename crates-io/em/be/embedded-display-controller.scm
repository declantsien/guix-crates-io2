(define-module (crates-io em be embedded-display-controller) #:use-module (crates-io))

(define-public crate-embedded-display-controller-0.1.0 (c (n "embedded-display-controller") (v "0.1.0") (h "1wll2hrsafzzsa4qpv7z1z7d56kb2l5v73gaxxv3yd2cvllavhgz")))

(define-public crate-embedded-display-controller-0.2.0 (c (n "embedded-display-controller") (v "0.2.0") (h "0ib5rkk0waacsnb6fhzfgsw2pgxvlih7phnzlnwa7vd28j6m3nxb")))

