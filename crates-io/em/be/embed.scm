(define-module (crates-io em be embed) #:use-module (crates-io))

(define-public crate-embed-0.1.0 (c (n "embed") (v "0.1.0") (h "00jhw7snj6cg9p41fp4444whbj51n8w6vc6k4rddkhk05v55kzjn")))

(define-public crate-embed-0.1.1 (c (n "embed") (v "0.1.1") (d (list (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "06f8ib4akc4rs8ir3i5g871hnpg9mnicnb1nmb4sdhamfb450iph")))

