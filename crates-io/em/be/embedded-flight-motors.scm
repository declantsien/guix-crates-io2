(define-module (crates-io em be embedded-flight-motors) #:use-module (crates-io))

(define-public crate-embedded-flight-motors-0.1.0 (c (n "embedded-flight-motors") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1f1wj3wd0giwrk9xvxm9r6yjqg1w76956j82i4kn0wwk1da93iz1")))

(define-public crate-embedded-flight-motors-0.1.1 (c (n "embedded-flight-motors") (v "0.1.1") (d (list (d (n "embedded-flight-core") (r "^0.1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0w0k7k0m4nzd1vpa9i1qjlgmg0l546n2nmaynh5v6h2m0vfwqxwl")))

(define-public crate-embedded-flight-motors-0.1.2 (c (n "embedded-flight-motors") (v "0.1.2") (d (list (d (n "embedded-flight-core") (r "^0.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (f (quote ("libm-force"))) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)))) (h "1jxdgnxp5cqyaq8cbdi8rbq1kpzx22l8zkqscv4m7qjx2v07mgmj")))

