(define-module (crates-io em be embedded-platform) #:use-module (crates-io))

(define-public crate-embedded-platform-0.1.0 (c (n "embedded-platform") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("async-await"))) (k 0)))) (h "08sif6drmwwklx1vmnl8cfq61yndqgzri6fyzkm4f5jl59frwzbl")))

(define-public crate-embedded-platform-0.1.1 (c (n "embedded-platform") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("async-await"))) (k 0)))) (h "0qjcb5lzzhn6nsdf1pz16qz9cxgay7zpxlzk3gc0dj2yrivcrj4w")))

(define-public crate-embedded-platform-0.1.2 (c (n "embedded-platform") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("async-await"))) (k 0)))) (h "1bj86w8clkb8lq6j18436y606sls2s343v74df2iafzwih9d9487")))

