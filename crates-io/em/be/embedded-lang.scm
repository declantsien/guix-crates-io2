(define-module (crates-io em be embedded-lang) #:use-module (crates-io))

(define-public crate-embedded-lang-0.5.0 (c (n "embedded-lang") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1r4yfjxnz564amacs0vlr81f3iixn9wynk6xyk5lza8142x0nzm5")))

(define-public crate-embedded-lang-0.5.1 (c (n "embedded-lang") (v "0.5.1") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0i0s1vapihdbh2ms51jf926syr7n3wrqd6kziy0g83jppk96k7px")))

(define-public crate-embedded-lang-0.5.2 (c (n "embedded-lang") (v "0.5.2") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "10clbh3xm1mp5ip4sr9vcibj4crp19y9r1qs4zhvpmcibxphmnkd")))

(define-public crate-embedded-lang-0.5.3 (c (n "embedded-lang") (v "0.5.3") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0f8ay5k3z53pikrvkkgb5vwza2854mp5sjpzwxdhhl6wkdyk4qhi")))

(define-public crate-embedded-lang-0.6.0 (c (n "embedded-lang") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0hp4njs4fwg7vif2bnxwsl9ghminj9gsf49y75hvlkgwlh131k2x")))

(define-public crate-embedded-lang-0.7.0 (c (n "embedded-lang") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "09ddgpdmkdqdlwbn96pgddpzgv2x6hc56r6m67a6n75dsjx4va9h")))

(define-public crate-embedded-lang-0.8.0 (c (n "embedded-lang") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "15v817x3z47jm2ai66alhn2xcfydbixnpjazvhrzqzsyxik87aq8")))

(define-public crate-embedded-lang-0.8.1 (c (n "embedded-lang") (v "0.8.1") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1baiy5z3sd7xwkm09xzkw56fijrmpv7ch02qgy1kqy46cmqa9069")))

(define-public crate-embedded-lang-0.9.0 (c (n "embedded-lang") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ia446ms014i3ck8vw2j0gj7n5a4r8gp3g2yb7v815062vk5m04i")))

