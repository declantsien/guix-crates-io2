(define-module (crates-io em be ember-rs) #:use-module (crates-io))

(define-public crate-ember-rs-0.1.0 (c (n "ember-rs") (v "0.1.0") (d (list (d (n "minifb") (r "^0.24.0") (d #t) (k 0)))) (h "0y02xkl6qff2kglcr7zz23aiwzyvggnr506yx0gyc9r9msbmgq9p")))

(define-public crate-ember-rs-0.1.1 (c (n "ember-rs") (v "0.1.1") (d (list (d (n "minifb") (r "^0.24.0") (d #t) (k 0)))) (h "1z1964vz49pcvbfarg9pyb2c60pnp864g810awn7zwjfpsscj6kr")))

(define-public crate-ember-rs-0.1.2 (c (n "ember-rs") (v "0.1.2") (d (list (d (n "minifb") (r "^0.24.0") (d #t) (k 0)))) (h "0j6klswap93pz5qkgqy3b98mnkjyv17l6g1z5f64prj64wfc7wjf")))

(define-public crate-ember-rs-0.1.3 (c (n "ember-rs") (v "0.1.3") (d (list (d (n "minifb") (r "^0.24.0") (d #t) (k 0)))) (h "11ax7dkq73pvnzi1jhij1qzjysfz1pjm7dzb0bp3w3sligsi2pg7")))

(define-public crate-ember-rs-0.1.4 (c (n "ember-rs") (v "0.1.4") (d (list (d (n "minifb") (r "^0.24.0") (d #t) (k 0)))) (h "1zw9aw2yi3nm1bh4vpjh9rpg0aq6faxbicx7c5zg88il5dzsxvpl")))

(define-public crate-ember-rs-0.1.5 (c (n "ember-rs") (v "0.1.5") (d (list (d (n "minifb") (r "^0.24.0") (d #t) (k 0)))) (h "0gmj9zhvs6r0iwl03kr3mgwnhlx1l0jnbhj2g35kirv1xwmzpyka")))

