(define-module (crates-io em be embedded-graphics-sparklines) #:use-module (crates-io))

(define-public crate-embedded-graphics-sparklines-0.0.1 (c (n "embedded-graphics-sparklines") (v "0.0.1") (d (list (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "05gfnvrhxw4cqwvlyjd8qj09nrwa1lpcmwv0w9mg9f9hyyqbfx83") (f (quote (("build-binary" "rand" "embedded-graphics-simulator"))))))

(define-public crate-embedded-graphics-sparklines-0.1.0 (c (n "embedded-graphics-sparklines") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "15zwjd06iq6lswfrahn3jmvwd0jg7rsqrp7zmzdd4830jyvy46cn") (f (quote (("build-binary" "rand" "embedded-graphics-simulator"))))))

