(define-module (crates-io em be embedded-menu-macros) #:use-module (crates-io))

(define-public crate-embedded-menu-macros-0.1.0 (c (n "embedded-menu-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("clone-impls" "proc-macro" "full" "extra-traits" "derive" "parsing" "printing"))) (k 0)))) (h "1vln0pc9mizva9axafbn3dw6a6gfgxhq4k4y3dk3pbl4kxsyljjj")))

(define-public crate-embedded-menu-macros-0.2.0 (c (n "embedded-menu-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("clone-impls" "proc-macro" "full" "extra-traits" "derive" "parsing" "printing"))) (k 0)))) (h "1macxwgwg3i5ws8awg5ywn0jbqqmhvp1walsgdd01k0x1vgkrv2m")))

(define-public crate-embedded-menu-macros-0.3.0 (c (n "embedded-menu-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("clone-impls" "proc-macro" "full" "extra-traits" "derive" "parsing" "printing"))) (k 0)))) (h "0ar65jgspvlpnyd86xrcvj7lk8kb38wsdr8lzn0vp3d47lxijzig")))

(define-public crate-embedded-menu-macros-0.3.1 (c (n "embedded-menu-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("clone-impls" "proc-macro" "full" "extra-traits" "derive" "parsing" "printing"))) (k 0)))) (h "1wg2c53qmk5dzklmrlma2za5pcbfnk08jz3mazwql2yk75bkr55j")))

(define-public crate-embedded-menu-macros-0.4.0 (c (n "embedded-menu-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("clone-impls" "proc-macro" "full" "extra-traits" "derive" "parsing" "printing"))) (k 0)))) (h "0107bwq0f704rd6lsrzb3xdppsdr7fsjsznrlcsny8maahxppiyn")))

