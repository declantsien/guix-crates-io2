(define-module (crates-io em be embedded-error) #:use-module (crates-io))

(define-public crate-embedded-error-0.1.0 (c (n "embedded-error") (v "0.1.0") (h "087pvck7diyvkmyiwg5k5bsishzn3hxaadm6q4cxsf2ih9jd12c7")))

(define-public crate-embedded-error-0.2.0 (c (n "embedded-error") (v "0.2.0") (h "1qj73398x4zkmy68ldyjr3lhvbf4zbjhf7yci8q8bdmvr8x0lzxf")))

(define-public crate-embedded-error-0.3.0 (c (n "embedded-error") (v "0.3.0") (h "0463mfj59rmi4yjy0i1v6nznhswva0sx63nhwxnvczq7arxqy21v")))

(define-public crate-embedded-error-0.4.0 (c (n "embedded-error") (v "0.4.0") (h "0k2agq517vsyqiz5mna1nz7z5mf6f9nzp84aqk9g8qa3ykblgkpd")))

(define-public crate-embedded-error-0.5.0 (c (n "embedded-error") (v "0.5.0") (h "1h683bzfrp03zhx3wdv0n97wgcj8cml2p2acci3977gbnzh49382")))

(define-public crate-embedded-error-0.6.0 (c (n "embedded-error") (v "0.6.0") (h "0q5kc66w2i584mgpfx2s2h6ld3x7p2dqilyh42gvs5d5nd3yfc22")))

