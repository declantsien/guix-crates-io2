(define-module (crates-io em be embedded-flight-scheduler) #:use-module (crates-io))

(define-public crate-embedded-flight-scheduler-0.1.0 (c (n "embedded-flight-scheduler") (v "0.1.0") (d (list (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)))) (h "1048fylg48zlfvz48jpjls82bq516x2z817z76xpycyrhig80xqq")))

(define-public crate-embedded-flight-scheduler-0.1.1 (c (n "embedded-flight-scheduler") (v "0.1.1") (d (list (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "0fh1yyagaadp930c75qkfamyw74cbb5s946ivkanrdfvs59335gq")))

