(define-module (crates-io em be embedded-profiling-proc-macros) #:use-module (crates-io))

(define-public crate-embedded-profiling-proc-macros-0.1.0 (c (n "embedded-profiling-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1aph3gwn7z35wkixlcvmvhbj3n4x8zgkdch1l99x4dvvc6h53vgg")))

(define-public crate-embedded-profiling-proc-macros-0.1.1 (c (n "embedded-profiling-proc-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1inn637hsdvdfsd7dmpwh6xf5bq6n9vk1fb8vmcjrd8da3hk3cwl")))

(define-public crate-embedded-profiling-proc-macros-0.2.0 (c (n "embedded-profiling-proc-macros") (v "0.2.0") (d (list (d (n "embedded-profiling") (r "^0.2") (f (quote ("proc-macros"))) (d #t) (k 2)) (d (n "quote") (r "^1.0") (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v3s12fddq747xh90zichwvd2np0c9q9mh5293awwjy8m8lc900z")))

