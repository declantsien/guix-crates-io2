(define-module (crates-io em be embedded-cli-macros) #:use-module (crates-io))

(define-public crate-embedded-cli-macros-0.1.0 (c (n "embedded-cli-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1i6mgqhh12gx63k619ri2zy1hxvsgk66b5xfch04p5rh1gj7w42x") (f (quote (("default"))))))

(define-public crate-embedded-cli-macros-0.1.1 (c (n "embedded-cli-macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "171l183h75f6jwbkjmchfq8c9b8p7g46pmg30rpsh2xsfj8rsvl5") (f (quote (("default"))))))

(define-public crate-embedded-cli-macros-0.1.2 (c (n "embedded-cli-macros") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0q2681hmm0g882ygw3lnnrs1sz56w2zg3l8c47k8sf3p7gwr254q") (f (quote (("help") ("default") ("autocomplete"))))))

(define-public crate-embedded-cli-macros-0.2.0 (c (n "embedded-cli-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "17krbkb89c9bwvdhqrhk2ckrnygnr92qv7k6j338ngvwm4wa2qh4") (f (quote (("help") ("default") ("autocomplete"))))))

(define-public crate-embedded-cli-macros-0.2.1 (c (n "embedded-cli-macros") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1gr24n9k3i5c6j0br0djszfk0c51csbp775i7wjgpw88nfq8a061") (f (quote (("help") ("default") ("autocomplete"))))))

