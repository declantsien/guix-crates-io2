(define-module (crates-io em be embedded-nal-async) #:use-module (crates-io))

(define-public crate-embedded-nal-async-0.1.0 (c (n "embedded-nal-async") (v "0.1.0") (d (list (d (n "embedded-nal") (r "^0.6.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "no-std-net") (r "^0.5") (d #t) (k 0)))) (h "0m5asgwzxhyh5h8xshyj9kcyh8h84an8x1fiwbj85j5vvn1fwp10")))

(define-public crate-embedded-nal-async-0.2.0 (c (n "embedded-nal-async") (v "0.2.0") (d (list (d (n "embedded-io") (r "^0.3.0") (f (quote ("async"))) (k 0)) (d (n "embedded-nal") (r "^0.6.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "no-std-net") (r "^0.5") (d #t) (k 0)))) (h "10kc1xdhq724v699qmz2gfhpw8y8q558bjs4aws2qj7y02hz8vxy")))

(define-public crate-embedded-nal-async-0.3.0 (c (n "embedded-nal-async") (v "0.3.0") (d (list (d (n "embedded-io") (r "^0.4.0") (f (quote ("async"))) (k 0)) (d (n "embedded-nal") (r "^0.6.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "no-std-net") (r "^0.6") (d #t) (k 0)))) (h "1a0dz7m63z1k3pnsn1kp80b2xwbfzwzm1dfrrag5xsd66zcgbj0d")))

(define-public crate-embedded-nal-async-0.4.0 (c (n "embedded-nal-async") (v "0.4.0") (d (list (d (n "embedded-io") (r "^0.4.0") (f (quote ("async"))) (k 0)) (d (n "embedded-nal") (r "^0.6.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "no-std-net") (r "^0.6") (d #t) (k 0)))) (h "166zn7sxl4pgsq3xaijfinzy65fnyhsv4g8lxivjg4fa33sq9ki7")))

(define-public crate-embedded-nal-async-0.5.0 (c (n "embedded-nal-async") (v "0.5.0") (d (list (d (n "embedded-io-async") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-nal") (r "^0.7.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "no-std-net") (r "^0.6") (d #t) (k 0)))) (h "05azcddsd9m0yj2fk6p9wh198hl56gi58kdjalhmianfiqlba1p7") (f (quote (("ip_in_core"))))))

(define-public crate-embedded-nal-async-0.6.0 (c (n "embedded-nal-async") (v "0.6.0") (d (list (d (n "embedded-io-async") (r "^0.6.0") (d #t) (k 0)) (d (n "embedded-nal") (r "^0.7.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "no-std-net") (r "^0.6") (d #t) (k 0)))) (h "0hgkc0v9pbjkw565pf8q4y86751ry5734cik8iag619mr055zxx0") (f (quote (("ip_in_core"))))))

(define-public crate-embedded-nal-async-0.7.0 (c (n "embedded-nal-async") (v "0.7.0") (d (list (d (n "embedded-io-async") (r "^0.6.0") (d #t) (k 0)) (d (n "embedded-nal") (r "^0.8.0") (d #t) (k 0)) (d (n "no-std-net") (r "^0.6") (d #t) (k 0)))) (h "0lvn6dr4p0mm1g2a6lpkqli1hmjr1z6q49sxqjp857pxxmd8sw14") (f (quote (("ip_in_core"))))))

(define-public crate-embedded-nal-async-0.7.1 (c (n "embedded-nal-async") (v "0.7.1") (d (list (d (n "embedded-io-async") (r "^0.6.0") (d #t) (k 0)) (d (n "embedded-nal") (r "^0.8.0") (d #t) (k 0)) (d (n "no-std-net") (r "^0.6") (d #t) (k 0)))) (h "0ndpqizgb6why7rs10npls6ng407nc20zxdpn0wx44pwlhvr28kj") (f (quote (("ip_in_core"))))))

