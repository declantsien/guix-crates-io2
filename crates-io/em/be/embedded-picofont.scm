(define-module (crates-io em be embedded-picofont) #:use-module (crates-io))

(define-public crate-embedded-picofont-0.1.0 (c (n "embedded-picofont") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.14.0") (d #t) (k 1)) (d (n "embedded-graphics") (r "^0.5.1") (d #t) (k 0)) (d (n "lodepng") (r "^2.4.2") (d #t) (k 1)) (d (n "lodepng") (r "^2.4.2") (d #t) (k 2)))) (h "07nhykpkhnk35wi640xp5542hcqhp8svav2c6ak5zvpw6f06rwfv")))

(define-public crate-embedded-picofont-0.1.1 (c (n "embedded-picofont") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.15.0") (d #t) (k 1)) (d (n "embedded-graphics") (r "^0.5.1") (d #t) (k 0)) (d (n "lodepng") (r "^2.4.2") (d #t) (k 1)) (d (n "lodepng") (r "^2.4.2") (d #t) (k 2)))) (h "01245nvh42a0758xirzhma8lqhsaz31k7zxhild03s1z2aw411m1")))

(define-public crate-embedded-picofont-0.2.0 (c (n "embedded-picofont") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.17.1") (d #t) (k 1)) (d (n "embedded-graphics") (r "^0.6.0") (d #t) (k 0)) (d (n "lodepng") (r "^2.4.2") (d #t) (k 1)) (d (n "lodepng") (r "^2.4.2") (d #t) (k 2)))) (h "1gskqc1ph11bm6np34gb9w40dnv3mxzbsyqxamsaflyc4gfvqp8g")))

(define-public crate-embedded-picofont-0.2.1 (c (n "embedded-picofont") (v "0.2.1") (d (list (d (n "bitvec") (r "^0.19.4") (d #t) (k 1)) (d (n "embedded-graphics") (r "^0.6.0") (d #t) (k 0)) (d (n "lodepng") (r "^3.2") (d #t) (k 1)) (d (n "lodepng") (r "^3.2") (d #t) (k 2)))) (h "1sc0hab58sdl5i26vc37b292fj4msgzliq8grgzyv1s2diaypk63")))

