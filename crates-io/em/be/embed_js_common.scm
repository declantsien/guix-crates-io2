(define-module (crates-io em be embed_js_common) #:use-module (crates-io))

(define-public crate-embed_js_common-0.1.0 (c (n "embed_js_common") (v "0.1.0") (d (list (d (n "cpp_syn") (r "^0.12.0") (d #t) (k 0)) (d (n "cpp_synmap") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "15aj9xma1x5hclfb1xaxc8km8zgiswx8bapinph5v62sxys9g9sl")))

(define-public crate-embed_js_common-0.1.1 (c (n "embed_js_common") (v "0.1.1") (d (list (d (n "cpp_syn") (r "^0.12.0") (d #t) (k 0)) (d (n "cpp_synmap") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0hn44l24r1zkpn9ywbz3436f0k3n9bhswy1iyv47mj32laqd1byf")))

(define-public crate-embed_js_common-0.2.0 (c (n "embed_js_common") (v "0.2.0") (d (list (d (n "cpp_syn") (r "^0.12.0") (d #t) (k 0)) (d (n "cpp_synmap") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0bkw0n4h3n72wrnmdrmcbaqlbl4rclqvrbs2p2j400n4k5qqvpa5")))

(define-public crate-embed_js_common-0.2.1 (c (n "embed_js_common") (v "0.2.1") (d (list (d (n "cpp_syn") (r "^0.12.0") (d #t) (k 0)) (d (n "cpp_synmap") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1g7fjmi421y71s47216x7rmwrcalx6w7rz1p1gd57v3dy5n16357")))

(define-public crate-embed_js_common-0.3.0 (c (n "embed_js_common") (v "0.3.0") (d (list (d (n "cpp_syn") (r "^0.12.0") (d #t) (k 0)) (d (n "cpp_synmap") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1vi0f0cvma74xpk9vqvhsr0qmcn5fbchr0ig9a1kbdn917gdzdjc")))

