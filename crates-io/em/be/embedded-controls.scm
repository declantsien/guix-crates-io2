(define-module (crates-io em be embedded-controls) #:use-module (crates-io))

(define-public crate-embedded-controls-0.1.1 (c (n "embedded-controls") (v "0.1.1") (d (list (d (n "num-integer") (r "^0.1.45") (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "switch-hal") (r "^0.4.0") (d #t) (k 0)))) (h "0n9j9gngq2cdr4a9b5lkp7rw4knmriznwpcl1v1kjhsfwv513b0s") (y #t)))

(define-public crate-embedded-controls-0.1.2 (c (n "embedded-controls") (v "0.1.2") (d (list (d (n "num-integer") (r "^0.1.45") (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "switch-hal") (r "^0.4.0") (d #t) (k 0)))) (h "1fymwnrzlwbknfchj727dibpkhhra7lhmykp5g64qjba71l5n1vz")))

(define-public crate-embedded-controls-0.1.3 (c (n "embedded-controls") (v "0.1.3") (d (list (d (n "num-integer") (r "^0.1.45") (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "switch-hal") (r "^0.4.0") (d #t) (k 0)))) (h "0nccwfplg481vdgk2wfas18mqci4jsclpwms4qj2vch5s7lmmask")))

(define-public crate-embedded-controls-0.1.4 (c (n "embedded-controls") (v "0.1.4") (d (list (d (n "num-integer") (r "^0.1.45") (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "switch-hal") (r "^0.4.0") (d #t) (k 0)) (d (n "timestamp-source") (r "^0.1.2") (d #t) (k 0)))) (h "0qq80bpyy9lcnmskfxibx822m9vh0rpbxxnqqy8kaw9inbc3a0ys")))

(define-public crate-embedded-controls-0.1.5 (c (n "embedded-controls") (v "0.1.5") (d (list (d (n "num-integer") (r "^0.1.45") (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "switch-hal") (r "^0.4.0") (d #t) (k 0)) (d (n "timestamp-source") (r "^0.1.3") (d #t) (k 0)))) (h "0gg5iyx84rbyjhmcnhw12p2bw1h5lwx9sw4803gank4hm31h50w5")))

