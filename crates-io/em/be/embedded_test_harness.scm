(define-module (crates-io em be embedded_test_harness) #:use-module (crates-io))

(define-public crate-embedded_test_harness-0.1.0 (c (n "embedded_test_harness") (v "0.1.0") (d (list (d (n "cortex-m-semihosting") (r "^0.3.7") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.6.0") (d #t) (k 0)))) (h "1k3yydfyz43gaji4gzgasif4fgc0ad4vxjihi5zm84wzkrx7cn5x")))

(define-public crate-embedded_test_harness-0.1.1 (c (n "embedded_test_harness") (v "0.1.1") (d (list (d (n "cortex-m-semihosting") (r "^0.3.7") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.6.0") (d #t) (k 0)))) (h "1637fkwr56b0dpicgf1p2w2zr2w04cbk025yf6k17rcrbzv017ws")))

