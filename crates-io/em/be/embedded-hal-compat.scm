(define-module (crates-io em be embedded-hal-compat) #:use-module (crates-io))

(define-public crate-embedded-hal-compat-0.1.0 (c (n "embedded-hal-compat") (v "0.1.0") (d (list (d (n "eh0_2") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "^1.0.0-alpha.4") (d #t) (k 0) (p "embedded-hal")))) (h "00bnw9kp7svayj1fkvym2mmqh8v0ki4152g2a1l6101a88rfwnck")))

(define-public crate-embedded-hal-compat-0.1.1 (c (n "embedded-hal-compat") (v "0.1.1") (d (list (d (n "eh0_2") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "^1.0.0-alpha.4") (d #t) (k 0) (p "embedded-hal")))) (h "1nmgpyxid2sri2bwa2025aggc9w09kngadksmmvz96jlri66b438")))

(define-public crate-embedded-hal-compat-0.1.2 (c (n "embedded-hal-compat") (v "0.1.2") (d (list (d (n "eh0_2") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "^1.0.0-alpha.4") (d #t) (k 0) (p "embedded-hal")))) (h "0kq204g86g3rq09z5vi0ml73yl2vhy4gsvmmpdp1dhfd1cvr06rc")))

(define-public crate-embedded-hal-compat-0.1.3 (c (n "embedded-hal-compat") (v "0.1.3") (d (list (d (n "eh0_2") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "^1.0.0-alpha.4") (d #t) (k 0) (p "embedded-hal")))) (h "1xxbzif6q25rmhfars14g8pzwdiqpc50dslr4zjysy5fx02bbbx2")))

(define-public crate-embedded-hal-compat-0.2.0 (c (n "embedded-hal-compat") (v "0.2.0") (d (list (d (n "eh0_2") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "^1.0.0-alpha.4") (d #t) (k 0) (p "embedded-hal")))) (h "1mqmf627myid0zzq46dnijh73w2r28bbjvxzkwgwh2b9m1imj1dc")))

(define-public crate-embedded-hal-compat-0.2.1 (c (n "embedded-hal-compat") (v "0.2.1") (d (list (d (n "eh0_2") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "^1.0.0-alpha.4") (d #t) (k 0) (p "embedded-hal")))) (h "0ba53lyvs5rcfh45hnx64ck00mlrd8793gx3m4xay9qf88yvyrx4")))

(define-public crate-embedded-hal-compat-0.3.0 (c (n "embedded-hal-compat") (v "0.3.0") (d (list (d (n "eh0_2") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "^1.0.0-alpha.4") (d #t) (k 0) (p "embedded-hal")))) (h "075dxd2vimn991aalhvialskkx906m5hkjc1q8daj4d4g3xmysvm")))

(define-public crate-embedded-hal-compat-0.4.0 (c (n "embedded-hal-compat") (v "0.4.0") (d (list (d (n "eh0_2") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "^1.0.0-alpha.5") (d #t) (k 0) (p "embedded-hal")))) (h "08mc6gzm8s9sbffhh4ygj4m97s0fbsjr0pvfi2rgi9gxrzywc14f")))

(define-public crate-embedded-hal-compat-0.5.0 (c (n "embedded-hal-compat") (v "0.5.0") (d (list (d (n "eh0_2") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "=1.0.0-alpha.6") (d #t) (k 0) (p "embedded-hal")))) (h "198nh6jfjigkswhi95d19d2j1j9y1w26pcsj86zrw6zp6hirn9p8")))

(define-public crate-embedded-hal-compat-0.6.0 (c (n "embedded-hal-compat") (v "0.6.0") (d (list (d (n "eh0_2") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "=1.0.0-alpha.7") (d #t) (k 0) (p "embedded-hal")))) (h "0pzwd2scnml1lpdlp0s408099w1mbsq9d8bx6346a7163yx0gk41")))

(define-public crate-embedded-hal-compat-0.6.1 (c (n "embedded-hal-compat") (v "0.6.1") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "eh0_2") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "=1.0.0-alpha.7") (d #t) (k 0) (p "embedded-hal")))) (h "1v9zfib54dkdi89zjdhg8sm1yr9l614zny5ni496dfvigjg2d5v2")))

(define-public crate-embedded-hal-compat-0.7.0 (c (n "embedded-hal-compat") (v "0.7.0") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "eh0_2") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "=1.0.0-alpha.8") (d #t) (k 0) (p "embedded-hal")))) (h "1aq0ns68zsdr172rscf83pdzi8gwbcv4j2p5yf2xb1r2s66f3768")))

(define-public crate-embedded-hal-compat-0.6.2 (c (n "embedded-hal-compat") (v "0.6.2") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "eh0_2") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "=1.0.0-alpha.7") (d #t) (k 0) (p "embedded-hal")))) (h "1arm3r6azf0qn7pdvzb2b1gxaryzf1pcq9jpxi96i7ly9ghvk4jz")))

(define-public crate-embedded-hal-compat-0.10.0 (c (n "embedded-hal-compat") (v "0.10.0") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "eh0_2") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "=1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")) (d (n "nb") (r "^1.1") (d #t) (k 0)))) (h "1j002k0yvr6fc190ax84s0dgcy0ylsf1f2bwf2ipk2ffckdfzch9")))

(define-public crate-embedded-hal-compat-0.10.1 (c (n "embedded-hal-compat") (v "0.10.1") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "eh0_2") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "=1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")) (d (n "nb") (r "^1.1") (d #t) (k 0)))) (h "1n2wdfg55j7i6w685p57iliwx08ii378jihmm91598p29hldpv01") (f (quote (("alloc"))))))

(define-public crate-embedded-hal-compat-0.11.0 (c (n "embedded-hal-compat") (v "0.11.0") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "eh0_2") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "=1.0.0-alpha.11") (d #t) (k 0) (p "embedded-hal")) (d (n "nb") (r "^1.1") (d #t) (k 0)))) (h "06n83knlfm57f37nv9z68fcxwz8d1aims5fciv7dij2idr11y23m") (f (quote (("alloc"))))))

(define-public crate-embedded-hal-compat-0.11.1 (c (n "embedded-hal-compat") (v "0.11.1") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "eh0_2") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "=1.0.0-alpha.11") (d #t) (k 0) (p "embedded-hal")) (d (n "nb") (r "^1.1") (d #t) (k 0)))) (h "1h76m86r9axv7k65y4m20l6zmcw6pq6f7jzwk05h9v8qzl0n7vzd") (f (quote (("alloc"))))))

(define-public crate-embedded-hal-compat-0.12.0 (c (n "embedded-hal-compat") (v "0.12.0") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "eh0_2") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "=1.0.0-rc.1") (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-io") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.1") (d #t) (k 0)))) (h "18c2hcfcxnrxjvir1iqxm4aw2ji8ix4bp4vnzabx87iz2d4wgxlc") (s 2) (e (quote (("embedded-io" "dep:embedded-io") ("defmt-03" "dep:defmt" "embedded-io?/defmt-03") ("alloc" "embedded-io?/alloc"))))))

(define-public crate-embedded-hal-compat-0.13.0 (c (n "embedded-hal-compat") (v "0.13.0") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "eh0_2") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "eh1_0") (r "^1.0.0") (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-io") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.1") (d #t) (k 0)))) (h "1wqxx1fi7mr8gw3pk5afk9f2wbjh8myz1j8990nssxdyyrrarv08") (s 2) (e (quote (("embedded-io" "dep:embedded-io") ("defmt-03" "dep:defmt" "embedded-io?/defmt-03") ("alloc" "embedded-io?/alloc"))))))

