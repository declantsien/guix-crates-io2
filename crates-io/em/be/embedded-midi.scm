(define-module (crates-io em be embedded-midi) #:use-module (crates-io))

(define-public crate-embedded-midi-0.0.1 (c (n "embedded-midi") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "0n8z575mla7nwnwcm02jrb5am6cr7x5x21vydga8vd3mcydmbs58")))

(define-public crate-embedded-midi-0.0.2 (c (n "embedded-midi") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1b5c37i67j1gyxapbl4sabfsxig8sxnkq6s1jdkz9clhxwsbyj0v")))

(define-public crate-embedded-midi-0.1.0 (c (n "embedded-midi") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)) (d (n "midi-types") (r "^0.1.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1y3gbs5dvniswjkf7gvgwyrvciraps2njf9rdii31295snr2hypg")))

(define-public crate-embedded-midi-0.1.1 (c (n "embedded-midi") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "midi-types") (r "^0.1.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "117jvyf9v7rz8ds94ghmixj0j17rc3pbjb97g0d719fs3y9713yr")))

(define-public crate-embedded-midi-0.1.2 (c (n "embedded-midi") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "midi-types") (r "^0.1.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "15w4k11l2v0clw9mg7c59f43yapcxri1jrn1v4lnlk4ch8vr6496")))

