(define-module (crates-io em be embeddir) #:use-module (crates-io))

(define-public crate-embeddir-0.1.0 (c (n "embeddir") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (d #t) (k 0)))) (h "1m36450vrpc003l5gw00l4pggi5f57c994s29160yrzmj6v5x3g5")))

(define-public crate-embeddir-0.1.1 (c (n "embeddir") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (d #t) (k 0)))) (h "0fvyi3mbzm0ykxsrwhwmzzspwyvd87qph5nq7nw2cdgws5gaav6c")))

