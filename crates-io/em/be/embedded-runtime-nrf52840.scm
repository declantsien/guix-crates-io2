(define-module (crates-io em be embedded-runtime-nrf52840) #:use-module (crates-io))

(define-public crate-embedded-runtime-nrf52840-0.2.0 (c (n "embedded-runtime-nrf52840") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "embedded-runtime") (r "^0.2.0") (d #t) (k 0)))) (h "1lfggbp6sw25nzmp2jarlsmalqwidjwsr3490l65hsfwq7grz4vq") (f (quote (("default"))))))

(define-public crate-embedded-runtime-nrf52840-0.3.0 (c (n "embedded-runtime-nrf52840") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "embedded-runtime") (r "^0.3.0") (d #t) (k 0)))) (h "0r5ylf485gam622ib7bwnpcygi2qzhhh4lypq7amyvnq5q8v0d90") (f (quote (("default"))))))

