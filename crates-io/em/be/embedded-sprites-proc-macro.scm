(define-module (crates-io em be embedded-sprites-proc-macro) #:use-module (crates-io))

(define-public crate-embedded-sprites-proc-macro-0.1.0 (c (n "embedded-sprites-proc-macro") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0svggi096nywnxcsq4iz5fsq1s5n65038j6jvg2l8ybw0fmr0ny9")))

(define-public crate-embedded-sprites-proc-macro-0.1.1 (c (n "embedded-sprites-proc-macro") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "printing" "full"))) (d #t) (k 0)))) (h "0zhh38910hrdphk6bim7xp1qkqxhaql6igrc9mwmkwxgpf6frrzy")))

(define-public crate-embedded-sprites-proc-macro-0.2.0 (c (n "embedded-sprites-proc-macro") (v "0.2.0") (d (list (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "printing" "full"))) (d #t) (k 0)))) (h "0p785yyvd3d35qx5hnn4dwj3g4z6xlbvadxaz0frhzdpzsrh1a35")))

