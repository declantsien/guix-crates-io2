(define-module (crates-io em be embedded-graphics-core) #:use-module (crates-io))

(define-public crate-embedded-graphics-core-0.1.0 (c (n "embedded-graphics-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "nalgebra") (r "^0.23.0") (o #t) (k 0)))) (h "12yaz221yi8a8xcm6y5j3nx9a4pw8ckycfjc498vfvqnfwzllk0d") (f (quote (("nalgebra_support" "nalgebra") ("default"))))))

(define-public crate-embedded-graphics-core-0.1.1 (c (n "embedded-graphics-core") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "nalgebra") (r "^0.23.0") (o #t) (k 0)))) (h "0aswlzwr8pqznwxbq6sc0disaa6qg74g6q2rq3dwiqbgn5xqjgn6") (f (quote (("nalgebra_support" "nalgebra") ("default"))))))

(define-public crate-embedded-graphics-core-0.2.0 (c (n "embedded-graphics-core") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "nalgebra") (r "^0.23.0") (o #t) (k 0)))) (h "122fv1bniicgjs5r1yp48nqf5cjbp6gsy4d8zl228vhsgy5ns9zp") (f (quote (("nalgebra_support" "nalgebra") ("default"))))))

(define-public crate-embedded-graphics-core-0.3.0 (c (n "embedded-graphics-core") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "nalgebra") (r "^0.23.0") (o #t) (k 0)))) (h "1g7dr040ghrmi99cyrkljprlpjv0v6sx3ni40gs9vhm8788srapg") (f (quote (("nalgebra_support" "nalgebra") ("default"))))))

(define-public crate-embedded-graphics-core-0.3.1 (c (n "embedded-graphics-core") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "nalgebra") (r "^0.23.0") (o #t) (k 0)))) (h "06svwqdkhp992yz81abnkrvl7kdjla5hjljx9v5klqklij3x07ma") (f (quote (("nalgebra_support" "nalgebra") ("default"))))))

(define-public crate-embedded-graphics-core-0.3.2 (c (n "embedded-graphics-core") (v "0.3.2") (d (list (d (n "arrayvec") (r "^0.5.2") (k 2)) (d (n "az") (r "^1.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "nalgebra") (r "^0.23.0") (o #t) (k 0)))) (h "1nmjk5i3w4dwr2p9669zxcibmj025vxqqvsmfnnajaj2skc04fby") (f (quote (("nalgebra_support" "nalgebra") ("default"))))))

(define-public crate-embedded-graphics-core-0.3.3 (c (n "embedded-graphics-core") (v "0.3.3") (d (list (d (n "arrayvec") (r "^0.5.2") (k 2)) (d (n "az") (r "^1.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "nalgebra") (r "^0.23.0") (o #t) (k 0)))) (h "1ykw6cqi8d89d9r1y1vyn4vjaysf06yi1g9m7vivgvpknnfj7cdq") (f (quote (("nalgebra_support" "nalgebra") ("default"))))))

(define-public crate-embedded-graphics-core-0.4.0 (c (n "embedded-graphics-core") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 2)) (d (n "az") (r "^1.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (o #t) (k 0)))) (h "0i50qw5pmj2vnz9na89a86gwsik96zcgc1r21ljmc64r3wkcv7ms") (f (quote (("nalgebra_support" "nalgebra") ("default"))))))

