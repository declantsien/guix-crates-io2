(define-module (crates-io em be embedded-timeout-macros) #:use-module (crates-io))

(define-public crate-embedded-timeout-macros-0.1.0 (c (n "embedded-timeout-macros") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "1vp8xqakdnl32k2mk77j9s9dka1wpgi6kg2b0s0ar6yvvc8am6s2")))

(define-public crate-embedded-timeout-macros-0.2.0 (c (n "embedded-timeout-macros") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "07pjih6vz5gxn1bjis9kyyx0mg7i94cp7f80x6hyai7c91cffaz2")))

(define-public crate-embedded-timeout-macros-0.3.0 (c (n "embedded-timeout-macros") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "1hx1vkbqlrbs9n1wwnmydwfazgrwxgpzwj6rmvisacrsndrry7jz")))

