(define-module (crates-io em be embedded-plots) #:use-module (crates-io))

(define-public crate-embedded-plots-0.1.0 (c (n "embedded-plots") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.6.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.2.1") (d #t) (k 2)) (d (n "heapless") (r "^0.5.6") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "16pkqq5jfrdlh0zarwikky3vkm496jnpir0nqhddq1bhja6g7108")))

(define-public crate-embedded-plots-0.1.1 (c (n "embedded-plots") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.6.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.2.1") (d #t) (k 2)) (d (n "heapless") (r "^0.5.6") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "1myssmzkqd9icyjw7kra1y66abgvqbpwmqc1a6flybf1gp440s2x")))

(define-public crate-embedded-plots-0.1.2 (c (n "embedded-plots") (v "0.1.2") (d (list (d (n "embedded-graphics") (r "^0.6.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.2.1") (d #t) (k 2)) (d (n "heapless") (r "^0.5.6") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "1iz74rjxc0y9i8ly1l0l01fwm577wvqanv64nzv40dp8hci7c0n3")))

(define-public crate-embedded-plots-0.2.0 (c (n "embedded-plots") (v "0.2.0") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0") (d #t) (k 2)) (d (n "heapless") (r "^0.7.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "0sfn29sns95z9a9jbd4028bmxvcmw2bsja4drr2js8vm5c4wjmfq")))

