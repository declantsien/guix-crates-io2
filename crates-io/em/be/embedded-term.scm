(define-module (crates-io em be embedded-term) #:use-module (crates-io))

(define-public crate-embedded-term-0.1.0 (c (n "embedded-term") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.3") (d #t) (k 2)) (d (n "embedded-graphics-simulator") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 2)) (d (n "pty") (r "^0.2") (d #t) (k 2)) (d (n "termios") (r "^0.3") (d #t) (k 2)) (d (n "vte") (r "^0.10") (d #t) (k 0)))) (h "00pw519la64sc30jgnv53pz5vjllnl5m06qkdb38ppik34x02vl9") (f (quote (("default" "log"))))))

