(define-module (crates-io em be embedded-mogeefont) #:use-module (crates-io))

(define-public crate-embedded-mogeefont-0.1.0 (c (n "embedded-mogeefont") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "embedded-text") (r "^0.7.1") (d #t) (k 2)))) (h "0cmn1xlmcrdmmvhiq0cm1lhspd1z5vhnsag9j7hkjvpgi97wfddy")))

