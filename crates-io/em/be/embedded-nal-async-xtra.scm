(define-module (crates-io em be embedded-nal-async-xtra) #:use-module (crates-io))

(define-public crate-embedded-nal-async-xtra-0.1.0 (c (n "embedded-nal-async-xtra") (v "0.1.0") (d (list (d (n "embedded-io-async") (r "^0.6") (k 0)) (d (n "embedded-nal-async") (r "^0.7") (k 0)))) (h "0slqpv5p4w3kp33siwkw46896w0lcx0jfrhjpvhdz713jpcn1aai") (r "1.75")))

(define-public crate-embedded-nal-async-xtra-0.2.0 (c (n "embedded-nal-async-xtra") (v "0.2.0") (d (list (d (n "embedded-io-async") (r "^0.6") (k 0)) (d (n "embedded-nal-async") (r "^0.7") (k 0)))) (h "19vbr7gaiajvkqwswkmk5fb5724sb9gwja96qni3qx6kjphx3nhy") (r "1.75")))

