(define-module (crates-io em be embed-rust-in-rust) #:use-module (crates-io))

(define-public crate-embed-rust-in-rust-1.0.0 (c (n "embed-rust-in-rust") (v "1.0.0") (h "0pxxz088v9102p9402wary13raprci182c633cds6zm2z21kryzg")))

(define-public crate-embed-rust-in-rust-999.0.0 (c (n "embed-rust-in-rust") (v "999.0.0") (h "1q7ggwa7p374p5r4mx9wwg99dq754wxgxi71104wnx9hkv1gcy5l")))

