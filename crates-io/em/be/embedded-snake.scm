(define-module (crates-io em be embedded-snake) #:use-module (crates-io))

(define-public crate-embedded-snake-0.0.1 (c (n "embedded-snake") (v "0.0.1") (d (list (d (n "embedded-graphics") (r "^0.7.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "1h4c21ib6a3f6i0h6wv8jl93gwirh2iwzjkfcsf22wcid7czkxxq")))

(define-public crate-embedded-snake-0.0.2 (c (n "embedded-snake") (v "0.0.2") (d (list (d (n "embedded-graphics") (r "^0.7.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "1f5vxbvnrs60dppqm9bnqwz30sd0353aj5g6bfwjd7431g98lndg")))

