(define-module (crates-io em be embed-wasm-build) #:use-module (crates-io))

(define-public crate-embed-wasm-build-0.1.0-alpha (c (n "embed-wasm-build") (v "0.1.0-alpha") (d (list (d (n "cargo-web") (r "=0.6.26") (d #t) (k 0)) (d (n "ignore") (r "^0.4.14") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1awvwxhf472iwz0674zjf05cbcq7cd1xayv11n4whbafw586pxvv")))

(define-public crate-embed-wasm-build-0.1.0 (c (n "embed-wasm-build") (v "0.1.0") (d (list (d (n "cargo-web") (r "=0.6.26") (d #t) (k 0)) (d (n "ignore") (r "^0.4.14") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "04wk68xdrg82sd3283v01xh4v61dx3g9w605njnl4z4403rfp7hc")))

(define-public crate-embed-wasm-build-0.1.1 (c (n "embed-wasm-build") (v "0.1.1") (d (list (d (n "cargo-web") (r "=0.6.26") (d #t) (k 0)) (d (n "ignore") (r "^0.4.14") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0gp40vl98rkmri9f9x4y81jjkcxy238rnj76a00l59jyxq69pi9d")))

