(define-module (crates-io em be embed_js_derive) #:use-module (crates-io))

(define-public crate-embed_js_derive-0.1.0 (c (n "embed_js_derive") (v "0.1.0") (d (list (d (n "cpp_syn") (r "^0.12.0") (f (quote ("full" "parsing"))) (d #t) (k 0)) (d (n "embed_js_common") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)))) (h "1w15ryz4r58fwvwcgy2fkq0jwgaih09xhvsqhnhsq0sii9741fvv")))

(define-public crate-embed_js_derive-0.1.1 (c (n "embed_js_derive") (v "0.1.1") (d (list (d (n "cpp_syn") (r "^0.12.0") (f (quote ("full" "parsing"))) (d #t) (k 0)) (d (n "embed_js_common") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)))) (h "1b590ka3qr7bi6zl24q8j9gv0simmir69yvnf5qwx5nf9pvzx6fj")))

(define-public crate-embed_js_derive-0.1.2 (c (n "embed_js_derive") (v "0.1.2") (d (list (d (n "cpp_syn") (r "^0.12.0") (f (quote ("full" "parsing"))) (d #t) (k 0)) (d (n "embed_js_common") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)))) (h "11lckmrvap11s0hc6ipy8032g9b6j4dc0ln6kxlw6rnd4081rk9c")))

