(define-module (crates-io em be embed_plist) #:use-module (crates-io))

(define-public crate-embed_plist-1.0.0 (c (n "embed_plist") (v "1.0.0") (h "1hmv23pbs8f85ishy4q4izz7dl3zcvdznpgwk3gyqd11iq80a397")))

(define-public crate-embed_plist-1.1.0 (c (n "embed_plist") (v "1.1.0") (h "15lh64bs9chl8c0487j8vbppyali6csdhw08315rjbk8155pz56i")))

(define-public crate-embed_plist-1.2.0 (c (n "embed_plist") (v "1.2.0") (h "16fw2gzjsdny8nqfmrc41ri5hf8qfl6ywha10nk54afklx1jxpak")))

(define-public crate-embed_plist-1.2.1 (c (n "embed_plist") (v "1.2.1") (h "0y2m43fdn262m34wir9kq4sxmh7wa6xvq1wzr0wd2j4ajddm48h6")))

(define-public crate-embed_plist-1.2.2 (c (n "embed_plist") (v "1.2.2") (h "1drvfi1lh9lh8b7pcqp98jdigzsji0kfavbrv126c69pbfgbixjf")))

