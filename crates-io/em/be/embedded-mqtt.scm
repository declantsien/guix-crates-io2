(define-module (crates-io em be embedded-mqtt) #:use-module (crates-io))

(define-public crate-embedded-mqtt-0.1.0 (c (n "embedded-mqtt") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "1nfxg3wy60pr73hgbk005ngnjlxsxg36gvacgmv7ddfj7ivp277p") (f (quote (("std" "byteorder/std"))))))

