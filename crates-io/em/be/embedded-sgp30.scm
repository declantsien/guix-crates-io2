(define-module (crates-io em be embedded-sgp30) #:use-module (crates-io))

(define-public crate-embedded-sgp30-0.1.0 (c (n "embedded-sgp30") (v "0.1.0") (d (list (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4.0") (d #t) (k 2)) (d (n "micromath") (r "^2.1.0") (d #t) (k 0)))) (h "1lfq0ddhmkbw589rq2ggh81dhn70maxps5l8bxllhbqivw7p1ynp")))

