(define-module (crates-io em be embed_js) #:use-module (crates-io))

(define-public crate-embed_js-0.1.0 (c (n "embed_js") (v "0.1.0") (d (list (d (n "embed_js_derive") (r "^0.1") (d #t) (k 0)))) (h "10zhhbw0a2nx716fjwrzbxzr17895xgabn1s8d9srghvz72wyp53")))

(define-public crate-embed_js-0.1.1 (c (n "embed_js") (v "0.1.1") (d (list (d (n "embed_js_derive") (r "^0.1") (d #t) (k 0)))) (h "19sfmi7ajn6jvpc089xbjra6bl0afp74cgwzf7xvdlq402rw60s5")))

(define-public crate-embed_js-0.1.2 (c (n "embed_js") (v "0.1.2") (d (list (d (n "embed_js_derive") (r "^0.1") (d #t) (k 0)))) (h "053ic1v7nmvl9fxis8dl97qh7apnsvhyz2400vgfh7zj7vwmxxfa")))

(define-public crate-embed_js-0.1.3 (c (n "embed_js") (v "0.1.3") (d (list (d (n "embed_js_derive") (r "^0.1.1") (d #t) (k 0)))) (h "10gh1i8mxydvkxqafq3y7rdb0cc1avqblndv17y9639m9jsjrmfm")))

(define-public crate-embed_js-0.1.4 (c (n "embed_js") (v "0.1.4") (d (list (d (n "embed_js_derive") (r "^0.1.2") (d #t) (k 0)))) (h "034s6addf3zbx7a122hw7x2y6agg4iv5fmxc81fgjqh6gjc1xypj")))

