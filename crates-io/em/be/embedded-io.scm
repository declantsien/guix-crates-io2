(define-module (crates-io em be embedded-io) #:use-module (crates-io))

(define-public crate-embedded-io-0.1.0 (c (n "embedded-io") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (o #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "02w2wjygc21kxx8jbr1hmi68czqqx8184qaxk5cb75lb9d5rm952") (y #t) (s 2) (e (quote (("std" "futures?/std") ("async" "dep:futures"))))))

(define-public crate-embedded-io-0.2.0 (c (n "embedded-io") (v "0.2.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (o #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "1x3a1v7wcd6vg92jxj288x97742qxflvbc5842swpwjzlwn5gqbz") (s 2) (e (quote (("std" "futures?/std") ("async" "dep:futures"))))))

(define-public crate-embedded-io-0.3.0 (c (n "embedded-io") (v "0.3.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (o #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)))) (h "1iy1dknahi9zs40riarrczzn9xgz5h5wmbh07w7frx2ghiwknrrn") (f (quote (("async") ("alloc")))) (s 2) (e (quote (("tokio" "std" "async" "dep:tokio") ("std" "alloc" "futures?/std") ("futures" "std" "async" "dep:futures"))))))

(define-public crate-embedded-io-0.3.1 (c (n "embedded-io") (v "0.3.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (o #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)))) (h "1xdwh6af2lhidcsjx5w6dkk4an9lly03g53wagc2ygqidjgbmprk") (f (quote (("async") ("alloc")))) (s 2) (e (quote (("tokio" "std" "async" "dep:tokio") ("std" "alloc" "futures?/std") ("futures" "std" "async" "dep:futures"))))))

(define-public crate-embedded-io-0.4.0 (c (n "embedded-io") (v "0.4.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (o #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)))) (h "1v9wrc5nsgaaady7i3ya394sik5251j0iq5rls7mrx7fv696h6pg") (f (quote (("async") ("alloc")))) (s 2) (e (quote (("tokio" "std" "async" "dep:tokio") ("std" "alloc" "futures?/std") ("futures" "std" "async" "dep:futures"))))))

(define-public crate-embedded-io-0.5.0 (c (n "embedded-io") (v "0.5.0") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")))) (h "0q20x5fi2gr0hc02zdw7fkmmdh0iah7wpw02mswqdhldcbfbm2v5") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-embedded-io-0.6.0 (c (n "embedded-io") (v "0.6.0") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")))) (h "0wff9vnawka3krqwk45flzwyg4pcqqz5xclmbz1a4h2lid3ig57s") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("defmt-03" "dep:defmt-03"))))))

(define-public crate-embedded-io-0.6.1 (c (n "embedded-io") (v "0.6.1") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")))) (h "0v901xykajh3zffn6x4cnn4fhgfw3c8qpjwbsk6gai3gaccg3l7d") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("defmt-03" "dep:defmt-03"))))))

