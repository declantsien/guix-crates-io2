(define-module (crates-io em be embedded-async-helpers) #:use-module (crates-io))

(define-public crate-embedded-async-helpers-0.1.0 (c (n "embedded-async-helpers") (v "0.1.0") (d (list (d (n "atomic-polyfill") (r "^1") (d #t) (k 0)) (d (n "critical-section") (r "^1") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "1v96s5y59g24ygig0zz8vdygn3m38ildp4d81fmk0lh6s41j7dbf")))

(define-public crate-embedded-async-helpers-0.1.1 (c (n "embedded-async-helpers") (v "0.1.1") (d (list (d (n "atomic-polyfill") (r "^1") (d #t) (k 0)) (d (n "critical-section") (r "^1") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "1k6hgv4xv3yzczf2iaiwgmwl92by0sikc682k9qd65qvw3i08pz3")))

(define-public crate-embedded-async-helpers-0.1.2 (c (n "embedded-async-helpers") (v "0.1.2") (d (list (d (n "atomic-polyfill") (r "^1") (d #t) (k 0)) (d (n "critical-section") (r "^1") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "12j87pjwbpm1chc4ym8x12cbp2cxxc820nh8j9iirh29s753bhzc")))

(define-public crate-embedded-async-helpers-0.1.3 (c (n "embedded-async-helpers") (v "0.1.3") (d (list (d (n "atomic-polyfill") (r "^1") (d #t) (k 0)) (d (n "critical-section") (r "^1") (d #t) (k 0)))) (h "0g5kbk8lkr4jz8nlnnw9j70nvdp74x56fxxxjdw0zs1bhnfcbd05")))

