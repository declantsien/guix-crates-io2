(define-module (crates-io em be embed_staticfile) #:use-module (crates-io))

(define-public crate-embed_staticfile-0.1.0 (c (n "embed_staticfile") (v "0.1.0") (d (list (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "include_dir_bytes") (r "^0.1") (d #t) (k 2)) (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "iron-test") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "router") (r "^0.5") (d #t) (k 2)))) (h "0r6hgnjkjrnjxyg298lbc1j9d919pc7wa6an0n7b2d911p8h249g")))

(define-public crate-embed_staticfile-0.2.0 (c (n "embed_staticfile") (v "0.2.0") (d (list (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "include_dir_bytes") (r "^0.2") (d #t) (k 2)) (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "iron-test") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "router") (r "^0.5") (d #t) (k 2)))) (h "0k389fnarb8njgyl0w3m6nd6gin2hrll325r8k8y07g8aln6w3qy")))

