(define-module (crates-io em be embed-wasm) #:use-module (crates-io))

(define-public crate-embed-wasm-0.1.0-alpha (c (n "embed-wasm") (v "0.1.0-alpha") (d (list (d (n "headers") (r "^0.3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)))) (h "1mqszg3qa19hs13m3amfr3iivk1cw355c3j1xy19h38f25d21cq4")))

(define-public crate-embed-wasm-0.1.0 (c (n "embed-wasm") (v "0.1.0") (d (list (d (n "headers") (r "^0.3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)))) (h "0zggm42hfpyg44cvnqkxvidlh8g7pwbgsrrmx9ry14r8sc12zxqc")))

(define-public crate-embed-wasm-0.1.1 (c (n "embed-wasm") (v "0.1.1") (d (list (d (n "headers") (r "^0.3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)))) (h "0bzldqj0clnb7wjam7sd5xjwqx7y4l4gsdi1lxjp2zrhipcbri6h")))

