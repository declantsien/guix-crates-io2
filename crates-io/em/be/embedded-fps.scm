(define-module (crates-io em be embedded-fps) #:use-module (crates-io))

(define-public crate-embedded-fps-0.1.0 (c (n "embedded-fps") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.7") (d #t) (k 2)) (d (n "embedded-graphics-simulator") (r "^0.3") (d #t) (k 2)) (d (n "embedded-time") (r "^0.12") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (k 0)))) (h "0yai6c9dqana8pxpn3m80r7pdvdhnpfimk8m46ciaj9fj56qpnjs") (f (quote (("std") ("default")))) (r "1.56")))

