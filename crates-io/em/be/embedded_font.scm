(define-module (crates-io em be embedded_font) #:use-module (crates-io))

(define-public crate-embedded_font-0.1.0 (c (n "embedded_font") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0-alpha.2") (d #t) (k 2)) (d (n "embedded-text") (r "^0.4.0") (d #t) (k 2)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "0ajpmmvs35y094v5a5gjd3gdpr4mdng0f0kr20xb12rhxi4w03kz") (f (quote (("std" "rusttype/std") ("default"))))))

(define-public crate-embedded_font-0.1.1 (c (n "embedded_font") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0-alpha.2") (d #t) (k 2)) (d (n "embedded-text") (r "^0.4.0") (d #t) (k 2)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1mkrrxc0qc3m4h9i9rarkf6psivqida90czyiz81dg8ibc0mdz0q") (f (quote (("std" "rusttype/std") ("default"))))))

(define-public crate-embedded_font-0.1.2 (c (n "embedded_font") (v "0.1.2") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0-alpha.2") (d #t) (k 2)) (d (n "embedded-text") (r "^0.4.0") (d #t) (k 2)) (d (n "rusttype") (r "^0.9.2") (k 0)))) (h "12ns6s1xaccll5i51wccimfzaspzl051iizm38l8c3y5rpp0c9wg") (f (quote (("std" "rusttype/std") ("default" "rusttype/libm-math"))))))

(define-public crate-embedded_font-0.1.3 (c (n "embedded_font") (v "0.1.3") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0-alpha.2") (d #t) (k 2)) (d (n "embedded-text") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "rusttype") (r "^0.9.2") (k 0)))) (h "1mn8z28hq77dw50q6a0hp1hs83g959qhj161fy7czm8wy61fr2am") (f (quote (("std" "rusttype/std" "num-traits/default") ("default" "rusttype/libm-math" "num-traits/libm"))))))

