(define-module (crates-io em be embedded-hal-fuzz) #:use-module (crates-io))

(define-public crate-embedded-hal-fuzz-0.1.0 (c (n "embedded-hal-fuzz") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "13r6svpj5iwv9z31509kvzi1wj99m5sc87qyzjcfpf1nhxj49rpf")))

(define-public crate-embedded-hal-fuzz-0.1.1 (c (n "embedded-hal-fuzz") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "10fjfm9x4594634szaxa3aikd3x6y8vc2cqa2np77cc9nki2ii55")))

(define-public crate-embedded-hal-fuzz-0.1.2 (c (n "embedded-hal-fuzz") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "0nism8ifs8jkisiwd50p2315bhj9ybrn0rl0iii8iq0qpgznr39q")))

(define-public crate-embedded-hal-fuzz-1.0.0-rc.1 (c (n "embedded-hal-fuzz") (v "1.0.0-rc.1") (d (list (d (n "arbitrary") (r "^1.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1vnsxvyhqq8s90b8wr49a3sh4a5vmjfz08jqcsvf6c1vxamawcah")))

(define-public crate-embedded-hal-fuzz-1.0.0 (c (n "embedded-hal-fuzz") (v "1.0.0") (d (list (d (n "arbitrary") (r "^1.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "18jbkzdj1wclxpmc64rxya0yxnwag3z0mqqpdk6dcjzw68cgxm7d")))

