(define-module (crates-io em be embedded-gfx) #:use-module (crates-io))

(define-public crate-embedded-gfx-0.1.0 (c (n "embedded-gfx") (v "0.1.0") (d (list (d (n "embedded-graphics-core") (r "^0.4.0") (d #t) (k 0)) (d (n "line_drawing") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm"))) (k 0)))) (h "0g77z235ia0sxvnknys59p533da0rw7q22yv15xllswzihb3z35l")))

