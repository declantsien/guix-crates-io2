(define-module (crates-io em be embedded-dma) #:use-module (crates-io))

(define-public crate-embedded-dma-0.1.0 (c (n "embedded-dma") (v "0.1.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "167n108k5drqbf26h5nldhbj7rsqsksxhwp4nn93wi4c4pyp6a65")))

(define-public crate-embedded-dma-0.1.1 (c (n "embedded-dma") (v "0.1.1") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "0xjvnm9cvaygs3ji6jkq02fvf45w19piyk3457a7cajgvwx0j9ff")))

(define-public crate-embedded-dma-0.1.2 (c (n "embedded-dma") (v "0.1.2") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "195xy4vs70d321h4qirjq945jk3z059cj4q8lry2d8278cpc1j26")))

(define-public crate-embedded-dma-0.2.0 (c (n "embedded-dma") (v "0.2.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "0ijld5jblcka4b95s1lwxd9k109nyaap34h44g122ddjbidpwkwr")))

