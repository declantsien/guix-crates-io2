(define-module (crates-io em be embedded-cv) #:use-module (crates-io))

(define-public crate-embedded-cv-0.1.0 (c (n "embedded-cv") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.22.0") (d #t) (k 0)) (d (n "tflite") (r "^0.6") (d #t) (k 0)))) (h "1qxzmvmwqkzmyf331z534g8lmn2r353avnzljbcpwgjhq37vj6cg")))

