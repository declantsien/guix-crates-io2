(define-module (crates-io em be embedded-counters) #:use-module (crates-io))

(define-public crate-embedded-counters-0.1.0 (c (n "embedded-counters") (v "0.1.0") (d (list (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "st7789") (r "^0.6") (d #t) (k 2)))) (h "0y77jh522rswi9lq9pcsg5a3gmg5pgywvxj478j7qzrpijw84h0i")))

(define-public crate-embedded-counters-0.1.1 (c (n "embedded-counters") (v "0.1.1") (d (list (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "st7789") (r "^0.6") (d #t) (k 2)))) (h "1sry9rmsrlvgjv9cic7139sgcgwlbj3yhbmvz2byy7zrqczdm0xy")))

(define-public crate-embedded-counters-0.1.2 (c (n "embedded-counters") (v "0.1.2") (d (list (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "st7789") (r "^0.6") (d #t) (k 2)))) (h "0z4zc62sdp8yk02whb5rbfywlklh1x0qpcy6xdimj3vycy4pds1f")))

