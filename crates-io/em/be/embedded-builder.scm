(define-module (crates-io em be embedded-builder) #:use-module (crates-io))

(define-public crate-embedded-builder-0.1.0 (c (n "embedded-builder") (v "0.1.0") (h "1gcp64bcyh4qw1f08php2fy2nx3a483ip5pgbjn3hdwm9km3h779")))

(define-public crate-embedded-builder-0.1.1 (c (n "embedded-builder") (v "0.1.1") (h "1sbxg6fbn24c60aq97p72i8zbrn65ci4192nyl41kvg6a3zql32x")))

