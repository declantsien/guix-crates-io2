(define-module (crates-io em be embeddy) #:use-module (crates-io))

(define-public crate-embeddy-0.1.0 (c (n "embeddy") (v "0.1.0") (h "1x6wdc3xkrz7vwx6qw35al4b6b74z1r6s6nav75iwfdhynak0wad")))

(define-public crate-embeddy-0.1.1 (c (n "embeddy") (v "0.1.1") (d (list (d (n "embeddy-derive") (r "^0.1") (d #t) (k 0)))) (h "10rjv23fzchmci5jiir1cbldigvx5xlfzxflx2fp4xl733llpl70")))

