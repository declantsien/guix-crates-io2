(define-module (crates-io em be embeddy-derive) #:use-module (crates-io))

(define-public crate-embeddy-derive-0.1.0 (c (n "embeddy-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("proc-macro" "parsing" "derive"))) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1lqb2f9bxy6liqrr994g12lvmknrb66prkpdpwdk8rsfb05sn3xz")))

