(define-module (crates-io em be embedded-ccs811) #:use-module (crates-io))

(define-public crate-embedded-ccs811-0.1.0 (c (n "embedded-ccs811") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "1yff7ax34s1mrcy9niz06va97yw88faaqn0lgsg3dbxmdl9bng5i")))

(define-public crate-embedded-ccs811-0.2.0 (c (n "embedded-ccs811") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "109a4cgxijzrfx7jb9glx0i948bx3qvjv0zd3fhncfcs9j29rf2j")))

