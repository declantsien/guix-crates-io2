(define-module (crates-io em be embedded-crc-macros) #:use-module (crates-io))

(define-public crate-embedded-crc-macros-0.1.0 (c (n "embedded-crc-macros") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "10dnq6zkdi8prayxc63cbrdfjvk3kp2m83pb25f3yg2d5a45k1mx")))

(define-public crate-embedded-crc-macros-1.0.0 (c (n "embedded-crc-macros") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0b3nksyzfjqcdnx4pxdbw0jhgg4hjn42myw73bgqdc23g9s7a72g")))

