(define-module (crates-io em be embedded-nal) #:use-module (crates-io))

(define-public crate-embedded-nal-0.1.0 (c (n "embedded-nal") (v "0.1.0") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "no-std-net") (r "^0.4") (d #t) (k 0)))) (h "067bng8isyr210v759zk8z6d42jvzxbxc8gas3cpcnny08dynimf")))

(define-public crate-embedded-nal-0.2.0 (c (n "embedded-nal") (v "0.2.0") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "no-std-net") (r "^0.4") (d #t) (k 0)))) (h "0p3f86j0bbbk8yfy3v9s67s0dw5hjkp83wc8vh05jaray1j8nhwl")))

(define-public crate-embedded-nal-0.3.0 (c (n "embedded-nal") (v "0.3.0") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "no-std-net") (r "^0.4") (d #t) (k 0)))) (h "1wgibwn9mxqddpykmjdqmhjrcymi1ddzxn31a65fdgv0azmk2vxb")))

(define-public crate-embedded-nal-0.4.0 (c (n "embedded-nal") (v "0.4.0") (d (list (d (n "heapless") (r "^0.6.1") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "no-std-net") (r "^0.5") (d #t) (k 0)))) (h "1sylln74rw1psqs02w3dls6x3iim58a4i00fkinm7fq6ggcx2p5z")))

(define-public crate-embedded-nal-0.5.0 (c (n "embedded-nal") (v "0.5.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "no-std-net") (r "^0.5") (d #t) (k 0)))) (h "0cxzx1ydi1chs17r6rjmv74awk85dynajd6091n98vdgb4vl6hlb")))

(define-public crate-embedded-nal-0.6.0 (c (n "embedded-nal") (v "0.6.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "no-std-net") (r "^0.5") (d #t) (k 0)))) (h "04xi83lbw6mqxizkb88gqmlp2r1pgm6qgwihhy8zlm5baz5zx7nv")))

(define-public crate-embedded-nal-0.7.0 (c (n "embedded-nal") (v "0.7.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "no-std-net") (r "^0.6") (o #t) (d #t) (k 0)))) (h "19vfz6a7w3k4qnkpjdlinh222vbx4qdv30iyq618fdxsc78icx24") (f (quote (("ip_in_core") ("default" "no-std-net"))))))

(define-public crate-embedded-nal-0.8.0 (c (n "embedded-nal") (v "0.8.0") (d (list (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "no-std-net") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1k0rvhcb6pjkfy2c73pj1mziw7rpp9mhzs7i0253yggdspx47adq") (f (quote (("ip_in_core") ("default" "no-std-net"))))))

