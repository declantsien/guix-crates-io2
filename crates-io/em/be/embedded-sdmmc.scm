(define-module (crates-io em be embedded-sdmmc) #:use-module (crates-io))

(define-public crate-embedded-sdmmc-0.1.0 (c (n "embedded-sdmmc") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "01388vy83b4yyv4m119z9qah6hd2d6sn973bqn7c5f9hj2r0r2fx")))

(define-public crate-embedded-sdmmc-0.1.1 (c (n "embedded-sdmmc") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "0mz29pvgxx413mdx4rclwnhkbm114bs0d5h64zvsmrci91sbl1zc")))

(define-public crate-embedded-sdmmc-0.2.0 (c (n "embedded-sdmmc") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "0wgqy7msi6gwyfa3fgzf1v75dyyfmy14x0g6dvsh6qr6vn5anfj5")))

(define-public crate-embedded-sdmmc-0.2.1 (c (n "embedded-sdmmc") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "0z0k32gg4d78prxwd1kmrxfm60kf86afcg2dpfbw80wr0652zap0")))

(define-public crate-embedded-sdmmc-0.3.0 (c (n "embedded-sdmmc") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "0yhyxb5sf9k8j8lx2mlvqgz10k8y2c7jkn99lglqgjxynnig0fvd")))

(define-public crate-embedded-sdmmc-0.4.0 (c (n "embedded-sdmmc") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "0016dxkxxjj57a0mxhxxbg2rl28gjvjfp0hzqfqzzs5g4y6y6jiv") (f (quote (("defmt-log" "defmt") ("default" "log"))))))

(define-public crate-embedded-sdmmc-0.5.0 (c (n "embedded-sdmmc") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "0dip22mydq2cbsfxaqmrlc2injcpki7il3jslhjaza3n18c18k9g") (f (quote (("defmt-log" "defmt") ("default" "log"))))))

(define-public crate-embedded-sdmmc-0.6.0 (c (n "embedded-sdmmc") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "sha256") (r "^1.4") (d #t) (k 2)))) (h "0sp03zavjh2h97pzq8kfbi7i4qxn1pv887cf0j0nppgy92g0jy1p") (f (quote (("defmt-log" "defmt") ("default" "log"))))))

(define-public crate-embedded-sdmmc-0.7.0 (c (n "embedded-sdmmc") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-bus") (r "^0.1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)))) (h "1n00jgrs8hxnryy5bpin35yb36vr0i6k7g2j2lr0n7qw7yzqslns") (f (quote (("default" "log")))) (s 2) (e (quote (("log" "dep:log") ("defmt-log" "dep:defmt"))))))

