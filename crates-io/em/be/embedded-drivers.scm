(define-module (crates-io em be embedded-drivers) #:use-module (crates-io))

(define-public crate-embedded-drivers-0.0.1 (c (n "embedded-drivers") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)))) (h "0lcls0fpm9vp1nmfgicg8nnl4jmfimpvpya03wxis7xf2v2pqj7q")))

(define-public crate-embedded-drivers-0.0.2 (c (n "embedded-drivers") (v "0.0.2") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)))) (h "0zdfj1nknyj913srd5kd9sj6j8h0g60j3680bgx0941mah60bbfp")))

