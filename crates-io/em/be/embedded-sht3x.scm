(define-module (crates-io em be embedded-sht3x) #:use-module (crates-io))

(define-public crate-embedded-sht3x-0.1.0 (c (n "embedded-sht3x") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4.0") (d #t) (k 2)) (d (n "micromath") (r "^2.1.0") (d #t) (k 0)))) (h "1lv84bj6c20l6db6kkfyc9i7mhaznr0mgh3ylnm7r3ziha051m4b")))

