(define-module (crates-io em be embedded-flight) #:use-module (crates-io))

(define-public crate-embedded-flight-0.1.0 (c (n "embedded-flight") (v "0.1.0") (d (list (d (n "embedded-flight-control") (r "^0.1.0") (d #t) (k 0)) (d (n "embedded-flight-motors") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)))) (h "1dd89n979494mg76aa64m99s50i9daa8srijxrjcqb67qyqiwsgg") (y #t)))

(define-public crate-embedded-flight-0.1.1 (c (n "embedded-flight") (v "0.1.1") (d (list (d (n "embedded-flight-control") (r "^0.1.0") (d #t) (k 0)) (d (n "embedded-flight-motors") (r "^0.1.0") (d #t) (k 0)) (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)))) (h "1k4vrmj8slsfmpw6jrp52swhy8lacky65szjqixagg0vm5w97khg") (y #t)))

(define-public crate-embedded-flight-0.1.2 (c (n "embedded-flight") (v "0.1.2") (d (list (d (n "embedded-flight-control") (r "^0.1.0") (d #t) (k 0)) (d (n "embedded-flight-motors") (r "^0.1.0") (d #t) (k 0)) (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)))) (h "0adr7i0bhxi8fhkspy85z2wcl6hqrlxy0skv793bwm5k4vg6kf5w") (y #t)))

(define-public crate-embedded-flight-0.2.0 (c (n "embedded-flight") (v "0.2.0") (d (list (d (n "embedded-flight-control") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-flight-core") (r "^0.1.0") (d #t) (k 0)) (d (n "embedded-flight-motors") (r "^0.1.1") (d #t) (k 0)) (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)))) (h "0zfm88d965569v8jal853760aqm1zcb40dahmnli6zlk9m6n26mk") (y #t)))

(define-public crate-embedded-flight-0.2.1 (c (n "embedded-flight") (v "0.2.1") (d (list (d (n "embedded-flight-control") (r "^0.2.1") (d #t) (k 0)) (d (n "embedded-flight-core") (r "^0.1.1") (d #t) (k 0)) (d (n "embedded-flight-motors") (r "^0.1.2") (d #t) (k 0)) (d (n "embedded-flight-scheduler") (r "^0.1.0") (d #t) (k 0)) (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "04zydxfh5nwi617ksnzqrg9p193grjypkd52554djw6rxrxdfgrn") (y #t)))

(define-public crate-embedded-flight-0.3.0 (c (n "embedded-flight") (v "0.3.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "pid-controller") (r "^0.1.0") (d #t) (k 0)))) (h "0cjn8r6lgjl7hwk0bjj73s1z4akgwi0i9wxxqabq4dq099k52q55") (y #t)))

(define-public crate-embedded-flight-0.3.1 (c (n "embedded-flight") (v "0.3.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "pid-controller") (r "^0.1.0") (d #t) (k 0)) (d (n "std-embedded-time") (r "^0.1.0") (d #t) (k 2)))) (h "1x18d4g1gj3pfdmrkzsvr7rh0l9nvp0kdl70m5mxgkckh77p8hmx") (y #t)))

