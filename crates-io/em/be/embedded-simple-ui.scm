(define-module (crates-io em be embedded-simple-ui) #:use-module (crates-io))

(define-public crate-embedded-simple-ui-1.0.0 (c (n "embedded-simple-ui") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)))) (h "0d6sa6xpjgnbg25ri22gsk1z3v5a1jyj7i6454b85ywydadbsa1l") (y #t) (r "1.71.0")))

(define-public crate-embedded-simple-ui-1.0.1 (c (n "embedded-simple-ui") (v "1.0.1") (d (list (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-time") (r "^0.12.1") (d #t) (k 0)))) (h "0q6gnxp5ds8ka04np1mn50j1sb3pcpyczns6fbr4sc6r8b4zncmn") (r "1.71.0")))

