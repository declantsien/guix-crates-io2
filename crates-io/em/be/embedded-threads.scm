(define-module (crates-io em be embedded-threads) #:use-module (crates-io))

(define-public crate-embedded-threads-0.1.0 (c (n "embedded-threads") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.5.0") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.6.0") (f (quote ("exit"))) (d #t) (k 0)) (d (n "riot-rs-runqueue") (r "0.1.*") (d #t) (k 0)))) (h "1idrls9x5nmfvljd1zhlsm362ssd1nfqvvkgxiyc592g5938nqjy")))

