(define-module (crates-io em be embedded-semver) #:use-module (crates-io))

(define-public crate-embedded-semver-0.1.0 (c (n "embedded-semver") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.22.1") (d #t) (k 0)))) (h "01bi39bfkd8207lm9n73yasnqk3f7pfb5bjvmgf6vblwgbsq14yv") (f (quote (("std") ("default" "std"))))))

(define-public crate-embedded-semver-0.2.0 (c (n "embedded-semver") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)))) (h "13b6ja6a9j74fa5wa9j8rqxfi234i0l8n8vkrqg8mzha1cgsbwfk") (f (quote (("std") ("default" "std")))) (r "1.51")))

(define-public crate-embedded-semver-0.3.0 (c (n "embedded-semver") (v "0.3.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)))) (h "09iy0d9dznjk5ygc8i5xb897k6n3b1ll0cchkidfnvf78h6k5fgs") (f (quote (("std") ("default" "std")))) (r "1.51")))

