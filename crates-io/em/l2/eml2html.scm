(define-module (crates-io em l2 eml2html) #:use-module (crates-io))

(define-public crate-eml2html-0.1.0 (c (n "eml2html") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eml-parser") (r "^0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.11") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.5.0") (d #t) (k 0)))) (h "17a83c8gw9q4y7y8b50yx29yndd0xc9my3q2mb69z932ih5zj33p")))

