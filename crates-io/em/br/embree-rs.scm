(define-module (crates-io em br embree-rs) #:use-module (crates-io))

(define-public crate-embree-rs-0.1.1 (c (n "embree-rs") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.16.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17k9wmr67yxb4rlkrrpj0824frccdawi3zvfz70qdsg85wq7j8ps")))

(define-public crate-embree-rs-0.1.2 (c (n "embree-rs") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qap0ip1yi4n7jllwxngbxrp9k2hpy1j78l7ia2y5r22jw5h1mrr")))

(define-public crate-embree-rs-0.2.0 (c (n "embree-rs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.189") (o #t) (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hg0s67kwz8p6z42q4458xgskr3p9xb889zkvff94ld8fi0fs97g")))

(define-public crate-embree-rs-0.3.0 (c (n "embree-rs") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "clippy") (r "^0.0.189") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08qlbmxbp3b3npvrqf9gcrr9xl0hhbd7mbhh09mq4avz9fz09vzs")))

(define-public crate-embree-rs-0.3.1 (c (n "embree-rs") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "clippy") (r "^0.0.189") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x9b988p98kr21na8my47dva716l6ry835kizjl9wpr6in5q8sma")))

(define-public crate-embree-rs-0.3.2 (c (n "embree-rs") (v "0.3.2") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)))) (h "0iisvndjvaa76jl2vgaa1mqbvvbv14c79akcdb5xzxwb8cfrzjj0")))

(define-public crate-embree-rs-0.3.4 (c (n "embree-rs") (v "0.3.4") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)))) (h "0v32kanfc2289mrxsb2rrmnl1g14aczar59qbw8wmvnxj788lhh2")))

(define-public crate-embree-rs-0.3.5 (c (n "embree-rs") (v "0.3.5") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)))) (h "1n2mcckvr8cgbj9gdk22iivxh0p6cr17f1nxz29v2h653nlbri1w")))

(define-public crate-embree-rs-0.3.6 (c (n "embree-rs") (v "0.3.6") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)))) (h "0bz33b4dmxxnac80mvhx32pmppwm6nf3mg366cwra30ccfaybwpn")))

