(define-module (crates-io em br embree4-sys) #:use-module (crates-io))

(define-public crate-embree4-sys-0.0.1 (c (n "embree4-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "1clj5qn5x5113yc5jn5ikpiib2y5hcj62j0s5a68h3b1gqgabxqx")))

(define-public crate-embree4-sys-0.0.2 (c (n "embree4-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "1mqnzm3zn6nhhl7ljyp8bsyan3jjg8rp1p44nw2v0k9f6gm1ygpd")))

(define-public crate-embree4-sys-0.0.3 (c (n "embree4-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "1q8k5l3mvi6qqs0l08fz99fqn53ffsz07kaf2bz7rxfnly47xy4l")))

(define-public crate-embree4-sys-0.0.4 (c (n "embree4-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "0qlcsj6jvdq8p2lwjlqyi1k5y5zrvv27lxjmidy3h69yh93x2r86")))

(define-public crate-embree4-sys-0.0.5 (c (n "embree4-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "01hd1z6fhh4kd5xaz4bzipn2hcs2lwd2g6m5ppz9f8f24brghy4x")))

(define-public crate-embree4-sys-0.0.6 (c (n "embree4-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "09smy61v9yj6v8bwa8bwkr0ncdbpj0531clhzi4ndkjnvcz9hfx4")))

(define-public crate-embree4-sys-0.0.7 (c (n "embree4-sys") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "0qa7dk1k1vigaik8s9lnrkchxbg7z1i920y938pfx4czi0rx49in")))

(define-public crate-embree4-sys-0.0.8 (c (n "embree4-sys") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "0457vg9jwczy7vhijr49yq6spfff4iwsg97cy52bc27k8ihsax0l")))

(define-public crate-embree4-sys-0.0.9 (c (n "embree4-sys") (v "0.0.9") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "1kza9k1gq7czvmfhdd9v9ci46mlkl7wsi206sjahs93illrqni83")))

(define-public crate-embree4-sys-0.0.10 (c (n "embree4-sys") (v "0.0.10") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "0izylzc2i62ql45gdpnxg6kbc0da737aa9mcn9ykjqxpcfvbbx5w")))

