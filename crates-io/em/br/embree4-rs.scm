(define-module (crates-io em br embree4-rs) #:use-module (crates-io))

(define-public crate-embree4-rs-0.0.1 (c (n "embree4-rs") (v "0.0.1") (d (list (d (n "embree4-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0rjk2a8mcsl0s9khx0p8b8r219cx67rnncnwaz032zml8zpbzlgr")))

(define-public crate-embree4-rs-0.0.2 (c (n "embree4-rs") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1hc46p3ndawpwn1sdvsr9pmgap1pdiki58ial07gafa60hc382ml")))

(define-public crate-embree4-rs-0.0.3 (c (n "embree4-rs") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.4") (d #t) (k 0)))) (h "1aqdrwv4mc98n0c6i9582rnfpas3ccvqjdvyg0hprqx33i34w4ly")))

(define-public crate-embree4-rs-0.0.4 (c (n "embree4-rs") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.4") (d #t) (k 0)))) (h "0i7bnv08w7c5lys0l30208vixnj1v3140aqqwszg3379hyc6jidj")))

(define-public crate-embree4-rs-0.0.5 (c (n "embree4-rs") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.5") (d #t) (k 0)))) (h "1jmq5l0124y6scv2hq4fzn73ffpji66f45i2k2nih7a5jp98kifn")))

(define-public crate-embree4-rs-0.0.6 (c (n "embree4-rs") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.7") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (d #t) (k 2)))) (h "1xrs9ha2lqll7whx569yj3barylnap7g27g28ynd7mbk3k4hnmgi")))

(define-public crate-embree4-rs-0.0.7 (c (n "embree4-rs") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.7") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (f (quote ("rand"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1gmcmbaq9awh11zx0gnk0kva8i57r5wcscmbpcjyi9h5h0v2h4f0")))

(define-public crate-embree4-rs-0.0.8 (c (n "embree4-rs") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (f (quote ("rand"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1gkhyzbry1b2cwjrblx8f3wz28hpj0qccklc8kpljs0h60q87k3p")))

(define-public crate-embree4-rs-0.0.9 (c (n "embree4-rs") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (f (quote ("rand"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1a6gxgh7cf264lv1a6070wnk2ak2bb25ysmag5d6rgb26bwi9dak")))

(define-public crate-embree4-rs-0.0.10 (c (n "embree4-rs") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (f (quote ("rand"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1vi462k5fpy2i71799dzzji0nxl5g3hnbn7zf4jp14833dbbrlav")))

(define-public crate-embree4-rs-0.0.11 (c (n "embree4-rs") (v "0.0.11") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (f (quote ("rand"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "18y2w601n3zz49sja7ry8f90lrd1ncn1bdq9vmfmrdsc7ccvb174")))

(define-public crate-embree4-rs-0.0.12 (c (n "embree4-rs") (v "0.0.12") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (f (quote ("rand"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "11hcja2iv8ag3wqafjchz6sar2knb5yhnnhvmjzl95z2y45af7bw")))

(define-public crate-embree4-rs-0.0.13 (c (n "embree4-rs") (v "0.0.13") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (f (quote ("rand"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "07qwq6n8q4i9z7ijnb7ybx4dsmnkwip4qz2axxch50sd319lcksa")))

(define-public crate-embree4-rs-0.0.14 (c (n "embree4-rs") (v "0.0.14") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.10") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (f (quote ("rand"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1yr6bn94f83x95wn7p2m84jvga09q56mm3ldpysqjmggck2hdw4x")))

(define-public crate-embree4-rs-0.0.15 (c (n "embree4-rs") (v "0.0.15") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.10") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (f (quote ("rand"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "19zf243343qfj0pjbskmc55qg6ihxilllmwi5vcii6c2xq24jnj3")))

(define-public crate-embree4-rs-0.0.16 (c (n "embree4-rs") (v "0.0.16") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "embree4-sys") (r "^0.0.10") (d #t) (k 0)) (d (n "glam") (r "^0.24.2") (f (quote ("rand"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0zc2a6lbpc9xgzkpca73cr4j7pz8dm16vlar37djir91scj1jd3y")))

