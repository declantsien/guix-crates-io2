(define-module (crates-io em br embree) #:use-module (crates-io))

(define-public crate-embree-0.0.1 (c (n "embree") (v "0.0.1") (h "0g8shraz6pk9w5398ghzjym833a03pr03h137i9d8wwgk90bf8x7") (y #t)))

(define-public crate-embree-0.3.6 (c (n "embree") (v "0.3.6") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)))) (h "0wxxgbr0k4fc5w29516ydwyhx6f9z7m0993a912p0wbrwzr8zvyz")))

(define-public crate-embree-0.3.7 (c (n "embree") (v "0.3.7") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)))) (h "10g8adjpqkqaalxk9ji9jzvf1wzbq7zb8839mlqxxw9f6x4iwiiz")))

(define-public crate-embree-0.3.8 (c (n "embree") (v "0.3.8") (d (list (d (n "cgmath") (r "^0.18") (d #t) (k 0)))) (h "069j1x265l1m3b2wigfkg65ib3m9n1f6i382jq2mcqj700rcn2hj")))

