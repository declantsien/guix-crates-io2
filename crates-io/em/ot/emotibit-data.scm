(define-module (crates-io em ot emotibit-data) #:use-module (crates-io))

(define-public crate-emotibit-data-0.1.0 (c (n "emotibit-data") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "17qa97gbmaizkqwm48yb20la87jaq4shyql9favba9cq4xrh36vh")))

(define-public crate-emotibit-data-0.1.1 (c (n "emotibit-data") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0y5nxr7h9hmgc4xpkn1rrbqqhm7jy464qq46hbkaypyf861z7ixm")))

