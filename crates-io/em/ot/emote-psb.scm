(define-module (crates-io em ot emote-psb) #:use-module (crates-io))

(define-public crate-emote-psb-0.1.0 (c (n "emote-psb") (v "0.1.0") (d (list (d (n "adler") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)))) (h "0l9f7zxbk7qj9d2j5bq5hfxq017b3x3mn7d3rxc86n6j3gkvfwfi")))

(define-public crate-emote-psb-0.2.0 (c (n "emote-psb") (v "0.2.0") (d (list (d (n "adler") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)))) (h "0f9c2af6sbx2rz4pan3l31l5pgdvsgpqaac4jp865476wzyn8xmx")))

(define-public crate-emote-psb-0.2.1 (c (n "emote-psb") (v "0.2.1") (d (list (d (n "adler") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "0w91p0nrqlrsh6lzvhsc7yq15l72arnh9zi26gl8kkh0w9x8bfws")))

(define-public crate-emote-psb-0.3.0 (c (n "emote-psb") (v "0.3.0") (d (list (d (n "adler") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "1pwnds199fph8wxyb2mp4ip8snzc4yxgqld1qyrj55imdjl658mc")))

(define-public crate-emote-psb-0.3.1 (c (n "emote-psb") (v "0.3.1") (d (list (d (n "adler") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "1fy17dcnmfdlrfgag23zaqzfv2dw5mw8a23ifn0cjsf2r3glqip5")))

(define-public crate-emote-psb-0.3.2 (c (n "emote-psb") (v "0.3.2") (d (list (d (n "adler") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "0av7wal1v8jdcwfl412hzxx5rpx8089rsj9zabiphqx25jpspsz5")))

(define-public crate-emote-psb-0.3.3 (c (n "emote-psb") (v "0.3.3") (d (list (d (n "adler") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "0x04j3gx624aijzxphykvy713077n6fvy2c08gbqfxi2zbcdnhzb")))

(define-public crate-emote-psb-0.4.0 (c (n "emote-psb") (v "0.4.0") (d (list (d (n "adler") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b0mggmcf2al823ha4nr82qrnkka09gam8zc1bnnmi3gs4bhcpkk") (f (quote (("default"))))))

(define-public crate-emote-psb-0.4.1 (c (n "emote-psb") (v "0.4.1") (d (list (d (n "adler") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mj8d9xwnfmab91a1aibxpbwq9v2plqflkbp68nhf5l9v9jxplyd") (f (quote (("default"))))))

(define-public crate-emote-psb-0.5.0 (c (n "emote-psb") (v "0.5.0") (d (list (d (n "adler") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00zvj83g54q3qx1dgss79fz985g0i0znyx9yzgpz11zfzs1nfwp5") (f (quote (("default"))))))

