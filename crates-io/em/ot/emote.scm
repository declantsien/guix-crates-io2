(define-module (crates-io em ot emote) #:use-module (crates-io))

(define-public crate-emote-0.1.0 (c (n "emote") (v "0.1.0") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "blake2") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "digest") (r "^0.7") (f (quote ("std"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "00005428cxjxfsqrjskhnx8gwjdgqhkfvlid01x7jw5d2iicvxfv")))

