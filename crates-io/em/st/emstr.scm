(define-module (crates-io em st emstr) #:use-module (crates-io))

(define-public crate-emstr-0.1.0 (c (n "emstr") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.38") (o #t) (k 0)))) (h "1s4g83xgzd99k654g3k4p5q9xgy25mg3cj0xk1w78zw5gfw24rm1") (f (quote (("std" "thiserror") ("default" "std"))))))

(define-public crate-emstr-0.1.1 (c (n "emstr") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "thiserror") (r "^1.0.38") (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03f2v7vszrlwqzqr21sgmngvgwyyhl4ny31ny5vk8hhrqm9kcj7n") (f (quote (("std" "thiserror") ("default" "std"))))))

(define-public crate-emstr-0.1.2 (c (n "emstr") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "thiserror") (r "^1.0.38") (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0bidrsx6wsjyi70r4g1hcm0fsbdywwwzki3gd5vfc6bld3yf65wz") (f (quote (("std" "thiserror") ("default" "std"))))))

(define-public crate-emstr-0.2.0 (c (n "emstr") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "thiserror") (r "^1.0.38") (o #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1db70rwfllf9k96pql11l9hs9j9nvr0hm43rv5kai7j2vq752jrc") (f (quote (("std" "thiserror") ("default" "std"))))))

