(define-module (crates-io em sc emscripten_main_loop) #:use-module (crates-io))

(define-public crate-emscripten_main_loop-0.1.0 (c (n "emscripten_main_loop") (v "0.1.0") (h "0qivvmmriaxx3a0b504ph3r0njvsghwp91lchp7av69lc5ixlhbc")))

(define-public crate-emscripten_main_loop-0.1.1 (c (n "emscripten_main_loop") (v "0.1.1") (h "10mnd5b3ikc7aix9h8i7jy4qv8bqwqmig82ypslpn6745y5x5yjs")))

