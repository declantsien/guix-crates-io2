(define-module (crates-io em sc emscripten-sys) #:use-module (crates-io))

(define-public crate-emscripten-sys-0.1.0 (c (n "emscripten-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)))) (h "19jsabfwyzh08091hwq1v2v517crpbqg94iij9i9gv1v249fvq4p")))

(define-public crate-emscripten-sys-0.2.0 (c (n "emscripten-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)))) (h "1z9xf2q519qgsm3yps4l6kkw3lc8znvj9pj4syhl30mbqzh460j9") (y #t)))

(define-public crate-emscripten-sys-0.3.0 (c (n "emscripten-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)))) (h "15n1rm43kikirqwpm1cyd24nywg6bkb21cwpa8mvggd20b785kxa")))

(define-public crate-emscripten-sys-0.3.1 (c (n "emscripten-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.24.0") (d #t) (k 1)))) (h "1mwrnk2chqv91mmg60pyf08163647gdvns80vk94nzsg88hlby6f")))

(define-public crate-emscripten-sys-0.3.2 (c (n "emscripten-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.24.0") (d #t) (k 1)))) (h "0f9nfcs9db2zbi40lxfz006d41ikcssjgx1nm2wwiah9prfjjfb0")))

