(define-module (crates-io em sc emscripten-functions-sys) #:use-module (crates-io))

(define-public crate-emscripten-functions-sys-3.1.44 (c (n "emscripten-functions-sys") (v "3.1.44") (h "1i00svs4y9i77w8hkmfpg0yad4svq4jnagfa55flpyxw2aifsd3i")))

(define-public crate-emscripten-functions-sys-3.2.46 (c (n "emscripten-functions-sys") (v "3.2.46") (h "1j6s4myipp8ggz2r1dw3gp681p3njkd9p1cxcmzn14797289nwqs")))

