(define-module (crates-io em sc emscripten-functions) #:use-module (crates-io))

(define-public crate-emscripten-functions-0.1.0 (c (n "emscripten-functions") (v "0.1.0") (d (list (d (n "emscripten-functions-sys") (r "^3.1.44") (d #t) (k 0)))) (h "0zd8v2caqs53m7mpxn7idk8f3xkm8a452klm11cifizbq3av837a")))

(define-public crate-emscripten-functions-0.2.0 (c (n "emscripten-functions") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "emscripten-functions-sys") (r "^3.1.44") (d #t) (k 0)))) (h "0rhs7v51xvd8rwi9a3iyvcgfbk0g50yyssawmcq3lqvnwf65r906")))

(define-public crate-emscripten-functions-0.2.1 (c (n "emscripten-functions") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "emscripten-functions-sys") (r "^3.2.46") (d #t) (k 0)))) (h "01arfdg0bm542miwmpzg002w5dwnyiz9yb1fzjvh10kvgpdvl896")))

