(define-module (crates-io em at emath) #:use-module (crates-io))

(define-public crate-emath-0.8.0 (c (n "emath") (v "0.8.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hdqkgw1qrswrbv48vyxs8psc4m2a0asmndqj5qm64h7j5dnm1vd") (f (quote (("default"))))))

(define-public crate-emath-0.9.0 (c (n "emath") (v "0.9.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kwkjsvp2vy35i76gr37qmmd3n82a724vhqdbkjlyzgzrrk1i1m7") (f (quote (("default"))))))

(define-public crate-emath-0.10.0 (c (n "emath") (v "0.10.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0j2bz0sfr8sh16vg70q4dci0kyrghr00cn8yvhhqj8v9snqnlb33") (f (quote (("default"))))))

(define-public crate-emath-0.11.0 (c (n "emath") (v "0.11.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bhljmjzbx63z0x55xg4mf0i11zammj0hjcq05f7wlb68v0p5vd4") (f (quote (("default"))))))

(define-public crate-emath-0.12.0 (c (n "emath") (v "0.12.0") (d (list (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04paxwv5dpjhs9nqhmmn8qxicpmj78q3rg7vaqbvgb8fqz4dcwry") (f (quote (("default"))))))

(define-public crate-emath-0.13.0 (c (n "emath") (v "0.13.0") (d (list (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k4szwd5zp5vxj0bjsrcc838c3a0dz5cvba1l3ib99q8ii8agvl0") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default"))))))

(define-public crate-emath-0.14.0 (c (n "emath") (v "0.14.0") (d (list (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p71cnrm8blrkbqny182nikcqy9slzfmdfvrpg72hlfim6d1g9fc") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default"))))))

(define-public crate-emath-0.15.0 (c (n "emath") (v "0.15.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vpbf80j94jhlxvrgmnq5vavjzry2lypr59jpyiacpfm4alsm894") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default"))))))

(define-public crate-emath-0.16.0 (c (n "emath") (v "0.16.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vd73cg5mc2yzpf79v1b3anywjq5lwv2syxs4pg0srlnxgi3srsm") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.56")))

(define-public crate-emath-0.17.0 (c (n "emath") (v "0.17.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04kg03d6ii21y66wk87mrv74dvxdpb8c2scfsk1a4n5yaq2ahxx9") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.56")))

(define-public crate-emath-0.18.0 (c (n "emath") (v "0.18.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wmsh04xn6mvbnvbqrknnq24gg9z3fgrcyrn1xvy3arqgs6ga8y2") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.60")))

(define-public crate-emath-0.19.0 (c (n "emath") (v "0.19.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1krkd9ld95xhq8qb0jd00fly2l508qbqshaz0lx99fpx0q0s8hlm") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.61")))

(define-public crate-emath-0.20.0 (c (n "emath") (v "0.20.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "187h1mqyva2k6adwwcp13bqvmrw5fjkl0b2ggq9ffc1lijf28xsj") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.65")))

(define-public crate-emath-0.21.0 (c (n "emath") (v "0.21.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zppsgn2x9lwyra3kffs4i700l711xvm2lwxj1kh4zlk283div5q") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.65")))

(define-public crate-emath-0.22.0 (c (n "emath") (v "0.22.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jqsfb4x41fr84xpj6ziz3spbsing96af8mnc3fiqx70lr1xfmrq") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.65")))

(define-public crate-emath-0.23.0 (c (n "emath") (v "0.23.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1h7f5sihbnd0fm8ycpxjyy8m2vmyrikl2scbq5syax1hwnfv5why") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.70")))

(define-public crate-emath-0.24.0 (c (n "emath") (v "0.24.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k85d8ywmgf1a452zag15z4lirf0ashnip0fjp1is57fxpm7nx6z") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.72")))

(define-public crate-emath-0.24.1 (c (n "emath") (v "0.24.1") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1r6caqgn0ral6kxbkk6a4yn82a5l78c9s7pw2f2yjdabnk0ccid0") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.72")))

(define-public crate-emath-0.25.0 (c (n "emath") (v "0.25.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zivhbj6zwkfxywlhaschg6m4c5d7kck12bkldxxp1vmfraq7r9y") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.72")))

(define-public crate-emath-0.26.0-alpha.1 (c (n "emath") (v "0.26.0-alpha.1") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ml3navh76l2s3aqsv3c4swqbfib4bpkf3kpjr4smy3z1q5ipd8m") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.72")))

(define-public crate-emath-0.26.0-alpha.2 (c (n "emath") (v "0.26.0-alpha.2") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0db7vmaxa7snlb38f5xj2g6fa9bh4kczg0g2k3iaia867cx92ibh") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.72")))

(define-public crate-emath-0.26.0 (c (n "emath") (v "0.26.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rbamamfla5la8idmxpk4gpplp26iv8z6mz50lmcw5m4axglg8kb") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.72")))

(define-public crate-emath-0.26.1 (c (n "emath") (v "0.26.1") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wc7fjqk2yh8r6lwpfp7ny7k9ffy093ys3ags4n3y15xmwznd1i3") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.72")))

(define-public crate-emath-0.26.2 (c (n "emath") (v "0.26.2") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02dgx8946bcy254rs8ijhx9k3n5xklfvagnzdiw8yi40rwg305k9") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.72")))

(define-public crate-emath-0.27.0 (c (n "emath") (v "0.27.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1glgk7s8ayx64h4jv3cfi0s1gzpk1gfn85vy4qi19qfydvsrjxz8") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.72")))

(define-public crate-emath-0.27.1 (c (n "emath") (v "0.27.1") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11yrvjvzcz6ffzpvfc3zyqsw988ghh61k67qz2sixj2jrjzpqnjm") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.72")))

(define-public crate-emath-0.27.2 (c (n "emath") (v "0.27.2") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gqgc81shxci0p372dr7cdcx38443ks3b7a4083n656arx9abhz4") (f (quote (("extra_debug_asserts") ("extra_asserts") ("default")))) (r "1.72")))

