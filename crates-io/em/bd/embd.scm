(define-module (crates-io em bd embd) #:use-module (crates-io))

(define-public crate-embd-0.1.0 (c (n "embd") (v "0.1.0") (d (list (d (n "embd-macros") (r "^0.1") (d #t) (k 0)))) (h "145x4p0zdq8x8pzw9kzdqj23yvkpw7aqgdijlbc30wb6pd18mbjg")))

(define-public crate-embd-0.1.1 (c (n "embd") (v "0.1.1") (d (list (d (n "embd-macros") (r "^0.1") (d #t) (k 0)))) (h "05hgsx9knk45zjxm9x745wncw4zlfrfrm56rxdf563rhwkalpx3k")))

(define-public crate-embd-0.1.2 (c (n "embd") (v "0.1.2") (d (list (d (n "embd-macros") (r "^0.1") (d #t) (k 0)))) (h "1jwgxqvw1ranfa1nhxw9508jdvfaz8vg43hyg6prwqk9p84dy3r5")))

