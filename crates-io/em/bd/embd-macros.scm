(define-module (crates-io em bd embd-macros) #:use-module (crates-io))

(define-public crate-embd-macros-0.1.0 (c (n "embd-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ngf7vk1frsgsm14i4a7wly9zf7j0a75l8ljvz69i9ympn2qxsxk")))

(define-public crate-embd-macros-0.1.1 (c (n "embd-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "03vvfv3kl8dyzakwmw9n9wmspxg35vxyyw6gfx6q6d56305zz2kd")))

(define-public crate-embd-macros-0.1.2 (c (n "embd-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "19n39hy30ikp3bj478aa41qicsaa3qj9qn8fgqkxd8if8888vsly")))

