(define-module (crates-io em ce emcee) #:use-module (crates-io))

(define-public crate-emcee-0.1.0 (c (n "emcee") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1w2977hs7zmidymvmib4d4q1fcn6qsq8gil7kq8kya45pqwm70hp")))

(define-public crate-emcee-0.1.1 (c (n "emcee") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "090k6rcr9439hx2p4kyp0dflm6q5858x0r6ghqilkmxskd9rlw0q")))

(define-public crate-emcee-0.2.0 (c (n "emcee") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "03nbjsk7bqqj0wrwrzi2hviskpbjb96icq27hbmlzl6a767h7rpd")))

(define-public crate-emcee-0.3.0 (c (n "emcee") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1ya5axmzl3aqamicrzfsd4nqjmh7pr63znwqral6jy7791dvc0yr")))

(define-public crate-emcee-1.0.0-alpha.1 (c (n "emcee") (v "1.0.0-alpha.1") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "13f0ni9dr3kx8r24bkigc2hvy9f73h0f8ka7khnqwwk1k0whga72")))

(define-public crate-emcee-1.0.0-alpha.2 (c (n "emcee") (v "1.0.0-alpha.2") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1bdgvcby4q102wdsc1lwas18dwqp7j6lcxlibm2iv52wssh71ncd")))

