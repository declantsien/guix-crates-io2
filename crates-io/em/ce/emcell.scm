(define-module (crates-io em ce emcell) #:use-module (crates-io))

(define-public crate-emcell-0.0.0 (c (n "emcell") (v "0.0.0") (d (list (d (n "cortex-m") (r "^0.7.7") (o #t) (d #t) (k 0)))) (h "19nznnj6dnjys32lkflbb81mq07200chcf84igdrvzkn52sp8db8") (f (quote (("rt-crate-cortex-m-rt" "cortex-m") ("default" "rt-crate-cortex-m-rt") ("build-rs"))))))

(define-public crate-emcell-0.0.1 (c (n "emcell") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.7") (o #t) (d #t) (k 0)))) (h "0x8shcbr3nlcivp32026py6ch68qqskswmbl597gl83xprpmpy21") (f (quote (("rt-crate-cortex-m-rt" "cortex-m") ("default" "rt-crate-cortex-m-rt") ("build-rs"))))))

