(define-module (crates-io em ce emcell-macro) #:use-module (crates-io))

(define-public crate-emcell-macro-0.0.0 (c (n "emcell-macro") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full"))) (d #t) (k 0)))) (h "0x0qm3jis8ksh7q1c6lc4y7r93sxhi3zymv8aj3lx0ljr39g903f") (f (quote (("default"))))))

(define-public crate-emcell-macro-0.0.1 (c (n "emcell-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full"))) (d #t) (k 0)))) (h "195vr5qmn4vm31wgz4qdkb0zgh806gmmznk6cfly30is366l6dxw") (f (quote (("default"))))))

