(define-module (crates-io em u- emu-audio) #:use-module (crates-io))

(define-public crate-emu-audio-0.1.0 (c (n "emu-audio") (v "0.1.0") (d (list (d (n "emu-audio-types") (r "^0.1.0") (d #t) (k 0)) (d (n "emu-core-audio-driver") (r "^0.1.1") (d #t) (k 0)))) (h "1nhmzc90nbwx3q47rz8rimcdkawm9mwdck7lmfy5zjs00jqm7708")))

(define-public crate-emu-audio-0.1.1 (c (n "emu-audio") (v "0.1.1") (d (list (d (n "emu-audio-types") (r "^0.1.0") (d #t) (k 0)) (d (n "emu-core-audio-driver") (r "^0.1.2") (d #t) (k 0)))) (h "128pry6gs35lhx78q1l6qq8njr0ayafhq2wznnyi6dyybipvhx9m")))

(define-public crate-emu-audio-0.1.2 (c (n "emu-audio") (v "0.1.2") (d (list (d (n "emu-audio-types") (r "^0.1.0") (d #t) (k 0)) (d (n "emu-core-audio-driver") (r "^0.1.3") (d #t) (k 0)))) (h "1fhv8zf6a9ss49di0k6p7jig4mrsbpb684b0ri6awr19b4bldc8w")))

