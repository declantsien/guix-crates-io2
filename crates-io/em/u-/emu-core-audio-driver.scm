(define-module (crates-io em u- emu-core-audio-driver) #:use-module (crates-io))

(define-public crate-emu-core-audio-driver-0.1.0 (c (n "emu-core-audio-driver") (v "0.1.0") (d (list (d (n "coreaudio-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "emu-audio-types") (r "*") (d #t) (k 0)))) (h "10ld6nmrzngrsl8jynp0xhsc2j4hzh49hggjq23vg5629dmjc65d")))

(define-public crate-emu-core-audio-driver-0.1.1 (c (n "emu-core-audio-driver") (v "0.1.1") (d (list (d (n "coreaudio-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "emu-audio-types") (r "^0.1.0") (d #t) (k 0)))) (h "1qphm574lp045jgrjp4677v5ljf725hfp8l8hjxrvb74d3kl7plb")))

(define-public crate-emu-core-audio-driver-0.1.2 (c (n "emu-core-audio-driver") (v "0.1.2") (d (list (d (n "coreaudio-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "emu-audio-types") (r "^0.1.0") (d #t) (k 0)))) (h "07jj14bzwybn4l6mf7a1iycymwdw805sphdl2gbmgril6634hb2z")))

(define-public crate-emu-core-audio-driver-0.1.3 (c (n "emu-core-audio-driver") (v "0.1.3") (d (list (d (n "coreaudio-rs") (r "^0.2.2") (d #t) (k 0)) (d (n "emu-audio-types") (r "^0.1.0") (d #t) (k 0)))) (h "0gxx7skd3vdirdq2nccdxynycn13gnma5nx6xhxrq6px6p3byl1r")))

