(define-module (crates-io em u- emu-runner) #:use-module (crates-io))

(define-public crate-emu-runner-0.1.0 (c (n "emu-runner") (v "0.1.0") (d (list (d (n "camino") (r "^1.1") (d #t) (k 0)) (d (n "sha1_smol") (r "^1") (d #t) (k 0)))) (h "060ii6vq6hhj9ky8fdyhsgm78nf4j16x26nqz0nfcpkb8s2nvzxd")))

(define-public crate-emu-runner-0.1.1 (c (n "emu-runner") (v "0.1.1") (d (list (d (n "camino") (r "^1.1") (d #t) (k 0)) (d (n "sha1_smol") (r "^1") (d #t) (k 0)))) (h "077k2drnff3f319z1li4rnwyjrhm7p4k9falxd922dj1fm9ngfaa")))

