(define-module (crates-io em ei emei) #:use-module (crates-io))

(define-public crate-emei-0.1.0 (c (n "emei") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi" "memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0h8ph80wx48682k0il0s699m037wav208j46gfb1g1cwwq60011z")))

(define-public crate-emei-0.1.1 (c (n "emei") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi" "memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1h5pfg762z7mxd734rc83i6l8rwk4zrap3hij5kqvlfb6sb3a35c")))

(define-public crate-emei-0.1.2 (c (n "emei") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi" "memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "11y5mf82p3s3zw6nkm2kgs95djrpzqfphj5w23m6pnd44hnywpbc")))

(define-public crate-emei-0.2.0 (c (n "emei") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi" "memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ldk2lcdjh2zlfllj11cg8bdca8g3kzrgmmfiwd1yzrp4rqbl68r")))

(define-public crate-emei-0.2.1 (c (n "emei") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi" "memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "077njpi4clpzmswh7krvb2y2z615bmmqpaq2g94c04p6xf1q25ih")))

(define-public crate-emei-0.2.2 (c (n "emei") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi" "memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1b4a93v7ayrki03ykq7b7m442k2xgyqy7mn8h2bh8r456nh8pjsi")))

(define-public crate-emei-0.2.3 (c (n "emei") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi" "memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1saipz2325g80zsr0b0dkl5bbvmjalplppra952r8b7gazvc7703")))

(define-public crate-emei-0.3.0 (c (n "emei") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi" "memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1w6wshimjhlamlhapfjglh3lgy635r6dpkpsjafjh6ss573yx6n2")))

(define-public crate-emei-0.4.0 (c (n "emei") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi" "memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0y2rxxl4x3yqp59qiw0rcnjnnypgld8m3rk4n5kzrmfb1kww2pm8")))

(define-public crate-emei-0.5.0 (c (n "emei") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi" "memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1d8ika88y1sdlyr2mgy84y0pypqdjmwv9ka139arxjy08rwnw0ra")))

