(define-module (crates-io em #{71}# em7180) #:use-module (crates-io))

(define-public crate-em7180-0.1.0 (c (n "em7180") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1hqbv4wvi8ndad9cr3rvvqnng6019pq3kww7ivfw5iyc5wzhrm2x")))

(define-public crate-em7180-0.1.1 (c (n "em7180") (v "0.1.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.13.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.10.1") (k 0)))) (h "1b2qi1sm6pn0pg6f2zjswxpdlvr130sdyygn80pdjqxkj8w27663")))

(define-public crate-em7180-0.2.0 (c (n "em7180") (v "0.2.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.10.1") (k 0)))) (h "04y2qyw9wpva7s670sw5gx5729hzmkm3qcf2i1fkz5hipsxlm37n")))

