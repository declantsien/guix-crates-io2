(define-module (crates-io em oj emojito) #:use-module (crates-io))

(define-public crate-emojito-0.1.0 (c (n "emojito") (v "0.1.0") (d (list (d (n "emoji") (r "^0.2.1") (d #t) (k 0)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)))) (h "0wnxx54zydfyky6dmhrjsvdrjrm33qywfql8vl1ymy4javn4wic3")))

(define-public crate-emojito-0.2.1 (c (n "emojito") (v "0.2.1") (d (list (d (n "emoji") (r "^0.2.1") (d #t) (k 0)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)))) (h "0zngdgqrxp7yw6bf7crnk2bcx6ay9zmrficvii3sxww4jsbv1byn")))

(define-public crate-emojito-0.3.0 (c (n "emojito") (v "0.3.0") (d (list (d (n "emoji") (r "^0.2.1") (d #t) (k 0)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)))) (h "0kx83x9jk0hlf7c7fkhrfacf545pkdaarj2iqx4d4xhl7v80772r")))

(define-public crate-emojito-0.3.5 (c (n "emojito") (v "0.3.5") (d (list (d (n "emoji") (r "^0.2.1") (d #t) (k 0)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)))) (h "16q00m1qv865grd1hz6an49d89siimc1fv9sm4p93i3nnbpyyh5y")))

