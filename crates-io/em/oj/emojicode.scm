(define-module (crates-io em oj emojicode) #:use-module (crates-io))

(define-public crate-emojicode-0.1.0 (c (n "emojicode") (v "0.1.0") (d (list (d (n "emojicode-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0yi9k22plhgpbbc5qrn5z3mhw341zf21iw0n78p5c00ip9xwl1m2")))

(define-public crate-emojicode-0.1.1 (c (n "emojicode") (v "0.1.1") (d (list (d (n "emojicode-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0wqn0ypnmyviiidw3x37jrzz323n0hqxm9jxa1rj8kv852c4iyb4")))

(define-public crate-emojicode-0.1.2 (c (n "emojicode") (v "0.1.2") (d (list (d (n "emojicode-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1547kfxk9wzbnqrc2kmwxc2ya15gs7zcv6h9bbr0hk0ld7ldjhy5")))

(define-public crate-emojicode-0.1.3 (c (n "emojicode") (v "0.1.3") (d (list (d (n "emojicode-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1wpx0yfvhgd8dyg1ms0fl50pz7r6xqn2pr3nf0naa7lgq2bb4d0k")))

(define-public crate-emojicode-0.1.4 (c (n "emojicode") (v "0.1.4") (d (list (d (n "emojicode-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1lni3fwb35si845iwk5ciscsjnycfbxcah1s4rr4w3c4gfi4p1qf")))

