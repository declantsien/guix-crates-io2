(define-module (crates-io em oj emoji-logger) #:use-module (crates-io))

(define-public crate-emoji-logger-0.1.0 (c (n "emoji-logger") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1k4x76la3bknfxfyrx78w8zv1lbh1a6qai7xg1kn5fvzkvd506p2")))

