(define-module (crates-io em oj emoji-quickpick) #:use-module (crates-io))

(define-public crate-emoji-quickpick-1.0.0 (c (n "emoji-quickpick") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "gdk") (r "^0.10.0") (d #t) (k 0)) (d (n "gio") (r "^0.6.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "libxdo-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "sublime_fuzzy") (r "^0.6.0") (d #t) (k 0)))) (h "1kky55zpwjqzydjkz10h4jrn586gaszzzb5nwhq9zkc03i68kdr4")))

