(define-module (crates-io em oj emoji-clock-cli) #:use-module (crates-io))

(define-public crate-emoji-clock-cli-0.0.0 (c (n "emoji-clock-cli") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1") (d #t) (k 0)) (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "emoji-clock") (r "^0.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0xfdhnmrnpw14mqv837k0jvbzw2lsiff7vpd6m2dl1q3mq6c87cj")))

