(define-module (crates-io em oj emojicons) #:use-module (crates-io))

(define-public crate-emojicons-0.3.1 (c (n "emojicons") (v "0.3.1") (d (list (d (n "phf") (r "^0.5.0") (d #t) (k 0)) (d (n "phf_mac") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.10") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.5") (d #t) (k 0)))) (h "0cpkd65w0z6k0ls4sf684si76gzh5z3h6www144kb0aifd55q8xz")))

(define-public crate-emojicons-0.3.2 (c (n "emojicons") (v "0.3.2") (d (list (d (n "phf") (r "^0.5.0") (d #t) (k 0)) (d (n "phf_mac") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.10") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.5") (d #t) (k 0)))) (h "0adhngnq7mgl6jg1wsq9cvnrd7pyx13bcckn9fiw7aqwbmbzy3m1")))

(define-public crate-emojicons-0.4.0 (c (n "emojicons") (v "0.4.0") (d (list (d (n "phf") (r "^0.5.0") (d #t) (k 0)) (d (n "phf_mac") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.10") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.5") (d #t) (k 0)))) (h "1hpfy6n6b6ag7fxblqjhqpj8pmmhrfl655mz6v2w159nqczfd8ps")))

(define-public crate-emojicons-0.5.0 (c (n "emojicons") (v "0.5.0") (d (list (d (n "phf") (r "^0.5.0") (d #t) (k 0)) (d (n "phf_mac") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.10") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.5") (d #t) (k 0)))) (h "0miww3dyq8wvwszjsrl6788bgdkyyih5sfw0jhqgvwrdnwb0zpzh")))

(define-public crate-emojicons-0.5.3 (c (n "emojicons") (v "0.5.3") (d (list (d (n "phf") (r "^0.7.10") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.10") (d #t) (k 0)) (d (n "regex") (r "^0.1.44") (d #t) (k 0)) (d (n "regex_macros") (r "^0.1.25") (d #t) (k 0)))) (h "1ki0bw9kfmdfpp64a9b9xisq7za42cfh6xydn3yyw4dwci3knkjg")))

(define-public crate-emojicons-1.0.0 (c (n "emojicons") (v "1.0.0") (d (list (d (n "phf") (r "^0.7.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.10") (d #t) (k 1)) (d (n "regex") (r "^0.1.58") (d #t) (k 0)))) (h "0jrjcazrs8wgsz13zzvkp7pd9iv3y0ah4kbrlk6fg9skfj77cgdk")))

(define-public crate-emojicons-1.0.1 (c (n "emojicons") (v "1.0.1") (d (list (d (n "phf") (r "^0.7.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.10") (d #t) (k 1)) (d (n "regex") (r "^0.1.58") (d #t) (k 0)))) (h "1s33j911ghkfdb5rf6118bwzgfj0196q6jvj4l4dsvlajb8jwamh")))

