(define-module (crates-io em oj emoji-clock) #:use-module (crates-io))

(define-public crate-emoji-clock-0.0.0 (c (n "emoji-clock") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)))) (h "18y618anyhx5znm5kls1ycmclwzmsapavd2fnvc896q1zvg5ql1p")))

