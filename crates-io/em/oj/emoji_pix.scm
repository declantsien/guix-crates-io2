(define-module (crates-io em oj emoji_pix) #:use-module (crates-io))

(define-public crate-emoji_pix-0.1.0 (c (n "emoji_pix") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "scarlet") (r "^1.1.0") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.4.0") (d #t) (k 0)))) (h "0pwq9qd4kxykg22nvkdx80ps3f7inzf915am2kmfg2mn7bkc4ws8")))

(define-public crate-emoji_pix-0.2.0 (c (n "emoji_pix") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "scarlet") (r "^1.1.0") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.4.0") (d #t) (k 0)))) (h "1m43fvx7j4yy741hdfgjh0wg8cicghql77lw7imswmm69b8y2gci")))

(define-public crate-emoji_pix-0.2.1 (c (n "emoji_pix") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "scarlet") (r "^1.1.0") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.4.0") (d #t) (k 0)))) (h "15w04y12w9g7mxbk4fyzpqvvghkkz07nga1grmp0k06fi9vcypl5")))

