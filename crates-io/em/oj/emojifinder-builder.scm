(define-module (crates-io em oj emojifinder-builder) #:use-module (crates-io))

(define-public crate-emojifinder-builder-0.1.0 (c (n "emojifinder-builder") (v "0.1.0") (d (list (d (n "emojifinder-core") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "roxmltree") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)))) (h "0v880nay27100dwm5aacsms3mg1ggwpc2r80ax6acrp7lhvbqw19")))

(define-public crate-emojifinder-builder-0.2.0 (c (n "emojifinder-builder") (v "0.2.0") (d (list (d (n "emojifinder-core") (r "^0.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "0zli88lq97dy044sqjq39xajz63fcj26zk3r6pkddmk1xi81ldph")))

