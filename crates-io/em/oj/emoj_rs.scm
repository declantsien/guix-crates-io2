(define-module (crates-io em oj emoj_rs) #:use-module (crates-io))

(define-public crate-emoj_rs-0.1.0 (c (n "emoj_rs") (v "0.1.0") (d (list (d (n "arboard") (r "^2.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tuikit") (r "^0.4.5") (d #t) (k 0)))) (h "0nsn9kszk712vnh421dk9rh9lv2666v11yfc8rz3yp4qn3diknwa")))

(define-public crate-emoj_rs-0.1.1 (c (n "emoj_rs") (v "0.1.1") (d (list (d (n "arboard") (r "^2.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tuikit") (r "^0.4.5") (d #t) (k 0)))) (h "09h7kbyliaadckacdqh4r4cqnxmyd1r9paiwsii2xg2lq88gz28l")))

