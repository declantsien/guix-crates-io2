(define-module (crates-io em oj emojify) #:use-module (crates-io))

(define-public crate-emojify-0.1.0 (c (n "emojify") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0c95nz7h0dxb9a775xy5g8z92zi11k4a40xzkml8liqbwgc3xr9x")))

(define-public crate-emojify-0.2.0 (c (n "emojify") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "emojicons") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1a2na41rwhlyk7rx1rn2aashmjg25pr72llchg5g4gs9icz7bxqr")))

