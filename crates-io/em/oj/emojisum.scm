(define-module (crates-io em oj emojisum) #:use-module (crates-io))

(define-public crate-emojisum-0.0.1 (c (n "emojisum") (v "0.0.1") (d (list (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bxm52kcns9s667nn9dxznqy4qg1gwail3lpjp63xld91sbdhc2s")))

