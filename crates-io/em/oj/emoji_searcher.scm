(define-module (crates-io em oj emoji_searcher) #:use-module (crates-io))

(define-public crate-emoji_searcher-0.1.0 (c (n "emoji_searcher") (v "0.1.0") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "00alb1bfya4x0bg41nmfscxhixg33wv4lwqsp0ry39zsv7v49qm2")))

