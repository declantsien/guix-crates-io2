(define-module (crates-io em oj emojic) #:use-module (crates-io))

(define-public crate-emojic-0.1.0 (c (n "emojic") (v "0.1.0") (h "1n6yb8bjwv9n8x3hwbvdxcfydzqdfk34fnb9a9yxqg39snarncld")))

(define-public crate-emojic-0.2.0 (c (n "emojic") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1vwxg6yh8bvjifsfdp6z3z6h9ifqm7ypb1lq9ck0q4mx3jcnp514")))

(define-public crate-emojic-0.2.1 (c (n "emojic") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "03w089zwk5n649dphzr62r1qfmxx1z43g6cxz8zmh6mqns9aqr34")))

(define-public crate-emojic-0.3.0 (c (n "emojic") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1g3iwp8nm07gvnrg06ggqh2slq6wir13vqr6wkndg70a3i2vaxsv") (f (quote (("default" "alloc") ("alloc" "hashbrown"))))))

(define-public crate-emojic-0.4.0 (c (n "emojic") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "1ii3d1d4klxj2jsp51j2g6bznfpggzxhkkvvk218rf2wv0sb2icy") (f (quote (("doc_cfg") ("default" "alloc") ("alloc" "hashbrown" "lazy_static"))))))

(define-public crate-emojic-0.4.1 (c (n "emojic") (v "0.4.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "0ycgli9zqqn4mg51g5xhrv0r6hlf8y1dnp43m5lyz4wvmnxrdkvj") (f (quote (("doc_cfg") ("default" "alloc") ("alloc" "hashbrown" "lazy_static"))))))

