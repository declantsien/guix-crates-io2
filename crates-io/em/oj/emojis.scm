(define-module (crates-io em oj emojis) #:use-module (crates-io))

(define-public crate-emojis-0.0.0 (c (n "emojis") (v "0.0.0") (d (list (d (n "fuzzy-matcher") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "0vg958fampj2iz6n97ij9yidw2fhn4zhnilgcidxla7q4hjhh94x") (f (quote (("std") ("search" "std" "fuzzy-matcher" "itertools") ("default" "search"))))))

(define-public crate-emojis-0.1.0 (c (n "emojis") (v "0.1.0") (d (list (d (n "strsim") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "09azpwlvydhgzai99wgbkm4abb3gc34l9xyq6v657diigqi702qy") (f (quote (("std") ("search" "std" "strsim") ("default" "search"))))))

(define-public crate-emojis-0.1.1 (c (n "emojis") (v "0.1.1") (d (list (d (n "strsim") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "0455ksfzyxklby5a9iz6bid9iwdmlvb6wzdp2qc0rw0vpnvdbf2n") (f (quote (("std") ("search" "std" "strsim") ("default" "search"))))))

(define-public crate-emojis-0.1.2 (c (n "emojis") (v "0.1.2") (d (list (d (n "strsim") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "15jggkxb75lxkzsschbfjdk6b152caakjfcbwzcvbdvjrkm5my6b") (f (quote (("std") ("search" "std" "strsim") ("default" "search"))))))

(define-public crate-emojis-0.2.0 (c (n "emojis") (v "0.2.0") (h "0d0vaw22l09s6x4n9al01939xn46b4bgn0r3b5vb77myir032v6r")))

(define-public crate-emojis-0.2.1 (c (n "emojis") (v "0.2.1") (h "1aigl2cnkdq061a82rxiw5hz92lqcvq2xnpnf4vrhgj8v194dq0v")))

(define-public crate-emojis-0.3.0 (c (n "emojis") (v "0.3.0") (h "0b37f7xb379mnn7y9fqkliphlcsahv3511y49lymyixlgsrzpi1a")))

(define-public crate-emojis-0.4.0 (c (n "emojis") (v "0.4.0") (h "1fn9n51hqn1a5k8cipbj7kffwm5znk3mmibrqzp306l8wlv01p1z")))

(define-public crate-emojis-0.5.0 (c (n "emojis") (v "0.5.0") (d (list (d (n "phf") (r "^0.11.1") (k 0)))) (h "1k49m9qgqm69rjd0lpscyn886p4cs0ccqbsi0qlwq4qcgqdhyrbs")))

(define-public crate-emojis-0.5.1 (c (n "emojis") (v "0.5.1") (d (list (d (n "phf") (r "^0.11.1") (k 0)))) (h "0mf2h61xlrqmab78f4kvak52k1l051fy00jlsa6bamw3czfl9bcy")))

(define-public crate-emojis-0.5.2 (c (n "emojis") (v "0.5.2") (d (list (d (n "phf") (r "^0.11.1") (k 0)))) (h "1w3b1k8zxg66sq0pzdv6iln9v6sa8znkq1fl2794lm5ncjw61zj4") (r "1.60")))

(define-public crate-emojis-0.5.3 (c (n "emojis") (v "0.5.3") (d (list (d (n "phf") (r "^0.11.1") (k 0)))) (h "0pdrhzlx53ksb41dx3qjl6qg9nj711vj28r8d92pv0lij5sbq1rl") (r "1.60")))

(define-public crate-emojis-0.6.0 (c (n "emojis") (v "0.6.0") (d (list (d (n "phf") (r "^0.11.1") (k 0)))) (h "1xah655zbmilvpsxcs0dcifv6kh8p7apl6fxirhnq264yc83spy1") (r "1.60")))

(define-public crate-emojis-0.6.1 (c (n "emojis") (v "0.6.1") (d (list (d (n "phf") (r "^0.11.1") (k 0)))) (h "0yf9xllzvvnm7x0hfably07jjcvwqqwps5cxs7kmxxmz8nwixrjf") (r "1.60")))

(define-public crate-emojis-0.6.2 (c (n "emojis") (v "0.6.2") (d (list (d (n "phf") (r "^0.11.1") (k 0)))) (h "1nf58nj4gj061jcjv5p299n0sjhka6q109nql14p3bhncs99lqcz") (r "1.60")))

