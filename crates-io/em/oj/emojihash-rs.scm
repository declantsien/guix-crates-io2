(define-module (crates-io em oj emojihash-rs) #:use-module (crates-io))

(define-public crate-emojihash-rs-0.1.0 (c (n "emojihash-rs") (v "0.1.0") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "144340b3q1sbm7hfwryvrqa5bjg95dhwnv79n0dbm52lp4q076vz")))

(define-public crate-emojihash-rs-0.2.0 (c (n "emojihash-rs") (v "0.2.0") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0zrmmvq9bx7057mam9zvgbqbgmvgh6nv48s36ra7s3i3b0fqblcn")))

(define-public crate-emojihash-rs-0.2.1 (c (n "emojihash-rs") (v "0.2.1") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0kb21a4zgn6a1piyvld0ps2d7x0zdxizzkaf5gkhl5sx7n5zps6j")))

