(define-module (crates-io em oj emoji_utils) #:use-module (crates-io))

(define-public crate-emoji_utils-0.0.1 (c (n "emoji_utils") (v "0.0.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0a0ml8874gynwjvca15wdc30jqnkwap2njjj9chn5n0h14nfqdq5")))

(define-public crate-emoji_utils-0.1.0 (c (n "emoji_utils") (v "0.1.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "19846limxpz0ja6sh2p07s2brxdk488jwy9x34l4icrbwrcn7was")))

(define-public crate-emoji_utils-0.1.1 (c (n "emoji_utils") (v "0.1.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0jzirq93d6fx1jhrhp02h4rj86jr8nvakj92y79js362grgik953")))

(define-public crate-emoji_utils-0.1.2 (c (n "emoji_utils") (v "0.1.2") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0xzmfbk1d7ypzddby4lxmb514r4izmiqn61r7klh3dzw5n2lgi2w")))

(define-public crate-emoji_utils-0.2.0 (c (n "emoji_utils") (v "0.2.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1vf4nj8vsprr3zl2gmcpnmkiixyk645c8r0ax8vzarb9drlssrhi")))

