(define-module (crates-io em oj emoji-commit) #:use-module (crates-io))

(define-public crate-emoji-commit-0.1.0 (c (n "emoji-commit") (v "0.1.0") (d (list (d (n "default-editor") (r "~0.1.0") (d #t) (k 0)) (d (n "log-update") (r "~0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "1xf8dyla2r1k2gvdxblk16jwg1rcwyrs994n1g5zwk822vhzkxwg")))

(define-public crate-emoji-commit-0.1.1 (c (n "emoji-commit") (v "0.1.1") (d (list (d (n "default-editor") (r "~0.1.0") (d #t) (k 0)) (d (n "log-update") (r "~0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "1zbz00xmh4zww7brwn0q19g1fpgc5jq4fmsz58x5zsm0kdflsvxm")))

(define-public crate-emoji-commit-0.1.2 (c (n "emoji-commit") (v "0.1.2") (d (list (d (n "default-editor") (r "~0.1.0") (d #t) (k 0)) (d (n "emoji-commit-type") (r "~0.1.0") (d #t) (k 0)) (d (n "log-update") (r "~0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "0x8k2jx83xaixcxph9hjybpnhz8v8779mah4wp7ixrcxcy64a64a")))

(define-public crate-emoji-commit-0.1.3 (c (n "emoji-commit") (v "0.1.3") (d (list (d (n "default-editor") (r "~0.1.0") (d #t) (k 0)) (d (n "emoji-commit-type") (r "~0.1.0") (d #t) (k 0)) (d (n "log-update") (r "~0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "1mkpd7py8llqd8531h1ilic3j8q6a68p1597k0z2zjhchb5g55gz")))

(define-public crate-emoji-commit-0.1.4 (c (n "emoji-commit") (v "0.1.4") (d (list (d (n "default-editor") (r "~0.1.0") (d #t) (k 0)) (d (n "emoji-commit-type") (r "~0.1.1") (d #t) (k 0)) (d (n "log-update") (r "~0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "15blnf1z6m2pn83fj0ywb7imhd9lr2gsnqrz3vkknpfsg6gx9rrj")))

(define-public crate-emoji-commit-0.1.5 (c (n "emoji-commit") (v "0.1.5") (d (list (d (n "default-editor") (r "~0.1.0") (d #t) (k 0)) (d (n "emoji-commit-type") (r "~0.1.1") (d #t) (k 0)) (d (n "log-update") (r "~0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "071aywbwjpmdqd13sfhixhnvcmq23bm5qhrdjdhcbpczmgk7bdjz")))

(define-public crate-emoji-commit-0.1.6 (c (n "emoji-commit") (v "0.1.6") (d (list (d (n "ansi_term") (r "~0.12") (d #t) (k 0)) (d (n "default-editor") (r "~0.1.0") (d #t) (k 0)) (d (n "emoji-commit-type") (r "~0.1.1") (d #t) (k 0)) (d (n "git2") (r "~0.13.20") (d #t) (k 0)) (d (n "log-update") (r "~0.1.0") (d #t) (k 0)) (d (n "structopt") (r "~0.3") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "0iqawrnq2ncxbxfk1x5hlr092425348pg3gl3cc3hi5cafiig5ff")))

(define-public crate-emoji-commit-0.1.7 (c (n "emoji-commit") (v "0.1.7") (d (list (d (n "ansi_term") (r "~0.12") (d #t) (k 0)) (d (n "default-editor") (r "~0.1.0") (d #t) (k 0)) (d (n "emoji-commit-type") (r "~0.1.1") (d #t) (k 0)) (d (n "git2") (r "~0.13.20") (d #t) (k 0)) (d (n "log-update") (r "~0.1.0") (d #t) (k 0)) (d (n "structopt") (r "~0.3") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "18dy20ivngchadm78vn9dwhvdhv6l7aq4f302faikkaqhh9hy4qc")))

(define-public crate-emoji-commit-0.1.8 (c (n "emoji-commit") (v "0.1.8") (d (list (d (n "ansi_term") (r "~0.12") (d #t) (k 0)) (d (n "default-editor") (r "~0.1.0") (d #t) (k 0)) (d (n "emoji-commit-type") (r "~0.1.1") (d #t) (k 0)) (d (n "git2") (r "~0.13.20") (d #t) (k 0)) (d (n "log-update") (r "~0.1.0") (d #t) (k 0)) (d (n "structopt") (r "~0.3") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "10hdhl0x55qc4irm0x5xmg1mdlyjjp2yk2mrvp8rxfkapg7n2yj3")))

(define-public crate-emoji-commit-0.1.9 (c (n "emoji-commit") (v "0.1.9") (d (list (d (n "ansi_term") (r "~0.12") (d #t) (k 0)) (d (n "default-editor") (r "~0.1.0") (d #t) (k 0)) (d (n "emoji-commit-type") (r "~0.1.1") (d #t) (k 0)) (d (n "git2") (r "~0.15.0") (d #t) (k 0)) (d (n "log-update") (r "~0.1.0") (d #t) (k 0)) (d (n "structopt") (r "~0.3") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "0y729wfwdvk3aby0lfd5x50h12awf3k7s2ss26lsvwmpkcfr4b84")))

(define-public crate-emoji-commit-0.1.10 (c (n "emoji-commit") (v "0.1.10") (d (list (d (n "ansi_term") (r "~0.12") (d #t) (k 0)) (d (n "default-editor") (r "~0.1.0") (d #t) (k 0)) (d (n "emoji-commit-type") (r "~0.1.1") (d #t) (k 0)) (d (n "git2") (r "~0.15.0") (d #t) (k 0)) (d (n "log-update") (r "~0.1.0") (d #t) (k 0)) (d (n "structopt") (r "~0.3") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "0j5724v8pl2gf8qwgd678jvrdx6kzvkfk9rgyams0f7jdh458p2k")))

