(define-module (crates-io em oj emojilib) #:use-module (crates-io))

(define-public crate-emojilib-0.1.0 (c (n "emojilib") (v "0.1.0") (h "176cxm0ajs1px5a1dz4v396vkl1mcp55xinphsnzkqm55jsb7k85")))

(define-public crate-emojilib-0.1.1 (c (n "emojilib") (v "0.1.1") (h "0r88n5plhjgsikvm7n7y3jh1c8d79855zj0cq0zxvmx0vfgh0150")))

(define-public crate-emojilib-0.1.2 (c (n "emojilib") (v "0.1.2") (h "1dl0z7418xb8nvcd7m078lz4zixzzn3r67hm5bzl6hbqfgl0h17p")))

