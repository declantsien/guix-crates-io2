(define-module (crates-io em oj emoji-clock-2) #:use-module (crates-io))

(define-public crate-emoji-clock-2-0.1.0 (c (n "emoji-clock-2") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (k 0)))) (h "07q5hvqy4ga8vwfd1845c2fa8d4dn0cp2prn5yi0ifwfnkfy31s9") (f (quote (("full" "time" "chrono"))))))

(define-public crate-emoji-clock-2-0.1.1 (c (n "emoji-clock-2") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (k 0)))) (h "11lp28hsb8ircvswg1v6dm5x5cdxcivsfkas17v2y8lam17kjcdi") (f (quote (("full" "time" "chrono"))))))

(define-public crate-emoji-clock-2-0.2.0 (c (n "emoji-clock-2") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (k 0)))) (h "0ka6zvjsb25mk96l7ngv5wb02x3nzz9gln39p41r288ir656kwmj") (f (quote (("full" "time" "chrono"))))))

