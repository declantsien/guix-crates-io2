(define-module (crates-io em oj emojicode-sys) #:use-module (crates-io))

(define-public crate-emojicode-sys-0.1.0 (c (n "emojicode-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "0knvsywfgiaqv547hb83788l4hd64ykbh23nzxq07jlf4ppjpvnk")))

(define-public crate-emojicode-sys-0.1.1 (c (n "emojicode-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "11nyk2lhvrqfajpx17sl4wzv5ssgbw166k3psp4yd289br1xi41s")))

(define-public crate-emojicode-sys-0.1.2 (c (n "emojicode-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "01c612q8hxjp5l5505iwcw2isqdj29r8qv08cikppjrrwry54ddy")))

(define-public crate-emojicode-sys-0.1.3 (c (n "emojicode-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "0jn468wi79ych4flzhbk5vzq4cc3mvy0933kjzkwr6wk0s39pxyc")))

(define-public crate-emojicode-sys-0.1.4 (c (n "emojicode-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "0r8q174xkqa0jdgaqzgvgrcrd7c3lxiicnwvm1vhv0yjk66npdbc")))

