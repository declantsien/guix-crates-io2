(define-module (crates-io em oj emojicon) #:use-module (crates-io))

(define-public crate-emojicon-0.1.0 (c (n "emojicon") (v "0.1.0") (h "11w7fmhhaxxky9chfwgny2lrx38s12ig9cqd9ca7br3mrxj73m8f")))

(define-public crate-emojicon-0.2.0 (c (n "emojicon") (v "0.2.0") (h "0nn12k7v6zm7z9vrnpz0alij7v8g113c6bvn4npvh2f30wpxq1dw")))

(define-public crate-emojicon-0.3.0 (c (n "emojicon") (v "0.3.0") (h "1phbr0dwdh70h37qffyxphqy569ibj17mhp9cm2ilxql88f8gng0")))

(define-public crate-emojicon-0.3.1 (c (n "emojicon") (v "0.3.1") (h "1ycy15ck6hg3hlaf9zp8qmbmkmzyk1zlhhgp5x4xh0akraqvz71l")))

(define-public crate-emojicon-0.4.0 (c (n "emojicon") (v "0.4.0") (h "1jd6dbrsicqczqmm2zbvbfgrpz2alh58m37ff0qqr77m3jgl1ijg") (f (quote (("internal") ("custom")))) (r "1.56.0")))

