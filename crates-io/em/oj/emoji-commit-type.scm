(define-module (crates-io em oj emoji-commit-type) #:use-module (crates-io))

(define-public crate-emoji-commit-type-0.1.0 (c (n "emoji-commit-type") (v "0.1.0") (h "0d005ma53a5r1y321p9idvqnn1w03j9lbza1qh7wwmg97w6cz67s")))

(define-public crate-emoji-commit-type-0.1.1 (c (n "emoji-commit-type") (v "0.1.1") (h "0cshrwfcc4pa8rs5a5fp439b6j50hzpabb02ihlrpmj18j9sq9rf")))

