(define-module (crates-io em v_ emv_tlv_parser) #:use-module (crates-io))

(define-public crate-emv_tlv_parser-0.1.0 (c (n "emv_tlv_parser") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1z0aaxkfqrkkjjpij65jalbf77pviyi5vnzqyclb9i4w4aid4xp0") (y #t)))

(define-public crate-emv_tlv_parser-0.1.1 (c (n "emv_tlv_parser") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "04prx96j3l1g61lcw11a8akkrz5a8iv264m9a3wc9agx5ii5y1av") (y #t)))

(define-public crate-emv_tlv_parser-0.1.2 (c (n "emv_tlv_parser") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "120xjs6mwr5rskj0hssva52rg0l5x25xqrwgp89myc68y2phv8p1")))

(define-public crate-emv_tlv_parser-0.1.3 (c (n "emv_tlv_parser") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0nzzd6c5f1zq3qcg8m1s80f7ym18crih1mpgvs3iir02nx40ybnm")))

(define-public crate-emv_tlv_parser-0.1.5 (c (n "emv_tlv_parser") (v "0.1.5") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1yc1wpn832jx4hk1yph6qc5irpkixwvfs3vipzdqf61k0hqpn1b7")))

(define-public crate-emv_tlv_parser-0.1.6 (c (n "emv_tlv_parser") (v "0.1.6") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "11kfjs7p2jhyn5n24n0cb3fmnx0prqx4aisg229kkl62rs2lnxjq")))

(define-public crate-emv_tlv_parser-0.1.7 (c (n "emv_tlv_parser") (v "0.1.7") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0zhfz307f4l6y6ygnyy3mv6nh2m1v7a9b6g5yh3mznh13gcpna94")))

(define-public crate-emv_tlv_parser-0.1.8 (c (n "emv_tlv_parser") (v "0.1.8") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0fv3pj225avf3ymy2pbwdj0wgjjwhxnqy97xaclca8nqm53iqz27")))

(define-public crate-emv_tlv_parser-0.1.9 (c (n "emv_tlv_parser") (v "0.1.9") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "03ng9fqbs4wrgdh541l5k7s90yghvhwsihvkndrg1rycssnqb5k4")))

(define-public crate-emv_tlv_parser-0.1.10 (c (n "emv_tlv_parser") (v "0.1.10") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "09wwc2ris1k97pj308d3ss3f08l1kkyi4rsj8vzxlxjdzc68c2vz")))

(define-public crate-emv_tlv_parser-0.1.11 (c (n "emv_tlv_parser") (v "0.1.11") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0bckkfr6w8kiyrl8cfxvsvzx4xafmb2g6nw7pvss8fndxsl7hil8")))

