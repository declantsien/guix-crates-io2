(define-module (crates-io em v_ emv_parser) #:use-module (crates-io))

(define-public crate-emv_parser-0.1.1 (c (n "emv_parser") (v "0.1.1") (d (list (d (n "emv_tlv_parser") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0657ll4kmn1p9q1lrfprn9sq08gywjkylqsvy59vgwfxjq70f27n") (y #t)))

(define-public crate-emv_parser-0.1.2 (c (n "emv_parser") (v "0.1.2") (d (list (d (n "emv_tlv_parser") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1s77yzlnc3wvsk0yqsapyh36lah6w1w0m7k87cknrfbihfy0y4mg") (y #t)))

(define-public crate-emv_parser-0.1.3 (c (n "emv_parser") (v "0.1.3") (d (list (d (n "emv_tlv_parser") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0an3fg6vqixmwz2r255n651dgfl3fplg1rqf73nclkfzcgh5x168") (y #t)))

(define-public crate-emv_parser-0.1.4 (c (n "emv_parser") (v "0.1.4") (d (list (d (n "emv_tlv_parser") (r "^0.1.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0sjg7nhmc5kig3aayqfq273sv3cbbawg7vkc2x138pa1327mmygs") (y #t)))

(define-public crate-emv_parser-0.1.5 (c (n "emv_parser") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "emv_tlv_parser") (r "^0.1.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0yw3lzxvj48lh5n2znpy6aw7fzzxdqrk87zgpjpmzicydkqg0zz0") (y #t)))

