(define-module (crates-io em ve emver) #:use-module (crates-io))

(define-public crate-emver-0.1.0 (c (n "emver") (v "0.1.0") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "fp-core") (r "^0.1.9") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.14") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.68") (o #t) (d #t) (k 0)))) (h "1zhznyf88rkwnfpy32f2msvwi9lzbrlc132zxcn2l58r0pgz88bn")))

(define-public crate-emver-0.1.1 (c (n "emver") (v "0.1.1") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "fp-core") (r "^0.1.9") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.14") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.68") (o #t) (d #t) (k 0)))) (h "060m2z3bp3j7waqk5yldi76dqhy4fga0b8g4shnv8xpz3sd3rbgm")))

(define-public crate-emver-0.1.2 (c (n "emver") (v "0.1.2") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "fp-core") (r "^0.1.9") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.14") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.68") (o #t) (d #t) (k 0)))) (h "1f2xxipx0bmi0s9kkwm20g0xgb2qqpiq500az1li13cr0iaw5ckr")))

(define-public crate-emver-0.1.6 (c (n "emver") (v "0.1.6") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "fp-core") (r "^0.1.9") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.14") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.68") (o #t) (d #t) (k 0)))) (h "0147y93yix9n0a02a2g5pwmxzqrny4y4sv2gkhdh7v7sgr6hq9pd")))

