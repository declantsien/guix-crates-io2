(define-module (crates-io em v- emv-qrcps) #:use-module (crates-io))

(define-public crate-emv-qrcps-0.1.1 (c (n "emv-qrcps") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "emv-qrcps-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "03lsyichsrsvyfgi0w0wp3yvcrmhphb8v4ar3z299c942vx2am98") (f (quote (("qr-code-gen") ("default"))))))

