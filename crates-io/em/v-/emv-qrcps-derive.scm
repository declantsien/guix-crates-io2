(define-module (crates-io em v- emv-qrcps-derive) #:use-module (crates-io))

(define-public crate-emv-qrcps-derive-0.1.1 (c (n "emv-qrcps-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xlvgrv8932rb62g0718iavl0qa5bvygihabrqsazyd81yjc8x4c") (y #t)))

(define-public crate-emv-qrcps-derive-0.1.2 (c (n "emv-qrcps-derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fpnwnal5xzhvfbgc2kh6ih713b15iw16537gmdxpcfhdccdrdjh")))

