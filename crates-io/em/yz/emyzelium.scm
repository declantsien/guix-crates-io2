(define-module (crates-io em yz emyzelium) #:use-module (crates-io))

(define-public crate-emyzelium-0.9.2 (c (n "emyzelium") (v "0.9.2") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "15yaxj1z8igv2xma3c992gzk0nzvjhp36q78shsphnx7q209jh6i")))

(define-public crate-emyzelium-0.9.3 (c (n "emyzelium") (v "0.9.3") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1khvhqlmf64i8dnjwvraj7mj6hcrfnh0ps46dbhbh3i63y4grrdk")))

(define-public crate-emyzelium-0.9.4 (c (n "emyzelium") (v "0.9.4") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1dza8alrg06rbsi52rnq1sry0g5r8bxdzvpqpi84zc513wf40p3q")))

(define-public crate-emyzelium-0.9.6 (c (n "emyzelium") (v "0.9.6") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0lwx8s8s55i0c2ix68r2ifyq5jjxpnj1df1p373pi5cmdl2maq6l")))

(define-public crate-emyzelium-0.9.8 (c (n "emyzelium") (v "0.9.8") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "getrandom") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1hccpwlff1pv3nn2vpzj31zxhnx9fwylwz2wvrh33rl4p1gxflrf")))

(define-public crate-emyzelium-0.9.10 (c (n "emyzelium") (v "0.9.10") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "getrandom") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1n6mhgdxb32qsrwqacfs6kg25h8wl0apbamqhjhli694vdr912hc")))

