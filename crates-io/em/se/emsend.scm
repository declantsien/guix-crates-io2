(define-module (crates-io em se emsend) #:use-module (crates-io))

(define-public crate-emsend-0.1.0 (c (n "emsend") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)) (d (n "lettre") (r "^0.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "19wl5s2p6nsnsmp00iz1d2hmmci53yyg68hy0pfyyajc7gkzm8ar")))

