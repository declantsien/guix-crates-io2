(define-module (crates-io em pr empris) #:use-module (crates-io))

(define-public crate-empris-0.1.0 (c (n "empris") (v "0.1.0") (d (list (d (n "dbus") (r "^0.9.2") (d #t) (k 0)) (d (n "dbus-tokio") (r "^0.7.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "macros" "signal"))) (d #t) (k 0)))) (h "199xh9xzqr4qwfmrw2py9mmzv6cwxm2s0mx40j1wjn7vl2cblpba") (y #t)))

(define-public crate-empris-0.1.1 (c (n "empris") (v "0.1.1") (d (list (d (n "dbus") (r "^0.9.2") (d #t) (k 0)) (d (n "dbus-tokio") (r "^0.7.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "macros" "signal"))) (d #t) (k 0)))) (h "03ws2033ag0pvp8lnx9g7w6bij1h9a4rg5xdssib6i6iwjwyr26i") (y #t)))

(define-public crate-empris-0.1.2 (c (n "empris") (v "0.1.2") (d (list (d (n "dbus") (r "^0.9.2") (d #t) (k 0)) (d (n "dbus-tokio") (r "^0.7.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "macros" "signal"))) (d #t) (k 0)))) (h "1s9c6x3jsjj4bbapv8p57j8l5jldh9lrq0i3x1yi941y5xvcdqhb")))

