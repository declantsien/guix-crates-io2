(define-module (crates-io em pt empty_type_traits) #:use-module (crates-io))

(define-public crate-empty_type_traits-0.1.0 (c (n "empty_type_traits") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "07nqyi7dm8v4iqprfwcj4d76ibmw8hr2ysmfc6vv7pkvmp8jjrnq") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-empty_type_traits-0.2.0 (c (n "empty_type_traits") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "04amw8p1dh48dcgmc94hh4qix18ivbsy3nx0jqlfn2gdrn35qx8z") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-empty_type_traits-0.2.1 (c (n "empty_type_traits") (v "0.2.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "14hdk0jpnwpjx0xbwpp4f1hbv3ij0hqslz81hrpxb4nr4m2a6iss") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-empty_type_traits-0.2.2 (c (n "empty_type_traits") (v "0.2.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "06ag2fs1nn2hbp11pxljs86n4l66dm7q1b9g5d7vxdcf20nsp757") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

