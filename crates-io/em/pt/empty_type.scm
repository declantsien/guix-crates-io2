(define-module (crates-io em pt empty_type) #:use-module (crates-io))

(define-public crate-empty_type-0.1.0 (c (n "empty_type") (v "0.1.0") (d (list (d (n "empty_type_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "empty_type_traits") (r "^0.1.0") (d #t) (k 0)))) (h "00yf2j5k9w72l1f5mpml28j0n90zw2zbpgvzxisd6jg2nzbbshpj") (f (quote (("serde" "empty_type_traits/serde" "empty_type_derive/serde") ("derive" "empty_type_derive"))))))

(define-public crate-empty_type-0.2.0 (c (n "empty_type") (v "0.2.0") (d (list (d (n "empty_type_derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "empty_type_traits") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1nlwfisqv8i8z4np81hld8iybf68c94y10x3c0jn2lsw63kywpa4") (f (quote (("serde" "empty_type_traits/serde" "empty_type_derive/serde") ("derive" "empty_type_derive"))))))

(define-public crate-empty_type-0.2.1 (c (n "empty_type") (v "0.2.1") (d (list (d (n "empty_type_derive") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "empty_type_traits") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "13ysmzb40sjv10qwsvbwrx99fpb0lzrri4n7gwc66zw07fkzqzhl") (f (quote (("serde" "empty_type_traits/serde" "empty_type_derive/serde") ("derive" "empty_type_derive"))))))

(define-public crate-empty_type-0.2.2 (c (n "empty_type") (v "0.2.2") (d (list (d (n "empty_type_derive") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "empty_type_traits") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0rv91pasg5gzpriz0zpv3zspfcdf9ghhyw94w9xzbj9vmwz7kx43") (f (quote (("serde" "empty_type_traits/serde" "empty_type_derive/serde") ("derive" "empty_type_derive"))))))

