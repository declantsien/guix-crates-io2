(define-module (crates-io em pt empty-option) #:use-module (crates-io))

(define-public crate-empty-option-0.1.0 (c (n "empty-option") (v "0.1.0") (h "1wxxid9ff8k78zw0awy0l01knphxz0y84cxwjj1x7djhiji9zir5")))

(define-public crate-empty-option-0.1.1 (c (n "empty-option") (v "0.1.1") (h "0jz03ckvir6b48sv6sqg9zfmdh523ivvd9hn6p58815m7knslkd1")))

