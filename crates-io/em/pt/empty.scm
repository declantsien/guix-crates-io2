(define-module (crates-io em pt empty) #:use-module (crates-io))

(define-public crate-empty-0.0.1 (c (n "empty") (v "0.0.1") (d (list (d (n "void") (r "^0.0.5") (d #t) (k 0)))) (h "023p31siypnnd7ryyy5hkasp2ngp7khn74krkjvjd4v800mlr6f6")))

(define-public crate-empty-0.0.2 (c (n "empty") (v "0.0.2") (d (list (d (n "void") (r "^0.0.5") (d #t) (k 0)))) (h "12ff8k3kyv238h0dm1dvldgg7xbynjqwny4yhk845ar90148japx")))

(define-public crate-empty-0.0.3 (c (n "empty") (v "0.0.3") (d (list (d (n "void") (r "^0.0.5") (d #t) (k 0)))) (h "0zgcpdig3k1j1fq5lg53019vbsn2df8g8lb08h1sxb3hnhwx4pz6")))

(define-public crate-empty-0.0.4 (c (n "empty") (v "0.0.4") (d (list (d (n "void") (r "^1") (d #t) (k 0)))) (h "1c0k8v0vblfwz92141px90398p4wfi3s8n4jafprvw9fzdxalbcy")))

