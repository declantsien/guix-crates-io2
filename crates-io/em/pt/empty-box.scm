(define-module (crates-io em pt empty-box) #:use-module (crates-io))

(define-public crate-empty-box-0.1.0 (c (n "empty-box") (v "0.1.0") (h "0lxjl65dmv2q6lq9r1imhnsqaia8q85lslfmm3d6p5wmxyz2piz3")))

(define-public crate-empty-box-0.1.1 (c (n "empty-box") (v "0.1.1") (h "17p0bxm82dcrnw7ndc4j0jd07azp9qb184ig1v2q6g6jxr2l8jmd")))

