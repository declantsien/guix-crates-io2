(define-module (crates-io em pt empty_package_one) #:use-module (crates-io))

(define-public crate-empty_package_one-0.1.0 (c (n "empty_package_one") (v "0.1.0") (h "0f8lg7qf77pjg2n3fh98sld4a9qzpmb22172s7k63h1mcbs7rzz2") (y #t)))

(define-public crate-empty_package_one-0.1.2 (c (n "empty_package_one") (v "0.1.2") (h "1xzzpwq7fr9pc3z5cb0r9a6bh9lmzf75dvckj98wcgkc5d9mxx4g") (y #t)))

(define-public crate-empty_package_one-0.1.1 (c (n "empty_package_one") (v "0.1.1") (h "1vrxzjzqpwjxcqzs126rjn6r6y37fm5qng25kgwp97gfvmas6vxg") (y #t)))

(define-public crate-empty_package_one-0.1.3 (c (n "empty_package_one") (v "0.1.3") (h "10psxcxyhrydzl2bbb3lzlkghpagbcg4scb1yf61mwm9gn7zbrnm") (y #t)))

