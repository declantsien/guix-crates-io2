(define-module (crates-io em l- eml-codec) #:use-module (crates-io))

(define-public crate-eml-codec-0.1.0 (c (n "eml-codec") (v "0.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "12g94zm5ss609jm8qyn1cpykjhpkrg2gig4bcj1wq5akrvjfshx4")))

(define-public crate-eml-codec-0.1.1 (c (n "eml-codec") (v "0.1.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "03f96vdvhh7h12a5cr9nln1w5diwwqx3bnd5zy2j7xya6zswy85c")))

(define-public crate-eml-codec-0.1.2 (c (n "eml-codec") (v "0.1.2") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0bkdsj1swh630z383pdyiz1sp2zs03lfr5pgaxmf5g3sv0j92jfl")))

