(define-module (crates-io em l- eml-parser) #:use-module (crates-io))

(define-public crate-eml-parser-0.1.0 (c (n "eml-parser") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0f82z4ryz4wchr0n99gj20s093ygx8ps0dqq6pr01ljcw8a0vqyr")))

(define-public crate-eml-parser-0.1.2 (c (n "eml-parser") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0y2makpz4sq40ywmphywqbgs8mg8dik5bdbnbcfbij6f29ky67q3")))

(define-public crate-eml-parser-0.1.3 (c (n "eml-parser") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08g8cp47kyb0vws2h3jvhhmwn8lp1vhn37mmfmv4g3k5fipgrrj3")))

(define-public crate-eml-parser-0.1.4 (c (n "eml-parser") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rfc2047-decoder") (r "^1.0.1") (d #t) (k 0)))) (h "0bhgx1i952g2v7w0icnqkylvhfiyb2am2mbw89v8zq0jz0aqvnx7")))

