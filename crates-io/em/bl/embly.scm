(define-module (crates-io em bl embly) #:use-module (crates-io))

(define-public crate-embly-0.0.1 (c (n "embly") (v "0.0.1") (h "1mgqayvfq55zkzd3rf7mc5743lg90r1cdjhbi5hxhs9kzcg0sdld")))

(define-public crate-embly-0.0.2 (c (n "embly") (v "0.0.2") (h "0p2mlmafjlvmzj396q374wi42qs83k2j3aw8aym6blsmhsjkzb31")))

(define-public crate-embly-0.0.3 (c (n "embly") (v "0.0.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "0b8mm04hsnczb42dj7jlz47jh0fknxr8qg9ylzqfnj54cyc35m02")))

(define-public crate-embly-0.0.4 (c (n "embly") (v "0.0.4") (d (list (d (n "failure") (r "~0.1") (f (quote ("std"))) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.18") (o #t) (d #t) (k 0)) (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "0a25pnp4nfwl87rk77vaq49xsc10rnh248br0chjgagqx6nd37in") (f (quote (("build_protos" "futures-preview"))))))

(define-public crate-embly-0.0.5 (c (n "embly") (v "0.0.5") (d (list (d (n "failure") (r "~0.1") (f (quote ("std"))) (k 0)) (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.6.2") (d #t) (k 0)))) (h "0v5kq3318v0zk2gabbyrzz5wr77y339kwr3hd2x8a28p5d3m4b8p")))

