(define-module (crates-io em bl embly-wrapper) #:use-module (crates-io))

(define-public crate-embly-wrapper-0.0.2 (c (n "embly-wrapper") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "failure") (r "~0.1") (f (quote ("std"))) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lucet-runtime") (r "^0.4.1") (d #t) (k 0)) (d (n "lucet-runtime-internals") (r "^0.4.1") (d #t) (k 0)) (d (n "lucet-wasi") (r "^0.4.1") (d #t) (k 0)) (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "08l0s3cw6h44r2mnx1x4409pzp5wm20mslq1shxl2b0crjhrinnh")))

