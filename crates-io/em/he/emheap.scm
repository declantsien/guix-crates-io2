(define-module (crates-io em he emheap) #:use-module (crates-io))

(define-public crate-emheap-0.1.0 (c (n "emheap") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0s83wrmmdbi671svcrp8www762m3d3c26p1b94a94d77xlpq47jl")))

(define-public crate-emheap-0.1.1 (c (n "emheap") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0d9kx0z2057667jyfqa5dv3mzyqzjfcb87k9xk5zv03v9dra8rhd")))

