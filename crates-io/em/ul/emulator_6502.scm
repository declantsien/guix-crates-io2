(define-module (crates-io em ul emulator_6502) #:use-module (crates-io))

(define-public crate-emulator_6502-0.1.0 (c (n "emulator_6502") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0glwh30knxzm6jw275w9rc5787p6hmgi4rz0w9zd6ay72xhxns26") (f (quote (("default") ("binary_coded_decimal"))))))

(define-public crate-emulator_6502-0.2.0 (c (n "emulator_6502") (v "0.2.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1jk46jhwdvssf4zlarp7kj7irqh5hdwk1xa27665wypy0mwc8z9z") (f (quote (("illegal_opcodes") ("default") ("binary_coded_decimal"))))))

(define-public crate-emulator_6502-1.0.0 (c (n "emulator_6502") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0c6jl30a58w3dalayph5ac549894bd5pks586x07fx70d311378r") (f (quote (("illegal_opcodes") ("default") ("binary_coded_decimal"))))))

(define-public crate-emulator_6502-1.1.0 (c (n "emulator_6502") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "0l4cb0xc4icygzqd6ggn21d062j836bba2wln9vndg3jc2a872wf") (f (quote (("implementation_transparency") ("illegal_opcodes") ("default") ("binary_coded_decimal"))))))

