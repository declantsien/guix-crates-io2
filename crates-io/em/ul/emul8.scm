(define-module (crates-io em ul emul8) #:use-module (crates-io))

(define-public crate-emul8-0.1.0 (c (n "emul8") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1xyiva96rb6fpl296k0qwb5d8mwc50xdn2yppkwzbqj8r8ks721v")))

(define-public crate-emul8-0.1.1 (c (n "emul8") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1gp1rgj95vr3rppn7r96wkywphys83gyh9ma7shcxafssh4qfbxv")))

(define-public crate-emul8-0.1.2 (c (n "emul8") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "188wd91ip4a6n7708bhya84iv77fn5v5263vn2ddapzffrymqs2x")))

