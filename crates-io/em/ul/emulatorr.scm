(define-module (crates-io em ul emulatorr) #:use-module (crates-io))

(define-public crate-emulatorr-0.1.0-alpha (c (n "emulatorr") (v "0.1.0-alpha") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "mpsc") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "stdr") (r "^0.1.1") (d #t) (k 0)) (d (n "thread") (r "^0.1.0") (d #t) (k 0)))) (h "08bby2rk3npcy6d3vpbzsfidnwzj3yxv948lw699hv853rm49as4")))

