(define-module (crates-io em d_ emd_earcutr) #:use-module (crates-io))

(define-public crate-emd_earcutr-0.1.0 (c (n "emd_earcutr") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 2)))) (h "11z1c5k389jm7m29j5m1d63ff9s14ma31z607zyprf0clhd5xcna")))

