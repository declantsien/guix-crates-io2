(define-module (crates-io em kd emkdir) #:use-module (crates-io))

(define-public crate-emkdir-0.0.0 (c (n "emkdir") (v "0.0.0") (d (list (d (n "cplogger") (r "^0.0.1") (d #t) (k 0)) (d (n "die") (r "^0.2.0") (d #t) (k 0)) (d (n "io") (r "^0.0.2") (d #t) (k 0)))) (h "0rl70cn46h0kg0rfplzb68dc2bzxgq51j1n9inzqvxk99hviwh1c")))

(define-public crate-emkdir-0.0.1 (c (n "emkdir") (v "0.0.1") (d (list (d (n "cplogger") (r "^0.0.2") (d #t) (k 0)) (d (n "die") (r "^0.2.0") (d #t) (k 0)))) (h "17fn62wralpb2j6gz92rhl177f15yvnc1dghpgcpv63mg7am6mfs")))

(define-public crate-emkdir-0.0.2 (c (n "emkdir") (v "0.0.2") (d (list (d (n "cplogger") (r "^0.0.2") (d #t) (k 0)) (d (n "die") (r "^0.2.0") (d #t) (k 0)))) (h "02myrh9z76fcicb2sp1sw8x7ykiwqvji580a9r04r43ixw55jmx4")))

