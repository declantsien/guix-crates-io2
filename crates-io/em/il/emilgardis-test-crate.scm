(define-module (crates-io em il emilgardis-test-crate) #:use-module (crates-io))

(define-public crate-emilgardis-test-crate-0.0.1 (c (n "emilgardis-test-crate") (v "0.0.1") (h "1s6kllknc37dzns5kr5kljw6qi42dbfnvb0cmggym9mkf3xr8nqx")))

(define-public crate-emilgardis-test-crate-0.0.2 (c (n "emilgardis-test-crate") (v "0.0.2") (h "1bdag16m894y7apjvlafw6qr56h0paflf8pnlkg5mnmp17gshih9")))

(define-public crate-emilgardis-test-crate-0.0.3-rc.1 (c (n "emilgardis-test-crate") (v "0.0.3-rc.1") (h "1ywa8dlnzhpq276ld8rfzv8ry7fwyrf17ivpswwcjlyx072arslg")))

(define-public crate-emilgardis-test-crate-0.0.3 (c (n "emilgardis-test-crate") (v "0.0.3") (h "0ghnqxx9jlakv5g03xn9jkzi826xdy2bxlyqjw8nqcvqk0dfpqg6")))

