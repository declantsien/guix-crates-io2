(define-module (crates-io jl in jlink_rtt) #:use-module (crates-io))

(define-public crate-jlink_rtt-0.1.0 (c (n "jlink_rtt") (v "0.1.0") (h "1w87x0ij387ahjs1mdvsbfl6a2d256rxnbaq95mnlf66lbhp3439")))

(define-public crate-jlink_rtt-0.2.0 (c (n "jlink_rtt") (v "0.2.0") (h "1hnzmshgmykccy5b6cn45059pygy6dlms95mjmqbsr2g2m0azxj2") (f (quote (("rtt_in_ram"))))))

