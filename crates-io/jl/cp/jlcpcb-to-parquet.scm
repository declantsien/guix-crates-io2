(define-module (crates-io jl cp jlcpcb-to-parquet) #:use-module (crates-io))

(define-public crate-jlcpcb-to-parquet-0.3.0 (c (n "jlcpcb-to-parquet") (v "0.3.0") (d (list (d (n "polars") (r "^0.37.0") (f (quote ("parquet" "lazy" "object" "streaming"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00scm4m84l7g22mjylxqm81cxdgc20x8hq64wxs6nap8gy37qqbv")))

(define-public crate-jlcpcb-to-parquet-0.3.1 (c (n "jlcpcb-to-parquet") (v "0.3.1") (d (list (d (n "polars") (r "^0.37.0") (f (quote ("parquet" "lazy" "object" "streaming"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yjm6przw4ivrvsc4rr580l3nkpgqwc29b1zssvki0hmbhxhm86b")))

(define-public crate-jlcpcb-to-parquet-0.3.2 (c (n "jlcpcb-to-parquet") (v "0.3.2") (d (list (d (n "polars") (r "^0.37.0") (f (quote ("parquet" "lazy" "object" "streaming"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03jxc16d6v127jdvj9flv1c1jcf96gfv1nzpwlwbnyrv5x38w3b1")))

(define-public crate-jlcpcb-to-parquet-0.3.3 (c (n "jlcpcb-to-parquet") (v "0.3.3") (d (list (d (n "polars") (r "^0.37.0") (f (quote ("parquet" "lazy" "object" "streaming"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cnkjzgzl1l9iwrrw2fmyyb8mrqp608rjbbmbcbmk2wikw8a0sg3")))

(define-public crate-jlcpcb-to-parquet-0.3.4 (c (n "jlcpcb-to-parquet") (v "0.3.4") (d (list (d (n "polars") (r "^0.37.0") (f (quote ("parquet" "lazy" "object" "streaming"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16dajmpwb7jrbn4pfc693ymsgy2gcnfk9s2fbwvsdjyzbi2vpb1i")))

(define-public crate-jlcpcb-to-parquet-0.3.5 (c (n "jlcpcb-to-parquet") (v "0.3.5") (d (list (d (n "polars") (r "^0.37.0") (f (quote ("parquet" "lazy" "object" "streaming"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0swhdr2vy9x6al6a00hpxrwr4ypp65f5gqcjybhmprkkb76mwbf1")))

(define-public crate-jlcpcb-to-parquet-0.3.6 (c (n "jlcpcb-to-parquet") (v "0.3.6") (d (list (d (n "polars") (r "^0.37.0") (f (quote ("parquet" "lazy" "object" "streaming"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ssryxh0g8xb4zy5xn825i3fm29sz2ssvpd2xzq4a2lnzm2vziml")))

(define-public crate-jlcpcb-to-parquet-0.4.0 (c (n "jlcpcb-to-parquet") (v "0.4.0") (d (list (d (n "polars") (r "^0.37.0") (f (quote ("parquet" "lazy" "object" "streaming"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c05qjb4zsvm02fgxdw6lwr9l1iqa4b3vd0x5591yh64a2ijav96")))

(define-public crate-jlcpcb-to-parquet-0.4.1 (c (n "jlcpcb-to-parquet") (v "0.4.1") (d (list (d (n "polars") (r "^0.37.0") (f (quote ("parquet" "lazy" "object" "streaming"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14ywwvb1bmf8cy48bdskxpfra34adxpl8glmpsxn27q5mnaxsd2p")))

(define-public crate-jlcpcb-to-parquet-0.4.2 (c (n "jlcpcb-to-parquet") (v "0.4.2") (d (list (d (n "polars") (r "^0.37.0") (f (quote ("parquet" "lazy" "object" "streaming"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("sqlite" "runtime-tokio"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08prc85j9jm3m8hhdaxz6y2r8a603014wp9hycsvfyq6ycdz1hd0")))

