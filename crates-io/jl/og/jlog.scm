(define-module (crates-io jl og jlog) #:use-module (crates-io))

(define-public crate-jlog-0.1.0 (c (n "jlog") (v "0.1.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.15") (d #t) (k 0)))) (h "1d9pdgi0fpbsy7fbp7jz89mby192fvnl7i9hnbv6qn396sn6ahjs")))

(define-public crate-jlog-0.1.1 (c (n "jlog") (v "0.1.1") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.15") (d #t) (k 0)))) (h "083aq8j49nx5c5qnx7i8vncx6fjjwsww87fpyjqx3ri7xpn9hywc")))

(define-public crate-jlog-0.1.2 (c (n "jlog") (v "0.1.2") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.15") (d #t) (k 0)))) (h "0v3fpgypdmy5hns61asb18xd0wn91hp2xl6ppldzs26xx28pb639")))

(define-public crate-jlog-0.1.3 (c (n "jlog") (v "0.1.3") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.15") (d #t) (k 0)))) (h "1vfq8ki1sqqb9zg3sinh38icpdizjk11va0k7kg9jk2zr6n5j3sc")))

(define-public crate-jlog-0.1.4 (c (n "jlog") (v "0.1.4") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.15") (d #t) (k 0)))) (h "14rhpp4azzbqsyz2r505b4sg5sa4rwp9xq7m6zyyx768xxinbkfx")))

(define-public crate-jlog-0.1.5 (c (n "jlog") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.15") (d #t) (k 0)))) (h "1f6s3rc76cm6qsa9s4jf73a3w2x3gbkcwdi4ldvcnn7850d52id9")))

(define-public crate-jlog-0.2.1 (c (n "jlog") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.15") (d #t) (k 0)))) (h "0x0vpq1l7rsv52q6gsn4c6v6si943zwa4kdbq6lsy7wiq4pk8ghd")))

