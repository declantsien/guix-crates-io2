(define-module (crates-io jl og jlogger-tracing) #:use-module (crates-io))

(define-public crate-jlogger-tracing-0.1.1 (c (n "jlogger-tracing") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 2)) (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1jhlbxispgz7mvqins24kahhy4zn4kilfyx645ivkr9lqvp1pnqk")))

(define-public crate-jlogger-tracing-0.1.2 (c (n "jlogger-tracing") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 2)) (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "064p3lgc3k9ns8hak42rpmsw0nfsgq8phl982zr51qlvgkqmalwi")))

(define-public crate-jlogger-tracing-0.1.3 (c (n "jlogger-tracing") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 2)) (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0km4cp94nps1lxjijwpd26s09839k1mr0k73mvbrd8r7cns6kfh7")))

(define-public crate-jlogger-tracing-0.1.4 (c (n "jlogger-tracing") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 2)) (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1r2gpnpqwxlmxa4dhxfx8yx7ni9pg0hbd8ngww0flym4vh369hcn")))

