(define-module (crates-io jl og jlogger) #:use-module (crates-io))

(define-public crate-jlogger-0.1.0 (c (n "jlogger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1lmhy85idp6981c1gj1if4wskcyajpl881na1q22vjcys3mpy2g8") (y #t)))

(define-public crate-jlogger-0.1.1 (c (n "jlogger") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1k456qfxsv2nz3mpybhmglpnqakwvjdfvkpcf5kfhk8zq8x89hmr")))

(define-public crate-jlogger-0.1.2 (c (n "jlogger") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1107pz1ky7hadyz7vhy5in6qha2vx0xpwx9vgx5pp0625i3vbfs5")))

(define-public crate-jlogger-0.1.3 (c (n "jlogger") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "05pmarix0fxgcyr48hpxw3yc3891s2ip4zdydp85bf8hxjgbhpr2")))

(define-public crate-jlogger-0.1.4 (c (n "jlogger") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1p3m4yzng5570qnbr5k1brjm8q454pnwvg82d2psdg3qmyi4ija4")))

(define-public crate-jlogger-0.1.5 (c (n "jlogger") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "17sm55d6ksc8v9hh54431winfxinkv686kbp517sj61y41bc48g1")))

(define-public crate-jlogger-0.1.6 (c (n "jlogger") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0ivr8p21rjfsya807yzixi7kkqsk859sca549x3is6ih8g0kkpn8")))

(define-public crate-jlogger-0.1.7 (c (n "jlogger") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 2)) (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0d6zai44pjp1yk56gk4jh4sw51kjwvqwjcinz54bacd21c4265w3")))

