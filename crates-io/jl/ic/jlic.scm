(define-module (crates-io jl ic jlic) #:use-module (crates-io))

(define-public crate-jlic-0.1.0 (c (n "jlic") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.17") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde_toml") (r "^0.0.1") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1s2qbcjjgmi8632bvcj7fkra1m54pya506yfmlm8wxya0b7lwv11")))

