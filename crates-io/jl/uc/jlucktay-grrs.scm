(define-module (crates-io jl uc jlucktay-grrs) #:use-module (crates-io))

(define-public crate-jlucktay-grrs-0.1.0 (c (n "jlucktay-grrs") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1wy5vzyqnhrh2i0lmmhfsc460rbdbjzahndhi3698y8mfzzb6d8n")))

(define-public crate-jlucktay-grrs-0.2.0 (c (n "jlucktay-grrs") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_mangen") (r "^0.1.10") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "09ar6xv5n0v680biy9r96f8hr2vsaffcaxcn01w30n5rp9plfdlq")))

