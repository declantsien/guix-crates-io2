(define-module (crates-io jl ab jlabel) #:use-module (crates-io))

(define-public crate-jlabel-0.1.0 (c (n "jlabel") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0cg8zqxmlk3a7749r9c5dqw5kfmyx57q6fdmyk5bd44wm0qly6qc") (r "1.65.0")))

(define-public crate-jlabel-0.1.1 (c (n "jlabel") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1sc0v8yafzdxrmj4lichhrjyl8jg8v5sah04hia26nz65xgynm5w") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65.0")))

(define-public crate-jlabel-0.1.2 (c (n "jlabel") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0j1ik006vddxy6hlih4r158wqjdy6n3bz786jrr9fa2nqli0n10z") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65.0")))

(define-public crate-jlabel-0.1.3 (c (n "jlabel") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1bbyf4330wpv9v6jdswgbiv5y3p0mi2bmqi7xfpyhzmkkbjy25di") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65.0")))

(define-public crate-jlabel-0.1.4 (c (n "jlabel") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1dzh4bb35c5z5zcvd5cx8h6461rx206zmpa9dpg0q6l7jpsfcphl") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65.0")))

