(define-module (crates-io jl ab jlabel-question) #:use-module (crates-io))

(define-public crate-jlabel-question-0.1.0 (c (n "jlabel-question") (v "0.1.0") (d (list (d (n "jlabel") (r "^0.1.0") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1d7cwq231ccw9l882vap4gbfb2v84vgi094ydk0a1n3whiwvg58a") (s 2) (e (quote (("regex" "dep:regex-automata" "dep:regex-syntax")))) (r "1.65.0")))

(define-public crate-jlabel-question-0.1.1 (c (n "jlabel-question") (v "0.1.1") (d (list (d (n "jlabel") (r "^0.1.1") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "01f0y5f858shd2c1sqbnzp03j5fvg98npdq0hn9gccl5pfx8fvrl") (s 2) (e (quote (("regex" "dep:regex-automata" "dep:regex-syntax")))) (r "1.65.0")))

(define-public crate-jlabel-question-0.1.2 (c (n "jlabel-question") (v "0.1.2") (d (list (d (n "jlabel") (r "^0.1.2") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1a7m37jkgg9bmh4sbb0mn6mbvfyba4xhccxvcs0sisglssl6h77y") (s 2) (e (quote (("regex" "dep:regex-automata" "dep:regex-syntax")))) (r "1.65.0")))

(define-public crate-jlabel-question-0.1.3 (c (n "jlabel-question") (v "0.1.3") (d (list (d (n "jlabel") (r "^0.1.3") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1r9jrlf6b4qvybb3nx2900lz9gdg21p95r1jpg2xbhk57h7sjiqc") (s 2) (e (quote (("regex" "dep:regex-automata" "dep:regex-syntax")))) (r "1.65.0")))

(define-public crate-jlabel-question-0.1.4 (c (n "jlabel-question") (v "0.1.4") (d (list (d (n "jlabel") (r "^0.1.4") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "05siap13z3bmhap3s4dcdr0q3xc2d3cykqgw9dffmzacdgjl8kx2") (s 2) (e (quote (("serde" "dep:serde") ("regex" "dep:regex-automata" "dep:regex-syntax")))) (r "1.65.0")))

