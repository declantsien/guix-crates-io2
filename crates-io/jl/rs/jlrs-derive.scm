(define-module (crates-io jl rs jlrs-derive) #:use-module (crates-io))

(define-public crate-jlrs-derive-0.1.0 (c (n "jlrs-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vi24rwsirpj0wgnjiwbbnnwvmacb7b2w2fp1hxs7l706d6qz19s")))

(define-public crate-jlrs-derive-0.2.0 (c (n "jlrs-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "12k2f0fflhwkz9hp8sfp42dkgkzhghbiypza84j1nfmcm5bjam1m")))

(define-public crate-jlrs-derive-0.3.0 (c (n "jlrs-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1x7406zxr3nwvdlqs995nb4nlg7vr2rga5ncjy4w5rw047h27p6q")))

(define-public crate-jlrs-derive-0.4.0 (c (n "jlrs-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1igaq3jc7sylssj4az0sfi88gp6kkq5109s22wlxxs8n4lfdn1z2")))

(define-public crate-jlrs-derive-0.5.0 (c (n "jlrs-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0v5rc1mwsjzkmvh8rms2gkxmhzxwqslannp99c1bnsy151ldzx60")))

