(define-module (crates-io jl rs jlrs-ndarray) #:use-module (crates-io))

(define-public crate-jlrs-ndarray-0.1.0 (c (n "jlrs-ndarray") (v "0.1.0") (d (list (d (n "jlrs") (r "^0.7.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)))) (h "05zbc9jkb08rzaxj7z0wv0rj8i43n6c0hg9rlbcphfq87lz8fj2n")))

(define-public crate-jlrs-ndarray-0.1.1 (c (n "jlrs-ndarray") (v "0.1.1") (d (list (d (n "jlrs") (r "^0.7.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)))) (h "0032p2cixnqs9sl9rqzp1hw7v7wr3kc9vq5pyf51bsy7x2yx9836") (f (quote (("docs-rs" "jlrs/docs-rs"))))))

(define-public crate-jlrs-ndarray-0.2.0 (c (n "jlrs-ndarray") (v "0.2.0") (d (list (d (n "jlrs") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (d #t) (k 0)))) (h "086r2gz923ipa26kg8zgq8fcanm8685z8yfldrah3p9i1i61n0rj") (f (quote (("docs-rs" "jlrs/docs-rs"))))))

(define-public crate-jlrs-ndarray-0.3.0 (c (n "jlrs-ndarray") (v "0.3.0") (d (list (d (n "jlrs") (r "^0.9") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (d #t) (k 0)))) (h "079xlw915fj4m8nalw09xf3nnvd7y076bkw0hqrfm1s1nnpin73d") (f (quote (("docs-rs" "jlrs/docs-rs"))))))

