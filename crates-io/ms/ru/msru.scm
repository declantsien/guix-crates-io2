(define-module (crates-io ms ru msru) #:use-module (crates-io))

(define-public crate-msru-0.1.0 (c (n "msru") (v "0.1.0") (h "1habkzxw7ig0s6iaw281j03a7wd6f9y4fbbkhnl1n08yv0kd3xw7") (r "1.59")))

(define-public crate-msru-0.2.0 (c (n "msru") (v "0.2.0") (h "1aiy58yzy0rppfsgir1djbcm38fb7q3fpv02xnggss7hiqh1980m") (r "1.59")))

