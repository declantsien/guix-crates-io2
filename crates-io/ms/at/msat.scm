(define-module (crates-io ms at msat) #:use-module (crates-io))

(define-public crate-msat-0.1.0 (c (n "msat") (v "0.1.0") (d (list (d (n "rsat") (r "=0.1.11") (d #t) (k 0)))) (h "0f6ljdzvgm5hj9axbxjraw62k44mmzpqac215c4xfi0r810cavyj")))

(define-public crate-msat-0.1.1 (c (n "msat") (v "0.1.1") (d (list (d (n "rsat") (r "=0.1.12") (d #t) (k 0)) (d (n "solhop-types") (r "=0.1.0") (d #t) (k 0)))) (h "1vnwsmrhj8xs4qv8fh6xkj9y3jzv2mzmfalfdik1m53dz48iwplq")))

