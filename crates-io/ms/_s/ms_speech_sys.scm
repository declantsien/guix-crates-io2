(define-module (crates-io ms _s ms_speech_sys) #:use-module (crates-io))

(define-public crate-ms_speech_sys-0.1.0 (c (n "ms_speech_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.3") (d #t) (k 1)))) (h "1h5aga8rlax0sznhbpzbgfqn9y2633dhafb81x15sza9dsikn090")))

(define-public crate-ms_speech_sys-0.1.1 (c (n "ms_speech_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)))) (h "0bvp4hr2s1pak6w5zpri2sf34gcrnrgmwni730km9pzzcxwnhcxk")))

(define-public crate-ms_speech_sys-0.1.2 (c (n "ms_speech_sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)))) (h "0xrlbkvhbsz84lpfzw9g0ij0dy8x2y4qp0ad56gnydlgid3xp3nm")))

(define-public crate-ms_speech_sys-0.1.3 (c (n "ms_speech_sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)))) (h "01s7mlwcm60cf8hpsg9hcqyw59mmkspv7aw1xfyp5bjjpmpwfnpc")))

(define-public crate-ms_speech_sys-0.2.0 (c (n "ms_speech_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)))) (h "07x29shh68jx163jawpibr2vahqpm1y2dx2vmjj51z138m8rn77r")))

(define-public crate-ms_speech_sys-0.2.1 (c (n "ms_speech_sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)))) (h "1l3af5diirn9wzkd97qghz2xlb363m2bvwbnxpas60dzgvypxrj7")))

