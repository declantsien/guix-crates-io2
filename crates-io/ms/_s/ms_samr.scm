(define-module (crates-io ms _s ms_samr) #:use-module (crates-io))

(define-public crate-ms_samr-0.0.1 (c (n "ms_samr") (v "0.0.1") (d (list (d (n "ms_dtyp") (r "^0.0") (d #t) (k 0)))) (h "1kj1pyzip17zhg9ik24lqmw4iyw9wnms24xm4yszsa2y98lvrhvv")))

(define-public crate-ms_samr-0.0.2 (c (n "ms_samr") (v "0.0.2") (d (list (d (n "ms_dtyp") (r "^0.0") (d #t) (k 0)))) (h "06d7gkcikap67319wjr9jbjkj4ckvk3a60r3d6b1dxxqqn08c54f")))

