(define-module (crates-io ms -o ms-oforms) #:use-module (crates-io))

(define-public crate-ms-oforms-0.1.0 (c (n "ms-oforms") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "nom-methods") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "089nb3kn887ghih4zbm0wa2nb4fd2c8cz86kzwk1dv90mi2dv8hq")))

(define-public crate-ms-oforms-0.2.0 (c (n "ms-oforms") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "nom-methods") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0g539hrmgg6g82p6q7vxj2azvllna6wq7nbsczi6dm5d3a97p8p0")))

(define-public crate-ms-oforms-0.3.0-alpha1 (c (n "ms-oforms") (v "0.3.0-alpha1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "nom-methods") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1gaisv07wkprj66510njfana9wkwbh3sd460brcbb3axnwabkwyl")))

