(define-module (crates-io ms f- msf-webrtc) #:use-module (crates-io))

(define-public crate-msf-webrtc-0.1.0 (c (n "msf-webrtc") (v "0.1.0") (d (list (d (n "msf-ice") (r "^0.1") (d #t) (k 0)) (d (n "msf-rtp") (r "^0.1") (d #t) (k 0)) (d (n "msf-sdp") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "msf-srtp") (r "^0.1") (d #t) (k 0)))) (h "14aqf9hm038b3918x2mn5d5izh40jdlwskypv2p7wgfif1i3vl5a") (f (quote (("sdp" "msf-sdp/ice") ("logging-slog" "msf-ice/slog") ("logging" "msf-ice/log"))))))

(define-public crate-msf-webrtc-0.2.0 (c (n "msf-webrtc") (v "0.2.0") (d (list (d (n "msf-ice") (r "^0.2") (d #t) (k 0)) (d (n "msf-rtp") (r "^0.2") (d #t) (k 0)) (d (n "msf-sdp") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "msf-srtp") (r "^0.2") (d #t) (k 0)))) (h "0hhblwvkdxgra0gbir4v0vfpp9dx2l0j3f5wn1p6lzdr00f1p976") (f (quote (("sdp" "msf-sdp/ice") ("logging-slog" "msf-ice/slog") ("logging" "msf-ice/log"))))))

