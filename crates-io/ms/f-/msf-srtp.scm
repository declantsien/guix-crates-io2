(define-module (crates-io ms f- msf-srtp) #:use-module (crates-io))

(define-public crate-msf-srtp-0.1.0 (c (n "msf-srtp") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "msf-rtp") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1znlrgplrh4fshlg0dimw4q9m1lzrs0jh90ckm8k829kffqvmizf")))

(define-public crate-msf-srtp-0.2.0 (c (n "msf-srtp") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "msf-rtp") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "18b25hqzfx3g73lnzm8f4ch4lxnmy6lya41qhzcpvm6dwsva5p89")))

