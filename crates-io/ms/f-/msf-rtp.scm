(define-module (crates-io ms f- msf-rtp) #:use-module (crates-io))

(define-public crate-msf-rtp-0.1.0 (c (n "msf-rtp") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "1b7xhzxz4smknqqdzx2w8p90x0qkwa4l8f7adv0i704m8n7hid53")))

(define-public crate-msf-rtp-0.2.0 (c (n "msf-rtp") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "msf-sdp") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "msf-util") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "14p1cnkcv8ayg029b1my5737r8s3z1miwp8d1qy46rcfzbxp7qhk") (f (quote (("pcm" "msf-sdp") ("h264" "msf-util/h264") ("default"))))))

