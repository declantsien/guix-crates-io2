(define-module (crates-io ms f- msf-ice) #:use-module (crates-io))

(define-public crate-msf-ice-0.1.0 (c (n "msf-ice") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "msf-stun") (r "^0.1") (f (quote ("ice"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "slog") (r "^2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "rt" "time"))) (d #t) (k 0)))) (h "0ngkdraaqa7yrnz08qriarxmkn2ww1gmand6rk9bvjhv3cx6aj2c")))

(define-public crate-msf-ice-0.2.0 (c (n "msf-ice") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "msf-stun") (r "^0.1") (f (quote ("ice"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "slog") (r "^2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "rt" "time"))) (d #t) (k 0)))) (h "0rzbcrf3idvk0ggww3h5271y6lsicfxv5pia5r2q1hic7i9kcf3g")))

(define-public crate-msf-ice-0.2.1 (c (n "msf-ice") (v "0.2.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "msf-stun") (r "^0.1") (f (quote ("ice"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "slog") (r "^2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "rt" "time"))) (d #t) (k 0)))) (h "06qjrav5db09zny9fj184gd7lynxgipmvbsr7wlabyffwvylzcgl")))

