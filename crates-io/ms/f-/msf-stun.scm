(define-module (crates-io ms f- msf-stun) #:use-module (crates-io))

(define-public crate-msf-stun-0.1.0 (c (n "msf-stun") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^2") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (d #t) (k 0)))) (h "17ypiqjj0h0sy7cnbsql0jwfhc1j1s4akmcg5hhfxvkp13cly0ln") (f (quote (("ice") ("default"))))))

(define-public crate-msf-stun-0.1.1 (c (n "msf-stun") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (d #t) (k 0)))) (h "185ziqgxn70i4dzaag6h5wlhrbydjsn450ih05i1rxzc5w0x64ck") (f (quote (("ice") ("default"))))))

