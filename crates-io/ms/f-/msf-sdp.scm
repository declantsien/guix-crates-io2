(define-module (crates-io ms f- msf-sdp) #:use-module (crates-io))

(define-public crate-msf-sdp-0.1.0 (c (n "msf-sdp") (v "0.1.0") (d (list (d (n "msf-ice") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "str-reader") (r "^0.1") (d #t) (k 0)))) (h "1g0wmna48mw4rxzvbsfkm48i6d6s1akzazzsn3dqsprzliphh68r") (f (quote (("ice" "msf-ice") ("default"))))))

(define-public crate-msf-sdp-0.2.0 (c (n "msf-sdp") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "msf-ice") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "msf-util") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "str-reader") (r "^0.1") (d #t) (k 0)))) (h "024mv1v4rhxvfpf5j1yqp1niz711km8pih8942v1lq5a193144c5") (f (quote (("ice" "msf-ice") ("h264" "base64" "bytes" "msf-util/h264") ("default"))))))

(define-public crate-msf-sdp-0.2.1 (c (n "msf-sdp") (v "0.2.1") (d (list (d (n "base64") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "msf-ice") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "msf-util") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "str-reader") (r "^0.1") (d #t) (k 0)))) (h "1zn6pl19gcrml0hd67m8c363szmq4hsmhn2wip91i0xg5qc3n0wq") (f (quote (("ice" "msf-ice") ("h264" "base64" "bytes" "msf-util/h264") ("default"))))))

