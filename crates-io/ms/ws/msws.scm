(define-module (crates-io ms ws msws) #:use-module (crates-io))

(define-public crate-msws-0.1.0 (c (n "msws") (v "0.1.0") (h "11dvqkj32jnmp9zhswr3a9dhnlh49kw1ly7aa21h4isziz18a61v")))

(define-public crate-msws-0.2.0 (c (n "msws") (v "0.2.0") (h "1vfdknsx106pmddqzhyllsfzhj808q9j4wizndr4hkanrygcba3d")))

