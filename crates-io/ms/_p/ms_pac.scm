(define-module (crates-io ms _p ms_pac) #:use-module (crates-io))

(define-public crate-ms_pac-0.0.1 (c (n "ms_pac") (v "0.0.1") (d (list (d (n "ms_adts") (r "^0.0") (d #t) (k 0)) (d (n "ms_dtyp") (r "^0.0") (d #t) (k 0)) (d (n "ms_samr") (r "^0.0") (d #t) (k 2)))) (h "1flzsq016dychzysl4wq4s7bvvpwz03a0z0n977hsxh51087k82g")))

(define-public crate-ms_pac-0.0.2 (c (n "ms_pac") (v "0.0.2") (d (list (d (n "ms_adts") (r "^0.0") (d #t) (k 0)) (d (n "ms_dtyp") (r "^0.0") (d #t) (k 0)) (d (n "ms_samr") (r "^0.0") (d #t) (k 2)))) (h "0vadcvfvv53jgix0jsqk2cmhcyxyzwna3k78pk41kqh89y2aan3f")))

(define-public crate-ms_pac-0.0.3 (c (n "ms_pac") (v "0.0.3") (d (list (d (n "ms_adts") (r "^0.0") (d #t) (k 0)) (d (n "ms_dtyp") (r "^0.0") (d #t) (k 0)) (d (n "ms_samr") (r "^0.0") (d #t) (k 2)))) (h "1q4ix157q9902vpikswf7rgbs9yn48ng0vvmcw29adznmfsc2l8x")))

(define-public crate-ms_pac-0.0.4 (c (n "ms_pac") (v "0.0.4") (d (list (d (n "ms_adts") (r "^0.0") (d #t) (k 0)) (d (n "ms_dtyp") (r "^0.0") (d #t) (k 0)) (d (n "ms_samr") (r "^0.0") (d #t) (k 2)))) (h "15yj66i4hkf5cs7y7ics2blj9fby7ry4drl1icy8yphj3n72rnkl")))

