(define-module (crates-io ms -d ms-detours) #:use-module (crates-io))

(define-public crate-ms-detours-4.0.1 (c (n "ms-detours") (v "4.0.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "libloaderapi"))) (d #t) (k 2)))) (h "14mbkcw7zdnl6c08va7p3prp6l8zcfy1s40s18wliabk4x3yq2g8")))

(define-public crate-ms-detours-4.0.2 (c (n "ms-detours") (v "4.0.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "libloaderapi"))) (d #t) (k 2)))) (h "00al2j6wlpsfc42n6s1nq1lsg9dnh7cggcw4b0n1mg4l11diy1s7") (y #t)))

(define-public crate-ms-detours-4.0.3 (c (n "ms-detours") (v "4.0.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "libloaderapi" "processthreadsapi" "windef"))) (d #t) (k 0)))) (h "18ycd8hypd84svg4wdj8vysqcb94z1xyzy14phcjf485rd8w3w8h")))

