(define-module (crates-io ms #{58}# ms5837) #:use-module (crates-io))

(define-public crate-ms5837-0.1.0 (c (n "ms5837") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "1fhj2hnm4dl36y8rwapzi5pqrfsfsxp0vprqzqq04nxhjx8m98ha")))

(define-public crate-ms5837-0.1.1 (c (n "ms5837") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "0y4hf4105bfvqnpa8qr41yvsihxl18gj27b07mpskpkijfp9fsdi")))

(define-public crate-ms5837-0.1.2 (c (n "ms5837") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "1rz8rmd1byjjlq7pcjk1kq0cbqwv8zgicqv25wxlcybz9zb5mnfq")))

(define-public crate-ms5837-0.1.3 (c (n "ms5837") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "0cry1lsqj1j6f9n7ixp3jhf38xvzkbxk4sq89xcnmx6vaddiaq9i")))

(define-public crate-ms5837-0.2.1 (c (n "ms5837") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "029cillwilzzcc8n3zznjhc8q1wkfrcsf5c19h8558b1mccqdhn9")))

