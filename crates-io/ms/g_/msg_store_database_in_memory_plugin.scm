(define-module (crates-io ms g_ msg_store_database_in_memory_plugin) #:use-module (crates-io))

(define-public crate-msg_store_database_in_memory_plugin-0.1.0 (c (n "msg_store_database_in_memory_plugin") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "msg-store") (r "^0.9.0") (d #t) (k 0)) (d (n "msg_store_database_plugin") (r "^0.1.0") (d #t) (k 0)) (d (n "msg_store_uuid") (r "^0.1.0") (d #t) (k 0)))) (h "15n78prnfxnn8q6cg5lfqz839qcnd1h48bii8wk691fl68y6f54m")))

