(define-module (crates-io ms g_ msg_parser) #:use-module (crates-io))

(define-public crate-msg_parser-0.1.0 (c (n "msg_parser") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0yymdjc885z90bmnzgh72bw1a8zn282zqazycmv5c6n0yqsqw1rz")))

(define-public crate-msg_parser-0.1.1 (c (n "msg_parser") (v "0.1.1") (d (list (d (n "hex") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1pcyj1v5h0i9bdswn08phwmxpxsx04hr1xvy5namfcxz61ladx2m")))

