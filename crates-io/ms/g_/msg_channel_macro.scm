(define-module (crates-io ms g_ msg_channel_macro) #:use-module (crates-io))

(define-public crate-msg_channel_macro-0.1.0-beat.1 (c (n "msg_channel_macro") (v "0.1.0-beat.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1i0rqjqvxi58x4ak4camsmgprb32000fjgvjwskm148s55sr3mhl") (y #t)))

(define-public crate-msg_channel_macro-0.1.0-beat.2 (c (n "msg_channel_macro") (v "0.1.0-beat.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0z3jkysjwd4sj4vjlx7mz7jb9brgyqlv5nqrbw52g07v11i6pgqs")))

(define-public crate-msg_channel_macro-0.1.0-beat.3 (c (n "msg_channel_macro") (v "0.1.0-beat.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1vaxsdyjdka9q7wmvmfnh4fwy4zix2syngc80yg2qbbwmzwrnrdq")))

