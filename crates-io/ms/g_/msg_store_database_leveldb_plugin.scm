(define-module (crates-io ms g_ msg_store_database_leveldb_plugin) #:use-module (crates-io))

(define-public crate-msg_store_database_leveldb_plugin-0.1.0 (c (n "msg_store_database_leveldb_plugin") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.6") (d #t) (k 0)) (d (n "msg-store") (r "^0.9.0") (d #t) (k 0)) (d (n "msg_store_database_plugin") (r "^0.1.0") (d #t) (k 0)) (d (n "msg_store_uuid") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pfpg5d9z0p4p0l4aqrzqjvqdxsa6zg0hndkxrb49aispyld1div")))

