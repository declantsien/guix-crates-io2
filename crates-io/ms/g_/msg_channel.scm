(define-module (crates-io ms g_ msg_channel) #:use-module (crates-io))

(define-public crate-msg_channel-0.1.0-beat.1 (c (n "msg_channel") (v "0.1.0-beat.1") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 2)) (d (n "msg_channel_core") (r "^0.1.0-beat.1") (d #t) (k 0)) (d (n "msg_channel_macro") (r "^0.1.0-beat.1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1sr62r6i7r0kyh9j3sw0ghnghc71d75lw3h7vfg5hpj2w9wd1nmz") (y #t)))

(define-public crate-msg_channel-0.1.0-beat.2 (c (n "msg_channel") (v "0.1.0-beat.2") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 2)) (d (n "msg_channel_core") (r "^0.1.0-beat.2") (d #t) (k 0)) (d (n "msg_channel_macro") (r "^0.1.0-beat.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "19sagp7yyprmx5q2d84nqja4502qrs829l2xsbs8dk0j0rg8hdic")))

(define-public crate-msg_channel-0.1.0-beat.3 (c (n "msg_channel") (v "0.1.0-beat.3") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 2)) (d (n "msg_channel_core") (r "^0.1.0-beat.3") (d #t) (k 0)) (d (n "msg_channel_macro") (r "^0.1.0-beat.3") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "034g3iq8h9j0cks95vw06yazh4zx27ymsabps1n8nmb976ghd5lm")))

