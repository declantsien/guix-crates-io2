(define-module (crates-io ms g_ msg_channel_core) #:use-module (crates-io))

(define-public crate-msg_channel_core-0.1.0-beat.1 (c (n "msg_channel_core") (v "0.1.0-beat.1") (d (list (d (n "force-send-sync") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "msg_channel_macro") (r "^0.1.0-beat.1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt"))) (k 0)))) (h "03wgji0ppfx4dnmd67qi3mlw05c55pf9x8516n1xiw0rnw6iw7yh") (y #t)))

(define-public crate-msg_channel_core-0.1.0-beat.2 (c (n "msg_channel_core") (v "0.1.0-beat.2") (d (list (d (n "force-send-sync") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "msg_channel_macro") (r "^0.1.0-beat.2") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt"))) (k 0)))) (h "04ac0pchwfn5szl975gg8gyqlasc7lqghndy26wf62s71kgvsc4a")))

(define-public crate-msg_channel_core-0.1.0-beat.3 (c (n "msg_channel_core") (v "0.1.0-beat.3") (d (list (d (n "force-send-sync") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "msg_channel_macro") (r "^0.1.0-beat.3") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt"))) (k 0)))) (h "1c48yjlvpr5ld6w14qw2mcph22jdwkzds3prjxisg8mapf9jp2c6")))

