(define-module (crates-io ms tr mstr) #:use-module (crates-io))

(define-public crate-mstr-0.1.0 (c (n "mstr") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)))) (h "1fzkpap0b1vw3a31619pkh95w7i7p69wg9sa1lmj19macfyk0gy6") (f (quote (("default")))) (y #t) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

(define-public crate-mstr-0.1.1 (c (n "mstr") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "16vazm7pdz68qsx0gavqwgljsh2hz6kg2sqh7gg14pnz8ayqcg1p") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.68")))

(define-public crate-mstr-0.1.2 (c (n "mstr") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "02730mgh1v4302ajfb27d38ffj47aiymgz646zfgjw3hmh9nwkqd") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.68")))

(define-public crate-mstr-0.1.3 (c (n "mstr") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0511739f2psyml2mkk4xv9i4a6sl6kq0lm18vrhxzp5jgvf20wmy") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.68")))

(define-public crate-mstr-0.1.4 (c (n "mstr") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0hwwa4yr0gqshrq5pbvkfy3wxg84d758dnbkr7zr2xhhjairrh9y") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.68")))

