(define-module (crates-io ms i- msi-klc) #:use-module (crates-io))

(define-public crate-msi-klc-1.0.0 (c (n "msi-klc") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.2") (d #t) (k 0)))) (h "13pyw4nskks3v394a960ng2rbah8yd26j783fs8vpml73kmkz6aw")))

(define-public crate-msi-klc-1.0.1 (c (n "msi-klc") (v "1.0.1") (d (list (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.2") (d #t) (k 0)))) (h "1mkpcl98669vvppdwd2fa6fcnz5fy16cvbdgxffmaadqn293n1im")))

(define-public crate-msi-klc-1.0.2 (c (n "msi-klc") (v "1.0.2") (d (list (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.2") (d #t) (k 0)))) (h "0cx3v61hmjks0gbsgk6dkym0gf072vjn8a0zh9qna62xzkpqimlz")))

(define-public crate-msi-klc-1.0.3 (c (n "msi-klc") (v "1.0.3") (d (list (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "hidapi") (r "^2.4.1") (d #t) (k 0)))) (h "06lbfvzyy8xfn5aa2r2w3a8w096sn9jhib348hvjhxgx38d0fi4r")))

(define-public crate-msi-klc-1.0.4 (c (n "msi-klc") (v "1.0.4") (d (list (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.2") (d #t) (k 0)))) (h "1piqisb6w0pvv105kqwqmfqrk7j74j7isyf1viflbly9q433jy27")))

(define-public crate-msi-klc-1.1.0 (c (n "msi-klc") (v "1.1.0") (d (list (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "hidapi") (r "^2.4.1") (f (quote ("linux-static-libusb"))) (k 0)))) (h "0088vq2jkq74s6iviqx7dw5ldzjdw976myp9zfycai3vdgs6kzq2")))

