(define-module (crates-io ms dk msdk_adapter) #:use-module (crates-io))

(define-public crate-msdk_adapter-0.1.0 (c (n "msdk_adapter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "11qp8bfv8wmflhky8njx5xky8bwvnag3jfwq57bbw41fdrgsylhj") (y #t)))

(define-public crate-msdk_adapter-0.1.1 (c (n "msdk_adapter") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)))) (h "18gmdi9jyxik776k2qaxv2cfw9azl7sy84vp0xc7325ll7z4jjz3") (y #t)))

(define-public crate-msdk_adapter-0.1.2 (c (n "msdk_adapter") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)))) (h "17f481syivm3az9pn4hpfl6ar5wx7c6fnlnsc1hiybn8xvv0dgn8")))

