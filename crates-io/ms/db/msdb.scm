(define-module (crates-io ms db msdb) #:use-module (crates-io))

(define-public crate-msdb-0.4.0 (c (n "msdb") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rpassword") (r "^4.0.3") (d #t) (k 0)) (d (n "rustyline") (r "^6.0.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)))) (h "0175b26mkglqdymvpf5rj9pm9il62yfymfayhki1xj0k3h4vphk5")))

