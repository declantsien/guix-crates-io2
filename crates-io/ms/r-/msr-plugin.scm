(define-module (crates-io ms r- msr-plugin) #:use-module (crates-io))

(define-public crate-msr-plugin-0.3.0 (c (n "msr-plugin") (v "0.3.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "msr-core") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync"))) (k 0)))) (h "1shngih0350m2cjw852wcicpb1lmdr2pa44p83iwkl9674lnnf9v")))

(define-public crate-msr-plugin-0.3.1 (c (n "msr-plugin") (v "0.3.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "msr-core") (r "=0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync"))) (k 0)))) (h "07gaaf89jvw5bvi99bmnbvxw5jc8qj6fm5qyl09d4axb4fprm9sp")))

(define-public crate-msr-plugin-0.3.2 (c (n "msr-plugin") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "msr-core") (r "=0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("sync"))) (k 0)))) (h "0a4jnkvg3ah6g6bpwbw4kvxsdqjm65z9nnczz4f85g9ns2bkg521")))

(define-public crate-msr-plugin-0.3.3 (c (n "msr-plugin") (v "0.3.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "msr-core") (r "=0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("sync"))) (k 0)))) (h "01xcjawxwx3kp774cxwj89xzlr13yxicj64k4aa58nslv8k2aamz")))

(define-public crate-msr-plugin-0.3.4 (c (n "msr-plugin") (v "0.3.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "msr-core") (r "=0.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("sync"))) (k 0)))) (h "1vmdvp52yhnb4b7996cdy0m9ccncn1ainbbx5sdy4rzpip2czqpq")))

(define-public crate-msr-plugin-0.3.5 (c (n "msr-plugin") (v "0.3.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "msr-core") (r "=0.3.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("sync"))) (k 0)))) (h "1b0bpc35h00af5c7w4rqyv2lpiwig5k0fvhmfcl4k7j5nwlnz0hj")))

(define-public crate-msr-plugin-0.3.6 (c (n "msr-plugin") (v "0.3.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "msr-core") (r "=0.3.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("sync"))) (k 0)))) (h "1fflfzvqf71wi0rlphb40mpmp59y6pl2p9lhbxp57kqjsyi160ca")))

(define-public crate-msr-plugin-0.3.7 (c (n "msr-plugin") (v "0.3.7") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "msr-core") (r "=0.3.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("sync"))) (k 0)))) (h "1bfk2k3cail0qxsyg4sk1ag5id6i0wkkkk00sd00mqmmwrplidp3") (r "1.71")))

