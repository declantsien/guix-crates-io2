(define-module (crates-io ms -c ms-converter) #:use-module (crates-io))

(define-public crate-ms-converter-0.1.0 (c (n "ms-converter") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0a9llhx8gmhgyxlr7fpqrir83lx2l3mlg3i7x37z8jx6nas6cg9s")))

(define-public crate-ms-converter-0.4.0 (c (n "ms-converter") (v "0.4.0") (h "0jmvgpvbhhy52xrbz70vzsjll4ahyj6fh0bpy27x7y6lf00nc4al")))

(define-public crate-ms-converter-0.4.1 (c (n "ms-converter") (v "0.4.1") (h "1x8kcrdrhfb354k3qvw43jbl3w8sh9r256m9nbyswac6d2l2b0s6")))

(define-public crate-ms-converter-0.4.2 (c (n "ms-converter") (v "0.4.2") (h "1vi4m50z7j8b8cf8vk3kcb173n8hi3jrffb5dawz1vbpmfyy6klw")))

(define-public crate-ms-converter-0.4.3 (c (n "ms-converter") (v "0.4.3") (h "164db0rb1xgh46zq62dfyf9wlfxg3xdy0jlpcmzszcmqm711ba18")))

(define-public crate-ms-converter-0.5.0 (c (n "ms-converter") (v "0.5.0") (h "1k1fjd6bdmcxk0wldcvdy9vsrsvgcjv33gdc653nb0pai4g3f5k0")))

(define-public crate-ms-converter-0.5.1 (c (n "ms-converter") (v "0.5.1") (h "1zgshl9kyys4ilg0y1j1dbbv3mxpigx40xzpaavcrfqva5wfjzzl")))

(define-public crate-ms-converter-0.6.0 (c (n "ms-converter") (v "0.6.0") (h "1lj5jnyqr50lci14mc7l6mznkwf59a323m70ji9l5nxal2388cfh")))

(define-public crate-ms-converter-0.7.0 (c (n "ms-converter") (v "0.7.0") (h "0h26vkvfws2pmy0bg1avdbc54wifvrqvq48yxf3llk20dia4qdpa")))

(define-public crate-ms-converter-0.7.1 (c (n "ms-converter") (v "0.7.1") (h "1j3jzhls3mii4pc1bca31jfvxy12hnmrbgyazf5w0jqx0jc0xsa8")))

(define-public crate-ms-converter-0.7.2 (c (n "ms-converter") (v "0.7.2") (h "0d6rh90n6kxfcvnl84vsip56iaqn6vpr9s80qhkyimk4f6y74f6g")))

(define-public crate-ms-converter-0.7.3 (c (n "ms-converter") (v "0.7.3") (h "0s34q3k30rkqnrpijg5bif7s81z4d00c6kx8nhhnvn3y1vynbww6")))

(define-public crate-ms-converter-0.7.4 (c (n "ms-converter") (v "0.7.4") (h "1rys7x8j8rh72q67yhp3a43771qpminvzkpnyibdx79qkalrkfy0")))

(define-public crate-ms-converter-1.0.0 (c (n "ms-converter") (v "1.0.0") (h "1iyqg4l6j20nk00824yylpwspirvysvmd74s1z7xv78qsallizwg")))

(define-public crate-ms-converter-1.1.0 (c (n "ms-converter") (v "1.1.0") (h "0x3if0lwzj9i7gpasnd5l6ms4qfidbz8lqgmld7gzg6grxv939gp")))

(define-public crate-ms-converter-1.1.1 (c (n "ms-converter") (v "1.1.1") (h "1c8c8s3g3dkipcshn3i4qbhw0skxpsf8zrd4mxh734irifg1mlql")))

(define-public crate-ms-converter-1.2.0 (c (n "ms-converter") (v "1.2.0") (h "1cgvp6i51s3gfngh0hin3fa10h2ncgsi4bxcn6v1ahmx5z4ap16h")))

(define-public crate-ms-converter-1.2.1 (c (n "ms-converter") (v "1.2.1") (h "0w8ir6yjzmcgxj51kkifnlb4rg5snwp22vxf1jxalfxj2wydgx9k")))

(define-public crate-ms-converter-1.3.0 (c (n "ms-converter") (v "1.3.0") (h "1n96ixbf16ypj4k43d3yh0mwkgmkvr8r1qc5fz2cvrpqrjjnpnsb")))

(define-public crate-ms-converter-1.3.1 (c (n "ms-converter") (v "1.3.1") (h "03wwi4ycm1530w6dr5k9b657ynhywpbja9nfkmja0a3gh3nig75r")))

(define-public crate-ms-converter-1.4.0 (c (n "ms-converter") (v "1.4.0") (h "1mf4gyb1zvvlafqsy9074syzgjxf6vi7bz6872sdgj55gaf7w4c3")))

