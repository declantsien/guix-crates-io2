(define-module (crates-io ms -l ms-learn-md-parser) #:use-module (crates-io))

(define-public crate-ms-learn-md-parser-0.0.1 (c (n "ms-learn-md-parser") (v "0.0.1") (d (list (d (n "deno_core") (r "^0.175.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)))) (h "0na5dfr237r9wkbn0j01113iyqjzj12wqmhadf5sdrpnpy5izmrr")))

