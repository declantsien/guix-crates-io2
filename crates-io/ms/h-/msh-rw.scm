(define-module (crates-io ms h- msh-rw) #:use-module (crates-io))

(define-public crate-msh-rw-0.1.0 (c (n "msh-rw") (v "0.1.0") (d (list (d (n "bstr") (r "^0.2.13") (d #t) (k 0)) (d (n "insta") (r "^0.16.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0ys16jwvl4pjanfip54ki47w872qvj7w6a0ji75awfcx8q7q6d0w")))

