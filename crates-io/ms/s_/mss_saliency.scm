(define-module (crates-io ms s_ mss_saliency) #:use-module (crates-io))

(define-public crate-mss_saliency-1.0.0 (c (n "mss_saliency") (v "1.0.0") (d (list (d (n "imgref") (r "^1.0.0") (d #t) (k 0)))) (h "0q2a0c2i6xh1qh94dcb61kb383lv5wy3vndr158y99vpckmfbfr5") (y #t)))

(define-public crate-mss_saliency-1.0.1 (c (n "mss_saliency") (v "1.0.1") (d (list (d (n "imgref") (r "^1.0.0") (d #t) (k 0)) (d (n "lodepng") (r "^1.1.1") (d #t) (k 2)))) (h "12jh8n79iazirmgkb75zf7x4g7d1yxbwgmxax0387g50qvbb2kkc") (y #t)))

(define-public crate-mss_saliency-1.0.2 (c (n "mss_saliency") (v "1.0.2") (d (list (d (n "imgref") (r "^1.2.0") (d #t) (k 0)) (d (n "lodepng") (r "^1.1.3") (d #t) (k 2)))) (h "1gzzf4d78s20n56n2dbk20ybsfz61iqqcd9rn0wq0bwjiidm0d1v") (y #t)))

(define-public crate-mss_saliency-1.0.3 (c (n "mss_saliency") (v "1.0.3") (d (list (d (n "imgref") (r "^1.2.0") (d #t) (k 0)) (d (n "lodepng") (r "^2.0.0") (d #t) (k 2)))) (h "08k3i1zdfw9a2b82wl5c1az8zan2y7967w9f4hb43xh8zz8v5jva") (y #t)))

(define-public crate-mss_saliency-1.0.4 (c (n "mss_saliency") (v "1.0.4") (d (list (d (n "imgref") (r "^1.4.0") (d #t) (k 0)) (d (n "lodepng") (r "^2.5.0") (d #t) (k 2)))) (h "0vdkc1363b8g27cg098bn9ymxybxwsmqr85s0jpq1d4fzx6292i0") (y #t)))

(define-public crate-mss_saliency-1.0.5 (c (n "mss_saliency") (v "1.0.5") (d (list (d (n "imgref") (r "^1.6.1") (d #t) (k 0)) (d (n "lodepng") (r "^3.0.0") (d #t) (k 2)))) (h "1050naygc2cv2zd0b5fym6lanvmi65nq8b04xxfpbfk0xl0ywz6z")))

(define-public crate-mss_saliency-1.0.6 (c (n "mss_saliency") (v "1.0.6") (d (list (d (n "imgref") (r "^1.7.0") (d #t) (k 0)) (d (n "lodepng") (r "^3.2.1") (d #t) (k 2)))) (h "1snfrclhrmhww2kn6fvk98a06wm7zxv7m6l76hhyg3pq6pzhx3wn")))

(define-public crate-mss_saliency-1.1.0 (c (n "mss_saliency") (v "1.1.0") (d (list (d (n "imgref") (r "^1.9") (d #t) (k 0)) (d (n "lodepng") (r "^3.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.34") (k 0)) (d (n "summed-area") (r "^1.0") (d #t) (k 0)))) (h "1vhwx7028rjra0fqaznpbnr3ar57a7g7kh054088d9a5rv5a8cdv")))

