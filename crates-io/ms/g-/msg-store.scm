(define-module (crates-io ms g- msg-store) #:use-module (crates-io))

(define-public crate-msg-store-0.1.1 (c (n "msg-store") (v "0.1.1") (h "15lz745rx25ixhiw44s7k804sabk1yqmpp18gr803bk57bqgdi4f")))

(define-public crate-msg-store-0.2.1 (c (n "msg-store") (v "0.2.1") (h "11djwf4v93kkaaj6h5ccx8p689v63r2s4lyha99vjdgdva6kd6yq")))

(define-public crate-msg-store-0.3.0 (c (n "msg-store") (v "0.3.0") (h "0aa7nylw5w5lacavbijs08bxqf3j9wmz7zr23lks7y14lc7hhbf4")))

(define-public crate-msg-store-0.3.1 (c (n "msg-store") (v "0.3.1") (h "0kb3wvpdy5dqjiyfxplan94m9jf0xhbdvamz5j8kk3nva850nqx5")))

(define-public crate-msg-store-0.3.2 (c (n "msg-store") (v "0.3.2") (h "11si1iw4q680rhg7yflbv1kmzcll6zhqh9rsrxibbxj79rqm5ijd")))

(define-public crate-msg-store-0.3.3 (c (n "msg-store") (v "0.3.3") (h "10gs1l75ib05kl8pvr52k2a2nkyv4rkqnsbk8nzlgbj9wqmxq5f1")))

(define-public crate-msg-store-0.4.0 (c (n "msg-store") (v "0.4.0") (h "09jv7kc2r5sczxjc39cjplz82zpk1jr90hm7yrj0nk0ppsab6517")))

(define-public crate-msg-store-0.4.1 (c (n "msg-store") (v "0.4.1") (h "1bq1fa3bqf3ynrfxqz4al5nfailzsjmzqg78wrspb5v13xpppq2r")))

(define-public crate-msg-store-0.4.2 (c (n "msg-store") (v "0.4.2") (h "17vgjn7pfnsbcgwaa4mx6qy2zflsjh7vp5zcihly8psr8bk29xkk")))

(define-public crate-msg-store-0.5.0 (c (n "msg-store") (v "0.5.0") (h "0yraq43jkz7sk67k8pvnifswh3br28wc7662421cmfl0nl2imlip")))

(define-public crate-msg-store-0.6.0 (c (n "msg-store") (v "0.6.0") (h "1a5h2gqx5naambksgj0wlpj9al1xirfddqbkr4ldd88vdjwgkkz1")))

(define-public crate-msg-store-0.7.0 (c (n "msg-store") (v "0.7.0") (h "0kfn0j3gn9501403m55llspzh2320mhx8z4zl1a8s1wdymqww9av")))

(define-public crate-msg-store-0.7.1 (c (n "msg-store") (v "0.7.1") (h "1lp8d3v3xdm50g8kw7qkl216869dbh54bvw7vfsbdj8apya1m9fv")))

(define-public crate-msg-store-0.8.0 (c (n "msg-store") (v "0.8.0") (h "06wldb5h2zn4jsl7wgk7k1vxaw7rqw7rclkiz6zjwsg5a1kylcir")))

(define-public crate-msg-store-0.9.0 (c (n "msg-store") (v "0.9.0") (d (list (d (n "msg_store_uuid") (r "^0.1.0") (d #t) (k 0)))) (h "1hhnbf1r1inw8afcvc8vgkbwkcskqpscq97a7h0alkgl82flg1q5")))

(define-public crate-msg-store-0.9.1 (c (n "msg-store") (v "0.9.1") (d (list (d (n "msg_store_uuid") (r "^0.1.0") (d #t) (k 0)))) (h "0pjlisj7pvpshjbdcgdf9dphxf0fkzxcmki06zyn4aj2snj6n4sx")))

