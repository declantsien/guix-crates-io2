(define-module (crates-io ms g- msg-store-plugin-leveldb) #:use-module (crates-io))

(define-public crate-msg-store-plugin-leveldb-0.1.1 (c (n "msg-store-plugin-leveldb") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.6") (d #t) (k 0)) (d (n "msg-store") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wg1a7k8hnqzyrv2f3rlh68pw9pm0azs4nvih89faq6pzyjq3bcx")))

(define-public crate-msg-store-plugin-leveldb-0.2.0 (c (n "msg-store-plugin-leveldb") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.6") (d #t) (k 0)) (d (n "msg-store") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lrknpllayi11nlmas42dwbsv6b27lzqmp7mnf9rda25jch40ywc")))

(define-public crate-msg-store-plugin-leveldb-0.3.0 (c (n "msg-store-plugin-leveldb") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.6") (d #t) (k 0)) (d (n "msg-store") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ndyj652jzpd9c21fjrdgvsjhplnbsz38yivf4n2qn5bs26wpciw")))

(define-public crate-msg-store-plugin-leveldb-0.4.0 (c (n "msg-store-plugin-leveldb") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.6") (d #t) (k 0)) (d (n "msg-store") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11idggyf42lmpxy5krmka8hxiafs1v9lyb49pgx79ldg9vdsq9rb")))

(define-public crate-msg-store-plugin-leveldb-0.4.1 (c (n "msg-store-plugin-leveldb") (v "0.4.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.6") (d #t) (k 0)) (d (n "msg-store") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wrfn7rpd702j9sgpfkcd26wk6ih0y5i3ckh0hvzi2sx9si1g9c3")))

(define-public crate-msg-store-plugin-leveldb-0.4.2 (c (n "msg-store-plugin-leveldb") (v "0.4.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.6") (d #t) (k 0)) (d (n "msg-store") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1srf0jd2z7z07r16hga0bdlyzvfa87jl0gqkjsh5krav9qksvgvf")))

(define-public crate-msg-store-plugin-leveldb-0.4.3 (c (n "msg-store-plugin-leveldb") (v "0.4.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.6") (d #t) (k 0)) (d (n "msg-store") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i0spjqw7gz0ii02xca2wmn0wsp8011gmwdaf2i4bfx38akw7shg")))

(define-public crate-msg-store-plugin-leveldb-0.5.0 (c (n "msg-store-plugin-leveldb") (v "0.5.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.6") (d #t) (k 0)) (d (n "msg-store") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ay6j3qxxy0bmyrzzxzjl01kg4j1xk8dsj4hpp4k12ajy2ybwmss")))

(define-public crate-msg-store-plugin-leveldb-0.5.1 (c (n "msg-store-plugin-leveldb") (v "0.5.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "db-key") (r "^0.0.5") (d #t) (k 0)) (d (n "leveldb") (r "^0.8.6") (d #t) (k 0)) (d (n "msg-store") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17cvnnxka0dkgsrdlbhngs8gb7ha4w2ig4jmmyb54rn8l518rm70")))

