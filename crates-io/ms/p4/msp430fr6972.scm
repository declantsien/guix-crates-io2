(define-module (crates-io ms p4 msp430fr6972) #:use-module (crates-io))

(define-public crate-msp430fr6972-0.1.0 (c (n "msp430fr6972") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0vhrmvkqn8dcgs5yhrhy0v72c24gbcfjw826axi8j2v15a698jl4") (f (quote (("rt" "msp430-rt/device")))) (y #t)))

(define-public crate-msp430fr6972-0.1.1 (c (n "msp430fr6972") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430") (r "^0.2.2") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1xbnsm9g9ky8lzsray47im5kd7y8rr5qhcmzr54apgqlfdh4s7dc") (f (quote (("rt" "msp430-rt/device")))) (y #t)))

(define-public crate-msp430fr6972-0.2.0 (c (n "msp430fr6972") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430") (r "^0.2.2") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1lrzm87403mwd66gd23fpwf9phxmhg4ri9c95sdzbkgfh4wgq8xx") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430fr6972-0.2.1 (c (n "msp430fr6972") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430") (r "^0.2.2") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1zx19pif442kx16pg23j5758n1yr6zxflvl1lwncdqcmw9z2kr9x") (f (quote (("rt" "msp430-rt/device"))))))

