(define-module (crates-io ms p4 msp430g2211) #:use-module (crates-io))

(define-public crate-msp430g2211-0.1.0 (c (n "msp430g2211") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1yaldyhn8lj8s3y43wjy4w5gp3hs5zra3205vxpvh2zp2x1h2s24") (f (quote (("rt"))))))

(define-public crate-msp430g2211-0.1.1 (c (n "msp430g2211") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0plmlm9fn0vvqilnz6i814ggvr7aqpi52dd997hjccnh6gjj43z6") (f (quote (("rt"))))))

(define-public crate-msp430g2211-0.1.3 (c (n "msp430g2211") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.1.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "16fgfn6936b3rki3k3x8i6wff2gcnyj3ji2skzclwr74ia3vic4s") (f (quote (("rt"))))))

(define-public crate-msp430g2211-0.1.4 (c (n "msp430g2211") (v "0.1.4") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.1.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "17ndskwj4pwcghda65g76993fwpxkj8mxv4alz5z3b131llf15zv") (f (quote (("rt"))))))

(define-public crate-msp430g2211-0.2.0 (c (n "msp430g2211") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "15nsvl4ypxmpzbdgnwxa6bn6cg1h6vms7395p6ld8dqyi7n8f30x") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430g2211-0.2.1 (c (n "msp430g2211") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1zjdcghffpapjhnlsj7njkxhry8ild3bvy0w1cs90jphq2qip1cw") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430g2211-0.3.0 (c (n "msp430g2211") (v "0.3.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "msp430") (r "^0.3.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "03x2al5wlwd7s6gb6vcykb48idwq7g297d51qx5qpi8kz5lcyvrs") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430g2211-0.4.0 (c (n "msp430g2211") (v "0.4.0") (d (list (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "msp430") (r "^0.4.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0cbkwzjvgpm39v5j3viylpxv9v4d0z388k9m51aps53ry22aw2pq") (f (quote (("rt" "msp430-rt/device"))))))

