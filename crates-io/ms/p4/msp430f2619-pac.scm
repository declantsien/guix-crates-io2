(define-module (crates-io ms p4 msp430f2619-pac) #:use-module (crates-io))

(define-public crate-msp430f2619-pac-0.1.0 (c (n "msp430f2619-pac") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "msp430") (r "^0.4.0") (d #t) (k 0)) (d (n "msp430-atomic") (r "^0.1.4") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.4.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0sg9mvphg743dn0crcg5j1bl5kg6qw1ksn128w644ds1scgpr154") (f (quote (("rt" "msp430-rt/device"))))))

