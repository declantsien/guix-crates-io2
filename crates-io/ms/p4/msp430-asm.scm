(define-module (crates-io ms p4 msp430-asm) #:use-module (crates-io))

(define-public crate-msp430-asm-0.1.0 (c (n "msp430-asm") (v "0.1.0") (h "082kiprqcl63paqq9xfsbfzqd4njl8abpdch1rba223l04kbpa48")))

(define-public crate-msp430-asm-0.2.0 (c (n "msp430-asm") (v "0.2.0") (h "1jzk5y8klh9xpqv94fjdapamv783xn5abqhhrj4bf1f227qj4779")))

