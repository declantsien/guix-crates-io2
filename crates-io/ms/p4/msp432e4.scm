(define-module (crates-io ms p4 msp432e4) #:use-module (crates-io))

(define-public crate-msp432e4-0.1.0 (c (n "msp432e4") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "13v40c1c0g1s6nbdhkadhqak3m0k83sqnd7m5dpnq2ia49qwk3ig") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-msp432e4-0.1.1 (c (n "msp432e4") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "09ampc4b1wyabffwsfpawznzmd9xcqzzmmc41qcipp7ii0cxkcbj") (f (quote (("rt" "cortex-m-rt/device"))))))

