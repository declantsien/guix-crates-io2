(define-module (crates-io ms p4 msp430fr247x) #:use-module (crates-io))

(define-public crate-msp430fr247x-0.1.0 (c (n "msp430fr247x") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "msp430") (r "^0.4.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0sj9gljrprwq1lq9szw9n3nhfjdbn6zd2w63h7qqxn5jja4jknx0") (f (quote (("rt" "msp430-rt/device"))))))

