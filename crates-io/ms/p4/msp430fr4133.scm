(define-module (crates-io ms p4 msp430fr4133) #:use-module (crates-io))

(define-public crate-msp430fr4133-0.1.0 (c (n "msp430fr4133") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0a9c1n8xs1vqx1jzs390p9g707a6n0qyybyfy73jx1aqwq3i5k32") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430fr4133-0.1.1 (c (n "msp430fr4133") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1qwz0ahisk1ngjpqp6rq9rawgyg053dh72vr1l8b1238w9pwzi82") (f (quote (("rt" "msp430-rt/device"))))))

