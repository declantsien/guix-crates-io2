(define-module (crates-io ms p4 msp430f6736) #:use-module (crates-io))

(define-public crate-msp430f6736-0.1.1 (c (n "msp430f6736") (v "0.1.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "msp430") (r "^0.3.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "108nmgsamfxl7lyy44b9gjr2386j1bqv1bv3m0c1vb7lxnwvv4x5") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430f6736-0.1.2 (c (n "msp430f6736") (v "0.1.2") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "msp430") (r "^0.3.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1agwsb6ajwzh09mqzj3vvsyjq7xbibgaw9hl73wxp61cc7yi6dia") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430f6736-0.1.3 (c (n "msp430f6736") (v "0.1.3") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "msp430") (r "^0.3.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "045p9kbqn58zfg1mai6abf9s5s23iii3j3ql7lna6wqqxwj8406j") (f (quote (("rt" "msp430-rt/device"))))))

