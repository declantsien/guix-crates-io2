(define-module (crates-io ms p4 msp430) #:use-module (crates-io))

(define-public crate-msp430-0.1.0 (c (n "msp430") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)))) (h "1sij6xpvi8xl1x5z6h8hq11g5vsg38rl0jmpyxas9zy8737rdy4z")))

(define-public crate-msp430-0.2.0 (c (n "msp430") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)))) (h "0cx4gjbkpf0w6n7f7c7y3qlks83xvcw328fqm4kwygncdp292pas")))

(define-public crate-msp430-0.2.1 (c (n "msp430") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)))) (h "0kd5m0qjq26cwxl9afwgmr4k1bnhcdq5zfkl6mg49rw8afmwyds5")))

(define-public crate-msp430-0.2.2 (c (n "msp430") (v "0.2.2") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)))) (h "1dp4n4p809imf327lqbdjk0mxcl4hnkw5kfyi44wmwrgs8qbsvhj")))

(define-public crate-msp430-0.3.0 (c (n "msp430") (v "0.3.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)))) (h "0zmifcdy15iixg85bvhmi8gldw9n8xja2xhkqdny7if608sww8km")))

(define-public crate-msp430-0.4.0 (c (n "msp430") (v "0.4.0") (d (list (d (n "critical-section") (r "^1.0.0") (d #t) (k 0)))) (h "1gbaqxyns06p347c6hcfssk8p8j1hp23d6apcz46s5wrfccrl5vs") (f (quote (("outline-cs-rel") ("outline-cs-acq") ("outline-cs" "outline-cs-acq" "outline-cs-rel") ("critical-section-single-core" "critical-section/restore-state-u16"))))))

(define-public crate-msp430-0.4.1 (c (n "msp430") (v "0.4.1") (d (list (d (n "critical-section") (r "^1.0.0") (d #t) (k 0)))) (h "079y9vf0lnwaq1r3gjncm3anq1060k8v274pg6lxdaf9n99z9881") (f (quote (("outline-cs-rel") ("outline-cs-acq") ("outline-cs" "outline-cs-acq" "outline-cs-rel") ("critical-section-single-core" "critical-section/restore-state-u16"))))))

