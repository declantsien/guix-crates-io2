(define-module (crates-io ms p4 msp432p401r-pac) #:use-module (crates-io))

(define-public crate-msp432p401r-pac-0.1.0 (c (n "msp432p401r-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "0.1.*") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 0)) (d (n "vcell") (r "0.1.*") (d #t) (k 0)))) (h "0b52csbqf5jf32v6gigrxxsk6p69rn9490s3f5cvh4imwq04xmhh") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-msp432p401r-pac-0.1.1 (c (n "msp432p401r-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "0.1.*") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 0)) (d (n "vcell") (r "0.1.*") (d #t) (k 0)))) (h "1hc3zpjw70lklhz7g2ac0mwkjmz9m4yr8l9xqdl7byswc9kpj8sb") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-msp432p401r-pac-0.2.0 (c (n "msp432p401r-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 0)) (d (n "vcell") (r "0.1.*") (d #t) (k 0)))) (h "1ywjl9pdwy0xrqdg85aqndf612ix1pfrb2dqnhakkl49kx8hlhsx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-msp432p401r-pac-0.3.0 (c (n "msp432p401r-pac") (v "0.3.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (d #t) (k 0)) (d (n "vcell") (r "0.1.*") (d #t) (k 0)))) (h "0gafi2gs4i774xvfi12939v6vrjrkijw9panqclpzrdyj6d2frsz") (f (quote (("rt" "cortex-m-rt/device"))))))

