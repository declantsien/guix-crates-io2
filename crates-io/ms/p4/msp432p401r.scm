(define-module (crates-io ms p4 msp432p401r) #:use-module (crates-io))

(define-public crate-msp432p401r-0.1.0 (c (n "msp432p401r") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "12fbw49f4dg0mxl1mh3mjgdf7issbw52w85shpfwvwzv2fv4syzq") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-msp432p401r-0.1.1 (c (n "msp432p401r") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0f2mq4bq42vi6ifp6sqlnbg15qbxk2gldi2smz6lg6iwyky03gyp") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-msp432p401r-0.1.2 (c (n "msp432p401r") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0vlyx8dasvli0mfhz891y6vfx488fy08c2mv971dqy8k929czvdl") (f (quote (("rt" "cortex-m-rt"))))))

