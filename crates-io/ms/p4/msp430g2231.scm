(define-module (crates-io ms p4 msp430g2231) #:use-module (crates-io))

(define-public crate-msp430g2231-0.1.0 (c (n "msp430g2231") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "14nbv0sc0ng78a6r8i0xr7v1b9mfb77hx70hdiwi8wdln9p00976") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430g2231-0.2.0 (c (n "msp430g2231") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "msp430") (r "^0.4.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1bc4a1hwz4x182148m0mhcbhfck9r8jkqzs6m3npm30ggl3dj0v1") (f (quote (("rt" "msp430-rt/device"))))))

