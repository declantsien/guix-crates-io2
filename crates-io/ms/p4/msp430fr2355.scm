(define-module (crates-io ms p4 msp430fr2355) #:use-module (crates-io))

(define-public crate-msp430fr2355-0.1.0 (c (n "msp430fr2355") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1r0512jjwlj41nq8alm5wgzg8hapbxw3v54yflkvf3r5xbs94wj4") (f (quote (("rt" "msp430-rt")))) (y #t)))

(define-public crate-msp430fr2355-0.2.0 (c (n "msp430fr2355") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1iq17lh8gf898mfzprfr2p69i6b8v94ain8qcwc6z6gysf53i0sw") (f (quote (("rt" "msp430-rt")))) (y #t)))

(define-public crate-msp430fr2355-0.2.1 (c (n "msp430fr2355") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0jabxgyypajpgfvfkvilldacl75k679ak6c8m2f7zsjs5wi0p9f5") (f (quote (("rt" "msp430-rt"))))))

(define-public crate-msp430fr2355-0.3.0 (c (n "msp430fr2355") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0j75an4ydr9hdw1xwk9wqd3f6lah40m8baghvrill46z5wj7izdq") (f (quote (("rt" "msp430-rt"))))))

(define-public crate-msp430fr2355-0.4.0 (c (n "msp430fr2355") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-atomic") (r "^0.1.1") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "13b8pg773w8rr1ndxvqk16zabxcdcahjl1jva3ximzhzp6rbcxvj") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430fr2355-0.4.1 (c (n "msp430fr2355") (v "0.4.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-atomic") (r "^0.1.1") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1lpm3gvgh1cgknbckyzirdv9rr820m7akbxbvl8fxwb961lq603y") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430fr2355-0.4.2 (c (n "msp430fr2355") (v "0.4.2") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430") (r "^0.2.2") (d #t) (k 0)) (d (n "msp430-atomic") (r "^0.1.2") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rj4ynls3l6qafn8xbc6lkxgwa5bigxhhldfnyzm16r2mvvx5jim") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430fr2355-0.5.0 (c (n "msp430fr2355") (v "0.5.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "msp430") (r "^0.4.0") (d #t) (k 0)) (d (n "msp430-atomic") (r "^0.1.4") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0vqacyrnwc4c9qcsbyrzg0krjnc9nnkqrbgbgzb9zp92lczidcyq") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430fr2355-0.5.1 (c (n "msp430fr2355") (v "0.5.1") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "msp430") (r "^0.4.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.15") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1jm09ziy2zpbbddj59vysh2lvl1im3cdj3xz75pbzcazgm6aw2f0") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430fr2355-0.5.2 (c (n "msp430fr2355") (v "0.5.2") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "msp430") (r "^0.4.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3.16") (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "03gbf1pxhy1xwq8a1swnjbxa8ly5nplci52hbv9srmlrz3xga3gz") (f (quote (("rt" "msp430-rt/device"))))))

