(define-module (crates-io ms p4 msp430fr2433) #:use-module (crates-io))

(define-public crate-msp430fr2433-0.1.0 (c (n "msp430fr2433") (v "0.1.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "msp430") (r "^0.3.0") (d #t) (k 0)) (d (n "msp430-atomic") (r "^0.1.4") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1851j6l996fvl154xpnjcnfmsibdjvszrgwbxim7yjjczkm035r4") (f (quote (("rt" "msp430-rt/device"))))))

