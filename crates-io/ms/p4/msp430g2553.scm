(define-module (crates-io ms p4 msp430g2553) #:use-module (crates-io))

(define-public crate-msp430g2553-0.1.0 (c (n "msp430g2553") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0pq23l3vdrd4gfl1a82dj5hxmrqp7fz44g4glsnh3ap1gcmd6jv7") (f (quote (("rt"))))))

(define-public crate-msp430g2553-0.1.1 (c (n "msp430g2553") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1rwvynvgp7wfdx02myjfqf4i7vbd0xchg1jh7plvl8a1dlilwplq") (f (quote (("rt"))))))

(define-public crate-msp430g2553-0.1.2 (c (n "msp430g2553") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.1.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0gnv9q1908iz52yz2nrlxpcv38rv6hkw804hdii2dwrs4pfc8pdr") (f (quote (("rt"))))))

(define-public crate-msp430g2553-0.1.3 (c (n "msp430g2553") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.1.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1zkj2a8rf5lf1h0irinndivh01ahvgjv8wgc7fg9lai24k4vw7yy") (f (quote (("rt"))))))

(define-public crate-msp430g2553-0.1.4 (c (n "msp430g2553") (v "0.1.4") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.1.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0xjzyyywk4s1ihrz877l2xrsr8ml8v3r4z9x4mjincqjybbqzm3v") (f (quote (("rt"))))))

(define-public crate-msp430g2553-0.2.0 (c (n "msp430g2553") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1265l369kcz8ipqqd4p0n1h9hpbrgrxjhqpz7fzswjqwmld1hmp6") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430g2553-0.3.0 (c (n "msp430g2553") (v "0.3.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "msp430") (r "^0.3.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "145yin7x4dyrzsqvamf3lz8qkc0nqf99jkps64yk7xifr4wlvvk0") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430g2553-0.4.0 (c (n "msp430g2553") (v "0.4.0") (d (list (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "msp430") (r "^0.4.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1vikl8kq9dnlrch5w7rxrxi46l6qakzfxll7vyaqf6xcrwbqhbyh") (f (quote (("rt" "msp430-rt/device"))))))

