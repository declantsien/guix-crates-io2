(define-module (crates-io ms p4 msp430fr2476) #:use-module (crates-io))

(define-public crate-msp430fr2476-0.1.0 (c (n "msp430fr2476") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "msp430") (r "^0.4.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "09y735j4n6c877arx63xqf5hcz46fzx737cg3w2zyap28d8dwbfk") (f (quote (("rt" "msp430-rt/device")))) (y #t)))

(define-public crate-msp430fr2476-0.1.1 (c (n "msp430fr2476") (v "0.1.1") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "msp430") (r "^0.4.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1334114p79wgkhxfb2drlrd3if0hkcrpq2z1mxni77ap4rr0gx6a") (f (quote (("rt" "msp430-rt/device")))) (y #t)))

