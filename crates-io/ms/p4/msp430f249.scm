(define-module (crates-io ms p4 msp430f249) #:use-module (crates-io))

(define-public crate-msp430f249-0.1.0 (c (n "msp430f249") (v "0.1.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "msp430") (r "^0.4.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "14kh67mv570djsviar1ax9zqcxrqiirbgidj49nqxfp491scaicf") (f (quote (("rt" "msp430-rt/device"))))))

(define-public crate-msp430f249-0.1.1 (c (n "msp430f249") (v "0.1.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "msp430") (r "^0.4.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0gpa4jqzzwc1qr11j6jh2c62jyn210xz9vc094qdjx0qn2a55mms") (f (quote (("rt" "msp430-rt/device"))))))

