(define-module (crates-io ms p4 msp430-rt) #:use-module (crates-io))

(define-public crate-msp430-rt-0.1.0 (c (n "msp430-rt") (v "0.1.0") (d (list (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)))) (h "0rx4iw94r96bqhcxwq62vndgfksw1bic8xifl1sxchsyy7l1zcdx") (f (quote (("abort-on-panic"))))))

(define-public crate-msp430-rt-0.1.1 (c (n "msp430-rt") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 1)) (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)))) (h "0mgfknvs852v3sl9hgzicjfga9n1g737fh274p6fs1nysa7cmi26") (f (quote (("abort-on-panic"))))))

(define-public crate-msp430-rt-0.1.2 (c (n "msp430-rt") (v "0.1.2") (d (list (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)))) (h "1p3rns9fqfrla34888brbq77gh99c1m56pys2pxw83zyid4h8d9x") (f (quote (("abort-on-panic"))))))

(define-public crate-msp430-rt-0.1.3 (c (n "msp430-rt") (v "0.1.3") (d (list (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)))) (h "0a12ccyryf661f71av69wq6p3gh0bqmjvj0y9qyjd4c4hypg0w0d") (f (quote (("abort-on-panic"))))))

(define-public crate-msp430-rt-0.1.4 (c (n "msp430-rt") (v "0.1.4") (d (list (d (n "msp430") (r "^0.1.0") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)))) (h "0yd2vvy5y89a11mq9ycgi67slc5m7xybldl7jgfcp2h5k72r1if0")))

(define-public crate-msp430-rt-0.2.0 (c (n "msp430-rt") (v "0.2.0") (d (list (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-rt-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)))) (h "1hq2g2l03dxk3xd10jlhxbhsr79isnk660ync66kmjnmhns2rd7w") (f (quote (("device" "msp430-rt-macros/device"))))))

(define-public crate-msp430-rt-0.2.1 (c (n "msp430-rt") (v "0.2.1") (d (list (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-rt-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)))) (h "0530y38367r6y658fhj5mb93kxlnz3fv8f8wl912g9m5yxmls2f6") (f (quote (("device" "msp430-rt-macros/device"))))))

(define-public crate-msp430-rt-0.2.2 (c (n "msp430-rt") (v "0.2.2") (d (list (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-rt-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)))) (h "08cr9f9nj9iq607mmggg6wswzcydzkfy37vy1w3915zyx0miqxm9") (f (quote (("device" "msp430-rt-macros/device"))))))

(define-public crate-msp430-rt-0.2.3 (c (n "msp430-rt") (v "0.2.3") (d (list (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-rt-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)))) (h "1zrbka348x4gpibv4hvlpaiskdh99w9ryrwibnc2jwa6wdvqqyx8") (f (quote (("device" "msp430-rt-macros/device")))) (y #t)))

(define-public crate-msp430-rt-0.2.4 (c (n "msp430-rt") (v "0.2.4") (d (list (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-rt-macros") (r "^0.2.4") (d #t) (k 0)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)))) (h "086cnfk568v7fdhk3393f3l4jj2bliw23iajgsiigqphdnalbvrf") (f (quote (("device" "msp430-rt-macros/device"))))))

(define-public crate-msp430-rt-0.2.5 (c (n "msp430-rt") (v "0.2.5") (d (list (d (n "msp430") (r "^0.2.0") (d #t) (k 0)) (d (n "msp430-rt-macros") (r "=0.2.4") (d #t) (k 0)))) (h "1wzk332c0y6yvwkkjm60cq8ay325xpl1bgxjsnydsfikbbrfi61b") (f (quote (("device" "msp430-rt-macros/device"))))))

(define-public crate-msp430-rt-0.3.0 (c (n "msp430-rt") (v "0.3.0") (d (list (d (n "msp430") (r "^0.3.0") (d #t) (k 0)) (d (n "msp430-rt-macros") (r "=0.3.0") (d #t) (k 0)))) (h "0ljc5k406czhphmp1gynxrzr4q5z3rxfyp3dmy8hwp573zqq2f2h") (f (quote (("device" "msp430-rt-macros/device"))))))

(define-public crate-msp430-rt-0.3.1 (c (n "msp430-rt") (v "0.3.1") (d (list (d (n "msp430") (r "^0.3.0") (d #t) (k 0)) (d (n "msp430-rt-macros") (r "=0.3.1") (d #t) (k 0)))) (h "1jb3bcix8vw37zilqh7nfrghls8igp5iv4mgsv9hd73fgqca2nmj") (f (quote (("device" "msp430-rt-macros/device"))))))

(define-public crate-msp430-rt-0.4.0 (c (n "msp430-rt") (v "0.4.0") (d (list (d (n "msp430") (r "^0.4.0") (d #t) (k 0)) (d (n "msp430-rt-macros") (r "=0.4.0") (d #t) (k 0)))) (h "0gn0i3c1a2p5rfvl1vhcn243w0wvr6pb1kl3qqblkxwzl8mhzhqw") (f (quote (("device" "msp430-rt-macros/device"))))))

