(define-module (crates-io ms p4 msp430-atomic) #:use-module (crates-io))

(define-public crate-msp430-atomic-0.1.0 (c (n "msp430-atomic") (v "0.1.0") (h "0m48gsp87cp8idghy1qhcqan9mc7khf8kdm1hfilfa69rkhcl65n")))

(define-public crate-msp430-atomic-0.1.1 (c (n "msp430-atomic") (v "0.1.1") (h "1ihpkrh5jmr5mvi275z49wmv8ww6rpskaipssngqr9vl0y15cknj")))

(define-public crate-msp430-atomic-0.1.2 (c (n "msp430-atomic") (v "0.1.2") (h "1m98wdblfmk42b5ksw2957c30diqik8h4960ykv6f3dyxyj7xf3v")))

(define-public crate-msp430-atomic-0.1.3 (c (n "msp430-atomic") (v "0.1.3") (h "13548s1i2031fnzws00iaw6saa3fxd6gzv2nmciah6dvcqivh1qz")))

(define-public crate-msp430-atomic-0.1.4 (c (n "msp430-atomic") (v "0.1.4") (h "0yw4a2i9dgydb3aa4ayvdvyr4dzp8i7973455p786lam1nzfcxmp")))

(define-public crate-msp430-atomic-0.1.5 (c (n "msp430-atomic") (v "0.1.5") (h "0iqqrswpx9d0mp1lwzqlr6glwbivd2w1hgy822cb3khqdd71gwc7")))

