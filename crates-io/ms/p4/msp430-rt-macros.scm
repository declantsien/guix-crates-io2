(define-module (crates-io ms p4 msp430-rt-macros) #:use-module (crates-io))

(define-public crate-msp430-rt-macros-0.2.0 (c (n "msp430-rt-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "02qixbpn3s58b26aw0n8byrmfd9plxzv4v1qp2yanydkxbfwfggi") (f (quote (("device"))))))

(define-public crate-msp430-rt-macros-0.2.4 (c (n "msp430-rt-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0avbkl3svhsqi4zgqrz0a5z4mf8kvca52832vb5k4b688y8m3gyg") (f (quote (("device"))))))

(define-public crate-msp430-rt-macros-0.3.0 (c (n "msp430-rt-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0wdcbwl7fqvvix8dpp708wkq5abaxlbq0by92bnhk1xgkzszbf5k") (f (quote (("device"))))))

(define-public crate-msp430-rt-macros-0.3.1 (c (n "msp430-rt-macros") (v "0.3.1") (d (list (d (n "msp430") (r "^0.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)))) (h "07cydjz1firb7wj1kp6p3xbik44kwdacjhyxpyryqnjfd6qs8z6k") (f (quote (("device"))))))

(define-public crate-msp430-rt-macros-0.4.0 (c (n "msp430-rt-macros") (v "0.4.0") (d (list (d (n "msp430") (r "^0.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (t "cfg(not(target_os = \"none\"))") (k 2)))) (h "1nsqbz6f1rr9sripkdhl328sw3dv96zigq3qh2y1a1c8kdavklip") (f (quote (("device"))))))

