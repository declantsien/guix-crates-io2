(define-module (crates-io ms p4 msp432p401r-hal) #:use-module (crates-io))

(define-public crate-msp432p401r-hal-0.1.0 (c (n "msp432p401r-hal") (v "0.1.0") (d (list (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.10, <0.7.0") (d #t) (k 0)) (d (n "embedded-hal") (r ">=1.0.0-alpha.3, <2.0.0") (d #t) (k 0)) (d (n "msp432p401r-pac") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0mi1cldhrx5yvhqc3f418n21ywcd1p9sfrkmz82n271v19qbf4p9") (f (quote (("rt" "msp432p401r-pac/rt"))))))

(define-public crate-msp432p401r-hal-0.2.0 (c (n "msp432p401r-hal") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.11") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-alpha.4") (d #t) (k 0)) (d (n "msp432p401r-pac") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)))) (h "1hmwqybsg76vpzpfhhqh0ll5sscm32c6nffn8gb5m9hflq0i8551") (f (quote (("rt" "msp432p401r-pac/rt"))))))

