(define-module (crates-io ms p4 msp430f6736a) #:use-module (crates-io))

(define-public crate-msp430f6736a-0.1.0 (c (n "msp430f6736a") (v "0.1.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "msp430") (r "^0.3.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1rij09az1p97437n5xqd8h2xa66yphifrvxhbn19fcd03318219q") (f (quote (("rt" "msp430-rt/device")))) (y #t)))

(define-public crate-msp430f6736a-0.1.1 (c (n "msp430f6736a") (v "0.1.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "msp430") (r "^0.3.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "03478iaw1xqla84l73nzzzbbavz5kdrf05i3hvyz4w0rny9267qv") (f (quote (("rt" "msp430-rt/device")))) (y #t)))

(define-public crate-msp430f6736a-0.1.2 (c (n "msp430f6736a") (v "0.1.2") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "msp430") (r "^0.3.0") (d #t) (k 0)) (d (n "msp430-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1pr1djwrl1bw0v8bpnyr9rk552savf1vxx079bff8x85bpl10dhc") (f (quote (("rt" "msp430-rt/device")))) (y #t)))

