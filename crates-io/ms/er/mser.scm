(define-module (crates-io ms er mser) #:use-module (crates-io))

(define-public crate-mser-1.0.0 (c (n "mser") (v "1.0.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "0jc2026f256yyscp1frbc977scv6c09mvf4ij1ax23i1msw64mq7") (y #t)))

(define-public crate-mser-1.0.2 (c (n "mser") (v "1.0.2") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "1hwii89jqqrgdwa66l8hizzb4537rzpgvk50izwk3yaf125zl7j5") (y #t)))

(define-public crate-mser-1.0.3 (c (n "mser") (v "1.0.3") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "0zjc6c4ldpq02cy8lm1b98zfj121f9hzqbxqg6yxzn6if5s4fmak") (y #t)))

(define-public crate-mser-1.0.4 (c (n "mser") (v "1.0.4") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "0bjxgld8ya0iq2limdwibg8vnsys01b3j54q3mb8zkyvdl7m7bfa") (y #t)))

(define-public crate-mser-1.0.5 (c (n "mser") (v "1.0.5") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "1hhbhvkvnakllv6s7vdvd0bv757ishm3a7v8j68phm5qwwlja1k8") (y #t)))

(define-public crate-mser-1.1.0 (c (n "mser") (v "1.1.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "0kjmk6x2g0152f09iaibngxyv66myd2zdr1yn8m2plhrmn564ryq") (y #t)))

(define-public crate-mser-1.1.1 (c (n "mser") (v "1.1.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "0ky79p0vsrl6xjl04sz0q96zh0f4xwiv6ssaqc7xsv2bcp4253sw")))

(define-public crate-mser-1.1.2 (c (n "mser") (v "1.1.2") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "11hlmyrjq5v54fwf495wmbdslqjr40hvs0frvf2fbzf5mgz4n1y2")))

(define-public crate-mser-1.1.3 (c (n "mser") (v "1.1.3") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "17gi9399xdv41d0b6lpcy7402xryys3k5vfxzynqpim1b4zgn9ay")))

(define-public crate-mser-1.1.4 (c (n "mser") (v "1.1.4") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "simdutf8") (r "^0") (k 0)) (d (n "uuid") (r "^1") (k 0)))) (h "04qv13v3ww314p73629i0xc4wacv0d4d4rlf62h1dv5pvnz5hh81") (f (quote (("std" "simdutf8/std") ("default" "std"))))))

