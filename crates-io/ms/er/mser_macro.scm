(define-module (crates-io ms er mser_macro) #:use-module (crates-io))

(define-public crate-mser_macro-1.0.0 (c (n "mser_macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "002svh7d82jafc6niccddnvv9njdx0v69m3yjdqsc5cmrvajadjs") (y #t)))

(define-public crate-mser_macro-1.0.1 (c (n "mser_macro") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0r4xd5kx4zgxj451n6yj4ic0450y0cjmxrn0agwrx6mqh3ikfv2j") (y #t)))

(define-public crate-mser_macro-1.0.2 (c (n "mser_macro") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "08zassya54jqb41p9s5vll0m166p8r1xlwc7wqzz30axvfx2dz6j")))

(define-public crate-mser_macro-1.0.3 (c (n "mser_macro") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0whx8k4dhzbv58a320fix1hxs6fpf9f0qzlzgr707k6xkki9h9wf")))

(define-public crate-mser_macro-1.0.4 (c (n "mser_macro") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "039qzkwi7p2ki41hcfpkpdnbbdbssiv7p52wscsilscq1zqc2sj4") (y #t)))

(define-public crate-mser_macro-1.0.5 (c (n "mser_macro") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1nimrbdnzcarpn76i9gli0z2x74dh9y73dmck8ilz6yrqbxk277n") (y #t)))

(define-public crate-mser_macro-1.0.6 (c (n "mser_macro") (v "1.0.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0scrb44zzd8bpbp7pzl79izn2bsql82g8k61cn4slni9f2jn8fjp") (y #t)))

(define-public crate-mser_macro-1.0.7 (c (n "mser_macro") (v "1.0.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1p756gbgrj7iywjpcnkdgx9yzc528mbjr53mshnfx1n0c8yk9729")))

(define-public crate-mser_macro-1.0.8 (c (n "mser_macro") (v "1.0.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0h9lvmh93c78gc69fwgmarprx29pnak2l8kivbbpvzjqkbgzl236")))

(define-public crate-mser_macro-1.0.9 (c (n "mser_macro") (v "1.0.9") (d (list (d (n "mser") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0mcm7bdcz0snrl2v4ykn1hvfywf4pzq9zkcjxlyflqwjxgrxrgph")))

(define-public crate-mser_macro-1.0.10 (c (n "mser_macro") (v "1.0.10") (d (list (d (n "mser") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0bim8i9667n1i7miab853wrb8i74zazanikpn685h6ywfnwqq0i9")))

