(define-module (crates-io ms hi mshio) #:use-module (crates-io))

(define-public crate-mshio-0.4.0 (c (n "mshio") (v "0.4.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zj2chxkiasxx8b1kk2a9zgdpximr3yak0wmqydfv2rgg0y7jx5j")))

(define-public crate-mshio-0.4.1 (c (n "mshio") (v "0.4.1") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hbpnmlv7jzqjxmdxvll20dy2d77phnrnv7pk3qbw3j7vqnandld")))

(define-public crate-mshio-0.4.2 (c (n "mshio") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0blfdalif0b7lg8cv404w9b77mqiaw2p9slcyni6ksw99a2amwq1")))

