(define-module (crates-io ms u1 msu1-reverser) #:use-module (crates-io))

(define-public crate-msu1-reverser-1.0.0 (c (n "msu1-reverser") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("wrap_help" "derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kickmbj47fgdal5d1q125k4kyrlzmjq3gkbl5i06n5w2wq970fv")))

