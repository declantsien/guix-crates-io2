(define-module (crates-io ms -s ms-symfetch) #:use-module (crates-io))

(define-public crate-ms-symfetch-0.1.0 (c (n "ms-symfetch") (v "0.1.0") (d (list (d (n "cargo-deny") (r "^0.14.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (d #t) (k 0)))) (h "0273js146a2wbld0xghzaql76rnxhmslp4mfvfgh7ss168k9q3h7")))

(define-public crate-ms-symfetch-0.1.1 (c (n "ms-symfetch") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (d #t) (k 0)))) (h "0v7hb85pwdf2y27ij7qbf2xins9pacs219p0pdpqll47ddwy12kb")))

(define-public crate-ms-symfetch-0.1.2 (c (n "ms-symfetch") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (d #t) (k 0)))) (h "12k8pprksvk8p98zxx6lsffsf764n6ripfxaik5n2aknqm647c5n")))

(define-public crate-ms-symfetch-0.1.3 (c (n "ms-symfetch") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (d #t) (k 0)))) (h "0vklfl6zqafbwxb44bv03wvs83jf1a74n3wlz1b2zrw9lr3991g9")))

(define-public crate-ms-symfetch-0.1.4 (c (n "ms-symfetch") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (d #t) (k 0)))) (h "0g7aagyhjhpj90dman3x2yxl8w8wzsj096zd60c3bib4w37b9jx3")))

(define-public crate-ms-symfetch-0.1.5 (c (n "ms-symfetch") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (d #t) (k 0)))) (h "06hgmcwb0yyi7sfh6ns6wdn45nja8z6cv4swhb1ihghbx57r2a87")))

