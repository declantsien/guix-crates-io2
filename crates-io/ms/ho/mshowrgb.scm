(define-module (crates-io ms ho mshowrgb) #:use-module (crates-io))

(define-public crate-mshowrgb-0.1.0 (c (n "mshowrgb") (v "0.1.0") (h "0c9dlkhq7ywvnkkhqxbz8dgf653jzw8n9mjw5bdl92md59p4mcdv")))

(define-public crate-mshowrgb-0.1.1 (c (n "mshowrgb") (v "0.1.1") (h "1sxii5xdlfcr5nzwl202q0a1rr90mi0cjzpwcf10j3w1lmx7ss2f")))

