(define-module (crates-io ms fs msfs_derive) #:use-module (crates-io))

(define-public crate-msfs_derive-0.0.1-alpha.0 (c (n "msfs_derive") (v "0.0.1-alpha.0") (d (list (d (n "msfs_sdk") (r "^0.0.1-alpha.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04ndkh14jycc8s33sns3z1hyapg4wiy57mfgc8hfybhlag7a7v84")))

(define-public crate-msfs_derive-0.0.1-alpha.2 (c (n "msfs_derive") (v "0.0.1-alpha.2") (d (list (d (n "msfs_sdk") (r "^0.0.1-alpha.2") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xzg5h6s53p4za2n0lb23vaqgfdsldfj8awjyrlzkk4c242rqdj2")))

(define-public crate-msfs_derive-0.1.0 (c (n "msfs_derive") (v "0.1.0") (d (list (d (n "msfs_sdk") (r "^0.1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f4k0hlk40pa0jgfwpmqr0r0kw27dd6acg83h8hgs01pjs5a2w90")))

