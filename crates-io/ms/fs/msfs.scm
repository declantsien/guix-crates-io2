(define-module (crates-io ms fs msfs) #:use-module (crates-io))

(define-public crate-msfs-0.0.0-pre.1 (c (n "msfs") (v "0.0.0-pre.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "0wqxa0y0qr9glh9dv8vv2v39cv41s7s9nks6mghbh2qhzba519id")))

(define-public crate-msfs-0.0.1-alpha.0 (c (n "msfs") (v "0.0.1-alpha.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "msfs_derive") (r "^0.0.1-alpha.0") (d #t) (k 0)) (d (n "msfs_sdk") (r "^0.0.1-alpha.0") (d #t) (k 1)))) (h "0zmsq23l6hfcx2v9qn6ma5kbl4k5cwv7476sh5fysrz42ii9h8na")))

(define-public crate-msfs-0.0.1-alpha.2 (c (n "msfs") (v "0.0.1-alpha.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "msfs_derive") (r "^0.0.1-alpha.2") (d #t) (k 0)) (d (n "msfs_sdk") (r "^0.0.1-alpha.2") (d #t) (k 1)))) (h "0vlv07zqk0bj5w9illprlq4p5n32i623px0jfw9pv99shq9ish8m")))

(define-public crate-msfs-0.1.0 (c (n "msfs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "msfs_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "msfs_sdk") (r "^0.1.0") (d #t) (k 1)))) (h "1ydw4mwv80lva5af20kaip7jv8203wl191p7gv97jq2xn8jkf7bx")))

