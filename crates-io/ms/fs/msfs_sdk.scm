(define-module (crates-io ms fs msfs_sdk) #:use-module (crates-io))

(define-public crate-msfs_sdk-0.0.0 (c (n "msfs_sdk") (v "0.0.0") (h "0zyzmpsii5pdv69w5s1pb2fs6kg4y120myh2y47yab9wjpvvspg8")))

(define-public crate-msfs_sdk-0.0.1-alpha.1 (c (n "msfs_sdk") (v "0.0.1-alpha.1") (h "0h7vyr5nc4qy7mmszlvy5q1863kz4yq91xgg2ayn6dg942mkk13w")))

(define-public crate-msfs_sdk-0.0.1-alpha.2 (c (n "msfs_sdk") (v "0.0.1-alpha.2") (h "0v94qqp3n2r64yfs5ymlj5rh111mpd5nhwlad2mi2kx1kr23zbg0")))

(define-public crate-msfs_sdk-0.1.0 (c (n "msfs_sdk") (v "0.1.0") (h "0fj6407lkmziwbsm64r7ky35f1k00wy1r86470mqmssq30nqfgcg")))

