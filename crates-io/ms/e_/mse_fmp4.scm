(define-module (crates-io ms e_ mse_fmp4) #:use-module (crates-io))

(define-public crate-mse_fmp4-0.1.0 (c (n "mse_fmp4") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "mpeg2ts") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "04scasjc6wxnwxhapiw1msi9h6xwqf58rca0x55j5vaf9gmycqry")))

(define-public crate-mse_fmp4-0.1.1 (c (n "mse_fmp4") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "mpeg2ts") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "05xqkaiswak4xhq5b93gb20yfkcdy2mk97ci8ksigxk2wabkp5i7")))

(define-public crate-mse_fmp4-0.1.2 (c (n "mse_fmp4") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "mpeg2ts") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "03p6jgdyfgs7bxgyv0dm7zlyzxzdl8yv3v4a6i45ypmkjawbimr7")))

(define-public crate-mse_fmp4-0.1.3 (c (n "mse_fmp4") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "mpeg2ts") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1d7gh8xks5qb872ny4gkzyxdlha6s8l5xq9f51bj9jc0dxw0dak0")))

(define-public crate-mse_fmp4-0.2.0 (c (n "mse_fmp4") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "mpeg2ts") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0ma4q4z74c7r19f98xvz9m6vjvyc1cd3dj360mvisvcb14sgpril")))

