(define-module (crates-io ms x- msx-mcp) #:use-module (crates-io))

(define-public crate-msx-mcp-0.4.1 (c (n "msx-mcp") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0s3r9lk1c3qip4f2k1vraakncjhd9f634jy1vd8fhdhfbd82j0qj") (y #t)))

