(define-module (crates-io ms sq mssql-browser) #:use-module (crates-io))

(define-public crate-mssql-browser-0.1.0 (c (n "mssql-browser") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.29") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "udp"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "12zshgrv4rmcm0k5z2qr32q142y42x7xzhkjrfg74cpkd78zkadh") (f (quote (("default"))))))

(define-public crate-mssql-browser-0.1.1 (c (n "mssql-browser") (v "0.1.1") (d (list (d (n "async-std") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.29") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "udp"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "07p83v76skjwi9ls87jlnvsk5xa0sm3wgcmr2icjv1n79j7mnfww") (f (quote (("default"))))))

