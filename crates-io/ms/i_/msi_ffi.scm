(define-module (crates-io ms i_ msi_ffi) #:use-module (crates-io))

(define-public crate-msi_ffi-0.7.0 (c (n "msi_ffi") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "msi") (r "^0.7.0") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.1.6") (f (quote ("headers" "python-headers"))) (d #t) (k 0)))) (h "1lf2jy0qdvmji3njja1gdm1ybd97lv9m6rynlmphsp0zxcn97lvi")))

