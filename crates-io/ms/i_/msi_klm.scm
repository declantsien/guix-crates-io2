(define-module (crates-io ms i_ msi_klm) #:use-module (crates-io))

(define-public crate-msi_klm-0.1.1 (c (n "msi_klm") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2.12") (d #t) (k 0)) (d (n "hidapi_rust") (r "^0.1.1") (d #t) (k 0)))) (h "0lyhx435m5i1r84x84my8g24yzg95x9s5g2v8qjagsy3bvl35hwn")))

(define-public crate-msi_klm-0.1.2 (c (n "msi_klm") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2.12") (d #t) (k 0)) (d (n "hidapi_rust") (r "^0.1.1") (d #t) (k 0)))) (h "12dnm9h9731lzf8xs309a88p2kl0vgc0zvqscga3wh19b5all4b5")))

(define-public crate-msi_klm-0.1.3 (c (n "msi_klm") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2.12") (d #t) (k 0)) (d (n "hidapi_rust") (r "^0.1.2") (d #t) (k 0)))) (h "122yndaalg0cjazxjpgdl0x7qhrnc2mivwwsnwhaj1p99k0fn5la")))

(define-public crate-msi_klm-0.1.4 (c (n "msi_klm") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2.12") (d #t) (k 0)) (d (n "hidapi_rust") (r "^0.1.3") (d #t) (k 0)))) (h "1wrh7zg3nsg6pd3cxzdbmsnyy8sg68ibs8695s4a1bhwbx1320zz")))

(define-public crate-msi_klm-0.1.5 (c (n "msi_klm") (v "0.1.5") (d (list (d (n "getopts") (r "^0.2.12") (d #t) (k 0)) (d (n "hidapi_rust") (r "^0.1.3") (d #t) (k 0)))) (h "0bkdqfk5xl04fbxv4sksc0dnx8grwmpw4yllm4bwza7cvy0amw2v")))

(define-public crate-msi_klm-0.1.6 (c (n "msi_klm") (v "0.1.6") (d (list (d (n "getopts") (r "^0.2.12") (d #t) (k 0)) (d (n "hidapi_rust") (r "^0.1.3") (d #t) (k 0)))) (h "0i2zs5rfih8bb0di95v9n3shsiv5mi7zwyfip65qdwa68x139gr8")))

(define-public crate-msi_klm-0.2.0 (c (n "msi_klm") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2.12") (d #t) (k 0)) (d (n "hidapi") (r "^0.2.0") (d #t) (k 0)))) (h "0pyplivac7albhn992lvw1zkwrxrk341lzh220zkppz7kgjsiic8")))

(define-public crate-msi_klm-0.2.1 (c (n "msi_klm") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2.12") (d #t) (k 0)) (d (n "hidapi") (r "^0.2.0") (d #t) (k 0)))) (h "04cmb7xix9d8lv7d5kf1ns4srjnrp5b0pqsmcxjfcmi4s71iiqnc")))

(define-public crate-msi_klm-0.2.2 (c (n "msi_klm") (v "0.2.2") (d (list (d (n "getopts") (r "^0.2.12") (d #t) (k 0)) (d (n "hidapi") (r "^0.2.0") (d #t) (k 0)))) (h "0nw4iqnxpn3151rlz0as054zikd9mpvaky9wima6k9r1ph838x4i")))

(define-public crate-msi_klm-0.3.0 (c (n "msi_klm") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2.12") (d #t) (k 0)) (d (n "hidapi") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "0lygl71jv1b1kg9mlm56idg1w3bj52f7qgz2dal5xff57j5mcdb8")))

