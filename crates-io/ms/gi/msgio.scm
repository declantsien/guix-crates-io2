(define-module (crates-io ms gi msgio) #:use-module (crates-io))

(define-public crate-msgio-0.1.0 (c (n "msgio") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)) (d (n "varmint") (r "^0.1.2") (d #t) (k 0)))) (h "0p2dya6mxaqb2ffk1b8wn3f4l2r234slnm2q4734l03kspwdgfqp")))

(define-public crate-msgio-0.1.1 (c (n "msgio") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)) (d (n "varmint") (r "^0.1.2") (d #t) (k 0)))) (h "1rij885409hw4qjlcxmsfhnasr5x8m7a7kzxv0sxryqccw5qgd77")))

(define-public crate-msgio-0.1.2 (c (n "msgio") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)) (d (n "varmint") (r "^0.1.2") (d #t) (k 0)))) (h "0nmv8kkb7l15ykxndzldg1zf5js4xf67z1a09m2x9xr9mpcxaici")))

