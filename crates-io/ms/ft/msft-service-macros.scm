(define-module (crates-io ms ft msft-service-macros) #:use-module (crates-io))

(define-public crate-msft-service-macros-0.0.1 (c (n "msft-service-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1nz3wlynqd4nzpl4w6aag8w4p2h5337hgmwg09mxj37nq7hxmhjc")))

(define-public crate-msft-service-macros-0.0.2 (c (n "msft-service-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "09ppa4brvcnh3c0b9cdjv190x5gwkx8s1j0vc5xh5sgpyhkfbzk1")))

(define-public crate-msft-service-macros-0.0.3 (c (n "msft-service-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "03f62zgyylsd747qzi27vb11wgfr6pi1gsc7zd3cmipfb0rznwnb")))

(define-public crate-msft-service-macros-0.0.4 (c (n "msft-service-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1lzwb5xnxyv7qwjrr7n7f53zn5wvrd5mwbjx9nqn9swgy3a5w76h")))

(define-public crate-msft-service-macros-0.0.5 (c (n "msft-service-macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1m88sn1ln9xfqn87n9vps2zciqgbr86qv7drrbbk0rclx5f0nsmv")))

(define-public crate-msft-service-macros-0.0.6 (c (n "msft-service-macros") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0asns20labb5yacyg5cjzav8pvi010qnkx9hfy716sm4ij7g4jhq")))

(define-public crate-msft-service-macros-0.0.7 (c (n "msft-service-macros") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1y3gjjvqpbppg3rpqw40khg9fmmwrlszn8whw0kikq110hlmbvz6")))

(define-public crate-msft-service-macros-0.0.8 (c (n "msft-service-macros") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0qw6mypa9pzj9njfmk6pmwn2zjnxbb9rz5rij81ahlhxan566y41")))

(define-public crate-msft-service-macros-0.0.9 (c (n "msft-service-macros") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1qqaxakb3wppzqr956cxkmb11pql45y7z9q210fy9sm7j3w41x18")))

(define-public crate-msft-service-macros-0.0.10 (c (n "msft-service-macros") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "13i13shybssribqlhpzyqvn7bqyd23d3lf08nwn1sq0swa2wp1il")))

(define-public crate-msft-service-macros-0.0.11 (c (n "msft-service-macros") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1wmv9f0bcfykhmvfmv12b9xbq4vrmafld7k0b4cl8bzv74fma1dr")))

(define-public crate-msft-service-macros-0.0.12 (c (n "msft-service-macros") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0w9arjjskhhzcf7ya00ydqvbakzapd89a2km1ydv98nzaq9pvp7y")))

(define-public crate-msft-service-macros-0.0.13 (c (n "msft-service-macros") (v "0.0.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ya44gfsn816lx4rzw1vr33llpl9b60ppdvlqvynmks71mix0hqy")))

(define-public crate-msft-service-macros-0.0.14 (c (n "msft-service-macros") (v "0.0.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1j208m9iqbi3rb9z29q9i3p7mkbnbabinnrkw0fllrlsz5vccvrn")))

(define-public crate-msft-service-macros-0.0.15 (c (n "msft-service-macros") (v "0.0.15") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "12g896g15996v4j39r3qxra97y3dc5fry4gcsy7ikgxrxbpfird9")))

(define-public crate-msft-service-macros-0.0.16 (c (n "msft-service-macros") (v "0.0.16") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1as1x036p2av3n2kc2z1084r0ag32jn9dl92gxikfgf7maf8cxjn")))

(define-public crate-msft-service-macros-0.0.17 (c (n "msft-service-macros") (v "0.0.17") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0i8kcnzxrqnjpqh2yzm3bl9haiiikyi40dn5xamaj463j2pn2sdx")))

(define-public crate-msft-service-macros-0.0.18 (c (n "msft-service-macros") (v "0.0.18") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0vijvl43rcy1z9hsbvhr7pych03y7aarw6n6h3laq976ka5wiayw")))

(define-public crate-msft-service-macros-0.0.21 (c (n "msft-service-macros") (v "0.0.21") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "04hzqya1k8davrsqqg7cp1v0pa0nhk8dpfdirbmkxm3k21k5904c")))

(define-public crate-msft-service-macros-0.0.22 (c (n "msft-service-macros") (v "0.0.22") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1dp6h41xmxcw5ylxwwp0nhc3qqchah903crwyxk4z2dsdxfylvxr")))

(define-public crate-msft-service-macros-0.0.23 (c (n "msft-service-macros") (v "0.0.23") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0pnayk6jcrfpkvc2lransr95a2z3jdsf035kw7pjh6khyqixs781")))

(define-public crate-msft-service-macros-0.0.24 (c (n "msft-service-macros") (v "0.0.24") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1z3dzxvhnzrpmryvapddllkkfvz60vjwdykdjvz3bjk9lnihqq59")))

