(define-module (crates-io ms gb msgbus) #:use-module (crates-io))

(define-public crate-msgbus-0.1.3 (c (n "msgbus") (v "0.1.3") (h "0sl8f8b8n21x6dqcf6dgk8yy5bkilm6da690bk96ig2q01znrwrd")))

(define-public crate-msgbus-0.1.4 (c (n "msgbus") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)))) (h "0kdpvydh5xma5xkllr8j4c1c0sjbzdig0r5ijrs6ddywy72v2nqy")))

