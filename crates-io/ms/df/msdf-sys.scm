(define-module (crates-io ms df msdf-sys) #:use-module (crates-io))

(define-public crate-msdf-sys-0.1.0 (c (n "msdf-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "image") (r "^0.24.2") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.15.2") (d #t) (k 2)))) (h "1kgqlhh0s1h5i02xx793zw1w9rf0bg1y7jc202jbrkswkq9gd4bc")))

