(define-module (crates-io ms df msdfgen-lib) #:use-module (crates-io))

(define-public crate-msdfgen-lib-0.1.0 (c (n "msdfgen-lib") (v "0.1.0") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (d #t) (k 1)))) (h "1sxzbqmpywifcd5rgwkld2r3gcxw3pkiadwdq745j3m160bmmrn3") (f (quote (("stdcxx-static") ("shared") ("rustdoc") ("libcxx"))))))

