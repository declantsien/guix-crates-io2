(define-module (crates-io ms df msdfgen-sys) #:use-module (crates-io))

(define-public crate-msdfgen-sys-0.1.0 (c (n "msdfgen-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (o #t) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (o #t) (d #t) (k 1)))) (h "0m6xys67chhrc12bfmq4f5lwf85r2yjkhakjk7z72y3h2hbrp3zr") (f (quote (("rustdoc") ("generate-bindings" "fetch_unroll" "bindgen"))))))

(define-public crate-msdfgen-sys-0.2.0 (c (n "msdfgen-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)))) (h "04slxr5rg80m1c9j64smqpzvdis2a3blk7h919rz9d85lmsm2x39") (f (quote (("update-bindings" "bindgen") ("stdcxx-static") ("static") ("shared") ("rustdoc") ("libcxx"))))))

(define-public crate-msdfgen-sys-0.2.1 (c (n "msdfgen-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0c1xqqhp6c3mnvpg1b7v3im2zr19hlzgk6jfbs94jq386f1shim6") (f (quote (("update-bindings" "bindgen") ("stdcxx-static") ("static") ("shared") ("rustdoc") ("libcxx"))))))

