(define-module (crates-io ms df msdf) #:use-module (crates-io))

(define-public crate-msdf-0.1.0 (c (n "msdf") (v "0.1.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (d #t) (k 0)) (d (n "msdf-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15.2") (d #t) (k 0)))) (h "0npbzfqg304bmivk0y1rv5bbbd03zf72krs8wmz7xji0v47gbvsa")))

(define-public crate-msdf-0.2.0 (c (n "msdf") (v "0.2.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (d #t) (k 0)) (d (n "msdf-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15.2") (d #t) (k 0)))) (h "1xzlsb9hbsj8dxyi4p596g2aw3pvv2zqlx34sc3f4fn9rrak0a3z")))

(define-public crate-msdf-0.2.1 (c (n "msdf") (v "0.2.1") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (d #t) (k 0)) (d (n "msdf-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15.2") (d #t) (k 0)))) (h "1h55kiwzp6b8wajqc3zxfx8jzsa8m1shwny38j0ssp53ndz3awxr")))

