(define-module (crates-io ms gr msgrpc) #:use-module (crates-io))

(define-public crate-msgrpc-0.1.0 (c (n "msgrpc") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8.9") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)))) (h "0szm9qz1fs72xkwgx6603vb0pqx224hq6wx1wxbaw96wzwaapjf2")))

