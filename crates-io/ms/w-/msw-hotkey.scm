(define-module (crates-io ms w- msw-hotkey) #:use-module (crates-io))

(define-public crate-msw-hotkey-0.1.0 (c (n "msw-hotkey") (v "0.1.0") (h "1f6yhabsd13dvdwfvws7a64sic0h8fiz6bqkb6zmdpb6w93c9whc") (y #t)))

(define-public crate-msw-hotkey-0.1.1 (c (n "msw-hotkey") (v "0.1.1") (h "0lrl7lwcdssndxgl0jk2gscwzgwvy2cl2b635sc9rcsr2f0bq9d3")))

(define-public crate-msw-hotkey-0.2.0 (c (n "msw-hotkey") (v "0.2.0") (h "00y0r252lxdx588bhp93cqxai8jjx6s09xm1kdvlijkbnxbgs9xf")))

(define-public crate-msw-hotkey-0.2.1 (c (n "msw-hotkey") (v "0.2.1") (h "1rzzk20ikz2wyimhkw67wdw5yxg9pq017psl5kndwm5w9azjq31h")))

