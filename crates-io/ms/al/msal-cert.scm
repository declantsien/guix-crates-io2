(define-module (crates-io ms al msal-cert) #:use-module (crates-io))

(define-public crate-msal-cert-0.1.0 (c (n "msal-cert") (v "0.1.0") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "jwt") (r "^0.16.0") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.64") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ksygx5m1n8fas6mf4h56zvbsabb9sfmkcwcq711jp00hq49bz9v")))

