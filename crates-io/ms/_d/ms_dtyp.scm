(define-module (crates-io ms _d ms_dtyp) #:use-module (crates-io))

(define-public crate-ms_dtyp-0.0.1 (c (n "ms_dtyp") (v "0.0.1") (h "1rczkkz0s8dg5x9xp33glviaaszaqi49adypjgcxcrzy42bivlwy")))

(define-public crate-ms_dtyp-0.0.2 (c (n "ms_dtyp") (v "0.0.2") (h "1dk3a1yv3mvwj7hbal7yknrhxmmrby7ga07a48cachq92nvy4wfb")))

(define-public crate-ms_dtyp-0.0.3 (c (n "ms_dtyp") (v "0.0.3") (h "09ygj2mnc02j41wc3z284cbsk32j1hj5m6q28c3zspbcbm189fn0")))

