(define-module (crates-io ms #{3d}# ms3d) #:use-module (crates-io))

(define-public crate-ms3d-0.1.0 (c (n "ms3d") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "1w0wclg9mh9rcnz1s3hv82i3w2rasl4ig7ysdfky64cm9k3qvbpk")))

(define-public crate-ms3d-0.1.1 (c (n "ms3d") (v "0.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "03bbb5qmh5qh3zj1p177nq1rczbsax4kl05cjlyldglh2hjypad5")))

(define-public crate-ms3d-0.1.2 (c (n "ms3d") (v "0.1.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "0y2fc1b3gxknjy4hw60264lc4nxasjmwh98w9pg47kdnh47jkng0")))

(define-public crate-ms3d-0.1.3 (c (n "ms3d") (v "0.1.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "1bl4bqhi3y3zsbdab96mnfvdg9mzf4aqjxkv9cz41i18iggd1gpa")))

