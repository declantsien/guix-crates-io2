(define-module (crates-io ms ta mstak-util) #:use-module (crates-io))

(define-public crate-mstak-util-0.1.1 (c (n "mstak-util") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "181qkx4fapiam60rxm8q84hlvg34sbfl0sl2z7diggb84vx00h01")))

(define-public crate-mstak-util-0.1.2 (c (n "mstak-util") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "0nkindli60f85w6m3ccx0i53y6lra5jp1472kmk3xbji6nzm63h9")))

(define-public crate-mstak-util-0.1.3 (c (n "mstak-util") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "1y6c5w5x8672myxjw0fh0r0q10l791i8wx8pv74rcq97b6rabm09")))

(define-public crate-mstak-util-0.1.4 (c (n "mstak-util") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "1cm8k3j1lyq2368la71q4qhnbmfpgap9d6qdbzl627bbf62fvz7j")))

(define-public crate-mstak-util-0.1.5 (c (n "mstak-util") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "1g0gi8pgif3d8ij373xcrdcxq5fw1pwksq2cq4ldl0bpg5m9yxyv")))

(define-public crate-mstak-util-0.1.6 (c (n "mstak-util") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "0d21gw6sbdh52ar29mqckxw109rhwrp6shjvgqgr5hs2aldprnfl")))

(define-public crate-mstak-util-0.1.7 (c (n "mstak-util") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "1vlms1z18hfcbpg5s52hygx5k2xq9b30fj82rgz3409j3qqw20pw")))

(define-public crate-mstak-util-0.1.8 (c (n "mstak-util") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "0xjmxjsibna77448b5wvqd5cqvrd00qfqb6dcph9pj1zd3rq2lvv")))

(define-public crate-mstak-util-0.1.9 (c (n "mstak-util") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "00wsv7lvm6k9rcinbp2vq44jk3f7c4n893dnzdk3jjg4905s8dkk")))

(define-public crate-mstak-util-0.1.10 (c (n "mstak-util") (v "0.1.10") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "01r2q0g20q6ddzch4hfs8jnfypr5nkrc4krwbskcjr6xdx3g3q80")))

(define-public crate-mstak-util-0.1.11 (c (n "mstak-util") (v "0.1.11") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "08azhk6iw16dlvqvmla55njyhyvdvwkg17b2jhjxzvfzqx8bm381")))

(define-public crate-mstak-util-0.1.12 (c (n "mstak-util") (v "0.1.12") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "0fqqz490ihvq66cllh45jhqi0nlf12phyrclqgv7ravr6f5vn06j")))

(define-public crate-mstak-util-0.1.13 (c (n "mstak-util") (v "0.1.13") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "1fx73ps21q6ih1z86c7lw8r951zbzgipqqb2bhv0aa4wlljxcr39")))

(define-public crate-mstak-util-0.1.14 (c (n "mstak-util") (v "0.1.14") (d (list (d (n "libc") (r "^0.2.153") (k 0)))) (h "12ksaaj7k46bz8ilz8mgdxsdvc961nainknw6n1m9kibmdq3a32j")))

(define-public crate-mstak-util-0.1.15 (c (n "mstak-util") (v "0.1.15") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0yw337p804nq517bzyqy9q8pq6gfbmfcyf73rmp5dcng3szbd65w")))

(define-public crate-mstak-util-0.1.16 (c (n "mstak-util") (v "0.1.16") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1rr9052dlp8gmr6y7zfxddhqgfmhk3cjwn4wjrhkadzrxr1hdjjz")))

