(define-module (crates-io ms ta mstak-interpret) #:use-module (crates-io))

(define-public crate-mstak-interpret-0.1.0 (c (n "mstak-interpret") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3.25") (d #t) (k 0)))) (h "1bljg8c1bz1mhma4imx1kq3asrd692r343siii8sfavc67dfhgn0")))

(define-public crate-mstak-interpret-0.1.1 (c (n "mstak-interpret") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "mstak-util") (r "^0.1.2") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3.25") (d #t) (k 0)))) (h "0kykwky8gkdr4lhynxnzn44f8f2dbnhajy2kpjpv904rq0v26sdj")))

(define-public crate-mstak-interpret-0.1.2 (c (n "mstak-interpret") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "mstak-util") (r "^0.1.3") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3.25") (d #t) (k 0)))) (h "00v18i0dx19n1zmdaig7xj8pimcmdqwnl7znpzxg699x39qmhx06")))

(define-public crate-mstak-interpret-0.1.3 (c (n "mstak-interpret") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "mstak-util") (r "^0.1.4") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3.25") (d #t) (k 0)))) (h "0i02jpbaad62mqs00yxylj8bznm4hv3jg9a4wnzbccrcvvpsdzhf")))

(define-public crate-mstak-interpret-0.1.4 (c (n "mstak-interpret") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "mstak-util") (r "^0.1.5") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3.25") (d #t) (k 0)))) (h "0hldvfka4rh7l07lvqx93k5imfcrkcggd85wyjq7zlfp919gqm5n")))

(define-public crate-mstak-interpret-0.1.5 (c (n "mstak-interpret") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "mstak-util") (r "^0.1.6") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.3.25") (d #t) (k 0)))) (h "0g9p7gxxnp00pqhm4x5mircrkb6f03i4yrcayhivxh0vd7n0564x")))

(define-public crate-mstak-interpret-0.1.6 (c (n "mstak-interpret") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "mstak-util") (r "^0.1.7") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4.0") (d #t) (k 0)))) (h "066hsj5ggqyb9055n0ki6dkycir0g1lk2jn9vb9lk98h5akyksdq")))

(define-public crate-mstak-interpret-0.1.7 (c (n "mstak-interpret") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "mstak-util") (r "^0.1.8") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4.0") (d #t) (k 0)))) (h "1adsj4f1kmfjw5qi6y0a9vjslgmjfrxkiywrkcjhf58w8an0hrd7")))

(define-public crate-mstak-interpret-0.1.8 (c (n "mstak-interpret") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "mstak-util") (r "^0.1.9") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4.0") (d #t) (k 0)))) (h "0ab8wvga8vcaglx6bkf46bnzhl74c2lh5ia9azgmlg73cv166xmz")))

(define-public crate-mstak-interpret-0.1.9 (c (n "mstak-interpret") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "mstak-util") (r "^0.1.10") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4.0") (d #t) (k 0)))) (h "1qz2m73v0k1zkz0lkpg5mh8vwgchwpv5n21ds3idic0v7s3f8v2r")))

(define-public crate-mstak-interpret-0.1.10 (c (n "mstak-interpret") (v "0.1.10") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "mstak-util") (r "^0.1.11") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4.0") (d #t) (k 0)))) (h "1r0sahcndwgpcvnq5mfm1qyg77sm4wv7vh26r3537jn75y610nr5")))

(define-public crate-mstak-interpret-0.1.11 (c (n "mstak-interpret") (v "0.1.11") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "mstak-util") (r "^0.1.12") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4.0") (d #t) (k 0)))) (h "1fdi08514vfblhp27690yql2d59f6xv3xw2sqkh84d86syr061aa")))

(define-public crate-mstak-interpret-0.1.12 (c (n "mstak-interpret") (v "0.1.12") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "mstak-util") (r "^0.1.13") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4.0") (d #t) (k 0)))) (h "1fw1fn9rx5j2853dkx6zrzqswap6hk814fzz2hcqacrgc6ww6h4r")))

(define-public crate-mstak-interpret-0.1.13 (c (n "mstak-interpret") (v "0.1.13") (d (list (d (n "libc") (r "^0.2.153") (k 0)) (d (n "mstak-util") (r "^0.1.14") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4.0") (d #t) (k 0)))) (h "1r2hsklp985asvvnfwahcwn0fg6cf4a2hs2q7lwchdma5nnjf89w")))

(define-public crate-mstak-interpret-0.1.14 (c (n "mstak-interpret") (v "0.1.14") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "mstak-util") (r "^0.1.15") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4.0") (d #t) (k 0)))) (h "1a8wf0j98sfy9sjv9rq1a6rlbbqx6hxh43w3jfp2d46fg2jfifmm")))

(define-public crate-mstak-interpret-0.1.15 (c (n "mstak-interpret") (v "0.1.15") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "mstak-util") (r "^0.1.16") (d #t) (k 0)) (d (n "stak-device") (r "^0.2.24") (f (quote ("libc"))) (d #t) (k 0)) (d (n "stak-primitive") (r "^0.2.25") (d #t) (k 0)) (d (n "stak-vm") (r "^0.4.0") (d #t) (k 0)))) (h "1as3dip6lzgxjaxs42kgw44c35mg6cb9v6msvj2yl4yq8lh8lm1m")))

