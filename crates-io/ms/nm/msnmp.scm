(define-module (crates-io ms nm msnmp) #:use-module (crates-io))

(define-public crate-msnmp-0.1.0 (c (n "msnmp") (v "0.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "snmp_mp") (r "^0.1.0") (d #t) (k 0)) (d (n "snmp_usm") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1l8rj55l8867377y6cqcpq97jdxil85g21f1i0h79xql00xjxca2")))

(define-public crate-msnmp-0.1.1 (c (n "msnmp") (v "0.1.1") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "snmp_mp") (r "^0.1.0") (d #t) (k 0)) (d (n "snmp_usm") (r "^0.2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "0c3x66nvrclgd6yziijdcsf11jd9xi4pgqaw0qz1l17rvj509rlv")))

