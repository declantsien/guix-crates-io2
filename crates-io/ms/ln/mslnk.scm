(define-module (crates-io ms ln mslnk) #:use-module (crates-io))

(define-public crate-mslnk-0.1.0 (c (n "mslnk") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "0i23wbv7zmfh1dwgm93qpkgyg5wyw6frbklqx59k43wbv1rxnfh7")))

(define-public crate-mslnk-0.1.1 (c (n "mslnk") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "129vy9c7f7058awz8z5yiic9wspaxkchr3q3s5dc04ng1j3gljq2")))

(define-public crate-mslnk-0.1.2 (c (n "mslnk") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "04h0hfh3b0xaknqiy9icxg9ja4m6y2jnhkw86xaqldfb0md7f42d")))

(define-public crate-mslnk-0.1.3 (c (n "mslnk") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "1q8kgmq7zc1hm2bk3j47d407wcc5gx8yxqqw0hzg6i1jmk1z65f9")))

(define-public crate-mslnk-0.1.4 (c (n "mslnk") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "0cc7zd9p2ziq2sc2mvkgka824k7c4hi2p9jcx0d2mq4q6jbm2pgl")))

(define-public crate-mslnk-0.1.5 (c (n "mslnk") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "0p7129dlh5arl4jwyyl7qzh2fkr0riqj7idyw6ib7rr49xwk4cmf")))

(define-public crate-mslnk-0.1.6 (c (n "mslnk") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "0wykp029x63marzrnsw7732gpyyfa70f98kaaxx7r7gx62g7mvsy")))

(define-public crate-mslnk-0.1.7 (c (n "mslnk") (v "0.1.7") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1jkwx5fl08z4v1lgfv1s8527033v94qwih7kga38316ycdp9kc7s")))

(define-public crate-mslnk-0.1.8 (c (n "mslnk") (v "0.1.8") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0xjw6xaavlq9zcq9vjk8zyarvf92g3d905pkjdm4jzqb2l877jc6")))

