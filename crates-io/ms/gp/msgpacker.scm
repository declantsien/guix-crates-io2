(define-module (crates-io ms gp msgpacker) #:use-module (crates-io))

(define-public crate-msgpacker-0.1.0 (c (n "msgpacker") (v "0.1.0") (h "0vnspir4jnlbvzzws81hwxlifrzz8lqx72ajmh54hnmdlynl1zqv")))

(define-public crate-msgpacker-0.1.1 (c (n "msgpacker") (v "0.1.1") (h "0zx138nk2a2jkqjig0n300n3bq72y5qj01w2szxw85f45dwlyx47")))

(define-public crate-msgpacker-0.1.3 (c (n "msgpacker") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0llnbd5z3ibazz9z6pzwf3w1pl6kc4mzh2sjrcbix593qrn5svpk")))

(define-public crate-msgpacker-0.1.4 (c (n "msgpacker") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "03a5ywm4s562ih1r19c1cqhcxhm274hryfrfa5fxprii29jx4p7j")))

(define-public crate-msgpacker-0.1.5 (c (n "msgpacker") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1hklyl1db0l1mcxv6746vw6n4hbisr65yfd3by98ng76021z33mk")))

(define-public crate-msgpacker-0.1.6 (c (n "msgpacker") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0q9vrs366ysm0rcis42va3znn2rlfmy8zsn3pc6k8hfdibhzz3cv")))

(define-public crate-msgpacker-0.1.7 (c (n "msgpacker") (v "0.1.7") (h "1qsgdgggq092aqkswnadq7iaj0chzm3zg8zpcnqkj98dgcq01nrx")))

(define-public crate-msgpacker-0.2.0 (c (n "msgpacker") (v "0.2.0") (d (list (d (n "msgpacker-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "181jsawx3162y4agcd0v7zrqnskappwdy80y9f06a7wd3b7akzwz") (f (quote (("derive" "msgpacker-derive") ("default" "derive"))))))

(define-public crate-msgpacker-0.2.1 (c (n "msgpacker") (v "0.2.1") (d (list (d (n "msgpacker-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "09qkwad69xf4bmi3a3cfhq3bjwlh40349c5zkyrrmwdw6ym75ssn") (f (quote (("derive" "msgpacker-derive") ("default" "derive"))))))

(define-public crate-msgpacker-0.2.2 (c (n "msgpacker") (v "0.2.2") (d (list (d (n "msgpacker-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1jdkslxkwxaka7wdz2y4prr32v9mzg9c0linmp5r0h8vyb0qn1dn") (f (quote (("derive" "msgpacker-derive") ("default" "derive"))))))

(define-public crate-msgpacker-0.3.0 (c (n "msgpacker") (v "0.3.0") (d (list (d (n "msgpacker-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1pgawcaxj1rz66vvb5cabr3yx45zpvx6yks8ss5b651c2ghr9jiz") (f (quote (("impl-io") ("derive" "msgpacker-derive") ("default" "derive" "impl-io"))))))

(define-public crate-msgpacker-0.3.1 (c (n "msgpacker") (v "0.3.1") (d (list (d (n "msgpacker-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0xg8hjck8xw7qjkx332c2fzvazbljx6xqc9qx7dc3pmjy9wiq4f8") (f (quote (("impl-io") ("derive" "msgpacker-derive") ("default" "derive" "impl-io"))))))

(define-public crate-msgpacker-0.4.0 (c (n "msgpacker") (v "0.4.0") (d (list (d (n "msgpacker-derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)))) (h "1b3bhi9z0xmmq2dkjmi52w8af7q0mh0bxnbgd8bnvavd8qar45sq") (f (quote (("strict") ("derive" "msgpacker-derive") ("default" "alloc" "derive") ("alloc"))))))

(define-public crate-msgpacker-0.4.1 (c (n "msgpacker") (v "0.4.1") (d (list (d (n "msgpacker-derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)))) (h "0b61xcyb90syk4psg9fp5q99xnn4nf60z6knjn96xfv5hz10b9i1") (f (quote (("strict") ("derive" "msgpacker-derive") ("default" "alloc" "derive") ("alloc"))))))

(define-public crate-msgpacker-0.4.2 (c (n "msgpacker") (v "0.4.2") (d (list (d (n "msgpacker-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)))) (h "0mhl5i2ll85gjw4gwbgy0fhy6qh4aw9dlifal76kp0dw1wah36rm") (f (quote (("strict") ("std" "alloc") ("derive" "msgpacker-derive") ("default" "std" "derive") ("alloc"))))))

(define-public crate-msgpacker-0.4.3 (c (n "msgpacker") (v "0.4.3") (d (list (d (n "msgpacker-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)))) (h "1xj9hb8a3v8p00aak61shsafli566wsz6vdl6kldg7m9110ppxvr") (f (quote (("strict") ("std" "alloc") ("derive" "msgpacker-derive") ("default" "std" "derive") ("alloc"))))))

