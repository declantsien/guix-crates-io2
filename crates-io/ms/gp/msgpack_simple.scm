(define-module (crates-io ms gp msgpack_simple) #:use-module (crates-io))

(define-public crate-msgpack_simple-1.0.0 (c (n "msgpack_simple") (v "1.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)))) (h "00s9s56vw0zbvrr16j47f03hb516w3kxxvsal4bw5s7wha7bdfg4")))

(define-public crate-msgpack_simple-1.0.1 (c (n "msgpack_simple") (v "1.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)))) (h "1cffvi32kjrai5m9kvmpa79va46ji2frg3as815rfalyvwkhd74p")))

(define-public crate-msgpack_simple-1.0.2 (c (n "msgpack_simple") (v "1.0.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)))) (h "0rdidkkq5na0zayqsr4ri7pkxkkqgy6dh3f8qlhr25riw2rany03")))

