(define-module (crates-io ms gp msgpack-rpc) #:use-module (crates-io))

(define-public crate-msgpack-rpc-0.4.2 (c (n "msgpack-rpc") (v "0.4.2") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1.4") (f (quote ("macros" "net" "time"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.4") (f (quote ("codec" "compat"))) (d #t) (k 0)))) (h "0h3k2jhmff5gvkwh7kpx5sj79n40pakxa1k8a3qmwzlqxxhj9bjg") (r "1.60.0")))

