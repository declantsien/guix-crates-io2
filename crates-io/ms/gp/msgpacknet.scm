(define-module (crates-io ms gp msgpacknet) #:use-module (crates-io))

(define-public crate-msgpacknet-0.1.0 (c (n "msgpacknet") (v "0.1.0") (d (list (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "18ihxbqx4dlw95ip0y5lv3vsv4dk29yw94g8ljwp8c6qvcwgaq0a")))

(define-public crate-msgpacknet-0.1.1 (c (n "msgpacknet") (v "0.1.1") (d (list (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "02sggvfg3a4vg1d0q9aqp2xps71x9nj0xn109in26p05ljcmwmjw")))

