(define-module (crates-io ms gp msgpack-cli-viewer) #:use-module (crates-io))

(define-public crate-msgpack-cli-viewer-1.0.0 (c (n "msgpack-cli-viewer") (v "1.0.0") (d (list (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.7") (d #t) (k 0)))) (h "018ja9fqcbqqdsqyn6w5sfzbh855k3maz6gagr66l8m3pf9vy6w8")))

