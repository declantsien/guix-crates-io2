(define-module (crates-io ms gp msgp-abi) #:use-module (crates-io))

(define-public crate-msgp-abi-0.1.0 (c (n "msgp-abi") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "msgp") (r "^0.1.1") (d #t) (k 0)))) (h "0bd3isq08w5r78imldj3pn6qdqp3xha30w01ndjddf95y6i9n187")))

(define-public crate-msgp-abi-0.1.1 (c (n "msgp-abi") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "msgp") (r "^0.1.1") (d #t) (k 0)))) (h "05005i7913ffzw2yajd2x2lydjlb7kbizi9s0ggna50wbp3chnnp")))

