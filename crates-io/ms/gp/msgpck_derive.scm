(define-module (crates-io ms gp msgpck_derive) #:use-module (crates-io))

(define-public crate-msgpck_derive-0.1.0 (c (n "msgpck_derive") (v "0.1.0") (d (list (d (n "embedded-io-async") (r "^0.6.0") (d #t) (k 2)) (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.4.0") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "smol") (r "^1.3.0") (d #t) (k 2)) (d (n "smol-potat") (r "^1.1.2") (d #t) (k 2)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "derive" "parsing" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "1zk86lx4hsrg32dnqcdcwy2ndq540cnmhdidsfk79gas8700zi7c")))

(define-public crate-msgpck_derive-0.2.8 (c (n "msgpck_derive") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "08kqw8id1nyadz85z26vkq3p7zk8avzcz39zpnbj4aq2q3680z38")))

