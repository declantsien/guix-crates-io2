(define-module (crates-io ms gp msgpacker-derive) #:use-module (crates-io))

(define-public crate-msgpacker-derive-0.1.0 (c (n "msgpacker-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "173rx3a2c7qnsajhf2vll6xr6rbzsgmsvc094w8wz74m1ka4y0y6")))

(define-public crate-msgpacker-derive-0.1.1 (c (n "msgpacker-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fh4c4c6safyn1zirql5cp5srajwc8n7ns7b68v1h1v67yvhq41j")))

(define-public crate-msgpacker-derive-0.1.2 (c (n "msgpacker-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vx96rifmmi83wc4wvkiv0d0gwf05maxncl03hjmi74r7b9fvxk7")))

(define-public crate-msgpacker-derive-0.2.0 (c (n "msgpacker-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r36d52pdhi6i521dg02bq998nmb69wrs7g0vmiwlxij3fsv5y2m")))

(define-public crate-msgpacker-derive-0.3.0 (c (n "msgpacker-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1amvm7wqrxrlf4999p34iacc5a1fvss2jh2f09x1mn1r4v4sfk38")))

(define-public crate-msgpacker-derive-0.3.1 (c (n "msgpacker-derive") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nkvymzhwq8vjxbj686dzm5gy74axrhsnrq628c615qdmjjyq9p9")))

