(define-module (crates-io ms gp msgpack-value) #:use-module (crates-io))

(define-public crate-msgpack-value-0.4.2 (c (n "msgpack-value") (v "0.4.2") (d (list (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0ii1gqvaycgxamd40r8w9j3sa8chrwr5irq3aklnxnr5361gv69m")))

(define-public crate-msgpack-value-0.5.0 (c (n "msgpack-value") (v "0.5.0") (d (list (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1djmygjry8sgx8bbxlwd6y4ij9jsfcikahsi9qld54axfr5sfmny")))

(define-public crate-msgpack-value-0.6.0 (c (n "msgpack-value") (v "0.6.0") (d (list (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0ba2mw4xmkxhqdq7iwhm2qsj2c3ynsa4bpkhigvvrs4qwy46asic")))

(define-public crate-msgpack-value-0.7.0 (c (n "msgpack-value") (v "0.7.0") (d (list (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0g80j1fpb86n8k2640wdgx7g4fh8jclszjxig6h5yaj15ixs26af")))

(define-public crate-msgpack-value-0.8.0 (c (n "msgpack-value") (v "0.8.0") (d (list (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1jh13hsg0bjmwfpbl8jpffwn9yflpqylpnqns3slkyr9f9cqgv0x")))

(define-public crate-msgpack-value-0.8.1 (c (n "msgpack-value") (v "0.8.1") (d (list (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0vlrqk8r88pmgdg3srkd1cnv1vdbaxb9m1p3qmxhf21s58blnlzq")))

(define-public crate-msgpack-value-1.0.0 (c (n "msgpack-value") (v "1.0.0") (d (list (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1ij5gw41kgj99vc3rxwhjhknyz9imk80rnshyggddw6wbvxafi0s")))

(define-public crate-msgpack-value-1.0.1 (c (n "msgpack-value") (v "1.0.1") (d (list (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "08bjpsb52q26sd05mczab6lvx86v9bbp2idz9r41px9x2k770gw5")))

(define-public crate-msgpack-value-1.1.0 (c (n "msgpack-value") (v "1.1.0") (d (list (d (n "proptest") (r "^1") (o #t) (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0yvpj3i5vvy4pyq8jd2vm5w3rd5afn1gpgpn8qw3lf171b2q9dm9") (f (quote (("default" "proptest")))) (s 2) (e (quote (("proptest" "dep:proptest" "dep:proptest-derive"))))))

