(define-module (crates-io ms gp msgpack-rs) #:use-module (crates-io))

(define-public crate-msgpack-rs-0.0.1 (c (n "msgpack-rs") (v "0.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0igli7zq7pfy50gc9p8va9jvj0dcxpk8w7khbz95iigf67klq34x")))

