(define-module (crates-io ms gp msgpass) #:use-module (crates-io))

(define-public crate-msgpass-0.1.0 (c (n "msgpass") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (d #t) (k 2)))) (h "0fkr91dzx00nix8ablrms0w49cspkkq80hzdcj278vvdqlwkwxzq")))

(define-public crate-msgpass-0.2.0 (c (n "msgpass") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (d #t) (k 2)))) (h "0iy3l42jvslfxj7q6svn35dvngahf0df8yp0klsqcybgms4yi5b0")))

(define-public crate-msgpass-0.2.1 (c (n "msgpass") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (d #t) (k 2)))) (h "178j34ykjhbwfghlasb8cgx2zvz3i36s1s8v4zwbrw2lapqf7mki")))

(define-public crate-msgpass-0.2.2 (c (n "msgpass") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (d #t) (k 2)))) (h "0z03iar3g0azy8askz8djb8l3vvv0453i1lrkjmwvcjs597krd32")))

(define-public crate-msgpass-0.3.0 (c (n "msgpass") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (d #t) (k 2)))) (h "1772iaayzdwvxl6gnwwxcp2w7qxh2pzlfyrz6qxdz244w3mw9j2h")))

(define-public crate-msgpass-0.4.0 (c (n "msgpass") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (d #t) (k 2)))) (h "0rpqwgnpcz9qh0fvwvrcm5qqad16bj49fy1f5087a64af33whzq8")))

(define-public crate-msgpass-0.5.0 (c (n "msgpass") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (d #t) (k 2)))) (h "1lrk2fl4h94cw4k2izg79xfi2s1qk3ad1w6ks8p0knd6p3c4lpi0") (f (quote (("mpich") ("intel"))))))

