(define-module (crates-io ms gp msgpackin_core) #:use-module (crates-io))

(define-public crate-msgpackin_core-0.0.1 (c (n "msgpackin_core") (v "0.0.1") (h "0avxg8axah85drqadspk3yj3rxgxp4zcz8hda925r5rhnv4kr77c")))

(define-public crate-msgpackin_core-0.0.2 (c (n "msgpackin_core") (v "0.0.2") (h "0ql96d3ljz72gbhbybfwr1l76f5wh0bc5lybxgydwmrvkxmmr0qf")))

(define-public crate-msgpackin_core-0.0.3 (c (n "msgpackin_core") (v "0.0.3") (h "0qi35z83mh4g4pg4xa0yw44rwj34ak34hdbgjqqbmqaj8igc3dfq")))

(define-public crate-msgpackin_core-0.0.4 (c (n "msgpackin_core") (v "0.0.4") (h "0n6886q8vhksmnbdjz3iwm6kgypq6iz56drnl2sw1kwrf43h1zr8")))

