(define-module (crates-io ms qu msquic) #:use-module (crates-io))

(define-public crate-msquic-1.5.0-alpha (c (n "msquic") (v "1.5.0-alpha") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "06j27zga7ssb4sjasz4axxvy2nsm9v41ly0y7bl27ys09xdxrcbl") (y #t)))

(define-public crate-msquic-1.5.0-alpha2 (c (n "msquic") (v "1.5.0-alpha2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "09zajbg30j992hr0lx9prg4vwqm6j8i37w4zcy7as6mny1cn56xr") (y #t)))

(define-public crate-msquic-1.5.0-alpha3 (c (n "msquic") (v "1.5.0-alpha3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1anqc4m8zdv23j4fdlf2rwwv2gi2pab5imwd5wpp9gzwcf4dlmgn") (y #t)))

(define-public crate-msquic-1.6.0-alpha4 (c (n "msquic") (v "1.6.0-alpha4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "08vbawz59yl271j8nmc5h8qg7ajfnhrgp8c2w1spqlis6n748vp6") (y #t)))

(define-public crate-msquic-1.6.0-alpha5 (c (n "msquic") (v "1.6.0-alpha5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1qam603ndl0x499vv52rrvv5a0gbdbkf7jqgjqgbpcc88j14gw7x") (y #t)))

(define-public crate-msquic-1.6.0-alpha6 (c (n "msquic") (v "1.6.0-alpha6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0rgip4skyyhw92qijxql1sablyvc8yl57r04fxql3qgl5l08y8k1") (y #t)))

(define-public crate-msquic-1.8.0-alpha7 (c (n "msquic") (v "1.8.0-alpha7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1wa87b37dp6ack9zwm12acl11ldpj79fbd7hxvffw5as11631d91") (y #t)))

(define-public crate-msquic-1.10.0-alpha8 (c (n "msquic") (v "1.10.0-alpha8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ivhgnh515v1nw3ad0sw6yw2j1b6vw7z565s4m1icvp7bald3pjs")))

(define-public crate-msquic-2.0.0-beta (c (n "msquic") (v "2.0.0-beta") (d (list (d (n "c-types") (r "^1.2.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "14myshk9ckic5jpjk1j08yisq22fw5a99bcgn1h9rmlb3cwgqc72") (y #t)))

(define-public crate-msquic-2.0.1-beta (c (n "msquic") (v "2.0.1-beta") (d (list (d (n "c-types") (r "^1.2.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "12ibn6bk0adrkzkj7sl2qlaf63zfdpava8qfj6003s2gv7ffw1b9") (y #t)))

(define-public crate-msquic-2.0.1-beta2 (c (n "msquic") (v "2.0.1-beta2") (d (list (d (n "c-types") (r "^1.2.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "027jsqwm5avyn6il9bvbwxkm3n8mhwkab5zf3brfnhn112c0xpzs")))

(define-public crate-msquic-2.0.2-beta (c (n "msquic") (v "2.0.2-beta") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "c-types") (r "^1.2.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c1l0mvcjc87d0jyhr4ml6bzidfqsy98zlfcvy5l9c1myhrw9ja2")))

(define-public crate-msquic-2.1.0-beta (c (n "msquic") (v "2.1.0-beta") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "c-types") (r "^2.0.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i78qvjcvasj9gv2izinyxvwx9ixfi60l33pkb9r9x5f30ikl41m")))

(define-public crate-msquic-2.1.1-beta (c (n "msquic") (v "2.1.1-beta") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "c-types") (r "^2.0.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lzqmgml78p1wqgd8m955j8vmi5vsd51hy7nhlxf9y7pl28rxy6b")))

(define-public crate-msquic-2.1.2-beta (c (n "msquic") (v "2.1.2-beta") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "c-types") (r "^2.0.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fbv3cvh4nnv6n0sfz647yab3f4ibd3aj62l1cvjhfxlsjpihw53")))

(define-public crate-msquic-2.1.3-beta (c (n "msquic") (v "2.1.3-beta") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "c-types") (r "^2.0.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "06v0jbcblv1m1y3pais1hxyqp988c7cwvh5xa8hvby2k5xvrrvpv")))

