(define-module (crates-io ms bf msbfinal_db) #:use-module (crates-io))

(define-public crate-msbfinal_db-0.1.0 (c (n "msbfinal_db") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qfg3i6h6fww28582pc6p3p7884iqndp4lf23i03g8p70i0lrnbn")))

