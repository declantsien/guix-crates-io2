(define-module (crates-io ms ic msica) #:use-module (crates-io))

(define-public crate-msica-0.1.0 (c (n "msica") (v "0.1.0") (h "1wm2jmj7kcndxn17jb5iirf9w78shpa5z3wwqlrbi9wibxq9bckh")))

(define-public crate-msica-0.2.0 (c (n "msica") (v "0.2.0") (h "0k0yxhwnk4a8xg53y4i17ijh3qcq7fdx6dlcvagy7cnxcm3yaghq")))

(define-public crate-msica-0.3.0 (c (n "msica") (v "0.3.0") (h "0xxamdlpq3fy2cr4jq28xrrrysk2fvkhrmpgaaadjqrasiv0g5c3") (f (quote (("nightly") ("default"))))))

(define-public crate-msica-0.4.0 (c (n "msica") (v "0.4.0") (h "04d966kynr2pp96924iwwjp5cgprda1ab9zvf1r2njwrcz43j9sv") (f (quote (("nightly") ("default"))))))

