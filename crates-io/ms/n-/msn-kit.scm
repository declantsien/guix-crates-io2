(define-module (crates-io ms n- msn-kit) #:use-module (crates-io))

(define-public crate-msn-kit-0.1.0 (c (n "msn-kit") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "069gmyi5m7afaw2r3x65y77dwl1rhida9gvh5raa2nzrm9dwk4rk")))

(define-public crate-msn-kit-0.2.3 (c (n "msn-kit") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "160za2dypifk4q9h9kxng3blbpfx47j0rr5myafkf16rqhbhzvnw")))

(define-public crate-msn-kit-0.2.4 (c (n "msn-kit") (v "0.2.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structure") (r "^0.1") (d #t) (k 0)))) (h "0xpyysk8pj1sqnqlh4pwfldgk5gmm9chl6pjj4nq55rffzab3hmn")))

(define-public crate-msn-kit-0.2.5 (c (n "msn-kit") (v "0.2.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structure") (r "^0.1") (d #t) (k 0)))) (h "1qrhlkmfhpa68763f9lcih2076217ncb5drnwmwy6jpxilawg8lq")))

