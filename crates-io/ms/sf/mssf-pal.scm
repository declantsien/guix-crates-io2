(define-module (crates-io ms sf mssf-pal) #:use-module (crates-io))

(define-public crate-mssf-pal-0.0.1 (c (n "mssf-pal") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "19gw0r1ykw1znqyf81bd2j58jblnd6fmmb8fqys50p29wjh258mp")))

(define-public crate-mssf-pal-0.0.4 (c (n "mssf-pal") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows") (r "^0.56") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "0nszf1i3vwycs4ldggkk9hpw6h2g8s616x4v0hfvdda83ns643z5")))

(define-public crate-mssf-pal-0.0.5 (c (n "mssf-pal") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows") (r "^0.56") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "0wdr3ngj4xghz6i5frp89zgphfii9d3mna15hsk85q1fwbjwp31q")))

(define-public crate-mssf-pal-0.0.6 (c (n "mssf-pal") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows") (r "^0.56") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "03iy13lfrqv3l0hlx7xhdzjhryisl6ccqg31njfwg71c2il85bdc")))

