(define-module (crates-io ms iz msiz_rustc-ap-serialize) #:use-module (crates-io))

(define-public crate-msiz_rustc-ap-serialize-1.0.0 (c (n "msiz_rustc-ap-serialize") (v "1.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "01vs12h63s65hsq3nywq7gjzh0yd3ycnzhyjcmp4x96lmqyanmch")))

(define-public crate-msiz_rustc-ap-serialize-2.0.0 (c (n "msiz_rustc-ap-serialize") (v "2.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1wdnh2x6yx15ma4b6jjrmhl3l1j0dmmswhqn0449ng4qg8jd7csp")))

(define-public crate-msiz_rustc-ap-serialize-3.0.0 (c (n "msiz_rustc-ap-serialize") (v "3.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "02gjllxz5fv100ki6yhlxdsc9iwci5jhhaqv5bqdlapiza24289n")))

