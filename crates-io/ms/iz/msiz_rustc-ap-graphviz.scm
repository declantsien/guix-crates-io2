(define-module (crates-io ms iz msiz_rustc-ap-graphviz) #:use-module (crates-io))

(define-public crate-msiz_rustc-ap-graphviz-1.0.0 (c (n "msiz_rustc-ap-graphviz") (v "1.0.0") (h "0w6j41k3gasjy3w4k520115bkvi1vxbcyjq0r32qyl4lc22vlk1s")))

(define-public crate-msiz_rustc-ap-graphviz-2.0.0 (c (n "msiz_rustc-ap-graphviz") (v "2.0.0") (h "0sgvbynybi8yl64yxmyq1cvwjbvqyn37wblp8lxn9w4rkmzrilm9")))

(define-public crate-msiz_rustc-ap-graphviz-3.0.0 (c (n "msiz_rustc-ap-graphviz") (v "3.0.0") (h "0wlim0kj5ihd56b0d53ssc9ish31q9mksqydqyanyz3c2lpq1vda")))

