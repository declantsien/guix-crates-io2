(define-module (crates-io ms iz msiz_rustc-ap-arena) #:use-module (crates-io))

(define-public crate-msiz_rustc-ap-arena-1.0.0 (c (n "msiz_rustc-ap-arena") (v "1.0.0") (d (list (d (n "rustc_data_structures") (r "^1.0.0") (d #t) (k 0) (p "msiz_rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0fm9qpc8pv6bs4g15zqcjag7q491g364d7ign7x0xsdbdf7jm172")))

(define-public crate-msiz_rustc-ap-arena-2.0.0 (c (n "msiz_rustc-ap-arena") (v "2.0.0") (d (list (d (n "rustc_data_structures") (r "^2.0.0") (d #t) (k 0) (p "msiz_rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0cykybiil4hygjinxsgzr1bl23z4cmvh540206l6l6wq11lzjiar")))

(define-public crate-msiz_rustc-ap-arena-3.0.0 (c (n "msiz_rustc-ap-arena") (v "3.0.0") (d (list (d (n "rustc_data_structures") (r "^3.0.0") (d #t) (k 0) (p "msiz_rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0rlhx8c1x6d14pizah8gx20x8l3h4skdp040zhqxnffqm9w7q4xn")))

