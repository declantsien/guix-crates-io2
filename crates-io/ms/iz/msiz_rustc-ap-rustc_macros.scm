(define-module (crates-io ms iz msiz_rustc-ap-rustc_macros) #:use-module (crates-io))

(define-public crate-msiz_rustc-ap-rustc_macros-1.0.0 (c (n "msiz_rustc-ap-rustc_macros") (v "1.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0dqd49j44h170acfak78m3180mqx0cg54dpgz8c5gd6d1hq30fpl")))

(define-public crate-msiz_rustc-ap-rustc_macros-2.0.0 (c (n "msiz_rustc-ap-rustc_macros") (v "2.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "00mzcs5nwyvh3545i0apaqyh59gkjvvny36zw64pcr3ag6jj26ph")))

(define-public crate-msiz_rustc-ap-rustc_macros-3.0.0 (c (n "msiz_rustc-ap-rustc_macros") (v "3.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0zg5vmmm5k6vjkgl7xd949xz7jlvn4zm615z7s8gil0s1r7iyzb3")))

