(define-module (crates-io ms iz msiz_rustc-ap-rustc_lexer) #:use-module (crates-io))

(define-public crate-msiz_rustc-ap-rustc_lexer-1.0.0 (c (n "msiz_rustc-ap-rustc_lexer") (v "1.0.0") (d (list (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0xcwhv0fj48j31i47crb6450kkz3915rzhpw2arhw97qqfzm20q4")))

(define-public crate-msiz_rustc-ap-rustc_lexer-2.0.0 (c (n "msiz_rustc-ap-rustc_lexer") (v "2.0.0") (d (list (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0gindap9ish9vc21cbqv5xxr0kv5nnq2hdx0gisnfqv3wcn9nl8c")))

(define-public crate-msiz_rustc-ap-rustc_lexer-3.0.0 (c (n "msiz_rustc-ap-rustc_lexer") (v "3.0.0") (d (list (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "15prnyr5bbqlbsy5slk3nz5is7sxn7qn0vyk76758c4a3avba6ch")))

