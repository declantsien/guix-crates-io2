(define-module (crates-io ms vc msvc_spectre_libs) #:use-module (crates-io))

(define-public crate-msvc_spectre_libs-0.1.0 (c (n "msvc_spectre_libs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1xn9pg6f39d39fi92k6ffs30q1zh8mbv1hgjdq8x4jzj69yzx6fb") (f (quote (("error"))))))

(define-public crate-msvc_spectre_libs-0.1.1 (c (n "msvc_spectre_libs") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(all(target_os = \"windows\", target_env = \"msvc\"))") (k 1)))) (h "1cdvpwlc1q8rlqsma5jxhjlilg8cfs3hcalbzw2rhs4b88ldjcb4") (f (quote (("error"))))))

