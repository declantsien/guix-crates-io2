(define-module (crates-io ms vc msvc-demangler) #:use-module (crates-io))

(define-public crate-msvc-demangler-0.1.0 (c (n "msvc-demangler") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)))) (h "1vlg8x4yxi3y40s9pmbhiyh2y3i7ldyh6q00lwd5vwgn9yk6l0dv")))

(define-public crate-msvc-demangler-0.2.0 (c (n "msvc-demangler") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)))) (h "199q44v0x503hkv35in54kilh17knnb1rm4wy4pvwbd6kc3w096c")))

(define-public crate-msvc-demangler-0.3.0 (c (n "msvc-demangler") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)))) (h "1b5v6jabfvsz2a4ax22yshnd9rh12csp9fdvig88vvl4pazn6lxx")))

(define-public crate-msvc-demangler-0.3.1 (c (n "msvc-demangler") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)))) (h "1n8ji0dc30g8fxg5icn830rwi8wiv6azj37mw87j3a40j0xgqx08")))

(define-public crate-msvc-demangler-0.3.2 (c (n "msvc-demangler") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)))) (h "1qk6vfb6hi1hj5x5ajvgqf5zwk5lna0ck1wdw8s4b4gf86rq2rzg")))

(define-public crate-msvc-demangler-0.4.0 (c (n "msvc-demangler") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)))) (h "07da296jngcv16b9hqyd3w4qyd28c78c0fw0inhsky2q8zr3r4sr")))

(define-public crate-msvc-demangler-0.5.0 (c (n "msvc-demangler") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)))) (h "1avam6df0gsysyjg4ww7spfy95l67wj26rwv0w180yjdm5wwqf78")))

(define-public crate-msvc-demangler-0.6.0 (c (n "msvc-demangler") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)))) (h "0r21gyb16abbpjfpay9pps5mmxk7mjzrbhr2xlnpzqq1nh00zfn7")))

(define-public crate-msvc-demangler-0.7.0 (c (n "msvc-demangler") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)))) (h "14wgakm2rywx1vnrfyrilbbsvpgmg9gyj6nsfzb9gc7jhc9428wz")))

(define-public crate-msvc-demangler-0.8.0 (c (n "msvc-demangler") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)))) (h "0q602z4zrhqra4fgxps78gn7d9x9jp9nq48a8ab07z025w9cz135")))

(define-public crate-msvc-demangler-0.9.0 (c (n "msvc-demangler") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)))) (h "1j7kkmbd9yvhk7dmvd29pqcg9mcjdw5p1ia1kihh16zss1nprdmz")))

(define-public crate-msvc-demangler-0.10.0 (c (n "msvc-demangler") (v "0.10.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)))) (h "1czx6fzw00b0zngp0g0hl6967z23rk529y117gbzpyx7wf1ck215")))

(define-public crate-msvc-demangler-0.10.1 (c (n "msvc-demangler") (v "0.10.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)))) (h "1z55341d1lrvy30r2n821z9001xd84qjz0mlxbnfi06qnwxmmhn4")))

