(define-module (crates-io ms vc msvc-alloca) #:use-module (crates-io))

(define-public crate-msvc-alloca-0.1.0 (c (n "msvc-alloca") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1197g8vw5ix9wn5qbq3fdvz7sc95y7v60lxrr5zzw88ndm52j49s")))

(define-public crate-msvc-alloca-0.2.0 (c (n "msvc-alloca") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0af4xjafsa5168v8vhmp2mjskffngjgyp653s78y05z9bssd1lkz")))

(define-public crate-msvc-alloca-0.3.0 (c (n "msvc-alloca") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 1)))) (h "16qs1wqkazhx0vr3bzdc31xp2j41xilmwj8hdmx6nj1izxha8dzh")))

