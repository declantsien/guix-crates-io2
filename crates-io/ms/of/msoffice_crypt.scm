(define-module (crates-io ms of msoffice_crypt) #:use-module (crates-io))

(define-public crate-msoffice_crypt-0.1.0 (c (n "msoffice_crypt") (v "0.1.0") (h "0kcj2lf1qwpd4qpim9a8qw70m4v40qdlr2n38j3pglqmqczp698h")))

(define-public crate-msoffice_crypt-0.1.1 (c (n "msoffice_crypt") (v "0.1.1") (h "0qh4v8ja4wfyiq8lmh5kk1k3iipl9gdb8m513q4w790scxspr797")))

(define-public crate-msoffice_crypt-0.1.2 (c (n "msoffice_crypt") (v "0.1.2") (h "1470bk5whxbzrd898s9qhhljwm1y5va1zcbp437rjviz4icdk7fl")))

(define-public crate-msoffice_crypt-0.1.3 (c (n "msoffice_crypt") (v "0.1.3") (h "1vqr6lr5q0fabii5p0yqjmkqram4y6bry2ijpryfkvwifialbb4x")))

(define-public crate-msoffice_crypt-0.1.4 (c (n "msoffice_crypt") (v "0.1.4") (h "1p1y9wxwza7wyn8wf23nm07bawhdq570bk60l3a5zzb71asyc919")))

(define-public crate-msoffice_crypt-0.1.5 (c (n "msoffice_crypt") (v "0.1.5") (h "0xbjkv1qs4aizn4qixl68qjzbnwnn5bxairivswpffpnlcmiangp")))

