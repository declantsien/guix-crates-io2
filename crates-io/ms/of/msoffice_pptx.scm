(define-module (crates-io ms of msoffice_pptx) #:use-module (crates-io))

(define-public crate-msoffice_pptx-0.1.0 (c (n "msoffice_pptx") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.13.1") (d #t) (k 0)) (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "0621i6fvlb7q42waj0mprdaywpxib742v3nq8jag8psan5rzrhcd")))

(define-public crate-msoffice_pptx-0.1.1 (c (n "msoffice_pptx") (v "0.1.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)) (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "0i0f28gsp3d00cjg6bqknpl80ynpb6m19ypvwzm2cqv3rmhrirlr")))

(define-public crate-msoffice_pptx-0.2.0 (c (n "msoffice_pptx") (v "0.2.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "msoffice_shared") (r "^0.1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)) (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "09xisv2164y4npbxqxyklwadjcrxwglscqqx885x9nlq0ighv413")))

(define-public crate-msoffice_pptx-0.2.1 (c (n "msoffice_pptx") (v "0.2.1") (d (list (d (n "enum_from_str") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_from_str_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "msoffice_shared") (r "^0.1.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.2.0") (d #t) (k 2)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "1ii8x2ksrwv3vdzk8srl3n9b138sy2msyfdc6wrj18gcy33ih9hl")))

