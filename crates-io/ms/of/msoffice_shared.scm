(define-module (crates-io ms of msoffice_shared) #:use-module (crates-io))

(define-public crate-msoffice_shared-0.1.0 (c (n "msoffice_shared") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)) (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "1fa2k5ak4c81mhkxhz2f7hfal90nd63m58w55h0061kh9fipy8p0")))

(define-public crate-msoffice_shared-0.1.1 (c (n "msoffice_shared") (v "0.1.1") (d (list (d (n "enum_from_str") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_from_str_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "quick-xml") (r "^0.14.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.2.0") (d #t) (k 2)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "0wsiwddnwlzsrhl78wh8k6xym75ch8y0nr75avkxznxgmr7n7agz")))

