(define-module (crates-io ms #{56}# ms5611) #:use-module (crates-io))

(define-public crate-ms5611-1.0.0 (c (n "ms5611") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)))) (h "1l07clrmiwqnyr8h1a13qlhffk7ybnqczishhrxb8a6j940kjjww")))

