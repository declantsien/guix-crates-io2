(define-module (crates-io ms #{56}# ms5637) #:use-module (crates-io))

(define-public crate-ms5637-0.1.0 (c (n "ms5637") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0ib73v7i2pfk8m9gry46961q9lwrpkzxzzvcznw73rznqhm03ciz") (f (quote (("second-order")))) (s 2) (e (quote (("altitude-adjust" "dep:libm")))) (r "1.60")))

