(define-module (crates-io ms #{56}# ms5611-spi) #:use-module (crates-io))

(define-public crate-ms5611-spi-0.1.0 (c (n "ms5611-spi") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.5") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)))) (h "1fhkzz1f62h2nh78wr1hxjk1zfvn702h8z1s02xxczh336aqzrfy")))

(define-public crate-ms5611-spi-0.2.0 (c (n "ms5611-spi") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.5") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)))) (h "15wxqf365iyspwjlbgbz1grb5sv4z53m4md2vnnryxz47pgpqfg4") (f (quote (("ms5611") ("ms5607") ("default" "ms5611"))))))

