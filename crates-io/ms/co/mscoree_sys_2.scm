(define-module (crates-io ms co mscoree_sys_2) #:use-module (crates-io))

(define-public crate-mscoree_sys_2-0.1.0 (c (n "mscoree_sys_2") (v "0.1.0") (d (list (d (n "mscorlib-sys") (r "^0.1.11") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("combaseapi" "hstring" "oaidl" "oleauto" "processthreadsapi" "winerror"))) (d #t) (k 0)) (d (n "winreg") (r "^0.6") (d #t) (k 1)))) (h "12ck1y7px0pdmvx72mzids4diqdsyzwi99d6iv1fw7qshqmaphcb")))

