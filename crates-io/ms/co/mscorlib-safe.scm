(define-module (crates-io ms co mscorlib-safe) #:use-module (crates-io))

(define-public crate-mscorlib-safe-0.1.0 (c (n "mscorlib-safe") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mscorlib-sys") (r "^0.1.8") (d #t) (k 0)) (d (n "mscorlib_safe_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("winerror" "oaidl" "oleauto"))) (d #t) (k 0)))) (h "10y6fsclq4hfa3dm0w1z58mll5f65hc3rs7xg8lp0hfhiw2dyz0p")))

(define-public crate-mscorlib-safe-0.1.1 (c (n "mscorlib-safe") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mscorlib-sys") (r "^0.1.9") (d #t) (k 0)) (d (n "mscorlib_safe_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("winerror" "oaidl" "oleauto"))) (d #t) (k 0)))) (h "0mfwk9pmzwg1dk31y0x1cfxz196cdvphjrdij9xnb4bw6ax2p06k")))

(define-public crate-mscorlib-safe-0.1.2 (c (n "mscorlib-safe") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mscorlib-sys") (r "^0.1.9") (d #t) (k 0)) (d (n "mscorlib_safe_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("winerror" "oaidl" "oleauto"))) (d #t) (k 0)))) (h "03i0qmki81h4848r1hvsramc62kph3wbnrw0fck4njyz9gr73bm8")))

(define-public crate-mscorlib-safe-0.1.3 (c (n "mscorlib-safe") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "mscorlib-sys") (r "^0.1.10") (d #t) (k 0)) (d (n "mscorlib_safe_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("winerror" "oaidl" "oleauto"))) (d #t) (k 0)))) (h "0fii9ljq0vh3ssdq3czrygvq48l2gpqzvwddwjmw2p7hhbm4fsqz")))

