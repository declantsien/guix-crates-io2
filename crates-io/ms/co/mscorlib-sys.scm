(define-module (crates-io ms co mscorlib-sys) #:use-module (crates-io))

(define-public crate-mscorlib-sys-0.1.0 (c (n "mscorlib-sys") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.5") (f (quote ("oaidl" "winerror"))) (d #t) (k 0)))) (h "18np5ackf1r68hhl7552j5rzhqccw6xmhzcdy3mndbvraihafggk")))

(define-public crate-mscorlib-sys-0.1.1 (c (n "mscorlib-sys") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.5") (f (quote ("oaidl" "winerror"))) (d #t) (k 0)))) (h "0p9ljcqis792zj646gx2szgw75ghkkpl54cca3j81hrysigac5dp")))

(define-public crate-mscorlib-sys-0.1.2 (c (n "mscorlib-sys") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.5") (f (quote ("oaidl" "winerror"))) (d #t) (k 0)))) (h "1nvhai5qw94wx1f0vkapklfi5cb3glx3ydjsdwbnyq9f5lpz06dv")))

(define-public crate-mscorlib-sys-0.1.3 (c (n "mscorlib-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3.5") (f (quote ("combaseapi" "oaidl" "winerror"))) (d #t) (k 0)))) (h "0m9b7m5s24a8mjdixxkn9623myyn2p7gw8p6d2qkam84kah5s773")))

(define-public crate-mscorlib-sys-0.1.4 (c (n "mscorlib-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3.5") (f (quote ("combaseapi" "oaidl" "winerror"))) (d #t) (k 0)))) (h "0mq8nknhkk2c1awxkdnx3ayqdz6p5xv9xhhh578asnc235ywhd7v")))

(define-public crate-mscorlib-sys-0.1.5 (c (n "mscorlib-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3.5") (f (quote ("combaseapi" "oaidl" "winerror"))) (d #t) (k 0)))) (h "1r61z47di3hdgvdfcvi2bsafw7amwpqmswnczn6lzl48viyp72b7")))

(define-public crate-mscorlib-sys-0.1.6 (c (n "mscorlib-sys") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3.5") (f (quote ("combaseapi" "oaidl" "winerror"))) (d #t) (k 0)))) (h "0xh7a4lnmrnh6746h3ipq47p98d450cdl6795fm01xff6q48ainq")))

(define-public crate-mscorlib-sys-0.1.7 (c (n "mscorlib-sys") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3.5") (f (quote ("combaseapi" "oaidl" "winerror"))) (d #t) (k 0)))) (h "0s7ymbg4dy1hvia4sijplvx9kvkrskk2qy8gzxaqd0w2438gccg6")))

(define-public crate-mscorlib-sys-0.1.8 (c (n "mscorlib-sys") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3.5") (f (quote ("combaseapi" "oaidl" "winerror"))) (d #t) (k 0)))) (h "1w6da8g1np5q4pbyisb8cgsniv85rba0zzq5q5a79zz103gq32qs")))

(define-public crate-mscorlib-sys-0.1.9 (c (n "mscorlib-sys") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3.5") (f (quote ("combaseapi" "oaidl" "winerror"))) (d #t) (k 0)))) (h "0p9l5km4z3iynlypapqb19gbidl6hg1zg542vj6yg8rya2rnyvai")))

(define-public crate-mscorlib-sys-0.1.10 (c (n "mscorlib-sys") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3.5") (f (quote ("combaseapi" "oaidl" "winerror"))) (d #t) (k 0)))) (h "0nfz34fwzr2sp7hv60cj0b5csc6jv7s4v7c20v2my12akp78p4a1")))

(define-public crate-mscorlib-sys-0.1.11 (c (n "mscorlib-sys") (v "0.1.11") (d (list (d (n "cc") (r "^1.0.29") (d #t) (k 1)) (d (n "winapi") (r "^0.3.6") (f (quote ("combaseapi" "oaidl" "winerror"))) (d #t) (k 0)) (d (n "winreg") (r "^0.6") (d #t) (k 1)))) (h "1yf3p3pkncky48vinlbflgsa5hz2gfv9v40hhibq451zbmljgykf")))

