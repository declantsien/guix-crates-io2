(define-module (crates-io ms co mscorlib_safe_derive) #:use-module (crates-io))

(define-public crate-mscorlib_safe_derive-0.1.0 (c (n "mscorlib_safe_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (d #t) (k 0)))) (h "1av8dr17zkg1hmj0mv78nap1gqrhdp4raa81nb6gz4gynqdp5qaj")))

(define-public crate-mscorlib_safe_derive-0.1.1 (c (n "mscorlib_safe_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (d #t) (k 0)))) (h "0j0dp629qm9dprw64c88vpx023d20lq3h52di3251n30xmbqvvcw")))

(define-public crate-mscorlib_safe_derive-0.1.2 (c (n "mscorlib_safe_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (d #t) (k 0)))) (h "11393x4ymhxji5hn7a5d8n4kx4nqiydkx7pwpnqdwi7pkml1xapf")))

(define-public crate-mscorlib_safe_derive-0.1.3 (c (n "mscorlib_safe_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (d #t) (k 0)))) (h "127p4k6bsvrldgfa6xkzg43gs2rk7s22szrvp7mar1m9lp4wy9hb")))

