(define-module (crates-io ms co mscout) #:use-module (crates-io))

(define-public crate-mscout-0.3.0 (c (n "mscout") (v "0.3.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "id3") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minijinja") (r "^1.0.4") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "pbr") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)))) (h "11v1b75sa7mprfc13cnv80d2fxpa0ccbsa880wlazh52kpl088si")))

