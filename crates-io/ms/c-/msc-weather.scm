(define-module (crates-io ms c- msc-weather) #:use-module (crates-io))

(define-public crate-msc-weather-0.6.2 (c (n "msc-weather") (v "0.6.2") (h "1xwibqn0nnk6d3hrg7lm88kh2xd3jh51c29sx8cam9f1hnk1dp3y") (y #t)))

(define-public crate-msc-weather-0.6.3 (c (n "msc-weather") (v "0.6.3") (h "0mffcr5cfq5j3crzlr4rw1sz1bpfzfgd7kdy0ykxpp5alsyq970x") (y #t)))

