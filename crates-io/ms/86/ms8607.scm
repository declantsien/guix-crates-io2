(define-module (crates-io ms #{86}# ms8607) #:use-module (crates-io))

(define-public crate-ms8607-0.1.0 (c (n "ms8607") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0114gzb7rnm60y67kp8q0prbsgmv0knk640z7rdifrs2rvnwvclz")))

(define-public crate-ms8607-0.1.1 (c (n "ms8607") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7.2") (d #t) (k 2)) (d (n "fugit") (r "^0.3.6") (d #t) (k 2)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "rp-pico") (r "^0.6.0") (d #t) (k 2)) (d (n "rp2040-hal") (r "^0.7.0") (d #t) (k 2)))) (h "0hn5ci0dzwmgb7hmsanigbmcz2j3xfs4l8ihx0qq4h2qcszhzs6d")))

(define-public crate-ms8607-0.1.2 (c (n "ms8607") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7.2") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.5.0") (d #t) (k 2)) (d (n "fugit") (r "^0.3.6") (d #t) (k 2)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "rp-pico") (r "^0.6.0") (d #t) (k 2)) (d (n "rp2040-hal") (r "^0.7.0") (d #t) (k 2)) (d (n "shared-bus") (r "^0.2.5") (d #t) (k 2)))) (h "01zrn86w94zbydb0n0a5fknlwix0dvbsg11ncg32daj50i07rh3r")))

