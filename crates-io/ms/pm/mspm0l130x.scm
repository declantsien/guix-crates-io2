(define-module (crates-io ms pm mspm0l130x) #:use-module (crates-io))

(define-public crate-mspm0l130x-0.1.0 (c (n "mspm0l130x") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "03vr2d24iarp19zxr2nababvync13xa8jkcqssxsl067cn0dif3h") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-mspm0l130x-0.1.1 (c (n "mspm0l130x") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1xi5vkpja2xjaildvnd2mx2vlimh6l5mbzz62xmlgkl416rqcfky") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-mspm0l130x-0.1.2 (c (n "mspm0l130x") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0mcwzydswcjkfwyjr5i0np6g7z1gafmz3mifa4d53h4hidnh2x85") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-mspm0l130x-0.1.3 (c (n "mspm0l130x") (v "0.1.3") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0bmsh37vjjk926jfm7zqlbzahsjcfl2frwf4f69mb4bi9qhj0p0p") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-mspm0l130x-0.1.4 (c (n "mspm0l130x") (v "0.1.4") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "19zv42zsycv7c3nxf7kagcxyw432dzajwyxbmy7rvfjh31vk7rsz") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-mspm0l130x-0.1.5 (c (n "mspm0l130x") (v "0.1.5") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1vaznj8adb6y4zfnxsqf9aj20709154gibcl9lzhvr541z9icx31") (f (quote (("rt" "cortex-m-rt/device"))))))

