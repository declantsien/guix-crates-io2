(define-module (crates-io ms o5 mso5k_dumpfb) #:use-module (crates-io))

(define-public crate-mso5k_dumpfb-0.1.0 (c (n "mso5k_dumpfb") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "1fikggiigj5l74hd2fycwd9cfpv7g4walcz2lqqdidxxw4psmkyv")))

