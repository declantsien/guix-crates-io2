(define-module (crates-io ms f6 msf60_utils) #:use-module (crates-io))

(define-public crate-msf60_utils-0.3.1 (c (n "msf60_utils") (v "0.3.1") (d (list (d (n "radio_datetime_utils") (r "^0.5.0") (d #t) (k 0)))) (h "0aw67gl2l04mbyaf0jq15qkdc875pp0aarm8a4qcsr65v0hqc7rr")))

(define-public crate-msf60_utils-0.3.2 (c (n "msf60_utils") (v "0.3.2") (d (list (d (n "radio_datetime_utils") (r "^0.5.0") (d #t) (k 0)))) (h "0vza8pzk5nys1vbmq0pqn89dv9rv1zrqb4wscb8g45gmxlpcgnaj")))

(define-public crate-msf60_utils-0.4.0 (c (n "msf60_utils") (v "0.4.0") (d (list (d (n "radio_datetime_utils") (r "^0.5") (d #t) (k 0)))) (h "0l1wgvq2bnkl5w8b68wvm5ni9cr7jnpmy5a6wxf4vvzfjrnj75zb")))

