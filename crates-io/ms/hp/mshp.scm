(define-module (crates-io ms hp mshp) #:use-module (crates-io))

(define-public crate-mshp-0.1.0 (c (n "mshp") (v "0.1.0") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "1476088rs6g04wzmapgwl2gzgbm1qy0292253k38vh2hcl3s99r8")))

(define-public crate-mshp-0.2.0 (c (n "mshp") (v "0.2.0") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0p24gxm69jznb6vv20vrf4sqhh0ri7pip97vx80hm7kwdcyz79d3")))

