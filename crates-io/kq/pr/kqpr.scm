(define-module (crates-io kq pr kqpr) #:use-module (crates-io))

(define-public crate-kqpr-0.1.0 (c (n "kqpr") (v "0.1.0") (d (list (d (n "gdk") (r "0.14.*") (d #t) (k 0)) (d (n "glib") (r "0.14.*") (d #t) (k 0)) (d (n "gtk") (r "0.14.*") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "keepass") (r "^0.4.9") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)))) (h "1g672mcagi94p583d18l2axn99632saqh90n3xh1677w28s06nq0")))

