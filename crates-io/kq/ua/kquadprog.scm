(define-module (crates-io kq ua kquadprog) #:use-module (crates-io))

(define-public crate-kquadprog-0.1.0 (c (n "kquadprog") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.13") (f (quote ("openblas"))) (d #t) (k 0)))) (h "19lcqh5v9ipnadz7bsg9hi3gx5f7920c4mqy6sffg28vsznl10pd")))

