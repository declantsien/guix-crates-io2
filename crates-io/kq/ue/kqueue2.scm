(define-module (crates-io kq ue kqueue2) #:use-module (crates-io))

(define-public crate-kqueue2-0.2.2 (c (n "kqueue2") (v "0.2.2") (d (list (d (n "kqueue2-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06z9r8nq34bmv5lbgi9ilj0np1fqqnwjqwmmzk5iwd828irc4nm2")))

