(define-module (crates-io kq ue kqueue2-sys) #:use-module (crates-io))

(define-public crate-kqueue2-sys-0.1.4 (c (n "kqueue2-sys") (v "0.1.4") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jh1wpc8sqr5cr6xswbmi9wqwk4mh5n6aj1dg5mi1i0rpcnm3c51")))

(define-public crate-kqueue2-sys-0.1.5 (c (n "kqueue2-sys") (v "0.1.5") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lp7i891z7vaj5x0qzmg8cdsbxp6vi92djp1nvr0wi6jh2l4cxmh")))

