(define-module (crates-io uy ma uymas_cli) #:use-module (crates-io))

(define-public crate-uymas_cli-2.0.0 (c (n "uymas_cli") (v "2.0.0") (h "0k3xf46p9sz83vy5hj4x6a1r8rrn85r8w7nniypz7sfll67x2f0q")))

(define-public crate-uymas_cli-2.0.1 (c (n "uymas_cli") (v "2.0.1") (h "18rblbmcbsxaay6f8avq318i0jb4d2xml5ik52c7c7fa4rbl94n8")))

(define-public crate-uymas_cli-2.0.2 (c (n "uymas_cli") (v "2.0.2") (h "1dq786hfb0iz484w7bs438h7vnvhs97n1nbkwlawd08cl9q18flx")))

(define-public crate-uymas_cli-2.1.0 (c (n "uymas_cli") (v "2.1.0") (h "1vi81yhzd6s7ngj7ls9nabd6m1ar10nk57cpsi5fqajl9p70w14l")))

(define-public crate-uymas_cli-2.2.0 (c (n "uymas_cli") (v "2.2.0") (h "02ynsasdbva7miangnvprh3rgpbhwzvqzrnka2g3qgjrd34wacrl")))

