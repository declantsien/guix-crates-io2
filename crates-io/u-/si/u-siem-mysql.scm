(define-module (crates-io u- si u-siem-mysql) #:use-module (crates-io))

(define-public crate-u-siem-mysql-0.0.2 (c (n "u-siem-mysql") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "0m164v3jjsjwh4aiw6y5ji7ny551q1p33sggv2irsk5g4i53fmyp")))

(define-public crate-u-siem-mysql-0.0.3 (c (n "u-siem-mysql") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "0vzn5bmj3r9641lixpzzrm818bfrsiwllkkk8b2v4c3ly8l8ggdc")))

