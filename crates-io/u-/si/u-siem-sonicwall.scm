(define-module (crates-io u- si u-siem-sonicwall) #:use-module (crates-io))

(define-public crate-u-siem-sonicwall-0.0.5 (c (n "u-siem-sonicwall") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "1w1gmp6crlnp3d3q1xn5f11gxid2zndvqqy8q4zli73zq4g1xl1p")))

(define-public crate-u-siem-sonicwall-0.0.6 (c (n "u-siem-sonicwall") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "187k01zryx7zhszih5yxj2dqc7hg82szp85m1ip6ch6l1pmiklw4")))

(define-public crate-u-siem-sonicwall-0.0.9 (c (n "u-siem-sonicwall") (v "0.0.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "1305f8w5dh4wf5rv1d02vawhhb7dznxyv02gbh8vld94n2bpzylf")))

(define-public crate-u-siem-sonicwall-0.0.10 (c (n "u-siem-sonicwall") (v "0.0.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "11apd70d5n6qjxnphv8v1asq3kg208kx6h5gk2jmyssc5mddhc5m")))

