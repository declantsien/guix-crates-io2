(define-module (crates-io u- si u-siem-enricher) #:use-module (crates-io))

(define-public crate-u-siem-enricher-0.0.1 (c (n "u-siem-enricher") (v "0.0.1") (d (list (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "0iq55rzfylhhv0balax4cpzpmqya3rf9w5bxvydhxzmz4fjydsv0")))

(define-public crate-u-siem-enricher-0.1.0 (c (n "u-siem-enricher") (v "0.1.0") (d (list (d (n "u-siem") (r "^0") (d #t) (k 0)))) (h "0l675a02397wmwakw9q6wgzrqc6r8bbdy4szfb1v9jzvqmcmv3d3")))

(define-public crate-u-siem-enricher-0.2.0 (c (n "u-siem-enricher") (v "0.2.0") (d (list (d (n "coarsetime") (r "^0.1.23") (d #t) (k 0)) (d (n "u-siem") (r "^0") (d #t) (k 0)))) (h "1ixywyhnbwcf5nv1cgmifkn303ydy69x6qy4qzipd3hy0s772ah9") (f (quote (("metrics") ("default" "metrics"))))))

