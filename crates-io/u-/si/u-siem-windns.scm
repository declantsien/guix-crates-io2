(define-module (crates-io u- si u-siem-windns) #:use-module (crates-io))

(define-public crate-u-siem-windns-0.0.1 (c (n "u-siem-windns") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "0cwnj52np7ylj3caxkkbillm3i8pj29wh5a20qxgmd0jb4wcphvp")))

(define-public crate-u-siem-windns-0.0.2 (c (n "u-siem-windns") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "0v92a9qrw6s0z33hwa4ciqfrnpn0ba8dgmvi603pfn481cbsnnri")))

(define-public crate-u-siem-windns-0.0.4 (c (n "u-siem-windns") (v "0.0.4") (d (list (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "03fvpkx6rx2c14v13crfknfpiq98qyl3wihlidmj9zkkkpfs77hc")))

(define-public crate-u-siem-windns-0.0.5 (c (n "u-siem-windns") (v "0.0.5") (d (list (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "0b4x1cak8mym9fl6pdg3b13hinf85jqdjpaa2f96anzmpbqf8f5r")))

