(define-module (crates-io u- si u-siem-syslog) #:use-module (crates-io))

(define-public crate-u-siem-syslog-0.0.1 (c (n "u-siem-syslog") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.18") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "1y5scz1b2353z8iyyppgs0wicqk93rp1wywmiyalb2j42g0002kb")))

(define-public crate-u-siem-syslog-0.0.3 (c (n "u-siem-syslog") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.18") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "12wrls86ycs2g2pyz4ld2fcmknqfvm6krngk57jryipxgs6w76ih")))

(define-public crate-u-siem-syslog-0.1.1 (c (n "u-siem-syslog") (v "0.1.1") (d (list (d (n "coarsetime") (r "^0.1.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0") (d #t) (k 0)))) (h "0880136k2df8910llsgqpcjgc573yg1czir55wk8709kklj5a773")))

(define-public crate-u-siem-syslog-0.2.0 (c (n "u-siem-syslog") (v "0.2.0") (d (list (d (n "coarsetime") (r "^0.1.18") (d #t) (k 0)) (d (n "rustls") (r "^0.22.2") (f (quote ("ring" "tls12"))) (o #t) (k 0)) (d (n "rustls-pemfile") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "u-siem") (r "^0") (d #t) (k 0)))) (h "1ibpfjg1vb4177mjgmxnm1q6fkj52cw9l5bf348j3waj40jrydhg") (f (quote (("metrics") ("default" "metrics" "tls")))) (s 2) (e (quote (("tls" "dep:rustls" "dep:rustls-pemfile"))))))

