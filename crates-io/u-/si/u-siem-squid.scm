(define-module (crates-io u- si u-siem-squid) #:use-module (crates-io))

(define-public crate-u-siem-squid-0.0.1 (c (n "u-siem-squid") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.18") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "05m715vcss57kj5n2lin051y2whgfmddvxqzrxfvq928b5l84fx6")))

