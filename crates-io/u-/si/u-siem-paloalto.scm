(define-module (crates-io u- si u-siem-paloalto) #:use-module (crates-io))

(define-public crate-u-siem-paloalto-0.0.1 (c (n "u-siem-paloalto") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "0xcbrd25l5bbwyj64zmap5h8r15w2vskk7lm8qh8plqpiv2sh4i8")))

(define-public crate-u-siem-paloalto-0.0.2 (c (n "u-siem-paloalto") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "0smhgfg5b8vi43cl136z684218qfij3xapvy2cxrn0b8hrrl3p2j")))

(define-public crate-u-siem-paloalto-0.0.3 (c (n "u-siem-paloalto") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "1m98cli0z0k6adpjwfqj9a4lfr4hlyblkiwb5hhc28x054nfq6zp")))

(define-public crate-u-siem-paloalto-0.0.4 (c (n "u-siem-paloalto") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0.0.70") (d #t) (k 0)))) (h "1inacyriiwqj30x0773n89bkj7csmjjlhhnxv6g0cq3bddp0b774")))

(define-public crate-u-siem-paloalto-0.0.6 (c (n "u-siem-paloalto") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "0zn8zg4yzg3lv7ygv75qr1v4hy740m3plmd77b5pxxl72qff4kcp")))

(define-public crate-u-siem-paloalto-0.0.7 (c (n "u-siem-paloalto") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "0y73zg5fskrjg1rgpscsafx4s671jzn2y785cfxnq74a09jkw9cz")))

