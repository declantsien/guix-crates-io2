(define-module (crates-io u- si u-siem-datasetmanager-sqlite) #:use-module (crates-io))

(define-public crate-u-siem-datasetmanager-sqlite-0.0.2 (c (n "u-siem-datasetmanager-sqlite") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "12vfda8vcsn9y6zzbi3wziwr4wb3lkdwyw981b43cqjimd1ny58y")))

(define-public crate-u-siem-datasetmanager-sqlite-0.0.3 (c (n "u-siem-datasetmanager-sqlite") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "1lcvkaq2wd35w7jybxxp01ri9f4yr6dfipfambli59n9vdyjbg76")))

