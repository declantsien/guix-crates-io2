(define-module (crates-io u- si u-siem-sqlite-store) #:use-module (crates-io))

(define-public crate-u-siem-sqlite-store-0.0.1 (c (n "u-siem-sqlite-store") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "15ivhk4zpynan9cx7m37n8ivipqg9xpsp1gmiil63jh6ymakz9h7")))

(define-public crate-u-siem-sqlite-store-0.0.3 (c (n "u-siem-sqlite-store") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0qyafh4dvlsgi5xdmwiczcw21g1jdyq6rc1kywaf34ccqn50xlwy")))

