(define-module (crates-io u- si u-siem-opnsense) #:use-module (crates-io))

(define-public crate-u-siem-opnsense-0.0.2 (c (n "u-siem-opnsense") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "08zmrxq1kjsx28gybmgbqwp16kdbjg8vwjabpkgrcxhzxq6snv7w")))

(define-public crate-u-siem-opnsense-0.0.3 (c (n "u-siem-opnsense") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "1pjj7sa74z3sskvrjc0004fcg299i836blc0wny4m71f7z7qfwv7")))

(define-public crate-u-siem-opnsense-0.0.4 (c (n "u-siem-opnsense") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "u-siem") (r "^0.0") (d #t) (k 0)))) (h "16rh3hx4j680m01z3pr72xdaa2sjfvnwkvyn53ikz5dphdjdgphs")))

