(define-module (crates-io u- nu u-num-it) #:use-module (crates-io))

(define-public crate-u-num-it-0.1.0 (c (n "u-num-it") (v "0.1.0") (d (list (d (n "eval") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full"))) (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 2)))) (h "18xxwfvbvhhrj7vhpl70q3qf5039ppvvy9kkzjsgxc7cqn786w6s") (r "1.67")))

(define-public crate-u-num-it-0.1.1 (c (n "u-num-it") (v "0.1.1") (d (list (d (n "eval") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full"))) (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 2)))) (h "1yn0q01inlhzz649yw3hsnlhvihjykqal56wl6x8ydcjwhs37bnv") (r "1.67")))

(define-public crate-u-num-it-0.1.2 (c (n "u-num-it") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full"))) (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 2)))) (h "05v6y003bqpxi40wxfi829q78wqhx0fjndi966y0pj4nl33wcwam") (r "1.67")))

