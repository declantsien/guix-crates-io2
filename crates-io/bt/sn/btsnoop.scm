(define-module (crates-io bt sn btsnoop) #:use-module (crates-io))

(define-public crate-btsnoop-0.1.0 (c (n "btsnoop") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom-derive") (r "^0.10.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0xxd8ayxw0hvknkq30w4g6vvczl7ihdghl68ylfsv3hyljy8g70x")))

(define-public crate-btsnoop-0.2.0 (c (n "btsnoop") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom-derive") (r "^0.10.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0n7rs9w4ar6a675q5wy64b26wzcilqhn02p5l2f3fasjl8ikhqcz")))

(define-public crate-btsnoop-0.2.1 (c (n "btsnoop") (v "0.2.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom-derive") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0ghlf0q41j171z91ml661n3028wn6ky3rb7kc0fwhd3xirg100dh")))

