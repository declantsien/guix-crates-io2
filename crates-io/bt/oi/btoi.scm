(define-module (crates-io bt oi btoi) #:use-module (crates-io))

(define-public crate-btoi-0.1.0 (c (n "btoi") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "1hx38cxp99jicis7wydvkk55h4fasfdqri75b04271vkfy84w1hl")))

(define-public crate-btoi-0.1.1 (c (n "btoi") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "1qbsqx76rynmx9b5pl0mazxy17n4w7xphhxgznd26r9pfgwwxcgv")))

(define-public crate-btoi-0.1.2 (c (n "btoi") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)))) (h "1kgcqqic1krng42g1p59hxwily86dhwb339856f9cy60hz3cxii4")))

(define-public crate-btoi-0.1.3 (c (n "btoi") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0ah9379rmdh4gvzjcp1i7agwa58v1ykdr35lyy4yvdsqdcnipa3x")))

(define-public crate-btoi-0.2.0 (c (n "btoi") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0p88d7nmzv49lblikcjgd9ivkhpngppgd8xz6ip63a0xcbn4x6nw")))

(define-public crate-btoi-0.3.0 (c (n "btoi") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "16c3v220x6fnj0czss923vxyrb21j4kl2q2jxqnbdz4ya3zfq5cd") (f (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-btoi-0.4.0 (c (n "btoi") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0n3n93nm2qcdspl7f4p7x070ws1gj7jnc4w1vcjdwsd3q4q8cxk3") (f (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-btoi-0.4.1 (c (n "btoi") (v "0.4.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "0w8lhvwjpz3liza1hwsr6dhzl22qfgnkvm88s19ybnbhbqhdikp4") (f (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-btoi-0.4.2 (c (n "btoi") (v "0.4.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "04ba4j96icaan10c613s2rwpn2kdbl8728qhz2xzi0dakyd8dh4p") (f (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-btoi-0.4.3 (c (n "btoi") (v "0.4.3") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1bg02zgsv5njbhng9sq2b70przbazsczjbla5lbbdf59fdzl1mlx") (f (quote (("std" "num-traits/std") ("default" "std"))))))

