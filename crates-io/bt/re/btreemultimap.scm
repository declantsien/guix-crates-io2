(define-module (crates-io bt re btreemultimap) #:use-module (crates-io))

(define-public crate-btreemultimap-0.1.0 (c (n "btreemultimap") (v "0.1.0") (d (list (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "~1.0") (d #t) (k 2)))) (h "1jlp3w2qvq6hbrrr9mgm11dnwyaz5ri5jn3lbs8x8zal33mfm9xb") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

(define-public crate-btreemultimap-0.1.1 (c (n "btreemultimap") (v "0.1.1") (d (list (d (n "serde") (r "~1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "~1.0") (d #t) (k 2)))) (h "0islmba70a68v0cg3lm64cfc843cz7anvcjg11r1vkxvch2f91k7") (f (quote (("serde_impl" "serde") ("default" "serde_impl"))))))

