(define-module (crates-io bt re btree-range-map) #:use-module (crates-io))

(define-public crate-btree-range-map-0.1.0 (c (n "btree-range-map") (v "0.1.0") (d (list (d (n "btree-slab") (r "^0.4.0") (d #t) (k 0)) (d (n "cc-traits") (r "^0.8.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "059x49kzr3s2nbhwnpm5syx180ygnd6pd4awcd73yqndkyfgyihf")))

(define-public crate-btree-range-map-0.2.0 (c (n "btree-range-map") (v "0.2.0") (d (list (d (n "btree-slab") (r "^0.4.0") (d #t) (k 0)) (d (n "cc-traits") (r "^0.8.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "149198615fmbn22669y8vafp81x2lc912rcbmrfg0dvf11j0237k")))

(define-public crate-btree-range-map-0.2.1 (c (n "btree-range-map") (v "0.2.1") (d (list (d (n "btree-slab") (r "^0.4.0") (d #t) (k 0)) (d (n "cc-traits") (r "^0.8.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "0jc0six6gk20dr9n5pi679913psmpc1k9r0h84nzs17azg63k6w4")))

(define-public crate-btree-range-map-0.2.2 (c (n "btree-range-map") (v "0.2.2") (d (list (d (n "btree-slab") (r "^0.4.0") (d #t) (k 0)) (d (n "cc-traits") (r "^0.8.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "1kdpbvc7y9y31dx1hn2p8bij1lq4iilngcpzniijhdvlpij9pwfg") (y #t)))

(define-public crate-btree-range-map-0.2.3 (c (n "btree-range-map") (v "0.2.3") (d (list (d (n "btree-slab") (r "^0.4.0") (d #t) (k 0)) (d (n "cc-traits") (r "^0.8.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "1jmwjy5c2xbykfzxs03r6b4adrml3byrb7jkd6a58v9bcjfdrgcd") (y #t)))

(define-public crate-btree-range-map-0.2.4 (c (n "btree-range-map") (v "0.2.4") (d (list (d (n "btree-slab") (r "^0.4.0") (d #t) (k 0)) (d (n "cc-traits") (r "^0.8.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "0zgyjv2zs2nn0b9a5d66r1girx5zdzl3fhhp40wd0613mkzgfx8k") (y #t)))

(define-public crate-btree-range-map-0.2.5 (c (n "btree-range-map") (v "0.2.5") (d (list (d (n "btree-slab") (r "^0.4.0") (d #t) (k 0)) (d (n "cc-traits") (r "^0.8.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "18d8x8v5im4lcj0bgzhcylqy0rw9w05l8v56nzd5hm50rk0zrlsz")))

(define-public crate-btree-range-map-0.2.6 (c (n "btree-range-map") (v "0.2.6") (d (list (d (n "btree-slab") (r "^0.4.0") (d #t) (k 0)) (d (n "cc-traits") (r "^0.8.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "0cpjcr180n10pvpwlp25ml93z62y48vzs6n9av6jb1760idclzhs")))

(define-public crate-btree-range-map-0.3.0 (c (n "btree-range-map") (v "0.3.0") (d (list (d (n "btree-slab") (r "^0.4.0") (d #t) (k 0)) (d (n "cc-traits") (r "^0.8.0") (d #t) (k 0)) (d (n "range-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "1f2arhcjz74969zx3xnlbfvn3lsxkp3cajpgk0rsvs6y5g8bfy2f")))

(define-public crate-btree-range-map-0.5.0 (c (n "btree-range-map") (v "0.5.0") (d (list (d (n "btree-slab") (r "^0.5.1") (d #t) (k 0)) (d (n "cc-traits") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "range-traits") (r "^0.3.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "1r0jrqi06dassw9f1mcmyy82dim1p2ijw6ds1hymi7dgq84s3wyy")))

(define-public crate-btree-range-map-0.5.1 (c (n "btree-range-map") (v "0.5.1") (d (list (d (n "btree-slab") (r "^0.5.1") (d #t) (k 0)) (d (n "cc-traits") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "range-traits") (r "^0.3.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "0szl6gn2xp65xkwkacyg5qmi0cmrbijq3j8l0d0y6lyym8inb4jp")))

(define-public crate-btree-range-map-0.5.2 (c (n "btree-range-map") (v "0.5.2") (d (list (d (n "btree-slab") (r "^0.5.1") (d #t) (k 0)) (d (n "cc-traits") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "range-traits") (r "^0.3.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "0hfcldacazi8pgb0rbfm5mpw3bdqgx6z2sqhy9r96xdf2xdbxkg3")))

(define-public crate-btree-range-map-0.6.0 (c (n "btree-range-map") (v "0.6.0") (d (list (d (n "btree-slab") (r "^0.6") (d #t) (k 0)) (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "range-traits") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "14h24fn8swrl311lyq8flxhb20zd7fbdb2lh3vwclwsy0j2d93nq")))

(define-public crate-btree-range-map-0.7.0 (c (n "btree-range-map") (v "0.7.0") (d (list (d (n "btree-slab") (r "^0.6") (d #t) (k 0)) (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "range-traits") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "0x9z2bgqnqyw47z1isjklinsdk6x7jq8d3w9cawal7kg18gpgsk1") (y #t)))

(define-public crate-btree-range-map-0.7.1 (c (n "btree-range-map") (v "0.7.1") (d (list (d (n "btree-slab") (r "^0.6") (d #t) (k 0)) (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "range-traits") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "0ya7isnl8wgxjmc2cm5gf2pnk4a51x7kddbhs7hpipyn24mdq6mm") (y #t)))

(define-public crate-btree-range-map-0.7.2 (c (n "btree-range-map") (v "0.7.2") (d (list (d (n "btree-slab") (r "^0.6") (d #t) (k 0)) (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "range-traits") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4.5") (d #t) (k 0)))) (h "0cvw6xnzgyi25dbc802pn8gjzqhz2axaxayarc5q1ls64ikwkr8v")))

