(define-module (crates-io bt re btree-vec) #:use-module (crates-io))

(define-public crate-btree-vec-0.1.0 (c (n "btree-vec") (v "0.1.0") (d (list (d (n "tagged-pointer") (r "^0.2") (d #t) (k 0)))) (h "17nrjxmvn83v6r3jc6zp35fcahpc9i78ydcrwbxwkq9ck3n9zyls") (y #t)))

(define-public crate-btree-vec-0.1.1 (c (n "btree-vec") (v "0.1.1") (d (list (d (n "tagged-pointer") (r "^0.2") (d #t) (k 0)))) (h "1l2hwbjrxgc1q2sk0bk6z20bqzz8zm7xsp0h4i2bba23gq969z2n") (y #t)))

(define-public crate-btree-vec-0.1.2 (c (n "btree-vec") (v "0.1.2") (d (list (d (n "tagged-pointer") (r "^0.2") (d #t) (k 0)))) (h "129bjywyln1g4yklrh009lg4br5sjag24gy67qfl7dd58n301jgf") (y #t)))

(define-public crate-btree-vec-0.2.0 (c (n "btree-vec") (v "0.2.0") (d (list (d (n "tagged-pointer") (r "^0.2") (d #t) (k 0)))) (h "1lw8lfjd9q1lynybgg9qiajyalci4c5ynppnvsj0raj6n0kcq57y") (y #t) (r "1.59")))

(define-public crate-btree-vec-0.2.1 (c (n "btree-vec") (v "0.2.1") (d (list (d (n "tagged-pointer") (r "^0.2") (d #t) (k 0)))) (h "0fg3kjiy4jnphmc2gcg5v41smhcdgpv795kgg4x92np17j8b7nw3") (y #t) (r "1.59")))

(define-public crate-btree-vec-0.1.3 (c (n "btree-vec") (v "0.1.3") (d (list (d (n "tagged-pointer") (r "^0.2") (d #t) (k 0)))) (h "11l8r39fsp14p1z1cmk7y65j3iiyc8lxlkq7gk608cdkqjpks44r") (y #t)))

(define-public crate-btree-vec-0.2.2 (c (n "btree-vec") (v "0.2.2") (d (list (d (n "tagged-pointer") (r "^0.2") (d #t) (k 0)))) (h "01gcijww02jkqdns3hgqjahg4kklmrhdqb1pg7x9qv42dy5cj28l") (y #t) (r "1.59")))

(define-public crate-btree-vec-0.2.3 (c (n "btree-vec") (v "0.2.3") (d (list (d (n "tagged-pointer") (r "^0.2") (d #t) (k 0)))) (h "1px1lqywxj2y8n8bw82ndskb2g6wgvr8hg3v9rq7b3ns7h3dvh4w") (y #t) (r "1.59")))

(define-public crate-btree-vec-0.1.4 (c (n "btree-vec") (v "0.1.4") (d (list (d (n "tagged-pointer") (r "^0.2") (d #t) (k 0)))) (h "05js5q7kvw02vwibv18d91lbjhkl5hjg21d1yacxlp4pqkb8ivd4")))

(define-public crate-btree-vec-0.2.4 (c (n "btree-vec") (v "0.2.4") (d (list (d (n "tagged-pointer") (r "^0.2") (d #t) (k 0)))) (h "1d03z8mm49v7i3i3q3vg5bwxdjkw6149v86fs4aq3g68s37qaykb") (f (quote (("dropck_eyepatch")))) (y #t) (r "1.59")))

(define-public crate-btree-vec-0.2.6 (c (n "btree-vec") (v "0.2.6") (d (list (d (n "tagged-pointer") (r "^0.2.4") (d #t) (k 0)))) (h "0ga3g6hyrbg23bw80whawxh1y2dq3gi3f9570mgkkb72y0bxna3y") (f (quote (("dropck_eyepatch")))) (r "1.59")))

(define-public crate-btree-vec-0.3.0 (c (n "btree-vec") (v "0.3.0") (d (list (d (n "add-syntax") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "allocator-fallback") (r "^0.1.7") (o #t) (k 0)) (d (n "allocator-fallback") (r "^0.1.7") (o #t) (k 1)) (d (n "tagged-pointer") (r "^0.2.4") (d #t) (k 0)))) (h "0cwhyh4fcnzn194gv3s5k4bsp61bbpxax6lvjq1zksd103wf7391") (f (quote (("dropck_eyepatch" "add-syntax")))) (s 2) (e (quote (("allocator_api" "allocator-fallback?/allocator_api")))) (r "1.60")))

(define-public crate-btree-vec-0.2.7 (c (n "btree-vec") (v "0.2.7") (d (list (d (n "add-syntax") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "allocator-fallback") (r "^0.1.7") (o #t) (k 0)) (d (n "allocator-fallback") (r "^0.1.7") (o #t) (k 1)) (d (n "tagged-pointer") (r "^0.2.4") (d #t) (k 0)))) (h "07an01npqgvkzldklnbplw7micy16nfm2f2vp6x1vms2lw5s34xh") (f (quote (("dropck_eyepatch" "add-syntax") ("allocator_api" "allocator-fallback/allocator_api")))) (r "1.59")))

(define-public crate-btree-vec-0.2.8 (c (n "btree-vec") (v "0.2.8") (d (list (d (n "add-syntax") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "allocator-fallback") (r "^0.1.7") (o #t) (k 0)) (d (n "allocator-fallback") (r "^0.1.7") (o #t) (k 1)) (d (n "tagged-pointer") (r "^0.2.4") (d #t) (k 0)))) (h "0wbn4b8nmrsyidjsdr2ankd2g53wfs5gfarwx79innv9jd3cbxjd") (f (quote (("dropck_eyepatch" "add-syntax") ("allocator_api" "allocator-fallback/allocator_api")))) (r "1.59")))

(define-public crate-btree-vec-0.3.1 (c (n "btree-vec") (v "0.3.1") (d (list (d (n "add-syntax") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "allocator-fallback") (r "^0.1.7") (o #t) (k 0)) (d (n "allocator-fallback") (r "^0.1.7") (o #t) (k 1)) (d (n "tagged-pointer") (r "^0.2.4") (d #t) (k 0)))) (h "0gqwmwiz40zk3gwpzy065xszbr47kzhs97j3miyfn8imfjdm5zkr") (f (quote (("dropck_eyepatch" "add-syntax")))) (s 2) (e (quote (("allocator_api" "allocator-fallback?/allocator_api")))) (r "1.60")))

