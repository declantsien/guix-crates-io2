(define-module (crates-io bt re btreec) #:use-module (crates-io))

(define-public crate-btreec-0.1.0 (c (n "btreec") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1n0w070w4rdz37rhlxz71mw5agqpjfbdv9y6iz9xjkx99yhz5v79")))

(define-public crate-btreec-0.2.0 (c (n "btreec") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1i1fnag0jlia247mg8hia3sz07fkwh3lr3813nc21f1gddgqakka")))

(define-public crate-btreec-0.3.0 (c (n "btreec") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1mjqsm1zxcf1qb9csl2995wzbk0jrcakh256frjsaqj5l2nry60y")))

