(define-module (crates-io bt re btree-plus-store) #:use-module (crates-io))

(define-public crate-btree-plus-store-0.1.0 (c (n "btree-plus-store") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rustc-arena-modified") (r "^0.1.0") (f (quote ("slab"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "14mvcgg5j6gl4a24h8ahfqcydcd5s4f8w3x1mwk629l27lliyilg") (f (quote (("dot") ("default"))))))

(define-public crate-btree-plus-store-0.1.1 (c (n "btree-plus-store") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rustc-arena-modified") (r "^0.1.0") (f (quote ("slab"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "13f4yiw86kj8y0prvamrm2f0zxidaq2rpa9b484japxyafqdwyvi") (f (quote (("dot") ("default"))))))

(define-public crate-btree-plus-store-0.2.0 (c (n "btree-plus-store") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rustc-arena-modified") (r "^0.1.1") (f (quote ("slab"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "10lshjav043784bb71r8bfaifca0w55ma56p9kd8bmmfq39wnsi7") (f (quote (("default") ("copyable"))))))

(define-public crate-btree-plus-store-0.2.1 (c (n "btree-plus-store") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rustc-arena-modified") (r "^0.1.1") (f (quote ("slab"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "1934qqpsgis9rg1qfdm58x78p7g1yx8023bgcnv9mijzd31xdmnd") (f (quote (("default") ("copyable"))))))

