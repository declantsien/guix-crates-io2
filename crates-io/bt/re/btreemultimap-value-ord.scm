(define-module (crates-io bt re btreemultimap-value-ord) #:use-module (crates-io))

(define-public crate-btreemultimap-value-ord-0.1.0 (c (n "btreemultimap-value-ord") (v "0.1.0") (h "0wnj5x4bng0pf5ry5dd1m4mpx0lm87vfh7ilc2gsc2zg7ahljdg1")))

(define-public crate-btreemultimap-value-ord-0.2.0 (c (n "btreemultimap-value-ord") (v "0.2.0") (h "0xzzhv91qlfs13hbb100g7bnnzkhcsv7q8i370gzl92ksgikhv2b")))

(define-public crate-btreemultimap-value-ord-0.3.0 (c (n "btreemultimap-value-ord") (v "0.3.0") (h "1r1nq032mamd33wr5cavlisvfc7r2w0d1vn881c5p646rnrq3l8s")))

(define-public crate-btreemultimap-value-ord-0.3.1 (c (n "btreemultimap-value-ord") (v "0.3.1") (h "0p4ri9piy7mfq312l01m4rkl4xkar3605qdj182m069w9smqqbyn") (r "1.56.1")))

(define-public crate-btreemultimap-value-ord-0.4.0 (c (n "btreemultimap-value-ord") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06z2ggylnpacj5z7jkfh7p3lr8gbjlkwr8kxq9n29gyxpxb5pky8") (r "1.56.1")))

