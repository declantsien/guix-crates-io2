(define-module (crates-io bt re btree_network) #:use-module (crates-io))

(define-public crate-btree_network-0.1.3 (c (n "btree_network") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "try_encoding_from") (r "^0.1") (o #t) (k 0)))) (h "1yj0qy8i0llb0b7szznhy109plfnv3kbf86s2knsl4j7dnrrgrv2") (f (quote (("serde_yaml" "try_encoding_from/yaml") ("serde_json" "try_encoding_from/json") ("serde_cbor" "try_encoding_from/cbor") ("fmt"))))))

(define-public crate-btree_network-0.2.1 (c (n "btree_network") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "try_encoding_from") (r "^0.1") (o #t) (k 0)))) (h "127hq1r52cl35yvq8v4s7xg785hhgkcz2fiq5558mwi30kvapwg6") (f (quote (("serde_yaml" "try_encoding_from/yaml") ("serde_json" "try_encoding_from/json") ("serde_cbor" "try_encoding_from/cbor") ("fmt")))) (y #t)))

(define-public crate-btree_network-0.2.2 (c (n "btree_network") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "try_encoding_from") (r "^0.1") (o #t) (k 0)))) (h "10l259y1rv5q04nsxbdslqcm15d374rk4lq6h03zaxxb6i5fz0kr") (f (quote (("serde_yaml" "try_encoding_from/yaml") ("serde_json" "try_encoding_from/json") ("serde_cbor" "try_encoding_from/cbor") ("fmt"))))))

(define-public crate-btree_network-0.2.3 (c (n "btree_network") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "try_encoding_from") (r "^0.1") (o #t) (k 0)))) (h "02n9j5z44mbzb6sv9n3r3dfcfnqryab6q55qmpr6xibpn8zxwvz5") (f (quote (("serde_yaml" "try_encoding_from/yaml") ("serde_json" "try_encoding_from/json") ("serde_cbor" "try_encoding_from/cbor") ("fmt"))))))

