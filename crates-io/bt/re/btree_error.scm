(define-module (crates-io bt re btree_error) #:use-module (crates-io))

(define-public crate-btree_error-0.1.0 (c (n "btree_error") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "try_encoding_from") (r "^0.1.3") (o #t) (k 0)))) (h "1x17kz1gzii279v3q52kqdf2541668wb41jhis8n3f93v6m2z9af") (f (quote (("yaml" "try_encoding_from/yaml") ("json" "try_encoding_from/json") ("fmt") ("cbor" "try_encoding_from/cbor"))))))

