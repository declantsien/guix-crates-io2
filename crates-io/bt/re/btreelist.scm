(define-module (crates-io bt re btreelist) #:use-module (crates-io))

(define-public crate-btreelist-0.1.0 (c (n "btreelist") (v "0.1.0") (d (list (d (n "btree-vec") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1dpwd2x7f0djl3lpd2jl6pmgl7fpmbbcbni95ifp2qv426cslaxa")))

(define-public crate-btreelist-0.1.1 (c (n "btreelist") (v "0.1.1") (d (list (d (n "btree-vec") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "150yavh6mhcwjhapv8kdggi0gdlc08prglfk8zdrqy6k1ngw6gwn")))

(define-public crate-btreelist-0.2.0 (c (n "btreelist") (v "0.2.0") (d (list (d (n "btree-vec") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1qv2a9aiq84a5bhpqgps43xi4f23camrn56apsxy5l71dsgsgshh")))

(define-public crate-btreelist-0.3.0 (c (n "btreelist") (v "0.3.0") (d (list (d (n "btree-vec") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0k3c7k1386bk1hcs7q7bqcnl7qk129g919znryn9xwigsp1g6biv")))

(define-public crate-btreelist-0.4.0 (c (n "btreelist") (v "0.4.0") (d (list (d (n "btree-vec") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0f7r39r4d6g150gbx4l83qs0cw7x416gvfhr8435rsq254ngaxjb")))

(define-public crate-btreelist-0.5.0 (c (n "btreelist") (v "0.5.0") (d (list (d (n "btree-vec") (r "^0.2.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "10j45q07axvr3fbrmi0fq3ryi8aqyd0h5i5cyvw80hs3y09kssnk")))

