(define-module (crates-io bt re btree_dag) #:use-module (crates-io))

(define-public crate-btree_dag-0.1.0 (c (n "btree_dag") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "try_encoding_from") (r "^0.1") (o #t) (k 0)))) (h "182dykshrk66y1ir906d2his82ml9xfqixqyd2rz7kix0fqdamfx") (f (quote (("serde_yaml" "try_encoding_from/yaml") ("serde_json" "try_encoding_from/json") ("serde_cbor" "try_encoding_from/cbor") ("fmt"))))))

