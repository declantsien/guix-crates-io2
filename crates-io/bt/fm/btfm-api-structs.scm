(define-module (crates-io bt fm btfm-api-structs) #:use-module (crates-io))

(define-public crate-btfm-api-structs-0.1.0 (c (n "btfm-api-structs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^0.5") (f (quote ("serde"))) (d #t) (k 0)))) (h "1cww9r4dbg7czpdcqjzkv08z14yrdafrphdzlas4r727igkb1frp")))

(define-public crate-btfm-api-structs-0.3.0 (c (n "btfm-api-structs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0pzvnxvhfx5sf0gk6d7hdi4lxcjf6k98pcikrxjvzigw7rsc5rfw")))

(define-public crate-btfm-api-structs-0.3.1 (c (n "btfm-api-structs") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "021c1hrym6q66cqs7h6g56gq0pwq7a7wd7pgj0p4h5j4xp43ljf1")))

