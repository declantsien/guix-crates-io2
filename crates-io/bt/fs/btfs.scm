(define-module (crates-io bt fs btfs) #:use-module (crates-io))

(define-public crate-btfs-0.0.1 (c (n "btfs") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "fuse") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0xlws4ln0nl4k6sl7vy9fzyiwg0fycq9axi60f1hawkkcy3z0gg1")))

(define-public crate-btfs-0.0.2 (c (n "btfs") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "fuse") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0035z0n5kmbsr0lkkhy01jqy5dn7yg2xrpqqllkxbwp0vx0xqchs")))

(define-public crate-btfs-0.0.3 (c (n "btfs") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "fuse") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0b6vff2prxx6nh325lndjhylrgmqhykbzn2cdngdqmlk2khpia2z")))

(define-public crate-btfs-0.0.4 (c (n "btfs") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "fuse") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "1j0f57yy0iqsd65f98fgq8lxxz9rmckx82cnmkr8jwaaa7lfqd8h")))

