(define-module (crates-io bt w- btw-nl) #:use-module (crates-io))

(define-public crate-btw-nl-0.1.0 (c (n "btw-nl") (v "0.1.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)))) (h "0b7n58fh8w6vv47n7r273b9v6z0sbipixk2rn86pgjazi8d13rxn")))

(define-public crate-btw-nl-0.1.1 (c (n "btw-nl") (v "0.1.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)))) (h "150vz0z0468vnmszna59mvz49s8z9bxlra09ml8ls1vh12vgy3za")))

(define-public crate-btw-nl-0.2.0 (c (n "btw-nl") (v "0.2.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)))) (h "16z3pwg2ka3p8q0j28hslss1fx9dnx5m0mcg3gdr79661z3yjbki")))

(define-public crate-btw-nl-0.2.1 (c (n "btw-nl") (v "0.2.1") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)))) (h "16qzs0sjx7423k56iqj2kf0i5hln6r508bhkix6r05axicqy4dib")))

