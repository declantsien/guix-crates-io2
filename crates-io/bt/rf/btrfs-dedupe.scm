(define-module (crates-io bt rf btrfs-dedupe) #:use-module (crates-io))

(define-public crate-btrfs-dedupe-1.0.0 (c (n "btrfs-dedupe") (v "1.0.0") (d (list (d (n "btrfs") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.18") (d #t) (k 0)) (d (n "sha2") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "167nniw4f4zh8bpc5nw24z67f39pfc6gh6238mcvsqcxjvqgkc7w")))

(define-public crate-btrfs-dedupe-1.0.1 (c (n "btrfs-dedupe") (v "1.0.1") (d (list (d (n "btrfs") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.18") (d #t) (k 0)) (d (n "sha2") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "0m7yhi4givampxfigpd7cp0kx9dhngyavi64lv2wr0kv7yp6y2l1")))

(define-public crate-btrfs-dedupe-1.0.2 (c (n "btrfs-dedupe") (v "1.0.2") (d (list (d (n "btrfs") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.18") (d #t) (k 0)) (d (n "sha2") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "1arb4xz3a3f0j0fwxijmcvyn78inbrbl5qmc4d8qb3p5j7qlc6r9")))

