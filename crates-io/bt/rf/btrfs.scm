(define-module (crates-io bt rf btrfs) #:use-module (crates-io))

(define-public crate-btrfs-0.1.0 (c (n "btrfs") (v "0.1.0") (d (list (d (n "ioctl") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (d #t) (k 0)))) (h "06x7911br98ylaqyfp869wsmggf6dxs2b06jl49s1xid14kqlkia")))

(define-public crate-btrfs-1.0.0 (c (n "btrfs") (v "1.0.0") (d (list (d (n "ioctl") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (d #t) (k 0)))) (h "10h1x52r2kigj6vc8v4nz9l6zf5jw1wppc4nk1l1c3cc7k6byh09")))

(define-public crate-btrfs-1.1.0 (c (n "btrfs") (v "1.1.0") (d (list (d (n "ioctl") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (d #t) (k 0)))) (h "06380h6l16qnmym07m6hnxa8x0md36jws12i9izkh29da9i7gzi7")))

(define-public crate-btrfs-1.2.0 (c (n "btrfs") (v "1.2.0") (d (list (d (n "ioctl") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (d #t) (k 0)))) (h "1icc7b18xp604avd860h9dli7h6l23x4a8zyhzdxzya79qhkqznd")))

(define-public crate-btrfs-1.2.1 (c (n "btrfs") (v "1.2.1") (d (list (d (n "crc") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "ioctl") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "minilzo") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (d #t) (k 0)))) (h "1kwwn7ggiss19sq05ybmi682aph8gwb1n8qmbpm9kayvjmvm5x4j")))

(define-public crate-btrfs-1.2.2 (c (n "btrfs") (v "1.2.2") (d (list (d (n "crc") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "minilzo") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (d #t) (k 0)))) (h "1wd33b8lrs1arw819i4j54brhqzi5y1dmsv920l3zghd3la5f2lp")))

