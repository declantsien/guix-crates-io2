(define-module (crates-io bt rf btrfs2) #:use-module (crates-io))

(define-public crate-btrfs2-1.2.2 (c (n "btrfs2") (v "1.2.2") (d (list (d (n "crc") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "minilzo") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "^0.8.1") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (d #t) (k 0)))) (h "1rrjnyznlx83d0v82bqps9i38fxcbnnakv9jmdx5hj05jha97za3")))

