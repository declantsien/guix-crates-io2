(define-module (crates-io bt rf btrfs-no-std) #:use-module (crates-io))

(define-public crate-btrfs-no-std-0.2.0 (c (n "btrfs-no-std") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "num_enum") (r "^0.5.1") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (f (quote ("derive"))) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "0h1l789qxmgy3q275vpqh1vw5mdwjf4c8qbl1m0alsxaj7wndsfb") (y #t)))

(define-public crate-btrfs-no-std-0.2.1 (c (n "btrfs-no-std") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "num_enum") (r "^0.5.1") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (f (quote ("derive"))) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "0hsbj6z025g6fgjjmjayf85cf67pzxvkff0j18ckn5g9bl20llhp")))

