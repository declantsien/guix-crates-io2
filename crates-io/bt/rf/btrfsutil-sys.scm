(define-module (crates-io bt rf btrfsutil-sys) #:use-module (crates-io))

(define-public crate-btrfsutil-sys-0.1.0 (c (n "btrfsutil-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "180vmbq8hhclw987p2yrrjdfm0ik8zyp31iw7hqdfw591jijazcr") (y #t) (l "libbtrfsutil")))

(define-public crate-btrfsutil-sys-0.1.1 (c (n "btrfsutil-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "1vi7jfw5xjprmf6k72qrgxjygpkq2qyni951qhsrg8qhc0kx65ms") (y #t) (l "btrfs")))

(define-public crate-btrfsutil-sys-0.1.2 (c (n "btrfsutil-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "1sy4snbf3sq546gc0c6z65ym1808wicpahjgr6xbr6cad105wjby") (l "btrfsutil")))

(define-public crate-btrfsutil-sys-1.2.0 (c (n "btrfsutil-sys") (v "1.2.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "1k1zawqalz01pjls18j8c68r341a22ykvw8crh7l078jcvxdcivp") (l "btrfsutil")))

(define-public crate-btrfsutil-sys-1.2.1 (c (n "btrfsutil-sys") (v "1.2.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "034h8byya47h8jsczr92fnbjvkvdh1nxfx5lwpr15zsivzy9y7wg") (l "btrfsutil")))

(define-public crate-btrfsutil-sys-1.3.0 (c (n "btrfsutil-sys") (v "1.3.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "04v1wbxq7qvh9czha27fi227wjk5nr4b0drnqfqj07fhf1w1d3ls") (l "btrfsutil")))

