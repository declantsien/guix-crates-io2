(define-module (crates-io bt rf btrfs-diskformat) #:use-module (crates-io))

(define-public crate-btrfs-diskformat-0.1.0 (c (n "btrfs-diskformat") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "enumflags2") (r "^0.6.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zerocopy") (r "^0.3.0") (d #t) (k 0)))) (h "12x86qvb5kiix5xdv93d174bvmp9s0wry2j92f2xsbl0zjp4q1f4") (f (quote (("std" "enumflags2/std"))))))

(define-public crate-btrfs-diskformat-0.2.0 (c (n "btrfs-diskformat") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zerocopy") (r "^0.4.0") (d #t) (k 0)))) (h "0b9z988mqq452ai69jz7q5a3jmfh5npc0gdyq82drqq9h1975z57")))

(define-public crate-btrfs-diskformat-0.3.0 (c (n "btrfs-diskformat") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zerocopy") (r "^0.4.0") (d #t) (k 0)))) (h "0h6p78w4q8vzvandh2mlrckp0kq7fdc5iisqs3k6v6qk6j6jf3br")))

(define-public crate-btrfs-diskformat-0.4.0 (c (n "btrfs-diskformat") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "num_enum") (r "^0.5") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (k 0)) (d (n "zerocopy") (r "^0.4.0") (d #t) (k 0)))) (h "1yhxgvm8llxfqjwrrlakafb54n26qqf3hsdhs9bc932db9jfx55w")))

