(define-module (crates-io bt ni btnify) #:use-module (crates-io))

(define-public crate-btnify-0.3.0 (c (n "btnify") (v "0.3.0") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "12bnydim8ynrdmpawvydqq6yarjzanwj2lzkg8r016wb8bia5jwh")))

(define-public crate-btnify-1.0.0 (c (n "btnify") (v "1.0.0") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "html-to-string-macro") (r "^0.2.5") (d #t) (k 2)) (d (n "hyper") (r "^0.14.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1hgy9zm4pcjjkdhp5wa8x0rshp0im82gvxil3lpk51qrgfizlkmi")))

(define-public crate-btnify-2.0.0 (c (n "btnify") (v "2.0.0") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "html-to-string-macro") (r "^0.2.5") (d #t) (k 2)) (d (n "hyper") (r "^0.14.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("sync" "macros" "signal"))) (d #t) (k 0)))) (h "0600j49hv31jyszyvix7xhcqa7w3g8fp516vafi1fxa6jiih0g5r")))

(define-public crate-btnify-2.0.1 (c (n "btnify") (v "2.0.1") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "html-to-string-macro") (r "^0.2.5") (d #t) (k 2)) (d (n "hyper") (r "^0.14.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("sync" "macros" "signal"))) (d #t) (k 0)))) (h "0q4h50xizcbvx807hfd8rg10iacjmyca7278v02jxjcb3bnnma2a")))

(define-public crate-btnify-2.0.2 (c (n "btnify") (v "2.0.2") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "html-to-string-macro") (r "^0.2.5") (d #t) (k 2)) (d (n "hyper") (r "^0.14.27") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("sync" "macros" "signal"))) (d #t) (k 0)))) (h "05x8z93cj88vvi14r043cmcsvhn5ypj0z3sf1rapr2zzh0dbmwjj")))

