(define-module (crates-io bt cp btcpay-client) #:use-module (crates-io))

(define-public crate-btcpay-client-0.1.0 (c (n "btcpay-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "195q4as4nyrvqilhs0z5yiidryj4lma9j7is08i341rvhr6a7qjx")))

