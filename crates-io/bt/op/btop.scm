(define-module (crates-io bt op btop) #:use-module (crates-io))

(define-public crate-btop-0.1.0 (c (n "btop") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("all-widgets"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "0djchikn05j3ayppwxcv8qjlljl325y86rg0yxl29g2gx8qbbjai")))

