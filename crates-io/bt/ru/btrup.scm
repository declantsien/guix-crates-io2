(define-module (crates-io bt ru btrup) #:use-module (crates-io))

(define-public crate-btrup-0.1.0 (c (n "btrup") (v "0.1.0") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "docopt_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0ycqh4arm7cis5m70q0mgb99ac9xhpamgrjp31c9vsx7zxlbjvbs")))

(define-public crate-btrup-0.1.1 (c (n "btrup") (v "0.1.1") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "docopt_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1v0j6mbnb49vz8d492axznp3kqwmm6dx9njllga47cjk6w6a12ki")))

