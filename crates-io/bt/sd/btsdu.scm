(define-module (crates-io bt sd btsdu) #:use-module (crates-io))

(define-public crate-btsdu-0.1.0 (c (n "btsdu") (v "0.1.0") (d (list (d (n "btrfs-send-parse") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)))) (h "1ig0diram5n1phxqyg6kbadssh9nk0mcasbwanwdrhkglqjlvr9j")))

