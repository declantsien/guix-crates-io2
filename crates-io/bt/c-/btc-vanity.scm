(define-module (crates-io bt c- btc-vanity) #:use-module (crates-io))

(define-public crate-btc-vanity-0.1.0 (c (n "btc-vanity") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1b1m6wmdm7hwncslas4pngzjrhd8akwixgpxwhyn3amn232dysm9")))

(define-public crate-btc-vanity-0.2.0 (c (n "btc-vanity") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "18w74phh5b6cz5x92g2x3ci0s0ixifixryq0zk8lacsxv0gqamyf")))

(define-public crate-btc-vanity-0.3.0 (c (n "btc-vanity") (v "0.3.0") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "07b14d1fflyss8k1axws1qjlfry7vqnjcks3yzggs191xvjza7zw")))

(define-public crate-btc-vanity-0.4.0 (c (n "btc-vanity") (v "0.4.0") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0nriqgfp626h3l0ahrvmxz16wbcbf3hcqwin0kw1drfq70qqh10b") (y #t)))

(define-public crate-btc-vanity-0.4.1 (c (n "btc-vanity") (v "0.4.1") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "07n3hznqqs28f77ag10vm2fzmhiq6q2fmg0s0cdavjvg1f2kb9dy")))

(define-public crate-btc-vanity-0.5.0 (c (n "btc-vanity") (v "0.5.0") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0kvlw3yswjfs9yjvfm2mhx692cid6r1ppzpxrg6aqvs0w8h3z8hr")))

(define-public crate-btc-vanity-0.6.0 (c (n "btc-vanity") (v "0.6.0") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0cam1y314z3x0rgr3wdk7f71la36p0r4vsjr5bis1i1r8qqzb2b9")))

(define-public crate-btc-vanity-0.7.0 (c (n "btc-vanity") (v "0.7.0") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1khjwwys405l91p2hjr8ikhk3zc90nlw4h1r7l31njap7ixm55fh")))

(define-public crate-btc-vanity-0.8.0 (c (n "btc-vanity") (v "0.8.0") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0zhzv9dl7m7ll13399ffjhbdcw561mjkdd7dva05d776c1lnjf20") (y #t)))

(define-public crate-btc-vanity-0.8.1 (c (n "btc-vanity") (v "0.8.1") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "11y04igbjxksp546jfwn67dsllysa7nqxd5li9afdy0y33lwxzg0")))

(define-public crate-btc-vanity-0.9.0 (c (n "btc-vanity") (v "0.9.0") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1gkzzpp3p2fg5vbbj6hrlq5v5x69s3l61lyfr376w6fjhipbr9gk")))

(define-public crate-btc-vanity-1.0.0 (c (n "btc-vanity") (v "1.0.0") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0wwywd0qbpwindc7y5a5k2ckhalyafrn2kc323x94iszfpq1w9h6")))

(define-public crate-btc-vanity-1.0.1 (c (n "btc-vanity") (v "1.0.1") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "103c1mmiyfp1msr33kzl1adsslqh97l250n9sb4rzrdvs50wbm4r")))

(define-public crate-btc-vanity-1.0.2 (c (n "btc-vanity") (v "1.0.2") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0c6abgyn3iifmzb977anliv7klvcaalf2y48ib80xh4xfd6r7gjj")))

(define-public crate-btc-vanity-1.0.3 (c (n "btc-vanity") (v "1.0.3") (d (list (d (n "bitcoin") (r "^0.30.1") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "071r590063n7s805mhs2v5pmx3dpfz0091gwfjmalqm50s9v4k0g")))

