(define-module (crates-io bt c- btc-cli) #:use-module (crates-io))

(define-public crate-btc-cli-0.1.0 (c (n "btc-cli") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.18.0") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.11.0") (d #t) (k 0)) (d (n "rpassword") (r "^0.0.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.12") (f (quote ("rand"))) (d #t) (k 0)))) (h "0skziddks5kl46hhk2q8kba64a7cnkf8h6sb7njy15y38g8fzaca")))

(define-public crate-btc-cli-0.1.1 (c (n "btc-cli") (v "0.1.1") (d (list (d (n "bitcoin") (r "^0.18.0") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.11.0") (d #t) (k 0)) (d (n "rpassword") (r "^0.0.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.12") (f (quote ("rand"))) (d #t) (k 0)))) (h "1minyv41g6jdw3fsn6adpx96nr0ppjb9y1xpiay7imlzc1fh1yvs")))

