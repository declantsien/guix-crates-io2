(define-module (crates-io bt pa btparse) #:use-module (crates-io))

(define-public crate-btparse-0.1.0 (c (n "btparse") (v "0.1.0") (d (list (d (n "eyre") (r "^0.3.8") (d #t) (k 2)))) (h "03xql0731mvmg1qay9z61jpx07rsx34cnav43rkiwnk367cl2mxk")))

(define-public crate-btparse-0.1.1 (c (n "btparse") (v "0.1.1") (d (list (d (n "eyre") (r "^0.3.8") (d #t) (k 2)))) (h "19i8zy9zdg4bxgfwx0qh0w00rv0fmlmnqb9675ywpxhf9pqb73ya")))

