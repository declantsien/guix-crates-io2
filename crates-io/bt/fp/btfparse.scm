(define-module (crates-io bt fp btfparse) #:use-module (crates-io))

(define-public crate-btfparse-1.3.0 (c (n "btfparse") (v "1.3.0") (h "0a4q4id6dzga4r61yqx2w0jg5rzsgbnp20gq6x5jcxiamdlhlmw7")))

(define-public crate-btfparse-1.3.1 (c (n "btfparse") (v "1.3.1") (h "0f5p6g0dxg9mp49yngzkkjfxxpgmy9mlwsis60fz10l2ixds9l95")))

