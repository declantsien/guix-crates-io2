(define-module (crates-io bt ui btui) #:use-module (crates-io))

(define-public crate-btui-0.1.0 (c (n "btui") (v "0.1.0") (h "0n386nw7ig8hvi3lfjmdccwn6djh2zxacbd88vzc1bk7a716am36")))

(define-public crate-btui-0.1.1 (c (n "btui") (v "0.1.1") (h "1sv90f6jq4yb8v6r3jh6gj9wywhg0l0jbv5ig4jdgf6bl5g39gzf")))

(define-public crate-btui-0.2.1 (c (n "btui") (v "0.2.1") (h "1ngmmsrqzgb16m0whrn3ghxcxh5bc67pkhi20p7dmdd601blnslf")))

(define-public crate-btui-0.3.2 (c (n "btui") (v "0.3.2") (h "1nmwxklkfkkfq08am8ph7isjr6qy5pywnik0pnq2r25qk5fl7196")))

(define-public crate-btui-0.3.3 (c (n "btui") (v "0.3.3") (h "0z38q2bi1qrizawhbd2qgdzgk2ijmsr7aamyz4bb8jsf2pprk9wp")))

(define-public crate-btui-0.4.3 (c (n "btui") (v "0.4.3") (h "0r1h5hmagyssp2fds7b90swhccw7azsnf62g2dyqvfaljs5i1l2b") (f (quote (("linux" "core") ("default" "core") ("core"))))))

(define-public crate-btui-0.4.4 (c (n "btui") (v "0.4.4") (h "1g3zlmv1b7sq1nyi630vxmaag81ccrzgb63jyglanfzmppqw4yc5") (f (quote (("linux" "core") ("default" "core") ("core"))))))

(define-public crate-btui-0.4.5 (c (n "btui") (v "0.4.5") (h "0iqdjqca7i2k3m865db1qgh05q7ld92011zggyka4gxprlligvv1") (f (quote (("linux" "core") ("default" "core") ("core"))))))

(define-public crate-btui-0.5.5 (c (n "btui") (v "0.5.5") (h "0n1mfm6a0m10y4sh3ak91zw5rd5sg7n4nkx4w7jh8klgjzm76n0f") (f (quote (("pbar" "linux") ("linux" "core") ("default" "core" "linux" "pbar") ("core"))))))

(define-public crate-btui-0.6.5 (c (n "btui") (v "0.6.5") (d (list (d (n "ncurses") (r "^5.101.0") (o #t) (d #t) (k 0)))) (h "1vg355329hjdyzhiczf1b0n8vxm0r75hs06ain39bwa7w8i04zz4") (f (quote (("pbar" "linux") ("linux" "core" "ncurses") ("default" "core" "linux" "pbar") ("core"))))))

(define-public crate-btui-0.6.6 (c (n "btui") (v "0.6.6") (h "02jq1z4z2dz61b20x5yip0p7815b50f9gj04rg9zjh3pdc48dl4x") (f (quote (("pbar" "linux") ("linux" "core") ("default" "core" "linux" "pbar") ("core"))))))

(define-public crate-btui-0.6.7 (c (n "btui") (v "0.6.7") (h "1v2gsm29q4j5g6kdxyzay5vz1r7pxinhl0wqxj8vi3g6mj95202z") (f (quote (("pbar" "linux") ("linux" "core") ("default" "core" "linux" "pbar") ("core"))))))

(define-public crate-btui-0.6.8 (c (n "btui") (v "0.6.8") (h "0ay534dil3c83whcv21xqm89yg9dzkysncdmnc5kv8jskyyrr1sr") (f (quote (("pbar" "linux") ("linux" "core") ("default" "core" "linux" "pbar") ("core"))))))

(define-public crate-btui-0.6.9 (c (n "btui") (v "0.6.9") (h "071yhshryrgxah6y6kd6r8pl051f17yxx9zhp7l9xw74234zl34z") (f (quote (("pbar" "linux") ("linux" "core") ("default" "core" "linux" "pbar") ("core"))))))

(define-public crate-btui-0.6.10 (c (n "btui") (v "0.6.10") (h "0rqcijmkhgfks7khshkyfgzbpbhwpsgriqxy652mcdh2qw4nd79r") (f (quote (("pbar" "linux") ("linux" "core") ("default" "core" "linux" "pbar") ("core"))))))

(define-public crate-btui-0.6.11 (c (n "btui") (v "0.6.11") (h "1dzcbrjmiqs04ygvl3rwqx5qy1zqnny4kcci9d3j5ch1jwcz5n6z") (f (quote (("pbar" "linux") ("linux" "core") ("default" "core" "linux" "pbar") ("core"))))))

(define-public crate-btui-0.6.12 (c (n "btui") (v "0.6.12") (h "10v4gfjvgx2liwmmvipnxr87g1bkh25gjixsbvh9gj3niv7a6wwp") (f (quote (("pbar" "linux") ("linux" "core") ("default" "core" "linux" "pbar") ("core"))))))

