(define-module (crates-io bt f2 btf2wit) #:use-module (crates-io))

(define-public crate-btf2wit-0.1.0 (c (n "btf2wit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "btfdump") (r "^0.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.11.0") (d #t) (k 0)))) (h "01l3l9q2d8lsgyjjz8gzwz481p69z0kv3fsc9wvmlk9wgps6ha4h")))

(define-public crate-btf2wit-0.1.1 (c (n "btf2wit") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "btfdump") (r "^0.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.11.0") (d #t) (k 0)))) (h "08mmnwb5x75vijnixiva2afz97alxxy922bcgm9n92sryy22hyp1")))

(define-public crate-btf2wit-0.1.2 (c (n "btf2wit") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "btfdump") (r "^0.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.11.0") (d #t) (k 0)))) (h "1xd7h1m0s10igd986r8bncanfd6n8w5dy0n1xxhvm6vahia9milf")))

