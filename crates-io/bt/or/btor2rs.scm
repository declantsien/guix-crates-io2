(define-module (crates-io bt or btor2rs) #:use-module (crates-io))

(define-public crate-btor2rs-0.1.0-alpha.1 (c (n "btor2rs") (v "0.1.0-alpha.1") (d (list (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1w16l0dnv6wr1zx56x9a9mvaii0csghcvgjx3rmqf02gwfkjnw17") (r "1.70")))

(define-public crate-btor2rs-0.1.0-alpha.2 (c (n "btor2rs") (v "0.1.0-alpha.2") (d (list (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1zhqjx3jdc85aj500l9jxhmapvmj3p1i8dkhaddim8s9cwsbqigr") (r "1.70")))

(define-public crate-btor2rs-0.1.0-alpha.3 (c (n "btor2rs") (v "0.1.0-alpha.3") (d (list (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0qm5f0m4n7ajnpnib183rycivjb3wzlk5xmgk809k7cmxhkb3rfm") (r "1.70")))

(define-public crate-btor2rs-0.1.0 (c (n "btor2rs") (v "0.1.0") (d (list (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0bimq4cr7whfzvjsqg1sgc4bpvdnir31f8za0rv6q6sffji2sr9n") (r "1.70")))

