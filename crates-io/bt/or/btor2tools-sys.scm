(define-module (crates-io bt or btor2tools-sys) #:use-module (crates-io))

(define-public crate-btor2tools-sys-1.0.0 (c (n "btor2tools-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "~0.55.1") (d #t) (k 1)) (d (n "copy_dir") (r "~0.1.2") (d #t) (k 1)))) (h "0xqx0kfswgfhygd1xb3zdrskyshach2fv66yman6rpvbximlrz0d") (l "btor2parser")))

(define-public crate-btor2tools-sys-1.1.0 (c (n "btor2tools-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "~0.68.0") (d #t) (k 1)) (d (n "copy_dir") (r "~0.1.2") (d #t) (k 1)))) (h "0pr8krcdyzbp1xwbm8gvz8xj3gbmhskkycxyz6sw8ja0f37rl4ys") (l "btor2parser")))

