(define-module (crates-io bt or btor2tools) #:use-module (crates-io))

(define-public crate-btor2tools-1.0.0 (c (n "btor2tools") (v "1.0.0") (d (list (d (n "btor2tools-sys") (r "~1.0.0") (d #t) (k 0)))) (h "0blxa6jzdgajfycvdhql42xa8iv5arbak32nm35b6yjr2spp42n9")))

(define-public crate-btor2tools-1.1.0 (c (n "btor2tools") (v "1.1.0") (d (list (d (n "btor2tools-sys") (r "~1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.22") (d #t) (k 0)))) (h "16rn66y2fy8rlx1qnv1h4zfh8q0ww9vjcwkw41a13y02qaiyqds3")))

