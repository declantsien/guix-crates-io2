(define-module (crates-io bt _b bt_bencode) #:use-module (crates-io))

(define-public crate-bt_bencode-0.1.0 (c (n "bt_bencode") (v "0.1.0") (d (list (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "00l209b97icq9w9gzsp5vzzhhqbn1hjxbsrc23l1qghhr695ywjk") (y #t)))

(define-public crate-bt_bencode-0.2.0 (c (n "bt_bencode") (v "0.2.0") (d (list (d (n "itoa") (r "^0.4") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_bytes") (r "^0.11") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1xp0r86wmxpbc2cr47pwl9pyn664j3cv4k212v6s3wg4xidn8f4d") (f (quote (("std" "serde/std" "serde_bytes/std" "itoa/std") ("default" "std") ("alloc" "serde/alloc" "serde_bytes/alloc")))) (y #t)))

(define-public crate-bt_bencode-0.3.0 (c (n "bt_bencode") (v "0.3.0") (d (list (d (n "itoa") (r "^0.4") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_bytes") (r "^0.11") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1wxlncp4iyvsj6n6adq7sipnpbisclfxnslzg0s7qsicjxjlqrqr") (f (quote (("std" "serde/std" "serde_bytes/std" "itoa/std") ("default" "std") ("alloc" "serde/alloc" "serde_bytes/alloc")))) (y #t)))

(define-public crate-bt_bencode-0.4.0 (c (n "bt_bencode") (v "0.4.0") (d (list (d (n "itoa") (r "^0.4") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_bytes") (r "^0.11") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0kla6c1pgsm2av65c92hgdjf49ilr1dv134h810byspgdvnkaf5h") (f (quote (("std" "serde/std" "serde_bytes/std" "itoa/std") ("default" "std") ("alloc" "serde/alloc" "serde_bytes/alloc")))) (y #t)))

(define-public crate-bt_bencode-0.5.0 (c (n "bt_bencode") (v "0.5.0") (d (list (d (n "itoa") (r "^1.0.1") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_bytes") (r "^0.11") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "104lc9k3g89p322km6lr5s01kbhpywyqw7s8m8d1d1kilywql2c0") (f (quote (("std" "serde/std" "serde_bytes/std") ("default" "std") ("alloc" "serde/alloc" "serde_bytes/alloc")))) (y #t)))

(define-public crate-bt_bencode-0.5.1 (c (n "bt_bencode") (v "0.5.1") (d (list (d (n "itoa") (r "^1.0.1") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_bytes") (r "^0.11") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "08w7nvxlg8jhyy13v1srgfapg17bzv33nyvs5ny2gs5ww4gzk58z") (f (quote (("std" "serde/std" "serde_bytes/std") ("default" "std") ("alloc" "serde/alloc" "serde_bytes/alloc")))) (y #t)))

(define-public crate-bt_bencode-0.6.0 (c (n "bt_bencode") (v "0.6.0") (d (list (d (n "itoa") (r "^1.0.1") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_bytes") (r "^0.11") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0j18aibxfqdl9hrlm7gri0z86z9zivn2adqgfmbz0rbj81smwqpy") (f (quote (("std" "serde/std" "serde_bytes/std") ("default" "std") ("alloc" "serde/alloc" "serde_bytes/alloc")))) (y #t)))

(define-public crate-bt_bencode-0.6.1 (c (n "bt_bencode") (v "0.6.1") (d (list (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "itoa") (r "^1.0.1") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_bytes") (r "^0.11") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "sha1") (r "^0.10.1") (d #t) (k 2)))) (h "1giagvbkfrkiwjn4zfppb73ymkd13ci3nm3d2lrimyxxn5jycap3") (f (quote (("std" "serde/std" "serde_bytes/std") ("default" "std") ("alloc" "serde/alloc" "serde_bytes/alloc")))) (y #t)))

(define-public crate-bt_bencode-0.7.0 (c (n "bt_bencode") (v "0.7.0") (d (list (d (n "itoa") (r "^1") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_bytes") (r "^0.11") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "sha1") (r "^0.10.1") (d #t) (k 2)))) (h "0xi2wksv4428cz3s401b3wvjsgvkhdkvn9ypkg3dc6izj75q1blm") (f (quote (("std" "serde/std" "serde_bytes/std") ("default" "std") ("alloc" "serde/alloc" "serde_bytes/alloc")))) (r "1.36.0")))

(define-public crate-bt_bencode-0.8.0 (c (n "bt_bencode") (v "0.8.0") (d (list (d (n "itoa") (r "^1") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "sha1") (r "^0.10.1") (d #t) (k 2)))) (h "1d5ib19zi18bjw3famz4211n8jq48czch9g283wrnl497fvi6mf2") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.36.0")))

(define-public crate-bt_bencode-0.8.1 (c (n "bt_bencode") (v "0.8.1") (d (list (d (n "itoa") (r "^1.0.0") (k 0)) (d (n "serde") (r "^1.0.100") (k 0)) (d (n "serde_derive") (r "^1.0.100") (d #t) (k 2)) (d (n "sha1") (r "^0.10.1") (d #t) (k 2)))) (h "093nhnaincf2b6svydry0x6mxakcpryaidqax4way7m060rkalxh") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.36.0")))

