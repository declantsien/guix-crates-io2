(define-module (crates-io bt f- btf-rs) #:use-module (crates-io))

(define-public crate-btf-rs-0.1.0 (c (n "btf-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "test-case") (r "^2.2") (d #t) (k 2)))) (h "1a8a3ggch3gg2l9x504frk2169g943nljyy49rv6hkna9zkpdvqn") (f (quote (("test_runtime"))))))

(define-public crate-btf-rs-1.0.0 (c (n "btf-rs") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "test-case") (r "^3.2") (d #t) (k 2)))) (h "07a9fisy0ppg9lwawi2i822w5qzbx3q6hsxyh984v1vdmh3pmc58") (f (quote (("test_runtime"))))))

(define-public crate-btf-rs-1.1.0 (c (n "btf-rs") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "test-case") (r "^3.2") (d #t) (k 2)))) (h "1182qmgs68k0gybs18r9zpyxqqhy4fp06rklh3401xxm78fx3r7i") (f (quote (("test_runtime"))))))

