(define-module (crates-io bt f- btf-derive) #:use-module (crates-io))

(define-public crate-btf-derive-0.3.0 (c (n "btf-derive") (v "0.3.0") (d (list (d (n "btf") (r "^0.3.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05wanv2jx7yr39534q6chhx616k6glqjqxgprn1dxajmp6s9zqx2")))

(define-public crate-btf-derive-0.3.1 (c (n "btf-derive") (v "0.3.1") (d (list (d (n "btf") (r "^0.3.1") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0imzxm89cksjjk1as9grka6j1lwv6kxggiyba37vj79saawjhfms")))

(define-public crate-btf-derive-0.3.2 (c (n "btf-derive") (v "0.3.2") (d (list (d (n "btf") (r "^0.3.2") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05dqyx8vhxi9hsda5f3hrfs0y15qgj3f3gr5xd8casy3a43v72zc")))

