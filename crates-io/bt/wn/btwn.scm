(define-module (crates-io bt wn btwn) #:use-module (crates-io))

(define-public crate-btwn-0.2.0 (c (n "btwn") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0w0m2xib5br9anz36jwdbh9s3mvm400iq5dns15vcfp72xaqiy33")))

(define-public crate-btwn-0.2.1 (c (n "btwn") (v "0.2.1") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0vlb4ji40y2rxhaqyzjkkvbib414sjavdb0svwnvlrpg3fbz5ngm")))

(define-public crate-btwn-0.3.0 (c (n "btwn") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0k87f6gazjm706r85mg1v7yk2gpqxj8lhmw717w983amw1fdc4qh")))

(define-public crate-btwn-0.4.0 (c (n "btwn") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1pmmqqyrjs73462hxcfa56cg387b2bvv6xxii1inw56a40fgx4z8")))

(define-public crate-btwn-0.5.0 (c (n "btwn") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1vhxhj2iw8ivq1ihgnql9mvfmffykahcvi245zw457h7yklzck6k")))

(define-public crate-btwn-0.5.1 (c (n "btwn") (v "0.5.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "16bg9jww8x6qsl8in66dhzw1ipgdhnwg430qb72nb4s8py0m9hxd")))

(define-public crate-btwn-0.6.0 (c (n "btwn") (v "0.6.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1qz5gaxdqy0scljxz404xjifwk896bib9z8szj8rrxlqdc2wjxpv")))

