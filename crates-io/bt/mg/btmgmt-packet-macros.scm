(define-module (crates-io bt mg btmgmt-packet-macros) #:use-module (crates-io))

(define-public crate-btmgmt-packet-macros-0.2.0 (c (n "btmgmt-packet-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0gjn1682vss9x5018hkg6ac310wwb4pxmvig9bj9ygg73lbxzhl8")))

(define-public crate-btmgmt-packet-macros-0.2.1 (c (n "btmgmt-packet-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "09d8cg451c6ph77f7bvfl3nh44g953r4x5ymi5llc4anqf7bgivn")))

(define-public crate-btmgmt-packet-macros-0.2.2 (c (n "btmgmt-packet-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1qskh0cbyhcyw224wjzhz41aqwbsdgc04yva6yw4k6gkhaf0ppx5")))

(define-public crate-btmgmt-packet-macros-0.2.3 (c (n "btmgmt-packet-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0kyck80p5240m527h8h5m9554sbcd2hi6jyvcv3hi4rad8817yl9")))

(define-public crate-btmgmt-packet-macros-0.2.4 (c (n "btmgmt-packet-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "17k2yp3isvsv77ra6s460d66753isdp8sr3kpinc4r27flbfckjj")))

(define-public crate-btmgmt-packet-macros-0.2.5 (c (n "btmgmt-packet-macros") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0rjh3rz4j8ib1zmkaxh50c6h2i8glli814f0zrvqf4vb4iy82flx")))

(define-public crate-btmgmt-packet-macros-0.3.0-alpha.1 (c (n "btmgmt-packet-macros") (v "0.3.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0g6lvv9vcpbxhm9jawl4xz0d9n6jpd56gg3jwawrbznwym3sr5ny")))

(define-public crate-btmgmt-packet-macros-0.3.0-alpha.2 (c (n "btmgmt-packet-macros") (v "0.3.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0gqsrk1lmw2wp3xkq18nv6bdbc093z4b5ygpicnzs5y35h1z98rx")))

(define-public crate-btmgmt-packet-macros-0.3.0-alpha.3 (c (n "btmgmt-packet-macros") (v "0.3.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "06mmhm1xmnxcp49pac41adabncjn08bnn6pv6kvs68dilnazjwdj")))

(define-public crate-btmgmt-packet-macros-0.3.0-alpha.4 (c (n "btmgmt-packet-macros") (v "0.3.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "01nl9z6ndz212f4kld5pclijmiby78lbfh0c0d15sx96c9nwq31y")))

