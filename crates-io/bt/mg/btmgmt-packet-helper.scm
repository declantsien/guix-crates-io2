(define-module (crates-io bt mg btmgmt-packet-helper) #:use-module (crates-io))

(define-public crate-btmgmt-packet-helper-0.2.0 (c (n "btmgmt-packet-helper") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 2)) (d (n "btmgmt-packet-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0dypry8mx51f2y97a5ckf1qbfjdiy1jizs0r9radssdpimnsd897")))

(define-public crate-btmgmt-packet-helper-0.2.1 (c (n "btmgmt-packet-helper") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 2)) (d (n "btmgmt-packet-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1glmwwikpmj4lz3vq2iwwig7mdpdi9lpn07l2vmrhzia2jzf6qsk")))

(define-public crate-btmgmt-packet-helper-0.2.2 (c (n "btmgmt-packet-helper") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 2)) (d (n "btmgmt-packet-macros") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1g56c5dm9i05cfxl6mai7vrf4vhik6k7l48c1r6xzgk9aaabhkk9")))

(define-public crate-btmgmt-packet-helper-0.2.3 (c (n "btmgmt-packet-helper") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 2)) (d (n "btmgmt-packet-macros") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0k10k19q0v1npkg8jqzgblxl5iqrswiwzp8f861fi0i5crxp4vv9")))

(define-public crate-btmgmt-packet-helper-0.2.4 (c (n "btmgmt-packet-helper") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 2)) (d (n "btmgmt-packet-macros") (r "^0.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "194921z05hi6psdg4m9j2y28hj92ksrvdgacpj93bsr15jf666lw")))

(define-public crate-btmgmt-packet-helper-0.2.5 (c (n "btmgmt-packet-helper") (v "0.2.5") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 2)) (d (n "btmgmt-packet-macros") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1mk6x94309afc8vjl90w3igh9yd96bnb4lp0v0cv8s4jm5ik1jcb")))

(define-public crate-btmgmt-packet-helper-0.3.0-alpha.1 (c (n "btmgmt-packet-helper") (v "0.3.0-alpha.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 2)) (d (n "btmgmt-packet-macros") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1c1zzbdnlf8ygniq8lsx8z1abq0xx62l0fxm03w29yc9pd13778c")))

(define-public crate-btmgmt-packet-helper-0.3.0-alpha.2 (c (n "btmgmt-packet-helper") (v "0.3.0-alpha.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 2)) (d (n "btmgmt-packet-macros") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "11k32yifcyarvy6l3wxcw28a89yrfsajys6j2qli6kdi4i5jss7q")))

(define-public crate-btmgmt-packet-helper-0.3.0-alpha.3 (c (n "btmgmt-packet-helper") (v "0.3.0-alpha.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 2)) (d (n "btmgmt-packet-macros") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0d72xmv07flmlzgscw81j9dh8pqxy8j273g1a2nhc894dzcvmzrj")))

(define-public crate-btmgmt-packet-helper-0.3.0-alpha.4 (c (n "btmgmt-packet-helper") (v "0.3.0-alpha.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 2)) (d (n "btmgmt-packet-macros") (r "^0.3.0-alpha.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0bv3d4svvdgfqm6kckk520xq4r6fndwzr9ghxq42g79iy637j6ca")))

