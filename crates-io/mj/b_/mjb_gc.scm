(define-module (crates-io mj b_ mjb_gc) #:use-module (crates-io))

(define-public crate-mjb_gc-0.1.0 (c (n "mjb_gc") (v "0.1.0") (d (list (d (n "mjb_gc_derive") (r "^0.1.0") (d #t) (k 0)))) (h "14lnwqng0q8glr1x0z7x9sjzd2xjh46hr57d0d9k8f551l7aa0g1") (f (quote (("trace"))))))

(define-public crate-mjb_gc-0.2.0 (c (n "mjb_gc") (v "0.2.0") (d (list (d (n "mjb_gc_derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "14ljrdzbhdhgljdbdhkd85cz2g9dia9kxiylh5am5z0alg3v7348") (f (quote (("trace") ("derive" "mjb_gc_derive"))))))

