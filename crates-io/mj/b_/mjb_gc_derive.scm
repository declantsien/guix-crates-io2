(define-module (crates-io mj b_ mjb_gc_derive) #:use-module (crates-io))

(define-public crate-mjb_gc_derive-0.1.0 (c (n "mjb_gc_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1cmflkgcln9zyw2xrv06qkilqmm0qi720xwfy1y50fjm3pjhxxyl")))

(define-public crate-mjb_gc_derive-0.2.0 (c (n "mjb_gc_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0xw4xwpx6yc7pbg8d9ki0xq1w955sfi4vryj8wi2vvd8xnvmip4d")))

