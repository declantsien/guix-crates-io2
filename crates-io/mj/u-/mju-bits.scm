(define-module (crates-io mj u- mju-bits) #:use-module (crates-io))

(define-public crate-mju-bits-0.1.0 (c (n "mju-bits") (v "0.1.0") (d (list (d (n "typenum") (r "^1.11.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0b4swlg4z6wdrbcr700jq5iigy5kjxfvkp0w7bzz3k1h45yz9wjc")))

(define-public crate-mju-bits-0.2.0 (c (n "mju-bits") (v "0.2.0") (d (list (d (n "typenum") (r "^1.11.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1i6vd0lz7pzd4xxw3xdajx3ivi18inqava5v8yj7scv544bc2d9z")))

(define-public crate-mju-bits-0.3.0 (c (n "mju-bits") (v "0.3.0") (d (list (d (n "typenum") (r "^1.11.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1x0d20v02w0j2i0v8xk2dycdkrgs0dbjizd0014rvxvbpkddd2gn")))

