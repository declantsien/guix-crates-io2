(define-module (crates-io ks z8 ksz8863) #:use-module (crates-io))

(define-public crate-ksz8863-0.1.0 (c (n "ksz8863") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.19.4") (d #t) (k 0)) (d (n "hash32") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "13m07b19rlq8hxgpm9pbq8nim30hkx6ik9b23m93zlqmy1i93x9w") (f (quote (("hash-32" "hash32" "hash32-derive"))))))

(define-public crate-ksz8863-0.1.1 (c (n "ksz8863") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.20.0") (k 0)) (d (n "hash32") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "129vayi3irrqsscz0vcgbr6jfndhxmq0ns4hxvnbm5475lgcv51w") (f (quote (("hash-32" "hash32" "hash32-derive"))))))

(define-public crate-ksz8863-0.2.0 (c (n "ksz8863") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.20.0") (k 0)) (d (n "hash32") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "mdio") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "ufmt") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1cgap7nmny6b8fcp39d5l83rpijx98a39qjfkw7gg7p6ah2qzfi5") (f (quote (("hash-32" "hash32" "hash32-derive"))))))

