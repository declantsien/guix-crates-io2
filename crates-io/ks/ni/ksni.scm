(define-module (crates-io ks ni ksni) #:use-module (crates-io))

(define-public crate-ksni-0.0.0 (c (n "ksni") (v "0.0.0") (d (list (d (n "dbus") (r "^0.6") (d #t) (k 0)) (d (n "dbus-codegen") (r "^0.3") (d #t) (k 1)))) (h "1wi54wfcniq1yz72mb9rnphbzafbs9hjyvcws3s2nnl7fxhp3b9c")))

(define-public crate-ksni-0.1.0 (c (n "ksni") (v "0.1.0") (d (list (d (n "dbus") (r "^0.7") (d #t) (k 0)) (d (n "dbus-codegen") (r "^0.4") (d #t) (k 1)))) (h "0bzs6q3w4zd2p9hq27cf0q69dnxirqzk948mzy9qk3wf11pspri0")))

(define-public crate-ksni-0.1.1 (c (n "ksni") (v "0.1.1") (d (list (d (n "dbus") (r "^0.7") (d #t) (k 0)) (d (n "dbus-codegen") (r "^0.4") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0md8j1g7bjb5ky8dca0qa7yg778jpjrvvk67f7ibzcgihh6n73mc")))

(define-public crate-ksni-0.1.2 (c (n "ksni") (v "0.1.2") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "dbus-codegen") (r "^0.9") (d #t) (k 1)) (d (n "dbus-tree") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1i1gwz7li7whxkcsf1w60whzk3asy84yg93rgs0201c6mgisfizp")))

(define-public crate-ksni-0.1.3 (c (n "ksni") (v "0.1.3") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "dbus-codegen") (r "^0.9") (d #t) (k 1)) (d (n "dbus-tree") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19xk6y1zfgwh8piv0514g54bkkp4qgwdjl9dh20fmi5a4mqn4zbc")))

(define-public crate-ksni-0.2.0 (c (n "ksni") (v "0.2.0") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "dbus-codegen") (r "^0.9") (d #t) (k 1)) (d (n "dbus-tree") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1w3k14kk7izypczg8wsrnw6ycgaahcd8x7981806lmvbd8a8dds8")))

(define-public crate-ksni-0.2.1 (c (n "ksni") (v "0.2.1") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "dbus-codegen") (r "^0.9") (d #t) (k 1)) (d (n "dbus-tree") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "030x4w6xqcm6f7b8gl7w4vsh0yzqii9l82wh176jplmwhvs9q6xk")))

(define-public crate-ksni-0.2.2 (c (n "ksni") (v "0.2.2") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "dbus-codegen") (r "^0.9") (d #t) (k 1)) (d (n "dbus-tree") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ny832wm7nd2kaaxhmdrq5cd0vy1q1dd7f42air5avh1vl5k2d29") (r "1.58")))

