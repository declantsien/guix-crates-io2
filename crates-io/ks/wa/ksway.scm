(define-module (crates-io ks wa ksway) #:use-module (crates-io))

(define-public crate-ksway-0.0.0 (c (n "ksway") (v "0.0.0") (h "1slhfw0a2igkvc3yfgrdcq2fj016zab00srn3cm9li5xzyf29iq0")))

(define-public crate-ksway-0.1.0 (c (n "ksway") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "globwalk") (r "^0.7.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "json") (r "^0.11.14") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 2)) (d (n "num-derive") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 2)) (d (n "regex") (r "^1.1.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1i84f1vvknj4743fxlp44b02j1fb7dihxx1sfs7zfd7hflvdlyyv")))

