(define-module (crates-io ks nn ksnn) #:use-module (crates-io))

(define-public crate-ksnn-0.1.1 (c (n "ksnn") (v "0.1.1") (d (list (d (n "indicatif") (r "~0.16.2") (d #t) (k 0)) (d (n "ndarray") (r "~0.15.4") (d #t) (k 0)) (d (n "ndarray-rand") (r "~0.14.0") (d #t) (k 0)) (d (n "rand") (r "~0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "~0.4.3") (d #t) (k 0)))) (h "1l197cskdi9nbbb12ff4l5c2l3wid5klppmiw9gl15c6xfvkkwg7") (y #t)))

(define-public crate-ksnn-0.1.2 (c (n "ksnn") (v "0.1.2") (d (list (d (n "indicatif") (r "~0.16.2") (d #t) (k 0)) (d (n "ndarray") (r "~0.15.4") (d #t) (k 0)) (d (n "ndarray-rand") (r "~0.14.0") (d #t) (k 0)) (d (n "rand") (r "~0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "~0.4.3") (d #t) (k 0)))) (h "10alaszk818f5scdr0rfkslfq12yyswdfz8ixfq2f4p0rcv55wig")))

(define-public crate-ksnn-0.2.0 (c (n "ksnn") (v "0.2.0") (d (list (d (n "indicatif") (r "~0.16.2") (d #t) (k 0)) (d (n "ndarray") (r "~0.15.4") (d #t) (k 0)) (d (n "ndarray-rand") (r "~0.14.0") (d #t) (k 0)) (d (n "rand") (r "~0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "~0.4.3") (d #t) (k 0)))) (h "0140inh6f8i9l3r36d2p81d6znfnmdqrdg2fvs3p79a1k517f9s1")))

(define-public crate-ksnn-0.3.0 (c (n "ksnn") (v "0.3.0") (d (list (d (n "indicatif") (r "~0.16.2") (d #t) (k 0)) (d (n "ndarray") (r "~0.15.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "~0.14.0") (d #t) (k 0)) (d (n "rand") (r "~0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "~0.4.3") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "0ghb7flpy3v57apqx4yld0ka3y1j94ph9ff6mxlvy606y01kgwz5")))

