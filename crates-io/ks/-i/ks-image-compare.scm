(define-module (crates-io ks #{-i}# ks-image-compare) #:use-module (crates-io))

(define-public crate-ks-image-compare-0.1.0 (c (n "ks-image-compare") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eframe") (r "^0.25.0") (d #t) (k 0)) (d (n "egui") (r "^0.25.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.25.0") (f (quote ("all_loaders"))) (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0xlgqi9smigrv2kk3dbwk8k3p5dh380ahzwmis8rlycwzi2h7hkr")))

