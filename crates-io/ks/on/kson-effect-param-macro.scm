(define-module (crates-io ks on kson-effect-param-macro) #:use-module (crates-io))

(define-public crate-kson-effect-param-macro-0.1.0 (c (n "kson-effect-param-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "15w14j8l7384wfnv3lh1x5db8dx0g4fq2kqsjhymi01i0bgs0dv9")))

