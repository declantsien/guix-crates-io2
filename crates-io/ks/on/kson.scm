(define-module (crates-io ks on kson) #:use-module (crates-io))

(define-public crate-kson-0.1.0 (c (n "kson") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 2)) (d (n "kson-effect-param-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1si0pfwg29nqvpr23m2qzx26r85nbm81rb1237x4dwyhpqalygf3")))

