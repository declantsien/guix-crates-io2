(define-module (crates-io ks ui ksuid-cli) #:use-module (crates-io))

(define-public crate-ksuid-cli-0.2.0 (c (n "ksuid-cli") (v "0.2.0") (d (list (d (n "docopt") (r "^0.8.0") (d #t) (k 0)) (d (n "ksuid") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "17xs46xyzrc3g18bq4hdj9bwxg6qx4xl0iv3yw50wrpz592kfd5p")))

