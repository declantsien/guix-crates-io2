(define-module (crates-io ks ta kstat-rs) #:use-module (crates-io))

(define-public crate-kstat-rs-0.1.0 (c (n "kstat-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05043cp4gwwh8b37nz8b42zy9b502w0h2dmi6sg8p5i52g79r2s9")))

(define-public crate-kstat-rs-0.2.0 (c (n "kstat-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0x9rsm97bvkz5i0vvzd50m0rc00zjrbqvr7lhwmq85p9sw865ly1")))

(define-public crate-kstat-rs-0.2.1 (c (n "kstat-rs") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1h5ra0dkgd4fc39d7pl4qx0ys179dy9z87qlid11w77rd9pcx7la")))

(define-public crate-kstat-rs-0.2.3 (c (n "kstat-rs") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rvdzyi7xsj791q7nik04rgm1sk472nxn4j0q3q7qx9gj33i7izw")))

