(define-module (crates-io ks pc kspconfigtool) #:use-module (crates-io))

(define-public crate-kspconfigtool-0.1.0 (c (n "kspconfigtool") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "string-builder") (r "^0.2") (d #t) (k 0)))) (h "0101l5v31ijnhl2yfwkx1bvdaq1bb528vc7b7da1dp7f611way3p") (r "1.68.2")))

