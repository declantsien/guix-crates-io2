(define-module (crates-io ks to kstool) #:use-module (crates-io))

(define-public crate-kstool-0.1.0 (c (n "kstool") (v "0.1.0") (h "1hxcxpcjp0i19xzlly141j6fa08274bh0nxx1mxhqv55yzsjiqic")))

(define-public crate-kstool-0.1.1 (c (n "kstool") (v "0.1.1") (h "0a0zgl0pki8ixdsnvx8b8mysyrcqiv3a5d9rkli112xqj5ny7rdg")))

(define-public crate-kstool-0.1.2 (c (n "kstool") (v "0.1.2") (h "071lrmbaypljp2pwx9bsmfmr6m1mwswygyb87hrpil2sk1yfdidm")))

(define-public crate-kstool-0.2.0 (c (n "kstool") (v "0.2.0") (d (list (d (n "sqlx") (r "^0.7.0-alpha.3") (f (quote ("sqlite"))) (o #t) (d #t) (k 0)))) (h "01h063famm4msfpa329fzkshspfj7pk0r2ijam0wj7x0qifgfal9")))

(define-public crate-kstool-0.2.1 (c (n "kstool") (v "0.2.1") (d (list (d (n "sqlx") (r "^0.7.0-alpha.3") (f (quote ("sqlite"))) (o #t) (d #t) (k 0)))) (h "0jk1dcxnd9i3ksfdqngzddkpk0y8iwq2v2m0jw71srybzvijl0nk")))

