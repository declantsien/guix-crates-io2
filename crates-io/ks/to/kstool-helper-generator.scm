(define-module (crates-io ks to kstool-helper-generator) #:use-module (crates-io))

(define-public crate-kstool-helper-generator-0.4.1 (c (n "kstool-helper-generator") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i1x88kacfx3cqq8ikzc1z3dc922kba7zzfahy7z90ys1xzx1kr3")))

(define-public crate-kstool-helper-generator-0.4.2 (c (n "kstool-helper-generator") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0za5xy9rmgcczah3wk7y38s2hrmwiha6py0r37inq1izq708wwhs") (y #t)))

(define-public crate-kstool-helper-generator-0.4.3 (c (n "kstool-helper-generator") (v "0.4.3") (d (list (d (n "fancy-regex") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0655zxpsl7ndj92y781ypl3qh3i0fhjlpj36fnihymvsbz07p93a")))

(define-public crate-kstool-helper-generator-0.4.4 (c (n "kstool-helper-generator") (v "0.4.4") (d (list (d (n "fancy-regex") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ypsqj90wj9s8b9ivyn626kbn1c7bldpndcl37z1ivw33s2gryrk")))

