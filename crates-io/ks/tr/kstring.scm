(define-module (crates-io ks tr kstring) #:use-module (crates-io))

(define-public crate-kstring-0.1.0-alpha.2 (c (n "kstring") (v "0.1.0-alpha.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1hnn4rbs3mzd0d995129y426wqc8zyjc4ghx6blxbzpy8kb3abig")))

(define-public crate-kstring-0.1.0-alpha.3 (c (n "kstring") (v "0.1.0-alpha.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0df0wvhjz0nqc5nbrsnw7p0b8lj5krwgi44cb5475mbxgf7mf7ba")))

(define-public crate-kstring-0.1.0-alpha.4 (c (n "kstring") (v "0.1.0-alpha.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1gqmrg8030hyjyg0qkb3d1z1dm0c6kmn758p7avkznlg48qk5m04")))

(define-public crate-kstring-0.1.0-alpha.5 (c (n "kstring") (v "0.1.0-alpha.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0ghlf5cn676xz9ng55jbjbqn5mfqv76s37bk9ks9y7byl8ifv6ys")))

(define-public crate-kstring-0.1.0-alpha.6 (c (n "kstring") (v "0.1.0-alpha.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0nliy1496d4qaqn1swg0kr3ywalrcpwp7pwwyn8nqvfj34ihb7fa")))

(define-public crate-kstring-0.1.0-alpha.7 (c (n "kstring") (v "0.1.0-alpha.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "19822qqkgjg1i4zd1d5h5p2i0pfxk362hqv9k3bwh7bxyncdh6fa")))

(define-public crate-kstring-0.1.0-alpha.8 (c (n "kstring") (v "0.1.0-alpha.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0sb5bs6fawhgg8gax81accfbi3l9r1by169gcp4wnyjrqx45g688")))

(define-public crate-kstring-0.1.0-alpha.9 (c (n "kstring") (v "0.1.0-alpha.9") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "06cfcgsyydn2rxf9i40vgyq8i2wbkcb8z3b0nprjp495ab0pzs1q")))

(define-public crate-kstring-0.1.0 (c (n "kstring") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "02fnfl4hgmvl69xp53fvfq81ajn9413n7dihj0a2xlhg219xz0k3")))

(define-public crate-kstring-0.1.1 (c (n "kstring") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "13mm31pnnkjak3lcv965wng355mspix5r4vf6kvdsmhdp2z31g7v")))

(define-public crate-kstring-0.1.2 (c (n "kstring") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "019lla1dp2gplck8rp86frgr4cnsqg44n2ynwwg644a1wbc33b0y")))

(define-public crate-kstring-1.0.0 (c (n "kstring") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1d5l025g1g0sdh4k1yfi80ibch7jl44b871j8n4ihpvqdm3hay65")))

(define-public crate-kstring-1.0.1 (c (n "kstring") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "059j6kcibdrmc2my4hagy62955ivr7vdwrzrfjynfc5lhn1khrqi")))

(define-public crate-kstring-1.0.2 (c (n "kstring") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0716xg4imyskaylrk2lw3dh12qicqm4wd40p6h965547vf42fvjf") (f (quote (("default" "serde"))))))

(define-public crate-kstring-1.0.3 (c (n "kstring") (v "1.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0h5jwjz7lz743yx1clj1k1ypvp07zmmr176g6lzdd4rqdlyxyxm9") (f (quote (("default" "serde"))))))

(define-public crate-kstring-1.0.4 (c (n "kstring") (v "1.0.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1yp72jlb8x657wdl1qw9rb5gdsv2frrq9fjrqdz6zjff54mxpwhf") (f (quote (("default" "serde"))))))

(define-public crate-kstring-1.0.5 (c (n "kstring") (v "1.0.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "smol_str") (r "^0.1.18") (d #t) (k 2)))) (h "08b2cwmr5a0214l4qf7sdvdgkrq0qjyxapyvr1w91k1q56cpx3bf") (f (quote (("max_inline") ("default" "serde") ("bench_subset_unstable") ("arc"))))))

(define-public crate-kstring-1.0.6 (c (n "kstring") (v "1.0.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "smol_str") (r "^0.1.18") (d #t) (k 2)))) (h "09j5xb3rnjd3kmc2v667wzsc4mz4c1hl1vkzszbj30fyxb60qccb") (f (quote (("max_inline") ("default" "serde") ("bench_subset_unstable") ("arc"))))))

(define-public crate-kstring-1.1.0 (c (n "kstring") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1r4n9fa5scikqvl736nxghcfa6s3b07xz61w43hyzs2qb3wmd3nk") (f (quote (("unstable_bench_subset") ("unsafe") ("max_inline") ("default" "unsafe" "serde") ("arc")))) (y #t)))

(define-public crate-kstring-2.0.0 (c (n "kstring") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "document-features") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0isp7kmk4q0qxpcd877q77ykgb3ryfbmj18djmnwv8c210sncc7c") (f (quote (("unstable_bench_subset") ("unsafe") ("std") ("max_inline") ("default" "std" "unsafe") ("arc"))))))

