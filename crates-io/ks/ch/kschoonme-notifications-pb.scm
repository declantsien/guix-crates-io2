(define-module (crates-io ks ch kschoonme-notifications-pb) #:use-module (crates-io))

(define-public crate-kschoonme-notifications-pb-0.1.0 (c (n "kschoonme-notifications-pb") (v "0.1.0") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.0") (d #t) (k 1)))) (h "06vig2vvh7l2lb29ls2vvgl8zmlw9p34kk16ypvsq0s0y9nd2364") (y #t)))

(define-public crate-kschoonme-notifications-pb-0.1.1 (c (n "kschoonme-notifications-pb") (v "0.1.1") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.0") (d #t) (k 1)))) (h "106f1zd6glir5yc2giw6a9ybvxrbcqhldb86rb6a41dkl1akg0zp") (y #t)))

(define-public crate-kschoonme-notifications-pb-0.1.2 (c (n "kschoonme-notifications-pb") (v "0.1.2") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.0") (d #t) (k 1)))) (h "1wm3a8mf42as9s7i3as22d5vn74d2ppibvr9lmf530l01jjr4jpw") (y #t)))

(define-public crate-kschoonme-notifications-pb-1.2.0 (c (n "kschoonme-notifications-pb") (v "1.2.0") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.0") (d #t) (k 1)))) (h "0dax4c6w4g9m9jwxaj3m7cfa7gm0911b9y8jypx2mwx581q43yd2") (y #t)))

(define-public crate-kschoonme-notifications-pb-1.3.0 (c (n "kschoonme-notifications-pb") (v "1.3.0") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.0") (d #t) (k 1)))) (h "16j4ga5h7b9h29xmr8pnwz6qxfrkibpg0lanv896g71rf9vlda07") (y #t)))

(define-public crate-kschoonme-notifications-pb-1.0.0 (c (n "kschoonme-notifications-pb") (v "1.0.0") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.0") (d #t) (k 1)))) (h "1i10bsqps3ndjrz9lp6l4kj8c1smla1sdy7p98ynkm1ngazn9305") (y #t)))

(define-public crate-kschoonme-notifications-pb-1.3.1 (c (n "kschoonme-notifications-pb") (v "1.3.1") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.0") (d #t) (k 1)))) (h "1ldg232cxj3yiar5hc4f9f9ald4ich1bm9d9vgk7y6bpdj3rzpmh")))

(define-public crate-kschoonme-notifications-pb-2.0.0-alpha.0 (c (n "kschoonme-notifications-pb") (v "2.0.0-alpha.0") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.0") (d #t) (k 1)))) (h "1fqq8f0wcyx388dvcdphmpy593lpc75j6dlfqq9v3kak28qk947q")))

(define-public crate-kschoonme-notifications-pb-2.0.0-alpha.1 (c (n "kschoonme-notifications-pb") (v "2.0.0-alpha.1") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.0") (d #t) (k 1)))) (h "0yscflp1fd619ddxq80zc7rfsa1qbqrwiz0jzpwkggshn38dz797")))

(define-public crate-kschoonme-notifications-pb-2.0.0 (c (n "kschoonme-notifications-pb") (v "2.0.0") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.0") (d #t) (k 1)))) (h "1lapzwq49mxxggw3xs8dvimpim09f699nnq1irqzc06q94x5px3x")))

