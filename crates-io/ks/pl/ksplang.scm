(define-module (crates-io ks pl ksplang) #:use-module (crates-io))

(define-public crate-ksplang-0.1.0 (c (n "ksplang") (v "0.1.0") (d (list (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1w8fvsd1r28739b24nk95d9p4jz322hm4n59lpqwa4ndnp6walwz")))

