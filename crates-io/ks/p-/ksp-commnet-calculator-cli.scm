(define-module (crates-io ks p- ksp-commnet-calculator-cli) #:use-module (crates-io))

(define-public crate-ksp-commnet-calculator-cli-0.1.0 (c (n "ksp-commnet-calculator-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ksp-commnet-calculator-core") (r "^0.1.2") (d #t) (k 0)))) (h "1bkyz9zgvwldgqz1xb2aix4bvdqpi1pj6zinaqqxcgi7gd77wal4")))

(define-public crate-ksp-commnet-calculator-cli-0.1.1 (c (n "ksp-commnet-calculator-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ksp-commnet-calculator-core") (r "^0.1.4") (d #t) (k 0)))) (h "0kd4dmgdryj116jj6gk76vwk7g7f8jiq996b18cfb45klfcfrdcp")))

(define-public crate-ksp-commnet-calculator-cli-0.1.2 (c (n "ksp-commnet-calculator-cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ksp-commnet-calculator-core") (r "^0.1.5") (d #t) (k 0)))) (h "0vxnhxm0zf0yhp7pswizlpiqfyxkwlahrz167cvk7183j6f0rlab")))

