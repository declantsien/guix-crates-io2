(define-module (crates-io ks p- ksp-commnet-calculator-core) #:use-module (crates-io))

(define-public crate-ksp-commnet-calculator-core-0.1.2 (c (n "ksp-commnet-calculator-core") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "0f6h973s55q551xk3b5q1dwlj9vvm9n7p5x02017bd7nvw8cbva2")))

(define-public crate-ksp-commnet-calculator-core-0.1.3 (c (n "ksp-commnet-calculator-core") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "0nsk1q7g5gb01z7mvpblighz8r44zn8dw2fy4rv4zxhj0fcfa9s2")))

(define-public crate-ksp-commnet-calculator-core-0.1.4 (c (n "ksp-commnet-calculator-core") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "00lhk8jp7gm2p4xqh8vcjwb6l1badxyjzybvhcdzyzgy1fkq5gk3")))

(define-public crate-ksp-commnet-calculator-core-0.1.5 (c (n "ksp-commnet-calculator-core") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "1p6vlrx8934xq741g28xv4c6vlmcv7jfcb3kkxllhzplp9dbchm7")))

