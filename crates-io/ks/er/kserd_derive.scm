(define-module (crates-io ks er kserd_derive) #:use-module (crates-io))

(define-public crate-kserd_derive-0.1.0 (c (n "kserd_derive") (v "0.1.0") (d (list (d (n "kserd") (r "^0.1") (k 2)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "proc-macro" "visit"))) (k 0)))) (h "19q9fqv829gyikmns4yn1vygzkqjpa3l9nihn28w55jrza07kad8")))

