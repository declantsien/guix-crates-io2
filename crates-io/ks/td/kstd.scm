(define-module (crates-io ks td kstd) #:use-module (crates-io))

(define-public crate-kstd-0.0.1 (c (n "kstd") (v "0.0.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "0yhswmxld9vc9f9bcfn71jwz5bbn0fi1da2gx2ni10xnp3q29fwv")))

(define-public crate-kstd-0.0.2-rc.1 (c (n "kstd") (v "0.0.2-rc.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "0adfkjay00jbqlh376kjjl02l6zz6h8b2sh0wy0laiqqq1fm82r5")))

(define-public crate-kstd-0.0.2 (c (n "kstd") (v "0.0.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "0fdws402pjibmp47743s7ych7nighqgh95dlbm471dmsx4f3q87c")))

(define-public crate-kstd-0.0.3 (c (n "kstd") (v "0.0.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "05md35dg55j45xh3hk0cx3z6hh5sxn6xpssdnglmpavzqbi5d1m9")))

(define-public crate-kstd-0.0.4-rc.1 (c (n "kstd") (v "0.0.4-rc.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "0hk9833fd19vqd0kcc2q5fh0d07rl3xjfxmrm0hz0plisnx6hvqf")))

(define-public crate-kstd-0.0.4-rc.2 (c (n "kstd") (v "0.0.4-rc.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "1hcn75hw700qaym7ahn7l87h24hpbf2a820ac8p0wq5gqb5m6imj")))

(define-public crate-kstd-0.0.4 (c (n "kstd") (v "0.0.4") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "0mgr46xcyic1ys5blk89wfrq9v45sq44pw1xpk2zvvqljzb1vbll")))

