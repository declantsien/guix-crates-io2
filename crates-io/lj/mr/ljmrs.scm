(define-module (crates-io lj mr ljmrs) #:use-module (crates-io))

(define-public crate-ljmrs-0.0.1 (c (n "ljmrs") (v "0.0.1") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "183g7lby5apwk4zahapgrj2qs0ppwq72kjnhqg4dr3v2n6sm45mf")))

(define-public crate-ljmrs-0.0.2 (c (n "ljmrs") (v "0.0.2") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0nszqr9yn6s09749bhb8k3h6gs7gwdhnsqp8fimi7pmiy8hykrb6")))

(define-public crate-ljmrs-0.0.3 (c (n "ljmrs") (v "0.0.3") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0ygzaaw0drgycqnz5rn0q9rr9k8xwqf3b8m86ng4lfjqwm05k4l9")))

(define-public crate-ljmrs-0.0.4 (c (n "ljmrs") (v "0.0.4") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0cyl2sjnq6waq4sxrwi0fk2klf9yrnpy4fxk6kvnjwx9kny65prm")))

(define-public crate-ljmrs-0.0.5 (c (n "ljmrs") (v "0.0.5") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0x6gzmvyjfff8wfhxazzis3axbiqggwkaq7qjirkhnsq8qgzp1qi")))

(define-public crate-ljmrs-0.0.6 (c (n "ljmrs") (v "0.0.6") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0wbxw682x30r2g5vzif8npnacn2hv4fzskmfca2mpq4yviqsiarg")))

(define-public crate-ljmrs-0.0.7 (c (n "ljmrs") (v "0.0.7") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "16riid0qwl75agibvy3i39ddyr931pp3bfgimjgnh846337qfrcy")))

(define-public crate-ljmrs-0.0.8 (c (n "ljmrs") (v "0.0.8") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0yjv7ipfnl9bajw10q3i5rwky7nbl9nkyqvdj7742s7kqnvb58r8")))

(define-public crate-ljmrs-0.0.9 (c (n "ljmrs") (v "0.0.9") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0fr9vw03sp1mb6yl48ai8y476m1kh96qx4yw35kb7c57zb8zvib7")))

(define-public crate-ljmrs-0.0.10 (c (n "ljmrs") (v "0.0.10") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0ihra2q9s85rs4rywpy4d4xx91djkjzifycfsb57scz3mgi34a18")))

(define-public crate-ljmrs-0.0.11 (c (n "ljmrs") (v "0.0.11") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1w2wadzxzszbw0sggg7nd8fd47js25vqd9br61kcfxcvw2xv0x1l")))

(define-public crate-ljmrs-0.0.12 (c (n "ljmrs") (v "0.0.12") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "15k91dw8jq4f113hsw7zj2qgzqd4xf8gygbsk4vqgwjaakcldhyj")))

(define-public crate-ljmrs-0.0.13 (c (n "ljmrs") (v "0.0.13") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1djypip3ssnhszdav8mx7lj4vx5v8rb1yghh8jwx63rp559gyw3a") (y #t)))

(define-public crate-ljmrs-0.0.15 (c (n "ljmrs") (v "0.0.15") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0cdg517ijhl21j3yi9rkm723v6742ilhllxnsg85g73s76pbg22z")))

(define-public crate-ljmrs-0.0.16 (c (n "ljmrs") (v "0.0.16") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1xjrzm043d0phxkc9adjfgxwbfri23cijiwpd6fg7nz009yp64w7")))

(define-public crate-ljmrs-0.0.17 (c (n "ljmrs") (v "0.0.17") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "022fzqwk73djni5ihgjh7rrrxdfp5wzwx91l3jgqh4zr18l94fhd")))

(define-public crate-ljmrs-0.1.0 (c (n "ljmrs") (v "0.1.0") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0m6v4z33sl0pxwi3fx011pwyc1fym5n88ff7a0nvcg6v8iyks4hv")))

(define-public crate-ljmrs-0.1.1 (c (n "ljmrs") (v "0.1.1") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "00z7ybrbzh4lc3cpk8n366h8wjmi1dqb0n93bkqbi0q2wrgwv62x")))

