(define-module (crates-io lj m- ljm-sys) #:use-module (crates-io))

(define-public crate-ljm-sys-0.1.0 (c (n "ljm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "1b2q1r9v8kh8dywf8ir69rkkp843sbxf1c8fgv2fzh3swm03jaxb")))

(define-public crate-ljm-sys-0.1.1 (c (n "ljm-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "15h474816p7xscgl1fmc38gf1zbvra633rcajls4lfdr4z2n61gn")))

