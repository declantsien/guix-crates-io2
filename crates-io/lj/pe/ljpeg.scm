(define-module (crates-io lj pe ljpeg) #:use-module (crates-io))

(define-public crate-ljpeg-0.1.0 (c (n "ljpeg") (v "0.1.0") (h "0w1b70p5famzx0hvz1g6gshqw4pxa0ix1sp2f2isn32b07p7rlm4") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ljpeg-0.1.1 (c (n "ljpeg") (v "0.1.1") (h "04gj7891ai2ijhjr2lhf8r8qqw3jkc22nv8y8qv0dms53y3zpsjl") (f (quote (("std") ("default" "std"))))))

