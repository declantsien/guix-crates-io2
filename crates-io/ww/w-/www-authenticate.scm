(define-module (crates-io ww w- www-authenticate) #:use-module (crates-io))

(define-public crate-www-authenticate-0.1.0 (c (n "www-authenticate") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10.0") (d #t) (k 0)) (d (n "unicase") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "1jhydvxxybwxca78dgf5n9qc0m90axaisq9ikn70hjcww14f3l0l")))

(define-public crate-www-authenticate-0.2.0 (c (n "www-authenticate") (v "0.2.0") (d (list (d (n "hyper") (r "^0.11.0") (d #t) (k 0)) (d (n "unicase") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1nsiqv516mdyp0mp01izaaq4cw3wgx1jas6x3s5kjiyf8n97q43i")))

(define-public crate-www-authenticate-0.3.0 (c (n "www-authenticate") (v "0.3.0") (d (list (d (n "hyperx") (r "^0.13.0") (d #t) (k 0)) (d (n "unicase") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1p87dhx51j0kwvrxqgn4lkd7hyr305vkk1r2fd64xnlw4nwfyqlc")))

(define-public crate-www-authenticate-0.4.0 (c (n "www-authenticate") (v "0.4.0") (d (list (d (n "hyperx") (r "^1.3") (f (quote ("headers"))) (d #t) (k 0)) (d (n "unicase") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1l18fkkmg9kya700yzb7jw41qcxnqsh9n8jb2119i3axa1q1kz82")))

