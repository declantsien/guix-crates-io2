(define-module (crates-io ww pa wwpasswd) #:use-module (crates-io))

(define-public crate-wwpasswd-1.0.0 (c (n "wwpasswd") (v "1.0.0") (d (list (d (n "base64") (r "^0.8") (d #t) (k 0)) (d (n "dialog") (r "^0.3") (d #t) (k 0)) (d (n "lsx") (r "^1.1") (f (quote ("sha256"))) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0mxch2202xkhzbfcfpc1123v6jy2wqs4571q8002qil3pavzdlv1")))

(define-public crate-wwpasswd-1.0.1 (c (n "wwpasswd") (v "1.0.1") (d (list (d (n "base64") (r "^0.8") (d #t) (k 0)) (d (n "dialog") (r "^0.3") (d #t) (k 0)) (d (n "lsx") (r "^1.1") (f (quote ("sha256"))) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "05n2clffjmd3y1h25gn1d5m95akvkfrg9rk4pyamnb6i285r8jrw")))

