(define-module (crates-io ww -m ww-macro) #:use-module (crates-io))

(define-public crate-ww-macro-0.0.1 (c (n "ww-macro") (v "0.0.1") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)))) (h "1avd16xqwwsnnqayj13z6im7xjy15ar782sy7xkv5kglmi7iwjlm")))

