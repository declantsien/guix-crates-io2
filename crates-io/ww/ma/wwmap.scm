(define-module (crates-io ww ma wwmap) #:use-module (crates-io))

(define-public crate-wwmap-0.1.0 (c (n "wwmap") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cidr-utils") (r "^0.5.7") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert-base") (r "^1.1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0v3i0xwx2s81wxga86k809dmqvwq1bmj7m3acvq9mgm05l7jbxxf")))

