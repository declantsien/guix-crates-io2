(define-module (crates-io ww sv wwsvc-rs-derive) #:use-module (crates-io))

(define-public crate-wwsvc-rs-derive-3.0.0-pre.14 (c (n "wwsvc-rs-derive") (v "3.0.0-pre.14") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1b43y2niblyn82fb3jqpmpysy0xy2ps78bd9r14snb6ya3dmd0nw")))

(define-public crate-wwsvc-rs-derive-3.0.0-pre.15 (c (n "wwsvc-rs-derive") (v "3.0.0-pre.15") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0vj5hf8v1yn2w5djjz899rs2mqpjbbjlkl76x7zdl5hlmdqscl3z")))

(define-public crate-wwsvc-rs-derive-3.0.0-pre.16 (c (n "wwsvc-rs-derive") (v "3.0.0-pre.16") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0sqaygs8j1ih0j5bypwvicz0pvwphzgiicssk6xjak7ln4szim8c")))

(define-public crate-wwsvc-rs-derive-3.0.0-pre.17 (c (n "wwsvc-rs-derive") (v "3.0.0-pre.17") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1fhrxcsm7jgzvq056zmd89r7flycrdnqb7ahwcc9zm5c9jqpc2a7")))

(define-public crate-wwsvc-rs-derive-3.0.0-pre.18 (c (n "wwsvc-rs-derive") (v "3.0.0-pre.18") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0y7szi4k2fca0z871bfbm2z6fgh5ijhvrsgn56w0mbjv2qkn2ql6")))

(define-public crate-wwsvc-rs-derive-3.0.0-pre.19 (c (n "wwsvc-rs-derive") (v "3.0.0-pre.19") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "1bbhpi152cl4r6r8ld2dj3d9pa5vxz8g67ryn9injmcfxp1612hm") (r "1.74.0")))

(define-public crate-wwsvc-rs-derive-3.0.0-pre.20 (c (n "wwsvc-rs-derive") (v "3.0.0-pre.20") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "1bkjha5jpmljvznb1ig2f607d6qjppfrbb826lkr1agjqrnfvldk") (r "1.74.0")))

(define-public crate-wwsvc-rs-derive-3.0.0 (c (n "wwsvc-rs-derive") (v "3.0.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "1q5438jgyhdga4vig4cwzx2vvxd5xffg52hkqbrv259pbv16d74q") (r "1.74.0")))

(define-public crate-wwsvc-rs-derive-3.1.0 (c (n "wwsvc-rs-derive") (v "3.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "0rkcpmbgc629wdm9qgs68izhgz6mjrh684rnsq5kc52vh8y2vsbw") (r "1.74.0")))

(define-public crate-wwsvc-rs-derive-3.1.1 (c (n "wwsvc-rs-derive") (v "3.1.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "0knig2cdq6g0j10lgjcas6p3aik9s2cqr25jcwlwgc97sysd0mc3") (r "1.74.0")))

(define-public crate-wwsvc-rs-derive-3.1.2 (c (n "wwsvc-rs-derive") (v "3.1.2") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "0rm2wwzyl3zcr8xsw38xrj6pzxc6qsz68lzwks63m02bb22nyg97") (r "1.74.0")))

(define-public crate-wwsvc-rs-derive-3.1.3 (c (n "wwsvc-rs-derive") (v "3.1.3") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "1shws2j21g9w3wlgrrbzv36awgc88bgm78cgvlg3937lgkr4232s") (r "1.74.0")))

(define-public crate-wwsvc-rs-derive-3.1.4 (c (n "wwsvc-rs-derive") (v "3.1.4") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "0n1rngd15pbzxva4vdvi13shbssgq9cibdym0vi4xahqg15fvmb8") (r "1.74.0")))

