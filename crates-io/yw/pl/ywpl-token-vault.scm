(define-module (crates-io yw pl ywpl-token-vault) #:use-module (crates-io))

(define-public crate-ywpl-token-vault-0.1.1 (c (n "ywpl-token-vault") (v "0.1.1") (d (list (d (n "borsh") (r "~0.9.1") (d #t) (k 0)) (d (n "num-derive") (r "~0.3") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "shank") (r "~0.0.1") (d #t) (k 0)) (d (n "solana-program") (r "~1.9.15") (d #t) (k 0)) (d (n "spl-token") (r "~3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)))) (h "1w8a8fp95qf6lisn3g7xlrma0vk6vkq8zpy45gnirshhqnrz57kq") (f (quote (("test-bpf") ("no-entrypoint"))))))

