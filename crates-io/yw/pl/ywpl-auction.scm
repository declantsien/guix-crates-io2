(define-module (crates-io yw pl ywpl-auction) #:use-module (crates-io))

(define-public crate-ywpl-auction-0.0.2 (c (n "ywpl-auction") (v "0.0.2") (d (list (d (n "arrayref") (r "~0.3.6") (d #t) (k 0)) (d (n "borsh") (r "~0.9.1") (d #t) (k 0)) (d (n "num-derive") (r "~0.3") (d #t) (k 0)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "solana-program") (r "~1.9.5") (d #t) (k 0)) (d (n "solana-program-test") (r "~1.9.5") (d #t) (k 2)) (d (n "solana-sdk") (r "~1.9.5") (d #t) (k 2)) (d (n "spl-token") (r "~3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)))) (h "11cnjr0f64135g504l4ch6ycfg61x7iqjg827ld3wrl8gki5cizh") (f (quote (("test-bpf") ("no-entrypoint"))))))

