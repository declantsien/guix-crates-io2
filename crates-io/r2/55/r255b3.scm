(define-module (crates-io r2 #{55}# r255b3) #:use-module (crates-io))

(define-public crate-r255b3-0.1.0 (c (n "r255b3") (v "0.1.0") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^4.0.0-rc.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "subtle") (r "^2.4.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 0)))) (h "1f32v1j9lg4jxbnnwc9jajp764p8kzmgxgm75acilrjrr745sj6y")))

(define-public crate-r255b3-0.1.1 (c (n "r255b3") (v "0.1.1") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "subtle") (r "^2.4.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "0vddda14znaafjj4rlra78b55nq2cmw4m7iqmrdzbiapnq3mmvnj") (f (quote (("default" "zeroize")))) (s 2) (e (quote (("zeroize" "dep:zeroize" "curve25519-dalek/zeroize"))))))

(define-public crate-r255b3-0.2.0 (c (n "r255b3") (v "0.2.0") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "subtle") (r "^2.4.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "0ff7r77vcs06h8sjlp953d4ya05vraxwlml2lxgvzf2fwqwjl1vw") (f (quote (("default" "zeroize")))) (s 2) (e (quote (("zeroize" "dep:zeroize" "curve25519-dalek/zeroize"))))))

(define-public crate-r255b3-0.3.0 (c (n "r255b3") (v "0.3.0") (d (list (d (n "blake3") (r "^1.3.3") (k 0)) (d (n "curve25519-dalek") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "subtle") (r "^2.4.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "0nxifyy42vvy71i1hzqfz9qp484cj1v92x5lzbwxva50hwvlv8i4") (f (quote (("std" "blake3/std") ("default" "zeroize" "std")))) (s 2) (e (quote (("zeroize" "dep:zeroize" "curve25519-dalek/zeroize"))))))

