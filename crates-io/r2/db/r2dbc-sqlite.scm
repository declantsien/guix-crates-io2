(define-module (crates-io r2 db r2dbc-sqlite) #:use-module (crates-io))

(define-public crate-r2dbc-sqlite-0.0.1 (c (n "r2dbc-sqlite") (v "0.0.1") (d (list (d (n "fallible-streaming-iterator") (r "^0.1.9") (d #t) (k 0)) (d (n "r2dbc") (r "^0.0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0q8r9lk66686ji4196pijkr47jywzm4qxm05bwp70vri5wf6jsz3")))

