(define-module (crates-io r2 db r2dbc-cli) #:use-module (crates-io))

(define-public crate-r2dbc-cli-0.0.1 (c (n "r2dbc-cli") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "r2dbc") (r "^0.0.1") (d #t) (k 0)) (d (n "r2dbc-mysql") (r "^0.0.1") (d #t) (k 0)) (d (n "r2dbc-postgres") (r "^0.0.1") (d #t) (k 0)) (d (n "r2dbc-sqlite") (r "^0.0.1") (d #t) (k 0)))) (h "198c4xyi8cp930q2bvq99nmh3mqbkwpnmfwf1fxln7jnfaj0y4d9")))

