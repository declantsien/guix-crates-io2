(define-module (crates-io r2 db r2dbc-postgres) #:use-module (crates-io))

(define-public crate-r2dbc-postgres-0.0.1 (c (n "r2dbc-postgres") (v "0.0.1") (d (list (d (n "postgres") (r "^0.19.0") (d #t) (k 0)) (d (n "r2dbc") (r "^0.0.1") (d #t) (k 0)) (d (n "sqlparser") (r "^0.9.0") (d #t) (k 0)))) (h "12i1gbl5d22ikib2v4sxrr40hn25686i1yyfdm6x1ha2hvra2di4")))

