(define-module (crates-io r2 db r2dbc-mysql) #:use-module (crates-io))

(define-public crate-r2dbc-mysql-0.0.1 (c (n "r2dbc-mysql") (v "0.0.1") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.26.0") (d #t) (k 0)) (d (n "r2dbc") (r "^0.0.1") (d #t) (k 0)) (d (n "sqlparser") (r "^0.9.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.0") (d #t) (k 0)))) (h "15jg1s9dfjmfmd2xsghrr3jqwpmhsmzn1mhc2bgydfr4ckc757ys")))

