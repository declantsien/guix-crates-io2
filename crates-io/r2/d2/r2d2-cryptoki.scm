(define-module (crates-io r2 d2 r2d2-cryptoki) #:use-module (crates-io))

(define-public crate-r2d2-cryptoki-0.1.0 (c (n "r2d2-cryptoki") (v "0.1.0") (d (list (d (n "backoff") (r "^0.4.0") (d #t) (k 2)) (d (n "cached") (r "^0.42.0") (d #t) (k 2)) (d (n "cryptoki") (r "^0.4.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.6") (d #t) (k 2)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "19qbggw3dp09pn5blxd9im8lypry2f6jlmfb4rc98b7lbx5mcspc") (f (quote (("serde" "zeroize/serde"))))))

(define-public crate-r2d2-cryptoki-0.2.0 (c (n "r2d2-cryptoki") (v "0.2.0") (d (list (d (n "backoff") (r "^0.4.0") (d #t) (k 2)) (d (n "cached") (r "^0.44.0") (d #t) (k 2)) (d (n "cryptoki") (r "^0.5.0") (d #t) (k 0)) (d (n "loom") (r "^0.7.0") (d #t) (k 2)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)))) (h "11hag0ap4lyqhmnigc3a1xrgyzypp631wafy721yxga4ml86mp47") (f (quote (("serde" "cryptoki/serde"))))))

(define-public crate-r2d2-cryptoki-0.2.1 (c (n "r2d2-cryptoki") (v "0.2.1") (d (list (d (n "backoff") (r "^0.4.0") (d #t) (k 2)) (d (n "cached") (r "^0.46.0") (d #t) (k 2)) (d (n "cryptoki") (r "^0.6.0") (d #t) (k 0)) (d (n "loom") (r "^0.7.1") (d #t) (k 2)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)))) (h "0h4jmdlpax1ra8zzvvgn6wd3q5nsnbcn5djkhzprwvd4whj7m9hv") (f (quote (("serde" "cryptoki/serde"))))))

