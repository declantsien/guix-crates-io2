(define-module (crates-io r2 d2 r2d2_mysql_batis) #:use-module (crates-io))

(define-public crate-r2d2_mysql_batis-0.1.0 (c (n "r2d2_mysql_batis") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "r2d2_mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "r2d2_mysql_batis_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "096h1l4b6cqwyswjjayz94y33vxd6497947irgpkxdjnk2gqaan2") (y #t)))

(define-public crate-r2d2_mysql_batis-0.1.1 (c (n "r2d2_mysql_batis") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "r2d2_mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "r2d2_mysql_batis_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1yix2h793lna9damxxsr5p2i0c1fqr6fqzw1h1irfma92dyzs7sz") (y #t)))

(define-public crate-r2d2_mysql_batis-0.1.2 (c (n "r2d2_mysql_batis") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "r2d2_mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "r2d2_mysql_batis_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0ar9w154la2pm9g3nm8y7zm35gr7hzc99c69qlfkl432h6z57j2k")))

(define-public crate-r2d2_mysql_batis-0.1.3 (c (n "r2d2_mysql_batis") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "r2d2_mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "r2d2_mysql_batis_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0did65p48bzx2kzhvsgww5k279bsrcahwq73jwma9fq0mmd2xvxm")))

(define-public crate-r2d2_mysql_batis-0.1.4 (c (n "r2d2_mysql_batis") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "r2d2_mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "r2d2_mysql_batis_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "11d75ia3gxyyi8q3bhkvqafbg0byyba71hsc44n84kc27042wrki")))

