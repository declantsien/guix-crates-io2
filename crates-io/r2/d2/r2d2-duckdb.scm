(define-module (crates-io r2 d2 r2d2-duckdb) #:use-module (crates-io))

(define-public crate-r2d2-duckdb-0.1.0 (c (n "r2d2-duckdb") (v "0.1.0") (d (list (d (n "duckdb") (r "^0.8.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "12cj2hmm8q8ji7pxg5s6khhzhm884v844b37hr4yxhr6ca4mc6ds")))

