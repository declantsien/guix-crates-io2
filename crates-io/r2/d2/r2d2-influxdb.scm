(define-module (crates-io r2 d2 r2d2-influxdb) #:use-module (crates-io))

(define-public crate-r2d2-influxdb-0.1.0 (c (n "r2d2-influxdb") (v "0.1.0") (d (list (d (n "influxdb") (r "^0.0.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "128szi3m6i1y6c284paq63fhwgkr6j4x9jp7i7h8g6saz43mbrag")))

