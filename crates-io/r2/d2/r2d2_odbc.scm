(define-module (crates-io r2 d2 r2d2_odbc) #:use-module (crates-io))

(define-public crate-r2d2_odbc-0.1.0 (c (n "r2d2_odbc") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "odbc") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "0yhj19krql5nflhm9ya3abdad8cxag9rfjcrcszv7a07jx2hvac9")))

(define-public crate-r2d2_odbc-0.2.0 (c (n "r2d2_odbc") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "odbc") (r "^0.10") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "0g08lsjs9llliraj6bld6hgv5qd7my5827lxqi5fcnf9i8mkyh51")))

(define-public crate-r2d2_odbc-0.3.0 (c (n "r2d2_odbc") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "odbc") (r "^0.14") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "1hvz5gd4j8aga0why9jan64j7bbf867v9ibdjrmw3qcraca5rcxm")))

(define-public crate-r2d2_odbc-0.4.0 (c (n "r2d2_odbc") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "odbc") (r "^0.15") (d #t) (k 0)) (d (n "odbc-safe") (r "^0.4.2") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "1h8hgbzwx6sscr1iqk9kf0dy7wcglqb5ifnpnkcs3ab49spl80lv")))

(define-public crate-r2d2_odbc-0.5.0 (c (n "r2d2_odbc") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "odbc") (r "^0.16") (d #t) (k 0)) (d (n "odbc-safe") (r "^0.5") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "1py8a3jrgn8vrw60dambf2x4k5cqgg80pszlg0w6m4vnvs2vfcfw")))

