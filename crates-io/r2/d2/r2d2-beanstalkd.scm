(define-module (crates-io r2 d2 r2d2-beanstalkd) #:use-module (crates-io))

(define-public crate-r2d2-beanstalkd-0.1.0 (c (n "r2d2-beanstalkd") (v "0.1.0") (d (list (d (n "beanstalkc") (r "^1.0.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1gzg7lq6f27ad25z17ib1k2qcg1a6jkvmcmd961f748gmq622p1h")))

