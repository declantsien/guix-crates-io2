(define-module (crates-io r2 d2 r2d2_arangors) #:use-module (crates-io))

(define-public crate-r2d2_arangors-0.1.0 (c (n "r2d2_arangors") (v "0.1.0") (d (list (d (n "arangors") (r "^0.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)))) (h "0l527ciqpi22371kq6f1hwdqkvzk5jhpbpw997hxmzr8h1mqj5ll")))

(define-public crate-r2d2_arangors-0.2.0 (c (n "r2d2_arangors") (v "0.2.0") (d (list (d (n "arangors") (r "^0.3") (f (quote ("reqwest_blocking"))) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)))) (h "1mcgr0yr1l2gx7dnwm8h9swh9kgsdrm7fjwz1x36q0zhp4qs75l2")))

(define-public crate-r2d2_arangors-0.2.1 (c (n "r2d2_arangors") (v "0.2.1") (d (list (d (n "arangors") (r ">=0.3") (f (quote ("reqwest_blocking"))) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)))) (h "0d2vvjfm4s8zz9db8xsyv3bsikidjx9bvz5biyf1lb8iwfjffsih")))

(define-public crate-r2d2_arangors-0.2.2 (c (n "r2d2_arangors") (v "0.2.2") (d (list (d (n "arangors") (r ">=0.4.3") (f (quote ("reqwest_blocking"))) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)))) (h "1qw7cnhwc8ms23lc3rhck02gyqf02jk1fm1n5062xnnln2lm93iq")))

(define-public crate-r2d2_arangors-0.2.3 (c (n "r2d2_arangors") (v "0.2.3") (d (list (d (n "arangors") (r "^0.5") (f (quote ("reqwest_blocking"))) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "uclient") (r "^0.1") (f (quote ("blocking_reqwest"))) (k 0)))) (h "0b2srshxgl02mhyz77siyx3hzq07dsi1dbwpv78i1p0zxbrnbjw0")))

