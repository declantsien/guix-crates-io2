(define-module (crates-io r2 d2 r2d2_sqlite_pool) #:use-module (crates-io))

(define-public crate-r2d2_sqlite_pool-0.1.0 (c (n "r2d2_sqlite_pool") (v "0.1.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0a7qkci1d37r4sgi0zx8rrngqk4yy30n6jrkjd1526pz1plqrayz") (f (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled")))) (y #t)))

(define-public crate-r2d2_sqlite_pool-0.1.1 (c (n "r2d2_sqlite_pool") (v "0.1.1") (d (list (d (n "nanoid") (r "^0") (d #t) (k 0)) (d (n "r2d2") (r "^0") (d #t) (k 0)) (d (n "rusqlite") (r "^0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "08lpmy5vyxjp7j0rcgg3mlc84fc2pwdbl6sj2syl0r5749qbj4f2") (f (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

