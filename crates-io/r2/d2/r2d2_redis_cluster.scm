(define-module (crates-io r2 d2 r2d2_redis_cluster) #:use-module (crates-io))

(define-public crate-r2d2_redis_cluster-0.1.0 (c (n "r2d2_redis_cluster") (v "0.1.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)) (d (n "redis_cluster_rs") (r "^0.1") (d #t) (k 0)))) (h "04x6mm3y9b983pv87ndg3zrmhf8ia6xbn44w7z4a37v62bl4w4jh")))

(define-public crate-r2d2_redis_cluster-0.1.1 (c (n "r2d2_redis_cluster") (v "0.1.1") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.9") (d #t) (k 0)) (d (n "redis_cluster_rs") (r "^0.1") (d #t) (k 0)))) (h "0njm99g62sl7r9wva641srlxz6gavwnc3fa5jmsbda0qy3h9yan3")))

(define-public crate-r2d2_redis_cluster-0.1.2 (c (n "r2d2_redis_cluster") (v "0.1.2") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis_cluster_rs") (r "^0.1") (d #t) (k 0)))) (h "00aw9p0zhnyxmx52par3py3jak2f7msa0dqbcnwphiik1pp9jq0a")))

(define-public crate-r2d2_redis_cluster-0.1.3 (c (n "r2d2_redis_cluster") (v "0.1.3") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis_cluster_rs") (r "^0.1.6") (d #t) (k 0)))) (h "0vldd58wv2kmb2gm2xb54z9nrcah6p90rqbplkj7n5pi25qbs1lm")))

(define-public crate-r2d2_redis_cluster-0.1.4 (c (n "r2d2_redis_cluster") (v "0.1.4") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis_cluster_rs") (r "^0.1") (d #t) (k 0)))) (h "1sl5g3jlpwb4i5l4igxmiyqb2r5s15flk86vnci035zs05cfyn1r")))

(define-public crate-r2d2_redis_cluster-0.1.5 (c (n "r2d2_redis_cluster") (v "0.1.5") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis_cluster_rs") (r "^0.1") (d #t) (k 0)))) (h "17w3d95ads9gv5lk70nn8ik28vp12jj8c27l47v9av2kqdrg3cjm")))

(define-public crate-r2d2_redis_cluster-0.1.6 (c (n "r2d2_redis_cluster") (v "0.1.6") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis_cluster_rs") (r "^0.1") (d #t) (k 0)))) (h "01i9926r76q0mqqdfjw27y8j6ammx899wqx9bz2lrbk8ydn22n04")))

