(define-module (crates-io r2 d2 r2d2_mysql) #:use-module (crates-io))

(define-public crate-r2d2_mysql-0.1.1 (c (n "r2d2_mysql") (v "0.1.1") (d (list (d (n "mysql") (r "^0.15.0") (k 0)) (d (n "r2d2") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0n8vy31ajmap6kq6349fpb0783akv3m2mmbjlbgkadmkfgv5wj1i")))

(define-public crate-r2d2_mysql-0.2.0 (c (n "r2d2_mysql") (v "0.2.0") (d (list (d (n "mysql") (r "^0.16.0") (k 0)) (d (n "r2d2") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "14l36pcm6622lhbkhlqzk3z6hl3225cg39qzdf23ami67vkhaaan")))

(define-public crate-r2d2_mysql-0.2.1 (c (n "r2d2_mysql") (v "0.2.1") (d (list (d (n "mysql") (r "^0.17.2") (k 0)) (d (n "r2d2") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1azh4xandw5y96mv473b1d5rargzdy31ndkqvin4yaz6k6ww88cl")))

(define-public crate-r2d2_mysql-0.2.2 (c (n "r2d2_mysql") (v "0.2.2") (d (list (d (n "mysql") (r "~0.18.0") (k 0)) (d (n "r2d2") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.0") (d #t) (k 0)))) (h "1k4dk76a4p5nap15xd21fpy94192cjz4skb7jjl1wnff3i8d318n")))

(define-public crate-r2d2_mysql-2.2.0 (c (n "r2d2_mysql") (v "2.2.0") (d (list (d (n "mysql") (r "~2.2.0") (k 0)) (d (n "r2d2") (r "~0.6.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.16") (d #t) (k 0)))) (h "1a1raxbq4v0m3d5xzm8p4p82ifmhgfx3bwqhzz9bm2g6q6w7pbf8")))

(define-public crate-r2d2_mysql-2.2.1 (c (n "r2d2_mysql") (v "2.2.1") (d (list (d (n "mysql") (r "^2.2") (k 0)) (d (n "r2d2") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.16") (d #t) (k 0)))) (h "1rc0xnb4h30ljgc89y8zc0bwl2ri3libvkdrd3lp9gxab0x3vm52")))

(define-public crate-r2d2_mysql-3.0.0 (c (n "r2d2_mysql") (v "3.0.0") (d (list (d (n "mysql") (r "^3.0") (k 0)) (d (n "r2d2") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.16") (d #t) (k 0)))) (h "1dcyml9mlggffyffih056gl487l97znsqrnbj9w1vhmnmxipp4sj")))

(define-public crate-r2d2_mysql-7.1.0 (c (n "r2d2_mysql") (v "7.1.0") (d (list (d (n "mysql") (r "^7.1.2") (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.19") (d #t) (k 0)))) (h "0dx61v5n8drqagky6bzy3fx2kcfvc7ij311mzwcw77rqlxv2h5gn")))

(define-public crate-r2d2_mysql-8.0.0 (c (n "r2d2_mysql") (v "8.0.0") (d (list (d (n "mysql") (r "^8.0.0") (k 0)) (d (n "r2d2") (r "^0.7.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.22") (d #t) (k 0)))) (h "01p56n6vrsgklzspjn1ljgwx7vwngw53hb9aw9rv0qjwbnyz0k43")))

(define-public crate-r2d2_mysql-9.0.0 (c (n "r2d2_mysql") (v "9.0.0") (d (list (d (n "mysql") (r "^14") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1gfaabgk51nyg4pi1759bjjq0hdd5mvvcbwjr5kr1l0xx1afw76k")))

(define-public crate-r2d2_mysql-15.0.0 (c (n "r2d2_mysql") (v "15.0.0") (d (list (d (n "mysql") (r "^15") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "0iz8971dj9q4ac7s5d7cwr9znhxycyprbf9qnkkizv1v111p3ghq")))

(define-public crate-r2d2_mysql-16.0.0 (c (n "r2d2_mysql") (v "16.0.0") (d (list (d (n "mysql") (r "^16") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "055lqp1hgbs2i2q3bfrlkp79dk67kxi18smpm0q0v64754g9skgp")))

(define-public crate-r2d2_mysql-17.0.0 (c (n "r2d2_mysql") (v "17.0.0") (d (list (d (n "mysql") (r "^17") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)))) (h "0zwnx3bqvfvkfr40135vh4cv6y4jjj3063f0zq0pss194q31cxmv")))

(define-public crate-r2d2_mysql-18.0.0 (c (n "r2d2_mysql") (v "18.0.0") (d (list (d (n "mysql") (r "^18") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)))) (h "0bi5kb5srvcqs91f69hfinmvvkn5a1b7ak7pzlzqcrpyq93c6f10")))

(define-public crate-r2d2_mysql-20.0.0 (c (n "r2d2_mysql") (v "20.0.0") (d (list (d (n "mysql") (r "^20") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)))) (h "0i93il547r509s5764rc0ncq0klqkp6c8wdxd7imnvsdy31c7j04")))

(define-public crate-r2d2_mysql-21.0.0 (c (n "r2d2_mysql") (v "21.0.0") (d (list (d (n "mysql") (r "^21") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)))) (h "1qm9qg1mal8p55qyy8zl032jk52biwmj1zzy2mamyf8bd52m3l51")))

(define-public crate-r2d2_mysql-22.0.0 (c (n "r2d2_mysql") (v "22.0.0") (d (list (d (n "mysql") (r "^22") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)))) (h "0fz7wsmsbigyr6abkfzhd015q87jbycrsjjhl3f9kp70ga7dms7b")))

(define-public crate-r2d2_mysql-23.0.0 (c (n "r2d2_mysql") (v "23.0.0") (d (list (d (n "mysql") (r "^23") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)))) (h "0xf5n33ppi3in63593nl31z8c2k9m9lswyrq9xs9m5b5rqwdfcwp")))

(define-public crate-r2d2_mysql-24.0.0 (c (n "r2d2_mysql") (v "24.0.0") (d (list (d (n "mysql") (r "^24") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)))) (h "0x15yljhqfbdrb1kc446xck2573dpr6gbwl0jpdir5r1diz15r9z") (r "1.65")))

