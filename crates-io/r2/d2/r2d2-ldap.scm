(define-module (crates-io r2 d2 r2d2-ldap) #:use-module (crates-io))

(define-public crate-r2d2-ldap-0.1.0 (c (n "r2d2-ldap") (v "0.1.0") (d (list (d (n "ldap3") (r "^0.8") (f (quote ("sync"))) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "154qh8fn4vlzsq1f788w5pz7c2lpvf51p0jy12im8mdjxqqz0i1r")))

(define-public crate-r2d2-ldap-0.1.1 (c (n "r2d2-ldap") (v "0.1.1") (d (list (d (n "ldap3") (r "^0.8") (f (quote ("sync"))) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "0vxjn1k2l19dqdghk6r2lyswckm6v59ip05l9xf91ggrf3n7g304")))

