(define-module (crates-io r2 d2 r2d2-memcache) #:use-module (crates-io))

(define-public crate-r2d2-memcache-0.1.0 (c (n "r2d2-memcache") (v "0.1.0") (d (list (d (n "memcache") (r "^0.0.14") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)))) (h "018w4f5rbfpmis0lx3iv4c6laa6w4xv788vggwjcjaqc3vrkq5db")))

(define-public crate-r2d2-memcache-0.1.1 (c (n "r2d2-memcache") (v "0.1.1") (d (list (d (n "memcache") (r "^0.7") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)))) (h "0if09wzqqbhxq4vgvh3274dgc0hf9nvfwcacg7slljsyx6qxgyf0")))

(define-public crate-r2d2-memcache-0.1.2 (c (n "r2d2-memcache") (v "0.1.2") (d (list (d (n "memcache") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)))) (h "0slp57qsmwajvzh0qyjfcaz3pf1c157k5zar70ir0wa2z2zmfrq7")))

(define-public crate-r2d2-memcache-0.2.0 (c (n "r2d2-memcache") (v "0.2.0") (d (list (d (n "memcache") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "0ya8yh2z88ypq8bb1mjxy8al0gjadjif9pa15j28scfkl3j5lxbl")))

(define-public crate-r2d2-memcache-0.2.1 (c (n "r2d2-memcache") (v "0.2.1") (d (list (d (n "memcache") (r "^0.10") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "1n72zi06b9ff5ky37n76hjf33pgrll1rmz9d11vhsj3qjmky2pgr")))

(define-public crate-r2d2-memcache-0.3.0 (c (n "r2d2-memcache") (v "0.3.0") (d (list (d (n "memcache") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "0g84qlymwq9k56xrfvg0jf8blw9dd5pjinxk1qgnf6lh315vb6kf")))

(define-public crate-r2d2-memcache-0.3.1 (c (n "r2d2-memcache") (v "0.3.1") (d (list (d (n "memcache") (r "^0.13.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "13wwgg4md2f2pr85pm06ll17sqbgdd2jv6g16nx4p5hmw6r6hgi3")))

(define-public crate-r2d2-memcache-0.3.2 (c (n "r2d2-memcache") (v "0.3.2") (d (list (d (n "memcache") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "07kawqxf8m9akgpa4fjn8xpn7qzw8pfxgp28v2mwlxk7imrvshx9")))

(define-public crate-r2d2-memcache-0.4.0 (c (n "r2d2-memcache") (v "0.4.0") (d (list (d (n "memcache") (r "^0.13") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "0f7gqlmrxhpm6l4i0liyhfywm2yk1sf62ijvz7qh2bfnhd2ilhk5")))

(define-public crate-r2d2-memcache-0.5.0 (c (n "r2d2-memcache") (v "0.5.0") (d (list (d (n "memcache") (r "^0.14") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "17zr81vhvlr818jn11dfmgw5ls7f5vzrfpw2qcwpynnqxd63852i")))

(define-public crate-r2d2-memcache-0.6.0 (c (n "r2d2-memcache") (v "0.6.0") (d (list (d (n "memcache") (r "^0.15") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "0h7n37c5fsvdfvj9arhqsvn1acp2vayy9wbbzszhj6d18jydgfbl")))

