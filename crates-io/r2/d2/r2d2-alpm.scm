(define-module (crates-io r2 d2 r2d2-alpm) #:use-module (crates-io))

(define-public crate-r2d2-alpm-0.1.0 (c (n "r2d2-alpm") (v "0.1.0") (d (list (d (n "alpm") (r "^2.2") (d #t) (k 0)) (d (n "alpm-utils") (r "^2.0") (d #t) (k 0)) (d (n "pacmanconf") (r "^2.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "0nhp63igjix1cvkfpvx620sfvgp2krk1f752k62nywsli7dk5a22")))

(define-public crate-r2d2-alpm-0.2.0 (c (n "r2d2-alpm") (v "0.2.0") (d (list (d (n "alpm") (r "^3.0") (d #t) (k 0)) (d (n "alpm-utils") (r "^3.0") (d #t) (k 0)) (d (n "pacmanconf") (r "^2.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 2)))) (h "1i9j3dr5i3d7fqm9bpqd7jx8jgr0131kqsyyjnggd3yd7v4ilw69")))

