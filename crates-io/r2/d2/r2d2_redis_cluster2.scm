(define-module (crates-io r2 d2 r2d2_redis_cluster2) #:use-module (crates-io))

(define-public crate-r2d2_redis_cluster2-0.23.3 (c (n "r2d2_redis_cluster2") (v "0.23.3") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis_cluster_rs2") (r "^0.23.3") (d #t) (k 0)))) (h "1fz1d6qn302pvwlp06psm926ngwpbgvjg2yq9lwn1fn8iwr7n33f")))

