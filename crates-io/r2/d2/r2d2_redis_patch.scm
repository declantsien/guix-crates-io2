(define-module (crates-io r2 d2 r2d2_redis_patch) #:use-module (crates-io))

(define-public crate-r2d2_redis_patch-0.6.1 (c (n "r2d2_redis_patch") (v "0.6.1") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1x3ip72qchsgp1kvj5q3f46c9pa0f1gavx71cad2xsfmdfvzcqi1")))

