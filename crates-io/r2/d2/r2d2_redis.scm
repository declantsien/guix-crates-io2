(define-module (crates-io r2 d2 r2d2_redis) #:use-module (crates-io))

(define-public crate-r2d2_redis-0.0.2 (c (n "r2d2_redis") (v "0.0.2") (d (list (d (n "r2d2") (r "^0.5") (d #t) (k 0)) (d (n "redis") (r "^0.3.1") (d #t) (k 0)))) (h "1zlridw9sj77prm49c19399f4c8p549ykr1b62w133gvblqyjyal")))

(define-public crate-r2d2_redis-0.1.0 (c (n "r2d2_redis") (v "0.1.0") (d (list (d (n "r2d2") (r "^0.6") (d #t) (k 0)) (d (n "redis") (r "^0.3.1") (d #t) (k 0)))) (h "194arh4zix4nhsbslffjc0ayj6jn3rd4mkvln4jix6k876b0953s")))

(define-public crate-r2d2_redis-0.2.0 (c (n "r2d2_redis") (v "0.2.0") (d (list (d (n "r2d2") (r "^0.6") (d #t) (k 0)) (d (n "redis") (r "^0.5") (d #t) (k 0)))) (h "1axvgh0dc01qas5098r0pcpxjhpzpx4pjwkxfrc4yhvy0q6ka0fn")))

(define-public crate-r2d2_redis-0.3.0 (c (n "r2d2_redis") (v "0.3.0") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.5") (d #t) (k 0)))) (h "0wyjzfxm8721i93lhh3gwjxyki7abbl4kd1633f6jamg8zv9qpj4")))

(define-public crate-r2d2_redis-0.3.1 (c (n "r2d2_redis") (v "0.3.1") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.5") (d #t) (k 0)))) (h "12wlfcrv4rc0mbbskj44r9lxh98mqmsgpdx45mvyamnqqyma4hq8")))

(define-public crate-r2d2_redis-0.4.0 (c (n "r2d2_redis") (v "0.4.0") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.6") (d #t) (k 0)))) (h "0ai9h1knzzdqv222b0sn01wr2p569fnp8g4dibksphqwpxrgbacq")))

(define-public crate-r2d2_redis-0.5.0 (c (n "r2d2_redis") (v "0.5.0") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.7") (d #t) (k 0)))) (h "01jrfxnjaxj6xjjif0pz2dacvwk0ppisa94wqs57v0ijghxw3ji4")))

(define-public crate-r2d2_redis-0.5.1 (c (n "r2d2_redis") (v "0.5.1") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.7.1") (d #t) (k 0)))) (h "1hr4f5bwij4ff81jb3f89hgaxb2w56n4d0hb3zbdakrg413n9nr7")))

(define-public crate-r2d2_redis-0.6.0 (c (n "r2d2_redis") (v "0.6.0") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1vvn5a6n0bik7lb3nl1l2ml3z450xfd7wf4nl29ch7ispv9si68l")))

(define-public crate-r2d2_redis-0.7.0 (c (n "r2d2_redis") (v "0.7.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1i7mlw0qn7wd9y3w35gwkn835gsm9xc76qgbvxqxbgzsylnq801v")))

(define-public crate-r2d2_redis-0.8.0 (c (n "r2d2_redis") (v "0.8.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.9") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1nxramcqfkn7ghl6i1pgvirlyyfvmrsyhm6dhkh90jazpf88lccq")))

(define-public crate-r2d2_redis-0.9.0 (c (n "r2d2_redis") (v "0.9.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.10") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1pfmg95rlnqzdxqw37f35wmcwsnfjk34vy8daw7ijxdfhap0ll2l")))

(define-public crate-r2d2_redis-0.10.0 (c (n "r2d2_redis") (v "0.10.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.11") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1v938c03d5xbha60h7mxih7jhvpf4zk8daxa4l9i9xk5xa1pjada") (y #t)))

(define-public crate-r2d2_redis-0.10.1 (c (n "r2d2_redis") (v "0.10.1") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.11") (d #t) (k 0)))) (h "08iknxs8l13nyvlbi6j5zwnpjcs4kxv5pw7mrzbfmy4ai5vnnlwx")))

(define-public crate-r2d2_redis-0.11.0 (c (n "r2d2_redis") (v "0.11.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.12") (d #t) (k 0)))) (h "17zbpc9vszk1xkpqa6dl3v00vci9w9in9ybig52b29l0jn4dvlja")))

(define-public crate-r2d2_redis-0.12.0 (c (n "r2d2_redis") (v "0.12.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.13") (d #t) (k 0)))) (h "0rqs7sa8lpklj52ya0wlyjv4gamhz3wrbs61wfs3743nb2igd3qx")))

(define-public crate-r2d2_redis-0.13.0 (c (n "r2d2_redis") (v "0.13.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.15") (d #t) (k 0)))) (h "034l3v62zgjahhf6wvzdvlqjwxrqg1b6hrcrhw8r91fp07cbcxsc")))

(define-public crate-r2d2_redis-0.14.0 (c (n "r2d2_redis") (v "0.14.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.20.0") (d #t) (k 0)))) (h "1fv38fvqcaz5bw2f1fvq4akb876x0zi5iv426qskxfdhfsw7690q")))

