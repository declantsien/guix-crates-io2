(define-module (crates-io r2 d2 r2d2_cypher) #:use-module (crates-io))

(define-public crate-r2d2_cypher-0.0.1 (c (n "r2d2_cypher") (v "0.0.1") (d (list (d (n "r2d2") (r "*") (d #t) (k 0)) (d (n "rusted_cypher") (r "*") (d #t) (k 0)))) (h "1c30db6smmgyp1643cc4793j6p6b4mwaimqc8bmhgb5qwwpb2g15")))

(define-public crate-r2d2_cypher-0.0.2 (c (n "r2d2_cypher") (v "0.0.2") (d (list (d (n "r2d2") (r "*") (d #t) (k 0)) (d (n "rusted_cypher") (r "*") (d #t) (k 0)))) (h "0f9h2lxampl3p1a2m16ywx644ar3ws3j513g6qc1yn1i78l8pd6x")))

(define-public crate-r2d2_cypher-0.1.0 (c (n "r2d2_cypher") (v "0.1.0") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusted_cypher") (r "^0.9") (d #t) (k 0)))) (h "19a34km231bx6rwrm1brckkyy8pvyqxh1x0rl6kwfii5iz4p7qgj")))

(define-public crate-r2d2_cypher-0.2.0 (c (n "r2d2_cypher") (v "0.2.0") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.0") (d #t) (k 0)))) (h "10fcps6x0apg47xaay05fb9s54mqclngwpknq7dxk9jdbvsxpc25")))

(define-public crate-r2d2_cypher-0.3.0 (c (n "r2d2_cypher") (v "0.3.0") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1") (d #t) (k 0)))) (h "079jzrna25zhl3qr4gvbwrdr19cbxmmh98s9yasx5kgap9sm78k9")))

(define-public crate-r2d2_cypher-0.4.0 (c (n "r2d2_cypher") (v "0.4.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusted_cypher") (r "^1.1") (d #t) (k 0)))) (h "144maik2k1jhk8ijm31ak1s5jrkgylij4ms8lj922hr9xxvd54g4")))

