(define-module (crates-io r2 d2 r2d2_firebird) #:use-module (crates-io))

(define-public crate-r2d2_firebird-0.15.0 (c (n "r2d2_firebird") (v "0.15.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rsfbclient") (r "^0.15") (k 0)) (d (n "rsfbclient") (r "^0.15") (f (quote ("pure_rust" "date_time"))) (k 2)) (d (n "rsfbclient-core") (r "^0.15") (d #t) (k 0)))) (h "0hjkmy7ww8kh11qxdqianm1haykib940wv3m2r0q5ysi2gcpz04b")))

(define-public crate-r2d2_firebird-0.16.0 (c (n "r2d2_firebird") (v "0.16.0") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "rsfbclient") (r "^0.16") (k 0)) (d (n "rsfbclient") (r "^0.16") (f (quote ("pure_rust"))) (k 2)) (d (n "rsfbclient-core") (r "^0.16") (d #t) (k 0)))) (h "1wjvcf0gq106jj7x2gxwlmpf7y6xx2g082wbrvarviq00kmx5w6n")))

(define-public crate-r2d2_firebird-0.17.0 (c (n "r2d2_firebird") (v "0.17.0") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "rsfbclient") (r "^0.17") (k 0)) (d (n "rsfbclient") (r "^0.17") (f (quote ("pure_rust"))) (k 2)) (d (n "rsfbclient-core") (r "^0.17") (d #t) (k 0)))) (h "16harjlg52rfm34nh9sl87v0q6ni679fxa9r6q5pps9grps3nfyj")))

(define-public crate-r2d2_firebird-0.19.0 (c (n "r2d2_firebird") (v "0.19.0") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "rsfbclient") (r "^0.19") (k 0)) (d (n "rsfbclient") (r "^0.19") (f (quote ("pure_rust"))) (k 2)) (d (n "rsfbclient-core") (r "^0.19") (d #t) (k 0)))) (h "1wcd6gsi2nsan96yi77ixs20mlqdnfvy6dl5i8z0xqdgcy7nvldi")))

(define-public crate-r2d2_firebird-0.20.0 (c (n "r2d2_firebird") (v "0.20.0") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "rsfbclient") (r "^0.20") (k 0)) (d (n "rsfbclient") (r "^0.20") (f (quote ("pure_rust"))) (k 2)) (d (n "rsfbclient-core") (r "^0.20") (d #t) (k 0)))) (h "0rajmwbszzgylxb2zlzxdvpbmrkc6g0acl3ix8bw8pd0dv3xaqbj")))

(define-public crate-r2d2_firebird-0.23.0 (c (n "r2d2_firebird") (v "0.23.0") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "rsfbclient") (r "^0.23") (k 0)) (d (n "rsfbclient") (r "^0.23") (f (quote ("pure_rust"))) (k 2)) (d (n "rsfbclient-core") (r "^0.23") (d #t) (k 0)))) (h "1sk5sbngb5gmsjnvbqpnlyfgnkj7pdqam1vciamwjwayamk6sww7")))

(define-public crate-r2d2_firebird-0.23.1 (c (n "r2d2_firebird") (v "0.23.1") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "rsfbclient") (r "^0.23") (k 0)) (d (n "rsfbclient") (r "^0.23") (f (quote ("pure_rust"))) (k 2)) (d (n "rsfbclient-core") (r "^0.23") (d #t) (k 0)))) (h "0y3vgjwgq2j0mywxrkwl6p8z6831vfzj7y5vik28h0iyv3x8792x")))

(define-public crate-r2d2_firebird-0.23.2 (c (n "r2d2_firebird") (v "0.23.2") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "rsfbclient") (r "^0.23") (k 0)) (d (n "rsfbclient") (r "^0.23") (f (quote ("pure_rust"))) (k 2)) (d (n "rsfbclient-core") (r "^0.23") (d #t) (k 0)))) (h "078y9f2kxijimva755w1ds9fid450h3xgf4gsf5zix1f2cl5d0cy")))

(define-public crate-r2d2_firebird-0.24.0 (c (n "r2d2_firebird") (v "0.24.0") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "rsfbclient") (r "^0.23") (k 0)) (d (n "rsfbclient") (r "^0.23") (f (quote ("pure_rust"))) (k 2)) (d (n "rsfbclient-core") (r "^0.23") (d #t) (k 0)))) (h "155l2l7cnva6zbyaxypdywhdxm3imkm08rl7w6h8977gfqrrg806")))

