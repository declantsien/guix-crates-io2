(define-module (crates-io r2 d2 r2d2_mysql_batis_macros) #:use-module (crates-io))

(define-public crate-r2d2_mysql_batis_macros-0.1.0 (c (n "r2d2_mysql_batis_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bs5cvv93phhimpsvag88jgh07zbvssjxf5j82fcf7jkiisdmkyx")))

