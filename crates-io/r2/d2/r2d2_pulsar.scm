(define-module (crates-io r2 d2 r2d2_pulsar) #:use-module (crates-io))

(define-public crate-r2d2_pulsar-0.1.0 (c (n "r2d2_pulsar") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "pulsar") (r "^0.1.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "1gmw89xji48al356jm3b5l8k0cl1ciq8lyy5jngk0i4lzmghxcw1")))

(define-public crate-r2d2_pulsar-0.1.1 (c (n "r2d2_pulsar") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "pulsar") (r "^0.1.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "1vi4n23h0hrqacdamka1sayr9jp98nfginjcc3p25l95k7c9hrmv")))

(define-public crate-r2d2_pulsar-0.1.2 (c (n "r2d2_pulsar") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "pulsar") (r "^0.1.2") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "0c58khy1m482vcfi4rki728dn4i0cnb7mrpif0zqs0d50bm6swmi")))

(define-public crate-r2d2_pulsar-0.1.3 (c (n "r2d2_pulsar") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "pulsar") (r "^0.1.3") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "022qy810b4x8pib77s7v9q7zncxma7r8ka16ycpzc5hxn8rxycyc")))

