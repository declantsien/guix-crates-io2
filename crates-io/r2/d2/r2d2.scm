(define-module (crates-io r2 d2 r2d2) #:use-module (crates-io))

(define-public crate-r2d2-0.0.1 (c (n "r2d2") (v "0.0.1") (h "1wd0gh13ghjp65ci8n72yia4f815lh76ckczv1c49c79fdm28rnl")))

(define-public crate-r2d2-0.1.0 (c (n "r2d2") (v "0.1.0") (h "0nci1i0f9qw911456lwvc07f5sbi76j62bl5c4ird1vsnylbx2wz")))

(define-public crate-r2d2-0.1.1 (c (n "r2d2") (v "0.1.1") (h "1c93vdfiz19csxcb3dk4gicmhzfi9339czk1chvbxid66wap52zx")))

(define-public crate-r2d2-0.1.2 (c (n "r2d2") (v "0.1.2") (h "1rxm6x6q57ik9k0134vrpff4nkrpwr9bcr3bl2lz31zap9rfhbpb")))

(define-public crate-r2d2-0.2.0 (c (n "r2d2") (v "0.2.0") (d (list (d (n "log") (r "^0.1") (d #t) (k 0)))) (h "1isyli270hm7vwy7hp08s0w2hdmcykzm7ya3b4ffpfincj30b3iy")))

(define-public crate-r2d2-0.2.1 (c (n "r2d2") (v "0.2.1") (d (list (d (n "log") (r "^0.1") (d #t) (k 0)))) (h "05gj7av9v5f26jjyb2z2kp0mpf0lgkvpjqzhf829rc1qk82vsm2y")))

(define-public crate-r2d2-0.2.2 (c (n "r2d2") (v "0.2.2") (d (list (d (n "log") (r "^0.1") (d #t) (k 0)))) (h "1679la6z7pqgvdwzr1cp8zxnhsjik1mgv6jakj6pl0shb50fzjp0")))

(define-public crate-r2d2-0.2.3 (c (n "r2d2") (v "0.2.3") (d (list (d (n "log") (r "^0.1") (d #t) (k 0)))) (h "1qb3y380hig4b9h3q6cw29zfhkmmnbvmm3ipsflvbpdh0rhg615v")))

(define-public crate-r2d2-0.2.4 (c (n "r2d2") (v "0.2.4") (d (list (d (n "log") (r "^0.1") (d #t) (k 0)))) (h "1qdrz2ankz58qzj0g7066vri3ywqcvsb2yhkfggh4f0pnv3bilpb")))

(define-public crate-r2d2-0.2.5 (c (n "r2d2") (v "0.2.5") (d (list (d (n "log") (r "^0.1") (d #t) (k 0)))) (h "1h59khw24kg481adffih9r7xihpfyss1x2ckq1gv5hh7bj40h2p0")))

(define-public crate-r2d2-0.3.0 (c (n "r2d2") (v "0.3.0") (d (list (d (n "log") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1.14") (d #t) (k 0)))) (h "0rjz0kh8p3f4iq46dri0mf9vg5v2wbqzjcvwifkkhja3lli4mwdb")))

(define-public crate-r2d2-0.4.0 (c (n "r2d2") (v "0.4.0") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.14") (d #t) (k 0)))) (h "14ppvj9mn3wrpfmwcdmps269d60hqmpzgn2mya0yg81cssiyyc2j")))

(define-public crate-r2d2-0.5.0 (c (n "r2d2") (v "0.5.0") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.14") (d #t) (k 0)))) (h "02zydfrsyfnivm6k88jjpkky890bpfjp02r6y1l5adj7zbjj5svf")))

(define-public crate-r2d2-0.5.1 (c (n "r2d2") (v "0.5.1") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.14") (d #t) (k 0)))) (h "1ak9jjm9y3lg9zv60pcl920fqs2dy63s86jc6z6s6nz9v5mbkahs")))

(define-public crate-r2d2-0.5.2 (c (n "r2d2") (v "0.5.2") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.14") (d #t) (k 0)))) (h "0ad1bb0fci0w8zq62250g52jfnpdhzli5is8vjiwsvvr75isbz8h")))

(define-public crate-r2d2-0.5.3 (c (n "r2d2") (v "0.5.3") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.14") (d #t) (k 0)))) (h "01d36p9a1iz7hdnhy22n41wxk7kjgwlambl8611mpp5airls68pg")))

(define-public crate-r2d2-0.5.4 (c (n "r2d2") (v "0.5.4") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.14") (d #t) (k 0)))) (h "0b109iy9zd6h7wy6sh32dwfyx4zgmxl4hah3knk1drwicss1hr9f")))

(define-public crate-r2d2-0.5.5 (c (n "r2d2") (v "0.5.5") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.22") (f (quote ("std-duration"))) (d #t) (k 0)))) (h "0jidhp8fvs8cwzh7jpd94q3wn6sw15v1d56m3k1cfgxnvi0sb7wn")))

(define-public crate-r2d2-0.5.6 (c (n "r2d2") (v "0.5.6") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.22") (f (quote ("std-duration"))) (d #t) (k 0)))) (h "0d6mzwpbdx5c7fj7c8jv5zy8940k3a9sy06jgb86jrxcba9n9r7q")))

(define-public crate-r2d2-0.5.7 (c (n "r2d2") (v "0.5.7") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.22") (d #t) (k 0)))) (h "116lab542xq4m16n0s92zi6ww54ab4mnn2yrg02154bg3xllw1qb")))

(define-public crate-r2d2-0.5.8 (c (n "r2d2") (v "0.5.8") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.22") (d #t) (k 0)))) (h "1myfbfgwl8gdf5b7vrwlwlmagphkkxjfpa5iz4fqk22fwz3gy8k2")))

(define-public crate-r2d2-0.6.0 (c (n "r2d2") (v "0.6.0") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.22") (d #t) (k 0)))) (h "12d3ghbl691kijvawykxs9mac01j7jihmmk4zv8aamxigm1vv6ws")))

(define-public crate-r2d2-0.6.1 (c (n "r2d2") (v "0.6.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.22") (d #t) (k 0)))) (h "1bcyqcdk5in78py7pv0rn1bncizm5qv5lkfadcbc6x11i9wg6r5r")))

(define-public crate-r2d2-0.6.2 (c (n "r2d2") (v "0.6.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.22") (d #t) (k 0)))) (h "1ndnsbc1jn7raf997i81mpdzk746q54v1834409zpfli6ayr911p")))

(define-public crate-r2d2-0.6.3 (c (n "r2d2") (v "0.6.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.22") (d #t) (k 0)))) (h "05i5gf2ldg1xyn8rg2qds76hpadap8kx34phxhh81xbygr3prnnm")))

(define-public crate-r2d2-0.6.4 (c (n "r2d2") (v "0.6.4") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.22") (d #t) (k 0)))) (h "029g7677ax53grlqaw3n6y2cj2mmsxqqmi3ddzagfx1vyqybcm6i")))

(define-public crate-r2d2-0.7.0 (c (n "r2d2") (v "0.7.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1g4pvpp7kznjxam0fb38cfi7c8ds922hw52wbha6acavcpb7sg56")))

(define-public crate-r2d2-0.7.1 (c (n "r2d2") (v "0.7.1") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0cmdlgfy88dk334x6ns04lzjam5d7lbyyz689ri6cbmy0cdyvksf")))

(define-public crate-r2d2-0.7.2 (c (n "r2d2") (v "0.7.2") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.1") (d #t) (k 0)))) (h "1ywp7ynj6qisix3kyzwrsyypmi07z848dyvz33z3rn0fkp14im0x")))

(define-public crate-r2d2-0.7.3 (c (n "r2d2") (v "0.7.3") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.1") (d #t) (k 0)))) (h "1jnj8rjpqyvm32m306zwgf1j2csx6wxn9riv14h1d3i1id9mlrkc")))

(define-public crate-r2d2-0.7.4 (c (n "r2d2") (v "0.7.4") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.1") (d #t) (k 0)))) (h "1kmwwhwfx3zmv9qb49j2ywi0ny644dg3j9rmiw7l9prqid8890ic")))

(define-public crate-r2d2-0.8.0 (c (n "r2d2") (v "0.8.0") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2") (d #t) (k 0)))) (h "1b8g5qsrlr8ygq6xsc64qm8q10pnawb8nrhx0rf8hkx3xdd9jkv4")))

(define-public crate-r2d2-0.8.1 (c (n "r2d2") (v "0.8.1") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2") (d #t) (k 0)))) (h "1s8nmqizly78c1zhvh8g3fc7bv5l978kwknqxj3cb5p4pq114qar")))

(define-public crate-r2d2-0.8.2 (c (n "r2d2") (v "0.8.2") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2") (d #t) (k 0)))) (h "0invv0bipra9k36dsrzq2sfvd5cjvkvv4fq88b8qwmm5m2k8q1zr")))

(define-public crate-r2d2-0.8.3 (c (n "r2d2") (v "0.8.3") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2") (d #t) (k 0)))) (h "08mxwyrd0p6xaglxr32b7gdhvsalb19sswzzlz79rcfsl346yx2x")))

(define-public crate-r2d2-0.8.4 (c (n "r2d2") (v "0.8.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2") (d #t) (k 0)))) (h "1490s6rmj0v80sxq2ncnnkfndx69pg6mz4j8538a908j4n9s5n4x")))

(define-public crate-r2d2-0.8.5 (c (n "r2d2") (v "0.8.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2") (d #t) (k 0)))) (h "06zq354jfkhnkg0hx1bj8l84kncds7aixgjbl2r7yi7lv5swwhmw")))

(define-public crate-r2d2-0.8.6 (c (n "r2d2") (v "0.8.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2") (d #t) (k 0)))) (h "07c65xpzbixik2in4kp4ynn8549z91hqykabxsv8c0pgk14ad3z4")))

(define-public crate-r2d2-0.8.7 (c (n "r2d2") (v "0.8.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2") (d #t) (k 0)))) (h "0lsmnxn18lbjnwykj8rhsri1a3if85xk71rqy1rl61msbzycbd92")))

(define-public crate-r2d2-0.8.8 (c (n "r2d2") (v "0.8.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2") (d #t) (k 0)))) (h "1bw50kp6im2asi52bj9sb921xkjb2xaqwzbn8254m3ilal4f95ql")))

(define-public crate-r2d2-0.8.9 (c (n "r2d2") (v "0.8.9") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2") (d #t) (k 0)))) (h "0vxjgh83bss63mkx308p16iwl33s80c781p422f3r5w0p315np2l")))

(define-public crate-r2d2-0.8.10 (c (n "r2d2") (v "0.8.10") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2") (d #t) (k 0)))) (h "14qw32y4m564xb1f5ya8ii7dwqyknvk8bsx2r0lljlmn7zxqbpji")))

