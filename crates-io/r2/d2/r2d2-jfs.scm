(define-module (crates-io r2 d2 r2d2-jfs) #:use-module (crates-io))

(define-public crate-r2d2-jfs-0.0.1 (c (n "r2d2-jfs") (v "0.0.1") (d (list (d (n "jfs") (r "^0.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "14bxz5zh8pc4ssndi0zwzdrfbap89yswhm4v9fh81gg5x0mhdkbm")))

(define-public crate-r2d2-jfs-0.1.0 (c (n "r2d2-jfs") (v "0.1.0") (d (list (d (n "jfs") (r "^0.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0x4x419i5cls0scil5xi05v408l2l49wj1g1qazzz0qv0pydms7r")))

(define-public crate-r2d2-jfs-0.2.0 (c (n "r2d2-jfs") (v "0.2.0") (d (list (d (n "jfs") (r "^0.7") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "15hvqlgb3pb6fqjja7qhw20qwz1qn44f6d3y3php3p0japravmnz")))

