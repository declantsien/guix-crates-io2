(define-module (crates-io r2 d2 r2d2_sqlite) #:use-module (crates-io))

(define-public crate-r2d2_sqlite-0.0.1 (c (n "r2d2_sqlite") (v "0.0.1") (d (list (d (n "r2d2") (r "*") (d #t) (k 0)) (d (n "rusqlite") (r "*") (d #t) (k 0)))) (h "0k7j68bkw1sk3gy9p06hb4bnzwfhcdwxk4aw01cyg1d13z7m70ay")))

(define-public crate-r2d2_sqlite-0.0.2 (c (n "r2d2_sqlite") (v "0.0.2") (d (list (d (n "r2d2") (r "^0.6.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.4.0") (d #t) (k 0)))) (h "0zba6k69i5xgkcg2s0619ixssfzjgnygmyjqiqz97ngf6w5vzfn9")))

(define-public crate-r2d2_sqlite-0.0.3 (c (n "r2d2_sqlite") (v "0.0.3") (d (list (d (n "r2d2") (r "^0.6.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.4.0") (d #t) (k 0)))) (h "1611h61k6dhlrx9a1y0lbw84zd9gvi6kafh351mxbagnhggpmjs7")))

(define-public crate-r2d2_sqlite-0.0.4 (c (n "r2d2_sqlite") (v "0.0.4") (d (list (d (n "r2d2") (r "^0.6.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 0)))) (h "1njpgw0l1didbaahsfrgw1zyl4d7c73x83qyhwyzkshdpw92khps")))

(define-public crate-r2d2_sqlite-0.0.5 (c (n "r2d2_sqlite") (v "0.0.5") (d (list (d (n "r2d2") (r "^0.6.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.6.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1gnbnnp5np40ghwz17xx5rzdy7sgps60d3z7m02qy4lx4ljnhgln")))

(define-public crate-r2d2_sqlite-0.0.6 (c (n "r2d2_sqlite") (v "0.0.6") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0dhidy7zac4l158jkwiprxvczgisj30dsidvxm3sh6a1ql0czc1w")))

(define-public crate-r2d2_sqlite-0.1.0 (c (n "r2d2_sqlite") (v "0.1.0") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.7") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0ysdwyz9vkl23z8c2p4x11dn7spys208i8lgwkkvgajviy9gd86f")))

(define-public crate-r2d2_sqlite-0.1.1 (c (n "r2d2_sqlite") (v "0.1.1") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1y7nzd59rzxyqaka9x912chaf4qyn1i8pyv5bkgpj761a271p3b5")))

(define-public crate-r2d2_sqlite-0.1.2 (c (n "r2d2_sqlite") (v "0.1.2") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0dx2fjnmqsyyzbnv9chn5nnkdkil6lv5x3kq13s37y8fh17lm06w")))

(define-public crate-r2d2_sqlite-0.1.3 (c (n "r2d2_sqlite") (v "0.1.3") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1gw05m7dmy9i3z23q05ni7ilxs2ma6n1f9mrsmal3p9q9b62kicn")))

(define-public crate-r2d2_sqlite-0.1.4 (c (n "r2d2_sqlite") (v "0.1.4") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "19blwqvqh8fhbvb3gzc3ldds98vlydayal889cnrp41isw05f7p4")))

(define-public crate-r2d2_sqlite-0.2.0 (c (n "r2d2_sqlite") (v "0.2.0") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0x03wbfsc6dn0q18p97ssjyhjyavy686dr3vbsjdb7xy26zxkhbv")))

(define-public crate-r2d2_sqlite-0.2.1 (c (n "r2d2_sqlite") (v "0.2.1") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.12") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0lg25sqz7m7jsp162zgxz0q709klx4bawrs5gz9bhd8kr19qkc9q")))

(define-public crate-r2d2_sqlite-0.3.0 (c (n "r2d2_sqlite") (v "0.3.0") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.12") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0if8dc9lpla7gmfv3ksr9573g1gc6x26c81cbycap4l0cx4cjxlf")))

(define-public crate-r2d2_sqlite-0.3.1 (c (n "r2d2_sqlite") (v "0.3.1") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "02jl3c8wi07ny05c7gs266zkbqlzly39g11ynax1yhpx1ih7hkac") (y #t)))

(define-public crate-r2d2_sqlite-0.4.0 (c (n "r2d2_sqlite") (v "0.4.0") (d (list (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1cdmz0vxwy84k80v0lgx4nhhrrsy94hrgm5sid516qynfwbphv2w")))

(define-public crate-r2d2_sqlite-0.5.0 (c (n "r2d2_sqlite") (v "0.5.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1gbbcdkbihkkxc3cvsgqq37db7ph6aahzzsqw9xljx57xivsb8j0")))

(define-public crate-r2d2_sqlite-0.5.1 (c (n "r2d2_sqlite") (v "0.5.1") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.14.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0wqdphn2b4x1i0g59qdfa3z1sys69rbyrh39ybkvp22p4azs2xnp") (y #t)))

(define-public crate-r2d2_sqlite-0.6.0 (c (n "r2d2_sqlite") (v "0.6.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.14.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "09l417ysllcn3zhdjpb2axqx8660cxrs3jbq3qb05x5fw77f8lvk")))

(define-public crate-r2d2_sqlite-0.7.0 (c (n "r2d2_sqlite") (v "0.7.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "02g9x8xam4389ymy36d8j36gfmpl02rzvlqq10l546xvmd36sinf")))

(define-public crate-r2d2_sqlite-0.8.0 (c (n "r2d2_sqlite") (v "0.8.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0mrqcl9zc4iw4kh845dr6rvsg2aap3v4bvv5lyqnbrcsna8akjx7")))

(define-public crate-r2d2_sqlite-0.9.0 (c (n "r2d2_sqlite") (v "0.9.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.17") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1b0dfv0cddlfb2g3fj2g1bb21010g2wxfcawr5chrxvvkrypyqvh")))

(define-public crate-r2d2_sqlite-0.10.0 (c (n "r2d2_sqlite") (v "0.10.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.18") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1hplljsw1cxdrg6913y8dr4cq98dfgm3svjm8r19slj7pdfml00p")))

(define-public crate-r2d2_sqlite-0.11.0 (c (n "r2d2_sqlite") (v "0.11.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.19") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "02rwshwizxw7klfwyhkvzqj6m7dhslmwazaydxfz1xh5dbm9rq24")))

(define-public crate-r2d2_sqlite-0.12.0 (c (n "r2d2_sqlite") (v "0.12.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.20") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0czgr8qgbfmhilryhj1fxxcqpwiy0y5aq5zn9dh5m7nf6n02cvl0")))

(define-public crate-r2d2_sqlite-0.13.0 (c (n "r2d2_sqlite") (v "0.13.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "17hx5nlhm8jabizydjy4anjaw4vxzkk943r6xcgfifwjcsqzaqdh")))

(define-public crate-r2d2_sqlite-0.14.0 (c (n "r2d2_sqlite") (v "0.14.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0fryajmnpfx7ssfbq7qawzvj8qdrrdba15m6i0rild24pvf8jikw")))

(define-public crate-r2d2_sqlite-0.15.0 (c (n "r2d2_sqlite") (v "0.15.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.22") (d #t) (k 0)) (d (n "rusqlite") (r "^0.22") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0cvg350xm02g17a03n4df2ps794kdan5pb6c42pbi2vw9rwzy5ay")))

(define-public crate-r2d2_sqlite-0.16.0 (c (n "r2d2_sqlite") (v "0.16.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0afr0z3ydx695vf5s7m0wd9zy0nkxkmgph1vav02ib17iglfnq7d")))

(define-public crate-r2d2_sqlite-0.17.0 (c (n "r2d2_sqlite") (v "0.17.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1n59vg0z50d9bzmib14yilybjxz61xcn5w58dnkizc6byigv6yi2")))

(define-public crate-r2d2_sqlite-0.18.0 (c (n "r2d2_sqlite") (v "0.18.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0ncrf5s23ixjmhi33zpflp34rhr3i8fsqlyzsd15wk1195q6094x")))

(define-public crate-r2d2_sqlite-0.19.0 (c (n "r2d2_sqlite") (v "0.19.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0028yccvqn1904f8vcf7xbpn5hrgd1cnlj2cfanw4vx7d2a3rjjl") (f (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite-0.20.0 (c (n "r2d2_sqlite") (v "0.20.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "15g8zw9psinz3rhhgadbn73a89hkyanvfcmyjdw151h5lx6qxp3g") (f (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite-0.21.0 (c (n "r2d2_sqlite") (v "0.21.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0d0hlnj9pgzrqihgb7a5xy6hgn69din35z0zv6n5rkcrgqrx1xdl") (f (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite-0.22.0 (c (n "r2d2_sqlite") (v "0.22.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "uuid") (r "^1.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1p4d6h4bxh0zi4sjv1blzaa8c3hfabgkhrq4hprqa4qnsqii7wwr") (f (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite-0.23.0 (c (n "r2d2_sqlite") (v "0.23.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "uuid") (r "^1.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1xkpfyxyidhhzwc8hp4w84hd0qinw6xifdc13rsj03nkd6v91hjd") (f (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite-0.24.0 (c (n "r2d2_sqlite") (v "0.24.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "uuid") (r "^1.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1wn5rh3vrcr5k1x84ph31ns85x1f55dpgy3jp9npjaf1cpgjx63a") (f (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

