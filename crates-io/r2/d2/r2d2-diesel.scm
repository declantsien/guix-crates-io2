(define-module (crates-io r2 d2 r2d2-diesel) #:use-module (crates-io))

(define-public crate-r2d2-diesel-0.4.0 (c (n "r2d2-diesel") (v "0.4.0") (d (list (d (n "diesel") (r "^0.4.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.6.1") (d #t) (k 0)))) (h "1niwcy8cg703cf2b0kzmz112ira3h6zihk2bzj73407sgp5cajw4")))

(define-public crate-r2d2-diesel-0.5.0 (c (n "r2d2-diesel") (v "0.5.0") (d (list (d (n "diesel") (r "^0.5.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.6.1") (d #t) (k 0)))) (h "0dq41znyb0x6vck12fizzkda138mz068a88dfj6zcnvicspfncdn")))

(define-public crate-r2d2-diesel-0.6.0 (c (n "r2d2-diesel") (v "0.6.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.7.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)))) (h "04p78hjh6yxfdpbl414kkf8b8jghkmpbbz7r6xdj0dscimmv0dy0")))

(define-public crate-r2d2-diesel-0.7.0 (c (n "r2d2-diesel") (v "0.7.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.8.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)))) (h "11p8kgcw0w7b7isqph54z2wgnni9r56lv94zbfy7xgkdgdlqjc3v")))

(define-public crate-r2d2-diesel-0.7.1 (c (n "r2d2-diesel") (v "0.7.1") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.8.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)))) (h "16z3aksi7x3j48q2ld4lkyxblkw6ilm0nwabkycdq1plwy10ca0s") (f (quote (("sqlite" "diesel/sqlite") ("default"))))))

(define-public crate-r2d2-diesel-0.8.0 (c (n "r2d2-diesel") (v "0.8.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.9.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)))) (h "1k6pd9b46im749v5hrcjjavji2rx6vh3s9vfry20xcc1ii7xmnmc") (f (quote (("sqlite" "diesel/sqlite") ("default"))))))

(define-public crate-r2d2-diesel-0.9.0 (c (n "r2d2-diesel") (v "0.9.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.10.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)))) (h "1c0ps3jzk9gm0i2da7dkqqd71rm1fj2iv923bghqc8h75admpkla") (f (quote (("sqlite" "diesel/sqlite") ("default"))))))

(define-public crate-r2d2-diesel-0.10.0 (c (n "r2d2-diesel") (v "0.10.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.11.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)))) (h "15bl9xk1a33w1wykk68ivvvg1mn85nsa7k2d82xgnf3vg8r2aa96") (f (quote (("sqlite" "diesel/sqlite") ("default"))))))

(define-public crate-r2d2-diesel-0.11.0 (c (n "r2d2-diesel") (v "0.11.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.12.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)))) (h "1kna13dxl6b9gd1awmh54s8x1sp5kydxapx68jwwa6m924nk6j8m") (f (quote (("sqlite" "diesel/sqlite") ("default"))))))

(define-public crate-r2d2-diesel-0.12.0 (c (n "r2d2-diesel") (v "0.12.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.13.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)))) (h "00j4g9zjhgw4pd4l9d260naq5619js8ji88jhm01rk7n2b13bsjb")))

(define-public crate-r2d2-diesel-0.13.0 (c (n "r2d2-diesel") (v "0.13.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.14.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)))) (h "1ifnib0w911v0smxwmjv3d1i4i1aavyvjgi3qj1xgg48p7k49gqv")))

(define-public crate-r2d2-diesel-0.14.0 (c (n "r2d2-diesel") (v "0.14.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.15.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)))) (h "0rf0z7n40vywfi8i4k3ypq3agakf8qwcscl5828bksq3hamx1lb0")))

(define-public crate-r2d2-diesel-0.15.0 (c (n "r2d2-diesel") (v "0.15.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.16.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)))) (h "0d7hsrzxsjbalgms4vfz1bzi0wy338js06lg8jfszpg2c82d98hc")))

(define-public crate-r2d2-diesel-0.16.0 (c (n "r2d2-diesel") (v "0.16.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.17.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)))) (h "0y04dhnqqicr3zd4ylj7dipvb7rvjznm46yjjq99jibcd9lj3fgn")))

(define-public crate-r2d2-diesel-0.99.0 (c (n "r2d2-diesel") (v "0.99.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 1.0.0") (d #t) (k 0)) (d (n "r2d2") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "0rnqvnhh9cb7s6qkzg61hy3lv8xjw5cz69s4cr5hywl2k8afvakp")))

(define-public crate-r2d2-diesel-1.0.0-beta1 (c (n "r2d2-diesel") (v "1.0.0-beta1") (d (list (d (n "diesel") (r ">= 0.5.0, <= 1.0.0-beta1") (d #t) (k 0)) (d (n "r2d2") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "0ckqvzz9w6cvysila38mqs6vw5qsxh1v094mb03pl9n2ligk8g4j")))

(define-public crate-r2d2-diesel-1.0.0-rc1 (c (n "r2d2-diesel") (v "1.0.0-rc1") (d (list (d (n "diesel") (r ">= 0.5.0, <= 1.0.0-rc1") (d #t) (k 0)) (d (n "r2d2") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "1f2ykfbd71w9n925s56as8ql2kmn50vqzihply2lj23zmrcn3mf0")))

(define-public crate-r2d2-diesel-1.0.0 (c (n "r2d2-diesel") (v "1.0.0") (d (list (d (n "diesel") (r "^1.0") (d #t) (k 0)) (d (n "r2d2") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "18xi9xp12hqwf4gbkyjclxpzx8y3xd9080icph16v9rdv6x2k77b")))

