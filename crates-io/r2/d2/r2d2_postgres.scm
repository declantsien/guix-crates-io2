(define-module (crates-io r2 d2 r2d2_postgres) #:use-module (crates-io))

(define-public crate-r2d2_postgres-0.1.0 (c (n "r2d2_postgres") (v "0.1.0") (d (list (d (n "postgres") (r "*") (d #t) (k 0)) (d (n "r2d2") (r "^0.1") (d #t) (k 0)))) (h "0n1ia1slkxaf9d8cm2a6f63g88dfj9spqdgha3i8jfqv236s8ayz")))

(define-public crate-r2d2_postgres-0.1.1 (c (n "r2d2_postgres") (v "0.1.1") (d (list (d (n "postgres") (r "^0.2") (d #t) (k 0)) (d (n "r2d2") (r "^0.1") (d #t) (k 0)))) (h "0rydl4gbfzl0lfqwx4m4x4r05jn0sjxlrdlpjaqfmykqxsf8ih9c")))

(define-public crate-r2d2_postgres-0.2.0 (c (n "r2d2_postgres") (v "0.2.0") (d (list (d (n "postgres") (r "^0.2") (d #t) (k 0)) (d (n "r2d2") (r "^0.2") (d #t) (k 0)))) (h "00q89gp58cy9b7x0mnlc8f47llz1injbx2zjhhqlj6j3fcclvrbd")))

(define-public crate-r2d2_postgres-0.2.1 (c (n "r2d2_postgres") (v "0.2.1") (d (list (d (n "collect") (r "~0.0") (d #t) (k 0)) (d (n "postgres") (r "^0.2") (d #t) (k 0)) (d (n "r2d2") (r "^0.2") (d #t) (k 0)))) (h "0rs5k7ygxm9mr1gbq861h72rrld724hcb6jwqbix78f560r38izs")))

(define-public crate-r2d2_postgres-0.3.0 (c (n "r2d2_postgres") (v "0.3.0") (d (list (d (n "collect") (r "~0.0") (d #t) (k 0)) (d (n "postgres") (r "^0.4") (d #t) (k 0)) (d (n "r2d2") (r "^0.2") (d #t) (k 0)))) (h "1ng96x96k124rmlqw2gggqjri34rmsy50m6dfwpvvf92zwbhdm1z")))

(define-public crate-r2d2_postgres-0.3.1 (c (n "r2d2_postgres") (v "0.3.1") (d (list (d (n "collect") (r "~0.0") (d #t) (k 0)) (d (n "postgres") (r "^0.4") (d #t) (k 0)) (d (n "r2d2") (r "^0.2") (d #t) (k 0)))) (h "0bhjlmqxaz3y4kz18263hy8jsrb02j1di4z91n04bdpg8hb4i5p7")))

(define-public crate-r2d2_postgres-0.4.0 (c (n "r2d2_postgres") (v "0.4.0") (d (list (d (n "postgres") (r "^0.5") (d #t) (k 0)) (d (n "r2d2") (r "^0.3") (d #t) (k 0)))) (h "1kbzqcrvylnjcr499rb31i506i7cagxvi9si3k6bw8am3b6afg6l")))

(define-public crate-r2d2_postgres-0.5.0 (c (n "r2d2_postgres") (v "0.5.0") (d (list (d (n "postgres") (r "^0.5") (d #t) (k 0)) (d (n "r2d2") (r "^0.4") (d #t) (k 0)))) (h "1sb10d6nj657a9pn4pkm9k4lgf9pqvzyxb69drg7g6rx5rizr3bq")))

(define-public crate-r2d2_postgres-0.6.0 (c (n "r2d2_postgres") (v "0.6.0") (d (list (d (n "postgres") (r "^0.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.4") (d #t) (k 0)))) (h "061qw88b6mm6fnhkps9vfsxsgp1ih78cfj5az8j1268wz0nslwn6")))

(define-public crate-r2d2_postgres-0.7.0 (c (n "r2d2_postgres") (v "0.7.0") (d (list (d (n "postgres") (r "^0.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.5") (d #t) (k 0)))) (h "1n11ias0jv98zbdzycwkx7rm1rb2xj9ljr83akw7xh2wbd9rcrcg")))

(define-public crate-r2d2_postgres-0.8.0 (c (n "r2d2_postgres") (v "0.8.0") (d (list (d (n "postgres") (r "^0.7") (d #t) (k 0)) (d (n "r2d2") (r "^0.5") (d #t) (k 0)))) (h "0jsbahssfjrshbllmjvizimr3j5xad03fgx52v9ffaix12w0zmp3")))

(define-public crate-r2d2_postgres-0.9.0 (c (n "r2d2_postgres") (v "0.9.0") (d (list (d (n "postgres") (r "^0.8") (d #t) (k 0)) (d (n "r2d2") (r "^0.5") (d #t) (k 0)))) (h "1r63crici9sqrq2qciym8k2a3zsz0z34jxmb70jcgy9vzy6q812x")))

(define-public crate-r2d2_postgres-0.9.1 (c (n "r2d2_postgres") (v "0.9.1") (d (list (d (n "postgres") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.5") (d #t) (k 0)))) (h "05dz37c93ps1llqnwm0sdjzzv2r36krpwgnca1cvqx1iv3mx9518")))

(define-public crate-r2d2_postgres-0.9.2 (c (n "r2d2_postgres") (v "0.9.2") (d (list (d (n "postgres") (r "^0.9") (d #t) (k 0)) (d (n "r2d2") (r "^0.6") (d #t) (k 0)))) (h "00774nk402sfrjd1y7kjj954l45hz4nw4vcaxfc75za3n14w1fhx")))

(define-public crate-r2d2_postgres-0.9.3 (c (n "r2d2_postgres") (v "0.9.3") (d (list (d (n "postgres") (r "^0.10") (d #t) (k 0)) (d (n "r2d2") (r "^0.6") (d #t) (k 0)))) (h "0sn1c91mnf5r3admkzjxm3ilvs6fivcwz7fyh6xa85j2d8m9y556")))

(define-public crate-r2d2_postgres-0.10.0 (c (n "r2d2_postgres") (v "0.10.0") (d (list (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r "^0.6") (d #t) (k 0)))) (h "1g2ilz8ljk1ndjypyyxzc6ps9bdv94pb2ls6d6a1lq82nca53hjm")))

(define-public crate-r2d2_postgres-0.10.1 (c (n "r2d2_postgres") (v "0.10.1") (d (list (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r ">= 0.6, < 0.8") (d #t) (k 0)))) (h "1jk7j2fwn5pmn0ispnzy4fmzbjxgfqk3rnip0ahqjqk09808jsjx")))

(define-public crate-r2d2_postgres-0.11.0 (c (n "r2d2_postgres") (v "0.11.0") (d (list (d (n "postgres") (r "^0.12") (d #t) (k 0)) (d (n "r2d2") (r ">= 0.6, < 0.8") (d #t) (k 0)))) (h "0g6fvklrwk331yllp2cb2m49jsn542q8z4jznb7vrg3sr4f9nwsr")))

(define-public crate-r2d2_postgres-0.11.1 (c (n "r2d2_postgres") (v "0.11.1") (d (list (d (n "postgres") (r ">= 0.12, < 0.14") (d #t) (k 0)) (d (n "r2d2") (r ">= 0.6, < 0.8") (d #t) (k 0)))) (h "0brcpwlzyzxns1yma9yx9n79wq18fffasxa630jz0nvjfd3kvsnr")))

(define-public crate-r2d2_postgres-0.12.0 (c (n "r2d2_postgres") (v "0.12.0") (d (list (d (n "postgres") (r "^0.14") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)))) (h "0p1bvlkjkjybxdd49yapx5lh67gfszy1cn0wl3g77717ls7f3ah0")))

(define-public crate-r2d2_postgres-0.13.0 (c (n "r2d2_postgres") (v "0.13.0") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "postgres-shared") (r "^0.4") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)))) (h "03jnw122535y55ppzsiwhg56d26rzqbm420ivmk6arv3xak1k2qz")))

(define-public crate-r2d2_postgres-0.14.0 (c (n "r2d2_postgres") (v "0.14.0") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "postgres-shared") (r "^0.4") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "14zv00d4mcv5kxszxn8gcamhxpwwp3j3rg32ya62jb1x1jfgxivq")))

(define-public crate-r2d2_postgres-0.15.0-rc.1 (c (n "r2d2_postgres") (v "0.15.0-rc.1") (d (list (d (n "postgres") (r "^0.16.0-rc.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.4.0-rc.2") (d #t) (k 0)))) (h "0c01rzi1l3z0804g7hgn302ar1w86mb87mal2z2wn7g70pv4i8g1")))

(define-public crate-r2d2_postgres-0.16.0-alpha.1 (c (n "r2d2_postgres") (v "0.16.0-alpha.1") (d (list (d (n "postgres") (r "= 0.17.0-alpha.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "1w4wvplk0k5l7fdy86mn2c62wgdxi9i31j45ngz1x3c2blmsljdq")))

(define-public crate-r2d2_postgres-0.16.0-alpha.2 (c (n "r2d2_postgres") (v "0.16.0-alpha.2") (d (list (d (n "postgres") (r "= 0.17.0-alpha.2") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "1a42brjg8j1fxhr9zig4cl5k6jx4x714r1ixfwn62v9qn9kjw85q")))

(define-public crate-r2d2_postgres-0.16.0 (c (n "r2d2_postgres") (v "0.16.0") (d (list (d (n "postgres") (r "^0.17") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "13zyiq4rsqakmk3rgbj153dfgk7z3xhxkxj1244c3fj3dzv2fzbh")))

(define-public crate-r2d2_postgres-0.17.0 (c (n "r2d2_postgres") (v "0.17.0") (d (list (d (n "postgres") (r "^0.18") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "199axyc7j4pndjfj6b2s49wkj1a15mds7kd6xq4hg08dfpxpdwg5")))

(define-public crate-r2d2_postgres-0.18.0 (c (n "r2d2_postgres") (v "0.18.0") (d (list (d (n "postgres") (r "^0.19") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "0viggj6105q8901x3k2137z5b5g930jkx6y4k97mq6w3dlcmsrpp")))

(define-public crate-r2d2_postgres-0.18.1 (c (n "r2d2_postgres") (v "0.18.1") (d (list (d (n "postgres") (r "^0.19") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "02r329mz0sq5sxcjbyimnyb6gq8fh6bybgp047rm9jsqwrmwaabh")))

