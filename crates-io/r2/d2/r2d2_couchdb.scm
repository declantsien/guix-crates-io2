(define-module (crates-io r2 d2 r2d2_couchdb) #:use-module (crates-io))

(define-public crate-r2d2_couchdb-0.1.0 (c (n "r2d2_couchdb") (v "0.1.0") (d (list (d (n "chill") (r "^0.2") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "rustc-test") (r "^0.1") (d #t) (k 2)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 2)))) (h "04fk7b73ixqcmlpvphlhwn9lqqzrv3152w3rmhin8r7v7akyap2c")))

