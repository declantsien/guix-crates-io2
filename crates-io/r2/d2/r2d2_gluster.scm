(define-module (crates-io r2 d2 r2d2_gluster) #:use-module (crates-io))

(define-public crate-r2d2_gluster-0.1.0 (c (n "r2d2_gluster") (v "0.1.0") (d (list (d (n "gfapi-sys") (r "~0.3") (d #t) (k 0)) (d (n "r2d2") (r "~0.8") (d #t) (k 0)))) (h "0jcpa486gzir37dd62ji1qlk7wx1sv44x2h32m6fhibx2h9d9052")))

(define-public crate-r2d2_gluster-0.1.1 (c (n "r2d2_gluster") (v "0.1.1") (d (list (d (n "gfapi-sys") (r "~0.3") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "r2d2") (r "~0.8") (d #t) (k 0)))) (h "1rpy1mmp8ygixzdkc8i3p6lgjhszjyw5vgcb9ziizj3ia9dvjjk3")))

