(define-module (crates-io r2 d2 r2d2-mongodb) #:use-module (crates-io))

(define-public crate-r2d2-mongodb-0.1.0 (c (n "r2d2-mongodb") (v "0.1.0") (d (list (d (n "mongodb") (r "^0.3.7") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)))) (h "0cl73nixafijxbhmhzfrhmc57jacl3hk5r773kqd4rgm9r3wg8rf")))

(define-public crate-r2d2-mongodb-0.1.1 (c (n "r2d2-mongodb") (v "0.1.1") (d (list (d (n "mongodb") (r "^0.3.7") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)))) (h "16835v9snh5i4gvbcih7ddhmfbnwvsvllp3jhq7rq1ndzb2hpb0m")))

(define-public crate-r2d2-mongodb-0.1.2 (c (n "r2d2-mongodb") (v "0.1.2") (d (list (d (n "mongodb") (r "^0.3.7") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)))) (h "05j7lb3zz44kqxwf3a2kx3d3ai4gnigpj2v1m0kjml061b18bvsk")))

(define-public crate-r2d2-mongodb-0.1.3 (c (n "r2d2-mongodb") (v "0.1.3") (d (list (d (n "mongodb") (r "^0.3.7") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)))) (h "0pkrkxfkxz1skn07r6nxh0aiq04zz8pxp9rxdgk5g0ydzs7ng5g5")))

(define-public crate-r2d2-mongodb-0.2.0 (c (n "r2d2-mongodb") (v "0.2.0") (d (list (d (n "mongodb") (r "^0.3.12") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0xpa41shw4wyd6gly0ymhvnsgs3b25q2rcjrxwi57m1rrggg3kbz")))

(define-public crate-r2d2-mongodb-0.2.1 (c (n "r2d2-mongodb") (v "0.2.1") (d (list (d (n "mongodb") (r "^0.3.12") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "13x9mf7xp1rr3x7nsbjklm1g4srijd3mlw125qnzh582m15kbnvw")))

(define-public crate-r2d2-mongodb-0.2.2 (c (n "r2d2-mongodb") (v "0.2.2") (d (list (d (n "mongodb") (r "^0.3.12") (f (quote ("ssl"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)))) (h "1xadsqkmmy0svvnily1s57xisxnv0hw5mhik795h49j93rnb2f58")))

