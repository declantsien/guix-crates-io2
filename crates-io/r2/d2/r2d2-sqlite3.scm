(define-module (crates-io r2 d2 r2d2-sqlite3) #:use-module (crates-io))

(define-public crate-r2d2-sqlite3-0.1.0 (c (n "r2d2-sqlite3") (v "0.1.0") (d (list (d (n "r2d2") (r "^0.7.4") (d #t) (k 0)) (d (n "sqlite") (r "^0.23.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "19497zk3dbyvwgw5crankmjvg1xhzwvl65x08rs6kdiwa67gqfj5")))

(define-public crate-r2d2-sqlite3-0.1.1 (c (n "r2d2-sqlite3") (v "0.1.1") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "sqlite3") (r "^0.24.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1y7xzb62zjszkaf5arm1cx10403b9fyqm07g4rjs8a65p0h3bgc0")))

