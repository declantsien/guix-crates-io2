(define-module (crates-io r2 d2 r2d2-oracle) #:use-module (crates-io))

(define-public crate-r2d2-oracle-0.1.0 (c (n "r2d2-oracle") (v "0.1.0") (d (list (d (n "oracle") (r "^0.3") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "1vhiqrixwcydjnv772g71iqb65i52pl8q5n7k57wlgx9f4ysp9z5")))

(define-public crate-r2d2-oracle-0.2.0 (c (n "r2d2-oracle") (v "0.2.0") (d (list (d (n "oracle") (r "^0.3.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "1ihbhqncs6pr2g10prjidi72s9ji6wgrhdjxcq218kypaa31ph4p")))

(define-public crate-r2d2-oracle-0.3.0 (c (n "r2d2-oracle") (v "0.3.0") (d (list (d (n "oracle") (r "^0.3.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "1vc7mms50nm3gyrf14dnbvnp8024c558w2x5lhqivh73yw90ycrv") (f (quote (("chrono" "oracle/chrono"))))))

(define-public crate-r2d2-oracle-0.4.0 (c (n "r2d2-oracle") (v "0.4.0") (d (list (d (n "oracle") (r "^0.4.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "1rk174gys02hanwzdnl0ggpzzn5nz59qrjma6gw0y9dsx26d2svq") (f (quote (("chrono" "oracle/chrono"))))))

(define-public crate-r2d2-oracle-0.4.1 (c (n "r2d2-oracle") (v "0.4.1") (d (list (d (n "oracle") (r "^0.4.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "1s5f9vkz1p8gckh9lcl47x5g119rmkmjx3a7qdx4pic1hsjq5akm") (f (quote (("chrono" "oracle/chrono"))))))

(define-public crate-r2d2-oracle-0.5.0 (c (n "r2d2-oracle") (v "0.5.0") (d (list (d (n "oracle") (r "^0.5.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "0axhv3m0q0v5d68wqmjamd1a9lshv6kbbrrhgdakwhjlra6kb9gc") (f (quote (("chrono" "oracle/chrono"))))))

(define-public crate-r2d2-oracle-0.6.0 (c (n "r2d2-oracle") (v "0.6.0") (d (list (d (n "oracle") (r "^0.5.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "0zynd512z823rdsa7a7ghss460r0hx87gj5dlndfpch4kndc54p5") (f (quote (("chrono" "oracle/chrono")))) (r "1.56")))

