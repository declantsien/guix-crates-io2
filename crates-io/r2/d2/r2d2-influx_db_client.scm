(define-module (crates-io r2 d2 r2d2-influx_db_client) #:use-module (crates-io))

(define-public crate-r2d2-influx_db_client-0.1.0 (c (n "r2d2-influx_db_client") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "influx_db_client") (r "^0.3.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "00fxdvwzgryv3rm5amylvah90ycx6zkamjx1d8ki5nnff75xpkby")))

(define-public crate-r2d2-influx_db_client-0.1.1 (c (n "r2d2-influx_db_client") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "influx_db_client") (r "^0.3.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "1zxpz26inf409ji8vjvvdy21xymsci94h2d34v4bp53ycg0mvva7")))

