(define-module (crates-io r2 d2 r2d2_redis2) #:use-module (crates-io))

(define-public crate-r2d2_redis2-0.23.3 (c (n "r2d2_redis2") (v "0.23.3") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "redis") (r "^0.23.3") (d #t) (k 0)))) (h "169gd98n7micwbx6d2vjbixdgfnvfd0k43zbkwnrijv01c5pf55h")))

