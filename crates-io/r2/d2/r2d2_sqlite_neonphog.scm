(define-module (crates-io r2 d2 r2d2_sqlite_neonphog) #:use-module (crates-io))

(define-public crate-r2d2_sqlite_neonphog-0.18.0 (c (n "r2d2_sqlite_neonphog") (v "0.18.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0vyfwmpw0k87w95zl2s4aaj52sqmdh14hr93qx4j19gw0caiqs3m") (f (quote (("sqlcipher" "rusqlite/sqlcipher") ("bundled-sqlcipher-vendored-openssl" "rusqlite/bundled-sqlcipher-vendored-openssl") ("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite_neonphog-0.1.0 (c (n "r2d2_sqlite_neonphog") (v "0.1.0") (d (list (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("trace"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "177l0z1byami8ficjx0ga59cvdppibhzp529bqnfb754hyrra7jd") (f (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

