(define-module (crates-io r2 ap r2api) #:use-module (crates-io))

(define-public crate-r2api-5.6.0 (c (n "r2api") (v "5.6.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0mrccg2igg9b9cvmjrvg31n7n8wzyrbb502lcmnwrxm8x0dajgbm") (l "r_core")))

(define-public crate-r2api-5.6.1 (c (n "r2api") (v "5.6.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1rmij8cl46gnfwb1wlpm5x2ma0arq3y2bn4s4glz5rbn5smrvyhf") (l "r_core")))

(define-public crate-r2api-5.8.0 (c (n "r2api") (v "5.8.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1ynzfphaxyfk3bgbii0vs1f1zgghk4srvhgyaj7a0gkzz46n21an") (l "r_core")))

(define-public crate-r2api-5.9.0 (c (n "r2api") (v "5.9.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "0bn5n89hw3i3av7s0b5f667i8d3986i1f7aswkxvk1m8vgc7xhlk") (f (quote (("static")))) (l "r_core")))

