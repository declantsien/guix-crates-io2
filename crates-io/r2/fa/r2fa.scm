(define-module (crates-io r2 fa r2fa) #:use-module (crates-io))

(define-public crate-r2fa-0.1.0 (c (n "r2fa") (v "0.1.0") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.31") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "1m9bnagjqzq1aw4mkq4mzlljzas31b0kpg9449v43la081f8cbdr")))

(define-public crate-r2fa-0.1.1 (c (n "r2fa") (v "0.1.1") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.31") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "0i4nzmshk33hpb0wm9gv9g9yajxq37gs71a8m60hl09krgx81dhx")))

(define-public crate-r2fa-0.2.1 (c (n "r2fa") (v "0.2.1") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.31") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "1aqh50k7d13wqj147s9chpyj23i7v4ya1ziirhjq7c26kn1j457x")))

(define-public crate-r2fa-0.3.0 (c (n "r2fa") (v "0.3.0") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.31") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "1ji9lb80n5vndkrw5y4fzhazxh28j4rq2hr5b4q44qhs430dx399")))

(define-public crate-r2fa-0.4.0 (c (n "r2fa") (v "0.4.0") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.31") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "0qbhdyayi3wvri0jcncp1nzln4dlk1xs77h8fbi6dickj16m68wb")))

(define-public crate-r2fa-0.4.1 (c (n "r2fa") (v "0.4.1") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.31") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "1sl48yqyq603wgh1miybbljrv1x6j9z9y206an8y8bmdyfn5jx66") (f (quote (("cbindings" "libc"))))))

(define-public crate-r2fa-0.4.2 (c (n "r2fa") (v "0.4.2") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.31") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "134s92i6p58ncrwzz26471kjlnh5iva3a7wbm6jqadzifwniqh9h") (f (quote (("cbindings" "libc"))))))

(define-public crate-r2fa-0.5.0 (c (n "r2fa") (v "0.5.0") (d (list (d (n "base32") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.31") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "13qfkgfjm89lz2ph57j1xfv95agmgy44sqjqg5hfkbabkj5pzfby") (f (quote (("cbindings" "libc"))))))

