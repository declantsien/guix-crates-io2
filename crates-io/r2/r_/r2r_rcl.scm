(define-module (crates-io r2 r_ r2r_rcl) #:use-module (crates-io))

(define-public crate-r2r_rcl-0.2.0 (c (n "r2r_rcl") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "itertools") (r "^0.10.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "r2r_common") (r "^0.2.0") (d #t) (k 1)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)))) (h "11fb7d7s1npgdrlmjp4s1k2g02mcqj25yz6fp5qx1269h653gwff")))

(define-public crate-r2r_rcl-0.3.0 (c (n "r2r_rcl") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "itertools") (r "^0.10.3") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "r2r_common") (r "^0.3.0") (d #t) (k 1)) (d (n "widestring") (r "^0.5.1") (d #t) (k 0)))) (h "0vv243qrgvx1gk2g078i8rvqp0vzjcvzdlnfkwha2wc5sd3f7lhb")))

(define-public crate-r2r_rcl-0.3.1 (c (n "r2r_rcl") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "itertools") (r "^0.10.3") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "r2r_common") (r "^0.3.1") (d #t) (k 1)) (d (n "widestring") (r "^0.5.1") (d #t) (k 0)))) (h "0ikxfxxjhik4b792b6v6hf9cbmxip9pgxw4q4l3fnc0ypa3a2df6")))

(define-public crate-r2r_rcl-0.3.2 (c (n "r2r_rcl") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "r2r_common") (r "^0.3.2") (d #t) (k 1)) (d (n "widestring") (r "^0.5.1") (d #t) (k 0)))) (h "0x4g0bcxxmk57jkiplfmz2nq5mvl8qb6igmzzdky668ikd53ydld")))

(define-public crate-r2r_rcl-0.3.3 (c (n "r2r_rcl") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "r2r_common") (r "^0.3.3") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "06b2dfg9adnbd24i7njhzjwy1ncd8hirpcvrlqb855wm33rs338l") (f (quote (("save-bindgen") ("doc-only"))))))

(define-public crate-r2r_rcl-0.3.4 (c (n "r2r_rcl") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "r2r_common") (r "^0.3.4") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "18yjmvb4v8ddjs8blpk1qry9xm8wlhl7npz5bixqgz36qvg1znfp") (f (quote (("save-bindgen") ("doc-only"))))))

(define-public crate-r2r_rcl-0.3.5 (c (n "r2r_rcl") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "r2r_common") (r "^0.3.4") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0nyvqnvay9022n79369gv4djri3995fp043425b80zjw6qv3bx8q") (f (quote (("save-bindgen") ("doc-only"))))))

(define-public crate-r2r_rcl-0.3.6 (c (n "r2r_rcl") (v "0.3.6") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "r2r_common") (r "^0.3.5") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1nl7vlzbqsyjaijbvfqacy9d6830drv8lgd5jbhqkz2li4dzsrc5") (f (quote (("save-bindgen") ("doc-only"))))))

(define-public crate-r2r_rcl-0.3.7 (c (n "r2r_rcl") (v "0.3.7") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "r2r_common") (r "^0.3.6") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "043bcwd9dh2g1plzgm0k9ryg0pa9f9h6729ifmm3avcwil1z71ii") (f (quote (("save-bindgen") ("doc-only" "r2r_common/doc-only"))))))

(define-public crate-r2r_rcl-0.3.8 (c (n "r2r_rcl") (v "0.3.8") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "r2r_common") (r "^0.3.7") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0cnf4166by134fs4j6za5xn71crbysyvnsscp5g5ldf836fbzzhx") (f (quote (("save-bindgen") ("doc-only" "r2r_common/doc-only"))))))

(define-public crate-r2r_rcl-0.3.9 (c (n "r2r_rcl") (v "0.3.9") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "r2r_common") (r "^0.3.8") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1g2n6ydaca471163mxizb7d5m6bgksa4bf380y531vy6kaymjlxx") (f (quote (("save-bindgen") ("doc-only" "r2r_common/doc-only"))))))

(define-public crate-r2r_rcl-0.8.0 (c (n "r2r_rcl") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "r2r_common") (r "^0.8.0") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0cayakk120nz27r1ghydr6m3c0f39lhaxiy2ri6ds3jk7356xny2") (f (quote (("save-bindgen") ("doc-only" "r2r_common/doc-only"))))))

(define-public crate-r2r_rcl-0.8.1 (c (n "r2r_rcl") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "r2r_common") (r "^0.8.1") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1bn4rk5887jrikq9rlm7r8rhyjcdgnr1jsm5rmpx05kq4s7l33b1") (f (quote (("save-bindgen") ("doc-only" "r2r_common/doc-only"))))))

(define-public crate-r2r_rcl-0.8.2 (c (n "r2r_rcl") (v "0.8.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "r2r_common") (r "^0.8.2") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0r5wfabs550zrfb86kclp2jd662ph59cv776szpcs8ckwk2p4dwa") (f (quote (("save-bindgen") ("doc-only" "r2r_common/doc-only"))))))

(define-public crate-r2r_rcl-0.8.3 (c (n "r2r_rcl") (v "0.8.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "r2r_common") (r "^0.8.3") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1rl02b66iv2vrisj5k4vi8iji3wghyw34p032wvhghnc87x67plf") (f (quote (("save-bindgen") ("doc-only" "r2r_common/doc-only"))))))

(define-public crate-r2r_rcl-0.8.4 (c (n "r2r_rcl") (v "0.8.4") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "r2r_common") (r "^0.8.4") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1345ppfs98c9m12ba2xs3x03sq4g733q7q5a3b7rvfqyy4pzswx7") (f (quote (("save-bindgen") ("doc-only" "r2r_common/doc-only"))))))

(define-public crate-r2r_rcl-0.9.0 (c (n "r2r_rcl") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "r2r_common") (r "^0.9.0") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "00czfhq0mxdr68my67m50mxvdpyg9m04zcc322a13bwdmcdzgnm6") (f (quote (("save-bindgen") ("doc-only" "r2r_common/doc-only"))))))

