(define-module (crates-io r2 r_ r2r_macros) #:use-module (crates-io))

(define-public crate-r2r_macros-0.8.0 (c (n "r2r_macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "02qi1r6dq31vvr4r4hb49pgnd8m7c5sbg8f39gvc1nrh9p4g9whp")))

(define-public crate-r2r_macros-0.8.1 (c (n "r2r_macros") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "1x2bpc0zhinnk5c234x3dlzdj4jmi26yb284clcp4i3f7gqp30xk")))

(define-public crate-r2r_macros-0.8.2 (c (n "r2r_macros") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "1pam5jys9j22y2983x88irczgkmncx4ijcwwc3nglcs46r3pk3zj")))

(define-public crate-r2r_macros-0.8.3 (c (n "r2r_macros") (v "0.8.3") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "05d5kax7wkzwnpwlxg3r23dnjr339d8v7lp4vdcyhj65yndrcx2g")))

(define-public crate-r2r_macros-0.8.4 (c (n "r2r_macros") (v "0.8.4") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "0gs0jb2vmhs7gciij17gxhm9hh8q8363q337ca21rg8wvv98gwsg")))

(define-public crate-r2r_macros-0.9.0 (c (n "r2r_macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "15l7b8jjszs6siz0l5bp9qlcsiwmazfrrac8jvxp8mkm0brfv1vv")))

