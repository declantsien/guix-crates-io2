(define-module (crates-io r2 r_ r2r_common) #:use-module (crates-io))

(define-public crate-r2r_common-0.2.0 (c (n "r2r_common") (v "0.2.0") (h "0g6izbbhrbpbj41vb8li3v35yfp8qii6hbg37h359xr8amqrxi5x")))

(define-public crate-r2r_common-0.3.0 (c (n "r2r_common") (v "0.3.0") (h "1r6jwk0dd2vas2gy97pk2146zfmqqb5k2hvmdh4xzycala2v6fyf")))

(define-public crate-r2r_common-0.3.1 (c (n "r2r_common") (v "0.3.1") (h "1rfshb5j8x7y62xx3slihq8iall7nldwajrdq1731hxn1w78rg49")))

(define-public crate-r2r_common-0.3.2 (c (n "r2r_common") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "07yl2fvk2vi17zfasl0wxnb3hi16c5677g7ya3x0vfpmrvih91fj")))

(define-public crate-r2r_common-0.3.3 (c (n "r2r_common") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "04dy5bzjgnr6v2143wjy61124v4vzqczfy3rbcqd3az0p1481wmf")))

(define-public crate-r2r_common-0.3.4 (c (n "r2r_common") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0pg42hxfv0ahxfx2br9hjd6nxq0f2bnrnnl11hxr3zvk69gqy3pv")))

(define-public crate-r2r_common-0.3.5 (c (n "r2r_common") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0pcjnkildrlqz27ynhdpjs0hv7ksyc8ai3x3jkby1d9a85cczc1r")))

(define-public crate-r2r_common-0.3.6 (c (n "r2r_common") (v "0.3.6") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1449gpqxi891qc9p9q2amlnfpx29gvm9s4lwv68la94zvx98wbpc") (f (quote (("doc-only"))))))

(define-public crate-r2r_common-0.3.7 (c (n "r2r_common") (v "0.3.7") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1kmdjf3jnazhak1xrhz6x9rha6j9p722i12pncfyhvnz29ddd48r") (f (quote (("doc-only"))))))

(define-public crate-r2r_common-0.3.8 (c (n "r2r_common") (v "0.3.8") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1ybsgbc6cfh1is0szd0xbr628ph7cjpqkpiilvyvnhc3xr7wahvq") (f (quote (("doc-only"))))))

(define-public crate-r2r_common-0.8.0 (c (n "r2r_common") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0yqs3zx67mpsfxrkvsv26h40gdxdix8s5nxhxj127w646ykmpvyq") (f (quote (("doc-only"))))))

(define-public crate-r2r_common-0.8.1 (c (n "r2r_common") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "01fcbiiq38819gnaba4l545lvkz1qndnh9818rhdkzrjai6v65yc") (f (quote (("doc-only"))))))

(define-public crate-r2r_common-0.8.2 (c (n "r2r_common") (v "0.8.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1kx0w6i8l6988m25fgmnlgq758yplkvbb6gd4c0nz6b1g9hkm5s3") (f (quote (("doc-only"))))))

(define-public crate-r2r_common-0.8.3 (c (n "r2r_common") (v "0.8.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "03d9jiiayqi5za3i0cdflkwzxcwfcn80v7msvwv0l1qc52m6nbg6") (f (quote (("doc-only"))))))

(define-public crate-r2r_common-0.8.4 (c (n "r2r_common") (v "0.8.4") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "16s5azwl9cvxwqkdng9h94gmw7frsskcmwxkpwd1zd4dz4b5abad") (f (quote (("doc-only"))))))

(define-public crate-r2r_common-0.9.0 (c (n "r2r_common") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1vgbk0c7fxj4sjnadcd66wfdlv2f6g17z0nzscv8l1vc3kmyvb4b") (f (quote (("doc-only"))))))

