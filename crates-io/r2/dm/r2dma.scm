(define-module (crates-io r2 dm r2dma) #:use-module (crates-io))

(define-public crate-r2dma-0.1.0 (c (n "r2dma") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "r2dma-sys") (r "^0") (d #t) (k 0)) (d (n "tracing") (r "^0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0") (d #t) (k 0)))) (h "0vvlysvfqd863z7a35l6i4x7az4nwlfzmalsaw93fyaf4vg6kn2h")))

