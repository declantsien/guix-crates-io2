(define-module (crates-io r2 dm r2dma-sys) #:use-module (crates-io))

(define-public crate-r2dma-sys-0.1.0 (c (n "r2dma-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "pkg-config") (r "^0") (d #t) (k 1)))) (h "1grhrd8lvrwhn3zlgvzhkqslfn508bqbm8hq2zrh4y259caihh5p")))

