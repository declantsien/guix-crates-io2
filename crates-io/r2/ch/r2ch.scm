(define-module (crates-io r2 ch r2ch) #:use-module (crates-io))

(define-public crate-r2ch-0.1.0 (c (n "r2ch") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)))) (h "0nzrm4lx2xpj0325zgjwbxfgavcyn5nb2hnjfj6hlyd9clcb9gff")))

(define-public crate-r2ch-0.1.1 (c (n "r2ch") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0mb5w4xdpcb1zi9cqirwbniiv37qdpal5l62m8j9pj10vfx8zjsq")))

(define-public crate-r2ch-0.1.2 (c (n "r2ch") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0mxvfib6d3x64c0va33l3adgg6h9a8fnyw801980hhxw93bp6dzi")))

