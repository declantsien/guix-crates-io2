(define-module (crates-io r2 pi r2pipe) #:use-module (crates-io))

(define-public crate-r2pipe-0.1.0 (c (n "r2pipe") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "08d2241jrpb1jdf44rzk5bahgjf2ksh80b1dhnjjwfdhwjad3g73")))

(define-public crate-r2pipe-0.1.1 (c (n "r2pipe") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1n809vgf29w8zm1zsqcbiiyc7zw9dvx75md24ygsrgbij0bif1lq")))

(define-public crate-r2pipe-0.4.0 (c (n "r2pipe") (v "0.4.0") (d (list (d (n "clippy") (r "^0.0.61") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.13") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0wp9kgpfxd488ifkyca9cvaz36wsvxi8m7j843y4538l06hiyyi5") (f (quote (("default"))))))

(define-public crate-r2pipe-0.5.0 (c (n "r2pipe") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sixd95gijl94pkpcphsgd2844g7bw8vmfkk04ggvrafyzp7zp3k") (f (quote (("default"))))))

(define-public crate-r2pipe-0.6.0 (c (n "r2pipe") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ianlskgs7sfic84l489nki5g8xw5f46g1nc1kv6h4mz98qm3pzm")))

(define-public crate-r2pipe-0.7.0 (c (n "r2pipe") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0axgnijvphds7rrqafrcda68xwwdwdiih5pd42cgb0yvvxw30i33")))

