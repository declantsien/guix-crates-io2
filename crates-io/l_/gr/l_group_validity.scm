(define-module (crates-io l_ gr l_group_validity) #:use-module (crates-io))

(define-public crate-l_group_validity-0.1.0 (c (n "l_group_validity") (v "0.1.0") (d (list (d (n "l_group_cnf") (r "^0.1.0") (d #t) (k 0)) (d (n "l_group_formulas") (r "^0.1.0") (d #t) (k 0)) (d (n "truncated_free_groups") (r "^0.1.0") (d #t) (k 0)))) (h "1a39vz272jspyxh3vd0qdkdrzrzm9a0a1dkqrriz9azf0v9847r5")))

