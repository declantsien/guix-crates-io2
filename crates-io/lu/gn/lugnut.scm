(define-module (crates-io lu gn lugnut) #:use-module (crates-io))

(define-public crate-lugnut-0.0.1 (c (n "lugnut") (v "0.0.1") (h "170g4xqi3bkfwzybhbwslzg1p5nh59qgrrf1j27nnjzi0h5mb7fm")))

(define-public crate-lugnut-0.0.2 (c (n "lugnut") (v "0.0.2") (d (list (d (n "crypto-mac") (r "^0.10") (f (quote ("std"))) (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "149m6pp1x6gs4p554xw4lrgy9yyl1sj19qzc8mciq4k6ssmhjlmf")))

(define-public crate-lugnut-0.0.3 (c (n "lugnut") (v "0.0.3") (d (list (d (n "crypto-mac") (r "^0.10") (f (quote ("std"))) (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0bpljx6xay6z9nsvlpi8f2ccc7g0ha7yrakyqisdy231kvvkblxy")))

(define-public crate-lugnut-0.0.4 (c (n "lugnut") (v "0.0.4") (d (list (d (n "crypto-mac") (r "^0.10") (f (quote ("std"))) (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "11q2jkg193f9gkabn0d95affi0yys9qkw3dkgzqp54xp9k8gh6ab")))

(define-public crate-lugnut-0.0.5 (c (n "lugnut") (v "0.0.5") (d (list (d (n "crypto-mac") (r "^0.10") (f (quote ("std"))) (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1r4sbsj3l7y91ym439jxnqd58kv9s3hdk02g61llghzim8ijqb7y")))

