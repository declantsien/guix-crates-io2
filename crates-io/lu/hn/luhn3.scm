(define-module (crates-io lu hn luhn3) #:use-module (crates-io))

(define-public crate-luhn3-1.0.0 (c (n "luhn3") (v "1.0.0") (h "0hkpdjh8xgw0p7z3py095fmpbh6m9wi094q455srb8m569mfpqhx")))

(define-public crate-luhn3-1.0.1 (c (n "luhn3") (v "1.0.1") (h "0g5zbic7amzhxvz5scdvwmjz3bxl3fyzgykm2vj9jcf69j071j6i")))

(define-public crate-luhn3-1.0.2 (c (n "luhn3") (v "1.0.2") (h "0xfxygfhhzf3b47q92ir1qa07sym9nxhsqrf51bxzxwj6f2zh1iw")))

(define-public crate-luhn3-1.0.3 (c (n "luhn3") (v "1.0.3") (h "13dcf94qvk5zq7pf9b28p5jcnlyx8a8f95md0wgrh3rb8v7zg7dh")))

(define-public crate-luhn3-1.0.4 (c (n "luhn3") (v "1.0.4") (h "1sflbdq4z9z92mkaq5nsxca9wp228v2bq7xlqcfss03awfyzqqcb") (y #t)))

(define-public crate-luhn3-1.1.0 (c (n "luhn3") (v "1.1.0") (h "1xk7m8dpgzif8ap1i2mznzmhbs8mwz9nm69baad8d53j7iyvpj7p")))

