(define-module (crates-io lu hn luhn2) #:use-module (crates-io))

(define-public crate-luhn2-0.1.0 (c (n "luhn2") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-digitize") (r "^0.1") (d #t) (k 0)))) (h "0ifx47bg7mdk6jinhb4gy8dj7911pw39ql2f7dfl30kxpcqaaclk")))

(define-public crate-luhn2-0.1.1 (c (n "luhn2") (v "0.1.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-digitize") (r "^0.1") (d #t) (k 0)))) (h "02167rkhvrjqgx44v5a4y4lfz04qsi4ijpfkn4gikamnsqan797z")))

(define-public crate-luhn2-0.1.2 (c (n "luhn2") (v "0.1.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-digitize") (r "^0.4") (d #t) (k 0)))) (h "19125ii0dq4n4ipa4qkwrvysgln1j3pq2x1hkv91kk21vg88vjjk")))

