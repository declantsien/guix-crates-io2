(define-module (crates-io lu hn luhnr) #:use-module (crates-io))

(define-public crate-luhnr-0.1.0 (c (n "luhnr") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x3d96sjqabvw18axf1bj7znfk4bb0ziklg69vl5dj5vav332lar") (y #t)))

(define-public crate-luhnr-0.2.0 (c (n "luhnr") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "15g9mqjsg9nqq8jjw43h2gd9c313f442biyhjpfdyz4bvafzg78j") (y #t)))

(define-public crate-luhnr-0.3.0 (c (n "luhnr") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0d18gz8mq91mzn3apbjsf6m4hp9cndb09z9djqwrfpzv39h3dvhd")))

(define-public crate-luhnr-0.3.1 (c (n "luhnr") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0vc7gd6jld0cmrcqx2c6ikl8lhx5839wy9z8n882bsybxc39q6fx")))

(define-public crate-luhnr-0.3.2 (c (n "luhnr") (v "0.3.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0sv1cr9x0ql5nwjlrkvb9y3iflchhng66jvyzca6jmdgdlxkx9nh")))

(define-public crate-luhnr-0.3.3 (c (n "luhnr") (v "0.3.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "15v07pb4gln7xb5f2pira86rgjwvarrgkx8bzrwz68y70q1106gq")))

(define-public crate-luhnr-0.3.4 (c (n "luhnr") (v "0.3.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0ipknbaha8bn59xdfh6cr4ysszdw3p59wvh3mv78kka38z242pa7")))

