(define-module (crates-io lu hn luhncalc) #:use-module (crates-io))

(define-public crate-luhncalc-1.0.0 (c (n "luhncalc") (v "1.0.0") (d (list (d (n "digits_iterator") (r "^0.1") (d #t) (k 0)) (d (n "luhn") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1nnmbychyhavahv6xn18jbsbaszkliq8m9brd00hf0hmrqlvpx5l") (y #t)))

(define-public crate-luhncalc-1.0.1 (c (n "luhncalc") (v "1.0.1") (d (list (d (n "digits_iterator") (r "^0.1") (d #t) (k 0)) (d (n "luhn") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0m0j059m1b79y8jvx1ybs6g40vk0m1s6whb5kpyjlv0p5yja1h03")))

