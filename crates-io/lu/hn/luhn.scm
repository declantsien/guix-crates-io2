(define-module (crates-io lu hn luhn) #:use-module (crates-io))

(define-public crate-luhn-0.1.0 (c (n "luhn") (v "0.1.0") (h "031jd1x1wdyhdg0p9ygqyqq6kssp4bpqi38w7d4gxz58364i8m9d")))

(define-public crate-luhn-1.0.0 (c (n "luhn") (v "1.0.0") (d (list (d (n "digits_iterator") (r "^0.1") (d #t) (k 0)))) (h "0kl6fdwpc1hfkwbc2rs507a5n978gls9ipivbf2lrj1c779zp9fm")))

(define-public crate-luhn-1.0.1 (c (n "luhn") (v "1.0.1") (d (list (d (n "digits_iterator") (r "^0.1") (d #t) (k 0)))) (h "02vhz2xbkk1s76pwn6arfb86fjilqc77zg6833h10fra80qvh42d")))

