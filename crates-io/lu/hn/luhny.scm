(define-module (crates-io lu hn luhny) #:use-module (crates-io))

(define-public crate-luhny-0.1.0 (c (n "luhny") (v "0.1.0") (d (list (d (n "cliply") (r "^0.2.0") (d #t) (k 0)) (d (n "coutils") (r "^1.5.0") (d #t) (k 0)))) (h "1hkyscmnhzw3y7hlh5y1hk541g04wx739rh88nfx1bmiw4977iqr")))

(define-public crate-luhny-0.2.0 (c (n "luhny") (v "0.2.0") (d (list (d (n "cliply") (r "^0.2.0") (d #t) (k 0)) (d (n "coutils") (r "^1.5.0") (d #t) (k 0)))) (h "065scgs0m6w0q13dr5kiwm06qnbv7vx3aa0756dw4h2ryj4clh70")))

(define-public crate-luhny-0.3.0 (c (n "luhny") (v "0.3.0") (d (list (d (n "cliply") (r "^0.2.0") (d #t) (k 0)) (d (n "coutils") (r "^1.5.0") (d #t) (k 0)))) (h "1x5kfganvddv6i1fxmff0b5r24l1vynxq5x3jfnsc4fx2n9qxfr3")))

