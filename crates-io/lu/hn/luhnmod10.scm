(define-module (crates-io lu hn luhnmod10) #:use-module (crates-io))

(define-public crate-luhnmod10-1.0.0 (c (n "luhnmod10") (v "1.0.0") (h "0lvqsjzk3x792ml1dnakci21084g4c9bs5c0n077v6nbqphbr2rs")))

(define-public crate-luhnmod10-1.1.0 (c (n "luhnmod10") (v "1.1.0") (h "0hsxmpxnvlay5rbc1khh4zxzl9wrngl5fp1ypda6qj5yvrsax5fv")))

