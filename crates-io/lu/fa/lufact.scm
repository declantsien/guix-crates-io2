(define-module (crates-io lu fa lufact) #:use-module (crates-io))

(define-public crate-lufact-0.1.0 (c (n "lufact") (v "0.1.0") (d (list (d (n "lufact-sys") (r "^0.2") (d #t) (k 0)))) (h "0n4vjqs2gqn2fp3qz73yyz4yapdxgcsqbfzpclg4jq0m62v17cqq")))

(define-public crate-lufact-0.1.1 (c (n "lufact") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lufact-sys") (r "^0.2") (d #t) (k 0)))) (h "0ikbmq83wdgwqq34svmlj3rv8zvm27vgfpmik43g2nfgqyxq75g2")))

(define-public crate-lufact-0.1.2 (c (n "lufact") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lufact-sys") (r "^0.2") (d #t) (k 0)))) (h "0771zirhyhgkzr2d4j89spmay2mzgi0a0nzkmvfmwlibpn336v57")))

