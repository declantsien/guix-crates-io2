(define-module (crates-io lu fa lufact-sys) #:use-module (crates-io))

(define-public crate-lufact-sys-0.1.0 (c (n "lufact-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v7y9vf6vk2pqw74xhg4z13rggw0cdn2kf6bc2fabn7h5cydxc1w")))

(define-public crate-lufact-sys-0.2.0 (c (n "lufact-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xfdy8wg5dy33q427q29x77xvj8967zwn6sf478yi02z5yaiwmb8")))

(define-public crate-lufact-sys-0.2.1 (c (n "lufact-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d1yjhkaza81l5s03xr7pk1qjv8i117aa024yvisrvb7f4y4hr3m")))

