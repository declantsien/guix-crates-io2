(define-module (crates-io lu x- lux-ik) #:use-module (crates-io))

(define-public crate-lux-ik-0.1.0 (c (n "lux-ik") (v "0.1.0") (d (list (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)))) (h "026ljjz8b3fv71mdbw081n4s82w1h0j9w2vsqw2kn11sq87xxn9g")))

(define-public crate-lux-ik-0.1.1 (c (n "lux-ik") (v "0.1.1") (d (list (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)))) (h "0yf34mqjy4jsfq0zh8p95cxkvady89bwp4f8pwfhczids8fxksbm")))

