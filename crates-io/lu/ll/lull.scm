(define-module (crates-io lu ll lull) #:use-module (crates-io))

(define-public crate-lull-1.0.1 (c (n "lull") (v "1.0.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "gdk") (r "^0.13.2") (d #t) (k 0)) (d (n "gio") (r "^0") (d #t) (k 0)) (d (n "gtk") (r "^0") (d #t) (k 0)) (d (n "rodio") (r "^0.11.0") (d #t) (k 0)))) (h "0v1z7d0ii6a8jgjz68vfdwz6pxynjdr292vxd3xqmifsfxbgwp9i")))

(define-public crate-lull-1.0.2 (c (n "lull") (v "1.0.2") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "gdk") (r "^0.13.2") (d #t) (k 0)) (d (n "gio") (r "^0") (d #t) (k 0)) (d (n "gtk") (r "^0") (d #t) (k 0)) (d (n "rodio") (r "^0.11.0") (d #t) (k 0)))) (h "0l23lzwp5bhgy6kgxmq5l0piw218p70n0z1kb3nz07szcnxrvjh1")))

