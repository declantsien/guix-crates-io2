(define-module (crates-io lu ke luke) #:use-module (crates-io))

(define-public crate-luke-0.1.0 (c (n "luke") (v "0.1.0") (d (list (d (n "openai") (r "^0.3.0") (d #t) (k 0)))) (h "1xbznpfhgs8lxxh1h7g15kayqbz1vag9fb60j5wv7bbask14vx56") (y #t)))

(define-public crate-luke-0.1.1 (c (n "luke") (v "0.1.1") (d (list (d (n "openai") (r "^0.3.0") (d #t) (k 0)))) (h "1i5fj5sg9l3ixvqgwcbaa1cmp8mbg4v8gih6hb83slr00mrnbcfb") (y #t)))

(define-public crate-luke-0.1.2 (c (n "luke") (v "0.1.2") (d (list (d (n "openai") (r "^0.3.0") (d #t) (k 0)))) (h "0mk221p7kpa5ns34bsv4mhhzn7l2fjnnfv0sl7nmmirrx3d9v376") (y #t)))

(define-public crate-luke-0.1.4 (c (n "luke") (v "0.1.4") (d (list (d (n "openai") (r "^0.4.0") (d #t) (k 0)))) (h "1g4x0jjdwbx46cj01qlyn9ppqrma04539r9wvya4sh6nzv99s2lv") (y #t)))

