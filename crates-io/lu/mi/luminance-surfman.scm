(define-module (crates-io lu mi luminance-surfman) #:use-module (crates-io))

(define-public crate-luminance-surfman-0.1.0 (c (n "luminance-surfman") (v "0.1.0") (d (list (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "luminance") (r "^0.43.2") (d #t) (k 0)) (d (n "luminance-glow") (r "^0.1.0") (d #t) (k 0)) (d (n "surfman") (r "^0.4.0") (f (quote ("sm-x11"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 0)))) (h "191lqdba392gprckfgkllgiyqkp0c7nd7vva05c105yixxr9qvaf")))

