(define-module (crates-io lu mi luminal_macro) #:use-module (crates-io))

(define-public crate-luminal_macro-0.2.0 (c (n "luminal_macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "1xl52f4gi22ap9dbbyjq1bh9xrngrsiq68wwfbzv3qshkb8qs7ra")))

