(define-module (crates-io lu mi luminal-router) #:use-module (crates-io))

(define-public crate-luminal-router-0.0.1 (c (n "luminal-router") (v "0.0.1") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "13gcsbnbl1wy2ykmnd4fnz5vv7a00677dk0rik1yzkdjdl04klf6")))

(define-public crate-luminal-router-0.0.2 (c (n "luminal-router") (v "0.0.2") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "1l1g9rzwdax9jpwmg1zl7yx2vmag0ix6g81549pd9zszmlampgi3")))

(define-public crate-luminal-router-0.0.3 (c (n "luminal-router") (v "0.0.3") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "1ya2kyx68k2l94dvlqbhqm974yg21gv4171qy7s2q80442c6hgc2")))

(define-public crate-luminal-router-0.0.5 (c (n "luminal-router") (v "0.0.5") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "1js8hjnha71cajy85r4wrp60q9yx4lj84m60y7bk3wgz89zy62kk")))

(define-public crate-luminal-router-0.0.6 (c (n "luminal-router") (v "0.0.6") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "1mk1i9cfsnx2id06cw0z6r7df18d0kd2by9s7hrspdrmy1yikwgq")))

(define-public crate-luminal-router-0.0.7 (c (n "luminal-router") (v "0.0.7") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0pjn835fkjy4qaklhfkvzagi0ff23fwjp44171yqbhnj8yl8w9z6")))

(define-public crate-luminal-router-0.0.8 (c (n "luminal-router") (v "0.0.8") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "luminal-handler") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0g4xxwcry4amci72746pcwzwsybbjkdm8g6mfyz4v7s5p5w4sf69") (f (quote (("handler" "luminal-handler"))))))

(define-public crate-luminal-router-0.0.9 (c (n "luminal-router") (v "0.0.9") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "luminal-handler") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "1ybzs0wdl0wx1h08n2bkql7mjbbssvrx4xmww73bnydyh3am8z1d") (f (quote (("handler" "luminal-handler"))))))

(define-public crate-luminal-router-0.0.10 (c (n "luminal-router") (v "0.0.10") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "luminal-handler") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "1q5j2wivp69sc0j39b85d6b4nh52ki4cnl7l2a9yc9jma9xsij0c") (f (quote (("handler" "luminal-handler"))))))

(define-public crate-luminal-router-0.0.11 (c (n "luminal-router") (v "0.0.11") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.11") (f (quote ("compat"))) (d #t) (k 0)) (d (n "luminal-handler") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0s32c6gmnwyd83z9fx5xw2kn2gfmf2kkjm4pdjsrbsfndlxzq96l") (f (quote (("handler" "http" "luminal-handler"))))))

(define-public crate-luminal-router-0.0.12 (c (n "luminal-router") (v "0.0.12") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.11") (f (quote ("compat"))) (d #t) (k 0)) (d (n "luminal-handler") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0wws850k8vmdnbwgv0mwmljbdfv264q2m5rk9qq7df2qf9jnb56r") (f (quote (("handler" "http" "luminal-handler"))))))

