(define-module (crates-io lu mi luminance-sdl2) #:use-module (crates-io))

(define-public crate-luminance-sdl2-0.1.0 (c (n "luminance-sdl2") (v "0.1.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.40") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.14") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.2") (d #t) (k 0)))) (h "1lccvbsdxqca5gk896src2z4fjxg6knrwfag8v4l3n4kf5lmirdh") (f (quote (("default" "bundled") ("bundled" "sdl2/bundled"))))))

(define-public crate-luminance-sdl2-0.1.1 (c (n "luminance-sdl2") (v "0.1.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r ">=0.40, <0.42") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.14") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.2") (d #t) (k 0)))) (h "03sixiy29fy2s8l81dv92l0fa9x2ycgacaaih49bb68msrj5pbik") (f (quote (("default" "bundled") ("bundled" "sdl2/bundled"))))))

(define-public crate-luminance-sdl2-0.2.0 (c (n "luminance-sdl2") (v "0.2.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.42") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.15") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "17y6rab0k3smpb5shz0d9s16qiqjv925srr3rk4lg9dfaaasvcs8") (f (quote (("default" "bundled") ("bundled" "sdl2/bundled"))))))

(define-public crate-luminance-sdl2-0.2.1 (c (n "luminance-sdl2") (v "0.2.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r ">=0.42, <0.44") (d #t) (k 0)) (d (n "luminance-gl") (r ">=0.15, <0.17") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "0lkmvvrrnn4qb8lk7b6a55wi8jm1l0mcbzahqlzn106zh3fkvpv4") (f (quote (("default" "bundled") ("bundled" "sdl2/bundled"))))))

(define-public crate-luminance-sdl2-0.2.2 (c (n "luminance-sdl2") (v "0.2.2") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r ">=0.42, <0.44") (d #t) (k 0)) (d (n "luminance-gl") (r ">=0.15, <0.17") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "1hkqhlbx6rk45cxaqydcwinzkfmj0vcn7x0m2p31plrc7mp7hk5f") (f (quote (("default" "bundled") ("bundled" "sdl2/bundled"))))))

(define-public crate-luminance-sdl2-0.3.0 (c (n "luminance-sdl2") (v "0.3.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.44") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.17") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "1w9n1cjrlaljapgjikqdphzvi48dclcshnnfl7h037bzs4mzm61a") (f (quote (("default" "bundled") ("bundled" "sdl2/bundled"))))))

(define-public crate-luminance-sdl2-0.4.0 (c (n "luminance-sdl2") (v "0.4.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.45") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.18") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.1") (d #t) (k 0)))) (h "1j2a2mdhjj3s7gzbda6dr46mmifzx6kvg1amn8s26nwhfqar999w") (f (quote (("default" "bundled") ("bundled" "sdl2/bundled")))) (r "1.56.0")))

(define-public crate-luminance-sdl2-0.5.1 (c (n "luminance-sdl2") (v "0.5.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r ">=0.46, <0.48") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.19") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.1") (d #t) (k 0)))) (h "1p2q6ln7vdj67ldlq3by9knc6xj1g0ccd3qx27nybm3pwzzxqsay") (f (quote (("default" "bundled") ("bundled" "sdl2/bundled")))) (r "1.56.0")))

