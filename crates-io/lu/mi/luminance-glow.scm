(define-module (crates-io lu mi luminance-glow) #:use-module (crates-io))

(define-public crate-luminance-glow-0.1.0 (c (n "luminance-glow") (v "0.1.0") (d (list (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "glow") (r "^0.9.0") (d #t) (k 0)) (d (n "luminance") (r "^0.43") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("WebGlRenderingContext" "WebGl2RenderingContext"))) (d #t) (k 0)))) (h "1jigi700r0na6mknrb7vv2n9ac70g78xxj17pfk1amjf861a5dwi")))

