(define-module (crates-io lu mi luminal_metal) #:use-module (crates-io))

(define-public crate-luminal_metal-0.2.0 (c (n "luminal_metal") (v "0.2.0") (d (list (d (n "dfdx") (r "^0.13") (f (quote ("f16"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "luminal") (r "^0.2.0") (d #t) (k 0)) (d (n "metal-rs") (r "^0.27.0") (f (quote ("mps"))) (d #t) (k 0) (p "metal")) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0wqb2j2bbxcd0z5nwr8j5zdlcd5765zw07a2ma9cbvgga1mzla75")))

