(define-module (crates-io lu mi luminance-glutin) #:use-module (crates-io))

(define-public crate-luminance-glutin-0.1.0 (c (n "luminance-glutin") (v "0.1.0") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (k 0)) (d (n "luminance") (r "^0.31") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.3") (d #t) (k 0)))) (h "0dgf7wnywqs4kbfni24gky0viyfdhlr7b1kiq63m83kvfn3l5894") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.1.1 (c (n "luminance-glutin") (v "0.1.1") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (k 0)) (d (n "luminance") (r "^0.32") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.3") (d #t) (k 0)))) (h "1zr7dipm8bhw9sc048mk05kj81dfnvlw6ibc0ymmaf7h0bszkcnx") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.2.0 (c (n "luminance-glutin") (v "0.2.0") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (k 0)) (d (n "luminance") (r "^0.33") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.4") (d #t) (k 0)))) (h "008l92dkl3yxg4jllr1bj7dfg0rvlvw7bpvzbpjy6fgj7x6whhc0") (f (quote (("serde" "glutin/serde")))) (y #t)))

(define-public crate-luminance-glutin-0.3.0 (c (n "luminance-glutin") (v "0.3.0") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (k 0)) (d (n "luminance") (r "^0.34") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.5") (d #t) (k 0)))) (h "19n719nwkrkhq7kqmgrw96f4q5ll97hf8qc6rfn2jjj4xfkjf2fr") (f (quote (("serde" "glutin/serde")))) (y #t)))

(define-public crate-luminance-glutin-0.3.1 (c (n "luminance-glutin") (v "0.3.1") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (k 0)) (d (n "luminance") (r ">= 0.34, < 0.36") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.5") (d #t) (k 0)))) (h "0xz9vi8hqqzqzkq9xv80q5ya2rzsirr6s15wrzna3bai67x0p5rv") (f (quote (("serde" "glutin/serde")))) (y #t)))

(define-public crate-luminance-glutin-0.4.0 (c (n "luminance-glutin") (v "0.4.0") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (k 0)) (d (n "luminance") (r "^0.35") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.6") (d #t) (k 0)))) (h "1cw7xsv5rpw2ibr7d7myqywz1r54hswbxxsklhv3drdkz85l928a") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.5.0 (c (n "luminance-glutin") (v "0.5.0") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (k 0)) (d (n "luminance") (r "^0.36") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.7") (d #t) (k 0)))) (h "14ddn9fgizrcfmc4bg27jiv293x49d0c1hzlvmc0jqalbj14ilk6") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.6.0 (c (n "luminance-glutin") (v "0.6.0") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (k 0)) (d (n "luminance") (r "^0.37") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.8") (d #t) (k 0)))) (h "1qfhy2w40vqqy42pqcvili5bddwbg02k2gp2p69ikyw6x9a0xjl3") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.6.1 (c (n "luminance-glutin") (v "0.6.1") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (k 0)) (d (n "luminance") (r "^0.37") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.8") (d #t) (k 0)))) (h "0vky3b5lqj5948z32b6hmhackbld5lcp9qg5qdnvr6jfrr3fw5b5") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.7.0 (c (n "luminance-glutin") (v "0.7.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (k 0)) (d (n "luminance") (r "^0.38") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.8") (d #t) (k 0)))) (h "1vz2rz3hp97dib2ykz0z4y27dz04v9xv2csaxvdvrnh0ivkb95zy") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.8.0 (c (n "luminance-glutin") (v "0.8.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (k 0)) (d (n "luminance") (r "^0.38") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.8") (d #t) (k 0)))) (h "1rxa04qrbrq1kby6d3phswz6cg4c64bizc8y2c8j7g33wbgfsgwg") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.8.1 (c (n "luminance-glutin") (v "0.8.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (k 0)) (d (n "luminance") (r "^0.38") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.8") (d #t) (k 0)))) (h "0jcd9ipkv6lxflnhww41wk5gv9fk0zp0v3lwpvwgl3k5dnznlvhd") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.8.2 (c (n "luminance-glutin") (v "0.8.2") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (k 0)) (d (n "luminance") (r "^0.38") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.8") (d #t) (k 0)))) (h "0bmirig7km6zqqrrz4c6ffygrnjvxakcd78k4x99jq4k6ima2zfz") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.9.0 (c (n "luminance-glutin") (v "0.9.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.23") (k 0)) (d (n "luminance") (r "^0.38") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.8") (d #t) (k 0)))) (h "0mzv4kpgqsszghyvy4v12dxpx2wlwdwf6n5gvlacv91gwrinxpsr") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.9.1 (c (n "luminance-glutin") (v "0.9.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.23") (k 0)) (d (n "luminance") (r ">= 0.38, < 0.40") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.8") (d #t) (k 0)))) (h "10n1swz5wphyqid316vhrw4vcwpb62q0yj4km2jirhd367rnazif") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.10.0 (c (n "luminance-glutin") (v "0.10.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (k 0)) (d (n "luminance") (r "^0.40") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "0xsq6jxci8rxyb5is56skxaqvpy8qy9jayfl55m3gaylxwy8wjqv") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.10.1 (c (n "luminance-glutin") (v "0.10.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (k 0)) (d (n "luminance") (r ">=0.40, <0.42") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "16xcqajhlxmj2hs5g4jpji075wcimjqq9sxv4vpwn99rzwpq1vbc") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.11.0 (c (n "luminance-glutin") (v "0.11.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (k 0)) (d (n "luminance") (r "^0.42") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.15") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "0jmzccg6g7hgkl05gybwr6j02ifai5c8sdz1zk5yyqlr7x02fxmm") (f (quote (("serde" "glutin/serde"))))))

(define-public crate-luminance-glutin-0.11.1 (c (n "luminance-glutin") (v "0.11.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.25") (k 0)) (d (n "luminance") (r "^0.42") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.15") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "07l7fp0059iq6v98hcq9a61j63gl8lj8hk64r2dbnn1a61bmamhi") (f (quote (("x11" "glutin/x11") ("wayland" "glutin/wayland") ("serde" "glutin/serde") ("default" "x11" "wayland"))))))

(define-public crate-luminance-glutin-0.11.2 (c (n "luminance-glutin") (v "0.11.2") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.25") (k 0)) (d (n "luminance") (r ">=0.42, <0.44") (d #t) (k 0)) (d (n "luminance-gl") (r ">=0.15, <0.17") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "1m2gkbmcwa5wi86kirlfcn2xgiq3jbb01dm5g8hilwclpg68zmx5") (f (quote (("x11" "glutin/x11") ("wayland" "glutin/wayland") ("serde" "glutin/serde") ("default" "x11" "wayland"))))))

(define-public crate-luminance-glutin-0.11.3 (c (n "luminance-glutin") (v "0.11.3") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r ">=0.25, <0.27") (k 0)) (d (n "luminance") (r ">=0.42, <0.44") (d #t) (k 0)) (d (n "luminance-gl") (r ">=0.15, <0.17") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "1lchq4djakymprz90j4072ba0xp3vrgj3bsb875ay45m46srfsvz") (f (quote (("x11" "glutin/x11") ("wayland" "glutin/wayland") ("serde" "glutin/serde") ("default" "x11" "wayland"))))))

(define-public crate-luminance-glutin-0.12.0 (c (n "luminance-glutin") (v "0.12.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.27") (k 0)) (d (n "luminance") (r "^0.44") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.17") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.10") (d #t) (k 0)))) (h "00v7jyi7m429j3mmyd26fdlafxywq8j0q4kq5hkw9jvsqq7pxpnm") (f (quote (("x11" "glutin/x11") ("wayland" "glutin/wayland") ("serde" "glutin/serde") ("default" "x11" "wayland"))))))

(define-public crate-luminance-glutin-0.13.0 (c (n "luminance-glutin") (v "0.13.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.27") (k 0)) (d (n "luminance") (r "^0.45") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.18") (d #t) (k 0)))) (h "16p9i17hnc5am40yfyydsllm2vs35iyv100f0rvmnkyjjhx38dw5") (f (quote (("x11" "glutin/x11") ("wayland" "glutin/wayland") ("serde" "glutin/serde") ("default" "x11" "wayland")))) (r "1.56.0")))

(define-public crate-luminance-glutin-0.14.0 (c (n "luminance-glutin") (v "0.14.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (k 0)) (d (n "luminance") (r "^0.46") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.19") (d #t) (k 0)))) (h "0b024vbnj4a3ag0qlsd8b87jcf21jsdvpvmqsvnwy32bvhwrb9d4") (f (quote (("x11" "glutin/x11") ("wayland" "glutin/wayland") ("serde" "glutin/serde") ("default" "x11" "wayland")))) (r "1.56.0")))

(define-public crate-luminance-glutin-0.14.1 (c (n "luminance-glutin") (v "0.14.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (k 0)) (d (n "luminance") (r ">=0.46, <0.48") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.19") (d #t) (k 0)))) (h "1jjmaqm9wck09czayp789gac71klgk2mzmp14bf7kd008b342b1l") (f (quote (("x11" "glutin/x11") ("wayland" "glutin/wayland") ("serde" "glutin/serde") ("default" "x11" "wayland")))) (r "1.56.0")))

