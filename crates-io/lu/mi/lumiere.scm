(define-module (crates-io lu mi lumiere) #:use-module (crates-io))

(define-public crate-lumiere-0.1.0 (c (n "lumiere") (v "0.1.0") (h "173cszd492q8w9zw8fbzxhs6s4wq53nj2xzgpq8qc0afgj5dcfcj")))

(define-public crate-lumiere-0.2.0 (c (n "lumiere") (v "0.2.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "png") (r "^0.17.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1sv5gqvrfa2hbgyr8y79z5m4v4pmvpnzya2rm55jfa2axlivm8a2")))

