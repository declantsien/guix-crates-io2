(define-module (crates-io lu mi luminance) #:use-module (crates-io))

(define-public crate-luminance-0.1.0 (c (n "luminance") (v "0.1.0") (h "0ymp289zjvzd9ayxjwplgvp3kxgr5p0c2jrhn9jlqqamav6lb1zk")))

(define-public crate-luminance-0.2.0 (c (n "luminance") (v "0.2.0") (h "1j2nir43j41ilkhajj4b36la5ay3ry7q5yqrj87rbhxgw8i1d2zg")))

(define-public crate-luminance-0.2.1 (c (n "luminance") (v "0.2.1") (h "13jhkzr878cf3vz8a780zk1r3p0r8ckhydxyb58wdh9zdfzwhwav")))

(define-public crate-luminance-0.3.0 (c (n "luminance") (v "0.3.0") (h "18bc6yxp3ncrazk0fx7kkha1hwimmqrdhdrrk3w63q37n93cz7gb")))

(define-public crate-luminance-0.3.1 (c (n "luminance") (v "0.3.1") (h "0g1r145906kh7v6d9062bxxq9xlh84qbii6lz5zmsp1frbmmwgpr")))

(define-public crate-luminance-0.4.0 (c (n "luminance") (v "0.4.0") (h "1g9qn89p4gnaab3hywk1mwqdmf8wh974xsxfhyhi0l20jwn3j8sa")))

(define-public crate-luminance-0.5.0 (c (n "luminance") (v "0.5.0") (h "03nqikqzw8xyyc6wc8shm3x1nwjk3m6wc9w1l4bn7msxjhaf72cc")))

(define-public crate-luminance-0.5.1 (c (n "luminance") (v "0.5.1") (h "1jsp0zqy70lj6j2yqpac50miiqkia97xxk52cdhw0rx24naqpbg0")))

(define-public crate-luminance-0.5.2 (c (n "luminance") (v "0.5.2") (h "11jdlmxmk638p8dbl2zkqkzkjqd2vr1qxc85xg1knag0fkq7krhc")))

(define-public crate-luminance-0.5.3 (c (n "luminance") (v "0.5.3") (h "1xc7i5mmi1dn1pidwxvrsamx3zk89y71lr482vafw45qcrgw6244")))

(define-public crate-luminance-0.6.0 (c (n "luminance") (v "0.6.0") (h "0pl478rydfxsgmcr6i3q389rf7ppnja6srqkqh4azx99j4hdi1bp")))

(define-public crate-luminance-0.6.1 (c (n "luminance") (v "0.6.1") (h "17g9shz1wgmd70yvmfvjzlzzy42w9qmraqxb6fpp0hdxmfvr2hyw")))

(define-public crate-luminance-0.6.2 (c (n "luminance") (v "0.6.2") (h "1z8wvvjria7ra8pmgl2va60zhmzbpwc5sk0aaj6076k6z3z5r1jm")))

(define-public crate-luminance-0.6.3 (c (n "luminance") (v "0.6.3") (h "1kjvagwabbx1b476pb0m6jyqdyp6jsla3z2g7h65vfk04r8xiia8")))

(define-public crate-luminance-0.6.4 (c (n "luminance") (v "0.6.4") (h "17p8xbnxq1q9azdy6429rld6jin6wp694ifkq9cb37w5dfz222kh")))

(define-public crate-luminance-0.7.0 (c (n "luminance") (v "0.7.0") (h "18im208cj8ds2hqqyfdwwmx5xjrxwd2j6mg3pr22xq8vx5arnap9")))

(define-public crate-luminance-0.8.0 (c (n "luminance") (v "0.8.0") (h "15zxhh1fpar0kw0hm99m1c1ylp0y8kcf77ghhy6zxa0hnhfi43c1")))

(define-public crate-luminance-0.9.0 (c (n "luminance") (v "0.9.0") (h "0vb4jj49qfngzg3v2zyba6dkisl4f4sdk0qqn92n1cb8g7m6rgln")))

(define-public crate-luminance-0.9.1 (c (n "luminance") (v "0.9.1") (h "17825nmsmdafnga9wih6vlc29wgaxcd5rpahl4ni2m5k0wjmx7bn")))

(define-public crate-luminance-0.10.0 (c (n "luminance") (v "0.10.0") (h "02gafmv9h0zk5a8d3qmkgyi24hmld3cs7jnbz4063aq410fmn85b")))

(define-public crate-luminance-0.11.0 (c (n "luminance") (v "0.11.0") (h "1vvf1mjql7l04caqbq58p05lfiw82aqhmzqv6lv826mr5y03p4rq")))

(define-public crate-luminance-0.12.0 (c (n "luminance") (v "0.12.0") (h "1m4b11b96s1iy28yk5869w9xbkbmaw8v5xvrx6z5fic1ic3nbimf")))

(define-public crate-luminance-0.12.1 (c (n "luminance") (v "0.12.1") (h "0dj3qqqh7m0rkjvsjg6iwivyibnp81h2r7ws1mi7f262izm7jasx")))

(define-public crate-luminance-0.13.0 (c (n "luminance") (v "0.13.0") (h "1si7xbdxcdzvy1cxm3w75kd50mn6pmprwva0cakh2d728h3ddpkl")))

(define-public crate-luminance-0.13.1 (c (n "luminance") (v "0.13.1") (h "0jcv6dz49hwqxg7h2bizhi0cxpgxddmjh70yfp78n8yaig7rz8j2")))

(define-public crate-luminance-0.14.0 (c (n "luminance") (v "0.14.0") (h "1k0dcmqgcjckr9r3nxsn7h29rmq5bfamw1plsyjccnarf7rsl121")))

(define-public crate-luminance-0.15.0 (c (n "luminance") (v "0.15.0") (h "0bbm4vxkmz74i12xzncqzf6z9m6274493x4pg83x2a451xx5j2hd")))

(define-public crate-luminance-0.16.0 (c (n "luminance") (v "0.16.0") (h "1sb7hfljd4ay6fchxfpfqnxmmgh2adjwyhg52lzs2jl57v76rm7f")))

(define-public crate-luminance-0.17.0 (c (n "luminance") (v "0.17.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)))) (h "1fzgv6yb0f7r9m3fqnxpgpra9cnxswyrhbyaqrpiz37wfhyn2s4w")))

(define-public crate-luminance-0.17.1 (c (n "luminance") (v "0.17.1") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)))) (h "0drsa1vc0665brwhy2rggsr77af5310pb41xaj3j608wfljfchcq")))

(define-public crate-luminance-0.17.2 (c (n "luminance") (v "0.17.2") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)))) (h "072wl5rzxb3rxiyhi9qip7p5q6h7pw2ijbss8ackcqpx91479vip")))

(define-public crate-luminance-0.18.0 (c (n "luminance") (v "0.18.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)))) (h "1rbwcnly4p9ifranmfn8bf3kfijdhwjl7375372ks8ahmg4fdq5m")))

(define-public crate-luminance-0.18.1 (c (n "luminance") (v "0.18.1") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "03pgkiw34qbh4lr0ph8sdm41dm2mmjfar0fa7xmzj4s8823hp82m")))

(define-public crate-luminance-0.18.2 (c (n "luminance") (v "0.18.2") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "02cx8dfi3hnp9q1f5q731h7k7a2nxb222gbhlxi2z8pr0qx1cmc1")))

(define-public crate-luminance-0.19.0 (c (n "luminance") (v "0.19.0") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "0a0pzfxaq7xz9d927054gv9skqhlv0wz1y3mdj946h9rdw4v2dk5")))

(define-public crate-luminance-0.20.0 (c (n "luminance") (v "0.20.0") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "162c86i5yncwlcmcqp0m80ap43a0f017mai4pqdp1pknpzd610h3")))

(define-public crate-luminance-0.21.0 (c (n "luminance") (v "0.21.0") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "1f4k8c1vbv4rph12alf1r0zval15nv94x8d4idng3s7v1jqqhwcr")))

(define-public crate-luminance-0.21.1 (c (n "luminance") (v "0.21.1") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "17qqkppy0pmxq4jhhpm9d4rpi6d85zgn214nzg2nr80kpyf1s8i0")))

(define-public crate-luminance-0.21.2 (c (n "luminance") (v "0.21.2") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "1vffizx6chgqwqk1i5wfxyr07wb6hfqkwbv67vb547gxfkzmpv70")))

(define-public crate-luminance-0.21.3 (c (n "luminance") (v "0.21.3") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "02fpk6mspxia72jdj7gz637yk3d49ahzipmiqk905zh7h4h9yg6z")))

(define-public crate-luminance-0.22.0 (c (n "luminance") (v "0.22.0") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "16318cr52w1hhn9kg21k1kgq3wbl1ifxvl718xnhy9krfqwnqnkg")))

(define-public crate-luminance-0.22.1 (c (n "luminance") (v "0.22.1") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "1sas4kf4v9w3vdlx7pa2za3fcbz2r8ayxdcdz856dlkz4g4c0p3x")))

(define-public crate-luminance-0.22.2 (c (n "luminance") (v "0.22.2") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "0l6p3lfn6jgfgh0rsgviv1lnl5k27rjb10z73qs2rqhpcgsgqmsq")))

(define-public crate-luminance-0.22.3 (c (n "luminance") (v "0.22.3") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "0rsdxzy31cfcyrc98myaxh53snwvi0mcq8kxnzg1lhb7cgdv5d8q")))

(define-public crate-luminance-0.22.4 (c (n "luminance") (v "0.22.4") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "0akivxprb721r0h4sr74pz51vxkamdjpi1wdm9s4zvzqs1g1533l")))

(define-public crate-luminance-0.22.5 (c (n "luminance") (v "0.22.5") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "0ar4ciafqzigbxlbcqqrm05z2x6jsbac6arpz4bicqwmmaj3j57q")))

(define-public crate-luminance-0.22.6 (c (n "luminance") (v "0.22.6") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "1iwnakhjgh5jjxc5qkjk3375lfnqack1dp9abvdrnr2h0bscy4nm")))

(define-public crate-luminance-0.22.7 (c (n "luminance") (v "0.22.7") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "1rb39hzsgw784ixlbkx5v5vszywf33gcwkh0ppql1igsla6lzg5g")))

(define-public crate-luminance-0.23.0 (c (n "luminance") (v "0.23.0") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "088shgyjv9xqfinj7mjlr3pj8ihyw5yazqx3qsw7yfyv3k07wgzv")))

(define-public crate-luminance-0.23.1 (c (n "luminance") (v "0.23.1") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "05swxcwgihag258fx24s58as23jq2w6psccfndnza7d2k2ri7gcv")))

(define-public crate-luminance-0.24.0 (c (n "luminance") (v "0.24.0") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "01y5p7jmn22c2jwqs7c39rbb3glh27db1xxyj0ky0aa3hicxgf12")))

(define-public crate-luminance-0.24.1 (c (n "luminance") (v "0.24.1") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "1462cjqb42cz97i01vnsrk07jl315qfw2aq35cmrszm1wf4slhfc")))

(define-public crate-luminance-0.25.0 (c (n "luminance") (v "0.25.0") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "1pd2pylzj7dqm9hj942qfakbgwcyz9qbpbg9b50s8ywb4l2j6yda")))

(define-public crate-luminance-0.25.1 (c (n "luminance") (v "0.25.1") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "1p5d54cz025465plby4sdr6cpc518f0g46h8r0ir9ss73hqv25rf")))

(define-public crate-luminance-0.25.2 (c (n "luminance") (v "0.25.2") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "07y9zshzfs6chc3kvix5wdnswadr6kpsyfn6zjycy22b3mbs13a8")))

(define-public crate-luminance-0.25.3 (c (n "luminance") (v "0.25.3") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "0xm22f295ipbr3msiiiw2fj03c7hg4xhiii6shflbfj6yn2s98yy")))

(define-public crate-luminance-0.25.4 (c (n "luminance") (v "0.25.4") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)))) (h "1vqjrkscgsywfv6k2bp350rn2ajylsd5y9gijnhy3b0slwzis9y6")))

(define-public crate-luminance-0.25.5 (c (n "luminance") (v "0.25.5") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)))) (h "1zn065jq25cd3lnhxi3g0k1asrmhmp6fzy593mmc1klvg8wbrrgh")))

(define-public crate-luminance-0.25.6 (c (n "luminance") (v "0.25.6") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)))) (h "1wyr2lkbvgcdgwgdjamg4904x5nj9qgd8s72nryaqwlcf6l6sksm")))

(define-public crate-luminance-0.25.7 (c (n "luminance") (v "0.25.7") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)))) (h "0c43gbkmyvpq6mb5i550qgldz2px7sr5pdpa9q7l141l5fczwdan")))

(define-public crate-luminance-0.26.0 (c (n "luminance") (v "0.26.0") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)))) (h "00kn74dv653hz0g3bf7dhcsw9x4rg9lc46xbqxqa7kxazpiw7m4c")))

(define-public crate-luminance-0.27.0 (c (n "luminance") (v "0.27.0") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)))) (h "0bdsxcnsxzsdy8v1qi99kj5anxgdxpmgi9f3nnzpczc036r16lcw")))

(define-public crate-luminance-0.27.1 (c (n "luminance") (v "0.27.1") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)))) (h "1qasg3vphaiimm9i6w6254w054vwmh0by6zainphkb08slzk02f8")))

(define-public crate-luminance-0.27.2 (c (n "luminance") (v "0.27.2") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)))) (h "0prwvw84c35jszcm049figbz1nwc80rimn5zjyzkhn58pvn12hpx")))

(define-public crate-luminance-0.28.0 (c (n "luminance") (v "0.28.0") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)))) (h "0m31qxxy259rj1936bqbqv7qw48rgxxahrw1wr64dbbc534jsr59")))

(define-public crate-luminance-0.29.0 (c (n "luminance") (v "0.29.0") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)))) (h "15m640xm06bxhf49i4z698z99x8nfv30cvav4i725lv3i3kfpcny")))

(define-public crate-luminance-0.30.0 (c (n "luminance") (v "0.30.0") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)))) (h "0lnw5pnmdwmlr48kdvsvbfpzqy3g5n25dw27k4wsbabq8b66cf0q")))

(define-public crate-luminance-0.30.1 (c (n "luminance") (v "0.30.1") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)))) (h "0s51mpz4p4791whl1phv2q4n4w7mzsc8bv118s4ijvnhicw3a8xb")))

(define-public crate-luminance-0.31.0 (c (n "luminance") (v "0.31.0") (d (list (d (n "gl") (r "^0.13") (o #t) (d #t) (k 0)))) (h "0rxh6ypcwphhwqr13s30vq9im7nhdgd6kmkr5wgxk7ahjbbcgnf2") (f (quote (("std" "gl") ("default" "std"))))))

(define-public crate-luminance-0.31.1 (c (n "luminance") (v "0.31.1") (d (list (d (n "gl") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "luminance-derive") (r "^0.1") (d #t) (k 2)) (d (n "luminance-glfw") (r "^0.6") (d #t) (k 2)) (d (n "luminance-glutin") (r "^0.1") (d #t) (k 2)))) (h "1n3mjgjvp7d4njq4hazznc7lqyysvbgch1zhdd1jpzw0w08jr27j") (f (quote (("std" "gl") ("default" "std"))))))

(define-public crate-luminance-0.32.0 (c (n "luminance") (v "0.32.0") (d (list (d (n "gl") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "luminance-derive") (r "^0.1") (d #t) (k 2)) (d (n "luminance-glfw") (r "^0.6") (d #t) (k 2)) (d (n "luminance-glutin") (r "^0.1") (d #t) (k 2)))) (h "1bvsqjcaxc4ya2jzs6v2zrdb4hshgrgax5bif8lww1kqlh3x2361") (f (quote (("std" "gl") ("default" "std"))))))

(define-public crate-luminance-0.33.0 (c (n "luminance") (v "0.33.0") (d (list (d (n "gl") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "luminance-derive") (r "^0.1") (d #t) (k 2)) (d (n "luminance-glfw") (r "^0.6") (d #t) (k 2)) (d (n "luminance-glutin") (r "^0.1") (d #t) (k 2)))) (h "0rf65idhhfjixksshd18aa77a38aghvihh5w4wj15lks98jcrq6g") (f (quote (("std" "gl") ("default" "std")))) (y #t)))

(define-public crate-luminance-0.34.0 (c (n "luminance") (v "0.34.0") (d (list (d (n "gl") (r "^0.13") (o #t) (d #t) (k 0)))) (h "1ppnr7azycn7df9rg1j4w17g8dprp1w0k7cydpl1f98n1xa92khf") (f (quote (("std" "gl") ("default" "std")))) (y #t)))

(define-public crate-luminance-0.34.1 (c (n "luminance") (v "0.34.1") (d (list (d (n "gl") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "luminance-derive") (r "^0.2") (d #t) (k 2)) (d (n "luminance-glfw") (r "^0.8") (d #t) (k 2)) (d (n "luminance-glutin") (r "^0.3") (d #t) (k 2)))) (h "11f3lwsmzv198b1y4m2k74xpp2ckxcvxr1w2wj8al9pp9cx011yd") (f (quote (("std" "gl") ("default" "std")))) (y #t)))

(define-public crate-luminance-0.35.0 (c (n "luminance") (v "0.35.0") (d (list (d (n "gl") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "luminance-derive") (r "^0.2") (d #t) (k 2)) (d (n "luminance-glfw") (r "^0.8") (d #t) (k 2)) (d (n "luminance-glutin") (r "^0.3") (d #t) (k 2)))) (h "0qj4ss3xrmkkna2c9cpl2ci2laxlykp58h9m62q315460m738m5f") (f (quote (("std" "gl") ("default" "std")))) (y #t)))

(define-public crate-luminance-0.35.1 (c (n "luminance") (v "0.35.1") (d (list (d (n "gl") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "luminance-derive") (r "^0.3") (d #t) (k 2)) (d (n "luminance-glfw") (r "^0.9") (d #t) (k 2)) (d (n "luminance-glutin") (r "^0.4") (d #t) (k 2)))) (h "1jmprncfcxly08n489gsiqwfvb0qh83l101vnp6lrii8x0i8i4rz") (f (quote (("std" "gl") ("default" "std"))))))

(define-public crate-luminance-0.36.0 (c (n "luminance") (v "0.36.0") (d (list (d (n "gl") (r "^0.13") (o #t) (d #t) (k 0)))) (h "0h1j7pw63q3w4q2zmss47kg109spp5f811qms3j5bh1gb6w0rjh4") (f (quote (("std" "gl") ("default" "std"))))))

(define-public crate-luminance-0.36.1 (c (n "luminance") (v "0.36.1") (d (list (d (n "gl") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "luminance-derive") (r "^0.4") (d #t) (k 2)) (d (n "luminance-glfw") (r "^0.10") (d #t) (k 2)) (d (n "luminance-glutin") (r "^0.5") (d #t) (k 2)))) (h "09bnvddrmv793fqglbyl6h3ramcxyqjibw28k7kk2ahi1mvp4q1y") (f (quote (("std" "gl") ("default" "std"))))))

(define-public crate-luminance-0.37.0 (c (n "luminance") (v "0.37.0") (d (list (d (n "gl") (r "^0.13") (o #t) (d #t) (k 0)))) (h "1nsw4x4w4703y57ywiyr6jd2gqk4hkd9wlq39v1yk9ikxlrzp56a") (f (quote (("std" "gl") ("default" "std"))))))

(define-public crate-luminance-0.37.1 (c (n "luminance") (v "0.37.1") (d (list (d (n "gl") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "luminance-derive") (r "^0.5") (d #t) (k 2)) (d (n "luminance-glfw") (r "^0.11") (d #t) (k 2)) (d (n "luminance-glutin") (r "^0.6") (d #t) (k 2)))) (h "1d4fj6gjzfzzymjg8j5j2qxw4bn7wlkilnw9k7d9zfshdfl59rmw") (f (quote (("std" "gl") ("default" "std"))))))

(define-public crate-luminance-0.38.0 (c (n "luminance") (v "0.38.0") (d (list (d (n "gl") (r "^0.14") (o #t) (d #t) (k 0)))) (h "0z5glw5yk1bv85ra63dalm533cb5m26y2sk8m601jcr9vf5zz990") (f (quote (("std" "gl") ("default" "std"))))))

(define-public crate-luminance-0.39.0 (c (n "luminance") (v "0.39.0") (d (list (d (n "gl") (r "^0.14") (o #t) (d #t) (k 0)))) (h "09h4yxww0831i78j9ac8bwnlsnd7m88j2zfa419lbv847vvk6x1n") (f (quote (("std" "gl") ("default" "std"))))))

(define-public crate-luminance-0.40.0 (c (n "luminance") (v "0.40.0") (h "1kr8bdqjfb21mnzzp06lk548al4h2n3bpr0d5h2a5n900cjxaw14")))

(define-public crate-luminance-0.41.0 (c (n "luminance") (v "0.41.0") (h "00cgndzjjibm7mn5alxyw0b1bkrbqhj7yy1l1aqyh0jaq8r9mppx")))

(define-public crate-luminance-0.42.0 (c (n "luminance") (v "0.42.0") (h "153p37rqg021qymws9wk6yvf0pr12v6crgj1yba7k8vpsdbrlw3b")))

(define-public crate-luminance-0.42.1 (c (n "luminance") (v "0.42.1") (h "1v0qnggwqlypygbzqc6jk9jwi729s2fm19lshyakbr5wsrvblz8r")))

(define-public crate-luminance-0.42.2 (c (n "luminance") (v "0.42.2") (h "0sbc790yfnx6ighhf15h3j0kr0ii91nxnfk6z0ra10d2r6j2nj90")))

(define-public crate-luminance-0.42.3 (c (n "luminance") (v "0.42.3") (h "00ggiqc62d1498hd9p1252dc5wwhqd3z80lcvkg5r5vk8yr8hbq5")))

(define-public crate-luminance-0.43.0 (c (n "luminance") (v "0.43.0") (h "0d1fa7pn1lr9ay7yvvx0q3mh9r9zinz4akd3a9s3q9p5nvcmk4rb")))

(define-public crate-luminance-0.43.1 (c (n "luminance") (v "0.43.1") (d (list (d (n "luminance-derive") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "1bx3xyxbmnxpymzcf392nrajiadx929z24fialz7magfs8md745g") (f (quote (("derive" "luminance-derive") ("default" "derive"))))))

(define-public crate-luminance-0.43.2 (c (n "luminance") (v "0.43.2") (d (list (d (n "luminance-derive") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "08qai1vb1fjrblb1c66nm4lcg1imihnaaqr11a8lld8ndfgj3r2z") (f (quote (("derive" "luminance-derive") ("default" "derive"))))))

(define-public crate-luminance-0.44.0 (c (n "luminance") (v "0.44.0") (d (list (d (n "luminance-derive") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0k9i9xiylx15j3x2gs6c5qix28m55c0mrkzhykc1674sbl1p1d10") (f (quote (("derive" "luminance-derive") ("default" "derive"))))))

(define-public crate-luminance-0.44.1 (c (n "luminance") (v "0.44.1") (d (list (d (n "luminance-derive") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "14aqhzqxcgi7ghawc00d40ky7z1lcbqif4gqw3yw6i3h48k79wak") (f (quote (("derive" "luminance-derive") ("default" "derive"))))))

(define-public crate-luminance-0.45.0 (c (n "luminance") (v "0.45.0") (d (list (d (n "luminance-derive") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "01p5na4lk3kgx66153lq9f64y7jawqxcp3fd8pyzsd81gv7yzmfg") (f (quote (("derive" "luminance-derive") ("default" "derive")))) (r "1.56.0")))

(define-public crate-luminance-0.46.0 (c (n "luminance") (v "0.46.0") (d (list (d (n "luminance-derive") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0ijq2ylwc4mjf79v8fraz5ckr7fxc30c1p3yjxw4qmms6k92hwm4") (f (quote (("derive" "luminance-derive") ("default" "derive")))) (r "1.56.0")))

(define-public crate-luminance-0.46.1 (c (n "luminance") (v "0.46.1") (d (list (d (n "luminance-derive") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0lg69pn55ah02cpvfsv68rqw3pb131g26hxr1gf132dp3axgy1rb") (f (quote (("derive" "luminance-derive") ("default" "derive")))) (y #t) (r "1.56.0")))

(define-public crate-luminance-0.47.0 (c (n "luminance") (v "0.47.0") (d (list (d (n "luminance-derive") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "07dd8r8dc65ij71xbywzbn2fbqlrrclam76x335qiv16v2937f7d") (f (quote (("derive" "luminance-derive") ("default" "derive")))) (r "1.56.0")))

