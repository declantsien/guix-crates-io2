(define-module (crates-io lu mi lumi) #:use-module (crates-io))

(define-public crate-lumi-0.1.0 (c (n "lumi") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help" "suggestions"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "14nciws59kpppni2ki3r3dcc9580y6ypxy60in8qfgnajij4rwz1")))

(define-public crate-lumi-0.2.0 (c (n "lumi") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1xhbrm7vbq9lphvsd43gck6s12h8jl4909zaiksfaq9w0xbylbbf")))

