(define-module (crates-io lu mi luminance-derive) #:use-module (crates-io))

(define-public crate-luminance-derive-0.1.0 (c (n "luminance-derive") (v "0.1.0") (d (list (d (n "luminance") (r "^0.31") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0svypnin38ikm34grvq543y6179syiiwnlkimkaxmmhcabfp2kj0")))

(define-public crate-luminance-derive-0.1.1 (c (n "luminance-derive") (v "0.1.1") (d (list (d (n "luminance") (r "^0.32") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "07ha3vm2ifp0kg9wwxibbia2zipplwdmkgpggjjvgjbd6slfbjq5")))

(define-public crate-luminance-derive-0.2.0 (c (n "luminance-derive") (v "0.2.0") (d (list (d (n "luminance") (r "^0.33") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "10vc3h4ank46z83v9zfg0927kbhnvm7jh4yfyk6s3855kf18ff4j") (y #t)))

(define-public crate-luminance-derive-0.2.1 (c (n "luminance-derive") (v "0.2.1") (d (list (d (n "luminance") (r ">= 0.33, < 0.35") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1g1fdcs0cj6k1jnll218cm9vqn81aq7i7rmb31zwniags84wh34g") (y #t)))

(define-public crate-luminance-derive-0.2.2 (c (n "luminance-derive") (v "0.2.2") (d (list (d (n "luminance") (r ">= 0.33, < 0.36") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1gx6mnfr63vbq8lh0j2pw0cdlgpfgb2g6hv53b8m08kcvfzsgbz1") (y #t)))

(define-public crate-luminance-derive-0.3.0 (c (n "luminance-derive") (v "0.3.0") (d (list (d (n "luminance") (r "^0.35") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1dg5cnrj8zksk8yh3jmcyammyggjrhwx0dv4kf3bc6k5fni4cdlw")))

(define-public crate-luminance-derive-0.4.0 (c (n "luminance-derive") (v "0.4.0") (d (list (d (n "luminance") (r "^0.36") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ap5jw04bp6kx80i3na167ndahgp4k1g3pgjf2w0km3idbqmvi5r")))

(define-public crate-luminance-derive-0.5.0 (c (n "luminance-derive") (v "0.5.0") (d (list (d (n "luminance") (r "^0.37") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0w9i4b468hapd3s2kviw1lbp0dar74vi8ydx9f12x5j5nm9lk8z3")))

(define-public crate-luminance-derive-0.5.1 (c (n "luminance-derive") (v "0.5.1") (d (list (d (n "luminance") (r ">= 0.37, < 0.39") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ai0i7wlngipnblq5h0qsr5v2c30285x5am471ks425yzmn2pm3x")))

(define-public crate-luminance-derive-0.5.2 (c (n "luminance-derive") (v "0.5.2") (d (list (d (n "luminance") (r ">= 0.37, < 0.39") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ng2b59rrn0s37d3jixpaf27mpxqw8v5ixkx73zi3na87vrpfn96")))

(define-public crate-luminance-derive-0.5.3 (c (n "luminance-derive") (v "0.5.3") (d (list (d (n "luminance") (r ">= 0.37, < 0.40") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1pxavlkh4k8sbql7czm7x74zwj15k8g0mpvwb4rcbirnx57vpn5v")))

(define-public crate-luminance-derive-0.6.0 (c (n "luminance-derive") (v "0.6.0") (d (list (d (n "luminance") (r "^0.40") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0rnsdkr05q41jnnjydl28hjz9rh97wgf9xr3h7fnkd53p54syjns")))

(define-public crate-luminance-derive-0.6.1 (c (n "luminance-derive") (v "0.6.1") (d (list (d (n "luminance") (r ">=0.40, <0.42") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17ib4pya449zyx9531ixybjg87r4yfai94x7iqdy6p345qypgp25")))

(define-public crate-luminance-derive-0.6.2 (c (n "luminance-derive") (v "0.6.2") (d (list (d (n "luminance") (r ">=0.40, <0.43") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0gv8bcmx3h1zlc05pzdfdfn3pmjyq7r8fxvdnclvhcdw9cy1d0aa")))

(define-public crate-luminance-derive-0.6.3 (c (n "luminance-derive") (v "0.6.3") (d (list (d (n "luminance") (r ">=0.40, <0.44") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lhwkwwjyf7zmpmrwymghslsnlchd5ynb835v95l6z9xfkavimmx")))

(define-public crate-luminance-derive-0.7.0 (c (n "luminance-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17khm31i83mh38zfcl1b0jzr3lqc7pg0hj81mp7amna6j7mlrnwp")))

(define-public crate-luminance-derive-0.8.0 (c (n "luminance-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "16qnina759q57y2gpqcpc606drqbv7s4grby5vwpq4l7i65jkkgk") (r "1.56.0")))

(define-public crate-luminance-derive-0.9.0 (c (n "luminance-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1d0jzr4k1fnwbnqypm5n66ringx5hzwnikcn8bn5b9bn3dqp2wdz") (r "1.56.0")))

(define-public crate-luminance-derive-0.10.0 (c (n "luminance-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ppp78fxjikw6kqg0f33x8k5ccgyrggfi0291bmrx8bp63f0vc2b") (r "1.56.0")))

