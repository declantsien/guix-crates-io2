(define-module (crates-io lu mi luminvent-raw-window-handle) #:use-module (crates-io))

(define-public crate-luminvent-raw-window-handle-0.4.3 (c (n "luminvent-raw-window-handle") (v "0.4.3") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.6") (f (quote ("js"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hc6d6z2z5fslm74pwdnrw7pmwkxjh75c3v9pjg8fpa7lf07cjx2") (f (quote (("alloc"))))))

