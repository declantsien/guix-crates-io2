(define-module (crates-io lu mi luminance-glfw) #:use-module (crates-io))

(define-public crate-luminance-glfw-0.1.0 (c (n "luminance-glfw") (v "0.1.0") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.20") (d #t) (k 0)))) (h "1km15klnacg6r8b6c5kiac5l29zp25c7mbmv8a7rbrxpc526s97j")))

(define-public crate-luminance-glfw-0.1.1 (c (n "luminance-glfw") (v "0.1.1") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.20") (d #t) (k 0)))) (h "1zpmhi63xyaljhzr47rx32wipib6y6yz69yc9dzn4pcisqxki9b8")))

(define-public crate-luminance-glfw-0.1.2 (c (n "luminance-glfw") (v "0.1.2") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.20") (d #t) (k 0)))) (h "1r8bg7r5qcihdsz4fynmcymlsnghmkpxiplaaihgd6ih15id7ilg")))

(define-public crate-luminance-glfw-0.1.3 (c (n "luminance-glfw") (v "0.1.3") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.20") (d #t) (k 0)))) (h "04m0lb4byfzl2bysy4db1pcjxcj04vzh26zibmy2ndygfbqhmsqd")))

(define-public crate-luminance-glfw-0.1.4 (c (n "luminance-glfw") (v "0.1.4") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.21") (d #t) (k 0)))) (h "1afmshcc5d7w79nf2gdd6xqlfgk1w0z2gsqb2i96mk5rfdrd7lq3")))

(define-public crate-luminance-glfw-0.1.5 (c (n "luminance-glfw") (v "0.1.5") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.21") (d #t) (k 0)))) (h "07fbi8sa1r9zpaknk1jiaknglbv6p8wbzn84z5qbzs6866w2kmf6")))

(define-public crate-luminance-glfw-0.2.0 (c (n "luminance-glfw") (v "0.2.0") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.21") (d #t) (k 0)))) (h "0z9xqy9h8m313jp93rxy63fg1kyfzp9xgkm2vfjvbk4r6zf1ppp3")))

(define-public crate-luminance-glfw-0.3.0 (c (n "luminance-glfw") (v "0.3.0") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.21") (d #t) (k 0)))) (h "0vnmg15y40q9q7v9flx9jnhy0xvz9va1pch0kn3akdi0hw4gl1yr")))

(define-public crate-luminance-glfw-0.3.1 (c (n "luminance-glfw") (v "0.3.1") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.16") (d #t) (k 0)) (d (n "luminance") (r "^0.21") (d #t) (k 0)))) (h "1vagvwppdfrczw59r372l8c3jlr4z7cp06qnqrib0yg83sjdmhi0")))

(define-public crate-luminance-glfw-0.3.2 (c (n "luminance-glfw") (v "0.3.2") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.16") (d #t) (k 0)) (d (n "luminance") (r "^0.21") (d #t) (k 0)))) (h "12639nj3glw5ar326bfrfxb49lzaw1xkmqxmdd53mdfzn6i6zklb")))

(define-public crate-luminance-glfw-0.3.3 (c (n "luminance-glfw") (v "0.3.3") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.16") (d #t) (k 0)))) (h "0cxc0jjcmbdn63q7zxnjqazkq95ba0fia2wgmg1z6kw0j8f7jd8b")))

(define-public crate-luminance-glfw-0.4.0 (c (n "luminance-glfw") (v "0.4.0") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.16") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.1") (d #t) (k 0)))) (h "0y443dsx2cqjrxx1v1lbi2zi1hx1kf2l1nscc11l2192adkyrpb1")))

(define-public crate-luminance-glfw-0.4.1 (c (n "luminance-glfw") (v "0.4.1") (d (list (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.16") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.1") (d #t) (k 0)))) (h "13l53ch06p0k6ibpxwiz2il570vcki97im9hkppmf3yxq662i5z1")))

(define-public crate-luminance-glfw-0.4.2 (c (n "luminance-glfw") (v "0.4.2") (d (list (d (n "gl") (r "^0.9") (d #t) (k 0)) (d (n "glfw") (r "^0.20") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.1") (d #t) (k 0)))) (h "0d0l6x7h0v8dx1ppjv189kavslvyf1gn99zxyy69aw4pacsk6y3n")))

(define-public crate-luminance-glfw-0.4.3 (c (n "luminance-glfw") (v "0.4.3") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)) (d (n "glfw") (r "^0.20") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.1") (d #t) (k 0)))) (h "06xz0a9i9m22jihgyxhx196hx579qn4md9nmdakp9wv65ai3wk7p")))

(define-public crate-luminance-glfw-0.5.0 (c (n "luminance-glfw") (v "0.5.0") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)) (d (n "glfw") (r "^0.23") (d #t) (k 0)) (d (n "luminance") (r "^0.26") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.2") (d #t) (k 0)))) (h "0lby5kw34w2nsi4blsavxbzspvlb1h7yhjdi4679l7iyvjgw4hc1")))

(define-public crate-luminance-glfw-0.5.1 (c (n "luminance-glfw") (v "0.5.1") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)) (d (n "glfw") (r "^0.23") (d #t) (k 0)) (d (n "luminance") (r "^0.27") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.2") (d #t) (k 0)))) (h "0s7rqh7bvpar08qadxmflfljw11ifd4jkx4zabm8jgxn0ldibhkr")))

(define-public crate-luminance-glfw-0.5.2 (c (n "luminance-glfw") (v "0.5.2") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)) (d (n "glfw") (r "^0.23") (d #t) (k 0)) (d (n "luminance") (r "^0.28") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.2") (d #t) (k 0)))) (h "1rh679x6ldghasanp4ajk6ckzvr9bim6wzb7bc9ai7wpy23lkfb7")))

(define-public crate-luminance-glfw-0.5.3 (c (n "luminance-glfw") (v "0.5.3") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)) (d (n "glfw") (r "^0.23") (d #t) (k 0)) (d (n "luminance") (r "^0.29") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.2") (d #t) (k 0)))) (h "085nhjqkl8a7a84r1i8yyrj9bqqch6hnb694rdy3jmpkcbwsnzs4")))

(define-public crate-luminance-glfw-0.5.4 (c (n "luminance-glfw") (v "0.5.4") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)) (d (n "glfw") (r "^0.23") (d #t) (k 0)) (d (n "luminance") (r "^0.30") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.2") (d #t) (k 0)))) (h "0mwampqvhz3rjq5a892jpjfalmmjdzqn5dw7y6jgrqrmwmc9b7fd")))

(define-public crate-luminance-glfw-0.6.0 (c (n "luminance-glfw") (v "0.6.0") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glfw") (r "^0.31") (k 0)) (d (n "luminance") (r "^0.31") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.3") (d #t) (k 0)))) (h "031srll5alb2dk4dradzrvk2didh4d12al7al8ayr83rfkkk0zkj") (f (quote (("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.6.1 (c (n "luminance-glfw") (v "0.6.1") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glfw") (r "^0.31") (k 0)) (d (n "luminance") (r "^0.32") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.3") (d #t) (k 0)))) (h "06fp7vil2qcqqzy2g9m4wbgikrchxf7hkbxfigpbfii4mr68l0yj") (f (quote (("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.7.0 (c (n "luminance-glfw") (v "0.7.0") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glfw") (r "^0.31") (k 0)) (d (n "luminance") (r "^0.33") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.4") (d #t) (k 0)))) (h "0xd5x4lx857vv1ysh873rhdydnndi3jdxy9yc8vs8diw78mibw64") (f (quote (("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys")))) (y #t)))

(define-public crate-luminance-glfw-0.8.0 (c (n "luminance-glfw") (v "0.8.0") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glfw") (r "^0.31") (k 0)) (d (n "luminance") (r "^0.34") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.5") (d #t) (k 0)))) (h "1323g4x2f9q37myzbayd5a0y10njpza98k9pii1ivvrh0201m2d5") (f (quote (("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys")))) (y #t)))

(define-public crate-luminance-glfw-0.8.1 (c (n "luminance-glfw") (v "0.8.1") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glfw") (r "^0.31") (k 0)) (d (n "luminance") (r ">= 0.34, < 0.36") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.5") (d #t) (k 0)))) (h "041dr3c1nysiqpsw16krdmxrhv40437249n9gz0h75n6dx8zscpy") (f (quote (("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys")))) (y #t)))

(define-public crate-luminance-glfw-0.9.0 (c (n "luminance-glfw") (v "0.9.0") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glfw") (r "^0.31") (k 0)) (d (n "luminance") (r "^0.35") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.6") (d #t) (k 0)))) (h "07r3vn0xav8n4rcfzr3q2m1ni9qm5lnqrb85d6j4ppid36zmr8sz") (f (quote (("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.10.0 (c (n "luminance-glfw") (v "0.10.0") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glfw") (r "^0.31") (k 0)) (d (n "luminance") (r "^0.36") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.7") (d #t) (k 0)))) (h "0fya1smsyx9clc0yp5q6lz1233mxwb27sg12v8hy8lyn0hmwmil4") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.11.0 (c (n "luminance-glfw") (v "0.11.0") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glfw") (r "^0.31") (k 0)) (d (n "luminance") (r "^0.37") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.8") (d #t) (k 0)))) (h "18yxpy1zkszsvipvyd7fgadk3mv6rms25z0hib68kwphbydyd148") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.11.1 (c (n "luminance-glfw") (v "0.11.1") (d (list (d (n "gl") (r "^0.13") (d #t) (k 0)) (d (n "glfw") (r "^0.33") (k 0)) (d (n "luminance") (r "^0.37") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.8") (d #t) (k 0)))) (h "1cb87akgq398n2pcdf872zip93rzfkzc1lrj805hs86sys4n8jz5") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys")))) (y #t)))

(define-public crate-luminance-glfw-0.12.0 (c (n "luminance-glfw") (v "0.12.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.34") (k 0)) (d (n "luminance") (r "^0.38") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.8") (d #t) (k 0)))) (h "0kflbxg87fnaggy1w6m8l2v46555zs1znw9m7rls2zxd6wh05wbj") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.12.1 (c (n "luminance-glfw") (v "0.12.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.34") (k 0)) (d (n "luminance") (r ">= 0.38, < 0.40") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.8") (d #t) (k 0)))) (h "159f4sl9f0pv04y09j2jbga6xnjknc1k8lpbl12rvf7ahk7d1mbq") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.12.2 (c (n "luminance-glfw") (v "0.12.2") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.34") (k 0)) (d (n "luminance") (r ">= 0.38, < 0.40") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.8") (d #t) (k 0)))) (h "1vkw5l8fs84yn8ypx2pj4rxlykv4ly3ys2n8bjwccnf9z70w0w0y") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.13.0 (c (n "luminance-glfw") (v "0.13.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.39") (k 0)) (d (n "luminance") (r "^0.40") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "1c3b132n80l7cjx22gnrzbk4lpg8kvhxyrw68cn980dpv6gk77fh") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.13.1 (c (n "luminance-glfw") (v "0.13.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.39") (k 0)) (d (n "luminance") (r ">=0.40, <0.42") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "1an4x8npr7g63fbnpx4q8zv9ijry2wmm2bfj17598ag9bjiibw1q") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.14.0 (c (n "luminance-glfw") (v "0.14.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.39") (k 0)) (d (n "luminance") (r "^0.42") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.15") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "080pd9cnyi30kdc2ps3lqjgx3pp3wfhmdhnpj0vp6p2wy88fnqs6") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.14.1 (c (n "luminance-glfw") (v "0.14.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r ">=0.39, <0.41") (k 0)) (d (n "luminance") (r "^0.42") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.15") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "1v3mspl3k9yjkqbw5c9shnpxp87yxj93a1vhsjlj1p6g4wz3rxzn") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.14.2 (c (n "luminance-glfw") (v "0.14.2") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r ">=0.39, <0.41") (k 0)) (d (n "luminance") (r ">=0.42, <0.44") (d #t) (k 0)) (d (n "luminance-gl") (r ">=0.15, <0.17") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "1ccfk3hh060nqqqvkh7gd9zp269ym60cyfir106sgmx1jgnvbv0r") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.14.3 (c (n "luminance-glfw") (v "0.14.3") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r ">=0.39, <0.42") (k 0)) (d (n "luminance") (r ">=0.42, <0.44") (d #t) (k 0)) (d (n "luminance-gl") (r ">=0.15, <0.17") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "18hb2xwaq2mkfxfl74za1i32r98w4a9ypcvizrii05v96h9bjq4l") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.14.4 (c (n "luminance-glfw") (v "0.14.4") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r ">=0.39, <0.42") (k 0)) (d (n "luminance") (r ">=0.42, <0.44") (d #t) (k 0)) (d (n "luminance-gl") (r ">=0.15, <0.17") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "14c29ncfqlkp32y4ml1y06pmq9906igi5vifrshlcrsyg1xsa9s9") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.15.0 (c (n "luminance-glfw") (v "0.15.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r ">=0.39, <0.42") (k 0)) (d (n "luminance") (r ">=0.42, <0.44") (d #t) (k 0)) (d (n "luminance-gl") (r ">=0.15, <0.17") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.9") (d #t) (k 0)))) (h "1vqminzf78n74j2nf0sym7py6msd19v9xpzrx81i7b25mcd43knn") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.16.0 (c (n "luminance-glfw") (v "0.16.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.41") (k 0)) (d (n "luminance") (r "^0.44") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.17") (d #t) (k 0)) (d (n "luminance-windowing") (r "^0.10") (d #t) (k 0)))) (h "1qb5drfmff6la899snx639z977h3b44aqr8l5nwv59qrmhbhz35s") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys"))))))

(define-public crate-luminance-glfw-0.17.0 (c (n "luminance-glfw") (v "0.17.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.42") (k 0)) (d (n "luminance") (r "^0.45") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.18") (d #t) (k 0)))) (h "10syaijfvzxnwm8vmzjwkk65arrgnsj46020rzmzgw8y2l5b4mzc") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys")))) (r "1.56.0")))

(define-public crate-luminance-glfw-0.18.0 (c (n "luminance-glfw") (v "0.18.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.43") (k 0)) (d (n "luminance") (r "^0.46") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.19") (d #t) (k 0)))) (h "0n62g4pyxra6dss9zk4ic85r5sxn6nnnfsjmqf8d29pligd442ma") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys")))) (r "1.56.0")))

(define-public crate-luminance-glfw-0.18.1 (c (n "luminance-glfw") (v "0.18.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.43") (k 0)) (d (n "luminance") (r ">=0.46, <0.48") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.19") (d #t) (k 0)))) (h "0d8c81wjsbmgw1ph7hwivf609qn1p89wdpijh6jchprgvr4yzf87") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys")))) (r "1.56.0")))

(define-public crate-luminance-glfw-0.18.2 (c (n "luminance-glfw") (v "0.18.2") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r ">=0.43, <0.45") (k 0)) (d (n "luminance") (r ">=0.46, <0.48") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.19") (d #t) (k 0)))) (h "1hc1yjh4yfd9jhcsynnw1gbzr3mx9n56h238p8ga9gwxf3wl3g44") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys")))) (r "1.56.0")))

(define-public crate-luminance-glfw-0.18.3 (c (n "luminance-glfw") (v "0.18.3") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r ">=0.43, <0.46") (k 0)) (d (n "luminance") (r ">=0.46, <0.48") (d #t) (k 0)) (d (n "luminance-gl") (r "^0.19") (d #t) (k 0)))) (h "0apnl47x3kf920v95k4v69g4slznxhan4vkf7mbqp8x1rx4g62zi") (f (quote (("log-errors") ("glfw-sys" "glfw/glfw-sys") ("default" "glfw-sys") ("all" "glfw-sys")))) (r "1.56.0")))

