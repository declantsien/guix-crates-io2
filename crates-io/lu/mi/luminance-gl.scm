(define-module (crates-io lu mi luminance-gl) #:use-module (crates-io))

(define-public crate-luminance-gl-0.1.0 (c (n "luminance-gl") (v "0.1.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.1.0") (d #t) (k 0)))) (h "1vf9hqmmh5j6pjbqmc37bvjmmy8vn27lqpgnfhssg3lz3qhnj3cz")))

(define-public crate-luminance-gl-0.2.0 (c (n "luminance-gl") (v "0.2.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.2.0") (d #t) (k 0)))) (h "1i17w9ix9mxnzs71n1kdan8a2wmzla7rwlnclxhp0gxdxakapdr6")))

(define-public crate-luminance-gl-0.2.1 (c (n "luminance-gl") (v "0.2.1") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.3.0") (d #t) (k 0)))) (h "0fblnk5jj247plyqspm17bqlsj899xghicvcf2gzc8kidyglwlxx")))

(define-public crate-luminance-gl-0.3.0 (c (n "luminance-gl") (v "0.3.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.4.0") (d #t) (k 0)))) (h "0ms5yag2rny7lmcqwbxx3fba2x16vh1kw4wjfpcnd2ysfcp7r673")))

(define-public crate-luminance-gl-0.3.1 (c (n "luminance-gl") (v "0.3.1") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.4.0") (d #t) (k 0)))) (h "0k6yw6pwgd8lrizx0g1j01rk099vzp5zvc3vrr5p5f1iywdd3ajm")))

(define-public crate-luminance-gl-0.3.2 (c (n "luminance-gl") (v "0.3.2") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.4.0") (d #t) (k 0)))) (h "0ddx7xnq5iqfdl2ai0i5y7942zmg4w64sl8lkkfdwyy3cgahznid")))

(define-public crate-luminance-gl-0.4.0 (c (n "luminance-gl") (v "0.4.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.5.0") (d #t) (k 0)))) (h "0jipxzzd5skwl8mnq97v3kb9s056bx2wlscqb9jdbpr5nkcxq0ff") (y #t)))

(define-public crate-luminance-gl-0.4.1 (c (n "luminance-gl") (v "0.4.1") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.5.0") (d #t) (k 0)))) (h "1wpf2pzs2lhfync8r9w7ifza0q75xfp8mbimx3dvylkgbkzcscdn")))

(define-public crate-luminance-gl-0.4.2 (c (n "luminance-gl") (v "0.4.2") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.5.0") (d #t) (k 0)))) (h "1dh9sr819vl624ns4jq9amrxlm5q2s8dav90ih7m0i73qs03j1dx")))

(define-public crate-luminance-gl-0.4.3 (c (n "luminance-gl") (v "0.4.3") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.5.3") (d #t) (k 0)))) (h "0fc5yw1s09p5vymsfck5f21hv9nnhgciq3da8w0k11iylw4slyns")))

(define-public crate-luminance-gl-0.5.0 (c (n "luminance-gl") (v "0.5.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.6.0") (d #t) (k 0)))) (h "028li67nvxi9rxbsl256cyrsws0lxnxyb3j3ng1g4w5fmjyvraiy")))

(define-public crate-luminance-gl-0.5.1 (c (n "luminance-gl") (v "0.5.1") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.6.0") (d #t) (k 0)))) (h "1qizx0h4clgfbmqcdka7zs0xjk7wirg3rvjnnjnr8b9yg3x1cilx")))

(define-public crate-luminance-gl-0.5.2 (c (n "luminance-gl") (v "0.5.2") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.6.0") (d #t) (k 0)))) (h "0rd8smp3dl4m3b47i4bq4258kcnh3hhdzh0jzn82xqdfg14qgr34")))

(define-public crate-luminance-gl-0.5.3 (c (n "luminance-gl") (v "0.5.3") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.6.2") (d #t) (k 0)))) (h "1901pj5armp560c65cdg73fq92xbhhyaqk5bl66h22l46q609hjl")))

(define-public crate-luminance-gl-0.5.4 (c (n "luminance-gl") (v "0.5.4") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.6.3") (d #t) (k 0)))) (h "1nmnrxhp9fnhan2wb098dq7yxm3dcq7d48l12zhb8ci7rd8y14k0")))

(define-public crate-luminance-gl-0.5.5 (c (n "luminance-gl") (v "0.5.5") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.6.4") (d #t) (k 0)))) (h "14f502i2a26km45gpdh529ysw16yb302vcdr4rwfhd6kpgr124rd") (f (quote (("gl33") ("default" "gl33")))) (y #t)))

(define-public crate-luminance-gl-0.5.6 (c (n "luminance-gl") (v "0.5.6") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.6.4") (d #t) (k 0)))) (h "0wzy71ajwihsfscvjfwxf5lgj46ddplg3xmfn2ydpsibgp7170kh") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.6.0 (c (n "luminance-gl") (v "0.6.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.7.0") (d #t) (k 0)))) (h "1pl3mw28nmcpblfhqbdjd0pcxfbvszpkq94187s6295nnpij8bvb") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.6.1 (c (n "luminance-gl") (v "0.6.1") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.7.0") (d #t) (k 0)))) (h "0v6lggkmjd0sqkn4vjs9lz55iigfdy5hmmzjisah1q4p3v5hizmg") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.6.2 (c (n "luminance-gl") (v "0.6.2") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.8.0") (d #t) (k 0)))) (h "1vr1nc5dipfil2m9f1ajycq6gcp5995l599ldr0ggsa5zgp9kaz1") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.7.0 (c (n "luminance-gl") (v "0.7.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.9.0") (d #t) (k 0)))) (h "0sd90dxasg1bii7nl5n7ivm6mn4kjbfb8g8ggcw2cvqyy5ysjzs1") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.7.1 (c (n "luminance-gl") (v "0.7.1") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.10.0") (d #t) (k 0)))) (h "1nmdhb32n9iyk0kmcyc6nhsckb08fxb7wb9hq0199p7l0f1bb2pj") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.9.0 (c (n "luminance-gl") (v "0.9.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.12.0") (d #t) (k 0)))) (h "0fwp0zqn52cz9a0w3lh4xl1ihvwdprhz809137xaaznzlr6hv4x9") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.9.1 (c (n "luminance-gl") (v "0.9.1") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.12.0") (d #t) (k 0)))) (h "05alm3f9mdhvr0hvwa0d86kx28kjyww94xdql7h4v8m8pxp8x6ic") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.10.0 (c (n "luminance-gl") (v "0.10.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.13.0") (d #t) (k 0)))) (h "0yird4g2sl82ba3mjgl72cl9r3i4krai6gw7z85ay06sgq25r91f") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.11.0 (c (n "luminance-gl") (v "0.11.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.14.0") (d #t) (k 0)))) (h "0ww3iw9c55qlldam41i1zf2bn6ch996i2kh819i6szjxr43zf936") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.12.0 (c (n "luminance-gl") (v "0.12.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.15.0") (d #t) (k 0)))) (h "0cclxy1rmdav84sc9nxsx142cc04bkrb5hmmk1rl5ghac25zrclj") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.13.0 (c (n "luminance-gl") (v "0.13.0") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.16.0") (d #t) (k 0)))) (h "1b2905b6drznxlzgwpv0nn26kr51k856jb094hzdq1521algh9li") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.13.1 (c (n "luminance-gl") (v "0.13.1") (d (list (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "luminance") (r "^0.16") (d #t) (k 0)))) (h "0vfng1bjaq4x4nry3cjmzvvlk0wahwb7kv0w2m8dq7a54lxxrvvh") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.14.0 (c (n "luminance-gl") (v "0.14.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.40") (d #t) (k 0)))) (h "1iv04wzz1hdb2x93pympzv36zn6mbqhscrr1vna865qr3l34wc80") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.14.1 (c (n "luminance-gl") (v "0.14.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r ">=0.40, <0.42") (d #t) (k 0)))) (h "09q40yw9npcis26fn8yzz6s4v2iyfkk9nfccgxym7bx3f2a47j24") (f (quote (("gl33") ("default" "gl33"))))))

(define-public crate-luminance-gl-0.15.0 (c (n "luminance-gl") (v "0.15.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.42") (d #t) (k 0)))) (h "1l2417qajqx7hdyhixgciy5j776vfp23jz3b1a6l3kj69zf6sjyb") (f (quote (("gl33") ("default" "gl33") ("GL_ARB_gpu_shader_fp64"))))))

(define-public crate-luminance-gl-0.15.1 (c (n "luminance-gl") (v "0.15.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.42") (d #t) (k 0)))) (h "1k4jxa52di37k58sk59rv0gl538vflr02j91w8g244hrswwjrq4n") (f (quote (("gl33") ("default" "gl33") ("GL_ARB_gpu_shader_fp64"))))))

(define-public crate-luminance-gl-0.16.0 (c (n "luminance-gl") (v "0.16.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.43") (d #t) (k 0)))) (h "118kyfmkf2dcnhw9kq7whni835bbwqd5b0gsyv54q9v6alj3g2bc") (f (quote (("gl33") ("default" "gl33") ("GL_ARB_gpu_shader_fp64"))))))

(define-public crate-luminance-gl-0.16.1 (c (n "luminance-gl") (v "0.16.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.43") (d #t) (k 0)))) (h "1sz16g9x55h4bizbx2na98w4jff31hbhccbd65i08s3wax625ily") (f (quote (("gl33") ("default" "gl33") ("GL_ARB_gpu_shader_fp64"))))))

(define-public crate-luminance-gl-0.17.0 (c (n "luminance-gl") (v "0.17.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.44") (d #t) (k 0)))) (h "1f78mlfx6sxzv4dgml9v7102yl4bp15bq1yqqrhhvgdgliw8axyg") (f (quote (("gl33") ("default" "gl33") ("GL_ARB_gpu_shader_fp64"))))))

(define-public crate-luminance-gl-0.18.0 (c (n "luminance-gl") (v "0.18.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.45") (d #t) (k 0)) (d (n "luminance-std140") (r "^0.1") (d #t) (k 0)))) (h "0yrpnwfpdfqdljixsd3ijy29yxx3gd9rdy11h27n2g6lqamzljxf") (f (quote (("gl33") ("default" "gl33") ("GL_ARB_gpu_shader_fp64")))) (r "1.56.0")))

(define-public crate-luminance-gl-0.19.0 (c (n "luminance-gl") (v "0.19.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r "^0.46") (d #t) (k 0)) (d (n "luminance-std140") (r "^0.2") (d #t) (k 0)))) (h "0h5lww2zbgk1z1h5rjzc5rk36k0lpsr1kj2hkrhyw2i635gq74vg") (f (quote (("gl33") ("default" "gl33") ("GL_ARB_gpu_shader_fp64")))) (r "1.56.0")))

(define-public crate-luminance-gl-0.19.1 (c (n "luminance-gl") (v "0.19.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "luminance") (r ">=0.46, <0.48") (d #t) (k 0)) (d (n "luminance-std140") (r "^0.2") (d #t) (k 0)))) (h "0mn8mwsn7zipbbczmjav7vh8k2h29kmprx33j0vlf0zqdgy34bz2") (f (quote (("gl33") ("default" "gl33") ("GL_ARB_gpu_shader_fp64")))) (r "1.56.0")))

