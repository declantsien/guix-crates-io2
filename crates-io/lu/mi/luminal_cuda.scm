(define-module (crates-io lu mi luminal_cuda) #:use-module (crates-io))

(define-public crate-luminal_cuda-0.2.0 (c (n "luminal_cuda") (v "0.2.0") (d (list (d (n "dfdx") (r "^0.13") (f (quote ("f16"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "luminal") (r "^0.2.0") (d #t) (k 0)) (d (n "luminal_cudarc") (r "^0.10.0") (f (quote ("cublas" "f16"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1s428fy53rxqpjavhwc3gdl6jqlyqja6q7m8r7735sidfl40zsak")))

