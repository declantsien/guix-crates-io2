(define-module (crates-io lu mi luminal-handler) #:use-module (crates-io))

(define-public crate-luminal-handler-0.0.1 (c (n "luminal-handler") (v "0.0.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "1a2bmdbxy0ayd2qmxvqak9k5a3i0ca6w9vf8k88ckfqpls32jnj1")))

(define-public crate-luminal-handler-0.0.2 (c (n "luminal-handler") (v "0.0.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "luminal-router") (r "^0.0") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "08y7s4yv1gfnvabw5xj1kwaq9cw6fpxv1db1nswn2n4lgiacwj1a")))

(define-public crate-luminal-handler-0.0.3 (c (n "luminal-handler") (v "0.0.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "1nnmz74vb1p647h8ay6hjpnn9cvgvd5w0jym0c38ifh880nm78la")))

(define-public crate-luminal-handler-0.0.4 (c (n "luminal-handler") (v "0.0.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "0g51h2r3rjbd1qff7x50ljpy27fnx8596zfz6kwn54w285vg7vqy")))

(define-public crate-luminal-handler-0.0.5 (c (n "luminal-handler") (v "0.0.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (f (quote ("compat"))) (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0nra4r8q909nj5m57x3dkjjz6w0my0vjs7hmcl70sglcl9vxsl1l")))

