(define-module (crates-io lu mi luminance-windowing) #:use-module (crates-io))

(define-public crate-luminance-windowing-0.1.0 (c (n "luminance-windowing") (v "0.1.0") (h "1ybyddf5fzvk3l2r8xddia53nzchjk0nj29wvrv9ymgw967vg81c")))

(define-public crate-luminance-windowing-0.1.1 (c (n "luminance-windowing") (v "0.1.1") (h "17maf94rpf99wm9692s9a9jl1zmrm2g4di9kjy8pj5kbms0qzwbz")))

(define-public crate-luminance-windowing-0.2.0 (c (n "luminance-windowing") (v "0.2.0") (d (list (d (n "luminance") (r "^0.26") (d #t) (k 0)))) (h "0ks9zfairl6g6y52gxn9gaaw9kfb590vs3bxgakgmw34ds5xnhjn")))

(define-public crate-luminance-windowing-0.2.1 (c (n "luminance-windowing") (v "0.2.1") (d (list (d (n "luminance") (r "^0.27") (d #t) (k 0)))) (h "1is5psvcvxclca8wklfk5psah3fghp2vcmgfp7xg2n7lms58pppj")))

(define-public crate-luminance-windowing-0.2.2 (c (n "luminance-windowing") (v "0.2.2") (d (list (d (n "luminance") (r "^0.28") (d #t) (k 0)))) (h "03ffbip92f8n3dfmnqyn8k4b5ypm6h8q2zv9xa2i8sqgvvpw84c2")))

(define-public crate-luminance-windowing-0.2.3 (c (n "luminance-windowing") (v "0.2.3") (d (list (d (n "luminance") (r "^0.29") (d #t) (k 0)))) (h "1dl3613nh2v10aj8gxh5k2g36yad92f7a1ni40xr71ixbz1nj6pr")))

(define-public crate-luminance-windowing-0.2.4 (c (n "luminance-windowing") (v "0.2.4") (d (list (d (n "luminance") (r "^0.30") (d #t) (k 0)))) (h "17j6cwqq2dc6qw8dmrhbi8j6bac5lg9qklj62fl06pnvjlaz3lhg")))

(define-public crate-luminance-windowing-0.3.0 (c (n "luminance-windowing") (v "0.3.0") (d (list (d (n "luminance") (r "^0.31") (d #t) (k 0)))) (h "19j0p783h2179ca9gyd05phkhm2s3cd0dx5y3xaj9y1mrq35zy6n")))

(define-public crate-luminance-windowing-0.3.1 (c (n "luminance-windowing") (v "0.3.1") (d (list (d (n "luminance") (r "^0.32") (d #t) (k 0)))) (h "0vfdw52rjzqs5vlanmpk9j4sjdlnkpkp52kcp6r7yfswlc7pq87p")))

(define-public crate-luminance-windowing-0.4.0 (c (n "luminance-windowing") (v "0.4.0") (d (list (d (n "luminance") (r "^0.33") (d #t) (k 0)))) (h "0cnw4z5mffpp3dd02061b72i0afr084i5k0z6n4gymqv3a1rxm3n") (y #t)))

(define-public crate-luminance-windowing-0.5.0 (c (n "luminance-windowing") (v "0.5.0") (d (list (d (n "luminance") (r "^0.34") (d #t) (k 0)))) (h "1jb8w4568kg4irrn7pl1wpyd7y1f9z3kz5jm8cs38nw59rb3kjs8") (y #t)))

(define-public crate-luminance-windowing-0.5.1 (c (n "luminance-windowing") (v "0.5.1") (d (list (d (n "luminance") (r ">= 0.34, < 0.36") (d #t) (k 0)))) (h "1mgnfnpf6cpk2r1v2ihj3pl852jzvcd9adb5s8sw3ifflwqa2d4i") (y #t)))

(define-public crate-luminance-windowing-0.6.0 (c (n "luminance-windowing") (v "0.6.0") (d (list (d (n "luminance") (r "^0.35") (d #t) (k 0)))) (h "0kzi2dzz4f6alqm8w922jhhndqzrs936i22kbwqvfsnhd71lrfdl")))

(define-public crate-luminance-windowing-0.7.0 (c (n "luminance-windowing") (v "0.7.0") (d (list (d (n "luminance") (r "^0.36") (d #t) (k 0)))) (h "1lqmlf7p5gmzcmlywlakdq4nacrm8p26kidl1wj30axv4hwwnjwj")))

(define-public crate-luminance-windowing-0.8.0 (c (n "luminance-windowing") (v "0.8.0") (d (list (d (n "luminance") (r "^0.37") (d #t) (k 0)))) (h "1jmldldms9pqp417a83ky06b3f655sgw7v6j5hh1x6k4nwacryap")))

(define-public crate-luminance-windowing-0.8.1 (c (n "luminance-windowing") (v "0.8.1") (d (list (d (n "luminance") (r ">= 0.37, < 0.39") (d #t) (k 0)))) (h "06p0wi59zk3dami5gkv7chxylda72gikvbfsmzqaln94cxzxv5n7")))

(define-public crate-luminance-windowing-0.8.2 (c (n "luminance-windowing") (v "0.8.2") (d (list (d (n "luminance") (r ">= 0.37, < 0.40") (d #t) (k 0)))) (h "0v20l8gcb4r2596j8bqqixqsr2bi43yxdl5pzvlhxqcj259vn5dd")))

(define-public crate-luminance-windowing-0.9.0 (c (n "luminance-windowing") (v "0.9.0") (d (list (d (n "luminance") (r "^0.40") (d #t) (k 0)))) (h "164sy898qbqb4sxpjhxwgh7wlzy2iafmx7ipap7fpgqf59kvp63r")))

(define-public crate-luminance-windowing-0.9.1 (c (n "luminance-windowing") (v "0.9.1") (d (list (d (n "luminance") (r ">=0.40, <0.42") (d #t) (k 0)))) (h "0qlhmfkm53f1q5cf693zwxnchfpg80n1qmnqx6habws3qanda2dy")))

(define-public crate-luminance-windowing-0.9.2 (c (n "luminance-windowing") (v "0.9.2") (d (list (d (n "luminance") (r ">=0.40, <0.43") (d #t) (k 0)))) (h "1lv65vgv6dvwkgwry15cqdwglgg7ydkd5qjsgy7wxk13ghs5d2dg")))

(define-public crate-luminance-windowing-0.9.3 (c (n "luminance-windowing") (v "0.9.3") (d (list (d (n "luminance") (r ">=0.40, <0.44") (d #t) (k 0)))) (h "014pjawvih0d4y2azql0j5gi0z25j9j50vdal4sryp5wj5wn8nm2")))

(define-public crate-luminance-windowing-0.10.0 (c (n "luminance-windowing") (v "0.10.0") (d (list (d (n "luminance") (r "^0.44") (d #t) (k 0)))) (h "125lbh58ipmn7l3k4v4glf1s2dn97fnki3r0l926859ic574ww0i")))

(define-public crate-luminance-windowing-0.10.1 (c (n "luminance-windowing") (v "0.10.1") (d (list (d (n "luminance") (r "^0.44") (d #t) (k 0)))) (h "04gsqq0ya617xq45cawnb2mq7a065wj3hlpraxrfxi5lgr03gamd") (r "1.56.0")))

