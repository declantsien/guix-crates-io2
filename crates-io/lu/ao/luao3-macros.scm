(define-module (crates-io lu ao luao3-macros) #:use-module (crates-io))

(define-public crate-luao3-macros-0.1.0 (c (n "luao3-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-kwargs") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "069nlc9h7hglayxpavp9vwlqpssz70ycs47nlpyfnp9vr566xl9l")))

