(define-module (crates-io lu ao luao3) #:use-module (crates-io))

(define-public crate-luao3-0.1.0 (c (n "luao3") (v "0.1.0") (d (list (d (n "luao3-macros") (r "^0.1") (d #t) (k 0)) (d (n "mlua") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)))) (h "0nx5kf102qxq7a9ps9vir55x5p0h1lwxd1pq2xgqayc4mwxxn3di") (f (quote (("module" "mlua/module") ("luajit" "mlua/luajit") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53") ("lua52" "mlua/lua52") ("lua51" "mlua/lua51") ("default" "lua54")))) (y #t) (r "1.57")))

(define-public crate-luao3-0.1.1 (c (n "luao3") (v "0.1.1") (d (list (d (n "luao3-macros") (r "^0.1") (d #t) (k 0)) (d (n "mlua") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)))) (h "067j94b639ijhb1gl1jldbsvqjr1mxgmb22i1yl07g59fnd7xpyn") (f (quote (("module" "mlua/module") ("luajit" "mlua/luajit") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53") ("lua52" "mlua/lua52") ("lua51" "mlua/lua51") ("default" "lua54")))) (y #t) (r "1.57")))

(define-public crate-luao3-0.1.2 (c (n "luao3") (v "0.1.2") (d (list (d (n "luao3-macros") (r "^0.1") (d #t) (k 0)) (d (n "mlua") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)))) (h "1hbk6ksbpyxrl2mkvyv22zd18qbag8yzf5k4icb73x8wzdlkrdgr") (f (quote (("vendored" "mlua/vendored") ("module" "mlua/module") ("luajit" "mlua/luajit") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53") ("lua52" "mlua/lua52") ("lua51" "mlua/lua51") ("default" "lua54")))) (r "1.57")))

