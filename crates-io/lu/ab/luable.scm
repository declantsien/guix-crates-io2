(define-module (crates-io lu ab luable) #:use-module (crates-io))

(define-public crate-luable-0.1.0 (c (n "luable") (v "0.1.0") (h "113jpsz3w9q017zb1156pxxv61kixbbvs807s6jdslx0c6ig7j9s")))

(define-public crate-luable-0.1.1 (c (n "luable") (v "0.1.1") (h "17cjcxxljc2653j92qjmlrvb4n0cb4wylszaiammv4z6k2pdmcbl")))

(define-public crate-luable-0.1.2 (c (n "luable") (v "0.1.2") (h "01rf22lc70isczir0wz212pvarcvxxzq6crwgyzx4bmh38smxvrz")))

(define-public crate-luable-0.1.6 (c (n "luable") (v "0.1.6") (h "0q06af2ja5ph8p13bkdbjn1wq7k7ml9az8bvk933aiv4sx2dghmx")))

(define-public crate-luable-0.1.7 (c (n "luable") (v "0.1.7") (h "183yqs1mv6ydq40xi74fh56bbbx10k7ngzy34wd5i5c2cygwas2d")))

(define-public crate-luable-0.1.8 (c (n "luable") (v "0.1.8") (h "1md5wwgm702r19vxjqj6ma5a0sk2kx17mbwvyjwshh9c5b7bxr3f")))

