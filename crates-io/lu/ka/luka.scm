(define-module (crates-io lu ka luka) #:use-module (crates-io))

(define-public crate-luka-0.1.0 (c (n "luka") (v "0.1.0") (h "1a4my9qihn46hhs0yhy7w4aasqgz1x5c8x2hi5i751sa14ibkw6p")))

(define-public crate-luka-0.2.0 (c (n "luka") (v "0.2.0") (h "1bswkh0a0qyy7r33h8s3vgfkzlq29si842jvapsb3s1mjjkdr1r4")))

(define-public crate-luka-0.3.0 (c (n "luka") (v "0.3.0") (h "0qpifi8rmmv63zbwr6bbfqvrlnlwhwmbiwkdmi96q26xkl7475bb")))

(define-public crate-luka-0.4.0 (c (n "luka") (v "0.4.0") (h "173b666gdm81832xgdzrg06spn2rrmd6qb3pijwmaflx0n1rzxc3")))

