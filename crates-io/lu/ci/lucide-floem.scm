(define-module (crates-io lu ci lucide-floem) #:use-module (crates-io))

(define-public crate-lucide-floem-0.1.0 (c (n "lucide-floem") (v "0.1.0") (d (list (d (n "const_format") (r "^0.2.32") (f (quote ("fmt"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "12f31p37ai2p7v1h7a1vhwwamp42vr0gn6zi3ml7lcxd6wm7ap3v")))

