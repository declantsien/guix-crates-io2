(define-module (crates-io lu ci lucia-lang) #:use-module (crates-io))

(define-public crate-lucia-lang-0.1.0 (c (n "lucia-lang") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 0)))) (h "0gdpmghkcjqib2y98d2s36bz9la7lnxr2frfyilz66klnhxswc1y")))

(define-public crate-lucia-lang-0.2.0 (c (n "lucia-lang") (v "0.2.0") (d (list (d (n "gc-arena") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0gxhfh9fyi0j7g7kziq7lvwrm0jnrcv1ncdr82b67r8cpccm46d0")))

