(define-module (crates-io lu ci lucien) #:use-module (crates-io))

(define-public crate-lucien-0.0.1 (c (n "lucien") (v "0.0.1") (h "0phacypg23f36ww15z19yhyn0n069sln6a88i8cs52ay41yqfljq") (y #t)))

(define-public crate-lucien-0.0.2 (c (n "lucien") (v "0.0.2") (d (list (d (n "nvml-wrapper") (r "^0.10.0") (d #t) (k 0)) (d (n "nvml-wrapper-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)))) (h "1dkfglgjgvjx0p4f98r60kawr6vfapri4zh1il13r948039ll86d")))

(define-public crate-lucien-0.0.3 (c (n "lucien") (v "0.0.3") (d (list (d (n "nvml-wrapper") (r "^0.10.0") (d #t) (k 0)) (d (n "nvml-wrapper-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.0") (d #t) (k 0)))) (h "1ic533x2014jlwgcrp0li9qxc4ccmpl936bnak896lnn07lacj0s")))

