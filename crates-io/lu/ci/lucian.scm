(define-module (crates-io lu ci lucian) #:use-module (crates-io))

(define-public crate-lucian-0.1.0 (c (n "lucian") (v "0.1.0") (h "1q0lrqpz3ncqmfxirx0kvn8i3xhzjqq69wxc5n1xj9bpncp711xh")))

(define-public crate-lucian-0.1.1 (c (n "lucian") (v "0.1.1") (h "064m8wc7jhc6m651zp200rhcvv21xkys82sg416wp3rpjxl1x0i0")))

(define-public crate-lucian-0.1.2 (c (n "lucian") (v "0.1.2") (h "0vp5r8rv27xn77kdpviv6gsw6nbbiy7y1i117gbfsd8x9csgljns")))

(define-public crate-lucian-0.1.3 (c (n "lucian") (v "0.1.3") (h "1m7ys9xv2drm0nz99f7j2icj2fy9w0syc0bb1fdjrjmfnsixgrjr")))

(define-public crate-lucian-0.1.4 (c (n "lucian") (v "0.1.4") (h "1wyj78zl472rca7vqwvjrkxmi66v2cp5ddgahcrzgynr9qiq9dg2")))

(define-public crate-lucian-0.1.5 (c (n "lucian") (v "0.1.5") (h "0nqik8mvjnars39rl9y26b3dnxk1cbvf862z4s5qp512gh575vgz")))

(define-public crate-lucian-0.1.6 (c (n "lucian") (v "0.1.6") (h "1sdg9saiyj5k61kcmq56y7w7yrk4lspca7vf09z5l7l8gan5gi5b")))

(define-public crate-lucian-0.1.7 (c (n "lucian") (v "0.1.7") (h "0dacnwx6gs3fk16k97j7pf94in90bcfp0nq43rvr7bdnv0fipdkq")))

(define-public crate-lucian-0.1.8 (c (n "lucian") (v "0.1.8") (h "1ssml5dwirhigcxd43bh945ki498xvf9wcnarilqfsp887dfvw54")))

(define-public crate-lucian-0.1.9 (c (n "lucian") (v "0.1.9") (d (list (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "0i48k4w847qds8iqrah0k6fy2whkc9d8rm1cmbk8njqchvh6mx0v")))

(define-public crate-lucian-0.2.0 (c (n "lucian") (v "0.2.0") (d (list (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "1g85b4nnb444dmi3g2f5difwfx9562idr3lnfzl9rlwik83l1mw8")))

(define-public crate-lucian-0.2.1 (c (n "lucian") (v "0.2.1") (d (list (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "0f6li3hgx56y46n65pywnxzb2md9dnskd9l7xnvbifpwb3ma0h0z")))

(define-public crate-lucian-0.2.2 (c (n "lucian") (v "0.2.2") (d (list (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "0li2nhc50dvvvph8pikhfnsjlhfnq98isizfvsd7l0i3lsq25vz0")))

(define-public crate-lucian-0.2.3 (c (n "lucian") (v "0.2.3") (d (list (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "1qaxaw3qcvnygbshwwkczm1z8zapfvp6r8pgffj3x5x21hdm9ihg")))

(define-public crate-lucian-0.2.4 (c (n "lucian") (v "0.2.4") (d (list (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "00jm5wf1lfcl1mkn0f2za0zkga2j0x0djjzcddnl7vmnacy2f97a")))

(define-public crate-lucian-0.2.5 (c (n "lucian") (v "0.2.5") (d (list (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "0yb7d38ar4kwgk1zr36w0cjkq6mng3hqcqglna6wiil7nib8bzd0")))

(define-public crate-lucian-0.2.6 (c (n "lucian") (v "0.2.6") (d (list (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "0f1p41vsrx0icclwnh91c75h1pd4xajw4klz50804jxi8n3d94im")))

(define-public crate-lucian-0.2.7 (c (n "lucian") (v "0.2.7") (d (list (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "sni_shuffle") (r "^0.1") (d #t) (k 0)))) (h "1by1jwlikifm98psn9p8l4wffgf2ai9lxg9c6hqylvaxfdkmyy1l")))

(define-public crate-lucian-0.2.8 (c (n "lucian") (v "0.2.8") (d (list (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "sni_shuffle") (r "^0.1") (d #t) (k 0)))) (h "1q6v0i45l5rncqjs55hyj1v4rccfzz02a2yyws7pbm926fijz46c")))

(define-public crate-lucian-0.2.9 (c (n "lucian") (v "0.2.9") (d (list (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "sni_shuffle") (r "^0.1") (d #t) (k 0)))) (h "0vlhcvfraz423znlm1f2vf27j6ryh374cr2mjrz1cyfjckmn73qq")))

(define-public crate-lucian-0.2.10 (c (n "lucian") (v "0.2.10") (d (list (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "sni_shuffle") (r "^0.1") (d #t) (k 0)))) (h "163zlgjifj002fvjiry5nypw8m6sfh89v8rdjkydqc9772wvbl96")))

(define-public crate-lucian-0.2.11 (c (n "lucian") (v "0.2.11") (d (list (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "sni_shuffle") (r "^0.1") (d #t) (k 0)))) (h "1d84mhd4ij8j3ragashklfkp9ai49n84cwik9d5z7x61cy76alyq")))

(define-public crate-lucian-0.2.12 (c (n "lucian") (v "0.2.12") (d (list (d (n "enum-iterator") (r "^1.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "omg-cool") (r "^0.1.1") (d #t) (k 0)) (d (n "sni_shuffle") (r "^0.1") (d #t) (k 0)))) (h "1n78jrvrhk7pzjlnpfndyns639a4xi5mrb05n5lxg7qvn5njq070")))

(define-public crate-lucian-0.2.13 (c (n "lucian") (v "0.2.13") (d (list (d (n "enum-iterator") (r "^1.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "omg-cool") (r "^0.1.1") (d #t) (k 0)) (d (n "sni_shuffle") (r "^0.1") (d #t) (k 0)))) (h "09km9nmhgpblzfbi60cbx68j5ap0qgrd4apwqnp6jnbhf6j7cahz")))

(define-public crate-lucian-0.2.14 (c (n "lucian") (v "0.2.14") (d (list (d (n "enum-iterator") (r "^1.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "omg-cool") (r "^0.1.1") (d #t) (k 0)) (d (n "sni_shuffle") (r "^0.1") (d #t) (k 0)))) (h "0l5krdkjbdmmvm58ia3bgf9m9jzfy0z2gywvrk1fwha2x4ghw9s6")))

(define-public crate-lucian-0.2.15 (c (n "lucian") (v "0.2.15") (d (list (d (n "enum-iterator") (r "^1.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "omg-cool") (r "^0.1.1") (d #t) (k 0)) (d (n "sni_shuffle") (r "^0.1") (d #t) (k 0)))) (h "1rz93j4834mgshkifqd9xzwhplp8xbyp8gkih4dmf6325pv8d5dv")))

(define-public crate-lucian-0.2.16 (c (n "lucian") (v "0.2.16") (d (list (d (n "enum-iterator") (r "^1.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "omg-cool") (r "^0.1.1") (d #t) (k 0)) (d (n "sni_shuffle") (r "^0.1") (d #t) (k 0)))) (h "0bijk2ffqv4na0xnf58zpambrkyqismqv7l5rriwy5ha80mn0qbg")))

