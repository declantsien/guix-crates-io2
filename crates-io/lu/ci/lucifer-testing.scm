(define-module (crates-io lu ci lucifer-testing) #:use-module (crates-io))

(define-public crate-lucifer-testing-0.1.0 (c (n "lucifer-testing") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0alc8a3cx3v6ay0j42dcxs7gyqxnq9hv81vw2ynzq6k2p5l7mmyi")))

(define-public crate-lucifer-testing-0.1.1 (c (n "lucifer-testing") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "16hhh5z1jwdpww0ikxh9b7iwpjwfpml11lin8s6j0148yjmg1yrm")))

(define-public crate-lucifer-testing-0.1.2 (c (n "lucifer-testing") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0gcgxva432qzd11mf73wyldg93b25n3rg0lsgrqz4m7xcb75c6c7")))

(define-public crate-lucifer-testing-0.0.0 (c (n "lucifer-testing") (v "0.0.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0sq4xz12nb5lp84l2lr1mhj30w7icxgz8bn6qhxxgcpnwpgkx9vb") (y #t)))

(define-public crate-lucifer-testing-0.1.13 (c (n "lucifer-testing") (v "0.1.13") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1za6k8w50zl6qbdyff5gqvxj1bdcdh3vrzw9hs6dy6nmdfvgqcm3")))

(define-public crate-lucifer-testing-0.1.14 (c (n "lucifer-testing") (v "0.1.14") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1h4xw0d90sy1n7n858gibl0rin3b34xdw4vg72w0bxlzjqlvaidw")))

(define-public crate-lucifer-testing-0.1.15 (c (n "lucifer-testing") (v "0.1.15") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0jfycb4v7as7x8zaxs5wly5ixnz3cpbk8zx2ffnlk6mdf3sm9ksj")))

(define-public crate-lucifer-testing-0.2.0 (c (n "lucifer-testing") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "02827h1lwaxk6v55rngm3f6vqsnbi9hq6sgjcxbw5l9kqlv8k8c7")))

(define-public crate-lucifer-testing-0.2.1 (c (n "lucifer-testing") (v "0.2.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0gfpclxrmyzxagsj7wrkyfpvb74fan1k9bfhn3wz5zsavgkgb7kd")))

(define-public crate-lucifer-testing-0.3.0 (c (n "lucifer-testing") (v "0.3.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "01hih1wk0kh69a6a5l4faqk8bv91ljd8ppjk6j315lrmb23i92sf")))

(define-public crate-lucifer-testing-1.0.0 (c (n "lucifer-testing") (v "1.0.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0plsp6g0092nxmha41jcsr2shydhrkyr1yqy0pqk23m206qj09ha")))

