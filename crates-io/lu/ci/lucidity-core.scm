(define-module (crates-io lu ci lucidity-core) #:use-module (crates-io))

(define-public crate-lucidity-core-0.1.0 (c (n "lucidity-core") (v "0.1.0") (d (list (d (n "lunatic") (r "^0.14.1") (d #t) (k 0) (p "lunatic-twitchax-patch")) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0zw26xxz485dkr838j241kc03ym9ynspc4f1xrzsyplxzxncq00n")))

