(define-module (crates-io lu ci lucidity-macros) #:use-module (crates-io))

(define-public crate-lucidity-macros-0.1.0 (c (n "lucidity-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "lucidity-core") (r "^0.1.0") (d #t) (k 0)) (d (n "lunatic") (r "^0.14.1") (d #t) (k 0) (p "lunatic-twitchax-patch")) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "09j0224x61md2j1nrask36da7dvr42bwvx549y918rq9fi31kp4g")))

