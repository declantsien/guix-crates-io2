(define-module (crates-io lu ci lucidity) #:use-module (crates-io))

(define-public crate-lucidity-0.1.0 (c (n "lucidity") (v "0.1.0") (d (list (d (n "lucidity-core") (r "^0.1.0") (d #t) (k 0)) (d (n "lucidity-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "nightfly") (r "^0.1.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)))) (h "06xf49pc7h0izndk8qwf0mc02ryrpa2mqc885hy47pylgnnbx311") (f (quote (("fly") ("default"))))))

