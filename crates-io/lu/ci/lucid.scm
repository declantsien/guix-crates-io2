(define-module (crates-io lu ci lucid) #:use-module (crates-io))

(define-public crate-lucid-0.1.0 (c (n "lucid") (v "0.1.0") (d (list (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)))) (h "17v790nci3ms3dsgx6ryh0vgz3mdf23lx4daskhqvv9simx28gzx")))

(define-public crate-lucid-0.2.0 (c (n "lucid") (v "0.2.0") (d (list (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)))) (h "07zb6h6nv1j2sikdl005jmkcdmfsvcjpznbpzfzvwnys4vnj2k9h")))

(define-public crate-lucid-0.3.0 (c (n "lucid") (v "0.3.0") (d (list (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)))) (h "00ps5g2llh833736jshiwd9lgw7hwl8rnz4hdpm8xf4s1bncr13x")))

