(define-module (crates-io lu ci lucidmq) #:use-module (crates-io))

(define-public crate-lucidmq-0.1.0 (c (n "lucidmq") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nolan") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "13af0amwf4vgl9780fn3fybp74advbm2731rfjx70n79mbn6y65v")))

(define-public crate-lucidmq-0.1.1 (c (n "lucidmq") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nolan") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1hk6ccnvycwyjmjqm0nlrrzzrqky6mixi8dmkgpk74q0fvxanbia")))

