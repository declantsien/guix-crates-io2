(define-module (crates-io lu t_ lut_parser) #:use-module (crates-io))

(define-public crate-lut_parser-0.1.0 (c (n "lut_parser") (v "0.1.0") (h "0frbr99hpfdbp60qkc4wzwl5h1f6zh3dp14amdcazz5jhcp71hdj")))

(define-public crate-lut_parser-1.0.0 (c (n "lut_parser") (v "1.0.0") (h "003qjmlsfaj9faf97ji14fz75q4rxh1jr6ax33gscysk2n8h2gif")))

(define-public crate-lut_parser-1.0.1 (c (n "lut_parser") (v "1.0.1") (h "1svlb18x1pg9bx15bn033af633sbqp49s32xp4x02js5ix95ah0d")))

(define-public crate-lut_parser-1.0.2 (c (n "lut_parser") (v "1.0.2") (h "1shzvvr18zsnk6pz26n5c0682j4rns1h4sf5x5i327wcifnnbcdi")))

(define-public crate-lut_parser-1.1.0 (c (n "lut_parser") (v "1.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1wq4bk23ivdkwyzasa6rb3253ap9si787j21gwwk7jfag2jrvmf4")))

(define-public crate-lut_parser-2.0.0 (c (n "lut_parser") (v "2.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1aci513b8q9vgm5ajx27fl1988n5vwy2n02si0wkpd792sf4g4a1")))

(define-public crate-lut_parser-2.1.0 (c (n "lut_parser") (v "2.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1haifybdhb2acr40y51q0vm744qwakmlxs4k8rdzv1l8pf44xj8v")))

(define-public crate-lut_parser-2.1.1 (c (n "lut_parser") (v "2.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "slice-of-array") (r "^0.2.1") (d #t) (k 0)))) (h "02j4p15gl2j0z8qb88z6bgb8nziypfwsm1x4r84cnsdjh0dqmvpc")))

