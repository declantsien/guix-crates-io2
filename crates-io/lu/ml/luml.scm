(define-module (crates-io lu ml luml) #:use-module (crates-io))

(define-public crate-luml-0.1.0 (c (n "luml") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lalrpop") (r "^0.16.3") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16.3") (d #t) (k 0)) (d (n "libremexre") (r "^0.1.4") (f (quote ("log"))) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1k116zjn35ipf0rg2snfdssw6vw254wcma72ws9hdklx4gxw5qf5")))

