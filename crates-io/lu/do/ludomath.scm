(define-module (crates-io lu do ludomath) #:use-module (crates-io))

(define-public crate-ludomath-1.0.0 (c (n "ludomath") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1817zk62hjwmni7b9svl3fm0j1fnv1jr83jw64ls914nmz87k3hs")))

(define-public crate-ludomath-1.1.0 (c (n "ludomath") (v "1.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0vfkrnlwps0br10327fvg7mfaqznc84jagyfbfcpchfrlzh09njq")))

(define-public crate-ludomath-1.1.1 (c (n "ludomath") (v "1.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0snrcrl0mnb4alysj3f3a92pjryp8f7c8kq8alk5n135scxjz8z5")))

