(define-module (crates-io lu mu lumus-sql-builder) #:use-module (crates-io))

(define-public crate-lumus-sql-builder-0.1.0 (c (n "lumus-sql-builder") (v "0.1.0") (h "0i005lazdr6npwm13s5d9xm2vw7xfgizg4y0k1lpxal9x6sj9ndc")))

(define-public crate-lumus-sql-builder-0.1.1 (c (n "lumus-sql-builder") (v "0.1.1") (h "1zjbv1z9d20spf8xjpyl0cjp6ygw1j6s08wx0w80ni07i1fm8dh9")))

(define-public crate-lumus-sql-builder-0.1.2 (c (n "lumus-sql-builder") (v "0.1.2") (h "0207hvcmq03dydzp1lm28b12p3v9f6pr9l469g59i9j4ww2kd2mh")))

(define-public crate-lumus-sql-builder-0.1.3 (c (n "lumus-sql-builder") (v "0.1.3") (d (list (d (n "sqlite") (r "^0.34.0") (d #t) (k 2)))) (h "13xi6pf26fpwwrgp36lg410sqljx5g9972mk6saklkl1xxmzy2j2")))

