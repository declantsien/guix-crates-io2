(define-module (crates-io lu mu lumus-log) #:use-module (crates-io))

(define-public crate-lumus-log-0.1.0 (c (n "lumus-log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "10rrm15yx1bsvd1x8xm8ajhgh22afxpfh4z9qlsz9w76j334i15y")))

(define-public crate-lumus-log-0.1.1 (c (n "lumus-log") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0rzr928wrmlzyrdfwa9pyri9mz7fnc7m6pjm3f6id43fgwjln4i8")))

