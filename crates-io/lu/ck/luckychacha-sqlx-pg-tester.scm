(define-module (crates-io lu ck luckychacha-sqlx-pg-tester) #:use-module (crates-io))

(define-public crate-luckychacha-sqlx-pg-tester-0.1.0 (c (n "luckychacha-sqlx-pg-tester") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.6.2") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rp6smg2vycyi1prnny4n9lv18pp6abykr9axd7cl9m2jrw0hldk")))

(define-public crate-luckychacha-sqlx-pg-tester-0.1.1 (c (n "luckychacha-sqlx-pg-tester") (v "0.1.1") (d (list (d (n "sqlx") (r "^0.6.2") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0a8bih54b86nirlqwb65k8z178kg5z13k193n3hncy5xdrh10w6g")))

