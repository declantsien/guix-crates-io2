(define-module (crates-io lu ck luckperms-rs) #:use-module (crates-io))

(define-public crate-luckperms-rs-0.1.0 (c (n "luckperms-rs") (v "0.1.0") (d (list (d (n "form_urlencoded") (r "^1.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0p0imz0wq8i7v4pp1ajhlv49yd8c3fbpfl0vravnmblcy9vh2vgh")))

