(define-module (crates-io lu nk lunka) #:use-module (crates-io))

(define-public crate-lunka-0.1.0 (c (n "lunka") (v "0.1.0") (d (list (d (n "allocator-api2") (r "^0.2.16") (d #t) (k 0)))) (h "013l5vhzgqp5b3rw08arzj70zcmpjjw9py2zfap3c2sbgd54bicg") (f (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib")))) (y #t)))

(define-public crate-lunka-0.1.1 (c (n "lunka") (v "0.1.1") (d (list (d (n "allocator-api2") (r "^0.2.16") (d #t) (k 0)))) (h "1i4d9pgxbfxga8g3ybj1wlsxgx8pl255k33kjfws1pc0z00wngny") (f (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

(define-public crate-lunka-0.2.0 (c (n "lunka") (v "0.2.0") (d (list (d (n "allocator-api2") (r "^0.2.16") (f (quote ("alloc"))) (k 0)))) (h "1wh3wg1yv0iv4hzjfsv9zdm2y46hzmx8n6hqzk7b88vpj588ycli") (f (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

(define-public crate-lunka-0.3.0 (c (n "lunka") (v "0.3.0") (d (list (d (n "allocator-api2") (r "^0.2.16") (f (quote ("alloc"))) (k 0)))) (h "1hx8lwk7r0mfl8vk1yh2jq430hwxmd419ph829hwnwyrrc4qhp7g") (f (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

(define-public crate-lunka-0.4.0 (c (n "lunka") (v "0.4.0") (d (list (d (n "allocator-api2") (r "^0.2.16") (f (quote ("alloc"))) (k 0)))) (h "1lybaxls7a3ngc7psb3rcm1jh8gwb35ha5wpwlp603gyxi9hcw56") (f (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

(define-public crate-lunka-0.4.1 (c (n "lunka") (v "0.4.1") (d (list (d (n "allocator-api2") (r "^0.2.16") (f (quote ("alloc"))) (k 0)))) (h "0p2f635fk8pkzp91m7p3xb9caaswpk1a4l8cgsmk4jc066bbmfnr") (f (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

(define-public crate-lunka-0.4.2 (c (n "lunka") (v "0.4.2") (d (list (d (n "allocator-api2") (r "^0.2.16") (f (quote ("alloc"))) (k 0)))) (h "1yl7406d6qhp06f8kmj7hvs9v8zgqjydyvqlryjianr4156i5m0s") (f (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib")))) (y #t)))

(define-public crate-lunka-0.4.3 (c (n "lunka") (v "0.4.3") (d (list (d (n "allocator-api2") (r "^0.2.16") (f (quote ("alloc"))) (k 0)))) (h "0gf66iy2s054f6sa7n4h82xdwfn4cchh8gz4lwjn6qf3abkvvxk4") (f (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

(define-public crate-lunka-0.5.0 (c (n "lunka") (v "0.5.0") (d (list (d (n "allocator-api2") (r "^0.2.16") (f (quote ("alloc"))) (k 0)))) (h "05bbdifp5v520pg8jynfvbr65zj3zv9an67bq22dbfvavww5jg4q") (f (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

(define-public crate-lunka-0.5.1 (c (n "lunka") (v "0.5.1") (d (list (d (n "allocator-api2") (r "^0.2.16") (f (quote ("alloc"))) (k 0)))) (h "0y5r2hn8whzjvlmlw09jrg1yy7frzp7w7578g85vs6gqwjsh2576") (f (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

(define-public crate-lunka-0.5.2 (c (n "lunka") (v "0.5.2") (d (list (d (n "allocator-api2") (r "^0.2.16") (f (quote ("alloc"))) (k 0)))) (h "1mqd5vibkhr1891gh58id4849qj59gqvfmyihlmhz32h2p1kj9kv") (f (quote (("use-32-bits") ("stdlibs") ("link-static") ("default" "auxlib" "stdlibs") ("auxlib"))))))

