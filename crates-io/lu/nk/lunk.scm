(define-module (crates-io lu nk lunk) #:use-module (crates-io))

(define-public crate-lunk-0.1.0 (c (n "lunk") (v "0.1.0") (d (list (d (n "gensym") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0b5y1la6xwwg8va226gi3xijg42ipx7daq59ycp1sznn5d63fmmq")))

(define-public crate-lunk-0.1.1 (c (n "lunk") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "082h7737bw763bwk3k5fi40vs997v35k3ylhwcs4ym7lgcy04mrk")))

(define-public crate-lunk-0.1.2 (c (n "lunk") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1jb3ffpjwgfblj80m11h9dx598i45hsfpnpr2aqm3nfl4jmbi6dg")))

(define-public crate-lunk-0.1.3 (c (n "lunk") (v "0.1.3") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "13h7xm4nl6zg7hcklbc12p991x8pn0qzy9f5rdf36hxp5xp53lbl")))

(define-public crate-lunk-0.2.0 (c (n "lunk") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1c0f8yk985ijcym67bqga6pwkwvmhggnjm71hvnpm3han9chk826")))

