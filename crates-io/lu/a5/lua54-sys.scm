(define-module (crates-io lu a5 lua54-sys) #:use-module (crates-io))

(define-public crate-lua54-sys-0.1.0 (c (n "lua54-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "1hdsmf2shrwjhz9z1v6dxk2q6k4arkqzff3gwaqa6q72492vnrhp")))

(define-public crate-lua54-sys-0.1.1 (c (n "lua54-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "1wl21dcis7hvilkf3jxqhhfpmrwh4dcj72x8ygjmi9k0302hwr9j")))

