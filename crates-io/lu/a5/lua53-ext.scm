(define-module (crates-io lu a5 lua53-ext) #:use-module (crates-io))

(define-public crate-lua53-ext-0.1.0 (c (n "lua53-ext") (v "0.1.0") (d (list (d (n "lua") (r "^0.0.10") (d #t) (k 0)))) (h "1ifij5brhxmx1c4g63jaldh87w66na4549wsqzy8nv475vkzmn9a")))

(define-public crate-lua53-ext-0.1.1 (c (n "lua53-ext") (v "0.1.1") (d (list (d (n "lua") (r "^0.0.10") (d #t) (k 0)))) (h "0rycqm4fsapv69895593azpq0m0w17ncn074n2s8fsq7818542dz")))

