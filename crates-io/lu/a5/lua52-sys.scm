(define-module (crates-io lu a5 lua52-sys) #:use-module (crates-io))

(define-public crate-lua52-sys-0.0.1 (c (n "lua52-sys") (v "0.0.1") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0i31wsfz5qc8g1i20zz7sgw2rffrhynavr2rf0hwhnwcql7jirfn")))

(define-public crate-lua52-sys-0.0.2 (c (n "lua52-sys") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0gz0z2j2w1hbxz37fbjwg5r3ndik7jvrpaq69c5jk74np5rdrjyq")))

(define-public crate-lua52-sys-0.0.3 (c (n "lua52-sys") (v "0.0.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1rbxpv02rl7sbp23mdpaw545ql56kympr2ihc45jxvyj0vy7hmdp")))

(define-public crate-lua52-sys-0.0.4 (c (n "lua52-sys") (v "0.0.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07l7znfmxja589r3q3dcs974ifwfhf28mqkaqlbqbhsmqmkz4cmv")))

(define-public crate-lua52-sys-0.1.0 (c (n "lua52-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1gn0vskyvvh9nfniqqhjs8yf1dxnli108x3qn8dcvy1brr8sdsc2")))

(define-public crate-lua52-sys-0.1.1 (c (n "lua52-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "11qiif81mn3ydxy5gfba0mmzay713aj8zchvz4r130icqrf87hir")))

(define-public crate-lua52-sys-0.1.2 (c (n "lua52-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "151gvyw5a715j2g51cwdq8jj88znfjb8yf3xh7f5br4l7haxnlfl") (l "lua")))

