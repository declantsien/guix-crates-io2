(define-module (crates-io lu a5 lua53-sys) #:use-module (crates-io))

(define-public crate-lua53-sys-0.1.0 (c (n "lua53-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1djd8mm1kpzi7p0j40c8x9lx6xsmnrlv9pc9syc09w4k8iwck1k4")))

(define-public crate-lua53-sys-0.1.1 (c (n "lua53-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jkwvpyxqz765irzh1w7h720cccb4n6sjm1nzmwhcz52alc9ciy7")))

