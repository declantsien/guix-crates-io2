(define-module (crates-io lu a5 lua51-sys) #:use-module (crates-io))

(define-public crate-lua51-sys-0.1.0 (c (n "lua51-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.39.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1xq503j8sa4bhx1wz8s0gsdx714h6w8srpylyyv1arp9m00mfxkn") (l "lua")))

