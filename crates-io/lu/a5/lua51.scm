(define-module (crates-io lu a5 lua51) #:use-module (crates-io))

(define-public crate-lua51-0.1.0 (c (n "lua51") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "09mqfmnaf0b33hpj7cr68srfjwccwd1xpbb03vh5d93s8ysyszaf")))

(define-public crate-lua51-0.1.1 (c (n "lua51") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0f5lj73jnc7ymfhmx84g7pk0qqbmicp3jmyhd6kg9xlcgrv2z9jb")))

(define-public crate-lua51-0.1.2 (c (n "lua51") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0qxish69ppnhwi0m05dnzxmqzpvwn2hiqlsq41vqh492z9xqlfkr")))

(define-public crate-lua51-0.1.3 (c (n "lua51") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "12as9axm3m8qn6rxzaqm8iqsd459s3x60prc0s76w3ansn50h4bm")))

(define-public crate-lua51-0.1.4 (c (n "lua51") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1r3b0xbkxlmql7ba3jksvr7ji1195s3vfdfzp9qky9gs076axqxk")))

(define-public crate-lua51-0.1.5 (c (n "lua51") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "12jdddxd5shph4i6d3aqwf7qbw7876bs0yv2d042iiayadfz4597")))

(define-public crate-lua51-0.1.6 (c (n "lua51") (v "0.1.6") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1cghqxcqi27v71fjsvmsdjknsw7xwskjfsncj5h6h2bkk19bzwpj")))

(define-public crate-lua51-0.1.7 (c (n "lua51") (v "0.1.7") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1z3wzg1sdh12mbk9rnn4wl5v82glfzjgw6f22isz0hjvchsa7wix") (y #t)))

(define-public crate-lua51-0.1.8 (c (n "lua51") (v "0.1.8") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0jx508qzlcgma0ga94qq23jiaidsld2gw7xyxpd0iwna60v0wwjm") (y #t)))

(define-public crate-lua51-0.1.9 (c (n "lua51") (v "0.1.9") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1pqwjf38q8gsf54vspn2a0dxk02ykb6yizrx8z84mghay599fhdj") (y #t)))

