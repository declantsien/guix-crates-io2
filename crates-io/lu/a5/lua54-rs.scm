(define-module (crates-io lu a5 lua54-rs) #:use-module (crates-io))

(define-public crate-lua54-rs-0.1.0 (c (n "lua54-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1rhc5z2agr0dbpg62pjnqin8vz1n6gscqq34fvz3ardfs57r35f3") (y #t)))

