(define-module (crates-io lu a2 lua2json) #:use-module (crates-io))

(define-public crate-lua2json-0.1.0 (c (n "lua2json") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "14aaanp019zi03jbnphq3f7g6xm667lr67hi6303ir5kv59phcfs")))

