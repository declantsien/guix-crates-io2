(define-module (crates-io lu ap luaparser) #:use-module (crates-io))

(define-public crate-luaparser-0.1.0 (c (n "luaparser") (v "0.1.0") (d (list (d (n "lualexer") (r "^0.1.0") (d #t) (k 0)))) (h "1p3v5vbslh39xmjirysrq3rhzlfizvpcbmhxwvn826j84kvrn22h") (f (quote (("nodes") ("default" "nodes"))))))

(define-public crate-luaparser-0.1.1 (c (n "luaparser") (v "0.1.1") (d (list (d (n "lualexer") (r "^0.1.2") (d #t) (k 0)))) (h "0dm4ilgx83bi1w793822s7q9l4bamxa0nsq8b6vnhij8xksjbj6m") (f (quote (("nodes") ("default" "nodes"))))))

