(define-module (crates-io lu ap luaparse) #:use-module (crates-io))

(define-public crate-luaparse-0.1.0 (c (n "luaparse") (v "0.1.0") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "03jfp4whrszfc8kwljgifxr38kw5hhj0q0kv7sargliyicb3sj2a")))

(define-public crate-luaparse-0.1.1 (c (n "luaparse") (v "0.1.1") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1xjpr0b3fs9an2lsckp0xi1x9izsvna8q7iywc9dpragn3ls1gvn")))

(define-public crate-luaparse-0.2.0 (c (n "luaparse") (v "0.2.0") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "09ffj1xc86xz3l8fm7p8yjfwn4zqlmprrc7ia6mg7phc1z896k7v")))

