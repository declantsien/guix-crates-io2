(define-module (crates-io lu mb lumberjack) #:use-module (crates-io))

(define-public crate-lumberjack-0.1.0 (c (n "lumberjack") (v "0.1.0") (h "0kc7mw06y4x9plb3p217d7ga87gj4l2jjyxfm6f87bdsym1klv41") (y #t)))

(define-public crate-lumberjack-0.2.0 (c (n "lumberjack") (v "0.2.0") (d (list (d (n "conllx") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "1drw8p7xqly2dlwqp0rgmc3wl5bff18z7qhk2hxr4izwdahv3x7j")))

(define-public crate-lumberjack-0.3.0 (c (n "lumberjack") (v "0.3.0") (d (list (d (n "conllx") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "0wkpikd6lslrkknf5ws95xbsi82i0rw8sxpy9q4v0m4myibzqrxz")))

(define-public crate-lumberjack-0.3.1 (c (n "lumberjack") (v "0.3.1") (d (list (d (n "conllx") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "0a7fbwmfi4sxpk5acs3lzzd0kspxzhdy32mlzqm9k9vwhl8gklf2")))

