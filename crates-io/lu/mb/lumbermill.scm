(define-module (crates-io lu mb lumbermill) #:use-module (crates-io))

(define-public crate-lumbermill-0.0.1 (c (n "lumbermill") (v "0.0.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "formatting"))) (d #t) (k 0)))) (h "0lgcl8g8dmzn5lbr007hzdps7srcr46k4s8vx9zzgdp65dz5hvxs")))

(define-public crate-lumbermill-0.0.2 (c (n "lumbermill") (v "0.0.2") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "formatting"))) (d #t) (k 0)))) (h "18g14krll59a00ql947w968192sb4j8j7yqmnrz8yykrqqvbfdkw")))

(define-public crate-lumbermill-0.0.3 (c (n "lumbermill") (v "0.0.3") (d (list (d (n "ctor") (r "^0.2.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "formatting"))) (d #t) (k 0)))) (h "0qq90d49xj95077gaipv09j5z3y972np5j45j635g6bd67314wjr")))

(define-public crate-lumbermill-0.0.4 (c (n "lumbermill") (v "0.0.4") (d (list (d (n "ctor") (r "^0.2.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "formatting"))) (d #t) (k 0)))) (h "1nwm2xqi4dk86v0zyczd056c6sb90ns00j6jk99df1m52jqjhqi5")))

(define-public crate-lumbermill-0.1.0 (c (n "lumbermill") (v "0.1.0") (d (list (d (n "ctor") (r "^0.2.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "formatting"))) (d #t) (k 0)))) (h "177q1dckpm4n9ysap05slyh0s8pm213nqv4jwxxiw9527azixyjn")))

(define-public crate-lumbermill-0.1.1 (c (n "lumbermill") (v "0.1.1") (d (list (d (n "ctor") (r "^0.2.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "formatting"))) (d #t) (k 0)))) (h "0sszx2s0ryx1jmpfm4vr6dc2bgjif0ji74i98xd5x7l7bir8rp7l")))

(define-public crate-lumbermill-0.1.2 (c (n "lumbermill") (v "0.1.2") (d (list (d (n "ctor") (r "^0.2.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "formatting"))) (d #t) (k 0)))) (h "0i26bqii54yz5fl5jbc51mkk21qaivk339x13x3hnhs7cmv29a3x")))

(define-public crate-lumbermill-0.1.3 (c (n "lumbermill") (v "0.1.3") (d (list (d (n "ctor") (r "^0.2.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("std" "formatting"))) (d #t) (k 0)))) (h "0gr4q7ldrzmfp1a5p81zxk5zrkar56j3gqj0xj5w8a8pzlzbmnln")))

(define-public crate-lumbermill-0.1.4 (c (n "lumbermill") (v "0.1.4") (d (list (d (n "ctor") (r "^0.2.2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.21") (f (quote ("std" "formatting"))) (d #t) (k 0)))) (h "096bgx7qmdnr2sngqy6q8j7wkffpkn3i612qa5cabvs6snihr3fd")))

(define-public crate-lumbermill-0.2.0 (c (n "lumbermill") (v "0.2.0") (d (list (d (n "ctor") (r "^0.2.2") (d #t) (k 2)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.21") (f (quote ("std" "formatting"))) (d #t) (k 0)))) (h "0gjyv8iw27l21p2fsx70k1ksb7v294wfnjms7wgdwaqz453wj56c")))

