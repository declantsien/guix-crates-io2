(define-module (crates-io lu mb lumberjack-utils) #:use-module (crates-io))

(define-public crate-lumberjack-utils-0.2.0 (c (n "lumberjack-utils") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conllx") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lumberjack") (r "^0.2.0") (d #t) (k 0)) (d (n "stdinout") (r "^0.4") (d #t) (k 0)))) (h "1fk84nh5iqhclcsd7x3m93qq60m64yg8xcl33x5arq3jfamf63qk")))

(define-public crate-lumberjack-utils-0.3.0 (c (n "lumberjack-utils") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conllx") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lumberjack") (r "^0.3.0") (d #t) (k 0)) (d (n "stdinout") (r "^0.4") (d #t) (k 0)))) (h "0nxc7xlslbwv1j8bn1503fvrki2m0w383gda0w1ikw1vaq00mz4r")))

(define-public crate-lumberjack-utils-0.3.1 (c (n "lumberjack-utils") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conllx") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lumberjack") (r "^0.3") (d #t) (k 0)) (d (n "stdinout") (r "^0.4") (d #t) (k 0)))) (h "1alhlfzf9jwmf20zz5h3vxhi2dqpymbrli62i4474fdkwc089br7")))

