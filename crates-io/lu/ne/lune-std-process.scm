(define-module (crates-io lu ne lune-std-process) #:use-module (crates-io))

(define-public crate-lune-std-process-0.1.0 (c (n "lune-std-process") (v "0.1.0") (d (list (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "lune-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "mlua") (r "^0.9.7") (f (quote ("luau"))) (d #t) (k 0)) (d (n "mlua-luau-scheduler") (r "^0.0.2") (d #t) (k 0)) (d (n "os_str_bytes") (r "^7.0") (f (quote ("conversions"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "io-util" "process" "rt" "sync"))) (k 0)))) (h "0l9pa3gyd0fly3sgjarhdi5pllmlhqgni39nyl43v2zfqvh92vk9")))

