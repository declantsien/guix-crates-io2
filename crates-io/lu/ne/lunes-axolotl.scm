(define-module (crates-io lu ne lunes-axolotl) #:use-module (crates-io))

(define-public crate-lunes-axolotl-0.4.0 (c (n "lunes-axolotl") (v "0.4.0") (d (list (d (n "blake2") (r "^0.10.2") (d #t) (k 0)) (d (n "ed25519-axolotl") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.0") (f (quote ("keccak"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.29") (d #t) (k 0)))) (h "19q6p1jgy3x94xxb1c71wlzwcmk77yhkl51wx6d2nyvsawxy4kqd") (y #t)))

