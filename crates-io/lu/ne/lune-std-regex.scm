(define-module (crates-io lu ne lune-std-regex) #:use-module (crates-io))

(define-public crate-lune-std-regex-0.1.0 (c (n "lune-std-regex") (v "0.1.0") (d (list (d (n "lune-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "mlua") (r "^0.9.7") (f (quote ("luau"))) (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "self_cell") (r "^1.0") (d #t) (k 0)))) (h "16hqqxprrdbgxvnwm4bkygs37dx1m70b70mhjn0izxv7wiidn3vp")))

