(define-module (crates-io lu ne lunes) #:use-module (crates-io))

(define-public crate-lunes-0.4.0 (c (n "lunes") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)))) (h "0xd9brwj343whnz4d246jqr0vqc5rd2q8q5drmf5w40ma7fba247")))

(define-public crate-lunes-0.5.1 (c (n "lunes") (v "0.5.1") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)))) (h "1mlcq6xcx8h2m0xvavd7slh6naiqq2vds99wnfn1p5cgk5lkb3qp")))

(define-public crate-lunes-0.5.2 (c (n "lunes") (v "0.5.2") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)))) (h "18m3vilkagqbxj2jmfz3zwzx073x2q0w5h4frpb73dgfjflidx1l")))

(define-public crate-lunes-0.6.0 (c (n "lunes") (v "0.6.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)))) (h "19fvkkcwjz124s700mwppmr09m0hqjz9pddclk7dj4rinc3jsrsm")))

(define-public crate-lunes-0.6.1 (c (n "lunes") (v "0.6.1") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)))) (h "0yw63xdiy6g146k28hj1jd5grxgiqnfwpqzvb3zdzg27ima8qjf0")))

(define-public crate-lunes-0.7.1 (c (n "lunes") (v "0.7.1") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "lunesweb") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (d #t) (k 0)) (d (n "trauma") (r "^2") (d #t) (k 0)))) (h "03i4vzpdfmz6c469z6zsxnyi1dk9jfm6msadhx0n4hllrvffa1d1")))

