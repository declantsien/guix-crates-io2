(define-module (crates-io lu ne lune-roblox) #:use-module (crates-io))

(define-public crate-lune-roblox-0.1.0 (c (n "lune-roblox") (v "0.1.0") (d (list (d (n "glam") (r "^0.27") (d #t) (k 0)) (d (n "lune-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "mlua") (r "^0.9.7") (f (quote ("luau"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rbx_binary") (r "^0.7.3") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^2.6.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.4.0") (d #t) (k 0)) (d (n "rbx_reflection_database") (r "^0.2.9") (d #t) (k 0)) (d (n "rbx_xml") (r "^0.13.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kwdkyss1k62fmq15ggbf4gpdjvk8pkcindlxzskll7mgxi6v2sm")))

