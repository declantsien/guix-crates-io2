(define-module (crates-io lu ne lune-std-luau) #:use-module (crates-io))

(define-public crate-lune-std-luau-0.1.0 (c (n "lune-std-luau") (v "0.1.0") (d (list (d (n "lune-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "mlua") (r "^0.9.7") (f (quote ("luau"))) (d #t) (k 0)))) (h "083lh69g05fc0l2nmsd09ql46lia07sd38bxaz842yp2zbc39vs7")))

