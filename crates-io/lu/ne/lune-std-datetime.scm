(define-module (crates-io lu ne lune-std-datetime) #:use-module (crates-io))

(define-public crate-lune-std-datetime-0.1.0 (c (n "lune-std-datetime") (v "0.1.0") (d (list (d (n "chrono") (r "=0.4.34") (d #t) (k 0)) (d (n "chrono_lc") (r "^0.1") (d #t) (k 0)) (d (n "lune-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "mlua") (r "^0.9.7") (f (quote ("luau"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "003da5yszgalf9mra3640j0i55s8c4a2kjvwkk5nfzc327g7hbc1")))

(define-public crate-lune-std-datetime-0.1.1 (c (n "lune-std-datetime") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "chrono_lc") (r "^0.1.6") (d #t) (k 0)) (d (n "lune-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "mlua") (r "^0.9.7") (f (quote ("luau"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "154828vsiyvkanfqxdggl2pc85sbkblpwdyrimm6kxjnzi6dwwm6")))

