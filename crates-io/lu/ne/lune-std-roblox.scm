(define-module (crates-io lu ne lune-std-roblox) #:use-module (crates-io))

(define-public crate-lune-std-roblox-0.1.0 (c (n "lune-std-roblox") (v "0.1.0") (d (list (d (n "lune-roblox") (r "^0.1.0") (d #t) (k 0)) (d (n "lune-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "mlua") (r "^0.9.7") (f (quote ("luau"))) (d #t) (k 0)) (d (n "mlua-luau-scheduler") (r "^0.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "rbx_cookie") (r "^0.1.4") (k 0)))) (h "02qahwgw8qw286p1b9qp9s79x8prnmgakpzzl4hnnmnangv3a0s1")))

