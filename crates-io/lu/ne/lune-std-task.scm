(define-module (crates-io lu ne lune-std-task) #:use-module (crates-io))

(define-public crate-lune-std-task-0.1.0 (c (n "lune-std-task") (v "0.1.0") (d (list (d (n "lune-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "mlua") (r "^0.9.7") (f (quote ("luau"))) (d #t) (k 0)) (d (n "mlua-luau-scheduler") (r "^0.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (k 0)))) (h "104h7h44w5x6s4shcld08kky0kbf18dbwk2qpl616kg5zn9s33i6")))

