(define-module (crates-io lu ne lune-std-fs) #:use-module (crates-io))

(define-public crate-lune-std-fs-0.1.0 (c (n "lune-std-fs") (v "0.1.0") (d (list (d (n "bstr") (r "^1.9") (d #t) (k 0)) (d (n "lune-std-datetime") (r "^0.1.0") (d #t) (k 0)) (d (n "lune-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "mlua") (r "^0.9.7") (f (quote ("luau"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (k 0)))) (h "05vh6jdr7h809r3mdvgajcnpqf4q3qs7qvwadcwkk3abdh981z2a")))

