(define-module (crates-io lu ne lunesweb) #:use-module (crates-io))

(define-public crate-lunesweb-0.2.0 (c (n "lunesweb") (v "0.2.0") (d (list (d (n "blake2") (r "^0.10.2") (d #t) (k 0)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "ed25519-axolotl") (r "^1.7.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.0") (f (quote ("keccak"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)))) (h "1i6vpflqdcz5zixa20i821ymrljk3vl7gm1cab3c2yjrhqz2rn5g")))

(define-public crate-lunesweb-0.3.0 (c (n "lunesweb") (v "0.3.0") (d (list (d (n "lunesrs") (r "^1.9.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)))) (h "0if7d3kw4j1xjsbljv2w6xnchfszc9vq30fnbrgdvl4y63xxpyyk")))

