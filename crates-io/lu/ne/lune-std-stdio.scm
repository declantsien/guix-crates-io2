(define-module (crates-io lu ne lune-std-stdio) #:use-module (crates-io))

(define-public crate-lune-std-stdio-0.1.0 (c (n "lune-std-stdio") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.11") (d #t) (k 0)) (d (n "lune-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "mlua") (r "^0.9.7") (f (quote ("luau"))) (d #t) (k 0)) (d (n "mlua-luau-scheduler") (r "^0.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "io-util"))) (k 0)))) (h "0vphqcjqvl4a9s3lpm7ws6ksi7v7b1mxs51m006vn953wavmybs4")))

