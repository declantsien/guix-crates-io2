(define-module (crates-io lu ne lune-utils) #:use-module (crates-io))

(define-public crate-lune-utils-0.1.0 (c (n "lune-utils") (v "0.1.0") (d (list (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "dunce") (r "^1.0") (d #t) (k 0)) (d (n "mlua") (r "^0.9.7") (f (quote ("luau" "async"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "path-clean") (r "^1.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (k 0)))) (h "031caiz2rhn4g0sppld4ig01cf3nv7cdlmbf4gk5zyi83c3k31h3")))

