(define-module (crates-io lu ne lunes-cli) #:use-module (crates-io))

(define-public crate-lunes-cli-0.3.0 (c (n "lunes-cli") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)))) (h "1dfdiwxmya61213gb4wfwpkvava4vrj8f8vxldyc6qf815l70pyd")))

