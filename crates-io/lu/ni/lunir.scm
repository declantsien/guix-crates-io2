(define-module (crates-io lu ni lunir) #:use-module (crates-io))

(define-public crate-lunir-0.1.0 (c (n "lunir") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "10n5gnq02v5n0yqy2sh0l32glpprdhk36ldbp3kmv97r2wdgjf4g") (r "1.58.1")))

(define-public crate-lunir-0.2.0 (c (n "lunir") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0xnv0ybi97sxm1jj6wl7260fkq2d42h67lyh6hhfbac2gfav5ydm") (r "1.62.1")))

