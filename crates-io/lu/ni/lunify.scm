(define-module (crates-io lu ni lunify) #:use-module (crates-io))

(define-public crate-lunify-0.1.0 (c (n "lunify") (v "0.1.0") (d (list (d (n "mlua") (r "^0.8") (f (quote ("lua51" "vendored"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "12rfav9rw2zvcqi51d4iljian4n68mm67vd0mali6v2jz79vxywa") (f (quote (("integration" "mlua") ("debug"))))))

(define-public crate-lunify-0.1.1 (c (n "lunify") (v "0.1.1") (d (list (d (n "mlua") (r "^0.8") (f (quote ("lua51" "vendored"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0s8ma6abl8fxd2s34f9j71fz837hx4yffmpinad0gl7v49273dba") (f (quote (("integration" "mlua") ("debug"))))))

(define-public crate-lunify-0.1.2 (c (n "lunify") (v "0.1.2") (d (list (d (n "mlua") (r "^0.8") (f (quote ("lua51" "vendored"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0x2d51cl20jnn8g2n540zl5amn6glm71xcj2k6bjl3b9jf6ag30n") (f (quote (("integration" "mlua") ("debug"))))))

(define-public crate-lunify-1.0.0 (c (n "lunify") (v "1.0.0") (d (list (d (n "mlua") (r "^0.8") (f (quote ("lua51" "vendored"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1w9b0h2kyz6kc7ibwfb833b0rnmmrnk74migpydsh5im29jdgysx") (f (quote (("integration" "mlua") ("debug"))))))

(define-public crate-lunify-1.0.1 (c (n "lunify") (v "1.0.1") (d (list (d (n "mlua") (r "^0.8") (f (quote ("lua51" "vendored"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0pgsjznavgjcdsl88xm9dlyly0iz9wwagj71bpc4x7zns8zlhpb8") (f (quote (("integration" "mlua") ("debug"))))))

(define-public crate-lunify-1.1.0 (c (n "lunify") (v "1.1.0") (d (list (d (n "mlua") (r "^0.8") (f (quote ("lua51" "vendored"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1vdacnkl0v8ivm5fjqhcdcbi2xppd8h8q762hzf1xk4m66brwcdg") (f (quote (("integration" "mlua") ("debug"))))))

