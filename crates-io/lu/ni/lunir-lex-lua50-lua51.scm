(define-module (crates-io lu ni lunir-lex-lua50-lua51) #:use-module (crates-io))

(define-public crate-lunir-lex-lua50-lua51-0.1.0 (c (n "lunir-lex-lua50-lua51") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 2)) (d (n "lifering") (r "^0.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "0jgam8kibbsmr4vf1s8rzcjhy510d56llynw4xdsxf55ls1ch3h4") (y #t)))

(define-public crate-lunir-lex-lua50-lua51-0.2.0 (c (n "lunir-lex-lua50-lua51") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 2)) (d (n "lifering") (r "^0.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "0ry2ivzhr6i6pjprd8a1n8naa6qmphd2bzxlzw1fq9mf5scvz2l4") (y #t)))

(define-public crate-lunir-lex-lua50-lua51-0.3.0 (c (n "lunir-lex-lua50-lua51") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 2)) (d (n "lifering") (r "^0.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "0b97w2nfdb5hksa946qa5cg46kx0ai2dx28l4zydkz94lhshmz1q") (y #t)))

