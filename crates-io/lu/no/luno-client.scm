(define-module (crates-io lu no luno-client) #:use-module (crates-io))

(define-public crate-luno-client-0.1.0 (c (n "luno-client") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1k5853pvv67w4cia54pcs0sfw1aw2pd0ldla3sw3lw97fbyj0qss")))

