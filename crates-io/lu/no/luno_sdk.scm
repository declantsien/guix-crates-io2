(define-module (crates-io lu no luno_sdk) #:use-module (crates-io))

(define-public crate-luno_sdk-0.1.0 (c (n "luno_sdk") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.33") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "141pkizkvlgbaahnwlhf89123jjjzvm9ygzzk79v2dwfm7z408y6")))

(define-public crate-luno_sdk-0.1.1 (c (n "luno_sdk") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.33") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "13qhvqwfsal9hc8payd1p8ghb9fcmzpawmi3xg8x3q052s4g5sdd")))

(define-public crate-luno_sdk-0.1.2 (c (n "luno_sdk") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.33") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "16y7bhk7gjqnjfdjw7zws0hx4q62fzqg7lyln5hz25pv5y55y1x2")))

