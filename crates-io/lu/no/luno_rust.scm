(define-module (crates-io lu no luno_rust) #:use-module (crates-io))

(define-public crate-luno_rust-0.1.0 (c (n "luno_rust") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0gz5s2dxgwh3505gm2s4n0gz3pk3y9lssb2j13y49llw4gxcsvn5")))

