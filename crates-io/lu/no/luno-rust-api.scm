(define-module (crates-io lu no luno-rust-api) #:use-module (crates-io))

(define-public crate-luno-rust-api-0.0.1 (c (n "luno-rust-api") (v "0.0.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11gwhvp1f95cpi4ha22kmdf8d63psgrjz23r6ki2w9a2wjgkh6vp")))

(define-public crate-luno-rust-api-0.1.0 (c (n "luno-rust-api") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1p2vnzl96hp07d2l5pkv2xnjk0hnzr02mwiirzr03g6pym0z1zlj")))

