(define-module (crates-io lu be lubeck) #:use-module (crates-io))

(define-public crate-lubeck-0.0.0 (c (n "lubeck") (v "0.0.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "17zbk4s54ik0gwm19h2xm7v85p3anckfid7908jv907z2q49qnb5") (y #t)))

(define-public crate-lubeck-0.0.0-prealpha.1 (c (n "lubeck") (v "0.0.0-prealpha.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0cn16yyah0bagq0vhasw5f0xwjz9g0r7vfw6fnjlsjv5lha0fs7j")))

(define-public crate-lubeck-0.0.0-prealpha.2 (c (n "lubeck") (v "0.0.0-prealpha.2") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1kalg50jw0pcak0ha2gdz8cwn5vww0f20gvyihyli9szqhxgv9pk")))

(define-public crate-lubeck-0.0.0-prealpha.3 (c (n "lubeck") (v "0.0.0-prealpha.3") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1z166nna22gp8k53wg46kk4xmhvh2skdr29qdiyiq2w7wyx7gz6p")))

(define-public crate-lubeck-0.0.0-prealpha.4 (c (n "lubeck") (v "0.0.0-prealpha.4") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0sg4qf5ggr7j98x0j0zvssxczl4mw90vz8ic3jrbz1ckhxr916x5")))

(define-public crate-lubeck-0.0.0-prealpha.5-abandoned (c (n "lubeck") (v "0.0.0-prealpha.5-abandoned") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1lln1k1sim2qk5722sa0982vsxixw6hxfic1ywfpjfdd2qn5y29s")))

