(define-module (crates-io lu a_ lua_bind_hash) #:use-module (crates-io))

(define-public crate-lua_bind_hash-1.0.0 (c (n "lua_bind_hash") (v "1.0.0") (h "1azqn76fl3mray7qnl5iixj9xf8vxbx5igcv9apj199k8kyhnx2k") (y #t)))

(define-public crate-lua_bind_hash-1.0.1 (c (n "lua_bind_hash") (v "1.0.1") (h "12himfbi77wvq1nbv3pjax0kp6k3x6dca82xgmmdhfcb37ypb5pf")))

