(define-module (crates-io lu a_ lua_actor) #:use-module (crates-io))

(define-public crate-lua_actor-0.1.0 (c (n "lua_actor") (v "0.1.0") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "02fmb9f0fccwik9rnnjbwv753x4avjsb0pavscpff9mh624gfk65") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.1 (c (n "lua_actor") (v "0.1.1") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "0d4w7f9wi6z49nfr49s11r8cml347p890h4iw1kd2arrw3gi45jf") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.2 (c (n "lua_actor") (v "0.1.2") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "1dfg7nry2lriilvs2isv7d2j6f9vmv2glbajr9a01c7visw3zllf") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.3 (c (n "lua_actor") (v "0.1.3") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "1plvsri398hr0xykah6321mg4zwrbdj0q98dy1rd4jh90x6hc3dl") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.4 (c (n "lua_actor") (v "0.1.4") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "0v0hzpx67qndvd3cjqxjdzjyf83rjmjzkvbb31j44v0j6pm7x2li") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.5 (c (n "lua_actor") (v "0.1.5") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "1783xkzdvm26rdszcbj82bf4qg40w40zr88lvv50pzhwvmg5590h") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.6 (c (n "lua_actor") (v "0.1.6") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "1bc8misjar4h6gdkqf0hz8sx13yxys2h8dzmfj65l29371amkq0b") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.7 (c (n "lua_actor") (v "0.1.7") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "1r8mvl7i9laaz8mmhl3agmdl7fa2qasxkk7pnc7cnrv1v1cj99js") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.8 (c (n "lua_actor") (v "0.1.8") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "0cnx4h6dw7pqkwhn6sqc4zjj3pwg2kxhqfl8y8rkf5vsj247khi6") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.9 (c (n "lua_actor") (v "0.1.9") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "028bnv8gg0gqvsqzr2z1qv5cq8pvp39li510jwxbjjmb0k6p1awy") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.10 (c (n "lua_actor") (v "0.1.10") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "0vf0dmw47bk22k8b3885klfjnhw0p1vx2jyrz0viiykpss6icqlh") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.11 (c (n "lua_actor") (v "0.1.11") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "1b2xm1rhjl857baymnpjir5462ayanz2l5f8vwdwsik9mz9j598p") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.12 (c (n "lua_actor") (v "0.1.12") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "1w3fphpwvi867xvy7cgxfncrdbvzmik4mvjhsa5iw58g88pxmfpn") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.13 (c (n "lua_actor") (v "0.1.13") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "1lsmqf7w2mc4wfwqhbg7vgrc8af4z8cr37xgrbrd8vk0h2zv71ag") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.14 (c (n "lua_actor") (v "0.1.14") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "1w0vzr4l3c4v6b9wrmq4rc9v3dxi01hib9lr1228v454k1dcvccj") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.15 (c (n "lua_actor") (v "0.1.15") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "02mim6bsp4pld2gclhja287r521mqfk2zqz9fbx3j25dcss2yhvc") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.16 (c (n "lua_actor") (v "0.1.16") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "1j5jq3gqz23kmkxx7mq5ii42d33agff791zwjikj1p663f0pg7gf") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.17 (c (n "lua_actor") (v "0.1.17") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "0qr2fblx274cq9q6z0a0l61qjxc8950xdg2dayifcr26qi0ms3ib") (f (quote (("default"))))))

(define-public crate-lua_actor-0.1.18 (c (n "lua_actor") (v "0.1.18") (d (list (d (n "fp_rust") (r "0.*") (d #t) (k 0)) (d (n "rlua") (r "0.*") (d #t) (k 0)))) (h "0nzd803nvb6p973lz61wc1gya09ykr5v2ipfwsqk8ah3jx7h4rqb") (f (quote (("default"))))))

(define-public crate-lua_actor-0.2.0 (c (n "lua_actor") (v "0.2.0") (d (list (d (n "fp_rust") (r "^0.1.40") (d #t) (k 0)) (d (n "rlua") (r "^0.17.0") (d #t) (k 0)))) (h "1klym43pfyk3sd3l0zg120qxz2f6lrmrjmdpn1jiz34cna13s8cl") (f (quote (("default"))))))

(define-public crate-lua_actor-0.2.1 (c (n "lua_actor") (v "0.2.1") (d (list (d (n "fp_rust") (r "^0.1.40") (d #t) (k 0)) (d (n "rlua") (r "^0.17.0") (d #t) (k 0)))) (h "1q2kc5p446jb8d5drqvfgfhpdxgsbjgb6gslq3xr7qvv5wlxgfx4") (f (quote (("default"))))))

(define-public crate-lua_actor-0.2.2 (c (n "lua_actor") (v "0.2.2") (d (list (d (n "fp_rust") (r "^0.1.40") (d #t) (k 0)) (d (n "rlua") (r "^0.17.0") (d #t) (k 0)))) (h "0d47m0zmi3bc4408lyzf1b0ixsmf95qp6m91c2n9cfk29x3x570a") (f (quote (("default"))))))

(define-public crate-lua_actor-0.2.3 (c (n "lua_actor") (v "0.2.3") (d (list (d (n "fp_rust") (r "^0.1.40") (d #t) (k 0)) (d (n "rlua") (r "^0.17.0") (d #t) (k 0)))) (h "1ym8il9bmqgqnr5s1zfr9jd78v47842wwaz76gg6y8h2fpylwm0j") (f (quote (("default"))))))

(define-public crate-lua_actor-0.2.4 (c (n "lua_actor") (v "0.2.4") (d (list (d (n "fp_rust") (r "^0.1.40") (d #t) (k 0)) (d (n "rlua") (r "^0.17.0") (d #t) (k 0)))) (h "013g8hgi62330fdlxqfrgvp9r8qbhpndryj39qvx50v897x4laiy") (f (quote (("default"))))))

