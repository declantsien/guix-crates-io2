(define-module (crates-io lu a_ lua_engine) #:use-module (crates-io))

(define-public crate-lua_engine-0.1.0 (c (n "lua_engine") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lua_engine_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0kq9cm4vr3i545k0dyjjfrjr7y44gq8cwv5p8ny6a1nx403cb5sa")))

(define-public crate-lua_engine-0.1.1 (c (n "lua_engine") (v "0.1.1") (d (list (d (n "lua_engine_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0lifzsqw2n5f34zgwnw9jsjj0jzqjnb7x4jw8awd5az7z6c0ny5i")))

