(define-module (crates-io lu a_ lua_engine_macros) #:use-module (crates-io))

(define-public crate-lua_engine_macros-0.1.0 (c (n "lua_engine_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "02cz2vn7pm3mwa2p8r3csh080bnmvf5px0z1frqv3i1rnv00g8nb")))

