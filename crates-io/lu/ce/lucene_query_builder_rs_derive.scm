(define-module (crates-io lu ce lucene_query_builder_rs_derive) #:use-module (crates-io))

(define-public crate-lucene_query_builder_rs_derive-0.2.1 (c (n "lucene_query_builder_rs_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1hvzb2vs50d659sp720gd1m5mj7yxqdhxdv26wcfl8nca9qfrrlm")))

(define-public crate-lucene_query_builder_rs_derive-0.2.2 (c (n "lucene_query_builder_rs_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "0gzykgv8mwbd85z7lv68zkk9px99pl868h7q95rs7r52ssjl7sc5")))

(define-public crate-lucene_query_builder_rs_derive-0.2.3 (c (n "lucene_query_builder_rs_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "04jb1m6p7nii620m9hk8v3m1z2asd0r7m7k0vpn1knzkfyywxwc2")))

(define-public crate-lucene_query_builder_rs_derive-0.3.0 (c (n "lucene_query_builder_rs_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1rnk37ywq8dkyh31azra6pl3ba3rli8dxqawghc8rhfv92rbpslp")))

