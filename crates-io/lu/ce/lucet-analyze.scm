(define-module (crates-io lu ce lucet-analyze) #:use-module (crates-io))

(define-public crate-lucet-analyze-0.1.1 (c (n "lucet-analyze") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.21") (d #t) (k 0)) (d (n "lucet-module-data") (r "^0.1.1") (d #t) (k 0)))) (h "0n7py01jn1c69pbmrjx3ykzrjrcakqa8hxkyr78m2rbc0gnay4sl")))

(define-public crate-lucet-analyze-0.1.2 (c (n "lucet-analyze") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.21") (d #t) (k 0)) (d (n "lucet-module-data") (r "^0.1.1") (d #t) (k 0)))) (h "11pipifki4slhgrmfj4hw56430dbs4cz8r5m6vwxdnkazgqf9g7v")))

