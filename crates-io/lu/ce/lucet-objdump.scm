(define-module (crates-io lu ce lucet-objdump) #:use-module (crates-io))

(define-public crate-lucet-objdump-0.2.0 (c (n "lucet-objdump") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.24") (d #t) (k 0)) (d (n "lucet-module") (r "^0.2.0") (d #t) (k 0)))) (h "11q9x8493vc9azcm9np3lbsdadh6d2wr2mcrjjk5jg9cdqizyhpa")))

(define-public crate-lucet-objdump-0.2.1 (c (n "lucet-objdump") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.24") (d #t) (k 0)) (d (n "lucet-module") (r "^0.2.1") (d #t) (k 0)))) (h "1zpcagfxwddlyl9r8aj9n56x80yjb0g20kmshlkrm7jbqm7y5xj5")))

(define-public crate-lucet-objdump-0.3.0 (c (n "lucet-objdump") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.24") (d #t) (k 0)) (d (n "lucet-module") (r "^0.3.0") (d #t) (k 0)))) (h "1bxfa6hz5xcg9ix0icfdmzh0652r7vjjq0hayvykw1dffyh45zzf")))

(define-public crate-lucet-objdump-0.3.1 (c (n "lucet-objdump") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.24") (d #t) (k 0)) (d (n "lucet-module") (r "^0.3.1") (d #t) (k 0)))) (h "0sqm91n7jl34n9x1kd9hgq3bc9kl05jzar6md783ml45kkgza2dx")))

(define-public crate-lucet-objdump-0.4.1 (c (n "lucet-objdump") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "goblin") (r "^0.0.24") (d #t) (k 0)) (d (n "lucet-module") (r "^0.4.1") (d #t) (k 0)))) (h "0rllf94cazkjikhwfzrnjdrr3vfc0q2ky1p0slnxzxh73ajf3xkj")))

(define-public crate-lucet-objdump-0.5.0 (c (n "lucet-objdump") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "goblin") (r "^0.0.24") (d #t) (k 0)) (d (n "lucet-module") (r "= 0.5.0") (d #t) (k 0)))) (h "0iw6ikqp1z1ij5vcw4qlfhz3yd2kwymr38xj94zaivl6cvc03q3m") (y #t)))

(define-public crate-lucet-objdump-0.4.3 (c (n "lucet-objdump") (v "0.4.3") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "goblin") (r "^0.0.24") (d #t) (k 0)) (d (n "lucet-module") (r "= 0.4.3") (d #t) (k 0)))) (h "17hb18m40c59jlcrysakgpns7f8lvdh1sn7fzdhmvnmhyc24w96x")))

(define-public crate-lucet-objdump-0.5.1 (c (n "lucet-objdump") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "goblin") (r "^0.0.24") (d #t) (k 0)) (d (n "lucet-module") (r "= 0.5.1") (d #t) (k 0)))) (h "00gqs6apmx506l1nbgf9wsymwyakkgzp9rcncm191np463vw6cck")))

(define-public crate-lucet-objdump-0.6.0 (c (n "lucet-objdump") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "goblin") (r "^0.0.24") (d #t) (k 0)) (d (n "lucet-module") (r "= 0.6.0") (d #t) (k 0)))) (h "1qkgm9f40ijg1qqn780w4njkbr128gkanrxxkx62wxqyf9j6hdaz")))

(define-public crate-lucet-objdump-0.6.1 (c (n "lucet-objdump") (v "0.6.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "goblin") (r "^0.0.24") (d #t) (k 0)) (d (n "lucet-module") (r "= 0.6.1") (d #t) (k 0)))) (h "0hch2vkany3lywv74qpy3r9r5dqjaffj8gf3ksiwxzhj8zka0r5a")))

