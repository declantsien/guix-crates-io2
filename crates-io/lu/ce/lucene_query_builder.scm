(define-module (crates-io lu ce lucene_query_builder) #:use-module (crates-io))

(define-public crate-lucene_query_builder-0.1.0 (c (n "lucene_query_builder") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "08fii7iwm160bhs6dikj43pw721cs4gb2pm7r5v0c13ww93vp07j")))

(define-public crate-lucene_query_builder-0.2.0 (c (n "lucene_query_builder") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1zx53npb5mipf9yavnjcaddalh32lk7jgx1s1fwfh4x09391ibz2")))

(define-public crate-lucene_query_builder-0.2.1 (c (n "lucene_query_builder") (v "0.2.1") (d (list (d (n "lucene_query_builder_rs_derive") (r "^0.2.1") (d #t) (k 0)))) (h "02nnrhklhjj6w1zyfzmgyb82a5lzafdlwg4zd6qx9w92ils2nfgc")))

(define-public crate-lucene_query_builder-0.2.2 (c (n "lucene_query_builder") (v "0.2.2") (d (list (d (n "lucene_query_builder_rs_derive") (r "^0.2.2") (d #t) (k 0)))) (h "1f7z21gqpy9vmx6qjqc6frd7q3n95kfrjrxw51jr3iim21w52pnj")))

(define-public crate-lucene_query_builder-0.2.3 (c (n "lucene_query_builder") (v "0.2.3") (d (list (d (n "lucene_query_builder_rs_derive") (r "^0.2.2") (d #t) (k 0)))) (h "1czv34732w0glbsi8imfyxmpdpswlq984k5i9ws5djnbbfvn50b1")))

(define-public crate-lucene_query_builder-0.2.4 (c (n "lucene_query_builder") (v "0.2.4") (d (list (d (n "lucene_query_builder_rs_derive") (r "^0.2.3") (d #t) (k 0)))) (h "05v1gxl8xm2ld6dngd2fdjgsdknc3wwp261wrr13y5ngmhzn9ggy")))

(define-public crate-lucene_query_builder-0.3.0 (c (n "lucene_query_builder") (v "0.3.0") (d (list (d (n "lucene_query_builder_rs_derive") (r "^0.3.0") (d #t) (k 0)))) (h "073jx1djvg4aazx8hd5hf7glmvjfd2jjmgp2b6m54y49hk8518h6")))

