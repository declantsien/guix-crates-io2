(define-module (crates-io lu ce lucet-wasi-sdk) #:use-module (crates-io))

(define-public crate-lucet-wasi-sdk-0.1.1 (c (n "lucet-wasi-sdk") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lucet-module-data") (r "^0.1.1") (d #t) (k 0)) (d (n "lucetc") (r "^0.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "1rfk5i6l6h780x7i1hgz7hpl69sf9zxbypcymx7wa5dd22wcpjfn")))

(define-public crate-lucet-wasi-sdk-0.2.0 (c (n "lucet-wasi-sdk") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lucet-module") (r "^0.2.0") (d #t) (k 0)) (d (n "lucet-validate") (r "^0.2.0") (d #t) (k 2)) (d (n "lucetc") (r "^0.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "15b8xpgr3d0137nxkfy27f8w8xdvq0zwsvdyc5ayvprb8dlh4y9r")))

(define-public crate-lucet-wasi-sdk-0.2.1 (c (n "lucet-wasi-sdk") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lucet-module") (r "^0.2.1") (d #t) (k 0)) (d (n "lucetc") (r "^0.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "1aldxz3axwnblwmcnxa306jc38g9msqqaddmsfffyb250w6cvqwv")))

(define-public crate-lucet-wasi-sdk-0.3.0 (c (n "lucet-wasi-sdk") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lucet-module") (r "^0.3.0") (d #t) (k 0)) (d (n "lucetc") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "0zs0mg8ia9lchaip5wq718v4vajpmwyrb83fxs01x20vb77glg7j")))

(define-public crate-lucet-wasi-sdk-0.3.1 (c (n "lucet-wasi-sdk") (v "0.3.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lucet-module") (r "^0.3.1") (d #t) (k 0)) (d (n "lucet-validate") (r "^0.3.1") (d #t) (k 2)) (d (n "lucetc") (r "^0.3.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "19z52c3zy0q20hw68hcnadgdn7dn9n0ggdxak365qmnssw7fpp2l")))

(define-public crate-lucet-wasi-sdk-0.4.1 (c (n "lucet-wasi-sdk") (v "0.4.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lucet-module") (r "^0.4.1") (d #t) (k 0)) (d (n "lucet-validate") (r "^0.4.1") (d #t) (k 2)) (d (n "lucetc") (r "^0.4.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "0cwfw0j2pffk6z6anwmxfilk4mn6k2mbhg5bk5qx4phgjmijm4xk")))

(define-public crate-lucet-wasi-sdk-0.5.0 (c (n "lucet-wasi-sdk") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lucet-module") (r "= 0.5.0") (d #t) (k 0)) (d (n "lucet-validate") (r "= 0.5.0") (d #t) (k 2)) (d (n "lucetc") (r "= 0.5.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1knm4d341xj3jnwh7wb8l655dl8rfacyl0fkgl478hc1k6hc64ls") (y #t)))

(define-public crate-lucet-wasi-sdk-0.4.3 (c (n "lucet-wasi-sdk") (v "0.4.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lucet-module") (r "= 0.4.3") (d #t) (k 0)) (d (n "lucet-validate") (r "= 0.4.3") (d #t) (k 2)) (d (n "lucetc") (r "= 0.4.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "07jqaz1j9xz27nbiar17bfizjja855c05hgrcnjxdikh6m8xqdxf")))

(define-public crate-lucet-wasi-sdk-0.5.1 (c (n "lucet-wasi-sdk") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lucet-module") (r "= 0.5.1") (d #t) (k 0)) (d (n "lucet-validate") (r "= 0.5.1") (d #t) (k 2)) (d (n "lucetc") (r "= 0.5.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.9") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1x77wgmgmj4mc4a9g5nnmzivg5h24zdqcs5r7nw2dhyxn3jy2bdj")))

(define-public crate-lucet-wasi-sdk-0.6.0 (c (n "lucet-wasi-sdk") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lucet-module") (r "= 0.6.0") (d #t) (k 0)) (d (n "lucet-validate") (r "= 0.6.0") (d #t) (k 2)) (d (n "lucetc") (r "= 0.6.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.9") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1v7fq9fmpndwra8g5nrllbc1qd3g1s15k51sdksx42nm15qdq54g")))

(define-public crate-lucet-wasi-sdk-0.6.1 (c (n "lucet-wasi-sdk") (v "0.6.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lucet-module") (r "= 0.6.1") (d #t) (k 0)) (d (n "lucet-validate") (r "= 0.6.1") (d #t) (k 2)) (d (n "lucetc") (r "= 0.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.9") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "0s16rv8zl78cli5hv0abfzmwbdrly8y9xv8bah9h2d3wf0nbkaqh")))

