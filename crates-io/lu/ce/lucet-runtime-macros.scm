(define-module (crates-io lu ce lucet-runtime-macros) #:use-module (crates-io))

(define-public crate-lucet-runtime-macros-0.5.0 (c (n "lucet-runtime-macros") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yb42s01x29i45cvn8lgxkfz7w232vb5022nwf05fb1fwsdyrf97") (y #t)))

(define-public crate-lucet-runtime-macros-0.5.1 (c (n "lucet-runtime-macros") (v "0.5.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hdk7432fq3srk59kj7fhil8m2kfg7g515daiam0nx1hx0fgwwyc")))

(define-public crate-lucet-runtime-macros-0.6.0 (c (n "lucet-runtime-macros") (v "0.6.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jn1za3b068fsdipppjw18j8zqlhclvmsbj67bivk1yvahi77rk6")))

(define-public crate-lucet-runtime-macros-0.6.1 (c (n "lucet-runtime-macros") (v "0.6.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1aixz4mf7vqzrvlkhxvl4mmgrv8z6cx7fm4bnyb6inlkyml8rfhf")))

