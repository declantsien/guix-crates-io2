(define-module (crates-io lu ar luar) #:use-module (crates-io))

(define-public crate-luar-0.1.0 (c (n "luar") (v "0.1.0") (d (list (d (n "ferris-says") (r "^0.2") (d #t) (k 0)))) (h "0yg8wncs8pqg5ada7dvhr55jrpzq87rjlaxban25353yjcql13lr")))

(define-public crate-luar-0.1.1 (c (n "luar") (v "0.1.1") (d (list (d (n "ferris-says") (r "^0.2") (d #t) (k 0)))) (h "186mk6ym7d81nf0k53czvl7p6920qmni2j5ifn6575xjfgi5kp71")))

