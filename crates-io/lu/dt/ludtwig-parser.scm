(define-module (crates-io lu dt ludtwig-parser) #:use-module (crates-io))

(define-public crate-ludtwig-parser-0.2.0 (c (n "ludtwig-parser") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "nom") (r "~6.0.1") (d #t) (k 0)))) (h "1h49r81q577s086xklgvq46f5r5hij6l4nxwnfmbjhhbcl6p93k2")))

(define-public crate-ludtwig-parser-0.2.1 (c (n "ludtwig-parser") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "nom") (r "~6.1") (d #t) (k 0)))) (h "1iir5pvxl5n3gl8qa8rcn2zd33ryvqar4w1slijcsm5dlqxs0m1l")))

(define-public crate-ludtwig-parser-0.3.0 (c (n "ludtwig-parser") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "nom") (r "~6.1") (d #t) (k 0)))) (h "144n8p5s1qh7mj49ijdhpp3426rxx90f22x0dfbhcd5x3f65ym8w")))

(define-public crate-ludtwig-parser-0.3.1 (c (n "ludtwig-parser") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "nom") (r "~6.1") (d #t) (k 0)))) (h "0h93skc8nxscfmdry3c21middy67y8xwjynzfsn7cpb02hzdk813")))

(define-public crate-ludtwig-parser-0.3.2 (c (n "ludtwig-parser") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "nom") (r "~6.1") (d #t) (k 0)))) (h "0lm1ic56yydjfr2v2w1c6i73na5mr1fcwr9lhflgjxxq6j93p9v5")))

(define-public crate-ludtwig-parser-0.3.3 (c (n "ludtwig-parser") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "nom") (r "~6.1") (d #t) (k 0)))) (h "0qbasbiyb8dzwg734bcs56gflq43nbs8nf3k7isnkzl8sjrkwfzw")))

(define-public crate-ludtwig-parser-0.4.0 (c (n "ludtwig-parser") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rowan") (r "^0.15.10") (d #t) (k 0)))) (h "15qyxjdv4hc87mbcsfcsjgp4shnr9p2zz0lnvkndlkavggb6x4pv")))

(define-public crate-ludtwig-parser-0.5.0 (c (n "ludtwig-parser") (v "0.5.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rowan") (r "^0.15.10") (d #t) (k 0)))) (h "0q3pna2n2m1xsbf6s4dl0h67ls6b622zhj49wssgcrsk2jn87y01")))

(define-public crate-ludtwig-parser-0.5.1 (c (n "ludtwig-parser") (v "0.5.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rowan") (r "^0.15.10") (d #t) (k 0)))) (h "1shhchls2jcjm2aj7627w9xlvx6m9pvzq6fhsgzc1wwipy14rzii")))

(define-public crate-ludtwig-parser-0.5.2 (c (n "ludtwig-parser") (v "0.5.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "rowan") (r "^0.15.15") (d #t) (k 0)))) (h "0pixqrlaw96dnmjhzz696vbhdq34jb7jranffzl9xpnqa75fjvlx")))

(define-public crate-ludtwig-parser-0.5.3 (c (n "ludtwig-parser") (v "0.5.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "rowan") (r "^0.15.15") (d #t) (k 0)))) (h "1krnh5rpd41jjbqlgx57s1f9wvdgblgnri85cmr4c16dm7gqc307")))

