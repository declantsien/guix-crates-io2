(define-module (crates-io lu tg lutgen-palettes) #:use-module (crates-io))

(define-public crate-lutgen-palettes-0.1.0 (c (n "lutgen-palettes") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exoquant") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)) (d (n "tera") (r "^1") (k 1)))) (h "0kyjc7xqn6psyz140p0klx77by5pqc9japiacdanb44l90cinxpm") (f (quote (("default"))))))

(define-public crate-lutgen-palettes-0.1.1 (c (n "lutgen-palettes") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exoquant") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tera") (r "^1") (k 1)))) (h "0r9z78vjmjlsm1p0xlphmywx1kvz52lg2h9k0wqgql7cnvd7liwz") (f (quote (("default"))))))

(define-public crate-lutgen-palettes-0.2.0 (c (n "lutgen-palettes") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exoquant") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tera") (r "^1") (k 1)))) (h "18rkx3zxbdz3szsghajjjg59bv7rp7sc7p5d1mmmiix7wljk133n") (f (quote (("default"))))))

(define-public crate-lutgen-palettes-0.2.1 (c (n "lutgen-palettes") (v "0.2.1") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exoquant") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tera") (r "^1") (k 1)))) (h "1mhjm0wlk9ckpg9yr9sag5pg6gv0hpynh67zn8pbja85mlmnqs9y") (f (quote (("default"))))))

(define-public crate-lutgen-palettes-0.2.2 (c (n "lutgen-palettes") (v "0.2.2") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exoquant") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tera") (r "^1") (k 1)))) (h "16crya3542dssxj2cn2lgbwm3dbxhg6kzjdvnbpd8wjngdygmg74") (f (quote (("default"))))))

(define-public crate-lutgen-palettes-0.2.3 (c (n "lutgen-palettes") (v "0.2.3") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tera") (r "^1") (k 1)))) (h "1z0ldqb3yv3l5wckpmsidd4yhl3wvbw94zpjl5my7mn1ibxi6cli") (f (quote (("default"))))))

(define-public crate-lutgen-palettes-0.2.4 (c (n "lutgen-palettes") (v "0.2.4") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tera") (r "^1") (k 1)))) (h "04mqkw8678q0mvak5hs0g35pv0frr8bz47vj79xbl85x72cb2r0i") (f (quote (("default"))))))

(define-public crate-lutgen-palettes-0.2.5 (c (n "lutgen-palettes") (v "0.2.5") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tera") (r "^1") (k 1)))) (h "02vp3l9sfl33ls6js23k6pc89iip64ljpn7q0s3aym8y2bl8plf2") (f (quote (("default"))))))

(define-public crate-lutgen-palettes-0.2.6 (c (n "lutgen-palettes") (v "0.2.6") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tera") (r "^1") (k 1)))) (h "1g691295hbikd8i9ww81m95h4c1r4drk6phyxn3cbg4gzc223lvf") (f (quote (("default"))))))

(define-public crate-lutgen-palettes-0.2.7 (c (n "lutgen-palettes") (v "0.2.7") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tera") (r "^1") (k 1)))) (h "0jm1rblvmn0dksif2i2cl50g25zwzw8bsj9wz17hrd2f9phjjbvj") (f (quote (("default"))))))

(define-public crate-lutgen-palettes-0.3.0 (c (n "lutgen-palettes") (v "0.3.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tera") (r "^1") (k 1)))) (h "00qr0nn710m7asf15rzpavxgd84jjr5lr9ijqc0j9bhdlvhnza2d") (f (quote (("default"))))))

