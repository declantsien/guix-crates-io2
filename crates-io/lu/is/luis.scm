(define-module (crates-io lu is luis) #:use-module (crates-io))

(define-public crate-luis-0.1.0 (c (n "luis") (v "0.1.0") (h "15m136mgf8hdjcsh5b57rgvfs8hn40nzg6azcm2m5vq5njg18avb")))

(define-public crate-luis-0.1.1 (c (n "luis") (v "0.1.1") (h "00d9km10xwwcbjr7yywwbpq5fvligh1hamrnzmsgrqi1vzfj5v93")))

(define-public crate-luis-0.1.2 (c (n "luis") (v "0.1.2") (h "0xqazq8gmkvjgppmarfkvpablg9q1pl3g7jjfy5xh9adn5qd63i1")))

