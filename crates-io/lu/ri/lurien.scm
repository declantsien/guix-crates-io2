(define-module (crates-io lu ri lurien) #:use-module (crates-io))

(define-public crate-lurien-0.1.0 (c (n "lurien") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "grep") (r "^0") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0hsccd5nm5gxzyp5nwmq62mh51s3djh20nv90b0122qfnpyw4mcm")))

