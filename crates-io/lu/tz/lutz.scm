(define-module (crates-io lu tz lutz) #:use-module (crates-io))

(define-public crate-lutz-1.0.0 (c (n "lutz") (v "1.0.0") (d (list (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)))) (h "0ar65vcnrli27n3qwb7k47476i5jqpmw3693sk748rzbs3x1yjf4") (f (quote (("nightly"))))))

(define-public crate-lutz-1.1.0 (c (n "lutz") (v "1.1.0") (d (list (d (n "genawaiter") (r "^0.99.1") (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)))) (h "1ycxglw7467z7ygdbp35s95cys7643dqic796d642c5kl4m14zzy") (f (quote (("nightly"))))))

(define-public crate-lutz-1.1.1 (c (n "lutz") (v "1.1.1") (d (list (d (n "auto_impl") (r "^0.4.1") (d #t) (k 0)) (d (n "genawaiter") (r "^0.99.1") (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)))) (h "14047pj0kb5xvzp9c7sww1m2mdfyfs8vgzw5jwsdr5qls0004kic") (f (quote (("nightly"))))))

(define-public crate-lutz-1.2.0 (c (n "lutz") (v "1.2.0") (d (list (d (n "auto_impl") (r "^0.4.1") (d #t) (k 0)) (d (n "genawaiter") (r "^0.99.1") (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)))) (h "174gyvn57z3b8rjdrr6g48zp5b05f48smw61cwxgyd8wvvd38qlq") (f (quote (("nightly"))))))

(define-public crate-lutz-1.3.0 (c (n "lutz") (v "1.3.0") (d (list (d (n "auto_impl") (r "^0.4.1") (d #t) (k 0)) (d (n "genawaiter") (r "^0.99.1") (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)))) (h "11zi3la62dai78acwv77plmzpwm0i0cb8cji55qyfl68icjpy6zh") (f (quote (("nightly"))))))

(define-public crate-lutz-1.3.1 (c (n "lutz") (v "1.3.1") (d (list (d (n "auto_impl") (r "^0.4.1") (d #t) (k 0)) (d (n "genawaiter") (r "^0.99.1") (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)))) (h "0vf2j3hm9b8b20snczkfg4dayqyyvckgx7vlzf7ss3ngvxgq8fz4") (f (quote (("nightly"))))))

(define-public crate-lutz-1.4.0 (c (n "lutz") (v "1.4.0") (d (list (d (n "auto_impl") (r "^1.1.0") (d #t) (k 0)) (d (n "genawaiter") (r "^0.99.1") (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 2)))) (h "0im1gshzakfax2an1g6kczjn4lavs43z2apgdkmkp40kqcc9fqf3") (f (quote (("nightly"))))))

