(define-module (crates-io lu au luau0-src) #:use-module (crates-io))

(define-public crate-luau0-src-0.1.0+luau516 (c (n "luau0-src") (v "0.1.0+luau516") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "08i7vr7phgiqysjgaw7qmmkzwa2ndb4swwk4lss8yys7m80zd4b5")))

(define-public crate-luau0-src-0.2.0+luau519 (c (n "luau0-src") (v "0.2.0+luau519") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1vvgnbzzdggli5hhpifmknyqylxg00g501c7dpnaaw3cjhj7ak8v")))

(define-public crate-luau0-src-0.2.1+luau519 (c (n "luau0-src") (v "0.2.1+luau519") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1pnlc6mrmwyh4pd9vh24xpcv9rq3wh158kbmxlk6a4yhlhkp17ji")))

(define-public crate-luau0-src-0.2.2+luau521 (c (n "luau0-src") (v "0.2.2+luau521") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "0fnpfaammzjfnlikr4b8g282y83h8ikk7b4z49bic8vv8zddy22z")))

(define-public crate-luau0-src-0.3.0+luau526 (c (n "luau0-src") (v "0.3.0+luau526") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1sfcihjd62rs91f41815ngdinlpxfbizrzrya1m1cr2jja2dggn6")))

(define-public crate-luau0-src-0.3.1+luau529 (c (n "luau0-src") (v "0.3.1+luau529") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1ksb2kidm35shiyym4xzji5lnxlamhx2n79dbr3zysaciwszv7d6")))

(define-public crate-luau0-src-0.3.2+luau530 (c (n "luau0-src") (v "0.3.2+luau530") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1icc6fmqvn61h5x5sph9cyw25hc6k6hic8cpydwdh4rvz3pmk8g6")))

(define-public crate-luau0-src-0.3.3+luau531 (c (n "luau0-src") (v "0.3.3+luau531") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "0j6i9h4abs9zgiax5232m99jqrshpb7g9kagrfqxs5hcm65ngj50")))

(define-public crate-luau0-src-0.3.4+luau533 (c (n "luau0-src") (v "0.3.4+luau533") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1ghyfqbjq8za9zzfp4nx61g7gfy49v90xi8wnipivpappfp3i9c0")))

(define-public crate-luau0-src-0.3.5+luau535 (c (n "luau0-src") (v "0.3.5+luau535") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1807vzvii3hcc1arqq3nmbpfa5mxfaf7qkz39kliampb02j1jhkr")))

(define-public crate-luau0-src-0.3.6+luau536 (c (n "luau0-src") (v "0.3.6+luau536") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1fg5j9fr78v0lrhbx3q095s0amsjzyvgya5w7vyvzdmzlqv19xzd")))

(define-public crate-luau0-src-0.3.7+luau541 (c (n "luau0-src") (v "0.3.7+luau541") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1c69cn0k26yaxgl8501ka3aczsklfjsg87k125ss9l0dfwmwm07l")))

(define-public crate-luau0-src-0.3.8+luau545 (c (n "luau0-src") (v "0.3.8+luau545") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "0m3g0dh5bd1cabjlbaa9c3niiy63iqjazx4b2q1k00imllfl1gfx")))

(define-public crate-luau0-src-0.4.0+luau548 (c (n "luau0-src") (v "0.4.0+luau548") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "17xyk4xlxyy15nrj3an7h4mhmbgdffcww4vcp9ngwfs28nz9cnga")))

(define-public crate-luau0-src-0.4.1+luau553 (c (n "luau0-src") (v "0.4.1+luau553") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1i94nqsy3h3i295za0x3wdxmripz7aljx8fjjp2i3ncjv1mp5wx3")))

(define-public crate-luau0-src-0.5.0+luau555 (c (n "luau0-src") (v "0.5.0+luau555") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "0a85jln5m8qzxsy7n9732an29g29a2v8inivzqij34jfr21awvlj")))

(define-public crate-luau0-src-0.5.1+luau558 (c (n "luau0-src") (v "0.5.1+luau558") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "09kn92ykgh8sg0cv4nrnjwx118padppfq15nig155cgr66x6j22y")))

(define-public crate-luau0-src-0.5.2+luau561 (c (n "luau0-src") (v "0.5.2+luau561") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1k25lx3pia6v0dzh8nj51ihh1sslcp3gkayx5iyi6pan4zikaqbl")))

(define-public crate-luau0-src-0.5.3+luau566 (c (n "luau0-src") (v "0.5.3+luau566") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "0rrcqk7m3al5mps4659b90df6srvrc5z5sn2xcbldm851lk50yr3")))

(define-public crate-luau0-src-0.5.4+luau567 (c (n "luau0-src") (v "0.5.4+luau567") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "06jnmjmr42nmp89jh9g3xng49lrsx41m9apm04mn8ilyxz5kgqqp")))

(define-public crate-luau0-src-0.5.5+luau571 (c (n "luau0-src") (v "0.5.5+luau571") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1ck7rz2b3l22kkw7a9rlaf76jcnan722mhjspb107svrds4sjp2g")))

(define-public crate-luau0-src-0.5.6+luau573 (c (n "luau0-src") (v "0.5.6+luau573") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1smm6p3n3b4nyl8hfah59nsb04ayjm4kfbkw2swj6pgklzwq0lpx")))

(define-public crate-luau0-src-0.5.7+luau576 (c (n "luau0-src") (v "0.5.7+luau576") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "16hhndzc34nvybk6mgywljbzkjq85l4shmn6va2i8k8zz8x84w2p")))

(define-public crate-luau0-src-0.5.8+luau577 (c (n "luau0-src") (v "0.5.8+luau577") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1ax92z13rz2f993xqcc04qk3mf0l1gprvjawyyw6wcfhwsdw90a4")))

(define-public crate-luau0-src-0.5.9+luau579 (c (n "luau0-src") (v "0.5.9+luau579") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "07gs30asq4zk23gi3wp80c4h57a8m61lz8cvr5fir34d7mlfi6h3")))

(define-public crate-luau0-src-0.5.10+luau581 (c (n "luau0-src") (v "0.5.10+luau581") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "079is0n4q11wr7vll91qvj35q6sr24kf3d5m8gsn4v3jhby510wb")))

(define-public crate-luau0-src-0.5.11+luau583 (c (n "luau0-src") (v "0.5.11+luau583") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1m96d6k2qrgig8j4hyjvak0q5brnjm27pb6l7m08g8zalg886w8a")))

(define-public crate-luau0-src-0.5.12+luau588 (c (n "luau0-src") (v "0.5.12+luau588") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "0wkw5q37zj80469hl1zzs3iv85zmhkvdbi209lnzmmr3qbcj2h43") (y #t)))

(define-public crate-luau0-src-0.6.0+luau588 (c (n "luau0-src") (v "0.6.0+luau588") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "0jbci99gxf7ka8bj7qy8apd3ayy737k2x2s75ndahqnc4maqyqjc")))

(define-public crate-luau0-src-0.7.0+luau590 (c (n "luau0-src") (v "0.7.0+luau590") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "1cfgjgdnsmm2pd0ffq0nid7cqaikadfk653bxpix8jyqw0vp5w0k")))

(define-public crate-luau0-src-0.7.1+luau591 (c (n "luau0-src") (v "0.7.1+luau591") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "1ixznrvfqgw5qr1a3j01zkybarwsambbgpbgfjgbc2xwrpn01dig")))

(define-public crate-luau0-src-0.7.2+luau592 (c (n "luau0-src") (v "0.7.2+luau592") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "1jyydcid3rdfh241vs5phwafzjxpv3lk4kpv3i2qvdw1dj8qalgr")))

(define-public crate-luau0-src-0.7.3+luau593 (c (n "luau0-src") (v "0.7.3+luau593") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "0bwd8cl0zpmzbbm23v3scpa0d85m2p3h4nj71gkd80342ixpxljb")))

(define-public crate-luau0-src-0.7.4+luau594 (c (n "luau0-src") (v "0.7.4+luau594") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "036jm8av537na427vpvwlriny26kpqrnq96hyk8zszm966y66fpx")))

(define-public crate-luau0-src-0.7.5+luau596 (c (n "luau0-src") (v "0.7.5+luau596") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "01qmap8ldfpfx9pqy2j6dh4sf3qigncahg6yy4ha64vmg3pj38nd")))

(define-public crate-luau0-src-0.7.6+luau598 (c (n "luau0-src") (v "0.7.6+luau598") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "1pbm25904kzxivakqxq6vz4yy4sbp4q98vl2ik89ak5l6a0gfgjk")))

(define-public crate-luau0-src-0.7.7+luau599 (c (n "luau0-src") (v "0.7.7+luau599") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "0pnhcbxmkb06acbd6f1czgr9x7yxy063pk6sn05hcfqr020j0a7k")))

(define-public crate-luau0-src-0.7.8+luau601 (c (n "luau0-src") (v "0.7.8+luau601") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "1nc0fbyjvcsgd88ii22rp5z4wkmd58dsfqa1zpvq595jnf6mv40r")))

(define-public crate-luau0-src-0.7.9+luau603 (c (n "luau0-src") (v "0.7.9+luau603") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "0x78slj33cxl2synjirdwryxkr1q03yd208m1ng26h9f7vl57ln4")))

(define-public crate-luau0-src-0.7.10+luau604 (c (n "luau0-src") (v "0.7.10+luau604") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "1dy87v2f9zikyqrv54gddbykb346gf48xawa7sqv7794w3y06crl")))

(define-public crate-luau0-src-0.7.11+luau606 (c (n "luau0-src") (v "0.7.11+luau606") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "020gg5ymj468fylwi2jw23y7a4p1345y0cdkn8ya6lz9bsac9zq7")))

(define-public crate-luau0-src-0.8.0+luau609 (c (n "luau0-src") (v "0.8.0+luau609") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "0m7hq1fg790c68nvx3sfsql5whfwp6vnn84f3hyzz4p03kvy50f5")))

(define-public crate-luau0-src-0.8.1+luau611 (c (n "luau0-src") (v "0.8.1+luau611") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "0y8j9i84j156h23cr538xxx5hmzn3xg7fswcvnpq0pgmpw6r1s9j")))

(define-public crate-luau0-src-0.8.2+luau613 (c (n "luau0-src") (v "0.8.2+luau613") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "0g9khwbl0kxrzwg0r7a5lflkg4rby9wgibdvjx6f7dipfwsd4x1d")))

(define-public crate-luau0-src-0.8.3+luau614 (c (n "luau0-src") (v "0.8.3+luau614") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "161qmalgb3gv402xryvdz5vp8vi9z0j625dg30yqjx6ha3wwj3ad")))

(define-public crate-luau0-src-0.8.4+luau616 (c (n "luau0-src") (v "0.8.4+luau616") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "1ngv728gw0aki1l0i145xn7yb0rkw0ibg2fnqmrn4q2jirn4rwvl")))

(define-public crate-luau0-src-0.8.5+luau617 (c (n "luau0-src") (v "0.8.5+luau617") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "1skqf1j88q5pyndv87rijg664g48c9ygwca9db3px02xqfw3cbk5")))

(define-public crate-luau0-src-0.8.6+luau622 (c (n "luau0-src") (v "0.8.6+luau622") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "076ynmj2mbg7hwrb0b9iry5c6z4hcs6azvq9j7fzkxq8b4gqqx87")))

(define-public crate-luau0-src-0.8.7+luau624 (c (n "luau0-src") (v "0.8.7+luau624") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "0cqaa9dx34ajr07rz4650lni01rl22a7xk4yq1p6pjnip05xdwwr") (y #t)))

(define-public crate-luau0-src-0.9.0+luau624 (c (n "luau0-src") (v "0.9.0+luau624") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "14cmpsz10gfvh7mmz5irafaxkqrqvx1z69hgzw10zc311kwi3gyl")))

(define-public crate-luau0-src-0.9.1+luau625 (c (n "luau0-src") (v "0.9.1+luau625") (d (list (d (n "cc") (r "^1.0.82") (d #t) (k 0)))) (h "0jlw8lampvby0fngi2jhh3y2dwqw1znmx7lz7cvb4z06ppjlxprr")))

