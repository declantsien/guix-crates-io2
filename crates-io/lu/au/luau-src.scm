(define-module (crates-io lu au luau-src) #:use-module (crates-io))

(define-public crate-luau-src-0.1.0 (c (n "luau-src") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0v5fh7i9hx8c2gbh3gw1p9h44j366hdsa9vgs5ncad3s13z2wb87") (f (quote (("no-link") ("default"))))))

(define-public crate-luau-src-0.3.0 (c (n "luau-src") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.71") (d #t) (k 1)))) (h "1hxgf6ww58y90qny025skdfrkxc3i45ssjicrqvrli8d3ki7qs8d") (f (quote (("link") ("default" "link"))))))

