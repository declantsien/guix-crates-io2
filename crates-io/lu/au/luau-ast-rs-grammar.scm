(define-module (crates-io lu au luau-ast-rs-grammar) #:use-module (crates-io))

(define-public crate-luau-ast-rs-grammar-0.0.1 (c (n "luau-ast-rs-grammar") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "0wa3w9n71yavfr0lvpx6aicq65jmibyzqr768xpizfv1lgswnww2")))

(define-public crate-luau-ast-rs-grammar-0.0.2 (c (n "luau-ast-rs-grammar") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20.3") (d #t) (k 0)))) (h "11zk2d86p2jzkrahgjry8d7isb64mxp51j88rd35c5lirbp2rdhj")))

