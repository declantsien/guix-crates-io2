(define-module (crates-io lu me lumenpyx) #:use-module (crates-io))

(define-public crate-lumenpyx-0.1.0 (c (n "lumenpyx") (v "0.1.0") (d (list (d (n "glium") (r "^0.34.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.9") (d #t) (k 0)))) (h "1jlpbbrwgax69gnqhs9kjf7mk54bkdxn663rfgsccrbcdn4pjgiv")))

