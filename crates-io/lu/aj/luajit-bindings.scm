(define-module (crates-io lu aj luajit-bindings) #:use-module (crates-io))

(define-public crate-luajit-bindings-0.2.0 (c (n "luajit-bindings") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "089x9jl2vkjx1shp4ghrng5cqzs4qpykmpswckzr5c0xqymhah67")))

