(define-module (crates-io lu aj luajit-sys) #:use-module (crates-io))

(define-public crate-luajit-sys-0.0.1 (c (n "luajit-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "18s7a4k3dga0ns88w76mwnm7fziba2l0wjrprqj1l2m2h3j0xw5s")))

(define-public crate-luajit-sys-0.0.2 (c (n "luajit-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "0pcp1jsklk5f78kph5dr3qgggwjhk93bx4hm2prljhykz3wd9cd3")))

(define-public crate-luajit-sys-0.0.3 (c (n "luajit-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "pkg-config") (r "= 0.3.2") (d #t) (k 1)))) (h "03fgyspyyyfm744yl5k8c4qc5flh8cxzsh5wk1jp2rdb8apg8bjy")))

(define-public crate-luajit-sys-0.0.4 (c (n "luajit-sys") (v "0.0.4") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "1pmgbc3jy6vxgjjnqjiy2rynyyb7sjsy9llxnv38in2myk91lxl2")))

(define-public crate-luajit-sys-0.0.5 (c (n "luajit-sys") (v "0.0.5") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "0kv13l6m733r0zlzdd2ai4faffzf1y031y4p5bkpxycacsdc7wvs")))

