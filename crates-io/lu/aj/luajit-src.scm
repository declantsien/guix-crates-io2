(define-module (crates-io lu aj luajit-src) #:use-module (crates-io))

(define-public crate-luajit-src-0.0.0 (c (n "luajit-src") (v "0.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "07xx2b67mwlyf13b7v28pk63l68c2m490x53f37kdkg0y44xgp8s")))

(define-public crate-luajit-src-210.0.0+694d69a (c (n "luajit-src") (v "210.0.0+694d69a") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "035j7092akqsr4zxz3j1c1gqw0f7f0j6svpl96x8v458mcq4zn40")))

(define-public crate-luajit-src-210.1.0+resty31116c4 (c (n "luajit-src") (v "210.1.0+resty31116c4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0ksdmr448m2qlq0i2pr2c7dmaa81jgqbk2w3w5sa3vjqvk9rcbfh")))

(define-public crate-luajit-src-210.1.1+resty29a66f7 (c (n "luajit-src") (v "210.1.1+resty29a66f7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "01ijcsp2fpkppzcccgc21yckypvrh7hj9pkryda7xv1djs7f9zc7")))

(define-public crate-luajit-src-210.1.2+resty29a66f7 (c (n "luajit-src") (v "210.1.2+resty29a66f7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0pz9nxs8q13b7mcglz04c0f0sflnvid2zx36s2b6rvr6hsn33zxr")))

(define-public crate-luajit-src-210.1.3+restyfe08842 (c (n "luajit-src") (v "210.1.3+restyfe08842") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "16q27b04nr8fmf0iz36wsbnaj9mqgvxvw9v5chmlkq6qfy1xxlrn")))

(define-public crate-luajit-src-210.1.4+resty8404d7f (c (n "luajit-src") (v "210.1.4+resty8404d7f") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0nx7k9j0jqanpw56wsicv7r2nqyfvavaklxm4kxg86ircwbhb66d")))

(define-public crate-luajit-src-210.2.0+resty5f13855 (c (n "luajit-src") (v "210.2.0+resty5f13855") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0wiviqvv0y7wcjvv5jcdxsas248h4z4idfbpl02j60lyx8i5gy29") (f (quote (("lua52compat"))))))

(define-public crate-luajit-src-210.3.0+restyeced77f (c (n "luajit-src") (v "210.3.0+restyeced77f") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0621nx5lllz09lpnz5gkwpqs5l6m074ib57wkw0nfxak6fjhfj08")))

(define-public crate-luajit-src-210.3.1+restycd2285f (c (n "luajit-src") (v "210.3.1+restycd2285f") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1z3y8w904s5xa6ljc1k9d2yg1w765r1njm1ka126pafb56gmf1q1")))

(define-public crate-luajit-src-210.3.2+resty1085a4d (c (n "luajit-src") (v "210.3.2+resty1085a4d") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "191vv51bikl4gbl4v709nfnzc8v3a9ghmz12vng5l8hkymb79qmi")))

(define-public crate-luajit-src-210.3.3+resty673aaad (c (n "luajit-src") (v "210.3.3+resty673aaad") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "13kn2yrjcy6l74nnghi0vcvcwjw3bi7y0bf2vlg1zvi0b3kfl09j")))

(define-public crate-luajit-src-210.3.4+resty073ac54 (c (n "luajit-src") (v "210.3.4+resty073ac54") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "03qkh322rh8fnasxgsmr7cqs98c8h6khdx0fvas4593mjplhj2v4")))

(define-public crate-luajit-src-210.4.0+resty124ff8d (c (n "luajit-src") (v "210.4.0+resty124ff8d") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0v3g5165ws4yq7sgagdyvk22axlg2jd1qclxf4c2w6f7q3ib4vzp")))

(define-public crate-luajit-src-210.4.1+restyaa7a722 (c (n "luajit-src") (v "210.4.1+restyaa7a722") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1cr7dl4w0bbdrr37sll4swria44vykphhbpy1hafx5wsbws7ja69")))

(define-public crate-luajit-src-210.4.2+resty89d53e9 (c (n "luajit-src") (v "210.4.2+resty89d53e9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0qdkisf2lghczdckbr5j42z753wvdf9s6iiby53yh2r6v0qkhk7x")))

(define-public crate-luajit-src-210.4.3+resty8384278 (c (n "luajit-src") (v "210.4.3+resty8384278") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0i7g3ijad3m3aqyabssbhc1sgy7j04q7rjjmz9pwf7nzzmd5vvhr")))

(define-public crate-luajit-src-210.4.4+restydf15b79 (c (n "luajit-src") (v "210.4.4+restydf15b79") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1hfwvj9533dvy34si5r7skaqn1xabi1hxdlbbmzgjs7k1pg83w21")))

(define-public crate-luajit-src-210.4.5+resty2cf5186 (c (n "luajit-src") (v "210.4.5+resty2cf5186") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "15yblw421y66iss5gzfwvs75hjm4xhmnyk6qf9i7h0p680m9kdr7")))

(define-public crate-luajit-src-210.4.6+resty2cf5186 (c (n "luajit-src") (v "210.4.6+resty2cf5186") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 0)))) (h "05zia3z4id8p9d1qaa7rqnw3gdvwv5bqnx8l08b5nq3h0a7g4pn4")))

(define-public crate-luajit-src-210.4.7+resty107baaf (c (n "luajit-src") (v "210.4.7+resty107baaf") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 0)))) (h "0cdzv1m79srhg068d1bw5yvsnnq9yzs945zppvdfac615xss6xna")))

(define-public crate-luajit-src-210.4.8+resty107baaf (c (n "luajit-src") (v "210.4.8+resty107baaf") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 0)))) (h "0kkwr0mk56rcxzh08hif0wvwx2xxwm0kbliyv1c5f652nbl6flg0")))

(define-public crate-luajit-src-210.5.0+becf5cc (c (n "luajit-src") (v "210.5.0+becf5cc") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 0)))) (h "02vi3b1w569b6m406jr6jli8bl0qdba51shddrpkzvlxpgg6s049")))

(define-public crate-luajit-src-210.5.1+b94fbfb (c (n "luajit-src") (v "210.5.1+b94fbfb") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 0)))) (h "10klvhy87128jwgmhs5n7kydrg592ab6avqmy6d2b2jlzks11q36")))

(define-public crate-luajit-src-210.5.2+113a168 (c (n "luajit-src") (v "210.5.2+113a168") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 0)))) (h "1vpz579mvf0jchww7c4jhjq2s25hh7lkln5x6cbb268qvfzcfgl2")))

(define-public crate-luajit-src-210.5.3+29b0b28 (c (n "luajit-src") (v "210.5.3+29b0b28") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 0)))) (h "0bbr3lhi2nbm3mwplxl0v7hmhnj3dnyiy6ph974yav4i2f8bhaqc")))

(define-public crate-luajit-src-210.5.4+c525bcb (c (n "luajit-src") (v "210.5.4+c525bcb") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 0)))) (h "024rb8i25m6gza3rw0c04hnlnnfaqpvf9v23xx8cn8ids57an41a")))

(define-public crate-luajit-src-210.5.5+f2336c4 (c (n "luajit-src") (v "210.5.5+f2336c4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^5.0") (d #t) (k 0)))) (h "1i1rz4qkcknbsbsmmaaxqxmvwfx6278csxbx8v0v3qzlj2bvmg6q")))

(define-public crate-luajit-src-210.5.6+9cc2e42 (c (n "luajit-src") (v "210.5.6+9cc2e42") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^6.0") (d #t) (k 0)))) (h "0ms1amqfxsmdyi23bxb87z7l0fqcr1gf5cwbyj3w3zy9b7c6bcr3")))

(define-public crate-luajit-src-210.5.7+d06beb0 (c (n "luajit-src") (v "210.5.7+d06beb0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^6.0") (d #t) (k 0)))) (h "1r79w95mpcmywl4fxhsm13hxc8zbn7wc32pl9iq8ggxbrpd1y98d")))

(define-public crate-luajit-src-210.5.8+5790d25 (c (n "luajit-src") (v "210.5.8+5790d25") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^6.0") (d #t) (k 0)))) (h "0ijk6xg4fqavhcxyzpfvdzszd1n30a4wpwppq8gqfbkrmpcih7s4")))

