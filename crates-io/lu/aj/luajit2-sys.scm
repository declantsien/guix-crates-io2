(define-module (crates-io lu aj luajit2-sys) #:use-module (crates-io))

(define-public crate-luajit2-sys-0.0.1 (c (n "luajit2-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.40") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v4lc2jxlnvzik5184bvdqr3vqlsw569dnb91w5fal3w5lrfyfab")))

(define-public crate-luajit2-sys-0.0.2 (c (n "luajit2-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.40") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1whssk7syhqbhr2jab3ny57ikxzm38wvlvm0bdj280jsrp67mfrk") (l "luajit")))

