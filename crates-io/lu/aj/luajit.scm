(define-module (crates-io lu aj luajit) #:use-module (crates-io))

(define-public crate-luajit-0.1.0 (c (n "luajit") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.32") (d #t) (k 0)))) (h "0zkqlcffhrfw0ihg589rm35piv1s4svmi4hmb5nkfg15xy4kp3q4")))

(define-public crate-luajit-0.1.1 (c (n "luajit") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.32") (d #t) (k 0)))) (h "0m3fp23bx8li0xj89a2nfvmd4zji8ybwwg0nq6zvbpvyizmxhvi1")))

