(define-module (crates-io lu sl lusl) #:use-module (crates-io))

(define-public crate-lusl-1.0.0 (c (n "lusl") (v "1.0.0") (d (list (d (n "md-5") (r "^0.10.5") (d #t) (k 0)))) (h "0lpximrmkqzkcf6nrb3bajr1zmcsk80z5q54lvn99iz5nywjmsmg")))

(define-public crate-lusl-1.1.0 (c (n "lusl") (v "1.1.0") (d (list (d (n "md-5") (r "^0.10.5") (d #t) (k 0)))) (h "1a968b2kym9n9k21wl9y48inyp0cj403gqmjqbd3r9rn886bz673")))

(define-public crate-lusl-1.2.0 (c (n "lusl") (v "1.2.0") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (f (quote ("stream"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0.0") (d #t) (k 0)) (d (n "zip_archive") (r "^1.2.2") (d #t) (k 0)))) (h "010x89yy06l09mzmryffvlvpy77la8yf1d0i9jhgfaj584yy6ahj")))

(define-public crate-lusl-1.2.1 (c (n "lusl") (v "1.2.1") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (f (quote ("stream"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0.0") (d #t) (k 0)))) (h "0xg8z6jp350vh9hh30h9al139dpqkmyxnimjw5bvbqz3vrcjzf73")))

(define-public crate-lusl-1.2.2 (c (n "lusl") (v "1.2.2") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (f (quote ("stream"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0.0") (d #t) (k 0)))) (h "03h9rxwklh6lxay74nz4zql41gz4q2ifxc5cvkfwnn412x4rax8s")))

(define-public crate-lusl-2.0.0 (c (n "lusl") (v "2.0.0") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (f (quote ("stream"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0.0") (d #t) (k 0)))) (h "0wj2s4f6iy95y14ic1vlhl0vxpfd46qagq7xmnmd6812gl0q0mfn")))

(define-public crate-lusl-2.0.2 (c (n "lusl") (v "2.0.2") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (f (quote ("stream"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0.0") (d #t) (k 0)))) (h "0mqryzccm7awva4wl715panpzlfz4bn4bv6z0gp0aiv1hny04h1g")))

(define-public crate-lusl-2.1.0 (c (n "lusl") (v "2.1.0") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (f (quote ("stream"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "044wv4yqrn2245i4wm0f0a1mv8wr6fqkryh6fyyd8nwyv3s2ccx7")))

