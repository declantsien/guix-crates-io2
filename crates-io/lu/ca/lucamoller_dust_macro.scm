(define-module (crates-io lu ca lucamoller_dust_macro) #:use-module (crates-io))

(define-public crate-lucamoller_dust_macro-0.1.0 (c (n "lucamoller_dust_macro") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "printing"))) (d #t) (k 0)))) (h "04prlaynxmlkisgj1dvryykrzw1lilbdajpcd1j2b3gs3bl6jhrs") (r "1.75")))

(define-public crate-lucamoller_dust_macro-0.1.1 (c (n "lucamoller_dust_macro") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "printing"))) (d #t) (k 0)))) (h "0sf6bzcj64yfj71xj3aw41x76qx59lx9cz9rlyywxrd5lzpsvimx") (r "1.75")))

