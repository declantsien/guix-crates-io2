(define-module (crates-io lu ca lucas_lehmer) #:use-module (crates-io))

(define-public crate-lucas_lehmer-0.1.0 (c (n "lucas_lehmer") (v "0.1.0") (d (list (d (n "num-integer") (r "^0.1.39") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "ramp") (r "^0.5.1") (d #t) (k 0)))) (h "04imcm6032xsf1hddqj1fgw81f0q98zxd33vjdbdjbk6b3p21lmk")))

