(define-module (crates-io lu cc lucchetto) #:use-module (crates-io))

(define-public crate-lucchetto-0.1.0 (c (n "lucchetto") (v "0.1.0") (d (list (d (n "lucchetto-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "rb-sys") (r "^0") (d #t) (k 0)))) (h "13042j5ls7yd5j3kabl5al3b08ix5l8sgg956smp1r6xzz5ysz7w")))

(define-public crate-lucchetto-0.2.0 (c (n "lucchetto") (v "0.2.0") (d (list (d (n "lucchetto-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "rb-sys") (r "^0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "00184f06lcgjsjf0j6fqbcdmvxknnw2843v6b49vczdv8c70nd2k")))

(define-public crate-lucchetto-0.3.0 (c (n "lucchetto") (v "0.3.0") (d (list (d (n "lucchetto-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "rb-sys") (r "^0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "194h0g92kn4rv0byadflraadvdnr4yf7p11g8m1198z1hdndsmv9")))

(define-public crate-lucchetto-0.4.0 (c (n "lucchetto") (v "0.4.0") (d (list (d (n "lucchetto-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "rb-sys") (r "^0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0k1vw3k863fv1fxqmw856gpv0s098mg6q8z2kyzzp0b71r5j9kdh")))

