(define-module (crates-io lu cc lucchetto-macros) #:use-module (crates-io))

(define-public crate-lucchetto-macros-0.1.0 (c (n "lucchetto-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1dklcd0awhirihdjjiizxrbssmmc3h4jdzca8nglj909r2f5jai9")))

(define-public crate-lucchetto-macros-0.2.0 (c (n "lucchetto-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1cv0san07d2i7zdlv53drrvmydbhgj2gq6ak75xl9ncwj1qh5h6s")))

