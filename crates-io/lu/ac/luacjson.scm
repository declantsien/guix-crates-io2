(define-module (crates-io lu ac luacjson) #:use-module (crates-io))

(define-public crate-luacjson-0.1.0 (c (n "luacjson") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "td_rlua") (r "^0.2.2") (d #t) (k 0)))) (h "0fidn4l8dxrhd1jnsncdlaii88znsj5ym0max3jmvqg2qlss0bp9") (l "luacjson")))

(define-public crate-luacjson-0.1.2 (c (n "luacjson") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "td_rlua") (r "^0.2.2") (d #t) (k 0)))) (h "00c0pb466njrn7jrsr0vv3728h8n85g93fnh60fvcbg3wnf03pr1") (l "luacjson")))

(define-public crate-luacjson-0.1.3 (c (n "luacjson") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "td_rlua") (r "^0.3.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "181qlgpb4vs7sf84vnkg97nkyc11a0i9k1694s49i4flcs5l2c3g") (l "luacjson")))

