(define-module (crates-io lu as luasocket) #:use-module (crates-io))

(define-public crate-luasocket-0.1.3 (c (n "luasocket") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "td_rlua") (r "^0.2.2") (d #t) (k 0)))) (h "02qf8wpr4s1k549gdglb24dfpda0jk8fg8w0w9kk7yhd0bmnb931") (l "luasocket")))

(define-public crate-luasocket-0.1.4 (c (n "luasocket") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "td_rlua") (r "^0.2.2") (d #t) (k 0)))) (h "1x2a0bchpzz1qq30jr8z2jlv9ynd3610dg992q5kr5y74c8p6ffb") (l "luasocket")))

(define-public crate-luasocket-0.1.5 (c (n "luasocket") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "td_rlua") (r "^0.2.2") (d #t) (k 0)))) (h "0krvvq762ziqadvh1ln3sab6zzm8xhr6mb83nhg943sa00dglrzh") (l "luasocket")))

(define-public crate-luasocket-0.1.6 (c (n "luasocket") (v "0.1.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "td_rlua") (r "^0.2.2") (d #t) (k 0)))) (h "0rcs4raj4lq5g0zjsflalimkqdnfk6g8ifzbsfxqa0iy018802wh") (l "luasocket")))

(define-public crate-luasocket-0.1.7 (c (n "luasocket") (v "0.1.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "td_rlua") (r "^0.2.2") (d #t) (k 0)))) (h "0yvywy3c8g24cd2gmilpi8a7lcwkbr38q4z83lkj3rldqzy018kd") (l "luasocket")))

(define-public crate-luasocket-0.1.8 (c (n "luasocket") (v "0.1.8") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "td_rlua") (r "^0.2.2") (d #t) (k 0)))) (h "0k812ykccxa4zkq1mc0n5024grjhr5gihpnx6l5ccvgyk4swxi19") (l "luasocket")))

(define-public crate-luasocket-0.1.9 (c (n "luasocket") (v "0.1.9") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "td_rlua") (r "^0.2.2") (d #t) (k 0)))) (h "110dh17vcpr180ni4d50d2m7ylysfmc4230890q3zk7xn491zicx") (l "luasocket")))

(define-public crate-luasocket-0.1.10 (c (n "luasocket") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "td_rlua") (r "^0.3.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dqrpfwnx0pydb8gp23m8jzpwhbdl18f7sqrj6dk1s4liwq1xbvp") (l "luasocket")))

