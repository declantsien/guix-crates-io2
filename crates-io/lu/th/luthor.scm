(define-module (crates-io lu th luthor) #:use-module (crates-io))

(define-public crate-luthor-0.0.1 (c (n "luthor") (v "0.0.1") (h "11b5k02j53wcpi21kl2yq41sh1dydgqg6b7mh9h16vscwrqp1yrg")))

(define-public crate-luthor-0.0.2 (c (n "luthor") (v "0.0.2") (h "04m6z56qix77xl8c556f1n5x4qnl0xlxaa26bkns6cxjdhlb4xwi")))

(define-public crate-luthor-0.0.3 (c (n "luthor") (v "0.0.3") (h "00cllym0vajgh399ixs39f649gkcbjgmgz0z0b6g2xfj16kjsq63")))

(define-public crate-luthor-0.0.4 (c (n "luthor") (v "0.0.4") (h "185daim0hyplj7f2zdkl40mz8nf04vfmgh2z90iw8h65pjh6ff6r")))

(define-public crate-luthor-0.0.5 (c (n "luthor") (v "0.0.5") (h "0f0hq221d8b5hca9n1s6mdfh12bgp7pnn3gbvymr06f2hxk9ahbv")))

(define-public crate-luthor-0.1.1 (c (n "luthor") (v "0.1.1") (h "1dydpmxl92jpa2bdh6hddwpzcxjba8n77bvgyzbq71nvy8aafcrh")))

(define-public crate-luthor-0.1.2 (c (n "luthor") (v "0.1.2") (h "1bzciphafcdrxx12y3gdvn9phxn7sb28vh6i9mp747yinh9wad3s")))

(define-public crate-luthor-0.1.3 (c (n "luthor") (v "0.1.3") (h "0ra0ajsj9caciizh43s95jk19xhbv2z0yv35w4qsn5c166i500ns")))

(define-public crate-luthor-0.1.4 (c (n "luthor") (v "0.1.4") (h "1w7ybnq2k3b0rvrqp1wdp820hqwvar4f8c5g4vq5siq6jkzszld8")))

(define-public crate-luthor-0.1.5 (c (n "luthor") (v "0.1.5") (h "08fj5ajbh7svkhwaqqfahav53ixc523lidxj8gnmzcmbqc8ldaqv")))

(define-public crate-luthor-0.1.6 (c (n "luthor") (v "0.1.6") (h "0qfkwkprz242f6dn4yajwxvm37bgjfakg82l805s3hsbadj2m2js")))

(define-public crate-luthor-0.1.7 (c (n "luthor") (v "0.1.7") (h "0pk6mwzyfzp8hbgpjln3j0hh1ci6i18smr63pqwijiwh3kk65m87")))

(define-public crate-luthor-0.2.0 (c (n "luthor") (v "0.2.0") (h "0cr320xbvp2p2sdvdh6kn2aalvljyiir9pwlpn0sh6s0hy9gsdgd")))

