(define-module (crates-io lu th luther) #:use-module (crates-io))

(define-public crate-luther-0.1.0 (c (n "luther") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.1.0") (d #t) (k 2)) (d (n "encode_unicode") (r "^0.3.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6.0") (d #t) (k 2)) (d (n "regex") (r "^0.2.5") (d #t) (k 2)))) (h "0360k7g762i917xv9jlfbgfhcr37vm31bqsqhjb4f8pbs2iwxx06")))

