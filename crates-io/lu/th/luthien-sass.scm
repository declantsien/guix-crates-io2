(define-module (crates-io lu th luthien-sass) #:use-module (crates-io))

(define-public crate-luthien-sass-0.1.0 (c (n "luthien-sass") (v "0.1.0") (d (list (d (n "askama") (r "^0.10") (d #t) (k 0)) (d (n "grass") (r "^0.10") (d #t) (k 0)) (d (n "luthien-plugin") (r "^0.1") (d #t) (k 0)))) (h "0d8z90264pvsf6qs3nxp6h89fiylhxmg23sb021z4v3yhn91dib2")))

