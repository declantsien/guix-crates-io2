(define-module (crates-io lu th luthien-plugin) #:use-module (crates-io))

(define-public crate-luthien-plugin-0.1.0 (c (n "luthien-plugin") (v "0.1.0") (d (list (d (n "ipipe") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "palette") (r "^0.5") (f (quote ("serializing"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07rl09ywqcnpdxsbz05zwf8j20669mziyiibnj89klix0zw9yaql") (f (quote (("io" "ipipe") ("default" "io"))))))

