(define-module (crates-io lu th luther-derive) #:use-module (crates-io))

(define-public crate-luther-derive-0.1.0 (c (n "luther-derive") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.1.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 2)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "luther") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "redfa") (r "^0.0.2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("visit"))) (d #t) (k 0)))) (h "0qkcb7rrnrfl345hwrr995kr7q1g9686xmkh73d2103jc2963rk4")))

