(define-module (crates-io lu th luthien-templates) #:use-module (crates-io))

(define-public crate-luthien-templates-0.1.0 (c (n "luthien-templates") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "handlebars") (r "^3.5") (d #t) (k 0)) (d (n "luthien-plugin") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0drxi03m71sdarz0ifpzcnl62f8pmsw7pv7ln9rispcwb533a9m7")))

