(define-module (crates-io lu du ludus) #:use-module (crates-io))

(define-public crate-ludus-0.1.0 (c (n "ludus") (v "0.1.0") (h "047pqw8qs4d2kj8xf5hcmrrndi0s0g477k6lf63118y2ljz2rjh2")))

(define-public crate-ludus-0.2.0 (c (n "ludus") (v "0.2.0") (h "097zh0phhhkxp6syk878qrpxli75zcpz2m8aa783xld129gnlwb6")))

(define-public crate-ludus-0.2.1 (c (n "ludus") (v "0.2.1") (h "11m96ll5vbsqds33cm9wba2kjslhqskwr3j0js2v6903p67335mz")))

(define-public crate-ludus-0.2.2 (c (n "ludus") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0g7kycxzghbxdnz191i239kmc5bzivwjna98d7wfvs5m50qfrxvn")))

