(define-module (crates-io lu xo luxon) #:use-module (crates-io))

(define-public crate-luxon-0.1.0 (c (n "luxon") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "gix") (r "^0.52.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.3.7") (d #t) (k 0)) (d (n "miette") (r "^5.10.0") (f (quote ("fancy"))) (d #t) (k 0)))) (h "1kqfnl8c62wvk2qwymfn9hr5r6f0p0asfx442akq0lgx8ym8yiaq")))

