(define-module (crates-io lu we luwen-ref) #:use-module (crates-io))

(define-public crate-luwen-ref-0.3.1 (c (n "luwen-ref") (v "0.3.1") (d (list (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "luwen-core") (r "^0.1.0") (d #t) (k 0)) (d (n "luwen-if") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "ttkmd-if") (r "^0.1.0") (d #t) (k 0)))) (h "1g9w9iqj4h63aadzwn48flbrbqwjc0blk6i17g06vgxaczhywh4r")))

