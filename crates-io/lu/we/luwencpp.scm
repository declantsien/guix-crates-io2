(define-module (crates-io lu we luwencpp) #:use-module (crates-io))

(define-public crate-luwencpp-0.2.0 (c (n "luwencpp") (v "0.2.0") (d (list (d (n "cbindgen") (r "^0.25.0") (d #t) (k 1)) (d (n "luwen-core") (r "^0.1.0") (d #t) (k 0)) (d (n "luwen-if") (r "^0.4.4") (d #t) (k 0)))) (h "0m1xyivx48yxmyd807m38vbfinjkpx652y59y5arz2rbhq49szsi")))

