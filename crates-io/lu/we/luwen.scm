(define-module (crates-io lu we luwen) #:use-module (crates-io))

(define-public crate-luwen-0.3.6 (c (n "luwen") (v "0.3.6") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "luwen-core") (r "^0.1.0") (d #t) (k 0)) (d (n "luwen-if") (r "^0.4.4") (d #t) (k 0)) (d (n "luwen-ref") (r "^0.3.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)) (d (n "ttkmd-if") (r "^0.1.0") (d #t) (k 0)))) (h "1fcnfl39pl6lj09ba9hq8c4795fbvblj368w2vmayfsswif6i0q9")))

