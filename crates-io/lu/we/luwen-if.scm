(define-module (crates-io lu we luwen-if) #:use-module (crates-io))

(define-public crate-luwen-if-0.4.4 (c (n "luwen-if") (v "0.4.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "luwen-core") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1zk3qza0a73jndzdwcsvkccaf6zlc5cfd7bv479f1a7bpr2yad84")))

