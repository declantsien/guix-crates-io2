(define-module (crates-io lu de lude) #:use-module (crates-io))

(define-public crate-lude-0.1.0 (c (n "lude") (v "0.1.0") (d (list (d (n "containers") (r "^0.7") (k 0)) (d (n "loca") (r "^0.5") (d #t) (k 0)) (d (n "siphasher") (r "^0.2") (d #t) (k 0)))) (h "16dgl2rai2i1mnvy8qgxscwvgp4g080b4l5nnl7758l5ld9flhrb")))

(define-public crate-lude-0.0.0 (c (n "lude") (v "0.0.0") (h "1ix7zgbcd4m0ij2zpw6wgl98svqc2p2d03w6zyjz36xq9sj77cj0")))

