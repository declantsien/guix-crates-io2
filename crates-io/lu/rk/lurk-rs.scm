(define-module (crates-io lu rk lurk-rs) #:use-module (crates-io))

(define-public crate-lurk-rs-0.1.0 (c (n "lurk-rs") (v "0.1.0") (d (list (d (n "bellperson") (r "^0.14") (k 0)) (d (n "ff") (r "^0.3.1") (d #t) (k 0) (p "fff")) (d (n "neptune") (r "^3.0") (k 0)))) (h "0wprq26g9gjlqigffcpmqdjjajanzl8fim8292rzpqa0czk8wcar") (f (quote (("pairing" "bellperson/pairing" "neptune/pairing" "bellperson/pairing") ("gpu" "bellperson/gpu" "neptune/opencl") ("default" "gpu" "pairing") ("blst" "bellperson/blst" "neptune/blst" "bellperson/blst"))))))

