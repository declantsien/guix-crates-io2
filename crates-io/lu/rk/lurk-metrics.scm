(define-module (crates-io lu rk lurk-metrics) #:use-module (crates-io))

(define-public crate-lurk-metrics-0.2.0 (c (n "lurk-metrics") (v "0.2.0") (d (list (d (n "hdrhistogram") (r "^7.5.2") (k 0)) (d (n "metrics") (r "^0.21.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (f (quote ("unicode-case"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2") (f (quote ("no-env-filter"))) (d #t) (k 2)))) (h "0bsqkrmxx2w6rfmlqhwnx6s4y28f58crqri613sp4grj5xnp0z5a")))

