(define-module (crates-io lu rk lurk-macros) #:use-module (crates-io))

(define-public crate-lurk-macros-0.2.0 (c (n "lurk-macros") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "pasta_curves") (r "^0.5.1") (f (quote ("repr-c" "serde"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proptest") (r "^1.2.0") (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("derive" "extra-traits" "full"))) (d #t) (k 0)))) (h "1h6nri1iz7rl3l8ilfw40pwmz3jshh2qh9gl8fvdgrhcafrbxdpj")))

