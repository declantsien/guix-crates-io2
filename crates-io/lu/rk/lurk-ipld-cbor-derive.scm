(define-module (crates-io lu rk lurk-ipld-cbor-derive) #:use-module (crates-io))

(define-public crate-lurk-ipld-cbor-derive-0.1.0 (c (n "lurk-ipld-cbor-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1j6q0jnl1g3af0p3jvy8k0bmc0k11b9z9f619naibwvzrvls4yc3")))

(define-public crate-lurk-ipld-cbor-derive-0.2.0 (c (n "lurk-ipld-cbor-derive") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1mn9flkafy4rfljancl4ckh5sn5ivak9c8byg16ys2xbmbbr6gcp")))

(define-public crate-lurk-ipld-cbor-derive-0.3.0 (c (n "lurk-ipld-cbor-derive") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1pbbgw08g05skirjngj21m7jwdyiwb3wh0fv924y50khwk2wadk3")))

