(define-module (crates-io lu rk lurk-ipld-macro) #:use-module (crates-io))

(define-public crate-lurk-ipld-macro-0.1.0 (c (n "lurk-ipld-macro") (v "0.1.0") (d (list (d (n "lurk-ipld-core") (r "^0.1.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (f (quote ("blake3"))) (d #t) (k 2)))) (h "0wh2ijj0f9r0snh8zf4gffwnh9kkwhiagsv06802hbviyhw7ay8v")))

(define-public crate-lurk-ipld-macro-0.2.0 (c (n "lurk-ipld-macro") (v "0.2.0") (d (list (d (n "lurk-ipld-core") (r "^0.2.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (f (quote ("blake3"))) (d #t) (k 2)))) (h "03l0j6az4v6k7m21gjjjnf11id7azfjazd0a96d5jfm5r3d3n61v")))

(define-public crate-lurk-ipld-macro-0.3.0 (c (n "lurk-ipld-macro") (v "0.3.0") (d (list (d (n "lurk-ipld-core") (r "^0.3.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (f (quote ("blake3"))) (d #t) (k 2)))) (h "0jpzdl0ycq2rqiww32inzfi20d4m7i4kdj7pggzz17pdxig0ma6x")))

