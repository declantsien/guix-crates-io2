(define-module (crates-io lu rk lurk-elsa) #:use-module (crates-io))

(define-public crate-lurk-elsa-0.1.0 (c (n "lurk-elsa") (v "0.1.0") (d (list (d (n "indexmap") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "07zzmyqj15jlwf0v1hl74i0lg9c7ddnvryvk3pfsqks0ipy36wjc") (f (quote (("default")))) (s 2) (e (quote (("indexmap" "dep:indexmap"))))))

(define-public crate-lurk-elsa-0.1.1 (c (n "lurk-elsa") (v "0.1.1") (d (list (d (n "indexmap") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1w4vwv8f4qyi8cd160j2cgbfdfv1nnjxhi0cmw63bba63f96qmcm") (f (quote (("default")))) (s 2) (e (quote (("indexmap" "dep:indexmap"))))))

