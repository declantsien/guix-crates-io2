(define-module (crates-io lu rk lurk-ipld-json) #:use-module (crates-io))

(define-public crate-lurk-ipld-json-0.1.0 (c (n "lurk-ipld-json") (v "0.1.0") (d (list (d (n "lurk-ipld-core") (r "^0.1.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("float_roundtrip"))) (d #t) (k 0)))) (h "0526c5nd89lvdymw01c7y5lr0kqis23l0h11mgmb42kin4180m6v")))

(define-public crate-lurk-ipld-json-0.2.0 (c (n "lurk-ipld-json") (v "0.2.0") (d (list (d (n "lurk-ipld-core") (r "^0.2.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("float_roundtrip"))) (d #t) (k 0)))) (h "08q0s95c84awpbvw6pql2agqhklzzrligansn4mfl5kv6mn69akq")))

(define-public crate-lurk-ipld-json-0.3.0 (c (n "lurk-ipld-json") (v "0.3.0") (d (list (d (n "lurk-ipld-core") (r "^0.3.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("float_roundtrip"))) (d #t) (k 0)))) (h "1rnrd0fja9jhjkbd4swdcvvwwfvg3dbgb6z6r8vj6w4fgfxgsiri")))

