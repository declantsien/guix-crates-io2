(define-module (crates-io lu rk lurk-pasta-msm) #:use-module (crates-io))

(define-public crate-lurk-pasta-msm-0.1.0 (c (n "lurk-pasta-msm") (v "0.1.0") (d (list (d (n "pasta_curves") (r "^0.5.2") (f (quote ("repr-c"))) (d #t) (k 0) (p "fil_pasta_curves")) (d (n "semolina") (r "~0.1.2") (d #t) (k 0)) (d (n "sppark") (r "~0.1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "which") (r "^4.0") (d #t) (k 1)))) (h "0v3f96w1nvr8kc3xyxac154ms919c118qc4i8pw4yaia5602568j") (f (quote (("portable" "semolina/portable") ("force-adx" "semolina/force-adx") ("default") ("cuda-mobile"))))))

