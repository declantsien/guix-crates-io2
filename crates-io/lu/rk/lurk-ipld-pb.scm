(define-module (crates-io lu rk lurk-ipld-pb) #:use-module (crates-io))

(define-public crate-lurk-ipld-pb-0.1.0 (c (n "lurk-ipld-pb") (v "0.1.0") (d (list (d (n "lurk-ipld-core") (r "^0.1.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (d #t) (k 2)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0jvq97hpqa79a2ik6sy9xfyfqcnmiqmik49ix2k8c08kc59c4a9x")))

(define-public crate-lurk-ipld-pb-0.2.0 (c (n "lurk-ipld-pb") (v "0.2.0") (d (list (d (n "lurk-ipld-core") (r "^0.2.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (d #t) (k 2)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0li0qhmv7mcgsy3hb016n27mjmwhwyvd6ibb45n9gknwhra1yblr")))

(define-public crate-lurk-ipld-pb-0.3.0 (c (n "lurk-ipld-pb") (v "0.3.0") (d (list (d (n "lurk-ipld-core") (r "^0.3.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (d #t) (k 2)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0lgpyqhxi623xc13yrrzl9y5528qwzvi1gwf2qnpn2qs7gdfc3hk")))

