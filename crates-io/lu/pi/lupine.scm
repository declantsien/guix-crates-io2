(define-module (crates-io lu pi lupine) #:use-module (crates-io))

(define-public crate-lupine-0.2.0 (c (n "lupine") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "0gham21gq9wv5m9fc0x8nx00rafgrnz3zna4vpxgbxxd2l6yysp3")))

(define-public crate-lupine-0.2.1 (c (n "lupine") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "08g9waq2681mnds8y4yqafyr6wzpx2s9nvckfygkm0pm0cf06z7b")))

(define-public crate-lupine-0.2.2 (c (n "lupine") (v "0.2.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "0frisg93rimv2386izkwzxi90a8v0hxprzcw3pi4lczgrxhrngz6")))

(define-public crate-lupine-0.3.0 (c (n "lupine") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "0vg3fnw54j3p9mr9j50gd9fcm10a9hsna38m1954gynls136jrm4")))

(define-public crate-lupine-0.3.1 (c (n "lupine") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "0brdlfvhvqg9yhnjpaq42bnpfh2n3w7s14jxypnxz2rbrl8lbxxc")))

(define-public crate-lupine-0.3.2 (c (n "lupine") (v "0.3.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "04qsndiy4cqvzyyrs0i7b6rr7ijyzis6fpdd1whk5ykjlxnap5ra")))

