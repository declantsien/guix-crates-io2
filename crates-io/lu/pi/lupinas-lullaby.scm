(define-module (crates-io lu pi lupinas-lullaby) #:use-module (crates-io))

(define-public crate-lupinas-lullaby-0.1.0 (c (n "lupinas-lullaby") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.15") (d #t) (k 0)))) (h "0lpxymxzs6p0anyr4k5s6j5rk2hjaxk22kkwvl2pf93ba85h9fii")))

(define-public crate-lupinas-lullaby-0.1.1 (c (n "lupinas-lullaby") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.15") (d #t) (k 0)))) (h "1xfbgnw2ssj8i58g00w10g5d5v853phz3npkf0qdib71v69fh95s")))

(define-public crate-lupinas-lullaby-0.1.2 (c (n "lupinas-lullaby") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.15") (d #t) (k 0)))) (h "1c8xrr9qx3iy1dfb2q8b09rygdq2wmgs97sx6g37s2wvzy07ilw9")))

(define-public crate-lupinas-lullaby-0.1.3 (c (n "lupinas-lullaby") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.15") (d #t) (k 0)))) (h "0hmbxiac91i2pi377j1qmcjk989cnzh1mclhlssbbw7mgnrgdj5w")))

(define-public crate-lupinas-lullaby-0.1.4 (c (n "lupinas-lullaby") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.15") (d #t) (k 0)))) (h "02ybm3qijf7rrcgmp2vzd3l0gsh3y5i5f9w8b7bkwvv5zaw7l23a")))

(define-public crate-lupinas-lullaby-0.1.5 (c (n "lupinas-lullaby") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.15") (d #t) (k 0)))) (h "1pp2sljl26brq8ppjfpmg7gswn770jcymffwxw709j7b81hr5rl3")))

(define-public crate-lupinas-lullaby-0.1.7 (c (n "lupinas-lullaby") (v "0.1.7") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.15") (d #t) (k 0)))) (h "0z4wz4m9k0qx1h8i72v9717r2808xmyhzbyl49zyqcrz5lgvl308")))

(define-public crate-lupinas-lullaby-0.2.0 (c (n "lupinas-lullaby") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.15") (d #t) (k 0)))) (h "1i0yppl09mnj4h2nrr9fsvisbq5m9lbbb058vx56zs5qvzmy8bcn")))

(define-public crate-lupinas-lullaby-0.3.0 (c (n "lupinas-lullaby") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.15") (d #t) (k 0)))) (h "0w5l88g5lssrzf40vaid96q2pshbw7zkll42zy5wsjbr8alvhv3d")))

(define-public crate-lupinas-lullaby-0.3.2 (c (n "lupinas-lullaby") (v "0.3.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "15a2j7xac2viafsifhxqap89y1jahq017fcmc46733vlc9fyrll9")))

(define-public crate-lupinas-lullaby-0.3.3 (c (n "lupinas-lullaby") (v "0.3.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "07bgcwc2ixjzvhx7f47vvfhs6rkzhrwnx8jp4v7k6wfclzcdxyxz")))

