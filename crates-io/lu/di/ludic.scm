(define-module (crates-io lu di ludic) #:use-module (crates-io))

(define-public crate-ludic-0.1.0 (c (n "ludic") (v "0.1.0") (d (list (d (n "either") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "faber") (r "^0.1.0") (d #t) (k 0)) (d (n "forgic") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libic") (r "^0.1.0") (d #t) (k 0)) (d (n "quantum-mc") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "vanel") (r "^0.1.0") (d #t) (k 0)))) (h "0wvkhn7ha0zp84762q8rnljmhbsl1m9niv0y1xmagqcfsnxypr3i")))

