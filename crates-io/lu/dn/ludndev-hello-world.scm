(define-module (crates-io lu dn ludndev-hello-world) #:use-module (crates-io))

(define-public crate-ludndev-hello-world-0.1.0 (c (n "ludndev-hello-world") (v "0.1.0") (h "04k8vbqz5xnc1xm4ai6y68ddp856ssjil29bmrc5qps0ryy6dg70")))

(define-public crate-ludndev-hello-world-0.1.1 (c (n "ludndev-hello-world") (v "0.1.1") (h "1zrbjz5kqd73n40anpbahgx26ygh7kcqy80aaaqnhiw38m8341p3")))

