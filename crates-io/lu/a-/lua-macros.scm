(define-module (crates-io lu a- lua-macros) #:use-module (crates-io))

(define-public crate-lua-macros-0.1.8 (c (n "lua-macros") (v "0.1.8") (d (list (d (n "lua") (r "^0.0.10") (d #t) (k 0)))) (h "0fbq0svq4v9xad0w84hc4m3kygyslbn8735mn59q84k92adi3iv9")))

(define-public crate-lua-macros-0.1.9 (c (n "lua-macros") (v "0.1.9") (d (list (d (n "lua") (r "^0.0.10") (d #t) (k 0)))) (h "17yi50fwfw3y8zzv2nfh6fb755s27dl0xyfzv8kpziav3d2p5jz0")))

