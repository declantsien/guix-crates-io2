(define-module (crates-io lu a- lua-rs) #:use-module (crates-io))

(define-public crate-lua-rs-0.0.1 (c (n "lua-rs") (v "0.0.1") (h "0bk00jjcz7g1q750wn3ny0mx68i2qspqvl7f1j5d3fch09pq33xw") (y #t)))

(define-public crate-lua-rs-0.0.2 (c (n "lua-rs") (v "0.0.2") (h "0dwbxzg2gd4pf9zm6x9hjsvqslmsr30gldjib8bfb5jp7n4vm41y") (y #t)))

(define-public crate-lua-rs-0.0.3 (c (n "lua-rs") (v "0.0.3") (h "0zwb4fiqzpj0cmcyl3rcyq8zvdvy1zaf40mzspkwmv7aych3svyd") (y #t)))

(define-public crate-lua-rs-0.0.4 (c (n "lua-rs") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "04fxd379fqbvrndh208i4ahasxbcmv97dnsz6qlvbr6q82brbzyi") (y #t)))

(define-public crate-lua-rs-0.0.5 (c (n "lua-rs") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1lbf2kxnq5s0cklvccr2q6gjax0dc3a9la47anww12hkxwcj5qc9")))

(define-public crate-lua-rs-0.0.6 (c (n "lua-rs") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "1ba1bph7bzgp93y9ljf5akv8x4l1gxa5prsk5q74jgksam28qzk7")))

(define-public crate-lua-rs-0.0.7 (c (n "lua-rs") (v "0.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "12k70n1b5njriszv6whmgidhqnyaazqn1kyvc3j34hxfn1wjc251")))

(define-public crate-lua-rs-0.0.8 (c (n "lua-rs") (v "0.0.8") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "0srq8rk8dmgnzp1wszz07kl1w49hg3niffa2pw3xqw4a5g50nr23")))

(define-public crate-lua-rs-0.0.9 (c (n "lua-rs") (v "0.0.9") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "version-sync") (r "^0.5.0") (d #t) (k 2)))) (h "1c5fqhxqi7gyznd7v6hmc7aibns6h4ibdrr9bb1bs3i5dvj63zcz")))

(define-public crate-lua-rs-0.0.10 (c (n "lua-rs") (v "0.0.10") (d (list (d (n "cc") (r "^1.0.34") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "09a15n2inv5r94jbgqscjsiw4fm49pdn7k2nslfcy0rlmkcp28f7")))

(define-public crate-lua-rs-0.0.11 (c (n "lua-rs") (v "0.0.11") (d (list (d (n "cc") (r "^1.0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2.71") (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "1dxbrlkcac3nn0y5qvk448jj7aspr8kf5wmx0f8yzazqc29rzfc0")))

