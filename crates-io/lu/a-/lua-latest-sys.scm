(define-module (crates-io lu a- lua-latest-sys) #:use-module (crates-io))

(define-public crate-lua-latest-sys-0.0.0 (c (n "lua-latest-sys") (v "0.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "18p6nplmyd4dix7iappil0pvm1rw0d0wrncy06clmpi2rppajp5b") (l "lua5.4")))

(define-public crate-lua-latest-sys-0.0.1 (c (n "lua-latest-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1k0g035hhmbcspqw1dgah4sakbgp6fr11a40lnyyyn8cglrnm85w") (l "lua5.4")))

(define-public crate-lua-latest-sys-0.0.2 (c (n "lua-latest-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "15bmm9d6bqar92ajsxp54w8bfkc0dcg7s5kdcnlwib1r8i91g8hv") (l "lua5.4")))

