(define-module (crates-io lu a- lua-pattern) #:use-module (crates-io))

(define-public crate-lua-pattern-0.1.0 (c (n "lua-pattern") (v "0.1.0") (d (list (d (n "document-features") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (o #t) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "10pj4548gj8apgvklwzbn2kfk8bpqkxrp7jq65y5y8k2zg6aa5n4") (f (quote (("to-regex") ("default")))) (s 2) (e (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

(define-public crate-lua-pattern-0.1.1 (c (n "lua-pattern") (v "0.1.1") (d (list (d (n "document-features") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (o #t) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0mvci1a07i2jm0sx7h63332fg3f3vdrxik9djkrch0fq0p36vz7n") (f (quote (("to-regex") ("default")))) (s 2) (e (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

(define-public crate-lua-pattern-0.1.2 (c (n "lua-pattern") (v "0.1.2") (d (list (d (n "document-features") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (o #t) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0irmcn6hnqaimxra2k8r56ygn4zpc0p226ak0bbzwwqpfigi9fam") (f (quote (("to-regex") ("default")))) (s 2) (e (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

