(define-module (crates-io lu a- lua-src) #:use-module (crates-io))

(define-public crate-lua-src-535.0.0+540b (c (n "lua-src") (v "535.0.0+540b") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0nr0zml34zqbfr0fwrcgqckvylvg1nj4757li1zij961kxd8xj5a")))

(define-public crate-lua-src-535.0.1+540b (c (n "lua-src") (v "535.0.1+540b") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0qif405xw2wsyw03k0lc8pr74x2jzd4l0aslgd78kb47j1i4yz0d")))

(define-public crate-lua-src-535.0.2+540rc1 (c (n "lua-src") (v "535.0.2+540rc1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1j44dwlhl0a0cjzqp9bvkhhl9mwyaadqjc0i54ynq6qqzjjv823v")))

(define-public crate-lua-src-535.0.3+540rc3 (c (n "lua-src") (v "535.0.3+540rc3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1wkf1k9rx4qjdk9nvksa5ddizaxpa0b93vvrd8n35fvy09ba2w06")))

(define-public crate-lua-src-535.0.4+540rc4 (c (n "lua-src") (v "535.0.4+540rc4") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "05ynlz7mdqx9xzhxigcm03s3qb6pni5lr9avkqqkwixhcxdvyng4")))

(define-public crate-lua-src-540.0.0 (c (n "lua-src") (v "540.0.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "13l2hqzh35n88fcnxkvlm2hwn9wmzrc8wd5sqgjpii2pff5vrh9r")))

(define-public crate-lua-src-540.0.1 (c (n "lua-src") (v "540.0.1") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "17znw1sgvhh5ld4m1cifc6sd8jan44cgk0wc81rzkkwl6z8araz3")))

(define-public crate-lua-src-541.0.0 (c (n "lua-src") (v "541.0.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "1gwcl8w46ax23jrqh3ay3yyhvqi271y579m4xwrscd7lgm1cvj78")))

(define-public crate-lua-src-542.0.0 (c (n "lua-src") (v "542.0.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "1d6r22l7bxx572rx703w23vrskxq5500w23567ysaczj2gm6d22c")))

(define-public crate-lua-src-543.0.0 (c (n "lua-src") (v "543.0.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "0zd23idkgvhm3iw2x3kpfg8ggb0nnmljby85v53nzcxrjkwq1482")))

(define-public crate-lua-src-543.1.0 (c (n "lua-src") (v "543.1.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "0iq29k01j975p0nqacpxrjla84czcdfi6adjhl8hxvzi5cri8adp") (f (quote (("ucid"))))))

(define-public crate-lua-src-544.0.0 (c (n "lua-src") (v "544.0.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "12y6by7xhqwpj8g5p94nskpvy1sfyiaiqxi0ra14j73qk81vlhbk") (f (quote (("ucid"))))))

(define-public crate-lua-src-544.0.1 (c (n "lua-src") (v "544.0.1") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "0k4179hlmgms3wkbgi5k37sp1hqp3j3xs2aaxy6x7sfm8k4a72vh") (f (quote (("ucid"))))))

(define-public crate-lua-src-546.0.0 (c (n "lua-src") (v "546.0.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "1kps2frfbn8r6ayr1r9rl9r4i8zz05rw04fjin9b9d7ih09hrc4c") (f (quote (("ucid"))))))

(define-public crate-lua-src-546.0.1 (c (n "lua-src") (v "546.0.1") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "1a6wdx60i3118xwlykaz97vsz4hmrmjbk8h37md047ing2px89kw") (f (quote (("ucid"))))))

(define-public crate-lua-src-546.0.2 (c (n "lua-src") (v "546.0.2") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "1ci9l4nkx6fj6s9vlw9raxpf4rjjyldf6plg1k1s84g6xskxm81d") (f (quote (("ucid"))))))

