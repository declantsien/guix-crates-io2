(define-module (crates-io lu a- lua-macro) #:use-module (crates-io))

(define-public crate-lua-macro-0.1.0 (c (n "lua-macro") (v "0.1.0") (d (list (d (n "lua-macro-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "mlua") (r "^0.7.4") (f (quote ("lua54"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)))) (h "1yp2p88y6yyf0jrkq3rndxjacs8bvmmrxjw4lksd0f0pf10b3s6n")))

(define-public crate-lua-macro-0.1.1 (c (n "lua-macro") (v "0.1.1") (d (list (d (n "lua-macro-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "mlua") (r "^0.7.4") (f (quote ("lua54"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)))) (h "1cf21z572ifcff0x4kyqlx0m0mi4r43k6r2vj99cbg7yx2viy16f")))

