(define-module (crates-io lu a- lua-sys) #:use-module (crates-io))

(define-public crate-lua-sys-0.1.0 (c (n "lua-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.21.0") (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "metadeps") (r "^1.1.0") (d #t) (k 1)))) (h "0d3jm2y8hyl3yciarvjca7g09jb0fqlcsv7zmnvnfcybkns3hf88") (y #t)))

(define-public crate-lua-sys-0.2.0 (c (n "lua-sys") (v "0.2.0") (d (list (d (n "cc") (r "~1.0.46") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (o #t) (d #t) (k 1)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "va_list") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.7") (o #t) (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1sx1pgkaxpj082vyv3r73vfq6dw2vzj4z12cw9dcdgs1109xarlp") (f (quote (("va-list" "va_list") ("system-lua" "pkg-config" "vcpkg") ("std") ("lua-compat") ("embedded-lua" "cc") ("default" "embedded-lua" "va-list" "std")))) (y #t) (l "lua")))

