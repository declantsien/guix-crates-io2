(define-module (crates-io lu a- lua-sql-builder) #:use-module (crates-io))

(define-public crate-lua-sql-builder-0.1.0 (c (n "lua-sql-builder") (v "0.1.0") (h "151avy4xrmyfizvy3nnss339q298h11c8y6ld1lwvcv942h0phsg") (y #t)))

(define-public crate-lua-sql-builder-0.1.1 (c (n "lua-sql-builder") (v "0.1.1") (h "0z0ga46vr899qcpgrn3f88bmg0j6wdz5z8sb895jp1y17zbmk1y9") (y #t)))

(define-public crate-lua-sql-builder-0.1.2 (c (n "lua-sql-builder") (v "0.1.2") (h "06kac65hwk1rgrsnhiin4cxx7ksh8j4915774vmfgpplamx9xvi0") (y #t)))

(define-public crate-lua-sql-builder-0.1.3 (c (n "lua-sql-builder") (v "0.1.3") (h "14ph6lxl5ymnw73b3264shshacb3shc0szrfxz21p2w2wjx5m3c6") (y #t)))

(define-public crate-lua-sql-builder-0.1.4 (c (n "lua-sql-builder") (v "0.1.4") (h "0zx1l6rk1hlpj016j367cdgkpl0l7q26qh0drk3yq64088h39aig") (y #t)))

