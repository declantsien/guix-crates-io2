(define-module (crates-io lu a- lua-kit) #:use-module (crates-io))

(define-public crate-lua-kit-0.1.0 (c (n "lua-kit") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1x7f26h93w0hh8bfiph5w5p1dgz9lzmcn8dm9kxjy2rm8a39628a")))

