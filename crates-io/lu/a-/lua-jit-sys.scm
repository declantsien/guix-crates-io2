(define-module (crates-io lu a- lua-jit-sys) #:use-module (crates-io))

(define-public crate-lua-jit-sys-2.0.50 (c (n "lua-jit-sys") (v "2.0.50") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0wmiwyck18i9ws6xybmc7zhci5fjch1v4jp2r6nm9h3w9wbzaf94") (f (quote (("use_valgrind") ("use_sysmalloc") ("use_gdb_jit") ("use_assert") ("use_apicheck") ("strip") ("sse2") ("sse") ("ps3" "use_sysmalloc" "cellos_lv2") ("optimize_size") ("nummode_2") ("nummode_1") ("force_32_bit") ("extra_warnings") ("enable_lua52_compat") ("disable_jit") ("default" "arch_x64") ("cellos_lv2") ("arch_x86") ("arch_x64") ("arch_powerpc_spe") ("arch_powerpc") ("arch_mipsel") ("arch_mips") ("arch_arm"))))))

