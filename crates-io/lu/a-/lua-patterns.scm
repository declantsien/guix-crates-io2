(define-module (crates-io lu a- lua-patterns) #:use-module (crates-io))

(define-public crate-lua-patterns-0.1.0 (c (n "lua-patterns") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0wpqzdqa0hxdsd2qwkq4b2f87wkmiyj8afy3vbyljz2gjv9xa92k")))

(define-public crate-lua-patterns-0.1.1 (c (n "lua-patterns") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "16x4z3l5bi1cbvvxacwfk6a5bbikwzv8a8lghx45sk41ay6whsid")))

(define-public crate-lua-patterns-0.3.0 (c (n "lua-patterns") (v "0.3.0") (h "05h2jwpbqdb75xg5s457pw0a578h6l2yfcj5fmfq5bx57pp6ph45")))

(define-public crate-lua-patterns-0.4.0 (c (n "lua-patterns") (v "0.4.0") (h "10d2wlih28wbrw47c1ib9b871qjgadfw14z5xynnci0zy07ajg9g")))

