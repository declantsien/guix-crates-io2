(define-module (crates-io lu os luoshu_rust_client) #:use-module (crates-io))

(define-public crate-luoshu_rust_client-0.1.0 (c (n "luoshu_rust_client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "luoshu") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0rszq84484mq7g5zi1i6qd7gvncyw40pjwd86kk41x8n49adrmyf")))

