(define-module (crates-io lu os luos_memory_sandbox) #:use-module (crates-io))

(define-public crate-luos_memory_sandbox-0.0.0 (c (n "luos_memory_sandbox") (v "0.0.0") (h "0irdkgqvcp3bvlf93pv43a4zvzzmm0vm5lfw5knkr4hz51dhghk7")))

(define-public crate-luos_memory_sandbox-0.0.1 (c (n "luos_memory_sandbox") (v "0.0.1") (h "1dkdrwbcq0a5ikx24wpm9zlrzm513mbvabsr85jb2bgmcvq36p30")))

