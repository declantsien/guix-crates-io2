(define-module (crates-io lu os luoshu_sled_storage) #:use-module (crates-io))

(define-public crate-luoshu_sled_storage-0.1.0 (c (n "luoshu_sled_storage") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "luoshu_configuration") (r "^0.1.0") (d #t) (k 2)) (d (n "luoshu_connection") (r "^0.1.0") (d #t) (k 2)) (d (n "luoshu_core") (r "^0.1.0") (d #t) (k 0)) (d (n "luoshu_namespace") (r "^0.1.0") (d #t) (k 2)) (d (n "luoshu_registry") (r "^0.1.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0a4darz6lal1awlvgg669h94a4c9xy0ifszf0r1fqf4vl6hfb7q9")))

