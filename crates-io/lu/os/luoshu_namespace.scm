(define-module (crates-io lu os luoshu_namespace) #:use-module (crates-io))

(define-public crate-luoshu_namespace-0.1.0 (c (n "luoshu_namespace") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "luoshu_core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nl8yymy3nnnlcx8cd4c3zxhjmavfkipf3rwlc2c5c7hbq1mqd8y")))

