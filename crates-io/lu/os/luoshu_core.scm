(define-module (crates-io lu os luoshu_core) #:use-module (crates-io))

(define-public crate-luoshu_core-0.1.0 (c (n "luoshu_core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "013wkwfsbi8w6kza7dqdz02x1d0v7mlzpjxwvh6vsjd25i2jyif9")))

