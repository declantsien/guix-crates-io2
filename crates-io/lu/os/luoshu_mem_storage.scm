(define-module (crates-io lu os luoshu_mem_storage) #:use-module (crates-io))

(define-public crate-luoshu_mem_storage-0.1.0 (c (n "luoshu_mem_storage") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "luoshu_core") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)))) (h "19rhkkxw59pmkpnj2bs52lgq7i69a7gf6iwaa7yawwi6pv8d3w4z")))

