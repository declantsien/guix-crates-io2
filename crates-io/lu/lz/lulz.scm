(define-module (crates-io lu lz lulz) #:use-module (crates-io))

(define-public crate-lulz-0.1.0 (c (n "lulz") (v "0.1.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mlua") (r "^0.6") (f (quote ("luajit"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.1") (f (quote ("short-space-opt"))) (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.4.0") (d #t) (k 0)))) (h "111v7xkxhxa0ix35izd4q0q8ci47gccf75zj76px7drlscsqkfz4")))

(define-public crate-lulz-0.1.1 (c (n "lulz") (v "0.1.1") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mlua") (r "^0.6") (f (quote ("luajit"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.1") (f (quote ("short-space-opt"))) (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.4.0") (d #t) (k 0)))) (h "08d3qxh2qdb2z2hnghqd6qsh83pp89q875iqj555g529lbahicls")))

