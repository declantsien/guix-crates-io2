(define-module (crates-io lu na lunatic-metrics-api) #:use-module (crates-io))

(define-public crate-lunatic-metrics-api-0.13.2 (c (n "lunatic-metrics-api") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lunatic-common-api") (r "^0.13") (d #t) (k 0)) (d (n "metrics") (r "^0.20.1") (d #t) (k 0)) (d (n "wasmtime") (r "^8") (d #t) (k 0)))) (h "0zr4yskwkm59w2ymdv194cf3fshf8f07hzmbbnn10gaf5mkglvvh")))

