(define-module (crates-io lu na lunaria-api) #:use-module (crates-io))

(define-public crate-lunaria-api-0.0.0 (c (n "lunaria-api") (v "0.0.0") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3.1") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.1") (d #t) (k 1)))) (h "1rb27aj3pbk6pmgvqhq66fmp7cx0kglxbwz6gdkl2xza0jqnzpg0")))

(define-public crate-lunaria-api-0.1.0 (c (n "lunaria-api") (v "0.1.0") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)) (d (n "tonic") (r "^0.3.1") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.1") (d #t) (k 1)))) (h "1cyasppxq8kgqy2jwi3z0hjq8cc8wisb4qlkj5drv1nlvmngaiz0") (f (quote (("server"))))))

(define-public crate-lunaria-api-0.1.1 (c (n "lunaria-api") (v "0.1.1") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tokio") (r "^0.3.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-compat-02") (r "^0.1.1") (d #t) (k 2)) (d (n "tonic") (r "^0.3.1") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.1") (d #t) (k 1)))) (h "06wz0y77hsnjgn6n6v19xwc5p8lq5fg7j2z54nabprrkwabrh5dr") (f (quote (("server"))))))

(define-public crate-lunaria-api-0.1.2 (c (n "lunaria-api") (v "0.1.2") (d (list (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.4.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.4.2") (d #t) (k 1)))) (h "0yv8qi661140il3whi8ydah3bp7p77z2cabci493pf5n4zqhr65m") (f (quote (("server"))))))

(define-public crate-lunaria-api-0.1.3 (c (n "lunaria-api") (v "0.1.3") (d (list (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.5.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5.0") (d #t) (k 1)))) (h "000yb6h7508l2w44kx0wkaw1xc06255w65aywkm1pjfy4v65gsp5") (f (quote (("server"))))))

(define-public crate-lunaria-api-0.2.0 (c (n "lunaria-api") (v "0.2.0") (d (list (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.5.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5.0") (d #t) (k 1)))) (h "179ca2flxmas5dn0qbmlcgd3w0cfmkan90bq261cpsr9k435mr37") (f (quote (("server"))))))

(define-public crate-lunaria-api-0.2.1 (c (n "lunaria-api") (v "0.2.1") (d (list (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.5.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5.0") (d #t) (k 1)))) (h "12klvkvjzhdqrfv6l8s7lyqnji9fndmv9dm881dzy7cd8mnwikx5") (f (quote (("server"))))))

