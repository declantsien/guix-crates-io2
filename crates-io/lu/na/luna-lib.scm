(define-module (crates-io lu na luna-lib) #:use-module (crates-io))

(define-public crate-luna-lib-0.4.0 (c (n "luna-lib") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1366wdd25m0gmvl32fz14mzbxwa6g5k3cx4vr5ch43knv83rks1c")))

(define-public crate-luna-lib-0.4.1 (c (n "luna-lib") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19ap2cqjkaqsnbvxvwmga0dbnzbkplf9hrljz5bwv3ys995ipkk7")))

(define-public crate-luna-lib-1.1.0 (c (n "luna-lib") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0lcvgzxnk23zi68ydq384p11b5f29hjid624c37g6xhnf2cpn0mf")))

(define-public crate-luna-lib-1.1.1 (c (n "luna-lib") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1y8y8bpkari7k4ahvhh860giv21cfd8xmj40748x8hksjkg5bnjr")))

(define-public crate-luna-lib-1.1.2 (c (n "luna-lib") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nqy30kfsd8x6m915d79g9x864kgaqasvm7xfgavxsghsqy9gb1s")))

(define-public crate-luna-lib-1.2.0 (c (n "luna-lib") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17bq65frbvv3rsydzisilrid2156k49iidrrv07xbr9jn66sjy50")))

(define-public crate-luna-lib-1.3.0 (c (n "luna-lib") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0261g3pw2w9gr9k01a2yfdbwz9rsw5lizplx699rsb8gi7fz3ds4")))

