(define-module (crates-io lu na lunatic_tasks) #:use-module (crates-io))

(define-public crate-lunatic_tasks-0.1.0 (c (n "lunatic_tasks") (v "0.1.0") (d (list (d (n "lunatic") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1gqd24y6ln44k33a6pxjbkbhycr0c427pz37xz8w8sckimb5n271")))

(define-public crate-lunatic_tasks-0.1.1 (c (n "lunatic_tasks") (v "0.1.1") (d (list (d (n "lunatic") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0bz8nrsjyyxh8a5gaamcn6cm4dw4ig96mdidylbbq7clrzgff1nw")))

