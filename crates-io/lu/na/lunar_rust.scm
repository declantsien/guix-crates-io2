(define-module (crates-io lu na lunar_rust) #:use-module (crates-io))

(define-public crate-lunar_rust-1.0.1 (c (n "lunar_rust") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1nmgpgazb436lavhk7m8hk3n3w1cwabh75n7glm05ywyzvjbn3gg")))

