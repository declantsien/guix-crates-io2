(define-module (crates-io lu na lunarity-lexer) #:use-module (crates-io))

(define-public crate-lunarity-lexer-0.1.0 (c (n "lunarity-lexer") (v "0.1.0") (d (list (d (n "toolshed") (r "^0.4") (d #t) (k 0)))) (h "1dpc9jjsgh256iy82glpzbb8bmsvdq76dm0nf5b94mrh3xkp05la")))

(define-public crate-lunarity-lexer-0.2.0 (c (n "lunarity-lexer") (v "0.2.0") (d (list (d (n "logos") (r "^0.6.1") (f (quote ("nul_term_source"))) (d #t) (k 0)))) (h "0xrmy08fkkk3g31p3savkmkxf35gayivhdbdcp7zqnvfrrz9b4zq")))

(define-public crate-lunarity-lexer-0.2.1 (c (n "lunarity-lexer") (v "0.2.1") (d (list (d (n "logos") (r "^0.7.7") (f (quote ("nul_term_source"))) (d #t) (k 0)))) (h "1dykcg5p953i0k27b3vqnr0rggfrhi62qcna5jpd8nzd0dn49998")))

