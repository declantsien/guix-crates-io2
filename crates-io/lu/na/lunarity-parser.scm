(define-module (crates-io lu na lunarity-parser) #:use-module (crates-io))

(define-public crate-lunarity-parser-0.1.0 (c (n "lunarity-parser") (v "0.1.0") (d (list (d (n "lunarity-ast") (r "^0.1") (d #t) (k 0)) (d (n "lunarity-lexer") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4") (d #t) (k 2)) (d (n "toolshed") (r "^0.4") (d #t) (k 0)))) (h "1j56smila7cpplck0paqifwbbgmhab8jxhzrprjwqhpxibvdnp1v")))

(define-public crate-lunarity-parser-0.2.0 (c (n "lunarity-parser") (v "0.2.0") (d (list (d (n "lunarity-ast") (r "^0.2") (d #t) (k 0)) (d (n "lunarity-lexer") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "toolshed") (r "^0.6") (d #t) (k 0)))) (h "1g99ljqykqj6g85fwjcxhgf5m4i210mffffg4mkd4x6p4pmh7nr7")))

(define-public crate-lunarity-parser-0.2.1 (c (n "lunarity-parser") (v "0.2.1") (d (list (d (n "lunarity-ast") (r "^0.2") (d #t) (k 0)) (d (n "lunarity-lexer") (r "^0.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "toolshed") (r "^0.6") (d #t) (k 0)))) (h "131hldf6c76z2w7js58g973ipq7rf4r6gksj5k230v0mba2ikbir")))

