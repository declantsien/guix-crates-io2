(define-module (crates-io lu na lunatic-log) #:use-module (crates-io))

(define-public crate-lunatic-log-0.1.0 (c (n "lunatic-log") (v "0.1.0") (d (list (d (n "lunatic") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "018vb32rx8w14zxfv1fknk6asnilxx8xzyjj3nf75mjk17hwczp8")))

(define-public crate-lunatic-log-0.1.1 (c (n "lunatic-log") (v "0.1.1") (d (list (d (n "lunatic") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m7l2qi8ngfxr357ghc7a4ldyqy3jkazszspkw7xjygc9lga5g98")))

(define-public crate-lunatic-log-0.2.0 (c (n "lunatic-log") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lunatic") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18gp8jsg4qrhchmdcvywi6ilz1gcppjcddrc3i53qx0ix261f65h")))

(define-public crate-lunatic-log-0.2.1 (c (n "lunatic-log") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lunatic") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pqqj9svr1as52179dh4pa6prpgrfms6qaw8j9g9nc5zclq1m562")))

(define-public crate-lunatic-log-0.2.2 (c (n "lunatic-log") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lunatic") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bw5gwj84siijnpl5v8cpdfsssll2gjxv1bwifyafr9ishci6r5h")))

(define-public crate-lunatic-log-0.2.3 (c (n "lunatic-log") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lunatic") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "142h2imk31nqnnfzpncyldz1h8c4hv65dyxbddmafz4gcswydl0b")))

(define-public crate-lunatic-log-0.3.0 (c (n "lunatic-log") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lunatic") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13b4rbgzrjs7y9h4kifv7x9wvpjdhav3ab6p5847g9hdy6wcl25i")))

(define-public crate-lunatic-log-0.4.0 (c (n "lunatic-log") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lunatic") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1rsc2hlpvhpvhrxhh49x9kx716l7vva5wp6j4yi4rywd2vq3c8mc")))

