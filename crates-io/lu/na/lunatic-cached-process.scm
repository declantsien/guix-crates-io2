(define-module (crates-io lu na lunatic-cached-process) #:use-module (crates-io))

(define-public crate-lunatic-cached-process-0.1.0 (c (n "lunatic-cached-process") (v "0.1.0") (d (list (d (n "lunatic") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hhs8nlq2hz6m9gmlc01inpwvya1m57l3h9s4l80snxphx8xv77c")))

(define-public crate-lunatic-cached-process-0.1.1 (c (n "lunatic-cached-process") (v "0.1.1") (d (list (d (n "lunatic") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "115iminzlsm17k9hj8z0fwy7l9i9527ga98nj197c208hclib45m")))

(define-public crate-lunatic-cached-process-0.2.0 (c (n "lunatic-cached-process") (v "0.2.0") (d (list (d (n "lunatic") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zj6hsrwb4agqszndw5my3zyfsy89qdsrm4ql6h0zlpzw6jxg2vk")))

(define-public crate-lunatic-cached-process-0.2.1 (c (n "lunatic-cached-process") (v "0.2.1") (d (list (d (n "lunatic") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r7s7bbx8w35b4v9dvcbhrqmbbfh41vaw6b1apc9km9x75dhywnp")))

(define-public crate-lunatic-cached-process-0.2.2 (c (n "lunatic-cached-process") (v "0.2.2") (d (list (d (n "lunatic") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "004inxfwwzgf6vwg64zdsqajigpdl776pv1xwrxg7fasqihzir45")))

(define-public crate-lunatic-cached-process-0.2.3 (c (n "lunatic-cached-process") (v "0.2.3") (d (list (d (n "lunatic") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mm0dzspw4zwmy9zcqjdnip7j9lgb2gy27jb23p2cgjpmrcvrymn")))

