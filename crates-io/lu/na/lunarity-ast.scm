(define-module (crates-io lu na lunarity-ast) #:use-module (crates-io))

(define-public crate-lunarity-ast-0.1.0 (c (n "lunarity-ast") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.4") (d #t) (k 2)) (d (n "toolshed") (r "^0.4") (d #t) (k 0)))) (h "08i17pwn8w7cq5dz6haswzlzs52x71mj4x1y5k3ankvf7zq0piv9")))

(define-public crate-lunarity-ast-0.2.0 (c (n "lunarity-ast") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "toolshed") (r "^0.6") (d #t) (k 0)))) (h "1h8629q8k94v73j3z2vijr01yb9fyn4yw5bbwbbc47xwwwvij1kx")))

