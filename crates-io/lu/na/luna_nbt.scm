(define-module (crates-io lu na luna_nbt) #:use-module (crates-io))

(define-public crate-luna_nbt-0.0.1 (c (n "luna_nbt") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00chmdznvyn8h44mjzr8c1fzw933nsdi425qg2zjd1bxl9bd2lqs") (f (quote (("with_serde" "serde") ("serde_unsigned") ("serde_boolean") ("default" "with_serde" "serde_boolean")))) (y #t)))

(define-public crate-luna_nbt-0.0.2 (c (n "luna_nbt") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 2)))) (h "0n8w9x8kxyddac5fj2pjw5zl5i4g08z573g6w4inii206g0abvg6") (f (quote (("with_serde" "serde") ("serde_unsigned") ("serde_boolean") ("default" "with_serde" "serde_boolean"))))))

(define-public crate-luna_nbt-0.0.3 (c (n "luna_nbt") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 2)))) (h "1pzg1w2by9fydzmyj6h0287xyqzisn6kds43lzli6gni88dkrzj6") (f (quote (("with_serde" "serde") ("serde_unsigned") ("serde_boolean") ("default" "with_serde" "serde_boolean"))))))

