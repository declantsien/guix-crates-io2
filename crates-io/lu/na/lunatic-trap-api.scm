(define-module (crates-io lu na lunatic-trap-api) #:use-module (crates-io))

(define-public crate-lunatic-trap-api-0.13.2 (c (n "lunatic-trap-api") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lunatic-common-api") (r "^0.13") (d #t) (k 0)) (d (n "wasmtime") (r "^8") (d #t) (k 0)))) (h "13sswwsgr4kxngv33ajbvhvp9cji08cq0x79bgr7gwakdbkp6jzd")))

