(define-module (crates-io lu na lunatic-sys) #:use-module (crates-io))

(define-public crate-lunatic-sys-0.13.0 (c (n "lunatic-sys") (v "0.13.0") (h "0dd46a15limj3v9nxki51psyki7f3dqnm0gzdigmjr4n7h1bkf10")))

(define-public crate-lunatic-sys-0.14.0 (c (n "lunatic-sys") (v "0.14.0") (h "1njsqw0fwwlr28a8apcx25lvn67j7j36c9fla13734s6zasxhwnm")))

