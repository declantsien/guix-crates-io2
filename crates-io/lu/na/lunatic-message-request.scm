(define-module (crates-io lu na lunatic-message-request) #:use-module (crates-io))

(define-public crate-lunatic-message-request-0.1.0 (c (n "lunatic-message-request") (v "0.1.0") (d (list (d (n "lunatic") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f5qad0zn23dxx46cd8ihmbi4d4jlgjzyvxzp7qnp4xlq2yjnq1q")))

(define-public crate-lunatic-message-request-0.1.1 (c (n "lunatic-message-request") (v "0.1.1") (d (list (d (n "lunatic") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x11mgmll37sk3lxhfl6w0xzpwmxdkyqw9qkj8lgx5jdsinl96l6")))

