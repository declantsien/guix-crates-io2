(define-module (crates-io lu na lunatic-envelop) #:use-module (crates-io))

(define-public crate-lunatic-envelop-1.0.0 (c (n "lunatic-envelop") (v "1.0.0") (d (list (d (n "hash-map-id") (r "^0.10.0") (d #t) (k 0)) (d (n "lunatic") (r "^0.11.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)))) (h "037r16ldivs7vfqkjl3q1wv657jxw6g0hb1lxx7bymjbhwx6di74")))

(define-public crate-lunatic-envelop-1.0.1 (c (n "lunatic-envelop") (v "1.0.1") (d (list (d (n "hash-map-id") (r "^0.10.0") (d #t) (k 0)) (d (n "lunatic") (r "^0.11.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)))) (h "1pryfxvn7vjqmlzqjglvxx78mblkvz0agy274ymk0inv8df47p04")))

(define-public crate-lunatic-envelop-1.0.2 (c (n "lunatic-envelop") (v "1.0.2") (d (list (d (n "hash-map-id") (r "^0.10.0") (d #t) (k 0)) (d (n "lunatic") (r "^0.11.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)))) (h "0gwrs2m7mqmhk8mqsz4niw8sy1v70z6c9h9nhs7dv23l38hzrrxn")))

