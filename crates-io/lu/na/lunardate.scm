(define-module (crates-io lu na lunardate) #:use-module (crates-io))

(define-public crate-lunardate-0.0.1 (c (n "lunardate") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "071gf7kk48jdn5gsxgs94442rjcjdx8z3cb6pxj1iffkzm987f75")))

(define-public crate-lunardate-0.0.2 (c (n "lunardate") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1qigwbgv73j8m6jjz4gqjgnmdjss6prmg2hvngfrjdbb4i994sqv")))

(define-public crate-lunardate-0.1.0 (c (n "lunardate") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1p6qr14hpw9jlyh8cg80g335pqjmjxr2zp9fz0fkrkn8yvvn39ns")))

(define-public crate-lunardate-0.1.1 (c (n "lunardate") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1ingib5zhr1w18ld7pvj5lnhbq3cmw5yns3k8if0faxaa87b2sjb")))

(define-public crate-lunardate-0.1.2 (c (n "lunardate") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0xmf0if7k3bbvznz50lbajl2iz8cq5na0ar7bplakkj8jc49z6vq")))

(define-public crate-lunardate-0.1.3 (c (n "lunardate") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0nfzaz4n58bsym6xfx7x17s4g1lsxg2ga03lk720pngzjwwll4zr")))

(define-public crate-lunardate-0.1.4 (c (n "lunardate") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1p7i0r0d6534jkg2gjnsx3rvylag17b89v799w15dbcgvyrzha1m")))

(define-public crate-lunardate-0.2.0 (c (n "lunardate") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "14cxwgg2nhjjgsy4g1z5r9i86rdb6543hppl2k05ha8sq8g3270a")))

(define-public crate-lunardate-0.2.1 (c (n "lunardate") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std" "clock"))) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1gff8yps0nj3nsg6n5b4ny2mbr9ymq2aawyg8501m5x9l1nyf5gz")))

