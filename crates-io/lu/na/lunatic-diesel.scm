(define-module (crates-io lu na lunatic-diesel) #:use-module (crates-io))

(define-public crate-lunatic-diesel-0.1.0 (c (n "lunatic-diesel") (v "0.1.0") (d (list (d (n "bigdecimal") (r ">=0.0.13, <0.4.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "diesel") (r "^2.0") (f (quote ("i-implement-a-third-party-backend-and-opt-into-breaking-changes"))) (d #t) (k 0)) (d (n "lunatic") (r "^0.12.0") (d #t) (k 0)) (d (n "lunatic-sqlite-api") (r "^0.13.0") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 2)))) (h "1l9lljc0wrn5822d5m49nxp0s9lcd0j9bhi630mjfkz8mgqj6y0d")))

