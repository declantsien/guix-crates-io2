(define-module (crates-io lu na lunatic_message_derive) #:use-module (crates-io))

(define-public crate-lunatic_message_derive-0.1.0 (c (n "lunatic_message_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jqg3zf53xbz6psmi9rn8fkz946zz1ay57x9x90vslqjpx4n00cw")))

