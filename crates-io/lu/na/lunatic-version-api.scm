(define-module (crates-io lu na lunatic-version-api) #:use-module (crates-io))

(define-public crate-lunatic-version-api-0.9.0 (c (n "lunatic-version-api") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^0.36") (d #t) (k 0)))) (h "1yic16y3pdazdayhhgsq52zlj5psgx1k56ha6d7vi66c8gar3wvj")))

(define-public crate-lunatic-version-api-0.9.2 (c (n "lunatic-version-api") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^0.39") (d #t) (k 0)))) (h "16i7rvzlwhsc047ir496m5r2vfgxwzd0bsn2dijwr1rbzc0j3r4f")))

(define-public crate-lunatic-version-api-0.10.0 (c (n "lunatic-version-api") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^0.39") (d #t) (k 0)))) (h "0mz6cflpfzpgndznzsb483vlwkw3axy6k0m42slds6rjzab6a0yx")))

(define-public crate-lunatic-version-api-0.12.0 (c (n "lunatic-version-api") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^2") (d #t) (k 0)))) (h "19rxixk1f5skn2s0l3lpk2piapdgkwhhq5l91f3ls2p6v987cpiy")))

(define-public crate-lunatic-version-api-0.13.0 (c (n "lunatic-version-api") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^8") (d #t) (k 0)))) (h "1f7cxmd0fx27f54n2xagv07r96b92ksj0h34ph9wc51z7736j389")))

(define-public crate-lunatic-version-api-0.13.1 (c (n "lunatic-version-api") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^8") (d #t) (k 0)))) (h "0w7v1x0nc0g4s15w2ya8p6aai1gf7x5yjkify2f24gz54rcd6kli")))

(define-public crate-lunatic-version-api-0.13.2 (c (n "lunatic-version-api") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^8") (d #t) (k 0)))) (h "189xgi9q8lzmzxi16sad1bwgwywlpay7snfcs7w76ycjc1bnmxfg")))

