(define-module (crates-io lu na lunatic-common-api) #:use-module (crates-io))

(define-public crate-lunatic-common-api-0.9.0 (c (n "lunatic-common-api") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^0.36") (d #t) (k 0)))) (h "18hp8648d5ysvs6lin2dwll87fnnzr1fw9px36cg6gipc48ch5kb")))

(define-public crate-lunatic-common-api-0.9.2 (c (n "lunatic-common-api") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^0.39") (d #t) (k 0)))) (h "1ayp5y6rw1bbgvs2sa2q8mjb3y0sf771rn0rw4vrm4mq69v2ngm7")))

(define-public crate-lunatic-common-api-0.10.0 (c (n "lunatic-common-api") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^0.39") (d #t) (k 0)))) (h "16b252wczrqxgvwxgf5i9j63h9ll8mwvhjzq4fs0xlxk9izpkhv0")))

(define-public crate-lunatic-common-api-0.12.0 (c (n "lunatic-common-api") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^2") (d #t) (k 0)))) (h "0l1m917mrbxhixv31w78zz4apywng6r9pvgqy5bipxmvs2y92ab9")))

(define-public crate-lunatic-common-api-0.13.0 (c (n "lunatic-common-api") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^5") (d #t) (k 0)))) (h "1n39566p3prq5hc6b6jh8msrgaw93i68q6xshmkrh2fbly0ya3gq")))

(define-public crate-lunatic-common-api-0.13.1 (c (n "lunatic-common-api") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^8") (d #t) (k 0)))) (h "0zash87d4gl3gx97cjdpijlcv2zvai5dir7709namd18jmg9jc61")))

(define-public crate-lunatic-common-api-0.13.2 (c (n "lunatic-common-api") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^8") (d #t) (k 0)))) (h "1l8rbr0s144x0vvryxrnbnj0gpfg2103szswyx2x75l6l2pg44ck")))

