(define-module (crates-io lu na lunatic-macros) #:use-module (crates-io))

(define-public crate-lunatic-macros-0.6.0 (c (n "lunatic-macros") (v "0.6.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ypbrr2mb2s7538as3j0wpgmj8pgk22kczl3y0nyf2labgrgibcc")))

(define-public crate-lunatic-macros-0.6.1 (c (n "lunatic-macros") (v "0.6.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rrp7ahd8kf3bv80dkxqmw3p8586cjg8x4mmq7nxfbw15liswbxj")))

(define-public crate-lunatic-macros-0.9.0 (c (n "lunatic-macros") (v "0.9.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r1j6ksbi41krx8ihvvpkbv8a964zy5v2jj399al11a658flzivk")))

(define-public crate-lunatic-macros-0.10.0 (c (n "lunatic-macros") (v "0.10.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wz67ad79ccm3byp2ff5f5q8cmnycy02q6jfw3va0655c81pqydk")))

(define-public crate-lunatic-macros-0.10.1 (c (n "lunatic-macros") (v "0.10.1") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lax244v7vdcwdj3rvqpjwnx9hiisn08yzsk397p03vr6gbx50im")))

(define-public crate-lunatic-macros-0.11.0 (c (n "lunatic-macros") (v "0.11.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05c6xjq9ahihz6rhji1c21p91sdsxn201khvzx5c80lanfzzlk65")))

(define-public crate-lunatic-macros-0.11.4 (c (n "lunatic-macros") (v "0.11.4") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "029sifxv94jpd9ppkjw41z7da1bx6qshglw5kydckr4d14dv7y9j")))

(define-public crate-lunatic-macros-0.12.0 (c (n "lunatic-macros") (v "0.12.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00zxg66jk453ywx1hffw18aa3h3sh97qnm9qv79q5gl5wqbjy4jj")))

(define-public crate-lunatic-macros-0.13.0 (c (n "lunatic-macros") (v "0.13.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "165p2nwi3zmn7cfwl6v4gajfw0y1gmnn3cv7jqzrm46dplnxnzas")))

