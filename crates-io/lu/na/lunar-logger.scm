(define-module (crates-io lu na lunar-logger) #:use-module (crates-io))

(define-public crate-lunar-logger-0.1.0 (c (n "lunar-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "0y3yxsys6pf1lylnzspa015js7cvm6fmx4sadx74fhv4wlwwx98b")))

(define-public crate-lunar-logger-0.1.1 (c (n "lunar-logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1hch0cbhl0dwzqbddqynxfj5xn82xr0w41lficl1klb57nrqf1qs")))

(define-public crate-lunar-logger-0.1.2 (c (n "lunar-logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "173ka2yvc2xr8adrfbjb70rl7vs5cwanxh3sah23snr9mrw2ga42")))

