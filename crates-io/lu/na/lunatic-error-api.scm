(define-module (crates-io lu na lunatic-error-api) #:use-module (crates-io))

(define-public crate-lunatic-error-api-0.9.0 (c (n "lunatic-error-api") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hash-map-id") (r "^0.9") (d #t) (k 0)) (d (n "lunatic-common-api") (r "^0.9") (d #t) (k 0)) (d (n "wasmtime") (r "^0.36") (d #t) (k 0)))) (h "0060qs9bjzymq5hsis3cv07y808vc4apmf1q52w4rfhhwn0qw2xs")))

(define-public crate-lunatic-error-api-0.9.2 (c (n "lunatic-error-api") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hash-map-id") (r "^0.9") (d #t) (k 0)) (d (n "lunatic-common-api") (r "^0.9") (d #t) (k 0)) (d (n "wasmtime") (r "^0.39") (d #t) (k 0)))) (h "1x4wv8p18d2fk9x04p4lsrisamy5qxhyxsgwq9gi7p7psixl2ayh")))

(define-public crate-lunatic-error-api-0.10.0 (c (n "lunatic-error-api") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hash-map-id") (r "^0.10") (d #t) (k 0)) (d (n "lunatic-common-api") (r "^0.10") (d #t) (k 0)) (d (n "wasmtime") (r "^0.39") (d #t) (k 0)))) (h "1igm3pn4f0ylvy585sjh6w0barl71phspryblnfpgg7pa1nh61hl")))

(define-public crate-lunatic-error-api-0.12.0 (c (n "lunatic-error-api") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hash-map-id") (r "^0.12") (d #t) (k 0)) (d (n "lunatic-common-api") (r "^0.12") (d #t) (k 0)) (d (n "wasmtime") (r "^2") (d #t) (k 0)))) (h "1vv8hzs01zc73pgmi3ygwc42jkz1dbfaqn5yq9qw7l9ysmxzscx0")))

(define-public crate-lunatic-error-api-0.13.0 (c (n "lunatic-error-api") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hash-map-id") (r "^0.12") (d #t) (k 0)) (d (n "lunatic-common-api") (r "^0.13") (d #t) (k 0)) (d (n "wasmtime") (r "^5") (d #t) (k 0)))) (h "0fsckywns97id9mdhqmxb8dpdxshpva70wrcja41fwsi1as7gz0k")))

(define-public crate-lunatic-error-api-0.13.1 (c (n "lunatic-error-api") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hash-map-id") (r "^0.13") (d #t) (k 0)) (d (n "lunatic-common-api") (r "^0.13") (d #t) (k 0)) (d (n "wasmtime") (r "^8") (d #t) (k 0)))) (h "0qmczc88173mf6i89ial8b5xy1rgqv6xj3j03p3rirxzh4yipb26")))

(define-public crate-lunatic-error-api-0.13.2 (c (n "lunatic-error-api") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hash-map-id") (r "^0.13") (d #t) (k 0)) (d (n "lunatic-common-api") (r "^0.13") (d #t) (k 0)) (d (n "wasmtime") (r "^8") (d #t) (k 0)))) (h "01x84a8vzgpgf0chrdjabdb65lycxx0l741wqm2w2ica6i40xxlc")))

