(define-module (crates-io lu na lunatic-control) #:use-module (crates-io))

(define-public crate-lunatic-control-0.13.1 (c (n "lunatic-control") (v "0.13.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1cpfp5v2arhchmahn8ka5k5fh1gjqkn0qhwxyyfr4a6rv7s5y9xx")))

(define-public crate-lunatic-control-0.13.2 (c (n "lunatic-control") (v "0.13.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0wn3n7zlcspcyw694mfh26h3n9621875mk6m74ms1h4x9pnmkzsb")))

