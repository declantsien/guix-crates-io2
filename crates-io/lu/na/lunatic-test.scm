(define-module (crates-io lu na lunatic-test) #:use-module (crates-io))

(define-public crate-lunatic-test-0.9.0 (c (n "lunatic-test") (v "0.9.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pqhkssl635vw5mlh4kpzig5f5gr1rj5iabivc9sargnw6cvayzw")))

(define-public crate-lunatic-test-0.10.0 (c (n "lunatic-test") (v "0.10.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bd9gy3khmzzx9jzx6lqk6hq61sapmcqfap3s09r65ish40zp5im")))

(define-public crate-lunatic-test-0.10.1 (c (n "lunatic-test") (v "0.10.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r8623a4cfqkq191250q2yr3n5ya0vg3jwbvm45rp7mcnkm5pvfi")))

(define-public crate-lunatic-test-0.12.0 (c (n "lunatic-test") (v "0.12.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06mw5yi7ffy08s9jl7kffp2fgvabzkx95p9yk29wd800rrnfgbib")))

(define-public crate-lunatic-test-0.13.0 (c (n "lunatic-test") (v "0.13.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zc202zyh8rjiksv7y98n024zmc9hp916l38z07aknqpbamckq1i")))

