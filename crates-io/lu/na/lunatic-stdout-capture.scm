(define-module (crates-io lu na lunatic-stdout-capture) #:use-module (crates-io))

(define-public crate-lunatic-stdout-capture-0.9.0 (c (n "lunatic-stdout-capture") (v "0.9.0") (d (list (d (n "wasi-common") (r "^0.36") (d #t) (k 0)) (d (n "wiggle") (r "^0.35") (d #t) (k 0)))) (h "1p754w8wflr0hadi5ngmjiv323v6ksh5m55vnvl15mi7sh1gwk6i")))

(define-public crate-lunatic-stdout-capture-0.9.2 (c (n "lunatic-stdout-capture") (v "0.9.2") (d (list (d (n "wasi-common") (r "^0.39") (d #t) (k 0)) (d (n "wiggle") (r "^0.39") (d #t) (k 0)))) (h "13f2z10919scq5sdsx5zyxxqcq8xzx41c9qq7qbi5z8rjglsqmk4")))

(define-public crate-lunatic-stdout-capture-0.10.0 (c (n "lunatic-stdout-capture") (v "0.10.0") (d (list (d (n "wasi-common") (r "^0.39") (d #t) (k 0)) (d (n "wiggle") (r "^0.39") (d #t) (k 0)))) (h "0ph4vdx34zcf1438nn3r0hqr9s1vnyap02iidj6y6brznvnmyvb3")))

(define-public crate-lunatic-stdout-capture-0.12.0 (c (n "lunatic-stdout-capture") (v "0.12.0") (d (list (d (n "wasi-common") (r "^2") (d #t) (k 0)) (d (n "wiggle") (r "^2") (d #t) (k 0)))) (h "057krywxxk3iva909vnxdk8skziwnw6z8hqnph47aba7fdwb06nw")))

(define-public crate-lunatic-stdout-capture-0.13.0 (c (n "lunatic-stdout-capture") (v "0.13.0") (d (list (d (n "wasi-common") (r "^5") (d #t) (k 0)) (d (n "wiggle") (r "^5") (d #t) (k 0)))) (h "0za1pvkdd8wpzcpqg0sph9ic03sxvb16ghfdvjzaz8pli58gfrja")))

(define-public crate-lunatic-stdout-capture-0.13.1 (c (n "lunatic-stdout-capture") (v "0.13.1") (d (list (d (n "wasi-common") (r "^8") (d #t) (k 0)) (d (n "wiggle") (r "^8") (d #t) (k 0)))) (h "1m7vn1y88v4wkz1zcqwvrkjiifq4f28n4065hjypiqnh343wcdd3")))

(define-public crate-lunatic-stdout-capture-0.13.2 (c (n "lunatic-stdout-capture") (v "0.13.2") (d (list (d (n "wasi-common") (r "^8") (d #t) (k 0)) (d (n "wiggle") (r "^8") (d #t) (k 0)))) (h "1qlnzsabdg4119bf214yrs4b3kwml8hbqqv12s9cnk3g5vhklr3v")))

