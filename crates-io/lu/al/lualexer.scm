(define-module (crates-io lu al lualexer) #:use-module (crates-io))

(define-public crate-lualexer-0.1.0 (c (n "lualexer") (v "0.1.0") (d (list (d (n "insta") (r "^0.13") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1raq1yl4cxpfbwqg0dxjfxz75yrhzmv6v50qgqf13nmg0slk5mfs")))

(define-public crate-lualexer-0.1.1 (c (n "lualexer") (v "0.1.1") (d (list (d (n "insta") (r "^0.13") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "10rpdv38aaww7n7p3bclwd8xr0bzpwpx78yyq4dwsy43kiqp6ddz")))

(define-public crate-lualexer-0.1.2 (c (n "lualexer") (v "0.1.2") (d (list (d (n "insta") (r "^0.13") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0a4wqn9dpjgc8jwhgd73jhknny503c7dx690dfmjdwnx52ik0k37")))

