(define-module (crates-io lu st luster) #:use-module (crates-io))

(define-public crate-luster-0.1.0 (c (n "luster") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "0aqlixr9p6qcs7711h1n3kb0qm0933nqlz6prj434fzza4sd0s7m")))

(define-public crate-luster-0.1.1 (c (n "luster") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "18q7fbzybr6l362mhxyjid8y9jcg6im2r3mypsdv3vjlyfrbvxka")))

(define-public crate-luster-0.1.2 (c (n "luster") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "1ffdzihbpd378s2lysi6c2id2qvg0z0yv6k28fgpfnirw5bfi9nj")))

(define-public crate-luster-0.1.3 (c (n "luster") (v "0.1.3") (d (list (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "1kcwkaahdm1q5qq1cynflzcfz242zdxv8kdzymwmn37c5scpmriy")))

