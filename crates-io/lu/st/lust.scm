(define-module (crates-io lu st lust) #:use-module (crates-io))

(define-public crate-lust-0.1.0 (c (n "lust") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "03ykjyy5ymsf0p0k8ppr4dzwf8gkc7kklyq2w4z7brm5yv9s7xn6") (f (quote (("default"))))))

(define-public crate-lust-0.1.1 (c (n "lust") (v "0.1.1") (d (list (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1jh412byzjg9kd9191p215vzhq1nl47hsx16w02544fpbgfvipcz") (f (quote (("default"))))))

