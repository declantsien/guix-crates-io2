(define-module (crates-io k5 #{10}# k510-pac) #:use-module (crates-io))

(define-public crate-k510-pac-0.0.1 (c (n "k510-pac") (v "0.0.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1x4x6j8zyl2943q0nxm31zbnwx29zhy2j4hf8gr94qchz0c4vwzi") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-k510-pac-0.0.2 (c (n "k510-pac") (v "0.0.2") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0fkr75ag5vifbrgqayga9fh06xmc78a2zdfd2zrwlb88jifgb9hc") (f (quote (("rt" "riscv-rt"))))))

