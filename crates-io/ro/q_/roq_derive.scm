(define-module (crates-io ro q_ roq_derive) #:use-module (crates-io))

(define-public crate-roq_derive-0.1.0 (c (n "roq_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1d7hnrkg516350mdwd9z9675j8j9fr7zh6z7a4lwj9j8v1fkapki")))

