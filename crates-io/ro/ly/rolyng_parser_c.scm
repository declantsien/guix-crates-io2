(define-module (crates-io ro ly rolyng_parser_c) #:use-module (crates-io))

(define-public crate-rolyng_parser_c-0.1.0 (c (n "rolyng_parser_c") (v "0.1.0") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0hk243m64chg11sqv17zpndzn0hvqsng391pcclln0gyb0cg8jw9") (y #t)))

(define-public crate-rolyng_parser_c-0.1.1 (c (n "rolyng_parser_c") (v "0.1.1") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1xn73jbvg1j4dzg3nnmq971rfaahhjcqdbjsidlrznmrp1jbabih") (y #t)))

