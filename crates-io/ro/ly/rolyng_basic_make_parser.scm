(define-module (crates-io ro ly rolyng_basic_make_parser) #:use-module (crates-io))

(define-public crate-rolyng_basic_make_parser-0.1.2 (c (n "rolyng_basic_make_parser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "15kdym7qg0wjcmbjvcajls1yhjj7qkkh18f9zn3pvf9k1445bxyn")))

