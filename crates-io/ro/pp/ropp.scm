(define-module (crates-io ro pp ropp) #:use-module (crates-io))

(define-public crate-ropp-0.1.0 (c (n "ropp") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "rpassword") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "18fqngnnjm4yc8cvc6yh9g4w9yq2dpar02aywxyl84d8ng9ww2iz") (y #t)))

(define-public crate-ropp-0.1.1 (c (n "ropp") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "rpassword") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "1p87138ap78ga0m5imssg0r6hrwfr0qbl97d44dprclr02wq3djz")))

