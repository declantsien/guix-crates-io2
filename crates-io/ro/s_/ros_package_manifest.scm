(define-module (crates-io ro s_ ros_package_manifest) #:use-module (crates-io))

(define-public crate-ros_package_manifest-0.1.0 (c (n "ros_package_manifest") (v "0.1.0") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "once_cell") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)) (d (n "roxmltree") (r "^0.6.1") (d #t) (k 0)))) (h "1blxcav82gkhr9zrzl4banxgfis6zpi2ljks93dc9wn8xbhr8w2w")))

(define-public crate-ros_package_manifest-0.2.0 (c (n "ros_package_manifest") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.4.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "03vnvmzqvvz52dnsn3h17wywxscqj47j96iy9zj79s1rn1f54gsc")))

