(define-module (crates-io ro nn ronnie-crypto) #:use-module (crates-io))

(define-public crate-ronnie-crypto-0.1.0 (c (n "ronnie-crypto") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "openssl") (r "^0.10.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0ma8mfa9kg2pcd5l41cxynj8a3rqqbybssm41mv0cfwr02m3bv4p")))

