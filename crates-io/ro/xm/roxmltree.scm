(define-module (crates-io ro xm roxmltree) #:use-module (crates-io))

(define-public crate-roxmltree-0.1.0 (c (n "roxmltree") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "elementtree") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "sxd-document") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "treexml") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "xmlparser") (r "^0.6.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1xzbb8hl7yk8bwg1vrax234fsym4jy19isdfz62bq1gxcxhrzdyy") (f (quote (("benchmark" "bencher" "xmltree" "sxd-document" "elementtree" "treexml" "xml-rs"))))))

(define-public crate-roxmltree-0.2.0 (c (n "roxmltree") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "xmlparser") (r "^0.6.1") (d #t) (k 0)))) (h "0q2arqzs9i15h5pxm7ls3xwqmgr2c6x2513zsr1i2xxdqxd3drwd")))

(define-public crate-roxmltree-0.3.0 (c (n "roxmltree") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "xmlparser") (r "^0.7") (d #t) (k 0)))) (h "0sbblz35kfpwxyc71i8a3h8rlixjqigr75p24i2zg29zcs1hq1xd")))

(define-public crate-roxmltree-0.4.0 (c (n "roxmltree") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "xmlparser") (r "^0.8") (d #t) (k 0)))) (h "0jv5wa9cjp4nf4fyg432hqcsyf63vfz5gr4as9n8q8bjcd2pg6l6")))

(define-public crate-roxmltree-0.4.1 (c (n "roxmltree") (v "0.4.1") (d (list (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "xmlparser") (r "^0.8") (d #t) (k 0)))) (h "1wxcag6r9cl5y51k4hjrzywcapp1wsp02984fri1pnn2s1kh8rh2")))

(define-public crate-roxmltree-0.5.0 (c (n "roxmltree") (v "0.5.0") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "xmlparser") (r "^0.9") (d #t) (k 0)))) (h "0vim9fywnj8zcf67yc43hxk4zhx26vsm9kjsqk9r9r8y0flifvzd")))

(define-public crate-roxmltree-0.6.0 (c (n "roxmltree") (v "0.6.0") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "xmlparser") (r "^0.9") (d #t) (k 0)))) (h "0ydm25hc0cyisfzqyanbazf1syk9gmqr4r87sv7zdcx8pw621c2k")))

(define-public crate-roxmltree-0.6.1 (c (n "roxmltree") (v "0.6.1") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "xmlparser") (r "^0.9") (d #t) (k 0)))) (h "0747azn8njasc5lsvw87d6wj8zhfjx2y73lh12v3rg3lla08y39k")))

(define-public crate-roxmltree-0.7.0 (c (n "roxmltree") (v "0.7.0") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "xmlparser") (r "^0.9") (d #t) (k 0)))) (h "0756w1df02ylhw1cygzg0f5zi81v12yr5vrpwspzg3pvx5y3cg0m")))

(define-public crate-roxmltree-0.7.1 (c (n "roxmltree") (v "0.7.1") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.10") (d #t) (k 0)))) (h "0x71f30ssznlg4vxvbj7m50r5xy7hpq7ml3zh4pjcvlcaqz1k8xi")))

(define-public crate-roxmltree-0.7.2 (c (n "roxmltree") (v "0.7.2") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.10") (d #t) (k 0)))) (h "0l8ciwi5h0f79yskwpnq5jind9sbyi2scj5zbvjwgl9zanjwpbz9")))

(define-public crate-roxmltree-0.7.3 (c (n "roxmltree") (v "0.7.3") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.10") (d #t) (k 0)))) (h "0ay04flfbwz6a205qwpsl922g73nwzzv77bbqsh9ddn1axr40lh8")))

(define-public crate-roxmltree-0.8.0 (c (n "roxmltree") (v "0.8.0") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.12") (d #t) (k 0)))) (h "0hr6f0h749rpgvb3z2cr67m3skqj0l6vvmsbffag8ri6rkd4r0i7")))

(define-public crate-roxmltree-0.9.0 (c (n "roxmltree") (v "0.9.0") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13") (d #t) (k 0)))) (h "1wz23l88gx0s2cxrv4x75xdrnd31v6vkpc2sr9mchw2vbqsfi3kh")))

(define-public crate-roxmltree-0.9.1 (c (n "roxmltree") (v "0.9.1") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13") (d #t) (k 0)))) (h "0mq99l2grdlwbchac4diwscpq003mgq5d17x10pf1lwj1fr9dmlr")))

(define-public crate-roxmltree-0.10.0 (c (n "roxmltree") (v "0.10.0") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.1") (d #t) (k 0)))) (h "10m0f13zz115jz66489kr0x1cs6mbjxg0an2bi9biwwfvlpka55z")))

(define-public crate-roxmltree-0.10.1 (c (n "roxmltree") (v "0.10.1") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.1") (d #t) (k 0)))) (h "0mz1mzsgqrmi5z5ixqhii3g54rsjps72rl4p5dh79xh5ylx78aa1")))

(define-public crate-roxmltree-0.11.0 (c (n "roxmltree") (v "0.11.0") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.1") (d #t) (k 0)))) (h "17bq0paqvbx1liy1qgx4yx2j9pwhnr9992vwyy3rs1kp809iy06m")))

(define-public crate-roxmltree-0.11.1 (c (n "roxmltree") (v "0.11.1") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.2") (d #t) (k 0)))) (h "035zz0s1y09qql3gi01gbjmlcfss9nqr2i91zmnsklkjiighfy02") (y #t)))

(define-public crate-roxmltree-0.12.0 (c (n "roxmltree") (v "0.12.0") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.2") (d #t) (k 0)))) (h "1f1vhq4fvibsjx2sjc77phwjb7zvdxbwlsv72rcyii4cqb2i2g78")))

(define-public crate-roxmltree-0.13.0 (c (n "roxmltree") (v "0.13.0") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.2") (d #t) (k 0)))) (h "1wji8hszj8jxjsdpi00ka26n0yax0l9ashn45ryzqsw4kz1wdpqp")))

(define-public crate-roxmltree-0.13.1 (c (n "roxmltree") (v "0.13.1") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.3") (d #t) (k 0)))) (h "0wgnlvwf6j180j233gyx63z7xnm6cf01alw11q6khvb4xaqxgxyv")))

(define-public crate-roxmltree-0.14.0 (c (n "roxmltree") (v "0.14.0") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.3") (d #t) (k 0)))) (h "010mahzyarkzb7kaawgga1a9y46nsp209yh2i4g4pq98bg8afn5z") (f (quote (("std"))))))

(define-public crate-roxmltree-0.14.1 (c (n "roxmltree") (v "0.14.1") (d (list (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.3") (d #t) (k 0)))) (h "0sv44pfl218p6r9anq5si6fhv0vz26vq20y42pi3f3j15sk086cj") (f (quote (("std") ("default" "std"))))))

(define-public crate-roxmltree-0.15.0 (c (n "roxmltree") (v "0.15.0") (d (list (d (n "xmlparser") (r "^0.13.3") (d #t) (k 0)))) (h "03jcdal74zrznrvnjcmaa6ln3w7mr8p46j1r9das24a7mk1hha8i") (f (quote (("std") ("default" "std"))))))

(define-public crate-roxmltree-0.15.1 (c (n "roxmltree") (v "0.15.1") (d (list (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "12p4vyg6c906pclhpgq8h21x1acza3dl5wk1gqp156qj3a1yk7bb") (f (quote (("std") ("default" "std"))))))

(define-public crate-roxmltree-0.16.0 (c (n "roxmltree") (v "0.16.0") (d (list (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "13pxvdvbn17jhjjzgdbsii6w2bl30a2wcw7jqy4axc3hjyslfvgv") (f (quote (("std") ("positions") ("default" "std" "positions"))))))

(define-public crate-roxmltree-0.17.0 (c (n "roxmltree") (v "0.17.0") (d (list (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "10vk3i8yv2ph9z5rc4p003mzvldm1086vwpf79winj6i9ja9n726") (f (quote (("std") ("positions") ("default" "std" "positions"))))))

(define-public crate-roxmltree-0.18.0 (c (n "roxmltree") (v "0.18.0") (d (list (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "1n5ikvn00ciqkkr4hnch5ws1k3gfj8z50j3alv6wdf5nayj9bxfq") (f (quote (("std") ("positions") ("default" "std" "positions"))))))

(define-public crate-roxmltree-0.18.1 (c (n "roxmltree") (v "0.18.1") (d (list (d (n "xmlparser") (r "^0.13.6") (k 0)))) (h "00mkd2xyrxm8ap39sxpkhzdzfn2m98q3zicf6wd2f6yfa7il08w6") (f (quote (("std" "xmlparser/std") ("positions") ("default" "std" "positions"))))))

(define-public crate-roxmltree-0.19.0 (c (n "roxmltree") (v "0.19.0") (h "0zs0q8hg5nnh91s1ib6r0fky7xm8ay63ayfa5i1afxxpwgalzl9w") (f (quote (("std") ("positions") ("default" "std" "positions")))) (r "1.60")))

(define-public crate-roxmltree-0.20.0 (c (n "roxmltree") (v "0.20.0") (h "15vw91ps91wkmmgy62khf9zb63bdinvm80957dascbsw7dwvc83c") (f (quote (("std") ("positions") ("default" "std" "positions")))) (r "1.60")))

