(define-module (crates-io ro db rodbus-client) #:use-module (crates-io))

(define-public crate-rodbus-client-0.1.1 (c (n "rodbus-client") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodbus") (r "^0.1.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.5") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "19nxsi51z78wrkzvnbxk3xbfb1ii77z73lcxpy1bkrwj4icr87fn") (y #t)))

