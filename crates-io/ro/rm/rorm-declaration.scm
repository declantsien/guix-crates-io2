(define-module (crates-io ro rm rorm-declaration) #:use-module (crates-io))

(define-public crate-rorm-declaration-0.1.0 (c (n "rorm-declaration") (v "0.1.0") (d (list (d (n "ordered-float") (r "^3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cqxh0wvwbwzgxxlabj7prkawjl04jkk7k31jnb7sq602j8w3ryp")))

(define-public crate-rorm-declaration-0.2.0 (c (n "rorm-declaration") (v "0.2.0") (d (list (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "~0.24") (d #t) (k 0)))) (h "0x35hdwldqsawjwv0s0scp3i3pp68ddsh8gjn5nz2v3dzrh9wmd5") (f (quote (("default" "serde/derive" "ordered-float/serde" "strum/derive"))))))

(define-public crate-rorm-declaration-0.3.0 (c (n "rorm-declaration") (v "0.3.0") (d (list (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "~0.24") (d #t) (k 0)))) (h "12cjcm0sjj8b0c931hdsx3ycwh2fhwwrylkxv2i818c006bl11ac") (f (quote (("default" "ordered-float/serde" "serde/derive" "strum/derive"))))))

(define-public crate-rorm-declaration-0.4.0 (c (n "rorm-declaration") (v "0.4.0") (d (list (d (n "ordered-float") (r "~3") (d #t) (k 0)) (d (n "rustc_version") (r "~0.4") (d #t) (k 1)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "strum") (r "~0.25") (d #t) (k 0)))) (h "10j25810ixljhxsx4c5mdvrpxrg7nv8k7krkxgkg027i67581vmn") (f (quote (("sqlite") ("postgres") ("mysql") ("default" "ordered-float/serde" "serde/derive" "strum/derive"))))))

