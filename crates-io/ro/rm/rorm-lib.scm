(define-module (crates-io ro rm rorm-lib) #:use-module (crates-io))

(define-public crate-rorm-lib-0.1.0 (c (n "rorm-lib") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "rorm-db") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (d #t) (k 0)))) (h "0zzs0lgvz0s23nmin84nv4ix058rl9f5vj4ylg8dlriz8h7cv6dh") (f (quote (("default" "rorm-db/tokio-rustls" "tokio/rt-multi-thread" "uuid/v4"))))))

(define-public crate-rorm-lib-0.2.0 (c (n "rorm-lib") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rorm-db") (r "~0.2") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (d #t) (k 0)))) (h "135bg7qjpbdcxbinlcvp3xda4vcq8w6rmspgdayg7f1s98c5m4k8") (f (quote (("default" "rorm-db/tokio-rustls" "tokio/rt-multi-thread"))))))

(define-public crate-rorm-lib-0.3.0 (c (n "rorm-lib") (v "0.3.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "env_logger") (r "~0.9") (o #t) (d #t) (k 0)) (d (n "futures") (r "~0.3") (d #t) (k 0)) (d (n "rorm-db") (r "~0.3") (d #t) (k 0)) (d (n "sqlx") (r "~0.6") (d #t) (k 0)) (d (n "tokio") (r "~1.22") (d #t) (k 0)))) (h "0sjdr2afy4567jv0nkazsfnxda8dajpk7qr9cyhfx0xfv4byj5v1") (f (quote (("default" "rorm-db/tokio-rustls" "tokio/rt-multi-thread")))) (s 2) (e (quote (("logging" "dep:env_logger"))))))

(define-public crate-rorm-lib-0.4.0 (c (n "rorm-lib") (v "0.4.0") (d (list (d (n "chrono") (r ">=0.4.20") (d #t) (k 0)) (d (n "env_logger") (r "~0.10") (o #t) (d #t) (k 0)) (d (n "futures") (r "~0.3") (d #t) (k 0)) (d (n "rorm-db") (r "~0.7") (f (quote ("tokio-rustls"))) (d #t) (k 0)) (d (n "sqlx") (r "~0.6") (d #t) (k 0)) (d (n "tokio") (r ">=1.23.1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "02g7p745ibkkbmarqqh5h784n8w6pard01fq2yphbg3rq1n1pk31") (s 2) (e (quote (("logging" "dep:env_logger"))))))

