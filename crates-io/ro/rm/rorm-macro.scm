(define-module (crates-io ro rm rorm-macro) #:use-module (crates-io))

(define-public crate-rorm-macro-0.1.0 (c (n "rorm-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "106ywksgi3lhv014vmk35cfdg08cs16dfq7m53ss0ykx5nf695jw") (f (quote (("unstable"))))))

(define-public crate-rorm-macro-0.2.0 (c (n "rorm-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12g3c9jp9f4nkv00sckd03a7b21m076xyja5a258nfrl1pmjjmgg") (f (quote (("unstable") ("default" "syn/full"))))))

(define-public crate-rorm-macro-0.3.0 (c (n "rorm-macro") (v "0.3.0") (d (list (d (n "darling") (r "~0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "00wfm125zg6g6cwvjmsd4zh8mxg8iq92zjch4qylidzlf5wszvml") (f (quote (("unstable") ("default" "syn/full"))))))

(define-public crate-rorm-macro-0.4.0 (c (n "rorm-macro") (v "0.4.0") (d (list (d (n "darling") (r "~0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "0v4g7fr14hw9wlzf9y8lqkd7v82p96i0kqfraqgaskl4fr2h2ksh") (f (quote (("unstable") ("default" "syn/full"))))))

(define-public crate-rorm-macro-0.5.0 (c (n "rorm-macro") (v "0.5.0") (d (list (d (n "darling") (r "~0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "0lpq8m8dr4yfpqgg28kp32crv5v5g2bz6w35di8m1sl88xf75d1g") (f (quote (("unstable") ("default" "syn/full"))))))

(define-public crate-rorm-macro-0.6.0 (c (n "rorm-macro") (v "0.6.0") (d (list (d (n "darling") (r "~0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "0iy81vpa32dsixzvs64xygdfzckmxmq3ch66cyw83lwxln33d9gw") (f (quote (("unstable") ("default" "syn/full"))))))

(define-public crate-rorm-macro-0.7.0 (c (n "rorm-macro") (v "0.7.0") (d (list (d (n "darling") (r "~0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "~1") (d #t) (k 0)))) (h "14vdpwy97q8rcdbqcgqfp9lph7biy531agjsmf8aa11wcbldiy62") (f (quote (("unstable") ("default" "syn/full")))) (y #t)))

(define-public crate-rorm-macro-0.7.1 (c (n "rorm-macro") (v "0.7.1") (d (list (d (n "darling") (r "~0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "~1") (d #t) (k 0)))) (h "0bg8vxbwgg4mvixbl7739wq4qj2m524g92l7jqh8y1wbssnkj7bi") (f (quote (("unstable") ("default" "syn/full"))))))

(define-public crate-rorm-macro-0.8.0 (c (n "rorm-macro") (v "0.8.0") (d (list (d (n "darling") (r "~0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "~1") (d #t) (k 0)))) (h "0hram8qfq49d20jsd7q7a7m8iwypk58ssj5rcpi7fjp60ls6kdqp") (f (quote (("unstable") ("default" "syn/full")))) (y #t)))

(define-public crate-rorm-macro-0.8.1 (c (n "rorm-macro") (v "0.8.1") (d (list (d (n "darling") (r "~0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "~1") (d #t) (k 0)))) (h "09ldxf0d8fg9166g4mhh92kzkpnss6s6h58m1w69zrmngyz4dxx0") (f (quote (("unstable") ("default" "syn/full"))))))

(define-public crate-rorm-macro-0.8.2 (c (n "rorm-macro") (v "0.8.2") (d (list (d (n "darling") (r "~0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "~1") (d #t) (k 0)))) (h "03480w2idjf5bi1gvbgjmdrzi19xs080f3wyrk8hvfr5i1ygdnmn") (f (quote (("unstable") ("default" "syn/full"))))))

