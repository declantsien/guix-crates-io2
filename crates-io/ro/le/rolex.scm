(define-module (crates-io ro le rolex) #:use-module (crates-io))

(define-public crate-rolex-0.1.0 (c (n "rolex") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1lpgv156ji6p9nayil0jdxlv97x8wqd2dx8wmpvzcjmgvdps0xh2") (y #t)))

(define-public crate-rolex-0.1.1 (c (n "rolex") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)))) (h "1kqr3xzlhszhlxpq1xgnjbm98j6h364kz6zfqpbh6kdq61yq1hyb") (y #t)))

(define-public crate-rolex-0.1.2 (c (n "rolex") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)))) (h "18nnxzairmnabmii58i22mijym7hfw2c90fg3dz9niiicj7q6592") (y #t)))

(define-public crate-rolex-0.1.3 (c (n "rolex") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)))) (h "1hfm07mjzirxdlwx77jp1zfdridp88kdi2z9dkhqwdfpg7g0i5v7") (y #t)))

