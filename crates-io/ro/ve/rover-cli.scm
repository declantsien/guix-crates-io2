(define-module (crates-io ro ve rover-cli) #:use-module (crates-io))

(define-public crate-rover-cli-0.1.0 (c (n "rover-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1byk66ng98rbk7h8yk5lx7xsp9p0ricgj1q4p027zgfpr608vq73")))

