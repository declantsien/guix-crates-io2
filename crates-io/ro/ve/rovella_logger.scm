(define-module (crates-io ro ve rovella_logger) #:use-module (crates-io))

(define-public crate-rovella_logger-0.1.0 (c (n "rovella_logger") (v "0.1.0") (h "1argsxsg5dxlgm3l8pgmpi4xrnn1ybvjz4nvr2n7ai3w25ahjlnr")))

(define-public crate-rovella_logger-0.1.1 (c (n "rovella_logger") (v "0.1.1") (h "02vn73gvdahcxkslc2nzg67623jz0ka8gv7ljjvvlp42nwszm6ll") (y #t)))

(define-public crate-rovella_logger-0.1.2 (c (n "rovella_logger") (v "0.1.2") (h "1vb3fpm37rhahzwig7a1q8v8w9im2bs9g8rlykf1mp3x1a73fmgf")))

(define-public crate-rovella_logger-0.1.3 (c (n "rovella_logger") (v "0.1.3") (h "0w3zkqrs5g7gzyiy3vimbfswfgaklf9qcpav3cy7p6qvlw7zdp82")))

(define-public crate-rovella_logger-0.1.4 (c (n "rovella_logger") (v "0.1.4") (h "0z4ml4ml4xf02k5h1ypiy6pc9ppw50k8iif3802f5qyvcbvy105p")))

