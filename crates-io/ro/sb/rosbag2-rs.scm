(define-module (crates-io ro sb rosbag2-rs) #:use-module (crates-io))

(define-public crate-rosbag2-rs-0.1.0 (c (n "rosbag2-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)))) (h "1gvpklwlpf09jbx56njq9rmyzvc9l7wxh3ik09g27v69bpy0y9zd") (y #t)))

(define-public crate-rosbag2-rs-0.1.1 (c (n "rosbag2-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)))) (h "1nr19skwbv2aq8bw4gghsji920104x1f4hmrb2i2acdd1gjf62k6")))

(define-public crate-rosbag2-rs-0.2.0 (c (n "rosbag2-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)))) (h "16slf6z850bb0qr3r78i9qhd9zp880i70vcz4jjf3bzqmr9hfgh3")))

(define-public crate-rosbag2-rs-0.2.1 (c (n "rosbag2-rs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)))) (h "0bdm07dh8dayvwfw387ld7g58cqbwknzlpyrvdgymzh0dc62cvpr")))

