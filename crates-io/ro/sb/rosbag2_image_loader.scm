(define-module (crates-io ro sb rosbag2_image_loader) #:use-module (crates-io))

(define-public crate-rosbag2_image_loader-0.1.0 (c (n "rosbag2_image_loader") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (d #t) (k 0)))) (h "0scyrsppzabhh4ww6dbfzzf40z0irsbw91r9v6di9249csl9dnlw")))

(define-public crate-rosbag2_image_loader-0.1.1 (c (n "rosbag2_image_loader") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (d #t) (k 0)))) (h "00dcsj9n6iiwagrm173qxjxn4in34cmlmj8jkqxkv9hb5q98q16z")))

(define-public crate-rosbag2_image_loader-0.1.2 (c (n "rosbag2_image_loader") (v "0.1.2") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (d #t) (k 0)))) (h "1v2ar6hqc8qhqv12llvb43brx7nvsh8kc3g96alwn3ycwji0whf4")))

