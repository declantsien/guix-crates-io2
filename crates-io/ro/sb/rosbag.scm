(define-module (crates-io ro sb rosbag) #:use-module (crates-io))

(define-public crate-rosbag-0.1.0 (c (n "rosbag") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wk1n17g5ixz8rbiccb6kswil84x666ajiaf3bdsg1za5ss7hrp7")))

(define-public crate-rosbag-0.2.0 (c (n "rosbag") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "00dqsvyd5ngsn361i64018g5vdnbmj9bgpmsg8xp220g57qwq065")))

(define-public crate-rosbag-0.2.1 (c (n "rosbag") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "1p9waq7zy1zm6683dzmsh95mqc7v7s4f5rf55kx3ibz1nwv40029")))

(define-public crate-rosbag-0.3.0 (c (n "rosbag") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1xc2cc1dmdfk9c56xx8wx9kff15mzrpz7xk2xdfjnacn35bj10xg")))

(define-public crate-rosbag-0.4.0 (c (n "rosbag") (v "0.4.0") (d (list (d (n "base16ct") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "bzip2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)))) (h "1rhskw9827a0zawbyfjgfzxxqxp9b4ba72snfl8g961lzyc1ghx6") (r "1.56")))

(define-public crate-rosbag-0.5.0 (c (n "rosbag") (v "0.5.0") (d (list (d (n "base16ct") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "bzip2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)))) (h "1xdmrsbjdhz4z9xag26i5n9l06kpwz1j7zhnbk7ykpr2n4f6jl22") (r "1.56")))

(define-public crate-rosbag-0.6.0 (c (n "rosbag") (v "0.6.0") (d (list (d (n "base16ct") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "bzip2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)))) (h "1x4w564q5vnr8hsw301mnahb4aw48cvlyrkkp7716l6w7bbf1vqc") (r "1.56")))

(define-public crate-rosbag-0.6.1 (c (n "rosbag") (v "0.6.1") (d (list (d (n "base16ct") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "bzip2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)))) (h "0q99dfw3g5fzaj04nf0yahwvlc515pi1x6yg4vgwrnn72cd0lisc") (r "1.56")))

(define-public crate-rosbag-0.6.2 (c (n "rosbag") (v "0.6.2") (d (list (d (n "base16ct") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "bzip2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)))) (h "155jhnn5dl9rv334pmq0xa1cxn0zcnrwfvawmw1y8q0z69lng6vp") (r "1.63")))

(define-public crate-rosbag-0.6.3 (c (n "rosbag") (v "0.6.3") (d (list (d (n "base16ct") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "bzip2") (r "^0.4.4") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)))) (h "1rxxx8m8xb9bdq5cc6hsxavdd471mipjbmz3j54sklg35y66hxzg") (r "1.63")))

