(define-module (crates-io ro a- roa-jsonrpc) #:use-module (crates-io))

(define-public crate-roa-jsonrpc-0.5.0-alpha (c (n "roa-jsonrpc") (v "0.5.0-alpha") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "jsonrpc-v2") (r "^0.5") (k 0)) (d (n "roa") (r "^0.5") (f (quote ("json"))) (k 0)))) (h "19zz9qwcxjpaf1gaszahwbgpvgqp9kjk0fm4swl1ibjmca6q5wzi")))

