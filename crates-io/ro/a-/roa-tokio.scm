(define-module (crates-io ro a- roa-tokio) #:use-module (crates-io))

(define-public crate-roa-tokio-0.5.0-rc (c (n "roa-tokio") (v "0.5.0-rc") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 2)) (d (n "roa-core") (r "^0.5.0-rc") (d #t) (k 0)) (d (n "roa-tcp") (r "^0.5.0-rc") (d #t) (k 2)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1fj80j0g672plc8dnr8c11rp4klrgxhk2yp30zq2bpp7mb4rrf26")))

(define-public crate-roa-tokio-0.5.0-rc.1 (c (n "roa-tokio") (v "0.5.0-rc.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 2)) (d (n "roa") (r "^0.5.0-rc.3") (k 0)) (d (n "roa") (r "^0.5.0-rc.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1bqlmzphnxig0bdwzgvl6yvhbagav10xi0m6nxnp7z7l1xbn1jy1") (f (quote (("docs" "roa/docs"))))))

(define-public crate-roa-tokio-0.5.0-rc.2 (c (n "roa-tokio") (v "0.5.0-rc.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 2)) (d (n "roa") (r "^0.5.0-rc.7") (k 0)) (d (n "roa") (r "^0.5.0-rc.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "05zshac40fwmw8pam4c9ysc7f1mgmvv3xsl0jiwxnyih2kzb4mmh") (f (quote (("docs" "roa/docs"))))))

(define-public crate-roa-tokio-0.5.0 (c (n "roa-tokio") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 2)) (d (n "roa") (r "^0.5.0") (k 0)) (d (n "roa") (r "^0.5.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "15qvkapg09nl2w1k2ldkavq97lg2jwqlnr05z9vflyrdqpz5czcx") (f (quote (("docs" "roa/docs"))))))

(define-public crate-roa-tokio-0.5.1 (c (n "roa-tokio") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 2)) (d (n "roa") (r "^0.5") (k 0)) (d (n "roa") (r "^0.5") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "16rl2syix8cn4vhd0s4bwxmdckjmxfv447a3zhry77bf26743fqk") (f (quote (("docs" "roa/docs"))))))

