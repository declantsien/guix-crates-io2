(define-module (crates-io ro a- roa-diesel) #:use-module (crates-io))

(define-public crate-roa-diesel-0.5.0-alpha (c (n "roa-diesel") (v "0.5.0-alpha") (d (list (d (n "diesel") (r "^1.4") (f (quote ("extras"))) (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("extras" "sqlite"))) (d #t) (k 2)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "roa-core") (r "^0.5.0-alpha") (d #t) (k 0)))) (h "1dcvdhd66ab5sgfnrwnd4hyapij8aw9dy37lwnlir086ly7a4kh0")))

(define-public crate-roa-diesel-0.5.0-beta (c (n "roa-diesel") (v "0.5.0-beta") (d (list (d (n "diesel") (r "^1.4") (f (quote ("extras"))) (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("extras" "sqlite"))) (d #t) (k 2)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "roa-core") (r "^0.5.0-beta") (d #t) (k 0)))) (h "189xgjgkbax2ky1k2fzqfsqg0b903v9licd12cpcb4dr3fypsbgy")))

(define-public crate-roa-diesel-0.5.0-rc (c (n "roa-diesel") (v "0.5.0-rc") (d (list (d (n "diesel") (r "^1.4") (f (quote ("extras"))) (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("extras" "sqlite"))) (d #t) (k 2)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "roa-core") (r "^0.5.0-rc") (d #t) (k 0)))) (h "04hn4j7x7lylirwbwdxyqb32xdcxfynr2875n3smxyfn6nxg7j64")))

(define-public crate-roa-diesel-0.5.0-rc.1 (c (n "roa-diesel") (v "0.5.0-rc.1") (d (list (d (n "diesel") (r "^1.4") (f (quote ("extras"))) (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("extras" "sqlite"))) (d #t) (k 2)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "roa") (r "^0.5.0-rc.3") (k 0)))) (h "0rv8gf4p5zvxyl5q99zrfvsjrfay8mdad02ji6rfcl5m54ghyn2v") (f (quote (("docs" "roa/docs"))))))

(define-public crate-roa-diesel-0.5.0-rc.2 (c (n "roa-diesel") (v "0.5.0-rc.2") (d (list (d (n "diesel") (r "^1.4") (f (quote ("extras"))) (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("extras" "sqlite"))) (d #t) (k 2)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "roa") (r "^0.5.0-rc.3") (k 0)))) (h "1fjxlmdsm3mbqlay15jm422fmpxw2l2w7cs4j6p1by9s6b2xk95m") (f (quote (("docs" "roa/docs"))))))

(define-public crate-roa-diesel-0.5.0-rc.3 (c (n "roa-diesel") (v "0.5.0-rc.3") (d (list (d (n "diesel") (r "^1.4") (f (quote ("extras"))) (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("extras" "sqlite"))) (d #t) (k 2)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "roa") (r "^0.5.0-rc.7") (k 0)))) (h "0vkc53kwfz339yg8lv0jcflhv22vxfl7vkaa3jhpg4llh7p8agcv") (f (quote (("docs" "roa/docs"))))))

(define-public crate-roa-diesel-0.5.0 (c (n "roa-diesel") (v "0.5.0") (d (list (d (n "diesel") (r "^1.4") (f (quote ("extras"))) (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("extras" "sqlite"))) (d #t) (k 2)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "roa") (r "^0.5.0") (k 0)))) (h "0s0rs3s2ip9lxcqzadqpyjs6x598z965fwcgh57w9gygkg1flx77") (f (quote (("docs" "roa/docs"))))))

(define-public crate-roa-diesel-0.6.0 (c (n "roa-diesel") (v "0.6.0") (d (list (d (n "diesel") (r "^1.4") (f (quote ("extras"))) (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("extras" "sqlite"))) (d #t) (k 2)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "roa") (r "^0.6.0") (k 0)))) (h "0q21yadj7grdvc9h2lb9gn89h2k9ahffxwrjj8f0gl5z3aizd7wq") (f (quote (("docs" "roa/docs"))))))

