(define-module (crates-io ro a- roa-async-std) #:use-module (crates-io))

(define-public crate-roa-async-std-0.6.0 (c (n "roa-async-std") (v "0.6.0") (d (list (d (n "async-std") (r "^1.10") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.10") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "roa") (r "^0.6.0") (k 0)) (d (n "roa") (r "^0.6.0") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0x733xbrqmqfq6m37ifxfhqkmcvdzdmmqql4an8v88642qy047dw") (f (quote (("docs" "roa/docs"))))))

