(define-module (crates-io ro st rostend) #:use-module (crates-io))

(define-public crate-rostend-0.1.0 (c (n "rostend") (v "0.1.0") (h "0x4dh51jas9mg4fbapabxabh6wh4agn18xc9n9l5fa0hdqfixbxq")))

(define-public crate-rostend-0.1.1 (c (n "rostend") (v "0.1.1") (d (list (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)))) (h "181shbjzf7ax6lmnlgzirykz14nndvajj5502cvk06hmg61v0nz0")))

