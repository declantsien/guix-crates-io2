(define-module (crates-io ro st rost) #:use-module (crates-io))

(define-public crate-rost-0.1.0 (c (n "rost") (v "0.1.0") (h "001qs7wkslhassiswgvk0v278yhv0prq898bwlivnn3javyi359g")))

(define-public crate-rost-0.1.1 (c (n "rost") (v "0.1.1") (h "1mf10bali6ahj86rvpyjmz874hhhjmvj5n4l0al1s4xfm52as6v5")))

(define-public crate-rost-0.1.2 (c (n "rost") (v "0.1.2") (h "0clyx49129labp0gwi52vzsz3i2ch4kzq6sz247bkdnnaa70rj8b")))

(define-public crate-rost-0.2.0 (c (n "rost") (v "0.2.0") (h "0ss8wwif064j9i5yzhsb2jg0yjkhyhyaimx1i341ryxki87h3mg8")))

(define-public crate-rost-0.2.1 (c (n "rost") (v "0.2.1") (h "1wgs8z92afqj85naw207h8jk7x7lvy8diy6bah1i1ayxhp3fclk3")))

