(define-module (crates-io ro st rost_app) #:use-module (crates-io))

(define-public crate-rost_app-0.1.0 (c (n "rost_app") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "1pbqcd0l44azzap8f2zxkn7naxg2mwpfwyxfh9jyd1zrkpgwf8cx")))

(define-public crate-rost_app-0.2.0 (c (n "rost_app") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "0r0p6jjzmz4c3kfvhyglqikyrinc3g0l4qxlxzx98gn9ir6hpkhx")))

