(define-module (crates-io ro t- rot-13) #:use-module (crates-io))

(define-public crate-rot-13-0.1.0 (c (n "rot-13") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "0q5a9zqpy7cqsbmsxnlzdgsd1y5j122fby8h99h53l7ahw8b6xcn")))

(define-public crate-rot-13-0.1.1 (c (n "rot-13") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "1ijinbjjfngvwpi3wz3gq1alsjrvjq0kqvwykmrab6z58asr8zjs")))

