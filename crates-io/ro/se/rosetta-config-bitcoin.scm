(define-module (crates-io ro se rosetta-config-bitcoin) #:use-module (crates-io))

(define-public crate-rosetta-config-bitcoin-0.1.0 (c (n "rosetta-config-bitcoin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.1.0") (d #t) (k 0)))) (h "1pwfzaw10fadhk24hmfz0i83ks5x3hiczckf860bch6nx2d3h0gg")))

(define-public crate-rosetta-config-bitcoin-0.2.5 (c (n "rosetta-config-bitcoin") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.2.5") (d #t) (k 0)))) (h "0z1pq5dvg3sf83bjq9dc6kqf5pkzz8dh97aabw05xilkflq1dxfj")))

(define-public crate-rosetta-config-bitcoin-0.3.0 (c (n "rosetta-config-bitcoin") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.2.5") (d #t) (k 0)))) (h "1dagviadzqd1bmizfci0h4nr5778jg01fpinifvqr8w12vpv6p51")))

(define-public crate-rosetta-config-bitcoin-0.4.0 (c (n "rosetta-config-bitcoin") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.4.0") (d #t) (k 0)))) (h "07v4gsjy4zi3sy140n6qqcwm6nc6qw71bzcvc0izvgq9gn7qdmbd")))

