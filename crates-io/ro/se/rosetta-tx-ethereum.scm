(define-module (crates-io ro se rosetta-tx-ethereum) #:use-module (crates-io))

(define-public crate-rosetta-tx-ethereum-0.1.0 (c (n "rosetta-tx-ethereum") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "ethabi") (r "^18.0.0") (d #t) (k 0)) (d (n "ethers-core") (r "^2.0.0") (d #t) (k 0)) (d (n "rosetta-config-ethereum") (r "^0.1.0") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "1l9y0aj0388agkd3aj6mmz7rwakfdjql5zf52kd3bc8k6z72nx7w")))

(define-public crate-rosetta-tx-ethereum-0.2.5 (c (n "rosetta-tx-ethereum") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "ethabi") (r "^18.0.0") (d #t) (k 0)) (d (n "ethers-core") (r "^2.0.0") (d #t) (k 0)) (d (n "rosetta-config-ethereum") (r "^0.2.5") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.2.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "1z8mp3wsk4kix7nxpd153yd7c6826ck8vniahwqz51gxaivpqkh3")))

(define-public crate-rosetta-tx-ethereum-0.4.0 (c (n "rosetta-tx-ethereum") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "ethabi") (r "^18.0.0") (d #t) (k 0)) (d (n "ethers-core") (r "^2.0.0") (d #t) (k 0)) (d (n "rosetta-config-ethereum") (r "^0.4.0") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "12c9whmas1b4z70d87kjixbqm6v84yaaqh8ssaw5vfm7d38bvfi7")))

