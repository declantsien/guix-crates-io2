(define-module (crates-io ro se rosetta-types) #:use-module (crates-io))

(define-public crate-rosetta-types-0.1.0 (c (n "rosetta-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1cy6qbi4kgcycm6xhkfjivgm2hgkbyiwvyjpwl69mb919fxbzq93")))

(define-public crate-rosetta-types-0.4.0 (c (n "rosetta-types") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0il5vixy6x1c1wffj03kqhglzbv68c1lbww8v9m54m53pgdp9wi1")))

