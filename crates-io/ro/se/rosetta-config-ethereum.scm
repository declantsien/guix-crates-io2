(define-module (crates-io ro se rosetta-config-ethereum) #:use-module (crates-io))

(define-public crate-rosetta-config-ethereum-0.1.0 (c (n "rosetta-config-ethereum") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j9ivgqicd2w54k98gw0j3wjxgzppkvzw4hdji6f3s2jh2vz13n9")))

(define-public crate-rosetta-config-ethereum-0.2.5 (c (n "rosetta-config-ethereum") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bzk1cpbmz9ka7af5l78wj73i3q5f3cia7nas84jk78h7pxq6g8d")))

(define-public crate-rosetta-config-ethereum-0.3.0 (c (n "rosetta-config-ethereum") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vzgsdym6g7pr7kbrqvxgfcxi1ld01i86inkqgah1v9ml7d96yy9")))

(define-public crate-rosetta-config-ethereum-0.4.0 (c (n "rosetta-config-ethereum") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nd3vzxix4m3cfqs4c049gw5hq54hgbi2jr2cq37cx90j6qh8gk6")))

