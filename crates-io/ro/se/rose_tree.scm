(define-module (crates-io ro se rose_tree) #:use-module (crates-io))

(define-public crate-rose_tree-0.1.0 (c (n "rose_tree") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.1.11") (d #t) (k 0)))) (h "0v94asxdsgdcyfg4p24h89jyijhpiy1ggihk3bh4yr7gc6saghlb")))

(define-public crate-rose_tree-0.1.1 (c (n "rose_tree") (v "0.1.1") (d (list (d (n "petgraph") (r "^0.1.11") (d #t) (k 0)))) (h "0nw47a9y3zzv0hfk24k3v8q04vy9aiq3g4i327z8bklhg782cszg")))

(define-public crate-rose_tree-0.1.2 (c (n "rose_tree") (v "0.1.2") (d (list (d (n "petgraph") (r "^0.1.11") (d #t) (k 0)))) (h "0jla56n46q3hah554zvgr36rzgjfnf4v46liwv3b5i57rn4kdpif")))

(define-public crate-rose_tree-0.2.0 (c (n "rose_tree") (v "0.2.0") (d (list (d (n "petgraph") (r "^0.5") (d #t) (k 0)))) (h "1hp11pawv1x4akvhj8wcqnwfjvsagfa7xgda7a0y4x47wgdfjk98")))

(define-public crate-rose_tree-0.3.0 (c (n "rose_tree") (v "0.3.0") (d (list (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "1bir822zssn8yrzzcbsv0awgihml0mkhj8la9zx4jn90x5hnrlgw")))

