(define-module (crates-io ro se rosewood) #:use-module (crates-io))

(define-public crate-rosewood-0.1.0 (c (n "rosewood") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.5") (d #t) (k 2)) (d (n "loam") (r "^0.1") (d #t) (k 0)) (d (n "pointy") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0igcrw8azxn719a0wnqsaviawmhwzharkjx57z0qvxpx8lp55v9q")))

(define-public crate-rosewood-0.2.0 (c (n "rosewood") (v "0.2.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "loam") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pointy") (r "^0.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "191i16vid74m1mxllb88kfd1gwdhm8jb9im9m70hwy7p9c8wiv33")))

(define-public crate-rosewood-0.3.0 (c (n "rosewood") (v "0.3.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "loam") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pointy") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1prf7bmcq1sr51ac978ha0hpjaxzz0cgy377ckf9k3ms6z4a67h8")))

