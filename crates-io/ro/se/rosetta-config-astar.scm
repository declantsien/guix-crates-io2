(define-module (crates-io ro se rosetta-config-astar) #:use-module (crates-io))

(define-public crate-rosetta-config-astar-0.3.0 (c (n "rosetta-config-astar") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.3.0") (d #t) (k 0)))) (h "03a668v50gaa526frmpnsyhcxisj7gyd3v20rqra9fz1n8kw466m")))

(define-public crate-rosetta-config-astar-0.4.0 (c (n "rosetta-config-astar") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.4.0") (d #t) (k 0)))) (h "0w9ygvcvq3dvmq1fvnqqc224b8irqig4a9mfvdpwipyddfv74w41")))

