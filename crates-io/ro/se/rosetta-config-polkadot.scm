(define-module (crates-io ro se rosetta-config-polkadot) #:use-module (crates-io))

(define-public crate-rosetta-config-polkadot-0.1.0 (c (n "rosetta-config-polkadot") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ykygghli9dinxl5wanfm6lnfw2y362d33z2d3ncpmal1ncjwmbk")))

(define-public crate-rosetta-config-polkadot-0.2.5 (c (n "rosetta-config-polkadot") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)))) (h "12sm5mfg42l44z1a2n06qgqcahnw1ml5i40pvs6yvmas0cm2dfxx")))

(define-public crate-rosetta-config-polkadot-0.4.0 (c (n "rosetta-config-polkadot") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rosetta-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)))) (h "06j6jbb792cp6cwpm55jfiappnhr6khhmg5m1rqv71nlrxlvkz1m")))

