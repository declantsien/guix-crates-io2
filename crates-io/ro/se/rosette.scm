(define-module (crates-io ro se rosette) #:use-module (crates-io))

(define-public crate-rosette-0.1.0 (c (n "rosette") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json" "native-tls" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "04f665a8mblvpfcwz85j9gwqsqslppwlzdkc34g6fz26xq8ahk5g")))

(define-public crate-rosette-0.1.1 (c (n "rosette") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json" "native-tls" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ghcp2bz2bsir9hq51rw9bnmkw717f4nbch0lydlrmcaj5gbi64i")))

