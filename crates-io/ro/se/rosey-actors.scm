(define-module (crates-io ro se rosey-actors) #:use-module (crates-io))

(define-public crate-rosey-actors-0.1.0 (c (n "rosey-actors") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.45") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.4") (f (quote ("sync"))) (d #t) (k 0)))) (h "0vf3pm4p4rd17161g6b35n4dmd9jd7d1mn6hijzfd0ccgm59b0j1")))

(define-public crate-rosey-actors-0.1.1 (c (n "rosey-actors") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.45") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.4") (f (quote ("sync"))) (d #t) (k 0)))) (h "185sizv0b7h2zil85d12wsxavrh52i840zajdvgpb04rvr0vjk1f")))

