(define-module (crates-io ro se rosetta-core) #:use-module (crates-io))

(define-public crate-rosetta-core-0.1.0 (c (n "rosetta-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.66") (d #t) (k 0)) (d (n "rosetta-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "rosetta-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1mxck6chzqnjn0y309d1jnygkp349p9zjicimvzgwzi321yzysk8")))

(define-public crate-rosetta-core-0.2.5 (c (n "rosetta-core") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.66") (d #t) (k 0)) (d (n "rosetta-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "rosetta-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0cykaqvjnjmchy8i4hi3kz60ws2rx589i49mhq59b5ckvjvqy4qq")))

(define-public crate-rosetta-core-0.3.0 (c (n "rosetta-core") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.66") (d #t) (k 0)) (d (n "rosetta-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "rosetta-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1c3ip99lsqqximgqhjmsjza9sw8kb4521v9b9ks9rwd334msy075")))

(define-public crate-rosetta-core-0.4.0 (c (n "rosetta-core") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.66") (d #t) (k 0)) (d (n "rosetta-crypto") (r "^0.4.0") (d #t) (k 0)) (d (n "rosetta-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "07020yzqrwk576prj253g0ppfid6zhfrxmgzsya1bdadw8520xdb")))

