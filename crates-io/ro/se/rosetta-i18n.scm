(define-module (crates-io ro se rosetta-i18n) #:use-module (crates-io))

(define-public crate-rosetta-i18n-0.1.0 (c (n "rosetta-i18n") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0k27ddsxgkafgs9ibbqr5ymhl0xnbgiq1164a3dvr65frwl5ma6a")))

(define-public crate-rosetta-i18n-0.1.1 (c (n "rosetta-i18n") (v "0.1.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "11ym8pikqgrsixpgra6450mbmv8j4qlkyisjzi2w80slrv7n2nji")))

(define-public crate-rosetta-i18n-0.1.2 (c (n "rosetta-i18n") (v "0.1.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1ph3jppx9wvg3ci13b3sv9bvxw5zv4sz96438p4yqak06bl8saf5")))

(define-public crate-rosetta-i18n-0.1.3 (c (n "rosetta-i18n") (v "0.1.3") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "18jvphr59ss345z7sgj77vf7lkc51qfa8hf0g9damqwd2nwh331g")))

