(define-module (crates-io ro se rose_bloom) #:use-module (crates-io))

(define-public crate-rose_bloom-0.1.0 (c (n "rose_bloom") (v "0.1.0") (h "1v86ky0i68rddbwkw3m4pgkliis4sak3kpb04lngqi2jvx6rrx0d") (y #t)))

(define-public crate-rose_bloom-0.1.1 (c (n "rose_bloom") (v "0.1.1") (h "16h7kzi69sr7k6j5nxk9hjlkqkb1zd0mrlwj9z7w8hjkfklal278") (y #t)))

(define-public crate-rose_bloom-0.1.2 (c (n "rose_bloom") (v "0.1.2") (h "011mf58wck8vd4cs0l3gs5ymrwq5bm9biasdb4za502jk2hbc315") (y #t)))

(define-public crate-rose_bloom-0.1.3 (c (n "rose_bloom") (v "0.1.3") (h "020ggs1g1k5fmfc5gbibhil31h08i5wga0w8cfzsz41s8s2lmy5l") (y #t)))

(define-public crate-rose_bloom-0.1.4 (c (n "rose_bloom") (v "0.1.4") (h "1wdwvbdv16l6x361yaajjpm5pkznjf55l1vx12rfr6cqlic3lxwx")))

(define-public crate-rose_bloom-0.1.5 (c (n "rose_bloom") (v "0.1.5") (h "0isgp83qqsllfzc9wmv4zr0vgck13262sflc8s9wg4pg8b9d6n9a")))

