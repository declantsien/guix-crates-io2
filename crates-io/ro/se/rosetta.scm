(define-module (crates-io ro se rosetta) #:use-module (crates-io))

(define-public crate-rosetta-0.1.0 (c (n "rosetta") (v "0.1.0") (d (list (d (n "actix-web") (r "^0.7.19") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1cdnjwq98y5k92ppq6nsirfg3lsmfyj0805y85sg7zzqs87kdwc3")))

