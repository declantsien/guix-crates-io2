(define-module (crates-io ro se rosetta-cli) #:use-module (crates-io))

(define-public crate-rosetta-cli-0.1.0 (c (n "rosetta-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "rosetta-client") (r "^0.1.0") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (f (quote ("h1-client-rustls"))) (k 0)))) (h "0kcdh5a2cfbph3p3kbx5x43b22cjqgiharyx956vf8fvkhxxh4vx")))

(define-public crate-rosetta-cli-0.4.0 (c (n "rosetta-cli") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "rosetta-client") (r "^0.4.0") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (f (quote ("h1-client-rustls"))) (k 0)))) (h "1n8bj2xx56m1bd06a772gmni5j5qm4amazkij2z0jrrws8kn2b5r")))

