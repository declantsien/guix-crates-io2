(define-module (crates-io ro do rodo_tui) #:use-module (crates-io))

(define-public crate-rodo_tui-0.1.0 (c (n "rodo_tui") (v "0.1.0") (d (list (d (n "rodo_lib") (r "^0.1.0") (d #t) (k 0)))) (h "0m8y0769wj8z9w2hr8as3wqmmqfbldhljf3v3s8vnky3pqkwnyzq")))

(define-public crate-rodo_tui-0.1.1 (c (n "rodo_tui") (v "0.1.1") (d (list (d (n "rodo_lib") (r "^0.1.0") (d #t) (k 0)))) (h "0c6vrb5cqj97kf30ldw66hxyp4j92vwvdc9mvllzn39m24mhw9ml")))

