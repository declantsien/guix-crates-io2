(define-module (crates-io ro do rodo_lib) #:use-module (crates-io))

(define-public crate-rodo_lib-0.1.0 (c (n "rodo_lib") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "1ch5dmynn82h51cqimms7mb0jimfv9nmw108rhkz5r9hd5vjn8yy")))

(define-public crate-rodo_lib-0.1.1 (c (n "rodo_lib") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "07jj5mz3xbaf4nxn4qid6zqxs50rp2l606hbbbn94xz29v9hpmsl")))

(define-public crate-rodo_lib-0.1.2 (c (n "rodo_lib") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.3.2") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "1qjbvi61bm516z7spkjibqj1w0flqa2zj0yjfapjcw03aqs4vlqf")))

