(define-module (crates-io ro wr rowrap-rs) #:use-module (crates-io))

(define-public crate-rowrap-rs-0.1.0 (c (n "rowrap-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00s156xqphbwq4f5z9aijpqsb52hixw0vkfshiz49px7dyc64mxh") (y #t)))

(define-public crate-rowrap-rs-0.0.1 (c (n "rowrap-rs") (v "0.0.1") (h "1qm2pixyv24f1j2s6yqkk57q795sjxjxdsdqlh472d8izdc98s9s") (y #t)))

(define-public crate-rowrap-rs-0.0.0 (c (n "rowrap-rs") (v "0.0.0") (h "1b9p7xjx1s8wgs61b8jqihqvl1m5h5gf0as62cw9hi0aqzxxlaq7") (y #t)))

(define-public crate-rowrap-rs-0.0.0-moved (c (n "rowrap-rs") (v "0.0.0-moved") (h "014a53vhlf8gbvq9f2ajjr7dshdbjxw0brb2wrgjmsglh59ik94r") (y #t)))

