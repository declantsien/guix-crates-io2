(define-module (crates-io ro wr rowrap) #:use-module (crates-io))

(define-public crate-rowrap-0.0.0 (c (n "rowrap") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rw8jvylbss2hvh4irm15bkxwsbhjxjxwsyx16cr8qp9r0kvjrjm")))

