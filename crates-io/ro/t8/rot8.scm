(define-module (crates-io ro t8 rot8) #:use-module (crates-io))

(define-public crate-rot8-0.1.1 (c (n "rot8") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "003qsckwgbhhjpsfx1rcb3d3a03viaf0rnbi2v0wnkh90m3ypxc7")))

(define-public crate-rot8-0.1.2 (c (n "rot8") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yq948mr46n5a07m57k9642ipwal0zdyiiiwi9grcfplw7cwq5rn")))

(define-public crate-rot8-0.1.3 (c (n "rot8") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15mhszk9qy2q49dpab4p0d9d4aph61yshaxjf02mhdx07n9qpnmh")))

(define-public crate-rot8-0.1.4 (c (n "rot8") (v "0.1.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m5kzpqq9pgc19lbnh20iaq654lzlmc1m5fc9f73w2vpwqdiw1qf")))

(define-public crate-rot8-0.1.5 (c (n "rot8") (v "0.1.5") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "088h57ppch3i3gqnad67fimz24js0mjn8v1pb85ysl1gic130q2i")))

(define-public crate-rot8-1.0.0 (c (n "rot8") (v "1.0.0") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.31.0") (d #t) (k 0)) (d (n "wayland-protocols-wlr") (r "^0.2.0") (f (quote ("client"))) (d #t) (k 0)))) (h "1bvb87sr9pkf6sj5ghgmga4nrp5kwiqnllzi672da5vs915xh8li")))

