(define-module (crates-io ro om room) #:use-module (crates-io))

(define-public crate-room-0.1.0 (c (n "room") (v "0.1.0") (d (list (d (n "screenshots") (r "^0.5.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("image"))) (d #t) (k 0)))) (h "0zzvv9y0m82q5w1k1p2knxrfj0j2xp0qhbjnvg7mg4flqb4hx078") (y #t)))

(define-public crate-room-0.2.0 (c (n "room") (v "0.2.0") (d (list (d (n "screenshots") (r "^0.5.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("image"))) (d #t) (k 0)))) (h "1ghm53zzrv4ny3zjqlls3m75y0kbcpas6nd6f4ksl04883i6jk3g") (y #t)))

