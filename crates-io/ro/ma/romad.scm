(define-module (crates-io ro ma romad) #:use-module (crates-io))

(define-public crate-romad-0.1.0 (c (n "romad") (v "0.1.0") (h "0vkgcnvms095v256045wkgagb30lilhsiv77v4iai277hy2p9vmp")))

(define-public crate-romad-0.1.0-0 (c (n "romad") (v "0.1.0-0") (h "0209fai5rk1adkcb6rjvhx5hcdyw3wxqirjicrfwx53ydd980f3g")))

(define-public crate-romad-0.1.0-1 (c (n "romad") (v "0.1.0-1") (h "1i696lvk9dkpbbilnjzspf9345wyb7dvk88kjmy4ls2s33lvrasy")))

(define-public crate-romad-0.2.0 (c (n "romad") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (d #t) (k 2)))) (h "0z6wb3n465j6nmkkbyla5ajq21jikq2n2iprbf72nsf93qqinbs3")))

