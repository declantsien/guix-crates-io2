(define-module (crates-io ro ma roman_encoder) #:use-module (crates-io))

(define-public crate-roman_encoder-1.0.2 (c (n "roman_encoder") (v "1.0.2") (h "070hw9c7c0wgkspqa04cwffwdvvapc6xvv2mrjam85glhfqxnn7v")))

(define-public crate-roman_encoder-1.1.0 (c (n "roman_encoder") (v "1.1.0") (h "0m0bgz56vbl0707sqn9nlb18m20l1q95lw525d77hybz59376rlv")))

