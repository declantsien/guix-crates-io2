(define-module (crates-io ro ma roman-literals) #:use-module (crates-io))

(define-public crate-roman-literals-0.1.0 (c (n "roman-literals") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "173jp8nsjqv0qc5fn3wyvkfpzqz8d2f1g6mhjah448hj0ficpxbq")))

(define-public crate-roman-literals-0.2.0 (c (n "roman-literals") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "0nsara4jcynmsi8299cjzpwib34z9liwbk3ncbrvksm0sn71viwy")))

(define-public crate-roman-literals-0.2.1 (c (n "roman-literals") (v "0.2.1") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "1hacrgffifvzigly4p3zih4k8d1s1d4vz2rci5pdcn6ybn7zdyi2")))

