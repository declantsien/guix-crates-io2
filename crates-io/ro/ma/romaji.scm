(define-module (crates-io ro ma romaji) #:use-module (crates-io))

(define-public crate-romaji-0.1.0 (c (n "romaji") (v "0.1.0") (h "16bjb0zq2dsbm3biccf7rzqbmsh7r7qzjlgvcbywqw3vyvka89ps")))

(define-public crate-romaji-0.1.1 (c (n "romaji") (v "0.1.1") (h "1y96npzpksd1404xgirvqib20360l1nl1znfjckgxx8mhclh9ag8")))

