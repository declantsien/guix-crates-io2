(define-module (crates-io ro ma roman) #:use-module (crates-io))

(define-public crate-roman-0.0.1 (c (n "roman") (v "0.0.1") (h "1fpnc01lzzhy07jvmifwsdivhj6g5qc0nf7w6l940ql304p4xf24")))

(define-public crate-roman-0.0.2 (c (n "roman") (v "0.0.2") (h "1fdpc7chh1pxnq6q7d008yvll83jfz8ws7q67wd7q9rcs7vj8jdg")))

(define-public crate-roman-0.0.3 (c (n "roman") (v "0.0.3") (h "0z81illbal616lhmp1b1fr0blq4jdr3gmfk4fps6i3cxg61fjd8k")))

(define-public crate-roman-0.0.4 (c (n "roman") (v "0.0.4") (h "0dsps0mqqd1c589f99k65n5gahvwp0lv5q6klmias627fgdxkkbq")))

(define-public crate-roman-0.1.0 (c (n "roman") (v "0.1.0") (h "0z4aqx9whkd9mb5hh7y9fqcdk569s0iiqvngh6ikjzbdbdj923bp")))

(define-public crate-roman-0.1.1 (c (n "roman") (v "0.1.1") (h "1cpkf2a88s5ifyzl0a153ny6is82mb34v7rii7ixlvj6dkywj6g5")))

(define-public crate-roman-0.1.2 (c (n "roman") (v "0.1.2") (h "02gx1awhpd1rd4gvqysydvj0yskd9ijy9l80zmycfii1r9lmmr30")))

(define-public crate-roman-0.1.3 (c (n "roman") (v "0.1.3") (h "0i38bd4799vy5x8nm2p411q6jzlk8bzgvlar7p68jrgf0zmqkfxh")))

(define-public crate-roman-0.1.4 (c (n "roman") (v "0.1.4") (h "1d6d4avfi2b6ihfw44z869bvyfl27sgl8mbarispv3zsqvgh7m86")))

(define-public crate-roman-0.1.5 (c (n "roman") (v "0.1.5") (h "0k75359ydgqv44zygpjigdxrbp0s28k2w8xc9pv2kc6a3g0i66q7")))

(define-public crate-roman-0.1.6 (c (n "roman") (v "0.1.6") (h "16wdsmkw9qzsd16lrfw56yz0lbff9gyi160mjggj9bi7r3q47ifj")))

