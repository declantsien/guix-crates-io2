(define-module (crates-io ro ma romantic) #:use-module (crates-io))

(define-public crate-romantic-0.1.0 (c (n "romantic") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0mx5fpxjx6rgm4w1phbazj8gy866nad7sj8ky78k21cz6q85sp9p")))

(define-public crate-romantic-0.1.1 (c (n "romantic") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "1r7w627kd2cawi5mnnzbswfjhzyj92q2wsplvr0q1wmis24072ya")))

(define-public crate-romantic-0.1.2 (c (n "romantic") (v "0.1.2") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "19rhczpjmrgcbcsvdcmj6k06dva9gara45jipfwqc93xv4yhv92g")))

