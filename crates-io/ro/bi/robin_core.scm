(define-module (crates-io ro bi robin_core) #:use-module (crates-io))

(define-public crate-robin_core-0.1.0 (c (n "robin_core") (v "0.1.0") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0l0cc7q792rkpxxnrq2rvwisr3i0mvbhqn1z1ji8wfzvdbpql6c4")))

(define-public crate-robin_core-0.2.0 (c (n "robin_core") (v "0.2.0") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0k8jm7g1gvbqp51kph4sbjgylsf6dbja16sg5ayzx177wy1y63ij")))

(define-public crate-robin_core-0.2.1 (c (n "robin_core") (v "0.2.1") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0xywsvkk6h42nh17ql0wqv722cz0jfmn3iixsr6izxbd29qy8yl2")))

(define-public crate-robin_core-0.2.3 (c (n "robin_core") (v "0.2.3") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0c1yqzna73wpgqfr4nvw0ppmcjmynllnw73yzzhm1mbkgd7g9k4a")))

(define-public crate-robin_core-0.2.4 (c (n "robin_core") (v "0.2.4") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "18b6dwrf82hqr7hqwdpfzl33m7dfz8isbyqgwpsjb2rfmwnr46wh")))

(define-public crate-robin_core-0.2.5 (c (n "robin_core") (v "0.2.5") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0wx1f8v0dxxsffknlbnvd68bnc95w675lmhaafh02fqbri9jvq5s")))

(define-public crate-robin_core-0.2.6 (c (n "robin_core") (v "0.2.6") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1cj2myad212fnw9hry4v2fhjwqjvfsgj6x9dpf5h8q8gdgcc777r")))

(define-public crate-robin_core-0.2.7 (c (n "robin_core") (v "0.2.7") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0wciml2g93df763mbgr3vj9qgkh37fhrmcdvnmafhxq7pv0xxr70")))

(define-public crate-robin_core-0.2.8 (c (n "robin_core") (v "0.2.8") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "16wizc0d0i8gx1g4zmf9sx2r3fdy591s23jm9nbqwrxczfxp1w8d")))

(define-public crate-robin_core-0.2.9 (c (n "robin_core") (v "0.2.9") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0nhc0vybjcsr8gn53wgxaq9lkahjgmyvd961hqdcahgpy4vzyxv9")))

(define-public crate-robin_core-0.2.10 (c (n "robin_core") (v "0.2.10") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "14dxqmcpimg49b0n0gaar4d7pl3ijzk4a97cb783mdkfz9b4k43h")))

(define-public crate-robin_core-0.2.11 (c (n "robin_core") (v "0.2.11") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "01fy8c07dr9564hi7bl8iw5q953lmf9m89wii2gzisq6s44jzass")))

(define-public crate-robin_core-0.2.12 (c (n "robin_core") (v "0.2.12") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "19rvd33lrwf1bqvnwl2f7c40ddhhi0ddbhr4li1wrv17kmrhfybb")))

(define-public crate-robin_core-0.2.13 (c (n "robin_core") (v "0.2.13") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0mgkxxblaxjw9bjlrkf3wfj8pjy1cz91yk32vvjrsk7yjniv1f9v")))

(define-public crate-robin_core-0.2.14 (c (n "robin_core") (v "0.2.14") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "017svcwgzaasnnn3jdcwccqaa6rx6g4prki8f9za2pzy9bsn7zck")))

(define-public crate-robin_core-0.2.15 (c (n "robin_core") (v "0.2.15") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1c22hdbw3m70gdxxjw10dqw1pq81qcgz86dpm4hnixd9808qpf4c")))

(define-public crate-robin_core-0.2.16 (c (n "robin_core") (v "0.2.16") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0mvdmxdm22zwknczn557r8qc8khax35q71w71jyvgb1wsqjmznrz")))

(define-public crate-robin_core-0.2.17 (c (n "robin_core") (v "0.2.17") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1xg0sgj6xqmv7p8zaalxds9mxg957diwwva2qngm97jj6m1wcmjk")))

(define-public crate-robin_core-0.2.18 (c (n "robin_core") (v "0.2.18") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1g8yph9v537kgj63nbrk03k1ppm6i96iaxlqaw6psgcxs2s96d0l")))

(define-public crate-robin_core-0.3.0 (c (n "robin_core") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "15lpgjn8b21rrzvwwj7kngv8c5wk8mrvmxspgicay8qscrafln15")))

(define-public crate-robin_core-0.3.1 (c (n "robin_core") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1ad2fkd4ws9aap3qxwc8vjcd69hls23k7dmzjslbzasvjlikqlk1")))

(define-public crate-robin_core-0.3.2 (c (n "robin_core") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1hgw6sb4mqbwq72ikc0g7gim81dcavkx19lmgdqff0r0vbbk6g61")))

(define-public crate-robin_core-0.3.3 (c (n "robin_core") (v "0.3.3") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1s965r9r5m6h9f88sfhrpbc99vv8v4h02skr37j98wpxn4d62kqv")))

(define-public crate-robin_core-0.3.4 (c (n "robin_core") (v "0.3.4") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1scxg42m335074w9i2ibyhlxnvjihkns879gqmp2sib3d9sz3s57")))

(define-public crate-robin_core-0.3.5 (c (n "robin_core") (v "0.3.5") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0zai5dp8f19h6lsginapw9l5nqg4cfsd3m86z8mvb334j8scr5pk")))

(define-public crate-robin_core-0.3.6 (c (n "robin_core") (v "0.3.6") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1z8n839sdas3gg3100jjh6sigi7bz7zqmvcx6hgs4zbh0wsh8ax0")))

(define-public crate-robin_core-0.3.7 (c (n "robin_core") (v "0.3.7") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0k3f9c5f4s1vy893sy62m41cdnhz2775vhmqzxj5f4d977xi1pys")))

(define-public crate-robin_core-0.3.8 (c (n "robin_core") (v "0.3.8") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1m3mhbg14wqqhv92qg1632iqb0wnyq10h8hrxgqa196m83fyq3j6")))

(define-public crate-robin_core-0.3.9 (c (n "robin_core") (v "0.3.9") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0rqvbgdjdxs2k7vl2yjl0c0q0mzyk6sl5x4bml1pidxmzksw49qj")))

(define-public crate-robin_core-0.4.0 (c (n "robin_core") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0gaspq2zvl033vwgc91iaz1cwasyfhivd122wcln62627wq17yy5")))

(define-public crate-robin_core-0.4.1 (c (n "robin_core") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0f5mgmv5g11089yc0a1kq6jwqlz5k5wlcks8n97g7v08nss4ais5")))

(define-public crate-robin_core-0.4.2 (c (n "robin_core") (v "0.4.2") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1zj6m0mnd5g35m00k6acffx0cb2fpjllr5d9bv6av0ck2mfsnq6a")))

(define-public crate-robin_core-0.4.3 (c (n "robin_core") (v "0.4.3") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1a4gcxvv8zib1gz4k463mfav50ync59gqgb1nhiv43224kl6cyh9")))

(define-public crate-robin_core-0.4.4 (c (n "robin_core") (v "0.4.4") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0pfk6xkjymn534dnjkmsxpmdpxzc59xlr9xsi3hc4c2f9jrjjnmy")))

(define-public crate-robin_core-0.4.5 (c (n "robin_core") (v "0.4.5") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1h2wbc36rx27sjra95lwb292a3mryh9j5w8d01g8m592f2rpv360")))

(define-public crate-robin_core-0.4.6 (c (n "robin_core") (v "0.4.6") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0gnzzvpkwk66w3k062pyv6alxrv10iyv8viivpqxmcwwkn7zdsd7")))

(define-public crate-robin_core-0.4.7 (c (n "robin_core") (v "0.4.7") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0ii1zwdjc0wb4h1r988w24bjif8ymfy0hzy1gqj4g5dp6cmw6xn8")))

(define-public crate-robin_core-0.4.8 (c (n "robin_core") (v "0.4.8") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0wy6c788mfa2xp0x336s9n28ylmif22ynd5alycvwb7xwnp6kh1v")))

(define-public crate-robin_core-0.4.9 (c (n "robin_core") (v "0.4.9") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "00wl4581bf1r74457ajcz8diibn7j5rdpzgmv4q6afcf8fxc1c9n")))

(define-public crate-robin_core-0.4.10 (c (n "robin_core") (v "0.4.10") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "1rdp2xa7vl1yw0wpf8w113gna3v3z98nkpwrm6qjjk8gn3sjavvb")))

(define-public crate-robin_core-0.4.11 (c (n "robin_core") (v "0.4.11") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0kndr0kmhp1rk204yjxa4khd3fcldkqadlh10pkqzn5fian2g6j5")))

(define-public crate-robin_core-0.4.12 (c (n "robin_core") (v "0.4.12") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "048yhyh9sygkmqih28dh1rvcfk5m67j5vk7r0lhdks049vw7j443")))

(define-public crate-robin_core-0.4.13 (c (n "robin_core") (v "0.4.13") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta") (d #t) (k 0)))) (h "11c5v2ylq2s67h0zh416856iy0k7h6ryabpdbrn96riw12bbflvq")))

