(define-module (crates-io ro bi robius-use-makepad) #:use-module (crates-io))

(define-public crate-robius-use-makepad-0.1.0 (c (n "robius-use-makepad") (v "0.1.0") (d (list (d (n "robius-android-env") (r "^0.1.0") (o #t) (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1i49bxkpszifhnikjnf98993xs9zs4qji6s0iagwv20w27209i0x") (f (quote (("default" "robius-android-env/makepad"))))))

