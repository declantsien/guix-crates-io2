(define-module (crates-io ro bi robin_cli) #:use-module (crates-io))

(define-public crate-robin_cli-0.1.0 (c (n "robin_cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "robin_cli_core") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "129a6gi4ff4f842yw4x00088rspfyvsa5706ywb9m59h9byav4q2")))

(define-public crate-robin_cli-0.1.1 (c (n "robin_cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "robin_cli_core") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "0imy3xrw9pjnc13gsnv2bvyqwwqr1iv92hqil1icb6c61s7fp8v9")))

