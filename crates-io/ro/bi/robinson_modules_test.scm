(define-module (crates-io ro bi robinson_modules_test) #:use-module (crates-io))

(define-public crate-robinson_modules_test-0.1.0 (c (n "robinson_modules_test") (v "0.1.0") (d (list (d (n "avl") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0v3mf3kc4dj6csynkp5v54lk83y5y44sgprg1pnvwal9mnjcr3z3") (y #t)))

(define-public crate-robinson_modules_test-0.2.0 (c (n "robinson_modules_test") (v "0.2.0") (d (list (d (n "avl") (r "^0.7.1") (d #t) (k 0)))) (h "1yqd6yychp8dv8n4n5j1ydh1cppppi8zx1x4q9dwgcq51p8yzgz5") (y #t)))

(define-public crate-robinson_modules_test-0.3.0 (c (n "robinson_modules_test") (v "0.3.0") (d (list (d (n "avl") (r "^0.7.1") (d #t) (k 0)))) (h "1j9p96alzqqspkr2d5wmcxcdiq814p1xgsmaw37kpvnw79x3h7hc") (y #t)))

(define-public crate-robinson_modules_test-0.4.0 (c (n "robinson_modules_test") (v "0.4.0") (d (list (d (n "avl") (r "^0.7.1") (d #t) (k 0)))) (h "15wysd8nqwfhcgjdci6fsdqqq4lnrhl1mzqs1bzssa15y77vmqn9") (y #t)))

(define-public crate-robinson_modules_test-0.5.0 (c (n "robinson_modules_test") (v "0.5.0") (d (list (d (n "avl") (r "^0.7.1") (d #t) (k 0)))) (h "16xl4ciam1q778z3w2dlzq7naxrn9rp9p3zf8b4gb6k6f7pm0y60") (y #t)))

