(define-module (crates-io ro bi robin_merge) #:use-module (crates-io))

(define-public crate-robin_merge-0.1.0 (c (n "robin_merge") (v "0.1.0") (h "05ryvs15mifqh20p3sznvxh8i5dacbsrm34vm3j1znwnwzd4023j")))

(define-public crate-robin_merge-0.1.1 (c (n "robin_merge") (v "0.1.1") (h "0qb40qidsvbnql1j5y8k6p9yp4h479z4mgikjj08brbfnsm80c6b")))

