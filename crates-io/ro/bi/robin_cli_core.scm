(define-module (crates-io ro bi robin_cli_core) #:use-module (crates-io))

(define-public crate-robin_cli_core-0.1.0 (c (n "robin_cli_core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "pbr") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("cookies" "gzip" "brotli" "deflate" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1llv8ysj00mms8g74ma7gvvf81nmn9k0ngybb003ylgi1ba1zvks")))

