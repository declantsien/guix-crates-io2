(define-module (crates-io ro bi robinhood) #:use-module (crates-io))

(define-public crate-robinhood-0.1.0 (c (n "robinhood") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "openssl") (r "~0.9") (d #t) (t "cfg(not(any(target_os = \"windows\", target_os = \"macos\")))") (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1l79bar3088hpwkdb6gw40v06iy4j3ikrn6p0y4jai1kghdiwh5q")))

(define-public crate-robinhood-0.1.1-alpha.1 (c (n "robinhood") (v "0.1.1-alpha.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "openssl") (r "~0.9") (d #t) (t "cfg(not(any(target_os = \"windows\", target_os = \"macos\")))") (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0lr701hcraqg8jahgfzwmpyg2yky61idk3q09npxgyllxm88n9hq")))

