(define-module (crates-io ro bi robin-derives) #:use-module (crates-io))

(define-public crate-robin-derives-0.1.0 (c (n "robin-derives") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "0ba4d32hq1xpy6s0bz3vs2zcxk3cnzc4x79y95kaklp02df1kbz0")))

(define-public crate-robin-derives-0.2.0 (c (n "robin-derives") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "0awanign50gj90kfj1xi47a4rhypkx82ldxrx9bbr0j3fk9c82sn")))

(define-public crate-robin-derives-0.3.0 (c (n "robin-derives") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "13w741gd11mv0lldp4xbpkj65ki7ark23dq23n7vg79i979njani")))

