(define-module (crates-io ro id roid) #:use-module (crates-io))

(define-public crate-roid-0.1.1 (c (n "roid") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1bkga4kkzf4hnypq47hzcbs3s7jpm5xlhclmb9c5lw85mn67v3r6")))

(define-public crate-roid-0.1.2 (c (n "roid") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1znqq6zc1jq7hvjsqadd8zi74gs3k585gbph6c9g2ds2m6yx1v6h")))

