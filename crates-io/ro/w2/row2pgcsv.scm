(define-module (crates-io ro w2 row2pgcsv) #:use-module (crates-io))

(define-public crate-row2pgcsv-0.1.0 (c (n "row2pgcsv") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)))) (h "0gc6641lr47bnpkq7wm0r6hy5vbv2jbv0ls2cpahahwzbr1wvs3w") (f (quote (("default"))))))

(define-public crate-row2pgcsv-1.0.0 (c (n "row2pgcsv") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)))) (h "1iphyqc6jfnfs6nz9n68sks90pq9pqngpg3yjp0fay8p7zj41awv") (f (quote (("default"))))))

(define-public crate-row2pgcsv-1.0.1 (c (n "row2pgcsv") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)))) (h "005a683j3bdq7kk1a30nwf46wvilw2dxwms5pp8gb750bz8wz631") (f (quote (("default"))))))

