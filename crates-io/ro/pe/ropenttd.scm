(define-module (crates-io ro pe ropenttd) #:use-module (crates-io))

(define-public crate-ropenttd-0.0.2 (c (n "ropenttd") (v "0.0.2") (d (list (d (n "argopt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.5.1") (d #t) (k 0)) (d (n "subslice") (r "^0.2.2") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "1471ka10p15kfjfwgpzxj8m0nnaycjc3ri9did1bkhfr193rpyad") (f (quote (("default" "cli")))) (s 2) (e (quote (("cli" "dep:argopt" "dep:tabled"))))))

