(define-module (crates-io ro pe roped) #:use-module (crates-io))

(define-public crate-roped-0.1.0 (c (n "roped") (v "0.1.0") (d (list (d (n "bundle-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "strand-derive") (r "^0.1.0") (d #t) (k 0)))) (h "032b1dxhgkazpvk3r9s1c0kczyyizv9c4g3zarbgpkckrnx3hx14")))

(define-public crate-roped-0.2.0 (c (n "roped") (v "0.2.0") (d (list (d (n "bundle-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "strand-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1g1ckrq4k4nvl983alk08vyfb4zz4a7k2rk8fvbfb109azywlg2v")))

(define-public crate-roped-0.3.0 (c (n "roped") (v "0.3.0") (d (list (d (n "bundle-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "strand-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0wncg4x38nvggdl3jplv9rx23qkkf75nsgpz8v3i0vw3m8f6nfw5")))

(define-public crate-roped-0.4.0 (c (n "roped") (v "0.4.0") (d (list (d (n "bundle-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "strand-derive") (r "^0.3.0") (d #t) (k 0)))) (h "05vknmyibgbf7hckv4mqd84wbnjank6is65xz62wsffrbfwcfiw6")))

(define-public crate-roped-0.5.0 (c (n "roped") (v "0.5.0") (d (list (d (n "bundle-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "strand-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1z3qqmydvyc6zgkmyk7kcmzd67abn11665qy61252d6k6injd4ix")))

(define-public crate-roped-0.6.0 (c (n "roped") (v "0.6.0") (d (list (d (n "bundle-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "strand-derive") (r "^0.3.0") (d #t) (k 0)))) (h "062g8ifzhjc3z2zg9qrircsp0r1ij8c360rixw5b9mxh766bbqyr")))

(define-public crate-roped-0.7.0 (c (n "roped") (v "0.7.0") (d (list (d (n "parsr") (r "^0.2.6") (d #t) (k 0)) (d (n "strand-derive") (r "^0.4.0") (d #t) (k 0)))) (h "11k31px8m01mxmlc411mh0xqb76y0k01n8xfvsi68m73yxcc5rhn")))

(define-public crate-roped-0.7.1 (c (n "roped") (v "0.7.1") (d (list (d (n "parsr") (r "^0.2.6") (d #t) (k 0)) (d (n "strand-derive") (r "^0.4.0") (d #t) (k 0)))) (h "18ihn780qi10dyb9qkx250g4fcn6wkggar0d4hv51n2gby44k1qb")))

(define-public crate-roped-0.8.0 (c (n "roped") (v "0.8.0") (d (list (d (n "parsr") (r "^0.3.1") (d #t) (k 0)) (d (n "strand-derive") (r "^0.5.0") (d #t) (k 0)))) (h "09pw8ih8a7lynrpnv4i8ch6jlgf1ywr9k0mak6rflpbg0bdmq3n9")))

