(define-module (crates-io ro pe ropey) #:use-module (crates-io))

(define-public crate-ropey-0.1.0 (c (n "ropey") (v "0.1.0") (h "1ny1whjg2wjfcahb58lw1dhcwr4f08bcplj7pz5xngcxszrf5hyr")))

(define-public crate-ropey-0.2.0 (c (n "ropey") (v "0.2.0") (h "16h8d8pw8yl47j6nmmh1afwplimchhgydadljv1pvjx8plqchzah")))

(define-public crate-ropey-0.3.0 (c (n "ropey") (v "0.3.0") (h "12q2s1nw4ik62wd2f1xbzzjh6c7j4wp2xg16nn06dfbd34vcxyqg")))

(define-public crate-ropey-0.4.0 (c (n "ropey") (v "0.4.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "10366iyzdpwsr5g8f9xxv587qiji295iv1rpl4qnylb1ggvlx66v")))

(define-public crate-ropey-0.4.1 (c (n "ropey") (v "0.4.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "1l4k0rv7nv9lj9pccnp2iwslprmzkbdd0js4c2jfkahvlb3m66s1")))

(define-public crate-ropey-0.5.0 (c (n "ropey") (v "0.5.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "0d84dk9hwiwrixharfsl76d4i83axxa82bakfvspy58iggcpvbiz")))

(define-public crate-ropey-0.5.1 (c (n "ropey") (v "0.5.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "0zhp30alrhs4yahzc7ra77f84vi51lr3667qfj7pibhfncvws28p")))

(define-public crate-ropey-0.5.2 (c (n "ropey") (v "0.5.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "01xplafmljdzfgr6lr3ifzx87gv5pyv6vx8p0y2b69acssj2cmwq")))

(define-public crate-ropey-0.5.3 (c (n "ropey") (v "0.5.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "0ayq6n18n97ag9s5ddiiyn9v0wrm1diy72pl7b7iq3p8nfcmvssp")))

(define-public crate-ropey-0.5.4 (c (n "ropey") (v "0.5.4") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "16vskg9hszwvk5hp053xaq6znkf4pr9yr3mxlvwdq2gp8452xvsr")))

(define-public crate-ropey-0.5.5 (c (n "ropey") (v "0.5.5") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "181cpzj3i3kimsfk43jka8sp162kazlr7cgbia6k6wmwfr7vgd3f")))

(define-public crate-ropey-0.5.6 (c (n "ropey") (v "0.5.6") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "1jd2jajxpsny27g6sfpl0kviqsfzbv042r8hjibmacm8nfi9h7bn")))

(define-public crate-ropey-0.6.0 (c (n "ropey") (v "0.6.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "0zg9d4hyybmazha4wx0gjmd9s80z14zjr85xia4f864crsmz2kg8") (y #t)))

(define-public crate-ropey-0.6.1 (c (n "ropey") (v "0.6.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "10xv98vllx2hy39s0vgbxark5bama8iia2ir3b740k7n37dsgavz") (y #t)))

(define-public crate-ropey-0.6.2 (c (n "ropey") (v "0.6.2") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "1w9i1sgyah7xqz1yw70z9kx7md059chfwmq4v2gnr4d12vkrc5v6")))

(define-public crate-ropey-0.6.3 (c (n "ropey") (v "0.6.3") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "1hakpfl4w02ph9b9bgnx6vr278sv28hhpqwpvjp8hiir56fcw23c")))

(define-public crate-ropey-0.7.0 (c (n "ropey") (v "0.7.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 2)))) (h "1pixwxyypfv5s1l4a546vqmyn8cfw5g5q9nfaqsb5h0399d5j9gq")))

(define-public crate-ropey-0.7.1 (c (n "ropey") (v "0.7.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 2)))) (h "1z0xfgqp89zky12m9fh1z89gaja77ffy9gpid6nlcj3k9fmvny9p")))

(define-public crate-ropey-0.8.0 (c (n "ropey") (v "0.8.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 2)))) (h "0zi4rh84kgmpfqsbiiqy350czfa7zva4iqsspidqmh4rsx6vwpkk")))

(define-public crate-ropey-0.8.1 (c (n "ropey") (v "0.8.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 2)))) (h "0bz2iw0hqlcqihk2affwqlgq48dxmyi1iqn23l243l9ik3ap4dfi")))

(define-public crate-ropey-0.8.2 (c (n "ropey") (v "0.8.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 2)))) (h "1qb4qvi2ch3lqk7sdjxsbr35j0s7g80s24b1hr5dqgbvy7f7fhmh")))

(define-public crate-ropey-0.8.3 (c (n "ropey") (v "0.8.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 2)))) (h "04yvn9sdigzkdwmhphirkhqv1f86y5xp5gjfmz45m260snvsy1af")))

(define-public crate-ropey-0.8.4 (c (n "ropey") (v "0.8.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 2)))) (h "1p94xifz5rkf1c17svs74k6h0i0a722fh5bw7gf7dwvlnx4r3si2")))

(define-public crate-ropey-0.9.0 (c (n "ropey") (v "0.9.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 2)))) (h "15djrggwz0bh9p4cw7a35v4if9abw0fz3vrw068vwp4g4zm50n7j")))

(define-public crate-ropey-0.9.1 (c (n "ropey") (v "0.9.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 2)))) (h "08fzs0kdi93dfb5avw662fggw6jkdm55hjz0i2n5sk1sn0cmhnn5")))

(define-public crate-ropey-0.9.2 (c (n "ropey") (v "0.9.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 2)))) (h "0rccb87zg65r3hv3qspvmmqql81f9803c7dffcq3c5zkffayrwi5")))

(define-public crate-ropey-1.0.0 (c (n "ropey") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 2)))) (h "10qf20dzjmx1bl5zpwvn8xfg20fbryrympvm4lr33dycb9fh49yi")))

(define-public crate-ropey-1.0.1 (c (n "ropey") (v "1.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 2)))) (h "1z2r8fgamcp7v8xi9rq9hq49lz2j9238364r8hfy5lfc5hy0bpd0")))

(define-public crate-ropey-1.1.0 (c (n "ropey") (v "1.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "0wbz3cj314bfxl8xwbspyfji6ql95nm360r6gdzd9bd4122nlcms")))

(define-public crate-ropey-1.2.0 (c (n "ropey") (v "1.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "10qsj7m6hz953ar68q7iqwwizrh89jaclgffzglb7nwzb0bfzwzh")))

(define-public crate-ropey-1.3.0 (c (n "ropey") (v "1.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "1cpvflizvxlw3bfnxzm0f1fsv88av0r98chi0ynbidigv1m9kcg4")))

(define-public crate-ropey-1.3.1 (c (n "ropey") (v "1.3.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "0a3zjhbspghszgbfkra67q1x3g3qlrqg128827nj0nxjvvvayl4i")))

(define-public crate-ropey-1.3.2 (c (n "ropey") (v "1.3.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "1hzyq8y5nzcfp74j0gi1a222p8xamyhv8n3igk9hiwyrpijsmfg6")))

(define-public crate-ropey-1.4.0 (c (n "ropey") (v "1.4.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "str_indices") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "1lq4qfvrmcsw7gr3g1pndn99fzwbpjdwz9rirrmf7k0nqijhqv2z") (f (quote (("unicode_lines" "cr_lines") ("default" "unicode_lines") ("cr_lines")))) (y #t)))

(define-public crate-ropey-1.4.1 (c (n "ropey") (v "1.4.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "str_indices") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "0cwnblhxfj2v3h5skvcnxca406n8jv8vgdq0shrjn41adsrdj3gs") (f (quote (("unicode_lines" "cr_lines") ("default" "unicode_lines") ("cr_lines"))))))

(define-public crate-ropey-1.5.0 (c (n "ropey") (v "1.5.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "str_indices") (r "^0.4.0") (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "0r00vmw0w9116mx8379psa06lwhp7i7hdnm5ih9l5z7yz8wj5lmv") (f (quote (("unicode_lines" "cr_lines") ("simd" "str_indices/simd") ("default" "unicode_lines" "simd") ("cr_lines"))))))

(define-public crate-ropey-1.5.1-alpha (c (n "ropey") (v "1.5.1-alpha") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "str_indices") (r "^0.4.0") (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "1cq9fix7jd8nsk04i7xl7bhb1lmaxwy4n5hkvn9694p8vv064zli") (f (quote (("unicode_lines" "cr_lines") ("small_chunks") ("simd" "str_indices/simd") ("default" "unicode_lines" "simd") ("cr_lines"))))))

(define-public crate-ropey-1.5.1 (c (n "ropey") (v "1.5.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "str_indices") (r "^0.4.0") (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "13jcjqhnjwyflq7fza96r89xzxc4q6w4qsbmya1kwq95an8k5y54") (f (quote (("unicode_lines" "cr_lines") ("small_chunks") ("simd" "str_indices/simd") ("default" "unicode_lines" "simd") ("cr_lines"))))))

(define-public crate-ropey-1.6.0 (c (n "ropey") (v "1.6.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "str_indices") (r "^0.4.0") (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "1vbiqqh6zyqimfy2945lnixi8c8v4n05lg73cvb50bm38cn7mkjk") (f (quote (("unicode_lines" "cr_lines") ("small_chunks") ("simd" "str_indices/simd") ("default" "unicode_lines" "simd") ("cr_lines"))))))

(define-public crate-ropey-1.6.1 (c (n "ropey") (v "1.6.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "str_indices") (r "^0.4") (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "1dckf3likfi1my2ilqwhq2ifsm9iq8cayg6ws7fpa6nd1d11whck") (f (quote (("unicode_lines" "cr_lines") ("small_chunks") ("simd" "str_indices/simd") ("default" "unicode_lines" "simd") ("cr_lines"))))))

