(define-module (crates-io ro pe rope_rd) #:use-module (crates-io))

(define-public crate-rope_rd-0.1.0 (c (n "rope_rd") (v "0.1.0") (d (list (d (n "cargo-release") (r "^0.24.11") (d #t) (k 2)))) (h "1gr2jqkayzkkjgq2v0in7ianiryvybi0kzv158fr9fc1vsd1zdv8")))

(define-public crate-rope_rd-0.1.1 (c (n "rope_rd") (v "0.1.1") (d (list (d (n "cargo-release") (r "^0.24.11") (d #t) (k 2)))) (h "0r8xbv6dwck62hw2a69x09rczswcr1aa9dlrcinhlq780csp8hm8")))

(define-public crate-rope_rd-0.1.2 (c (n "rope_rd") (v "0.1.2") (d (list (d (n "cargo-release") (r "^0.24.11") (d #t) (k 2)))) (h "12sm8x87rqmwiydgagbyhifaabfr468hz1qmha4fly0yz2cdbfzi")))

(define-public crate-rope_rd-0.2.0 (c (n "rope_rd") (v "0.2.0") (d (list (d (n "cargo-release") (r "^0.24.11") (d #t) (k 2)))) (h "0xp1lmx75yx89xl1vhip6cz6ffkgp8df9mb5gqvz3hdjgn8f7csv")))

(define-public crate-rope_rd-0.3.0 (c (n "rope_rd") (v "0.3.0") (d (list (d (n "cargo-release") (r "^0.24.11") (d #t) (k 2)))) (h "03sjf5snz3sn59qm575bwm6pi7mqa1k9s3px85lszdpz81dvqwp7")))

(define-public crate-rope_rd-0.4.0 (c (n "rope_rd") (v "0.4.0") (d (list (d (n "cargo-release") (r "^0.24.11") (d #t) (k 2)))) (h "1jhb2jh7pf4v9gy3r5d75fxyb91ww2zsifa776l9bakzq8ch5aal")))

