(define-module (crates-io ro su rosu-storyboard) #:use-module (crates-io))

(define-public crate-rosu-storyboard-0.1.0 (c (n "rosu-storyboard") (v "0.1.0") (d (list (d (n "rosu-map") (r "^0.1.0") (d #t) (k 0)) (d (n "rosu-map") (r "^0.1.0") (f (quote ("tracing"))) (d #t) (k 2)) (d (n "test-log") (r "^0.2.14") (f (quote ("trace"))) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "15jd2l76r28rk5ms6ll7q4577zxa9n7n75pkkmxp4vrm95gx5d4l")))

(define-public crate-rosu-storyboard-0.1.1 (c (n "rosu-storyboard") (v "0.1.1") (d (list (d (n "rosu-map") (r "^0.1.1") (d #t) (k 0)) (d (n "rosu-map") (r "^0.1.0") (f (quote ("tracing"))) (d #t) (k 2)) (d (n "test-log") (r "^0.2.14") (f (quote ("trace"))) (k 2)))) (h "0602yh1l8fg71prl2j2myy6bsih698j40gspdk9nx5p79i90rin1")))

