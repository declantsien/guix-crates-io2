(define-module (crates-io ro su rosu-map) #:use-module (crates-io))

(define-public crate-rosu-map-0.1.0 (c (n "rosu-map") (v "0.1.0") (d (list (d (n "test-log") (r "^0.2.14") (f (quote ("trace"))) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (k 0)))) (h "086z59pd2bqcymjq90ffh9mql96qp1i85lcb2hrf8rinxdhxfwci")))

(define-public crate-rosu-map-0.1.1 (c (n "rosu-map") (v "0.1.1") (d (list (d (n "test-log") (r "^0.2.14") (f (quote ("trace"))) (k 2)) (d (n "tracing") (r "^0.1.40") (o #t) (k 0)))) (h "1wx4cwj91rilmcr2blrx8j342b1s12kgg5my5yqivv8gixn94m9w")))

