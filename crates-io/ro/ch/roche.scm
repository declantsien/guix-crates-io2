(define-module (crates-io ro ch roche) #:use-module (crates-io))

(define-public crate-roche-0.1.0 (c (n "roche") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo-generate") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.5.3") (d #t) (k 2)))) (h "1cc3a9xgyvi0g48i994m5k1b3i2d7chin2n3ahmshswjijkyy8zv")))

(define-public crate-roche-0.2.0 (c (n "roche") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo-generate") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.5.3") (d #t) (k 2)))) (h "16142yadh214706splx7ff9w4lmdkfll11waqj1iyvhkkk6yrz1k")))

(define-public crate-roche-0.3.0 (c (n "roche") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo-generate") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.5.3") (d #t) (k 2)))) (h "0zdhilhjyh2fc5wg8ndj1cvz745xx3ycvxnw92k755pp3zwa3xyk")))

(define-public crate-roche-0.3.1 (c (n "roche") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo-generate") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.5.3") (d #t) (k 2)))) (h "0cm7hsjclzx2xwajyq7fzwn7fvkpadfyl4lzssyvvair86fy7n32")))

(define-public crate-roche-0.3.2 (c (n "roche") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo-generate") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.5.3") (d #t) (k 2)))) (h "1sbz6j5na62wlc4pslb3mxj5gch1nj1wqssg5bfwga894m1kv1h6")))

(define-public crate-roche-0.3.3 (c (n "roche") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo-generate") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.5.3") (d #t) (k 2)))) (h "02i4vzv8l1dqaxx82db7nj5w2hb1i87b30rm79p32zb6vd9ciy54")))

