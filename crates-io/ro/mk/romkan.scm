(define-module (crates-io ro mk romkan) #:use-module (crates-io))

(define-public crate-romkan-0.2.1 (c (n "romkan") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1crc8p5p8q8i4dw2p75pzxb1njkqd10qdfc6w14r4w0rwjrffrjj")))

(define-public crate-romkan-0.2.2 (c (n "romkan") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0bdn24c5siac03k5l13nhcg6hq37v7xls7fgbjanc1nxcvl6x51m")))

