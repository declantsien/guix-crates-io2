(define-module (crates-io ro sa rosa_parse) #:use-module (crates-io))

(define-public crate-rosa_parse-0.1.0 (c (n "rosa_parse") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "1cbw51ynfyngc5yr5mjn86c749bp9ghf8i5xhfmvh6hfm5mnmydv")))

(define-public crate-rosa_parse-0.1.1 (c (n "rosa_parse") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "1j4ff0kmyp4jvlxcdz8inihwlqm6cnqifvp6bdq0lha9pa3816x3")))

(define-public crate-rosa_parse-0.2.0 (c (n "rosa_parse") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "1f9dcq230n6idjd6frziszp86rvp2m64gcsan6k8msgfknw43b6p")))

