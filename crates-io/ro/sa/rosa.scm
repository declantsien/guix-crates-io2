(define-module (crates-io ro sa rosa) #:use-module (crates-io))

(define-public crate-rosa-0.0.1 (c (n "rosa") (v "0.0.1") (h "057syw3zym7cs8hnyz5d84hw8z96dsnzh0n9qczdyjqzfhjwwhv2") (y #t)))

(define-public crate-rosa-0.0.5 (c (n "rosa") (v "0.0.5") (h "1xw7y3jkbqlkxmdzw2cvwy90pa8gij8c2lj6jq7c66mm0nkngpg2") (y #t)))

(define-public crate-rosa-0.0.7 (c (n "rosa") (v "0.0.7") (h "01n24zy36aymvayh0x10pr7ijxqkl0g3my8scx9ig27dkihd458l") (y #t)))

(define-public crate-rosa-0.1.1 (c (n "rosa") (v "0.1.1") (h "13hn3fcvsvhmq02aw27pn08m9ycrvpqb5b5ykvrrnikjsrscw45a") (y #t)))

(define-public crate-rosa-0.1.3 (c (n "rosa") (v "0.1.3") (h "0w6x2aqqgayd65l4vlixng2969c8cav5as5fn82mrlyja9lh9pag") (y #t)))

(define-public crate-rosa-0.1.4 (c (n "rosa") (v "0.1.4") (h "1angw5ivv64fmhfcir5hpc7h4yli6aj8z7g72hav51sw4bpz10wv") (y #t)))

(define-public crate-rosa-0.1.77 (c (n "rosa") (v "0.1.77") (h "0l6a6fax98s92jys0fy7adcl5z8mp34z0xrc131ybq073g45ks32") (y #t)))

(define-public crate-rosa-0.77.0 (c (n "rosa") (v "0.77.0") (h "16m2jkjc0aq3fzyqb364l96gvp5dlb52kck1na8i9z0f7i01cbk0") (y #t)))

(define-public crate-rosa-7.7.18 (c (n "rosa") (v "7.7.18") (h "0w8k5z4h9vsfk9gfnnd0zcjgpd4hdmkfd2hddkaxa5x3xpfpsy2z") (y #t)))

(define-public crate-rosa-2018.7.7 (c (n "rosa") (v "2018.7.7") (h "0w965aw5nyi89si0603lycj21mqyriwsj6yyxmppfgnhl67izp8s") (y #t)))

(define-public crate-rosa-2019.12.13 (c (n "rosa") (v "2019.12.13") (h "1mwz6ajcdj6rhnclpbpf6xbaffvpaxh9v9kvyq79psf14f35pfz0") (y #t)))

(define-public crate-rosa-9999.999.99 (c (n "rosa") (v "9999.999.99") (h "0aj4086d2hq5cdj962zn4hllwh69ll9ab19zfnmqd96bsb65phc1") (y #t)))

(define-public crate-rosa-9.9.9 (c (n "rosa") (v "9.9.9") (h "1gz0wsyhd199kmr0xx6x23km47gkrycw2nbl2hqi1xgp37mzq2yx") (y #t)))

(define-public crate-rosa-99999.99999.99999 (c (n "rosa") (v "99999.99999.99999") (h "08sp73m4yrws5iq8pzyx42y23fv8jpqp3zvb1nhydy64qiy39nj6") (y #t)))

(define-public crate-rosa-9999999.9999999.9999999 (c (n "rosa") (v "9999999.9999999.9999999") (h "04dpbhbfr7vfhz1vzf3197jmyiwgrz1q3hlmr5sdbi5wiqwlm464") (y #t)))

(define-public crate-rosa-999999999.999999999.999999999 (c (n "rosa") (v "999999999.999999999.999999999") (h "0gk2fcl626k4vkrahx0gjkivb4az0l0kmgv3val9bdppzk4n3848")))

