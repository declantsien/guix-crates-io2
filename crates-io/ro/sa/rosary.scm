(define-module (crates-io ro sa rosary) #:use-module (crates-io))

(define-public crate-rosary-0.1.0 (c (n "rosary") (v "0.1.0") (h "05i40k1j2zlaslbq6w8czn3sbzs22vil2kay9h0l38i45zls67h9")))

(define-public crate-rosary-0.1.1 (c (n "rosary") (v "0.1.1") (d (list (d (n "boolinator") (r ">=2.4") (d #t) (k 0)))) (h "01f1bmz5qg00wv4xb6d2zx32bd7gfp0gpwywflwhlwmkyyjr4a1p")))

(define-public crate-rosary-0.1.2 (c (n "rosary") (v "0.1.2") (d (list (d (n "boolinator") (r ">=2.4") (d #t) (k 0)))) (h "10wx01q1q87p3mvk4nbpq1dgyzaw3cag38yfdcpw1dyjbahg9840")))

(define-public crate-rosary-0.1.3 (c (n "rosary") (v "0.1.3") (d (list (d (n "boolinator") (r ">=2.4") (d #t) (k 0)))) (h "0h1ah9hsgdbbaxbba322f27hr00qp0wr3q3mifd9gy8dxy92svya")))

(define-public crate-rosary-0.1.4 (c (n "rosary") (v "0.1.4") (d (list (d (n "boolinator") (r ">=2.4") (d #t) (k 0)))) (h "13lll3lgpi8ban0iarhbqm6yamrw3iv66h95prr3x1xr32x013g2")))

(define-public crate-rosary-0.1.5 (c (n "rosary") (v "0.1.5") (d (list (d (n "boolinator") (r ">=2.4") (d #t) (k 0)))) (h "10441h5rrvvyrin7qr4hg6a8kq649xg9vzw89avkgff7qpkqn2kh")))

(define-public crate-rosary-0.1.6 (c (n "rosary") (v "0.1.6") (d (list (d (n "boolinator") (r ">=2.4") (d #t) (k 0)))) (h "012is7cqc2xz52rr8s5j3r7xf92009dyyscx4mlbyy25k1md7qls")))

