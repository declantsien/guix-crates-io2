(define-module (crates-io ro sa rosascript) #:use-module (crates-io))

(define-public crate-rosascript-0.1.0 (c (n "rosascript") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 0)) (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0qa75s8i7mg8qd42v5f6ydsnj3ms57b5n7qiyx9443h04cfn0q0v")))

(define-public crate-rosascript-0.1.1 (c (n "rosascript") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 0)) (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0jpd9i85cj8v0z0w7qasjhwg5fgspwyf2s742q7xgfxbr15f8rzp")))

(define-public crate-rosascript-0.1.2 (c (n "rosascript") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 0)) (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0iijvm282567dmxjrz1s19ln79ysrclhywih9s58zqz6gnlm1ksa")))

