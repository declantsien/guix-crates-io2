(define-module (crates-io ro sa rosalind) #:use-module (crates-io))

(define-public crate-rosalind-0.0.1 (c (n "rosalind") (v "0.0.1") (h "0s5piqrn4v30wziczrb7y8nxwlr71q32bv0milsm47pbykgzym6q")))

(define-public crate-rosalind-0.0.2 (c (n "rosalind") (v "0.0.2") (h "115qc56i285zam9qpg06qrg22iidkrlry2hbwa9w2bqzmj69qhxv")))

(define-public crate-rosalind-0.1.0 (c (n "rosalind") (v "0.1.0") (h "0swmw488xlcppimn66b6fwkv8wcbbr9ii6mbmi6wa4ffx673l255")))

(define-public crate-rosalind-0.2.0 (c (n "rosalind") (v "0.2.0") (h "0a2w90xpk9wl7s1ns716wr3gs9yqkdacnkj4v50cxg292sjshjx4")))

(define-public crate-rosalind-0.2.1 (c (n "rosalind") (v "0.2.1") (h "1v53ll2c4cmja5sd9lf6ikfzbml0f7xy6yis425fmmpdcxgb4mz6")))

(define-public crate-rosalind-0.3.0 (c (n "rosalind") (v "0.3.0") (h "08a9iyhlaq9dmcab7a6v6s752dcjzr3g5dr5rs8yx5y0l0pl5yhs")))

(define-public crate-rosalind-0.3.1 (c (n "rosalind") (v "0.3.1") (h "0y1jpwm1ihqvq6sy5psm2ifyhv2sycgvlhdxvdwzlvphz850l9ha")))

(define-public crate-rosalind-0.4.0 (c (n "rosalind") (v "0.4.0") (h "0xj24zcjgz4izdp2hknr96190mrjjp0yfffjx5br61dclshb1rb9")))

(define-public crate-rosalind-0.4.1 (c (n "rosalind") (v "0.4.1") (h "0l3wpykl021jgd09yxk7bhplhdbgbzvvzyzrgprc3r4yiqlxl263")))

(define-public crate-rosalind-0.5.0 (c (n "rosalind") (v "0.5.0") (h "0k0kswyiqkbkhaknclzswjm6lpriqwgz0rxcpri8f9hm1riy1dj6")))

(define-public crate-rosalind-0.5.1 (c (n "rosalind") (v "0.5.1") (h "1v93f3kz6ff8v1j6grdha7jjylalkc72zrj9lfcww11cdr4bw1nb")))

(define-public crate-rosalind-0.6.0 (c (n "rosalind") (v "0.6.0") (h "1cx7idk2mjsg32525x8j9d5drk9qfhss8c469mzakrk16xmjv80q")))

(define-public crate-rosalind-0.6.1 (c (n "rosalind") (v "0.6.1") (h "0xgikbckdrban3wign1z4yqv5ygikjb6icv8vxazfjs7avpmdc4c")))

(define-public crate-rosalind-0.7.0 (c (n "rosalind") (v "0.7.0") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "0nrxk010h5i20y7byx968hcl4q3mjb8y8i6hqk050cf59rwxc6y0")))

(define-public crate-rosalind-0.8.0 (c (n "rosalind") (v "0.8.0") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "0z4v14l71gzc18wfi3syaznwg4wxp5g4px0isps0ym79nfsscka4")))

(define-public crate-rosalind-0.8.1 (c (n "rosalind") (v "0.8.1") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "109kzfkk5l7wij5zikaaaglfcx1a4dfba4h6llq7dj21v423v9vc")))

(define-public crate-rosalind-0.9.0 (c (n "rosalind") (v "0.9.0") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "02v2ijml6jlxcbbs13l44cn086fi5n54xwdg98zkyc5jn1xp01dh")))

(define-public crate-rosalind-0.10.0 (c (n "rosalind") (v "0.10.0") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "127qrd6hck172bc8k2dmsf1d0vvrqa1vv9cid85z20lkpjkbfb12")))

