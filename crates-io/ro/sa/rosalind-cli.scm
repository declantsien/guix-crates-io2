(define-module (crates-io ro sa rosalind-cli) #:use-module (crates-io))

(define-public crate-rosalind-cli-0.0.1 (c (n "rosalind-cli") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r "^0.0.1") (d #t) (k 0)))) (h "1i3jv2wkpnlzdfs23xhc49spz3xqb3vinq57xnn9lc7r690w32cf")))

(define-public crate-rosalind-cli-0.0.2 (c (n "rosalind-cli") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "0bjcpm0v22vh4snrb2ddzra0c6s915v95bjwfavy44yrhkczv4j4")))

(define-public crate-rosalind-cli-0.0.3 (c (n "rosalind-cli") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "01ls20c747kgxa31hgq2bpyighyf6cs6p7l6s61rran5d4rkcwv6")))

(define-public crate-rosalind-cli-0.0.4 (c (n "rosalind-cli") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "032frcjxaap3lmblid2c3pjq0m1rchf7x11fqppwiamz0lx7kwqp")))

(define-public crate-rosalind-cli-0.0.5 (c (n "rosalind-cli") (v "0.0.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "1mz8zmvg5f2g4y646bc9fi6dzcv9ry6icxh51ff0jil5hgh0aj5y")))

(define-public crate-rosalind-cli-0.0.6 (c (n "rosalind-cli") (v "0.0.6") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "0lxnmfj3hsdpp4zvmfyvmy104gknbwdw85r1hycdfbrzl890vqwg")))

(define-public crate-rosalind-cli-0.0.7 (c (n "rosalind-cli") (v "0.0.7") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "1szzvh7c8nvckxziqgjmk2s36sq1k92mvr49q528kmnk82r3xnhk")))

(define-public crate-rosalind-cli-0.0.8 (c (n "rosalind-cli") (v "0.0.8") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "0jbq6j4l05wpa4zdn6xmi9m3cpil6lpkbpycj2n2p6c4sdsbxhmg")))

(define-public crate-rosalind-cli-0.0.9 (c (n "rosalind-cli") (v "0.0.9") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "011zcmaqa5av0bqinkxbws5g2c1wjcfjhypl6b6lnn5r9fjxnrwf")))

(define-public crate-rosalind-cli-0.0.10 (c (n "rosalind-cli") (v "0.0.10") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "1lirxyyw5gafsfgkmnyg7qy7mdmn3nd77ss0787hyw5x34dj9rxa")))

(define-public crate-rosalind-cli-0.0.11 (c (n "rosalind-cli") (v "0.0.11") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "14f3sgwgaml746jca8y2zfmhs7p569n3vrxlcwl1j4c6fpvzw3df")))

(define-public crate-rosalind-cli-0.0.12 (c (n "rosalind-cli") (v "0.0.12") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "1jqmr5wp2zc0inkvjyqv4cnj3i5kr1jqczab1l7126475l6zpiqw")))

(define-public crate-rosalind-cli-0.0.13 (c (n "rosalind-cli") (v "0.0.13") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "1n5ahzjhjfki9cpvvb4vp3vdrly6nafhc7lzzr8cw280s5imdk1q")))

(define-public crate-rosalind-cli-0.0.14 (c (n "rosalind-cli") (v "0.0.14") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "0h2zz7sr72gnbqlmvf6z4fsg75cbis07r9viamnbmfrj2m9bwqvf")))

(define-public crate-rosalind-cli-0.0.15 (c (n "rosalind-cli") (v "0.0.15") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rosalind") (r ">= 0.0.1") (d #t) (k 0)))) (h "0sdkh7bf5n1342n50qsw8yw6v6wbr5wpxkxxd5prmq9zgqv9xd2v")))

