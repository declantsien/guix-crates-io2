(define-module (crates-io ro sc rosc) #:use-module (crates-io))

(define-public crate-rosc-0.1.1 (c (n "rosc") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)))) (h "1c2zdy2sx3d7r6hmdisigfkaacfman4fki33ssigs036gqm1yfjq")))

(define-public crate-rosc-0.1.1-2 (c (n "rosc") (v "0.1.1-2") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)))) (h "006y5jb4c50sjp95kc6hkav7xj5rgwvn9r5d5ps46c3vfr3fc9bn")))

(define-public crate-rosc-0.1.2 (c (n "rosc") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)))) (h "15cr6xhsh741p4l6xi6rk3pxihzwqr7a4myx6hfgg3zpmc3a1q69")))

(define-public crate-rosc-0.1.3 (c (n "rosc") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)))) (h "0j8y8wsyxhcny05sn087ai4l92d6402434x7dmi0q8wlgl4ihfxr")))

(define-public crate-rosc-0.1.4 (c (n "rosc") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)))) (h "1slsmzj16iq3dn23szkyh171znsgc7aaxri779v056nz3wvk0jby")))

(define-public crate-rosc-0.1.5 (c (n "rosc") (v "0.1.5") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "056cq2rqq2nn8xw4zz3nw12vrgmjv5p0cbdrx0jf16xzby75lzkd") (f (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.1.6 (c (n "rosc") (v "0.1.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "1ndqy5zyhzap82gihw30k7697nbxpczjlscgjmylj1089sj2ncrr") (f (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.2.0 (c (n "rosc") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "104v0xyqdq5y6ldfiw7jypiwsdn5ilylzly8p2c09lgm1a0pi9gj") (f (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.3.0 (c (n "rosc") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "0q9in4galy2p1hm1lf989w793zfs27g3bbwq20cvil7a0qf2d9ph") (f (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.4.0 (c (n "rosc") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "1g00yfwna5ibn7ql4jzwhm6yrvw6bsnbb1p0ssr7984dsnyadnlc") (f (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.4.1 (c (n "rosc") (v "0.4.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "08hg99gq68q32qc0n8zyyw853ikj2bh5j95mz94l9h7qigarwr9m") (f (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.4.2 (c (n "rosc") (v "0.4.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "1w1bhlappypa9lxp3nnfi1r8pr9dsfyq496s1x68vixl505qj55b") (f (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.4.3 (c (n "rosc") (v "0.4.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "0kxxbh70fqjxznwk813h8ji19ld82lizhv3vl3sjbp5inbsf21a7") (f (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.5.0 (c (n "rosc") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "13p59jmc54wx07h7frpq5m7n5lmv1iz26xnfr0d6wwx7slpvzzql") (f (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.5.1 (c (n "rosc") (v "0.5.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "1qgagn57zrb1n6bzj2x6lc2awnv1w5z3007zhi737hmmx34h7ws4") (f (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.5.2 (c (n "rosc") (v "0.5.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "1p7z1skwf749hl03cyza6g3sfipwjzc6v9fq38cyza0ccjdkbjn2") (f (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.6.0 (c (n "rosc") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "181ikf5zzb8k56y7pvgqvpjrsi8p92k4mpv44jazzss0rrq6rwn2") (f (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.7.0 (c (n "rosc") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "1k6bb2zz3frvbfrvmgznnfk0gfhiyza0vwvd1cvmh4cz9c9pg48x") (f (quote (("std") ("lints" "clippy") ("default" "std"))))))

(define-public crate-rosc-0.8.0 (c (n "rosc") (v "0.8.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "05306kw80syg5j38kkv54f5h0ac4maz4kqn8fzsmf68136m0dizh") (f (quote (("std") ("lints" "clippy") ("default" "std"))))))

(define-public crate-rosc-0.8.1 (c (n "rosc") (v "0.8.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "06f13kqhclz1s7z577nbcymb865vq2n2q3007lcb5rfz6yza4lcq") (f (quote (("std") ("lints" "clippy") ("default" "std"))))))

(define-public crate-rosc-0.9.0 (c (n "rosc") (v "0.9.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "04d2q56ij6d406gc0675c9dwv5r2d6a91m1gpvxwkbh6341sbzxk") (f (quote (("std") ("lints" "clippy") ("default" "std"))))))

(define-public crate-rosc-0.9.1 (c (n "rosc") (v "0.9.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "1pbman9gy1qpn9dd3pcjlpss0knx0jmd526z6y66w2vldg153dqh") (f (quote (("std") ("lints" "clippy") ("default" "std"))))))

(define-public crate-rosc-0.10.0 (c (n "rosc") (v "0.10.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "0rb3h5f1lsx200xwhfffbg9gk5pzcdsbik2m2nq8r96s986vi745") (f (quote (("std") ("lints" "clippy") ("default" "std")))) (r "1.52")))

(define-public crate-rosc-0.9.2 (c (n "rosc") (v "0.9.2") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "1zzlpy92c5gs24vzccnabz2klfchfr8j90j77pq6rbzzs7ka7b5j") (f (quote (("std") ("lints" "clippy") ("default" "std"))))))

(define-public crate-rosc-0.10.1 (c (n "rosc") (v "0.10.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "10d9li3zz8jfaiiljqg1sg976facw2qmkwaw93hhn28ddfg3vrmj") (f (quote (("std") ("lints" "clippy") ("default" "std")))) (r "1.52")))

