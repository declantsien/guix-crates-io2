(define-module (crates-io ro sc rosc_supercollider) #:use-module (crates-io))

(define-public crate-rosc_supercollider-0.2.0 (c (n "rosc_supercollider") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "05mw6w84wxs3bjc9a46c38rnddwyh7xg08djmz9i5vk0aw3c8dki") (f (quote (("lints" "clippy"))))))

(define-public crate-rosc_supercollider-0.2.1-pre.0 (c (n "rosc_supercollider") (v "0.2.1-pre.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "15cmv0k1kc4bd0k8k8xa68ljr0kbq4km1z86ihlgys7nhjqz20pg") (f (quote (("lints" "clippy"))))))

