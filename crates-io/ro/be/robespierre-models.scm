(define-module (crates-io ro be robespierre-models) #:use-module (crates-io))

(define-public crate-robespierre-models-0.1.0 (c (n "robespierre-models") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "189ygssaklkk2ram3grpk430bkzq7zis4qsgigxnbc16cbv3bglc")))

(define-public crate-robespierre-models-0.2.0 (c (n "robespierre-models") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0k4x3xmh28svhgk3r9x0x8kl72lackxqvg7x5m7pb32nqhaf2m00")))

(define-public crate-robespierre-models-0.3.0 (c (n "robespierre-models") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rusty_ulid") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "16f0hrly28jvwpgg1z2mbad667wh4anbhf06hjblni9bbq76awry")))

(define-public crate-robespierre-models-0.3.1 (c (n "robespierre-models") (v "0.3.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rusty_ulid") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wwiyjblfx83vph996j03pqjkaxc5ky066267px1j2948vfczaca")))

