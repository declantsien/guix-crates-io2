(define-module (crates-io ro be robespierre-cache) #:use-module (crates-io))

(define-public crate-robespierre-cache-0.1.0 (c (n "robespierre-cache") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "robespierre-models") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m24jhbhzv149ryak6hhdfxjl9jzljlhwp0zvlqygfajfqvl4dsk")))

(define-public crate-robespierre-cache-0.2.0 (c (n "robespierre-cache") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "robespierre-models") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03w68f3va2812vxkwkzwr8y1wbzlykmnirpg0d1v3zhcwldmlnpb")))

(define-public crate-robespierre-cache-0.3.1 (c (n "robespierre-cache") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "robespierre-models") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vinzv22wqvd4xs3rpvglvk1glfrnwjplvznq2r7fvqsf6z328g3")))

