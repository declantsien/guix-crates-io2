(define-module (crates-io ro be robespierre-fw-macros) #:use-module (crates-io))

(define-public crate-robespierre-fw-macros-0.2.0 (c (n "robespierre-fw-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1br7b5h7nvmskgin4ycz4pgqmhacbrx02bli3b3l8kgp8w6pyw2w")))

(define-public crate-robespierre-fw-macros-0.3.0 (c (n "robespierre-fw-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gg61jc0bnnnyw3cpp72i5s8va3shkfjhak98152niq4cyckpfqq")))

(define-public crate-robespierre-fw-macros-0.3.1 (c (n "robespierre-fw-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w9gzy3q2kn2ibhnrnh6fl20qh7i65x00z5zms5jbs74s35xnnq3")))

