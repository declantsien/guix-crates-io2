(define-module (crates-io ro ku roku-ecp) #:use-module (crates-io))

(define-public crate-roku-ecp-0.0.0 (c (n "roku-ecp") (v "0.0.0") (h "1cw3zkc5mqgid02j65wnvpyhvjf1x593p5qzcp51zipl0kf72g30")))

(define-public crate-roku-ecp-0.0.1 (c (n "roku-ecp") (v "0.0.1") (d (list (d (n "async-std") (r "^1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.1") (d #t) (k 0)) (d (n "surf") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "01rbdcdy88bfmh7s4finv7s60f8i9mcq71sp8mc9aa2h0vm906k0")))

(define-public crate-roku-ecp-0.0.2 (c (n "roku-ecp") (v "0.0.2") (d (list (d (n "async-std") (r "^1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.1") (d #t) (k 0)) (d (n "surf") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "1h4wd3kl5r7p5q1fzq209ij9z3arsmggmmkcw6qn4y7zrzgdnad3")))

(define-public crate-roku-ecp-0.0.3 (c (n "roku-ecp") (v "0.0.3") (d (list (d (n "async-std") (r "^1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.1") (d #t) (k 0)) (d (n "surf") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "0jra3kgha0wmz48hkdllnkhb16ia5q5iijg3hk1l96945n15cinv")))

