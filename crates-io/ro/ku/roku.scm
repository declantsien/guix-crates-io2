(define-module (crates-io ro ku roku) #:use-module (crates-io))

(define-public crate-roku-0.1.0 (c (n "roku") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "ssdp-client") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1ca2ri28izinvba3in2dwkjbkdfqvy5bi8prxdvrp0axfgsi9xg7")))

(define-public crate-roku-0.1.1 (c (n "roku") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "ssdp-client") (r "^0.5") (d #t) (k 0)))) (h "1q01fym0520vn5qk0gkjamlsw1r0pmwkb4i0w5mzxp7ydnppriab")))

(define-public crate-roku-0.1.2 (c (n "roku") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "ssdp-client") (r "^0.6") (d #t) (k 0)))) (h "0gfdgqar1g1f315zb3pwbmdnl291scbl2g40qz66l25afkj1hlms")))

(define-public crate-roku-0.2.0 (c (n "roku") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "ssdp-client") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "07gfp6v3ngkzl6hygf43hn9fxn6m9d88sg6dvnk3x553idpk0vf3")))

