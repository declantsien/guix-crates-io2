(define-module (crates-io ro lo rolodex) #:use-module (crates-io))

(define-public crate-rolodex-0.1.0 (c (n "rolodex") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "typed-builder") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "1ypzajmlryjh2vnynjxv509hxqcb32dhkwbx488gr09vp3q763wg") (f (quote (("serialize" "serde" "chrono/serde") ("default" "serialize" "typed-builder"))))))

(define-public crate-rolodex-0.1.1 (c (n "rolodex") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "typed-builder") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0gbp93zrpf876c7d4hz4icfmdhvaw96v5r0m95capjq5rd7g5xd1") (f (quote (("serialize" "serde" "chrono/serde") ("default" "serialize" "typed-builder"))))))

(define-public crate-rolodex-0.1.2 (c (n "rolodex") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "typed-builder") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "1raia5vi3zsmw1fy6nxkz07i25njy8sxghjmd79z7jgiyxi525k3") (f (quote (("serialize" "serde" "chrono/serde") ("default" "serialize" "typed-builder"))))))

