(define-module (crates-io ro lo rolock) #:use-module (crates-io))

(define-public crate-rolock-0.1.0 (c (n "rolock") (v "0.1.0") (h "0gkzfkwcv4nhf7hh8mvs73q30cqwry5lcwwn0adcl0i353gw99pr")))

(define-public crate-rolock-0.1.1 (c (n "rolock") (v "0.1.1") (h "0bl9vjaykzf2xxy4mdjw188m9inkx67rm0vy3dfldbw6qjkpaqyv")))

(define-public crate-rolock-0.1.2 (c (n "rolock") (v "0.1.2") (h "0gvw49f5ki264dfsw17n3a8fmhr2xwwm73wgr0zc6lwy91kdn5cp")))

