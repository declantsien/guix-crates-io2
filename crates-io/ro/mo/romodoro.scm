(define-module (crates-io ro mo romodoro) #:use-module (crates-io))

(define-public crate-romodoro-0.1.0 (c (n "romodoro") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "146fiqqy858anzaps7m5j3n6dcbs6j3mga1nichs2ds7r6wszfjc")))

(define-public crate-romodoro-0.2.0 (c (n "romodoro") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "11f2hhrn856lk1d3lx6hx2r0s3l636l6v9fh1w5kmr8l90lybkb7")))

(define-public crate-romodoro-0.3.0 (c (n "romodoro") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0hapsppjm3j605hj4skkl1qsmhmlyccgjxivzd4ggxa1f1bp0hc7")))

(define-public crate-romodoro-0.4.0 (c (n "romodoro") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "15kqwiyh5wprh47nl3yvyl4mj20cb4m91z5vb491gd5hq026ld1r")))

(define-public crate-romodoro-0.5.0 (c (n "romodoro") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.1") (d #t) (k 0)) (d (n "humantime") (r "^1.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0w8nxf3w87p9lk0v7lah7manfzpr1ns1nl22cjf0h4bx6janax1n")))

