(define-module (crates-io ro du rodu) #:use-module (crates-io))

(define-public crate-rodu-0.1.1 (c (n "rodu") (v "0.1.1") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)))) (h "10d20qi285lnfp6fc8p09y0csadsf0wyzn39p8v4hwiwl5mid4l0")))

(define-public crate-rodu-0.1.3 (c (n "rodu") (v "0.1.3") (d (list (d (n "git2") (r "^0.18.1") (d #t) (k 0)))) (h "1p4a3cqwl6wdvz7qiybisswbvwg2r54ig6jqpcaxwznfd6wi21g0")))

