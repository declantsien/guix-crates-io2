(define-module (crates-io ro bo robojackets-robocup-rtp) #:use-module (crates-io))

(define-public crate-robojackets-robocup-rtp-0.1.0 (c (n "robojackets-robocup-rtp") (v "0.1.0") (d (list (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "0bsw0ghnk9wmf5x96bhwz88g7ryvj4g1yf14jl1spwx50prvw752") (f (quote (("std" "packed_struct/std") ("nostd" "packed_struct/alloc") ("default" "std"))))))

(define-public crate-robojackets-robocup-rtp-0.1.1 (c (n "robojackets-robocup-rtp") (v "0.1.1") (d (list (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "12gbj6jk6x1rswxdra9jvbnsvaxp5kx0yj0318b7gqpmqwvs92da") (f (quote (("std" "packed_struct/std") ("nostd" "packed_struct/alloc") ("default" "std"))))))

(define-public crate-robojackets-robocup-rtp-0.1.2 (c (n "robojackets-robocup-rtp") (v "0.1.2") (d (list (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "1i8k7vx29d69hxzd2ylk5cgbky21i3976n2fazi3jdk1bsqzsp17") (f (quote (("std" "packed_struct/std") ("nostd" "packed_struct/alloc") ("default" "std"))))))

(define-public crate-robojackets-robocup-rtp-0.1.3 (c (n "robojackets-robocup-rtp") (v "0.1.3") (d (list (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "1wqx3x5shi00jxrsgi681abr5mjvmyamp07jfmigdadhc7rnjzcp") (f (quote (("std" "packed_struct/std") ("nostd" "packed_struct/alloc") ("default" "std"))))))

(define-public crate-robojackets-robocup-rtp-0.1.4 (c (n "robojackets-robocup-rtp") (v "0.1.4") (d (list (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "1mld8yqpspywld7hqn5ycylbj0jmihs3k9n3ir8mgj3ywrc7lq3b") (f (quote (("std" "packed_struct/std") ("nostd" "packed_struct/alloc") ("default" "std"))))))

(define-public crate-robojackets-robocup-rtp-0.1.5 (c (n "robojackets-robocup-rtp") (v "0.1.5") (d (list (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "1xg01034p49glhmmnflmy56akmg3lqywv6dlxmi9wmhgzrjdj15p") (f (quote (("std" "packed_struct/std") ("nostd" "packed_struct/alloc") ("default" "std"))))))

(define-public crate-robojackets-robocup-rtp-0.1.6 (c (n "robojackets-robocup-rtp") (v "0.1.6") (d (list (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "08y4f9c85fyj587yjlvd1raq9ifx7h3grqs6ldq68f8f81ra95zq") (f (quote (("std" "packed_struct/std") ("nostd" "packed_struct/alloc") ("default" "std"))))))

(define-public crate-robojackets-robocup-rtp-0.1.7 (c (n "robojackets-robocup-rtp") (v "0.1.7") (d (list (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "12y3wslss03pjazxz09w5hgw3adfhjfcbj98hbqk29ix0fvjh2fk") (f (quote (("std" "packed_struct/std") ("nostd" "packed_struct/alloc") ("default" "std"))))))

(define-public crate-robojackets-robocup-rtp-0.2.0 (c (n "robojackets-robocup-rtp") (v "0.2.0") (d (list (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "1dkrr6ayfz8j4dqj6i5sl2qp8j08dmis943znp9drai0ahvjpkyj") (f (quote (("std" "packed_struct/std") ("nostd" "packed_struct/alloc") ("default" "nostd"))))))

(define-public crate-robojackets-robocup-rtp-0.2.1 (c (n "robojackets-robocup-rtp") (v "0.2.1") (d (list (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "0h19h7525f6mic86ffgdcm0grji161m3vc1p91mw2nzra6l17kjr") (f (quote (("std" "packed_struct/std") ("nostd" "packed_struct/alloc") ("default" "nostd"))))))

(define-public crate-robojackets-robocup-rtp-0.2.2 (c (n "robojackets-robocup-rtp") (v "0.2.2") (d (list (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm" "nalgebra-macros" "alloc"))) (o #t) (k 0)) (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "0vq0pl2wjc1cwbly7c8sxcsljvqv7bhd0y5w1fivd9y2wf8hzrkk") (f (quote (("std" "packed_struct/std") ("nostd" "packed_struct/alloc" "nalgebra") ("default" "nostd"))))))

(define-public crate-robojackets-robocup-rtp-0.3.0 (c (n "robojackets-robocup-rtp") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm" "nalgebra-macros" "alloc"))) (o #t) (k 0)) (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "1mjhhghlvw8v14g4s9lzjv9da05fbba3inwv9pj2qdp6pzdxrkvh") (f (quote (("yellow-team") ("std" "packed_struct/std") ("nostd" "packed_struct/alloc" "nalgebra") ("default" "nostd") ("blue-team"))))))

(define-public crate-robojackets-robocup-rtp-0.4.0 (c (n "robojackets-robocup-rtp") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm" "nalgebra-macros" "alloc"))) (o #t) (k 0)) (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "1rajh0ravb8rnc4gl11rxsnkvwfxgp9z036mvinj7yjcfsyb2zq6") (f (quote (("yellow-team") ("std" "packed_struct/std") ("nostd" "packed_struct/alloc" "nalgebra") ("default" "nostd") ("blue-team"))))))

(define-public crate-robojackets-robocup-rtp-0.4.1 (c (n "robojackets-robocup-rtp") (v "0.4.1") (d (list (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm" "nalgebra-macros" "alloc"))) (o #t) (k 0)) (d (n "packed_struct") (r "^0.10.1") (k 0)))) (h "0qlyjl3sbnj066mv2ksi78qny92vb6dfdg2cqy39bm2y2nzhax99") (f (quote (("yellow-team") ("std" "packed_struct/std") ("nostd" "packed_struct/alloc" "nalgebra") ("default" "nostd") ("blue-team"))))))

