(define-module (crates-io ro bo roboxide) #:use-module (crates-io))

(define-public crate-roboxide-0.1.0 (c (n "roboxide") (v "0.1.0") (d (list (d (n "bincode") (r ">=1.3.1, <2.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.117, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (d #t) (k 0)) (d (n "zmq") (r ">=0.9.0, <0.10.0") (d #t) (k 0)))) (h "163mfzrmsc0fbcjm81nk7ck71m31h8s0vy3hlg17svg825h07wby") (y #t)))

(define-public crate-roboxide-0.1.1 (c (n "roboxide") (v "0.1.1") (d (list (d (n "bincode") (r ">=1.3.1, <2.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.117, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (d #t) (k 0)) (d (n "zmq") (r ">=0.9.0, <0.10.0") (d #t) (k 0)))) (h "1g2jsbn9pjmdjjyvsn5z5rcm6hps7lpg4ha9r3m4bh9skc4xdhax") (y #t)))

