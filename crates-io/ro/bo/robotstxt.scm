(define-module (crates-io ro bo robotstxt) #:use-module (crates-io))

(define-public crate-robotstxt-0.1.0 (c (n "robotstxt") (v "0.1.0") (d (list (d (n "url") (r "^1") (d #t) (k 0)))) (h "1hkblkk9pdjrck4wkxc5m4nd1v0xnmz0qi9v4w06vvc7mnhxrwhk") (y #t)))

(define-public crate-robotstxt-0.2.0 (c (n "robotstxt") (v "0.2.0") (h "075bgx963hznfx94yw9nmwajna3zy509f6x0zjim9am4bgszvcm7")))

(define-public crate-robotstxt-0.3.0 (c (n "robotstxt") (v "0.3.0") (h "19xlsihm6qrafi5zz2gz6kfsqv4b7dhclj67lb1zxqw0vdvj7idv")))

