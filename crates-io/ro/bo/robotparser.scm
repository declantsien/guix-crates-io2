(define-module (crates-io ro bo robotparser) #:use-module (crates-io))

(define-public crate-robotparser-0.1.0 (c (n "robotparser") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1ajivs2mh1as1rzcysjkbg4i1g7q823liynqn7l7g62c3hhinljx") (f (quote (("unstable"))))))

(define-public crate-robotparser-0.2.0 (c (n "robotparser") (v "0.2.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0jd3v6h7nahpz2yg5qzbia5x4wxn3vz9kg5hc804bgv5aqxhpdg6") (f (quote (("unstable"))))))

(define-public crate-robotparser-0.3.0 (c (n "robotparser") (v "0.3.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "01m4pdadzbylkwpz2w1q5gprzhhz7n57564zwhm4q52clmz5dj3y") (f (quote (("unstable"))))))

(define-public crate-robotparser-0.4.0 (c (n "robotparser") (v "0.4.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "11zkxb53sbb6chkiq5hxdqhdbf9rawa46dyca48lw96hx0i8fan3") (f (quote (("unstable"))))))

(define-public crate-robotparser-0.4.1 (c (n "robotparser") (v "0.4.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "0d6xvfab4dnwx2nd3pywivrsvczh18mggibm7mpvwzd00gfhqc03") (f (quote (("unstable"))))))

(define-public crate-robotparser-0.4.2 (c (n "robotparser") (v "0.4.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "1012bplhzj5sdi3n6jxg7gxhr0rjc0vgb8ng7l1w06dz2gjfqqcm") (f (quote (("unstable"))))))

(define-public crate-robotparser-0.4.3 (c (n "robotparser") (v "0.4.3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1cqps00n646p0kzsfz015mb4aambbszq4i75l0kszrhv4gyknivv") (f (quote (("unstable"))))))

(define-public crate-robotparser-0.4.4 (c (n "robotparser") (v "0.4.4") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "07b7wmkm6x7chi69hlf5d90dgfs7hljj7lma8brz5iy31n0w7jik") (f (quote (("unstable"))))))

(define-public crate-robotparser-0.5.0 (c (n "robotparser") (v "0.5.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "0rnsq1bha0mif66cdkfhld72nz5y3rnp1lx5zds48z3fm5pfga35") (f (quote (("unstable"))))))

(define-public crate-robotparser-0.6.0 (c (n "robotparser") (v "0.6.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "19in61af49jv0zyj5dbzbw8n3jd4j1g4f116w4d1q1q1z4wb3gyy") (f (quote (("unstable"))))))

(define-public crate-robotparser-0.7.0 (c (n "robotparser") (v "0.7.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "01np3gmcfksrydjv6vbcrk5nx49jxxb930670xjax66l4j82yx86") (f (quote (("unstable") ("http" "hyper") ("default" "http"))))))

(define-public crate-robotparser-0.8.0 (c (n "robotparser") (v "0.8.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0fm3ycvyw0mkvlgdnsgyp5lz2s9pcsm8g34g9p1h5wsv6ykfk0mg") (f (quote (("unstable") ("http" "reqwest") ("default" "http"))))))

(define-public crate-robotparser-0.8.1 (c (n "robotparser") (v "0.8.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "05wif3lx7x628d5z9jb85a6rgzq57c35lsvdir5jhzq1b7njnxj8") (f (quote (("unstable") ("http" "reqwest") ("default" "http"))))))

(define-public crate-robotparser-0.8.2 (c (n "robotparser") (v "0.8.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "1r9j7aazcgp2mji28pwwvpy0rdrcxr0fbv3cc0dimj16lslzhsfy") (f (quote (("unstable") ("http" "reqwest") ("default" "http"))))))

(define-public crate-robotparser-0.9.0 (c (n "robotparser") (v "0.9.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0xwgj3y6sx89hql8f3wv1i565jgrz234rlmzapvm5pgx5aw9grxi") (f (quote (("unstable") ("http" "reqwest") ("default" "http"))))))

(define-public crate-robotparser-0.10.0 (c (n "robotparser") (v "0.10.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0w1c8pvm8faxd99py5v580dnzvy8ixdg9xwk9ck6x10rzjpf1cl6") (f (quote (("unstable") ("http" "reqwest") ("default" "http"))))))

(define-public crate-robotparser-0.10.1 (c (n "robotparser") (v "0.10.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0d5zqb4z3s8x0w4lxb91lzpy2qxkxf2byv3i6f84d2ab4bfjbazq") (f (quote (("unstable") ("http" "reqwest") ("default" "http"))))))

(define-public crate-robotparser-0.10.2 (c (n "robotparser") (v "0.10.2") (d (list (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0fgkakz0l74s02287s0xicsa5h1cm6kvlq88vv1dgq5pzg1kzzyh") (f (quote (("unstable") ("http" "reqwest") ("default" "http"))))))

(define-public crate-robotparser-0.11.0 (c (n "robotparser") (v "0.11.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1l0jvl9rj8wvzhv2hpypi737zy10r283xs5g7nr228cmqkjdvzx9") (f (quote (("unstable") ("default" "reqwest" "futures"))))))

