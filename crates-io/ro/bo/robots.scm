(define-module (crates-io ro bo robots) #:use-module (crates-io))

(define-public crate-RobotS-0.3.0 (c (n "RobotS") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1lm3xrgqj5lhkfqfwmzd1ajp73fyk206j7ahbl9cjdyshffwrysn") (f (quote (("dev") ("default"))))))

