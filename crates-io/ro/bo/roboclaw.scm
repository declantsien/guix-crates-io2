(define-module (crates-io ro bo roboclaw) #:use-module (crates-io))

(define-public crate-roboclaw-0.1.0 (c (n "roboclaw") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "0pldjqqcxqgsvd9ppqjh6gb5df0fmmyv4bad0y3x8rd581k31lhf")))

