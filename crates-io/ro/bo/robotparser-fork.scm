(define-module (crates-io ro bo robotparser-fork) #:use-module (crates-io))

(define-public crate-robotparser-fork-0.11.0 (c (n "robotparser-fork") (v "0.11.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.1") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "11w3kz5zkhzrmsfa5j83b8wr6p1radwhgynvydrdja76q1cc09g2") (f (quote (("unstable") ("default" "reqwest" "futures"))))))

(define-public crate-robotparser-fork-0.10.3 (c (n "robotparser-fork") (v "0.10.3") (d (list (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0r66pi4rsf8j3k3ni5miqx0pmm4q0zi739pmbbpbsmfqwflgrbfl") (f (quote (("unstable") ("http" "reqwest") ("default" "http"))))))

(define-public crate-robotparser-fork-0.10.4 (c (n "robotparser-fork") (v "0.10.4") (d (list (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0a20w06mcaa33mck64kbsxsnpxyz4s5y77f6p7mm7y8xhkqxriw0") (f (quote (("unstable") ("http" "reqwest") ("default" "http"))))))

(define-public crate-robotparser-fork-0.10.5 (c (n "robotparser-fork") (v "0.10.5") (d (list (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1yg4207w5xws0hnr9nd7mp55dgm98035bihwj67nknkfs5a5857v") (f (quote (("unstable") ("http" "reqwest") ("default" "http"))))))

(define-public crate-robotparser-fork-0.10.6 (c (n "robotparser-fork") (v "0.10.6") (d (list (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "192hi0njq9w1k5vck0yfxb2gdxs0g0k0xza2mjis1r4zpifm2w87") (f (quote (("unstable") ("http" "reqwest") ("default" "http")))) (y #t)))

(define-public crate-robotparser-fork-0.10.7 (c (n "robotparser-fork") (v "0.10.7") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0n7c7g1z2md30nzfmbvz3bz28qkcl34dxrl0xxsnz40qzjx4lbl5") (y #t)))

(define-public crate-robotparser-fork-0.10.8 (c (n "robotparser-fork") (v "0.10.8") (d (list (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "12qlya047gj6fxfhxlhffk8wj0gqjqm1jz537wgzqd2hhjlybvcb") (f (quote (("unstable") ("http" "reqwest") ("default" "http")))) (y #t)))

