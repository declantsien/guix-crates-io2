(define-module (crates-io ro bo robo6502) #:use-module (crates-io))

(define-public crate-robo6502-0.1.0 (c (n "robo6502") (v "0.1.0") (d (list (d (n "machine_int") (r "^0.1.0") (d #t) (k 0)))) (h "0ba92258n5pcj24k9fm36g4gbma33pydayfxqwrbykpgj2vlsiz7")))

(define-public crate-robo6502-0.2.0 (c (n "robo6502") (v "0.2.0") (d (list (d (n "machine_int") (r "^0.1.1") (d #t) (k 0)))) (h "05fk0snrm00ay2zq20fqrpsc3sk0hqpfcaxb0n5khk775ibswlmp")))

(define-public crate-robo6502-0.3.0 (c (n "robo6502") (v "0.3.0") (d (list (d (n "machine_int") (r "^0.1.1") (d #t) (k 0)))) (h "04aic86za9dj8qgh1dg3qgqp2szbb3lh8nk2ch3w7aiygmm45fg4")))

(define-public crate-robo6502-0.3.1 (c (n "robo6502") (v "0.3.1") (d (list (d (n "machine_int") (r "^0.1.2") (d #t) (k 0)))) (h "1cg1vnqx6n6qjwd98mp2nvsh0j4547n4wwsqzxp3bz6991bkp1q1")))

(define-public crate-robo6502-0.3.2 (c (n "robo6502") (v "0.3.2") (d (list (d (n "machine_int") (r "^0.1.2") (d #t) (k 0)))) (h "1a0mi2vfsnn3s8v137gfsdmp2hjgwhp5i2qdydxbxylpa98zfywy")))

