(define-module (crates-io ro bo roboto) #:use-module (crates-io))

(define-public crate-roboto-0.1.0 (c (n "roboto") (v "0.1.0") (d (list (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)))) (h "0ln5p3qvn8ly5pq845g46qcz607x40y25zvr36vwhsvm9k337101")))

(define-public crate-roboto-0.1.1 (c (n "roboto") (v "0.1.1") (d (list (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)))) (h "1rp6hsvxcndlskq8h8i35dqy0a8zws6iai7vdfymbpnrc0b2ldq0")))

