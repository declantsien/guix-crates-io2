(define-module (crates-io ro bo robots_txt) #:use-module (crates-io))

(define-public crate-robots_txt-0.0.0 (c (n "robots_txt") (v "0.0.0") (h "063rr0rydf37c5nl9w0m0qyhmfrxi3fm9aa6rvngn2kci4r1s0i0")))

(define-public crate-robots_txt-0.3.0 (c (n "robots_txt") (v "0.3.0") (d (list (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1lw9bv79l1wb72w1ij8wwfzhh4fgiw7kaq521qa87kk05aaa349f") (f (quote (("release") ("dev") ("default" "release"))))))

(define-public crate-robots_txt-0.4.0 (c (n "robots_txt") (v "0.4.0") (d (list (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0jq5sl6a2lka9hgwxaix5wgx0vv5bxaw7iil60c9pcr6j2pyv7yj") (f (quote (("release") ("dev") ("default" "release"))))))

(define-public crate-robots_txt-0.5.0 (c (n "robots_txt") (v "0.5.0") (d (list (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1x44gq8kpaxpnn0jwr2arjh213wqhsn5zrsn90ax397f82pmgy7w") (f (quote (("release") ("dev") ("default" "release"))))))

(define-public crate-robots_txt-0.5.1 (c (n "robots_txt") (v "0.5.1") (d (list (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1s8jkapil74bajl1w2sh09048grn4sb2gdaj2k0jz1s8qd7z77y7") (f (quote (("release") ("dev") ("default" "release"))))))

(define-public crate-robots_txt-0.5.3 (c (n "robots_txt") (v "0.5.3") (d (list (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0s23knnxd57shlpawyj4f0b7mggq9nr8nmqj5ia8c7405x2zhv2h") (f (quote (("release") ("default" "release"))))))

(define-public crate-robots_txt-0.5.4 (c (n "robots_txt") (v "0.5.4") (d (list (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0kv149hvlj6svhfzcp9knbclil3qmkszyyvhqrqx2dz4f2kyk5l3") (f (quote (("release") ("default" "release"))))))

(define-public crate-robots_txt-0.5.5 (c (n "robots_txt") (v "0.5.5") (d (list (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "05zq0hg1fc3a5yi2mj9qyhc4577z7vgjh0cy2xmqwj04hj8zk1fg") (f (quote (("release") ("default" "release"))))))

(define-public crate-robots_txt-0.6.0 (c (n "robots_txt") (v "0.6.0") (d (list (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1f7w0dqzn2sdisbci33ay73i34gksvv03cv98pdi7cwq7mvp691p") (f (quote (("release") ("default" "release"))))))

(define-public crate-robots_txt-0.7.0 (c (n "robots_txt") (v "0.7.0") (d (list (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "03gs02c8a40v2hj6cy31n66j96qx66z4vnhql2rkap67i4w553hy") (f (quote (("release") ("default" "release"))))))

