(define-module (crates-io ro bo robotics-signals) #:use-module (crates-io))

(define-public crate-robotics-signals-0.1.0 (c (n "robotics-signals") (v "0.1.0") (d (list (d (n "cdds_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "cyclonedds-rs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "00qs41dkcrr2zsbhkk0rdq7hqbn8yr4r5hxryznszdgv8gkplqwi")))

(define-public crate-robotics-signals-0.1.1 (c (n "robotics-signals") (v "0.1.1") (d (list (d (n "cdds_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "cyclonedds-rs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0dhyq836njm0nv7b59bh8j0k602542zg8wkjpix2jvr84qrijjw5")))

(define-public crate-robotics-signals-0.2.0 (c (n "robotics-signals") (v "0.2.0") (d (list (d (n "cdds_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "cyclonedds-rs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1z26ksj24nmxv5jnwv0fcg78igc98q0h5aim2gfgxwnnhzbn86yy")))

(define-public crate-robotics-signals-0.2.1 (c (n "robotics-signals") (v "0.2.1") (d (list (d (n "cdds_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "cyclonedds-rs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "13qswq77a5amvin35nd78vxj7z0cr2av9fh1c3f0rrql4adjnnig")))

