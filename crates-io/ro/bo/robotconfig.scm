(define-module (crates-io ro bo robotconfig) #:use-module (crates-io))

(define-public crate-robotconfig-0.1.0 (c (n "robotconfig") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0y5wgmj0y979g4hv1g40b500wf3d7axb3dghkrp23c30b8c1x2l6")))

