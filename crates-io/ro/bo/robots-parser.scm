(define-module (crates-io ro bo robots-parser) #:use-module (crates-io))

(define-public crate-robots-parser-0.1.0 (c (n "robots-parser") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0wfgx37a1k9fqnbxhfs922r3vxpwqzd9vxxqpxdmprx1ijlfhxm1") (f (quote (("web" "reqwest") ("default" "web"))))))

