(define-module (crates-io ro bo roboime-next) #:use-module (crates-io))

(define-public crate-roboime-next-0.1.0 (c (n "roboime-next") (v "0.1.0") (d (list (d (n "clap") (r "^2.5.0") (d #t) (k 0)) (d (n "clock_ticks") (r "~0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "glium") (r "~0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "roboime-next-protocol") (r "^0.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "unicode-normalization") (r "~0.1.2") (d #t) (k 0)))) (h "1y00q8790aapbgvgl20s8jlr68vzgvgxw66qpdchk655c4nl5msc") (f (quote (("unstable") ("default"))))))

