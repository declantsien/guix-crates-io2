(define-module (crates-io ro bo robot36-encoder) #:use-module (crates-io))

(define-public crate-robot36-encoder-0.1.0 (c (n "robot36-encoder") (v "0.1.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0w95rmc01jfhm05nc38x5b4m9ns6b5jn86pb1n8r2pcmx2nnlkdm")))

(define-public crate-robot36-encoder-0.1.1 (c (n "robot36-encoder") (v "0.1.1") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "19ldqi1s7i9r873hrvamkwq0643ydys5wzgrjhqy64lbs4niklgk")))

(define-public crate-robot36-encoder-0.2.0 (c (n "robot36-encoder") (v "0.2.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17cmjwcxr02pdyj1dfk1xkx574mq72agqk0m88dmv7bxxnd2vqkl")))

(define-public crate-robot36-encoder-0.2.1 (c (n "robot36-encoder") (v "0.2.1") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "018vlifxywnvg03ppcxl2w1byvfsbzyqjfdwhycyng6r6s4svzq0")))

