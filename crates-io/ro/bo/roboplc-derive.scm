(define-module (crates-io ro bo roboplc-derive) #:use-module (crates-io))

(define-public crate-roboplc-derive-0.1.0 (c (n "roboplc-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z0rac6vgx06h51r9ppigkfp42awfl8m07h46fk1dbz0wlwcnm01")))

(define-public crate-roboplc-derive-0.1.1 (c (n "roboplc-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g02j76lmzq62n5d90ny7wm0rgsywgsjlqhkd0y217d3yvahgg6b")))

(define-public crate-roboplc-derive-0.1.2 (c (n "roboplc-derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roboplc") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y2v5iq4bdvcs876w7rsc87gdy095bbgqbqw12i3vrz6vfipns54")))

(define-public crate-roboplc-derive-0.1.3 (c (n "roboplc-derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roboplc") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yahpb1hanqpxz3b71yfgm305gs89bpa617vl6r06h7dqhkyd4si")))

(define-public crate-roboplc-derive-0.1.4 (c (n "roboplc-derive") (v "0.1.4") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roboplc") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j5alil0v145nipaq054kpv38pq5zz7cmc7npjdi8v5b7knbbpnk")))

(define-public crate-roboplc-derive-0.1.5 (c (n "roboplc-derive") (v "0.1.5") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roboplc") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17rgj04n2jqhxa8h25ch195f6rbvyccnx405dszc5z2xwp1f8gf2")))

(define-public crate-roboplc-derive-0.1.6 (c (n "roboplc-derive") (v "0.1.6") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roboplc") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0iqnr77j6g695jcl6q6aym1yc2p1dclr2pfrncg4fdl6f70lh8gm")))

(define-public crate-roboplc-derive-0.1.7 (c (n "roboplc-derive") (v "0.1.7") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roboplc") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l2bmsksljdmbvgd0hn6z385njc0js18icpbs58bzraar4pvrh46")))

(define-public crate-roboplc-derive-0.1.8 (c (n "roboplc-derive") (v "0.1.8") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roboplc") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lr9rp4a2ii2srgfbzk5pnwb4snwplr03vq1l510q44phsqscc8s")))

(define-public crate-roboplc-derive-0.1.9 (c (n "roboplc-derive") (v "0.1.9") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roboplc") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14m5ggxx2z1inm5a1c5qdmg7lh9scby7r4z0g2i3aqv2avxmmy1d")))

