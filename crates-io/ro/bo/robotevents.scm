(define-module (crates-io ro bo robotevents) #:use-module (crates-io))

(define-public crate-robotevents-0.1.0 (c (n "robotevents") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p3ks90cdzfir4pqily2ddjvljaqf7ywqr4q4cyszgfsnyqcl64f")))

(define-public crate-robotevents-0.2.1 (c (n "robotevents") (v "0.2.1") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0531k8iqm8fx4fgdphy97iaq8jfjy25m8hgh6igcc48vimf2cx2d")))

(define-public crate-robotevents-0.2.2 (c (n "robotevents") (v "0.2.2") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fqwrlkx0fv2dxqj65w47d1f2sg1w19yf3ya2rqhk3a4w3prddas")))

(define-public crate-robotevents-0.2.3 (c (n "robotevents") (v "0.2.3") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y9gmw49lqwvxj9bs2mjxbjs00dq5ca5862mfca97k3qksksps6f")))

(define-public crate-robotevents-0.2.4 (c (n "robotevents") (v "0.2.4") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j7padb72rffn1wf03pvfs4vdlfwv7w2q5pxmhjqzbzpnxy4n024")))

(define-public crate-robotevents-0.2.5 (c (n "robotevents") (v "0.2.5") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lpshfj25p6hggwmq6g4fs08l1f62082b1llz04c32v1y24lasj2")))

(define-public crate-robotevents-0.2.6 (c (n "robotevents") (v "0.2.6") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1033kzzni96v4rd2i1sg5yyxi7grk4qb3px8kg84szkg83jik3lr")))

(define-public crate-robotevents-0.2.7 (c (n "robotevents") (v "0.2.7") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jfpkgblzc7903v4xrnazbbyhhp3pwbvpg29kl4s9j5iiw47fwzz")))

(define-public crate-robotevents-0.2.8 (c (n "robotevents") (v "0.2.8") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w37zp6x3msryabg3b3cn5lxvjqyx2fx8l4gclp45d7czs0pkajj")))

(define-public crate-robotevents-0.2.9 (c (n "robotevents") (v "0.2.9") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "040m00i7lhs688sh3brl8ks15la96bjcywiq9a3v23m83mqgfsrk")))

(define-public crate-robotevents-0.3.0 (c (n "robotevents") (v "0.3.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a4x3cnhaf4m6a2rvqn45jmjac85f3wr3qszd2avv64r9w2ym2ci")))

(define-public crate-robotevents-0.4.0 (c (n "robotevents") (v "0.4.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n6zgzwqhk48npnvgffar3fmy1npx02mz4vz1li6i8q4ay2s9vca")))

(define-public crate-robotevents-0.5.0 (c (n "robotevents") (v "0.5.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0898cmh3rdnizgx8hnjwni1bq6yqx3krb0ymp1azl4b74javm42y")))

