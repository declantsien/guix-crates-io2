(define-module (crates-io ro nf ronfmt) #:use-module (crates-io))

(define-public crate-ronfmt-0.1.0 (c (n "ronfmt") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0m4fdjcgpjkpz6mp78xgfmnj7g52anrv4mmnyadpxf4v6fj3jrv4")))

