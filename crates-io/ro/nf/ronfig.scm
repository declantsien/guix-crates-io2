(define-module (crates-io ro nf ronfig) #:use-module (crates-io))

(define-public crate-ronfig-0.1.0 (c (n "ronfig") (v "0.1.0") (d (list (d (n "encoding_rs_io") (r "^0.1") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "15fbn46n1a2l9b78j14s94r233l69wmylfb6jirqk0ils7d48xiz")))

(define-public crate-ronfig-0.1.1 (c (n "ronfig") (v "0.1.1") (d (list (d (n "encoding_rs_io") (r "^0.1") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0mrf26rjv5ji4f17frczgvahkmmmikf4qwd0b535h5dps77sa0j3")))

