(define-module (crates-io ro q- roq-dec) #:use-module (crates-io))

(define-public crate-roq-dec-0.1.0 (c (n "roq-dec") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0lbc6cgkf11cnqr7z8hi1gwblf1fnf7ffisd4d0xnzwjj1iq190k")))

