(define-module (crates-io ro bm robma_builder) #:use-module (crates-io))

(define-public crate-robma_builder-0.0.1 (c (n "robma_builder") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.31") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1m84ns8lrk132yfmh4a999lp0iricpk5r46kq93dfk65mbr443vd")))

