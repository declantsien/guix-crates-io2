(define-module (crates-io ro f_ rof_rs_macros) #:use-module (crates-io))

(define-public crate-rof_rs_macros-0.1.0 (c (n "rof_rs_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.55") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rof_rs_core") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1vzqcfiwwgva5jacjcaaanzsd8v9wryd2ldx7f264hm7m5jpzxpf")))

(define-public crate-rof_rs_macros-0.1.1 (c (n "rof_rs_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.55") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0zx3zwldcfzwa05m7rpshr1ygzvvlsvh0si03h3gwdayamarccwj")))

(define-public crate-rof_rs_macros-0.1.2 (c (n "rof_rs_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.55") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0van6fqdfbxiyi2il80s8z24xrhqxrbvlfmpx069ilk9fzx8ij17")))

(define-public crate-rof_rs_macros-0.1.3 (c (n "rof_rs_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.55") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0kd87bmzc8xsnk1k20ix7ls9bkidxmw6r0ws4c9hg3p5nma5mphn")))

