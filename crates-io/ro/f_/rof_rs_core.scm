(define-module (crates-io ro f_ rof_rs_core) #:use-module (crates-io))

(define-public crate-rof_rs_core-0.1.0 (c (n "rof_rs_core") (v "0.1.0") (h "0pkgzi6gmsariag27icdawvaqmjcdpzvjs7gpg3myvf2z1qs8q2a") (y #t)))

(define-public crate-rof_rs_core-0.1.1 (c (n "rof_rs_core") (v "0.1.1") (h "1vdkj13fs19sx17py0bany43yrpdi966sfdnns26wahdm08hsyx7") (y #t)))

(define-public crate-rof_rs_core-0.1.2 (c (n "rof_rs_core") (v "0.1.2") (h "1wsd5ns407snz972pi5aiqw863c4cn2y0xd4rndsrdj46df18l6m") (y #t)))

