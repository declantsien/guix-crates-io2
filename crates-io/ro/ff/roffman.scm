(define-module (crates-io ro ff roffman) #:use-module (crates-io))

(define-public crate-roffman-0.1.0 (c (n "roffman") (v "0.1.0") (h "1fp2dhll9556bkgzmrqaabl696fjiyh04smq7bj0abngbsf1zjx1")))

(define-public crate-roffman-0.2.0 (c (n "roffman") (v "0.2.0") (h "07bnrwjv9brsgqgyi1imwpd1jk33a1ckyqda87is37p37ijhzxvz")))

(define-public crate-roffman-0.3.0 (c (n "roffman") (v "0.3.0") (h "08kdjh0yhabprf5daa0a1k7sndywybin1hi7j8c5mx8365p02b7n")))

(define-public crate-roffman-0.4.0 (c (n "roffman") (v "0.4.0") (h "04m1krlm47950073jm38pjyaixf7r7nyivsr5d177895is49xwhs")))

