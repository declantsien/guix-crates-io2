(define-module (crates-io ro ff roff) #:use-module (crates-io))

(define-public crate-roff-0.1.0 (c (n "roff") (v "0.1.0") (d (list (d (n "duct") (r "^0.10.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "0pk76fw9hqnvr8qbd5r8yq08zpgymk14wgkn5h2qhs54gfrlygp3")))

(define-public crate-roff-0.2.0 (c (n "roff") (v "0.2.0") (d (list (d (n "duct") (r "^0.13") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "0l3dpqy846awxpwq6cm9y39khwkflbm70r5zd85rymbny35jck2g")))

(define-public crate-roff-0.2.1 (c (n "roff") (v "0.2.1") (d (list (d (n "duct") (r "^0.13") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "05j324x84xkgp848smhnknnlpl70833xb9lalqg4n2ga6k8dhcxq")))

