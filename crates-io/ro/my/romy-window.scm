(define-module (crates-io ro my romy-window) #:use-module (crates-io))

(define-public crate-romy-window-0.2.0 (c (n "romy-window") (v "0.2.0") (d (list (d (n "cpal") (r "^0.11.0") (d #t) (k 0)) (d (n "romy-core") (r "^0.2.0") (d #t) (k 0)) (d (n "sample") (r "^0.10.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha5") (d #t) (k 0)))) (h "1cdqnpc12fk9jdfrwq9byiwn7l8m600aa7rjq15gr7mlgh2j7fia")))

(define-public crate-romy-window-0.2.1 (c (n "romy-window") (v "0.2.1") (d (list (d (n "cpal") (r "^0.11.0") (d #t) (k 0)) (d (n "romy-core") (r "^0.2.0") (d #t) (k 0)) (d (n "sample") (r "^0.10.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha5") (d #t) (k 0)))) (h "106xrc0wpxrll7ss68f45r4c0hqlp9gznyb7600jlvldhyq0z32l")))

(define-public crate-romy-window-0.2.2 (c (n "romy-window") (v "0.2.2") (d (list (d (n "cpal") (r "^0.11.0") (d #t) (k 0)) (d (n "romy-core") (r "^0.2.0") (d #t) (k 0)) (d (n "sample") (r "^0.10.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.20.0") (d #t) (k 0)))) (h "107q8g97sz42104fkrbxcqf88wriahzvhbvz03pxv4951jq53p0m")))

(define-public crate-romy-window-0.2.3 (c (n "romy-window") (v "0.2.3") (d (list (d (n "cpal") (r "^0.11.0") (d #t) (k 0)) (d (n "romy-core") (r "^0.2.0") (d #t) (k 0)) (d (n "sample") (r "^0.10.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.20.0") (d #t) (k 0)))) (h "1h9wrmqs0y9d5y4qn439im6fibw1lbvw13xrk4yizzs6h4i0v3c1")))

(define-public crate-romy-window-0.2.4 (c (n "romy-window") (v "0.2.4") (d (list (d (n "cpal") (r "^0.11.0") (d #t) (k 0)) (d (n "romy-core") (r "^0.2.0") (d #t) (k 0)) (d (n "sample") (r "^0.10.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.20.0") (d #t) (k 0)))) (h "1fx13hala2lnbsgxiavdp55q6qgr4ldf3li9yv04h6g571bk957x")))

(define-public crate-romy-window-0.2.5 (c (n "romy-window") (v "0.2.5") (d (list (d (n "cpal") (r "^0.11.0") (d #t) (k 0)) (d (n "romy-core") (r "^0.2.0") (d #t) (k 0)) (d (n "sample") (r "^0.10.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.20.0") (d #t) (k 0)))) (h "1kd8ddqx37132kb3z1lils56m3lc2fyqk0wjk3lzz1lzhv9h0c3q")))

(define-public crate-romy-window-0.2.6 (c (n "romy-window") (v "0.2.6") (d (list (d (n "cpal") (r "^0.11.0") (d #t) (k 0)) (d (n "romy-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.20.0") (d #t) (k 0)))) (h "1q0xh2pxxcj3ymgynvpd8yiy3z91yz2i6y1cqj1m43gckbpla7zd")))

(define-public crate-romy-window-0.2.7 (c (n "romy-window") (v "0.2.7") (d (list (d (n "cpal") (r "^0.11.0") (d #t) (k 0)) (d (n "romy-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.20.0") (d #t) (k 0)))) (h "0ymcwwnmc1whyf3w4h7bif1zjgsf1dcsfr4qjlzpl74gppgagi5l")))

