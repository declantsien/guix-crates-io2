(define-module (crates-io ro my romy-sdl) #:use-module (crates-io))

(define-public crate-romy-sdl-0.1.0 (c (n "romy-sdl") (v "0.1.0") (d (list (d (n "romy-core") (r "^0.1.0") (d #t) (k 0)) (d (n "romy-sdl2") (r "^0.32.2") (f (quote ("bundled" "static-link"))) (k 0)))) (h "1l3bvw1h0s3clfy2ndw3s2rd8cdab7abzijc4l912r2n1hnz1cd8")))

(define-public crate-romy-sdl-0.1.1 (c (n "romy-sdl") (v "0.1.1") (d (list (d (n "romy-core") (r "^0.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.32.2") (f (quote ("bundled" "static-link"))) (k 0)))) (h "1d91jq6m55d3ksla7l5ghbf46rqsfz24qd5n4r9zyfvw2jygz0pm")))

