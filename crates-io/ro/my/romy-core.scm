(define-module (crates-io ro my romy-core) #:use-module (crates-io))

(define-public crate-romy-core-0.1.0 (c (n "romy-core") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)))) (h "14ccrq07562cc5jlgg73iv5mwfl2377wyj75pk3m4bxv6is1p2kw")))

(define-public crate-romy-core-0.2.0 (c (n "romy-core") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)))) (h "1hiblag31fb0bygr3v13xvv5i00pk6anrwlk31j5y78nyldl32zp")))

