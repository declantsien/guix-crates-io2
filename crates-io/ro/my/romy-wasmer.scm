(define-module (crates-io ro my romy-wasmer) #:use-module (crates-io))

(define-public crate-romy-wasmer-0.1.0 (c (n "romy-wasmer") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "romy-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.3.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1nlz5gxz83ns7pfgazfk2hx2r7wr989hjdqi2vh41sggdqlfaa1b")))

(define-public crate-romy-wasmer-0.2.0 (c (n "romy-wasmer") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "romy-core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.12.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0cc13g2xppqn38mjxfcxqdkz6smr1rrkjakx3igw0lyvy8a7dsdw")))

