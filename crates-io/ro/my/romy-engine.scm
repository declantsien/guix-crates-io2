(define-module (crates-io ro my romy-engine) #:use-module (crates-io))

(define-public crate-romy-engine-0.1.0 (c (n "romy-engine") (v "0.1.0") (d (list (d (n "image") (r "^0.20.1") (d #t) (k 0)) (d (n "lewton") (r "^0.9.3") (d #t) (k 0)) (d (n "romy-core") (r "^0.1.0") (d #t) (k 0)))) (h "1180r752cbv2slr0yi0nl52qcfyz4gm5ibmjcdp44x0mky5dr0j5")))

(define-public crate-romy-engine-0.2.0 (c (n "romy-engine") (v "0.2.0") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "lewton") (r "^0.9.4") (d #t) (k 0)) (d (n "romy-core") (r "^0.2.0") (d #t) (k 0)))) (h "13g6zqgxqqgjdcnhxxi1xr3xik9zd6zjw6swq1vxvffqg8c1xlaq")))

