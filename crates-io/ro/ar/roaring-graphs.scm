(define-module (crates-io ro ar roaring-graphs) #:use-module (crates-io))

(define-public crate-roaring-graphs-0.10.0 (c (n "roaring-graphs") (v "0.10.0") (d (list (d (n "cov-mark") (r "^1.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "roaring") (r "^0.10") (d #t) (k 0)))) (h "18d7gl10jdbnsj9j1y7z9kchi2j7765d63227996p4j1mv86msq7")))

(define-public crate-roaring-graphs-0.10.1 (c (n "roaring-graphs") (v "0.10.1") (d (list (d (n "cov-mark") (r "^1.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "roaring") (r "^0.10") (d #t) (k 0)))) (h "1615zfijrxvrjxbn7hjlvmzn2d6fpfp2yf98fnz4zylxf800pmlm")))

(define-public crate-roaring-graphs-0.11.0 (c (n "roaring-graphs") (v "0.11.0") (d (list (d (n "cov-mark") (r "^1.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "roaring") (r "^0.10") (d #t) (k 0)))) (h "1212mx1jcpfka39l2pq60yv7rmz9x35nsp0pfz42rf4bmxf0941a")))

(define-public crate-roaring-graphs-0.11.1 (c (n "roaring-graphs") (v "0.11.1") (d (list (d (n "cov-mark") (r "^1.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "roaring") (r "^0.10") (d #t) (k 0)))) (h "06c2km8cf8adm107ibsaiirwzxln3fp8gy3aw6fnqf6xg4h43bir")))

(define-public crate-roaring-graphs-0.11.2 (c (n "roaring-graphs") (v "0.11.2") (d (list (d (n "cov-mark") (r "^1.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "roaring") (r "^0.10") (d #t) (k 0)))) (h "09fbnb2n2rl2q266j3pcirwag40srk3nyqxgj1qi2awb5py1ag03")))

(define-public crate-roaring-graphs-0.11.3 (c (n "roaring-graphs") (v "0.11.3") (d (list (d (n "cov-mark") (r "^1.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "roaring") (r "^0.10") (d #t) (k 0)))) (h "083cqcja76zmzv1qr62a68x44mqhbj51phqhxmwnlpp64bxpd90s")))

(define-public crate-roaring-graphs-0.11.4 (c (n "roaring-graphs") (v "0.11.4") (d (list (d (n "cov-mark") (r "^1.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "roaring") (r "^0.10") (d #t) (k 0)))) (h "1lr6x45ln8i3q33gp7y8938sw7wj58dqyjhsz0sg6b8jj70kz21r")))

(define-public crate-roaring-graphs-0.11.5 (c (n "roaring-graphs") (v "0.11.5") (d (list (d (n "cov-mark") (r "^1.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "roaring") (r "^0.10") (d #t) (k 0)))) (h "08dz3vv36hs24gmflbjz0c7whakln1qn2x3s7xf9lr72y9raxk14")))

(define-public crate-roaring-graphs-0.12.0 (c (n "roaring-graphs") (v "0.12.0") (d (list (d (n "cov-mark") (r "^1.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roaring") (r "^0.10") (d #t) (k 0)))) (h "07yn6lxaigph6flp3jljgamc89vqsc3s3m43ry9fxcspm6v6sfzz")))

