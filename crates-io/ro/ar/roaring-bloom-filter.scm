(define-module (crates-io ro ar roaring-bloom-filter) #:use-module (crates-io))

(define-public crate-roaring-bloom-filter-0.1.0 (c (n "roaring-bloom-filter") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "roaring") (r "^0.8.1") (d #t) (k 0)))) (h "1z0j4xvqjjk94ybcbbkj4w8c46v578cjx0m40jqla4la3i686pz3")))

(define-public crate-roaring-bloom-filter-0.1.1 (c (n "roaring-bloom-filter") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "roaring") (r "^0.8.1") (d #t) (k 0)))) (h "15j1pwdgj36n66qmlxsq44z677qd8icqfm3gdxwy0aa7ycsfld1j")))

(define-public crate-roaring-bloom-filter-0.1.2 (c (n "roaring-bloom-filter") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "roaring") (r "^0.8.1") (d #t) (k 0)))) (h "1zmy7zp0jjj0nk9mld1v89sdi1rwrpdjwdv7l40ly9wfg5vkm6j0")))

(define-public crate-roaring-bloom-filter-0.2.0 (c (n "roaring-bloom-filter") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "roaring") (r "^0.9.0") (d #t) (k 0)))) (h "0gmgc7gc9s2dn443c63x4vcsn9lf2d0yziqkkn1g447naa2x85m7")))

