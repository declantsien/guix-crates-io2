(define-module (crates-io ro ar roarsvg) #:use-module (crates-io))

(define-public crate-roarsvg-0.1.0 (c (n "roarsvg") (v "0.1.0") (d (list (d (n "lyon_path") (r "^1.0.4") (d #t) (k 0)) (d (n "usvg") (r "^0.36.0") (d #t) (k 0)))) (h "1zaj9y91380m9splzia136642gy9bawdlrf3n5pim67lsw2my748")))

(define-public crate-roarsvg-0.2.0 (c (n "roarsvg") (v "0.2.0") (d (list (d (n "lyon_path") (r "^1.0.4") (d #t) (k 0)) (d (n "usvg") (r "^0.36.0") (d #t) (k 0)))) (h "14b8jgp760hckm11pk30214vw23nva16dqh8zz020jnq4wqzii25")))

(define-public crate-roarsvg-0.2.1 (c (n "roarsvg") (v "0.2.1") (d (list (d (n "lyon_path") (r "^1.0.4") (d #t) (k 0)) (d (n "usvg") (r "^0.36.0") (d #t) (k 0)))) (h "18pjqj3n77nm4z77wd0jfdwfhjp6idiai6kmvm253zb611h2r5a7")))

(define-public crate-roarsvg-0.3.0 (c (n "roarsvg") (v "0.3.0") (d (list (d (n "lyon_path") (r "^1.0.4") (d #t) (k 0)) (d (n "usvg") (r "^0.36.0") (d #t) (k 0)))) (h "1gpirivjzn736frjqwkdz5kmqjbxz40zjhhw3mg6yh6q0jmjfrl3")))

(define-public crate-roarsvg-0.3.1 (c (n "roarsvg") (v "0.3.1") (d (list (d (n "lyon_path") (r "^1.0.4") (d #t) (k 0)) (d (n "usvg") (r "^0.36.0") (d #t) (k 0)))) (h "0fshdzy32q0zbk9qcgjswx39cl24dpjy1ay1gzc05fim1s3349va")))

(define-public crate-roarsvg-0.4.0 (c (n "roarsvg") (v "0.4.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "console_error_panic_hook") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "lyon_path") (r "^1.0.4") (d #t) (k 0)) (d (n "usvg") (r "^0.36.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (f (quote ("serde" "serde-serialize"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("Blob" "CanvasRenderingContext2d" "CssStyleDeclaration" "Document" "Element" "EventTarget" "File" "FileList" "HtmlLabelElement" "HtmlInputElement" "HtmlElement" "MouseEvent" "Url" "Node" "Window" "console"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "199ms60hq2hn4lxkrn9f6906dp585s3lbcvv4a04yp127brzjh7x")))

(define-public crate-roarsvg-0.4.1 (c (n "roarsvg") (v "0.4.1") (d (list (d (n "console_error_panic_hook") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "lyon_path") (r "^1.0.4") (d #t) (k 0)) (d (n "usvg") (r "^0.36.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (f (quote ("serde" "serde-serialize"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("Blob" "CanvasRenderingContext2d" "CssStyleDeclaration" "Document" "Element" "EventTarget" "File" "FileList" "HtmlLabelElement" "HtmlInputElement" "HtmlElement" "MouseEvent" "Url" "Node" "Window" "console"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "07xzvxz5iwrp1zlxwyyb4dk1m14kfxiivczyn65km4c01bf11mkd")))

