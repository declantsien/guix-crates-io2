(define-module (crates-io ro ar roaring) #:use-module (crates-io))

(define-public crate-roaring-0.0.1 (c (n "roaring") (v "0.0.1") (h "116q7fhjw4wl3vi79yw83v09h0xzgrxr6fd88i4z11p5k0kxn038")))

(define-public crate-roaring-0.0.8 (c (n "roaring") (v "0.0.8") (h "1hg089c7ci3idf251ayn1ml6nxdmrzyb0v5rhf1hfnr739bcv6gb")))

(define-public crate-roaring-0.0.9 (c (n "roaring") (v "0.0.9") (h "06sm5h4v6k3abvk2dmv3kzhd2vc0hivf3hf45va6n63f9mvhppqh")))

(define-public crate-roaring-0.0.10 (c (n "roaring") (v "0.0.10") (h "1kmy096rviyj8bb9gnkzjzfkln3q2g60w0hz2jf61jhhal0dnvx1")))

(define-public crate-roaring-0.0.11 (c (n "roaring") (v "0.0.11") (h "1mqr0ih44n27dq1jj2zjdvpz9l7iglv2ay9jh7qj9hwggjysn7lm")))

(define-public crate-roaring-0.0.12 (c (n "roaring") (v "0.0.12") (h "1nbk4sm2lzw4bv49alba9apq3bnls6yalcivn9pv86ymlkx6kljn")))

(define-public crate-roaring-0.0.13 (c (n "roaring") (v "0.0.13") (h "1d5s19caqy3z6gyadnlwx3kmv6zz5ksym45g14qzar0k3flpxk15")))

(define-public crate-roaring-0.0.14 (c (n "roaring") (v "0.0.14") (h "11y659hl3vrm8z71jg3l85aqwkqcp493mggy6b3byb7qdkj2dixx")))

(define-public crate-roaring-0.0.15 (c (n "roaring") (v "0.0.15") (h "1gcga8b1wl6j73jgx1s8a2hkvrl0082sylfbpg8p6lhdl3k76lvn")))

(define-public crate-roaring-0.1.0 (c (n "roaring") (v "0.1.0") (h "05gawkkz4rhaykvmg1yscxb9gzj0fxv3z7r1dyx6kbb31scnh245")))

(define-public crate-roaring-0.2.0 (c (n "roaring") (v "0.2.0") (h "099a3yz0yandgnb93bi06gvkb79gw0fbwah65cxz6j29qiyjl6sb")))

(define-public crate-roaring-0.2.1 (c (n "roaring") (v "0.2.1") (h "01kl8hr4r1xjpidax6l2xpnx32wm216qdcr1h5ni75zp5jfcxb6l")))

(define-public crate-roaring-0.2.2 (c (n "roaring") (v "0.2.2") (h "17r90rp2k6sa3qxm7zx8iimxcc39jqd3fdcdm7varjw58wg5x9mm")))

(define-public crate-roaring-0.3.1 (c (n "roaring") (v "0.3.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1jnx0scrdavixl5hhvxrsif55dqpznfgmnwzbxr4aqg0zsv2rfj4")))

(define-public crate-roaring-0.3.2 (c (n "roaring") (v "0.3.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1pvm3b36iny2nqy12df25wlp2msajlafijlr1x2p7ll7wqb738g8")))

(define-public crate-roaring-0.4.0 (c (n "roaring") (v "0.4.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "01qvgcdxqgir3igp89v0di9gyn00gcdmmj9xyrr0mvh4j3xjd7ln")))

(define-public crate-roaring-0.4.2 (c (n "roaring") (v "0.4.2") (d (list (d (n "num") (r "^0.1.36") (d #t) (k 0)))) (h "1babn8p39w2wk0vk3b2px0bnfdwz3dsx3sm4vpqrqvgirzsqgw29")))

(define-public crate-roaring-0.5.0 (c (n "roaring") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)))) (h "1n7xkvb09q24i07mpmqpb81szn62vpvz1fvjwzpzzmhs44xrzrmk")))

(define-public crate-roaring-0.5.1 (c (n "roaring") (v "0.5.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)))) (h "1lmh0n4526v3kl39wj9r6ak90sgs9y20k1g7m46raj1scjb5mzbw")))

(define-public crate-roaring-0.5.2 (c (n "roaring") (v "0.5.2") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)))) (h "0bf25j9sd3vkxg6j0fxw9ysn34x2iiv9g8lz91bjlws47rfhxwja")))

(define-public crate-roaring-0.6.0 (c (n "roaring") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "07lc66xzb8lqnnz5sfg7xcx68nnz0mn2z1db7lyg8wm1yjq40v0x")))

(define-public crate-roaring-0.6.1 (c (n "roaring") (v "0.6.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0xq0av4rs17cii0hyf67rji0h6jxzrxd78gl91chkwkxzfq618lr")))

(define-public crate-roaring-0.6.2 (c (n "roaring") (v "0.6.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "0x4krj2vg3w83wcmxc5h1vxjph86xzz7c1i6g0wlcf44m68hhmgv")))

(define-public crate-roaring-0.6.3 (c (n "roaring") (v "0.6.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "0d9s5fvgnjvbmp0gwiyfsf21x3s36sg7zyg9iqa15zdjp71xnazi")))

(define-public crate-roaring-0.6.4 (c (n "roaring") (v "0.6.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "01138bwz8gia76i1ripzbq2zqd7jpdpb8p0jmgn7rl15iwfb8q2d")))

(define-public crate-roaring-0.6.5 (c (n "roaring") (v "0.6.5") (d (list (d (n "bytemuck") (r "^1.5.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "retain_mut") (r "^0.1.2") (d #t) (k 0)))) (h "13zq5bm34w31bmqpzrwszgcqcfll5qgajmnks6d3b4cfj554lx66")))

(define-public crate-roaring-0.6.6 (c (n "roaring") (v "0.6.6") (d (list (d (n "bytemuck") (r "^1.5.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "retain_mut") (r "^0.1.2") (d #t) (k 0)))) (h "0iyllgdkqff3cky5cmxlccad41nvl1hlfgxfb12i8bdv1fmygcm4")))

(define-public crate-roaring-0.6.7 (c (n "roaring") (v "0.6.7") (d (list (d (n "bytemuck") (r "^1.5.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "retain_mut") (r "^0.1.2") (d #t) (k 0)))) (h "02capyx1axzi621ipnlm2ff7v6w4f1wnvygdd6pbi263by4glv2k")))

(define-public crate-roaring-0.7.0 (c (n "roaring") (v "0.7.0") (d (list (d (n "bytemuck") (r "^1.5.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "retain_mut") (r "^0.1.2") (d #t) (k 0)))) (h "03qmshl5hfhhph84ldl4jf20f8phr998mfsvqyxljvc7vbf0p7l5")))

(define-public crate-roaring-0.8.0 (c (n "roaring") (v "0.8.0") (d (list (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "retain_mut") (r "^0.1.4") (d #t) (k 0)))) (h "099bcrvnh2cs9zqizpkm06z7fbsmnnxk6bixaba7viv38s0bx5wp")))

(define-public crate-roaring-0.8.1 (c (n "roaring") (v "0.8.1") (d (list (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "retain_mut") (r "^0.1.4") (d #t) (k 0)))) (h "1zaw54cnqc0gi6i9njihrxl126myay7l27rj50lnrc722nbyb8q7")))

(define-public crate-roaring-0.9.0 (c (n "roaring") (v "0.9.0") (d (list (d (n "bytemuck") (r "^1.7.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "retain_mut") (r "^0.1.6") (d #t) (k 0)))) (h "1xx5hl7vplvn7ljw8libpvs04j3dpdhg233yzrb9j09j9smrqlyx") (f (quote (("simd")))) (r "1.56.1")))

(define-public crate-roaring-0.10.0 (c (n "roaring") (v "0.10.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bytemuck") (r "^1.7.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "retain_mut") (r "=0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "0vzdap52h6s81pg0vf87ymx42x4cxvc1fj6kv739k1djxdgmppj8") (f (quote (("simd")))) (r "1.56.1")))

(define-public crate-roaring-0.10.1 (c (n "roaring") (v "0.10.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bytemuck") (r "^1.7.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "retain_mut") (r "=0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "0wcgprdw49xy7w00zzspbhrq8f1kvlwqasmfxh8y1gd84vlba3zg") (f (quote (("simd")))) (r "1.56.1")))

(define-public crate-roaring-0.10.2 (c (n "roaring") (v "0.10.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bytemuck") (r "^1.7.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "retain_mut") (r "=0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "0wvq3hcif2b8yzqmgazgi9n73jf6lcarfpl9b10q7xc7hp7va1k1") (f (quote (("simd")))) (r "1.65.0")))

(define-public crate-roaring-0.10.3 (c (n "roaring") (v "0.10.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bytemuck") (r "^1.14.3") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "1gx65l6yx848sjw2bfcxhx0qfldp2m28dwl6rcbf002kln0p1ix1") (f (quote (("simd") ("default" "std")))) (s 2) (e (quote (("std" "dep:bytemuck" "dep:byteorder") ("serde" "dep:serde" "std")))) (r "1.65.0")))

(define-public crate-roaring-0.10.4 (c (n "roaring") (v "0.10.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bytemuck") (r "^1.14.3") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "1l84m1s1lmqd9kcri0fyjpg40f696yxdcvfrpjhv7z04lqjlqvxj") (f (quote (("simd") ("default" "std")))) (s 2) (e (quote (("std" "dep:bytemuck" "dep:byteorder") ("serde" "dep:serde" "std")))) (r "1.65.0")))

