(define-module (crates-io ro m- rom-thumbnails) #:use-module (crates-io))

(define-public crate-rom-thumbnails-0.1.1 (c (n "rom-thumbnails") (v "0.1.1") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "cxx") (r "^1.0.110") (f (quote ("c++20"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0fjpl4pjzdwijvbml7m3m2gka5fc66jly440r12n2p1vkc5kn3jz") (f (quote (("default")))) (s 2) (e (quote (("cxx" "dep:cxx"))))))

(define-public crate-rom-thumbnails-0.1.2 (c (n "rom-thumbnails") (v "0.1.2") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1fmxi5p0pqp98v6jfn5lk4f08qvczwam2kkrd79dhmkiw02zmr65") (f (quote (("default"))))))

(define-public crate-rom-thumbnails-0.1.3 (c (n "rom-thumbnails") (v "0.1.3") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0hc02dnbpp2h8b1vs4dksahrwrgv6ajli161ljd8asxh2rrz72n0") (f (quote (("default"))))))

(define-public crate-rom-thumbnails-0.1.4 (c (n "rom-thumbnails") (v "0.1.4") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0cbax89rbyxc6x1ph2hr4rbsjvs5xl2mj222a6wxfwr716yk2gwp") (f (quote (("default"))))))

(define-public crate-rom-thumbnails-0.1.5 (c (n "rom-thumbnails") (v "0.1.5") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0zawhma3c3dwv3qmhmizqii16lm9qljdhmyxfjj8wcqdvm5qv2lv") (f (quote (("default"))))))

(define-public crate-rom-thumbnails-0.1.6 (c (n "rom-thumbnails") (v "0.1.6") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0azpwl3xacc5ih44d6qwim28548hg76wbywswg6m1a4niyb3nr1y") (f (quote (("default"))))))

