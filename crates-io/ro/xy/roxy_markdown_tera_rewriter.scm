(define-module (crates-io ro xy roxy_markdown_tera_rewriter) #:use-module (crates-io))

(define-public crate-roxy_markdown_tera_rewriter-0.1.0 (c (n "roxy_markdown_tera_rewriter") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "roxy_core") (r "^0.1.0") (d #t) (k 0)))) (h "1pnh9z1gdnf8f4p1fzcmfidc9zdlm0wdcrkvyca10qp7w9njvls6")))

