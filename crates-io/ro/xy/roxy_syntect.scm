(define-module (crates-io ro xy roxy_syntect) #:use-module (crates-io))

(define-public crate-roxy_syntect-0.1.0 (c (n "roxy_syntect") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "roxy_core") (r "^0.1.0") (d #t) (k 0)) (d (n "syntect") (r "^5.1.0") (d #t) (k 0)))) (h "0mrrw5yyzg0wy2fgky72lrgznrzb8f2ckkb8bm9jamlyqqgm0254")))

