(define-module (crates-io ro xy roxy_markdown_parser) #:use-module (crates-io))

(define-public crate-roxy_markdown_parser-0.1.0 (c (n "roxy_markdown_parser") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "roxy_core") (r "^0.1.0") (d #t) (k 0)))) (h "1jp54h25nl80z2kd3cdyby58579fb5i07qcdaqqw5vvpxqyx96jc")))

