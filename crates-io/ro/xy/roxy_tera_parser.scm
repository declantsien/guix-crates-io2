(define-module (crates-io ro xy roxy_tera_parser) #:use-module (crates-io))

(define-public crate-roxy_tera_parser-0.1.0 (c (n "roxy_tera_parser") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "roxy_core") (r "^0.1.0") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)))) (h "0dja58v43ngxhwpjm0va4snxlrz8ycmm8gkdcipfvb104psf67gf")))

