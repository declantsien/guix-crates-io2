(define-module (crates-io ro xy roxy_core) #:use-module (crates-io))

(define-public crate-roxy_core-0.1.0 (c (n "roxy_core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "16s0v8l91ybv7zcgyiyzh5841g6varh9va5szklqng96ssksph8d")))

(define-public crate-roxy_core-0.1.1 (c (n "roxy_core") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1si38mm1mkh186swwk95mzjfz9b7ck8p4sb0kgsd54200bss0ahx")))

