(define-module (crates-io ro di rodio-xm) #:use-module (crates-io))

(define-public crate-rodio-xm-0.1.0 (c (n "rodio-xm") (v "0.1.0") (d (list (d (n "libxm") (r "^1.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "1vj7hgbh3swayi7rjwkgvag418vjp0hrvyarcdym67nnkqxc1r8c")))

(define-public crate-rodio-xm-0.1.1 (c (n "rodio-xm") (v "0.1.1") (d (list (d (n "libxm") (r "^1.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "0d47lih6d42kxhsps4ijkvfwli15lvyi3cj2s1wisl0f739lc1ka")))

