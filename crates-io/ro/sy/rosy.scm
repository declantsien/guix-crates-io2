(define-module (crates-io ro sy rosy) #:use-module (crates-io))

(define-public crate-rosy-0.0.0 (c (n "rosy") (v "0.0.0") (d (list (d (n "aloxide") (r "^0.0.2") (k 1)))) (h "17sw2ajcjq603m1y1dcmbg6bzd8kn82pi7c07akbf3zmyppsapcr") (f (quote (("download" "aloxide/download")))) (l "ruby")))

(define-public crate-rosy-0.0.1 (c (n "rosy") (v "0.0.1") (d (list (d (n "aloxide") (r "^0.0.8") (k 1)) (d (n "rustc_version") (r "^0.2") (o #t) (d #t) (k 1)) (d (n "static_assertions") (r "^0.3.0") (d #t) (k 2)))) (h "1jfnbwyafdnr3y5qrfcm4by3zxwkbildzvfjs8lvvwf5n4yldncy") (f (quote (("static") ("ruby_2_6") ("download" "aloxide/download") ("default" "static")))) (l "ruby")))

(define-public crate-rosy-0.0.2 (c (n "rosy") (v "0.0.2") (d (list (d (n "aloxide") (r "^0.0.8") (k 1)) (d (n "rustc_version") (r "^0.2") (o #t) (d #t) (k 1)) (d (n "static_assertions") (r "^0.3.0") (d #t) (k 2)))) (h "06k59nsyfm50gh01ph5ysvylhm97a85jyg5khqrchlnm25hlwsw3") (f (quote (("static") ("ruby_2_6") ("download" "aloxide/download") ("default" "static") ("_skip_linking")))) (l "ruby")))

(define-public crate-rosy-0.0.3 (c (n "rosy") (v "0.0.3") (d (list (d (n "aloxide") (r "^0.0.8") (k 1)) (d (n "rustc_version") (r "^0.2") (o #t) (d #t) (k 1)) (d (n "static_assertions") (r "^0.3.0") (d #t) (k 2)))) (h "1vbd52dn1mdc88nl78smr2gfhhpild4nd8gqbvk53h6x2qwa12ia") (f (quote (("static") ("ruby_2_6") ("download" "aloxide/download") ("default" "static") ("_skip_linking")))) (l "ruby")))

(define-public crate-rosy-0.0.4 (c (n "rosy") (v "0.0.4") (d (list (d (n "aloxide") (r "^0.0.8") (k 1)) (d (n "static_assertions") (r "^0.3.0") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1d5n08qnx3y82dgnv7rjh6cmqcc58ig2vy4hgnsmj6w19csqfsrk") (f (quote (("static") ("ruby_2_6") ("download" "aloxide/download") ("default" "static") ("_skip_linking")))) (l "ruby")))

(define-public crate-rosy-0.0.5 (c (n "rosy") (v "0.0.5") (d (list (d (n "aloxide") (r "^0.0.8") (k 1)) (d (n "static_assertions") (r "^0.3.0") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1hnv734sbz7pkcsycsxic1xh38drqy2h8879innc88wxak3hgdg3") (f (quote (("static") ("ruby_2_6") ("download" "aloxide/download") ("default" "static") ("_skip_linking")))) (l "ruby")))

(define-public crate-rosy-0.0.6 (c (n "rosy") (v "0.0.6") (d (list (d (n "aloxide") (r "^0.0.8") (k 1)) (d (n "static_assertions") (r "^0.3.0") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0pm1cyn4sjplmdbg8mwlibxn123yhmjzqmiggjvk9wrvwjilwjlv") (f (quote (("static") ("ruby_2_6") ("download" "aloxide/download") ("default" "static") ("_skip_linking")))) (l "ruby")))

(define-public crate-rosy-0.0.7 (c (n "rosy") (v "0.0.7") (d (list (d (n "aloxide") (r "^0.0.8") (k 1)) (d (n "static_assertions") (r "^0.3.0") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1gk09fw17cgpw2wbsqmxvqknds21d5mamml62fbqw5fgfjlg2cpb") (f (quote (("static") ("ruby_2_6") ("download" "aloxide/download") ("default" "static") ("_skip_linking")))) (l "ruby")))

(define-public crate-rosy-0.0.8 (c (n "rosy") (v "0.0.8") (d (list (d (n "aloxide") (r "^0.0.8") (k 1)) (d (n "static_assertions") (r "^0.3.0") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1qlmcvf8kx9pclbzw9qj9zzb5awkd5ky859nig91b8llnwdvg6qj") (f (quote (("static") ("ruby_2_6") ("download" "aloxide/download") ("default" "static") ("_skip_linking")))) (l "ruby")))

(define-public crate-rosy-0.0.9 (c (n "rosy") (v "0.0.9") (d (list (d (n "aloxide") (r "^0.0.8") (k 1)) (d (n "static_assertions") (r "^0.3.0") (d #t) (k 2)) (d (n "version_check") (r "^0.1") (o #t) (d #t) (k 1)))) (h "07widdd9vi4wa4605bgbfrz2d4nc944q1574wx4g7j1sil23i865") (f (quote (("static") ("ruby_2_6") ("download" "aloxide/download") ("default" "static") ("_skip_linking")))) (l "ruby")))

