(define-module (crates-io ro s2 ros2-pkginfogen) #:use-module (crates-io))

(define-public crate-ros2-pkginfogen-0.1.0 (c (n "ros2-pkginfogen") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.16.0") (d #t) (k 0)) (d (n "xml") (r "^0.8.10") (d #t) (k 0)))) (h "1fhhq2lsr3kwv14isry4ys7kzjlxp6s8mcnvva77r5532650wb4r")))

