(define-module (crates-io ro rd rordle) #:use-module (crates-io))

(define-public crate-rordle-0.1.0 (c (n "rordle") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0d1yps2bbjsdlwa4byp82kb2y965nvaydmbfl1h8fw262hgcyn04")))

(define-public crate-rordle-0.1.1 (c (n "rordle") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "08wa14zsjcf3l8l51hkdncr9ag1ws2dnv43q4vjjqkv0d2gwf4q4")))

(define-public crate-rordle-0.1.2 (c (n "rordle") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "13sg4wkjqj9c5a0b9qkcyyribypza66j7g4682dgyby2z0ri3zw4")))

(define-public crate-rordle-0.2.0 (c (n "rordle") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1arrn7ph5bg3wkvcr2x3s566c0rrar8sib55hihz5wg1bz035z7g")))

