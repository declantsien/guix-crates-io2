(define-module (crates-io ro lt rolt) #:use-module (crates-io))

(define-public crate-rolt-0.1.0+Jolt-5.0.0 (c (n "rolt") (v "0.1.0+Jolt-5.0.0") (d (list (d (n "joltc-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1ygvh73p4ry3j543n7a0svwyh0drfp46bf7j29z6bf062cd6g4fm") (f (quote (("object-layer-u32" "joltc-sys/object-layer-u32") ("double-precision" "joltc-sys/double-precision"))))))

(define-public crate-rolt-0.1.1+Jolt-5.0.0 (c (n "rolt") (v "0.1.1+Jolt-5.0.0") (d (list (d (n "joltc-sys") (r "^0.1.1") (d #t) (k 0)))) (h "041dbdzhrcrrdpv7r18dpprbdkcjvphvshjhj0jzvgh5kx4apgv7") (f (quote (("object-layer-u32" "joltc-sys/object-layer-u32") ("double-precision" "joltc-sys/double-precision"))))))

(define-public crate-rolt-0.2.0+Jolt-5.0.0 (c (n "rolt") (v "0.2.0+Jolt-5.0.0") (d (list (d (n "joltc-sys") (r "^0.2.0") (d #t) (k 0)))) (h "017xj3fy8k2bsf505nya8amarhx9302dn3ndhs7yyjlwcczxlrlh") (f (quote (("object-layer-u32" "joltc-sys/object-layer-u32") ("double-precision" "joltc-sys/double-precision"))))))

(define-public crate-rolt-0.3.0+Jolt-5.0.0 (c (n "rolt") (v "0.3.0+Jolt-5.0.0") (d (list (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "joltc-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)))) (h "05215bvlkxx69p0gipdwlnsndgc3sd6xyzgb4xymf9dh6qidr79j") (f (quote (("object-layer-u32" "joltc-sys/object-layer-u32") ("double-precision" "joltc-sys/double-precision"))))))

(define-public crate-rolt-0.3.1+Jolt-5.0.0 (c (n "rolt") (v "0.3.1+Jolt-5.0.0") (d (list (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "joltc-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)))) (h "0q9rbpdmhsnxsxh90sxlxgj1x7x3zr7wvzi6qzsnkxqnbhvpa1nx") (f (quote (("object-layer-u32" "joltc-sys/object-layer-u32") ("double-precision" "joltc-sys/double-precision"))))))

