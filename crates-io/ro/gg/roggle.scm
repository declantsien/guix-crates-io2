(define-module (crates-io ro gg roggle) #:use-module (crates-io))

(define-public crate-roggle-0.1.0 (c (n "roggle") (v "0.1.0") (h "1r2jm5d69pq2j367pcayagrqzryscv4ckiqszmnl1b9qbvafjp47")))

(define-public crate-roggle-0.2.0 (c (n "roggle") (v "0.2.0") (h "0gwi3n5dqn6vn834bkciv22b2pdv8fn5ramrq58pz672xdys75rf")))

(define-public crate-roggle-0.3.0 (c (n "roggle") (v "0.3.0") (h "1bf09ahdbhhz3bk99dg9mny2iwls9ad93xpswa136dgvzxdpzf0a")))

(define-public crate-roggle-0.4.0 (c (n "roggle") (v "0.4.0") (h "0y8nskcs1kcil24f3a1pvyrm7rymhbs8ngx6a6zfpmmb5vgrcbva")))

(define-public crate-roggle-0.5.0 (c (n "roggle") (v "0.5.0") (h "1f5r2n1sa98j32sapidvd0gyd5w0fhslilpg3kzm1k0cd7aah5gh")))

(define-public crate-roggle-0.5.1 (c (n "roggle") (v "0.5.1") (h "0nd8nj6s10vgn3dj6w7ilphi0vcf7yvixr52iy1hyn3x66fzgbm7")))

(define-public crate-roggle-0.5.2 (c (n "roggle") (v "0.5.2") (h "0nl6xraf8v1nlw2mygy1b5pfjz2101bai93lvhsawg4s02lrxpji")))

(define-public crate-roggle-0.6.0 (c (n "roggle") (v "0.6.0") (h "1ljf1l968z20wh9if4bx72qinwbk8ps7iz1lnmsbrvr2rdh37ca6")))

(define-public crate-roggle-0.7.0 (c (n "roggle") (v "0.7.0") (h "1qzkmwaj2122b9av781r9yih08wxslk5zrf78g6zhz3izrfzyi6w")))

(define-public crate-roggle-0.7.1 (c (n "roggle") (v "0.7.1") (h "0vcwi0q25skc0fd0d9gq5f4c0n0pyfv1n38gbrs0zbry4x0ibh06")))

(define-public crate-roggle-0.7.2 (c (n "roggle") (v "0.7.2") (h "1p4qybf0nkrriny5v13886bzdkwmsxiyc4x4b37l8wrz6ms3j07g")))

