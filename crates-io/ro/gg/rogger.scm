(define-module (crates-io ro gg rogger) #:use-module (crates-io))

(define-public crate-rogger-0.1.0 (c (n "rogger") (v "0.1.0") (h "17ki4qak2xscgwchcbqkh5gr4hk4a82vr1mw7jf435vg5klhc42f")))

(define-public crate-rogger-0.1.1 (c (n "rogger") (v "0.1.1") (h "024xbiw1079ygdr49g1yqw6ngl29wsi7v6rmfasym4jjg88mp61d") (f (quote (("utc_jst") ("jst"))))))

