(define-module (crates-io ro gg rogg) #:use-module (crates-io))

(define-public crate-rogg-0.1.0 (c (n "rogg") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0ipxyh6az2p1kd75fqs7xccq8cjp7kpn0an4h3g7q90giviz76pr")))

