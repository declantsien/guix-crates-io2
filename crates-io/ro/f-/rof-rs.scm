(define-module (crates-io ro f- rof-rs) #:use-module (crates-io))

(define-public crate-rof-rs-0.1.1 (c (n "rof-rs") (v "0.1.1") (d (list (d (n "rof_rs_core") (r "^0.1.0") (d #t) (k 0)) (d (n "rof_rs_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0hijsakpgkw8w5wlgbx2g0y1yxqfnlgwn721f5s8b9myyydy6gln") (y #t)))

(define-public crate-rof-rs-0.1.3 (c (n "rof-rs") (v "0.1.3") (d (list (d (n "rof_rs_core") (r "^0.1.1") (d #t) (k 0)) (d (n "rof_rs_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1lwiw8z7xikz3aj3a49w8b95f51nji03a26519n1rppf59lrnxqy") (y #t)))

(define-public crate-rof-rs-0.1.4 (c (n "rof-rs") (v "0.1.4") (d (list (d (n "rof_rs_core") (r "^0.1.1") (d #t) (k 0)) (d (n "rof_rs_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1xi5ii1rdb41mwlg6f5qyipms8jdljz6pfs65y1j4gb41cbsq2rh") (y #t)))

(define-public crate-rof-rs-0.1.5 (c (n "rof-rs") (v "0.1.5") (d (list (d (n "rof_rs_core") (r "^0.1.1") (d #t) (k 0)) (d (n "rof_rs_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1cs0k1g2nic5irxfr4y60zz7adkkv589lh2ac2zihp556cd38gig") (y #t)))

(define-public crate-rof-rs-0.1.6 (c (n "rof-rs") (v "0.1.6") (d (list (d (n "rof_rs_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1mnyszs13nm11q06z2w27543yy3nw7bkx8zpxir1m3nifh8vc72r") (y #t)))

(define-public crate-rof-rs-0.1.7 (c (n "rof-rs") (v "0.1.7") (d (list (d (n "rof_rs_macros") (r "^0.1.3") (d #t) (k 0)))) (h "1hwx23yd06gbcfh6s3xq03nczl5yyid4cq4vssn1mzd9p0g4g196")))

