(define-module (crates-io ro fi rofi-plugin-sys) #:use-module (crates-io))

(define-public crate-rofi-plugin-sys-0.1.0 (c (n "rofi-plugin-sys") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.15.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.15.10") (d #t) (k 0)))) (h "11qw8yz53bv0cxa7vpddlxbs2zkfhzcynax7j2dc5d5mq1v0nwyk") (r "1.59.0")))

(define-public crate-rofi-plugin-sys-0.1.1 (c (n "rofi-plugin-sys") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.0.1") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.17.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.17.4") (d #t) (k 0)))) (h "0jszgxszdd8n7b5k7lwfg5yj4a1hizbm3hal8bz9qgf13k623asc") (y #t) (r "1.59.0")))

(define-public crate-rofi-plugin-sys-0.2.0 (c (n "rofi-plugin-sys") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.0.1") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.17.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.17.4") (d #t) (k 0)))) (h "0rd55gvd4rciqjagp9iykpw2g78lld3i80l0rv201klpgxffipds") (r "1.59.0")))

(define-public crate-rofi-plugin-sys-0.3.0 (c (n "rofi-plugin-sys") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.0.1") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.17.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.17.4") (d #t) (k 0)))) (h "13gfq8v3hw9pc1b1f8z99q68vfvkkbr16d7gxzxg43zi1mdgx1my") (r "1.59.0")))

(define-public crate-rofi-plugin-sys-0.3.1 (c (n "rofi-plugin-sys") (v "0.3.1") (d (list (d (n "bitflags") (r "^2.0.1") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.17.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.17.4") (d #t) (k 0)))) (h "08q2ss9brgpwn5qqr16kcxlw88gpq2yj6l4qw8mqar7mgmrrqy43") (r "1.59.0")))

(define-public crate-rofi-plugin-sys-0.3.2 (c (n "rofi-plugin-sys") (v "0.3.2") (d (list (d (n "bitflags") (r "^2.0.1") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.17.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.17.4") (d #t) (k 0)))) (h "008vx3pn3q9h61cfbx0n2p188hcimsc7yq61s4xh7xw037aqrwlv") (r "1.59.0")))

(define-public crate-rofi-plugin-sys-0.4.0 (c (n "rofi-plugin-sys") (v "0.4.0") (d (list (d (n "bitflags") (r "^2.0.1") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.19.2") (d #t) (k 0)) (d (n "glib-sys") (r "^0.19.0") (d #t) (k 0)))) (h "1g150xk17b8zvjqvl10bs6pklhsb3blj3llnl4l8m91f9m221izz") (r "1.59.0")))

