(define-module (crates-io ro fi rofi-menus) #:use-module (crates-io))

(define-public crate-rofi-menus-0.1.0 (c (n "rofi-menus") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0j82qxzlsc4c5b0ld2x8agndr63nqvp7xndx8hqvx8802hvw2mg7")))

(define-public crate-rofi-menus-0.1.1 (c (n "rofi-menus") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1614qr8ql3rggdcjvjvdrky8mssavllvkyw9nsag3id3v7pxhcx5")))

(define-public crate-rofi-menus-0.1.2 (c (n "rofi-menus") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1qr7w97fka2fybkq3hy4lsmv60k76zchbnl60biiyqhmjgvan01j")))

