(define-module (crates-io ro fi rofi-games) #:use-module (crates-io))

(define-public crate-rofi-games-0.0.2 (c (n "rofi-games") (v "0.0.2") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "005hx2dzf6f65hg9jra7493a5ldxcazwwzm4sbbm8w4fs7x9v19m") (y #t)))

