(define-module (crates-io ro fi rofi-randr) #:use-module (crates-io))

(define-public crate-rofi-randr-0.1.0 (c (n "rofi-randr") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30.2") (d #t) (k 0)) (d (n "x11") (r "^2.21.0") (d #t) (k 0)) (d (n "xrandr") (r "^0.2.0") (d #t) (k 0)))) (h "18js7li6q2pwld375bqcrqi26wckp53yl8g8al2qf7z1mywcnsdv")))

