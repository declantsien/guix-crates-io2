(define-module (crates-io ro fi rofi-plugin) #:use-module (crates-io))

(define-public crate-rofi-plugin-0.1.0 (c (n "rofi-plugin") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "genawaiter") (r "^0.99.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "real_c_string") (r "^0.1.1") (d #t) (k 0)) (d (n "terminated") (r "^1.0.0") (d #t) (k 0)))) (h "06rk5hcjr8s060alqy7yzqsmnxlfrjcfrgh6wyjxf0ixsjwsvlhd")))

