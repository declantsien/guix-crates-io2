(define-module (crates-io ro fi rofi-mode) #:use-module (crates-io))

(define-public crate-rofi-mode-0.1.0 (c (n "rofi-mode") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.15.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)) (d (n "pango") (r "^0.15.10") (d #t) (k 0)) (d (n "rofi-plugin-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0a75hjx5zqwpnpql5qdjrf4b7rfsqj4cbv5w4c18lwlbv50gm9dn") (r "1.60.0")))

(define-public crate-rofi-mode-0.1.1 (c (n "rofi-mode") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.15.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)) (d (n "pango") (r "^0.15.10") (d #t) (k 0)) (d (n "rofi-plugin-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0yvwq9kkz86q215xshmgjlra4z1m451acgidpvx5sj5586jyxpw1") (r "1.60.0")))

(define-public crate-rofi-mode-0.2.0 (c (n "rofi-mode") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.15.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)) (d (n "pango") (r "^0.15.10") (d #t) (k 0)) (d (n "rofi-plugin-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1zas3jlx4drg6lqig3j3ka7ffy0sa0m8vj62idj69a8216cb2c4r") (r "1.60.0")))

(define-public crate-rofi-mode-0.2.1 (c (n "rofi-mode") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.15.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)) (d (n "pango") (r "^0.15.10") (d #t) (k 0)) (d (n "rofi-plugin-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1yflqap5vqkarcna8287difjx7isbpf12chd02fncwgkfcsizkg1") (r "1.60.0")))

(define-public crate-rofi-mode-0.2.2 (c (n "rofi-mode") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.15.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)) (d (n "pango") (r "^0.15.10") (d #t) (k 0)) (d (n "rofi-plugin-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0xs9y2d4yr3qpvryxd4rvj2sbakci4bd63kdw6w2ij8f7f6ndf0j") (r "1.60.0")))

(define-public crate-rofi-mode-0.2.3 (c (n "rofi-mode") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.15.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)) (d (n "pango") (r "^0.15.10") (d #t) (k 0)) (d (n "rofi-plugin-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0iy87kn1nnnxjir519q21dk3bal8sgbd8f9iimpzb25f3q6hjf3y") (r "1.60.0")))

(define-public crate-rofi-mode-0.2.4 (c (n "rofi-mode") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.15.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)) (d (n "pango") (r "^0.15.10") (d #t) (k 0)) (d (n "rofi-plugin-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0ifsq06wq15jx56qqsn4336l5m2yzgfa9g4wqxv8vdcb29wjnhxm") (r "1.60.0")))

(define-public crate-rofi-mode-0.3.0 (c (n "rofi-mode") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.142") (d #t) (k 0)) (d (n "pango") (r "^0.17.4") (d #t) (k 0)) (d (n "rofi-plugin-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1c15n8fwv7sfxyzmmqnx72glwk968v1j81x56gqmfr88xgdypb01") (r "1.60.0")))

(define-public crate-rofi-mode-0.3.1 (c (n "rofi-mode") (v "0.3.1") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.142") (d #t) (k 0)) (d (n "pango") (r "^0.17.4") (d #t) (k 0)) (d (n "rofi-plugin-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1fh6id8v1vn8f4rpz05d8sqshq30xmy3zm6jrg7bw4vkgjm6b28v") (r "1.60.0")))

(define-public crate-rofi-mode-0.4.0 (c (n "rofi-mode") (v "0.4.0") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.19.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.142") (d #t) (k 0)) (d (n "pango") (r "^0.19.2") (d #t) (k 0)) (d (n "rofi-plugin-sys") (r "^0.4.0") (d #t) (k 0)))) (h "19w933zg9k469kk5n188gka37x03jql8ywrbgcj83c7wb0lq08xm") (r "1.60.0")))

