(define-module (crates-io ro fi rofisys) #:use-module (crates-io))

(define-public crate-rofisys-0.1.1 (c (n "rofisys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 1)))) (h "0w2h860zxbadlkak9qr5cxgjaknvy4cwvksjqdhyq80krps82jg0")))

(define-public crate-rofisys-0.2.0 (c (n "rofisys") (v "0.2.0") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)) (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 1)))) (h "15fifc7vb81mfhywsvzsani1srw6g5wxvxyk4xkbp9d813zgjq42")))

(define-public crate-rofisys-0.2.1 (c (n "rofisys") (v "0.2.1") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)) (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 1)))) (h "0lvrlz6d66y4ikk2ai7p9qyqda96085y36ymnmmgzpaw1ffxfx1r")))

(define-public crate-rofisys-0.2.2 (c (n "rofisys") (v "0.2.2") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)) (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 1)))) (h "0zlaa61bs6azd15w30nz9ifcl8dak5ymiaqgh2g41kcmjywpg737")))

(define-public crate-rofisys-0.2.3 (c (n "rofisys") (v "0.2.3") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)) (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 1)))) (h "1giy31w6i9gyqbiv74jdc3jp0l9xaixlkg89hs5mz2mkkvdswy0h")))

(define-public crate-rofisys-0.2.4 (c (n "rofisys") (v "0.2.4") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)) (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 1)))) (h "0nf6s6sqzxdvh2f6fasr8qi8cf0v1y8fwyg7h5s77c4vd8ikyz38") (y #t)))

(define-public crate-rofisys-0.2.5 (c (n "rofisys") (v "0.2.5") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)) (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 1)))) (h "0xzglgp4jd58l5j993gx94gspx4q5r3bb6j0x5f8bc4l8frmd3nh")))

(define-public crate-rofisys-0.2.6 (c (n "rofisys") (v "0.2.6") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)) (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 1)))) (h "1xwxzalg455930162m8jpnhyfy2w253854dvaw348cm8gg38ckvp")))

(define-public crate-rofisys-0.2.7 (c (n "rofisys") (v "0.2.7") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)) (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 1)))) (h "1w8479b0j5s73ii3s2vnqj40vrckd1zv7gwzmafdw2n3kngnzrv9")))

(define-public crate-rofisys-0.3.0 (c (n "rofisys") (v "0.3.0") (d (list (d (n "autotools") (r "^0.2.4") (d #t) (k 1)) (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 1)))) (h "0n30nzm04kx2vcxhlzwdrf2r1h30z24spdmarxah393j12gx1sbn")))

