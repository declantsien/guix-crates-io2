(define-module (crates-io ro mu romulan) #:use-module (crates-io))

(define-public crate-romulan-0.1.0 (c (n "romulan") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "redox_uefi") (r "^0.1.0") (d #t) (k 0)))) (h "007v8m4g9wzq2rrxcg8amhwi6fjmvjmsayxhk2zj0manzl9k8ihw")))

(define-public crate-romulan-0.1.1 (c (n "romulan") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "redox_uefi") (r "^0.1.0") (d #t) (k 0)))) (h "0f60hvjdxkc35c7lw6cxfw9473rnr1qdxb62j8phhs7p5w3yn941")))

