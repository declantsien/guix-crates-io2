(define-module (crates-io ro mu romu) #:use-module (crates-io))

(define-public crate-romu-0.1.0 (c (n "romu") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0sx38bkmqzqz37d48syhrkd76zaksb8alsspqis2gn5c35mgr7ki") (f (quote (("unstable_tls" "std" "tls") ("unstable_simd") ("tls" "std") ("std") ("default" "std" "tls" "getrandom")))) (y #t)))

(define-public crate-romu-0.1.1 (c (n "romu") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)))) (h "03vkync7gyxlf7xa1wxi18dap5hxiw0m2bqfm0f4hyckd43di5ja") (f (quote (("unstable_tls" "std" "tls") ("unstable_simd") ("tls" "std") ("std") ("default" "std" "tls" "getrandom")))) (y #t)))

(define-public crate-romu-0.1.2 (c (n "romu") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0zpqhjx0nsrjj47dzi8kk64y6h2597i44xvjljppg95zkk6zg8b0") (f (quote (("unstable_tls" "std" "tls") ("unstable_simd") ("tls" "std") ("std") ("default" "std" "tls" "getrandom"))))))

(define-public crate-romu-0.2.0 (c (n "romu") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)))) (h "00ssjf9a7fxk5jjywrh0kd58yzc8pl7rk3jd3nsq3gga0wpj9ixv") (f (quote (("unstable_tls" "std" "tls") ("unstable_simd") ("tls" "std") ("std") ("default" "std" "tls" "getrandom"))))))

(define-public crate-romu-0.2.1 (c (n "romu") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1mncyp82ngadc3b74via5vr0r4gssqm2fxgcqwy4fvl2yshpbakl") (f (quote (("unstable_tls" "std" "tls") ("unstable_simd") ("tls" "std") ("std") ("default" "std" "tls" "getrandom"))))))

(define-public crate-romu-0.3.0 (c (n "romu") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)))) (h "08bgbdyr3i122s92nqjk0hhvjcypzjsfykmiw09v4j2k89qq1d8x") (f (quote (("unstable_tls" "std" "tls") ("unstable_simd") ("tls" "std") ("std") ("default" "std" "tls" "getrandom"))))))

(define-public crate-romu-0.4.0 (c (n "romu") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1nsnf0qs7kacdpy74sqcraj4pr6zjglqfykafkkf5rh9fmr4aicl") (f (quote (("unstable_tls" "std" "tls") ("unstable_simd") ("tls" "std") ("std") ("default" "std" "tls" "getrandom"))))))

(define-public crate-romu-0.4.1 (c (n "romu") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)))) (h "18qjjzppr6zlql1h7nphp97dqq2fb20w7zzaya6jgp9nmmafwxqc") (f (quote (("unstable_tls" "std" "tls") ("unstable_simd") ("tls" "std") ("std") ("default" "std" "tls" "getrandom"))))))

(define-public crate-romu-0.5.0 (c (n "romu") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1kg3fdwdzsqp33q1acam8rykbhd749cx5k82inbhw5l75i7dlgpx") (f (quote (("unstable_tls" "std" "tls") ("unstable_simd") ("tls" "std") ("std") ("default" "std" "tls" "getrandom")))) (r "1.57")))

(define-public crate-romu-0.5.1 (c (n "romu") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)))) (h "06wzfw17zzfffknixpgj466ys3nl2nwmfm16715h3qmsz64cnrfg") (f (quote (("unstable_tls" "std" "tls") ("unstable_simd") ("tls" "std") ("std") ("default" "std" "tls" "getrandom")))) (r "1.57")))

(define-public crate-romu-0.6.0 (c (n "romu") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0iap3x38igk1l173675l780kajxxagcixfa1g5bpl0lh766khc2a") (f (quote (("unstable_tls" "std" "tls") ("unstable_simd") ("tls" "std") ("std") ("default" "std" "tls" "getrandom")))) (r "1.57")))

