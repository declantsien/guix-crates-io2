(define-module (crates-io ro na ronat) #:use-module (crates-io))

(define-public crate-ronat-0.1.0 (c (n "ronat") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.2.5") (d #t) (k 2)) (d (n "ispell") (r "^0.3.0") (d #t) (k 0)) (d (n "markdown") (r "^0.2.0") (d #t) (k 0)))) (h "0s6f98cs2immr7hzb1rs6rim0ndjkhb0556yma6ans959ddbvwi1")))

