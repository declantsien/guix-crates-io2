(define-module (crates-io ro ko rokol_derive) #:use-module (crates-io))

(define-public crate-rokol_derive-0.1.0 (c (n "rokol_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("derive" "parsing" "printing" "clone-impls" "proc-macro" "extra-traits"))) (d #t) (k 0)))) (h "18fyx9pz1ckjh3g0c5vyrp1m334vvy3b51psxi52ibbm9r59pgd2")))

(define-public crate-rokol_derive-0.2.0 (c (n "rokol_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("derive" "parsing" "printing" "clone-impls" "proc-macro" "extra-traits"))) (d #t) (k 0)))) (h "0qrgp14l12y3nigj54vvmvxb1d304lqj6gmnv9zxbv749jsqidj4")))

