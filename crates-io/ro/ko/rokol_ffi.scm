(define-module (crates-io ro ko rokol_ffi) #:use-module (crates-io))

(define-public crate-rokol_ffi-0.1.0 (c (n "rokol_ffi") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.62") (d #t) (k 1)))) (h "0pr78zzbn2x7bqsjlm9lwixa61bphw0n3yl1ij7xrv7ld87wnii6") (f (quote (("force-glcore33")))) (l "sokol")))

(define-public crate-rokol_ffi-0.1.1 (c (n "rokol_ffi") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.62") (d #t) (k 1)))) (h "10n0016bd2c5q7340nj4dh8phcg9zhqzkfhafp6am6l1pnqz8p70") (f (quote (("metal") ("glcore33") ("d3d11")))) (l "sokol")))

(define-public crate-rokol_ffi-0.1.2 (c (n "rokol_ffi") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.62") (d #t) (k 1)))) (h "0m3d35mdcsfpqyc45l5pffgaal46xybmmk57rmsjmxwcjqvb5n4s") (f (quote (("metal") ("glcore33") ("d3d11")))) (l "sokol")))

(define-public crate-rokol_ffi-0.1.3 (c (n "rokol_ffi") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.62") (d #t) (k 1)))) (h "15lbgz1pir7n2jxpgdcyjmfvhfzf6gm4p2hn63iqnfd3dc45j3jx") (f (quote (("metal") ("glcore33") ("d3d11")))) (l "sokol")))

(define-public crate-rokol_ffi-0.2.0 (c (n "rokol_ffi") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.62") (d #t) (k 1)))) (h "06n8g6j7p6yif0pwbwnl93b5p33c839ns3aykqkmii3wckmwdaz3") (f (quote (("metal") ("impl-gfx") ("impl-app") ("glcore33") ("default" "impl-app" "impl-gfx" "glcore33") ("d3d11")))) (l "sokol")))

(define-public crate-rokol_ffi-0.2.1 (c (n "rokol_ffi") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.62") (d #t) (k 1)))) (h "1sw40qdr44qqyc752ja2ppskw04rvamjw61dzwnrs65k17vcjw8i") (f (quote (("metal") ("impl-gfx") ("impl-app") ("glcore33") ("default" "impl-app" "impl-gfx" "glcore33") ("d3d11")))) (l "sokol")))

(define-public crate-rokol_ffi-0.2.2 (c (n "rokol_ffi") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.62") (d #t) (k 1)))) (h "1yhcd4xdinin4v75830hrqpmj0isvxrq7ymnl8bd1ccc10d0jl63") (f (quote (("metal") ("impl-gfx") ("impl-app") ("glcore33") ("default" "impl-app" "impl-gfx" "glcore33") ("d3d11")))) (l "sokol")))

(define-public crate-rokol_ffi-0.2.3 (c (n "rokol_ffi") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.62") (d #t) (k 1)))) (h "1h5dcfpsjvs48j5cfp26hian3b6p5dylh07l94zgaqzdqspp7k5z") (f (quote (("metal") ("impl-gfx") ("impl-app") ("glcore33") ("d3d11")))) (l "sokol")))

(define-public crate-rokol_ffi-0.3.0 (c (n "rokol_ffi") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.68") (d #t) (k 1)))) (h "00l58pizlf0z4rmmc03m6pfqag3il1va0adpbwaypdkgwflh8b39") (f (quote (("metal") ("impl-gfx") ("impl-app") ("glcore33") ("default" "impl-app" "impl-gfx" "glcore33") ("d3d11")))) (l "sokol")))

(define-public crate-rokol_ffi-0.3.1 (c (n "rokol_ffi") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.68") (d #t) (k 1)))) (h "0xqr48nmrv9ahwpkg30zq549ph2prkhh1nqqdz4xppfg1d2dx747") (f (quote (("metal") ("impl-gfx") ("impl-app") ("glcore33") ("default" "impl-app" "impl-gfx" "glcore33") ("d3d11")))) (l "sokol")))

