(define-module (crates-io ro ot rootvg-image) #:use-module (crates-io))

(define-public crate-rootvg-image-0.1.0 (c (n "rootvg-image") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.0") (k 0)) (d (n "rootvg-core") (r "^0.1") (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.13.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (f (quote ("wgsl"))) (k 0)))) (h "01zg543yrjaj47az5sdwaibsfscps5qxsgv7dy8rklbfzpnscf5i")))

