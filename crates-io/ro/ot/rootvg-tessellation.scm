(define-module (crates-io ro ot rootvg-tessellation) #:use-module (crates-io))

(define-public crate-rootvg-tessellation-0.1.0 (c (n "rootvg-tessellation") (v "0.1.0") (d (list (d (n "lyon") (r "^1.0.1") (d #t) (k 0)) (d (n "rootvg-core") (r "^0.1") (k 0)) (d (n "rootvg-mesh") (r "^0.1") (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (f (quote ("wgsl"))) (k 0)))) (h "14d66a3svasf5ilwwcm80zjxxd9wc9qz6bvjk4c4bxci78x7azv4") (f (quote (("gradient" "rootvg-core/gradient" "rootvg-mesh/gradient") ("default" "gradient"))))))

