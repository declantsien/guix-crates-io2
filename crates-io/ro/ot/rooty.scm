(define-module (crates-io ro ot rooty) #:use-module (crates-io))

(define-public crate-rooty-0.1.0 (c (n "rooty") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 2)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rooty_derive") (r "^0.1") (d #t) (k 0)) (d (n "rooty_shared") (r "^0.1") (d #t) (k 0)))) (h "00crqcbbsax3n4p0z10prn0kdp960phqczqa03pf2bcgpnc4c4yp")))

(define-public crate-rooty-0.1.1 (c (n "rooty") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 2)) (d (n "rooty_derive") (r "^0.1") (d #t) (k 0)) (d (n "rooty_shared") (r "^0.1") (d #t) (k 0)))) (h "1sjvdjxhqfanjbmnrl3jj5y9i4m7dpvplji4vqlwsr5vxwhif6vn")))

