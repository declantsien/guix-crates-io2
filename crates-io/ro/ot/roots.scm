(define-module (crates-io ro ot roots) #:use-module (crates-io))

(define-public crate-roots-0.0.1 (c (n "roots") (v "0.0.1") (h "1an3la90m26h4psmzvh3wc6z9ijgdnc0418ibsr5hvq1xcybjjbp")))

(define-public crate-roots-0.0.2 (c (n "roots") (v "0.0.2") (h "0qhc9q87gmg6vlyirgvsp2vbfszfsm25223104scgaw4fl9kisnr")))

(define-public crate-roots-0.0.3 (c (n "roots") (v "0.0.3") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)))) (h "1yil0ggi5f3i5wp1q63nh9b3lifla4vjj865a4nn6gpfa0xa5crl")))

(define-public crate-roots-0.0.4 (c (n "roots") (v "0.0.4") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)))) (h "1k8g3kxhnffll8bfd0xz5cjz9gpbas0kz88ysrd1n1i91r6pf7i6")))

(define-public crate-roots-0.0.5 (c (n "roots") (v "0.0.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0v9pzqhphr894xpwf5f1m80fxm2xp7i612db4j5yaaxn59qprip4")))

(define-public crate-roots-0.0.6 (c (n "roots") (v "0.0.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1nx6rm5avh9m32nwa1ica6firhfdsx0456n4s0lmgm3spm288d44")))

(define-public crate-roots-0.0.7 (c (n "roots") (v "0.0.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1agfv6ik8dbg2mns58r11jn54bnx5yp90rvdimk81x33qyxx4dhw")))

(define-public crate-roots-0.0.8 (c (n "roots") (v "0.0.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0n5h88vib8j4mcs05bjhj3yy5bgsr8dflszaqv1gdgivl3zi2bq8")))

