(define-module (crates-io ro ot rootvg-msaa) #:use-module (crates-io))

(define-public crate-rootvg-msaa-0.1.0 (c (n "rootvg-msaa") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rootvg-core") (r "^0.1") (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (f (quote ("wgsl"))) (k 0)))) (h "1cil9mb99jgrbfarqh1yn2x3xaaishpljnrq30axazlxppg8ikk8")))

