(define-module (crates-io ro ot rooting_forms_proc_macros) #:use-module (crates-io))

(define-public crate-rooting_forms_proc_macros-0.0.1 (c (n "rooting_forms_proc_macros") (v "0.0.1") (d (list (d (n "genemichaels") (r "^0.1.21") (d #t) (k 2)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "similar") (r "^2.3.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "06wkciprg41mm6h5x1azpph4dpz3zyvkp2z1l0v82kzi7l0xpa58")))

(define-public crate-rooting_forms_proc_macros-0.0.3 (c (n "rooting_forms_proc_macros") (v "0.0.3") (d (list (d (n "genemichaels") (r "^0.1.21") (d #t) (k 2)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "similar") (r "^2.3.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "0fk5nma7x484ab8xv86zkmkln2l3pmjl688gdw6f8k4pp82c46f3")))

(define-public crate-rooting_forms_proc_macros-0.0.4 (c (n "rooting_forms_proc_macros") (v "0.0.4") (d (list (d (n "genemichaels") (r "^0.1.21") (d #t) (k 2)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "similar") (r "^2.3.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "07f8wx6qxxi7fldg351lv79m7nv3qndsr8g92kf9kc27ndv9k7m6")))

