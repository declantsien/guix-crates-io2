(define-module (crates-io ro ot rootvg-mesh) #:use-module (crates-io))

(define-public crate-rootvg-mesh-0.1.0 (c (n "rootvg-mesh") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rootvg-core") (r "^0.1") (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (f (quote ("wgsl"))) (k 0)))) (h "17m7nfq6s28ljcz292iyp2n95ms8lcar147a9q16q41w52dhc54k") (f (quote (("gradient" "rootvg-core/gradient") ("default"))))))

(define-public crate-rootvg-mesh-0.1.1 (c (n "rootvg-mesh") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.14.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rootvg-core") (r "^0.1") (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (f (quote ("wgsl"))) (k 0)))) (h "1bhlnqlj6hhhcvq5xsv00xshl2mg9y6qynpnvys0nl9j3rs526xw") (f (quote (("gradient" "rootvg-core/gradient") ("default"))))))

