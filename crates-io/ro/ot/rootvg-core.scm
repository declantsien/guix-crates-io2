(define-module (crates-io ro ot rootvg-core) #:use-module (crates-io))

(define-public crate-rootvg-core-0.1.0 (c (n "rootvg-core") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "euclid") (r "^0.22.9") (k 0)) (d (n "half") (r "^2.3.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rgb") (r "^0.8.37") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (f (quote ("wgsl"))) (k 0)))) (h "1qbgilkc53l5jbllh5n8hacw8lf23ag59sia0zpsxa4zyw59wjv7") (f (quote (("default" "gradient")))) (s 2) (e (quote (("gradient" "dep:half"))))))

(define-public crate-rootvg-core-0.1.1 (c (n "rootvg-core") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.14.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "euclid") (r "^0.22.9") (k 0)) (d (n "half") (r "^2.3.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rgb") (r "^0.8.37") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (f (quote ("wgsl"))) (k 0)))) (h "07javixn08hskk46a754nlzfmysdqyh5rba15wz523h0llnjwn24") (f (quote (("default" "gradient")))) (s 2) (e (quote (("gradient" "dep:half"))))))

