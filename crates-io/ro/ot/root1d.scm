(define-module (crates-io ro ot root1d) #:use-module (crates-io))

(define-public crate-root1d-0.1.0 (c (n "root1d") (v "0.1.0") (d (list (d (n "roots") (r "^0.0.7") (d #t) (k 2)) (d (n "rug") (r "^1.15.0") (f (quote ("float" "rational"))) (o #t) (k 0)))) (h "1p6g51dm396bzcz6gm7202hgybwhcyv3pblkkng03fmfyhfjd897")))

(define-public crate-root1d-0.2.0 (c (n "root1d") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "roots") (r "^0.0.7") (d #t) (k 2)) (d (n "rug") (r "^1.15.0") (f (quote ("float" "rational"))) (o #t) (k 0)))) (h "1w0i2qw24f2nphgv20pphsn5knz3234685dvqsg5dfq26nsf2i99")))

(define-public crate-root1d-0.2.1 (c (n "root1d") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "roots") (r "^0.0.7") (d #t) (k 2)) (d (n "rug") (r "^1.15.0") (f (quote ("float" "rational"))) (o #t) (k 0)))) (h "03bhqvyggf796kya9cgjylisrjrs3rn73hl5an133s4sfghnbrj9")))

(define-public crate-root1d-0.3.0 (c (n "root1d") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "roots") (r "^0.0.7") (d #t) (k 2)) (d (n "rug") (r "^1.15.0") (f (quote ("float" "rational"))) (o #t) (k 0)))) (h "1qmm0j11nml6wfv3s36w5xm9fhs4y4g2cd9i5a5f2g8ayqg05587")))

(define-public crate-root1d-0.3.1 (c (n "root1d") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "roots") (r "^0.0.7") (d #t) (k 2)) (d (n "rug") (r "^1.15.0") (f (quote ("float" "rational"))) (o #t) (k 0)))) (h "1g0smxm0xlfv6y60dzvj4il6jaw3q08kfhzw0405087risvsml1n")))

(define-public crate-root1d-0.3.2 (c (n "root1d") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "roots") (r "^0.0.7") (d #t) (k 2)) (d (n "rug") (r "^1.15.0") (f (quote ("float" "rational"))) (o #t) (k 0)))) (h "1jwdg34n5jg4r3rbbfsfzkzm4cbw8b03msy26659j7gj81gm3fh4")))

