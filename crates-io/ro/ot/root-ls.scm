(define-module (crates-io ro ot root-ls) #:use-module (crates-io))

(define-public crate-root-ls-0.1.0 (c (n "root-ls") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "root-io") (r "^0.1.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.9.0") (d #t) (k 0)))) (h "0cdx22qiyzi0nzmdyvk2j2sc2c4x0phlqpnkwkp58b11m0krxqk9")))

(define-public crate-root-ls-0.1.1 (c (n "root-ls") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "root-io") (r "^0.1.1") (d #t) (k 0)) (d (n "rustfmt") (r "^0.9.0") (d #t) (k 0)))) (h "1c0smbznbvr1rm6xmdalyrvhmz47szp9i0rz64rfm2ba06bpfg77")))

(define-public crate-root-ls-0.1.2 (c (n "root-ls") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "root-io") (r "^0.1.2") (d #t) (k 0)) (d (n "rustfmt") (r "^0.9.0") (d #t) (k 0)))) (h "07ijhgwdwr62bsgvhjyd5sy2vwx1z6zzf3ksm8kqcqpfz1vgjx7a")))

(define-public crate-root-ls-0.1.3 (c (n "root-ls") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "root-io") (r "^0.2") (d #t) (k 0)) (d (n "rustfmt") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0a0nni43498q9l25bsvnbqa4f3mdjcqhqnlxqll6n107alzhsd7l")))

(define-public crate-root-ls-0.1.4 (c (n "root-ls") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "root-io") (r "^0.2.1") (d #t) (k 0)) (d (n "rustfmt") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1rgb4wgc8kfvii9m1lxznfhcx8v2zdjngy89y3hhnfma4l4zxnlm")))

(define-public crate-root-ls-0.2.0 (c (n "root-ls") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "root-io") (r "^0.3.0") (d #t) (k 0)) (d (n "rustfmt") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "02kkyi4br6qlca2ajfci7c7y0zc3c91cvjqm62bgnb7mckb4wm2g")))

