(define-module (crates-io ro ot rooty_derive) #:use-module (crates-io))

(define-public crate-rooty_derive-0.1.0 (c (n "rooty_derive") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 2)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rooty_shared") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.15.32") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1ad72b4qnr1lixj5f5ydng49c9zxyzilgzp6cf3q05nvjxjvcjlh")))

(define-public crate-rooty_derive-0.1.1 (c (n "rooty_derive") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 2)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rooty") (r "^0.1.0") (d #t) (k 2)) (d (n "rooty_shared") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.15.32") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1g1avhgkd2qp199z8snyqk6cvfgcaxb97s34b0d0icrl2npv1z0b")))

(define-public crate-rooty_derive-0.1.2 (c (n "rooty_derive") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 2)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rooty") (r "^0.1.0") (d #t) (k 2)) (d (n "rooty_shared") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.15.32") (d #t) (k 0)))) (h "10vr18i7rg255gc3vy33y8p8msv4lr0wjni1fsp45ldlsqy39935")))

(define-public crate-rooty_derive-0.1.3 (c (n "rooty_derive") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rooty") (r "^0.1.0") (d #t) (k 2)) (d (n "rooty_shared") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.15.32") (d #t) (k 0)))) (h "0d9qw4rn3397f4a6lvkb3dvqpcddmrbl3xh6qnsis6qvqjwbli96")))

(define-public crate-rooty_derive-0.1.4 (c (n "rooty_derive") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rooty") (r "^0.1.0") (d #t) (k 2)) (d (n "rooty_shared") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.15.32") (d #t) (k 0)))) (h "1bkcgs8y8yva92ng218y8g36zwqi3hz6irw7fqir88c5gsnjg4g7")))

