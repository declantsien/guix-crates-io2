(define-module (crates-io ro ot rootvg-quad) #:use-module (crates-io))

(define-public crate-rootvg-quad-0.1.0 (c (n "rootvg-quad") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rootvg-core") (r "^0.1") (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (f (quote ("wgsl"))) (k 0)))) (h "13p53vng5wq371a0xqax0sk281nwfxvwcr54pwnz2b47izackfkc") (f (quote (("gradient" "rootvg-core/gradient") ("default" "gradient"))))))

