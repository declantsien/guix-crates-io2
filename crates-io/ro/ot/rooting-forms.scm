(define-module (crates-io ro ot rooting-forms) #:use-module (crates-io))

(define-public crate-rooting-forms-0.0.1 (c (n "rooting-forms") (v "0.0.1") (d (list (d (n "rooting") (r "^0.1.6") (d #t) (k 0)) (d (n "rooting_forms_proc_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("HtmlSelectElement" "HtmlInputElement"))) (d #t) (k 0)))) (h "0pfkvzkhjszrxj4k59h771in5nyp8qgjn4pyghg4pr64fg59rb4l")))

(define-public crate-rooting-forms-0.0.3 (c (n "rooting-forms") (v "0.0.3") (d (list (d (n "rooting") (r "^0.1") (d #t) (k 0)) (d (n "rooting_forms_proc_macros") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlSelectElement" "HtmlInputElement"))) (d #t) (k 0)))) (h "1r2g05ky0nz95fk4vj477jadd5azasjcdakq0nn5p1kwyy2v2lmj") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-rooting-forms-0.0.4 (c (n "rooting-forms") (v "0.0.4") (d (list (d (n "rooting") (r "^0.1") (d #t) (k 0)) (d (n "rooting_forms_proc_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlSelectElement" "HtmlInputElement" "HtmlTextAreaElement"))) (d #t) (k 0)))) (h "0b84pz1vg41kphflrbamqb54jfzmnp0n6bwg92c81l8v6071202h") (s 2) (e (quote (("serde" "dep:serde"))))))

