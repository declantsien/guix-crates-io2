(define-module (crates-io ro ot rootvg-text) #:use-module (crates-io))

(define-public crate-rootvg-text-0.1.0 (c (n "rootvg-text") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glyphon") (r "^0.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rootvg-core") (r "^0.1") (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.3") (f (quote ("wgsl"))) (k 0)))) (h "1jj82yqcz2br7z7m0yvhl1vmz4y30abm36kjs7cr393776mgrxrg")))

