(define-module (crates-io ro de rodeo) #:use-module (crates-io))

(define-public crate-rodeo-0.1.0 (c (n "rodeo") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.11.1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1wyc8d7za0070rxjy9hd2zwkg38vv9hn91c4bkzpwsm534hmfw1x") (f (quote (("default" "bumpalo"))))))

(define-public crate-rodeo-0.1.1 (c (n "rodeo") (v "0.1.1") (d (list (d (n "bumpalo") (r "^3.11.1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1y0qmz5h3kir20iibzh59k03lpdp0r96pjn9m2mql6087plmivny") (f (quote (("std") ("default" "bumpalo" "std")))) (y #t)))

(define-public crate-rodeo-0.2.0 (c (n "rodeo") (v "0.2.0") (d (list (d (n "bumpalo") (r "^3.11.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 2)))) (h "1igycx9vd88xh0fxxq9ywzy9xbgdn08hkkasxsdszyh8f1hab9qv") (f (quote (("std") ("default" "bumpalo" "std")))) (y #t) (r "1.56.1")))

(define-public crate-rodeo-0.2.1 (c (n "rodeo") (v "0.2.1") (d (list (d (n "bumpalo") (r "^3.15.4") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 2)))) (h "0mjq88fb40qd14w67iqnrl9hs216384hfq07rvm3hx34pipckvb9") (f (quote (("std") ("default" "bumpalo" "std")))) (r "1.73.0")))

