(define-module (crates-io ro sd rosd_cli) #:use-module (crates-io))

(define-public crate-rosd_cli-0.1.0 (c (n "rosd_cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rosd") (r "~0.1.0") (k 0)))) (h "08k3ld4vwmsqi1wxhjgvbbrr95dn6f2ablvk977m5npf7vklkwj4")))

