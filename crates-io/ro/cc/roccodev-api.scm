(define-module (crates-io ro cc roccodev-api) #:use-module (crates-io))

(define-public crate-roccodev-api-1.1.0 (c (n "roccodev-api") (v "1.1.0") (d (list (d (n "curl") (r "^0.4.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kf34c97c9dc3lz62cicn7zyafr92vkz1rk1ldgd33f6jh8cbdal")))

(define-public crate-roccodev-api-1.1.1 (c (n "roccodev-api") (v "1.1.1") (d (list (d (n "curl") (r "^0.4.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iscfg30ivd9qa4xrwzqf6h32hjd2vc87ygxh4ra4s3ksw2v0ksc")))

(define-public crate-roccodev-api-1.2.0 (c (n "roccodev-api") (v "1.2.0") (d (list (d (n "curl") (r "^0.4.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jwiv2fqw1y2l833snw86wfjimdy4knhi7ndcvzwbgnbw3gzjbs2")))

