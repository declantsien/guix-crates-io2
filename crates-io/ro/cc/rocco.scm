(define-module (crates-io ro cc rocco) #:use-module (crates-io))

(define-public crate-rocco-0.1.0 (c (n "rocco") (v "0.1.0") (d (list (d (n "comrak") (r "^0.8.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.1") (d #t) (k 0)) (d (n "ramhorns") (r "^0.10.1") (f (quote ("default"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "028i9ndjrb5dfz5vc0yxd004fcw2hirfkjici87iwgx890mbhbmp")))

