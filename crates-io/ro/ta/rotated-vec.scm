(define-module (crates-io ro ta rotated-vec) #:use-module (crates-io))

(define-public crate-rotated-vec-0.1.0 (c (n "rotated-vec") (v "0.1.0") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "0i8h64jflic5lajgrj48gnlvm4m5zxbhxwmra27gxfbi9m0biim5")))

(define-public crate-rotated-vec-0.1.1 (c (n "rotated-vec") (v "0.1.1") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "1i4xkgsqyvrb81bgiplxf5gyr226w2069cplvkl8xlgg9cgr0jcr")))

