(define-module (crates-io ro ta rotate) #:use-module (crates-io))

(define-public crate-rotate-0.3.0 (c (n "rotate") (v "0.3.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "qc_file_parsers") (r "^0.2.2") (d #t) (k 0)))) (h "149s72gxyl7c5917kfhvnpbqadds5yrh6adk41fg9scas5y19h0y")))

(define-public crate-rotate-0.4.0 (c (n "rotate") (v "0.4.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "qc_file_parsers") (r "^0.2.2") (d #t) (k 0)))) (h "0sl5qkyp6k9d5slbcacv8zxm9z091r7ddbg01fb81fmnkcy4hri1")))

