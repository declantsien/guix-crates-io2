(define-module (crates-io ro ta rotate-puts) #:use-module (crates-io))

(define-public crate-rotate-puts-0.2.0 (c (n "rotate-puts") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rust_util") (r "^0.6.41") (d #t) (k 0)))) (h "09dgbsas34qz3xgd7v8ywasj1k5jjdw7b3mss7fjziq22ifc7cph")))

(define-public crate-rotate-puts-0.3.0 (c (n "rotate-puts") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "rust_util") (r "^0.6.41") (d #t) (k 0)))) (h "1n4zd4v2h0dsbw2xgwv2jyb7ab5pkqk9idgb3hcpzcna6d6pn6aq")))

