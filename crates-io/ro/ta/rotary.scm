(define-module (crates-io ro ta rotary) #:use-module (crates-io))

(define-public crate-rotary-0.1.0 (c (n "rotary") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1g0ik0yacdqziw2a5k17q5hjsip21yzy29vjy60qq714xzm5kc0j")))

(define-public crate-rotary-0.2.0 (c (n "rotary") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0cnymvrddg9a77q3b13pzh89v883bjf7654v4mp8jr89rss7z217")))

(define-public crate-rotary-0.3.0 (c (n "rotary") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0ylykprrwms8jv5cb3yp115j543slsn8n1bpjxnr8y16hw8y2mf9")))

(define-public crate-rotary-0.4.0 (c (n "rotary") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1dv9zldx8h1xbbgpl3dg3zah8n2kygg92yncv73fpwr3bgy1jbzm")))

(define-public crate-rotary-0.4.1 (c (n "rotary") (v "0.4.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "09l6zyrw99wvaxvjx1hfl795xz15w4rld6hgh24gfixclyc69can")))

(define-public crate-rotary-0.5.0 (c (n "rotary") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0npbj061sm3n1b4cl2qh76jxw0d7l09p96l9rkx0a5yr6cyxbq49")))

(define-public crate-rotary-0.7.0 (c (n "rotary") (v "0.7.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0cq707y7gqfgjgf455dljpyj0xhvzw8mb7m1sa914vwrh8nvzdhc")))

(define-public crate-rotary-0.8.0 (c (n "rotary") (v "0.8.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0gk37db05an5nndpasni32kkw5g77df8ygl22d833kgqris34h46")))

(define-public crate-rotary-0.9.0 (c (n "rotary") (v "0.9.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1351vkl5kmf7kra9j5yrjlighlw146ssa88r2n7xppyd1bgq42cq")))

(define-public crate-rotary-0.9.1 (c (n "rotary") (v "0.9.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0ppxsxjyhd592h9sx656frzbapyhndm984sn9ylspqd445x57d23")))

(define-public crate-rotary-0.10.0 (c (n "rotary") (v "0.10.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1p6pq2p2s3kp9gx6zwqw0702nzyw8dmhbvizj0mkdvfq47xnq6kw")))

(define-public crate-rotary-0.11.0 (c (n "rotary") (v "0.11.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1vqvgyy6fv9f9p4hxpjz160w9mqk4fa73m6nd2v7f5gfa32qv7mk")))

(define-public crate-rotary-0.12.0 (c (n "rotary") (v "0.12.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0cmk1jqb43wsilkqzlf5cl0br950l4yg9a3apak714kqccvmvi4v")))

(define-public crate-rotary-0.13.0 (c (n "rotary") (v "0.13.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1ik0yr7613vm53jppna41a7qdhisblpzswwx6nly6v5yzp2rx31p")))

(define-public crate-rotary-0.15.0 (c (n "rotary") (v "0.15.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "09i0fz04pp5n6siw8n4cbffhl4zfnnjm5r3dcwsb8cfbqk1cssgl")))

(define-public crate-rotary-0.16.0 (c (n "rotary") (v "0.16.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1sl4hxr1s28cqf0wk1a65ra0v10m47n1zins5lff2dv53rnxic30")))

(define-public crate-rotary-0.17.0 (c (n "rotary") (v "0.17.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1naafn9cqhc2b8hvg53d6770nkng3hf382g7vwazf74c215jxh7c")))

(define-public crate-rotary-0.18.0 (c (n "rotary") (v "0.18.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1bv61yzkwdqcmv5195rc0000zj78jr4ls8qfzzs4yn3sf4ws5sjy")))

(define-public crate-rotary-0.19.0 (c (n "rotary") (v "0.19.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "18017zp1jmdz112jzjiaj9fddi2w8zrb2yg6rqv0zzwclqzgribx")))

(define-public crate-rotary-0.20.0 (c (n "rotary") (v "0.20.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0yf0sys3zywwbv7chagb1mp8cph7ns6ywl8km6zrz25id375s8ma")))

(define-public crate-rotary-0.21.0 (c (n "rotary") (v "0.21.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0kd1qcy8vhhb1c9clpnrqrkv1ry0mk5n8057wkgxpi0ssf93li44")))

(define-public crate-rotary-0.22.0 (c (n "rotary") (v "0.22.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "15ir2h7415mf7g417z71cjr8jp33l9hlyh5cxsbmljqzahc2xq1f")))

(define-public crate-rotary-0.23.0 (c (n "rotary") (v "0.23.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "027436icbzdlb96r6amwi00bqbas4x42x5d2qhf0f11iwqzp27zk")))

(define-public crate-rotary-0.24.0 (c (n "rotary") (v "0.24.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0w7b6rhlwl3na74mkg98n3i65s50vs4qq6mwn88mam8zxkb4nki6")))

(define-public crate-rotary-0.25.0 (c (n "rotary") (v "0.25.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "13mcpph21grcixzp3bglkjbi7ffzwrqd6lnp6vg470r1byk4yipa")))

(define-public crate-rotary-0.26.0 (c (n "rotary") (v "0.26.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1sg7nn3fc270dsv10y23d27ixnvjsipargb1sk85ap2snvfkps5y")))

(define-public crate-rotary-0.27.0 (c (n "rotary") (v "0.27.0") (d (list (d (n "bittle") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rotary-core") (r "^0.1.0") (d #t) (k 0)))) (h "0hvj392lbkhljk5wmifjkzijc4gbsndp5lqmr5j5jarw0cbnd77b") (y #t)))

(define-public crate-rotary-0.27.1 (c (n "rotary") (v "0.27.1") (d (list (d (n "bittle") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rotary-core") (r "^0.1.0") (d #t) (k 0)))) (h "0i76h8hymzqn0c5jpx6gnzsanjadq0cvvpfr2b919wxydqzcjnrr") (y #t)))

(define-public crate-rotary-0.27.2 (c (n "rotary") (v "0.27.2") (d (list (d (n "bittle") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rotary-core") (r "^0.1.0") (d #t) (k 0)))) (h "1qrp7ky6lhyl3cgzjw9llg6pnjxyvqqxs06xwsgl7p9d4psiidmr") (y #t)))

(define-public crate-rotary-0.28.0 (c (n "rotary") (v "0.28.0") (d (list (d (n "bittle") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rotary-core") (r "^0.1.0") (d #t) (k 0)))) (h "0k5v65s7mc75ry5v1z02snq143144x6s4z8bz4z4w20052045arh")))

(define-public crate-rotary-0.28.1 (c (n "rotary") (v "0.28.1") (d (list (d (n "bittle") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rotary-core") (r "^0.1.0") (d #t) (k 0)))) (h "1d3203v6gfrn6v9581xfbdch9lx9sg9cn0m9ji4yrzs9109i0lcm")))

(define-public crate-rotary-0.29.0-alpha.1 (c (n "rotary") (v "0.29.0-alpha.1") (d (list (d (n "bittle") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rotary-core") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "13102vxrfd3hyqj05337jywrdi0qn7vsphzwmma0andidgc7i27l")))

(define-public crate-rotary-0.29.0-alpha.2 (c (n "rotary") (v "0.29.0-alpha.2") (d (list (d (n "bittle") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rotary-core") (r "^0.2.0-alpha.2") (d #t) (k 0)))) (h "1i4kwgcicrxnbhbdkd80m13zx88yagrlris6y8lb19g7mny5skn0")))

