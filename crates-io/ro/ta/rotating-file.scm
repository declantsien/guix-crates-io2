(define-module (crates-io ro ta rotating-file) #:use-module (crates-io))

(define-public crate-rotating-file-0.1.0 (c (n "rotating-file") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 0)))) (h "10bjk73wbivvvxqv8vrl6hlidz6m6dbj7x54xz78w5agxszsggsg")))

(define-public crate-rotating-file-0.2.0 (c (n "rotating-file") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 0)))) (h "1c45bn0l0fajh4hsw8cfdnv5lvkspmzgjgw95m325paahjpm2qrc")))

(define-public crate-rotating-file-0.3.0 (c (n "rotating-file") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 0)))) (h "0vmp6qcqj2fqdvykxpl0sjdwjghknmw0q0jjbwf9pqld03rfb0xq")))

(define-public crate-rotating-file-0.3.1 (c (n "rotating-file") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 0)))) (h "0j31pblpcpdck25lk455670hkg3flidsq5714binyppsnq0b11pd")))

(define-public crate-rotating-file-0.3.2 (c (n "rotating-file") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 0)))) (h "0c5jcvqrbzv4bvksigjir0bhirml4irv276mn182k96mdg1d1g6d")))

(define-public crate-rotating-file-0.3.3 (c (n "rotating-file") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 0)))) (h "1sgk3h3rnlab5qsc9054610l2sn55ijxqr3l460qi5nhzfzgacyd")))

(define-public crate-rotating-file-0.3.4 (c (n "rotating-file") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 0)))) (h "1s8pjszwdvqcdikb2hmj8k578hb2f0gzf6v22hj17zz5grrssdkl")))

(define-public crate-rotating-file-0.3.5 (c (n "rotating-file") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 0)))) (h "15i2s3p1cgk6i7iz3daklvafh35cjcxdb9g6qag4n03xd2railh2")))

(define-public crate-rotating-file-0.3.6 (c (n "rotating-file") (v "0.3.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "flate2") (r "^1.0.23") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 2)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1x0chdjxvzljlqf3a06b9b41z4jnfz5jfskck60aigal91v077dg")))

