(define-module (crates-io ro ta rotate-enum) #:use-module (crates-io))

(define-public crate-rotate-enum-0.1.0 (c (n "rotate-enum") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "165iwd90v8gqssdlvb631bi3y38djdbqn9wpfli5mvk2dl8gb0pb")))

(define-public crate-rotate-enum-0.1.1 (c (n "rotate-enum") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nzwa4f8lrmxcghj4sjn0ilajsqjd9ny29sphg8f6b3w6iibx916")))

(define-public crate-rotate-enum-0.1.2 (c (n "rotate-enum") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "120rl0dmq7lan6p4hgqkwp1jvxhwcd2gfx4qg7b4wphg93br92q0")))

