(define-module (crates-io ro ta rotary-encoder) #:use-module (crates-io))

(define-public crate-rotary-encoder-0.1.0 (c (n "rotary-encoder") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 2)) (d (n "sysfs_gpio") (r "^0.5") (f (quote ("tokio"))) (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "08lfnyzpfysjgzpi7wjh0fvz724nc45grj3b0958jj1c66sy9jq4")))

