(define-module (crates-io ro ta rotary-encoder-embedded) #:use-module (crates-io))

(define-public crate-rotary-encoder-embedded-0.0.1 (c (n "rotary-encoder-embedded") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1q9240xglwh8q70rg31ycnwds3sv94r5yjx7b3k7p1kx2qpd7rjr")))

(define-public crate-rotary-encoder-embedded-0.0.2 (c (n "rotary-encoder-embedded") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1g7xhfvkkc2drhriixfbvbxm0552cipqvaj31ifb0ssq17qpak8j")))

(define-public crate-rotary-encoder-embedded-0.0.3 (c (n "rotary-encoder-embedded") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0ydhck1pgbbhix1ql0gxr64g94ij9iq4gf4kk6zcm0865sf126y1")))

(define-public crate-rotary-encoder-embedded-0.0.4 (c (n "rotary-encoder-embedded") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "15d3nfvhaiayclma10890c36751x6ijs420zv7j5w6mkc7a6h4bj")))

(define-public crate-rotary-encoder-embedded-0.1.0 (c (n "rotary-encoder-embedded") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1ydrzl7xl9lzhbqpw5k44ndbk0r2sm1dbq8m2xwlpg17iy6g0z8i") (f (quote (("angular-velocity"))))))

(define-public crate-rotary-encoder-embedded-0.1.1 (c (n "rotary-encoder-embedded") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0qp9fw1arz94k7vz2qch4xxz5f8i605qaan73q5wvn007kswcb6x") (f (quote (("angular-velocity"))))))

(define-public crate-rotary-encoder-embedded-0.2.0 (c (n "rotary-encoder-embedded") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0pfnlyyi681ynn332zy24b65wxiiax48pxa6hcyj0g46gwzrzj01") (f (quote (("standard") ("default" "standard") ("angular-velocity"))))))

(define-public crate-rotary-encoder-embedded-0.3.0 (c (n "rotary-encoder-embedded") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (f (quote ("eh0"))) (d #t) (k 2)))) (h "1brqfv6bnshzpg8j9f4nb5gx6ax2n5hg375dricbsx2x79jhqc6s")))

