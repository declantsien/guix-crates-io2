(define-module (crates-io ro ta rotated-array-set) #:use-module (crates-io))

(define-public crate-rotated-array-set-0.1.0 (c (n "rotated-array-set") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "039d0xjr5jc5ykma9sxfhih32991yapd7sxrhrl99k3sk27mnr71")))

(define-public crate-rotated-array-set-0.1.1 (c (n "rotated-array-set") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0xdss41ybyx9bnvp40vsxjc2dkas31gh73r990sms7pq3dy2kz44")))

