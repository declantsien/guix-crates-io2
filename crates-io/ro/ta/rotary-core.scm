(define-module (crates-io ro ta rotary-core) #:use-module (crates-io))

(define-public crate-rotary-core-0.1.0 (c (n "rotary-core") (v "0.1.0") (d (list (d (n "rotary") (r "^0.26.0") (d #t) (k 2)))) (h "1bbqpw10x5xhyl7yy16149byny9i7sm62bvdmqs9g9dsxhq64fjd") (y #t)))

(define-public crate-rotary-core-0.1.1 (c (n "rotary-core") (v "0.1.1") (d (list (d (n "rotary") (r "^0.27.0") (d #t) (k 2)))) (h "0ghsrgisvg501mv54lk2d6fi1gjw7zc33wb5g6bhlzp1psk21rw5") (y #t)))

(define-public crate-rotary-core-0.1.2 (c (n "rotary-core") (v "0.1.2") (d (list (d (n "rotary") (r "^0.27.0") (d #t) (k 2)))) (h "1s3snp38laaqnz2i5knsmjghlf9i6s2yrijq8d43gpc1b7qnig5h") (y #t)))

(define-public crate-rotary-core-0.1.3 (c (n "rotary-core") (v "0.1.3") (d (list (d (n "rotary") (r "^0.28.0") (d #t) (k 2)))) (h "0hps8wz1cnkjs3m0y1zhlx84dkszsdc0hvagi6n2hlqr7fpxrgy1") (y #t)))

(define-public crate-rotary-core-0.1.4 (c (n "rotary-core") (v "0.1.4") (d (list (d (n "rotary") (r "^0.28.0") (d #t) (k 2)))) (h "1y6vsqq2yg23mmd5b4rhp75lvzflv2x0zz156dcyb1zcpyymnd58")))

(define-public crate-rotary-core-0.2.0-alpha.1 (c (n "rotary-core") (v "0.2.0-alpha.1") (d (list (d (n "rotary") (r "^0.29.0-alpha.1") (d #t) (k 2)))) (h "11xaxqywn6kng2v0jprk3igr5yqff43c1pf9y3wi0qf2rwzy36k4")))

(define-public crate-rotary-core-0.2.0-alpha.2 (c (n "rotary-core") (v "0.2.0-alpha.2") (d (list (d (n "rotary") (r "^0.29.0-alpha.2") (d #t) (k 2)))) (h "0vhnf91v9z9jgzqkn47c2755dvrqmqzqxwgp3jfj5ki8dnv8y63y")))

