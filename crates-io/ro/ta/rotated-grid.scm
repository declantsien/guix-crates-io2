(define-module (crates-io ro ta rotated-grid) #:use-module (crates-io))

(define-public crate-rotated-grid-0.0.1 (c (n "rotated-grid") (v "0.0.1") (d (list (d (n "opencv") (r "^0.82.1") (f (quote ("imgproc" "imgcodecs" "highgui"))) (k 2)))) (h "1sivahhzxgk25maylran4q6197vi3iydm53phv2pc7lkr30csq5s") (y #t) (r "1.56")))

(define-public crate-rotated-grid-0.1.0 (c (n "rotated-grid") (v "0.1.0") (h "0wzadmzr18sc5ds2dn722w6jbkwsy2kcz7la2dd7cbh18awk9nd8") (r "1.59")))

(define-public crate-rotated-grid-0.1.1 (c (n "rotated-grid") (v "0.1.1") (h "0xdij5qax9z01d904n4g7gs1pk41xzp2hl8jw243xcmj5r7s615k") (r "1.59")))

