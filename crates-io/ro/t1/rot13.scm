(define-module (crates-io ro t1 rot13) #:use-module (crates-io))

(define-public crate-rot13-0.1.0 (c (n "rot13") (v "0.1.0") (h "17lm8b9ywl8n42hclrk9v71bqr4k74f370dhsgg3hra34pclj59a")))

(define-public crate-rot13-0.1.1 (c (n "rot13") (v "0.1.1") (h "1nwikrs00qrqb4bzf9iv8bc64jn1fidlrrcmg6lhc63hf84j8kwl")))

