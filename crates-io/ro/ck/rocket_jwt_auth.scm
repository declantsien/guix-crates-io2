(define-module (crates-io ro ck rocket_jwt_auth) #:use-module (crates-io))

(define-public crate-rocket_jwt_auth-0.0.1 (c (n "rocket_jwt_auth") (v "0.0.1") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)))) (h "0bfy4px0vbl0c9j8r9yi01f2vl0r0yq7m05wj9x6anq3v09sh7rc")))

(define-public crate-rocket_jwt_auth-0.0.2 (c (n "rocket_jwt_auth") (v "0.0.2") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)))) (h "1rmyv1125xbd5iz82f1nc15009mvw65a6ilbsys1ya1bb4x83kad")))

(define-public crate-rocket_jwt_auth-0.0.3 (c (n "rocket_jwt_auth") (v "0.0.3") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)))) (h "1x8vwdv8z0znh8l5ccgbwfm2nc733zgyiqbamv8nmj61a4f0nlg6")))

