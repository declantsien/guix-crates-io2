(define-module (crates-io ro ck rocket-governor) #:use-module (crates-io))

(define-public crate-rocket-governor-0.0.1-rc.5 (c (n "rocket-governor") (v "0.0.1-rc.5") (d (list (d (n "governor") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "rocket-governor-derive") (r "^0.0.1-rc.2") (d #t) (k 2)))) (h "1zl1lyd1x74xwsmgybiwlxlhad5fl73psvf6yndxxi68x8x02wxs")))

(define-public crate-rocket-governor-0.0.1-rc.6 (c (n "rocket-governor") (v "0.0.1-rc.6") (d (list (d (n "governor") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "rocket-governor-derive") (r "^0.0.1-rc.3") (d #t) (k 2)))) (h "11k8fn11n6vhskgdn64yk82pbdgwwzy4fy7lfhjs2af3y95hh7ba")))

(define-public crate-rocket-governor-0.0.1-rc.9 (c (n "rocket-governor") (v "0.0.1-rc.9") (d (list (d (n "governor") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "1li69pn7w7jidqnmc3rs0s5gs5jccbxq07vzk50lq73dsv8h6xis") (f (quote (("logger"))))))

(define-public crate-rocket-governor-0.1.0-rc.2 (c (n "rocket-governor") (v "0.1.0-rc.2") (d (list (d (n "governor") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "1nviqv0a0pnppx64cynd4lfxq1zy3sd7izhkqcahrlifppg1v89q") (f (quote (("logger") ("limit_info"))))))

(define-public crate-rocket-governor-0.1.0-rc.3 (c (n "rocket-governor") (v "0.1.0-rc.3") (d (list (d (n "governor") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "1dmf5fm4zijm1ihsqzja24brgl2zvawy54718im2pphbqpmhxyyw") (f (quote (("logger") ("limit_info"))))))

(define-public crate-rocket-governor-0.1.0-rc.4 (c (n "rocket-governor") (v "0.1.0-rc.4") (d (list (d (n "governor") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "1cyq912xw7ync5srz6aibwwmd10azkavq2j73a6933vf7crjji32") (f (quote (("logger") ("limit_info"))))))

(define-public crate-rocket-governor-0.2.0-rc.1 (c (n "rocket-governor") (v "0.2.0-rc.1") (d (list (d (n "governor") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5") (d #t) (k 0)))) (h "0nkj45xjpn957p0d3p3d134hs4hv4vnzzc2zq8kpfbi6s84c3l9j") (f (quote (("logger") ("limit_info"))))))

(define-public crate-rocket-governor-0.2.0-rc.3 (c (n "rocket-governor") (v "0.2.0-rc.3") (d (list (d (n "governor") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5") (k 0)))) (h "01n2bscl03hs8ws79izdwgz0l73sl8kspyjh13jii4wa6524lfpl") (f (quote (("logger") ("limit_info"))))))

