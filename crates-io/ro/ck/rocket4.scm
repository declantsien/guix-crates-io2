(define-module (crates-io ro ck rocket4) #:use-module (crates-io))

(define-public crate-rocket4-0.1.0 (c (n "rocket4") (v "0.1.0") (d (list (d (n "rocket") (r "^0.4.0-rc.1") (d #t) (k 0)))) (h "1rb8p9m10rwg1nv5p18zdh36b4ybfmzbqkhsy268bajyj7539g7v")))

(define-public crate-rocket4-0.2.0 (c (n "rocket4") (v "0.2.0") (d (list (d (n "rocket") (r "^0.4.0") (d #t) (k 0)))) (h "1y6m617aqh585p8kg0h4xaj6ndyvl4pj6vrgsjfra9p8apd81cpc")))

