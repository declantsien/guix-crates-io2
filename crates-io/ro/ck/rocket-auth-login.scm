(define-module (crates-io ro ck rocket-auth-login) #:use-module (crates-io))

(define-public crate-rocket-auth-login-0.2.0 (c (n "rocket-auth-login") (v "0.2.0") (d (list (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "rocket_codegen") (r "0.3.*") (d #t) (k 0)) (d (n "unic-ucd") (r "^0.6.0") (d #t) (k 0)))) (h "0da80k534jxfsbnb7l5cs5f9l3fc2bas2pvk0mx0wzajsj83gqj8")))

(define-public crate-rocket-auth-login-0.2.1 (c (n "rocket-auth-login") (v "0.2.1") (d (list (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "rocket") (r "= 0.3.3") (d #t) (k 0)) (d (n "rocket_codegen") (r "= 0.3.3") (d #t) (k 0)) (d (n "unic-ucd") (r "^0.6.0") (d #t) (k 0)))) (h "1wns6c6gln3f1yvinq7j7i2jc751kx00vvxh2a7xd3xng9q0cgyc")))

(define-public crate-rocket-auth-login-0.2.2 (c (n "rocket-auth-login") (v "0.2.2") (d (list (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "rocket") (r "= 0.3.3") (d #t) (k 0)) (d (n "rocket_codegen") (r "= 0.3.3") (d #t) (k 0)) (d (n "unic-ucd") (r "^0.6.0") (d #t) (k 0)))) (h "15m61hzsn3wzcbwxx40bjm4wgymivrahr45afl716hk14l7y5rry")))

(define-public crate-rocket-auth-login-0.2.3 (c (n "rocket-auth-login") (v "0.2.3") (d (list (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "rocket") (r "0.3.*") (d #t) (k 0)) (d (n "rocket_codegen") (r "0.3.*") (d #t) (k 0)) (d (n "unic-ucd") (r "^0.6.0") (d #t) (k 0)))) (h "0zvmqhmrlqvcv41y7wbk9yhs163ybd3iz2baq0m3r1g14q81k8z3")))

