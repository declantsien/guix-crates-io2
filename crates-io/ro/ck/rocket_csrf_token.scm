(define-module (crates-io ro ck rocket_csrf_token) #:use-module (crates-io))

(define-public crate-rocket_csrf_token-0.3.1 (c (n "rocket_csrf_token") (v "0.3.1") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "bcrypt") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rocket") (r "=0.5.0-rc.3") (f (quote ("secrets"))) (d #t) (k 0)))) (h "0s7syb59avpq9kidfsmsfrikz5ldnbrng29hxrdjpjczimjwaamj")))

(define-public crate-rocket_csrf_token-0.3.2 (c (n "rocket_csrf_token") (v "0.3.2") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "bcrypt") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rocket") (r "=0.5.0-rc.3") (f (quote ("secrets"))) (d #t) (k 0)))) (h "0i9icgg6jlcsklms8y1g2yjim9ah13jg22h0yf1krndxc2ssp9cd")))

(define-public crate-rocket_csrf_token-0.3.3 (c (n "rocket_csrf_token") (v "0.3.3") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "bcrypt") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rocket") (r "=0.5.0-rc.3") (f (quote ("secrets"))) (d #t) (k 0)))) (h "00v4y1j8czjyrg9xagyzvx4fwppdaw5hqzmxdj4fad42jp4c528l")))

(define-public crate-rocket_csrf_token-0.3.4 (c (n "rocket_csrf_token") (v "0.3.4") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "bcrypt") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rocket") (r "=0.5.0") (f (quote ("secrets"))) (d #t) (k 0)))) (h "1bqff7y5pjfi3dql2g9qmzd77mq5shn9jdnw2lr6gnjsvpzzslpj")))

(define-public crate-rocket_csrf_token-0.3.5 (c (n "rocket_csrf_token") (v "0.3.5") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "bcrypt") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rocket") (r "=0.5.0") (f (quote ("secrets"))) (d #t) (k 0)))) (h "05w580im5vjc8jdm88d45nzsss23di08nj39fcm8873zgqzpjg2d")))

