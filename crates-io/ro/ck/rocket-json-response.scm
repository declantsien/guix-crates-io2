(define-module (crates-io ro ck rocket-json-response) #:use-module (crates-io))

(define-public crate-rocket-json-response-0.1.0 (c (n "rocket-json-response") (v "0.1.0") (d (list (d (n "json-gettext") (r "^1.5.0") (d #t) (k 0)) (d (n "rocket") (r "^0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.16") (d #t) (k 0)))) (h "0vy1v74pnfvvazpv696q67x1qidg0rlp26nsns1nf0i1ydc05zcy")))

(define-public crate-rocket-json-response-0.1.1 (c (n "rocket-json-response") (v "0.1.1") (d (list (d (n "json-gettext") (r "^1.5.0") (d #t) (k 0)) (d (n "rocket") (r "^0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.16") (d #t) (k 0)))) (h "05hqwc9dgwb8r6kkn30g5lb7zvcrh1yzaj1av1l7v1gdyr10d9mp")))

(define-public crate-rocket-json-response-0.1.2 (c (n "rocket-json-response") (v "0.1.2") (d (list (d (n "json-gettext") (r "^1.5.0") (d #t) (k 0)) (d (n "rocket") (r "^0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.16") (d #t) (k 2)))) (h "11i9fajb30baxjqf5is0jn03v2ynsp3rmcq0xyyv2a6ngcxr7jma")))

(define-public crate-rocket-json-response-0.2.0 (c (n "rocket-json-response") (v "0.2.0") (d (list (d (n "json-gettext") (r "^2.1.0") (d #t) (k 0)) (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "0b7qs0zp4a3ras96gs0xj173i1kblc2hdw3mkn84c5kqs6drp23n")))

(define-public crate-rocket-json-response-0.3.0 (c (n "rocket-json-response") (v "0.3.0") (d (list (d (n "json-gettext") (r "^2.1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0sjbiyfb2z1wmnjx17dgixqv557dc7i2jcc6l28n728mkwgplkdn")))

(define-public crate-rocket-json-response-0.4.0 (c (n "rocket-json-response") (v "0.4.0") (d (list (d (n "json-gettext") (r "^2.2") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "1w4fk24jhmgckfvzkb9nyk07gszrz9v986ghj1lgdb2gz31813lc")))

(define-public crate-rocket-json-response-0.5.0 (c (n "rocket-json-response") (v "0.5.0") (d (list (d (n "json-gettext") (r "^2.2") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "1yc1slnhrzkjwqn2yj4fwz92z0iqpm3bp9n7j95bdlmwdfv7rwac")))

(define-public crate-rocket-json-response-0.5.1 (c (n "rocket-json-response") (v "0.5.1") (d (list (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0m6vzvf1arqpkgn9fbalw3x838j0pxw94254f4hn4vijmd0zndvf")))

(define-public crate-rocket-json-response-0.5.2 (c (n "rocket-json-response") (v "0.5.2") (d (list (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "1ycgg8zgvp20cwsscx2b85nw75jg35vclyrfvfay9cywrf323fv4")))

(define-public crate-rocket-json-response-0.5.3 (c (n "rocket-json-response") (v "0.5.3") (d (list (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0j327yrxfqnqspapq10dnnm9vych0sy6knkj4kgimsncx2pm924p")))

(define-public crate-rocket-json-response-0.5.4 (c (n "rocket-json-response") (v "0.5.4") (d (list (d (n "enum-ordinalize") (r "^2") (d #t) (k 2)) (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1yjpk060ic7gcqcgjyzkv3rda5lcr4vi5ndqb21n19wgzfq57iv2")))

(define-public crate-rocket-json-response-0.5.5 (c (n "rocket-json-response") (v "0.5.5") (d (list (d (n "enum-ordinalize") (r "^2") (d #t) (k 2)) (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0f47py84273n6bfy18zhky2zyfvxrdff2kp5m5i47xkicza0ff4f")))

(define-public crate-rocket-json-response-0.5.6 (c (n "rocket-json-response") (v "0.5.6") (d (list (d (n "enum-ordinalize") (r "^2") (d #t) (k 2)) (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0y3bcsmzgww4ssj1sw62mnxbvlyz46is04nx0na1nymhc7aqglx4")))

(define-public crate-rocket-json-response-0.5.7 (c (n "rocket-json-response") (v "0.5.7") (d (list (d (n "debug-helper") (r "^0.1") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^2") (d #t) (k 2)) (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1g6jn9lc1845dx8px8a7h0ydpw78rnmwwaxph582780pzwxx0cas")))

(define-public crate-rocket-json-response-0.5.8 (c (n "rocket-json-response") (v "0.5.8") (d (list (d (n "debug-helper") (r "^0.2") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^2") (d #t) (k 2)) (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1rbqq8h7nwsxkp6k07sdd4fjvvp1gbav4f2dfhk6l6h7rb1n8hrd")))

(define-public crate-rocket-json-response-0.5.9 (c (n "rocket-json-response") (v "0.5.9") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^2") (d #t) (k 2)) (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "01vnmz83q2w4vvajq16mbfw785wyizjxlsfanwai34lkm0fd0vwd")))

(define-public crate-rocket-json-response-0.5.10 (c (n "rocket-json-response") (v "0.5.10") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^2") (d #t) (k 2)) (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1c6b95wggfkczjx1bbpm4kwya8fa21x0hg9k6v8h5447kza5cfq6")))

(define-public crate-rocket-json-response-0.5.11 (c (n "rocket-json-response") (v "0.5.11") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 2)) (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0g4hcg90l4mab6k19ns4hm176nx75wa2p5aay5vjjzbcf21fdw8h")))

(define-public crate-rocket-json-response-0.5.12 (c (n "rocket-json-response") (v "0.5.12") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 2)) (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0dsvs0x71b7g5cpx7r9v8kjcl64ipgrjwh623hpfgl3jgxc9spcj")))

(define-public crate-rocket-json-response-0.5.13 (c (n "rocket-json-response") (v "0.5.13") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 2)) (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "01s7k2ycs1d6jasdqg4zmnsh6fs6badgk2lm0g3jdfzbr4n2dqyz") (y #t)))

(define-public crate-rocket-json-response-0.5.14 (c (n "rocket-json-response") (v "0.5.14") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 2)) (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1x10cxisd7qs6rkkvnjq9bzw8l901q0igxlmy0kijbfcrya30rsa")))

(define-public crate-rocket-json-response-0.5.15 (c (n "rocket-json-response") (v "0.5.15") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 2)) (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0qcscl6an2mq4lxq3g250rnhy87vc7xhv35d84fhjkcds0h7q99f") (y #t)))

(define-public crate-rocket-json-response-0.5.16 (c (n "rocket-json-response") (v "0.5.16") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 2)) (d (n "json-gettext") (r "^3") (f (quote ("rocketly"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "19cyrfpvfm2nfnnzg890vwqpf4g9ni5cjmr4xzrd2awra7mcjx1w")))

(define-public crate-rocket-json-response-0.6.0 (c (n "rocket-json-response") (v "0.6.0") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 2)) (d (n "json-gettext") (r "^4") (f (quote ("rocket"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1gczkiy1slw92pxbayh1js2imns7xhpyldp8xazlxpnbx0bl09gk")))

(define-public crate-rocket-json-response-0.6.1 (c (n "rocket-json-response") (v "0.6.1") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 2)) (d (n "json-gettext") (r "^4") (f (quote ("rocket"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "10i4b35la0xdkgr6ghiwrf2rrd0rimil0wf6xhngpr39hl0md6i8")))

(define-public crate-rocket-json-response-0.6.2 (c (n "rocket-json-response") (v "0.6.2") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 2)) (d (n "json-gettext") (r "^4.0.3") (f (quote ("rocket"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "10xchaphy4hr5nrlvlalwlajryk8r8101s6dkv8ia4zrlsi4ahdy")))

(define-public crate-rocket-json-response-0.6.3 (c (n "rocket-json-response") (v "0.6.3") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 2)) (d (n "json-gettext") (r "^4.0.3") (f (quote ("rocket"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "1aycvn5a5ijdl8kxkqghhgshz7hyiw3mrqzj1c22r074wmfnyr1g")))

(define-public crate-rocket-json-response-0.6.4 (c (n "rocket-json-response") (v "0.6.4") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 2)) (d (n "json-gettext") (r "^4.0.3") (f (quote ("rocket"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0ag8hvy1nnda3hb1ri29kinahwvrqxh707aihvpawsgnajqd7w00") (r "1.67")))

(define-public crate-rocket-json-response-0.6.5 (c (n "rocket-json-response") (v "0.6.5") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 2)) (d (n "json-gettext") (r "^4.0.3") (f (quote ("rocket"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0k3i8brgiqkzxaggyixgfvdc8vya9vw2jqf606f7yf03v64gbnji") (r "1.69")))

