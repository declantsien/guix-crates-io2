(define-module (crates-io ro ck rocket_failure) #:use-module (crates-io))

(define-public crate-rocket_failure-0.1.0 (c (n "rocket_failure") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0bwkvwga3k8f74grw9gz5iga9sy1rnxy286zagzzj7q5h952d7wr") (f (quote (("with-rocket" "rocket" "serde_json"))))))

(define-public crate-rocket_failure-0.1.1 (c (n "rocket_failure") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1fy7fdnc84h21brbhg420fmkz32fiyg0fihqhinybgzbw94hlmpl") (f (quote (("with-rocket" "rocket" "serde_json"))))))

(define-public crate-rocket_failure-0.1.2 (c (n "rocket_failure") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "15gv6admvh38rgklxb0qqn5jmmd99aryp3yl5gxy50bx7b2pxgml") (f (quote (("with-rocket" "rocket" "serde_json"))))))

(define-public crate-rocket_failure-0.2.0 (c (n "rocket_failure") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (k 0)) (d (n "rocket_failure_errors") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l5bwcgyvzld6c4qj60hpg7sswfbdzydpwvcmz39xrcnms10sbmn")))

