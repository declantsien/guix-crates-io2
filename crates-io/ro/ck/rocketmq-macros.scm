(define-module (crates-io ro ck rocketmq-macros) #:use-module (crates-io))

(define-public crate-rocketmq-macros-0.1.0 (c (n "rocketmq-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "12xni1iwdwy6zx6fj28kv57l3da6irs0v30wvf87im8kpgd6ay4j")))

