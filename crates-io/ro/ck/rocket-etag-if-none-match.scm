(define-module (crates-io ro ck rocket-etag-if-none-match) #:use-module (crates-io))

(define-public crate-rocket-etag-if-none-match-0.1.0 (c (n "rocket-etag-if-none-match") (v "0.1.0") (d (list (d (n "rocket") (r "^0.3.15") (d #t) (k 0)))) (h "0951yjn2lws9brwmabxdmsv8w3s9wjdnwsx9zs0hq26q83l4iwxx")))

(define-public crate-rocket-etag-if-none-match-0.1.1 (c (n "rocket-etag-if-none-match") (v "0.1.1") (d (list (d (n "rocket") (r "^0.3.16") (d #t) (k 0)))) (h "1hbdadbvr165vgwsdaps07jpd03042nknjrwdb9kg209sr6hljg9")))

(define-public crate-rocket-etag-if-none-match-0.2.0 (c (n "rocket-etag-if-none-match") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "0jajc734qkvbjz9yh3ri1rd9s6dxd9nnamjapxb1dhalv0phqxb7")))

(define-public crate-rocket-etag-if-none-match-0.2.1 (c (n "rocket-etag-if-none-match") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "1ka04j7qdwix8x53v74kph6kmdxjkf9gm0vs334xb92r71sh6lz7")))

(define-public crate-rocket-etag-if-none-match-0.3.0 (c (n "rocket-etag-if-none-match") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "12x4hgaq5m9jrixzn25hbj0nnby7m92d4s95dhq3r8wwaybi9fd3")))

(define-public crate-rocket-etag-if-none-match-0.3.1 (c (n "rocket-etag-if-none-match") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "1n57kmvy0afbscniqi7m41hi9j4qp8wssqn1zbrd773lsj7l705d")))

(define-public crate-rocket-etag-if-none-match-0.3.2 (c (n "rocket-etag-if-none-match") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0rw4ppikj3m21b05dzzs8ffxmsfcrw4yj0ykq47pzv79my8isgx9")))

(define-public crate-rocket-etag-if-none-match-0.3.3 (c (n "rocket-etag-if-none-match") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2") (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "1hq2hmwi0qnjlz16xdzbiqak5rb6nly0yyc1hapj7pwzq6psv9w6")))

(define-public crate-rocket-etag-if-none-match-0.3.4 (c (n "rocket-etag-if-none-match") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2") (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "1796nl7qdv9xif5z1xygy1s1s6y4bgf78zi3ypnb2qdjl8xyr116")))

(define-public crate-rocket-etag-if-none-match-0.3.5 (c (n "rocket-etag-if-none-match") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2") (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "197h1hkqflja2gzy69wc6bp3f2jfn4k1my3ragv60r6y6skp6a43")))

(define-public crate-rocket-etag-if-none-match-0.4.0 (c (n "rocket-etag-if-none-match") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "entity-tag") (r "^0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "19206z5c54q6lgzlrkj4inaaklvy1lqwhwc9j58ks9hadbl3a9ld")))

(define-public crate-rocket-etag-if-none-match-0.4.1 (c (n "rocket-etag-if-none-match") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "entity-tag") (r "^0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "0ywlk5pp5qnjkx96i7a6axfw7127xxhbnd25jyld5471k5jhdjhp")))

(define-public crate-rocket-etag-if-none-match-0.4.2 (c (n "rocket-etag-if-none-match") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "entity-tag") (r "^0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "11b38yj61ic3rwby8ac1ijmyykwmnvjrp9hj60czfk2irnvi1fhw")))

(define-public crate-rocket-etag-if-none-match-0.4.3 (c (n "rocket-etag-if-none-match") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "entity-tag") (r "^0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)))) (h "1p6r3z4asmp4j39g9ijc9pmvl9qinnl265r7678x08jpzi4a5qsq") (r "1.67")))

(define-public crate-rocket-etag-if-none-match-0.4.4 (c (n "rocket-etag-if-none-match") (v "0.4.4") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "entity-tag") (r "^0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.4") (d #t) (k 0)))) (h "1w9kh9fv3v026ww4y0b4g5b5b94xckgp0qgbhjpjqwwxkmi9k0b9") (r "1.69")))

