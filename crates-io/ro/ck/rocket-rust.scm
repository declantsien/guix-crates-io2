(define-module (crates-io ro ck rocket-rust) #:use-module (crates-io))

(define-public crate-rocket-rust-0.1.0 (c (n "rocket-rust") (v "0.1.0") (h "1dq82szkb7chcdnc6j0qjsbqa48hrb99r6ig7fcghnfjg671v8lq") (y #t)))

(define-public crate-rocket-rust-0.2.0 (c (n "rocket-rust") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mfy7as3ai5w84grf7b4p6c7ny7g1fr82n6dacffxqai9m8nyqsx") (y #t)))

(define-public crate-rocket-rust-0.3.0 (c (n "rocket-rust") (v "0.3.0") (h "1wkrs34vcw362s9xjxglj4ygjmkvhdrcx4g94c5p801c0zbp5jc1")))

