(define-module (crates-io ro ck rocket-model-codegen) #:use-module (crates-io))

(define-public crate-rocket-model-codegen-0.1.0 (c (n "rocket-model-codegen") (v "0.1.0") (h "1zn9y2ryp6nhj525fb08n9yn6l1w2q8wzz30f6j4wfw6vyi5b0h6")))

(define-public crate-rocket-model-codegen-0.1.1 (c (n "rocket-model-codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "10zwhx1wxhf4szsg68c3fx64813cd6ja2j8slcbv627gw4v7jrpq")))

