(define-module (crates-io ro ck rocket-sessions) #:use-module (crates-io))

(define-public crate-rocket-sessions-0.0.1 (c (n "rocket-sessions") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "chashmap") (r "^2.2.2") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "redis") (r "^0.21.5") (f (quote ("aio" "tokio-comp"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "0svrpj8rg1dfgnlpb4iz9vhwkvb02plkz33vnc93i63cjl4g76k2") (f (quote (("in-memory" "chashmap"))))))

