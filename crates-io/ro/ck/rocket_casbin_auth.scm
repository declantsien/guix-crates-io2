(define-module (crates-io ro ck rocket_casbin_auth) #:use-module (crates-io))

(define-public crate-rocket_casbin_auth-0.1.0 (c (n "rocket_casbin_auth") (v "0.1.0") (d (list (d (n "casbin") (r "^1.1.2") (f (quote ("runtime-tokio" "incremental" "cached"))) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("stream" "rt-core"))) (k 0)))) (h "0fal9wv4kqakaxv5l93wivx49gl5ki7x5yrvjbd0w3bxi9gv31w1") (y #t)))

(define-public crate-rocket_casbin_auth-0.1.1 (c (n "rocket_casbin_auth") (v "0.1.1") (d (list (d (n "casbin") (r "^1.1.2") (f (quote ("runtime-tokio" "incremental" "cached"))) (k 0)) (d (n "rocket") (r "^0.4.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("stream" "rt-core"))) (k 0)))) (h "0fmhndc6payg2h8047ycawd7pk3cy6i1hik218cz36rggp8yb7ld")))

