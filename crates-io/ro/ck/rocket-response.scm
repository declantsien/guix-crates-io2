(define-module (crates-io ro ck rocket-response) #:use-module (crates-io))

(define-public crate-rocket-response-0.0.1-rc.1 (c (n "rocket-response") (v "0.0.1-rc.1") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "rocket_dyn_templates") (r "^0.1.0-rc.1") (o #t) (d #t) (k 0)))) (h "0xmrfmcp46yz9fpp8m9x6ygrpm5511w2lgznfp4hkcv4qj6j69cp") (f (quote (("templates-tera" "rocket_dyn_templates" "rocket_dyn_templates/tera") ("templates-handlebars" "rocket_dyn_templates" "rocket_dyn_templates/handlebars") ("msgpack" "rocket/msgpack") ("json" "rocket/json"))))))

(define-public crate-rocket-response-0.0.1-rc.2 (c (n "rocket-response") (v "0.0.1-rc.2") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "rocket_dyn_templates") (r "^0.1.0-rc.2") (o #t) (d #t) (k 0)))) (h "1fdkmry3v1gxw4wwybcs7ivmyy47b0nhksnlhlarjd7a438m7474") (f (quote (("templates-tera" "rocket_dyn_templates" "rocket_dyn_templates/tera") ("templates-handlebars" "rocket_dyn_templates" "rocket_dyn_templates/handlebars") ("msgpack" "rocket/msgpack") ("json" "rocket/json"))))))

