(define-module (crates-io ro ck rocket_upload) #:use-module (crates-io))

(define-public crate-rocket_upload-0.1.0 (c (n "rocket_upload") (v "0.1.0") (d (list (d (n "multipart") (r "^0.16.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "063hpnjcxjsyjhn6rhp7709nmllwzfrl302h9qk13mwl91i227r4")))

