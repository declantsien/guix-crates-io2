(define-module (crates-io ro ck rocksdb_datastructures) #:use-module (crates-io))

(define-public crate-rocksdb_datastructures-0.0.1 (c (n "rocksdb_datastructures") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1f1ilhgaryw4xnr5wwz2k3jqr3d6hpnap91g6fpzjdrrjsz76nkr")))

