(define-module (crates-io ro ck rocket-cgi) #:use-module (crates-io))

(define-public crate-rocket-cgi-0.1.0 (c (n "rocket-cgi") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("process" "io-util"))) (k 0)))) (h "0hcfz6gd6ffmw48x7jvigq8rs4wac57khjfki9n39bvid1dzwf3a")))

(define-public crate-rocket-cgi-0.2.0 (c (n "rocket-cgi") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("process" "io-util"))) (k 0)))) (h "1lqy5dbm2j9d5dxwqixrf869kw0r6dh6l1brzgvk0j6frpcdvqxs")))

(define-public crate-rocket-cgi-0.2.1 (c (n "rocket-cgi") (v "0.2.1") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("process" "io-util"))) (k 0)))) (h "1b50j6fll5x9fwi4f8g6q6m14csc5zid2924p28b85p8bz9jpm58")))

