(define-module (crates-io ro ck rocket_async_compression) #:use-module (crates-io))

(define-public crate-rocket_async_compression-0.1.0 (c (n "rocket_async_compression") (v "0.1.0") (d (list (d (n "async-compression") (r "^0.3.8") (f (quote ("gzip" "brotli" "tokio"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "1z6ircknaaw4mkpa3k4hzwzc1yk9q2dxjs28bnb8gb66h6b6y7ca")))

(define-public crate-rocket_async_compression-0.1.1 (c (n "rocket_async_compression") (v "0.1.1") (d (list (d (n "async-compression") (r "^0.3.8") (f (quote ("gzip" "brotli" "tokio"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "0ai8q5bbr4433cw9ij9wy7iswmj6akxwdvdhm5afz638kwcqcdb4")))

(define-public crate-rocket_async_compression-0.1.2 (c (n "rocket_async_compression") (v "0.1.2") (d (list (d (n "async-compression") (r "^0.3.8") (f (quote ("gzip" "brotli" "tokio"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "1l8qzk4baa9spzhgvq3hp22vazks7j6pihh6r19l22iqcd2h8d82")))

(define-public crate-rocket_async_compression-0.2.0 (c (n "rocket_async_compression") (v "0.2.0") (d (list (d (n "async-compression") (r "^0.3.8") (f (quote ("gzip" "brotli" "tokio"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "0vciqyqxhs6m3gnbpvvfsl2bsy6qsbmxrhb0q62m8j0mrka043dh")))

(define-public crate-rocket_async_compression-0.3.0 (c (n "rocket_async_compression") (v "0.3.0") (d (list (d (n "async-compression") (r "^0.3.8") (f (quote ("gzip" "brotli" "tokio"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)))) (h "11ghkb99k0halac8b7msbips4fn8zkwalh5yn7wdlni70dsx0401")))

(define-public crate-rocket_async_compression-0.4.0 (c (n "rocket_async_compression") (v "0.4.0") (d (list (d (n "async-compression") (r "^0.4.0") (f (quote ("gzip" "brotli" "tokio"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)))) (h "1gs7kazypvq4swqgknb43rnpn1yxsk83xbjmh6ls0kypdbqz0bmp")))

(define-public crate-rocket_async_compression-0.5.0 (c (n "rocket_async_compression") (v "0.5.0") (d (list (d (n "async-compression") (r "^0.4.0") (f (quote ("gzip" "brotli" "tokio"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)))) (h "0vzys9jqhg512af22r0p12bd3w244kdjpplglh998n43scnh8rp4")))

(define-public crate-rocket_async_compression-0.5.1 (c (n "rocket_async_compression") (v "0.5.1") (d (list (d (n "async-compression") (r "^0.4.0") (f (quote ("gzip" "brotli" "tokio"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "17k6apclrhr64fazm0kdc05zk3hq3y0iymqxifk7zm4zz1v309ah")))

(define-public crate-rocket_async_compression-0.6.0 (c (n "rocket_async_compression") (v "0.6.0") (d (list (d (n "async-compression") (r "^0.4.0") (f (quote ("gzip" "brotli" "tokio"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "1fk77wlcmagqpjipr37sl57wamlmd67fmkkkr1rkgw6cvpic67rj")))

