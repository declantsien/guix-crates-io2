(define-module (crates-io ro ck rocket_client) #:use-module (crates-io))

(define-public crate-rocket_client-0.1.0 (c (n "rocket_client") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rocket_sync") (r "^0.1.0") (d #t) (k 0)))) (h "03sz0sk17cznb33k865bwy6k6xn12v1r664ml97i5bwgxnmnfyh9")))

(define-public crate-rocket_client-0.1.1 (c (n "rocket_client") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rocket_sync") (r "^0.1.1") (d #t) (k 0)))) (h "1caibqfxfxl82plm1c8wvw8ibqqx9jhnx4k7zrmhsgwb4rfqk8xm")))

