(define-module (crates-io ro ck rocket-file-cache) #:use-module (crates-io))

(define-public crate-rocket-file-cache-0.1.0 (c (n "rocket-file-cache") (v "0.1.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.3") (d #t) (k 0)))) (h "08ak1s1y3lx0cxhpxak7nc23gb5q2530kz4yhyjzdnnap4rzf2hi")))

(define-public crate-rocket-file-cache-0.2.0 (c (n "rocket-file-cache") (v "0.2.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)))) (h "1yhzgclvq8ppymb3hqp9g3mjf6kc3z3v8cmsr304ii86k96gv1gy")))

(define-public crate-rocket-file-cache-0.2.1 (c (n "rocket-file-cache") (v "0.2.1") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)))) (h "1q86lh96z3apvn02mswqqnzqwpm7wcgg9z7wliyma6bciifmc2cj")))

(define-public crate-rocket-file-cache-0.2.2 (c (n "rocket-file-cache") (v "0.2.2") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0a3ahl1ayp39p2b2j2l9q71nrvm5xjaanvsiyy4hqyy1f66jf6bj")))

(define-public crate-rocket-file-cache-0.3.0 (c (n "rocket-file-cache") (v "0.3.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1gwwx9rlhwcjqf9q1fw3gwpbbvafgx6x0zcy7cjkir01qk6cg4sd")))

(define-public crate-rocket-file-cache-0.4.0 (c (n "rocket-file-cache") (v "0.4.0") (d (list (d (n "either") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1j9nvfwaw8q3dmz4hs5870vg70g6jym2msdfvs7mwb75majlv0lx")))

(define-public crate-rocket-file-cache-0.4.1 (c (n "rocket-file-cache") (v "0.4.1") (d (list (d (n "either") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "14k7y1nc2simqyig298w5hb0kksj23049lqwp1hkd8dx5z4q4crm")))

(define-public crate-rocket-file-cache-0.5.0 (c (n "rocket-file-cache") (v "0.5.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "08iv525k2ryjpz6mxy4nlbs5vdv7r24yqi41709fzkxb41336wa2")))

(define-public crate-rocket-file-cache-0.6.0 (c (n "rocket-file-cache") (v "0.6.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0n273bbs6zkbz28m2sjlxv10pzdvh8h926s1xa6877z26d1fwacr")))

(define-public crate-rocket-file-cache-0.6.1 (c (n "rocket-file-cache") (v "0.6.1") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0vnjk36irjwj1h9nn1007f76k8dkikcbnm72z9a89ix57gf69qvg")))

(define-public crate-rocket-file-cache-0.6.2 (c (n "rocket-file-cache") (v "0.6.2") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "07axsplal1hnv5495jbqx0fy137rw8wwq6rl2pj09fqzyshfjgfg")))

(define-public crate-rocket-file-cache-0.7.0 (c (n "rocket-file-cache") (v "0.7.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0mf9fp5a0xnj14f64g77pj2pscx3m5b2mqvmgw3b5g6k4iw8an8d")))

(define-public crate-rocket-file-cache-0.8.0 (c (n "rocket-file-cache") (v "0.8.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1qksyinvm3i74kzjhr1942r5m913nj119zvg6j2b164zm6b96d9n")))

(define-public crate-rocket-file-cache-0.9.0 (c (n "rocket-file-cache") (v "0.9.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0yjcz56j0lql80n0058a55dr44fkahs3nd418xq7zc9nw8jmdhah")))

(define-public crate-rocket-file-cache-0.10.0 (c (n "rocket-file-cache") (v "0.10.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "13v9w0aq0qv5sxjjpq9rpi73dghgfw7biaqndhplq7lskakq86b7")))

(define-public crate-rocket-file-cache-0.10.1 (c (n "rocket-file-cache") (v "0.10.1") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1ax85556375milw1aqd9mm6fv0w2yg33z83fdcza0s7ci6a3y3gb")))

(define-public crate-rocket-file-cache-0.11.0 (c (n "rocket-file-cache") (v "0.11.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1w5nr1b1bx3dlkab1f5d7i4m7jb8zb3mqjkvjd829pdvbz84mx0h")))

(define-public crate-rocket-file-cache-0.11.1 (c (n "rocket-file-cache") (v "0.11.1") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0qcpfvpp59k1bzr2cqy423sqfap4y9x8qxj45gqmm4vq01lx0fxl")))

(define-public crate-rocket-file-cache-0.12.0 (c (n "rocket-file-cache") (v "0.12.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1004ads6sjhl29ryi02zicla91mv70xa7zvmhs65ys1d9d2ijzm7")))

(define-public crate-rocket-file-cache-1.0.0-beta (c (n "rocket-file-cache") (v "1.0.0-beta") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rocket") (r "^0.3.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0fi7rqh43b59fmpbbb14pd1b7mj4i7lm259mr3v07mn1d67gkkp6")))

(define-public crate-rocket-file-cache-1.0.0 (c (n "rocket-file-cache") (v "1.0.0") (d (list (d (n "concurrent-hashmap") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1pz9nfg3v97nkwsyxnl0hxz2f5prs1k407rkc5fjywkw5bki4vfx")))

