(define-module (crates-io ro ck rocket-slog) #:use-module (crates-io))

(define-public crate-rocket-slog-0.1.0 (c (n "rocket-slog") (v "0.1.0") (d (list (d (n "rocket") (r "^0.3.17") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)))) (h "18mxl9bbjzniln1zync0qws4a6skalrwqsy9mcpwkr2qzj4pbdc1")))

(define-public crate-rocket-slog-0.2.0 (c (n "rocket-slog") (v "0.2.0") (d (list (d (n "rocket") (r "^0.3.17") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)))) (h "15xjbnrd6j3s0qmzrpzqlr9mri8xvxp0c085a9cwvq3bixd20nf5")))

(define-public crate-rocket-slog-0.2.1 (c (n "rocket-slog") (v "0.2.1") (d (list (d (n "rocket") (r "^0.3.17") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.17") (d #t) (k 2)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "sloggers") (r "^0.3") (d #t) (k 2)))) (h "0izgn352bgzdb6rnfgv4rkw37wd79rpnrvn1929qa1wkp3jnxyyn")))

(define-public crate-rocket-slog-0.2.2 (c (n "rocket-slog") (v "0.2.2") (d (list (d (n "rocket") (r "^0.3.17") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.17") (d #t) (k 2)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "sloggers") (r "^0.3") (d #t) (k 2)))) (h "1lz2c2wqffvh1vxp3z2yygrsclcamkj7lc69nj82mgd88djmyq3r")))

(define-public crate-rocket-slog-0.3.0 (c (n "rocket-slog") (v "0.3.0") (d (list (d (n "rocket") (r "^0.3.17") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.17") (d #t) (k 2)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "sloggers") (r "^0.3") (d #t) (k 2)))) (h "1d2fzb4n7cxa8hj75l2gkizasxahkwqbksy4icrxl1jximsyj2k0")))

(define-public crate-rocket-slog-0.4.0-rc.2 (c (n "rocket-slog") (v "0.4.0-rc.2") (d (list (d (n "rocket") (r "^0.4.0-rc.2") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "sloggers") (r "^0.3") (d #t) (k 2)))) (h "01y9fvlmvgyxy4ghl733hw0840lg07ldhqy1psm06f86bzf9i55l")))

(define-public crate-rocket-slog-0.4.0 (c (n "rocket-slog") (v "0.4.0") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "slog") (r "^2.4") (d #t) (k 0)) (d (n "sloggers") (r "^0.3") (d #t) (k 2)))) (h "0zsz7r8gvnw731cbjgc0w174liizbzl2m06x87mf13vrmcaqlg6k")))

