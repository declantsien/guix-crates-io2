(define-module (crates-io ro ck rocket_json) #:use-module (crates-io))

(define-public crate-rocket_json-0.1.0 (c (n "rocket_json") (v "0.1.0") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4") (d #t) (k 0)))) (h "0ajxgw8j8wdps8wlr3dmj4zr2z148z47v2mdh1i4c0z8mf4l4wv1") (y #t)))

(define-public crate-rocket_json-0.1.1 (c (n "rocket_json") (v "0.1.1") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4") (d #t) (k 0)))) (h "0r0qrachk64srlgm74hb3c6a1spyajn1da3j8vwlwxgh3fv8vffg") (y #t)))

(define-public crate-rocket_json-0.1.2 (c (n "rocket_json") (v "0.1.2") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4") (d #t) (k 0)))) (h "1r010hshw1y3zkl4sb6gn5r1i7vrsfcsys5kcz2mq29wmmpgsl0x")))

(define-public crate-rocket_json-0.1.3 (c (n "rocket_json") (v "0.1.3") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d1hla53aqbwn9dzgzlqqr8789ix1qraj211mrqn9qs1w6wd7wh9")))

(define-public crate-rocket_json-0.1.4 (c (n "rocket_json") (v "0.1.4") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jhws78p4l5394ighm462i6sbcnz8v90x21i3dc0yg5kpm6xm7l8")))

