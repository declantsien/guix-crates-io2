(define-module (crates-io ro ck rocksdb_sq) #:use-module (crates-io))

(define-public crate-rocksdb_sq-0.1.0 (c (n "rocksdb_sq") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "019yp5fc2yr69cf6ggndihihmp5r08mclfj8bwjwgf05airm5fh8")))

(define-public crate-rocksdb_sq-0.1.1 (c (n "rocksdb_sq") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1w8z9yzy1dj1j4051f624np06hamlp4nh8rm25wcdcpp39qfjg7a")))

(define-public crate-rocksdb_sq-0.1.2 (c (n "rocksdb_sq") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "0fzc932s2pkqazh01apx5d96vjqaimcz2v4pl9qbs4551pifdzfp") (y #t)))

(define-public crate-rocksdb_sq-0.1.3 (c (n "rocksdb_sq") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1hci5mv0s75wk9fh16wicqxkvyi932px36w0w1lk7xw3vil2fm07") (y #t)))

(define-public crate-rocksdb_sq-0.1.4 (c (n "rocksdb_sq") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "06647v7hwsfb8nwmj0kilbbf48vmjzwd72kv5ldz6idss7xhnnaz")))

