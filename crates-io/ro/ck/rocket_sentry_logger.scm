(define-module (crates-io ro ck rocket_sentry_logger) #:use-module (crates-io))

(define-public crate-rocket_sentry_logger-0.1.0 (c (n "rocket_sentry_logger") (v "0.1.0") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "sentry") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "01n2xy1s7vy0kwlz7w92a4ic33k6s85vbfzp1afgr4lwd588zw79") (y #t)))

(define-public crate-rocket_sentry_logger-0.1.1 (c (n "rocket_sentry_logger") (v "0.1.1") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "sentry") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "06z4z18vkmncfjj42zxaigaxxl4l5rgsk6mafzhjs2zl3fkprisr") (y #t)))

(define-public crate-rocket_sentry_logger-0.1.2 (c (n "rocket_sentry_logger") (v "0.1.2") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "sentry") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "0j6bc6vn3l8knhffaykaqwpf85cq51519him139apd82mlh8d1qr")))

(define-public crate-rocket_sentry_logger-0.2.0 (c (n "rocket_sentry_logger") (v "0.2.0") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "sentry") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "00563h0806481qzzgrzkfizy6hj4w3s0rafnhj16fzhxg4sjhk45")))

(define-public crate-rocket_sentry_logger-0.2.1 (c (n "rocket_sentry_logger") (v "0.2.1") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "sentry") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "1fin4348wn9say0nqnsn8ya9jh5rynp0cp3f5kmakyp17vz9qjpk")))

(define-public crate-rocket_sentry_logger-0.2.2 (c (n "rocket_sentry_logger") (v "0.2.2") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "sentry") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "1yc1i3imc8dbpgd8k0s0xpkr12n23rd0d5faam4l0jvasfkrvl5s")))

(define-public crate-rocket_sentry_logger-0.3.0 (c (n "rocket_sentry_logger") (v "0.3.0") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "sentry") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "1ngkkqgqbysv1awaa5khqmnqmlrb4iqgy8j1qxgpdb4jh3xi613z")))

(define-public crate-rocket_sentry_logger-0.3.1 (c (n "rocket_sentry_logger") (v "0.3.1") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "sentry") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "0myn8haas4hv642anw9p8675kd47r8ijgfqxabwmfkk3zjfhgbxn")))

(define-public crate-rocket_sentry_logger-0.3.2 (c (n "rocket_sentry_logger") (v "0.3.2") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "sentry") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "0k06mab7i37jihvjl8hwm2rc7s0kyk87qn7fr80nn68vh7kczm90")))

(define-public crate-rocket_sentry_logger-0.4.0 (c (n "rocket_sentry_logger") (v "0.4.0") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "sentry") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "1xjjs6asywwk6iy4r64xax14amzsa2iy1s8mbbpdx4qbpb7f3w67")))

(define-public crate-rocket_sentry_logger-0.4.1 (c (n "rocket_sentry_logger") (v "0.4.1") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "sentry") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "1kipsiyjpar9n5qcyj3rv8vvg074rx7wqzc2ap2pvrl6yd12h1i5")))

