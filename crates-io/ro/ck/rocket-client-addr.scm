(define-module (crates-io ro ck rocket-client-addr) #:use-module (crates-io))

(define-public crate-rocket-client-addr-0.1.0 (c (n "rocket-client-addr") (v "0.1.0") (d (list (d (n "rocket") (r "^0.3.15") (d #t) (k 0)))) (h "1nlsk63vsx98w9gcn4gpc7rrdgs74aa11kx2xvg455rjm2pygvjd")))

(define-public crate-rocket-client-addr-0.1.1 (c (n "rocket-client-addr") (v "0.1.1") (d (list (d (n "rocket") (r "^0.3.15") (d #t) (k 0)))) (h "0sb2g9z6hh6ggzgq386kd6md48wprv2ak3y6d2qhl0ffwmw3g43q")))

(define-public crate-rocket-client-addr-0.1.2 (c (n "rocket-client-addr") (v "0.1.2") (d (list (d (n "rocket") (r "^0.3.15") (d #t) (k 0)))) (h "0xfl0xykkyc2r6dq8a9vn2q9s43vl6h008f8j7nqvfmr413zwnj6")))

(define-public crate-rocket-client-addr-0.1.3 (c (n "rocket-client-addr") (v "0.1.3") (d (list (d (n "rocket") (r "^0.3.15") (d #t) (k 0)))) (h "0m694ny4aj47f6v3ipzf8a3x1njkv6r15cb7q9c3lcxq80zmg0rl") (f (quote (("nightly"))))))

(define-public crate-rocket-client-addr-0.2.0 (c (n "rocket-client-addr") (v "0.2.0") (d (list (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "0lw0s6v7xmlx5rhi5w2332dz7pq1mjnsh8dzkh4x3r0xranakw7l")))

(define-public crate-rocket-client-addr-0.2.1 (c (n "rocket-client-addr") (v "0.2.1") (d (list (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "08zsld9wx15i42lwmd2aam5h0kwcvak6dwaciw9rcvvmwmqv45dj")))

(define-public crate-rocket-client-addr-0.2.2 (c (n "rocket-client-addr") (v "0.2.2") (d (list (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "02amlbdal8r3bib7q9g6486bhshgajwf1acxm0226dqkqk7j68v9")))

(define-public crate-rocket-client-addr-0.3.0 (c (n "rocket-client-addr") (v "0.3.0") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "1fmh5ml75fv6nzsfvj5cxzxdbcvya26zj75md43nlrxh01yxm34j")))

(define-public crate-rocket-client-addr-0.3.1 (c (n "rocket-client-addr") (v "0.3.1") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "14aqyp9lzmnpp9pqkx0390pkjv2jkhjg255h1acp023wdbd8anx9")))

(define-public crate-rocket-client-addr-0.3.2 (c (n "rocket-client-addr") (v "0.3.2") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "19fy4sbrqg1a8lq0yfjz95hsr5wr1igfzlqxdx1v38pj0jwszhas")))

(define-public crate-rocket-client-addr-0.4.0 (c (n "rocket-client-addr") (v "0.4.0") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "1j5hka8bnmghpbjfbhlcdcsnh5syxgk27x6j1nvc0jmx83ixm3v4")))

(define-public crate-rocket-client-addr-0.4.1 (c (n "rocket-client-addr") (v "0.4.1") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "1vzr8c6r9dlfgvfmpx7dd5gc9wzybmc5wb8vvb15w8yzb4yxs3kk")))

(define-public crate-rocket-client-addr-0.4.2 (c (n "rocket-client-addr") (v "0.4.2") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0za7gygbbb98lhwkvnczjy7s0vljb85pyjglj0ybyyd3k3dvkzw0")))

(define-public crate-rocket-client-addr-0.4.3 (c (n "rocket-client-addr") (v "0.4.3") (d (list (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "012xlsay5sydzvqabygh2pfclr0z4mn55r8wzs4b9lzily255dys")))

(define-public crate-rocket-client-addr-0.4.4 (c (n "rocket-client-addr") (v "0.4.4") (d (list (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "04x401hzffbshjzir8sxrkzqbizinw2kndn6xnhcqa2gp4nmiqsn")))

(define-public crate-rocket-client-addr-0.4.5 (c (n "rocket-client-addr") (v "0.4.5") (d (list (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "0abmm4958dw38lfrnjwd491hrwmimphnqp3yjsl6781xgpsgk761")))

(define-public crate-rocket-client-addr-0.4.6 (c (n "rocket-client-addr") (v "0.4.6") (d (list (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "1a2qr2xf1fs5lwz02bkx025lqpgk9ahd9jm18g3vi8n3vwzxkdld")))

(define-public crate-rocket-client-addr-0.5.0 (c (n "rocket-client-addr") (v "0.5.0") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "037x2sj5msaw01wz9mvcc72b35h81ns7sqi66cccg39vypjnqcbw")))

(define-public crate-rocket-client-addr-0.5.1 (c (n "rocket-client-addr") (v "0.5.1") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "0mhr8v6jjhn1jfy68abky0fiv373qcsv27n401dibdxx23p260xm")))

(define-public crate-rocket-client-addr-0.5.2 (c (n "rocket-client-addr") (v "0.5.2") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "1wcxmvmd9hh25443sfhkfps8rqj97dh63yj7i9f4d5nn7mp4mdcc")))

(define-public crate-rocket-client-addr-0.5.3 (c (n "rocket-client-addr") (v "0.5.3") (d (list (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)))) (h "06q2k30nnd1pv1rngff0m8yr7d3gjppf02mfzx71wi41vp6n5sn5") (r "1.67")))

(define-public crate-rocket-client-addr-0.5.4 (c (n "rocket-client-addr") (v "0.5.4") (d (list (d (n "rocket") (r "^0.5.0-rc.4") (d #t) (k 0)))) (h "0cm4dwmkbj0rawfz7j9v82301bax3l4na8pxvzxx6pv0ax3kpjih") (r "1.69")))

