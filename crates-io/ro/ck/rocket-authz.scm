(define-module (crates-io ro ck rocket-authz) #:use-module (crates-io))

(define-public crate-rocket-authz-0.1.0 (c (n "rocket-authz") (v "0.1.0") (d (list (d (n "casbin") (r "^2.0.7") (f (quote ("cached" "runtime-async-std" "logging" "incremental"))) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0isw1j3l01l356gp8z5zxbzy3fma8rkcxla92qcgzk0riwxjp66n")))

