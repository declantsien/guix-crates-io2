(define-module (crates-io ro ck rocket-dependency-injection) #:use-module (crates-io))

(define-public crate-rocket-dependency-injection-0.1.0 (c (n "rocket-dependency-injection") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "rocket-dependency-injection-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)))) (h "1rdsmaff14bdx21x2kbsaii30pbf8f655j297s3bfx5kzlmi2fyf") (f (quote (("derive" "rocket-dependency-injection-derive") ("default" "derive"))))))

