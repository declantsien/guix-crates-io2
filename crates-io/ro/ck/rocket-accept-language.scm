(define-module (crates-io ro ck rocket-accept-language) #:use-module (crates-io))

(define-public crate-rocket-accept-language-0.1.0 (c (n "rocket-accept-language") (v "0.1.0") (d (list (d (n "accept-language") (r "^1.2.0") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)))) (h "0fvgarnzvaql4zcwbaql6psl88wab3nskf2f2rswpdyx006r5265")))

(define-public crate-rocket-accept-language-0.2.0 (c (n "rocket-accept-language") (v "0.2.0") (d (list (d (n "accept-language") (r "^1.2.1") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "0xk8rssx58jwfymjri3p44hfmwxy5pnb0a5ax7nvdr2ql98b1vwd")))

(define-public crate-rocket-accept-language-0.2.1 (c (n "rocket-accept-language") (v "0.2.1") (d (list (d (n "accept-language") (r "^1.2.1") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "1fngsw5v4v45bcy4dzva0sh027831vwcp05diwb65f33njy6c0gx")))

(define-public crate-rocket-accept-language-0.2.2 (c (n "rocket-accept-language") (v "0.2.2") (d (list (d (n "accept-language") (r "^1.2.1") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "1jp4g2qyg5bw6b9nrrm3zdvm0crmhklm85dkywljsndn8xb6izdk")))

(define-public crate-rocket-accept-language-0.2.4 (c (n "rocket-accept-language") (v "0.2.4") (d (list (d (n "accept-language") (r "^1.2.1") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "0pvrdh7aw9xwsvxlfwzmvpf9a796z05132g1zv5m9xrzkx58c66y")))

(define-public crate-rocket-accept-language-0.3.0 (c (n "rocket-accept-language") (v "0.3.0") (d (list (d (n "accept-language") (r "^1.2.1") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0a3n7n30hn5bvjfrfq12k0k57cy2c6hjq88qn82lwc5ylp9f1cli")))

(define-public crate-rocket-accept-language-0.3.1 (c (n "rocket-accept-language") (v "0.3.1") (d (list (d (n "accept-language") (r "^2.0.0") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0cm1ivqa8ddnjqmahc0rsn0rb1gr0qb4d6z0ccywhxdsamqczcxa")))

(define-public crate-rocket-accept-language-0.3.2 (c (n "rocket-accept-language") (v "0.3.2") (d (list (d (n "accept-language") (r "^2.0.0") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "1900hcvfncbghipk4b0mnx45b9cy5iqmq55qyddl7dqqq865yx6x")))

(define-public crate-rocket-accept-language-0.4.0 (c (n "rocket-accept-language") (v "0.4.0") (d (list (d (n "accept-language") (r "^2.0.0") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0xdm2rrsil856kqpqrwyyfky0jhcikf1i88fdlkx1vahrcad3i2v")))

(define-public crate-rocket-accept-language-0.4.1 (c (n "rocket-accept-language") (v "0.4.1") (d (list (d (n "accept-language") (r "^2.0.0") (d #t) (k 0)) (d (n "fluent-locale") (r "^0.4.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0fkhfrwbcz65sbrlqxmxgczc55v457b2kj88h31m40mqjyx5i16f")))

(define-public crate-rocket-accept-language-0.5.0 (c (n "rocket-accept-language") (v "0.5.0") (d (list (d (n "accept-language") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "unic-langid") (r "^0.4") (d #t) (k 0)))) (h "0819rkqdqd2ig62ffy18b58vq1ka9yr55zc89i5g8y3ga2jfylvb")))

(define-public crate-rocket-accept-language-0.5.1 (c (n "rocket-accept-language") (v "0.5.1") (d (list (d (n "accept-language") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "unic-langid") (r "^0.4") (d #t) (k 0)))) (h "0h9ww9gr8kkhsdfar0z6vnd7lwaqsg2snz7y85rq35zsi0n30nk5")))

(define-public crate-rocket-accept-language-0.5.2 (c (n "rocket-accept-language") (v "0.5.2") (d (list (d (n "accept-language") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.5") (d #t) (k 0)))) (h "0hwl7ccxm2shryda3hk387jrkq6jg0xc2yd2qq4m3lfpq7db79ia")))

(define-public crate-rocket-accept-language-0.5.3 (c (n "rocket-accept-language") (v "0.5.3") (d (list (d (n "accept-language") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (d #t) (k 0)))) (h "0d5lpxg17ayp6p43689a2yzyq27crlh4mdwdz0p0dppq422wvi61")))

(define-public crate-rocket-accept-language-0.6.0 (c (n "rocket-accept-language") (v "0.6.0") (d (list (d (n "accept-language") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "1rl7lb3xpcmpvvcf5xj4id4mai9fljiqqviqji2pymvwrz02v7h4")))

(define-public crate-rocket-accept-language-0.7.0 (c (n "rocket-accept-language") (v "0.7.0") (d (list (d (n "accept-language") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 0)))) (h "06jbxhgdlv155zrj4wwv6yxj1hf92b5prdy5sb55q2hrlbrqahq1")))

(define-public crate-rocket-accept-language-0.7.1 (c (n "rocket-accept-language") (v "0.7.1") (d (list (d (n "accept-language") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 0)))) (h "1q9qj46g3z2q9kg26d6bxyq9gzs7mj3cx16znc5nzi6w0qq3paw0")))

(define-public crate-rocket-accept-language-0.7.2 (c (n "rocket-accept-language") (v "0.7.2") (d (list (d (n "accept-language") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 0)))) (h "1976sjci5byiqjddm7rgz1vf12jgd03qrmqg6npjk517aa8gwzy5")))

(define-public crate-rocket-accept-language-0.7.3 (c (n "rocket-accept-language") (v "0.7.3") (d (list (d (n "accept-language") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 0)))) (h "040mlg32sn1w9rsybfydfps9z8j231nawchm1y0iqk09n9kwyv19")))

(define-public crate-rocket-accept-language-0.8.0 (c (n "rocket-accept-language") (v "0.8.0") (d (list (d (n "accept-language") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 0)))) (h "1558qplgp5dgrvml7arw0jj82lhzmsa1zrmhh68s93ajfbkc39f5")))

(define-public crate-rocket-accept-language-0.8.1 (c (n "rocket-accept-language") (v "0.8.1") (d (list (d (n "accept-language") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 0)))) (h "0aszxd1vsmjv7805066h59hn2crbn3zjl1q089mh9v7196gsbdwq")))

(define-public crate-rocket-accept-language-0.8.2 (c (n "rocket-accept-language") (v "0.8.2") (d (list (d (n "accept-language") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 0)))) (h "0dvdg3nqa9ic15073iidyv3g7v9shacvmfvq62rmkj82jqf54hyb")))

(define-public crate-rocket-accept-language-0.8.3 (c (n "rocket-accept-language") (v "0.8.3") (d (list (d (n "accept-language") (r "^3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 0)))) (h "1jla6ivrs6jzr3qxwg1019rcfn7z8cqi4l5sdjgizgbpqfncq1nf") (r "1.67")))

(define-public crate-rocket-accept-language-0.8.4 (c (n "rocket-accept-language") (v "0.8.4") (d (list (d (n "accept-language") (r "^3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.4") (d #t) (k 0)) (d (n "tinystr") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 0)))) (h "1d6fr2cy5zq370vjwskqx71p4driayzglbhmmh43s4kimv2shyiv") (r "1.69")))

(define-public crate-rocket-accept-language-0.8.5 (c (n "rocket-accept-language") (v "0.8.5") (d (list (d (n "accept-language") (r "^3") (d #t) (k 0)) (d (n "rocket") (r "^0.5") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unic-langid-macros") (r "^0.9") (d #t) (k 0)))) (h "1cpj5gw3i6m61n8ydxhxish7ichvsfzrsh354lr2fz9p7hdj8qqv") (r "1.69")))

(define-public crate-rocket-accept-language-0.8.6 (c (n "rocket-accept-language") (v "0.8.6") (d (list (d (n "accept-language") (r "^3") (d #t) (k 0)) (d (n "rocket") (r "^0.5") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)))) (h "0rkcwscrsqvylydc36r3alq55pkz3w519cszw33awnd9lsb5z3ll") (r "1.69")))

