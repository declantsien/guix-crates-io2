(define-module (crates-io ro ck rocksbin) #:use-module (crates-io))

(define-public crate-rocksbin-0.1.0 (c (n "rocksbin") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0mzhci6zzf1rzj5dk6ira3gxi18sw63fsnkkd8x3sasd9nii6gvl")))

(define-public crate-rocksbin-0.1.1 (c (n "rocksbin") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1prrqmxqr5akjf2yr9n6niv02rbdj23m8i6v5gcv24d0b5b3cnn8")))

(define-public crate-rocksbin-0.2.0 (c (n "rocksbin") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "16giyghdrakhshz21m5ajkz0yk26zfrvam9xc1bvx3cvmkcb9ag6")))

(define-public crate-rocksbin-0.3.0 (c (n "rocksbin") (v "0.3.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0lh530xmcmcqxc51h87x9972wfn17n79gkikz49xjmpn1g9azp5k")))

