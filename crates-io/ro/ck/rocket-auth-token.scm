(define-module (crates-io ro ck rocket-auth-token) #:use-module (crates-io))

(define-public crate-rocket-auth-token-0.1.0 (c (n "rocket-auth-token") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rf1vz1y7lhmafl9j3xlzlgxib4v82dggzqc1xf6fv27zd4ghfqc") (y #t)))

(define-public crate-rocket-auth-token-0.1.1 (c (n "rocket-auth-token") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hc43yr6lmn5x4xsn1bl4j13d0wgmak1ayr3sbzh550ywhdyf5sp")))

