(define-module (crates-io ro ck rocksdb2) #:use-module (crates-io))

(define-public crate-rocksdb2-0.1.0 (c (n "rocksdb2") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0c21k7gan7h72qzyqicqz777hq2cn4zzn21934k5ndla90yiv8y9") (f (quote (("valgrind") ("default"))))))

