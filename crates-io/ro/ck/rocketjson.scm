(define-module (crates-io ro ck rocketjson) #:use-module (crates-io))

(define-public crate-rocketjson-1.0.0 (c (n "rocketjson") (v "1.0.0") (d (list (d (n "rocketjson_data") (r "^1.0.2") (d #t) (k 0)) (d (n "rocketjson_macro") (r "^1.0.0") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "10f1a4bf0diqz4llf28g8pf4vvqfsmzrf9bxdp062z0sflf2ls6z")))

(define-public crate-rocketjson-1.0.1 (c (n "rocketjson") (v "1.0.1") (d (list (d (n "rocketjson_data") (r "^1.0.3") (d #t) (k 0)) (d (n "rocketjson_macro") (r "^1.0.1") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "03sijsc8i2zd2iair83lh5f7wviirs5d9p3xr30lp7rp6nwfhkbx")))

(define-public crate-rocketjson-1.0.2 (c (n "rocketjson") (v "1.0.2") (d (list (d (n "rocketjson_data") (r "^1.0.4") (d #t) (k 0)) (d (n "rocketjson_macro") (r "^1.0.2") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "12a96kbjiy1g8jg1g7bhgia7m80yxwa7wpqrvznfryvy1z43m6zy")))

(define-public crate-rocketjson-1.0.3 (c (n "rocketjson") (v "1.0.3") (d (list (d (n "rocketjson_data") (r "^1.0.5") (d #t) (k 0)) (d (n "rocketjson_macro") (r "^1.0.2") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "1599kwrr5g4c66bv51qmwc37rzgp4sl4b3icg5rkif2m3n48jka6")))

(define-public crate-rocketjson-1.1.0 (c (n "rocketjson") (v "1.1.0") (d (list (d (n "rocketjson_data") (r "^1.1.0") (d #t) (k 0)) (d (n "rocketjson_macro") (r "^1.1.0") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "1wkys4f6n97dhnl5waf7v9z31njgj31bjjfqmc38pld29g6q376m")))

(define-public crate-rocketjson-1.1.1 (c (n "rocketjson") (v "1.1.1") (d (list (d (n "rocketjson_data") (r "^1.1.1") (d #t) (k 0)) (d (n "rocketjson_macro") (r "^1.1.1") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "0jkc3rb54spmnmpbgafil8x7nhhpr3rgy0bdddsab5wj7808zvp8")))

(define-public crate-rocketjson-1.1.2 (c (n "rocketjson") (v "1.1.2") (d (list (d (n "rocketjson_data") (r "^1.1.2") (d #t) (k 0)) (d (n "rocketjson_macro") (r "^1.1.2") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "0ghgwhk46hp8n3ws35q11ad16dfa4i7s5k7gp4yvwvxn32binqyp")))

(define-public crate-rocketjson-1.1.3 (c (n "rocketjson") (v "1.1.3") (d (list (d (n "rocketjson_data") (r "^1.1.3") (d #t) (k 0)) (d (n "rocketjson_macro") (r "^1.1.3") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "12f8lh8vm9cjsvsbckf7k7zc7wpf0pn2cbcbcx0i245kc5bk09dd")))

(define-public crate-rocketjson-1.2.0 (c (n "rocketjson") (v "1.2.0") (d (list (d (n "rocketjson_data") (r "^1.2.0") (d #t) (k 0)) (d (n "rocketjson_macro") (r "^1.2.0") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "0pv269k48cfr283kk3ypizrcsd6qiamg70n09spia74lw6wr1ai6")))

(define-public crate-rocketjson-1.2.1 (c (n "rocketjson") (v "1.2.1") (d (list (d (n "rocketjson_data") (r "^1.2.1") (d #t) (k 0)) (d (n "rocketjson_macro") (r "^1.2.1") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "06ai5gl4jkq3vg1si956j30imc0dd9y8670kkpbnfb6a4h0i2c8f")))

