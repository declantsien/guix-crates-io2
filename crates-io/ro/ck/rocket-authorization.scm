(define-module (crates-io ro ck rocket-authorization) #:use-module (crates-io))

(define-public crate-rocket-authorization-0.2.0-rc.1 (c (n "rocket-authorization") (v "0.2.0-rc.1") (d (list (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "0vp2s2cnk82szbrnrn45lq6s1xprbmpqlmrpsyaskyj92xj712pm")))

(define-public crate-rocket-authorization-1.0.0 (c (n "rocket-authorization") (v "1.0.0") (d (list (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.4") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)))) (h "0s33qfza3y78z6p4jqr69gh53zf9fil8kw269dmn9dwwj4hyhbzs")))

