(define-module (crates-io ro ck rocket_conditional_attach) #:use-module (crates-io))

(define-public crate-rocket_conditional_attach-0.1.0 (c (n "rocket_conditional_attach") (v "0.1.0") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "05an81pq5c5sg0vqjsnvmsjr4qj9cxbpv80s1lm96xxx9hf2gyny")))

(define-public crate-rocket_conditional_attach-0.1.1 (c (n "rocket_conditional_attach") (v "0.1.1") (d (list (d (n "rocket") (r "^0.4") (k 0)))) (h "1nddjii5v8ki5xbwp2s2l9pl9l2xp09jnr3kiww38p0ak4x8qz96")))

