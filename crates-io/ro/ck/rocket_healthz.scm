(define-module (crates-io ro ck rocket_healthz) #:use-module (crates-io))

(define-public crate-rocket_healthz-0.1.0 (c (n "rocket_healthz") (v "0.1.0") (h "1i1bmxmdipdikxs5b6w3237d17w7zvy9bbg74hzgh97qqis2665f")))

(define-public crate-rocket_healthz-0.2.0 (c (n "rocket_healthz") (v "0.2.0") (h "1kgv2kafgqlxc3az787vpx8jiq3kw467s6xia42g2h9ca5l6pbjj")))

(define-public crate-rocket_healthz-0.5.0-rc.1 (c (n "rocket_healthz") (v "0.5.0-rc.1") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "1ybjl2sfh6z32qn7d64jf5xgzgaa3mn031dkq8816d8p9r1kpmw3") (r "1.57")))

(define-public crate-rocket_healthz-0.5.0-rc.2 (c (n "rocket_healthz") (v "0.5.0-rc.2") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "19gzmn87vdk1rps4zmmw6hxi4l4whvdwshrq6w2w2bny9gblfqb0") (r "1.57")))

(define-public crate-rocket_healthz-0.5.0 (c (n "rocket_healthz") (v "0.5.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "1ifh6jamdbhr39nmrrxkibv01syi3ijs1bbski3fwkjxmfqbdrxj") (r "1.57")))

