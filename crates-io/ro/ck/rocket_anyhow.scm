(define-module (crates-io ro ck rocket_anyhow) #:use-module (crates-io))

(define-public crate-rocket_anyhow-0.1.0 (c (n "rocket_anyhow") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "rocket") (r "^0.4.7") (d #t) (k 0)))) (h "0d4xmdsb8dygk6bbnb0a2l6f6b77w14ribj6rl62rb363xis6fm8")))

(define-public crate-rocket_anyhow-0.1.1 (c (n "rocket_anyhow") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "rocket") (r "^0.4.7") (d #t) (k 0)))) (h "0r76vl4vl0nqaxmk9b3p3v57sxk9ab2lqzacwcv11v4qaw6qlw7m")))

