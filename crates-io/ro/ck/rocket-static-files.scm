(define-module (crates-io ro ck rocket-static-files) #:use-module (crates-io))

(define-public crate-rocket-static-files-0.1.0 (c (n "rocket-static-files") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2") (o #t) (d #t) (k 0)))) (h "16qm3kqyz7p8x6v07xa427c8m29irasc4ylfjy0zvficcxb24pcf") (f (quote (("gen" "walkdir" "phf_codegen" "siphasher" "base64"))))))

