(define-module (crates-io ro ck rocket-include-dir) #:use-module (crates-io))

(define-public crate-rocket-include-dir-1.0.0 (c (n "rocket-include-dir") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.7.3") (k 0)) (d (n "rocket") (r "^0.5.0") (k 0)))) (h "0vyhaagbjwmhbwrlv80fnniq4lv3r3dcipldqkhxxipg2khbvmgb") (r "1.56")))

(define-public crate-rocket-include-dir-1.1.0 (c (n "rocket-include-dir") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (k 0)) (d (n "rocket") (r "^0.5.0") (k 0)))) (h "133wm3kv0sgr98wdbh3pxaldznvlwkbgpzdhrfq2l71zngw17vmb") (r "1.56")))

