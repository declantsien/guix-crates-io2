(define-module (crates-io ro ck rocket_cc_file_server) #:use-module (crates-io))

(define-public crate-rocket_cc_file_server-0.1.0-alpha.1 (c (n "rocket_cc_file_server") (v "0.1.0-alpha.1") (d (list (d (n "httpdate") (r "^1.0.2") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "1h91r1dzp8265nxi9zlr9nbq49wzgdwgfr9dgv1rh57h67yr65p2")))

(define-public crate-rocket_cc_file_server-0.1.0-beta.1 (c (n "rocket_cc_file_server") (v "0.1.0-beta.1") (d (list (d (n "httpdate") (r "^1.0.2") (d #t) (k 2)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "0p76cwmm771dnkjpmxgdmpfpafjw2bhfpaimcq9bl2hnwxnk0hi1")))

