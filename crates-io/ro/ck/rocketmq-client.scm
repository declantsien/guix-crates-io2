(define-module (crates-io ro ck rocketmq-client) #:use-module (crates-io))

(define-public crate-rocketmq-client-0.1.0 (c (n "rocketmq-client") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("default" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c51gnc37vvbfyc588sikwsv7vp4bg14yf9gz2cvbgsbhaldd34x")))

