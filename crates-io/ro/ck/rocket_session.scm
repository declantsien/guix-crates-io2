(define-module (crates-io ro ck rocket_session) #:use-module (crates-io))

(define-public crate-rocket_session-0.1.1 (c (n "rocket_session") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "1gwnhba2ak65g1wsgvn94qivl6bmf9p1ggaj5q5lsgdi0qigdlpl")))

(define-public crate-rocket_session-0.2.0 (c (n "rocket_session") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "09awhwlsrgfzq8rw4aa3wyza5w6w7j69kyx5nfb8q09ihh8asy7k")))

(define-public crate-rocket_session-0.2.1 (c (n "rocket_session") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "141ynwlw4zw9dxa0zgymw7acl4wrjl6w5zhcfs0cjawvh0aix127")))

(define-public crate-rocket_session-0.2.2 (c (n "rocket_session") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0m0d4lsrqdzak47an6xipmwrpw7hgjrfp21s37f2b6qz6cimb4gp")))

(define-public crate-rocket_session-0.3.0 (c (n "rocket_session") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "0vzas4ynn12db12wgx4787am14wbcdwl8n3m36n9d9db6xk0y6ls")))

