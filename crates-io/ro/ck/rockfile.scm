(define-module (crates-io ro ck rockfile) #:use-module (crates-io))

(define-public crate-rockfile-0.1.0 (c (n "rockfile") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 2)) (d (n "crc") (r "^3.0.1") (d #t) (k 2)))) (h "00ixbgjnm6z0zw1hhs279my5fjja2z4nbdqf9zgj2kb2kx6nb4p4")))

(define-public crate-rockfile-0.1.1 (c (n "rockfile") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 2)) (d (n "crc") (r "^3.0.1") (d #t) (k 2)))) (h "0nj5s7z9agqn53fxjch7rb2bwxqgk79ynba7abqc2bkmp56ik2by")))

