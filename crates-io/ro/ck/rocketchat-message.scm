(define-module (crates-io ro ck rocketchat-message) #:use-module (crates-io))

(define-public crate-rocketchat-message-0.1.0 (c (n "rocketchat-message") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vxx8j70b46jdyj9b94gmglaax8hqxrwj6dz1f8xf8fsqrs70m2q")))

(define-public crate-rocketchat-message-0.1.1 (c (n "rocketchat-message") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jclb699vzdhcczfwc9i5b1a35q5wl3is9ydy22czz6md47xmf0s")))

(define-public crate-rocketchat-message-0.1.2 (c (n "rocketchat-message") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "15qx9fsrsr6jxnwblblsl5f5dcpdq5g2v7gfipc53dbxrk3c43yn")))

(define-public crate-rocketchat-message-0.1.3 (c (n "rocketchat-message") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yrkssw01r1mqhjrc8qljgbikij2hxrk681q1g3w0fpzj4f9jwfi")))

