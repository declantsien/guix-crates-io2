(define-module (crates-io ro ck rockerize) #:use-module (crates-io))

(define-public crate-rockerize-0.0.1 (c (n "rockerize") (v "0.0.1") (d (list (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "which") (r "^4.0.1") (d #t) (k 0)))) (h "17gfy2hsj9iq8zvza9zgxa579d7yyi2v2h4zxmzrscdwissnpb83")))

(define-public crate-rockerize-0.0.2 (c (n "rockerize") (v "0.0.2") (d (list (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "which") (r "^4.0.1") (d #t) (k 0)))) (h "093wp089iry5wf5c54s61qg63vkb9wvmcb365m412q7d27gry8jd")))

