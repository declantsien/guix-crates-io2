(define-module (crates-io ro ck rocket_csrf) #:use-module (crates-io))

(define-public crate-rocket_csrf-0.0.0 (c (n "rocket_csrf") (v "0.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (f (quote ("private-cookies"))) (d #t) (k 0)))) (h "1z8z43q2s5wi8r1f8rczqd4fln98h39b561jncd1gd2pshnl7zz1")))

(define-public crate-rocket_csrf-0.0.1 (c (n "rocket_csrf") (v "0.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (f (quote ("private-cookies"))) (d #t) (k 0)))) (h "04vchms1b2ihixxiq41mq0ph8y17b3bgnjcrimjghj7njkavk6gq")))

(define-public crate-rocket_csrf-0.0.2 (c (n "rocket_csrf") (v "0.0.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (f (quote ("private-cookies"))) (d #t) (k 0)))) (h "1aar0am0c0wqxr4wn9af3mj41dff0mz7pa0pz88phd2hxic5crm6")))

(define-public crate-rocket_csrf-0.1.0 (c (n "rocket_csrf") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (f (quote ("private-cookies"))) (d #t) (k 0)))) (h "0nrvwx4s90h6gp3lwy526p2qb5h123a180a4wndxy9vdzfv3dgck")))

(define-public crate-rocket_csrf-0.1.1 (c (n "rocket_csrf") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (f (quote ("private-cookies"))) (d #t) (k 0)))) (h "0hq27r00hipfiaf4dxh832950b2781mn00iy33k3gqq8yl747b4w")))

(define-public crate-rocket_csrf-0.2.0 (c (n "rocket_csrf") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (f (quote ("private-cookies"))) (d #t) (k 0)))) (h "1y3w9nmfj8n255pwhdkx2rjwy4qghk38rq831fv441fsflxa7grq")))

(define-public crate-rocket_csrf-0.3.0 (c (n "rocket_csrf") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bcrypt") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (f (quote ("private-cookies"))) (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "0a83azcp3gds1i1x20m07bpdjinklrd0609a1czh8nkqbr93ggg5")))

