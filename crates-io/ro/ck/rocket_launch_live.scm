(define-module (crates-io ro ck rocket_launch_live) #:use-module (crates-io))

(define-public crate-rocket_launch_live-0.1.0 (c (n "rocket_launch_live") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mp4lz8amcramrbwc5j48nw2di7f6vzylpwllxf97bnn3s5y30xr")))

(define-public crate-rocket_launch_live-0.1.1 (c (n "rocket_launch_live") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xkalr89nkbj968bx846cbsyr8f8x0xlv2lnsfrimfxpnh7rb4ns")))

(define-public crate-rocket_launch_live-0.1.2 (c (n "rocket_launch_live") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lqzpzfysg639alc1i8pcp74gh4wyi47rjlw6wqk9ksjsxfkj432")))

(define-public crate-rocket_launch_live-0.1.3 (c (n "rocket_launch_live") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17sqgpjdvxx6ngzzqmmnxifqm7zk7ydrygl973p519rihlxiqlx0")))

