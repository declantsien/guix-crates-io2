(define-module (crates-io ro ck rocket-versioning) #:use-module (crates-io))

(define-public crate-rocket-versioning-0.1.0 (c (n "rocket-versioning") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "1wibv0db0j0rbpkvcibqjxb74m8qcmdvwzcnvzzds9x3kg3m7q29")))

