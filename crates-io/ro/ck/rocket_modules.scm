(define-module (crates-io ro ck rocket_modules) #:use-module (crates-io))

(define-public crate-rocket_modules-0.1.0 (c (n "rocket_modules") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "fold"))) (d #t) (k 0)))) (h "1bhy3cwx273swv98gjppsppp59ww3n46j9fsr0z8spby4xq630b5")))

(define-public crate-rocket_modules-0.1.1 (c (n "rocket_modules") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "fold"))) (d #t) (k 0)))) (h "1mdysp5zkbg8v8nfdrgrvkyyi486dymd5kilzc2wbc5a07vikwq3")))

(define-public crate-rocket_modules-0.1.2 (c (n "rocket_modules") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1mz385xb16m5xfmzc1m0a8pqgln8mfac8fw6dfz6ifvx34nkzl0j")))

