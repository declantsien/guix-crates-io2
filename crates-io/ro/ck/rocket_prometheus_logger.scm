(define-module (crates-io ro ck rocket_prometheus_logger) #:use-module (crates-io))

(define-public crate-rocket_prometheus_logger-0.1.0 (c (n "rocket_prometheus_logger") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.5.0") (f (quote ("push"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (d #t) (k 0)))) (h "0d3va5qyscmnhpiz54i5zbbpnf1nmvb6j41y3hn55yyancn5fp1k") (f (quote (("test"))))))

