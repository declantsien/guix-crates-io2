(define-module (crates-io ro ck rocket-simple-authorization) #:use-module (crates-io))

(define-public crate-rocket-simple-authorization-0.1.0 (c (n "rocket-simple-authorization") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 2)))) (h "1ri2wq1kyzmaln71718658vm6zm1lf573b0ad860ba5z4f9hgswd")))

(define-public crate-rocket-simple-authorization-0.2.0 (c (n "rocket-simple-authorization") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 2)))) (h "1sdyf78fsafs7w8ph941yzxpj0y490zcgnwnsib6m5r7w4v6y990")))

(define-public crate-rocket-simple-authorization-0.3.0 (c (n "rocket-simple-authorization") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 2)))) (h "0kab83d06c2hgnj67lpawjlz3ldgaras932k4xb13jsrrk7jj89a")))

(define-public crate-rocket-simple-authorization-0.4.0 (c (n "rocket-simple-authorization") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 2)))) (h "03acmzg07lrjkaw0czhyff6l5bwph2jabb4d2nbvii5ahbiz8v62")))

(define-public crate-rocket-simple-authorization-0.5.0 (c (n "rocket-simple-authorization") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 2)))) (h "1yky4iqhy6bcvaxpp3czzql4y419mz2a0nzlmsh0hnj32di3kf1c")))

(define-public crate-rocket-simple-authorization-0.5.1 (c (n "rocket-simple-authorization") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 2)))) (h "09myhch6af3dfxj4kr05fdl51si63xf556qk44jz5n7x91ak6bx6")))

(define-public crate-rocket-simple-authorization-0.5.2 (c (n "rocket-simple-authorization") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 2)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "19jr62bggjbmr2nyvljw9dm6i8pq6lmn4yiqijjwvawrwlbmajvn")))

(define-public crate-rocket-simple-authorization-0.6.0 (c (n "rocket-simple-authorization") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 2)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "08a3775km0m7w5y3a8ld4rqwdw9qyw2wfncv2nfixf8z0wnn5hkx")))

(define-public crate-rocket-simple-authorization-0.6.1 (c (n "rocket-simple-authorization") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 2)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "0l9z4ki34zv4syvhzk7dllybjx8ms24868v2vm6iz10jhnmh7pxl")))

(define-public crate-rocket-simple-authorization-0.6.2 (c (n "rocket-simple-authorization") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 2)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "0g9ppp4diii5h08mrxcxgb7pazfnvn26nsjr07gnglsdi7k200m8")))

(define-public crate-rocket-simple-authorization-0.7.0 (c (n "rocket-simple-authorization") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 2)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "1hcs223h0whvbm5l4q7wasxlfi6bw6smfdnydmmgi0jlsvryglbg")))

(define-public crate-rocket-simple-authorization-0.8.0 (c (n "rocket-simple-authorization") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "0x2izv6rwwcanyzva6aq9vn43v74brpp25nxjybkxgnmrxm64jpl")))

(define-public crate-rocket-simple-authorization-0.9.0 (c (n "rocket-simple-authorization") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "0ymsim7gg045nr3nmh84a803dxbd79x783735318cs3qg0yrmg9f")))

(define-public crate-rocket-simple-authorization-0.9.1 (c (n "rocket-simple-authorization") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "easy-http-request") (r "^0.1") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "17bymncab41rwsqssq4jz32pk7vf52k388sm8b1ibkb8d4kv6qi8")))

(define-public crate-rocket-simple-authorization-0.9.2 (c (n "rocket-simple-authorization") (v "0.9.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "1bmjld1xm3qmw80ng2lci7rkl90i7p37wvv8dqxkjbjb052mrv89")))

(define-public crate-rocket-simple-authorization-0.9.3 (c (n "rocket-simple-authorization") (v "0.9.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "13v54kvz07l4gs9s8jch6n1ccl35s84axhrp7d51lkkdmndg6s1v")))

(define-public crate-rocket-simple-authorization-0.9.5 (c (n "rocket-simple-authorization") (v "0.9.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "1qsi1ypnmvckrl3d3i83zsbkb044z5s8rfrzqaavp0b30dwxzpsr")))

(define-public crate-rocket-simple-authorization-0.9.6 (c (n "rocket-simple-authorization") (v "0.9.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "0v36sv5wnndd05ag56xiw4iz3bwz4yi443vsp803yj0w3qcqrag7")))

(define-public crate-rocket-simple-authorization-0.9.7 (c (n "rocket-simple-authorization") (v "0.9.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "0c6j3py8j8li6xh16pg0sfsrn399hliqpavyzcpy4n17k0h6ch18")))

(define-public crate-rocket-simple-authorization-0.9.8 (c (n "rocket-simple-authorization") (v "0.9.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "1zin09syzk76pw8n4ada3vfx0838lrs2z7dq4yzkvlhjal83lhjd")))

(define-public crate-rocket-simple-authorization-0.10.0 (c (n "rocket-simple-authorization") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "0a3s0hjq23zz6b181lbk4x5kcsnazgw231p1v6braidyijnggqlf")))

(define-public crate-rocket-simple-authorization-0.10.1 (c (n "rocket-simple-authorization") (v "0.10.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "009q85ic8giajjfszwnghi2i8sgzrp3aywh82w922ddbs7sn2alb")))

(define-public crate-rocket-simple-authorization-0.10.2 (c (n "rocket-simple-authorization") (v "0.10.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "1h2hk3alqnlsvnjrvlrpphgf0q7b9sg6qz7zqh94d124aklyliam")))

(define-public crate-rocket-simple-authorization-0.10.3 (c (n "rocket-simple-authorization") (v "0.10.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "10fd7fqbkp0r9lczczkq9xc60bpsr5kpj6szs5apg0623kmgms3q") (r "1.67")))

(define-public crate-rocket-simple-authorization-0.10.5 (c (n "rocket-simple-authorization") (v "0.10.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "rocket") (r "^0.5.0-rc.4") (d #t) (k 0)) (d (n "short-crypt") (r "^1") (d #t) (k 2)))) (h "1jqh41xlk68xj4hbryw7aa85icl2x716fr1n4haida8x8y8pc03w") (r "1.69")))

