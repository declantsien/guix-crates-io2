(define-module (crates-io ro ck rocket_airlock) #:use-module (crates-io))

(define-public crate-rocket_airlock-0.0.0 (c (n "rocket_airlock") (v "0.0.0") (h "18n6v536a0kyk2l9b7cwbj75243h1wvl0ixpg0ka6mwpq6q951a1")))

(define-public crate-rocket_airlock-0.3.0-rc.1 (c (n "rocket_airlock") (v "0.3.0-rc.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("secrets"))) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "07a37vf2wxckn5y89vv1335hqaj4gib0z1l1xdv3m6vkb9qlw5cc")))

