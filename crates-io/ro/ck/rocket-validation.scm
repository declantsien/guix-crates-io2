(define-module (crates-io ro ck rocket-validation) #:use-module (crates-io))

(define-public crate-rocket-validation-0.1.0 (c (n "rocket-validation") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (k 0)) (d (n "validator") (r "^0.14.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j2k20680gnn63s9g71xq7dnbw3pp02kgdw8ia9bf7c1x8878rj0") (r "1.56.0")))

(define-public crate-rocket-validation-0.1.1 (c (n "rocket-validation") (v "0.1.1") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (f (quote ("json"))) (k 0)) (d (n "validator") (r "^0.15.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mb0i7zhxyi74gi4ia4izs6xwmp1ykrxmjlm1m10dsf69f1f5h5x") (r "1.57.0")))

(define-public crate-rocket-validation-0.1.2 (c (n "rocket-validation") (v "0.1.2") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (f (quote ("json"))) (k 0)) (d (n "validator") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qd6md4b5jwy9sqvr7zsqbc5gykfy8v9j3sr909y51ccza055xvp") (r "1.57.0")))

(define-public crate-rocket-validation-0.1.3 (c (n "rocket-validation") (v "0.1.3") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (f (quote ("json"))) (k 0)) (d (n "validator") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "123kmf9fq8s2f1d0a6rd7rr3j0vig9rj14x1rxxlzr5n1bh41pn4") (r "1.59.0")))

(define-public crate-rocket-validation-0.1.4 (c (n "rocket-validation") (v "0.1.4") (d (list (d (n "rocket") (r "^0.5.0") (f (quote ("json"))) (k 0)) (d (n "validator") (r "^0.16.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v27bcrapxny2rzgy57d8w5kn2rpxpim23lz4hzryxrrgk3a6hx2") (y #t) (r "1.59.0")))

(define-public crate-rocket-validation-0.2.0 (c (n "rocket-validation") (v "0.2.0") (d (list (d (n "rocket") (r "^0.5.0") (f (quote ("json"))) (k 0)) (d (n "validator") (r "^0.16.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0irfwjisj7dz8addm2321679cx80kkgjndkzas2crp8ndkvx02vv") (r "1.59.0")))

