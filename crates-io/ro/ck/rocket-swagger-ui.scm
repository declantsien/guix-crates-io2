(define-module (crates-io ro ck rocket-swagger-ui) #:use-module (crates-io))

(define-public crate-rocket-swagger-ui-0.1.0 (c (n "rocket-swagger-ui") (v "0.1.0") (d (list (d (n "swagger-ui") (r "^0.1.0") (d #t) (k 0)))) (h "16gz17p083b5c6i3yhfmx0k6r2ka2fnjm7x5vdbhnsg3p4hgk3g5")))

(define-public crate-rocket-swagger-ui-0.1.2 (c (n "rocket-swagger-ui") (v "0.1.2") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "swagger-ui") (r "^0.1.2") (d #t) (k 0)))) (h "1bd5ffa5wgsrvry4ri6wy79c8fg9ixxa2zakw680bxfw6spmljv0")))

(define-public crate-rocket-swagger-ui-0.1.3 (c (n "rocket-swagger-ui") (v "0.1.3") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "swagger-ui") (r "^0.1.3") (d #t) (k 0)))) (h "18d9hb2klihi6lfklgl5x22wb27121csibvbpvi1zvnjc58wssya")))

(define-public crate-rocket-swagger-ui-0.1.5 (c (n "rocket-swagger-ui") (v "0.1.5") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "swagger-ui") (r "^0.1") (d #t) (k 0)))) (h "13hqb19sd410y3y00pl0cw79ldfbaap1jxymvrk5z4zma01a8csg")))

