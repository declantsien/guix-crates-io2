(define-module (crates-io ro ck rocket_lamb) #:use-module (crates-io))

(define-public crate-rocket_lamb-0.1.0 (c (n "rocket_lamb") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (k 0)))) (h "0kqqc649wm2vxjrd2dz2gdwrifwrf2lqp6cc3brz83fvchddzc9l")))

(define-public crate-rocket_lamb-0.1.1 (c (n "rocket_lamb") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (k 0)))) (h "14rcx4gcvm7ci5cqjb34kilfsiywdsfxd0azf7mbk97b1hrm70m7")))

(define-public crate-rocket_lamb-0.2.0 (c (n "rocket_lamb") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (k 0)))) (h "0266f88g1b8i67dkdv8q4jdi777dgiysjd494nck67dczgwzkj02")))

(define-public crate-rocket_lamb-0.3.0 (c (n "rocket_lamb") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (k 0)))) (h "0ylgxbjlq3a47q9qamvfmfhd63537gqi6r7nmjnc4k4ki54arkca")))

(define-public crate-rocket_lamb-0.3.1 (c (n "rocket_lamb") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (k 0)))) (h "175pahh2mcsxybix3izvglyxakbb860d3vi184np8q04yh4r3q46")))

(define-public crate-rocket_lamb-0.4.0 (c (n "rocket_lamb") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (k 0)))) (h "13k7qpa48idb7z90hmm81974glv8yi9zm87w9n1r4rcx71vzqpkp")))

(define-public crate-rocket_lamb-0.5.0 (c (n "rocket_lamb") (v "0.5.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (k 0)))) (h "1s2cl9xn1n2ysr10j0nlh3r1b0xcsmxp2z2d8dhjmkxq9lfx4ai7")))

(define-public crate-rocket_lamb-0.6.0 (c (n "rocket_lamb") (v "0.6.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (k 0)))) (h "1gdlx7mkpz309ysnc8m1x3z30n7zk2lwld4m6fw32ycg6cbcpjch")))

