(define-module (crates-io ro ck rocket-governor-derive) #:use-module (crates-io))

(define-public crate-rocket-governor-derive-0.0.1-rc.2 (c (n "rocket-governor-derive") (v "0.0.1-rc.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kcj8y9slrk1j0fcmraapqpww5d2raidh6i5acsw1js5s1jfdc43")))

(define-public crate-rocket-governor-derive-0.0.1-rc.3 (c (n "rocket-governor-derive") (v "0.0.1-rc.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1svcpbj22d4ilg4xhfcq5jrggz434i1cn8xjy2v986490fasq1yb")))

(define-public crate-rocket-governor-derive-0.0.1-rc.4 (c (n "rocket-governor-derive") (v "0.0.1-rc.4") (d (list (d (n "rocket-governor") (r "^0.0.1-rc.9") (d #t) (k 0)))) (h "02k5wg5n9a6ja86qmlzzz7ijs0vwixc3m62xbgjg9mp3nsyiwa47")))

