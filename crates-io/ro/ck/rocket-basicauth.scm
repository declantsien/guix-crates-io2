(define-module (crates-io ro ck rocket-basicauth) #:use-module (crates-io))

(define-public crate-rocket-basicauth-1.0.0 (c (n "rocket-basicauth") (v "1.0.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "1mxcrhqxjj0i90pkm7d5hvi7px80ymssd7nm0w32gv05x251xvxn")))

(define-public crate-rocket-basicauth-2.1.0 (c (n "rocket-basicauth") (v "2.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "09ms5fazd6hg7ch1r59pchzkyvx9q911j6mlavc6sgbwj9r5mzjz") (f (quote (("default" "log"))))))

(define-public crate-rocket-basicauth-2.1.1 (c (n "rocket-basicauth") (v "2.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "023almysmnhfh1l99c0msrmibvmsyhddf5iqq62ngg5pal7qq4kr") (f (quote (("default" "log"))))))

(define-public crate-rocket-basicauth-3.0.0 (c (n "rocket-basicauth") (v "3.0.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "17ydx2fcrirdwgs82f3hbkpqnypvf1klifd9lkafn908zkn3g3x2") (f (quote (("default" "log"))))))

