(define-module (crates-io ro ck rocksdb) #:use-module (crates-io))

(define-public crate-rocksdb-0.0.1 (c (n "rocksdb") (v "0.0.1") (h "0arbhrqipyl4dl34xbmymldpaw6bvd6jpyzn2l6dw5hy73dv8cs9")))

(define-public crate-rocksdb-0.0.2 (c (n "rocksdb") (v "0.0.2") (h "1l14cw6kc1cnq8kr9dwypr0yv29h3dpmxgys5f1m6cksvrb5cmbc")))

(define-public crate-rocksdb-0.0.3 (c (n "rocksdb") (v "0.0.3") (h "09bcy2ar3w7qq13n4qcpyh7k0qbj1b1982yj7z2jbpixdkp3q539")))

(define-public crate-rocksdb-0.0.4 (c (n "rocksdb") (v "0.0.4") (h "0gnn7h394m3w12r8nlng0amhz7is7mm6ycl5k8x366qlm94an4lp")))

(define-public crate-rocksdb-0.0.5 (c (n "rocksdb") (v "0.0.5") (h "1l3k8gqqiws7prdpbfs7cajw15npv1csy54d3sxp6yjiliyfw02p")))

(define-public crate-rocksdb-0.0.6 (c (n "rocksdb") (v "0.0.6") (h "06mjxbh64b0nlgkfxps6pfffjrpm54hdfvszpyiakc187ksm4i6w") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.0.7 (c (n "rocksdb") (v "0.0.7") (h "173rm47bz0312g4ppalndy41ni9sga5c0miwm3nhlylfyy975wgm") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.0.8 (c (n "rocksdb") (v "0.0.8") (h "162w9pd3l6fmmfz8qnhrpr66n7gzpwdsj7687ynyysjkkbnhsx5i") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.1.0 (c (n "rocksdb") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "030mrfwn5i9wlsb1ndll1fb3zzphvj2dv1qnm65qrx53b3ivz7m1") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.1.1 (c (n "rocksdb") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0pnbxaiddm6xc20lq4s0wvk4i4f7w44lfnycbwh42cx0rdajlgf7") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.2.0 (c (n "rocksdb") (v "0.2.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "14m4x93x7pv38jpsdvwy9zbyvhh8b2s3fws3qlwxnnmp12yz041r") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.2.1 (c (n "rocksdb") (v "0.2.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "04g4h0axj81qq13vk8k3gj6lpjbmn9jj49f6n9q2z0za2xfqbvgz") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.2.2 (c (n "rocksdb") (v "0.2.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0p6dpkqjvnjgpg1di62fsi97nyx822njshpf53daz3vbb9ra91y7") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.3.0 (c (n "rocksdb") (v "0.3.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0xkrwbphgsmc5i4x0j5d8vb4hr7bv2ihmdnqdy6nxf8b1cjznpr7") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.3.1 (c (n "rocksdb") (v "0.3.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0n6kp824y7sjvi0wkbzkc6ca7byj52z5gzsj7i4ypfmxcszzpjh4") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.3.2 (c (n "rocksdb") (v "0.3.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1lfvva18c6ysd7x5ihsgi3wss7w4kvj38ygbfzdb79iv548asbbh") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.3.3 (c (n "rocksdb") (v "0.3.3") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1r5sxfgrchw3igd86igbqd2rjy764aqp5i3n7vv3yrqrjcqsxbxa") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.3.4 (c (n "rocksdb") (v "0.3.4") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "043ivxifdbrrwjdgvj2qg5h6dvshljy1h1bqlxq4k3b4j4d1g6nc") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.3.5 (c (n "rocksdb") (v "0.3.5") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "0s7c12khzv9q1pl0jzbq1pazgmrxilijnj2a1z4sawdcmb02hlpv") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.4.0 (c (n "rocksdb") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.13") (d #t) (k 0)))) (h "0xfq9m03cyp1vwglq73g4a9iydjm3rmx5bgvbxa6najy4jblx38c") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.4.1 (c (n "rocksdb") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.13") (d #t) (k 0)))) (h "1vda8hapk5kmyrbp0c72n872wyvgsw1anh3i0vr2qjxprh7cwmjn") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.5.0-rc.1 (c (n "rocksdb") (v "0.5.0-rc.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.4.0") (d #t) (k 0)))) (h "036y5ff00c1sdjs2gavm1imzjrdvcirmjm8wn1mil4mmbaigqwkx") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.4.2 (c (n "rocksdb") (v "0.4.2") (d (list (d (n "libc") (r "^0.2.13") (d #t) (k 0)))) (h "1cg4j96812zza2bkf6vmzn4kw3iw2z32c5vsqhssdrz081f5vv0f") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.5.0 (c (n "rocksdb") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.4.0") (d #t) (k 0)))) (h "03p34qqsfmqannqkdrs841g9rwcc9zsvgscfc5xhljyyjbkff2xd") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.5.1 (c (n "rocksdb") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.4.1") (d #t) (k 0)))) (h "0cgchic7lvgi6xv3pxzjcsc0kxxmaj9iaxa2baib133c4cv0jgdv") (f (quote (("valgrind") ("default")))) (y #t)))

(define-public crate-rocksdb-0.6.0 (c (n "rocksdb") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1fgxc8vbja9ah4d35c7iawz5w6ns08h406m6m5y238yc7bj1bs8g") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.6.1 (c (n "rocksdb") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1zzqi2xd7sgjnl3ri1in2b0qi9n6a35yy0wq8l481phvjrfi7kvg") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.7.0 (c (n "rocksdb") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0cw3inaz3a6csd5418h5ycfqz6nb9rgd3vd7bw590xn7iacxh5g0") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.7.1 (c (n "rocksdb") (v "0.7.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.6.1") (d #t) (k 0)))) (h "0m9s67zz4aw2b5mjlgyf687a7pyp2210rzcinv3mgkf0j5siq237") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.7.2 (c (n "rocksdb") (v "0.7.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.6.2") (d #t) (k 0)))) (h "0hivyv9zz68fbvp2n8p51p5gg6gm6xj3lvsz596ba889jr5n601v") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.8.0 (c (n "rocksdb") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.6.2") (d #t) (k 0)))) (h "1i76wjvii51m64dbqnr4x4cvygfqwha74dkbp6bl3fjdbjszrfqi") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.8.1 (c (n "rocksdb") (v "0.8.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.6.2") (d #t) (k 0)))) (h "0rgyl70qjgjri3xdapcnsipppwijhx604p8zkhxhkwcm381gldp2") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.8.2 (c (n "rocksdb") (v "0.8.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.7.1") (d #t) (k 0)))) (h "028pwx531p6ig4lv07aqgk73nbacdzh117464x0mcim5099c072y") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.8.3 (c (n "rocksdb") (v "0.8.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.10.2") (d #t) (k 0)))) (h "1sqn6jivfjrfvw3hz6bdbf6jflwi4245h93lyn3znn61q9w392bz") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.9.1 (c (n "rocksdb") (v "0.9.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.10.2") (d #t) (k 0)))) (h "1hs2vkcagr7v4yl8ygkahsp2bvxjkj7v6lm8z6rz5r6lpqd4dss1") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.10.0 (c (n "rocksdb") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.11.3") (d #t) (k 0)))) (h "0vyld3kngcmh120p24z9gv9qdk07ssppip6lkrr3fvyjl4f021m8") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.10.1 (c (n "rocksdb") (v "0.10.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.14.2") (d #t) (k 0)))) (h "1vgjk0jb2jrzpwd6r1d52420rwvdkyzg3k919paj2vvfamp75gir") (f (quote (("valgrind") ("default"))))))

(define-public crate-rocksdb-0.11.0-pre.0 (c (n "rocksdb") (v "0.11.0-pre.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.14.3") (d #t) (k 0)))) (h "1qsvzm7g2nvi6va833l35js9h61q3kb7jyyv3h2g3wqkh5gywqif") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("lz4" "librocksdb-sys/lz4") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2")))) (y #t)))

(define-public crate-rocksdb-0.11.0 (c (n "rocksdb") (v "0.11.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.14.3") (d #t) (k 0)))) (h "0pjbjaamwaxj6drbj4q2g9axw84x5b66d56n9zxkn9zxzsbicrgi") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("lz4" "librocksdb-sys/lz4") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.12.0 (c (n "rocksdb") (v "0.12.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.17.2") (d #t) (k 0)))) (h "14h5dnbl31711wvdkjmflwpd04ns8264259klqj2q087k6kfw713") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("lz4" "librocksdb-sys/lz4") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.12.1 (c (n "rocksdb") (v "0.12.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.17.2") (d #t) (k 0)))) (h "0h7368cipvh48416ld1xd3davl22mwlsbi3b1q4qasanjzxpvjiy") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("lz4" "librocksdb-sys/lz4") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.12.2 (c (n "rocksdb") (v "0.12.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^5.18.3") (d #t) (k 0)))) (h "0575vz4qmcn285b67m98ygk1sgg71d234w9krbxrp93rnfm157nj") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("lz4" "librocksdb-sys/lz4") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.12.3 (c (n "rocksdb") (v "0.12.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^6.1.2") (d #t) (k 0)))) (h "0vb3nwp46zsh79bcp3ifcmn0iggwv9hmjr208ny2xgr65v1j6x9f") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("lz4" "librocksdb-sys/lz4") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.12.4 (c (n "rocksdb") (v "0.12.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^6.1.3") (d #t) (k 0)))) (h "1y5scp8f0rqjxj8d3cm6p20hkpmgpn33phvc1pcnziq6wsgywnyn") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("lz4" "librocksdb-sys/lz4") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.13.0 (c (n "rocksdb") (v "0.13.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^6.2.4") (d #t) (k 0)))) (h "0hz6q63bbj2l970vf855g43jsj2icz4d2zdb7qyi1il1d489n1hj") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("lz4" "librocksdb-sys/lz4") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.14.0 (c (n "rocksdb") (v "0.14.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^6.7.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.21") (d #t) (k 2)))) (h "0jhdqhlw96cx1vwjg489sl6dxmysb6zr2rhhq5qws4r4kaligak1") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("lz4" "librocksdb-sys/lz4") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.15.0 (c (n "rocksdb") (v "0.15.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^6.11.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.21") (d #t) (k 2)))) (h "1rlp2gf0xv1pfjs2a4g2jkl0sb863vimmbsf8xc4s119qh13rn13") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("lz4" "librocksdb-sys/lz4") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.16.0 (c (n "rocksdb") (v "0.16.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^6.17.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1cs8bhin1galcwmy2qhhianb7p21r2zmjgb4vv891z4bv97i6jf7") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("multi-threaded-cf") ("lz4" "librocksdb-sys/lz4") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.17.0 (c (n "rocksdb") (v "0.17.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^6.20.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "19s4iwpq03rz2vsjjjxypx08sna5y3cvwcc629hq4b6grajyqqks") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("multi-threaded-cf") ("lz4" "librocksdb-sys/lz4") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.18.0 (c (n "rocksdb") (v "0.18.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.6.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1402jb6kvbgxkjy9wxv43fawf88c8y3wcjqqil9agwaz90ll23v2") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("serde1" "serde") ("rtti" "librocksdb-sys/rtti") ("multi-threaded-cf") ("lz4" "librocksdb-sys/lz4") ("jemalloc" "librocksdb-sys/jemalloc") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.19.0 (c (n "rocksdb") (v "0.19.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1g4phvfxafs6a1vm5dn63jlwql1vfmvxj8jaldiwrh3h3pm655by") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("serde1" "serde") ("rtti" "librocksdb-sys/rtti") ("multi-threaded-cf") ("lz4" "librocksdb-sys/lz4") ("jemalloc" "librocksdb-sys/jemalloc") ("io-uring" "librocksdb-sys/io-uring") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.20.0 (c (n "rocksdb") (v "0.20.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "14m95aw6nlxfc44p0dmbylc63x89526i8cggwpw1d1cnpadgmqqx") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("serde1" "serde") ("rtti" "librocksdb-sys/rtti") ("multi-threaded-cf") ("lz4" "librocksdb-sys/lz4") ("jemalloc" "librocksdb-sys/jemalloc") ("io-uring" "librocksdb-sys/io-uring") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2"))))))

(define-public crate-rocksdb-0.20.1 (c (n "rocksdb") (v "0.20.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "16ca3hq32ipf7245x69xv7slljzi7z9pi417bzapa7nfgxw3jm01") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("serde1" "serde") ("rtti" "librocksdb-sys/rtti") ("multi-threaded-cf") ("lz4" "librocksdb-sys/lz4") ("jemalloc" "librocksdb-sys/jemalloc") ("io-uring" "librocksdb-sys/io-uring") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2")))) (r "1.60")))

(define-public crate-rocksdb-0.21.0 (c (n "rocksdb") (v "0.21.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1zlz55fkk55nln4jchifx9ishv3dj4a2w3abw060mma18051fvxv") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("serde1" "serde") ("rtti" "librocksdb-sys/rtti") ("multi-threaded-cf") ("lz4" "librocksdb-sys/lz4") ("jemalloc" "librocksdb-sys/jemalloc") ("io-uring" "librocksdb-sys/io-uring") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2")))) (r "1.60")))

(define-public crate-rocksdb-0.22.0 (c (n "rocksdb") (v "0.22.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librocksdb-sys") (r "^0.16.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "19x7c2m7j0lsfwplgx4rdikqgmbwaqki25k9ll7cvf6psrakxlbb") (f (quote (("zstd" "librocksdb-sys/zstd") ("zlib" "librocksdb-sys/zlib") ("valgrind") ("snappy" "librocksdb-sys/snappy") ("serde1" "serde") ("rtti" "librocksdb-sys/rtti") ("multi-threaded-cf") ("lz4" "librocksdb-sys/lz4") ("jemalloc" "librocksdb-sys/jemalloc") ("io-uring" "librocksdb-sys/io-uring") ("default" "snappy" "lz4" "zstd" "zlib" "bzip2") ("bzip2" "librocksdb-sys/bzip2")))) (r "1.66.0")))

