(define-module (crates-io ro ck rocky) #:use-module (crates-io))

(define-public crate-rocky-0.1.0 (c (n "rocky") (v "0.1.0") (d (list (d (n "threadpool") (r "^0.1") (d #t) (k 0)))) (h "1is8in2dc54vi682iqxswbjka6wsfi20rnqbl75ssjwfp75phn0v")))

(define-public crate-rocky-0.2.0 (c (n "rocky") (v "0.2.0") (d (list (d (n "redis") (r "^0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.26") (d #t) (k 0)) (d (n "url") (r "^0.2.35") (d #t) (k 0)))) (h "0sjgd5nhz3wh1qmc0xpcddz7yv2ydf8g3ddsmvzbscw7x8a2yw40")))

(define-public crate-rocky-0.2.1 (c (n "rocky") (v "0.2.1") (d (list (d (n "redis") (r "~0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "~0.1.4") (d #t) (k 0)) (d (n "time") (r "~0.1.26") (d #t) (k 0)) (d (n "url") (r "~0.2.35") (d #t) (k 0)))) (h "02jfhikxx0szip9c00ixm8cbakl3q1gjvcpryv4zp8m0lkn3q8iy")))

(define-public crate-rocky-0.2.2 (c (n "rocky") (v "0.2.2") (d (list (d (n "redis") (r "~0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "~0.1.4") (d #t) (k 0)) (d (n "time") (r "~0.1.26") (d #t) (k 0)) (d (n "url") (r "~0.2.35") (d #t) (k 0)))) (h "0hga7lg7wwai7nw2v9i5y6l628dyxbi0qxnpl504fhld5yh8fbnf")))

(define-public crate-rocky-0.2.3 (c (n "rocky") (v "0.2.3") (d (list (d (n "redis") (r "~0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "~0.1.4") (d #t) (k 0)) (d (n "time") (r "~0.1.26") (d #t) (k 0)) (d (n "url") (r "~0.2.35") (d #t) (k 0)))) (h "1jq8sz6hb35rlj5jskymfrrsmf4gvr8l41922l032rmhnl0j59w0")))

