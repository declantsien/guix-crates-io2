(define-module (crates-io ro ck rocket-starter) #:use-module (crates-io))

(define-public crate-rocket-starter-0.1.0 (c (n "rocket-starter") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "179fdl9r5pddqahwxj4r8112yhqip0klsfd0dzc3z7amz6jnvr1z")))

(define-public crate-rocket-starter-0.2.0 (c (n "rocket-starter") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "14l7kixc1cyb0nxlsgh8fbr2kf0kgw72skj841lhfbknzahj2zw8")))

(define-public crate-rocket-starter-0.3.0 (c (n "rocket-starter") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i29lhy83ykyakr842m85nbmj5sn7p8iavm9v22sjkqx3bsvgdai")))

(define-public crate-rocket-starter-0.4.0 (c (n "rocket-starter") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 1)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 1)))) (h "010jzf9cbv4v1z9ipq518m1rkxpkynp8pwdbk2vz02gfnjv51zci")))

