(define-module (crates-io ro ck rocket_i18n) #:use-module (crates-io))

(define-public crate-rocket_i18n-0.1.0 (c (n "rocket_i18n") (v "0.1.0") (d (list (d (n "gettext-rs") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.3.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "04cclcnll0dvxn5yhiz7v9wam1kcx69vn1kpkqk6287y7m67sl7p")))

(define-public crate-rocket_i18n-0.3.0 (c (n "rocket_i18n") (v "0.3.0") (d (list (d (n "actix-web") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "gettext") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0-rc.1") (o #t) (d #t) (k 0)))) (h "0cxhpxikil8l6r5fz1z3r8p6bn9hr5gb9d803np86c6nv2hm5c10") (f (quote (("default" "rocket") ("build"))))))

(define-public crate-rocket_i18n-0.3.1 (c (n "rocket_i18n") (v "0.3.1") (d (list (d (n "actix-web") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "gettext") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0-rc.1") (o #t) (d #t) (k 0)))) (h "1ida3a7c5ihvc7nwwjg8snnnq1pqyvbbh9bdm0h0sh4c9hwwf3gi") (f (quote (("default" "rocket") ("build"))))))

(define-public crate-rocket_i18n-0.4.0 (c (n "rocket_i18n") (v "0.4.0") (d (list (d (n "actix-web") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "gettext") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0sy8qaf71y7cp7z59bfxy48i72cabk4h4cilxqwcsl06dlacwxpw") (f (quote (("default" "rocket"))))))

(define-public crate-rocket_i18n-0.4.1 (c (n "rocket_i18n") (v "0.4.1") (d (list (d (n "actix-web") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "gettext") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0f53sbzb1rz25vh2m39p1rnxc583dv3fjysm0q2sq95qfb4g9yfv") (f (quote (("default" "rocket"))))))

