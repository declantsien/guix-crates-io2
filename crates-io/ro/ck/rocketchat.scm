(define-module (crates-io ro ck rocketchat) #:use-module (crates-io))

(define-public crate-rocketchat-0.1.0 (c (n "rocketchat") (v "0.1.0") (h "1gw7a0sqxkn6bcq9hamnx7b6lasq9j30ii0hajzgg7kbcvn8xb73")))

(define-public crate-rocketchat-0.2.0 (c (n "rocketchat") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "08arldsldzwzk441i4ldw68lr6xzwpm34rz1in39xj70kgph4wj5")))

(define-public crate-rocketchat-0.3.0 (c (n "rocketchat") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c76iw5zzdvk3byvwdfywllpp5psml3kc5ahahpyh1m11jjrr8yy")))

(define-public crate-rocketchat-0.3.1 (c (n "rocketchat") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p3hrh6lpkk2gwnxq43c3ps05926lzsihy8vlvhr1am5cp40hzx0")))

(define-public crate-rocketchat-0.4.0 (c (n "rocketchat") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "16siggqcb12xnm9yyvl0qgzk3a4vih4a5fxm01f7r1kbaihlik54")))

(define-public crate-rocketchat-0.4.1 (c (n "rocketchat") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "1namfml7irpdv4rgbhfhvkh28na3v6cz646688i6fv7ijkc970qv")))

