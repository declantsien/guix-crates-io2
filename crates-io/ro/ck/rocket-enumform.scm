(define-module (crates-io ro ck rocket-enumform) #:use-module (crates-io))

(define-public crate-rocket-enumform-0.5.0-rc.1 (c (n "rocket-enumform") (v "0.5.0-rc.1") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (d #t) (k 0)))) (h "04qdqyxvdni0fiyj17daki7rh27g000a5qwp12glnpb0psin6dkw")))

