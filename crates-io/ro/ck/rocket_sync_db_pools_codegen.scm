(define-module (crates-io ro ck rocket_sync_db_pools_codegen) #:use-module (crates-io))

(define-public crate-rocket_sync_db_pools_codegen-0.1.0-rc.1 (c (n "rocket_sync_db_pools_codegen") (v "0.1.0-rc.1") (d (list (d (n "devise") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "099fkhhzx8a0f26b2b6r7fdzy1ffybwsl9c93mp3ddad16680rsj")))

(define-public crate-rocket_sync_db_pools_codegen-0.1.0-rc.2 (c (n "rocket_sync_db_pools_codegen") (v "0.1.0-rc.2") (d (list (d (n "devise") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "1avspx56mx08cqzq8i5swp6afxjlxdr6j5fsjg5njglj6b9g43i8") (r "1.56")))

(define-public crate-rocket_sync_db_pools_codegen-0.1.0-rc.3 (c (n "rocket_sync_db_pools_codegen") (v "0.1.0-rc.3") (d (list (d (n "devise") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "0x14xgqxr3fp4kkn44k5b3djw374qnfxkvwxsy1b2ijancnxf9nx") (r "1.56")))

(define-public crate-rocket_sync_db_pools_codegen-0.1.0-rc.4 (c (n "rocket_sync_db_pools_codegen") (v "0.1.0-rc.4") (d (list (d (n "devise") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "0x8697w5vid8q7xrsf97l9dj7frr7y6y1jciwqwnk60zfa1w9k3v") (r "1.56")))

(define-public crate-rocket_sync_db_pools_codegen-0.1.0 (c (n "rocket_sync_db_pools_codegen") (v "0.1.0") (d (list (d (n "devise") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "0s7fjnwyv37ki78ncjf4x2vzv4vhcxbrjp5iiv1714y7bn991j2w") (r "1.56")))

