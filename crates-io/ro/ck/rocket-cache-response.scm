(define-module (crates-io ro ck rocket-cache-response) #:use-module (crates-io))

(define-public crate-rocket-cache-response-0.1.0 (c (n "rocket-cache-response") (v "0.1.0") (d (list (d (n "rocket") (r "^0.3.15") (d #t) (k 0)))) (h "0mbsh3ap3yf0wp4bxkk9q6psfkyzi0b9agzc84kav9lwrph2fkkm")))

(define-public crate-rocket-cache-response-0.2.0 (c (n "rocket-cache-response") (v "0.2.0") (d (list (d (n "rocket") (r "^0.3.15") (d #t) (k 0)))) (h "0iy1lvdj4fg3082bjm57sfrabk0jh460ycnha9mdfjlwsj00f3rx")))

(define-public crate-rocket-cache-response-0.3.0 (c (n "rocket-cache-response") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "0dxxwg8y56slb2d6igfjw9bka99myr02l2z9yq2vc849fqavizcs")))

(define-public crate-rocket-cache-response-0.4.0 (c (n "rocket-cache-response") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "1a93dhgfr1iy7mvjnqwvkq4mc4g70s3xzk50fh28yzb9l9qq8vqw")))

(define-public crate-rocket-cache-response-0.5.0 (c (n "rocket-cache-response") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0sz3mnky0bnyzh8bzp5g9ljmfjsxfm5jxw7by6fd9lq459cxzfna")))

(define-public crate-rocket-cache-response-0.5.1 (c (n "rocket-cache-response") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "0vs8fhmad9zr4i22dq9nnv2cdfqahii8v3mdkzlkcs3kbwdzvchy")))

(define-public crate-rocket-cache-response-0.5.2 (c (n "rocket-cache-response") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "0b4i4jlazgapn1453rjli3p8pj361r767grsxssyq8nlb02hwrbf")))

(define-public crate-rocket-cache-response-0.5.3 (c (n "rocket-cache-response") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "0aslrg23b08j0wr92llf6kmj1kr16pc0amfsw97gyndvk9qi3j5b")))

(define-public crate-rocket-cache-response-0.5.4 (c (n "rocket-cache-response") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "0iwbrhyfcqqcnczq3hf1dx1c3kj67k2xg6nk3n5br5m9hh8qimh5") (y #t)))

(define-public crate-rocket-cache-response-0.5.5 (c (n "rocket-cache-response") (v "0.5.5") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "0mkn40k5pgh131ipwf84myrckzj0x3s2s1biw4p3wz0374h9dvd6")))

(define-public crate-rocket-cache-response-0.6.0 (c (n "rocket-cache-response") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "103kyjfx9kmc03h0q5hr7nqjjcww0clv8v2fqq187l5jv3h9ycqx")))

(define-public crate-rocket-cache-response-0.6.1 (c (n "rocket-cache-response") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "162ii9glavldf80i0jq1gd5lpgdn5jrqf957h5l0l4ayr2p40q7m")))

(define-public crate-rocket-cache-response-0.6.2 (c (n "rocket-cache-response") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "1hficfxj7s34wnyysl5iz0fw57yaq9hkl7vcnw8dhpnb52w1pini")))

(define-public crate-rocket-cache-response-0.6.3 (c (n "rocket-cache-response") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)))) (h "1znsi7fmkd2h9vmyg5rh68b0wh6c6i57sgp0cxpcf92dy1bgnsp9") (r "1.67")))

(define-public crate-rocket-cache-response-0.6.4 (c (n "rocket-cache-response") (v "0.6.4") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "rocket") (r "^0.5.0-rc.4") (d #t) (k 0)))) (h "0jd4217xypgjmiy50370wd73mb6qyknbb0kwhjb9qcxw801c0ah6") (r "1.69")))

