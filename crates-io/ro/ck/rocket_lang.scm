(define-module (crates-io ro ck rocket_lang) #:use-module (crates-io))

(define-public crate-rocket_lang-0.0.1 (c (n "rocket_lang") (v "0.0.1") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1k5006x05r58cbvbrxzan2bg68v5jskgl7n76h5f0bf6ajhny2f9")))

(define-public crate-rocket_lang-0.0.2 (c (n "rocket_lang") (v "0.0.2") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0b7acjl8zw6q307dq8isd3p6pvm9i2vag7j1rcwybwq9pidnxqkg")))

(define-public crate-rocket_lang-0.1.0 (c (n "rocket_lang") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0wky3i84lb57xd6nlyhbmj0m4adainh9qwjb30bdkrms6zv7ildi")))

(define-public crate-rocket_lang-0.1.1 (c (n "rocket_lang") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1crwk8f6schywbr5r8yvxl1fndln75m4wsrrj2a6zgcd1qig3gmv")))

(define-public crate-rocket_lang-0.1.2 (c (n "rocket_lang") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "00r999n3igl6rjq26vmhczslj8dxxhxxgpqhq811q2g2viw6d248")))

(define-public crate-rocket_lang-0.2.0 (c (n "rocket_lang") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1s3hm1kc8ikfz9ij49afzarc31666lksps94f8lbgg0vvqxglvlw")))

