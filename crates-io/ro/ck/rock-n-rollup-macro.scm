(define-module (crates-io ro ck rock-n-rollup-macro) #:use-module (crates-io))

(define-public crate-rock-n-rollup-macro-0.0.1 (c (n "rock-n-rollup-macro") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16cisbw2i1z5zwifpmd480assw114807iwc32ws1g6wjbaj828b8")))

(define-public crate-rock-n-rollup-macro-0.0.2 (c (n "rock-n-rollup-macro") (v "0.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s45v9jvjxbyb7dmvhvzib3395naxg55b3q4hzq17wbh6rln1zm7")))

