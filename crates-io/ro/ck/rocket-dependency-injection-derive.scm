(define-module (crates-io ro ck rocket-dependency-injection-derive) #:use-module (crates-io))

(define-public crate-rocket-dependency-injection-derive-0.1.0 (c (n "rocket-dependency-injection-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "04fgfjbbzbzq8y2513qrimh598g1v45163fkriz0hyyj88cby5sr")))

