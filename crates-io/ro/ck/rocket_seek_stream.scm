(define-module (crates-io ro ck rocket_seek_stream) #:use-module (crates-io))

(define-public crate-rocket_seek_stream-0.2.0 (c (n "rocket_seek_stream") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "range_header") (r "^0.2.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "tree_magic") (r "^0.2.1") (d #t) (k 0)))) (h "0zbsnpx7h89sddm3xa7b7gpy2mqcaqmi07qd7chm0ix504lip8vs")))

(define-public crate-rocket_seek_stream-0.2.1 (c (n "rocket_seek_stream") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "range_header") (r "^0.2.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "tree_magic") (r "^0.2.1") (d #t) (k 0)))) (h "1l9z5a4grd5f5cz92pb0gf2iqyg2jf78xjdrqajxr177dil50zgv")))

(define-public crate-rocket_seek_stream-0.2.3 (c (n "rocket_seek_stream") (v "0.2.3") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "range_header") (r "^0.2.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)) (d (n "tree_magic") (r "^0.2.1") (d #t) (k 0)))) (h "1pzp8allnpaqprq1bh50jvvmd8vbima0cwn234n5w0h7sm0lykik")))

(define-public crate-rocket_seek_stream-0.2.4 (c (n "rocket_seek_stream") (v "0.2.4") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "range_header") (r "^0.2") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "tree_magic") (r "^0.2") (d #t) (k 0)))) (h "10vdp3vi6b95j847kxf4d4g15qbhgfpyj2gdqna6g0q2if842ll6")))

(define-public crate-rocket_seek_stream-0.2.5 (c (n "rocket_seek_stream") (v "0.2.5") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "range_header") (r "^0.2") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "tree_magic") (r "^0.2") (d #t) (k 0)))) (h "1cid83kxa3pmp2m33plgrnqjpgqp2jgv66nr4qqys7bw0qp6lmmx")))

(define-public crate-rocket_seek_stream-0.2.6 (c (n "rocket_seek_stream") (v "0.2.6") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "range_header") (r "^0.2") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)) (d (n "tree_magic_mini") (r "^3.0.3") (d #t) (k 0)))) (h "0ypd7waja476ag9z33pkglr6a4r9jxpxc08hjzpg6j0m0qcbpa0p")))

