(define-module (crates-io ro ck rocksdb-sys) #:use-module (crates-io))

(define-public crate-rocksdb-sys-0.1.0 (c (n "rocksdb-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1djfc906jz7w24l24abkkgx1k8bkxy67hwfa9mvqvih7nlsf8n0c")))

(define-public crate-rocksdb-sys-0.2.0 (c (n "rocksdb-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xikswj2kxp7z6vr234p9qfwccxbfx04qs283vfr7krzc9scy53c")))

(define-public crate-rocksdb-sys-0.2.1 (c (n "rocksdb-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "078x2i0lck12vg7q8k2xbxpkjqypfgxi6lxryikb68y4iybi6jq1")))

(define-public crate-rocksdb-sys-0.2.2 (c (n "rocksdb-sys") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y06j2j3nj8hafxwnmyxzciisifradcxhahpv35yv35j1865khbi")))

