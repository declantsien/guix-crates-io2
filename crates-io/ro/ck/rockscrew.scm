(define-module (crates-io ro ck rockscrew) #:use-module (crates-io))

(define-public crate-rockscrew-0.1.0 (c (n "rockscrew") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "httparse") (r "^1.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("fs" "io-util" "io-std" "macros" "net" "rt"))) (d #t) (k 0)))) (h "1b1nin3z1m5c6v06qlwvyl155pfs5w2m5fj9mjap6f3h4ili9waw")))

(define-public crate-rockscrew-0.1.1 (c (n "rockscrew") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "httparse") (r "^1.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("fs" "io-util" "io-std" "macros" "net" "rt"))) (d #t) (k 0)))) (h "1nazxv7z5c01pbw0b1wzjqfrrxhldya36cppd0h203df66cb1srm")))

(define-public crate-rockscrew-0.2.0 (c (n "rockscrew") (v "0.2.0") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "compio") (r "^0.9.0") (f (quote ("macros" "nightly"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "httparse") (r "^1.8.0") (d #t) (k 0)))) (h "1ylxgrs18jwlwhj68jm5y1ma2shvmfmzqcq4cvzcnrw12zjfl55m") (f (quote (("polling" "compio/polling") ("default"))))))

