(define-module (crates-io ro ck rocketjson_data) #:use-module (crates-io))

(define-public crate-rocketjson_data-1.0.0 (c (n "rocketjson_data") (v "1.0.0") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "1mmqd3fm3qg50d7yz0zwdg4inm8wkyfgw5yi9y931nma5579hhwz") (y #t)))

(define-public crate-rocketjson_data-1.0.1 (c (n "rocketjson_data") (v "1.0.1") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "19n3mvpi876pj6x4g0jdasmsjm5bdzm0nfc6nlvy73xcz7lid4zs")))

(define-public crate-rocketjson_data-1.0.2 (c (n "rocketjson_data") (v "1.0.2") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "1cr7q5b27isjr7rvrkgyj3jh35r7664gfyh99f8vdc2c31inlfym")))

(define-public crate-rocketjson_data-1.0.3 (c (n "rocketjson_data") (v "1.0.3") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "1zmy4hy16893dgaaj1x61sij625dpd609aqckhbssi9cc9xy36d4")))

(define-public crate-rocketjson_data-1.0.4 (c (n "rocketjson_data") (v "1.0.4") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "1936f06ian4rc5zifdl6iyapn5sx2iqy3qpjw055nfqypmz78a0c")))

(define-public crate-rocketjson_data-1.0.5 (c (n "rocketjson_data") (v "1.0.5") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "0dfnbic0n1hb4pkin8r1766gp666z0jwqr04lshln3abhfshiw0y")))

(define-public crate-rocketjson_data-1.1.0 (c (n "rocketjson_data") (v "1.1.0") (d (list (d (n "diesel") (r "^1.4.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "1fwcwa0w6ymmkq2q11r0sc729cf2gnwhsz72qwgg6slh4yl6nljg")))

(define-public crate-rocketjson_data-1.1.1 (c (n "rocketjson_data") (v "1.1.1") (d (list (d (n "diesel") (r "^1.4.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "1hbyb93lq1i2gnzh08fwnv5m4a87mjfl6rmhkqsfc7q6prgxil2q")))

(define-public crate-rocketjson_data-1.1.2 (c (n "rocketjson_data") (v "1.1.2") (d (list (d (n "diesel") (r "^1.4.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-async-std-native-tls" "mysql"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "1bw93bvv2rc85zajjk56wlpm0wvsvjfh6fi5y6ivb2jh9cqbk4s9")))

(define-public crate-rocketjson_data-1.1.3 (c (n "rocketjson_data") (v "1.1.3") (d (list (d (n "diesel") (r "^1.4.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-async-std-native-tls" "mysql"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "0xzmqr5c1klv6pi7kinj68p36qkxmfchrpinfad3rz8jd8x53zai")))

(define-public crate-rocketjson_data-1.2.0 (c (n "rocketjson_data") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "06g2abcmcn70wbj2h6l35s58j6yxxxrd3xfsbj7qiir9gmkqd6b1")))

(define-public crate-rocketjson_data-1.2.1 (c (n "rocketjson_data") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "19kl44bsa2vxzrzdbhfh8dbhssbwf7immxh9yd11xqppm46j4qzm")))

