(define-module (crates-io ro ck rocket_empty) #:use-module (crates-io))

(define-public crate-rocket_empty-0.1.0 (c (n "rocket_empty") (v "0.1.0") (d (list (d (n "revolt_okapi") (r "^0.7.0-rc.1") (o #t) (k 0)) (d (n "revolt_rocket_okapi") (r "^0.8.0-rc.1") (o #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (k 0)))) (h "02il7lni94h2xpkzwg7zyxx4sa74hdbfr3wxggn94rb6n12phnlz") (f (quote (("schema" "revolt_rocket_okapi" "revolt_okapi") ("default"))))))

(define-public crate-rocket_empty-0.1.1 (c (n "rocket_empty") (v "0.1.1") (d (list (d (n "revolt_okapi") (r "^0.9.1") (o #t) (k 0)) (d (n "revolt_rocket_okapi") (r "^0.9.1") (o #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (k 0)))) (h "0s9xiqffd8yzdfggbdqfmjjc39a7rzpshy45wgz084lqgzj2429c") (f (quote (("schema" "revolt_rocket_okapi" "revolt_okapi") ("default"))))))

