(define-module (crates-io ro ck rocket-simpleauth) #:use-module (crates-io))

(define-public crate-rocket-simpleauth-0.1.0 (c (n "rocket-simpleauth") (v "0.1.0") (d (list (d (n "rocket") (r "^0.1.5") (d #t) (k 0)))) (h "1qx0gvaa2v94abi2g7mbf54ckmzgzqcrr9xkb40nz2yqxb2spwrz")))

(define-public crate-rocket-simpleauth-0.1.1 (c (n "rocket-simpleauth") (v "0.1.1") (d (list (d (n "rocket") (r "^0.2.6") (d #t) (k 0)))) (h "1gnw2fx650ivs6wgsp09lcdjxhlcm85bm7lr15rpa9zhg6dfr3yf")))

(define-public crate-rocket-simpleauth-0.2.0 (c (n "rocket-simpleauth") (v "0.2.0") (d (list (d (n "rocket") (r "^0.3.0") (d #t) (k 0)))) (h "1i8zza1kbbr6pkdknr6xsr8dgi7887v1kxc82rb1yif79rzc9f19")))

(define-public crate-rocket-simpleauth-0.3.0 (c (n "rocket-simpleauth") (v "0.3.0") (d (list (d (n "rocket") (r "^0.3.0") (d #t) (k 0)))) (h "02jrhsqpq00cw2y13q522pgicchfn04rp9iqqm5xam8w75yff39d")))

(define-public crate-rocket-simpleauth-0.4.0 (c (n "rocket-simpleauth") (v "0.4.0") (d (list (d (n "rocket") (r "^0.3.0") (d #t) (k 0)))) (h "04l7wjpa9l607k80n3hv05k7ifslc4yi03ysb8ll4vgpsbrb4nkv")))

