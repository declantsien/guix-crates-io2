(define-module (crates-io ro ck rocketchat-hooks) #:use-module (crates-io))

(define-public crate-rocketchat-hooks-0.1.0 (c (n "rocketchat-hooks") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "0mc0szb7zwf6f6s47kcdhff87iz6j4k64v7d1m7rxm13c4l7y29n")))

(define-public crate-rocketchat-hooks-0.2.0 (c (n "rocketchat-hooks") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "07i1l5x17b1qf8sba0hn8v12fy4ck1dc8k961fbysfgb81fbfyc8")))

