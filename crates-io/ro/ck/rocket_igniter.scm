(define-module (crates-io ro ck rocket_igniter) #:use-module (crates-io))

(define-public crate-rocket_igniter-0.0.1 (c (n "rocket_igniter") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.3.16") (d #t) (k 0)))) (h "0nbpji59cs5h4fhib3qjspjmn3rn3riwc07afkxwyybbf6l3mkxd")))

(define-public crate-rocket_igniter-0.0.2 (c (n "rocket_igniter") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (d #t) (k 0)))) (h "0n2z6vs5l7wka0rllhj7a8xzvny6v2nvzs3czfqyw7n154b0mzbg")))

(define-public crate-rocket_igniter-0.0.3 (c (n "rocket_igniter") (v "0.0.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "1wiss3pqccgmrp3hz6y17633bp086mnv7naj9mqb1fw1v020kyaq")))

