(define-module (crates-io ro ck rocket_csrf_guard_derive) #:use-module (crates-io))

(define-public crate-rocket_csrf_guard_derive-0.0.1 (c (n "rocket_csrf_guard_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "0gwqxrgy2r0qy378wlsjl63d6s76prcvsbdpwnky7px1zwh8zh0c")))

(define-public crate-rocket_csrf_guard_derive-0.0.2 (c (n "rocket_csrf_guard_derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "0g79x62ms9lki38p474c3y8bsl2mddc3n4ba72x6qbwn7iiz9qlz")))

