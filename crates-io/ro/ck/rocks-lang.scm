(define-module (crates-io ro ck rocks-lang) #:use-module (crates-io))

(define-public crate-rocks-lang-0.1.0 (c (n "rocks-lang") (v "0.1.0") (d (list (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "19sycjjbpv3al17a36s7drr3sgiakamh9lx7dyql8s421h33zk7d")))

(define-public crate-rocks-lang-0.1.1 (c (n "rocks-lang") (v "0.1.1") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0x47nym6z15wfbim7riv793vjaib7yaf8dipwlc59jj3a4xjpjg5")))

(define-public crate-rocks-lang-0.2.0 (c (n "rocks-lang") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "peekmore") (r "^1.3") (d #t) (k 0)) (d (n "rustyline") (r "^11.0") (d #t) (k 0)) (d (n "substring") (r "^1.4") (d #t) (k 0)))) (h "0lwxcxwj0kqkn08l964wqmqgd0zdmbjg5a53ffmiknb49ggzmmkp")))

(define-public crate-rocks-lang-0.2.1 (c (n "rocks-lang") (v "0.2.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "peekmore") (r "^1.3") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (d #t) (k 0)))) (h "1lqr72b4whg6jx1pk8xjvp23y4bf5c9j5zp3pgninhc1ami3qy8g")))

(define-public crate-rocks-lang-0.2.2 (c (n "rocks-lang") (v "0.2.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "peekmore") (r "^1.3") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (d #t) (k 0)))) (h "0gp7zx3ffh50fj30m1lsba48wsinx6b9v6ndsilada7l7z6vn0pd")))

