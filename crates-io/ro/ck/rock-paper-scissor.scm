(define-module (crates-io ro ck rock-paper-scissor) #:use-module (crates-io))

(define-public crate-rock-paper-scissor-0.1.0 (c (n "rock-paper-scissor") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "1isqkhbbybym7nagyc2xj0q8rigrqhg69p89vkxlzdgi7z4vjm7z")))

(define-public crate-rock-paper-scissor-0.1.1 (c (n "rock-paper-scissor") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "ucli") (r "^0.2.2") (d #t) (k 0)))) (h "0g17da7043wcq29kskp4wjnbn6m9vabr8jda461r38al7yxk0j10")))

