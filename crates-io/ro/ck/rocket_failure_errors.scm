(define-module (crates-io ro ck rocket_failure_errors) #:use-module (crates-io))

(define-public crate-rocket_failure_errors-0.2.0 (c (n "rocket_failure_errors") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1qk002yl4z877mkd5hkxwl9lrpma4h1fwfqmagjcbh7zhzfmq4kv")))

