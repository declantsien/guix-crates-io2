(define-module (crates-io ro ck rocket_okapi_codegen_fork) #:use-module (crates-io))

(define-public crate-rocket_okapi_codegen_fork-0.7.1 (c (n "rocket_okapi_codegen_fork") (v "0.7.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1skkfkqqpjk4xbyxgn37fwzr64jfk95n82rd9mrywcsfxmc8smc3")))

