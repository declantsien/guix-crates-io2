(define-module (crates-io ro ck rocket-download-response) #:use-module (crates-io))

(define-public crate-rocket-download-response-0.1.0 (c (n "rocket-download-response") (v "0.1.0") (d (list (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)))) (h "03afdpigq3wllb3gc9w1njnkqqldl3mh0rhdywaspqq9xl5drs9m")))

(define-public crate-rocket-download-response-0.1.1 (c (n "rocket-download-response") (v "0.1.1") (d (list (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)))) (h "0xzqqz1dh4ijv7y64n61b9fpmbnfippjcznhb6l1abwa5419lcmh")))

(define-public crate-rocket-download-response-0.1.2 (c (n "rocket-download-response") (v "0.1.2") (d (list (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)))) (h "12i4d33lyk1i98f0sbc3v4v13nzi80d84l1h87l42mbm97q5ayj2")))

(define-public crate-rocket-download-response-0.1.3 (c (n "rocket-download-response") (v "0.1.3") (d (list (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)))) (h "1fb8iwylby2if6x5yinirk30cm2bxvs229qd4vk636xjk3xy76kn")))

(define-public crate-rocket-download-response-0.1.4 (c (n "rocket-download-response") (v "0.1.4") (d (list (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)))) (h "1bjfvqa6196fmrayxsq8ax38z4jzcsnfwn378m9zbw0crynrs263")))

(define-public crate-rocket-download-response-0.2.0 (c (n "rocket-download-response") (v "0.2.0") (d (list (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "1mn368h2mad6mgsswf6jqva44iafs7qfhxfcw6zjm97dyczmg3zl") (y #t)))

(define-public crate-rocket-download-response-0.2.1 (c (n "rocket-download-response") (v "0.2.1") (d (list (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "1xqrjfn76rmnhbbnyfzqr8s3phdmcamcv55ll65rry94mabqzhr9") (y #t)))

(define-public crate-rocket-download-response-0.2.2 (c (n "rocket-download-response") (v "0.2.2") (d (list (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "11s08hm8v4ylf7fjsf7w8zrbm3zhpbggd7kqnlq0iblz1yqzsjxb") (y #t)))

(define-public crate-rocket-download-response-0.2.3 (c (n "rocket-download-response") (v "0.2.3") (d (list (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r ">= 0.3.16") (d #t) (k 0)) (d (n "rocket_codegen") (r ">= 0.3.16") (d #t) (k 2)))) (h "0hh9zyxyf54r87hcyn54d1czy8c1ycd52c3nnhdbpi057dlqb5rj")))

(define-public crate-rocket-download-response-0.3.0 (c (n "rocket-download-response") (v "0.3.0") (d (list (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0l78s350v25xp846fpypiyzcg31lrinkr4fh62fd55h4ahj0fdr7")))

(define-public crate-rocket-download-response-0.3.1 (c (n "rocket-download-response") (v "0.3.1") (d (list (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "04fddmf55qakqdm4gfq7d91n9f2lafhq63x4p24qqx291lqqjfmn")))

(define-public crate-rocket-download-response-0.3.2 (c (n "rocket-download-response") (v "0.3.2") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "1qmf5qnyksa3xgsahhqfnj3agsi1qj5yy9v8mf9vk35f38ifz072")))

(define-public crate-rocket-download-response-0.4.0 (c (n "rocket-download-response") (v "0.4.0") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0j2qb1gw62w18vpxgki2kn2dz0sa4l16l5jhp6a0hx5cn5ihrjkb") (y #t)))

(define-public crate-rocket-download-response-0.4.1 (c (n "rocket-download-response") (v "0.4.1") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0bzp8hhksd6i74vi0aqfqy46nkhdskdm5lx9731bnda2hfyr6kiw") (y #t)))

(define-public crate-rocket-download-response-0.4.2 (c (n "rocket-download-response") (v "0.4.2") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "16y78h2dr3pfmkm2yskczqbaa2ajnmv6sm29cylw8l4cz59fhjmq") (y #t)))

(define-public crate-rocket-download-response-0.4.3 (c (n "rocket-download-response") (v "0.4.3") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "0jdw2gagn8xvlwvr0bc984lxqx0gnavvg9gm4ijkz8rjdgkbsnb9") (y #t)))

(define-public crate-rocket-download-response-0.4.4 (c (n "rocket-download-response") (v "0.4.4") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "161da9yfmv49nj28shxji1nxqg914wwr4wnd7wq2njnfni3d72sf")))

(define-public crate-rocket-download-response-0.4.5 (c (n "rocket-download-response") (v "0.4.5") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)))) (h "1p0avrdzizxn5nfl1nq7609c7wjscss2v9zjxggcwz5brhsb50ka")))

(define-public crate-rocket-download-response-0.4.6 (c (n "rocket-download-response") (v "0.4.6") (d (list (d (n "educe") (r ">= 0.0.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "11m1n01930nav22lr5nr31g4anl652xr4cxq2n9mlb2p40987bxh")))

(define-public crate-rocket-download-response-0.4.7 (c (n "rocket-download-response") (v "0.4.7") (d (list (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "mime") (r "^0.3.12") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "1243pc27q4lgcgd8zq01fwarajzdi6vqfa8bykcq87jpf241bhp8")))

(define-public crate-rocket-download-response-0.4.8 (c (n "rocket-download-response") (v "0.4.8") (d (list (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "1pwqkfq438wpamhpjp4vv8s4sf0yf6kghd82147205ys6fzv99fn")))

(define-public crate-rocket-download-response-0.4.9 (c (n "rocket-download-response") (v "0.4.9") (d (list (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "1mys8hbzfn5izlld6y6c3vcab1knpxmk4bdy21bmykg5m4r05m11")))

(define-public crate-rocket-download-response-0.4.10 (c (n "rocket-download-response") (v "0.4.10") (d (list (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "1apfxlw66q8hzp618hg3vvp5647pdc9lkvgyanyyzxzjhvpyl2js")))

(define-public crate-rocket-download-response-0.4.11 (c (n "rocket-download-response") (v "0.4.11") (d (list (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "0z0qmi1idbxi696cijw0lady9s3p69gzikpmgfg4s95zfxdap7pb")))

(define-public crate-rocket-download-response-0.4.12 (c (n "rocket-download-response") (v "0.4.12") (d (list (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "0fsh7nc441xqshgjgmclqk2c9a2x5dqv0y7p00smw6mlynd82953")))

(define-public crate-rocket-download-response-0.4.13 (c (n "rocket-download-response") (v "0.4.13") (d (list (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (d #t) (k 0)))) (h "05gr2gxnzykv89h6w9c90lk2vr1mg9wd82g3rb2x2vpgas6b93v6")))

(define-public crate-rocket-download-response-0.5.0 (c (n "rocket-download-response") (v "0.5.0") (d (list (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "url-escape") (r "^0.1") (d #t) (k 0)))) (h "1bn1ixq7970r186i77z9v5jrqzz653frzjn1afgbjmwldkxabnp6")))

(define-public crate-rocket-download-response-0.5.1 (c (n "rocket-download-response") (v "0.5.1") (d (list (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "url-escape") (r "^0.1") (d #t) (k 0)))) (h "04srqmqyq9dcihsn324q72q296czwb9wc0ivd9ffgvahicrirr92")))

(define-public crate-rocket-download-response-0.5.2 (c (n "rocket-download-response") (v "0.5.2") (d (list (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "url-escape") (r "^0.1") (d #t) (k 0)))) (h "1kn62ds0vqwdw6bsjlhmpfns8mlnyjhnjjrvswy8s0cx3c5sdh4q")))

(define-public crate-rocket-download-response-0.5.3 (c (n "rocket-download-response") (v "0.5.3") (d (list (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)) (d (n "url-escape") (r "^0.1") (d #t) (k 0)))) (h "114vvdydylcxw0w7sp0ib9gpizx0ifdvks0gm1vclcfcb0j72jhy") (r "1.67")))

(define-public crate-rocket-download-response-0.5.4 (c (n "rocket-download-response") (v "0.5.4") (d (list (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.4") (d #t) (k 0)) (d (n "url-escape") (r "^0.1") (d #t) (k 0)))) (h "0glhjaymdryfn0czsvnqmbqqfc3dc7j83s7rp65fsrqnzhzn8y0p") (r "1.69")))

