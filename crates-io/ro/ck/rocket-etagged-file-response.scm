(define-module (crates-io ro ck rocket-etagged-file-response) #:use-module (crates-io))

(define-public crate-rocket-etagged-file-response-0.1.0 (c (n "rocket-etagged-file-response") (v "0.1.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)) (d (n "rocket-etag-if-none-match") (r "^0.1.0") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.15") (d #t) (k 0)))) (h "1n7bk5q5qw430m5l22zcfsixv6mlhfarkcqm537yxw7gfsk1h1xs")))

(define-public crate-rocket-etagged-file-response-0.1.1 (c (n "rocket-etagged-file-response") (v "0.1.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)) (d (n "rocket-etag-if-none-match") (r "^0.1.0") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.15") (d #t) (k 0)))) (h "0wxkmqiip4hj22gb3fb1z7287h7yr3py21ww1yky9w05gb4laijj")))

(define-public crate-rocket-etagged-file-response-0.2.0 (c (n "rocket-etagged-file-response") (v "0.2.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)) (d (n "rocket-etag-if-none-match") (r "^0.1.0") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.15") (d #t) (k 0)))) (h "1vklyyabzq06dfqrwgszbffvrn4q0rpn9yan2yh18vxay3n5208s")))

(define-public crate-rocket-etagged-file-response-0.2.1 (c (n "rocket-etagged-file-response") (v "0.2.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)) (d (n "rocket-etag-if-none-match") (r "^0.1.0") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.15") (d #t) (k 0)))) (h "1bbchadyf4482ykbs1qpzsf6i99737wd3kmcmqgg2p4wfgicrrrh")))

(define-public crate-rocket-etagged-file-response-0.2.2 (c (n "rocket-etagged-file-response") (v "0.2.2") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)) (d (n "rocket-etag-if-none-match") (r "^0.1.0") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.15") (d #t) (k 0)))) (h "000hzm12wz8wzjkcbdr3plilwqj1ng1xjk2r1h377zvqa6akb492")))

(define-public crate-rocket-etagged-file-response-0.2.3 (c (n "rocket-etagged-file-response") (v "0.2.3") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)) (d (n "rocket-etag-if-none-match") (r "^0.1.0") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.15") (d #t) (k 0)))) (h "105prz0h4040brw3ilss3l40clfz6lp1n5g4yw1ia9i7nq3i2q6p")))

(define-public crate-rocket-etagged-file-response-0.3.0 (c (n "rocket-etagged-file-response") (v "0.3.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)) (d (n "rocket-etag-if-none-match") (r "^0.1.0") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.15") (d #t) (k 0)))) (h "10idqbj2rx8fw7pvrddqp6z2wbky3661b5m9h8k22c6sv7mgpqhl")))

(define-public crate-rocket-etagged-file-response-0.4.0 (c (n "rocket-etagged-file-response") (v "0.4.0") (d (list (d (n "crc-any") (r "^1.0.0") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)) (d (n "rocket-etag-if-none-match") (r "^0.1.0") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.15") (d #t) (k 0)))) (h "08f3shgyjjz7rrwsassn0k34wr73xl19v9wwxp42l32hbzfccshh")))

(define-public crate-rocket-etagged-file-response-0.4.1 (c (n "rocket-etagged-file-response") (v "0.4.1") (d (list (d (n "crc-any") (r "^1.0.0") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.16") (d #t) (k 0)) (d (n "rocket-etag-if-none-match") (r "^0.1.0") (d #t) (k 0)))) (h "014jpdb2d5nzxi9mcck2h9x8wmvh436amg571bdqc4rgymalj2vs")))

(define-public crate-rocket-etagged-file-response-0.5.0 (c (n "rocket-etagged-file-response") (v "0.5.0") (d (list (d (n "crc-any") (r "^1.0.0") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.16") (d #t) (k 0)) (d (n "rocket-etag-if-none-match") (r "^0.1.0") (d #t) (k 0)))) (h "13n9m2r65hpikn154wmjqppgfqdmll0wnv50v60x43bbw0k9vi0z")))

