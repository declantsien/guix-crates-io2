(define-module (crates-io ro ck rocket_contrib_codegen) #:use-module (crates-io))

(define-public crate-rocket_contrib_codegen-0.4.0-rc (c (n "rocket_contrib_codegen") (v "0.4.0-rc") (d (list (d (n "devise") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "0vlw8bgna617g00a8cdswsjfjj552ph8pqk9nqlmhd229zzda8sv") (f (quote (("database_attribute")))) (y #t)))

(define-public crate-rocket_contrib_codegen-0.4.0-rc.1 (c (n "rocket_contrib_codegen") (v "0.4.0-rc.1") (d (list (d (n "devise") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "16l9ddgf1bl17d3vrw9whl7jql8rrb39vasacbb9bml48ywiav1g") (f (quote (("database_attribute"))))))

(define-public crate-rocket_contrib_codegen-0.4.0-rc.2 (c (n "rocket_contrib_codegen") (v "0.4.0-rc.2") (d (list (d (n "devise") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "1k5w4vpp05vmqf0bi3c8i4zgcyn8p4c3hbp0ym76wxjizkbdkpl2") (f (quote (("database_attribute"))))))

(define-public crate-rocket_contrib_codegen-0.4.0 (c (n "rocket_contrib_codegen") (v "0.4.0") (d (list (d (n "devise") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "16pcxqd3fbalq7zhgl4vr7vhis1lzdpl8sa430ik7ijli3vhj38d") (f (quote (("database_attribute"))))))

(define-public crate-rocket_contrib_codegen-0.4.1 (c (n "rocket_contrib_codegen") (v "0.4.1") (d (list (d (n "devise") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "0anfrrpsgc7rzfxlgmwap4n6h456s1sd0r7lbmqmr2cys8ky4npi") (f (quote (("database_attribute"))))))

(define-public crate-rocket_contrib_codegen-0.4.2 (c (n "rocket_contrib_codegen") (v "0.4.2") (d (list (d (n "devise") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "14jh0msifcd7vnkig04lyjks7rr3r2pyzmig4h0xkky39xlscg9l") (f (quote (("database_attribute"))))))

(define-public crate-rocket_contrib_codegen-0.4.3 (c (n "rocket_contrib_codegen") (v "0.4.3") (d (list (d (n "devise") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "19wly7jlyq570967y7gn17y4nnkncsbmx6ppbqmpy5wbfab43mcv") (f (quote (("database_attribute"))))))

(define-public crate-rocket_contrib_codegen-0.4.4 (c (n "rocket_contrib_codegen") (v "0.4.4") (d (list (d (n "devise") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "039dq63zw3lyhaxls9d036wkycm668ncwj0khgbcslz11vhyddca") (f (quote (("database_attribute"))))))

(define-public crate-rocket_contrib_codegen-0.4.5 (c (n "rocket_contrib_codegen") (v "0.4.5") (d (list (d (n "devise") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "0164gb6z0gfbcz0k0nm40v4lg2ss4jy3rzdqjx81zy0ajcyckhgv") (f (quote (("database_attribute"))))))

(define-public crate-rocket_contrib_codegen-0.4.6 (c (n "rocket_contrib_codegen") (v "0.4.6") (d (list (d (n "devise") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "0zfxj12y52km72amcidc17x9fcr7dr0wa750xsk7p3694c3vac7x") (f (quote (("database_attribute"))))))

(define-public crate-rocket_contrib_codegen-0.4.7 (c (n "rocket_contrib_codegen") (v "0.4.7") (d (list (d (n "devise") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "0pav4vkzppxy4yzk77szfiig9q0krwskjalaag1zm49vqpgbdpih") (f (quote (("database_attribute"))))))

(define-public crate-rocket_contrib_codegen-0.4.8 (c (n "rocket_contrib_codegen") (v "0.4.8") (d (list (d (n "devise") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "13dyawrncfziy65dqzsygsv9gmkglfdq2kq645msipnys3rs11wl") (f (quote (("database_attribute"))))))

(define-public crate-rocket_contrib_codegen-0.4.9 (c (n "rocket_contrib_codegen") (v "0.4.9") (d (list (d (n "devise") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "0h389495zsb6b9pzfcn3ip1kyxylm7d962rnhb7irbhrs1k1lk8v") (f (quote (("database_attribute"))))))

(define-public crate-rocket_contrib_codegen-0.4.10 (c (n "rocket_contrib_codegen") (v "0.4.10") (d (list (d (n "devise") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "0azyvmzvwchbwlz7g2j9c5rk27wxikzq4xpprl5arcq9dk5wpwm0") (f (quote (("database_attribute"))))))

(define-public crate-rocket_contrib_codegen-0.4.11 (c (n "rocket_contrib_codegen") (v "0.4.11") (d (list (d (n "devise") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "1bhn52yflcff2vkwp251lpqlw2cgz5qspx5h0mpbimymd7sfmdx7") (f (quote (("database_attribute"))))))

