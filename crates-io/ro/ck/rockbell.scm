(define-module (crates-io ro ck rockbell) #:use-module (crates-io))

(define-public crate-rockbell-0.0.1 (c (n "rockbell") (v "0.0.1") (h "1wj6xhi0llzv5dhv1f833a47f45nadl5b1ki9821hqjbbvn9kpvb") (y #t)))

(define-public crate-rockbell-0.0.2 (c (n "rockbell") (v "0.0.2") (h "1n5jmkiqry4sdbiq6472241n1izysd9nkyp6civlja6h1fjpm4cq")))

(define-public crate-rockbell-0.0.3 (c (n "rockbell") (v "0.0.3") (h "0xiir2140y775v62a1k5i65m47ix88n7n9nna2s4i9yzpb4khw2a")))

