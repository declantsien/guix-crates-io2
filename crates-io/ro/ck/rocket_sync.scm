(define-module (crates-io ro ck rocket_sync) #:use-module (crates-io))

(define-public crate-rocket_sync-0.1.0 (c (n "rocket_sync") (v "0.1.0") (d (list (d (n "smallvec") (r "^0.4.2") (d #t) (k 0)))) (h "05mdg7l0q9fzgmv5ibs1iqk99zlr2gvqn0jm2s7kwib2wbcx4q2m")))

(define-public crate-rocket_sync-0.1.1 (c (n "rocket_sync") (v "0.1.1") (d (list (d (n "smallvec") (r "^0.4.2") (d #t) (k 0)))) (h "1b2ip1cqwkjfgg7d9zkzib49jbgmlw84pyjjm0h0a8bqn03w83ci")))

