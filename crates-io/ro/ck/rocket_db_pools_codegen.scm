(define-module (crates-io ro ck rocket_db_pools_codegen) #:use-module (crates-io))

(define-public crate-rocket_db_pools_codegen-0.1.0-rc.2 (c (n "rocket_db_pools_codegen") (v "0.1.0-rc.2") (d (list (d (n "devise") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "0v1k47zbl9vldrfvcg3ia91hzlwi9vbh196clmx85m5igfrzka0a") (r "1.56")))

(define-public crate-rocket_db_pools_codegen-0.1.0-rc.3 (c (n "rocket_db_pools_codegen") (v "0.1.0-rc.3") (d (list (d (n "devise") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "0xksczyzhsym4cqchdqbzwlmaigx0mki5rh9j82gwky52yszavz2") (r "1.56")))

(define-public crate-rocket_db_pools_codegen-0.1.0-rc.4 (c (n "rocket_db_pools_codegen") (v "0.1.0-rc.4") (d (list (d (n "devise") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "1vrhsz4gz3r4jg945ni1rmmsc42nn3rlyggw76nsqjl5ga25jr15") (r "1.56")))

(define-public crate-rocket_db_pools_codegen-0.1.0 (c (n "rocket_db_pools_codegen") (v "0.1.0") (d (list (d (n "devise") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "15gf3ff5m639n9c1395q56lfz28w5859d4dhrhf3vgyhxrcnyjhq") (r "1.56")))

(define-public crate-rocket_db_pools_codegen-0.2.0 (c (n "rocket_db_pools_codegen") (v "0.2.0") (d (list (d (n "devise") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "1gffasy9z8byvacsybg64s3z2hzvq1b5gql11zy3x8l75sgqabl4") (r "1.64")))

