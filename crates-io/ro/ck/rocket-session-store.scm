(define-module (crates-io ro ck rocket-session-store) #:use-module (crates-io))

(define-public crate-rocket-session-store-0.1.0 (c (n "rocket-session-store") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.76") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gxm8yi59xj2f3hz79rswy9qd59dvbxvy1xvanqmsdvf3nnkm8vb")))

(define-public crate-rocket-session-store-0.2.0 (c (n "rocket-session-store") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.76") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "06hkqwvqx4b8d445nv8xxrvpm4nnnn3kgzqnm8gb5lpqklf3kn0q")))

