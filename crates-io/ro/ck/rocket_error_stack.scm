(define-module (crates-io ro ck rocket_error_stack) #:use-module (crates-io))

(define-public crate-rocket_error_stack-0.1.0 (c (n "rocket_error_stack") (v "0.1.0") (d (list (d (n "error-stack") (r "^0.2") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "0la9jn6n7dmwhmdlgfrd07jczxjn4k1y2zl1fnccp2s6la0mj0k8")))

(define-public crate-rocket_error_stack-0.1.1 (c (n "rocket_error_stack") (v "0.1.1") (d (list (d (n "error-stack") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "0c38i07ssciv4m1g8awfzyv6mdq6nnq89k11dddbv0bi82mk4zyy")))

