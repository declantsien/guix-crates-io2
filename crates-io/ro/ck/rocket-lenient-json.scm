(define-module (crates-io ro ck rocket-lenient-json) #:use-module (crates-io))

(define-public crate-rocket-lenient-json-0.1.0 (c (n "rocket-lenient-json") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1whh6xrib90c3dmkh7nqdph2wpk9644m9j7428gw984973wv6bs5")))

(define-public crate-rocket-lenient-json-0.1.1 (c (n "rocket-lenient-json") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05d4wzczw4fikix433zkn0avcabb4xsky26dv4xjp83gr9kbajfy")))

(define-public crate-rocket-lenient-json-0.1.2 (c (n "rocket-lenient-json") (v "0.1.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17ma1xj3gpk7f2172k33gjlz9kvch8d5rqwq3l4y38w98vgxqshx")))

(define-public crate-rocket-lenient-json-0.1.3 (c (n "rocket-lenient-json") (v "0.1.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nsdbfkfm4fq6ng15pi608wfk0d47dnzq1n42gns6byzs5mkpxg6")))

(define-public crate-rocket-lenient-json-0.1.4 (c (n "rocket-lenient-json") (v "0.1.4") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lxs2wzyj725m35j73kmbdy0sd8fdw7ywmvl3g6p1r3p3r4lnwq2")))

(define-public crate-rocket-lenient-json-0.3.0 (c (n "rocket-lenient-json") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1acl0rgb579b75s3zq88v0mznri4sdz44nz0qh2njafssyxpf8n1")))

(define-public crate-rocket-lenient-json-0.3.1 (c (n "rocket-lenient-json") (v "0.3.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g08rm56pkv9arrjgyni121xqihqs553fwlrb3mvwr2zhbpk9x6v")))

