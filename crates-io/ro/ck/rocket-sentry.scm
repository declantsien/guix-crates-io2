(define-module (crates-io ro ck rocket-sentry) #:use-module (crates-io))

(define-public crate-rocket-sentry-0.1.0 (c (n "rocket-sentry") (v "0.1.0") (d (list (d (n "rocket") (r "^0.4.2") (k 0)) (d (n "sentry") (r "^0.12.0") (d #t) (k 0)))) (h "0vdxz8qk58jgm62xr7p1xkhywhayr4s02fllivgzljllmbnf33v8")))

(define-public crate-rocket-sentry-0.2.0 (c (n "rocket-sentry") (v "0.2.0") (d (list (d (n "rocket") (r "^0.4.2") (k 0)) (d (n "sentry") (r "^0.18.0") (d #t) (k 0)))) (h "1ary9dralafah5bfn2ynsfz9hwprgbi071ryx5lqwmbf89kydpv6")))

(define-public crate-rocket-sentry-0.3.0 (c (n "rocket-sentry") (v "0.3.0") (d (list (d (n "rocket") (r "^0.4.2") (k 0)) (d (n "sentry") (r "^0.19.0") (d #t) (k 0)))) (h "0r49s75x00049dx1rxsry8d8qvzph0r1lpg6wcgfkqsf1fslnx46")))

(define-public crate-rocket-sentry-0.4.0 (c (n "rocket-sentry") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (k 0)) (d (n "sentry") (r "^0.19") (d #t) (k 0)))) (h "1jxmjh5a1y25zi6qxyiwrcnhfinwl321b9v9lkh2crb1zx0mhbip")))

(define-public crate-rocket-sentry-0.5.0 (c (n "rocket-sentry") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (k 0)) (d (n "sentry") (r "^0.20") (d #t) (k 0)))) (h "13cpsipvfnflzpf6dx3zp9v1pqxlya2r6vagbjgmpa4qdlpk9jlk")))

(define-public crate-rocket-sentry-0.6.0 (c (n "rocket-sentry") (v "0.6.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (k 0)) (d (n "sentry") (r "^0.22") (d #t) (k 0)))) (h "09wnvjsdiva24pgf3rapc3igv5hygplb6z11xjwwk1wwkgasa2p7")))

(define-public crate-rocket-sentry-0.7.0 (c (n "rocket-sentry") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (k 0)) (d (n "sentry") (r "^0.23") (d #t) (k 0)))) (h "0yf3np1bswm5s6lg8g7ja2jp27cgyfmibfniwvgq29chgbhj8v67")))

(define-public crate-rocket-sentry-0.8.0 (c (n "rocket-sentry") (v "0.8.0") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (k 0)) (d (n "sentry") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "15zaac8z5mv67fv05zlfv88493rni45768wg1ilprynjmqfn943d")))

(define-public crate-rocket-sentry-0.9.0 (c (n "rocket-sentry") (v "0.9.0") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (k 0)) (d (n "sentry") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (d #t) (k 0)))) (h "0gqs44h13kajcz02vyq99r0yfm3sv96n9zvklbhf2g9ln8wfbk0w")))

(define-public crate-rocket-sentry-0.10.0 (c (n "rocket-sentry") (v "0.10.0") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (k 0)) (d (n "sentry") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "170lqxxkmq1ig7iqiybwbzlwh4zm1n9sq2wp684i9k94bkcpsbja")))

(define-public crate-rocket-sentry-0.11.0 (c (n "rocket-sentry") (v "0.11.0") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (k 0)) (d (n "sentry") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0vsx4ypzz1x30p3sdnyqzhs4js0s72invc4bzbb9ycvwka9cgsqb")))

(define-public crate-rocket-sentry-0.12.0 (c (n "rocket-sentry") (v "0.12.0") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (k 0)) (d (n "sentry") (r "^0.27.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0j6kbiaqfwq5i8bpdzdrn18r4w6c1qfwa743h26zmvmdma6wab63")))

(define-public crate-rocket-sentry-0.13.0 (c (n "rocket-sentry") (v "0.13.0") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (k 0)) (d (n "sentry") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "1qlca1p7m94dqv13dfi0xk5ajhrsgbb6zbps7qvcfmiypndbqzan")))

(define-public crate-rocket-sentry-0.14.0 (c (n "rocket-sentry") (v "0.14.0") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (k 0)) (d (n "sentry") (r "^0.29.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "1nm773q1jpdyhpam1ns0csk7d1bf8djg1d51x24flaaj8n6inh54")))

(define-public crate-rocket-sentry-0.15.0 (c (n "rocket-sentry") (v "0.15.0") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (k 0)) (d (n "sentry") (r "^0.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "018c8fzdg578597fdl1c6ii5i600dzqkahqdcxrgr7d1b7ry8d5w") (r "1.64.0")))

(define-public crate-rocket-sentry-0.16.0 (c (n "rocket-sentry") (v "0.16.0") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (k 0)) (d (n "sentry") (r "^0.31.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0gsp14wmlhb3y2w5nssdb49c069xpi0dcq5379gqpvcphnmx432b") (r "1.64.0")))

(define-public crate-rocket-sentry-0.17.0 (c (n "rocket-sentry") (v "0.17.0") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (k 0)) (d (n "sentry") (r "^0.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "1mavsz8jij1grx963m7ggbpppzsydrbv0q6i4n49dkfpmsfscx69") (r "1.64.0")))

