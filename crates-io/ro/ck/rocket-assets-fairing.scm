(define-module (crates-io ro ck rocket-assets-fairing) #:use-module (crates-io))

(define-public crate-rocket-assets-fairing-0.1.0 (c (n "rocket-assets-fairing") (v "0.1.0") (d (list (d (n "normpath") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "140nwazzjsskw9kfbv0z6rwwph8hd6nb82s3x335sgfwgw0scr6n")))

