(define-module (crates-io ro ck rocket-post-as-delete) #:use-module (crates-io))

(define-public crate-rocket-post-as-delete-0.1.0 (c (n "rocket-post-as-delete") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "0f4gs8z0byivfx1d1sfyvcyczr7c5x5k0r4dmfb1kg56bsh0yblf")))

(define-public crate-rocket-post-as-delete-0.1.1 (c (n "rocket-post-as-delete") (v "0.1.1") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "0wcprsknfb3fma5vdxxpvbfqz00dsbdzl53xiyfjm7kd69bz4gv7")))

