(define-module (crates-io ro ck rocket-errors) #:use-module (crates-io))

(define-public crate-rocket-errors-0.1.0 (c (n "rocket-errors") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "03zwmz0wja96girm43607lw3pd21mlfa0wcxq5f593apnhi15b6m") (f (quote (("default" "anyhow")))) (s 2) (e (quote (("eyre" "dep:eyre") ("anyhow" "dep:anyhow"))))))

