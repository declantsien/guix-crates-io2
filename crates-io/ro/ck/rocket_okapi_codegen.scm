(define-module (crates-io ro ck rocket_okapi_codegen) #:use-module (crates-io))

(define-public crate-rocket_okapi_codegen-0.1.0 (c (n "rocket_okapi_codegen") (v "0.1.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (d #t) (k 0)))) (h "1aldgxyg8mdmhd1f0rfv4rs4c77ll4dkvv0q32aq475q0k8r60mx")))

(define-public crate-rocket_okapi_codegen-0.1.1 (c (n "rocket_okapi_codegen") (v "0.1.1") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (d #t) (k 0)))) (h "0jvll1v98xinym0n8wqkgg7fql1aclq8k0b0f8ggyp1wpirm1ccf")))

(define-public crate-rocket_okapi_codegen-0.1.2 (c (n "rocket_okapi_codegen") (v "0.1.2") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gbpzzg41a3iyxml2j9igfgk2aawdflg737cs9wbn8zw1dvz438c")))

(define-public crate-rocket_okapi_codegen-0.2.0 (c (n "rocket_okapi_codegen") (v "0.2.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17rkls7whcfsxi8gsam3ab3284453nkcc0dljvgnmjdhdxal9jq6")))

(define-public crate-rocket_okapi_codegen-0.3.0 (c (n "rocket_okapi_codegen") (v "0.3.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yq8cqz4fdikqnpb31rvc5sabm8n32v8j9xglnzwa39fv2xqg01q")))

(define-public crate-rocket_okapi_codegen-0.3.1 (c (n "rocket_okapi_codegen") (v "0.3.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "111mlsv09x8b05rc25czq2s0z579xiv8lqnimlf1z17yqxlbrhs9")))

(define-public crate-rocket_okapi_codegen-0.3.2 (c (n "rocket_okapi_codegen") (v "0.3.2") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sq45p8iiq0dam654i5v2gh31lkv8w0svgsb1az9mnck30rzi3ld")))

(define-public crate-rocket_okapi_codegen-0.3.3 (c (n "rocket_okapi_codegen") (v "0.3.3") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "1zjglx9f8jrzd5d064k9pds9z4ycwp5ql7gl8dgbpsjnqjfj8r97")))

(define-public crate-rocket_okapi_codegen-0.3.4 (c (n "rocket_okapi_codegen") (v "0.3.4") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "0lm2lzqnhl9la0brxwcz43ry1dg74zkmk63am2vypffv8z8wsdm0")))

(define-public crate-rocket_okapi_codegen-0.3.5 (c (n "rocket_okapi_codegen") (v "0.3.5") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold"))) (d #t) (k 0)))) (h "0gw4m2jwqshy5sa9v1rca7xpgzd6fwa528l3v4gij5a4n7zrh771")))

(define-public crate-rocket_okapi_codegen-0.3.6 (c (n "rocket_okapi_codegen") (v "0.3.6") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold"))) (d #t) (k 0)))) (h "0ck4b9gkzvi20bqvvmdwbh7ly1pdmmy6y1q3a8zp8fc689qf2bcw")))

(define-public crate-rocket_okapi_codegen-0.4.0 (c (n "rocket_okapi_codegen") (v "0.4.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold"))) (d #t) (k 0)))) (h "1c6c8d6dpkgnv2ilix8hxq7qln2gzy5z67i7n10pya1mps1gz7zm")))

(define-public crate-rocket_okapi_codegen-0.4.1 (c (n "rocket_okapi_codegen") (v "0.4.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold"))) (d #t) (k 0)))) (h "0hcjsr8jmb704a6l0w61mm39k1lzrsiyqj7alplrjhqbs04jkqhv")))

(define-public crate-rocket_okapi_codegen-0.5.0 (c (n "rocket_okapi_codegen") (v "0.5.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j2gp64mg34mc4f22d7prn77kfhr8wvv6n1srihm3bi7vpxmjc6z")))

(define-public crate-rocket_okapi_codegen-0.5.1 (c (n "rocket_okapi_codegen") (v "0.5.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1434v6n6ab6mdc90kli88si817f1dcp76ir54qjn8yh9z8pwjc5n")))

(define-public crate-rocket_okapi_codegen-0.6.0-alpha-1 (c (n "rocket_okapi_codegen") (v "0.6.0-alpha-1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pkyg3b3khh17d0qmcxi3469bafdl28yx94x9fca15i6xdmp26hb")))

(define-public crate-rocket_okapi_codegen-0.7.0-alpha-1 (c (n "rocket_okapi_codegen") (v "0.7.0-alpha-1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hadn5xab8wsmdcxw2zm3ga021xi9sv2m3mhlrhn900w1raz3k48")))

(define-public crate-rocket_okapi_codegen-0.8.0-rc.1 (c (n "rocket_okapi_codegen") (v "0.8.0-rc.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04jnnvfwsx15zwf1h5fwk1xgsiwy8rprcgi3g60vgbr7ziwlf4fw")))

(define-public crate-rocket_okapi_codegen-0.8.0-rc.2 (c (n "rocket_okapi_codegen") (v "0.8.0-rc.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "=0.5.0-rc.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0203dgykqgdkd61mwa25yx6xq16vy5s2cyix8q42wis1zqglvyal")))

(define-public crate-rocket_okapi_codegen-0.8.0-rc.3 (c (n "rocket_okapi_codegen") (v "0.8.0-rc.3") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "=0.5.0-rc.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jjajlxxwj8v2cswr6gc0ii11h9n1a3w23hb4857b23xqpnzhhwc")))

(define-public crate-rocket_okapi_codegen-0.8.0 (c (n "rocket_okapi_codegen") (v "0.8.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "=0.5.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16grscm9lznkxvlsnm098sdnw09006y8r550py07ypcywqa63ffg")))

