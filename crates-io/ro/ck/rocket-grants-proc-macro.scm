(define-module (crates-io ro ck rocket-grants-proc-macro) #:use-module (crates-io))

(define-public crate-rocket-grants-proc-macro-0.5.0-beta.1 (c (n "rocket-grants-proc-macro") (v "0.5.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "17xn9z770wx31wbrqlhf2xibjdis0rfiqi4nfgm2ikwb1vlk6qwg")))

(define-public crate-rocket-grants-proc-macro-0.5.0-rc.2 (c (n "rocket-grants-proc-macro") (v "0.5.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "115krkwp5agxmd7gvrdnbmgdqfymbr5a28qbi2yw6gv155y8kry3")))

(define-public crate-rocket-grants-proc-macro-0.1.0 (c (n "rocket-grants-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0s5p5wyz6mi9yqzvkcliv8ps4bhbksl1v4l3jkiyk48jsd118sdh")))

(define-public crate-rocket-grants-proc-macro-0.1.1 (c (n "rocket-grants-proc-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "rocket-grants") (r "^0.5.0-rc.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1m6hjwil81yckc69i0lyzizq5c2yr9r7yg9dk4r3p535wz7wcaq9")))

