(define-module (crates-io ro ck rocket-sass-fairing) #:use-module (crates-io))

(define-public crate-rocket-sass-fairing-0.1.0-pre1 (c (n "rocket-sass-fairing") (v "0.1.0-pre1") (d (list (d (n "grass") (r "^0.10") (k 0)) (d (n "normpath") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "0gnllmr2qlqxkphdzj9rjixzn34ln57wjrrmhkxdylrkm5c3d094")))

(define-public crate-rocket-sass-fairing-0.1.0 (c (n "rocket-sass-fairing") (v "0.1.0") (d (list (d (n "grass") (r "^0.10") (k 0)) (d (n "normpath") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "1l7fj9dlfp54fyn433vsf9wb7j8akzif7vyvf5wzg2vncfsvfy2r")))

(define-public crate-rocket-sass-fairing-0.1.1 (c (n "rocket-sass-fairing") (v "0.1.1") (d (list (d (n "grass") (r "^0.10") (k 0)) (d (n "normpath") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "0i0kyl04m0xbz1kw6730zn9b3hy1qj91wxzfsr2ighk3nmgry3qs")))

