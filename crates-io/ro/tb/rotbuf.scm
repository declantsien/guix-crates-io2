(define-module (crates-io ro tb rotbuf) #:use-module (crates-io))

(define-public crate-rotbuf-0.0.1 (c (n "rotbuf") (v "0.0.1") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (o #t) (d #t) (k 0)))) (h "15ympas1vqafqfi22cx3d8366xl3hrc4v8cnd7ycgsy620yn0hgs") (f (quote (("default" "DEBUG_TRACING")))) (s 2) (e (quote (("DEBUG_TRACING" "dep:env_logger" "dep:log"))))))

(define-public crate-rotbuf-0.0.2 (c (n "rotbuf") (v "0.0.2") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)))) (h "1z4mnlip8cp6qqd1a4wx4hlkknrgsg9m9j128bzm44i15r4d3x9h") (f (quote (("default" "DEBUG_TRACING") ("DEBUG_TRACING"))))))

(define-public crate-rotbuf-0.0.3 (c (n "rotbuf") (v "0.0.3") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)))) (h "0m5lb4az2n5mc0ixn70wrm51qz4m7hrk1w52r65lyvdr2wijgnbm") (f (quote (("default" "DEBUG_TRACING") ("DEBUG_TRACING"))))))

