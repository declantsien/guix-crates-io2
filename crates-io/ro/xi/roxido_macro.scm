(define-module (crates-io ro xi roxido_macro) #:use-module (crates-io))

(define-public crate-roxido_macro-0.4.1 (c (n "roxido_macro") (v "0.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0c058gscwddgwksr55g67xaa46rk0pyfn8q2w100kim82f40qgas") (y #t)))

(define-public crate-roxido_macro-0.4.2 (c (n "roxido_macro") (v "0.4.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "1ngjl9q07l0wn2iggbazr1w2g4hq4wckai6v0dwa6j159ikipl7h") (y #t)))

(define-public crate-roxido_macro-0.5.0 (c (n "roxido_macro") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qvlj0j1s5hknfvkf2y1mvzibbxhxlmzyn8l03ppjigchxw9z3zy") (y #t)))

