(define-module (crates-io ro cd rocdoc) #:use-module (crates-io))

(define-public crate-rocdoc-0.1.1 (c (n "rocdoc") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0") (d #t) (k 2)))) (h "1vxmzwp7cncyyd5757iy94wlis4ilvgvqkw51yvr62qcjssj3rxm")))

(define-public crate-rocdoc-0.1.2 (c (n "rocdoc") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "test-case") (r "^1.0") (d #t) (k 2)))) (h "0shyjarvpdq56gn2d7bads5i4laa790vp0qyi0djphvf2j7lj51q")))

