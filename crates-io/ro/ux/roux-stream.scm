(define-module (crates-io ro ux roux-stream) #:use-module (crates-io))

(define-public crate-roux-stream-0.1.0 (c (n "roux-stream") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "logtest") (r "^2.0.0") (d #t) (k 2)) (d (n "roux") (r "^1.3.7") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.1") (d #t) (k 2)) (d (n "tokio") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-retry") (r "^0.3.0") (d #t) (k 0)))) (h "1h1v649hh4lw98n3hlh4dgrs22rygfrr8i8dxmayqjz2wl4s72a4")))

(define-public crate-roux-stream-0.2.0 (c (n "roux-stream") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "logtest") (r "^2.0.0") (d #t) (k 2)) (d (n "roux") (r "^2") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-retry") (r "^0.3") (d #t) (k 0)))) (h "0ls7l3z0n8abhk6cp62247x0qzv6ps443fj2dilj0qwfiavnagl9")))

