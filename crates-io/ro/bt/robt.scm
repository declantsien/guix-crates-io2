(define-module (crates-io ro bt robt) #:use-module (crates-io))

(define-public crate-robt-0.1.0 (c (n "robt") (v "0.1.0") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mkit") (r "^0.2.0") (d #t) (k 0)))) (h "1gzy9wwh9pzxxkgyipgrjg2sps7v17x7halif6qh172klpq1ga1q")))

(define-public crate-robt-0.2.0 (c (n "robt") (v "0.2.0") (d (list (d (n "arbitrary") (r "^0.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mkit") (r "^0.4.0") (d #t) (k 0)) (d (n "ppom") (r "^0.6.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "structopt") (r "^0.3.20") (o #t) (k 0)) (d (n "xorfilter-rs") (r "^0.4.0") (d #t) (k 2)))) (h "0adj3vm30ssvccs5by0qndcc8svasgz2x8fna3673frr7ac7yqp6") (f (quote (("robt" "structopt"))))))

