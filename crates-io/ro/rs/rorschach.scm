(define-module (crates-io ro rs rorschach) #:use-module (crates-io))

(define-public crate-rorschach-0.1.0 (c (n "rorschach") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "0w1d8010pw9yafxxwz7ig48bz0kaz3z7165h925c45d37i8mr1fl")))

(define-public crate-rorschach-0.1.1 (c (n "rorschach") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "0077bm4f0ffck6azb9ai4narblqc4is3316fllcx5g5n0ps7ykyj")))

(define-public crate-rorschach-0.1.2 (c (n "rorschach") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "10zr1iqh578vcb4k16bq5r53fzcqzxq8kycng0pw5ibz7684vwcm")))

(define-public crate-rorschach-0.1.3 (c (n "rorschach") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.8") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "154j8vy3hmisdx3wfrp2fvs8r3mnvgjcr7byd12g02vk20hj9wg9")))

