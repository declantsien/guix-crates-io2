(define-module (crates-io ro rs rorshach) #:use-module (crates-io))

(define-public crate-rorshach-0.2.0 (c (n "rorshach") (v "0.2.0") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hotwatch") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pub-sub") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^1.0.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "0xs4cmzrs8aw8brf8p9hvyfdljx7i1b84z0cfdw1qr89n0abhb31")))

