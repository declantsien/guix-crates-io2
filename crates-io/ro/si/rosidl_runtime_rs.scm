(define-module (crates-io ro si rosidl_runtime_rs) #:use-module (crates-io))

(define-public crate-rosidl_runtime_rs-0.2.0 (c (n "rosidl_runtime_rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1g4hvs5bbgn7j643fkn59l7n7mzzfil5nb8xbac8s9w73ssk583x")))

(define-public crate-rosidl_runtime_rs-0.3.0 (c (n "rosidl_runtime_rs") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "00ack21qpxvlgzviz636faskpyg4v1b2q8rx41gr72k31bz04spm")))

(define-public crate-rosidl_runtime_rs-0.3.1 (c (n "rosidl_runtime_rs") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0lrli0yyhz2pxbglynllyj3kjbfldk1qjrsnb9zyhn9x0qfnxsgn")))

(define-public crate-rosidl_runtime_rs-0.4.0 (c (n "rosidl_runtime_rs") (v "0.4.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0j98agh0war0ndm31xkxx323nw07h910p991a7kyj8qy89ma2xjf")))

(define-public crate-rosidl_runtime_rs-0.4.1 (c (n "rosidl_runtime_rs") (v "0.4.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1nspd33dnhjzx8fgxgn806lh7alvlhfklh1ax9db6lxs2gxjhymb") (f (quote (("generate_docs") ("default"))))))

