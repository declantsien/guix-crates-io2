(define-module (crates-io ro me rome_console) #:use-module (crates-io))

(define-public crate-rome_console-0.0.1 (c (n "rome_console") (v "0.0.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "rome_markup") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_text_size") (r "^0.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0lsnlwan4qgr3h4xb4da1m179p1vmqv98i3bk64gl59sm5fcx791") (f (quote (("serde_markup" "serde" "schemars"))))))

