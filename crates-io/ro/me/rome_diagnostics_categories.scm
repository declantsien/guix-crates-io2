(define-module (crates-io ro me rome_diagnostics_categories) #:use-module (crates-io))

(define-public crate-rome_diagnostics_categories-0.0.1 (c (n "rome_diagnostics_categories") (v "0.0.1") (d (list (d (n "schemars") (r "^0.8.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 1)))) (h "0nzsq34pv9545a1mw2rwgz4cjz0h6w54kpfwqg8fidhvz9ycv5zx")))

