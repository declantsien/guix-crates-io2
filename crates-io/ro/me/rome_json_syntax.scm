(define-module (crates-io ro me rome_json_syntax) #:use-module (crates-io))

(define-public crate-rome_json_syntax-0.0.0 (c (n "rome_json_syntax") (v "0.0.0") (d (list (d (n "rome_rowan") (r "^0.0.1") (d #t) (k 0)))) (h "1cq63a1c9qp0rq2pg45flmpigrz8172y04827g1d90pjw64l44a8")))

(define-public crate-rome_json_syntax-0.0.1 (c (n "rome_json_syntax") (v "0.0.1") (d (list (d (n "rome_rowan") (r "^0.0.1") (d #t) (k 0)))) (h "0nh84s40d5msb6xgnbby3f6mlsq1abvk8xdhzki0ja998ks7a9id")))

