(define-module (crates-io ro me rome_json_parser) #:use-module (crates-io))

(define-public crate-rome_json_parser-0.0.1 (c (n "rome_json_parser") (v "0.0.1") (d (list (d (n "rome_console") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_diagnostics") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_js_unicode_table") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_json_factory") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_json_syntax") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_parser") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_rowan") (r "^0.0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.31") (f (quote ("std"))) (k 0)) (d (n "insta") (r "^1.21.2") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0f9z8sahqnsracpyyvhf8l28302gvid7d7v8jsibsscqldnj3irj")))

