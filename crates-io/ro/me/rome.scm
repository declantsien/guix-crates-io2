(define-module (crates-io ro me rome) #:use-module (crates-io))

(define-public crate-rome-0.1.0 (c (n "rome") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "nom") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "1slb09m29xf75ly4am0icyzq1j2pdsxmf97xqblpkrhjgcazf4wc") (f (quote (("unstable") ("default"))))))

(define-public crate-rome-0.1.1 (c (n "rome") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "nom") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "05j47d2n58km6ycfj09aahyfgjag6m3hnbblj8sr2m9bbnkprhgp") (f (quote (("unstable") ("default"))))))

(define-public crate-rome-0.1.2 (c (n "rome") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.112") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "nom") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "0gbrfmkfha16zm0xcyrxxh8ns01hvqqzmsmkq72h03wyfkq4ix9y") (f (quote (("unstable") ("default"))))))

(define-public crate-rome-0.1.3 (c (n "rome") (v "0.1.3") (d (list (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "17i34j2a94ldxhn7mibw25slsf7xrz78njvdcp4xqij1ivqqbcgh") (f (quote (("unstable") ("default"))))))

