(define-module (crates-io ro me rome_text_edit) #:use-module (crates-io))

(define-public crate-rome_text_edit-0.0.0 (c (n "rome_text_edit") (v "0.0.0") (h "1kn63qh2ki70ijr0gbl40dlvp8xlmcjl83px1lfdqwv56xhdhvk9")))

(define-public crate-rome_text_edit-0.0.1 (c (n "rome_text_edit") (v "0.0.1") (d (list (d (n "rome_text_size") (r "^0.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (f (quote ("unicode"))) (d #t) (k 0)))) (h "1xx35mfh133n2dir8p3qh7azbm4gqniy4s266wn4sfimz4v0f34r") (s 2) (e (quote (("schemars" "dep:schemars" "rome_text_size/schemars"))))))

