(define-module (crates-io ro me rome_js_syntax) #:use-module (crates-io))

(define-public crate-rome_js_syntax-0.0.1 (c (n "rome_js_syntax") (v "0.0.1") (d (list (d (n "rome_console") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_diagnostics") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_rowan") (r "^0.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09ssr73icrj2i7ln3nskxxgrf550985fxwqa0pn8l1f5ip466lb6") (s 2) (e (quote (("serde" "dep:serde" "schemars" "rome_rowan/serde"))))))

(define-public crate-rome_js_syntax-0.0.2 (c (n "rome_js_syntax") (v "0.0.2") (d (list (d (n "rome_console") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_diagnostics") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_rowan") (r "^0.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13mij80nl6ng1gi1syzarljf755sjfa2wrqy55qyvysfgf4mjsl4") (s 2) (e (quote (("serde" "dep:serde" "schemars" "rome_rowan/serde"))))))

