(define-module (crates-io ro me rome_json_formatter) #:use-module (crates-io))

(define-public crate-rome_json_formatter-0.0.1 (c (n "rome_json_formatter") (v "0.0.1") (d (list (d (n "rome_formatter") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_json_syntax") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_rowan") (r "^0.0.1") (d #t) (k 0)) (d (n "countme") (r "^3.0.1") (f (quote ("enable"))) (d #t) (k 2)) (d (n "insta") (r "^1.21.2") (f (quote ("glob"))) (d #t) (k 2)) (d (n "rome_parser") (r "^0.0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "12dhyxkbxm60ka7z2x8y7i70ml5djjsda59lr6zv0kazbx45dzjx")))

