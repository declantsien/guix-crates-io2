(define-module (crates-io ro me rome_parser) #:use-module (crates-io))

(define-public crate-rome_parser-0.0.0 (c (n "rome_parser") (v "0.0.0") (h "1njhika2lb2d2lp67h525f3l9c1q2c1xwbmxqvrzv3j8zayjdkbd")))

(define-public crate-rome_parser-0.0.1 (c (n "rome_parser") (v "0.0.1") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "rome_console") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_diagnostics") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_rowan") (r "^0.0.1") (d #t) (k 0)))) (h "19fy72dlkpn87i8bcij8zm7ag38zd4a64yzp9g77sv8pv5czw7b4")))

