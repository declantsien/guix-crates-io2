(define-module (crates-io ro me rome_markup) #:use-module (crates-io))

(define-public crate-rome_markup-0.0.1 (c (n "rome_markup") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "1h9sp6w0pq070i44spl5jdid2zk0lml3m1wc6l5rzwjjhjzy6pp5")))

