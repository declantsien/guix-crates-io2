(define-module (crates-io ro me rome_formatter) #:use-module (crates-io))

(define-public crate-rome_formatter-0.0.0 (c (n "rome_formatter") (v "0.0.0") (h "0k78ipkr9j3zp0xdawa5p23yrcwhxr1kbnnyqy67ks5xv4795r90")))

(define-public crate-rome_formatter-0.0.1 (c (n "rome_formatter") (v "0.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "countme") (r "^3.0.1") (d #t) (k 0)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "rome_console") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_diagnostics") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_rowan") (r "^0.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.31") (f (quote ("std"))) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "insta") (r "^1.21.2") (d #t) (k 2)))) (h "00zk8dnnw54v1dqikvjdpnd8sh6jh3z8zxvbqf5fxj15ssvz4vdk") (s 2) (e (quote (("serde" "dep:serde" "schemars" "rome_rowan/serde"))))))

