(define-module (crates-io ro me rome_rowan) #:use-module (crates-io))

(define-public crate-rome_rowan-0.0.0 (c (n "rome_rowan") (v "0.0.0") (h "12g5sbq1d8y0amqlvvjfaps5bjlj6z5y9y4clmqdn6fm50kr2p10")))

(define-public crate-rome_rowan-0.0.1 (c (n "rome_rowan") (v "0.0.1") (d (list (d (n "countme") (r "^3.0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (f (quote ("inline-more"))) (k 0)) (d (n "memoffset") (r "^0.8.0") (d #t) (k 0)) (d (n "rome_text_edit") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_text_size") (r "^0.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (o #t) (k 0)) (d (n "tracing") (r "^0.1.31") (f (quote ("std"))) (k 0)) (d (n "iai") (r "^0.1.1") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0vk88mgh7sjx2lhrqslsksamjzwwysxlwb3ssjy1cjwscrm94g83") (s 2) (e (quote (("serde" "dep:serde" "schemars" "rome_text_size/serde"))))))

