(define-module (crates-io ro me rome_js_factory) #:use-module (crates-io))

(define-public crate-rome_js_factory-0.0.1 (c (n "rome_js_factory") (v "0.0.1") (d (list (d (n "rome_js_syntax") (r "^0.0.1") (d #t) (k 0)) (d (n "rome_rowan") (r "^0.0.1") (d #t) (k 0)))) (h "1h4pxl6g34bs4papfp8a4rs0l9jirmshxsppary6cgdgm70byz7y")))

(define-public crate-rome_js_factory-0.0.2 (c (n "rome_js_factory") (v "0.0.2") (d (list (d (n "rome_js_syntax") (r "^0.0.2") (d #t) (k 0)) (d (n "rome_rowan") (r "^0.0.1") (d #t) (k 0)))) (h "00ps78pnzcfd45xy2vxwdnyryd6xgsixls782ll6iq7ry8rzs2d1")))

