(define-module (crates-io ro me rome_js_semantic) #:use-module (crates-io))

(define-public crate-rome_js_semantic-0.1.0 (c (n "rome_js_semantic") (v "0.1.0") (d (list (d (n "rome_console") (r "^0.0.1") (d #t) (k 2)) (d (n "rome_diagnostics") (r "^0.0.1") (d #t) (k 2)) (d (n "rome_js_syntax") (r "^0.0.2") (d #t) (k 0)) (d (n "rome_markup") (r "^0.0.1") (d #t) (k 2)) (d (n "rome_rowan") (r "^0.0.1") (d #t) (k 0)) (d (n "rust-lapper") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "017j8qpj49x1jhba2h8q53kl2s4c0yvsfllrnyg79v7xzlfchym5")))

