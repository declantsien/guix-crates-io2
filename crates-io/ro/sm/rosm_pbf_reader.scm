(define-module (crates-io ro sm rosm_pbf_reader) #:use-module (crates-io))

(define-public crate-rosm_pbf_reader-1.0.0 (c (n "rosm_pbf_reader") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0.25") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "prost") (r "^0.11.6") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.6") (d #t) (k 1)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 2)))) (h "1hlw48mxqzaqpf1g271lk18n6s8r7h6rm0gix443i4zz9fnf4ypz") (f (quote (("default" "flate2"))))))

(define-public crate-rosm_pbf_reader-1.0.1 (c (n "rosm_pbf_reader") (v "1.0.1") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.28") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "prost") (r "^0.12.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.12.1") (d #t) (k 1)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 2)))) (h "1l1cs3cfliyd6fmxickxnagr82lgwxldcp6jwi67d9x12gc1rm6n") (f (quote (("default" "flate2"))))))

(define-public crate-rosm_pbf_reader-1.0.2 (c (n "rosm_pbf_reader") (v "1.0.2") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.28") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 2)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.12.3") (d #t) (k 1)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 2)))) (h "0w2sq8wrlgfbbja6a4mqykhvc2hyqm1909ys9hai6l335f6qgyil") (f (quote (("default" "flate2"))))))

