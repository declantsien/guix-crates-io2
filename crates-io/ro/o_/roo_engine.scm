(define-module (crates-io ro o_ roo_engine) #:use-module (crates-io))

(define-public crate-roo_engine-0.1.0 (c (n "roo_engine") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "02p872xs862rqd0mfp5wpkrmdmwlr349flk0zfrpgax107ifn0h6")))

