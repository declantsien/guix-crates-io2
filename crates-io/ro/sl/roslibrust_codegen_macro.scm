(define-module (crates-io ro sl roslibrust_codegen_macro) #:use-module (crates-io))

(define-public crate-roslibrust_codegen_macro-0.1.0 (c (n "roslibrust_codegen_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roslibrust_codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d1zhihakan10658dz9rwkalz0lzggnavy9pr1xdynm1imzjypgm")))

(define-public crate-roslibrust_codegen_macro-0.2.0 (c (n "roslibrust_codegen_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roslibrust_codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1844n4b2drqfdbf58jy9xagrhlkn7xbmsaqmka321ny80gs4ags1")))

(define-public crate-roslibrust_codegen_macro-0.5.0 (c (n "roslibrust_codegen_macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roslibrust") (r "^0.5.0") (f (quote ("codegen"))) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0ja7p62njmwr5nklcbfw79s050giba6ykzwy4h9r760n9d99rnbz")))

(define-public crate-roslibrust_codegen_macro-0.5.2 (c (n "roslibrust_codegen_macro") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roslibrust") (r "^0.5.2") (f (quote ("codegen"))) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 0)))) (h "091406aphqbvx11qjg98ana10444i181zxmd5c3718b38k5y5c4j")))

(define-public crate-roslibrust_codegen_macro-0.6.0 (c (n "roslibrust_codegen_macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roslibrust_codegen") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1arnd7g8disjbyh6fplb2xzr22z2lcz7irgrhbayjls7afhg3wjp")))

(define-public crate-roslibrust_codegen_macro-0.7.0 (c (n "roslibrust_codegen_macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roslibrust_codegen") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0w4y6k98h0xv2dd4xjd2qbzsk6wzch9659bwlskr4k12svz84c2m")))

(define-public crate-roslibrust_codegen_macro-0.8.0 (c (n "roslibrust_codegen_macro") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roslibrust_codegen") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wn41bp4dmi4qbk2dvmcy6kwm85657dfqsf5slgndkcfhqr4bklk")))

(define-public crate-roslibrust_codegen_macro-0.9.0 (c (n "roslibrust_codegen_macro") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roslibrust_codegen") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kq7sjmpr9r9glzrc9n77zk6fr3mj6mb9n9bs7fi2r96rmk0r8iq")))

