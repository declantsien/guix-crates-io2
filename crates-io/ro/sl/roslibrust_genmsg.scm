(define-module (crates-io ro sl roslibrust_genmsg) #:use-module (crates-io))

(define-public crate-roslibrust_genmsg-0.9.0 (c (n "roslibrust_genmsg") (v "0.9.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minijinja") (r "^2.0") (d #t) (k 0)) (d (n "roslibrust_codegen") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wbjgn3jyihkhwhgcf7ck0826rmf2x739nw5nwbss2zp7g7l83qk")))

