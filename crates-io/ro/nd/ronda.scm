(define-module (crates-io ro nd ronda) #:use-module (crates-io))

(define-public crate-ronda-0.1.0 (c (n "ronda") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (f (quote ("ron"))) (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "ron-edit") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 2)))) (h "0sak1wlzf6i9a5y0fz64v3zr9a4877cmfl6mmz1440wqav10xdwg")))

(define-public crate-ronda-0.2.0 (c (n "ronda") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (f (quote ("ron"))) (d #t) (k 2)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "ron-edit") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 2)))) (h "0xwl9czyi8w3jh1q36kgy050qry8vq4p6bk1lhs2bkndpnridi6z")))

