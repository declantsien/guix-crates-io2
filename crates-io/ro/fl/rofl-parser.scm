(define-module (crates-io ro fl rofl-parser) #:use-module (crates-io))

(define-public crate-rofl-parser-0.1.0 (c (n "rofl-parser") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0spr1rx5iwyk8rf6xjxangn3nj3sr10wrpnh1rvbqaa061z08c13")))

(define-public crate-rofl-parser-0.1.1 (c (n "rofl-parser") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "14dbp1iihyzcwr9h4fdww02h0zr8gxxprabfrhyp8qfrybj26isf")))

(define-public crate-rofl-parser-0.1.2 (c (n "rofl-parser") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1k3rx6d1r5sd97zdpqap7hbp7bvw17rwfgcwrr6dx10wj7wpkxjr")))

(define-public crate-rofl-parser-0.1.21 (c (n "rofl-parser") (v "0.1.21") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01m6z56s0yq0p5h70b03dwnf2y8wxixh671zzr3p5i28l4svjfhd")))

(define-public crate-rofl-parser-0.1.22 (c (n "rofl-parser") (v "0.1.22") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1rms3xxqmbsl6pf8c9mda5kfg562a8pxnk5vvl6ymx62a611mybc")))

(define-public crate-rofl-parser-0.1.23 (c (n "rofl-parser") (v "0.1.23") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18vli22gwfk6y5hxwjrhqivj6vfi40lnmskw49fvq6chib3skzx4")))

(define-public crate-rofl-parser-0.1.24 (c (n "rofl-parser") (v "0.1.24") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1i1i89ixhg2i0ycadr7ihz53b5dq14jvh4qpnd9rx26cwc263p9h")))

(define-public crate-rofl-parser-0.1.25 (c (n "rofl-parser") (v "0.1.25") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06x7xjnns4qwvz8j7p7zzqj57cf075bq07ya1s1qpn877i55cbhv")))

(define-public crate-rofl-parser-0.1.26 (c (n "rofl-parser") (v "0.1.26") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1hds7glmywv79ab42r9n0zib6nww1wf6vv0vpxkv6q3q2brvf7sn")))

