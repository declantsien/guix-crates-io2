(define-module (crates-io ro un round_float) #:use-module (crates-io))

(define-public crate-round_float-0.0.1 (c (n "round_float") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "11559k7r0ddwzx7fdf1fj40r9p5rdl6mrkrmx4h6ip377a0zjd60") (y #t) (r "1.75.0")))

(define-public crate-round_float-0.0.2 (c (n "round_float") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "143i1n0b74xlkdrwvk8pb247k4zxk1flfivhd4c7ksvgjq1rvn27") (r "1.75.0")))

(define-public crate-round_float-0.0.3 (c (n "round_float") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "06kpac7hksvr7c7k9zy1n3vgv8q0a9j0qa11vhsq420kyl4ppc68") (r "1.75.0")))

(define-public crate-round_float-0.0.4 (c (n "round_float") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "18vsx6l2p9a02ay4zdlklsyprdgw4gad25s4xwvdvmdnx8prjqhg") (r "1.75.0")))

(define-public crate-round_float-0.0.5 (c (n "round_float") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "1mky4rbip1wd01f8zsyy1azbc6bp3rs0d99idlkggcpvnyfawkjn") (r "1.75.0")))

(define-public crate-round_float-0.0.6 (c (n "round_float") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0swhdjaq57kpgwzydjp8pdmfpsddx5qly8ffzp33dd1an1wmg3r1") (r "1.75.0")))

(define-public crate-round_float-0.0.7 (c (n "round_float") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0z5cs77gfwb33rdkfdkbr0xw73dv17jna0x6bjayhps3b50qfvqq") (r "1.75.0")))

(define-public crate-round_float-0.0.8 (c (n "round_float") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "159yyc4v58hyqyn2k7fca59r4ifsmgr6504v4dh3j3kka2d10lkc") (r "1.75.0")))

(define-public crate-round_float-0.0.9 (c (n "round_float") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "09rr22h561c3viph0y1ys79mv8pmkqi2y1izrvgxggvk4yqdjh4y") (r "1.75.0")))

(define-public crate-round_float-0.0.10 (c (n "round_float") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "18zi997kzz6s8mr71di2rdn33312adf0rqaqr099ycsv487d1fr6") (r "1.75.0")))

(define-public crate-round_float-0.0.12 (c (n "round_float") (v "0.0.12") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "18fd733h6dmrrxmqspmi1bajfqlpyk46gvf3x308p0w5jn5wrb1q") (r "1.75.0")))

(define-public crate-round_float-1.0.0 (c (n "round_float") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "07nccdfxpng7745b2wfasnh83ag7fny0k4w8iw02ay1gng5svvhj") (r "1.75.0")))

(define-public crate-round_float-1.0.1 (c (n "round_float") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "16jg7pya2r5xpbashxqhnkrs3xifggcrrbkrdf3fgq6hla2li1aq") (r "1.75.0")))

