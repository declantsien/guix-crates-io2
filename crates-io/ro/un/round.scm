(define-module (crates-io ro un round) #:use-module (crates-io))

(define-public crate-round-0.0.0 (c (n "round") (v "0.0.0") (h "0fsqv7zksmnrmynkiy6h8snmj2gys1mkb0iy9kg9igjjligsfy5s")))

(define-public crate-round-0.1.0 (c (n "round") (v "0.1.0") (h "1v3f19cryp965namn3mzrbm3vq2bl1c0xzh4v43n8f22kkd46m6s")))

(define-public crate-round-0.1.2 (c (n "round") (v "0.1.2") (h "0mxncgfzjl15m192q4ay99n4wmzsgwxq1z34whvswwm89zdha9xh")))

