(define-module (crates-io ro un round_to_int) #:use-module (crates-io))

(define-public crate-round_to_int-0.1.0 (c (n "round_to_int") (v "0.1.0") (h "0j319pg2kqjvnl357brpdwyj1by9z6y1imh5arb82rmq2swd83c9") (y #t)))

(define-public crate-round_to_int-0.1.1 (c (n "round_to_int") (v "0.1.1") (h "1j12j4bpq95cmv87z7svk0zi0zh1qzzhq4nzgvlyv3f3cw2ysnis") (y #t)))

(define-public crate-round_to_int-0.1.2 (c (n "round_to_int") (v "0.1.2") (h "1dm3firwf75d2g3rp8f0vkn6pgb408gmlb6yhlj3qdzqni5q7gkm") (y #t)))

(define-public crate-round_to_int-0.1.3 (c (n "round_to_int") (v "0.1.3") (h "1cmhv9jbj3zi0qqvsgkppks9rr45nkqacq7mjh4kigr5zr7pyzdd") (y #t)))

