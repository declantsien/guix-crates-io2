(define-module (crates-io ro un roundabout) #:use-module (crates-io))

(define-public crate-roundabout-0.1.0 (c (n "roundabout") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "174b4qvsjxx4bca658kgag8346hqgs7k0vwj34llxgjjbxfq84iy")))

(define-public crate-roundabout-0.2.0 (c (n "roundabout") (v "0.2.0") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 2)) (d (n "indexmap") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1rh6q32syvgpgn1fkaagxgqbd3zb578jb761d10ab5kf3i2bkpm5")))

