(define-module (crates-io ro un round_ties_to_positive_infinity) #:use-module (crates-io))

(define-public crate-round_ties_to_positive_infinity-0.1.0 (c (n "round_ties_to_positive_infinity") (v "0.1.0") (h "1r5c1a4sbv8ab6p23y9cbi345zmbjiy02xagaqkwvbv586cahksl") (y #t)))

(define-public crate-round_ties_to_positive_infinity-0.1.1 (c (n "round_ties_to_positive_infinity") (v "0.1.1") (h "1nxdqhfd5d6nv5wig6k4gapd3gsxrg4lwabyz9lgww13vjmfcca2")))

