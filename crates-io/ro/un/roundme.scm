(define-module (crates-io ro un roundme) #:use-module (crates-io))

(define-public crate-roundme-0.1.0 (c (n "roundme") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cargo-audit") (r "^0.18.3") (d #t) (k 2)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer" "unicode"))) (d #t) (k 0)) (d (n "latex") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1nivzv0p8hhm487l95hs1bf8mhfs5g8xd3v0fdgg00dhdznvrj9a")))

