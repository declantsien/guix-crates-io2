(define-module (crates-io ro un round_robin_tournament) #:use-module (crates-io))

(define-public crate-round_robin_tournament-0.1.0 (c (n "round_robin_tournament") (v "0.1.0") (h "06isq4nhls8zi0xp1jcr9hc6ck98rmqfx187qpfr9z0cd4cx0pvk")))

(define-public crate-round_robin_tournament-0.2.0 (c (n "round_robin_tournament") (v "0.2.0") (h "0d59rc22k3zkxwcw5aqama9l1gw8wr0sxf16nr9a67vc47s51nbp")))

(define-public crate-round_robin_tournament-0.3.0 (c (n "round_robin_tournament") (v "0.3.0") (h "07m3fdy5m7awjkkwiqpwvcwfnk63gnfm19f1r3sb7m3ph2680mb0")))

(define-public crate-round_robin_tournament-0.3.1 (c (n "round_robin_tournament") (v "0.3.1") (h "1kv9wsrhvhg2qazmc5v6d4z809afclc37cn90pxb7n0w0h0jvdjc")))

