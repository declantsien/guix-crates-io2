(define-module (crates-io ro un round-based-derive) #:use-module (crates-io))

(define-public crate-round-based-derive-0.2.0 (c (n "round-based-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1g9icf5qxlchjbyfnc665jl82vx9j2x3pr16d4l3pjyv9wibz5q3")))

(define-public crate-round-based-derive-0.2.1 (c (n "round-based-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0bm3bllp3fhmk5wkmarka9kq81a7xkj6qlcg8j511avvn47j4gqw")))

