(define-module (crates-io ro un roundrobin) #:use-module (crates-io))

(define-public crate-roundrobin-0.1.0 (c (n "roundrobin") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1m5axf0kbr3bqp9dqdfpvwri3adrx8w5ypk60ww4qdi2aiim8g9s")))

(define-public crate-roundrobin-0.1.1 (c (n "roundrobin") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0sgd9vr7w7gmxxbni6m1759f7gk8xl4gifaf9ykipsmlm4xn7ada")))

