(define-module (crates-io ro un round5) #:use-module (crates-io))

(define-public crate-round5-0.1.0 (c (n "round5") (v "0.1.0") (d (list (d (n "array-init") (r "^0.1.1") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)))) (h "15vp5079z5fz8jylfvd4xa78w05abnnngnma46hcshhc089p29l9")))

(define-public crate-round5-0.1.1 (c (n "round5") (v "0.1.1") (d (list (d (n "array-init") (r "^0.1.1") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "10kl3csww0vj9gf3fhrfm9wyyiyscsibbwxgmyg39s0s2i6w7xsd") (f (quote (("pke") ("kem"))))))

(define-public crate-round5-0.1.2 (c (n "round5") (v "0.1.2") (d (list (d (n "array-init") (r "^0.1.1") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0wa0hbfqyw85fjq7va0sm5y87hyzbpcq4qk7yk7q8hkz55ib634z") (f (quote (("support-snow") ("pke") ("kem"))))))

