(define-module (crates-io ro un round_view_derive) #:use-module (crates-io))

(define-public crate-round_view_derive-0.1.0 (c (n "round_view_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "05zn6jr2x2snh5cj63xgwh23rkwn4nz8wjslb5hsklbzr2gml16c")))

(define-public crate-round_view_derive-0.2.0 (c (n "round_view_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0yqdfqf4kpxlwnp5j0p3zanzrhbnvynk6q9nzdpqfisc5767jxj0")))

(define-public crate-round_view_derive-0.3.0 (c (n "round_view_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0wd7mj28yj5s3vh9y1v62j5zc572j4x422b1i0avpaqj6l8hy9g4")))

(define-public crate-round_view_derive-0.4.0 (c (n "round_view_derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)))) (h "1m2xf6sxqn2wibsy5zg6i8li080a1qfycmvznxcvk6gkak6151r7")))

(define-public crate-round_view_derive-0.5.0 (c (n "round_view_derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)))) (h "18czppszhbdhhddvx7vxa5jss5vl2bj1g42vj251ylx6hf8y5wf4")))

