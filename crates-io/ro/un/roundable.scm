(define-module (crates-io ro un roundable) #:use-module (crates-io))

(define-public crate-roundable-0.1.0 (c (n "roundable") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3.7") (d #t) (k 2)))) (h "143hy0x48v12pfv0xs5nsmfaa9qv6vzk7ziz40h74f0hj3w7wwv0") (r "1.56.1")))

(define-public crate-roundable-0.1.1 (c (n "roundable") (v "0.1.1") (d (list (d (n "assert2") (r "^0.3.7") (d #t) (k 2)))) (h "0xdbhhgcz6c8msm9a17j1fgs3njpcld0l8yp3pn4f6zr65248knn") (r "1.56.1")))

(define-public crate-roundable-0.2.0 (c (n "roundable") (v "0.2.0") (d (list (d (n "assert2") (r "^0.3.7") (d #t) (k 2)))) (h "0sa3z799ykpm0syxnavn00d5fmfhsxqkmrhizv3ykhmn17l94k03") (r "1.56.1")))

