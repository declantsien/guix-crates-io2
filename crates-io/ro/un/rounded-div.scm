(define-module (crates-io ro un rounded-div) #:use-module (crates-io))

(define-public crate-rounded-div-0.0.0 (c (n "rounded-div") (v "0.0.0") (h "0m02ykk11c841ycjn9arf08m5zragvg6vympy5w4sszjxrdjgaq0")))

(define-public crate-rounded-div-0.1.0 (c (n "rounded-div") (v "0.1.0") (h "0b1r7r5s2gixh4vyd3v3cmx8f6d6138khszisa6a4bv1if4xqmji")))

(define-public crate-rounded-div-0.1.1 (c (n "rounded-div") (v "0.1.1") (h "0qp6482031g5bczdx9gl1qjvgcpxvjckx6lgxrmmygcykydxjm32")))

(define-public crate-rounded-div-0.1.2 (c (n "rounded-div") (v "0.1.2") (h "0bsy7xa4zmji2jl839qag3as0b9clqdbysmgdcra1mi6l6q8yk26")))

