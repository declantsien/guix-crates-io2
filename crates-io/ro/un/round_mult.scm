(define-module (crates-io ro un round_mult) #:use-module (crates-io))

(define-public crate-round_mult-0.1.0 (c (n "round_mult") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (o #t) (k 0)))) (h "0h206c5fd97wyyzp5bfaxy6jjlrshr0vj89hzapmzn269kf85if2") (y #t)))

(define-public crate-round_mult-0.1.1 (c (n "round_mult") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "10b8x5iwvi6mbdlrcfzbnc5v7q9584pzkg42l1ingwhrhzgzmzqn")))

(define-public crate-round_mult-0.1.2 (c (n "round_mult") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "01rzdpz5l805kwcwwdpf4pwj56rd2wdpa3pw43xv4r63cb1hcsw5")))

(define-public crate-round_mult-0.1.3 (c (n "round_mult") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "1vjxcmilgcnym5q2kh1bss60dapnmiv3zsbam84nzly4hr97vg3l")))

