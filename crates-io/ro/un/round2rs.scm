(define-module (crates-io ro un round2rs) #:use-module (crates-io))

(define-public crate-round2rs-0.1.0 (c (n "round2rs") (v "0.1.0") (h "1gfks7r4zigy6aaw55jy4b1xyqxn8vhfs2kd58y88996s7a5pz4x")))

(define-public crate-round2rs-0.1.1 (c (n "round2rs") (v "0.1.1") (h "17ii62v1b7g7lai7x1y8i09arc2xdr8slwyk3sf94yyy7k36d6cf")))

(define-public crate-round2rs-0.1.3 (c (n "round2rs") (v "0.1.3") (h "0v05snhhcgy2scl57w1sscl40b7qr8cl4fjs469j8271vybsrrdq")))

