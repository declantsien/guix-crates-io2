(define-module (crates-io ro ha rohanasan) #:use-module (crates-io))

(define-public crate-rohanasan-0.1.2 (c (n "rohanasan") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "0yc69x5hgjfqkap5f60szhjsqx3674w23gfz76z50rkb6dvyfmmk")))

(define-public crate-rohanasan-0.1.1 (c (n "rohanasan") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1ll3yif9srf6d02hrqh2l14phqxji9fki9pjdfc0g335vkd6r7bs")))

(define-public crate-rohanasan-0.1.4 (c (n "rohanasan") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "0nmwv77fvcvns6fwscrb4i6gzwil0smfny16zn683lg93yclas6c")))

(define-public crate-rohanasan-0.1.5 (c (n "rohanasan") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1n73vjrhjpd7sq2gb720aw984rlxb9zd9id5p7g9zaj20kcfkd0f")))

(define-public crate-rohanasan-0.2.0 (c (n "rohanasan") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "0jfn119n6y5nsvq1bhk0hkkgd0dv7s0d8rycc49fzdqrj7sxhpyy")))

(define-public crate-rohanasan-0.2.1 (c (n "rohanasan") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1rg59n29p32pjagxgzrfxbgp8d8iqlfs65zrwk0amshfc45rnv6i")))

(define-public crate-rohanasan-0.2.5 (c (n "rohanasan") (v "0.2.5") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1a4bap26j8jrppjrsps62lc9fmhldfsfd43c179nab2kwi40jffr")))

(define-public crate-rohanasan-0.2.9 (c (n "rohanasan") (v "0.2.9") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "192q3yjd1rr3iyz2qpl7ndb477hsivib7vrd0llz05cssf1pj510") (y #t)))

(define-public crate-rohanasan-0.4.0 (c (n "rohanasan") (v "0.4.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "0kj73khhklbba07w5k31r5yi910767k8v4xyvim1krgn0mcqzgfj") (y #t)))

(define-public crate-rohanasan-0.4.1 (c (n "rohanasan") (v "0.4.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "07mi7vzgzbasyc93mbzhry2hkna0jpjvrw415yg2r9i7ikd295c8") (y #t)))

(define-public crate-rohanasan-0.4.2 (c (n "rohanasan") (v "0.4.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "1xh439anxfrixws06jrrdc9my3rdw4nl4xm8pcy0figfqydp2a61") (y #t)))

(define-public crate-rohanasan-0.4.3 (c (n "rohanasan") (v "0.4.3") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "04kharp7mxgfg5my6kjylg55yzwp1n5nwsplm2r6x4l8zqh387sr") (y #t)))

(define-public crate-rohanasan-0.4.4 (c (n "rohanasan") (v "0.4.4") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "0ba3aw70w2ja16wvpxhdiw15mxh3n807kc0b38rh2jljrwl15z1m")))

(define-public crate-rohanasan-0.4.49 (c (n "rohanasan") (v "0.4.49") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "0q3f9l59iq3x3l33bm5spy0kk5xh93rxa26zn79z7kay57alv47j")))

(define-public crate-rohanasan-0.4.54 (c (n "rohanasan") (v "0.4.54") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "1s518d52whv1xf23s80k3wprh7x1j5h9kcsy7wvniyndvr8a4fh9")))

(define-public crate-rohanasan-0.4.55 (c (n "rohanasan") (v "0.4.55") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "1inwj0a8v2ipppl96qv6lv1caqxl8waxs7k18hpgysn06f9j47g6")))

(define-public crate-rohanasan-0.4.56 (c (n "rohanasan") (v "0.4.56") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "0sxw0dmnkqrm7dv5hyzl09ydwvggkx5y5bddl35rw4pcrldlyjzp")))

(define-public crate-rohanasan-0.4.57 (c (n "rohanasan") (v "0.4.57") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "187mnfw9bfhyv9d0jszhhbbhi767j00485p3wh4d4g24w3gr3rpv")))

(define-public crate-rohanasan-0.4.60 (c (n "rohanasan") (v "0.4.60") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "1iznpixygn50p945aa9adplzif1m0m2ab8px51vs6czfxdb4jwdh")))

(define-public crate-rohanasan-0.4.90 (c (n "rohanasan") (v "0.4.90") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "1fal9qiinavdq01mbggpj0xjc8nlqrq8ynmnw3yspsp732fam7zp")))

(define-public crate-rohanasan-0.4.91 (c (n "rohanasan") (v "0.4.91") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "0v6gzp05bins2r4xdwkplwdzi7l7f2i5c0jk335v130qhiw4lv9f")))

(define-public crate-rohanasan-0.5.0 (c (n "rohanasan") (v "0.5.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)))) (h "04pn4cwdfah074kmj01jwzghwgvdw49a72bdkbjwx9nivhkiwiwh")))

(define-public crate-rohanasan-0.5.1 (c (n "rohanasan") (v "0.5.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)))) (h "1rdwdblnza5yq41mfk4xc049h7f6s2pxfvnyxka7862lyc2pyr0m")))

(define-public crate-rohanasan-0.5.2 (c (n "rohanasan") (v "0.5.2") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "173piy3cc4y08sb46xdaxg8wnvd5zczvpy3zm1bjgqzx05kalvnj")))

(define-public crate-rohanasan-0.5.21 (c (n "rohanasan") (v "0.5.21") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "0czs0kacmgsvvq2p4yjny31gb66gddl9sivhdr65xjxazwc3q7ia")))

(define-public crate-rohanasan-0.5.22 (c (n "rohanasan") (v "0.5.22") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "04dql9bpm204dpmpvg7d5rf2xny6jp1d8p115bfj0296zz2d9w6d")))

(define-public crate-rohanasan-0.5.24 (c (n "rohanasan") (v "0.5.24") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "0jin8nsi0zdiqrim5bif8c1iaamsnvsd0bm2mlhn53mzbrvl3vc8")))

(define-public crate-rohanasan-0.5.25 (c (n "rohanasan") (v "0.5.25") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "17x7dv9wbyck3i8rddgs4q2gzxfb6xrfydh0cp2a61vda42cj0w5")))

(define-public crate-rohanasan-0.5.26 (c (n "rohanasan") (v "0.5.26") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "1llhgg52g4x7584srpgx6fqjszjk3ijvcrf34cqjkkhf0axgqcql")))

(define-public crate-rohanasan-0.5.27 (c (n "rohanasan") (v "0.5.27") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "0pxndlmv7cwgjs6q5zn4lva283irx1p8b9k86xky4c4qa79iaaxv")))

(define-public crate-rohanasan-0.5.28 (c (n "rohanasan") (v "0.5.28") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "11r06f8avdghqk99v65077igz3qs0iq528pkpyzcy151vi7ishfs")))

(define-public crate-rohanasan-0.5.29 (c (n "rohanasan") (v "0.5.29") (d (list (d (n "quinn") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "1r585lcggc8gln11i0p3b7khnq019xai69jnlb84084xv57xhygz")))

(define-public crate-rohanasan-0.5.40 (c (n "rohanasan") (v "0.5.40") (d (list (d (n "quinn") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "1w0w6v0v325kb93g3m4k9aildz5zr4vgw48fpkrvqccgmsnzrsjs")))

(define-public crate-rohanasan-0.5.41 (c (n "rohanasan") (v "0.5.41") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "06d5xkyq8f7g5s5y9r3gg9mb8w571sgv2c7w0gbv3v0adfc8mbj3")))

(define-public crate-rohanasan-0.5.42 (c (n "rohanasan") (v "0.5.42") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "1lwq5m19ny5b3zjb7lzvi8w0cl4hhggab4rbgpmpl8p8p3qgh8n7")))

(define-public crate-rohanasan-0.5.44 (c (n "rohanasan") (v "0.5.44") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "1jb3rjwiiz73wg5awqjzmrn1mkqhz3pd8ny8hmdjdgyq1bjcwv6k")))

(define-public crate-rohanasan-0.5.46 (c (n "rohanasan") (v "0.5.46") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "1yjg9i0hna6z0h8ryv7v11l1jwv9j1qx3ajzdcbip13pgz53hsad")))

(define-public crate-rohanasan-0.5.47 (c (n "rohanasan") (v "0.5.47") (d (list (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "0z2adzgf4c6jp23iiqm982j3v3q3xqjzdl19mymhr40fcaxm87v5")))

