(define-module (crates-io ro ha rohanasanpm) #:use-module (crates-io))

(define-public crate-rohanasanpm-0.0.0 (c (n "rohanasanpm") (v "0.0.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "14whsbnlbd76vn3zygqppfr6p2knzfhaprfis8wpyyha3d5kwgq7")))

(define-public crate-rohanasanpm-0.1.1 (c (n "rohanasanpm") (v "0.1.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1fq75fqmag5a305fdscq5bil60zwddm3i0g7jyhd0zh11by04w5g")))

