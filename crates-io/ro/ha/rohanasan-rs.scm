(define-module (crates-io ro ha rohanasan-rs) #:use-module (crates-io))

(define-public crate-rohanasan-rs-0.1.0 (c (n "rohanasan-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "05ym5r430nznk4v8l37675y97f6py2wv4fb60i267yg66fjbfyvn") (y #t)))

(define-public crate-rohanasan-rs-0.1.1 (c (n "rohanasan-rs") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "01hfpcd58cvn38lzsk92mx7d8v5665lnlqhv0n11nm1n37qll90s") (y #t)))

(define-public crate-rohanasan-rs-0.1.2 (c (n "rohanasan-rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "0c1jxdj4vsi00dg3n5364m85x2gbag26rj9sd2vfnlbvazxi2v5h") (y #t)))

(define-public crate-rohanasan-rs-0.2.0 (c (n "rohanasan-rs") (v "0.2.0") (h "0m3qimjbaq7yrwzz89kppylb28kq6nglmafr55lx1qxk0rdld64h") (y #t)))

