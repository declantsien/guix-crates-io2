(define-module (crates-io ro ha rohanasantml) #:use-module (crates-io))

(define-public crate-rohanasantml-0.0.0 (c (n "rohanasantml") (v "0.0.0") (h "0ady5ak9k7q6y0pi8qnjwgic4d1n9xh3803bm500ws55a3iyhwyw")))

(define-public crate-rohanasantml-0.0.1 (c (n "rohanasantml") (v "0.0.1") (h "01wbz3db1f74jiskfqvk6snlm7vj1navilfcii1aqm9cp1riaj8p")))

(define-public crate-rohanasantml-0.0.2 (c (n "rohanasantml") (v "0.0.2") (h "0wbmyz8q1a1iknr1vl0w9l8g1wx5wfgvbs0270wah4cq4lybl823")))

