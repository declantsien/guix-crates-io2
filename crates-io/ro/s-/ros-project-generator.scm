(define-module (crates-io ro s- ros-project-generator) #:use-module (crates-io))

(define-public crate-ros-project-generator-0.1.3 (c (n "ros-project-generator") (v "0.1.3") (d (list (d (n "askama") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1495h2llddg1jy4cinm37l5p6s64r6h2d7ywf39yls26kc86nr5g")))

(define-public crate-ros-project-generator-0.1.4 (c (n "ros-project-generator") (v "0.1.4") (d (list (d (n "askama") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0y9c455ql7plzs40fsbpczbix0v88gpk6w7axa7vzbshcjpklppv")))

(define-public crate-ros-project-generator-0.1.5 (c (n "ros-project-generator") (v "0.1.5") (d (list (d (n "askama") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1mpx1ci502v4f7r79nyllcliz61dqsvrf9xld8lkw2c11khgjp3a")))

