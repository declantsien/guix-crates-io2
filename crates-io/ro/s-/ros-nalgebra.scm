(define-module (crates-io ro s- ros-nalgebra) #:use-module (crates-io))

(define-public crate-ros-nalgebra-0.0.1 (c (n "ros-nalgebra") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.21") (d #t) (k 0)) (d (n "rosrust") (r "^0.9") (d #t) (k 0)))) (h "1pxhry03zfkagqn5l44c8zx06mmfsxzimy3pp2l4zh3y7w9nbl3n")))

(define-public crate-ros-nalgebra-0.0.2 (c (n "ros-nalgebra") (v "0.0.2") (d (list (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "rosrust") (r "^0.9") (d #t) (k 0)))) (h "11lc7dpcx001jqxfrsiiqvdj360jshmccmdnhja6hcf1zl85skbh")))

(define-public crate-ros-nalgebra-0.0.3 (c (n "ros-nalgebra") (v "0.0.3") (d (list (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "rosrust") (r "^0.9") (d #t) (k 0)))) (h "0664qvbmzrwrav3masvqscgcvwkb1xkpskb27jgr9zh131la71kx")))

(define-public crate-ros-nalgebra-0.0.4 (c (n "ros-nalgebra") (v "0.0.4") (d (list (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "rosrust") (r "^0.9") (d #t) (k 0)))) (h "1qbrl4jrshmvfsb56ld3q3x8z01bgz4ass0ymwghqwsq3zwara5v")))

(define-public crate-ros-nalgebra-0.0.5 (c (n "ros-nalgebra") (v "0.0.5") (d (list (d (n "nalgebra") (r "^0.25") (d #t) (k 2)) (d (n "rosrust") (r "^0.9") (d #t) (k 2)))) (h "01sr3cc8qxzn5s2ipliwf2mb5x31dadizhd8ch4gb9dd74f48n56")))

(define-public crate-ros-nalgebra-0.0.6 (c (n "ros-nalgebra") (v "0.0.6") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 2)) (d (n "rosrust") (r "^0.9") (d #t) (k 2)))) (h "1iqzdscrriypr3y4934ws3wyfwvcj72if0wb8bnrxdj7n2l2p19i")))

(define-public crate-ros-nalgebra-0.1.0 (c (n "ros-nalgebra") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31") (d #t) (k 2)) (d (n "rosrust") (r "^0.9") (d #t) (k 2)))) (h "0l0xw9rhya9gb9fv9gw1gp9wdx2vz3sm75h97hiadq6s32ccg6nx")))

