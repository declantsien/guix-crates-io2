(define-module (crates-io ro n- ron-utils) #:use-module (crates-io))

(define-public crate-ron-utils-0.1.0-preview1 (c (n "ron-utils") (v "0.1.0-preview1") (d (list (d (n "ron-reboot") (r "^0.1.0-preview2") (f (quote ("utf8_parser"))) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0hjskz9yzpa5ixrw5k2npnhq36vfigw9my3344ajq7g2nq9gw33z") (f (quote (("serde1" "serde" "ron-reboot/utf8_parser_serde1"))))))

(define-public crate-ron-utils-0.1.0-preview2 (c (n "ron-utils") (v "0.1.0-preview2") (d (list (d (n "ron-reboot") (r "^0.1.0-preview2") (f (quote ("utf8_parser"))) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0iz5xw5mf2b3jkl7nqs185rr8maykdmcmvjf6az7cywp1973lg27") (f (quote (("serde1" "serde" "ron-reboot/utf8_parser_serde1"))))))

(define-public crate-ron-utils-0.1.0-preview3 (c (n "ron-utils") (v "0.1.0-preview3") (d (list (d (n "ron-reboot") (r "^0.1.0-preview4") (f (quote ("utf8_parser"))) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1q9q9j7bk59r1prr9phq0qdxci63b4rriz0gziixdazx02v9vf1n") (f (quote (("serde1" "serde" "ron-reboot/utf8_parser_serde1"))))))

(define-public crate-ron-utils-0.1.0-preview4 (c (n "ron-utils") (v "0.1.0-preview4") (d (list (d (n "ron-reboot") (r "^0.1.0-preview4") (f (quote ("utf8_parser"))) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0qjv2hx7pr3jjwr0nxpy0ym7px9j83hvylihyiw6sf3dgfhzxysb") (f (quote (("serde1" "serde" "ron-reboot/utf8_parser_serde1"))))))

(define-public crate-ron-utils-0.1.0-preview6 (c (n "ron-utils") (v "0.1.0-preview6") (d (list (d (n "ron-reboot") (r "^0.1.0-preview6") (f (quote ("utf8_parser"))) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0c6kvpg02n3p64zm229ndg28l11axbk7m3jwa4a9kwdc7df8w1sq") (f (quote (("serde1" "serde" "ron-reboot/utf8_parser_serde1"))))))

(define-public crate-ron-utils-0.1.0-preview7 (c (n "ron-utils") (v "0.1.0-preview7") (d (list (d (n "ron-reboot") (r "^0.1.0-preview8") (f (quote ("utf8_parser"))) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0cqsnf62fscbr8kmaiizrgc5fk0jrwqawiy03hf3jvg2l6wcb0va") (f (quote (("serde1" "serde" "ron-reboot/utf8_parser_serde1"))))))

(define-public crate-ron-utils-0.1.0-preview8 (c (n "ron-utils") (v "0.1.0-preview8") (d (list (d (n "ron-reboot") (r "^0.1.0-preview10") (f (quote ("utf8_parser"))) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1d39f1q3dg26q83nyrn6m3xgmrk87gijndai88vnz2s8c0b7y8pd") (f (quote (("serde1" "serde" "ron-reboot/utf8_parser_serde1"))))))

