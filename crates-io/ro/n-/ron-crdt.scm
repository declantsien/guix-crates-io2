(define-module (crates-io ro n- ron-crdt) #:use-module (crates-io))

(define-public crate-ron-crdt-0.1.0 (c (n "ron-crdt") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "03bjryaxrz5fhcjw4bh1xwvr1hyw2pcswl000p8g0fh0iqkr5ghs")))

(define-public crate-ron-crdt-0.2.0 (c (n "ron-crdt") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0ljznchw6mxgv6ykphi4rhscbqbmvx44vq2xsjjb8n0gdi893j00")))

(define-public crate-ron-crdt-0.3.0 (c (n "ron-crdt") (v "0.3.0") (d (list (d (n "ron-uuid") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "08j7xbplrdy6h982grzqxjin5222r461c4b1j9hyniqp7m82l8gn")))

(define-public crate-ron-crdt-0.3.1 (c (n "ron-crdt") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ron-uuid") (r "^0") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "006f79mwg0xh450vg0y3w82k74lami6ylqlffb7img3575zpd02a")))

(define-public crate-ron-crdt-0.3.2 (c (n "ron-crdt") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ron-uuid") (r "^0") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1afn4420avxfisnnqk0k73prr5nyyh1lky5jb4imy50gcnwydryc")))

(define-public crate-ron-crdt-0.4.0 (c (n "ron-crdt") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ron-uuid") (r "^0") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0fws326088arb5ara3xs5ir5ry6yqd45hkiyadgjrpbrh9fn013q")))

