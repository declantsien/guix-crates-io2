(define-module (crates-io ro n- ron-uuid) #:use-module (crates-io))

(define-public crate-ron-uuid-0.2.0 (c (n "ron-uuid") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0jz3d4v2f6mg2lz9nwmfm4m3h9bllhd5i4652jvaagki0gnchasp")))

(define-public crate-ron-uuid-0.3.0 (c (n "ron-uuid") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)))) (h "0v4cffdg04gs3a4sr8r32ramrq9z51n6nrf18zhbbb7gs9hk784m")))

(define-public crate-ron-uuid-0.3.1 (c (n "ron-uuid") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)))) (h "0z1k01n9gcpwqn0rnnla8c07m9xidqvizf1k3vnc9fm6g73pbb5b")))

(define-public crate-ron-uuid-0.4.0 (c (n "ron-uuid") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "11alr915jdh6wnd74yl18mz6lfzfvzfkqz7qx24nkrimqfy79kyp")))

