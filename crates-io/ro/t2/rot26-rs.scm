(define-module (crates-io ro t2 rot26-rs) #:use-module (crates-io))

(define-public crate-rot26-rs-0.1.0 (c (n "rot26-rs") (v "0.1.0") (h "02p4cwphibaa3m9b2xvcamqd0xb0wxbdb1zjnckghidcn9yhhwb9")))

(define-public crate-rot26-rs-0.1.1 (c (n "rot26-rs") (v "0.1.1") (h "1l1zisrz3rzcw2i7y3yl2xh8ldni63ijbm45pjj6shn7hnwxfy68")))

(define-public crate-rot26-rs-0.1.2 (c (n "rot26-rs") (v "0.1.2") (h "1vzxpp3kq82vffgj86knrvj3vizw3b9f95jzc8dzlbgr32pvb65z")))

