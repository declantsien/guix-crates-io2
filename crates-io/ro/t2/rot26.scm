(define-module (crates-io ro t2 rot26) #:use-module (crates-io))

(define-public crate-rot26-0.1.0 (c (n "rot26") (v "0.1.0") (h "0jdlrqf3s5abg523l68whp05z1aa6kb2pcdm409r0wqs7bbblqi9")))

(define-public crate-rot26-0.1.1 (c (n "rot26") (v "0.1.1") (d (list (d (n "rayon") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0d4ml5bbai7n7nwrmxi9vgnpal7byphdwfypyy6l861v04lr2xgy")))

(define-public crate-rot26-0.1.2 (c (n "rot26") (v "0.1.2") (d (list (d (n "rayon") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "06936zyckzy5xr8mffqdy16ny4lmla6fpwvj8rx6wak32wqyk6k8")))

