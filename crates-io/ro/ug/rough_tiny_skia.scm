(define-module (crates-io ro ug rough_tiny_skia) #:use-module (crates-io))

(define-public crate-rough_tiny_skia-0.4.0 (c (n "rough_tiny_skia") (v "0.4.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "roughr") (r "^0.4.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.8") (d #t) (k 0)))) (h "0piv6sbynd72cv3qvzny96hgl2z4by910z1fbwvnhcsyiqxv4i7s")))

(define-public crate-rough_tiny_skia-0.5.0 (c (n "rough_tiny_skia") (v "0.5.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "roughr") (r "^0.5.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.8") (d #t) (k 0)))) (h "0fydvrxwwkn0nmk8cw5k6bk3aggkyshv7da09jydy6zyvbnds4hz")))

(define-public crate-rough_tiny_skia-0.6.0 (c (n "rough_tiny_skia") (v "0.6.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.7") (d #t) (k 0)) (d (n "roughr") (r "^0.6.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.8") (d #t) (k 0)))) (h "12kndmrpgm2hwb9d88rnbnjky4za487d2mr6z6csgz8ckw891bc3")))

