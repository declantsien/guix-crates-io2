(define-module (crates-io ro ug rough) #:use-module (crates-io))

(define-public crate-rough-0.2.0 (c (n "rough") (v "0.2.0") (d (list (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "0rynjc46kxa1kddhdl4xv8wqq5y638bmax7l696pa4j19kvm6x7b")))

