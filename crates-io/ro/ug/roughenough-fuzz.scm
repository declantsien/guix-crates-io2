(define-module (crates-io ro ug roughenough-fuzz) #:use-module (crates-io))

(define-public crate-roughenough-fuzz-0.1.0 (c (n "roughenough-fuzz") (v "0.1.0") (d (list (d (n "afl") (r "^0.3") (d #t) (k 0)) (d (n "honggfuzz") (r "^0.5") (d #t) (k 0)) (d (n "roughenough") (r "^1.0") (d #t) (k 0)))) (h "05zgsv6zkz9gm7lrqi6wfrlqigvj2wi86k8x4an2r96vqwafq8gq")))

