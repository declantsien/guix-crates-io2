(define-module (crates-io ro ug rouge) #:use-module (crates-io))

(define-public crate-rouge-0.0.1 (c (n "rouge") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (f (quote ("compat" "thread-pool"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vy6xjy3qn1w34nk5g96xiwm9hyyxr8lir23yc4x7vd4sgs5vm8h")))

