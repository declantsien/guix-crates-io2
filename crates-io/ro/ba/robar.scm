(define-module (crates-io ro ba robar) #:use-module (crates-io))

(define-public crate-robar-0.1.0 (c (n "robar") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (f (quote ("randr"))) (d #t) (k 0)))) (h "1241hmby8a2f0fqa1zwxf8z1vlww2p24q9km5zfcbm6jvqb8ppjd")))

(define-public crate-robar-1.0.0 (c (n "robar") (v "1.0.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (f (quote ("randr"))) (d #t) (k 0)))) (h "0nly5r1nz2lhzrg7fwyn7dqjk75fw0yiqaara0c41hhi01gyccpv")))

(define-public crate-robar-2.0.0 (c (n "robar") (v "2.0.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (f (quote ("randr"))) (d #t) (k 0)))) (h "1r8afl7kxyvyijsdjqnyl7q7rwhr6gsa98gpp5w54lak3732s2b2")))

(define-public crate-robar-2.0.1 (c (n "robar") (v "2.0.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (f (quote ("randr"))) (d #t) (k 0)))) (h "0x5hjsj62ic5wm30bx8gm165a6xywzv1jg1k9fwp19awvdcch951")))

