(define-module (crates-io ro vv rovv_derive) #:use-module (crates-io))

(define-public crate-rovv_derive-0.1.0 (c (n "rovv_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "15cr347pbhv18wklz13w91195amxcsihvbwingpjbswrd83x559j")))

(define-public crate-rovv_derive-0.1.1 (c (n "rovv_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1hcbfwibvsbjwbj7cvljd1s0p237l53zvvi7djxkhpddjmzdh6ww")))

(define-public crate-rovv_derive-0.2.0 (c (n "rovv_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1kqii35bnsr0ds4qn4ifc7fcan47kk44hap3djnd3j0j43xiv17v")))

