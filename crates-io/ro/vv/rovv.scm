(define-module (crates-io ro vv rovv) #:use-module (crates-io))

(define-public crate-rovv-0.1.0 (c (n "rovv") (v "0.1.0") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs") (r "^0.2") (d #t) (k 0)) (d (n "rovv_derive") (r "^0.1") (d #t) (k 0)))) (h "1xhazryf5d4dwvq50d87jjfcn8xqvl4j32520xz1rx0sp7rz92ba")))

(define-public crate-rovv-0.1.1 (c (n "rovv") (v "0.1.1") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs") (r "^0.2") (d #t) (k 0)) (d (n "rovv_derive") (r "^0.1") (d #t) (k 0)))) (h "1cving396i9yilxc0c8fyv3sdm3zm101r637m7c7w10ywc6hj86n")))

(define-public crate-rovv-0.2.0 (c (n "rovv") (v "0.2.0") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "rovv_derive") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 1)))) (h "0sivwqdpm14wyzal4z47msvbrakdsb05aba7wc47d7cgszf8zl6m")))

(define-public crate-rovv-0.2.1 (c (n "rovv") (v "0.2.1") (d (list (d (n "inwelling") (r "^0.3") (d #t) (k 1)) (d (n "lens-rs") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "rovv_derive") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 1)))) (h "1hw4m9s14hvk7mcdg3x3zvdc7yyp0xsm5zzx9wfazvziwbdqqlnl")))

