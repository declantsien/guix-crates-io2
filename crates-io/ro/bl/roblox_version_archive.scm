(define-module (crates-io ro bl roblox_version_archive) #:use-module (crates-io))

(define-public crate-roblox_version_archive-1.0.0 (c (n "roblox_version_archive") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07ia6csc283rvrqpn9liks43d30qw7350lbsp924irj0g5d8g5m6")))

(define-public crate-roblox_version_archive-1.0.1 (c (n "roblox_version_archive") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q98dpha0v6wiyp6b0v9f5qv80xx7fxlxyc453lad046ssgylb3s")))

(define-public crate-roblox_version_archive-1.0.2 (c (n "roblox_version_archive") (v "1.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "017sam9k0i4z106i7wda8l6q5svnvnvrzq958hy0hjz3rrd39xbc")))

(define-public crate-roblox_version_archive-1.0.3 (c (n "roblox_version_archive") (v "1.0.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dyx7gk9rzsw78ky4iy6s5zlnavl5qldhxadl755ymi6w7hdc1c2")))

(define-public crate-roblox_version_archive-1.0.4 (c (n "roblox_version_archive") (v "1.0.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wyc0kj1hpmxrzgwqjj8hcnrvxwr806lx4pbmpp91hpq202zw89a")))

