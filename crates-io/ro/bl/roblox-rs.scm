(define-module (crates-io ro bl roblox-rs) #:use-module (crates-io))

(define-public crate-roblox-rs-0.1.0 (c (n "roblox-rs") (v "0.1.0") (h "1wz8a2ysbyx5ds7glb59ca3d91g0483vpym789yz5hbfm5njnpdc") (y #t)))

(define-public crate-roblox-rs-0.0.0 (c (n "roblox-rs") (v "0.0.0") (h "1ayafn83wbd2b4aj3lzn29dxxcrjhj9fk4ikxwf21wza5873m4wr") (y #t)))

(define-public crate-roblox-rs-0.0.0-moved (c (n "roblox-rs") (v "0.0.0-moved") (h "0jy07kvnw0iwrkk59qa2ngxhw53vg470cqvkni8ydm5g8scj2zan") (y #t)))

