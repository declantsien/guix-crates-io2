(define-module (crates-io ro bl roblib-macro) #:use-module (crates-io))

(define-public crate-roblib-macro-0.1.0 (c (n "roblib-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y6zlajzhrwcilbsfnc1sr6a1xmjrb6dnfcq9lm20vmjj7lndl01")))

