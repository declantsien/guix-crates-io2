(define-module (crates-io ro bl roblox) #:use-module (crates-io))

(define-public crate-roblox-0.9.0 (c (n "roblox") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.5") (f (quote ("std"))) (o #t) (k 0)))) (h "14ai3a75xp7yf0r552wixrc62fmvxi4a3d6wb6qnvj2lk1kipa88")))

(define-public crate-roblox-1.0.0 (c (n "roblox") (v "1.0.0") (h "1xv2raplxwmws3wivgcwamjv78anlwmijph9si3g114hnxg96ggi")))

(define-public crate-roblox-1.0.1 (c (n "roblox") (v "1.0.1") (h "1ksgca514fh7rq40l86drr3lh3gv8xyav1dnj95hib03cjx25bkz")))

