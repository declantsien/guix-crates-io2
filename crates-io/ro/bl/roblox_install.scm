(define-module (crates-io ro bl roblox_install) #:use-module (crates-io))

(define-public crate-roblox_install-0.1.0 (c (n "roblox_install") (v "0.1.0") (d (list (d (n "winreg") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)))) (h "0ssg1qlz5aql803kk6fkkf6dwjnxns577zsingpd0nzpxja84shn")))

(define-public crate-roblox_install-0.1.1 (c (n "roblox_install") (v "0.1.1") (d (list (d (n "winreg") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)))) (h "01mqs1vrp9ymamf7rm6pjb4skg1pvxcfgm0xfpmqb2fysxp80x65")))

(define-public crate-roblox_install-0.1.2 (c (n "roblox_install") (v "0.1.2") (d (list (d (n "winreg") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)))) (h "13pv435smliq439gq86jja94pfmi1qsizsp9j0j516qfwq07jyi0")))

(define-public crate-roblox_install-0.2.0 (c (n "roblox_install") (v "0.2.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "winreg") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)))) (h "075q3wf4pqrf9gjg0wwdkcajxgyxikmk39g7l3n30a7x34mgdilz")))

(define-public crate-roblox_install-0.2.1 (c (n "roblox_install") (v "0.2.1") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "winreg") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)))) (h "170y7pgag4kglx8nw5ndp050zwqvxc4y6r6k24aizyymcnwvhwpx")))

(define-public crate-roblox_install-0.2.2 (c (n "roblox_install") (v "0.2.2") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "winreg") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)))) (h "0m2abr9llzwq1i607pyzxrr0mbihdckr8kbmmzfygg37rnxxn3pq")))

(define-public crate-roblox_install-0.3.0 (c (n "roblox_install") (v "0.3.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "winreg") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)))) (h "09w52h1j77n0aq732hszfpz49j14k8dvarmz4yqvjgdqh5pwin4n")))

(define-public crate-roblox_install-1.0.0 (c (n "roblox_install") (v "1.0.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "winreg") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)))) (h "04xvjpjkpnrgn7vw35vvfz25q5qcicnxh9i0inpg31x3jg3bhfvl")))

