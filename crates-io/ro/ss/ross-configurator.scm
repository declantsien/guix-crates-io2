(define-module (crates-io ro ss ross-configurator) #:use-module (crates-io))

(define-public crate-ross-configurator-0.1.1 (c (n "ross-configurator") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ross-protocol") (r "^1.0.1") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "1aw39h02dkhbma9pjplxzkncq48lhak79rg51jqzwmf334agy3rg") (y #t)))

(define-public crate-ross-configurator-0.2.0 (c (n "ross-configurator") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ross-protocol") (r "^1.0.1") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "1gnsfjcyphmv1xqyzw61w3hkswwdy5qbzn7kq8bm0031jjl9vc4v") (y #t)))

(define-public crate-ross-configurator-1.0.0 (c (n "ross-configurator") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "parse_int") (r "^0.5.0") (d #t) (k 0)) (d (n "ross-config") (r "^1.14.0") (d #t) (k 0)) (d (n "ross-dsl") (r "^1.0.0") (d #t) (k 0)) (d (n "ross-protocol") (r "^1.19.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "140d5da9ggzxsvqi0dlgqa2gvmxvlhks4199554kdd2wz5jy305v") (y #t)))

(define-public crate-ross-configurator-1.1.0 (c (n "ross-configurator") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "parse_int") (r "^0.5.0") (d #t) (k 0)) (d (n "ross-config") (r "^2.22.0") (d #t) (k 0)) (d (n "ross-dsl") (r "^2.14.0") (d #t) (k 0)) (d (n "ross-protocol") (r "^1.33.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "1wq4b0yy4yh6b4j8sgcn78pz6adwn4ak2ffbynq6m0mfvvhjqmkq") (y #t)))

(define-public crate-ross-configurator-1.2.0 (c (n "ross-configurator") (v "1.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "parse_int") (r "^0.5.0") (d #t) (k 0)) (d (n "ross-config") (r "^2.24.0") (d #t) (k 0)) (d (n "ross-dsl") (r "^2.19.0") (d #t) (k 0)) (d (n "ross-protocol") (r "^1.33.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "072v49cknl9crz32adv29xrgz7aw8zgjrk0w93bsz0qgyp07hq0n") (y #t)))

(define-public crate-ross-configurator-1.3.0 (c (n "ross-configurator") (v "1.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "parse_int") (r "^0.5.0") (d #t) (k 0)) (d (n "ross-config") (r "^2.24.0") (d #t) (k 0)) (d (n "ross-dsl") (r "^2.19.0") (d #t) (k 0)) (d (n "ross-protocol") (r "^2.0.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "0zlijajanfkkvvprgjwsnslc662kb56nrwg12iqaig8g8z06qwhi") (y #t)))

(define-public crate-ross-configurator-1.4.0 (c (n "ross-configurator") (v "1.4.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "parse_int") (r "^0.5.0") (d #t) (k 0)) (d (n "ross-config") (r "^2.25.0") (d #t) (k 0)) (d (n "ross-dsl") (r "^2.20.0") (d #t) (k 0)) (d (n "ross-protocol") (r "^2.4.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "12ah0vgwha1yqw57bsj585w4l1ijfj7zcmc6lpm2ry54zm4868lz") (y #t)))

(define-public crate-ross-configurator-1.5.0 (c (n "ross-configurator") (v "1.5.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "parse_int") (r "^0.5.0") (d #t) (k 0)) (d (n "ross-config") (r "^2.26.0") (d #t) (k 0)) (d (n "ross-dsl") (r "^2.21.0") (d #t) (k 0)) (d (n "ross-protocol") (r "^2.5.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "1ws7pdhw16byhvngif9zf862imp4cjc1a1wjb3zp7jhj8fs1bzpq") (y #t)))

(define-public crate-ross-configurator-1.6.0 (c (n "ross-configurator") (v "1.6.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "parse_int") (r "^0.5.0") (d #t) (k 0)) (d (n "ross-config") (r "^2.27.0") (d #t) (k 0)) (d (n "ross-dsl") (r "^2.22.0") (d #t) (k 0)) (d (n "ross-protocol") (r "^2.6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "0ip87jcflrgjxhviw6pgdj397l4gi80m3lszmm4jpz896b7lh467") (y #t)))

