(define-module (crates-io ro ss ross) #:use-module (crates-io))

(define-public crate-ross-0.1.6 (c (n "ross") (v "0.1.6") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "statistical") (r "^0.1.1") (d #t) (k 0)))) (h "1zdm19dnd84fqv0pzriyjh45hiiixqrz7g12p08myh1zyycj8vfd")))

(define-public crate-ross-0.1.7 (c (n "ross") (v "0.1.7") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "statistical") (r "^0.1.1") (d #t) (k 0)))) (h "0iyhycq4bzqwnr6p2krrghf4pi1gh6zbgjwddbk0djql1i424gsn")))

