(define-module (crates-io ro ss ross-config) #:use-module (crates-io))

(define-public crate-ross-config-0.1.0 (c (n "ross-config") (v "0.1.0") (d (list (d (n "ross-protocol") (r "^1.14.0") (d #t) (k 0)))) (h "10fmwr0yjfx1f24scd0kkdz8bzmp89mlpa5r5bm2plpvrv7hwcs3") (y #t)))

(define-public crate-ross-config-1.0.0 (c (n "ross-config") (v "1.0.0") (d (list (d (n "ross-protocol") (r "^1.14.0") (d #t) (k 0)))) (h "068c9w90x9i91h31ndag3ha7qjvxfsqkyxz2n2dvvjh31mxjhm5z") (y #t)))

(define-public crate-ross-config-1.1.0 (c (n "ross-config") (v "1.1.0") (d (list (d (n "ross-protocol") (r "^1.14.0") (d #t) (k 0)))) (h "1lcfc0drml67swp473bayz4nxl9x2hg1z1nx3rzrjgsa14xwwkca") (y #t)))

(define-public crate-ross-config-1.2.0 (c (n "ross-config") (v "1.2.0") (d (list (d (n "ross-protocol") (r "^1.14.0") (d #t) (k 0)))) (h "0nmn0ggjwqkv5v9szbb6zz3igpbvdlrhxcnzdrlqg73q2kyyzfgk") (y #t)))

(define-public crate-ross-config-1.3.0 (c (n "ross-config") (v "1.3.0") (d (list (d (n "ross-protocol") (r "^1.14.0") (d #t) (k 0)))) (h "1parg9gycflp68b7gm3px1yax25dshj6xavp6m6a5445w3kc2dh7") (y #t)))

(define-public crate-ross-config-1.4.0 (c (n "ross-config") (v "1.4.0") (d (list (d (n "ross-protocol") (r "^1.14.0") (d #t) (k 0)))) (h "0lcbxzqq4m2pzs57lhsmjnk275l1pq3mqzh0i6njiv5rz8ipzw0v") (y #t)))

(define-public crate-ross-config-1.5.0 (c (n "ross-config") (v "1.5.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.14.0") (d #t) (k 0)))) (h "01hmmirdrq1wjqs7ah5s2qgy844aa350f8qs8z4nj6gp1djflm9g") (y #t)))

(define-public crate-ross-config-1.6.0 (c (n "ross-config") (v "1.6.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.14.0") (d #t) (k 0)))) (h "0779hj3nrhhbcm4fpyckpjmckc2mxbv1c8p92vfx251wjlhh21nf") (y #t)))

(define-public crate-ross-config-1.7.0 (c (n "ross-config") (v "1.7.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.14.0") (d #t) (k 0)))) (h "01qbmwhvdyd9c2i932fqj26i1jfs45l5400xwcycg7sd2jhlin88") (y #t)))

(define-public crate-ross-config-1.8.0 (c (n "ross-config") (v "1.8.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "typetag") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0v3xrw4v9vnpjack9c1041k2bjfq0blknrahsiksa0dzcax72s9l") (f (quote (("std" "serde" "typetag") ("default")))) (y #t)))

(define-public crate-ross-config-1.9.0 (c (n "ross-config") (v "1.9.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "typetag") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "1dbw0qinvwzi8cfpn4b95cdq0d4n52l165pznq1dq7k777l6dali") (f (quote (("std" "serde" "typetag") ("default")))) (y #t)))

(define-public crate-ross-config-1.10.0 (c (n "ross-config") (v "1.10.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "typetag") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "14r6cv7jrhfhy4vb62wmqxlzqn0l0y4saybfayl94g6aifn2nkm5") (f (quote (("std" "serde" "typetag") ("default")))) (y #t)))

(define-public crate-ross-config-1.11.0 (c (n "ross-config") (v "1.11.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "typetag") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "1i4z4k2q6qms3fxa4vh4qvqs2qszfc2rkswjxphlpxvb5c9cck2w") (f (quote (("std" "serde" "typetag") ("default")))) (y #t)))

(define-public crate-ross-config-1.12.0 (c (n "ross-config") (v "1.12.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.16.0") (d #t) (k 0)))) (h "1m38av5s5pmd1z5zpxxpwp9cz01pp8djp8fa1nbnya6d0wg222n9") (y #t)))

(define-public crate-ross-config-1.13.0 (c (n "ross-config") (v "1.13.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.16.0") (d #t) (k 0)))) (h "1525szc7slw8qlwi8pnvwzkdfykvc9z6na7rwkaj4vlpfsgfrq08") (y #t)))

(define-public crate-ross-config-1.14.0 (c (n "ross-config") (v "1.14.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.18.0") (d #t) (k 0)))) (h "1my12wbmalb2r2g2rcqsyvz2ibcqss5cyd6x3ikzw2vlkhddx0jj") (y #t)))

(define-public crate-ross-config-1.15.0 (c (n "ross-config") (v "1.15.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.20.0") (d #t) (k 0)))) (h "1ig5jna0lpzipl3bva2vcjha6cndjhjq5qp11776c8q7hnjin0al") (y #t)))

(define-public crate-ross-config-1.16.0 (c (n "ross-config") (v "1.16.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.20.0") (d #t) (k 0)))) (h "0qfygmh3bzdqqjafrlm94nknbkfiz252bn5rkflgrrgjv894q1rj") (y #t)))

(define-public crate-ross-config-1.17.0 (c (n "ross-config") (v "1.17.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "ross-protocol") (r "^1.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "0yk7h075y3rv5hv8xlvl86z2dpjprz2b3j0h1xk0ypzk1xnzsmrk") (y #t)))

(define-public crate-ross-config-1.18.0 (c (n "ross-config") (v "1.18.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "ross-protocol") (r "^1.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "1fw37nf9d8an031nqrgz14vd18f590vl8pqai0zyg1bg3wfy0365") (y #t)))

(define-public crate-ross-config-1.19.0 (c (n "ross-config") (v "1.19.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "ross-protocol") (r "^1.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "081wv1mjj4x9w6g5hk4d422v51c7w6g9blrczrwh3qvi9cdr10bp") (y #t)))

(define-public crate-ross-config-1.20.0 (c (n "ross-config") (v "1.20.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "19g9yng4r7l0dqxiv8x57nmi1p13qf8kiwm98cfq6s88ysy2x0wr") (y #t)))

(define-public crate-ross-config-1.21.0 (c (n "ross-config") (v "1.21.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "05dj8wkj1caqvw54mim0pcbap0bmwcb73j15zdnxp42p5qfs7jqz") (y #t)))

(define-public crate-ross-config-1.22.0 (c (n "ross-config") (v "1.22.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "00c4sb4gqb79i1b9w4v46j8jxm0yf061n4wbq7bams2z6wnyv7lc") (y #t)))

(define-public crate-ross-config-1.23.0 (c (n "ross-config") (v "1.23.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "0gqn6r31hrcik30iw0aslvr6zzwm6pr3n118z9qc0yvnb6cfk299") (y #t)))

(define-public crate-ross-config-1.24.0 (c (n "ross-config") (v "1.24.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "0nl3b2ik9k21z0psz0rijipgsjpkfmrbdwgm99lg0401h4yhr8qj") (y #t)))

(define-public crate-ross-config-1.25.0 (c (n "ross-config") (v "1.25.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "1061agj1cw8az3wfqbbz7ai4k6q797fr1np8vdd9z5klr4kbnpgn") (y #t)))

(define-public crate-ross-config-1.26.0 (c (n "ross-config") (v "1.26.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "0rpmiw3rs1diw2ldn99vwqpn12xd7schsd898ximf9psc83p61sq") (y #t)))

(define-public crate-ross-config-1.27.0 (c (n "ross-config") (v "1.27.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "1s5aas2368pcyncxfhd1l5y7n7b326b4hkiyq822lk0kwd4h7lym") (y #t)))

(define-public crate-ross-config-1.28.0 (c (n "ross-config") (v "1.28.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "0vvn8q1ldkg3j4pvad0nijsg340pv1fr95ks5x607dixaarznmwq") (y #t)))

(define-public crate-ross-config-1.29.0 (c (n "ross-config") (v "1.29.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "1rcpl18fk59mcqslxhggg0yvk8nx8gilj8nag3l7zfbdcxix5ng7") (y #t)))

(define-public crate-ross-config-1.30.0 (c (n "ross-config") (v "1.30.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "1c6fsf2a4b34q60gbwxc7fl4j092y4lryvlnw5d3b5chw97gb196") (y #t)))

(define-public crate-ross-config-1.31.0 (c (n "ross-config") (v "1.31.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "199wyibv7ybwdmlhn6wvia5nld1ykz8rz6mnxsqwjh08hxp6fl79") (y #t)))

(define-public crate-ross-config-2.0.0 (c (n "ross-config") (v "2.0.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "0ca0xfgx9l4g7yvzdvxfd3a2xdm0bh07frpvmjw6vsl7mg7y33n2") (y #t)))

(define-public crate-ross-config-2.1.0 (c (n "ross-config") (v "2.1.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("alloc"))) (k 0)) (d (n "ross-protocol") (r "^1.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("alloc"))) (k 0)))) (h "0c7iqqs75ipp50gzs1bdf7vfh9f5l8jkq39bwjfi52xx84w1bbbz") (y #t)))

(define-public crate-ross-config-2.2.0 (c (n "ross-config") (v "2.2.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.22.0") (d #t) (k 0)))) (h "0pvxjxb86yccalz4bar1l9x1w0bs2k891jizklwb70c2wbxc2xfx") (y #t)))

(define-public crate-ross-config-2.3.0 (c (n "ross-config") (v "2.3.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.25.0") (d #t) (k 0)))) (h "1hd7qqgx99s00h6gzqf73a9mama6chrs5z9zsm6h622z6k7r7mc8") (y #t)))

(define-public crate-ross-config-2.4.0 (c (n "ross-config") (v "2.4.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.25.0") (d #t) (k 0)))) (h "1xxs9fq3irpjdsradh3rkpycnjq2hb2mlhh0i4vgxnh4730mp6gq") (y #t)))

(define-public crate-ross-config-2.5.0 (c (n "ross-config") (v "2.5.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.25.0") (d #t) (k 0)))) (h "09pmwkch2qxn4gv98ssjyym4h7arl7pndpa39wllxi6p0c2ilymz") (y #t)))

(define-public crate-ross-config-2.6.0 (c (n "ross-config") (v "2.6.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.25.0") (d #t) (k 0)))) (h "11bwih60p7rv1i8mkb8ns9ycmqsmg1x8k98azz7hl40aa56f2lqy") (y #t)))

(define-public crate-ross-config-2.7.0 (c (n "ross-config") (v "2.7.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.25.0") (d #t) (k 0)))) (h "16yvxai93nipd6h7r8avrpypw2ffn673ailkmkp4f9mmdgxx04qw") (y #t)))

(define-public crate-ross-config-2.8.0 (c (n "ross-config") (v "2.8.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.25.0") (d #t) (k 0)))) (h "1c7fwd1yw7110fzs4v26nn6yri7639290qw3p055yicm2rq64cbz") (y #t)))

(define-public crate-ross-config-2.9.0 (c (n "ross-config") (v "2.9.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.25.0") (d #t) (k 0)))) (h "0cc2w77aynj35ycrdpr9zkjgm87wg48mjxkjypf1pivnwfjiyckp") (y #t)))

(define-public crate-ross-config-2.10.0 (c (n "ross-config") (v "2.10.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.25.0") (d #t) (k 0)))) (h "1ckki9mdcf2ks1gwsnscdrqakrl3q59idackzi42iyafzpf29r3k") (y #t)))

(define-public crate-ross-config-2.11.0 (c (n "ross-config") (v "2.11.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.28.0") (d #t) (k 0)))) (h "10bbpd7zz59a0qvmw234p2qdkgqirb7m0936r4x50kj3gwas0l72") (y #t)))

(define-public crate-ross-config-2.12.0 (c (n "ross-config") (v "2.12.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.28.0") (d #t) (k 0)))) (h "1bbqmd0zy01g3n80691qf1ra66ddf1yphplvx2gbwnnqhgaq6srk") (y #t)))

(define-public crate-ross-config-2.13.0 (c (n "ross-config") (v "2.13.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.28.0") (d #t) (k 0)))) (h "0lrrv1cflm4mdxvkdvih0v7x2srza98liq50f3lxblfmsy6l53wq") (y #t)))

(define-public crate-ross-config-2.14.0 (c (n "ross-config") (v "2.14.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.28.0") (d #t) (k 0)))) (h "1s6iypnapccg41x3hhxw53qzg0f3bcp930pr6kar7i4l6h4ly1bq") (y #t)))

(define-public crate-ross-config-2.15.0 (c (n "ross-config") (v "2.15.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.28.0") (d #t) (k 0)))) (h "06z43x9g441wnancczc6gcci2xvz11z560pfvkjmhb4cp74y4xdw") (y #t)))

(define-public crate-ross-config-2.16.0 (c (n "ross-config") (v "2.16.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.30.0") (d #t) (k 0)))) (h "06nrfqjplhdy738fb64zjil84cpc77wwf239f5hql4f7z9h6fqh9") (y #t)))

(define-public crate-ross-config-2.17.0 (c (n "ross-config") (v "2.17.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.30.0") (d #t) (k 0)))) (h "19drgn3nnhpvcyqj2pq0r5qbiqqyg8v1vfbq9g36hp8mf34mvcik") (y #t)))

(define-public crate-ross-config-2.18.0 (c (n "ross-config") (v "2.18.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.30.0") (d #t) (k 0)))) (h "0gimifvlnsxvsj28bhcjhv84q31s8vkx9131gdq6ky26ygi700xl") (y #t)))

(define-public crate-ross-config-2.19.0 (c (n "ross-config") (v "2.19.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.30.0") (d #t) (k 0)))) (h "1iphnh8ihp1wcgcfnj5sdvhqv8l24pjwqfzn0n3p7pmxnr9vc0j6") (y #t)))

(define-public crate-ross-config-2.20.0 (c (n "ross-config") (v "2.20.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.31.0") (d #t) (k 0)))) (h "1qqi5sg5s2qzah74cs9irm6gx7655pgkkrg4c8m743fijvn9c8kn") (y #t)))

(define-public crate-ross-config-2.21.0 (c (n "ross-config") (v "2.21.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.31.0") (d #t) (k 0)))) (h "1lniy3gvcnbq0mkq86d2h95nly25rfgwld85w422498qwbyivngx") (y #t)))

(define-public crate-ross-config-2.22.0 (c (n "ross-config") (v "2.22.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.34.0") (d #t) (k 0)))) (h "0bj48la697hr6ckjg4s36m65243f783misrs625iqrp2w0acb8i3") (y #t)))

(define-public crate-ross-config-2.23.0 (c (n "ross-config") (v "2.23.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^1.34.0") (d #t) (k 0)))) (h "1i9yhdxb56f74pysag12xq1idh4n6agqwaa60ims8ahlsmvbllrp") (y #t)))

(define-public crate-ross-config-2.24.0 (c (n "ross-config") (v "2.24.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^2.0.0") (d #t) (k 0)))) (h "132s022z8mlzsfhm0v4gagnvysv9ccwbw3s8zlhssp4vzyj24wmg") (y #t)))

(define-public crate-ross-config-2.25.0 (c (n "ross-config") (v "2.25.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^2.4.1") (d #t) (k 0)))) (h "181vgck8b1q3ivqidmg3c1vd58d7r3652lmkc008ww93ps9fcgj6") (y #t)))

(define-public crate-ross-config-2.26.0 (c (n "ross-config") (v "2.26.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^2.5.0") (d #t) (k 0)))) (h "1xbk62yr2vblljzkbip0d67qbd8zrvbmdw3rfr4cshzj4q1fr80d") (y #t)))

(define-public crate-ross-config-2.27.0 (c (n "ross-config") (v "2.27.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^2.6.0") (d #t) (k 0)))) (h "16a41gg06i2izvrbcyybnqpyamkf3f16vn442jm70bx0k7v5kr4x") (y #t)))

(define-public crate-ross-config-2.28.0 (c (n "ross-config") (v "2.28.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^2.10.0") (d #t) (k 0)))) (h "11r7hzwfak3xw8v8g0i250jvqv6vl8xd6rrri4dc4bafkraxi2iv") (y #t)))

(define-public crate-ross-config-2.29.0 (c (n "ross-config") (v "2.29.0") (d (list (d (n "chrono") (r "^0.4.22") (k 0)) (d (n "downcast-rs") (r "^1.2.0") (k 0)) (d (n "ross-protocol") (r "^2.11.0") (d #t) (k 0)))) (h "0l577da3g5spvbma1kz6jhbb5xdfjvq5cm77d4184s0v5w8gm2yk") (y #t)))

