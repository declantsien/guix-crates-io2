(define-module (crates-io ro ss ross-mock) #:use-module (crates-io))

(define-public crate-ross-mock-1.0.0 (c (n "ross-mock") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ross-protocol") (r "^2.15.0") (d #t) (k 0)))) (h "19jwij5wja5lizmpyx7yj4pzyxfqiryj1337bv2i0yx3d24fy75b") (y #t)))

(define-public crate-ross-mock-1.1.0 (c (n "ross-mock") (v "1.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ross-protocol") (r "^2.15.0") (d #t) (k 0)))) (h "1p97v1mrc8sgggp9dfsmgw9q8ckl06yfxsh74krdmyinr62klnm2") (y #t)))

(define-public crate-ross-mock-1.2.0 (c (n "ross-mock") (v "1.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ross-protocol") (r "^2.15.0") (d #t) (k 0)))) (h "1cnfyk740rw3l9dpyasdr55dsmc6c2qr2ql9vm9cs82j3sj9n5r1") (y #t)))

(define-public crate-ross-mock-1.3.0 (c (n "ross-mock") (v "1.3.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ross-protocol") (r "^2.15.0") (d #t) (k 0)))) (h "1ygfd5wmnzwkpnqaf5nsx99sfc71z9y90qnjb2a5d0ngbj4q4ar9") (y #t)))

(define-public crate-ross-mock-1.4.0 (c (n "ross-mock") (v "1.4.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ross-protocol") (r "^2.15.0") (d #t) (k 0)))) (h "0qs9j7ipbkl2w6mqcbq99j2hwhgiz2vc4l9c9zp4pr79p443zank") (y #t)))

(define-public crate-ross-mock-1.5.0 (c (n "ross-mock") (v "1.5.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "ross-protocol") (r "^2.15.0") (d #t) (k 0)))) (h "10a5ahsg7njgjsjkngfhlyqsv672sw4jqljlka44d0z5cyl700yf") (y #t)))

