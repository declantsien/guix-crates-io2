(define-module (crates-io ro ss ross-logger) #:use-module (crates-io))

(define-public crate-ross-logger-0.1.0 (c (n "ross-logger") (v "0.1.0") (h "0glx3vidasghc5x2n3nmn7q0aqpnnb4xmg94038dp8hv8x3xyhc5") (y #t)))

(define-public crate-ross-logger-1.0.0 (c (n "ross-logger") (v "1.0.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "0fqx8dhf48dcz3zq86icf9fwi7la2zr5azvpc56gx6afc5h07zcr") (y #t)))

(define-public crate-ross-logger-1.0.1 (c (n "ross-logger") (v "1.0.1") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "1c04zslzpl0p8j27mk7lsgipqasmyzz3sl4mmzlddkwvp05zkgaw") (y #t)))

(define-public crate-ross-logger-1.0.2 (c (n "ross-logger") (v "1.0.2") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "13cphbsa70q8m4jvnd4h5a8phl9r8c8f9vzhg45g339rpx0dl5w1") (y #t)))

(define-public crate-ross-logger-1.0.3 (c (n "ross-logger") (v "1.0.3") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "0f0c3ks9llqizxlpnhbl4bywpi39z3jp00lq1gz85qwl6i8zh4la") (y #t)))

(define-public crate-ross-logger-1.1.0 (c (n "ross-logger") (v "1.1.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "006zfydb2hfapwgklm46jj48kbbiazrcas9dwldhzqhjfgrmv81l") (y #t)))

(define-public crate-ross-logger-1.1.1 (c (n "ross-logger") (v "1.1.1") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "1h6ayhjy0d54jpja8z34y07936cqxwdfqkrbyxmvajqx1yahi5cf") (y #t)))

(define-public crate-ross-logger-1.2.0 (c (n "ross-logger") (v "1.2.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "0rjiazdarngli99vjzj42hwvsjssqba6cfgpsvxkjvwhbhi62hkr") (y #t)))

(define-public crate-ross-logger-1.2.1 (c (n "ross-logger") (v "1.2.1") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "09gis75xyq3qblr35a5xig9wzpf8v4prf2n3g1w5bflrp3pxjn8x") (y #t)))

(define-public crate-ross-logger-1.2.2 (c (n "ross-logger") (v "1.2.2") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "1797z783cckkffmkv6m27jgdxpwbjfxqlb0kmpaf9vq3p4c7npb7") (y #t)))

(define-public crate-ross-logger-1.2.3 (c (n "ross-logger") (v "1.2.3") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "1hmha6n889l74ajbaxkqpk496rh473c680284cc65jw8axc6ydjp") (y #t)))

(define-public crate-ross-logger-1.3.0 (c (n "ross-logger") (v "1.3.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "1q1cy5v7j6p1q27yki949mc5fqq85xghapa1fx2vlhwzsxwbww21") (y #t)))

(define-public crate-ross-logger-1.4.0 (c (n "ross-logger") (v "1.4.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "0gnsdxb2r7r6g5gjzj9cyg89jlqivn4jg4pyj6914q93wjnqdfcc") (y #t)))

(define-public crate-ross-logger-1.5.0 (c (n "ross-logger") (v "1.5.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "0qigivrnmy98g2z8fgy6p5fcdjw1dabb7g3sa0yddzcyj2l1cljm") (y #t)))

(define-public crate-ross-logger-1.6.0 (c (n "ross-logger") (v "1.6.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "stm32f1xx-hal-bxcan") (r "^0.8.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "0qi1xq9418dwjyflx2fvgd98qcli845rgajp9ywrkxzi9qkdkcgd") (y #t)))

(define-public crate-ross-logger-1.7.0 (c (n "ross-logger") (v "1.7.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "stm32f1xx-hal") (r "^0.9.0") (f (quote ("rt" "stm32f103" "medium"))) (d #t) (k 0)))) (h "0r7f5dwf3i1840mn26s16drv0psgb02k2llj4j57drd19071zbva") (y #t)))

