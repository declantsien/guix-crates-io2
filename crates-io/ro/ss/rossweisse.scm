(define-module (crates-io ro ss rossweisse) #:use-module (crates-io))

(define-public crate-rossweisse-0.0.0 (c (n "rossweisse") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1sbzx3p4nl7f54fgl7cdjj2px2f1iydnndlqkq9g9sd370jmnj70")))

(define-public crate-rossweisse-0.0.1 (c (n "rossweisse") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "14p59m8c59l37b21sl5cn4bzk4m8f189543z6gs6qv357bj5xdar")))

(define-public crate-rossweisse-0.0.2 (c (n "rossweisse") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "13za4ji6rbvn0f58n5hfx0767k530rs6n3wm7zn4h4cs2wridrx8")))

(define-public crate-rossweisse-0.0.3 (c (n "rossweisse") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1rv9zcsdzibfi899969p6cqbpvlvz2lgjg5rqp792vncwb6wpag0")))

