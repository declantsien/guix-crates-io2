(define-module (crates-io ro ss ross-mqtt) #:use-module (crates-io))

(define-public crate-ross-mqtt-0.1.0 (c (n "ross-mqtt") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "dotenv_codegen") (r "^0.15.0") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2.0") (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.9.1") (d #t) (k 0)) (d (n "ross-configurator") (r "^1.4.0") (d #t) (k 0)) (d (n "ross-protocol") (r "^2.4.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "071zd9g228byb0rppjljwlrvc1n33mbazhprnrn1bfwyp9nwa7cn") (y #t)))

