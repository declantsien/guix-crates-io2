(define-module (crates-io ro ss ross-http) #:use-module (crates-io))

(define-public crate-ross-http-0.1.0 (c (n "ross-http") (v "0.1.0") (h "18yylfnd837cv0x01mhq3bhxb7ganwhclxl8xrf7mngxgdyhiz3c") (y #t)))

(define-public crate-ross-http-1.0.0 (c (n "ross-http") (v "1.0.0") (h "0z22zdcyf89p18jcmy4cpilsivmwmpllvpfw8w8xnkidab3gk0jj") (y #t)))

(define-public crate-ross-http-1.1.0 (c (n "ross-http") (v "1.1.0") (h "0iw57qz690scr5dm2djy5ksj4w02kiyp2wj46pmpqyyr2a5lcfxn") (y #t)))

(define-public crate-ross-http-1.2.0 (c (n "ross-http") (v "1.2.0") (h "1i6vn79di9zbhyy920p25ri3bza58nig7c7lfbq8is6qa989ly30") (y #t)))

(define-public crate-ross-http-1.3.0 (c (n "ross-http") (v "1.3.0") (h "0w5axfa42q8hkkwqdnvj19qhfbnqgra084g95dcghx4lmhic3iiv") (y #t)))

(define-public crate-ross-http-1.4.0 (c (n "ross-http") (v "1.4.0") (h "05jl728kf9ksi6l4x6amp0xfwbrxfgw6lizyy6wi3khlbh10z808") (y #t)))

(define-public crate-ross-http-1.5.0 (c (n "ross-http") (v "1.5.0") (d (list (d (n "regex-automata") (r "^0.1.10") (k 0)))) (h "0i3667dgm36knzdpq1yy3nf9yaimll4y6s33p8a66fah5m7p11sh") (y #t)))

(define-public crate-ross-http-1.6.0 (c (n "ross-http") (v "1.6.0") (h "07s9wfpy5bjw6gy37cl7v8d0g2b5yn0dxkv35zrzg5hl87wxcrzn") (y #t)))

(define-public crate-ross-http-1.7.0 (c (n "ross-http") (v "1.7.0") (h "114ra6h3a0l2fwhnqwgvmyzv5g1yzyfamfjcmf5q4afyvr0f9bxx") (y #t)))

(define-public crate-ross-http-1.8.0 (c (n "ross-http") (v "1.8.0") (h "08b76pkmy16dplvwpihvy8gyhk8ml04gbrqarwkbkc17f7vm3w4b") (y #t)))

