(define-module (crates-io ro qo roqoqo-mock) #:use-module (crates-io))

(define-public crate-roqoqo-mock-0.1.0 (c (n "roqoqo-mock") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.3") (d #t) (k 0)) (d (n "qoqo_calculator") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^0.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^0.1") (d #t) (k 0)))) (h "1sv0l3dll42a9yr6qiy00lr7w5wpc4rqw3096ivhy3zs5vw1a7ky")))

(define-public crate-roqoqo-mock-0.1.1 (c (n "roqoqo-mock") (v "0.1.1") (d (list (d (n "num-complex") (r "^0.3") (d #t) (k 0)) (d (n "qoqo_calculator") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^0.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^0.1") (d #t) (k 0)))) (h "0hgb1jk9gfa005kswnnrr0am6ynrz65f1dd66fbwwvxx4mgs4912")))

(define-public crate-roqoqo-mock-0.2.0 (c (n "roqoqo-mock") (v "0.2.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "qoqo_calculator") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^0.6") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^0.6") (d #t) (k 0)))) (h "0a1r0lv7idqs9qr5y2582rgxnd78rvvmj56mpfqnfb00hkqzw4n8")))

(define-public crate-roqoqo-mock-0.2.1 (c (n "roqoqo-mock") (v "0.2.1") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "qoqo_calculator") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^0.8.2") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^0.8.2") (d #t) (k 0)))) (h "05b76jl7hmjywl1r9b65pq60dxp88rkdaqdk78sxq2pwc8h14wll")))

(define-public crate-roqoqo-mock-0.2.2 (c (n "roqoqo-mock") (v "0.2.2") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "qoqo_calculator") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^0.9.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^0.9.0") (d #t) (k 0)))) (h "06vs4jlj4jggqcw1n33knl5p833xfvxxhyg70caw6rmbr6wvyvql")))

(define-public crate-roqoqo-mock-0.2.3 (c (n "roqoqo-mock") (v "0.2.3") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "qoqo_calculator") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^0.10.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^0.10.0") (d #t) (k 0)))) (h "1i55pg9yn5pjnszl9gki580ckj54b0gkwkka7g2l7r8spviaqm2x")))

(define-public crate-roqoqo-mock-0.4.4 (c (n "roqoqo-mock") (v "0.4.4") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "qoqo_calculator") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^0.10.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^0.10.0") (d #t) (k 0)))) (h "0w1mr9fnyyy01wrff4md3q9j1ryf284z605ibfrwwjmrvgxf4k5m")))

(define-public crate-roqoqo-mock-0.4.5 (c (n "roqoqo-mock") (v "0.4.5") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "qoqo_calculator") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^0.10.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^0.10.0") (d #t) (k 0)))) (h "134gb01lrlinc6bbqq64fmhfwsgpwl9gi7a34i99ad4q2b0w4i2x")))

(define-public crate-roqoqo-mock-0.4.6 (c (n "roqoqo-mock") (v "0.4.6") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "qoqo_calculator") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^0.10.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^0.10.0") (d #t) (k 0)))) (h "1cwmjr8ldsd732c2h0h84cvrssmpdcxjzngma85d66pfvm76m6br")))

(define-public crate-roqoqo-mock-0.5.0 (c (n "roqoqo-mock") (v "0.5.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "qoqo_calculator") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^1.0.0-alpha.5") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^1.0.0-alpha.5") (d #t) (k 0)))) (h "121ndmks946q4br0r4z4jfglcnfpknrjg67ff33x4l03cn6vjndf")))

(define-public crate-roqoqo-mock-0.5.1 (c (n "roqoqo-mock") (v "0.5.1") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "qoqo_calculator") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^1.0.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^1.0.0") (d #t) (k 0)))) (h "068damc37qihfxvap7w4aga8s4wd2q00vvlc534hjxsdr2m36pmf")))

(define-public crate-roqoqo-mock-0.5.2 (c (n "roqoqo-mock") (v "0.5.2") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "qoqo_calculator") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^1.0.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^1.0.0") (d #t) (k 0)))) (h "0l9ygcmv1a6nc7v10r43vfsxagpx2fvbxqq0hcnz9j3wpj881wvs")))

(define-public crate-roqoqo-mock-0.5.3 (c (n "roqoqo-mock") (v "0.5.3") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^1.2.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^1.2.0") (d #t) (k 0)))) (h "0qbavd2ddk2z6g37wwgvlri39yww8qr1drpvswmxck6zy8s44paj")))

(define-public crate-roqoqo-mock-0.5.4 (c (n "roqoqo-mock") (v "0.5.4") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^1.3") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^1.3") (d #t) (k 0)))) (h "15074mf826g7k61af1rmd39ykrdkjgyqagrvs63531xyn9kccjw9")))

(define-public crate-roqoqo-mock-0.5.5 (c (n "roqoqo-mock") (v "0.5.5") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^1.4") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^1.4") (d #t) (k 0)))) (h "03dhm57l2j619k1smjsi2m28pan1ny092x1rb5inhspff4p835sl")))

(define-public crate-roqoqo-mock-0.5.6 (c (n "roqoqo-mock") (v "0.5.6") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^1.5") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^1.5") (d #t) (k 0)))) (h "0zlncw4l7xj5nqz2x4b6jgxrfd6jjkfbcvf9bb3wkl3x4d0rga32")))

(define-public crate-roqoqo-mock-0.5.7 (c (n "roqoqo-mock") (v "0.5.7") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "roqoqo") (r "^1.8") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "roqoqo-test") (r "^1.8") (d #t) (k 0)))) (h "0v70a17xxmmjwg4q3pmybsvj46fz2ivxghlrqa8cadw26h88b90v")))

