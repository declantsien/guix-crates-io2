(define-module (crates-io ro qo roqoqo-derive) #:use-module (crates-io))

(define-public crate-roqoqo-derive-0.1.0 (c (n "roqoqo-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qxwyrz31d81w2n8nfw595vf38fs986qdzj2cz1gskpw8rzc2qd3") (f (quote (("overrotate") ("default"))))))

(define-public crate-roqoqo-derive-0.1.1 (c (n "roqoqo-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11bbk1assx4lzavrs853fd4sz3c9lbp3nyf466f966vri3bqh1dl") (f (quote (("overrotate") ("default"))))))

(define-public crate-roqoqo-derive-0.1.2 (c (n "roqoqo-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dmc9n6bbcifwsj6z8c9cw8n5hipys43hncp48awfs0w97r93sxa") (f (quote (("overrotate") ("default"))))))

(define-public crate-roqoqo-derive-0.1.3 (c (n "roqoqo-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "09h08ar24qsv0g5zqr4mrdhj85080snyj0x6jxf0qcv8xvciyz3h") (f (quote (("overrotate") ("default"))))))

(define-public crate-roqoqo-derive-0.1.5 (c (n "roqoqo-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0kk3zk54zap3xh09c3vrrzwr72sydmfvs6rprxf8xac4v70dw0hm") (f (quote (("overrotate") ("default"))))))

(define-public crate-roqoqo-derive-0.5.0 (c (n "roqoqo-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "04i3g00s1k19ynz5sv63y6brs453jkgv1dnka8dmhxv9fg3xcwy8") (f (quote (("overrotate") ("default"))))))

(define-public crate-roqoqo-derive-0.5.1 (c (n "roqoqo-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0nv15fhdlbqmvv86f4cffmwzgvnlqhdl6zhlcxc75871f2kn9bfs") (f (quote (("overrotate") ("default"))))))

(define-public crate-roqoqo-derive-0.6.0 (c (n "roqoqo-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0cqch4xckbsndnjp44yw6plgh0c1drkiky3q65v2cgjq211jf7nh") (f (quote (("overrotate") ("default"))))))

(define-public crate-roqoqo-derive-0.6.1 (c (n "roqoqo-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "004g7j1zgwx4gz6im1qflkv40fnzx9iqahll9kb2gx6ji0rjj52n") (f (quote (("overrotate") ("default"))))))

(define-public crate-roqoqo-derive-0.6.2 (c (n "roqoqo-derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "120n6hqd9bkh9m9sgjqszkdp5p23gr00i2dzrf0pfr91clin19pz") (f (quote (("overrotate") ("default"))))))

(define-public crate-roqoqo-derive-0.6.3 (c (n "roqoqo-derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1966xrzrm4vvinggg1kqm209q0i2jqjw98r35q2188qdwcx0c0kp") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-0.7.0 (c (n "roqoqo-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "18cvxgwcqg992nr4xzvqvs1s892jgqqq4d3q0fqq9vsp1mf52i0w") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-0.8.0 (c (n "roqoqo-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "17jjh954znllana1avhd36sddx4adw0pfyx17fdy6288nldgxg4s") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-0.8.1 (c (n "roqoqo-derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "044wv4693gy1pzkvhr54dvqahb0byv1722wagb29nfr344pcvbx4") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-0.8.2 (c (n "roqoqo-derive") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1m1f8a81q2amlm1knyyiyy5vdrhfhfz9i3gn8ayplsjy8aj12n41") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-0.9.0 (c (n "roqoqo-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1s3wx3dgcafnry95i1z3lir1mksvzpp3m3pq4iq9hc3zqwjjq8kn") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-0.10.0 (c (n "roqoqo-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1097vc2dgfjanyw0hgai53kf0zvmr9hvbpp1x1cf3n5jxjv75392") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-0.11.0 (c (n "roqoqo-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0ah2v7n2ban35bpaxbsbn00ngbc7n266ynh2mmrzbn40jlqnb4cd") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-0.11.1 (c (n "roqoqo-derive") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1g2dw5m9id49fl62fyiz55n20b1pgpp51w8d0i7i7rwqahpnzgs2") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-0.11.3 (c (n "roqoqo-derive") (v "0.11.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0bz0f4aznafnhpgmqvizmi43yqhldrf0am9h8i19l4cmrsd8mabr") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.0.0-alpha (c (n "roqoqo-derive") (v "1.0.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "14wny2zrvdw8hh4l7iq12sldzaim444qbzni9rrnn2dgb440ygnv") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.0.0-alpha.1 (c (n "roqoqo-derive") (v "1.0.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1gia564vmcd4dzq40v8xwn15hw8rcxjwdh4i1p3gx40jl788ibfn") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.0.0-alpha.2 (c (n "roqoqo-derive") (v "1.0.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0azyj2jd9f8js73r5vrf96ycqj81gkqc4xn9jxawgszksrp0kz51") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.0.0-alpha.3 (c (n "roqoqo-derive") (v "1.0.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1giqvzrasvx78jsn70gdy93qrvk7p2bn9q1m1ny24gapwd7b3ci6") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.0.0-alpha.4 (c (n "roqoqo-derive") (v "1.0.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "004r1bz2d11h0bscn9d8654r20w951l8maach3qjp5cbkmbxg4sy") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.0.0-alpha.5 (c (n "roqoqo-derive") (v "1.0.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0d7izaac1ccpbq5dawnqs07m07d3s6d211nqzdhn9j1fd9d3adk7") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.0.0-beta.1 (c (n "roqoqo-derive") (v "1.0.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "11a441nliva58dy8k5jmdnmikj9adkxrwqdbl16rz1b1b68ik3pm") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.0.0-beta.2 (c (n "roqoqo-derive") (v "1.0.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1zwqckjgpzjbl4k0m1i25v9c4cw1kiszsl5xp25pf09j6wr2v5c6") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.0.0 (c (n "roqoqo-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0q2hzs19kw0khdzbzz445jys3ipx9r4mf6v7bcabhlr5dxn756f7") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.1.0-beta.1 (c (n "roqoqo-derive") (v "1.1.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0x37ivvzmavhaxswrbp0l0pwa5jnds1i07ikrmms28wzmmiid6wn") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.1.0-beta.2 (c (n "roqoqo-derive") (v "1.1.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1x0r9dgyvr5wkrpkgwwrdv2lhq1s902bd8bjiqx9g4czysl5h9w5") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.1.0-beta.3 (c (n "roqoqo-derive") (v "1.1.0-beta.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "12yj96rpsni3yf5v75i0p9v07rk4swd03hwzma2ci1g5rxnkvci3") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.1.0-beta.4 (c (n "roqoqo-derive") (v "1.1.0-beta.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "08236pq3p8vz8bd1l3941djzk1lsn86qz0bma5bxdf71dbj412sg") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.1.0-beta.5 (c (n "roqoqo-derive") (v "1.1.0-beta.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0pmqfv1jcll4jmllq7mw9kgqwgmvafk3ii94z069gfrl2k18dc97") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.1.0-beta.6 (c (n "roqoqo-derive") (v "1.1.0-beta.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "09ni25wrl4f3zrxwsys2mrpz2h6ay0b70h1dncmdclar8cslpm82") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.1.0-beta.7 (c (n "roqoqo-derive") (v "1.1.0-beta.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "test_roqoqo_derive_1_0") (r "=1.0.0") (o #t) (d #t) (k 0) (p "roqoqo-derive")))) (h "0z44mc7ylqcdd8b940dlqzbc6sammvahqd7x7xpd0zh2xr8idasz") (f (quote (("overrotate") ("default") ("compatibility" "test_roqoqo_derive_1_0")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.1.0-beta.8 (c (n "roqoqo-derive") (v "1.1.0-beta.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "061cmr60j1zghismb3g440bxn1c1chk79gspaqf38h082syshn7x") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.1.0-beta.9 (c (n "roqoqo-derive") (v "1.1.0-beta.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0vjdcpas0ayzsxxz1dfnkkqfy7rcj8wbj1cwm9acy7lwhiy7dlaq") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.1.0-beta.10 (c (n "roqoqo-derive") (v "1.1.0-beta.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0mvxgafg0m1qkx0mydwmbnl6z8sfxd6k4drgzh8ibcsznjcap2ix") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.1.0-beta.12 (c (n "roqoqo-derive") (v "1.1.0-beta.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1s56mvczdpd3vb4ngvxhiippylid3i1ya89w4amr91c46wd8wbf0") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.1.0 (c (n "roqoqo-derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1248qdxiqa5qbymaahcx5zhs9yqd7a1ilgw1jryazgx01irl67q6") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.2.0-alpha.1 (c (n "roqoqo-derive") (v "1.2.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0469amhqa7286x9j9yk2car3xgmsmwb2q3fxvkf4fvm6wg9cvp6z") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.2.0-alpha.2 (c (n "roqoqo-derive") (v "1.2.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "09pqdzw16qm4fn0axlzqlzhsh2anb23dfwb5li8nkgc28kq6hi12") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.2.0-alpha.3 (c (n "roqoqo-derive") (v "1.2.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1ggx19d2pd19qhj0f25azda9g3m3fnpq15wgvplxf2dy5f4jipag") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.2.0 (c (n "roqoqo-derive") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1dy314bs6cnddxj65m29bqy493pmrg4csyya3d7j7amgzppvfgy2") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.2.1 (c (n "roqoqo-derive") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0i1pkqg4dmiswd8az56f5nild9ncj5lvrkh7m1rqb3m2as1dv197") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.2.2 (c (n "roqoqo-derive") (v "1.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "11mqa9jgxv24hyjqnw0d96gn8vcxn5wxxbqrmra8zcx50ygbz0vm") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.2.3 (c (n "roqoqo-derive") (v "1.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0s7p720jbrpppvqp1z1dsghr4abkpgpjany4kvmqwfp3q9jfsjaz") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.2.4 (c (n "roqoqo-derive") (v "1.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0v5f3zcslamkr4z7jzfmbcrk7mx0yz4b4b1l7kxc27d6ayis2k4x") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.2.5 (c (n "roqoqo-derive") (v "1.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0m4dyvc8i3rw1qlvmla9rsw2bbzm17q4wa332qd1zz379yfrbzyl") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.3.0 (c (n "roqoqo-derive") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "02617vxn38y5m7x3niashayy2w6rcnnibfpq54zwgc9r0wbdq3r4") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.3.1 (c (n "roqoqo-derive") (v "1.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0zy7zfi0f573b98z2qadmj26nkf5n4ygvrg4licrxxlhwkjwnj1f") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.3.2 (c (n "roqoqo-derive") (v "1.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1r1dwz4cfwxak3ipbgf76iw2yzrxxvimgw7csnr29sdi8c9jzwxy") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.4.0 (c (n "roqoqo-derive") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0i5bvgh43bgkl570r6n3wp68smbybshij74nk5a92lyccr10qrkl") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.5.0 (c (n "roqoqo-derive") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1mjx6pawici89138fv6ai7kvz3faifk2plgch1almi2f91qds2zy") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.6.0-alpha.0 (c (n "roqoqo-derive") (v "1.6.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0zszk9qs5q8nkgq1w3l2qn0khcdqfhv6l5qdgnsclqwqn74q63j6") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.5.1 (c (n "roqoqo-derive") (v "1.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "033j064njdsfas4j5py2fnn87ki3ydxh69y4wz11fa9jm9h5l4gc") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.6.0-alpha.1 (c (n "roqoqo-derive") (v "1.6.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1cjm84isx86z20n06l63m8552a7n7dbaqqzqw0dpqmja9vi6x1w3") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.6.0-alpha.2 (c (n "roqoqo-derive") (v "1.6.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1q5nbh4cfdal4h5sanfk7v2cimqad69w4rndyzk4c7l41kz4yzcr") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.6.0 (c (n "roqoqo-derive") (v "1.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "17qxc9j9m7v7qyp38h36110g1gn6cl1sl1sjvx5cvf07znfg4apw") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.6.1 (c (n "roqoqo-derive") (v "1.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0i5l7d7qb3qf7v2fw8z3njzmysmpssdqi15dkqkijz5qsphf5wcc") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.6.2 (c (n "roqoqo-derive") (v "1.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "080cl5vxfdbyknrklw45iyvkgf0zb4vs4gz3cnw9pfaicd64bmxm") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.7.0 (c (n "roqoqo-derive") (v "1.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0mgacy92xcpj02y1di5fp6yglvnmciwcaq6z36xjiy902wmcin99") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.7.1 (c (n "roqoqo-derive") (v "1.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "09fby0yc156cndb5p4pwgkwa2q1am1rr03ndyf59j03vckxvmf0d") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.8.0-alpha.0 (c (n "roqoqo-derive") (v "1.8.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "08bhlhqj9759c2zbfliiizpvv3jrmbrrrnznfi4dq5vywsv67sn2") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.8.0-alpha.1 (c (n "roqoqo-derive") (v "1.8.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "02zvc6wahs979vc9ddsada3akwkpz7wlsxww7h6f093sgx2p6ckw") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.8.0-alpha.2 (c (n "roqoqo-derive") (v "1.8.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1alfrk5biqcndvlk5zwnkj7aax7bf3qh3p4dr66dcdyfqkngd5n2") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.8.0 (c (n "roqoqo-derive") (v "1.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1lvz3vk38ywxc3hzwda3ms5xfq786fdyvdyy4s90ica7r654m611") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.9.0 (c (n "roqoqo-derive") (v "1.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1kc10zdi8d1gsxzfd05qfxjczlxfgn73c3kl9m05ih7hiy7p4im2") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.9.1 (c (n "roqoqo-derive") (v "1.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "09250ir0adpixw7mr9hcirkbngqq5xh0ksnvj3qbq8qgsfq3mkd2") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.10.0 (c (n "roqoqo-derive") (v "1.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "091zzj1gb1m1p4ivhylrc13zzigg2m9klwizml8khd58gq2pz5ib") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.11.0-alpha.0 (c (n "roqoqo-derive") (v "1.11.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0s7cfzid7615ack8h7niddzar7cwngr6xm66r2cf5fgzygmvfrjc") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.11.0-alpha.1 (c (n "roqoqo-derive") (v "1.11.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0w7m68fs7jamngb380vz7b45l3sjf30kn0pixkdphq96ac1nf5ms") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.11.0-alpha.2 (c (n "roqoqo-derive") (v "1.11.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "031gn31in00lhc9lfjp6baxw5hxwfw5xc7drjk8v4k6vb5myxg52") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.11.0 (c (n "roqoqo-derive") (v "1.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1xfsp0yvmryyxysxlpalg5w9rj6vsq76ygwlplhppj18kvri2jjf") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.12.0 (c (n "roqoqo-derive") (v "1.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "132vnqqfjakyjhyqzn11jmsjc18655yzck4py5nd3zjzy83bbg4r") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.11.1 (c (n "roqoqo-derive") (v "1.11.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1zxlwgr7bv849blk81bsvwh1vmnc0yz51laxkxwb7qz1a8iflif4") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.10.1 (c (n "roqoqo-derive") (v "1.10.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0fpy2sp83jp8iijjg1l7k42j7pafh5h04lc09zzdl9d2d1jamr90") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.9.2 (c (n "roqoqo-derive") (v "1.9.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0vgcc8zpya4s78sap4wrz3jbizwi2m2xadbdig4ry1qk9ish2dyn") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.12.1 (c (n "roqoqo-derive") (v "1.12.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "04dqid229lv76nkcs6936im9g0pkf8hppnx867lx438nlg7jj0rm") (f (quote (("overrotate") ("default")))) (r "1.56")))

(define-public crate-roqoqo-derive-1.13.0 (c (n "roqoqo-derive") (v "1.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0h3ir13qm975nl9irdgql3wcj215qyi10yymihnw3lw3d5gm733i") (f (quote (("overrotate") ("default")))) (r "1.56")))

