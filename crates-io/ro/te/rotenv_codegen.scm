(define-module (crates-io ro te rotenv_codegen) #:use-module (crates-io))

(define-public crate-rotenv_codegen-0.15.0 (c (n "rotenv_codegen") (v "0.15.0") (d (list (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rotenv") (r "^0.15") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0y0v8dyq3kfm71px5gzn81j592nd1h55wrqbpkc9h66n2fkc8k9y")))

