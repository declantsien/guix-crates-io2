(define-module (crates-io ro tu rotuli) #:use-module (crates-io))

(define-public crate-rotuli-0.0.1 (c (n "rotuli") (v "0.0.1") (d (list (d (n "document_tree") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "rst_parser") (r "^0.3") (d #t) (k 0)) (d (n "rst_renderer") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tera") (r "^1.3") (d #t) (k 0)))) (h "0ya5b6vaqs42mx18ihgraabj3x57ldlb4ppm6lw779ij6vsxnrpx")))

