(define-module (crates-io ro g_ rog_fan_curve) #:use-module (crates-io))

(define-public crate-rog_fan_curve-0.1.0 (c (n "rog_fan_curve") (v "0.1.0") (h "0bs0wlk73dbv4skbx87c6wps866cngfmkxsgpwwq3yr65z90grd2")))

(define-public crate-rog_fan_curve-0.1.1 (c (n "rog_fan_curve") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "04rqfvsiyhpmkv71p55ikhgskj38ifq491kq10fa9hqihh09qc35")))

(define-public crate-rog_fan_curve-0.1.2 (c (n "rog_fan_curve") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "12av38c8xhl50g0mcb0r77acjcw0afr13kpygvnpr1hwwggwk1ny")))

(define-public crate-rog_fan_curve-0.1.3 (c (n "rog_fan_curve") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "002kr0m895anw0yrs6pbf7097hzxlp7zp7i27md3zss78n5dklyg")))

(define-public crate-rog_fan_curve-0.1.4 (c (n "rog_fan_curve") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "1ff5jmvs7n34ip5jmmj0sfr3w9hah4r6p4gb4vzz9agmyf2apvrq")))

(define-public crate-rog_fan_curve-0.1.5 (c (n "rog_fan_curve") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "0gwqr68g1hk3g76ibir3d9c0jp1f7kxv5s7bdrygayzl1mx8dlxr")))

(define-public crate-rog_fan_curve-0.1.6 (c (n "rog_fan_curve") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "1aanz1z5w1q82fy851kpsim8ynss0wr6mjiyv1sah7fld95gwxys")))

(define-public crate-rog_fan_curve-0.1.7 (c (n "rog_fan_curve") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "1jvkcbl4mpb5m2xi1pdm5cgv3vswrsc4xspgmsb4f40ply8zw767")))

(define-public crate-rog_fan_curve-0.1.8 (c (n "rog_fan_curve") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "1wp2h7ypsb1v0x399ppn95iil887xiymbzz7h9cd1096dsfi90yh")))

(define-public crate-rog_fan_curve-0.1.9 (c (n "rog_fan_curve") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "1sww8b4w7vhf2cilfzwk3shwq4c5jgdfbjqh1nrq92gyjqnwxv7i")))

(define-public crate-rog_fan_curve-0.1.10 (c (n "rog_fan_curve") (v "0.1.10") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "147fk578vr2yiyvi6gch7xbp1x69fqc1mflqj0fz67fm6ribh458")))

(define-public crate-rog_fan_curve-0.1.11 (c (n "rog_fan_curve") (v "0.1.11") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "19l37my3x2fv71llf25khqvam6k62k63jqagwkvas32h2z9dnhmf")))

