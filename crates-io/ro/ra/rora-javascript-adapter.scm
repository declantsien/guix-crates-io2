(define-module (crates-io ro ra rora-javascript-adapter) #:use-module (crates-io))

(define-public crate-rora-javascript-adapter-0.0.1 (c (n "rora-javascript-adapter") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1sm450v161c8wlkbvx20viqrslwijdrp3vqnjgql3s36hfvpcgxs")))

(define-public crate-rora-javascript-adapter-0.0.2 (c (n "rora-javascript-adapter") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "09wzai0blkv7w7wjyq0grxqdw3798x31mydf7myk1ryrjf4ylgv7")))

