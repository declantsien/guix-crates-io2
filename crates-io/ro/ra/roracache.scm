(define-module (crates-io ro ra roracache) #:use-module (crates-io))

(define-public crate-roracache-0.0.0-b0 (c (n "roracache") (v "0.0.0-b0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "fuser") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liso") (r "^1.0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "06qr66vfmcfyha1qnhd9msg96f2rbs24gfd397nvl5kybr9s1y61")))

(define-public crate-roracache-0.0.0-b1 (c (n "roracache") (v "0.0.0-b1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "fuser") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liso") (r "^1.0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0l4c8awi0whxpzl3rldwad71dml226f1nplvhd5qncwk3ag4ii6m")))

