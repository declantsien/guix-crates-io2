(define-module (crates-io ro ae roaes) #:use-module (crates-io))

(define-public crate-roaes-0.1.1 (c (n "roaes") (v "0.1.1") (d (list (d (n "aes") (r "^0.7.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rhexdump") (r "^0.1.1") (d #t) (k 2)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "typenum") (r "^1.14.0") (d #t) (k 0)))) (h "1yzds5r2i06l1qp79q2f3si8fd9krdvh1jfb3d04bc4v5g2jcryc")))

(define-public crate-roaes-0.1.2 (c (n "roaes") (v "0.1.2") (d (list (d (n "aes") (r "^0.7.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rhexdump") (r "^0.1.1") (d #t) (k 2)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "typenum") (r "^1.14.0") (d #t) (k 0)))) (h "0l1ml5hpm0a8b4ckayd6ihhca3nhc4msb26lvsizi8j9m36g4fch")))

