(define-module (crates-io ro sv rosvgtree) #:use-module (crates-io))

(define-public crate-rosvgtree-0.1.0 (c (n "rosvgtree") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18") (d #t) (k 0)) (d (n "simplecss") (r "^0.2") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "svgtypes") (r "^0.9") (d #t) (k 0)) (d (n "xmlwriter") (r "^0.1") (d #t) (k 2)))) (h "0l6i81zfcm1kh1a4jc40ivp622yqiiqg05kx7havimh3rqd3vhmx")))

(define-public crate-rosvgtree-0.2.0 (c (n "rosvgtree") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18") (d #t) (k 0)) (d (n "simplecss") (r "^0.2") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "svgtypes") (r "^0.11") (d #t) (k 0)) (d (n "xmlwriter") (r "^0.1") (d #t) (k 2)))) (h "1wrrhxgf2853ggx9rjd9hsnn9ni2njv4lnsqqalchp1w02b7knwc")))

(define-public crate-rosvgtree-0.3.0 (c (n "rosvgtree") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18") (d #t) (k 0)) (d (n "simplecss") (r "^0.2") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "svgtypes") (r "^0.11") (d #t) (k 0)) (d (n "xmlwriter") (r "^0.1") (d #t) (k 2)))) (h "15yl8cbws8nbq9kpla6mqv0497xdgcxl7fhm7grpn3llhirpwx5d")))

