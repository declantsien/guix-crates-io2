(define-module (crates-io ro sv rosv) #:use-module (crates-io))

(define-public crate-rosv-1.0.1 (c (n "rosv") (v "1.0.1") (h "0ga1hrhrnd87x7l5ci1baxzbd1rrin0a5ym59dgj7j5bzrk5jqk9")))

(define-public crate-rosv-1.0.2 (c (n "rosv") (v "1.0.2") (h "0xvbd297dqamg7wyxjvbwbjqpp0x1hi3yjys5scm4lxyn24gki37")))

(define-public crate-rosv-1.0.3 (c (n "rosv") (v "1.0.3") (h "0lzwlijn0l2qpzfnpb2hrky1ymrg8xjhsycggx6w6bh5b93qs0ir")))

(define-public crate-rosv-1.1.0 (c (n "rosv") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 2)) (d (n "error-stack") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "02c57ll7prw94b2vvlvvwjg5d4q7dw7q6b9qjzgfljiijxylz41c") (f (quote (("default" "fs")))) (s 2) (e (quote (("fs" "dep:error-stack"))))))

