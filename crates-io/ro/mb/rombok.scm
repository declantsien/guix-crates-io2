(define-module (crates-io ro mb rombok) #:use-module (crates-io))

(define-public crate-rombok-0.1.0 (c (n "rombok") (v "0.1.0") (d (list (d (n "rombok_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1w9mb0s0v0arn970hdakhnbxw6f5wwxxix8yq4mjh1hrf78qggc1")))

(define-public crate-rombok-0.1.1 (c (n "rombok") (v "0.1.1") (d (list (d (n "rombok_macro") (r "^0.1.1") (d #t) (k 0)))) (h "02hsw8khqrjzbxldcn7zacyw4rgfis3779viz83bsr24kx3sia28")))

(define-public crate-rombok-0.1.2 (c (n "rombok") (v "0.1.2") (d (list (d (n "rombok_macro") (r "^0.1.1") (d #t) (k 0)))) (h "05xd3vqi5mwizs6ih19rzkclm1zs0jr3hq13f6nwk7phmdfwngh3")))

(define-public crate-rombok-0.2.0 (c (n "rombok") (v "0.2.0") (d (list (d (n "rombok_macro") (r "^0.2.0") (d #t) (k 0)))) (h "1sn5z7y9jx762qmi9b3r8vnp4kcpllzimrl7aw8s2b2xma6lcggy")))

(define-public crate-rombok-0.3.0 (c (n "rombok") (v "0.3.0") (d (list (d (n "rombok_macro") (r "^0.3.0") (d #t) (k 0)))) (h "0396fd1kvc97y5mb0ap24yaqpl4bi50bny960p3bd09kjfdyn7y6")))

