(define-module (crates-io ro n_ ron_to_table) #:use-module (crates-io))

(define-public crate-ron_to_table-0.1.0 (c (n "ron_to_table") (v "0.1.0") (d (list (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "tabled") (r "^0.11") (f (quote ("std"))) (k 0)))) (h "16gwxzqg7w0wwvljrjfpizswpzmi5fb2qi013ay949msjxclm5cd") (f (quote (("color" "tabled/color"))))))

(define-public crate-ron_to_table-0.2.0 (c (n "ron_to_table") (v "0.2.0") (d (list (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "tabled") (r "^0.12") (f (quote ("std"))) (k 0)))) (h "1hrw1h5i28kibnkdmqpy3had84l1bqm3ca5x3jfwxcw97j01bf48") (f (quote (("color" "tabled/color"))))))

(define-public crate-ron_to_table-0.3.0 (c (n "ron_to_table") (v "0.3.0") (d (list (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "tabled") (r "^0.15") (f (quote ("std"))) (k 0)))) (h "0dkmjs8jg926akvry4qzbprqjq2icqscx73w5pfzvl7cl7xxgizk") (f (quote (("macros" "tabled/macros") ("derive" "tabled/derive") ("ansi" "tabled/ansi"))))))

