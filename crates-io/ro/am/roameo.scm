(define-module (crates-io ro am roameo) #:use-module (crates-io))

(define-public crate-roameo-0.1.1 (c (n "roameo") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "network-interface") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("net"))) (d #t) (k 0)) (d (n "socket2") (r "^0.4") (d #t) (k 0)))) (h "07wg3ybqq0qb0pmbfi7m4am50w9sk2y1nwnqhfccrhh97g6lb3sx")))

(define-public crate-roameo-0.2.1 (c (n "roameo") (v "0.2.1") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "network-interface") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("net"))) (d #t) (k 0)) (d (n "socket2") (r "^0.4") (d #t) (k 0)))) (h "04aq5iicmj1ffd2anb89vycdg3kd20w8c6kzch9n0n28dq0rxysy")))

