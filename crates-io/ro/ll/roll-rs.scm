(define-module (crates-io ro ll roll-rs) #:use-module (crates-io))

(define-public crate-roll-rs-0.1.0 (c (n "roll-rs") (v "0.1.0") (d (list (d (n "bnf") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "1nyn19xzj70i7qvwx7srawnk11f5xd7qn4n8nwzb4grg9hzhnh3h")))

(define-public crate-roll-rs-0.2.0 (c (n "roll-rs") (v "0.2.0") (d (list (d (n "bnf") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "0ja51zx4fsdcp5c6cbbjqk7qwwvczgbhm7nd9wlzz64k5v718z00")))

(define-public crate-roll-rs-0.3.0 (c (n "roll-rs") (v "0.3.0") (d (list (d (n "bnf") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "1dbzi9hcgjzn6a2rixvhyc6sc2n1cbwj77ajjps7pqm9006qk11p")))

