(define-module (crates-io ro ll rolldown_error) #:use-module (crates-io))

(define-public crate-rolldown_error-0.0.1 (c (n "rolldown_error") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "sugar_path") (r "^0.0.11") (d #t) (k 0)))) (h "01dq92jzfw7632ah7y23xwvhrz3wxpkmcmcvnk9nv8hbmd1a74y3")))

