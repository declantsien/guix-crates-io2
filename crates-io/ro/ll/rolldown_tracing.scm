(define-module (crates-io ro ll rolldown_tracing) #:use-module (crates-io))

(define-public crate-rolldown_tracing-0.0.1 (c (n "rolldown_tracing") (v "0.0.1") (d (list (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0i9c26ziajx3yj9lw03zp599nj66vfxdarh8snc0jjzrdi02wqwg")))

