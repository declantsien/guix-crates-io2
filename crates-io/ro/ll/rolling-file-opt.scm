(define-module (crates-io ro ll rolling-file-opt) #:use-module (crates-io))

(define-public crate-rolling-file-opt-1.0.0 (c (n "rolling-file-opt") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0h860c44mnwf47z3rprjwqkqcs4wrfb4lgr8cwgmmj2qw62hb5ip") (y #t)))

(define-public crate-rolling-file-opt-1.0.1 (c (n "rolling-file-opt") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "05f22n3s7ywxaazf27n234rljspy20jqqijijqg3hjd2k4l623yv") (y #t)))

(define-public crate-rolling-file-opt-1.0.2 (c (n "rolling-file-opt") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "08nvi4j1mfyadckwgwymqh3p0bbfwnbdc5vq09p402nzldlmk25i") (y #t)))

(define-public crate-rolling-file-opt-1.0.3 (c (n "rolling-file-opt") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1k56mza2bl40ri7nl2aq9wxy1xzdvy7vlgy0x53ngry8s48ajyzb") (y #t)))

(define-public crate-rolling-file-opt-1.0.4 (c (n "rolling-file-opt") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0lc6q2i0kbhfw00yypcsrljj3nqmrdd2ffwv4chqbswp57kxrj2q")))

