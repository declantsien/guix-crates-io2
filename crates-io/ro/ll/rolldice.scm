(define-module (crates-io ro ll rolldice) #:use-module (crates-io))

(define-public crate-rolldice-0.1.0 (c (n "rolldice") (v "0.1.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0ca782wx8yh6slg9kn32dfc2148yb310rrhjs1rs1mhy750kfn9y")))

(define-public crate-rolldice-0.1.1 (c (n "rolldice") (v "0.1.1") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "05h7zfmm3mplckbqp4xl49cky41gwy1j41i6i0fcf2mglc3hxzxj")))

(define-public crate-rolldice-0.1.2 (c (n "rolldice") (v "0.1.2") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1wnfiw65ff75dxzzd3n11xd78hx31m5354cmnwv9n8wrlr6ydjb6")))

(define-public crate-rolldice-0.1.3 (c (n "rolldice") (v "0.1.3") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "19d4hjj7s3vsajv15qklgaxh83gdrb7pw4fbqv9ap4gaka5vwp3i")))

(define-public crate-rolldice-0.1.4 (c (n "rolldice") (v "0.1.4") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0gixnlmphg9jszlzwrqfas8g5nkzkj5hdx40xzvlrr4mznn0n0h7")))

