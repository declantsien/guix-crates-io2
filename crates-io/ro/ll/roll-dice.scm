(define-module (crates-io ro ll roll-dice) #:use-module (crates-io))

(define-public crate-roll-dice-0.1.0 (c (n "roll-dice") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17p35fjin6yqr90qgyhphnwkg776ykz7qk431k9rypgbn4rag1qj")))

(define-public crate-roll-dice-0.2.0 (c (n "roll-dice") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0493grji09pl88209kbzlqn89pybr96r2l04cn742p39nyyb3z4v")))

