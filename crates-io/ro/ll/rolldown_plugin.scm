(define-module (crates-io ro ll rolldown_plugin) #:use-module (crates-io))

(define-public crate-rolldown_plugin-0.0.1 (c (n "rolldown_plugin") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.62") (d #t) (k 0)) (d (n "rolldown_common") (r "^0.0.1") (d #t) (k 0)) (d (n "rolldown_error") (r "^0.0.1") (d #t) (k 0)))) (h "1588rknvwiwjhmc530bmswi0yzxviajqj1awmj5m9ckxv7b9sd7x")))

