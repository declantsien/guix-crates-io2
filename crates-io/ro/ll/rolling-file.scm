(define-module (crates-io ro ll rolling-file) #:use-module (crates-io))

(define-public crate-rolling-file-0.1.0 (c (n "rolling-file") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1wafflnfavc7injrri6ms2hgr5rhfncwza3ci4xk9hf1kgl59cir")))

(define-public crate-rolling-file-0.2.0 (c (n "rolling-file") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "01m94rh928myc7phbksq4qdfh8yqshnfm5m2407p8sw5c3wb95c3")))

