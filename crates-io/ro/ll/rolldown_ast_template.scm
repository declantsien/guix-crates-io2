(define-module (crates-io ro ll rolldown_ast_template) #:use-module (crates-io))

(define-public crate-rolldown_ast_template-0.0.1 (c (n "rolldown_ast_template") (v "0.0.1") (d (list (d (n "swc_core") (r "^0.59.26") (f (quote ("ecma_ast" "common" "common_concurrent" "ecma_utils"))) (d #t) (k 0)))) (h "1957xi15z0p2r9v4knb27m69i5n3d01hj243ad4vqpx88wlw9pzw")))

