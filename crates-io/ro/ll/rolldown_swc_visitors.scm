(define-module (crates-io ro ll rolldown_swc_visitors) #:use-module (crates-io))

(define-public crate-rolldown_swc_visitors-0.0.1 (c (n "rolldown_swc_visitors") (v "0.0.1") (d (list (d (n "hashlink") (r "^0.8.1") (d #t) (k 0)) (d (n "rolldown_common") (r "^0.0.1") (d #t) (k 0)) (d (n "rolldown_error") (r "^0.0.1") (d #t) (k 0)) (d (n "rolldown_swc_utils") (r "^0.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_core") (r "^0.59.26") (f (quote ("ecma_visit" "ecma_ast" "ecma_minifier" "common" "common_concurrent" "ecma_transforms_optimization" "ecma_transforms_module" "ecma_utils"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "16s1ij12wm4wz3xr4kq3gz3g0d96vh71r7fdicy9mmvjcj4indgq")))

