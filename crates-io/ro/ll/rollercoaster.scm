(define-module (crates-io ro ll rollercoaster) #:use-module (crates-io))

(define-public crate-rollercoaster-0.0.1 (c (n "rollercoaster") (v "0.0.1") (h "1gixq0b7n6l2yqvk697b2v64dhpl8vni1s3bgf9cvf18p7dl85ad")))

(define-public crate-rollercoaster-0.0.2 (c (n "rollercoaster") (v "0.0.2") (h "1yqlvjkdfk8yhinvhgnj647w74yxchdsfw1lx1g2nzp69084b1f2")))

(define-public crate-rollercoaster-0.0.3 (c (n "rollercoaster") (v "0.0.3") (h "1lh2s6pyh6l2719zc9jfh9mbl0x0kly9pgwq328dl8nisbzbgp05")))

(define-public crate-rollercoaster-0.0.4 (c (n "rollercoaster") (v "0.0.4") (h "1s4fhhs45lyqw0hqrswfzv7kbkmk0nwwsn7yw0dbaa13lf1ipa93")))

