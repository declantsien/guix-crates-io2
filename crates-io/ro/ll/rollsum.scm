(define-module (crates-io ro ll rollsum) #:use-module (crates-io))

(define-public crate-rollsum-0.1.0 (c (n "rollsum") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1picwv0dk8r87fza0bbh51gw6hbpz4yzyq4axrlc518v9r0pgnp4")))

(define-public crate-rollsum-0.2.0 (c (n "rollsum") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0fqk8jq3fw3lp1yp205rwkwb2irix3yinyqjqjsbfzr1l0ks6lvw")))

(define-public crate-rollsum-0.2.1 (c (n "rollsum") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0z1m3mlh6bjc0q0zfli27dwn2rxamia4jjmrsrf3dyb29kv3p0x1")))

(define-public crate-rollsum-0.3.0 (c (n "rollsum") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "14xx6nsssrgykh3690n0jpn4yh8ryal87c9xxswv2fs56zj19gly") (f (quote (("default") ("bench"))))))

