(define-module (crates-io ro ll rolldown_compiler) #:use-module (crates-io))

(define-public crate-rolldown_compiler-0.0.1 (c (n "rolldown_compiler") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "rolldown_error") (r "^0.0.1") (d #t) (k 0)) (d (n "sugar_path") (r "^0.0.11") (d #t) (k 0)) (d (n "swc_core") (r "^0.59.26") (f (quote ("common" "common_tty" "common_concurrent" "ecma_parser" "ecma_ast" "ecma_codegen" "ecma_visit"))) (d #t) (k 0)))) (h "1hm595nk90q4z8nggvjw68i8fhf8dx97v0n722dxmhazg4z868w6")))

