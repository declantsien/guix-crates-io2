(define-module (crates-io ro ll rolling-dual-crc) #:use-module (crates-io))

(define-public crate-rolling-dual-crc-0.1.0 (c (n "rolling-dual-crc") (v "0.1.0") (d (list (d (n "crc32c") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "crc64fast") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 1)))) (h "1bb9g4r4mb4whr515vc235l8j1rd77ipfvfpin9wz9s1lfwmflgr") (f (quote (("fast" "crc32c" "crc64fast"))))))

