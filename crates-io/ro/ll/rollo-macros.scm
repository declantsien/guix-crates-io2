(define-module (crates-io ro ll rollo-macros) #:use-module (crates-io))

(define-public crate-rollo-macros-0.1.0 (c (n "rollo-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14y2lh1hai4f7wj5yin8sac055jkrqffvd964qp0f1wwg3aym3kp")))

(define-public crate-rollo-macros-0.1.1 (c (n "rollo-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h8wgq2dnk6ijszhlz0grf0jpxsr7fwz95l4bkdnmm4n8l8vnnzq")))

(define-public crate-rollo-macros-0.1.2 (c (n "rollo-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00spp0q4bh2higrnlvpl42fn24962j3nr04pnhdrsr08zbxb52a8")))

(define-public crate-rollo-macros-0.1.3 (c (n "rollo-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1x3cjdf5wg03blq1xycp8m0ic63sy396bbkvwnh0magxxq09xzvj")))

(define-public crate-rollo-macros-0.1.4 (c (n "rollo-macros") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0si853859l3fxxrzrb1p6dw0znihn4rfz6xc4z6c59kdfzplhz8p")))

(define-public crate-rollo-macros-0.1.5 (c (n "rollo-macros") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rf5p6l0kvcx2n2sv6ndhr3a5w515p9lifjr5phzmvvz5ym09skv")))

(define-public crate-rollo-macros-0.2.0 (c (n "rollo-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "010p53b1s2ak1kjissvr8ljzqzq7b3m25z7ns0igz9np2pxadc5f")))

(define-public crate-rollo-macros-0.3.0 (c (n "rollo-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17afkl30ap029si2l4k032i1k1j3gxwb0f9gs06flf9ngn045ngg")))

(define-public crate-rollo-macros-0.4.0 (c (n "rollo-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "165v8s5b8afb6s5pgki6j112614cmbwdpxiqqrvigh2q97hvrk0x")))

(define-public crate-rollo-macros-0.4.1 (c (n "rollo-macros") (v "0.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "154lh2q1239sl3dw050i75186nvmn0pls5g4mnishl8sy85mbk54")))

(define-public crate-rollo-macros-0.6.0 (c (n "rollo-macros") (v "0.6.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0k22liv30wi23jdpx11hvsd4jrh9j7qqhsmr6k40c1xcbiaccg7f")))

(define-public crate-rollo-macros-0.6.1 (c (n "rollo-macros") (v "0.6.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02qyh2gmnpn7yfwkp7d520c515x7g76zs0s4cw5ww9hk28znnjvs")))

