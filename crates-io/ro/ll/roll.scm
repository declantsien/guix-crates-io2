(define-module (crates-io ro ll roll) #:use-module (crates-io))

(define-public crate-roll-1.0.0 (c (n "roll") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1grq4xqm3h2qbldfdmpmvvns6ms2qp93iq9vzdr7kcmpkwjnfr7w")))

(define-public crate-roll-1.0.1 (c (n "roll") (v "1.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1db854zs3arfhjzlp8vsabhgwsn6z2z239xi8mmc8nrsm45b6ycq")))

(define-public crate-roll-1.0.2 (c (n "roll") (v "1.0.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1jlx8mdr6b8xdvfk90srwvar7hrq7lw1kj9a5j08r01zqv8qgnkc")))

(define-public crate-roll-1.0.3 (c (n "roll") (v "1.0.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ng86bbm97jxmm1r7k3snlhv589fjd9zf0wi94ks1vs3yp0nd6qm")))

