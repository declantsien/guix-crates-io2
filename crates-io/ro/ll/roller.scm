(define-module (crates-io ro ll roller) #:use-module (crates-io))

(define-public crate-roller-0.1.0 (c (n "roller") (v "0.1.0") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.2") (d #t) (k 0)))) (h "0p4mkygvdav2z960hxijhp0axvq6hawa3ji793fr9mc2xbnl1l0b")))

