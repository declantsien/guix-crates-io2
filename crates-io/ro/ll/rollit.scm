(define-module (crates-io ro ll rollit) #:use-module (crates-io))

(define-public crate-rollit-0.1.3 (c (n "rollit") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)))) (h "1v6x1lr48ccs08bawansa6lc79xpx8868pvyr122wi2kyk6kk76q")))

