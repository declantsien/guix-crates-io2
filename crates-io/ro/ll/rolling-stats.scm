(define-module (crates-io ro ll rolling-stats) #:use-module (crates-io))

(define-public crate-rolling-stats-0.1.0 (c (n "rolling-stats") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0m8dkyy73smfxyn66399ylha1585ny5wklkc7wyiw8h1wzsfx9q5")))

(define-public crate-rolling-stats-0.1.1 (c (n "rolling-stats") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1p8h01a1dx4ywwxpby20pxwlf0rgyq9m8k34ckizpmjvxs2kv2zv")))

(define-public crate-rolling-stats-0.2.0 (c (n "rolling-stats") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gjdqv9bibbblhl7g5yvkgw4s5bizpz477z2w1syppkan6r0n6vh")))

(define-public crate-rolling-stats-0.2.1 (c (n "rolling-stats") (v "0.2.1") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "17sn8k610kcqj8fbghndc3dxir65akmlz0v6zv93lg9a0rmbilqg")))

(define-public crate-rolling-stats-0.3.0 (c (n "rolling-stats") (v "0.3.0") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "17nnald9j247p5rwbqsxymd4nkj0lx6y7f38q3gv4xbib8js4fxv")))

(define-public crate-rolling-stats-0.4.0 (c (n "rolling-stats") (v "0.4.0") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)))) (h "04qhv3qn9cb0xp9igdfhm87q73mjxdar3k6ng0a3jqw45rqzz2cv")))

(define-public crate-rolling-stats-0.5.0 (c (n "rolling-stats") (v "0.5.0") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "135absalm38i49kgzf6wbals27va3r7l0yiip5riz9rq84njk8gb")))

(define-public crate-rolling-stats-0.5.1 (c (n "rolling-stats") (v "0.5.1") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gw3dqk3gcc6hhja1n9j4jw64an845a1j45divkpnsmz9j1j112l")))

(define-public crate-rolling-stats-0.6.0 (c (n "rolling-stats") (v "0.6.0") (d (list (d (n "float-cmp") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rfch4cnc8s9ksmxhmb1mdvh285jc5sd4vyszy0ips1r7wfljl1r")))

(define-public crate-rolling-stats-0.7.0 (c (n "rolling-stats") (v "0.7.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "libm") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (k 0)))) (h "09l3a3dxb7np3q46vkn04ja9k2yqip1sv4n9rmz5rpjxgznlv2xv")))

