(define-module (crates-io ro ll rollout) #:use-module (crates-io))

(define-public crate-rollout-0.0.1 (c (n "rollout") (v "0.0.1") (d (list (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "10lrczsxmz1zfh6ql4xr49w6haw94lfiajd8xi85vjm7ri46fj83")))

(define-public crate-rollout-0.0.2 (c (n "rollout") (v "0.0.2") (d (list (d (n "redis") (r "^0.8") (d #t) (k 0)))) (h "1vkihymxl81d6sc4285d5lb88f1ny8mwhc2r4l2qnzw9zjf5yy7p")))

