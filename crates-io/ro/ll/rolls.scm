(define-module (crates-io ro ll rolls) #:use-module (crates-io))

(define-public crate-rolls-0.1.2 (c (n "rolls") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1mmwldmvr5210yvv8077ql3iakv8kk729h9q8dh8psgxkyn0pgkh")))

