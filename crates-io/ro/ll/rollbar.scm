(define-module (crates-io ro ll rollbar) #:use-module (crates-io))

(define-public crate-rollbar-0.1.0 (c (n "rollbar") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1xhjdgkhx5qiqjcrkpd3svs75r8k956wyqy57z1lnbb646n8v2rm")))

(define-public crate-rollbar-0.1.1 (c (n "rollbar") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1934w7m0czw2q60s4bss3mdk3d1vkaspl5qclzqhdx8iv60jk1rq")))

(define-public crate-rollbar-0.2.0 (c (n "rollbar") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0hi5328cn9v7bikkqlsgwg4322l3zi26qv0dakl2frklnj78lsd0")))

(define-public crate-rollbar-0.3.0 (c (n "rollbar") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1jn28mrn081fmq0j1533hia5fxlqz2wlmfwrp8d98c1g0ijdyp05")))

(define-public crate-rollbar-0.3.1 (c (n "rollbar") (v "0.3.1") (d (list (d (n "backtrace") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1lpkghlqmh65j4122ljqaxgbpwk6812ccddyb2qmsmkzvqdb12vz")))

(define-public crate-rollbar-0.4.0 (c (n "rollbar") (v "0.4.0") (d (list (d (n "backtrace") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0f6rfl7pl7b3h85yz3rvmjlj5s1ngkf0gghrvlzn8l2l2l1vcajj")))

(define-public crate-rollbar-0.5.0 (c (n "rollbar") (v "0.5.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xsmwcxznp0b9c7px5mnv0i9fl0x977bd5igls566k4aj1wlmrdl")))

(define-public crate-rollbar-0.5.1 (c (n "rollbar") (v "0.5.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1316kf9ph2cxk3qmnhrr9gaypb8iyfd60fny7ini40sx3ffcrcfr")))

(define-public crate-rollbar-0.6.0 (c (n "rollbar") (v "0.6.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "00a7rd1jzhrzv15n78akvh07hv8rm886zvnx3qbypy0qx23wsc03")))

(define-public crate-rollbar-0.7.0 (c (n "rollbar") (v "0.7.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1926rhyd59s5lkxa147q5jb1yrzvmy91rc0as9qb9y8vdihmds3y")))

