(define-module (crates-io ro ll rolling_norm) #:use-module (crates-io))

(define-public crate-rolling_norm-0.1.0 (c (n "rolling_norm") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0acccm74ajp3482qzji904jybc9gn1issy01ddspvab0y7caqj1b")))

(define-public crate-rolling_norm-0.2.0 (c (n "rolling_norm") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0dlgnd2g2fy7gl1vniakcp2qsqzfq1xjnkfrdfajbdwlgw3va0j4")))

(define-public crate-rolling_norm-0.3.0 (c (n "rolling_norm") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1d1l9y9n1a6lsl17rh03i1wh64gsbpmq6f8jjy9hjn72hqwf1v6n")))

(define-public crate-rolling_norm-0.4.0 (c (n "rolling_norm") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0jzv1iwp6wqjqcgchgqfyjvgsn2spxk779vn79cybaxsc02hd3xz")))

