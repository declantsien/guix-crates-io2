(define-module (crates-io ro ll rolldown_resolver) #:use-module (crates-io))

(define-public crate-rolldown_resolver-0.0.1 (c (n "rolldown_resolver") (v "0.0.1") (d (list (d (n "rolldown_error") (r "^0.0.1") (d #t) (k 0)) (d (n "sugar_path") (r "^0.0.11") (d #t) (k 0)))) (h "1j8bwmnrm93a7j8qq02nlvksfqxmqia188vqvy3g8gxirq69lnyg")))

