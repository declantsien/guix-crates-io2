(define-module (crates-io ro ll rolldown_common) #:use-module (crates-io))

(define-public crate-rolldown_common-0.0.1 (c (n "rolldown_common") (v "0.0.1") (d (list (d (n "ena") (r "^0.14.0") (d #t) (k 0)) (d (n "hashlink") (r "^0.8.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "swc_core") (r "^0.59.26") (f (quote ("ecma_ast" "common"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0nrn4sxhni4kn5j6nb1rd53r3a64pwwdy65gnnis558xxfydj3k1")))

