(define-module (crates-io ro cl rocl) #:use-module (crates-io))

(define-public crate-rocl-0.0.1 (c (n "rocl") (v "0.0.1") (d (list (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "0ylwb6k1y3ssnl9lxdi0zllrkf4sbwww6kx8dmc1i2xmdranrxrs")))

(define-public crate-rocl-0.0.2 (c (n "rocl") (v "0.0.2") (d (list (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1y78qjgp8ls696kjwmjllx4avv5mmpn56ckqz3jxgiv2sdihm7iq")))

(define-public crate-rocl-0.0.3 (c (n "rocl") (v "0.0.3") (d (list (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "101wzvb2b57zii1cy02virrx2c87nmbdyljy53rvcn451a9j7w6d")))

(define-public crate-rocl-0.0.4 (c (n "rocl") (v "0.0.4") (d (list (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1ixg56z32nv4npr1g611dhbxahrxmfdrp6bbgxmjv86jw25v4flh")))

(define-public crate-rocl-0.0.5 (c (n "rocl") (v "0.0.5") (d (list (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1gmc4p1mjx3rmbzf4g2djy4lv10nadcai1297d0bcbdqiq93l0ki")))

(define-public crate-rocl-0.0.7 (c (n "rocl") (v "0.0.7") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1cnz13qbsalzd3ffniblp4j43plciys2mzbp9f274y8xap06p58v")))

