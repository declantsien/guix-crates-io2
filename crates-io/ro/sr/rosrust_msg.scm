(define-module (crates-io ro sr rosrust_msg) #:use-module (crates-io))

(define-public crate-rosrust_msg-0.1.0 (c (n "rosrust_msg") (v "0.1.0") (d (list (d (n "rosrust") (r "^0.9.4") (d #t) (k 0)))) (h "0ggb1lim9wx82ccivizi9v8hz0vc0mw9w7s532hp0nshli3s5izk")))

(define-public crate-rosrust_msg-0.1.1 (c (n "rosrust_msg") (v "0.1.1") (d (list (d (n "rosrust") (r "^0.9.5") (d #t) (k 0)))) (h "0fx5gx8b59zbpz5fhqdkz5ibl6af3wz50xjpr04dix1155ivl9qp")))

(define-public crate-rosrust_msg-0.1.2 (c (n "rosrust_msg") (v "0.1.2") (d (list (d (n "rosrust") (r "^0.9.6") (d #t) (k 0)))) (h "07a5p65hl8q4bwkw6lvvhjrwf64yfqisv4dq8bhyf1wl3ajzfldz")))

(define-public crate-rosrust_msg-0.1.3 (c (n "rosrust_msg") (v "0.1.3") (d (list (d (n "rosrust") (r "^0.9.7") (d #t) (k 0)))) (h "0w1bkd4am4vqnhxzik0fx820khgvkhq9ghzdqqa4f7cdd3m9wr7i")))

(define-public crate-rosrust_msg-0.1.4 (c (n "rosrust_msg") (v "0.1.4") (d (list (d (n "rosrust") (r "^0.9.8") (d #t) (k 0)))) (h "1927vw96gq1awmp8r7lxmamfz5rg365fy91kg0369wc5s50wy8wh")))

(define-public crate-rosrust_msg-0.1.5 (c (n "rosrust_msg") (v "0.1.5") (d (list (d (n "rosrust") (r "^0.9.9") (d #t) (k 0)))) (h "0v8hm99250wlzpzjhqxvmd40g6prd76invdrbka25a98mg8a0vsr")))

(define-public crate-rosrust_msg-0.1.6 (c (n "rosrust_msg") (v "0.1.6") (d (list (d (n "rosrust") (r "^0.9.10") (d #t) (k 0)))) (h "09z20nfvwydj5j2z6md0n49gxnky3k11cy0nx5ghh23ad12nark3")))

(define-public crate-rosrust_msg-0.1.7 (c (n "rosrust_msg") (v "0.1.7") (d (list (d (n "rosrust") (r "^0.9.11") (d #t) (k 0)))) (h "1v2fs7ln2xgras0d8gr9r6rxgzamwljwwrhd6sk7lxlfg6hhkid4")))

