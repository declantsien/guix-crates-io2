(define-module (crates-io ro op roopert_macro_common) #:use-module (crates-io))

(define-public crate-roopert_macro_common-0.1.0 (c (n "roopert_macro_common") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1cxjgqvsydbfhmhkidyvj9dr0aabgmpykmkn82wib31f435bqi4k") (f (quote (("verbose"))))))

(define-public crate-roopert_macro_common-0.2.0 (c (n "roopert_macro_common") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0d5kqfhqmz3ps1k0iaky93q2k1gdznli1nff7nbx4xwkmwdclg7f") (f (quote (("verbose"))))))

