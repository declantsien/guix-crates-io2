(define-module (crates-io ro op roopes) #:use-module (crates-io))

(define-public crate-roopes-0.1.0 (c (n "roopes") (v "0.1.0") (d (list (d (n "delegate") (r "^0.9.0") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "enclose") (r "^1.1.8") (d #t) (k 0)) (d (n "roopes-core") (r "^0.1.1") (d #t) (k 0)) (d (n "roopes-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1zsh7w8zklbkwn03cjqh6hnvxbzmvyrlw6gv8ddb61vgagarxfra") (f (quote (("doc-images"))))))

(define-public crate-roopes-0.1.1 (c (n "roopes") (v "0.1.1") (d (list (d (n "delegate") (r "^0.9.0") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "enclose") (r "^1.1.8") (d #t) (k 0)) (d (n "roopes-core") (r "^0.1.1") (d #t) (k 0)) (d (n "roopes-derive") (r "^0.1.0") (d #t) (k 0)))) (h "182ibmpxgv2qni37cxpk1yp1gylwfimdnjlr36myv9mr33dfz8sz") (f (quote (("doc-images"))))))

(define-public crate-roopes-0.1.2 (c (n "roopes") (v "0.1.2") (d (list (d (n "delegate") (r "^0.9.0") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "enclose") (r "^1.1.8") (d #t) (k 0)) (d (n "roopes-core") (r "^0.1.1") (d #t) (k 0)) (d (n "roopes-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1rj6797lxm1wyj932z6fccqa11sr3w4c7kf8dssxhik05zld7mjq") (f (quote (("doc-images"))))))

(define-public crate-roopes-0.1.3 (c (n "roopes") (v "0.1.3") (d (list (d (n "delegate") (r "^0.9.0") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "enclose") (r "^1.1.8") (d #t) (k 0)) (d (n "roopes-core") (r "^0.1.1") (d #t) (k 0)) (d (n "roopes-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0jymlrikg6lpbm7m5zcrngqr2kr4zr347dpf40iw3jjl6q9q7h40") (f (quote (("doc-images"))))))

