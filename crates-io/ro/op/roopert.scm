(define-module (crates-io ro op roopert) #:use-module (crates-io))

(define-public crate-roopert-0.1.0 (c (n "roopert") (v "0.1.0") (d (list (d (n "roopert_macro_root") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1p6n38adcck9ba550bfvkjv91ffi16ffdpy36qcm02ss7bd70ak0") (f (quote (("macros" "roopert_macro_root") ("default" "macros"))))))

(define-public crate-roopert-0.2.0 (c (n "roopert") (v "0.2.0") (d (list (d (n "roopert_macro_root") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "028m4f6zl19pmv8r0l4gqqvxda9iw9j38g28kd1pl5n90nvkvl1n") (f (quote (("macros" "roopert_macro_root") ("default" "macros"))))))

