(define-module (crates-io ro op roopes-derive) #:use-module (crates-io))

(define-public crate-roopes-derive-0.1.0 (c (n "roopes-derive") (v "0.1.0") (d (list (d (n "delegate") (r "^0.9.0") (d #t) (k 0)) (d (n "derive-getters") (r "^0.3.0") (d #t) (k 0)) (d (n "enclose") (r "^1.1.8") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "roopes-core") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "028ydmfb66p2lkrmn947r6d0y7m0dpl2c66k3rrg60q492fczj48")))

