(define-module (crates-io ro op roopes-core) #:use-module (crates-io))

(define-public crate-roopes-core-0.1.0 (c (n "roopes-core") (v "0.1.0") (d (list (d (n "delegate") (r "^0.9.0") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "enclose") (r "^1.1.8") (d #t) (k 0)))) (h "0acbbk7y2cxpfk4l49nnh6h83pzxv2np8wvbi5xifkwdhir78ls3") (f (quote (("doc-images"))))))

(define-public crate-roopes-core-0.1.1 (c (n "roopes-core") (v "0.1.1") (d (list (d (n "delegate") (r "^0.9.0") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "enclose") (r "^1.1.8") (d #t) (k 0)))) (h "1pm1al52v83ini9zx21kriq10bfc2vk67vsfzrwyis5hrvkwphbs") (f (quote (("doc-images"))))))

