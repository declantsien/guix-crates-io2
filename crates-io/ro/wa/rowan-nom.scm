(define-module (crates-io ro wa rowan-nom) #:use-module (crates-io))

(define-public crate-rowan-nom-0.1.0 (c (n "rowan-nom") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "rowan") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1.12") (d #t) (k 2)) (d (n "logos") (r "^0.12.1") (d #t) (k 2)))) (h "16gajg2cjgml11hlk9lw26n66w9v2cxfkg8cg6z7la30jsmczzzz")))

