(define-module (crates-io ro wc rowcol) #:use-module (crates-io))

(define-public crate-rowcol-0.1.0 (c (n "rowcol") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.3.20") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "typenum") (r "^1.4.0") (d #t) (k 0)))) (h "0xm2qqx34qb5mxz7r1mm4vi9s4awalq7g13wj0lxdk710dw966bx") (y #t)))

(define-public crate-rowcol-0.1.1 (c (n "rowcol") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.3.20") (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "typenum") (r "^1.4.0") (d #t) (k 0)))) (h "0ffvcrhlli27a8v84dj3n7jgac4k00xzpahj079jc0ci0phsijhq") (f (quote (("std" "arrayvec/std") ("default" "std")))) (y #t)))

(define-public crate-rowcol-0.2.0 (c (n "rowcol") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.3.20") (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "typenum") (r "^1.5.0") (d #t) (k 0)))) (h "1y9i65vjphbs73afih4w5ajanhrka6vxv9a0abmlg7d3wpjwdgfd") (f (quote (("std" "arrayvec/std") ("default" "std")))) (y #t)))

(define-public crate-rowcol-0.2.1 (c (n "rowcol") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.3.20") (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "typenum") (r "^1.5.0") (d #t) (k 0)))) (h "0bpd3c4aw61imh339aw7hjjwbra95p0yjdiyy5nx00602qjgxdw6") (f (quote (("std" "arrayvec/std") ("default" "std")))) (y #t)))

(define-public crate-rowcol-0.3.0 (c (n "rowcol") (v "0.3.0") (d (list (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "typenum") (r "^1.5.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0clnhcqafq763nzi2llmqb8hgc28ywg7gyjrhm385jwf8jh1l2aw") (f (quote (("use_needs_drop" "nodrop/use_needs_drop") ("unicode_width" "unicode-width") ("std" "nodrop/std") ("default" "std" "unicode_width")))) (y #t)))

(define-public crate-rowcol-0.3.1 (c (n "rowcol") (v "0.3.1") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "typenum") (r "^1.5.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0nmkrjl620ddigrpq9m3ipzbvhvi77hz5nmmk4pi26g8w4q220h9") (f (quote (("use_needs_drop" "nodrop/use_needs_drop") ("unicode_width" "unicode-width") ("std" "nodrop/std") ("default" "std" "unicode_width")))) (y #t)))

(define-public crate-rowcol-0.3.2 (c (n "rowcol") (v "0.3.2") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "odds") (r "^0.2.24") (d #t) (k 0)) (d (n "typenum") (r "^1.5.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0ff7xk8597hq07syhglshy8dqryf6mv1z3aqaw6dm7giigllfjab") (f (quote (("use_needs_drop" "nodrop/use_needs_drop") ("unicode_width" "unicode-width") ("std" "nodrop/std") ("default" "std" "unicode_width")))) (y #t)))

(define-public crate-rowcol-0.3.3 (c (n "rowcol") (v "0.3.3") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "typenum") (r "^1.5.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "10vwpmrfknv8qjm370giccfp068nbv7x3wx960lllj3saxckydim") (f (quote (("use_needs_drop" "nodrop/use_needs_drop") ("unicode_width" "unicode-width") ("std" "nodrop/std") ("default" "std" "unicode_width")))) (y #t)))

