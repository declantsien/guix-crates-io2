(define-module (crates-io ro cm rocm_smi_lib) #:use-module (crates-io))

(define-public crate-rocm_smi_lib-0.1.0-beta (c (n "rocm_smi_lib") (v "0.1.0-beta") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0c11jdwyibdl7wi2rdmcmzilmkychhzddl1bpj3h3vbyf32rjirc") (y #t)))

(define-public crate-rocm_smi_lib-0.1.1-beta (c (n "rocm_smi_lib") (v "0.1.1-beta") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1crggyr3d66n2viq2bjxswg7rx843hqd59rjqqdkvfhdl34jvfhm") (y #t)))

(define-public crate-rocm_smi_lib-0.1.2-beta (c (n "rocm_smi_lib") (v "0.1.2-beta") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1560akmxwp3w1lf2fbsblwbdnalkgs0azbg7js0qd3fab71cx8l3") (y #t)))

(define-public crate-rocm_smi_lib-0.1.3-beta (c (n "rocm_smi_lib") (v "0.1.3-beta") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0r3npcwwxqkwz08m83l217067ls2jsf3w063r1l37k32xwkfd0m5") (y #t)))

(define-public crate-rocm_smi_lib-0.1.4-beta (c (n "rocm_smi_lib") (v "0.1.4-beta") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "18cbhh876213a8044jy8ilrscml2b52w4x32jr6db33c4208k45i") (y #t)))

(define-public crate-rocm_smi_lib-0.1.5-beta (c (n "rocm_smi_lib") (v "0.1.5-beta") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0r4q97s9l1n8k4rlfgm4xyf5ilpsxys1s33xsy873irywjgc8qb4") (y #t)))

(define-public crate-rocm_smi_lib-0.1.6-beta (c (n "rocm_smi_lib") (v "0.1.6-beta") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0i8r037is0xaiz08gd9pg0hsywkpr72z1a216i66x3w3zbxwr7q6") (y #t)))

(define-public crate-rocm_smi_lib-0.1.7-beta (c (n "rocm_smi_lib") (v "0.1.7-beta") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0gr7n6djs7g7x8zi9fb436bp2y3vrk6zbqj49m5sq2nvh88vjch2") (y #t)))

(define-public crate-rocm_smi_lib-0.1.8-beta (c (n "rocm_smi_lib") (v "0.1.8-beta") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "12552njs8w1v449divgf9z35w772rcl2f3jpiryfpfi42sbpdzpj") (y #t)))

(define-public crate-rocm_smi_lib-0.1.8-beta.2 (c (n "rocm_smi_lib") (v "0.1.8-beta.2") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0h820zgcq5xln6dccw53r0m8n6v5smmzf1m8nyjshg4x9lbhq0fa") (y #t)))

(define-public crate-rocm_smi_lib-0.1.8-beta.3 (c (n "rocm_smi_lib") (v "0.1.8-beta.3") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0ylwjdd7y4dykf0h1kmd9n78c1ph0bp1wxn021lpfg33cpspv2nq") (y #t)))

(define-public crate-rocm_smi_lib-0.1.9 (c (n "rocm_smi_lib") (v "0.1.9") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1jzd07pakfbb11v38ksjajdwwd1wi8v5hysbqs9rcyvbg424pvbq") (y #t)))

(define-public crate-rocm_smi_lib-0.1.10 (c (n "rocm_smi_lib") (v "0.1.10") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1yxbj8rjm8k9p24di1ln84zmmbhb8w1ds0k7mcm36hkscwjw88ka") (f (quote (("vendored")))) (y #t)))

(define-public crate-rocm_smi_lib-0.1.11 (c (n "rocm_smi_lib") (v "0.1.11") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0kpc4zkxfbrvkwwi1rnqbahgll1974n0bihlj7l8vhr3kqillk3c") (f (quote (("vendored")))) (y #t)))

(define-public crate-rocm_smi_lib-0.1.12 (c (n "rocm_smi_lib") (v "0.1.12") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "17g60bixyl63lrx3z7a0k6l2m13k4c6rixjsx4i08jqy8006g37l") (f (quote (("vendored")))) (y #t)))

(define-public crate-rocm_smi_lib-0.1.13 (c (n "rocm_smi_lib") (v "0.1.13") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "rocm_smi_lib_sys") (r "^0.1.0") (d #t) (k 0)))) (h "00l306drw4ih8sqq9midzaxrkvzjjrsrvpjnp6l54y8k23rmk0g0") (f (quote (("vendored" "rocm_smi_lib_sys/vendored")))) (y #t)))

(define-public crate-rocm_smi_lib-0.1.14-rc.1 (c (n "rocm_smi_lib") (v "0.1.14-rc.1") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "rocm_smi_lib_sys") (r "^0.1.2") (d #t) (k 0)))) (h "1xf06512p5vb1kncxb69jg66ffh6djvq5040j658ki4qrgbabzb3") (f (quote (("vendored" "rocm_smi_lib_sys/vendored")))) (y #t)))

(define-public crate-rocm_smi_lib-0.1.14 (c (n "rocm_smi_lib") (v "0.1.14") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "rocm_smi_lib_sys") (r "^0.1.3") (d #t) (k 0)))) (h "1fv5jhfdx1hg693hjm0y9ymkbsq4kz3j9jg5zjgdfcj3gc9aicxa") (f (quote (("vendored" "rocm_smi_lib_sys/vendored")))) (y #t)))

(define-public crate-rocm_smi_lib-0.1.15 (c (n "rocm_smi_lib") (v "0.1.15") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "rocm_smi_lib_sys") (r "^0.1.4") (d #t) (k 0)))) (h "15szvclwx4ypwsm4nc43w5lcg2hxp0dffykj322r08lv0kk8awzj") (f (quote (("vendored" "rocm_smi_lib_sys/vendored")))) (y #t)))

(define-public crate-rocm_smi_lib-0.1.16 (c (n "rocm_smi_lib") (v "0.1.16") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "rocm_smi_lib_sys") (r "^0.1.4") (d #t) (k 0)))) (h "0nys57qdr9dxwrnjcwh79f8rcksjn4h9jrcnxrj3j5kk1vmcva9a") (f (quote (("vendored" "rocm_smi_lib_sys/vendored") ("fn_query") ("device")))) (y #t)))

(define-public crate-rocm_smi_lib-0.1.17 (c (n "rocm_smi_lib") (v "0.1.17") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "rocm_smi_lib_sys") (r "^0.1.4") (d #t) (k 0)))) (h "0p8apgc7wvhbpcxihag98l1si0grv93zz2x7rwncs3f9p6mwl8b4") (f (quote (("vendored" "rocm_smi_lib_sys/vendored") ("fn_query") ("device"))))))

(define-public crate-rocm_smi_lib-0.2.0-rc1 (c (n "rocm_smi_lib") (v "0.2.0-rc1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "rocm_smi_lib_sys") (r "^0.2.1") (d #t) (k 0)))) (h "1d8jn50sky9s7d44ys2pm8nlh3sxxz1cic5vchz5ya4g9fr4rvn0") (f (quote (("fn_query") ("device")))) (y #t)))

(define-public crate-rocm_smi_lib-0.2.0 (c (n "rocm_smi_lib") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "rocm_smi_lib_sys") (r "^0.2.1") (d #t) (k 0)))) (h "1107sqzl68349xhphw12ymn83z7ii0dq2n8ywcq5hla5f3qmrv7l") (f (quote (("fn_query") ("device"))))))

(define-public crate-rocm_smi_lib-0.2.2 (c (n "rocm_smi_lib") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "rocm_smi_lib_sys") (r "^0.2.2") (d #t) (k 0)))) (h "0z999mb5l4yvdrdk6bnvhj8wqk5l0ld90ag5cphjmj2hfahjgm7n") (f (quote (("fn_query") ("device"))))))

