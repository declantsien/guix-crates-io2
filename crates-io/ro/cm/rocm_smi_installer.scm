(define-module (crates-io ro cm rocm_smi_installer) #:use-module (crates-io))

(define-public crate-rocm_smi_installer-0.1.0 (c (n "rocm_smi_installer") (v "0.1.0") (d (list (d (n "sudo") (r "^0.6.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "0ba22sfbspsmmjd4jcc7dlzn3i7hgyf01hwdkdxda91p39bxxdas")))

(define-public crate-rocm_smi_installer-0.1.1 (c (n "rocm_smi_installer") (v "0.1.1") (d (list (d (n "sudo") (r "^0.6.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "1xspz00697bsgqqxcpyd8m9zs1zwhwqggv7l7lx7w4j6ihk1026w")))

(define-public crate-rocm_smi_installer-0.1.2 (c (n "rocm_smi_installer") (v "0.1.2") (d (list (d (n "sudo") (r "^0.6.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "0w8lp7v0mzr92xfdwg79w29sf23i5ffyhsf41a8dbnzmd14hzv8d")))

