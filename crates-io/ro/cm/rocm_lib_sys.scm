(define-module (crates-io ro cm rocm_lib_sys) #:use-module (crates-io))

(define-public crate-rocm_lib_sys-0.1.0 (c (n "rocm_lib_sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1riklwq9r344x8a7m5kcxm7axjnmmj5pzd38wxaxjblyd7m2j2pi") (f (quote (("vendored")))) (y #t)))

