(define-module (crates-io ro cm rocm_smi_lib_sys) #:use-module (crates-io))

(define-public crate-rocm_smi_lib_sys-0.1.0 (c (n "rocm_smi_lib_sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1q4wwvf4lj5m18bd3c2b3427avbgm3ikhkcwrckjhsphkqsisj12") (f (quote (("vendored")))) (y #t)))

(define-public crate-rocm_smi_lib_sys-0.1.1 (c (n "rocm_smi_lib_sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1220s2685jb56y38ckcd6gc0pjnvmbcnj8r7v6n1yylsgc2n93bd") (f (quote (("vendored")))) (y #t)))

(define-public crate-rocm_smi_lib_sys-0.1.2 (c (n "rocm_smi_lib_sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1ba786yzs73iymhfxsvc368k5wsxfi5sq23y2c0jwqm4lc94x5cd") (f (quote (("vendored")))) (y #t)))

(define-public crate-rocm_smi_lib_sys-0.1.3 (c (n "rocm_smi_lib_sys") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0yb4blmhph0ibac3arwlgyx0amjywg6ybaic6h1x6mjfyl9j7hwg") (f (quote (("vendored")))) (y #t)))

(define-public crate-rocm_smi_lib_sys-0.1.4 (c (n "rocm_smi_lib_sys") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0jnwc2dq38fpkdq5xvg7ahdyja82lgj0q4pdiczbrfwg8ziawvr7") (f (quote (("vendored")))) (y #t)))

(define-public crate-rocm_smi_lib_sys-0.1.5 (c (n "rocm_smi_lib_sys") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1fqnlziij844s31f1lynz3mxr4pxb9h5mkw5f8z65nf0ghf0fmlj") (f (quote (("vendored"))))))

(define-public crate-rocm_smi_lib_sys-0.2.0-rc1 (c (n "rocm_smi_lib_sys") (v "0.2.0-rc1") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "14wnjc8gcv561rfppg1vc036950pqnbx53r95ix8kcjxgy6mzr9f") (y #t)))

(define-public crate-rocm_smi_lib_sys-0.2.0-rc2 (c (n "rocm_smi_lib_sys") (v "0.2.0-rc2") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "0h9593w0phijsqj8gqy60ymrn1fsqqp2zlam2s2mjhbcvs1dvi3p") (y #t)))

(define-public crate-rocm_smi_lib_sys-0.2.0 (c (n "rocm_smi_lib_sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "1ba7p5811c19csw028302s5dpm5z82iy608r31y5x7wd54cdibjh") (y #t)))

(define-public crate-rocm_smi_lib_sys-0.2.1 (c (n "rocm_smi_lib_sys") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "1cnpib9sfsnq5w42nr53bbnrq2mcnv4c9b5ipq15lh145xr3vmfz")))

(define-public crate-rocm_smi_lib_sys-0.2.2-rc1 (c (n "rocm_smi_lib_sys") (v "0.2.2-rc1") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "1cvhsx1sq5zqmhrq64fdpq7c7c773ywfqqh536pz2ccn2fq98cl3")))

(define-public crate-rocm_smi_lib_sys-0.2.2-rc2 (c (n "rocm_smi_lib_sys") (v "0.2.2-rc2") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "1w40clqikkba7bzb1z57x42xb0qbb1a8gmvkxiy8m4rkzgiikw8w")))

(define-public crate-rocm_smi_lib_sys-0.2.2 (c (n "rocm_smi_lib_sys") (v "0.2.2") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "1vl66yxxd5dcpc5a43qsrj6dlh96ggvvnm6ni6p15hln6i13sv6h")))

(define-public crate-rocm_smi_lib_sys-0.2.2-patch1 (c (n "rocm_smi_lib_sys") (v "0.2.2-patch1") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "0lxyvj46fd2jlmyik3kl98zj6zkczrl7q3s4rpk07vshb547bhyn")))

