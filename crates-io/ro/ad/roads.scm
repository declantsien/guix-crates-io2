(define-module (crates-io ro ad roads) #:use-module (crates-io))

(define-public crate-roads-0.1.0 (c (n "roads") (v "0.1.0") (d (list (d (n "oats") (r "^0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1sgdzfk93jsnj959f1d7rc8jq7g5h64lclhzbbhrzhaykcf2ga5f")))

