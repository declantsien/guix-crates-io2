(define-module (crates-io ro ad roads-from-nd) #:use-module (crates-io))

(define-public crate-roads-from-nd-0.1.0 (c (n "roads-from-nd") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6.86") (d #t) (k 0)) (d (n "osm4routing") (r "^0.2.0") (d #t) (k 0)) (d (n "pdf") (r "^0.5.0") (d #t) (k 0)))) (h "0rh5r1n1msa7wva6xl0z6y6hlixlh6mcnzs134w4y1qlw0k6ir3b")))

(define-public crate-roads-from-nd-0.1.1 (c (n "roads-from-nd") (v "0.1.1") (d (list (d (n "docopt") (r "^0.6.86") (d #t) (k 0)) (d (n "osm4routing") (r "^0.2.0") (d #t) (k 0)) (d (n "pdf") (r "^0.5.0") (d #t) (k 0)))) (h "0r7d4krgpy6hrs30n6ffmrjc74igcx2wdwnmrxd9jszqjkb9dp79")))

