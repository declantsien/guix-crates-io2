(define-module (crates-io ro ad roadmap) #:use-module (crates-io))

(define-public crate-roadmap-0.1.0 (c (n "roadmap") (v "0.1.0") (d (list (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 0)))) (h "1x34xsp6z3r7wh6axmlqkgi24njb21pi3qwc0xq9czg1hz2m4pyk")))

(define-public crate-roadmap-0.1.1 (c (n "roadmap") (v "0.1.1") (d (list (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 0)))) (h "1d6pj56kjqj2cwibii4ak6mgvkakcwxa8br6ni9mrv87z9z109ac")))

(define-public crate-roadmap-0.2.0 (c (n "roadmap") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ciahn8zdy9sqhyh3s2pr57c1a7h0c944zh04drr8g02lsmx5f0d")))

(define-public crate-roadmap-0.3.0 (c (n "roadmap") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1009wd62iaaixkmp0niv10lzq07s7li40hnjva0r66m19wvah067")))

(define-public crate-roadmap-0.4.0 (c (n "roadmap") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1d34z05bdpk91c1gs1f837pfas78dyp62yf41y06f63q050wd8nc")))

(define-public crate-roadmap-0.4.1 (c (n "roadmap") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zqv248q35jhx5y3rlffva4cjszraik5xpymjh5zj2qa9xdynkkn")))

(define-public crate-roadmap-0.4.3 (c (n "roadmap") (v "0.4.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ijkwl4zzlflf6ah13i5ff4x783wwwq69jydkwxzsyzh2pw1r39a")))

(define-public crate-roadmap-0.4.4 (c (n "roadmap") (v "0.4.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cwlwdc6s1f26ifyfbvvh3fxq80mkfvm5ckdkzmx86snlagxhq64")))

(define-public crate-roadmap-0.4.5 (c (n "roadmap") (v "0.4.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "195xmv9x4a40cd6crffmfwbjh5fmry9f5w6fjj8lj9yl5l00h31z")))

(define-public crate-roadmap-0.5.0 (c (n "roadmap") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0d0j88wn59sfxwh2k4f609w56n6bmcgf54m0jk9rwc3vci5f8ad1")))

(define-public crate-roadmap-0.6.0 (c (n "roadmap") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ndf322spl9jyjivsd73fvaqvr7ydqk68wamdfidykzh6gcn489n")))

