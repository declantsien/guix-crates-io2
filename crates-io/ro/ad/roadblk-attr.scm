(define-module (crates-io ro ad roadblk-attr) #:use-module (crates-io))

(define-public crate-roadblk-attr-0.1.0 (c (n "roadblk-attr") (v "0.1.0") (d (list (d (n "procmeta") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17pf05w88q9qiagfb5jbswidsprn3dhl3lhy0hf3dzv01g9n8jrx") (f (quote (("proc" "procmeta") ("cod" "serde"))))))

(define-public crate-roadblk-attr-0.1.1 (c (n "roadblk-attr") (v "0.1.1") (d (list (d (n "procmeta") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vm4783s8pwhcbwzhvcsvhhdd0xjs9n33yn1x7gjzbkcxz3wi7rq") (f (quote (("proc" "procmeta") ("core" "serde"))))))

(define-public crate-roadblk-attr-0.1.3 (c (n "roadblk-attr") (v "0.1.3") (d (list (d (n "procmeta") (r "^0.2.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0w0fl3is3wqp6y9wsprvj73ihcia4z1lls5jfv9srr58kb8pk5cm") (f (quote (("proc" "procmeta") ("core" "serde"))))))

(define-public crate-roadblk-attr-0.1.4 (c (n "roadblk-attr") (v "0.1.4") (d (list (d (n "procmeta") (r "^0.2.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17nza8na3kcnbh1x5c763jxc11s29l5cma3qzhap7axbaz9yvamd") (f (quote (("proc" "procmeta") ("core" "serde"))))))

(define-public crate-roadblk-attr-0.1.5 (c (n "roadblk-attr") (v "0.1.5") (d (list (d (n "procmeta") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qnfspxs251rrl5zbqhpmi2f4rm7mpc4jyq7z8imqq0xiifj10v2") (f (quote (("proc" "procmeta") ("core" "serde"))))))

(define-public crate-roadblk-attr-0.1.6 (c (n "roadblk-attr") (v "0.1.6") (d (list (d (n "procmeta") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wxfjp9m3aprsqygbh38gkavbnnw7wschz0n22kqribd6ynqr80v") (f (quote (("proc" "procmeta") ("core" "serde"))))))

(define-public crate-roadblk-attr-0.1.7 (c (n "roadblk-attr") (v "0.1.7") (d (list (d (n "procmeta") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "143j6s5chxyp2nq9m2hd4i2nrddm8hvcikl6qbff47kz4yr62cza") (f (quote (("proc" "procmeta") ("core" "serde"))))))

(define-public crate-roadblk-attr-0.1.8 (c (n "roadblk-attr") (v "0.1.8") (d (list (d (n "procmeta") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03l3yf975zwyjdr696pfc8j2l0bry8habndb5jp0ncqhjjwbdx28") (f (quote (("proc" "procmeta") ("core" "serde"))))))

