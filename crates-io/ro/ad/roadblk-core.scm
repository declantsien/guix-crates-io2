(define-module (crates-io ro ad roadblk-core) #:use-module (crates-io))

(define-public crate-roadblk-core-0.1.0 (c (n "roadblk-core") (v "0.1.0") (d (list (d (n "lazy-regex") (r "^3.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.0") (f (quote ("cod"))) (d #t) (k 0)))) (h "18vb7k0f9wq3l20vrx60b26zr0jqmn9cdkmlyrdyr5dsf1ndn7ai") (y #t)))

(define-public crate-roadblk-core-0.1.1 (c (n "roadblk-core") (v "0.1.1") (d (list (d (n "lazy-regex") (r "^3.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.0") (f (quote ("cod"))) (d #t) (k 0)))) (h "1cw205gqb2kr3law76i6jgmqivzqgchyrmpn80p9h5p0k4699508")))

(define-public crate-roadblk-core-0.1.2 (c (n "roadblk-core") (v "0.1.2") (d (list (d (n "lazy-regex") (r "^3.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.1") (f (quote ("core"))) (d #t) (k 0)))) (h "1z2lpmi2ssac6hqqs8an44fqmbyfwj2dkm4c9fp49fap56cr0cfj")))

(define-public crate-roadblk-core-0.1.3 (c (n "roadblk-core") (v "0.1.3") (d (list (d (n "lazy-regex") (r "^3.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.3") (f (quote ("core"))) (d #t) (k 0)))) (h "0rr2irnp6ywwdfxnwrgj4ff2fj8wbbh0jg056p45kw1kapad7x9c")))

(define-public crate-roadblk-core-0.1.4 (c (n "roadblk-core") (v "0.1.4") (d (list (d (n "lazy-regex") (r "^3.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.4") (f (quote ("core"))) (d #t) (k 0)))) (h "0r1mczxhsjpn6q05mcnrzfqhgf9h6xxpnfg19m8i97yn3mqzlmn1")))

(define-public crate-roadblk-core-0.1.5 (c (n "roadblk-core") (v "0.1.5") (d (list (d (n "lazy-regex") (r "^3.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.5") (f (quote ("core"))) (d #t) (k 0)))) (h "16jb1xrjblnq4i9y6fbd9v48pz8jimxiszpyjkw6v7vw4rd5dj8f")))

(define-public crate-roadblk-core-0.1.6 (c (n "roadblk-core") (v "0.1.6") (d (list (d (n "lazy-regex") (r "^3.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.6") (f (quote ("core"))) (d #t) (k 0)))) (h "13cgmy3dj3h7194c0nixydxinvd4v64b8sxqmgyp688id0h2lgiq")))

(define-public crate-roadblk-core-0.1.7 (c (n "roadblk-core") (v "0.1.7") (d (list (d (n "lazy-regex") (r "^3.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.7") (f (quote ("core"))) (d #t) (k 0)))) (h "11g09jwmgdvym9igm52phim66apvv9rrdjgcjg1d28p8hbrj7n68")))

(define-public crate-roadblk-core-0.1.8 (c (n "roadblk-core") (v "0.1.8") (d (list (d (n "lazy-regex") (r "^3.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.8") (f (quote ("core"))) (d #t) (k 0)))) (h "1sv28sscxrjbhfwcrzai5viazm1s46afmwhhllvmfgs3dv5psj19")))

