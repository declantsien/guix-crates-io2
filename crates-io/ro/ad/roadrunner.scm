(define-module (crates-io ro ad roadrunner) #:use-module (crates-io))

(define-public crate-roadrunner-0.0.0 (c (n "roadrunner") (v "0.0.0") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)))) (h "0ka5bj3cpkxqsx486i4pfc128n0avbzx62bxdxj0nydwvjbjfmv3")))

(define-public crate-roadrunner-0.1.1 (c (n "roadrunner") (v "0.1.1") (d (list (d (n "base64") (r "~0.5.0") (d #t) (k 2)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 2)) (d (n "flate2") (r "^0.2.19") (d #t) (k 0)) (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "0ap88h2cagd3islp21jzjc0lzdd2cmv0jfvwy6s92dvcqy05lskd")))

