(define-module (crates-io ro ad roadblk-expand) #:use-module (crates-io))

(define-public crate-roadblk-expand-0.1.0 (c (n "roadblk-expand") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.0") (f (quote ("proc"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "03ay2lbrnd0dpmcy7d2dz1z06mgkk2zm6m322jyzizvwil15i87r")))

(define-public crate-roadblk-expand-0.1.1 (c (n "roadblk-expand") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.1") (f (quote ("proc"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0nw2dvf8jxgs873ngm7icv42i1zniiav0hw8inrhvcdzg047zj18")))

(define-public crate-roadblk-expand-0.1.3 (c (n "roadblk-expand") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.2.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.3") (f (quote ("proc"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xx4p25b5ayz66bpwwzjgz97j70hr94w8yd04vf9bc2wdwpb6h3l")))

(define-public crate-roadblk-expand-0.1.4 (c (n "roadblk-expand") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.2.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.4") (f (quote ("proc"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1n1aqk86yv8zs80vaglrajknpqkqj8y4nyyrfb6h0nvrzzacnv1d")))

(define-public crate-roadblk-expand-0.1.5 (c (n "roadblk-expand") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.5") (f (quote ("proc"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0xhvwq3iawdiwzh7qag2602yxj1jb72sr920qqk33741y83p40vb")))

(define-public crate-roadblk-expand-0.1.6 (c (n "roadblk-expand") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.6") (f (quote ("proc"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1ha9pkfmwkzfykx76dwnxhgazm387h8z5zw4prnkf1pih8k6cxak")))

(define-public crate-roadblk-expand-0.1.7 (c (n "roadblk-expand") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.7") (f (quote ("proc"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0wq63djdqzsiakgpw05ail10qrb1c95safhsmn9a6bxxf71a928i")))

(define-public crate-roadblk-expand-0.1.8 (c (n "roadblk-expand") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.3.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "roadblk-attr") (r "^0.1.8") (f (quote ("proc"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1bidghp0s4ic5wllnx9mm1l5va77m034c1l4kpfc731c3f6dvrh1")))

