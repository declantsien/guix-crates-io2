(define-module (crates-io ro us rousan_main_rust) #:use-module (crates-io))

(define-public crate-rousan_main_rust-0.1.0 (c (n "rousan_main_rust") (v "0.1.0") (h "16rvwbdqpjxnlk0dx3vbdb22s26n5rvn90mqcmkd457dpg0w8dgr")))

(define-public crate-rousan_main_rust-0.1.1 (c (n "rousan_main_rust") (v "0.1.1") (h "0wyz5ya4cd586kggll74y5vn8wy3x13nsb48g2b6khffkphdxn4z")))

(define-public crate-rousan_main_rust-0.2.1 (c (n "rousan_main_rust") (v "0.2.1") (h "0a6cpskjfnswqjk8vg69nlbs293d09dp9kkwk66gsf674b30xqi3")))

(define-public crate-rousan_main_rust-0.3.0 (c (n "rousan_main_rust") (v "0.3.0") (h "1667wqw6x2rpjg5phlnpzpgiphh4a1q4a8j3jgx01mn832zycq2y")))

(define-public crate-rousan_main_rust-0.4.0 (c (n "rousan_main_rust") (v "0.4.0") (h "0h9p9fvkzv7pvjspz0drpvn0ynapd97z7q85b75n94ifvjpqckhz")))

(define-public crate-rousan_main_rust-0.5.0 (c (n "rousan_main_rust") (v "0.5.0") (h "0hkj2fwz7xxplmr936i8badz6p9b9x5vakfwcdqpl48lkyhb8x3m")))

