(define-module (crates-io ro us rouse) #:use-module (crates-io))

(define-public crate-rouse-0.1.0 (c (n "rouse") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1ywip5x82ffvl3c3xg9l4062559lky95vd0fmnyxqz04810fp239")))

(define-public crate-rouse-0.1.1 (c (n "rouse") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1x7ys7v8pabbzsajyq0png93rmpl67f2xc3fh2yxxvavy3k2xxcq")))

(define-public crate-rouse-0.1.2 (c (n "rouse") (v "0.1.2") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0dcjpnbz7c1g1r00dlry0y2yrrdrg3zcxqsw3zp81bfqj086fgbq")))

