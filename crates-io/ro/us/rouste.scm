(define-module (crates-io ro us rouste) #:use-module (crates-io))

(define-public crate-rouste-0.1.0 (c (n "rouste") (v "0.1.0") (h "0xpsxrx8qxrlck6600laa16cw8g1jqicni07cvizpa8759vhxhd7") (y #t)))

(define-public crate-rouste-0.1.1 (c (n "rouste") (v "0.1.1") (h "01lcqkysw1blh8f3wn36n83pzjg7asq3cp9cazy21zg9v35f16bg") (y #t)))

(define-public crate-rouste-0.1.2 (c (n "rouste") (v "0.1.2") (h "05n1ijiwl6fjsikisfrykm3i6pm86cmydm3qq4vv6mfd87qaqrc3") (y #t)))

(define-public crate-rouste-0.1.3 (c (n "rouste") (v "0.1.3") (h "1v65jdrqhjf4cpk247h592l4yw0qf0mfb1w73ni8pngdhhw81g21") (y #t)))

(define-public crate-rouste-0.1.4 (c (n "rouste") (v "0.1.4") (h "0x4vzk69zkggh0xg44nz7p3cp72p3hhkgg8g2zga2nrv3x58lyc1") (y #t)))

(define-public crate-rouste-0.1.5 (c (n "rouste") (v "0.1.5") (h "1aqsv6zx2fcpav4f5qgvb40lfgx1r50ymrkcfkpyhzn5wvnybxjw") (y #t)))

(define-public crate-rouste-0.1.6 (c (n "rouste") (v "0.1.6") (h "0pnf75dzmywmrb9ql3h4pwm3lpw1bhpb49g7n6nycsarl0aiks34") (y #t)))

(define-public crate-rouste-0.1.7 (c (n "rouste") (v "0.1.7") (h "0vbyq83mcdszliw2xk25a66bfhaj0v2wb1n9g3ngpd6z2xws7g9v") (y #t)))

(define-public crate-rouste-0.2.0 (c (n "rouste") (v "0.2.0") (h "0vyxclhv3rv4dcf6wr2mrk3ya0ff2hicnk7i1xy98fwx2vhyli9l")))

(define-public crate-rouste-0.2.1 (c (n "rouste") (v "0.2.1") (h "0y93j3ydgkild4jhs404lipha4qick4v4llkih944idambc1a9n2")))

