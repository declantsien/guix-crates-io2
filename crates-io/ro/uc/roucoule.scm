(define-module (crates-io ro uc roucoule) #:use-module (crates-io))

(define-public crate-roucoule-0.1.0 (c (n "roucoule") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)))) (h "156hzm4x134gwfkwjsgwdnyk3wspdm61mm2msakl755flxycykx8")))

