(define-module (crates-io ro bs robs) #:use-module (crates-io))

(define-public crate-robs-0.1.0 (c (n "robs") (v "0.1.0") (d (list (d (n "stringr") (r "^0.1.0") (d #t) (k 0)))) (h "079mcnaiwdsmp1f1dk3j7dk3rzwi1i8h3qp4zrxkwrx5k6zj9yh8")))

(define-public crate-robs-0.1.1 (c (n "robs") (v "0.1.1") (d (list (d (n "stringr") (r "^0.1.1") (d #t) (k 0)))) (h "1fdh3m1n20mxyr3jll6d1mzhi5r0pi3w68ig48bmi5g4gxiq9znn")))

(define-public crate-robs-0.1.2 (c (n "robs") (v "0.1.2") (d (list (d (n "stringr") (r "^0.1.2") (d #t) (k 0)))) (h "0fkanrq5ijn13cdii56iys6fwg3l9ns1iplb6545gmj0a9aam4dy")))

(define-public crate-robs-0.2.0 (c (n "robs") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "stringr") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0nigiky1am4p4xdmivfpbn59dmilj4nis47ahd1mxm3y71g77j39")))

(define-public crate-robs-0.2.1 (c (n "robs") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "stringr") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "14d65ddqh0czl61dmw2i4xipkzshg5syv6cigr07aqfbpcdlgr3p")))

(define-public crate-robs-0.2.2 (c (n "robs") (v "0.2.2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "stringr") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "07bdhpn0mmbx8hprk2hd6mk70z7lllwm22wssmzxx89krisksw78")))

