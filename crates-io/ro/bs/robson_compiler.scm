(define-module (crates-io ro bs robson_compiler) #:use-module (crates-io))

(define-public crate-robson_compiler-0.1.4 (c (n "robson_compiler") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06d33hzmw793ycjqhnw34yj2qgxgqrfmp43aw4x2v1gl3ybsl5ib")))

(define-public crate-robson_compiler-0.1.5 (c (n "robson_compiler") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12vlpc0ivx6af0kmljxbdmbmiqsg7gr2lxn3jg1c13akjbhhhss4")))

(define-public crate-robson_compiler-0.1.6 (c (n "robson_compiler") (v "0.1.6") (d (list (d (n "getrandom") (r "^0.2.8") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wlmajsb6ncpszw3xaj3fpr2gpg515f5vjss3fqz2xc532c73ia8")))

