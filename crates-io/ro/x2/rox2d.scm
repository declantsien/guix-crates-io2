(define-module (crates-io ro x2 rox2d) #:use-module (crates-io))

(define-public crate-rox2d-0.1.0 (c (n "rox2d") (v "0.1.0") (d (list (d (n "glam") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "bevy") (r "^0.9.1") (d #t) (k 2)))) (h "06v1aqjmv5s5qq7p62cknmaaghdyl7sa7as4i96n4d81wph61vm8")))

