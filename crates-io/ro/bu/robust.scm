(define-module (crates-io ro bu robust) #:use-module (crates-io))

(define-public crate-robust-0.1.0 (c (n "robust") (v "0.1.0") (h "0m7gll55mxjjmnzkwkj830mp7lbh0r08h34y9jqy93i4zz0cvwfa")))

(define-public crate-robust-0.1.1 (c (n "robust") (v "0.1.1") (h "1nfim9fhsqyr5vqbn33kyqw2m91i0jybr4ywjp9950z2m61q4ysv")))

(define-public crate-robust-0.1.2 (c (n "robust") (v "0.1.2") (h "13ppwbnb28gc8iq343mn4v95df51ib8gppnwmw093g8aapjma892")))

(define-public crate-robust-0.2.0 (c (n "robust") (v "0.2.0") (h "0b97frxwl33qh8mz9bb016azy9vc95jxhq3bmp80s1yv3aaz27gr")))

(define-public crate-robust-0.2.1 (c (n "robust") (v "0.2.1") (h "1zl32b1cw42r575j06gs1apjc0q1xm99z28aj8wafnrbw7gcyxxw")))

(define-public crate-robust-0.2.2 (c (n "robust") (v "0.2.2") (h "1j7srgkk3rc0zp04ppn4g8s8nxrhl3xpn12llc4kp6a1s8xyp9ni")))

(define-public crate-robust-0.2.3 (c (n "robust") (v "0.2.3") (d (list (d (n "float_extras") (r "^0.1.6") (d #t) (k 2)) (d (n "ieee754") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 2)))) (h "1sjjyli1b69wcq50ssjla8xys977cmg6agyasvqvrdx6y5z4x1p5") (f (quote (("no_std" "ieee754"))))))

(define-public crate-robust-1.0.0 (c (n "robust") (v "1.0.0") (d (list (d (n "float_extras") (r "^0.1.6") (d #t) (k 2)) (d (n "ieee754") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 2)))) (h "0k8xzz4p3hxmg75dy15npcaq8z2f1v00z0qdr7336jx96snxli86") (f (quote (("no_std" "ieee754"))))))

(define-public crate-robust-1.1.0 (c (n "robust") (v "1.1.0") (d (list (d (n "float_extras") (r "^0.1.6") (d #t) (k 2)) (d (n "ieee754") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 2)))) (h "0c5fhzk7dc1ci9dyjy0xrv6nlrkbmprlj1lqkvrqhs3dbymadx6b") (f (quote (("no_std" "ieee754"))))))

