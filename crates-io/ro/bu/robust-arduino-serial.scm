(define-module (crates-io ro bu robust-arduino-serial) #:use-module (crates-io))

(define-public crate-robust-arduino-serial-0.1.0 (c (n "robust-arduino-serial") (v "0.1.0") (d (list (d (n "serial") (r "^0.4") (d #t) (k 0)) (d (n "std-semaphore") (r "^0.1") (d #t) (k 0)))) (h "0g3vpc8zwrazlkvj00r9z8gp94kdjj9lh6sbbqjr9nhswwf2pc7h")))

(define-public crate-robust-arduino-serial-0.1.1 (c (n "robust-arduino-serial") (v "0.1.1") (d (list (d (n "serial") (r "^0.4") (d #t) (k 0)) (d (n "std-semaphore") (r "^0.1") (d #t) (k 0)))) (h "1l2z9x5vlvxv7djxc64dd3nif3pg29v5naaampp6r500p83m0787")))

