(define-module (crates-io ro bu robust2d) #:use-module (crates-io))

(define-public crate-robust2d-0.1.0 (c (n "robust2d") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.106") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3.41") (d #t) (k 1)) (d (n "matches") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)))) (h "12cqbl4smp0pwi94a0vd6r1b50gicn4sabal2dyd1fn08hd11fr4") (f (quote (("default"))))))

