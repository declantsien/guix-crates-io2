(define-module (crates-io ro bu robust-git-bisect) #:use-module (crates-io))

(define-public crate-robust-git-bisect-0.1.0 (c (n "robust-git-bisect") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "robust-binary-search") (r "^0.1.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.8.0") (d #t) (k 0)) (d (n "union-find") (r "^0.3.2") (d #t) (k 0)))) (h "1l80qrmkzhcl0jd01bgi4yxw5jciwdf06sn4hgcpairxvglhsy6f")))

(define-public crate-robust-git-bisect-0.1.1 (c (n "robust-git-bisect") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "robust-binary-search") (r "^0.1.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.8.0") (d #t) (k 0)) (d (n "union-find") (r "^0.3.2") (d #t) (k 0)))) (h "0lyj9w0ahxyjhz02j9w3xw6ry4n339h0xmqblxd5dkbx7wf7vx6l")))

