(define-module (crates-io ro bu robusta_jni) #:use-module (crates-io))

(define-public crate-robusta_jni-0.0.3 (c (n "robusta_jni") (v "0.0.3") (d (list (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "robusta-codegen") (r "^0.0.3") (d #t) (k 0)))) (h "0rf9h44rlbf5vbcl54phwzlzvqg3j6vwv3fxisd0vhkk1s3bcziz") (f (quote (("no_jni" "robusta-codegen/no_jni"))))))

(define-public crate-robusta_jni-0.1.0 (c (n "robusta_jni") (v "0.1.0") (d (list (d (n "jni") (r "^0.17.0") (f (quote ("invocation"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "robusta-codegen") (r "^0.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "099807a2vfn2sr3mb6gm3g4m450ch9l0lf1gwnlyif5w9fi2x83j")))

(define-public crate-robusta_jni-0.2.0 (c (n "robusta_jni") (v "0.2.0") (d (list (d (n "jni") (r "^0.17.0") (f (quote ("invocation"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "robusta-codegen") (r "^0.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1y019dm9jqf08b16srf7afr969jcbzk2z7zmwlxgjfgl0jikq7x6")))

(define-public crate-robusta_jni-0.2.1 (c (n "robusta_jni") (v "0.2.1") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (f (quote ("invocation"))) (d #t) (k 2)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "robusta-codegen") (r "^0.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1ss6skwy6k5qxdnzgi64i1d41ghfcxi2g8pjw23j0v2npf1am7vc")))

(define-public crate-robusta_jni-0.2.2 (c (n "robusta_jni") (v "0.2.2") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (f (quote ("invocation"))) (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "robusta-codegen") (r "^0.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "1qq4yf1gkrdvzv10a4k4g7c1py9a2iqkhh80wmznjcy71ip19060")))

