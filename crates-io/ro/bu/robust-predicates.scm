(define-module (crates-io ro bu robust-predicates) #:use-module (crates-io))

(define-public crate-robust-predicates-0.1.0 (c (n "robust-predicates") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "0nffx0nqxp257kki8msqj2mcg03r762f0wbflrdm8x81z40f8wkl")))

(define-public crate-robust-predicates-0.1.1 (c (n "robust-predicates") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "0gppigddi3s100h8sk37rysl4gyhk5dynjwh84p3hrq8mdwm63iw")))

(define-public crate-robust-predicates-0.1.2 (c (n "robust-predicates") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "0c8wwq0n3l1xglmaypzvsx98d7mryhpfdcrwjq3kdnwwc5ji6f5v")))

(define-public crate-robust-predicates-0.1.3 (c (n "robust-predicates") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "1xjy4a9gmqqyxwsh4gljn9i4prfazcmpw754sla4wwp46ndldnql")))

(define-public crate-robust-predicates-0.1.4 (c (n "robust-predicates") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "0izdizlsaw7zrakk939kf7zbl38faz5sl3swxnmhsc5vdc5xvjyx")))

