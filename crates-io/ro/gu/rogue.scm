(define-module (crates-io ro gu rogue) #:use-module (crates-io))

(define-public crate-rogue-0.0.1 (c (n "rogue") (v "0.0.1") (h "1spi8rhjs4mfrsvk9f5ps8yakkfk283pndh0dpi7yk56065dil9w")))

(define-public crate-rogue-0.0.2 (c (n "rogue") (v "0.0.2") (h "0k5kr0ns5xpg4m8p0zv8jrcmpich22q9d1b80j6gb04skjkhrhvl")))

