(define-module (crates-io ro gu rogue-lang) #:use-module (crates-io))

(define-public crate-rogue-lang-0.0.0 (c (n "rogue-lang") (v "0.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "rug") (r "^1.18.0") (d #t) (k 0)))) (h "1ncwwrkk9q24nwsaycsrjviy44x3y370f49vpp7nc5g1qqvhkn0g") (y #t)))

