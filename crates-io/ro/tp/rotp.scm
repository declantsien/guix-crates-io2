(define-module (crates-io ro tp rotp) #:use-module (crates-io))

(define-public crate-rotp-0.1.0 (c (n "rotp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "0dmaascj7irr8nygr48p2nkqimp9xawykz4akm3fqhgk69ljdlxy")))

(define-public crate-rotp-0.1.1 (c (n "rotp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "0wwlc9vsldf5cfzz0ahz6l4bs40q8sm3mlqk9l7jadlm9b4f6rij")))

(define-public crate-rotp-0.1.2 (c (n "rotp") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "1y50wh0v423bbvar9rsqsvxsqjci2w49m9lxb0zjlbnkcdgmqwmf")))

(define-public crate-rotp-0.1.3 (c (n "rotp") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "1hy4laljlf66nvcad50sfbbxvn8c50723qb8hzpjyy4xkv7g7sxy")))

(define-public crate-rotp-0.2.0 (c (n "rotp") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "11aqr9f1qp0vxchxrpynq8ikq720wm8jlsms412s9ywlfaxnhzvx")))

(define-public crate-rotp-0.2.1 (c (n "rotp") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.5.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "ring") (r "^0.17.7") (d #t) (k 0)))) (h "0jky5y313d09g8lqlfpl5kgvs9rnva289kki73iz29mcgw8v2yhd")))

