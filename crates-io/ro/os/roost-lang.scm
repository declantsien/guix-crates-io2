(define-module (crates-io ro os roost-lang) #:use-module (crates-io))

(define-public crate-roost-lang-1.2.0 (c (n "roost-lang") (v "1.2.0") (d (list (d (n "rust_decimal") (r "^1.20.0") (f (quote ("maths"))) (d #t) (k 0)))) (h "0vkkjin07x2cmv7xgvcahrjlxd9wjm34rvy7slqbwypbvc997c0l") (f (quote (("no_std_io"))))))

(define-public crate-roost-lang-2.0.0 (c (n "roost-lang") (v "2.0.0") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (f (quote ("maths"))) (d #t) (k 0)))) (h "1c1w6kjf7vvcnvzqw6mmf5c8617q4gmydnffk13inqmnm7cwk5fn") (f (quote (("no_std_io"))))))

(define-public crate-roost-lang-2.0.1 (c (n "roost-lang") (v "2.0.1") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (f (quote ("maths"))) (d #t) (k 0)))) (h "0ncjb06pahyisq2pnllrvzajr7x83jj4bkmf984ysxm020vxmaiz") (f (quote (("no_std_io"))))))

(define-public crate-roost-lang-2.0.2 (c (n "roost-lang") (v "2.0.2") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (f (quote ("maths"))) (d #t) (k 0)))) (h "1ni03hmjz5l11b95rzmi8h9ic4fbs44n37ly2w7r5y5h73bmqfhy") (f (quote (("no_std_io"))))))

(define-public crate-roost-lang-2.0.3 (c (n "roost-lang") (v "2.0.3") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (f (quote ("maths"))) (d #t) (k 0)))) (h "0fywkyr9dvz9j0zd0kf0wzz5zp7pbbjnzfpxagpy6010chyxfsjv") (f (quote (("no_std_io"))))))

(define-public crate-roost-lang-2.0.4 (c (n "roost-lang") (v "2.0.4") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (f (quote ("maths"))) (d #t) (k 0)))) (h "06v7s8gr1kfgr8dd8w97dpqf65a641qri6rjjbyf7iajncfwffil") (f (quote (("no_std_io"))))))

