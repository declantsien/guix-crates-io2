(define-module (crates-io ro os roosterteeth_rs) #:use-module (crates-io))

(define-public crate-roosterteeth_rs-0.1.0 (c (n "roosterteeth_rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19b62crkp379apa66imnmwcqbdrfpglnr9h76kdil2whrkmzhd3q")))

(define-public crate-roosterteeth_rs-0.3.0 (c (n "roosterteeth_rs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fpdglmq5zd802b15qk9ymv93xd2nfb0im4b08idj4f08m751hsd")))

(define-public crate-roosterteeth_rs-0.3.1 (c (n "roosterteeth_rs") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ph4q2y6d24ngy8y5ml9y5941w8wa73n85v1flkpq8ld53082qah")))

