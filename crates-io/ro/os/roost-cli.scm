(define-module (crates-io ro os roost-cli) #:use-module (crates-io))

(define-public crate-roost-cli-1.2.0 (c (n "roost-cli") (v "1.2.0") (d (list (d (n "ntest") (r "^0.7.3") (d #t) (k 2)) (d (n "plist") (r "^1.3.1") (d #t) (k 2)) (d (n "roost") (r "^1.2.0") (d #t) (k 0) (p "roost-lang")) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "syntect") (r "^4.6.0") (f (quote ("default-fancy"))) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 2)))) (h "06ihqh2dkxslw5d1c72gfm5hcw50p87x9ar54l135rl2wd6mrlrw")))

(define-public crate-roost-cli-2.0.0 (c (n "roost-cli") (v "2.0.0") (d (list (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ntest") (r "^0.8.1") (d #t) (k 2)) (d (n "plist") (r "^1.3.1") (d #t) (k 2)) (d (n "roost") (r "^2.0.0") (d #t) (k 0) (p "roost-lang")) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)) (d (n "syntect") (r "^5.0.0") (f (quote ("default-fancy"))) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 2)))) (h "0vl46hdx9md5d3k7qgvl2g60r1q8dj7qa31fkj1rq8kyz1bsvxw5")))

