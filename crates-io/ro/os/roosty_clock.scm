(define-module (crates-io ro os roosty_clock) #:use-module (crates-io))

(define-public crate-roosty_clock-0.0.0 (c (n "roosty_clock") (v "0.0.0") (h "01yrsm6vjyj2vrhdjkl27549gwwdq18ifpc01dv3binx8h0vp17c")))

(define-public crate-roosty_clock-0.1.0 (c (n "roosty_clock") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (f (quote ("dark-light"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rfd") (r "^0.11.3") (d #t) (k 0)) (d (n "simple_file_logger") (r "^0.3.1") (d #t) (k 0)))) (h "0pabk3ld3fnhnd8scw753hykq43srbxqlh5f9fsr3y4ikraaw0y9")))

