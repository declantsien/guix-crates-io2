(define-module (crates-io ro ut routerify-unixsocket) #:use-module (crates-io))

(define-public crate-routerify-unixsocket-0.1.0 (c (n "routerify-unixsocket") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 2)) (d (n "hyper") (r "^0.14.4") (d #t) (k 0)) (d (n "hyperlocal") (r "^0.8.0") (d #t) (k 0)) (d (n "routerify") (r "^2.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("net" "macros"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.6") (f (quote ("util"))) (d #t) (k 0)))) (h "1656w8vjm8f959fibkgnh7gx5zmhmbmj8n7hyciyckmawhsxlsyj")))

(define-public crate-routerify-unixsocket-0.1.1 (c (n "routerify-unixsocket") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 2)) (d (n "hyper") (r "^0.14.4") (d #t) (k 0)) (d (n "hyperlocal") (r "^0.8.0") (d #t) (k 0)) (d (n "routerify") (r "^2.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("net" "macros"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.6") (f (quote ("util"))) (d #t) (k 0)))) (h "1cgbm7bhqyzgpsyks9f013kd87k3b96is6h5c3hrq1qrkvwkanbp")))

(define-public crate-routerify-unixsocket-3.0.0 (c (n "routerify-unixsocket") (v "3.0.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "hyper") (r "^0.14.16") (d #t) (k 0)) (d (n "hyperlocal") (r "^0.8.0") (d #t) (k 0)) (d (n "routerify") (r "^3.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("net" "macros"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.11") (f (quote ("util"))) (d #t) (k 0)))) (h "0hawn3dyw3imry2c82vwki0sz6jcrvnrw88bypafwbwdb6lqhijr")))

