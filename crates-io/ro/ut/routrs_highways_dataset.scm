(define-module (crates-io ro ut routrs_highways_dataset) #:use-module (crates-io))

(define-public crate-routrs_highways_dataset-0.1.0 (c (n "routrs_highways_dataset") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0x35q7pr53vd7szwdhl07q4vl2bir2yiqbjnqinh3c2wnvwf3jn4")))

