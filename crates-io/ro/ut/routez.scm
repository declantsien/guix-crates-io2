(define-module (crates-io ro ut routez) #:use-module (crates-io))

(define-public crate-routez-0.0.1 (c (n "routez") (v "0.0.1") (h "1l8adqqni86l6ilbgk1pcby7qy1s463ksnvmwm117911lhd7irpc") (y #t)))

(define-public crate-routez-0.0.2 (c (n "routez") (v "0.0.2") (h "089rvlmhfhwighzs6hpkrknrqmqzj1pyl4h93w08ijlkcvd0kliw") (y #t)))

(define-public crate-routez-0.0.3 (c (n "routez") (v "0.0.3") (h "0kvzwln59qdvs53ssch0qzf1ssgbiv7zjv5qxqmczfajzl3gaz71") (y #t)))

(define-public crate-routez-0.1.0 (c (n "routez") (v "0.1.0") (h "1vycfs1z56sni0sn21rmk5q4dazjqbrh42gzfk0kdygxw19lbyxv")))

(define-public crate-routez-0.2.0 (c (n "routez") (v "0.2.0") (h "073cxky8kcy1xcm0y7zbykp5lqzvy0h7lm3dk8l8p9y7x2kg84h7")))

(define-public crate-routez-0.3.0 (c (n "routez") (v "0.3.0") (h "1jp4n1jbykk35s2v979iayjzv8ndiv43bksbqgnphmz5c888jsd2")))

(define-public crate-routez-0.3.1 (c (n "routez") (v "0.3.1") (h "0arp8a64fj9ipd1y3vc91jsjchfa4awiamr9pshhwk7ydanjryvm")))

