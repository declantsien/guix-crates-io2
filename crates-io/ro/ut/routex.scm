(define-module (crates-io ro ut routex) #:use-module (crates-io))

(define-public crate-routex-0.1.0 (c (n "routex") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jjr6c9c0kpfvj44w1fa6xnkzjdm8m2ja1y4fagli21jffcsm6mi")))

(define-public crate-routex-0.1.1 (c (n "routex") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pzc7z1gnqk90d0fkmsaal9spa9xb1inyq7d2g5lnn6wgmg5s1ns")))

(define-public crate-routex-0.1.4 (c (n "routex") (v "0.1.4") (d (list (d (n "ipnetwork") (r "^0.20") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "netlink-packet-core") (r "^0.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "netlink-packet-route") (r "^0.19") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1sy7ysr13vzvyj4zghkhglifdj94cxgb1f7l0rn8sdc26awazq5f")))

(define-public crate-routex-0.1.5 (c (n "routex") (v "0.1.5") (d (list (d (n "ipnetwork") (r "^0.20") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "netlink-packet-core") (r "^0.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "netlink-packet-route") (r "^0.19") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0gbjng4zaqf8gbsv6708vp9459csp56gb9nsx9zd9lpdz1ry8scd")))

