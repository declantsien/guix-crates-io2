(define-module (crates-io ro ut router-rs-macro) #:use-module (crates-io))

(define-public crate-router-rs-macro-0.1.0 (c (n "router-rs-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xv8wqc6v9x1l9mqppa7whs18yhz67vnan34rz4nnhdygq445hf4")))

(define-public crate-router-rs-macro-0.1.1 (c (n "router-rs-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ac64hjsgnfdgjai4wxszjjxlkqfv78ir76ffapw0760chqp0cpj")))

