(define-module (crates-io ro ut routem) #:use-module (crates-io))

(define-public crate-routem-0.1.0 (c (n "routem") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)))) (h "14ygyilr9l6hh1lnrn23417b0ih01ap3a6avfmmmr8jdpp4xz8pl")))

(define-public crate-routem-0.2.0 (c (n "routem") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)))) (h "0jqrryrnyjr4ahn0cr7n0x3pvc9f60xn2aj2sf32fx06zy8vygcn")))

(define-public crate-routem-0.3.0 (c (n "routem") (v "0.3.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)))) (h "07x4nqkqwxcmmv43nij50vyri1i74ay560giivp2l57dr27f4nxf")))

(define-public crate-routem-0.4.0 (c (n "routem") (v "0.4.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (d #t) (k 0)))) (h "08v7bbwpaljc1qp0qm5m16lagvld41yd8myxqnpph4wqsg79lxyb")))

