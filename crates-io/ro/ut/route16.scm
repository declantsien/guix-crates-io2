(define-module (crates-io ro ut route16) #:use-module (crates-io))

(define-public crate-Route16-0.0.1 (c (n "Route16") (v "0.0.1") (d (list (d (n "bit-set") (r "^0.5.1") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.3.0") (d #t) (k 0)) (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "osmpbfreader") (r "^0.13.3") (d #t) (k 0)) (d (n "priority-queue") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)))) (h "0yvdqm5p2jzr1qqp9nmhvy8ghnvszr1v195v47xayy0109c83kw5") (y #t)))

