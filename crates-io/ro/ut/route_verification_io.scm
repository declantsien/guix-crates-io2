(define-module (crates-io ro ut route_verification_io) #:use-module (crates-io))

(define-public crate-route_verification_io-0.1.0 (c (n "route_verification_io") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("unbounded_depth"))) (d #t) (k 0)))) (h "0a91p9lsrhl9ym672p1h1j78liksq51dixhnkzp8b7c5vx5x0rnw")))

