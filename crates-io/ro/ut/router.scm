(define-module (crates-io ro ut router) #:use-module (crates-io))

(define-public crate-router-0.0.2 (c (n "router") (v "0.0.2") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)))) (h "154r23d6c1lwm5ifv4qc19gi4cli0yl1ivp810z8sz840wxjximd")))

(define-public crate-router-0.0.3 (c (n "router") (v "0.0.3") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)))) (h "0hv6z4f705di0gsjxn21jp15wckazs46c0484173bd1996msk50k")))

(define-public crate-router-0.0.4 (c (n "router") (v "0.0.4") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)))) (h "1dhw178rfaghv08jdh53s5xn7hrcxfcnbqb3zzn8zcv7qjc4gz9p")))

(define-public crate-router-0.0.5 (c (n "router") (v "0.0.5") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)))) (h "1wykjn33jf9x43caqxbzi1psilz1spj4x0hdlsfz5j60mb1pkqdy")))

(define-public crate-router-0.0.6 (c (n "router") (v "0.0.6") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)))) (h "053gxpifhbiw4bxnkfa8c253yix0zdfrsjc4g3db4zbrj9ixdqyj")))

(define-public crate-router-0.0.7 (c (n "router") (v "0.0.7") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)))) (h "0cgp4dw14pm0yk2r8kjr3pnvba99hh28b9fzinp3qlkkf3szl6vd")))

(define-public crate-router-0.0.8 (c (n "router") (v "0.0.8") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)))) (h "04009hl42n2hsqyrlsg7axflj6igz2xv0yznmyz78w9nz483672c")))

(define-public crate-router-0.0.9 (c (n "router") (v "0.0.9") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)))) (h "0sdyl5gm25hnp8nq8skkypa7fsn8c4hjr9xzr8a373blpcw6lvx9")))

(define-public crate-router-0.0.10 (c (n "router") (v "0.0.10") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)))) (h "14mxhz2206w7hd107rfp0pk3xbwywnbsysn38p0qh41sb01xbwy8")))

(define-public crate-router-0.0.11 (c (n "router") (v "0.0.11") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)))) (h "0jrq3v00fp8vyw8xd9df0zg3mldb9aycsjjaq0f2z1v328vdbpxz")))

(define-public crate-router-0.0.12 (c (n "router") (v "0.0.12") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)))) (h "04jf8b65qgz8yqhq43b25sa65qb11wmna3jbyqn5m6li7di1f1jx")))

(define-public crate-router-0.0.13 (c (n "router") (v "0.0.13") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)))) (h "1fmblny8mh5d64r4jv08ilpa41cc7vz2mx5nzg5rqf2bignwikrd")))

(define-public crate-router-0.0.14 (c (n "router") (v "0.0.14") (d (list (d (n "iron") (r "^0.1") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)))) (h "1a0a2vya4nv1r6ca48ypbd46r3a2k85c718jkq6zzfxg9rjkhasa")))

(define-public crate-router-0.0.15 (c (n "router") (v "0.0.15") (d (list (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)))) (h "0b7p6sg7i7a1fh6x1jxpi1vv2bp9nfpdvgij41jwkxaif4w0npxf")))

(define-public crate-router-0.0.16 (c (n "router") (v "0.0.16") (d (list (d (n "iron") (r "^0.2") (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)))) (h "1015aj20n9awsqr52294830zckia8gqx2f059b37hd3qjh56kq5i")))

(define-public crate-router-0.0.17 (c (n "router") (v "0.0.17") (d (list (d (n "iron") (r "^0.2") (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)))) (h "08g8qna99jj26jx6lmsnb91x919g7kq78ajs3dbn34xsp0pk47jr")))

(define-public crate-router-0.1.0 (c (n "router") (v "0.1.0") (d (list (d (n "iron") (r "^0.2") (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)))) (h "03cwl342nnaj2421hxqv2ddxjjnnilxzsgpka944c15n8pnfpr3p")))

(define-public crate-router-0.1.1 (c (n "router") (v "0.1.1") (d (list (d (n "iron") (r ">= 0.2, < 0.4") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)))) (h "1lvq4fifzw8592gip6wcxbai3h5y0nryl48li5y6kg74qb1grh1z")))

(define-public crate-router-0.2.0 (c (n "router") (v "0.2.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)))) (h "0hlbz6shaljrgika6l5m8kn27paxbhvikvadc1afymyw2fhmnrpz")))

(define-public crate-router-0.3.0 (c (n "router") (v "0.3.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "17q3j4vsa3vn3xs30b98x0l0mf0j2kgx8fiiqk9a6x1r8m03r74i")))

(define-public crate-router-0.3.1 (c (n "router") (v "0.3.1") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "1ir4y3qvmkmp86b8h5j95fi9d8rpzwbfzyrjiikl0sicbzk3y5bw")))

(define-public crate-router-0.3.2 (c (n "router") (v "0.3.2") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "10brh0681iz1aprm6x2lxz6c34pq5g55dj3dlkn9lszmv3cr9afw")))

(define-public crate-router-0.3.3 (c (n "router") (v "0.3.3") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "0rlzn0k3y8l0knvcjm7yq6sv165anz403cq77nsid2cb1hz855bl")))

(define-public crate-router-0.3.4 (c (n "router") (v "0.3.4") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "0a0ccsfy6qrspbgxav7bmz80ib7y8ffbiqycl33zzfxhraaiwg8p")))

(define-public crate-router-0.4.0 (c (n "router") (v "0.4.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "09ppdpasln1967nlcp73n9s1w73w19b15np4bcvv8wmplnzrfhxr")))

(define-public crate-router-0.5.0 (c (n "router") (v "0.5.0") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "07hig4p95rscwq8f2fmpi2xmyj3v8szqy6jsjrzmf3y2avhfzfcy")))

(define-public crate-router-0.5.1 (c (n "router") (v "0.5.1") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "0kjfb3q9qp7hqqja6xfgnihlkrcn4rabayr36av9q0k6y5zpkcdr")))

(define-public crate-router-0.6.0 (c (n "router") (v "0.6.0") (d (list (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "15vksm3p60cfmgqbijx3rid2vfpxb2mb3chnx020snw9p3rvcqyw")))

