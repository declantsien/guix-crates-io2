(define-module (crates-io ro ut rout) #:use-module (crates-io))

(define-public crate-rout-0.1.0 (c (n "rout") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "ap") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_consume") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "0h4caw55klwayay63svxnih6yvjvjh1rkcllaa97md2ns3b98d6i")))

