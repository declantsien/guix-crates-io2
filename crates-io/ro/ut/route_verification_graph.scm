(define-module (crates-io ro ut route_verification_graph) #:use-module (crates-io))

(define-public crate-route_verification_graph-0.1.0 (c (n "route_verification_graph") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ir") (r "^0.2.0") (d #t) (k 0) (p "route_verification_ir")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (k 0)))) (h "063243lnzc4d2vx23878073bg0aldssh6bvwhplc9cbarmr39fw8")))

