(define-module (crates-io ro ut routerify-cors) #:use-module (crates-io))

(define-public crate-routerify-cors-1.0.0 (c (n "routerify-cors") (v "1.0.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "routerify") (r "^1.0") (d #t) (k 0)) (d (n "stream-body") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0c0gvn0js06w6w4bxvl9x2f1kwa2c0bg1mrnkldm7nl1kzvk2ygb")))

(define-public crate-routerify-cors-1.1.0 (c (n "routerify-cors") (v "1.1.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "routerify") (r "^1.1") (d #t) (k 0)) (d (n "stream-body") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0s6rv94g1nb2h9fw6n40abw4nq2k4fzj3p2qr7f3pmbnranwnmm9")))

(define-public crate-routerify-cors-2.0.0 (c (n "routerify-cors") (v "2.0.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "routerify") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1s46v5y93fscivz4di22zx5cyra2zyfjmwi2lyx1bglvg2k0459k")))

(define-public crate-routerify-cors-3.0.0 (c (n "routerify-cors") (v "3.0.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "routerify") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0i0jhiapd0wi0mwaqp661jlqmw8ibs3vdmlk59c7qzck6bblr12k")))

