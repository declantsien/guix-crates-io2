(define-module (crates-io ro ut routee-compass-py) #:use-module (crates-io))

(define-public crate-routee-compass-py-0.2.0 (c (n "routee-compass-py") (v "0.2.0") (d (list (d (n "pyo3") (r "^0.19") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "routee-compass") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hdr96q033ng424yq7ly6cn6lqivy0l0jzsafyb634rmijdnx5ny")))

(define-public crate-routee-compass-py-0.3.0 (c (n "routee-compass-py") (v "0.3.0") (d (list (d (n "pyo3") (r "^0.19") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "routee-compass") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rggkzl96knqknl91pby1zlp4jvqlk1b6madkhkm4iwd14nj22da")))

(define-public crate-routee-compass-py-0.4.0 (c (n "routee-compass-py") (v "0.4.0") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "routee-compass") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0znzkgh2xcc5xw0ii56gp60zbizhny1hga6dl44pfgwbjw4019h8")))

(define-public crate-routee-compass-py-0.5.0 (c (n "routee-compass-py") (v "0.5.0") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "routee-compass") (r "^0.5.0") (d #t) (k 0)) (d (n "routee-compass-core") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17k6f6piy0lbgfd7dmnipgn9zwi0iaxvs4x10qjp9yns1gqvzn08")))

(define-public crate-routee-compass-py-0.6.0 (c (n "routee-compass-py") (v "0.6.0") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "routee-compass") (r "^0.6.0") (d #t) (k 0)) (d (n "routee-compass-core") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bf6hq1hi7kxj9h0h90wcr6pip948mx1c9f3z2il80a4z1x2iv62")))

(define-public crate-routee-compass-py-0.6.1 (c (n "routee-compass-py") (v "0.6.1") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "routee-compass") (r "^0.6.1") (d #t) (k 0)) (d (n "routee-compass-core") (r "^0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15ycagjq8jsi3xrnykvnncwhg9zg13kzgrbbwxka710jbrrsz166")))

