(define-module (crates-io ro ut routerify-query) #:use-module (crates-io))

(define-public crate-routerify-query-1.0.0 (c (n "routerify-query") (v "1.0.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "routerify") (r "^1.0") (d #t) (k 0)) (d (n "stream-body") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "07gpcss9ylbnybqqmv3g16bfikwvqp963my84mlzzzxhvs5mav9a")))

(define-public crate-routerify-query-1.1.0 (c (n "routerify-query") (v "1.1.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "routerify") (r "^1.1") (d #t) (k 0)) (d (n "stream-body") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0jlch6w1yi3rgpqxas4fq27800iyzvpp555mm9z7ja65adrf25s3")))

(define-public crate-routerify-query-2.0.0 (c (n "routerify-query") (v "2.0.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "routerify") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0rm41fyvnylk6rwgfrd0iarbqz552wmrvrh7h9xvppqjwrgs6msc")))

(define-public crate-routerify-query-3.0.0 (c (n "routerify-query") (v "3.0.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "routerify") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "010kxsrpbi4ab5x393x5idaxc6nlf49ij0rf370djq9z9kvgh3zy")))

