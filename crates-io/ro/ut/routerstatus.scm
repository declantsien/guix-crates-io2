(define-module (crates-io ro ut routerstatus) #:use-module (crates-io))

(define-public crate-routerstatus-1.0.0 (c (n "routerstatus") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "1rpbqyvzy5vvsjvw7nya5qbrindryxygf72mhy1hrh55snxaf4as") (y #t)))

