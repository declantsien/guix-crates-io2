(define-module (crates-io ro ut router-hello) #:use-module (crates-io))

(define-public crate-router-hello-0.2.0 (c (n "router-hello") (v "0.2.0") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0b22k3d5i6bjg9hfcww986pay1lryz6gw1h4yhx0hqkarb2xalaq")))

(define-public crate-router-hello-0.2.1 (c (n "router-hello") (v "0.2.1") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0v9gjids0bn6qfrx2mwvhzahrb2fj4klqs6f7hiiy10mxk720r1v")))

