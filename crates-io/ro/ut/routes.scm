(define-module (crates-io ro ut routes) #:use-module (crates-io))

(define-public crate-routes-0.1.0-dev (c (n "routes") (v "0.1.0-dev") (h "17sw5r4pjmdgr2gcx1apndcfdwynafmc6k9l4r81xqgnk3qvnm13")))

(define-public crate-routes-0.1.0-rc0 (c (n "routes") (v "0.1.0-rc0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2.31") (d #t) (k 0)) (d (n "easy-repl") (r "^0.2.1") (d #t) (k 0)) (d (n "routecore") (r "^0.4.0-rc0") (f (quote ("bmp"))) (d #t) (k 0)))) (h "0ky5lkkrafya1vc4fpa6x7wl9p41i1rj4dcvag9v3hjkmfkdybd5")))

(define-public crate-routes-0.1.0 (c (n "routes") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2.31") (d #t) (k 0)) (d (n "easy-repl") (r "^0.2.1") (d #t) (k 0)) (d (n "routecore") (r "^0.4.0") (f (quote ("bmp"))) (d #t) (k 0)))) (h "08ck0fks9fm7r98mzaj5n8jllgyw0dhhjj848qid16br5i694l14")))

