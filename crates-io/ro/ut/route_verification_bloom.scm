(define-module (crates-io ro ut route_verification_bloom) #:use-module (crates-io))

(define-public crate-route_verification_bloom-0.1.0 (c (n "route_verification_bloom") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (f (quote ("rayon" "raw"))) (d #t) (k 0)))) (h "11jchaifyyz1kggx0wyk77n75pxp0f12ixr4ffxg5kk3cbdwk2ak")))

(define-public crate-route_verification_bloom-0.1.1 (c (n "route_verification_bloom") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (f (quote ("rayon" "raw"))) (d #t) (k 0)))) (h "145y0v83pj9zbwblc4d5pan1759gvdf8m8yks18lycgw1xj02d95")))

