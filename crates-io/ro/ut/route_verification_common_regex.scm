(define-module (crates-io ro ut route_verification_common_regex) #:use-module (crates-io))

(define-public crate-route_verification_common_regex-0.1.0 (c (n "route_verification_common_regex") (v "0.1.0") (d (list (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "097md70qvl7m99dkal702z0qlz06dq31yrv0l7z8dfi7w5b9w1kj")))

