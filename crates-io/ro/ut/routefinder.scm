(define-module (crates-io ro ut routefinder) #:use-module (crates-io))

(define-public crate-routefinder-0.1.0 (c (n "routefinder") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0dyl4h019ncks50dbqhx71c3npx0y7w8wbpf67c0ai78q0ckn0h5")))

(define-public crate-routefinder-0.1.1 (c (n "routefinder") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1khwhxksv5kfq6a1sr1ldgjm4wzaviwxsi6lrjq9ik0vhmx5s6p7")))

(define-public crate-routefinder-0.2.0 (c (n "routefinder") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1hiargyzcwss4rp4cpnb9rrac46ayz937xlyq3h0yp0hw4jj599y")))

(define-public crate-routefinder-0.3.0 (c (n "routefinder") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0gn4vznp096q56b3hk122yl3shg06h2mvcdzl3g5a9pzx091dwr2")))

(define-public crate-routefinder-0.3.1 (c (n "routefinder") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0zbfj5cwk5pn569j55w6zzw3ga0kw7b9wnvawg8xa6yb7hv60a69")))

(define-public crate-routefinder-0.4.0 (c (n "routefinder") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "smartcow") (r "^0.1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.9") (d #t) (k 0)))) (h "1d0djihc4mvqc5n9wbmg2d8q1q3y6l7znp6s45q1mr07qrfgjkm4")))

(define-public crate-routefinder-0.5.0 (c (n "routefinder") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "smartcow") (r "^0.1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.9") (d #t) (k 0)))) (h "03gqgnh0csl8l17vyfccfsz6r47lgvhvyi88li540y2279dzjppm")))

(define-public crate-routefinder-0.5.1 (c (n "routefinder") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "smartcow") (r "^0.1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.9") (d #t) (k 0)))) (h "1n0civpm08kdg5ps282zqq6b0w3w2g711p7nbpajzvndyxnha2j8")))

(define-public crate-routefinder-0.5.2 (c (n "routefinder") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "smartcow") (r "^0.2.0") (d #t) (k 0)) (d (n "smartstring") (r "^1.0.0") (d #t) (k 0)))) (h "199fjpxwhkkfk9ybs1zilq33k8j5jpgir8v41px7lqnlnxzsprjl")))

(define-public crate-routefinder-0.5.3 (c (n "routefinder") (v "0.5.3") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "smartcow") (r "^0.2.1") (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)))) (h "1jia5377jj56960zgqcnmi538kn1lwgxlg952isk3pfy22dzky4l")))

(define-public crate-routefinder-0.5.4 (c (n "routefinder") (v "0.5.4") (d (list (d (n "criterion") (r "^0.5.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2.6.4") (o #t) (d #t) (k 0)) (d (n "smartcow") (r "^0.2.1") (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)))) (h "1db6w2bqix8gjpgs74hs73kr6xgs9bf2yy0dppb6fqisjk4d6w89") (f (quote (("default")))) (s 2) (e (quote (("memchr" "dep:memchr"))))))

