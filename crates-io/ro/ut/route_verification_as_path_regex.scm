(define-module (crates-io ro ut route_verification_as_path_regex) #:use-module (crates-io))

(define-public crate-route_verification_as_path_regex-0.1.0 (c (n "route_verification_as_path_regex") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "common_regex") (r "^0.1.0") (d #t) (k 0) (p "route_verification_common_regex")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kq8mb3lqx2vpnhs5xk8fxmdbbnxx7v8h75p32i6jrbg1mv8wlf0")))

(define-public crate-route_verification_as_path_regex-0.2.0 (c (n "route_verification_as_path_regex") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 2)) (d (n "common_regex") (r "^0.1.0") (d #t) (k 0) (p "route_verification_common_regex")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14g4kx9rjyfp4q9nj8kj4nd2sw6dyzix58hll0znvx9z8r71p88b")))

