(define-module (crates-io ro ut router-rs) #:use-module (crates-io))

(define-public crate-router-rs-0.2.0 (c (n "router-rs") (v "0.2.0") (d (list (d (n "router-rs-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "virtual-dom-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1j31v43zvxrmkqykn2ya5bp5g6h4zl4124idxbvjlzl1xp5h3lfk")))

(define-public crate-router-rs-0.2.1 (c (n "router-rs") (v "0.2.1") (d (list (d (n "router-rs-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "virtual-dom-rs") (r "^0.6.0") (d #t) (k 0)))) (h "12i49krlx53k13fm769hd8ffs1sgdrml5j9j3z80hy9yr2v6h5fc")))

