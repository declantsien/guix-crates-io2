(define-module (crates-io ro ut route_verification_shared_struct) #:use-module (crates-io))

(define-public crate-route_verification_shared_struct-0.1.0 (c (n "route_verification_shared_struct") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16gazj4z5cfdsfpi8yb2igd3d8inj84x16clzgh19fkafq8vxbmx")))

(define-public crate-route_verification_shared_struct-0.1.1 (c (n "route_verification_shared_struct") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "028zjnym4wnisqwhj665yj8jh90xc1gmfl21z1c6xjksi3hpqflj")))

(define-public crate-route_verification_shared_struct-0.2.0 (c (n "route_verification_shared_struct") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06mvxjrs1rk9nb0kpcb77mfz3mdj26m26zh1amywv7r826v9iq37")))

