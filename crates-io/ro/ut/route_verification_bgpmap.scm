(define-module (crates-io ro ut route_verification_bgpmap) #:use-module (crates-io))

(define-public crate-route_verification_bgpmap-0.1.0 (c (n "route_verification_bgpmap") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "ipnet") (r "^2.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "197a5y198d4vq1nwykkl1ld1jld52iyv2pcwb0c67ri2ld1pgai7")))

