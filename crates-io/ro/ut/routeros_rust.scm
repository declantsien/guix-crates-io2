(define-module (crates-io ro ut routeros_rust) #:use-module (crates-io))

(define-public crate-routeros_rust-0.0.2 (c (n "routeros_rust") (v "0.0.2") (d (list (d (n "rust-crypto") (r "^0.2.31") (d #t) (k 0)))) (h "0qj4i4sfgb1j2yw06dzm89k0l482vb9szn4q9x4ly262w57sdxh2")))

(define-public crate-routeros_rust-0.0.21 (c (n "routeros_rust") (v "0.0.21") (d (list (d (n "rust-crypto") (r "^0.2.31") (d #t) (k 0)))) (h "0rz3r6m95sqx5v35lvfc6flv587lk67bdjl7iv6zyg4rr7xm1xnv")))

