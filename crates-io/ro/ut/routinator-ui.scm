(define-module (crates-io ro ut routinator-ui) #:use-module (crates-io))

(define-public crate-routinator-ui-0.2.3 (c (n "routinator-ui") (v "0.2.3") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.35") (d #t) (k 1)))) (h "1hll9la93y1dsldwjjisp8vhmk7v0mxx0qh8yr5di0sz8bxyidyz")))

(define-public crate-routinator-ui-0.2.4 (c (n "routinator-ui") (v "0.2.4") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.35") (d #t) (k 1)))) (h "09j2ky4ckhxgwsg6q0qh74d1cv5l69ggg3azkq34ifsi47dwg6rr")))

(define-public crate-routinator-ui-0.2.5 (c (n "routinator-ui") (v "0.2.5") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.35") (d #t) (k 1)))) (h "1h5vmf5l9m942hwl8iarqsfqg1rk165s7fai5bdv4wm4vsgipj74")))

(define-public crate-routinator-ui-0.3.0 (c (n "routinator-ui") (v "0.3.0") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "rustls-tls"))) (k 1)) (d (n "tar") (r "^0.4.35") (d #t) (k 1)))) (h "1xdgl84kcng4mcrk8gbnjdiaqyyvhbzf3whrld18h1dlmq9k3j90")))

(define-public crate-routinator-ui-0.3.1 (c (n "routinator-ui") (v "0.3.1") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "rustls-tls"))) (k 1)) (d (n "tar") (r "^0.4.35") (d #t) (k 1)))) (h "1ljzd7pi54i98992wiacyj96yz63v94rn5wdm3jvlc96ndq6i57j")))

(define-public crate-routinator-ui-0.3.2 (c (n "routinator-ui") (v "0.3.2") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "rustls-tls"))) (k 1)) (d (n "tar") (r "^0.4.35") (d #t) (k 1)))) (h "1wx09lcgcg1rz72sdmdyygkqbfwvjvi9avwaxmlqvsr7fvxsl453")))

(define-public crate-routinator-ui-0.3.3 (c (n "routinator-ui") (v "0.3.3") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "rustls-tls"))) (k 1)) (d (n "tar") (r "^0.4.35") (d #t) (k 1)))) (h "0z34mhrigdr2fz47y6s162wazfii3n4k8agg2ayl6rjgal0kq8jq")))

(define-public crate-routinator-ui-0.3.4 (c (n "routinator-ui") (v "0.3.4") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking" "rustls-tls"))) (k 1)) (d (n "tar") (r "^0.4.35") (d #t) (k 1)))) (h "1flgzk1k936b9pvbdwqq567jxz42lsyhi009mccqrmgh6js3xfgj")))

