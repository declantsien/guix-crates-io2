(define-module (crates-io ro ut routrs_maritime_dataset) #:use-module (crates-io))

(define-public crate-routrs_maritime_dataset-0.1.0 (c (n "routrs_maritime_dataset") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1cvsdgbkp14614isrc0x1ww3nhqz3jh7p3n02f9f5iqqj2b9maw8")))

