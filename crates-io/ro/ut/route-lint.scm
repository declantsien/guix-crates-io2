(define-module (crates-io ro ut route-lint) #:use-module (crates-io))

(define-public crate-route-lint-0.1.0 (c (n "route-lint") (v "0.1.0") (d (list (d (n "annotate-snippets") (r "^0.9.1") (f (quote ("color"))) (d #t) (k 0)) (d (n "deno_ast") (r "^0.5.0") (f (quote ("transforms" "utils" "visit" "view"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "swc_ecmascript") (r "^0.82.0") (f (quote ("parser"))) (d #t) (k 0)))) (h "0wwx5dk2ls06l5qzirk72hpq54yfrb2niffwabqg3r8x6zj80b4b")))

