(define-module (crates-io ro ut route-recognizer) #:use-module (crates-io))

(define-public crate-route-recognizer-0.1.0 (c (n "route-recognizer") (v "0.1.0") (h "0r9986r54sa7agw0jqch8r1vh4cmkcd4p0c458498gpj3vx48r0q")))

(define-public crate-route-recognizer-0.1.1 (c (n "route-recognizer") (v "0.1.1") (h "0y7s271ixypw74fk14b5fbdxf09sj2z4qf0lkixppi2knw2h4arb")))

(define-public crate-route-recognizer-0.1.2 (c (n "route-recognizer") (v "0.1.2") (h "11bz1fa1j0dvqyi5y50sifq8nwyxfz3p3prngbgqf2wxxg6s333a")))

(define-public crate-route-recognizer-0.1.3 (c (n "route-recognizer") (v "0.1.3") (h "1b7xvcdrz92nmjjw6r5c5mybnbj8zcx6rap9i55kma913sq6ihpc")))

(define-public crate-route-recognizer-0.1.4 (c (n "route-recognizer") (v "0.1.4") (h "1qaw5qyczkywnxhlncbl77j0b3jfiirzriganxkqz5gfjiy2hkh7")))

(define-public crate-route-recognizer-0.1.5 (c (n "route-recognizer") (v "0.1.5") (h "02ldzj3psl6rf9am26fkd5mf3kpqmy43pfhj6cik15gjim5zqx01")))

(define-public crate-route-recognizer-0.1.6 (c (n "route-recognizer") (v "0.1.6") (h "1lqp0bhqzyv1z04mmv6hjrr2wz72xcy3bazmlvqx4dyji4fp04bs")))

(define-public crate-route-recognizer-0.1.7 (c (n "route-recognizer") (v "0.1.7") (h "0zg6iga5ivd6hp5lwmr8q266skajjsx6n844ljfzzl271y4yqrbh")))

(define-public crate-route-recognizer-0.1.8 (c (n "route-recognizer") (v "0.1.8") (h "0qm8s7xxi4cgk5wxwkpppzfk1zz17bgqbxqfrrdpv0xkmdlapmkr")))

(define-public crate-route-recognizer-0.1.9 (c (n "route-recognizer") (v "0.1.9") (h "0sb3bg0qipxfnbs8ax4dqbxjrm5xlj340gmi6wa5zglq2aiga6w1")))

(define-public crate-route-recognizer-0.1.10 (c (n "route-recognizer") (v "0.1.10") (h "1aw6xr4075l5irrk3m5skrxzpvj8cpwjkb14rhy0zz28ff10n1sz")))

(define-public crate-route-recognizer-0.1.11 (c (n "route-recognizer") (v "0.1.11") (h "0b8j00jgqqa9y2rwp9wxn2cmhn1hm2yafklnymw1knqa086pa2jg")))

(define-public crate-route-recognizer-0.1.12 (c (n "route-recognizer") (v "0.1.12") (h "0mi2rr11l63mbb03p5z2dhag2mimisdhmmk3p9383pw8h0rmacng")))

(define-public crate-route-recognizer-0.1.13 (c (n "route-recognizer") (v "0.1.13") (h "1y7ln295s0sjclfyj2yw084w6zan8q6jy46hrmm48g0bxdjr0l7a")))

(define-public crate-route-recognizer-0.2.0 (c (n "route-recognizer") (v "0.2.0") (h "17mmnyzg7yr5x84n28y6bll1qn21g11k61p6sgg2fjf0xdshcxsn")))

(define-public crate-route-recognizer-0.3.0 (c (n "route-recognizer") (v "0.3.0") (h "0ghrilccmfr23ydg9shdzr718wj5r455bx8574y7gwycmzq74hc2")))

(define-public crate-route-recognizer-0.3.1 (c (n "route-recognizer") (v "0.3.1") (h "0ikp3blbina00jdbifxw1c9whg6mljli24lq5pv82iar53xr9axg")))

