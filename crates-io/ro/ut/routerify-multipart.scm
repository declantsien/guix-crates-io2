(define-module (crates-io ro ut routerify-multipart) #:use-module (crates-io))

(define-public crate-routerify-multipart-1.0.1 (c (n "routerify-multipart") (v "1.0.1") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "multer") (r "^1.0") (d #t) (k 0)) (d (n "routerify") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "103chf7lwmfkkqagh9z5654z3ndc07jfxsad7bpbxkxblhxnpimf") (f (quote (("json" "multer/json") ("default") ("all" "json"))))))

(define-public crate-routerify-multipart-1.0.2 (c (n "routerify-multipart") (v "1.0.2") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "multer") (r "^1.1") (d #t) (k 0)) (d (n "routerify") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "08kxp93bcckbbimhsrp0wvcxzmcwz15286iwmb6r05cfj67vvcwd") (f (quote (("json" "multer/json") ("default") ("all" "json"))))))

(define-public crate-routerify-multipart-1.1.0 (c (n "routerify-multipart") (v "1.1.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "multer") (r "^1.2") (d #t) (k 0)) (d (n "routerify") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0mhm7s40hw2chhijisxy61mp7g4hsaaksw8vdw7q3s52j8qw7vf0") (f (quote (("json" "multer/json") ("default") ("all" "json"))))))

(define-public crate-routerify-multipart-2.0.0 (c (n "routerify-multipart") (v "2.0.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "multer") (r "^2") (d #t) (k 0)) (d (n "routerify") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1dnjx4jzypgfcngxn5plx5r35q1isqvpssjqz2xkyhzf4jz0x6mj") (f (quote (("json" "multer/json") ("default") ("all" "json"))))))

(define-public crate-routerify-multipart-3.0.0 (c (n "routerify-multipart") (v "3.0.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("stream"))) (k 0)) (d (n "multer") (r "^2") (d #t) (k 0)) (d (n "routerify") (r "^3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0qd8ywq9krn67x8ah5i88xdscymqjizikbf61nz04cw9a0ws0jq7") (f (quote (("json" "multer/json") ("default") ("all" "json"))))))

