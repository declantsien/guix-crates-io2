(define-module (crates-io ro ut router-xiaobei) #:use-module (crates-io))

(define-public crate-router-xiaobei-0.1.0 (c (n "router-xiaobei") (v "0.1.0") (d (list (d (n "dodrio") (r "^0.1.0") (d #t) (k 0)) (d (n "router-rs-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1wdf4a3wn9kwjiqzj3y3afnkr5wfa061hl15sbk16idjxvz385nf")))

(define-public crate-router-xiaobei-0.1.1 (c (n "router-xiaobei") (v "0.1.1") (d (list (d (n "dodrio") (r "^0.1.0") (d #t) (k 0)) (d (n "router-rs-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1fx4w6jdhhrprr2dhl1hpil37v92rx9ssj4313fak1vi6lgpbdzh")))

(define-public crate-router-xiaobei-0.1.2 (c (n "router-xiaobei") (v "0.1.2") (d (list (d (n "dodrio") (r "^0.1.0") (d #t) (k 0)) (d (n "router-xiaobei-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1gdmf42k5pygz3as42lbl9d632bcpx1jj35r2xklsddp60kxi1vf")))

