(define-module (crates-io ro ut route-error) #:use-module (crates-io))

(define-public crate-route-error-1.0.0 (c (n "route-error") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ssa8c8s36danx4h9j4nc99xdachp6ir58yr7pr03av7l98a7pwz") (y #t)))

(define-public crate-route-error-1.0.1 (c (n "route-error") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pq450bjwlnfk07n7rpv4d8f4qs0h12j38wyy56823wmgdsp9bbm") (y #t)))

