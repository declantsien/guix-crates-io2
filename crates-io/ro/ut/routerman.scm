(define-module (crates-io ro ut routerman) #:use-module (crates-io))

(define-public crate-routerman-0.0.0 (c (n "routerman") (v "0.0.0") (h "1pnprzzwr7bhxfrds8kv2zzm8chqlfhm6yk0d6rm1ixx7ayjg7az")))

(define-public crate-routerman-0.0.1 (c (n "routerman") (v "0.0.1") (d (list (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "hyper") (r "^0.14.19") (f (quote ("server" "tcp"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14.19") (f (quote ("full"))) (d #t) (k 2)) (d (n "matchit") (r "^0.6.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)))) (h "02qnsi32xbb4pyhk1y99ak29vj9348srrx8jlzi43n0zzf7jp34g") (f (quote (("json" "serde" "serde_json") ("default" "json"))))))

