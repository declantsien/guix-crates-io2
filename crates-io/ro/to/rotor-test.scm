(define-module (crates-io ro to rotor-test) #:use-module (crates-io))

(define-public crate-rotor-test-0.1.0 (c (n "rotor-test") (v "0.1.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "rotor") (r "^0.6.0") (d #t) (k 0)))) (h "1p5grmk34dd4cdrzl80wpsilsh0k3p35zriyjpas1ycv4s04bpy2")))

(define-public crate-rotor-test-0.2.0 (c (n "rotor-test") (v "0.2.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "rotor") (r "^0.6.0") (d #t) (k 0)) (d (n "rotor-stream") (r "^0.6.0") (d #t) (k 0)))) (h "1d90njl47wd71f0dfp4pz1dnq3q9950cidwfbm2x76fq8s4xf174")))

