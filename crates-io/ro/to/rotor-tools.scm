(define-module (crates-io ro to rotor-tools) #:use-module (crates-io))

(define-public crate-rotor-tools-0.1.0 (c (n "rotor-tools") (v "0.1.0") (d (list (d (n "rotor") (r "^0.5.0") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)) (d (n "void") (r "^0.0.5") (d #t) (k 0)))) (h "16k1lqzdy788d5awm9skjb9l310rhjgal4invjvvjd72g4dp7c18")))

(define-public crate-rotor-tools-0.2.0 (c (n "rotor-tools") (v "0.2.0") (d (list (d (n "rotor") (r "^0.5.0") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)) (d (n "void") (r "^0.0.5") (d #t) (k 0)))) (h "17i182ffdw5pkfslm47x85ijd2f60fjipcvdpbmx2pm189q2d67q")))

(define-public crate-rotor-tools-0.3.0 (c (n "rotor-tools") (v "0.3.0") (d (list (d (n "rotor") (r "^0.6.0") (d #t) (k 0)))) (h "0n6s7n2fhhkkj35nc416yn28a0arf0sjmwhcm48197d6jmyv1rxp")))

(define-public crate-rotor-tools-0.3.1 (c (n "rotor-tools") (v "0.3.1") (d (list (d (n "rotor") (r "^0.6.1") (d #t) (k 0)))) (h "0b4a6jgnxafk8jwg5j1pqaycx3rkmbvfvdm0na6gawdwrg0z9k7x")))

(define-public crate-rotor-tools-0.3.2 (c (n "rotor-tools") (v "0.3.2") (d (list (d (n "rotor") (r "^0.6.1") (d #t) (k 0)))) (h "00ia3fw7091dll6bf14r4xxxprbfwn85cm733zzma7wmgsgmw18d")))

