(define-module (crates-io ro to rotors) #:use-module (crates-io))

(define-public crate-rotors-0.1.0 (c (n "rotors") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "ciborium") (r "^0.2") (d #t) (k 0)) (d (n "rotors-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (k 0)))) (h "19fx9v2641jnyjj7f71a73k05pmn65amq7ni90fd19bxx4mr0vbs") (f (quote (("transport" "rotors-macros/transport") ("default" "transport"))))))

(define-public crate-rotors-0.2.0 (c (n "rotors") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "ciborium") (r "^0.2") (d #t) (k 0)) (d (n "rotors-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (k 0)))) (h "0cdf3kq3yfz3ns68kmy5hvk1f96jkgwyr62633nkyd7rqqxqaiy5") (f (quote (("transport" "rotors-macros/transport") ("default" "transport"))))))

