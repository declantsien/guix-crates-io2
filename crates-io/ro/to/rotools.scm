(define-module (crates-io ro to rotools) #:use-module (crates-io))

(define-public crate-rotools-0.1.0 (c (n "rotools") (v "0.1.0") (d (list (d (n "graphql_client") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.5") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0dxa97h47pdn9pm0qj38fb12lg0vx52a35qr7cj7n06l1vxlswlb")))

(define-public crate-rotools-0.2.0 (c (n "rotools") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "graphql_client") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0vpq9rnvfilsiph778k1f3vyvs1rzx2hrqfcnxiw2bhpf6rplzls")))

(define-public crate-rotools-0.2.1 (c (n "rotools") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "graphql_client") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "00ia1nq3gg4npp50hfmlb2y0n9hj41dm1m0r9ga8lailjcx7ss7k")))

(define-public crate-rotools-0.2.2 (c (n "rotools") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "graphql_client") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1wlikyg3imb6h3m43ziildswabxikmxpvg58zqgw300n8a80j4n8")))

(define-public crate-rotools-0.3.0 (c (n "rotools") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "graphql_client") (r "^0.11.0") (d #t) (k 0)) (d (n "persy") (r "^1.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0q0k8ywggd24h255w0jbn4yiny3aw66dfnr495iyzv4q0404bjap")))

