(define-module (crates-io ro to rotors-macros) #:use-module (crates-io))

(define-public crate-rotors-macros-0.1.0 (c (n "rotors-macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (k 0)))) (h "0znjigkcr2xi5mdx835i8li6xg58372i4sw1gi2mr09697drxrl9") (f (quote (("transport" "tonic-build/transport") ("default"))))))

(define-public crate-rotors-macros-0.2.0 (c (n "rotors-macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (k 0)))) (h "0hsf9zc90f15v043avmhrbjh6357x6jrc12s0g8ripr8bynyl5g5") (f (quote (("transport" "tonic-build/transport") ("default"))))))

