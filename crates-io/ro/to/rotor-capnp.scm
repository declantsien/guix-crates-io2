(define-module (crates-io ro to rotor-capnp) #:use-module (crates-io))

(define-public crate-rotor-capnp-0.1.0 (c (n "rotor-capnp") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "capnp") (r "^0.6.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rotor") (r "^0.6.3") (d #t) (k 0)) (d (n "rotor-stream") (r "^0.5.1") (d #t) (k 0)))) (h "1s1207nkrcba5synkn0v75n3ckq93fwjr9b8f7asa9nwrls4bqqx")))

