(define-module (crates-io ro to rotonda-macros) #:use-module (crates-io))

(define-public crate-rotonda-macros-0.1.1 (c (n "rotonda-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("proc-macro" "full" "parsing" "printing"))) (d #t) (k 0)))) (h "0i7j13biv2dprjbkfwrjqg6rna9mw6nzragj60wgnqsqnaww4nwi")))

(define-public crate-rotonda-macros-0.2.0-pre.1 (c (n "rotonda-macros") (v "0.2.0-pre.1") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("proc-macro" "full" "parsing" "printing"))) (d #t) (k 0)))) (h "0r2lvc92h2b30v28356p453cgf53gw2yk875vbwy0dg0704zipg4")))

(define-public crate-rotonda-macros-0.2.0-pre.2 (c (n "rotonda-macros") (v "0.2.0-pre.2") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("proc-macro" "full" "parsing" "printing"))) (d #t) (k 0)))) (h "0shp0phq46gjq7yl3h19wbp1l3djmrxam3p6qlks3l0d2fby21si") (y #t)))

(define-public crate-rotonda-macros-0.2.0-pre.3 (c (n "rotonda-macros") (v "0.2.0-pre.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("proc-macro" "full" "parsing" "printing"))) (d #t) (k 0)))) (h "08xdh5ldrv6246msqnjz8603zfnbja993nb2pwyznjfidkrrz5hm")))

(define-public crate-rotonda-macros-0.2.0-pre.4 (c (n "rotonda-macros") (v "0.2.0-pre.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("proc-macro" "full" "parsing" "printing"))) (d #t) (k 0)))) (h "11i0fnm0b40vsknxgagzh00gzlnkrsv3wck4wy6b9fncc5n18kjd")))

(define-public crate-rotonda-macros-0.2.0-pre.5 (c (n "rotonda-macros") (v "0.2.0-pre.5") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("proc-macro" "full" "parsing" "printing"))) (d #t) (k 0)))) (h "1wwvdm26sb7xlqn0lsgjcjfq77fpm0ianmn2lmnbmv47j40yyqh9")))

(define-public crate-rotonda-macros-0.2.0-pre.6 (c (n "rotonda-macros") (v "0.2.0-pre.6") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("proc-macro" "full" "parsing" "printing"))) (d #t) (k 0)))) (h "03jir3frpjrg7749ghprpn4dhhayq8jhszm5px1pavsr3n5nb7va")))

(define-public crate-rotonda-macros-0.3.0 (c (n "rotonda-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("proc-macro" "full" "parsing" "printing"))) (d #t) (k 0)))) (h "17i83yl7hyg63rpfwi4an0jvlla0kffh88ms8lqw21rjvm0bzcfk")))

(define-public crate-rotonda-macros-0.3.1 (c (n "rotonda-macros") (v "0.3.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("proc-macro" "full" "parsing" "printing"))) (d #t) (k 0)))) (h "1fdqr3l1dlvkhza5l8p4jcaddf4pnknwdzf2n4ip1jgvs5xh1blm")))

