(define-module (crates-io ro to rotor) #:use-module (crates-io))

(define-public crate-rotor-0.1.0 (c (n "rotor") (v "0.1.0") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "memchr") (r "*") (d #t) (k 0)) (d (n "mio") (r ">= 0.4.2") (d #t) (k 0)) (d (n "netbuf") (r "^0.2") (d #t) (k 0)))) (h "02vxb49fya59vq8wgkn0wg2c08pas9yfn5ikyfjmgn749z1np2p6")))

(define-public crate-rotor-0.2.0 (c (n "rotor") (v "0.2.0") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "memchr") (r "*") (d #t) (k 0)) (d (n "mio") (r ">= 0.4.2") (d #t) (k 0)) (d (n "netbuf") (r "^0.2") (d #t) (k 0)))) (h "0rbrdww6hw0vx45ajrm7bcvffcd02f5c8rdvpfncacnbaij43253")))

(define-public crate-rotor-0.3.0 (c (n "rotor") (v "0.3.0") (d (list (d (n "log") (r "^0.3.2") (d #t) (k 0)) (d (n "memchr") (r "^0.1.6") (d #t) (k 0)) (d (n "mio") (r "^0.4.3") (d #t) (k 0)) (d (n "netbuf") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.23") (d #t) (k 0)))) (h "0f8j286vy1j05ivqjjrq01nr4spr5432pc3l7yls4bjqafkygagz")))

(define-public crate-rotor-0.4.0 (c (n "rotor") (v "0.4.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 2)) (d (n "void") (r "^0.0.5") (d #t) (k 2)))) (h "0vykgyqifx7dx8p9gmv4da9wdnbwkpi62vcjznqllfszi3l12r00")))

(define-public crate-rotor-0.5.0 (c (n "rotor") (v "0.5.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 2)) (d (n "quick-error") (r "^0.2.1") (d #t) (k 0)) (d (n "void") (r "^0.0.5") (d #t) (k 2)))) (h "0zlvjyb0n7sww7xyyhkisw309r5ij727zwi6bpcq1s3fyxzq9agw")))

(define-public crate-rotor-0.5.1 (c (n "rotor") (v "0.5.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 2)) (d (n "quick-error") (r "^0.2.1") (d #t) (k 0)) (d (n "void") (r "^0.0.5") (d #t) (k 2)))) (h "0lrpd549idry3mh99ln2sv5v5157i3l9lkz09mjx8p7df7q4zw5x")))

(define-public crate-rotor-0.6.0 (c (n "rotor") (v "0.6.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 2)) (d (n "quick-error") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)) (d (n "void") (r "^1.0.0") (d #t) (k 0)) (d (n "void") (r "^1.0.0") (d #t) (k 2)))) (h "0aj6384s6imj6fb7cl204l89q6avv02703a5cfivv0329ykqa867") (f (quote (("log_errors"))))))

(define-public crate-rotor-0.6.1 (c (n "rotor") (v "0.6.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 2)) (d (n "quick-error") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)) (d (n "void") (r "^1.0.0") (d #t) (k 0)) (d (n "void") (r "^1.0.0") (d #t) (k 2)))) (h "0ky5s3d2mxmbqg3c22vrfbfnzblfqw0wxayrdcsi2bd5j9q1j03x") (f (quote (("log_errors"))))))

(define-public crate-rotor-0.6.2 (c (n "rotor") (v "0.6.2") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 2)) (d (n "quick-error") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)) (d (n "void") (r "^1.0.0") (d #t) (k 0)) (d (n "void") (r "^1.0.0") (d #t) (k 2)))) (h "098sxhbrd7kkz71crphlm2y715hric7q3q9zbr3n6qyv2aphrq59") (f (quote (("log_errors"))))))

(define-public crate-rotor-0.6.3 (c (n "rotor") (v "0.6.3") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 2)) (d (n "quick-error") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)) (d (n "void") (r "^1.0.0") (d #t) (k 0)) (d (n "void") (r "^1.0.0") (d #t) (k 2)))) (h "052chdrw4f7ap7arzn9pgz8d22z6gxz6a3r7swipcp4vcsndd9h7") (f (quote (("log_errors"))))))

