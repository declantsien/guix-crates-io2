(define-module (crates-io ro da rodash) #:use-module (crates-io))

(define-public crate-rodash-0.1.0 (c (n "rodash") (v "0.1.0") (d (list (d (n "num") (r "^0.4.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "095p1lyxdj4rxinmb7yf5mg8212arqasxr63kymx3dzsvmrhyigz")))

(define-public crate-rodash-0.1.1 (c (n "rodash") (v "0.1.1") (d (list (d (n "num") (r "^0.4.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1if70m41hq5in8kavbvnp5r67hkvc531xw3a4fpiiwjkgnvc0ynq")))

