(define-module (crates-io ro ul roulette) #:use-module (crates-io))

(define-public crate-roulette-0.1.0 (c (n "roulette") (v "0.1.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "1w49f2qyc3na3p8mzg5v50iap796d55wlp9jbw8nbwdkv78cddsx")))

(define-public crate-roulette-0.2.0 (c (n "roulette") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "000f0apzyq59gjmrp8jq35f5nskmlga1rajzgl7hm8m3iadgrd26")))

(define-public crate-roulette-0.3.0 (c (n "roulette") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0s381hl0s7rhp3yhd1lx7apjcrb2h5pj2mfp8sa07k930q4swpxg")))

