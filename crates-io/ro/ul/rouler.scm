(define-module (crates-io ro ul rouler) #:use-module (crates-io))

(define-public crate-rouler-0.1.0 (c (n "rouler") (v "0.1.0") (d (list (d (n "pest") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0w4ndjslnrwvab45bsnaa0sz4h3miwnl2qaqskq6bdqplnm6qrph")))

(define-public crate-rouler-0.1.1 (c (n "rouler") (v "0.1.1") (d (list (d (n "pest") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1yq07hjl1hz02kjvn1c3klig4vxjmzsncncdvyz52vyhfgq73s3j")))

(define-public crate-rouler-0.1.2 (c (n "rouler") (v "0.1.2") (d (list (d (n "pest") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0b6z2c2k6j3cbx3pkd14srsxdjjcq6vmbrw0yxhgxsxcrmzg41ii")))

(define-public crate-rouler-0.1.3 (c (n "rouler") (v "0.1.3") (d (list (d (n "pest") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1j6pkynhg26ggw818rqzd431aal7i6jmf3ipcj8g4xkkmmxplhni")))

(define-public crate-rouler-0.2.0 (c (n "rouler") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "030ipp38wpbb0hykn0pv3l8n3dd3ffkn8cskg2zzgkl6lqbf0r8v")))

(define-public crate-rouler-0.2.1 (c (n "rouler") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1lsd98p8ssrny9ihgzklw32fnxg6134bw9wq0ijbdn6k8d2zgi65")))

(define-public crate-rouler-0.2.2 (c (n "rouler") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "00rrbi4c1p60x3hbwj89x3vakbckm6318n99w1ynczbjcp9m3y9h")))

