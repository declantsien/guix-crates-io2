(define-module (crates-io ro ul roulette-wheel) #:use-module (crates-io))

(define-public crate-roulette-wheel-0.1.0 (c (n "roulette-wheel") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0gndw4cdcfyjaawr3fmdaggm1zkwh90i2ml31wpfx8knxc9kkr6j")))

(define-public crate-roulette-wheel-0.2.0 (c (n "roulette-wheel") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0nrjcp85xlrai4p8c4247rz4y4ihy3xa3ld7fjn8kxsqi37jm6s4")))

(define-public crate-roulette-wheel-0.2.1 (c (n "roulette-wheel") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "036v0hwm06zika68wl3zr3434fszwgjnbpbmknfsbg4k34cbjxn1")))

(define-public crate-roulette-wheel-0.2.2 (c (n "roulette-wheel") (v "0.2.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0m9y45x0ssx9afwc1x02ckch06d6iqg45skxld59yqg1v1q1hhb7")))

