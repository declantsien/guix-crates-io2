(define-module (crates-io ol in olin) #:use-module (crates-io))

(define-public crate-olin-0.1.0 (c (n "olin") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1qmn2gl21v12wl9fw3gybljw6kii6p9wm6qiaah4m2693pv2hawd")))

(define-public crate-olin-0.3.0 (c (n "olin") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "httparse") (r "^1.3.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.3") (d #t) (k 0)))) (h "1yvidc6aqavq5z05p8d1d5899bip64hy96apr1n8bakjwjwrsgrl")))

