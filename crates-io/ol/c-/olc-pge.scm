(define-module (crates-io ol c- olc-pge) #:use-module (crates-io))

(define-public crate-olc-pge-0.1.0 (c (n "olc-pge") (v "0.1.0") (d (list (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1rd5zvszj0v7s2722l09ilhn8p879yizqkqy8jvsv3lrxnqy69y0") (y #t)))

(define-public crate-olc-pge-0.1.1 (c (n "olc-pge") (v "0.1.1") (d (list (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1i23xn23gxgp9lk33y0rhcad543gjjh5yv8hgaajsgyd20rpp7ji") (y #t)))

(define-public crate-olc-pge-0.1.2 (c (n "olc-pge") (v "0.1.2") (d (list (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0k6qy63hr7sm47p9x6apxngjl1mda7rbv5zyig6n903ihg718kzj")))

