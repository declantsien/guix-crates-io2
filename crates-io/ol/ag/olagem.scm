(define-module (crates-io ol ag olagem) #:use-module (crates-io))

(define-public crate-olagem-0.1.0 (c (n "olagem") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "1nz58j83aviwdymbryy37hymnmqg735xngc5n2bqngda17xajxwm")))

(define-public crate-olagem-0.1.1 (c (n "olagem") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "0znjl1hy4pgbh1kk8b2k1v9lcb8fik1pbhry955z2l1dvrh19c9p")))

