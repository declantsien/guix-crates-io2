(define-module (crates-io ol ym olympia_engine) #:use-module (crates-io))

(define-public crate-olympia_engine-0.1.0 (c (n "olympia_engine") (v "0.1.0") (h "0gbwkk5f1gx1lqfpam5n5ik4xmafbdxb3b75j8sngmlknj29bjq8") (f (quote (("std") ("disassembler") ("default" "disassembler"))))))

(define-public crate-olympia_engine-0.1.1 (c (n "olympia_engine") (v "0.1.1") (h "0jspmv9sazjn1ww9fhqpx9qy4ww5imidrnfqq8avzh6bvg8z313q") (f (quote (("std") ("disassembler") ("default" "disassembler"))))))

(define-public crate-olympia_engine-0.1.2 (c (n "olympia_engine") (v "0.1.2") (h "1r9i6l89a86dmhpayzm6ag8g5ffgglnq45r6fd5hkvbya33q0lv9") (f (quote (("std") ("disassembler") ("default" "disassembler"))))))

(define-public crate-olympia_engine-0.2.0 (c (n "olympia_engine") (v "0.2.0") (d (list (d (n "olympia_core") (r "^0.1.0") (d #t) (k 0)))) (h "06774zrxjiw924xjql4sd1cfdx6iwc74f9hya3skxbay3y0ba0r4") (f (quote (("std") ("disassembler") ("default" "disassembler"))))))

(define-public crate-olympia_engine-0.3.0 (c (n "olympia_engine") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7.2") (d #t) (k 0)) (d (n "olympia_core") (r "^0.3.0") (d #t) (k 0)) (d (n "olympia_derive") (r "^0.3.0") (d #t) (k 0)))) (h "1dig5ljjdnpqpm3iwa635zczz134y3g2gyghz49nz6irq4lbdjcb") (f (quote (("std") ("disassembler") ("default" "disassembler"))))))

