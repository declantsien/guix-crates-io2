(define-module (crates-io ol ym olympia_derive) #:use-module (crates-io))

(define-public crate-olympia_derive-0.3.0 (c (n "olympia_derive") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "olympia_core") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "runtime-macros-derive") (r "^0.4.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.11") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0wdpz16mak0dhn8l08c3cjpspwpj9m2gj7k1804w6wyz5gwdxij5")))

