(define-module (crates-io ol ym olympus-pro) #:use-module (crates-io))

(define-public crate-olympus-pro-1.0.0 (c (n "olympus-pro") (v "1.0.0") (d (list (d (n "cosmwasm-std") (r "^0.16.2") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.16.2") (d #t) (k 0)) (d (n "cw20") (r "^0.8.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.25.2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "terra-cosmwasm") (r "^2.2.0") (d #t) (k 0)) (d (n "terraswap") (r "^2.4.0") (d #t) (k 0)))) (h "0gmng0a3i2ibrg7ba8dhyn34d6cylq11brf275viylpf393yw1yp") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

