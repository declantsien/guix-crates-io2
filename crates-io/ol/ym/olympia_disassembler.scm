(define-module (crates-io ol ym olympia_disassembler) #:use-module (crates-io))

(define-public crate-olympia_disassembler-0.1.0 (c (n "olympia_disassembler") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "olympia_engine") (r "^0.1.0") (f (quote ("disassembler"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0vgawafpjww267440jj4s9lhxdkq6qq08mlyvsfh6v53bjxq8zpb")))

(define-public crate-olympia_disassembler-0.1.1 (c (n "olympia_disassembler") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "olympia_engine") (r "^0.1.0") (f (quote ("disassembler"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1cv4igpaar4hq6hr6ly04xvkwiki780wyiq8ydhyh2qrnvggnqhd")))

