(define-module (crates-io ol ym olympia_cli) #:use-module (crates-io))

(define-public crate-olympia_cli-0.1.0 (c (n "olympia_cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "olympia_engine") (r "^0.1.0") (f (quote ("disassembler" "std"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "18l38j4raqwf09vvf0b8fq95rl4bx245xkd9kjr3infkvsyb4h5n")))

(define-public crate-olympia_cli-0.1.1 (c (n "olympia_cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "olympia_engine") (r "^0.1.1") (f (quote ("disassembler" "std"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1yw0lia6a2v4zlxzbvm7xa4gr2r83h4v7klzi33nsys9ack78nk1")))

(define-public crate-olympia_cli-0.2.0 (c (n "olympia_cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "olympia_engine") (r "^0.1.2") (f (quote ("disassembler" "std"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "00868n9jwn6jknzyff1l31cskm0as178rmlgb3ijaf6n22f5ijay")))

(define-public crate-olympia_cli-0.2.1 (c (n "olympia_cli") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "olympia_engine") (r "^0.2.0") (f (quote ("disassembler" "std"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "109aycxijwxy9j0c0inpc0c43v9y6lyhs3gk8ddyr2sz5n80ajpr")))

(define-public crate-olympia_cli-0.3.0 (c (n "olympia_cli") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "olympia_engine") (r "^0.3.0") (f (quote ("disassembler" "std"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0vgn5ypnirpyvryd6f08969rs0j88pd1pm78jja6znc79199jmd2")))

