(define-module (crates-io ol ym olympia_native) #:use-module (crates-io))

(define-public crate-olympia_native-0.1.0 (c (n "olympia_native") (v "0.1.0") (d (list (d (n "olympia_engine") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)))) (h "1gsgsan8xgwp7kx944k9maxyjrj00dm5m1ifvr0ml60vpwq5cn86")))

(define-public crate-olympia_native-0.2.0 (c (n "olympia_native") (v "0.2.0") (d (list (d (n "cairo-rs") (r "^0.8.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "gdk") (r "^0.12.0") (f (quote ("v3_22"))) (d #t) (k 0)) (d (n "gio") (r "^0.8.0") (f (quote ("v2_56"))) (d #t) (k 0)) (d (n "glib") (r "^0.9.0") (f (quote ("v2_56"))) (d #t) (k 0)) (d (n "gtk") (r "^0.8.0") (f (quote ("v3_22"))) (d #t) (k 0)) (d (n "olympia_engine") (r "^0.3.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "pango") (r "^0.8.0") (f (quote ("v1_40"))) (d #t) (k 0)))) (h "1b9fl5vqcl1p83farp6ycrfkya5qr5awrr3dbbvskn5v1izzg7cx")))

