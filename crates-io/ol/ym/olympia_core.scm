(define-module (crates-io ol ym olympia_core) #:use-module (crates-io))

(define-public crate-olympia_core-0.1.0 (c (n "olympia_core") (v "0.1.0") (h "1w116jf7i9ypv0js4d6kn82rsila1rgzlx38di2k8hqha470mkil")))

(define-public crate-olympia_core-0.3.0 (c (n "olympia_core") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)))) (h "1v9jhc596fhnpy767ybzv0jy7gbdp09qnhrk3wqd99ycfz2x05km")))

