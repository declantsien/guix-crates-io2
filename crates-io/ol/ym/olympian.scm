(define-module (crates-io ol ym olympian) #:use-module (crates-io))

(define-public crate-olympian-0.1.0 (c (n "olympian") (v "0.1.0") (d (list (d (n "dyn-stack") (r "^0.9") (d #t) (k 0)) (d (n "faer-core") (r "^0.9") (d #t) (k 0)) (d (n "faer-lu") (r "^0.9") (d #t) (k 0)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)) (d (n "rstar") (r "^0.9.3") (d #t) (k 0)))) (h "13bq0habbzwvww5c808nv3j6msd8qyfwpjmhhpsz9a0hhixprr9q")))

(define-public crate-olympian-0.2.0 (c (n "olympian") (v "0.2.0") (d (list (d (n "dyn-stack") (r "^0.9") (d #t) (k 0)) (d (n "faer-core") (r "^0.9") (d #t) (k 0)) (d (n "faer-lu") (r "^0.9") (d #t) (k 0)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)) (d (n "rstar") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0ray60v9mprkzcj0mizd7yq6jzrswc1n1drw1ldjz12gba8c6hhj") (y #t)))

(define-public crate-olympian-0.2.1 (c (n "olympian") (v "0.2.1") (d (list (d (n "dyn-stack") (r "^0.9") (d #t) (k 0)) (d (n "faer-core") (r "^0.9") (d #t) (k 0)) (d (n "faer-lu") (r "^0.9") (d #t) (k 0)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)) (d (n "rstar") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "19mlvjjwljavm87b95gj287m4wz1kmxl8qc4qkmv4wy1yijiis79")))

(define-public crate-olympian-0.3.0 (c (n "olympian") (v "0.3.0") (d (list (d (n "dyn-stack") (r "^0.9") (d #t) (k 0)) (d (n "faer-core") (r "^0.9") (d #t) (k 0)) (d (n "faer-lu") (r "^0.9") (d #t) (k 0)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)) (d (n "rstar") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1b4hryxr2ais1zr4nfcg4sjasavz3hkay2pkm94ls1n063hv2am8")))

(define-public crate-olympian-0.3.1 (c (n "olympian") (v "0.3.1") (d (list (d (n "dyn-stack") (r "^0.9") (d #t) (k 0)) (d (n "faer-core") (r "^0.9") (d #t) (k 0)) (d (n "faer-lu") (r "^0.9") (d #t) (k 0)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)) (d (n "rstar") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "07krcwq08wqv4dc0afcz7cqn5kzsrsrvqq7b2fdl6f1f2sjmqav3")))

(define-public crate-olympian-0.3.2 (c (n "olympian") (v "0.3.2") (d (list (d (n "dyn-stack") (r "^0.9") (d #t) (k 0)) (d (n "faer-core") (r "^0.9") (d #t) (k 0)) (d (n "faer-lu") (r "^0.9") (d #t) (k 0)) (d (n "reborrow") (r "^0.5") (d #t) (k 0)) (d (n "rstar") (r "^0.9.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "10i27y92msi9c5a12rj6ahdwlh62csqs137iliyjvpkjpsbpkcgn")))

