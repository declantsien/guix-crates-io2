(define-module (crates-io ol ym olympia) #:use-module (crates-io))

(define-public crate-olympia-0.1.0 (c (n "olympia") (v "0.1.0") (h "0wz09zdz14fa3fk2d7zb0b00cgxi2zry9gk6blg7qal4v6ycca5p")))

(define-public crate-olympia-0.1.1 (c (n "olympia") (v "0.1.1") (h "0sdfvpkvqz1cc3m31vsabfhvakqj1118x57j1q946qfgkd74w8cn")))

