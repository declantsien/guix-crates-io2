(define-module (crates-io ol d- old-http) #:use-module (crates-io))

(define-public crate-old-http-0.1.0-pre (c (n "old-http") (v "0.1.0-pre") (d (list (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0048k2fm36r2k8a0rgigjvwz5pdppgi0m05p3gpq2qlngwqm7klz")))

(define-public crate-old-http-0.1.1-pre (c (n "old-http") (v "0.1.1-pre") (d (list (d (n "openssl") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)))) (h "1gcq8a0cmv2qaf69q04d4f7jmkj9qp92p18k76s90wmaym8pvy9f") (f (quote (("ssl" "openssl") ("default" "ssl"))))))

