(define-module (crates-io ol im olimex-stm32-p107) #:use-module (crates-io))

(define-public crate-olimex-stm32-p107-0.1.0 (c (n "olimex-stm32-p107") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.4") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 2)) (d (n "panic-rtt-target") (r "^0.1.1") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "rtt-target") (r "^0.1.1") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "stm32f1xx-hal") (r "^0.7.0") (f (quote ("rt" "stm32f107"))) (d #t) (k 0)))) (h "1c2spz70aal2d0n258l6gfsx8bqadpdl7bxfxv8bczqjahdnk2wr")))

