(define-module (crates-io ol if oliframe) #:use-module (crates-io))

(define-public crate-oliframe-0.1.0 (c (n "oliframe") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csscolorparser") (r "^0.6.2") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "junk_file") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1my1vj7qw5y8g9s9qapvy02x1p6ddpdd1b77ypzv88s536r11xb0") (r "1.75.0")))

(define-public crate-oliframe-0.1.1 (c (n "oliframe") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csscolorparser") (r "^0.6.2") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "junk_file") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "087r3gsfr3sja9wfzhqw7ggrr5wjrgyk3ys8r1xmk3npifrv9hbn") (r "1.75.0")))

(define-public crate-oliframe-0.1.2 (c (n "oliframe") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csscolorparser") (r "^0.6.2") (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "junk_file") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "0qlwzid4y68lajjn1hdl4jwjxycmj1afvnrd804c8769m4sdjazc") (r "1.75.0")))

(define-public crate-oliframe-0.1.3 (c (n "oliframe") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csscolorparser") (r "^0.6.2") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "junk_file") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "02fc79qzjbycw3nha1nd76c83a4779wl53cjdffppssm3yk0dl6b") (r "1.75.0")))

(define-public crate-oliframe-0.1.4 (c (n "oliframe") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csscolorparser") (r "^0.6.2") (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "junk_file") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "1lf4f6svm2w3jl7dksxc5fki3dxfkr3zwxm2iaw8rhf3fcwz4bac") (r "1.75.0")))

(define-public crate-oliframe-0.1.5 (c (n "oliframe") (v "0.1.5") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csscolorparser") (r "^0.6.2") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "junk_file") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "1a83a4xkj5i4azsglwrx4x8l5l9i9yjasd65j828bpxs0qddz989") (r "1.75.0")))

