(define-module (crates-io ol d_ old_icelandic_zoega) #:use-module (crates-io))

(define-public crate-old_icelandic_zoega-1.0.0 (c (n "old_icelandic_zoega") (v "1.0.0") (d (list (d (n "insta") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wjh3nwgdvc89vb40x82id2hqvg7agnw101xrbc0r0fjb7yypax2")))

(define-public crate-old_icelandic_zoega-1.1.0 (c (n "old_icelandic_zoega") (v "1.1.0") (d (list (d (n "insta") (r "^1.21.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wkdcfmxlv1dpr9ba6shh6c63zv2vzfq7hsvf34f6w7hq8133hd7")))

