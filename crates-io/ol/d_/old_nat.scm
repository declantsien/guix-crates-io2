(define-module (crates-io ol d_ old_nat) #:use-module (crates-io))

(define-public crate-old_nat-0.1.0 (c (n "old_nat") (v "0.1.0") (d (list (d (n "ansi_term") (r ">=0.12.1, <0.13.0") (d #t) (k 0)) (d (n "chrono") (r ">=0.4.19, <0.5.0") (d #t) (k 0)) (d (n "filetime") (r ">=0.2.13, <0.3.0") (d #t) (k 0)) (d (n "humansize") (r ">=1.1.0, <2.0.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "pretty-bytes") (r ">=0.2.2, <0.3.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "termion") (r ">=1.5.5, <2.0.0") (d #t) (k 0)) (d (n "users") (r ">=0.11.0, <0.12.0") (d #t) (k 0)))) (h "1b90d2wgm4m96li7hwx6x7wx4phvznjmazh79iip142bglxihsjs")))

