(define-module (crates-io ol d_ old_norwegian_dictionary) #:use-module (crates-io))

(define-public crate-old_norwegian_dictionary-1.0.0 (c (n "old_norwegian_dictionary") (v "1.0.0") (d (list (d (n "insta") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02nr0dnpqiyplfwv3a06b6097dl63z7w956ck1a8w3hv48qflxvk")))

(define-public crate-old_norwegian_dictionary-2.0.0 (c (n "old_norwegian_dictionary") (v "2.0.0") (d (list (d (n "insta") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qz7b57qz8jsxyz7kzsx4bhp2s7y7kk5i7xyvmkjrpbrh9pyr1g0")))

