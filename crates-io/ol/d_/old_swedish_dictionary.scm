(define-module (crates-io ol d_ old_swedish_dictionary) #:use-module (crates-io))

(define-public crate-old_swedish_dictionary-0.1.0 (c (n "old_swedish_dictionary") (v "0.1.0") (d (list (d (n "insta") (r "^1.21.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z9n020vcxfldcx87jvaimxfaknhlmjnjms2b30yrq3v4wgl43lh")))

(define-public crate-old_swedish_dictionary-0.2.0 (c (n "old_swedish_dictionary") (v "0.2.0") (d (list (d (n "insta") (r "^1.21.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cgls03hkkzmc0haayx8zy543795hsrrm9gaxcxyl06dkc2rzkj5")))

(define-public crate-old_swedish_dictionary-1.0.0 (c (n "old_swedish_dictionary") (v "1.0.0") (d (list (d (n "insta") (r "^1.21.0") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rf3rm966sf5a4q9pga297dcc67xipliidzk2qhaxd8si4vc6qgq")))

