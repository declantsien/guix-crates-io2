(define-module (crates-io ol ed oled) #:use-module (crates-io))

(define-public crate-oled-0.0.0 (c (n "oled") (v "0.0.0") (d (list (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "display-interface") (r "^0.4.1") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal-02") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "^1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")))) (h "1sf026fwsg11b2282pwmas0fd1vy5vazmshjx4snvczigmksd04d")))

