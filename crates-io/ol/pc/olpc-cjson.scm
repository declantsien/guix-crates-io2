(define-module (crates-io ol pc olpc-cjson) #:use-module (crates-io))

(define-public crate-olpc-cjson-0.1.0 (c (n "olpc-cjson") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.29") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1hfrvygsaf6hj09z2j5znm0pb4wwmf4m3369hy9z3j366d4y42cl")))

(define-public crate-olpc-cjson-0.1.1 (c (n "olpc-cjson") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1753jspq3h3k2xqw9jcxxfk6bfyp9slpsm7f4kqvn52hd3z4kjkj")))

(define-public crate-olpc-cjson-0.1.2 (c (n "olpc-cjson") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0g61m0ck54zg81rydpbqwfql67jxbp6bq6hwcx9xi310fb7pbp47")))

(define-public crate-olpc-cjson-0.1.3 (c (n "olpc-cjson") (v "0.1.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0yh2n9ppm4mlqgpqm5hy5ynm21ihd2llz3ysjzswz733bg0wjdyn")))

