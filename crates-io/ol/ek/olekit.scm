(define-module (crates-io ol ek olekit) #:use-module (crates-io))

(define-public crate-olekit-1.0.0 (c (n "olekit") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "ole") (r "^0.1.13") (d #t) (k 0)))) (h "0a0n4g4xzc2d012dpksb53mv4f3zs15brpgn5855cxkcpg5c1jhq")))

(define-public crate-olekit-1.0.1 (c (n "olekit") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "ole") (r "^0.1.13") (d #t) (k 0)))) (h "1llags763cpa09zcnak8187q24y3gdag0n6m9n8iqc3d6fq9kvkv")))

(define-public crate-olekit-1.0.2 (c (n "olekit") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "ole") (r "^0.1.13") (d #t) (k 0)))) (h "0xla2vawb4396qh9figb2cxskbmhfpknrzincl2pzkb8wcdz23h9")))

