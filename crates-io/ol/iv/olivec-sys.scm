(define-module (crates-io ol iv olivec-sys) #:use-module (crates-io))

(define-public crate-olivec-sys-0.1.0 (c (n "olivec-sys") (v "0.1.0") (d (list (d (n "bindgen") (r ">=0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "stb-sys") (r "^0.6.0") (f (quote ("stb_image_write"))) (d #t) (k 2)))) (h "1gnqz0hqv7rwxiwjbqw70av8s7hhnii631b20cihhk42xmrj4i46") (l "olivec")))

(define-public crate-olivec-sys-0.1.1 (c (n "olivec-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "stb-sys") (r "^0.6.0") (f (quote ("stb_image_write"))) (d #t) (k 2)))) (h "1wgw8h7l4zssf6yr6286cn6608lnczjkymfc4zi9hr048jqkx0b1") (l "olivec")))

