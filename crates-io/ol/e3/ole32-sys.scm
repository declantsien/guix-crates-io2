(define-module (crates-io ol e3 ole32-sys) #:use-module (crates-io))

(define-public crate-ole32-sys-0.0.1 (c (n "ole32-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1z9ydzn8ggymvify7svmzzxky3k6p42csvzkcjvra6akdj48g3cy")))

(define-public crate-ole32-sys-0.0.2 (c (n "ole32-sys") (v "0.0.2") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "18wn7jxj3z4pd1qapfk9kfz9v90h3p0hnsdkxsgwh5ak223p8h8h")))

(define-public crate-ole32-sys-0.1.0 (c (n "ole32-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1w7cn6vjpj7izjf31nh87dch1bh7f6a1zb74fyx17mvzp258nh59")))

(define-public crate-ole32-sys-0.2.0 (c (n "ole32-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.4") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "134xg38xicrqynx4pfjfxnpp8x83m3gqw5j3s8y27rc22w14jb2x")))

