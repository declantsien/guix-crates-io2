(define-module (crates-io ol m- olm-sys) #:use-module (crates-io))

(define-public crate-olm-sys-0.1.0 (c (n "olm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26") (d #t) (k 1)))) (h "0z5lbll4jxa6pfjrc5gkz8jjx8g45xrrcivdg3hp8ap3rb1nv1jv")))

(define-public crate-olm-sys-0.1.1 (c (n "olm-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.26") (d #t) (k 1)))) (h "03xrgd5wvnv0k2xzvi45pm4n36knbgklj8vypyxdvxhmanm5ppgh")))

(define-public crate-olm-sys-0.1.2 (c (n "olm-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.26") (d #t) (k 1)))) (h "0nsgnkqvm8x27zqrb4kny616kq6rxkqylqx86mjmxjllaqzfgl02")))

(define-public crate-olm-sys-0.1.3 (c (n "olm-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)))) (h "1dlcvs462c0gal2hq4wrh9pqam88rrxqpl8icic6lwyn4r3dffsx")))

(define-public crate-olm-sys-0.1.4 (c (n "olm-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)))) (h "0ib1vpzaz4k4pm0mc8494224kvnh9hc9w7zkxpp15g7v8rvg08rr")))

(define-public crate-olm-sys-0.1.5 (c (n "olm-sys") (v "0.1.5") (h "1z78mk3df25vx12ld7hf8k2r4fsbmd7wba8pzyrgb6z8vbzxwxvx")))

(define-public crate-olm-sys-0.2.0 (c (n "olm-sys") (v "0.2.0") (h "1cav0v5bysx255lvxy32y5bc43p3mmybzw4i18y1za1cbv519niq")))

(define-public crate-olm-sys-1.0.0 (c (n "olm-sys") (v "1.0.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0krm2gc0ygdf6d0vnxbkczay7zr0vkaipqy0v84xp1kbdak87w2v")))

(define-public crate-olm-sys-1.0.1 (c (n "olm-sys") (v "1.0.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "086lwc2vpmn8xniv1mavxaljh4p7yins5qjx08qr5r3wgpvp9yl7")))

(define-public crate-olm-sys-1.0.2 (c (n "olm-sys") (v "1.0.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "02507vlx573xqvifmwxykscry28ri7wspn1r1vnkjvwbxsf968q8")))

(define-public crate-olm-sys-1.1.0 (c (n "olm-sys") (v "1.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1165hj6dw69wqvyrsixz6jgpbp8zvq0b5mzh6w6ib4ap5418k30h")))

(define-public crate-olm-sys-1.1.1 (c (n "olm-sys") (v "1.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0k95alwc9j7lvdclxispi8rll09dfbsnx4sxfa7905i9ppgb6acx")))

(define-public crate-olm-sys-1.1.2 (c (n "olm-sys") (v "1.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0sv9ymbavq41zhw6v5ag2w2233j3db8jq201jq0dkd8a7inxhzh6")))

(define-public crate-olm-sys-1.1.3 (c (n "olm-sys") (v "1.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0kvyn50f7wjcvvvzllrr053hccjfza85w793pk6z3m4cvlqbbwbd")))

(define-public crate-olm-sys-1.2.0 (c (n "olm-sys") (v "1.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "08gnlk1rsbnhml4mmpas1qjaw2jqmph7hsr47pa6j3c4w4lajx4h")))

(define-public crate-olm-sys-1.3.0 (c (n "olm-sys") (v "1.3.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1fs59xld36cyg2m71zx3cnriiy7j4rics66hzwv53hhwivnvfb52")))

(define-public crate-olm-sys-1.3.1 (c (n "olm-sys") (v "1.3.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "1bxjkx8c3spy5hqxamym5rkgmiczwcln271m1wmxygmmblbgmjr3") (l "olm")))

(define-public crate-olm-sys-1.3.2 (c (n "olm-sys") (v "1.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0ffk09h237vk962d0iclp13x2bqha9qpy58gkwgh52a9c8jwzzia") (l "olm")))

