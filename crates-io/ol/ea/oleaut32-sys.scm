(define-module (crates-io ol ea oleaut32-sys) #:use-module (crates-io))

(define-public crate-oleaut32-sys-0.0.1 (c (n "oleaut32-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0yyhv61py3xpgc9m2va1sl4kr5fld154x04w0vps1hinbz8vs3wq")))

(define-public crate-oleaut32-sys-0.2.0 (c (n "oleaut32-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "0bv1vzlzjp8wcvjibgglccibhmn3ki1ca0jvkbyllf171frigphz")))

