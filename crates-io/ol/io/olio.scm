(define-module (crates-io ol io olio) #:use-module (crates-io))

(define-public crate-olio-0.1.0 (c (n "olio") (v "0.1.0") (d (list (d (n "bytes") (r ">= 0.4.6, < 2") (d #t) (k 2)) (d (n "memmap") (r ">= 0.6.2, < 2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r ">= 2.2.0, < 4") (d #t) (k 2)))) (h "1jr151dwmlqswlx18vzl8l9h1qqzdddc84d0fvviq475d6m6syq5") (f (quote (("default" "memmap"))))))

(define-public crate-olio-0.2.0 (c (n "olio") (v "0.2.0") (d (list (d (n "bytes") (r ">= 0.4.6, < 2") (d #t) (k 2)) (d (n "memmap") (r ">= 0.6.2, < 2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r ">= 2.2.0, < 4") (d #t) (k 2)))) (h "1hxp8mmakg606zxr4aqpbcpiwv0cr5p4m2byx0y3f7241yi05y9b") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-olio-0.3.0 (c (n "olio") (v "0.3.0") (d (list (d (n "bytes") (r ">= 0.4.6, < 2") (d #t) (k 2)) (d (n "memmap") (r ">= 0.6.2, < 2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r ">= 2.2.0, < 4") (d #t) (k 2)))) (h "1541nxqjla7y0fw64mhwdpx3nzv8dl48nk07fn26mabkcc901km2") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-olio-0.4.0 (c (n "olio") (v "0.4.0") (d (list (d (n "bytes") (r ">= 0.4.6, < 2") (d #t) (k 2)) (d (n "libc") (r ">= 0.2.42, < 2") (d #t) (k 0)) (d (n "memmap") (r ">= 0.6.2, < 2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r ">= 2.2.0, < 4") (d #t) (k 2)))) (h "1gi7nb0piiz93fg503xhl9y1v8vbfyh17ibfzgmn0mp2r17rq4w7") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-olio-0.5.0 (c (n "olio") (v "0.5.0") (d (list (d (n "bytes") (r ">= 0.4.6, < 2") (d #t) (k 2)) (d (n "libc") (r ">= 0.2.42, < 2") (d #t) (k 0)) (d (n "memmap") (r ">= 0.7.0, < 2") (o #t) (d #t) (k 0)) (d (n "rand") (r ">= 0.5.5, < 2") (d #t) (k 2)) (d (n "tempfile") (r ">= 2.2.0, < 4") (d #t) (k 2)))) (h "0j2i0fk6fzpffy1n4ixnyxdyvlp41z0qhqpmhhgq89ajn0pryv9q") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-olio-1.0.0 (c (n "olio") (v "1.0.0") (d (list (d (n "bytes") (r ">= 0.4.6, < 0.5") (d #t) (k 2)) (d (n "libc") (r ">= 0.2.42, < 0.3") (d #t) (k 0)) (d (n "memmap") (r ">= 0.7.0, < 0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r ">= 0.6.1, < 0.7") (d #t) (k 2)) (d (n "tempfile") (r ">= 2.2.0, < 4") (d #t) (k 2)))) (h "1x8nzn3gpicm3h9v35rkqyn74sszpp96i7qz2md30pmzx7nzg6ab") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-olio-1.0.1 (c (n "olio") (v "1.0.1") (d (list (d (n "bytes") (r ">= 0.4.6, < 0.5") (d #t) (k 2)) (d (n "libc") (r ">= 0.2.42, < 0.3") (d #t) (k 0)) (d (n "memmap") (r ">= 0.7.0, < 0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r ">= 0.6.1, < 0.7") (d #t) (k 2)) (d (n "tempfile") (r ">= 3.0.5, < 4") (d #t) (k 2)))) (h "0dwj7jl42xmwrimw3sh7jpc622x118k16dflq0cgab3b5wk94x24") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-olio-1.1.0 (c (n "olio") (v "1.1.0") (d (list (d (n "bytes") (r ">= 0.4.6, < 0.5") (d #t) (k 2)) (d (n "libc") (r ">= 0.2.42, < 0.3") (d #t) (k 0)) (d (n "memmap") (r ">= 0.7.0, < 0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r ">= 0.6.1, < 0.7") (d #t) (k 2)) (d (n "tempfile") (r ">= 3.0.5, < 3.1") (d #t) (k 2)))) (h "0h0c12bk4imhd9x1cmv963g5r0hl3zhy4ym6r3a71xflqk4jdzzq") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-olio-1.2.0 (c (n "olio") (v "1.2.0") (d (list (d (n "bytes") (r ">= 0.4.6, < 0.5") (d #t) (k 2)) (d (n "libc") (r ">= 0.2.42, < 0.3") (d #t) (k 0)) (d (n "memmap") (r ">= 0.7.0, < 0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r ">= 0.7.0, < 0.8") (d #t) (k 2)) (d (n "tempfile") (r ">= 3.1.0, < 3.2") (d #t) (k 2)))) (h "06qqdksszs11chah7rkp1zimhk8wj4fqpziivfy7ds60jp9r8q63") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-olio-1.3.0 (c (n "olio") (v "1.3.0") (d (list (d (n "bytes") (r ">= 0.4.6, < 0.6") (d #t) (k 2)) (d (n "libc") (r ">= 0.2.42, < 0.3") (d #t) (k 0)) (d (n "memmap") (r ">= 0.7.0, < 0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r ">= 0.7.0, < 0.8") (d #t) (k 2)) (d (n "tempfile") (r ">= 3.1.0, < 3.2") (d #t) (k 2)))) (h "0k6qmfwm2rv7naxkr7aprfm4zfkpdqs91c6jqvyi69kqca81zv35") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-olio-1.4.0 (c (n "olio") (v "1.4.0") (d (list (d (n "bytes") (r ">=0.5.3, <0.6") (d #t) (k 2)) (d (n "libc") (r ">=0.2.42, <0.3") (d #t) (k 0)) (d (n "memmap") (r ">=0.7.0, <0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r ">=0.7.0, <0.8") (d #t) (k 2)) (d (n "remove_dir_all") (r ">=0.5.0, <0.5.3") (k 2)) (d (n "tempfile") (r ">=3.1.0, <3.2") (d #t) (k 2)))) (h "0zb12l2p63cwynaannz6fb6amzic4sy9wliv81v8w4gq7pc7b42m") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

