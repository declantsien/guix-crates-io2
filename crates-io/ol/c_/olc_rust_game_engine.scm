(define-module (crates-io ol c_ olc_rust_game_engine) #:use-module (crates-io))

(define-public crate-olc_rust_game_engine-0.1.0 (c (n "olc_rust_game_engine") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1zbvg1hr9lrv57l2a216b8dh9k2szx5r6hza0clyfs50wz5x0dbw")))

(define-public crate-olc_rust_game_engine-0.2.0 (c (n "olc_rust_game_engine") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "keyboard_query") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "06xw0797nzxdf5j02ixfnlnbk0942gdjh5y3sygqyrzfbp5hjfcm")))

(define-public crate-olc_rust_game_engine-0.2.1 (c (n "olc_rust_game_engine") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "keyboard_query") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0k0ifvk9wilmppcymhbgpx0a023s844xlz55kprgpzyi3rkfxqxd")))

(define-public crate-olc_rust_game_engine-0.2.2 (c (n "olc_rust_game_engine") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "keyboard_query") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1zv0pkbin9bq9xabajh3wh6vxz4v9a2sf145j9l6sa97m3ga3k4s")))

(define-public crate-olc_rust_game_engine-0.3.0 (c (n "olc_rust_game_engine") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "keyboard_query") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "09p5dyf4r3j5l3ghaaffq6k728kawp1awzg1wi20035h106imlc5")))

(define-public crate-olc_rust_game_engine-0.3.1 (c (n "olc_rust_game_engine") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "keyboard_query") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1bbaxwn4qiiqbanyjry2217ddc6rmlq9wys6kgbjv60gx6ri1hs6")))

(define-public crate-olc_rust_game_engine-0.3.11 (c (n "olc_rust_game_engine") (v "0.3.11") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "keyboard_query") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1j0jj23b0sgq3vr8i01xg6a94f71glmlb6d7a11x61z8f9442n1r")))

