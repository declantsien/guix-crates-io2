(define-module (crates-io ol c_ olc_pixel_game_engine) #:use-module (crates-io))

(define-public crate-olc_pixel_game_engine-0.1.0 (c (n "olc_pixel_game_engine") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0dwdfvp7l4xsnyc7agxdr8gqw6287i1wfjzxq92bxdk8vir6j244")))

(define-public crate-olc_pixel_game_engine-0.2.0 (c (n "olc_pixel_game_engine") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1sfpwsmrh1sdfa07mql5mxqsj6nacia5a55ncv8mm1q407pqlgwr")))

(define-public crate-olc_pixel_game_engine-0.3.0 (c (n "olc_pixel_game_engine") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "10d74zvvm5cmm048j5sxcx665alr6i1vif9rk42y18rfd1jcwzbf")))

(define-public crate-olc_pixel_game_engine-0.4.0 (c (n "olc_pixel_game_engine") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "13zniliaxpbcwqga3g8wc2i55d6kks3f245qsmrh7gacl9g8y2pc")))

(define-public crate-olc_pixel_game_engine-0.5.0 (c (n "olc_pixel_game_engine") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1znvckaijm4md5ybs7g348hqz22ns019wpg7x1sgfa1dicq339b1")))

(define-public crate-olc_pixel_game_engine-0.6.0 (c (n "olc_pixel_game_engine") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0h0fqci1821riy9rm42j50w0pyn9x383djbfq39bs9jb20prripq")))

