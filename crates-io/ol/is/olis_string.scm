(define-module (crates-io ol is olis_string) #:use-module (crates-io))

(define-public crate-olis_string-0.1.0 (c (n "olis_string") (v "0.1.0") (h "0r832gjj8i3yg0dl2l60zm5yy0qps74bqkskq5fqyainjij31xij") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-olis_string-0.1.1 (c (n "olis_string") (v "0.1.1") (h "0zx6x7b0x1ah553fmv5cpjfx8nzx01m1pz13gh9l40kyj8kjzqks") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-olis_string-0.1.2 (c (n "olis_string") (v "0.1.2") (h "0s16a8ql9vdns1np7sqyhzv3hgd4jvivmz60bz5iz8ms5rrqzmy4") (f (quote (("nightly") ("default"))))))

(define-public crate-olis_string-0.1.3 (c (n "olis_string") (v "0.1.3") (h "1frnibadv6611dfvbfhf3hnamb0ksv6k01wakrzzaik0f3l6jpv1") (f (quote (("nightly") ("default"))))))

