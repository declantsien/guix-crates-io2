(define-module (crates-io ol la ollama-inquire) #:use-module (crates-io))

(define-public crate-ollama-inquire-0.1.0 (c (n "ollama-inquire") (v "0.1.0") (d (list (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)))) (h "1nffyy96ap94y6mx5yvzmm6sm06ysj6p1nhi8ig5qmwd86warfls")))

(define-public crate-ollama-inquire-1.0.0 (c (n "ollama-inquire") (v "1.0.0") (d (list (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)))) (h "0mkszww1c3z2z1kil11cqp94c1zvivg989q36562q6h0q1mqb1yq")))

(define-public crate-ollama-inquire-1.0.1 (c (n "ollama-inquire") (v "1.0.1") (d (list (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)))) (h "1q61lba45qvniwr0ynbjjq977vq8mhwp02q3w2swyi5gjm2v2jpv")))

(define-public crate-ollama-inquire-1.0.2 (c (n "ollama-inquire") (v "1.0.2") (d (list (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)))) (h "0ax8h9cjhxx51zq25akl6dw3ny36ljyxixpaic53kzgnazidk2xl")))

(define-public crate-ollama-inquire-1.0.3 (c (n "ollama-inquire") (v "1.0.3") (d (list (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)))) (h "1afjaqmqwd3ip0dv7dwgh3w0kry0gr9nxw8jn88pc88sjzpbn6ci")))

(define-public crate-ollama-inquire-1.0.4 (c (n "ollama-inquire") (v "1.0.4") (d (list (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)))) (h "0f03d0vv3yi50dd6bpvpgp9cad3lv8lywcqbfynh9sa5paw6fq0d")))

(define-public crate-ollama-inquire-1.0.6 (c (n "ollama-inquire") (v "1.0.6") (d (list (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)))) (h "0qdc93h18xldapx2zlfwg4a7jl6va8kizh45m5l7xv8r7pf0vwnc")))

