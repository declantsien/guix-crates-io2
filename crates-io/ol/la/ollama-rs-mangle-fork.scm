(define-module (crates-io ol la ollama-rs-mangle-fork) #:use-module (crates-io))

(define-public crate-ollama-rs-mangle-fork-0.1.1 (c (n "ollama-rs-mangle-fork") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.14") (o #t) (d #t) (k 0)))) (h "15xdbrcxr8sqjw4zvrkdaqwdvdf3010yylgisa0rh19ifcq4afs0") (f (quote (("stream" "tokio-stream" "reqwest/stream" "tokio"))))))

