(define-module (crates-io ol la ollama-bash-command-error) #:use-module (crates-io))

(define-public crate-ollama-bash-command-error-0.1.0 (c (n "ollama-bash-command-error") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "0jk56r7jw8w8i0q91l7bcjf7vlxww29qs8ky5286ayffnnhz6jbm")))

