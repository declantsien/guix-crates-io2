(define-module (crates-io m5 x7 m5x7) #:use-module (crates-io))

(define-public crate-m5x7-1.0.0 (c (n "m5x7") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.70.0") (d #t) (k 2)) (d (n "rusttype") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "rusttype") (r "^0.8.1") (d #t) (k 2)))) (h "02mhd52n81f9xghxapplzirjnnww1a51f4i3v06b70mp308s4ky4") (f (quote (("parsed" "rusttype" "lazy_static") ("default"))))))

