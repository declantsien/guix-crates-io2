(define-module (crates-io fa dv fadvise) #:use-module (crates-io))

(define-public crate-fadvise-0.1.0 (c (n "fadvise") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.2.4") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "1hywws7ax0c6zz26pvl1dparympgafkvfj9yvcik69n82gp36mji") (r "1.63")))

