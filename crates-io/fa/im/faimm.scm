(define-module (crates-io fa im faimm) #:use-module (crates-io))

(define-public crate-faimm-0.1.0 (c (n "faimm") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "1sc73b547c5kim13ayfyv2bm58irhkhr21znw9a3srjf780aq326")))

(define-public crate-faimm-0.2.0 (c (n "faimm") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0sw2sxwb4d2416iscl2rxr2nwqajj31g5yzkisqi4xg4mzcg146f")))

(define-public crate-faimm-0.2.1 (c (n "faimm") (v "0.2.1") (d (list (d (n "indexmap") (r "^1.5.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0rzbg4m1kmhf10b4kb4z3cljsng12jm596hl5yfpampp6anqn5jz")))

(define-public crate-faimm-0.3.0 (c (n "faimm") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.5.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.3") (d #t) (k 0)))) (h "0kfq7zy9wvhri7vdlv7i2k4ghspznkfqy9w0lnqbc84d8gxjpbm3")))

(define-public crate-faimm-0.4.0 (c (n "faimm") (v "0.4.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.7") (d #t) (k 0)))) (h "0vzblbcyf28dmx4ihcx49j7y4nmdzzknb88bg4ny8wd70facx9ss")))

(define-public crate-faimm-0.5.0 (c (n "faimm") (v "0.5.0") (d (list (d (n "indexmap") (r "^2.2.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)))) (h "1idh4rvk62byf7m4k8zx36b6gf104wsvdww20fpp0ac14mfkjbii")))

