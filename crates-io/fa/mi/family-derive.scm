(define-module (crates-io fa mi family-derive) #:use-module (crates-io))

(define-public crate-family-derive-0.1.0 (c (n "family-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1qzwqphwjfdqh8i4fwy2ncfm25rkrv17jhhc87vd2d84k5g672vy")))

(define-public crate-family-derive-0.1.1 (c (n "family-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0jm1dbg1vpbv3q622mybyankvhiff0ymd29gxf8c6fbv7swsff81")))

