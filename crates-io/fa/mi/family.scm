(define-module (crates-io fa mi family) #:use-module (crates-io))

(define-public crate-family-0.1.0 (c (n "family") (v "0.1.0") (h "0vafyhlmcmfp5prd8rzjdvfkzsmmjzbjbx65c241xm4f74s5wbzg") (y #t)))

(define-public crate-family-0.1.1 (c (n "family") (v "0.1.1") (h "1ds53488x0s12m9qjr1vyi41ddmw9msrcsapql2kr0xw1hyqwba3")))

(define-public crate-family-0.1.2 (c (n "family") (v "0.1.2") (d (list (d (n "family-derive") (r "^0.1.0") (d #t) (k 0)))) (h "14rkfg9ylbf18v3dmm1yzy8nnks6iv0blx9p15437xdn3a2z93w7")))

