(define-module (crates-io fa ux faux_macros) #:use-module (crates-io))

(define-public crate-faux_macros-0.0.1 (c (n "faux_macros") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cb2siii50rh4gf96wa9ilcykw6p93ns4q0060a92wpc4f4mch13")))

(define-public crate-faux_macros-0.0.2 (c (n "faux_macros") (v "0.0.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cf1b3ydgpasgjhk9c9fq4ygzz7xl6dnq1fsbm4f9p7q771gpqvs")))

(define-public crate-faux_macros-0.0.3 (c (n "faux_macros") (v "0.0.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1f7rz5ind11gk04jmg2s8g3n0hhrbv0czyj7i577y85x3n440qcc")))

(define-public crate-faux_macros-0.0.4 (c (n "faux_macros") (v "0.0.4") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q8gaj3k33r6r109qwddx7qi79j9ljf1y8g01irsvv368z4j7pnp")))

(define-public crate-faux_macros-0.0.5 (c (n "faux_macros") (v "0.0.5") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "189xigd6bnvgbkh8m0yhmsi9r5f7a1g4626jhqzr14hz46v0mvbf")))

(define-public crate-faux_macros-0.0.6 (c (n "faux_macros") (v "0.0.6") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fv0g24b6qy3pmyzscbbb1wki0zsp0chw0qdm5ciz51v264i94iv")))

(define-public crate-faux_macros-0.0.7 (c (n "faux_macros") (v "0.0.7") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10kd1w4mygy78xahhzczd6r1splbn3dslhlwz9qx0nqs6970fb4a")))

(define-public crate-faux_macros-0.0.8 (c (n "faux_macros") (v "0.0.8") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bs5farqlwr6fqxq8n0hzskkd0zq6pbll12vjj2qgf0vhbh69w3p")))

(define-public crate-faux_macros-0.0.9 (c (n "faux_macros") (v "0.0.9") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13yrjxvz5xwv7mmxx69w89gcwfqwgxvb4v549mz1gsx0p4dkypz4")))

(define-public crate-faux_macros-0.0.10 (c (n "faux_macros") (v "0.0.10") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zzils4ciz7hhgbj70y89bhs9x201dc3y6gscr7pb12hdsnhbp0k")))

(define-public crate-faux_macros-0.1.0 (c (n "faux_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19xfa660in6sf28ny45kgfym4xcwzysdzrprf6l4id268nl5ywi5")))

(define-public crate-faux_macros-0.1.1 (c (n "faux_macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "089k7lns28kxwjkqm658xyhdc71j78x1wa7g2s18xffdlvpv7wyi")))

(define-public crate-faux_macros-0.1.2 (c (n "faux_macros") (v "0.1.2") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xv990ip9fcq6skzvf33rmm9rvjwbipadvl28j8lkqs2cp6d78mv")))

(define-public crate-faux_macros-0.1.3 (c (n "faux_macros") (v "0.1.3") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "087l49wjk0vr694cp7xi74mdk9vhjdygfpdyz8n8lm14817p3qdm")))

(define-public crate-faux_macros-0.1.4 (c (n "faux_macros") (v "0.1.4") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gq22f4jx33fsfv5zpr25rmypwwd148zk4fjdz3qlzlmvama2799")))

(define-public crate-faux_macros-0.1.5 (c (n "faux_macros") (v "0.1.5") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xqjnlcy66b6jv2jd3qa8bxvq5n170g1hb3wzmrarg427ry13yf2")))

(define-public crate-faux_macros-0.1.6 (c (n "faux_macros") (v "0.1.6") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n2b4n4rrfswvfa62zkj2gi9dq779y1d5nc7x2fhxzqsaj3x5dgf")))

(define-public crate-faux_macros-0.1.7 (c (n "faux_macros") (v "0.1.7") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "000f2xzz6ihx451jmb169h4696n4paprha0ym1p1sacjd1d3vxb5")))

(define-public crate-faux_macros-0.1.8 (c (n "faux_macros") (v "0.1.8") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0kr0p4dqf9c86py20vl6ybn94v9b59xabcvadyam1dj5gmyrnx5k") (r "1.56")))

(define-public crate-faux_macros-0.1.9 (c (n "faux_macros") (v "0.1.9") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jkcs8ry9bj90kdvcya7d9qa343i9spjm41r7alv7zqk5i5bpj9m") (r "1.56")))

(define-public crate-faux_macros-0.1.10 (c (n "faux_macros") (v "0.1.10") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1m9mmbcyzlqn813m2fz5yv5alzkjwmmg2ylh4r4a8m1vz3bp6m6i") (r "1.58")))

