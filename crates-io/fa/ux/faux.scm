(define-module (crates-io fa ux faux) #:use-module (crates-io))

(define-public crate-faux-0.0.1 (c (n "faux") (v "0.0.1") (d (list (d (n "faux_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "170g7pv2rhlblfqdhjn1b26641f3nr9l8yn6b6snclm83ycahfxi")))

(define-public crate-faux-0.0.2 (c (n "faux") (v "0.0.2") (d (list (d (n "faux_macros") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "09nrvnbdr9yvj3hrkyz16lld3y9jw6zhk2m6h05f1q4k8n3asnw4")))

(define-public crate-faux-0.0.3 (c (n "faux") (v "0.0.3") (d (list (d (n "faux_macros") (r "^0.0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1i3v4n9zxg8vywihyg8d58pqw4xm1w8ni0sj5jnwb7bvv70c2jxr")))

(define-public crate-faux-0.0.4 (c (n "faux") (v "0.0.4") (d (list (d (n "faux_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "10w29rf7hjfm3la6g548fikys2iq84zy0fxxaibdabc7xiscd0xv")))

(define-public crate-faux-0.0.5 (c (n "faux") (v "0.0.5") (d (list (d (n "faux_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "07dnkdcy25zw4gis7l67w0ggnafqyzhv0dci8sdf3sd8b46va1k7")))

(define-public crate-faux-0.0.6 (c (n "faux") (v "0.0.6") (d (list (d (n "faux_macros") (r "^0.0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "166qxv4lylz7axqdkc3dhdgiz1fzabiy8b1xnnnlz46bvym3zds3")))

(define-public crate-faux-0.0.7 (c (n "faux") (v "0.0.7") (d (list (d (n "faux_macros") (r "^0.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)))) (h "12vrm3g1bg524x87mbydain5clkbajxv460mz7mlw53a6h0s5p60")))

(define-public crate-faux-0.0.8 (c (n "faux") (v "0.0.8") (d (list (d (n "faux_macros") (r "^0.0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)))) (h "1ys4704lscdg35rqdf5b24nwh66vzxvf67kyjikqdxw7072r8vjk")))

(define-public crate-faux-0.0.9 (c (n "faux") (v "0.0.9") (d (list (d (n "faux_macros") (r "^0.0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "135an3xwahg79hm3pn18csn4p6ys71zkdh6kzfyjmga6mbnp7ahc")))

(define-public crate-faux-0.0.10 (c (n "faux") (v "0.0.10") (d (list (d (n "faux_macros") (r "^0.0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1r0hpd0j73splzpyx9b06ql6pw445pssh5czrsd7abklgy6kyrxg")))

(define-public crate-faux-0.1.0 (c (n "faux") (v "0.1.0") (d (list (d (n "faux_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1z5s501yab7274rjpp8r1103r2k88h4s76kw8hrpisb50vrglz31")))

(define-public crate-faux-0.1.1 (c (n "faux") (v "0.1.1") (d (list (d (n "faux_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0ydhxy3i2z9kxly8zw85ri2z9kpdfqmnva6xfmljbbmvqv4z7028")))

(define-public crate-faux-0.1.2 (c (n "faux") (v "0.1.2") (d (list (d (n "faux_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "07q6hybsy505qvcpqszflfna3jjplfpwbvhbh70i4al47616dhs0")))

(define-public crate-faux-0.1.3 (c (n "faux") (v "0.1.3") (d (list (d (n "faux_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1b75m3n0bs5h1c16ispkchdwkdsrp71f6gdbnwsrxfjvql4swnr5")))

(define-public crate-faux-0.1.4 (c (n "faux") (v "0.1.4") (d (list (d (n "faux_macros") (r "^0.1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "01ngb8nsmv5gbffdn7sn29cjwfvx4k6qqqwn7f20ib4j0dsz8a5g")))

(define-public crate-faux-0.1.5 (c (n "faux") (v "0.1.5") (d (list (d (n "faux_macros") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1vy3qalfl1ykybhqkhch8b00has4wzh47zjybpvh4qy3xwfm0rhj")))

(define-public crate-faux-0.1.6 (c (n "faux") (v "0.1.6") (d (list (d (n "faux_macros") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0w7syywlmfzq9h3w6z0zwa7aglw3xd78zwwd8bq2shgnfivb6815")))

(define-public crate-faux-0.1.7 (c (n "faux") (v "0.1.7") (d (list (d (n "faux_macros") (r "^0.1.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0cpkby1b3yrxfrb3nzml0i74m2zb0nl3a3wi2c0pin6d1vjx2d09")))

(define-public crate-faux-0.1.8 (c (n "faux") (v "0.1.8") (d (list (d (n "faux_macros") (r "^0.1.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1i6rmqpq7kanrf5wiyfxkrv26awvkv3zqf6hf7hgd3fwhmdk56wk") (r "1.56")))

(define-public crate-faux-0.1.9 (c (n "faux") (v "0.1.9") (d (list (d (n "faux_macros") (r "^0.1.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1w61clgzvsj8hhp9qlsykzl0zyql9r49vkci24j7r9lwlrb5wfvw") (r "1.56")))

(define-public crate-faux-0.1.10 (c (n "faux") (v "0.1.10") (d (list (d (n "faux_macros") (r "^0.1.10") (d #t) (k 0)) (d (n "futures") (r "^0.3.9") (d #t) (k 2)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0q6dz9gbrbib47xdrq5zag25mc8ijxnxqp2v24a26lsf89zmmd8l") (r "1.58")))

