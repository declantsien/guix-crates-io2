(define-module (crates-io fa ux fauxgen-macros) #:use-module (crates-io))

(define-public crate-fauxgen-macros-0.1.0 (c (n "fauxgen-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0svlkb4izvrpv5mfmiyj15wsg1xf6apm5zjj86rv4bk45zd29h0h")))

(define-public crate-fauxgen-macros-0.1.1 (c (n "fauxgen-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1fg4m8l4bj31pi74xw931c15bqsfdmf55i3mjrr3yqfkll80jy5w")))

(define-public crate-fauxgen-macros-0.1.2 (c (n "fauxgen-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1l40a15yriadw5h7zliqfifvw258i9xd7ck7d0aligil8cmyv8kn")))

(define-public crate-fauxgen-macros-0.1.3 (c (n "fauxgen-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1fqjz2l41cjgqv8s2bkgxyfh87wnm0phg06jaygbg0l0kx56mm4x")))

(define-public crate-fauxgen-macros-0.1.4 (c (n "fauxgen-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "13q0bxji5pwijrkm0nw8llzhfw432rc91bd9rzfvbxzvadfqz16i")))

