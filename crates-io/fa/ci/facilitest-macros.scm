(define-module (crates-io fa ci facilitest-macros) #:use-module (crates-io))

(define-public crate-facilitest-macros-0.1.0 (c (n "facilitest-macros") (v "0.1.0") (h "1qhpn5w821ggkbfq7zb40mqih36wns7967lf6fd68ygsjs0wzv6z") (y #t)))

(define-public crate-facilitest-macros-1.0.0 (c (n "facilitest-macros") (v "1.0.0") (h "17dyp5pvaxjhcdvpszg8yxfpa3bdfanh1h34zxw86yhfjszmpcl0")))

