(define-module (crates-io fa ci facilitest) #:use-module (crates-io))

(define-public crate-facilitest-0.1.0 (c (n "facilitest") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "06q9p6gcyj87cc3pz3z2srsbhvsawx3zw4n3vhqwg5iybhhx5vr1") (y #t)))

(define-public crate-facilitest-0.1.1 (c (n "facilitest") (v "0.1.1") (d (list (d (n "facilitest-macros") (r "^0.1.0") (d #t) (k 0)))) (h "18hq6gm50pkmk2zkppdlpfc49dd44axl2lnj6qwj9g5dfbbxmc0d") (y #t)))

(define-public crate-facilitest-0.1.2 (c (n "facilitest") (v "0.1.2") (d (list (d (n "facilitest-macros") (r "^0.1.0") (d #t) (k 0)))) (h "10lr3psdk2mlpcrkks11fyysj20v5d0flj2q63sbyyk7alxi15dn") (y #t)))

(define-public crate-facilitest-1.0.0 (c (n "facilitest") (v "1.0.0") (d (list (d (n "facilitest-macros") (r "^1.0.0") (d #t) (k 0)))) (h "0l7svsijp8861z4448iqfyl0qizm4c4kn4jwcyc7zlwv1j9avyr1")))

