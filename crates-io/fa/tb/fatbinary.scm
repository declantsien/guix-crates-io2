(define-module (crates-io fa tb fatbinary) #:use-module (crates-io))

(define-public crate-fatbinary-0.1.0 (c (n "fatbinary") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "binread") (r "^2.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0rd8vypny8xf68sbi7s5ay6l618g2hjfvri7g1wpd7i7a360s83x")))

(define-public crate-fatbinary-0.2.0 (c (n "fatbinary") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "binread") (r "^2.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0azkrfcmdvy25396p31fmprc6j8f71imag9kpxicr1jcjjv044q1")))

