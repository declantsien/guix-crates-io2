(define-module (crates-io fa ll fallible_vec) #:use-module (crates-io))

(define-public crate-fallible_vec-0.1.0 (c (n "fallible_vec") (v "0.1.0") (h "1fic6qq0ay627ai221zj0rs6s1s8v2nim3xzlqvbhp31r7ygh47z")))

(define-public crate-fallible_vec-0.2.0 (c (n "fallible_vec") (v "0.2.0") (h "1nchxv1251g2q3phg7z8vg6sx5pypybjn11gdhahylgkxz975jiw")))

(define-public crate-fallible_vec-0.3.0 (c (n "fallible_vec") (v "0.3.0") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0pgxr83jvgyv2chapd6vl8pxb8ycwcd20k364lccc78vp4y29gk9") (f (quote (("use_unstable_apis") ("default" "allocator_api" "use_unstable_apis") ("allocator_api"))))))

(define-public crate-fallible_vec-0.3.1 (c (n "fallible_vec") (v "0.3.1") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1wmcncnjjky178ja9fn7v5shc8r30l3nclwz4mfvid7m6v2zmr21") (f (quote (("use_unstable_apis") ("default" "allocator_api" "use_unstable_apis") ("allocator_api"))))))

