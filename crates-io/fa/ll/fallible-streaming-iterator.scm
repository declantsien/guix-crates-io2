(define-module (crates-io fa ll fallible-streaming-iterator) #:use-module (crates-io))

(define-public crate-fallible-streaming-iterator-0.1.0 (c (n "fallible-streaming-iterator") (v "0.1.0") (h "156g52n4b5svsa6m7qyvzc739s3zy7ng4dlq1dg71gsscapy79f3")))

(define-public crate-fallible-streaming-iterator-0.1.1 (c (n "fallible-streaming-iterator") (v "0.1.1") (h "1jh3jkk477icph252wvl0q0zmbbyd7qmb0hr2nn7xbvi4jkqj5zj") (f (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1.2 (c (n "fallible-streaming-iterator") (v "0.1.2") (h "02rmzd9ff5bfhx0zsj5v04agn4m94xsyhapm01r743ibdv4kwqfr") (f (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1.3 (c (n "fallible-streaming-iterator") (v "0.1.3") (h "1mvnkdwla828q42p7hjfx3v5ip4ibx1y2q2lmm9vff32nbny2lfj") (f (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1.4 (c (n "fallible-streaming-iterator") (v "0.1.4") (h "12s18mlgi5wn4gdf38xxy9qccval4zbnwsb3dmjsl4x3p7llrgfb") (f (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1.5 (c (n "fallible-streaming-iterator") (v "0.1.5") (h "04zvan922pg4ckhvbmx2skjr6q7v3bqxirh2l1dw5wqplfmslwg0") (f (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1.6 (c (n "fallible-streaming-iterator") (v "0.1.6") (h "16ys004saja9s1lf36wc9x4wrzyy31l4z9rvwhi5grr2b91bd7nb") (f (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1.7 (c (n "fallible-streaming-iterator") (v "0.1.7") (h "0djrydqw94qgkrwhd1zhzfsfinwadbrjn27m6di20c8pnq3ws7gx") (f (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1.8 (c (n "fallible-streaming-iterator") (v "0.1.8") (h "0xi94rg46idbq6psppjijn664z1rkyzwi880z54ak0j93wiiwr75") (f (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1.9 (c (n "fallible-streaming-iterator") (v "0.1.9") (h "0nj6j26p71bjy8h42x6jahx1hn0ng6mc2miwpgwnp8vnwqf4jq3k") (f (quote (("std"))))))

