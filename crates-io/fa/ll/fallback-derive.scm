(define-module (crates-io fa ll fallback-derive) #:use-module (crates-io))

(define-public crate-fallback-derive-0.1.0 (c (n "fallback-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vrii71k9iwipl1csqizzlch92mx99r7cvhyvlrnh1rsvv9fivyk")))

(define-public crate-fallback-derive-0.1.1 (c (n "fallback-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i4vqaq2hz9z8b49mbcl9y6xf3p60nqx0dga33mzahnhkcqcmnmc")))

(define-public crate-fallback-derive-0.1.2 (c (n "fallback-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p1ydzagp77wfg9vi53mj1vcxl11a8kmsbg6vsa4iss0x0cg4396")))

