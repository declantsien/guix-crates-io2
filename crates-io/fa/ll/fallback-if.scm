(define-module (crates-io fa ll fallback-if) #:use-module (crates-io))

(define-public crate-fallback-if-1.0.0 (c (n "fallback-if") (v "1.0.0") (d (list (d (n "yare") (r "^3") (d #t) (k 2)))) (h "1xpqb3qyhb77xra7zxvj23x832zk5jl0npwjf7jcvymyhsfy6375") (r "1.60")))

(define-public crate-fallback-if-1.0.1 (c (n "fallback-if") (v "1.0.1") (d (list (d (n "yare") (r "^3") (d #t) (k 2)))) (h "01sq9fqwrk8h57j70pp75m2laj8acp3vwniawdww2by4zpsd5zik") (r "1.60")))

