(define-module (crates-io fa ll fallible_collections) #:use-module (crates-io))

(define-public crate-fallible_collections-0.1.0 (c (n "fallible_collections") (v "0.1.0") (h "1vj145k492q7wj9y8vn2mvci2zbzppm39lbxb09vis8k7avvgsrf")))

(define-public crate-fallible_collections-0.1.1 (c (n "fallible_collections") (v "0.1.1") (h "18fmg4fsx2slg8knjkzpfmypyzaymm2p6j0iz0207ywv2y94ms72")))

(define-public crate-fallible_collections-0.1.2 (c (n "fallible_collections") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.7.1") (d #t) (k 0)))) (h "01h30fx0ifbl0ssld7g6ks3q4fsfmdch752livwkag43bz0h5ayl") (f (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.1.3 (c (n "fallible_collections") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.7.1") (d #t) (k 0)))) (h "0rsa3wnplwi6ibqw7kdn9kwy23v3cbwlpzr6qqczrlslvsy02cxs") (f (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.2.0 (c (n "fallible_collections") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "0fwhvnk8f54cg23ksx1pgr33zpv400qfhbpzy0qj6km8ph24vniv") (f (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.3.0 (c (n "fallible_collections") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "05lfchnb9aib2vy84a55277mr718inypbri2ndkgqnhp98hzwvpl") (f (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.3.1 (c (n "fallible_collections") (v "0.3.1") (d (list (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "1shgcljh6pliv1b1qk6knk2hzig5ah76hx01f1icpgkiqp6fi6cm") (f (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.4.0 (c (n "fallible_collections") (v "0.4.0") (d (list (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "11iijssvznmd5xfi5mgbji30ccs3fvqy6mf434b3r21fzq7bzgkl") (f (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.4.1 (c (n "fallible_collections") (v "0.4.1") (d (list (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "1rn5dscajzm9f6dih2safzid0p60h4z0w5z6zjbyiwp1d38q0ydg") (f (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.4.2 (c (n "fallible_collections") (v "0.4.2") (d (list (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "1sk6ckixvf0pax47qgs8lfd2zi2gmyg1xgk1k7z2qgalhaaidnaa") (f (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.4.3 (c (n "fallible_collections") (v "0.4.3") (d (list (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)))) (h "006cfs45jdsczk7a8rhmf2rsw4vz5m2x74q7dzqmim2i04cx9vza") (f (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.4.4 (c (n "fallible_collections") (v "0.4.4") (d (list (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)))) (h "1xwmi3fdag0fysr5s2nfx71gzgx14cfg8c4vy6x4g4m1nrrmknsj") (f (quote (("unstable") ("std_io" "std") ("std") ("rust_1_57"))))))

(define-public crate-fallible_collections-0.4.5 (c (n "fallible_collections") (v "0.4.5") (d (list (d (n "hashbrown") (r "^0.12.1") (d #t) (k 0)))) (h "11ad7vdc5gn61afakyfhw6xjhmqddg2lsyw8xf9wklw5495wz5f1") (f (quote (("unstable") ("std_io" "std") ("std") ("rust_1_57"))))))

(define-public crate-fallible_collections-0.4.6 (c (n "fallible_collections") (v "0.4.6") (d (list (d (n "hashbrown") (r "^0.12.1") (d #t) (k 0)))) (h "0ma7lga3zqbpzrhl76raljc6y69f38mb6j5yhkk6ldkh531wqmrz") (f (quote (("unstable") ("std_io" "std") ("std") ("rust_1_57"))))))

(define-public crate-fallible_collections-0.4.7 (c (n "fallible_collections") (v "0.4.7") (d (list (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)))) (h "0k87q5f4j4iz5gh4s5idii785ncs2l792jsam7nczwslalh7gkws") (f (quote (("unstable") ("std_io" "std") ("std") ("rust_1_57") ("hashmap" "hashbrown") ("default" "hashmap"))))))

(define-public crate-fallible_collections-0.4.8 (c (n "fallible_collections") (v "0.4.8") (d (list (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)))) (h "01c5nbncky1azi8c0h0brzh97zn3afgi90djwx89r9cjwqhg52v1") (f (quote (("unstable") ("std_io" "std") ("std") ("rust_1_57") ("hashmap" "hashbrown") ("default" "hashmap"))))))

(define-public crate-fallible_collections-0.4.9 (c (n "fallible_collections") (v "0.4.9") (d (list (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)))) (h "1zf6ir26qbdwlywv9m266n19p6yzqqmi968qy8njc58aiiv6k358") (f (quote (("unstable") ("std_io" "std") ("std") ("rust_1_57") ("hashmap" "hashbrown") ("default" "hashmap"))))))

