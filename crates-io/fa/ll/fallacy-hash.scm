(define-module (crates-io fa ll fallacy-hash) #:use-module (crates-io))

(define-public crate-fallacy-hash-0.1.0 (c (n "fallacy-hash") (v "0.1.0") (d (list (d (n "fallacy-alloc") (r "^0.1.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)))) (h "1fx3n1y7qrs1sp62vcmc53csba4q49xqgaiks9swrcixalljnjqy") (r "1.57")))

