(define-module (crates-io fa ll fallacy-arc) #:use-module (crates-io))

(define-public crate-fallacy-arc-0.1.0 (c (n "fallacy-arc") (v "0.1.0") (d (list (d (n "fallacy-alloc") (r "^0.1.0") (d #t) (k 0)))) (h "04w16b5wnn211ym8w7zv8h5gxv8lyb7n3snqhiv4730b6hpl950q") (r "1.57")))

(define-public crate-fallacy-arc-0.1.1 (c (n "fallacy-arc") (v "0.1.1") (d (list (d (n "fallacy-alloc") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)))) (h "0rnwp4mj899n1kpbcibhsl45sz9qmn6bpk37kisapr419q9qh3vv") (r "1.57")))

