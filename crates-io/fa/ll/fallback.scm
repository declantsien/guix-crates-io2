(define-module (crates-io fa ll fallback) #:use-module (crates-io))

(define-public crate-fallback-0.1.0 (c (n "fallback") (v "0.1.0") (d (list (d (n "fallback-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1993r36ca1vjr47ka780dsdwvpl2sx5xlgkr0gwm1mnh3j3i3c29")))

(define-public crate-fallback-0.1.1 (c (n "fallback") (v "0.1.1") (d (list (d (n "fallback-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1fi1wsjgc2ap9van8z9gy0p1jz7p3kmrinlmnd30qdmfna2sm72v")))

(define-public crate-fallback-0.1.2 (c (n "fallback") (v "0.1.2") (d (list (d (n "fallback-derive") (r "^0.1.1") (d #t) (k 0)))) (h "09vhjnwpxyxpam6zcynjjqx3krs2l7jh2llyyrj8dzahrzcj386p")))

(define-public crate-fallback-0.1.3 (c (n "fallback") (v "0.1.3") (d (list (d (n "fallback-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1h8y7wrsgvm70r6n3m72ris11xyr1kkrrid6h9kgzypfh7my0gf7")))

