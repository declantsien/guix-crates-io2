(define-module (crates-io fa ll fallible) #:use-module (crates-io))

(define-public crate-fallible-0.1.0 (c (n "fallible") (v "0.1.0") (d (list (d (n "void") (r "^1") (k 0)))) (h "0nnr23cfclw9y49cim1zlyjqf0k4d0jp4fvxb8a4myzsxz85xn9i")))

(define-public crate-fallible-0.1.1 (c (n "fallible") (v "0.1.1") (d (list (d (n "void") (r "^1") (k 0)))) (h "1mr62xvjcy8rgih6w5sfgjz10rmzdpphlqcis2qc4w52sy9s0day")))

(define-public crate-fallible-0.1.2 (c (n "fallible") (v "0.1.2") (d (list (d (n "void") (r "^1") (k 0)))) (h "074vjhv9v3v1phqipz4kchzxafqjqpn868a43dx50a4yc6ss829x")))

(define-public crate-fallible-0.1.3 (c (n "fallible") (v "0.1.3") (d (list (d (n "void") (r "^1") (k 0)))) (h "10xjsv1slg0s1xn7vhibhbpsqfrq2axcaxb020f2vli61g9v5z7s")))

