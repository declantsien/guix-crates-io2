(define-module (crates-io fa ll fallbaq) #:use-module (crates-io))

(define-public crate-fallbaq-0.1.0 (c (n "fallbaq") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.2.2") (d #t) (k 0)) (d (n "actix-http") (r "^1.0.1") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1.1") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "path-dedot") (r "^3.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1ik59x2gsjl9imss8svhkjmd3ykadca7rwkdr407l98f5w8qwbqi")))

