(define-module (crates-io fa ll fallthrough) #:use-module (crates-io))

(define-public crate-fallthrough-0.1.0 (c (n "fallthrough") (v "0.1.0") (h "059lc44pk3gdy100jksz2c2np74ywfw1i0da266ya85gxc8n7ibd")))

(define-public crate-fallthrough-0.1.1 (c (n "fallthrough") (v "0.1.1") (h "1f3ik7pdhc5sdddm1k6j5vi9fy8zhm3b8raqpmkavqdnj35mzfsr")))

(define-public crate-fallthrough-0.1.2 (c (n "fallthrough") (v "0.1.2") (h "1jxqsws4a66r0ywcldzs5vls9nvxjq70pw44d0iv45b21qaplsc2")))

