(define-module (crates-io fa ll fallacy-clone-derive) #:use-module (crates-io))

(define-public crate-fallacy-clone-derive-0.1.0 (c (n "fallacy-clone-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "1vx2cwylhvw3l2yyc0rljpfrsk2h3vzdaak52iwdb6kbxcqzarlr") (r "1.57")))

