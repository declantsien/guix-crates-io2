(define-module (crates-io fa ll fallacy-clone) #:use-module (crates-io))

(define-public crate-fallacy-clone-0.1.0 (c (n "fallacy-clone") (v "0.1.0") (d (list (d (n "fallacy-alloc") (r "^0.1.0") (d #t) (k 0)) (d (n "fallacy-clone-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "10y8phpfq35dmyxq5cnyzj5bhdgpv93cxgd95x7flzzlplld1nvs") (f (quote (("derive" "fallacy-clone-derive")))) (r "1.57")))

(define-public crate-fallacy-clone-0.1.1 (c (n "fallacy-clone") (v "0.1.1") (d (list (d (n "fallacy-alloc") (r "^0.1.0") (d #t) (k 0)) (d (n "fallacy-clone-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0.6") (d #t) (k 1)))) (h "1br0czch5srvnbygzzsa7vy4hrqpj32fvkh5x2iv78ss8apnd84m") (f (quote (("derive" "fallacy-clone-derive")))) (r "1.57")))

