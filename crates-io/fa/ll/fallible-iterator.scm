(define-module (crates-io fa ll fallible-iterator) #:use-module (crates-io))

(define-public crate-fallible-iterator-0.1.0 (c (n "fallible-iterator") (v "0.1.0") (h "036lzbrsaf32svh6a7k4p9bnnadv0c2vm41xsh53kqdylsx745m4")))

(define-public crate-fallible-iterator-0.1.1 (c (n "fallible-iterator") (v "0.1.1") (h "188ndbdxqbmwcs230si0k5vibnryh9z55y41pmbiwyzz3mdnvppw")))

(define-public crate-fallible-iterator-0.1.2 (c (n "fallible-iterator") (v "0.1.2") (h "0iwy4zla38vn24gs4lkyf9j2ps9z2rxhi3yzshh0d81q62q5wan6")))

(define-public crate-fallible-iterator-0.1.3 (c (n "fallible-iterator") (v "0.1.3") (h "07gzk5lnn0ynf7xlhjswq158h2r0vk1c436cx0l6c20sq4dsnj2x")))

(define-public crate-fallible-iterator-0.1.4 (c (n "fallible-iterator") (v "0.1.4") (h "0b84kff2a1gfw9v4wagysa68f1plxq1d4a01f73igr7wknf4l0yn") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-fallible-iterator-0.1.5 (c (n "fallible-iterator") (v "0.1.5") (h "02gmh1rilwbbxah5wwrn5gy9bkc9f7zg2g3nkvqpf39yg9d2jyga") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-fallible-iterator-0.2.1 (c (n "fallible-iterator") (v "0.2.1") (h "1x31skjsynn2h7sq3qzyv4zlyk2w8jmqcs3phsg4qxhz52yj16qx") (f (quote (("std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-fallible-iterator-0.1.6 (c (n "fallible-iterator") (v "0.1.6") (h "0bpp2812lxm9fjv082dsy70ggsfg40nhqva7nxr5dp0j9091fwpb") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-fallible-iterator-0.2.0 (c (n "fallible-iterator") (v "0.2.0") (h "1xq759lsr8gqss7hva42azn3whgrbrs2sd9xpn92c5ickxm1fhs4") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-fallible-iterator-0.3.0 (c (n "fallible-iterator") (v "0.3.0") (h "0ja6l56yka5vn4y4pk6hn88z0bpny7a8k1919aqjzp0j1yhy9k1a") (f (quote (("std" "alloc") ("default" "alloc") ("alloc"))))))

