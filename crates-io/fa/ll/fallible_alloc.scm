(define-module (crates-io fa ll fallible_alloc) #:use-module (crates-io))

(define-public crate-fallible_alloc-0.1.0 (c (n "fallible_alloc") (v "0.1.0") (h "1d5q3qpchgipgfmhl82wdd4p1kvcl8isvwziyjpd973vnqly7gdx")))

(define-public crate-fallible_alloc-0.2.0 (c (n "fallible_alloc") (v "0.2.0") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0kymll4p71i0gx9170dgxds2mkjmilknvgnz163g9z00wpc9dm6h")))

