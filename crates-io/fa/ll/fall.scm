(define-module (crates-io fa ll fall) #:use-module (crates-io))

(define-public crate-fall-0.1.0 (c (n "fall") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "frunk") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "lalrpop") (r "^0.16.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "1x9752k3zmvmsmxczykp8rwqcjd2hl9mcai2vjhjyi99xbygf1yx")))

(define-public crate-fall-0.1.1 (c (n "fall") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "frunk") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "lalrpop") (r "^0.16.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "0mrzcfqavddiwcvr98fpacrwhx2gdzrjzwrsrh9ffj6ypv4my6xd")))

(define-public crate-fall-0.1.2 (c (n "fall") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "frunk") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "lalrpop") (r "^0.16.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "1vha8j5v06wv5gyf9xkvb3yz09b4hljjwzzpzaqvz44fqp5caxjl")))

