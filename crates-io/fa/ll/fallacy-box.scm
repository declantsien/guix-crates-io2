(define-module (crates-io fa ll fallacy-box) #:use-module (crates-io))

(define-public crate-fallacy-box-0.1.0 (c (n "fallacy-box") (v "0.1.0") (d (list (d (n "fallacy-clone") (r "^0.1.1") (d #t) (k 0)))) (h "00w1pysr08ljf9i6xyb04bdxbciliig8049qgp4rixll3f54ycqh") (r "1.57")))

(define-public crate-fallacy-box-0.1.1 (c (n "fallacy-box") (v "0.1.1") (d (list (d (n "fallacy-clone") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)))) (h "1qxnwl7zlnwy0xxy7d5aj2ygqp0fi6n2i2h52prxd8qmj77hfxci") (r "1.57")))

