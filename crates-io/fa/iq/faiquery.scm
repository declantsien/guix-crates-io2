(define-module (crates-io fa iq faiquery) #:use-module (crates-io))

(define-public crate-faiquery-0.1.0 (c (n "faiquery") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)))) (h "18mzsw9qkq4b1ph1bjiz8i9idm0zqb0pnpi8rx7c67nvjwa591g2")))

(define-public crate-faiquery-0.1.1 (c (n "faiquery") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)))) (h "17q4x2ck0an5i3chm60scxab7xdn4pmd1qq923bzfbg510mqq4zc")))

(define-public crate-faiquery-0.1.2 (c (n "faiquery") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xdfxpiqp58abk2jssq46cikmgv83h7c70d2c4b62i7lc46dxrv7")))

(define-public crate-faiquery-0.1.3 (c (n "faiquery") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)))) (h "117v8ih7kb7srhdxinsvq59cw2c7ynhv6zgbv4wab0i61f5mh0qy")))

