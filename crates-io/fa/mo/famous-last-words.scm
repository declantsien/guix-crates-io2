(define-module (crates-io fa mo famous-last-words) #:use-module (crates-io))

(define-public crate-famous-last-words-0.1.0 (c (n "famous-last-words") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1df319x9cbaxm5n7a5hsmzza8awkjbbb990arbw7hy21bqrzyhfx")))

(define-public crate-famous-last-words-0.1.1 (c (n "famous-last-words") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0s5pg3xca4cn6mfzbkk8a97lk53cykvhxfjwh8fkkv0p08d2m4wv")))

