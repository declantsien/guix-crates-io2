(define-module (crates-io fa ri farid) #:use-module (crates-io))

(define-public crate-farid-0.1.0 (c (n "farid") (v "0.1.0") (h "1miz9fhgjcksrswc11y2m5smm57xh6n7rg0izcsnfj5j3g3fd2il")))

(define-public crate-farid-0.1.1 (c (n "farid") (v "0.1.1") (h "1j9i85na432ciwqp25hv3ms2b9i4l9988xxm1jjzfsr8nnp03f9n")))

(define-public crate-farid-0.1.2 (c (n "farid") (v "0.1.2") (h "0sym76imqfh3z3gnnihzabxq92kiz6hkz35k7c21d8s2plz97v7p")))

