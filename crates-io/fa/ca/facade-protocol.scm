(define-module (crates-io fa ca facade-protocol) #:use-module (crates-io))

(define-public crate-facade-protocol-0.1.0 (c (n "facade-protocol") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)))) (h "0b9ys4zii05ik08b9dnzr9pi9g2viaqj8jk2ndgbhkc40ashd781")))

