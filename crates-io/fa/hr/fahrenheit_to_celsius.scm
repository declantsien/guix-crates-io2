(define-module (crates-io fa hr fahrenheit_to_celsius) #:use-module (crates-io))

(define-public crate-fahrenheit_to_celsius-0.1.0 (c (n "fahrenheit_to_celsius") (v "0.1.0") (d (list (d (n "macro_colors") (r "^0.2.0") (d #t) (k 0)))) (h "1zihxgs1crrpqnxc0jfgcgf2dgw5lhirsjvinli334xagwm5dv2w")))

