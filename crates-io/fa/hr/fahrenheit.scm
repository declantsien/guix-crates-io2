(define-module (crates-io fa hr fahrenheit) #:use-module (crates-io))

(define-public crate-fahrenheit-4.5.1 (c (n "fahrenheit") (v "4.5.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)))) (h "0zx8p88cd6kjd6y176l5x722kx63ib389a3nk87qmx02s0abyari")))

(define-public crate-fahrenheit-4.5.2 (c (n "fahrenheit") (v "4.5.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)))) (h "0rpwjrm38r232b83agj2a0dzsbj4fwypb97xxl6j8wzr5wmpf96z")))

(define-public crate-fahrenheit-4.5.3 (c (n "fahrenheit") (v "4.5.3") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)))) (h "1bnrqaqczq49l245dsdxzfaiw73alxz3g4v8qq5c4ahkdbk1x371")))

(define-public crate-fahrenheit-4.5.4 (c (n "fahrenheit") (v "4.5.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)))) (h "1b2vybn5wxff1cvj6ap8sk2ga2wwyvhcn5wa1lwb1kwb5kh9c24v")))

