(define-module (crates-io fa er faery-ring) #:use-module (crates-io))

(define-public crate-faery-ring-0.1.0 (c (n "faery-ring") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 0)) (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.10.0") (d #t) (k 0)))) (h "0fr9fbw9g5q2n4rx6chlmcm4kjsf18qdvq57v6y24262rykbf0y4") (r "1.58")))

(define-public crate-faery-ring-0.2.0 (c (n "faery-ring") (v "0.2.0") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 0)) (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.10.0") (d #t) (k 0)))) (h "1adkmq0gm7cr5l787jhjwwj4mphwj91zs4qnkrj0src0nakzjhj0") (r "1.58")))

