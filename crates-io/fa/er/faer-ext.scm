(define-module (crates-io fa er faer-ext) #:use-module (crates-io))

(define-public crate-faer-ext-0.1.0 (c (n "faer-ext") (v "0.1.0") (d (list (d (n "faer") (r "^0.18.0") (k 0)) (d (n "nalgebra") (r "^0.32") (o #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (o #t) (k 0)) (d (n "num-complex") (r "^0.4.5") (k 0)) (d (n "polars") (r "^0.38") (f (quote ("lazy"))) (o #t) (d #t) (k 0)))) (h "18r3mn6za3s70442w6jfdy0zd6c33j5gf74znmbqrc2hpv2y0rsg") (f (quote (("default")))) (s 2) (e (quote (("polars" "dep:polars") ("ndarray" "dep:ndarray") ("nalgebra" "dep:nalgebra")))) (r "1.67.0")))

(define-public crate-faer-ext-0.2.0 (c (n "faer-ext") (v "0.2.0") (d (list (d (n "faer") (r "^0.19.0") (k 0)) (d (n "nalgebra") (r "^0.32") (o #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (o #t) (k 0)) (d (n "num-complex") (r "^0.4.5") (k 0)) (d (n "polars") (r "^0.40") (f (quote ("lazy"))) (o #t) (d #t) (k 0)))) (h "1j956v26hgb1j5h3jwadf45fw7hgwm26qmgp1h72qdrzwxm0zwsc") (f (quote (("default")))) (s 2) (e (quote (("polars" "dep:polars") ("ndarray" "dep:ndarray") ("nalgebra" "dep:nalgebra")))) (r "1.67.0")))

