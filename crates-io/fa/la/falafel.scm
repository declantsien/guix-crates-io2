(define-module (crates-io fa la falafel) #:use-module (crates-io))

(define-public crate-falafel-0.0.0 (c (n "falafel") (v "0.0.0") (h "1xyq3s0pfza40y2ralc2izdalim0xbnj5bxzj29ri070k7gqi9f0")))

(define-public crate-falafel-0.0.0+amqp-suppurt (c (n "falafel") (v "0.0.0+amqp-suppurt") (d (list (d (n "amiquip") (r "^0.4") (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)))) (h "0gnprl0sdvm96s1qhhn0hpmdwjnwzqr2f7cgirpljyz6yp76kdkp")))

(define-public crate-falafel-0.1.0+amqp-suppurt (c (n "falafel") (v "0.1.0+amqp-suppurt") (d (list (d (n "amiquip") (r "^0.4") (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)))) (h "1pzrkvlsaqd4rs9rbv7zfh4wf87i2i732nv7szymwvbhlxvk5nci")))

