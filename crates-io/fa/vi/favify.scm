(define-module (crates-io fa vi favify) #:use-module (crates-io))

(define-public crate-favify-0.1.0 (c (n "favify") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "resvg") (r "^0.26.1") (d #t) (k 0)))) (h "16h97nbm5cm0ss8z2ywm1xz43q1wgbd3f3qfayrliavkg9xhphh7")))

(define-public crate-favify-0.2.0 (c (n "favify") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "resvg") (r "^0.26.1") (d #t) (k 0)) (d (n "visioncortex") (r "^0.8.0") (d #t) (k 0)))) (h "0km7b5w0b23yrvpm9ckmbg3rc0s9cxsa39igl1k9424wap0kakbd")))

