(define-module (crates-io fa vi favicon-picker) #:use-module (crates-io))

(define-public crate-favicon-picker-1.0.0 (c (n "favicon-picker") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0") (d #t) (k 0)) (d (n "scraper") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0k9rzzwmdcn5idm55j45vlwklmswcmw05la5fibfb1h72x9n9r2w") (f (quote (("non_exhaustive") ("default" "non_exhaustive") ("blocking" "reqwest/blocking")))) (s 2) (e (quote (("serde" "dep:serde"))))))

