(define-module (crates-io fa vi favilla) #:use-module (crates-io))

(define-public crate-favilla-0.1.0 (c (n "favilla") (v "0.1.0") (h "15699y5x4n4cl7xwly2d8j61dv9zqjz3yywdxhjyjdfz2pv1ncfj")))

(define-public crate-favilla-0.1.1 (c (n "favilla") (v "0.1.1") (d (list (d (n "ash") (r "^0.33") (d #t) (k 0)) (d (n "ash-window") (r "^0.7") (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1jp073zqcjc4w94l6j045hsf65xlwypzjinfjxljjsswb4hkm0xb")))

(define-public crate-favilla-0.1.2 (c (n "favilla") (v "0.1.2") (d (list (d (n "ash") (r "^0.33") (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "132wqw2vy2n1rkyffjx5hgkryryhxaq39qaymkhkkfg0hsrlkaq5")))

(define-public crate-favilla-0.1.3 (c (n "favilla") (v "0.1.3") (d (list (d (n "ash") (r "^0.33") (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1q93ffrkd8whwad0mdmrv4msaxpa4zrph8aj1z9zhpp2imsa9c6g")))

(define-public crate-favilla-0.2.0 (c (n "favilla") (v "0.2.0") (d (list (d (n "ash") (r "^0.33") (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "19rzmqa9cikr9719zyjwixb4akm52z7bm33cb8fdwwv402z0kl0z")))

(define-public crate-favilla-0.2.1 (c (n "favilla") (v "0.2.1") (d (list (d (n "ash") (r "^0.33") (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1vmzgivsr58k02w1pm2x0lja1c7gdr5bfnr2i9g86d3l59vildr5")))

(define-public crate-favilla-0.3.0 (c (n "favilla") (v "0.3.0") (d (list (d (n "ash") (r "^0.37") (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1f8mm1r1y8lp1n1dk8bzampms02b7avb5fjkbqyv0jadgp4llpjk")))

