(define-module (crates-io fa vi favicon-rs) #:use-module (crates-io))

(define-public crate-favicon-rs-0.2.0 (c (n "favicon-rs") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(windows)") (k 1)))) (h "1pvf9danw4qaddqwxsl3rxnala6899ys0c8ag05vkja17igjnjlr")))

(define-public crate-favicon-rs-0.3.0 (c (n "favicon-rs") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(windows)") (k 1)))) (h "1i4skz0lh81pm5f24wzc4i2dnrd3m2mvhmgj75xx05bi5rpkkzjd")))

(define-public crate-favicon-rs-0.4.0 (c (n "favicon-rs") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(windows)") (k 1)))) (h "0q2jh1cnk60w0d8yg1dr68r87bfydgjzqr2ps38a6yrqjw3ycj4d")))

(define-public crate-favicon-rs-0.5.0 (c (n "favicon-rs") (v "0.5.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(windows)") (k 1)))) (h "1fkj4prlwjdzfvy09zpb0s4mnnyfa3y7fshi2az27a8wy9bd3l5q")))

