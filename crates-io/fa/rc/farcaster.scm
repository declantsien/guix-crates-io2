(define-module (crates-io fa rc farcaster) #:use-module (crates-io))

(define-public crate-farcaster-0.1.0 (c (n "farcaster") (v "0.1.0") (h "12lq716rzjiqcqqybfk0v5bmg2aqhyxwsfn6ax43bh86vpp6g1p1") (y #t)))

(define-public crate-farcaster-0.2.0 (c (n "farcaster") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "16qxcvbqagr269j1x47nlxwkky4hx88j8km6i5xvn2vilzhjkjhj")))

(define-public crate-farcaster-0.2.1 (c (n "farcaster") (v "0.2.1") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "thiserror-impl") (r "^1.0.38") (d #t) (k 0)))) (h "0zi89vzxzjy3rlhavpjqy1awyxnqyx74s229xax81wli7w0l7llq")))

