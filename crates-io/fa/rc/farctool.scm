(define-module (crates-io fa rc farctool) #:use-module (crates-io))

(define-public crate-farctool-1.0.0 (c (n "farctool") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pmd_farc") (r "^0.1.0") (d #t) (k 0)))) (h "1i5qw8yxfpz33cqz339bwax4nzqgwrm4fas6pqq5cahvrw6vn9c0")))

(define-public crate-farctool-1.1.0 (c (n "farctool") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pmd_farc") (r "^1.0.0") (d #t) (k 0)))) (h "1dyfcnm7af9m2zqgrz77w3sw8w1kb01gn73f6mi5awmgg2sfvfqg")))

