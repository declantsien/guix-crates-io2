(define-module (crates-io fa nt fantoccini-stable) #:use-module (crates-io))

(define-public crate-fantoccini-stable-0.7.2 (c (n "fantoccini-stable") (v "0.7.2") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.0") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)) (d (n "url") (r "^1.5.0") (d #t) (k 0)) (d (n "webdriver") (r "^0.31.0") (d #t) (k 0)))) (h "1vnak03ahh6rrg1j2f4569l1m7kzlxq72nrgvd0pjhd506rb6518")))

