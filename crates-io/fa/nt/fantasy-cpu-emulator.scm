(define-module (crates-io fa nt fantasy-cpu-emulator) #:use-module (crates-io))

(define-public crate-fantasy-cpu-emulator-0.1.0 (c (n "fantasy-cpu-emulator") (v "0.1.0") (d (list (d (n "fantasy-cpu-emulator-macros") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "0cwbx8azrzhrv33ci24idcmfnlnm8pnqyn9rhy5nmnsa1qd4wqwb")))

(define-public crate-fantasy-cpu-emulator-0.1.1 (c (n "fantasy-cpu-emulator") (v "0.1.1") (d (list (d (n "fantasy-cpu-emulator-macros") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "1qck8scid6gpdjwg3z1f3lsraxyskjfacv8b3sa1jham3kxxayr4")))

