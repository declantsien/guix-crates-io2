(define-module (crates-io fa nt fantasy-util) #:use-module (crates-io))

(define-public crate-fantasy-util-0.1.0 (c (n "fantasy-util") (v "0.1.0") (h "12vzi531j3026mqn158n063jm5i34zwa1czxwmn16ni16sdxcc64")))

(define-public crate-fantasy-util-0.1.1 (c (n "fantasy-util") (v "0.1.1") (h "0bl4jll11x86s0yx9a31s9sgc79mgzi4d7hdl00701cl8374asx4")))

(define-public crate-fantasy-util-0.1.2 (c (n "fantasy-util") (v "0.1.2") (h "14qpkmy72gaizv2n8xp4bl55mhy6wr2pa8ym18jxi5b8cc2icigz")))

(define-public crate-fantasy-util-0.1.3 (c (n "fantasy-util") (v "0.1.3") (h "1bfqq5izzg0k0fm8wss4gsjxh9svmn3irdbfqixqnc3vfs968zib")))

(define-public crate-fantasy-util-0.1.4 (c (n "fantasy-util") (v "0.1.4") (h "1mdvv1wygj9jx9hv537xc53xggn5jgsy662hrgqlg0nskl8lc7dc")))

(define-public crate-fantasy-util-0.1.5 (c (n "fantasy-util") (v "0.1.5") (h "0azxvfk1nsyjj56dg9f2vp61fsagn07232ps8h2rxakgxc2rbw1d")))

(define-public crate-fantasy-util-0.1.6 (c (n "fantasy-util") (v "0.1.6") (h "0hagblfyrh2nb7ys4sv4h5xladvnv3mbrqd8mjn3xh0r896g3x3d")))

(define-public crate-fantasy-util-0.1.8 (c (n "fantasy-util") (v "0.1.8") (h "1fjjzs45fbx0yhicm9n319adkiqp7ww1n91xcvzx32gim9h0qwav")))

