(define-module (crates-io fa nt fanta-cli) #:use-module (crates-io))

(define-public crate-fanta-cli-0.1.0 (c (n "fanta-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (f (quote ("pattern"))) (d #t) (k 0)))) (h "0jfsvfyr58cj0zjs5qakvhw18g6spmg2r6rff8w2b9vxj11an37y")))

(define-public crate-fanta-cli-0.1.1 (c (n "fanta-cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (f (quote ("pattern"))) (d #t) (k 0)))) (h "0ssjc5bk8ag5lppzsmj36fgna141bgi9n31lr1p4z1q38wnykl4q")))

