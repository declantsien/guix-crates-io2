(define-module (crates-io fa nt fantasy-cpu-emulator-macros) #:use-module (crates-io))

(define-public crate-fantasy-cpu-emulator-macros-0.1.0 (c (n "fantasy-cpu-emulator-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "129xc7q7agl67y0bdb6q3c3l0ln8ap2nxd34wzp6rlh0kx8gqpmg")))

(define-public crate-fantasy-cpu-emulator-macros-0.2.0 (c (n "fantasy-cpu-emulator-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "15640pinybgc28wh98fi1lraq26v3k5q17xxgbbwrzr2nfcwx2k7")))

