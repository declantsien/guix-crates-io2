(define-module (crates-io fa ns fansi) #:use-module (crates-io))

(define-public crate-fansi-0.1.0 (c (n "fansi") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.4") (f (quote ("consoleapi" "errhandlingapi" "fileapi" "handleapi" "processenv"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1xv55xrvixgnggmlwr7q82xl68ryjq9m3pjs0pwp71gjixp0njqz")))

