(define-module (crates-io fa ls falsefox) #:use-module (crates-io))

(define-public crate-falsefox-0.1.0 (c (n "falsefox") (v "0.1.0") (h "00l4cbmrlal1ibzl522an7pn2srwbzc0prrqxgl3r88ngj4cln7l")))

(define-public crate-falsefox-0.1.1 (c (n "falsefox") (v "0.1.1") (h "0d22gky7yiwk1ndlss2mr8312f2ym3qqrrz47vvkaviszq06k419")))

(define-public crate-falsefox-0.1.2 (c (n "falsefox") (v "0.1.2") (h "1g8yxjrn0ipp1sqwih1zhcg0hvhyk9836vgavm3wiy2jziya8pda")))

(define-public crate-falsefox-0.1.3 (c (n "falsefox") (v "0.1.3") (h "09h5a1k5zmzqhkbkdj6hdy4xqzn3jm0gnvym7w60xd7v77khzxfl")))

(define-public crate-falsefox-0.1.4 (c (n "falsefox") (v "0.1.4") (h "0afmms11a85g1xl83w7nnz5xx7ry1zp3f6y0l7p9s4lxcg2g481l")))

