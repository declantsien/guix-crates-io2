(define-module (crates-io fa ls falsepng) #:use-module (crates-io))

(define-public crate-falsepng-0.1.0 (c (n "falsepng") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "10lhzwsa921k69rs4zw1ynnbbyf96rncsmgavm8mck9hfi8a7m7y")))

