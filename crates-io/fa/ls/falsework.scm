(define-module (crates-io fa ls falsework) #:use-module (crates-io))

(define-public crate-falsework-0.0.1-dev (c (n "falsework") (v "0.0.1-dev") (h "0vc9wn2yf1a8pc6ys3i5qii01vmkgl0zs0znn5fhv4z8jmv8xyrb")))

(define-public crate-falsework-0.1.0 (c (n "falsework") (v "0.1.0") (h "1fnq8dgh9har36vy6fi2811g9jpriggakr7m1ip1m8z6j46681kf")))

(define-public crate-falsework-0.1.1 (c (n "falsework") (v "0.1.1") (h "0crn10h46n31b7960fc570ksians9k53csp15642qh9bpjxxxq2p")))

(define-public crate-falsework-0.1.2 (c (n "falsework") (v "0.1.2") (h "1gsijywyncizfg857sg5m3g0ihfjawym94h9wkahxi3bk540vpz1")))

(define-public crate-falsework-0.1.3 (c (n "falsework") (v "0.1.3") (h "11lr33dzzbif0vpa7gz3lrsdn52y2vbap1sfjlvpx5a222c3r577")))

