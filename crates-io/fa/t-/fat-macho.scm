(define-module (crates-io fa t- fat-macho) #:use-module (crates-io))

(define-public crate-fat-macho-0.1.0 (c (n "fat-macho") (v "0.1.0") (d (list (d (n "goblin") (r "^0.3.1") (d #t) (k 0)))) (h "1k290226q66a5qj50nx0j6idq61fypkmgd9a6q1y6x2rkip9jjwa")))

(define-public crate-fat-macho-0.1.1 (c (n "fat-macho") (v "0.1.1") (d (list (d (n "goblin") (r "^0.3.1") (d #t) (k 0)))) (h "116642730wzz48gb4dxir7m77xyy3ax4s9cf71xhg8yidfc70624")))

(define-public crate-fat-macho-0.1.2 (c (n "fat-macho") (v "0.1.2") (d (list (d (n "goblin") (r "^0.3.1") (d #t) (k 0)))) (h "0v1qdn9c23rlgy3qfhv196rjysi4hjjlncxrmflzb7n7mc18qan2")))

(define-public crate-fat-macho-0.2.0 (c (n "fat-macho") (v "0.2.0") (d (list (d (n "goblin") (r "^0.3.1") (d #t) (k 0)))) (h "1v20w6jk5vr7jj61f386g8ii5rxb389cpn7xhrdmkh4ql995sxpj")))

(define-public crate-fat-macho-0.3.0 (c (n "fat-macho") (v "0.3.0") (d (list (d (n "goblin") (r "^0.3") (d #t) (k 0)))) (h "0z2zsp1m69jaardrdsx7hml3f3pfmzq17ajlq7g99mdii0dxdrk0")))

(define-public crate-fat-macho-0.4.0 (c (n "fat-macho") (v "0.4.0") (d (list (d (n "goblin") (r "^0.3") (d #t) (k 0)))) (h "1ksac1g4ghgrvlj0g6l40nim95pps2yj7iyjyysz62aacjwvzc1p")))

(define-public crate-fat-macho-0.4.1 (c (n "fat-macho") (v "0.4.1") (d (list (d (n "goblin") (r "^0.3") (d #t) (k 0)))) (h "0x8439m6np1hab6ig6906y6x5fr2jlm5p1g6rwm6w4nygsvrdfg9")))

(define-public crate-fat-macho-0.4.2 (c (n "fat-macho") (v "0.4.2") (d (list (d (n "goblin") (r "^0.3") (d #t) (k 0)) (d (n "llvm-bitcode") (r "^0.1.1") (d #t) (k 0)))) (h "1dhr0fd43jwrkjwrd2gcpgr5zcyad7q4z67rjlm0583ssglg6hrm")))

(define-public crate-fat-macho-0.4.3 (c (n "fat-macho") (v "0.4.3") (d (list (d (n "goblin") (r "^0.4") (d #t) (k 0)) (d (n "llvm-bitcode") (r "^0.1.1") (d #t) (k 0)))) (h "1yi4lhrmzff60w82g1y6c1icccil0dvg2y50p990rn63ygs1xskw")))

(define-public crate-fat-macho-0.4.4 (c (n "fat-macho") (v "0.4.4") (d (list (d (n "goblin") (r "^0.4") (d #t) (k 0)) (d (n "llvm-bitcode") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "14bvgdg8dlvksjyx6v4p3y2qkn4z5jav9bj8drb602a7ghv3i8vj") (f (quote (("default" "bitcode") ("bitcode" "llvm-bitcode"))))))

(define-public crate-fat-macho-0.4.5 (c (n "fat-macho") (v "0.4.5") (d (list (d (n "goblin") (r "^0.5.1") (d #t) (k 0)) (d (n "llvm-bitcode") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1w5hd1ma3cpai2kcjrg1jh0ksbz23cy8bqlbmis1fwcjraw1wgls") (f (quote (("default" "bitcode") ("bitcode" "llvm-bitcode"))))))

(define-public crate-fat-macho-0.4.6 (c (n "fat-macho") (v "0.4.6") (d (list (d (n "goblin") (r "^0.6.0") (d #t) (k 0)) (d (n "llvm-bitcode") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0jfib3z5vzs45f6lfm2lvw8rq17g01310n3a5fsc4i5rl8qp3w37") (f (quote (("default" "bitcode") ("bitcode" "llvm-bitcode"))))))

(define-public crate-fat-macho-0.4.7 (c (n "fat-macho") (v "0.4.7") (d (list (d (n "goblin") (r "^0.7.1") (d #t) (k 0)) (d (n "llvm-bitcode") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0ywr3xqi884i12d5mfidbma1hrd4rxj9f8jw7p4bignagmy13yk3") (f (quote (("default" "bitcode") ("bitcode" "llvm-bitcode"))))))

(define-public crate-fat-macho-0.4.8 (c (n "fat-macho") (v "0.4.8") (d (list (d (n "goblin") (r "^0.8.0") (d #t) (k 0)) (d (n "llvm-bitcode") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1pqsjf13pdbhki2sdh70575hwqd18gm3vp8hpir3vl5djgrr6k0d") (f (quote (("default" "bitcode") ("bitcode" "llvm-bitcode"))))))

