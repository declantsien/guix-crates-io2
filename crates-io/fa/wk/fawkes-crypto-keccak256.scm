(define-module (crates-io fa wk fawkes-crypto-keccak256) #:use-module (crates-io))

(define-public crate-fawkes-crypto-keccak256-0.1.1 (c (n "fawkes-crypto-keccak256") (v "0.1.1") (d (list (d (n "fawkes-crypto") (r "^4.3.3") (f (quote ("rand_support"))) (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)))) (h "0j5x838jvrz5fmmc0cwvmid4y0h5a0wclwxvc33xsh451h7fz68p")))

