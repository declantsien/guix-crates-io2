(define-module (crates-io fa wk fawkes-crypto-zkbob-pairing_ce) #:use-module (crates-io))

(define-public crate-fawkes-crypto-zkbob-pairing_ce-0.19.0 (c (n "fawkes-crypto-zkbob-pairing_ce") (v "0.19.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0) (p "ff_ce")) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1bcrv27pkqd7qyqj7ghp6i41f8wb12hz3dbl7r0cr12rrqpy9cjc") (f (quote (("expose-arith") ("default"))))))

