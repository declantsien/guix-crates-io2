(define-module (crates-io fa wk fawkes-crypto-zkbob-keccak256) #:use-module (crates-io))

(define-public crate-fawkes-crypto-zkbob-keccak256-0.1.1 (c (n "fawkes-crypto-zkbob-keccak256") (v "0.1.1") (d (list (d (n "fawkes-crypto") (r "^4.5.0") (f (quote ("rand_support"))) (d #t) (k 0) (p "fawkes-crypto-zkbob")) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)))) (h "133ms92z6559jq15zdmxv5hrwf3mddwsk7j8iarx0lyqsm2vzy7g")))

