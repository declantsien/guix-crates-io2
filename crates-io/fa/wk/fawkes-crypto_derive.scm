(define-module (crates-io fa wk fawkes-crypto_derive) #:use-module (crates-io))

(define-public crate-fawkes-crypto_derive-0.1.1 (c (n "fawkes-crypto_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0mf68v3vqvlj8kld57sfr1n95pqsx7f1wv6v1vafmgxxzk43jyi5")))

(define-public crate-fawkes-crypto_derive-0.1.2 (c (n "fawkes-crypto_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0fxbghjqdfjsazfx7zalkilibjj1942rri26i6krh4lasfyigvnb")))

(define-public crate-fawkes-crypto_derive-3.0.1 (c (n "fawkes-crypto_derive") (v "3.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0h7p9rycswjgibyk23w69wmhj0wd2xa4lk2v4j2nms069mr97wk9")))

(define-public crate-fawkes-crypto_derive-3.0.2 (c (n "fawkes-crypto_derive") (v "3.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0nw0sxih0iffp1gqja91zaw20ld8rphfmpwxh6gj0aqs47ncmnn4")))

(define-public crate-fawkes-crypto_derive-3.0.3 (c (n "fawkes-crypto_derive") (v "3.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1z8g54a4ki5vsbhf06hw871ycdrgh0fjyvfly87j1yf5b6pzghb9")))

(define-public crate-fawkes-crypto_derive-3.0.4 (c (n "fawkes-crypto_derive") (v "3.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "=1.0.41") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1c9xal71v6dwprxknhn3k8y60pbm2a26cp0lxyvp9sw80cf6fm7f")))

(define-public crate-fawkes-crypto_derive-3.1.1 (c (n "fawkes-crypto_derive") (v "3.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0zf278msi06kwfj66p20vdsmjay2gnchvyj9dc1vd3z4kn4fs9qf")))

(define-public crate-fawkes-crypto_derive-3.1.2 (c (n "fawkes-crypto_derive") (v "3.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zr6r4cwswmggk9kly0jz58mn4dcp9c49mlkykibjrinbr749586")))

(define-public crate-fawkes-crypto_derive-4.0.0 (c (n "fawkes-crypto_derive") (v "4.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jsd7z7r0d191hzzyhwizqbarimggi8mlym0572mv1z4cmxdb3ww")))

(define-public crate-fawkes-crypto_derive-4.0.1 (c (n "fawkes-crypto_derive") (v "4.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hk6w7i9ac6q8rzmfsyk3qshdaj3myr014z9cwawjx555z7lyi1d")))

(define-public crate-fawkes-crypto_derive-4.0.2 (c (n "fawkes-crypto_derive") (v "4.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1yv8yb9nha7k1n12sc2rc7p5gbzhmaq8l3hnfjr8l67a01myk57i")))

(define-public crate-fawkes-crypto_derive-4.3.0 (c (n "fawkes-crypto_derive") (v "4.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1p3dhghgd0md8nmk8bmmfbi7wdk3h8h3nszzi6s8lq176x3kdkqh")))

