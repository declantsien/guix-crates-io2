(define-module (crates-io fa bc fabcode) #:use-module (crates-io))

(define-public crate-fabcode-0.1.0 (c (n "fabcode") (v "0.1.0") (h "0r35zg32j8cmyy6pnh0fn0fssm1lwc9b6yjwipv7vhplp27vrfmp")))

(define-public crate-fabcode-0.2.0 (c (n "fabcode") (v "0.2.0") (h "14r7dnvxajbvvlkjbbnrs938aw8vp2fpahis98p5dx8jhkqwwqa5")))

