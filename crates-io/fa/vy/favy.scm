(define-module (crates-io fa vy favy) #:use-module (crates-io))

(define-public crate-favy-0.1.0-beta.0 (c (n "favy") (v "0.1.0-beta.0") (d (list (d (n "ikon") (r "^0.1.0-beta.1") (d #t) (k 0)))) (h "1q7v6gsw19z92nbwl82rjwr05n5syi64mwshxhfhkiphd93f6jf4") (y #t)))

(define-public crate-favy-0.1.0-beta.1 (c (n "favy") (v "0.1.0-beta.1") (d (list (d (n "ikon") (r "^0.1.0-beta.2") (d #t) (k 0)))) (h "0ldf3z18690wymyy755nx4b3h4vs4ic00ld1gnq3idr5fab7x8w6") (y #t)))

(define-public crate-favy-0.1.0-beta.2 (c (n "favy") (v "0.1.0-beta.2") (d (list (d (n "ikon") (r "^0.1.0-beta.3") (d #t) (k 0)))) (h "1jm0r0vzcq7g72rdvdjflx87bapz8ah0rj3lpngj6wrb13ayyi81") (y #t)))

(define-public crate-favy-0.1.0-beta.3 (c (n "favy") (v "0.1.0-beta.3") (d (list (d (n "ikon") (r "^0.1.0-beta.4") (d #t) (k 0)))) (h "0418c0775q93a341ch6yycw0sb5fsznk2yp161q0048pd5sfv8ll") (y #t)))

