(define-module (crates-io fa ji fajita) #:use-module (crates-io))

(define-public crate-fajita-0.0.1 (c (n "fajita") (v "0.0.1") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "im") (r "^15.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "rstar") (r "^0.8.2") (d #t) (k 0)))) (h "0rlw7ikk52ip447hrrz9fdppv7p4i6jpz2nxcglbw14jff69gnh2")))

