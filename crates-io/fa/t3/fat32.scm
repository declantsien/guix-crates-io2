(define-module (crates-io fa t3 fat32) #:use-module (crates-io))

(define-public crate-fat32-0.1.0 (c (n "fat32") (v "0.1.0") (h "1yfsk0dx609a17vz4lklww5yy8gcs7a89h9flxrbsrcvzpl43h6v") (f (quote (("default" "512") ("512") ("4086") ("2048") ("1024")))) (y #t)))

(define-public crate-fat32-0.1.1 (c (n "fat32") (v "0.1.1") (h "0zgvcrrx7ddxij6dmvcxwnl6f7qg3gvln99gm8q90i01z9fiq48j") (f (quote (("default" "512") ("512") ("4086") ("2048") ("1024")))) (y #t)))

(define-public crate-fat32-0.1.2 (c (n "fat32") (v "0.1.2") (h "1glym47kmq84sfh7kxd8xc40lwzjf1s3zhdfpva9j44mp5j2x19h") (f (quote (("default" "512") ("512") ("4086") ("2048") ("1024")))) (y #t)))

(define-public crate-fat32-0.1.3 (c (n "fat32") (v "0.1.3") (d (list (d (n "block_device") (r "^0.1") (d #t) (k 0)))) (h "0xj6c7mwhfhwxws46lzp02wdj7ig332xalskjrz69kb9hf6zny2m") (f (quote (("default" "512") ("512") ("4086") ("2048") ("1024")))) (y #t)))

(define-public crate-fat32-0.1.4 (c (n "fat32") (v "0.1.4") (d (list (d (n "block_device") (r "^0.1") (d #t) (k 0)))) (h "12j8snzjpi1r0r0wgzgav7ayq8z7ixilvw5ry17nxs0cjdba7b75") (f (quote (("default" "512") ("512") ("4086") ("2048") ("1024")))) (y #t)))

(define-public crate-fat32-0.1.5 (c (n "fat32") (v "0.1.5") (d (list (d (n "block_device") (r "=0.1.0") (d #t) (k 0)))) (h "0wfk1aaa15ywlzdzpqm58fhvq4v92y27205vqwclaby8agfwm93y") (f (quote (("default" "512") ("512") ("4086") ("2048") ("1024")))) (y #t)))

(define-public crate-fat32-0.2.0 (c (n "fat32") (v "0.2.0") (d (list (d (n "block_device") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "winioctl" "ioapiset"))) (d #t) (t "cfg(windows)") (k 2)))) (h "05rcf1s424fpqlxbilm81nmxp2qvhllk67adgp03f035vk4f9b84") (f (quote (("default" "512") ("512") ("4096") ("2048") ("1024"))))))

