(define-module (crates-io fa cc faccess) #:use-module (crates-io))

(define-public crate-faccess-0.1.0 (c (n "faccess") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "0zxl0d4lr246s4619py4vp6q4s15r1mw9g70q7y6p6ifrdizxdfk")))

(define-public crate-faccess-0.2.0 (c (n "faccess") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("accctrl" "aclapi" "handleapi" "impl-default" "minwindef" "processthreadsapi" "securitybaseapi" "winbase" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1mxd38ryyw6l69j8970l3njbi3zwcafzvbvxn50afb53g1rycfv8")))

(define-public crate-faccess-0.2.1 (c (n "faccess") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("accctrl" "aclapi" "handleapi" "impl-default" "minwindef" "processthreadsapi" "securitybaseapi" "winbase" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xjpsj3b68v0z8p0cyi70v45ssj7yhwgjdsdql5p45lncnv0ya95")))

(define-public crate-faccess-0.2.2 (c (n "faccess") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "~0.2.68") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("accctrl" "aclapi" "handleapi" "impl-default" "minwindef" "processthreadsapi" "securitybaseapi" "winbase" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "180pmk5sillvczdw0irva9wi3r0imslnl6kmiy7m79d0vih2kka1")))

(define-public crate-faccess-0.2.3 (c (n "faccess") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "~0.2.68") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("accctrl" "aclapi" "handleapi" "impl-default" "minwindef" "processthreadsapi" "securitybaseapi" "winbase" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0z7015mrf6zis91cz27f26aawk9dp406kxzlvpfn7xxsg5b1ffg0")))

(define-public crate-faccess-0.2.4 (c (n "faccess") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "~0.2.68") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("accctrl" "aclapi" "handleapi" "impl-default" "minwindef" "processthreadsapi" "securitybaseapi" "winbase" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1m967rz9qknzbqh7l8hga91s32y3p041mbk8w81skmh2b116dbjr")))

