(define-module (crates-io fa ex faex) #:use-module (crates-io))

(define-public crate-faex-0.1.0 (c (n "faex") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.182") (f (quote ("derive"))) (d #t) (k 0)))) (h "058177q3219bln08cz359dsxvqim4pmkcs1sv4dqky63cscsyvjc")))

(define-public crate-faex-0.1.1 (c (n "faex") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 2)) (d (n "serde") (r "^1.0.182") (f (quote ("derive"))) (d #t) (k 0)))) (h "178xnbpl7bj8zjkww6lqlrzkmkf2ynbk44ww0z080r12f00404qj")))

(define-public crate-faex-0.1.2 (c (n "faex") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.182") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xnbl3vivd9mgmq3yb7sc271sm4mjrz4k7zxf8vkxgkhxspwfqj8")))

