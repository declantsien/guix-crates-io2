(define-module (crates-io fa lc falcon-cli) #:use-module (crates-io))

(define-public crate-falcon-cli-0.1.0 (c (n "falcon-cli") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^2.2.2") (d #t) (k 0)))) (h "08d8xmcisw4mjg3a7bcm81xxfmspav34v07sk46j51i5bm6qk92j")))

(define-public crate-falcon-cli-0.1.1 (c (n "falcon-cli") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^2.2.2") (d #t) (k 0)))) (h "1v84smpm82yh7vz7w39f148xqmjz44lyzz25nmv8jsf24s716pwf")))

(define-public crate-falcon-cli-0.1.2 (c (n "falcon-cli") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^2.2.2") (d #t) (k 0)))) (h "1rcn1sh81bq47q2qj4yi8md6aym08fi0yfyg2jpicd69y0n92rfc")))

(define-public crate-falcon-cli-0.1.3 (c (n "falcon-cli") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^2.2.2") (d #t) (k 0)))) (h "0syl7860hw9pfdzy5clvh4jlilgrj13y4c86d18rnkkqfmpq5a53")))

(define-public crate-falcon-cli-0.1.4 (c (n "falcon-cli") (v "0.1.4") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^2.2.2") (d #t) (k 0)))) (h "01nfxhh9z2ljq7kgc7ryzjfrfl4zhcxscvbb7s7h4y6l26rqbcp7")))

(define-public crate-falcon-cli-0.1.5 (c (n "falcon-cli") (v "0.1.5") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^2.2.2") (d #t) (k 0)))) (h "01nssmmn6x8avg6zn1cz8c2hf83b9a86dj9rlfpxjh9r988h4znp")))

(define-public crate-falcon-cli-0.1.6 (c (n "falcon-cli") (v "0.1.6") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^2.2.2") (d #t) (k 0)))) (h "1g4xkldrx1diab9bx80n2a0shzp3lvxip2r020z8vksanc18745m")))

(define-public crate-falcon-cli-0.1.7 (c (n "falcon-cli") (v "0.1.7") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^2.2.2") (d #t) (k 0)))) (h "01qp02imjg9b1sxd0k7cr8q8wjw6d3kqk223c71nnlslka1la865")))

(define-public crate-falcon-cli-0.1.8 (c (n "falcon-cli") (v "0.1.8") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^2.2.2") (d #t) (k 0)))) (h "1jgwjgp06zqhx436xvlli7ckmv7wlvg9sbhlbac3vfr80iaxri2q")))

