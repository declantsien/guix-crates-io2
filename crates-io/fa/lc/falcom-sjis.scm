(define-module (crates-io fa lc falcom-sjis) #:use-module (crates-io))

(define-public crate-falcom-sjis-0.1.0 (c (n "falcom-sjis") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "gospel") (r "^0.1.1") (d #t) (k 1)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)))) (h "0nnlxpmhqkxmlvmrqdw1xaqppig2dgap8b5qm69y38z0xi1f8rdq")))

(define-public crate-falcom-sjis-0.1.1 (c (n "falcom-sjis") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "gospel") (r "^0.1.1") (d #t) (k 1)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)))) (h "1kly4r4y5cqgflngfh5h4gi0hzrgpxr65w46dh3qs2gfp36slidq")))

(define-public crate-falcom-sjis-0.1.2 (c (n "falcom-sjis") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "gospel") (r "^0.1.1") (d #t) (k 1)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)))) (h "0m4xik8pjlx3190jm3swzd8j0dqqv5f5nlnyijg85amz9k1zvszz")))

