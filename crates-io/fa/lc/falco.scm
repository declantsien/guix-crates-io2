(define-module (crates-io fa lc falco) #:use-module (crates-io))

(define-public crate-falco-0.0.0 (c (n "falco") (v "0.0.0") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.7") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "~2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "16vypm52v1q51wnc2ksbcd23ry4kvr1s7z8r65mslvcjy0adsc68")))

