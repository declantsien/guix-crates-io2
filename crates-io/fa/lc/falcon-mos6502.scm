(define-module (crates-io fa lc falcon-mos6502) #:use-module (crates-io))

(define-public crate-falcon-mos6502-0.0.1 (c (n "falcon-mos6502") (v "0.0.1") (h "1pjsgrmvgpnpb1295wm16xkdbpqdjz5i2fgbqg8j8yqh57zrnvhm")))

(define-public crate-falcon-mos6502-0.0.2 (c (n "falcon-mos6502") (v "0.0.2") (h "1l3w01acg54rad8w5p7rc2ipngsr5q6qn7gv6hyp829g2dz89569")))

