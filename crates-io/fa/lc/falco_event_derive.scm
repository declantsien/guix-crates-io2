(define-module (crates-io fa lc falco_event_derive) #:use-module (crates-io))

(define-public crate-falco_event_derive-0.1.0 (c (n "falco_event_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0bv6xsbzwin6l7gsqz1jcsnn36shh8jljcaamy7zj5lj3f64rkr2")))

(define-public crate-falco_event_derive-0.1.1 (c (n "falco_event_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0pbmagnjfd23gbjqgkbih73lxv1acfdfx9ck9nzq4v4c9rd56m80")))

