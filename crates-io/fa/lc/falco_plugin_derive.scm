(define-module (crates-io fa lc falco_plugin_derive) #:use-module (crates-io))

(define-public crate-falco_plugin_derive-0.1.0 (c (n "falco_plugin_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1m1kzz0cbbjjz58fbxwi893j6fxs7qrpfc0amria6n1apgr8is93")))

(define-public crate-falco_plugin_derive-0.1.1 (c (n "falco_plugin_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1idvc14a894y9drqjl3klksh1ksgb6p981zqcv1hsppv22jdmsax")))

