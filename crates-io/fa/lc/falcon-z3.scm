(define-module (crates-io fa lc falcon-z3) #:use-module (crates-io))

(define-public crate-falcon-z3-0.0.1 (c (n "falcon-z3") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.33.0") (d #t) (k 1)))) (h "1008l2fb9n2jdrw3drlvpqd3s3pzw4hi66yrr08k0yjafs383dg5")))

(define-public crate-falcon-z3-0.3.1 (c (n "falcon-z3") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.35.0") (d #t) (k 1)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "falcon") (r "^0.3.1") (f (quote ("thread_safe"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "1y9nhxj5mfg0xlsmxh11807myzwhrlp5iypjsr2r3bqmr4fdfayv")))

(define-public crate-falcon-z3-0.4.0 (c (n "falcon-z3") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.35.0") (d #t) (k 1)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "falcon") (r "^0.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "03kcsbxx6xmfi2pww7s6b9lrxpaq6h81lcd5dq9h3glgrab4irs7")))

(define-public crate-falcon-z3-0.4.1 (c (n "falcon-z3") (v "0.4.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "falcon") (r "^0.4.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "z3-sys") (r "^0.3") (d #t) (k 0)))) (h "1hwyv6zxdr6257mlgi66c28gvkf1hkflf2zgpckgaja698rq4ww8")))

(define-public crate-falcon-z3-0.4.2 (c (n "falcon-z3") (v "0.4.2") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "falcon") (r "^0.4.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "z3-sys") (r "^0.3") (d #t) (k 0)))) (h "01aiihyhggfnkyp69fbqlg9wfwrxwk7zm78mj5q22gc57d32vkp5")))

(define-public crate-falcon-z3-0.4.3 (c (n "falcon-z3") (v "0.4.3") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "falcon") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "z3-sys") (r "^0.3") (d #t) (k 0)))) (h "1k8kkwr220hdwgi3mdibd06y4das3vs4rqjy1yy4wb4ibi3i3wh5")))

(define-public crate-falcon-z3-0.4.4 (c (n "falcon-z3") (v "0.4.4") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "falcon") (r "^0.4.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "z3-sys") (r "^0.3") (d #t) (k 0)))) (h "1lkwx6bx3qg9zawlc5k0a2dm91rcixqankk5y9dpjny1pii9arkp")))

(define-public crate-falcon-z3-0.4.5 (c (n "falcon-z3") (v "0.4.5") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "falcon") (r "^0.4.9") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "z3-sys") (r "^0.5") (d #t) (k 0)))) (h "1jdmyd9ikgbfgs4a40aq23ck5pjpxnfqjwadjfy5ai80ij611cgn") (f (quote (("capstone4" "falcon/capstone4"))))))

(define-public crate-falcon-z3-0.4.10 (c (n "falcon-z3") (v "0.4.10") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "falcon") (r "^0.4.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "z3-sys") (r "^0.5") (d #t) (k 0)))) (h "0h5kq361zh88rha4lpi937hzr8ljvrkx8hriwxwh66wcm0r308ip") (f (quote (("capstone4" "falcon/capstone4"))))))

(define-public crate-falcon-z3-0.4.11 (c (n "falcon-z3") (v "0.4.11") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "falcon") (r "^0.4.11") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "z3-sys") (r "^0.5") (d #t) (k 0)))) (h "17fr4lzk75bx4zk5m3kdcsvh9qr940xbi1lxs7ymwx51mz87mhz7") (f (quote (("capstone4" "falcon/capstone4"))))))

(define-public crate-falcon-z3-0.4.12 (c (n "falcon-z3") (v "0.4.12") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "falcon") (r "^0.4.12") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "z3-sys") (r "^0.5") (d #t) (k 0)))) (h "0vb7ky0vcivhid3yp1yf5gp9qcdnyq31r8a2fvv2ad2dzkag34c4") (f (quote (("capstone4" "falcon/capstone4"))))))

(define-public crate-falcon-z3-0.5.1 (c (n "falcon-z3") (v "0.5.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "falcon") (r "^0.5.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "z3-sys") (r "^0.6") (d #t) (k 0)))) (h "0ciyhb1knl9rx4l5wndjmx21jgnpgxh55ipmmp1gg6gl4asilqlh") (f (quote (("capstone4" "falcon/capstone4"))))))

(define-public crate-falcon-z3-0.5.2 (c (n "falcon-z3") (v "0.5.2") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "falcon") (r "^0.5.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "z3-sys") (r "^0.7") (d #t) (k 0)))) (h "1c6j6zchz3j3qmqzd3b6rbamjda6yachpm3zbjqkav2kfr5milp9") (f (quote (("capstone4" "falcon/capstone4"))))))

