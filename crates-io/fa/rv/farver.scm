(define-module (crates-io fa rv farver) #:use-module (crates-io))

(define-public crate-farver-3.1.0 (c (n "farver") (v "3.1.0") (d (list (d (n "bevy") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "bevy") (r "^0.8.1") (f (quote ("bevy_render"))) (d #t) (k 2)) (d (n "palette") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)))) (h "1saz88w005h1j2i7c1jbazz08ncfm9q0fdbkc5ijxcaak1frnpv4")))

