(define-module (crates-io fa rv farve) #:use-module (crates-io))

(define-public crate-farve-0.1.0 (c (n "farve") (v "0.1.0") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1qbnv5lmwfgxyrwxmjrbfx4dmfmjkk38cx7n3m2sa8ra65n5yg20")))

(define-public crate-farve-0.1.1 (c (n "farve") (v "0.1.1") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "09shcx7gm0yas8kn1zvbhc6pxsrij316jzmzn2bcl8wdkkqpabfh")))

(define-public crate-farve-0.1.2 (c (n "farve") (v "0.1.2") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0gz2cs3567zh2psmylv37ddv72kw7ajyx9jrg5bnldsb3i2k7mac")))

(define-public crate-farve-0.1.3 (c (n "farve") (v "0.1.3") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "1cxxmi82s5g1ssdrp4xryx4q9r9nx90s755s32qflnikh8mll548")))

