(define-module (crates-io fa ce facebook-webhook-warp) #:use-module (crates-io))

(define-public crate-facebook-webhook-warp-0.1.0 (c (n "facebook-webhook-warp") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (k 0)) (d (n "facebook-webhook") (r "=0.1.0") (d #t) (k 0)) (d (n "warp") (r "^0.3") (k 0)))) (h "1w1wm905rvn4kqqz2dir0666hkdafiwnhf2zb0qvfan59iybr8kw")))

(define-public crate-facebook-webhook-warp-0.1.1 (c (n "facebook-webhook-warp") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0") (k 0)) (d (n "facebook-webhook") (r "=0.1.1") (d #t) (k 0)) (d (n "warp") (r "^0.3") (k 0)))) (h "06z69xpk5jsawb5q3p6p8is887k2ig33wy43sjybmc1qvjwk43za")))

(define-public crate-facebook-webhook-warp-0.1.2 (c (n "facebook-webhook-warp") (v "0.1.2") (d (list (d (n "bytes") (r "^1.0") (k 0)) (d (n "facebook-webhook") (r "^0.1") (d #t) (k 0)) (d (n "warp") (r "^0.3") (k 0)))) (h "07ip7pqjpxq1gqvhcz2s5krn2z6dbv62y0v17zddx8gvky0ywxi8")))

(define-public crate-facebook-webhook-warp-0.1.3 (c (n "facebook-webhook-warp") (v "0.1.3") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "facebook-webhook") (r "^0.1") (d #t) (k 0)) (d (n "warp") (r "^0.3") (k 0)))) (h "12a4f2abbpfks3rf20sc6xkq97m5vhk3v13pcr2avmpck6gwr3nh")))

