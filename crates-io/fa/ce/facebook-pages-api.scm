(define-module (crates-io fa ce facebook-pages-api) #:use-module (crates-io))

(define-public crate-facebook-pages-api-0.0.0 (c (n "facebook-pages-api") (v "0.0.0") (h "1khljk5pdasvbx95izdr4rvyzj630wam4915gijq5d6551z7kyvp")))

(define-public crate-facebook-pages-api-0.1.0 (c (n "facebook-pages-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "facebook-graph-api-object-error") (r "^0.1") (d #t) (k 0)) (d (n "facebook-graph-api-object-paging") (r "^0.1") (d #t) (k 0)) (d (n "http-api-client-endpoint") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-aux") (r "^4") (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (k 0)))) (h "0jzfzxdfxf58irx55z0bm7nngnbwz421fzhd99awmxmpjlckr9wb")))

(define-public crate-facebook-pages-api-0.2.0 (c (n "facebook-pages-api") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "facebook-graph-api-object-error") (r "^0.2") (d #t) (k 0)) (d (n "facebook-graph-api-object-paging") (r "^0.1") (d #t) (k 0)) (d (n "http-api-client-endpoint") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-aux") (r "^4") (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (k 0)))) (h "16wkpwk4bp09qy05kq1ff6d4d6byi7xb273p2gzp50qjin94mjc7")))

