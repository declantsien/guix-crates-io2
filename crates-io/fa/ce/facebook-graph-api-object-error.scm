(define-module (crates-io fa ce facebook-graph-api-object-error) #:use-module (crates-io))

(define-public crate-facebook-graph-api-object-error-0.1.0 (c (n "facebook-graph-api-object-error") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)))) (h "08rl77yccma5a0j7b48dk22l4wvrabxdpa2a4in9g0np1f7190yy")))

(define-public crate-facebook-graph-api-object-error-0.1.1 (c (n "facebook-graph-api-object-error") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)))) (h "15ac81h83cf8lyv192x2vr9n6m4pg8rzmzqn27nv9bmikp574rbp")))

(define-public crate-facebook-graph-api-object-error-0.2.0 (c (n "facebook-graph-api-object-error") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)))) (h "0bijq3ynbj37sv52h58fas2g20fg7skmnjdl3ia0jznkmjsldhkr")))

