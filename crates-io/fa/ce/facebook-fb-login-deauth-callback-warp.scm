(define-module (crates-io fa ce facebook-fb-login-deauth-callback-warp) #:use-module (crates-io))

(define-public crate-facebook-fb-login-deauth-callback-warp-0.1.0 (c (n "facebook-fb-login-deauth-callback-warp") (v "0.1.0") (d (list (d (n "facebook-fb-login-deauth-callback") (r "=0.1.0") (d #t) (k 0)) (d (n "warp") (r "^0.3") (k 0)))) (h "03qkz83jmnjslafd85vbg8f2q1lqz432imww78n1njjvl2x28mmb")))

(define-public crate-facebook-fb-login-deauth-callback-warp-0.1.1 (c (n "facebook-fb-login-deauth-callback-warp") (v "0.1.1") (d (list (d (n "facebook-fb-login-deauth-callback") (r "^0.1") (d #t) (k 0)) (d (n "warp") (r "^0.3") (k 0)))) (h "1h0p3rshgcg4685y0vk0j3x8036jbp309abfxi8ipwpd4f9ah2r3")))

(define-public crate-facebook-fb-login-deauth-callback-warp-0.1.2 (c (n "facebook-fb-login-deauth-callback-warp") (v "0.1.2") (d (list (d (n "facebook-fb-login-deauth-callback") (r "^0.1") (d #t) (k 0)) (d (n "warp") (r "^0.3") (k 0)))) (h "17pwnx4gsfm1cir5jj83cllc5zhiz82pa3xfir5vl82n0kp5xdi1")))

(define-public crate-facebook-fb-login-deauth-callback-warp-0.2.0 (c (n "facebook-fb-login-deauth-callback-warp") (v "0.2.0") (d (list (d (n "facebook-fb-login-deauth-callback") (r "^0.2") (d #t) (k 0)) (d (n "warp") (r "^0.3") (k 0)))) (h "1nicg9y2z83xj91mig2hvd1md5bsk5a2bcp78csp9ha2mlnnc022")))

