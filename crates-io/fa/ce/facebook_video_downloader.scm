(define-module (crates-io fa ce facebook_video_downloader) #:use-module (crates-io))

(define-public crate-facebook_video_downloader-0.1.0 (c (n "facebook_video_downloader") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "1xpkkb76m4ls398nnqp4bxmvxfchqnx5d33l68a2r6a304gvsixl")))

(define-public crate-facebook_video_downloader-0.1.1 (c (n "facebook_video_downloader") (v "0.1.1") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "159allx5q59qw29x790wn01pxa7bw1gd54721s12izimpfglk5di")))

(define-public crate-facebook_video_downloader-0.1.2 (c (n "facebook_video_downloader") (v "0.1.2") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "0cs0323mym9133ip88bd4f8rcrg6wd9v1jarz4f2kk2ll0c9nw3m")))

