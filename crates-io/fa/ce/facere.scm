(define-module (crates-io fa ce facere) #:use-module (crates-io))

(define-public crate-facere-0.1.0 (c (n "facere") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c72y05fs6my90534g6xn6vb21mwpsh4r9xh4h9b2am4hrywdh2j")))

(define-public crate-facere-0.1.3 (c (n "facere") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p99zjrsrjkw7bgqnxfhapn68vc3ldc96lbq4vh2w1fwkk63gld1")))

(define-public crate-facere-0.1.5 (c (n "facere") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p3iaq8g3cpwq2ijr8gir0jn6sxpw80v16axki9lsb2zmg96g6a3")))

(define-public crate-facere-0.1.8 (c (n "facere") (v "0.1.8") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "19g00y8g6mqv900pp0bk2c17mx36v5g44d60dwyzspzvzszyaxxy")))

