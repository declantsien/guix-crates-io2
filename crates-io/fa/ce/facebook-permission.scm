(define-module (crates-io fa ce facebook-permission) #:use-module (crates-io))

(define-public crate-facebook-permission-0.1.0 (c (n "facebook-permission") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.1.1") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1iv5y7fci7c1cy1phqgrv0q2rlcb41nhcgcf7b3zmk5i2phz34rj")))

(define-public crate-facebook-permission-0.1.1 (c (n "facebook-permission") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.1.4") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ranvvl7ipizq5pv2sr2126p26v7vs22pj63sfiyl7kyds5il4f0")))

(define-public crate-facebook-permission-0.1.2 (c (n "facebook-permission") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kykkj6i3glhib19wx42w6hnk1farsmw6h0wxgz88gnm01xrchhq")))

(define-public crate-facebook-permission-0.1.3 (c (n "facebook-permission") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1i7yq2hr7yxnrim5a47gd5xmqiwiiqgag3jy8p7dpn7gzmiyagjm")))

(define-public crate-facebook-permission-0.1.4 (c (n "facebook-permission") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "02425m2fsdfjn2127qpv43yyzf7kq6jq37kvjj30xx8ihnx2dvx5")))

(define-public crate-facebook-permission-0.1.5 (c (n "facebook-permission") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ng89849g6i6zr5scq1rmv93yvqna197s4hh20s52fmh330zvpl2")))

(define-public crate-facebook-permission-0.1.6 (c (n "facebook-permission") (v "0.1.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0v944m7gh31cbrmhghg590ni86bgahpca0wimfy0xmrb1jqgzl9x")))

(define-public crate-facebook-permission-0.1.7 (c (n "facebook-permission") (v "0.1.7") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0g7p1hbxx7zx75qylb5akmqky2ngvdyxpjxikkxifyahn3xbfcg2")))

(define-public crate-facebook-permission-0.2.0 (c (n "facebook-permission") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.3") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "19p7s6w58h671albsjanahyn6r8gycr7ckapnxc2wc8g9c9y1y27")))

