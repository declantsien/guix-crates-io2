(define-module (crates-io fa ce face_detection_mtcnn) #:use-module (crates-io))

(define-public crate-face_detection_mtcnn-0.1.0 (c (n "face_detection_mtcnn") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tensorflow") (r "^0.16.0") (d #t) (k 0)))) (h "0z820lazhswa4365cnvzlq70v7x0il2ry0pxc3mzkpa180qyjpm4")))

