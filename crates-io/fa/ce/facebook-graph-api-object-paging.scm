(define-module (crates-io fa ce facebook-graph-api-object-paging) #:use-module (crates-io))

(define-public crate-facebook-graph-api-object-paging-0.1.0 (c (n "facebook-graph-api-object-paging") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1l9as07bszc0v4hs3xwq1nlz47gc06wb1l9a1w8gzzz3jzqw6058")))

