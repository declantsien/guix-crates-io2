(define-module (crates-io fa ce facebook-fb-login-deauth-callback) #:use-module (crates-io))

(define-public crate-facebook-fb-login-deauth-callback-0.1.0 (c (n "facebook-fb-login-deauth-callback") (v "0.1.0") (d (list (d (n "facebook-signed-request") (r "=0.1.0") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1.0") (k 0)) (d (n "http") (r "^0.2") (k 0)))) (h "1xqxs37rx898x0q35g0w4p8fq9kkkpiv6hj9palmlk1jkxcm4hpd")))

(define-public crate-facebook-fb-login-deauth-callback-0.1.1 (c (n "facebook-fb-login-deauth-callback") (v "0.1.1") (d (list (d (n "facebook-signed-request") (r "^0.1") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1.0") (k 0)) (d (n "http") (r "^0.2") (k 0)))) (h "169lnw04z16varjkhc0wdppdj4a2h8p4miaksr41f6jpx5kp5c3g")))

(define-public crate-facebook-fb-login-deauth-callback-0.1.2 (c (n "facebook-fb-login-deauth-callback") (v "0.1.2") (d (list (d (n "facebook-signed-request") (r "^0.1") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1") (k 0)) (d (n "http") (r "^0.2") (k 0)))) (h "05fyk7x4xczryy44xarr4a424qhywhdar3n7q2h5fpb59fn2sihh")))

(define-public crate-facebook-fb-login-deauth-callback-0.2.0 (c (n "facebook-fb-login-deauth-callback") (v "0.2.0") (d (list (d (n "facebook-signed-request") (r "^0.2") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (k 0)))) (h "1x7m2brfk8hhx42010gjdar44l3j7ifzihph489k3sjy671fhwg1")))

