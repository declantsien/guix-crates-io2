(define-module (crates-io fa ce facebook-access-token) #:use-module (crates-io))

(define-public crate-facebook-access-token-0.1.0 (c (n "facebook-access-token") (v "0.1.0") (d (list (d (n "wrapping-macro") (r "^0.1") (d #t) (k 0)))) (h "0drswrc79ii2w645rvliqkd0viz2mm2jqzfqy4w9qsivx1qw3397")))

(define-public crate-facebook-access-token-0.1.1 (c (n "facebook-access-token") (v "0.1.1") (d (list (d (n "wrapping-macro") (r "^0.2") (d #t) (k 0)))) (h "16fvyqnb4i1cpg0zcci47hy623dmb29ysd4wf1ww7l1a4xi8y3gi") (y #t)))

(define-public crate-facebook-access-token-0.1.2 (c (n "facebook-access-token") (v "0.1.2") (d (list (d (n "wrapping-macro") (r "^0.2") (d #t) (k 0)))) (h "0cb1i6w3isf1lnrz3y8a1l8lcjqlk3z8lgm8884km21byb9v8m9y")))

