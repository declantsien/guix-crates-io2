(define-module (crates-io fa ce facebook-rs) #:use-module (crates-io))

(define-public crate-facebook-rs-0.1.0 (c (n "facebook-rs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0v4sp0k7fbxqyajg7k6ndh0cvi1w654lj4v97hnrzcm2f8ygdpp2")))

