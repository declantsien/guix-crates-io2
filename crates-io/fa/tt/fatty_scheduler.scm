(define-module (crates-io fa tt fatty_scheduler) #:use-module (crates-io))

(define-public crate-fatty_scheduler-0.1.1 (c (n "fatty_scheduler") (v "0.1.1") (h "1dmd5ddarihdhx61n066xypmvvjkl8w93v7h82g1w0803dzsi3a5")))

(define-public crate-fatty_scheduler-0.1.2 (c (n "fatty_scheduler") (v "0.1.2") (h "0x0v1w4sw9y74awvq37sx39w5z8qvii0s0kjmr13x8v3dra8ljzi")))

(define-public crate-fatty_scheduler-0.1.3 (c (n "fatty_scheduler") (v "0.1.3") (h "15hx73iayy5dfnafsykm5lv1snnlxkklg98kcqw33hgvkqyp5zcm")))

(define-public crate-fatty_scheduler-0.1.4 (c (n "fatty_scheduler") (v "0.1.4") (h "02cwy27cwxfv5xsmjv398hqlfyc911w2m9v2gmmxljbmwn6v92jb")))

