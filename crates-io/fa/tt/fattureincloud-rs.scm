(define-module (crates-io fa tt fattureincloud-rs) #:use-module (crates-io))

(define-public crate-fattureincloud-rs-2.0.29 (c (n "fattureincloud-rs") (v "2.0.29") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.3.0") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0jl96n9p4qrx2wz6a7pj6ql8cv5jpna98i4a50bijvq5rd7s1hq2")))

(define-public crate-fattureincloud-rs-2.0.32 (c (n "fattureincloud-rs") (v "2.0.32") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^3") (f (quote ("base64" "std" "macros"))) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1a2npc5xn71jaa54kgg4xw53qsr96s2rp4pg97af1yhl122yji8h")))

