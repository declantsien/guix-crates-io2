(define-module (crates-io fa tf fatfs-sys) #:use-module (crates-io))

(define-public crate-fatfs-sys-0.1.0 (c (n "fatfs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.48.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "059mw8zg6ks06pznirpzbvwmbv1ji5m13yfs8za7akqs61lifh32")))

(define-public crate-fatfs-sys-0.2.0 (c (n "fatfs-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.48.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xhvlkjpkxxbczyavg9f01v3iamr4jnq7h5aakxv54czc56mq34f")))

