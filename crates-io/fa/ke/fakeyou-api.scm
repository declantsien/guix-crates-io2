(define-module (crates-io fa ke fakeyou-api) #:use-module (crates-io))

(define-public crate-fakeyou-api-0.1.0 (c (n "fakeyou-api") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json" "tls"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jnhqx5dyw71k5wv717i9pyc5nak28izcsf2j92cxgq13gffpny6")))

(define-public crate-fakeyou-api-0.1.1 (c (n "fakeyou-api") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json" "tls"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "09z5z7qy0vj7l97vwyq719gi749858abp42rg0n7xg9plsqsfmp8")))

(define-public crate-fakeyou-api-0.1.2 (c (n "fakeyou-api") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json" "tls"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0sh3hvvhassszbw49xi81ypdcb6l5jzcsvz6818290rcpc9i4kmz")))

(define-public crate-fakeyou-api-0.1.3 (c (n "fakeyou-api") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json" "tls"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1403y4059qhqfrpv0aqiz9ln53x8dllv654lmxw35ywbg0wq2rcv")))

(define-public crate-fakeyou-api-0.2.0 (c (n "fakeyou-api") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "blocking" "default-tls" "native-tls"))) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "19rl9kyw6smdmp0r2yd3knircqy1zh5v8aawh3qviliscvsbpjrr")))

(define-public crate-fakeyou-api-0.2.1 (c (n "fakeyou-api") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "blocking" "default-tls" "native-tls"))) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "06b4v6nz67i1fjzgkcwlimmmp87fxbkzfcm31nxi18zqrfwf91bg")))

(define-public crate-fakeyou-api-0.3.0 (c (n "fakeyou-api") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "blocking" "default-tls" "native-tls"))) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "07ls0r9x6v0lf5b1facdjlf5b325qiwcdqdzj337lnq8qb6xkyi8")))

