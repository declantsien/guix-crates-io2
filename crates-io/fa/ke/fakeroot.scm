(define-module (crates-io fa ke fakeroot) #:use-module (crates-io))

(define-public crate-fakeroot-0.1.0 (c (n "fakeroot") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "redhook") (r "^2.0.0") (d #t) (k 0)))) (h "18wrjjh6lmahhf08cq871128nyicb3rzzw6c17flnigh6vdrrnnh")))

(define-public crate-fakeroot-0.1.1 (c (n "fakeroot") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "redhook") (r "^2.0.0") (d #t) (k 0)))) (h "06i0ci9ph14850h7p9f3ghwhrd2j3m2c8l94ws0g1bli4g1qzmql")))

(define-public crate-fakeroot-0.2.0 (c (n "fakeroot") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "redhook") (r "^2.0.0") (d #t) (k 0)))) (h "0216kcgfkkagpf1kzy1r3zdv5v3h7hp9y5b0ya14dkz6mw2dfkiy")))

(define-public crate-fakeroot-0.3.0 (c (n "fakeroot") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "redhook") (r "^2.0.0") (d #t) (k 0)))) (h "0wna93bi7aqw7851xw1iynyl80n9gk73zixjxnfs7cgzs9l1brgh")))

(define-public crate-fakeroot-0.4.0 (c (n "fakeroot") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "redhook") (r "^2.0.0") (d #t) (k 0)))) (h "09h93ipvwxfn57qr61yhpqdi86d0g1khrwdjjnyzlawssyv2svn5")))

(define-public crate-fakeroot-0.4.1 (c (n "fakeroot") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "redhook") (r "^2.0.0") (d #t) (k 0)))) (h "061vbvg7kcwls7m89vvd34d316rgkz84nlf5khz31dfw26sbixi5")))

