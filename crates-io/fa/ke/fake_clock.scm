(define-module (crates-io fa ke fake_clock) #:use-module (crates-io))

(define-public crate-fake_clock-0.1.0 (c (n "fake_clock") (v "0.1.0") (h "000438qg4y23ydy8gmvrqzj5kjplhw1x9q561jqzh00aqg59nam3")))

(define-public crate-fake_clock-0.2.0 (c (n "fake_clock") (v "0.2.0") (h "075q62i7kcdm0fpjiars76q2igz37mrrg40gf02398rsgk1ri01d")))

(define-public crate-fake_clock-0.3.0 (c (n "fake_clock") (v "0.3.0") (h "085kbfvvb8hs9iamcsxbpm4dhnahjvyf0w3k8yiw60z6y3zww8d1")))

(define-public crate-fake_clock-0.3.1 (c (n "fake_clock") (v "0.3.1") (h "14vgn3lla36i7f0ljgh4yv0s4k10ww2d5gvqqylpd2w46097fhgh")))

