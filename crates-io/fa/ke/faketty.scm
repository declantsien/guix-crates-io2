(define-module (crates-io fa ke faketty) #:use-module (crates-io))

(define-public crate-faketty-1.0.0 (c (n "faketty") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "02j9h62y7k17cb912gm48ybpdi8x8mqa1y51hwdj3iy5j5k8lhv5")))

(define-public crate-faketty-1.0.1 (c (n "faketty") (v "1.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "1frykxslxyinkw718njpl0nw4s0g5539hy6893mj21i4ny4yxvgr")))

(define-public crate-faketty-1.0.2 (c (n "faketty") (v "1.0.2") (d (list (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "08jzdciib2w69w058lp3s028yia0xypfpm5ahp69bfmq9yg3jaqc")))

(define-public crate-faketty-1.0.3 (c (n "faketty") (v "1.0.3") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "1lgyzsx2dfx3acyvfg0gr5xz2205zmdxc5b61x55cs3z0d0v9c4a")))

(define-public crate-faketty-1.0.4 (c (n "faketty") (v "1.0.4") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "nix") (r "^0.24") (d #t) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "1f52clizpwgp3vmq5my02draa6sm45sq68nzb7pd1wiancfmf2zs")))

(define-public crate-faketty-1.0.5 (c (n "faketty") (v "1.0.5") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("fs" "process" "term"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "1qld6h25n2v6vslskzh49sj4lq4x5k8pzkz6lhr0b70nqvq0hjwh")))

(define-public crate-faketty-1.0.6 (c (n "faketty") (v "1.0.6") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("fs" "process" "term"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "02s6kydh3h5rpbfw63yk50z1rb067qank7hpqs8zj4r9an5bbdwp")))

(define-public crate-faketty-1.0.7 (c (n "faketty") (v "1.0.7") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("deprecated"))) (d #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("fs" "process" "term"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "1dygrzd0j5swy6m7i4rirr02gpviwdlknhlj25z0q2ynfxw9r4ad")))

(define-public crate-faketty-1.0.8 (c (n "faketty") (v "1.0.8") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("deprecated"))) (d #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("fs" "process" "term"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "0vwx6p90j09lmmmfmjqj6h2m6cryc9dgp77h4nbz8gj612gpdvjf")))

(define-public crate-faketty-1.0.9 (c (n "faketty") (v "1.0.9") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("deprecated"))) (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("fs" "process" "term"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "012ln3vynl7jns3y5jsxw23fi90d8zydhh8lqwpk9awpkgvhxdk1")))

(define-public crate-faketty-1.0.10 (c (n "faketty") (v "1.0.10") (d (list (d (n "clap") (r "^4") (f (quote ("deprecated"))) (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("fs" "process" "term"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "0macmrnrjjkvzs7ms42fy58am5phs9l24v5qrm6xyaywnlgzx8gy")))

(define-public crate-faketty-1.0.11 (c (n "faketty") (v "1.0.11") (d (list (d (n "clap") (r "^4") (f (quote ("deprecated"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("fs" "process" "term"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "1hjiahrnvxa2cqaplfslhxfzrrgcmfn0dsx08c03hw0kvjx49z6b")))

(define-public crate-faketty-1.0.12 (c (n "faketty") (v "1.0.12") (d (list (d (n "clap") (r "^4") (f (quote ("deprecated"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("fs" "process" "term"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "1b2wcikq2yqnx9s4x7qpmirm4vqi218d9r1w0ww3gywdjg2zh646")))

(define-public crate-faketty-1.0.13 (c (n "faketty") (v "1.0.13") (d (list (d (n "clap") (r "^4") (f (quote ("deprecated"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("fs" "process" "term"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "1yh97984g96ncbz78d22kdxb70akcv3hgzwi7vjragxrxvxn1phg")))

(define-public crate-faketty-1.0.14 (c (n "faketty") (v "1.0.14") (d (list (d (n "clap") (r "^4") (f (quote ("deprecated"))) (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("fs" "process" "term"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "1bq6gj0cvhssgc1lkh9j83gdajk590gsxjc8mc17pm7walyrimw6")))

(define-public crate-faketty-1.0.15 (c (n "faketty") (v "1.0.15") (d (list (d (n "clap") (r "^4") (f (quote ("deprecated"))) (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("fs" "process" "term"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "12psfhhvqilg3by7dyx2zbzp74g6zw4gi7qgckw38shaq2lf4whf")))

(define-public crate-faketty-1.0.16 (c (n "faketty") (v "1.0.16") (d (list (d (n "clap") (r "^4") (f (quote ("deprecated"))) (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("fs" "process" "term"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "0nn8a92cgcg8vhclnk9495qa24qh3r4ld2zgzc8why1c9s02w6ja")))

(define-public crate-faketty-1.0.17 (c (n "faketty") (v "1.0.17") (d (list (d (n "clap") (r "^4") (f (quote ("deprecated"))) (d #t) (k 0)) (d (n "nix") (r "^0.29") (f (quote ("fs" "process" "term"))) (k 0)) (d (n "scratch") (r "^1.0") (d #t) (k 2)))) (h "1wa59vdlgrhcvdixv8wys8q4pl521m3z9lhja5zk0hczvggbzqw5")))

