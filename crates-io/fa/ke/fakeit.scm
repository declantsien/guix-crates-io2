(define-module (crates-io fa ke fakeit) #:use-module (crates-io))

(define-public crate-fakeit-0.0.1 (c (n "fakeit") (v "0.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0jvw9zn20ashm3wfm9gvsmb3ckzikxs3k4kn4sg6qyb7qqyml6bj")))

(define-public crate-fakeit-0.0.2 (c (n "fakeit") (v "0.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0vsrnw5lvw1gjpxwhdhcb8bp9kr2an3vkfq6xr7a06400mhnrhwp")))

(define-public crate-fakeit-0.0.3 (c (n "fakeit") (v "0.0.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v1" "v4"))) (d #t) (k 0)))) (h "0yi791p8nkwqa83kcdgclyzs4f7xiv72b2139bllhzl555w6vy13")))

(define-public crate-fakeit-0.0.4 (c (n "fakeit") (v "0.0.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v1" "v4"))) (d #t) (k 0)))) (h "1bhkgb1x5603mrc25h0l8ha2m4w04n5yrb87fpmwbgp2jm51xg4z")))

(define-public crate-fakeit-0.1.0 (c (n "fakeit") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v1" "v4"))) (d #t) (k 0)))) (h "1qk3kj7pf1lzg1ypw1ppg6l8y4jxk6id5wmzsrqhqh3lrkl25g66")))

(define-public crate-fakeit-1.0.0 (c (n "fakeit") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v1" "v4"))) (d #t) (k 0)))) (h "0cyspnkkd0f03alblispak2z9ypcrf5ph1zpl7j6bav24r2b95bv")))

(define-public crate-fakeit-1.1.0 (c (n "fakeit") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "simplerand") (r "^1.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v1" "v4"))) (d #t) (k 0)))) (h "0k3a4rylyicw8pln5iyij45vm3nbasmv026ai85ck26s7qhdqzq7")))

(define-public crate-fakeit-1.1.1 (c (n "fakeit") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "simplerand") (r "^1.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v1" "v4"))) (d #t) (k 0)))) (h "0mjng0fa4kfcd2mwr8ffc1dqg42blss66y0dbxypwaiad964hg45")))

(define-public crate-fakeit-1.1.2 (c (n "fakeit") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "simplerand") (r "^1.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v1" "v4"))) (d #t) (k 0)))) (h "0kcn869q50k0m26jbldlsbxb6wwabdbdn6nqbvk2kmrrbbyapg0q")))

(define-public crate-fakeit-1.1.3 (c (n "fakeit") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "simplerand") (r "^1.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v1" "v4"))) (d #t) (k 0)))) (h "0gy8sk8n4p5b4zll0sbsm4gy8h87axavs3982clf7n7br7vlkr6b")))

(define-public crate-fakeit-1.2.0 (c (n "fakeit") (v "1.2.0") (d (list (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "simplerand") (r "^1.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v1" "v4"))) (d #t) (k 0)))) (h "0zsz58zfawf3zk9ig3n7dbd382lgndjx0xxngwsymilcgipr0bfi")))

