(define-module (crates-io fa ke fake_instant) #:use-module (crates-io))

(define-public crate-fake_instant-0.4.0 (c (n "fake_instant") (v "0.4.0") (h "1b903v65hy0lf0pmpqy1zpp58bxh4101cc9rk2s945gjgcpdy1ih")))

(define-public crate-fake_instant-0.5.0 (c (n "fake_instant") (v "0.5.0") (h "1g6gfm44m6gw44nks4mc2ff5qrdnr2lz6qc23jh6lydk89vx9f1q")))

