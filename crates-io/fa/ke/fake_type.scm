(define-module (crates-io fa ke fake_type) #:use-module (crates-io))

(define-public crate-fake_type-0.1.1 (c (n "fake_type") (v "0.1.1") (h "0b0j965mcan0phhv91g06iv1l49qcr7w9snixy18i6rj8xlfvmy5") (y #t)))

(define-public crate-fake_type-0.1.2 (c (n "fake_type") (v "0.1.2") (h "153kqaiqw7xvrhccn32lknibg95qslabvi1s51r4qywq8i7zlsla") (y #t)))

(define-public crate-fake_type-0.1.3 (c (n "fake_type") (v "0.1.3") (h "1w7d6h8nqv8963hdwyy9ii30cdh13cxqyyhz4yh321yw8xfvxgjf")))

(define-public crate-fake_type-0.1.4 (c (n "fake_type") (v "0.1.4") (h "1yfhsd9bybb8nz01x1cdlg7j44d2rqiizvg2rq3y0i8x36p9rsrl")))

