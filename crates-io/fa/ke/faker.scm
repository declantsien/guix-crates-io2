(define-module (crates-io fa ke faker) #:use-module (crates-io))

(define-public crate-faker-0.0.1 (c (n "faker") (v "0.0.1") (h "0mjnwxwpx2lsglg3nifcgbyl43klc7gi7f9ljhzgbkdnalbyvwdr")))

(define-public crate-faker-0.0.2 (c (n "faker") (v "0.0.2") (h "102gjydj976dkr99wz1z3l8hihqhsghr8hzylr4j9a3lbaqj98mb") (y #t)))

(define-public crate-faker-0.0.3 (c (n "faker") (v "0.0.3") (h "0j3nngcsxyfz3ga08m9sw93w0i1b57pq36wfmqpkng4p3ry5vi0n")))

(define-public crate-faker-0.0.4 (c (n "faker") (v "0.0.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 2)))) (h "0jf17j47y5ysmmspyq5syvzqphzgl32khn8q27pakbcm0zv6yhwj")))

