(define-module (crates-io fa ke fake-torrent-client) #:use-module (crates-io))

(define-public crate-fake-torrent-client-0.9.0 (c (n "fake-torrent-client") (v "0.9.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_regex") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1gm39cnrf9yw4si0xvg8qwpw2m7p24123ir986nvjip4qf2ky6d7") (y #t)))

(define-public crate-fake-torrent-client-0.9.1 (c (n "fake-torrent-client") (v "0.9.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_regex") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0skzz9j7lfynvhiqsnz2mj44klhnmvki7532cvnd0lj7zm251di8") (y #t)))

(define-public crate-fake-torrent-client-0.9.2 (c (n "fake-torrent-client") (v "0.9.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_regex") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0dwy3dhf03drsfmq56n5gf8yklhh9yzqr226wiandw6kp30q6cbz") (y #t)))

(define-public crate-fake-torrent-client-0.9.3 (c (n "fake-torrent-client") (v "0.9.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_regex") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0ppk2bhrq4qfpp5jz01bz6jsws3vwlsywsvx7f8lfigca9b4cnkp") (y #t)))

(define-public crate-fake-torrent-client-0.9.4 (c (n "fake-torrent-client") (v "0.9.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_regex") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1qvry4lbsxprcjpj93xr924cx3v7f93ibj8xdypgbd64ww42zxhr") (y #t)))

(define-public crate-fake-torrent-client-0.9.5 (c (n "fake-torrent-client") (v "0.9.5") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_regex") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1hqd8dz4jl0cpw1nwlpxf42lis0sgkkjaaqkr05zxa6hymnw7mix") (y #t)))

(define-public crate-fake-torrent-client-0.9.6 (c (n "fake-torrent-client") (v "0.9.6") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_regex") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0b62ln1w08ncbd42yyxrbsxywjr7026x1wx6ab3q9f1dd19kbgnp")))

