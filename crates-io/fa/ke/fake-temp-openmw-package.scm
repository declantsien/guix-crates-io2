(define-module (crates-io fa ke fake-temp-openmw-package) #:use-module (crates-io))

(define-public crate-fake-temp-openmw-package-0.2.0 (c (n "fake-temp-openmw-package") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.9") (d #t) (k 0)) (d (n "crossterm_input") (r "^0.3") (d #t) (k 0)) (d (n "esplugin") (r "^3") (d #t) (k 0)) (d (n "tui") (r "^0.6") (f (quote ("crossterm"))) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1jiycb934bjgq1pq23mrab2dnbkh74kh0w1phgj49bc6dwyra5nn")))

