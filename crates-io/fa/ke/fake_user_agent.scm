(define-module (crates-io fa ke fake_user_agent) #:use-module (crates-io))

(define-public crate-fake_user_agent-0.1.0 (c (n "fake_user_agent") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "1jkmk2cp9apmd9rfpj986jssm00j3k4gz3sgyr2sm0jibldw8kxw")))

(define-public crate-fake_user_agent-0.1.1 (c (n "fake_user_agent") (v "0.1.1") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "11f4lv7chp7bpdfg8c4hsml8j2dlhr2y3piq9xi2bdqr6bw8pk34")))

(define-public crate-fake_user_agent-0.1.2 (c (n "fake_user_agent") (v "0.1.2") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "0gw4wx70pxrcr1yszisbyk9168zyclpnndalw7c2bqszsfz4wm8x")))

(define-public crate-fake_user_agent-0.1.3 (c (n "fake_user_agent") (v "0.1.3") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "0ackcd6bp9km41m9qqcnmq8rrxx9816ii5d637vzxx8m2yz9n9dy")))

(define-public crate-fake_user_agent-0.1.4 (c (n "fake_user_agent") (v "0.1.4") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "176vbwnk96733fml1ajpwldzy4p8b8a295v7z93qwiac7fgxc65r")))

(define-public crate-fake_user_agent-0.2.0 (c (n "fake_user_agent") (v "0.2.0") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)))) (h "0ckk1g5q1g2dvhyc7b014cnhiv1ffq4fa39q7dsb4i9b3zs3z3y7")))

(define-public crate-fake_user_agent-0.2.1 (c (n "fake_user_agent") (v "0.2.1") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)))) (h "1a3f465hhf9saadqyiwim35qj28391a78n4y5c3pyj8akr0qwsgb")))

