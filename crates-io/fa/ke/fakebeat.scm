(define-module (crates-io fa ke fakebeat) #:use-module (crates-io))

(define-public crate-fakebeat-0.1.0 (c (n "fakebeat") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elasticsearch") (r "^8.4.0-alpha.1") (d #t) (k 0)) (d (n "fakebeat_core") (r "^0.1.0") (d #t) (k 0)) (d (n "linya") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09isllv7n4m8sb3bqmrl9ckbf3b96dzhzg27wg2byggaj4hckrjr")))

(define-public crate-fakebeat-0.1.1 (c (n "fakebeat") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elasticsearch") (r "^8.4.0-alpha.1") (d #t) (k 0)) (d (n "fakebeat_core") (r "0.1.*") (d #t) (k 0)) (d (n "linya") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b0hf4a8ykgchdgx1z5if1jgk8hjrf5bkw9lddz4lvg6zdx36ja8")))

(define-public crate-fakebeat-0.1.2 (c (n "fakebeat") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "elasticsearch") (r "^8.4.0-alpha.1") (d #t) (k 0)) (d (n "fakebeat_core") (r "0.1.*") (d #t) (k 0)) (d (n "linya") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p1vfqhmjhrj01v0ylvcj7mz8xnjz94w5hz8hd1zamgsn4d6r5ab")))

