(define-module (crates-io fa ke fakedata_generator) #:use-module (crates-io))

(define-public crate-fakedata_generator-0.1.0 (c (n "fakedata_generator") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f3lx1ycbx1rq9nvm9ch55hwgwndgv088db5f8l3qn7waf4zadaw")))

(define-public crate-fakedata_generator-0.2.0 (c (n "fakedata_generator") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11izvivkp5fhj033gmkc5sw5a1lcm7x4fdn8c1s4l3li1l7isc84")))

(define-public crate-fakedata_generator-0.2.1 (c (n "fakedata_generator") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hfbyc4095765jsl350cj2wslq4fdrjj3s41l2wa723m0l16f31v")))

(define-public crate-fakedata_generator-0.2.2 (c (n "fakedata_generator") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12ir6idxqqs0cp7fn85rsb19hb7bmj18w27nn46cawz8ass3ixac")))

(define-public crate-fakedata_generator-0.2.3 (c (n "fakedata_generator") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04f9fz7vs0lrlqkq16rp68raf7k0zpakcaw6a3cnpz7jr1kvxh35")))

(define-public crate-fakedata_generator-0.2.4 (c (n "fakedata_generator") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "125vbnl317qv2v03qkgvv97ci4i3qmaf4lkyjlbdvl6c48mj05b8")))

(define-public crate-fakedata_generator-0.3.0 (c (n "fakedata_generator") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "195i1al6fbj6lzg1w89g1mki215ikq9s81kacszwxb76fxjzq9na")))

(define-public crate-fakedata_generator-0.4.0 (c (n "fakedata_generator") (v "0.4.0") (d (list (d (n "passt") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x56lq0s7kwcgw5jc5cj9y2dz06sj9ri0c9ikmy28p0h1pv4aaih")))

(define-public crate-fakedata_generator-0.4.1 (c (n "fakedata_generator") (v "0.4.1") (d (list (d (n "passt") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0irf85vjvf5c3kjwhvp3v01b469mpli11llys5icmjzrc3h9m3fx")))

(define-public crate-fakedata_generator-0.5.0 (c (n "fakedata_generator") (v "0.5.0") (d (list (d (n "passt") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00jaywws57rcq4x5h8kvrva2p7cad04j20blvsgq2ns89fx2zf2p")))

