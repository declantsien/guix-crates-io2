(define-module (crates-io fa ke fakes-gen) #:use-module (crates-io))

(define-public crate-fakes-gen-0.1.0 (c (n "fakes-gen") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0kb2b4wghb0x0vnpxhlk7qg1j3f3i8czfq7a742ap5yhp1vnc50d") (y #t)))

(define-public crate-fakes-gen-0.1.1 (c (n "fakes-gen") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0x0gpkhp0d8wm6p6wwl4k3pvv58l9x6bd017xpk2x2vqz8cq91lj")))

(define-public crate-fakes-gen-0.1.2 (c (n "fakes-gen") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "12qllwv8ix9wyiirjhabwcdcpan0h4xjxdpn4k0ynp5nxydhzyrr")))

(define-public crate-fakes-gen-0.2.0 (c (n "fakes-gen") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0dnq1dk7m4rf3sh4aflr79snjz3dcs3wzhnkllgvisvh0xm317a4")))

(define-public crate-fakes-gen-0.2.1 (c (n "fakes-gen") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1mm4dlbllx1qs03sr444pjm8gg94craw1wzhmfqgcsayjk5ydvvy")))

(define-public crate-fakes-gen-0.2.2 (c (n "fakes-gen") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0yb8y0w0h86kwch6qigpy7midhpqlp8gbwbj84vdrdf1ck5z1if3")))

(define-public crate-fakes-gen-0.2.4 (c (n "fakes-gen") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "105anxvq7cm4l53yc8p40k0g00dhy5xcr961jrsy7a2357lf6f3x")))

(define-public crate-fakes-gen-0.2.6 (c (n "fakes-gen") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1spffgrjzpiq5m1nkxgcs6hnrq9p9sgfb1v5f9pzhqr3f09j1c6k")))

