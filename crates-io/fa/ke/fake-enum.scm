(define-module (crates-io fa ke fake-enum) #:use-module (crates-io))

(define-public crate-fake-enum-0.1.0 (c (n "fake-enum") (v "0.1.0") (h "0rwmrpfvi8iqs3cqjnm212aal50giw4930a25kvz3ljnwvxg0kay") (y #t)))

(define-public crate-fake-enum-0.1.1 (c (n "fake-enum") (v "0.1.1") (h "0a6npxsyqgnzga4gznmlgfcd1biv14l01mcw6xrv8yk0bpva59ag")))

(define-public crate-fake-enum-0.1.2 (c (n "fake-enum") (v "0.1.2") (h "15ffwwnz5prja9r9zjmamy9fiji0m8j1qw2zzj8q9hrxpinwmy0r")))

(define-public crate-fake-enum-0.1.3 (c (n "fake-enum") (v "0.1.3") (h "0nc3qdk26qj8sw6729zm53v248k3l266a86mvyy2sjyp3mx4h9w8") (y #t)))

(define-public crate-fake-enum-0.1.4 (c (n "fake-enum") (v "0.1.4") (h "1fjy5hq6hbz75cq6ng6dasbnpm45nlr3g9racsw8jdzzdb94qnqq")))

