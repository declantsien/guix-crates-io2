(define-module (crates-io fa ke faker-cli) #:use-module (crates-io))

(define-public crate-faker-cli-0.1.0 (c (n "faker-cli") (v "0.1.0") (d (list (d (n "fake") (r "^2.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "10a58rgsqla1p7d8cm5jfmzc524qcmnhx8w8h17mhqi8d6mmhz6l")))

