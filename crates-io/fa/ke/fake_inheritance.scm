(define-module (crates-io fa ke fake_inheritance) #:use-module (crates-io))

(define-public crate-fake_inheritance-0.1.0 (c (n "fake_inheritance") (v "0.1.0") (h "08sbi4ag2agjp90qjq5zwxr4h905r1f4ypdm8l22pnxcz3jxv4al")))

(define-public crate-fake_inheritance-0.1.1 (c (n "fake_inheritance") (v "0.1.1") (h "1vlxrrjdkzmpd3h2pzzg56rpn4sixccwcg63sxdc9cna7fh9qa06")))

(define-public crate-fake_inheritance-0.2.1 (c (n "fake_inheritance") (v "0.2.1") (h "19n0iljig753il7lnwpi0rnzf0142ay2rwfdsxdi7ny5069kag7f")))

