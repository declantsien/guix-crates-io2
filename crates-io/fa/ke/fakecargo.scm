(define-module (crates-io fa ke fakecargo) #:use-module (crates-io))

(define-public crate-fakecargo-0.1.0 (c (n "fakecargo") (v "0.1.0") (h "13d46l25z1hig4nqb6whgg6kh6nk3ajmb1pi8p52q2nmwmik06gp")))

(define-public crate-fakecargo-0.1.1 (c (n "fakecargo") (v "0.1.1") (h "1fhvm6zwcqqdyxrhyam3a41k1xjvlci23xmf6lzfkn1cxkaninyp")))

(define-public crate-fakecargo-0.2.0 (c (n "fakecargo") (v "0.2.0") (h "1j2zz3ppjhb91lai2rhcj3mm68kavbaj5czqqfvw5n97dhsk74jh")))

(define-public crate-fakecargo-0.2.1 (c (n "fakecargo") (v "0.2.1") (h "0hzfd9ijfmpa7pbcpf9wvxkc7m0yxbc111q9vndwbpa15gibl3k1")))

(define-public crate-fakecargo-0.2.5 (c (n "fakecargo") (v "0.2.5") (h "0fdygil1wslkk0wx3wrx8n05bmpgw00fq2ifqrirq979904nsj93")))

(define-public crate-fakecargo-0.2.51 (c (n "fakecargo") (v "0.2.51") (h "109mwai50mgc5n2b31gqqp6wi8a9j9m8j5f9kba8a2p8g7r3q8fm")))

(define-public crate-fakecargo-0.3.0 (c (n "fakecargo") (v "0.3.0") (h "1zjs1nvmlysddjnwl8150rw17fyzp26jd2pdpdqc4czilr46j2x3")))

(define-public crate-fakecargo-0.3.2 (c (n "fakecargo") (v "0.3.2") (h "132lszyxa4qp3ka1vc9q85rlfrzvixas74kbbcggn4hxsja4p6r0")))

(define-public crate-fakecargo-0.3.3 (c (n "fakecargo") (v "0.3.3") (h "12fmpz04xhzf9iihxckkvd6sh089i6dnks6jhhvags1qmanxqs39")))

(define-public crate-fakecargo-0.3.4 (c (n "fakecargo") (v "0.3.4") (h "1pc46f7bk66aw92nnvh4vikd93l2qy4z5qqirqy61lf3dsllvmbp")))

(define-public crate-fakecargo-0.4.0 (c (n "fakecargo") (v "0.4.0") (h "13h24w7xsg6anqkpc5n2zmky9xbjzvlpndsw71mcjfmdjd62ipcy")))

(define-public crate-fakecargo-0.5.0 (c (n "fakecargo") (v "0.5.0") (h "01ly7p0pjsazbxrmw184bjbfwbp8y6r6p6kp0fmxl5v6zxqvphbc")))

(define-public crate-fakecargo-0.6.0 (c (n "fakecargo") (v "0.6.0") (h "1a8psixk4h7qdd7h2m6p1hwhj8gq4z9xmvh2zjp3w1sn4w65b6b4")))

(define-public crate-fakecargo-0.7.0 (c (n "fakecargo") (v "0.7.0") (h "06iq7bjjqvjzkh5k5zp1rpkni0cshmzbm9r6fmcxn1ixhwr7kz2c")))

