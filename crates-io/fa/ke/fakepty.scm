(define-module (crates-io fa ke fakepty) #:use-module (crates-io))

(define-public crate-fakepty-1.0.0 (c (n "fakepty") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "portable-pty") (r "^0.8.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0a3g7sgv32j7k4idzidh6fn6abljrjjfs48ih4p8jbk5fx3rrv2s")))

(define-public crate-fakepty-1.1.0 (c (n "fakepty") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "portable-pty") (r "^0.8.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1yxz3k4ggdm9f79vaamicp4c2y4qai0lqbkkph7af91frpc1n49i")))

(define-public crate-fakepty-1.2.0 (c (n "fakepty") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "portable-pty") (r "^0.8.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0v8h34pcs86cpx97b5a1bhsq9j5vrcwwyws32krdv1y903f2l5qv")))

