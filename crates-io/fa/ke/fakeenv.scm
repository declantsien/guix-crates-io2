(define-module (crates-io fa ke fakeenv) #:use-module (crates-io))

(define-public crate-fakeenv-0.1.0 (c (n "fakeenv") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0.2") (o #t) (d #t) (k 0)))) (h "1jna7l1r3v8pggyd0p6givrdhg1a9r8xc1lag7c6bb4yy2p7109h") (f (quote (("fake") ("default" "dirs" "fake") ("__doc_cfg"))))))

