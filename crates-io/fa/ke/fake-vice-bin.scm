(define-module (crates-io fa ke fake-vice-bin) #:use-module (crates-io))

(define-public crate-fake-vice-bin-0.1.0 (c (n "fake-vice-bin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)))) (h "1m4d7wl00xnbf542nsmn0f0dgaiy39il95pxl3bsig5s1ymdkbaq")))

(define-public crate-fake-vice-bin-0.1.1 (c (n "fake-vice-bin") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "ringbuf") (r "^0.3.0") (d #t) (k 0)))) (h "0varmgpikkj8ff7lj68n3cvbbq7cn5sjqxqxw9agazc7qax3p2l6")))

