(define-module (crates-io fa ke fake-tty) #:use-module (crates-io))

(define-public crate-fake-tty-0.1.0 (c (n "fake-tty") (v "0.1.0") (h "1j2rbndhbc0x6alnmp7gf0nfaxwcy3vh2qhai67g7myrkf9v239z")))

(define-public crate-fake-tty-0.2.0 (c (n "fake-tty") (v "0.2.0") (h "0w9r1qlbi35rhhzrai0rjyj581slfny9lqn38dyrl7qxqr4c7ahw")))

(define-public crate-fake-tty-0.3.1 (c (n "fake-tty") (v "0.3.1") (h "1bg1lgkz0padjg7kbqni1cbaghj0hj1ba4qg1bwl0sax19s2lv5a")))

