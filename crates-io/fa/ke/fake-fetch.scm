(define-module (crates-io fa ke fake-fetch) #:use-module (crates-io))

(define-public crate-fake-fetch-0.0.1 (c (n "fake-fetch") (v "0.0.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "tetsy-fetch") (r "^0.1.0") (d #t) (k 0)))) (h "1kfw7gpk0waibvj6ms7sdphhziqydncpwr1h1b9ybskbgykspy3a")))

