(define-module (crates-io fa ke fakelogs) #:use-module (crates-io))

(define-public crate-fakelogs-0.1.5-6e5ec74 (c (n "fakelogs") (v "0.1.5-6e5ec74") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "ticker") (r "^0.1") (d #t) (k 0)))) (h "1r8lji9ijbdvzkgx4vc0hsgapyl0wsy9sjk5v94rpi7vcak10ajw")))

(define-public crate-fakelogs-0.1.6-a180978 (c (n "fakelogs") (v "0.1.6-a180978") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "ticker") (r "^0.1") (d #t) (k 0)))) (h "0cl0r4y798nam4xdl0821zrn7ivgjj7m88j1z3v7b6afdd1p1gmc")))

(define-public crate-fakelogs-0.1.7-35bfaa1 (c (n "fakelogs") (v "0.1.7-35bfaa1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "ticker") (r "^0.1") (d #t) (k 0)))) (h "0y10rf450nnzvv47j3clp55v08wgz1mrzz2w61cw1cwjqj9vkci5")))

(define-public crate-fakelogs-0.1.9-bb1f021 (c (n "fakelogs") (v "0.1.9-bb1f021") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "ticker") (r "^0.1") (d #t) (k 0)))) (h "1qi9badvgi3zf65qcp5yy51b0fqprw8qlicv7gjhvx2awzj187fp")))

(define-public crate-fakelogs-0.1.10-75501d4 (c (n "fakelogs") (v "0.1.10-75501d4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "ticker") (r "^0.1") (d #t) (k 0)))) (h "06j44z60i80v076pap6vw4nh1nalkvz35k75qg3xayd2nb5wvlnn")))

