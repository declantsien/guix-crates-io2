(define-module (crates-io fa ke fake-useragent) #:use-module (crates-io))

(define-public crate-fake-useragent-0.1.0 (c (n "fake-useragent") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "0g889lr5qsqbphszlr1n6cb4y9jyq1s2aqpj74plrf0j0hckbl8m")))

(define-public crate-fake-useragent-0.1.1 (c (n "fake-useragent") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "0pqdyx2avi6a4186a2hpql7j3q0fmrjs0lgnrgimmy1qki5alvh1")))

(define-public crate-fake-useragent-0.1.2 (c (n "fake-useragent") (v "0.1.2") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "09p5f4n9vyd2vgd6yjl6nk1k9kxibdw20nkyh1h7qwyl28p5j7rn")))

(define-public crate-fake-useragent-0.1.3 (c (n "fake-useragent") (v "0.1.3") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "1f971r02xikmyk1vgfxhmw0n4252yfkfb2kdgvxhyg0dabxc6hf3")))

