(define-module (crates-io fa ke faker_rand) #:use-module (crates-io))

(define-public crate-faker_rand-0.1.0 (c (n "faker_rand") (v "0.1.0") (d (list (d (n "deunicode") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1nwhb1dyrip9xz3agivj7nbk20685m6r4jcy0fip4na1c18q8kdb")))

(define-public crate-faker_rand-0.1.1 (c (n "faker_rand") (v "0.1.1") (d (list (d (n "deunicode") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1dpz77bjpy0scvxlb45yq97in4ik22bf159rf9g5nnr4ybdjs39h")))

