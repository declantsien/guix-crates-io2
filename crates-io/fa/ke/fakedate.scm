(define-module (crates-io fa ke fakedate) #:use-module (crates-io))

(define-public crate-fakedate-0.1.1-211a512 (c (n "fakedate") (v "0.1.1-211a512") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0imd64hc2hfh2z5b3b0yzbaijdhhzchp48f0cgxzwhvx7hw2ycya")))

(define-public crate-fakedate-0.1.2-619366e (c (n "fakedate") (v "0.1.2-619366e") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1krp5kd3kyb4b51p029l79byibm854dsl8mfmdkigjahkwrf630v")))

