(define-module (crates-io fa ke faketime) #:use-module (crates-io))

(define-public crate-faketime-0.1.0 (c (n "faketime") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.0") (d #t) (t "cfg(not(disable_faketime))") (k 0)))) (h "0hrs4dl2a8kb69qwxdbaad7d71ip5ka1qhvjd1ch4xa37phphh5h")))

(define-public crate-faketime-0.1.1 (c (n "faketime") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.0") (d #t) (t "cfg(not(disable_faketime))") (k 0)))) (h "1hv2200vgi0q4prxqqgn4wn43sahxqjng9sb9wyn3jbwir4d33pr")))

(define-public crate-faketime-0.2.0 (c (n "faketime") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.0") (d #t) (t "cfg(not(disable_faketime))") (k 0)))) (h "1y2c818q9v09vw8l5abwysy9lic7lnyx7ngc82yyi8x4qpc7n20p")))

(define-public crate-faketime-0.2.1 (c (n "faketime") (v "0.2.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"unknown\"))") (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (t "cfg(not(disable_faketime))") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0asq02l0mp6hrq6lkd4ma6f407807b5xd7k716x0nxiwwszw5kzx")))

