(define-module (crates-io fa ke fake_headers) #:use-module (crates-io))

(define-public crate-fake_headers-0.0.1 (c (n "fake_headers") (v "0.0.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "110sdkc0qcv32gkwvydqlh1s9kkdcbr2549c4bp88xv1dab5zsib")))

(define-public crate-fake_headers-0.0.2 (c (n "fake_headers") (v "0.0.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0j5scmsh9p8lh6dd1q118s0c2vmn8l1vavwrxc3vhdswhqg1g8nv")))

(define-public crate-fake_headers-0.0.3 (c (n "fake_headers") (v "0.0.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "168lrcbw2kil2lwqqq0grlxp6szg92hqw808y0x3z4nply04ffn1")))

(define-public crate-fake_headers-0.0.4 (c (n "fake_headers") (v "0.0.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "16gi3s7hr0563kx8kdz2pwy5lyr8azhv7mcz8gdc92ms87xv21bb")))

(define-public crate-fake_headers-0.0.5 (c (n "fake_headers") (v "0.0.5") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1ws28xs682djr0s4ji77266bikhclz6k5dlr1axrvsqy19bkf7zp")))

