(define-module (crates-io fa ke fake-simd) #:use-module (crates-io))

(define-public crate-fake-simd-0.1.0 (c (n "fake-simd") (v "0.1.0") (h "1c8cb9hijh7w7fm854cs54ymafa1kfvrj4c3g58c7vmimf8vj2iy")))

(define-public crate-fake-simd-0.1.1 (c (n "fake-simd") (v "0.1.1") (h "1dvxqw7njayabil3705x421msdgc4jc1yq95jg8vhvz9724flamm")))

(define-public crate-fake-simd-0.1.2 (c (n "fake-simd") (v "0.1.2") (h "1vfylvk4va2ivqx85603lyqqp0zk52cgbs4n5nfbbbqx577qm2p8")))

