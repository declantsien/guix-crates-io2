(define-module (crates-io fa ke fakebeat_core) #:use-module (crates-io))

(define-public crate-fakebeat_core-0.1.0 (c (n "fakebeat_core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "elasticsearch") (r "^8.4.0-alpha.1") (d #t) (k 0)) (d (n "fake") (r "^2.5") (f (quote ("http"))) (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "tera") (r "^1") (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18z1vaydgxnc3v7nf5yk5gmka52yv98slvx3s7r10n3kag59b1as")))

