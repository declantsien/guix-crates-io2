(define-module (crates-io fa ke fake_serialize) #:use-module (crates-io))

(define-public crate-fake_serialize-0.1.0 (c (n "fake_serialize") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fyik8rr0h5hmm8la2l8fqm1i3p42iqwzgj47pnf4bi3bimb195d") (f (quote (("json" "serde_json") ("defaults"))))))

(define-public crate-fake_serialize-0.1.1 (c (n "fake_serialize") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k10bznjy1nd6vdrsslyvnxmik5cx8828f34snhdv5whdjhgvj63") (f (quote (("json" "serde_json") ("defaults"))))))

