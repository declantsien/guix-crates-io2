(define-module (crates-io fa ke fake_haproxy) #:use-module (crates-io))

(define-public crate-fake_haproxy-0.1.0 (c (n "fake_haproxy") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1b14nwh9b739s1v3nz6d97wf1266arsvlf76ksyrn1l4jz1wla0r")))

(define-public crate-fake_haproxy-0.1.1 (c (n "fake_haproxy") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0y96sjih8p60zyfw6rfcrkqv7dasq18214jzy4ndma99s580ra6b")))

