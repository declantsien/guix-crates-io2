(define-module (crates-io fa no fano) #:use-module (crates-io))

(define-public crate-fano-0.1.0 (c (n "fano") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "05ns8qa7999jdsrlqfylz6y4xim1is0xakczpm42fwv7wh1y7ps2")))

(define-public crate-fano-0.1.1 (c (n "fano") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1cwk06ghp129imkpqm15kph52mriiqkvkd7bhdd0s4dzr4ckvxdq")))

