(define-module (crates-io fa no fanotify-rs) #:use-module (crates-io))

(define-public crate-fanotify-rs-0.1.0 (c (n "fanotify-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wndxpf7llv6i45zby6547scg89cgfbw3vq52351gfsnk517n0a7") (y #t)))

(define-public crate-fanotify-rs-0.1.1 (c (n "fanotify-rs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12wslc4l4zzjf34w65bdf1bd6dyi3y7agjzrzhrn5zibqqwnsazz") (y #t)))

(define-public crate-fanotify-rs-0.1.2 (c (n "fanotify-rs") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d79sn2xm2pjqqaxvk3y218f218nyg9zbl4a4y8ppi0wjrbp0nc0") (y #t)))

(define-public crate-fanotify-rs-0.1.3 (c (n "fanotify-rs") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09zawi1v72jf010z78v8dx81vryhxxq42xm81jl2k2h7acj9zzph") (y #t)))

(define-public crate-fanotify-rs-0.1.4 (c (n "fanotify-rs") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0prbvv5n9579bw18h4sl4ldd3c5ncpzxjwb5547zkz1z1la3mr2p") (y #t)))

(define-public crate-fanotify-rs-0.1.5 (c (n "fanotify-rs") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10j0rm6in9g7v7jd70xd7571srhiqfsjl26w5p6qynjqyk3yq9a8") (y #t)))

(define-public crate-fanotify-rs-0.2.0 (c (n "fanotify-rs") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sr1n58j1p6j9k0dxi8q813rakyng7kphy49f3j8za1li5p5m4m6") (y #t)))

(define-public crate-fanotify-rs-0.2.1 (c (n "fanotify-rs") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bbmq2mv4xf29al595nrnzx0g7hjnzy94kw79d7k8whsl1xwhryk") (y #t)))

(define-public crate-fanotify-rs-0.2.3 (c (n "fanotify-rs") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18cyqmklf22lsq85a7yfijajfcwnr8xf7k6zmgkjlvspgf0bq6lx") (y #t)))

(define-public crate-fanotify-rs-0.2.4 (c (n "fanotify-rs") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "087ypbj2fxhxw40iq4bj9wkwsbabsbfdymi0qmk6l4bggqpmc1x6") (y #t)))

(define-public crate-fanotify-rs-0.2.5 (c (n "fanotify-rs") (v "0.2.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0js6k18g135ifhqwxacpv5ivadm8r5rr25kwpbwdwn7jiq6lifgx") (y #t)))

(define-public crate-fanotify-rs-0.2.6 (c (n "fanotify-rs") (v "0.2.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0di5fph4vx76c6m18jm3bgcfgq3hzgbldr931cxfmkmqlj8hgd9l") (y #t)))

(define-public crate-fanotify-rs-0.2.7 (c (n "fanotify-rs") (v "0.2.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01qs711xwk51raqfcaydc7d3dlmsb28s6vpsn686as5kyby2rbm6")))

(define-public crate-fanotify-rs-0.2.8 (c (n "fanotify-rs") (v "0.2.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v586n1f3vcm20zzw5mbw5df7dk6i4ynr01r755djqji2kabzj69")))

(define-public crate-fanotify-rs-0.2.9 (c (n "fanotify-rs") (v "0.2.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1v58yjrwcnw64fdr0pq490yx4mb109pxrzndixbwg1a8bkc3q5bs")))

(define-public crate-fanotify-rs-0.2.10 (c (n "fanotify-rs") (v "0.2.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qlk0mjkkfa568harhafyg0m1ny4n1n089zri02qxlqd2r4x3z0c")))

(define-public crate-fanotify-rs-0.3.1-rc1 (c (n "fanotify-rs") (v "0.3.1-rc1") (d (list (d (n "enum-iterator") (r "^1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j7vfbqxniysk7lvkp5ynrxj12p3wlqncbdgkfghmwmz8rsfh5w4")))

(define-public crate-fanotify-rs-0.3.1-rc2 (c (n "fanotify-rs") (v "0.3.1-rc2") (d (list (d (n "enum-iterator") (r "^1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r3mvc4i9ya01r39sh14yrfj8pjwyjjla3f7n087ww3j81yffkrg")))

(define-public crate-fanotify-rs-0.3.1-rc3 (c (n "fanotify-rs") (v "0.3.1-rc3") (d (list (d (n "enum-iterator") (r "^1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10pxlkf7dkjllzcmwag5kjmgmr3897i9fmspjn8ikqks9pbax4bm")))

(define-public crate-fanotify-rs-0.3.1 (c (n "fanotify-rs") (v "0.3.1") (d (list (d (n "enum-iterator") (r "^1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s5zpdgc88iv601x569nhrg1bj57jcx6chhnvj7m3bpksqzmyln9")))

