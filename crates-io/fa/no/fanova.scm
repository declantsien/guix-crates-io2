(define-module (crates-io fa no fanova) #:use-module (crates-io))

(define-public crate-fanova-0.1.0 (c (n "fanova") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0n43axzqzsp2k019k8yfgavss2k16xq0smyh80pvvi3fkphg354c")))

(define-public crate-fanova-0.1.1 (c (n "fanova") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "057fc7sggwj0pf6y1sz3rgnm6v9zbk7rdlkalwbrdnj8lyx27l1k")))

(define-public crate-fanova-0.2.0 (c (n "fanova") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02ym1lnnn2b1m8wkpr5kqcg4xs82mmwmhvxm3h76gki9s2c9fzy3")))

