(define-module (crates-io fa ng fang-derive-error) #:use-module (crates-io))

(define-public crate-fang-derive-error-0.1.0 (c (n "fang-derive-error") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1wln6i1bi82p431msr13izsrfy17jj5dn1az5508nawhg4dn9xfr")))

