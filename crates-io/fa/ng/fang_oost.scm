(define-module (crates-io fa ng fang_oost) #:use-module (crates-io))

(define-public crate-fang_oost-0.1.0 (c (n "fang_oost") (v "0.1.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 0)) (d (n "black_scholes") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 0)))) (h "1ngvdsc61qn71p85rzf98qqqjnx1bnsb9b9a36hddplbw3h0wyy8")))

(define-public crate-fang_oost-0.2.0 (c (n "fang_oost") (v "0.2.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 0)) (d (n "black_scholes") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 0)))) (h "0007mw4dga6bz6l00ycmq4vp59nhsywjx7wg4xrc3nvpb9q8cdqs")))

(define-public crate-fang_oost-0.3.0 (c (n "fang_oost") (v "0.3.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 0)) (d (n "black_scholes") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 0)))) (h "04wpvl6vw36ihcgmf2qg4xhnyvqkhpczwlavqhic0kd4611k1mp1")))

(define-public crate-fang_oost-0.4.0 (c (n "fang_oost") (v "0.4.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 0)) (d (n "black_scholes") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 0)))) (h "0v59f1x0a9csmp812z34ja70rlpkqm8g8fcikw3w4ydhql0jar0c")))

(define-public crate-fang_oost-0.5.0 (c (n "fang_oost") (v "0.5.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 0)) (d (n "black_scholes") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 0)))) (h "154nh7jrjw4yh18ai3fjv1pjhkbfj1lkziq1pcrxgp9d45xxn8qq")))

(define-public crate-fang_oost-0.6.0 (c (n "fang_oost") (v "0.6.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 0)))) (h "13d4pp7w4pdkq6v8dfysb9bmc4r03lpz44l626z5zs191x80w7rx")))

(define-public crate-fang_oost-0.7.0 (c (n "fang_oost") (v "0.7.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "0x7g6dxcnykcvqr616cxvblcnv7mvpvv3sxkdnwbiv55bwi5invz")))

(define-public crate-fang_oost-0.7.1 (c (n "fang_oost") (v "0.7.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "13z6x1jrrnhx7dncp84v16dwn39v9jkcczkvqvrj2jhn9n4b6gmw")))

(define-public crate-fang_oost-0.7.2 (c (n "fang_oost") (v "0.7.2") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "1wy7xgmpk6f0ald16pn7j5mzw87ihjrsshmj4302d1sppxl2fnd3")))

(define-public crate-fang_oost-0.8.0 (c (n "fang_oost") (v "0.8.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "0ckgyyidz7cq1lrm5fv5mz4fgff16m0qhcwq5l3h2hbmh1a4y8gm")))

(define-public crate-fang_oost-0.9.0 (c (n "fang_oost") (v "0.9.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "0ycaasajrwgzsl7l5nwa1pzqp9p3qk4af95qba3fg9h7rh8m6dx7")))

(define-public crate-fang_oost-0.9.1 (c (n "fang_oost") (v "0.9.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "1s34axs1p0vyk58dq3iq4axfi96228hydasnabzjdcx34d6sp9n5")))

(define-public crate-fang_oost-0.10.0 (c (n "fang_oost") (v "0.10.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "1x89mzbd3cx61z5f3gjcc1rkdwny5w7yvbw5xa1nly5mqv5zd2v0")))

(define-public crate-fang_oost-0.11.0 (c (n "fang_oost") (v "0.11.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "1xm8bx5v0s9q1khhapfwbxf2ch96jifcvjxy5xd5l3x02dn9vxhs")))

(define-public crate-fang_oost-0.12.0 (c (n "fang_oost") (v "0.12.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "0jmndiv18daa1prdbffqp8gqkqilgpfw5b4i81pcjdbqq41w7jb9")))

(define-public crate-fang_oost-0.13.0 (c (n "fang_oost") (v "0.13.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "0gc21a8wfq4qab3rkq0gpx7mghaglfbdp4py0p9f4k50njgnnypw")))

(define-public crate-fang_oost-0.13.1 (c (n "fang_oost") (v "0.13.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "1bpl43h0by8mg47b3liflfqij6h3s3gsaz9l7409wa303vdh9dbj")))

(define-public crate-fang_oost-0.13.2 (c (n "fang_oost") (v "0.13.2") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "090cpjh7x0q7pxd25rdvfn9ngxcwqvjp7l2hw7zxn6xmzjpj2wlw")))

(define-public crate-fang_oost-0.13.3 (c (n "fang_oost") (v "0.13.3") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "0g6fpghi8417y53382k1md22ds4vlim16y252x1i8b9898d3yp9k")))

(define-public crate-fang_oost-0.13.5 (c (n "fang_oost") (v "0.13.5") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "0y94wir75xnxn9yw6vd2l0xasz833pgld4acixcsamll7dq9v93q")))

(define-public crate-fang_oost-0.13.6 (c (n "fang_oost") (v "0.13.6") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "138lkgdcq908wkybzgm15yg9hyf1a14p023qrywhqcj4zas5dx70")))

(define-public crate-fang_oost-0.13.7 (c (n "fang_oost") (v "0.13.7") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "1wj9brqm9asmy25m2brlx7r0cfkrzndv13mizyii5m8h8d7zq2bl")))

(define-public crate-fang_oost-0.13.8 (c (n "fang_oost") (v "0.13.8") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "114dkj7a5azsw0v02qwglxlr7hndfiapmgjsps3mqs73rcy5llgs")))

(define-public crate-fang_oost-0.14.0 (c (n "fang_oost") (v "0.14.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "089rh76cywvz9abky4dlxfd0sj40cjfka368aq9bl3fdg63krcyx")))

(define-public crate-fang_oost-0.14.2 (c (n "fang_oost") (v "0.14.2") (d (list (d (n "approx") (r ">=0.2.0, <0.3.0") (d #t) (k 2)) (d (n "criterion") (r ">=0.2.0, <0.3.0") (d #t) (k 2)) (d (n "num") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "num-complex") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "rayon") (r ">=1.5.0, <2.0.0") (d #t) (k 0)) (d (n "statrs") (r ">=0.7.0, <0.8.0") (d #t) (k 2)))) (h "1i3npsl7li9cs677qwsi9pjll89d4b4j2zanhgiqaxd0cjlrczri")))

(define-public crate-fang_oost-0.15.0 (c (n "fang_oost") (v "0.15.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "0czmpn7w7c2kd4x88gfi4l6gpf2f2zn3q56bbqxadf3g7aibfmsc")))

(define-public crate-fang_oost-0.15.1 (c (n "fang_oost") (v "0.15.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "statrs") (r "^0.7.0") (d #t) (k 2)))) (h "1kyphhkhv0jlfw90rvls92cqfn22ldqknimbalghjpf9pyvf062w")))

