(define-module (crates-io fa nn fann) #:use-module (crates-io))

(define-public crate-fann-0.0.1 (c (n "fann") (v "0.0.1") (d (list (d (n "fann-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1kmh9fzxd261am1hh512d1sydwhklyrfdp50znknwainwli16yqx")))

(define-public crate-fann-0.0.2 (c (n "fann") (v "0.0.2") (d (list (d (n "fann-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1h25zj0swslpwvr64id33y0nmjai8h7r768wcc4pg32bbpb0p9jw")))

(define-public crate-fann-0.0.3 (c (n "fann") (v "0.0.3") (d (list (d (n "fann-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0z9l7zmj7ww8vwjywl29kqz8pwdgw5v7drbj19bvbmb73k4l35sq")))

(define-public crate-fann-0.1.0 (c (n "fann") (v "0.1.0") (d (list (d (n "fann-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0irgp8gsifqd0mcmzil52588nagk7kfvh667lmiqi93r2l6l4rvc")))

(define-public crate-fann-0.1.1 (c (n "fann") (v "0.1.1") (d (list (d (n "fann-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "003y0vhz16xlxlvpf6g8l7qlcjngamahk0b22ybfjh4wx1jwji4k")))

(define-public crate-fann-0.1.2 (c (n "fann") (v "0.1.2") (d (list (d (n "fann-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0n8z7pg9wzj8vg5vm1wg2psn8d082a3fq5l5c4fzyp4mvqwkckpq")))

(define-public crate-fann-0.1.3 (c (n "fann") (v "0.1.3") (d (list (d (n "fann-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "13kln8mj261gdgg769cng0f6amgw4kby61qc19mj5r9xaj3v4p97") (f (quote (("double" "fann-sys/double"))))))

(define-public crate-fann-0.1.5 (c (n "fann") (v "0.1.5") (d (list (d (n "fann-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "0.1.*") (d #t) (k 0)))) (h "1yx7iaf7hwdhiwmyryy5dw1zb0wnxqk9bm6l9mzivpqx1dx6bcbl") (f (quote (("double" "fann-sys/double"))))))

(define-public crate-fann-0.1.6 (c (n "fann") (v "0.1.6") (d (list (d (n "fann-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0imx4pbqnbhp7hnnk35wrp83b45v2q085gxdzn4ack8kjah262pi") (f (quote (("double" "fann-sys/double"))))))

(define-public crate-fann-0.1.7 (c (n "fann") (v "0.1.7") (d (list (d (n "fann-sys") (r "^0.1.7") (d #t) (k 0)) (d (n "libc") (r "~0.2.20") (d #t) (k 0)))) (h "1dhwql727n3wlcp7khwik77q4sgx2rir57q94n566gw0wcyjwk2h") (f (quote (("double" "fann-sys/double"))))))

(define-public crate-fann-0.1.8 (c (n "fann") (v "0.1.8") (d (list (d (n "fann-sys") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "~0.2.20") (d #t) (k 0)))) (h "0nrkixmq10q8mypqliv3q1xpiqjmf9gilfpapjs0aa2xpgwgf9rh") (f (quote (("double" "fann-sys/double"))))))

