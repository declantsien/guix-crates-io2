(define-module (crates-io fa nn fann-sys) #:use-module (crates-io))

(define-public crate-fann-sys-0.1.0 (c (n "fann-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0zb1ryzs2qj68vd8m6xf396374mrwwzz8l4d6a58bpch34rv4nbv")))

(define-public crate-fann-sys-0.1.1 (c (n "fann-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0dyi1xrz49z3lx856khkkryxb5fkkhp5zl2d0qjhl0glvjg4bm3g")))

(define-public crate-fann-sys-0.1.2 (c (n "fann-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "128fz1x4kkpqaad0rf968v819qwcl0fd9cd87fxq309whgf5k4x0")))

(define-public crate-fann-sys-0.1.3 (c (n "fann-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1x6fy2gi42gdmnqql9d3fdz9myi2cm6ixsl4hv7ckadvmiq4q7rh")))

(define-public crate-fann-sys-0.1.4 (c (n "fann-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0nm2ckd3r6iyg9irfgng07fn8smik8wy7kk693brxgd6va6r0s85") (f (quote (("double"))))))

(define-public crate-fann-sys-0.1.5 (c (n "fann-sys") (v "0.1.5") (d (list (d (n "libc") (r "0.1.*") (d #t) (k 0)))) (h "02ijp3jfbkhc5gcg4zgk67gi6baarlrqg0h3mbc1y2ccl5r9fynk") (f (quote (("double"))))))

(define-public crate-fann-sys-0.1.6 (c (n "fann-sys") (v "0.1.6") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0r3ifv3damjjbnrc0vmwyqsg77akg2k22ckp3gpwzfp3v6z2n4yg") (f (quote (("double"))))))

(define-public crate-fann-sys-0.1.7 (c (n "fann-sys") (v "0.1.7") (d (list (d (n "libc") (r "~0.2.20") (d #t) (k 0)))) (h "1byj3g5iy7q3glffp2zfzwihazc3xppk1zfvsbp9zwl5y9642yab") (f (quote (("double"))))))

(define-public crate-fann-sys-0.1.8 (c (n "fann-sys") (v "0.1.8") (d (list (d (n "libc") (r "~0.2.20") (d #t) (k 0)))) (h "1v9rvslssqbk7xzyi6zqy2b2xaxsrisjiiy9pjdm73dx58fhnxrz") (f (quote (("double"))))))

