(define-module (crates-io fa te fates_macro) #:use-module (crates-io))

(define-public crate-fates_macro-0.1.0 (c (n "fates_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full"))) (d #t) (k 0)))) (h "16m1afl4k92m8537injik1afyjj6gs27hrs082vrr7ch8c426sq0") (y #t)))

(define-public crate-fates_macro-0.1.1 (c (n "fates_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full" "fold" "visit-mut"))) (d #t) (k 0)))) (h "1i7qdiqv8zx8flcnp416ysj8yvfdq4vnn8gfqfl6hz9ga4zmz5i3") (y #t)))

(define-public crate-fates_macro-0.1.2 (c (n "fates_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full" "fold" "visit-mut"))) (d #t) (k 0)))) (h "0babgmwa6z5zyxm4z6yrv3cd0d2yzxvknd7c2y883h270xm9jids") (y #t)))

(define-public crate-fates_macro-0.1.3 (c (n "fates_macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full" "fold" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1i9kjd2byhbh21rzqn39zdz9wds81k6favycy5h5k3ynnxb1q9bq") (y #t)))

(define-public crate-fates_macro-0.1.4 (c (n "fates_macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full" "fold" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "175q16hjwhmcvhncsxf0dfxyglyz6y34a2mqxhyvlnqcxrsbgyy1") (y #t)))

(define-public crate-fates_macro-0.1.6 (c (n "fates_macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full" "fold" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "17hz1m6mg2ww25dfqxjhb6rybhag82psw1x7fcyvdcklm0srdzq6")))

(define-public crate-fates_macro-0.1.7 (c (n "fates_macro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full" "fold" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1ch76v6817ylavwqh6jh0k5ssi72426w0gk4nrl9ks6fzk868fsm")))

(define-public crate-fates_macro-0.1.8 (c (n "fates_macro") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0h8v7xanvj8v8rs1xwhs1pal3zr2rdi3si39r1ps1awa9c8nndcg")))

(define-public crate-fates_macro-0.1.9 (c (n "fates_macro") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1ib2lxwrmdp2m59lixy5xdi4pq9dw06ggi6y5402240bhwnx21zj")))

