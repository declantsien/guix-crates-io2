(define-module (crates-io fa te fates) #:use-module (crates-io))

(define-public crate-fates-0.1.0 (c (n "fates") (v "0.1.0") (d (list (d (n "fates_macro") (r "^0.1.0") (d #t) (k 0)))) (h "16vd5jhwvyxnhq7isn099806x1jzninx2m5050j7mla42nfd3g6g") (y #t)))

(define-public crate-fates-0.1.1 (c (n "fates") (v "0.1.1") (d (list (d (n "fates_macro") (r "^0.1.1") (d #t) (k 0)))) (h "1mqyw1a5g4gc3yadkqm68010nvnphh6263px6rdfv38lqck74295") (y #t)))

(define-public crate-fates-0.1.2 (c (n "fates") (v "0.1.2") (d (list (d (n "fates_macro") (r "^0.1.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "03h2wx03xr37k69jqwp0yjs6hmmibp9hdcy7l2ks4qpyhpr7pph6") (y #t)))

(define-public crate-fates-0.1.3 (c (n "fates") (v "0.1.3") (d (list (d (n "fates_macro") (r "^0.1.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "1xb423d8z4lmlb776xqykmni15fww3m8qalf7kyrpn888sh9d7p2") (y #t)))

(define-public crate-fates-0.1.4 (c (n "fates") (v "0.1.4") (d (list (d (n "fates_macro") (r "^0.1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "0s3s7kwa6c72aidwbnp7lf4x1kbkl8i9pdsdprzna6hwladfrihs") (y #t)))

(define-public crate-fates-0.1.5 (c (n "fates") (v "0.1.5") (d (list (d (n "fates_macro") (r "^0.1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "1qnxj7h4garw3kaxza9i63ldkn42cb816lbhixan9x946dl70z3c") (y #t)))

(define-public crate-fates-0.1.6 (c (n "fates") (v "0.1.6") (d (list (d (n "fates_macro") (r "^0.1.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "1kfl3midk5z6snkzpnl7adz3i5qzg4wnabp3fixpwb9jm68w4p8y") (y #t)))

(define-public crate-fates-0.1.7 (c (n "fates") (v "0.1.7") (d (list (d (n "fates_macro") (r "^0.1.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "184q9n66cvm6blc27qpv3fnnb6h6xrmsja3i3i8ahw4qim0msa16")))

(define-public crate-fates-0.1.8 (c (n "fates") (v "0.1.8") (d (list (d (n "fates_macro") (r "^0.1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0y5nan2fc15zc7zlx4g96qapnf0g7xb8c9fqkd794vg8g9jnvcci")))

(define-public crate-fates-0.1.9 (c (n "fates") (v "0.1.9") (d (list (d (n "fates_macro") (r "^0.1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "05c1j9apl4c0ap2p2xh064bbmnrl8pbamzb29fm3ayag4d7095gw")))

