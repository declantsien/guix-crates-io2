(define-module (crates-io fa te fateful) #:use-module (crates-io))

(define-public crate-fateful-0.1.0 (c (n "fateful") (v "0.1.0") (h "1jjwfs3m32f12ag0zz7shbpbjf0vy9r0nw0zi42w437a51bfm23y")))

(define-public crate-fateful-0.1.1 (c (n "fateful") (v "0.1.1") (h "1vrf8wg41x1jzn4bvn32jbf1hqj3ll0hrb2702i9ssc7i6srgrv0")))

(define-public crate-fateful-0.1.2 (c (n "fateful") (v "0.1.2") (h "15mdwi9v6iicbwvwvq65gr9ykrr58k45zkl9cr9g50kjdhbcnp4f")))

