(define-module (crates-io fa br fabric) #:use-module (crates-io))

(define-public crate-fabric-0.1.0 (c (n "fabric") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "deploy-common") (r "= 0.1.0") (d #t) (k 0)) (d (n "either") (r "^1.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "032hwznlb401bxf0cprysvcx7m80bkvb77ian02vlsvk1sk1kfip")))

(define-public crate-fabric-0.1.1 (c (n "fabric") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "deploy-common") (r "= 0.1.1") (d #t) (k 0)) (d (n "either") (r "^1.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l1jw6wzp5a62yqdkwsj1k47hx02y8ka8f90qwdbcbj8l24v2fc0")))

(define-public crate-fabric-0.1.2 (c (n "fabric") (v "0.1.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "deploy-common") (r "= 0.1.2") (d #t) (k 0)) (d (n "either") (r "^1.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1565aar5x3km4sllpy47186hq117jiifzhl194kw10p4w7a0da2s")))

