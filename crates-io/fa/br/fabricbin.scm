(define-module (crates-io fa br fabricbin) #:use-module (crates-io))

(define-public crate-fabricbin-0.1.0 (c (n "fabricbin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "0kvq404qs65hf6gd611ph7nncc9wdckrvvwcszdlcf8zc6h65gq6")))

