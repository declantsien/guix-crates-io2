(define-module (crates-io fa br fabric-support-procedural) #:use-module (crates-io))

(define-public crate-fabric-support-procedural-2.0.1 (c (n "fabric-support-procedural") (v "2.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "fabric-support-procedural-tools") (r "^2.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1fx74iqbrpv1naq3s8q5df9mincj8ddfciqhwmmrb5bpy5vmwpki") (f (quote (("std") ("default" "std"))))))

