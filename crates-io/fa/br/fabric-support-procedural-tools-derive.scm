(define-module (crates-io fa br fabric-support-procedural-tools-derive) #:use-module (crates-io))

(define-public crate-fabric-support-procedural-tools-derive-2.0.1 (c (n "fabric-support-procedural-tools-derive") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("proc-macro" "full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0l9rk9al4cdzhz79cvkc5hk1f9ai3sf1h4pzxa4wzg965jwf50qc")))

