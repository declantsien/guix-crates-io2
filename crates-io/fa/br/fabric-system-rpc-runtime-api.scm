(define-module (crates-io fa br fabric-system-rpc-runtime-api) #:use-module (crates-io))

(define-public crate-fabric-system-rpc-runtime-api-2.0.0 (c (n "fabric-system-rpc-runtime-api") (v "2.0.0") (d (list (d (n "codec") (r "^2.0.1") (k 0) (p "tetsy-scale-codec")) (d (n "tp-api") (r "^2.0.2") (k 0)))) (h "13kgg9r0r7wmkp2vpnmg26yfb6qx9a88sqbaiqn8c1gvmr9bbw1k") (f (quote (("std" "tp-api/std" "codec/std") ("default" "std"))))))

