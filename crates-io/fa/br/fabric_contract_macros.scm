(define-module (crates-io fa br fabric_contract_macros) #:use-module (crates-io))

(define-public crate-fabric_contract_macros-0.1.0 (c (n "fabric_contract_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "09fg0rd8vjh4368kw7cpswwscpnyliimx0lgbgvbbl3q6mych9d1")))

(define-public crate-fabric_contract_macros-0.1.3 (c (n "fabric_contract_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "00if0zm3mai86a278hlk7az6yx56daiascnpv0pmfcii2al07v5r")))

(define-public crate-fabric_contract_macros-0.1.4 (c (n "fabric_contract_macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "131sf537h1lrri46zf4miinyhxjr9n3f8qljlk0sdaf4j2cz93sf")))

