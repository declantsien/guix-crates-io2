(define-module (crates-io fa br fabric-system) #:use-module (crates-io))

(define-public crate-fabric-system-0.0.0 (c (n "fabric-system") (v "0.0.0") (h "0jgg8x8l48v48waaq7vbrqwydfx3z19v6q7q7kp5dh1vr0adva7x") (y #t)))

(define-public crate-fabric-system-2.0.0 (c (n "fabric-system") (v "2.0.0") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "externalities") (r "^0.8.2") (d #t) (k 2)) (d (n "fabric-support") (r "^2.0.0") (k 0)) (d (n "impl-trait-for-tuples") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (k 0)) (d (n "tet-io") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)) (d (n "tp-version") (r "^2.0.2") (k 0)))) (h "013a787mqh354m9pllnlrw4rm1vbn9xnbck1v6nml2fmma1g0xf9") (f (quote (("std" "serde" "codec/std" "tet-core/std" "tetcore-std/std" "tet-io/std" "fabric-support/std" "tp-runtime/std" "tp-version/std") ("runtime-benchmarks" "tp-runtime/runtime-benchmarks" "fabric-support/runtime-benchmarks") ("default" "std"))))))

