(define-module (crates-io fa br fabric-benchmarking) #:use-module (crates-io))

(define-public crate-fabric-benchmarking-0.0.0 (c (n "fabric-benchmarking") (v "0.0.0") (h "0nbihb6c54yw1hhfwfaw5piijdnxpdsd6zsns45a1h7cff98r4s1") (y #t)))

(define-public crate-fabric-benchmarking-2.0.0 (c (n "fabric-benchmarking") (v "2.0.0") (d (list (d (n "codec") (r "^2.0.1") (k 0) (p "tetsy-scale-codec")) (d (n "fabric-support") (r "^2.0.0") (k 0)) (d (n "fabric-system") (r "^2.0.0") (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "linregress") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "tet-io") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)) (d (n "tetcore-storage") (r "^2.0.2") (k 0)) (d (n "tp-api") (r "^2.0.2") (k 0)) (d (n "tp-runtime") (r "^2.0.2") (k 0)) (d (n "tp-runtime-interface") (r "^2.0.2") (k 0)))) (h "1hjidwpd19z0s5w79l95v1c9ibz40iga79rshn9ibqnix5fxykfz") (f (quote (("std" "codec/std" "tp-runtime-interface/std" "tp-runtime/std" "tp-api/std" "tetcore-std/std" "fabric-support/std" "fabric-system/std" "linregress") ("default" "std"))))))

