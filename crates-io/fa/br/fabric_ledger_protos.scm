(define-module (crates-io fa br fabric_ledger_protos) #:use-module (crates-io))

(define-public crate-fabric_ledger_protos-0.1.0 (c (n "fabric_ledger_protos") (v "0.1.0") (d (list (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)))) (h "178bd3liiz0y9mm91hk3wqq8m1kdxjf4mck1k4v0y897v09vz4sf")))

(define-public crate-fabric_ledger_protos-0.2.0 (c (n "fabric_ledger_protos") (v "0.2.0") (d (list (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)))) (h "13169a6hz669pvk9lkqramxmkapzsksaax6f42rmc9y6jd1syvlv")))

(define-public crate-fabric_ledger_protos-0.3.0 (c (n "fabric_ledger_protos") (v "0.3.0") (d (list (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)))) (h "1z4alf7b259mz9wv5ra39x40y9ni4lyiiibwm0yh1k45347q8hp1")))

(define-public crate-fabric_ledger_protos-0.4.0 (c (n "fabric_ledger_protos") (v "0.4.0") (d (list (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)))) (h "11bflxaddgzh67195nlwfibl52ayv7vwn28fsj3sxdfa8sll0snx")))

(define-public crate-fabric_ledger_protos-0.5.0 (c (n "fabric_ledger_protos") (v "0.5.0") (d (list (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)))) (h "08zxlrpsz7g0rlb1q6wl0mq0avp0xjxj00yj0a3hn9n3jsblz727")))

(define-public crate-fabric_ledger_protos-0.6.0 (c (n "fabric_ledger_protos") (v "0.6.0") (d (list (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)))) (h "003913wlzsirc9jyi9nhjyv06vyn10d6pmwr8b49bc8lfqs5ghcq")))

(define-public crate-fabric_ledger_protos-0.7.0 (c (n "fabric_ledger_protos") (v "0.7.0") (d (list (d (n "protobuf") (r "^2.17.0") (d #t) (k 0)))) (h "1icdpfzd93fmi9cqr4zhnsbmi40rwbnif2vgx3gh4kjdk43ml93l")))

