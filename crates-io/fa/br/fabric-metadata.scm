(define-module (crates-io fa br fabric-metadata) #:use-module (crates-io))

(define-public crate-fabric-metadata-0.0.0 (c (n "fabric-metadata") (v "0.0.0") (h "0m7i02kl4r1hafa5s6yqr866d8j73yjc73irvk29xpw1a86m4lm1") (y #t)))

(define-public crate-fabric-metadata-12.0.0 (c (n "fabric-metadata") (v "12.0.0") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (k 0) (p "tetsy-scale-codec")) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (k 0)) (d (n "tetcore-std") (r "^2.0.2") (k 0)))) (h "0bvryijxdfnpzdn295i0x3x2hin9xf3b4j70gqn1kn3i9n9qx21k") (f (quote (("std" "codec/std" "tetcore-std/std" "tet-core/std" "serde") ("default" "std"))))))

