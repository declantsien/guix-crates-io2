(define-module (crates-io fa br fabric-support-procedural-tools) #:use-module (crates-io))

(define-public crate-fabric-support-procedural-tools-2.0.1 (c (n "fabric-support-procedural-tools") (v "2.0.1") (d (list (d (n "fabric-support-procedural-tools-derive") (r "^2.0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0kn60ndbdh8niacjac3yr13zy20iaqv7zm0jr98lmq1ljgsc9idr")))

