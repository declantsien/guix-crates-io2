(define-module (crates-io fa ma fama) #:use-module (crates-io))

(define-public crate-fama-0.1.0 (c (n "fama") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "1ja93mwa8h82j759234nj84cx1h4c25frc7z2nc8xqwsxa4nsg83") (r "1.70")))

(define-public crate-fama-0.1.1 (c (n "fama") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "00i6sxv9f1ca24zc1y60pdlmcky4d2j7avb94xmpxjqrjngpswxr") (r "1.70")))

(define-public crate-fama-0.1.2 (c (n "fama") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "1gmwc4zd6n3y3q2bg8zw03hvm3f7x4sw2jyidipxbzsbxh3wijq1") (r "1.70")))

(define-public crate-fama-0.1.3 (c (n "fama") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "0abnj2ravm503qffv67rr4arcwd3yrlzbccsh0qca3b627a37x3n") (r "1.70")))

(define-public crate-fama-0.1.4 (c (n "fama") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "0jrjjia3105pglilmib08khw9am4as9czv0yl0i4wg8ccpb8xkdb") (r "1.70")))

(define-public crate-fama-0.2.0 (c (n "fama") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "busybody") (r "^0.2.6") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "05klj1rq7kqn640x8ijwc1hzvlyrfxsdsfq3jx8z72jzvdbdrzvq") (r "1.70")))

(define-public crate-fama-0.2.1 (c (n "fama") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "busybody") (r "^0.2.6") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "024mi7jizrbyzi6zmll0ic0jprbr1v1a9whjyhcpbswciirwjg41") (r "1.70")))

(define-public crate-fama-0.2.2 (c (n "fama") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "busybody") (r "^0.2.6") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "0g5i8d0bbpja70i7qd3ijzf9bcpb6bj7fl9ca5fzazmh4q5y4iid") (r "1.70")))

(define-public crate-fama-0.3.0 (c (n "fama") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "busybody") (r "^0.2.6") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "1ik1518i8wbkxdlnfj2jj8r82pqam1jaqwljmwxb40i8hhwpp93b") (r "1.70")))

(define-public crate-fama-0.3.1 (c (n "fama") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "busybody") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "1w7xvc7qrpr5kfg2gn9ljjgpcxbcrbq16qp6sbj44scr693mvq9c") (r "1.70")))

(define-public crate-fama-0.3.2 (c (n "fama") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "busybody") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "1pkff680xlgchb5x8h0q6l5avlmg2ywzlwqkxfpg1hrlykc1rsbg") (r "1.70")))

(define-public crate-fama-0.3.3 (c (n "fama") (v "0.3.3") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "busybody") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "0l7si49sff967pib3v5xij1cpx663dk06rl8p19kzpxwknnh81zh") (r "1.70")))

(define-public crate-fama-0.3.4 (c (n "fama") (v "0.3.4") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "busybody") (r "^0.3.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "15zqn9mbwmnzgcwfkl3klan612cpv7zq8k6z4j2yhqnixms7qir2") (r "1.70")))

(define-public crate-fama-0.3.5 (c (n "fama") (v "0.3.5") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "busybody") (r "^0.3.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "0r9j4rdkl03xaca0lcxnqbbmqkqd0ja6kzhpb7dm663xvckl6ibg") (r "1.70")))

(define-public crate-fama-0.3.6 (c (n "fama") (v "0.3.6") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "busybody") (r "^0.3.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "1b6r0x759ar5zvs77161kk91cc2mj6b121fvlx8xhrkgx5cl1xyf") (r "1.70")))

