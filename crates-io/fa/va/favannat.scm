(define-module (crates-io fa va favannat) #:use-module (crates-io))

(define-public crate-favannat-0.1.1 (c (n "favannat") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "145vwnb4iadhwn21pzpnpyk9sddb46ybjfiwwaf00qnr448324yg")))

(define-public crate-favannat-0.1.2 (c (n "favannat") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "0zzkj3ckqk17l5hffnzaqnkv531314f6365h24wr4av7x15cn4l4")))

(define-public crate-favannat-0.2.0 (c (n "favannat") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "0wmgpfg0263zsn8y7ll911q06wi332598cz8x8jq5if2p5a1hd4k")))

(define-public crate-favannat-0.3.0 (c (n "favannat") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.28.0") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.4") (d #t) (k 0)))) (h "1znlw6r1chfn1pi4m28ff5p1qrrs9x4p3c96qfj4b3vlvk113ypx")))

(define-public crate-favannat-0.4.0 (c (n "favannat") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.28.0") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.4") (d #t) (k 0)))) (h "140wqban86d1ff1p7n3i75zb9jzkx3yc1vbyibskwyi8pcyjfgxp")))

(define-public crate-favannat-0.4.1 (c (n "favannat") (v "0.4.1") (d (list (d (n "nalgebra") (r "^0.28.0") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.4") (d #t) (k 0)))) (h "19s5380qmp38cgf2yy3jgh2asp3sv136ms55aqy4x3d6sh1cqy82")))

(define-public crate-favannat-0.5.0 (c (n "favannat") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.28.0") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1v1dx6xwgf69wnv8pbx1xpgm4p55q0iglc0ms4c16hrqgf1gk5ar")))

(define-public crate-favannat-0.5.1 (c (n "favannat") (v "0.5.1") (d (list (d (n "nalgebra") (r "^0.28.0") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)))) (h "04vspni32zrk2rkdvkk3z9qc9cb90vpbzr94iip9x0bdmkwnqx9r")))

(define-public crate-favannat-0.5.2 (c (n "favannat") (v "0.5.2") (d (list (d (n "nalgebra") (r "^0.28.0") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)))) (h "17d11kv6mxckm7nvw2v65m2gxyma2l4w8dnlh7nnqxh6lfdx7mnz")))

(define-public crate-favannat-0.6.0 (c (n "favannat") (v "0.6.0") (d (list (d (n "nalgebra") (r "^0.28.0") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)))) (h "10pr7bzgg3l0grs7ipypdks3nr3q95jsibbqadzm43h1kn0llnq7")))

(define-public crate-favannat-0.6.1 (c (n "favannat") (v "0.6.1") (d (list (d (n "nalgebra") (r "^0.28.0") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1fi8rld5xxczk1kqn2vmmrqs5bybkqwi5ispqa4p0xx6jqd0q8xy")))

(define-public crate-favannat-0.6.2 (c (n "favannat") (v "0.6.2") (d (list (d (n "nalgebra") (r "^0.28.0") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)))) (h "064qk4xirkhja8bfnikjdphxh7kp8ln09plm500mfl44j79qkfjr")))

(define-public crate-favannat-0.6.3 (c (n "favannat") (v "0.6.3") (d (list (d (n "nalgebra") (r "^0.28.0") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)))) (h "171fgmn8zh2iadbk90bv7ic0qwhy4c452vfj6ygqd1f8wl95dxlw")))

(define-public crate-favannat-0.6.4 (c (n "favannat") (v "0.6.4") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0c21869anfb3gswlnz8hn2x6wfwidzh35hk69k2d3qa1sagrixfn")))

