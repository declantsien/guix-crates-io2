(define-module (crates-io fa ct factbird-common) #:use-module (crates-io))

(define-public crate-factbird-common-0.1.0 (c (n "factbird-common") (v "0.1.0") (d (list (d (n "defmt") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)))) (h "0mac1wkkdiah3pssnvy88i5j8bl3yis4k8c06q57fw55j7ypingr") (f (quote (("V1_1"))))))

(define-public crate-factbird-common-0.2.0 (c (n "factbird-common") (v "0.2.0") (d (list (d (n "defmt") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)))) (h "1mnlr1r1hs558xpzcphbbif6q2qgcrp3spd46g6n7hivxr4phgzv") (f (quote (("firmware") ("default" "firmware") ("V1_1"))))))

(define-public crate-factbird-common-0.2.1 (c (n "factbird-common") (v "0.2.1") (d (list (d (n "defmt") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)))) (h "0rll8np7l3bq855ic4wh3fxznqmr0bhkk6wlqg61fd0062nscl86") (f (quote (("firmware") ("V1_1"))))))

