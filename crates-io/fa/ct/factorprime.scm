(define-module (crates-io fa ct factorprime) #:use-module (crates-io))

(define-public crate-factorprime-0.1.1 (c (n "factorprime") (v "0.1.1") (h "12yc0py6ngsysdpr3aaxq845gpdkp18r67my4qs8nyigli4p3hh2")))

(define-public crate-factorprime-0.1.2 (c (n "factorprime") (v "0.1.2") (h "1mw1pgibb10grz08n5qrvzj6b5xp59d329nklqjnx117wy7qzlyf")))

(define-public crate-factorprime-0.1.3 (c (n "factorprime") (v "0.1.3") (h "15hwhg59ii75ffqxxpyrni8plgx1iw8fff4gnr9vailpj581lsqj")))

(define-public crate-factorprime-0.1.4 (c (n "factorprime") (v "0.1.4") (h "105xcz9cxqwanrv8gnbs9z0cbjqpfksil86r0njach5gb8bn5j6f")))

