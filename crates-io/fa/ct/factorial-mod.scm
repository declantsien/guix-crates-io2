(define-module (crates-io fa ct factorial-mod) #:use-module (crates-io))

(define-public crate-factorial-mod-0.1.0 (c (n "factorial-mod") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "04rrnz8qzkydrav96r7q46jnw2qs6q43fz1gzgb4bbjg8w357ar2") (f (quote (("default"))))))

