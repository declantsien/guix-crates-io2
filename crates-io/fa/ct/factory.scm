(define-module (crates-io fa ct factory) #:use-module (crates-io))

(define-public crate-factory-0.1.0 (c (n "factory") (v "0.1.0") (d (list (d (n "atomic_immut") (r "^0.1") (o #t) (d #t) (k 0)))) (h "07fk0i86mjir9bmrlxqwfjf3armhp7zn1i5xd7s8m349z2dy4nmr") (f (quote (("swappable" "atomic_immut"))))))

(define-public crate-factory-0.1.1 (c (n "factory") (v "0.1.1") (d (list (d (n "atomic_immut") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1png9b9178klb4k8mxpn7hfsivdw1nnygn8g403wbxygw76mq0qj") (f (quote (("swappable" "atomic_immut"))))))

(define-public crate-factory-0.1.2 (c (n "factory") (v "0.1.2") (d (list (d (n "atomic_immut") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0b4vpj5jd2m7c6ff84pfabk8rd2ixqr3kfnqpq8xsr69bag5g8vk") (f (quote (("swappable" "atomic_immut"))))))

