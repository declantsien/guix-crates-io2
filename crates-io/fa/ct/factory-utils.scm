(define-module (crates-io fa ct factory-utils) #:use-module (crates-io))

(define-public crate-factory-utils-0.1.0 (c (n "factory-utils") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.3.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "terp721") (r "^0.1.0") (f (quote ("library"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "118g5bc26hz0xy3ncccac75ambsh6gadz5kszn4hspfhlvyanrsh")))

