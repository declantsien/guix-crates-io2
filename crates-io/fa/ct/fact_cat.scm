(define-module (crates-io fa ct fact_cat) #:use-module (crates-io))

(define-public crate-fact_cat-0.1.0 (c (n "fact_cat") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1w6f5r2fysz9lc63gqaysi49r1y0bjsh746d1dz746sfwn121aa2")))

