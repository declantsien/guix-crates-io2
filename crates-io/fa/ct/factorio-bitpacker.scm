(define-module (crates-io fa ct factorio-bitpacker) #:use-module (crates-io))

(define-public crate-factorio-bitpacker-0.1.0 (c (n "factorio-bitpacker") (v "0.1.0") (d (list (d (n "factorio-blueprint") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "noisy_float") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0infj8lrf3710b1lfdfm9qw7h9axdxp31v442d3fc8alzyj7845p")))

(define-public crate-factorio-bitpacker-0.1.1 (c (n "factorio-bitpacker") (v "0.1.1") (d (list (d (n "factorio-blueprint") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "noisy_float") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "00v6kyrvhfpp3wmm548i8nb0g465bp1w2qj64zdm7j50bhy063d4")))

(define-public crate-factorio-bitpacker-0.1.2 (c (n "factorio-bitpacker") (v "0.1.2") (d (list (d (n "factorio-blueprint") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "noisy_float") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "056bjynwjv960ralgmrcxprjpa97wi47d3finj1058lxjx5kxpdz")))

