(define-module (crates-io fa ct factor-prime) #:use-module (crates-io))

(define-public crate-factor-prime-0.1.0 (c (n "factor-prime") (v "0.1.0") (h "13pqgalw3p0f34kkpl9i4cpvi5r70mqb7l7lz16xn26iwb4dybgi")))

(define-public crate-factor-prime-0.1.1 (c (n "factor-prime") (v "0.1.1") (h "15b1wvgxy9376dgnqvmansg24af76hr7dkflgkg639x1i241fykd")))

