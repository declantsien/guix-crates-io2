(define-module (crates-io fa ct factorio-settings) #:use-module (crates-io))

(define-public crate-factorio-settings-1.0.0 (c (n "factorio-settings") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "indexmap") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1iq8qvambv8vs1la4n52sgl2rsmx99mq05vwidwwgnrcr7mk4m6g")))

