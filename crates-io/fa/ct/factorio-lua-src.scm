(define-module (crates-io fa ct factorio-lua-src) #:use-module (crates-io))

(define-public crate-factorio-lua-src-544.0.1 (c (n "factorio-lua-src") (v "544.0.1") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "0f8kqk48ldhd9xrda3i8radpzvcabb4viylcfwb17cky25nz32pf") (f (quote (("ucid"))))))

(define-public crate-factorio-lua-src-546.0.0 (c (n "factorio-lua-src") (v "546.0.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 0)))) (h "1gvr47zczxks8103sgdys0s426v38kyf09mddf0qpp2g32rqy7a4") (f (quote (("ucid"))))))

