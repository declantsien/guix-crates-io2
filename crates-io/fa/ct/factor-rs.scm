(define-module (crates-io fa ct factor-rs) #:use-module (crates-io))

(define-public crate-factor-rs-1.2.0 (c (n "factor-rs") (v "1.2.0") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "prime_factorization") (r "^1.0.3") (d #t) (k 0)) (d (n "radixal") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0mh9a7s395srj31n5rfmads6d8xjc9s9nwdrz7xh7jhjax6nmap9")))

(define-public crate-factor-rs-1.2.1 (c (n "factor-rs") (v "1.2.1") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "prime_factorization") (r "^1.0.3") (d #t) (k 0)) (d (n "radixal") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1csb3fr0mp9dqf7hzcpviyscv59vg7dyyyyp0bq41ffcas1w4xza")))

