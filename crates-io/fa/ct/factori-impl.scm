(define-module (crates-io fa ct factori-impl) #:use-module (crates-io))

(define-public crate-factori-impl-0.1.0 (c (n "factori-impl") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro-rules") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0r6jbgfrlf1447p1lc2pr04w38gx2dqyhcn4c6cpcik67gp02fhi")))

(define-public crate-factori-impl-0.1.1 (c (n "factori-impl") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro-rules") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1sp02gwgj09yxbx3226bcz1dnykxk7rgnvn5qvk2jg94x05j8cqn")))

(define-public crate-factori-impl-1.0.0 (c (n "factori-impl") (v "1.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro-rules") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1bpmv1kmmfhr5a3jp0084wbw3bda0w21xs7gjrnwpladpjy59dim")))

(define-public crate-factori-impl-1.1.0 (c (n "factori-impl") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mmx3p5aq3c1jmi5schx800jx8gjgwpn65m8j0fll2ibv7g48qvd")))

