(define-module (crates-io fa ct factorio-bp) #:use-module (crates-io))

(define-public crate-factorio-bp-0.1.0 (c (n "factorio-bp") (v "0.1.0") (h "14pgfmkhkzwirbj2i86vrvx30jc97s01mcf5vm2g4vcl3pkvlj9d") (y #t)))

(define-public crate-factorio-bp-0.1.1 (c (n "factorio-bp") (v "0.1.1") (h "1m6v7g7ind2shrdyvli7wpks9fks96wr97kdxjca4dx1mbz8fvif") (y #t)))

