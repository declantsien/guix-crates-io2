(define-module (crates-io fa ct factorial) #:use-module (crates-io))

(define-public crate-factorial-0.1.0 (c (n "factorial") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0py0g3n7qg1rqg1kqrggggyxygf2614gxjkqmnqf6zyg1zdcj4f1")))

(define-public crate-factorial-0.1.1 (c (n "factorial") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1q2bfjmlzbyzpnn7pxnh0ch5ghdkja32j7l8wnl7kccdw4lksmvd")))

(define-public crate-factorial-0.2.0 (c (n "factorial") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1pj9bnwf6bjhdi3m5bxc5kqks37gvdaffd8r2xxpkw0a34p6sfa2")))

(define-public crate-factorial-0.2.1 (c (n "factorial") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "070bk5pbncnfmpslfdidgs252fj6kk4nk2kb2l2qfxn56ff23633")))

(define-public crate-factorial-0.3.0 (c (n "factorial") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.3.5") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.3.5") (d #t) (k 1)))) (h "0jj1rd4s8yvpp32y8c9sbc372484ihfcnbz1yavgbb8di9dg73fi") (y #t)))

(define-public crate-factorial-0.4.0 (c (n "factorial") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.3.6") (d #t) (k 0)))) (h "0jvh2pr2aq2p95bkyqg0d0kzad4r0gcimbvcrl9rla64ac3lr7cx")))

