(define-module (crates-io fa ct facts) #:use-module (crates-io))

(define-public crate-facts-0.1.0 (c (n "facts") (v "0.1.0") (h "1zcj7caxvr50hwdh54alm8zxkg361ywbrya5r772983cngxidf4l")))

(define-public crate-facts-0.1.1 (c (n "facts") (v "0.1.1") (h "1nl5ri1vnc9f4n97lw3pl9avhvin9szwx7v8m4rfmbvflrl8y572")))

