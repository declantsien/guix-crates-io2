(define-module (crates-io fa ct factorial-zeros) #:use-module (crates-io))

(define-public crate-factorial-zeros-0.1.0 (c (n "factorial-zeros") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0lfwcvzsfwjjgn1iq04cy9jhxrvn1j9n5y9jyh16x6jn3hdj8kgl") (f (quote (("default"))))))

