(define-module (crates-io fa ct factor) #:use-module (crates-io))

(define-public crate-factor-0.1.0 (c (n "factor") (v "0.1.0") (h "0175d8q870zh97iyzhkhsjb94gvpsjy11dhmnj1z5mcm1m9xpr40")))

(define-public crate-factor-0.2.0 (c (n "factor") (v "0.2.0") (h "1ifnr1jsll3h94vwjy6mlg0h1nl8hhm3h6hdzlcfzg4kas4snj2v")))

(define-public crate-factor-0.3.0 (c (n "factor") (v "0.3.0") (h "1hznsmn5q2kv68sm8wadwx1kvvas8ck3w89b0cxlnxnmmqyl4sib")))

(define-public crate-factor-0.4.0 (c (n "factor") (v "0.4.0") (h "1prhc3xs8p7b61nqqljhgpm8jq2ig8v35nglqmfxi72sflwjybhh")))

