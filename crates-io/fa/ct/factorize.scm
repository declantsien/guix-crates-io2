(define-module (crates-io fa ct factorize) #:use-module (crates-io))

(define-public crate-factorize-0.0.0 (c (n "factorize") (v "0.0.0") (d (list (d (n "num-prime") (r "^0.3.1") (d #t) (k 0)))) (h "1kml2bbknn9kqxqli6gisnvm62v9x1w96q2w6b4bz9lif69wypxv")))

(define-public crate-factorize-0.1.0 (c (n "factorize") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-prime") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "06k3bingc46lsy6b6sd25n31nrxsx1yyvw7435wbhmrnnbd4x1kb")))

(define-public crate-factorize-0.1.1 (c (n "factorize") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-prime") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1dw9c93zdsy65r76rgb4kcaf4vmnj064j4r5avpg2pwcg7mg75rb")))

(define-public crate-factorize-0.1.2 (c (n "factorize") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-prime") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "01vky9a6g99h7gzdk7w7hallyz3w8b89xp0p4pnq180vw1c1zyr9")))

