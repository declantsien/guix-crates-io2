(define-module (crates-io fa ct factoryizer) #:use-module (crates-io))

(define-public crate-factoryizer-0.1.0 (c (n "factoryizer") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (d #t) (k 0)))) (h "0mg8ki5j922xh0i9dwrlfi0r2m3k9g7inzxq62ydmy0ixp8ia5my")))

(define-public crate-factoryizer-0.2.0 (c (n "factoryizer") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (d #t) (k 0)))) (h "0b6cc74fkxskwiy6x7qq1v3ma77n49xr7vcikls38n6v3c47a2bj")))

