(define-module (crates-io fa ct facto) #:use-module (crates-io))

(define-public crate-facto-0.1.0 (c (n "facto") (v "0.1.0") (d (list (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "redc") (r "^0.1.0") (d #t) (k 0)) (d (n "rug") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "twoword") (r "^0.1.0") (d #t) (k 0)))) (h "1nfk7l6dca88lyivf2xy2xwx8wmlazxbrnqfr387iw90jzjyn4f9")))

(define-public crate-facto-0.2.0 (c (n "facto") (v "0.2.0") (d (list (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "redc") (r "^0.1.0") (d #t) (k 0)) (d (n "rug") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "twoword") (r "^0.1.0") (d #t) (k 0)))) (h "0md8lfrjqpn9ahcr1nfz64mr2wsi627va3rgs08bf3kb8qwb9hb6")))

(define-public crate-facto-0.3.0 (c (n "facto") (v "0.3.0") (d (list (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "redc") (r "^0.1.0") (d #t) (k 0)) (d (n "rug") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "twoword") (r "^0.1.0") (d #t) (k 0)))) (h "03zvawnll0bvmjkk0cqxb1wfnsx5ri516mj35gflz3qyggvhc1pa")))

(define-public crate-facto-0.3.1 (c (n "facto") (v "0.3.1") (d (list (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "redc") (r "^0.1.0") (d #t) (k 0)) (d (n "rug") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "twoword") (r "^0.1.0") (d #t) (k 0)))) (h "1pwqbb2vr3qxahqr1is5bgj1s12rdw8i8h15xgq74m0ydxdj24y2")))

