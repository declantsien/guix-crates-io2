(define-module (crates-io fa ct factorialfunction) #:use-module (crates-io))

(define-public crate-factorialfunction-0.1.0 (c (n "factorialfunction") (v "0.1.0") (h "00k9a6rwj5jhfwz6v2gq6cl0hpvrmi70ds9ia5dd7h5d8fxqmy5d")))

(define-public crate-factorialfunction-0.1.1 (c (n "factorialfunction") (v "0.1.1") (h "0czhvifs44i6v1kn193ildsr82ww8dfhnddw0aynsmmhqx1gz8fw")))

