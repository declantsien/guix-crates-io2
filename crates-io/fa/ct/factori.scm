(define-module (crates-io fa ct factori) #:use-module (crates-io))

(define-public crate-factori-0.0.1 (c (n "factori") (v "0.0.1") (d (list (d (n "mashup") (r "^0.1") (d #t) (k 0)))) (h "0khprk9slkk419lvl8lcqpkwhfb46nd9h10ysa2syiwjzxach9wl")))

(define-public crate-factori-0.0.2 (c (n "factori") (v "0.0.2") (d (list (d (n "mashup") (r "^0.1") (d #t) (k 0)))) (h "1pmcjqkszm5k2l41aasp9am3lnxf5bgymv7i96d16y6nfxyhk2my")))

(define-public crate-factori-0.0.3 (c (n "factori") (v "0.0.3") (d (list (d (n "mashup") (r "^0.1") (d #t) (k 0)))) (h "014shc7d9828swayx1njnk3zf6zglsxihp6l9nddin45nv60n1a8")))

(define-public crate-factori-0.1.0 (c (n "factori") (v "0.1.0") (d (list (d (n "factori-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro-nested") (r "^0.1") (d #t) (k 0)))) (h "14xa5wjy7z27cnpk5x9nkajsz0gd7q8sb749m92z8zm2sqzisnac")))

(define-public crate-factori-0.1.1 (c (n "factori") (v "0.1.1") (d (list (d (n "factori-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro-nested") (r "^0.1") (d #t) (k 0)))) (h "1ypjbmhvjw5rzhi72c366d76g5q835ziy8mjzx167g7vzzb4vqsa")))

(define-public crate-factori-1.0.0 (c (n "factori") (v "1.0.0") (d (list (d (n "factori-impl") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro-nested") (r "^0.1") (d #t) (k 0)))) (h "17a3ira9livqbydqr2mi670pdcwzpmik1bshlfrfh3j6b75zqnk2")))

(define-public crate-factori-1.1.0 (c (n "factori") (v "1.1.0") (d (list (d (n "factori-impl") (r "^1.1.0") (d #t) (k 0)))) (h "0c8bg9rnrbzabsmfhx93ar08rhzrinmcdqax2h6577k02w4vbxjg")))

