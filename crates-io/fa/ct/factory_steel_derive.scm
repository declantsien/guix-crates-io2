(define-module (crates-io fa ct factory_steel_derive) #:use-module (crates-io))

(define-public crate-factory_steel_derive-0.1.0 (c (n "factory_steel_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1al7b1yqzc7gs3k3c6kpk7pmahz2kr5zjkw0vb70ylla9l50gsfm")))

