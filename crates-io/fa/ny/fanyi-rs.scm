(define-module (crates-io fa ny fanyi-rs) #:use-module (crates-io))

(define-public crate-fanyi-rs-0.1.0 (c (n "fanyi-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0m7vw13w5n4lkq39bl3kc0hn3zaazjkyp0gwfxv7wfxhl1rfdgks")))

