(define-module (crates-io fa rm farmfe_testing) #:use-module (crates-io))

(define-public crate-farmfe_testing-0.0.1 (c (n "farmfe_testing") (v "0.0.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0ya0f9g97v1bc0izf6gi2fr5bk9yia9zbjjmmq3bpacf1qf583fh")))

