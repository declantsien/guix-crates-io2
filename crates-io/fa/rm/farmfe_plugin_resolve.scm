(define-module (crates-io fa rm farmfe_plugin_resolve) #:use-module (crates-io))

(define-public crate-farmfe_plugin_resolve-0.0.1 (c (n "farmfe_plugin_resolve") (v "0.0.1") (d (list (d (n "farmfe_core") (r "^0.4.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0af740zi6sizqdynjgzqjhhhl208zxrl8dm0pjqy4311qykd2wlc") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_resolve-0.0.2 (c (n "farmfe_plugin_resolve") (v "0.0.2") (d (list (d (n "farmfe_core") (r "^0.4.2") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "10xirf7r4fkkylmigxn7bbr7hvzj2rj4hqplkzq2x6dcffi2pqxw") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_resolve-0.0.3 (c (n "farmfe_plugin_resolve") (v "0.0.3") (d (list (d (n "farmfe_core") (r "^0.4.4") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.5") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.4") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "191ji8g0s83dl31mncqb560hvxch4k7fjpckma5pgnz8s6aj0g07") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_resolve-0.0.4 (c (n "farmfe_plugin_resolve") (v "0.0.4") (d (list (d (n "farmfe_core") (r "^0.4.5") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "0zl5n678rd28gc0s8wfiwx4ppvy40kv4khh9vkawbyjfdlrlzm9i") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_resolve-0.0.5 (c (n "farmfe_plugin_resolve") (v "0.0.5") (d (list (d (n "farmfe_core") (r "^0.5.0") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "0fzgsa4kaw04s0pqd4bpysqin5r0mgjqggkgm6d5phz2yacj1irc") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_resolve-0.0.6 (c (n "farmfe_plugin_resolve") (v "0.0.6") (d (list (d (n "farmfe_core") (r "^0.5.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "1mrp1anf95jx373zxg6ahvr0g225znjj15g83ih45aqj6nzylzbg") (f (quote (("profile" "farmfe_core/profile"))))))

