(define-module (crates-io fa rm farmfe_utils) #:use-module (crates-io))

(define-public crate-farmfe_utils-0.0.1 (c (n "farmfe_utils") (v "0.0.1") (d (list (d (n "pathdiff") (r "^0.2") (d #t) (k 0)))) (h "056r34i32fxl26afzxr4syf667s104rd7cqn3swdc2qgqnp3mdrv")))

(define-public crate-farmfe_utils-0.0.2 (c (n "farmfe_utils") (v "0.0.2") (d (list (d (n "pathdiff") (r "^0.2") (d #t) (k 0)))) (h "0mkl31v6cavkj907dwv4ypjdx44mg3l2lr2hgl7a5xdzqil0jibh")))

(define-public crate-farmfe_utils-0.0.3 (c (n "farmfe_utils") (v "0.0.3") (d (list (d (n "pathdiff") (r "^0.2") (d #t) (k 0)))) (h "09wda1n7fg0wqgarj58d767h1asw5vrcs7wxs4yayhnv0jgz4wr5")))

(define-public crate-farmfe_utils-0.0.4 (c (n "farmfe_utils") (v "0.0.4") (d (list (d (n "pathdiff") (r "^0.2") (d #t) (k 0)))) (h "0l0xwh1p3hyliwkvijgq3jfgszsf1cy318rw3ahgkdrazqz4lr0s")))

(define-public crate-farmfe_utils-0.1.0 (c (n "farmfe_utils") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "12wz2167n795pgpjj1bhq4ivny8qm946gqkvaq77863mhp6n2kws")))

(define-public crate-farmfe_utils-0.1.1 (c (n "farmfe_utils") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1vakq4pkf1n9n58zrqb4vdd3jy50hkwz2hc16s8q1rsw9zramh2j")))

(define-public crate-farmfe_utils-0.1.2 (c (n "farmfe_utils") (v "0.1.2") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "05hzmydm274qb6r1jvy1pjl55ypydvzdgjll2qmaslipf73dji8s")))

(define-public crate-farmfe_utils-0.1.3 (c (n "farmfe_utils") (v "0.1.3") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1fb247bfn2yn9y8ciz81jpf0jkpkk022ksfgkl2y0qwhgyfdyib4")))

(define-public crate-farmfe_utils-0.1.4 (c (n "farmfe_utils") (v "0.1.4") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "128lwgimzs865afiififw3y305rjc74q2xxq08dlxcy5nx4j8k16")))

