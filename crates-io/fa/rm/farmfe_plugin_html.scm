(define-module (crates-io fa rm farmfe_plugin_html) #:use-module (crates-io))

(define-public crate-farmfe_plugin_html-0.0.1 (c (n "farmfe_plugin_html") (v "0.0.1") (d (list (d (n "farmfe_core") (r "^0.4.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.2") (d #t) (k 0)))) (h "15z78w1k4xx4lpglpvs7zsbzp2kcqw8bkbv86i2jy46fsm7qlikm")))

(define-public crate-farmfe_plugin_html-0.0.2 (c (n "farmfe_plugin_html") (v "0.0.2") (d (list (d (n "farmfe_core") (r "^0.4.2") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.3") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.42") (d #t) (k 0)))) (h "10j2vrr8a2x9ralqcpg2rp8bbfmkvjwam5kiakc5sr34m8pqa5m5")))

(define-public crate-farmfe_plugin_html-0.0.3 (c (n "farmfe_plugin_html") (v "0.0.3") (d (list (d (n "farmfe_core") (r "^0.4.3") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.4") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.4") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.42") (d #t) (k 0)))) (h "0zmb9nryf8jkmr9m0dz0lrj9qn7nisxx0pqsp603ckf8sxc2kwyn")))

(define-public crate-farmfe_plugin_html-0.0.4 (c (n "farmfe_plugin_html") (v "0.0.4") (d (list (d (n "farmfe_core") (r "^0.4.5") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.6") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.42") (d #t) (k 0)))) (h "0mj4b2pi6jd7h35g7hk4zwbk3l7hs9a0p928aqf6vgrcawn9dis7")))

(define-public crate-farmfe_plugin_html-0.0.5 (c (n "farmfe_plugin_html") (v "0.0.5") (d (list (d (n "farmfe_core") (r "^0.5.0") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.7") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.42") (d #t) (k 0)))) (h "0g86r01akr5agwx6s2hj3swz23qa0wck7c71iblk5z1rc5g1bpfz")))

(define-public crate-farmfe_plugin_html-0.0.6 (c (n "farmfe_plugin_html") (v "0.0.6") (d (list (d (n "farmfe_core") (r "^0.5.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.8") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.42") (d #t) (k 0)))) (h "1sg1kzn4d6mgzpa0mh1nwg5ha8xg2y2mv2wd5acj3mv83v1glmxb")))

