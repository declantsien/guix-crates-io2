(define-module (crates-io fa rm farmfe_plugin_minify) #:use-module (crates-io))

(define-public crate-farmfe_plugin_minify-0.0.1 (c (n "farmfe_plugin_minify") (v "0.0.1") (d (list (d (n "farmfe_core") (r "^0.4.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.2") (d #t) (k 0)))) (h "0bxcdw6yzncq70vyvfml95h2nxi84865gnkjjabllrpwgi6pvk6b")))

(define-public crate-farmfe_plugin_minify-0.0.2 (c (n "farmfe_plugin_minify") (v "0.0.2") (d (list (d (n "farmfe_core") (r "^0.4.2") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.3") (d #t) (k 0)))) (h "0dh5c6byw5390ki9rpbzlh2k5xk8qin3g96758i0y2qxqmnn0pd3")))

(define-public crate-farmfe_plugin_minify-0.0.3 (c (n "farmfe_plugin_minify") (v "0.0.3") (d (list (d (n "farmfe_core") (r "^0.4.3") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.4") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.4") (d #t) (k 0)))) (h "016pl2nkgn408202gq0dlqdkdgksmcvv564g4maag5v9k6dw0hci")))

(define-public crate-farmfe_plugin_minify-0.0.4 (c (n "farmfe_plugin_minify") (v "0.0.4") (d (list (d (n "farmfe_core") (r "^0.4.5") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.6") (d #t) (k 0)))) (h "0bq7l6n72ka7g0nwrpkpv0279dqrdwc14pmw8jlhm7k64vh4fnwx")))

(define-public crate-farmfe_plugin_minify-0.0.5 (c (n "farmfe_plugin_minify") (v "0.0.5") (d (list (d (n "farmfe_core") (r "^0.5.0") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.7") (d #t) (k 0)))) (h "17np4inxsq4ps9swsxfk49v24qzi87sbk21bdpzsim29jj4dsi89")))

(define-public crate-farmfe_plugin_minify-0.0.6 (c (n "farmfe_plugin_minify") (v "0.0.6") (d (list (d (n "farmfe_core") (r "^0.5.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.8") (d #t) (k 0)))) (h "084297chp3lk9c6zmp7h5nxzy414dk62dxm8r397351hcjxrdx1q")))

