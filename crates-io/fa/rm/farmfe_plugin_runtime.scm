(define-module (crates-io fa rm farmfe_plugin_runtime) #:use-module (crates-io))

(define-public crate-farmfe_plugin_runtime-0.0.1 (c (n "farmfe_plugin_runtime") (v "0.0.1") (d (list (d (n "farmfe_core") (r "^0.4.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1yvaya8qwfwg58yg1a0j26byq0pzd0n3avvz28xl29f3kll937r2")))

(define-public crate-farmfe_plugin_runtime-0.0.2 (c (n "farmfe_plugin_runtime") (v "0.0.2") (d (list (d (n "farmfe_core") (r "^0.4.2") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "1cwkfghhp13qz9vgg6372wqvd19gyixi2y824y2hwc04z41550xm")))

(define-public crate-farmfe_plugin_runtime-0.0.3 (c (n "farmfe_plugin_runtime") (v "0.0.3") (d (list (d (n "farmfe_core") (r "^0.4.4") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.5") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.4") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.42") (d #t) (k 0)))) (h "1fvw7kqpwjy578jalsa5qbdr9nzxdaj153r1pysi3xfy3x2m6mhh")))

(define-public crate-farmfe_plugin_runtime-0.0.4 (c (n "farmfe_plugin_runtime") (v "0.0.4") (d (list (d (n "farmfe_core") (r "^0.4.5") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.42") (d #t) (k 0)))) (h "0ka9jy1kh068pkddn6k0sa9y6gzgizihrqsggh9fxqq2wfbs984m")))

(define-public crate-farmfe_plugin_runtime-0.0.5 (c (n "farmfe_plugin_runtime") (v "0.0.5") (d (list (d (n "farmfe_core") (r "^0.5.0") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.42") (d #t) (k 0)))) (h "0lx82qla8g44wchaqbypv1ka64ha5k23v4nfjga52lc84v830n98")))

(define-public crate-farmfe_plugin_runtime-0.0.6 (c (n "farmfe_plugin_runtime") (v "0.0.6") (d (list (d (n "farmfe_core") (r "^0.5.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.42") (d #t) (k 0)))) (h "0k7bn589dmmkkgic8vrywcnqwazznl2i0ykk333n3byfwfikhm62")))

