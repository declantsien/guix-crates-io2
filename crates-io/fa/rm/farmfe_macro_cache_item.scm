(define-module (crates-io fa rm farmfe_macro_cache_item) #:use-module (crates-io))

(define-public crate-farmfe_macro_cache_item-0.0.1 (c (n "farmfe_macro_cache_item") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16g76fpkz4q05niwx6phgq4p022lf7lw3325scyxvylwq9r0dv03")))

(define-public crate-farmfe_macro_cache_item-0.0.2 (c (n "farmfe_macro_cache_item") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ks7awsfirpyxpwn9vh459jf4f52msalm8l0sjy7ban8m8ad7byy")))

(define-public crate-farmfe_macro_cache_item-0.1.0 (c (n "farmfe_macro_cache_item") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jdvayckwic3hz2lainzhq6wjflxi58nssdzfydkdjcsskz9jjl6")))

(define-public crate-farmfe_macro_cache_item-0.1.1 (c (n "farmfe_macro_cache_item") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nh1gapd3m3s7w8s7s2sk03dm2g7gdsv4a9a62fz3af6x7xg65s9")))

(define-public crate-farmfe_macro_cache_item-0.1.2 (c (n "farmfe_macro_cache_item") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15a7k1bp2akqb444nd6mr96r42bghj1zh4ssbz77sy2iccyjv6yf")))

(define-public crate-farmfe_macro_cache_item-0.1.3 (c (n "farmfe_macro_cache_item") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15v2q35mbv9f3ls0cg7qzrbx80ypccbi3d73bhkgmy2pg9hq45y6")))

