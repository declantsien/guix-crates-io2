(define-module (crates-io fa rm farmfe_plugin_define) #:use-module (crates-io))

(define-public crate-farmfe_plugin_define-0.0.1 (c (n "farmfe_plugin_define") (v "0.0.1") (d (list (d (n "farmfe_core") (r "^0.4.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.3") (d #t) (k 0)))) (h "16mkj6m48s1l6pjidlic25xmwqb0wah94sg25zk5fazzimmpsnqr")))

(define-public crate-farmfe_plugin_define-0.0.2 (c (n "farmfe_plugin_define") (v "0.0.2") (d (list (d (n "farmfe_core") (r "^0.4.2") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "1fsf737gslwkqp8nwj4gjd8msbz13y4ig4b0mpa8jaf4lrxg69cw")))

(define-public crate-farmfe_plugin_define-0.0.3 (c (n "farmfe_plugin_define") (v "0.0.3") (d (list (d (n "farmfe_core") (r "^0.4.3") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.4") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.4") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "0h2sdfwjfkrfgbx74xj19zcq0mxa2h2qgi6sbvwajb7fsxlq97d4")))

(define-public crate-farmfe_plugin_define-0.0.4 (c (n "farmfe_plugin_define") (v "0.0.4") (d (list (d (n "farmfe_core") (r "^0.4.5") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "0klf1agapw0c27l4n2va0b1cb2l3rsk5sg9p8diq5c148mciiyhk")))

(define-public crate-farmfe_plugin_define-0.0.5 (c (n "farmfe_plugin_define") (v "0.0.5") (d (list (d (n "farmfe_core") (r "^0.5.0") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "10fdwmxggha8xxqai9ag7hy3fqcd92d8g00ypqdimvcmkkpixjkj")))

(define-public crate-farmfe_plugin_define-0.0.6 (c (n "farmfe_plugin_define") (v "0.0.6") (d (list (d (n "farmfe_core") (r "^0.5.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "0i2jh0pxxcdg0vwgz08sfawbsh3nrl5n5sl8vl2n7amhk1zwc16d")))

