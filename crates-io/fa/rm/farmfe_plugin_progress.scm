(define-module (crates-io fa rm farmfe_plugin_progress) #:use-module (crates-io))

(define-public crate-farmfe_plugin_progress-0.0.1 (c (n "farmfe_plugin_progress") (v "0.0.1") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "farmfe_core") (r "^0.4.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)))) (h "0v1h4w8vn2i5rpkk5nzz41zz0j6lw8z6y460zwwyhnp2gp5hxr2a")))

(define-public crate-farmfe_plugin_progress-0.0.2 (c (n "farmfe_plugin_progress") (v "0.0.2") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "farmfe_core") (r "^0.4.2") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)))) (h "1bw9x0v0rs0m3swd3b84c0kspgkyi913crdnsyavzxls5mn9bz6q")))

(define-public crate-farmfe_plugin_progress-0.0.3 (c (n "farmfe_plugin_progress") (v "0.0.3") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "farmfe_core") (r "^0.4.4") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)))) (h "0qzwmvzgqsfgilawh4pnskmv0b6j18bkkvwg2d48lr46na0rga6d")))

(define-public crate-farmfe_plugin_progress-0.0.4 (c (n "farmfe_plugin_progress") (v "0.0.4") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "farmfe_core") (r "^0.4.5") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)))) (h "0awvaplz2x6mwl65yq16y7jikn4hgjlmsryc9s45fsdvkz3rvkxh")))

(define-public crate-farmfe_plugin_progress-0.0.5 (c (n "farmfe_plugin_progress") (v "0.0.5") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "farmfe_core") (r "^0.5.0") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)))) (h "1dcchm52lncjis5yjz7igp75wn81ba5nagcn3h3ykl0m8iyjzpap")))

(define-public crate-farmfe_plugin_progress-0.0.6 (c (n "farmfe_plugin_progress") (v "0.0.6") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "farmfe_core") (r "^0.5.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)))) (h "0d7wai082b65sh0kbhvm1iw2h8k4n3d29m421ik5712pxlndn4s0")))

