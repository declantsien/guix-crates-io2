(define-module (crates-io fa rm farmfe_swc_transformer_import_glob) #:use-module (crates-io))

(define-public crate-farmfe_swc_transformer_import_glob-0.0.1 (c (n "farmfe_swc_transformer_import_glob") (v "0.0.1") (d (list (d (n "farmfe_core") (r "^0.4.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0x054k5wqsc23v4qwzim57y11f8wprih6k3nvm8p4n5sfaa952zk")))

(define-public crate-farmfe_swc_transformer_import_glob-0.0.2 (c (n "farmfe_swc_transformer_import_glob") (v "0.0.2") (d (list (d (n "farmfe_core") (r "^0.4.2") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "0ygswq2q32h314dm07xai158q9p54arzq39dkvgmdzvlnmkxh34l")))

(define-public crate-farmfe_swc_transformer_import_glob-0.0.3 (c (n "farmfe_swc_transformer_import_glob") (v "0.0.3") (d (list (d (n "farmfe_core") (r "^0.4.4") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.5") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.4") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "02sw274a09ghwb3f8hns9gy9lhqnxcpzmsr1zy9dbi0v1sivqlfw")))

(define-public crate-farmfe_swc_transformer_import_glob-0.0.4 (c (n "farmfe_swc_transformer_import_glob") (v "0.0.4") (d (list (d (n "farmfe_core") (r "^0.4.5") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "0xdl4sa5wnpx549q55fqcky77kh2p86nn03g0lz8fihdkcgj2b1c")))

(define-public crate-farmfe_swc_transformer_import_glob-0.0.5 (c (n "farmfe_swc_transformer_import_glob") (v "0.0.5") (d (list (d (n "farmfe_core") (r "^0.5.0") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "1gz2s8l2lbjdgri7b2213snmb5x56w9i40a4fs9q3lakcmi42sw0")))

(define-public crate-farmfe_swc_transformer_import_glob-0.0.6 (c (n "farmfe_swc_transformer_import_glob") (v "0.0.6") (d (list (d (n "farmfe_core") (r "^0.5.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "10q4m629d60x0ihhay675yqc9l97iykrbfp0f2amx050dflbnj1l")))

