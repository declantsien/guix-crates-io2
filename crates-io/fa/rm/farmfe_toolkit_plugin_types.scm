(define-module (crates-io fa rm farmfe_toolkit_plugin_types) #:use-module (crates-io))

(define-public crate-farmfe_toolkit_plugin_types-0.0.2 (c (n "farmfe_toolkit_plugin_types") (v "0.0.2") (d (list (d (n "farmfe_core") (r "^0.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "04zg3qkbnaizz4xd2ps0cc835bi541vbfdphhc1jcn646dscnryz")))

(define-public crate-farmfe_toolkit_plugin_types-0.0.3 (c (n "farmfe_toolkit_plugin_types") (v "0.0.3") (d (list (d (n "farmfe_core") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "0q1ylqck9y34vm1hl1waa85xwbsxdzb45qif9bpn140s8pc4hpkn")))

(define-public crate-farmfe_toolkit_plugin_types-0.0.4 (c (n "farmfe_toolkit_plugin_types") (v "0.0.4") (d (list (d (n "farmfe_core") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "03sx0w53gnlcxyxfhg8psqbxb33p8m3j9qvvlgs37hambdg9vj8i")))

(define-public crate-farmfe_toolkit_plugin_types-0.0.5 (c (n "farmfe_toolkit_plugin_types") (v "0.0.5") (d (list (d (n "farmfe_core") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "0x9dgdhbc6p8lb94sydxfmnja0hd262sqg2qzf54xqsnrvpbq18l")))

(define-public crate-farmfe_toolkit_plugin_types-0.0.6 (c (n "farmfe_toolkit_plugin_types") (v "0.0.6") (d (list (d (n "farmfe_core") (r "^0.2.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "19sfcrz6x5y4n1mv7zk46i45l9xp5qrxqp1z3h9l51lm5w9lsdn6")))

(define-public crate-farmfe_toolkit_plugin_types-0.0.7 (c (n "farmfe_toolkit_plugin_types") (v "0.0.7") (d (list (d (n "farmfe_core") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "1na2iqcq9yhf5wnyihcfqpnnbz5vfbwrw0sq075bdfyb6c1a3n5p")))

(define-public crate-farmfe_toolkit_plugin_types-0.0.8 (c (n "farmfe_toolkit_plugin_types") (v "0.0.8") (d (list (d (n "farmfe_core") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "16fm41xh73gpgf1rgba840vy9jgz2hwba31nmccysxbzzbbycki1")))

(define-public crate-farmfe_toolkit_plugin_types-0.0.9 (c (n "farmfe_toolkit_plugin_types") (v "0.0.9") (d (list (d (n "farmfe_core") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "015gmnbd8vzzk77ab7ybvmy1m8jlsas54zarrkfakzlgmazgp41h")))

(define-public crate-farmfe_toolkit_plugin_types-0.0.10 (c (n "farmfe_toolkit_plugin_types") (v "0.0.10") (d (list (d (n "farmfe_core") (r "^0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "0sw0gdl79fz8dxrgvbyr11xwvz20r77s5nfhl989lf167wajll6s")))

(define-public crate-farmfe_toolkit_plugin_types-0.0.11 (c (n "farmfe_toolkit_plugin_types") (v "0.0.11") (d (list (d (n "farmfe_core") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "0zwiqb6s6cqgvl2l8fhxm8mhxbp23ynfz5m1mgr3a0pwlwc4yd6w")))

(define-public crate-farmfe_toolkit_plugin_types-0.0.12 (c (n "farmfe_toolkit_plugin_types") (v "0.0.12") (d (list (d (n "farmfe_core") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "15zpdpy7rk826fs34jnw1kgld7alg7znvrd9frw91rgk7w9l5dmn")))

(define-public crate-farmfe_toolkit_plugin_types-0.0.13 (c (n "farmfe_toolkit_plugin_types") (v "0.0.13") (d (list (d (n "farmfe_core") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "1bnrzg68fhz2vvlpjzbvza1ndrgc16z4bzr95s230dc8frxx7h5h")))

(define-public crate-farmfe_toolkit_plugin_types-0.0.14 (c (n "farmfe_toolkit_plugin_types") (v "0.0.14") (d (list (d (n "farmfe_core") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "1qzcygidjh7mhwwnrd0l3c9b6afcmypxxlg3a583fs9mi03xaajl")))

(define-public crate-farmfe_toolkit_plugin_types-0.0.15 (c (n "farmfe_toolkit_plugin_types") (v "0.0.15") (d (list (d (n "farmfe_core") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "0phy6c8ppib8kfmxbi4x6i7rgqwrq6spas7f36p1bz84sfpgchjh")))

