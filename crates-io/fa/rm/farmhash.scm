(define-module (crates-io fa rm farmhash) #:use-module (crates-io))

(define-public crate-farmhash-1.1.0 (c (n "farmhash") (v "1.1.0") (h "0mls8c5pjmp2cfs4dqz9pc3a1fpmkg3xd1yz0wyb932xqcr16sk6")))

(define-public crate-farmhash-1.1.1 (c (n "farmhash") (v "1.1.1") (d (list (d (n "fnv") (r "^1.0.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "14gsplx96pmgwvhc5kbq4rja8cjck2lyzyaf5mgymm45irmvvis7")))

(define-public crate-farmhash-1.1.2 (c (n "farmhash") (v "1.1.2") (d (list (d (n "fnv") (r "^1.0.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0h6g4pchv16qlkda4860n0sb32ml01n6qhlrccyqwg4i8j4ps5ys")))

(define-public crate-farmhash-1.1.4 (c (n "farmhash") (v "1.1.4") (d (list (d (n "fnv") (r "^1.0.0") (d #t) (k 2)))) (h "03b8g3v3754pmhyq6ng0761b61cwq3dks0fq771gh56z4939iy6n")))

(define-public crate-farmhash-1.1.3 (c (n "farmhash") (v "1.1.3") (d (list (d (n "fnv") (r "^1.0.0") (d #t) (k 2)))) (h "1sqlp0y9k4aqfjg9dr3pq04pb3wcn9psq5i5jzlavw2nlbz18296")))

(define-public crate-farmhash-1.1.5 (c (n "farmhash") (v "1.1.5") (d (list (d (n "fnv") (r "^1.0.0") (d #t) (k 2)))) (h "0jz3xvrrmsssjmshgw8anmly792i55sk1hyvx9fcg4cqzg4fjp7k")))

