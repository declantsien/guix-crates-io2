(define-module (crates-io fa rm farmfe_plugin_polyfill) #:use-module (crates-io))

(define-public crate-farmfe_plugin_polyfill-0.0.1 (c (n "farmfe_plugin_polyfill") (v "0.0.1") (d (list (d (n "farmfe_core") (r "^0.4.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.2") (d #t) (k 0)))) (h "0rqrspxxjmks4qilnras2b99cx21w0xp6vmyy2wfv3v48kv0pi39")))

(define-public crate-farmfe_plugin_polyfill-0.0.2 (c (n "farmfe_plugin_polyfill") (v "0.0.2") (d (list (d (n "farmfe_core") (r "^0.4.2") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.3") (d #t) (k 0)))) (h "15r4y6r1kd08hyc4scxlv2h3q2z54r57xgb8j17x34bjz1fyabzd")))

(define-public crate-farmfe_plugin_polyfill-0.0.3 (c (n "farmfe_plugin_polyfill") (v "0.0.3") (d (list (d (n "farmfe_core") (r "^0.4.4") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.5") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.4") (d #t) (k 0)))) (h "1d17gjqk57bzq317xr2lgxi0hxnr3rpgq2lir6b4vnw9bin9djli")))

(define-public crate-farmfe_plugin_polyfill-0.0.4 (c (n "farmfe_plugin_polyfill") (v "0.0.4") (d (list (d (n "farmfe_core") (r "^0.4.5") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.6") (d #t) (k 0)))) (h "0zjf5gqhhys67hm3y1j4ff6sqzpjsfaaah3q1nkh4gyqmwsmgr0c")))

(define-public crate-farmfe_plugin_polyfill-0.0.5 (c (n "farmfe_plugin_polyfill") (v "0.0.5") (d (list (d (n "farmfe_core") (r "^0.5.0") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.7") (d #t) (k 0)))) (h "183ncq89jbhd5bivadldmghf3784xm985nyfd4s401sy3slq8qwc")))

(define-public crate-farmfe_plugin_polyfill-0.0.6 (c (n "farmfe_plugin_polyfill") (v "0.0.6") (d (list (d (n "farmfe_core") (r "^0.5.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.8") (d #t) (k 0)))) (h "0n7bj6zacwykf5k7hbnryx4bbp061zbsppyn9w4gv2s1j7rj9s5c")))

