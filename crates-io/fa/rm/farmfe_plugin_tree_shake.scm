(define-module (crates-io fa rm farmfe_plugin_tree_shake) #:use-module (crates-io))

(define-public crate-farmfe_plugin_tree_shake-0.0.1 (c (n "farmfe_plugin_tree_shake") (v "0.0.1") (d (list (d (n "farmfe_core") (r "^0.4.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0gbjmfqxmj1bxxzibavfxnpqyw7pzcymwbaif7psa8896a8mma3n") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_tree_shake-0.0.2 (c (n "farmfe_plugin_tree_shake") (v "0.0.2") (d (list (d (n "farmfe_core") (r "^0.4.2") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "0vc5vsl7r3ilh6dpy0sk46mpx24crb19dqfi35v5j4ibbbla4skv") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_tree_shake-0.0.3 (c (n "farmfe_plugin_tree_shake") (v "0.0.3") (d (list (d (n "farmfe_core") (r "^0.4.4") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.5") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.5") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "0qcsf0dzbnkygb5ihk1gsx52lavlaaj51s2wcz34jl6r91w9xcpm") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_tree_shake-0.0.4 (c (n "farmfe_plugin_tree_shake") (v "0.0.4") (d (list (d (n "farmfe_core") (r "^0.4.5") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "02xdadzqdyda8m3dba407sj9wf9aw5zgags11vxvi6fv7rn7lsnn") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_tree_shake-0.0.5 (c (n "farmfe_plugin_tree_shake") (v "0.0.5") (d (list (d (n "farmfe_core") (r "^0.5.0") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "0k1lc40gymbrgpi53am8h2b3ld6dxn7ldpqlkigrb3dl4h3fx217") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_tree_shake-0.0.6 (c (n "farmfe_plugin_tree_shake") (v "0.0.6") (d (list (d (n "farmfe_core") (r "^0.5.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "09a3knyk15f47ib0h53gisyzvfqnhxna683jf7ibipz4l53m5axf") (f (quote (("profile" "farmfe_core/profile"))))))

