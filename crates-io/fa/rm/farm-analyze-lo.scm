(define-module (crates-io fa rm farm-analyze-lo) #:use-module (crates-io))

(define-public crate-farm-analyze-lo-1.0.0 (c (n "farm-analyze-lo") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.31") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0hd6nr06q4d6pzn05p0pac46w15b1rppxwk912bhg6gg8fvb8i2k")))

(define-public crate-farm-analyze-lo-1.0.1 (c (n "farm-analyze-lo") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.31") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0l0qa5yz2dyg5h4p8v4dxpn3n5xh9bk1l80ilcmbn3fbc7hlrlzc")))

(define-public crate-farm-analyze-lo-1.0.2 (c (n "farm-analyze-lo") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.31") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "06mydbzk1dqsyvxv010923s7nafj9xbrhsn38x1kx0cmdbvnq86l")))

