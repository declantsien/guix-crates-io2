(define-module (crates-io fa rm farmfe_plugin_lazy_compilation) #:use-module (crates-io))

(define-public crate-farmfe_plugin_lazy_compilation-0.0.1 (c (n "farmfe_plugin_lazy_compilation") (v "0.0.1") (d (list (d (n "farmfe_core") (r "^0.4.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1r4d6sfamqsr9p5x7m3fg7b05yhndv7559lirikxav4l8i3nqhgn")))

(define-public crate-farmfe_plugin_lazy_compilation-0.0.2 (c (n "farmfe_plugin_lazy_compilation") (v "0.0.2") (d (list (d (n "farmfe_core") (r "^0.4.2") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "0aa15dzzknmzc1rnkyvg8swxq977j80gdx0g9cj06k3i8rw56b4j")))

(define-public crate-farmfe_plugin_lazy_compilation-0.0.3 (c (n "farmfe_plugin_lazy_compilation") (v "0.0.3") (d (list (d (n "farmfe_core") (r "^0.4.3") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.4") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.4") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "14iyxb9qbffnhwqb40q2yfbd1d7fa5pcvrw8bk0bilp4qndx0x2g")))

(define-public crate-farmfe_plugin_lazy_compilation-0.0.4 (c (n "farmfe_plugin_lazy_compilation") (v "0.0.4") (d (list (d (n "farmfe_core") (r "^0.4.5") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "154kwxn7mlmfh19mvrgvgcmmny07qqavpfp1a5dw1myx0dhviqip")))

(define-public crate-farmfe_plugin_lazy_compilation-0.0.5 (c (n "farmfe_plugin_lazy_compilation") (v "0.0.5") (d (list (d (n "farmfe_core") (r "^0.5.0") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "0hym1h1hvlv0ak0crfv1h5vrkbf9fb7k67vzcj3v235gl8y7fg09")))

(define-public crate-farmfe_plugin_lazy_compilation-0.0.6 (c (n "farmfe_plugin_lazy_compilation") (v "0.0.6") (d (list (d (n "farmfe_core") (r "^0.5.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_utils") (r "^0.1.4") (d #t) (k 0)))) (h "1p3y0ahicf8dlk8rswl41hb31x26kxw37f750n40jbirmqsh2zkj")))

