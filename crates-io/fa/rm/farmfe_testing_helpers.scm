(define-module (crates-io fa rm farmfe_testing_helpers) #:use-module (crates-io))

(define-public crate-farmfe_testing_helpers-0.0.1 (c (n "farmfe_testing_helpers") (v "0.0.1") (d (list (d (n "farmfe_core") (r "^0.3.0") (d #t) (k 0)))) (h "0h31cdn22dzwssjmdldnnrkypjpsilpx1h8hw5m9bq9gkk036jdf")))

(define-public crate-farmfe_testing_helpers-0.0.2 (c (n "farmfe_testing_helpers") (v "0.0.2") (d (list (d (n "farmfe_core") (r "^0.4.1") (d #t) (k 0)))) (h "18lk9430pvn8468wvv83kvk7z1msi440zd9pn0crrnffwgbm7xii")))

(define-public crate-farmfe_testing_helpers-0.0.3 (c (n "farmfe_testing_helpers") (v "0.0.3") (d (list (d (n "farmfe_core") (r "^0.4.2") (d #t) (k 0)))) (h "1yslgrs1c4835cyd75ign1kbv0xrqvqygxz0xzv76gp3vh0y8ihb")))

(define-public crate-farmfe_testing_helpers-0.0.4 (c (n "farmfe_testing_helpers") (v "0.0.4") (d (list (d (n "farmfe_core") (r "^0.4.3") (d #t) (k 0)))) (h "0z9km63cdxfak4df99b63gr6i8aa00j4s35a8zazxxas1pdz8hsy")))

(define-public crate-farmfe_testing_helpers-0.0.5 (c (n "farmfe_testing_helpers") (v "0.0.5") (d (list (d (n "farmfe_core") (r "^0.4.4") (d #t) (k 0)))) (h "0ly02k6dh85qxkls6cqss88d1j3kfc5glxpgsf7z4nl0vs8ha03k")))

(define-public crate-farmfe_testing_helpers-0.0.6 (c (n "farmfe_testing_helpers") (v "0.0.6") (d (list (d (n "farmfe_core") (r "^0.4.5") (d #t) (k 0)))) (h "1j47zcngb7yx96knbcrzmlrxca8amis06ajnsjv6aqkc3wkcrkcs")))

(define-public crate-farmfe_testing_helpers-0.0.7 (c (n "farmfe_testing_helpers") (v "0.0.7") (d (list (d (n "farmfe_core") (r "^0.5.0") (d #t) (k 0)))) (h "1fv9w2vxnmnbinyl48i6dpr3arfv05bvqmmrvpfyq46akbf801fk")))

(define-public crate-farmfe_testing_helpers-0.0.8 (c (n "farmfe_testing_helpers") (v "0.0.8") (d (list (d (n "farmfe_core") (r "^0.5.1") (d #t) (k 0)))) (h "0daysniq85wvib589hr9vycshfk8wj884hd8i6jyzzs0nnxngjhb")))

