(define-module (crates-io fa rm farmfe_macro_plugin) #:use-module (crates-io))

(define-public crate-farmfe_macro_plugin-0.0.1 (c (n "farmfe_macro_plugin") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gj2jrq15s4g94w79avzknfd5zwd9267817bk71aad76h2yrxi6q")))

(define-public crate-farmfe_macro_plugin-0.0.2 (c (n "farmfe_macro_plugin") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1816y2slffwd1xybl3icr90jwr3yq5z3mpzjj3jc86rm3xhzdj05")))

(define-public crate-farmfe_macro_plugin-0.0.3 (c (n "farmfe_macro_plugin") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xmfyb7zir9qg5abph56zl0gdd8py1zaq98642wsdrsq52ms0aa2")))

(define-public crate-farmfe_macro_plugin-0.0.4 (c (n "farmfe_macro_plugin") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0cs99wmwfp4626qxgz899y86n2xgqqb4fabhvil58rw1s83djph5")))

