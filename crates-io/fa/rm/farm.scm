(define-module (crates-io fa rm farm) #:use-module (crates-io))

(define-public crate-farm-1.0.0 (c (n "farm") (v "1.0.0") (d (list (d (n "clap") (r "^2.27.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "mysql") (r "^14.0.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.0") (d #t) (k 0)) (d (n "r2d2_mysql") (r "^9.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "0pfb30g92qqwn24y0mdwy41kx228yxasy4qhagnj1hv62wm3yxb6")))

(define-public crate-farm-1.1.0 (c (n "farm") (v "1.1.0") (d (list (d (n "clap") (r "^2.27.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "mysql") (r "^14.0.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.0") (d #t) (k 0)) (d (n "r2d2_mysql") (r "^9.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "05cc7v79zzz9jqcac4smwly400bwnl7c3zd1d0k755zhzmr1aiw8")))

