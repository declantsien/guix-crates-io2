(define-module (crates-io fa rm farmfe_plugin_partial_bundling) #:use-module (crates-io))

(define-public crate-farmfe_plugin_partial_bundling-0.0.1 (c (n "farmfe_plugin_partial_bundling") (v "0.0.1") (d (list (d (n "farmfe_core") (r "^0.4.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.2") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.2") (d #t) (k 0)))) (h "1883sx4cw2b7c6aiih5gb4xkhrbgpf3qnd59xkj5rdhwcv23g6js") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_partial_bundling-0.0.2 (c (n "farmfe_plugin_partial_bundling") (v "0.0.2") (d (list (d (n "farmfe_core") (r "^0.4.2") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.3") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.3") (d #t) (k 0)))) (h "1p9yrl2ksb1962w2ib5viwc5xx393crqh8vnbl94bam6rpf16fgz") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_partial_bundling-0.0.3 (c (n "farmfe_plugin_partial_bundling") (v "0.0.3") (d (list (d (n "farmfe_core") (r "^0.4.3") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.4") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.4") (d #t) (k 0)))) (h "1ka1cgk0fqi8hzw5pnnar8jqj0gsch7382hblc9rzjlzcg08spqm") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_partial_bundling-0.0.4 (c (n "farmfe_plugin_partial_bundling") (v "0.0.4") (d (list (d (n "farmfe_core") (r "^0.4.5") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.6") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.6") (d #t) (k 0)))) (h "1hyl2bwr0i68vwmy8rgx22vz1yn62dgxblr7sj107dmk7y5rffdv") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_partial_bundling-0.0.5 (c (n "farmfe_plugin_partial_bundling") (v "0.0.5") (d (list (d (n "farmfe_core") (r "^0.5.0") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.7") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.7") (d #t) (k 0)))) (h "1l9fbz0nfzm73qsbni059cf98zqarr0lbr1dmcn7swvn0wp9snh9") (f (quote (("profile" "farmfe_core/profile"))))))

(define-public crate-farmfe_plugin_partial_bundling-0.0.6 (c (n "farmfe_plugin_partial_bundling") (v "0.0.6") (d (list (d (n "farmfe_core") (r "^0.5.1") (d #t) (k 0)) (d (n "farmfe_testing_helpers") (r "^0.0.8") (d #t) (k 0)) (d (n "farmfe_toolkit") (r "^0.0.8") (d #t) (k 0)))) (h "1bajbr5s2d39hkvsaarg2frfd55nzj66zv8c1522qg74vjv4n2cj") (f (quote (("profile" "farmfe_core/profile"))))))

