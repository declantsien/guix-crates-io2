(define-module (crates-io fa rm farmhash-ffi) #:use-module (crates-io))

(define-public crate-farmhash-ffi-0.1.0 (c (n "farmhash-ffi") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ikax9xfk7kdk7g3q2gsclmjjvx3r51nzkhbxlh2p39l0gy07w54")))

