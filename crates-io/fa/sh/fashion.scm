(define-module (crates-io fa sh fashion) #:use-module (crates-io))

(define-public crate-fashion-0.1.0 (c (n "fashion") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "cssparser") (r "^0.32") (d #t) (k 0)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "string_cache") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hglzka60s1rm4m3w8axs2k4l1n04wbkrhw6wxc8qcn4bjxnc9ks")))

(define-public crate-fashion-0.2.0 (c (n "fashion") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "cssparser") (r "^0.32") (d #t) (k 0)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "string_cache") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vd0qbhl0pidywyazhbkv91gvg7x3l0mf4bnc94zvqcxfz5pvvia")))

