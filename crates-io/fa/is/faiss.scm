(define-module (crates-io fa is faiss) #:use-module (crates-io))

(define-public crate-faiss-0.1.0 (c (n "faiss") (v "0.1.0") (d (list (d (n "faiss-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0hj2b1ixw2ww4xf5hqk5kkbn9q7nxhsmfhk23x3hgpi2r7j84cyd") (f (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.2.0 (c (n "faiss") (v "0.2.0") (d (list (d (n "faiss-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0l9kj17jzkdd1d4cbadw7gaama8j0xg5nl7680h83x83sna3wn83") (f (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.3.0 (c (n "faiss") (v "0.3.0") (d (list (d (n "faiss-sys") (r "^0.2.0") (d #t) (k 0)))) (h "05qx6xj2cky031m84kxfybsy9adcja97z8jz1rdc8srz10yyd57s") (f (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.4.0 (c (n "faiss") (v "0.4.0") (d (list (d (n "faiss-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0qq41rvm5hbza5kvzgx9d1arpp7mhygvh08a4sdcmxnx7mq65l5w") (f (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.5.0 (c (n "faiss") (v "0.5.0") (d (list (d (n "faiss-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1pm0kd1aiy80mjc9g5y3didpabc5321pp7jl19jj387b7hrks1xr") (f (quote (("gpu" "faiss-sys/gpu")))) (y #t)))

(define-public crate-faiss-0.5.1 (c (n "faiss") (v "0.5.1") (d (list (d (n "faiss-sys") (r "^0.3.0") (d #t) (k 0)))) (h "19gf342ma0icjkpryazb0hm6f91fdzijcxxk1z17m8qz7jgxnlh9") (f (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.6.0 (c (n "faiss") (v "0.6.0") (d (list (d (n "faiss-sys") (r "^0.3.0") (d #t) (k 0)))) (h "095xk22vmvjnfyanq9cjb8vim22x3f7g9cn486xc1ydnn7i41jxb") (f (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.7.0 (c (n "faiss") (v "0.7.0") (d (list (d (n "faiss-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0jgmhqsn6mg5dabdmfk6ywhbgmszmjpan45rvk698l0m4gy84jdx") (f (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.8.0 (c (n "faiss") (v "0.8.0") (d (list (d (n "faiss-sys") (r "^0.3.0") (d #t) (k 0)))) (h "187aj98hw62za3vrxf8nj2x5ch3ik6hm5aliyly3pbhyfsyhhsi6") (f (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.9.0 (c (n "faiss") (v "0.9.0") (d (list (d (n "faiss-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0fh7ss1r4w0zi34ky59ymzh9smcdy51ap1isqc3i6wxxn18jkly6") (f (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.10.0 (c (n "faiss") (v "0.10.0") (d (list (d (n "faiss-sys") (r "^0.5.0") (d #t) (k 0)))) (h "14ni2qqpxkq68w34f5y0gffd7f7qfsbbmw4j3sgnrcqx2xbilg30") (f (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.11.0 (c (n "faiss") (v "0.11.0") (d (list (d (n "faiss-sys") (r "^0.6.0") (d #t) (k 0)))) (h "02pnrlpqsj43pc6zky1w36w7r0llwbx9n4xakbqxc0p3892jr841") (f (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.12.0 (c (n "faiss") (v "0.12.0") (d (list (d (n "faiss-sys") (r "^0.6.1") (d #t) (k 0)))) (h "1hsib1x57qcyia30b4digy7l2kyk4hfrdahj4fvr8hy8p51kmn2y") (f (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.12.1 (c (n "faiss") (v "0.12.1") (d (list (d (n "faiss-sys") (r "^0.6.2") (d #t) (k 0)))) (h "0cwpvp92ghp57yla96s3h39ww20fw49ivaih1a5h51i78d4f1zxk") (f (quote (("static" "faiss-sys/static") ("gpu" "faiss-sys/gpu"))))))

