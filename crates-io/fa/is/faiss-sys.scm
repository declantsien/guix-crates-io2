(define-module (crates-io fa is faiss-sys) #:use-module (crates-io))

(define-public crate-faiss-sys-0.1.0 (c (n "faiss-sys") (v "0.1.0") (h "1m38h39pkg7zwg2j9pcnsvy337v2ybbc4p6wj141sfs787yjwbac") (f (quote (("gpu"))))))

(define-public crate-faiss-sys-0.2.0 (c (n "faiss-sys") (v "0.2.0") (h "1cm3k85a9r3fsdgrlp9yzmz1bbnjlq60r4m61vcnvz1k1syaivn3") (f (quote (("gpu"))))))

(define-public crate-faiss-sys-0.3.0 (c (n "faiss-sys") (v "0.3.0") (h "1ykziagly0bsyajxm2dffnaxd7dcdk9nphgq5xma1bisayvf5bp7") (f (quote (("gpu")))) (l "faiss_c")))

(define-public crate-faiss-sys-0.4.0 (c (n "faiss-sys") (v "0.4.0") (h "128bp9yhs3fvb3f2apd0dj9kw5lcsm3gh0jjwxj3pj13cqnspsjn") (f (quote (("gpu")))) (l "faiss_c")))

(define-public crate-faiss-sys-0.5.0 (c (n "faiss-sys") (v "0.5.0") (h "1bjd65bpj4w9llkl7sq6vm0ndgpbm7zij06qzdjsfh2dmki4y5a2") (f (quote (("gpu")))) (l "faiss_c")))

(define-public crate-faiss-sys-0.6.0 (c (n "faiss-sys") (v "0.6.0") (h "1jnpj7q9yqinyjp5dd0ahmanf4ylrljm121w5bwh4v6hj7fy90fc") (f (quote (("gpu")))) (l "faiss_c")))

(define-public crate-faiss-sys-0.6.1 (c (n "faiss-sys") (v "0.6.1") (h "1q56lsx87snadq7vcz0qk6lkl94ybrr01zq7z08ss8x6jvqdvhsl") (f (quote (("gpu")))) (l "faiss_c")))

(define-public crate-faiss-sys-0.6.2 (c (n "faiss-sys") (v "0.6.2") (d (list (d (n "cmake") (r "^0.1.50") (o #t) (d #t) (k 1)))) (h "0pli8qp12rszkisp8isv4kv2wp2skinj45vz6lsby8k4qn7h172b") (f (quote (("static" "cmake") ("gpu")))) (l "faiss_c")))

