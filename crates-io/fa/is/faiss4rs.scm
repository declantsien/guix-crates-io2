(define-module (crates-io fa is faiss4rs) #:use-module (crates-io))

(define-public crate-faiss4rs-1.6.301 (c (n "faiss4rs") (v "1.6.301") (d (list (d (n "cpp") (r "^0.5.4") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.4") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0a1j3by1mpcsrpsn0pvx2nwi0skmlin06iccqk8x54hv9n5w7gvy")))

(define-public crate-faiss4rs-1.6.302 (c (n "faiss4rs") (v "1.6.302") (d (list (d (n "cpp") (r "^0.5.4") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.4") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0rw713in3jzb0gqa6g1ikq7aqsga66p5f1ssg3lppaf7zqwmf82d")))

(define-public crate-faiss4rs-1.6.303 (c (n "faiss4rs") (v "1.6.303") (d (list (d (n "cpp") (r "^0.5.4") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.4") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0n7fgmw5qnw9ai0yaaqmqix29gkh2h4mw6473q347as5sgacd1va")))

(define-public crate-faiss4rs-1.6.304 (c (n "faiss4rs") (v "1.6.304") (d (list (d (n "cpp") (r "^0.5.4") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.4") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0x2li0cvbk9fmcr9nrb6iihjq80vysy88hdx8k6gyy8yrxljkb8m")))

(define-public crate-faiss4rs-1.6.305 (c (n "faiss4rs") (v "1.6.305") (d (list (d (n "cpp") (r "^0.5.4") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.4") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0a5fqiwnfadyf6nryz87dfywr2ypgasawkk2141lhp7v52w0qi0h") (l "faiss")))

(define-public crate-faiss4rs-1.6.306 (c (n "faiss4rs") (v "1.6.306") (d (list (d (n "cpp") (r "^0.5.4") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.4") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0aa1fyrp2bvysnxaji75bz02rz4ri6rzgbmsh1ljbpg7wvf2lnxb") (l "faiss")))

(define-public crate-faiss4rs-1.6.307 (c (n "faiss4rs") (v "1.6.307") (d (list (d (n "cpp") (r "^0.5.4") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.4") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1hfq8nch49ggfscpaam063ccb7ngmdx2z37hy518x6568l6vjn6q") (l "faiss")))

