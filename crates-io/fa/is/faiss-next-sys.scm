(define-module (crates-io fa is faiss-next-sys) #:use-module (crates-io))

(define-public crate-faiss-next-sys-0.1.1 (c (n "faiss-next-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)))) (h "13sxk1hf1lbhnmpcp1hgq4r9bjd1v4pjip3q8g0w8wl1g6vlgfb0") (f (quote (("gpu") ("default") ("bindgen"))))))

(define-public crate-faiss-next-sys-0.2.0 (c (n "faiss-next-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)))) (h "0lld11czsw4arr5cb9aa2madl8hs9zyh60k354bq2y9wccpb4mp5") (f (quote (("gpu") ("default") ("bindgen"))))))

