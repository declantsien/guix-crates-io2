(define-module (crates-io fa as faas-wasm-runtime) #:use-module (crates-io))

(define-public crate-faas-wasm-runtime-0.1.0 (c (n "faas-wasm-runtime") (v "0.1.0") (d (list (d (n "cloudevents") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "http") (r "^0.1.19") (d #t) (k 0)) (d (n "hyper") (r "^0.11.7") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "wasmtime") (r "^0.11.0") (d #t) (k 0)))) (h "03a1nvvc9qxlazr8341wrbz9710qvilwjny6b4n1i3a7sx1887ch")))

