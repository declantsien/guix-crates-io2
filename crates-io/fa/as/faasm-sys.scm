(define-module (crates-io fa as faasm-sys) #:use-module (crates-io))

(define-public crate-faasm-sys-0.0.10 (c (n "faasm-sys") (v "0.0.10") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "default-tls"))) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "0cv52q95ng1rd2jhjlgfrszq36rlsvhvy6pxwb4zhhr7vc9nyaf4")))

(define-public crate-faasm-sys-0.0.12 (c (n "faasm-sys") (v "0.0.12") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "default-tls"))) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "1frx6q1ar8490cbr17qndd0i0bknnr2hx7r1852h0mv525ryr8v6")))

