(define-module (crates-io fa v_ fav_derive) #:use-module (crates-io))

(define-public crate-fav_derive-0.0.1-alpha1 (c (n "fav_derive") (v "0.0.1-alpha1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qsl4h2c20fcsxpfcf3c7m1x74sg15rmy78y1vmyfacqb3kfj4is")))

(define-public crate-fav_derive-0.0.1-alpha2 (c (n "fav_derive") (v "0.0.1-alpha2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0apzwc4b5nmw9n4v698ha8y5jbmaifhfkf00jl99cas4m1v9sfc9")))

(define-public crate-fav_derive-0.0.1-alpha3 (c (n "fav_derive") (v "0.0.1-alpha3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ls5a92mmjpjb161c4r2bb9zar88wiql0mz0nh7946p5zlh6plxc")))

(define-public crate-fav_derive-0.0.1-alpha4 (c (n "fav_derive") (v "0.0.1-alpha4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10wa080n233z8cbdb09kwjinsq8gprdbcfynghmwnx1l2f3zy3j9")))

(define-public crate-fav_derive-0.0.1-alpha5 (c (n "fav_derive") (v "0.0.1-alpha5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hm6128s8846d81ic1dl64sb3myinx5lx1w21i50s9fwnpr36m3r")))

(define-public crate-fav_derive-0.0.1-beta1 (c (n "fav_derive") (v "0.0.1-beta1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xhpi76vgk3gr7vvwyz7ndkz358ysjafwkxvwia605xc26nqa6dm")))

(define-public crate-fav_derive-0.0.1 (c (n "fav_derive") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mlgp5wmnk7k2cbp8z0wvi31vim2832wqmf2aj1cinb77ljp0d4l")))

