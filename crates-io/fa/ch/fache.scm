(define-module (crates-io fa ch fache) #:use-module (crates-io))

(define-public crate-fache-0.1.0 (c (n "fache") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)))) (h "1z4pbkza9rjqxmzpf9jx0mhz59qyg3a618yvsm9b8r4lybh1s7zn")))

(define-public crate-fache-0.1.1 (c (n "fache") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)))) (h "1xnw65gm95f3vapf0i7ngmkhc9i41mk9f5cfl9xx3qfqifpx5m4p")))

(define-public crate-fache-0.1.2 (c (n "fache") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)))) (h "10is1cfx0bizlcihykj0bcqwlk2ljzv25nmgrp9lp992v0mvd3qq")))

(define-public crate-fache-0.1.3 (c (n "fache") (v "0.1.3") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)))) (h "0g5c959b65dddn809a8k4fc99bw8nwrc1628qjp45z862fax1z56")))

(define-public crate-fache-0.1.5 (c (n "fache") (v "0.1.5") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "enigo") (r "^0.1.2") (d #t) (k 0)))) (h "07qj4qcbal6r7q1a893sza8ip7n458501a1298fz7ci64a0rxslc")))

(define-public crate-fache-0.1.6 (c (n "fache") (v "0.1.6") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "enigo") (r "^0.1.2") (d #t) (k 0)))) (h "1a62czsg42ga9kc7hmsn5gs8ycybj670a4j2ykrki5z8cw04b0h5")))

(define-public crate-fache-0.1.7 (c (n "fache") (v "0.1.7") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "enigo") (r "^0.1.2") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.14") (d #t) (k 0)))) (h "0498y32pmjxi4vkg155x8lcpkkawhfkcsf6cc7r4878bmpyr6pa2")))

