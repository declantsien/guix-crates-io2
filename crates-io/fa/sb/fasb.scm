(define-module (crates-io fa sb fasb) #:use-module (crates-io))

(define-public crate-fasb-0.1.0 (c (n "fasb") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "savan") (r "^0.2.0") (d #t) (k 0)))) (h "02ymiapa0hx10w7jgwd1p2ln44gmbj7xil9kfkg51ik1vchd08qr") (f (quote (("verbose") ("interpreter"))))))

(define-public crate-fasb-0.1.1 (c (n "fasb") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "savan") (r "^0.2.0") (d #t) (k 0)))) (h "059vj6kkyz5z7hk1wabk3kryflq5w2173msnynj0n5f3fxfrzy02") (f (quote (("verbose") ("interpreter"))))))

