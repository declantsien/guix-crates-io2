(define-module (crates-io fa un fauna) #:use-module (crates-io))

(define-public crate-fauna-0.1.0 (c (n "fauna") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "std" "serde"))) (k 0)) (d (n "fql-parser") (r "^0.2.8") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0ajj719p7xnid613naq8fzzdyqd0wfl01qn9z624p37rqb2hp9jy") (y #t)))

