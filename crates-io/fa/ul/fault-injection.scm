(define-module (crates-io fa ul fault-injection) #:use-module (crates-io))

(define-public crate-fault-injection-0.1.0 (c (n "fault-injection") (v "0.1.0") (h "1g6ik8cn6iqaz8mdncxdv524fkiqykwmx09z7r610n2m5c5knf49")))

(define-public crate-fault-injection-1.0.0 (c (n "fault-injection") (v "1.0.0") (h "1xx38vwh8mncbhxir5w2zyscbg55i9l42wpac6hx1nnkjzapzcdr")))

(define-public crate-fault-injection-1.0.1 (c (n "fault-injection") (v "1.0.1") (h "0ksawgiznbppc7llwzpkinmfrxclpz709j3wg5ib017vnsn0wgh5")))

(define-public crate-fault-injection-1.0.2 (c (n "fault-injection") (v "1.0.2") (h "1a1y8s4bb2zshm15ahdlrpsblqr34bmh1yf8d0mhg7bdakhcrlam")))

(define-public crate-fault-injection-1.0.3 (c (n "fault-injection") (v "1.0.3") (h "1aq6bsbphfxaan6pirx4d6rcrxp8nlb2hqkggn00w1sxl25sq9i2")))

(define-public crate-fault-injection-1.0.4 (c (n "fault-injection") (v "1.0.4") (h "1jdxbp8vash1k5020sv9wamwan6b45fz4dqd9m91p0aa3x2s98gj")))

(define-public crate-fault-injection-1.0.5 (c (n "fault-injection") (v "1.0.5") (h "0cq687mhpwq0pg7i32yc3hdrcx783r5i6k48gs56102m2701m3jw") (y #t)))

(define-public crate-fault-injection-1.0.6 (c (n "fault-injection") (v "1.0.6") (h "0hqvcd4n7sj4aj4f1h6bbjpp0qlppmzy8psnj98w1bsp2dxsgv7j") (y #t)))

(define-public crate-fault-injection-1.0.7 (c (n "fault-injection") (v "1.0.7") (h "12rc9mmfwhyxi183x3hg1584lsm6mk9yfra3aw8wlrdjhmapzjmf")))

(define-public crate-fault-injection-1.0.8 (c (n "fault-injection") (v "1.0.8") (h "178v3kq16709rf64r4v7dlixrxgpmnqfmsyvl1xkd84k1c43q5x9")))

(define-public crate-fault-injection-1.0.9 (c (n "fault-injection") (v "1.0.9") (h "19q9nfs6szis14wg5z5f4jsq0cd58mriy9nii6gydbc77gq32378")))

(define-public crate-fault-injection-1.0.10 (c (n "fault-injection") (v "1.0.10") (h "1lzysyins92zrknfjcnhcb2y88xhrmxdbz5iygggvhyy8r91fgcy")))

