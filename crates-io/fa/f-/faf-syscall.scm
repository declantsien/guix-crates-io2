(define-module (crates-io fa f- faf-syscall) #:use-module (crates-io))

(define-public crate-faf-syscall-1.0.0 (c (n "faf-syscall") (v "1.0.0") (h "13vpckfs1jryxd5dh0dvmxysf5jf4pbkhbdfbkg1f4v7r3wkkq9s")))

(define-public crate-faf-syscall-1.0.1 (c (n "faf-syscall") (v "1.0.1") (h "0xskag8nqpgkh1mymbc467dx2why5r4c7k1xybw9c0mnwgcpnv0s")))

(define-public crate-faf-syscall-2.0.0 (c (n "faf-syscall") (v "2.0.0") (h "0halmfzcchmgdlhmbbq6vg5b48w3f5hg4pwzriyy5mw5l9imvalq")))

(define-public crate-faf-syscall-3.0.0 (c (n "faf-syscall") (v "3.0.0") (h "1rbz6wx8b1hhyrz6xkhd1j3arzp4qa2bar58gdzllx5m5y0isimg")))

(define-public crate-faf-syscall-3.0.1 (c (n "faf-syscall") (v "3.0.1") (h "0ifqk1apr5pgyjn6rrgspq4jbwhpb49sl0qcpchzcgymwrafc4rx")))

(define-public crate-faf-syscall-3.0.2 (c (n "faf-syscall") (v "3.0.2") (h "0c589lj83wcl1qr17k8dja2hx9pyzwgwzid6w8xjbfbdaxdi0qvi")))

(define-public crate-faf-syscall-3.0.3 (c (n "faf-syscall") (v "3.0.3") (h "14hlywvsdkwf34pscpgn43n7p26cv1z86c553i3x80p4mjrjdlv0")))

