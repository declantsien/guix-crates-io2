(define-module (crates-io fa ir fair-baccarat) #:use-module (crates-io))

(define-public crate-fair-baccarat-0.1.0 (c (n "fair-baccarat") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "027gj0nhh63zl8wjlx9lvcd8hjibv33wggp1rsad5q831xls8k7k")))

(define-public crate-fair-baccarat-0.1.1 (c (n "fair-baccarat") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "1w7pfii6cnl0lh2ni8cwrybcbmj0q5f0f4wkfafvzky1ilcj49bx")))

(define-public crate-fair-baccarat-0.1.2 (c (n "fair-baccarat") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "1ydqv1z3wd61srrlmm07a8inpy0s7vc8xs2inw9zs6fgkj3qpj87")))

(define-public crate-fair-baccarat-0.1.3 (c (n "fair-baccarat") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)))) (h "0lkpwcnxrsixai9kiwglakzbpycra2lpjd79cbm661j2g9x8f0d2")))

(define-public crate-fair-baccarat-0.2.0 (c (n "fair-baccarat") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "1j8cj8mapp25g7559x7b9lzkz1m0q09g4qj71rxhr9qb4fid9mz5") (f (quote (("default" "console_error_panic_hook"))))))

