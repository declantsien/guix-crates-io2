(define-module (crates-io fa ir fair-rate-limiter) #:use-module (crates-io))

(define-public crate-fair-rate-limiter-0.1.0 (c (n "fair-rate-limiter") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "1qcw7fqv15l2jqhsfwix10lqqlx8qmi1mkx01zbj616rnd0z0sra")))

