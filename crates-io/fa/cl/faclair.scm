(define-module (crates-io fa cl faclair) #:use-module (crates-io))

(define-public crate-faclair-0.1.0 (c (n "faclair") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1gmxx9y8h1fynwsiwdh2wzdl8s93if4ibp08m0pjrfvbnkxv7prx")))

