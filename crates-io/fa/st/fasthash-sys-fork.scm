(define-module (crates-io fa st fasthash-sys-fork) #:use-module (crates-io))

(define-public crate-fasthash-sys-fork-0.4.2 (c (n "fasthash-sys-fork") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)) (d (n "raw-cpuid") (r "^10") (d #t) (k 1)))) (h "03n1n082ixvaamfrx5c71fib8v0hhlp5q6cxwnyrkz4xdarcf0i7") (f (quote (("xx") ("wy") ("t1ha") ("sse42" "sse41") ("sse41") ("spooky") ("native") ("murmur") ("mum") ("metro") ("meow") ("lookup3") ("highway") ("gen" "bindgen") ("farm") ("default" "native" "all") ("city") ("avx2" "avx") ("avx") ("all" "city" "farm" "highway" "lookup3" "meow" "metro" "mum" "murmur" "spooky" "t1ha" "wy" "xx") ("aes"))))))

