(define-module (crates-io fa st fastorm) #:use-module (crates-io))

(define-public crate-FastORM-0.1.0 (c (n "FastORM") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "fastorm-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "fastorm-trait") (r "^0.1.0") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("runtime-tokio" "sqlite" "macros" "any"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14vagnr3mr5s66f5q8v8c4bx5xg4cqd04g53n13aijdckz7iaq2w")))

