(define-module (crates-io fa st fastfloat) #:use-module (crates-io))

(define-public crate-fastfloat-0.1.0 (c (n "fastfloat") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0ry6fjgxjrqdaqd2n2rssjwl2asrwz3yqq5a04qdag5akykapyjp")))

(define-public crate-fastfloat-0.2.0 (c (n "fastfloat") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "18rgkgkk5md7xl99mpb1j0jszqk320rvgdigrsl04v7s5lfhgq80")))

(define-public crate-fastfloat-0.2.1 (c (n "fastfloat") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0d1l0yfj909pkk8pmb8khn17qn562n7s8rd9lwzhax8zzgj9prp2")))

(define-public crate-fastfloat-0.2.2 (c (n "fastfloat") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0n33k8qr41s834ka4452kdl56b526yibbgpimqjvzsvc6rcpp9ia")))

