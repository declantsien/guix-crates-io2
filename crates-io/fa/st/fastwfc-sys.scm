(define-module (crates-io fa st fastwfc-sys) #:use-module (crates-io))

(define-public crate-fastwfc-sys-0.1.0 (c (n "fastwfc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.42") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "01a6k8rxwmvprkps540q9xq44dgbslw2jyh4yv9qr07mys39bgdn")))

