(define-module (crates-io fa st fast_async_trait) #:use-module (crates-io))

(define-public crate-fast_async_trait-0.1.0 (c (n "fast_async_trait") (v "0.1.0") (d (list (d (n "fast_async_trait_proc") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 2)))) (h "0n0xpjcgjb9qyb9zvqnw5cmrinca1xz34znvq157kfrjpvnlqhmr")))

(define-public crate-fast_async_trait-0.1.1 (c (n "fast_async_trait") (v "0.1.1") (d (list (d (n "fast_async_trait_proc") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 2)))) (h "0j6yim67xr146vzxhw0ydw23pva3q2nll5fm8j5kr90anfm48awz")))

