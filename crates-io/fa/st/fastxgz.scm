(define-module (crates-io fa st fastxgz) #:use-module (crates-io))

(define-public crate-fastxgz-0.1.0 (c (n "fastxgz") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "1jf9f74lzs88s5mz3ph05rradnf7930csfr438nasds6w063kgry")))

(define-public crate-fastxgz-0.2.0 (c (n "fastxgz") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "07cjldvs46gqznga7avbai3x4225r72pa3rg2127r919ck2k7g0f")))

(define-public crate-fastxgz-0.3.0 (c (n "fastxgz") (v "0.3.0") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "0cqr8j2hmn7408hp9fx9csr05wwz60rrw1jwvz65bfgq8sqsysfq")))

