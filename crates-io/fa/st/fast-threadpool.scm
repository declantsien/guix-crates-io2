(define-module (crates-io fa st fast-threadpool) #:use-module (crates-io))

(define-public crate-fast-threadpool-0.1.0 (c (n "fast-threadpool") (v "0.1.0") (d (list (d (n "flume") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "oneshot") (r "^0.1.2") (d #t) (k 0)))) (h "0ycyfng6wpvg1yyvbbgd3yhqvini1am2mfafy3g3jiaz6zwm5npm")))

(define-public crate-fast-threadpool-0.1.1 (c (n "fast-threadpool") (v "0.1.1") (d (list (d (n "flume") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "oneshot") (r "^0.1.2") (d #t) (k 0)))) (h "14sp4hagqnvx8dfijq9jjghp5k9dhp45c8n5ybw3azj12zr6jgwy")))

(define-public crate-fast-threadpool-0.1.2 (c (n "fast-threadpool") (v "0.1.2") (d (list (d (n "flume") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "oneshot") (r "^0.1.2") (d #t) (k 0)))) (h "0qkws6l4w34jkkr6dja0nrapvxjvd4pcg620kylswbab8cydsraw")))

(define-public crate-fast-threadpool-0.2.0 (c (n "fast-threadpool") (v "0.2.0") (d (list (d (n "async-oneshot") (r "^0.4.2") (d #t) (k 0)) (d (n "flume") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0q5rg9khmb6h2p8i6l2i59zd9gyysn73yga3yvd9jfyg14d8fqwz")))

(define-public crate-fast-threadpool-0.2.1 (c (n "fast-threadpool") (v "0.2.1") (d (list (d (n "async-oneshot") (r "^0.4.2") (d #t) (k 0)) (d (n "flume") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "050y467211kmxmcdz1k8n7b23cqz6cpcsbam5wviw71bv7m7d7yg")))

(define-public crate-fast-threadpool-0.2.2 (c (n "fast-threadpool") (v "0.2.2") (d (list (d (n "async-oneshot") (r "^0.4.2") (d #t) (k 0)) (d (n "flume") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "04af8hxzbhzwxc74kkdaqpnr3jjr4701ma4bbzl90h899cpszdzp")))

(define-public crate-fast-threadpool-0.2.3 (c (n "fast-threadpool") (v "0.2.3") (d (list (d (n "async-oneshot") (r "^0.4.2") (d #t) (k 0)) (d (n "flume") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0smqqkbgr43rknpr9hjgid943rkc5kjbap3sj9jlm32dl3ryi185")))

(define-public crate-fast-threadpool-0.3.0 (c (n "fast-threadpool") (v "0.3.0") (d (list (d (n "async-oneshot") (r "^0.4.2") (d #t) (k 0)) (d (n "flume") (r "^0.10.0") (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0g8sj1x3w0490q29ch3vka70bscxyndy068wwr5knx029rvndjyc") (f (quote (("default" "async") ("async" "flume/async" "flume/eventual-fairness"))))))

