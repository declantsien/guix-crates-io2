(define-module (crates-io fa st fastnoise-lite) #:use-module (crates-io))

(define-public crate-fastnoise-lite-1.0.1 (c (n "fastnoise-lite") (v "1.0.1") (h "12vi82w1ai3lbqrqx1rqslgynrlvqkn7caf8hchi9jm25jmb6hnn") (f (quote (("f64"))))))

(define-public crate-fastnoise-lite-1.1.0 (c (n "fastnoise-lite") (v "1.1.0") (d (list (d (n "num-traits") (r "^0.2.16") (o #t) (k 0)))) (h "16psh1wcg6mhdplkhrjcq7d472f4yz1qmx8jzjjri7iwzngksaza") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("f64") ("default" "std"))))))

(define-public crate-fastnoise-lite-1.1.1 (c (n "fastnoise-lite") (v "1.1.1") (d (list (d (n "num-traits") (r "^0.2.18") (o #t) (k 0)))) (h "1h55gv4d3v1kkkdak21gq9zs9jrkrn00xmdw3075s7h8r4y3qpry") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("f64") ("default" "std"))))))

