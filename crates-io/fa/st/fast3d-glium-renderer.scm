(define-module (crates-io fa st fast3d-glium-renderer) #:use-module (crates-io))

(define-public crate-fast3d-glium-renderer-0.3.0 (c (n "fast3d-glium-renderer") (v "0.3.0") (d (list (d (n "fast3d") (r "^0.3.0") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (f (quote ("approx" "bytemuck"))) (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0hgky4y35l44i7lgslzk7ra4cjrsfsf83hqd4407fns6rswm5r9p")))

(define-public crate-fast3d-glium-renderer-0.3.1 (c (n "fast3d-glium-renderer") (v "0.3.1") (d (list (d (n "fast3d") (r "^0.3.1") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (f (quote ("approx" "bytemuck"))) (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "1qm28vcn8h5ywrydfg5a4wr67vrkzxgycq9f7inj31yzbspiz293")))

(define-public crate-fast3d-glium-renderer-0.4.0 (c (n "fast3d-glium-renderer") (v "0.4.0") (d (list (d (n "fast3d") (r "^0.4.0") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (f (quote ("approx" "bytemuck"))) (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "1xrd7dkhbk8jizz60rdhw1b4ciikpc708plzm86j52hi01xwj2sw")))

(define-public crate-fast3d-glium-renderer-0.4.1 (c (n "fast3d-glium-renderer") (v "0.4.1") (d (list (d (n "fast3d") (r "^0.4.1") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (f (quote ("approx" "bytemuck"))) (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "1srgzpw08lz6dd00gj85s8p7j6p4z3cnq87v73mdrws1ix1m15i0")))

(define-public crate-fast3d-glium-renderer-0.4.2 (c (n "fast3d-glium-renderer") (v "0.4.2") (d (list (d (n "fast3d") (r "^0.4.2") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (f (quote ("approx" "bytemuck"))) (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "0fi3cjw1sal1dxykaxz97lpv4cwyhgnwz7r7fgf4c1x32y760b60")))

(define-public crate-fast3d-glium-renderer-0.4.3 (c (n "fast3d-glium-renderer") (v "0.4.3") (d (list (d (n "fast3d") (r "^0.4.3") (d #t) (k 0)) (d (n "fast3d-gbi") (r "^0.4.3") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (f (quote ("approx" "bytemuck"))) (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "1i8vk3rn2x8hwkwd8ysp8ipxlr8czz5yw6gkgqnk77k17pj8jpdm")))

(define-public crate-fast3d-glium-renderer-0.4.4 (c (n "fast3d-glium-renderer") (v "0.4.4") (d (list (d (n "fast3d") (r "^0.4.4") (d #t) (k 0)) (d (n "fast3d-gbi") (r "^0.4.4") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (f (quote ("approx" "bytemuck"))) (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "1q1k00nqgrzd9kjhy5as5a4pgqxz9kggrhikfk8m8jccncvwmbpd")))

(define-public crate-fast3d-glium-renderer-0.5.0 (c (n "fast3d-glium-renderer") (v "0.5.0") (d (list (d (n "fast3d") (r "^0.5.0") (d #t) (k 0)) (d (n "fast3d-gbi") (r "^0.5.0") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (f (quote ("approx" "bytemuck"))) (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "170vvdlbkr44p6nr5v8j4gz11z1f60r39ch45jfi23q142jh1v7f")))

