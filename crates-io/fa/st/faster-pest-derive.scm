(define-module (crates-io fa st faster-pest-derive) #:use-module (crates-io))

(define-public crate-faster-pest-derive-0.1.0 (c (n "faster-pest-derive") (v "0.1.0") (d (list (d (n "pest_meta") (r "^2.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xbhf23x8rmx5kbpgxf3d6cgnpw7l1arpib68irqnjav0gcwighc")))

(define-public crate-faster-pest-derive-0.1.1 (c (n "faster-pest-derive") (v "0.1.1") (d (list (d (n "pest_meta") (r "^2.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zcwxj5i27r9yx7z0hl4xmxwjcpiz2k93ibvz7s0w3d5jl44jkfi")))

(define-public crate-faster-pest-derive-0.1.2 (c (n "faster-pest-derive") (v "0.1.2") (d (list (d (n "pest_meta") (r "^2.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mp99nmg160ypgswadlmay83inv2nwahji2y0jchyl0qabvmwc3j")))

(define-public crate-faster-pest-derive-0.1.3 (c (n "faster-pest-derive") (v "0.1.3") (d (list (d (n "pest_meta") (r "^2.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kica5ynbmy9kw7dgpwdz57bizs65w32n2sn3kx4imi9ihip35nj")))

