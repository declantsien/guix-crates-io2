(define-module (crates-io fa st fastchemail) #:use-module (crates-io))

(define-public crate-fastchemail-0.9.0 (c (n "fastchemail") (v "0.9.0") (d (list (d (n "asciiutils") (r "^0") (d #t) (k 0)))) (h "0q8ib3ybc88685yi2gcdwxx9mn33ggayb01hn1azrggx9y28v0p8") (y #t)))

(define-public crate-fastchemail-0.9.1 (c (n "fastchemail") (v "0.9.1") (d (list (d (n "asciiutils") (r "^0") (d #t) (k 0)))) (h "0a7jsz4xg4pr2b5z1a3dyy51i4sdrndyj72n10j13snmca5rsikx") (y #t)))

(define-public crate-fastchemail-0.9.2 (c (n "fastchemail") (v "0.9.2") (d (list (d (n "asciiutils") (r "^0") (d #t) (k 0)))) (h "1x03jplzd2fhkz9c8rdfis95ya5ymw8x6973jj0r6n6qbsay6mvs") (y #t)))

(define-public crate-fastchemail-0.9.3 (c (n "fastchemail") (v "0.9.3") (d (list (d (n "asciiutils") (r "^0") (d #t) (k 0)))) (h "0k5lq5qhigmvfhh370sqaw7rj937qkglf1v838hxbj882r6pafnm") (y #t)))

