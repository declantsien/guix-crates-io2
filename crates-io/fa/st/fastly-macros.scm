(define-module (crates-io fa st fastly-macros) #:use-module (crates-io))

(define-public crate-fastly-macros-0.3.0 (c (n "fastly-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13vig43sm1vcj3rr6zn1ma46bxb50bykb88swwpnc3ipcjsm2xmp")))

(define-public crate-fastly-macros-0.3.1 (c (n "fastly-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19plyfkpbndhqa0kyfrd7x1m3h037wscjvscc639njhpfrljmn16")))

(define-public crate-fastly-macros-0.3.2 (c (n "fastly-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0iqb8xch8g3wynpg654685v49j851dkq515j3prfg09l6x9gf0ma")))

(define-public crate-fastly-macros-0.3.3 (c (n "fastly-macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "026qkw8v7ndsg307p1w5n34sqvmd89lrjnmb01z6f3kf6bclm65r")))

(define-public crate-fastly-macros-0.4.0-beta1 (c (n "fastly-macros") (v "0.4.0-beta1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fia5q065wn445bgv9b6rzsx5mfm4nk8xhfv82r9j00fjhyx3g64")))

(define-public crate-fastly-macros-0.4.0-beta2 (c (n "fastly-macros") (v "0.4.0-beta2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bfndr82pgcfciycyx8a74wyh0g0gqfh5lbsr1yqyjb9yygfjwpi")))

(define-public crate-fastly-macros-0.4.0-beta3 (c (n "fastly-macros") (v "0.4.0-beta3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1aal8861wlds8gaxrf2f6c83rj5rbc6h8v6ldbrhivr5z2lw2qrv")))

(define-public crate-fastly-macros-0.4.0 (c (n "fastly-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ac22f4qg6sdgwdds2rfxl1b2aj0mwrzhyqh14f16hyfw10cq1cq")))

(define-public crate-fastly-macros-0.5.0 (c (n "fastly-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1044z45nx6l91p3jliqdvw1piqyk62qi7dn14cf7c9qyss9pyvzl")))

(define-public crate-fastly-macros-0.8.0 (c (n "fastly-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04h0v7b9c7lw0366jqz3qjb425k10zpnca2j2xwr20s4dqamyjcb")))

(define-public crate-fastly-macros-0.8.1 (c (n "fastly-macros") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17bikq5p8izmlkjw99z9ia8xfpjxbajijz4nk8gcnzpvnvfpkmcf")))

(define-public crate-fastly-macros-0.8.2 (c (n "fastly-macros") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j2g6ryj0s1r1zvvqa89ag8sgax3bqwvrbf1szdlqlx3w44l3cw3")))

(define-public crate-fastly-macros-0.8.3 (c (n "fastly-macros") (v "0.8.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "163xhqzbxg4495m8lnk28n05p72wx821kf5ghi2lr6mfcr5l2xv5")))

(define-public crate-fastly-macros-0.8.4 (c (n "fastly-macros") (v "0.8.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h8bs6diw5ys7h2xyhnnixf37c2a4vi9m54qan5l5m74fac7xxx1")))

(define-public crate-fastly-macros-0.8.5 (c (n "fastly-macros") (v "0.8.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0skz5jwwz6rzg48j1jq8vzil9a3qn49n194b6fwa56707g0a293r")))

(define-public crate-fastly-macros-0.8.6 (c (n "fastly-macros") (v "0.8.6") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06mhj9jp5rljvxdbiia8icf8mf5gh9sbnxzg6s2qzhkdklvx95nv")))

(define-public crate-fastly-macros-0.8.7 (c (n "fastly-macros") (v "0.8.7") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16gxfv1fbs201kq9njw9kip95cb2bk658p05siml7l9vs1w0w1lr")))

(define-public crate-fastly-macros-0.8.8 (c (n "fastly-macros") (v "0.8.8") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zfyc5dcvk703w842asvwl98zqsr1aici3n5sj4vilv4a5qr9kzi")))

(define-public crate-fastly-macros-0.8.9 (c (n "fastly-macros") (v "0.8.9") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vrfgqph3gzdd6nixzy11lsrli7f8ccmj67x9fw8q5091jcb5lg3")))

(define-public crate-fastly-macros-0.9.0-pre1 (c (n "fastly-macros") (v "0.9.0-pre1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hy3d3a84f7qvivfkx8rxr2c0hhhvlx3lc72b9klzrzx2yaqnvnw")))

(define-public crate-fastly-macros-0.9.0 (c (n "fastly-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01y5m7d60j46cls8mvqglvmcrn1al2d07js1rgzs5h7311afl739")))

(define-public crate-fastly-macros-0.9.1 (c (n "fastly-macros") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sa9f9zybixpdma3w4c8xlbjspb367lm30r5cplbifp93y466lbb")))

(define-public crate-fastly-macros-0.9.2 (c (n "fastly-macros") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0np7kbbl836p2zhajxqylhdlp26f0bczwd6jlvszjnvs8mr6d1bj")))

(define-public crate-fastly-macros-0.9.3 (c (n "fastly-macros") (v "0.9.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "146af6mqg2i2zzbyvxpbj7mrb668ygxrw6j2il29krd5wb6lf0vk")))

(define-public crate-fastly-macros-0.9.4 (c (n "fastly-macros") (v "0.9.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1680jics30yfjc99kyw17q4lp07g7pp0m4f87vyf5b0h09s8vkrj")))

(define-public crate-fastly-macros-0.9.5 (c (n "fastly-macros") (v "0.9.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hp754pxikdxdiph8g1gjl0raa1v6rx6hl4g8k8175hwiqzfi3nn")))

(define-public crate-fastly-macros-0.9.6 (c (n "fastly-macros") (v "0.9.6") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00fz85alyk7ig0vzq9zg632axckc00j45k76brci9iz7k9bywz8b") (y #t)))

(define-public crate-fastly-macros-0.9.7 (c (n "fastly-macros") (v "0.9.7") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rlgkflzz6jk9mlhxap00n9ik3s02qsd57632qisgjb90rmvrz7w")))

(define-public crate-fastly-macros-0.9.8 (c (n "fastly-macros") (v "0.9.8") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j83xdyxjlfawlnvcx52yly9j6vy389gdbjk5z40svylsfw773in")))

(define-public crate-fastly-macros-0.9.9 (c (n "fastly-macros") (v "0.9.9") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15gsr25f3bf5b9j5n6pbpf52gyikabqxkjk0add68v9s29hhgvzr")))

(define-public crate-fastly-macros-0.9.10 (c (n "fastly-macros") (v "0.9.10") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01ql2b2x3hnnlg774qj161340hbwik9w4lqkby0nl8nxbsa54cjq")))

(define-public crate-fastly-macros-0.9.11 (c (n "fastly-macros") (v "0.9.11") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "061ibmniqdsy8sm5d0nvvrsg8py0z24fkyxlz3rdrz7gvv4d3mps")))

(define-public crate-fastly-macros-0.9.12 (c (n "fastly-macros") (v "0.9.12") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x5myv8m0sqy406qvyvsdfsg7cg8az6f7kl1n8w12idwx49dxmfm")))

(define-public crate-fastly-macros-0.10.0 (c (n "fastly-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11j4vrx1srbw5z0pxwx2hfb34wh4mp2q1imf57hnrcf4h84wy1pb")))

(define-public crate-fastly-macros-0.10.1 (c (n "fastly-macros") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1avkaxk4a9wx59pxf7dhlnqg0adzib3bfkv41z39aa7d7586bjcv")))

