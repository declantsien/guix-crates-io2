(define-module (crates-io fa st fastcache) #:use-module (crates-io))

(define-public crate-fastcache-0.1.0 (c (n "fastcache") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "dashmap") (r "^5") (f (quote ("inline"))) (d #t) (k 0)))) (h "1pway0g2p7blxxn5cfa66pfpxbyqhqv9kixqcrls91cl1rcgpny0")))

(define-public crate-fastcache-0.1.1 (c (n "fastcache") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "dashmap") (r "^5") (f (quote ("inline"))) (d #t) (k 0)))) (h "07gwjaxki3w2n80l1gq16xchzny1yg9bhv2wna1gamjyn9i7c69p")))

(define-public crate-fastcache-0.1.2 (c (n "fastcache") (v "0.1.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "dashmap") (r "^5") (f (quote ("inline"))) (d #t) (k 0)))) (h "1hcfxg1ldskcadi85ni0h616a0jgyh7h3crhfqyssl9bdl6zdwn9")))

(define-public crate-fastcache-0.1.3 (c (n "fastcache") (v "0.1.3") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "dashmap") (r "^5") (f (quote ("inline"))) (d #t) (k 0)))) (h "110qq5bmg9qkf0488sl4c3siirvc96zb7bglcyfjnfvkx63qgfx5")))

(define-public crate-fastcache-0.1.4 (c (n "fastcache") (v "0.1.4") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "dashmap") (r "^5") (f (quote ("inline"))) (d #t) (k 0)))) (h "0081lyqfwjjwicydw4qbrf0v66zrmd8w49r47fk7an8zs63v9zw9")))

(define-public crate-fastcache-0.1.5 (c (n "fastcache") (v "0.1.5") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "dashmap") (r "^5") (f (quote ("inline"))) (d #t) (k 0)))) (h "1qh7ayajdp516v7c7aaw8229ns1b88wdsm3nmmxazxl5d4c8prsl")))

