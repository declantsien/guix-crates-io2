(define-module (crates-io fa st fastkill) #:use-module (crates-io))

(define-public crate-fastkill-1.0.0 (c (n "fastkill") (v "1.0.0") (d (list (d (n "inquire") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.3.15") (d #t) (k 0)))) (h "13bcv1js8rvwfq7hk4b0i04dvsalakz4r9zhg0y8wix0jsh9d3h4")))

(define-public crate-fastkill-1.0.1 (c (n "fastkill") (v "1.0.1") (d (list (d (n "inquire") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.3.15") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1fbiiag3358q4fwm61mdbxxcijfkmsf8wghxyw8l8yazhqhiak4z")))

(define-public crate-fastkill-1.0.2 (c (n "fastkill") (v "1.0.2") (d (list (d (n "inquire") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "01cp8xlslvqd348z3hq269bxyagg1hzfsn40gsip4ffjjs40pigf")))

(define-public crate-fastkill-1.0.3 (c (n "fastkill") (v "1.0.3") (d (list (d (n "inquire") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("iphlpapi" "tcpmib" "minwindef" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1y24cdziaywz98c8ajx3ikb1lkypbdnxdk6zi7df69vq5b0v5h9x")))

