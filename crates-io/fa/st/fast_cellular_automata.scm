(define-module (crates-io fa st fast_cellular_automata) #:use-module (crates-io))

(define-public crate-fast_cellular_automata-0.1.0 (c (n "fast_cellular_automata") (v "0.1.0") (d (list (d (n "gif") (r "^0.12.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "102l0niv4gmnr6isjfcjarybynvd3cqacaxaidxbd2lpp8r48ms9")))

