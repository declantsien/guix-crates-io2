(define-module (crates-io fa st fast_arrays) #:use-module (crates-io))

(define-public crate-fast_arrays-0.1.0 (c (n "fast_arrays") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0jpm1qgd0wp5hbgkdhril6iqj3p0jjk3z9lp6hqcj218crmqjjf7")))

(define-public crate-fast_arrays-0.1.1 (c (n "fast_arrays") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1307zhsgc959m0af1rxwzc1glg4rxy86drp1qvhg48f3460mgm7p")))

(define-public crate-fast_arrays-0.1.2 (c (n "fast_arrays") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "029gsnzap0429r557i7a4df60b76jllg0rlmwznisgai9yhy391v")))

(define-public crate-fast_arrays-0.1.3 (c (n "fast_arrays") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0w91pr68sshsrdbgz1315740pq4bjdnsyrb0dw6zvpvplqg7vadj")))

(define-public crate-fast_arrays-0.1.4 (c (n "fast_arrays") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0vik4szl7qxilqwh211r9p1j5abvm76km9prd08mhgslzzc8j69p")))

(define-public crate-fast_arrays-0.1.5 (c (n "fast_arrays") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0q2hljmyrsp5l9rk5yla5j7cnkk6nzl1dznqajv1n84b3clk8bsx")))

(define-public crate-fast_arrays-0.1.6 (c (n "fast_arrays") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "03rqzsn4m8bx2ccziw675g54arvx33m33zfnvkv9wj364gjd4gyz")))

(define-public crate-fast_arrays-0.1.7 (c (n "fast_arrays") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "10xarcc9yxsaklzi5zr1flyi25qabsak3wdi0vz33ss42wwpcggf")))

(define-public crate-fast_arrays-0.1.8 (c (n "fast_arrays") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1hb6nab3gp27sgajflnxlsxgc0b0rbfiyb13qar9f2lkz82faa63")))

(define-public crate-fast_arrays-0.1.9 (c (n "fast_arrays") (v "0.1.9") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0pscljxd1nh7rlnq2d78jpk2wkbz5zxwr0n1fcn1cgq7y3znfks2")))

(define-public crate-fast_arrays-0.1.10 (c (n "fast_arrays") (v "0.1.10") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "09i6njz73gxrizkxvg3iaxhpfdz6w6axs32k7wl27j8851slk2sa")))

(define-public crate-fast_arrays-0.1.11 (c (n "fast_arrays") (v "0.1.11") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1kygfyx6xa5fvg29pip6x4jf837nxmk34qri3fgahgmw2qq88xj3")))

(define-public crate-fast_arrays-0.1.12 (c (n "fast_arrays") (v "0.1.12") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1ab5wzkkwcmflsfb615adwy7m3wxvbhi224gmfg9045hxmr8hpfr")))

(define-public crate-fast_arrays-0.1.13 (c (n "fast_arrays") (v "0.1.13") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "168mm4916h45186x2myqj2m9kvl3v79ca4wfci7b8n2s8z4nkhf9")))

(define-public crate-fast_arrays-0.1.14 (c (n "fast_arrays") (v "0.1.14") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1ja3j5pqd1vvy5m80nm1aiwnbc67wq32170khr6xiayq8bljmxa2")))

(define-public crate-fast_arrays-0.1.15 (c (n "fast_arrays") (v "0.1.15") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0dbzq9rbij2skpqyl5h2slkpalhpjqq8ahfipwacx1hryam310sh")))

(define-public crate-fast_arrays-0.1.16 (c (n "fast_arrays") (v "0.1.16") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1y8pii6gq376l4mqlwwknp2iiw5x7fnkzj4s6467g38yb698zgwf")))

(define-public crate-fast_arrays-0.1.17 (c (n "fast_arrays") (v "0.1.17") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "03wamkbx8g86h0876baazv493akvg3xa45jghdm7y0bd6p21wzrv")))

(define-public crate-fast_arrays-0.1.18 (c (n "fast_arrays") (v "0.1.18") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1hm3qhp8cy5lcln2f78ng5qs4fdqzxggmq006bjqjx3d3bh5c836")))

(define-public crate-fast_arrays-0.1.19 (c (n "fast_arrays") (v "0.1.19") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0xly2pmgqy1zbsqbnsn6667fbdck0pcbprysph1mg08103qvc2gi")))

(define-public crate-fast_arrays-0.1.20 (c (n "fast_arrays") (v "0.1.20") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08pl75nh28576y5i90c1vp8b4vbi7fi9bvw441vsa4pmvy0pcaab")))

(define-public crate-fast_arrays-0.1.21 (c (n "fast_arrays") (v "0.1.21") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0k1k7zij8m5q2w5k1spa1gm0gjblhp3vr04ncbqx37ar0nhhkrln")))

(define-public crate-fast_arrays-0.1.22 (c (n "fast_arrays") (v "0.1.22") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16gvw3pj5a6cl8r1q7q9l1d0nd7ndrbzr5rcx87x8mjhsjf9b5jf")))

(define-public crate-fast_arrays-0.1.23 (c (n "fast_arrays") (v "0.1.23") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1wvhg0nm9amdvivfccbkrj212gqkacl9nr8hnbvyxcxlbkxcdl7r")))

