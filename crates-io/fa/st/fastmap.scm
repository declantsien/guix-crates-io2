(define-module (crates-io fa st fastmap) #:use-module (crates-io))

(define-public crate-fastmap-0.1.0 (c (n "fastmap") (v "0.1.0") (d (list (d (n "tetsy-plain-hasher") (r "^0.2.1") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "0rlsy0zgd3n4qpaw050m532q68b7g9gk7aldncqs76lsijynksjr")))

