(define-module (crates-io fa st fast_polynomial) #:use-module (crates-io))

(define-public crate-fast_polynomial-0.1.0-alpha.0 (c (n "fast_polynomial") (v "0.1.0-alpha.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1f3vb642ljcavfg1zbqdq8xh8jvwv8r85x90mpmmf132ngn445z8") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "fma" "std")))) (y #t)))

(define-public crate-fast_polynomial-0.1.0 (c (n "fast_polynomial") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1nny7x5sxjf5rw7c6j0i603mghf9chb94jns0l0fz3hhw2mic7l3") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "fma" "std"))))))

