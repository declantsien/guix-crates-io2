(define-module (crates-io fa st fast_enum_conversion_macro) #:use-module (crates-io))

(define-public crate-fast_enum_conversion_macro-0.1.0 (c (n "fast_enum_conversion_macro") (v "0.1.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "printing" "extra-traits"))) (d #t) (k 0)) (d (n "template-quote") (r "^0.3.1") (d #t) (k 0)))) (h "11w1vc9lsnrzf93i6id2qwnfqlnqjz2v3wir4vwpv2msvkpfips1")))

