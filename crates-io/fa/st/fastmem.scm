(define-module (crates-io fa st fastmem) #:use-module (crates-io))

(define-public crate-fastmem-0.0.0 (c (n "fastmem") (v "0.0.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1j4fyghxplf34nj2ddqzrvgcndzm9phv0g7fqpklbhjfsd2kxvqj") (f (quote (("enable" "rayon"))))))

(define-public crate-fastmem-0.1.0 (c (n "fastmem") (v "0.1.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1r1806awqdmjx6p8j9v5hrlfzlmwaqaxpp7nayfl7cnh7fnv36lx") (f (quote (("enable" "rayon"))))))

