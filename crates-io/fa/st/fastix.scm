(define-module (crates-io fa st fastix) #:use-module (crates-io))

(define-public crate-fastix-0.1.0 (c (n "fastix") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.2") (d #t) (k 2)))) (h "1mzk65mg8vx0hz39xis6zqdmq56abhmza656gn9pgmlsn151gpx2")))

