(define-module (crates-io fa st fastar) #:use-module (crates-io))

(define-public crate-fastar-0.1.0 (c (n "fastar") (v "0.1.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "platter-walk") (r "^0.1.0") (d #t) (k 0)) (d (n "reapfrog") (r "^0.1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.11") (d #t) (k 0)))) (h "1vyn94aph7k1v16rjmz79dh3zk2bgxa4p4gvwfmlzj477rwyqw1s")))

(define-public crate-fastar-0.1.1 (c (n "fastar") (v "0.1.1") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "platter-walk") (r "^0.1.0") (d #t) (k 0)) (d (n "reapfrog") (r "^0.1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.11") (d #t) (k 0)))) (h "1wy267d2wya9k312aw6da3ak9yjswpgky6hc56ik3ibwsx51rnxb") (f (quote (("system_alloc"))))))

(define-public crate-fastar-0.1.3 (c (n "fastar") (v "0.1.3") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "platter-walk") (r "^0.1.2") (d #t) (k 0)) (d (n "reapfrog") (r "^0.2.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.11") (d #t) (k 0)))) (h "10cxrq0x0s48jwjhfgi147ka3npxaa178zmkbiv64260smhmig0a") (f (quote (("system_alloc"))))))

(define-public crate-fastar-0.1.4 (c (n "fastar") (v "0.1.4") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "nix") (r "^0.8.1") (d #t) (k 0)) (d (n "platter-walk") (r "^0.1.2") (d #t) (k 0)) (d (n "reapfrog") (r "^0.2.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.13") (d #t) (k 0)))) (h "00h1i6i8b13272zh5vj4hfwrfhz7312n5xm13j17n9lp36fyk0bj") (f (quote (("system_alloc"))))))

(define-public crate-fastar-0.1.5 (c (n "fastar") (v "0.1.5") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.5") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "platter-walk") (r "^0.1.3") (d #t) (k 0)) (d (n "reapfrog") (r "^0.2.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.22") (d #t) (k 0)))) (h "134fc087bq161lfzcma2dld8cwzc49fn74bfx3vygxmr6hgr8yic")))

(define-public crate-fastar-0.2.0 (c (n "fastar") (v "0.2.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.5") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "platter-walk") (r "^0.1.3") (d #t) (k 0)) (d (n "reapfrog") (r "^0.2.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1n2i602jglgavlp6ni7m05kdlsv97apgbm1g9qd1z6yqkg7bv1l0")))

