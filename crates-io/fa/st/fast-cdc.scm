(define-module (crates-io fa st fast-cdc) #:use-module (crates-io))

(define-public crate-fast-cdc-0.1.0 (c (n "fast-cdc") (v "0.1.0") (d (list (d (n "cdchunking") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "fake") (r "^2.4") (d #t) (k 2)))) (h "136byllbznyy11flzfg0v2a9x6wmhkblab50irnqhvg4zyx3sbh5") (r "1.66")))

