(define-module (crates-io fa st fast-map) #:use-module (crates-io))

(define-public crate-fast-map-0.1.0 (c (n "fast-map") (v "0.1.0") (d (list (d (n "fast-map-derive") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "0nhpxn6asvifivicwzyn0db44v8gqg721plxl42m6wj8293npiiw")))

(define-public crate-fast-map-0.1.1 (c (n "fast-map") (v "0.1.1") (d (list (d (n "fast-map-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "05qkiy6b9plamxn1h0gfzfml10fl8f6l4803fyxlx805brcbwil1")))

(define-public crate-fast-map-0.1.2 (c (n "fast-map") (v "0.1.2") (d (list (d (n "fast-map-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "0581rb2biqn59xclgl3spf339khp2r9faj7hnvp8d8ra754i6sbf")))

(define-public crate-fast-map-0.1.3 (c (n "fast-map") (v "0.1.3") (d (list (d (n "fast-map-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "1p7fnp1hyircrz7i78q95pi9kxpasiz9vwz4ql5qkwl4x92iv66w")))

(define-public crate-fast-map-0.1.4 (c (n "fast-map") (v "0.1.4") (d (list (d (n "fast-map-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "0awkqy9did172w7vvcxhcvrn9an2jcwaj2ad9ai6m9vijzqzc3xs") (f (quote (("no-build"))))))

(define-public crate-fast-map-0.1.5 (c (n "fast-map") (v "0.1.5") (d (list (d (n "fast-map-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "18g9v4cls26gjgf7ffvix5wq59zsnwvhbkynksgs3khgwb55fshj")))

(define-public crate-fast-map-0.1.6 (c (n "fast-map") (v "0.1.6") (d (list (d (n "fast-map-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "15i3c3klicn2vnmnyfc6bzay83q8diisw7jhyldy3agwjv1smqjs")))

(define-public crate-fast-map-0.2.0 (c (n "fast-map") (v "0.2.0") (d (list (d (n "fast-map-derive") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "1n0gwgr4xbsv1q4jhhdkd7i0mrf7myjifi6dkmma5dk9wslb1010")))

(define-public crate-fast-map-0.2.1 (c (n "fast-map") (v "0.2.1") (d (list (d (n "fast-map-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)))) (h "004q71v83i8i7wxnrsqs0zpzy4vc6qwz2kb8pn5s4af8fj9misi4")))

