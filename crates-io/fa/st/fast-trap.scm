(define-module (crates-io fa st fast-trap) #:use-module (crates-io))

(define-public crate-fast-trap-0.0.0 (c (n "fast-trap") (v "0.0.0") (h "09d3f7zfchyswigp9yz0ssv26gnv9b92lfxp3gk778d8aq972598") (f (quote (("riscv-s") ("riscv-m"))))))

(define-public crate-fast-trap-0.0.1 (c (n "fast-trap") (v "0.0.1") (h "0k2lyx416zm6h2qsdsa99g974mqb2kx4hxkqsa5cj39fvjx6kgkz") (f (quote (("riscv-s") ("riscv-m"))))))

