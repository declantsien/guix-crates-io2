(define-module (crates-io fa st faster-pest) #:use-module (crates-io))

(define-public crate-faster-pest-0.1.0 (c (n "faster-pest") (v "0.1.0") (d (list (d (n "faster-pest-derive") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1v5chgdb4m2jda0g9v0xkbiwd8hxyni7c8crzl8mx5kma1fni392")))

(define-public crate-faster-pest-0.1.1 (c (n "faster-pest") (v "0.1.1") (d (list (d (n "faster-pest-derive") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bhd1xf0z9g9x12gy5lpbcd6c42c0gppfld5sxk3xmxxjw0c8239")))

(define-public crate-faster-pest-0.1.2 (c (n "faster-pest") (v "0.1.2") (d (list (d (n "faster-pest-derive") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1xv48wv3gicv0z1aqqznm28297ndyf2sk2s0asxxqshf2acq0svi")))

(define-public crate-faster-pest-0.1.3 (c (n "faster-pest") (v "0.1.3") (d (list (d (n "faster-pest-derive") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11yl8qrvkdazk2iwywdgklclxhpx9ybspzxs8p9zky9rjaysmnn2")))

(define-public crate-faster-pest-0.1.4 (c (n "faster-pest") (v "0.1.4") (d (list (d (n "faster-pest-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11g04dkh00r3pnf7giknpxy35g20vgbl9vf9v2fv8hkq0k7pgrkh")))

