(define-module (crates-io fa st fast-srgb8) #:use-module (crates-io))

(define-public crate-fast-srgb8-0.1.0 (c (n "fast-srgb8") (v "0.1.0") (h "0bd17wffj8yl60kyv4i1k490jjapv0s03bh0bn4i249irh8yhjcj")))

(define-public crate-fast-srgb8-1.0.0 (c (n "fast-srgb8") (v "1.0.0") (h "18g6xwwh4gnkyx1352hnvwagpv0n4y98yp2llm8vyvwxh487abnx")))

