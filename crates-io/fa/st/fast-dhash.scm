(define-module (crates-io fa st fast-dhash) #:use-module (crates-io))

(define-public crate-fast-dhash-0.1.0 (c (n "fast-dhash") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)))) (h "01rxn677d2shy3bkygj8s3zg1050w2bvghvb59mjdb2xfl0g5lmb")))

