(define-module (crates-io fa st fastgraph) #:use-module (crates-io))

(define-public crate-fastgraph-0.1.0 (c (n "fastgraph") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1v4hwc1jfdq5hswakwkcy4d56rghjn093b94b51102jnnyc3vh7g")))

(define-public crate-fastgraph-0.1.1 (c (n "fastgraph") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "13damc4nbv1l1p1yd8j0iwlr7nbi5z2nffissp66xry0i8yd436z")))

(define-public crate-fastgraph-0.1.2 (c (n "fastgraph") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "07qzsylfz5y492pg34zvx896a78dh2wdhy4qb1c6j35yqr6ca6r7")))

(define-public crate-fastgraph-0.1.21 (c (n "fastgraph") (v "0.1.21") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0wpxk01f23if8632051vjm5w2m9cxknx00afcpnjcvqfppf2vpcd")))

