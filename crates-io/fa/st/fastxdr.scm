(define-module (crates-io fa st fastxdr) #:use-module (crates-io))

(define-public crate-fastxdr-1.0.0 (c (n "fastxdr") (v "1.0.0") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "185pjlqnczgma5lfibm9l9lfbi34gshi0dsvr2llxb9fchqilwjg")))

(define-public crate-fastxdr-1.0.1 (c (n "fastxdr") (v "1.0.1") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0k40hhz1hlns005h3xhrn8493qvb38lzb5dshlcf6ms8f1sxcs1j")))

(define-public crate-fastxdr-1.0.2 (c (n "fastxdr") (v "1.0.2") (d (list (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "04s8drkv2gkw44qqkzffzgr7flymj03pl3kr685gza53w6jghi10")))

