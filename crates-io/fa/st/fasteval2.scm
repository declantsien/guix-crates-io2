(define-module (crates-io fa st fasteval2) #:use-module (crates-io))

(define-public crate-fasteval2-2.0.0 (c (n "fasteval2") (v "2.0.0") (h "1b3lsnasz9lrbp0qyx3jpm5qxgppiwmgb3cmg4j5gj80ma5zabyk") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval2-2.0.1 (c (n "fasteval2") (v "2.0.1") (h "0qzwdcwc1r9xbmdxg7mqby8xcz6qyywd8i8fhrimsrfig13f6as4") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval2-2.0.2 (c (n "fasteval2") (v "2.0.2") (h "0248gc17vkrx2wqjhr6r1qf3swsjh5kqm7l9vnfwa8a2xl6c7bgc") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval2-2.0.3 (c (n "fasteval2") (v "2.0.3") (h "0knrarqijnv5xay8mnylw3dysq1hrf2cah6sjfqafvmr5yg5qzfd") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval2-2.0.4 (c (n "fasteval2") (v "2.0.4") (h "1fia4552j2jisrw05fn2bf9ysfv0bljw3hdlawfxcy2kbdnrdvyj") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval2-2.0.5 (c (n "fasteval2") (v "2.0.5") (h "1nmkkf8nqn3hmly8nbkrc5q78lf7ib3f4dz6kknw8g022vn34rbx") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval2-2.0.6 (c (n "fasteval2") (v "2.0.6") (h "1alqm08nznr3d4wjnii8bd98bnh445d7mmivpl39jgwgmf50q90d") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval2-2.1.0 (c (n "fasteval2") (v "2.1.0") (h "0r7glwwz9f8baasphsyskxk0byf0l2c2df342cs38fairfsd35lv") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval2-2.1.1 (c (n "fasteval2") (v "2.1.1") (h "1y2j3dn35yvgnqm5iwhwxzxn8706fzppx4l1fcmirr82k0ijsgv9") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

