(define-module (crates-io fa st fastid) #:use-module (crates-io))

(define-public crate-fastid-0.1.0 (c (n "fastid") (v "0.1.0") (h "1k6wzscdh7w263ipzm4iaazsy2bimsq5rn8gl4lgmhkiiywp3nyc")))

(define-public crate-fastid-0.1.1 (c (n "fastid") (v "0.1.1") (h "0j9i2wb97ck3z7bxz33hgay3zdir0zwv7d8xl49df999k9ldma79")))

(define-public crate-fastid-0.2.0 (c (n "fastid") (v "0.2.0") (d (list (d (n "base62") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0nrx2qf9xmk40mh2brnn25ydkj10mgwnvxxq9rm3hi4x757vbygi") (f (quote (("guid" "uuid") ("default")))) (y #t)))

(define-public crate-fastid-0.2.1 (c (n "fastid") (v "0.2.1") (d (list (d (n "base62") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1dflnzq5a8ms85582y9wni3iz13jj4wsy054g02xbmglfn33igc7") (f (quote (("guid" "uuid") ("default"))))))

(define-public crate-fastid-0.2.2 (c (n "fastid") (v "0.2.2") (d (list (d (n "base62") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0lhjav7q3f2mrv4j5z2s2qgawbvachiw7qdzy5cig77sxlwmd81b") (f (quote (("guid" "uuid") ("default"))))))

(define-public crate-fastid-0.3.0 (c (n "fastid") (v "0.3.0") (d (list (d (n "base62") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0dbi603akycpnmcw6baqz3ncf2i70jf4xlffj7kyp5w30xy346ag") (f (quote (("guid" "uuid") ("default"))))))

(define-public crate-fastid-0.3.1 (c (n "fastid") (v "0.3.1") (d (list (d (n "base62") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1dhprb7sz21jf31sh84jkz9h859pzym8r4ibbnar8y0k4hlsrv3l") (f (quote (("guid" "uuid") ("default"))))))

(define-public crate-fastid-0.3.2 (c (n "fastid") (v "0.3.2") (d (list (d (n "base62") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1ck8sf6158dy4fyfrjr9m23c48qkaq11yq9wfy7x3fd4jzbfvl98") (f (quote (("guid" "uuid") ("default"))))))

(define-public crate-fastid-0.3.3 (c (n "fastid") (v "0.3.3") (d (list (d (n "base62") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.6") (o #t) (d #t) (k 0)))) (h "151qrka07pal92rg6n1c6vljdxhlvv2aiiq1l3jd1qwsm2s50qvh") (f (quote (("guid" "uuid") ("default"))))))

