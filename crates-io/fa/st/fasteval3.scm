(define-module (crates-io fa st fasteval3) #:use-module (crates-io))

(define-public crate-fasteval3-3.0.0 (c (n "fasteval3") (v "3.0.0") (h "0s29ds8jvfjhqrvf5b3ysgwdf86hszk7234qvnq6d0w649h0nrqv") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval3-3.0.1 (c (n "fasteval3") (v "3.0.1") (h "1h32bayw5jg0qb77rp5kqw9vg1pmvb6hjn69cq9vh152jzy4vgcf") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

