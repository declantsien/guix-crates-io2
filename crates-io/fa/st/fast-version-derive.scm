(define-module (crates-io fa st fast-version-derive) #:use-module (crates-io))

(define-public crate-fast-version-derive-0.1.0 (c (n "fast-version-derive") (v "0.1.0") (d (list (d (n "fast-version-core") (r "^0.1.0") (d #t) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "1hyqr4y9w3zmq1l7znvwmh3y2c441j7h5nnhsckp4vdsjrx1lw5l")))

(define-public crate-fast-version-derive-0.1.1 (c (n "fast-version-derive") (v "0.1.1") (d (list (d (n "fast-version-core") (r "^0.1.1") (d #t) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0w91x9d9pbfgcd9n3hg9z7c7csz66isbi7p7djq8fnh4bi4c00fq")))

(define-public crate-fast-version-derive-0.1.2 (c (n "fast-version-derive") (v "0.1.2") (d (list (d (n "fast-version-core") (r "^0.2.1") (d #t) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "1rrw33v0xkqkb7bkh5bqysv47jvq8f4c6bsnql9c52k9g22vw1m3")))

(define-public crate-fast-version-derive-0.1.3 (c (n "fast-version-derive") (v "0.1.3") (d (list (d (n "fast-version-core") (r "^0.2.3") (d #t) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "01i0ihg1m71lj57s3sw7m3574klx76rbflw8yr1z8k6jb9pj554c")))

