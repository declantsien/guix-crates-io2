(define-module (crates-io fa st fasta_windows) #:use-module (crates-io))

(define-public crate-fasta_windows-0.2.3 (c (n "fasta_windows") (v "0.2.3") (d (list (d (n "bio") (r "^0.41.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "012d30pa6y2lx70z3iwjbq0kn40wgjbc18hyxnfkm2k6pryc7553")))

(define-public crate-fasta_windows-0.2.4 (c (n "fasta_windows") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "bio") (r "^0.41.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "18i1zb1zx0hypk132m7krp95jk3s6v384naj6kh2b7h8q56mlnx4")))

