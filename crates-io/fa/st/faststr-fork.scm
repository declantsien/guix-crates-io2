(define-module (crates-io fa st faststr-fork) #:use-module (crates-io))

(define-public crate-faststr-fork-0.2.18 (c (n "faststr-fork") (v "0.2.18") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itoa") (r "^1") (o #t) (d #t) (k 0)) (d (n "redis") (r "^0.25") (o #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "simdutf8") (r "^0.1") (f (quote ("aarch64_neon"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1x9qxjicdrj0nhi30qfmq6xn5zfpz7205q8vgy9v28ad0fr626g4") (f (quote (("std" "serde/std") ("serde-unsafe" "serde") ("redis-unsafe" "redis") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde") ("redis" "dep:redis" "itoa"))))))

