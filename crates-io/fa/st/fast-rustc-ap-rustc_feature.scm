(define-module (crates-io fa st fast-rustc-ap-rustc_feature) #:use-module (crates-io))

(define-public crate-fast-rustc-ap-rustc_feature-1.0.0 (c (n "fast-rustc-ap-rustc_feature") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc_data_structures") (r "^1.0.0") (d #t) (k 0) (p "fast-rustc-ap-rustc_data_structures")) (d (n "rustc_span") (r "^1.0.0") (d #t) (k 0) (p "fast-rustc-ap-rustc_span")))) (h "08n9sgz4j8vh22741szw2nag4wcvahmpgxlh202mcgiipcfm127j")))

