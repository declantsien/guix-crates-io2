(define-module (crates-io fa st fast-surface-nets) #:use-module (crates-io))

(define-public crate-fast-surface-nets-0.1.0 (c (n "fast-surface-nets") (v "0.1.0") (d (list (d (n "glam") (r "^0.18") (d #t) (k 0)) (d (n "ndshape") (r "^0.1") (d #t) (k 0)))) (h "043dgxd2hk8s3v9x3vazz84c4ibyvz3ybj6qzd1y9pnvfpxprdb1")))

(define-public crate-fast-surface-nets-0.2.0 (c (n "fast-surface-nets") (v "0.2.0") (d (list (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "ndshape") (r "^0.3") (d #t) (k 0)))) (h "1lrsl0d5lkka7zxrkhwj6l2b9r5z5p4jgsr9kckfnxzfby9wccnk")))

