(define-module (crates-io fa st fast-rustup) #:use-module (crates-io))

(define-public crate-fast-rustup-0.0.0 (c (n "fast-rustup") (v "0.0.0") (h "1y8n92sml6nqbyn2ahgh66xam54bybnncz6dqajvd0aw2j0qs900") (y #t)))

(define-public crate-fast-rustup-0.0.1 (c (n "fast-rustup") (v "0.0.1") (h "13fx4khr89w1xi71qzx1r3a5h5yhzfzvd0i56hk71sscw7xk7b14") (y #t)))

