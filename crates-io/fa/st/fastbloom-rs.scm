(define-module (crates-io fa st fastbloom-rs) #:use-module (crates-io))

(define-public crate-fastbloom-rs-0.1.0 (c (n "fastbloom-rs") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)))) (h "02p7bjn8fjap9cgxmqfzp395sfdf82adr8v96nwflglascz451xk") (y #t)))

(define-public crate-fastbloom-rs-0.1.1 (c (n "fastbloom-rs") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)))) (h "0rv8q5q436vfql3a6pp7ibvq753g9gxgibsa2kazh9xx1bx8xasl") (y #t)))

(define-public crate-fastbloom-rs-0.1.2 (c (n "fastbloom-rs") (v "0.1.2") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)))) (h "1rh3q0gi2hx09598brsp8gi1ziq9qhihf6m79qmfzpg2gysb4jv7") (y #t)))

(define-public crate-fastbloom-rs-0.1.3 (c (n "fastbloom-rs") (v "0.1.3") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)))) (h "0mc2zc9g2ivii1zsmfzwr7cj6jskjpyj5rbb2zaws2ib2dfibd12")))

(define-public crate-fastbloom-rs-0.2.0 (c (n "fastbloom-rs") (v "0.2.0") (d (list (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)))) (h "1b5bpbz63ajyr2v07nmimh4i373b729nfa14m485lhz69hhicr6h") (y #t)))

(define-public crate-fastbloom-rs-0.2.1 (c (n "fastbloom-rs") (v "0.2.1") (d (list (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)))) (h "0x6dyxwfjz54llj6fs5fxq9flry9km2hvwila9cgk41acdgc2862")))

(define-public crate-fastbloom-rs-0.2.2 (c (n "fastbloom-rs") (v "0.2.2") (d (list (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)))) (h "0fxywq1z31srxdr1z8q0gqfhdh5vy457m7c9vmgyzarivbjwq1pc")))

(define-public crate-fastbloom-rs-0.3.0 (c (n "fastbloom-rs") (v "0.3.0") (d (list (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)))) (h "06cnj6b6lpf3j1v53i9igrxhd65770k8ylzm93cba83hhjx4qd74")))

(define-public crate-fastbloom-rs-0.3.1 (c (n "fastbloom-rs") (v "0.3.1") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)) (d (n "xorfilter-rs") (r "^0.5.1") (d #t) (k 0)))) (h "1f4a3gsdr1dn2sim33gqrniap4bjpb6qzhip6vm2f12x2h179dh3")))

(define-public crate-fastbloom-rs-0.4.0 (c (n "fastbloom-rs") (v "0.4.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)) (d (n "xorfilter-rs") (r "^0.5.1") (d #t) (k 0)))) (h "14yjlaji56jk2ajzpy9yh3ipgwjzq60prqbddij0h4mpm02vzbar")))

(define-public crate-fastbloom-rs-0.5.0 (c (n "fastbloom-rs") (v "0.5.0") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)) (d (n "xorfilter-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "0jb0gs8gf3dxvn2fk5y1sxaisa8c471jj2lbsp556gh0bibkmzsd")))

(define-public crate-fastbloom-rs-0.5.1 (c (n "fastbloom-rs") (v "0.5.1") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)) (d (n "xorfilter-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "1if9ygcsrqpx5lx0xwq99a4fj2y1d1fswi9cv5i7skxwwawv8nm2")))

(define-public crate-fastbloom-rs-0.5.2 (c (n "fastbloom-rs") (v "0.5.2") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)) (d (n "xorfilter-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "17vas7yprr57gpnb6mi8gi17wqw4324bqdfr769wc467c87k01hs")))

(define-public crate-fastbloom-rs-0.5.3 (c (n "fastbloom-rs") (v "0.5.3") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)) (d (n "xorfilter-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "1vfz3hzvjvsxm296mndqzpmr9vbk7avjqd3zx4invnss0ri5ak7h")))

(define-public crate-fastbloom-rs-0.5.4 (c (n "fastbloom-rs") (v "0.5.4") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)) (d (n "xorfilter-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "14qbj3rp4ibp5l16fjz77wywy4kjl6qmh039w0wsiz8v4y537j23")))

(define-public crate-fastbloom-rs-0.5.5 (c (n "fastbloom-rs") (v "0.5.5") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)) (d (n "xorfilter-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "00913zkxhkxzwyc79q4pf7h1xqawm7pphnxrn2i4f1m7v1rl7aj3")))

(define-public crate-fastbloom-rs-0.5.6 (c (n "fastbloom-rs") (v "0.5.6") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xorfilter-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "1ny6kidv91zz5j43wfwf50nnlwb982qlg7qkbllkqd7ya2n4ydph") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fastbloom-rs-0.5.7 (c (n "fastbloom-rs") (v "0.5.7") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xorfilter-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "0r5k83f9icz2kp2ynvyz83jc5dv2vw1bq3lnhz1alq92xm1hn33d") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fastbloom-rs-0.5.8 (c (n "fastbloom-rs") (v "0.5.8") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xorfilter-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "0i9v9l84lzp4c4f9af58ld158qrhkbnnv78hjyzk04r6gz8nm7sd") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fastbloom-rs-0.5.9 (c (n "fastbloom-rs") (v "0.5.9") (d (list (d (n "cuckoofilter") (r "^0.5.0") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xorfilter-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3" "const_xxh3"))) (d #t) (k 0)))) (h "1ivwad6rmr0cqpjg66nx1r3sn7q1yrm7n6n4id67frdcmcm7f2y6") (s 2) (e (quote (("serde" "dep:serde"))))))

