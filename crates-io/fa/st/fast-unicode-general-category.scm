(define-module (crates-io fa st fast-unicode-general-category) #:use-module (crates-io))

(define-public crate-fast-unicode-general-category-1.0.0 (c (n "fast-unicode-general-category") (v "1.0.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "1rvrspd34kv8xrw09zmc123irdbzgnwsa4izgxx4b77jz6k0rjqd")))

(define-public crate-fast-unicode-general-category-1.0.1 (c (n "fast-unicode-general-category") (v "1.0.1") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0zb9pjs9sr7p2cddy6qk87xil9ca5qlv1issnwvy785447hpnf60")))

