(define-module (crates-io fa st fastrand-contrib) #:use-module (crates-io))

(define-public crate-fastrand-contrib-0.1.0 (c (n "fastrand-contrib") (v "0.1.0") (d (list (d (n "fastrand") (r "^2.0.0") (k 0)) (d (n "libm_dep") (r "^0.2.7") (o #t) (d #t) (k 0) (p "libm")))) (h "02r9dh3z7k9dypjhvknwwaahbzv3959sz6w5yibzda6dh1c08v7v") (f (quote (("std" "alloc" "fastrand/std") ("libm" "libm_dep") ("default" "std") ("alloc" "fastrand/alloc")))) (r "1.43.0")))

