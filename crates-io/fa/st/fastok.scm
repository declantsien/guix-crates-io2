(define-module (crates-io fa st fastok) #:use-module (crates-io))

(define-public crate-fastok-0.0.1 (c (n "fastok") (v "0.0.1") (d (list (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module" "generate-import-lib"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)))) (h "13dmfr51hjl06cnxz24gndsyk1kb0azipwr8bwm7bnbvnfs9cyk0")))

