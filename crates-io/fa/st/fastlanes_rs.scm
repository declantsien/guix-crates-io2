(define-module (crates-io fa st fastlanes_rs) #:use-module (crates-io))

(define-public crate-fastlanes_rs-0.1.0 (c (n "fastlanes_rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "1029lp5pmfi67qpk7wfrbfc2jga4ia7rbvf5x5m8by5286li6vj6")))

