(define-module (crates-io fa st fast_inv_sqrt) #:use-module (crates-io))

(define-public crate-fast_inv_sqrt-1.0.0 (c (n "fast_inv_sqrt") (v "1.0.0") (h "11ldx847xxavv98r6ymh2qz57dqab96zwm7yj0gaavqfk6ssp9pi") (f (quote (("omit-checking") ("nightly"))))))

(define-public crate-fast_inv_sqrt-1.0.1 (c (n "fast_inv_sqrt") (v "1.0.1") (d (list (d (n "more-asserts") (r "~0.1") (d #t) (k 2)))) (h "0kwpgq58qmswabprsnx0a1ypm781im1k66xh7gjmgb9k1d21mrn6") (f (quote (("omit-checking") ("nightly"))))))

