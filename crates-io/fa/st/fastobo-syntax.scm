(define-module (crates-io fa st fastobo-syntax) #:use-module (crates-io))

(define-public crate-fastobo-syntax-0.1.0 (c (n "fastobo-syntax") (v "0.1.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "14hbgrdcqinpgq9v85j9hh3y22p2q329y0qaxm105r7b7w7wmarv")))

(define-public crate-fastobo-syntax-0.1.1 (c (n "fastobo-syntax") (v "0.1.1") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "07mfmxg2sy721ay4nzi2850mvss80d1xwyl1iwv3akfv8nsy00w2")))

(define-public crate-fastobo-syntax-0.2.0 (c (n "fastobo-syntax") (v "0.2.0") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1vl0ambb6zmw8rdkfrg11z44wdyrmv60dvi6y9lma05fvv49s8kk")))

(define-public crate-fastobo-syntax-0.2.1 (c (n "fastobo-syntax") (v "0.2.1") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0n99qicib4aayg71s07p50dlq7jl60vwhiq06yhpapp11kxz9j28")))

(define-public crate-fastobo-syntax-0.3.0 (c (n "fastobo-syntax") (v "0.3.0") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1gfw7ms33pzzrdx16v97hnpsk4pwh9953p8alpwp660547c5sqsr")))

(define-public crate-fastobo-syntax-0.3.1 (c (n "fastobo-syntax") (v "0.3.1") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0s9qah07dm36pia06x93mp2hlzcs5s0kxah6jvq3wcmzif7p8w9s")))

(define-public crate-fastobo-syntax-0.3.2 (c (n "fastobo-syntax") (v "0.3.2") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "00kmv06qivsqvm3axz1qiy427s4sg0bsqjv3jyqh4d8znps53b69")))

(define-public crate-fastobo-syntax-0.3.3 (c (n "fastobo-syntax") (v "0.3.3") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0rl1aw6lbw4mxg19m14ghkjrrlnr2fwknwbdv0byn6bzs8sgdkxw")))

(define-public crate-fastobo-syntax-0.3.4 (c (n "fastobo-syntax") (v "0.3.4") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0nyklwqq3ba5pb050z95969pp2i6vbdpppj19fabm0kjr8clhdpi") (y #t)))

(define-public crate-fastobo-syntax-0.3.5 (c (n "fastobo-syntax") (v "0.3.5") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "191344wn25j5gqnzyqkf0sd3p1mhx2x4jl4pr151gmmvd5a059hm") (y #t)))

(define-public crate-fastobo-syntax-0.3.6 (c (n "fastobo-syntax") (v "0.3.6") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1zhk197mpipn43hd1jyfxa6q8bldip40q1mmjjp61cyy2yiwgax3")))

(define-public crate-fastobo-syntax-0.3.7 (c (n "fastobo-syntax") (v "0.3.7") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0zqlv7yf2wfi2vsldnk0scshdhmq4immz4lffi80b7ylp8hrzmif")))

(define-public crate-fastobo-syntax-0.3.8 (c (n "fastobo-syntax") (v "0.3.8") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0mjjchfln6n2pidqawkxf5whahvilzqmga8h6wg16n4f90wvg8a6")))

(define-public crate-fastobo-syntax-0.4.0 (c (n "fastobo-syntax") (v "0.4.0") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1aq5fv55x4bw9rni3658kix74q1s21bl9948rblbjdivfhfmdh59")))

(define-public crate-fastobo-syntax-0.5.0 (c (n "fastobo-syntax") (v "0.5.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0b35fbnxcydkq67a1mqfcr0xxlh80qwgzyvzryrwnb4rzscfmnva")))

(define-public crate-fastobo-syntax-0.6.0 (c (n "fastobo-syntax") (v "0.6.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1g5xp2zcd0fi8qxq546m4m01zkdgb9mi0hpwpiyw0czgwrmgh8rg")))

(define-public crate-fastobo-syntax-0.6.1 (c (n "fastobo-syntax") (v "0.6.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1y3y3f6dl5yswf8pn9vb9vjcfi2xp72nxhgjagr8rghp01g33iyq")))

(define-public crate-fastobo-syntax-0.6.2 (c (n "fastobo-syntax") (v "0.6.2") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1bzi6vcfv2rid950slmq37zrp8q71lygk5jacsgfi5gii8ns7fl2")))

(define-public crate-fastobo-syntax-0.7.0 (c (n "fastobo-syntax") (v "0.7.0") (d (list (d (n "pest") (r "=2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "=2.1.0") (d #t) (k 0)))) (h "02nhbzlvh77mssk64zqffnp4jiwyfj0lk615n1wfvzlrbaym7vgx")))

(define-public crate-fastobo-syntax-0.7.1 (c (n "fastobo-syntax") (v "0.7.1") (d (list (d (n "pest") (r "=2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "=2.1.0") (d #t) (k 0)))) (h "0pqlk4zhi3v64bpyaiqsfwq1i2ppbj5711pj7p7811laf497jjyh")))

(define-public crate-fastobo-syntax-0.7.2 (c (n "fastobo-syntax") (v "0.7.2") (d (list (d (n "pest") (r "=2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "=2.1.0") (d #t) (k 0)))) (h "19l35ic9s7ps0fvr73f8jg3xr14jsr0bq11yqa3xshkw0ds4y83p")))

(define-public crate-fastobo-syntax-0.7.3 (c (n "fastobo-syntax") (v "0.7.3") (d (list (d (n "pest") (r "=2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "=2.1.0") (d #t) (k 0)))) (h "1mh5l3mr9zf5ggzxakdcp3828mm6wq24cx3xhpjklag9xlvlgdfd")))

(define-public crate-fastobo-syntax-0.7.4 (c (n "fastobo-syntax") (v "0.7.4") (d (list (d (n "pest") (r "^2.5.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.1") (d #t) (k 0)))) (h "1jr3clkg0mw8l61wrb3mgvai7562jy167s5kgiqxfsvqgs938j7j")))

(define-public crate-fastobo-syntax-0.8.0 (c (n "fastobo-syntax") (v "0.8.0") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)))) (h "0mw0gcwhxvghb7qy65bwvb89743qszrnk498bisq8n4by08mr28g")))

(define-public crate-fastobo-syntax-0.8.1 (c (n "fastobo-syntax") (v "0.8.1") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)))) (h "0cgk3gnaaga508c6ivjaxsszk11dydg5hpidjzbw3k4bvx7dl847")))

