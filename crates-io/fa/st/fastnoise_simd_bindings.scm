(define-module (crates-io fa st fastnoise_simd_bindings) #:use-module (crates-io))

(define-public crate-fastnoise_simd_bindings-0.0.1 (c (n "fastnoise_simd_bindings") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1r4ixg7md14lkw4dqhvcf9xi7pcikwvxpxg8jk9v3jys0fagg25i")))

(define-public crate-fastnoise_simd_bindings-0.1.0 (c (n "fastnoise_simd_bindings") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1min9ljx0a0jy7cpihsf1j7z4x3dcmr6ppw5zpg1gn8nlphlq7fj")))

(define-public crate-fastnoise_simd_bindings-0.1.1 (c (n "fastnoise_simd_bindings") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1626k7gz8a90jizbf6mi0kqkf7sqr2n1g8823jijarfxmk5i6s7b")))

