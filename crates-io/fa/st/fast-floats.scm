(define-module (crates-io fa st fast-floats) #:use-module (crates-io))

(define-public crate-fast-floats-0.1.0 (c (n "fast-floats") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "0cd8ywga3br06ad5j43faywlfi7hqdhcxj7pdwsliw75rkgg9h6v")))

(define-public crate-fast-floats-0.1.1 (c (n "fast-floats") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "0brdxy3zfa9rjzbpf8adlgjqbpi6d9cnw53950h5dxdczgacy05f")))

(define-public crate-fast-floats-0.1.2 (c (n "fast-floats") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "1afk0x8p9y9na0f4zpqv2d2zyd3vxribsb296b12z6bh7cm0h3iv")))

(define-public crate-fast-floats-0.2.0 (c (n "fast-floats") (v "0.2.0") (h "0hkh2ngi6fq27p2qliyjx5150pyl14szx9j1zvij7z8w33rm29jf")))

