(define-module (crates-io fa st fastly-api) #:use-module (crates-io))

(define-public crate-fastly-api-1.0.0-beta.0 (c (n "fastly-api") (v "1.0.0-beta.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1sqrgz26bwba1n72k467xwmmlghr367ikwrrd0v52fs6hc2i8fs7")))

(define-public crate-fastly-api-1.0.0 (c (n "fastly-api") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1kmjxv3s97brxjg93nyc5ny8l6ix25xiydvcdqfjbmf78fx9a6r0")))

(define-public crate-fastly-api-1.0.1-alpha.0 (c (n "fastly-api") (v "1.0.1-alpha.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "15hjcfqiwlirr8yw9jy92p8zpk6mrm6c104lhcvchw6ghjb2y4lw")))

(define-public crate-fastly-api-1.1.0 (c (n "fastly-api") (v "1.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "11canxqxdvf68i89604jqa7jvf7q3c3a5xfp4081k22brrm82arj")))

(define-public crate-fastly-api-1.2.0 (c (n "fastly-api") (v "1.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "16nr39lsr50m0gl2sjv03nj82v59mvs8ar9k8qwappvnqy92w7nz")))

(define-public crate-fastly-api-1.3.0 (c (n "fastly-api") (v "1.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "042v1ijvrg7zi6mnyrspbmgc5a6b569a2wan7gfvfbjmgpbxj8gm")))

(define-public crate-fastly-api-1.3.1 (c (n "fastly-api") (v "1.3.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0pqym8lnd9gwrzci689wdihi79l96kxwmynhkvf6wm6847x612gm")))

(define-public crate-fastly-api-2.0.0 (c (n "fastly-api") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0a6rfldlqgb4qgsmd924971z1dayv84kw8cs35b9z8ybdv7zfz98")))

(define-public crate-fastly-api-2.1.0 (c (n "fastly-api") (v "2.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0pz2ksnl733w3v3xybr22r039hyljxihwaimiy9nbrxysdb4ysw8")))

(define-public crate-fastly-api-2.1.1 (c (n "fastly-api") (v "2.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0f6gaqsi1jj5xpp2rnyp2gv08fadqrpkp73kdjsgk3fr7744nk4s")))

(define-public crate-fastly-api-2.2.0 (c (n "fastly-api") (v "2.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0f7n4pxsgdm6alymj2h43g4nw68l5lss87y77bb6x3pni7rjz58b")))

(define-public crate-fastly-api-2.2.1 (c (n "fastly-api") (v "2.2.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1lgl4mxl3rr1l7j5xjl3b17vqnqh90hp78z5nqkfqmjs2079rrn1")))

(define-public crate-fastly-api-2.4.0 (c (n "fastly-api") (v "2.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "02213as07f8m4n2cvs1i7lc6gpf1badkxw6sxl1hd9wcslf4nbm0")))

(define-public crate-fastly-api-2.5.0 (c (n "fastly-api") (v "2.5.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0h84gqsjv2xm9281k1fvgshhgm792vk7pszz8wnzmnam66wx2ilf")))

(define-public crate-fastly-api-3.0.0 (c (n "fastly-api") (v "3.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0dg0hgxzsmjhmcly146nadl8y2y6l05jvdx87ab5y5a172xzb42d")))

(define-public crate-fastly-api-3.0.1 (c (n "fastly-api") (v "3.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "19h7w2cz4x2vmksmmwa9fm3xx5s4yvf1ig3n5rr97jy91fgbm9k8")))

(define-public crate-fastly-api-3.0.2 (c (n "fastly-api") (v "3.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0jpkfmqsg4nlhvyls73z0vp75q2dlpi9pg5xlln0wqh4i5xb80ch")))

(define-public crate-fastly-api-4.0.0 (c (n "fastly-api") (v "4.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1jcs8w0a8sb7qdbcx4s2ipmcn3l04fa0l82vy0j0vx5zxisp334h")))

(define-public crate-fastly-api-4.1.0 (c (n "fastly-api") (v "4.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0i85qi0a6n38bm3w0sg7p49i27pjwk1nmwk4x6894z1giavc8nbb")))

(define-public crate-fastly-api-4.1.1 (c (n "fastly-api") (v "4.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0dcr44hjk7wp9sxfqink37mqp32n6i3ck1si4vyf1ak4894bzsba")))

(define-public crate-fastly-api-4.2.0 (c (n "fastly-api") (v "4.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0mbbxradd4084f7m5jhr50ryxvacjwgbinzba1w15a8hw3wa43jk")))

(define-public crate-fastly-api-4.2.1 (c (n "fastly-api") (v "4.2.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0syd0y6fsd2dj8pmhg3vc8xwz3f2c4s1i9lgsriprr3j91ckqx9z")))

(define-public crate-fastly-api-4.2.2 (c (n "fastly-api") (v "4.2.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0p76swyy1dyaznd17jl4z6v4sad948826ama7pydl33i74dfcs4n")))

(define-public crate-fastly-api-4.3.0 (c (n "fastly-api") (v "4.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "12jj6ialh0631b9dhplf1yb0v0g8mc43iyfxzwwm89wphxsysb4y")))

(define-public crate-fastly-api-4.3.1 (c (n "fastly-api") (v "4.3.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "05avlhg338wn8fp92d0n46gdmd2bcslh5d38q3x082bvcy3smqm8")))

(define-public crate-fastly-api-4.4.0 (c (n "fastly-api") (v "4.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1j73lr03fmp30vabhfnz09rk029kfg210dd0yq1g3ykc20k6wv7j")))

(define-public crate-fastly-api-4.5.0 (c (n "fastly-api") (v "4.5.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "00vgc2z0pzqmbn4z156fvpl5ca3ghcr0rc8nn26p9fbdsg77y4k0")))

(define-public crate-fastly-api-4.6.0 (c (n "fastly-api") (v "4.6.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0pmvj8m25y7pp0dx5bqww8nqb9ybp5a7qb7d2g38mzc37290y86w")))

