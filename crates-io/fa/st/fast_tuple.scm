(define-module (crates-io fa st fast_tuple) #:use-module (crates-io))

(define-public crate-fast_tuple-0.1.0 (c (n "fast_tuple") (v "0.1.0") (h "1y99aggcy1vc17yyy4skhdsmwc9b1r4wxh3glnf2ac8ddgc7n9yw")))

(define-public crate-fast_tuple-0.1.1 (c (n "fast_tuple") (v "0.1.1") (h "0a3ymcc3906f9i9sawpgfslypddslgq66ffbr8vaglvxay1a4ppc")))

(define-public crate-fast_tuple-0.1.2 (c (n "fast_tuple") (v "0.1.2") (h "10x8v352p2k023j6w2ia6hmkc1bvabhh73jskibp45v48cisq5lc")))

(define-public crate-fast_tuple-0.1.3 (c (n "fast_tuple") (v "0.1.3") (h "103x8qsc4h578amj7sg205fyx9k1wihr8112w62d6xz231zcn7ps")))

