(define-module (crates-io fa st fast-slam) #:use-module (crates-io))

(define-public crate-fast-slam-0.1.0 (c (n "fast-slam") (v "0.1.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "bayes_estimate") (r "^0.8.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.25.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 0)))) (h "1gaiaf3ssnidk1qf3d8pr4pw9kizscsmv8yiwdrp06cka3pavn37")))

(define-public crate-fast-slam-0.2.0 (c (n "fast-slam") (v "0.2.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "bayes_estimate") (r "^0.8.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.25.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 0)))) (h "01dz432jbzb7x9xqj23alk4c95qqdmvbn60bhzqrfk6vq2rxrlwh")))

(define-public crate-fast-slam-0.3.0 (c (n "fast-slam") (v "0.3.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "bayes_estimate") (r "^0.9.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.26") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1a2mzk2m9f88xa0dk2mngcf68w6d6ziphajyy7sbgj542qnfphvk")))

(define-public crate-fast-slam-0.3.1 (c (n "fast-slam") (v "0.3.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "bayes_estimate") (r "^0.9.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1q5jgmqy52zr730xdyfvbhjj5hiz4xwvyy7l1hxpmsv9xckg948g")))

(define-public crate-fast-slam-0.4.0 (c (n "fast-slam") (v "0.4.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "bayes_estimate") (r "^0.10") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1q52va67mw2939nz1v9lvkbjvbqmzj3l6dpljnblnj0d90dkcr5f")))

(define-public crate-fast-slam-0.5.0 (c (n "fast-slam") (v "0.5.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "bayes_estimate") (r "^0.12") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0sdjs9ncmnzksya9y901lg0z4xmnaw9sh7bym6p82wm82y9ji5g7")))

(define-public crate-fast-slam-0.5.1 (c (n "fast-slam") (v "0.5.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "bayes_estimate") (r "^0.15") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1sw1wbgs07bax0gzbwxzbvwrfk9kbbdmdkz0ws6c1j2crfwa3hwb")))

