(define-module (crates-io fa st fasta) #:use-module (crates-io))

(define-public crate-fasta-0.1.0 (c (n "fasta") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0irxb4nhnfjjvwm3wqwwgb79rh47fc9xhnyjyfpvx0j19bd8jvm1")))

(define-public crate-fasta-0.1.1 (c (n "fasta") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1padrx7v8igpnpaaw9g7iyf6ha7idpk6vn5yq8p2drj91jhx9hvp")))

(define-public crate-fasta-0.1.2 (c (n "fasta") (v "0.1.2") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c5lxy30kgy9ac31a9wrdx1f30b9vnklc5g0pmfmbrllvhmh07fn")))

(define-public crate-fasta-0.1.3 (c (n "fasta") (v "0.1.3") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j0zfr63sxbgc879bdh28d0538c4p362479s0pymb60x11mpsq5b")))

