(define-module (crates-io fa st fastchr) #:use-module (crates-io))

(define-public crate-fastchr-0.1.0 (c (n "fastchr") (v "0.1.0") (h "1990150pp3gms27vw6w3vm8qsq90znhdp85dw11rbblc5l6ikbw0")))

(define-public crate-fastchr-0.2.0 (c (n "fastchr") (v "0.2.0") (d (list (d (n "faster") (r "^0.4.3") (d #t) (k 0)) (d (n "memchr") (r "^2.0.1") (d #t) (k 2)))) (h "0dsq6fcxxh6j72yx7mm986h2hrck5nxfp55l7702pzq0pbw6sjk1")))

(define-public crate-fastchr-0.3.0 (c (n "fastchr") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2.0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1v8q5227129abh8gkyg7drh3ghmfgb2h391fvxlw808n1j5rlwi5")))

