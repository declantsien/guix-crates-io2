(define-module (crates-io fa st fast-rustc-ap-rustc_ast_pretty) #:use-module (crates-io))

(define-public crate-fast-rustc-ap-rustc_ast_pretty-1.0.0 (c (n "fast-rustc-ap-rustc_ast_pretty") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc_ast") (r "^1.0.0") (d #t) (k 0) (p "fast-rustc-ap-rustc_ast")) (d (n "rustc_data_structures") (r "^1.0.0") (d #t) (k 0) (p "fast-rustc-ap-rustc_data_structures")) (d (n "rustc_span") (r "^1.0.0") (d #t) (k 0) (p "fast-rustc-ap-rustc_span")))) (h "1r0xy5nf69gh89jh4mkml4240kxydsg839qi77yr81cyj9zpqv1i")))

