(define-module (crates-io fa st fast-bernoulli) #:use-module (crates-io))

(define-public crate-fast-bernoulli-1.0.0 (c (n "fast-bernoulli") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1c1ymv1r84rbq1z51wyqdlvzqmfa19vvbv44h2wmq4zn55j3yl9a")))

(define-public crate-fast-bernoulli-1.0.1 (c (n "fast-bernoulli") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1q628g4rvg8dj01agwc30pbbvkbyx9r1hh5p948jk8j7h712h7nr")))

(define-public crate-fast-bernoulli-1.0.2 (c (n "fast-bernoulli") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1mxvrcfzam8f60a369pff54gzgk5mf5qzynsqfy2n0r52mg2kyxn")))

