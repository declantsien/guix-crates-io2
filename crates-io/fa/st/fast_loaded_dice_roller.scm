(define-module (crates-io fa st fast_loaded_dice_roller) #:use-module (crates-io))

(define-public crate-fast_loaded_dice_roller-0.1.0 (c (n "fast_loaded_dice_roller") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1l4hi7xirzplmwpddkf5hzsq97kklm7gha8na32i9nihg39g8cf8")))

(define-public crate-fast_loaded_dice_roller-0.1.1 (c (n "fast_loaded_dice_roller") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "1bb9gpkn1l9ll3zirbqkd3al283d133j35zs7knplp0v0wn3lv3n")))

(define-public crate-fast_loaded_dice_roller-0.1.2 (c (n "fast_loaded_dice_roller") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "0cwnrdz8w8bwjkyvppcvsw60cnbrrm2q1dpa147kbb0c075wz323")))

(define-public crate-fast_loaded_dice_roller-0.1.3 (c (n "fast_loaded_dice_roller") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "0q1387dja617fa9d59s5fcr97ng028iiz57al55ffbj0pdkcq4s8")))

(define-public crate-fast_loaded_dice_roller-0.1.4 (c (n "fast_loaded_dice_roller") (v "0.1.4") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "0vzvymwb7p1xc60a56vbwdbdhvdn6b0i3x0p5f86h3bi3i850zz5")))

(define-public crate-fast_loaded_dice_roller-0.1.5 (c (n "fast_loaded_dice_roller") (v "0.1.5") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "17v3wjq76ch33c5rp6xxvm1vrdh343v4pxdk3m0hjhasfg33i709")))

(define-public crate-fast_loaded_dice_roller-0.1.6 (c (n "fast_loaded_dice_roller") (v "0.1.6") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "05hnjlr20wgy9511xrn7fk9xpn8yvgq79sl8bsqhj84rbq7l7zr4")))

