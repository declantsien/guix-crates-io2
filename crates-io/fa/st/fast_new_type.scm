(define-module (crates-io fa st fast_new_type) #:use-module (crates-io))

(define-public crate-fast_new_type-0.1.0 (c (n "fast_new_type") (v "0.1.0") (h "1jilnhkz3lwmpni9vbh20bv6845s67xd6qfqkylxfimvpys8l6jd") (y #t)))

(define-public crate-fast_new_type-0.1.1 (c (n "fast_new_type") (v "0.1.1") (h "1afqbsnpn5frqyqbv7bkcjcm2k4fz0sv5iggabjacwvbhc50p5pn") (y #t)))

(define-public crate-fast_new_type-0.1.2 (c (n "fast_new_type") (v "0.1.2") (d (list (d (n "syn_str") (r "^0.1") (d #t) (k 0)))) (h "0ndsjcnrvm0x4yvzrzrlh666ng73cxf7p0dmwdx6y0sywnvm64h2") (y #t)))

(define-public crate-fast_new_type-0.1.3 (c (n "fast_new_type") (v "0.1.3") (d (list (d (n "syn_str") (r "^0.1") (d #t) (k 0)))) (h "1frrbfr1bqws1hikgq2gsahsb6wd1j9p4wa9c2fnbhbxqmiyb39n") (y #t)))

(define-public crate-fast_new_type-0.1.4 (c (n "fast_new_type") (v "0.1.4") (d (list (d (n "syn_str") (r "^0.1") (d #t) (k 0)))) (h "0s0nilhnbk6gbf9i7hxsinjx5xjc2xma9c8vr25xc81ksqyh3x9v")))

(define-public crate-fast_new_type-0.1.5 (c (n "fast_new_type") (v "0.1.5") (d (list (d (n "syn_str") (r "^0.1") (d #t) (k 0)))) (h "1ym1117abh7szbpmgq22z52ywjsa6ds3y85haz5q72lfp2yws459") (f (quote (("from_struct_for_type") ("default" "from_struct_for_type")))) (y #t)))

(define-public crate-fast_new_type-0.1.6 (c (n "fast_new_type") (v "0.1.6") (d (list (d (n "syn_str") (r "^0.1") (d #t) (k 0)))) (h "13lfdlnqi6l158316qxidvpvxbdcsb3m9lxpc9d30bb12nx7na3w") (f (quote (("from_struct_for_type") ("default" "from_struct_for_type")))) (y #t)))

(define-public crate-fast_new_type-0.1.7 (c (n "fast_new_type") (v "0.1.7") (d (list (d (n "syn_str") (r "^0.1") (d #t) (k 0)))) (h "1m85fvhml3mr2r3fsaacnljly87xdw3syq0r2lnghvpfjcl0nklb") (f (quote (("from_struct_for_type") ("default" "from_struct_for_type"))))))

