(define-module (crates-io fa st fast_hilbert) #:use-module (crates-io))

(define-public crate-fast_hilbert-0.1.0 (c (n "fast_hilbert") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hilbert") (r "^0.1.1") (d #t) (k 2)) (d (n "hilbert_2d") (r "^1.0.0") (d #t) (k 2)) (d (n "hilbert_curve") (r "^0.2.0") (d #t) (k 2)) (d (n "image") (r "^0.23.13") (d #t) (k 2)))) (h "19g5p93achjhmz9pb690gwvwp4x62vyn4dch3pc7rikgwwnp392d")))

(define-public crate-fast_hilbert-1.0.0 (c (n "fast_hilbert") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hilbert") (r "^0.1.1") (d #t) (k 2)) (d (n "hilbert_2d") (r "^1.0.0") (d #t) (k 2)) (d (n "hilbert_curve") (r "^0.2.0") (d #t) (k 2)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1i5y9d33lpvijf167yymd67pr78z6b0c8bn3yzj2ii0h8x7m3mpf")))

(define-public crate-fast_hilbert-1.0.1 (c (n "fast_hilbert") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hilbert") (r "^0.1.1") (d #t) (k 2)) (d (n "hilbert_2d") (r "^1.0.0") (d #t) (k 2)) (d (n "hilbert_curve") (r "^0.2.0") (d #t) (k 2)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.4") (f (quote ("i128"))) (d #t) (k 0)))) (h "0l1527xgfxgv16g0v0b5vsgwkvxpwj6kvjrqc6adc6iy9wzav0cw")))

(define-public crate-fast_hilbert-2.0.0 (c (n "fast_hilbert") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hilbert") (r "^0.1.1") (d #t) (k 2)) (d (n "hilbert_2d") (r "^1.0.0") (d #t) (k 2)) (d (n "hilbert_curve") (r "^0.2.0") (d #t) (k 2)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (f (quote ("i128"))) (d #t) (k 0)))) (h "1y7b91hcyyrip7qabi5ryb06c5s1yhhn08wyfd69b1xg2nz2pv7q")))

