(define-module (crates-io fa st fastcurve_3d) #:use-module (crates-io))

(define-public crate-fastcurve_3d-0.1.0 (c (n "fastcurve_3d") (v "0.1.0") (h "1m97my9q2csxq035yh9d5svda70fgcy3s64d4scjlh4x5521zyws")))

(define-public crate-fastcurve_3d-0.1.1 (c (n "fastcurve_3d") (v "0.1.1") (h "01lzxka16swyjmgxh5ahmnlrggfc9pvm3l4pf2r3hrw8qp4j4d9x")))

