(define-module (crates-io fa st fastcgi-client) #:use-module (crates-io))

(define-public crate-fastcgi-client-0.0.0 (c (n "fastcgi-client") (v "0.0.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)))) (h "1vpydpnh8310nzlsawfiz3vxja476lswzwldlpq6b054j8149gra") (y #t)))

(define-public crate-fastcgi-client-0.1.0 (c (n "fastcgi-client") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0sf0d62asxmyx4rrk8bqvhfay5dpwgjv3h26kx88gvkrcv5s7mpz")))

(define-public crate-fastcgi-client-0.1.1 (c (n "fastcgi-client") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1s5dxys7pxyvd20ldrl79djj9nj1396dxphxp511j70yw6a8yfs1")))

(define-public crate-fastcgi-client-0.2.0 (c (n "fastcgi-client") (v "0.2.0") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 1)))) (h "1ks35nkjwpkssnvi93wf4hg693d919v873mn090sj0j9jp7bdiih")))

(define-public crate-fastcgi-client-0.3.0 (c (n "fastcgi-client") (v "0.3.0") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 1)))) (h "1fkxcmwj1jsyqb9kvrv8ikv0ds35i1jnwp4hslklyphwh6avjpgz")))

(define-public crate-fastcgi-client-0.4.0 (c (n "fastcgi-client") (v "0.4.0") (d (list (d (n "async-attributes") (r "^1.1.1") (d #t) (k 2)) (d (n "async-std") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 1)))) (h "19l489r31j0m3h5zhpm94bymfk8n6i3vzksays6b6ncg1n78vrhq") (f (quote (("default" "async_std") ("async_std" "async-std"))))))

(define-public crate-fastcgi-client-0.5.0 (c (n "fastcgi-client") (v "0.5.0") (d (list (d (n "async-std") (r "^1.0.1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0j2wq1vshd2jwpgwvnmdap322z8hvsd7hpvcsql011nymih5d8hl") (f (quote (("default" "futures"))))))

(define-public crate-fastcgi-client-0.6.0 (c (n "fastcgi-client") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0fp85ii2q2rcwpd6phsrdp7n6w0wkhwrlc9rkcbsmak69xa91pgd")))

(define-public crate-fastcgi-client-0.6.1 (c (n "fastcgi-client") (v "0.6.1") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "050nr1dzsxca8cjhy1xrv1zlax6rd3in52clrzjw19db171pbcpl")))

(define-public crate-fastcgi-client-0.6.2 (c (n "fastcgi-client") (v "0.6.2") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "14gw6wr1fbdp914x0wmzbjsrgzc3q3dy2lwqldn313l9mpni32ys")))

(define-public crate-fastcgi-client-0.7.0 (c (n "fastcgi-client") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1qqih6dm2r1my278hr11vzh4sp6qmixh2p88s62cm47si75bbyv1")))

(define-public crate-fastcgi-client-0.8.0-alpha.1 (c (n "fastcgi-client") (v "0.8.0-alpha.1") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("io-util" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 2)))) (h "1p1d5qmdj0gmz0bn8pcbwxs2qkv81jixrzhf0pw4vr7wgi2nsvcx")))

(define-public crate-fastcgi-client-0.8.0-alpha.2 (c (n "fastcgi-client") (v "0.8.0-alpha.2") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("io-util" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 2)))) (h "19zxacwwzr9cbxzfmqbbim8ahx39pq2q2k0cacl1kwwjcpisqbak")))

(define-public crate-fastcgi-client-0.8.0 (c (n "fastcgi-client") (v "0.8.0") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("io-util" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 2)))) (h "1skk0jsikipahz407i6l1fvpi84j24l12zwmanl36047llwpz8jm")))

(define-public crate-fastcgi-client-0.9.0 (c (n "fastcgi-client") (v "0.9.0") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("io-util" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 2)))) (h "1s6jrhs2v7gwxj23nl8y9rfdxjdg5gn1jfshi23vj2shdcza92l8")))

