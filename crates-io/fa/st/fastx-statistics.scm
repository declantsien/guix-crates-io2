(define-module (crates-io fa st fastx-statistics) #:use-module (crates-io))

(define-public crate-fastx-statistics-0.1.0 (c (n "fastx-statistics") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "1zm4g8fr9as0h8v1yn88nlisf6c8ybchwf23nb6s06kw41wvypn0") (r "1.58.1")))

(define-public crate-fastx-statistics-0.2.0 (c (n "fastx-statistics") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "1wz6w8qjaz5s9sh2011bac0mirddg16c5nv7y9c44sjjnsjzjljc") (r "1.58.1")))

(define-public crate-fastx-statistics-0.2.1 (c (n "fastx-statistics") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "15svq679h8j447aykbm0h4jaiamajcm50w095xy2hna4da2bladn") (r "1.58.1")))

(define-public crate-fastx-statistics-0.3.0 (c (n "fastx-statistics") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "021xq9smhvs8c34nxvgcagxqs4pxha35c8y284d6vmfqkd2zi7p7") (r "1.60.0")))

(define-public crate-fastx-statistics-0.4.0 (c (n "fastx-statistics") (v "0.4.0") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "114qmqbxji0jn3mf9qwz13nxzlmawlqqvrxx101ivynfs97mzb2w") (r "1.60.0")))

(define-public crate-fastx-statistics-0.4.1 (c (n "fastx-statistics") (v "0.4.1") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "0p4r30wy86xrncwi2zxiqdyprw91fmk28dc362q2s1przk37kcl0") (r "1.60.0")))

(define-public crate-fastx-statistics-0.5.0 (c (n "fastx-statistics") (v "0.5.0") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "1n03p7g6g6gg9qlll8wb5sbs3i5n62plrb5s4rj1mij76aj3pmll") (r "1.60.0")))

(define-public crate-fastx-statistics-0.5.1 (c (n "fastx-statistics") (v "0.5.1") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "141pfbxpv0244yncad2pzyssnmaw8m8rmxw092jij4s408jddw9l") (r "1.60.0")))

(define-public crate-fastx-statistics-0.6.0 (c (n "fastx-statistics") (v "0.6.0") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "11h9r58f5da2ycj5pfj5rifz7xwrfasmnh2kkc2cmcqy0d90xlcb") (r "1.60.0")))

(define-public crate-fastx-statistics-0.7.0 (c (n "fastx-statistics") (v "0.7.0") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "049ckmns195gjzznry3w5wxp3114h5djmd2bshsv8lifblf3978n") (r "1.60.0")))

(define-public crate-fastx-statistics-1.0.0 (c (n "fastx-statistics") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "0clyzx2bcq6x206w52m9fzky7pwx3mhpg4d941agrzpx8wmabz6a") (r "1.60.0")))

