(define-module (crates-io fa st fast-counter) #:use-module (crates-io))

(define-public crate-fast-counter-0.0.1 (c (n "fast-counter") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "thread-id") (r "^4.0.0") (d #t) (k 2)))) (h "0p4iwnv372x1dxwq6dyd1v4vpr1g5ygkb6br14z7s9zmhn9sh8s1") (f (quote (("nightly") ("default"))))))

(define-public crate-fast-counter-0.0.2 (c (n "fast-counter") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "thread-id") (r "^4.0.0") (d #t) (k 2)))) (h "0mp4vhm50crbm70m6n0azxx8y6zgkc4racbamln6gfivhm9hd9iq") (f (quote (("nightly") ("default"))))))

(define-public crate-fast-counter-1.0.0 (c (n "fast-counter") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0d42gp1pfrf5n44s2ryq9xkwqrsyq5ggrxsms16ykgvbsgsh1w1b")))

