(define-module (crates-io fa st fastly-kv-preview) #:use-module (crates-io))

(define-public crate-fastly-kv-preview-0.8.2 (c (n "fastly-kv-preview") (v "0.8.2") (d (list (d (n "fastly") (r "^0.8.2") (d #t) (k 0)) (d (n "fastly-shared") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1969c72y1y86m4a2i5wbnbh2sbxdgx0pzhh9g68fwnickib3gnck")))

(define-public crate-fastly-kv-preview-0.8.3 (c (n "fastly-kv-preview") (v "0.8.3") (d (list (d (n "fastly") (r "^0.8.3") (d #t) (k 0)) (d (n "fastly-shared") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1l08rm1mymp223m90wjazdf5k8ii51z5xbwbw09qxjn303d1f4w3")))

(define-public crate-fastly-kv-preview-0.8.4 (c (n "fastly-kv-preview") (v "0.8.4") (d (list (d (n "fastly") (r "^0.8.4") (d #t) (k 0)) (d (n "fastly-shared") (r "^0.8.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "19jbh4h81x9h7301vjnk04gf2c165q2mz6xhs4bx2vvw514y6vpq")))

(define-public crate-fastly-kv-preview-0.8.5 (c (n "fastly-kv-preview") (v "0.8.5") (d (list (d (n "fastly") (r "^0.8.5") (d #t) (k 0)) (d (n "fastly-shared") (r "^0.8.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "025kky8vwdiqrpbsqvc8p70wigqvyrsc5plljl573k2jvdzggwza")))

(define-public crate-fastly-kv-preview-0.8.6 (c (n "fastly-kv-preview") (v "0.8.6") (d (list (d (n "fastly") (r "^0.8.6") (d #t) (k 0)) (d (n "fastly-shared") (r "^0.8.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1433jwwczwjxf0xyy57ac8vcz6r3bhj292akh90lffw9ph39b16g")))

(define-public crate-fastly-kv-preview-0.8.7 (c (n "fastly-kv-preview") (v "0.8.7") (d (list (d (n "fastly") (r "^0.8.7") (d #t) (k 0)) (d (n "fastly-shared") (r "^0.8.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "14vinick55nvgqizrw5k2mbvbffbs1flqykhhhpj6sj0z83ngcyj")))

(define-public crate-fastly-kv-preview-0.8.8 (c (n "fastly-kv-preview") (v "0.8.8") (d (list (d (n "fastly") (r "^0.8.7") (d #t) (k 0)) (d (n "fastly-shared") (r "^0.8.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1aphr7jk3zr2az4xw04ycwzjcbjn0lrix2aakxlf11ysqb02la7n")))

(define-public crate-fastly-kv-preview-0.8.9 (c (n "fastly-kv-preview") (v "0.8.9") (d (list (d (n "fastly") (r "^0.8.9") (d #t) (k 0)) (d (n "fastly-shared") (r "^0.8.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "03d7rk3rkl46vk36p9rfpbf144vhb0pwsvhgbyysdjyrzfs5s9lg")))

(define-public crate-fastly-kv-preview-0.9.2 (c (n "fastly-kv-preview") (v "0.9.2") (d (list (d (n "fastly") (r "^0.9.2") (d #t) (k 0)) (d (n "fastly-shared") (r "^0.9.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0a08c65nf0abigfhqs5vdz5wfxnp6psfc8k9j008z911prpp8s9b")))

(define-public crate-fastly-kv-preview-0.9.6 (c (n "fastly-kv-preview") (v "0.9.6") (d (list (d (n "fastly") (r "^0.9.6") (d #t) (k 0)) (d (n "fastly-shared") (r "^0.9.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "19g3kgbb01zhzv4cyyslfryxfr9d54v2dgy3kyqj29r5gvqqkbmm")))

