(define-module (crates-io fa st fasters_derive) #:use-module (crates-io))

(define-public crate-fasters_derive-0.2.0 (c (n "fasters_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0f3mr371ng0q34cmiy8hf1cmqqs6vvpdglzgj6wycla301pqsl13")))

(define-public crate-fasters_derive-0.3.0 (c (n "fasters_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "08wnnzz9k2gwj2bw1fl39q0vf6g5yb6mazl8n6wxk6ycg22a4fgq")))

(define-public crate-fasters_derive-0.4.0 (c (n "fasters_derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1vf5pamlmp3f1izyvy77rppfh0b5c467y8bfd42vph94kliv1g2m")))

