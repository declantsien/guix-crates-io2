(define-module (crates-io fa st fastsink) #:use-module (crates-io))

(define-public crate-fastsink-0.0.0 (c (n "fastsink") (v "0.0.0") (h "0yz20jazd9zxlb8z77n1dxl2hq25azb2xkyirp9fimi3qnw7sy59")))

(define-public crate-fastsink-0.1.2 (c (n "fastsink") (v "0.1.2") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.1") (d #t) (k 0)))) (h "1d9imjdacl6syw4gq114ckgh8wysah5y1xf9mq1bgc4plrf4pb6b")))

