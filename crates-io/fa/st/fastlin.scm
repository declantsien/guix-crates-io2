(define-module (crates-io fa st fastlin) #:use-module (crates-io))

(define-public crate-fastlin-0.1.0 (c (n "fastlin") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "boomphf") (r "^0.5.9") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.4") (d #t) (k 0)) (d (n "seq_io") (r "^0.3.1") (d #t) (k 0)))) (h "1hv40plqv3hwakqfs797394pvijdzhhn89svhf1dr96ypm4wyj61")))

(define-public crate-fastlin-0.2.0 (c (n "fastlin") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "boomphf") (r "^0.5.9") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.4") (d #t) (k 0)) (d (n "seq_io") (r "^0.3.1") (d #t) (k 0)))) (h "0dv3l1f0if65yxxxj89cav0r3z7qpg6c68yr20lsvphxpfvp2n1c")))

(define-public crate-fastlin-0.2.1 (c (n "fastlin") (v "0.2.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "boomphf") (r "^0.5.9") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.4") (d #t) (k 0)) (d (n "seq_io") (r "^0.3.1") (d #t) (k 0)))) (h "0rwjin3c98ppzpm4wkcw0vqjs2wygq8ik3xxpl3k21yvqkmis00l")))

