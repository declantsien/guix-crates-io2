(define-module (crates-io fa st fastcrypto-zkp) #:use-module (crates-io))

(define-public crate-fastcrypto-zkp-0.1.1 (c (n "fastcrypto-zkp") (v "0.1.1") (d (list (d (n "ark-bls12-377") (r "^0.4.0") (d #t) (k 2)) (d (n "ark-bls12-381") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 2)) (d (n "ark-crypto-primitives") (r "^0.4.0") (f (quote ("r1cs" "prf"))) (d #t) (k 0)) (d (n "ark-crypto-primitives") (r "^0.4.0") (f (quote ("r1cs" "prf"))) (d #t) (k 2)) (d (n "ark-ec") (r "^0.4.1") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.1") (f (quote ("asm" "parallel"))) (d #t) (k 0)) (d (n "ark-groth16") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-r1cs-std") (r "^0.4.0") (d #t) (k 2)) (d (n "ark-relations") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-serialize") (r "^0.4.1") (d #t) (k 0)) (d (n "ark-std") (r "^0.4.0") (f (quote ("parallel"))) (d #t) (k 2)) (d (n "blake2") (r "^0.10.6") (d #t) (k 2)) (d (n "blst") (r "^0.3.10") (d #t) (k 0)) (d (n "byte-slice-cast") (r "^1.2.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "fastcrypto") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (k 2)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)))) (h "09yy8hvpp095arr1mpia8a95ygarbb2gmrzfh5yabfmwbvh0xvzl")))

(define-public crate-fastcrypto-zkp-0.1.3 (c (n "fastcrypto-zkp") (v "0.1.3") (d (list (d (n "ark-bls12-377") (r "^0.4.0") (d #t) (k 2)) (d (n "ark-bls12-381") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-crypto-primitives") (r "^0.4.0") (f (quote ("r1cs" "prf"))) (d #t) (k 2)) (d (n "ark-ec") (r "^0.4.1") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.1") (f (quote ("asm"))) (d #t) (k 0)) (d (n "ark-groth16") (r "^0.4.0") (k 0)) (d (n "ark-r1cs-std") (r "^0.4.0") (d #t) (k 2)) (d (n "ark-relations") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-serialize") (r "^0.4.1") (d #t) (k 0)) (d (n "ark-snark") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-std") (r "^0.4.0") (d #t) (k 2)) (d (n "blake2") (r "^0.10.6") (d #t) (k 2)) (d (n "blst") (r "^0.3.10") (d #t) (k 0)) (d (n "byte-slice-cast") (r "^1.2.2") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "fastcrypto") (r "^0.1.5") (d #t) (k 0)) (d (n "ff") (r "^0.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "im") (r "^15") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "neptune") (r "^13.0.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json" "rustls-tls"))) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "test-strategy") (r "^0.3.1") (d #t) (k 2)) (d (n "tokio") (r "^1.24.1") (f (quote ("sync" "rt" "macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "typenum") (r "^1.13.0") (d #t) (k 0)))) (h "0wk8ab8x6fn0x3m71mzmbk64p4b8f77ql7niqnh8q00imr30jlfj") (f (quote (("e2e"))))))

