(define-module (crates-io fa st fast-router) #:use-module (crates-io))

(define-public crate-fast-router-0.1.0 (c (n "fast-router") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1hyrnrw4kjb6pncinq74c34z5x6mn86i731666q2ab85yqa5mjp7")))

(define-public crate-fast-router-0.1.1 (c (n "fast-router") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "141flbzd5b03b6qjn48y6k42p3jndk5vs6d3k358jwc3zvm9x4z9")))

