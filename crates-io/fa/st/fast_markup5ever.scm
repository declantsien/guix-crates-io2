(define-module (crates-io fa st fast_markup5ever) #:use-module (crates-io))

(define-public crate-fast_markup5ever-0.11.0 (c (n "fast_markup5ever") (v "0.11.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "string_cache") (r "^0.8") (d #t) (k 0)) (d (n "string_cache_codegen") (r "^0.5.1") (d #t) (k 1)) (d (n "tendril") (r "^0.4") (d #t) (k 0)))) (h "1cbgv040n4vcyf3j3b6a07249jxp3pmln17w89irg8pzjyvx9fm6")))

(define-public crate-fast_markup5ever-0.11.1 (c (n "fast_markup5ever") (v "0.11.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "string_cache") (r "^0.8") (d #t) (k 0)) (d (n "string_cache_codegen") (r "^0.5.1") (d #t) (k 1)) (d (n "tendril") (r "^0.4") (d #t) (k 0)))) (h "0z2mzzn05mbyffk7bdkk00iws3hyf9z8scchvn9m605zx6vz76gq")))

