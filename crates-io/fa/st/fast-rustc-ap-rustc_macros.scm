(define-module (crates-io fa st fast-rustc-ap-rustc_macros) #:use-module (crates-io))

(define-public crate-fast-rustc-ap-rustc_macros-1.0.0 (c (n "fast-rustc-ap-rustc_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1kz7lbpdv2bfl97fgkc3qpa4arhkhk73gi3lh3vqgy9m3schi0gz")))

