(define-module (crates-io fa st fast_set) #:use-module (crates-io))

(define-public crate-fast_set-0.1.0 (c (n "fast_set") (v "0.1.0") (h "0a2ysw9zclm34yl7xfdzynn0cra00lmnfdk6q4vhjz5p0vm5xvv6")))

(define-public crate-fast_set-0.1.1 (c (n "fast_set") (v "0.1.1") (h "1qqlnp9ib08s8pv9h401na4pi16cpjgidk4djackd42vkcshxjzj")))

