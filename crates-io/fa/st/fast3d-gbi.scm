(define-module (crates-io fa st fast3d-gbi) #:use-module (crates-io))

(define-public crate-fast3d-gbi-0.4.3 (c (n "fast3d-gbi") (v "0.4.3") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "pigment64") (r "^0.2") (d #t) (k 0)))) (h "0wakrgnw16wkagpblx3wsgwrnhbyy751mqcz07wyqmc4clgb8nzw") (f (quote (("hardware_version_1") ("gbifloats") ("f3dex2e") ("f3dex2") ("default" "f3dex2"))))))

(define-public crate-fast3d-gbi-0.4.4 (c (n "fast3d-gbi") (v "0.4.4") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "pigment64") (r "^0.2") (d #t) (k 0)))) (h "0rsddi3bvg9f148l9n721m9hg4lr7fmzbz61c7iifa5dpsgcccx0") (f (quote (("hardware_version_1") ("gbifloats") ("f3dex2e") ("f3dex2") ("default" "f3dex2"))))))

(define-public crate-fast3d-gbi-0.5.0 (c (n "fast3d-gbi") (v "0.5.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "pigment64") (r "^0.3.0") (d #t) (k 0)))) (h "0s2sdl46fss4mgzxpcfpqgrd4xib3054xnrbg42jrdaagqmkf6yv") (f (quote (("hardware_version_1") ("gbifloats") ("f3dex2e") ("f3dex2") ("default" "f3dex2"))))))

