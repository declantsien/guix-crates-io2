(define-module (crates-io fa st fast_fmt) #:use-module (crates-io))

(define-public crate-fast_fmt-0.1.0 (c (n "fast_fmt") (v "0.1.0") (d (list (d (n "void") (r "^1") (d #t) (k 0)))) (h "1zd3p9c1wn3gv9mkma3kdsw84zm6rz5gvcy7g95wrwjwc131mlpn") (f (quote (("with_std") ("nightly") ("default" "with_std")))) (y #t)))

(define-public crate-fast_fmt-0.1.1 (c (n "fast_fmt") (v "0.1.1") (d (list (d (n "void") (r "^1") (k 0)))) (h "0nkf3w8n8ls7ldgrha02zbnijmzlv8g9lp1d0fj8vngpsvn1qi2s") (f (quote (("with_std" "void/std") ("nightly") ("default" "with_std")))) (y #t)))

(define-public crate-fast_fmt-0.1.2 (c (n "fast_fmt") (v "0.1.2") (d (list (d (n "numtoa") (r "^0.0.7") (d #t) (k 0)) (d (n "void") (r "^1") (k 0)))) (h "05pvm7511xvm0c9yml8hwxpnjwjf9475f06rjli7k0laj7bwrllh") (f (quote (("with_std" "void/std") ("nightly") ("default" "with_std"))))))

(define-public crate-fast_fmt-0.1.3 (c (n "fast_fmt") (v "0.1.3") (d (list (d (n "numtoa") (r "^0.0.7") (d #t) (k 0)) (d (n "void") (r "^1") (k 0)))) (h "0k2wmllx8b3hz65xd4zramws766n9gxy85la5zimyk4m14zv7v6y") (f (quote (("with_std" "void/std") ("nightly") ("default" "with_std"))))))

