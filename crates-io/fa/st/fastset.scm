(define-module (crates-io fa st fastset) #:use-module (crates-io))

(define-public crate-fastset-0.1.0 (c (n "fastset") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0997k8whqjgm00g4kixa1dykyzdl07liszcxczl1anp69lrxsw4k") (f (quote (("default"))))))

(define-public crate-fastset-0.1.1 (c (n "fastset") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 2)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a1v8srrkrc0hmd8xsl3pjzkgby5cl0kajyr28id5lzshxb9b3fk") (f (quote (("default"))))))

(define-public crate-fastset-0.1.2 (c (n "fastset") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 2)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vcjh8xwnqnm4ppls9q76k2x0bbg8896vbifrrbgnqivic7wkqj0") (f (quote (("default"))))))

(define-public crate-fastset-0.2.0 (c (n "fastset") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 2)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mn2kl7sj60wj0pa7979s5y95qsacqd13q0s3fqwkbqi53z9b01v") (f (quote (("default"))))))

(define-public crate-fastset-0.2.1 (c (n "fastset") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 2)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "03kgmw0705vranbcngldgdfkx54n7cfn05hgaixqgmlnrj9jjhal") (f (quote (("default"))))))

(define-public crate-fastset-0.3.0 (c (n "fastset") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 2)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rj2237rgmi5wsghggknal05fn63yipiv9075xbrdj95wz3b0cz7") (f (quote (("default"))))))

(define-public crate-fastset-0.4.0 (c (n "fastset") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 2)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wj6cx2brrw1py8025w0qpdmvhvxx1cqsid670sgrdbmhbshhw3s")))

(define-public crate-fastset-0.4.1 (c (n "fastset") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 2)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 2)))) (h "0vilsx2mwd7xys6ivss5k48q3f4xpyalsgsz0254lqh3xq8n7pkq")))

