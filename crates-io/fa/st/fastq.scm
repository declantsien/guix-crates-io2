(define-module (crates-io fa st fastq) #:use-module (crates-io))

(define-public crate-fastq-0.2.0 (c (n "fastq") (v "0.2.0") (d (list (d (n "lz4") (r "^1.20") (d #t) (k 2)) (d (n "memchr") (r ">= 0.1") (d #t) (k 0)) (d (n "parasailors") (r "^0.3") (d #t) (k 2)))) (h "07k0aiac1zshkxz7mi5pgp1cf4nsw8v7j45k0jnfsgm4lj88r8f7")))

(define-public crate-fastq-0.3.0 (c (n "fastq") (v "0.3.0") (d (list (d (n "bio") (r "^0.10") (d #t) (k 2)) (d (n "lz4") (r "^1.20") (d #t) (k 2)) (d (n "memchr") (r ">= 0.1") (d #t) (k 0)) (d (n "parasailors") (r "^0.3") (d #t) (k 2)))) (h "0r5vs9dcfbbhylml2cpfac1mqvr7k5h4s2phmpzjfha6dp8396fn")))

(define-public crate-fastq-0.3.1 (c (n "fastq") (v "0.3.1") (d (list (d (n "bio") (r "^0.10") (d #t) (k 2)) (d (n "lz4") (r "^1.20") (d #t) (k 2)) (d (n "memchr") (r ">= 0.1") (d #t) (k 0)) (d (n "parasailors") (r "^0.3") (d #t) (k 2)))) (h "19xqj619pd1kysbi12dbqx4ca52f1zm157nnir8b7yh1sgcmrwqz")))

(define-public crate-fastq-0.4.0 (c (n "fastq") (v "0.4.0") (d (list (d (n "bio") (r "^0.10") (d #t) (k 2)) (d (n "flate2") (r "^0.2") (f (quote ("zlib"))) (k 0)) (d (n "lz4") (r "^1.20") (d #t) (k 0)) (d (n "memchr") (r ">= 0.1") (d #t) (k 0)) (d (n "parasailors") (r "^0.3") (d #t) (k 2)))) (h "18j955nlviwkxwj66ac67qbbsz85z5bsi3g6rg3f9xji2myxsvsy")))

(define-public crate-fastq-0.4.1 (c (n "fastq") (v "0.4.1") (d (list (d (n "bio") (r "^0.10") (d #t) (k 2)) (d (n "flate2") (r "^0.2") (f (quote ("zlib"))) (k 0)) (d (n "lz4") (r "^1.20") (d #t) (k 0)) (d (n "memchr") (r ">= 0.1") (d #t) (k 0)) (d (n "parasailors") (r "^0.3") (d #t) (k 2)))) (h "0lhzj683996a1rp74ca3r7ahpgsld4afb596df9z62icrh3hll73")))

(define-public crate-fastq-0.4.2 (c (n "fastq") (v "0.4.2") (d (list (d (n "bio") (r "^0.10") (d #t) (k 2)) (d (n "flate2") (r "^0.2") (f (quote ("zlib"))) (k 0)) (d (n "lz4") (r "^1.20") (d #t) (k 0)) (d (n "memchr") (r ">= 0.1") (d #t) (k 0)) (d (n "parasailors") (r "^0.3") (d #t) (k 2)))) (h "0ddyz1gqyqn8ak1z4m6ci0ih1nkcyfwp9vy9y88bsm322sdxbhxr")))

(define-public crate-fastq-0.5.0 (c (n "fastq") (v "0.5.0") (d (list (d (n "bio") (r "^0.10") (d #t) (k 2)) (d (n "flate2") (r "^0.2.18") (f (quote ("zlib"))) (k 0)) (d (n "lz4") (r "^1.20") (d #t) (k 0)) (d (n "memchr") (r ">= 0.1") (d #t) (k 0)) (d (n "parasailors") (r "^0.3") (d #t) (k 2)))) (h "1jadryk1ys7ab5z46609bvd6l5g8k4pvigk0mbx9dqdajd0s2qlc")))

(define-public crate-fastq-0.6.0 (c (n "fastq") (v "0.6.0") (d (list (d (n "bio") (r ">= 0.10") (d #t) (k 2)) (d (n "flate2") (r ">= 0.2.18") (f (quote ("zlib"))) (k 0)) (d (n "lz4") (r "^1.20") (d #t) (k 0)) (d (n "memchr") (r ">= 0.1") (d #t) (k 0)) (d (n "parasailors") (r ">= 0.3") (d #t) (k 2)))) (h "177jsfabnk3zl5zml6qvidzjpk53dp62rqjbdhbhr8cg7ms59p60")))

