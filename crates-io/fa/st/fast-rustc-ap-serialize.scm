(define-module (crates-io fa st fast-rustc-ap-serialize) #:use-module (crates-io))

(define-public crate-fast-rustc-ap-serialize-1.0.0 (c (n "fast-rustc-ap-serialize") (v "1.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0xn79xpghf6vkv01b7x73nli3snrc7vjq8sr82p9f8g7iyxzafxj")))

