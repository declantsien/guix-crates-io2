(define-module (crates-io fa st fastorm-macro) #:use-module (crates-io))

(define-public crate-fastorm-macro-0.1.0 (c (n "fastorm-macro") (v "0.1.0") (d (list (d (n "fastorm-trait") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("runtime-tokio" "sqlite" "mysql" "macros" "any"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vm9lnhi9z5iglm4pgia5ibgkfs2l31zw42ycs333bzqq31l45lh")))

