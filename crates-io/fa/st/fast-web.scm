(define-module (crates-io fa st fast-web) #:use-module (crates-io))

(define-public crate-fast-web-0.1.1 (c (n "fast-web") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "196mjclj7rj5jhm6kg86sjpmagxlsjlxxvg9gbd5s0ncl0z9pd6q")))

(define-public crate-fast-web-0.1.2 (c (n "fast-web") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "multipart") (r "^0.18") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0z2scz8lp4j40pn99cb70xgphva4vgakwz0kjww5fw9n3dgqsj8c")))

