(define-module (crates-io fa st fast_trie_tree) #:use-module (crates-io))

(define-public crate-fast_trie_tree-0.1.0 (c (n "fast_trie_tree") (v "0.1.0") (h "02r102f31p8b02y8h2vv4fwyl59n15r8kgf5gmzq9wbkknjaz3pq")))

(define-public crate-fast_trie_tree-0.1.1 (c (n "fast_trie_tree") (v "0.1.1") (h "04q0nq73xnm00bg0fr7my2i2xp5v5r8rj9ivmcqmf4syydbhq58w")))

(define-public crate-fast_trie_tree-0.1.2 (c (n "fast_trie_tree") (v "0.1.2") (h "0xmqsh0rgfbz4i16v0ks9yk0fbgy1n8088vlj3a0fyd8lnamxwdb")))

(define-public crate-fast_trie_tree-0.1.3 (c (n "fast_trie_tree") (v "0.1.3") (h "1sxgp2zmmyygz2qin8r9hd4pszcz66pl6y0grshavnsmsidx59mk")))

