(define-module (crates-io fa st fast_forward) #:use-module (crates-io))

(define-public crate-fast_forward-0.0.1 (c (n "fast_forward") (v "0.0.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "07npvwxg6xgc7v53c88p1lrlvc4m71pp7na8gbibq50b47c553h2") (s 2) (e (quote (("hashbrown" "dep:hashbrown"))))))

(define-public crate-fast_forward-0.0.2 (c (n "fast_forward") (v "0.0.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "0i475xwj8gi8l0dg17pfqsabcarlwr74scp6qzs9xkayri61w386") (s 2) (e (quote (("hashbrown" "dep:hashbrown"))))))

