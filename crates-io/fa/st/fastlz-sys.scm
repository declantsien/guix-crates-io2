(define-module (crates-io fa st fastlz-sys) #:use-module (crates-io))

(define-public crate-fastlz-sys-0.1.0 (c (n "fastlz-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0fasrxccb7glkq6pblsk5xn7z76f26ylc89qs845ljcr8a5pipi0") (l "fastlz")))

