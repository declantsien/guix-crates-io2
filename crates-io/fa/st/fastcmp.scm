(define-module (crates-io fa st fastcmp) #:use-module (crates-io))

(define-public crate-fastcmp-0.1.0 (c (n "fastcmp") (v "0.1.0") (d (list (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "simd") (r "^0") (o #t) (d #t) (k 0)))) (h "1dikvj8nw21csn7n2q274swf6rz447r8zvgpgx18gyv2i8yxwaxp") (f (quote (("simd_support" "simd"))))))

(define-public crate-fastcmp-1.0.0 (c (n "fastcmp") (v "1.0.0") (d (list (d (n "rand") (r "^0") (d #t) (k 2)))) (h "0ws9v778kp3k257pywkl9fv1b1f78r3shv0shk4n5cm4bm2vaza3")))

(define-public crate-fastcmp-1.0.1 (c (n "fastcmp") (v "1.0.1") (d (list (d (n "rand") (r "^0") (d #t) (k 2)))) (h "1pprfma5jjr0mgb9h98rvpbjdb33zf5d3dqhvz69f8m63x1q7vw9")))

