(define-module (crates-io fa st fastdl) #:use-module (crates-io))

(define-public crate-fastdl-0.1.0 (c (n "fastdl") (v "0.1.0") (d (list (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "101zjv62badrkq6f2yprb1857m4549w53d4792a3f7r11yd1wdhq")))

(define-public crate-fastdl-0.2.0 (c (n "fastdl") (v "0.2.0") (d (list (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "1a924hyrl6ldwz52rcy9dvq0mfrz60fv31ay7x11hpa39mq430p0")))

