(define-module (crates-io fa st fast-noise-lite-rs) #:use-module (crates-io))

(define-public crate-fast-noise-lite-rs-0.8.0 (c (n "fast-noise-lite-rs") (v "0.8.0") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 2)))) (h "0gpqfdpgm0p8snk129qdwzfz6v9i7p6283sa51fxjslsjjxyg3f6")))

