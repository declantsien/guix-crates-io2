(define-module (crates-io fa st fastly_rt) #:use-module (crates-io))

(define-public crate-fastly_rt-0.1.0 (c (n "fastly_rt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "gzip" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "09mww9k31vdk1j0c6v7yi3n5lwf1h53p5dj611mg1d0703lch1xn")))

