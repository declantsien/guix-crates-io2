(define-module (crates-io fa st fast_chemail) #:use-module (crates-io))

(define-public crate-fast_chemail-0.9.3 (c (n "fast_chemail") (v "0.9.3") (d (list (d (n "ascii_utils") (r "^0") (d #t) (k 0)))) (h "0lfaxq1jsc4prqmy2aq0540n1lg90ba60y8x85gyc14rhm1924i0")))

(define-public crate-fast_chemail-0.9.4 (c (n "fast_chemail") (v "0.9.4") (d (list (d (n "ascii_utils") (r "^0") (d #t) (k 0)))) (h "0l77pd5bk7szg679p8wi5bhs3alzscmmxanp03wr74fxxdj80pp8")))

(define-public crate-fast_chemail-0.9.5 (c (n "fast_chemail") (v "0.9.5") (d (list (d (n "ascii_utils") (r "^0") (d #t) (k 0)))) (h "0jvhafjrhkc20pj146995fzzf95d84fbif481w531hrnkvw1sphi")))

(define-public crate-fast_chemail-0.9.6 (c (n "fast_chemail") (v "0.9.6") (d (list (d (n "ascii_utils") (r "^0") (d #t) (k 0)))) (h "1r79x2i7bhk8y4nv7q245dlifxryszmby4k3psm2qk321p9kjnj9")))

