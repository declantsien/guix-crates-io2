(define-module (crates-io fa st fast-walker) #:use-module (crates-io))

(define-public crate-fast-walker-0.0.0 (c (n "fast-walker") (v "0.0.0") (d (list (d (n "tokio") (r "^1.26.0") (o #t) (d #t) (k 0)))) (h "178dz4pcq3h9nj6vw1jm9i21mjhvhc4q2wivymj2zk5aasvnazyh") (f (quote (("default"))))))

(define-public crate-fast-walker-0.1.0 (c (n "fast-walker") (v "0.1.0") (d (list (d (n "tokio") (r "^1.26.0") (o #t) (d #t) (k 0)))) (h "150w58vpia0s5c572b0lrqn8x1n4yy83qx1f1qqdgd95n4xawva3") (f (quote (("default"))))))

(define-public crate-fast-walker-0.1.1 (c (n "fast-walker") (v "0.1.1") (d (list (d (n "tokio") (r "^1.26.0") (o #t) (d #t) (k 0)))) (h "1vzqzxvch8dxdkgwchyh44cadv6c72k6j0rn07zhvkmp394xn47c") (f (quote (("default"))))))

(define-public crate-fast-walker-0.1.2 (c (n "fast-walker") (v "0.1.2") (d (list (d (n "tokio") (r "^1.26.0") (o #t) (d #t) (k 0)))) (h "0sxmi5kswxfx3djzxg2x1blw7f7a6hmbbl5lh7i9rjw5s0kn1vys") (f (quote (("default"))))))

(define-public crate-fast-walker-0.2.0 (c (n "fast-walker") (v "0.2.0") (d (list (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (o #t) (d #t) (k 0)))) (h "1pcfhfq0gaw4y1jyrzaq21x5nkh07af6qp7qa29kmklcdkvqf09w") (f (quote (("default"))))))

(define-public crate-fast-walker-0.2.1 (c (n "fast-walker") (v "0.2.1") (d (list (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (o #t) (d #t) (k 0)))) (h "09nhsgbvkhsr28yr37hqr2frh6hdbhs6mkv8r5izz1k6jqwwr354") (f (quote (("default"))))))

