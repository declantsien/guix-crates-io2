(define-module (crates-io fa st fast-blurhash) #:use-module (crates-io))

(define-public crate-fast-blurhash-1.0.0 (c (n "fast-blurhash") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "ril") (r "^0.9") (f (quote ("webp"))) (d #t) (k 2)))) (h "0pa8d636x4r15185gfhs84hp374i8cz92mba71wyy7rm2dczczlx")))

(define-public crate-fast-blurhash-1.0.1 (c (n "fast-blurhash") (v "1.0.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "ril") (r "^0.9") (f (quote ("webp"))) (d #t) (k 2)))) (h "00qmh9k4g0qgpilic6lk45pwpxj1hsx4znwcqf7ll5r97vrkpr3y")))

