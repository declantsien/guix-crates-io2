(define-module (crates-io fa st fastnoise-simd) #:use-module (crates-io))

(define-public crate-fastnoise-simd-0.0.1 (c (n "fastnoise-simd") (v "0.0.1") (d (list (d (n "fastnoise_simd_bindings") (r "^0.0.1") (d #t) (k 0)))) (h "011nznk8nvknj59sp0i2mfkrnq1vr1gb9wgs50z74k8pz08yspb0")))

(define-public crate-fastnoise-simd-0.1.0 (c (n "fastnoise-simd") (v "0.1.0") (d (list (d (n "fastnoise_simd_bindings") (r "^0.1.0") (d #t) (k 0)))) (h "0s6vq3qi02djc6wbn2dnxy7phl2m8c6fxswb9cyalnhc0qpq4z95")))

(define-public crate-fastnoise-simd-0.1.1 (c (n "fastnoise-simd") (v "0.1.1") (d (list (d (n "fastnoise_simd_bindings") (r "^0.1.1") (d #t) (k 0)))) (h "0hla63igym49fkd29zwx0lydp3sh9awlzixvja9jmbfdiwyxamni")))

