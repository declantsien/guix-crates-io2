(define-module (crates-io fa st fastcwt) #:use-module (crates-io))

(define-public crate-fastcwt-0.1.0 (c (n "fastcwt") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0nfkwlrlx6fw8a80cklcwixjan8ifjdg9iw8ir76piqrbsv7sz2s")))

(define-public crate-fastcwt-0.1.1 (c (n "fastcwt") (v "0.1.1") (d (list (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0z1kv03jpl1800kvp2i6zn5q83cs8hvyd3fizih9w0ijv754a1z3")))

(define-public crate-fastcwt-0.1.2 (c (n "fastcwt") (v "0.1.2") (d (list (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "16ppdyy66mmql4h7qf66rx346lmx284v1g4jkidjfviv099dxm73")))

(define-public crate-fastcwt-0.1.3 (c (n "fastcwt") (v "0.1.3") (d (list (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "096q9kgg7sm3w9s8nda772n899d28ijif2lhcnsa62azpl0yi7c5")))

(define-public crate-fastcwt-0.1.4 (c (n "fastcwt") (v "0.1.4") (d (list (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0j7dhzhnmmf73wmkm525y2n3j7fxysfnp1mrsr1ia4symwfcf22r")))

(define-public crate-fastcwt-0.1.5 (c (n "fastcwt") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "1ykhvdb4sjval4zklzg0kgyd5031mz2i41gk9xrvr1yvyp8jn5g2")))

(define-public crate-fastcwt-0.1.6 (c (n "fastcwt") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0iw4hgsza433s4lcgbmyibdgsgr9ihnnrq1pylzf20sz9s6c223l")))

(define-public crate-fastcwt-0.1.7 (c (n "fastcwt") (v "0.1.7") (d (list (d (n "mkaudiolibrary") (r "^0.1.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "1m6n3922xfyhb7r275xhrsxp7yszsna92xg7vvfbwb77v39l532v")))

(define-public crate-fastcwt-0.1.8 (c (n "fastcwt") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "1iq9g9brdvmjhzag3887ibb7yaz0ha4sdmd04znj5gzrklkfhir8")))

(define-public crate-fastcwt-0.1.9 (c (n "fastcwt") (v "0.1.9") (d (list (d (n "no_denormals") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "08ar56iyj0wbamvmm9j1yyls5v2bmghsx9cvqhlbmq5lvj0nxcsm")))

