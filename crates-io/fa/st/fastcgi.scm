(define-module (crates-io fa st fastcgi) #:use-module (crates-io))

(define-public crate-fastcgi-1.0.0-alpha (c (n "fastcgi") (v "1.0.0-alpha") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0cnwxjm4bnbsx8i54436p7rjwmmc0vflc79xr6h4vlz2p1wp2w8a")))

(define-public crate-fastcgi-1.0.0-alpha.2 (c (n "fastcgi") (v "1.0.0-alpha.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "196xhhqw3336xwmr7631zjwcjqsyz8f9njkxcckm8kprbcrjrdhl")))

(define-public crate-fastcgi-1.0.0-alpha.3 (c (n "fastcgi") (v "1.0.0-alpha.3") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "161ldz4hq79xalhrc7aksa5v5nm9maggzkaafm4zrxm138r6x0gl")))

(define-public crate-fastcgi-1.0.0-beta (c (n "fastcgi") (v "1.0.0-beta") (d (list (d (n "libc") (r "^0.2.9") (d #t) (k 0)))) (h "0z8p79664nyjg8zqa6hklrvlyrddaqg0dhxymm6inybgsxz6092b")))

(define-public crate-fastcgi-1.0.0 (c (n "fastcgi") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.9") (d #t) (k 0)))) (h "1k51mrs958v03bdzk014mkva2yvx1l3yn27508b2i85y907rl5f4")))

