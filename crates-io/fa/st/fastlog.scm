(define-module (crates-io fa st fastlog) #:use-module (crates-io))

(define-public crate-fastlog-0.0.1 (c (n "fastlog") (v "0.0.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0hzmjrw301433n66jhd0pzwwcx13wrwrv71asilsncq13l79h5gy")))

(define-public crate-fastlog-0.1.0 (c (n "fastlog") (v "0.1.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0f7jb6sm3zaszmpw2070giywd9rhlbr794vyijmkippgf2k906li")))

(define-public crate-fastlog-0.1.1 (c (n "fastlog") (v "0.1.1") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1w11yc62q3hk6alxmyf1761cdvw1npaplnpwd5w1dyd2vya4salf")))

(define-public crate-fastlog-0.2.0 (c (n "fastlog") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1c66l7rlnz4fnjqqv83nv2iyqdgjj8qjbym2yhq03r6j6mlcmwyv")))

(define-public crate-fastlog-0.2.1 (c (n "fastlog") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0mhwhbfrywxhn6khz5rdciy4kwyxanyhsirhp20p581lan1h0b44")))

