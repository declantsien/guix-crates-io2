(define-module (crates-io fa st fast_rsync) #:use-module (crates-io))

(define-public crate-fast_rsync-0.1.0 (c (n "fast_rsync") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0c7b7hccmaqdfjnx0dphn4fi7rc5svmxd62bwb689c3sqix4cm3s")))

(define-public crate-fast_rsync-0.1.1 (c (n "fast_rsync") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "18qagf6ng22qahkch35fh7cc3rjxa2zp7cz15hzrvgy4s3aj0bdj")))

(define-public crate-fast_rsync-0.1.2 (c (n "fast_rsync") (v "0.1.2") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1dmd2fl5df6kjwh6dwc7f7z5n56j24y8g06xqbv5w3qfm3mws9bw")))

(define-public crate-fast_rsync-0.1.3 (c (n "fast_rsync") (v "0.1.3") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.4") (d #t) (k 0) (p "packed_simd_2")) (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1im8ziv6x5dw4fy55k7rw75zr98grvccxy6zjjmvgyyvj7w7i89p")))

(define-public crate-fast_rsync-0.1.4 (c (n "fast_rsync") (v "0.1.4") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1jipqhsfpgnibbfxn2hisdrjia9i25dqyhn0qgfc9nz3jp30cjxx")))

(define-public crate-fast_rsync-0.2.0 (c (n "fast_rsync") (v "0.2.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (k 2)) (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "150705ck5zd9wr4mnkz2pxbgryy11xk28ak1c16ci484wbbc95rx")))

