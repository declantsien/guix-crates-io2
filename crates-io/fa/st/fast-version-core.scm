(define-module (crates-io fa st fast-version-core) #:use-module (crates-io))

(define-public crate-fast-version-core-0.1.0 (c (n "fast-version-core") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "022b34lzaz21dp649ydbkpyxvvy5pzisca5dgmmawjril4xxyx35") (f (quote (("default" "alloc") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fast-version-core-0.1.1 (c (n "fast-version-core") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0frcpxvhlbrqpiykddrpv78p2fvwl782x9qnhr9rywacjyd6n93v") (f (quote (("default" "alloc") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fast-version-core-0.2.0 (c (n "fast-version-core") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09gn5zpd321asvwnllnqsdm60sy6lz13rs5lfzy1xx9jyl449mpn") (f (quote (("nightly") ("default" "alloc") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fast-version-core-0.2.2 (c (n "fast-version-core") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dshhfnyl6zxlj4p7mf4z561hwz6y3xhyfhjyp1jcljbfcbw3m15") (f (quote (("nightly") ("default" "alloc") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fast-version-core-0.2.3 (c (n "fast-version-core") (v "0.2.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "043wyw7kzcnmy0iias65pbf6wrm2dad3ymmyy091w9ih7l5njlfw") (f (quote (("nightly") ("default" "alloc") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde"))))))

