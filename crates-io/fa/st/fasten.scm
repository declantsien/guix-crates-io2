(define-module (crates-io fa st fasten) #:use-module (crates-io))

(define-public crate-fasten-0.1.8 (c (n "fasten") (v "0.1.8") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "statistical") (r "^0.1.1") (d #t) (k 0)))) (h "0fvwmryicm47prbhjm6ng512rfq0frsz3m626cw46ncvbp7c4rs8") (y #t)))

(define-public crate-fasten-0.1.11 (c (n "fasten") (v "0.1.11") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "statistical") (r "^0.1.1") (d #t) (k 0)))) (h "0vgab4l3nsfsh7l74l3xlqnl5psb32yc65819gshdcgwys79rppx")))

(define-public crate-fasten-0.1.12 (c (n "fasten") (v "0.1.12") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "statistical") (r "^0.1.1") (d #t) (k 0)))) (h "0a7dwiqbk9z4m02d4bvw02r96w62f95vi9p7lk543yrsqfi3wpaj")))

(define-public crate-fasten-0.1.13 (c (n "fasten") (v "0.1.13") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "statistical") (r "^0.1.1") (d #t) (k 0)))) (h "1l5ycdk17sj0rhj4m5gi7cmbzbbwvy5xiv6vavmwqzgfam9hgdq6")))

(define-public crate-fasten-0.3.3 (c (n "fasten") (v "0.3.3") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "fastq") (r "^0.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "statistical") (r "^0.1.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "17jh9yscfh6w5hkc5syw39qd8iahyk9bp86h9dx8rilj4kwm0sl4")))

(define-public crate-fasten-0.4.3 (c (n "fasten") (v "0.4.3") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "fastq") (r "^0.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "statistical") (r "^0.1.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "05ha9bamx811kixb370pw52vzzdqgjgqnniw468kxpv1q1158kxx")))

(define-public crate-fasten-0.4.4 (c (n "fasten") (v "0.4.4") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "fastq") (r "^0.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "statistical") (r "^0.1.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1y5gng87qdrdwghc7klcrfp4hv2pgnysvikr13ixp4fnkmixdvxk")))

(define-public crate-fasten-0.7.2 (c (n "fasten") (v "0.7.2") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "fastq") (r "^0.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "statistical") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "08z6cr04p4bb35d52wiyszbw96n86rsh7pxcr8gnjix4vyx15dij")))

(define-public crate-fasten-0.8.1 (c (n "fasten") (v "0.8.1") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "fastq") (r "^0.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "statistical") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1464v1bx12zgsijpb98vlvgvqv279hyavgk9m0lq75wn7hsj740z")))

(define-public crate-fasten-0.8.3 (c (n "fasten") (v "0.8.3") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "fastq") (r "^0.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "statistical") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1rg2fhvz1xp0l90saccrdivigfb7yzvp9w8ci1ps2r317cgiqlv3")))

