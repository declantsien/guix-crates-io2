(define-module (crates-io fa st faster-hex) #:use-module (crates-io))

(define-public crate-faster-hex-0.1.0 (c (n "faster-hex") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)))) (h "0vvr6ny0fkp9iy1xcvsvax5mmxwcv4jrj6dgw5xnnq2k98xzl2k0")))

(define-public crate-faster-hex-0.2.0 (c (n "faster-hex") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)))) (h "03gw6swfvwprpsfav4szf9ab33m11rslmdvabih68kqvxpn9ydf4") (y #t)))

(define-public crate-faster-hex-0.1.1 (c (n "faster-hex") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)))) (h "0fj8v3ckcn06pfzy1i9akmvm91a4zq0d9w0hifxjrv11r7nf98if")))

(define-public crate-faster-hex-0.3.0 (c (n "faster-hex") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)))) (h "1w5jmsr7wdw83qgi6a1gg9lrlbnajhn2zl35n2ng35lq4kiviy27")))

(define-public crate-faster-hex-0.3.1 (c (n "faster-hex") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)))) (h "0ii61cr77nghbxqyi5n6lvg5x4j1yf85abk9hb1fiammmz5crf55")))

(define-public crate-faster-hex-0.4.0 (c (n "faster-hex") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)))) (h "0nhcqy6fbcycx6w3zvna72s4ifpdwpvnhc1xyy2fnpxxxp8xy3gc")))

(define-public crate-faster-hex-0.4.1 (c (n "faster-hex") (v "0.4.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)))) (h "0a2c9n2hk3z83nch3dglbznfynqszd3rdydfih0v0fz04gfki09l")))

(define-public crate-faster-hex-0.5.0 (c (n "faster-hex") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)))) (h "08x05gdd5hsxmhbk820043abc1wqmgms56amawqmcr45zn1ax2yn")))

(define-public crate-faster-hex-0.6.0 (c (n "faster-hex") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)))) (h "1bd231xkv931m685c0vivvw0w68fidlrpsbwkm7lis1dm3z62amh")))

(define-public crate-faster-hex-0.6.1 (c (n "faster-hex") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)))) (h "1y39f51sc867gwhq7bs1hlzgyl3r0ym8ammhjz7rbcjk9n4wxqji")))

(define-public crate-faster-hex-0.8.0 (c (n "faster-hex") (v "0.8.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "03997gp63dw3yfm8a2n968kwzqhl8mnfmhzqhwihzv2y38l2s179") (f (quote (("std" "alloc") ("default" "std" "serde") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-faster-hex-0.8.1 (c (n "faster-hex") (v "0.8.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12ikld53h5d682rn1j85d77n90pq4vy5mncwdaqhm0hgjgxpp7r3") (f (quote (("std" "alloc") ("default" "std" "serde") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-faster-hex-0.8.2 (c (n "faster-hex") (v "0.8.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0j2kpvrradd6x99awxwiib3qcb7rqq30sq3y689hyxq9xrhx9x0z") (f (quote (("std" "alloc") ("default" "std" "serde") ("alloc")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-faster-hex-0.9.0 (c (n "faster-hex") (v "0.9.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10wi4vqbdpkamw4qvra1ijp4as2j7j1zc66g4rdr6h0xv8gb38m2") (f (quote (("std" "alloc") ("default" "std" "serde") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde"))))))

