(define-module (crates-io fa st fast-nat) #:use-module (crates-io))

(define-public crate-fast-nat-1.0.0 (c (n "fast-nat") (v "1.0.0") (d (list (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "profiling") (r "^1.0.9") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0w1srcvmbnzzhcyn648lr6k8h7b7grnm03dmb98mi0hnqbrjkxrb") (f (quote (("profile-puffin" "profiling/profile-with-puffin") ("default"))))))

