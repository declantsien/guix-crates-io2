(define-module (crates-io fa st fast-cli-core) #:use-module (crates-io))

(define-public crate-fast-cli-core-0.0.0 (c (n "fast-cli-core") (v "0.0.0") (h "0qph1a1w4b9b33s2dza2iv126wcb2bf1315c7za9lvw68isdk8h7")))

(define-public crate-fast-cli-core-0.1.4 (c (n "fast-cli-core") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "headless_chrome") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1jxp6cqb2ghgz5nw7flnjpgcs4nl18w91b9av138m9v9rd57dpfj")))

(define-public crate-fast-cli-core-0.1.5 (c (n "fast-cli-core") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "headless_chrome") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0qrjgpy2b67qbyzwywc8qw2qsf1k3243z5wiv0idab02b9ja8q0r")))

