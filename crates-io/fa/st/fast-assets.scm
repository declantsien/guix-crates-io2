(define-module (crates-io fa st fast-assets) #:use-module (crates-io))

(define-public crate-fast-assets-0.1.0 (c (n "fast-assets") (v "0.1.0") (d (list (d (n "pro_csv") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "1aqwlz22hq9jx20dbrpgapsiv8nn9inmvn7zazfhm2q9f36ydaf3")))

(define-public crate-fast-assets-0.1.1 (c (n "fast-assets") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pro_csv") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "1j8v1vr2jvzj0l6c12gbnrim7mvihpa58fvmzi881sn9qmrdfik2")))

(define-public crate-fast-assets-0.1.2 (c (n "fast-assets") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pro_csv") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "1hc4dq3mwnm84czbr7bnjcq4jz87mzsafgqc0vflmh404356krzw")))

(define-public crate-fast-assets-0.1.3 (c (n "fast-assets") (v "0.1.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pro_csv") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "1bs0y1xrn30k6vgs03fr9929vi958i3dskqvxkfcmmxfw1lhhp0j")))

(define-public crate-fast-assets-0.1.4 (c (n "fast-assets") (v "0.1.4") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pro_csv") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "1f94vgbj0gnjlxpzcycf42ng91h5v509ph5cqhxw1j7dpa3pw3aq")))

(define-public crate-fast-assets-0.1.5 (c (n "fast-assets") (v "0.1.5") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pro_csv") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "0w0k7p38ls1hm8zd8j7c1k4m7vicg2abz4cz19qb5ig85vfwfilp")))

(define-public crate-fast-assets-0.1.6 (c (n "fast-assets") (v "0.1.6") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "pro_csv") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "1g5pjw11wh1srii58f27zllmz9l2cabckrsrfg2fb71r8cgc9q0c")))

(define-public crate-fast-assets-0.1.7 (c (n "fast-assets") (v "0.1.7") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "pro_csv") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "137v2l4s5kflyy9k5wf837kr3839x63412gmbn8979vjnpsxw4hp")))

