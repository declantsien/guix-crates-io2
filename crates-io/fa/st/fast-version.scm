(define-module (crates-io fa st fast-version) #:use-module (crates-io))

(define-public crate-fast-version-0.1.0 (c (n "fast-version") (v "0.1.0") (d (list (d (n "fast-version-core") (r "^0.1.1") (d #t) (k 0)) (d (n "fast-version-derive") (r "^0.1.1") (d #t) (k 0)))) (h "019cai29hj9zz1awai4sc5kcbm0icpnv716js5z2zkgxqvq2agka") (f (quote (("serde" "fast-version-core/serde") ("default" "alloc") ("alloc"))))))

(define-public crate-fast-version-0.1.1 (c (n "fast-version") (v "0.1.1") (d (list (d (n "fast-version-core") (r "^0.1.1") (d #t) (k 0)) (d (n "fast-version-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1ydjima2fn4a7kz4xl3drjg9f0iz54nrfb8c8aflcwlwbsz4hwkd") (f (quote (("serde" "fast-version-core/serde") ("default" "alloc") ("alloc"))))))

(define-public crate-fast-version-0.2.1 (c (n "fast-version") (v "0.2.1") (d (list (d (n "fast-version-core") (r "^0.2.1") (d #t) (k 0)) (d (n "fast-version-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1s5hycs0y45crxmai5dn7kirw8h9n0hccqnyxf71spxlh91jb1pr") (f (quote (("serde" "fast-version-core/serde") ("nightly" "fast-version-core/nightly") ("default" "alloc") ("alloc"))))))

