(define-module (crates-io fa st fast-erasure-shake-rng) #:use-module (crates-io))

(define-public crate-fast-erasure-shake-rng-0.1.0 (c (n "fast-erasure-shake-rng") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "keccak") (r "^0.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (d #t) (k 0)))) (h "12ky80mpvzwk5w3gs891nr9z39b4q97xypszzrs8xlfj49yp00zl") (f (quote (("default" "getrandom")))) (s 2) (e (quote (("rand-core" "dep:rand_core") ("getrandom" "dep:getrandom" "rand_core?/getrandom"))))))

(define-public crate-fast-erasure-shake-rng-0.2.0 (c (n "fast-erasure-shake-rng") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "keccak") (r "^0.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (d #t) (k 0)))) (h "0hzx33cf9wky5a0161klmamxw86gjpfwlvxkd8h33s6ijfc8c1nj") (f (quote (("default" "getrandom")))) (s 2) (e (quote (("rand-core" "dep:rand_core") ("getrandom" "dep:getrandom" "rand_core?/getrandom"))))))

