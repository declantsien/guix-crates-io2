(define-module (crates-io fa st fast-rustc-ap-rustc_index) #:use-module (crates-io))

(define-public crate-fast-rustc-ap-rustc_index-1.0.0 (c (n "fast-rustc-ap-rustc_index") (v "1.0.0") (d (list (d (n "rustc_serialize") (r "^1.0.0") (d #t) (k 0) (p "fast-rustc-ap-serialize")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "14cbmm3qhk635m2l30zsa1l0mn7s97hq8ivg5023vi5yl6rsv181")))

