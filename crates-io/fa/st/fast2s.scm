(define-module (crates-io fa st fast2s) #:use-module (crates-io))

(define-public crate-fast2s-0.1.0 (c (n "fast2s") (v "0.1.0") (d (list (d (n "character_converter") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "fst") (r "^0.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "opencc-rust") (r "^1") (d #t) (k 2)) (d (n "simplet2s") (r "^0.2") (d #t) (k 2)))) (h "1qb1695j7b69g330fxwig4wb2fx4p0zw7s05zzwv2jc4hm6d6727")))

(define-public crate-fast2s-0.2.0 (c (n "fast2s") (v "0.2.0") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "fst") (r "^0.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0ndpsn5iddpff442xfc796b6fidnj6nvy68m5kv76lq8g0cyq5nn")))

(define-public crate-fast2s-0.2.1 (c (n "fast2s") (v "0.2.1") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "fst") (r "^0.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)))) (h "09v7dfv4818203dxm40gn00hjm6lj58aychczpa59w23sjkm6vj3")))

(define-public crate-fast2s-0.3.0 (c (n "fast2s") (v "0.3.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 1)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "03qfi9hi3kqxypljw0s88ppyx64bpnzm4c80d5qcd12s9mz2m4kj")))

(define-public crate-fast2s-0.3.1 (c (n "fast2s") (v "0.3.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 1)) (d (n "hashbrown") (r "^0.12.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0zmj4fm181n6j10vvv69wwyq8jzhx9cwk1679jzzgw92ahxhc5hk")))

