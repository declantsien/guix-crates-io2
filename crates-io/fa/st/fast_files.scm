(define-module (crates-io fa st fast_files) #:use-module (crates-io))

(define-public crate-fast_files-0.1.0 (c (n "fast_files") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1ynljx9jh59awyfbgvmm28d493qvf4rfhdkpv81mqv4ncr93m3p3")))

(define-public crate-fast_files-0.1.1 (c (n "fast_files") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0dxwwyg3gxpkr0rr6zqrkb70cs3l431716w0729vx8bvfx1ajvji")))

