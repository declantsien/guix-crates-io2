(define-module (crates-io fa st fastrie) #:use-module (crates-io))

(define-public crate-fastrie-0.0.1 (c (n "fastrie") (v "0.0.1") (h "146x70jj3y274x5cw2j0jrvxqbh5g8qci8zn59f6a3a678xymvf0")))

(define-public crate-fastrie-0.0.2 (c (n "fastrie") (v "0.0.2") (h "00cc0bz2bnrjcgrm09ppgc9mc2vla56mns9qdizjpdmrrziz0nlv")))

(define-public crate-fastrie-0.0.3 (c (n "fastrie") (v "0.0.3") (h "00qhv6by1vsik37f25z6i614fdi38ypx66gfv9pbanjshy50451n")))

(define-public crate-fastrie-0.0.4 (c (n "fastrie") (v "0.0.4") (h "0f5g3hazk7kxg488pzvq83bvv4p27idwvvvrs02bm1h27yjs912c")))

(define-public crate-fastrie-0.0.5 (c (n "fastrie") (v "0.0.5") (h "1c778rpychmyi0har9mawrsrmajd1gw4n77p9g54faw9wp8dmvlm")))

(define-public crate-fastrie-0.0.6 (c (n "fastrie") (v "0.0.6") (h "05jdi1hwjqf0ajl8k2dxi7jlzi3i8qzwvb2yw46i10kn11ryia0n")))

(define-public crate-fastrie-0.1.0 (c (n "fastrie") (v "0.1.0") (h "175d26x59kjmhgz2ai5aayv1qavynd4dkbvsxynrzn45j7zprv7q")))

(define-public crate-fastrie-0.2.0 (c (n "fastrie") (v "0.2.0") (h "1drkr24wmkpsrbjcj62p80l0xz9037k7fspwwkv20iaic95frazz")))

(define-public crate-fastrie-0.2.1 (c (n "fastrie") (v "0.2.1") (h "1gb5bsyy7nrh1qwj0l4iyqj2gxm4y1zgrx3z5gqnqjxk88pr9fff")))

