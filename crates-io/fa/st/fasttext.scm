(define-module (crates-io fa st fasttext) #:use-module (crates-io))

(define-public crate-fasttext-0.1.0 (c (n "fasttext") (v "0.1.0") (d (list (d (n "cfasttext-sys") (r "^0.1") (d #t) (k 0)))) (h "0wds943hg2iws8943n01aj95kx8dc2kycz4i686p45mqbblqk1dm")))

(define-public crate-fasttext-0.1.1 (c (n "fasttext") (v "0.1.1") (d (list (d (n "cfasttext-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1fik50kq3hlmbcym0vad6kqsdnmn643pplixirimrxfcwhpvx2lm")))

(define-public crate-fasttext-0.1.2 (c (n "fasttext") (v "0.1.2") (d (list (d (n "cfasttext-sys") (r "^0.1.2") (d #t) (k 0)))) (h "017d02smqvqv6k0lim2221mhqcn125zhvl7gh5qv9hw7kl2k6s7b")))

(define-public crate-fasttext-0.1.3 (c (n "fasttext") (v "0.1.3") (d (list (d (n "cfasttext-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0vb3ps8lk8zvc43xh205wslbsf220mbw2l0dz6bnpac8nk4rpaal")))

(define-public crate-fasttext-0.1.4 (c (n "fasttext") (v "0.1.4") (d (list (d (n "cfasttext-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1pivly60hlgy1rbspvmz6c5mvblr5qmgs0w7rrw0ci32p04mv2ci")))

(define-public crate-fasttext-0.1.5 (c (n "fasttext") (v "0.1.5") (d (list (d (n "cfasttext-sys") (r "^0.1.5") (d #t) (k 0)))) (h "05mik82ndpg4szgvsx0vf23rkj5l9jm73ynddibv0ifrx33c99bz")))

(define-public crate-fasttext-0.2.0 (c (n "fasttext") (v "0.2.0") (d (list (d (n "cfasttext-sys") (r "^0.2.0") (d #t) (k 0)))) (h "16pi16dciwfn7fy7nzgxvbimwzgsba50rfl3fj5blg0qg9v3812d")))

(define-public crate-fasttext-0.3.0 (c (n "fasttext") (v "0.3.0") (d (list (d (n "cfasttext-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0jwr9nxgkirqjdf2m2rvix43xkkxrmj342dp82x1hry4ndllsm6i")))

(define-public crate-fasttext-0.4.0 (c (n "fasttext") (v "0.4.0") (d (list (d (n "cfasttext-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1d8hjsvi0c4346ac78kqh90y5pdsl6sx176f4qwyym81wkqnplpa")))

(define-public crate-fasttext-0.4.1 (c (n "fasttext") (v "0.4.1") (d (list (d (n "cfasttext-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0p7yqsll66z6lasxfhh4hs09lmzry9x15zmhjq39xcxqbpwhqn1l")))

(define-public crate-fasttext-0.5.0 (c (n "fasttext") (v "0.5.0") (d (list (d (n "cfasttext-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1ql9pnnqnsbhyzximc2mdigz09y4y0msy9pxbjn0ygzp582jampb")))

(define-public crate-fasttext-0.5.1 (c (n "fasttext") (v "0.5.1") (d (list (d (n "cfasttext-sys") (r "^0.5.2") (d #t) (k 0)))) (h "0lr41m9byqfmjfzl1n1x6142ramm3xi7k1wwwwhmqwc9h9x87s9f")))

(define-public crate-fasttext-0.6.0 (c (n "fasttext") (v "0.6.0") (d (list (d (n "cfasttext-sys") (r "^0.6.0") (d #t) (k 0)))) (h "03mm2zrdg6iv0ac73qy3phjs7n0ckinblax0fhqjvc3nyl8drr7w")))

(define-public crate-fasttext-0.7.0 (c (n "fasttext") (v "0.7.0") (d (list (d (n "cfasttext-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1dza481mcsrzpyhs0d2xqp1fja2av6gsv1zqfwbp5nr71q946a3l")))

(define-public crate-fasttext-0.7.1 (c (n "fasttext") (v "0.7.1") (d (list (d (n "cfasttext-sys") (r "^0.7.1") (d #t) (k 0)))) (h "0yzf35gq2hmlrvvj6aw0gikzldd56c9x9qw028g6zml9q4l57j54")))

(define-public crate-fasttext-0.7.2 (c (n "fasttext") (v "0.7.2") (d (list (d (n "cfasttext-sys") (r "^0.7.1") (d #t) (k 0)))) (h "014lmb11a3vbv9m1ppbs72wqaxz0n8laqb7kp7zm7zqmmqn0mk2l")))

(define-public crate-fasttext-0.7.3 (c (n "fasttext") (v "0.7.3") (d (list (d (n "cfasttext-sys") (r "^0.7.3") (d #t) (k 0)))) (h "0znjxdlc7andflbzfq5s2j97ad8kxklzlailj04s0jv2qgjrhhwz")))

(define-public crate-fasttext-0.7.4 (c (n "fasttext") (v "0.7.4") (d (list (d (n "cfasttext-sys") (r "^0.7.4") (d #t) (k 0)))) (h "065q4jqj42fckskyimy5zy3k1h097a6f5qpjr4hzs1z5xg4zp4n2")))

(define-public crate-fasttext-0.7.5 (c (n "fasttext") (v "0.7.5") (d (list (d (n "cfasttext-sys") (r "^0.7.5") (d #t) (k 0)))) (h "1l5wx8xhm2vj86gvv49r2n9nkin2y9imf67qcxb4pk1z4birbp07") (y #t)))

(define-public crate-fasttext-0.7.6 (c (n "fasttext") (v "0.7.6") (d (list (d (n "cfasttext-sys") (r "^0.7.6") (d #t) (k 0)))) (h "1zzmfnr4a3h0hpdic0jdhwkgjnx1xkkqvkw57wgv2ppfls3qizj8")))

(define-public crate-fasttext-0.7.7 (c (n "fasttext") (v "0.7.7") (d (list (d (n "cfasttext-sys") (r "^0.7.7") (d #t) (k 0)))) (h "0sfkg8nvlj8k8a56ry9rcvbc4vag0wwg0n0mbl3vjzwdb5lf3zil")))

(define-public crate-fasttext-0.7.8 (c (n "fasttext") (v "0.7.8") (d (list (d (n "cfasttext-sys") (r "^0.7.8") (d #t) (k 0)))) (h "1j20z728xb56qp88j21ngpykfhk4v896j0pr99cjxcppiybz69px")))

