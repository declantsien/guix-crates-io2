(define-module (crates-io fa st fastx) #:use-module (crates-io))

(define-public crate-fastx-0.1.0 (c (n "fastx") (v "0.1.0") (d (list (d (n "memchr") (r "^2.4.0") (d #t) (k 0)))) (h "1fxpcjh437yg2rk2nh2iw1zaxn285gxmralqk32pbmkill963p2s")))

(define-public crate-fastx-0.1.1 (c (n "fastx") (v "0.1.1") (d (list (d (n "memchr") (r "^2.4.0") (d #t) (k 0)))) (h "00x17kixhz3fgj9n8wzxl38b39aiyxh52zbdxjj3hsfzpzkcz0mg")))

(define-public crate-fastx-0.2.0 (c (n "fastx") (v "0.2.0") (d (list (d (n "memchr") (r "^2.4.0") (d #t) (k 0)))) (h "1x1wlx6a8xm0vf8cv1djgh7hf0p2wn9jj8m8c29d6dr0f8vmn4kl")))

(define-public crate-fastx-0.2.1 (c (n "fastx") (v "0.2.1") (d (list (d (n "memchr") (r "^2.7.1") (d #t) (k 0)))) (h "1vz20v9l3xr8bi6796vrl7i9kvbs17kqax1jspjvz55llc5y6q8n")))

