(define-module (crates-io fa st fast_escape) #:use-module (crates-io))

(define-public crate-fast_escape-0.1.0 (c (n "fast_escape") (v "0.1.0") (d (list (d (n "fast_fmt") (r "^0.1.3") (k 0)) (d (n "void") (r "^1") (d #t) (k 2)))) (h "14qkqjnc6w8364q8sc2dvmwxsvy3kaxmd33nmylbghmcl9dfgzy9") (f (quote (("std" "fast_fmt/with_std") ("default" "std"))))))

