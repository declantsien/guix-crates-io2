(define-module (crates-io fa st fast-paillier) #:use-module (crates-io))

(define-public crate-fast-paillier-0.1.0 (c (n "fast-paillier") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "libpaillier") (r "^0.5") (f (quote ("gmp"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "rand_dev") (r "^0.1") (d #t) (k 2)) (d (n "rug") (r "^1.21") (f (quote ("integer" "rand"))) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rjsxc5rjffj6v4q15hi5imsbh85cz3kvwzn50mwfv9zmfzzz9ld") (s 2) (e (quote (("serde" "dep:serde" "rug/serde"))))))

