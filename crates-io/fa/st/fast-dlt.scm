(define-module (crates-io fa st fast-dlt) #:use-module (crates-io))

(define-public crate-fast-dlt-0.1.0 (c (n "fast-dlt") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1503z8kn6cp2q1iwpiz39knj64r5zs4i07mq0hsx0y2kk6l2s369")))

