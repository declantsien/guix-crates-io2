(define-module (crates-io fa st fastrlp-derive) #:use-module (crates-io))

(define-public crate-fastrlp-derive-0.1.0 (c (n "fastrlp-derive") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1gnmj1vf4prda9c20sjd8kmiwv9y75aanp5pxd5h3a1a4y9zn6f2") (y #t)))

(define-public crate-fastrlp-derive-0.1.1 (c (n "fastrlp-derive") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1hjg1cj2xpyax8y0l7fy8w0j93y53r6inkg25m1499x15w5g56cl") (y #t)))

(define-public crate-fastrlp-derive-0.1.2 (c (n "fastrlp-derive") (v "0.1.2") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0niqw7a20jh393lzzbq2d44wjbhqysjd86mik082ibriqbml3yp1")))

(define-public crate-fastrlp-derive-0.1.3 (c (n "fastrlp-derive") (v "0.1.3") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0wl8s1pxds0whzs2x5v4p9l059xvm8p5d4cidiqpl2lg3n61bsfr")))

(define-public crate-fastrlp-derive-0.1.4 (c (n "fastrlp-derive") (v "0.1.4") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0mckxzrca6s9rmq55hbr6zxkbyp33irxfxg0bkchrpqh6z859r6n")))

(define-public crate-fastrlp-derive-0.1.5 (c (n "fastrlp-derive") (v "0.1.5") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0x904fhrppskl36h1hy5dprpv73mxpz4nm0jihw1ngb2mdsd1yd0")))

(define-public crate-fastrlp-derive-0.2.0 (c (n "fastrlp-derive") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1s4flclgh38mkmmsry0pf96wq4jzx6bhin6wgxaq1ixz488z7jml")))

