(define-module (crates-io fa st fastrange-rs) #:use-module (crates-io))

(define-public crate-fastrange-rs-0.0.1 (c (n "fastrange-rs") (v "0.0.1") (h "0wmm9kwl7hmj33zbcax8j73nxqi5rhnkih8h0245pl92k31v9w34")))

(define-public crate-fastrange-rs-0.0.2 (c (n "fastrange-rs") (v "0.0.2") (h "1rxc0dd1rlj5iw9fm5w9ymvnl2fxdim33c8k2lwgzs67blbyiwjx") (y #t)))

(define-public crate-fastrange-rs-0.0.3 (c (n "fastrange-rs") (v "0.0.3") (h "12c6jki0zr4fbw330rxfpgdadjmcmsfbcim21javdhihp0zakkq2")))

(define-public crate-fastrange-rs-0.0.4 (c (n "fastrange-rs") (v "0.0.4") (h "0ia85gbf1ay2sidz6hci2n9r6w0nlwndw95a07zs55ghv85vvcbw")))

(define-public crate-fastrange-rs-0.1.0 (c (n "fastrange-rs") (v "0.1.0") (h "04i52yrbz61q7y8mrw3pqdafcsxsnzr53wna5kjfpibfrn9162p9")))

