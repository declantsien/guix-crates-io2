(define-module (crates-io fa st fastobo-derive-internal) #:use-module (crates-io))

(define-public crate-fastobo-derive-internal-0.4.0 (c (n "fastobo-derive-internal") (v "0.4.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (d #t) (k 0)))) (h "1pnjmrgppy106xmldvcyr2m6q3f40siqhyn5gh8a7axnvxd6hqsg")))

(define-public crate-fastobo-derive-internal-0.4.1 (c (n "fastobo-derive-internal") (v "0.4.1") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (d #t) (k 0)))) (h "17p6j2wx2m205vnjanw8xa3gjqfx128ymrngm4349ykv4rqb1py0")))

(define-public crate-fastobo-derive-internal-0.4.2 (c (n "fastobo-derive-internal") (v "0.4.2") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (d #t) (k 0)))) (h "1kk8l6fgpkjgacspgpvl9zxzcsn8g85aaaqqs7rlcl1ag6ql9ksx")))

(define-public crate-fastobo-derive-internal-0.4.3 (c (n "fastobo-derive-internal") (v "0.4.3") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (d #t) (k 0)))) (h "1lm6w6lvby6chdd85rgfw1kfljj32729mxby1pmxnn5b7xj9y0y5")))

(define-public crate-fastobo-derive-internal-0.4.4 (c (n "fastobo-derive-internal") (v "0.4.4") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (d #t) (k 0)))) (h "0rw3zv2igs42ad49zmg9hvp25n2n60nx9388nb818g03l9wf4vsm")))

(define-public crate-fastobo-derive-internal-0.5.0 (c (n "fastobo-derive-internal") (v "0.5.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (d #t) (k 0)))) (h "01nanf2nsv8bc2fd71s7ys3pf4l1iqagk5y79rig1x52dhd02kk1")))

(define-public crate-fastobo-derive-internal-0.6.0 (c (n "fastobo-derive-internal") (v "0.6.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (d #t) (k 0)))) (h "1ch5alxs2hfn291yamx8lb2pg9ipgw8mnpfq6w6a7ihp8v8dan2l")))

(define-public crate-fastobo-derive-internal-0.6.1 (c (n "fastobo-derive-internal") (v "0.6.1") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (d #t) (k 0)))) (h "1dz64g1frfigrfsdkx3awlkwrybrh0j998p827dmddi2bkm9bbmh")))

(define-public crate-fastobo-derive-internal-0.7.0 (c (n "fastobo-derive-internal") (v "0.7.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (d #t) (k 0)))) (h "0xw9y7c7ws6gnylsnlr4w99zx90ldq2j9qk122c7hp4d5gsy0zqx")))

(define-public crate-fastobo-derive-internal-0.7.1 (c (n "fastobo-derive-internal") (v "0.7.1") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (d #t) (k 0)))) (h "1nmi4qrfpj6hnxgc8qpql7kvac1i2ikr0hrfd522b196my0dxn9l")))

(define-public crate-fastobo-derive-internal-0.7.2 (c (n "fastobo-derive-internal") (v "0.7.2") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19p87lpbj5cb91z1lldidrs8pp4nqkhqpwvyjqh9plk77wjswqci")))

(define-public crate-fastobo-derive-internal-0.7.3 (c (n "fastobo-derive-internal") (v "0.7.3") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0n2mmsczpsw5k36a0h9ls1w1xwd227za1yqvymvdcb9z5p7kds60")))

(define-public crate-fastobo-derive-internal-0.7.4 (c (n "fastobo-derive-internal") (v "0.7.4") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l8zsiirhw3d212cmnpxzm8yf5xygaw3akx0563sbh2p1kqmlaf8")))

(define-public crate-fastobo-derive-internal-0.7.5 (c (n "fastobo-derive-internal") (v "0.7.5") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07g8yiqq8g06dkljm6bf5567zj7pij9cfmcf36hi0qaincw9vh51")))

(define-public crate-fastobo-derive-internal-0.8.0 (c (n "fastobo-derive-internal") (v "0.8.0") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "004hd5fc714pj8biwzvmisv5wzpn8lwzn80fs70z91npjg6xwm88")))

(define-public crate-fastobo-derive-internal-0.8.1 (c (n "fastobo-derive-internal") (v "0.8.1") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03s8dlpfzwwzmwifbw4y1dkxcmhg54ahwrsxwp216vgh91qhfp45")))

(define-public crate-fastobo-derive-internal-0.8.2 (c (n "fastobo-derive-internal") (v "0.8.2") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hykf8h3cvm12kb4145rxggaz1qj4jmyz8jsmacs0jfjap88bygv")))

(define-public crate-fastobo-derive-internal-0.8.3 (c (n "fastobo-derive-internal") (v "0.8.3") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dw37h1zhmfvyvgn6cnlqd5g1ay02272049c4y43440nz1bk0lir")))

(define-public crate-fastobo-derive-internal-0.8.4 (c (n "fastobo-derive-internal") (v "0.8.4") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i015ghkkvmv6c9sj8s4d5mdljnx4w99mkfbnbzpsrwbxpdvqx0c")))

(define-public crate-fastobo-derive-internal-0.9.0 (c (n "fastobo-derive-internal") (v "0.9.0") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nwk06d8h7451f335ac1dpdhgp7is5bndc963f5h87vs7qji1q9c")))

(define-public crate-fastobo-derive-internal-0.10.0 (c (n "fastobo-derive-internal") (v "0.10.0") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f29v1yx40954rz8fhv99qvnpz7h249mmqd9g2sgznx2hfz9s1lb")))

(define-public crate-fastobo-derive-internal-0.11.0 (c (n "fastobo-derive-internal") (v "0.11.0") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k6j1g3ykb49k8v5vn9xdhq3d1avk09fx6a3q9rp0rf0i0fhn4ch")))

(define-public crate-fastobo-derive-internal-0.11.1 (c (n "fastobo-derive-internal") (v "0.11.1") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qxablarjp7cnp4mlsfnnfmrcp92s3a5cczb0qgaahn8w85mmzhz")))

(define-public crate-fastobo-derive-internal-0.11.2 (c (n "fastobo-derive-internal") (v "0.11.2") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16gng3b29giwf01v8jlzmm1kqjg3l66c1wwl12lncpjg9yd8pn7a")))

(define-public crate-fastobo-derive-internal-0.12.0 (c (n "fastobo-derive-internal") (v "0.12.0") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m7k73mvh68i3fkafshdx9hnm5gh39zn574lbg48yrx2xmv1xiy7")))

(define-public crate-fastobo-derive-internal-0.13.0 (c (n "fastobo-derive-internal") (v "0.13.0") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0va0hlq1g93kj2bqbjz7164sk3x0dhp5n0rkk1dyldamp99zwbzy")))

(define-public crate-fastobo-derive-internal-0.13.1 (c (n "fastobo-derive-internal") (v "0.13.1") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lda4k830snrckgjvv0c9jlvv17p5slp234znhr9abk6miig52z6")))

(define-public crate-fastobo-derive-internal-0.13.2 (c (n "fastobo-derive-internal") (v "0.13.2") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1md1pp02rwxvx058xxjzwx63hgpx5bpjw11p161mckda93waqv90")))

(define-public crate-fastobo-derive-internal-0.14.0 (c (n "fastobo-derive-internal") (v "0.14.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gk6q3vhwbcdydf1ydkbp7pgkiya0vsqsynysrlcqgl7mx19i1av")))

(define-public crate-fastobo-derive-internal-0.14.1 (c (n "fastobo-derive-internal") (v "0.14.1") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bsak6ljpw9b02yd7x5s6l1ss4fbiklslkxq7wymb3cvwqqr3izb")))

(define-public crate-fastobo-derive-internal-0.14.2 (c (n "fastobo-derive-internal") (v "0.14.2") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fzf0jqsly7wnikidj6brbgzjg374lisjpw14w28mhjpjfix5nkm")))

(define-public crate-fastobo-derive-internal-0.15.0 (c (n "fastobo-derive-internal") (v "0.15.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13p1w0vv1wd3xxc9ci5ckcbndcsfnrishxwaij5swv66k9bhgf5c")))

(define-public crate-fastobo-derive-internal-0.15.1 (c (n "fastobo-derive-internal") (v "0.15.1") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0di2j18387c120sffh0pv3nlay7b3wnhxqzg8lw466k3rdr4py67")))

(define-public crate-fastobo-derive-internal-0.15.2 (c (n "fastobo-derive-internal") (v "0.15.2") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17m61raf1293fnryp0mxnykiwqk60mkm7yvpjjf7v27m5j7i06wh")))

