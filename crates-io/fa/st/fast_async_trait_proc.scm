(define-module (crates-io fa st fast_async_trait_proc) #:use-module (crates-io))

(define-public crate-fast_async_trait_proc-0.1.0 (c (n "fast_async_trait_proc") (v "0.1.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bp8iv61xz7d39k8ijjsmr873zwxv8j734valhh6mnp1hg9d51bc")))

(define-public crate-fast_async_trait_proc-0.1.1 (c (n "fast_async_trait_proc") (v "0.1.1") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0awi0p1x1wll77dc31fpzjkdah4zkj04q7kz8307divpnv49qgk4")))

