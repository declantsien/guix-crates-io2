(define-module (crates-io fa st fast-food) #:use-module (crates-io))

(define-public crate-fast-food-0.1.0 (c (n "fast-food") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("string"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)))) (h "12z7n7fxs33xvcanzj4plpqxsvgjphjpwq0sj9cwprxy709592p3")))

(define-public crate-fast-food-0.2.0 (c (n "fast-food") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("string"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)))) (h "0s0xp0l989h3hvdwcczvmgf6fg5a7v9xw2hp84jl7wbnmwmhxbki")))

(define-public crate-fast-food-0.3.0 (c (n "fast-food") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("string"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)))) (h "17fan0m8vzvcfa7ia4a475hrcifjp9dbwx2cdxl3dbh506hsjwyw")))

