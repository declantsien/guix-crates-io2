(define-module (crates-io fa st fastsnbt) #:use-module (crates-io))

(define-public crate-fastsnbt-0.1.0 (c (n "fastsnbt") (v "0.1.0") (d (list (d (n "cesu8") (r "^1.1") (d #t) (k 0)) (d (n "fastnbt") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0lm3wm2ia9ssp1vlwcs9cxbwdms5cg4z2ccls72gf6kc8s87hx8n")))

(define-public crate-fastsnbt-0.2.0 (c (n "fastsnbt") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "fastnbt") (r "^2") (d #t) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1v585abqk7sddckbz2p9v0gyjj6h5cngwgdcdpml2m1afidqpd2i")))

