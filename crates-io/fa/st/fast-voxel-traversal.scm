(define-module (crates-io fa st fast-voxel-traversal) #:use-module (crates-io))

(define-public crate-fast-voxel-traversal-0.4.0 (c (n "fast-voxel-traversal") (v "0.4.0") (d (list (d (n "glam") (r "^0.18") (d #t) (k 0)))) (h "1071j7xg162ai2gdyjzll9xfjx0iskfhzwyavmgis52wj6bzmi9w")))

(define-public crate-fast-voxel-traversal-0.5.0 (c (n "fast-voxel-traversal") (v "0.5.0") (d (list (d (n "glam") (r "^0.18") (d #t) (k 0)))) (h "0l4k5fkp5hlbv9wjgs7wgxs2ziscf32fynd12mqxcxiiglrg20r0")))

