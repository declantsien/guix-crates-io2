(define-module (crates-io fa st fastpasta_toml_macro) #:use-module (crates-io))

(define-public crate-fastpasta_toml_macro-0.1.0 (c (n "fastpasta_toml_macro") (v "0.1.0") (d (list (d (n "fastpasta_toml_macro_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1z9ngniskb0wkanlllrhwwf5bfr0mbiaxckkha1l63ynq5ar8caa")))

(define-public crate-fastpasta_toml_macro-0.1.1 (c (n "fastpasta_toml_macro") (v "0.1.1") (d (list (d (n "fastpasta_toml_macro_derive") (r "^0.1.0") (d #t) (k 0)))) (h "039v9m7fwsyrzhzmwir7m77zkzpq450s3g4b71vga026gmvg3xks")))

