(define-module (crates-io fa st fast_enum_conversion) #:use-module (crates-io))

(define-public crate-fast_enum_conversion-0.1.0 (c (n "fast_enum_conversion") (v "0.1.0") (d (list (d (n "addr_of_enum") (r "^0.1.4") (d #t) (k 0)) (d (n "fast_enum_conversion_macro") (r "^0.1.0") (d #t) (k 0)))) (h "00y238wfw5r6s0lfjj7w11jdwppy6lgmz6a4qnp5lpp0ypcfjm9w") (y #t)))

