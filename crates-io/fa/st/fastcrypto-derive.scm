(define-module (crates-io fa st fastcrypto-derive) #:use-module (crates-io))

(define-public crate-fastcrypto-derive-0.1.0 (c (n "fastcrypto-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "14hz6sc2bpbka4m1za59mzzpah2zrd4f9sbwahkzn9ks961ia2h3")))

(define-public crate-fastcrypto-derive-0.1.2 (c (n "fastcrypto-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m3h6ycks5zn3hrb06ywvmyxqkacqvc0h1yxbxdd1kksw9yphfn0")))

(define-public crate-fastcrypto-derive-0.1.3 (c (n "fastcrypto-derive") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)))) (h "14gqz3skbj3m80v2sxc4adnkyxg21p6kc7g1hnw6qhbz2pr2l32c")))

