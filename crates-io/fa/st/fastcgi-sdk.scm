(define-module (crates-io fa st fastcgi-sdk) #:use-module (crates-io))

(define-public crate-fastcgi-sdk-0.0.1 (c (n "fastcgi-sdk") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "0whxcy16imv9avs7c9h01zdwkb469pjx710h2y86cgp9m8m9bji0")))

(define-public crate-fastcgi-sdk-0.0.2 (c (n "fastcgi-sdk") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "0q1nzzw75p79d7iq7srjfjjms6k7a2z3vxwcwzdinfafgkcd60k9")))

(define-public crate-fastcgi-sdk-0.0.3 (c (n "fastcgi-sdk") (v "0.0.3") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "0isf1k4lqxkmcchhhzny4l50s303iflar4cp0v8dlcayf6pzfnad")))

(define-public crate-fastcgi-sdk-0.0.4 (c (n "fastcgi-sdk") (v "0.0.4") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "0lrnlr3s0927lqggq3pc4i59x7w9gfpc9d8izhy7by0qas6fgm9x")))

(define-public crate-fastcgi-sdk-0.0.5 (c (n "fastcgi-sdk") (v "0.0.5") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "1af83k191bf5dkxkdyqliagzrflqkgd4zkdr2vwm419z6nzv2dp6")))

