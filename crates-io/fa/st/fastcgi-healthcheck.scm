(define-module (crates-io fa st fastcgi-healthcheck) #:use-module (crates-io))

(define-public crate-fastcgi-healthcheck-0.2.0 (c (n "fastcgi-healthcheck") (v "0.2.0") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "hyper") (r "^1.2.0") (f (quote ("server" "http1"))) (d #t) (k 0)) (d (n "json_env_logger") (r "^0.1.1") (f (quote ("iso-timestamps" "chrono"))) (d #t) (k 0)) (d (n "kvarn-fastcgi-client") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("kv_std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "io-util" "macros" "sync" "net" "rt-multi-thread"))) (d #t) (k 0)))) (h "0imbgaci216wq2fhbzl18aa7dbs1kd4r8kdavckwjaq9ajxi9paj")))

