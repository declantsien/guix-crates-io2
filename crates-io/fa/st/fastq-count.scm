(define-module (crates-io fa st fastq-count) #:use-module (crates-io))

(define-public crate-fastq-count-0.1.0 (c (n "fastq-count") (v "0.1.0") (d (list (d (n "bio") (r "^0.25") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03lxgq274wk42xxygjwvvm4afnxxq6zd0lh5ka8ncwm0s87ms05m")))

