(define-module (crates-io fa st fast-rustc-ap-rustc_lexer) #:use-module (crates-io))

(define-public crate-fast-rustc-ap-rustc_lexer-1.0.0 (c (n "fast-rustc-ap-rustc_lexer") (v "1.0.0") (d (list (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1pa2wwiwxsscxgqzhhcyp97fdhhxhpvhknf99g6akl62ng10304h")))

