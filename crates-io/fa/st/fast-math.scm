(define-module (crates-io fa st fast-math) #:use-module (crates-io))

(define-public crate-fast-math-0.1.0 (c (n "fast-math") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1hxhrg3l5pyvdm6n09v4prp4g026m1hh68bps2pizr6l1r5i14gj") (f (quote (("unstable"))))))

(define-public crate-fast-math-0.1.1 (c (n "fast-math") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "ieee754") (r "^0.2.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0rldjkc2wcqwp9ik9czdmq7wz0xc2v3b3qqg6l8j1z6d8qhjjr94") (f (quote (("unstable"))))))

