(define-module (crates-io fa st fastsim-proc-macros) #:use-module (crates-io))

(define-public crate-fastsim-proc-macros-0.1.0 (c (n "fastsim-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "0pcfjyjg8b733nf6lm5axvl4pxxh8vdp6cf41rwgl1j1h64s90xk")))

(define-public crate-fastsim-proc-macros-0.1.1 (c (n "fastsim-proc-macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "028ngzy42fzn8cl76c82cwlvsxlcwslciyd50h10b9y0y87l7qg4") (y #t)))

(define-public crate-fastsim-proc-macros-0.1.2 (c (n "fastsim-proc-macros") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "0izxfc9lligs2as7c23rry1vdmwcfjfr1lx86905ppi2ql8qqfwx")))

(define-public crate-fastsim-proc-macros-0.1.3 (c (n "fastsim-proc-macros") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "18dib7nl93pq952x90gkmdbl1jymxkqcdwbycqvx07ys23j472w9")))

(define-public crate-fastsim-proc-macros-0.1.4 (c (n "fastsim-proc-macros") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "0imq78iyh5xazxngccs95nw0ndlrcfzr4nk5n21qi2xz34qnwhm0")))

(define-public crate-fastsim-proc-macros-0.1.5 (c (n "fastsim-proc-macros") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "0m7d835i4lvl470152l8pini348pspkv2j91r8p02283xmac6c38")))

(define-public crate-fastsim-proc-macros-0.1.6 (c (n "fastsim-proc-macros") (v "0.1.6") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "0kyb5bmcnpxf0r9qpz125barqxdb5h47zf7ykqfy9k2hzw39md3q")))

