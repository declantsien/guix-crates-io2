(define-module (crates-io fa st fastdivide) #:use-module (crates-io))

(define-public crate-fastdivide-0.1.0 (c (n "fastdivide") (v "0.1.0") (h "068nfmmhp0hfh6ria9j3s8iprpi6f82c22a4b1k4ksqyylf7bkay")))

(define-public crate-fastdivide-0.2.0 (c (n "fastdivide") (v "0.2.0") (h "1b7gqvf7iialba3qlk67gj2sz5j0k02hzw5iz6jwls23zzvcna10")))

(define-public crate-fastdivide-0.3.0 (c (n "fastdivide") (v "0.3.0") (h "0g34ba9a19722bqza2648hny829v13jj5bc6158441pr7kas56aa")))

(define-public crate-fastdivide-0.4.0 (c (n "fastdivide") (v "0.4.0") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "012vydqbj8fhhdlaqnxd19abqjs1xm3iacmkf26ylraxjh4xzir5") (f (quote (("std"))))))

(define-public crate-fastdivide-0.4.1 (c (n "fastdivide") (v "0.4.1") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "02xhiqs0jvympj5w0wwgfvn78rzmk9i93hsqidmihp2yqm0qjrjr") (f (quote (("std"))))))

