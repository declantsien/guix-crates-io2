(define-module (crates-io fa st faster-chars-count) #:use-module (crates-io))

(define-public crate-faster-chars-count-0.1.3 (c (n "faster-chars-count") (v "0.1.3") (h "1c2c1w3pha2b0n43frraz5nn8qj078qrv0yj1sa300i5izs0l5f2")))

(define-public crate-faster-chars-count-0.1.4 (c (n "faster-chars-count") (v "0.1.4") (h "1r0zx4b508ssr2b1ycfv2vsmk92kmp7h28wh38i76pdlic9p5shs")))

(define-public crate-faster-chars-count-0.1.5 (c (n "faster-chars-count") (v "0.1.5") (h "1zf31hkwl43zvjmbnfrwn3ci2wcqdypl486immbbri57z0m7xn9p")))

(define-public crate-faster-chars-count-0.1.7 (c (n "faster-chars-count") (v "0.1.7") (h "1l5dxvr4ii5p9dvcr1sqqizwckrgjgsbr57mpk9kdwzn25anr4x7")))

(define-public crate-faster-chars-count-0.2.1 (c (n "faster-chars-count") (v "0.2.1") (h "1jgs2yx794mdz2dw8c6lj7jhyhdmkmy87d9ndg4x6vwx4mqimhf1") (f (quote (("std") ("runtime_detect" "std") ("default" "runtime_detect"))))))

(define-public crate-faster-chars-count-0.3.0 (c (n "faster-chars-count") (v "0.3.0") (h "1f2nmazndksivyyh5qf46ssdx96d58hay3kkijcw2616aj2qrsak") (f (quote (("std") ("runtime_detect" "std") ("default" "runtime_detect")))) (r "1.56")))

