(define-module (crates-io fa st fastly-shared) #:use-module (crates-io))

(define-public crate-fastly-shared-0.1.0-alpha1 (c (n "fastly-shared") (v "0.1.0-alpha1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0qaavzlwkv2nb6aavw244gkvz08p5np77aira5s1p2xvka2drn53") (y #t)))

(define-public crate-fastly-shared-0.2.0-alpha1 (c (n "fastly-shared") (v "0.2.0-alpha1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0zm29jh73zk1kifl9xxgjxn79p4yc06srrfffm57dcv9bq0c659n") (y #t)))

(define-public crate-fastly-shared-0.2.0-alpha2 (c (n "fastly-shared") (v "0.2.0-alpha2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "1db85vv8xyqapc0cdb49q2kw50v755fs74c5xbzb66wf726z7r38")))

(define-public crate-fastly-shared-0.2.0-alpha3 (c (n "fastly-shared") (v "0.2.0-alpha3") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0qgv99qr5h9w0z5r050a1byfiwsy1m1sm72c0kmxh9ak7nsszwni")))

(define-public crate-fastly-shared-0.2.0-alpha4 (c (n "fastly-shared") (v "0.2.0-alpha4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)))) (h "0jdqna96zg9pica4qc5x2i9865ryjs86ml6p3f3qr0nv6kd6g8pz")))

(define-public crate-fastly-shared-0.3.0 (c (n "fastly-shared") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)))) (h "0cbywgsh10k34cb1dh8zmwpiwm62lbhcvghj6yvm4csiv3amjfbj")))

(define-public crate-fastly-shared-0.3.1 (c (n "fastly-shared") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)))) (h "12rw0wk1z0471ncdp29yy4mc6ysk53s775nwz25nj6kx5wqmy2ha")))

(define-public crate-fastly-shared-0.3.2 (c (n "fastly-shared") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)))) (h "1mgqhd2l8ys12vlqfy8q1f7vpsp6iqqj146f0g7q64w534xxqyh5")))

(define-public crate-fastly-shared-0.4.0 (c (n "fastly-shared") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1057gwn791dwxahv29b7l0l91jmh3ffvddq2wv3g75ymz84khfsk")))

(define-public crate-fastly-shared-0.5.0 (c (n "fastly-shared") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1wh66pqacrnjb7yb56pw4vmi07b1ns0v5r06p97abq522fmqj1ac")))

(define-public crate-fastly-shared-0.6.0-beta1 (c (n "fastly-shared") (v "0.6.0-beta1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "09crfkv4bxc32j7yhx4xyppkaskmriy66dr03k6216vyb11qk7ch")))

(define-public crate-fastly-shared-0.6.0-beta2 (c (n "fastly-shared") (v "0.6.0-beta2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0zzlgxxs0rvz4mkwb2im64fgd72r1c26bpw7ssxsrmjdiiz6v90s")))

(define-public crate-fastly-shared-0.6.0-beta3 (c (n "fastly-shared") (v "0.6.0-beta3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "09vhqv5hxmwvx6xvvgdjbzswydn3609wfrwahzfqfx3ixzmm75nj")))

(define-public crate-fastly-shared-0.6.0 (c (n "fastly-shared") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1s3s935zbfbyvfx73ylc54axqjzqib1sg0c2afip5qzaqdxnn7m3")))

(define-public crate-fastly-shared-0.5.1 (c (n "fastly-shared") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "new-fastly-shared") (r "^0.6.0") (d #t) (k 0) (p "fastly-shared")) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "121f41dl07kn2v50n15mk4j8hsvqilx1v61yi207gy0lg31kn3ra")))

(define-public crate-fastly-shared-0.6.1 (c (n "fastly-shared") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1lkcr07jx4838f50a1m0sckk2nvcc01w5bgyvf54v3wg1njrfx63")))

(define-public crate-fastly-shared-0.6.2 (c (n "fastly-shared") (v "0.6.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1821kp2xpmjab9404v7l9y95hkmsaf0sngivprhw5dr0bpnbai01")))

(define-public crate-fastly-shared-0.6.3 (c (n "fastly-shared") (v "0.6.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0s77rjp92d98k3a7rycm9k4z2ibikjxs8cks8nvksc2snmc0y7qd")))

(define-public crate-fastly-shared-0.8.0 (c (n "fastly-shared") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0ih3s6jyw6x4cvrxvfs2ldpvjq2r4q0y5wz0y963s3nh17s6c0h1")))

(define-public crate-fastly-shared-0.8.1 (c (n "fastly-shared") (v "0.8.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "09rpv1i3l6akzm1caxl99rv0diq95ncgjdlfs7ffhyc7cy6aw3x5")))

(define-public crate-fastly-shared-0.8.2 (c (n "fastly-shared") (v "0.8.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "150572x0dpb57dxqyshpvayzry0lz9g5gfmf1yaq8p16d7dck4gs")))

(define-public crate-fastly-shared-0.8.3 (c (n "fastly-shared") (v "0.8.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "19jglnvvsgdjxi4b2b028md9k0jmkdsz6wc0k6wy3yv0zwbmhm50")))

(define-public crate-fastly-shared-0.8.4 (c (n "fastly-shared") (v "0.8.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "18h3dxqbh36v8zivz7i1zm409fbiylnjy249sbvr266580hx7rg5")))

(define-public crate-fastly-shared-0.8.5 (c (n "fastly-shared") (v "0.8.5") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0gvsmh3y5dqkpd45a0ycrzjkzkfy2jcxzjcmrwcqxhi7b5h5zr9f")))

(define-public crate-fastly-shared-0.8.6 (c (n "fastly-shared") (v "0.8.6") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0cm9l1bfsbhz74d91xq7xkr48jf26xjk5aadp91flm9ccq4gg85r")))

(define-public crate-fastly-shared-0.8.7 (c (n "fastly-shared") (v "0.8.7") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1yd5wlfg3gks5hn60jxy7p5rdzfz3a304cv74jq1qf8r4apvg3r3")))

(define-public crate-fastly-shared-0.8.8 (c (n "fastly-shared") (v "0.8.8") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "11s77xkb3z8ny5f9wajy7mk1v87qmplay7cm6lz6wsygdy1pbabq")))

(define-public crate-fastly-shared-0.8.9 (c (n "fastly-shared") (v "0.8.9") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0n5iy8ssvy87b9kh05384f34yi8rccw4yn4xfpgb3f5svfdr6ybg")))

(define-public crate-fastly-shared-0.9.0-pre1 (c (n "fastly-shared") (v "0.9.0-pre1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "12mhdr502di7qm0ic2kzzx56lxpgc7ap80br7ywfl05igbydghkz")))

(define-public crate-fastly-shared-0.9.0 (c (n "fastly-shared") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0mb63gy39q7x9h8cqbiymh8m2v3hr0j9qwn2cv8s3cnpn0l8pbwk")))

(define-public crate-fastly-shared-0.9.1 (c (n "fastly-shared") (v "0.9.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0bd2lhqp25w0nf8f1d61qw74q5nxzpz1cbcpyc17pl8n70x5pna0")))

(define-public crate-fastly-shared-0.9.2 (c (n "fastly-shared") (v "0.9.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "02rk3hryi87vn7zhnfk7bapra7ic209zps9p8rdm2sx7sizv8rzz")))

(define-public crate-fastly-shared-0.9.3 (c (n "fastly-shared") (v "0.9.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1d2vfmfhkdp329ppddhz7ll5qash6vnzg2lr1iqxvkv4dnjqyi4a")))

(define-public crate-fastly-shared-0.9.4 (c (n "fastly-shared") (v "0.9.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0ggg2xmbs52xbbnxnk1ak113swzjpx0php0r3ld8qgy67fswgaal")))

(define-public crate-fastly-shared-0.9.5 (c (n "fastly-shared") (v "0.9.5") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "09313hzipcyghcj3qls4br0apm5mh73xlrg03vn695j42zq7v37a")))

(define-public crate-fastly-shared-0.9.6 (c (n "fastly-shared") (v "0.9.6") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1rlgx2da4hg1hnagzd4mq2qmqgxy4rysqg8cf3y66md6jfsb7an0") (y #t)))

(define-public crate-fastly-shared-0.9.7 (c (n "fastly-shared") (v "0.9.7") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1dplmwsxm5cr7rflarf651ms02x4v5dvm5vrs60i9vg92gs0wr5d")))

(define-public crate-fastly-shared-0.9.8 (c (n "fastly-shared") (v "0.9.8") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1fb28framxh4k9gkz3vbzcy76iwqfqkpmdwssb4ajarpskm8813p")))

(define-public crate-fastly-shared-0.9.9 (c (n "fastly-shared") (v "0.9.9") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "08qqyivx08cri0z1vlzsliajl9xq0axrd6qjf6plf7ah7ycfr80x")))

(define-public crate-fastly-shared-0.9.10 (c (n "fastly-shared") (v "0.9.10") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0hmg5hb7b8kn3gd8f28zg6kp4sh45b3yrrkp45z8f41h1xw11p80")))

(define-public crate-fastly-shared-0.9.11 (c (n "fastly-shared") (v "0.9.11") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)))) (h "1d222ysgwjzz1g15hkr3hmqpr070y3g5zgcj7618a57avgqiadfn")))

(define-public crate-fastly-shared-0.9.12 (c (n "fastly-shared") (v "0.9.12") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)))) (h "0qc7wsbi00rs4q12sxhk2lp4a4gysynz6vzbllll1cziysn14s17")))

(define-public crate-fastly-shared-0.10.0 (c (n "fastly-shared") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)))) (h "154ir3b6hg64j1s3qs8daqfns463lwpxjnz378k77f7y2dwjf7qm")))

(define-public crate-fastly-shared-0.10.1 (c (n "fastly-shared") (v "0.10.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)))) (h "1xbp4gnbg7fnhcfchzppn18gza0r2pghyh91csk40b15g9y2vcv8")))

