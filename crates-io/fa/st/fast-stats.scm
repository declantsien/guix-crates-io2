(define-module (crates-io fa st fast-stats) #:use-module (crates-io))

(define-public crate-fast-stats-0.1.0 (c (n "fast-stats") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "00brns9p0ddh09n6b38iv3silhbslkgd4a6vi8nizmry965zdzi7")))

(define-public crate-fast-stats-0.1.1 (c (n "fast-stats") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "140md576xclmyqhfa19vy1sw6ysn53gh5p8smc38gzfamym5qps7")))

