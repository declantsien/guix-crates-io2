(define-module (crates-io fa st faster-fishers) #:use-module (crates-io))

(define-public crate-faster-fishers-0.1.1 (c (n "faster-fishers") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "numpy") (r "^0.16.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.6") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)))) (h "1mjwldg8f0spjb1398z61q6rp3akk57h3f0hvadvj8dshd889pk6")))

(define-public crate-faster-fishers-0.1.2 (c (n "faster-fishers") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "numpy") (r "^0.17.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0mklgakq12sqipclmjijik21xfnbdm9i9avgxlxw6hf9phh1270x")))

(define-public crate-faster-fishers-0.1.3 (c (n "faster-fishers") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "numpy") (r "^0.17.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1lj15irpn39rjhlym3yjcpqh2bxn9hjg3xma7lzk13p672xbah3f")))

