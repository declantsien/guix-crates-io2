(define-module (crates-io fa st fast32) #:use-module (crates-io))

(define-public crate-fast32-1.0.0 (c (n "fast32") (v "1.0.0") (d (list (d (n "uuid") (r "^1.6.1") (o #t) (k 0)))) (h "0az97m1w2pgn3zwc9zvjs38r6nfnglcymb364xba00hh9szycdw4") (f (quote (("default")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

(define-public crate-fast32-1.0.1 (c (n "fast32") (v "1.0.1") (d (list (d (n "uuid") (r "^1.6.1") (o #t) (k 0)))) (h "0jn5kck9pbf5ipnkmq101sglkswwny6l2kgnbfwqxb7jcwraasf8") (f (quote (("default")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

(define-public crate-fast32-1.0.2 (c (n "fast32") (v "1.0.2") (d (list (d (n "uuid") (r "^1.6.1") (o #t) (k 0)))) (h "0k4r4gbiv11bhq0gil7zwiqvshwv7x4sq8yg0cjakran4gdrpsi7") (f (quote (("default")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

