(define-module (crates-io fa st fast-fibonacci) #:use-module (crates-io))

(define-public crate-fast-fibonacci-0.1.0 (c (n "fast-fibonacci") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)))) (h "0igkd6shgki6s3dk1y9rlpx3zb5nq1piw8y0fsrw7win85krjm92")))

(define-public crate-fast-fibonacci-0.1.1 (c (n "fast-fibonacci") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)))) (h "03yf21qp8qv2j5nhx97lp5yv8g4z7y12r2992ak2iya62xplfb43")))

(define-public crate-fast-fibonacci-0.1.2 (c (n "fast-fibonacci") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)))) (h "1sdsry9gklmc9c7qij7caw3x3v11mp3vi1g31cl2f62l8y78hkss")))

(define-public crate-fast-fibonacci-0.2.0 (c (n "fast-fibonacci") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)))) (h "0fk9xfazi68cc2v2lxw04bw2akfachh90zrz6ya9vcbprjxc4avr")))

