(define-module (crates-io fa st fast-lru) #:use-module (crates-io))

(define-public crate-fast-lru-0.1.0 (c (n "fast-lru") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)))) (h "1vsb25ccxrgv6x9g4p793r9rd158ihwj8pcmzp4biclslr6in837")))

(define-public crate-fast-lru-0.1.1 (c (n "fast-lru") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)))) (h "14fkynwzf33vfj6zirb3wxlp0a9hjbm5hv044bp2k2m77gk4q2cq")))

(define-public crate-fast-lru-0.1.2 (c (n "fast-lru") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)))) (h "05xxdmi5iws55sc485ir5031hhpmb7q1223781g17ks1fmbm9sdj")))

