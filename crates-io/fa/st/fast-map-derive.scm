(define-module (crates-io fa st fast-map-derive) #:use-module (crates-io))

(define-public crate-fast-map-derive-0.1.0 (c (n "fast-map-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qb6p6whas427mwr4gpm4bmvhgydqwcrh8s2vysyx61a40vrjj83")))

(define-public crate-fast-map-derive-0.1.1 (c (n "fast-map-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k7chrnidc1mbnvy3rkf9zv46rlrhivk9zxj342mic2mv9v8bmv8")))

(define-public crate-fast-map-derive-0.2.0 (c (n "fast-map-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0l44fb1760w7msihs5rvdp65yhxx1llqvih530zbbqz6n9lc4y49")))

(define-public crate-fast-map-derive-0.2.1 (c (n "fast-map-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19mbd1ddpjwasnn5mzzhzwwj3b033n2gxf7ianpxhf8p6z2znjr0")))

