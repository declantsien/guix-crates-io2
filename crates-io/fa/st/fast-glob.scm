(define-module (crates-io fa st fast-glob) #:use-module (crates-io))

(define-public crate-fast-glob-0.1.0 (c (n "fast-glob") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "globset") (r "^0.4.14") (d #t) (k 2)))) (h "0lz5b35gikvd5hyn3wabykrzxad5sadgx8pl68cyvl4m265gl1rd")))

