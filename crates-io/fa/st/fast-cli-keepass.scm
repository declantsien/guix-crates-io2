(define-module (crates-io fa st fast-cli-keepass) #:use-module (crates-io))

(define-public crate-fast-cli-keepass-0.1.0 (c (n "fast-cli-keepass") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "keepass") (r "^0.7.7") (d #t) (k 0)) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)) (d (n "rust-fuzzy-search") (r "^0.1.1") (d #t) (k 0)))) (h "1fxjczj7i3758p54fp3gldy38pg062p9qxgsralnvv5zjpbqck2p")))

