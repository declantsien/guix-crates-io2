(define-module (crates-io fa st fast-float) #:use-module (crates-io))

(define-public crate-fast-float-0.1.0 (c (n "fast-float") (v "0.1.0") (d (list (d (n "hexf-parse") (r "^0.1") (d #t) (k 2)) (d (n "lexical-core") (r "^0.7") (d #t) (k 2)))) (h "11qrjxjzvdb5cjlng9kkpl7x3blnk65nwjh7spcwv7wm1cic40cp") (f (quote (("std") ("default" "std"))))))

(define-public crate-fast-float-0.2.0 (c (n "fast-float") (v "0.2.0") (d (list (d (n "fastrand") (r "^1.4") (d #t) (k 2)) (d (n "hexf-parse") (r "^0.1") (d #t) (k 2)) (d (n "lexical-core") (r "^0.7") (d #t) (k 2)) (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "ryu") (r "^1.0") (d #t) (k 2)))) (h "0g7kfll3xyh99kc7r352lhljnwvgayxxa6saifb6725inikmyxlm") (f (quote (("std") ("default" "std"))))))

