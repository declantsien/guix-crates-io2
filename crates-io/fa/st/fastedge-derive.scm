(define-module (crates-io fa st fastedge-derive) #:use-module (crates-io))

(define-public crate-fastedge-derive-0.1.5 (c (n "fastedge-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10qipqfsdgyafzcbazqlij4l0f48ys7i8l2hh9qiif78glhylqgj") (f (quote (("default"))))))

(define-public crate-fastedge-derive-0.1.6 (c (n "fastedge-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z9i3nay8j3vrywq99l7vfbsqnvy65n1ckd8z8bxn2inavwjh8xg") (f (quote (("default"))))))

