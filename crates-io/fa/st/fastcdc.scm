(define-module (crates-io fa st fastcdc) #:use-module (crates-io))

(define-public crate-fastcdc-0.1.0 (c (n "fastcdc") (v "0.1.0") (h "1ybn49ni6raa8xvv9plq1vkxzvp5w901y4zwk028abixp3s2gg85")))

(define-public crate-fastcdc-1.0.0 (c (n "fastcdc") (v "1.0.0") (h "0150vjkfxc2l0p9nyjxg5yhmadyzdcfqfnw7xn10mq4h92k18sir")))

(define-public crate-fastcdc-1.0.1 (c (n "fastcdc") (v "1.0.1") (d (list (d (n "aes-ctr") (r "^0.3.0") (d #t) (k 1)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 1)))) (h "1wqq3g3s9p1kph3q0rnjis5104nv4dwnhfqqld3xhyrqj8mhcxid")))

(define-public crate-fastcdc-1.0.2 (c (n "fastcdc") (v "1.0.2") (d (list (d (n "aes-ctr") (r "^0.3.0") (d #t) (k 1)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 1)))) (h "031szahpsx25nnwkqfb5dvcqh56ynv61hnk3i6f5qk7hpv7vl43f")))

(define-public crate-fastcdc-1.0.3 (c (n "fastcdc") (v "1.0.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 2)))) (h "1ickz2l01k2v7mvh2blaklnlbbwf72qq39wkz2av89wm3s7b7aia")))

(define-public crate-fastcdc-1.0.4 (c (n "fastcdc") (v "1.0.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 2)))) (h "1wyk0kwsgrwbhz5n9nb1m8hfrzpbayvzn0rqq99a4f7vymn9qxaw")))

(define-public crate-fastcdc-1.0.5 (c (n "fastcdc") (v "1.0.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 2)))) (h "0grkg7dz82xbixv0mswkv9jjcz673a6yyzcr1cw8qb5i8sz2kyjs")))

(define-public crate-fastcdc-1.0.6 (c (n "fastcdc") (v "1.0.6") (d (list (d (n "clap") (r "^3.0.5") (d #t) (k 2)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 2)))) (h "1jv19bfd605jz2piz0mnlyvn7qm2632abb5l01iy1wzv4ws7n7c6")))

(define-public crate-fastcdc-1.0.7 (c (n "fastcdc") (v "1.0.7") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 2)))) (h "1i143maw7v3fhld2vmzy0rswb7hy66w8095kkifxbwarwi1vx3y7")))

(define-public crate-fastcdc-1.0.8 (c (n "fastcdc") (v "1.0.8") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 2)))) (h "1vsv24mdf60ma798y04d8m59xkiy82hfnbd8jxd728md61i5kwgk")))

(define-public crate-fastcdc-2.0.0 (c (n "fastcdc") (v "2.0.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 2)))) (h "1cbjm24yq0rpykb0lp4s5w4hvixj0k3m10w95sncd62h784ax4w0")))

(define-public crate-fastcdc-3.0.0 (c (n "fastcdc") (v "3.0.0") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)) (d (n "md-5") (r "^0.10.5") (d #t) (k 2)) (d (n "memmap2") (r "^0.5.8") (d #t) (k 2)))) (h "1qi398l32355b9kh0qr57rin86cv2z8kga25h1yis1wab9cjcxy4")))

(define-public crate-fastcdc-3.0.1 (c (n "fastcdc") (v "3.0.1") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)) (d (n "md-5") (r "^0.10.5") (d #t) (k 2)) (d (n "memmap2") (r "^0.5.8") (d #t) (k 2)))) (h "0l265h3q2l96571gj785kyxmkxjpi442xc0zdb981klzkbfrgw4q")))

(define-public crate-fastcdc-3.0.2 (c (n "fastcdc") (v "3.0.2") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)) (d (n "md-5") (r "^0.10.5") (d #t) (k 2)) (d (n "memmap2") (r "^0.5.8") (d #t) (k 2)))) (h "0p8b0i6nwxwsxz3sviiwp2f756dqzmg6qjds0bqbsyvyd5myxx1y")))

(define-public crate-fastcdc-3.0.3 (c (n "fastcdc") (v "3.0.3") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 2)) (d (n "clap") (r "^4.2.1") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)) (d (n "md-5") (r "^0.10.5") (d #t) (k 2)) (d (n "memmap2") (r "^0.5.8") (d #t) (k 2)))) (h "0ykqz1wrzhspn41af7kfbklgqha2ry2m7csw8kdcy6k05sdhy08h")))

(define-public crate-fastcdc-3.1.0 (c (n "fastcdc") (v "3.1.0") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 2)) (d (n "async-stream") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 2)) (d (n "clap") (r "^4.2.1") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "ctr") (r "^0.9.2") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-test") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.10.5") (d #t) (k 2)) (d (n "memmap2") (r "^0.5.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "rt" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1wi82qd58j3ysf8m2dhb092qga6rj1wwbppgsajabadzjz862457") (f (quote (("default")))) (s 2) (e (quote (("tokio" "dep:tokio" "tokio-stream" "async-stream") ("futures" "dep:futures"))))))

