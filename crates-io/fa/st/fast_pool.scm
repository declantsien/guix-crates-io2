(define-module (crates-io fa st fast_pool) #:use-module (crates-io))

(define-public crate-fast_pool-0.1.0 (c (n "fast_pool") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0hy94w0kmy507gi147dv7z68jav860fh0jyfpvy6i5d7k50a0sjz") (y #t)))

(define-public crate-fast_pool-0.1.1 (c (n "fast_pool") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "17sjp0bl1bbk6bsbxxr2ywilz25mlg1s7ddhkmksl2jw8yr8gzbm") (y #t)))

(define-public crate-fast_pool-0.1.2 (c (n "fast_pool") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "crossfire") (r "^1.0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0r7n5qcx9zwy5g4k9wafvlmzix5av47wiw734x96s821vqaradn7") (y #t)))

(define-public crate-fast_pool-0.1.3 (c (n "fast_pool") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (f (quote ("async"))) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0h43vs92829q48p0hhvfci24vbigi404grq6w73k493fl39lg37r") (y #t)))

(define-public crate-fast_pool-0.1.4 (c (n "fast_pool") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (f (quote ("async"))) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0da5r0vghyaczmlj2sr3n4ysav14x4y7a1x9qsxfcwkxn6433rcf") (y #t)))

(define-public crate-fast_pool-0.1.5 (c (n "fast_pool") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (f (quote ("async"))) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0fxvg6x8s67ia1z34avalpvmrypkyrqb5a3f8jvz1p5ir99kszdh") (y #t)))

(define-public crate-fast_pool-0.1.6 (c (n "fast_pool") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (f (quote ("async"))) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1nm98b7pnl7qi8ghs5qq5vs6nmhrlil5syvb555r6yk4vq8x2khi") (y #t)))

(define-public crate-fast_pool-0.1.7 (c (n "fast_pool") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (f (quote ("async"))) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0zrzm3yf5r8114bs3klsw1i4cgpjacwyqs09fay7asmzdm0j7hmz") (y #t)))

(define-public crate-fast_pool-0.1.8 (c (n "fast_pool") (v "0.1.8") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (f (quote ("async"))) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "02wirlxb2jq6zwzl5dbyqx8j0vlj1a7whr5y346j9a5ybfqpfv3i")))

(define-public crate-fast_pool-0.1.9 (c (n "fast_pool") (v "0.1.9") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (f (quote ("async"))) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0hvpkjprl27a2mav2268xwi775av49ss0fqpx70kprnl913cipkc")))

(define-public crate-fast_pool-0.2.0 (c (n "fast_pool") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (f (quote ("async"))) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1s6xj18j5yclnjkfqb8kzxmibsf84ks3l02pvk3c5apqawky7y0q")))

