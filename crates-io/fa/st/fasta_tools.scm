(define-module (crates-io fa st fasta_tools) #:use-module (crates-io))

(define-public crate-fasta_tools-0.1.1 (c (n "fasta_tools") (v "0.1.1") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "io_utils") (r "^0.2.4") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "03q1r7pgzmp6w3gz54w6djdahfr31d8awbyzbq80dsiy8wx7ic5p")))

(define-public crate-fasta_tools-0.1.2 (c (n "fasta_tools") (v "0.1.2") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "0hpb0jjhz2ck0h9py3bw020mcccxi6dsp3fhhzdgd7lygy3kllwm")))

(define-public crate-fasta_tools-0.1.3 (c (n "fasta_tools") (v "0.1.3") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "0w0qsh1zbq1zlwr9y13mdb1l1m6ss62g2dzibfl0kmpzm0n7698i")))

(define-public crate-fasta_tools-0.1.4 (c (n "fasta_tools") (v "0.1.4") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "1bpc43ikc96apm9jphwmhd5925dcznfpiyirpyga1fmd0n9zs1s6")))

(define-public crate-fasta_tools-0.1.5 (c (n "fasta_tools") (v "0.1.5") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "0h3g4bwj7zg6diccrrx4shy0asaifpacrihqzl1pznzf8vrml4b5")))

(define-public crate-fasta_tools-0.1.6 (c (n "fasta_tools") (v "0.1.6") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "io_utils") (r "^0.3") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "1zzlz5zr6nch9mw8kr7yslbxb9ykzg29pk2hx95iwn24gi3wsx4v")))

(define-public crate-fasta_tools-0.1.7 (c (n "fasta_tools") (v "0.1.7") (d (list (d (n "debruijn") (r "^0.3") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "io_utils") (r "^0.3") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "104nwf7wpg3ylxl8f3rbdg5nr9bwjh701xx0lfph7a0df1h8p5j7")))

(define-public crate-fasta_tools-0.1.8 (c (n "fasta_tools") (v "0.1.8") (d (list (d (n "debruijn") (r "^0.3") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "io_utils") (r "^0.3") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "19yyrrr65ik4ha2caq9r08jw5vjvnv3nc01sgjaka514ncyzpbb8")))

