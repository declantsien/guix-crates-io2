(define-module (crates-io fa st fast_paths) #:use-module (crates-io))

(define-public crate-fast_paths-0.1.0 (c (n "fast_paths") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "priority-queue") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 0)))) (h "0b7wf2jih9n95vqw356yg2p8pwdyhrlzf33xqdak6ib8v6ql1w8y")))

(define-public crate-fast_paths-0.1.1 (c (n "fast_paths") (v "0.1.1") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "priority-queue") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 0)))) (h "038j32ihlnxn5dyf987pp4bxb86mpfi0zrxp148043c1pzqzqa26")))

(define-public crate-fast_paths-0.2.0 (c (n "fast_paths") (v "0.2.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "priority-queue") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 2)))) (h "1v4qf75644qy71sx2n2y6s6hpfi61jp94l6wgmdl21z0sp3652ja")))

(define-public crate-fast_paths-1.0.0 (c (n "fast_paths") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "priority-queue") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 2)))) (h "1nn8m7p5jjzp1pwsjpxmn4g2wb46rhr4lik260qdhqjp6igja768")))

