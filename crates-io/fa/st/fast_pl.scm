(define-module (crates-io fa st fast_pl) #:use-module (crates-io))

(define-public crate-fast_pl-1.0.0 (c (n "fast_pl") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "float-ord") (r "^0.3.2") (d #t) (k 0)) (d (n "geo") (r "^0.23.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.8") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "02j298np847dmwqsds1106m6kb6262vwm5rkkvz1lsnv2xj2ac0v") (s 2) (e (quote (("plot" "dep:plotters"))))))

