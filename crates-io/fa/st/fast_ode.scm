(define-module (crates-io fa st fast_ode) #:use-module (crates-io))

(define-public crate-fast_ode-0.1.0 (c (n "fast_ode") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "float_next_after") (r "^0.1.5") (d #t) (k 0)) (d (n "mathru") (r "^0.9.1") (d #t) (k 2)))) (h "0ls6r7xmjiaf37xhgj016hby7lj0dzzjsnyd2brnxj04gq98qnvj") (f (quote (("dormand_prince_logging"))))))

(define-public crate-fast_ode-0.1.1 (c (n "fast_ode") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "float_next_after") (r "^0.1.5") (d #t) (k 0)) (d (n "mathru") (r "^0.9.1") (d #t) (k 2)))) (h "1vrrwjgamqg2i57ngvm1n850azzkam94bxkqic5clq0kg50ih2cr") (f (quote (("dormand_prince_logging"))))))

(define-public crate-fast_ode-0.1.2 (c (n "fast_ode") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "float_next_after") (r "^0.1.5") (d #t) (k 0)) (d (n "mathru") (r "^0.9.1") (d #t) (k 2)))) (h "0igp3dqs1sxz4mpskhsq0bl7dy2mjsf05zwz3gihmip49n240lnv") (f (quote (("dormand_prince_logging"))))))

(define-public crate-fast_ode-0.1.3 (c (n "fast_ode") (v "0.1.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "float_next_after") (r "^0.1.5") (d #t) (k 0)) (d (n "mathru") (r "^0.9.1") (d #t) (k 2)))) (h "10kmndv0hghsm0q65k0iw6sl09r1inkcmrm4rgy2dsmafb4aj7kw") (f (quote (("dormand_prince_logging"))))))

(define-public crate-fast_ode-1.0.0 (c (n "fast_ode") (v "1.0.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "float_next_after") (r "^1.0.0") (d #t) (k 0)) (d (n "mathru") (r "^0.15.0") (d #t) (k 2)))) (h "0cv0aikafpyqxgm4j3j14x5ngy7azzwncf1n8v9n68bcsffgpywr") (f (quote (("dormand_prince_logging"))))))

