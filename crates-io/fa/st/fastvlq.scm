(define-module (crates-io fa st fastvlq) #:use-module (crates-io))

(define-public crate-fastvlq-1.0.0-alpha.1 (c (n "fastvlq") (v "1.0.0-alpha.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0yc17n8x2nibj80v0j4ff784lp0y0cxrphjsabz49jcywqnccv5w") (f (quote (("std") ("default" "std"))))))

(define-public crate-fastvlq-1.0.0-alpha.2 (c (n "fastvlq") (v "1.0.0-alpha.2") (d (list (d (n "bare-io") (r "^0.2") (o #t) (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0i2gwwjb5hwx55pn6xnv91d67zdbdmgqmr3gz37a3nq7y76qwpz5") (f (quote (("std-nightly" "std" "bare-io/std-nightly") ("std" "bare-io/std") ("nightly" "bare-io/nightly") ("default" "std"))))))

(define-public crate-fastvlq-1.0.0 (c (n "fastvlq") (v "1.0.0") (d (list (d (n "core2") (r "^0.3.0-alpha.1") (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0m1rr551b9nymccak6kld4jxs2ir8q0db78ggrndvkv8nrzr6zby") (f (quote (("std" "core2/std") ("default" "std"))))))

(define-public crate-fastvlq-1.1.0 (c (n "fastvlq") (v "1.1.0") (d (list (d (n "core2") (r "^0.3.0-alpha.1") (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1yj3haj2igc15zfkjsc4h0nip39hvqfr8m1q9krjajkwcsilhd2v") (f (quote (("std" "core2/std") ("default" "std"))))))

(define-public crate-fastvlq-1.1.1 (c (n "fastvlq") (v "1.1.1") (d (list (d (n "core2") (r "^0.3") (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1bwyacix4ks4dfc00cpag386b1zx38ir9hj6pfhimcbd0q14h9li") (f (quote (("std" "core2/std") ("default" "std"))))))

