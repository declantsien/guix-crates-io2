(define-module (crates-io fa st faststd) #:use-module (crates-io))

(define-public crate-faststd-0.1.0 (c (n "faststd") (v "0.1.0") (h "0idxrn2q4dj7d872yf3qs9q88qwaf2c4r452f13wj1y9c1nlc1qx")))

(define-public crate-faststd-0.1.1 (c (n "faststd") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1wzjv8ds7wbwq7qi8mlj1787w4cp7hr2jw9bb4a5r4aqak1bjlkn") (f (quote (("rand_nobias"))))))

(define-public crate-faststd-0.1.2 (c (n "faststd") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1srz496k5rvbygzqhklaj1kx8dz5w8gixga8xf5mf334yns54347") (f (quote (("rand_nobias"))))))

(define-public crate-faststd-0.1.3 (c (n "faststd") (v "0.1.3") (h "0wh9hp06gyx0hcqji8dxprlw80x6lhx6dxw2r80zxv8qibp6qg3b")))

