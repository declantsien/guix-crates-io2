(define-module (crates-io fa st fast_input) #:use-module (crates-io))

(define-public crate-fast_input-0.1.0 (c (n "fast_input") (v "0.1.0") (h "0ni2x281j3xyglcizg6s6kj9k5niqnpcllbqhi2pgbax1xcl20hv")))

(define-public crate-fast_input-0.1.1 (c (n "fast_input") (v "0.1.1") (h "1g93msnzqd5shlaldawc8qhh593w5zcz6vbp5n0ifwdnxvxzxnik")))

(define-public crate-fast_input-0.1.2 (c (n "fast_input") (v "0.1.2") (h "099ga5g7k5lvbdb52ynw6gj97mf44dqxr51qz0zrck8xzsrvmw6q")))

(define-public crate-fast_input-0.1.3 (c (n "fast_input") (v "0.1.3") (h "1v550gv09fhg13x5wz3wip8kc1i70xlk1waridw7jbfpbygs7lvn")))

(define-public crate-fast_input-0.1.4 (c (n "fast_input") (v "0.1.4") (h "0289jnh4gdb01zbxq7j24hf6dipynkvpfl1b1lqc67zjxlhnm67l")))

(define-public crate-fast_input-0.1.5 (c (n "fast_input") (v "0.1.5") (h "1zl32mcg8l0m3g94dnjznb4wg1hhi362v1f4v2xcglq4zlp1gfln")))

(define-public crate-fast_input-0.1.6 (c (n "fast_input") (v "0.1.6") (h "0qdvqp8786kxiv5w5jv0fy5vg64v8vx3r8rfmd8w7vvsdvg1p2py")))

(define-public crate-fast_input-0.2.0 (c (n "fast_input") (v "0.2.0") (h "01qrh8bnzask0jcgkqq7x9c24k7m0z3y67id776hakqlmfj98l4q")))

