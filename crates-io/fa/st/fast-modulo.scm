(define-module (crates-io fa st fast-modulo) #:use-module (crates-io))

(define-public crate-fast-modulo-0.1.0 (c (n "fast-modulo") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1gif8yj0iibz9i2phd3rkw4s4nwglg4cclzwg43haqln1y163lgd")))

(define-public crate-fast-modulo-0.1.1 (c (n "fast-modulo") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0g06ccxnw858sliypj9pfkj5sxvfmjn9r7v92x3y4ym876fr6rxh")))

(define-public crate-fast-modulo-0.1.2 (c (n "fast-modulo") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0q2by5ypy3fq65yqpj2d1f8gf42x9pgirp4yhswd9dia6ayc4cqw")))

(define-public crate-fast-modulo-0.1.3 (c (n "fast-modulo") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "10gy7q2q516ifkrmvias2zig610ndra806b11115crx9rqa0mkqk") (y #t)))

(define-public crate-fast-modulo-0.2.0 (c (n "fast-modulo") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1z9r1b0lhb2ga6wjwl2293nnxrr09s8v611z3df8xaxn82fq7dg5")))

(define-public crate-fast-modulo-0.3.0 (c (n "fast-modulo") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0nh4ix589wfkd8l61y141l9p5layvqb1wp2dgir144x3x8hkc6j1")))

(define-public crate-fast-modulo-0.4.0 (c (n "fast-modulo") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1nbim37hgpz7ka9wkf00llwgkkanzq5zhi76rv3p6f29zj16xzm3")))

