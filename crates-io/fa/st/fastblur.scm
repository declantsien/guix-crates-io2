(define-module (crates-io fa st fastblur) #:use-module (crates-io))

(define-public crate-fastblur-0.1.0 (c (n "fastblur") (v "0.1.0") (d (list (d (n "image") (r "^0.15.0") (d #t) (k 2)) (d (n "x11-dl") (r "^2.15.0") (d #t) (k 2)))) (h "1xvd9d6254q6yl9b3mi28d55c8hwfzpmb3jmfwlbq30f81nyh84v")))

(define-public crate-fastblur-0.1.1 (c (n "fastblur") (v "0.1.1") (d (list (d (n "image") (r "^0.15.0") (d #t) (k 2)) (d (n "x11-dl") (r "^2.15.0") (d #t) (k 2)))) (h "0bfadnla9alvayzs91c9mx3hhyb4cdf63zn8ih9h6qlk2v0vp0w3")))

