(define-module (crates-io fa st fast-async-mutex) #:use-module (crates-io))

(define-public crate-fast-async-mutex-0.2.0 (c (n "fast-async-mutex") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0jc3pghhgai7nwxpnd3a063n4pj4vdn2lg4awr8wjpjlc1g4d03i")))

(define-public crate-fast-async-mutex-0.3.0 (c (n "fast-async-mutex") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0n58wz0njnh4nihcdya22nig3fa3b81wmjk8fk34z7f8x28ww1cs")))

(define-public crate-fast-async-mutex-0.3.1 (c (n "fast-async-mutex") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "18mzlc17q6lcn20zb16wpx1lm4b7wmkmbwg8bgzxjkvhai1c6w65")))

(define-public crate-fast-async-mutex-0.3.2 (c (n "fast-async-mutex") (v "0.3.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "1jnq3p68m60mmbciw11pvc16wr9nw17j5y72ygn30xmjl9l3hv3i")))

(define-public crate-fast-async-mutex-0.3.3 (c (n "fast-async-mutex") (v "0.3.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0c3wc8lybx10clij6yk5qv1s3ylp4kiqb8wllbaln5xpf9pmls45")))

(define-public crate-fast-async-mutex-0.3.4 (c (n "fast-async-mutex") (v "0.3.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "19nvr355vwhqgsln8pdk5h7j640yjqgh8kbpzg7564gkbp3bi00j")))

(define-public crate-fast-async-mutex-0.3.5 (c (n "fast-async-mutex") (v "0.3.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0dr3xfb4vb0kjv5mlnz3hlwnfk35764flxax853zd69mvkfa0r33")))

(define-public crate-fast-async-mutex-0.3.6 (c (n "fast-async-mutex") (v "0.3.6") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0l83z98iw9ijzkk1frg7pglx519d8zpi7w1mvqziy4q40rfqcr3w")))

(define-public crate-fast-async-mutex-0.3.7 (c (n "fast-async-mutex") (v "0.3.7") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0pym0sq365rqlsbf9c3lnhzf0p9rk98i9kqbacnmi18hn4rmdidn")))

(define-public crate-fast-async-mutex-0.3.8 (c (n "fast-async-mutex") (v "0.3.8") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "15lp2i2sa58pyjbls5c6nmr6nqpyj26dg2va63dkmlyg3078pjg4")))

(define-public crate-fast-async-mutex-0.4.0 (c (n "fast-async-mutex") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "15zfkymnhpimas9cnnvrm65yl4dggsi5nvz1736dk2c97fkffhzx")))

(define-public crate-fast-async-mutex-0.4.1 (c (n "fast-async-mutex") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "15l63m2cyi6jy190hk9q0nw0q1n3z082zy5hmdvrj1ca1mshxp20")))

(define-public crate-fast-async-mutex-0.4.2 (c (n "fast-async-mutex") (v "0.4.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "17v27li3988l326k7mcs85286rcm77vfp15ic872hcjdzgbnvcr1")))

(define-public crate-fast-async-mutex-0.4.3 (c (n "fast-async-mutex") (v "0.4.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "19nqw7av0604724hz0p9lpzv7l8kw45qmqr397hf3dfc5j2dkmy0")))

(define-public crate-fast-async-mutex-0.5.0 (c (n "fast-async-mutex") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "09kdmdz10s8yv0k5790g3ggb00b8qnk3565yy3csb8z0jc0p3986")))

(define-public crate-fast-async-mutex-0.5.1 (c (n "fast-async-mutex") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "1ir4vjlk0w24rp93kq52s366ddgbjjk7phv66ph3pkvp6j0d7xf2")))

(define-public crate-fast-async-mutex-0.5.2 (c (n "fast-async-mutex") (v "0.5.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "1156ig4jwbrxnscx648dcqg4mygd8fglc7kca1j8kcn669wzxdmd")))

(define-public crate-fast-async-mutex-0.5.3 (c (n "fast-async-mutex") (v "0.5.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "1a3k091c4306f21r2jy470w69w63xi44s1kzmdhdlzhgnka06f0g")))

(define-public crate-fast-async-mutex-0.5.4 (c (n "fast-async-mutex") (v "0.5.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "1lcd1lwamdzv35c5vqz2w12z7s92hl3yl9h1rf8yqda86wgiy0jg")))

(define-public crate-fast-async-mutex-0.5.5 (c (n "fast-async-mutex") (v "0.5.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0424qvd7dfdgwwswsvyjy8y9d9w5piklx8ipv6ms4n66vqpzm06p")))

(define-public crate-fast-async-mutex-0.5.6 (c (n "fast-async-mutex") (v "0.5.6") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0xx0ywf9kyxfm2i9s9phlmlf87d3lmcc5ifpx8a63xg4mi3f1l8w")))

(define-public crate-fast-async-mutex-0.6.0 (c (n "fast-async-mutex") (v "0.6.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0shwvf3i2y56n1zby3lg0kkxrhqy6y0bb039969khk95gjdis126")))

(define-public crate-fast-async-mutex-0.6.1 (c (n "fast-async-mutex") (v "0.6.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0a24ry4ck4rwwl2sz0a62j41cd4jpm3pdb07lag6kg52jy8v0sa1")))

(define-public crate-fast-async-mutex-0.6.2 (c (n "fast-async-mutex") (v "0.6.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0xmmh5wbvpp8pq7j5671idap4lsy4vzj4ibchp3k95k3al0spiqd")))

(define-public crate-fast-async-mutex-0.6.3 (c (n "fast-async-mutex") (v "0.6.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "1c3y76zk57jy2hqskb6fsmkz17v4446s9s2y7g609340a6bim86s")))

(define-public crate-fast-async-mutex-0.6.4 (c (n "fast-async-mutex") (v "0.6.4") (d (list (d (n "futures") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "tokio") (r ">=0.3.0, <0.4.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1njpb54qrf0jjy1qbw6kzf1qb6ljgdq6zfgh7cvp5ddylhy8hqbh")))

(define-public crate-fast-async-mutex-0.6.5 (c (n "fast-async-mutex") (v "0.6.5") (d (list (d (n "futures") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "tokio") (r ">=0.3.0, <0.4.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1j3hfmz4wri10gm65y9j2r363shphmslpg9zlwfly6kv7pbzw6bk")))

(define-public crate-fast-async-mutex-0.6.6 (c (n "fast-async-mutex") (v "0.6.6") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)))) (h "0rwfiwiz4w5ig1ajq1kkhs7kx67xys82nrmi0nbmfhr386jxijrj")))

(define-public crate-fast-async-mutex-0.6.7 (c (n "fast-async-mutex") (v "0.6.7") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)))) (h "19idgqiykz8kp5kmvxr5khgii671dj160ydb7f124a9x5vzwwxxq")))

