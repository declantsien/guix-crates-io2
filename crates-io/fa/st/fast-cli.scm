(define-module (crates-io fa st fast-cli) #:use-module (crates-io))

(define-public crate-fast-cli-0.0.0 (c (n "fast-cli") (v "0.0.0") (d (list (d (n "fast-cli-core") (r "^0.0.0") (d #t) (k 0)))) (h "0jcd9a4fhf3bwhi9xpk0himzj1jdn5vmsiblbf5gc39np1b1sgys")))

(define-public crate-fast-cli-0.1.5 (c (n "fast-cli") (v "0.1.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fast-cli-core") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1ci99pdm7xg32z523h1k9b5layi7i6mkrs27pnc261wixi4rawll")))

