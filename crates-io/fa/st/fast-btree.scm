(define-module (crates-io fa st fast-btree) #:use-module (crates-io))

(define-public crate-fast-btree-0.1.0 (c (n "fast-btree") (v "0.1.0") (d (list (d (n "bitmask-enum") (r "^2.2.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "00c8cr69rq2hd09h7wpq2pz4fgzpcygcz8pkmiwk3sqr4dj6s20r")))

