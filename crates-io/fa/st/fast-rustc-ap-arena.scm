(define-module (crates-io fa st fast-rustc-ap-arena) #:use-module (crates-io))

(define-public crate-fast-rustc-ap-arena-1.0.0 (c (n "fast-rustc-ap-arena") (v "1.0.0") (d (list (d (n "rustc_data_structures") (r "^1.0.0") (d #t) (k 0) (p "fast-rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0vdmv5dwg5ydr8bvgmpkhc9rz95qk4a3i5m9l5yjvwjx9bj51z0l")))

