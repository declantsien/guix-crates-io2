(define-module (crates-io fa st fast_text) #:use-module (crates-io))

(define-public crate-fast_text-0.1.0 (c (n "fast_text") (v "0.1.0") (d (list (d (n "cute") (r "^0.3.0") (d #t) (k 0)) (d (n "kolmogorov_smirnov") (r "^1.1.0") (d #t) (k 0)))) (h "1h5jsgkil08r91hlf8h4c0b4pwfwwam2sxi335ii0zyppj9l0y88")))

(define-public crate-fast_text-0.0.9 (c (n "fast_text") (v "0.0.9") (d (list (d (n "cute") (r "^0.3.0") (d #t) (k 0)) (d (n "kolmogorov_smirnov") (r "^1.1.0") (d #t) (k 0)))) (h "094jaqjryri7s74xmj0h2nqxa244b4nw2j45365nhb1hdvsz1nbn")))

(define-public crate-fast_text-0.1.1 (c (n "fast_text") (v "0.1.1") (d (list (d (n "cute") (r "^0.3.0") (d #t) (k 0)) (d (n "kolmogorov_smirnov") (r "^1.1.0") (d #t) (k 0)))) (h "0wjkhxp647jf7n029l04l69p4hjsnq03dbgx75kj7fdq0cszf7v4")))

(define-public crate-fast_text-0.1.2 (c (n "fast_text") (v "0.1.2") (d (list (d (n "cute") (r "^0.3.0") (d #t) (k 0)) (d (n "kolmogorov_smirnov") (r "^1.1.0") (d #t) (k 0)))) (h "1ynvfdi5anqqyfhapa08kq7kdiy8s6l9bicv07yr8qyjhfhd2dl9")))

