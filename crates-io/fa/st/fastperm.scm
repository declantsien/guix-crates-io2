(define-module (crates-io fa st fastperm) #:use-module (crates-io))

(define-public crate-fastperm-1.0.0 (c (n "fastperm") (v "1.0.0") (h "1lrgshi0p6288yvgskdwvv5q84bfjrq3zk9plwxlkja6sp4jp28c")))

(define-public crate-fastperm-1.0.1 (c (n "fastperm") (v "1.0.1") (h "0qy7v4d008cdl4xfqmspiwf8f27lwvjdyyhmpd7nnmpm7lm0mr94")))

(define-public crate-fastperm-1.0.2 (c (n "fastperm") (v "1.0.2") (h "1g2wjkxzvsa4kljm3pp3ysd9z24ppmg4dhv7r08r94zl3gwqrhha")))

