(define-module (crates-io fa st faster-rs) #:use-module (crates-io))

(define-public crate-faster-rs-0.2.0 (c (n "faster-rs") (v "0.2.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "01w38bm5lvg0zbckch7vp45b2jqzamklr8bl8v6z7fhwk6i1pfxw")))

(define-public crate-faster-rs-0.3.0 (c (n "faster-rs") (v "0.3.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0vh2n0pkl9zn16pp1asqy7arankjy8wqiy3mbqpd0qyhlnpi25m9")))

(define-public crate-faster-rs-0.4.0 (c (n "faster-rs") (v "0.4.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0f398zhszx30qwb0f98aljjpf6dhmc4njzbh8rid17by2sdnn5n3")))

(define-public crate-faster-rs-0.5.0 (c (n "faster-rs") (v "0.5.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1s3vis9lf1y9a5d66lkkrz7zxp6xi8w27wffkzlsmlaxfj7b9wj2")))

(define-public crate-faster-rs-0.5.1 (c (n "faster-rs") (v "0.5.1") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1dz865wz898rcpm4r0v9y18wq7qgbb8zwa85y9gf32vbwn2i8d6j")))

(define-public crate-faster-rs-0.5.2 (c (n "faster-rs") (v "0.5.2") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1hsjhrard510cd0p98dlzrk86j0bd9k44q1r8zffrvc6j0ls01pq")))

(define-public crate-faster-rs-0.5.3 (c (n "faster-rs") (v "0.5.3") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1spg8y45n6nlwdxm5cjp7fkrl35j21prqa2wbknkwvwjzh23zhgp")))

(define-public crate-faster-rs-0.6.0 (c (n "faster-rs") (v "0.6.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "12cyl5wxz6p2k0dri696ix1am3jq78g279s9b55qrq0bzg0zkydr")))

(define-public crate-faster-rs-0.7.0 (c (n "faster-rs") (v "0.7.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "16hrlfd45invlm9djjvpfrjajpn474fsjdmzrca94zcbgps4mniw")))

(define-public crate-faster-rs-0.7.1 (c (n "faster-rs") (v "0.7.1") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1c7w0jxwybaj3p33zsjmskwp3zac295ircgdnh3lrqg0h8hsy4fr")))

(define-public crate-faster-rs-0.8.0 (c (n "faster-rs") (v "0.8.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "18dsy2w0izliar70fdw5qwaymxpv3bw72i2934bi3wzznmw4fj01")))

(define-public crate-faster-rs-0.8.1 (c (n "faster-rs") (v "0.8.1") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0j8s8jzy408y0f85614hnp5kqah53xgm629sqb0720lii9afpcz3")))

(define-public crate-faster-rs-0.8.2 (c (n "faster-rs") (v "0.8.2") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0l1dbww3n3nn6q944ma0jj5i0i1bk5w3nzsgkj97d9mbm31119vl")))

(define-public crate-faster-rs-0.8.3 (c (n "faster-rs") (v "0.8.3") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1hr887bz41x7lvh4f90hx2jn6mxgj32wbp190i2g3v38b96yk9ii")))

(define-public crate-faster-rs-0.8.4 (c (n "faster-rs") (v "0.8.4") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0nd6m97cprcmdypipygpkn5m9yb95rwqd21g577yz9xzsr51wapw") (y #t)))

(define-public crate-faster-rs-0.8.5 (c (n "faster-rs") (v "0.8.5") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "03ymkgv695hybplaj1bzqrly6qigfq6k5j3sv9fwp41ldpwk6g6z")))

(define-public crate-faster-rs-0.8.6 (c (n "faster-rs") (v "0.8.6") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "04zxp4a4n7siscx9jibxp4ywakf6rlw1nx97frfww38bqwy93q3j")))

(define-public crate-faster-rs-0.9.0 (c (n "faster-rs") (v "0.9.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "17ajd04zva51q8wixacgzhr0v07j7ns0bpjd21a93828dxxymbzi")))

(define-public crate-faster-rs-0.10.0 (c (n "faster-rs") (v "0.10.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1cgawgm0511m8jzxz58k6jg2g6wpp852bw4f2ggfppk96j81qhc8")))

(define-public crate-faster-rs-0.11.0 (c (n "faster-rs") (v "0.11.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfaster-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "16v7cbpykb3f06jg0ij7wpzn30m37z9nqricacm0hr12pldkx44h")))

