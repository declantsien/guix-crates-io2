(define-module (crates-io fa st fastorm-trait) #:use-module (crates-io))

(define-public crate-fastorm-trait-0.1.0 (c (n "fastorm-trait") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("runtime-tokio" "sqlite" "mysql" "macros" "any"))) (d #t) (k 0)))) (h "0mi8fp4b7c3b839h2gq2m6yg6d4n605427c54zpkl0dd1qv9kcpl")))

