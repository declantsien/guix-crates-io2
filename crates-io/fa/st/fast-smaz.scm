(define-module (crates-io fa st fast-smaz) #:use-module (crates-io))

(define-public crate-fast-smaz-0.1.0 (c (n "fast-smaz") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "smaz") (r "^0.1.0") (d #t) (k 2)))) (h "065cd7gfbhz8424223vwz8vs2fq5z7fl16dsyi19129mzavx09b6") (f (quote (("default") ("custom-cookbook"))))))

