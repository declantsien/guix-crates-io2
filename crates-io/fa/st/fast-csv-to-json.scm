(define-module (crates-io fa st fast-csv-to-json) #:use-module (crates-io))

(define-public crate-fast-csv-to-json-0.3.1 (c (n "fast-csv-to-json") (v "0.3.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde" "rayon"))) (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.36") (d #t) (k 0)) (d (n "polars") (r "^0.28.0") (f (quote ("performant"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1rxai6g74hgz13bxai31zimmvfkrkn04jdhw1xjhnqnmpkz5nghx")))

