(define-module (crates-io fa st fast-notebook-clear-output) #:use-module (crates-io))

(define-public crate-fast-notebook-clear-output-0.1.0 (c (n "fast-notebook-clear-output") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.30") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json-event-parser-witespace") (r "^0.1.0") (d #t) (k 0)))) (h "0zdcmb7i54wg4f84y5zsxc5d9f90p1ynpk6xdd39yad8xp1d56dg")))

