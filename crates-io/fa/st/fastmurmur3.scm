(define-module (crates-io fa st fastmurmur3) #:use-module (crates-io))

(define-public crate-fastmurmur3-0.1.2 (c (n "fastmurmur3") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 2)) (d (n "murmur3") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)) (d (n "sha-1") (r "^0.10.0") (d #t) (k 2)) (d (n "twox-hash") (r "^1.6.1") (d #t) (k 2)) (d (n "xxhash-rust") (r "^0.8.2") (f (quote ("xxh3"))) (d #t) (k 2)))) (h "18n2vhn7cdvg7qx9iv1qb9shr3zii7ky1fgpzzqs7wh1ms0z88nr") (f (quote (("murmur3c"))))))

(define-public crate-fastmurmur3-0.2.0 (c (n "fastmurmur3") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 2)) (d (n "murmur3") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)) (d (n "sha-1") (r "^0.10.0") (d #t) (k 2)) (d (n "twox-hash") (r "^1.6.1") (d #t) (k 2)) (d (n "xxhash-rust") (r "^0.8.2") (f (quote ("xxh3"))) (d #t) (k 2)))) (h "1ps9szibpkigriz7wb3zjf8i3g5mm00hn51qi6xspkg4ig39nzid") (f (quote (("murmur3c"))))))

