(define-module (crates-io fa st fast_runner) #:use-module (crates-io))

(define-public crate-fast_runner-0.1.0 (c (n "fast_runner") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "13mbpbhsqj07b8nr9hc149g282rs7v504izjdwf0zy1d9i24zfch")))

(define-public crate-fast_runner-0.1.1 (c (n "fast_runner") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ran7ggb08qk8br74frbziidnkgr6jpwyphksxcb3rbzbcgp9cj3")))

