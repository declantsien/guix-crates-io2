(define-module (crates-io fa st fast-rustc-ap-rustc_target) #:use-module (crates-io))

(define-public crate-fast-rustc-ap-rustc_target-1.0.0 (c (n "fast-rustc-ap-rustc_target") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc_data_structures") (r "^1.0.0") (d #t) (k 0) (p "fast-rustc-ap-rustc_data_structures")) (d (n "rustc_index") (r "^1.0.0") (d #t) (k 0) (p "fast-rustc-ap-rustc_index")) (d (n "rustc_macros") (r "^1.0.0") (d #t) (k 0) (p "fast-rustc-ap-rustc_macros")) (d (n "rustc_serialize") (r "^1.0.0") (d #t) (k 0) (p "fast-rustc-ap-serialize")) (d (n "rustc_span") (r "^1.0.0") (d #t) (k 0) (p "fast-rustc-ap-rustc_span")))) (h "14gdi8nify1lap93jsavls5lb1p9i3p3hi8bz0k6195z1r6yym53")))

