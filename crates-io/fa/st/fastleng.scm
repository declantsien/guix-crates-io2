(define-module (crates-io fa st fastleng) #:use-module (crates-io))

(define-public crate-fastleng-0.1.0 (c (n "fastleng") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "needletail") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1i6l6l3sv8zndpqwhhga1sj0hmlh9pw32i9gj70m9gy8b509jygc")))

(define-public crate-fastleng-0.1.1 (c (n "fastleng") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "needletail") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1w6ccmlmi11wvcybl7z6y48lxi84yf7hannqxgbc738vypbkk3s2")))

(define-public crate-fastleng-0.2.0 (c (n "fastleng") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "needletail") (r "^0.4.1") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.39.5") (f (quote ("static"))) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1rs8cil68fl0l9pjcl7fqdnlr3hrnch2dc9b9pixz3psxqf03zg6")))

