(define-module (crates-io fa st fastpasta_toml_macro_derive) #:use-module (crates-io))

(define-public crate-fastpasta_toml_macro_derive-0.1.0 (c (n "fastpasta_toml_macro_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "16lvxkbf0by0skaasc3a8snr73j1nfpwrcz5815chm42kfdlz7yp")))

(define-public crate-fastpasta_toml_macro_derive-0.1.1 (c (n "fastpasta_toml_macro_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1bg7h16xxhvv1hnwy15s64hazw7si6b4g13ms0399g83yjsrv8bw")))

