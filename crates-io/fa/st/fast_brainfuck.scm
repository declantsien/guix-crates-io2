(define-module (crates-io fa st fast_brainfuck) #:use-module (crates-io))

(define-public crate-fast_brainfuck-0.0.1 (c (n "fast_brainfuck") (v "0.0.1") (d (list (d (n "jit") (r "*") (d #t) (k 0)))) (h "1hiwnil7i9c7jr5bfbzw4fy0b6v11lrwhd5y2d3acwvdg573wwr3") (y #t)))

(define-public crate-fast_brainfuck-0.0.2 (c (n "fast_brainfuck") (v "0.0.2") (d (list (d (n "jit") (r "*") (d #t) (k 0)))) (h "0s0v5ifbq8876vax0j3capmsk49sxdgrzsaa5b8qi1s63x84dm21") (y #t)))

(define-public crate-fast_brainfuck-0.0.3 (c (n "fast_brainfuck") (v "0.0.3") (d (list (d (n "jit") (r "*") (d #t) (k 0)))) (h "1h2hrndkcbwb635gyg2hfsnx1wfbp4qldnsqwjbf4mn4jgv9nqbr") (y #t)))

