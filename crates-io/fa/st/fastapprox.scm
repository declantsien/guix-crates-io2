(define-module (crates-io fa st fastapprox) #:use-module (crates-io))

(define-public crate-fastapprox-0.1.0 (c (n "fastapprox") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "special") (r "^0.7.7") (d #t) (k 2)) (d (n "statrs") (r "^0.9.0") (d #t) (k 2)))) (h "00xpzq4whr1f5qm1fd9jaw1kqcxmy441bfjwbyybpbbdb6l2m82g")))

(define-public crate-fastapprox-0.2.0 (c (n "fastapprox") (v "0.2.0") (h "19ff5npq45zmryayjmi80zhxnxfd5fysd82jzlkvln6xs6pc6i8x")))

(define-public crate-fastapprox-0.3.0 (c (n "fastapprox") (v "0.3.0") (h "0954s6kjvx4s39svdfn39cnv6zkazzmk5ng25lkihpbvyf9iq0yh")))

(define-public crate-fastapprox-0.3.1 (c (n "fastapprox") (v "0.3.1") (h "0ihcphli9rqilss1b086j7mp3jkkyv1g902nh0wyhy2jsc7krylx")))

