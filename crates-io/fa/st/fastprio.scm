(define-module (crates-io fa st fastprio) #:use-module (crates-io))

(define-public crate-fastprio-0.1.0 (c (n "fastprio") (v "0.1.0") (d (list (d (n "heapless") (r "^0.7.16") (o #t) (d #t) (k 0)))) (h "03s693pck6bcsx10drzdm9i21li076nzmhd7n21nn9fmm5pa1gi4") (f (quote (("std"))))))

(define-public crate-fastprio-0.1.1 (c (n "fastprio") (v "0.1.1") (d (list (d (n "heapless") (r "^0.7.16") (o #t) (d #t) (k 0)))) (h "0x1jm8ql9m21czfbz4171574r9w8hijkzz6943gg33nb2l0jjkxm") (f (quote (("std"))))))

