(define-module (crates-io fa st fast-boolean-anf-transform) #:use-module (crates-io))

(define-public crate-fast-boolean-anf-transform-0.0.1 (c (n "fast-boolean-anf-transform") (v "0.0.1") (d (list (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)))) (h "0j9m1kp6acj908c453z583spgpzp33wlaj4b6g8v62sgkibyygxq") (y #t)))

(define-public crate-fast-boolean-anf-transform-0.0.2 (c (n "fast-boolean-anf-transform") (v "0.0.2") (d (list (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)))) (h "1h9rvclspy93vm0rfbm243rvvh6lzl2h6nsib67p6mqmrm2qwz47")))

