(define-module (crates-io fa st fast-tak) #:use-module (crates-io))

(define-public crate-fast-tak-0.1.0 (c (n "fast-tak") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "takparse") (r "^0.5.5") (d #t) (k 0)))) (h "1d7ci6yvqiijpxaxngwwc5gvm277kv5qd2x65qqkdpcndisigpkn") (r "1.65")))

(define-public crate-fast-tak-0.2.0 (c (n "fast-tak") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "takparse") (r "^0.5.5") (d #t) (k 0)))) (h "0jscl171jfqqxjbxqz2p1qjw9wx592kzkhwpgy9s1zkrfkmzc70r") (r "1.65")))

(define-public crate-fast-tak-0.2.1 (c (n "fast-tak") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "takparse") (r "^0.5.5") (d #t) (k 0)))) (h "0xmpgsirsrk91qjh7n0z29p2hsvjb2p0s7i0ark5k3qnf2xvhvxl") (r "1.65")))

(define-public crate-fast-tak-0.3.0 (c (n "fast-tak") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "takparse") (r "^0.5.5") (d #t) (k 0)))) (h "0wv5znpvya295lkib4s6pa5090adim3qhys7z0bxqpd6pdjvvwwn") (r "1.65")))

(define-public crate-fast-tak-0.3.1 (c (n "fast-tak") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "takparse") (r "^0.5.5") (d #t) (k 0)))) (h "15wqfpiw7dcbac8p71ndsz4i410syw52vb93dqfmvjrnkhhfisiq") (r "1.65")))

(define-public crate-fast-tak-0.3.2 (c (n "fast-tak") (v "0.3.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "takparse") (r "^0.5.5") (d #t) (k 0)))) (h "1cq4bbkilvxlhlx72fwzq1a0bvsw2j1vrj0gzn72v2004n42dsdg") (r "1.65")))

(define-public crate-fast-tak-0.3.3 (c (n "fast-tak") (v "0.3.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "takparse") (r "^0.5.5") (d #t) (k 0)))) (h "0i8kpx8jr4icdk6vv50vv4cwh1wz9i2vqi7wrh556nndkyzmr1pm") (r "1.65")))

(define-public crate-fast-tak-0.3.4 (c (n "fast-tak") (v "0.3.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "takparse") (r "^0.5.5") (d #t) (k 0)))) (h "16rp5ycv3y72gy07r6ydjys06d89651dgws700lyvma1d1ajilb1") (r "1.65")))

(define-public crate-fast-tak-0.3.5 (c (n "fast-tak") (v "0.3.5") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "takparse") (r "^0.6.0") (d #t) (k 0)))) (h "0h9c0i12109p5l6yyqbk3nn7sr1l68n19h4p0hn624m2rqaxkzjg") (r "1.65")))

(define-public crate-fast-tak-0.4.0 (c (n "fast-tak") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "takparse") (r "^0.6.0") (d #t) (k 0)))) (h "1593xizls6xj9zrdlmsl3vcir1kiz1lgw2w729r4mpn0h7sk33kr") (r "1.65")))

(define-public crate-fast-tak-0.4.1 (c (n "fast-tak") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "takparse") (r "^0.6.0") (d #t) (k 0)))) (h "0sqqayqk6z3ifzf4ls3yhkqvg17rvg1h5vd5bakxqi7havy4zfh4") (r "1.65")))

