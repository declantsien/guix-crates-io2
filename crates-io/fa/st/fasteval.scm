(define-module (crates-io fa st fasteval) #:use-module (crates-io))

(define-public crate-fasteval-0.1.5 (c (n "fasteval") (v "0.1.5") (h "0fnmwxyd1r58ys2j7knk92s2amk0qdnkjh6rhs516djw9kmakg62") (f (quote (("unsafe-vars") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval-0.1.6 (c (n "fasteval") (v "0.1.6") (h "08k47k6sj2wjgrv465dd509dcv1ggyfb4sgwrqz96vsbvnni3h3y") (f (quote (("unsafe-vars") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval-0.1.7 (c (n "fasteval") (v "0.1.7") (h "0zypgdv2s4vwg8sbllj5f6wqq6d1ydy50jz6kq8yf6w0dahc92qa") (f (quote (("unsafe-vars") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval-0.1.8 (c (n "fasteval") (v "0.1.8") (h "116krb5048h8pr4w0y65n29nbv5jjjyf4rl9j9bbsd512hfkmbpj") (f (quote (("unsafe-vars") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval-0.1.9 (c (n "fasteval") (v "0.1.9") (h "0dp5qqhhynp3hwwz76r6w90c18bx9zx4s93fs4v1ljdqg30vvvn4") (f (quote (("unsafe-vars") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval-0.2.0 (c (n "fasteval") (v "0.2.0") (h "1xpxqcnp92izagdw51fj4bfwbkzwz63k7dzna3wlbh0cj0asvfcq") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval-0.2.1 (c (n "fasteval") (v "0.2.1") (h "0bcz54zxql5sqh089hrbx68jihnnyhh6wwrlql7mwmw562hh435s") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval-0.2.2 (c (n "fasteval") (v "0.2.2") (h "0scf76hvsgbdwcg6r0k05fkgpa7cfi80gr7fzjl76zjjhia2xp0q") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval-0.2.3 (c (n "fasteval") (v "0.2.3") (h "1ghhnwxvdhsj99f9z4k43f5vfn7inydjp0ifrbwzwzcqid95nlr4") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

(define-public crate-fasteval-0.2.4 (c (n "fasteval") (v "0.2.4") (h "0xfgqxdkjprsldb40fm6fcx9mvwcbdkghw07wd47qp86wk4xlk2g") (f (quote (("unsafe-vars") ("nightly") ("default" "alpha-keywords") ("alpha-keywords"))))))

