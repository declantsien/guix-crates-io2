(define-module (crates-io fa dr fadroma-derive-serde) #:use-module (crates-io))

(define-public crate-fadroma-derive-serde-0.3.0 (c (n "fadroma-derive-serde") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cywqxs9h6hv67pzgmn06wf51bqx8qacp7mdiyv0ahzlx0wyj1ld")))

