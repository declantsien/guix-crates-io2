(define-module (crates-io fa dr fadroma-proc-derive) #:use-module (crates-io))

(define-public crate-fadroma-proc-derive-0.6.0 (c (n "fadroma-proc-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 2) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.8.11") (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (k 2)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)))) (h "1bzzldgjla4ymlmdgywlhm0w30h46mhnq6y9im49rjm9hzal0glq")))

(define-public crate-fadroma-proc-derive-0.7.0 (c (n "fadroma-proc-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 2) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.8.11") (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (k 2)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)))) (h "1ywdqdvic5w8i8cgc4vy6c8b0nvlxdypndm76n9fw4y7rwxndvxc")))

