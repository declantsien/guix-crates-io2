(define-module (crates-io fa dr fadroma-derive-canonize) #:use-module (crates-io))

(define-public crate-fadroma-derive-canonize-0.3.0 (c (n "fadroma-derive-canonize") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 2)))) (h "1nssjnqfpq54c3z45kng3j5c4aqdm9m1yz491nba4g8cykxr6ps7")))

(define-public crate-fadroma-derive-canonize-0.3.5 (c (n "fadroma-derive-canonize") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 2)))) (h "1fj54s98lndkr84816c4mam4nwgw395254pyjlydaa5y96wp8452")))

