(define-module (crates-io fa dr fadroma-proc-auth) #:use-module (crates-io))

(define-public crate-fadroma-proc-auth-0.1.0 (c (n "fadroma-proc-auth") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yayhj7d0qd2j4i9dhrqq2hiipxrhkir87f1h5759ary83hd64a1")))

(define-public crate-fadroma-proc-auth-0.1.1 (c (n "fadroma-proc-auth") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13rg8apq8v35w46wn92nia8ygxh3sb1zam9f30kp8yhbzhjlvm6b")))

