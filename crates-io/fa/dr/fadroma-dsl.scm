(define-module (crates-io fa dr fadroma-dsl) #:use-module (crates-io))

(define-public crate-fadroma-dsl-0.8.0 (c (n "fadroma-dsl") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y2ya92y6ydql2zl2s1zfcqkfaci2lhw62qzqshi3jswj22krz27")))

