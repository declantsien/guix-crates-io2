(define-module (crates-io fa il failure_derive) #:use-module (crates-io))

(define-public crate-failure_derive-0.0.1 (c (n "failure_derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6.0") (d #t) (k 0)))) (h "13is314zih7p8q8y1y0g0zy1k2fzsyc70zqqkxkzf2c51296lliy")))

(define-public crate-failure_derive-0.1.0 (c (n "failure_derive") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6.0") (d #t) (k 0)))) (h "018g0mzcz2zmyfas4fm0f3snz0prc0rz59haff8hgbqcc62xw8vj")))

(define-public crate-failure_derive-0.1.1 (c (n "failure_derive") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6.0") (d #t) (k 0)))) (h "12yvipnaz2jpv0x1490mk3sp73ny887hlrrvgav9n35rbdaxmkf7") (f (quote (("std") ("default" "std"))))))

(define-public crate-failure_derive-0.1.2 (c (n "failure_derive") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.9.0") (d #t) (k 0)))) (h "09k4d496l4c41vdw0a0hqaqmbx7qlwn8s0w9spsk360dlnc0wvcl") (f (quote (("std"))))))

(define-public crate-failure_derive-0.1.3 (c (n "failure_derive") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.0") (d #t) (k 0)))) (h "15h52biaaqpzrgasghhlbja8kfamh99z9vcfa73bdlwfzq9xkhk4") (f (quote (("std"))))))

(define-public crate-failure_derive-0.1.4 (c (n "failure_derive") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.0") (d #t) (k 0)))) (h "04ncsv5a2qvf7ki6zqcngank9wsibif47wjvx022gd17mca5lfbw") (f (quote (("std"))))))

(define-public crate-failure_derive-0.1.5 (c (n "failure_derive") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.0") (d #t) (k 0)))) (h "1q97n7dp51j5hndzic9ng2fgn6f3z5ya1992w84l7vypby8n647a") (f (quote (("std"))))))

(define-public crate-failure_derive-0.1.6 (c (n "failure_derive") (v "0.1.6") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.0") (d #t) (k 0)))) (h "022xfb9wcs1bdssfm2airsrfxpn2ccpbyh1ld2wf9483isvjbhhb") (f (quote (("std"))))))

(define-public crate-failure_derive-0.1.7 (c (n "failure_derive") (v "0.1.7") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.0") (d #t) (k 0)))) (h "0cfjz0c9szqpxn43b2r722p6m3swzxj7aj6xhqw23ml7h8y762h3") (f (quote (("std"))))))

(define-public crate-failure_derive-0.1.8 (c (n "failure_derive") (v "0.1.8") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.0") (d #t) (k 0)))) (h "1936adqqk080439kx2bjf1bds7h89sg6wcif4jw0syndcv3s6kda") (f (quote (("std"))))))

