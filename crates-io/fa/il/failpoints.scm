(define-module (crates-io fa il failpoints) #:use-module (crates-io))

(define-public crate-failpoints-0.1.0 (c (n "failpoints") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-test") (r "^0.1") (d #t) (k 2)))) (h "17a4k7das30qwa7r4kj8pswvpc0crkyjz0vgimjmr8zmfh3gswdg") (f (quote (("failpoints"))))))

(define-public crate-failpoints-0.2.0 (c (n "failpoints") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2.1") (d #t) (k 2)))) (h "1mbvzix0r827fc0m08cd3lfgfmch892qw5l2dvmc6kvzan419539") (f (quote (("failpoints"))))))

