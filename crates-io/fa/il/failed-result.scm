(define-module (crates-io fa il failed-result) #:use-module (crates-io))

(define-public crate-failed-result-0.1.0 (c (n "failed-result") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winnt" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1kc9qpx7ah3fihgvdyngqn1m2fww1kal38c4j8kzmmjjigac684w")))

(define-public crate-failed-result-0.2.0 (c (n "failed-result") (v "0.2.0") (d (list (d (n "windows") (r "^0.37") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xgwhhwrs0kc88nayi39w3mcq287gjc5z7zx7s72f5mkn1r0l3v2")))

(define-public crate-failed-result-0.2.1 (c (n "failed-result") (v "0.2.1") (d (list (d (n "windows") (r "^0.37") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "09ifiz9vzw3s1v22m2370i9gl67jyhax59ifksz6xlqgx5fip7q3")))

(define-public crate-failed-result-0.2.2 (c (n "failed-result") (v "0.2.2") (d (list (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "17f57zb346ljzfawvdanlhm7740miav4qrw4g24r4rhajdpcy3sy")))

(define-public crate-failed-result-0.2.3 (c (n "failed-result") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("winnt" "processthreadsapi" "psapi" "errhandlingapi" "winuser" "winbase" "fileapi" "ioapiset" "winerror" "stringapiset" "winnls"))) (d #t) (t "cfg(windows)") (k 2)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0mbiy3y204nrjhcbrqp9y6zwnl5s6sr3xlxzwdlzv5nhmgn9sb44")))

(define-public crate-failed-result-0.2.4 (c (n "failed-result") (v "0.2.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("winnt" "processthreadsapi" "psapi" "errhandlingapi" "winuser" "winbase" "fileapi" "ioapiset" "winerror" "stringapiset" "winnls"))) (d #t) (t "cfg(windows)") (k 2)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ch2fvyqhrc807vnyjwv9livcik1v1s0rd649rr5s138lg7x41zw")))

(define-public crate-failed-result-0.2.5 (c (n "failed-result") (v "0.2.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("winnt" "processthreadsapi" "psapi" "errhandlingapi" "winuser" "winbase" "fileapi" "ioapiset" "winerror" "stringapiset" "winnls"))) (d #t) (t "cfg(windows)") (k 2)) (d (n "windows") (r "^0.52") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12nym6jqjxf2cj3mjh1hipr5bsnyk78vy2q0kmhlcck7w7sfaxwm")))

