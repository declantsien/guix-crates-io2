(define-module (crates-io fa il failure) #:use-module (crates-io))

(define-public crate-failure-0.0.1 (c (n "failure") (v "0.0.1") (h "07iwcniji5r842impixs1984figs53sgzgg9y41v57issvf4pa1f")))

(define-public crate-failure-0.1.0 (c (n "failure") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "0377qrd2sirhkjh3lvf6rnjm0na2c315a7vcici86j2rcxk09akn") (f (quote (("std" "backtrace") ("default" "std"))))))

(define-public crate-failure-0.1.1 (c (n "failure") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "10nysp7fiqx2y7xvr6abxs4kgr5cw6nb1nh2580mliyyq6v9jiwk") (f (quote (("std" "backtrace") ("derive" "failure_derive") ("default" "std" "derive"))))))

(define-public crate-failure-0.1.2 (c (n "failure") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1fcmzwhdwcj0cp56sjaj0hscpa8zz6c2hp51q4g6nijadrl25yvy") (f (quote (("std" "backtrace") ("derive" "failure_derive") ("default" "std" "derive"))))))

(define-public crate-failure-0.1.3 (c (n "failure") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1iqs6jcd90xhlx5ba1gcji1j5hqrz8jfrqv7368wxdxiq6y7glvd") (f (quote (("std" "backtrace") ("derive" "failure_derive") ("default" "std" "derive"))))))

(define-public crate-failure-0.1.4 (c (n "failure") (v "0.1.4") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1gqzwlk4d61mkxdywn3n6aprgz370bccbihfa9xykihlq8zbjig9") (f (quote (("std" "backtrace") ("derive" "failure_derive") ("default" "std" "derive"))))))

(define-public crate-failure-0.1.5 (c (n "failure") (v "0.1.5") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1qppmgv4i5jj6vrss91qackqnl0a12h7lnby4l7j5fdy78yxhnvr") (f (quote (("std" "backtrace") ("derive" "failure_derive") ("default" "std" "derive"))))))

(define-public crate-failure-0.1.6 (c (n "failure") (v "0.1.6") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "1nay5c2cgi40kp84rbiir1dgwlh9aap9jazbnxfmqrkpr49ky9zq") (f (quote (("std" "backtrace") ("derive" "failure_derive") ("default" "std" "derive"))))))

(define-public crate-failure-0.1.7 (c (n "failure") (v "0.1.7") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0js6i6mb42q1g6q3csfbmi6q40s64k96705xbim0d8zg44j9qlmq") (f (quote (("std" "backtrace") ("derive" "failure_derive") ("default" "std" "derive"))))))

(define-public crate-failure-0.1.8 (c (n "failure") (v "0.1.8") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "11jg1wmbkijrs6bk9fqnbrm9zf0850whnqpgnxyswbn0dk8rnbnk") (f (quote (("std" "backtrace") ("derive" "failure_derive") ("default" "std" "derive"))))))

