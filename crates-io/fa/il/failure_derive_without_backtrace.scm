(define-module (crates-io fa il failure_derive_without_backtrace) #:use-module (crates-io))

(define-public crate-failure_derive_without_backtrace-0.1.2 (c (n "failure_derive_without_backtrace") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)) (d (n "synstructure") (r "^0.9.0") (d #t) (k 0)))) (h "17ndvqv8kzz2vwiqvaww0cw5maaz3gwbngn1d870j9j99j3wqfd0") (f (quote (("std"))))))

