(define-module (crates-io fa il failure-tools) #:use-module (crates-io))

(define-public crate-failure-tools-4.0.2 (c (n "failure-tools") (v "4.0.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "19z36hv8jx49cpq1r2k97i0ds5ng5m7s9mmkf8pvzhfq7x9vqvp7")))

(define-public crate-failure-tools-4.0.3 (c (n "failure-tools") (v "4.0.3") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "00948yvw3mzpkfaiyynzxnp1xqnd3jdi7g4phmyvnj6qwr5xp4nx")))

(define-public crate-failure-tools-4.0.4 (c (n "failure-tools") (v "4.0.4") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "13z3227mpscb1n98jgbk5ljjnnqga6p9sxa68a78zp2lvdhhvfzi")))

(define-public crate-failure-tools-4.0.5 (c (n "failure-tools") (v "4.0.5") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "1jkjgnbpblj4nzl3v80hrb8gw6dbmn9nxb8j5y9pwf0jfj8bdl83")))

