(define-module (crates-io fa il fail_on_ci) #:use-module (crates-io))

(define-public crate-fail_on_ci-0.1.0 (c (n "fail_on_ci") (v "0.1.0") (h "1dhbdq2cfc11gp1hamp0p74i3idq1h9kxnhh7bv10l703170a6pf")))

(define-public crate-fail_on_ci-0.1.1 (c (n "fail_on_ci") (v "0.1.1") (h "182xmk0zhv2sapnfivx9g3qahb94k2711w7rhccqr64frihjqfrs")))

(define-public crate-fail_on_ci-0.1.3 (c (n "fail_on_ci") (v "0.1.3") (h "1blsl1v70bhr9ac5j1hz5q7xv8x3lfl9zldy2li09jl92xihqzfx")))

(define-public crate-fail_on_ci-0.1.5 (c (n "fail_on_ci") (v "0.1.5") (h "003ycc1s34kn2ac8w4jbvpzd9grlimrfm5z78lrvrn51ghq052yz")))

(define-public crate-fail_on_ci-0.1.6 (c (n "fail_on_ci") (v "0.1.6") (h "1fdd4lzjd3ngrj8s0hdbqq22y93q1wbrmbi52ll5f9s49bb1kpa3")))

(define-public crate-fail_on_ci-0.1.7 (c (n "fail_on_ci") (v "0.1.7") (h "1ym2mrv83frfx5p59hp2mqrfashmswpfqqpr6i20ar1h938gc2n9")))

