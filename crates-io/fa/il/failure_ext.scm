(define-module (crates-io fa il failure_ext) #:use-module (crates-io))

(define-public crate-failure_ext-0.1.0 (c (n "failure_ext") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1dkjz25cm8g2l7ds960vj4941q2lbi2kypil091q654lvcj18dya")))

(define-public crate-failure_ext-0.1.1 (c (n "failure_ext") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (o #t) (d #t) (k 0)))) (h "0l032ncrlv7a65disfhb4xkac61037s0d5q7hrdmb56gaznlx29a") (f (quote (("future_ext" "futures") ("default"))))))

(define-public crate-failure_ext-0.1.2 (c (n "failure_ext") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (o #t) (d #t) (k 0)))) (h "1h6vmwz4il0nnr9iw4mw8rm331zc11mxmhjjba15rzw14lrmlynq") (f (quote (("future_ext" "futures") ("default"))))))

