(define-module (crates-io fa il fail) #:use-module (crates-io))

(define-public crate-fail-0.1.0 (c (n "fail") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "059lb2bmx5m1r7h9w3qdi8l26k36kd2fmccmm5v93lvy5sbw7n6l") (f (quote (("no_fail"))))))

(define-public crate-fail-0.2.0 (c (n "fail") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0khb1j1sdzhb5v128799fyxg6la5051w41vfdg4wij0nqqi1lbmx") (f (quote (("no_fail"))))))

(define-public crate-fail-0.2.1 (c (n "fail") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0nl0l62qszpfjywfafriq10v46gv5v10pax0yc2fbcv7k87mzjjf") (f (quote (("no_fail"))))))

(define-public crate-fail-0.3.0 (c (n "fail") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1301bn40n98cl7pvy16zc2bfv9az1kzkkczcl09fjgh1ldqyqgpn") (f (quote (("failpoints"))))))

(define-public crate-fail-0.4.0 (c (n "fail") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "19l1s48d9ial0rbbwn3h0n0cw8iicbl1x8rypifizjgxb4fcdqrv") (f (quote (("failpoints"))))))

(define-public crate-fail-0.5.0 (c (n "fail") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "04ah8vsdxzxk4m97v5mc52kmf3w7lqrxh83xg4y7ykjnrah4acpc") (f (quote (("failpoints"))))))

(define-public crate-fail-0.5.1 (c (n "fail") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "075hsgc9wnh5j4v69is5wizcwcg6g8fxpbjk2icsshlayz846ppy") (f (quote (("failpoints"))))))

