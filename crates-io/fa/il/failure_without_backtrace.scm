(define-module (crates-io fa il failure_without_backtrace) #:use-module (crates-io))

(define-public crate-failure_without_backtrace-0.1.2 (c (n "failure_without_backtrace") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "failure_derive_without_backtrace") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "146bbhrb8p7l12knxvl3agdkaicd1rn1rn9ykmvp1ybcvf1cjgm7") (f (quote (("std") ("derive" "failure_derive_without_backtrace") ("default" "std" "derive"))))))

