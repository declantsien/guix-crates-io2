(define-module (crates-io fa il failures_crate) #:use-module (crates-io))

(define-public crate-failures_crate-0.1.0 (c (n "failures_crate") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)))) (h "0xiv4i15aifiqk8r3pffm12b3s4kkcvnbs3nxz7cgf5xbr5s9600")))

