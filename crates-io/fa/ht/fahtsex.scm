(define-module (crates-io fa ht fahtsex) #:use-module (crates-io))

(define-public crate-fahtsex-0.1.0 (c (n "fahtsex") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0k5zn703x2jwg5rfnrwjfzwj7bvc337s60r4pbimw8403j7p7kdz")))

(define-public crate-fahtsex-0.1.1 (c (n "fahtsex") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0fyi6a4hv3hv2hn55f8479msx43hvs2c6ppdcw2a34ak8ncqwmkp")))

(define-public crate-fahtsex-0.1.2 (c (n "fahtsex") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0g7amr2c29k333006a5i6kcwqqabqb3k3i2h4y4fdnjp754zh603")))

(define-public crate-fahtsex-0.2.0 (c (n "fahtsex") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1kxzb9wgsk3gj12blzw15gvsq5n7szr3qzdqjzphnza7ysjappnh")))

(define-public crate-fahtsex-0.2.1 (c (n "fahtsex") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1ss1z68cp1y53k3wwaj7fg0kc7jayd95xnsj8j1cigylbhhx3mgb")))

(define-public crate-fahtsex-0.2.2 (c (n "fahtsex") (v "0.2.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "07zgib3m1yqlsvzcv051sra96aq47banggkcjxrflqpdzy57a3s5")))

