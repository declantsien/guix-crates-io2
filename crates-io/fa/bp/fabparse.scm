(define-module (crates-io fa bp fabparse) #:use-module (crates-io))

(define-public crate-fabparse-0.1.0 (c (n "fabparse") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.11.2") (d #t) (k 0)))) (h "0yzma4lmk4asdp3f2aqph9bs8cvqd4p08gh24yg5jj6qpg53hj87")))

(define-public crate-fabparse-0.1.1 (c (n "fabparse") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.11.2") (d #t) (k 0)))) (h "1izfivhzc196x4p24lywa5jyd7ms1zxfyx3ldxrw85dvny1yydif")))

