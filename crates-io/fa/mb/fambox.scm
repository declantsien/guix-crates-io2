(define-module (crates-io fa mb fambox) #:use-module (crates-io))

(define-public crate-fambox-0.1.0 (c (n "fambox") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "serde") (r "^1.0.200") (o #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (k 2)) (d (n "smallvec") (r "^1.13.2") (f (quote ("const_generics" "const_new"))) (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "1ldc2vydnlmyqa291q68pia3dhvy4ypwfgr1rc172l5c7n5zg6q1") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde" "dep:smallvec"))))))

(define-public crate-fambox-0.1.1 (c (n "fambox") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "serde") (r "^1.0.200") (o #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (k 2)) (d (n "smallvec") (r "^1.13.2") (f (quote ("const_generics" "const_new"))) (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "0f58z31ghfdq85xhjqzirpq91s96lw9ilkpwcmvimc6pprk1hbwz") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde" "dep:smallvec"))))))

(define-public crate-fambox-0.1.2 (c (n "fambox") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "serde") (r "^1.0.200") (o #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (k 2)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "0sdc3g4mpd58h3hfrk0928lvzk02y1v20kxbspfjmns007w2f56z") (f (quote (("default" "alloc") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde"))))))

