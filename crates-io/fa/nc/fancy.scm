(define-module (crates-io fa nc fancy) #:use-module (crates-io))

(define-public crate-fancy-0.1.2 (c (n "fancy") (v "0.1.2") (h "0nwsd76djykyvshn86hvf8p24mf9m7b5vi8kzapcxkhwxdmxar4n")))

(define-public crate-fancy-0.1.3 (c (n "fancy") (v "0.1.3") (h "1w5169wy0rcb4s47q1qjssxxvm7889dhr3483v2bapppxqq36v7j")))

(define-public crate-fancy-0.2.1 (c (n "fancy") (v "0.2.1") (h "04rq3jwmib01zzm3h1mkhfq06arbir526x30849kaz159mjc6qxx")))

(define-public crate-fancy-0.2.2 (c (n "fancy") (v "0.2.2") (h "04a1y85akyafk2ziz799kryfvqv32cyb846jmjvf0bqpznbz7jax")))

(define-public crate-fancy-0.2.3 (c (n "fancy") (v "0.2.3") (h "1r3ypik0vbf6xa7byh6qyld6xg5ifnkjqvwvbs1gaqaswb1vaja6")))

(define-public crate-fancy-0.3.0 (c (n "fancy") (v "0.3.0") (h "06w4rswgyj3hmkgv7qra2cm3kcavxkjdxvihcv401v6390ww4q2x")))

(define-public crate-fancy-0.3.1 (c (n "fancy") (v "0.3.1") (h "16a65nxfhkdkjndcf8dvw392k6p87l0l7j7f5332l5g9783h3dh5")))

