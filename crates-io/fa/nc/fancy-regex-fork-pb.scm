(define-module (crates-io fa nc fancy-regex-fork-pb) #:use-module (crates-io))

(define-public crate-fancy-regex-fork-pb-0.1.0 (c (n "fancy-regex-fork-pb") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "19zpnziqghfhys1517gwmc2d3xfy8ahdb59x2h757fb1nanbirxr")))

(define-public crate-fancy-regex-fork-pb-0.1.1 (c (n "fancy-regex-fork-pb") (v "0.1.1") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1nhfzhjfl1m82gpy3dwm33d5hprvmv34azisk6vjcal753qjzsck")))

(define-public crate-fancy-regex-fork-pb-0.3.1 (c (n "fancy-regex-fork-pb") (v "0.3.1") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.2") (d #t) (k 0)))) (h "1r16jixhgm9xq5vynnhlsmzhhjwnyjkiwlgznpv90shldwjv5bh6")))

(define-public crate-fancy-regex-fork-pb-0.3.2 (c (n "fancy-regex-fork-pb") (v "0.3.2") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.2") (d #t) (k 0)))) (h "1kbwqy6rmva77jlk1gc4fq6sdrpapzl7jwsm8dfzd1dvvja9awyd")))

