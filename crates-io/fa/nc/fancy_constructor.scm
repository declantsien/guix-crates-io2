(define-module (crates-io fa nc fancy_constructor) #:use-module (crates-io))

(define-public crate-fancy_constructor-1.0.0 (c (n "fancy_constructor") (v "1.0.0") (d (list (d (n "macroific") (r "^1.1.2") (f (quote ("attr_parse"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1bag657j3vg1lw005hqk9dqqn2j9hk3kxqkh4719lmkw2i14pdin") (f (quote (("nightly" "macroific/nightly")))) (r "1.60.0")))

(define-public crate-fancy_constructor-1.0.1 (c (n "fancy_constructor") (v "1.0.1") (d (list (d (n "macroific") (r "^1.2.2") (f (quote ("attr_parse" "full"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0v78454aifj75dmmp6qqhsyawa8wanlh76y4ygsdsmqglf4njs9i") (f (quote (("nightly" "macroific/nightly")))) (r "1.60.0")))

(define-public crate-fancy_constructor-1.1.0 (c (n "fancy_constructor") (v "1.1.0") (d (list (d (n "macroific") (r "^1.2.2") (f (quote ("attr_parse" "full"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0m3sv7jj5ywdf3aa0c2cgj8sikg0fhsfyrkxmk2y572bqhd9h4ad") (f (quote (("nightly" "macroific/nightly")))) (r "1.60.0")))

(define-public crate-fancy_constructor-1.1.1 (c (n "fancy_constructor") (v "1.1.1") (d (list (d (n "macroific") (r "^1.2.2") (f (quote ("attr_parse" "full"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0jxlzm248948a7k7x2q9fkwgbl4sdkv595pb9y9whdrmp60vahp2") (f (quote (("nightly" "macroific/nightly")))) (r "1.60.0")))

(define-public crate-fancy_constructor-1.2.0 (c (n "fancy_constructor") (v "1.2.0") (d (list (d (n "macroific") (r "^1.2.2") (f (quote ("attr_parse" "full"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "186hgxzlhvlym4vl6w3gr0lzwyjpgy7nn7x6hhyqmwjssviir5hh") (f (quote (("nightly" "macroific/nightly")))) (r "1.60.0")))

(define-public crate-fancy_constructor-1.2.1 (c (n "fancy_constructor") (v "1.2.1") (d (list (d (n "macroific") (r "^1.2.2") (f (quote ("attr_parse" "full"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0h6rdyyzjam21gqi9l95v6vf0g42jkasl58vcpzwwmdv9s61azvg") (f (quote (("nightly" "macroific/nightly")))) (r "1.60.0")))

(define-public crate-fancy_constructor-1.2.2 (c (n "fancy_constructor") (v "1.2.2") (d (list (d (n "macroific") (r "^1.2.2") (f (quote ("attr_parse" "full"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0dcllqf8hj7yn8fxyi91kn3v3i7aa861kb4gc27jyfzp99z327zp") (f (quote (("nightly" "macroific/nightly")))) (r "1.60.0")))

