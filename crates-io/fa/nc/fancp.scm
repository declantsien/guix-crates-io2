(define-module (crates-io fa nc fancp) #:use-module (crates-io))

(define-public crate-fancp-0.4.2 (c (n "fancp") (v "0.4.2") (h "0qbz0giy2x2rp1d4brphb7lb32xwdxls9j3ih0734z7k31wa8b50")))

(define-public crate-fancp-0.6.0 (c (n "fancp") (v "0.6.0") (h "16w03b9mdxwp0in9s39n358jhpq823zpcxm5w7m1n8m7vi990jnk")))

