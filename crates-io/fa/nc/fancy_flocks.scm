(define-module (crates-io fa nc fancy_flocks) #:use-module (crates-io))

(define-public crate-fancy_flocks-0.1.0 (c (n "fancy_flocks") (v "0.1.0") (d (list (d (n "fs2") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0v9xya4k48dpzmm0yg9j8g8n5991q1sxv2x7iaajdiiy6lb5ln28")))

