(define-module (crates-io fa nc fancy_sort) #:use-module (crates-io))

(define-public crate-fancy_sort-0.1.0 (c (n "fancy_sort") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w1wdflmh4vcx3075miq353nr6kdf8j1hjamlqp1rj8i0g5xzwn4") (y #t)))

(define-public crate-fancy_sort-0.1.1 (c (n "fancy_sort") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1w9jd7c3vjscq515mjycs4svjfxhvcgrdxlrxax0lgzlfr8qf23m") (y #t)))

(define-public crate-fancy_sort-0.1.2 (c (n "fancy_sort") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0djc9506kx9jlr47nsrma0y98n91l1y704kdmm6jsf4r5sg6z7hn") (y #t)))

