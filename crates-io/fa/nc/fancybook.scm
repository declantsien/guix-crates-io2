(define-module (crates-io fa nc fancybook) #:use-module (crates-io))

(define-public crate-fancybook-0.0.0 (c (n "fancybook") (v "0.0.0") (h "1djfnxv88g6xl2b0cmjfrzqf43ali5gwkzpqd520fz6s3xkhbhy4") (y #t)))

(define-public crate-fancybook-0.0.1 (c (n "fancybook") (v "0.0.1") (h "0w8acx3x39nl18bajy6i4rmjs2qwdrcij27amwxk3a2cf48wk51b")))

