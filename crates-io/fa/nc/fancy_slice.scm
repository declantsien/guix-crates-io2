(define-module (crates-io fa nc fancy_slice) #:use-module (crates-io))

(define-public crate-fancy_slice-0.1.0 (c (n "fancy_slice") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1zbcwjd6l648gpfzzpdsh54v0zv7hmaqhbgp922b839h4pcp6g28") (f (quote (("debug"))))))

(define-public crate-fancy_slice-0.1.1 (c (n "fancy_slice") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "103wknq79zfw1achnfc54my279wykd4mcrl5sax90abggqsin7ak") (f (quote (("debug"))))))

(define-public crate-fancy_slice-0.1.2 (c (n "fancy_slice") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1drzawxhz2vyawvjw85s8pw1zmm6l56mv6apj7pd5vb8pavqmj3v") (f (quote (("debug"))))))

