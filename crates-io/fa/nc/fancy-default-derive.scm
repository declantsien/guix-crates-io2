(define-module (crates-io fa nc fancy-default-derive) #:use-module (crates-io))

(define-public crate-fancy-default-derive-0.1.0 (c (n "fancy-default-derive") (v "0.1.0") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0xbrs5l2fhpj954m5ff16ia8w8r028yhx073zvjx5sryfw446ihb")))

