(define-module (crates-io fa nc fancy-regex-macro) #:use-module (crates-io))

(define-public crate-fancy-regex-macro-0.1.0 (c (n "fancy-regex-macro") (v "0.1.0") (d (list (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "05nyzqqv3xgvsmqvxammgh5qgnlk1jrv83asgkrg6ic2npmvgb53")))

