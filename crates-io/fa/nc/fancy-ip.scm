(define-module (crates-io fa nc fancy-ip) #:use-module (crates-io))

(define-public crate-fancy-ip-0.1.0 (c (n "fancy-ip") (v "0.1.0") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)))) (h "0fp6gyl606kl35q0mj9d33jdhbpzhjl0g2rczpxlw7yj0kcsxqka") (f (quote (("std") ("default" "std"))))))

(define-public crate-fancy-ip-1.0.0 (c (n "fancy-ip") (v "1.0.0") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)))) (h "04g961vky36kx7idan1201jx9cl2bg6hsmz4cj7a6a112fiss9r7") (f (quote (("std") ("default" "std"))))))

(define-public crate-fancy-ip-1.1.0 (c (n "fancy-ip") (v "1.1.0") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)))) (h "1dx1nmy3xci4mj41nqxnnrcphil66b0r59m88x6bjyb5sv6v8k6s") (f (quote (("std") ("default" "std"))))))

