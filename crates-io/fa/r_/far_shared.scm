(define-module (crates-io fa r_ far_shared) #:use-module (crates-io))

(define-public crate-far_shared-0.2.0-beta.0 (c (n "far_shared") (v "0.2.0-beta.0") (h "0g0yfbvybn6syb2n84myvl4hmgf4idqfm4g6rbwz5ky2ksn9p694")))

(define-public crate-far_shared-0.2.0 (c (n "far_shared") (v "0.2.0") (h "0dd7kz00qa1nva7wap6nxirbp9cii59dfc3cci4v76061w2jwxrn")))

