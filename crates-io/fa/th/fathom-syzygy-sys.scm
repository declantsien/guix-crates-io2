(define-module (crates-io fa th fathom-syzygy-sys) #:use-module (crates-io))

(define-public crate-fathom-syzygy-sys-0.1.0 (c (n "fathom-syzygy-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ksnw5qprwiy1pdsz70gvpc97k5fvpa0fszza9ryxs2zh9kr0q6j")))

