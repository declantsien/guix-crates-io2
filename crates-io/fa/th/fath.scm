(define-module (crates-io fa th fath) #:use-module (crates-io))

(define-public crate-fath-0.1.0 (c (n "fath") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0pyzqnk8dvng3cvpay8522q87ab8ff7q76pmvgkha8dvc8zm7m80")))

(define-public crate-fath-0.1.1 (c (n "fath") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "02qcbiydmmzn311rva8dd8258nwkswf4s97rrk241cnyz9lp1941")))

(define-public crate-fath-0.1.2 (c (n "fath") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03srzg0a84qjdcv2wpjskjd48jyzvj67m19f55yzm4vb8v4qc0h1") (y #t)))

(define-public crate-fath-0.1.3 (c (n "fath") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1nnmhychr5mcgcq2b23x9gfc3kf9j3i21gfnqxqmsjn5304k41sr")))

(define-public crate-fath-0.1.4 (c (n "fath") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0lnk74yd5f0dw2w87cz44y8kkazifkznqx3g7f6q731jzg96jg0w")))

(define-public crate-fath-0.1.5 (c (n "fath") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "00p289hc34qm3imjfrijm04qhnlq8jr9mb2rln1xp1v5jbgngiqm")))

(define-public crate-fath-0.1.6 (c (n "fath") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1bvfxrm47xqp9mpqwq44gk9sy1j0yg3bdf4xa60148kqrvdhks4l")))

(define-public crate-fath-0.1.7 (c (n "fath") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0asjqbgmdwa6q98vsrcd6s9bxvnyjg8d61475x09nk94say9fq81") (y #t)))

(define-public crate-fath-0.1.8 (c (n "fath") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03az57vqimjvr2h0ifkajljyglnb1yvadxf4sp151gnhqpzjcw2a")))

(define-public crate-fath-0.1.9 (c (n "fath") (v "0.1.9") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "02p3i733n5h8k2x35gghjci1pyjpzh8xj48nhvbkks6csa8m2pgh")))

(define-public crate-fath-0.2.0 (c (n "fath") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("real_blackbox" "html_reports"))) (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "sleef") (r "^0.3.0") (d #t) (k 2)))) (h "127p4k8s70yi0da9hm7y39vi7z6k1yn19i06fky5ns3y9qyqj3x1")))

(define-public crate-fath-0.2.1 (c (n "fath") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("real_blackbox" "html_reports"))) (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "sleef") (r "^0.3.0") (d #t) (k 2)))) (h "0c0bvkwiyapydg9l6n1ff2jq3f6h9pmwa4b73lfsdlw5v728ssgj")))

