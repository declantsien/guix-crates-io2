(define-module (crates-io fa th fathom-syzygy) #:use-module (crates-io))

(define-public crate-fathom-syzygy-0.1.0 (c (n "fathom-syzygy") (v "0.1.0") (d (list (d (n "fathom-syzygy-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fc63mlwz4kj97qalfqwf5k8x8dzmpw1gdli35s08lgvf2r002vw")))

