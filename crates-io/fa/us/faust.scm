(define-module (crates-io fa us faust) #:use-module (crates-io))

(define-public crate-faust-1.0.0 (c (n "faust") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-native-roots" "trust-dns"))) (k 0)) (d (n "rlimit") (r "^0.6") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^1.6") (f (quote ("rt" "rt-multi-thread" "io-util" "io-std" "sync"))) (d #t) (k 0)))) (h "1jfq25i3an3byknky6fq5vj987vsjmj6kzh06hbqcb6zm9h3waak")))

(define-public crate-faust-1.0.1 (c (n "faust") (v "1.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-native-roots" "trust-dns"))) (k 0)) (d (n "rlimit") (r "^0.6") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^1.6") (f (quote ("rt" "rt-multi-thread" "io-util" "io-std" "sync"))) (d #t) (k 0)))) (h "166swaaq8g889v9k5spq1wmczrg9j7l9ayvwm28q4z4w33syfgbn")))

(define-public crate-faust-1.0.2 (c (n "faust") (v "1.0.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls-native-roots" "trust-dns"))) (k 0)) (d (n "rlimit") (r "^0.6") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("rt" "rt-multi-thread" "io-util" "io-std" "sync"))) (d #t) (k 0)))) (h "19756jw38dhci1ii0nl5p5qvlxw4gmpr61l3kgdylihff7ywn8qq")))

