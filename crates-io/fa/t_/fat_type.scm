(define-module (crates-io fa t_ fat_type) #:use-module (crates-io))

(define-public crate-fat_type-0.1.0 (c (n "fat_type") (v "0.1.0") (h "0pba2id42vsr7v9clj4npzsg6ffpvpg4f04x2qns1gsga8v8bqfg")))

(define-public crate-fat_type-0.2.0 (c (n "fat_type") (v "0.2.0") (h "1469qg5j6drwc6zmzs4dh76zhm7g28v8j9rq8v0rxl0jaz5m4qff")))

(define-public crate-fat_type-0.3.0 (c (n "fat_type") (v "0.3.0") (h "1rm4yly6yijzpp02ip62n7whf8fqfdymc6gipcq93w6mq9l24dkn")))

