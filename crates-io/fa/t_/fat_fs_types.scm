(define-module (crates-io fa t_ fat_fs_types) #:use-module (crates-io))

(define-public crate-fat_fs_types-0.1.0 (c (n "fat_fs_types") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("min_const_generics" "derive"))) (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.31") (f (quote ("derive"))) (o #t) (k 0)))) (h "1nxaqdvjk4s5dhkzbng64b3zrmsjc4i59sgpfqd788rbgbq2hmp2") (f (quote (("std" "alloc") ("alloc")))) (r "1.73.0")))

(define-public crate-fat_fs_types-0.1.1 (c (n "fat_fs_types") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("min_const_generics" "derive"))) (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.31") (f (quote ("derive"))) (o #t) (k 0)))) (h "0rw0lifx0ffzs8417lphg486lsfqr31333n86qamhaaqb2gin2ml") (f (quote (("std" "alloc") ("alloc")))) (r "1.73.0")))

(define-public crate-fat_fs_types-0.2.0 (c (n "fat_fs_types") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("min_const_generics" "derive"))) (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.31") (f (quote ("derive"))) (o #t) (k 0)))) (h "0c8rgnchcgjjh0s3bdxm4yr9kzxqz0flw1gk10n8pd3r1ybzmjj3") (f (quote (("std" "alloc") ("alloc")))) (y #t) (r "1.73.0")))

(define-public crate-fat_fs_types-0.2.1 (c (n "fat_fs_types") (v "0.2.1") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("min_const_generics" "derive"))) (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.31") (f (quote ("derive"))) (o #t) (k 0)))) (h "1l6agrk5rlvsqz070q4q4hwv4s3lqw3j88gh9s98w8zfark4ziyl") (f (quote (("std" "alloc") ("alloc")))) (r "1.73.0")))

(define-public crate-fat_fs_types-0.3.0 (c (n "fat_fs_types") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("min_const_generics" "derive"))) (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.31") (f (quote ("derive"))) (o #t) (k 0)))) (h "1czjd6955k1jjvmk83pa8jrnbj3n8ni06r91sd268ih931dfy363") (f (quote (("std")))) (r "1.73.0")))

