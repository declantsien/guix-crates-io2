(define-module (crates-io fa uc faucet) #:use-module (crates-io))

(define-public crate-faucet-0.1.0 (c (n "faucet") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "deadqueue") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "time"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)))) (h "0b4ndhnbhm4fn8l1ng15a5sgs4a1l45f695sy09a5ig45l7lym4r")))

(define-public crate-faucet-0.1.1 (c (n "faucet") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "deadqueue") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "time"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)))) (h "0hcjq7hiis9kcbz190y53jh4y7mccfgmq4jbv3cj8rnxdllr0gsh")))

