(define-module (crates-io fa uc faucet-drain) #:use-module (crates-io))

(define-public crate-faucet-drain-0.1.0 (c (n "faucet-drain") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "deadqueue") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "time"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)))) (h "0gp1206jxxx33q7layy02c5dpqqfpw77mfzjm4nx2bmkasd4z836") (y #t)))

