(define-module (crates-io fa rt fart-utils) #:use-module (crates-io))

(define-public crate-fart-utils-0.3.0 (c (n "fart-utils") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0nc614w0pq0nj4zlh8zhb0j8zsw71vryb6mrivqyx9nbin9kx3sj")))

(define-public crate-fart-utils-0.4.0 (c (n "fart-utils") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0zjnfmmf64hijj6wfyipif1l9myj7fv9v30khlcx7hhryxap9i8q")))

