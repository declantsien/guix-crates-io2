(define-module (crates-io fa rt fart-aabb) #:use-module (crates-io))

(define-public crate-fart-aabb-0.3.0 (c (n "fart-aabb") (v "0.3.0") (d (list (d (n "euclid") (r "^0.19.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "partial-min-max") (r "= 0.3.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "1n5ljlisxs6pgr302ycfzb6x2blg108gw3c77xndhxw35q1k864h")))

(define-public crate-fart-aabb-0.4.0 (c (n "fart-aabb") (v "0.4.0") (d (list (d (n "euclid") (r "^0.20.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "partial-min-max") (r "= 0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.3") (d #t) (k 2)))) (h "0g98k36plss4va51xaq32vy1l2qnhmk021xv6sphb945znk79m3l")))

