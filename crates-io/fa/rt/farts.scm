(define-module (crates-io fa rt farts) #:use-module (crates-io))

(define-public crate-farts-0.1.0 (c (n "farts") (v "0.1.0") (d (list (d (n "rodio") (r "^0.18.0") (d #t) (k 0)) (d (n "rust-embed") (r "^8.3.0") (d #t) (k 0)))) (h "0jx5nifqpyd4bifpncr5f3l1201x9j6aw9zszq7xxk8gsfydx0hi")))

(define-public crate-farts-0.2.0 (c (n "farts") (v "0.2.0") (d (list (d (n "rodio") (r "^0.18.0") (d #t) (k 0)) (d (n "rust-embed") (r "^8.3.0") (d #t) (k 0)))) (h "0hc5qhjcsw3ci1jxjkm8hfjx5p9dd75ks09w3njh1wcz0vnx0kvk")))

(define-public crate-farts-0.3.0 (c (n "farts") (v "0.3.0") (d (list (d (n "rodio") (r "^0.18.0") (d #t) (k 0)) (d (n "rust-embed") (r "^8.3.0") (d #t) (k 0)))) (h "1dr2lxcgbyr34lf9rwdy5nxfs1w664765lypfbv9rn6856mm60vk")))

(define-public crate-farts-0.4.0 (c (n "farts") (v "0.4.0") (d (list (d (n "rodio") (r "^0.18.0") (d #t) (k 0)) (d (n "rust-embed") (r "^8.3.0") (d #t) (k 0)))) (h "1mj77dqabv6m2gwj4nasm91z733c2m5p1vsskj5a9yz3drvqp1dh")))

(define-public crate-farts-1.0.0 (c (n "farts") (v "1.0.0") (d (list (d (n "rodio") (r "^0.18.0") (d #t) (k 0)) (d (n "rust-embed") (r "^8.3.0") (d #t) (k 0)))) (h "12wbx22k5kyjjh93jy5w3cly08cy0988rqa91jb85aklv0ppkv4j")))

(define-public crate-farts-1.1.0 (c (n "farts") (v "1.1.0") (d (list (d (n "rodio") (r "^0.18.0") (d #t) (k 0)) (d (n "rust-embed") (r "^8.3.0") (d #t) (k 0)))) (h "0wkvmyqr2a8pq4jw6p4hk2y8h56dq4q8yb4k3xj42a4h2b04dhr0")))

