(define-module (crates-io fa rb farba) #:use-module (crates-io))

(define-public crate-farba-0.1.0 (c (n "farba") (v "0.1.0") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)))) (h "0w3ksqcfg3qy9vg614rbaxhrg28nd6jzb7zkgrcwjgkkji428lig") (f (quote (("default" "image")))) (s 2) (e (quote (("image" "dep:image"))))))

(define-public crate-farba-0.1.1 (c (n "farba") (v "0.1.1") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "minifb") (r "^0.24.0") (o #t) (d #t) (k 0)))) (h "1a6z1n4r0qb11p7132w7799lqhx8f01rhxnwg28nzj5kdddjjdml") (f (quote (("default")))) (s 2) (e (quote (("window" "dep:minifb") ("image" "dep:image"))))))

