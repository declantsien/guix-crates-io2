(define-module (crates-io fa rb farbe) #:use-module (crates-io))

(define-public crate-farbe-0.1.0 (c (n "farbe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)))) (h "0zkcd7vsl71r9157sshmgvby2fpbsr8v3zhd4929crvv3k8g330n")))

(define-public crate-farbe-0.2.0 (c (n "farbe") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)))) (h "0ggqk02wn62k9qhf0iy04kn4c5cxq9w3rj4rf0vs5cbq1m6ikqhh")))

