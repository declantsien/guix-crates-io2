(define-module (crates-io fa rb farbfeld) #:use-module (crates-io))

(define-public crate-farbfeld-0.2.0 (c (n "farbfeld") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "08kzx7a03vds3q5nxn59jgssfqqagb7mjszq84iaplj6alvfff4j")))

(define-public crate-farbfeld-0.3.1 (c (n "farbfeld") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "0zxrkmpfr59rbfgnsj9395ib2a0575190awlzhcb90m0kz50kxhf")))

