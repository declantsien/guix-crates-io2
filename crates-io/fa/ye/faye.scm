(define-module (crates-io fa ye faye) #:use-module (crates-io))

(define-public crate-faye-0.0.1 (c (n "faye") (v "0.0.1") (h "1x9jakgz9sm5xwj2kccdi7bmfak33n2bbjjmqda4c6gjwq59xj6b")))

(define-public crate-faye-0.1.0 (c (n "faye") (v "0.1.0") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "187ahyks157ryrx90s3i3ssiaazzbki734xzka36vl5rbnxk50ll")))

(define-public crate-faye-0.1.1 (c (n "faye") (v "0.1.1") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "0lrc0yy5pzzgm73bfbidfn18xi0yfk56wrj7ybq8m7xrzgs0pnfh")))

(define-public crate-faye-0.1.2 (c (n "faye") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "13vpqcy4a4k6nn2av4dhbfm0g8xxxap08zra18drc65sifikzpf3")))

(define-public crate-faye-0.1.3 (c (n "faye") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "1sbzszz5xv16hrq6yi92zrw97j1fmslcs1nmhbh5rqlablbvxrwj")))

(define-public crate-faye-0.2.0 (c (n "faye") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "0qy792c86g0y2vx7qvz3mifrj6n29bzasrgsqbkwc3p48d1in8f3")))

(define-public crate-faye-0.2.2 (c (n "faye") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "0ryr0z3mgr9z2dgki790zsy3ymbjhm9q6xq866pmgs9630adfhy0")))

(define-public crate-faye-0.3.0 (c (n "faye") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "1lcnr58b972hl4kzqx8raj687hmpfb0qfxdmvvl6628mkcb4jz7m")))

(define-public crate-faye-0.3.1 (c (n "faye") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (o #t) (d #t) (k 0)))) (h "1grafwcszbrkxnvxqxxx5wlblhzmackmzbnq3357spxavgsdza05") (f (quote (("repl" "rustyline") ("default" "cli") ("cli" "clap" "repl")))) (s 2) (e (quote (("rustyline" "dep:rustyline") ("clap" "dep:clap"))))))

(define-public crate-faye-0.3.2 (c (n "faye") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (o #t) (d #t) (k 0)))) (h "0yis3ndd11n8hn3kbbx41mn6b5qwqmxk3svxm3llnhkpcikhd1v6") (s 2) (e (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.3.3 (c (n "faye") (v "0.3.3") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (o #t) (d #t) (k 0)))) (h "1agp26w2n7i5pawcr26jibcsia5fs63gzs2b1169viixwb5hkz3d") (s 2) (e (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.4.0 (c (n "faye") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (o #t) (d #t) (k 0)))) (h "0rpw2nvxhx0rw57qnah6ai2x8xrf5dv787208f2f67wxm8cr8zxs") (s 2) (e (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.4.1 (c (n "faye") (v "0.4.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (o #t) (d #t) (k 0)))) (h "0dv2z85axx6whp4ijs01599lz0ids7pdn9b3dcpj80lad932i6g4") (s 2) (e (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.4.2 (c (n "faye") (v "0.4.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (o #t) (d #t) (k 0)))) (h "0cgk1xxv330znl22v5wv1ppm40lpzskf1c0a5vypaaw15gn7rsqd") (s 2) (e (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.5.0 (c (n "faye") (v "0.5.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (o #t) (d #t) (k 0)))) (h "01f244dpsnhamsrlxvm1a3q1n8i7nnagyhiw25kv0k70gcgvk68x") (s 2) (e (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.5.1 (c (n "faye") (v "0.5.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (o #t) (d #t) (k 0)))) (h "12r4igpa8vn0ql8yb00vmhb6zy1m1y0nxrd1j7s8c35yqhj08hvm") (s 2) (e (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.5.2 (c (n "faye") (v "0.5.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "faye-lsp") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "pomprt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "133pgggdcrncj5phx92drqp0x41dy642mj0bwvhs89cnifzwf4lv") (s 2) (e (quote (("lsp" "dep:faye-lsp" "dep:tokio") ("default" "dep:clap" "dep:pomprt"))))))

(define-public crate-faye-0.6.0 (c (n "faye") (v "0.6.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "faye-lsp") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "pomprt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0xrc54sfkm8ylfw1rl523kf784gb310dgaz9h74sf717q3881m7h") (s 2) (e (quote (("lsp" "dep:faye-lsp" "dep:tokio") ("default" "dep:clap" "dep:pomprt"))))))

