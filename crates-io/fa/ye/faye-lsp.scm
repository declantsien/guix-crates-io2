(define-module (crates-io fa ye faye-lsp) #:use-module (crates-io))

(define-public crate-faye-lsp-0.1.0 (c (n "faye-lsp") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "faye") (r "^0.3.2") (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.20.0") (d #t) (k 0)))) (h "1hw8y0zpbibalkdryr4cj7q0mqgb8l2mrg908m5cb46v9hzprjl2")))

(define-public crate-faye-lsp-0.2.0 (c (n "faye-lsp") (v "0.2.0") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "faye") (r "^0.5.1") (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.20.0") (d #t) (k 0)))) (h "1va7zx1pv8s9q4z0dq75k3dz1jd9fs8kvcczdfr7bj481l6y4nc8")))

