(define-module (crates-io fa wl fawlty) #:use-module (crates-io))

(define-public crate-fawlty-0.0.0 (c (n "fawlty") (v "0.0.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "0wvgmpsz41m52363nvqmjybcvvfr5j24b5g3dj66przbc2gbr6hs")))

