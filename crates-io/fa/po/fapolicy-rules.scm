(define-module (crates-io fa po fapolicy-rules) #:use-module (crates-io))

(define-public crate-fapolicy-rules-0.4.0 (c (n "fapolicy-rules") (v "0.4.0") (d (list (d (n "nom") (r "^6.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y8dhzdhpq5f4p0bin3gbs3x3pgb4lprzspc5fcc8dw5g2h70gy8")))

(define-public crate-fapolicy-rules-0.4.1 (c (n "fapolicy-rules") (v "0.4.1") (d (list (d (n "nom") (r "^6.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0slm828il5b46438qhj1180s43vw25mvz991a7d9vb61ypgrhx7z")))

