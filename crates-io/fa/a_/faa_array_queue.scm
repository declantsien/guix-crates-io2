(define-module (crates-io fa a_ faa_array_queue) #:use-module (crates-io))

(define-public crate-faa_array_queue-0.1.0 (c (n "faa_array_queue") (v "0.1.0") (d (list (d (n "array-macro") (r "^2.0") (d #t) (k 0)) (d (n "peril") (r "^0.4") (d #t) (k 0)))) (h "04zzgjw0fknlwf2c1vzx31vd1rrwhsapkgr3gld0k3lysycv4pqn")))

(define-public crate-faa_array_queue-0.1.1 (c (n "faa_array_queue") (v "0.1.1") (d (list (d (n "array-macro") (r "^2.0") (d #t) (k 0)) (d (n "peril") (r "^0.4") (d #t) (k 0)))) (h "1ls8cpnqqicc4kxa3jivl2jhna1ph0ia4wyhwz50i2w687j8h094")))

(define-public crate-faa_array_queue-0.1.2 (c (n "faa_array_queue") (v "0.1.2") (d (list (d (n "array-macro") (r "^2.0") (d #t) (k 0)) (d (n "peril") (r "^0.4") (d #t) (k 0)))) (h "1qkk6gaknpnkv10k4bm7gxm0wa55r93xgvgzrkxf6m0di4pxbc9d")))

