(define-module (crates-io fa ta fatal) #:use-module (crates-io))

(define-public crate-fatal-0.1.0 (c (n "fatal") (v "0.1.0") (d (list (d (n "termcolor") (r "^1.1") (o #t) (d #t) (k 0)))) (h "13c70n21j1ridf5y16820mdaqcyyha3mqvzm3l5hqpyib31svyjb") (f (quote (("default" "color") ("color" "termcolor")))) (y #t)))

(define-public crate-fatal-0.1.1 (c (n "fatal") (v "0.1.1") (d (list (d (n "termcolor") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1k84w1998592mk1w62pwrkn3lswcbkid2rqw6j15m2x30q1sr48l") (f (quote (("default" "color") ("color" "termcolor"))))))

