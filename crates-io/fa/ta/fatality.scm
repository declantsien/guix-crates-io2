(define-module (crates-io fa ta fatality) #:use-module (crates-io))

(define-public crate-fatality-0.0.0 (c (n "fatality") (v "0.0.0") (h "1773wlphmxwi4llil0yhqlbv9mvz1c06i2apgnisxhmjrxlfx57b")))

(define-public crate-fatality-0.0.1 (c (n "fatality") (v "0.0.1") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "fatality-proc-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0sb96ps1bwz3gj82xx6s2rz5fqqqm2ghla02wblpv56bqqgz9q3v")))

(define-public crate-fatality-0.0.2 (c (n "fatality") (v "0.0.2") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "fatality-proc-macro") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0cjx1b5qzxz89a319lj0y5xqfrvfl2za28xfy3pa9rz8id64fmn0") (f (quote (("expand" "fatality-proc-macro/expand") ("default"))))))

(define-public crate-fatality-0.0.3 (c (n "fatality") (v "0.0.3") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "fatality-proc-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "182k7mb1pzg79djz79gc521ydpyzfhlz39lm9lawggsyz8h2b0kp") (f (quote (("expand" "fatality-proc-macro/expand") ("default"))))))

(define-public crate-fatality-0.0.4 (c (n "fatality") (v "0.0.4") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "fatality-proc-macro") (r "^0.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "18caysnj0wlzs501587j1v239m3jzwlbhycdgac484yvx28m4rsq") (f (quote (("expand" "fatality-proc-macro/expand") ("default"))))))

(define-public crate-fatality-0.0.5 (c (n "fatality") (v "0.0.5") (d (list (d (n "fatality-proc-macro") (r "^0.0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02vg4b8wmy81jcdmlwrql5nhy652chfz817cmwzn2lfwhk55pdkv") (f (quote (("expand" "fatality-proc-macro/expand") ("default"))))))

(define-public crate-fatality-0.0.6 (c (n "fatality") (v "0.0.6") (d (list (d (n "fatality-proc-macro") (r "^0.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1arr1mhk67vgzc844xas7nid77kf2ghay9v34x3d1c2350b7bn1a") (f (quote (("expand" "fatality-proc-macro/expand") ("default"))))))

(define-public crate-fatality-0.1.0 (c (n "fatality") (v "0.1.0") (d (list (d (n "fatality-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0n2l6s44h80c7m1bln0dh6gx4qac65plzxdr7p14zdsds0rxrv31") (f (quote (("expand" "fatality-proc-macro/expand") ("default"))))))

(define-public crate-fatality-0.1.1 (c (n "fatality") (v "0.1.1") (d (list (d (n "fatality-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06fhcdipn0h8vn57h2p3aib2njbd2a4p2a41c665dw7p3x2q4vzc") (f (quote (("expand" "fatality-proc-macro/expand") ("default"))))))

