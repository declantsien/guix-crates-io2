(define-module (crates-io fa bl fable_format) #:use-module (crates-io))

(define-public crate-fable_format-0.1.0 (c (n "fable_format") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "18mp7r8k2zcarxjc7iwg4zfliyg78didqc02jqh6wfak807afz2k")))

(define-public crate-fable_format-0.1.1 (c (n "fable_format") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "0ngn75hqz623svb0lych49j0lqv7m3hjm8lwand484i052axhq8z")))

