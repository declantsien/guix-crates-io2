(define-module (crates-io fa bl fable_data) #:use-module (crates-io))

(define-public crate-fable_data-0.1.1 (c (n "fable_data") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "00nlg3rsynfzb5xn4jmb4dly7q4arrd140q0qqxhhj7r8j0qa0mm")))

