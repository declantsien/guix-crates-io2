(define-module (crates-io fa cs facsimile) #:use-module (crates-io))

(define-public crate-facsimile-0.0.1 (c (n "facsimile") (v "0.0.1") (h "0y92v2dhnc5wzyasp3vsj8mh7rghml1m19d1wvwz834nl2nr0rym")))

(define-public crate-facsimile-0.0.2 (c (n "facsimile") (v "0.0.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (f (quote ("gif" "jpeg" "ico" "png" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt" "dds" "farbfeld"))) (k 0)) (d (n "imageproc") (r "^0.23.0") (k 0)))) (h "193m74fxbk05q7akl6yhzxpwrkvh93ax689wa8jzi4kqn40gmx0h") (f (quote (("gen") ("det") ("default" "det" "gen" "crp") ("crp"))))))

(define-public crate-facsimile-0.0.3 (c (n "facsimile") (v "0.0.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (f (quote ("gif" "jpeg" "ico" "png" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt" "dds" "farbfeld"))) (k 0)) (d (n "imageproc") (r "^0.23.0") (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (o #t) (d #t) (k 0)))) (h "18kcx8n63k0rgbm2f3cy4rmr1cvx2529df8jbrx8948f541s814z") (f (quote (("no_wasm" "det" "gen" "crp") ("gen") ("det") ("default" "wasm") ("crp")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen" "det" "gen" "crp"))))))

