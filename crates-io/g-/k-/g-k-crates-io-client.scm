(define-module (crates-io g- k- g-k-crates-io-client) #:use-module (crates-io))

(define-public crate-g-k-crates-io-client-0.1.0 (c (n "g-k-crates-io-client") (v "0.1.0") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)))) (h "0366s94whxwdpnvii0vgwhx04d9a5vminklkrdjrql4g9z2yimy5")))

(define-public crate-g-k-crates-io-client-0.4.0 (c (n "g-k-crates-io-client") (v "0.4.0") (d (list (d (n "curl") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1vdlbn1z755l5wiqnwhnyjn5416zb80d41ma9drsbda3fgqmsm47")))

(define-public crate-g-k-crates-io-client-0.8.0 (c (n "g-k-crates-io-client") (v "0.8.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "182xq61d3x301wqmpylaj7zqxvxn6vhjyxqmvm9yk4i19w4jxfac")))

(define-public crate-g-k-crates-io-client-0.8.1 (c (n "g-k-crates-io-client") (v "0.8.1") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1slkn24hnz103cxhzl4027pr3ir6k2x5qaarz57iqggaxsr3ynin")))

(define-public crate-g-k-crates-io-client-0.9.0 (c (n "g-k-crates-io-client") (v "0.9.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0i5i8vlprnkm6jh37wnr3c3mmmp6khh62pvbc6df0v9yrwza55sh")))

(define-public crate-g-k-crates-io-client-0.16.0 (c (n "g-k-crates-io-client") (v "0.16.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1y0b5vk24wg949dqx2m9akn61jd2zg9d2ba687m9rprmly7mjjx0")))

(define-public crate-g-k-crates-io-client-0.27.0 (c (n "g-k-crates-io-client") (v "0.27.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1iyszkmfamxm5ws3ymnlrr423im0d8cwbf98gls2z9hrppsrfawm")))

(define-public crate-g-k-crates-io-client-0.27.1 (c (n "g-k-crates-io-client") (v "0.27.1") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1i82qgxgkzq7ixx9532qia31lbv8hr5j41882bh0xbwmjpg3hhfv")))

(define-public crate-g-k-crates-io-client-0.40.1 (c (n "g-k-crates-io-client") (v "0.40.1") (d (list (d (n "curl") (r "^0.4.46") (d #t) (k 0)) (d (n "openssl") (r "^0.10.57") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "00brpxbklvnblvy9fdj22d94j5q58bg36dyns4ywhmhg7jx6590d")))

