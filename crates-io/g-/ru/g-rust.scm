(define-module (crates-io g- ru g-rust) #:use-module (crates-io))

(define-public crate-g-rust-0.0.1-beta.1 (c (n "g-rust") (v "0.0.1-beta.1") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "packetvar-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parser-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0xpq34bp8s2xv898h9h6qpfi2ng3425v4cryla9fiya4ckhv3bcc")))

(define-public crate-g-rust-0.0.1-beta.2 (c (n "g-rust") (v "0.0.1-beta.2") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "packetvar-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parser-derive") (r "^0.1.0") (d #t) (k 0)))) (h "18inasvxcqnkxpirw6kz6cm7rb24aw7ymcqxgzg39zfmim7x8msz")))

(define-public crate-g-rust-0.0.1-beta.3 (c (n "g-rust") (v "0.0.1-beta.3") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "packetvar-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parser-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1w77jdqiilld2v2wb5gx4jw6p3w40ayvwhw59raba835mwv54z79")))

(define-public crate-g-rust-0.0.1-beta.4 (c (n "g-rust") (v "0.0.1-beta.4") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "packetvar-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parser-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0zg20xi3bzrzmb700x5rnmsafjdgd6fgaai7cc6ngn7fq0a5ikgy")))

(define-public crate-g-rust-0.0.1-beta.5 (c (n "g-rust") (v "0.0.1-beta.5") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "packetvar-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parser-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0hd1rbz51dqvaqy62c5wx14qr5km0rgainqb6chrbws5d116qjlj")))

(define-public crate-g-rust-0.0.1-beta.6 (c (n "g-rust") (v "0.0.1-beta.6") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "packetvar-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parser-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1lx4i3xfqy2jnrvhpgcqbn0814107ga4v6pp1rk10dmww908hp37")))

(define-public crate-g-rust-0.0.1-beta.7 (c (n "g-rust") (v "0.0.1-beta.7") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "packetvar-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parser-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1pmc43m5f10ivj4hk2m1y1gfy1gajgdw9aq1dnwl6xdr7y48mkfw")))

(define-public crate-g-rust-0.0.1-beta.8 (c (n "g-rust") (v "0.0.1-beta.8") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "packetvar-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parser-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0csszj27bi3pm9aw1b34qqnd4vrmbs6z58jn2z6ls27qr98vvlcq")))

