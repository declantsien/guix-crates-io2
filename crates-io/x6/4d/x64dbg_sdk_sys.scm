(define-module (crates-io x6 #{4d}# x64dbg_sdk_sys) #:use-module (crates-io))

(define-public crate-x64dbg_sdk_sys-0.1.0 (c (n "x64dbg_sdk_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 1)))) (h "0rdq9vvpggqz2kqbbskdrw9crdb4if3mfnqv2m33l3gh8667jl1s") (f (quote (("x86") ("x64") ("default" "x64"))))))

