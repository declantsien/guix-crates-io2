(define-module (crates-io x6 #{4_}# x64_static_linker) #:use-module (crates-io))

(define-public crate-x64_static_linker-0.1.36 (c (n "x64_static_linker") (v "0.1.36") (d (list (d (n "elf-utilities") (r "^0.1.48") (d #t) (k 0)))) (h "12ycvbf1633pa0ksf0ylp663807igivndvscba3d6csqgl5z84n5")))

(define-public crate-x64_static_linker-0.1.37 (c (n "x64_static_linker") (v "0.1.37") (d (list (d (n "elf-utilities") (r "^0.1.48") (d #t) (k 0)))) (h "1l9dx1cwff5c5ynrkr963j69pa76kgdzzni0llx6y8pb4gi455yw")))

(define-public crate-x64_static_linker-0.1.38 (c (n "x64_static_linker") (v "0.1.38") (d (list (d (n "elf-utilities") (r "^0.1.48") (d #t) (k 0)))) (h "0yjwy48s1hpcsh4xgl3p6wvpbrxcrd383xf8kvkszpspjhipz3b8")))

(define-public crate-x64_static_linker-0.1.39 (c (n "x64_static_linker") (v "0.1.39") (d (list (d (n "elf-utilities") (r "^0.1.48") (d #t) (k 0)))) (h "033gmdnxmv16gjlpn0134krpapk4fmvvp57j5lpmafk1qc4j84wp")))

(define-public crate-x64_static_linker-0.1.40 (c (n "x64_static_linker") (v "0.1.40") (d (list (d (n "elf-utilities") (r "^0.1.49") (d #t) (k 0)))) (h "0bakvjj8ghw5j443h7afj6gb6g9xfz2vlg5whb5xmfcckf7kdxzv")))

(define-public crate-x64_static_linker-0.1.41 (c (n "x64_static_linker") (v "0.1.41") (d (list (d (n "elf-utilities") (r "^0.1.51") (d #t) (k 0)))) (h "0wc45npsg93qwbbjy5j7n8f8ljpla0aid56sf78mqlxjh6zssaa7")))

(define-public crate-x64_static_linker-0.1.42 (c (n "x64_static_linker") (v "0.1.42") (d (list (d (n "elf-utilities") (r "^0.1.56") (d #t) (k 0)))) (h "0mlywr2ciq557m8p06xm6p5ffwsg2c53958nlanfjh1mzwgyhzgj")))

(define-public crate-x64_static_linker-0.1.43 (c (n "x64_static_linker") (v "0.1.43") (d (list (d (n "elf-utilities") (r "^0.1.73") (d #t) (k 0)))) (h "1xh5jsnk5ki52n25vsim0mphsrziycvcyl1b4qv9sww334r17jm0")))

(define-public crate-x64_static_linker-0.1.44 (c (n "x64_static_linker") (v "0.1.44") (d (list (d (n "elf-utilities") (r "^0.1.73") (d #t) (k 0)))) (h "0lrkw6b2bx13gjaiwi31cwvc0hrf7mfpdin3pr9jyhy1pk2g3pg9")))

