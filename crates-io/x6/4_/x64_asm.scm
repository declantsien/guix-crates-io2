(define-module (crates-io x6 #{4_}# x64_asm) #:use-module (crates-io))

(define-public crate-x64_asm-0.1.0 (c (n "x64_asm") (v "0.1.0") (h "0hjwgrcqbq85kawkqz8byf3rlh4jh5nihlp7vwgh9gscwa61kis6")))

(define-public crate-x64_asm-0.1.1 (c (n "x64_asm") (v "0.1.1") (h "0y3fqd8l0scqcly0kkshwb5w727pwv2d66qlls5i6sfqw8ni612h")))

(define-public crate-x64_asm-0.1.2 (c (n "x64_asm") (v "0.1.2") (h "11hqqmcrah23jqfpr78g94dy1cl0dfcjj5ajpl98s8kfrz4nrzbx")))

(define-public crate-x64_asm-0.1.4 (c (n "x64_asm") (v "0.1.4") (h "16i0040szwd0qq2y4m0dzikrk4d8jd275wi7xdrr0crfpw9z1l2q")))

(define-public crate-x64_asm-0.1.5 (c (n "x64_asm") (v "0.1.5") (h "0jmir737vq4kd68zvh452sz8fkl2p7hbgzv3xa02axmnq2amq0j2")))

(define-public crate-x64_asm-0.1.6 (c (n "x64_asm") (v "0.1.6") (h "1225kyzmjfb7qmsihq9giyhzw3h4cpfhxwkz7nzw81crcnzal8r4")))

(define-public crate-x64_asm-0.1.7 (c (n "x64_asm") (v "0.1.7") (h "0rslrwdmh7izdpfjxagp6vaqg1w5ddw3r83a900as60zjyamqrhr")))

(define-public crate-x64_asm-0.1.8 (c (n "x64_asm") (v "0.1.8") (h "0x67hs1fp9nxbfm0ql3fi5jxlxj32ac40vvw7pqaxhp29smjq594")))

(define-public crate-x64_asm-0.1.10 (c (n "x64_asm") (v "0.1.10") (h "1kgyrb3gdl16v9z8carwlgsqxljx3jadw429c8fsf58vfvbnr229")))

(define-public crate-x64_asm-0.1.11 (c (n "x64_asm") (v "0.1.11") (h "03jp151d1mx11hv38xkpc7xsazv5kyh3pz9ckjb2d6a326swda0s")))

(define-public crate-x64_asm-0.1.12 (c (n "x64_asm") (v "0.1.12") (h "1mgw6f1y61zsc682gz3yqqrassgibz3n6y39jk9ndpv8wlahzsls")))

(define-public crate-x64_asm-0.1.13 (c (n "x64_asm") (v "0.1.13") (h "0qfd2vnywlcw88zfd31k5xy6qn8fjq4k9fg4si5zb32aqsbawki1")))

(define-public crate-x64_asm-0.1.14 (c (n "x64_asm") (v "0.1.14") (h "084zif6vsl1gslna72q8x0v0islnpl3j48851km19f13b5725sc6")))

(define-public crate-x64_asm-0.1.15 (c (n "x64_asm") (v "0.1.15") (h "0wnr0fbsa2xqd2dhw52jjcqh82w70y0h22wchds2h983l1xs5ca2")))

(define-public crate-x64_asm-0.1.16 (c (n "x64_asm") (v "0.1.16") (h "149424152dbdssnnc6y44cs7vdhdlrr4lqq9438k99hc12bby303")))

(define-public crate-x64_asm-0.1.17 (c (n "x64_asm") (v "0.1.17") (h "14hlxv0yqkipqb0y6mabd9b00845xq9r1dkdv7q9nbb2fk0yn1g4")))

(define-public crate-x64_asm-0.1.18 (c (n "x64_asm") (v "0.1.18") (h "0ghr6am80gg64vzssphwqdi2arifb0szi396m3826gv8g1q4g06q")))

(define-public crate-x64_asm-0.1.19 (c (n "x64_asm") (v "0.1.19") (h "1734n447vgic3kckx046c5jshj57crhxj5fdj8qsk7zxvg6s6b50")))

(define-public crate-x64_asm-0.1.20 (c (n "x64_asm") (v "0.1.20") (h "0dmg4v1fjn614jc5vlqg34xwbn3ay3f745q8lskakvjdhg9xn8yr")))

(define-public crate-x64_asm-0.1.21 (c (n "x64_asm") (v "0.1.21") (d (list (d (n "elf-utilities") (r "^0.1.47") (d #t) (k 0)))) (h "1zpi883zlqmq3x9b3i5vvsn78lxgjhw8rvjjxx72yyc21gz7iybg")))

(define-public crate-x64_asm-0.1.22 (c (n "x64_asm") (v "0.1.22") (d (list (d (n "elf-utilities") (r "^0.1.47") (d #t) (k 0)))) (h "010gxhv9qf4554wiw051lgk3h2lgm9vz0nw863s6vrpli7i7hnzi")))

(define-public crate-x64_asm-0.1.23 (c (n "x64_asm") (v "0.1.23") (d (list (d (n "elf-utilities") (r "^0.1.47") (d #t) (k 0)))) (h "0cqm5csgf0ygm3q7s22mkrpbww4i1qphb57zbwpciygsd7j9dmmk")))

(define-public crate-x64_asm-0.1.24 (c (n "x64_asm") (v "0.1.24") (d (list (d (n "elf-utilities") (r "^0.1.47") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)))) (h "0nrdisj0aiiyghyxzs245vmxj6nm63h9zhn80lxy2dxn0j7qsrlk")))

(define-public crate-x64_asm-0.1.25 (c (n "x64_asm") (v "0.1.25") (d (list (d (n "elf-utilities") (r "^0.1.47") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)))) (h "158gxijq43y2vm2bjy697i3r53jx8ylapg1gh7jlz9fhq9dvankn")))

(define-public crate-x64_asm-0.1.26 (c (n "x64_asm") (v "0.1.26") (d (list (d (n "elf-utilities") (r "^0.1.47") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)))) (h "1nz6wziz17zjsplv8lzfilri4d99k23s69mcyvzcp2qlnl4m0ryk")))

(define-public crate-x64_asm-0.1.27 (c (n "x64_asm") (v "0.1.27") (d (list (d (n "elf-utilities") (r "^0.1.47") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)))) (h "0q81x87kpwryb8ibfznjmjyx9psqfdlca6d5ix0wzyrrin5ah29x")))

(define-public crate-x64_asm-0.1.28 (c (n "x64_asm") (v "0.1.28") (d (list (d (n "elf-utilities") (r "^0.1.47") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)))) (h "1cp6n2197xvlflmnz1hg98dsqb54maxg4yzcx6fx7n5rvpwaa4br")))

(define-public crate-x64_asm-0.1.29 (c (n "x64_asm") (v "0.1.29") (d (list (d (n "elf-utilities") (r "^0.1.48") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)))) (h "1cli7bn5kqv9fdqf89q4w7yim6bjv2yhah97vm7nvnngxx7mlp8a")))

(define-public crate-x64_asm-0.1.30 (c (n "x64_asm") (v "0.1.30") (d (list (d (n "elf-utilities") (r "^0.1.48") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)))) (h "0bdhsiqscf5cix0mafmzv6mxsf63hxv0y7wqc10k6i9mrp4ijhsq")))

(define-public crate-x64_asm-0.1.31 (c (n "x64_asm") (v "0.1.31") (d (list (d (n "elf-utilities") (r "^0.1.48") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)))) (h "0s6bpr3vzhn26z8ywr9arrv3hznbynbjkhgaf45d8cyybk49n3p5")))

(define-public crate-x64_asm-0.1.32 (c (n "x64_asm") (v "0.1.32") (d (list (d (n "elf-utilities") (r "^0.1.48") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)))) (h "04jbwh2qq5p2sqly0zc3s7jsk04f18pbilpmassmpr2yzy7wjaqa")))

(define-public crate-x64_asm-0.1.33 (c (n "x64_asm") (v "0.1.33") (d (list (d (n "elf-utilities") (r "^0.1.48") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)))) (h "17i9izx35g67g9qryb8yakakai1f8d2byd2w82l6hsn996f71jvl")))

(define-public crate-x64_asm-0.1.34 (c (n "x64_asm") (v "0.1.34") (d (list (d (n "elf-utilities") (r "^0.1.48") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)))) (h "0hhb9r9z2rzkn77jz2a2cmxxl296idc0l36im7al80xws2b918bi")))

(define-public crate-x64_asm-0.1.35 (c (n "x64_asm") (v "0.1.35") (d (list (d (n "elf-utilities") (r "^0.1.48") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)))) (h "0r282p0i165jzddzjp2irnnzd1rllvlyhxj6y29ajrjqbnm7632l")))

(define-public crate-x64_asm-0.1.36 (c (n "x64_asm") (v "0.1.36") (d (list (d (n "elf-utilities") (r "^0.1.48") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)))) (h "1j8v6z1z0bqlrakhwcsr53cl66q5iiwxicgcaxpyl55k3n8waw2i")))

(define-public crate-x64_asm-0.1.37 (c (n "x64_asm") (v "0.1.37") (d (list (d (n "elf-utilities") (r "^0.1.48") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)))) (h "16lgsw4x3m7a9cd1rbjacy0zh2bhzbin0r1pa4qgimkx4v4h3jnd")))

(define-public crate-x64_asm-0.1.38 (c (n "x64_asm") (v "0.1.38") (d (list (d (n "elf-utilities") (r "^0.1.50") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)))) (h "1i1j019xkvrhwkg2p53n8qqzfj66jbjlvv1gvcxdkxzrjp13qv4r")))

(define-public crate-x64_asm-0.1.39 (c (n "x64_asm") (v "0.1.39") (d (list (d (n "elf-utilities") (r "^0.1.50") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)))) (h "07rcryqq3w7j79gmn3bs9wfrz0byckww0alkipz0c7iackcs0iys")))

(define-public crate-x64_asm-0.1.40 (c (n "x64_asm") (v "0.1.40") (d (list (d (n "elf-utilities") (r "^0.1.50") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)))) (h "0xvayld05wschayicaf2iadl489nlwsvm8r3nbrx2w9g4jmcns2b")))

(define-public crate-x64_asm-0.1.41 (c (n "x64_asm") (v "0.1.41") (d (list (d (n "elf-utilities") (r "^0.1.54") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)))) (h "196hk4dyps4531x0lb447f4xi5nr33922lafybbdh1a2dqdhk959")))

(define-public crate-x64_asm-0.1.43 (c (n "x64_asm") (v "0.1.43") (d (list (d (n "elf-utilities") (r "^0.1.54") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)))) (h "042v92zwxhsdjihqwbr26lb3aqq120cjfjm2ql0y4aikl82jpfd3")))

(define-public crate-x64_asm-0.1.44 (c (n "x64_asm") (v "0.1.44") (d (list (d (n "elf-utilities") (r "^0.1.67") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)))) (h "1biwxsd0l8fydkyhqxzy2wfkbnakazmxlb5f8i86b66iw3dj623j")))

(define-public crate-x64_asm-0.1.45 (c (n "x64_asm") (v "0.1.45") (d (list (d (n "elf-utilities") (r "^0.1.73") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)))) (h "06bm53zbprj1dh6spm1vndmvxki13pjy46far4smqn0vwbsfac4b")))

