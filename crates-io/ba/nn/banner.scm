(define-module (crates-io ba nn banner) #:use-module (crates-io))

(define-public crate-banner-0.0.1 (c (n "banner") (v "0.0.1") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)))) (h "04rbq3ycq2cjr6phrl3ld6nsh7ssbzx5ggdj3gl5p8wpcrf5n17f") (y #t)))

(define-public crate-banner-0.0.2 (c (n "banner") (v "0.0.2") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)))) (h "15yvbjhrns6zg575r7b3xn2ydwr6gjwd53gr6rjs12yfirky1q47")))

(define-public crate-banner-0.0.3 (c (n "banner") (v "0.0.3") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)))) (h "1jh7s7jvb3r65bh957pwa7c032kjrgap70vdfjijswlm1rz4hac6")))

(define-public crate-banner-0.0.4 (c (n "banner") (v "0.0.4") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)))) (h "1q45xmn554z97wb3fbb0ivrc22m51vbybjww3kq04v4ljqyf125w")))

