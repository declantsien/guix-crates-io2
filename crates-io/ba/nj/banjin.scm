(define-module (crates-io ba nj banjin) #:use-module (crates-io))

(define-public crate-banjin-0.1.0 (c (n "banjin") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1czcs7bnk76hsvyj2rk2ws8lbwvak18qjb3mq07a87p6a2qjbz3l") (f (quote (("no_std"))))))

(define-public crate-banjin-0.2.0 (c (n "banjin") (v "0.2.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0kb6f6qfrp97l13cc9ld95as4l3f5j3qs24l7yi9vkimx7pl5sr0") (f (quote (("no_std"))))))

(define-public crate-banjin-0.3.0 (c (n "banjin") (v "0.3.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "04pq9s8x7d1lkay1z1jd7r7d6xy79bar4widl7a1jsm1yfq0j44d") (f (quote (("no_std"))))))

