(define-module (crates-io ba rg barg) #:use-module (crates-io))

(define-public crate-barg-0.0.1 (c (n "barg") (v "0.0.1") (d (list (d (n "fonterator") (r "^0.2.0-pre0") (d #t) (k 0)) (d (n "footile") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 2)))) (h "14vkkv0aj2lc82i43rijp38nc2p4840ri0padmy44gpa0isyx1kh")))

(define-public crate-barg-0.0.2 (c (n "barg") (v "0.0.2") (d (list (d (n "fonterator") (r "^0.2") (d #t) (k 0)) (d (n "footile") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 2)))) (h "0rh1il1kl35s0k42n6drzbj4vdv2v404bnnlx8abk7k7n6qngk89")))

(define-public crate-barg-0.0.3 (c (n "barg") (v "0.0.3") (d (list (d (n "fonterator") (r "^0.3") (d #t) (k 0)) (d (n "footile") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 2)))) (h "0h8pb7sq05palrzl5x6wbfsmdqdzgla65kqvlwx7b1s716ymizrm")))

(define-public crate-barg-0.1.0 (c (n "barg") (v "0.1.0") (d (list (d (n "fonterator") (r "^0.4") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 2)) (d (n "rvg") (r "^0.0.1") (d #t) (k 0)) (d (n "window") (r "^0.1") (d #t) (k 0)))) (h "1h2mi91pgdbpwwda9v2cdxa2bn0bcb8yaw0g7d6ckjvp40flkxg4")))

(define-public crate-barg-0.2.0 (c (n "barg") (v "0.2.0") (d (list (d (n "fonterator") (r "^0.4") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 2)) (d (n "res") (r "^0.4") (d #t) (k 1)) (d (n "rvg") (r "^0.0.2") (f (quote ("footile"))) (d #t) (k 0)) (d (n "window") (r "^0.2") (d #t) (k 0)))) (h "1zjz6d7phcapj1vadnnirvqdx39hfcbcvra6m1m1s3gf5mf7kimd")))

