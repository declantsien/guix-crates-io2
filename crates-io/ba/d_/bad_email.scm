(define-module (crates-io ba d_ bad_email) #:use-module (crates-io))

(define-public crate-bad_email-0.1.0 (c (n "bad_email") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0smfa57im0faqlv8690ki43vi0ipvnpzqzx0b7zrvz5zwn5fpi1x")))

(define-public crate-bad_email-0.1.1 (c (n "bad_email") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1sdhbns4wch7sy3xrnfz0an85vmh14m3lk4gdqa0vhybnq5fqk1w")))

