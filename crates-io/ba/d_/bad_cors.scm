(define-module (crates-io ba d_ bad_cors) #:use-module (crates-io))

(define-public crate-bad_cors-0.1.0 (c (n "bad_cors") (v "0.1.0") (d (list (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "rocket") (r "^0.4.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.4") (d #t) (k 0)))) (h "0535a0z16igahd8zv0ac788390k8y2s5fq5xkcjydhx5maa5c4v3")))

