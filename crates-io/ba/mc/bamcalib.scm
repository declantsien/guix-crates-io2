(define-module (crates-io ba mc bamcalib) #:use-module (crates-io))

(define-public crate-bamcalib-0.1.1 (c (n "bamcalib") (v "0.1.1") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "bigtools") (r "^0.1.11") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "141plbsax8xvqfvjbx3c50ndrn8zlhrx95n8x065fkxach5rh8c8") (y #t)))

(define-public crate-bamcalib-0.1.2 (c (n "bamcalib") (v "0.1.2") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "bigtools") (r "^0.1.11") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1pl6n3id4hdri1zf3n3b1qz1l0fv5hwn73jngy1jq0523lfgwv0g") (y #t)))

(define-public crate-bamcalib-0.1.3 (c (n "bamcalib") (v "0.1.3") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "bigtools") (r "^0.1.11") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "01qywkkjydzl94v0cmfxaw3y1bqnf84climrlzsrjkkkfwn8pim1")))

(define-public crate-bamcalib-0.1.4 (c (n "bamcalib") (v "0.1.4") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "bigtools") (r "^0.1.11") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0ficzix4i7wkwan6g7j2ryxn7z1pv9xjcbrg6vwjd8cahpswc7y8")))

(define-public crate-bamcalib-0.1.5 (c (n "bamcalib") (v "0.1.5") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "bigtools") (r "^0.1.11") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1rajycb791zphsbajsh282nb8z478rgwwrz4k73kdx0kricx1h52")))

