(define-module (crates-io ba al baal) #:use-module (crates-io))

(define-public crate-baal-0.1.0 (c (n "baal") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "portaudio") (r "^0.6.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.0") (d #t) (k 0)))) (h "011639ca8wi6f9qhky93hliv9a7i3vbchy21pj3kw0hb9nn0g805")))

(define-public crate-baal-0.2.0 (c (n "baal") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "portaudio") (r "^0.6.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.0") (d #t) (k 0)))) (h "1djrkmzzb3x4wg0l0dimiv2n3n5bhac57ahhaazb34xg0hr828nd")))

(define-public crate-baal-0.3.0 (c (n "baal") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "portaudio") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "toml") (r "^0.1.0") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.3.0") (d #t) (k 2)))) (h "0hfki79slzwxq08y09r2a7dhi4bplddbn993f3bkxcw5y7qz6kf7")))

(define-public crate-baal-0.4.0 (c (n "baal") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "portaudio") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)))) (h "0zmxd188kmmmyik1g89kqaf9cl32s4j2vbdjph05s00pmdj43q00")))

(define-public crate-baal-0.4.1 (c (n "baal") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "portaudio") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)))) (h "182bl81vdy456maz4fpsvbwl94w3ajbj4ggvkj089cii4fivvcnj")))

(define-public crate-baal-0.4.2 (c (n "baal") (v "0.4.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "portaudio") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)))) (h "12rcgrwib6ng6zb5bdisjjn2jp3fh7mcfyn7l4yxnb8ymj263hqp")))

(define-public crate-baal-0.5.0 (c (n "baal") (v "0.5.0") (d (list (d (n "rodio") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)))) (h "18h3d2hls9w15mgp8d15n1gn8izfjivnyqr38mx8y91922zkgd8p")))

(define-public crate-baal-0.5.1 (c (n "baal") (v "0.5.1") (d (list (d (n "rodio") (r "^0.4.0") (d #t) (k 0)))) (h "1sswqgss0ynfghcn6wvdb1l4pf1l5a0w26xm5q9b06ldjlsz1m18")))

(define-public crate-baal-0.7.0 (c (n "baal") (v "0.7.0") (h "0arcx98949g819lzri11iyi4ywid5dhilvnb8xvmrjr4y7bf0q24")))

