(define-module (crates-io ba sm basm) #:use-module (crates-io))

(define-public crate-basm-0.5.0 (c (n "basm") (v "0.5.0") (d (list (d (n "built") (r "^0.5.1") (f (quote ("chrono"))) (d #t) (k 1)) (d (n "cpclib-asm") (r "^0.5.0") (f (quote ("basm"))) (d #t) (k 0)) (d (n "cpclib-common") (r "^0.5.0") (d #t) (k 0)) (d (n "time") (r "^0.2.27") (d #t) (k 0)))) (h "1ksyy6rchwyyjadhi6qxw5wxc58vkhl98vwfyhly7nhgyrc8g3mp")))

