(define-module (crates-io ba nq banquo) #:use-module (crates-io))

(define-public crate-banquo-0.1.0 (c (n "banquo") (v "0.1.0") (d (list (d (n "banquo-core") (r "^0.1.0") (d #t) (k 0)) (d (n "banquo-hybrid_distance") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "151a7ycxc5xmxs30jyl51ar3p7krczp8j34kapm4invwmawff4cv") (f (quote (("default")))) (s 2) (e (quote (("hybrid-distance" "dep:banquo-hybrid_distance"))))))

