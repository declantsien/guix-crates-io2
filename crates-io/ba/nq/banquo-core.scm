(define-module (crates-io ba nq banquo-core) #:use-module (crates-io))

(define-public crate-banquo-core-0.1.0 (c (n "banquo-core") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1ikka2aiz4a7wr7haz4xx5rlvwzya6wfcailvxzqls23p1n831gi")))

