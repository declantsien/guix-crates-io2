(define-module (crates-io ba nq banquo-hybrid_distance) #:use-module (crates-io))

(define-public crate-banquo-hybrid_distance-0.1.0 (c (n "banquo-hybrid_distance") (v "0.1.0") (d (list (d (n "banquo-core") (r "^0.1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1aznbsjcf711b0qqs331xn9mar772k4h0yyskls623fg525ls9zm")))

