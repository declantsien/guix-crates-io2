(define-module (crates-io ba rs barse-derive) #:use-module (crates-io))

(define-public crate-barse-derive-0.1.0 (c (n "barse-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "13gwicv27n014qya5vbsg29mpsqn1mhwc78kjwsmj1n4d35dxx9z")))

(define-public crate-barse-derive-0.1.1 (c (n "barse-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1w3pkryy5429wyv7rcspvadapagaha24mki1mpdh0h29rl5ig50p")))

(define-public crate-barse-derive-0.2.0 (c (n "barse-derive") (v "0.2.0") (d (list (d (n "barse-derive-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)))) (h "1v63kzcpc2zdripf6fcr492an8gl19xjv5c64aibxbqrd8xb32hl")))

(define-public crate-barse-derive-0.3.0 (c (n "barse-derive") (v "0.3.0") (d (list (d (n "barse-derive-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)))) (h "1sic955w7dnqrllcy23l0gfs9240f35zw50h2jk0lrii9h5gqcnq")))

