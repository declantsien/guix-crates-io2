(define-module (crates-io ba rs barse) #:use-module (crates-io))

(define-public crate-barse-0.1.0 (c (n "barse") (v "0.1.0") (d (list (d (n "barse-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0nw2kw4yh01yhhnpgnqm0saaa4gpzc5yj729a6ny0blc4g8v4x1k") (s 2) (e (quote (("derive" "dep:barse-derive"))))))

(define-public crate-barse-0.1.1 (c (n "barse") (v "0.1.1") (d (list (d (n "barse-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0l2cq8k8abgg5jmpgj6q72jpvch89bbf5pzqarqr9k4vlbfxdp3f") (s 2) (e (quote (("derive" "dep:barse-derive"))))))

(define-public crate-barse-0.1.2 (c (n "barse") (v "0.1.2") (d (list (d (n "barse-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0zy2ra8c1cgly9ny8hbia56ra48w4ljylzrzcshnvf4aqp3f68wv") (s 2) (e (quote (("derive" "dep:barse-derive"))))))

(define-public crate-barse-0.2.0 (c (n "barse") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "barse-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1i2852l6q943hcskrpclhmhg1j9jn63g5vmxyljqvg690i1m4gg4") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:barse-derive"))))))

(define-public crate-barse-0.3.0 (c (n "barse") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "barse-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1rd3ws1a8iy75wxp37sz5cjr0k6ap79cs5b8qjq5apf66wkgc2n4") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:barse-derive"))))))

