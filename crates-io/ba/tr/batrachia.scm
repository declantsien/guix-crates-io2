(define-module (crates-io ba tr batrachia) #:use-module (crates-io))

(define-public crate-batrachia-0.1.0 (c (n "batrachia") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 1)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("sync" "rt"))) (d #t) (k 0)))) (h "016ldprpvjac866scc5m97m25qa7g5lp6v8m7wy4ivmahcdhyvib")))

(define-public crate-batrachia-0.1.1 (c (n "batrachia") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("sync" "rt"))) (d #t) (k 0)))) (h "0bzq79n9xgfbidyhszv8v44acchnzk0aswk3xjwdrd18651gjciw")))

