(define-module (crates-io ba dl badlog) #:use-module (crates-io))

(define-public crate-badlog-0.1.0 (c (n "badlog") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "144r4vy3a0zr7k62i74jga14i2dsln7p3psafk3vfarpqkmhwcs6")))

(define-public crate-badlog-0.1.1 (c (n "badlog") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1nn92jz3gy15njqs32wcd9457l5ibvw6xhjdqwydxxwy9q8jz9nw")))

(define-public crate-badlog-0.1.2 (c (n "badlog") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0rfyfxcp56rbsqknn3ycx2b450d2sh9kgyrpxf1wa0g4zxpxhgh5")))

(define-public crate-badlog-0.1.3 (c (n "badlog") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1421ii7h22axskbs9a84h5b9g1npppp17hz8cvc9a7r78gpqiwmz")))

(define-public crate-badlog-0.2.3 (c (n "badlog") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0ra5mgv4gv4s2w7pr5mgfjmw61k2pr6w2z5ia3lj5vpsdiwh6m12")))

(define-public crate-badlog-0.2.4 (c (n "badlog") (v "0.2.4") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "02k4d5i7hcy1rhrdwwsm6j307jhms41i8zpzwj9jcb0nbw7aywnn")))

(define-public crate-badlog-0.2.5 (c (n "badlog") (v "0.2.5") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1hq7zgiwwnzlc7bnwz4k3x4p6dg83ji0sk0g3jml5bh5avywfv3l")))

(define-public crate-badlog-0.3.0 (c (n "badlog") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0cwzq0kfmm25n88afwnpqwlwp5mjm12r620fwm8yqm9212dqwffx")))

(define-public crate-badlog-0.3.1 (c (n "badlog") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "09gvlqkkvs9bv346xjh2vf5rkkkgyn20xhk4kv7ly6mymzmhflk5")))

(define-public crate-badlog-1.0.0 (c (n "badlog") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1k20hx3452z8d7s0g4fjx412pw4jr0hi6wn6gr6dxfc1f3kpxzv9")))

(define-public crate-badlog-1.1.0 (c (n "badlog") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wr2a7axl98dhx3d20vh1s2q62625la4ks0k4mf7iyy71p4f7xia")))

(define-public crate-badlog-1.1.1 (c (n "badlog") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1whidxawbpy0lib5jka7zyi8x36l5jxvl2lchps9ad8h79d7wcag")))

