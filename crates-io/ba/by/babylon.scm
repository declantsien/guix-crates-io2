(define-module (crates-io ba by babylon) #:use-module (crates-io))

(define-public crate-babylon-0.0.0 (c (n "babylon") (v "0.0.0") (d (list (d (n "js_ffi") (r "^0") (d #t) (k 0)))) (h "00f23i325r9xdiqmcrr0lqadva5s6dxknkb4xzch61krf1kxssw1")))

(define-public crate-babylon-0.0.1 (c (n "babylon") (v "0.0.1") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js_ffi") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0mcc5g06lmfvm1g2k03xzi1z0is5y7r5p93z1ggjsj5mb8s6xy97")))

(define-public crate-babylon-0.0.2 (c (n "babylon") (v "0.0.2") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "062a4p9zqlxfpndc9xryhqz8w1gksnhk75jwxr3hc557ylx5w8b7")))

(define-public crate-babylon-0.0.3 (c (n "babylon") (v "0.0.3") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0nkk0ibdd1ka7dfq499lrm0scpn1fyhdvx0cs05vz3x1kk470v4x")))

(define-public crate-babylon-0.0.4 (c (n "babylon") (v "0.0.4") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "1ck46b1qvw1bgzymk8zjbyg192p9pqbwn0vvzha77kq9838f00ns")))

(define-public crate-babylon-0.0.5 (c (n "babylon") (v "0.0.5") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "1vsiws4vy0i2p0v4h81bxgfd4y0cnh63ingjrg8wlcdk5xbl34rj")))

(define-public crate-babylon-0.0.6 (c (n "babylon") (v "0.0.6") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "web_timer") (r "^0") (d #t) (k 0)))) (h "1k3y06zxzkhcch50aw8hcddx8kk2y5rrfn9qfy82m78fl05z618z")))

(define-public crate-babylon-0.0.7 (c (n "babylon") (v "0.0.7") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "web_timer") (r "^0") (d #t) (k 0)))) (h "1spi5g2lz6krzq9ja6c82p2lakmd06qf5fi8n1pxlj5ix84f1ak3")))

(define-public crate-babylon-0.0.8 (c (n "babylon") (v "0.0.8") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "0jikd8l12xcaljcxpddgjl4qky62qlkm36k9xvv5p2djjz4nbaqz")))

(define-public crate-babylon-0.0.9 (c (n "babylon") (v "0.0.9") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "0xhqz5myl1z55may05d6k9db2l3p6yxk6nj3hirfnd7268bghy8a")))

(define-public crate-babylon-0.0.11 (c (n "babylon") (v "0.0.11") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "1mjy9yvprky9cg5j4g7q2lxglmg3bym5y3r3kf4dj4xbl3bp78qs")))

(define-public crate-babylon-0.0.12 (c (n "babylon") (v "0.0.12") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "00ifygi064f90r0s47jws4xrzjsfz6vi6zkv08mmm7rwbkw2ckqd")))

(define-public crate-babylon-0.0.13 (c (n "babylon") (v "0.0.13") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "03sj42s7v97v2j3rrrralxv20xc7bdcbj5cbpkxzbj2b4xvhzhvp")))

(define-public crate-babylon-0.0.14 (c (n "babylon") (v "0.0.14") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "0y7jy97hqdjkklhrkap5a1588hi825zv5zy6y986698i546p3x3d")))

(define-public crate-babylon-0.0.15 (c (n "babylon") (v "0.0.15") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "1mjqxd4is9cdjxvipvbj7saj3rmricg0wln5k6aryp28y281db4s")))

