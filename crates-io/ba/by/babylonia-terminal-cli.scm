(define-module (crates-io ba by babylonia-terminal-cli) #:use-module (crates-io))

(define-public crate-babylonia-terminal-cli-0.1.0 (c (n "babylonia-terminal-cli") (v "0.1.0") (d (list (d (n "babylonia-terminal-sdk") (r "^0.1.0") (d #t) (k 0)) (d (n "downloader-for-babylonia-terminal") (r "^0.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wincompatlib") (r "^0.7.4") (d #t) (k 0)))) (h "0wix4s36c3cif84nls8x3npl7wc27nyykhlb903z1vg5bjaiinlg") (y #t)))

