(define-module (crates-io ba by baby-pool) #:use-module (crates-io))

(define-public crate-baby-pool-0.1.0 (c (n "baby-pool") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dispose") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "137fr6x4582qvxgrrhygayy4s0w52bynfvndlpai11zc57024r2i") (y #t)))

(define-public crate-baby-pool-0.2.0 (c (n "baby-pool") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dispose") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "012c9yrg2bmi65lb1s9m4qlgwjykhmymyl01y8431hiy58xqsb9v") (y #t)))

(define-public crate-baby-pool-0.2.1 (c (n "baby-pool") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dispose") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0zj8bswjc86n7fx4l2lw2311v9wc7jfqbfmqgg008s5c0a1qm7nl") (y #t)))

(define-public crate-baby-pool-0.3.0 (c (n "baby-pool") (v "0.3.0") (h "124k7yy7z99zx09bndid3jrryb3m41if66zz4kaqjxxja3p7w8b7")))

