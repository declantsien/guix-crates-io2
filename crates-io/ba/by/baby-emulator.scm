(define-module (crates-io ba by baby-emulator) #:use-module (crates-io))

(define-public crate-baby-emulator-0.1.0 (c (n "baby-emulator") (v "0.1.0") (h "15gaw9n2kxf32x8vw1c7b957krm22agzs28ghmgrnx2xa57g9x19")))

(define-public crate-baby-emulator-0.1.1 (c (n "baby-emulator") (v "0.1.1") (h "1bclxcshgjnxpi3mb259pkkbc2wqfq21fvsbxc1h7j2jz1z4k9ni")))

(define-public crate-baby-emulator-0.1.2 (c (n "baby-emulator") (v "0.1.2") (h "0d4c645bacs2warzrcnm4b1xw1baymq3cjqgjizsi8rh0p1969f9")))

(define-public crate-baby-emulator-0.1.3 (c (n "baby-emulator") (v "0.1.3") (h "0c5rrrpl7snv1wm3pnaqn24awykdaaxdxh0ibli7f51n6nav4dri")))

(define-public crate-baby-emulator-0.1.4 (c (n "baby-emulator") (v "0.1.4") (h "1vpzs451kya6bqflhqy0gw5hz2wrs3yzn9b8jaa9c8353f5syi9r")))

(define-public crate-baby-emulator-0.1.5 (c (n "baby-emulator") (v "0.1.5") (h "034yxyx7lk51sa0c386ajn2pzqbf86bsvq2p06y0fz7i10i60fwp")))

(define-public crate-baby-emulator-0.1.6 (c (n "baby-emulator") (v "0.1.6") (h "0s2zly2p8p15h16b3h2r28vf54h3pij8r6rrvamq13ld2xwzhbvj")))

(define-public crate-baby-emulator-0.1.7 (c (n "baby-emulator") (v "0.1.7") (h "0202f8z8sbnlmzxyzp7lbpcf2b3dw5vib5dwx7xinjrx7ly113s6")))

(define-public crate-baby-emulator-0.2.0 (c (n "baby-emulator") (v "0.2.0") (h "1bb5174wy9bm69w55j6z3x9p7qqr7vz56i8mxgbqzmmmgswfz6ya")))

