(define-module (crates-io ba by babygiant-alt-bn128) #:use-module (crates-io))

(define-public crate-babygiant-alt-bn128-0.1.0 (c (n "babygiant-alt-bn128") (v "0.1.0") (d (list (d (n "ark-ec") (r "^0.2.0") (d #t) (k 0)) (d (n "ark-ed-on-bn254") (r "^0.2.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.2.0") (d #t) (k 0)) (d (n "ark-std") (r "^0.2.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1am02p9prmnii60jm1vs1afskavwrbv9p70038papqfd3n1y4rvb") (r "1.73.0")))

(define-public crate-babygiant-alt-bn128-0.1.1 (c (n "babygiant-alt-bn128") (v "0.1.1") (d (list (d (n "ark-ec") (r "^0.2.0") (d #t) (k 0)) (d (n "ark-ed-on-bn254") (r "^0.2.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.2.0") (d #t) (k 0)) (d (n "ark-std") (r "^0.2.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1bbil9pl9ga4i66rsvg5qb96b23jkgcs4416jky6xaycl1pkzzyh") (r "1.73.0")))

