(define-module (crates-io ba m- bam-builder) #:use-module (crates-io))

(define-public crate-bam-builder-0.1.0 (c (n "bam-builder") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.35.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "02aw9pfx70mqll053ng0sl099c7l04lg47nxa4f4g7ikkskvp529")))

(define-public crate-bam-builder-0.1.1 (c (n "bam-builder") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.35.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1a1s17l9r6swsggkjcjbj3l60kfaqxpmcwlr5hcsd6vmaf74wbm5")))

(define-public crate-bam-builder-0.1.2 (c (n "bam-builder") (v "0.1.2") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.35.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "010zys5bywa0dq5yr09rzbqsy8xcqdkbpi6bppv20lvcwsyj72hk")))

(define-public crate-bam-builder-0.1.3 (c (n "bam-builder") (v "0.1.3") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.35.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0689ygd7d8gz1wp6z3s441xjm5kswfn1qnmpcxjx4dnxbai4gj5p")))

(define-public crate-bam-builder-1.0.0 (c (n "bam-builder") (v "1.0.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.44.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)))) (h "1bszwpm3vmxwi0xr6y75rqxk9mk0cbx1pml5f3wcrbwqmpwfn7vd")))

