(define-module (crates-io ba la balance-tui) #:use-module (crates-io))

(define-public crate-balance-tui-0.1.0 (c (n "balance-tui") (v "0.1.0") (d (list (d (n "arboard") (r "^3.2.0") (k 0)) (d (n "bpaf") (r "^0.7.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "chem-eq") (r "^0.1.1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0cclx903k69cn0mm46p1s513p2a05v33kyf5wg8agcana9f9licd")))

(define-public crate-balance-tui-0.1.1 (c (n "balance-tui") (v "0.1.1") (d (list (d (n "arboard") (r "^3.2.0") (k 0)) (d (n "bpaf") (r "^0.7.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "chem-eq") (r "^0.2.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "180vkz0pzrk2rzizab46dc2gq035nwdjkvlbzhxjm1vmjszb05c0")))

(define-public crate-balance-tui-0.1.2 (c (n "balance-tui") (v "0.1.2") (d (list (d (n "arboard") (r "^3.2.0") (k 0)) (d (n "bpaf") (r "^0.7.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "chem-eq") (r "^0.3.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1z85zqf0k445ghk6b9yqwvnbr9d7iiahf0bc5f1bl2dv641pb19f")))

