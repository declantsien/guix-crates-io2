(define-module (crates-io ba la balanced-tree-index) #:use-module (crates-io))

(define-public crate-balanced-tree-index-1.0.0 (c (n "balanced-tree-index") (v "1.0.0") (d (list (d (n "aligned-cmov") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (k 0)))) (h "0cs25m7gxv5z6250p5265r2j401ph80jmp4mmj4lb360kn2pfj57")))

(define-public crate-balanced-tree-index-2.0.0 (c (n "balanced-tree-index") (v "2.0.0") (d (list (d (n "aligned-cmov") (r "^2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "0a1zkadxg2h02m4nj1jmx9zrzi674gzsdqa3fzkf72q0n93vvmnn")))

(define-public crate-balanced-tree-index-2.2.0 (c (n "balanced-tree-index") (v "2.2.0") (d (list (d (n "aligned-cmov") (r "^2.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "1h9mg84fks9f3naki3lnw7r4jimaik585xpx4i4lq4fblr1qx1ii")))

(define-public crate-balanced-tree-index-2.3.0 (c (n "balanced-tree-index") (v "2.3.0") (d (list (d (n "aligned-cmov") (r "^2.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "0i1jv3hhzxz1iqvf45r3pa98wg8ppm7wjf2vn3k1n759pknkgdlw")))

