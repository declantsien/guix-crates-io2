(define-module (crates-io ba la balancer) #:use-module (crates-io))

(define-public crate-balancer-0.1.0 (c (n "balancer") (v "0.1.0") (h "1ghxpgxl7ypn7cvw5x20245v9372i6xfv2wzskkgjxq5vk0537p3") (y #t)))

(define-public crate-balancer-0.2.0 (c (n "balancer") (v "0.2.0") (d (list (d (n "mpi") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1wqdrrwxc756h00figgj39mfg5kna8gqx27kv5d4hsgh2p6dzgbs")))

