(define-module (crates-io ba ms bamsalvage) #:use-module (crates-io))

(define-public crate-bamsalvage-0.1.3 (c (n "bamsalvage") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^4.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (f (quote ("zlib-ng"))) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "06sqpmycg7m5mwwfnqi13jyr3zp3m7cgx5bfarxwy94ng2j497sy")))

