(define-module (crates-io ba ya bayard-cli) #:use-module (crates-io))

(define-public crate-bayard-cli-0.8.0 (c (n "bayard-cli") (v "0.8.0") (d (list (d (n "bayard-client") (r "^0.8.0") (d #t) (k 0)) (d (n "bayard-common") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "0wkw5bm3dgc5xymqn5ij3jwrl5casvigyhk30igslaqmv2fk46kg")))

(define-public crate-bayard-cli-0.8.1 (c (n "bayard-cli") (v "0.8.1") (d (list (d (n "bayard-client") (r "^0.8.1") (d #t) (k 0)) (d (n "bayard-common") (r "^0.8.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "1xcrnabsz1v4khd1mspsrzg2nnfpm8qnvs8n8g80dl1nwdrw8b59")))

(define-public crate-bayard-cli-0.8.2 (c (n "bayard-cli") (v "0.8.2") (d (list (d (n "bayard-client") (r "^0.8.1") (d #t) (k 0)) (d (n "bayard-common") (r "^0.8.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "08klgbs3qi7b1zb2sza35n09xqzv5wqdri4j558f85kkb7anfaps")))

(define-public crate-bayard-cli-0.8.3 (c (n "bayard-cli") (v "0.8.3") (d (list (d (n "bayard-client") (r "^0.8") (d #t) (k 0)) (d (n "bayard-common") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yx7mpn2q5yv97n69lmj74km41dgck4dk1f47yvk6qq30l1nq1w3")))

(define-public crate-bayard-cli-0.8.4 (c (n "bayard-cli") (v "0.8.4") (d (list (d (n "bayard-client") (r "^0.8") (d #t) (k 0)) (d (n "bayard-common") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cryml7a30n5hz8fah93hmlx117x9ljvnnrqx09p55ir6afs9xpp")))

(define-public crate-bayard-cli-0.8.5 (c (n "bayard-cli") (v "0.8.5") (d (list (d (n "bayard-client") (r "^0.8") (d #t) (k 0)) (d (n "bayard-common") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02v7azp76h0wa3f0rdczpjnwr57i6zvr59528fh8s2kqcwi2s8h3")))

(define-public crate-bayard-cli-0.8.6 (c (n "bayard-cli") (v "0.8.6") (d (list (d (n "bayard-client") (r "^0.8.6") (d #t) (k 0)) (d (n "bayard-common") (r "^0.8.6") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "017f229ia01ydrzmxhzc7nl041l0spzsxv5cb9qd7zb5pl6rlsky")))

(define-public crate-bayard-cli-0.8.7 (c (n "bayard-cli") (v "0.8.7") (d (list (d (n "bayard-client") (r "^0.8.7") (d #t) (k 0)) (d (n "bayard-common") (r "^0.8.7") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kzmrll12175pc9z5kl2jjld54lsabyi0xfj2zkp63y3kk5ai7kg")))

