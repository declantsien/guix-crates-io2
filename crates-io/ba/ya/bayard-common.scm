(define-module (crates-io ba ya bayard-common) #:use-module (crates-io))

(define-public crate-bayard-common-0.8.0 (c (n "bayard-common") (v "0.8.0") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)))) (h "1xn8ciwxjld5lrl897xmyjgfywqfmsbc0xdfyrpmrm2xfja97wfi")))

(define-public crate-bayard-common-0.8.1 (c (n "bayard-common") (v "0.8.1") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("signal"))) (d #t) (k 0)))) (h "0dp279naapi318q3cng2f3dq0snl84a88zgw9r18qwb8abxaivg8")))

(define-public crate-bayard-common-0.8.2 (c (n "bayard-common") (v "0.8.2") (d (list (d (n "crossbeam-channel") (r "^0.4.3") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.6") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("signal"))) (d #t) (k 0)))) (h "12jz6prassx82i9qlnk4dxbqcivpxhc45a0b0a47dm7qa044ksl0")))

(define-public crate-bayard-common-0.8.3 (c (n "bayard-common") (v "0.8.3") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("signal"))) (d #t) (k 0)))) (h "1nhhc5j54xqkr0pz100nc6qqqxxxa4b3b4lj5fcj3gz32286gxjj")))

(define-public crate-bayard-common-0.8.4 (c (n "bayard-common") (v "0.8.4") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("signal"))) (d #t) (k 0)))) (h "0cb5bk8mlr9f0bpbndkd72ghgpxwiphnmxfhi8jmi1g19w7rrhzl")))

(define-public crate-bayard-common-0.8.5 (c (n "bayard-common") (v "0.8.5") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("signal"))) (d #t) (k 0)))) (h "0ici86hmm99zgi39q9a5zwnvggkkdlpyrirgrz4aqwia13pizxaq")))

(define-public crate-bayard-common-0.8.6 (c (n "bayard-common") (v "0.8.6") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("signal"))) (d #t) (k 0)))) (h "1k97wnlsfip621h3ajz8qb3bx6jbcviymyah4b1wsd48man7lf0v")))

(define-public crate-bayard-common-0.8.7 (c (n "bayard-common") (v "0.8.7") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("signal"))) (d #t) (k 0)))) (h "0m3ii4gb58yvivxfh46ksy1gw4sbsyvk6bc322yzd28hywmz5g71")))

