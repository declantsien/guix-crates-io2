(define-module (crates-io ba mk bamkit) #:use-module (crates-io))

(define-public crate-bamkit-0.1.0 (c (n "bamkit") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "plotly") (r "^0.8.3") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.40.2") (d #t) (k 0)))) (h "0jqr6z0z3zc02wmgdx6kbx4hrvqadkxq9hm4wh00qdd3257f0kg9") (r "1.65.0")))

(define-public crate-bamkit-0.2.0 (c (n "bamkit") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "plotly") (r "^0.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.40.2") (d #t) (k 0)))) (h "1mbzkjgsv663ch070bqlg96i78p058baazrvm8njhw9qswiianbv") (r "1.65.0")))

