(define-module (crates-io ba yw baywatch) #:use-module (crates-io))

(define-public crate-baywatch-0.0.1 (c (n "baywatch") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "ignore") (r "^0.4.21") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)))) (h "1i97crpqc54j7d0ssky1kz4njpp5p7ax2w1jjrnyxbjahwl3vcg7")))

(define-public crate-baywatch-0.0.2 (c (n "baywatch") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "ignore") (r "^0.4.21") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)))) (h "0g7zwwh917f7vdxj1jic65dz8zyb54812ki1ggdjkdibkspfhvs8")))

(define-public crate-baywatch-0.0.3 (c (n "baywatch") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4.22") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)))) (h "09xlcyj40sw4ygz7mpirrxsd4850j9v6ic7yg5r7byyghak34p9a")))

