(define-module (crates-io ba s2 bas2wav) #:use-module (crates-io))

(define-public crate-bas2wav-0.1.0 (c (n "bas2wav") (v "0.1.0") (d (list (d (n "a2kit") (r "^2.4.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "1n6qbjpkwy4jw9lcl0x2qyl97gbmky3pr8v87wdc9vhgxjzxlhvw")))

(define-public crate-bas2wav-0.1.1 (c (n "bas2wav") (v "0.1.1") (d (list (d (n "a2kit") (r "^2.4.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)))) (h "1f8x5qyw5nsyd7418q18k9kxfcndw4bpfp4xw6w47c8zxymaba3a")))

