(define-module (crates-io ba st bastion-executor-trait) #:use-module (crates-io))

(define-public crate-bastion-executor-trait-0.4.0 (c (n "bastion-executor-trait") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bastion") (r "^0.4") (d #t) (k 0)) (d (n "executor-trait") (r "^1.0") (d #t) (k 0)) (d (n "lightproc") (r "^0.3") (d #t) (k 0)))) (h "0160nr2xwiw8vv5kd10q0nsk8mjgca2abkhc54qg0p42l2kzbyx6")))

(define-public crate-bastion-executor-trait-0.5.0 (c (n "bastion-executor-trait") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bastion") (r "^0.4") (d #t) (k 0)) (d (n "executor-trait") (r "^2.0") (d #t) (k 0)) (d (n "lightproc") (r "^0.3") (d #t) (k 0)))) (h "1j3v5c78hbjrw98rcwx01lcmyf1xjqnndrgfjv1vjv4ad46w3r3s") (y #t)))

(define-public crate-bastion-executor-trait-0.5.1 (c (n "bastion-executor-trait") (v "0.5.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bastion") (r "^0.4") (d #t) (k 0)) (d (n "executor-trait") (r "^2.0") (d #t) (k 0)) (d (n "lightproc") (r "^0.3") (d #t) (k 0)))) (h "07l33s9nkky8189aqnwhdwbh3v13xkxfx32ngrnwpn192v3457m0")))

(define-public crate-bastion-executor-trait-0.5.2 (c (n "bastion-executor-trait") (v "0.5.2") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bastion") (r "^0.4") (d #t) (k 0)) (d (n "executor-trait") (r "^2.0") (d #t) (k 0)) (d (n "lightproc") (r "^0.3") (d #t) (k 0)))) (h "0286fa3bn1v3kvx2ds1w3ma33ld7njikrqjw8xa8s6wp2bspj616")))

(define-public crate-bastion-executor-trait-0.5.3 (c (n "bastion-executor-trait") (v "0.5.3") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bastion") (r "^0.4") (d #t) (k 0)) (d (n "executor-trait") (r "^2.1") (d #t) (k 0)) (d (n "lightproc") (r "^0.3") (d #t) (k 0)))) (h "0jxjvkijv6zqjw07i4rw6zb6bv39qv20hbg3nfw58i78d60xyyqa") (r "1.56.0")))

