(define-module (crates-io ba st basti) #:use-module (crates-io))

(define-public crate-basti-0.0.1 (c (n "basti") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.10") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.12") (d #t) (k 2)) (d (n "predicates") (r "^3.0.2") (d #t) (k 2)))) (h "1h6hihbj7kbdwmdkh64nk2hdgmk1vyvazrf5v4wrvs8f3d11r4jn")))

