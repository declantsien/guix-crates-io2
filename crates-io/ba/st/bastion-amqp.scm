(define-module (crates-io ba st bastion-amqp) #:use-module (crates-io))

(define-public crate-bastion-amqp-0.1.0 (c (n "bastion-amqp") (v "0.1.0") (d (list (d (n "bastion") (r "^0.3.4") (d #t) (k 0)) (d (n "bastion-executor") (r "^0.3.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lapin") (r "^1.2.1") (k 0)) (d (n "lightproc") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "002pnnsiiz9k1l2nggs4gz4by4pna17lzivc9krqkgxx0fcd0w0g") (f (quote (("default" "lapin/default"))))))

(define-public crate-bastion-amqp-0.1.1 (c (n "bastion-amqp") (v "0.1.1") (d (list (d (n "bastion") (r "^0.3") (d #t) (k 2)) (d (n "bastion-executor") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lapin") (r "^1.2.1") (k 0)) (d (n "lightproc") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "0g19j89751maaxhdnz8ia04f9gaqn0gzrkv0hd5r5zg8mxmwpn2r") (f (quote (("default" "lapin/default"))))))

(define-public crate-bastion-amqp-0.1.2 (c (n "bastion-amqp") (v "0.1.2") (d (list (d (n "bastion") (r "^0.3") (d #t) (k 2)) (d (n "bastion-executor") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lapin") (r "^1.2.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "1486ahxph0fdrng9yr7bnjm90nx1j5c7pi9mbnlcxxinjgig3f97") (f (quote (("default" "lapin/default"))))))

(define-public crate-bastion-amqp-0.1.3 (c (n "bastion-amqp") (v "0.1.3") (d (list (d (n "bastion") (r "^0.4") (d #t) (k 2)) (d (n "bastion-executor") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lapin") (r "^1.2.2") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "0s5yym05zyiw3xzw9478z1kmy1wa8rc59wc0hls19pd5v9s8h9ay") (f (quote (("default" "lapin/default"))))))

(define-public crate-bastion-amqp-0.2.0 (c (n "bastion-amqp") (v "0.2.0") (d (list (d (n "bastion") (r "^0.4") (d #t) (k 2)) (d (n "bastion-executor") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lapin") (r "^1.2.2") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "1h9hxr99ir33j81ld8ph0a0ygazvfx9wmqr6swwlp1xryxyzibqy") (f (quote (("default" "lapin/default"))))))

(define-public crate-bastion-amqp-0.2.1 (c (n "bastion-amqp") (v "0.2.1") (d (list (d (n "bastion") (r "^0.4") (d #t) (k 2)) (d (n "bastion-executor") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "lapin") (r "^1.4.2") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "0vwq0qh4141v91cprgw2c7d505qjyfisg0fnhy2yjfk3bza59ar2") (f (quote (("default" "lapin/default"))))))

(define-public crate-bastion-amqp-0.2.2 (c (n "bastion-amqp") (v "0.2.2") (d (list (d (n "bastion") (r "^0.4") (d #t) (k 2)) (d (n "bastion-executor") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "lapin") (r "^1.5") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "101cm5xw8dbz1w1xyaz0lgfca18g297kxi8c90j7zwaqjjxhdmk6") (f (quote (("default" "lapin/default"))))))

(define-public crate-bastion-amqp-0.2.3 (c (n "bastion-amqp") (v "0.2.3") (d (list (d (n "bastion") (r "^0.4") (d #t) (k 2)) (d (n "bastion-executor") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "lapin") (r "^1.6") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "1cv9ivbjzg5n8yywdmz3hyrqryqy7wxp1air538mciqxfk64yk8d") (f (quote (("default" "lapin/default"))))))

(define-public crate-bastion-amqp-0.3.0 (c (n "bastion-amqp") (v "0.3.0") (d (list (d (n "bastion") (r "^0.4") (d #t) (k 2)) (d (n "bastion-executor-trait") (r "^0.5.3") (d #t) (k 0)) (d (n "lapin") (r "^2.0") (k 0)) (d (n "tracing") (r "^0.1") (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt"))) (d #t) (k 2)))) (h "0hhsv8n4r5d9aj1ff5i1za8wbp5hj1zr2v82j8nga0076zk1xi3z") (f (quote (("default" "lapin/default")))) (r "1.56.0")))

