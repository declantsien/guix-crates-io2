(define-module (crates-io ba gp bagpipe) #:use-module (crates-io))

(define-public crate-bagpipe-0.1.0 (c (n "bagpipe") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.5") (d #t) (k 0)))) (h "0gnnv26id2my6jy4mbqh0n5mznx1blf4bf46006vriwp79nz43ym") (f (quote (("staggered_indexes") ("prime_schedules") ("huge_segments") ("default" "check_empty_yq") ("check_empty_yq"))))))

