(define-module (crates-io ba wa bawawa) #:use-module (crates-io))

(define-public crate-bawawa-0.1.0 (c (n "bawawa") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2") (d #t) (k 0)))) (h "142csm40pdzz1zyw9jamp54y4k65y2k82xxr4psn62bh9ng8bs6w")))

(define-public crate-bawawa-0.1.1 (c (n "bawawa") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2") (d #t) (k 0)))) (h "1pxxl49j59cr42gv7xpkf8x8rx06kwnrf53ka6l5pq6vwb42d6zh")))

(define-public crate-bawawa-0.1.2 (c (n "bawawa") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2") (d #t) (k 0)))) (h "0czigsaypfy8qj2wqh1mcybibvwh3qcbvxnljlf8s4x5bnbriha5")))

(define-public crate-bawawa-0.1.3 (c (n "bawawa") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2") (d #t) (k 0)))) (h "03pj7zwk3lakk6zyfy5l4l941xbljs5famn3qp1k61vvqdgpmlnv")))

(define-public crate-bawawa-0.1.4 (c (n "bawawa") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2") (d #t) (k 0)))) (h "1r7v1iyxnycj5wxzwkbnp73siq9xaawkvml26x45lai0bn8p6yyc")))

(define-public crate-bawawa-0.1.5 (c (n "bawawa") (v "0.1.5") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2") (d #t) (k 0)))) (h "1qbyriwyy86l6h4sqdciq0pnyd64hvd05848xdjsxzl1z08vb87q")))

