(define-module (crates-io ba k9 bak9) #:use-module (crates-io))

(define-public crate-bak9-0.0.1 (c (n "bak9") (v "0.0.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 0)))) (h "118d4y13fjmv4pgibxsblphzpy5v01r84apnpkk4h50d5xfgwc33")))

(define-public crate-bak9-0.0.2 (c (n "bak9") (v "0.0.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 0)))) (h "06x5877r7cqhflnp774hgsv32xbnkyn09ac1kazbm34y2l9vq1r0")))

(define-public crate-bak9-0.1.1 (c (n "bak9") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 2)) (d (n "function_name") (r "^0") (d #t) (k 2)) (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wl705yv7v39x742rjidnkh2c4zkv2i7lb1kacji94837hpn1vg8")))

(define-public crate-bak9-0.1.2 (c (n "bak9") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 2)) (d (n "function_name") (r "^0") (d #t) (k 2)) (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1aq36js4vz88himdjab5lr35bf7j68b89jn3i80gqfqcrrdwl6iy")))

(define-public crate-bak9-0.2.0 (c (n "bak9") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 2)) (d (n "function_name") (r "^0") (d #t) (k 2)) (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vsw80qvj3k5v4wmggwl2q33zgnv3gbdb8rxfk9bk6ah602n71mk")))

(define-public crate-bak9-0.2.1 (c (n "bak9") (v "0.2.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 2)) (d (n "function_name") (r "^0") (d #t) (k 2)) (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ajcngi8lv05mxqavvhi6y5yrgwkp0fc3i996b2f1nhzg4yckg4x")))

(define-public crate-bak9-0.3.0 (c (n "bak9") (v "0.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 2)) (d (n "function_name") (r "^0") (d #t) (k 2)) (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xf3jpy1b5zhx6hw09ssyasljwi9azkmwfmsifb9h1jix8l48dfs")))

(define-public crate-bak9-0.3.1 (c (n "bak9") (v "0.3.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 0)) (d (n "file_diff") (r "^1") (d #t) (k 2)) (d (n "function_name") (r "^0") (d #t) (k 2)) (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06gsbjji33z36rw4ar404q3np8xz4xk5z94ngwvww7d3202ch2c3")))

