(define-module (crates-io ba ki baking) #:use-module (crates-io))

(define-public crate-baking-0.0.0-0.0.0-0.0.0 (c (n "baking") (v "0.0.0-0.0.0-0.0.0") (h "0zr2zahfdibdqfnh03ppw01c1440kd0hyq43jmwrkrrwq4jpcb0k") (y #t)))

(define-public crate-baking-2.0.1 (c (n "baking") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0.49") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-log") (r "^2.0.0") (d #t) (k 0)) (d (n "crokey") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "notify") (r "=5.0.0-pre.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termimad") (r "^0.20") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "vte") (r "^0.8") (d #t) (k 0)))) (h "0ijmn5nv3wsn1pi4g2mnsk71yqlf0h8kjzf3f03aadsy88pb7i46") (y #t) (r "1.56")))

(define-public crate-baking-0.0.0-- (c (n "baking") (v "0.0.0--") (h "00xmfvvwbv2bdic3ij4prklj7qv7l4hs809242mg9wpp9wa8nn1c") (y #t)))

