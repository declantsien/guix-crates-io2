(define-module (crates-io ba ic baicie_package_test) #:use-module (crates-io))

(define-public crate-baicie_package_test-0.1.0 (c (n "baicie_package_test") (v "0.1.0") (h "0bngvbfhdaikx94bzla3m8559g7s1hkv4c619khf6nvz75yxdbja")))

(define-public crate-baicie_package_test-0.1.1 (c (n "baicie_package_test") (v "0.1.1") (h "0hcs15bkzd4yjlffgg8kpnq98ppngcz7j050q0l1ia80z7qg7x2g")))

