(define-module (crates-io ba si basiclu-sys) #:use-module (crates-io))

(define-public crate-basiclu-sys-0.1.0 (c (n "basiclu-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)))) (h "10a63xpa417dh7xlc7gs5p4hd0vl2vy8vn2wwl9cl1fr5yhkp0jn")))

(define-public crate-basiclu-sys-0.1.1 (c (n "basiclu-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)))) (h "1rwk1a3mkyn21lyxj954p8kf31q9z6zf4ypyzasm3i6d9db6zpgk")))

(define-public crate-basiclu-sys-0.1.2 (c (n "basiclu-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "086wmf4z8f6zmqhvhfqgyyvpldr519airdp8yrb0n864dzy2yi1j")))

