(define-module (crates-io ba si basic-human-duration) #:use-module (crates-io))

(define-public crate-basic-human-duration-0.1.2 (c (n "basic-human-duration") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "0z6llrc4canbzicarj919l6c2iqq015msvf074zfrwy6grw2cns7")))

(define-public crate-basic-human-duration-0.2.0 (c (n "basic-human-duration") (v "0.2.0") (d (list (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "1bbngz72jsrsvgb2r3yhp9h81y07qf9s176scd370fpf6ncafcvl")))

