(define-module (crates-io ba si basiccountminsketch) #:use-module (crates-io))

(define-public crate-basiccountminsketch-0.1.0 (c (n "basiccountminsketch") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "1sn0f7qgs011h609ckyhwfsg9i31yxij09dq1vvhldcsjab5iq4z")))

(define-public crate-basiccountminsketch-0.1.2 (c (n "basiccountminsketch") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "1r8skwcbc7w716a1hjjrvj8kw9rk99kf0xssmqb75np905rw4ry2")))

