(define-module (crates-io ba si basic_stochastics) #:use-module (crates-io))

(define-public crate-basic_stochastics-0.1.0 (c (n "basic_stochastics") (v "0.1.0") (h "0iwvb1mdxmxlpxshwqxv46f0cw9x3r71nf5rwrz02zccwjawd1l6") (y #t)))

(define-public crate-basic_stochastics-0.1.1 (c (n "basic_stochastics") (v "0.1.1") (h "0955jhfr5mzlwlpn103cj4wvql7pw1xnvm37b25lplh1gqkcz24x") (y #t)))

(define-public crate-basic_stochastics-0.1.2 (c (n "basic_stochastics") (v "0.1.2") (h "09qyp1bd24rdv6x6q2mczyqcma5imilxfn4m8w937gjyl6k4r8ab") (y #t)))

(define-public crate-basic_stochastics-0.1.3 (c (n "basic_stochastics") (v "0.1.3") (h "1kfrilsaj1jyjh9fhq0hhihyv8fh8rww4biqk2cqcyx2h4mgzj30")))

