(define-module (crates-io ba si basic_dsp_interop) #:use-module (crates-io))

(define-public crate-basic_dsp_interop-0.4.0 (c (n "basic_dsp_interop") (v "0.4.0") (d (list (d (n "basic_dsp_vector") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 0)))) (h "14dm52i97icdirfv3h44gbyid73xnly1gkwnpbv920mxrlx3glk5") (f (quote (("use_sse") ("use_avx"))))))

(define-public crate-basic_dsp_interop-0.4.1 (c (n "basic_dsp_interop") (v "0.4.1") (d (list (d (n "basic_dsp_vector") (r "^0.4.1") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 0)))) (h "07qdx5l6fw3hvin5kap2vczfzn7kk2d460hjw3vf9l9k5ll7vwyz") (f (quote (("use_sse") ("use_avx"))))))

(define-public crate-basic_dsp_interop-0.4.3 (c (n "basic_dsp_interop") (v "0.4.3") (d (list (d (n "basic_dsp_vector") (r "^0.4.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.1.34") (d #t) (k 0)))) (h "1jwrhjwn3n8an4f1zavjkhk42c35zb7vrj34jx45ag3y9qb362y2") (f (quote (("use_sse") ("use_avx"))))))

(define-public crate-basic_dsp_interop-0.5.0 (c (n "basic_dsp_interop") (v "0.5.0") (d (list (d (n "basic_dsp_vector") (r "^0.5.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.1.34") (d #t) (k 0)))) (h "19qip56llzsdykrgjdrbc8pngvxyhqf5vrbn1r3b49c6yng8sx9v") (f (quote (("use_sse") ("use_avx"))))))

(define-public crate-basic_dsp_interop-0.5.1 (c (n "basic_dsp_interop") (v "0.5.1") (d (list (d (n "basic_dsp_vector") (r "^0.5.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.1.34") (d #t) (k 0)))) (h "16pdhpfdypdvbj7570kl6cibxdwmpj0yy7h6c96h0xl9pifwzz7b") (f (quote (("use_sse") ("use_avx"))))))

(define-public crate-basic_dsp_interop-0.5.2 (c (n "basic_dsp_interop") (v "0.5.2") (d (list (d (n "basic_dsp_vector") (r "^0.5.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1.34") (d #t) (k 0)))) (h "1jjslvd26ffhwsykyw448zn2jy3mgd8ba52pazcpwral90qzlqqk") (f (quote (("use_sse") ("use_avx"))))))

(define-public crate-basic_dsp_interop-0.6.0 (c (n "basic_dsp_interop") (v "0.6.0") (d (list (d (n "basic_dsp_vector") (r "^0.6.0") (k 0)) (d (n "num-complex") (r "^0.1.34") (d #t) (k 0)))) (h "1bkii65xj5aa3p2j6372srb0pbd2i5miha81v6l3jr23lsgmz4im") (f (quote (("use_sse2" "basic_dsp_vector/use_sse2") ("use_gpu" "basic_dsp_vector/use_gpu") ("use_avx512" "basic_dsp_vector/use_avx512") ("use_avx2" "basic_dsp_vector/use_avx2") ("std" "basic_dsp_vector/std") ("default" "use_sse2" "use_avx2" "std"))))))

(define-public crate-basic_dsp_interop-0.7.0 (c (n "basic_dsp_interop") (v "0.7.0") (d (list (d (n "basic_dsp_vector") (r "^0.7.0") (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)))) (h "1hqk26lkv6h51b661k29xkc3x6dfzcddxrc6a8wxbqxsfvs4lcdb") (f (quote (("use_sse2" "basic_dsp_vector/use_sse2") ("use_gpu" "basic_dsp_vector/use_gpu") ("use_avx512" "basic_dsp_vector/use_avx512") ("use_avx2" "basic_dsp_vector/use_avx2") ("std" "basic_dsp_vector/std") ("default" "use_sse2" "use_avx2" "std"))))))

(define-public crate-basic_dsp_interop-0.7.1 (c (n "basic_dsp_interop") (v "0.7.1") (d (list (d (n "basic_dsp_vector") (r "^0.7.0") (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)))) (h "0sryr8bp9h143821wpyjpdg8i42nbjjscyp3hlw877rqrfn0i1rj") (f (quote (("use_sse2" "basic_dsp_vector/use_sse2") ("use_gpu" "basic_dsp_vector/use_gpu") ("use_avx512" "basic_dsp_vector/use_avx512") ("use_avx2" "basic_dsp_vector/use_avx2") ("std" "basic_dsp_vector/std") ("default" "std"))))))

(define-public crate-basic_dsp_interop-0.8.0 (c (n "basic_dsp_interop") (v "0.8.0") (d (list (d (n "basic_dsp_vector") (r "^0.8.0") (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)))) (h "1kzxj3yibkkqgapd59h9wm0mydg1riq9cgra59dh2gkafcvia6mv") (f (quote (("use_sse2" "basic_dsp_vector/use_sse2") ("use_gpu" "basic_dsp_vector/use_gpu") ("use_avx512" "basic_dsp_vector/use_avx512") ("use_avx2" "basic_dsp_vector/use_avx2") ("std" "basic_dsp_vector/std") ("default" "std"))))))

(define-public crate-basic_dsp_interop-0.9.0 (c (n "basic_dsp_interop") (v "0.9.0") (d (list (d (n "basic_dsp_vector") (r "^0.9.0") (k 0)) (d (n "num-complex") (r "^0.3") (d #t) (k 0)))) (h "12ji5asbi5m8yv6wwbq4df9k846g5j1m0z1gbrv6g41fsr1cqz9j") (f (quote (("use_sse2" "basic_dsp_vector/use_sse2") ("use_gpu" "basic_dsp_vector/use_gpu") ("use_avx512" "basic_dsp_vector/use_avx512") ("use_avx2" "basic_dsp_vector/use_avx2") ("std" "basic_dsp_vector/std") ("default" "std"))))))

(define-public crate-basic_dsp_interop-0.9.1 (c (n "basic_dsp_interop") (v "0.9.1") (d (list (d (n "basic_dsp_vector") (r "^0.9.0") (k 0)) (d (n "num-complex") (r "^0.3") (d #t) (k 0)))) (h "1mg6k4jjrhwb5bdhhdlj6nhh8xgbrmm3lnj0nmr5ad6ifs4jf4ip") (f (quote (("use_sse2" "basic_dsp_vector/use_sse2") ("use_gpu" "basic_dsp_vector/use_gpu") ("use_avx512" "basic_dsp_vector/use_avx512") ("use_avx2" "basic_dsp_vector/use_avx2") ("std" "basic_dsp_vector/std") ("default" "std"))))))

(define-public crate-basic_dsp_interop-0.9.2 (c (n "basic_dsp_interop") (v "0.9.2") (d (list (d (n "basic_dsp_vector") (r "^0.9.0") (k 0)) (d (n "num-complex") (r "^0.3") (d #t) (k 0)))) (h "0s1sfhbc1ydl695b34x45cz5jlda9zaaqg2nycynhkk2zk2l5zq4") (f (quote (("use_sse2" "basic_dsp_vector/use_sse2") ("use_gpu" "basic_dsp_vector/use_gpu") ("use_avx512" "basic_dsp_vector/use_avx512") ("use_avx2" "basic_dsp_vector/use_avx2") ("std" "basic_dsp_vector/std") ("default" "std"))))))

(define-public crate-basic_dsp_interop-0.9.3 (c (n "basic_dsp_interop") (v "0.9.3") (d (list (d (n "basic_dsp_vector") (r "^0.9.0") (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)))) (h "0fm00hyc5125pdqm17mmfszlawy15ilhda4ms2b32icca36cr8yg") (f (quote (("use_sse2" "basic_dsp_vector/use_sse2") ("use_gpu" "basic_dsp_vector/use_gpu") ("use_avx512" "basic_dsp_vector/use_avx512") ("use_avx2" "basic_dsp_vector/use_avx2") ("std" "basic_dsp_vector/std") ("default" "std"))))))

(define-public crate-basic_dsp_interop-0.10.0 (c (n "basic_dsp_interop") (v "0.10.0") (d (list (d (n "basic_dsp_vector") (r "^0.10.0") (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)))) (h "0k210x3diksrcwsvc4vx91iaaylbwzmpylm0dji7wxd64l27x27s") (f (quote (("use_sse2" "basic_dsp_vector/use_sse2") ("use_gpu" "basic_dsp_vector/use_gpu") ("use_avx512" "basic_dsp_vector/use_avx512") ("use_avx2" "basic_dsp_vector/use_avx2") ("std" "basic_dsp_vector/std") ("default" "std"))))))

(define-public crate-basic_dsp_interop-0.10.1 (c (n "basic_dsp_interop") (v "0.10.1") (d (list (d (n "basic_dsp_vector") (r "^0.10.1") (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)))) (h "143yfj5n0qaj3vf10khlhx7qx2wmaswz38gmgl7q5my4h72pn5pa") (f (quote (("use_sse2" "basic_dsp_vector/use_sse2") ("use_gpu" "basic_dsp_vector/use_gpu") ("use_avx512" "basic_dsp_vector/use_avx512") ("use_avx2" "basic_dsp_vector/use_avx2") ("std" "basic_dsp_vector/std") ("default" "std"))))))

(define-public crate-basic_dsp_interop-0.10.2 (c (n "basic_dsp_interop") (v "0.10.2") (d (list (d (n "basic_dsp_vector") (r "^0.10.2") (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)))) (h "1939cdvn2cjzxmz5lwj39570mk7agxy15k7dh9g1cvwj6c2rqwd0") (f (quote (("use_sse2" "basic_dsp_vector/use_sse2") ("use_gpu" "basic_dsp_vector/use_gpu") ("use_avx512" "basic_dsp_vector/use_avx512") ("use_avx2" "basic_dsp_vector/use_avx2") ("std" "basic_dsp_vector/std") ("default" "std"))))))

