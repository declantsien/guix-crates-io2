(define-module (crates-io ba si basic_lexer) #:use-module (crates-io))

(define-public crate-basic_lexer-0.1.0 (c (n "basic_lexer") (v "0.1.0") (h "0akx6kz0l1yqbvjcflg9zai4bkd0df0znnbzjlbqfzs8dzwvi2i4") (y #t)))

(define-public crate-basic_lexer-0.1.1 (c (n "basic_lexer") (v "0.1.1") (h "0n2smd98lsisaq3dpg4lgvn4b9svbn73fvip9qgwgyj7f0b4f8ds") (y #t)))

(define-public crate-basic_lexer-0.1.2 (c (n "basic_lexer") (v "0.1.2") (h "1rh9s0qyfr297yxmij4a6v2qrjmqy3l8l3amwym5clw6r9hl2wwc")))

(define-public crate-basic_lexer-0.1.3 (c (n "basic_lexer") (v "0.1.3") (h "16dpiy3gd6wazj6lnhv43kgh7zl456kgq00na7pq8mjihw47mv9y")))

(define-public crate-basic_lexer-0.2.0 (c (n "basic_lexer") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pjz6phh00z18zayaba1xnxml42is06hfh438kiqpvjnkx9y6dw5")))

(define-public crate-basic_lexer-0.2.1 (c (n "basic_lexer") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0m81yiwlqcgkx7g645mn6hxcb1h9a8avrnimw7zm0im9bb3h4s5d")))

