(define-module (crates-io ba si basic-text-internals) #:use-module (crates-io))

(define-public crate-basic-text-internals-0.3.0 (c (n "basic-text-internals") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.17") (d #t) (k 0)))) (h "0x9xhrri4z499qf59xbdrf7m2dlcz3q70j3x9496m55ck76c6wfy")))

(define-public crate-basic-text-internals-0.3.1 (c (n "basic-text-internals") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.17") (d #t) (k 0)))) (h "1pkl4xrqqgnz4870bi7sl0gi159pc8cqsnbmn9riammaqnz0fk8n")))

(define-public crate-basic-text-internals-0.3.2 (c (n "basic-text-internals") (v "0.3.2") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.17") (d #t) (k 0)))) (h "10c3kh01cjkzw06w4i3jn5nvlflxj4fw7sk455zjbxjmiamsrzzp")))

(define-public crate-basic-text-internals-0.3.3 (c (n "basic-text-internals") (v "0.3.3") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.17") (d #t) (k 0)))) (h "1vh7yj54bm1ivxzl3n2x78zqbsw4g2sfsms8pmb0pdwsp29d82kf")))

(define-public crate-basic-text-internals-0.4.0 (c (n "basic-text-internals") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "01afq954wws87r7ippd9dbsfigzdm10dldw39l3nnx9aq2r0lwci")))

(define-public crate-basic-text-internals-0.5.0 (c (n "basic-text-internals") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0yanmm9mf50zaqspby81kkxqi0wz2y38x9334430sgnpf2wry5s3")))

(define-public crate-basic-text-internals-0.6.0 (c (n "basic-text-internals") (v "0.6.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1lz0n29lid09g0phkr4s7i1p9k3abaw0fkz5ynhzk1axx933b34a")))

(define-public crate-basic-text-internals-0.7.0 (c (n "basic-text-internals") (v "0.7.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1lch0p39q8qfcyf625vc12i6vypfafrqwjv38hpgfjjigkjxvrvp")))

(define-public crate-basic-text-internals-0.8.0 (c (n "basic-text-internals") (v "0.8.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "06gyqi6axlf48fvbb4r1wzxy42a6nxifp3ffhpljnv2fs5wll9sn")))

(define-public crate-basic-text-internals-0.9.0 (c (n "basic-text-internals") (v "0.9.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "10xpgsgwdbjgz4sn44asb4iiiq4xn2ab8p6a5309rmh6lilwidjy")))

(define-public crate-basic-text-internals-0.9.1 (c (n "basic-text-internals") (v "0.9.1") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1jb94cgsysqaskbsbsigij07r1b5p6m6i55wy1k81dxhmkli5ckb")))

(define-public crate-basic-text-internals-0.10.0 (c (n "basic-text-internals") (v "0.10.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1dxrnq6875qikvg780zf8w6dx48wss5hcmxx5yp8vxcp0h03yqwb")))

(define-public crate-basic-text-internals-0.11.0 (c (n "basic-text-internals") (v "0.11.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0a8ra7g5y3slqq62bv9g4hh1qf65ny4sihsrxbmf53ipd6dmqdzd")))

(define-public crate-basic-text-internals-0.12.0 (c (n "basic-text-internals") (v "0.12.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "158ahl9bj9qhn99iy5qg3gw6iqs38mnbssl86ad16iipwzwnbqm9")))

(define-public crate-basic-text-internals-0.13.0 (c (n "basic-text-internals") (v "0.13.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "14gcr87zvqgszal3gamiz0y980hy04f5m8zjvkr7xnm5x9a6x7g2")))

(define-public crate-basic-text-internals-0.14.0 (c (n "basic-text-internals") (v "0.14.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0bff6d99b50hyx3prdi79dm1y7xj7ggjy35nsgawqvm5q8ll8x31")))

(define-public crate-basic-text-internals-0.15.0 (c (n "basic-text-internals") (v "0.15.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1cl8pdrly7385zs77rscxi83jgbwys7wx1440ghsikwvn75hwj86")))

(define-public crate-basic-text-internals-0.16.0 (c (n "basic-text-internals") (v "0.16.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0n8b7k7gk91s7lh1psl8labwlvc2mf340v1j4m7i8mqv4lq5fhfc")))

(define-public crate-basic-text-internals-0.17.0 (c (n "basic-text-internals") (v "0.17.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1361aldizcvp11mm3xizhpvx66k3x5k99hj33nw5ip8lx1fvh9av")))

(define-public crate-basic-text-internals-0.18.0 (c (n "basic-text-internals") (v "0.18.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0rbrxyn2p2wwf6rgn6g68ysvsi7v68j833f3cgpc1kjksp601acg")))

(define-public crate-basic-text-internals-0.19.0 (c (n "basic-text-internals") (v "0.19.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1q7nfl9by0qh51pag8yndicx1gxnx0qi9l57d59b8xam9hb7f9v1")))

(define-public crate-basic-text-internals-0.19.1 (c (n "basic-text-internals") (v "0.19.1") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "0qnfa14ndg67p4jzi0vx3m9n9ghdwx7vs4jwlkdyp18i1dwsifqc")))

(define-public crate-basic-text-internals-0.19.2 (c (n "basic-text-internals") (v "0.19.2") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.23") (d #t) (k 0)))) (h "1vr2pi4vny7ywffz703b37y97m8hmnll0k4vm1lr5v4zwb5gc9cd")))

