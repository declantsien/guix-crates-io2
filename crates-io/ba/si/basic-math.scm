(define-module (crates-io ba si basic-math) #:use-module (crates-io))

(define-public crate-basic-math-0.1.0 (c (n "basic-math") (v "0.1.0") (h "0lyqx9cy8wnnxhzzlxni4j69ywkihcjjxjg2v2clxid0y2m2hmaf")))

(define-public crate-basic-math-0.2.0 (c (n "basic-math") (v "0.2.0") (h "1lmlnwl82vx5g26lsx3dhalmc57zh7kxyg84ddlxxsw97j6q35ww") (y #t)))

