(define-module (crates-io ba si basic_tcp_proxy) #:use-module (crates-io))

(define-public crate-basic_tcp_proxy-0.1.0 (c (n "basic_tcp_proxy") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0nch8y4vbcahgsb5lpc45yfwkh35crvf013sbkmprr7p3gvl5s1w")))

(define-public crate-basic_tcp_proxy-0.1.1 (c (n "basic_tcp_proxy") (v "0.1.1") (h "15j95jsdnb617xg60lryhvv3zy3aqcp2fsvxv3kii3q7563zmnbj")))

(define-public crate-basic_tcp_proxy-0.1.2 (c (n "basic_tcp_proxy") (v "0.1.2") (h "03jlvgjar3l08hmf2jrfg103j4fvshpw648jq9az6b558793jd91")))

(define-public crate-basic_tcp_proxy-0.1.3 (c (n "basic_tcp_proxy") (v "0.1.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1w70nksgsr72wgm3vpin322kc4qlacw47yy6pnf48hsn5z5a0wf9")))

(define-public crate-basic_tcp_proxy-0.2.0 (c (n "basic_tcp_proxy") (v "0.2.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "18j1qakypqdhaqg1l0lwr1bmh6s22y5qz0hv6r6xvp486gb4pzb4")))

(define-public crate-basic_tcp_proxy-0.3.0 (c (n "basic_tcp_proxy") (v "0.3.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1d8f2rm4xi125xh6ds8nbhibqzk8xl6czvmfzfwa3cj5nbxl9cph")))

(define-public crate-basic_tcp_proxy-0.3.1 (c (n "basic_tcp_proxy") (v "0.3.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0niynx843fbsca057lxx1jxwzzpl1aay56gs0l9bp3910dxpn9as")))

(define-public crate-basic_tcp_proxy-0.3.2 (c (n "basic_tcp_proxy") (v "0.3.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "060pyz41pkw3r7kmljjc2pv64hmxkjznidj5vjf8vsyqj7vm9fjp")))

