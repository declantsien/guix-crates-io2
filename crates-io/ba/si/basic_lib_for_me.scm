(define-module (crates-io ba si basic_lib_for_me) #:use-module (crates-io))

(define-public crate-basic_lib_for_me-0.1.0 (c (n "basic_lib_for_me") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "03ckq6jdiqy39nz3s2r0ncbn7b6wa2iwjglwnrg0gspmskcvs1f6")))

(define-public crate-basic_lib_for_me-0.2.0 (c (n "basic_lib_for_me") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1q4msrrr2l9a7pmxpva37hy5nsc37h8aw89v6sd2fi7nkhy2fgdq")))

(define-public crate-basic_lib_for_me-0.2.1 (c (n "basic_lib_for_me") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0pyqg1sw2nnhv1574bmr6w5k8786ysgyz37g980ylp7zlcs2xg5a")))

(define-public crate-basic_lib_for_me-0.2.2 (c (n "basic_lib_for_me") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "06accikq1sixqi7vpmp2gp7m0ldkpjqhlsfalnl6qpdcjdyrr9q8")))

(define-public crate-basic_lib_for_me-0.3.0 (c (n "basic_lib_for_me") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "11wkfddnaw3b4vxzs5kiyfsy3g04x3l29rns3yvviz4k9pksc4sq")))

(define-public crate-basic_lib_for_me-0.3.1 (c (n "basic_lib_for_me") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0p46j6qdihywhgiv20djcwad4af824n8jiniax9gd2wmcjs3x4kr")))

(define-public crate-basic_lib_for_me-0.3.2 (c (n "basic_lib_for_me") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0y37bb7j1d3hx4b2ispf69fvx7nn8ip4fm1kvqlxmswqy3c7my9a") (y #t)))

(define-public crate-basic_lib_for_me-0.3.3 (c (n "basic_lib_for_me") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0k2m2ca549mg788kjrxh6wvwai0q48kh6vnf1phrvsbyvisspa1j")))

(define-public crate-basic_lib_for_me-0.3.4 (c (n "basic_lib_for_me") (v "0.3.4") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "007a0zs0lvmvqkf6pzi198khmv2qixgl2mbbrf1r6zs6q30pw8ma")))

