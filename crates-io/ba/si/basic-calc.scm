(define-module (crates-io ba si basic-calc) #:use-module (crates-io))

(define-public crate-basic-calc-1.0.0 (c (n "basic-calc") (v "1.0.0") (h "1x43h9fkspapnrrx2ay9xncnqp5xlfs2mg8dvjdhzp47qhqwhn8y")))

(define-public crate-basic-calc-1.0.1 (c (n "basic-calc") (v "1.0.1") (h "15csin9nw43ik7anhikampd0wyxrfz3dfb16pwycni9bgy4f6n12")))

(define-public crate-basic-calc-1.0.2 (c (n "basic-calc") (v "1.0.2") (h "0zyzg64iyhd8r12lq5kji5152s72gdapw6lil5kddiyjjm9a36cj")))

(define-public crate-basic-calc-1.0.3 (c (n "basic-calc") (v "1.0.3") (h "0r99crw6h584sp712bnb6r1xjdm6xcp4gs2m2gnxf19qs0p84pki")))

(define-public crate-basic-calc-1.0.4 (c (n "basic-calc") (v "1.0.4") (h "1ygq2jcahz4n5i7lp1bp3gi22vlqjsih0nm4q3mmsswclc8jx4a0")))

