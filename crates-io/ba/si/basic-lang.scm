(define-module (crates-io ba si basic-lang) #:use-module (crates-io))

(define-public crate-basic-lang-0.1.0 (c (n "basic-lang") (v "0.1.0") (h "1a7c4vbvh1v3pv7442b60rndg0zwmqlr5fazmrprfnckkhrpkxiz")))

(define-public crate-basic-lang-0.2.0 (c (n "basic-lang") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 0)))) (h "04ahzfhvrbgfa8z5xlmraq9654gy9rfjnd16f72irqxgvvnpxvd5")))

(define-public crate-basic-lang-0.3.0 (c (n "basic-lang") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 0)))) (h "021xq88wamiiqqg51naqld13dj37wjjzzb30j7f97swx6cnzv4c3")))

(define-public crate-basic-lang-0.4.0 (c (n "basic-lang") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 0)))) (h "1l1dr1jk2wmay33yilg6w6wishr2sic8pj5vibwd81lmq7p5bimy")))

(define-public crate-basic-lang-0.5.0 (c (n "basic-lang") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 0)) (d (n "mortal") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0n73ccb9a0h0xnk53dgghmml657szq1h1964d48q79w4c6i5m85y")))

(define-public crate-basic-lang-0.6.0 (c (n "basic-lang") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 0)) (d (n "mortal") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "04mqzp1wf80ykb7sqzc90mmrn9d864vvcyq064l5dsxpqzijrpzg")))

(define-public crate-basic-lang-0.7.0 (c (n "basic-lang") (v "0.7.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 0)) (d (n "mortal") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1nlk2bl3m4557zga38ffiivdfr65n8xxnjjky4nvrx94ism8q3nw") (y #t)))

(define-public crate-basic-lang-0.7.1 (c (n "basic-lang") (v "0.7.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 0)) (d (n "mortal") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1n4b9dscxi5p7smkm1kwgka2196zl3l904mch4hajbfj1rb8i3w7")))

