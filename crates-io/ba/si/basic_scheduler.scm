(define-module (crates-io ba si basic_scheduler) #:use-module (crates-io))

(define-public crate-basic_scheduler-0.1.0 (c (n "basic_scheduler") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1zlq1nn6752bd2mcb4ddjlvhxwsxh1x1df70jaldvcwvpv1av3vx")))

(define-public crate-basic_scheduler-0.1.1 (c (n "basic_scheduler") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1b4sszprxsg62qdl4jrzaw9rrw1ai3r48k1z2lq1lp1f6cpzp8h0")))

