(define-module (crates-io ba si basic_maths) #:use-module (crates-io))

(define-public crate-basic_maths-0.0.0 (c (n "basic_maths") (v "0.0.0") (h "1d9ylm45f3ry1rrngg8hplaxq0bq37gw7lyd0kjkyb5jwgr1n4ic")))

(define-public crate-basic_maths-0.0.1 (c (n "basic_maths") (v "0.0.1") (h "0jfg55npnf17n53yw4psrpi1dxjy5w6rk2kkm62v0zxc6j0swmqs")))

