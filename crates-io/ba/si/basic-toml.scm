(define-module (crates-io ba si basic-toml) #:use-module (crates-io))

(define-public crate-basic-toml-0.0.0 (c (n "basic-toml") (v "0.0.0") (h "0cqzhq5lfhl0777kml41d44g0b59mv3z6s8lh17fwp53g1r2k3s7") (y #t)))

(define-public crate-basic-toml-0.1.0 (c (n "basic-toml") (v "0.1.0") (d (list (d (n "semver") (r "^1.0") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0y3knnmv7kl453s4k05m1pxn9flyfcx7p5cizy206mj27v4clwis")))

(define-public crate-basic-toml-0.1.1 (c (n "basic-toml") (v "0.1.1") (d (list (d (n "semver") (r "^1.0") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1d2h378373x0i8j4mi4sfaq8np9hf5xqr2rh9pa7r5irfxk9p09f")))

(define-public crate-basic-toml-0.1.2 (c (n "basic-toml") (v "0.1.2") (d (list (d (n "semver") (r "^1.0") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1wfxgh6ssdbvg3d1c566dr6q020f7w0qj2vmmz70r3da558yf3aw")))

(define-public crate-basic-toml-0.1.3 (c (n "basic-toml") (v "0.1.3") (d (list (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "1b03mfsjvlfl41bv3gw02fz97x5c9k8hp4xqi4iv2wjxf0xd0f7q")))

(define-public crate-basic-toml-0.1.4 (c (n "basic-toml") (v "0.1.4") (d (list (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "1ij8hkgb9yvnl5pkr7b9hcq86358n83ja1qxkqiyqw13g9p51z3v")))

(define-public crate-basic-toml-0.1.5 (c (n "basic-toml") (v "0.1.5") (d (list (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.190") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "0jzs2n08z300aaj56mdakskp2whqskhavqilqghvyjngflgii9ya")))

(define-public crate-basic-toml-0.1.6 (c (n "basic-toml") (v "0.1.6") (d (list (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.190") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "0vx2mj1z0p1kv3iij88rcrb9rk0vd1kf5fzrd8ismssscrjj5h94")))

(define-public crate-basic-toml-0.1.7 (c (n "basic-toml") (v "0.1.7") (d (list (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.190") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "0y4pl7a0klaxmiqh99mpmyz80jsv2p0jl50rizx9n8jrcdq3j89g")))

(define-public crate-basic-toml-0.1.8 (c (n "basic-toml") (v "0.1.8") (d (list (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.194") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.194") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "1r8l5kz5lzfw66llq8b023vx38kh35gdf8jd428ma76lr8j1bcid")))

(define-public crate-basic-toml-0.1.9 (c (n "basic-toml") (v "0.1.9") (d (list (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.194") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.194") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "1y2q0la176gmqgskarc5464fqq3rncnjc138iiayj57n53i8hcw2")))

