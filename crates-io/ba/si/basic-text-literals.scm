(define-module (crates-io ba si basic-text-literals) #:use-module (crates-io))

(define-public crate-basic-text-literals-0.3.0 (c (n "basic-text-literals") (v "0.3.0") (d (list (d (n "basic-text-internals") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "14dqp9313m0aaacj1p0qxlblfpfkdj2l4bgzn7wlf8ipsbdi36cn")))

(define-public crate-basic-text-literals-0.3.1 (c (n "basic-text-literals") (v "0.3.1") (d (list (d (n "basic-text-internals") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "0r0f74xid7yzfdxday1dh6937hd4pqh1897n25a60a5jl9cxr9nj")))

(define-public crate-basic-text-literals-0.3.2 (c (n "basic-text-literals") (v "0.3.2") (d (list (d (n "basic-text-internals") (r "^0.3.2-alpha.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1hdjy1y7wkfvyzil9alqqy3acis2aw89vpvzzm8qn3rvfy6266nd")))

(define-public crate-basic-text-literals-0.3.3 (c (n "basic-text-literals") (v "0.3.3") (d (list (d (n "basic-text-internals") (r "^0.3.3-alpha.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1pjjvs90phjs6rbvb80lw0a68brvy8dkf55gbyzgzhqrk0d5b6kg")))

(define-public crate-basic-text-literals-0.4.0 (c (n "basic-text-literals") (v "0.4.0") (d (list (d (n "basic-text-internals") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "16lnkcy34d2ngaapq14pich7dynh9hrq95cmsqf38vlqj8s3axhk")))

(define-public crate-basic-text-literals-0.5.0 (c (n "basic-text-literals") (v "0.5.0") (d (list (d (n "basic-text-internals") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "0mg3dmya3cb6mldh98il3qia27d8n6lrd9p8lh5hc6hhdzbkfks8")))

(define-public crate-basic-text-literals-0.6.0 (c (n "basic-text-literals") (v "0.6.0") (d (list (d (n "basic-text-internals") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "0xzxyzybzpz9j6jv2s9walhnyh9plyrp4hnyz4rnc5vbad7iyxww")))

(define-public crate-basic-text-literals-0.7.0 (c (n "basic-text-literals") (v "0.7.0") (d (list (d (n "basic-text-internals") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1xlwczv22sy37193a1z8a55i7fby6zdazsdfj11csi5jwndciiml")))

(define-public crate-basic-text-literals-0.8.0 (c (n "basic-text-literals") (v "0.8.0") (d (list (d (n "basic-text-internals") (r "^0.8.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "12n0sgb6p4b4bql166wxmz8ykz46nx7qp0qkyw0ymy5pc5vighpc")))

(define-public crate-basic-text-literals-0.9.0 (c (n "basic-text-literals") (v "0.9.0") (d (list (d (n "basic-text-internals") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1c88j394qvx3iwisgfplxkfx248f6hzvfs40d72zc5ylxfyp7bd8")))

(define-public crate-basic-text-literals-0.9.1 (c (n "basic-text-literals") (v "0.9.1") (d (list (d (n "basic-text-internals") (r "^0.9.1-alpha.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "0lcdrik1fjyddnrq8r57s8lx97yyq94gvynkcbfnqqzl7sap5km6")))

(define-public crate-basic-text-literals-0.10.0 (c (n "basic-text-literals") (v "0.10.0") (d (list (d (n "basic-text-internals") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1i4qg1rxlrbp3v8lxhn0w3d1b5mjkpm2r781lqlzb8cmwsg40jc9")))

(define-public crate-basic-text-literals-0.11.0 (c (n "basic-text-literals") (v "0.11.0") (d (list (d (n "basic-text-internals") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "0255pcrbpmnw5rcc8k4ilnnrv9r9jvbqchlgkmdd3m44kp2ym573")))

(define-public crate-basic-text-literals-0.12.0 (c (n "basic-text-literals") (v "0.12.0") (d (list (d (n "basic-text-internals") (r "^0.12.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1l3vmy0mvzdksf9wndnhpjgq8k8lfsswgbslw2xvixxs8nk8piad")))

(define-public crate-basic-text-literals-0.13.0 (c (n "basic-text-literals") (v "0.13.0") (d (list (d (n "basic-text-internals") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "08gmcrgqa88kfkg3il3zx9sqpmimh4c6v5xfwqdq64bb81cmjlxg")))

(define-public crate-basic-text-literals-0.14.0 (c (n "basic-text-literals") (v "0.14.0") (d (list (d (n "basic-text-internals") (r "^0.14.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "07l5j6vb4qfvfwyyqxfwhwqch19hwv61cla5dzcp1c25794357pi")))

(define-public crate-basic-text-literals-0.15.0 (c (n "basic-text-literals") (v "0.15.0") (d (list (d (n "basic-text-internals") (r "^0.15.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "0kxs19d08a44xy56xq3im7an5dmm412lki538iqr72yv0xv367gj")))

(define-public crate-basic-text-literals-0.16.0 (c (n "basic-text-literals") (v "0.16.0") (d (list (d (n "basic-text-internals") (r "^0.16.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "02fqkfsjw93yw2m6vckf818ln1ivvx5k2awbhw7d5c31y36nfly1")))

(define-public crate-basic-text-literals-0.17.0 (c (n "basic-text-literals") (v "0.17.0") (d (list (d (n "basic-text-internals") (r "^0.17.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "15rs9w6gbdpkqjcbmy2kqk1ghwj8j5vzi42c80wnq48ylazi6whf")))

(define-public crate-basic-text-literals-0.18.0 (c (n "basic-text-literals") (v "0.18.0") (d (list (d (n "basic-text-internals") (r "^0.18.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "0a9a946kx7fi9rjhz8ir8n9x9zhd7ax0p2xiscrxrdkn7hf6llg3")))

(define-public crate-basic-text-literals-0.19.0 (c (n "basic-text-literals") (v "0.19.0") (d (list (d (n "basic-text-internals") (r "^0.19.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "1ziv72rr9lmz40p0bwld28fka5ixb0ccrdigbq1v9vlb0mma6ypm")))

(define-public crate-basic-text-literals-0.19.1 (c (n "basic-text-literals") (v "0.19.1") (d (list (d (n "basic-text-internals") (r "^0.19.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "0hnj75warh2n4dq14527g3c0hdbic1bihsr4w9ihvhnk3aahnvz6")))

(define-public crate-basic-text-literals-0.19.2 (c (n "basic-text-literals") (v "0.19.2") (d (list (d (n "basic-text-internals") (r "^0.19.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "1g1k529h9wjjp3kp55mrjpq002xcys4bl81iacr0pwwz3m77adwh")))

