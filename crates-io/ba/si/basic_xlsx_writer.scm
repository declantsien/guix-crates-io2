(define-module (crates-io ba si basic_xlsx_writer) #:use-module (crates-io))

(define-public crate-basic_xlsx_writer-0.1.0 (c (n "basic_xlsx_writer") (v "0.1.0") (d (list (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "0lwbmksc0rkh5b4cbbypd9c7hhr0ghf92znnw2kpb803a4nglhi1")))

(define-public crate-basic_xlsx_writer-0.1.1 (c (n "basic_xlsx_writer") (v "0.1.1") (d (list (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "0y1pw45h65dvnh972gs6yrrr9gj9ggspfx3fzkryp3pv0vqkwrkr")))

(define-public crate-basic_xlsx_writer-0.1.2 (c (n "basic_xlsx_writer") (v "0.1.2") (d (list (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "1ib94da9l8yklvymgwy6ydis3sfbslfmmwyrmxdivhz2xl2sn2kf")))

(define-public crate-basic_xlsx_writer-0.1.3 (c (n "basic_xlsx_writer") (v "0.1.3") (d (list (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "1w838mbqb4mkrrr448lpymgrixp4mizgl7fi77zn1c5bav0ajpar")))

