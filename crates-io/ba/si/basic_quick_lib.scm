(define-module (crates-io ba si basic_quick_lib) #:use-module (crates-io))

(define-public crate-basic_quick_lib-0.1.0 (c (n "basic_quick_lib") (v "0.1.0") (h "0l1i204k6ylr5di8bn53cvsx9l8rmjkay8vrbyv71gcsayrk690k")))

(define-public crate-basic_quick_lib-0.1.2 (c (n "basic_quick_lib") (v "0.1.2") (h "0r2a14yn594n0wc548x4by2ja8965lj2s8qxj08x34k5dnr0ynbh")))

(define-public crate-basic_quick_lib-0.1.3 (c (n "basic_quick_lib") (v "0.1.3") (h "0nzwgdqkvjinchjdpmsslfcmbhbgfddj431q651g25kcnshqia6k")))

(define-public crate-basic_quick_lib-0.2.0 (c (n "basic_quick_lib") (v "0.2.0") (d (list (d (n "clearscreen") (r "^1.0.9") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "0yyxygdcvzfb5a1q4zvbxfn9yl6kxrbr89ri006qgwyb4klsc5ry")))

(define-public crate-basic_quick_lib-0.3.0 (c (n "basic_quick_lib") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clearscreen") (r "^1.0.9") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "145iznd64b78gglm8rf31dwa1wmwqhjiw70b6v493jkg0c34j2fh")))

