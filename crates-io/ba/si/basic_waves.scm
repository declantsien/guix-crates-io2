(define-module (crates-io ba si basic_waves) #:use-module (crates-io))

(define-public crate-basic_waves-0.1.0 (c (n "basic_waves") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 0)))) (h "0xa987qaa3x9ca93giy8ib5hla5kzb2p0ycyc46a9qzvsl0w03i8")))

(define-public crate-basic_waves-0.1.1 (c (n "basic_waves") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (d #t) (k 0)))) (h "03xbfid8aw24m60yfal9lqcaqzd38xfng2l08f125y5qnrfa9vq4")))

