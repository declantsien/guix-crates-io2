(define-module (crates-io ba si basin2-lib) #:use-module (crates-io))

(define-public crate-basin2-lib-152.1.0 (c (n "basin2-lib") (v "152.1.0") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0r0fc3gp1vgnnhwpzjibvc26syd39adgxypyqy434jwj9ql60vy6")))

