(define-module (crates-io ba si basis-universal-sys) #:use-module (crates-io))

(define-public crate-basis-universal-sys-0.0.1 (c (n "basis-universal-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0r9ylwrc4km5rra3xnayf6r5g0kb5gyffgnjll7vnhpn3zjfxp6q")))

(define-public crate-basis-universal-sys-0.0.2 (c (n "basis-universal-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0bkfvf588xn2ahm62xydfi73j8w9rzcaw6m5i0amd4y1pnwhzj15")))

(define-public crate-basis-universal-sys-0.0.3 (c (n "basis-universal-sys") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "04d0dzx55d3fk8v9glclcl5riwml9lby3qxsa8jsrhp08fapj7sa")))

(define-public crate-basis-universal-sys-0.1.0 (c (n "basis-universal-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "01dkrxz8zk5gzdhh9ynw7nyi76h33qa0x2rrbdxsfqgc9x1hz0bg")))

(define-public crate-basis-universal-sys-0.1.1 (c (n "basis-universal-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0rnmcg4pzxacj4s598lpg9nf7750an3rjnzj3flibqwh0gdhgqky")))

(define-public crate-basis-universal-sys-0.2.0 (c (n "basis-universal-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0hznjzqivv87vlpas28j4d4znrhdi0kbcg2wa8kzq95pkkian3l4")))

(define-public crate-basis-universal-sys-0.3.0 (c (n "basis-universal-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1zkxr1qhr758mxa6d7x3cmsyd18ykh1d2qhlc53j7waszynwdmlz")))

(define-public crate-basis-universal-sys-0.3.1 (c (n "basis-universal-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "01l5bvrnr4ari7pp5qy8w3ar3kzdg5wgqybxwyq8z5a7jmgdx6zx")))

