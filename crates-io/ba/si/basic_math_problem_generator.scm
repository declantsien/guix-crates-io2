(define-module (crates-io ba si basic_math_problem_generator) #:use-module (crates-io))

(define-public crate-basic_math_problem_generator-0.1.0 (c (n "basic_math_problem_generator") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1q03266cshknnqcif4jzk5idx68iraijg3z2bbk2mkf89s6bdxd4")))

(define-public crate-basic_math_problem_generator-0.1.1 (c (n "basic_math_problem_generator") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gpq35f0ccnxra2lavbbr38jypi3vg2sr87vpsk4lhp4ji0ck39m")))

(define-public crate-basic_math_problem_generator-0.1.2 (c (n "basic_math_problem_generator") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1q2ydadb2sqpsfr7wz8y4yj053p35pb4w5kmr9823hg24qrcnhzz")))

(define-public crate-basic_math_problem_generator-0.1.3 (c (n "basic_math_problem_generator") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1msqx41llyx8d4zsik6hlqm8rsjyyvy1c7jzpr0g7whwpngqvdhl")))

