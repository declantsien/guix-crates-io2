(define-module (crates-io ba si basic_log) #:use-module (crates-io))

(define-public crate-basic_log-0.1.0 (c (n "basic_log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0g6684z3fbhjdwd9z8m4a23dn0rq28wd528qpc4zk9midpg2q3qc")))

(define-public crate-basic_log-0.1.1 (c (n "basic_log") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0bbjbrxvl4x1cz8dyvf9h2bmdjdy5cfj8rnvspk25h09gnvlja65")))

(define-public crate-basic_log-0.2.0 (c (n "basic_log") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0qyjs6w1gx54yya30h4qkbbpa432kzkykyh9vrnjnfq98wvr5955")))

