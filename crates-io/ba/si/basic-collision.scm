(define-module (crates-io ba si basic-collision) #:use-module (crates-io))

(define-public crate-basic-collision-0.1.0 (c (n "basic-collision") (v "0.1.0") (h "1083yvp42r4jn7db4ngzdvlm4g9fyhj4qqni304lpsmqwv4i3yd2")))

(define-public crate-basic-collision-0.1.1 (c (n "basic-collision") (v "0.1.1") (h "17lqhhc02zbhfcy9901slqdav7w24giydxgpn6vcalw8p8p03b25")))

(define-public crate-basic-collision-0.2.0 (c (n "basic-collision") (v "0.2.0") (h "1ia4c1n1l7jqnirn495mgk00ghy65fw23i015higwzr3mrk8v3b6")))

(define-public crate-basic-collision-0.2.1 (c (n "basic-collision") (v "0.2.1") (h "06f08ifc1h7034bnsnn5hnj18gkw5i79dyg0z560smahy41kywn9")))

(define-public crate-basic-collision-0.3.0 (c (n "basic-collision") (v "0.3.0") (h "0jc05c7qi9r4jlizwq73qn7m7j5ddlzv1748cijyz6n7m50vmybg")))

(define-public crate-basic-collision-0.3.1 (c (n "basic-collision") (v "0.3.1") (h "1nlbb0kbbaz5mnpsh713va6svbwzp54jfmil2fnb1r8bf06p3fkz") (y #t)))

