(define-module (crates-io ba si basic-cookies) #:use-module (crates-io))

(define-public crate-basic-cookies-0.1.0 (c (n "basic-cookies") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.16") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1mpxfckyry2vj42pv9dr6lgxdlfskrp7xxy298v1ldksms649yd6")))

(define-public crate-basic-cookies-0.1.1 (c (n "basic-cookies") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.16") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1krnabwwj58v5w03xpxjf901nhwwvf539qnm09wm0nzrg8apys3f")))

(define-public crate-basic-cookies-0.1.2 (c (n "basic-cookies") (v "0.1.2") (d (list (d (n "lalrpop") (r "^0.16") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "19yh367075gxmh6p82pfw8x8cqv835f6914fsyjk3fcmrzilidij")))

(define-public crate-basic-cookies-0.1.3 (c (n "basic-cookies") (v "0.1.3") (d (list (d (n "lalrpop") (r "^0.16") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "05gd1s56bvinrgm3bszwpmlwjzy1yh4gjr01lffnw0il5n33frr6")))

(define-public crate-basic-cookies-0.1.4 (c (n "basic-cookies") (v "0.1.4") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0f5vlhr79k6z07989bvbjv4mzh0174xyaqmi2gqwf97r2nrvclyb")))

(define-public crate-basic-cookies-0.1.5 (c (n "basic-cookies") (v "0.1.5") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1xwnmmcn32m18nis7azfxylkqyhirkqcag94i23b1g8n5ka8zgb7")))

