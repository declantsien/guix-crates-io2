(define-module (crates-io ba si basic-auth-raw) #:use-module (crates-io))

(define-public crate-basic-auth-raw-0.1.0 (c (n "basic-auth-raw") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.1") (d #t) (k 0)) (d (n "rocket") (r "^0.3.10") (d #t) (k 0)))) (h "08zlh1gr5wld66cvya8md34qiyjp1dqyigfwamjcazycw1dh5cnq")))

