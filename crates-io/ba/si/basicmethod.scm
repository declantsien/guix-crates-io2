(define-module (crates-io ba si basicmethod) #:use-module (crates-io))

(define-public crate-basicmethod-0.1.0 (c (n "basicmethod") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "11d7ps9hv55q3ky6jp490b5m9yrs3m0jgqd20wdwd375fxx5k4i3")))

(define-public crate-basicmethod-0.1.1 (c (n "basicmethod") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "02fg2pyy7gkcqwfnn1s5k3lj5gzwv75r4gl0ixbqzb9pz1ji9ngc")))

(define-public crate-basicmethod-0.1.2 (c (n "basicmethod") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "15qd7x58p3mzdw84fxzl02446kj0ymshy11ll8gz57cc779nz9h8")))

