(define-module (crates-io ba si basichll) #:use-module (crates-io))

(define-public crate-basichll-0.1.6 (c (n "basichll") (v "0.1.6") (h "13lnagf9252r4b52ab5xc4ahifqzmb0lvmdpf3b37qgg0srb9ld1")))

(define-public crate-basichll-0.1.7 (c (n "basichll") (v "0.1.7") (h "0w0acps840scg7dc65s9jv5pdmrvbvsnhrd6fsxs9vjr79q7icd0")))

(define-public crate-basichll-0.1.8 (c (n "basichll") (v "0.1.8") (h "01mq56njkw3yyd48zsw04nywm8ipfb56ndyahqpdcjbkx5p3811a")))

(define-public crate-basichll-0.2.0 (c (n "basichll") (v "0.2.0") (h "1pbrk35airjcfm36yfk5gzwx9apjfk0kfq8nfi98hxz4fz5rfj6k")))

(define-public crate-basichll-0.2.1 (c (n "basichll") (v "0.2.1") (h "1kbyz4zisrv7ylm3zs45y5n4206z3riazgbwjc6yz84kr2v086zk")))

(define-public crate-basichll-0.2.2 (c (n "basichll") (v "0.2.2") (h "1slnghm5q9kxjcwa6mbv2qphcpiszwkxlycaqm4vfb03vbh4dxlq")))

(define-public crate-basichll-0.3.0 (c (n "basichll") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "0x03ljq2rgg7idjnc4l7l3mxwkf7bzqq2mzx8nsqfikg2mmj913w")))

(define-public crate-basichll-0.3.1 (c (n "basichll") (v "0.3.1") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "16885b239h5yaz5nrkvh2ydj65zil2fmljnjyxx6dr60nf40l8g7")))

