(define-module (crates-io ba si basic-reverb) #:use-module (crates-io))

(define-public crate-basic-reverb-0.1.0 (c (n "basic-reverb") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zz6gs2q84xjz2jkzj4m3z8qbr1v18hvd71f4f7v6aj31pr0var8")))

(define-public crate-basic-reverb-0.1.1 (c (n "basic-reverb") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0z19vv81z11a713d2fz57h7svbi0b4n7ma1lx8wii8pvvnxf2282")))

(define-public crate-basic-reverb-0.1.2 (c (n "basic-reverb") (v "0.1.2") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1178ba2idgfyl8laxfzram54989svlqkln21x3hd4g5mn25jmxfx")))

(define-public crate-basic-reverb-0.1.3 (c (n "basic-reverb") (v "0.1.3") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dw0mp9wvk8pwa6nd0xrrkbk01zcb2pv0f4llylj0s84cmjybhnl")))

