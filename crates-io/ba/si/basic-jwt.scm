(define-module (crates-io ba si basic-jwt) #:use-module (crates-io))

(define-public crate-basic-jwt-0.1.0 (c (n "basic-jwt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "elliptic-curve") (r "^0.13.8") (f (quote ("pkcs8" "pem"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9.3.0") (d #t) (k 0)) (d (n "p384") (r "^0.13.0") (f (quote ("ecdsa" "pkcs8" "pem"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)))) (h "168n25sxwmg4zdyi0gzgq447r6swysw4x409qfmk6p8yhlwql410")))

(define-public crate-basic-jwt-0.2.0 (c (n "basic-jwt") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "elliptic-curve") (r "^0.13.8") (f (quote ("pkcs8" "pem"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9.3.0") (d #t) (k 0)) (d (n "p384") (r "^0.13.0") (f (quote ("ecdsa" "pkcs8" "pem"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wgsk35y05ax7kw3k2b2jvmz6mm9812pkp7brsqr3w4j05wgn6kl")))

