(define-module (crates-io ba si basis-universal) #:use-module (crates-io))

(define-public crate-basis-universal-0.0.1 (c (n "basis-universal") (v "0.0.1") (d (list (d (n "basis-universal-sys") (r "=0.0.1") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0hxmvd5ihasx70m662x7cay0rz9d0yrz0f4v402rmakj6nwfd42x")))

(define-public crate-basis-universal-0.0.2 (c (n "basis-universal") (v "0.0.2") (d (list (d (n "basis-universal-sys") (r "=0.0.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0l1sv7bxx6a1swdzsvxhyxx7j2jxb3mgygs8i8kgs3yh4zn9ykyg")))

(define-public crate-basis-universal-0.0.3 (c (n "basis-universal") (v "0.0.3") (d (list (d (n "basis-universal-sys") (r "=0.0.3") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "07fc7n2wjfx22mch16wrm92n1y7x2bp2yxlyq19b2w1fqrknbifw")))

(define-public crate-basis-universal-0.1.0 (c (n "basis-universal") (v "0.1.0") (d (list (d (n "basis-universal-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0faan9j84nywvpz1n2g3igv2bwh9pc79d44yabzsvj5iw49zyqf4")))

(define-public crate-basis-universal-0.1.1 (c (n "basis-universal") (v "0.1.1") (d (list (d (n "basis-universal-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23") (d #t) (k 2)))) (h "0kisirafgfrjadylvabizpjmvcvxa58vyafwcl3vb8x1z2va9r6h")))

(define-public crate-basis-universal-0.2.0 (c (n "basis-universal") (v "0.2.0") (d (list (d (n "basis-universal-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23") (d #t) (k 2)))) (h "0h11ks4rwx5kvr433gx4cazjbh6a7hz0l0lid8k97ap27s1hqdz4")))

(define-public crate-basis-universal-0.3.0 (c (n "basis-universal") (v "0.3.0") (d (list (d (n "basis-universal-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23") (d #t) (k 2)))) (h "18ryf58jqxx9g92k6by8ycd7xvp4pqv9hpfq6ndjpac8x9zp4y6q")))

(define-public crate-basis-universal-0.3.1 (c (n "basis-universal") (v "0.3.1") (d (list (d (n "basis-universal-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23") (d #t) (k 2)))) (h "055xgkkqix8hyr0vln15rc7fnrzimh7lhfmrysi2zqgl15bv0psm")))

