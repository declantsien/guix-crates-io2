(define-module (crates-io ba si basic-authority) #:use-module (crates-io))

(define-public crate-basic-authority-0.0.0 (c (n "basic-authority") (v "0.0.0") (h "1c7w5hdf99qv0csfra5nxl2crj4lv8jxx5kgj68lxl8rkw96fcgm")))

(define-public crate-basic-authority-0.1.0 (c (n "basic-authority") (v "0.1.0") (d (list (d (n "client-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "common-types") (r "^0.1.0") (d #t) (k 0)) (d (n "enjen") (r "^0.1.0") (d #t) (k 0)) (d (n "enjen") (r "^0.1.0") (f (quote ("test-helpers"))) (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mashina") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tetsy-crypto") (r "^0.4.2") (f (quote ("publickey"))) (d #t) (k 0)) (d (n "tetsy-keccak-hash") (r "^0.4.0") (d #t) (k 2)) (d (n "tetsy-rlp") (r "^0.4.5") (d #t) (k 0)) (d (n "validator-set") (r "^0.1.0") (d #t) (k 0)) (d (n "vapjson") (r "^0.1.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "1ws5q6jaci002g1nzqkkmgav09p9qh2lrw1j9ql5a79zavl2fx40") (f (quote (("test-helpers"))))))

