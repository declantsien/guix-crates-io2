(define-module (crates-io ba si basic-logger) #:use-module (crates-io))

(define-public crate-basic-logger-0.1.0 (c (n "basic-logger") (v "0.1.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0jwsmdrw3k61jmlxn30n5zczrmc9iv7fmw5m72nqxl75cajzf44s") (f (quote (("default" "colors") ("colors" "colored")))) (y #t)))

(define-public crate-basic-logger-0.1.1 (c (n "basic-logger") (v "0.1.1") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1i0z20qpxhc3is9fcinrr5d325qm84iglvrsmgpan18l00z0qf14") (f (quote (("default" "colors") ("colors" "colored")))) (y #t)))

(define-public crate-basic-logger-0.1.2 (c (n "basic-logger") (v "0.1.2") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1yg893ckzk6gmbg8sqdlsk2mqd4270az14q5q0n4jxlilrcbyjgl") (f (quote (("default" "colors") ("colors" "colored"))))))

