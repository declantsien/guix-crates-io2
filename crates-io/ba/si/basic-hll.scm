(define-module (crates-io ba si basic-hll) #:use-module (crates-io))

(define-public crate-basic-hll-0.0.1 (c (n "basic-hll") (v "0.0.1") (h "1fhd5dzn3nfp53vp97x7x38saycwhlfb9xzk6yfzkc8ha5zi3dp5")))

(define-public crate-basic-hll-0.0.2 (c (n "basic-hll") (v "0.0.2") (d (list (d (n "algebra") (r "*") (d #t) (k 0)))) (h "02s31fr1jsbzgs51mylcsrs3mm7lriw4vj9z3jqhgmk7q9gpcig8")))

(define-public crate-basic-hll-0.0.3 (c (n "basic-hll") (v "0.0.3") (h "1lyhx7v144szqvg4nffvpswvvc8h061f2hh6gmh2cwd4ik92fbhx")))

(define-public crate-basic-hll-0.0.4 (c (n "basic-hll") (v "0.0.4") (h "0852gsd0jsdwsyqs2522p0zdg8vpizyfwfm9bgj16mjrmp1d5h2m")))

(define-public crate-basic-hll-0.0.6 (c (n "basic-hll") (v "0.0.6") (h "16xgv9sh1m1m3265p7ll8j1bb1gp05vf0m9vpnkc9awvj58isk5p")))

