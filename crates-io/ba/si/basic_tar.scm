(define-module (crates-io ba si basic_tar) #:use-module (crates-io))

(define-public crate-basic_tar-0.1.0 (c (n "basic_tar") (v "0.1.0") (h "0vxwi4m6cg23a38ysd7andlxvq3vfhnrm73sc2l4z4a9jh191g9d")))

(define-public crate-basic_tar-0.1.1 (c (n "basic_tar") (v "0.1.1") (h "1xrqw1jh538qxvq5qfa4pqy7w9i89ln1vkb7xw3p84xfychcnxrw")))

