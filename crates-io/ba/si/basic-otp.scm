(define-module (crates-io ba si basic-otp) #:use-module (crates-io))

(define-public crate-basic-otp-0.1.0 (c (n "basic-otp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1.3") (d #t) (k 0)))) (h "10apch0zn7w76wawffakxyxh8jbqi3102h7kiis5wdl1p2v7hvv0")))

(define-public crate-basic-otp-0.1.1 (c (n "basic-otp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "hmac-sha1") (r "^0.1.3") (d #t) (k 0)))) (h "1z3mbyczlxlnkg8awzk35wjhv1i2nwnrfsfi1dmn5hmf2iks6alp")))

