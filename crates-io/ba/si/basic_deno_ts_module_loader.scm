(define-module (crates-io ba si basic_deno_ts_module_loader) #:use-module (crates-io))

(define-public crate-basic_deno_ts_module_loader-0.1.0 (c (n "basic_deno_ts_module_loader") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "deno_ast") (r "^0.27.3") (f (quote ("transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.195.0") (d #t) (k 0)) (d (n "deno_runtime") (r "^0.120.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("fs"))) (d #t) (k 0)))) (h "1vkl9djz5z49id61dqj4xv4llypzqydlv5mwwncgn0bcrvl0daw1")))

(define-public crate-basic_deno_ts_module_loader-0.1.1 (c (n "basic_deno_ts_module_loader") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "deno_ast") (r "^0.27.3") (f (quote ("transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.195.0") (d #t) (k 0)) (d (n "deno_runtime") (r "^0.120.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("fs"))) (d #t) (k 0)))) (h "0cajfmc29fgswf9a27h72jdcpjq3sbjpa45sllkm1mm237a39ybd")))

