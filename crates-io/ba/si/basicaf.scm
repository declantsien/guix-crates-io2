(define-module (crates-io ba si basicaf) #:use-module (crates-io))

(define-public crate-basicaf-0.1.0 (c (n "basicaf") (v "0.1.0") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "nom") (r "^3.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1ci1ip0lxg7453g1cqp6cq451aw17ghvpmks0syw37yyrbm3hhx3")))

(define-public crate-basicaf-0.1.1 (c (n "basicaf") (v "0.1.1") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "nom") (r "^3.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1kmprc9qknqizq4jqknw5xpmlgblaqm174nmxmqb5gv06jzxn7f0")))

(define-public crate-basicaf-0.1.2 (c (n "basicaf") (v "0.1.2") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "nom") (r "^3.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "16pdxvipnnqzrgywgf3p2kpr3b2lp7rd0j78j6c9wzqvw2png5li")))

