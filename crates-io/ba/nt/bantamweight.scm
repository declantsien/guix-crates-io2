(define-module (crates-io ba nt bantamweight) #:use-module (crates-io))

(define-public crate-bantamweight-0.1.0 (c (n "bantamweight") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dmgsbk6mk3n1xsmqgmd652xlc9l3f55yy208prmjisddrlnrlmm") (y #t)))

(define-public crate-bantamweight-0.1.1 (c (n "bantamweight") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kqsdn3by409qzjh1xmfwf7gqlb0zjw0r5s2ci83w13hv5d58iym")))

(define-public crate-bantamweight-0.1.2 (c (n "bantamweight") (v "0.1.2") (d (list (d (n "plain-binary-stream") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16mm6ikwh8frphxhdp9p8l9sj3wl7dpims4x8v44xmlv9z6r0qbv")))

(define-public crate-bantamweight-0.1.21 (c (n "bantamweight") (v "0.1.21") (d (list (d (n "plain-binary-stream") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hy7nd35rnxr52gggc1jzhxh1f7gj5000c2hlmnpw2ln7pwcv1q2")))

