(define-module (crates-io ba za bazaar) #:use-module (crates-io))

(define-public crate-bazaar-3.3.3 (c (n "bazaar") (v "3.3.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "03gqj37zi0r0sm0hcdgl0fq581lfh5ipz8w0i90abb7sg1xnlbmv")))

(define-public crate-bazaar-3.3.4 (c (n "bazaar") (v "3.3.4") (d (list (d (n "bendy") (r "^0.3") (d #t) (k 0)) (d (n "breezy-osutils") (r ">=3.3.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fancy-regex") (r ">=0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "04rgwihyyqj7yk6n31x17ybx846l9aa7i7l6dj037rb4gfvvhfaf")))

