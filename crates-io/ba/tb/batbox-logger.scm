(define-module (crates-io ba tb batbox-logger) #:use-module (crates-io))

(define-public crate-batbox-logger-0.15.0 (c (n "batbox-logger") (v "0.15.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "06hdszymdyv9prxj0rp0fd6i2whblbvsilzgd4xfblaw3334pmla")))

(define-public crate-batbox-logger-0.16.0 (c (n "batbox-logger") (v "0.16.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (t "cfg(target_arch=\"wasm32\")") (k 0)))) (h "0j1r6ca0wwxzr01z254qdaz3r5pdcjknjnqjrk1k5m2x7b3b9924")))

