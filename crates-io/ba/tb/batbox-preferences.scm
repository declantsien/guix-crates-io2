(define-module (crates-io ba tb batbox-preferences) #:use-module (crates-io))

(define-public crate-batbox-preferences-0.15.0 (c (n "batbox-preferences") (v "0.15.0") (d (list (d (n "directories") (r "^4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Storage"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0lk1csyij6j5h106bspnppimr5031sxvznmcwpwqb0zm0q4z6ra6")))

(define-public crate-batbox-preferences-0.16.0 (c (n "batbox-preferences") (v "0.16.0") (d (list (d (n "directories") (r "^5") (d #t) (t "cfg(not(target_arch=\"wasm32\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Storage"))) (d #t) (t "cfg(target_arch=\"wasm32\")") (k 0)))) (h "007ibqfaqmyfwi5mqk4fllg1fsgxl774rcjpwyjb8p7nkds8fs8g")))

