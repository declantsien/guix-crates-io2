(define-module (crates-io ba tb batbox-diff) #:use-module (crates-io))

(define-public crate-batbox-diff-0.15.0 (c (n "batbox-diff") (v "0.15.0") (d (list (d (n "batbox-diff-derive") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0blm7da77s3aihx0fc4l4qrb63vxhnki2mbki6av5q7asg94w3x2")))

(define-public crate-batbox-diff-0.16.0 (c (n "batbox-diff") (v "0.16.0") (d (list (d (n "batbox-diff-derive") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1ighjcdryw66fkw4kynzr8g00sxdnbjcgkb0y7mrfr241sngrq88")))

