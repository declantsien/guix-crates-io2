(define-module (crates-io ba tb batbox-approx) #:use-module (crates-io))

(define-public crate-batbox-approx-0.15.0 (c (n "batbox-approx") (v "0.15.0") (d (list (d (n "batbox-num") (r "^0.15") (d #t) (k 0)))) (h "1cb8afddzfdifjb4a84p25dql4xgx0fxklak2vj8bvvnq6ni941h")))

(define-public crate-batbox-approx-0.16.0 (c (n "batbox-approx") (v "0.16.0") (d (list (d (n "batbox-num") (r "^0.16") (d #t) (k 0)))) (h "0vbyypmrm49lhil7056qm1apwfj9lnqy7gvnyndi83635blcp1z9")))

