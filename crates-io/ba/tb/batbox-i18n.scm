(define-module (crates-io ba tb batbox-i18n) #:use-module (crates-io))

(define-public crate-batbox-i18n-0.15.0 (c (n "batbox-i18n") (v "0.15.0") (d (list (d (n "batbox-i18n-macro") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Navigator"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "08i6bnypwg5yar90yyaa15vywdbydx68xmqdijcm2dhgqcwbd716")))

(define-public crate-batbox-i18n-0.16.0 (c (n "batbox-i18n") (v "0.16.0") (d (list (d (n "batbox-i18n-macro") (r "^0.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_arch=\"wasm32\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Navigator"))) (d #t) (t "cfg(target_arch=\"wasm32\")") (k 0)))) (h "1h7ni9d55hl8mw79mcb9jacykhpzxhwbjxmlvrkzscfs4agsx1vc")))

