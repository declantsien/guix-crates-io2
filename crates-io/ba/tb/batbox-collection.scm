(define-module (crates-io ba tb batbox-collection) #:use-module (crates-io))

(define-public crate-batbox-collection-0.15.0 (c (n "batbox-collection") (v "0.15.0") (d (list (d (n "batbox-collection-derive") (r "^0.15") (d #t) (k 0)) (d (n "batbox-diff") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "13bydaknphnwjrba8lwfysqpgs0498jvwn1gll84swg1p4c9bzj5")))

(define-public crate-batbox-collection-0.16.0 (c (n "batbox-collection") (v "0.16.0") (d (list (d (n "batbox-collection-derive") (r "^0.16") (d #t) (k 0)) (d (n "batbox-diff") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "12r5qf7i9jbg3aif46c8h04ahs0v7ys4b8jqhfy3xxd6cp4wibz8")))

