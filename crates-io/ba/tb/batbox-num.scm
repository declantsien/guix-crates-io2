(define-module (crates-io ba tb batbox-num) #:use-module (crates-io))

(define-public crate-batbox-num-0.15.0 (c (n "batbox-num") (v "0.15.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1r4bbdqclfnjxqd5c40b8rlpd5wqbg28ysfiy461vjpv207aya5i")))

(define-public crate-batbox-num-0.16.0 (c (n "batbox-num") (v "0.16.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(target_arch=\"wasm32\")") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0qaifjgdr6qxl12mslmfs31psvs1li1al3cfacxwrsv7wscj0k4s")))

