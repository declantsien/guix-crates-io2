(define-module (crates-io ba tb batbox-cli) #:use-module (crates-io))

(define-public crate-batbox-cli-0.15.0 (c (n "batbox-cli") (v "0.15.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Location"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0x4iwgnnk98zq4ykd4cdvnkrldgdzwkmw2aavcnmpsypq2a8js7g")))

(define-public crate-batbox-cli-0.16.0 (c (n "batbox-cli") (v "0.16.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (t "cfg(target_arch=\"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Location"))) (d #t) (t "cfg(target_arch=\"wasm32\")") (k 0)))) (h "0brj2imavy5z7bw2b1n1kmnbhk4jj0ylm4sv88dv8873byxgn166")))

