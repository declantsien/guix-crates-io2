(define-module (crates-io ba tb batbox-derive) #:use-module (crates-io))

(define-public crate-batbox-derive-0.1.0 (c (n "batbox-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19hjdvdliffbrgcnxf4dlzady5spwg9bjnracm45przl2nkrkibh")))

(define-public crate-batbox-derive-0.5.0-alpha.0 (c (n "batbox-derive") (v "0.5.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1q2xfc0r4sq2j7nhmxj0c6fhbz3gnpzymxiy2cjlbd0ghxx57fz9")))

(define-public crate-batbox-derive-0.5.0 (c (n "batbox-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06ijp3bqs6gqg8yy2y6x2rida2zrcdjyir7yhldw6jzzyn2q3f4y")))

(define-public crate-batbox-derive-0.6.0-alpha.0 (c (n "batbox-derive") (v "0.6.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0q59zw68dr9lil3v243mkfxk41s1jf40p153nqpwz5y54s0zinln")))

(define-public crate-batbox-derive-0.6.0-alpha.1 (c (n "batbox-derive") (v "0.6.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1096bia7bfv1qj6fw7s66bnmwmy7jilbfzmbbzhdxgw9fb2l7cby")))

(define-public crate-batbox-derive-0.6.0 (c (n "batbox-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1scmainbs3qjs79k8zf2i3sw8lkad4qhi44qcps1869w3qy0cvr1")))

(define-public crate-batbox-derive-0.7.0-alpha.0 (c (n "batbox-derive") (v "0.7.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1k93dnkl5jj5yqprp2c7ghkb9kklfyjgnlvb25hgyw9jcby1012n")))

(define-public crate-batbox-derive-0.7.0-alpha.1 (c (n "batbox-derive") (v "0.7.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lgfpl6vs6xf5fahi2dz0g1q5c23r16jq6h5zqwrydyag8vrc10a")))

(define-public crate-batbox-derive-0.8.0 (c (n "batbox-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qkdhcmwmf8a6xrlasfp8mihsnz0kcj558vd7hd9aar5hg3fyv90")))

(define-public crate-batbox-derive-0.9.0 (c (n "batbox-derive") (v "0.9.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0cdfcwzmynywrm05g169f427dp1lkym0xbcj64sywxxh6vy6576j")))

(define-public crate-batbox-derive-0.11.0 (c (n "batbox-derive") (v "0.11.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1w4w80w6rais8xb6hziqn7ril64s0szjq98nr71nw1279xrdmzhz")))

(define-public crate-batbox-derive-0.13.0 (c (n "batbox-derive") (v "0.13.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08q4mcmscrrqg21zlx15591s074mw62wkdh19a8kn2y19pzaypfm")))

