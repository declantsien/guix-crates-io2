(define-module (crates-io ba tb batbox-collection-derive) #:use-module (crates-io))

(define-public crate-batbox-collection-derive-0.15.0 (c (n "batbox-collection-derive") (v "0.15.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1lxfl0jvdamrw0l6472zbvd0hbc2w4inx0yz5w3dndj166smiwzq")))

(define-public crate-batbox-collection-derive-0.16.0 (c (n "batbox-collection-derive") (v "0.16.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1mjf0gv89j8qkz5l2xgbgl68ni4dcxxq660bbi5d5gk0wpikdn5x")))

