(define-module (crates-io ba tb batbox-la) #:use-module (crates-io))

(define-public crate-batbox-la-0.15.0 (c (n "batbox-la") (v "0.15.0") (d (list (d (n "batbox-approx") (r "^0.15") (d #t) (k 0)) (d (n "batbox-cmp") (r "^0.15") (d #t) (k 0)) (d (n "batbox-num") (r "^0.15") (d #t) (k 0)) (d (n "batbox-range") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "13gnh31j97agqg8bd6g5ibb04f4a035llxk2p6rpb4pwc7a7skv1")))

(define-public crate-batbox-la-0.16.0 (c (n "batbox-la") (v "0.16.0") (d (list (d (n "batbox-approx") (r "^0.16") (d #t) (k 0)) (d (n "batbox-cmp") (r "^0.16") (d #t) (k 0)) (d (n "batbox-num") (r "^0.16") (d #t) (k 0)) (d (n "batbox-range") (r "^0.16") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "06v85cdfq9x6m3c8vrpx7fwzw6bzabcgfx4gi4kdd9nilpfszld9")))

