(define-module (crates-io ba tb batbox-cmp) #:use-module (crates-io))

(define-public crate-batbox-cmp-0.15.0 (c (n "batbox-cmp") (v "0.15.0") (d (list (d (n "batbox-range") (r "^0.15") (d #t) (k 0)))) (h "0scml31cccqprjmcg9ikgm2g2xd47fwgp6hflkkl0i8i697y8ga2")))

(define-public crate-batbox-cmp-0.16.0 (c (n "batbox-cmp") (v "0.16.0") (d (list (d (n "batbox-range") (r "^0.16") (d #t) (k 0)))) (h "1hg16vlkpsmsjh6rjsj7v4vqi1mrxkp0i59llwghff84xzadyfz2")))

