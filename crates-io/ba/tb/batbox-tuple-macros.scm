(define-module (crates-io ba tb batbox-tuple-macros) #:use-module (crates-io))

(define-public crate-batbox-tuple-macros-0.15.0 (c (n "batbox-tuple-macros") (v "0.15.0") (h "017ciqnxivb59xvvqak969fw5g7q9difhr1ysk88s8if94jvj4yy")))

(define-public crate-batbox-tuple-macros-0.16.0 (c (n "batbox-tuple-macros") (v "0.16.0") (h "1i3wp8nximycdijk2yrayi1kj09bmmspmmnzwa6hpxm1ynk4rsr6")))

