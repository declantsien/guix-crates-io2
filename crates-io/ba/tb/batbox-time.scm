(define-module (crates-io ba tb batbox-time) #:use-module (crates-io))

(define-public crate-batbox-time-0.15.0 (c (n "batbox-time") (v "0.15.0") (d (list (d (n "async-std") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0hw2rr3803wqs055xr7hi02jljxqnzpwar0q0hiy6pvp2kwgyw89")))

(define-public crate-batbox-time-0.16.0 (c (n "batbox-time") (v "0.16.0") (d (list (d (n "async-std") (r "^1") (d #t) (t "cfg(not(target_arch=\"wasm32\"))") (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(target_arch=\"wasm32\")") (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (t "cfg(target_arch=\"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "cfg(target_arch=\"wasm32\")") (k 0)))) (h "1d3m4swkyh6fg0bpwqqgbvr8md8a9c0zl09cw4mlzndqzkx7ir9z")))

