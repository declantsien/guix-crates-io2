(define-module (crates-io ba tb batbox-macros) #:use-module (crates-io))

(define-public crate-batbox-macros-0.10.0 (c (n "batbox-macros") (v "0.10.0") (h "020rkfdg5rvjz06ym1fc7izsrg9bld03dfahqiz63zywf8bwnbay")))

(define-public crate-batbox-macros-0.11.0 (c (n "batbox-macros") (v "0.11.0") (h "1787yq57xpm2rznlxbnp4yvfj96n2wqqrhy5jjz551199n7hs3m4")))

