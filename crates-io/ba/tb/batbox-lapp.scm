(define-module (crates-io ba tb batbox-lapp) #:use-module (crates-io))

(define-public crate-batbox-lapp-0.15.0 (c (n "batbox-lapp") (v "0.15.0") (d (list (d (n "batbox-approx") (r "^0.15") (d #t) (k 0)) (d (n "batbox-cmp") (r "^0.15") (d #t) (k 0)) (d (n "batbox-la") (r "^0.15") (d #t) (k 0)) (d (n "batbox-num") (r "^0.15") (d #t) (k 0)) (d (n "batbox-range") (r "^0.15") (d #t) (k 0)))) (h "1ha8yvi86px6bhdmin4ham8nwf2q13dwp12iqhwlpx4l12sm916s")))

(define-public crate-batbox-lapp-0.16.0 (c (n "batbox-lapp") (v "0.16.0") (d (list (d (n "batbox-approx") (r "^0.16") (d #t) (k 0)) (d (n "batbox-cmp") (r "^0.16") (d #t) (k 0)) (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "batbox-num") (r "^0.16") (d #t) (k 0)) (d (n "batbox-range") (r "^0.16") (d #t) (k 0)))) (h "06fpx82mh5267qmgaxgpcpvmd4m4m21br3rlx70q8plsfc2lcq9h")))

