(define-module (crates-io ba tb batbox-color) #:use-module (crates-io))

(define-public crate-batbox-color-0.15.0 (c (n "batbox-color") (v "0.15.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "batbox-approx") (r "^0.15") (d #t) (k 0)) (d (n "batbox-la") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "19pg5qnn9aykypjmvwgpxz94flr4xgszh3jfyfknkmys6pnz09lw")))

(define-public crate-batbox-color-0.16.0 (c (n "batbox-color") (v "0.16.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "batbox-approx") (r "^0.16") (d #t) (k 0)) (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0n716ic7rfl6ljmnq6p56nsqch4wqp5c20czyq4ks2l583kmjwq2")))

