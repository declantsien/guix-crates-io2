(define-module (crates-io ba tb batbox-range) #:use-module (crates-io))

(define-public crate-batbox-range-0.15.0 (c (n "batbox-range") (v "0.15.0") (h "1bd6gnl2lw421zmg9r8m63cxicr00kmw1jqbwg01yb7zrljvhhjr")))

(define-public crate-batbox-range-0.16.0 (c (n "batbox-range") (v "0.16.0") (h "188lyrw7s7kvg7iz5q4x0mqf2d6ffbd07hs41xylziikp6xdwnsj")))

