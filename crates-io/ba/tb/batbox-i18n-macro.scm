(define-module (crates-io ba tb batbox-i18n-macro) #:use-module (crates-io))

(define-public crate-batbox-i18n-macro-0.15.0 (c (n "batbox-i18n-macro") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0w2bik8pjj9lpzjzls86rybk0x7vp33zwsn5vfrfq91d0an5p4ac")))

(define-public crate-batbox-i18n-macro-0.16.0 (c (n "batbox-i18n-macro") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0f9ya45yvay8swgjymsyql220zlcn7xqkiyripxmgvji1vlsisyb")))

