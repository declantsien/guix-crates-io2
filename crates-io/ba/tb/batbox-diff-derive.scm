(define-module (crates-io ba tb batbox-diff-derive) #:use-module (crates-io))

(define-public crate-batbox-diff-derive-0.15.0 (c (n "batbox-diff-derive") (v "0.15.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1p833ss5h8f8kypzdxsy5lhpimx7rw8yk9dy13xrxsnc5pphsc04")))

(define-public crate-batbox-diff-derive-0.16.0 (c (n "batbox-diff-derive") (v "0.16.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "12jqidbf2j154kaff9fvyddyz1n57mlv9i2x19rg5g4dag25gz7w")))

