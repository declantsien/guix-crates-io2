(define-module (crates-io ba ye bayer) #:use-module (crates-io))

(define-public crate-bayer-0.1.0 (c (n "bayer") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "flic") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.28") (f (quote ("image"))) (d #t) (k 2)))) (h "1cbskm7skggk0fj1z4a311igyqkbp9813jcsfqgl9lqz9sks7k1n")))

(define-public crate-bayer-0.1.1 (c (n "bayer") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "flic") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.28") (f (quote ("image"))) (d #t) (k 2)))) (h "05yqppk8gl0aa8rai3gshzdlq9yam5b5d6b2ifvc390wcvg78cw9") (f (quote (("bench"))))))

(define-public crate-bayer-0.1.2 (c (n "bayer") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "flic") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.29") (f (quote ("image"))) (d #t) (k 2)))) (h "0s2bmvj01fcccicmr5b40wihxq7xzmsh429njy98382k2il5z27h") (f (quote (("default" "rayon") ("bench"))))))

(define-public crate-bayer-0.1.3 (c (n "bayer") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "flic") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.29") (f (quote ("image"))) (d #t) (k 2)))) (h "0vipf5ivyklsi7mzxq8817dxpp77hqq7bqr2sqf36dxgb0n6bq69") (f (quote (("default" "rayon") ("bench"))))))

(define-public crate-bayer-0.1.4 (c (n "bayer") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "flic") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rayon") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.29") (f (quote ("image"))) (d #t) (k 2)))) (h "1js2mrfr0gavj261ycmmaqamnbrd6k0b8a06y8qr79syycw2yxyl") (f (quote (("default" "rayon") ("bench"))))))

(define-public crate-bayer-0.1.5 (c (n "bayer") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "flic") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (f (quote ("image"))) (d #t) (k 2)))) (h "0fqsw7bj4r7k8i5sf9vk38lwmaav2v3wl327ggvr0w5jwhn1kqd6") (f (quote (("default" "rayon") ("bench"))))))

