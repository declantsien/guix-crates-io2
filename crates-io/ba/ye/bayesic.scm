(define-module (crates-io ba ye bayesic) #:use-module (crates-io))

(define-public crate-bayesic-0.1.0 (c (n "bayesic") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 2)))) (h "0li3sypcihx1gnzchjcg9d4bz14n2i2cc4qa5s88f2fs477va8c5")))

(define-public crate-bayesic-0.1.1 (c (n "bayesic") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 2)))) (h "15v95v4zbls19rws5f7la839pghig2l5296zfhdbr2a760l93g0b")))

