(define-module (crates-io ba ye bayestest) #:use-module (crates-io))

(define-public crate-bayestest-0.1.0 (c (n "bayestest") (v "0.1.0") (d (list (d (n "statrs") (r "^0.15") (d #t) (k 0)))) (h "0c24qjq0c5s95367d6rrq65la1z387x08lchirwb0fqla99kvggp")))

(define-public crate-bayestest-0.1.1 (c (n "bayestest") (v "0.1.1") (h "0ijl6pwqd6bgqzi0hnmsc1fkmh4h3qpivr9w86zq6b03nv0l5mg6")))

