(define-module (crates-io ba ye bayes) #:use-module (crates-io))

(define-public crate-bayes-0.0.1 (c (n "bayes") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.21.0") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "petgraph") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simba") (r "^0.1.2") (d #t) (k 0)))) (h "08lj7i8wn58zxbr6rlry7whjcffscps5w2prfm5jcfjsmdk7ikpd") (f (quote (("mkl")))) (l "gsl")))

