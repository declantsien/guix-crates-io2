(define-module (crates-io ba ye bayespam) #:use-module (crates-io))

(define-public crate-bayespam-0.1.0 (c (n "bayespam") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s7qa4lmw9d88fggy68w95pkgx2ddilblh406pxf74k7ipjqln55")))

(define-public crate-bayespam-0.1.1 (c (n "bayespam") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01z184zp3hcy7a5lqlw1y0m84qhd1vh12wfs3g8wansnhjgd7d0c")))

(define-public crate-bayespam-0.1.2 (c (n "bayespam") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "029gizq3v3jy441vck9q9liy9s7ln1fnwwba922b52vji1bvq12b")))

(define-public crate-bayespam-0.1.3 (c (n "bayespam") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10mxq1qjrzxzrccj7wxa3ndhxqjy9krnn70fj63i7zpqjqnbwhbl")))

(define-public crate-bayespam-0.1.4 (c (n "bayespam") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13jmmfp5ppm3vdincjskrxq83m2cyh6b5dbyxdrvfa288znfa2y3")))

(define-public crate-bayespam-0.2.0 (c (n "bayespam") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "089ng3jnffx5jikj6sw8vq8741v59v1m4yk5xnw60bywvj05lgwy")))

(define-public crate-bayespam-0.2.1 (c (n "bayespam") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bmgxj2203ig496s59yn53nlr0wbxbdqh3z9bp4mana6gi8pqjb3")))

(define-public crate-bayespam-0.3.0 (c (n "bayespam") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yk57a49sa2vp8p1p7azpjc52hwf67lhxm17f5zm2a7hcjfk5nvd")))

(define-public crate-bayespam-1.0.0 (c (n "bayespam") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q5d3zdq563lzjgbhs062k09j3ip6hlfwg57ajygwzqr16f5ih1s")))

(define-public crate-bayespam-1.0.1 (c (n "bayespam") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14ny4df2rfqdg8xaf8l2p1fvz4md0jxpp10m9dbqw59zy4hd0fil")))

(define-public crate-bayespam-1.0.2 (c (n "bayespam") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z36k4h6v5rlgayhs4iacr2ps8j79ykpjg4wvvcbdsj3jl7imqvf")))

(define-public crate-bayespam-1.0.3 (c (n "bayespam") (v "1.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vymw0c46jcg95fqbk93a5kfl52ap1d1wlg8bnh32gz1ygapagrv")))

(define-public crate-bayespam-1.1.0 (c (n "bayespam") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0afhrgmwhxla3icn2icmrcqrd5hanjrc4a8rhhryzrhjcfj0vhrz")))

