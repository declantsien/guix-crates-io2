(define-module (crates-io ba bu baburu-macros) #:use-module (crates-io))

(define-public crate-baburu-macros-0.1.0 (c (n "baburu-macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12psjm8h4qglk4568khi3hsw3v7hg4zbwlb8wqj8qrw8hs97f60g")))

