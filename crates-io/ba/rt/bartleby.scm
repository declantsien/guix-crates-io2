(define-module (crates-io ba rt bartleby) #:use-module (crates-io))

(define-public crate-bartleby-0.2.2 (c (n "bartleby") (v "0.2.2") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0qgciyns84mfn8z1mlpigqignpplwh6nl14s7rfk6zj440sllykv") (y #t)))

(define-public crate-bartleby-0.2.3 (c (n "bartleby") (v "0.2.3") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "10j7jhxcs1hdxf660mwsr2hfgljnmni1sd83b78ggkk40pp5hcgb")))

