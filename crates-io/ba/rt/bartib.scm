(define-module (crates-io ba rt bartib) #:use-module (crates-io))

(define-public crate-bartib-1.0.0 (c (n "bartib") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.34.0") (d #t) (k 0)) (d (n "term_size") (r "^1.0.0-beta1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q3gf98kxjc0vx98kxi04pzqlns3qfz3y3kagjy10705h8n4b219")))

(define-public crate-bartib-1.0.1 (c (n "bartib") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.34.0") (d #t) (k 0)) (d (n "term_size") (r "^1.0.0-beta1") (d #t) (k 0)) (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0faywsx680ifj4jdzf1lw12ld49a5rkxhy404ccp50df6gfbhk0x")))

(define-public crate-bartib-1.1.0 (c (n "bartib") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "wildmatch") (r "^2.3.0") (d #t) (k 0)))) (h "1snsvqkqg6pd0zsbb6kqa0vjmbi0pzw4r9f2d8phkrnm3b6l5npj")))

