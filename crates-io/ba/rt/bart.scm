(define-module (crates-io ba rt bart) #:use-module (crates-io))

(define-public crate-bart-0.0.1 (c (n "bart") (v "0.0.1") (h "1w8w78snw99cp11mz6vyq08r625kq70jbvkh066ryh2r3fyzcv2z") (f (quote (("specialization") ("default"))))))

(define-public crate-bart-0.1.0 (c (n "bart") (v "0.1.0") (d (list (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "1jpyhv6fqjhwyvx8wpkhysgzal7q6qj66fvns3q13wn911nnd2pb") (f (quote (("specialization") ("default"))))))

(define-public crate-bart-0.1.1 (c (n "bart") (v "0.1.1") (d (list (d (n "bart_derive") (r "^0.1.1") (d #t) (k 2)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "0y4c3sp8lyrs0krph37qqls6kbyfdjcw5z8mh6crglxd08sm7n3p") (f (quote (("specialization") ("default"))))))

(define-public crate-bart-0.1.2 (c (n "bart") (v "0.1.2") (d (list (d (n "bart_derive") (r "^0.1.2") (d #t) (k 2)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "1aqsm9icg0n4qn5mlpmg9iz2y6xxdxbbq26yhfnvwf148v118jsm") (f (quote (("specialization") ("default"))))))

(define-public crate-bart-0.1.3 (c (n "bart") (v "0.1.3") (d (list (d (n "bart_derive") (r "^0.1.3") (d #t) (k 2)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "0lh9w3wn0gnzsbhsizyznrx6gasy9d22jm0rxx4vxir2bva1ffcj") (f (quote (("specialization") ("default"))))))

(define-public crate-bart-0.1.4 (c (n "bart") (v "0.1.4") (d (list (d (n "bart_derive") (r "^0.1.4") (d #t) (k 2)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "1rxsg0f188k2gb5nm9999i11xh10lgdfrhp1ibiapqj6bhmdbygh") (f (quote (("specialization") ("default"))))))

(define-public crate-bart-0.1.6 (c (n "bart") (v "0.1.6") (d (list (d (n "bart_derive") (r "^0.1.6") (d #t) (k 2)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "0q3c6kwp8alzbkrn5syg2933msyh6ivd9lllywr7mxvzlavsqy1n") (f (quote (("specialization") ("default"))))))

