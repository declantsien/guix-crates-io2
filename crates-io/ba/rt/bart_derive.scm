(define-module (crates-io ba rt bart_derive) #:use-module (crates-io))

(define-public crate-bart_derive-0.0.1 (c (n "bart_derive") (v "0.0.1") (d (list (d (n "bart") (r "^0.0.1") (d #t) (k 2)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "0x609bqank9rbx6yinbdsnww0pzc5gn9c5jz42m67pll034bpzpx")))

(define-public crate-bart_derive-0.1.0 (c (n "bart_derive") (v "0.1.0") (d (list (d (n "bart") (r "^0.1.0") (d #t) (k 2)) (d (n "nom") (r "^2.0.1") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "05jhpmkc9ihd21a9gsrs5qsl86n5h07a52q3x1v7800cw02hl247")))

(define-public crate-bart_derive-0.1.1 (c (n "bart_derive") (v "0.1.1") (d (list (d (n "num") (r "^0.1.36") (k 0)) (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "0q959zqsx0a22ikm2g0ny5rmk2ahmbsvw9ds4agd3b5qspxi29yb")))

(define-public crate-bart_derive-0.1.2 (c (n "bart_derive") (v "0.1.2") (d (list (d (n "num") (r "^0.1.36") (k 0)) (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "10w85p7k5chp2xrwrkm4sadr4n2kfnhv6d8775m64ghqa7jbfj2d")))

(define-public crate-bart_derive-0.1.3 (c (n "bart_derive") (v "0.1.3") (d (list (d (n "itertools") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (k 0)) (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "0r1spnas934764jwhijawihijrv54gi7lg9rz6pdxxzakk933pja")))

(define-public crate-bart_derive-0.1.4 (c (n "bart_derive") (v "0.1.4") (d (list (d (n "itertools") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (k 0)) (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "1wba3jhiqm1dl72y1sk0k6gwqz6y2zidlb1yk4afp6n2b81wkz9r")))

(define-public crate-bart_derive-0.1.5 (c (n "bart_derive") (v "0.1.5") (d (list (d (n "itertools") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (k 0)) (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "00w3lrh9zxqa2mpwc06l6bkrfpiis0l95znl4vjdc1pmndkyg4x8")))

(define-public crate-bart_derive-0.1.6 (c (n "bart_derive") (v "0.1.6") (d (list (d (n "itertools") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (k 0)) (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.11.1") (d #t) (k 0)))) (h "1nqgb5zsbw2pd1f2mz07bfh6b2d6w5i4gjw1wk54cls55c5hs6yb")))

