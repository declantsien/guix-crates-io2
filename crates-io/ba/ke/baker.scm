(define-module (crates-io ba ke baker) #:use-module (crates-io))

(define-public crate-baker-0.1.0 (c (n "baker") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "somok") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06n9kvbw7afizl9dij0qxgqq2a8sz78djdz5znsgy1v1i263f6rs")))

(define-public crate-baker-0.1.1 (c (n "baker") (v "0.1.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "somok") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0acmgm96hg4fw10l8q5pph2qsx4z0qva8zmp3avi7pk2lqcsxygh")))

(define-public crate-baker-0.1.2 (c (n "baker") (v "0.1.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "somok") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02fp91khdh0akr71121k19lkrr8f2qgcxahqln19f3sqnizchh7m")))

(define-public crate-baker-0.2.0 (c (n "baker") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "somok") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14ycjwc1nw7nflci68d9ayd8223wxdwg0qdkgxgcapk3y7galx6j")))

