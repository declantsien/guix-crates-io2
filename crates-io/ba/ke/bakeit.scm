(define-module (crates-io ba ke bakeit) #:use-module (crates-io))

(define-public crate-bakeit-0.1.0 (c (n "bakeit") (v "0.1.0") (d (list (d (n "curl") (r "^0.3.0") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rust-ini") (r "^0.9.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)) (d (n "webbrowser") (r "^0.1.3") (d #t) (k 0)))) (h "1gzjr48dd9pdlk107j8x3binc4gv6rvfc3ffk1q6bycblrn6gl12")))

(define-public crate-bakeit-0.1.1 (c (n "bakeit") (v "0.1.1") (d (list (d (n "curl") (r "^0.3.0") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rust-ini") (r "^0.9.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)) (d (n "webbrowser") (r "^0.1.3") (d #t) (k 0)))) (h "0rix84lzldfkihisj653ph36qwfxv5bradvgdmkcz94a1s2dkr3l")))

