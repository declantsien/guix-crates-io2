(define-module (crates-io ba ke bakery-derive) #:use-module (crates-io))

(define-public crate-bakery-derive-0.1.0 (c (n "bakery-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "1h9p16clpiirbhsg5mmhxqqszca7cxbp2chqbl7c561yzli4j72k")))

