(define-module (crates-io ba ba babalcore) #:use-module (crates-io))

(define-public crate-babalcore-0.1.0 (c (n "babalcore") (v "0.1.0") (h "1snq6yra7lsw1mgpq2g9h13yj0hbv4wyd0q85bf4104sbbhcj10l")))

(define-public crate-babalcore-0.2.0 (c (n "babalcore") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0srfifsb61k8a8ihqbv6923l9wvph3k6s1w8cpnl9w8az1rr0zd7")))

(define-public crate-babalcore-0.2.1 (c (n "babalcore") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "163acz9h0dl839h7l0idd6yca3p7jrb6q9q43mxfjky1isd01v3y")))

(define-public crate-babalcore-0.2.2 (c (n "babalcore") (v "0.2.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0sz1dlr3hl7p7gxbxbi7ab13z5m97l5biy58dqlp1xj2iw8qxkv0")))

(define-public crate-babalcore-0.2.3 (c (n "babalcore") (v "0.2.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "17zbcnh4m5l9h8dpggzy39sc6szb887h7wk0pfs50p4nis47n539")))

(define-public crate-babalcore-0.2.4 (c (n "babalcore") (v "0.2.4") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0wpbp22w31n4f3v0csvzvnl0vqb80v6x28szv5y5f4vr64qlmgh0")))

(define-public crate-babalcore-0.3.0 (c (n "babalcore") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0q5pf13m4sby8r5rl078g8pbibnqn09jgqw014r1wxpmh5pyq051")))

(define-public crate-babalcore-0.4.3 (c (n "babalcore") (v "0.4.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1s5xc53vz8sm32p3im06yha47ad65qcp04xa4cb6dhg13kg64vjz")))

(define-public crate-babalcore-0.5.0 (c (n "babalcore") (v "0.5.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "02kfj52p9hainq2yhk328ahwzr0sq21gqzp23y9s8vk6pr20x8k0")))

(define-public crate-babalcore-0.5.1 (c (n "babalcore") (v "0.5.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1whidb8csyfdpd3f0g47h2jbzdfaw1dj8wi9xfln4261a8xa9j9m")))

