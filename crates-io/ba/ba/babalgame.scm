(define-module (crates-io ba ba babalgame) #:use-module (crates-io))

(define-public crate-babalgame-0.2.0 (c (n "babalgame") (v "0.2.0") (d (list (d (n "babalcore") (r "^0.2.0") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.1") (d #t) (k 0)))) (h "1769kq1ahcmg5m341w7gbj8d8azhmscgwfihb1hanh1qcgncxd2i")))

(define-public crate-babalgame-0.2.1 (c (n "babalgame") (v "0.2.1") (d (list (d (n "babalcore") (r "^0.2.1") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.1") (d #t) (k 0)))) (h "10h87r280n714pcjkpmb7v0i3rb2z67dlm24k0z2j9gscy9wrjyb")))

(define-public crate-babalgame-0.2.2 (c (n "babalgame") (v "0.2.2") (d (list (d (n "babalcore") (r "^0.2.2") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.1") (d #t) (k 0)))) (h "10ma694ixwfmpsniaqcikmslq511nh44kxq03xg40c3dr0jl6yvc")))

(define-public crate-babalgame-0.2.3 (c (n "babalgame") (v "0.2.3") (d (list (d (n "babalcore") (r "^0.2.3") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.1") (d #t) (k 0)))) (h "1pfri33azn2np931jk5hjs3vzm13ylkakpw2bms0lxwiwy1lxr49")))

(define-public crate-babalgame-0.2.4 (c (n "babalgame") (v "0.2.4") (d (list (d (n "babalcore") (r "^0.2.4") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.1") (d #t) (k 0)))) (h "1aif6dk4mnnz581v6mhdnk38483mrz408bl4v691rr983204b9ga")))

(define-public crate-babalgame-0.3.0 (c (n "babalgame") (v "0.3.0") (d (list (d (n "babalcore") (r "^0.3.0") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.1") (d #t) (k 0)))) (h "1j3hhcsw3b5d8y6zb6xh300vhnml5k0iiqb7ha5173f8ixgs3d62")))

(define-public crate-babalgame-0.4.3 (c (n "babalgame") (v "0.4.3") (d (list (d (n "babalcore") (r "^0.4.3") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.1") (d #t) (k 0)))) (h "0skhapganq6i2l39fxvrhad34vlqja47068gn4fddywcx6vwsh7l")))

(define-public crate-babalgame-0.5.0 (c (n "babalgame") (v "0.5.0") (d (list (d (n "babalcore") (r "^0.5.0") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.3") (d #t) (k 0)))) (h "1hssr8c43czm3l0qilnlkk851wjaf2q84iz1h5vnrd9fbnb8kllb")))

(define-public crate-babalgame-0.5.1 (c (n "babalgame") (v "0.5.1") (d (list (d (n "babalcore") (r "^0.5.1") (d #t) (k 0)) (d (n "gdnative") (r "^0.9.3") (d #t) (k 0)))) (h "16rm00xixsacrwqgjj6h3p12ipdi7v1dh3h055zpyfmb30cxshpz")))

