(define-module (crates-io ba ff baffle) #:use-module (crates-io))

(define-public crate-baffle-0.1.0 (c (n "baffle") (v "0.1.0") (d (list (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "web3") (r "^0.10.0") (d #t) (k 0)))) (h "02lzx098l13n82n8c6klwwk8sxpz592dpyk4w1bsybm3zxzafbm3")))

(define-public crate-baffle-0.1.1 (c (n "baffle") (v "0.1.1") (d (list (d (n "web3") (r "^0.10.0") (d #t) (k 0)))) (h "1rbw16qlprm9iqylvfz2py9r5r09ab4k217d6dnflnmrnvs70whg")))

