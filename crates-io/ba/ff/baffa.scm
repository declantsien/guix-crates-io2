(define-module (crates-io ba ff baffa) #:use-module (crates-io))

(define-public crate-baffa-0.1.0 (c (n "baffa") (v "0.1.0") (h "08g0alljxh7viaqnp8jgc3q3j3ismgl33ybbck70m98cspfl7p8g") (f (quote (("std") ("alloc"))))))

(define-public crate-baffa-0.1.1 (c (n "baffa") (v "0.1.1") (h "1102jdslnj9j6rrdf7xg60afr40g0vq4k35k599kjar18kk643hf") (f (quote (("std") ("alloc"))))))

(define-public crate-baffa-0.1.2 (c (n "baffa") (v "0.1.2") (h "1fjll70zpkx1apafaq5w1ax2d714apbkxfwsml87w3my4crvqcq8") (f (quote (("std") ("alloc"))))))

