(define-module (crates-io ba rf barf) #:use-module (crates-io))

(define-public crate-barf-1.0.0 (c (n "barf") (v "1.0.0") (d (list (d (n "nano-leb128") (r "^0.1") (o #t) (k 0)) (d (n "vint64") (r "^1.0") (o #t) (k 0)))) (h "1ipgxla1f4vdin6132wc6xcndzag93z4b9awfvnqirnsk8kxbw9w") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (s 2) (e (quote (("vint64" "dep:vint64") ("leb128" "dep:nano-leb128")))) (r "1.60.0")))

