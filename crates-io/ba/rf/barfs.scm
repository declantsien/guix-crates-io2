(define-module (crates-io ba rf barfs) #:use-module (crates-io))

(define-public crate-barfs-0.1.0 (c (n "barfs") (v "0.1.0") (d (list (d (n "cranelift") (r "^0.78") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.78") (d #t) (k 0)) (d (n "cranelift-jit") (r "^0.78") (d #t) (k 0)) (d (n "cranelift-module") (r "^0.78") (d #t) (k 0)) (d (n "cranelift-native") (r "^0.78") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0asfa1vdi7s8n7mwc514vv2dwfphnfqh4d1diig8i7z355ql51dv")))

(define-public crate-barfs-0.1.1 (c (n "barfs") (v "0.1.1") (d (list (d (n "cranelift") (r "^0.78") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.78") (d #t) (k 0)) (d (n "cranelift-jit") (r "^0.78") (d #t) (k 0)) (d (n "cranelift-module") (r "^0.78") (d #t) (k 0)) (d (n "cranelift-native") (r "^0.78") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0kbqw4skvwkl0v66fqbfkmnwdk1d9wjn5b75xwmj8adhd0fljpbk")))

(define-public crate-barfs-0.1.2 (c (n "barfs") (v "0.1.2") (d (list (d (n "cranelift") (r "^0.89") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.89") (d #t) (k 0)) (d (n "cranelift-jit") (r "^0.89") (d #t) (k 0)) (d (n "cranelift-module") (r "^0.89") (d #t) (k 0)) (d (n "cranelift-native") (r "^0.89") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "07mkjjamzjbb9n1fxc3hwmzx04c07hjg9zrvn1b0daac36w6m212")))

