(define-module (crates-io ba rf barfly) #:use-module (crates-io))

(define-public crate-barfly-0.1.0 (c (n "barfly") (v "0.1.0") (h "00za8pn1065r6wdvbg3lin627j88z5yq7kf6w20fhi87s2skprln")))

(define-public crate-barfly-0.1.1 (c (n "barfly") (v "0.1.1") (h "1hflkk46x40hf5hwk63h8nrxvriij7z3zyakwpj7fgc7khikmvg8")))

(define-public crate-barfly-0.1.2 (c (n "barfly") (v "0.1.2") (d (list (d (n "cocoa") (r "*") (d #t) (k 0)) (d (n "core-graphics") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "objc") (r "*") (d #t) (k 0)) (d (n "objc-foundation") (r "*") (d #t) (k 0)) (d (n "objc_id") (r "*") (d #t) (k 0)))) (h "1ylqf1yakalljy94gznp84d05n8gl5f6f91rmvfx0n6lqmjglv4h")))

(define-public crate-barfly-0.1.3 (c (n "barfly") (v "0.1.3") (d (list (d (n "cocoa") (r "*") (d #t) (k 0)) (d (n "core-graphics") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "objc") (r "*") (d #t) (k 0)) (d (n "objc-foundation") (r "*") (d #t) (k 0)) (d (n "objc_id") (r "*") (d #t) (k 0)))) (h "178jfa7abs2jfiv7yb0rbvc7x3swhy09r7y25xvy4aqzakm369is")))

