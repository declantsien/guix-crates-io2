(define-module (crates-io ba rn barnacle) #:use-module (crates-io))

(define-public crate-barnacle-0.1.0 (c (n "barnacle") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "minijinja") (r "^0.8") (f (quote ("source"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1lkkqxipzi01ma5fgk3vb4sgwhfrjdh7dwdg4hw8zr5zla1slgmq")))

(define-public crate-barnacle-0.1.1 (c (n "barnacle") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "minijinja") (r "^0.13") (f (quote ("source" "json" "urlencode"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1b7rizgdl4dxb80dsa3waryv0iyqj5pv493qkgflhh12fkmi5d31")))

(define-public crate-barnacle-0.2.0 (c (n "barnacle") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "minijinja") (r "^0.23") (f (quote ("source" "json" "urlencode"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "136v8vk4lfi9kwsm51rhslfjw0dxjadycynyzb5vpy2j4x2hbnjw")))

(define-public crate-barnacle-0.3.0 (c (n "barnacle") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "minijinja") (r "^0.23") (f (quote ("source" "json" "urlencode"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "009sm7rczhbp9lfznv4p7v3dxyd1i1369s7zmdv08dn8fwx6q200")))

