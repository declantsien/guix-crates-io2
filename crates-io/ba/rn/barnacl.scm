(define-module (crates-io ba rn barnacl) #:use-module (crates-io))

(define-public crate-barnacl-0.1.0 (c (n "barnacl") (v "0.1.0") (d (list (d (n "barnacl_sys") (r "*") (d #t) (k 0)) (d (n "cbor") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "03a9xsjsbygsjg4ffgg2dq74avg4xc6n0r7wg4lypwyarqsb1x8n") (f (quote (("benchmarks"))))))

(define-public crate-barnacl-0.1.1 (c (n "barnacl") (v "0.1.1") (d (list (d (n "barnacl_sys") (r "*") (d #t) (k 0)) (d (n "cbor") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0zdvsn929fafphiny1f52ji3m5d7f8gwj10655a8adw38ny0crvr") (f (quote (("benchmarks"))))))

