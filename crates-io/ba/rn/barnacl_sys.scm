(define-module (crates-io ba rn barnacl_sys) #:use-module (crates-io))

(define-public crate-barnacl_sys-0.1.0 (c (n "barnacl_sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1z0f2bpyp4a9j9ngsm1cxyji2i69lw73w2q8gs5h4yfzd10r6qfq")))

(define-public crate-barnacl_sys-0.1.1 (c (n "barnacl_sys") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "11ryjvmcx7aldaxgwaphlcypfsm74r8a2plszq0x65qq296nbgni")))

