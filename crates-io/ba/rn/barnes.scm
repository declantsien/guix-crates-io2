(define-module (crates-io ba rn barnes) #:use-module (crates-io))

(define-public crate-barnes-0.1.0 (c (n "barnes") (v "0.1.0") (d (list (d (n "num_cpus") (r "^0.2.6") (d #t) (k 0)))) (h "06kj6r02159kj5zhvxla1shsylib2iynjrlfp5qafi6bn9072v05")))

(define-public crate-barnes-0.1.1 (c (n "barnes") (v "0.1.1") (d (list (d (n "num_cpus") (r "^0.2.6") (d #t) (k 0)))) (h "0s6922yv309w7nq937ixjdx41jh29zg3vdzqbpr1glr5ln8rlqfg")))

(define-public crate-barnes-0.1.2 (c (n "barnes") (v "0.1.2") (d (list (d (n "num_cpus") (r "^0.2.6") (d #t) (k 0)))) (h "0q3b9mqb30pgx04i3k42s3rkziw4dg44by0vwhzba08pyla2qf0g")))

