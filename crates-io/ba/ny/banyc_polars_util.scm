(define-module (crates-io ba ny banyc_polars_util) #:use-module (crates-io))

(define-public crate-banyc_polars_util-0.1.0 (c (n "banyc_polars_util") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "hdv") (r "^0.1") (f (quote ("polars"))) (d #t) (k 0)) (d (n "polars") (r "^0.40") (f (quote ("json" "lazy"))) (d #t) (k 0)))) (h "1x4miv33kc7ml3kzrvnvvsaxpvlfrfw9wnmyx694cv1zk3lm11r3")))

