(define-module (crates-io ba il bail-out) #:use-module (crates-io))

(define-public crate-bail-out-0.1.0 (c (n "bail-out") (v "0.1.0") (h "1zqqap1ll3gfkgic99xjm8bwy84lkzr1sf2xzx15x9dcfi1b4yq8")))

(define-public crate-bail-out-0.1.1 (c (n "bail-out") (v "0.1.1") (h "1j7bxjmavcvrj6nd1q6rncy8s51zxzh77bxxzncvchhgb2d4g722")))

(define-public crate-bail-out-0.1.2 (c (n "bail-out") (v "0.1.2") (h "0ihj98ilfpdfm89ja1iyqcnshw25y6bnzsq167wwp8gzl94d3brm")))

(define-public crate-bail-out-0.2.0 (c (n "bail-out") (v "0.2.0") (h "0p0zsp1ispa26b0ndms8z7pj6ayr0ijnx8g6lgx13y2q43zdcrma")))

