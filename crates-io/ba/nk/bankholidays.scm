(define-module (crates-io ba nk bankholidays) #:use-module (crates-io))

(define-public crate-bankholidays-0.1.0 (c (n "bankholidays") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "computus") (r "^0.1.1") (d #t) (k 0)))) (h "1ja57i1fp7lq4cnxnlsf0ypb9xrzxinaad5y9x9xjsdr8lwzm2m2")))

(define-public crate-bankholidays-1.0.0 (c (n "bankholidays") (v "1.0.0") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)))) (h "0pns9ldi84c7giaspgafa82scnr8r0764by85dq5ahgb4csdzr7c")))

(define-public crate-bankholidays-1.1.0 (c (n "bankholidays") (v "1.1.0") (d (list (d (n "chrono") (r ">= 0.2.0") (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)))) (h "0rpbqmxrifdprgaqb8za24rs5gganfg1rkhf0j7n2fwvbhisv3h8")))

(define-public crate-bankholidays-1.1.1 (c (n "bankholidays") (v "1.1.1") (d (list (d (n "chrono") (r ">= 0.2.0") (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)))) (h "00da3zxgpb6d0kq6f0h2xg9m0hqv9678wpmmsv9r7hnc8qri7jb9")))

(define-public crate-bankholidays-1.1.2 (c (n "bankholidays") (v "1.1.2") (d (list (d (n "chrono") (r ">=0.2.0") (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)))) (h "00qmnzmcppgzddr942avy3bz2a6rwl184rwq868nxa27yhk2byjz")))

(define-public crate-bankholidays-1.1.3 (c (n "bankholidays") (v "1.1.3") (d (list (d (n "chrono") (r ">=0.2.0") (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)))) (h "0hakywx6x7v1v2gcmb5023h4xd9s0j54q3j48414yp1c9z4z23yw")))

(define-public crate-bankholidays-1.1.4 (c (n "bankholidays") (v "1.1.4") (d (list (d (n "chrono") (r ">=0.2.0") (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)))) (h "1kpf72i9svijzzd30g8jfczhnnjk91c2c0lvmxgibwyfmbja255b")))

