(define-module (crates-io ba nk bankman) #:use-module (crates-io))

(define-public crate-bankman-0.1.0 (c (n "bankman") (v "0.1.0") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "crate-token") (r "^0.3.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^1.3.0") (d #t) (k 0)))) (h "0cshr7a6ca5kr23ixfh4qkgi2gw87ym4v5g9693xgrnipr29zhzg") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bankman-0.1.4 (c (n "bankman") (v "0.1.4") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "crate-token") (r "^0.4") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^1.5") (d #t) (k 0)))) (h "1s0xif6dsmi3xh8p9ggkblfk1gk06zg4msnns85f0bzgc99n1sb5") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bankman-0.2.0 (c (n "bankman") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.22") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22") (d #t) (k 0)) (d (n "crate-token") (r "^0.5") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^2") (d #t) (k 0)))) (h "0q5dsjidca9b3b0diwwqj08cs78igxd8fjrqgzm7fa5k3fq307yl") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bankman-0.2.1 (c (n "bankman") (v "0.2.1") (d (list (d (n "anchor-lang") (r "^0.22") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22") (d #t) (k 0)) (d (n "crate-token") (r "^0.5") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^2") (d #t) (k 0)))) (h "0kagq05gs0f3qnjnnn3z1bvyl6fy4j3q6v6i1dxkzn6bnambcfns") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bankman-0.3.0 (c (n "bankman") (v "0.3.0") (d (list (d (n "anchor-lang") (r "^0.24") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24") (d #t) (k 0)) (d (n "crate-token") (r "^0.6") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^2") (d #t) (k 0)))) (h "0qr7pz8cs8qv0q08lpljq9hfsp1rvmnnv9r5b6qwkpzhj5w1s0f0") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bankman-0.3.1 (c (n "bankman") (v "0.3.1") (d (list (d (n "anchor-lang") (r "^0.24") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24") (d #t) (k 0)) (d (n "crate-token") (r "^0.6") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "vipers") (r "^2") (d #t) (k 0)))) (h "1kgcqcywyrv781xb1mf9j2mn37z8sdnf2pi9ljy9qxdb008gfyj4") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

