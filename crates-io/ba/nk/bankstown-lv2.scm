(define-module (crates-io ba nk bankstown-lv2) #:use-module (crates-io))

(define-public crate-bankstown-lv2-1.0.0 (c (n "bankstown-lv2") (v "1.0.0") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "lv2") (r "^0.6.0") (d #t) (k 0)))) (h "0az1hcnvf9x61fw6687s4di7l4cfnh7z6rhmw55299l7b7hfq1lk")))

(define-public crate-bankstown-lv2-1.0.1 (c (n "bankstown-lv2") (v "1.0.1") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "lv2") (r "^0.6.0") (d #t) (k 0)))) (h "14dr7rpy3y9pzj0wz039sd3984l1s6b7s6z1zrb5pzvww3sxn90s")))

(define-public crate-bankstown-lv2-1.0.2 (c (n "bankstown-lv2") (v "1.0.2") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "lv2") (r "^0.6.0") (d #t) (k 0)))) (h "1ds21iiy6lxp6v3gidl498jszgx42pxyzvdxadqq9c6q3cwcr3nh")))

(define-public crate-bankstown-lv2-1.0.3 (c (n "bankstown-lv2") (v "1.0.3") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "lv2") (r "^0.6.0") (d #t) (k 0)))) (h "1v3vrj09zwcb4dm2skdaf6bwkigf7n1rp4m8598b9xmsjra34gwq")))

(define-public crate-bankstown-lv2-1.1.0 (c (n "bankstown-lv2") (v "1.1.0") (d (list (d (n "biquad") (r "^0.4.2") (d #t) (k 0)) (d (n "lv2") (r "^0.6.0") (d #t) (k 0)))) (h "1bcrn0b4b9v1mksaldhrdb6ncqlwldfwqxjlfp4gcpvl661qdmcb")))

