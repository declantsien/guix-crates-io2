(define-module (crates-io ba nk bank-csv) #:use-module (crates-io))

(define-public crate-bank-csv-0.1.0 (c (n "bank-csv") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)) (d (n "polars") (r "^0.37.0") (f (quote ("csv" "dtype-date" "lazy" "rows"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.8.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "1rkqp7i29cdm3ljwc10m3mjb80cddim8a8a3gxs8x0pkpla7d0xp")))

