(define-module (crates-io ba nk bank_vault) #:use-module (crates-io))

(define-public crate-bank_vault-0.0.1 (c (n "bank_vault") (v "0.0.1") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1l4cn6s3lw7kz0vgglc4i5vz4n273rclg62200hsdgka13k809z7")))

(define-public crate-bank_vault-0.0.2 (c (n "bank_vault") (v "0.0.2") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1l7gxfydcay5lkvh1pnkg9pm6mjpc6nkraqi4s11aq5aig0gs2vz")))

