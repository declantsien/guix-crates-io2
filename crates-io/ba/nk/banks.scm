(define-module (crates-io ba nk banks) #:use-module (crates-io))

(define-public crate-banks-0.0.1 (c (n "banks") (v "0.0.1") (d (list (d (n "bigdecimal") (r "^0.0.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "display_derive") (r "^0") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1v6a4y58jaxxbl8c9bf7xr24brpy27i513az8lrnhbvhmgz38kfl")))

(define-public crate-banks-0.0.2 (c (n "banks") (v "0.0.2") (d (list (d (n "bigdecimal") (r "^0.0.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "strum") (r "^0.15.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15.0") (d #t) (k 0)))) (h "1prj4w356l6zzqpjvabj3kk26gqv8rgk4ffk1kz5bz8rivkljsdz")))

(define-public crate-banks-0.0.3 (c (n "banks") (v "0.0.3") (d (list (d (n "bigdecimal") (r "^0.0.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "strum") (r "^0.15.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15.0") (d #t) (k 0)))) (h "14ivzg8ymic888j0cdk9q8chqvcd9wjm4mcf0m92f6a1gb7x73gb")))

(define-public crate-banks-0.0.4 (c (n "banks") (v "0.0.4") (d (list (d (n "bigdecimal") (r "^0.0.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "strum") (r "^0.15.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15.0") (d #t) (k 0)))) (h "0ng8nayzvibrsffkd9d6gh4a2yizryzp4g7xmjgklrj1xknzjr5n")))

