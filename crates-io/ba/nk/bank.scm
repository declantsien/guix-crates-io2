(define-module (crates-io ba nk bank) #:use-module (crates-io))

(define-public crate-bank-0.1.0 (c (n "bank") (v "0.1.0") (d (list (d (n "bigdecimal") (r ">= 0.0.1") (d #t) (k 0)) (d (n "chrono") (r ">= 0.3.0") (d #t) (k 0)) (d (n "csv") (r ">= 0.15.0") (d #t) (k 0)))) (h "1lv2gvdcf34k7f03n2imjgmv3fw1sj72ihrzjqbaz9k2yvhlarj5")))

(define-public crate-bank-0.2.0 (c (n "bank") (v "0.2.0") (d (list (d (n "bigdecimal") (r ">= 0.0.1") (d #t) (k 0)) (d (n "chrono") (r ">= 0.3.0") (d #t) (k 0)) (d (n "csv") (r ">= 0.15.0") (d #t) (k 0)))) (h "0qr54n68lzdyv4wq34l4695x1p1xcy5n65a4c70fnmc2azd87ws9")))

(define-public crate-bank-0.2.1 (c (n "bank") (v "0.2.1") (d (list (d (n "bigdecimal") (r ">= 0.0.1") (d #t) (k 0)) (d (n "chrono") (r ">= 0.3.0") (d #t) (k 0)) (d (n "csv") (r ">= 0.15.0") (d #t) (k 0)) (d (n "encoding") (r ">= 0.2") (d #t) (k 0)) (d (n "log") (r ">= 0.3.0") (d #t) (k 0)))) (h "0fnlk6a7xnbxdqbk3pq59irn7m1yvz6nz0jzlmssdd88i0bbmyzz")))

(define-public crate-bank-0.3.0 (c (n "bank") (v "0.3.0") (d (list (d (n "bigdecimal") (r ">= 0.0.1") (d #t) (k 0)) (d (n "chrono") (r ">= 0.3.0") (d #t) (k 0)) (d (n "csv") (r ">= 0.15.0") (d #t) (k 0)) (d (n "encoding") (r ">= 0.2") (d #t) (k 0)) (d (n "log") (r ">= 0.3.0") (d #t) (k 0)) (d (n "num") (r ">= 0.1.34") (d #t) (k 0)))) (h "0py7bz4cavnc7i068zywbcdam9xm09x3bl09s6yn4gwp2sx5bdwc")))

(define-public crate-bank-0.3.1 (c (n "bank") (v "0.3.1") (d (list (d (n "bigdecimal") (r ">= 0.0.1") (d #t) (k 0)) (d (n "chrono") (r ">= 0.3.0") (d #t) (k 0)) (d (n "csv") (r ">= 0.15.0") (d #t) (k 0)) (d (n "encoding") (r ">= 0.2") (d #t) (k 0)) (d (n "log") (r ">= 0.3.0") (d #t) (k 0)) (d (n "num") (r ">= 0.1.34") (d #t) (k 0)))) (h "08fsr34mxqs81h931vphsp34a4zpikc9mshlclay1k00z1vqqk5b")))

(define-public crate-bank-0.4.0 (c (n "bank") (v "0.4.0") (d (list (d (n "bigdecimal") (r ">= 0.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "csv") (r ">= 0.15.0") (d #t) (k 0)) (d (n "encoding") (r ">= 0.2") (d #t) (k 0)) (d (n "log") (r ">= 0.3.0") (d #t) (k 0)) (d (n "num") (r ">= 0.1.34") (d #t) (k 0)))) (h "16gcahk6hv6998wgbx8rcabkfdy8x2rkxhl0c54rhi8cn0waxq8s")))

