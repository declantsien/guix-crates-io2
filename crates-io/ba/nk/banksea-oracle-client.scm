(define-module (crates-io ba nk banksea-oracle-client) #:use-module (crates-io))

(define-public crate-banksea-oracle-client-0.1.0 (c (n "banksea-oracle-client") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "=1.8.1") (d #t) (k 0)))) (h "0gb12iqw4n76a6ipxjqwx0nihcnbx5fygrd646z0fdp47vi0241p")))

(define-public crate-banksea-oracle-client-0.2.0 (c (n "banksea-oracle-client") (v "0.2.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "=1.8.1") (d #t) (k 0)))) (h "1c78lym2fd955fqaxaqbp6acdn0hpkpqb5bd822yfz1qv33w7xsl")))

(define-public crate-banksea-oracle-client-0.3.0 (c (n "banksea-oracle-client") (v "0.3.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.3") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.24") (d #t) (k 0)))) (h "14x1kszzn1c4vkplrs8aw00n81p6rxp36cqsydvcz7y1amz62my7")))

(define-public crate-banksea-oracle-client-0.4.0 (c (n "banksea-oracle-client") (v "0.4.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.3") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.24") (d #t) (k 0)))) (h "1q7phg106xqhp5s7k892brkyirmxmbd00g1c0as3mmvq3zv04ryq")))

