(define-module (crates-io ba ll balloc) #:use-module (crates-io))

(define-public crate-balloc-0.1.0 (c (n "balloc") (v "0.1.0") (h "19dq48yhm022669g8f1bbdx8c8rwgghz1qfcldy6gv5lfw6s85ry")))

(define-public crate-balloc-0.2.0 (c (n "balloc") (v "0.2.0") (h "01wjj4mdf2ibp3yqirakplxf6x14ih7d21fwi111v3k2vw8a6bfn")))

