(define-module (crates-io ba ll ball-tree) #:use-module (crates-io))

(define-public crate-ball-tree-0.1.0 (c (n "ball-tree") (v "0.1.0") (h "0xj9l41gcalzq3iacwq92jmfn0f1i7sv1xpggl8dizkvigf2bsw3")))

(define-public crate-ball-tree-0.1.1 (c (n "ball-tree") (v "0.1.1") (h "039wrcb2f439wy7w9rhxx5nan7vcwyx4nfq7s764vfljjk1dk0m3")))

(define-public crate-ball-tree-0.2.0 (c (n "ball-tree") (v "0.2.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 2)))) (h "041g3ywh6cdpj35bv6qdw6z4fj8k29brlv2nlcyk90wq2nzf0g7m")))

(define-public crate-ball-tree-0.3.0 (c (n "ball-tree") (v "0.3.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 2)))) (h "0qni69f4qkycyasxcdzx9sm7ypg8lq6g68fydbb09rhd6anyww0a")))

(define-public crate-ball-tree-0.4.0 (c (n "ball-tree") (v "0.4.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 2)))) (h "1afhi4ka503a7z4g3amdi82793v0aslc2r741p35wpj5hw39nwga")))

(define-public crate-ball-tree-0.5.0 (c (n "ball-tree") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0a43c8lw33g0ngjgkklyzcf3kv044540xdi2iq3za3qvn3ypz6fy")))

