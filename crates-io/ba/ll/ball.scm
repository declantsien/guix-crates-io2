(define-module (crates-io ba ll ball) #:use-module (crates-io))

(define-public crate-ball-0.0.0 (c (n "ball") (v "0.0.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0r60ry87mjy12zz9b9fj2lzcf7fr41w8yfpmd4khjw1jcfrdpisj") (f (quote (("f64") ("f32") ("default" "f64"))))))

(define-public crate-ball-0.1.0 (c (n "ball") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0illrgqrss1xl0jg08ahmj9qssc21inx825027l0b4z4662q5i5d") (f (quote (("f64") ("f32") ("default" "f64"))))))

(define-public crate-ball-0.1.1 (c (n "ball") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0wc63qfym06310p4l1828cg4bjq2231vpgx8jwdsy2hgw68k2l67") (f (quote (("f64") ("f32") ("default" "f32"))))))

