(define-module (crates-io ba ll ball_dont_lie) #:use-module (crates-io))

(define-public crate-ball_dont_lie-0.1.0 (c (n "ball_dont_lie") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0g7gm5amhlsmlgn2iqi9gs5x9ayzzxd8rpsxk2a3gmcmyaac417l")))

(define-public crate-ball_dont_lie-0.2.0 (c (n "ball_dont_lie") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07h025l2l7dhq5as3jvxjxack9l89kdy1qsp9amlpvacgzg0rqsa")))

