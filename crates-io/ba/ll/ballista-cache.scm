(define-module (crates-io ba ll ballista-cache) #:use-module (crates-io))

(define-public crate-ballista-cache-0.12.0 (c (n "ballista-cache") (v "0.12.0") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "hashlink") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("macros" "parking_lot" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)))) (h "1s276bz6x4x4spyjxq51hyb1d2ij0x7zz43n9gsv06954a4v1cg0")))

