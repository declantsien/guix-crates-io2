(define-module (crates-io ba ll ballista-proto) #:use-module (crates-io))

(define-public crate-ballista-proto-0.1.0 (c (n "ballista-proto") (v "0.1.0") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)))) (h "1bnhqi751xsi617ybbyd5gi5wq48clpl6lmyynifywf43wrxixj4") (y #t)))

