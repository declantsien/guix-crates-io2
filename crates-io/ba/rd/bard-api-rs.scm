(define-module (crates-io ba rd bard-api-rs) #:use-module (crates-io))

(define-public crate-bard-api-rs-0.1.0 (c (n "bard-api-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cookie") (r "^0.17.0") (d #t) (k 0)) (d (n "ezio") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1sm384f71x16qndym246pldz5lr9nffxrwkaz928vdbfswb37d45")))

