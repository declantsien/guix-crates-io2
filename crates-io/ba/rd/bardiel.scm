(define-module (crates-io ba rd bardiel) #:use-module (crates-io))

(define-public crate-bardiel-0.1.0 (c (n "bardiel") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("net" "io-std" "io-util" "macros" "rt-multi-thread"))) (k 0)))) (h "0y1m9f83d3j0dclhi4v641mc05pfikri442j1a8fdqqqswhx0q4m")))

