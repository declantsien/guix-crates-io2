(define-module (crates-io ba ch bachue-auto_impl) #:use-module (crates-io))

(define-public crate-bachue-auto_impl-0.5.1 (c (n "bachue-auto_impl") (v "0.5.1") (d (list (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "183w7ns7w9lnabp7xsp525jgc2zqzd7mkjyxdfq4xnxxq6gjyrwb") (f (quote (("nightly")))) (r "1.37")))

