(define-module (crates-io ba ss bass-sys) #:use-module (crates-io))

(define-public crate-bass-sys-1.0.0 (c (n "bass-sys") (v "1.0.0") (d (list (d (n "widestring") (r "^0.4") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0whs2i5mxrdkzdn9334g3w7lvwl2v3vv4mb9wab79f65k5gldlik")))

(define-public crate-bass-sys-1.1.0 (c (n "bass-sys") (v "1.1.0") (d (list (d (n "widestring") (r "^0.4") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0g9pxvvhirzbb8615rxrx726cgvn3wpcda7l8b9xfyvxz612yzw9")))

(define-public crate-bass-sys-1.1.1 (c (n "bass-sys") (v "1.1.1") (d (list (d (n "widestring") (r "^0.4") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "024y1j0f6lw9w7qw25bdgh5xg3k0cz1vsvq7kiw6g1z9bx09nv77")))

(define-public crate-bass-sys-1.1.2 (c (n "bass-sys") (v "1.1.2") (d (list (d (n "widestring") (r "^0.4") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "14g0svqin0hl3xxqdi3r79i0z4rcqz5lcz1li8f3ssar1phl748g")))

(define-public crate-bass-sys-1.1.3 (c (n "bass-sys") (v "1.1.3") (d (list (d (n "widestring") (r "^0.4") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "16m1fhf61xvlysyr31wyaxw2s5275jz3sis96fppnf9xmxqddi2a")))

(define-public crate-bass-sys-1.1.4 (c (n "bass-sys") (v "1.1.4") (d (list (d (n "widestring") (r "^0.4") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1i4y94h4g9wpichvsfxs5kcr2aswa95khiy3s8fmmmz1gfh0lh9d")))

(define-public crate-bass-sys-2.0.0 (c (n "bass-sys") (v "2.0.0") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1pkpzsj33v58cphcirn8xq7j4iyic8553y9nri2sfxapx6bh8ayq")))

(define-public crate-bass-sys-2.0.1 (c (n "bass-sys") (v "2.0.1") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1mgv0cr2f1vsf4n8db2bjdgym5zdm422m37082sp9rfqqn5q6ylg")))

(define-public crate-bass-sys-2.1.0 (c (n "bass-sys") (v "2.1.0") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0ffxi9lcw5b38262dh9d96xaq5yc4zqgagkzkyd5qwhxzlmrd31d")))

(define-public crate-bass-sys-2.2.0 (c (n "bass-sys") (v "2.2.0") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0ri5sr2vg2wihzvyjh9s5vw7rq1r1gxjpqa54d9rli2lw3s59vr8")))

(define-public crate-bass-sys-2.2.1 (c (n "bass-sys") (v "2.2.1") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0i73lfw1a28q9kqdjrns5xpazwi3zcv7d6rrs201x0syh772n52w")))

(define-public crate-bass-sys-2.2.2 (c (n "bass-sys") (v "2.2.2") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "1rmjixy2357klfgz7a4vwyzy9hmbd4rr8x2q91h4swfgf15fsmaj")))

(define-public crate-bass-sys-2.3.0 (c (n "bass-sys") (v "2.3.0") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "19cx8h3f5r8vansvsggip2vmdiwrd6d6gic11q8428g0ybbhyxik")))

