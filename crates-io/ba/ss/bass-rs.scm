(define-module (crates-io ba ss bass-rs) #:use-module (crates-io))

(define-public crate-bass-rs-0.1.0 (c (n "bass-rs") (v "0.1.0") (d (list (d (n "bass-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "1kcaz8jsnhnq67gmf55gpzjgd0mfgni8i23a6d2qfw523vl8brd4") (f (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.1.1 (c (n "bass-rs") (v "0.1.1") (d (list (d (n "bass-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "0962882nkq2267vx2ggl0zw01ll0v4l37hr9hj49x205dl75i0ml") (f (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.1.2 (c (n "bass-rs") (v "0.1.2") (d (list (d (n "bass-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "17599aqbdrff82l987f1l0l5vhpvaqqb8yhrbkk78z4c3adhmkhm") (f (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.1.3 (c (n "bass-rs") (v "0.1.3") (d (list (d (n "bass-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "01nha19v8i8zjrh1ab99f4ldg7wgcyl30ikd5bwhvwpdd3cjbj6h") (f (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.2.0 (c (n "bass-rs") (v "0.2.0") (d (list (d (n "bass-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "0yyipszzvkl35xc0py839nkckrcm3li3dj4c3hh6kpx93a47yryc") (f (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.2.1 (c (n "bass-rs") (v "0.2.1") (d (list (d (n "bass-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "19bc9k9zdalc2ypamsdklrvnbhi7yl2rjgfvpy2jkaafaxq67b1k") (f (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.2.2 (c (n "bass-rs") (v "0.2.2") (d (list (d (n "bass-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "0pp5f6nby8nb8vf6j5859l71x86hvaj14yg6yb3fjy4cgkf9lrdn") (f (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.2.3 (c (n "bass-rs") (v "0.2.3") (d (list (d (n "bass-sys") (r "^2.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)))) (h "19l33b3hwv8836p7fixq1zlzgn601iw200dqrwmd5n2si0n56i1m") (f (quote (("drop_debug") ("bass_fx"))))))

