(define-module (crates-io ba dd badder_lang) #:use-module (crates-io))

(define-public crate-badder_lang-0.1.0 (c (n "badder_lang") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "single_value_channel") (r "^1") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.17") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "0486hmg295k8cwdhi4pf6p16h7p4dfn83wj9qd1j5k8pklb9q6fr")))

(define-public crate-badder_lang-0.1.1 (c (n "badder_lang") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "single_value_channel") (r "^1") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.17") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "1rid9nbpy7k6nskq2qv38z2zfwfk42z5pls68vwqjn0zrqyzm4kn")))

(define-public crate-badder_lang-0.2.0 (c (n "badder_lang") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "single_value_channel") (r "^1") (d #t) (k 0)) (d (n "smol_str") (r "^0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "0bhqnn7d515z2n8yb9nsd69wsq48l4zvkpqz2x2g3199ihvbk1qx")))

(define-public crate-badder_lang-0.3.0 (c (n "badder_lang") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "single_value_channel") (r "^1") (d #t) (k 0)) (d (n "smol_str") (r "^0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)))) (h "0dvvj9apf3nqg4jbda6aqpgbnakj4mdg9xx7sxsymypa3w79k1an")))

(define-public crate-badder_lang-0.3.1 (c (n "badder_lang") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "single_value_channel") (r "^1") (d #t) (k 0)) (d (n "smol_str") (r "^0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.11") (d #t) (k 0)))) (h "1j8vfj48vmvf5i3zac0j4d1qxbzyklr5nqdj98zwyzhc7hldlfsh")))

(define-public crate-badder_lang-0.3.2 (c (n "badder_lang") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "single_value_channel") (r "^1") (d #t) (k 0)) (d (n "smol_str") (r "^0.2") (d #t) (k 0)) (d (n "strsim") (r "^0.11") (d #t) (k 0)))) (h "1zvrw4yp1py88wzffxbd5sx89f8l31iqv3rplw9pys1ggdib7zny")))

