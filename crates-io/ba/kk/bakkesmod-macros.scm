(define-module (crates-io ba kk bakkesmod-macros) #:use-module (crates-io))

(define-public crate-bakkesmod-macros-0.1.0 (c (n "bakkesmod-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "0x92glha5z5yszviq9rjqn73dkxryp4103js0cn8zcb5y83qkaiy")))

(define-public crate-bakkesmod-macros-0.1.1 (c (n "bakkesmod-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "0f0qz0bs0qqb0f6nd3r6zmrn1wh9pz1rsh6r4xfsjk44fvnw0xvp")))

