(define-module (crates-io ba kk bakkesmod) #:use-module (crates-io))

(define-public crate-bakkesmod-0.1.0 (c (n "bakkesmod") (v "0.1.0") (d (list (d (n "bakkesmod-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0ninmgziygdjpk6a93zkh3r20j85flbrkclrf9y6971xif8bs6xk")))

(define-public crate-bakkesmod-0.1.1 (c (n "bakkesmod") (v "0.1.1") (d (list (d (n "bakkesmod-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1ihjab2h3h8a8janylpwmwjhydh405pmqipzdmazq72y03b6abxi")))

(define-public crate-bakkesmod-0.1.2 (c (n "bakkesmod") (v "0.1.2") (d (list (d (n "bakkesmod-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1a5xr0mf41cx9qlaaw5p8y5iypqzpms33fykn9xrxgi3w2hmma8v")))

(define-public crate-bakkesmod-0.2.0 (c (n "bakkesmod") (v "0.2.0") (d (list (d (n "bakkesmod-macros") (r "^0.1.1") (d #t) (k 0)))) (h "06yajd2hwik88r0yx87hgfvqj681rv477r0mja17bcvsd8mq8llc")))

(define-public crate-bakkesmod-0.2.1 (c (n "bakkesmod") (v "0.2.1") (d (list (d (n "bakkesmod-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1af7xy93pqamv81zz52lkzq8g8zixx1nhmc9l7x61y20f1mb7sj9")))

