(define-module (crates-io ba kk bakkesmod-cli) #:use-module (crates-io))

(define-public crate-bakkesmod-cli-0.1.0 (c (n "bakkesmod-cli") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "websocket") (r "^0.26.2") (d #t) (k 0)))) (h "1m4qgy2j8g7nb7bp340lxg41af3lk98hdfc5dxzsd86pryd44011")))

