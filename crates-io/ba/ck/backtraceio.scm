(define-module (crates-io ba ck backtraceio) #:use-module (crates-io))

(define-public crate-backtraceio-0.2.0 (c (n "backtraceio") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rustc_version_runtime") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ib1f6wjkki6jsbqwgiji22wl7900dz84gnvfiajmb1b0i47lkf7")))

