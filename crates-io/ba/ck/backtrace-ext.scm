(define-module (crates-io ba ck backtrace-ext) #:use-module (crates-io))

(define-public crate-backtrace-ext-0.1.0 (c (n "backtrace-ext") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)))) (h "0g6jpmyg4fx7022d31dspvkm0dbmxsj8k6qq4mycpmcs1fv88qyb")))

(define-public crate-backtrace-ext-0.2.0 (c (n "backtrace-ext") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)))) (h "1jns0g8hx1n1p07prgiis2rk4mr9c4cfc4nmhnhvfbs1rmvymd09")))

(define-public crate-backtrace-ext-0.2.1 (c (n "backtrace-ext") (v "0.2.1") (d (list (d (n "backtrace") (r "^0.3.61") (d #t) (k 0)) (d (n "miette") (r "^5.6.0") (f (quote ("fancy"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 2)))) (h "0l4xacjnx4jrn9k14xbs2swks018mviq03sp7c1gn62apviywysk") (r "1.56")))

