(define-module (crates-io ba ck backstage) #:use-module (crates-io))

(define-public crate-backstage-0.1.1 (c (n "backstage") (v "0.1.1") (d (list (d (n "async-recursion") (r "^0.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.5") (f (quote ("sync" "time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("macros" "rt-multi-thread" "signal"))) (d #t) (k 2)))) (h "143glwlgssv52pl4zsj0kg48x4jwzrs28ahfidc49h1387ckyxc2")))

