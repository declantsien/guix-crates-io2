(define-module (crates-io ba ck backtrace-rust) #:use-module (crates-io))

(define-public crate-backtrace-rust-0.1.0 (c (n "backtrace-rust") (v "0.1.0") (d (list (d (n "addr2line") (r "^0.12") (d #t) (k 0)) (d (n "gimli") (r "^0.21") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "object") (r "^0.17") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1") (d #t) (k 0)))) (h "0gmmyxzmgpx6bwpirh0wqa4kmvcxdsh264f9gcgbpd1blqf3j30m") (f (quote (("logging"))))))

