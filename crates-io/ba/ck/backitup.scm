(define-module (crates-io ba ck backitup) #:use-module (crates-io))

(define-public crate-backitup-0.1.0 (c (n "backitup") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "1ps54rp3zsfsnm7dh13h5ffnqydzk2yvhdickx6s7y68xizx907a")))

(define-public crate-backitup-0.1.1 (c (n "backitup") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "029x16vxdszizabd4mrdmsncfqw8h2f321vj8vrjb63p7i7l6bm4")))

