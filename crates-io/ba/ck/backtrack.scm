(define-module (crates-io ba ck backtrack) #:use-module (crates-io))

(define-public crate-backtrack-0.1.1 (c (n "backtrack") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1xly65f44vj91ww9lawy4lzxi2vd9qaivwxlf2p83r0562f89naw")))

(define-public crate-backtrack-0.2.0 (c (n "backtrack") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1a9rw5rzfhv6dzn091lkvs3sns8q75rdj9kfxpvmcmwnv0d1bqd3")))

(define-public crate-backtrack-0.2.2 (c (n "backtrack") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "07j64v4g6ij6qg21b5kwb15wfy4wmn3dcw3wnxa55qzl82jafhng")))

(define-public crate-backtrack-0.3.0 (c (n "backtrack") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0bjqclhzxk7bpgc098dg1r9g1c8flvkcbphngfzf19hchqaa792b")))

