(define-module (crates-io ba ck background_worker) #:use-module (crates-io))

(define-public crate-background_worker-0.1.0 (c (n "background_worker") (v "0.1.0") (h "0i7nnf1wjlrza5mlxqc9nqdy7ml5hgjw6g328l9n4zbl29p9vh8b")))

(define-public crate-background_worker-0.1.1 (c (n "background_worker") (v "0.1.1") (h "08avmq58f9y29sysc7197cn6678ph8qm2vfcq6idf8fax4whscgw")))

