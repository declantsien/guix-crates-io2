(define-module (crates-io ba ck backroll_transport) #:use-module (crates-io))

(define-public crate-backroll_transport-0.1.0 (c (n "backroll_transport") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "dashmap") (r "^4.0") (d #t) (k 0)))) (h "035hv63shh8kszrs84a89hayjbgsggaw90mpnhbcj6n81gj8njrl")))

(define-public crate-backroll_transport-0.1.1 (c (n "backroll_transport") (v "0.1.1") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "dashmap") (r "^4.0") (d #t) (k 0)))) (h "02hsasl5xlcfwlfn2jdcx28h7xr0lmf8196v6rzj6sllj2ngmi6b")))

(define-public crate-backroll_transport-0.1.2 (c (n "backroll_transport") (v "0.1.2") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "dashmap") (r "^4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "0xyljhh68kvccq75xxxr4vzq89bz2bvswlkdkmncznc2wzm8q5pf")))

(define-public crate-backroll_transport-0.2.0 (c (n "backroll_transport") (v "0.2.0") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "1883qby5cpkgwl019n3akw8b1x3kvwb2vwfhhyhb9f2p96ch3agh")))

