(define-module (crates-io ba ck backr) #:use-module (crates-io))

(define-public crate-backr-0.2.0 (c (n "backr") (v "0.2.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0pdid458hb46qi9w962hf8nvk9dv3z0ifx0596sy12z481p8gp62")))

(define-public crate-backr-0.2.2 (c (n "backr") (v "0.2.2") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "182759hvymgxjkxkd5mdf8lwjg32dggbj7fi3n2glag69yg56pbs")))

(define-public crate-backr-0.3.0 (c (n "backr") (v "0.3.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0q4hcvxbc2wl0bkzxysjrzp5dd6cwza7lqqmzlgp11939yyr5h77")))

(define-public crate-backr-0.4.0 (c (n "backr") (v "0.4.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "progress") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "12j6rs2k9m3n0lx5h7d1rjdk8w87877dqhm93yw81ix0pypl7z78")))

(define-public crate-backr-0.4.1 (c (n "backr") (v "0.4.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "progress") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0hjnakzhywj810bj6gki8cp2lmz8c6jbdil65ix31py712zbi2vw")))

(define-public crate-backr-0.5.0 (c (n "backr") (v "0.5.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "progress") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0bad0g34rqd0zv28gg9i2hzb0hrl10q3xq8nrbvsykyzfvsph2b0")))

