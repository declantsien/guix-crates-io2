(define-module (crates-io ba ck backtrace-sys) #:use-module (crates-io))

(define-public crate-backtrace-sys-0.1.0 (c (n "backtrace-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0sfwmxd9scx5vmilpw65185h8w638s50i4sdm5al59jba32azsg1")))

(define-public crate-backtrace-sys-0.1.1 (c (n "backtrace-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0ikwbmkr7m5agjkkws3calwchz31c6q966hkk0i72icavz1v2v0j")))

(define-public crate-backtrace-sys-0.1.2 (c (n "backtrace-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "03z63553m7k3g330cxwfahalnjw07583vmdxz1y6wkbbcpablsj5")))

(define-public crate-backtrace-sys-0.1.3 (c (n "backtrace-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ll0cbbljm9rmdg8kc5rgrgkdm94n7f0zaxgg2568v63nbidc5ij")))

(define-public crate-backtrace-sys-0.1.4 (c (n "backtrace-sys") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "023y95a36q2b6ggppxnjhs5p9y9lfinz02cyn2kv8sz0x1d7hwzz")))

(define-public crate-backtrace-sys-0.1.5 (c (n "backtrace-sys") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dvbdzs452jj24x8ch4lwadlm25kwb55byh5hn50hdikqkcfh0in")))

(define-public crate-backtrace-sys-0.1.6 (c (n "backtrace-sys") (v "0.1.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bkg2yqyyw5cbakz5zly6rf402bqw8r9wbd1a7cs8jhimb1lj6x8")))

(define-public crate-backtrace-sys-0.1.7 (c (n "backtrace-sys") (v "0.1.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02n58r9hv59dmy7wkqibcimnfk5j63dn79kmdwkr1pwfcx6iv05p")))

(define-public crate-backtrace-sys-0.1.8 (c (n "backtrace-sys") (v "0.1.8") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "046kngjchj4j3ha49gp8c0hsi2i5glwpcddgh1kxnb2yxc2wnb92")))

(define-public crate-backtrace-sys-0.1.9 (c (n "backtrace-sys") (v "0.1.9") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h7mlzipx8jspa5gv6gjkjbbs9wdjdn1afqb8vz1q48q4nwiy3qs")))

(define-public crate-backtrace-sys-0.1.10 (c (n "backtrace-sys") (v "0.1.10") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hiqsf9s5q2gxycm2lszp5vf6x2i5hnfrwn1jxsckyrjj49gv4ni")))

(define-public crate-backtrace-sys-0.1.11 (c (n "backtrace-sys") (v "0.1.11") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zziv4gkvw5jndw862v494km8j1ri0w9p2pp5fz95kl1lwp8839s")))

(define-public crate-backtrace-sys-0.1.12 (c (n "backtrace-sys") (v "0.1.12") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0z4wg79hg4cck7vxam6gkp2zh1ilz8055mb0vz6ancx35dvwbk5g")))

(define-public crate-backtrace-sys-0.1.14 (c (n "backtrace-sys") (v "0.1.14") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12fi5hljlgbp4xkg2rpz7gs4xy2imhqayp8gkm011nwgxx0s2gn6")))

(define-public crate-backtrace-sys-0.1.15 (c (n "backtrace-sys") (v "0.1.15") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1f02kid6b431qg7fbazl9nqbyj4r7jxjmp8flxdaqalkxjzc6zxi")))

(define-public crate-backtrace-sys-0.1.16 (c (n "backtrace-sys") (v "0.1.16") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qbn7whb0zi35hajmha8xzny8ryhdfmq5564mxbhy6qnsrhmfn24")))

(define-public crate-backtrace-sys-0.1.17 (c (n "backtrace-sys") (v "0.1.17") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02vpd2qrgl3n6yjv4l5fqgylqsy0v8ai9iq2jcsypn2q6w7cwi0a")))

(define-public crate-backtrace-sys-0.1.18 (c (n "backtrace-sys") (v "0.1.18") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03zm0xad3d0s86niga0rysv6ck5v4pvy2z8pwxvqi8dfrrjyigjb")))

(define-public crate-backtrace-sys-0.1.19 (c (n "backtrace-sys") (v "0.1.19") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kcs40rfh2ch6wxk3z1agyy1hyfdv4p17wysarq83maiavbsal9r")))

(define-public crate-backtrace-sys-0.1.20 (c (n "backtrace-sys") (v "0.1.20") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1j84n47xyy4h7xkz2bqc80h918jrkv56s5k5rfm6qmfc1ysjh3g6")))

(define-public crate-backtrace-sys-0.1.21 (c (n "backtrace-sys") (v "0.1.21") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zmqrm99xzqkn1nkah5qjwlisj4myrlv05g5l34dym29q9l4wsml")))

(define-public crate-backtrace-sys-0.1.22 (c (n "backtrace-sys") (v "0.1.22") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lnfq767w8qkkzxm5fiqzb3zyv2jq15jdpiqdzvh6ikc8si47lsz")))

(define-public crate-backtrace-sys-0.1.23 (c (n "backtrace-sys") (v "0.1.23") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zkamwz598sc9sh5plyivzcm1najmh70y2azdf70nv2m0q67vxmz")))

(define-public crate-backtrace-sys-0.1.24 (c (n "backtrace-sys") (v "0.1.24") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "186sd4nc5mgzvpfa045804v5nbr6p3s37xnsmim7zl5binn5cvf6")))

(define-public crate-backtrace-sys-0.1.25 (c (n "backtrace-sys") (v "0.1.25") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "libc") (r "^0.2") (k 0)))) (h "1ibq927qd5ywb9qyflpwni9wad0aw7fwyszxhza8609p7i8b7s90") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-backtrace-sys-0.1.26 (c (n "backtrace-sys") (v "0.1.26") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0y8jixgd7a3mijw0q9ymg8z5nhamgcgm0d4lmaf99j6mbagfik1z")))

(define-public crate-backtrace-sys-0.1.27 (c (n "backtrace-sys") (v "0.1.27") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "libc") (r "^0.2") (k 0)))) (h "13acng0n013y0ks887lk9rfwkchd19kidv3brfid3cqjn3bhvabf") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-backtrace-sys-0.1.28 (c (n "backtrace-sys") (v "0.1.28") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1dk9sx6sbm73aihlln8l2m9ia1s4vqmqdfd7z2kr5k2wq8586z3r")))

(define-public crate-backtrace-sys-0.1.29 (c (n "backtrace-sys") (v "0.1.29") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "libc") (r "^0.2") (k 0)))) (h "1czx2m431z30aji2g8jlbshkjqsi93xcj9jsmmlwh7qxxwg9zjqj") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-backtrace-sys-0.1.30 (c (n "backtrace-sys") (v "0.1.30") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "libc") (r "^0.2") (k 0)))) (h "0q9yxdhq1sgjcqw068g4m75ba11v83ycn0dwc6pm6dalkh5h0fjv") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-backtrace-sys-0.1.31 (c (n "backtrace-sys") (v "0.1.31") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "libc") (r "^0.2") (k 0)))) (h "0as2pk77br4br04daywhivpi1ixxb8y2c7f726kj849dxys31a42") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-backtrace-sys-0.1.32 (c (n "backtrace-sys") (v "0.1.32") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "libc") (r "^0.2") (k 0)))) (h "14c406z8bdmms8a5l8cv79jfkz1mk10qk5p97izf4vai53qparax") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-backtrace-sys-0.1.33 (c (n "backtrace-sys") (v "0.1.33") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "libc") (r "^0.2") (k 0)))) (h "0dfwq31a7842s8nsgibjzjlppsx036db486amdsps2n46zkm4yz1") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins") ("default" "backtrace-sys") ("backtrace-sys"))))))

(define-public crate-backtrace-sys-0.1.34 (c (n "backtrace-sys") (v "0.1.34") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "libc") (r "^0.2") (k 0)))) (h "0sgdq7my0zjhp2zl72phxif4b5a6hwx2iwzfl9x1mbkv0nq7syfa") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins") ("default" "backtrace-sys") ("backtrace-sys"))))))

(define-public crate-backtrace-sys-0.1.35 (c (n "backtrace-sys") (v "0.1.35") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "libc") (r "^0.2") (k 0)))) (h "066iviphi72mx9hd3njzsplk5v45jhi10mrccbbyij391ahsps3x") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins") ("default" "backtrace-sys") ("backtrace-sys"))))))

(define-public crate-backtrace-sys-0.1.36 (c (n "backtrace-sys") (v "0.1.36") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "libc") (r "^0.2") (k 0)))) (h "1iqvmqsnnk1n55l1rl9d6v8fghngvsfas28kbm4a4m8jxqc8g13q") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins") ("default" "backtrace-sys") ("backtrace-sys"))))))

(define-public crate-backtrace-sys-0.1.37 (c (n "backtrace-sys") (v "0.1.37") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "libc") (r "^0.2") (k 0)))) (h "16a3igz22q9lnnjjr77f4k8ci48v8zdwrs67khx3h7wx3jzfpyqq") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins") ("default" "backtrace-sys") ("backtrace-sys"))))))

