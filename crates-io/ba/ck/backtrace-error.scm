(define-module (crates-io ba ck backtrace-error) #:use-module (crates-io))

(define-public crate-backtrace-error-0.1.0 (c (n "backtrace-error") (v "0.1.0") (h "0pdlyw2nfdrkpm2xs4jw0rnw9337g4xl095k1gldzn0hb0n7wrzp")))

(define-public crate-backtrace-error-0.2.0 (c (n "backtrace-error") (v "0.2.0") (h "0zbzriil79nqvqpfcxbwfmbqycqpf528fnzm3ps85b0lykmgzsih")))

(define-public crate-backtrace-error-0.3.0 (c (n "backtrace-error") (v "0.3.0") (h "1xvxncwlrwymkhvns6mf6753yhpcp2a8sg8yr4apyrp76lmjgfkl")))

(define-public crate-backtrace-error-0.4.0 (c (n "backtrace-error") (v "0.4.0") (h "1kpmynin2lkd0b2m54h3ks3mp0bpnvmjvrj8ys2avnv09nlxzd9h") (r "1.65")))

(define-public crate-backtrace-error-0.5.0 (c (n "backtrace-error") (v "0.5.0") (h "1rksjv2i7hnk6a6m5qkzl104r0fg7mcbzb1x2zgyj5im595jmpbr") (r "1.65")))

(define-public crate-backtrace-error-0.5.1 (c (n "backtrace-error") (v "0.5.1") (h "0wbrzhswy9d9hlrzq5v6d3bp2zv8723ln4ydagnlsqbc0g5mlcak") (r "1.65")))

