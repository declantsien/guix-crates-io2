(define-module (crates-io ba ck backtracking_iterator) #:use-module (crates-io))

(define-public crate-backtracking_iterator-0.1.0 (c (n "backtracking_iterator") (v "0.1.0") (h "14gsisa07ss95djzdng030fayd0c5la0lj50458irgbm4prwkhk9")))

(define-public crate-backtracking_iterator-0.2.0 (c (n "backtracking_iterator") (v "0.2.0") (h "1xkfdihg5amxmv4jr7l679v9hh0dsi5awycj0g5dvpr18pw81j56")))

(define-public crate-backtracking_iterator-0.2.1 (c (n "backtracking_iterator") (v "0.2.1") (h "1v36vcn6n56lr01bzynk80xn57yyg6p7dkg7v9yybsiqg35dn32y")))

(define-public crate-backtracking_iterator-0.3.0 (c (n "backtracking_iterator") (v "0.3.0") (h "14adylnw53fbyjrrqi7lm57zfyzcpz4as1nsz1r6s2a7v8h0aqya")))

(define-public crate-backtracking_iterator-0.3.1 (c (n "backtracking_iterator") (v "0.3.1") (h "1mxf1qn242hn4yhva47bjl4iaawbyas2gc57j1pyw374pvcmn53d")))

(define-public crate-backtracking_iterator-0.4.0 (c (n "backtracking_iterator") (v "0.4.0") (h "0x6a9l6klgq9hlmliacqv5567024qbx2cnrlmwmp7z38a40yk7rv")))

(define-public crate-backtracking_iterator-0.4.1 (c (n "backtracking_iterator") (v "0.4.1") (h "0ihpml0619df0ha23r7zyhjllk094cdvkc2yk3x08ha6jm2v7wll")))

(define-public crate-backtracking_iterator-0.4.2 (c (n "backtracking_iterator") (v "0.4.2") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "0s3y4hd5rkp9h92flzk1r3yxv0zzz3mw1vfdsgscrwpndngzzsr9") (f (quote (("slice"))))))

(define-public crate-backtracking_iterator-0.4.3 (c (n "backtracking_iterator") (v "0.4.3") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "0i0xiy5ry6c247qvfxh4xgli9waci040ynagb8gmf41rry6n5ds2") (f (quote (("slice"))))))

(define-public crate-backtracking_iterator-0.4.4 (c (n "backtracking_iterator") (v "0.4.4") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "0paglg6xjnqh1fdrvqzrcgkycyq6k03pdbn87p7sgvlc3ir8zh3w") (f (quote (("slice"))))))

