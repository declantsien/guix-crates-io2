(define-module (crates-io ba ck backpub) #:use-module (crates-io))

(define-public crate-backpub-0.1.0 (c (n "backpub") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ds950y8f3f1bcfv5ryi40ikamkbwh6j1g9ns288gq1zp1397dbz")))

(define-public crate-backpub-0.1.1 (c (n "backpub") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)))) (h "0psgk8ahbjzgpkirg5cy1lc42qmxkahbhywfzijc1zxpxncaivhv")))

