(define-module (crates-io ba ck backtalk) #:use-module (crates-io))

(define-public crate-backtalk-0.1.0 (c (n "backtalk") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "queryst-prime") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cma0p9wxbg628gxrq3vr4sgk3phnvzq1hp2qydzvnz229qfisdh")))

