(define-module (crates-io ba ck backslash) #:use-module (crates-io))

(define-public crate-backslash-0.1.0 (c (n "backslash") (v "0.1.0") (h "11pp7s3fznyndxz0d9qmz9k9ssnqi79vf38wf60kqzrckyh9xa1m")))

(define-public crate-backslash-0.2.0 (c (n "backslash") (v "0.2.0") (h "0bg4hg4svz680jp1cd2ikair288n6x7bq1ln23mf8w7z5dwc17zw")))

