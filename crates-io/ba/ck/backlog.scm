(define-module (crates-io ba ck backlog) #:use-module (crates-io))

(define-public crate-backlog-0.1.0 (c (n "backlog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("formatting" "parsing"))) (d #t) (k 0)))) (h "0wv8ihw3i7wixjpdl835q94dpd0r27jfyf2vjgz8r8bv6945jmyf") (y #t)))

