(define-module (crates-io ba ck backtrace-on-stack-overflow) #:use-module (crates-io))

(define-public crate-backtrace-on-stack-overflow-0.1.0 (c (n "backtrace-on-stack-overflow") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.54") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "nix") (r "^0.19.0") (d #t) (k 0)))) (h "0jk3lm2zhbgfmj6r8i3gi0105p330h5iykasrdgl4n1c00518vxl")))

(define-public crate-backtrace-on-stack-overflow-0.1.1 (c (n "backtrace-on-stack-overflow") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.54") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "nix") (r "^0.19.0") (d #t) (k 0)))) (h "09q8nhyjy5xkkfljylp98wkip5v2zq3y488jwlhp1qh2ar79jsb5")))

(define-public crate-backtrace-on-stack-overflow-0.2.0 (c (n "backtrace-on-stack-overflow") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3.65") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)))) (h "1gf9b8wblqq74aal0g6130wq6a5lnmi9dji72xq71jypwimyy76m")))

(define-public crate-backtrace-on-stack-overflow-0.3.0 (c (n "backtrace-on-stack-overflow") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3.65") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)))) (h "06w4vy076h2kl2khdynimgmss6vcf1hf4mbks4d7lwzk4w2xglkz")))

