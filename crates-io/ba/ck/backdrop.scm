(define-module (crates-io ba ck backdrop) #:use-module (crates-io))

(define-public crate-backdrop-0.1.0 (c (n "backdrop") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "rt" "full"))) (o #t) (d #t) (k 0)))) (h "0gqvrl64awh2q1hm9myh2lar1w7b74yjzcnp7ci39k5516kj5vqi") (f (quote (("std" "lazy_static" "alloc") ("default" "std" "tokio") ("alloc"))))))

(define-public crate-backdrop-0.1.1 (c (n "backdrop") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "rt" "full"))) (o #t) (d #t) (k 0)))) (h "12q5s1ak9mdvy9vs84yrph4jk5wcjw0566928216gc7a0qzpf160") (f (quote (("std" "lazy_static" "alloc") ("default" "std" "tokio") ("alloc"))))))

(define-public crate-backdrop-0.1.2 (c (n "backdrop") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "rt" "full"))) (o #t) (d #t) (k 0)))) (h "1d0frl8l07c8gc1kh2zx3m30ny1bdrfpazx83mks2xklfaqkxsvn") (f (quote (("std" "lazy_static" "alloc") ("default" "std" "tokio") ("alloc"))))))

(define-public crate-backdrop-0.1.3 (c (n "backdrop") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "rt" "full"))) (o #t) (d #t) (k 0)))) (h "0j8gflswz1647nq21b63z7w48anhslmc0n9b9p45wafwd14b1rgf") (f (quote (("std" "lazy_static" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-backdrop-0.1.4 (c (n "backdrop") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "rt" "full"))) (o #t) (d #t) (k 0)))) (h "0fkbfif34mr6x0d50qr4ak94jcblszfwbvdxm3kq785vnzkdwan9") (f (quote (("std" "lazy_static" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-backdrop-0.1.5 (c (n "backdrop") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "rt" "full"))) (o #t) (d #t) (k 0)))) (h "16vym7n26v1n7r07qxv8zgpl9qdmach47g1hx4qb65r37733r226") (f (quote (("std" "lazy_static" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-backdrop-0.1.6 (c (n "backdrop") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "rt" "full"))) (o #t) (d #t) (k 0)))) (h "07yfi12dr416jd6yz1fhpz5yxzsbnql89rb1myxwwqmzkr63y4bz") (f (quote (("std" "lazy_static" "alloc") ("doc" "std" "tokio") ("default" "std") ("alloc"))))))

(define-public crate-backdrop-0.1.7 (c (n "backdrop") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "rt" "full"))) (o #t) (d #t) (k 0)))) (h "0y281c84pkqn8xwybcnsy1r68dmz40v9kl9za6g2v4f3s2gc9fag") (f (quote (("std" "lazy_static" "alloc") ("doc" "std" "tokio") ("default" "std") ("alloc"))))))

(define-public crate-backdrop-0.1.8 (c (n "backdrop") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "rt" "full"))) (o #t) (d #t) (k 0)))) (h "0zfdf9ywqw6rms0fgdm2sanja8r3syc334785pnkwc2mjb8k1vq8") (f (quote (("std" "lazy_static" "alloc") ("doc" "std" "tokio") ("default" "std") ("alloc"))))))

(define-public crate-backdrop-0.1.9 (c (n "backdrop") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "rt" "full"))) (o #t) (d #t) (k 0)))) (h "1940rhimzz0acicj26adi2rrh5qgmcwgvb26li0jz1h48mx4j7vm") (f (quote (("std" "lazy_static" "alloc") ("doc" "std" "tokio") ("default" "std") ("alloc"))))))

(define-public crate-backdrop-0.1.10 (c (n "backdrop") (v "0.1.10") (d (list (d (n "bytecheck") (r "^0.7.0") (o #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7.40") (o #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "rt" "full"))) (o #t) (d #t) (k 0)))) (h "0swrf4n83ipgx8aaxzhfhaidyrvkbwgys5i42xlxr0ibhnb8nxgh") (f (quote (("std" "lazy_static" "alloc") ("doc" "std" "tokio" "rkyv" "bytecheck") ("default" "std") ("alloc"))))))

(define-public crate-backdrop-0.1.11 (c (n "backdrop") (v "0.1.11") (d (list (d (n "bytecheck") (r "^0.7.0") (o #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7.40") (o #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "rt" "full"))) (o #t) (d #t) (k 0)))) (h "15qdfr2k58rjq7r7wvnrli4q4c90aqfvzkdrj4k86b47hsiw3p02") (f (quote (("std" "lazy_static" "alloc") ("doc" "std" "tokio" "rkyv/size_32" "bytecheck") ("default" "std") ("alloc"))))))

