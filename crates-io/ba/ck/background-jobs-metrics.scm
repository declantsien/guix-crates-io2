(define-module (crates-io ba ck background-jobs-metrics) #:use-module (crates-io))

(define-public crate-background-jobs-metrics-0.14.0 (c (n "background-jobs-metrics") (v "0.14.0") (d (list (d (n "metrics") (r "^0.20.1") (d #t) (k 0)) (d (n "metrics-util") (r "^0.14.0") (d #t) (k 0)))) (h "0x7gfzdc7m3xkrmwih0l94d7q5j60ijslyl8mmy5l0shfj9k144p")))

(define-public crate-background-jobs-metrics-0.15.0 (c (n "background-jobs-metrics") (v "0.15.0") (d (list (d (n "metrics") (r "^0.21.0") (d #t) (k 0)) (d (n "metrics-util") (r "^0.15.0") (d #t) (k 0)))) (h "16950f24l46pr9p1qcx2wadgzwl47diyf83gp1k40yn19h5l8738")))

(define-public crate-background-jobs-metrics-0.16.0 (c (n "background-jobs-metrics") (v "0.16.0") (d (list (d (n "metrics") (r "^0.22.0") (d #t) (k 0)) (d (n "metrics-util") (r "^0.16.0") (d #t) (k 0)))) (h "1i0s9r0dip77zsvpr9ajvd3xhffhwqyvihl79bqkqvbr4qhd8vd4")))

(define-public crate-background-jobs-metrics-0.17.0 (c (n "background-jobs-metrics") (v "0.17.0") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "background-jobs-core") (r "^0.17.0") (d #t) (k 0)) (d (n "metrics") (r "^0.22.0") (d #t) (k 0)) (d (n "metrics-util") (r "^0.16.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^1.6") (f (quote ("serde" "v7"))) (d #t) (k 0)))) (h "0y8zhb178c8yi32jk01v41wx5f9k7s9m9021z1kdcfyxv8dr56qi")))

(define-public crate-background-jobs-metrics-0.18.0 (c (n "background-jobs-metrics") (v "0.18.0") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "background-jobs-core") (r "^0.18.0") (d #t) (k 0)) (d (n "metrics") (r "^0.22.0") (d #t) (k 0)) (d (n "metrics-util") (r "^0.16.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^1.6") (f (quote ("serde" "v7"))) (d #t) (k 0)))) (h "1lh1njx3wcvzlacw9i0j465hk8rbz66bbjh75l29pzp87w7shzb1")))

