(define-module (crates-io ba ck backoff-macro) #:use-module (crates-io))

(define-public crate-backoff-macro-0.1.0 (c (n "backoff-macro") (v "0.1.0") (d (list (d (n "backoff") (r "^0.4.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d3b056ssn9hf3gh0a1pnpz0190bafdakwgh281pp7kih8yxf5x0")))

(define-public crate-backoff-macro-0.2.0 (c (n "backoff-macro") (v "0.2.0") (d (list (d (n "backoff") (r "^0.4.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p6fym801i5l8bhkzdbbgyk2ymnyq0kd0zh9bw73892zcw32cdkd")))

