(define-module (crates-io ba ck backtester) #:use-module (crates-io))

(define-public crate-backtester-0.1.0 (c (n "backtester") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tradestats") (r "^0.1.7") (d #t) (k 2)))) (h "0r233yx0kqa92qa6j9b1hmrv00w0knq77axwn87zfi9w395cmvwv")))

(define-public crate-backtester-0.1.1 (c (n "backtester") (v "0.1.1") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tradestats") (r "^0.1.7") (d #t) (k 2)))) (h "08miq423zw1rlcwmrwcchp211wpz93w40xlfz6g22mwkcs5chsy2")))

(define-public crate-backtester-0.1.2 (c (n "backtester") (v "0.1.2") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tradestats") (r "^0.1.7") (d #t) (k 2)))) (h "11z2l2yr074vv2z12msmr5053kklqi1g82ck0yf42ypdphbbixrw")))

(define-public crate-backtester-0.1.4 (c (n "backtester") (v "0.1.4") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tradestats") (r "^0.1.7") (d #t) (k 2)))) (h "0ajvv35jw7vav5n10g57g6ymabns44agqy02l6nwzar5jl80jdgs")))

(define-public crate-backtester-0.1.5 (c (n "backtester") (v "0.1.5") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tradestats") (r "^0.1.7") (d #t) (k 2)))) (h "147hpb5wjxl3znbgfsr1l74f0nhg4jbshmx8dngaasydqz0qii0q")))

(define-public crate-backtester-0.1.6 (c (n "backtester") (v "0.1.6") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tradestats") (r "^0.1.7") (d #t) (k 2)))) (h "0v6rl302iphkrim708flkwhk5kx08bd1724sdhx24g1s10gdvjyp")))

(define-public crate-backtester-0.1.7 (c (n "backtester") (v "0.1.7") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tradestats") (r "^0.1.7") (d #t) (k 2)) (d (n "ts-rs") (r "^6.2.1") (d #t) (k 0)))) (h "01h7rs5s2wpjqrwz15yb6w7y1f9b263bkfggsf82sim328wcm4x0")))

(define-public crate-backtester-0.1.8 (c (n "backtester") (v "0.1.8") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tradestats") (r "^0.1.7") (d #t) (k 2)) (d (n "ts-rs") (r "^6.2.1") (d #t) (k 0)))) (h "0lknrnzn68pyx8bq780ax11mj381zq6v2274mp34b6qdmr3csi6r")))

(define-public crate-backtester-0.1.9 (c (n "backtester") (v "0.1.9") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tradestats") (r "^0.1.7") (d #t) (k 2)) (d (n "ts-rs") (r "^7.0.0") (d #t) (k 0)))) (h "0bkkw82hb1fqqdswp6b5h43qfhb3viz4hyw32mkfjb590qaggz6n")))

