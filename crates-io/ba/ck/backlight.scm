(define-module (crates-io ba ck backlight) #:use-module (crates-io))

(define-public crate-backlight-0.1.0 (c (n "backlight") (v "0.1.0") (h "097grs14a76k0adm7q22yhy07wi2qhyq2sxbvv4cn4jvzlq0pk42")))

(define-public crate-backlight-0.1.1 (c (n "backlight") (v "0.1.1") (h "08czi1s6k0v43dfpfb3f8gl69ihjnixbsdw95wi54h63n87izpd1")))

