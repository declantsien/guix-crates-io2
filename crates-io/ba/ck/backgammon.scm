(define-module (crates-io ba ck backgammon) #:use-module (crates-io))

(define-public crate-backgammon-0.1.0 (c (n "backgammon") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.17") (d #t) (k 0)))) (h "0lll9h4zjv98w67a8ifrnvx1k2dhbcb3qja8lfnqwpgrxbxjl4dv")))

(define-public crate-backgammon-0.2.0 (c (n "backgammon") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0vp5amwk2gcxpfa4qrldif95lhms4b8jwwx46yyns7bm6aa0i9fg")))

(define-public crate-backgammon-0.3.0 (c (n "backgammon") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ivb0cgqil8fqiwjqdm0bp6ifm450aq71g7gwf1np8i7av765l9g")))

(define-public crate-backgammon-0.4.0 (c (n "backgammon") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.6") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "04vasfiivalqaphvk0ad8g894508k2akp4d1gkp62gafdv9y1ikh")))

(define-public crate-backgammon-0.5.0 (c (n "backgammon") (v "0.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1cl4d77vry9wn076dx6dynw7qh7681hy73842mdmxhrsj0v1gm6n")))

(define-public crate-backgammon-0.5.1 (c (n "backgammon") (v "0.5.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1zp8igsfzrgypbhqzgwszi3sngz85a92y1l0mb9m5pf5rwknqha9")))

(define-public crate-backgammon-0.6.0 (c (n "backgammon") (v "0.6.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11h5i6fdbs459k2w4dpj0cnrm7y84nb0gsdzxxjmfi10la9rj62c")))

