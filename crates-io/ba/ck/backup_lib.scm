(define-module (crates-io ba ck backup_lib) #:use-module (crates-io))

(define-public crate-backup_lib-0.1.1 (c (n "backup_lib") (v "0.1.1") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08gy3lhvpcxf9m5f1xijdp4bszlky8nwphcdqnarvjsyq6zipq5i")))

(define-public crate-backup_lib-0.1.2 (c (n "backup_lib") (v "0.1.2") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0spqzx5gmydf2gpmgyim142a6vvkvjh70bvxkvx06ma7qyklhb4a")))

(define-public crate-backup_lib-0.1.3 (c (n "backup_lib") (v "0.1.3") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cndh83krl0db18qgg0hj287ld1rr70gl52ysp0kh1hkypk5zk3h")))

(define-public crate-backup_lib-0.1.4 (c (n "backup_lib") (v "0.1.4") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gir9l6lbf8kipwxfiinj8y6jl4qspq5y27w7j38yijl70zp3x8j")))

(define-public crate-backup_lib-0.1.5 (c (n "backup_lib") (v "0.1.5") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g4chwmjcljwx6apq2s365xj60x7nvh8rmzy7vzk3xgd8647x674")))

(define-public crate-backup_lib-0.1.6 (c (n "backup_lib") (v "0.1.6") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gqgxl6j972yafl4453qz4raa2rbg07ba7gij16cw1xplgnhl34v")))

(define-public crate-backup_lib-0.1.7 (c (n "backup_lib") (v "0.1.7") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0inkpffv1j4q1rp32c0wx8wsmhwbfg093js14wi3jyimhgwp46yx")))

(define-public crate-backup_lib-0.1.8 (c (n "backup_lib") (v "0.1.8") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00h8kip0xzhq1cbdsynjyyvrxclanhxwyrvkbb61cr9md8nlky04")))

(define-public crate-backup_lib-0.1.9 (c (n "backup_lib") (v "0.1.9") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13rfknzygafgqi5lsgfg6zkf6406pwx2cgrl19j0dl8xxqd49jax") (y #t)))

