(define-module (crates-io ba ck backplane) #:use-module (crates-io))

(define-public crate-backplane-0.0.1 (c (n "backplane") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1784i9fd7xbbqafxa5q2jqw7naiy09zyzxyw6xagacs6i35r88hb")))

(define-public crate-backplane-0.0.4 (c (n "backplane") (v "0.0.4") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07l1n4xhcgg76554dj63wj3rxzkhn5cwx8qd5146i0dbf97dac4j")))

