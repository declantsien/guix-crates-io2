(define-module (crates-io ba ck back-to-the-future) #:use-module (crates-io))

(define-public crate-back-to-the-future-0.1.0 (c (n "back-to-the-future") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0fak6f037qpbaxkw1kg0qrwbp0g1p8pq62vgmpdpjymymfzaxqan")))

(define-public crate-back-to-the-future-0.1.1 (c (n "back-to-the-future") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "11wyv5q2nl7z47imz1pl6546na7gkqp7zw7w9dcj1aps4pqr5hxa")))

(define-public crate-back-to-the-future-0.1.2 (c (n "back-to-the-future") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0v94h169z485v5gg3m1q5l45263va129k5lmifjxb9zkhp9vj5l5")))

(define-public crate-back-to-the-future-0.1.3 (c (n "back-to-the-future") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1j7kc51kxlm9im55zcjph6dbhhpfy5gl75pyj21xw22zrlsw7wg4")))

(define-public crate-back-to-the-future-0.1.4 (c (n "back-to-the-future") (v "0.1.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0favjrkff5knb18917lqgkd5pqp95irpj9y9lv2qw30z7wx1g8x5")))

(define-public crate-back-to-the-future-0.1.5 (c (n "back-to-the-future") (v "0.1.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0l6iwnr6i92mnw9k65rnfr932rsv7bbg6hspbznwgma7nh0w58nz")))

(define-public crate-back-to-the-future-0.1.6 (c (n "back-to-the-future") (v "0.1.6") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1gqf1h775nk8y6l955ps7di4rcwcmm8zlg1snqh3d4i6x1s3kj0g")))

(define-public crate-back-to-the-future-0.1.7 (c (n "back-to-the-future") (v "0.1.7") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "01v23182pi1nj1b4rcgi063wjp3zhsdrw1lwa7y90pgg8kk4a7xs")))

