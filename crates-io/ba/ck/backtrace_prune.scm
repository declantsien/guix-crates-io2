(define-module (crates-io ba ck backtrace_prune) #:use-module (crates-io))

(define-public crate-backtrace_prune-0.1.0 (c (n "backtrace_prune") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "indoc") (r "^2") (d #t) (k 2)))) (h "10z0dl7rqdlzdh1wjid7vi792m0k6abcs2y9s06hd7l2r1a7w3lm")))

