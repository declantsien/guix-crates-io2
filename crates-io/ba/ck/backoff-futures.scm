(define-module (crates-io ba ck backoff-futures) #:use-module (crates-io))

(define-public crate-backoff-futures-0.1.0 (c (n "backoff-futures") (v "0.1.0") (d (list (d (n "backoff") (r "^0.1") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 2)) (d (n "isahc") (r "^0.6.0") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "07plkln1imb2lyy72jf4pylkx51bsf46m74b40dv6d17p8dmxc6v") (y #t)))

(define-public crate-backoff-futures-0.1.1 (c (n "backoff-futures") (v "0.1.1") (d (list (d (n "backoff") (r "^0.1") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 2)) (d (n "isahc") (r "^0.6.0") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "0ngdcscfavmql6n3pq1f0qasd9p0hbibmk89xlgbgk14p6a7n8wy")))

(define-public crate-backoff-futures-0.2.0-alpha.1 (c (n "backoff-futures") (v "0.2.0-alpha.1") (d (list (d (n "backoff") (r "^0.1") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.18") (d #t) (k 2)) (d (n "isahc") (r "^0.7") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.3.0-alpha.5") (d #t) (k 0)))) (h "1k8npr3h8amg2ckr7hp7997cg1wa2lna212v2bq7wryx4fvz8kvb")))

(define-public crate-backoff-futures-0.2.0-alpha.2 (c (n "backoff-futures") (v "0.2.0-alpha.2") (d (list (d (n "backoff") (r "^0.1") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 2)) (d (n "isahc") (r "^0.7") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.3.0-alpha.6") (d #t) (k 0)))) (h "0lsnki8v9yackyg8kvypiydyyx3g1z3y4fs9apaixq3x0lj08vls")))

(define-public crate-backoff-futures-0.2.0 (c (n "backoff-futures") (v "0.2.0") (d (list (d (n "backoff") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "isahc") (r "^0.7") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.1") (f (quote ("time"))) (d #t) (k 0)))) (h "1jifajyyq224ph0djkhdwsg0gc9mk8xf5aii0ljm0q6n12qxk7j3")))

(define-public crate-backoff-futures-0.3.0 (c (n "backoff-futures") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "backoff") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "isahc") (r "^0.9") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.1") (f (quote ("time"))) (d #t) (k 0)))) (h "1s5ra9gcnzilxkc273gg1yq5fshya6id5l3lz8gqa1vp94bxfv07")))

(define-public crate-backoff-futures-0.3.1 (c (n "backoff-futures") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "backoff") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "isahc") (r "^0.9") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.1") (f (quote ("time"))) (d #t) (k 0)))) (h "15x7aw40gjcm98fsxvff78v70f1wgb6m2nz8mraga2aa9i9gj0lf") (y #t)))

(define-public crate-backoff-futures-0.3.2 (c (n "backoff-futures") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "backoff") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "isahc") (r "^0.9") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.1") (f (quote ("time"))) (d #t) (k 0)))) (h "0xyv3wmyjn25hg4npdamrda3hryc13wzfkfpjsgxqmhk412hxzgg")))

