(define-module (crates-io ba ck backroll_transport_steam) #:use-module (crates-io))

(define-public crate-backroll_transport_steam-0.1.0 (c (n "backroll_transport_steam") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "backroll_transport") (r "^0.1") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.5") (d #t) (k 0)) (d (n "steamworks") (r "^0.7.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1v15gvjnlpnxj4ws8bl39byfdwgnjix05w193zcy9zrk1z26pa84") (f (quote (("docs-only" "steamworks/docs-only") ("default"))))))

(define-public crate-backroll_transport_steam-0.2.0 (c (n "backroll_transport_steam") (v "0.2.0") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "backroll_transport") (r "^0.2") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.6") (d #t) (k 0)) (d (n "steamworks") (r "^0.8.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0rsib7jnq2ahzd594scqc4d1f0c3h1dphrxhd5sf1bpx0krpvah0") (f (quote (("docs-only" "steamworks/docs-only") ("default"))))))

(define-public crate-backroll_transport_steam-0.3.0 (c (n "backroll_transport_steam") (v "0.3.0") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "backroll_transport") (r "^0.2") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.7") (d #t) (k 0)) (d (n "steamworks") (r "^0.9.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1lcfa6yp8dilppgfnind8n1q15dnf26v8wm4drzfqz5q7z9mpflw")))

(define-public crate-backroll_transport_steam-0.4.0 (c (n "backroll_transport_steam") (v "0.4.0") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "backroll_transport") (r "^0.2") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.8") (d #t) (k 0)) (d (n "steamworks") (r "^0.9.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1bwwmi87drc3wbsm6975ibp2igps1la7l0z5i24cc540y9v76bbb")))

(define-public crate-backroll_transport_steam-0.5.0 (c (n "backroll_transport_steam") (v "0.5.0") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "backroll_transport") (r "^0.2") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.9") (d #t) (k 0)) (d (n "steamworks") (r "^0.9.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1qhfvds0y3r3qvvnc0scqqgq1b3hjx4r64ngmivd84bxh8mfpp76")))

