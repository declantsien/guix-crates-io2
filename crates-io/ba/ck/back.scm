(define-module (crates-io ba ck back) #:use-module (crates-io))

(define-public crate-back-0.1.0 (c (n "back") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "xdg") (r "^2.4.1") (d #t) (k 0)))) (h "04dfi45d6lnd6pjrjbg8bcn6m01y26j7k8277rqqdm1gl9izikya")))

(define-public crate-back-0.1.1 (c (n "back") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "xdg") (r "^2.4.1") (d #t) (k 0)))) (h "07fjnsdr0hqwflsxif8brzndvdxbqzalj9m42024iam4xyf9jgpv")))

