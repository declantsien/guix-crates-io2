(define-module (crates-io ba ck backgeard) #:use-module (crates-io))

(define-public crate-backgeard-0.1.0 (c (n "backgeard") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "macros" "process" "sync"))) (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "0ajrz2zs7995h33hawb6p3l8ri0vywwjrklddzlw8wsaqvlch2pq")))

