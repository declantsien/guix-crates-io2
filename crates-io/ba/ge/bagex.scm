(define-module (crates-io ba ge bagex) #:use-module (crates-io))

(define-public crate-bagex-0.1.0 (c (n "bagex") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0vl762i9207mjfw41icziv5i8lyaj8fdnbny4inhl9jd2pqmxsxq")))

(define-public crate-bagex-0.2.0 (c (n "bagex") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1r6spsk212a0jbwxk7gblv0pqmgbh33xbxg0fqbzsghfpih3xcfn")))

(define-public crate-bagex-0.2.1 (c (n "bagex") (v "0.2.1") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "11igbykrr5nra236d8pym6972irs5x3mysgy7bd4qwkg0bp21nq2")))

(define-public crate-bagex-0.2.2 (c (n "bagex") (v "0.2.2") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1rmh0kg56nnmdv67qmq8sxwcbw769zr5xvf2pf3xgs06q6sg8pfn")))

(define-public crate-bagex-0.2.3 (c (n "bagex") (v "0.2.3") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "021nalc1hgsnk965gll3fhdzd92szkdxc3nmj9vpn44id6m2g700")))

