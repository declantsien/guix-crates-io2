(define-module (crates-io ba ge bagel) #:use-module (crates-io))

(define-public crate-bagel-0.1.0 (c (n "bagel") (v "0.1.0") (d (list (d (n "dough") (r "^0.1.1") (d #t) (k 0)))) (h "03nblphyqlbczsy6ps5h9is377qdah08s1zh1hbqx8i5yfqa22f0")))

(define-public crate-bagel-0.1.1 (c (n "bagel") (v "0.1.1") (d (list (d (n "dough") (r "^0.1.1") (d #t) (k 0)))) (h "1azrnrhmnv0wnp9ax0j433n0cshbkmz78icnsla7v38xd1gcdw4k")))

(define-public crate-bagel-0.1.2 (c (n "bagel") (v "0.1.2") (d (list (d (n "dough") (r "^0.1.1") (d #t) (k 0)))) (h "0c4p22j7wrrrywl90mazmhjkv2py430zvygb8c3zq42w8f95ww51")))

(define-public crate-bagel-0.1.3 (c (n "bagel") (v "0.1.3") (d (list (d (n "dough") (r "^0.1.3") (d #t) (k 0)))) (h "17bwzx3sqrakdvjd6902vbz88v63f3bb95s1lqni3xq14xmh94vs")))

