(define-module (crates-io ba nd bandit) #:use-module (crates-io))

(define-public crate-bandit-0.9.0 (c (n "bandit") (v "0.9.0") (d (list (d (n "rand") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "1kn2y8zavlcvvc45yksvjds1pkv87xiqaibgzg67867pbpzp8dc4")))

(define-public crate-bandit-0.10.0 (c (n "bandit") (v "0.10.0") (d (list (d (n "rand") (r "^0.4.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "0pyh376l97py4sjhgh74rbdq68i64nmfrpdv2krv184r99k2alm9")))

(define-public crate-bandit-0.11.0 (c (n "bandit") (v "0.11.0") (d (list (d (n "rand") (r "^0.4.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "01d4jv96ghdar0vxqpasklywrb19c1rab3xlr78bxfr4gic25vh1")))

(define-public crate-bandit-0.11.1 (c (n "bandit") (v "0.11.1") (d (list (d (n "rand") (r "^0.4.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "0fyaig1m381w6xsk0j5kgmsf6kjfmbdaq56h4cjzlpqsnmvab7r5")))

(define-public crate-bandit-0.12.0 (c (n "bandit") (v "0.12.0") (d (list (d (n "rand") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)))) (h "0kysxq6pp6yz1vxhpggmygax7da1fivv4bzyn895dbriysxikh5c")))

(define-public crate-bandit-0.12.1 (c (n "bandit") (v "0.12.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "16wbak1zql19zql7h3rgp4psj7slbdssrddllpa52bjszzxcqw6k")))

(define-public crate-bandit-0.12.2 (c (n "bandit") (v "0.12.2") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "06cgihk5pdb267sff3x6985jv8z0dqlva2dljavppvwfjg11zlnx")))

(define-public crate-bandit-0.12.3 (c (n "bandit") (v "0.12.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0bl82w6lsrpsvinjd7ly6r2289z81y8cbbprdp67l3gm2k1ja2d7")))

(define-public crate-bandit-0.12.4 (c (n "bandit") (v "0.12.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0j228dhq43ah2b51bi45gln40i94ihf4hcsi5qzzxwjwfn57w8qn")))

