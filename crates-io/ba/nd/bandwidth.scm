(define-module (crates-io ba nd bandwidth) #:use-module (crates-io))

(define-public crate-bandwidth-0.1.0 (c (n "bandwidth") (v "0.1.0") (h "1w0scgcmwc7m1z00q0nqw7wsfy64bah76lg7isqqh6hjn07l9czm")))

(define-public crate-bandwidth-0.2.0 (c (n "bandwidth") (v "0.2.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "0hfmxy9f20j6h8avkb55f2gvf8gb64rxarw9w8l3cgls62z354h7")))

(define-public crate-bandwidth-0.2.1 (c (n "bandwidth") (v "0.2.1") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "11cnizkkfxw8f967cng2v6g83429kppnqj7dxvx7jmfj10hcqr2f")))

(define-public crate-bandwidth-0.3.0 (c (n "bandwidth") (v "0.3.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (k 0)))) (h "187n5vaamjv6md92h0j5hq39vhpqh3wzc29x9nj1ni4r9kalqiks") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std"))))))

