(define-module (crates-io ba nd bandsocks-cli) #:use-module (crates-io))

(define-public crate-bandsocks-cli-0.2.0 (c (n "bandsocks-cli") (v "0.2.0") (d (list (d (n "bandsocks") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "macros"))) (d #t) (k 0)))) (h "1a23c1yiij8mbj4yrxnq652507k9v113jaw4h63w5k1nzs7ckizv")))

(define-public crate-bandsocks-cli-0.2.1 (c (n "bandsocks-cli") (v "0.2.1") (d (list (d (n "bandsocks") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "macros"))) (d #t) (k 0)))) (h "1v9d3mazj1j81a00fiwsphp393vdy5a2rmfdazalns6a05a4bhzw")))

