(define-module (crates-io ba nd band_proc_macro) #:use-module (crates-io))

(define-public crate-band_proc_macro-0.1.0 (c (n "band_proc_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0har399845yynphxswgsnj5fi949b8iq7ib3s83343i28chq36qd")))

(define-public crate-band_proc_macro-0.1.1 (c (n "band_proc_macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "06b2qp750rf9fsfam3fba4a41980hnjr3r0z4zqhq4aai3zsjj91")))

(define-public crate-band_proc_macro-0.1.2 (c (n "band_proc_macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1fs4v3al6qc1g5q07027jq116vl0l4dixciwarkz3wnm1mc1q8qa")))

(define-public crate-band_proc_macro-0.1.3 (c (n "band_proc_macro") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0q51gq4xrddbkrf5ks1h401k5dr2j16g7ms1igirk1fajdr0g7lr")))

(define-public crate-band_proc_macro-0.1.4 (c (n "band_proc_macro") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "18gz7w7bj43cca14wsvc25b95g40qw25n7zz4y9gy7s9ss8aw6kx")))

(define-public crate-band_proc_macro-0.1.5 (c (n "band_proc_macro") (v "0.1.5") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0x21jk3rjvnv85zmpf2kay9xzpd776ccwxm8yhcx857il4pb3zsp")))

(define-public crate-band_proc_macro-0.1.6 (c (n "band_proc_macro") (v "0.1.6") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0r22pv16ihn1psq3pjblc82p870d10jxwvibisvwxsqg1wv74r9g")))

(define-public crate-band_proc_macro-0.1.7 (c (n "band_proc_macro") (v "0.1.7") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0iwkm2kw2d38fzmb6fs9n8c1g9ji1ikqmsjfpwsvac7zxq3bjrbv")))

(define-public crate-band_proc_macro-0.1.8 (c (n "band_proc_macro") (v "0.1.8") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0xsx34qa4y4ghwj8i476nya9kzydm0rancvm4ixi74j7n3jb6pnl")))

(define-public crate-band_proc_macro-0.1.9 (c (n "band_proc_macro") (v "0.1.9") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0dmdjv01mx69lq871q33fc7yx7kqsdr0vhhydjhaxagsnnsc8qla")))

