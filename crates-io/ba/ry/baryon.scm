(define-module (crates-io ba ry baryon) #:use-module (crates-io))

(define-public crate-baryon-0.1.0 (c (n "baryon") (v "0.1.0") (h "1x5pg50xb6nvl7gkqxibbmljm70haf48vmmp7pqc6i2dm2m5avg7") (y #t)))

(define-public crate-baryon-0.0.0 (c (n "baryon") (v "0.0.0") (h "0y5662nf7k6k3li2bbi6phlmzcbrjjx6awm2snhsc5sr767zmrc4")))

(define-public crate-baryon-0.2.0 (c (n "baryon") (v "0.2.0") (d (list (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "0lmkgv9g9iqnwj4q2rh8svrgzdpzjs37xzp919z8k88grafqh9lb")))

(define-public crate-baryon-0.3.0 (c (n "baryon") (v "0.3.0") (d (list (d (n "bc") (r "^0.1") (d #t) (k 0) (p "baryon-core")) (d (n "bytemuck") (r "^1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "glam") (r "^0.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)) (d (n "naga") (r "^0.6") (f (quote ("wgsl-in"))) (d #t) (k 2)) (d (n "pollster") (r "^0.2") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.10") (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 0)))) (h "171q4djs8kj1dw0s7anflb2b3hl5b8s1giddg98fl3c0w8vyxlrn")))

