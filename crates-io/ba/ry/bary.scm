(define-module (crates-io ba ry bary) #:use-module (crates-io))

(define-public crate-bary-0.1.0 (c (n "bary") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "bary-macros") (r "^0") (d #t) (k 0)) (d (n "bary-server") (r "^0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)))) (h "0lq3fc9rpvnwlmgjhcv1jsyp9qvq734kfzfqc70cidi565y8pr54")))

(define-public crate-bary-0.1.1 (c (n "bary") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bary-macros") (r "^0") (d #t) (k 0)) (d (n "bary-server") (r "^0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "14k579hy5rd1rca09xwckrhk3xxbjgdd8ix8mx3ixry1z8gcwkcg")))

