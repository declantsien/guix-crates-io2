(define-module (crates-io ba ry baryuxn) #:use-module (crates-io))

(define-public crate-baryuxn-0.1.0 (c (n "baryuxn") (v "0.1.0") (h "1fyl15y0mj3152ibbzq5h8x1qy0nbsgb43jjz0q9siwyk9iv7lp5")))

(define-public crate-baryuxn-0.1.1 (c (n "baryuxn") (v "0.1.1") (h "0k58p4m418y0vfylgkri7fkiy7wyc7v3mqfc96jmnwddzpr172qp")))

