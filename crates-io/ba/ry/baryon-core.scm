(define-module (crates-io ba ry baryon-core) #:use-module (crates-io))

(define-public crate-baryon-core-0.1.0 (c (n "baryon-core") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "glam") (r "^0.18") (f (quote ("mint"))) (d #t) (k 0)) (d (n "hecs") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.10") (d #t) (k 0)))) (h "1mkch3zyhdv63ah04ssyin3a36r2ya0sx1h836qh6v47x8iflnmj") (f (quote (("default"))))))

