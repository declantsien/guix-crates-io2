(define-module (crates-io ba ry baryon_math) #:use-module (crates-io))

(define-public crate-baryon_math-0.1.0 (c (n "baryon_math") (v "0.1.0") (h "13y07v367rbvxz9z8vyq65pdqzyb45qwmwzggh1qzxfxgk3qp960") (y #t)))

(define-public crate-baryon_math-0.0.0 (c (n "baryon_math") (v "0.0.0") (h "03da3nb46rfzs8m47yszhv0r52mm5ivm29x9ilp9f3y1ab4lfc61")))

