(define-module (crates-io ba ry bary-macros) #:use-module (crates-io))

(define-public crate-bary-macros-0.1.0 (c (n "bary-macros") (v "0.1.0") (d (list (d (n "bary-server") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tokenstream") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "15rg70dmzcyfg87d90wgs9kcinrh84gv1irfklljj4rb6nhd6bzb")))

(define-public crate-bary-macros-0.1.1 (c (n "bary-macros") (v "0.1.1") (d (list (d (n "bary-server") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tokenstream") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1yz7yl2ax7qlckp43b05whi06yx95216p56q7hb650p68w26lan7")))

