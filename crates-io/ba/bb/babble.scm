(define-module (crates-io ba bb babble) #:use-module (crates-io))

(define-public crate-babble-0.1.0 (c (n "babble") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "1dpj95r4x7vh0m31bwdzhp0zxbhlsn2cnjcz2pv4pr1836mw436w")))

(define-public crate-babble-0.2.0 (c (n "babble") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (k 0)) (d (n "rand_core") (r "^0.6.3") (f (quote ("getrandom"))) (k 2)))) (h "1k1n4qjggb30wz6p4q0hk5h8if0863sqsmsalrj20ccpxgq6slrq") (r "1.56")))

