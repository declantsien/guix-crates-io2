(define-module (crates-io ba bb babbage) #:use-module (crates-io))

(define-public crate-babbage-0.1.0 (c (n "babbage") (v "0.1.0") (d (list (d (n "arctk") (r "^0.11.5") (f (quote ("netcdf" "terminal_size"))) (d #t) (k 0)) (d (n "arctk-attr") (r "0.2.*") (d #t) (k 0)) (d (n "arctk-proc") (r "0.2.*") (d #t) (k 0)) (d (n "ndarray") (r "0.13.*") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "0.3.*") (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a5sy7s84gc68cg2n8bz93y2nwcdp754gn6ji5l711ab62czbjfs")))

(define-public crate-babbage-0.1.1 (c (n "babbage") (v "0.1.1") (d (list (d (n "arctk") (r "^0.11.5") (f (quote ("netcdf" "terminal_size"))) (d #t) (k 0)) (d (n "arctk-attr") (r "0.2.*") (d #t) (k 0)) (d (n "arctk-proc") (r "0.2.*") (d #t) (k 0)) (d (n "ndarray") (r "0.13.*") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "0.3.*") (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x09wlgrmqm2xpkk266all4h934jlg84ws4daxvzf1ygz3h6hasi")))

