(define-module (crates-io ba d6 bad64-sys) #:use-module (crates-io))

(define-public crate-bad64-sys-0.1.0 (c (n "bad64-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "0qiwy74zfnn4kz069f467ahn1j8bpzy9grr2y7irrsl6mz31jm0z")))

(define-public crate-bad64-sys-0.1.1 (c (n "bad64-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.56") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1qxwkljzvp9av29a96b3w939707j0cv4zsbv9d8xgk7r9fr3lr14")))

(define-public crate-bad64-sys-0.1.2 (c (n "bad64-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.56") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1g5lygs3vv6sx9n3rxgd3caw71n9z22m6glyrd53kzirjs6frrbw")))

(define-public crate-bad64-sys-0.1.3 (c (n "bad64-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.56") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1zaqn1hrwi5cas2jx4q7wik8zqxfybdhvjhfbywhgwpxcdbsmjsp")))

(define-public crate-bad64-sys-0.1.4 (c (n "bad64-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.57") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1qh2x54fpn9g90dh2v78fmd8w8v79gbhhys0h594zn7zdkzzy75w")))

(define-public crate-bad64-sys-0.1.5 (c (n "bad64-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.57") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1v4zva8qyfxibka63j21b2fwwqpj78zdm9i2jz8vi47xfxpyrmic")))

(define-public crate-bad64-sys-0.2.0 (c (n "bad64-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1mvmwwspfbkcyndl1cnzxyisipsm31zk0nw5dn9pxq72gm6vzrdf")))

(define-public crate-bad64-sys-0.2.1 (c (n "bad64-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.57") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1x35970cm2ryy7f0wy66ibciw6v3p9k21qb9vcdm5hd3pbmw1nzv")))

(define-public crate-bad64-sys-0.3.0 (c (n "bad64-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "0dvc988hlnc04mjyngcv02zp933s6pgwr5s595324qn8jzq8fjsx")))

(define-public crate-bad64-sys-0.3.1 (c (n "bad64-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1bcs80q40l945957f5n9wdf26hc6infbla893dlcqkaz4ibzdgb2")))

(define-public crate-bad64-sys-0.4.0 (c (n "bad64-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "13w4pmzkjh0q2ymdanh23szdjpfcqq3kbpkfn51a7wphn1l51705")))

(define-public crate-bad64-sys-0.5.0 (c (n "bad64-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1sjafykig629zrj7yidvarwyy66mxc5xqypkrdwnfmx11hvjkvlm")))

(define-public crate-bad64-sys-0.6.0 (c (n "bad64-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1haafl94b24is8bxa217kgwhfmg8scwpqg9cmigq3d9wyqpyq8dr")))

(define-public crate-bad64-sys-0.7.0 (c (n "bad64-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1ivqzqk15wm9hchkawph3yqr41mxkda20w7k3gb6h47nlf68apan")))

