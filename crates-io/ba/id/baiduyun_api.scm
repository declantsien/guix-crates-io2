(define-module (crates-io ba id baiduyun_api) #:use-module (crates-io))

(define-public crate-baiduyun_api-0.1.0 (c (n "baiduyun_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1cgkvy7w8a15mby6ybf9avqzzlnqcr1liafy454d6m8pwy57aky6") (y #t)))

(define-public crate-baiduyun_api-0.1.1 (c (n "baiduyun_api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "176hvjh3q99nrbvgv3gpdmp2yga8dq472cq1a062c8p4qf41r5m7") (y #t)))

(define-public crate-baiduyun_api-0.2.0 (c (n "baiduyun_api") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1bamx8gp4ks9wvqv78z9ncscvmy2523b9sfmlpa84mlimq93jafw") (y #t)))

(define-public crate-baiduyun_api-0.2.1 (c (n "baiduyun_api") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1vilpb5iz2868pzf772gpk55lx1y5d9dqdrp8hrqip57gqnwp38v") (y #t)))

(define-public crate-baiduyun_api-0.2.2 (c (n "baiduyun_api") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0qc0p281h6rzbmzbb9lq9d9cjl05k8g0yi2zp9gyjjkhpf448gj8") (y #t)))

(define-public crate-baiduyun_api-0.2.3 (c (n "baiduyun_api") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0q6sygmqibslsxp6azl2finnmalvn6h4wj5q3vpd7by3zdxqzbr2") (y #t)))

(define-public crate-baiduyun_api-0.2.4 (c (n "baiduyun_api") (v "0.2.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1inkss88ijhz7nb42wj8dc5d4a2p4qnigxwpgs37z0q9mgg1fnm9") (y #t)))

(define-public crate-baiduyun_api-0.2.5 (c (n "baiduyun_api") (v "0.2.5") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "08vra456bpdz98jd9azjgb0lzq6scvhygd6zdivmyakaqm4mwifh")))

