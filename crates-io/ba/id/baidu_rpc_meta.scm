(define-module (crates-io ba id baidu_rpc_meta) #:use-module (crates-io))

(define-public crate-baidu_rpc_meta-0.1.0 (c (n "baidu_rpc_meta") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pilota") (r "^0.9") (d #t) (k 0)) (d (n "volo") (r "^0.8") (d #t) (k 0)) (d (n "volo-build") (r "^0.8") (d #t) (k 1)) (d (n "volo-grpc") (r "^0.8") (d #t) (k 0)))) (h "0rw6175bvx9hv71xi95qhkrf63xkpaqbs2r4q5ywmsz8pwy24s5v")))

