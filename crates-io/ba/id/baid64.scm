(define-module (crates-io ba id baid64) #:use-module (crates-io))

(define-public crate-baid64-0.1.0 (c (n "baid64") (v "0.1.0") (d (list (d (n "amplify") (r "^4.6.0") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1ipc7gllr0rgcn9cmmsfs6gzazfrl6j6gcjnz12qh11m894q12vv") (r "1.66.0")))

(define-public crate-baid64-0.2.0 (c (n "baid64") (v "0.2.0") (d (list (d (n "amplify") (r "^4.6.0") (d #t) (k 0)) (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "mnemonic") (r "^1.1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0s46x5fvlzbwn9f8dq8njdhh0mxkphgm8wv924shlpan2djncpnx") (r "1.66.0")))

(define-public crate-baid64-0.2.1 (c (n "baid64") (v "0.2.1") (d (list (d (n "amplify") (r "^4.6.0") (d #t) (k 0)) (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "mnemonic") (r "^1.1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0lbdg7y94x30cgvigicri8qxvg0bm4zcgrspyc86q38x2smxwcd6") (r "1.66.0")))

(define-public crate-baid64-0.2.2 (c (n "baid64") (v "0.2.2") (d (list (d (n "amplify") (r "^4.6.0") (d #t) (k 0)) (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "mnemonic") (r "^1.1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "151pcbbb9znlfng4z4whi2hq9wq1lxl9hqv854w2q7p0b4kvrnlm") (r "1.66.0")))

