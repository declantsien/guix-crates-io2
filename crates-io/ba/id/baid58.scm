(define-module (crates-io ba id baid58) #:use-module (crates-io))

(define-public crate-baid58-0.1.0 (c (n "baid58") (v "0.1.0") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)))) (h "1x41wfsk5bqqhbxbjkvksjzbpd9y9gxjp4pqg15wgina2259znqn") (r "1.60.0")))

(define-public crate-baid58-0.2.0 (c (n "baid58") (v "0.2.0") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)))) (h "11x451dvmilfi3a38bik8kw1spds5zc9i6zqv7bcwv2bshmij19a") (r "1.60.0")))

(define-public crate-baid58-0.3.0 (c (n "baid58") (v "0.3.0") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)))) (h "17sdijgxzyc9zbfl1j7fgf032a8q802ycdw2d83s2i4076l5qrzy") (r "1.60.0")))

(define-public crate-baid58-0.3.1 (c (n "baid58") (v "0.3.1") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)))) (h "1pll5004w7wjam78hdh5173gr8jwmi1j3zhy3jv5mdc4nbv3ikjx") (r "1.60.0")))

(define-public crate-baid58-0.3.2 (c (n "baid58") (v "0.3.2") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)))) (h "0jxd4nlb9jhdbmdcv4cx5pbaqcakxcfpqh4n6db45dl3m9n8bsr9") (r "1.60.0")))

(define-public crate-baid58-0.4.0 (c (n "baid58") (v "0.4.0") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "019y1bx9h680ksj2d8zn2b5y3pmibnzbhdqnvw64vi1x10glis8m") (y #t) (r "1.60.0")))

(define-public crate-baid58-0.4.1 (c (n "baid58") (v "0.4.1") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "blake3") (r "^1.4.1") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "1bp4zkfwbhzcvmdfkdkq7j3rgcjg60vp4sphi0nbc0ma1k668805") (r "1.66.0")))

(define-public crate-baid58-0.4.2 (c (n "baid58") (v "0.4.2") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "blake3") (r "^1.4.1") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "1byn8yx9wizhmg9ck8j9nwa0rah9b75hrjm5qjqiigp63h0dvfjm") (r "1.66.0")))

(define-public crate-baid58-0.4.3 (c (n "baid58") (v "0.4.3") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "blake3") (r "^1.4.1") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "0rd76i4rhhwhw5mvsiq4rr9r3y9k7rwxl19a5kdlzwr5kfy38a5f") (r "1.66.0")))

(define-public crate-baid58-0.4.4 (c (n "baid58") (v "0.4.4") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "blake3") (r "^1.4.1") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "11kl6nrbjhry9hdwkdm40h0i87vp1xmfhsnv0mp9gvc75lj8a1dw") (r "1.66.0")))

