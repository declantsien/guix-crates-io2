(define-module (crates-io ba am baam) #:use-module (crates-io))

(define-public crate-baam-0.0.1 (c (n "baam") (v "0.0.1") (h "1k8cgvhhp5g9l5vj13k19gs7jbqrlj8ly28swnlps2wafv0xgw5k") (y #t)))

(define-public crate-baam-0.0.1-alpha (c (n "baam") (v "0.0.1-alpha") (h "1wxzrdsq5ja49rhw4v2v4dzbsg8iill17sybbw3m1810ngzzkjw6") (y #t)))

(define-public crate-baam-0.0.1-alphav1 (c (n "baam") (v "0.0.1-alphav1") (h "0sscc4q664qgdnwhbnzafa9r7c93gyrwrjq26f9y7s56g2zc5dh1") (y #t)))

(define-public crate-baam-0.0.1-alpha-v2 (c (n "baam") (v "0.0.1-alpha-v2") (h "0l4dgbgxqrp57ni72jv1yjg3pq0vg5b84fmyfpqgnik7s2vm6pll") (y #t)))

(define-public crate-baam-0.0.1-alpha-v3 (c (n "baam") (v "0.0.1-alpha-v3") (h "0ngc2pkidsdc9sjjc1hg4n2aq54i0wszhwvgnfv35a6a4q1nv138")))

