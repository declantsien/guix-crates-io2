(define-module (crates-io ba th bath-signal) #:use-module (crates-io))

(define-public crate-bath-signal-0.1.0 (c (n "bath-signal") (v "0.1.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (d #t) (k 2)) (d (n "tower-http") (r "^0.4") (f (quote ("cors"))) (d #t) (k 2)))) (h "048gg6i4yh6lmgy7ysnk4sq3dbi8kw0awwx2lds17gwnr9a7x8ky")))

(define-public crate-bath-signal-0.1.1 (c (n "bath-signal") (v "0.1.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (d #t) (k 2)) (d (n "tower-http") (r "^0.4") (f (quote ("cors"))) (d #t) (k 2)))) (h "14h9f76hg7zdgkffv8jajrzp366cmd4q5jv7lhyj7wixr63ikdzg")))

