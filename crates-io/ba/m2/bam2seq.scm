(define-module (crates-io ba m2 bam2seq) #:use-module (crates-io))

(define-public crate-bam2seq-0.1.0 (c (n "bam2seq") (v "0.1.0") (d (list (d (n "bam") (r "0.1.*") (d #t) (k 0)) (d (n "clap") (r "3.*") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)))) (h "139nyjbv6hcjnv7323vkvmz72naswh6rlk83zp8cnls0jj0mcvvy")))

(define-public crate-bam2seq-0.1.1 (c (n "bam2seq") (v "0.1.1") (d (list (d (n "bam") (r "0.1.*") (d #t) (k 0)) (d (n "clap") (r "3.*") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)))) (h "1ci8dr1zlv734y1kkakddf7rqmy56m5isbl54ch4id7gy63vjx61")))

(define-public crate-bam2seq-0.1.2 (c (n "bam2seq") (v "0.1.2") (d (list (d (n "bam") (r "0.1.*") (d #t) (k 0)) (d (n "clap") (r "3.*") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)))) (h "0dbs8bm92rrnbxh7z96i6dsgp579wpdx6sh5r0shnc7kdrs11gj3")))

