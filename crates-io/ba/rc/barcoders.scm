(define-module (crates-io ba rc barcoders) #:use-module (crates-io))

(define-public crate-barcoders-0.3.0 (c (n "barcoders") (v "0.3.0") (d (list (d (n "image") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "0i1w0ir4c7hhcq512rijlq4vq8py5yysavkqm0kpmsgndgy9jjzm") (f (quote (("ascii"))))))

(define-public crate-barcoders-0.3.1 (c (n "barcoders") (v "0.3.1") (d (list (d (n "clippy") (r "^0.0.24") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "01jmwd58dixpf0pfjmn5rmyrjdjvjga9r8dfcydbb61ljz355ifb") (f (quote (("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.3.2 (c (n "barcoders") (v "0.3.2") (d (list (d (n "clippy") (r "^0.0.24") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.5.3") (o #t) (d #t) (k 0)))) (h "15pwlg5ybrb46vw39n1lab2mg9fyld4d5jjxv7kl2jlwbmsmlr7z") (f (quote (("svg") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.3.3 (c (n "barcoders") (v "0.3.3") (d (list (d (n "clippy") (r "^0.0.24") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.5.3") (o #t) (d #t) (k 0)))) (h "1r59a7z8dkdzsh4x9n5qadvr617ppdjh286cdffbhqmqjih3j3b2") (f (quote (("svg") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.3.4 (c (n "barcoders") (v "0.3.4") (d (list (d (n "clippy") (r "^0.0.24") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.5.3") (o #t) (d #t) (k 0)))) (h "0c1bd12cbayypw86j9sqcl49966mpw0mvwj4pvqgd00h86ykkxxz") (f (quote (("svg") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.3.5 (c (n "barcoders") (v "0.3.5") (d (list (d (n "clippy") (r "^0.0.24") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.5.3") (o #t) (d #t) (k 0)))) (h "1d96dk2r2q90brwwmkb1w5gv19s51ivjcv8cknqxkb783xbawfsb") (f (quote (("svg") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.3.6 (c (n "barcoders") (v "0.3.6") (d (list (d (n "clippy") (r "^0.0.35") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "1ky8c484c2rr253mdrgk7x8557vyx3mbp5sxn154bb53m37w8yhm") (f (quote (("svg") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.4.0 (c (n "barcoders") (v "0.4.0") (d (list (d (n "clippy") (r "^0.0.35") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0fabycwrc74zz79xap4km2ixnyhchqxv8rx7i0gxdwys2gjwpj2c") (f (quote (("svg") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.5.0 (c (n "barcoders") (v "0.5.0") (d (list (d (n "clippy") (r "^0.0.35") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0rb6rd8g693b752hx6mxi10bvgy3200vqw20hh7iy36hnhs6pgl7") (f (quote (("svg") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.5.1 (c (n "barcoders") (v "0.5.1") (d (list (d (n "clippy") (r "^0.0.83") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "0fcdv4j1iv9mx0brir9scpn0ik2gvg9bq5rka6s2qh7l99vzg6n4") (f (quote (("svg") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.6.0 (c (n "barcoders") (v "0.6.0") (d (list (d (n "clippy") (r "^0.0.83") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "1i7rlj97gbxmr1fpsy1dv8m8pvnnsb40ihg2jpb3p19hcdj88a4i") (f (quote (("svg") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.7.0 (c (n "barcoders") (v "0.7.0") (d (list (d (n "clippy") (r "^0.0.83") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "1kwcp5lhrq6jkwi8prn5ii1ky5267m4crz6vyw7k77cjdsvwqjl9") (f (quote (("svg") ("json") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.8.0 (c (n "barcoders") (v "0.8.0") (d (list (d (n "clippy") (r "^0.0.134") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "0ikwavv5c9z7mjlkv6zgbpy6h0avm12wkzk7b28m37s1ss8ax34l") (f (quote (("svg") ("json") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.8.1 (c (n "barcoders") (v "0.8.1") (d (list (d (n "clippy") (r "^0.0.134") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1cd80x0gdj7k56y517zvc7qbarvmz0di7jzf7c8cp3q6bv054zhz") (f (quote (("svg") ("json") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.8.2 (c (n "barcoders") (v "0.8.2") (d (list (d (n "clippy") (r "^0.0.134") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.15.0") (o #t) (d #t) (k 0)))) (h "032vbmsq1s3nragcv9wf0wrld8mwxkv1gxma4cgchrx8j50x7bkd") (f (quote (("svg") ("json") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.9.0 (c (n "barcoders") (v "0.9.0") (d (list (d (n "clippy") (r "^0.0.166") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "19j61hk3shaclcn532flvhy1j91fa5kwm04nwn2ccsik4q3z7kij") (f (quote (("svg") ("json") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-0.10.0 (c (n "barcoders") (v "0.10.0") (d (list (d (n "clippy") (r "^0.0.166") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "0917b30g1c839bz9fxavrw5b20pmizbpa7mfzy5x9vcqrv0d6ikb") (f (quote (("svg") ("json") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-1.0.0 (c (n "barcoders") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0.166") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22") (o #t) (d #t) (k 0)))) (h "1r9l0pyag42zzflgbgy9rnxfwhlk6jjn2ys2k0njzbw922hxmpxn") (f (quote (("svg") ("json") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-1.0.1 (c (n "barcoders") (v "1.0.1") (d (list (d (n "clippy") (r "^0.0.166") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22") (o #t) (d #t) (k 0)))) (h "0dii4rm4xc9g3mhfb1hsjnrvldl5xvmk3rczzfnpm9n2b2hp331r") (f (quote (("svg") ("json") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-1.0.2 (c (n "barcoders") (v "1.0.2") (d (list (d (n "clippy") (r "^0.0.166") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22") (o #t) (d #t) (k 0)))) (h "1dwadq64y338g0112138v9dd9x69qx97cxk2pand238ym6mjd0r9") (f (quote (("svg") ("json") ("dev" "clippy") ("ascii"))))))

(define-public crate-barcoders-2.0.0 (c (n "barcoders") (v "2.0.0") (d (list (d (n "image") (r "^0.25") (o #t) (d #t) (k 0)))) (h "13qy1asw313yhmlp80i161wicbckyh5a9jdq1l62riwfx6v6z0m3") (f (quote (("svg") ("std") ("json") ("default" "ascii" "json" "svg" "std") ("ascii")))) (s 2) (e (quote (("image" "dep:image"))))))

