(define-module (crates-io ba rc barcode-rs) #:use-module (crates-io))

(define-public crate-barcode-rs-0.1.0 (c (n "barcode-rs") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "1cg3wk03872an34lb9gdkzwyzhll7wvnd3jzaqfs915silfii1hx")))

