(define-module (crates-io ba rc barcode-scanner) #:use-module (crates-io))

(define-public crate-barcode-scanner-0.1.0 (c (n "barcode-scanner") (v "0.1.0") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("rt" "sync"))) (o #t) (d #t) (k 0)))) (h "14z9q6d9sc27lv4iqab3xlj58fgibi3md8z31yfnxyfxb54rhqxv") (s 2) (e (quote (("tokio" "dep:tokio"))))))

