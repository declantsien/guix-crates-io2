(define-module (crates-io ba rc barcode_command) #:use-module (crates-io))

(define-public crate-barcode_command-0.1.0 (c (n "barcode_command") (v "0.1.0") (h "0p8ibhk6d17saw9nva0pskm19cp7dr5xhmhzfclj5c24cqkwqjp0")))

(define-public crate-barcode_command-0.1.1 (c (n "barcode_command") (v "0.1.1") (h "1y7ycz7nyl80khiqi93y09vh1jhnyh0cixmc8azkjp7qk9p0dd9w")))

