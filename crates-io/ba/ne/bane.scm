(define-module (crates-io ba ne bane) #:use-module (crates-io))

(define-public crate-bane-0.0.1 (c (n "bane") (v "0.0.1") (d (list (d (n "parity-wasm") (r "^0.31") (d #t) (k 0)) (d (n "wasmi") (r "^0.4") (d #t) (k 0)))) (h "1jp6hsw4lvvy87aff7h6gw4ss07z6kx4r32df7gyc1csvynsnj3q")))

(define-public crate-bane-0.0.2 (c (n "bane") (v "0.0.2") (d (list (d (n "parity-wasm") (r "^0.31") (d #t) (k 0)) (d (n "wasmi") (r "^0.4") (d #t) (k 0)))) (h "1h13i8dp9d6k96m19hnkqhccd9v9slda9mskfp9shy1d69v7y6wc")))

