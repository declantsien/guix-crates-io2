(define-module (crates-io ba bi babilado-types) #:use-module (crates-io))

(define-public crate-babilado-types-0.1.0 (c (n "babilado-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01wq7ljlxilr7n7m2clq5jr96bhd7ng8ln19h4dmq9syp5l6l8vv")))

(define-public crate-babilado-types-0.2.0 (c (n "babilado-types") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pld81ikp8w1rsnad3wpj3r97imbkmwv34fyl5fvydl6l6nfic8n")))

(define-public crate-babilado-types-0.2.1 (c (n "babilado-types") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "062vlqzjfjfp78xa7jnm3pgi867akia7337d6cb8fbkgn8zm0q0d")))

(define-public crate-babilado-types-0.2.2 (c (n "babilado-types") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gw5vjr59g0qkbk6zdh5s1894g3042a0qh5ahd85plcf6ga59xj9")))

(define-public crate-babilado-types-0.2.3 (c (n "babilado-types") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fjnv3sic55y72c2g25d599706261ybbpdxxv8bvzzyxbx55b8dv")))

(define-public crate-babilado-types-0.3.0 (c (n "babilado-types") (v "0.3.0") (d (list (d (n "oorandom") (r "^11.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05ilcg9864i0yjdg9sw2ihz3d7biwbgm8mw88hwsllqsb1anb7mp")))

(define-public crate-babilado-types-0.4.0 (c (n "babilado-types") (v "0.4.0") (d (list (d (n "oorandom") (r "^11.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "031vsgxaki7v0mvcda1zfn7y8djczjlry4kzf70g0cn95p0dp5k5")))

