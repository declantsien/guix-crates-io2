(define-module (crates-io ba dg badgers) #:use-module (crates-io))

(define-public crate-badgers-1.0.0 (c (n "badgers") (v "1.0.0") (d (list (d (n "ab_glyph") (r "^0.2.16") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1crgaxca3xkx0qjg746wgpl0amf8vd013k3xl8ji896drf85yc96")))

(define-public crate-badgers-1.1.0 (c (n "badgers") (v "1.1.0") (d (list (d (n "ab_glyph") (r "^0.2.16") (d #t) (k 0)) (d (n "base64") (r "^0.20") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1xjhb1iknwlxs0kj4i26yd60j7jcb5rai6nvl250sbxfw84mfzlx")))

(define-public crate-badgers-1.2.0 (c (n "badgers") (v "1.2.0") (d (list (d (n "ab_glyph") (r "^0.2.16") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0hclfbi7jbzh3lggnzbclq9kvzazvkkyxfqryzq846xd2akw2m3r")))

