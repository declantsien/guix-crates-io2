(define-module (crates-io ba dg badger) #:use-module (crates-io))

(define-public crate-badger-0.1.0 (c (n "badger") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "itoa") (r "^0.1.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "17q8g8ijs5sq13p3s0z0czxpgzgga05wyazzry42pmcjlkjw0azz")))

(define-public crate-badger-0.2.0 (c (n "badger") (v "0.2.0") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "itoa") (r "^0.1.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0iiq8dwrq6k8rh68zk2jlsah8gh0pkn2aq03rs09lhngg6gh4vl9")))

(define-public crate-badger-0.2.1 (c (n "badger") (v "0.2.1") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "itoa") (r "^0.1.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "176y1a5fd1pcl1sanfk1j380kbgdwi5b1qswp1vhivclln8nijnm")))

