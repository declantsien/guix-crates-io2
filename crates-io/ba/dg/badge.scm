(define-module (crates-io ba dg badge) #:use-module (crates-io))

(define-public crate-badge-0.1.0 (c (n "badge") (v "0.1.0") (d (list (d (n "rusttype") (r "^0.2.0") (d #t) (k 0)))) (h "03y1li55wmb2d4b8ziilgj32fk33lqp87qrm9frdpmsizc882ai5")))

(define-public crate-badge-0.2.0 (c (n "badge") (v "0.2.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.4.0") (d #t) (k 0)))) (h "014np5njm4alz5xsavj2wky8hpx500rs6zq2d87vjdklcbza5f46")))

(define-public crate-badge-0.3.0 (c (n "badge") (v "0.3.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)))) (h "0za79szrnmjg2rgdmmz9503dlfx69mbw66bcx0hjkrw9bgk8l8nh")))

