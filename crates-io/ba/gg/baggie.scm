(define-module (crates-io ba gg baggie) #:use-module (crates-io))

(define-public crate-baggie-0.1.0 (c (n "baggie") (v "0.1.0") (h "1bh4ml3m2bhffbzy07gabkywr62q3mbqy67n00h2k8xqx2wmp68q")))

(define-public crate-baggie-0.2.0 (c (n "baggie") (v "0.2.0") (h "1blnpa6r5fcf43j9ajnzgffy6l0aa57iapflm3ad3giidnasri51")))

(define-public crate-baggie-0.2.1 (c (n "baggie") (v "0.2.1") (h "1ii051h4k7nbi51585d66ybgw28ndpsmqyc15fb5sbwj839xka5h")))

