(define-module (crates-io ba gg baggins) #:use-module (crates-io))

(define-public crate-baggins-0.1.4 (c (n "baggins") (v "0.1.4") (d (list (d (n "actix-web-lab") (r "^0.20.1") (d #t) (k 0)) (d (n "bigdecimal") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smartcore") (r "^0.2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1jyhsbcblwf3f536kw4s3srb26g774lkdl1ryb4bxwfjjvyr3q3w") (y #t)))

(define-public crate-baggins-0.1.5 (c (n "baggins") (v "0.1.5") (d (list (d (n "bigdecimal") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0322k0i0ix0s2mbirw174jblpmjm186hkksc2xkj7cmn9iq0wj83")))

(define-public crate-baggins-0.1.6 (c (n "baggins") (v "0.1.6") (d (list (d (n "bigdecimal") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mbbjqs0vxcpin6br17zkwc5a0si72axphx8ryp5qnicwsrzv144")))

(define-public crate-baggins-0.1.7 (c (n "baggins") (v "0.1.7") (d (list (d (n "bigdecimal") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qsm5x98s1sryzsfai7h7ks13capf06v94h6fngci474jr3mrw7b")))

(define-public crate-baggins-0.1.8 (c (n "baggins") (v "0.1.8") (d (list (d (n "bigdecimal") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14cwzjxj4df8whgpajn88jl7pc77imwr45lkia7370kgx8y5p5c9")))

(define-public crate-baggins-0.1.10 (c (n "baggins") (v "0.1.10") (d (list (d (n "bigdecimal") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xq0n6ixh6iqg55g12r1x4qh3wah1b8gz2wqkfknap6iwg6zx85c")))

(define-public crate-baggins-0.2.0 (c (n "baggins") (v "0.2.0") (d (list (d (n "bigdecimal") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yp3kfshca30qz85vbhly762fgdrvrja955vwl4clwcypfjpznvm")))

