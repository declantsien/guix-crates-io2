(define-module (crates-io ba ud baudot) #:use-module (crates-io))

(define-public crate-baudot-0.1.0 (c (n "baudot") (v "0.1.0") (h "0d8774xkbgfqavppjd61m4dygwljcc3aj86c0w0bsn90j4ikg57j")))

(define-public crate-baudot-0.0.0 (c (n "baudot") (v "0.0.0") (h "0gm4hfpnk7nzc9vqbn9bcs28mckds558adpn8j0pl1dz70j103mr")))

(define-public crate-baudot-0.1.1 (c (n "baudot") (v "0.1.1") (h "1hrcq1rkz7bv7apjc0iy535ganh4pnvg66w8j2h9bp03yz4g1xg1")))

