(define-module (crates-io ba sk baskerville) #:use-module (crates-io))

(define-public crate-baskerville-0.1.0 (c (n "baskerville") (v "0.1.0") (d (list (d (n "baskerville_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.27") (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (o #t) (d #t) (k 0)))) (h "0hrslz20izhhis9yhli7cy75hir2mizy7s3lbvxbs169rw06xr03") (f (quote (("default" "time")))) (s 2) (e (quote (("time" "dep:chrono") ("python" "dep:pyo3"))))))

(define-public crate-baskerville-0.2.0 (c (n "baskerville") (v "0.2.0") (d (list (d (n "baskerville_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.27") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (o #t) (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)))) (h "10w46w80gjmbayz6wfhdnx2lkcjhz9rwdfi20acpxwhis55m5sil") (f (quote (("default" "time")))) (s 2) (e (quote (("time" "dep:chrono") ("python" "dep:pyo3"))))))

(define-public crate-baskerville-0.3.0 (c (n "baskerville") (v "0.3.0") (d (list (d (n "baskerville_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.27") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (o #t) (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)))) (h "106svb6n2m07mb9jq2935i3i4bkln0l26dqa2qw303zidgbpsczk") (f (quote (("default" "time")))) (s 2) (e (quote (("time" "dep:chrono") ("python" "dep:pyo3"))))))

