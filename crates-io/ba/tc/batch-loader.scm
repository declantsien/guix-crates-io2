(define-module (crates-io ba tc batch-loader) #:use-module (crates-io))

(define-public crate-batch-loader-0.1.0 (c (n "batch-loader") (v "0.1.0") (d (list (d (n "batch-recv") (r "^0.1") (f (quote ("crossbeam"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "worker-sentinel") (r "^0.3") (d #t) (k 0)))) (h "1vgsm4sjm2l2biphjccxnvsdppxjwws3rs5r1hv2hygc1z6iav39")))

