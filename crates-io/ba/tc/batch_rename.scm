(define-module (crates-io ba tc batch_rename) #:use-module (crates-io))

(define-public crate-batch_rename-0.1.0 (c (n "batch_rename") (v "0.1.0") (h "1grd6dd7hd3qm601hagg4wdccdcyancgrq3wwyr0i5c5ir4p25ra") (y #t)))

(define-public crate-batch_rename-0.1.1 (c (n "batch_rename") (v "0.1.1") (h "0qkss8vw08q94pnp11xm9gj76in62rq05rxi4jlvyani1921hqzl") (y #t)))

(define-public crate-batch_rename-0.1.2 (c (n "batch_rename") (v "0.1.2") (h "0wbd9ag3h3lra0ip1sdqgamnnm7qcv54sa9jw2yq2z2xwvxrscd3")))

(define-public crate-batch_rename-0.1.3 (c (n "batch_rename") (v "0.1.3") (h "0s62nmqx5ylgfxjzwk1mah5vp5xbnr1jnaqi2s0qam9xjaag44r9")))

(define-public crate-batch_rename-0.1.4 (c (n "batch_rename") (v "0.1.4") (h "063sw316zw5k68n0f092h8mi0drrayvmg669k3a9pn12g0rhg4bx")))

(define-public crate-batch_rename-0.1.5 (c (n "batch_rename") (v "0.1.5") (h "0z7s3wxrqb8w90gs4z4ssa7d57hc5b2r1mrqqd19srq633277q31")))

