(define-module (crates-io ba tc batch3) #:use-module (crates-io))

(define-public crate-batch3-0.1.0 (c (n "batch3") (v "0.1.0") (h "079n9xwys2zqlvlsx151yxcl6frkg158cb5wm1708z7bs7k2dnl3")))

(define-public crate-batch3-0.1.1 (c (n "batch3") (v "0.1.1") (h "1na4ynw40rf8khvff44h0d9w5h5hgpxa928vgblnjnfc5sxvba5b")))

(define-public crate-batch3-0.1.2 (c (n "batch3") (v "0.1.2") (h "19zzzhhr9axnjzs6jx05vq4s6yic5a69m4mv24rnwzgz1mayg1dh")))

