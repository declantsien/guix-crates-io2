(define-module (crates-io ba tc batcher) #:use-module (crates-io))

(define-public crate-batcher-0.1.0 (c (n "batcher") (v "0.1.0") (h "06shwjf4cdb3ndywslb3kxqavkgyp1ai9phdyrcmdsvmh14039sb") (y #t)))

(define-public crate-batcher-0.1.1 (c (n "batcher") (v "0.1.1") (h "1w6qddkqrbibdy11x4i5gc9jd6rn239kjmdj5y8q5g8f7fsadmqy")))

(define-public crate-batcher-0.1.2 (c (n "batcher") (v "0.1.2") (h "0c4ff3xhpqyna5hvk044mah9gi7siz63h7b3bp9zyfrr2a5w0i6v")))

