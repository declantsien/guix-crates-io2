(define-module (crates-io ba tc batched-fn) #:use-module (crates-io))

(define-public crate-batched-fn-0.1.0 (c (n "batched-fn") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "time" "rt-core"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "time" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0z88xlw0zpjkfy2mkv7967vmcx1pqihs7izlc3nzls9xq7pv2lfj")))

(define-public crate-batched-fn-0.1.1 (c (n "batched-fn") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "time" "rt-core"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "time" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0bj5k1n8n7dz7qvihhwj7y5ppl5mizvgmm64rryzxs2m594f4b1a")))

(define-public crate-batched-fn-0.1.2 (c (n "batched-fn") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "time" "rt-core"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "time" "rt-core" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0bhmm94f97dg57xhr6k5j3dai67y121dsmirfjacjvsl9fn1r0ry")))

(define-public crate-batched-fn-0.1.3 (c (n "batched-fn") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "time" "rt-core"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "time" "rt-core" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "1b7iw53y1fsl8bmngwgzcrcsxkakxzc337kmaj7pkjz57mddwwrk")))

(define-public crate-batched-fn-0.1.4 (c (n "batched-fn") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "time" "rt-core"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "time" "rt-core" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0mpgzc7m626db0r23s1wl62bsmkjrpk33zmzbfim1xyqawxnil6a")))

(define-public crate-batched-fn-0.1.5 (c (n "batched-fn") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "time" "rt-core"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "time" "rt-core" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "1lsnbr16gxfzyfipibb466cdn3kk787y2jld6lz5f07nvg4gzjsj")))

(define-public crate-batched-fn-0.1.6 (c (n "batched-fn") (v "0.1.6") (d (list (d (n "flume") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("time" "rt-core" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "07jpyrxsfzfc7i8i320vf0320a1ird4pq1pzgi6nxblr8c2jfrww")))

(define-public crate-batched-fn-0.1.7 (c (n "batched-fn") (v "0.1.7") (d (list (d (n "flume") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("time" "rt-core" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0n9ads92n54xmj0sn7611k6gq2zdls7shnfij1g1hppjnsrakp5c")))

(define-public crate-batched-fn-0.1.8 (c (n "batched-fn") (v "0.1.8") (d (list (d (n "flume") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("time" "rt-core" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "00dpvylgcd119a28z792yarwz5pghs3i4p1jkbybb3azr5jny16g")))

(define-public crate-batched-fn-0.1.9 (c (n "batched-fn") (v "0.1.9") (d (list (d (n "flume") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("time" "rt-core" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "1qcyhymczy79d9rqwiwg5cbglyzf3z7kdfwhda7gf7hsnmqvcrn5")))

(define-public crate-batched-fn-0.1.10 (c (n "batched-fn") (v "0.1.10") (d (list (d (n "flume") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("time" "rt-core" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "19prn1w6z8sq9am747cxjsjsb26vmvfa0rwvycb7i1c2lfr57q6n")))

(define-public crate-batched-fn-0.2.0 (c (n "batched-fn") (v "0.2.0") (d (list (d (n "flume") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("time" "rt-core" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0bk796vwi7h029i0zwma7jlafjzv5dp67gj2ad8bmyng0bba07kc")))

(define-public crate-batched-fn-0.2.1 (c (n "batched-fn") (v "0.2.1") (d (list (d (n "flume") (r "^0.9.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("time" "rt-core" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0z45fa87kzxc8b0w5869w30sc16dgrsdwnmih046bci1br600xxs")))

(define-public crate-batched-fn-0.2.2 (c (n "batched-fn") (v "0.2.2") (d (list (d (n "flume") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.3.0") (f (quote ("time" "rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1aiv22hmj97l57pmky04mz977bs6hg1brzgpgdj3r2qhasxmpj3m")))

(define-public crate-batched-fn-0.2.3 (c (n "batched-fn") (v "0.2.3") (d (list (d (n "flume") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("time" "rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "05rb6g4xb2ib8y171xfwiszb6b5sf1ja1jpaq3s22g7jskw4kvch")))

(define-public crate-batched-fn-0.2.4 (c (n "batched-fn") (v "0.2.4") (d (list (d (n "flume") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("time" "rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0ax06hdgjfq8b8jqpsljm8yjv49jaicjxhavv0yfaz5krdypgpzx")))

(define-public crate-batched-fn-0.2.5 (c (n "batched-fn") (v "0.2.5") (d (list (d (n "flume") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("time" "rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0piqmpyv688q9mi184gx0kf80czqkjpv5lzdjhsivb99hh0py033")))

