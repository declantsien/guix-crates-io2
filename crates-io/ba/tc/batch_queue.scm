(define-module (crates-io ba tc batch_queue) #:use-module (crates-io))

(define-public crate-batch_queue-0.1.0 (c (n "batch_queue") (v "0.1.0") (d (list (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "pin-project") (r "^1.0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1fh8fcfbzbypf6jm22vrylrfmw5pz69985dzsk4pi0dp99cb7wlb")))

