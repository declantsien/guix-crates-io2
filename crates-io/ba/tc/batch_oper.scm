(define-module (crates-io ba tc batch_oper) #:use-module (crates-io))

(define-public crate-batch_oper-1.0.0 (c (n "batch_oper") (v "1.0.0") (h "15y2wksakb32jmn5y1357sdvnfwdysxz5pgc9rc5zz8zp7nm0n9y")))

(define-public crate-batch_oper-1.0.1 (c (n "batch_oper") (v "1.0.1") (h "0ggnljs4q2zi1nyqy4p7x7n4j536qiiy53377ymnvbbz0g0fvkcc")))

(define-public crate-batch_oper-1.1.0 (c (n "batch_oper") (v "1.1.0") (h "11vs00vwjyc1viw2g4b9w78jcx00f54x6635yi3c4rjf9jvlm3ba")))

(define-public crate-batch_oper-1.1.1 (c (n "batch_oper") (v "1.1.1") (h "1x1dx2p249sghyxsmxpmpmyrikdwp68a6j9xh451pqbmmbnrpmcn")))

(define-public crate-batch_oper-1.2.0 (c (n "batch_oper") (v "1.2.0") (h "11da4ar180iapyck5vra2pc2z8krbczwiz3dsb6hms5jldwjj0pn")))

(define-public crate-batch_oper-1.3.0 (c (n "batch_oper") (v "1.3.0") (h "022sbr7x96y4z0hj6gc06s5ljyi21lpydsnr0ancra32q572xf31")))

(define-public crate-batch_oper-1.3.1 (c (n "batch_oper") (v "1.3.1") (h "0xhyp9q0sby7gs4rn3xgh0l2wn992415bhw712wd50i1p0ll8pbc")))

(define-public crate-batch_oper-1.4.0 (c (n "batch_oper") (v "1.4.0") (h "04s4idivmz16p4vip8x2g68w1b3jnfmn9kk95yqn5khqicy9cxs2")))

(define-public crate-batch_oper-1.5.0 (c (n "batch_oper") (v "1.5.0") (h "15asslcsycr728va2ry7ribxngs41zr59mhn8m05c3lan5ykgm3q")))

(define-public crate-batch_oper-1.5.1 (c (n "batch_oper") (v "1.5.1") (h "0z55qj11x9yy1sahc6rblhnp1yrndyqg27h5g5cr6nvjyx46zyf5")))

(define-public crate-batch_oper-1.6.0 (c (n "batch_oper") (v "1.6.0") (h "0ph6j1p46k7haxlsyh0vd4x49jgr864sh0y3i9nnba8b9c3g98qh")))

(define-public crate-batch_oper-1.7.0 (c (n "batch_oper") (v "1.7.0") (h "038v0f62ij44bd8dj850n5vjjl3ndhxwnncawm32bfq6lx64b239") (f (quote (("std") ("default" "std"))))))

(define-public crate-batch_oper-1.8.0 (c (n "batch_oper") (v "1.8.0") (h "1n5lxvaj7bv9blsd6gpzaqifw8k52bn296lagjq0am7yaqxnnap4") (f (quote (("std") ("default" "std"))))))

(define-public crate-batch_oper-1.9.0 (c (n "batch_oper") (v "1.9.0") (h "16wlhq8bj7l9s6pa5i47f3210a2b1lbrv0rr8a5cpbz351sp6h3k") (f (quote (("std") ("default" "std"))))))

(define-public crate-batch_oper-1.10.0 (c (n "batch_oper") (v "1.10.0") (h "1gfjlhzzaaswgp54h9bm160lrmigk81vvwfgryz1xw4mvx3l6jry") (f (quote (("std") ("default" "std"))))))

(define-public crate-batch_oper-1.11.0 (c (n "batch_oper") (v "1.11.0") (h "1h7gclpwgp8xn5j9aa95ka46x13xdw3c2rqjgdm27g3j5h8rpibv") (f (quote (("std") ("side-effect") ("re-exports") ("named-into") ("macro-lit") ("default" "std" "combin" "named-into" "macro-lit" "side-effect" "re-exports") ("combin-mutual") ("combin"))))))

(define-public crate-batch_oper-1.11.1 (c (n "batch_oper") (v "1.11.1") (h "01l46dbvk5hqi3gib61n23nlnnw8qh2m1ip1yalc1y8rsvzkwh13") (f (quote (("std") ("side-effect") ("re-exports") ("named-into") ("macro-lit") ("default" "std" "combin" "named-into" "macro-lit" "side-effect" "re-exports") ("combin-mutual") ("combin"))))))

(define-public crate-batch_oper-1.12.0 (c (n "batch_oper") (v "1.12.0") (h "00xggmjl7bsx2fgrrgmnn2sj87s9912y3wg5xckrjcqrs7psxbqw") (f (quote (("std") ("side-effect") ("re-exports") ("named-into") ("macro-lit") ("default" "std" "combin" "named-into" "macro-lit" "side-effect" "re-exports" "chain_panic" "chain_todo") ("combin-mutual") ("combin") ("chain_todo") ("chain_panic"))))))

(define-public crate-batch_oper-1.12.1 (c (n "batch_oper") (v "1.12.1") (h "13sxkaplfq365ymvyl98rlr1f80mfwijf5xf5f2g8fz3hplca8si") (f (quote (("std") ("side-effect") ("re-exports") ("named-into") ("macro-lit") ("default" "std" "combin" "named-into" "macro-lit" "side-effect" "re-exports" "chain_panic" "chain_todo") ("combin-mutual") ("combin") ("chain_todo") ("chain_panic"))))))

(define-public crate-batch_oper-1.13.0 (c (n "batch_oper") (v "1.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "quote") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "syn") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1s1gfgk8691p34628sgvy3d4w741a4c8svsh0zl3izfks4cf01hl") (f (quote (("tuple_utils" "code_gen") ("tuple_iter" "code_gen") ("std") ("side-effect") ("re-exports") ("named-into") ("macro-lit") ("default" "std" "combin" "named-into" "macro-lit" "side-effect" "re-exports" "chain_panic" "chain_todo" "tuple_iter" "tuple_utils") ("combin-mutual") ("combin") ("code_gen" "syn" "quote" "proc-macro2") ("chain_todo") ("chain_panic"))))))

(define-public crate-batch_oper-2.0.0 (c (n "batch_oper") (v "2.0.0") (d (list (d (n "tuples") (r "^1.3") (o #t) (d #t) (k 0)))) (h "1x8didqkc1slp79lj4fa0926xkv91ys3090sb5ibn52bwnprj5jq") (f (quote (("std") ("side-effect") ("re-exports") ("named-into") ("macro-lit") ("default" "std" "combin" "named-into" "macro-lit" "side-effect" "re-exports" "chain_panic" "chain_todo" "tuples") ("combin") ("chain_todo") ("chain_panic"))))))

(define-public crate-batch_oper-2.1.0 (c (n "batch_oper") (v "2.1.0") (d (list (d (n "tuples") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1abq8x1y6wavbw4nzqyqxd369m9kpmkphmp3q0q2hbg64a6rnl4r") (f (quote (("std") ("side-effect") ("re-exports") ("named-into") ("macro-lit") ("default" "std" "combin" "named-into" "macro-lit" "side-effect" "re-exports" "chain_panic" "chain_todo" "tuples") ("combin") ("chain_todo") ("chain_panic"))))))

(define-public crate-batch_oper-2.2.0 (c (n "batch_oper") (v "2.2.0") (d (list (d (n "tuples") (r ">=1.4.0, <2.0.0") (o #t) (d #t) (k 0)))) (h "0yvrk6l41d941240dp43wrvzsljkazd8cyl0617fq92ml59qv06q") (f (quote (("std") ("side-effect") ("re-exports") ("once_get") ("named-into") ("macro-lit") ("default" "std" "combin" "named-into" "macro-lit" "side-effect" "re-exports" "chain_panic" "chain_todo" "tuples" "once_get") ("combin") ("chain_todo") ("chain_panic"))))))

(define-public crate-batch_oper-2.3.0 (c (n "batch_oper") (v "2.3.0") (d (list (d (n "tuples") (r "^1.4") (o #t) (d #t) (k 0)))) (h "14f2ah14w5zxaw3msa11n3wp24i4an1wn30b7rql85fxpwlwn9jf") (f (quote (("std") ("side-effect") ("re-exports") ("once_get") ("named-into") ("macro-lit") ("default" "std" "combin" "named-into" "macro-lit" "side-effect" "re-exports" "chain_panic" "chain_todo" "tuples" "once_get") ("combin") ("chain_todo") ("chain_panic"))))))

(define-public crate-batch_oper-2.3.1 (c (n "batch_oper") (v "2.3.1") (d (list (d (n "tuples") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0p95pr7ss0hqkahrzrhnq36d69fh3c1l8i4ri3na00fg5w9gvq1s") (f (quote (("std") ("side-effect") ("re-exports") ("once_get") ("named-into") ("macro-lit") ("default" "std" "combin" "named-into" "macro-lit" "side-effect" "re-exports" "chain_panic" "chain_todo" "tuples" "once_get") ("combin") ("chain_todo") ("chain_panic"))))))

