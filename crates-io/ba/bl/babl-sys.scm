(define-module (crates-io ba bl babl-sys) #:use-module (crates-io))

(define-public crate-babl-sys-0.1.0 (c (n "babl-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "12wlfkjjhypcig25ax7iacfj4a72hhrxiq78yl824gqn4cvnx11j")))

