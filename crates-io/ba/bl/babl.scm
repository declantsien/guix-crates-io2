(define-module (crates-io ba bl babl) #:use-module (crates-io))

(define-public crate-babl-0.1.0 (c (n "babl") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "babl-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "system-deps") (r "^6.0") (d #t) (k 2)) (d (n "version-compare") (r "^0.1") (d #t) (k 2)))) (h "0wffd1pn6vs2ixhpck1m4n90ns88mj1v61kjyxmg922zyws60xcr")))

(define-public crate-babl-0.1.1 (c (n "babl") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "babl-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "system-deps") (r "^6.0") (d #t) (k 2)) (d (n "version-compare") (r "^0.1") (d #t) (k 2)))) (h "1l4d721ns2ivyzskszfzvmjkxd9bkypn9vqlg0308nsrjkfrrwbp")))

