(define-module (crates-io ba sa basanta) #:use-module (crates-io))

(define-public crate-basanta-0.1.1 (c (n "basanta") (v "0.1.1") (h "0rvjqga7bgc8nglfkp4y4f67n2jr300h4aqpxg55ampbd8agsr03")))

(define-public crate-basanta-0.1.2 (c (n "basanta") (v "0.1.2") (h "03lz08pf8sqgvrzlspx6a476c5cfsyl0lcdcnw8l4vxcqld0nh45")))

(define-public crate-basanta-0.1.3 (c (n "basanta") (v "0.1.3") (h "0ka7vz8wb4kwlxw7mjr9mf5h2bjhpsplwnvqy957q3bp4lsm4h4a") (y #t)))

(define-public crate-basanta-0.1.4 (c (n "basanta") (v "0.1.4") (h "0z11yx9w735559zqg9zcba0ksdx6irb63bcbnbg0a3ayky2hgwwv")))

