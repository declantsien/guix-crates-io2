(define-module (crates-io ba sh bashtastic-visualizer) #:use-module (crates-io))

(define-public crate-bashtastic-visualizer-0.1.0 (c (n "bashtastic-visualizer") (v "0.1.0") (d (list (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0abzllfpqd703gkvz1pmagvxdrlhj4gwy4bs4zfccrkcc87zi4v3")))

(define-public crate-bashtastic-visualizer-0.1.1 (c (n "bashtastic-visualizer") (v "0.1.1") (d (list (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0629w51sa0zbw17z8sicqpw5p4q4pipradbkz0lh50zdxh1xxqkp")))

(define-public crate-bashtastic-visualizer-0.1.2 (c (n "bashtastic-visualizer") (v "0.1.2") (d (list (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fc4dcscharc19rg75krzqdvl4pvf8bwnvcmzi37c7b6mgji59sg")))

(define-public crate-bashtastic-visualizer-0.1.3 (c (n "bashtastic-visualizer") (v "0.1.3") (d (list (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0rgxl40qfk9cmd64wdqrbqpjnqhlfszcad9wkb51d3434y5wkcm2")))

(define-public crate-bashtastic-visualizer-0.1.4 (c (n "bashtastic-visualizer") (v "0.1.4") (d (list (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0p3f6fhr6mjc2il3dzhqshjjl1r7h3az8g2cngzs8mx6frgb5gxk")))

(define-public crate-bashtastic-visualizer-0.1.5 (c (n "bashtastic-visualizer") (v "0.1.5") (d (list (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0iwhmavsv33dlygk0kbprgdc4626w26gm5m79qap0xl7cq1mr93x")))

