(define-module (crates-io ba sh bash-builtins) #:use-module (crates-io))

(define-public crate-bash-builtins-0.1.0 (c (n "bash-builtins") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "bash_builtins_macro") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.43") (d #t) (k 2)))) (h "1y5z38yrxsdcyfyixhw5b3fd42p3dm27jdhx93wprb3bk7457g03")))

(define-public crate-bash-builtins-0.1.1 (c (n "bash-builtins") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "bash_builtins_macro") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.43") (d #t) (k 2)))) (h "1gmlslw3cvp02vr26zi1rhhmwycs7dip0qk6wxbz6kxz5nzj5kb8")))

(define-public crate-bash-builtins-0.1.2 (c (n "bash-builtins") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "bash_builtins_macro") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.43") (d #t) (k 2)))) (h "1f0wv7kzcspdlnv0ki7h00hjss4smpncan6xnk1px8c9cbxc335k")))

(define-public crate-bash-builtins-0.1.3 (c (n "bash-builtins") (v "0.1.3") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "bash_builtins_macro") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.43") (d #t) (k 2)))) (h "1x18g7xi1dcjdbbwqaa7khr4i6pa94jb74mxg1yvhhg28d4ndj1c")))

(define-public crate-bash-builtins-0.2.0 (c (n "bash-builtins") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "bash_builtins_macro") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.43") (d #t) (k 2)))) (h "101xsg5dn3rwikr693lpk1gs3vrs6d2idq1bim6dccc2bm5a8l9f")))

(define-public crate-bash-builtins-0.3.0 (c (n "bash-builtins") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "bash_builtins_macro") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.43") (d #t) (k 2)))) (h "11mv235jhpyynmzh5gg5qqw0smvwc225fyb2spvqqsjg25i5bm53")))

(define-public crate-bash-builtins-0.3.1 (c (n "bash-builtins") (v "0.3.1") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "bash_builtins_macro") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.43") (d #t) (k 2)))) (h "1gc0wzsfkzxmw29m66qk62xibjcm2m5z7vh22jvc23d3hb7f4axv")))

(define-public crate-bash-builtins-0.4.0 (c (n "bash-builtins") (v "0.4.0") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "bash_builtins_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.43") (d #t) (k 2)))) (h "07aw681yw3aid7385vfr7zxclhfyrhb2vf25xj678rl0v2bsjqaz")))

(define-public crate-bash-builtins-0.4.1 (c (n "bash-builtins") (v "0.4.1") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "bash_builtins_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.43") (d #t) (k 2)))) (h "1ddf60jgfl3d93j8dknk67k30g8hmi7l913x1fc0c5cchz7n9558")))

