(define-module (crates-io ba sh bash_plugin_rs) #:use-module (crates-io))

(define-public crate-bash_plugin_rs-0.1.0 (c (n "bash_plugin_rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)))) (h "17yg6zjrl6azlcrnniqrqylf028zh6zza2mg6qzz3vplsna1fb6h")))

(define-public crate-bash_plugin_rs-0.1.1 (c (n "bash_plugin_rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "hello_bash_plugin") (r "^0.1") (d #t) (k 2)))) (h "1jph51pdv8327ns29d760ni9gkxlv5s08hr24gsm9yc4al3j0v4y")))

(define-public crate-bash_plugin_rs-0.2.0 (c (n "bash_plugin_rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "090vsd14zwhq8rrgzrs38qhl37hfys50cnbiwxdh22zc0x4kwkd5")))

