(define-module (crates-io ba sh bash_bundler) #:use-module (crates-io))

(define-public crate-bash_bundler-0.1.0 (c (n "bash_bundler") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1qk2lsb9kzgvzh1sqqxm5lcvfgsbdy9mh79avkvm55pqd6f1sliz")))

(define-public crate-bash_bundler-0.1.1 (c (n "bash_bundler") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1s3mzrgajybplp9z9q450lih8sbx3zhg73fbjcl1wjig37n3qagq")))

