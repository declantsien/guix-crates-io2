(define-module (crates-io ba sh bashfull) #:use-module (crates-io))

(define-public crate-bashfull-0.1.0 (c (n "bashfull") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "17v239qa5zdkds7hhvhjah01pj1vpfd3n4a2mp1faqciyma77vvz")))

(define-public crate-bashfull-0.1.1 (c (n "bashfull") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "01wv9x9spxrh2z1sc7mi4j52ynf9jdm3cd3s54l81ba5h9gqxrf2")))

(define-public crate-bashfull-0.1.2 (c (n "bashfull") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3") (k 0)))) (h "1qz0076qb5rldbfnda3ijhvci3ds5992yn5ybich2rh662zhba4l")))

(define-public crate-bashfull-0.1.3 (c (n "bashfull") (v "0.1.3") (d (list (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dh9d23i370bl4ahhq4p1l4x29r7856c2p9f6xpf2aabxwa8zzja")))

(define-public crate-bashfull-0.1.4 (c (n "bashfull") (v "0.1.4") (d (list (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "execute") (r "^0.2.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19spldsi9dw6dmq2bj10j3g5xya77br0fxabi8akqvsy003m9z50")))

