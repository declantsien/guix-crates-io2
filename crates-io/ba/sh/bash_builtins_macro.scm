(define-module (crates-io ba sh bash_builtins_macro) #:use-module (crates-io))

(define-public crate-bash_builtins_macro-0.1.0 (c (n "bash_builtins_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xdlapxp508kdig0ff84g9f2471299hl5c0f81zm68pp2psyskax") (y #t)))

(define-public crate-bash_builtins_macro-0.1.1 (c (n "bash_builtins_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0z379wz9lxd87vcrby0w3ycak1kxizq1yw1g2cab3dysv76s2m7g")))

(define-public crate-bash_builtins_macro-0.2.0 (c (n "bash_builtins_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yi2787lba0a2gjs065hz9znf9gm8mq28m4pqc4wz7jb05g7lraj")))

