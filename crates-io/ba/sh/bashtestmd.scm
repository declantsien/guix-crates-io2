(define-module (crates-io ba sh bashtestmd) #:use-module (crates-io))

(define-public crate-bashtestmd-0.3.0 (c (n "bashtestmd") (v "0.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "1zfydrcqwpbnggxvfq5azgwh9pv7f7bg1s92b0qmivdz9q21mnd4")))

(define-public crate-bashtestmd-0.4.0 (c (n "bashtestmd") (v "0.4.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "1yg7idf449ngbcp6xf7hnnc4jci205c5x4yks8kgw374zdilfiqn")))

(define-public crate-bashtestmd-0.4.1 (c (n "bashtestmd") (v "0.4.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "025jz4nbgxpgi0wpgf145khg8cxy3hm0sn41nvzn8px2g6q6nav2")))

