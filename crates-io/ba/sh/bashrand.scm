(define-module (crates-io ba sh bashrand) #:use-module (crates-io))

(define-public crate-bashrand-0.1.0 (c (n "bashrand") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0ldawyg6x5jqavk0lz4ws1zyms9f17h8mwcslyzgpz1z53m012r6")))

(define-public crate-bashrand-0.2.0 (c (n "bashrand") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1sl5rfkbg916r0176w6khs5dx6zv45kmzyvcvzwnjn89ziyh6s9m")))

(define-public crate-bashrand-0.2.1 (c (n "bashrand") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0nfvpb0gj31y6wbh366mkx6fri2a8lp2jc2cdmhlnd0hjmmic2hx")))

