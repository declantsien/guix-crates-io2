(define-module (crates-io ba sh bash-kv) #:use-module (crates-io))

(define-public crate-bash-kv-1.0.0 (c (n "bash-kv") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0wdzkqnnhxhwi612pmy7p67ppgn8pbmkzxrp8w4h37axc4p47jis")))

