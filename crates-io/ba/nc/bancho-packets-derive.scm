(define-module (crates-io ba nc bancho-packets-derive) #:use-module (crates-io))

(define-public crate-bancho-packets-derive-0.1.0 (c (n "bancho-packets-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0f3xzjclwirss4iskpmm8xgyacwmrbf4xb9lxz0giv6jylrgbgs8")))

(define-public crate-bancho-packets-derive-0.2.0 (c (n "bancho-packets-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "bancho-packets") (r "^5") (d #t) (k 2)))) (h "1j8ghw711xhf25qqv380yq3bq740jqv6vxc0x5d0x83v9am3hfwv")))

