(define-module (crates-io ba nc bancho-packet) #:use-module (crates-io))

(define-public crate-bancho-packet-0.1.0 (c (n "bancho-packet") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "0sbypg7hys7l19isrzi3107jw3bap7dbwpzmvjnz5rcyam5pgr0v")))

(define-public crate-bancho-packet-0.2.0 (c (n "bancho-packet") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "1xky7n26grvp1fqrdvxhl56nmi35ilyknabclb78jrkl13hnwkpv")))

