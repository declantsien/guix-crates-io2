(define-module (crates-io ba z- baz-tree-sitter-traversal) #:use-module (crates-io))

(define-public crate-baz-tree-sitter-traversal-0.1.3 (c (n "baz-tree-sitter-traversal") (v "0.1.3") (d (list (d (n "tree-sitter") (r ">=0.22") (o #t) (d #t) (k 0)) (d (n "tree-sitter-cpp") (r "^0.22") (d #t) (k 2)) (d (n "tree-sitter-rust") (r "^0.21") (d #t) (k 2)))) (h "1adclcxiw9drgm725mqpjdzm23ma9x1pbhsxvlg5idkp4x050f5j") (f (quote (("default" "tree-sitter")))) (r "1.66")))

(define-public crate-baz-tree-sitter-traversal-0.1.4 (c (n "baz-tree-sitter-traversal") (v "0.1.4") (d (list (d (n "tree-sitter") (r ">=0.22") (o #t) (d #t) (k 0)) (d (n "tree-sitter-cpp") (r "^0.22") (d #t) (k 2)) (d (n "tree-sitter-rust") (r "^0.21") (d #t) (k 2)))) (h "0qiaizd6p934dg9ar21p000v77yp3rfzbnkzr14lydlzrsj7gbmj") (f (quote (("default" "tree-sitter")))) (r "1.66")))

