(define-module (crates-io ba im baimax) #:use-module (crates-io))

(define-public crate-baimax-0.1.0 (c (n "baimax") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.6.0") (d #t) (k 0)) (d (n "nom") (r "^3.0.0") (d #t) (k 0)) (d (n "penny") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (o #t) (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "02pcv3h8bwpqgacqz1yj5q39p0i6jf0sx4lmk9sbs53hlflb86vg") (f (quote (("serde-serialize" "chrono/serde" "penny/serde-serialize" "serde" "serde_derive") ("lint" "clippy") ("default" "serde-serialize"))))))

