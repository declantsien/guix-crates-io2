(define-module (crates-io ba ls balsa) #:use-module (crates-io))

(define-public crate-balsa-0.1.0 (c (n "balsa") (v "0.1.0") (d (list (d (n "lyn") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 0)))) (h "07r5sfm6ynhkjkq55pqs0nb9ypf69zili2a9zgm2ca6a5f1vijnd")))

(define-public crate-balsa-0.1.1 (c (n "balsa") (v "0.1.1") (d (list (d (n "lyn") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 0)))) (h "1pk1y6vl84r0csq5p81khip66dsrql30j25kyk474px7acpmzmad")))

(define-public crate-balsa-0.3.0 (c (n "balsa") (v "0.3.0") (d (list (d (n "lyn") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 0)))) (h "1zg031fffbvxb6z8f3ww5yi30hz7qa3pigj0fz4i4zd5kvjz6am8")))

(define-public crate-balsa-0.3.2 (c (n "balsa") (v "0.3.2") (d (list (d (n "lyn") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 0)))) (h "09j12m7w4h2gn6m3n110x81ngjzb6wvwabfdyybxzhhr8imwr4qp")))

