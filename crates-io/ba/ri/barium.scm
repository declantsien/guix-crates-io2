(define-module (crates-io ba ri barium) #:use-module (crates-io))

(define-public crate-barium-0.1.0 (c (n "barium") (v "0.1.0") (d (list (d (n "ash") (r "^0.37.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)))) (h "17h4mrdas78k5vk4f03517vg9gz3h0v2ky32np7254jwy9hnzbfl")))

