(define-module (crates-io ba ri barish) #:use-module (crates-io))

(define-public crate-barish-0.1.0 (c (n "barish") (v "0.1.0") (h "041i5rpjf9gg882gn8vmk1w7pwrwxvix2svvqsighmyxb1cj29l8")))

(define-public crate-barish-0.1.1 (c (n "barish") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "14dgykvsp11njvfim4h0igpqh8qdrqjfqm08msip8z0v1jz7nanr")))

(define-public crate-barish-0.1.2 (c (n "barish") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1iw753d4fpgij4xja882x21lpikiv0gpnmsnjpagzrzlylg7fmbs")))

(define-public crate-barish-0.1.3 (c (n "barish") (v "0.1.3") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1zik3bakgwpfqlffv5fhjq62565k53b7p1qgih3hkpwp9agwmzkv")))

(define-public crate-barish-0.1.4 (c (n "barish") (v "0.1.4") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0l93q03dn3zrhvlh6i7jp5a39scy5i6k7mwgy96w8lri8p180xj4")))

(define-public crate-barish-0.1.5 (c (n "barish") (v "0.1.5") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "14mi4chkqdi9wj51hckcg8bzw9h6d1h8s35yhwhadwpzz5sy3ffk")))

(define-public crate-barish-0.1.6 (c (n "barish") (v "0.1.6") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "01mnbgj4kc408wif0dd6l02h9bm2x7zv903v28yjdak3p1mabxwb")))

(define-public crate-barish-0.1.7 (c (n "barish") (v "0.1.7") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0kyqwx0a45bkxvdajm7jnr6lk44l2xn6daq482gs4nkiw0hgry74")))

(define-public crate-barish-0.1.8 (c (n "barish") (v "0.1.8") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1xsgdici1wilq0qvb0szbcmyjah2pj0jldf1wawp5vq5siiixxas")))

(define-public crate-barish-0.1.9 (c (n "barish") (v "0.1.9") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1frgza4k8663chcas2ixqmm40a16yd7jgz532qrzfdplf611fqzg")))

(define-public crate-barish-0.1.92 (c (n "barish") (v "0.1.92") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0zcrgi71p214myrxl8azv3dd3pmwnn2zrd1ivam0qc6izq6w5hma")))

(define-public crate-barish-0.1.93 (c (n "barish") (v "0.1.93") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1msi8s2m8kh3g9gb542n4cmf29vvd8j9pic817p015pbh10sb9np")))

(define-public crate-barish-0.1.94 (c (n "barish") (v "0.1.94") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1382kq0bmbd3lbqmm9xpdl470hp6b9jncpb9l3ilackzw44bw806")))

(define-public crate-barish-0.1.95 (c (n "barish") (v "0.1.95") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "07hlfjzjz9q8i96mj0v4xycvz0xyimh0gdqmiajvc4bwfii25pfd")))

