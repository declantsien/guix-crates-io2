(define-module (crates-io ba da badargs) #:use-module (crates-io))

(define-public crate-badargs-0.1.0 (c (n "badargs") (v "0.1.0") (h "0s89l1w3aa8llslh7pcfnz5ky0zyqcwc93y5ikfi6k4g94fcvxsq")))

(define-public crate-badargs-0.1.1 (c (n "badargs") (v "0.1.1") (h "0z6bsdidslf48smkn5w4dqz8cfyy58afabsx8d73wr6il8m4819l")))

(define-public crate-badargs-0.1.2 (c (n "badargs") (v "0.1.2") (h "1b1vsj0vkf9zyn8b15ai6wbdhrps7vh7wil5wj0yvw9k80b0hlv2")))

(define-public crate-badargs-0.2.0 (c (n "badargs") (v "0.2.0") (h "1rsd18hd96ikc6vlpp9rd3zhf6pm254s81iarlbpx6awnvwxcxmi")))

