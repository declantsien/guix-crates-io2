(define-module (crates-io ba cn bacnet_parse) #:use-module (crates-io))

(define-public crate-bacnet_parse-0.1.0 (c (n "bacnet_parse") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)))) (h "1xmzby1j2k9k73g4157yz0jis38m466jhpaxyab2qw5m68g1aybz")))

(define-public crate-bacnet_parse-0.1.1 (c (n "bacnet_parse") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)))) (h "0nfkffrs1323y4ysw2j3a0amk07m074g8f65ihv92wsddbj3bw9i")))

(define-public crate-bacnet_parse-0.2.0 (c (n "bacnet_parse") (v "0.2.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)))) (h "14p9b2g1dg676ld990fglsgs7v4qzwd4wj91drrlci1lq1dnv84c")))

