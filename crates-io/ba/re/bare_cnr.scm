(define-module (crates-io ba re bare_cnr) #:use-module (crates-io))

(define-public crate-bare_cnr-0.1.0-alpha.1 (c (n "bare_cnr") (v "0.1.0-alpha.1") (d (list (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_bare") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1w0s6bnnk0axgf2vbbhbzpxhvbbjmb4q7rlv17c67wyf2z1lk2pa")))

