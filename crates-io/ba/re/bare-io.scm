(define-module (crates-io ba re bare-io) #:use-module (crates-io))

(define-public crate-bare-io-0.0.0 (c (n "bare-io") (v "0.0.0") (h "0p0gf4lldi4vs61j9ls5d2aj9ngv839pnl3xmvlnhykylh357hry") (y #t)))

(define-public crate-bare-io-0.1.0 (c (n "bare-io") (v "0.1.0") (d (list (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "0s0rm7qmxmx2bmzvqmlnics8zg1aarhgzfz2bfiw0abxqfiqhgfw") (f (quote (("std") ("default" "memchr")))) (y #t)))

(define-public crate-bare-io-0.1.1 (c (n "bare-io") (v "0.1.1") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "1hah0j30f9z4wvyfjr2znw7pcbd10vp4xlpzfp4kl9jqsbmijxma") (f (quote (("std") ("default")))) (y #t)))

(define-public crate-bare-io-0.2.0 (c (n "bare-io") (v "0.2.0") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "0bjji3a09mvy86m6q4ny151wbk5z6p162gda9ns2qkcnj87lrnkj") (f (quote (("std-nightly" "std" "nightly") ("std" "alloc") ("nightly") ("default") ("alloc")))) (y #t)))

(define-public crate-bare-io-0.2.1 (c (n "bare-io") (v "0.2.1") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "17bwxvih747pycpbvy99kgvj4fw8wbwkw1gcwh5wvanp2m4friny") (f (quote (("std-nightly" "std" "nightly") ("std" "alloc") ("nightly") ("default") ("alloc")))) (y #t)))

