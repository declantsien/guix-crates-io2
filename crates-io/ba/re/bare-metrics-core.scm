(define-module (crates-io ba re bare-metrics-core) #:use-module (crates-io))

(define-public crate-bare-metrics-core-0.1.0 (c (n "bare-metrics-core") (v "0.1.0") (d (list (d (n "hdrhistogram") (r "^7.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bare") (r "^0.5.0") (d #t) (k 0)))) (h "0m4rgrsslkcxza5496afrihwxcq0pslz3s83gw46f2aw09j4cyzz")))

