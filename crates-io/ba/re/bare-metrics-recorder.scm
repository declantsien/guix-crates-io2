(define-module (crates-io ba re bare-metrics-recorder) #:use-module (crates-io))

(define-public crate-bare-metrics-recorder-0.1.0 (c (n "bare-metrics-recorder") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "bare-metrics-core") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "dashmap") (r "^4.0.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "metrics") (r "^0.17.0") (d #t) (k 0)) (d (n "serde_bare") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0x6wnb7dp22wvx59a3j2shqm7rr1hx6fwpjlsibg53q6h9c3709z")))

