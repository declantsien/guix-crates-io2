(define-module (crates-io ba re bare-metal) #:use-module (crates-io))

(define-public crate-bare-metal-0.1.0 (c (n "bare-metal") (v "0.1.0") (h "1c4qjxn6ba5n72mywzfb9ipidj593q9m1dsqi11dfxv95z20mphr")))

(define-public crate-bare-metal-0.1.1 (c (n "bare-metal") (v "0.1.1") (h "0x25382jm38znvfl94yxxhj87jh86azxbvasizhwz08sfwjbmjxa")))

(define-public crate-bare-metal-0.1.2 (c (n "bare-metal") (v "0.1.2") (h "0763ka1s06kbklqlvhv4dvdqdf4v55cd9rm5cww8zwa95m5zb87d") (f (quote (("default" "const-fn") ("const-fn"))))))

(define-public crate-bare-metal-0.2.0 (c (n "bare-metal") (v "0.2.0") (h "15lywq2958w4bakp0i0i7nf9h2hx1x93lk89dn55yrh61byzcysi") (f (quote (("const-fn")))) (y #t)))

(define-public crate-bare-metal-0.2.1 (c (n "bare-metal") (v "0.2.1") (h "1mrza2mrmnpmb8g53amwbp9rd8fjha1rdjxw7rcqp6vblfy6y15d") (f (quote (("const-fn"))))))

(define-public crate-bare-metal-0.2.2 (c (n "bare-metal") (v "0.2.2") (h "0p33kz0lyisfvfnwnhzy0900iimbx7n393ws0bldwv3gpxf1piws") (f (quote (("const-fn")))) (y #t)))

(define-public crate-bare-metal-0.2.3 (c (n "bare-metal") (v "0.2.3") (h "14lgpx02kbx3n42zds0aypp5l024l9pb07di576wfj6n9qlzkp0v") (f (quote (("const-fn"))))))

(define-public crate-bare-metal-0.1.3 (c (n "bare-metal") (v "0.1.3") (h "1x147xqxwj3h8s0ysc0dlnmym09zf11l8zfnpj6pchyq6fqqhc6z") (f (quote (("default" "const-fn") ("const-fn"))))))

(define-public crate-bare-metal-0.2.4 (c (n "bare-metal") (v "0.2.4") (d (list (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "0nkkbajx2hydm97lbnia73rww83hb5s0d3b3h0z4ab9vv69z7jm3") (f (quote (("const-fn"))))))

(define-public crate-bare-metal-0.2.5 (c (n "bare-metal") (v "0.2.5") (d (list (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "1cy5pbb92fznnri72y6drfpjxj4qdmd62f0rrlgy70dxlppn9ssx") (f (quote (("const-fn"))))))

(define-public crate-bare-metal-1.0.0 (c (n "bare-metal") (v "1.0.0") (h "00wn61b219nkfcwggim5pj7mcyn1gkq8zqaq4gjlb0rri9d8zzpq")))

