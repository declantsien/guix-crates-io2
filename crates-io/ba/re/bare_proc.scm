(define-module (crates-io ba re bare_proc) #:use-module (crates-io))

(define-public crate-bare_proc-0.1.0 (c (n "bare_proc") (v "0.1.0") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1jv8jk2m2acfp7028i87s4zmajhm2clkhlxjwlbhpk14w8gq1427")))

(define-public crate-bare_proc-0.1.1 (c (n "bare_proc") (v "0.1.1") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "13f6bk1m0mmsm6crz61k1hvlp6hmhgdcg022d3n6ffqb771d591v")))

(define-public crate-bare_proc-0.1.2 (c (n "bare_proc") (v "0.1.2") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1xlqma4xn1lia2yblr8rp893349c9jjz4i89ykhw4sx2yhfnqsal")))

(define-public crate-bare_proc-0.2.0 (c (n "bare_proc") (v "0.2.0") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0zmjyxxikwwnai4gpzvydcnw598z1vvvzmri38c3kq78z15gvkiz")))

