(define-module (crates-io ba re bare-websocket) #:use-module (crates-io))

(define-public crate-bare-websocket-0.0.1 (c (n "bare-websocket") (v "0.0.1") (d (list (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sha1-hasher") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "04ckdxn322bx53qwb7dy6aiycqs5r4qrnq182j9ss4736ihjgn6q") (y #t)))

(define-public crate-bare-websocket-0.0.2 (c (n "bare-websocket") (v "0.0.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sha1-hasher") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0brax1srpy1mwq6i428n3n10my73rxb15d3mbhfg9qfgx0mgm12c") (y #t)))

(define-public crate-bare-websocket-0.0.3 (c (n "bare-websocket") (v "0.0.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sha1-hasher") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "131izaxj87avbbc6yrvjm7500q4l57142h69hbrvj9fxf40nn7sg") (y #t)))

(define-public crate-bare-websocket-0.0.4 (c (n "bare-websocket") (v "0.0.4") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sha1-hasher") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1s35dip0zsvmb5dd1i1vi2cl79pkkdhhhb5z82ngbkqnpj2pry4b") (y #t)))

(define-public crate-bare-websocket-0.0.5 (c (n "bare-websocket") (v "0.0.5") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sha1-hasher") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1d5jvbqll0yh7axb7bvqjhs7ljgj2rgd8lkpas2h00pdmcp60ma1") (y #t)))

(define-public crate-bare-websocket-0.0.6 (c (n "bare-websocket") (v "0.0.6") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sha1-hasher") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "11pns6qxyyrydmlbpb8maflcsfsxlb2nf15gvcgqfiqmf5iz2fjp") (y #t)))

