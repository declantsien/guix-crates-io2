(define-module (crates-io ba re bare-metrics-reader) #:use-module (crates-io))

(define-public crate-bare-metrics-reader-0.1.0 (c (n "bare-metrics-reader") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "bare-metrics-core") (r "^0.1.0") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_bare") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0m52qbksbkakk6rw66khkl7qfx8gh0rax1mp1jkbzzs8q7xyrbna")))

