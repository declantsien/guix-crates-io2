(define-module (crates-io ba rl barley-proc) #:use-module (crates-io))

(define-public crate-barley-proc-0.0.1 (c (n "barley-proc") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "1zgdq5b15wpfb70wp6ndm7j2dg1w5k9j1ycai3kmgzpl2xl5ivjp") (y #t)))

(define-public crate-barley-proc-0.1.0 (c (n "barley-proc") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "1lgiaf9bhwpww30mzn5cr7b1nh76v9pxkhgh72xs6pssdz4vlx2i") (y #t)))

(define-public crate-barley-proc-0.2.0 (c (n "barley-proc") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0z71cp7519w2pgz1mx2lv1yn1k3m19czmhda97i0zx7n04d7582s") (y #t)))

