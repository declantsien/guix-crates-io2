(define-module (crates-io ba rl barley-interface) #:use-module (crates-io))

(define-public crate-barley-interface-0.0.1 (c (n "barley-interface") (v "0.0.1") (d (list (d (n "barley-runtime") (r "^0.0.1") (d #t) (k 0)) (d (n "barley-utils") (r "^0.0.1") (f (quote ("time" "process"))) (d #t) (k 2)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "18zs72715sc1lircvxd2bvr73gq22ma0yn5jmy5k4rf6q54g8dg5")))

(define-public crate-barley-interface-0.1.0 (c (n "barley-interface") (v "0.1.0") (d (list (d (n "barley-runtime") (r "^0.1.0") (d #t) (k 0)) (d (n "barley-utils") (r "^0.1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0svgnmb88zxydzss17yxr765wff8fm5s2xw1hgj6x4bzw0k2ragg")))

(define-public crate-barley-interface-0.2.0 (c (n "barley-interface") (v "0.2.0") (d (list (d (n "barley-runtime") (r "^0.2.0") (d #t) (k 0)) (d (n "barley-utils") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1h4ihqw330c2b48svvd8q7q04dqhc4gsc8zps4dqy4rn2m39v0vc")))

(define-public crate-barley-interface-0.3.0 (c (n "barley-interface") (v "0.3.0") (d (list (d (n "barley-runtime") (r "^0.3.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0kqsl7c780grkr8g7n867vfdka0kwdbja5r3a7yw7j4r4f1fnrfy")))

