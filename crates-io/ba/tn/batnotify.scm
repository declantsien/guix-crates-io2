(define-module (crates-io ba tn batnotify) #:use-module (crates-io))

(define-public crate-batnotify-0.1.0 (c (n "batnotify") (v "0.1.0") (d (list (d (n "battery") (r "^0.7") (d #t) (k 0)) (d (n "notify-rust") (r "^3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "09527bww06ivaikzv9jfj7ck6am79yrs3cql7cbgkr966ylpmgyp")))

(define-public crate-batnotify-0.1.1 (c (n "batnotify") (v "0.1.1") (d (list (d (n "battery") (r "^0.7") (d #t) (k 0)) (d (n "notify-rust") (r "^3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0j7nnql1l8k3akis8bl4zqyrkg1iiinwraqkhirdi2mbv29489i9")))

