(define-module (crates-io ba mi bami) #:use-module (crates-io))

(define-public crate-bami-0.1.0 (c (n "bami") (v "0.1.0") (d (list (d (n "amethyst_core") (r "^0.10.0") (d #t) (k 0)) (d (n "amethyst_input") (r "^0.11.0") (d #t) (k 0)) (d (n "gilrs") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1glrlm78i0bnd5xpfkxilxac5isg8ag4zzciksrj98ww5ysgy0af")))

(define-public crate-bami-0.2.0 (c (n "bami") (v "0.2.0") (d (list (d (n "amethyst_core") (r "^0.10.0") (d #t) (k 0)) (d (n "amethyst_error") (r "^0.5.0") (d #t) (k 0)) (d (n "amethyst_input") (r "^0.11.0") (d #t) (k 0)) (d (n "gilrs") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y6v4rs2jglnl6i6cn813jpym3shzdnf153fx9k6ha46pzy2pwmr")))

(define-public crate-bami-0.2.1 (c (n "bami") (v "0.2.1") (d (list (d (n "amethyst_core") (r "^0.10.0") (d #t) (k 0)) (d (n "amethyst_error") (r "^0.5.0") (d #t) (k 0)) (d (n "amethyst_input") (r "^0.11.0") (d #t) (k 0)) (d (n "gilrs") (r "^0.7.4") (o #t) (d #t) (k 0)))) (h "18bvz86h84hrb0lampzypvpb3v32idvniwhqaxgaaqshki6b2vmg")))

