(define-module (crates-io ba tt battlesnake) #:use-module (crates-io))

(define-public crate-battlesnake-0.1.0 (c (n "battlesnake") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nik1i6n55vz8dmz3z7mra6jhqlpvkhcarradnpm7mwyb6yx9jmk")))

(define-public crate-battlesnake-0.1.1 (c (n "battlesnake") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zyc01j5mmr6xgqibj82jr1zpsp7n8say15l044mwiv70cqrgjxj")))

(define-public crate-battlesnake-0.1.2 (c (n "battlesnake") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ac16i4q0yznzfm3k4128lhc5ks05q63jxb63ki8ga50f7ika264")))

(define-public crate-battlesnake-0.1.3 (c (n "battlesnake") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18s70nws8xb7bwgz4j75674881323kjxka9ik1ljysnfw7ggy0bm")))

(define-public crate-battlesnake-0.1.4 (c (n "battlesnake") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04gs4scafmins5d20gs6w0nj5il5lk5qc6iih9m4mac5pp781i47")))

(define-public crate-battlesnake-0.1.5 (c (n "battlesnake") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1msrb8qynay7504aq35x41i54qj0wx72q996f1whjq5gdrwav0df")))

