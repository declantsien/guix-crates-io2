(define-module (crates-io ba tt battrs) #:use-module (crates-io))

(define-public crate-battrs-0.1.0 (c (n "battrs") (v "0.1.0") (h "1dmrqis7z7jj7l5ql9k0ws6vxh12gzzn9yzs9i7vwhx3bqqj8r3y")))

(define-public crate-battrs-0.2.0 (c (n "battrs") (v "0.2.0") (h "1fs393ssndaxvk0qgb60xckkiwyyki2f04d580bphbsz20y1i87r")))

