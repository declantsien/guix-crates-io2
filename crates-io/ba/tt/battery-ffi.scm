(define-module (crates-io ba tt battery-ffi) #:use-module (crates-io))

(define-public crate-battery-ffi-0.1.0 (c (n "battery-ffi") (v "0.1.0") (d (list (d (n "battery") (r "^0.5.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "166znys1l7cf38krn9zkgw8gs8d6qs8z2x8c6ahl7q9fxm1a6ji0")))

(define-public crate-battery-ffi-0.1.1 (c (n "battery-ffi") (v "0.1.1") (d (list (d (n "battery") (r "^0.5.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0j8va2bxqyrnz9byrgdx2sxc22xqiyc66mcn5ylya9a0f4ji15dp") (f (quote (("default" "cbindgen"))))))

(define-public crate-battery-ffi-0.1.2 (c (n "battery-ffi") (v "0.1.2") (d (list (d (n "battery") (r "^0.5.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0gncli3ixpsc8g7pacj10pl62fj8fqg2lsk8ikwhhd6bxg2cx96z") (f (quote (("default" "cbindgen"))))))

(define-public crate-battery-ffi-0.1.3 (c (n "battery-ffi") (v "0.1.3") (d (list (d (n "battery") (r "^0.6.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0apgk08xb29fs307j0pq67glhqya2fy2d49mgqbi1inagp6wz7k1") (f (quote (("default" "cbindgen"))))))

(define-public crate-battery-ffi-0.1.4 (c (n "battery-ffi") (v "0.1.4") (d (list (d (n "battery") (r "^0.6.1") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "1s4r4ygmwxgh3rffs95hd5v151az30jqjlgg5fw9h1j8wgrfh0bp") (f (quote (("default" "cbindgen"))))))

(define-public crate-battery-ffi-0.1.5 (c (n "battery-ffi") (v "0.1.5") (d (list (d (n "battery") (r "^0.6.2") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "1cs9q09w54kxrn9z50nj57rv6q8ivrmy9nhry7v0vbkb6c7b3x7g") (f (quote (("default" "cbindgen"))))))

(define-public crate-battery-ffi-0.7.0 (c (n "battery-ffi") (v "0.7.0") (d (list (d (n "battery") (r "^0.7.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0x5g30vss4zmjdnhal8rjfvpk7ilaja37n30v3hkh8wcy3xnvf8s") (f (quote (("default" "cbindgen"))))))

(define-public crate-battery-ffi-0.7.1 (c (n "battery-ffi") (v "0.7.1") (d (list (d (n "battery") (r "^0.7.1") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "1igwvz2x6w38zbk0v89pdczdlsjc21ykr1rs1s91vw7abrjwjnj7") (f (quote (("default" "cbindgen"))))))

(define-public crate-battery-ffi-0.7.2 (c (n "battery-ffi") (v "0.7.2") (d (list (d (n "battery") (r "^0.7.1") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "03pxi8wnvaaj7w7bq3xwn5a4lgrvpkr1ysdi4aacillvhs4l6z6k") (f (quote (("default" "cbindgen"))))))

(define-public crate-battery-ffi-0.7.3 (c (n "battery-ffi") (v "0.7.3") (d (list (d (n "battery") (r "^0.7.3") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ziq9nr8zh2ywak731xg46cyp8wndkgwahmg9avmv3fjxyc7nqlb") (f (quote (("default" "cbindgen"))))))

(define-public crate-battery-ffi-0.7.4 (c (n "battery-ffi") (v "0.7.4") (d (list (d (n "battery") (r "^0.7.4") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8.7") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "142h6vsm7dmnb1j04fm49vm01jszzjd4wgw22wsbi664k7mig3lp") (f (quote (("default" "cbindgen"))))))

(define-public crate-battery-ffi-0.7.5 (c (n "battery-ffi") (v "0.7.5") (d (list (d (n "battery") (r "^0.7") (d #t) (k 0)) (d (n "cbindgen") (r "^0.10.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l0dnn4yxnc3d9dh66bgfj3aba27mm4ayvs7az5kng9n0gw9zv6z") (f (quote (("default" "cbindgen"))))))

