(define-module (crates-io ba tt battle-net-oauth) #:use-module (crates-io))

(define-public crate-battle-net-oauth-0.1.0 (c (n "battle-net-oauth") (v "0.1.0") (d (list (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0isj48jm75ffa5p670ysyyy4lvmx1jhhi347ab1377pxshk4ll6d")))

(define-public crate-battle-net-oauth-0.1.1 (c (n "battle-net-oauth") (v "0.1.1") (d (list (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0vyy8x1znmfd0f9bpsfd3inlxar3w25a0gi93hmy0ifscc49qzfc")))

(define-public crate-battle-net-oauth-0.1.2 (c (n "battle-net-oauth") (v "0.1.2") (d (list (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "10qy24x6bi07fb6i7qns8kcdb92n9m5c45g8vnxzsx8mhf63ic9x")))

(define-public crate-battle-net-oauth-0.1.3 (c (n "battle-net-oauth") (v "0.1.3") (d (list (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1r97q0sh7v6bwh8sd3s7cqp89ljdnas1n4gjyvvyabbdjx348l61")))

(define-public crate-battle-net-oauth-0.1.4 (c (n "battle-net-oauth") (v "0.1.4") (d (list (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "121dhlvj85fla620708pbiazx4sbmbxfgplj9psch5s2ip2zb0yv")))

(define-public crate-battle-net-oauth-0.1.5 (c (n "battle-net-oauth") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("full"))) (d #t) (k 0)))) (h "05csbm0qw6jf8d3sr2ffgl57jv46gxzfa2nnvrrsfbvzabrbw0fb") (y #t)))

(define-public crate-battle-net-oauth-0.1.6 (c (n "battle-net-oauth") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("full"))) (d #t) (k 0)))) (h "1jlpxxss9134ipyl0rg94nhwphpvsbws5p8ll5vxk211kjm8k1rv") (y #t)))

(define-public crate-battle-net-oauth-0.1.7 (c (n "battle-net-oauth") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "076vpbi2f9qz8cvp6lka82917mn4c1racy83iq74is25is92z1vz") (y #t)))

(define-public crate-battle-net-oauth-0.1.8 (c (n "battle-net-oauth") (v "0.1.8") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z42r72s3hs35aripwswbaajgb2gskp9i5d1nwkd3mb5ajabj6sp")))

(define-public crate-battle-net-oauth-0.2.0 (c (n "battle-net-oauth") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lyk9fafrfbf47lqfb0csjcjmcqh232ywjar04bdqy9niahkd4yg")))

(define-public crate-battle-net-oauth-0.2.1 (c (n "battle-net-oauth") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06bi017lr73wh4fb1r96875hdzd3p5l4pq6iasqqfxsbyfz0pm49")))

(define-public crate-battle-net-oauth-0.3.0 (c (n "battle-net-oauth") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h19ffhl0g0133160fixcl9bir6smb7czi8q8scl71j3660fv828")))

