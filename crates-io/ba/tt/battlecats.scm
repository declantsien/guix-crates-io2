(define-module (crates-io ba tt battlecats) #:use-module (crates-io))

(define-public crate-battlecats-0.1.0 (c (n "battlecats") (v "0.1.0") (d (list (d (n "find_folder") (r "^0.3.0") (d #t) (k 0)) (d (n "piston") (r "^0.36.0") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.26.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.53.0") (d #t) (k 0)) (d (n "piston2d-sprite") (r "^0.44.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.46.0") (d #t) (k 0)))) (h "0g57qp158zviswsmfc8ypzfz2hwr2dq22lg5wxri5i5a71k0p871")))

