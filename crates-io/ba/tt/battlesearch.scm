(define-module (crates-io ba tt battlesearch) #:use-module (crates-io))

(define-public crate-battlesearch-0.1.0 (c (n "battlesearch") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pikkr-annika") (r "^0.16.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0xr94w2wvdsgrkk49xyaxqm9yc486sx23r1fsakvqphw2xbjh98v")))

(define-public crate-battlesearch-0.1.1 (c (n "battlesearch") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pikkr-annika") (r "^0.16.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "00vmh9zvzvkmrgmnbd93y92g3sxl31g2r2vcigx2j4x4khd83l1w")))

(define-public crate-battlesearch-0.2.0 (c (n "battlesearch") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pikkr-annika") (r "^0.16.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1178ggfxpycgy72s7cj8fa3mdzx5i9cmq64a9v3wwh3maa29n17i")))

