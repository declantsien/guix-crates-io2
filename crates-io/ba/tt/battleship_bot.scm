(define-module (crates-io ba tt battleship_bot) #:use-module (crates-io))

(define-public crate-battleship_bot-1.0.0 (c (n "battleship_bot") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0irri22pb425b7ghn383jwdfwymgsf3m76yc5dklji2vfa741kwy") (y #t)))

(define-public crate-battleship_bot-1.1.0 (c (n "battleship_bot") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0v73kdkpjycj5qllba599q3l98pnbma1b673rmsjnlk9mpamgfbs")))

(define-public crate-battleship_bot-1.1.1 (c (n "battleship_bot") (v "1.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hqk36w8fxr5443nkd51jwmz360md7lrz4kmvjanywwvvbs96pbw")))

(define-public crate-battleship_bot-1.1.2 (c (n "battleship_bot") (v "1.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jcfwg2iz2wgnz2mf0dxriig4vq50y1270jxhf1iyray034kiimq")))

(define-public crate-battleship_bot-1.1.3 (c (n "battleship_bot") (v "1.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04g3zm3jygxif7xpmdx5cwdhfikn8acxcp14wchi48h1w4pav6r5")))

(define-public crate-battleship_bot-1.1.4 (c (n "battleship_bot") (v "1.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nji4ank14cjycs4i34cd95jnaglb7n5hwc3fv80ym05k8fz0yny")))

(define-public crate-battleship_bot-1.1.5 (c (n "battleship_bot") (v "1.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09k31vrv57prwyhsh382ws2lckahliz9lk4qhvhpxw5q64vcqga3")))

