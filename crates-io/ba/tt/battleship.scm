(define-module (crates-io ba tt battleship) #:use-module (crates-io))

(define-public crate-battleship-1.0.0 (c (n "battleship") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0x0v89y3zljvnijbfhzk3m8yxgrhs2yvn5libqdzsiqjbj2xnc18")))

(define-public crate-battleship-1.0.1 (c (n "battleship") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1qbx646x0s4z1l946x1vr6b88wpdwvrri5s8gmavr75jpsn47vpr")))

(define-public crate-battleship-1.1.0 (c (n "battleship") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0gmi33f75l0ypwlzh5xvlslihbdvy9kxvx93kvimfkqz39iqlksn")))

(define-public crate-battleship-1.2.0 (c (n "battleship") (v "1.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0kyas3lpnjb8xkpwjqjvnbr4ksqq17ffw6b4gps3w4vr6hnk7bxc")))

(define-public crate-battleship-1.3.0 (c (n "battleship") (v "1.3.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "15lk4xyg6n1maj4pzpajhgal4d9dck2ckdq82pshgkwvyz3myzzy")))

