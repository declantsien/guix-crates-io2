(define-module (crates-io ba tt battlerust) #:use-module (crates-io))

(define-public crate-battlerust-0.3.0 (c (n "battlerust") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "01lc0q2k7a1wwm8z5jcvhaw71vsm76m9g2fhjgsz0jxrpayg71d7") (f (quote (("wasm" "wasm-bindgen") ("debug"))))))

