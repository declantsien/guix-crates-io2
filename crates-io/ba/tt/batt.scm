(define-module (crates-io ba tt batt) #:use-module (crates-io))

(define-public crate-batt-0.1.0 (c (n "batt") (v "0.1.0") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.11.0") (d #t) (k 0)) (d (n "logos-derive") (r "^0.11.0") (d #t) (k 0)))) (h "1r00qsrrgq9xay9ahwlw8g65d4j044xn8m8s7f0d5yfbg07p2ddg")))

