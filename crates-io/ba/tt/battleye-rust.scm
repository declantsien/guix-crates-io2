(define-module (crates-io ba tt battleye-rust) #:use-module (crates-io))

(define-public crate-battleye-rust-1.0.0 (c (n "battleye-rust") (v "1.0.0") (d (list (d (n "crc32fast") (r "^1.3.1") (d #t) (k 0)))) (h "1n2g1307v509d0vssg5w2wl96kwk203g2kyssh933053a5bq42xp") (r "1.56")))

