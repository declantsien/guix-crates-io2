(define-module (crates-io ba tt battery-notification) #:use-module (crates-io))

(define-public crate-battery-notification-0.0.1 (c (n "battery-notification") (v "0.0.1") (d (list (d (n "battery") (r "^0.7.1") (d #t) (k 0)) (d (n "notify-rust") (r "^3.5.0") (d #t) (k 0)))) (h "1fcqyi31wmbi7574i8sz87nxjylh4wd5z8bglc29vimvnjafai13")))

(define-public crate-battery-notification-0.0.2 (c (n "battery-notification") (v "0.0.2") (d (list (d (n "battery") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "notify-rust") (r "^3.5") (d #t) (k 0)))) (h "0460fk90c23224rjxl7pf0xf8ldybkqwr00y52xydpc54v6h74w5")))

(define-public crate-battery-notification-0.0.3 (c (n "battery-notification") (v "0.0.3") (d (list (d (n "battery") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "notify-rust") (r "^3.5") (d #t) (k 0)))) (h "1m6zdnpc08x5kijvmihy0skqcx2qkhyzvr5w87a17kcaxg1z55hw")))

