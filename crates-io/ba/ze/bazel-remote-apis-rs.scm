(define-module (crates-io ba ze bazel-remote-apis-rs) #:use-module (crates-io))

(define-public crate-bazel-remote-apis-rs-0.1.0 (c (n "bazel-remote-apis-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6.2") (d #t) (k 1)))) (h "176k0hk6cpa6svr1xl3lxlh7dg8y8s60knvl5c33glp11imkqf7f")))

