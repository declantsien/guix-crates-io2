(define-module (crates-io ba ze baze64) #:use-module (crates-io))

(define-public crate-baze64-0.1.0 (c (n "baze64") (v "0.1.0") (d (list (d (n "bitreader") (r "^0.3.6") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1hfx0jqy41xyxa2sg2kgb83301zapab3dhx90r302rrp14i15rzm")))

(define-public crate-baze64-0.2.0 (c (n "baze64") (v "0.2.0") (d (list (d (n "bitreader") (r "^0.3.6") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "00yxkhx63cggfyy332qx8mnz8axqx4gl6rw93j2d54m0k9ivyvmm")))

(define-public crate-baze64-0.3.0 (c (n "baze64") (v "0.3.0") (d (list (d (n "bitreader") (r "^0.3.6") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0zg04zrgk26qcia849ldkn20z3ikzqvyvp0dv4zrgk5y6npfs3l8")))

(define-public crate-baze64-0.4.0 (c (n "baze64") (v "0.4.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0rnwyrji89gcbyq4b1hlkz6729l8ga2c0ga9dixlnzgzhn660rsm")))

(define-public crate-baze64-0.4.1 (c (n "baze64") (v "0.4.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "11rl9935mmcl41bhf0g1zm1443nrlgncvfzj73hpnhp3vjcsaks0")))

(define-public crate-baze64-0.5.0 (c (n "baze64") (v "0.5.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1valfszlbs3sw9l87qf1pqmmq61v43rcac5lfw912qrc7k5qfjy5")))

(define-public crate-baze64-0.6.0 (c (n "baze64") (v "0.6.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "195fy1vg20fkiw8zhi63v2fixrqr6rv7h75zafmygwcb1gmr5dhn")))

