(define-module (crates-io ba ze baze64-gui) #:use-module (crates-io))

(define-public crate-baze64-gui-0.1.0 (c (n "baze64-gui") (v "0.1.0") (d (list (d (n "baze64") (r "^0.3.0") (d #t) (k 0)) (d (n "eframe") (r "^0.21.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1vr1awcib8x5m74kzglqknyphmh05jpgv5mblbqjp2vw0jsqa327")))

(define-public crate-baze64-gui-0.2.0 (c (n "baze64-gui") (v "0.2.0") (d (list (d (n "baze64") (r "^0.3.0") (d #t) (k 0)) (d (n "eframe") (r "^0.21.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "14ma27sdic1s2jlq0jwdq8axxwl4g9vpgz3561w2pwd2vybsjy34")))

(define-public crate-baze64-gui-0.4.0 (c (n "baze64-gui") (v "0.4.0") (d (list (d (n "baze64") (r "^0.4.0") (d #t) (k 0)) (d (n "eframe") (r "^0.21.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1vj430xc1z4slbakp5ff3p10771acj1zdlwf8m006xmlrv1z322c")))

(define-public crate-baze64-gui-2.0.0 (c (n "baze64-gui") (v "2.0.0") (d (list (d (n "baze64") (r "^0.4.0") (d #t) (k 0)) (d (n "slint") (r "^1.2.1") (d #t) (k 0)) (d (n "slint-build") (r "^1.2.1") (d #t) (k 1)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0d4c650g9yf4sqc8dbdz07cwd8iyqxbvhks9ahszg0m8dwpgsbx2")))

(define-public crate-baze64-gui-2.0.1 (c (n "baze64-gui") (v "2.0.1") (d (list (d (n "baze64") (r "^0.4.1") (d #t) (k 0)) (d (n "slint") (r "^1.2.1") (d #t) (k 0)) (d (n "slint-build") (r "^1.2.1") (d #t) (k 1)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0n82fvrgkpigma49d4m8i81al16zysa4gwqg36gkxqladar67b9p")))

(define-public crate-baze64-gui-2.0.2 (c (n "baze64-gui") (v "2.0.2") (d (list (d (n "baze64") (r "^0.5.0") (d #t) (k 0)) (d (n "slint") (r "^1.2.1") (d #t) (k 0)) (d (n "slint-build") (r "^1.2.1") (d #t) (k 1)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "053vysk9g5bbfg36dcicky8m7g4a9ysff1iyrlmvgvy8hqsdipzx")))

(define-public crate-baze64-gui-2.1.0 (c (n "baze64-gui") (v "2.1.0") (d (list (d (n "baze64") (r "^0.6.0") (d #t) (k 0)) (d (n "slint") (r "^1.2.2") (d #t) (k 0)) (d (n "slint-build") (r "^1.2.2") (d #t) (k 1)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "1cy8zmjm93v5hrrgs86k2bw1k0ih8fm480apn9yjmjdvcb58lm2a")))

