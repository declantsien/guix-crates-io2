(define-module (crates-io ba ze bazel-bep) #:use-module (crates-io))

(define-public crate-bazel-bep-0.1.0 (c (n "bazel-bep") (v "0.1.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (d #t) (k 1)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ff08i98pc7vw2c77ryg78qcdq9w1ckbjiy0gn9ndw9wbxhq9b5v")))

(define-public crate-bazel-bep-0.2.0 (c (n "bazel-bep") (v "0.2.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (f (quote ("codegen" "prost"))) (k 0)) (d (n "tonic-build") (r "^0.10") (d #t) (k 1)))) (h "0b34bd68gw6yvj3i1z73gqmjn3x5i0n6avlwfhihxlx0y4xn5c7b") (f (quote (("server") ("default") ("client" "tonic/transport"))))))

(define-public crate-bazel-bep-0.2.1 (c (n "bazel-bep") (v "0.2.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (f (quote ("codegen" "prost"))) (k 0)) (d (n "tonic-build") (r "^0.10") (d #t) (k 1)))) (h "1wb5f7pvpkd8496iv0rphhpp4987dgff5xx2an4a3nvgz6vz2mp4") (f (quote (("server") ("default") ("client" "tonic/transport"))))))

(define-public crate-bazel-bep-0.2.2 (c (n "bazel-bep") (v "0.2.2") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (f (quote ("codegen" "prost"))) (k 0)) (d (n "tonic-build") (r "^0.10") (d #t) (k 1)))) (h "0qsz6zy2866gbagb0dw8agndybc5gqg6dcz7lvkxqdw1y9qrmlw1") (f (quote (("server") ("default") ("client" "tonic/transport"))))))

