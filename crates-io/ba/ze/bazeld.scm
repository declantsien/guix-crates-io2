(define-module (crates-io ba ze bazeld) #:use-module (crates-io))

(define-public crate-bazeld-0.1.0 (c (n "bazeld") (v "0.1.0") (d (list (d (n "clap") (r "~2.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "1jkknc2vjmfrmzilykmzqzj0kbb8fyffrl9rdkfa8z4b8sh5gy7b")))

(define-public crate-bazeld-0.1.1 (c (n "bazeld") (v "0.1.1") (d (list (d (n "clap") (r "~2.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "1ah9hi29vj63qz43528pjzj5v8q3q09p41kgkwk24jwa3hl43dka")))

