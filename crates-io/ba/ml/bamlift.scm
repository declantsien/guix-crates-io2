(define-module (crates-io ba ml bamlift) #:use-module (crates-io))

(define-public crate-bamlift-0.1.0 (c (n "bamlift") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.43.1") (d #t) (k 0)))) (h "1m68k6252v52dbf9015w45sfsd9l7mnl72hjhwl04cj6r95rmaf5") (y #t)))

(define-public crate-bamlift-0.1.1 (c (n "bamlift") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.44.1") (d #t) (k 0)))) (h "09r6kvayf7kvaw5kbikm8n2p28hp769napqn1dk45fmikc53lyzb")))

(define-public crate-bamlift-0.2.0 (c (n "bamlift") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.44.1") (d #t) (k 0)))) (h "0cmv2g4fryzhax1040wy7k8kyf18ia4iyg03ral4nbzf3dyhn9wr")))

(define-public crate-bamlift-0.3.0 (c (n "bamlift") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.44.1") (d #t) (k 0)))) (h "1l9jv5r9pv5nh115d8wjaw7km0981bi70mip0i1yalkpxj4xhcp1")))

