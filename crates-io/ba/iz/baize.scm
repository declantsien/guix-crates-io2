(define-module (crates-io ba iz baize) #:use-module (crates-io))

(define-public crate-baize-0.1.0 (c (n "baize") (v "0.1.0") (h "07lrdq3fimcd4w2qxc78ap95c4cvvgh96hbgbd290iayzf4rjgx1")))

(define-public crate-baize-0.1.1 (c (n "baize") (v "0.1.1") (h "0753rdxsq038avfia9cjiw39rf19g4ylb44mghj8il0aniqyz0ym")))

