(define-module (crates-io ba yo bayou) #:use-module (crates-io))

(define-public crate-bayou-0.1.0 (c (n "bayou") (v "0.1.0") (h "1z6fnbl29g1cxpgc15c1x9m5h6chzcgax2vr83al7i29xplgdhpp")))

(define-public crate-bayou-0.1.1 (c (n "bayou") (v "0.1.1") (h "00k02wdipj3rjx8k53slhci2pv49argvvsaz8g0wqdnfgndd4myg")))

(define-public crate-bayou-0.1.2 (c (n "bayou") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "file-rotate") (r "^0.7.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "opendal") (r "^0.39") (d #t) (k 0)) (d (n "rumqttc") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-cron-scheduler") (r "^0.9") (d #t) (k 0)))) (h "1bpxql3l3zszh0gsvis3732y6p5hr6x22syqzlf6pf36mik4zixb")))

(define-public crate-bayou-0.1.3 (c (n "bayou") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "file-rotate") (r "^0.7.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "opendal") (r "^0.39") (d #t) (k 0)) (d (n "rumqttc") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-cron-scheduler") (r "^0.9") (d #t) (k 0)))) (h "1x1z3vn1kw0919903mn1yw9h90a5bha31gdgqnwdzqxq564shxfk")))

