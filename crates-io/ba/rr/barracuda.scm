(define-module (crates-io ba rr barracuda) #:use-module (crates-io))

(define-public crate-barracuda-0.1.0 (c (n "barracuda") (v "0.1.0") (d (list (d (n "anchor-client") (r "=0.26.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "solana-sdk") (r "~1.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.4") (d #t) (k 1)))) (h "1qk8jynqb34j0lrc39z8cw6a89jggyljb51ms1ifflfpjx3s318w")))

