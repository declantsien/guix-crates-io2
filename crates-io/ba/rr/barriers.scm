(define-module (crates-io ba rr barriers) #:use-module (crates-io))

(define-public crate-barriers-0.1.0 (c (n "barriers") (v "0.1.0") (h "1lp099g42gdds18krdfwhnqnn63g13v7hv8gjarbqxl2kaagnfxj")))

(define-public crate-barriers-0.2.0 (c (n "barriers") (v "0.2.0") (h "1wgkk727jkl4kamzymc03dxm7lpwigf23cfacnl7xvpkxaw5j0s5")))

