(define-module (crates-io ba rr barrel) #:use-module (crates-io))

(define-public crate-barrel-0.0.1 (c (n "barrel") (v "0.0.1") (h "0mqprmrvhzr133my16g91rng4iirdsd8sb1gr8nspl9vmszsi9lf")))

(define-public crate-barrel-0.1.0 (c (n "barrel") (v "0.1.0") (d (list (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (o #t) (d #t) (k 0)))) (h "05gl4y4c8zw9y0k09aq4s9c0bc6gwr5l6c5w3kb4s1pvwycd8scq") (f (quote (("default" "diesel"))))))

(define-public crate-barrel-0.2.0 (c (n "barrel") (v "0.2.0") (d (list (d (n "diesel") (r ">= 1.1, < 2.0") (o #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1yw6kja300qs9d7lgish9vhj9yl2lrmjpp2kfy01lakbm376n0gj") (f (quote (("diesel-filled" "tempdir" "diesel") ("default"))))))

(define-public crate-barrel-0.2.1 (c (n "barrel") (v "0.2.1") (d (list (d (n "diesel") (r ">= 1.1, < 2.0") (o #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "12wndcq0s240lr9gaxi2rcjnxxamhsp629an2pvzx1pmfrmnfri9") (f (quote (("diesel-filled" "tempdir" "diesel") ("default"))))))

(define-public crate-barrel-0.2.2 (c (n "barrel") (v "0.2.2") (d (list (d (n "diesel") (r ">= 1.1, < 2.0") (o #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0alwnpi6b47b078v8xw82b3h7akp5vdj1qlarfzic0mq76r01r0b") (f (quote (("diesel-filled" "tempdir" "diesel") ("default"))))))

(define-public crate-barrel-0.2.3 (c (n "barrel") (v "0.2.3") (d (list (d (n "diesel") (r ">= 1.1, < 2.0") (o #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0fanw8ragn81sk7k39cvmv3fcx5z5140iy9z7xq5xpbqjjnr98yn") (f (quote (("sqlite3") ("pg") ("diesel-filled" "tempdir" "diesel") ("default")))) (y #t)))

(define-public crate-barrel-0.2.4 (c (n "barrel") (v "0.2.4") (d (list (d (n "diesel") (r ">= 1.1, < 2.0") (o #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0nzchmap4lxm4dyrm9bganvqas7mvwkpl8dwn3f16641kbh21mbm") (f (quote (("unstable") ("sqlite3") ("pg") ("diesel-filled" "tempdir" "diesel") ("default"))))))

(define-public crate-barrel-0.2.5 (c (n "barrel") (v "0.2.5") (d (list (d (n "diesel") (r ">= 1.1, < 2.0") (o #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0vqkwcp0qjvj5dipl1w1zyd3jy8j2wmnbaklksiis4wn7yrnvdj9") (f (quote (("unstable") ("sqlite3") ("pg") ("diesel-filled" "tempdir" "diesel") ("default"))))))

(define-public crate-barrel-0.2.6 (c (n "barrel") (v "0.2.6") (d (list (d (n "diesel") (r ">= 1.1, < 2.0") (o #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "06cwl0am1lx92glgklq37l7pk4b4dbbjibx7nisr1h5anqgqj3md") (f (quote (("unstable") ("sqlite3") ("pg") ("diesel-filled" "tempdir" "diesel") ("default"))))))

(define-public crate-barrel-0.2.7 (c (n "barrel") (v "0.2.7") (d (list (d (n "diesel") (r ">= 1.1, < 2.0") (o #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "01wz1l87mmgai5qcwqdlpgf1xmn6rav51q3g2cgk09n8arh1jcxi") (f (quote (("unstable") ("sqlite3") ("pg") ("diesel-filled" "tempdir" "diesel") ("default"))))))

(define-public crate-barrel-0.3.0 (c (n "barrel") (v "0.3.0") (d (list (d (n "diesel") (r ">= 1.2, < 2.0") (o #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1vpss2h3mfah1m7g1scskcbz72hmx115xachi8kg6rhk0pvqq9ya") (f (quote (("unstable") ("sqlite3") ("pg") ("diesel-filled" "tempdir" "diesel") ("default"))))))

(define-public crate-barrel-0.4.0 (c (n "barrel") (v "0.4.0") (d (list (d (n "diesel") (r ">= 1.2, < 2.0") (o #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "12i74zrni7rw39kxnz9vyxr89975c6fxfpm1qvhps76sj5yr4vsz") (f (quote (("unstable") ("sqlite3") ("pg") ("diesel-filled" "tempdir" "diesel") ("default"))))))

(define-public crate-barrel-0.4.1 (c (n "barrel") (v "0.4.1") (d (list (d (n "diesel") (r ">= 1.2, < 2.0") (o #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0b8lc8zcvm447rkpa0fxkgj0g1x1q7j3zhy07k94swyb83hm8m6y") (f (quote (("unstable") ("sqlite3") ("pg") ("diesel-filled" "tempdir" "diesel") ("default"))))))

(define-public crate-barrel-0.5.0-rc.1 (c (n "barrel") (v "0.5.0-rc.1") (d (list (d (n "diesel") (r ">= 1.2, < 2.0") (o #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0lqmdhyl0bc8ama49w8mhh32984xg1vfpvcv53svbgnx35si0i9s") (f (quote (("unstable") ("sqlite3") ("pg") ("diesel-filled" "tempdir" "diesel") ("default"))))))

(define-public crate-barrel-0.5.0 (c (n "barrel") (v "0.5.0") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1y7bwcqw8fvgd4y8q2g6npcxzifkghjvn8ia03gkjraaamlrq7q3") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempdir" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.5.1 (c (n "barrel") (v "0.5.1") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0mp1hkf69m0jvxp5zjpn4j572xjl12x402ah028yb2n533n7pcdw") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempdir" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.5.2 (c (n "barrel") (v "0.5.2") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1azbgspjdnaabyp59ifaj3kbalxikgnk7vcdrzdqj20a0bdid4i5") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempdir" "diesel_rs") ("default")))) (y #t)))

(define-public crate-barrel-0.5.3 (c (n "barrel") (v "0.5.3") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0i998jmjwspjlc8hxarg3c6iy27q7wbm4jqv9w76zyffxw0s5bnc") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempdir" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.5.4 (c (n "barrel") (v "0.5.4") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1warh0ai5nzvjz67qixzdzjhiwjrvagx5j6zfg895m38zwi2sfsz") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempdir" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.5.5 (c (n "barrel") (v "0.5.5") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0bmd6vyiqykx8gva0jd31kmhrpd30z8dkgzvyqqy83j3w2sg5x28") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempdir" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.5.6 (c (n "barrel") (v "0.5.6") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0jncvkc5pzc7gwbgcikng0mjjk25y2rfahd5vl6c1rjmjdixad0a") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempdir" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.5.7 (c (n "barrel") (v "0.5.7") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "000qc99xb2by2qw9rc78ddx7s4a87fqhks5yqmjb52qkzjrdqlw1") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempdir" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.5.8 (c (n "barrel") (v "0.5.8") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "09m7p9cj5vg8f7x20kms7hm9bgk6rv8rr89idmqnlh9x1zynd2qx") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempdir" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.6.0 (c (n "barrel") (v "0.6.0") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "17njbq1m4arjr9r78gzpkrkxbcl8ib8imh3a33bmfnx7payjgpah") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempdir" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.6.1 (c (n "barrel") (v "0.6.1") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "020di6l12blkqwix4grkc2jds49lcmsm300vq7a2a44j6lh6fx9f") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempdir" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.6.2 (c (n "barrel") (v "0.6.2") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempdir") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1bf0rh1x2bnarvzzj9izrm4g17lrkvlv6w4nw650b4x6c8l455sl") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempdir" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.6.3 (c (n "barrel") (v "0.6.3") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)))) (h "1gbkccaag6jpjlikqmvdimnhik7ihhm5zmwj7hablrcpfbm8qm8f") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempfile" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.6.4 (c (n "barrel") (v "0.6.4") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)))) (h "0dch7dzlqcydr6z6nyqp7aqw3pgchn7mh3psjbhhbfk7blykhljv") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempfile" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.6.5 (c (n "barrel") (v "0.6.5") (d (list (d (n "diesel_rs") (r ">= 1.2, < 2.0") (o #t) (k 0) (p "diesel")) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)))) (h "1qgzrfpp2acb06bbqrf5a8pjjwf3dwinq7sv2hqq0b1jn5wcjrwx") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("diesel" "tempfile" "diesel_rs") ("default"))))))

(define-public crate-barrel-0.7.0 (c (n "barrel") (v "0.7.0") (d (list (d (n "diesel_rs") (r ">=1.2, <2.0") (o #t) (k 0) (p "diesel")) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)))) (h "1nb81ij1c4r55ksp3chy7cmg34zys15qih2sxkxlx5m655cn17md") (f (quote (("unstable") ("sqlite3") ("pg") ("mysql") ("mssql") ("diesel" "tempfile" "diesel_rs") ("default"))))))

