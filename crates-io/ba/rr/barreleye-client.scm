(define-module (crates-io ba rr barreleye-client) #:use-module (crates-io))

(define-public crate-barreleye-client-0.0.1 (c (n "barreleye-client") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("clock" "std" "serde"))) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("rustls-tls" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h6yf0c3m23244rmp9byx86lyapmdd841dslgb6x3b6wsp9n0448")))

