(define-module (crates-io ba rr barr) #:use-module (crates-io))

(define-public crate-barr-0.1.0 (c (n "barr") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ry21rspx603267j76q90n6kxsnb3qkcw40c3llsnarw6fma3d70")))

(define-public crate-barr-0.1.1 (c (n "barr") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "166nrdp6ngiqv0ynzrrrwy15d0nrkki44hj1h7a8m8kchpi3n77m")))

