(define-module (crates-io ba ir bairstow-rs) #:use-module (crates-io))

(define-public crate-bairstow-rs-0.1.0 (c (n "bairstow-rs") (v "0.1.0") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1z6ckyi9gzgb63jh2bbnxzk749xvc2d7svxv681f8c6hvgdhlc7g")))

