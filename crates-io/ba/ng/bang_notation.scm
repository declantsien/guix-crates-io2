(define-module (crates-io ba ng bang_notation) #:use-module (crates-io))

(define-public crate-bang_notation-1.0.0 (c (n "bang_notation") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "13z4qng5h5yy7zna5pgjb1nr2a0ikini4igwgv6pcl5fml99c04c")))

