(define-module (crates-io ba e2 bae2) #:use-module (crates-io))

(define-public crate-bae2-1.0.0 (c (n "bae2") (v "1.0.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1fvcxg254lgzvpavgvkwhdvwfdj0vb2qydxw9hlyn38r70xdsh7x")))

