(define-module (crates-io ba mb bamboo) #:use-module (crates-io))

(define-public crate-bamboo-0.1.0 (c (n "bamboo") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0qhdhvhijn32iksndx4cw2fsb0jad2670c69p0hq9aqvps1x46px")))

