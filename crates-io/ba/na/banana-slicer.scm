(define-module (crates-io ba na banana-slicer) #:use-module (crates-io))

(define-public crate-banana-slicer-0.1.0 (c (n "banana-slicer") (v "0.1.0") (h "1057dz703x4q8nnrb2c68xbl5r2my2pcq83g6alva7pakbw0q9p9")))

(define-public crate-banana-slicer-0.1.1 (c (n "banana-slicer") (v "0.1.1") (h "14c8cwq3mk61jxhsk2x2p1s0bdsxxsf2639w9w8rh12ir4bfk65f")))

