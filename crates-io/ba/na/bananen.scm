(define-module (crates-io ba na bananen) #:use-module (crates-io))

(define-public crate-bananen-0.0.3-alpha (c (n "bananen") (v "0.0.3-alpha") (d (list (d (n "inline_colorization") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1msc15p435652vczp932fzpcaahf9m6dhf7571gwqs31qa882gkx") (y #t)))

(define-public crate-bananen-0.1.7-beta (c (n "bananen") (v "0.1.7-beta") (d (list (d (n "inline_colorization") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0cdcf998mv4d8ml7w4llcqmwl5lyyd5ddydcri88zd6kqlxl0azx") (y #t)))

(define-public crate-bananen-0.1.7-beta1 (c (n "bananen") (v "0.1.7-beta1") (d (list (d (n "inline_colorization") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1ckbmp2h81lvyxhiaam0b78ds11iqg7bkf8w3g7qw8kcndk8jzs8") (y #t)))

(define-public crate-bananen-0.1.7-beta2 (c (n "bananen") (v "0.1.7-beta2") (d (list (d (n "inline_colorization") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1xn863c019wcgbiavi1226caax2pnvzwgq4c8yf3i1i2zvvxzahv")))

(define-public crate-bananen-0.1.8 (c (n "bananen") (v "0.1.8") (d (list (d (n "inline_colorization") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "11nn62s55l3zcl1l66pjl611bv4qm1wmlvmga30j8vb8sgm2xfw0")))

