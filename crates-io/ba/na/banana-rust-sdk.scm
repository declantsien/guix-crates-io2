(define-module (crates-io ba na banana-rust-sdk) #:use-module (crates-io))

(define-public crate-banana-rust-sdk-0.1.0 (c (n "banana-rust-sdk") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "053l4kzvflpgnqsbarry5vnc4c01xizghrccr3dllrw2m49k0yab")))

