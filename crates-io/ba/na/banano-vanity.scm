(define-module (crates-io ba na banano-vanity) #:use-module (crates-io))

(define-public crate-banano-vanity-0.4.2 (c (n "banano-vanity") (v "0.4.2") (d (list (d (n "blake2") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "digest") (r "^0.7.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^0.6.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1.43") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "ocl") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "ocl-core") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1bv7p6lbxvky003ymi0ld504igk1f9121hj1y8rvkwsfvj75s0qz") (f (quote (("gpu" "ocl" "ocl-core"))))))

