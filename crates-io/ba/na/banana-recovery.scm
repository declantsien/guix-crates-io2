(define-module (crates-io ba na banana-recovery) #:use-module (crates-io))

(define-public crate-banana-recovery-0.1.0 (c (n "banana-recovery") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.1") (f (quote ("alloc"))) (k 0)) (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (k 0)) (d (n "scrypt") (r "^0.10.0") (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.89") (f (quote ("alloc"))) (k 0)) (d (n "sha2") (r "^0.10.6") (k 0)) (d (n "xsalsa20poly1305") (r "^0.9.0") (f (quote ("alloc"))) (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 0)))) (h "1yrbfnr8v1lfvzqdnvjyn3zc7d41ds4dm7cvkrw1f0hyb52hgswh") (f (quote (("std") ("default" "std"))))))

