(define-module (crates-io ba na banana) #:use-module (crates-io))

(define-public crate-banana-0.0.1 (c (n "banana") (v "0.0.1") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0kl424h9czfww2av6ja7z9vz0asg06krlg9gx1zchhdkbnk93whd")))

(define-public crate-banana-0.0.2 (c (n "banana") (v "0.0.2") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1p0ckwylbnxkx04f4vqy1prbxgh8dmbp9mvf1zqlj45gbriaz68g")))

