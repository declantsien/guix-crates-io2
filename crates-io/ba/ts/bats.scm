(define-module (crates-io ba ts bats) #:use-module (crates-io))

(define-public crate-bats-0.1.0 (c (n "bats") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.3") (d #t) (k 0)) (d (n "crossterm_terminal") (r "^0.3") (d #t) (k 0)) (d (n "gumdrop") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0a1488p0v89mchrxk1qlvimppwrqxi3wqs949k58pivx3kc2yrfb")))

(define-public crate-bats-0.1.1 (c (n "bats") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.3") (d #t) (k 0)) (d (n "crossterm_terminal") (r "^0.3") (d #t) (k 0)) (d (n "gumdrop") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "19ffh9zgcyimqbmacjsp2q1j3g2gihrdsf2xpxmlxibak569pb9a")))

(define-public crate-bats-0.10.31 (c (n "bats") (v "0.10.31") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.12") (d #t) (k 0)) (d (n "gumdrop") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1ff9djm8ll99gly6xyncn2514jkkk647dsxflpnfc7b9i7p85lwf")))

