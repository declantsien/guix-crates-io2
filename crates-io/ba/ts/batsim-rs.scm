(define-module (crates-io ba ts batsim-rs) #:use-module (crates-io))

(define-public crate-batsim-rs-0.1.0 (c (n "batsim-rs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zmq") (r "^0.8") (d #t) (k 0)))) (h "0kkxwbyz53cdg39412ffb7m7lr5ajcmfi1piimb3arr8qzbd1iq4")))

(define-public crate-batsim-rs-0.2.0 (c (n "batsim-rs") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zmq") (r "^0.8") (d #t) (k 0)))) (h "1dibnwnwbzfvfda6k3mv3bxc0k4qg9x72vp7jv9ajfpxc8dddp49")))

