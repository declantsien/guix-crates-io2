(define-module (crates-io ba co bacon_rajan_cc) #:use-module (crates-io))

(define-public crate-bacon_rajan_cc-0.1.0 (c (n "bacon_rajan_cc") (v "0.1.0") (h "0cf0349xgjf9zxi9np851z6w0sjmp43h369imzn2iy9hgxl46p2p")))

(define-public crate-bacon_rajan_cc-0.2.0 (c (n "bacon_rajan_cc") (v "0.2.0") (h "09zmagcv5lgclv8wy8znhhs2kkysyfdrnqvf23m70j5rgfw2wr8z")))

(define-public crate-bacon_rajan_cc-0.2.1 (c (n "bacon_rajan_cc") (v "0.2.1") (h "0wp9vz5zfbrhwky740m3hgfl490jawkqgfifj51kjp3d941vh76d")))

(define-public crate-bacon_rajan_cc-0.2.2 (c (n "bacon_rajan_cc") (v "0.2.2") (h "0gzqgcr3ld2h8wyfgka0cskijp5fykq3a06s2n36af6j4wd1fwdr")))

(define-public crate-bacon_rajan_cc-0.2.3 (c (n "bacon_rajan_cc") (v "0.2.3") (h "0b6zwyk964b9m9l8cidd02ajs0i1b89n4azlfa5znf05lpzwmllj")))

(define-public crate-bacon_rajan_cc-0.2.4 (c (n "bacon_rajan_cc") (v "0.2.4") (h "1v0zwcz02fvm173hs8i20c136jid9vs5lwh6kgf1d0isf9p9snz8")))

(define-public crate-bacon_rajan_cc-0.3.0 (c (n "bacon_rajan_cc") (v "0.3.0") (h "0jpp0mlh6zbh9wqmnnpjrl8z3flzalzay66mlgpp9p7asd0388kn")))

(define-public crate-bacon_rajan_cc-0.3.1 (c (n "bacon_rajan_cc") (v "0.3.1") (h "0c9l46rwxj1rcnzjvlcfj1qqkq5w5srrhfvpqsrmp7fga8wmdrl4")))

(define-public crate-bacon_rajan_cc-0.3.2 (c (n "bacon_rajan_cc") (v "0.3.2") (h "0b0i49hvsma5lqk14jhr9ry7x1v6ngy2hq7rdwmwkxdvbax0fpcx")))

(define-public crate-bacon_rajan_cc-0.4.0 (c (n "bacon_rajan_cc") (v "0.4.0") (h "1xakk541frpcxvgangmf44hb6lvknbgis8dx7bjknw0zqrl4ix07")))

