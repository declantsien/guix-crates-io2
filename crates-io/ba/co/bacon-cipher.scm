(define-module (crates-io ba co bacon-cipher) #:use-module (crates-io))

(define-public crate-bacon-cipher-0.1.0 (c (n "bacon-cipher") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.22") (o #t) (d #t) (k 0)))) (h "0mq99bncvjb00qbhpyqkn3d4cl2sdpby15i4i2k07fsiyrhbr3v7") (f (quote (("extended-steganography" "html5ever"))))))

(define-public crate-bacon-cipher-0.2.0 (c (n "bacon-cipher") (v "0.2.0") (d (list (d (n "html5ever") (r "^0.22") (o #t) (d #t) (k 0)))) (h "0gmdrq0cqsd7bzjq7d4xw2m99j3wv767pvlhh2i5d9wp04cs71ra") (f (quote (("extended-steganography" "html5ever"))))))

