(define-module (crates-io ba co bacon-sci-1) #:use-module (crates-io))

(define-public crate-bacon-sci-1-0.11.0 (c (n "bacon-sci-1") (v "0.11.0") (d (list (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "num-complex") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ml0y34glnafs155z3axxsrw8kfwr2nr3q9fcpqr4nr36ly0mxla") (f (quote (("serialize" "serde"))))))

