(define-module (crates-io ba rk barkup) #:use-module (crates-io))

(define-public crate-barkup-0.0.0 (c (n "barkup") (v "0.0.0") (h "03nmp8j10yip50vyhji19c4psmgr9w0jvpcbwlw4f3zsdv2d6jfv")))

(define-public crate-barkup-0.0.1 (c (n "barkup") (v "0.0.1") (h "0d2n8hlbp0aryqj9vdk42j3zcdr1yqgqmkskqzm1wmjb21dy9ph5")))

(define-public crate-barkup-0.0.2 (c (n "barkup") (v "0.0.2") (h "1x08cmklprhzxc0ka7vyii1sz2xcpm7n51gf8ba4p0lypyhhmy1y")))

