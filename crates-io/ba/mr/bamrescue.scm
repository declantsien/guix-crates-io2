(define-module (crates-io ba mr bamrescue) #:use-module (crates-io))

(define-public crate-bamrescue-0.0.1 (c (n "bamrescue") (v "0.0.1") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1f943yx83g5wlb9w366y6g134dpjxx4n8v5avx204y2j9ay0h43c")))

(define-public crate-bamrescue-0.1.0 (c (n "bamrescue") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "crc") (r "^1.4") (d #t) (k 0)) (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "inflate") (r "^0.2") (d #t) (k 0)) (d (n "number_prefix") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "slog") (r "^2.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.0") (d #t) (k 0)))) (h "1giv82zrq47qi7p54s0mj7yyn70gcr9qfz0bgw9zzh8vcc91720v")))

(define-public crate-bamrescue-0.3.0 (c (n "bamrescue") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "docopt") (r "^1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "number_prefix") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ab1cxxjrpblxaqi5jjz60478k33iw91d5dc2qb1ppjh2yd9clgk")))

