(define-module (crates-io ba se base62) #:use-module (crates-io))

(define-public crate-base62-0.1.0 (c (n "base62") (v "0.1.0") (h "1381jv69dx4lyc1bky0fgkh7wbl1j7rng17wwm0n9fvwscg2b71z")))

(define-public crate-base62-0.2.0 (c (n "base62") (v "0.2.0") (h "0k61wyfchpq1rbbf8h9h4q86fci946yz2gygiy6bi94bz2zmjyyd")))

(define-public crate-base62-0.3.0 (c (n "base62") (v "0.3.0") (h "1nkhwc3nrcwgvilpq5ksl7qq5ks0y34rgb976i34gphrcy1s3k5z")))

(define-public crate-base62-1.0.0 (c (n "base62") (v "1.0.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0d1840m0wd1mllvnznias7w85c43d5bp47d3kz2slfw6rcxhljzg")))

(define-public crate-base62-1.1.0 (c (n "base62") (v "1.1.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0p77qiwgg6h32mxfl66wraingfci8xrmm5c1h5dsaia0qwmi3cs5")))

(define-public crate-base62-1.1.1 (c (n "base62") (v "1.1.1") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "18c4aqayrzlw37fy7l3aix8v6vfqnymk1igd3nc027w0i9cznn9k")))

(define-public crate-base62-1.1.2 (c (n "base62") (v "1.1.2") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0cl9lch4h9ymq65za2028zimflzazaz8vjccjy8h2283kwvyrf8f")))

(define-public crate-base62-1.1.3 (c (n "base62") (v "1.1.3") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "02x0cmsp83451si8ayggw57ycsrdqlqc2hb7r25dgywc075y2yxh")))

(define-public crate-base62-1.1.4 (c (n "base62") (v "1.1.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0h1vh7z3mv3y803hip3bix29w4vv0zibv3q71ns6s6k9ssl9l9f7")))

(define-public crate-base62-1.1.5 (c (n "base62") (v "1.1.5") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1arl4zkg90mv2lfazmbv8p6mzmdzabcivhi7jchwsz9i2kgavz1m")))

(define-public crate-base62-2.0.0 (c (n "base62") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1p2id7m41cynfqn08y8h9f3q1qav0i782mbsrwspd4g9l1kr4b7c")))

(define-public crate-base62-2.0.1 (c (n "base62") (v "2.0.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1sgn5530rh8m0y0ps8ql8ndvcdbkmasfazgmhx1nlqin8j65936r")))

(define-public crate-base62-2.0.2 (c (n "base62") (v "2.0.2") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "14l9l2rdngpqfi5y2s2gfk18z20mcc8cn9v11rzysra6qy7yyygq")))

