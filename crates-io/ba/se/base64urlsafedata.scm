(define-module (crates-io ba se base64urlsafedata) #:use-module (crates-io))

(define-public crate-base64urlsafedata-0.1.0 (c (n "base64urlsafedata") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10qjy0mism3yrc5ih8jnqbdryiahn7r9vzcxp9fhfd1dam6c905h")))

(define-public crate-base64urlsafedata-0.1.1 (c (n "base64urlsafedata") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bwdv8wbavmjjk3c3fl5ai80cxfx0qs150flhnx2512wyb1l08yh")))

(define-public crate-base64urlsafedata-0.1.2 (c (n "base64urlsafedata") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mn2lyfn3ixmfvj06zb59i6wh073slywvnvn03h9bklw4lv79inv")))

(define-public crate-base64urlsafedata-0.1.3 (c (n "base64urlsafedata") (v "0.1.3") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "110kfb0jsqb6v54p4bhmvp2crza502g3nik7aafhl4klpc5d7cqq")))

(define-public crate-base64urlsafedata-0.5.0 (c (n "base64urlsafedata") (v "0.5.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor_2") (r "^0.12.0-dev") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1izhcay5n5h6zj6b0nps9zdb7q3wxfnm8x4d0skyzlawvx78jmhs") (r "1.70.0")))

