(define-module (crates-io ba se base_crate) #:use-module (crates-io))

(define-public crate-base_crate-0.1.0 (c (n "base_crate") (v "0.1.0") (h "0w9cyk3kvcw96b6yvqnmb2ncrvf6y02a37fyd5f4p610b38228ps")))

(define-public crate-base_crate-0.2.0 (c (n "base_crate") (v "0.2.0") (h "04g2arqf137x4f9gr4q1qg4xapr87iiwqkva84c2zq3j0ff2vli2")))

(define-public crate-base_crate-0.3.0 (c (n "base_crate") (v "0.3.0") (h "1hs6q7kvpr9l40bb9wma4pxsdzrb98c1bnspkz9hj9k5cd81mbss")))

(define-public crate-base_crate-0.4.0 (c (n "base_crate") (v "0.4.0") (h "1c49kn4gqc2q0dmj9gp6a6pajdf06dbcrhr5nl3ws3nlv769zqcl")))

