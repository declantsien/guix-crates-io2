(define-module (crates-io ba se base1112031) #:use-module (crates-io))

(define-public crate-base1112031-0.1.0 (c (n "base1112031") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1j6p325sz0mcmkggcng0hi8ag3yma82rq6f2ichmfxq2jxmhlg34")))

(define-public crate-base1112031-0.1.1 (c (n "base1112031") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)))) (h "1ds7035dpq7dhrncsn4sfy2bvq49d1mrllmnp3rx7kbi6dcrv9w4")))

(define-public crate-base1112031-0.1.2 (c (n "base1112031") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)))) (h "1rmk2pgq1ybi6h2h6vc30r4akwpdppml5ksknnf5fhszh3ndngiv")))

(define-public crate-base1112031-0.1.3 (c (n "base1112031") (v "0.1.3") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)))) (h "0lvxhcrw4bhiszxiim18cnwygyjjx5gp5llsfnvl6g5casj64qy5")))

