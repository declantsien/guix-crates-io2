(define-module (crates-io ba se base64-easy) #:use-module (crates-io))

(define-public crate-base64-easy-0.1.0 (c (n "base64-easy") (v "0.1.0") (h "0kz01hxqx749kmqjmxhkmfkgw9jic0glz12krvgx6sxip6hy55fk")))

(define-public crate-base64-easy-0.1.1 (c (n "base64-easy") (v "0.1.1") (h "05c1kq7x5jg4xl2j67sq6ba8zc300i42c3fg3rzg6nb8bfl11jr1")))

