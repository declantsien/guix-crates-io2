(define-module (crates-io ba se base64-rs) #:use-module (crates-io))

(define-public crate-base64-rs-0.1.0 (c (n "base64-rs") (v "0.1.0") (h "0f19dkmkh97gsjy5k423alxgypfw5axzwd9s9ch7al5d7avklbc3")))

(define-public crate-base64-rs-0.1.1 (c (n "base64-rs") (v "0.1.1") (h "0q60cn3mg7lw9wyisbiyjl6skp12rzh9b6qz7jcj0zs71vb72wsw")))

