(define-module (crates-io ba se baseline) #:use-module (crates-io))

(define-public crate-baseline-0.1.0 (c (n "baseline") (v "0.1.0") (d (list (d (n "smol_str") (r "^0.1") (d #t) (k 0)))) (h "06w0hzy4sl32yh3mgk6gi35wcc1rkza4znypsrx985m0c2qlf8a6")))

(define-public crate-baseline-0.1.1 (c (n "baseline") (v "0.1.1") (d (list (d (n "smol_str") (r "^0.1") (d #t) (k 0)))) (h "00q3inl1879x0p7rgylcvz08ssasxjhy75jlpvq8zz5dg20707h7")))

(define-public crate-baseline-0.1.3 (c (n "baseline") (v "0.1.3") (d (list (d (n "rust_decimal") (r "^1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1") (d #t) (k 0)) (d (n "smol_str") (r "^0.1") (d #t) (k 0)))) (h "1fmd94zdh8afm700ajsg6w3xrr8mf6w69iavyyv5saybqc3spzg3")))

(define-public crate-baseline-0.1.4 (c (n "baseline") (v "0.1.4") (d (list (d (n "rust_decimal") (r "^1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1") (d #t) (k 0)) (d (n "smol_str") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1v1ympp80qzbxlay33f2rpfqmaiqnd7v5illagcb8v3vamkm46r7")))

(define-public crate-baseline-0.2.0-alpha.1 (c (n "baseline") (v "0.2.0-alpha.1") (d (list (d (n "rust_decimal") (r "^1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1") (d #t) (k 0)) (d (n "smol_str") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1rw2d7xfpy1144c7z4k0i5609zp1lp4bbvdr99r07ic5vxv4r7ij")))

(define-public crate-baseline-0.2.0 (c (n "baseline") (v "0.2.0") (d (list (d (n "rust_decimal") (r "^1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1") (d #t) (k 0)) (d (n "smol_str") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1nb9vbbc3svlp90c3h4nlbm302hhpj0i14wrvgipil2bgc443rj4")))

