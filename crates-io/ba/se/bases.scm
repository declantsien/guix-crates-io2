(define-module (crates-io ba se bases) #:use-module (crates-io))

(define-public crate-bases-0.1.0 (c (n "bases") (v "0.1.0") (h "0fsw3qh5vsg0ls0yzfipc7hqax2rnnnrvdj8hh87isd75yl2vghf")))

(define-public crate-bases-0.2.0 (c (n "bases") (v "0.2.0") (h "1di8xw1f9xlkgr1nd6ksqpyqx2n68msl3fdykfrp5iibn5ip7q7v")))

(define-public crate-bases-0.2.1 (c (n "bases") (v "0.2.1") (h "0ap47jq6fv174n8rvlhz34ilxvw0mlzf6zzw2xhvlnb7fwjghj0r")))

(define-public crate-bases-0.2.2 (c (n "bases") (v "0.2.2") (h "19qxhj3dsldc23d5y7ihfzp2qqfipifykghcv3f0c3zj95c4v823")))

(define-public crate-bases-0.2.3 (c (n "bases") (v "0.2.3") (h "1yb60anjjyy5ra70si6pjwp2qn5q0lbnkwwcvd9bwyppv8fbzp4r")))

(define-public crate-bases-0.2.4 (c (n "bases") (v "0.2.4") (h "0ndlbm3m39qy2rarngpw1zv4arjvrhcghr17h6y2yppjkdmr8jix")))

(define-public crate-bases-0.2.5 (c (n "bases") (v "0.2.5") (h "1m1njrfq5s74mc1i573w6v3rgxma6vaarav7ghh6fd6rzvjjalr1")))

(define-public crate-bases-0.2.6 (c (n "bases") (v "0.2.6") (h "183vdrnviql4kk2amaa25xc6dqv6irym5nfbswpha5268v6ii06d")))

