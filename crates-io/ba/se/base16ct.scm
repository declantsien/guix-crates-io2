(define-module (crates-io ba se base16ct) #:use-module (crates-io))

(define-public crate-base16ct-0.0.0 (c (n "base16ct") (v "0.0.0") (h "0hkgkn6f3f400581hlm4mzqgi3l8pp03qs8sikdla337qld85kwv") (y #t)))

(define-public crate-base16ct-0.1.0 (c (n "base16ct") (v "0.1.0") (h "1v99jmhbssnqrh6dh67jpbzjdin02xbhv5afcpiy995g0220zkvf") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-base16ct-0.1.1 (c (n "base16ct") (v "0.1.1") (h "1klccxr7igf73wpi0x3asjd8n0xjg0v6a7vxgvfk5ybvgh1hd6il") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-base16ct-0.2.0 (c (n "base16ct") (v "0.2.0") (h "1kylrjhdzk7qpknrvlphw8ywdnvvg39dizw9622w3wk5xba04zsc") (f (quote (("std" "alloc") ("alloc")))) (r "1.60")))

