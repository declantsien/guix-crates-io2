(define-module (crates-io ba se base16-rs) #:use-module (crates-io))

(define-public crate-base16-rs-0.1.0 (c (n "base16-rs") (v "0.1.0") (h "1byifr9qgas40gal8awpjxydm0qzpfms9bkhmpx45lcvhcd3awkk")))

(define-public crate-base16-rs-0.1.1 (c (n "base16-rs") (v "0.1.1") (h "125affyx4hsl8hvm2qb75a04g6k7x45sa87qvxlzzxnfbl7rzkj2")))

