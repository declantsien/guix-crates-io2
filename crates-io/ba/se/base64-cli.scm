(define-module (crates-io ba se base64-cli) #:use-module (crates-io))

(define-public crate-base64-cli-0.1.0 (c (n "base64-cli") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.19") (d #t) (k 1)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inline-c") (r "^0.1") (d #t) (k 2)))) (h "0r68mza5m04xdlzk7nx5a55gr3a3qviqvv088qr635q3jbj38i5k")))

(define-public crate-base64-cli-0.1.1 (c (n "base64-cli") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.19") (d #t) (k 1)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inline-c") (r "^0.1") (d #t) (k 2)))) (h "0dcv0j4kk2m28jd319slryfdifkfsjjzk75xylyj8rys9q48abnk")))

(define-public crate-base64-cli-0.1.2 (c (n "base64-cli") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "cbindgen") (r "^0.21") (d #t) (k 1)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inline-c") (r "^0.1") (d #t) (k 2)))) (h "18nfas7al47y6iin6jy6r2jalvscg0gh060qc7gak489fjwawhrd")))

(define-public crate-base64-cli-0.1.3 (c (n "base64-cli") (v "0.1.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "cbindgen") (r "^0.23") (d #t) (k 1)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inline-c") (r "^0.1") (d #t) (k 2)))) (h "01bzjdz84v6n54hmhah1rqqdjq4wn1c0ckfb4997n6bg98dl3f45")))

(define-public crate-base64-cli-0.1.4 (c (n "base64-cli") (v "0.1.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "cbindgen") (r "^0.24") (d #t) (k 1)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inline-c") (r "^0.1") (d #t) (k 2)))) (h "0zrxy9kssarp7xb77jlx7nqjxhrrqygd5v8b0fpld3lk8xya5q99")))

(define-public crate-base64-cli-0.1.5 (c (n "base64-cli") (v "0.1.5") (d (list (d (n "base64") (r "^0.20") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inline-c") (r "^0.1") (d #t) (k 2)) (d (n "cbindgen") (r "^0.24") (d #t) (k 1)))) (h "0kn3qr622kwyq55900hr7jaa5qfwzy040cnni0m41k6mmwq9v6r2")))

