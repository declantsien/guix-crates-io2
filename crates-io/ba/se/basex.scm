(define-module (crates-io ba se basex) #:use-module (crates-io))

(define-public crate-basex-0.1.0 (c (n "basex") (v "0.1.0") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.9.0") (d #t) (k 2)))) (h "07l49ccqr2cp43s8k4ynjr0aj5i1a71nmn33qdvmcqr3m5w633wy")))

(define-public crate-basex-0.2.0 (c (n "basex") (v "0.2.0") (d (list (d (n "circbuf") (r "^0.2.0") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.9.0") (d #t) (k 2)))) (h "0r9ndsp4cky0wy11sw1j73zpafm2lxr2njlfdpgi1bdr8fa6kyl8")))

(define-public crate-basex-0.3.0 (c (n "basex") (v "0.3.0") (d (list (d (n "circbuf") (r "^0.2.0") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.9.0") (d #t) (k 2)))) (h "0g1823njdr8xlp1lfgwhwd9nyfwxp9lavjbnrszmf92ijfmmcf5q")))

(define-public crate-basex-0.4.0 (c (n "basex") (v "0.4.0") (d (list (d (n "circbuf") (r "^0.2.0") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.9.0") (d #t) (k 2)))) (h "1901sn46df0jmln4krrhiw19nwrhzgxy4qd5rpd01flvkwninbbb")))

(define-public crate-basex-0.5.0 (c (n "basex") (v "0.5.0") (d (list (d (n "circbuf") (r "^0.2.0") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.9.0") (d #t) (k 2)) (d (n "test-case") (r "^1.2.1") (d #t) (k 2)))) (h "1gdrk04sdblbf0qkg0bl9f3z0sbn2vr1gksy18dhz9m9rdbahcxf")))

(define-public crate-basex-0.6.0 (c (n "basex") (v "0.6.0") (d (list (d (n "circbuf") (r "^0.2.0") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.9.0") (d #t) (k 2)) (d (n "test-case") (r "^1.2.1") (d #t) (k 2)))) (h "0mjhfx8z6k8af55rlyirkq2jgnv2ljjphr8dr8sf1jsih7yapm9f")))

(define-public crate-basex-0.7.0 (c (n "basex") (v "0.7.0") (d (list (d (n "circbuf") (r "^0.2.0") (d #t) (k 2)) (d (n "matches") (r "<=0.1.9, >=0.1.0") (d #t) (k 2)) (d (n "md5") (r "<=0.7.0, >=0.3.0") (d #t) (k 0)) (d (n "rust-embed") (r "<7, >=6.3.0") (d #t) (k 2)) (d (n "test-case") (r "<2, >=0.3.2") (d #t) (k 2)))) (h "1zpfd6hgbqlswyxs5yzs6lh2aniqi0i4826q4iii1bwi8ifxiwq2")))

