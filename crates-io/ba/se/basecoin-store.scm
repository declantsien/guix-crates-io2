(define-module (crates-io ba se basecoin-store) #:use-module (crates-io))

(define-public crate-basecoin-store-0.1.0 (c (n "basecoin-store") (v "0.1.0") (d (list (d (n "displaydoc") (r "^0.2") (k 0)) (d (n "ics23") (r "^0.11") (f (quote ("host-functions"))) (k 0)) (d (n "prost") (r "^0.12") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "tendermint") (r "^0.35") (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "1iwswmhqqpasjsasqqv3dvij15xql0rdl07fiknd1gf56lp2v9ab")))

