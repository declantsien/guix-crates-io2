(define-module (crates-io ba se base64-serde) #:use-module (crates-io))

(define-public crate-base64-serde-0.1.0 (c (n "base64-serde") (v "0.1.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 2)))) (h "07yzwf4s9k69x3q0rfj71g2v4lg0clg6vrx99qwp5a5rpqvliy48")))

(define-public crate-base64-serde-0.1.1 (c (n "base64-serde") (v "0.1.1") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 2)))) (h "1362ah0h81njfp8rmkm390m38rc7s6ssf4szmgnah62iawsib658")))

(define-public crate-base64-serde-0.2.0 (c (n "base64-serde") (v "0.2.0") (d (list (d (n "base64") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 2)))) (h "1gzkwl8nq3sf74k6n3ig275dmgm7cqqrrwjmk8ywx8jlkx44xgh4")))

(define-public crate-base64-serde-0.3.0 (c (n "base64-serde") (v "0.3.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 2)))) (h "0w6gnx3gakplxzh7cbi65njdvh248nkvy5iix76nsbjywcimj449")))

(define-public crate-base64-serde-0.3.1 (c (n "base64-serde") (v "0.3.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 2)))) (h "11cl5vgdnxj9ndhvkxpap0j8xwawirf1r5k3aflkma1rf4kmlsig")))

(define-public crate-base64-serde-0.3.2 (c (n "base64-serde") (v "0.3.2") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 2)))) (h "007rjb22lgc03kva6xpyx5wh15pj62rqdahrbkazln4z3w6bijqx")))

(define-public crate-base64-serde-0.4.0 (c (n "base64-serde") (v "0.4.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 2)))) (h "13fjhwwhil2nbwga4mszvjxbk9c5rvgzkyv88s1ws7kh9v0ai5x1")))

(define-public crate-base64-serde-0.5.0 (c (n "base64-serde") (v "0.5.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "1fmnlx107lj53yw4cbpb0jyx6bvijv82zpi4405waljpglg6i3r2")))

(define-public crate-base64-serde-0.5.1 (c (n "base64-serde") (v "0.5.1") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "01c5idimd1vk4jb3rld25z2siw1bxsq4gl241gzi842hmwrk21qm")))

(define-public crate-base64-serde-0.6.0 (c (n "base64-serde") (v "0.6.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "1gnzs23hz0bizl2dn4z13xsvvi9yy202mih9iccqwdrynn6f6y5f")))

(define-public crate-base64-serde-0.6.1 (c (n "base64-serde") (v "0.6.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "064anl85g69ckl0ni2sbxsg8vn8xd6zqxcmxq33h60wk18z4x5if")))

(define-public crate-base64-serde-0.7.0-beta.1 (c (n "base64-serde") (v "0.7.0-beta.1") (d (list (d (n "base64") (r "^0.21.0-beta.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "03yxypyg7k66fr7sr8dkpp226lj1kd5qvmkxkp3fbxjrni47g16d") (r "1.57.0")))

(define-public crate-base64-serde-0.7.0-beta.2 (c (n "base64-serde") (v "0.7.0-beta.2") (d (list (d (n "base64") (r "^0.21.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0xn99jy24s28j3mkwjhp56mzwis7viirc98z0pkx6jq1m7c61m3v") (r "1.57.0")))

(define-public crate-base64-serde-0.7.0-rc.1 (c (n "base64-serde") (v "0.7.0-rc.1") (d (list (d (n "base64") (r "^0.21.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "1h5ccd12zi4l56idzmk7zqk2jg8bz90n94aj59s9ywxa738bgc4a") (r "1.57.0")))

(define-public crate-base64-serde-0.7.0 (c (n "base64-serde") (v "0.7.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "1iz3rcb544z4ndfvml94d4byzfygkjrz235gkajbx9bnvvsqsdms") (r "1.57.0")))

