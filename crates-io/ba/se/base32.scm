(define-module (crates-io ba se base32) #:use-module (crates-io))

(define-public crate-base32-0.1.1 (c (n "base32") (v "0.1.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (d #t) (k 0)))) (h "06mbc07x4cjs46h6acg0cr8yiym6i3yaf53gwijdhh6fz61p9mjj")))

(define-public crate-base32-0.2.0 (c (n "base32") (v "0.2.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)))) (h "07x357xibv14vqmcwbs90vhvdxzvhz82bx78jnm070x285kv7lk0")))

(define-public crate-base32-0.2.1 (c (n "base32") (v "0.2.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)))) (h "02082l4pask34bmbdclq5ymgwj2j5r7qm2yyqn2y6madrb9j9l59")))

(define-public crate-base32-0.2.2 (c (n "base32") (v "0.2.2") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "14da1pz4f6x60i3118x6kfin5mwi5v175da8ns31q1iwanp373i9")))

(define-public crate-base32-0.2.3 (c (n "base32") (v "0.2.3") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "08ik2v370zfc6ma3pwr4wxpjq3h44wcdlkrf144nsd9w01cv47gs")))

(define-public crate-base32-0.3.0 (c (n "base32") (v "0.3.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "029yh0rp0fmnqaw8skqnpck14gd3w7pbbkqzdyv93h9rmcda3204")))

(define-public crate-base32-0.3.1 (c (n "base32") (v "0.3.1") (d (list (d (n "quickcheck") (r "^0.2.24") (d #t) (k 2)) (d (n "rand") (r "^0.3.13") (d #t) (k 2)))) (h "132dg7z5cy1lkj88w3m9xdr83pbs02q8dila1m0z07fn8sx0b5hv")))

(define-public crate-base32-0.4.0 (c (n "base32") (v "0.4.0") (d (list (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "1ykwx8jhksqxghfgyw2pzikzjf4n9wqm1x2ww5wqyn68ssf6dki3")))

(define-public crate-base32-0.5.0 (c (n "base32") (v "0.5.0") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "1270pqp062ybcd04v48gv6b1gmdg8zjjzd8b4936dyymyijh7kni")))

