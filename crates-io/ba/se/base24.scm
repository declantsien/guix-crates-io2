(define-module (crates-io ba se base24) #:use-module (crates-io))

(define-public crate-base24-0.1.0 (c (n "base24") (v "0.1.0") (h "07x48l5dww95l7jl7p08hyp1fxsz76v4vyfbnrd5837scy074iq6")))

(define-public crate-base24-0.2.0 (c (n "base24") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "05vg5n9jm7ckd0bxv9703s0hqn08mql4mwhgd19hqqiinx803i2z")))

(define-public crate-base24-0.2.1 (c (n "base24") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0mw4wd34cn337s5aff3ih179njdd0zydscn4007pzm17908iy1hp")))

(define-public crate-base24-0.3.0 (c (n "base24") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "03762z84gmzz4jrk97jqyczk7rd6j23wjidw1f5limix1z5ldqw3")))

(define-public crate-base24-0.4.0 (c (n "base24") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "17jfhxri2x3xppxvn8lfmmyx0i5gzchklbl44x5ir5p27i2b33gn")))

(define-public crate-base24-0.4.1 (c (n "base24") (v "0.4.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "10905z105168b52v7dzr5cywmy5z5f8ivyd0fxngj11cd17q15q3")))

