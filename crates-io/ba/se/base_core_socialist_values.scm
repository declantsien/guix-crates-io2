(define-module (crates-io ba se base_core_socialist_values) #:use-module (crates-io))

(define-public crate-base_core_socialist_values-0.1.0 (c (n "base_core_socialist_values") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "187nrzp9yr17bgq5jsh2vlvjql4mh41ivfll6pjw1lwj85604ngy")))

(define-public crate-base_core_socialist_values-0.2.0 (c (n "base_core_socialist_values") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "0bmpn9qz64i5klzsdxrbyc6gv86a2a3g4nvi8mplmb94g0wgaxq8")))

(define-public crate-base_core_socialist_values-0.3.0 (c (n "base_core_socialist_values") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "18jbmirwx0fqk1rp9gz62wwzbgw3j62pw1rzvxyzcg11s4pzbc1z")))

(define-public crate-base_core_socialist_values-0.3.1 (c (n "base_core_socialist_values") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "0wwss3nn487g63ad5lh2w0yxml10awvrjsny1ds52909xaj34dsl")))

(define-public crate-base_core_socialist_values-0.3.2 (c (n "base_core_socialist_values") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "0az89bkrg2c10ahrglqfgr0bdf68750idslp412mhiqm13mshl0i")))

(define-public crate-base_core_socialist_values-0.3.3 (c (n "base_core_socialist_values") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "0k5ri8lsifyg6w82y6fm80qxdc44drww1sfp1zgrpccwqpn8crlh")))

(define-public crate-base_core_socialist_values-0.3.4 (c (n "base_core_socialist_values") (v "0.3.4") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1hvlfv4gi9va7461q3j1w4m34722jwdps1f28ir54ag38f27s45q")))

