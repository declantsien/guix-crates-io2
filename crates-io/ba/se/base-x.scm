(define-module (crates-io ba se base-x) #:use-module (crates-io))

(define-public crate-base-x-0.1.0 (c (n "base-x") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1dmrys6mn585pfzbmflhywffy70nka0g1pidwn3snmzqwkwmwai1")))

(define-public crate-base-x-0.1.1 (c (n "base-x") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1150fd2m5ybm76jacs8i9sc0y6fbfb1wpyky0c1bg8nslliyrk3a")))

(define-public crate-base-x-0.1.2 (c (n "base-x") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0zzhsihv8xp438dvr540xq6b23rcw18440d1p153aqqba29f5kvk")))

(define-public crate-base-x-0.1.3 (c (n "base-x") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "099qw43xhzw99nhabn92nhzwm3vmvkyy2zfgl06kqc88j2pgq0cp")))

(define-public crate-base-x-0.2.0 (c (n "base-x") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0kc5h77i90ykgfz5rlajvzgwzgyi375mlcm33fzj1wvng0pdk6py")))

(define-public crate-base-x-0.2.1 (c (n "base-x") (v "0.2.1") (d (list (d (n "json") (r "^0.11") (d #t) (k 2)))) (h "0ddx1nff75hxdr1pmwipcvpgc9cv8w1xcm12r7k91afbyh0zxgf1")))

(define-public crate-base-x-0.2.2 (c (n "base-x") (v "0.2.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "json") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1hgnf4y2752g6ddayvdannzjk7psqyp365pg7gh7czrh8wxi0n9g")))

(define-public crate-base-x-0.2.3 (c (n "base-x") (v "0.2.3") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "json") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0lba9lja4jri4ld7p33k60g9yazxp5slggyq588jklc4al7mvnjw")))

(define-public crate-base-x-0.2.4 (c (n "base-x") (v "0.2.4") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "json") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0p8f01yjqf8p7sdmbn52bsjnnz7imxklgmdl5nhsznr2x1ja4nnm")))

(define-public crate-base-x-0.2.5 (c (n "base-x") (v "0.2.5") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "json") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0hv4y5cdhv6bk0ghk2434clw8v4mmk5cc9lsh6qrpri92zlfmx3n")))

(define-public crate-base-x-0.2.6 (c (n "base-x") (v "0.2.6") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "json") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1hfy0wv7j5ynd73yk1vyr32pqa77rp15lkrc54f8ky9c6hcbc80v")))

(define-public crate-base-x-0.2.7 (c (n "base-x") (v "0.2.7") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "json") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1asx3hdd4fqn678asi1ijx5cfjnfvyi4h6yfxk6212fhisplnwy2") (f (quote (("std") ("default" "std"))))))

(define-public crate-base-x-0.2.8 (c (n "base-x") (v "0.2.8") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "json") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "12zj7vgrf7wlc46f6xxc14dq1r6z6vmhn51vkdkp04q37lz1ylm4") (f (quote (("std") ("default" "std"))))))

(define-public crate-base-x-0.2.9 (c (n "base-x") (v "0.2.9") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "json") (r "^0.12") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0gj3wbw2z6hv2b29h9av79zp67808w8wbfj7fch31z45binxr49a") (f (quote (("std") ("default" "std"))))))

(define-public crate-base-x-0.2.10 (c (n "base-x") (v "0.2.10") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "json") (r "^0.12") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0x5ggdzbh41bh0kj23rng9i10386887174wp6zikzgaggf9s86fw") (f (quote (("std") ("default" "std"))))))

(define-public crate-base-x-0.2.11 (c (n "base-x") (v "0.2.11") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "json") (r "^0.12") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0w02sdqvg7zwm91agb2phikw4ri8jmncw32paxsv8ra1jv8ckfsc") (f (quote (("std") ("default" "std"))))))

