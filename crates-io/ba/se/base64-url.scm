(define-module (crates-io ba se base64-url) #:use-module (crates-io))

(define-public crate-base64-url-1.0.0 (c (n "base64-url") (v "1.0.0") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)))) (h "0wzckc5k85xh3jkwrvfr432w20f2qyjfsdggsb95h490d4g84f0r")))

(define-public crate-base64-url-1.0.1 (c (n "base64-url") (v "1.0.1") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)))) (h "16vv6rfi8nfl7d4clx5z8f74m1517saccg6k9dg67h69mfhmdhdx")))

(define-public crate-base64-url-1.1.0 (c (n "base64-url") (v "1.1.0") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)))) (h "0sfgg1rqisdyd6f5hkh2y3ygqa94sm507162gaj35s83z0874fc0")))

(define-public crate-base64-url-1.1.1 (c (n "base64-url") (v "1.1.1") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)))) (h "1ifbvx1hygfnj5f76b9gbv6wbrjv90s4gh12fsz0z8iffk86j7qb")))

(define-public crate-base64-url-1.1.2 (c (n "base64-url") (v "1.1.2") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)))) (h "0y4cr5pb6pd8bc6ibmxamhrn8y3q09fr9hm00ic813pz0bzwm9a4")))

(define-public crate-base64-url-1.1.3 (c (n "base64-url") (v "1.1.3") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)))) (h "00g5ssqs0bap0r7395xhi3n7pfffl2ig7i2sv58gdhpzcpwpxp1w")))

(define-public crate-base64-url-1.1.4 (c (n "base64-url") (v "1.1.4") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)))) (h "1fqp42nz8h3acgzpyfzpwd6n9ghncivign67m0hxqddvwbj38x1y")))

(define-public crate-base64-url-1.1.5 (c (n "base64-url") (v "1.1.5") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)))) (h "18hb9nr5bgwafn3ggs25dm8l0khkzlj58nb1w09zyiijrry5rgdw")))

(define-public crate-base64-url-1.1.6 (c (n "base64-url") (v "1.1.6") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)))) (h "0nl1f5w329ij9ycgx1yl5x99ndw80c3is3r3wb9fik284fyjnvqx")))

(define-public crate-base64-url-1.1.7 (c (n "base64-url") (v "1.1.7") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)))) (h "0f4vc5zb4sbv8bi8v4i3cbswh7xx780qq5d5nnf6w0jsvfq3wzqh")))

(define-public crate-base64-url-1.1.8 (c (n "base64-url") (v "1.1.8") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)))) (h "1vzqb9l19m7y570p66y7mysqibwyxsflxn014y74k00cf4sbb5ki")))

(define-public crate-base64-url-1.1.9 (c (n "base64-url") (v "1.1.9") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)))) (h "0ndx8vbyrqp7n62f6vldmr3rpq2vkd6d11yrq9r41x12ir41n2vn")))

(define-public crate-base64-url-1.1.10 (c (n "base64-url") (v "1.1.10") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)))) (h "0vnla0bxbzxpshn2r8jlh7dp9wcvl7i6z4nyy87y1j0npbwxwa55")))

(define-public crate-base64-url-1.1.11 (c (n "base64-url") (v "1.1.11") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)))) (h "18ih0gnacdxdz679l51airsj8iw0dvvfhs61jdi41y4g0h72fa6k")))

(define-public crate-base64-url-1.1.12 (c (n "base64-url") (v "1.1.12") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)))) (h "1ask4awk6nb3px5azkg75fi17mybd295gfpxg0xrd6bbqdzgh4h7")))

(define-public crate-base64-url-1.1.13 (c (n "base64-url") (v "1.1.13") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)))) (h "1ns0rasqq9xl61qd7j73x2llah2n1krg9rz9npv19m2ab8b3jq66")))

(define-public crate-base64-url-1.1.14 (c (n "base64-url") (v "1.1.14") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "0z3y2gjnfhly8drj7pna28gpwavxa98idcwig0akzab778p9rkgd")))

(define-public crate-base64-url-1.2.0 (c (n "base64-url") (v "1.2.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "0isqxhjwrr0sk6yyx1fr6gdx2nhimhlxklag4mm1v3cd861w5x3h")))

(define-public crate-base64-url-1.3.0 (c (n "base64-url") (v "1.3.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "1drqj1apapgqhw91k3v7rc7vh76d43nx6x4qnbwwbhkqr0sd16ch")))

(define-public crate-base64-url-1.3.1 (c (n "base64-url") (v "1.3.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "15d64cb6yrv3i4m8m6rca48jkrvwr6sh7brnqrf4yvd575sz7cd3")))

(define-public crate-base64-url-1.4.0 (c (n "base64-url") (v "1.4.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "0km70ylc7cy0kq5s1d8bqf5ndg7ciaf9755aqbw7hl7nh4ii464v") (y #t)))

(define-public crate-base64-url-1.4.1 (c (n "base64-url") (v "1.4.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "1g1jqfcirbk7xbq4ijx6kz4q6f4ygphjzd19x404nii2qsnpbz1m") (y #t)))

(define-public crate-base64-url-1.4.2 (c (n "base64-url") (v "1.4.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "0pjrbvmzay66ik7qs954vl967qqdf0cl58l91xi4b2bvz50xc5d5") (y #t)))

(define-public crate-base64-url-1.4.3 (c (n "base64-url") (v "1.4.3") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "0zh5qfjcnsawihrz8220nx4wq4m4asr5i1ks7ls03n3k9j498gik") (y #t)))

(define-public crate-base64-url-1.4.4 (c (n "base64-url") (v "1.4.4") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "1fpazg01bnap1lwr2x8wp7wrhwg7xw186hm66fj3qkzhcdchw6c2")))

(define-public crate-base64-url-1.4.5 (c (n "base64-url") (v "1.4.5") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "1f7qkfyc3g7lqsg7x5in1rqjn56y9lvfkzg8iq82ypx5b9drxv6j")))

(define-public crate-base64-url-1.4.6 (c (n "base64-url") (v "1.4.6") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "10w92a00y3xpl55mphk80y04cn3nqpknadkg9shz0jy3ca1fdacl")))

(define-public crate-base64-url-1.4.7 (c (n "base64-url") (v "1.4.7") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)))) (h "1zwjinm93ncvc81s8dwlg7gj38793xan7ax3rxmq77slfw4n4mlr")))

(define-public crate-base64-url-1.4.8 (c (n "base64-url") (v "1.4.8") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)))) (h "1jwabii8mafvv7gc759mqyx707k890r8fw3x82fr1q4v2f55b9y8")))

(define-public crate-base64-url-1.4.9 (c (n "base64-url") (v "1.4.9") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)))) (h "0rdmaw103hkgwfiyzhl81cvgjsbnv54aaz70wm5sr9ivfz00wfv4")))

(define-public crate-base64-url-1.4.10 (c (n "base64-url") (v "1.4.10") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)))) (h "1vxypwbs8wjdf0964z3bkpnainnsq8pb68rg3jmgqxpm0gwmq9j4")))

(define-public crate-base64-url-1.4.12 (c (n "base64-url") (v "1.4.12") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)))) (h "0diigpgkhgx2r2jkxa8vp4nkqw6k8zrvqbbqgd036n3mczcnw2z1")))

(define-public crate-base64-url-1.4.13 (c (n "base64-url") (v "1.4.13") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)))) (h "17w9hi27wqyxhhxws3sm94y4nw28rvmrrynxbp47fzhcklirrab7")))

(define-public crate-base64-url-2.0.0 (c (n "base64-url") (v "2.0.0") (d (list (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (k 0)))) (h "0vcihh08qci3q6nfsfkq4bh16hsyikxi6arfxsaz1s9nma40lnww") (f (quote (("std" "base64/std") ("default" "std"))))))

(define-public crate-base64-url-2.0.1 (c (n "base64-url") (v "2.0.1") (d (list (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (k 0)))) (h "026afym0c980jdjp4yqj6iz730ij3c0cly8p9pxxd33pil1gqppd") (f (quote (("std") ("default")))) (y #t) (r "1.57")))

(define-public crate-base64-url-2.0.2 (c (n "base64-url") (v "2.0.2") (d (list (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (k 0)))) (h "0sc53i8ysxnsa0b4x84q2j3ka9zsxqhrm3f8bwxhdhwc0pxvk7zv") (f (quote (("std" "base64/std") ("default" "std")))) (r "1.57")))

(define-public crate-base64-url-3.0.0 (c (n "base64-url") (v "3.0.0") (d (list (d (n "base64") (r "^0.22") (f (quote ("alloc"))) (k 0)))) (h "0a2b9fbprmyhwn3v10vw28qrb9rmvqxnhg9wbs6jixq6ik3vdqiq") (f (quote (("std" "base64/std") ("default" "std")))) (r "1.57")))

