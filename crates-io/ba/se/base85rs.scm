(define-module (crates-io ba se base85rs) #:use-module (crates-io))

(define-public crate-base85rs-0.1.0 (c (n "base85rs") (v "0.1.0") (h "1nyayvmf117k7a5kw8knh6h6lvpw5k6vxx5d1qrxh8zb57riwrh6")))

(define-public crate-base85rs-0.1.1 (c (n "base85rs") (v "0.1.1") (h "1zvapb64fnqmlah6plldzvkgjmiqfdbb93ilc2qg9j27gc6j3vzp")))

(define-public crate-base85rs-0.1.2 (c (n "base85rs") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18jcqnzj4a5dl5y2ywcnqf4rl4ac9y7f5r1bgg54dm5388jigdz7")))

(define-public crate-base85rs-0.1.3 (c (n "base85rs") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0nkbkjnx2xlnd2zcqb7fxmbmav6a8sr2vx8ixlcz0wdgl8rqsrw7")))

