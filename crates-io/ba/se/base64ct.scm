(define-module (crates-io ba se base64ct) #:use-module (crates-io))

(define-public crate-base64ct-0.0.0 (c (n "base64ct") (v "0.0.0") (h "0h02q67x6gyqvg7v13j05a8gmcl6l5ypn9nkiv1265cgnmcimwyx") (y #t)))

(define-public crate-base64ct-0.1.0 (c (n "base64ct") (v "0.1.0") (h "1wyr6rii344z8w56zwczh9v7qpnsaj5wil1lfh23iz2g5sncn0n5") (f (quote (("std" "alloc") ("alloc")))) (y #t)))

(define-public crate-base64ct-0.1.1 (c (n "base64ct") (v "0.1.1") (h "19vrjr4c0i1phsd6wikcyqfl0nq88d67k5zanj2gwb5c3j66nv79") (f (quote (("std" "alloc") ("alloc")))) (y #t)))

(define-public crate-base64ct-0.1.2 (c (n "base64ct") (v "0.1.2") (h "1n439hfa3c2z8gbwcymymhws3127k2417cxf8y3g6949pc2v0idj") (f (quote (("std" "alloc") ("alloc")))) (y #t)))

(define-public crate-base64ct-0.2.0 (c (n "base64ct") (v "0.2.0") (h "0v1dkbrzsll9dhm0y6j5fgqlyi8lnz741ns3hpncxr32dvykiyr2") (f (quote (("std" "alloc") ("alloc")))) (y #t)))

(define-public crate-base64ct-0.2.1 (c (n "base64ct") (v "0.2.1") (h "19mi2ckf3q5qb0x812yvvg032n6lmknrcsi2i4lfbirnh3f4b2kv") (f (quote (("std" "alloc") ("alloc")))) (y #t)))

(define-public crate-base64ct-1.0.0 (c (n "base64ct") (v "1.0.0") (h "1dbk04hir0vr17hxb80sg2x1751j96fz92hlmx3k3r7insv7zlnh") (f (quote (("std" "alloc") ("alloc")))) (y #t)))

(define-public crate-base64ct-1.0.1 (c (n "base64ct") (v "1.0.1") (h "0sx4a44c2n450lsmi0q1mgfbjhkw1sx57462cv77p0mmy9mgscla") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-base64ct-1.1.0 (c (n "base64ct") (v "1.1.0") (h "0vmgx707qypzdlb1vfg72yakgwfg37ap89lynzdhv0awq23nbaa0") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-base64ct-1.1.1 (c (n "base64ct") (v "1.1.1") (h "0p4was874qc90q2chm2i14m9mn8zmxjis8vaxihd6a2x4aqxkd76") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-base64ct-1.2.0 (c (n "base64ct") (v "1.2.0") (h "068ym54zxcma0z4w58l5dxlmx97l45d8mmns1ij5ls1d04mpfb1r") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-base64ct-1.3.0 (c (n "base64ct") (v "1.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0xs1hwlyb71fx699pbm9rkif0g13q5cmvm622xdbrhz2p8i61923") (f (quote (("std" "alloc") ("alloc")))) (y #t) (r "1.56")))

(define-public crate-base64ct-1.3.1 (c (n "base64ct") (v "1.3.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0kav19m82hxq8gai8fphwc2rpjgjf9dh2hg8l9l7ifg839irq3x3") (f (quote (("std" "alloc") ("alloc")))) (y #t) (r "1.56")))

(define-public crate-base64ct-1.3.2 (c (n "base64ct") (v "1.3.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "09vikzd6wxy7m4i9nyva0lpz2y1wjb0cyp2556f16vnacj8ybpq0") (f (quote (("std" "alloc") ("alloc")))) (y #t) (r "1.56")))

(define-public crate-base64ct-1.3.3 (c (n "base64ct") (v "1.3.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1818i0gpg7q35s6yglmhgciwi3jwx65mqc2ipjl54jfbmm288kw7") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-base64ct-1.4.0-pre.0 (c (n "base64ct") (v "1.4.0-pre.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "09cd5pmzm9h8vs8cd5f8b3y8hyp9bxmd3bvcsfvf1bvdgg6a0sw1") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-base64ct-1.4.0 (c (n "base64ct") (v "1.4.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0cvyj9giwdvzybzsw43dki27326qg3jlzhijnzsnq1844dnwcax4") (f (quote (("std" "alloc") ("alloc")))) (y #t) (r "1.56")))

(define-public crate-base64ct-1.4.1 (c (n "base64ct") (v "1.4.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1xfcjxgr7ff9phvg1kci9pyrn4imqqhh3b00n7hwq8n5kx8gbb3i") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-base64ct-1.5.0 (c (n "base64ct") (v "1.5.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0y9i5wsfl01cbh2nnd4q8hxkxmqai3pk0zn1g3in933s6kkhiafy") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-base64ct-1.5.1 (c (n "base64ct") (v "1.5.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0ldqf5kfx1i7fb0wxhmnnq0kabdn9rishfcm7nqy08bqchsaip1v") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-base64ct-1.5.2 (c (n "base64ct") (v "1.5.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0x447hcnmdbjjzjg5blxl07q46m5c33grnfwh1k8akb1zmb28aza") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-base64ct-1.5.3 (c (n "base64ct") (v "1.5.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1gq7avqyrskz0an97fv68sn00nczlk0wp0ag3c8vdk1f2a4s0idn") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-base64ct-1.6.0 (c (n "base64ct") (v "1.6.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0nvdba4jb8aikv60az40x2w1y96sjdq8z3yp09rwzmkhiwv1lg4c") (f (quote (("std" "alloc") ("alloc")))) (r "1.60")))

