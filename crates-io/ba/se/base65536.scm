(define-module (crates-io ba se base65536) #:use-module (crates-io))

(define-public crate-base65536-0.1.0 (c (n "base65536") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "1cbqjp6gdr2x3ydi40yky0kiyvwcq63k3nd5iyzkddi5kiz8f62y")))

(define-public crate-base65536-0.2.0 (c (n "base65536") (v "0.2.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "1j3kiqzgrhrdy57kq2k5y1c45n4dixbg6kh67x93q44py3djrwj9")))

(define-public crate-base65536-0.3.0 (c (n "base65536") (v "0.3.0") (d (list (d (n "fnv") (r "^1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "1k3fbmybn9gm84sfamx928h2z6g68dfcxfxr98bz0vxkymy4lsnk") (f (quote (("default" "fnv"))))))

(define-public crate-base65536-0.3.1 (c (n "base65536") (v "0.3.1") (d (list (d (n "fnv") (r "^1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "09vhjlhrxpgj3l0nr3q9hqv9vlnkx92jhaj1yg6chdalpnfs242z") (f (quote (("nightly") ("default" "fnv"))))))

(define-public crate-base65536-0.4.0 (c (n "base65536") (v "0.4.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1r0clp42v7f5znl6sa5lv5n2811nhplbi4cw0zm8cha6abxx094p") (f (quote (("nightly") ("default" "fnv"))))))

(define-public crate-base65536-1.0.0 (c (n "base65536") (v "1.0.0") (d (list (d (n "fnv") (r "^1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1cm9bzs88d97w40azir3chwy7i90phzc3w92lxhwj22cp827xjq4") (f (quote (("nightly") ("default" "fnv"))))))

(define-public crate-base65536-1.0.1 (c (n "base65536") (v "1.0.1") (d (list (d (n "fnv") (r "^1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1vly5gfxz1l5xkm3qz27jnwnmzxlv8lbhr1b15lk4n7bb0a4axrz") (f (quote (("nightly") ("default" "fnv"))))))

