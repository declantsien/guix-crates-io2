(define-module (crates-io ba se baseunits-rs) #:use-module (crates-io))

(define-public crate-baseunits-rs-0.1.0 (c (n "baseunits-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "iso-4217") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "rust-fp-categories") (r "^0.0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.8.1") (d #t) (k 0)) (d (n "time") (r "^0.1.44") (d #t) (k 0)))) (h "0jn6y4mja25zsw3a848rdf5glyz3z1vbmh8b66d0rc7rgbqi4fbd")))

