(define-module (crates-io ba se base64_t) #:use-module (crates-io))

(define-public crate-base64_t-0.1.0 (c (n "base64_t") (v "0.1.0") (d (list (d (n "base64") (r "^0.7") (d #t) (k 0)))) (h "1yk6w7czypmfq26zcr9vpr01amr36kqz3w6rxqq9k7wnmrq7bj0a")))

(define-public crate-base64_t-0.1.1 (c (n "base64_t") (v "0.1.1") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)))) (h "1xx9vr4vslfh9fr579ql3kihjv0li85q09zzh1kw6s8lm6xkwmz9")))

