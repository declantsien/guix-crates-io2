(define-module (crates-io ba se base_url) #:use-module (crates-io))

(define-public crate-base_url-0.0.5 (c (n "base_url") (v "0.0.5") (d (list (d (n "try_from") (r "^0.2.2") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "0f750ilqg087d0cjhl4wf107d2sqsiyrr7dh3snkg8nfwksfb1cr")))

(define-public crate-base_url-0.0.6 (c (n "base_url") (v "0.0.6") (d (list (d (n "try_from") (r "^0.2.2") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "1m2zzn20w01hs1jmv51z8xpcr8ywkh934vlpyixz07mc2vpck44n")))

(define-public crate-base_url-0.0.7 (c (n "base_url") (v "0.0.7") (d (list (d (n "try_from") (r "^0.2.2") (d #t) (k 0)) (d (n "url") (r "^1.0.0") (d #t) (k 0)))) (h "1qiyb298c77c6fwf2lbm8gnggxiz02v9xij991jdyji5k7qpq2gw")))

(define-public crate-base_url-0.0.8 (c (n "base_url") (v "0.0.8") (d (list (d (n "robotparser") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "sitemap") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)) (d (n "url") (r "^1.0.0") (d #t) (k 0)))) (h "1h2axc2ka4g8xhjr0d0m28bga2incrzny6016ni26yn636viccr4") (f (quote (("sitemap_conversion" "sitemap" "_conversion_any") ("robot_conversion" "robotparser" "_conversion_any") ("default") ("_conversion_any"))))))

(define-public crate-base_url-0.0.9 (c (n "base_url") (v "0.0.9") (d (list (d (n "robotparser") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "sitemap") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.0.0") (d #t) (k 0)))) (h "00hyisxl1wwjg0fidqcvxncnl0xdl85yjp5mhsllv8fawmlsgl9n") (f (quote (("sitemap_conversion" "sitemap") ("robot_conversion" "robotparser") ("default" "_try_from") ("_try_from" "try_from"))))))

(define-public crate-base_url-0.0.10 (c (n "base_url") (v "0.0.10") (d (list (d (n "robotparser") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "sitemap") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "try_from") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.0.0") (d #t) (k 0)))) (h "17s5x2b9a3dqhnwna2y6ym2kx6354hz1az5lf4l0qlpr2bcqfarj") (f (quote (("sitemap_conversion" "sitemap") ("robot_conversion" "robotparser") ("default" "_try_from") ("_try_from" "try_from"))))))

(define-public crate-base_url-1.0.0 (c (n "base_url") (v "1.0.0") (d (list (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "try_from") (r "^0.2.2") (d #t) (k 0)) (d (n "url") (r "^1.0.0") (d #t) (k 0)))) (h "01cw64mwyls6rm9wxid75i8sygly2vz91f8ki229vag5h86zprk1")))

(define-public crate-base_url-1.0.1 (c (n "base_url") (v "1.0.1") (d (list (d (n "try_from") (r "^0.2.2") (d #t) (k 0)) (d (n "url") (r "^1.0.0") (d #t) (k 0)))) (h "0q1qs4y3wgdz4dn5vl8flnpp8c3s82mi99mhkdgi4mspsg8ixxy2")))

(define-public crate-base_url-1.0.2 (c (n "base_url") (v "1.0.2") (d (list (d (n "url") (r "^1.0.0") (d #t) (k 0)))) (h "0j9i06105qk3wzckk7wms70kc134hfsjbb6hnmgprklkhvrz2a7m")))

(define-public crate-base_url-1.1.0 (c (n "base_url") (v "1.1.0") (d (list (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1x76qgix0p7rnq92825d6afvshkhnyzx4qwvgclhbqr3ik0kqz23")))

