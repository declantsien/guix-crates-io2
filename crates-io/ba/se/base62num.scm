(define-module (crates-io ba se base62num) #:use-module (crates-io))

(define-public crate-base62num-0.1.0 (c (n "base62num") (v "0.1.0") (h "0nlhxzgbqa60gp1wvcj0xz3ra7ih8qncdyqn2rjrd00b1g6qmi31")))

(define-public crate-base62num-0.1.1 (c (n "base62num") (v "0.1.1") (h "1887y3zw7cnaybxc2yjxhm4265k6iwrgvgymmx8psjfhjcdf17p5")))

