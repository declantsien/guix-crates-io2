(define-module (crates-io ba se base64-literal) #:use-module (crates-io))

(define-public crate-base64-literal-0.1.0 (c (n "base64-literal") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "19c7inlfviz3p83bya9kxvvg7pb3szk1x29r2f58qpagfhwdhj92")))

