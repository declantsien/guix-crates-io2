(define-module (crates-io ba se base64-stream) #:use-module (crates-io))

(define-public crate-base64-stream-0.1.0 (c (n "base64-stream") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)))) (h "1y3xfkrqnianfbibdid7q0wp6dij4xg0mz0kvnfiza9qwrxy7rp4")))

(define-public crate-base64-stream-1.0.0 (c (n "base64-stream") (v "1.0.0") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)))) (h "1m39f3l1km1l2cyb5mqpkw6dbakhkadzd5qyscpyq9vwvg2vv68a")))

(define-public crate-base64-stream-1.0.1 (c (n "base64-stream") (v "1.0.1") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)))) (h "1pvfyqkzyv12q33lrw5vncqb1lr5fwimi5ig5fsv31xpqsz9rwcd")))

(define-public crate-base64-stream-1.0.2 (c (n "base64-stream") (v "1.0.2") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)))) (h "0bg70nzk6kakkah6k1i7icqwz720575lzmgkzh429fjg818q6qwp")))

(define-public crate-base64-stream-1.0.3 (c (n "base64-stream") (v "1.0.3") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)))) (h "1mkg8q6pxsqk4cnc0w3wgd2b4m56c31y98gw9fv0hj4fz5l1r5ls")))

(define-public crate-base64-stream-1.0.4 (c (n "base64-stream") (v "1.0.4") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "derivative") (r "^1") (d #t) (k 0)))) (h "0g4nzb8nbk1jb3jp1a5w8rv0704j32k0ghk0cqd5f3fldip1y8sg")))

(define-public crate-base64-stream-1.0.5 (c (n "base64-stream") (v "1.0.5") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "educe") (r ">= 0.0.1") (d #t) (k 0)))) (h "04x2n452lylfv551b9g3672h7z38691892r0w9idwycwxp089d2k")))

(define-public crate-base64-stream-1.0.6 (c (n "base64-stream") (v "1.0.6") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "educe") (r ">= 0.0.1") (d #t) (k 0)))) (h "0089yyg4vzzijzljnsszqgairnwps8jkjwyi5dfgxkj9dflrh6dq")))

(define-public crate-base64-stream-1.0.7 (c (n "base64-stream") (v "1.0.7") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "1xkkcyp3x9kfnb63y5ir6vyf34wa2b42rjiqiwsrhwk4zsih68yn")))

(define-public crate-base64-stream-1.0.8 (c (n "base64-stream") (v "1.0.8") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "1yxldmd55c158hh155b5mk984lzm2qmjk89gshbwwl4dc9n0kmv3")))

(define-public crate-base64-stream-1.0.9 (c (n "base64-stream") (v "1.0.9") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "1ym5dya6knfg3g62vrzsbzgrwf8ip331bw5lfzalbpvc8nfzd2r6")))

(define-public crate-base64-stream-1.0.10 (c (n "base64-stream") (v "1.0.10") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "132j64klz02n713ljlnjrh19jfl1v21a96zdb88hrbs7jhvr8zw4")))

(define-public crate-base64-stream-1.0.11 (c (n "base64-stream") (v "1.0.11") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "09ffkw3y1y5fbav8fjbwqvw7zk1xwka9vaq91xbpvdwzyvw6gs75")))

(define-public crate-base64-stream-1.0.12 (c (n "base64-stream") (v "1.0.12") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "0alap2bkjm585v685f13bk06sqlyj6j26pgfs3dlj98hiicr5kl5")))

(define-public crate-base64-stream-1.1.0 (c (n "base64-stream") (v "1.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "039j657cbbbcwq167bdp2ld3gqi11hxk104fibs410rxq1c19b55")))

(define-public crate-base64-stream-1.1.1 (c (n "base64-stream") (v "1.1.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "educe") (r ">= 0.4.0") (f (quote ("Debug"))) (k 0)))) (h "1ri86vw17p3nw89qlpgdbifpd9ch0ma1f9m73fw6la2x7m90dxjw")))

(define-public crate-base64-stream-1.2.0 (c (n "base64-stream") (v "1.2.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "07xf8idhr4bq5djxziakz7bs408q3h93m5axnpnyqn7inzgnzigs") (y #t)))

(define-public crate-base64-stream-1.2.1 (c (n "base64-stream") (v "1.2.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0j7ga6ycghdbxmpz480zp0qnrhv64s8myaq8ri3iycijgvj0ibb1")))

(define-public crate-base64-stream-1.2.2 (c (n "base64-stream") (v "1.2.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0w9q51si1d2iqnzziymfn28mhanbxz4mh3f1y0x7xsgrdpbx512m")))

(define-public crate-base64-stream-1.2.3 (c (n "base64-stream") (v "1.2.3") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0cgvkpdxvs7kaq5kfbgz5lilm58h1702gn0a6l1kg50965y6sm5q")))

(define-public crate-base64-stream-1.2.4 (c (n "base64-stream") (v "1.2.4") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0aaz63zisd1jd2g01v22yfqh9cvj7c1zrzb249ww7y6q171hvwgl")))

(define-public crate-base64-stream-1.2.5 (c (n "base64-stream") (v "1.2.5") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0z40lyp11n2ky2k0wswy9zkgzsvn78f2vpxxm62rxjw1p7a6qngn")))

(define-public crate-base64-stream-1.2.6 (c (n "base64-stream") (v "1.2.6") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0y2wv7c6wwwz065d9y4jgnq31k38x23md3nlr3ky0q874fcskx3a")))

(define-public crate-base64-stream-1.2.7 (c (n "base64-stream") (v "1.2.7") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1yyxndmmw75zs6ssk5y5vlj4x7ifav6q5xhirw82dk41rpsgg5j8")))

(define-public crate-base64-stream-2.0.0 (c (n "base64-stream") (v "2.0.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0dn7pyb8m9wfi10wxfgnsaxf5yk20dfj03awyri9zvf5ksidj4g5")))

(define-public crate-base64-stream-3.0.0 (c (n "base64-stream") (v "3.0.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^1") (d #t) (k 0)))) (h "1g7cq23xbzl828cqw4wmidjpphmq7q92w43awzq52a5lyqmnsbc2") (r "1.60")))

(define-public crate-base64-stream-3.0.1 (c (n "base64-stream") (v "3.0.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "educe") (r ">=0.4.0") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^1") (d #t) (k 0)))) (h "0j054kr79khg6y46017ncla6ndw8ab1xccmw5gp4kpkyzparhxmi") (r "1.65")))

(define-public crate-base64-stream-4.0.0 (c (n "base64-stream") (v "4.0.0") (d (list (d (n "base64") (r "^0.22") (d #t) (k 0)) (d (n "educe") (r "^0.5") (f (quote ("Debug"))) (k 0)) (d (n "generic-array") (r "^1") (d #t) (k 0)))) (h "1ps9jw2w3crf7hi911s2vn55nj2fk0278zk1m8gm652mxavqc8wk") (r "1.65")))

