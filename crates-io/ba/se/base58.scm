(define-module (crates-io ba se base58) #:use-module (crates-io))

(define-public crate-base58-0.1.0 (c (n "base58") (v "0.1.0") (h "10xfw6v7jzn9i682mkw9nqybzafrvl3i2wawwgp5a8gh2n0fw92h")))

(define-public crate-base58-0.2.0 (c (n "base58") (v "0.2.0") (h "10gmsivqfg1rpg8rzv1xbdsaa379yplxjy581na6hak8wqdzw1v1")))

