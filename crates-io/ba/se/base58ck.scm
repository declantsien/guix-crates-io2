(define-module (crates-io ba se base58ck) #:use-module (crates-io))

(define-public crate-base58ck-0.1.0 (c (n "base58ck") (v "0.1.0") (d (list (d (n "hashes") (r "^0.14.0") (f (quote ("alloc"))) (k 0) (p "bitcoin_hashes")) (d (n "hex") (r "^0.2.0") (f (quote ("alloc"))) (k 2) (p "hex-conservative")) (d (n "internals") (r "^0.3.0") (d #t) (k 0) (p "bitcoin-internals")))) (h "0pv0k3d1q3lnqisqrb3padx0cmh2rrr4ag4iq62s8bisb946d39c") (f (quote (("std" "hashes/std" "internals/std") ("default" "std")))) (r "1.56.1")))

