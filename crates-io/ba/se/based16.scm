(define-module (crates-io ba se based16) #:use-module (crates-io))

(define-public crate-based16-0.1.0 (c (n "based16") (v "0.1.0") (h "081jg6bpl1n7ibzqp1qxibpab36gh71cvsmy7yvqxg1s9bh48nc7")))

(define-public crate-based16-0.1.1 (c (n "based16") (v "0.1.1") (h "043q5gf5s4cvrh2ak5vlpbjr5xj44gz40jw8p0h0z3j6zvigakrg")))

(define-public crate-based16-0.1.2 (c (n "based16") (v "0.1.2") (h "1vwaz94ny0gkmvc0vnz3djxmld2zw6jmkdmgsh0bvwbxnqn0mmjk")))

(define-public crate-based16-0.2.0 (c (n "based16") (v "0.2.0") (h "1fqsa458qkmmgn0nmak0m2k7qangyqbdy75q13f49dxjfh2hyff8") (f (quote (("alloc"))))))

(define-public crate-based16-0.2.1 (c (n "based16") (v "0.2.1") (h "093xhraai8csk6m31b4vw7i9dbnb0xy5b0z5l64s6lqqv2yjfw8w") (f (quote (("alloc"))))))

