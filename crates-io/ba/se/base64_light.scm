(define-module (crates-io ba se base64_light) #:use-module (crates-io))

(define-public crate-base64_light-0.1.0 (c (n "base64_light") (v "0.1.0") (h "0wn8gb75vhfjshsca0mi7slpfqxqbiwr5wazgd1bhj21fdbawqsj")))

(define-public crate-base64_light-0.1.1 (c (n "base64_light") (v "0.1.1") (h "0hwqsnkdqbcl4lv45abrj2ffswv638fbfpsl2dv6c4kp233zmab5")))

(define-public crate-base64_light-0.1.2 (c (n "base64_light") (v "0.1.2") (h "1iw2h0a1ic9bk2w9a1bn8nc6ndyz93hy0hlvbiqp1n4pzq5rkqis")))

(define-public crate-base64_light-0.1.3 (c (n "base64_light") (v "0.1.3") (h "0vylmqv567zxmsdzi3asrh7f4p9ajzhyj0zqbcdhmknm1acckgyn")))

(define-public crate-base64_light-0.1.4 (c (n "base64_light") (v "0.1.4") (h "07gm0zq8kr7i6yfaiilh6lq4bz6c7b696lw2ycy2ysfvhyrllz0p")))

(define-public crate-base64_light-0.1.5 (c (n "base64_light") (v "0.1.5") (h "1pxa6qfv9y9djfld5ipcpknxd32sdq4v7890gaa8b13byw4clslc")))

