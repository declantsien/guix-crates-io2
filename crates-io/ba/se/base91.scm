(define-module (crates-io ba se base91) #:use-module (crates-io))

(define-public crate-base91-0.0.1 (c (n "base91") (v "0.0.1") (h "1v4cb7xckgjb0biwknk5phalli9xxfrnr9rivqw50ma2sgd73jcw")))

(define-public crate-base91-0.1.0 (c (n "base91") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "09f0gczxp5w0gkw1pbzkmfryrxdxm7fa6i4l4cpl5vmmwyx5zsxl") (f (quote (("xml-friendly") ("std") ("default" "std" "canonical" "xml-friendly") ("canonical"))))))

