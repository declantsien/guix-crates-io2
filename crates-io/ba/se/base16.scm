(define-module (crates-io ba se base16) #:use-module (crates-io))

(define-public crate-base16-0.1.0 (c (n "base16") (v "0.1.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1djhg99dydj0jsg80124n79217wv128q22vz3bmb6j3gc48794w4")))

(define-public crate-base16-0.1.1 (c (n "base16") (v "0.1.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0sizaz1yv0idgbdykxf1mgp73fncjmkxw1iaaj2z8sv6954lmzd7")))

(define-public crate-base16-0.1.2 (c (n "base16") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "06avpi2dwhm8ai6w4l0yyxwz76i5s9nry5a9j4m1ndswacw8lq2x") (f (quote (("std") ("default" "std"))))))

(define-public crate-base16-0.2.0 (c (n "base16") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "01w2zvaiwg2d4bhh7hzbz0gdh3p2h63v68jb80iiil6icb208szf") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-base16-0.2.1 (c (n "base16") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1a55jd3a4hsnsqg1fakpmll45plq8hifc465ib723vkaqc83cz6j") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

