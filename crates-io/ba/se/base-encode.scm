(define-module (crates-io ba se base-encode) #:use-module (crates-io))

(define-public crate-base-encode-0.1.0 (c (n "base-encode") (v "0.1.0") (h "0x3wx985klngqdcvj37pr8ffy4500xa2lj1k756x405z17x8k4as")))

(define-public crate-base-encode-0.1.1 (c (n "base-encode") (v "0.1.1") (h "12ziz9x9czvfbmzj2scdc34hxv5vhpyqj8f08yskcwifdcm677jy")))

(define-public crate-base-encode-0.1.2 (c (n "base-encode") (v "0.1.2") (h "0jhxzlmc6zp6s4w5rbzk794v02z54qf32dx7i0ny2wjd4vrxl1in")))

(define-public crate-base-encode-0.2.0 (c (n "base-encode") (v "0.2.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0qdpxikgi5lysihihifvw3ccj0vxzv4y6hy57s249zfp0flnijlr")))

(define-public crate-base-encode-0.3.0 (c (n "base-encode") (v "0.3.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1qqyy00kvkllxdl5rwr77v2ymq9fcvs79k8n3jrg4li1i74v8v24")))

(define-public crate-base-encode-0.3.1 (c (n "base-encode") (v "0.3.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "133v6dlp1y9d737piyw4yyg79dx7bvxarm7lhy9jxwvhgjgx4yx1")))

