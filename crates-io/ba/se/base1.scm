(define-module (crates-io ba se base1) #:use-module (crates-io))

(define-public crate-base1-0.1.0 (c (n "base1") (v "0.1.0") (h "1ifmmpbmgcmx3b55sdvahbwr5iill1ql0ga1dz17v57408758b3x")))

(define-public crate-base1-0.1.1 (c (n "base1") (v "0.1.1") (h "0p6jm50q55b4fafm0kpdw783pkdafabljk52s9m5l7hsa574s893")))

(define-public crate-base1-0.1.2 (c (n "base1") (v "0.1.2") (h "1iffsz3366djpqmgws5q7zmxn5k7wbf2s8vmvzdxkpbs0zwx9yjz")))

