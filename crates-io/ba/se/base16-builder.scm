(define-module (crates-io ba se base16-builder) #:use-module (crates-io))

(define-public crate-base16-builder-0.1.0 (c (n "base16-builder") (v "0.1.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "git2") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustache") (r "^0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1fglc3hdx8fr52a2xi2b5klbzvypv8bz4b7g78k01nyqxmkn5ir1")))

(define-public crate-base16-builder-0.1.1 (c (n "base16-builder") (v "0.1.1") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustache") (r "^0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1zbz1v8053mxvb62sqq7r46a2qvdzvs33p110x9gslirippf6p68")))

