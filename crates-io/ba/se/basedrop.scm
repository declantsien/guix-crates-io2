(define-module (crates-io ba se basedrop) #:use-module (crates-io))

(define-public crate-basedrop-0.1.0 (c (n "basedrop") (v "0.1.0") (h "1i0srvgz931zq56yhq1482h21b20gdswwy2aap4j89f6kjjb0cbm") (y #t)))

(define-public crate-basedrop-0.1.1 (c (n "basedrop") (v "0.1.1") (h "0c5pzjpyzyy2zdx9ggq2mhgq2qc603bawbysal8k4vk911xd703f") (y #t)))

(define-public crate-basedrop-0.1.2 (c (n "basedrop") (v "0.1.2") (h "18n9nyclnfzy82zqwpzn7zisrqga3y5sl8p3pygvh6zj1i9iwqa7")))

