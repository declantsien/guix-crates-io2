(define-module (crates-io ba se base-62) #:use-module (crates-io))

(define-public crate-base-62-0.1.0 (c (n "base-62") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.39") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "131cyk4fcx73mgfi869cidxlf4plyxby0mqxsdfhnxcjsmd12cp2")))

(define-public crate-base-62-0.1.1 (c (n "base-62") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.39") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1cl99zl58qw2fd6vy1jpjhs6xy9fdrfd7hiyp2ayh277ndqvv3pj")))

