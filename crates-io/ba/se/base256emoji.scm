(define-module (crates-io ba se base256emoji) #:use-module (crates-io))

(define-public crate-base256emoji-1.0.0 (c (n "base256emoji") (v "1.0.0") (d (list (d (n "const-str") (r "^0.4.3") (d #t) (k 0)) (d (n "match-lookup") (r "^0.1.1") (d #t) (k 0)))) (h "0k0919z3qmqs4c6kyn3z9raipbc7nzyav0g31bw8cnpkah584yzr")))

(define-public crate-base256emoji-1.0.1 (c (n "base256emoji") (v "1.0.1") (d (list (d (n "const-str") (r "^0.4.3") (d #t) (k 0)) (d (n "match-lookup") (r "^0.1.1") (d #t) (k 0)))) (h "1iizgq67c95fbivibn4817f5x16pyji7k8ij5salh2klsgzw67ac")))

(define-public crate-base256emoji-1.0.2 (c (n "base256emoji") (v "1.0.2") (d (list (d (n "const-str") (r "^0.4.3") (d #t) (k 0)) (d (n "match-lookup") (r "^0.1.1") (d #t) (k 0)))) (h "0z07il99l7k6s5crhf28ka1j0pr7dsplkrkn474pfni4k86l7sdm")))

