(define-module (crates-io ba se base_custom) #:use-module (crates-io))

(define-public crate-base_custom-0.1.0 (c (n "base_custom") (v "0.1.0") (h "06zps9z27ghb297nj4pnhvhvpgfl7hpqc0qpaf4llbzi3yxca4gc")))

(define-public crate-base_custom-0.1.1 (c (n "base_custom") (v "0.1.1") (h "1bqzgsa25z4vd28q1fvcdd8xjv3xg33bpya6zfcfq24gjvr39zkn")))

(define-public crate-base_custom-0.1.2 (c (n "base_custom") (v "0.1.2") (h "0ld6mnsmiwdniy03pghhw0cfj888vhwkgvmvh25qscwwgbhwjp9p")))

(define-public crate-base_custom-0.1.3 (c (n "base_custom") (v "0.1.3") (h "1r6c4i9dsyznnccnqji9nclls2ggyhgv328cqdx5rh1f77a90sar")))

(define-public crate-base_custom-0.1.4 (c (n "base_custom") (v "0.1.4") (h "13af0nhy4g4hhyfgp5a96501752mchw71bsfygfbvj9g9mhq52lr")))

(define-public crate-base_custom-0.1.5 (c (n "base_custom") (v "0.1.5") (h "14kzwj5v19x36fvfggngpmlsli086y7132rwsfwlkzxavyxy7arj")))

(define-public crate-base_custom-0.1.6 (c (n "base_custom") (v "0.1.6") (h "1sz3aqml5qp9ax444nb5vdyaj8a6f0vcspkqipwfca99psa3dfh0")))

(define-public crate-base_custom-0.1.7 (c (n "base_custom") (v "0.1.7") (h "14yb13hlr2h2jdwgjmyivlrlw80yh9xh2q5qbb52nrba5xixigzl")))

(define-public crate-base_custom-0.1.8 (c (n "base_custom") (v "0.1.8") (h "1il4y9gnsxqc3waxqpkg9ih1js4kn0xbsm19w3a0sdh64vj6z7gj")))

(define-public crate-base_custom-0.1.9 (c (n "base_custom") (v "0.1.9") (h "1wbfjyljbb2jp49zyssg49z06ng2a6dxd6f7jkinn2857sg2qzr6")))

(define-public crate-base_custom-0.1.10 (c (n "base_custom") (v "0.1.10") (h "0k99w4v38829dhv5433d705484hwbrjsq07if74ymj88bw8xaapj")))

(define-public crate-base_custom-0.1.11 (c (n "base_custom") (v "0.1.11") (h "0w5yfyfp7zyzkwzkz5qb8gzknxi9w6ij970b5kb3y1f63yc51j1c")))

(define-public crate-base_custom-0.1.12 (c (n "base_custom") (v "0.1.12") (h "0apz7yn8gi5pk5alxc556zwqlzx0z6f97a58p2fcx03gh731703y")))

(define-public crate-base_custom-0.1.13 (c (n "base_custom") (v "0.1.13") (h "1f0a6c3yw06l6hr0wmp0v8vqwx5lysjm81rqayyacz4i5sj0n02k")))

(define-public crate-base_custom-0.2.0 (c (n "base_custom") (v "0.2.0") (h "1lkx97il8bf2z13gllh7xdbs9amv1zmymx22r1fdgaq594iqyq8i")))

