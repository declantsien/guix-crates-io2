(define-module (crates-io ba se base64_type) #:use-module (crates-io))

(define-public crate-base64_type-0.2.0 (c (n "base64_type") (v "0.2.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "base64-serde") (r "^0.7") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proptest-derive") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1mbawjkbil5rg8ljjsdlj92rbnad544nkisy8vrmvagyrkx24grp") (f (quote (("arbitrary" "proptest" "proptest-derive")))) (r "1.70.0")))

