(define-module (crates-io ba se base64-bytes) #:use-module (crates-io))

(define-public crate-base64-bytes-0.1.0 (c (n "base64-bytes") (v "0.1.0") (d (list (d (n "base64") (r "^0.22") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11hkxrjjmkvvb337pgmmbgvni4g05cb4hlxaqgcyx82z9174xrbw")))

