(define-module (crates-io ba se basehan) #:use-module (crates-io))

(define-public crate-basehan-0.1.1 (c (n "basehan") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bcwcn6rdp8x8xp6xzmdllgp5dzffagxx9zkd28y64wqclb4rj9a")))

(define-public crate-basehan-0.1.2 (c (n "basehan") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k03islz1d6gm0a3h8fg0z1jbdqdivbx3y4mrs6dgxpysphga1v4")))

(define-public crate-basehan-0.1.3 (c (n "basehan") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r733wc9r6ghpsk2k0x4pqpn70a564pbvddh2h4m7w14qmi6j9b6")))

(define-public crate-basehan-0.1.4 (c (n "basehan") (v "0.1.4") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "1czj510wj02wn4xq7f3ca0p77hm334ripn0anybabgkf55w28sg3") (y #t)))

(define-public crate-basehan-0.2.0 (c (n "basehan") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v78vjiafl5rapampch1g4vl7dzj6f7i5gxyrmyj6gmjwa044agb")))

(define-public crate-basehan-0.2.1 (c (n "basehan") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "05bdjkl39jnf396b9pyj4c8w3y3020ccf6sp923q7cvbndg5b9bf")))

(define-public crate-basehan-0.2.2 (c (n "basehan") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jcv462wwz61c41a6v64cv0ks2ilnfrb6292vfb0b7h6jc17gg5k")))

(define-public crate-basehan-0.2.3 (c (n "basehan") (v "0.2.3") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1rywkp8ja0jjzhwklrjgfjjijlv752w7gi8gamicpp64dix2chzv")))

