(define-module (crates-io ba se based64) #:use-module (crates-io))

(define-public crate-based64-0.1.0 (c (n "based64") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "05nfpfwhzfrk04fb8dpd2d3vhdh3jr6ldfgnglmg7a4n2r3smbr3") (f (quote (("alloc"))))))

(define-public crate-based64-0.1.1 (c (n "based64") (v "0.1.1") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "0qmjzkm0mcr4np434y0lqxap0x0bqijccrncgwmgziz3h54cz7sk") (f (quote (("alloc"))))))

(define-public crate-based64-0.1.2 (c (n "based64") (v "0.1.2") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "01zn03d08aydagh384nqkynp822jjpy2bjhjb5d1xvqf4w0zqpav") (f (quote (("alloc"))))))

(define-public crate-based64-0.2.0 (c (n "based64") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "1h2z3nqqhwgy6ydbcjj7x0v9ssb4amjwp26k2psj9rz057zjk96j") (f (quote (("alloc")))) (y #t)))

(define-public crate-based64-0.3.0 (c (n "based64") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "1z5p2fscycwp2d5r4vj1bz56msjdf1pfhplq61z5h66cc2a3l347") (f (quote (("alloc")))) (y #t)))

(define-public crate-based64-0.3.1 (c (n "based64") (v "0.3.1") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "1vvly9qd9ymdnashg6ppxh4ladncp1kv2iq76jn0rw4asa25cls4") (f (quote (("alloc")))) (y #t)))

(define-public crate-based64-0.4.0 (c (n "based64") (v "0.4.0") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "03zsyxyyj67rsrbjnaniqi0fia9az3rmr8c36pg7k9b4sqzl9b4s") (f (quote (("alloc")))) (y #t)))

(define-public crate-based64-0.4.1 (c (n "based64") (v "0.4.1") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "02rwnqbj8r81gwkc5d03yfvw0b8clxzx47ccmfpq1mfrxxdap41s") (f (quote (("alloc")))) (y #t)))

(define-public crate-based64-0.4.2 (c (n "based64") (v "0.4.2") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "0f4lhzywwm8czzdyfqaxclcb7nk9wkgaav3z4mb8b71fh24p7kqd") (f (quote (("alloc"))))))

(define-public crate-based64-0.4.3 (c (n "based64") (v "0.4.3") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "1rrfkv4hqx89l4wh888hzzq6vdpj1wxw7zm3hk9ir33vdhls7g6q") (f (quote (("alloc"))))))

(define-public crate-based64-0.4.4 (c (n "based64") (v "0.4.4") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "0bqm0vxk8ckqy8qfap3ag749j4lc6a49j5n12n8zvhajiw8icn2g") (f (quote (("alloc"))))))

(define-public crate-based64-0.4.5 (c (n "based64") (v "0.4.5") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "1d3kxxv6sviw84qj1pqrm7k7raypxggpziacdh1dr27m0rpir702") (f (quote (("alloc"))))))

(define-public crate-based64-0.4.6 (c (n "based64") (v "0.4.6") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "11wx9cwmw6vp9csrvsib124k8n7578qlx9qgf170vdzn1vq4yi6b") (f (quote (("alloc"))))))

(define-public crate-based64-0.4.7 (c (n "based64") (v "0.4.7") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "174va1w7d1qqlrfhdx1dpfxidn9jnfz398wrkn08w8xcyddc7wrd") (f (quote (("alloc"))))))

(define-public crate-based64-0.4.8 (c (n "based64") (v "0.4.8") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 2)))) (h "1bi2qnfp46q8rxmh1f2sz21sil1nrrwkabk50hqzh287681l9p03") (f (quote (("alloc"))))))

