(define-module (crates-io ba se base32h) #:use-module (crates-io))

(define-public crate-base32h-0.1.0 (c (n "base32h") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "1118aiwlxmgc6fa2di75qq6qwj18mlnh35p3sk20nlbx1ccmk5zy")))

(define-public crate-base32h-0.2.0 (c (n "base32h") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "14ycsnziaw7vplwk3chw9k0lnlcmxdnjzpzhywmrahcv9v7j6xik")))

