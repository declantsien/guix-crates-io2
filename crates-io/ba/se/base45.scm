(define-module (crates-io ba se base45) #:use-module (crates-io))

(define-public crate-base45-1.0.0 (c (n "base45") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0smj1ha487g840m627jgnqzigxa5nvh3jvihg1x36mcsj0f6px0s")))

(define-public crate-base45-1.0.1 (c (n "base45") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1waghx8mrmc7sk9kf04cba7fczvspdh5l1q78b42vvj7xaijnzv8")))

(define-public crate-base45-1.0.2 (c (n "base45") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1qbw0q2zy8y8x2dz7jckgvh1jpv7gg0iydmrsnyb682kj589c019")))

(define-public crate-base45-2.0.0 (c (n "base45") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "024svdwb9kwxcwf8lm5d3p3gjbc8m2k44mwa3a28p5cqmhpbhwp8")))

(define-public crate-base45-2.0.1 (c (n "base45") (v "2.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1x3ngrqwn33n4a0rar7y5p1yc98s4brx6g7skxh016f8qnhx05rn")))

(define-public crate-base45-3.0.0 (c (n "base45") (v "3.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "16m531a14f1ljnmphlanzl8fjs7k77fg5gc0dgas3h2l7rq5d256")))

(define-public crate-base45-3.1.0 (c (n "base45") (v "3.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0r55jzplsnl5wl4z8cxsiaiqwmzwd5kwcsh2dgl2cdznd9nky45k") (f (quote (("array_chunks"))))))

