(define-module (crates-io ba se baseperm) #:use-module (crates-io))

(define-public crate-baseperm-0.1.0 (c (n "baseperm") (v "0.1.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "14wgz2hb6b74cj8gq4bwcxq8g4qwpy5iny98n3b8kczd4gyb53h9")))

(define-public crate-baseperm-0.1.1 (c (n "baseperm") (v "0.1.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "06isw4yadiczlmd3ak2na018293iqkx9janb56vyggbj34y80sx3")))

(define-public crate-baseperm-0.1.2 (c (n "baseperm") (v "0.1.2") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1ds4qh7d2xfqz6g0y1j82dcjgnwxrnfmxx38cms6izz6zyv2mns0")))

(define-public crate-baseperm-0.1.3 (c (n "baseperm") (v "0.1.3") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "188b3rqm8mr479wp6qkklpgp0nqxac59wzl93azdd0kcrpwwfs0k")))

