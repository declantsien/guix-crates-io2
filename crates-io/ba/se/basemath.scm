(define-module (crates-io ba se basemath) #:use-module (crates-io))

(define-public crate-basemath-1.1.0 (c (n "basemath") (v "1.1.0") (h "1n0qiss5sx04h6mhp1gnnvk6hn1vqssy0wbyq3d2smwf8fxx5a8m")))

(define-public crate-basemath-1.1.1 (c (n "basemath") (v "1.1.1") (h "0fx6620i2ivl9ch8gd9xfpjr5sybl9jny6as0m8gz9gj153hwa2y")))

(define-public crate-basemath-2.0.1 (c (n "basemath") (v "2.0.1") (h "0kj609wj4z16f9wkgsknpar4vh2rb20xr1njgcvsrk83x9qyjlk7")))

(define-public crate-basemath-2.3.0 (c (n "basemath") (v "2.3.0") (h "0qczsaxjhn9z5y01jp481mgnhmfyid1p8v2dn76y2wmiwvagxvhp")))

(define-public crate-basemath-2.4.0 (c (n "basemath") (v "2.4.0") (h "05smki7rb2w136rgf9v91bc00q68mvl20zqyym1dbp51v8ff270r")))

(define-public crate-basemath-3.0.0 (c (n "basemath") (v "3.0.0") (h "0ilx65f3c7zvwmr1zxigm7lrbhyrgjikpvm1lvq2rj1p3326mn7w")))

