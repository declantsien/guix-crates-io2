(define-module (crates-io ba se base256u-cli) #:use-module (crates-io))

(define-public crate-base256u-cli-1.0.0 (c (n "base256u-cli") (v "1.0.0") (d (list (d (n "base256u") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "utf-8") (r "^0.7.6") (d #t) (k 0)))) (h "09bqnrybvq0d9sz5pzvcz0dkznagvlw5kf1zjsy8bsq2r2msmqwg")))

(define-public crate-base256u-cli-1.1.0 (c (n "base256u-cli") (v "1.1.0") (d (list (d (n "base256u") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "utf-8") (r "^0.7.6") (d #t) (k 0)))) (h "08gh0h05cnfykbm2zvmwwwknf2px4lzysqbshk6x5ljkykk0596v")))

(define-public crate-base256u-cli-1.1.1 (c (n "base256u-cli") (v "1.1.1") (d (list (d (n "base256u") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "utf-8") (r "^0.7.6") (d #t) (k 0)))) (h "1vdrbpmish21vbrfpkdl274qavf03f1kl98lr81n99h4ss5pq8p4")))

