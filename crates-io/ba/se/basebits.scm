(define-module (crates-io ba se basebits) #:use-module (crates-io))

(define-public crate-basebits-0.2.0 (c (n "basebits") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1zl1xmrlsfa1jsar0smd5lyib152kdng9wl4yhw7gmjjyjcan4qb")))

(define-public crate-basebits-0.2.1 (c (n "basebits") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "12hjylgabmm3w5mvx2hcdkkjhw4mq3j86jk57hxibs0bw7kfiq32")))

(define-public crate-basebits-1.0.0 (c (n "basebits") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "05l453c10ylq5lx59vbw3wnxw1nywnvzl3grzrbkw3bj66k59hsv")))

(define-public crate-basebits-1.0.1 (c (n "basebits") (v "1.0.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0snsbln7av301sdgzkbbnvjlqikbm759zr0c9sa9bfisfya4yk4p")))

(define-public crate-basebits-1.1.0 (c (n "basebits") (v "1.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1gigyvaafahfmk8jwlmy7rh54ap48bxv6glrmf9wcnfgarn58iy0")))

(define-public crate-basebits-1.2.0 (c (n "basebits") (v "1.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0zr9wydyilmd2rlnjlipgb4h9a4h1wzsn26b42aw7x7dwhrxpwv9")))

