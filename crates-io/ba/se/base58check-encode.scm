(define-module (crates-io ba se base58check-encode) #:use-module (crates-io))

(define-public crate-base58check-encode-0.1.0 (c (n "base58check-encode") (v "0.1.0") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "043vzh0904vdy1zq9zl83yfd7sd7i093qp64gjppnbql261yxd0g")))

(define-public crate-base58check-encode-0.1.1 (c (n "base58check-encode") (v "0.1.1") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1l2j7q6kcr4qk49v5s9ijwjlxxvnz3hqkp29cp0cfpc4vcsvahvp")))

