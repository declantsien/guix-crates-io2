(define-module (crates-io ba se base64-compat) #:use-module (crates-io))

(define-public crate-base64-compat-0.1.0 (c (n "base64-compat") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "0s9f1iya3344v5mc9nbzin7vqcan95cbckc5cdc2psm1qijdzws2")))

(define-public crate-base64-compat-1.0.0 (c (n "base64-compat") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1mw89d9wnisgwgxzmfjsz58611zign8jdp9hjbj4367q8qklv3as")))

