(define-module (crates-io ba se base116) #:use-module (crates-io))

(define-public crate-base116-0.1.0 (c (n "base116") (v "0.1.0") (h "0fpg9slssdd4qw785lyp5l7kxin7n6iz4pfhix561ir2b9294k52") (f (quote (("std" "alloc") ("doc_cfg") ("default" "std") ("cli-panic") ("alloc")))) (y #t)))

(define-public crate-base116-0.1.1 (c (n "base116") (v "0.1.1") (h "1va39lkdrghvpw1wh2qjvl6lizqw8k8sr4skq4zdw6053qxymakk") (f (quote (("std" "alloc") ("doc_cfg") ("default" "std") ("cli-panic") ("alloc"))))))

(define-public crate-base116-0.1.2 (c (n "base116") (v "0.1.2") (h "079yvmafi4kkr3s0n31ik7apxk7kyix829sj9q6jrvvcfi58jqvj") (f (quote (("std" "alloc") ("doc_cfg") ("default" "std") ("cli-panic") ("alloc"))))))

(define-public crate-base116-0.1.3 (c (n "base116") (v "0.1.3") (h "07yvqjm3zs5xx1nr4k2sv8nim3mwy23qa61bb8m1dzibx2fpnq3f") (f (quote (("std" "alloc") ("doc_cfg") ("default" "std") ("cli-panic") ("alloc"))))))

(define-public crate-base116-0.1.4 (c (n "base116") (v "0.1.4") (h "16g9agknrxdm2r26mlyxlx4czqglyj4k7sh1lla75z2ig4ky747p") (f (quote (("std" "alloc") ("doc_cfg") ("default" "std") ("cli-panic") ("alloc"))))))

(define-public crate-base116-0.1.5 (c (n "base116") (v "0.1.5") (h "0lz7k5ka8mqyqls0mmfvsd7g8q4cg5p9g2grfl7myym7ryrkgvph") (f (quote (("std" "alloc") ("doc_cfg") ("default" "std") ("cli-panic") ("alloc"))))))

(define-public crate-base116-0.1.6 (c (n "base116") (v "0.1.6") (h "1nw2yhia2lakzwzf6zw0griq7201d04cqxlp4fh4kgadgx9kl311") (f (quote (("std" "alloc") ("doc_cfg") ("default" "std") ("cli-panic") ("alloc"))))))

(define-public crate-base116-0.1.7 (c (n "base116") (v "0.1.7") (h "1c9v5ygyanzmz8b5byv2vl9s41idrvm014lwc3y24fs5vgpiyh4n") (f (quote (("std" "alloc") ("doc_cfg") ("default" "std") ("cli-panic") ("alloc")))) (r "1.55")))

(define-public crate-base116-0.1.8 (c (n "base116") (v "0.1.8") (h "048xfhf2yr7ibfn4jgpmdcjny7rdjwzs2hpdgkljfivsl1prb75k") (f (quote (("std" "alloc") ("doc_cfg") ("default" "std") ("cli-panic") ("alloc")))) (r "1.55")))

