(define-module (crates-io ba se base16_color_scheme) #:use-module (crates-io))

(define-public crate-base16_color_scheme-0.1.0 (c (n "base16_color_scheme") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14.0") (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 2)))) (h "05z7rkyhdsya2xl0b7pmd16swi3pw733m24wc7s7pyn7794gh3ph")))

(define-public crate-base16_color_scheme-0.2.0 (c (n "base16_color_scheme") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14.0") (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 2)))) (h "0v89v449c0afl8i2frmnvwbvkw36bkw748pw1zyda1bw9965zm6k")))

(define-public crate-base16_color_scheme-0.3.0 (c (n "base16_color_scheme") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14.0") (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 2)))) (h "0gziisk9ac2wfmz13ngdrlnbvss6pzmsp4a7q7w3y14ypwrpkbj1")))

(define-public crate-base16_color_scheme-0.3.1 (c (n "base16_color_scheme") (v "0.3.1") (d (list (d (n "hex") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14.0") (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 2)))) (h "0r9z9c9j84qnaj5ygh23ymgnacswafap9sa0z1pfwsp6842cg438")))

