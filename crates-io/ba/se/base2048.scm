(define-module (crates-io ba se base2048) #:use-module (crates-io))

(define-public crate-base2048-0.1.0 (c (n "base2048") (v "0.1.0") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "1pb3p3qd2yy8b8rgzpca9vg36iq6gb482grl9lxj0xya8pj6wc3d") (f (quote (("nightly") ("default"))))))

(define-public crate-base2048-0.1.1 (c (n "base2048") (v "0.1.1") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "0p27laj43vqrdc8as81402y6y7ibfslir9k3wk5g8x4jk67grb0i") (f (quote (("nightly") ("default"))))))

(define-public crate-base2048-0.2.0 (c (n "base2048") (v "0.2.0") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "0j79l1lvb8a0m6vajn6z2kyq95v08drb0hl74jhwlx6dmzyh2l7f") (f (quote (("nightly") ("default"))))))

(define-public crate-base2048-0.2.1 (c (n "base2048") (v "0.2.1") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "08krfm72azvg0rynpx4p9gs34gq4hvcm2bn4009v9ni28smga8b9") (f (quote (("nightly") ("default"))))))

(define-public crate-base2048-1.0.0 (c (n "base2048") (v "1.0.0") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "1qvlq8n571n45sxa2s6lrfgg93y8vglbp04d3gsdyjxncn40v3sr") (f (quote (("nightly") ("default"))))))

(define-public crate-base2048-2.0.0 (c (n "base2048") (v "2.0.0") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "1sb05i2nfwhgdjpjjnnqyarcvyg78iwxgda5jdzp3ad5x040sapa") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-base2048-2.0.1 (c (n "base2048") (v "2.0.1") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "06rig8lrlz89jns9i8xsdnqyffgfadwknhd85gxyx2awmfxax8h2") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-base2048-2.0.2 (c (n "base2048") (v "2.0.2") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "1k8piyy7y56rvydp2x2kp8q2ggcjrs895ykx8fdvphwcgr0zxx3i") (f (quote (("nightly") ("default"))))))

