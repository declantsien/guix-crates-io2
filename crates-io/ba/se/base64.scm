(define-module (crates-io ba se base64) #:use-module (crates-io))

(define-public crate-base64-0.1.0 (c (n "base64") (v "0.1.0") (h "009k9rc8rxa785wk322x4va88gkv095bmwjil1hcp845430irkqg")))

(define-public crate-base64-0.1.1 (c (n "base64") (v "0.1.1") (h "0fxvwsfaab9b4fljljc5pr9qp7dczvbzswfwvqiyfhzq2z514455")))

(define-public crate-base64-0.2.0 (c (n "base64") (v "0.2.0") (h "1856df4c6s6payc0zmvqy717jjzs4q39271715q83wbdr7ji1q9w")))

(define-public crate-base64-0.2.1 (c (n "base64") (v "0.2.1") (h "14nqvphsyr7s13g3vwblkq1p042cx1cjk9z30xh5pajl6mwy6590")))

(define-public crate-base64-0.3.0 (c (n "base64") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "= 0.3.15") (d #t) (k 2)))) (h "0yqjdgn7kmj18gq494wa93qb8j99lvj16g7aj9p74kb9xh26l58x")))

(define-public crate-base64-0.4.0 (c (n "base64") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "= 0.3.15") (d #t) (k 2)))) (h "1l4997l1k372dya1zxb6qccra81jfavpngmfsnvd115b43i0qnh6")))

(define-public crate-base64-0.4.1 (c (n "base64") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "= 0.3.15") (d #t) (k 2)))) (h "1ggjrmhkicz0blmw604pl9ivcwn7hj4i471rxhnx17nq7cn8i4lq")))

(define-public crate-base64-0.5.0 (c (n "base64") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "= 0.3.15") (d #t) (k 2)))) (h "1kszgcj1jg2adkwrb28v86kszcnllrc7qxgjd7h7xc8mfmh2z43c")))

(define-public crate-base64-0.5.1 (c (n "base64") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "= 0.3.15") (d #t) (k 2)))) (h "1wldgc3avfb9m70vfiiqy363w32w2ym0k285ras8gqy4vwr56khj")))

(define-public crate-base64-0.5.2 (c (n "base64") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "= 0.3.15") (d #t) (k 2)))) (h "0mvmhasrh9aqf4ksy7dbgzij435ra591a2b28v890xaf0q1krs9h")))

(define-public crate-base64-0.4.2 (c (n "base64") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "= 0.3.15") (d #t) (k 2)))) (h "0zrcbcksiasv133bsklvxqkhl6phc7n0ix3xm18d1z0dqn6k97cp")))

(define-public crate-base64-0.3.1 (c (n "base64") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "= 0.3.15") (d #t) (k 2)))) (h "1cpasg1xns5symc7h6kicia4mz4mm85rb44snbxmkk9r33n8lrrk")))

(define-public crate-base64-0.6.0 (c (n "base64") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "= 0.3.15") (d #t) (k 2)) (d (n "safemem") (r "^0.2.0") (d #t) (k 0)))) (h "1ja3c7wlzp3sb14f2ksp27x1mk8ycgh1391nnd7fvw01fnc4yhwn")))

(define-public crate-base64-0.7.0 (c (n "base64") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "safemem") (r "^0.2.0") (d #t) (k 0)))) (h "1hlrhii3dvvd1dk5qwch5cggvnf6p3cn89pbvazjj5vll8fxacjh")))

(define-public crate-base64-0.8.0 (c (n "base64") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "safemem") (r "^0.2.0") (d #t) (k 0)))) (h "138nld90idn33kzls89ixnkzimqkqvi12qq3x0dqw9hb8lmk8jkw")))

(define-public crate-base64-0.9.0 (c (n "base64") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "safemem") (r "^0.2.0") (d #t) (k 0)))) (h "1d6jaqrw2gwd1ray5a766b84jgfhwrx1c9qby6bjcc4r38ph7792")))

(define-public crate-base64-0.9.1 (c (n "base64") (v "0.9.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "safemem") (r "^0.2.0") (d #t) (k 0)))) (h "01zhz31xbn16sq1x922pag497w00x3hkra4ibkn1w9ys71malqwj")))

(define-public crate-base64-0.9.2 (c (n "base64") (v "0.9.2") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "safemem") (r "^0.2.0") (d #t) (k 0)))) (h "1nb70kj4vbrzbfx7nbgdnk0jw9pkn9qd7h9h9akkhwvnjhjmshc5")))

(define-public crate-base64-0.9.3 (c (n "base64") (v "0.9.3") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "safemem") (r "^0.3") (d #t) (k 0)))) (h "0hs62r35bgxslawyrn1vp9rmvrkkm76fqv0vqcwd048vs876r7a8")))

(define-public crate-base64-0.10.0 (c (n "base64") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "1qmrlwns4ckz75dxi1hap0a9bkljsrn3b5cvzgbqd3q0p3ncf7v2")))

(define-public crate-base64-0.10.1 (c (n "base64") (v "0.10.1") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "13k6bvd3n6dm7jqn9x918w65dd9xhx454bqphbnv0bkd6n9dj98b")))

(define-public crate-base64-0.11.0 (c (n "base64") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1iqmims6yvr6vwzyy54qd672zw29ipjj17p8klcr578c9ajpw6xl") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-base64-0.12.0 (c (n "base64") (v "0.12.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1czzrk3i89259m47x144x2nvzh6cz6ynnnpax7wlhgyw1b6s4p3x") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-base64-0.12.1 (c (n "base64") (v "0.12.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "0hncd72kh8i0g1hw37xlxicf3961xlcvz5ss8qvrbv6ryyxcrlak") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-base64-0.12.2 (c (n "base64") (v "0.12.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "0ryc48pp8dpx3rl1dcwn723dyfgifi4imh1f6kwd95lcqh6sy8z2") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-base64-0.12.3 (c (n "base64") (v "0.12.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1zq33had71xh48n17g4kqs96szhx3yh7qibzwi4fk217n3vz0h9l") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-base64-0.13.0 (c (n "base64") (v "0.13.0") (d (list (d (n "criterion") (r "=0.3.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1z82g23mbzjgijkpcrilc7nljpxpvpf7zxf6iyiapkgka2ngwkch") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-base64-0.20.0-alpha.1 (c (n "base64") (v "0.20.0-alpha.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)) (d (n "rstest") (r "^0.11.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.1.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)))) (h "10ia264yx8d2a49j5yanwg9f6s1blfxzw2kpa0rm24fb4kfab7hl") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.47.0")))

(define-public crate-base64-0.13.1 (c (n "base64") (v "0.13.1") (d (list (d (n "criterion") (r "=0.3.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1s494mqmzjb766fy1kqlccgfg2sdcjb6hzbvzqv2jw65fdi5h6wy") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-base64-0.20.0 (c (n "base64") (v "0.20.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.3.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "1r855djiv8rirg37w5arazk42ya5gm5gd2bww75v14w0sy02i8hf") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.57.0")))

(define-public crate-base64-0.21.0-beta.1 (c (n "base64") (v "0.21.0-beta.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.3.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "10w1wrxjcgc1iy0frn41b0lpzq6dwwy0bbz67s8ry03vdjradq34") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.57.0")))

(define-public crate-base64-0.21.0-beta.2 (c (n "base64") (v "0.21.0-beta.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.3.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "1if8pz4pr67qk9l79dyl7f367bg49lmrz3gsczpblmw87j9fsiq5") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.57.0")))

(define-public crate-base64-0.21.0-rc.1 (c (n "base64") (v "0.21.0-rc.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.3.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "1dx3fjkxrjkfi2qbla5smvnni91q8msjf5rzn3swx5glr0iv57ik") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.57.0")))

(define-public crate-base64-0.21.0 (c (n "base64") (v "0.21.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.3.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "0sidjip5b33sr6w7kasfj9qxpbda41nw0x4gjjk55g55a6mdv954") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.57.0")))

(define-public crate-base64-0.21.1 (c (n "base64") (v "0.21.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.3.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "01a1rh3sv91r60nr0afhwiaiym6bwq2k3siq39wzpf560zi327iz") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.60.0")))

(define-public crate-base64-0.21.2 (c (n "base64") (v "0.21.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.3.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "0gfffgp4jmk517hymckk5jn393dqvw78312papf047y2qpv7hhb0") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.57.0")))

(define-public crate-base64-0.21.3 (c (n "base64") (v "0.21.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.3.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "0lvf1ishhckkjwiamhqr3iwy5ddrzgvgqfkblwkcaxrxqvxwwka1") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.48.0")))

(define-public crate-base64-0.21.4 (c (n "base64") (v "0.21.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.3.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "18jhmsli1l7zn6pgslgjdrnghqnz12g68n25fv48ids3yfk3x94v") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.48.0")))

(define-public crate-base64-0.21.5 (c (n "base64") (v "0.21.5") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.3.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "1y8x2xs9nszj5ix7gg4ycn5a6wy7ca74zxwqri3bdqzdjha6lqrm") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.48.0")))

(define-public crate-base64-0.21.6 (c (n "base64") (v "0.21.6") (d (list (d (n "clap") (r "^3.2.25") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.13.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.6.0") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 2)))) (h "1fgywwz1z7qwlwfs2vs3qi4ydqyhkyh5hznwvby97sa3vd6fv7y7") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.48.0")))

(define-public crate-base64-0.21.7 (c (n "base64") (v "0.21.7") (d (list (d (n "clap") (r "^3.2.25") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.13.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.6.0") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 2)))) (h "0rw52yvsk75kar9wgqfwgb414kvil1gn7mqkrhn9zf1537mpsacx") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.48.0")))

(define-public crate-base64-0.22.0 (c (n "base64") (v "0.22.0") (d (list (d (n "clap") (r "^3.2.25") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.13.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.6.0") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 2)))) (h "0lbsphibsb0lnyswqr8mjqw6am7zh780yh62ldbbwl8lxipqcxcl") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.48.0")))

(define-public crate-base64-0.22.1 (c (n "base64") (v "0.22.1") (d (list (d (n "clap") (r "^3.2.25") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rstest") (r "^0.13.0") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.6.0") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 2)))) (h "1imqzgh7bxcikp5vx3shqvw9j09g9ly0xr0jma0q66i52r7jbcvj") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.48.0")))

