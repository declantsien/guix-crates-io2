(define-module (crates-io ba se basex-rs) #:use-module (crates-io))

(define-public crate-basex-rs-0.1.0 (c (n "basex-rs") (v "0.1.0") (h "0jawcg3wa309d44vzj1x9va9b4gmnadmjxzywbz56qmwpxxlqbi7")))

(define-public crate-basex-rs-0.1.1 (c (n "basex-rs") (v "0.1.1") (h "1kz98j7vfz9d3lc3ihpkj9a3907xyjcrn20sys45hqrzf53h3igi")))

(define-public crate-basex-rs-0.2.0 (c (n "basex-rs") (v "0.2.0") (h "196z5i7pi0gmai79asia93nlsvw4gdzcpnami8w15vvx0h6kl3vi")))

