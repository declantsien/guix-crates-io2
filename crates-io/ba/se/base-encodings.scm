(define-module (crates-io ba se base-encodings) #:use-module (crates-io))

(define-public crate-base-encodings-0.1.0 (c (n "base-encodings") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "1pv6wsanayzn5mvagf6wyg7r9xz0smz3dwpgphqk8xhjmg2i3kip")))

(define-public crate-base-encodings-0.2.0 (c (n "base-encodings") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "1c4p5w6n182ip1an1dnb3h6pd0q94fy9333fjrf9bilfz3gsc448")))

