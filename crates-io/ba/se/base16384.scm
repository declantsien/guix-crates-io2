(define-module (crates-io ba se base16384) #:use-module (crates-io))

(define-public crate-base16384-0.1.0 (c (n "base16384") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0qhx7bi1ci8p2qc4yiwalz1alag5wl07iw70gsha4fnkayvv07y8") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.60")))

