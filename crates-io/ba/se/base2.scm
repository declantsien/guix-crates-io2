(define-module (crates-io ba se base2) #:use-module (crates-io))

(define-public crate-base2-0.1.0 (c (n "base2") (v "0.1.0") (h "1simcfc2r5kp9xcmvgfggnc5c4w73j7kfdi2sbq5mhswgpchyagy")))

(define-public crate-base2-0.1.1 (c (n "base2") (v "0.1.1") (h "081bn40gq3sgq8mcy7zv6cbn3y0fz8z90cjnc0zcc3fy15zbszgg")))

(define-public crate-base2-0.2.0 (c (n "base2") (v "0.2.0") (d (list (d (n "int") (r "^0.2.5") (d #t) (k 0)))) (h "0n3ykcmyg08zsv2rk9g99g7z1c989kiqh29a6xcrci43s6jip2ag")))

(define-public crate-base2-0.2.1 (c (n "base2") (v "0.2.1") (d (list (d (n "int") (r "^0.2.6") (d #t) (k 0)))) (h "05vjz4vprb2rj7h2asq0j9l5qimnr09a89jnpywwwhwzdbwcg7nn")))

(define-public crate-base2-0.2.2 (c (n "base2") (v "0.2.2") (d (list (d (n "int") (r "^0.2.10") (d #t) (k 0)))) (h "135a3pazzskprwdcvfngpmf0zngfapgmkaf7w8y3ymqzfpyqr0xx")))

(define-public crate-base2-0.3.0 (c (n "base2") (v "0.3.0") (d (list (d (n "int") (r "^0.2.11") (d #t) (k 0)))) (h "1p2af22hjpq21nswcjiz5v05jya62mzm986vafmqjj3phzlkvsjx")))

(define-public crate-base2-0.3.1 (c (n "base2") (v "0.3.1") (d (list (d (n "int") (r "^0.3.0") (d #t) (k 0)))) (h "1gh90hj3p4hqb7brima5lv57ai0j36b0d0zmr6bdk2svar1czmph")))

