(define-module (crates-io ba se base62-uuid) #:use-module (crates-io))

(define-public crate-base62-uuid-0.1.0 (c (n "base62-uuid") (v "0.1.0") (d (list (d (n "base62") (r "^1.1.5") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hif92phywwpi4k0j1x2pbz7k1c0bj4f1zz21hyxqf76mb010z7f")))

(define-public crate-base62-uuid-0.2.0 (c (n "base62-uuid") (v "0.2.0") (d (list (d (n "base62") (r "^1.1.5") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0137n6l6p08qjh6hr34jka9kyr351wlylhjnzgz4n7pyigr88468")))

(define-public crate-base62-uuid-1.0.0 (c (n "base62-uuid") (v "1.0.0") (d (list (d (n "base62") (r "^1.1.5") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "102qq69ldc9ciigzrwadswqyjxcjxyhx386xcrafrc8lxwgi7ndv")))

(define-public crate-base62-uuid-1.1.0 (c (n "base62-uuid") (v "1.1.0") (d (list (d (n "base62") (r "^1.1.5") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jmcbfv1fbw2rwdd8164k3wqrbidx6jl83ra2rgmmf60am1ajh30")))

