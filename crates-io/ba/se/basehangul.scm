(define-module (crates-io ba se basehangul) #:use-module (crates-io))

(define-public crate-basehangul-0.1.0 (c (n "basehangul") (v "0.1.0") (d (list (d (n "encoding-index-korean") (r "*") (d #t) (k 0)))) (h "0xksk5wj747wzy4675hb6shbgz7qk4q1m53amzmcs7lzlrz7d0rk")))

(define-public crate-basehangul-0.1.1 (c (n "basehangul") (v "0.1.1") (d (list (d (n "encoding-index-korean") (r "*") (d #t) (k 0)))) (h "0lvndwqm7marwwcz7zdln1g88vpl66sz8649ny4qvk8v9r7yzpw0")))

(define-public crate-basehangul-0.1.2 (c (n "basehangul") (v "0.1.2") (d (list (d (n "encoding-index-korean") (r "= 1.2.20141219") (d #t) (k 0)))) (h "0f82y1x9x3lyzii38x1wk5kskrk3xd2f451vgk6fzi3al8d6z2b3")))

(define-public crate-basehangul-0.1.3 (c (n "basehangul") (v "0.1.3") (d (list (d (n "encoding-index-korean") (r "= 1.2.20141220") (d #t) (k 0)))) (h "03yjwcph4gllc4dwvz59bwbn0wblivvzv0ah8cjmrfq010wb8dhw")))

(define-public crate-basehangul-0.1.4 (c (n "basehangul") (v "0.1.4") (d (list (d (n "encoding-index-korean") (r "~1.20141219.0") (d #t) (k 0)))) (h "16acl9pz7q8hyrixi2wyf4sgvdqwhrpbvy6whzdra3md6dq0jy8l")))

(define-public crate-basehangul-0.1.5 (c (n "basehangul") (v "0.1.5") (d (list (d (n "encoding-index-korean") (r "~1.20141219.0") (d #t) (k 0)))) (h "1j283n7bbr3xf15m8f0w6cqgzsm72wqiqjyfah6m28wdbalx3a7a")))

(define-public crate-basehangul-0.1.6 (c (n "basehangul") (v "0.1.6") (d (list (d (n "encoding-index-korean") (r "~1.20141219.0") (d #t) (k 0)))) (h "00s28ff5mqlfs9ycy61bj88q3hawyvxny8ybdxjfqaddi1ca7v8m")))

(define-public crate-basehangul-0.1.7 (c (n "basehangul") (v "0.1.7") (d (list (d (n "encoding-index-korean") (r "~1.20141219.0") (d #t) (k 0)))) (h "1mars7xv740q1l0s0k6i7ag3nxifirciiar6wmy0q381aqvj0fk6")))

(define-public crate-basehangul-0.1.8 (c (n "basehangul") (v "0.1.8") (d (list (d (n "encoding-index-korean") (r "~1.20141219.0") (d #t) (k 0)))) (h "1zqad4r8vxzxsijfmggr6xqqd3svqwgv71s01829rjy27z41bf8p")))

(define-public crate-basehangul-0.1.9 (c (n "basehangul") (v "0.1.9") (d (list (d (n "encoding-index-korean") (r "~1.20141219.2") (d #t) (k 0)))) (h "1xqxjqisrpbwsjfwsbi2db464d66z1hrrb6ci7fhc69q9bszrwa7")))

(define-public crate-basehangul-0.1.10 (c (n "basehangul") (v "0.1.10") (d (list (d (n "encoding-index-korean") (r "~1.20141219.5") (d #t) (k 0)))) (h "0x9a2i2an2mzdfzms0hk6wkfyaxzg5iklpkdr6qwsckqs7cj4la6")))

