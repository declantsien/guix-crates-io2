(define-module (crates-io ba se base32768) #:use-module (crates-io))

(define-public crate-base32768-0.1.0 (c (n "base32768") (v "0.1.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "0k0y36vlvw5plz821a2bd78lw77wkbhs5mvwz3l7rfcx0klbqmyp")))

