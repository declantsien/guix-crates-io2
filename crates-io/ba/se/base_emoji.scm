(define-module (crates-io ba se base_emoji) #:use-module (crates-io))

(define-public crate-base_emoji-1.0.0 (c (n "base_emoji") (v "1.0.0") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1hpi246jh9zqhsqysllvpj3rm79nwk6v5a6cr2n33f9jvm7sb93b")))

(define-public crate-base_emoji-1.1.0 (c (n "base_emoji") (v "1.1.0") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 0)))) (h "1kwg945d1s58kznxs8sb6ah8zihvvp461lvcbkiyz3gz0r28crcn")))

