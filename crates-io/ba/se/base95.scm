(define-module (crates-io ba se base95) #:use-module (crates-io))

(define-public crate-base95-0.1.0 (c (n "base95") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0l1fykq3wzb9b7yifglcwrckcsin6ynxckbxcw9hylddlq1qjm65")))

(define-public crate-base95-0.1.1 (c (n "base95") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "07csc8yalrd893g7adq2g3zv45pcjwqihzx0fnxndpck7nkfy3i7")))

