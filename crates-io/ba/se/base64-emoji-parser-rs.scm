(define-module (crates-io ba se base64-emoji-parser-rs) #:use-module (crates-io))

(define-public crate-base64-emoji-parser-rs-0.1.0 (c (n "base64-emoji-parser-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v7f0zpcwmv6rlqpjk81rh54m1n84hzwxwws5nscvs4hbf0jnlaf")))

