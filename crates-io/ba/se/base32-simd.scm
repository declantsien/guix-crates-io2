(define-module (crates-io ba se base32-simd) #:use-module (crates-io))

(define-public crate-base32-simd-0.0.1 (c (n "base32-simd") (v "0.0.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 2)) (d (n "getrandom") (r "^0.2.7") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "simd-abstraction") (r "^0.7.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.32") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "178kl0n8cn2a54ndpf8308jaccnzxa5bvc5dml43sf6gn8m0389h") (f (quote (("unstable" "simd-abstraction/unstable") ("std" "alloc" "simd-abstraction/std") ("detect" "simd-abstraction/detect") ("default" "std" "detect") ("alloc" "simd-abstraction/alloc")))) (y #t) (r "1.61")))

