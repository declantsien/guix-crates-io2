(define-module (crates-io ba se base-converter) #:use-module (crates-io))

(define-public crate-base-converter-2.0.0-alpha.0 (c (n "base-converter") (v "2.0.0-alpha.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0vlf0yk0g17d2mplapdsrg3vrrd810mb7wnh9vkxfy3swk9qjgmr")))

(define-public crate-base-converter-2.0.0-alpha.1 (c (n "base-converter") (v "2.0.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0m0hlfghjasrry4xwkmbk99jqg6c1qc9x5v3mi3rkk91z0b9krhl") (s 2) (e (quote (("bin" "dep:clap"))))))

(define-public crate-base-converter-2.0.0-alpha.2 (c (n "base-converter") (v "2.0.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1nvfmf44r9l9x5f7191b4zz0b2lyfkl6l5qsjgyp4j4bp968hjjd") (s 2) (e (quote (("bin" "dep:clap"))))))

(define-public crate-base-converter-2.0.0-alpha.3 (c (n "base-converter") (v "2.0.0-alpha.3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "12bl7ys0gc7wdxm6ssh3pqxwzdclfav2qjrnxv7nrdqrgm2db5qy") (s 2) (e (quote (("bin" "dep:clap"))))))

(define-public crate-base-converter-0.0.0 (c (n "base-converter") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)))) (h "13v0ps9rbwrg2kmjhf650sx077hw4ilkpkr2sh9msrzfjqqiw4nb") (s 2) (e (quote (("wasm" "dep:wasm-bindgen") ("bin" "dep:clap"))))))

(define-public crate-base-converter-2.0.0-beta.3 (c (n "base-converter") (v "2.0.0-beta.3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)))) (h "1l1jh9kzhb0f7kchi92anax925zv255ycjlbdsrr93hwb504j9cs") (s 2) (e (quote (("wasm" "dep:wasm-bindgen") ("bin" "dep:clap"))))))

(define-public crate-base-converter-2.0.0-beta.5 (c (n "base-converter") (v "2.0.0-beta.5") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)))) (h "1kmml9lb8pqxsix7xd01qwph1y6l43dgj4n84842qly33qk9kjg2") (s 2) (e (quote (("wasm" "dep:wasm-bindgen") ("bin" "dep:clap"))))))

(define-public crate-base-converter-2.0.0-beta.6 (c (n "base-converter") (v "2.0.0-beta.6") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)))) (h "1ch74sl5z6vrknp7cav9sy3fjpddh4968als1k6crlyfmx8wb3ky") (s 2) (e (quote (("wasm" "dep:wasm-bindgen") ("bin" "dep:clap"))))))

(define-public crate-base-converter-2.0.0 (c (n "base-converter") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (o #t) (d #t) (k 0)))) (h "075mkczbbgddbpr6ghdm52yg2j8rs7amlz69q31h9ywwvdfh832f") (s 2) (e (quote (("wasm" "dep:wasm-bindgen") ("bin" "dep:clap"))))))

