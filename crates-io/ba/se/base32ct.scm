(define-module (crates-io ba se base32ct) #:use-module (crates-io))

(define-public crate-base32ct-0.0.0 (c (n "base32ct") (v "0.0.0") (h "1q2136nrr91hfv58z5a4j0vmpp20fn8qxs3yq4fgclw94f6kcb81")))

(define-public crate-base32ct-0.1.0 (c (n "base32ct") (v "0.1.0") (d (list (d (n "base32") (r "^0.4") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1v2mlk6qnqn9c1bj4cvhrwrgk0mp8005hgb8cdr8b89lz6bb64s5") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-base32ct-0.2.0 (c (n "base32ct") (v "0.2.0") (d (list (d (n "base32") (r "^0.4") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1jcqidjrfn5j7qk5i23kmjzzfdmlq2jr20q0c6vmmb9hdw0n8rir") (f (quote (("std" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-base32ct-0.2.1 (c (n "base32ct") (v "0.2.1") (d (list (d (n "base32") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "=1.2.0") (f (quote ("std"))) (k 2)))) (h "18il86pixkm5igxq0xax4m4j8w0ck6cax1bhay65hm5hmp2rap1f") (f (quote (("std" "alloc") ("alloc")))) (r "1.60")))

