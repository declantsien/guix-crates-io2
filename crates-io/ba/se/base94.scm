(define-module (crates-io ba se base94) #:use-module (crates-io))

(define-public crate-base94-0.1.0 (c (n "base94") (v "0.1.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1kdx3mn8qmqrfvh84syzqya2f3g7b79y6gzkz44zzccb7l0wp8rd")))

(define-public crate-base94-0.2.0 (c (n "base94") (v "0.2.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1kwawcx89ghxji6apc0gf6rlymld2yvbbq8zlxsx8lrlv5ngx103")))

(define-public crate-base94-0.3.0 (c (n "base94") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "16sfm7x0vc0zdq0r8zmyv0w56f0vy0kw0ffab1nk57z2dnl3n4yw")))

