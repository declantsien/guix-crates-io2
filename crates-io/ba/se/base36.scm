(define-module (crates-io ba se base36) #:use-module (crates-io))

(define-public crate-base36-0.0.1 (c (n "base36") (v "0.0.1") (d (list (d (n "base-x") (r "^0.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1ibppxnij04mdd88wj6127i2vpm6xf77wygcwl9727r7q7fnphmr")))

