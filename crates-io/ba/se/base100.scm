(define-module (crates-io ba se base100) #:use-module (crates-io))

(define-public crate-base100-0.2.0 (c (n "base100") (v "0.2.0") (d (list (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)))) (h "0r9ba77sabfng756n0v6rbng4zxd4d6z7x1wjrz3blnwwdlsnsn0")))

(define-public crate-base100-0.3.0 (c (n "base100") (v "0.3.0") (d (list (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "stdsimd") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "0hm16m9nlvj30w5sfdw8zbqrc4jq22vh9fis6v9x9ln3fr9q82n4") (f (quote (("simd" "stdsimd"))))))

(define-public crate-base100-0.4.0 (c (n "base100") (v "0.4.0") (d (list (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "stdsimd") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "1az0xsyp00iiff03n8bdd3g5njxl6sqmh6jq81yw3f8gczzih41g") (f (quote (("simd" "stdsimd"))))))

(define-public crate-base100-0.4.1 (c (n "base100") (v "0.4.1") (d (list (d (n "clap") (r "^2.19") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "stdsimd") (r "^0.0.3") (o #t) (d #t) (k 0)))) (h "03zfrj5afmadwv2ja1c9smsvpfnz5mklczs39wx5xp92zs5v2gvx") (f (quote (("simd" "stdsimd"))))))

