(define-module (crates-io ba se base64_img_cli) #:use-module (crates-io))

(define-public crate-base64_img_cli-0.1.0 (c (n "base64_img_cli") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "infer") (r "^0.13.0") (d #t) (k 0)))) (h "1ff9vjyi2l9m2wlklx4z1hp0nq89zknczqjll6ni3qkmm857xlka")))

(define-public crate-base64_img_cli-0.1.1 (c (n "base64_img_cli") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "infer") (r "^0.13.0") (d #t) (k 0)))) (h "0xwrq7i3yy0shlhsdsnyys7i12r09d0vyx1hzmy0v8ianm4xfi22")))

