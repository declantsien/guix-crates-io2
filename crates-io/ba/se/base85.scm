(define-module (crates-io ba se base85) #:use-module (crates-io))

(define-public crate-base85-1.0.0 (c (n "base85") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0wwnamy6g40slvhs1js5dz0sdar8qldc4hsfd2rf7avb16zzsfjh")))

(define-public crate-base85-1.0.1 (c (n "base85") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0jv4ki0znb87p5y55k6vlvcpnkrfh2navbp4mshyjr3fj2l498qy")))

(define-public crate-base85-1.1.0 (c (n "base85") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "16bhwfgpk8lmh74gf83xqj81lhiwis8m25g6s9kmgz0gkkw3as1f")))

(define-public crate-base85-1.1.1 (c (n "base85") (v "1.1.1") (h "17pb2lcgcdbfyaf8jzqn390d0cnw2pi9hm8wp2fwyff593g8rdl8")))

(define-public crate-base85-1.2.1 (c (n "base85") (v "1.2.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (k 0)))) (h "01ngpmkiw91v54g37rsid67w8v5ckw53m7svzszarfvc97wsiyxg") (y #t)))

(define-public crate-base85-2.0.0 (c (n "base85") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (k 0)))) (h "0yc0f7ir4wv9pq42l2nkb6isb9rb5z819gdmi5k64z13rax5p49n")))

