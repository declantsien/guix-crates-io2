(define-module (crates-io ba se base64-lt) #:use-module (crates-io))

(define-public crate-base64-lt-1.0.0 (c (n "base64-lt") (v "1.0.0") (d (list (d (n "lib-base64") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (k 0)))) (h "09y6bc4rk6mlqmdaq3m4bhjsckxvjxcki8n5fzxnxcbx54wyvmcb")))

(define-public crate-base64-lt-1.0.1 (c (n "base64-lt") (v "1.0.1") (d (list (d (n "lib-base64") (r "^1.0") (d #t) (k 0)))) (h "0gfn8nbha9ydk0zwijwbs251a49pxpba9ijb50bv9q2xv4s66w0y")))

(define-public crate-base64-lt-1.0.2 (c (n "base64-lt") (v "1.0.2") (d (list (d (n "lib-base64") (r "^1.0") (d #t) (k 0)))) (h "0agbr2sn33afsf7q16x4l4z8yw2m2b1icak85yl3hs5f374pxb0p")))

(define-public crate-base64-lt-1.0.3 (c (n "base64-lt") (v "1.0.3") (d (list (d (n "lib-base64") (r "^1.0") (d #t) (k 0)))) (h "0ir4mc7mpn65f6zkf5bd6v78lsvw0gnfhq7d52jjpgnrxxqywbaq")))

(define-public crate-base64-lt-1.0.4 (c (n "base64-lt") (v "1.0.4") (d (list (d (n "lib-base64") (r "^2.0") (d #t) (k 0)))) (h "0hifvyngzi8rjr3cxqgi3wivyziwk1zh1syxrzybs4xqzz3rmq74")))

(define-public crate-base64-lt-1.0.5 (c (n "base64-lt") (v "1.0.5") (d (list (d (n "lib-base64") (r "^2.0") (d #t) (k 0)))) (h "18r89gzh6i9r7v8mrbaiqp0x10c3zljfnckxp95dqyq0v9nj10m5")))

