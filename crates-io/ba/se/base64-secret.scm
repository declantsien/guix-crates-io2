(define-module (crates-io ba se base64-secret) #:use-module (crates-io))

(define-public crate-base64-secret-0.1.0 (c (n "base64-secret") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)))) (h "17005l874wr2kh1y4ajvqsjpy797azh8zdr717xpkiy41q0l03ik")))

(define-public crate-base64-secret-0.1.1 (c (n "base64-secret") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)))) (h "1hy1vlp6sxivzyrx8v73ymmy63dgz7d94mdvk5yblcin4p94ifak")))

