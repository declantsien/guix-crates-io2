(define-module (crates-io ba se base64id-derive) #:use-module (crates-io))

(define-public crate-base64id-derive-0.4.0-alpha.0 (c (n "base64id-derive") (v "0.4.0-alpha.0") (d (list (d (n "base64id-core") (r "=0.4.0-alpha.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02lnk9a892s4i4qkidvjrlkj4rpiavg21fl4k8nw2ddw3rmnakrh")))

