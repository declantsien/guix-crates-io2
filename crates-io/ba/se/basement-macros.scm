(define-module (crates-io ba se basement-macros) #:use-module (crates-io))

(define-public crate-basement-macros-0.0.1 (c (n "basement-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0w1z9gkz2g7b194qqvi0lwi9n3frlwbmbq1aqnbnn438zy4z51ni")))

