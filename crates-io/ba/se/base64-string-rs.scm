(define-module (crates-io ba se base64-string-rs) #:use-module (crates-io))

(define-public crate-base64-string-rs-0.0.1 (c (n "base64-string-rs") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1yqszh773dqp1iqmzf8fv4pkaxl9nc5r6i8lhxgb4xg76pfiwfb8")))

