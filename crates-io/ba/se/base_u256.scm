(define-module (crates-io ba se base_u256) #:use-module (crates-io))

(define-public crate-base_u256-0.1.0 (c (n "base_u256") (v "0.1.0") (d (list (d (n "ux") (r "^0.1.3") (d #t) (k 0)))) (h "062wngwkaa0sw3yir2b7pr31w42rcmb2pzmhskrcdmy3akgrb914")))

(define-public crate-base_u256-0.1.1 (c (n "base_u256") (v "0.1.1") (d (list (d (n "ux") (r "^0.1.3") (d #t) (k 0)))) (h "1diqapqyky2w2zvribyxg1mrr9h80g0j1n3fg77p8wrhibpzmgb9")))

