(define-module (crates-io ba se base256u) #:use-module (crates-io))

(define-public crate-base256u-1.0.0 (c (n "base256u") (v "1.0.0") (h "06vaysbgxfnylpfc9lmh7h7cirxqqsqk6xhisd2r89fhjiddlfl1")))

(define-public crate-base256u-1.0.1 (c (n "base256u") (v "1.0.1") (h "0qs9n8w06d9n2hwcz0xwcblwab0qrns2ljfkfrbmbi9fkg98wppy")))

(define-public crate-base256u-1.1.0 (c (n "base256u") (v "1.1.0") (h "1g77pvgyis46nb18r485l3sagidaais1sikf0ybdy6y2qn1cj9pc")))

(define-public crate-base256u-1.1.1 (c (n "base256u") (v "1.1.1") (h "0c0b96frvbl2lhvhqjvi8f3jgs6rbcbf5vqxdmm72zyz14swgphj")))

