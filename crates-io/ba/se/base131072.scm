(define-module (crates-io ba se base131072) #:use-module (crates-io))

(define-public crate-base131072-0.1.0 (c (n "base131072") (v "0.1.0") (h "0qkvl9bndfdgqhk1qjabklxy4ra3p032qam21sglbrfg38s8ihwf")))

(define-public crate-base131072-0.1.1 (c (n "base131072") (v "0.1.1") (h "0jy7qva4a3pp997m3scfy1w32yl52awhbjkiybj9hr46mn3cx1xs") (f (quote (("std") ("default" "std"))))))

(define-public crate-base131072-0.1.2 (c (n "base131072") (v "0.1.2") (h "00f3d0bjdvxdrj88fscibafpmrl1gaaibxcljiam865fq4ndbf2q") (f (quote (("std") ("default" "std"))))))

