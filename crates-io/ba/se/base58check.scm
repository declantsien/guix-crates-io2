(define-module (crates-io ba se base58check) #:use-module (crates-io))

(define-public crate-base58check-0.0.1 (c (n "base58check") (v "0.0.1") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "12wr65jcghwgc5kkzc50nim27j594f5nrld99yifgmyw3zvq0742")))

(define-public crate-base58check-0.1.0 (c (n "base58check") (v "0.1.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "16yrfifrk7lxd0c9qcv3dnpj39s4cx3f5aka2dgm310ck96gxqif")))

