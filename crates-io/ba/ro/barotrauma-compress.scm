(define-module (crates-io ba ro barotrauma-compress) #:use-module (crates-io))

(define-public crate-barotrauma-compress-1.0.0 (c (n "barotrauma-compress") (v "1.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0bxl1v5pbc9l4rk69xpl3z1n421c5r53mkr16a8ih2r05r284m8d")))

