(define-module (crates-io ba us baus) #:use-module (crates-io))

(define-public crate-baus-0.1.0 (c (n "baus") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)))) (h "00m7hs5xxmwdl6lrl2jpb78iwxddkzzy491rcc7hd209kaqzcqxi")))

(define-public crate-baus-0.2.0 (c (n "baus") (v "0.2.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)))) (h "195zmmmd0hqd0i6l73nnhpyzw1v2zrbqgvdzs69b1y6w91xgklj2")))

