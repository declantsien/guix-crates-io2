(define-module (crates-io ba be babel-bridge) #:use-module (crates-io))

(define-public crate-babel-bridge-0.2.0 (c (n "babel-bridge") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0pn74qshnmjqqgllyj0qy3rzbvv05xy03ar91schpbddklgiyx7b")))

(define-public crate-babel-bridge-0.2.1 (c (n "babel-bridge") (v "0.2.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "13bxnpcdsnhgh6qrm54n0m7crpsxz0fdpbp1ijx1inq6a9xji1j0")))

(define-public crate-babel-bridge-0.2.2 (c (n "babel-bridge") (v "0.2.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0f9gvvwzbqp502hq4fgh907q22jjg44c6k1wfiwc1wl67yb11b4s")))

(define-public crate-babel-bridge-0.3.0 (c (n "babel-bridge") (v "0.3.0") (h "0knw99x4arls5zqxkllb9zdags0igv79igjxhcznywcbyx0imfhl")))

