(define-module (crates-io ba be babel) #:use-module (crates-io))

(define-public crate-babel-0.1.0 (c (n "babel") (v "0.1.0") (h "1i7ii7vhqhxilpkr8ca4yds1gmvkqmlvs29k2nicg47vfraryfm5")))

(define-public crate-babel-0.1.1 (c (n "babel") (v "0.1.1") (h "1z068qkfw8s9883512ssd9d7d9l71jrpbq3p69b7f0gn00jkbr64")))

