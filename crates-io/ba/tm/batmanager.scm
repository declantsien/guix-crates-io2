(define-module (crates-io ba tm batmanager) #:use-module (crates-io))

(define-public crate-batmanager-1.0.0 (c (n "batmanager") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)))) (h "0hhcszqccswjm9b11a3bk47z61dcmqyajw16ga59kwyanl07n069")))

(define-public crate-batmanager-1.0.1 (c (n "batmanager") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)))) (h "1h09vj1ri2rhkj0gvvp73w9syi8sficqfng66j51cbs3452563wg")))

(define-public crate-batmanager-1.0.2 (c (n "batmanager") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)))) (h "0prsnz0zy0y0pl798d0vrkvz2hqr311fx52qd0yn9aq7kgv9vn3d")))

