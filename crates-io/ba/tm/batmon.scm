(define-module (crates-io ba tm batmon) #:use-module (crates-io))

(define-public crate-batmon-0.0.1 (c (n "batmon") (v "0.0.1") (d (list (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "humantime") (r "^1.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "starship-battery") (r "^0.8") (d #t) (k 0)) (d (n "stderrlog") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)) (d (n "tui") (r "^0.8") (f (quote ("crossterm"))) (k 0)))) (h "1qd8wn3rcqla9kz4b8gzw2qzncs9ysbl012k4wiaha92dy2c9vr3")))

