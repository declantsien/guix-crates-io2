(define-module (crates-io ba lt balter-macros) #:use-module (crates-io))

(define-public crate-balter-macros-0.1.0 (c (n "balter-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "19fzy3db6a2py1a8qg6d5qzn8p45hkkhmhqz6mfb1jw6z9cbr5dc")))

(define-public crate-balter-macros-0.1.1 (c (n "balter-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0ahih0s6csn8j1f5hffxyaimyf86aqvx70sfivd64mcrygn59b36")))

(define-public crate-balter-macros-0.1.2 (c (n "balter-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "1nx2jspg29xr50v207ip16mhbh362vxrxlyzl1dwj8nn4wh6i57s")))

(define-public crate-balter-macros-0.1.3 (c (n "balter-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0l07p7g99xsq3clsygmsqz8w8byvmgc1imv57ksby2lqx1mp107m")))

(define-public crate-balter-macros-0.1.4 (c (n "balter-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0l3i11x49n99x90aa65a1vlf1863yhk652dh7ddn9af8lzzfb9ia")))

(define-public crate-balter-macros-0.2.0 (c (n "balter-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "1ybymr2jdbcznkb3895ky2m26ihgq1mkq0c7zg11l7wi0jrgf98j")))

(define-public crate-balter-macros-0.2.1 (c (n "balter-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "08ndzhksbxyziscq88p452j4d24s3wkylahxkhxrnbfsvn886ann")))

(define-public crate-balter-macros-0.3.0 (c (n "balter-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0ihaqnycrv8ada0z0rfhlq37l7819swibgibdfmr5563qmlx4lzj")))

