(define-module (crates-io k1 #{92}# k1921vk035-pac) #:use-module (crates-io))

(define-public crate-k1921vk035-pac-0.1.0 (c (n "k1921vk035-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1lg22gknajhyxqbwx9lwrdj0inz5rp8khwjwmqcan2zqdk3pvk0q") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-k1921vk035-pac-0.1.1 (c (n "k1921vk035-pac") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "01xrv7d78bhcg60sfm3m557z6ny6z8zp52iikwac34harv2pzyj0") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-k1921vk035-pac-0.1.2 (c (n "k1921vk035-pac") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "12cs6zppdc8q25qsycf7fwcqja1bj66w7k2jwalzvncs66jbm19q") (f (quote (("rt" "cortex-m-rt/device"))))))

