(define-module (crates-io k1 #{92}# k1921vk01t-pac) #:use-module (crates-io))

(define-public crate-k1921vk01t-pac-0.1.0 (c (n "k1921vk01t-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.10") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.11") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1ksq6m4fjlfqlkdm7yyijr1zg8qi5h8x0yxm3935kaqygn5ydxzs") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-k1921vk01t-pac-0.1.1 (c (n "k1921vk01t-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.10") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.11") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0j4hmfx0igzjyh7v59v0sdxhmw65jn8dflw48mrs9rfvlwl4ib7y") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-k1921vk01t-pac-0.1.2 (c (n "k1921vk01t-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.10") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.11") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "01xdfp472pgfjr756v2c6vmz8499r3nzr0i19ll41j2hi1p174nv") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-k1921vk01t-pac-0.1.3 (c (n "k1921vk01t-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.10") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.11") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1lwx1vf7fvb2ff4aw9radc752jwc1bff6i7spb56h5gdkag6gplz") (f (quote (("rt" "cortex-m-rt/device"))))))

