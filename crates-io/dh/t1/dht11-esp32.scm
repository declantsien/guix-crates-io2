(define-module (crates-io dh t1 dht11-esp32) #:use-module (crates-io))

(define-public crate-dht11-esp32-0.1.0 (c (n "dht11-esp32") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embuild") (r "^0.31.0") (d #t) (k 1)) (d (n "esp-idf-hal") (r "^0.40.1") (d #t) (k 0)) (d (n "esp-idf-sys") (r "^0.32.1") (f (quote ("binstart"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0yy4r30q8w0nxm47xjhhfjwf12c7jh6xdp7n9xc6vkavsr26wp1b") (f (quote (("pio" "esp-idf-sys/pio"))))))

