(define-module (crates-io dh b- dhb-postgres-heroku) #:use-module (crates-io))

(define-public crate-dhb-postgres-heroku-0.1.0-alpha.0 (c (n "dhb-postgres-heroku") (v "0.1.0-alpha.0") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "postgres") (r "^0.16.0-rc.2") (d #t) (k 0)) (d (n "postgres-openssl") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "1x7hnjlsnnbx1s0vshdg4rza5igh00ghhiih7mr1f9dnyqbggypw")))

(define-public crate-dhb-postgres-heroku-0.1.0-alpha.1 (c (n "dhb-postgres-heroku") (v "0.1.0-alpha.1") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "postgres") (r "^0.16.0-rc.2") (d #t) (k 0)) (d (n "postgres-openssl") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "0y6piqzi8m25m704kcl2hh02gcas6s385f1lqslay1ba4p2v7lg1")))

(define-public crate-dhb-postgres-heroku-0.1.0-alpha.2 (c (n "dhb-postgres-heroku") (v "0.1.0-alpha.2") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "postgres") (r "^0.16.0-rc.2") (d #t) (k 0)) (d (n "postgres-openssl") (r "^0.2.0-rc.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.15.0-rc.1") (d #t) (k 0)))) (h "171mkvp3nffza739q957jvhynwwkkyak3ajb1cqs0lgsqgzh5dcr")))

(define-public crate-dhb-postgres-heroku-0.1.0-alpha.3 (c (n "dhb-postgres-heroku") (v "0.1.0-alpha.3") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "postgres") (r "^0.16.0-rc.2") (d #t) (k 0)) (d (n "postgres-openssl") (r "^0.2.0-rc.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.15.0-rc.1") (d #t) (k 0)))) (h "1lbiczn7gqxm8anzwg777pp0r7iryq1pj43qyr25w47wflmz770y")))

(define-public crate-dhb-postgres-heroku-0.1.0-alpha.4 (c (n "dhb-postgres-heroku") (v "0.1.0-alpha.4") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "postgres") (r "^0.16.0-rc.2") (f (quote ("with-serde_json-1" "with-chrono-0_4" "with-uuid-0_7"))) (d #t) (k 0)) (d (n "postgres-openssl") (r "^0.2.0-rc.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.15.0-rc.1") (d #t) (k 0)))) (h "1lfdzq61cjvb2l0fpaxyxgam631v3s8jjg8k3crvywmv5s0i3dl2")))

