(define-module (crates-io dh b- dhb-heroku-postgres-client) #:use-module (crates-io))

(define-public crate-dhb-heroku-postgres-client-0.1.0-alpha.0 (c (n "dhb-heroku-postgres-client") (v "0.1.0-alpha.0") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "postgres") (r "^0.16.0-rc.2") (d #t) (k 0)) (d (n "postgres-openssl") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "0waq16frx7pm554xavks8qd9k2s5vz4f8hrmaaqi221dzjzxqg1a")))

