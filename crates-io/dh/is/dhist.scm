(define-module (crates-io dh is dhist) #:use-module (crates-io))

(define-public crate-dhist-0.1.0 (c (n "dhist") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "cargo"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "0ndl371dfyh3i4j4hl5ikzxpi8pfkqimljkcgibfr6gqcwgm1gvq")))

(define-public crate-dhist-0.1.1 (c (n "dhist") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (f (quote ("color" "cargo"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "0wmkcjq69q5f8gc7sw9ihs9lhmn09gdv8y13i437rkkdz96zzmwl")))

