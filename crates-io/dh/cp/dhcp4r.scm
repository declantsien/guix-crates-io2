(define-module (crates-io dh cp dhcp4r) #:use-module (crates-io))

(define-public crate-dhcp4r-0.1.0 (c (n "dhcp4r") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1903agigsh1w5is772phhjqc78r5334s9q291qjwfaamv5pk99yq")))

(define-public crate-dhcp4r-0.2.0 (c (n "dhcp4r") (v "0.2.0") (d (list (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "13mfizcd8sn9iiy6m4lv9g7sgcq7cv59xq71f6647q0mw4pz0287")))

(define-public crate-dhcp4r-0.2.1 (c (n "dhcp4r") (v "0.2.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 2)))) (h "11kffg8dsmb86ph70r47s6yv2xfarnk2nl49l6gja7bl44sxkvri")))

(define-public crate-dhcp4r-0.2.2 (c (n "dhcp4r") (v "0.2.2") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 2)))) (h "0paq13i9iz5545f5aw8ijxflw20fvb3k8s4m3inr5q19q5rirpbc")))

(define-public crate-dhcp4r-0.2.3 (c (n "dhcp4r") (v "0.2.3") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 2)))) (h "0f5v6a9bqx5z3gnrvh5066p4fzksg4k3gldx9d8rm2q76msckaxg")))

