(define-module (crates-io dh cp dhcprs) #:use-module (crates-io))

(define-public crate-dhcprs-0.1.0 (c (n "dhcprs") (v "0.1.0") (d (list (d (n "eui48") (r "^1.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "129bcs8d75gjhkmivcgvj2pqki510g3ppl9ma3q9cxds1l164c1d")))

(define-public crate-dhcprs-0.1.1 (c (n "dhcprs") (v "0.1.1") (d (list (d (n "eui48") (r "^1.1.0") (d #t) (k 0)))) (h "13qpzg9s5xzzfr287y9pv5817igwdlqwjdj0a87xafn05x2a4jvr")))

(define-public crate-dhcprs-0.1.2 (c (n "dhcprs") (v "0.1.2") (d (list (d (n "eui48") (r "^1.1.0") (d #t) (k 0)))) (h "1sjpg90j6miyv4b743rcisc25frqz8hm3bs7c9mca5dm07x4jn7v")))

(define-public crate-dhcprs-0.1.3 (c (n "dhcprs") (v "0.1.3") (d (list (d (n "eui48") (r "^1.1.0") (d #t) (k 0)))) (h "03inq174ir4b17k3mics1aa0pmjzx3rjzd3xkf6qfaqbgp9f36xg")))

