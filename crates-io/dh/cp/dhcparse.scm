(define-module (crates-io dh cp dhcparse) #:use-module (crates-io))

(define-public crate-dhcparse-0.1.0 (c (n "dhcparse") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "147630ip3hmhijn7mjw2zlfbxscwra3kz1wibgpfc8im4dyad0d3") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-dhcparse-1.0.0-alpha (c (n "dhcparse") (v "1.0.0-alpha") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0pkks4rni9n6hc688cbyvbshjfrjipkfmkq4fynjc6v25k0h5336") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-dhcparse-1.0.0 (c (n "dhcparse") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (k 0)) (d (n "memchr") (r "^2.4") (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)))) (h "0qidv6qfml4kdi3xpx38369v3xzwlg3dwa50zxafypwlab17n6ww") (f (quote (("std" "byteorder/std" "memchr/std") ("default" "std"))))))

