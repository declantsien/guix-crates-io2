(define-module (crates-io dh cp dhcproto) #:use-module (crates-io))

(define-public crate-dhcproto-0.1.0 (c (n "dhcproto") (v "0.1.0") (h "16zlfqvl62m0ac8p7qb7x4ayfim3bjfyk2pfcbmdhxbvkf6jcfq8") (y #t)))

(define-public crate-dhcproto-0.2.0 (c (n "dhcproto") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h95s2v01ajqxbxa5v5805v341j3lm6f1b526zf9rsdbl1fv5q0s")))

(define-public crate-dhcproto-0.2.1 (c (n "dhcproto") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g1kija9xi1dmmmxjlh83pkxykvjkhd30sc67wgfzynynql2mqrl")))

(define-public crate-dhcproto-0.3.0 (c (n "dhcproto") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jjb4g5kbq6kznyyd9vdki986wcaz19sn2bv9gbhczxivbql9l5r")))

(define-public crate-dhcproto-0.4.0 (c (n "dhcproto") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j5b1c0biylxp4ch54q2ixq20ii80p5j9419ghdlr9bc3510ihc1")))

(define-public crate-dhcproto-0.4.1 (c (n "dhcproto") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "157wb3d6dzz0jzlqpfbqjm561fqhj4sr3zcr8b0b23wjnkrrzfhh")))

(define-public crate-dhcproto-0.5.0 (c (n "dhcproto") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w8699vf0b4blsz5ahr3kk2ba70c4dvwxxdi2cj29q9b7mgrv0lv")))

(define-public crate-dhcproto-0.6.0 (c (n "dhcproto") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0iyy94r8yp66vwz7fp2i328vybzxxrsi5b7sphrbc4l24qlda58x")))

(define-public crate-dhcproto-0.7.0 (c (n "dhcproto") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17hnhcsd2z34jmyadgx5v51zjdhg2p712xhb4n5xafx8anry3qym")))

(define-public crate-dhcproto-0.8.0 (c (n "dhcproto") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trust-dns-proto") (r "^0.21.2") (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0n3sn0lis9ziqrkkx3cvbmrb1vgabl8l0yzilmyqj7ckx6piysv5") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "url/serde"))))))

(define-public crate-dhcproto-0.9.0 (c (n "dhcproto") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "dhcproto-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ipnet") (r "^2.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trust-dns-proto") (r "^0.22.0") (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1hsbl77lxvxa94ihn2vna1sx2icrkch427w24a883xymhm9h9vnw") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "url/serde" "ipnet/serde" "trust-dns-proto/serde-config"))))))

(define-public crate-dhcproto-0.10.0 (c (n "dhcproto") (v "0.10.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "dhcproto-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ipnet") (r "^2.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trust-dns-proto") (r "^0.22.0") (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1iybzdsndxgfbrrmnfd1kdbkrxpf6b8nk9djvnd50i36wc7gfl0g") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "url/serde" "ipnet/serde" "trust-dns-proto/serde-config"))))))

(define-public crate-dhcproto-0.11.0 (c (n "dhcproto") (v "0.11.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "dhcproto-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ipnet") (r "^2.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trust-dns-proto") (r "^0.22.0") (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0sw6y7r4z71lpxma4g02lsdl9380rfxf06al36v0ff4iyss1f1q0") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "url/serde" "ipnet/serde" "trust-dns-proto/serde-config"))))))

