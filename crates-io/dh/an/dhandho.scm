(define-module (crates-io dh an dhandho) #:use-module (crates-io))

(define-public crate-dhandho-0.1.0 (c (n "dhandho") (v "0.1.0") (d (list (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "13r1lmx7y7ba6blr8h48irc5q2c9c7gz827znm5cgjifl1hhc9n2")))

(define-public crate-dhandho-0.2.0 (c (n "dhandho") (v "0.2.0") (d (list (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "0y7kk8chnfcw6z1rv3jrhdvndx20qxjjycy10fnl6pcifcpac0y2")))

