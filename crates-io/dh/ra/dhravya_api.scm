(define-module (crates-io dh ra dhravya_api) #:use-module (crates-io))

(define-public crate-dhravya_api-0.1.0 (c (n "dhravya_api") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16kl1jbd4yci81pvb1fq68ii692qbky71fgjc90sd2k3gdk5jbfw") (y #t)))

(define-public crate-dhravya_api-0.1.1 (c (n "dhravya_api") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y3c2wpy43c0bnjcjy0y9b1a6kvw4ink9890nnq4jnbimyljkyln") (y #t)))

(define-public crate-dhravya_api-0.1.2 (c (n "dhravya_api") (v "0.1.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gpik2dsmvxif9hxghd4vs7y9bzv7jx1sg3nysljh33b8fpj1ifq")))

