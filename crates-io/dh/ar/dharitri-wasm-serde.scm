(define-module (crates-io dh ar dharitri-wasm-serde) #:use-module (crates-io))

(define-public crate-dharitri-wasm-serde-0.0.2 (c (n "dharitri-wasm-serde") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0zcm3xz5n1flxv9lri01sgq15d2z7v7h508i4pmh9ycd5gjh41c9")))

(define-public crate-dharitri-wasm-serde-0.0.3 (c (n "dharitri-wasm-serde") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1glrmydzp31xbl1va6gcs51h8ya7hcl1s5bgdr4w7lrknrjv5s55")))

