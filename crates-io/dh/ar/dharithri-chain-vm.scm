(define-module (crates-io dh ar dharithri-chain-vm) #:use-module (crates-io))

(define-public crate-dharithri-chain-vm-0.5.2 (c (n "dharithri-chain-vm") (v "0.5.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "multiversx-chain-vm-executor") (r "^0.1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "1fflwf0q0a3vv30bkhsmh0sd6ikw62qghpjw6y2b0k6xbildha6q")))

