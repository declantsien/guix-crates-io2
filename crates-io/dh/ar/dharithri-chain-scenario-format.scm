(define-module (crates-io dh ar dharithri-chain-scenario-format) #:use-module (crates-io))

(define-public crate-dharithri-chain-scenario-format-0.20.0 (c (n "dharithri-chain-scenario-format") (v "0.20.0") (d (list (d (n "bech32") (r "^0.9.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "1fssxs34sc827wiaj456072ak3blxsvc2zfl101ihxccm9kixpyv")))

