(define-module (crates-io dh ar dharitri-wasm-output) #:use-module (crates-io))

(define-public crate-dharitri-wasm-output-0.0.50 (c (n "dharitri-wasm-output") (v "0.0.50") (d (list (d (n "dharitri-wasm-node") (r "^0.0.50") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0mipm3mf6wa7083g7xapp0dhmbvn87pgl851jkhpl088z80cxym4") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.0.2 (c (n "dharitri-wasm-output") (v "0.0.2") (d (list (d (n "dharitri-wasm-node") (r "^0.0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1543p60x6170pm5na4rlgczz8b5j1h4lgjpvx48kxc5bdn25492i") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.0.3 (c (n "dharitri-wasm-output") (v "0.0.3") (d (list (d (n "dharitri-wasm-node") (r "^0.0.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1zdrkiixcc1x4pvp2wm4wiiw98b7nkpccimrcl5hp3007gj168c3") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.0.4 (c (n "dharitri-wasm-output") (v "0.0.4") (d (list (d (n "dharitri-wasm-node") (r "^0.0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0m4dj0jf0d00adj808rhadg563xw6lr4bx48pgy0cjnars9vvk2p") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.0.5 (c (n "dharitri-wasm-output") (v "0.0.5") (d (list (d (n "dharitri-wasm-node") (r "^0.0.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0mfak9ap8cw78087q7g47vkb070c279x6xvd8xd8lnppd3bd9ghl") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.0.6 (c (n "dharitri-wasm-output") (v "0.0.6") (d (list (d (n "dharitri-wasm-node") (r "^0.0.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1v0ya3yjz3gxwkiifh2hk0x3axwmqlnl011ipnnw589gjg684apm") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.0.7 (c (n "dharitri-wasm-output") (v "0.0.7") (d (list (d (n "dharitri-wasm-node") (r "^0.0.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1ghj06fdi8hkayw3n9py98izdpk0wcgrhg8lcdxifd6s5j2wjqnr") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.0.8 (c (n "dharitri-wasm-output") (v "0.0.8") (d (list (d (n "dharitri-wasm-node") (r "^0.0.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1k50ngxk0mvz3z69kpggyvkrcds90ic3bc0waiqnf9nyaynh43c7") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.0.9 (c (n "dharitri-wasm-output") (v "0.0.9") (d (list (d (n "dharitri-wasm-node") (r "^0.0.9") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0v67q92yq3zkq2mld7kbx80rpkri42vmy6qvwm1qbr4pr6sjasj2") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.1.0 (c (n "dharitri-wasm-output") (v "0.1.0") (d (list (d (n "dharitri-wasm-node") (r "^0.1.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "15s19s1izh4qbnvkd9pb1sicrrwvjjhixwcmaq8llmain98hp2v1") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.1.1 (c (n "dharitri-wasm-output") (v "0.1.1") (d (list (d (n "dharitri-wasm-node") (r "^0.1.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0jmga33rc5hs1bcmjiariif951wvqdjd6r9616nk452ddcp9vw6x") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.1.2 (c (n "dharitri-wasm-output") (v "0.1.2") (d (list (d (n "dharitri-wasm-node") (r "^0.1.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0p8bfylzsxy212nf3i5py9l7ifaiw0jqrb6wy3iknlqzxawkmvrw") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.1.3 (c (n "dharitri-wasm-output") (v "0.1.3") (d (list (d (n "dharitri-wasm-node") (r "^0.1.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "190sn8avs3wwwkiqandy677dmfn39spwdw02xlb6bdajhaq8pbss") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.1.4 (c (n "dharitri-wasm-output") (v "0.1.4") (d (list (d (n "dharitri-wasm-node") (r "^0.1.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0cm7dcr95240gjiz5s6w6mdmndyqldazhmy5qjxjlxjb2c7ixrbl") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.1.5 (c (n "dharitri-wasm-output") (v "0.1.5") (d (list (d (n "dharitri-wasm-node") (r "^0.1.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1j4l2ky2n0qy6w0am4d1sa1zf17blz5drbiy3iqd3vf5hqzzqha4") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.1.6 (c (n "dharitri-wasm-output") (v "0.1.6") (d (list (d (n "dharitri-wasm-node") (r "^0.1.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0x2akskzkm8i64phmb2cqj4rzd0gkj93dhlcp3pa9jis5p8459a2") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.1.7 (c (n "dharitri-wasm-output") (v "0.1.7") (d (list (d (n "dharitri-wasm-node") (r "^0.1.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1pn4nnj617b5ni55yv9hprk6iwqcvxqpbjlisyi0ca0m8scpmrmx") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.1.8 (c (n "dharitri-wasm-output") (v "0.1.8") (d (list (d (n "dharitri-wasm-node") (r "^0.1.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1x1822s2vn8ly4pp29lcdwm4sqf2qbnz4xy3rsx6bhr89djzrf47") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.1.9 (c (n "dharitri-wasm-output") (v "0.1.9") (d (list (d (n "dharitri-wasm-node") (r "^0.1.9") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "144j0ql41nnjf667i6jc28913dy22gcdpnvx874nvvr86121j0b5") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.2.0 (c (n "dharitri-wasm-output") (v "0.2.0") (d (list (d (n "dharitri-wasm-node") (r "^0.2.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1yqqssxyycvy96q58dqx5ahh0qgj62nlm1wgbz40ak6v26288v2v") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.2.1 (c (n "dharitri-wasm-output") (v "0.2.1") (d (list (d (n "dharitri-wasm-node") (r "^0.2.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0l3b7c8grxavblz7viwg8csjfan8mx2qsf3zfwr34b94g7n5gbnp") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.2.2 (c (n "dharitri-wasm-output") (v "0.2.2") (d (list (d (n "dharitri-wasm-node") (r "=0.2.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0sasy5r5qm6vkvx528mn7crcs4j9qk3vj296k14q8a6nkr2f3438") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.2.3 (c (n "dharitri-wasm-output") (v "0.2.3") (d (list (d (n "dharitri-wasm-node") (r "=0.2.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0jbxsc9fa16q4kkl4jsasb38w289q3ff19z2q6s8ik5sx1kjsidz") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.2.4 (c (n "dharitri-wasm-output") (v "0.2.4") (d (list (d (n "dharitri-wasm-node") (r "=0.2.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0wpna3l4jwgs3s4c607fsjphb38rg3837jycsf1dgc46sy43b7cp") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.2.5 (c (n "dharitri-wasm-output") (v "0.2.5") (d (list (d (n "dharitri-wasm-node") (r "=0.2.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "15wgbgfcf0c9khwp0kwhkhnng44ambyv3m3mkq28lzz3j8lw3zkq") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.2.6 (c (n "dharitri-wasm-output") (v "0.2.6") (d (list (d (n "dharitri-wasm-node") (r "=0.2.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0g05cm8wjk24v17w9rlif40pccdarapjmng2x8q6lmp95fbz22j9") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.2.7 (c (n "dharitri-wasm-output") (v "0.2.7") (d (list (d (n "dharitri-wasm-node") (r "=0.2.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1fh3dkwzlszfjy4ann704zpvb8km7k05h3jckccvi5qpbgbxybn8") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.2.8 (c (n "dharitri-wasm-output") (v "0.2.8") (d (list (d (n "dharitri-wasm-node") (r "=0.2.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0b2x4h75dzs5hlm1nf09815f0bpkpyj43p34bcz9wf50q0jf99q3") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.2.9 (c (n "dharitri-wasm-output") (v "0.2.9") (d (list (d (n "dharitri-wasm-node") (r "=0.2.9") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "10gnyrnd7wiw14rnsdvj3v162wwnbkchwwj6w3hqb08qs1glm90m") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.3.0 (c (n "dharitri-wasm-output") (v "0.3.0") (d (list (d (n "dharitri-wasm-node") (r "=0.3.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "03y9xcnlwmcyqn5w5y3z10gccyg5drb4bmdrxmla2hgsczzsszp1") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.3.1 (c (n "dharitri-wasm-output") (v "0.3.1") (d (list (d (n "dharitri-wasm-node") (r "=0.3.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "14q4xv32gsfdy9gyqf6mmhjriq88bmm16ya34yhklfpl8rr9cvip") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.3.2 (c (n "dharitri-wasm-output") (v "0.3.2") (d (list (d (n "dharitri-wasm-node") (r "=0.3.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0gsriq3spay8v9fw333fiwpbqszwlsr0zm4pcivjwmr5v4sni6jj") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.3.3 (c (n "dharitri-wasm-output") (v "0.3.3") (d (list (d (n "dharitri-wasm-node") (r "=0.3.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0662vh14y3l31z7i0gcz8rv02c4crp5pw1cbgv6wfijp4w8ai98k") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.3.4 (c (n "dharitri-wasm-output") (v "0.3.4") (d (list (d (n "dharitri-wasm-node") (r "=0.3.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "11fs2vd2f1bjp73i063if2jyfjv0n41hzxjffias2177jv07fji2") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.3.5 (c (n "dharitri-wasm-output") (v "0.3.5") (d (list (d (n "dharitri-wasm-node") (r "=0.3.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "07r9lixd0km2icidf4fl78hd740rz7gqapz7fyh8mrp8bg8a50lp") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.3.6 (c (n "dharitri-wasm-output") (v "0.3.6") (d (list (d (n "dharitri-wasm-node") (r "=0.3.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0b1lsycwbgh3p3w6q2gwjrbds8y5dgvnfmvg8cwkbqmvpfyxv87d") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.3.7 (c (n "dharitri-wasm-output") (v "0.3.7") (d (list (d (n "dharitri-wasm-node") (r "=0.3.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1a76wz3qcwh4nxj63f99d140sfrynszly9rmdg3vgnzwc1acbw4a") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.3.8 (c (n "dharitri-wasm-output") (v "0.3.8") (d (list (d (n "dharitri-wasm-node") (r "=0.3.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "16r8z13j135hc37f6f5rmxzxlg0bvhkqhqda9h91svbmzv745j95") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.3.9 (c (n "dharitri-wasm-output") (v "0.3.9") (d (list (d (n "dharitri-wasm-node") (r "=0.3.9") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0h2f0lh3nj0jr7mskkvnrr2v4a3c8vfiv5hb1qd7j4h7kq1vc3fc") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.4.0 (c (n "dharitri-wasm-output") (v "0.4.0") (d (list (d (n "dharitri-wasm-node") (r "=0.4.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "04ba2laj75rjf5bf0ldx85zyw6ydaww3rg4i5499lnxqalmdf52h") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.4.1 (c (n "dharitri-wasm-output") (v "0.4.1") (d (list (d (n "dharitri-wasm-node") (r "=0.4.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1vlms53q9wjm1s7nqs9zynb7l1wdan0020ndv5aadjpbsmqp8y3g") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.4.2 (c (n "dharitri-wasm-output") (v "0.4.2") (d (list (d (n "dharitri-wasm-node") (r "=0.4.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1byq9nhrc05rp3ydp5q8cmarxgkzgbyp0vfmdpi1zdlkibbs6inc") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.4.3 (c (n "dharitri-wasm-output") (v "0.4.3") (d (list (d (n "dharitri-wasm-node") (r "=0.4.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1fg2h5z7k2s5id0azlzgcc7nsf2v8869sxc1rg8gl5f9qkkrhqrf") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.4.4 (c (n "dharitri-wasm-output") (v "0.4.4") (d (list (d (n "dharitri-wasm-node") (r "=0.4.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0ryw6023rmj97cxiilw2wnkf34v89dczpbnydcxc4j44sag546g4") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.4.5 (c (n "dharitri-wasm-output") (v "0.4.5") (d (list (d (n "dharitri-wasm-node") (r "=0.4.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "06y2h4phvkgmp919c8dadnz3yaqp6h98kl4h0j23c96sq8hnskn9") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.4.6 (c (n "dharitri-wasm-output") (v "0.4.6") (d (list (d (n "dharitri-wasm-node") (r "=0.4.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0s4329ndswwyi38bmmgcprdib6kr42ayzsr0fv3s5ka3j9p4hxmq") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.4.7 (c (n "dharitri-wasm-output") (v "0.4.7") (d (list (d (n "dharitri-wasm-node") (r "=0.4.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0s6vq16ph06x4jhkjf6ikcizmjqbvi2332dj6ddkgw0sxvvxjphl") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.4.8 (c (n "dharitri-wasm-output") (v "0.4.8") (d (list (d (n "dharitri-wasm-node") (r "=0.4.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1a7dwshnydsngx1chzgx0mafqjdzx06nhd761dswx4nidk82qg00") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.4.9 (c (n "dharitri-wasm-output") (v "0.4.9") (d (list (d (n "dharitri-wasm-node") (r "=0.4.9") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0vqa81dwa6jyqqjwfs7wfb7kii8ik1g639qkrx0mmj7cyiqxxgyx") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.5.0 (c (n "dharitri-wasm-output") (v "0.5.0") (d (list (d (n "dharitri-wasm-node") (r "=0.5.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "05i5bblqqm26b0bmg4akq8dp55fw3bil3nczfbn1kdh7q8lp0ikr") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.5.1 (c (n "dharitri-wasm-output") (v "0.5.1") (d (list (d (n "dharitri-wasm-node") (r "=0.5.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1p88wnqz9ydlm7absk61lqid4bjhmvcwh9x3nkqz0y2vsaqnb311") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.5.2 (c (n "dharitri-wasm-output") (v "0.5.2") (d (list (d (n "dharitri-wasm-node") (r "=0.5.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1ss6zcfhsfdfx0vm2zfgq0mcikagp1qa37h2hvl133xzh73bsjxp") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.5.3 (c (n "dharitri-wasm-output") (v "0.5.3") (d (list (d (n "dharitri-wasm-node") (r "=0.5.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "131pza0lv7rhpcgsjbx1w35ycz12ps2alkiqmbnpk4qm8c48fkpc") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.5.4 (c (n "dharitri-wasm-output") (v "0.5.4") (d (list (d (n "dharitri-wasm-node") (r "=0.5.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "00kyfb4nqqlsz99isa78jl1gppnkbinz5zhny7jpkkzgcr2kwc0r") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.5.5 (c (n "dharitri-wasm-output") (v "0.5.5") (d (list (d (n "dharitri-wasm-node") (r "^0.5.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0mxnyk31kgk4j6vy569z9w7xycqnp5qv1znqclakbz8f6i9871fz") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.5.6 (c (n "dharitri-wasm-output") (v "0.5.6") (d (list (d (n "dharitri-wasm-node") (r "^0.5.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "07bblxrw4p9gwysr64g2mwx5ckynpb4p91jrj8icfvib29vawk7d") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.5.7 (c (n "dharitri-wasm-output") (v "0.5.7") (d (list (d (n "dharitri-wasm-node") (r "^0.5.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1xix75r0s2j3bqh1l748774ki0h7k6ljr9dxzqzmhg2vgb1s54n7") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.5.8 (c (n "dharitri-wasm-output") (v "0.5.8") (d (list (d (n "dharitri-wasm-node") (r "^0.5.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1hfxd1wwz4m3b0ch7kqbp1qrnrgf2srpyl52mp6d0mmagbaqb6d2") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.5.9 (c (n "dharitri-wasm-output") (v "0.5.9") (d (list (d (n "dharitri-wasm-node") (r "^0.5.9") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0m7hy196w8nn97mnx8z0nh83ip2bglj126pjamg938l3g7814idj") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.6.0 (c (n "dharitri-wasm-output") (v "0.6.0") (d (list (d (n "dharitri-wasm-node") (r "^0.6.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0r7qcwixhyv5ylzjmihx7y865k348496k4gn3cl1290r2p91rwwn") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.6.1 (c (n "dharitri-wasm-output") (v "0.6.1") (d (list (d (n "dharitri-wasm-node") (r "^0.6.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0cg4r5m3n4kz3k5ci44h269aiz2prb1wz4y9z5a2w3dkc258g78s") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.6.2 (c (n "dharitri-wasm-output") (v "0.6.2") (d (list (d (n "dharitri-wasm-node") (r "^0.6.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1r2s00ndqha8a7n1q52px2hsbhinskjk7v8049flw333i1b3zn33") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.6.3 (c (n "dharitri-wasm-output") (v "0.6.3") (d (list (d (n "dharitri-wasm-node") (r "^0.6.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0dpn34248ql22f2pgg1gw92wxiqdf8lpj7kpiwy56wj9f1chxrd8") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.6.4 (c (n "dharitri-wasm-output") (v "0.6.4") (d (list (d (n "dharitri-wasm-node") (r "^0.6.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0j3m7fcxb09snnl49y9myhx22s9rvrxs7x7qmdvj588yp7m20qj5") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.6.5 (c (n "dharitri-wasm-output") (v "0.6.5") (d (list (d (n "dharitri-wasm-node") (r "^0.6.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1b1yw0x5vj4284xdflv6i2cx4xlcfmgz51cmjkircchw3pxp4j8x") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.6.6 (c (n "dharitri-wasm-output") (v "0.6.6") (d (list (d (n "dharitri-wasm-node") (r "^0.6.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1yfdxv3hsd8vwdkafj4kqzjqraipvlbw91hz23qbf0jgz31gdsia") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.6.7 (c (n "dharitri-wasm-output") (v "0.6.7") (d (list (d (n "dharitri-wasm-node") (r "^0.6.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0mazsnh11zz5xlfi3fd2bns1526rqnjpjp93pblmnj3rrwb0vsnb") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.6.8 (c (n "dharitri-wasm-output") (v "0.6.8") (d (list (d (n "dharitri-wasm-node") (r "^0.6.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0d59kppl2lp2pw1wb98wyzcy2rsrfkphic3l6g25c9n4circ8h3q") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.6.9 (c (n "dharitri-wasm-output") (v "0.6.9") (d (list (d (n "dharitri-wasm-node") (r "^0.6.9") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0p4faw0a160qnrp0w1bqb8aycykcn5mnajhb1gayz8i3d0cc6axd") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.7.0 (c (n "dharitri-wasm-output") (v "0.7.0") (d (list (d (n "dharitri-wasm-node") (r "^0.7.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0dxg2x41m8z0m4b12y7n2q2awqkdz41fhxsnfacmhxm1xn42sl5v") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.7.1 (c (n "dharitri-wasm-output") (v "0.7.1") (d (list (d (n "dharitri-wasm-node") (r "^0.7.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1rm3jgkgg6bnd2sp6ajbfxkhldsz46arfq10fvvj124aif0r7pzv") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.7.2 (c (n "dharitri-wasm-output") (v "0.7.2") (d (list (d (n "dharitri-wasm-node") (r "=0.7.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "149fkqrjxd1kcv26pmvra9bcz46i37y53m49mjpbyzprnh7vpwr4") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.7.3 (c (n "dharitri-wasm-output") (v "0.7.3") (d (list (d (n "dharitri-wasm-node") (r "=0.7.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1y7m4plngs1z16h90r07v038makdvw3b2zharm7w43qmyshgix5r") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.7.4 (c (n "dharitri-wasm-output") (v "0.7.4") (d (list (d (n "dharitri-wasm-node") (r "=0.7.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1hbxf5nyq6f8hcw0fnn452yj19xpi0gzclwff7lv0zqnfbzkr9yw") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.7.5 (c (n "dharitri-wasm-output") (v "0.7.5") (d (list (d (n "dharitri-wasm-node") (r "=0.7.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1jcxb0fpdai26p3a8cy0q6s0s3hk9pvm2ycwvrc31xk932hv5bf4") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.7.6 (c (n "dharitri-wasm-output") (v "0.7.6") (d (list (d (n "dharitri-wasm-node") (r "=0.7.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1hrpmfd7b0g31bw9zylw1lh2r8041y1a494yzf5cj99n5nq5gnl1") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.7.7 (c (n "dharitri-wasm-output") (v "0.7.7") (d (list (d (n "dharitri-wasm-node") (r "=0.7.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "04m873qizqrlwjg67aarr6yqp999q9gglbnklz8j20n39z9zzpc6") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.7.8 (c (n "dharitri-wasm-output") (v "0.7.8") (d (list (d (n "dharitri-wasm-node") (r "=0.7.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0271fkv69vv9kb4aabw7kvqs6rgbqkjqnldlbfkifw9c6pp84c8n") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.7.9 (c (n "dharitri-wasm-output") (v "0.7.9") (d (list (d (n "dharitri-wasm-node") (r "=0.7.9") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1njs9cjan30an639x2pjmncgyrd3fsdysvmwi1x07y6811vh40i4") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.8.0 (c (n "dharitri-wasm-output") (v "0.8.0") (d (list (d (n "dharitri-wasm-node") (r "=0.8.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1qzxfwp6wvz3irgj85z5x5h3lj3mizqw4r04ib8f272g25grsalm") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.8.1 (c (n "dharitri-wasm-output") (v "0.8.1") (d (list (d (n "dharitri-wasm-node") (r "=0.8.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1c2by724dhkxpmwb2r4kz1nnpgfaw6fcw3pdi8bqccy5dlavaz4d") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.8.2 (c (n "dharitri-wasm-output") (v "0.8.2") (d (list (d (n "dharitri-wasm-node") (r "=0.8.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0xivjmkzn24dvd45df62irmxq1d79q18zgx7ysycs1cpmylgyarl") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.8.3 (c (n "dharitri-wasm-output") (v "0.8.3") (d (list (d (n "dharitri-wasm-node") (r "=0.8.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0jsx7iwdljv9drlyxdg8bnydkc3yrjsih2h8aw9hxskfrqw0rqxz") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.8.4 (c (n "dharitri-wasm-output") (v "0.8.4") (d (list (d (n "dharitri-wasm-node") (r "=0.8.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1xs1b5kwrcaj5mz8aivay27mfqkhjmz75fgls59z0w3nmzkhplzs") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.8.5 (c (n "dharitri-wasm-output") (v "0.8.5") (d (list (d (n "dharitri-wasm-node") (r "=0.8.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "059pisfn05ix62yjpgkrc74vqmds9vsbpd9d2s8yxs5jg06z25bw") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.8.6 (c (n "dharitri-wasm-output") (v "0.8.6") (d (list (d (n "dharitri-wasm-node") (r "=0.8.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0hii2ni1531kgy4l5lms1vwmdly12l96l67109bpmh4xpp1b7w5l") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.8.7 (c (n "dharitri-wasm-output") (v "0.8.7") (d (list (d (n "dharitri-wasm-node") (r "=0.8.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1j86266q18a2cqrknfasnsifhdb5vm5756lkkz0s5j7agg4g1phg") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.8.8 (c (n "dharitri-wasm-output") (v "0.8.8") (d (list (d (n "dharitri-wasm-node") (r "=0.8.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1dhmls0w849y45k3m5y3bdsy76l9v0wj0gksppi03m68vhgbqgwy") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.8.9 (c (n "dharitri-wasm-output") (v "0.8.9") (d (list (d (n "dharitri-wasm-node") (r "=0.8.9") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0lirp06vswb01nc6yy44szaaq9if44qnmqdzxc7sz0ny3gzm2pab") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.9.0 (c (n "dharitri-wasm-output") (v "0.9.0") (d (list (d (n "dharitri-wasm-node") (r "=0.9.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0hqyypd43bqn3i5m77jrqdnhwqbk6cmyxccvilv1did2qqkywg67") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.9.1 (c (n "dharitri-wasm-output") (v "0.9.1") (d (list (d (n "dharitri-wasm-node") (r "=0.9.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0zjwqfvbscwq3akybvjnacq2q0aniyxcxrqf8719a553p7cipz61") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.9.2 (c (n "dharitri-wasm-output") (v "0.9.2") (d (list (d (n "dharitri-wasm-node") (r "=0.9.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "04wrjidw9alyy6a52il3gla1zdjpz1dgw8hqwmr3fqa4siir02mg") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.9.3 (c (n "dharitri-wasm-output") (v "0.9.3") (d (list (d (n "dharitri-wasm-node") (r "=0.9.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "11hjfz9d2rzlg8kk8gqk74hkg9ac5nids52ha9valvzm2x1h5q89") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.9.4 (c (n "dharitri-wasm-output") (v "0.9.4") (d (list (d (n "dharitri-wasm-node") (r "=0.9.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "07y9qrkfvm9iwi9zfgikf7a1b3n96fm9kc1mih0c9h66kfamf74q") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.9.5 (c (n "dharitri-wasm-output") (v "0.9.5") (d (list (d (n "dharitri-wasm-node") (r "=0.9.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1kjsmxzddrkqq2jqvfa5v8yxg2fkqf9s20d0xghk534kfhypbna6") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.9.6 (c (n "dharitri-wasm-output") (v "0.9.6") (d (list (d (n "dharitri-wasm-node") (r "=0.9.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "12dlfwi0axr2ivfvmylhkda9x2fbvlwfr8sm76va3jcj1sy4zf4f") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.9.7 (c (n "dharitri-wasm-output") (v "0.9.7") (d (list (d (n "dharitri-wasm-node") (r "=0.9.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1vbxdmplsd3zrw19937krw3mv1qqpz8w82ki4m8k0gisy1n2sjgj") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.9.8 (c (n "dharitri-wasm-output") (v "0.9.8") (d (list (d (n "dharitri-wasm-node") (r "=0.9.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "18lpazx9sdd2sqjywqn2bv3p6p19jd9835k2wss92jkcpsxsj12p") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.9.9 (c (n "dharitri-wasm-output") (v "0.9.9") (d (list (d (n "dharitri-wasm-node") (r "=0.9.9") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0gb8cn1mq5m8rziqm3xf1inb3f7bw87fw6sfnai97jzg90fxwx2q") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.10.0 (c (n "dharitri-wasm-output") (v "0.10.0") (d (list (d (n "dharitri-wasm-node") (r "=0.10.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "177785x84z0hlzhqvdsl01s9zh28riji4yzr2zs0lnf4cs5759ps") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.10.1 (c (n "dharitri-wasm-output") (v "0.10.1") (d (list (d (n "dharitri-wasm-node") (r "=0.10.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1fvn24aip70gxgh0iiawkh6dr2a8143fgwnfiylj6krz2s3kzdci") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.10.2 (c (n "dharitri-wasm-output") (v "0.10.2") (d (list (d (n "dharitri-wasm-node") (r "=0.10.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1lbdgg2diih7q64ahiq91fa1k0j948psc5bqyb67m18yv2ms79fx") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.10.3 (c (n "dharitri-wasm-output") (v "0.10.3") (d (list (d (n "dharitri-wasm-node") (r "=0.10.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1n85bmda3vmlmqrcv54v0shwzxywx4hy1argvnr4z689mvivbkkz") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.10.4 (c (n "dharitri-wasm-output") (v "0.10.4") (d (list (d (n "dharitri-wasm-node") (r "=0.10.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0kbnhbczim94dd67fgrxpjxwka2ikikf7rdmq6vaiyw8hnqybfya") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.10.5 (c (n "dharitri-wasm-output") (v "0.10.5") (d (list (d (n "dharitri-wasm-node") (r "=0.10.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0hdl42cxnmh03jc340i691hs7n7np70d97qhhxrnd6ghl1abg4fs") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.10.6 (c (n "dharitri-wasm-output") (v "0.10.6") (d (list (d (n "dharitri-wasm-node") (r "=0.10.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0llzssqn4rmp8h3n62lnc4d2120gdia70zkqmjb6f5m3w9xab2al") (f (quote (("wasm-output-mode") ("panic-message"))))))

(define-public crate-dharitri-wasm-output-0.10.7 (c (n "dharitri-wasm-output") (v "0.10.7") (d (list (d (n "dharitri-wasm-node") (r "=0.10.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1dlk1x4r2qj4r58pbn3rf8pyjvvzlvv537k5xgralcj9xk5hyswh") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

(define-public crate-dharitri-wasm-output-0.10.8 (c (n "dharitri-wasm-output") (v "0.10.8") (d (list (d (n "dharitri-wasm-node") (r "=0.10.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1fnl6fki3zis952nch5xjg4w80v20425dzx46wd1525pjdnzydrw") (f (quote (("wasm-output-mode") ("panic-message")))) (y #t)))

