(define-module (crates-io dh ar dhartitri-chain-vm-executor) #:use-module (crates-io))

(define-public crate-dhartitri-chain-vm-executor-0.0.1 (c (n "dhartitri-chain-vm-executor") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "enumset") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (o #t) (d #t) (k 0)))) (h "01gagbv9xr5bq0bkfmn37z1rv8msl4j74lylhvhsqhwcnswvci9b")))

