(define-module (crates-io dh ar dharitri-wasm-node) #:use-module (crates-io))

(define-public crate-dharitri-wasm-node-0.0.50 (c (n "dharitri-wasm-node") (v "0.0.50") (d (list (d (n "dharitri-wasm") (r "^0.0.50") (d #t) (k 0)))) (h "0r9gr9zzk569vzy07ki99jz087g1cpzf39pj24lpc870gyzjrk2l")))

(define-public crate-dharitri-wasm-node-0.0.2 (c (n "dharitri-wasm-node") (v "0.0.2") (d (list (d (n "dharitri-wasm") (r "^0.0.2") (d #t) (k 0)))) (h "13hd7jcvjbf2nd0k214pc2sfh1bmf3ynsc26hzzm2b621iq678sl")))

(define-public crate-dharitri-wasm-node-0.0.3 (c (n "dharitri-wasm-node") (v "0.0.3") (d (list (d (n "dharitri-wasm") (r "^0.0.3") (d #t) (k 0)))) (h "0k5bc6hpv65ncv357iyw74g1a62j4ifcyvs1axyqvbyc7lkz1cwg")))

(define-public crate-dharitri-wasm-node-0.0.4 (c (n "dharitri-wasm-node") (v "0.0.4") (d (list (d (n "dharitri-wasm") (r "^0.0.4") (d #t) (k 0)))) (h "16f0cvkxg3wmxda1mrlr3556b8zg0n6nmbcg0kbj5fqlkcw5xn40")))

(define-public crate-dharitri-wasm-node-0.0.5 (c (n "dharitri-wasm-node") (v "0.0.5") (d (list (d (n "dharitri-wasm") (r "^0.0.5") (d #t) (k 0)))) (h "1jkb4d0r1mfi8d22xl3g9k7ljspcrvvwrjraakl7mgd3w2qjmkww")))

(define-public crate-dharitri-wasm-node-0.0.6 (c (n "dharitri-wasm-node") (v "0.0.6") (d (list (d (n "dharitri-wasm") (r "^0.0.6") (d #t) (k 0)))) (h "0iv9rvsr297x6pi7nvji5rv133y9zmx00sj69xysc7ynw0cckmcf")))

(define-public crate-dharitri-wasm-node-0.0.7 (c (n "dharitri-wasm-node") (v "0.0.7") (d (list (d (n "dharitri-wasm") (r "^0.0.7") (d #t) (k 0)))) (h "1ihk9gx9db13k20xmpyny0yvjba87ibq9mbm7yva3qqnfdsii309")))

(define-public crate-dharitri-wasm-node-0.0.8 (c (n "dharitri-wasm-node") (v "0.0.8") (d (list (d (n "dharitri-wasm") (r "^0.0.8") (d #t) (k 0)))) (h "0xphi13c82pqp7yd5j7wskysp4b46157hcsz7pnx1al36pfk0zh4")))

(define-public crate-dharitri-wasm-node-0.0.9 (c (n "dharitri-wasm-node") (v "0.0.9") (d (list (d (n "dharitri-wasm") (r "^0.0.9") (d #t) (k 0)))) (h "16hryj6yvyghiz6f8c8zvaylifjyh61wd34a7vw0ysz5vj5asay4")))

(define-public crate-dharitri-wasm-node-0.1.0 (c (n "dharitri-wasm-node") (v "0.1.0") (d (list (d (n "dharitri-wasm") (r "^0.1.0") (d #t) (k 0)))) (h "0f448qlciyypnllhik3imrqn0vg2kcx33rx752qpvxq7c1z9vkkx")))

(define-public crate-dharitri-wasm-node-0.1.1 (c (n "dharitri-wasm-node") (v "0.1.1") (d (list (d (n "dharitri-wasm") (r "^0.1.1") (d #t) (k 0)))) (h "0r5mskqs6kkxb38ppqpwk31gyhadf7kd9mnjj9alj4l1bpssawir")))

(define-public crate-dharitri-wasm-node-0.1.2 (c (n "dharitri-wasm-node") (v "0.1.2") (d (list (d (n "dharitri-wasm") (r "^0.1.2") (d #t) (k 0)))) (h "02vyli5g64vaim0vjpgl196zfw4731m1vmlgm8v91f07s1caapmp")))

(define-public crate-dharitri-wasm-node-0.1.3 (c (n "dharitri-wasm-node") (v "0.1.3") (d (list (d (n "dharitri-wasm") (r "^0.1.3") (d #t) (k 0)))) (h "0mmcnn5yxgh9anpwmins62gy0ywxfdypqh0r8nl3ik7ak591xiq6")))

(define-public crate-dharitri-wasm-node-0.1.4 (c (n "dharitri-wasm-node") (v "0.1.4") (d (list (d (n "dharitri-wasm") (r "^0.1.4") (d #t) (k 0)))) (h "17hrjmjk7lcg0rd23q9vlvrskbbc15brpbz6wfca9lc4w3avq602")))

(define-public crate-dharitri-wasm-node-0.1.5 (c (n "dharitri-wasm-node") (v "0.1.5") (d (list (d (n "dharitri-wasm") (r "^0.1.5") (d #t) (k 0)))) (h "0hqajk7zphqy6a61kfi798yw96icbkw9gm4r9k022ysfk2r32vah")))

(define-public crate-dharitri-wasm-node-0.1.6 (c (n "dharitri-wasm-node") (v "0.1.6") (d (list (d (n "dharitri-wasm") (r "^0.1.6") (d #t) (k 0)))) (h "0k3389dwxsysr19nhyjhp4v1pdl0yjivlbsaaqfb7q5ir2g7bv7m")))

(define-public crate-dharitri-wasm-node-0.1.7 (c (n "dharitri-wasm-node") (v "0.1.7") (d (list (d (n "dharitri-wasm") (r "^0.1.7") (d #t) (k 0)))) (h "179hrpqgnjfq0pwvwhzd02sfls7ffi8za7a6712k2qbpvg995kiw") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.1.8 (c (n "dharitri-wasm-node") (v "0.1.8") (d (list (d (n "dharitri-wasm") (r "^0.1.8") (d #t) (k 0)))) (h "11rr5q8b186ga6bf6cp1kfhwsrmq67if76f2l6fip4klx9g95gv4") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.1.9 (c (n "dharitri-wasm-node") (v "0.1.9") (d (list (d (n "dharitri-wasm") (r "^0.1.9") (d #t) (k 0)))) (h "0syfj9qkb1ndvlxg0141369aq01l3mj0whfrpjbcrwwfql9r9045") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.2.0 (c (n "dharitri-wasm-node") (v "0.2.0") (d (list (d (n "dharitri-wasm") (r "^0.2.0") (d #t) (k 0)))) (h "077a3mvfjy7xnd56gvniz9np63cwhnsfadqddac4ksg50lv8s9nz") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.2.1 (c (n "dharitri-wasm-node") (v "0.2.1") (d (list (d (n "dharitri-wasm") (r "^0.2.1") (d #t) (k 0)))) (h "1jl4ls86g0yfx8nq72rhpic661sf4h4wg0kadypn1q61dmria56q") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.2.2 (c (n "dharitri-wasm-node") (v "0.2.2") (d (list (d (n "dharitri-wasm") (r "=0.2.2") (d #t) (k 0)))) (h "0w3i341yy5d99j59g6r5qmb839yqf7kpnqrvhrxpk5xi8n4464r3") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.2.3 (c (n "dharitri-wasm-node") (v "0.2.3") (d (list (d (n "dharitri-wasm") (r "=0.2.3") (d #t) (k 0)))) (h "19hdqyvwiqv2y1yx7pky00i9d6kyqyizyijqrlpdlnz9dpiinmgc") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.2.4 (c (n "dharitri-wasm-node") (v "0.2.4") (d (list (d (n "dharitri-wasm") (r "=0.2.4") (d #t) (k 0)))) (h "1njmxcmi05hbkrl917g0xrm5xf6n3s15xphnc2wkmcvn3kkp287b") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.2.5 (c (n "dharitri-wasm-node") (v "0.2.5") (d (list (d (n "dharitri-wasm") (r "=0.2.5") (d #t) (k 0)))) (h "0hn9jyjryc1hdm03b9c9x5wixl6sr2g4q3d7d5wvcykyq3yf1va9") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.2.6 (c (n "dharitri-wasm-node") (v "0.2.6") (d (list (d (n "dharitri-wasm") (r "=0.2.6") (d #t) (k 0)))) (h "0vqz0ah0j37w4xl7mwx3dah54myd4kvklwydzj8b3pms261yqygk") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.2.7 (c (n "dharitri-wasm-node") (v "0.2.7") (d (list (d (n "dharitri-wasm") (r "=0.2.7") (d #t) (k 0)))) (h "1lzx5xgmg6aclbyspp15xla2hz2nrviiv4wijakf5m95wb0plw1k") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.2.8 (c (n "dharitri-wasm-node") (v "0.2.8") (d (list (d (n "dharitri-wasm") (r "=0.2.8") (d #t) (k 0)))) (h "0h3wx6yanbm7117g723mck15zdia01m2ksar1g4xr3idfc7rlbbl") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.2.9 (c (n "dharitri-wasm-node") (v "0.2.9") (d (list (d (n "dharitri-wasm") (r "=0.2.9") (d #t) (k 0)))) (h "1pky8kja4gw7fy9cly1gaxfk48n7xdhjspfsa9ajk3804hbi0l1f") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.3.0 (c (n "dharitri-wasm-node") (v "0.3.0") (d (list (d (n "dharitri-wasm") (r "=0.3.0") (d #t) (k 0)))) (h "1f2hj1mwf49s0b9qz8hh1f508nrw6idgb4w4jdhx55yh751gyg9i") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.3.1 (c (n "dharitri-wasm-node") (v "0.3.1") (d (list (d (n "dharitri-wasm") (r "=0.3.1") (d #t) (k 0)))) (h "1vkpspm91ac48459h2p76xprp4w7nh6y8cw1i575ihh7966rnkc0") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.3.2 (c (n "dharitri-wasm-node") (v "0.3.2") (d (list (d (n "dharitri-wasm") (r "=0.3.2") (d #t) (k 0)))) (h "0fpv88jds42l673scd664rsmpxm3l1xi382j0kmkb0507y9w0cvc") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.3.3 (c (n "dharitri-wasm-node") (v "0.3.3") (d (list (d (n "dharitri-wasm") (r "=0.3.3") (d #t) (k 0)))) (h "1wxv0r5ah7jr42lh6g37pn1ag5v44mpvmhi24j2iy442w6y9fpmg") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.3.4 (c (n "dharitri-wasm-node") (v "0.3.4") (d (list (d (n "dharitri-wasm") (r "=0.3.4") (d #t) (k 0)))) (h "0sf9qgmq1szf8nv0afid0xzrml13dl0m24n83d8cswgq31f6bk70") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.3.5 (c (n "dharitri-wasm-node") (v "0.3.5") (d (list (d (n "dharitri-wasm") (r "=0.3.5") (d #t) (k 0)))) (h "0xk3igq6fffc3vl31dam83cslg5r1044sjwr401dgl24ps0jsqil") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.3.6 (c (n "dharitri-wasm-node") (v "0.3.6") (d (list (d (n "dharitri-wasm") (r "=0.3.6") (d #t) (k 0)))) (h "1r54z7kfmgp8ic7hk1m8n9fdhmxigdvxmg8w9biqzvld7zn3dmhv") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.3.7 (c (n "dharitri-wasm-node") (v "0.3.7") (d (list (d (n "dharitri-wasm") (r "=0.3.7") (d #t) (k 0)))) (h "076w3a7a77785z5fvvchx4kb2r7lq3a0436far14j1lr3dyqcji4") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.3.8 (c (n "dharitri-wasm-node") (v "0.3.8") (d (list (d (n "dharitri-wasm") (r "=0.3.8") (d #t) (k 0)))) (h "16srpxkzr4p619dlk4m6im727wrsq6h61jsm0w825bjdv95bszyd") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.3.9 (c (n "dharitri-wasm-node") (v "0.3.9") (d (list (d (n "dharitri-wasm") (r "=0.3.9") (d #t) (k 0)))) (h "1cqqj6zb36apj88ishfjwr9fb9kp9nsxm3j3zvr7yk2rarb0g3rx") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.4.0 (c (n "dharitri-wasm-node") (v "0.4.0") (d (list (d (n "dharitri-wasm") (r "=0.4.0") (d #t) (k 0)))) (h "1zsqydjx6qh1amy8nsibwvq1rkhm6ls9fg466khc2vl19pdyap5c") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.4.1 (c (n "dharitri-wasm-node") (v "0.4.1") (d (list (d (n "dharitri-wasm") (r "=0.4.1") (d #t) (k 0)))) (h "1xmm1fa1wvml3ciwf78ln78nl4d1vmwnq9l4h5nn8varf8iw7a5a") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.4.2 (c (n "dharitri-wasm-node") (v "0.4.2") (d (list (d (n "dharitri-wasm") (r "=0.4.2") (d #t) (k 0)))) (h "0m4a9m7z89h32rf8k9jzid5iahb8g342wnd5dn5dsjwpdqw4wpj9") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.4.3 (c (n "dharitri-wasm-node") (v "0.4.3") (d (list (d (n "dharitri-wasm") (r "=0.4.3") (d #t) (k 0)))) (h "132qb0rb9sgk0bcrqlcm17vv4kw361kj1lcaqrnkv64qj96vf460") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.4.4 (c (n "dharitri-wasm-node") (v "0.4.4") (d (list (d (n "dharitri-wasm") (r "=0.4.4") (d #t) (k 0)))) (h "1xaal3qv8dq6pq0lnx08cgpcx3scip8aif4a6hk15as35anp11bn") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.4.5 (c (n "dharitri-wasm-node") (v "0.4.5") (d (list (d (n "dharitri-wasm") (r "=0.4.5") (d #t) (k 0)))) (h "1i7r6wv8zjr8504c9jf5y1naabwpgxr5p1jaq9b337pcx1vs71kh") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.4.6 (c (n "dharitri-wasm-node") (v "0.4.6") (d (list (d (n "dharitri-wasm") (r "=0.4.6") (d #t) (k 0)))) (h "124mxipphq50m3hq73vp04xxnbipv2p8cy09yq8i63f9bkmnkdw6") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged") ("ei-1-1" "dharitri-wasm/ei-1-1"))))))

(define-public crate-dharitri-wasm-node-0.4.7 (c (n "dharitri-wasm-node") (v "0.4.7") (d (list (d (n "dharitri-wasm") (r "=0.4.7") (d #t) (k 0)))) (h "083lfg9lw4pyf8a42gcvkqys7qzwr73d1g6923qz95l9v4wiisx9") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged"))))))

(define-public crate-dharitri-wasm-node-0.4.8 (c (n "dharitri-wasm-node") (v "0.4.8") (d (list (d (n "dharitri-wasm") (r "=0.4.8") (d #t) (k 0)))) (h "0lxxnkbrxycmm858jyrzkbdnb8bn7w6j1fh688a7zg1s44f5xd26") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged"))))))

(define-public crate-dharitri-wasm-node-0.4.9 (c (n "dharitri-wasm-node") (v "0.4.9") (d (list (d (n "dharitri-wasm") (r "=0.4.9") (d #t) (k 0)))) (h "0r974gcn518f8vi16lk0zjnx5p38hj9prrk0lzibw6hy983lx8d8") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-wasm-node-0.5.0 (c (n "dharitri-wasm-node") (v "0.5.0") (d (list (d (n "dharitri-wasm") (r "=0.5.0") (d #t) (k 0)))) (h "1cn6hijnrq88f2k477ayzvks0hml5dp72284566m36w5lbsdvpql") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-wasm-node-0.5.1 (c (n "dharitri-wasm-node") (v "0.5.1") (d (list (d (n "dharitri-wasm") (r "=0.5.1") (d #t) (k 0)))) (h "1f3ys03r7vzgckxsxnxw61fy7226402fy2ibiyywy1q1gsnxym2r") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-wasm-node-0.5.2 (c (n "dharitri-wasm-node") (v "0.5.2") (d (list (d (n "dharitri-wasm") (r "=0.5.2") (d #t) (k 0)))) (h "1ra4nznwwqmv62sl3l6dydl475bgb7imxvv58fhn9n7a53gpx245") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-wasm-node-0.5.3 (c (n "dharitri-wasm-node") (v "0.5.3") (d (list (d (n "dharitri-wasm") (r "=0.5.3") (d #t) (k 0)))) (h "0mc43dmalm0pldzxai868biww2p7651z5d07da7jxk9g30cabm6q") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-wasm-node-0.5.4 (c (n "dharitri-wasm-node") (v "0.5.4") (d (list (d (n "dharitri-wasm") (r "=0.5.4") (d #t) (k 0)))) (h "0qgmjg00cla54ibg9wfd2b00bg68rnf5qkxdjd81vli1qiqm7c2a") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged"))))))

(define-public crate-dharitri-wasm-node-0.5.5 (c (n "dharitri-wasm-node") (v "0.5.5") (d (list (d (n "dharitri-wasm") (r "^0.5.5") (d #t) (k 0)))) (h "12dgz9c8gg1fj2y2c4rbhsgx7rk7fv50163nfj3vq1dmisxmvxll")))

(define-public crate-dharitri-wasm-node-0.5.6 (c (n "dharitri-wasm-node") (v "0.5.6") (d (list (d (n "dharitri-wasm") (r "^0.5.6") (d #t) (k 0)))) (h "09vk4a35iy8b492g56wni8sqvhlxbc710dcsbkwarjmzqv2m5cwk")))

(define-public crate-dharitri-wasm-node-0.5.7 (c (n "dharitri-wasm-node") (v "0.5.7") (d (list (d (n "dharitri-wasm") (r "^0.5.7") (d #t) (k 0)))) (h "1c9zdfqic0252b7vyjhi6h6c5dq97zcavl15paglaagps0fl54v6")))

(define-public crate-dharitri-wasm-node-0.5.8 (c (n "dharitri-wasm-node") (v "0.5.8") (d (list (d (n "dharitri-wasm") (r "^0.5.8") (d #t) (k 0)))) (h "1axd06daxjgrm3rpmhsxakz3kh987zksmszfsqzwkq9k7j1m2qi2")))

(define-public crate-dharitri-wasm-node-0.5.9 (c (n "dharitri-wasm-node") (v "0.5.9") (d (list (d (n "dharitri-wasm") (r "^0.5.9") (d #t) (k 0)))) (h "10qb81fjarp2v8d868nl5b55iy1g09vbcj3n3kn4maibp61hq2z9")))

(define-public crate-dharitri-wasm-node-0.6.0 (c (n "dharitri-wasm-node") (v "0.6.0") (d (list (d (n "dharitri-wasm") (r "^0.6.0") (d #t) (k 0)))) (h "0jm45qq6q8amlr9zwhzvhlnifx92fpr3prwhgaz6mbw136gf3sdm")))

(define-public crate-dharitri-wasm-node-0.6.1 (c (n "dharitri-wasm-node") (v "0.6.1") (d (list (d (n "dharitri-wasm") (r "^0.6.1") (d #t) (k 0)))) (h "071mdrwp95bvznwakw2lma9zjahjswvdq4mi406sgggyghsazlx4")))

(define-public crate-dharitri-wasm-node-0.6.2 (c (n "dharitri-wasm-node") (v "0.6.2") (d (list (d (n "dharitri-wasm") (r "^0.6.2") (d #t) (k 0)))) (h "05mj542ihw38hpbspnbcwvmwqxrrl0hrsyk2hpk09pk4j4s92ff4")))

(define-public crate-dharitri-wasm-node-0.6.3 (c (n "dharitri-wasm-node") (v "0.6.3") (d (list (d (n "dharitri-wasm") (r "^0.6.3") (d #t) (k 0)))) (h "08z8bh80qqbryw0bvbs0v3cb62hzmk9gqm7dzwg7lbcdxahfdx51")))

(define-public crate-dharitri-wasm-node-0.6.4 (c (n "dharitri-wasm-node") (v "0.6.4") (d (list (d (n "dharitri-wasm") (r "^0.6.4") (d #t) (k 0)))) (h "03s0prw3b7kn3f5znxgmrqmx6wv7flx6gc9xs5mrdfs3j29g78j8") (y #t)))

(define-public crate-dharitri-wasm-node-0.6.5 (c (n "dharitri-wasm-node") (v "0.6.5") (d (list (d (n "dharitri-wasm") (r "^0.6.5") (d #t) (k 0)))) (h "0jq75x9ibrjrvcpnl6idznyi9flwd5hqh8iz162r4ilh0b076ldx") (y #t)))

(define-public crate-dharitri-wasm-node-0.6.6 (c (n "dharitri-wasm-node") (v "0.6.6") (d (list (d (n "dharitri-wasm") (r "^0.6.6") (d #t) (k 0)))) (h "13crq6f0v56vpjlb6wmsydlq8nnqvic26ima0k6m0p58qiyif1vf") (y #t)))

(define-public crate-dharitri-wasm-node-0.6.7 (c (n "dharitri-wasm-node") (v "0.6.7") (d (list (d (n "dharitri-wasm") (r "^0.6.7") (d #t) (k 0)))) (h "1wiz5hxjxz6wzagl3yz8ir3828mpjfypf7c31miqjffcvwf56cvb") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.6.8 (c (n "dharitri-wasm-node") (v "0.6.8") (d (list (d (n "dharitri-wasm") (r "^0.6.8") (d #t) (k 0)))) (h "13xgm5zcf1r73s5v8237d34sm1n4ma86b3fa3071waxpqsixbs61") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.6.9 (c (n "dharitri-wasm-node") (v "0.6.9") (d (list (d (n "dharitri-wasm") (r "^0.6.9") (d #t) (k 0)))) (h "0vh9rnjasaihi10srskzsn4filps43xcqvi811j69lygwczidz8m") (f (quote (("unmanaged-ei"))))))

(define-public crate-dharitri-wasm-node-0.7.0 (c (n "dharitri-wasm-node") (v "0.7.0") (d (list (d (n "dharitri-wasm") (r "^0.7.0") (d #t) (k 0)))) (h "0fwbxx009wd5pswmdjn09zr9iqnz4lcf7gyvh6v1ri2h6wfj2jzc") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.7.1 (c (n "dharitri-wasm-node") (v "0.7.1") (d (list (d (n "dharitri-wasm") (r "^0.7.1") (d #t) (k 0)))) (h "0vrp0k6fy7ldapjjnr9y3g7jxzj07lcg1538jzglhrv65j1r9yvi") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.7.2 (c (n "dharitri-wasm-node") (v "0.7.2") (d (list (d (n "dharitri-wasm") (r "=0.7.2") (d #t) (k 0)))) (h "1w8fgm2kh19k5hm2mi6vqiv4h3j81dk7sifhdf858lvf6662hcad") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.7.3 (c (n "dharitri-wasm-node") (v "0.7.3") (d (list (d (n "dharitri-wasm") (r "=0.7.3") (d #t) (k 0)))) (h "0v2l218a9m3n5pq3wg4yvygkv64l0q38b0kf5rp6qwcdkwy3h8ha") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.7.4 (c (n "dharitri-wasm-node") (v "0.7.4") (d (list (d (n "dharitri-wasm") (r "=0.7.4") (d #t) (k 0)))) (h "0hgaac7s1c2fy1c6g4xr280jvd5kgn7562588r3rdf1nig5s651c") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.7.5 (c (n "dharitri-wasm-node") (v "0.7.5") (d (list (d (n "dharitri-wasm") (r "=0.7.5") (d #t) (k 0)))) (h "0gc97g4j95ckc237m1fb61y7zw2zz33bf2dng16gbgnjbr4bc1am") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.7.6 (c (n "dharitri-wasm-node") (v "0.7.6") (d (list (d (n "dharitri-wasm") (r "=0.7.6") (d #t) (k 0)))) (h "05hgv2xncdz4nncmgzl5skvj7rvn4qnn51wdriha6aiz455853gf") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.7.7 (c (n "dharitri-wasm-node") (v "0.7.7") (d (list (d (n "dharitri-wasm") (r "=0.7.7") (d #t) (k 0)))) (h "1nx5gbsa1sncadammcl8q0rzn299rpknbm3ggvjw2dpbshrisl5g") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.7.8 (c (n "dharitri-wasm-node") (v "0.7.8") (d (list (d (n "dharitri-wasm") (r "=0.7.8") (d #t) (k 0)))) (h "0xxvhqjm7fy30kj4njgv2577wqlzlcwvq9xz0773nikcdahc4kdl") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.7.9 (c (n "dharitri-wasm-node") (v "0.7.9") (d (list (d (n "dharitri-wasm") (r "=0.7.9") (d #t) (k 0)))) (h "0dwx2271f0608qydvl15j4nzrhv9crki30cq3i15qf6a1hdh38c1") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.8.0 (c (n "dharitri-wasm-node") (v "0.8.0") (d (list (d (n "dharitri-wasm") (r "=0.8.0") (d #t) (k 0)))) (h "15106945srg41yfwd7js8bx3dyripjlhhdmxriippk03lzmggz30") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.8.1 (c (n "dharitri-wasm-node") (v "0.8.1") (d (list (d (n "dharitri-wasm") (r "=0.8.1") (d #t) (k 0)))) (h "09xw2s48pjasf4gl88kh750rid3cvwzir8p6zpksiqdf29i3fwk8") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.8.2 (c (n "dharitri-wasm-node") (v "0.8.2") (d (list (d (n "dharitri-wasm") (r "=0.8.2") (d #t) (k 0)))) (h "04nvixa4m4rrihlmmm63zpm0mxzqv52ywjyi8nxnmsmlbg145d92") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.8.3 (c (n "dharitri-wasm-node") (v "0.8.3") (d (list (d (n "dharitri-wasm") (r "=0.8.3") (d #t) (k 0)))) (h "19wjxd1lzd4lb62ywz80j6hq6vq81wdpp6cdkrdmrjfxvs2b67p2") (f (quote (("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.8.4 (c (n "dharitri-wasm-node") (v "0.8.4") (d (list (d (n "dharitri-wasm") (r "=0.8.4") (d #t) (k 0)))) (h "1bhx2cga86vg2gsd5l18n1mjizd7yxcjwdkn1yky2fq9vs7kqif6") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.8.5 (c (n "dharitri-wasm-node") (v "0.8.5") (d (list (d (n "dharitri-wasm") (r "=0.8.5") (d #t) (k 0)))) (h "0kwrj9g3fxgp9yslda8qx5qhxr2wjx0zszhwdlnwwiq1rylcnmxk") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.8.6 (c (n "dharitri-wasm-node") (v "0.8.6") (d (list (d (n "dharitri-wasm") (r "=0.8.6") (d #t) (k 0)))) (h "0gwvrgkb2w170zs586sy9a3s86dalmvpwjgbx36l3z5dnkbk8pdb") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.8.7 (c (n "dharitri-wasm-node") (v "0.8.7") (d (list (d (n "dharitri-wasm") (r "=0.8.7") (d #t) (k 0)))) (h "0k580vxa41gqj695ik2cbylc1117ia5pz00hliqix1b7n41gcxnl") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.8.8 (c (n "dharitri-wasm-node") (v "0.8.8") (d (list (d (n "dharitri-wasm") (r "=0.8.8") (d #t) (k 0)))) (h "12xff21mrkg8faxbz3jp14xz9cb0mszlign58p9rxgscgasnl7rg") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.8.9 (c (n "dharitri-wasm-node") (v "0.8.9") (d (list (d (n "dharitri-wasm") (r "=0.8.9") (d #t) (k 0)))) (h "0wh3xl4avavvh1rrbjirdsd5zx5rklpz5d7d9jdpphbrc72mv18l") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.9.0 (c (n "dharitri-wasm-node") (v "0.9.0") (d (list (d (n "dharitri-wasm") (r "=0.9.0") (d #t) (k 0)))) (h "0qc5wc18fl7wylp9smkcgi6ypf98djsgxvzaiipcxcr7cysjlz0q") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.9.1 (c (n "dharitri-wasm-node") (v "0.9.1") (d (list (d (n "dharitri-wasm") (r "=0.9.1") (d #t) (k 0)))) (h "019jgdwh178nyipaxxr7ai8mylfii1l2cs5axrh44bbjxp7yd9hv") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.9.2 (c (n "dharitri-wasm-node") (v "0.9.2") (d (list (d (n "dharitri-wasm") (r "=0.9.2") (d #t) (k 0)))) (h "1906prswifam4cy6gcpkc4c72wy9x9shpac0l8xxqqcviwz6iziv") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.9.3 (c (n "dharitri-wasm-node") (v "0.9.3") (d (list (d (n "dharitri-wasm") (r "=0.9.3") (d #t) (k 0)))) (h "1rdzvp3sgrgb8vq3v26lqxlhf2gkrkhn8iy3g6vd447748k3n2cr") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.9.4 (c (n "dharitri-wasm-node") (v "0.9.4") (d (list (d (n "dharitri-wasm") (r "=0.9.4") (d #t) (k 0)))) (h "1s2mrfqg862iazs4ivx615g0zapqzn82n67az868q6jk1szqddk7") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.9.5 (c (n "dharitri-wasm-node") (v "0.9.5") (d (list (d (n "dharitri-wasm") (r "=0.9.5") (d #t) (k 0)))) (h "0g3r87rqfw635y8z2whsj3dh0ag54ambz0pwmh9b18764kphzghx") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("unmanaged-ei")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.9.6 (c (n "dharitri-wasm-node") (v "0.9.6") (d (list (d (n "dharitri-wasm") (r "=0.9.6") (d #t) (k 0)))) (h "1942d2zmnzx5nb0ywg2xldf5z1656xrp780w3piiwdb0f0j3r7hv") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged") ("ei-1-1" "dharitri-wasm/ei-1-1")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.9.7 (c (n "dharitri-wasm-node") (v "0.9.7") (d (list (d (n "dharitri-wasm") (r "=0.9.7") (d #t) (k 0)))) (h "1yamhyh6dih2nlwyi0jwh3ha45445fndc3ir4wcx861v93lb6yrf") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.9.8 (c (n "dharitri-wasm-node") (v "0.9.8") (d (list (d (n "dharitri-wasm") (r "=0.9.8") (d #t) (k 0)))) (h "1dmzfil5bw83jcsi8mgh9pi7qxp4km5b904dzkpdg5ibchspz4ky") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged"))))))

(define-public crate-dharitri-wasm-node-0.9.9 (c (n "dharitri-wasm-node") (v "0.9.9") (d (list (d (n "dharitri-wasm") (r "=0.9.9") (d #t) (k 0)))) (h "1qc467712h69ik2657l385hlki2m0vyr43rsx1zixdr46kiy5mbh") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.10.0 (c (n "dharitri-wasm-node") (v "0.10.0") (d (list (d (n "dharitri-wasm") (r "=0.10.0") (d #t) (k 0)))) (h "18d5799wp4jvcddlsiw35whsana5fnnpdjy74dmmgkqsr73qfhf0") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.10.1 (c (n "dharitri-wasm-node") (v "0.10.1") (d (list (d (n "dharitri-wasm") (r "=0.10.1") (d #t) (k 0)))) (h "1gmskqda0plk0dbbi0lr1bw3nv5yjljx9b6310f8b6jajf19gb09") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.10.2 (c (n "dharitri-wasm-node") (v "0.10.2") (d (list (d (n "dharitri-wasm") (r "=0.10.2") (d #t) (k 0)))) (h "19rfzmgbm15nmrgy255y159ic04wxfnmbxz7xfm1dsa55p2626qd") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.10.3 (c (n "dharitri-wasm-node") (v "0.10.3") (d (list (d (n "dharitri-wasm") (r "=0.10.3") (d #t) (k 0)))) (h "1r7jyqa29bwqkzpp7v5przdar2hwnvkragl1jydyxqknvfx1isr4") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.10.4 (c (n "dharitri-wasm-node") (v "0.10.4") (d (list (d (n "dharitri-wasm") (r "=0.10.4") (d #t) (k 0)))) (h "0hhrfp2gf29vsvgvn1m4hz3mz1fi89j1rdazkxmwlikg1id26qkh") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-wasm-node-0.10.5 (c (n "dharitri-wasm-node") (v "0.10.5") (d (list (d (n "dharitri-wasm") (r "=0.10.5") (d #t) (k 0)))) (h "0k939nzq6qf795g59imysnkiq8w7x497hjrc5wv1zbvddp7wz6im") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-wasm-node-0.10.6 (c (n "dharitri-wasm-node") (v "0.10.6") (d (list (d (n "dharitri-wasm") (r "=0.10.6") (d #t) (k 0)))) (h "0kh3gbxvd35qcwnacfqlj5iz6sa747nnqqz7b6k1w5ybgwly45lj") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-wasm-node-0.10.7 (c (n "dharitri-wasm-node") (v "0.10.7") (d (list (d (n "dharitri-wasm") (r "=0.10.7") (d #t) (k 0)))) (h "0nr5qc6x1f5l7pbpk4py77a4fbnk4rs5nx72r8d43pvp06pw6ncl") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node")))) (y #t)))

(define-public crate-dharitri-wasm-node-0.10.8 (c (n "dharitri-wasm-node") (v "0.10.8") (d (list (d (n "dharitri-wasm") (r "=0.10.8") (d #t) (k 0)))) (h "0209cjgsi6b765v9r4d6a4fkmq77l1nmf3r715hr5jqn5gfplrc3") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node")))) (y #t)))

