(define-module (crates-io dh ar dharithri-sc-codec) #:use-module (crates-io))

(define-public crate-dharithri-sc-codec-0.18.1 (c (n "dharithri-sc-codec") (v "0.18.1") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "dharithri-sc-codec-derive") (r "=0.18.1") (o #t) (d #t) (k 0)) (d (n "dharithri-sc-codec-derive") (r "=0.18.1") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "08s5y54f931v6k22m3wyq2bvigrjysxys0wwxy6jhyxgc76dn43b") (f (quote (("derive" "dharithri-sc-codec-derive") ("alloc"))))))

