(define-module (crates-io dh ar dharithri-sc-snippets) #:use-module (crates-io))

(define-public crate-dharithri-sc-snippets-0.43.3 (c (n "dharithri-sc-snippets") (v "0.43.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "dharithri-sc-scenario") (r "=0.43.3") (d #t) (k 0)) (d (n "dharithri-sdk") (r "=0.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("full"))) (d #t) (k 0)))) (h "1q91f9ir4idbjsr1ab80gf6zpj4ln2l0iaqcb1sxdd94pq00plm5")))

