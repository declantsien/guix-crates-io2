(define-module (crates-io dh ar dharvya_api) #:use-module (crates-io))

(define-public crate-dharvya_api-0.1.0 (c (n "dharvya_api") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zq11l75c7k1k5xwr76i9g3s3s58vdxjd2bg81bsba2z7hf36z34")))

