(define-module (crates-io dh ar dharitri-wasm-module-users) #:use-module (crates-io))

(define-public crate-dharitri-wasm-module-users-0.0.2 (c (n "dharitri-wasm-module-users") (v "0.0.2") (d (list (d (n "dharitri-wasm") (r "^0.0.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.2") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.2") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "07wkfn9vw96ci55ql6wjyrlv1s409dgw7k97r09814wjh1nslw1g") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.0.3 (c (n "dharitri-wasm-module-users") (v "0.0.3") (d (list (d (n "dharitri-wasm") (r "^0.0.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.3") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.3") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.3") (o #t) (d #t) (k 0)))) (h "033a4mg811fzaajj3f1br66z71ad1cxx41r9yb5x5s0pcda2hxaz") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.0.4 (c (n "dharitri-wasm-module-users") (v "0.0.4") (d (list (d (n "dharitri-wasm") (r "^0.0.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.4") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.4") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "16n8k0w8ynz4vq1iwx9146gsx9mfdqn501zaaf9p8hr8df8c4l4a") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.0.5 (c (n "dharitri-wasm-module-users") (v "0.0.5") (d (list (d (n "dharitri-wasm") (r "^0.0.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.5") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.5") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.5") (o #t) (d #t) (k 0)))) (h "0xsa36czls77yg4z1vspffgm5hmfa8iifxdx16wadbqprvj66ybn") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.0.6 (c (n "dharitri-wasm-module-users") (v "0.0.6") (d (list (d (n "dharitri-wasm") (r "^0.0.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.6") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.6") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.6") (o #t) (d #t) (k 0)))) (h "147l4a5c491pafk3391kzgyh8x79p0cwkhxvhxwjqmlb19hz5yk3") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.0.7 (c (n "dharitri-wasm-module-users") (v "0.0.7") (d (list (d (n "dharitri-wasm") (r "^0.0.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.7") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.7") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.7") (o #t) (d #t) (k 0)))) (h "1n6949wcsphd9bxdjqw2z5xz5vkvfqwgn9ysla83iv0zdxp208gl") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.0.8 (c (n "dharitri-wasm-module-users") (v "0.0.8") (d (list (d (n "dharitri-wasm") (r "^0.0.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.8") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.8") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.8") (o #t) (d #t) (k 0)))) (h "1mwraia96wd037jjs2562r502k71c8qx2843krhll2jh1l1f23bw") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.0.9 (c (n "dharitri-wasm-module-users") (v "0.0.9") (d (list (d (n "dharitri-wasm") (r "^0.0.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.9") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.9") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.9") (o #t) (d #t) (k 0)))) (h "04pl9wv07isb48wgllfc5lcaj9vrf0fzrswsa2kxa509psh0z789") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.1.0 (c (n "dharitri-wasm-module-users") (v "0.1.0") (d (list (d (n "dharitri-wasm") (r "^0.1.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.0") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "120x1lr4mmrc4wv9i5fcypk2k53vwkzkb1lcn1z2rv0b83fdqa1i") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.1.1 (c (n "dharitri-wasm-module-users") (v "0.1.1") (d (list (d (n "dharitri-wasm") (r "^0.1.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.1") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "18izrdhg1qd6d2pmbyj64hivih3xqfz9km0ymw5wsmhl2liy3scj") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.1.2 (c (n "dharitri-wasm-module-users") (v "0.1.2") (d (list (d (n "dharitri-wasm") (r "^0.1.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.2") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1rxs4dfqrqxqdgkchk2329l9ykp18s57r01llz8w90p4h8q84y2l") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.1.3 (c (n "dharitri-wasm-module-users") (v "0.1.3") (d (list (d (n "dharitri-wasm") (r "^0.1.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.3") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1nb1rvc515hr2yjbsqwzzsd94brraiyxmlkhsnhbdzai3k0fd55j") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.1.4 (c (n "dharitri-wasm-module-users") (v "0.1.4") (d (list (d (n "dharitri-wasm") (r "^0.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.4") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "09zd595lq1wgirvjzpyllkf5x7cyqc1wrbmwsrqhr70k1107lqpw") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.1.5 (c (n "dharitri-wasm-module-users") (v "0.1.5") (d (list (d (n "dharitri-wasm") (r "^0.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.5") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "10g5la0iqzv16m1080lk6hc8pdc28nr97sgf58fb4fxp8fb49jwj") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.1.6 (c (n "dharitri-wasm-module-users") (v "0.1.6") (d (list (d (n "dharitri-wasm") (r "^0.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.6") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "0js5qpzblizk0ykjrpmdl0mdw3flcaq9frqp9dhmi8gcmll0fz11") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.1.7 (c (n "dharitri-wasm-module-users") (v "0.1.7") (d (list (d (n "dharitri-wasm") (r "^0.1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.7") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0rjg4zj95qhfa96148adfh4h1y5vh8vppllp6cdaa2mvlv76j15b") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.1.8 (c (n "dharitri-wasm-module-users") (v "0.1.8") (d (list (d (n "dharitri-wasm") (r "^0.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.8") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "1yh9mgqpm4w681sn502j5m1vva8qf79wsgxd26ksr0q8a6pllczp") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.1.9 (c (n "dharitri-wasm-module-users") (v "0.1.9") (d (list (d (n "dharitri-wasm") (r "^0.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.9") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "1r1dr2y2sm1i759z4xi68y2q6jkrq8dzvw3fspipqsq5zxrr617z") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.2.0 (c (n "dharitri-wasm-module-users") (v "0.2.0") (d (list (d (n "dharitri-wasm") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.0") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "12l411cm9kb4wsvkrj30ww78vh06srdndksbsnjz8ivxacgw20yl") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.2.1 (c (n "dharitri-wasm-module-users") (v "0.2.1") (d (list (d (n "dharitri-wasm") (r "^0.2.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.1") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1v88yylh880x5l4lhpgdkk77jdmz7kd611fy74s0cjsrqi5vpnzb") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.2.2 (c (n "dharitri-wasm-module-users") (v "0.2.2") (d (list (d (n "dharitri-wasm") (r "^0.2.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.2") (d #t) (k 2)))) (h "1wz4b6hgasba5dyxr6gslw3y1vphwkwvwb7im8g0pxjzda5cwzlh")))

(define-public crate-dharitri-wasm-module-users-0.2.3 (c (n "dharitri-wasm-module-users") (v "0.2.3") (d (list (d (n "dharitri-wasm") (r "^0.2.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.3") (d #t) (k 2)))) (h "0nfwvngjk8qa084mpjb8hghrf1j6xv934v0gkn61xiciwa3k32cd")))

(define-public crate-dharitri-wasm-module-users-0.2.4 (c (n "dharitri-wasm-module-users") (v "0.2.4") (d (list (d (n "dharitri-wasm") (r "^0.2.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.4") (d #t) (k 2)))) (h "04blisczchjs06cw3m4jv2g062ywbx0n4dcipa6vylf47sg5gz0w")))

(define-public crate-dharitri-wasm-module-users-0.2.5 (c (n "dharitri-wasm-module-users") (v "0.2.5") (d (list (d (n "dharitri-wasm") (r "^0.2.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.5") (d #t) (k 2)))) (h "1zn5nqa3wfsijnxcr994zj20d97izrz54m5nwiccpx53xxqnmi5l")))

(define-public crate-dharitri-wasm-module-users-0.2.6 (c (n "dharitri-wasm-module-users") (v "0.2.6") (d (list (d (n "dharitri-wasm") (r "^0.2.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.6") (d #t) (k 2)))) (h "0i91ilgfd54zvlkvsvr2fym9i9n0806kzxz1j3w73hslim3qphyx")))

(define-public crate-dharitri-wasm-module-users-0.2.7 (c (n "dharitri-wasm-module-users") (v "0.2.7") (d (list (d (n "dharitri-wasm") (r "^0.2.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.7") (d #t) (k 2)))) (h "12zww31ls0kaq2b44jcn5mz667c07vak1akbjx704n71ggh04zn8")))

(define-public crate-dharitri-wasm-module-users-0.2.8 (c (n "dharitri-wasm-module-users") (v "0.2.8") (d (list (d (n "dharitri-wasm") (r "^0.2.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.8") (d #t) (k 2)))) (h "0jhnqqh57kfc4d6rnfkrlzg0pks26wfxs5klz2xwi80nc074d8vz")))

(define-public crate-dharitri-wasm-module-users-0.2.9 (c (n "dharitri-wasm-module-users") (v "0.2.9") (d (list (d (n "dharitri-wasm") (r "^0.2.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.9") (d #t) (k 2)))) (h "14j7rsriqzw6w2whvid3di1kpxlbbn8hjpn6ac3pz6rwisy048gb")))

(define-public crate-dharitri-wasm-module-users-0.3.0 (c (n "dharitri-wasm-module-users") (v "0.3.0") (d (list (d (n "dharitri-wasm") (r "^0.3.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.0") (d #t) (k 2)))) (h "1rmmr07k39k51wh3zkdj20zxyi8czsb6k8xipmhaxbgl1dzaf5cd")))

(define-public crate-dharitri-wasm-module-users-0.3.1 (c (n "dharitri-wasm-module-users") (v "0.3.1") (d (list (d (n "dharitri-wasm") (r "^0.3.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.1") (d #t) (k 2)))) (h "07rkgg65shagdrl2ycmdpc59p343aa0pfygyxn26jhq9p71ywcaq")))

(define-public crate-dharitri-wasm-module-users-0.3.2 (c (n "dharitri-wasm-module-users") (v "0.3.2") (d (list (d (n "dharitri-wasm") (r "^0.3.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.2") (d #t) (k 2)))) (h "1vz70v3na5hsczc1qhmfndsn4hdgqf2xi69aj0w2k70r5n8bpsbb")))

(define-public crate-dharitri-wasm-module-users-0.3.3 (c (n "dharitri-wasm-module-users") (v "0.3.3") (d (list (d (n "dharitri-wasm") (r "^0.3.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.3") (d #t) (k 2)))) (h "0f410c71kdis3jllwirpd9qlpq6i4dzri2hq5j1xycc51kdpz2wr")))

(define-public crate-dharitri-wasm-module-users-0.3.4 (c (n "dharitri-wasm-module-users") (v "0.3.4") (d (list (d (n "dharitri-wasm") (r "^0.3.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.4") (d #t) (k 2)))) (h "1ri16728a8aj991ikirw6y5izxqcdzf9rn0zsq06rwkqy2wi2880")))

(define-public crate-dharitri-wasm-module-users-0.3.5 (c (n "dharitri-wasm-module-users") (v "0.3.5") (d (list (d (n "dharitri-wasm") (r "^0.3.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.5") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.5") (d #t) (k 0)))) (h "0l3wr4k2f1mp4hcchb94ni52ishxpgb605njhw1c06mwvs8l6rxg")))

(define-public crate-dharitri-wasm-module-users-0.3.6 (c (n "dharitri-wasm-module-users") (v "0.3.6") (d (list (d (n "dharitri-wasm") (r "^0.3.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.6") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.6") (d #t) (k 0)))) (h "1wc6j7kbn64s51gz1zcjnpq7886sv9khl9j4w8ql56cp62qf4479")))

(define-public crate-dharitri-wasm-module-users-0.3.7 (c (n "dharitri-wasm-module-users") (v "0.3.7") (d (list (d (n "dharitri-wasm") (r "^0.3.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.7") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.7") (d #t) (k 0)))) (h "1c7ad4vsd15s1c1f8mflll5a4r38d0hb966jrcw1bqz4zckysi57")))

(define-public crate-dharitri-wasm-module-users-0.3.8 (c (n "dharitri-wasm-module-users") (v "0.3.8") (d (list (d (n "dharitri-wasm") (r "^0.3.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.8") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.8") (d #t) (k 0)))) (h "1mrsipxnhsfl9dfdg8y89x0agxdca2j2ssqgdz8y306r7i8y1m6l")))

(define-public crate-dharitri-wasm-module-users-0.3.9 (c (n "dharitri-wasm-module-users") (v "0.3.9") (d (list (d (n "dharitri-wasm") (r "^0.3.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.9") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.9") (d #t) (k 0)))) (h "1n93a54hf9p7mn4s41q04pylzijl8yxifzjm700s18hd4dmh8pv8")))

(define-public crate-dharitri-wasm-module-users-0.4.0 (c (n "dharitri-wasm-module-users") (v "0.4.0") (d (list (d (n "dharitri-wasm") (r "^0.4.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.4.0") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.4.0") (d #t) (k 0)))) (h "0q76ai2pgz7g6c31m3bi82qp39inrj2bvhf73k46zwl6pm9dp9li")))

(define-public crate-dharitri-wasm-module-users-0.4.1 (c (n "dharitri-wasm-module-users") (v "0.4.1") (d (list (d (n "dharitri-wasm") (r "^0.4.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.4.1") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.4.1") (d #t) (k 0)))) (h "0bgr19hhvql44hqzh03grjgzllih0rjcy2qdiq349k6bz91kbqjj")))

(define-public crate-dharitri-wasm-module-users-0.4.2 (c (n "dharitri-wasm-module-users") (v "0.4.2") (d (list (d (n "dharitri-wasm") (r "^0.4.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.4.2") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.4.2") (d #t) (k 0)))) (h "1klr3ghfgvh9q4acvndlpg4iyxfhas3qi4pmzjs0cmw5wg88fcbf")))

(define-public crate-dharitri-wasm-module-users-0.5.5 (c (n "dharitri-wasm-module-users") (v "0.5.5") (d (list (d (n "dharitri-wasm") (r "^0.5.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.5") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.5") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.5") (o #t) (d #t) (k 0)))) (h "1bkbgk2l4xczm2l6iczax576akiqvxns4ba8li3v10ysny3r3g62") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.5.6 (c (n "dharitri-wasm-module-users") (v "0.5.6") (d (list (d (n "dharitri-wasm") (r "^0.5.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.6") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.6") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "0zd2q1fnig482p2q1fp69iwpbklhvj9bwqqvicsj2srpf8ynli9m") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.5.7 (c (n "dharitri-wasm-module-users") (v "0.5.7") (d (list (d (n "dharitri-wasm") (r "^0.5.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.7") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.7") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.7") (o #t) (d #t) (k 0)))) (h "1p7i10pw3fhymkrzzcfjc682fzpn9bbk3k0f39rmgj3xwjkffm03") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.5.8 (c (n "dharitri-wasm-module-users") (v "0.5.8") (d (list (d (n "dharitri-wasm") (r "^0.5.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.8") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.8") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.8") (o #t) (d #t) (k 0)))) (h "06a2ii24hq6sdhp1ili4zps1sgqj8g5ck1dm1wm304sb5g4phn75") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.5.9 (c (n "dharitri-wasm-module-users") (v "0.5.9") (d (list (d (n "dharitri-wasm") (r "^0.5.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.9") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.9") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "1sg6r808n4868vnvqy6qfq4nign5bi4mya2fxzgxp7pg4569cfaq") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.6.0 (c (n "dharitri-wasm-module-users") (v "0.6.0") (d (list (d (n "dharitri-wasm") (r "^0.6.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.0") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0n2pb0c5a51i2fzi72k1mryxn38l1519k27bh8kd7aww9wvk2x5q") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.6.1 (c (n "dharitri-wasm-module-users") (v "0.6.1") (d (list (d (n "dharitri-wasm") (r "^0.6.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.1") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.6.1") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "0sfr183sfyv0xgrxh7bx9v1da690b5f8i7jig30d9zwa7labkm8s") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.6.2 (c (n "dharitri-wasm-module-users") (v "0.6.2") (d (list (d (n "dharitri-wasm") (r "^0.6.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.2") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.6.2") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "05syxzng0mlh26hrlcznwd5hfw96jv31w5d5cxn20inkyyzvqbdp") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.6.3 (c (n "dharitri-wasm-module-users") (v "0.6.3") (d (list (d (n "dharitri-wasm") (r "^0.6.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.3") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.6.3") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "0f97gsfgllc8qaqhj5axl5aw5vx8pfjijn42gbrajjjxc620nxq0") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.6.4 (c (n "dharitri-wasm-module-users") (v "0.6.4") (d (list (d (n "dharitri-wasm") (r "^0.6.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.4") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "1cx07vf3mhyq5nfkwcirhm8z97mj3fbpclx10314b245vwa58lmg") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.6.5 (c (n "dharitri-wasm-module-users") (v "0.6.5") (d (list (d (n "dharitri-wasm") (r "^0.6.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.5") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.5") (o #t) (d #t) (k 0)))) (h "1qakhinnlammaa948bi78l03vhyrb8nw24363m0r7awlihsfxbia") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.6.6 (c (n "dharitri-wasm-module-users") (v "0.6.6") (d (list (d (n "dharitri-wasm") (r "^0.6.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.6") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.6") (o #t) (d #t) (k 0)))) (h "1pjhmq8gvp1ln2758p4fpi121zsg5msnw0abiafsspabjzz18g4n") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.6.7 (c (n "dharitri-wasm-module-users") (v "0.6.7") (d (list (d (n "dharitri-wasm") (r "^0.6.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.7") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "1a8fh7ampapwv97v8y2pz1bz25k4vh83zmd2gwl96hlk8x83lnbg") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.6.8 (c (n "dharitri-wasm-module-users") (v "0.6.8") (d (list (d (n "dharitri-wasm") (r "^0.6.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.8") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.8") (o #t) (d #t) (k 0)))) (h "1n2f60vkr6hfgqda59bzvp4yl8f5g30mg295x21bh19kl7rf0vfa") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.6.9 (c (n "dharitri-wasm-module-users") (v "0.6.9") (d (list (d (n "dharitri-wasm") (r "^0.6.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.9") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.9") (o #t) (d #t) (k 0)))) (h "1vqa436nwr7cnppbqg5mphyfxin4abmh4wzk9g466v5fxq61jv6d") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.7.0 (c (n "dharitri-wasm-module-users") (v "0.7.0") (d (list (d (n "dharitri-wasm") (r "^0.7.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.0") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "080i6gljz7bybfkivji5yz36v0cghhp3zxvgc239z0skzjy7k88j") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.7.1 (c (n "dharitri-wasm-module-users") (v "0.7.1") (d (list (d (n "dharitri-wasm") (r "^0.7.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.1") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "194rldclq1jsay5hgv2nf3k4ksgmv5pmrmglyiwma502ya2zl4qf") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-users-0.7.2 (c (n "dharitri-wasm-module-users") (v "0.7.2") (d (list (d (n "dharitri-wasm") (r "^0.7.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.2") (d #t) (k 2)))) (h "1dnwbqfpb091whb2zwacmlg4zsh1fql5lqhly65n3sl2fa74i1r7")))

(define-public crate-dharitri-wasm-module-users-0.7.3 (c (n "dharitri-wasm-module-users") (v "0.7.3") (d (list (d (n "dharitri-wasm") (r "^0.7.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.3") (d #t) (k 2)))) (h "1ddflpsm8cl56s36238c3r87c004dw8gczsc866z8k1x2jay8rwf")))

(define-public crate-dharitri-wasm-module-users-0.7.4 (c (n "dharitri-wasm-module-users") (v "0.7.4") (d (list (d (n "dharitri-wasm") (r "^0.7.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.4") (d #t) (k 2)))) (h "1lnhmz62fmryzcc0hfrm8x5yf64p7jhjcvxr1x0i233jwa8siv1j")))

(define-public crate-dharitri-wasm-module-users-0.7.5 (c (n "dharitri-wasm-module-users") (v "0.7.5") (d (list (d (n "dharitri-wasm") (r "^0.7.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.5") (d #t) (k 2)))) (h "19krd30h46nsbdxz0vrq3sfdsq49lp8q9hqyh6v35hwy3f6lk4mk")))

(define-public crate-dharitri-wasm-module-users-0.7.6 (c (n "dharitri-wasm-module-users") (v "0.7.6") (d (list (d (n "dharitri-wasm") (r "^0.7.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.6") (d #t) (k 2)))) (h "0b8s3bcr7f7kvmaa2w0znn5pvk6gjx29f01i9l02m6q4q6nq4vcb")))

(define-public crate-dharitri-wasm-module-users-0.7.7 (c (n "dharitri-wasm-module-users") (v "0.7.7") (d (list (d (n "dharitri-wasm") (r "^0.7.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.7") (d #t) (k 2)))) (h "1v772bsqpfarr49fjxlbyqrd5z7w93zgqa39w1drr8l2i3nwbq5k")))

(define-public crate-dharitri-wasm-module-users-0.7.8 (c (n "dharitri-wasm-module-users") (v "0.7.8") (d (list (d (n "dharitri-wasm") (r "^0.7.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.8") (d #t) (k 2)))) (h "00y8nsdm1m32d97fc25p2hb2l3fj9alcnaga5barisjjwnjbd2fa")))

(define-public crate-dharitri-wasm-module-users-0.7.9 (c (n "dharitri-wasm-module-users") (v "0.7.9") (d (list (d (n "dharitri-wasm") (r "^0.7.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.9") (d #t) (k 2)))) (h "16ja1jgjh607lz2ww54x3sjlsvch4bjjccg2h3dw2zq7zp159y9b")))

(define-public crate-dharitri-wasm-module-users-0.8.0 (c (n "dharitri-wasm-module-users") (v "0.8.0") (d (list (d (n "dharitri-wasm") (r "^0.8.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.0") (d #t) (k 2)))) (h "1l1xwkqd50350s6g3sp865gpy970068fgbi05ljq0mwxr7i8c4c6")))

(define-public crate-dharitri-wasm-module-users-0.8.1 (c (n "dharitri-wasm-module-users") (v "0.8.1") (d (list (d (n "dharitri-wasm") (r "^0.8.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.1") (d #t) (k 2)))) (h "0mw3p7a7drakaq1kpr90zmxf61bnhkwalaj3n71hp9skq5ymm8hm")))

(define-public crate-dharitri-wasm-module-users-0.8.2 (c (n "dharitri-wasm-module-users") (v "0.8.2") (d (list (d (n "dharitri-wasm") (r "^0.8.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.2") (d #t) (k 2)))) (h "1dipd8844f3nx89wm1r2ayc689by8i0dp94ssm6482f5ipc0mw6i")))

(define-public crate-dharitri-wasm-module-users-0.8.3 (c (n "dharitri-wasm-module-users") (v "0.8.3") (d (list (d (n "dharitri-wasm") (r "^0.8.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.3") (d #t) (k 2)))) (h "1qjzkram508xij109ss88ydvkgmqi4d54f39b02c9j3n0yjzrrsx")))

(define-public crate-dharitri-wasm-module-users-0.8.4 (c (n "dharitri-wasm-module-users") (v "0.8.4") (d (list (d (n "dharitri-wasm") (r "^0.8.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.4") (d #t) (k 2)))) (h "13d7kk5nsmrd26lbqrjn3ml193ys8g2fc83ap17rp29vywwmv224")))

(define-public crate-dharitri-wasm-module-users-0.8.5 (c (n "dharitri-wasm-module-users") (v "0.8.5") (d (list (d (n "dharitri-wasm") (r "^0.8.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.5") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.5") (d #t) (k 0)))) (h "0kdnfrgjxsnf1lh7sc68d16hgblpnyi7c64432spnc59qrq6hfbz")))

(define-public crate-dharitri-wasm-module-users-0.8.6 (c (n "dharitri-wasm-module-users") (v "0.8.6") (d (list (d (n "dharitri-wasm") (r "^0.8.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.6") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.6") (d #t) (k 0)))) (h "0j52dld0j7380pfj8hl2c46h94p3ggvdmp1vl29qb3w56zajxd80")))

(define-public crate-dharitri-wasm-module-users-0.8.7 (c (n "dharitri-wasm-module-users") (v "0.8.7") (d (list (d (n "dharitri-wasm") (r "^0.8.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.7") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.7") (d #t) (k 0)))) (h "0dd90axqxzxvlv1nci3ima4qs9mhm4hsrm3966hivccbd6l0d4sy")))

(define-public crate-dharitri-wasm-module-users-0.8.8 (c (n "dharitri-wasm-module-users") (v "0.8.8") (d (list (d (n "dharitri-wasm") (r "^0.8.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.8") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.8") (d #t) (k 0)))) (h "1525swhcy5fnfq0gdy8h6a2ri4jbzglyi9fcvpzczis2i5ylqgs2")))

(define-public crate-dharitri-wasm-module-users-0.8.9 (c (n "dharitri-wasm-module-users") (v "0.8.9") (d (list (d (n "dharitri-wasm") (r "^0.8.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.9") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.9") (d #t) (k 0)))) (h "0fvkgwzd5lpq09s1pq6l3h4cjr2463g5c38wiz2gpzg5jmr3qp1p")))

(define-public crate-dharitri-wasm-module-users-0.9.0 (c (n "dharitri-wasm-module-users") (v "0.9.0") (d (list (d (n "dharitri-wasm") (r "^0.9.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.9.0") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.9.0") (d #t) (k 0)))) (h "0md04d4304xqnxjl3vs6qfdw7wcg82vn814q4wxbwd036zman055")))

(define-public crate-dharitri-wasm-module-users-0.9.1 (c (n "dharitri-wasm-module-users") (v "0.9.1") (d (list (d (n "dharitri-wasm") (r "^0.9.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.9.1") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.9.1") (d #t) (k 0)))) (h "0x2r686nn60wifswbbpcpnfbl7jbks6kykgkyhf5ja9c28aq66bw")))

(define-public crate-dharitri-wasm-module-users-0.9.2 (c (n "dharitri-wasm-module-users") (v "0.9.2") (d (list (d (n "dharitri-wasm") (r "^0.9.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.9.2") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.9.2") (d #t) (k 0)))) (h "0bddyj4vndm6gbcwv4ia0lzmg31xs1vhk3fjai8kfb4rgd5cmj5q")))

