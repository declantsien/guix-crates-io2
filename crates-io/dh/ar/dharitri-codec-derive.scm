(define-module (crates-io dh ar dharitri-codec-derive) #:use-module (crates-io))

(define-public crate-dharitri-codec-derive-0.0.40 (c (n "dharitri-codec-derive") (v "0.0.40") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vqq1wfb6nfxg58fzi5jaqdw9s4fs9jixn9hlc125r0zkjlyachx") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.0.50 (c (n "dharitri-codec-derive") (v "0.0.50") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z2gr7gagq0ax3g1b5mrryj60f2px78361h0dqnkgagab1b4w1ff") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.0.1 (c (n "dharitri-codec-derive") (v "0.0.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rzdspaqjvfyvmnjzirqdkgj3n9mj834gbffr3z8gc46pqj193zw") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.0.2 (c (n "dharitri-codec-derive") (v "0.0.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09g1pxrdlnxg53q0wcdg5cy90nfnn3ijq936yn1n54d8bybz4rqg") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.0.3 (c (n "dharitri-codec-derive") (v "0.0.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ppdp0905szr96ficahcap07zvpax4nv10kngf8hmcrc49lbavng") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.0.4 (c (n "dharitri-codec-derive") (v "0.0.4") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rs7yy4j6y1rvp1is09m1gqg7q946cbp8d6xd7db4hrrlvgsqr9l") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.0.5 (c (n "dharitri-codec-derive") (v "0.0.5") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jyhp58agg7z7ngj6h00145cbl7iahz4142yl8nv2wbd62665p42") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.0.6 (c (n "dharitri-codec-derive") (v "0.0.6") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "193yrmgszjj8xl7ymi19l4gqbrydsvj86swrqvciyii964zpa492") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.0.7 (c (n "dharitri-codec-derive") (v "0.0.7") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x2ksczrr6kr1d7ckjgnsfvbkxc7vg7a99iwr3lhbmm9kn1l6nzk") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.0.8 (c (n "dharitri-codec-derive") (v "0.0.8") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16qvfqgd3svg300pmpwhc3q7a41a1lmgrq0j4h20f3xy500z7a8x") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.0.9 (c (n "dharitri-codec-derive") (v "0.0.9") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hw3k4y8v288s2ajwm8lqkcqynk0pmfncigc6g4994yd9ijdzdnp") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.1.0 (c (n "dharitri-codec-derive") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jd01znjajmvppm2pgwnk78iph2dz4mbpa4xyzd7j0qr7x20ywi8") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.1.1 (c (n "dharitri-codec-derive") (v "0.1.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15ika490prgqza7ivav29sj2z3nv9gqykcyix8j9yb829bqmvjrf") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.1.2 (c (n "dharitri-codec-derive") (v "0.1.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0c2pjcp1gwnwq6qxajncyaqdrclvyk8w6cfd6ksr8mcqgvbcpdaq") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.1.3 (c (n "dharitri-codec-derive") (v "0.1.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "086rr93dgxqr96k7wvb354awylyd3h81hlsr6i64sk9q06vlqds3") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.1.4 (c (n "dharitri-codec-derive") (v "0.1.4") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ycn5zb6kvx060mii1g2q56ksip6q5w9mym5fqqrdrg3cl59c49h") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.1.5 (c (n "dharitri-codec-derive") (v "0.1.5") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1p52s51asawzpj6893kpyii2fplmxmq9y44w8abrhbydg6fsg1zg") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.1.6 (c (n "dharitri-codec-derive") (v "0.1.6") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17qivsfn2fbp0374kd2bpn74p1ygbmfyfcnb2xqggbpk63j1fclp") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.1.7 (c (n "dharitri-codec-derive") (v "0.1.7") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0n36drbcxypvccs70x0fanlf1pxdbbfljs8v2daj207pi96kk0iz") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.1.8 (c (n "dharitri-codec-derive") (v "0.1.8") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04nxii0h83iak778zdbarr7s4bb45cfwzjda6r9s28kg5148s890") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.1.9 (c (n "dharitri-codec-derive") (v "0.1.9") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wh9g9n5sa41m93n17arg6phn6n450aby5vs2fr5mscbm6w651za") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.2.0 (c (n "dharitri-codec-derive") (v "0.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fshz4y2ffzm2dx9hy75xsh9bjwazizra9s7cdn5qpad2b3ah929") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.2.1 (c (n "dharitri-codec-derive") (v "0.2.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fs4as7h32927y05gvbmqh3l8qnza40004ihvd87mr3y9ipfrng7") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.2.2 (c (n "dharitri-codec-derive") (v "0.2.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xlidknxizjx51d7b33h4p0p6z9bbqkkgsjmly6slvl695w9ar5c") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.2.3 (c (n "dharitri-codec-derive") (v "0.2.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fy2cab4qaqalp0w0j19is0wbx9mpl3klm1s43vdzgd9qsl0hk0k") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.2.4 (c (n "dharitri-codec-derive") (v "0.2.4") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03x3qz4xkrkjg5i5a59xkxagwsyb9vg2w1g8kdv5b4355v4zvwr6") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.2.5 (c (n "dharitri-codec-derive") (v "0.2.5") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pb26gml99rm6gvvh1zaa9mf9ylipnzkwdvdfbm2xw2rdp0rskca") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.2.6 (c (n "dharitri-codec-derive") (v "0.2.6") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ajmqv43mkcf23airr3cgjk54kxrv110ykvzg1q0j2n3df9xyl65") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.2.7 (c (n "dharitri-codec-derive") (v "0.2.7") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1n5fi8rdf2wwapr691v8pvyw7y9j49dx9rgf115dki3bj9z95yx3") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.2.8 (c (n "dharitri-codec-derive") (v "0.2.8") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k29l6gn812kmyfshnqfi3pkka9978wrpq50r1rfsdh9cpfcym62") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.2.9 (c (n "dharitri-codec-derive") (v "0.2.9") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bn10bfdfr1cmp98g6m1l760xkll6jypxgv5w5li7l0xlrbr5p5y") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.3.0 (c (n "dharitri-codec-derive") (v "0.3.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05whib6fwva0dg4crymapj0vgf8xlcspqbwvm7ix6pr07sl77f6m") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.3.1 (c (n "dharitri-codec-derive") (v "0.3.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04i80vyrhz1kxl9h2knawfhfz1zc5jlr9plljc5n3rgpksbsk7vg") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.3.2 (c (n "dharitri-codec-derive") (v "0.3.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10f5hzyiabihp98x96mqzv2mfhl3g65lvj1l195jiv238smlvg41") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.3.3 (c (n "dharitri-codec-derive") (v "0.3.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1944qsv9l956bpp7slh57rqj3nphsn0hsk2rlnj1lyghvs4wb7jp") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.3.4 (c (n "dharitri-codec-derive") (v "0.3.4") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1w1sb7vl5p0j7i8hnffl55hxvfi0aa58pnmxwg9i3mwbivkcgay1") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.3.5 (c (n "dharitri-codec-derive") (v "0.3.5") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02gi7qgjdpafk4adhhd6cxklkf59by8pa67cdr7yhwx5rr5nrnqq") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.3.6 (c (n "dharitri-codec-derive") (v "0.3.6") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "080inppd2wzmyfp04z7gkdzlzl1az3wj2297951mi70h00hjzwz9") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.3.7 (c (n "dharitri-codec-derive") (v "0.3.7") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qf4dh901pdbpamp99xcj05fqw22kprx0hkvbcdfpn24wbkq2rzr") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.3.8 (c (n "dharitri-codec-derive") (v "0.3.8") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xv1fbaf8awywn2xginlimdk34zhypd7dm2fph86hqdc2xazbrq6") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.3.9 (c (n "dharitri-codec-derive") (v "0.3.9") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kjv2alf7fxk83c4xwadi1hbbk0038b68x7ca59p0jcq9z5klr0m") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.4.0 (c (n "dharitri-codec-derive") (v "0.4.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1i7vqx3x2nsx0md5vq8kwkyw2s9ml200fldvs46mksn1h0s7gc4c") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-codec-derive-0.4.1 (c (n "dharitri-codec-derive") (v "0.4.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qy1ip6l7gklc2x2qar3wr9i3fk69lyr0zdp29f5r70c8g8hw668") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

