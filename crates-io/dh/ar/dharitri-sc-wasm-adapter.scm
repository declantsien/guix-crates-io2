(define-module (crates-io dh ar dharitri-sc-wasm-adapter) #:use-module (crates-io))

(define-public crate-dharitri-sc-wasm-adapter-0.0.1 (c (n "dharitri-sc-wasm-adapter") (v "0.0.1") (d (list (d (n "dharitri-sc") (r "=0.0.1") (d #t) (k 0)))) (h "0hhgk96vwdycymxp84kirg04w6xqkxrcki01awwa2ikwn6zx418c")))

(define-public crate-dharitri-sc-wasm-adapter-0.0.2 (c (n "dharitri-sc-wasm-adapter") (v "0.0.2") (d (list (d (n "dharitri-sc") (r "=0.0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1mw4pzcwmjwlrn6vqw0v7xp2n714xjqr33fh34ykqpx0h16mnv6f") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.0.3 (c (n "dharitri-sc-wasm-adapter") (v "0.0.3") (d (list (d (n "dharitri-sc") (r "=0.0.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "07m64gc5v4q5daaa0412imh8s4k2hh3r7r4agnhs9rmv0yg527bj") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.0.4 (c (n "dharitri-sc-wasm-adapter") (v "0.0.4") (d (list (d (n "dharitri-sc") (r "=0.0.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1kflldm78x3646kmxdgzh560wvfcvlskrnx8cz0fdfywmczigkdn") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.0.5 (c (n "dharitri-sc-wasm-adapter") (v "0.0.5") (d (list (d (n "dharitri-sc") (r "=0.0.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0wcd3wayq77nd9qsc294s8n2lrwkvrfniz619swazcbpvxjgabrj") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.0.6 (c (n "dharitri-sc-wasm-adapter") (v "0.0.6") (d (list (d (n "dharitri-sc") (r "=0.0.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0sbcnh6vx5vx48lbghrl3v3h43099b1wq9il8yw2dfb14vw1lphp") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.0.7 (c (n "dharitri-sc-wasm-adapter") (v "0.0.7") (d (list (d (n "dharitri-sc") (r "=0.0.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1qrl80pz159syj9bn4dz5cw0574s9c7ddrs0fh5wvv5yh7fiy3q8") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.0.8 (c (n "dharitri-sc-wasm-adapter") (v "0.0.8") (d (list (d (n "dharitri-sc") (r "=0.0.8") (d #t) (k 0)))) (h "072bnrzbpp9rw6y1fagvq7nfk60sjyr05mv5ffdms6ychhlslp9w") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.0.9 (c (n "dharitri-sc-wasm-adapter") (v "0.0.9") (d (list (d (n "dharitri-sc") (r "=0.0.9") (d #t) (k 0)))) (h "0d8lp3dfqqyrkf7i7mccyq6wcmvmd9jvcgrl8ayw4xgqb98d0bwa") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.1.0 (c (n "dharitri-sc-wasm-adapter") (v "0.1.0") (d (list (d (n "dharitri-sc") (r "=0.1.0") (d #t) (k 0)))) (h "1zw7z16pz06rrhjgljdjnvyvc2a0lhp4864mgj0c0hwmi5l7ij76") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.1.1 (c (n "dharitri-sc-wasm-adapter") (v "0.1.1") (d (list (d (n "dharitri-sc") (r "=0.1.1") (d #t) (k 0)))) (h "05z11ng05gc75la5yva0an1wjadjby9rdgb6257nswc2jwllajna") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.1.2 (c (n "dharitri-sc-wasm-adapter") (v "0.1.2") (d (list (d (n "dharitri-sc") (r "=0.1.2") (d #t) (k 0)))) (h "059x6qc0pvlwfk2sdd11bbyz8w6qzl59hzrzakq4vfd5d1j29z8k")))

(define-public crate-dharitri-sc-wasm-adapter-0.1.3 (c (n "dharitri-sc-wasm-adapter") (v "0.1.3") (d (list (d (n "dharitri-sc") (r "=0.1.3") (d #t) (k 0)))) (h "0642j345rfpbxhgg3n63b6pm7hzx3z8lib03azwlzynfi07lzzrz")))

(define-public crate-dharitri-sc-wasm-adapter-0.1.4 (c (n "dharitri-sc-wasm-adapter") (v "0.1.4") (d (list (d (n "dharitri-sc") (r "=0.1.4") (d #t) (k 0)))) (h "1pgvcd27bw5zi8i3m07p5gan6z90ykb9ap25phkxygrmydp3l0hr")))

(define-public crate-dharitri-sc-wasm-adapter-0.1.5 (c (n "dharitri-sc-wasm-adapter") (v "0.1.5") (d (list (d (n "dharitri-sc") (r "=0.1.5") (d #t) (k 0)))) (h "0y1xby0iqn92cs3p82bdwnwc59p65pr89yskvmmnpcpi56aqqszz")))

(define-public crate-dharitri-sc-wasm-adapter-0.1.6 (c (n "dharitri-sc-wasm-adapter") (v "0.1.6") (d (list (d (n "dharitri-sc") (r "=0.1.6") (d #t) (k 0)))) (h "1nv1w7w7q61b3k2c3rz86zabmzs2qxp53mbyij29h3kp0mlg5zwj")))

(define-public crate-dharitri-sc-wasm-adapter-0.1.7 (c (n "dharitri-sc-wasm-adapter") (v "0.1.7") (d (list (d (n "dharitri-sc") (r "=0.1.7") (d #t) (k 0)))) (h "1iilpk4n1aznynj5wn8582y0fcbjpydg23vwpkaiw9m40p7c2nig")))

(define-public crate-dharitri-sc-wasm-adapter-0.1.8 (c (n "dharitri-sc-wasm-adapter") (v "0.1.8") (d (list (d (n "dharitri-sc") (r "=0.1.8") (d #t) (k 0)))) (h "1qdyvmiqim0mj7gfacffrswrimqpjw58lc0ggc1zywm7jkhbaiy2")))

(define-public crate-dharitri-sc-wasm-adapter-0.1.9 (c (n "dharitri-sc-wasm-adapter") (v "0.1.9") (d (list (d (n "dharitri-sc") (r "=0.1.9") (d #t) (k 0)))) (h "0i729vg5q79i48wr4a7y1hzwkziwi4czxhm1jhixs8hx9cb2fsff")))

(define-public crate-dharitri-sc-wasm-adapter-0.2.0 (c (n "dharitri-sc-wasm-adapter") (v "0.2.0") (d (list (d (n "dharitri-sc") (r "=0.2.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1008646klm9r4y0xhncbcd7qm6w0k3f3h1g87ifk1y1bfqawbdcq") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.10.9 (c (n "dharitri-sc-wasm-adapter") (v "0.10.9") (d (list (d (n "dharitri-sc") (r "=0.10.9") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1afjxnfcc3vibq25cciaw0qz053qcq0vsd9xss82an4ghwfas1vm") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.11.0 (c (n "dharitri-sc-wasm-adapter") (v "0.11.0") (d (list (d (n "dharitri-sc") (r "=0.11.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0v2g2g5sfvfz6hl8hw8sc1gjncs88kx4krwg94pzjqh5qczlq2r2") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.11.1 (c (n "dharitri-sc-wasm-adapter") (v "0.11.1") (d (list (d (n "dharitri-sc") (r "=0.11.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1yqzk8cax0m8a7p3g9wpx4di4jzhvj6ldgzr3435pm618gz8igh3") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.11.2 (c (n "dharitri-sc-wasm-adapter") (v "0.11.2") (d (list (d (n "dharitri-sc") (r "=0.11.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1arppkccr3jf7l418c4rad96nd2gsfapw6nzsf2jxy4hkr253iiv") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.11.3 (c (n "dharitri-sc-wasm-adapter") (v "0.11.3") (d (list (d (n "dharitri-sc") (r "=0.11.3") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1hzvky5y0934cp15pmiaiqgv9y3qwmrs1880jk54w03773r6z96d") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.11.4 (c (n "dharitri-sc-wasm-adapter") (v "0.11.4") (d (list (d (n "dharitri-sc") (r "=0.11.4") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0d52z5rpc1i1n32ym16xxb1jfd82zni5icslsanqlvdidvc80v0d") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.11.5 (c (n "dharitri-sc-wasm-adapter") (v "0.11.5") (d (list (d (n "dharitri-sc") (r "=0.11.5") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1rzr0j000kdj9bg5a89p4hyr48l6xi5ngdcg8ykhjzp9diwr8xxc") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.11.6 (c (n "dharitri-sc-wasm-adapter") (v "0.11.6") (d (list (d (n "dharitri-sc") (r "=0.11.6") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "098532xkd73zvq1588ghdh4fn7mnvz61l7yvdivygjl25wfqv9ss") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.11.7 (c (n "dharitri-sc-wasm-adapter") (v "0.11.7") (d (list (d (n "dharitri-sc") (r "=0.11.7") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0j131nn9q3whbr0kbgvcr1mhdgx0k452zi6rqx8x6qkagq8xs1a7") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.11.8 (c (n "dharitri-sc-wasm-adapter") (v "0.11.8") (d (list (d (n "dharitri-sc") (r "=0.11.8") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1070w235xhharzqm0mfqnj4444vkjimwirp28xpan9xani8g60sg") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.11.9 (c (n "dharitri-sc-wasm-adapter") (v "0.11.9") (d (list (d (n "dharitri-sc") (r "=0.11.9") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0v7gr3fk4qig0ll0q7h0pah3rg0flwlh9wdp0kw7ryv6z2cjwqd1") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.12.0 (c (n "dharitri-sc-wasm-adapter") (v "0.12.0") (d (list (d (n "dharitri-sc") (r "=0.12.0") (d #t) (k 0)))) (h "0lypgb145y4rawr093b3d0hcqxp1i3ymcgili12hrbw0mz6ww9ij") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.12.1 (c (n "dharitri-sc-wasm-adapter") (v "0.12.1") (d (list (d (n "dharitri-sc") (r "=0.12.1") (d #t) (k 0)))) (h "181k7mgjv5kdz0z7c21ajvyn1l3n6i6kfwg121z81a11vnm9anha") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.12.2 (c (n "dharitri-sc-wasm-adapter") (v "0.12.2") (d (list (d (n "dharitri-sc") (r "=0.12.2") (d #t) (k 0)))) (h "08hrsblbvii5pa4qs299gfjca8irjcq2haw5y1vl1kp93w5bznpq") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.12.3 (c (n "dharitri-sc-wasm-adapter") (v "0.12.3") (d (list (d (n "dharitri-sc") (r "=0.12.3") (d #t) (k 0)))) (h "1y5b0igsz6k8mlw31ibr8k6a0hazrm2lj4w805m812n0zmz1jniw") (f (quote (("vm-validate-token-identifier") ("vm-dct-local-roles") ("ei-unmanaged-node"))))))

(define-public crate-dharitri-sc-wasm-adapter-0.12.4 (c (n "dharitri-sc-wasm-adapter") (v "0.12.4") (d (list (d (n "dharitri-sc") (r "=0.12.4") (d #t) (k 0)))) (h "0qdnn9yli12zw8hy15611m102p9bkmksmhmhmmfcdmjg1igws2fs")))

(define-public crate-dharitri-sc-wasm-adapter-0.12.5 (c (n "dharitri-sc-wasm-adapter") (v "0.12.5") (d (list (d (n "dharitri-sc") (r "=0.12.5") (d #t) (k 0)))) (h "05yx63agcmxyr038j2n80x8r24m0ivjksr9j4fsg952xz1yhj7cc")))

(define-public crate-dharitri-sc-wasm-adapter-0.12.6 (c (n "dharitri-sc-wasm-adapter") (v "0.12.6") (d (list (d (n "dharitri-sc") (r "=0.12.6") (d #t) (k 0)))) (h "1a86lsnfk9kl4x3k341snph2w6hibcp9iijqjy288p8aihl9sj8z")))

(define-public crate-dharitri-sc-wasm-adapter-0.12.7 (c (n "dharitri-sc-wasm-adapter") (v "0.12.7") (d (list (d (n "dharitri-sc") (r "=0.12.7") (d #t) (k 0)))) (h "1b887ij6scj5w0c4m6pz6xfmjw2if7q4qqiz825736b0cdx6ddmc")))

(define-public crate-dharitri-sc-wasm-adapter-0.12.8 (c (n "dharitri-sc-wasm-adapter") (v "0.12.8") (d (list (d (n "dharitri-sc") (r "=0.12.8") (d #t) (k 0)))) (h "05aw9bdhxh7g36phxqyphxhwqddhv6djij9y58yqh1ylkzv8d7w8")))

(define-public crate-dharitri-sc-wasm-adapter-0.12.9 (c (n "dharitri-sc-wasm-adapter") (v "0.12.9") (d (list (d (n "dharitri-sc") (r "=0.12.9") (d #t) (k 0)))) (h "14ln7zdx080xlip734wbyxf4jqjzfl0niv89i8mlc05h34m83vfm") (y #t)))

(define-public crate-dharitri-sc-wasm-adapter-0.13.0 (c (n "dharitri-sc-wasm-adapter") (v "0.13.0") (d (list (d (n "dharitri-sc") (r "=0.13.0") (d #t) (k 0)))) (h "1fj69aqazi6ms4w66b62b9m45x3yx8qmf4l4fpq28bi9sai3596z")))

(define-public crate-dharitri-sc-wasm-adapter-0.13.1 (c (n "dharitri-sc-wasm-adapter") (v "0.13.1") (d (list (d (n "dharitri-sc") (r "=0.13.1") (d #t) (k 0)))) (h "138aw85p9jy3fv8f26a52wiwx2qlgrw434fkzlnwh416l4zdwd2n")))

(define-public crate-dharitri-sc-wasm-adapter-0.13.2 (c (n "dharitri-sc-wasm-adapter") (v "0.13.2") (d (list (d (n "dharitri-sc") (r "=0.13.2") (d #t) (k 0)))) (h "11qzswr56dxc51alraf71d4hkzcynnsp8g2f621sdz6f2kkdivg9")))

(define-public crate-dharitri-sc-wasm-adapter-0.13.3 (c (n "dharitri-sc-wasm-adapter") (v "0.13.3") (d (list (d (n "dharitri-sc") (r "=0.13.3") (d #t) (k 0)))) (h "0mbysvx9sy3v39nvd5z0rhdgx2s1y1sviz7xvr72b4gd495mc0ak")))

(define-public crate-dharitri-sc-wasm-adapter-0.13.4 (c (n "dharitri-sc-wasm-adapter") (v "0.13.4") (d (list (d (n "dharitri-sc") (r "=0.13.4") (d #t) (k 0)))) (h "0ns0dzpd4vxrfsqlylv28wbm1s3y50d8j2bxgyhfgn5pv4ga136j") (y #t)))

(define-public crate-dharitri-sc-wasm-adapter-0.13.5 (c (n "dharitri-sc-wasm-adapter") (v "0.13.5") (d (list (d (n "dharitri-sc") (r "=0.13.5") (d #t) (k 0)))) (h "1iwk15zg782y501072388gz0rhsar1jh3vssa4qk10y92c4hp7cp") (y #t)))

(define-public crate-dharitri-sc-wasm-adapter-0.13.6 (c (n "dharitri-sc-wasm-adapter") (v "0.13.6") (d (list (d (n "dharitri-sc") (r "=0.13.6") (d #t) (k 0)))) (h "0vqz1kzr1k3hzbw0z3va6j5mrnyi29jh8ha9ri1f7ibv6lsvavlh") (y #t)))

(define-public crate-dharitri-sc-wasm-adapter-0.13.7 (c (n "dharitri-sc-wasm-adapter") (v "0.13.7") (d (list (d (n "dharitri-sc") (r "=0.13.7") (d #t) (k 0)))) (h "0sidhdbki4kjz07y76g0k96ghc0j3hn4krbrsjqj1w4znpg9iraz") (y #t)))

(define-public crate-dharitri-sc-wasm-adapter-0.13.8 (c (n "dharitri-sc-wasm-adapter") (v "0.13.8") (d (list (d (n "dharitri-sc") (r "=0.13.8") (d #t) (k 0)))) (h "06ybjlf918n18if1x1jk8gc9kbgmgmyr5qq1mr69pszza5w5msj3") (y #t)))

(define-public crate-dharitri-sc-wasm-adapter-0.13.9 (c (n "dharitri-sc-wasm-adapter") (v "0.13.9") (d (list (d (n "dharitri-sc") (r "=0.13.9") (d #t) (k 0)))) (h "1z7z8n47f78fa7iqksgkv0qfsb3ywbhamvz9nj78x6sm36dygy9b") (y #t)))

(define-public crate-dharitri-sc-wasm-adapter-0.14.0 (c (n "dharitri-sc-wasm-adapter") (v "0.14.0") (d (list (d (n "dharitri-sc") (r "=0.14.0") (d #t) (k 0)))) (h "00qxa9vy6an6xd1v5zv0f3gwywwj67mwqln96lgh2678s5i1ff1d") (y #t)))

(define-public crate-dharitri-sc-wasm-adapter-0.14.1 (c (n "dharitri-sc-wasm-adapter") (v "0.14.1") (d (list (d (n "dharitri-sc") (r "=0.14.1") (d #t) (k 0)))) (h "0g8yv8h40nznb5lp4dqpkhr9b24436ffaibhkf2mglwjcxyfavnw") (y #t)))

