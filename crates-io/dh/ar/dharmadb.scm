(define-module (crates-io dh ar dharmadb) #:use-module (crates-io))

(define-public crate-dharmadb-0.1.0 (c (n "dharmadb") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "buffered_offset_reader") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "subway") (r "^0.1.2") (d #t) (k 0)))) (h "0sa6dcfjzrl2hm439nf5xy5cahcgsm18pi70wbwwqvpq0l71gsiv") (y #t)))

(define-public crate-dharmadb-0.1.1 (c (n "dharmadb") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "buffered_offset_reader") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "subway") (r "^0.1.2") (d #t) (k 0)))) (h "1sgxhp0aqsf7i5d0wwmii2zs0nn2d10h5r5xddadwrlq2af3z6xn")))

