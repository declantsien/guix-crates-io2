(define-module (crates-io dh ar dharitri-sc-wmoax-swap) #:use-module (crates-io))

(define-public crate-dharitri-sc-wmoax-swap-0.5.3 (c (n "dharitri-sc-wmoax-swap") (v "0.5.3") (d (list (d (n "dharitri-wasm") (r "^0.5.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.3") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.5.3") (d #t) (k 0)))) (h "01h22r5fnck273016n33xmw9m6614bshxmkxphhvy8ljgfndhwh9")))

(define-public crate-dharitri-sc-wmoax-swap-0.10.4 (c (n "dharitri-sc-wmoax-swap") (v "0.10.4") (d (list (d (n "dharitri-wasm") (r "^0.10.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.10.4") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.10.4") (d #t) (k 0)))) (h "0by4s20k371fpz1arnvnpl21zpihyx1ndp4vf40cwws61gr651aw")))

(define-public crate-dharitri-sc-wmoax-swap-0.10.5 (c (n "dharitri-sc-wmoax-swap") (v "0.10.5") (d (list (d (n "dharitri-wasm") (r "^0.10.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.10.5") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.10.5") (d #t) (k 0)))) (h "0gq8lsqgk0z31a9z7cgwy9np0fwsjwr3lmzwhaaqm8ykdc19q830")))

(define-public crate-dharitri-sc-wmoax-swap-0.10.6 (c (n "dharitri-sc-wmoax-swap") (v "0.10.6") (d (list (d (n "dharitri-wasm") (r "^0.10.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.10.6") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.10.6") (d #t) (k 0)))) (h "1h8kj6clba3d08agpcrmrs54kghwzr1rb6qgcgg6k9imicb29syb")))

(define-public crate-dharitri-sc-wmoax-swap-0.10.7 (c (n "dharitri-sc-wmoax-swap") (v "0.10.7") (d (list (d (n "dharitri-wasm") (r "^0.10.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.10.7") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.10.7") (d #t) (k 0)))) (h "09jpgsc97clli0277jy96az8ln8dhqb1gdwmkkp7hmbj67xnjvmc")))

(define-public crate-dharitri-sc-wmoax-swap-0.10.8 (c (n "dharitri-sc-wmoax-swap") (v "0.10.8") (d (list (d (n "dharitri-wasm") (r "^0.10.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.10.8") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.10.8") (d #t) (k 0)))) (h "1ycav6xc29bzym2kn5fm6cmfnjfpwkx9xrxa3p2hy0x8ln2x5q4v")))

