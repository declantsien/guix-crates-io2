(define-module (crates-io dh ar dharitri-wasm-module-pause) #:use-module (crates-io))

(define-public crate-dharitri-wasm-module-pause-0.0.2 (c (n "dharitri-wasm-module-pause") (v "0.0.2") (d (list (d (n "dharitri-wasm") (r "^0.0.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.2") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.2") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "14q0dry8x9v47n139m2flb495l7fqgqnhbvw72m3pwxzmzdpzvlq") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.0.3 (c (n "dharitri-wasm-module-pause") (v "0.0.3") (d (list (d (n "dharitri-wasm") (r "^0.0.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.3") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.3") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.3") (o #t) (d #t) (k 0)))) (h "0rm989yzdjy7flpqrhzxb55bqf0svj3n6g1q588cbk65m66r2lsb") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.0.4 (c (n "dharitri-wasm-module-pause") (v "0.0.4") (d (list (d (n "dharitri-wasm") (r "^0.0.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.4") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.4") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "0xrfmsqgz5rmw2y0m87k33wfxbmmvzgwb1m0lz7wpzdm0rzdsmpp") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.0.5 (c (n "dharitri-wasm-module-pause") (v "0.0.5") (d (list (d (n "dharitri-wasm") (r "^0.0.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.5") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.5") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.5") (o #t) (d #t) (k 0)))) (h "0xazc7zbk8dlay01s1jhmvvsx0v0hh1iyxpv45wsdadwywcwgi2w") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.0.6 (c (n "dharitri-wasm-module-pause") (v "0.0.6") (d (list (d (n "dharitri-wasm") (r "^0.0.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.6") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.6") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.6") (o #t) (d #t) (k 0)))) (h "0g320g74an373w38j3ghqv4x0a1zcsmzsvsb4cw2vvsk18vypxi2") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.0.7 (c (n "dharitri-wasm-module-pause") (v "0.0.7") (d (list (d (n "dharitri-wasm") (r "^0.0.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.7") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.7") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.7") (o #t) (d #t) (k 0)))) (h "0xncsc3z14d2fwr4q3yr2c9ahg5x6n0jvizyiqqi680cqssbvhlk") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.0.8 (c (n "dharitri-wasm-module-pause") (v "0.0.8") (d (list (d (n "dharitri-wasm") (r "^0.0.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.8") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.8") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.8") (o #t) (d #t) (k 0)))) (h "1wy2akxlx20ynfpjbpd6zgmqzkffb92apbsk71g93ihna4nmdb63") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.0.9 (c (n "dharitri-wasm-module-pause") (v "0.0.9") (d (list (d (n "dharitri-wasm") (r "^0.0.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.9") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.9") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.9") (o #t) (d #t) (k 0)))) (h "0261m7v3wdcn8gipp7x9cncrwmawiiyggdganvldripnb6d36l6v") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.1.0 (c (n "dharitri-wasm-module-pause") (v "0.1.0") (d (list (d (n "dharitri-wasm") (r "^0.1.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.0") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1jm81p0695a3q658g2am9ip4lav1pyz94l37dvmfr5q7zihv3fb8") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.1.1 (c (n "dharitri-wasm-module-pause") (v "0.1.1") (d (list (d (n "dharitri-wasm") (r "^0.1.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.1") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "06zcirwr3habrlfs0r8psc6fp9plmzd8clx42pa7anxwbgsnfs6k") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.1.2 (c (n "dharitri-wasm-module-pause") (v "0.1.2") (d (list (d (n "dharitri-wasm") (r "^0.1.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.2") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1nbg3snismayyz42b4y4g2p5xgs1qgbnjl91qazl3la5gg216jgx") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.1.3 (c (n "dharitri-wasm-module-pause") (v "0.1.3") (d (list (d (n "dharitri-wasm") (r "^0.1.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.3") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1id4v70cslyzinwwlxps5d975mln9j3gg9rp8armwhiak1c7aqs4") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.1.4 (c (n "dharitri-wasm-module-pause") (v "0.1.4") (d (list (d (n "dharitri-wasm") (r "^0.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.4") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1vcyhicg1y3gx73gkbkg2hl872cfsqyckn5jfkkmk1qnhxg9dqxr") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.1.5 (c (n "dharitri-wasm-module-pause") (v "0.1.5") (d (list (d (n "dharitri-wasm") (r "^0.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.5") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0s1syw4ll29m36z7blb9y05iw32j0p63jnar25nx0mn6nqdz9yxj") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.1.6 (c (n "dharitri-wasm-module-pause") (v "0.1.6") (d (list (d (n "dharitri-wasm") (r "^0.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.6") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "1ys55iwr4y34zhab4x99n3d9wc21v71xszaa23dlkp6222i7l0h4") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.1.7 (c (n "dharitri-wasm-module-pause") (v "0.1.7") (d (list (d (n "dharitri-wasm") (r "^0.1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.7") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "19vxwncfhxil5d7l9zmn27a2zszgn0rm8qbn6bf7qkfy4s2m95bl") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.1.8 (c (n "dharitri-wasm-module-pause") (v "0.1.8") (d (list (d (n "dharitri-wasm") (r "^0.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.8") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "06xywdv9p7azbrhgw8qabvs7k9hwnmf0bvwscf03iybchdhv39k1") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.1.9 (c (n "dharitri-wasm-module-pause") (v "0.1.9") (d (list (d (n "dharitri-wasm") (r "^0.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.9") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "091y70xlnxwi370m4kqq74pk3cgkm4g9ii8m35wgpwgrffrvmya9") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.2.0 (c (n "dharitri-wasm-module-pause") (v "0.2.0") (d (list (d (n "dharitri-wasm") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.0") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "04lmvw1kar0cln19cdz9iy1y3wgvf5mls1181vz2y64ss4hld3f4") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.2.1 (c (n "dharitri-wasm-module-pause") (v "0.2.1") (d (list (d (n "dharitri-wasm") (r "^0.2.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.1") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0ra7yl2nbkayl0l1q6j6a2ys8df7jjiz807sv0bsf5qfd7rvvqpn") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.2.2 (c (n "dharitri-wasm-module-pause") (v "0.2.2") (d (list (d (n "dharitri-wasm") (r "^0.2.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.2") (d #t) (k 2)))) (h "0q9g7lk812zsgmqs8hb7va5a7zfq7zpfq4kmhhfrbyfl5lf54kzv")))

(define-public crate-dharitri-wasm-module-pause-0.2.3 (c (n "dharitri-wasm-module-pause") (v "0.2.3") (d (list (d (n "dharitri-wasm") (r "^0.2.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.3") (d #t) (k 2)))) (h "0c70g72jj6s8xl17vysry1mx5q6hqzx82rmnvvh2wq4xrj78f9k3")))

(define-public crate-dharitri-wasm-module-pause-0.2.4 (c (n "dharitri-wasm-module-pause") (v "0.2.4") (d (list (d (n "dharitri-wasm") (r "^0.2.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.4") (d #t) (k 2)))) (h "1l7ldp93rvfmh09s42xbbf27xi9s8dlvna7mq2q6h7vcj2ny8qq7")))

(define-public crate-dharitri-wasm-module-pause-0.2.5 (c (n "dharitri-wasm-module-pause") (v "0.2.5") (d (list (d (n "dharitri-wasm") (r "^0.2.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.5") (d #t) (k 2)))) (h "030pwdjair632ls94ayg5wgc5cva6qvrm7w44fcymyccr34frg30")))

(define-public crate-dharitri-wasm-module-pause-0.2.6 (c (n "dharitri-wasm-module-pause") (v "0.2.6") (d (list (d (n "dharitri-wasm") (r "^0.2.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.6") (d #t) (k 2)))) (h "11blzmsp51bvb0w4ilcrxgkqxrcl5z5h7cqrsfiwr9yr37myb36w")))

(define-public crate-dharitri-wasm-module-pause-0.2.8 (c (n "dharitri-wasm-module-pause") (v "0.2.8") (d (list (d (n "dharitri-wasm") (r "^0.2.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.8") (d #t) (k 2)))) (h "0j2qzn9b317527cqfxpzd842qlagl53lyj3nbv1xlhdrv4fs3hbv")))

(define-public crate-dharitri-wasm-module-pause-0.2.9 (c (n "dharitri-wasm-module-pause") (v "0.2.9") (d (list (d (n "dharitri-wasm") (r "^0.2.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.9") (d #t) (k 2)))) (h "036vynwh3bbm6pk1pcf8s6adwkflvhqhg0mpdll5sw9gi8m3gifg")))

(define-public crate-dharitri-wasm-module-pause-0.3.0 (c (n "dharitri-wasm-module-pause") (v "0.3.0") (d (list (d (n "dharitri-wasm") (r "^0.3.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.0") (d #t) (k 2)))) (h "1qwd208mf4cbzfx9ifbmjjk5rmvsgq359r8lszjjm72japnjfvyh")))

(define-public crate-dharitri-wasm-module-pause-0.3.1 (c (n "dharitri-wasm-module-pause") (v "0.3.1") (d (list (d (n "dharitri-wasm") (r "^0.3.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.1") (d #t) (k 2)))) (h "1pqcxg1szlwmv4y30dxvj7cqmv20zj85a22jamciwbbwn0smp6jv")))

(define-public crate-dharitri-wasm-module-pause-0.3.2 (c (n "dharitri-wasm-module-pause") (v "0.3.2") (d (list (d (n "dharitri-wasm") (r "^0.3.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.2") (d #t) (k 2)))) (h "1l94cj5n0phankam5rq80qc3d5v4vwycbjk1b5jqpfniwg6c1csx")))

(define-public crate-dharitri-wasm-module-pause-0.3.3 (c (n "dharitri-wasm-module-pause") (v "0.3.3") (d (list (d (n "dharitri-wasm") (r "^0.3.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.3") (d #t) (k 2)))) (h "1ycfi58x1vljapczag3sma4c64qfs6k6idp15s9bp4cvq31qczbp")))

(define-public crate-dharitri-wasm-module-pause-0.3.4 (c (n "dharitri-wasm-module-pause") (v "0.3.4") (d (list (d (n "dharitri-wasm") (r "^0.3.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.4") (d #t) (k 2)))) (h "14ws0x6kxy6wy0py0db95igg69d9a5w4srhv3qs2ncrj4xhj5wiy")))

(define-public crate-dharitri-wasm-module-pause-0.3.5 (c (n "dharitri-wasm-module-pause") (v "0.3.5") (d (list (d (n "dharitri-wasm") (r "^0.3.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.5") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.5") (d #t) (k 0)))) (h "08zz85clbp0q9kki7v7lj8rj8hwrc23gzy8w7gs123b91hjlw66r")))

(define-public crate-dharitri-wasm-module-pause-0.3.6 (c (n "dharitri-wasm-module-pause") (v "0.3.6") (d (list (d (n "dharitri-wasm") (r "^0.3.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.6") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.6") (d #t) (k 0)))) (h "08sflrqlqg1hzljh9vx8dxllyi87zy78sjs9d9jiwlw6wwnrmsc1")))

(define-public crate-dharitri-wasm-module-pause-0.3.7 (c (n "dharitri-wasm-module-pause") (v "0.3.7") (d (list (d (n "dharitri-wasm") (r "^0.3.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.7") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.7") (d #t) (k 0)))) (h "1appi8rjwg1qdz0w03yqbg4inr7can75nvwg8k56hwlgv18iphdm")))

(define-public crate-dharitri-wasm-module-pause-0.3.8 (c (n "dharitri-wasm-module-pause") (v "0.3.8") (d (list (d (n "dharitri-wasm") (r "^0.3.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.8") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.8") (d #t) (k 0)))) (h "16r6skcz79f3005q005i20bmjrcn9jq30kkf78lgq4zbk8m7csgl")))

(define-public crate-dharitri-wasm-module-pause-0.3.9 (c (n "dharitri-wasm-module-pause") (v "0.3.9") (d (list (d (n "dharitri-wasm") (r "^0.3.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.9") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.9") (d #t) (k 0)))) (h "0i15v02mr8xym3a7s2fj8n6zgc55p2y8s60w0rvlmrvjbhzgh8gz")))

(define-public crate-dharitri-wasm-module-pause-0.4.0 (c (n "dharitri-wasm-module-pause") (v "0.4.0") (d (list (d (n "dharitri-wasm") (r "^0.4.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.4.0") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.4.0") (d #t) (k 0)))) (h "1nhdvvgiba0kysrpyw8zgck62bzg8xyv3cq3792qma897nl5zwrr")))

(define-public crate-dharitri-wasm-module-pause-0.4.1 (c (n "dharitri-wasm-module-pause") (v "0.4.1") (d (list (d (n "dharitri-wasm") (r "^0.4.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.4.1") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.4.1") (d #t) (k 0)))) (h "0gb7n48n9zw4ifibphb8l2dniz2n3gvbq9wcngrc82crg0b9w18c")))

(define-public crate-dharitri-wasm-module-pause-0.4.2 (c (n "dharitri-wasm-module-pause") (v "0.4.2") (d (list (d (n "dharitri-wasm") (r "^0.4.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.4.2") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.4.2") (d #t) (k 0)))) (h "0dzyziwcz4lsbv0hwpq30v48xzcr66qdaybl5jc7jg7hmprxqdng")))

(define-public crate-dharitri-wasm-module-pause-0.5.5 (c (n "dharitri-wasm-module-pause") (v "0.5.5") (d (list (d (n "dharitri-wasm") (r "^0.5.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.5") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.5") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.5") (o #t) (d #t) (k 0)))) (h "1gk9zb1isizfk1ql0p86q3pb42bqg259p25lzidp8qfl1y3kj666") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.5.6 (c (n "dharitri-wasm-module-pause") (v "0.5.6") (d (list (d (n "dharitri-wasm") (r "^0.5.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.6") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.6") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "06dh6113xh4irrwf857nm1qd64i2icc8l7jpvzzzxsk8q0f97s0b") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.5.7 (c (n "dharitri-wasm-module-pause") (v "0.5.7") (d (list (d (n "dharitri-wasm") (r "^0.5.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.7") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.7") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.7") (o #t) (d #t) (k 0)))) (h "05hpk4i60k2jsmbk465gj8slfv1whrwxif5l47w3i2pqjbxky4bd") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.5.8 (c (n "dharitri-wasm-module-pause") (v "0.5.8") (d (list (d (n "dharitri-wasm") (r "^0.5.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.8") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.8") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.8") (o #t) (d #t) (k 0)))) (h "0dnl9vng5knsjmpwfn6ckbyq4zkhmb3wq2nq4pkszyyzglscn8ag") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.5.9 (c (n "dharitri-wasm-module-pause") (v "0.5.9") (d (list (d (n "dharitri-wasm") (r "^0.5.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.9") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.9") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "0vg0ij6nhxxfnb2nny02l3acpdawnjy9znppcfxd6zb0mp67pbbm") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.6.0 (c (n "dharitri-wasm-module-pause") (v "0.6.0") (d (list (d (n "dharitri-wasm") (r "^0.6.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.0") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0g1blzfkjka399f6b4r4dr3hxx9117h032k64pn401vaznnqx86r") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.6.1 (c (n "dharitri-wasm-module-pause") (v "0.6.1") (d (list (d (n "dharitri-wasm") (r "^0.6.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.1") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.6.1") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "1x2ar2gmsp0ng2fll1v08k2m29n0izh8yniznvgmii4f8z7q61sg") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.6.2 (c (n "dharitri-wasm-module-pause") (v "0.6.2") (d (list (d (n "dharitri-wasm") (r "^0.6.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.2") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.6.2") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "1f9jfb645r05q4aqdvxvpbz6q23dv0skkz88za8xnhd0kjdzi1hj") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.6.3 (c (n "dharitri-wasm-module-pause") (v "0.6.3") (d (list (d (n "dharitri-wasm") (r "^0.6.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.3") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.6.3") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "1gvg5lh6vihis5cgdzqm1ny0kzjfcfrvhch5zm0gs4mmbb216r3b") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.6.4 (c (n "dharitri-wasm-module-pause") (v "0.6.4") (d (list (d (n "dharitri-wasm") (r "^0.6.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.4") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "0ag4xnh199gk1kr313q32bz1z0qj5vmxk31a5w594pvjg84crkxf") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.6.6 (c (n "dharitri-wasm-module-pause") (v "0.6.6") (d (list (d (n "dharitri-wasm") (r "^0.6.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.6") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.6") (o #t) (d #t) (k 0)))) (h "0cnsl26dc6m55kq5mifbia5dachmpdsk0blg7067pfg7wzzqmbm8") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.6.7 (c (n "dharitri-wasm-module-pause") (v "0.6.7") (d (list (d (n "dharitri-wasm") (r "^0.6.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.7") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "02r0vlyl8sa224rw2h96gx9m8dkrwmxyqm5gs2f7zs7p466rv1r3") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.6.8 (c (n "dharitri-wasm-module-pause") (v "0.6.8") (d (list (d (n "dharitri-wasm") (r "^0.6.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.8") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.8") (o #t) (d #t) (k 0)))) (h "1xgyrqssagicfafci493231a7ap8bjj4d1fd7bvjs9147qpvqsdl") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.6.9 (c (n "dharitri-wasm-module-pause") (v "0.6.9") (d (list (d (n "dharitri-wasm") (r "^0.6.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.9") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.9") (o #t) (d #t) (k 0)))) (h "086fvdrfpziswvi17nxn5638aifb284fpj1rm9xjw7sdhmkxqhms") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.7.0 (c (n "dharitri-wasm-module-pause") (v "0.7.0") (d (list (d (n "dharitri-wasm") (r "^0.7.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.0") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "14jnv9wv07ygk7pz9zk2xm97ac4d251a88xw9nqzc23k8rhg0bb8") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.7.1 (c (n "dharitri-wasm-module-pause") (v "0.7.1") (d (list (d (n "dharitri-wasm") (r "^0.7.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.1") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "1q9nkccsqhx4hgpgmr0a11mrwqsdqm1dgss84sm09kb2adykm9wb") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-pause-0.7.2 (c (n "dharitri-wasm-module-pause") (v "0.7.2") (d (list (d (n "dharitri-wasm") (r "^0.7.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.2") (d #t) (k 2)))) (h "0r4qswc6myx5mbkhfvkwkqa2c463vxybvnqg7hp69lja0z579qv3")))

(define-public crate-dharitri-wasm-module-pause-0.7.3 (c (n "dharitri-wasm-module-pause") (v "0.7.3") (d (list (d (n "dharitri-wasm") (r "^0.7.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.3") (d #t) (k 2)))) (h "1a5wgbpll7j7cd0pp1c0xwv31wpg1qp6cq9iw17rf6h054w6fa6d")))

(define-public crate-dharitri-wasm-module-pause-0.7.4 (c (n "dharitri-wasm-module-pause") (v "0.7.4") (d (list (d (n "dharitri-wasm") (r "^0.7.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.4") (d #t) (k 2)))) (h "067gl8kih03gvkwl1cd797bb65a7rm09lyqrn2a7310hy25dp1b5")))

(define-public crate-dharitri-wasm-module-pause-0.7.5 (c (n "dharitri-wasm-module-pause") (v "0.7.5") (d (list (d (n "dharitri-wasm") (r "^0.7.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.5") (d #t) (k 2)))) (h "0mdfqj47c9h813yv5zqgxs2q327ky7hj5ycd8z63n5xa7mp73001")))

(define-public crate-dharitri-wasm-module-pause-0.7.6 (c (n "dharitri-wasm-module-pause") (v "0.7.6") (d (list (d (n "dharitri-wasm") (r "^0.7.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.6") (d #t) (k 2)))) (h "1hnv1xh1ynhjyz0id6sawriqy8zpiwsjdsj7gqmh9fhj86hh78pk")))

(define-public crate-dharitri-wasm-module-pause-0.7.7 (c (n "dharitri-wasm-module-pause") (v "0.7.7") (d (list (d (n "dharitri-wasm") (r "^0.7.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.7") (d #t) (k 2)))) (h "1ja6zwr1szwqr29imnk648ahqgxrkasx5dgcnff0xd73w4mychph")))

(define-public crate-dharitri-wasm-module-pause-0.7.8 (c (n "dharitri-wasm-module-pause") (v "0.7.8") (d (list (d (n "dharitri-wasm") (r "^0.7.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.8") (d #t) (k 2)))) (h "1vabh5x2whwy5g7pi14zsa6sdbicwhj6phbv2fc81591zpppnzfx")))

(define-public crate-dharitri-wasm-module-pause-0.7.9 (c (n "dharitri-wasm-module-pause") (v "0.7.9") (d (list (d (n "dharitri-wasm") (r "^0.7.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.9") (d #t) (k 2)))) (h "00c9ahd4zk8m2k3d9c2b66y0maw080rns16bsjs1j2ic2bpcmcm3")))

(define-public crate-dharitri-wasm-module-pause-0.8.0 (c (n "dharitri-wasm-module-pause") (v "0.8.0") (d (list (d (n "dharitri-wasm") (r "^0.8.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.0") (d #t) (k 2)))) (h "13symqm2abjxhp6lgm4zgj02q5g9kvfdsvzfifqjvmbc6b4s9zr4")))

(define-public crate-dharitri-wasm-module-pause-0.8.1 (c (n "dharitri-wasm-module-pause") (v "0.8.1") (d (list (d (n "dharitri-wasm") (r "^0.8.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.1") (d #t) (k 2)))) (h "088k273z5gy4pxczl01gfdv9aw68bbkzkdgnrzs7rkgsyina3jxy")))

(define-public crate-dharitri-wasm-module-pause-0.8.2 (c (n "dharitri-wasm-module-pause") (v "0.8.2") (d (list (d (n "dharitri-wasm") (r "^0.8.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.2") (d #t) (k 2)))) (h "0l64zdfbpgcbp0qqgwadb7mnv1l1fhv8rfgpijm4sp7yr6wcavlg")))

(define-public crate-dharitri-wasm-module-pause-0.8.3 (c (n "dharitri-wasm-module-pause") (v "0.8.3") (d (list (d (n "dharitri-wasm") (r "^0.8.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.3") (d #t) (k 2)))) (h "1x68cvlj89s6n0asscizlwd2dni4qf71wg0wzcd8vz80nlyvv1hv")))

(define-public crate-dharitri-wasm-module-pause-0.8.4 (c (n "dharitri-wasm-module-pause") (v "0.8.4") (d (list (d (n "dharitri-wasm") (r "^0.8.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.4") (d #t) (k 2)))) (h "0y6i5771kvsh9kqhzw3znbzbcvwr1nkfcxcnrcmw66xrmn5ccq6a")))

(define-public crate-dharitri-wasm-module-pause-0.8.5 (c (n "dharitri-wasm-module-pause") (v "0.8.5") (d (list (d (n "dharitri-wasm") (r "^0.8.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.5") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.5") (d #t) (k 0)))) (h "0ix8pkjxqaa7k5ipikbc7fvypwfxggs1xdp99f92ka3y2pcabmsx")))

(define-public crate-dharitri-wasm-module-pause-0.8.6 (c (n "dharitri-wasm-module-pause") (v "0.8.6") (d (list (d (n "dharitri-wasm") (r "^0.8.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.6") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.6") (d #t) (k 0)))) (h "15irba1s8x52dpbsmnncrwxi3g4czxfq24j74m326k3289fzjhcc")))

(define-public crate-dharitri-wasm-module-pause-0.8.7 (c (n "dharitri-wasm-module-pause") (v "0.8.7") (d (list (d (n "dharitri-wasm") (r "^0.8.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.7") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.7") (d #t) (k 0)))) (h "090m84j0la0zdkyn0mwqfd3nhfvy2pnfxnsyv7mgnyb7gk2397da")))

(define-public crate-dharitri-wasm-module-pause-0.8.8 (c (n "dharitri-wasm-module-pause") (v "0.8.8") (d (list (d (n "dharitri-wasm") (r "^0.8.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.8") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.8") (d #t) (k 0)))) (h "0ba332fkscwsx3shx1k44nnblz2zjvq6nhv6yacsamni6yzxna3z")))

(define-public crate-dharitri-wasm-module-pause-0.8.9 (c (n "dharitri-wasm-module-pause") (v "0.8.9") (d (list (d (n "dharitri-wasm") (r "^0.8.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.9") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.9") (d #t) (k 0)))) (h "06hqrqjsjph356v1x4kgjmxcyb0j56kviiv45j2j6lzni75qlcdf")))

(define-public crate-dharitri-wasm-module-pause-0.9.0 (c (n "dharitri-wasm-module-pause") (v "0.9.0") (d (list (d (n "dharitri-wasm") (r "^0.9.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.9.0") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.9.0") (d #t) (k 0)))) (h "06367nzwnsw7q92njbsn13xgq5fib0jiyy9l1afrn1kxn60vr5fm")))

(define-public crate-dharitri-wasm-module-pause-0.9.1 (c (n "dharitri-wasm-module-pause") (v "0.9.1") (d (list (d (n "dharitri-wasm") (r "^0.9.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.9.1") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.9.1") (d #t) (k 0)))) (h "0daqlndvx9cpzhlgcizaf5acy2g21p1lqkls07ym04k7rgsiq0r9")))

(define-public crate-dharitri-wasm-module-pause-0.9.2 (c (n "dharitri-wasm-module-pause") (v "0.9.2") (d (list (d (n "dharitri-wasm") (r "^0.9.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.9.2") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.9.2") (d #t) (k 0)))) (h "125jfyg8l76p66spwjbkyyyy9lvha3p05lglpfmqs05bxh7zidb1")))

