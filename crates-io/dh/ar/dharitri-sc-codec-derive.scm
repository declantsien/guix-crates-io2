(define-module (crates-io dh ar dharitri-sc-codec-derive) #:use-module (crates-io))

(define-public crate-dharitri-sc-codec-derive-0.0.1 (c (n "dharitri-sc-codec-derive") (v "0.0.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12gfl4aw8yj0x9cj8s0mg73k9b21a7901p5ndx2w1inps60v6l61") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-sc-codec-derive-0.0.2 (c (n "dharitri-sc-codec-derive") (v "0.0.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nh6x35s0g6nhwqcxsmid0m79757m221ml51m20ydb875f0g7i9x") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-sc-codec-derive-0.0.3 (c (n "dharitri-sc-codec-derive") (v "0.0.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14h3s07rhfl7sbjd0i4dkr2s2wfkwinrx7jniwnrm5w2c4d18aw6") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-sc-codec-derive-0.0.4 (c (n "dharitri-sc-codec-derive") (v "0.0.4") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jvaxii8vv1xajpf28mn0wkj3a3wr0728djcgyf2pn4pq0mnz2z9") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-sc-codec-derive-0.0.5 (c (n "dharitri-sc-codec-derive") (v "0.0.5") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zx2paa6fkrcvbg5mwa8jyswaymxghm1vi6rmgk0l37k9wqf995i") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-sc-codec-derive-0.2.0 (c (n "dharitri-sc-codec-derive") (v "0.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rsniib8lfzz6q1pgsfvpcyxrq6m9mlqjrnr1gwj8acc3r1jab93") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-sc-codec-derive-0.4.2 (c (n "dharitri-sc-codec-derive") (v "0.4.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yp9bh9vgll481xj6h93p1yj0p1q03irhm4vbqh3rvy6y7nv7s72") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-sc-codec-derive-0.4.3 (c (n "dharitri-sc-codec-derive") (v "0.4.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cqvizxd5arygyb0yp1640wakfigsxzdpzvrz10xrmfszp7mi8c6") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-sc-codec-derive-0.4.4 (c (n "dharitri-sc-codec-derive") (v "0.4.4") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x8fkasdc6ghgkhzni1aqdfpbknh90qpl06habqxqy559sq1z1rx") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits")))) (y #t)))

(define-public crate-dharitri-sc-codec-derive-0.4.5 (c (n "dharitri-sc-codec-derive") (v "0.4.5") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ybkhj7504zcq60bpx84xrb44jifqni1xvyq5x88bdn9sjzqc79f") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits")))) (y #t)))

(define-public crate-dharitri-sc-codec-derive-0.4.6 (c (n "dharitri-sc-codec-derive") (v "0.4.6") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g692yaqj9h1imkiy7nix49n9y2jpz23v3k35pgdkpl1vc98vc2l") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits")))) (y #t)))

(define-public crate-dharitri-sc-codec-derive-0.4.7 (c (n "dharitri-sc-codec-derive") (v "0.4.7") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kn8gyxyi8nvfv5vj5hzg6br2fccix98zys63ax6qvqg8x9mwgi1") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

(define-public crate-dharitri-sc-codec-derive-0.4.8 (c (n "dharitri-sc-codec-derive") (v "0.4.8") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0qc1gyqm0k7kws1pi6dav8wvc6n0zs0z0cd7g4fsc2k7h51qbzqh") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits")))) (y #t)))

(define-public crate-dharitri-sc-codec-derive-0.4.9 (c (n "dharitri-sc-codec-derive") (v "0.4.9") (d (list (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.76") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (d #t) (k 0)))) (h "0ixnmykiam28i140k1jvb5kx10qyvjcfbczihfsnlbfm4qhkak78") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits")))) (y #t)))

(define-public crate-dharitri-sc-codec-derive-0.5.0 (c (n "dharitri-sc-codec-derive") (v "0.5.0") (d (list (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.78") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (d #t) (k 0)))) (h "164d786k50y3ivvax3dw0hbfgpzxa0svcsr5m4fr795p54jv77gn") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits")))) (y #t)))

(define-public crate-dharitri-sc-codec-derive-0.5.1 (c (n "dharitri-sc-codec-derive") (v "0.5.1") (d (list (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.78") (d #t) (k 0)) (d (n "quote") (r "=1.0.35") (d #t) (k 0)) (d (n "syn") (r "=2.0.48") (d #t) (k 0)))) (h "1fyc649dgl58d89grylkpih0i3jla6n3dbzrkpky60ga9g9jn894") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits")))) (y #t)))

