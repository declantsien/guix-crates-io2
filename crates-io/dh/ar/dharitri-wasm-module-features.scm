(define-module (crates-io dh ar dharitri-wasm-module-features) #:use-module (crates-io))

(define-public crate-dharitri-wasm-module-features-0.0.2 (c (n "dharitri-wasm-module-features") (v "0.0.2") (d (list (d (n "dharitri-wasm") (r "^0.0.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.2") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.2") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "1yyvidhy0k5677r5v0i6lzv0b27himjxz8gvz7759fpxa5sf2hyb") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.0.3 (c (n "dharitri-wasm-module-features") (v "0.0.3") (d (list (d (n "dharitri-wasm") (r "^0.0.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.3") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.3") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.3") (o #t) (d #t) (k 0)))) (h "12324a74rv77q3vbv9lj7y9krdpsqbhksjkb69rajg4vp77742i6") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.0.4 (c (n "dharitri-wasm-module-features") (v "0.0.4") (d (list (d (n "dharitri-wasm") (r "^0.0.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.4") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.4") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "0sy792hbs9scixbycdqj9vwwlbky92li1g0vy3xx6a80zp6c8z3q") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.0.5 (c (n "dharitri-wasm-module-features") (v "0.0.5") (d (list (d (n "dharitri-wasm") (r "^0.0.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.5") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.5") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.5") (o #t) (d #t) (k 0)))) (h "0ac98qr00s5kjfq5mvrfsb7brzq4ivdb8ppga40smcmxlfhymr40") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.0.6 (c (n "dharitri-wasm-module-features") (v "0.0.6") (d (list (d (n "dharitri-wasm") (r "^0.0.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.6") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.6") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.6") (o #t) (d #t) (k 0)))) (h "1xcy6fbgr80s2cidlpjhj4ljiv6f6p7zsamyrna6zzij2hvq1d8b") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.0.7 (c (n "dharitri-wasm-module-features") (v "0.0.7") (d (list (d (n "dharitri-wasm") (r "^0.0.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.7") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.7") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.7") (o #t) (d #t) (k 0)))) (h "05msg1gnavapvaqyqdkc90548fzl8hj2cpzk2hl3prkd4c3mykn1") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.0.8 (c (n "dharitri-wasm-module-features") (v "0.0.8") (d (list (d (n "dharitri-wasm") (r "^0.0.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.8") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.8") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.8") (o #t) (d #t) (k 0)))) (h "0py7v5lpjbdkazqg61bdkjf3xsvpgkpr2avkys9n8vvdvki77xgl") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.0.9 (c (n "dharitri-wasm-module-features") (v "0.0.9") (d (list (d (n "dharitri-wasm") (r "^0.0.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.0.9") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.0.9") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.0.9") (o #t) (d #t) (k 0)))) (h "1qmp6fb183d199b9a2wf6gwhivi9g73i5ws0hh8l65x9wiiqzmn7") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.1.0 (c (n "dharitri-wasm-module-features") (v "0.1.0") (d (list (d (n "dharitri-wasm") (r "^0.1.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.0") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0hxw397w90wl4yjaljlb76i4v5syg0l7z4hqidlg9q6v0w38n66f") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.1.1 (c (n "dharitri-wasm-module-features") (v "0.1.1") (d (list (d (n "dharitri-wasm") (r "^0.1.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.1") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "16jlwr4ipk6gvnhpwim445v3r024sy3nlnffsps2am1inibb78a7") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.1.2 (c (n "dharitri-wasm-module-features") (v "0.1.2") (d (list (d (n "dharitri-wasm") (r "^0.1.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.2") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "14c7zsqbd7mzk37j9n3rqxzsdpkp35n67yy45zsy9gzp3bmaimif") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.1.3 (c (n "dharitri-wasm-module-features") (v "0.1.3") (d (list (d (n "dharitri-wasm") (r "^0.1.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.3") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0rjwd97kddbhw0rw7cnyp26x78g8v4n8vyaa37nlgxgmvdmkryv7") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.1.4 (c (n "dharitri-wasm-module-features") (v "0.1.4") (d (list (d (n "dharitri-wasm") (r "^0.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.4") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1ng11iwg0i3j2f3gchb8yqlx5jkdl0vcqk82i7shqd7lpx7a15rd") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.1.5 (c (n "dharitri-wasm-module-features") (v "0.1.5") (d (list (d (n "dharitri-wasm") (r "^0.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.5") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1rd9m1h3rbffk49yd6wcczvb722k3a9g9h00nnarjsibi7injqa5") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.1.6 (c (n "dharitri-wasm-module-features") (v "0.1.6") (d (list (d (n "dharitri-wasm") (r "^0.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.6") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "0rvkpl66208v3h623z21rdk0ah2fzdj3qnp8lzi4ajsngb27n75h") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.1.7 (c (n "dharitri-wasm-module-features") (v "0.1.7") (d (list (d (n "dharitri-wasm") (r "^0.1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.7") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "10aiavgjzhzpm6v77psyjn2ggrkf90a0pdb3kdpaavlgdcshw696") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.1.8 (c (n "dharitri-wasm-module-features") (v "0.1.8") (d (list (d (n "dharitri-wasm") (r "^0.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.8") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "1lzksps9r0kfana91mrr714ixn4fnakxi84ch00q7gbd7lg2lq5a") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.1.9 (c (n "dharitri-wasm-module-features") (v "0.1.9") (d (list (d (n "dharitri-wasm") (r "^0.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.1.9") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0pm3kcmc2k94sxc02h714mk7dgbqbxgpnygkblxdv23q21rikwfy") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.2.0 (c (n "dharitri-wasm-module-features") (v "0.2.0") (d (list (d (n "dharitri-wasm") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.0") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "13iywbzpa477ni9izlfxadf3ni7y5phrdxwwf5p8yv2jgvxsvlds") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.2.1 (c (n "dharitri-wasm-module-features") (v "0.2.1") (d (list (d (n "dharitri-wasm") (r "^0.2.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.1") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1jpqf36vrykywn6ji7wbyxn6ry6cgxlzcwr2pp0hisd3vkdnkkvj") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.2.2 (c (n "dharitri-wasm-module-features") (v "0.2.2") (d (list (d (n "dharitri-wasm") (r "^0.2.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.2") (d #t) (k 2)))) (h "05rbdn0xbwnsv1b4h8rglirzdnsf6d1cxz1fray88h1qbiccs434")))

(define-public crate-dharitri-wasm-module-features-0.2.3 (c (n "dharitri-wasm-module-features") (v "0.2.3") (d (list (d (n "dharitri-wasm") (r "^0.2.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.3") (d #t) (k 2)))) (h "1nd5bsk3xjfs90y0n834ivnvsbd77y1rrf9h14n2sri1q9h7skki")))

(define-public crate-dharitri-wasm-module-features-0.2.4 (c (n "dharitri-wasm-module-features") (v "0.2.4") (d (list (d (n "dharitri-wasm") (r "^0.2.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.4") (d #t) (k 2)))) (h "1ybwj7hpgbykq7nr98m5mhiw20jbys9simpvvvqq6q9mf31mnb23")))

(define-public crate-dharitri-wasm-module-features-0.2.5 (c (n "dharitri-wasm-module-features") (v "0.2.5") (d (list (d (n "dharitri-wasm") (r "^0.2.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.5") (d #t) (k 2)))) (h "0h38jvy1z7ymri96jin9yqrfncxgzp1m3w4sm08c2q3wxl9jbizr")))

(define-public crate-dharitri-wasm-module-features-0.2.6 (c (n "dharitri-wasm-module-features") (v "0.2.6") (d (list (d (n "dharitri-wasm") (r "^0.2.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.6") (d #t) (k 2)))) (h "0h8vsdmikyli3b87f2dvvifgwahy20rm1yzvm3a2nn6kdn7wz4yq")))

(define-public crate-dharitri-wasm-module-features-0.2.7 (c (n "dharitri-wasm-module-features") (v "0.2.7") (d (list (d (n "dharitri-wasm") (r "^0.2.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.7") (d #t) (k 2)))) (h "0ncdk40hqw8117h6skgfvq0swg3j19qj8qywhgw7plghjr9ga5lx")))

(define-public crate-dharitri-wasm-module-features-0.2.8 (c (n "dharitri-wasm-module-features") (v "0.2.8") (d (list (d (n "dharitri-wasm") (r "^0.2.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.8") (d #t) (k 2)))) (h "19mjrpg2mjfz5dcyg4ycf826qf57zycdvsii24zj8w0787fryjm9")))

(define-public crate-dharitri-wasm-module-features-0.2.9 (c (n "dharitri-wasm-module-features") (v "0.2.9") (d (list (d (n "dharitri-wasm") (r "^0.2.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.2.9") (d #t) (k 2)))) (h "07j4a0yn30h98k5yscwmycp1nw6b36gp7nhycnjyvv4nmzv9bq5s")))

(define-public crate-dharitri-wasm-module-features-0.3.0 (c (n "dharitri-wasm-module-features") (v "0.3.0") (d (list (d (n "dharitri-wasm") (r "^0.3.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.0") (d #t) (k 2)))) (h "00xw5xyanhxd2az91sbs3j3c4r3mb11s96c0kkk4gikyjwqkrmcq")))

(define-public crate-dharitri-wasm-module-features-0.3.1 (c (n "dharitri-wasm-module-features") (v "0.3.1") (d (list (d (n "dharitri-wasm") (r "^0.3.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.1") (d #t) (k 2)))) (h "08ck6vaai352isji8v6cld5fy32g1wksys4rjg52cmpsxlakyb6h")))

(define-public crate-dharitri-wasm-module-features-0.3.2 (c (n "dharitri-wasm-module-features") (v "0.3.2") (d (list (d (n "dharitri-wasm") (r "^0.3.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.2") (d #t) (k 2)))) (h "18k2jh212jl4q0g94q20khhsycxkidavl1sd18lncdcgbmadxajq")))

(define-public crate-dharitri-wasm-module-features-0.3.3 (c (n "dharitri-wasm-module-features") (v "0.3.3") (d (list (d (n "dharitri-wasm") (r "^0.3.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.3") (d #t) (k 2)))) (h "0c6iqk6rcpscwxs2lzrnr9azwv52bnmvm4446jiiaxgiyga3yy00")))

(define-public crate-dharitri-wasm-module-features-0.3.4 (c (n "dharitri-wasm-module-features") (v "0.3.4") (d (list (d (n "dharitri-wasm") (r "^0.3.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.4") (d #t) (k 2)))) (h "0xbjf0m8v2n0f0k67v7xy2gm358r95dckc9w20v5mkfmpcdy8zij")))

(define-public crate-dharitri-wasm-module-features-0.3.5 (c (n "dharitri-wasm-module-features") (v "0.3.5") (d (list (d (n "dharitri-wasm") (r "^0.3.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.5") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.5") (d #t) (k 0)))) (h "12y2kqh6icbvpmgjq2i3y58kld51q64b7nq4v2pjxxfzx9gsmas2")))

(define-public crate-dharitri-wasm-module-features-0.3.6 (c (n "dharitri-wasm-module-features") (v "0.3.6") (d (list (d (n "dharitri-wasm") (r "^0.3.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.6") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.6") (d #t) (k 0)))) (h "05cmc26603yxf8d33hq89dva524inxrmr6zqswsmq8as785mly02")))

(define-public crate-dharitri-wasm-module-features-0.3.7 (c (n "dharitri-wasm-module-features") (v "0.3.7") (d (list (d (n "dharitri-wasm") (r "^0.3.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.7") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.7") (d #t) (k 0)))) (h "12fkq3cwclkksadv1sb6ykfhxpparmg5780q115jdcvhgv3k5zbz")))

(define-public crate-dharitri-wasm-module-features-0.3.8 (c (n "dharitri-wasm-module-features") (v "0.3.8") (d (list (d (n "dharitri-wasm") (r "^0.3.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.8") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.8") (d #t) (k 0)))) (h "05wzp27pcdar0srji80z38k9aj0dw0rqqf7ddpzbh5hj0k6ni9f3")))

(define-public crate-dharitri-wasm-module-features-0.3.9 (c (n "dharitri-wasm-module-features") (v "0.3.9") (d (list (d (n "dharitri-wasm") (r "^0.3.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.9") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.3.9") (d #t) (k 0)))) (h "192q8l6pryqa17p47mhhv8l0w56yh09mxkj4klif48p8xwyk4dyy")))

(define-public crate-dharitri-wasm-module-features-0.4.0 (c (n "dharitri-wasm-module-features") (v "0.4.0") (d (list (d (n "dharitri-wasm") (r "^0.4.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.4.0") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.4.0") (d #t) (k 0)))) (h "148w5lpgm076s3lja4d5q29m7zy8hgw44sz75ixk01z4ksh3l090")))

(define-public crate-dharitri-wasm-module-features-0.4.1 (c (n "dharitri-wasm-module-features") (v "0.4.1") (d (list (d (n "dharitri-wasm") (r "^0.4.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.4.1") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.4.1") (d #t) (k 0)))) (h "13792v908il7ycf67h9cr3k3bv4zn5gamy504raf5g43lnbp8i6s")))

(define-public crate-dharitri-wasm-module-features-0.4.2 (c (n "dharitri-wasm-module-features") (v "0.4.2") (d (list (d (n "dharitri-wasm") (r "^0.4.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.4.2") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.4.2") (d #t) (k 0)))) (h "0cj441vgn5819zr7slhcralrmyic05fd33fgxnylxfd59yxjwpxj")))

(define-public crate-dharitri-wasm-module-features-0.5.5 (c (n "dharitri-wasm-module-features") (v "0.5.5") (d (list (d (n "dharitri-wasm") (r "^0.5.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.5") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.5") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.5") (o #t) (d #t) (k 0)))) (h "0zahzwycr0f95y5z6v8z5kf9nxqsg4wd9qjpcqhswd9p7km2scyy") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.5.6 (c (n "dharitri-wasm-module-features") (v "0.5.6") (d (list (d (n "dharitri-wasm") (r "^0.5.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.6") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.6") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "051wak8nxrkmd9rssl1xfckyw7150ghk8sw19wnj01d56r2ll8n4") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.5.7 (c (n "dharitri-wasm-module-features") (v "0.5.7") (d (list (d (n "dharitri-wasm") (r "^0.5.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.7") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.7") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.7") (o #t) (d #t) (k 0)))) (h "1q1khl5im0phzfw2zzij0d9vgdrbmxkcyc7jag9agrrdyyxnqvfh") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.5.8 (c (n "dharitri-wasm-module-features") (v "0.5.8") (d (list (d (n "dharitri-wasm") (r "^0.5.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.8") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.8") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.8") (o #t) (d #t) (k 0)))) (h "1cfdylxq4c59fjphs67z9151m77hhask9bm4b5b535959xrh6mbb") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.5.9 (c (n "dharitri-wasm-module-features") (v "0.5.9") (d (list (d (n "dharitri-wasm") (r "^0.5.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.9") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.5.9") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "1ii9ji76dhb63fa3lwqjhxy1qb2aq7c5by2va6p8anpl680mplf0") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.6.0 (c (n "dharitri-wasm-module-features") (v "0.6.0") (d (list (d (n "dharitri-wasm") (r "^0.6.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.0") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0kcsdiyimzfnr9i5n48l2s1paclv5x3nmxzy6jqckbz4ip73762a") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.6.1 (c (n "dharitri-wasm-module-features") (v "0.6.1") (d (list (d (n "dharitri-wasm") (r "^0.6.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.1") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.6.1") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "1n6p4msc95zswci3y0z58nfik7yyxhkh1d09hny2gxm66rh3k40g") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.6.2 (c (n "dharitri-wasm-module-features") (v "0.6.2") (d (list (d (n "dharitri-wasm") (r "^0.6.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.2") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.6.2") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "1lz8kj4xh59m5zf2v0h7rmyh77wry20f8wgdi5ipnxb10sb7149q") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.6.3 (c (n "dharitri-wasm-module-features") (v "0.6.3") (d (list (d (n "dharitri-wasm") (r "^0.6.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.3") (d #t) (k 2)) (d (n "dharitri-wasm-derive") (r "^0.6.3") (d #t) (k 0)) (d (n "dharitri-wasm-node") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "1m8zbzp6maw6128dxcy1bgv6aj2xh3lg8r293hna4xiwbmcrsjx6") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.6.4 (c (n "dharitri-wasm-module-features") (v "0.6.4") (d (list (d (n "dharitri-wasm") (r "^0.6.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.4") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "1vm698rj993dy1z2wx2l04v59i7789kn4gr7hlbm0750f8y36hp8") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.6.5 (c (n "dharitri-wasm-module-features") (v "0.6.5") (d (list (d (n "dharitri-wasm") (r "^0.6.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.5") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.5") (o #t) (d #t) (k 0)))) (h "0nkjf231gaml72a2snzr6ycmqkglkfwm1daz218g0dr3l5vfa7vl") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.6.6 (c (n "dharitri-wasm-module-features") (v "0.6.6") (d (list (d (n "dharitri-wasm") (r "^0.6.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.6") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.6") (o #t) (d #t) (k 0)))) (h "14b4k5h04a2w1v8dwd8rd4b3chghchrwrh2y6zzfc0dqza7sdj8w") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.6.7 (c (n "dharitri-wasm-module-features") (v "0.6.7") (d (list (d (n "dharitri-wasm") (r "^0.6.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.7") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "1zdv7rin72hw24zd2g1f66kfw5dsbdbm6lmda770dajppjays8i9") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.6.8 (c (n "dharitri-wasm-module-features") (v "0.6.8") (d (list (d (n "dharitri-wasm") (r "^0.6.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.8") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.8") (o #t) (d #t) (k 0)))) (h "1wm22n62fm8dl82sj9ys5l8408y51n82ppd337q5ba8z1cq5aiq6") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.6.9 (c (n "dharitri-wasm-module-features") (v "0.6.9") (d (list (d (n "dharitri-wasm") (r "^0.6.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.6.9") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.6.9") (o #t) (d #t) (k 0)))) (h "0zibmlppirvdsficnf39w59xkcq1x4hdd2j0dwqic9812rrvji6j") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.7.0 (c (n "dharitri-wasm-module-features") (v "0.7.0") (d (list (d (n "dharitri-wasm") (r "^0.7.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.0") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1kcvlz6l1a44sfmjn98kps837wq4i1b2hf5n64fh2d186nh40d5l") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.7.1 (c (n "dharitri-wasm-module-features") (v "0.7.1") (d (list (d (n "dharitri-wasm") (r "^0.7.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.1") (d #t) (k 2)) (d (n "dharitri-wasm-node") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "0swlm3kp4kg6a2glf4qs1nyrq3zmmr92k966ka07w4nppi42zkpn") (f (quote (("wasm-output-mode" "dharitri-wasm-node"))))))

(define-public crate-dharitri-wasm-module-features-0.7.2 (c (n "dharitri-wasm-module-features") (v "0.7.2") (d (list (d (n "dharitri-wasm") (r "^0.7.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.2") (d #t) (k 2)))) (h "01qdggirlcbdg7k74w33ycfp23icpjlnx3lr5j4qpbj0773v2jl0")))

(define-public crate-dharitri-wasm-module-features-0.7.3 (c (n "dharitri-wasm-module-features") (v "0.7.3") (d (list (d (n "dharitri-wasm") (r "^0.7.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.3") (d #t) (k 2)))) (h "0b7709k545d847gq0c0hykwnj4win3a1f708pffsaz5lipncxjxw")))

(define-public crate-dharitri-wasm-module-features-0.7.4 (c (n "dharitri-wasm-module-features") (v "0.7.4") (d (list (d (n "dharitri-wasm") (r "^0.7.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.4") (d #t) (k 2)))) (h "17pmwfrb97h925vfyx3z4ycmpl3d74h08ykwpr6v78krrkq8rrl9")))

(define-public crate-dharitri-wasm-module-features-0.7.5 (c (n "dharitri-wasm-module-features") (v "0.7.5") (d (list (d (n "dharitri-wasm") (r "^0.7.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.5") (d #t) (k 2)))) (h "0lp1wb7dk1226b554jvd1lcisfpkd82f5irav4p38ddwb8x6snhb")))

(define-public crate-dharitri-wasm-module-features-0.7.6 (c (n "dharitri-wasm-module-features") (v "0.7.6") (d (list (d (n "dharitri-wasm") (r "^0.7.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.6") (d #t) (k 2)))) (h "1xmp221m0xr5g1dxhy0skp381yd118pf570kkq60g5m4g5dsaz5x")))

(define-public crate-dharitri-wasm-module-features-0.7.7 (c (n "dharitri-wasm-module-features") (v "0.7.7") (d (list (d (n "dharitri-wasm") (r "^0.7.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.7") (d #t) (k 2)))) (h "00qcxkal74jlw9naj0pc665jj9kapr4wnnngr5mw8zjy6a8l22c7")))

(define-public crate-dharitri-wasm-module-features-0.7.8 (c (n "dharitri-wasm-module-features") (v "0.7.8") (d (list (d (n "dharitri-wasm") (r "^0.7.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.8") (d #t) (k 2)))) (h "12aq16nl60mg6bhjzhbm86202x19lbvqb4n2x2xczk5ccf28gi01")))

(define-public crate-dharitri-wasm-module-features-0.7.9 (c (n "dharitri-wasm-module-features") (v "0.7.9") (d (list (d (n "dharitri-wasm") (r "^0.7.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.7.9") (d #t) (k 2)))) (h "0kjclfxvizh2djmd5642mln7hgh4xr0w8xq3mb0z3mxgs1xqv5cn")))

(define-public crate-dharitri-wasm-module-features-0.8.0 (c (n "dharitri-wasm-module-features") (v "0.8.0") (d (list (d (n "dharitri-wasm") (r "^0.8.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.0") (d #t) (k 2)))) (h "03zxyvqiqvpp8rgjqndv0i58s2qv1azi27mddyhhm6kd2az9bcvw")))

(define-public crate-dharitri-wasm-module-features-0.8.1 (c (n "dharitri-wasm-module-features") (v "0.8.1") (d (list (d (n "dharitri-wasm") (r "^0.8.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.1") (d #t) (k 2)))) (h "0n8il9505dsm3wmidfzdljscmdgfdrfxd44lm34dldqd3bzdnw3c")))

(define-public crate-dharitri-wasm-module-features-0.8.2 (c (n "dharitri-wasm-module-features") (v "0.8.2") (d (list (d (n "dharitri-wasm") (r "^0.8.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.2") (d #t) (k 2)))) (h "0bmki0w9jh90bpcjid9z8x798s5yznjdm164arkdzp2iwsga77ab")))

(define-public crate-dharitri-wasm-module-features-0.8.3 (c (n "dharitri-wasm-module-features") (v "0.8.3") (d (list (d (n "dharitri-wasm") (r "^0.8.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.3") (d #t) (k 2)))) (h "00w71inbxglql4v7vi50pn7h7bbbxicwln40rhrm4yxvlxxxfj05")))

(define-public crate-dharitri-wasm-module-features-0.8.4 (c (n "dharitri-wasm-module-features") (v "0.8.4") (d (list (d (n "dharitri-wasm") (r "^0.8.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.4") (d #t) (k 2)))) (h "04dclyf40n3hj1yxb58rddfv9fqv9w0kzmkbxnqn7smvyrspwyhf")))

(define-public crate-dharitri-wasm-module-features-0.8.5 (c (n "dharitri-wasm-module-features") (v "0.8.5") (d (list (d (n "dharitri-wasm") (r "^0.8.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.5") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.5") (d #t) (k 0)))) (h "10n3mpa6wx9a1bmx9sa5bdq9a812fyp4pm9vnfid4nqsgdwwg5kp")))

(define-public crate-dharitri-wasm-module-features-0.8.6 (c (n "dharitri-wasm-module-features") (v "0.8.6") (d (list (d (n "dharitri-wasm") (r "^0.8.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.6") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.6") (d #t) (k 0)))) (h "0ygmdfk88nnkfvbrad7y3ybbwxrp9hr42sazg4rp4dcz62j1ngjk")))

(define-public crate-dharitri-wasm-module-features-0.8.7 (c (n "dharitri-wasm-module-features") (v "0.8.7") (d (list (d (n "dharitri-wasm") (r "^0.8.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.7") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.7") (d #t) (k 0)))) (h "0n37lirccrfz0sqssl59byh8sf6i880k1n7dp3av8wakhlbbc8r0")))

(define-public crate-dharitri-wasm-module-features-0.8.8 (c (n "dharitri-wasm-module-features") (v "0.8.8") (d (list (d (n "dharitri-wasm") (r "^0.8.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.8") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.8") (d #t) (k 0)))) (h "0i6j27gryak79wpvkll2xgchf3q3h8473yy5pf4ibx93lazffpbq")))

(define-public crate-dharitri-wasm-module-features-0.8.9 (c (n "dharitri-wasm-module-features") (v "0.8.9") (d (list (d (n "dharitri-wasm") (r "^0.8.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.9") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.8.9") (d #t) (k 0)))) (h "1s7l1yxm042nshj5d45wwxj2fc7dccmb5bajb96h3hyqqjnwzi0s")))

(define-public crate-dharitri-wasm-module-features-0.9.0 (c (n "dharitri-wasm-module-features") (v "0.9.0") (d (list (d (n "dharitri-wasm") (r "^0.9.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.9.0") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.9.0") (d #t) (k 0)))) (h "1b1dkl85p6hinsbkfwnbsmhhzn47nnd149h946vdx83450dmbw3l")))

(define-public crate-dharitri-wasm-module-features-0.9.1 (c (n "dharitri-wasm-module-features") (v "0.9.1") (d (list (d (n "dharitri-wasm") (r "^0.9.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.9.1") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.9.1") (d #t) (k 0)))) (h "0rn8gjvpxvj21l8a03d2rlggn55af5nsad2vrgmj2lakavhap3z5")))

(define-public crate-dharitri-wasm-module-features-0.9.2 (c (n "dharitri-wasm-module-features") (v "0.9.2") (d (list (d (n "dharitri-wasm") (r "^0.9.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.9.2") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.9.2") (d #t) (k 0)))) (h "1ppv3fqc4hxx5nmqf1nhcmrfs58wk8hip6jgiz4697xzicwky932")))

