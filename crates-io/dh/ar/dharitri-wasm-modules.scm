(define-module (crates-io dh ar dharitri-wasm-modules) #:use-module (crates-io))

(define-public crate-dharitri-wasm-modules-0.3.5 (c (n "dharitri-wasm-modules") (v "0.3.5") (d (list (d (n "dharitri-wasm") (r "^0.3.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.5") (d #t) (k 2)))) (h "1y1vlfqrrm0vyi5svh9xq6r3yiji2axfmnl6d87whygy5xp80p78")))

(define-public crate-dharitri-wasm-modules-0.3.6 (c (n "dharitri-wasm-modules") (v "0.3.6") (d (list (d (n "dharitri-wasm") (r "^0.3.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.6") (d #t) (k 2)))) (h "0sq96q6j79sks90ygsk0hfnam25hhx4j7jn1ma3sqc076b9mk40z")))

(define-public crate-dharitri-wasm-modules-0.3.7 (c (n "dharitri-wasm-modules") (v "0.3.7") (d (list (d (n "dharitri-wasm") (r "^0.3.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.7") (d #t) (k 2)))) (h "1c38fr2l63avwdvq87qw1jlxnjh1d3xakabgksgcb64rwfwzzj7p")))

(define-public crate-dharitri-wasm-modules-0.3.8 (c (n "dharitri-wasm-modules") (v "0.3.8") (d (list (d (n "dharitri-wasm") (r "^0.3.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.8") (d #t) (k 2)))) (h "09k2hklr37y6a3fhx7h90c1ynmykchhb5jaxllwbv9i5jpimcwa0")))

(define-public crate-dharitri-wasm-modules-0.3.9 (c (n "dharitri-wasm-modules") (v "0.3.9") (d (list (d (n "dharitri-wasm") (r "^0.3.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.3.9") (d #t) (k 2)))) (h "0g7j5klvnhbdbhvw8w3yjmwz7zibylbp48d1rksvndcgkn4rrrfr")))

(define-public crate-dharitri-wasm-modules-0.4.0 (c (n "dharitri-wasm-modules") (v "0.4.0") (d (list (d (n "dharitri-wasm") (r "^0.4.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.4.0") (d #t) (k 2)))) (h "0zybii4852j0xvv4kb6wxnm4sil52mg94pawqahp2sw9c062s22f")))

(define-public crate-dharitri-wasm-modules-0.4.1 (c (n "dharitri-wasm-modules") (v "0.4.1") (d (list (d (n "dharitri-wasm") (r "^0.4.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.4.1") (d #t) (k 2)))) (h "1ipkvhm17ils9l70snvf9hwjz62m7kz0zd0sq8m7c6scd3lhxiia")))

(define-public crate-dharitri-wasm-modules-0.4.2 (c (n "dharitri-wasm-modules") (v "0.4.2") (d (list (d (n "dharitri-wasm") (r "^0.4.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.4.2") (d #t) (k 2)))) (h "06bc2jlzqiz1h545b0m1w9spy7zyaj8ng3r7lbc978qqncgv5azd")))

(define-public crate-dharitri-wasm-modules-0.4.5 (c (n "dharitri-wasm-modules") (v "0.4.5") (d (list (d (n "dharitri-wasm") (r "^0.4.5") (d #t) (k 0)))) (h "0656hbn9dg38033y07lh0ahr7dmg93fjnpabl2k8d7qdv4qv7sb0")))

(define-public crate-dharitri-wasm-modules-0.4.6 (c (n "dharitri-wasm-modules") (v "0.4.6") (d (list (d (n "dharitri-wasm") (r "^0.4.6") (d #t) (k 0)))) (h "1saysb6kalrgfrfkgpix12c6gkkzal4cmxyr3cpl5bwz734dv0rc") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.4.7 (c (n "dharitri-wasm-modules") (v "0.4.7") (d (list (d (n "dharitri-wasm") (r "^0.4.7") (d #t) (k 0)))) (h "0l6rrrbx3808pv3gq3a49y9wfs7msxrshn5n46rfn582yqkadfpc") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.4.8 (c (n "dharitri-wasm-modules") (v "0.4.8") (d (list (d (n "dharitri-wasm") (r "^0.4.8") (d #t) (k 0)))) (h "072ckh0dkxhq0zzxrxrbf2ryiy244p7iwgaxlwqkqp4vwfx7lshd") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.4.9 (c (n "dharitri-wasm-modules") (v "0.4.9") (d (list (d (n "dharitri-wasm") (r "^0.4.9") (d #t) (k 0)))) (h "18v38d13l4a1bvnh1wixaydrz5zy2ir570y8855zxk3rzhmqq5zf") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.5.0 (c (n "dharitri-wasm-modules") (v "0.5.0") (d (list (d (n "dharitri-wasm") (r "^0.5.0") (d #t) (k 0)))) (h "0s1xchcpfsc4w01g453king2ba4vc2jxh7q12dlzbdbv6xhyqjl0") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.5.1 (c (n "dharitri-wasm-modules") (v "0.5.1") (d (list (d (n "dharitri-wasm") (r "^0.5.1") (d #t) (k 0)))) (h "1sq4qwl5x5d89bmx5vw9p0cvjs8i210mwd69wjl5bgbx9bcxgf3l") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.5.2 (c (n "dharitri-wasm-modules") (v "0.5.2") (d (list (d (n "dharitri-wasm") (r "^0.5.2") (d #t) (k 0)))) (h "1i1603z6cbq52fw5x563p47r2xssjldynja92ar1hv6b066vgc56") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.5.3 (c (n "dharitri-wasm-modules") (v "0.5.3") (d (list (d (n "dharitri-wasm") (r "^0.5.3") (d #t) (k 0)))) (h "06vn2hl6d29nmmjkwrxjf3j8ffhmc97a9vvx6ii3xi73y55nq7pj") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.5.4 (c (n "dharitri-wasm-modules") (v "0.5.4") (d (list (d (n "dharitri-wasm") (r "^0.5.4") (d #t) (k 0)))) (h "0di4wyiixyx89czl02gn1blsfc8n61npk0k816cvnggnxigy75x8") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.8.5 (c (n "dharitri-wasm-modules") (v "0.8.5") (d (list (d (n "dharitri-wasm") (r "^0.8.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.5") (d #t) (k 2)))) (h "1196jgpihr9mdbfjn8s0hhia8097q86v0zcxy6gr0vnanksc23yi")))

(define-public crate-dharitri-wasm-modules-0.8.6 (c (n "dharitri-wasm-modules") (v "0.8.6") (d (list (d (n "dharitri-wasm") (r "^0.8.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.6") (d #t) (k 2)))) (h "01j2190hgsfw0vnmq7g10vl4cjk3a5my5kfbvgad188ds84k8fcl")))

(define-public crate-dharitri-wasm-modules-0.8.7 (c (n "dharitri-wasm-modules") (v "0.8.7") (d (list (d (n "dharitri-wasm") (r "^0.8.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.7") (d #t) (k 2)))) (h "1s5as80ba6py1vpzr6rr5vmi4c9wnsagxdyfmhypp47gc05sizl0")))

(define-public crate-dharitri-wasm-modules-0.8.8 (c (n "dharitri-wasm-modules") (v "0.8.8") (d (list (d (n "dharitri-wasm") (r "^0.8.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.8") (d #t) (k 2)))) (h "1yzxvz3fmgq2bsj3h8wcrxj84jbsvcsfyq12gp975gv2mhcahqf8")))

(define-public crate-dharitri-wasm-modules-0.8.9 (c (n "dharitri-wasm-modules") (v "0.8.9") (d (list (d (n "dharitri-wasm") (r "^0.8.9") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.8.9") (d #t) (k 2)))) (h "0kq8hc5cp106hwjavgh8qxjgpwp0yflcwl4fm7awskbvfbwv0vcm")))

(define-public crate-dharitri-wasm-modules-0.9.0 (c (n "dharitri-wasm-modules") (v "0.9.0") (d (list (d (n "dharitri-wasm") (r "^0.9.0") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.9.0") (d #t) (k 2)))) (h "0xaaqjl07lshzj29irvqhhr8a1pj8sxvllaw3q3nlmxclz3n8fqx")))

(define-public crate-dharitri-wasm-modules-0.9.1 (c (n "dharitri-wasm-modules") (v "0.9.1") (d (list (d (n "dharitri-wasm") (r "^0.9.1") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.9.1") (d #t) (k 2)))) (h "0p3cw4sgbzwvjhc1vfd2zs1m0xcsjzl69gxbgi8ys203h2vjgivz")))

(define-public crate-dharitri-wasm-modules-0.9.2 (c (n "dharitri-wasm-modules") (v "0.9.2") (d (list (d (n "dharitri-wasm") (r "^0.9.2") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.9.2") (d #t) (k 2)))) (h "0yaqibb7z7lwkmvplq0vhjgsfgkrks5saygccf2c74ybkdp999za")))

(define-public crate-dharitri-wasm-modules-0.9.3 (c (n "dharitri-wasm-modules") (v "0.9.3") (d (list (d (n "dharitri-wasm") (r "^0.9.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.9.3") (d #t) (k 2)))) (h "112ajbfzj9xs1wgp9mqadaxzzfvcgib0cj4p9crhyfzxd4apcm7x")))

(define-public crate-dharitri-wasm-modules-0.9.4 (c (n "dharitri-wasm-modules") (v "0.9.4") (d (list (d (n "dharitri-wasm") (r "^0.9.4") (d #t) (k 0)))) (h "1q90r4cl9cynz8g9wjml7fzxvyil7jzhsy8mpc7krp3ml9lmpg3d")))

(define-public crate-dharitri-wasm-modules-0.9.5 (c (n "dharitri-wasm-modules") (v "0.9.5") (d (list (d (n "dharitri-wasm") (r "^0.9.5") (d #t) (k 0)))) (h "03m47gh61lc2ppn1bd674l5hf9a31cc6gjc4p0j8w9zddin8vrd1")))

(define-public crate-dharitri-wasm-modules-0.9.6 (c (n "dharitri-wasm-modules") (v "0.9.6") (d (list (d (n "dharitri-wasm") (r "^0.9.6") (d #t) (k 0)))) (h "0wlk6i98hnlapg7x14ll00jd59av3c5cl8idlidq45hxhidq5aaz") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.9.8 (c (n "dharitri-wasm-modules") (v "0.9.8") (d (list (d (n "dharitri-wasm") (r "^0.9.8") (d #t) (k 0)))) (h "0p359xwgjgad7p070z1a2bqr7nybsmz72xwi4ai3la5kl45n66v8") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.9.7 (c (n "dharitri-wasm-modules") (v "0.9.7") (d (list (d (n "dharitri-wasm") (r "^0.9.7") (d #t) (k 0)))) (h "11v8ii3ikfg35gkixvcz6p2dby54fn5asl6w8idv6djbcy8iwg7z") (f (quote (("alloc" "dharitri-wasm/alloc")))) (y #t)))

(define-public crate-dharitri-wasm-modules-0.9.9 (c (n "dharitri-wasm-modules") (v "0.9.9") (d (list (d (n "dharitri-wasm") (r "^0.9.9") (d #t) (k 0)))) (h "0bvhfixii4rzfgdsk1cylazdwh1wl23qmfihn9vhaczi1w48ii97") (f (quote (("alloc" "dharitri-wasm/alloc")))) (y #t)))

(define-public crate-dharitri-wasm-modules-0.10.0 (c (n "dharitri-wasm-modules") (v "0.10.0") (d (list (d (n "dharitri-wasm") (r "^0.10.0") (d #t) (k 0)))) (h "1xkvkyjrn5r0yv232wj9np0qjzvc9sb4jzfn642fgid3143ql4ml") (f (quote (("alloc" "dharitri-wasm/alloc")))) (y #t)))

(define-public crate-dharitri-wasm-modules-0.10.1 (c (n "dharitri-wasm-modules") (v "0.10.1") (d (list (d (n "dharitri-wasm") (r "^0.10.1") (d #t) (k 0)))) (h "10wcg6ych9ycc48g0myih2pi6nww5dcvafga14g7mk59wwf2jcjy") (f (quote (("alloc" "dharitri-wasm/alloc")))) (y #t)))

(define-public crate-dharitri-wasm-modules-0.10.2 (c (n "dharitri-wasm-modules") (v "0.10.2") (d (list (d (n "dharitri-wasm") (r "^0.10.2") (d #t) (k 0)))) (h "0p5lfayy9c951idkwmj316hzpd2wvs4c7xvzsgpm2mw045p2zmv6") (f (quote (("alloc" "dharitri-wasm/alloc")))) (y #t)))

(define-public crate-dharitri-wasm-modules-0.10.3 (c (n "dharitri-wasm-modules") (v "0.10.3") (d (list (d (n "dharitri-wasm") (r "^0.10.3") (d #t) (k 0)))) (h "0fcdpfwh36w36f8srwnp9d06fi2zb7dkn1a8zpdx31pknd24mbp0") (f (quote (("alloc" "dharitri-wasm/alloc")))) (y #t)))

(define-public crate-dharitri-wasm-modules-0.10.4 (c (n "dharitri-wasm-modules") (v "0.10.4") (d (list (d (n "dharitri-wasm") (r "^0.10.4") (d #t) (k 0)))) (h "0s5gz8k1j0a0yw00i0qf2gz77xymhnpi1f8n9aiq1wfkaqd6acky") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.10.5 (c (n "dharitri-wasm-modules") (v "0.10.5") (d (list (d (n "dharitri-wasm") (r "^0.10.5") (d #t) (k 0)))) (h "0visvjc97zjjypw7fa8f8k1rpk76w8w7m10h3s05hmgy3sf5mbak") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.10.6 (c (n "dharitri-wasm-modules") (v "0.10.6") (d (list (d (n "dharitri-wasm") (r "^0.10.6") (d #t) (k 0)))) (h "10740ydrjfv1rv8716zk8d8lnihz5v3jis7i19cisl4mbjb8rg86") (f (quote (("alloc" "dharitri-wasm/alloc"))))))

(define-public crate-dharitri-wasm-modules-0.10.7 (c (n "dharitri-wasm-modules") (v "0.10.7") (d (list (d (n "dharitri-wasm") (r "^0.10.7") (d #t) (k 0)))) (h "108g4fz2gfhg5rzhfqzb82652pwwbrd0mrxmgiygijx22xmq966r") (f (quote (("alloc" "dharitri-wasm/alloc")))) (y #t)))

(define-public crate-dharitri-wasm-modules-0.10.8 (c (n "dharitri-wasm-modules") (v "0.10.8") (d (list (d (n "dharitri-wasm") (r "^0.10.8") (d #t) (k 0)))) (h "1ncqszzjdl61rpqhk988allpgdyzc6i5n7ayk8pxb3sji9iha4zd") (f (quote (("alloc" "dharitri-wasm/alloc")))) (y #t)))

