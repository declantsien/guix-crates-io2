(define-module (crates-io dh ar dharitri-sc-price-aggregator) #:use-module (crates-io))

(define-public crate-dharitri-sc-price-aggregator-0.5.3 (c (n "dharitri-sc-price-aggregator") (v "0.5.3") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "dharitri-wasm") (r "^0.5.3") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.5.3") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.5.3") (d #t) (k 0)))) (h "1jsdfkjj8ckc1kmcrs0lvj02n82cl547zk345i5fp0ajk993wpaj")))

(define-public crate-dharitri-sc-price-aggregator-0.10.4 (c (n "dharitri-sc-price-aggregator") (v "0.10.4") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "dharitri-wasm") (r "^0.10.4") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.10.4") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.10.4") (d #t) (k 0)))) (h "1bzac0dcck2ybh4fjzq0kh233l1504spjmspa4zvn5rfq1rcxxil")))

(define-public crate-dharitri-sc-price-aggregator-0.10.5 (c (n "dharitri-sc-price-aggregator") (v "0.10.5") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "dharitri-wasm") (r "^0.10.5") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.10.5") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.10.5") (d #t) (k 0)))) (h "1lzflc0bn32vhbv9lvmq79fsgwn2vhdkswx4nmzry4nhcfx16hcl")))

(define-public crate-dharitri-sc-price-aggregator-0.10.6 (c (n "dharitri-sc-price-aggregator") (v "0.10.6") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "dharitri-wasm") (r "^0.10.6") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.10.6") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.10.6") (d #t) (k 0)))) (h "01aqfja27db8dgam1s0wkd6i92r2gpakha7l9rd5kk204jjdr83a")))

(define-public crate-dharitri-sc-price-aggregator-0.10.7 (c (n "dharitri-sc-price-aggregator") (v "0.10.7") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "dharitri-wasm") (r "^0.10.7") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.10.7") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.10.7") (d #t) (k 0)))) (h "1dw9iys2w69pcl9pfmd4af0m6q690s8ik9s4ff9mml0v55l87s3p")))

(define-public crate-dharitri-sc-price-aggregator-0.10.8 (c (n "dharitri-sc-price-aggregator") (v "0.10.8") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "dharitri-wasm") (r "^0.10.8") (d #t) (k 0)) (d (n "dharitri-wasm-debug") (r "^0.10.8") (d #t) (k 2)) (d (n "dharitri-wasm-modules") (r "^0.10.8") (d #t) (k 0)))) (h "1319igsg9hqid95nbjdrj61ccqcazwqa98x7l8y00z6rs9mbjz10")))

