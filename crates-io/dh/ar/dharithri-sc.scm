(define-module (crates-io dh ar dharithri-sc) #:use-module (crates-io))

(define-public crate-dharithri-sc-0.43.3 (c (n "dharithri-sc") (v "0.43.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "dharithri-sc-codec") (r "=0.18.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dharithri-sc-derive") (r "=0.43.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "190s4x349jz3xd0hj1jfqw9a24p2k88cxyk2sj877bz6l06ww3hp") (f (quote (("promises") ("num-bigint" "dharithri-sc-codec/num-bigint") ("managed-map") ("esdt-token-payment-legacy-decode") ("alloc" "dharithri-sc-codec/alloc"))))))

