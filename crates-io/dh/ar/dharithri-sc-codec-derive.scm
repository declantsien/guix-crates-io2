(define-module (crates-io dh ar dharithri-sc-codec-derive) #:use-module (crates-io))

(define-public crate-dharithri-sc-codec-derive-0.18.1 (c (n "dharithri-sc-codec-derive") (v "0.18.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hfaf2ql79xasqv795gshsa81pb20gj4625ymw60g4yb6rmxsrc8") (f (quote (("default" "syn/full" "syn/parsing" "syn/extra-traits"))))))

