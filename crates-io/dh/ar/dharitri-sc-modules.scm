(define-module (crates-io dh ar dharitri-sc-modules) #:use-module (crates-io))

(define-public crate-dharitri-sc-modules-0.0.1 (c (n "dharitri-sc-modules") (v "0.0.1") (d (list (d (n "dharitri-sc") (r "^0.0.1") (d #t) (k 0)))) (h "17vyzrc67gvg153as3586ai2jr8q2fb9hzfpl29lignvv5yagi6k") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.0.2 (c (n "dharitri-sc-modules") (v "0.0.2") (d (list (d (n "dharitri-sc") (r "^0.0.2") (d #t) (k 0)))) (h "1351y2354kxlb96mvzfgj417jpjp0xs8697rva6pc68lyzdzxi4h") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.0.3 (c (n "dharitri-sc-modules") (v "0.0.3") (d (list (d (n "dharitri-sc") (r "^0.0.3") (d #t) (k 0)))) (h "0pk6rw3psil8qzvkpm5yg9rb4nlpmki20yc394k17fr30all7dgg") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.0.4 (c (n "dharitri-sc-modules") (v "0.0.4") (d (list (d (n "dharitri-sc") (r "^0.0.4") (d #t) (k 0)))) (h "01kma08wai30c7p1rfapgmqia1vw54c68hj44y1vqrvrsz6qgqcq") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.0.5 (c (n "dharitri-sc-modules") (v "0.0.5") (d (list (d (n "dharitri-sc") (r "^0.0.5") (d #t) (k 0)))) (h "12fymj7rysyqngyc3854zkd6gs84wbjp0zv6rm5h22xhgq6pkga0") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.0.6 (c (n "dharitri-sc-modules") (v "0.0.6") (d (list (d (n "dharitri-sc") (r "^0.0.6") (d #t) (k 0)))) (h "0vj5hdgj9mpp860kz73rgzhgmcbqrc60pxj0vv1aayzs0i3qwdmq") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.0.7 (c (n "dharitri-sc-modules") (v "0.0.7") (d (list (d (n "dharitri-sc") (r "^0.0.7") (d #t) (k 0)))) (h "0vcbp315y89rl7nyidrm2yzaimi9j954wf89pvksmizfmd1dagq8") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.0.8 (c (n "dharitri-sc-modules") (v "0.0.8") (d (list (d (n "dharitri-sc") (r "^0.0.8") (d #t) (k 0)))) (h "1d7yhi4xdb4ibj4f1cd6kwma15fzyji22jb1hph1xqqmq9hm0jsb") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.0.9 (c (n "dharitri-sc-modules") (v "0.0.9") (d (list (d (n "dharitri-sc") (r "^0.0.9") (d #t) (k 0)))) (h "0lbz3n3jksa8fwscb1s7wyp2rb7shxkvmvwciw90zllsc6aywjj1") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.1.0 (c (n "dharitri-sc-modules") (v "0.1.0") (d (list (d (n "dharitri-sc") (r "^0.1.0") (d #t) (k 0)))) (h "0wlcnd8br08chssm3s69gim41wz25vkzvjskgg3f58aycciyz492") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.1.1 (c (n "dharitri-sc-modules") (v "0.1.1") (d (list (d (n "dharitri-sc") (r "^0.1.1") (d #t) (k 0)))) (h "0aixbrl0pic20vzxyfxpxgl0c9956gksj3cwdwi1np464sfzljma") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.1.2 (c (n "dharitri-sc-modules") (v "0.1.2") (d (list (d (n "dharitri-sc") (r "^0.1.2") (d #t) (k 0)))) (h "0xw5yi45m8mrf8qnmlikn9n5izxd9qpa7niqqvjqvi0qax176qxv") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.1.3 (c (n "dharitri-sc-modules") (v "0.1.3") (d (list (d (n "dharitri-sc") (r "^0.1.3") (d #t) (k 0)))) (h "1kyvnb593030dng9hvnzwbglkfidh5z4p9qcxn0dc05dbgdwj02v") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.1.4 (c (n "dharitri-sc-modules") (v "0.1.4") (d (list (d (n "dharitri-sc") (r "^0.1.4") (d #t) (k 0)))) (h "1qmycv1wshbw1cmd26qdpddng3sq9inww2rka6ambqibkr7gar8s") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.1.5 (c (n "dharitri-sc-modules") (v "0.1.5") (d (list (d (n "dharitri-sc") (r "^0.1.5") (d #t) (k 0)))) (h "1xllamk947hjjfm36bx9b5xjjfdvzmwfhmvklxs97x9hdzmxrzy6") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.1.6 (c (n "dharitri-sc-modules") (v "0.1.6") (d (list (d (n "dharitri-sc") (r "^0.1.6") (d #t) (k 0)))) (h "0nnb4iny4s874zp3hp0azmy13c1cvsn6q7gc96kprlc1mif34f4g") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.1.7 (c (n "dharitri-sc-modules") (v "0.1.7") (d (list (d (n "dharitri-sc") (r "^0.1.7") (d #t) (k 0)))) (h "05s2zh0kcg1xbzj7bljd1gvkvbzj7qn13ylgyxxjw75d6c4jgs67") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.1.8 (c (n "dharitri-sc-modules") (v "0.1.8") (d (list (d (n "dharitri-sc") (r "^0.1.8") (d #t) (k 0)))) (h "1nmmh9s64dsicgsyyshhafjbw2yslz04qw5rpbvxp50xvr45lshd") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.1.9 (c (n "dharitri-sc-modules") (v "0.1.9") (d (list (d (n "dharitri-sc") (r "^0.1.9") (d #t) (k 0)))) (h "0qahf39wjwvwfjk8992gmy2jajwd576gfvn8mr2mrjksnprc6vdg") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.2.0 (c (n "dharitri-sc-modules") (v "0.2.0") (d (list (d (n "dharitri-sc") (r "^0.2.0") (d #t) (k 0)))) (h "0pc6skg6wlrs09vy1q66210ilpldkkfgk0y3h6h8iwfkjfih0573") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.10.9 (c (n "dharitri-sc-modules") (v "0.10.9") (d (list (d (n "dharitri-sc") (r "^0.10.9") (d #t) (k 0)))) (h "17riacv7g1npcvzly8hv00bzpvagbpr316jfj6bgsx88i2m71ba4") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.11.0 (c (n "dharitri-sc-modules") (v "0.11.0") (d (list (d (n "dharitri-sc") (r "^0.11.0") (d #t) (k 0)))) (h "04iyx8nf7z11zb0yv0q2y2j33rhl3xiiw0h9d31a7xn0wpqw3ga1") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.11.1 (c (n "dharitri-sc-modules") (v "0.11.1") (d (list (d (n "dharitri-sc") (r "^0.11.1") (d #t) (k 0)))) (h "15xlnydrn2mxxilf9wvjd9hchnacv8p3rks7v6clx0h7n3q1hrck") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.11.2 (c (n "dharitri-sc-modules") (v "0.11.2") (d (list (d (n "dharitri-sc") (r "^0.11.2") (d #t) (k 0)))) (h "1g13w4ak883i5nxspqki8awidxh2d4lhxs3x9mzri3zvzbacazzh") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.11.3 (c (n "dharitri-sc-modules") (v "0.11.3") (d (list (d (n "dharitri-sc") (r "^0.11.3") (d #t) (k 0)))) (h "111lrwg4ipwpahaah6rr76nsscj8hm2whz7ihrsp2qk8ljhh9kbh") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.11.4 (c (n "dharitri-sc-modules") (v "0.11.4") (d (list (d (n "dharitri-sc") (r "^0.11.4") (d #t) (k 0)))) (h "0f7p763s86w1p7skfwnml3x78yxval15klbhmkabzmzqyd2paxlq") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.11.5 (c (n "dharitri-sc-modules") (v "0.11.5") (d (list (d (n "dharitri-sc") (r "^0.11.5") (d #t) (k 0)))) (h "0xxiad7jl3rp0dmpgf6w2r6rdyz8xbk6z0cc8xnlcadiaysjpjzz") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.11.6 (c (n "dharitri-sc-modules") (v "0.11.6") (d (list (d (n "dharitri-sc") (r "^0.11.6") (d #t) (k 0)))) (h "07lr91jmc713bdy5wdkad3d58sin1j1p9ximkabqsvlpammwhk0z") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.11.7 (c (n "dharitri-sc-modules") (v "0.11.7") (d (list (d (n "dharitri-sc") (r "^0.11.7") (d #t) (k 0)))) (h "0xaj43gdhi3qz89nnbgd1r4pbr4jj0pfjihbvkf657bsm7m5vwl3") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.11.8 (c (n "dharitri-sc-modules") (v "0.11.8") (d (list (d (n "dharitri-sc") (r "^0.11.8") (d #t) (k 0)))) (h "1y7zw14j87cc5226ismnkp9s4wqly7mbrqn2xx2j0vf7pdppyk4g") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.11.9 (c (n "dharitri-sc-modules") (v "0.11.9") (d (list (d (n "dharitri-sc") (r "^0.11.9") (d #t) (k 0)))) (h "1xfpi62lq4ir4m69jnp50b085p97qx50p3r7xz0yfsjfrlh9j7pz") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.12.0 (c (n "dharitri-sc-modules") (v "0.12.0") (d (list (d (n "dharitri-sc") (r "^0.12.0") (d #t) (k 0)))) (h "1yalmz9h5h5bhfy2nxgcinr88pqgbbvrw1nhp0ij4hwdzh6xcpdh") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.12.1 (c (n "dharitri-sc-modules") (v "0.12.1") (d (list (d (n "dharitri-sc") (r "^0.12.1") (d #t) (k 0)))) (h "1xkhgcn1x47bbbcvwf918fwfcb55crbla8shhf68gjv78ngvfnhi") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.12.2 (c (n "dharitri-sc-modules") (v "0.12.2") (d (list (d (n "dharitri-sc") (r "^0.12.2") (d #t) (k 0)))) (h "090y6yj708v6yjw8fxvh4s8j1h373ffmf397rccgg0kw8624f2m7") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.12.3 (c (n "dharitri-sc-modules") (v "0.12.3") (d (list (d (n "dharitri-sc") (r "^0.12.3") (d #t) (k 0)))) (h "04375x8f8r4sccx4xxb1dnf8ja5r6j7xwzv7yvw0h20dchbxmbk9") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.12.4 (c (n "dharitri-sc-modules") (v "0.12.4") (d (list (d (n "dharitri-sc") (r "^0.12.4") (d #t) (k 0)))) (h "1r9ns6gxr70vzqcr56ghz9vmn0qax3nf0a29y7rvw1y7bywd502n") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.12.5 (c (n "dharitri-sc-modules") (v "0.12.5") (d (list (d (n "dharitri-sc") (r "^0.12.5") (d #t) (k 0)))) (h "0d8nzfhxkrzqz5ym7a2syc1dnhdpsq0m5xjd47xda7cidc8drpdg") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.12.6 (c (n "dharitri-sc-modules") (v "0.12.6") (d (list (d (n "dharitri-sc") (r "^0.12.6") (d #t) (k 0)))) (h "0s9v4fq0jqsq65s28s8cn3fw4f7nqpnm3mm8ivkw2wa6aj115zc9") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.12.7 (c (n "dharitri-sc-modules") (v "0.12.7") (d (list (d (n "dharitri-sc") (r "^0.12.7") (d #t) (k 0)))) (h "0vbgardypfzqm89989m4kzadawp6szpmfy4n3bcvf1x09sqx2p6g") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.12.8 (c (n "dharitri-sc-modules") (v "0.12.8") (d (list (d (n "dharitri-sc") (r "^0.12.8") (d #t) (k 0)))) (h "1cm18f76ha4jw3w97ni1vibcmh25fiffrsri7v7fmv5xr8a9nwl1") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.12.9 (c (n "dharitri-sc-modules") (v "0.12.9") (d (list (d (n "dharitri-sc") (r "^0.12.9") (d #t) (k 0)))) (h "0vwyba86cqpbp0v8pg3v9y4bxynp1mrkwqbg10xl499l2irsdyri") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.13.0 (c (n "dharitri-sc-modules") (v "0.13.0") (d (list (d (n "dharitri-sc") (r "^0.13.0") (d #t) (k 0)))) (h "0g60f76yr6d78ixwq97r5lzi0r6akaip3il6z5fyj48gg7hs3x86") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.13.1 (c (n "dharitri-sc-modules") (v "0.13.1") (d (list (d (n "dharitri-sc") (r "^0.13.1") (d #t) (k 0)))) (h "0wj4b4w6il5ija5bfkq64rmwfmzmli2zzlfzk5dc9zdiz8m8d40g") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.13.2 (c (n "dharitri-sc-modules") (v "0.13.2") (d (list (d (n "dharitri-sc") (r "^0.13.2") (d #t) (k 0)))) (h "03rn2pywzamnkcrmpcyfc6d4cx8c279mzp6khqyfndaz8053n16s") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.13.3 (c (n "dharitri-sc-modules") (v "0.13.3") (d (list (d (n "dharitri-sc") (r "^0.13.3") (d #t) (k 0)))) (h "142hrbl0v8nnqj5qwsm31hdhfj9qjkwh6a5my854wh6xzkjng6yg") (f (quote (("alloc" "dharitri-sc/alloc"))))))

(define-public crate-dharitri-sc-modules-0.13.4 (c (n "dharitri-sc-modules") (v "0.13.4") (d (list (d (n "dharitri-sc") (r "^0.13.4") (d #t) (k 0)))) (h "1jv8s2z5b3pc3ydp0qj4wjmj9wpn6zxzp0n5fmaf7wr44n94ryc3") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.13.5 (c (n "dharitri-sc-modules") (v "0.13.5") (d (list (d (n "dharitri-sc") (r "^0.13.5") (d #t) (k 0)))) (h "0cr4hl2ql00cvdbf1yi2smslyywhgv7ikni8i8m478l6nbzalqlh") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.13.6 (c (n "dharitri-sc-modules") (v "0.13.6") (d (list (d (n "dharitri-sc") (r "^0.13.6") (d #t) (k 0)))) (h "1k6wc9qf849hqicsf9jz6ng1730zsm22ddpz9ylfry01ryx3w1la") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.13.7 (c (n "dharitri-sc-modules") (v "0.13.7") (d (list (d (n "dharitri-sc") (r "^0.13.7") (d #t) (k 0)))) (h "0vg58v089hz59l1isibw2zhrrx9rmh0jwrq4pscxr7ci6sbynzp7") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.13.8 (c (n "dharitri-sc-modules") (v "0.13.8") (d (list (d (n "dharitri-sc") (r "^0.13.8") (d #t) (k 0)))) (h "1s8wg7f5jm5jf8gdwcladhybk96j3rxxxrj2adynj5n223ag85ii") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.13.9 (c (n "dharitri-sc-modules") (v "0.13.9") (d (list (d (n "dharitri-sc") (r "^0.13.9") (d #t) (k 0)))) (h "0l999aws1g74ws73l37dv91n3yh1nzkaanpqiffx6r78csi4nf7y") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.14.0 (c (n "dharitri-sc-modules") (v "0.14.0") (d (list (d (n "dharitri-sc") (r "^0.14.0") (d #t) (k 0)))) (h "1kmsd72i8i02kh65x2y0zn53d1g1ywzvn21hpxpnjm6p6ikciz5g") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

(define-public crate-dharitri-sc-modules-0.14.1 (c (n "dharitri-sc-modules") (v "0.14.1") (d (list (d (n "dharitri-sc") (r "^0.14.1") (d #t) (k 0)))) (h "1d39axj97b8cj4srwmp4i6jg6m1dd7qhl6wjnq5zz1qgbap2mc2i") (f (quote (("alloc" "dharitri-sc/alloc")))) (y #t)))

