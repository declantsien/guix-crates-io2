(define-module (crates-io dh ar dharitri-wmoax-swap-sc) #:use-module (crates-io))

(define-public crate-dharitri-wmoax-swap-sc-0.0.1 (c (n "dharitri-wmoax-swap-sc") (v "0.0.1") (d (list (d (n "dharitri-sc") (r "^0.0.1") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.0.1") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.0.1") (d #t) (k 2)))) (h "1p5q8izf9bvp9rzry5yadgr9nmmcf0vmxw5iq5khhjdv4gskdcx9")))

(define-public crate-dharitri-wmoax-swap-sc-0.0.2 (c (n "dharitri-wmoax-swap-sc") (v "0.0.2") (d (list (d (n "dharitri-sc") (r "^0.0.2") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.0.2") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.0.2") (d #t) (k 2)))) (h "18zyfy1fmdq2c2mxwl6y463jx5vkvl635pl0wzkmjkhdf6fq29ic")))

(define-public crate-dharitri-wmoax-swap-sc-0.0.3 (c (n "dharitri-wmoax-swap-sc") (v "0.0.3") (d (list (d (n "dharitri-sc") (r "^0.0.3") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.0.3") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.0.3") (d #t) (k 2)))) (h "05vdmdyk912i5qscf8kj79cl9jqbk3rs0fbbi97rxcy1i2zc4j5k")))

(define-public crate-dharitri-wmoax-swap-sc-0.0.4 (c (n "dharitri-wmoax-swap-sc") (v "0.0.4") (d (list (d (n "dharitri-sc") (r "^0.0.4") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.0.4") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.0.4") (d #t) (k 2)))) (h "0hkbzm7ypfalhaszbzbp8n0nxinihsxa4w3iy1sgi7q78l3162vp")))

(define-public crate-dharitri-wmoax-swap-sc-0.0.5 (c (n "dharitri-wmoax-swap-sc") (v "0.0.5") (d (list (d (n "dharitri-sc") (r "^0.0.5") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.0.5") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.0.5") (d #t) (k 2)))) (h "1bf3pf6vz9a8cwrr0bkk8q89dmg6map4i6a48jp6v1m23k4xgbw1")))

(define-public crate-dharitri-wmoax-swap-sc-0.0.6 (c (n "dharitri-wmoax-swap-sc") (v "0.0.6") (d (list (d (n "dharitri-sc") (r "^0.0.6") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.0.6") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.0.6") (d #t) (k 2)))) (h "063ragyyyjs01k90f79461wwzjfid81znx3hk4pgxq3yzzrlp3ps")))

(define-public crate-dharitri-wmoax-swap-sc-0.0.7 (c (n "dharitri-wmoax-swap-sc") (v "0.0.7") (d (list (d (n "dharitri-sc") (r "^0.0.7") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.0.7") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.0.7") (d #t) (k 2)))) (h "1iqr4njsajzq6svz9lig4cbm2fzj7sa3r08jrpxzmwvi7wqf9xwb")))

(define-public crate-dharitri-wmoax-swap-sc-0.0.8 (c (n "dharitri-wmoax-swap-sc") (v "0.0.8") (d (list (d (n "dharitri-sc") (r "^0.0.8") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.0.8") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.0.8") (d #t) (k 2)))) (h "1vszl53nwi9mb2720v3mximiic777d01ahqd018468cl2219499g")))

(define-public crate-dharitri-wmoax-swap-sc-0.0.9 (c (n "dharitri-wmoax-swap-sc") (v "0.0.9") (d (list (d (n "dharitri-sc") (r "^0.0.9") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.0.9") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.0.9") (d #t) (k 2)))) (h "1bqpm35yx3nyp1xy3hrvgwr9ar7yar3399fjrk3x4x4s343fd99k")))

(define-public crate-dharitri-wmoax-swap-sc-0.1.0 (c (n "dharitri-wmoax-swap-sc") (v "0.1.0") (d (list (d (n "dharitri-sc") (r "^0.1.0") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.1.0") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.1.0") (d #t) (k 2)))) (h "0d4fyjrak149r8c48qaspawclal3xlzlaqzayckajv6fp9rq9npb")))

(define-public crate-dharitri-wmoax-swap-sc-0.1.1 (c (n "dharitri-wmoax-swap-sc") (v "0.1.1") (d (list (d (n "dharitri-sc") (r "^0.1.1") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.1.1") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.1.1") (d #t) (k 2)))) (h "0v0wmgpp5xmcdi8ymi4wnjsik4479xcnwginjxd5nbp47nhzp3kq")))

(define-public crate-dharitri-wmoax-swap-sc-0.1.2 (c (n "dharitri-wmoax-swap-sc") (v "0.1.2") (d (list (d (n "dharitri-sc") (r "^0.1.2") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.1.2") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.1.2") (d #t) (k 2)))) (h "01rndmcpvp4kd1c3bq3qh6cxkdsjvnvdcs9rjdg1jlgij7v0jrbl")))

(define-public crate-dharitri-wmoax-swap-sc-0.1.3 (c (n "dharitri-wmoax-swap-sc") (v "0.1.3") (d (list (d (n "dharitri-sc") (r "^0.1.3") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.1.3") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.1.3") (d #t) (k 2)))) (h "1rcfgshsy8jpq1s3pkch0q4xs2xk1zmvvigv78zl1dc6bpairbyi")))

(define-public crate-dharitri-wmoax-swap-sc-0.1.4 (c (n "dharitri-wmoax-swap-sc") (v "0.1.4") (d (list (d (n "dharitri-sc") (r "^0.1.4") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.1.4") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.1.4") (d #t) (k 2)))) (h "1j0g8spdi9kipn94sdwnqyd9cmxl3h7qfjsw3cdx6zs4b50vmdj9")))

(define-public crate-dharitri-wmoax-swap-sc-0.1.5 (c (n "dharitri-wmoax-swap-sc") (v "0.1.5") (d (list (d (n "dharitri-sc") (r "^0.1.5") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.1.5") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.1.5") (d #t) (k 2)))) (h "0s1r5m26y9jflrya9dp61xqi3z1mhnryaadkcf8vdazhsqr0xv35")))

(define-public crate-dharitri-wmoax-swap-sc-0.1.6 (c (n "dharitri-wmoax-swap-sc") (v "0.1.6") (d (list (d (n "dharitri-sc") (r "^0.1.6") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.1.6") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.1.6") (d #t) (k 2)))) (h "0f666cyip56sj373rpq79b9z69564ynb76104znq2k11ccc5mn6f")))

(define-public crate-dharitri-wmoax-swap-sc-0.1.7 (c (n "dharitri-wmoax-swap-sc") (v "0.1.7") (d (list (d (n "dharitri-sc") (r "^0.1.7") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.1.7") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.1.7") (d #t) (k 2)))) (h "0wyb49jykf645x47qpknd4mk721m9dncvzhcv48vh2x2p93g0bam")))

(define-public crate-dharitri-wmoax-swap-sc-0.1.8 (c (n "dharitri-wmoax-swap-sc") (v "0.1.8") (d (list (d (n "dharitri-sc") (r "^0.1.8") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.1.8") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.1.8") (d #t) (k 2)))) (h "0a6v0cg6a035mnldsc7bs0b11izgnh47lmlj2qadc1vl746p5jwm")))

(define-public crate-dharitri-wmoax-swap-sc-0.1.9 (c (n "dharitri-wmoax-swap-sc") (v "0.1.9") (d (list (d (n "dharitri-sc") (r "^0.1.9") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.1.9") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.1.9") (d #t) (k 2)))) (h "1fng39i4jv5qjmb8jxrv0d9h4anwxvkxz40i0ckp9k72yiq4hf4q")))

(define-public crate-dharitri-wmoax-swap-sc-0.2.0 (c (n "dharitri-wmoax-swap-sc") (v "0.2.0") (d (list (d (n "dharitri-sc") (r "^0.2.0") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.2.0") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.2.0") (d #t) (k 2)))) (h "1md4yx7fn6jiimhmglwzyrwm98ib09747f10m6h09fv00p8vywhd")))

(define-public crate-dharitri-wmoax-swap-sc-0.10.9 (c (n "dharitri-wmoax-swap-sc") (v "0.10.9") (d (list (d (n "dharitri-sc") (r "^0.10.9") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.10.9") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.10.9") (d #t) (k 2)))) (h "0h54y9knyd1ffzl685v1zwb5f8lnmpvcksq44fms97f8y1rgcr8q")))

(define-public crate-dharitri-wmoax-swap-sc-0.11.0 (c (n "dharitri-wmoax-swap-sc") (v "0.11.0") (d (list (d (n "dharitri-sc") (r "^0.11.0") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.11.0") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.11.0") (d #t) (k 2)))) (h "0ghh4p96hdzbyvihwlx4lj93l0wf0gm0rsg7jkcfyh1lgbz4xmqc")))

(define-public crate-dharitri-wmoax-swap-sc-0.11.1 (c (n "dharitri-wmoax-swap-sc") (v "0.11.1") (d (list (d (n "dharitri-sc") (r "^0.11.1") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.11.1") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.11.1") (d #t) (k 2)))) (h "0zrgl79idjj3kgw90jy8i66qh31kv8fxf9sh5agsmmljncmadq2r")))

(define-public crate-dharitri-wmoax-swap-sc-0.11.2 (c (n "dharitri-wmoax-swap-sc") (v "0.11.2") (d (list (d (n "dharitri-sc") (r "^0.11.2") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.11.2") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.11.2") (d #t) (k 2)))) (h "0dlzdkjpvgcj63pni4s0yg97ivv4xcbk6mhpdj8gxfhgwrswjj15")))

(define-public crate-dharitri-wmoax-swap-sc-0.11.3 (c (n "dharitri-wmoax-swap-sc") (v "0.11.3") (d (list (d (n "dharitri-sc") (r "^0.11.3") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.11.3") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.11.3") (d #t) (k 2)))) (h "1cd54k9p1k9pglmn1nizgr7az3zxd0s043sn7yis7zbg8bi4ba64")))

(define-public crate-dharitri-wmoax-swap-sc-0.11.4 (c (n "dharitri-wmoax-swap-sc") (v "0.11.4") (d (list (d (n "dharitri-sc") (r "^0.11.4") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.11.4") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.11.4") (d #t) (k 2)))) (h "14hyd1359xjh6ca2v3bxpf2vx4n9bb5mmqrb0gpn24wc4v81mqss")))

(define-public crate-dharitri-wmoax-swap-sc-0.11.5 (c (n "dharitri-wmoax-swap-sc") (v "0.11.5") (d (list (d (n "dharitri-sc") (r "^0.11.5") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.11.5") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.11.5") (d #t) (k 2)))) (h "1964m4v79lq506jssc7mq1xpmccx3ivk52qwk9iaa99a52q32jsy")))

(define-public crate-dharitri-wmoax-swap-sc-0.11.6 (c (n "dharitri-wmoax-swap-sc") (v "0.11.6") (d (list (d (n "dharitri-sc") (r "^0.11.6") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.11.6") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.11.6") (d #t) (k 2)))) (h "0nx3qvp8v2qsf26k73mccrw550a8s4sm66l65bmdvj2d4400jnps")))

(define-public crate-dharitri-wmoax-swap-sc-0.11.7 (c (n "dharitri-wmoax-swap-sc") (v "0.11.7") (d (list (d (n "dharitri-sc") (r "^0.11.7") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.11.7") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.11.7") (d #t) (k 2)))) (h "1k7p4m2f8zjlqqxnr1m4hmkbi1yjzdhbf3cp8m1x48xxz0bqawkc")))

(define-public crate-dharitri-wmoax-swap-sc-0.11.8 (c (n "dharitri-wmoax-swap-sc") (v "0.11.8") (d (list (d (n "dharitri-sc") (r "^0.11.8") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.11.8") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.11.8") (d #t) (k 2)))) (h "0hrbjmwxa9z8wq1sh7mwp82vv8wj8y45x0vqxbl2asg02kyyd9w0")))

(define-public crate-dharitri-wmoax-swap-sc-0.11.9 (c (n "dharitri-wmoax-swap-sc") (v "0.11.9") (d (list (d (n "dharitri-sc") (r "^0.11.9") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.11.9") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.11.9") (d #t) (k 2)))) (h "013q5bv39xp1p11lfhiap09nqgmzdd9r6fn5i1z52bvd6ymlyrsp")))

(define-public crate-dharitri-wmoax-swap-sc-0.12.0 (c (n "dharitri-wmoax-swap-sc") (v "0.12.0") (d (list (d (n "dharitri-sc") (r "^0.12.0") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.12.0") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.12.0") (d #t) (k 2)))) (h "08fsn2rscn8alzc91kz9gr66x3ljam3d1gz9mxiiw31b3023lr74")))

(define-public crate-dharitri-wmoax-swap-sc-0.12.1 (c (n "dharitri-wmoax-swap-sc") (v "0.12.1") (d (list (d (n "dharitri-sc") (r "^0.12.1") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.12.1") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.12.1") (d #t) (k 2)))) (h "0c6llhw62vbwzp8y7yf4hlkq6p26iwg98jkwfw99hqpim847j6v9")))

(define-public crate-dharitri-wmoax-swap-sc-0.12.2 (c (n "dharitri-wmoax-swap-sc") (v "0.12.2") (d (list (d (n "dharitri-sc") (r "^0.12.2") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.12.2") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.12.2") (d #t) (k 2)))) (h "0wpq3q8pza75p83c6c460gxwyjj3hjhn6x2h10wm75sy86ymnab9")))

(define-public crate-dharitri-wmoax-swap-sc-0.12.3 (c (n "dharitri-wmoax-swap-sc") (v "0.12.3") (d (list (d (n "dharitri-sc") (r "^0.12.3") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.12.3") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.12.3") (d #t) (k 2)))) (h "14v03ar4cmp40d8i4knys0nv6k3nb5yfzr7aqlbgaw98klgvp50c")))

(define-public crate-dharitri-wmoax-swap-sc-0.12.4 (c (n "dharitri-wmoax-swap-sc") (v "0.12.4") (d (list (d (n "dharitri-sc") (r "^0.12.4") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.12.4") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.12.4") (d #t) (k 2)))) (h "0ml1c73xp92hk8h9hs6jn4wbz7mjvdhl43mqc2wbnn0c9644xvff")))

(define-public crate-dharitri-wmoax-swap-sc-0.12.5 (c (n "dharitri-wmoax-swap-sc") (v "0.12.5") (d (list (d (n "dharitri-sc") (r "^0.12.5") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.12.5") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.12.5") (d #t) (k 2)))) (h "1dxdb5c75b8gy4qyad19pnrihizzv52xjinf80zdhji474w0wq30")))

(define-public crate-dharitri-wmoax-swap-sc-0.12.6 (c (n "dharitri-wmoax-swap-sc") (v "0.12.6") (d (list (d (n "dharitri-sc") (r "^0.12.6") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.12.6") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.12.6") (d #t) (k 2)))) (h "0vqmkqgdj3gsrx3zwqb54jzkhns2zql0sawbv3w66893h0ngi16h")))

(define-public crate-dharitri-wmoax-swap-sc-0.12.7 (c (n "dharitri-wmoax-swap-sc") (v "0.12.7") (d (list (d (n "dharitri-sc") (r "^0.12.7") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.12.7") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.12.7") (d #t) (k 2)))) (h "14b1qhflyq40zq2zbk2n8mxyqc7mm0v073c6v6yjsxa0hpag6060")))

(define-public crate-dharitri-wmoax-swap-sc-0.12.8 (c (n "dharitri-wmoax-swap-sc") (v "0.12.8") (d (list (d (n "dharitri-sc") (r "^0.12.8") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.12.8") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.12.8") (d #t) (k 2)))) (h "0p8ys08kh3v81y8k320sc1hblj3zzlj5pkm3pbvjg5i7zj2nabkh")))

(define-public crate-dharitri-wmoax-swap-sc-0.12.9 (c (n "dharitri-wmoax-swap-sc") (v "0.12.9") (d (list (d (n "dharitri-sc") (r "^0.12.9") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.12.9") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.12.9") (d #t) (k 2)))) (h "18d3dhl0aydi5aaxsklw7wcdyi3nwb2asmmx9krqldw8kpq8l04w")))

(define-public crate-dharitri-wmoax-swap-sc-0.13.0 (c (n "dharitri-wmoax-swap-sc") (v "0.13.0") (d (list (d (n "dharitri-sc") (r "^0.13.0") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.13.0") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.13.0") (d #t) (k 2)))) (h "1rvmr9qwiwp45glg46dqvmx9ysik2l4n2gfilwr5lhhaj3gwsbwm")))

(define-public crate-dharitri-wmoax-swap-sc-0.13.1 (c (n "dharitri-wmoax-swap-sc") (v "0.13.1") (d (list (d (n "dharitri-sc") (r "^0.13.1") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.13.1") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.13.1") (d #t) (k 2)))) (h "14l7f9859lrz5z432kpw9frnn1ldr3i8bwf1m9crg750gwa9i608")))

(define-public crate-dharitri-wmoax-swap-sc-0.13.2 (c (n "dharitri-wmoax-swap-sc") (v "0.13.2") (d (list (d (n "dharitri-sc") (r "^0.13.2") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.13.2") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.13.2") (d #t) (k 2)))) (h "13q89sbsafcsi8j70vvpjqhx4b15i41cvvqx4ns8x7bc6jkdy728")))

(define-public crate-dharitri-wmoax-swap-sc-0.13.3 (c (n "dharitri-wmoax-swap-sc") (v "0.13.3") (d (list (d (n "dharitri-sc") (r "^0.13.3") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.13.3") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.13.3") (d #t) (k 2)))) (h "0bpya56p3icsjlgp6ipfscffwq72aa0h8mxvg1d9k478jlhfjc4q")))

(define-public crate-dharitri-wmoax-swap-sc-0.13.4 (c (n "dharitri-wmoax-swap-sc") (v "0.13.4") (d (list (d (n "dharitri-sc") (r "^0.13.4") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.13.4") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.13.4") (d #t) (k 2)))) (h "0aw5v9fi8wh21w8dfb9gd9f7by8q1qpxp45b0pl8vsfs07y3sipk")))

(define-public crate-dharitri-wmoax-swap-sc-0.13.5 (c (n "dharitri-wmoax-swap-sc") (v "0.13.5") (d (list (d (n "dharitri-sc") (r "^0.13.5") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.13.5") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.13.5") (d #t) (k 2)))) (h "1xgkak2m2zl7rjqy8ayffhqjsns8scsr0spsfq9ybfazq5by39s3")))

(define-public crate-dharitri-wmoax-swap-sc-0.13.6 (c (n "dharitri-wmoax-swap-sc") (v "0.13.6") (d (list (d (n "dharitri-sc") (r "^0.13.6") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.13.6") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.13.6") (d #t) (k 2)))) (h "0knilll8jfq3d33hyhfnk8jrl11bl29njxfna3bzdj2m84nza699")))

(define-public crate-dharitri-wmoax-swap-sc-0.13.7 (c (n "dharitri-wmoax-swap-sc") (v "0.13.7") (d (list (d (n "dharitri-sc") (r "^0.13.7") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.13.7") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.13.7") (d #t) (k 2)))) (h "1878scndfgdyib6xq3h24m6k848153lg3zqlrhl6xhldq1k4ml4v")))

(define-public crate-dharitri-wmoax-swap-sc-0.13.8 (c (n "dharitri-wmoax-swap-sc") (v "0.13.8") (d (list (d (n "dharitri-sc") (r "^0.13.8") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.13.8") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.13.8") (d #t) (k 2)))) (h "1sd178xnfd8y01hsr2n4l4spa16aldpwz3pdjxhxg7vbay4qypc6")))

(define-public crate-dharitri-wmoax-swap-sc-0.13.9 (c (n "dharitri-wmoax-swap-sc") (v "0.13.9") (d (list (d (n "dharitri-sc") (r "^0.13.9") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.13.9") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.13.9") (d #t) (k 2)))) (h "1gbd2xyz2c9xfd6g5q4pl0hmzlj8c15kfgk770qkczr02c5f22h1")))

(define-public crate-dharitri-wmoax-swap-sc-0.14.0 (c (n "dharitri-wmoax-swap-sc") (v "0.14.0") (d (list (d (n "dharitri-sc") (r "^0.14.0") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.14.0") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.14.0") (d #t) (k 2)))) (h "0xdfyn7z3ngka0yi7715a32ys30a58pdjdrq0y2g3a7al72rcqx4")))

(define-public crate-dharitri-wmoax-swap-sc-0.14.1 (c (n "dharitri-wmoax-swap-sc") (v "0.14.1") (d (list (d (n "dharitri-sc") (r "^0.14.1") (d #t) (k 0)) (d (n "dharitri-sc-modules") (r "^0.14.1") (d #t) (k 0)) (d (n "dharitri-sc-scenario") (r "^0.14.1") (d #t) (k 2)))) (h "1m31y0n0f5lp2bf7848pyrgc9m2aib4w080pd9mglhgwhz83gfzi")))

