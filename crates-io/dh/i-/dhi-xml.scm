(define-module (crates-io dh i- dhi-xml) #:use-module (crates-io))

(define-public crate-dhi-xml-0.1.1 (c (n "dhi-xml") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "01idc5xv1mlabc0ghidd50n2jz6gpg9dj8y4d5d2vblazs5p4538")))

(define-public crate-dhi-xml-0.1.2 (c (n "dhi-xml") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0n5q8a10adgxfdyzvhf38gjf7a6fgkk5flqd82h9ay07glhwynsc")))

(define-public crate-dhi-xml-0.1.3 (c (n "dhi-xml") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1rwjydhch2hgkacpdjqn7qnaqysdr9va8f71g20ln3afmm6zd0pn")))

