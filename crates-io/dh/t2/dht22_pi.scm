(define-module (crates-io dh t2 dht22_pi) #:use-module (crates-io))

(define-public crate-dht22_pi-0.1.0 (c (n "dht22_pi") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "rppal") (r "^0.1.2") (d #t) (k 0)))) (h "17abwi2cdcz4g7p865af5ikpdkj26hflxnndnxsp568p3mp0d85d")))

(define-public crate-dht22_pi-0.2.0 (c (n "dht22_pi") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1n447r5q5sxr3y7xh7z0bxcv9i6cbwbz4dcm6m8939m2cqijl4b4")))

(define-public crate-dht22_pi-0.3.0 (c (n "dht22_pi") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "05l2xwam61z6pdpm8c114bdq1nzcw4i3z3qqmr27v5hqlf5vm3n9")))

(define-public crate-dht22_pi-1.0.0 (c (n "dht22_pi") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "rppal") (r "^0.13.1") (d #t) (k 0)))) (h "1k2ac84xd95zq3nfix3wphjs9l0czhfbzb3f4g0ay80pfz4482sz")))

