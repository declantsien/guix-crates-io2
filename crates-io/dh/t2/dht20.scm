(define-module (crates-io dh t2 dht20) #:use-module (crates-io))

(define-public crate-dht20-0.1.0 (c (n "dht20") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "18vcf9b9x602fybw5m3p9a9chvmxr2fnj4bjdg4c0hfrzhpaka3v")))

