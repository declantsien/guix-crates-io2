(define-module (crates-io dh t2 dht22-driver) #:use-module (crates-io))

(define-public crate-dht22-driver-0.1.0 (c (n "dht22-driver") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)))) (h "01m58nswmikwpndnv6cgv6a05rdb579wqv229rvihjlg7ly68acf") (f (quote (("std") ("default" "critical-section")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-dht22-driver-0.1.1 (c (n "dht22-driver") (v "0.1.1") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)))) (h "0mq1dl5wrlbbq87haf3l150380p5200d4xs112x90g5f5jz94n5d") (f (quote (("std") ("default" "critical-section")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

