(define-module (crates-io dh re dhref) #:use-module (crates-io))

(define-public crate-dhref-0.1.0 (c (n "dhref") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "nipper") (r "^0.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gnj97qx2plm2k32q0id8ziv4sdw32rgwha467q874sw17q8bais")))

(define-public crate-dhref-0.1.1 (c (n "dhref") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "nipper") (r "^0.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0svafyrj02inbnwzwpgr77clkmbw1mm8bssszjc1ir3d7w6ckksi")))

(define-public crate-dhref-0.1.2 (c (n "dhref") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "nipper") (r "^0.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vbqkz6fnqrnx9nrannyqaf80hap9773dgiyfna4w7b05srz2mys")))

(define-public crate-dhref-0.2.0 (c (n "dhref") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "nipper") (r "^0.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vasyivzs9b3cngrs4lm8ifm4bpi5fkbwmfny27wrwddwqq68396")))

