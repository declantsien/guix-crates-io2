(define-module (crates-io dh t- dht-sensor) #:use-module (crates-io))

(define-public crate-dht-sensor-0.1.0 (c (n "dht-sensor") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.1") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.11") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32f0xx-hal") (r "^0.15.2") (f (quote ("stm32f042"))) (d #t) (k 2)))) (h "1rj6k9hflyzjqv0wbbwsjdxd9zh0wb7dapwzykxjyyxh025wfy72")))

(define-public crate-dht-sensor-0.1.1 (c (n "dht-sensor") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.1") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.11") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32f0xx-hal") (r "^0.15.2") (f (quote ("stm32f042"))) (d #t) (k 2)))) (h "0r4199qrxz9bzsql281vn17ql7xlbps0d4gv89sc9v0qjc7si7ln")))

(define-public crate-dht-sensor-0.2.0 (c (n "dht-sensor") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32f0xx-hal") (r "^0.17.1") (f (quote ("stm32f042"))) (d #t) (k 2)))) (h "1hwk6zxhx0k27bwj0gsf9q1wd17pkgyzw2y1dhz8qfvsgdadq69f")))

(define-public crate-dht-sensor-0.2.1 (c (n "dht-sensor") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.6.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32f0xx-hal") (r "^0.17.1") (f (quote ("stm32f042"))) (d #t) (k 2)))) (h "0a6yf0sm8pp63c0wgwgsjprw5z2wxjzynlvixbd04krj9y2l7mkl")))

