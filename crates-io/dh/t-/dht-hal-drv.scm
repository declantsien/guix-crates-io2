(define-module (crates-io dh t- dht-hal-drv) #:use-module (crates-io))

(define-public crate-dht-hal-drv-0.1.0 (c (n "dht-hal-drv") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "05caaqmwf54gmnpipq2759y727cxvw0zxff17c7zjlabip0kdnwv")))

(define-public crate-dht-hal-drv-0.2.0 (c (n "dht-hal-drv") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "08mjq1s6llnizrmrfzvrf2f9lczpj5p1a0ca46gdkbxzxqjc2ibd")))

(define-public crate-dht-hal-drv-0.2.1 (c (n "dht-hal-drv") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "15d7bp863gkqidkj05pylydr9k9ii64402b1j3az127parbsqyfr")))

(define-public crate-dht-hal-drv-0.2.2 (c (n "dht-hal-drv") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1bmlqw74rk11m5iy74fmwaad4f1ndfhyhd0ikc858pj0wgkiix2h")))

