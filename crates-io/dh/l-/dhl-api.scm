(define-module (crates-io dh l- dhl-api) #:use-module (crates-io))

(define-public crate-dhl-api-0.0.1 (c (n "dhl-api") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "10kb244mwg8rjxnfn10anmyvmij95pl2zcm5npsnkn7mnkkxfy13") (y #t)))

(define-public crate-dhl-api-0.0.2 (c (n "dhl-api") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1lgb4miz3a8jf7sqmsrr974b9j31vxmwywj3ynjq46l9y75ng64i")))

