(define-module (crates-io dh l_ dhl_tracking) #:use-module (crates-io))

(define-public crate-dhl_tracking-0.1.0 (c (n "dhl_tracking") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0n7s7s8fgwl0dwvv0ah19d5694m37xwjj8lywppavfha02q5g7z3")))

(define-public crate-dhl_tracking-0.1.1 (c (n "dhl_tracking") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1249yif2jf652rq145hjwzcajpmr8q040yf5540qls6idq5j61za")))

(define-public crate-dhl_tracking-0.1.2 (c (n "dhl_tracking") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "14fw1w251dbqb5wks2b3rqdqqnhnmjf645hhwpx020yh38rlki81")))

(define-public crate-dhl_tracking-0.1.4 (c (n "dhl_tracking") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0zg6a33wpknlfnpc5v9g54y413dmz9cri52hjib0avizf47sxzcz")))

