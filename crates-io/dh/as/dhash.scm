(define-module (crates-io dh as dhash) #:use-module (crates-io))

(define-public crate-dhash-0.1.0 (c (n "dhash") (v "0.1.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0gs733zbyrcfvkgkzr7g5cc19a4f3xvacf5bp7djsk3vm987zh1v")))

