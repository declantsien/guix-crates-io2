(define-module (crates-io dh al dhall_proc_macros) #:use-module (crates-io))

(define-public crate-dhall_proc_macros-0.1.0 (c (n "dhall_proc_macros") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0c55zxna01jpvv2bd0gzgvdikmbpgvbhz0bgyri9fwpfs92czsr4")))

(define-public crate-dhall_proc_macros-0.4.0 (c (n "dhall_proc_macros") (v "0.4.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "190r5078q12wyqw9i3r3h7ywqb21l4rnrj0gjm4wrvnh5yi0hi93")))

(define-public crate-dhall_proc_macros-0.5.0 (c (n "dhall_proc_macros") (v "0.5.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "02azji8s2ha2mljvmacxzk4pgj0xn21fkkg0a8b8a0yx5lggyv5z")))

(define-public crate-dhall_proc_macros-0.5.1 (c (n "dhall_proc_macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1j92yg599w99m8mrq72ysrbkx80sb8aa35vw3lp3w8lv3psacjzn")))

(define-public crate-dhall_proc_macros-0.6.0 (c (n "dhall_proc_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1gs6a6h85qy8dh86hhdb6y4zk230phrfyz5h63srx1vhd38q2z6z")))

(define-public crate-dhall_proc_macros-0.6.1 (c (n "dhall_proc_macros") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0lsn8wsrjsxwwk90dv7571i9cmb2kfaw6niyhk6j2aw0pwlb5kgg")))

