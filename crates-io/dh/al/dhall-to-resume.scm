(define-module (crates-io dh al dhall-to-resume) #:use-module (crates-io))

(define-public crate-dhall-to-resume-0.1.0 (c (n "dhall-to-resume") (v "0.1.0") (d (list (d (n "serde_dhall") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1bkbvk6n7mr80yhb8m1p43pkib807h2zfmiw4217wjkn70p6mzyn")))

(define-public crate-dhall-to-resume-0.1.1 (c (n "dhall-to-resume") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1p5hgzfmh657bfv8zih9c6iimwfvvs2vjjgwizfb21cpyj643fk2")))

(define-public crate-dhall-to-resume-0.1.2 (c (n "dhall-to-resume") (v "0.1.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "09ldr4vqvw09s443wx06187a8qd03nj7xidvcagx32760pbldrpa")))

(define-public crate-dhall-to-resume-0.1.3 (c (n "dhall-to-resume") (v "0.1.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "111mascs111hi00xma9svrjpnhg2y8vp4hl3hwl5r74fm09ggfhs")))

(define-public crate-dhall-to-resume-0.1.5 (c (n "dhall-to-resume") (v "0.1.5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "05balcapz6kqgqyvrnwgk10zw5y9qavr8w1lyl0r8m5np3pms3sl")))

(define-public crate-dhall-to-resume-0.1.6 (c (n "dhall-to-resume") (v "0.1.6") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "16pwfxrl441vhdh1262j274936kpd0v72b8xbg5ysz5qdd56k6zp")))

(define-public crate-dhall-to-resume-0.1.7 (c (n "dhall-to-resume") (v "0.1.7") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "195mwazd58j740a9mx2ah1764qj3bjikwz63bss4inyi90wbkpvn")))

(define-public crate-dhall-to-resume-0.1.8 (c (n "dhall-to-resume") (v "0.1.8") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "1zqgzsmqs87p88d35qgsih2ly7i427cmp2i7c7sbzg431gm2vf3x") (y #t)))

(define-public crate-dhall-to-resume-0.1.9 (c (n "dhall-to-resume") (v "0.1.9") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "0xdhbpbw3qlz07nb2xf17yc9xnd4hnjaznkh5kvhprlinsw9wnpm")))

(define-public crate-dhall-to-resume-0.1.10 (c (n "dhall-to-resume") (v "0.1.10") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "0whx7d2qylrzv9snzarc0p66x3ds0r0bk321rmnalnw73k1jyyvw")))

(define-public crate-dhall-to-resume-0.1.11 (c (n "dhall-to-resume") (v "0.1.11") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "00ffmjihsk6hnbjxzni9inh5plh53nhgyjq7kg70m6ki1mazxpls")))

