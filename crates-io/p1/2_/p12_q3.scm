(define-module (crates-io p1 #{2_}# p12_q3) #:use-module (crates-io))

(define-public crate-p12_q3-0.7.0 (c (n "p12_q3") (v "0.7.0") (d (list (d (n "cbc") (r "^0.1") (f (quote ("block-padding"))) (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("alloc" "block-padding"))) (d #t) (k 0)) (d (n "des") (r "^0.8") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rc2") (r "^0.8") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "yasna") (r "^0.5") (f (quote ("std"))) (d #t) (k 0)))) (h "1vmfy1p4k7swbgn6g3hhq0lls20k18007jafinjykyrrjhzfqhxn") (r "1.57.0")))

