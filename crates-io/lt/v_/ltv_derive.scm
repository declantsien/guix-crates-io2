(define-module (crates-io lt v_ ltv_derive) #:use-module (crates-io))

(define-public crate-ltv_derive-0.1.0 (c (n "ltv_derive") (v "0.1.0") (d (list (d (n "ltv") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1qj2rx6y4dbyr6hladi5316c7ndxfyraapbigv89bg53m2ns14dz")))

(define-public crate-ltv_derive-0.1.2 (c (n "ltv_derive") (v "0.1.2") (d (list (d (n "ltv") (r "^0.1") (d #t) (k 2)) (d (n "ltv_derive_impl") (r "^0.1") (d #t) (k 0)))) (h "0axmb6mckwb8va6z4rl84xg6dzbhh9h0x8iqnbyh68ml8m5cmxad")))

(define-public crate-ltv_derive-0.1.3 (c (n "ltv_derive") (v "0.1.3") (d (list (d (n "ltv") (r "^0.1") (d #t) (k 2)) (d (n "ltv_derive_impl") (r "^0.1") (d #t) (k 0)))) (h "059fx405a7b8v5w8h4zwx6wcd5km353wk59qpim6sxp9wwr0r1s6")))

(define-public crate-ltv_derive-0.1.4 (c (n "ltv_derive") (v "0.1.4") (d (list (d (n "ltv") (r "^0.1") (d #t) (k 2)) (d (n "ltv_derive_impl") (r "^0.1") (d #t) (k 0)))) (h "05qijd9x8hn1ns2s6cpbr74rd355y76am92lal25gf6ykla41bbn")))

(define-public crate-ltv_derive-0.2.1 (c (n "ltv_derive") (v "0.2.1") (d (list (d (n "ltv") (r "^0.2") (d #t) (k 2)) (d (n "ltv_derive_impl") (r "^0.2") (d #t) (k 0)))) (h "18zy2qn446rzn950p1r11m28fhmxyfw1hxykq2lpz420b5qm4d5a")))

(define-public crate-ltv_derive-0.2.2 (c (n "ltv_derive") (v "0.2.2") (d (list (d (n "ltv") (r "^0.2") (d #t) (k 2)) (d (n "ltv_derive_impl") (r "^0.2.3") (d #t) (k 0)))) (h "0amqf4k6mnfil3dly09558ifsz0zbah82xrm9j8wn7bf1mj6kyxk")))

(define-public crate-ltv_derive-0.2.3 (c (n "ltv_derive") (v "0.2.3") (d (list (d (n "ltv") (r "^0.2.9") (d #t) (k 2)) (d (n "ltv_derive_impl") (r "^0.2.7") (d #t) (k 0)))) (h "0s6krwp4lacd942c3ij2pzd8cx6sfwh1nkj803hncr2v4v7gd4pq")))

(define-public crate-ltv_derive-0.2.4 (c (n "ltv_derive") (v "0.2.4") (d (list (d (n "ltv") (r "^0.2.11") (d #t) (k 2)) (d (n "ltv_derive_impl") (r "^0.2.8") (d #t) (k 0)))) (h "0aw44p5zmzn04z7n1ygy82qk7cl0hgi62y2bv6ilgz0llf1npqz2")))

(define-public crate-ltv_derive-0.2.5 (c (n "ltv_derive") (v "0.2.5") (d (list (d (n "ltv") (r "^0.2.15") (d #t) (k 2)) (d (n "ltv_derive_impl") (r "^0.2.9") (d #t) (k 0)))) (h "09hva5i8hlir078dlzsjnvcxi2zmbss26gihpy278mh2v5ngsd5i")))

(define-public crate-ltv_derive-0.2.6 (c (n "ltv_derive") (v "0.2.6") (d (list (d (n "ltv") (r "^0.2.15") (d #t) (k 2)) (d (n "ltv_derive_impl") (r "^0.2.10") (d #t) (k 0)))) (h "0xxb5fdd3w5im3b5khaa46vhx6aab3jdgkawajhcfalmv9b8qazv")))

(define-public crate-ltv_derive-0.2.7 (c (n "ltv_derive") (v "0.2.7") (d (list (d (n "ltv") (r "^0.2.15") (d #t) (k 2)) (d (n "ltv_derive_impl") (r "^0.2.11") (d #t) (k 0)))) (h "13biv08r4cwrc6sj1avxscxgcqy2wqk5d0zf82cq2c43gkn2glis")))

(define-public crate-ltv_derive-0.2.8 (c (n "ltv_derive") (v "0.2.8") (d (list (d (n "ltv") (r "^0.2.15") (d #t) (k 2)) (d (n "ltv_derive_impl") (r "^0.2.11") (d #t) (k 0)))) (h "0ig6rm6nwmw7vq4q2xh3jhzlwjwxy6ij0bmx8pmbyh9r7fv5i64j")))

