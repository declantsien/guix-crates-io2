(define-module (crates-io lt v_ ltv_derive_impl) #:use-module (crates-io))

(define-public crate-ltv_derive_impl-0.1.0 (c (n "ltv_derive_impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1zxfjys0f4kh4mj3fkrzvrbbkwzw9d48pkplawglwfcdvb8fxm7i")))

(define-public crate-ltv_derive_impl-0.1.1 (c (n "ltv_derive_impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0mqjgskqxzrmx4h3alyajhvk0xnixdf2qmq3ixr3a6fyhnbivp36")))

(define-public crate-ltv_derive_impl-0.1.2 (c (n "ltv_derive_impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0lmy9dxmgnpvjfki4p36zk7aza1im42cnywxj2qac65swxgjkb64")))

(define-public crate-ltv_derive_impl-0.2.0 (c (n "ltv_derive_impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1dbjh0b3l0p29v71qwfzrl2bnh4gi7nj5qjgb37pg4mm1xrpc81y")))

(define-public crate-ltv_derive_impl-0.2.1 (c (n "ltv_derive_impl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "16y997sidyn2fnni4chxvmd3xvyrd23kx0gq4x4db2sfwpivifkl")))

(define-public crate-ltv_derive_impl-0.2.2 (c (n "ltv_derive_impl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "15vr94kf0facim72qmyq8qxn0snkk37ybi8vqlmy15vyizjq6y1v")))

(define-public crate-ltv_derive_impl-0.2.3 (c (n "ltv_derive_impl") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1mj88g4nvg7r8gb5lvy9zjw8lq3wpr76zngp4waa6vdjfydmwlxx")))

(define-public crate-ltv_derive_impl-0.2.7 (c (n "ltv_derive_impl") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "056gsj9pilzgcvv3cxgw0d2dl5csbp1128cbz57la4g9l330nn4c")))

(define-public crate-ltv_derive_impl-0.2.8 (c (n "ltv_derive_impl") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "170v28a76y9i1qyvgljn1givzd68h62iwygba2rbzf9k3fjrs417")))

(define-public crate-ltv_derive_impl-0.2.9 (c (n "ltv_derive_impl") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "113h30lpjv0pn7snmm9bzdqwmkxgl6553xc93zr9wgrabcvxvkyn")))

(define-public crate-ltv_derive_impl-0.2.10 (c (n "ltv_derive_impl") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0i4086gr5d2xva9hzbql426fnz561nhvkxz77svb5v8mv9qfj712")))

(define-public crate-ltv_derive_impl-0.2.11 (c (n "ltv_derive_impl") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0w5vnyb2f3l81r7gk4f809xwwh9iigl0zf6fw680b9xhz5vmiqw1")))

