(define-module (crates-io lt pt ltptr) #:use-module (crates-io))

(define-public crate-ltptr-0.1.0 (c (n "ltptr") (v "0.1.0") (h "0qqlx1jd05hgbh1vg9ald0rp3p6mcpzjggjx2n5hvvlj4rxhasky") (f (quote (("std") ("default" "std"))))))

(define-public crate-ltptr-0.1.1 (c (n "ltptr") (v "0.1.1") (h "1lwxyg6r6r6l8czk1gdhichmrbwwab41k3r4vmgvhqbcrdbgdqfp") (f (quote (("std") ("default" "std"))))))

(define-public crate-ltptr-0.1.2 (c (n "ltptr") (v "0.1.2") (h "1ra91gknk75z9kdj0y50fj2czis2mks5mgz5i6ml7amicmhjcc2k") (f (quote (("std") ("default" "std"))))))

(define-public crate-ltptr-0.1.3 (c (n "ltptr") (v "0.1.3") (h "04w3mzjb2va2bgwj221zgf1a0vy4qxw0dim9b0agx7g2iljkrkck") (f (quote (("std") ("default" "std"))))))

(define-public crate-ltptr-0.1.4 (c (n "ltptr") (v "0.1.4") (h "0b84xyzcrwc87ygq54xj1b2x2fx81by09kr218i7nkqbnkbazssn") (f (quote (("std") ("default" "std"))))))

