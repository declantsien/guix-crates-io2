(define-module (crates-io lt xc ltxcut) #:use-module (crates-io))

(define-public crate-ltxcut-0.1.0 (c (n "ltxcut") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)))) (h "1d2nx9s2b1w8nqbihvamjc95acbpjkrx52lg2ivz4a39c2wfb1ml")))

(define-public crate-ltxcut-0.1.1 (c (n "ltxcut") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)))) (h "04kcrw8ww9771f5j21i8xw4gx471b76v8ry2jhhf3r7x9prayddz")))

