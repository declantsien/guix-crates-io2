(define-module (crates-io lt c6 ltc681x) #:use-module (crates-io))

(define-public crate-ltc681x-0.1.0 (c (n "ltc681x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)))) (h "1xpmwv7i42akl3xv521q8n7bdsf0zb3l10v5f1sw2av4hrqiv7v2") (f (quote (("strict") ("example") ("default" "example"))))))

(define-public crate-ltc681x-0.2.0 (c (n "ltc681x") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)))) (h "0zj97ypxyfyc2fry6r71nai911z4qgmx2xz353nqyk1mx66z3gb8") (f (quote (("strict") ("example") ("default" "example"))))))

(define-public crate-ltc681x-0.3.0 (c (n "ltc681x") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)))) (h "10qj430i57r1x0rwjphvcagpi8ng1zdaf2irrhm86nf9jl2hi0hk") (f (quote (("strict") ("example") ("default" "example"))))))

(define-public crate-ltc681x-0.3.1 (c (n "ltc681x") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)))) (h "1hg4712prmavydbnjyqr84rdgb377xrs2xbkmnl02f8xf89rn3jp") (f (quote (("strict") ("example") ("default" "example"))))))

(define-public crate-ltc681x-0.3.2 (c (n "ltc681x") (v "0.3.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)))) (h "077m1x5xdpxsqgvi7lkc8dxqjkz8snas66hrjpz17d87xlxrf87d") (f (quote (("strict") ("example") ("default" "example"))))))

(define-public crate-ltc681x-0.3.3 (c (n "ltc681x") (v "0.3.3") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)))) (h "1zkjja58piiybfkv4jxfrw5jdlqnyq3ny4fcbjk2vq4grl309w67") (f (quote (("strict") ("example") ("default" "example"))))))

(define-public crate-ltc681x-0.4.0 (c (n "ltc681x") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)))) (h "0zrs0b5xqzwgjljw1r2k21g1izfkc1vhyghkd0vaay9yam9rnxqf") (f (quote (("strict") ("example") ("default" "example"))))))

(define-public crate-ltc681x-0.4.1 (c (n "ltc681x") (v "0.4.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)))) (h "0imlvmk0yhw38hi51kyimvjxhvmah59m7cibwsbkg67ff0bw58wb") (f (quote (("strict") ("example") ("default" "example"))))))

(define-public crate-ltc681x-0.4.2 (c (n "ltc681x") (v "0.4.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)))) (h "1xczfiipn4f446h3dh3lmfnrncl266jsfvfvrpcvc7jhrah54rj5") (f (quote (("strict") ("example") ("default" "example"))))))

(define-public crate-ltc681x-0.5.0 (c (n "ltc681x") (v "0.5.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "fixed") (r "^1.15.0") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)))) (h "0s57pr64fvaf7cv0a62va53blynyq1mg6gfblfyjl3zgxhw219sq") (f (quote (("strict") ("example") ("default" "example"))))))

