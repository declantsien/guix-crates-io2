(define-module (crates-io lt r3 ltr303) #:use-module (crates-io))

(define-public crate-ltr303-0.1.0 (c (n "ltr303") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "num") (r "^0.4") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "0jzsbbqnm6x1lfcka4wfgypwl23x7ayxyga3gkfr4bsz0cs1s97k")))

(define-public crate-ltr303-0.1.1 (c (n "ltr303") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "num") (r "^0.4") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "05z2407bvc516rhbgsc4mp0iqm37qig76dr0344wbnpfvwf8lmjf")))

(define-public crate-ltr303-0.1.2 (c (n "ltr303") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "num") (r "^0.4") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "1fjgyhzc501ks0fdfjpmji7pyi29636zki948aqpn2smpcw8z526")))

(define-public crate-ltr303-0.1.3 (c (n "ltr303") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "num") (r "^0.4") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "19v8w0f2qnbbj8f8zci19rrnab0bq5lkzq2sv0j4ha5ixj5n2pia")))

(define-public crate-ltr303-0.1.4 (c (n "ltr303") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.4.0") (d #t) (k 2)) (d (n "num") (r "^0.4") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "0i3p4jkdlckyd6x7yqmz2d74dczarg7j48n1nwrd2vg6jdrpqnk0")))

