(define-module (crates-io lt r- ltr-559) #:use-module (crates-io))

(define-public crate-ltr-559-0.1.0 (c (n "ltr-559") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 2)))) (h "0w9vwlf3kmjygplz0x98773yjlwsdavjwalf5d6ph3vf7p30ldyv")))

(define-public crate-ltr-559-0.1.1 (c (n "ltr-559") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 2)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 2)))) (h "020f9ck35ff6zxyi4p6pqj22wswjga3gqrq4ihipwdr9a2d2a0f1")))

