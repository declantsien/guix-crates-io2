(define-module (crates-io lt tn lttng-ust-logging) #:use-module (crates-io))

(define-public crate-lttng-ust-logging-0.1.0 (c (n "lttng-ust-logging") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lttng-ust") (r "^0.1.0") (d #t) (k 0)) (d (n "lttng-ust-generate") (r "^0.1.1") (d #t) (k 1)))) (h "159paz11zjq1q9ixwd9dk22vbkgc3yg87lsjqdw53xnv0q6g7kf7")))

