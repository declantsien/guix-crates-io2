(define-module (crates-io lt tb lttb) #:use-module (crates-io))

(define-public crate-lttb-0.1.0 (c (n "lttb") (v "0.1.0") (h "1ljhvly71x4j2kwsj9iv9z77n0l8l7y6z67hdnxvj41wh760jbff")))

(define-public crate-lttb-0.1.1 (c (n "lttb") (v "0.1.1") (h "19pwi7pg1n6f00awr96n4mjb0ws5d7z2smz0z9i9xy23w26ah93v")))

(define-public crate-lttb-0.2.0 (c (n "lttb") (v "0.2.0") (h "0j141c1z5gbv87wb1qi7jr0z7cys7ppcgwcj215pn5h4686pbz5b")))

