(define-module (crates-io lt a_ lta_blocking) #:use-module (crates-io))

(define-public crate-lta_blocking-0.1.0 (c (n "lta_blocking") (v "0.1.0") (d (list (d (n "lta_models") (r "^0.1.0") (d #t) (k 0)) (d (n "lta_utils_commons") (r "^0.1.0") (d #t) (k 0)))) (h "0ja2abafzr6br53rx3q9l2ws0r6ry24ccyc9q19g4pcnkljqmlqi")))

(define-public crate-lta_blocking-0.2.0 (c (n "lta_blocking") (v "0.2.0") (d (list (d (n "lta_models") (r "^0.2.0") (d #t) (k 0)) (d (n "lta_utils_commons") (r "^0.2.0") (d #t) (k 0)))) (h "1s4sa1x5xjriq9byz7lmcsz8g4xrcbv9fh087m118id14axlkwrk")))

