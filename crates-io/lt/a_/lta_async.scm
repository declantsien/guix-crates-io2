(define-module (crates-io lt a_ lta_async) #:use-module (crates-io))

(define-public crate-lta_async-0.1.0 (c (n "lta_async") (v "0.1.0") (d (list (d (n "lta_models") (r "^0.1.0") (d #t) (k 0)) (d (n "lta_utils_commons") (r "^0.1.0") (d #t) (k 0)))) (h "1z3rnz47idrwsivk89fq7pqhgpz1nwnzm3b3ygb593l46mada7qc")))

(define-public crate-lta_async-0.2.0 (c (n "lta_async") (v "0.2.0") (d (list (d (n "lta_models") (r "^0.2.0") (d #t) (k 0)) (d (n "lta_utils_commons") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("macros"))) (d #t) (k 2)))) (h "0ib21rqvnmsq2nscqf3r20l2wsz0yh2jqnkcssbyn9wvlj2vhkmw")))

