(define-module (crates-io lt a_ lta_utils_commons) #:use-module (crates-io))

(define-public crate-lta_utils_commons-0.1.0 (c (n "lta_utils_commons") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "19k0shjyvmkq0hb55m4cahxgsn4b84sj0jc82nz8mfcjshd3jyv9")))

(define-public crate-lta_utils_commons-0.2.0 (c (n "lta_utils_commons") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "09ci3j5h4fk3yp2qdagy8kfng9ndnnblp48hgpnrsmm1f2x1zlqn")))

