(define-module (crates-io lt c2 ltc2983) #:use-module (crates-io))

(define-public crate-ltc2983-0.1.0 (c (n "ltc2983") (v "0.1.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.9") (d #t) (k 0)))) (h "1r7rpvpchy8w1xr65p4rx4y3agn1023jmbxyma2bjp9fqvrlfjsn")))

(define-public crate-ltc2983-0.1.1 (c (n "ltc2983") (v "0.1.1") (d (list (d (n "bytebuffer") (r "^2.1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.9") (d #t) (k 0)) (d (n "fixed") (r "^1.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "15jw0lxa4q8ii4jqha603fxx2dykhv05x4bms2kcx0z6bsixps80")))

(define-public crate-ltc2983-0.1.2 (c (n "ltc2983") (v "0.1.2") (d (list (d (n "bytebuffer") (r "^2.1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.9") (d #t) (k 0)) (d (n "fixed") (r "^1.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "17vczxd2amrhg55bjr11gznpf6nj92qazhphphpq0h7r8aq22rxa")))

(define-public crate-ltc2983-0.2.0 (c (n "ltc2983") (v "0.2.0") (d (list (d (n "bytebuffer") (r "^2.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.9") (d #t) (k 0)) (d (n "fixed") (r "^1.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0b94f0icn26sivwg28inlm8dadzj0bpagv8zlbgfypn45yydfwfb")))

