(define-module (crates-io lt c2 ltc2497) #:use-module (crates-io))

(define-public crate-ltc2497-0.1.0 (c (n "ltc2497") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)) (d (n "rppal") (r "^0.14.1") (f (quote ("hal"))) (d #t) (k 2)))) (h "1xq4afzhp3fs5qa636z5nncam34jdy2qhh371nllrmhhkf29j5yv")))

