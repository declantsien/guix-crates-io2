(define-module (crates-io vq f- vqf-rs) #:use-module (crates-io))

(define-public crate-vqf-rs-0.1.0 (c (n "vqf-rs") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)))) (h "07i81qzcynnrzaiqhgrmasncpzmsbx28myqb6ymhpixy2p604fwf") (f (quote (("std") ("motion-bias-estimation") ("f32") ("default" "std" "motion-bias-estimation")))) (s 2) (e (quote (("libm" "dep:libm"))))))

(define-public crate-vqf-rs-0.1.1 (c (n "vqf-rs") (v "0.1.1") (d (list (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)))) (h "14ij7j5dyd529y09r4lf0m5wmvq7zlc7vjz3dkjd4sgxh4vdp4ah") (f (quote (("std") ("motion-bias-estimation") ("f32") ("default" "std" "motion-bias-estimation")))) (s 2) (e (quote (("libm" "dep:libm"))))))

(define-public crate-vqf-rs-0.2.0 (c (n "vqf-rs") (v "0.2.0") (d (list (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "micromath") (r "^2.1.0") (o #t) (d #t) (k 0)))) (h "193yfdk0qxdkmx22iilk0n7k0d8yry94js1sn7sw5hkhfyvmii1y") (f (quote (("std") ("motion-bias-estimation") ("f32") ("default" "std" "motion-bias-estimation")))) (s 2) (e (quote (("micromath" "dep:micromath" "f32") ("libm" "dep:libm"))))))

