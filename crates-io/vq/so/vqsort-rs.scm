(define-module (crates-io vq so vqsort-rs) #:use-module (crates-io))

(define-public crate-vqsort-rs-0.1.0 (c (n "vqsort-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.98") (d #t) (k 1)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "system-deps") (r "^6.2.2") (d #t) (k 1)))) (h "0b94zabg0w8x4wv669w20d4bgn3hqajld5s92lampqgsby0qsccv")))

(define-public crate-vqsort-rs-0.2.0 (c (n "vqsort-rs") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.98") (d #t) (k 1)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.17") (d #t) (k 0)) (d (n "system-deps") (r "^6.2.2") (d #t) (k 1)))) (h "06151yymnf1vfm18c8g91yfkrqjj5swz9lbw6zh0szpg6hbmdj5h")))

