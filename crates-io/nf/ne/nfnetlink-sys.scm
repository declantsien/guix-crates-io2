(define-module (crates-io nf ne nfnetlink-sys) #:use-module (crates-io))

(define-public crate-nfnetlink-sys-0.1.0 (c (n "nfnetlink-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.99") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1jh6287fkp2ns6w3k2as4hrjz5q9y34v0iakvnvzzhn06kf2kn0n") (l "nfnetlink")))

