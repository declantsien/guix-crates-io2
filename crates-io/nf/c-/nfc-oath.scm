(define-module (crates-io nf c- nfc-oath) #:use-module (crates-io))

(define-public crate-nfc-oath-0.1.0 (c (n "nfc-oath") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nfc") (r "^0.1.11") (d #t) (k 0)))) (h "12x0bd4rpcv4xln01b8fz90sginx7ljnyj292f7cj6hn53w3h6rn")))

(define-public crate-nfc-oath-0.2.0 (c (n "nfc-oath") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nfc") (r "^0.1.11") (d #t) (k 0)))) (h "1vzl3w4rlvd3ard258vwn12xwjnd6dpkgk1yak15b6m1m1ibcqxw") (l "nfc")))

