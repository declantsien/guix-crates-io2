(define-module (crates-io nf c- nfc-sys) #:use-module (crates-io))

(define-public crate-nfc-sys-0.1.0 (c (n "nfc-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "04f2cx30sy6aivcli9ppl3k322a36h3n3apjhvmsb6p6qj0wraby")))

(define-public crate-nfc-sys-0.1.1 (c (n "nfc-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "183srb563dsmmgbqcj1rnnabw3bhkndyb8hdkkg0fwwm79adf9kv")))

(define-public crate-nfc-sys-0.1.2 (c (n "nfc-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1085pavr4lnfp5hnxh9z0m0nribfmp1grkjfr95ma56irv956fqd")))

(define-public crate-nfc-sys-0.1.3 (c (n "nfc-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0ygpw1886027rsmhxs1i7qdpfab6gm06fc1vjz4y5hv46s2zm3d7")))

(define-public crate-nfc-sys-0.1.4 (c (n "nfc-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "02k6x8l06qwhxhz8i237q1ac2h4p8lhjfk2v9bjkf2100gq35a02")))

(define-public crate-nfc-sys-0.1.5 (c (n "nfc-sys") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "13nxzhcsa8qr44vcnzgxl306nsw59dx7q6yznwwk41j10yj1ms4q")))

