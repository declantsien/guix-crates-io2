(define-module (crates-io nf tn nftnl-rs) #:use-module (crates-io))

(define-public crate-nftnl-rs-0.1.0 (c (n "nftnl-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1r3in41l8jiwqlqmpj0a9rs2f8kdkhd5r6iab0i1lsm1vdr3mi6s")))

(define-public crate-nftnl-rs-0.2.0 (c (n "nftnl-rs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0h5xa05cml4jshvsrrhckwfi8rjs1b1as1ssyq8zvrcv219m4lbc")))

(define-public crate-nftnl-rs-0.3.0 (c (n "nftnl-rs") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mpp6h6fa518p3my7ilmlqijyjdh72spk6slcpz1gysc79rh69j3")))

(define-public crate-nftnl-rs-0.3.1 (c (n "nftnl-rs") (v "0.3.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zj8dmis4k0lgz0favr991s29f2af2b5k6ms2jnwwrv77g2m1rbh") (y #t)))

(define-public crate-nftnl-rs-0.3.2 (c (n "nftnl-rs") (v "0.3.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g6kxg3xgjr3bh37sb3zkrhx815m9c2799rfkk6gyz2183xvzfcp")))

(define-public crate-nftnl-rs-0.4.0 (c (n "nftnl-rs") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zjaj50irn8l21jcb9843f3nxkgvxq4w8jzb3wcxgvv03bq1bf05")))

