(define-module (crates-io nf qu nfqueue) #:use-module (crates-io))

(define-public crate-nfqueue-0.9.0 (c (n "nfqueue") (v "0.9.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pnet") (r "^0.12.0") (d #t) (k 2)))) (h "12kcqqfskznb397crnbg91rg32dgwyhspz9n0qgplc2djdff88fm")))

(define-public crate-nfqueue-0.9.1 (c (n "nfqueue") (v "0.9.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pnet") (r "^0.17.1") (d #t) (k 2)))) (h "1jhv24qdknj0gkgvvajk1q7cvi3v66zni27ynbk78kxfcrp6k146")))

