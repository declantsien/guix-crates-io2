(define-module (crates-io nf t_ nft_storage_cli) #:use-module (crates-io))

(define-public crate-nft_storage_cli-0.1.0 (c (n "nft_storage_cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "nft_storage_api") (r "^1.1.0") (d #t) (k 0)) (d (n "nft_storage_core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0gb30ycqpccj2ac9bfvk1fl0awcw1jx5ic3mha2w3117nf3cfxzd")))

