(define-module (crates-io nf t_ nft_collection) #:use-module (crates-io))

(define-public crate-nft_collection-0.1.1 (c (n "nft_collection") (v "0.1.1") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.7.0") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.7.0") (d #t) (k 2)) (d (n "spl-associated-token-account") (r "^1.0.3") (d #t) (k 2)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xb7g3nwbrdkg8v4qxj07926yqwrqir6dkw90j5azflzppkg542y")))

