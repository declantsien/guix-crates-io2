(define-module (crates-io nf lz nflz) #:use-module (crates-io))

(define-public crate-nflz-0.1.0 (c (n "nflz") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "0lk5q0izrgvgc06m1yg2mb1kc8kyalj6bgqqlmvj4w4xwbcnb7mx")))

(define-public crate-nflz-0.1.1 (c (n "nflz") (v "0.1.1") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1q3x9gkg090iqxr5nggk63z9knzambbb5sxa0ig86rafpdrl93cw")))

(define-public crate-nflz-0.1.2 (c (n "nflz") (v "0.1.2") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1vabshjm654xwb3sj3h3b0x6c0sn6hzank0gx44j9fsrw9grbbka")))

(define-public crate-nflz-1.0.0 (c (n "nflz") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "189xqagj7m9zky54s1gc2v6rhhiknl5z1k7fbs728b0979n3kwk3")))

(define-public crate-nflz-1.0.1 (c (n "nflz") (v "1.0.1") (d (list (d (n "fs_extra") (r "^1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "15gm3lj6s0rl021aw2zxs6raz5prfg91qyrxwlpd62n1clijr525")))

(define-public crate-nflz-1.0.2 (c (n "nflz") (v "1.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 2)))) (h "164i9bg86mywmxwkwbz9izxwb8dkrn0fqq5081cck1vrd5kx37sb")))

