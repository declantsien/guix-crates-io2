(define-module (crates-io nf ta nftables-json) #:use-module (crates-io))

(define-public crate-nftables-json-0.1.0 (c (n "nftables-json") (v "0.1.0") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "serde_with") (r "~2.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1bx96s6yxanr69h0r9vm4r1r3m4077riafsv9j93vlfl3fjsjgs3")))

(define-public crate-nftables-json-0.2.0 (c (n "nftables-json") (v "0.2.0") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "serde_with") (r "~2.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0ki1hmbz5lj8337d4qqkcv2bvnwcdncwbvn9z83sz1942jvqn7d2")))

(define-public crate-nftables-json-0.3.0 (c (n "nftables-json") (v "0.3.0") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "serde_with") (r "~2.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1dpi6p4wg3wdbg7mg2m1vii2bi7qp0mlivkvphs1rkaf1aim0kcz")))

