(define-module (crates-io nf ta nftables) #:use-module (crates-io))

(define-public crate-nftables-0.2.0 (c (n "nftables") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1c2wm1pw8zpc92xhvpcxpa0xidrqnzbd3xn1cqyv7ay36s50n77a")))

(define-public crate-nftables-0.2.1 (c (n "nftables") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0xcxq4lda5dfa5dz9vnnyg5m5lsnfzh31d3kbjdyilf1fs7h2klj")))

(define-public crate-nftables-0.2.2 (c (n "nftables") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1c165q2ylhql1p74q09hjd8mgmhazxx1hx5d44gmkn7z4zzcr4xl")))

(define-public crate-nftables-0.2.3 (c (n "nftables") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1lhx87mr790avsliha06x2qigyx3ijqms0fkd6fx4vi5n1dkvcdl")))

(define-public crate-nftables-0.2.4 (c (n "nftables") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rq74a91r76s3jqpv0y1pb3bqq8msbfv9v006q6g74qzcc6vw2p1")))

(define-public crate-nftables-0.3.0 (c (n "nftables") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lmn2qkd5d24ip963ijj864djmqnm0q2mvgansbkv82fqysiplbi")))

(define-public crate-nftables-0.4.0 (c (n "nftables") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "strum") (r "^0.26.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l98618mmlkvg42kvx7wnwb1h32sg0gp02gnnsa2i37w6d5v92g6")))

