(define-module (crates-io nf -r nf-rated) #:use-module (crates-io))

(define-public crate-nf-rated-0.1.0 (c (n "nf-rated") (v "0.1.0") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.7") (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "tui") (r "^0.10.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "0w93qyb6zrxyrvf2356xz8bb13d42kjf0wg48as44yk04081633z") (f (quote (("log"))))))

