(define-module (crates-io nf de nfde) #:use-module (crates-io))

(define-public crate-nfde-0.0.1 (c (n "nfde") (v "0.0.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)))) (h "0b5qx9syvxkwl625ws6g48di4bzjfpz1nypxdi37142a40vav1hx")))

(define-public crate-nfde-0.0.2 (c (n "nfde") (v "0.0.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)))) (h "0vk2y7z9ny5iimapnz8k834hbv0rgc0qs9n15zkj2pm5n04lb5gw")))

(define-public crate-nfde-0.0.3 (c (n "nfde") (v "0.0.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)))) (h "0zg0x4pvgb71kz2fk3cl3y3hdj15jm0l6hqc0ziyn5zjqb1bm1gh")))

(define-public crate-nfde-0.0.4 (c (n "nfde") (v "0.0.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)))) (h "027lvc4s98zddfiryq9119x058k2frxyhv4hw3g6xymhzx0vdq65")))

(define-public crate-nfde-0.0.5 (c (n "nfde") (v "0.0.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^0.5") (d #t) (t "cfg(windows)") (k 0)))) (h "0vchrn0qgdmwak7cpild5ydx883lxxwpgq0qx9ik14dwmz9igfl3")))

(define-public crate-nfde-0.0.6 (c (n "nfde") (v "0.0.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^0.5") (d #t) (t "cfg(windows)") (k 0)))) (h "04rjap43amdfl4r7lcwf2zi2qxxwg1av38hh2s72jv56zg7k7mlv") (y #t)))

(define-public crate-nfde-0.0.7 (c (n "nfde") (v "0.0.7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0nxq1x4pvvn8q425zyxkzr22ikk0zs235rfwdikv1mghm0w7pbgw") (y #t)))

(define-public crate-nfde-0.0.8 (c (n "nfde") (v "0.0.8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "03qkcjfzl6w86r2hi2klydlaw4zasi7n8601xfc8q6l6hxarv1ln")))

