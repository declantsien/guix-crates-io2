(define-module (crates-io nf a_ nfa_regex) #:use-module (crates-io))

(define-public crate-nfa_regex-1.0.0 (c (n "nfa_regex") (v "1.0.0") (h "015m4dk9072py6mvl0lhqkr0aqlgvppda1m3csya9xy43n64nfm6")))

(define-public crate-nfa_regex-1.0.1 (c (n "nfa_regex") (v "1.0.1") (h "03pfsry5a8n44wn2f3rqcllkc90gbs0qxcaf66ga7rd27hr11gxb")))

