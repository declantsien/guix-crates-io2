(define-module (crates-io nf d- nfd-sys) #:use-module (crates-io))

(define-public crate-nfd-sys-0.1.3 (c (n "nfd-sys") (v "0.1.3") (d (list (d (n "cocoa") (r "^0.2.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "gtk") (r "^0.0.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1spp11g9i47bwjv0vwq00v6892wcb1rf7infizakn5s4y5k1jhny")))

(define-public crate-nfd-sys-0.1.4 (c (n "nfd-sys") (v "0.1.4") (d (list (d (n "cocoa") (r "^0.2.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "gtk") (r "^0.0.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1lzz28g33a3nh5ypjxcsl1k1qxlpx04hrnqm4x14s5ncfqna2h8s")))

(define-public crate-nfd-sys-0.1.5 (c (n "nfd-sys") (v "0.1.5") (d (list (d (n "cocoa") (r "^0.2.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "gtk") (r "^0.0.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "18xjvcsy6nh445xb7yd7ilizzffm6vphy604qbpcgbpd6r397iaz")))

