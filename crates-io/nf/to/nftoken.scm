(define-module (crates-io nf to nftoken) #:use-module (crates-io))

(define-public crate-nftoken-0.1.0 (c (n "nftoken") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24.2") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (f (quote ("const-generics"))) (d #t) (k 0)))) (h "1byh41qmkxjrv3g1vw34l16w0w331y3ciwzhz8jj1nyjkhfcn5s7") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-nftoken-0.1.4 (c (n "nftoken") (v "0.1.4") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24.2") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (f (quote ("const-generics"))) (d #t) (k 0)))) (h "1v69i2wckhvmkrk35p115jf78q2764jrqs2dd0xc8g1i33313g87") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-nftoken-1.0.1 (c (n "nftoken") (v "1.0.1") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24.2") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (f (quote ("const-generics"))) (d #t) (k 0)))) (h "0mpgf5by66jr97rjn0kdvhq62jxcp3kv3xaz4i39r2brz72rx7ja") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

