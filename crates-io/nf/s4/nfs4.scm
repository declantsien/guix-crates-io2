(define-module (crates-io nf s4 nfs4) #:use-module (crates-io))

(define-public crate-nfs4-0.1.0 (c (n "nfs4") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "bitflags_serde_shim") (r "^0.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6") (d #t) (k 0)) (d (n "num_enum") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xdr") (r "^0.6") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "sun_rpc") (r "^0.1") (d #t) (k 0)) (d (n "xdr_extras") (r "^0.1") (d #t) (k 0)))) (h "1yy74w9hrch3syirggp33kx8r2fbcs1dpgrw2dgvbissp58grw6c")))

