(define-module (crates-io nf s4 nfs4_client) #:use-module (crates-io))

(define-public crate-nfs4_client-0.1.0 (c (n "nfs4_client") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "nfs4") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde-xdr") (r "^0.6") (d #t) (k 0)) (d (n "sun_rpc_client") (r "^0.1") (d #t) (k 0)))) (h "0ly9v0q8v3mqcv5c9fv2wb5f266xfbhir1j2mqx2c3wjm7cx8slv")))

