(define-module (crates-io nf s4 nfs4_acl_tools) #:use-module (crates-io))

(define-public crate-nfs4_acl_tools-0.0.1 (c (n "nfs4_acl_tools") (v "0.0.1") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "bitflags_serde_shim") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ljhwhdwn3qrd48fn1ilh10ignmrg8cabkc5b1pgnzm2bswp2y0d")))

