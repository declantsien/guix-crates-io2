(define-module (crates-io nf s4 nfs4_cli) #:use-module (crates-io))

(define-public crate-nfs4_cli-0.1.0 (c (n "nfs4_cli") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "nfs4") (r "^0.1") (d #t) (k 0)) (d (n "nfs4_client") (r "^0.1") (d #t) (k 0)) (d (n "sun_rpc_client") (r "^0.1") (d #t) (k 0)))) (h "0h1h8a5ayzwpbpmgnwgpyhglapxgg4hv7svmsinhxl2c8i8km2qn")))

