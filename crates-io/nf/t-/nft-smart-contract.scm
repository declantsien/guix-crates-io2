(define-module (crates-io nf t- nft-smart-contract) #:use-module (crates-io))

(define-public crate-nft-smart-contract-0.1.0 (c (n "nft-smart-contract") (v "0.1.0") (h "0aw64fchnm5qw86h1wxn2h7f6bbccgh2ldr1j1fcq8b7vr1yipw0") (y #t)))

(define-public crate-nft-smart-contract-0.1.1 (c (n "nft-smart-contract") (v "0.1.1") (h "14x2l3n6yk3hw6927q41jcjjlmxbc5hgizvc65lmi0rhppd0115s")))

