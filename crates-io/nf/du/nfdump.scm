(define-module (crates-io nf du nfdump) #:use-module (crates-io))

(define-public crate-nfdump-0.1.0 (c (n "nfdump") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "bzip2") (r "^0.4.4") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "minilzo") (r "^0.2.0") (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "1b3hnl354ki214w08kp4zpr1xcgz7hbdj4l25maj8421c4fizdwd")))

