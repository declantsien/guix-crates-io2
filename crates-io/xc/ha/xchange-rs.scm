(define-module (crates-io xc ha xchange-rs) #:use-module (crates-io))

(define-public crate-xchange-rs-0.1.0 (c (n "xchange-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jabxr9vxh3blgykg62b9grv8cv8f6cr7cpn1y5ykyjfhnxiamg4") (y #t)))

(define-public crate-xchange-rs-0.1.1 (c (n "xchange-rs") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h1hrqbrm4vwwlqxrfr86gsg31q2xg9gmpbp6y32imzai0kir43w")))

