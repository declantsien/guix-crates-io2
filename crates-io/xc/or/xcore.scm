(define-module (crates-io xc or xcore) #:use-module (crates-io))

(define-public crate-xcore-0.1.0 (c (n "xcore") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "110219zb1ybhldsfygkhlkp0y2nl2z2rs9lhymfp8kmfp8kc3dba")))

(define-public crate-xcore-0.1.1 (c (n "xcore") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "0shhfkzw91r80jrsqp8szsrivqbnlvanvbvslmzkgsqdddm0sxil")))

(define-public crate-xcore-0.1.2 (c (n "xcore") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "1339l152dyc22pn33xj5vp4rcqan5lwchdswa2iq472gzk1vpbs1")))

(define-public crate-xcore-0.1.3 (c (n "xcore") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "0l3xafx5gjp2axk21kp9rnkjs0bg1vsdplshn129mksynmgr4r2s")))

