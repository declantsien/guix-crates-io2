(define-module (crates-io xc ur xcursorlocate) #:use-module (crates-io))

(define-public crate-xcursorlocate-0.1.1 (c (n "xcursorlocate") (v "0.1.1") (d (list (d (n "confy") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("shape"))) (d #t) (k 0)))) (h "1wx00ay1kxs3yc0qg4zwp0l7pynklpxvkvjckb6yzx1dqxif0qy7")))

