(define-module (crates-io xc ur xcursor) #:use-module (crates-io))

(define-public crate-xcursor-0.1.0 (c (n "xcursor") (v "0.1.0") (d (list (d (n "rust-ini") (r "^0.15") (d #t) (k 0)))) (h "04w6kdkq95i273r8w8pshp7abw3n0m8kyx5s83l3r1j9n9gw2wpq")))

(define-public crate-xcursor-0.1.1 (c (n "xcursor") (v "0.1.1") (d (list (d (n "rust-ini") (r "^0.15") (d #t) (k 0)))) (h "18fqjajwrx87wd31gmzl5qnbrn33h2n4mnrc8zrqn3w7ysrg6gxd")))

(define-public crate-xcursor-0.1.2 (c (n "xcursor") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1cczzjxz5slv8rxd50pk0q1sbpv1laws40bb0ci7nhcwi8ypw8ix")))

(define-public crate-xcursor-0.2.0 (c (n "xcursor") (v "0.2.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vwzn4s2ysi12cc7d5162rdcqnnzazbk0lqch6gs3nc4m7ivf2ab")))

(define-public crate-xcursor-0.3.0 (c (n "xcursor") (v "0.3.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "05yxfrdimm2akfxg14rpif1cr50kjhx6szmpiynfp9c6271qqmhs")))

(define-public crate-xcursor-0.3.1 (c (n "xcursor") (v "0.3.1") (d (list (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "0d8lxz3yyxdnalc8cv6pycv4d5bl6d5gvww925nz9l22nf8frvan")))

(define-public crate-xcursor-0.3.2 (c (n "xcursor") (v "0.3.2") (d (list (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "008d3cqp3pf5bngnai4m0fc5qs8d02l37bkk032y2dgxvv7q396k")))

(define-public crate-xcursor-0.3.3 (c (n "xcursor") (v "0.3.3") (d (list (d (n "nom") (r "^6.0") (k 0)))) (h "022x7jm71dyqrxwsjkqfgj8bx57y7g8yyz318qb80y5ffhaj76is")))

(define-public crate-xcursor-0.3.4 (c (n "xcursor") (v "0.3.4") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)))) (h "1dwh8vkll79i2vr56dmn9fdpw2ig0klcb0a3300l7k8k6fk0ads6")))

(define-public crate-xcursor-0.3.5 (c (n "xcursor") (v "0.3.5") (h "0499ff2gy9hfb9dvndn5zyc7gzz9lhc5fly3s3yfsiak99xws33a")))

