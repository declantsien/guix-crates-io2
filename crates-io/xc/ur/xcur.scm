(define-module (crates-io xc ur xcur) #:use-module (crates-io))

(define-public crate-xcur-0.1.0 (c (n "xcur") (v "0.1.0") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "1chbfnh2a6wivwg45yv5ghc4bw67mafkvl4rhn4fnxl8dgrgl5sn")))

(define-public crate-xcur-0.1.1 (c (n "xcur") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.80") (o #t) (d #t) (k 0)) (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "0s47ybkmjxlrnpja4jfbj72dab8k4xb9v63nrx4mknaa51qagn47") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-xcur-0.1.2 (c (n "xcur") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.80") (o #t) (d #t) (k 0)) (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "1d7lyi7vv9n9qrirjski3wag7px72jyi6npkq0hcx9cs7nnmgvrf") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-xcur-0.1.3 (c (n "xcur") (v "0.1.3") (d (list (d (n "clippy") (r "^0.0.80") (o #t) (d #t) (k 0)) (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "12qy40ckdibdh8pzlrcn9sb11hir991n18niyf6gbj42dq07j65g") (f (quote (("dev" "clippy") ("default"))))))

