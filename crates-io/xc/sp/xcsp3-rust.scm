(define-module (crates-io xc sp xcsp3-rust) #:use-module (crates-io))

(define-public crate-xcsp3-rust-0.1.0 (c (n "xcsp3-rust") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.29.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1xdpam10dklksan96iv29jiw9ndn3ilxmgcy1gzvp3ndzmymzx5d") (r "1.70")))

