(define-module (crates-io xc m_ xcm_grpc_lib) #:use-module (crates-io))

(define-public crate-xcm_grpc_lib-0.1.0 (c (n "xcm_grpc_lib") (v "0.1.0") (h "1vs7ca6wizn5wryqg2zf64551cyvaxswvh92zsl19cpg8g1hz533")))

(define-public crate-xcm_grpc_lib-0.1.1 (c (n "xcm_grpc_lib") (v "0.1.1") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "14n1lrqpn7hkv31zi53ih3mpyzs73l0z4m7i9dm135liqxhpfip3")))

