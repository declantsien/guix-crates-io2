(define-module (crates-io xc #{2b}# xc2bit) #:use-module (crates-io))

(define-public crate-xc2bit-0.0.0 (c (n "xc2bit") (v "0.0.0") (h "0ppxn88hcacfbd4z9y1kn4xl9dz2cfn5ni135x3527f79m43v4bl")))

(define-public crate-xc2bit-0.0.1 (c (n "xc2bit") (v "0.0.1") (h "0f3ncrpbiaza1ydvr28dr2a44wrm7ig013qw50zwiy26qmx35idp")))

(define-public crate-xc2bit-0.0.2 (c (n "xc2bit") (v "0.0.2") (h "1zr4sxn38g3hd23alaxdkaw1q2dymvkkx28zqq5f4c7q244y7bdg")))

(define-public crate-xc2bit-0.0.3 (c (n "xc2bit") (v "0.0.3") (d (list (d (n "bittwiddler") (r "^0.0.1") (d #t) (k 0)) (d (n "jedec") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "yosys-netlist-json") (r "^0.0.2") (d #t) (k 0)))) (h "0bdlalzxpp5lvkm98zwd8f3aljh62axyxbs95g4hyd013drmffw2")))

(define-public crate-xc2bit-0.0.4 (c (n "xc2bit") (v "0.0.4") (d (list (d (n "bittwiddler") (r "^0.0.2") (d #t) (k 0)) (d (n "jedec") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "yosys-netlist-json") (r "^0.0.3") (d #t) (k 0)))) (h "19y03p9ybhr4wvzfmybd3g4zcnyh69xlhjxspbm78sj5pa4y7dk5")))

