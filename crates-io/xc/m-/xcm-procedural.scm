(define-module (crates-io xc m- xcm-procedural) #:use-module (crates-io))

(define-public crate-xcm-procedural-0.0.0 (c (n "xcm-procedural") (v "0.0.0") (h "1l0zcii4am0xhh294dgxrm5ngqb26cld0f3127d6jqj2v5rhrajq") (y #t)))

(define-public crate-xcm-procedural-0.1.0-dev.1 (c (n "xcm-procedural") (v "0.1.0-dev.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0kxrxv5xp11ijdl6ym7bha83i63faf1994mi1lpyh51lz7lmbplq")))

(define-public crate-xcm-procedural-0.1.0-dev.2 (c (n "xcm-procedural") (v "0.1.0-dev.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "09bfimvqix98sx2l4szy3nhwk8pm4y3pfy203f84zb5xw5wl3mms")))

(define-public crate-xcm-procedural-0.1.0-dev.3 (c (n "xcm-procedural") (v "0.1.0-dev.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "04nbzlzn6cc56imb5px5m3s99djjwldg5dbyzbk4xda29jjwai2a")))

(define-public crate-xcm-procedural-0.1.0-dev.6 (c (n "xcm-procedural") (v "0.1.0-dev.6") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "16nq8skpaabqc23dh02fvk7989cqm4zbccp5nsirgbmqc76imbks")))

(define-public crate-xcm-procedural-1.0.0 (c (n "xcm-procedural") (v "1.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0i8pnqwhmbcxzsfdb4851j38zmbnpmj3lcz3sx6cflyic28sg8pw")))

(define-public crate-xcm-procedural-2.0.0 (c (n "xcm-procedural") (v "2.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "19mw6fbjam3756nrr9a2nv9116s1f4qi36skjipr5mkxahm4bplv")))

(define-public crate-xcm-procedural-3.0.0-dev.1 (c (n "xcm-procedural") (v "3.0.0-dev.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1jx0nany2sx84v6ff0rvcj9saq69yzcppjf0918cggxyancxrhkn")))

(define-public crate-xcm-procedural-3.0.0 (c (n "xcm-procedural") (v "3.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1r25hj3ifhwwwwgi2f737w42slns0iqdds8krix311i3h0g9jsax")))

(define-public crate-xcm-procedural-4.0.0 (c (n "xcm-procedural") (v "4.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0gq0asplmk4m5phrrswdvmbna68qh7jnzc7cxinj9acdc9i2n7j0")))

(define-public crate-xcm-procedural-5.0.0 (c (n "xcm-procedural") (v "5.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.74") (f (quote ("diff"))) (d #t) (k 2)))) (h "0qg0qcl4f2gsjvn33ckd1hr98lvyhvxih8vdh8pipmv7qb3wszsf")))

(define-public crate-xcm-procedural-6.0.0 (c (n "xcm-procedural") (v "6.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.74") (f (quote ("diff"))) (d #t) (k 2)))) (h "08z2blsvsa9p4gl34s4p36rjvj7736fh7q9ybzhdy58q752191ip")))

(define-public crate-xcm-procedural-7.0.0 (c (n "xcm-procedural") (v "7.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.88") (f (quote ("diff"))) (d #t) (k 2)))) (h "05fmc23lzs0z538vrf4dbiy19ys1vgy73kx426dyqhhwfp6zm63r")))

(define-public crate-xcm-procedural-8.0.0 (c (n "xcm-procedural") (v "8.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.88") (f (quote ("diff"))) (d #t) (k 2)))) (h "1kd2gbm3jjjv692p91bwddpjyp31s98cylyvsxqdm70ajybplwgl")))

(define-public crate-xcm-procedural-9.0.0 (c (n "xcm-procedural") (v "9.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.88") (f (quote ("diff"))) (d #t) (k 2)))) (h "1601hmk82v5cxlnl000s33d86jmymvx5bcf4a013hbgzdaz9i57x")))

