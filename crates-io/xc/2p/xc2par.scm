(define-module (crates-io xc #{2p}# xc2par) #:use-module (crates-io))

(define-public crate-xc2par-0.0.0 (c (n "xc2par") (v "0.0.0") (h "18pacx1wl78p67adk7fa6n8yy4bf1pidx6b4jgrfrjgn44j2y29w")))

(define-public crate-xc2par-0.0.1 (c (n "xc2par") (v "0.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)) (d (n "xc2bit") (r "^0.0.3") (d #t) (k 0)) (d (n "yosys-netlist-json") (r "^0.0.2") (d #t) (k 0)))) (h "17cyyp0s9pm332r1gmsp6iw8qsjr3jd9z7ix67zqfh5ja6s6yw37")))

(define-public crate-xc2par-0.0.2 (c (n "xc2par") (v "0.0.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-stdlog") (r "^3") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)) (d (n "xc2bit") (r "^0.0.4") (d #t) (k 0)) (d (n "yosys-netlist-json") (r "^0.0.3") (d #t) (k 0)))) (h "0nda7bg3r3bqfb51i8b8sgnr0qd8vssirxrw56wqdq1xnz04r4b8")))

