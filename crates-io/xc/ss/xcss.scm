(define-module (crates-io xc ss xcss) #:use-module (crates-io))

(define-public crate-xcss-0.1.0 (c (n "xcss") (v "0.1.0") (h "07ayd86j99nhw732wy0z70nrsj856zmn763mpgiwqjffgaa3bm52")))

(define-public crate-xcss-0.1.1 (c (n "xcss") (v "0.1.1") (h "0cfb72z9m75srvinsrirjrqsglh1llhx9inv302hcmf5k869zabz")))

