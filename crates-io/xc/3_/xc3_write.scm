(define-module (crates-io xc #{3_}# xc3_write) #:use-module (crates-io))

(define-public crate-xc3_write-0.1.0 (c (n "xc3_write") (v "0.1.0") (d (list (d (n "binrw") (r "^0.11.2") (d #t) (k 0)) (d (n "xc3_write_derive") (r "^0.1.0") (d #t) (k 0)))) (h "15qb14q7x3qh0960r0z6jvqyazlr0pazz8w82bjfxanvghl1ii83")))

(define-public crate-xc3_write-0.2.0 (c (n "xc3_write") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hexlit") (r "^0.5.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "xc3_write_derive") (r "^0.2.0") (d #t) (k 0)))) (h "08wry3zlbq2l4kx6qr53i2rsniyschwyycglkzyqdsfmjimy15i6")))

(define-public crate-xc3_write-0.3.0 (c (n "xc3_write") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hexlit") (r "^0.5.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "xc3_write_derive") (r "^0.3.0") (d #t) (k 0)))) (h "13wmarqiwmjl5l9d2wgf310j1rybh90fvnddcsvgnpygh2xvh5w6")))

(define-public crate-xc3_write-0.4.0 (c (n "xc3_write") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hexlit") (r "^0.5.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "xc3_write_derive") (r "^0.4.0") (d #t) (k 0)))) (h "08zsg0qj3a66yb041ah9cnfxnln0b2lb11yfv6r34qvk3g7lgbjg")))

(define-public crate-xc3_write-0.5.0 (c (n "xc3_write") (v "0.5.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hexlit") (r "^0.5.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "xc3_write_derive") (r "^0.5.0") (d #t) (k 0)))) (h "1kzasppvfr7r0ddiq3bkc1s2np3kqxlspqgxy3ywp2ciks11s466")))

(define-public crate-xc3_write-0.6.0 (c (n "xc3_write") (v "0.6.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hexlit") (r "^0.5.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "xc3_write_derive") (r "^0.6.0") (d #t) (k 0)))) (h "1iba6hkd7hrq55iyxj692vqrgi54z88szwczrcqs3cqjxyh1r3j2")))

(define-public crate-xc3_write-0.7.0 (c (n "xc3_write") (v "0.7.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hexlit") (r "^0.5.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "xc3_write_derive") (r "^0.7.0") (d #t) (k 0)))) (h "17n3bfkr72qgyhhk6v8mcqj0vgvdrd4na4yklc985vfchdpb3lz2")))

(define-public crate-xc3_write-0.8.0 (c (n "xc3_write") (v "0.8.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hexlit") (r "^0.5.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "xc3_write_derive") (r "^0.8.0") (d #t) (k 0)))) (h "0knasaa2qm4zxcpx8908qhh5zcncsv71wvs9da0affza8niki7pr")))

(define-public crate-xc3_write-0.9.0 (c (n "xc3_write") (v "0.9.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hexlit") (r "^0.5.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "xc3_write_derive") (r "^0.9.0") (d #t) (k 0)))) (h "0rm17gdahcpj8cj3zidr8jiczcbm3wl72zy4xvyan29fw1sshjg3")))

