(define-module (crates-io xc #{3_}# xc3_write_derive) #:use-module (crates-io))

(define-public crate-xc3_write_derive-0.1.0 (c (n "xc3_write_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0fyyf3f1x03cgc577cj7wzb3lkggyhappn8xhjj2pf23nfi6lkzd")))

(define-public crate-xc3_write_derive-0.2.0 (c (n "xc3_write_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "19xg6hcky7wyrivq1p87vr843iyn87g81wp1wix23iq7v0c55kv4")))

(define-public crate-xc3_write_derive-0.3.0 (c (n "xc3_write_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0xdfvi71gvcrg65w2x8f8iylpiyd81vcbyl6vxl59n0qp81b1fmr")))

(define-public crate-xc3_write_derive-0.4.0 (c (n "xc3_write_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0wi65kpvw3hvq0rzgs6sh8d88lxb9xb2p7xb0p450lhc3s6dxiz9")))

(define-public crate-xc3_write_derive-0.5.0 (c (n "xc3_write_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0sdavhvmz7x1971f86yb5rf582db4akcpa8vvgjb0skvvzpw0mwy")))

(define-public crate-xc3_write_derive-0.6.0 (c (n "xc3_write_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0nzz3g62jciq8jyk24bijsr9pfy4sk72s2m38d29h2wc9vqknpiv")))

(define-public crate-xc3_write_derive-0.7.0 (c (n "xc3_write_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1z7zwrz4mhrqg4bb3fla8kkdy0ixbaywr4j040x7svm48avyh3jg")))

(define-public crate-xc3_write_derive-0.8.0 (c (n "xc3_write_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "10lp50js11glwrxjsibfv0p9v0ldihmp24p1zb1mhfz6pww2ibgm")))

(define-public crate-xc3_write_derive-0.9.0 (c (n "xc3_write_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "13r8jlpd2qmm6ybc18cdafpr32fwkrg1zbnckhr78hsyh3djzscj")))

