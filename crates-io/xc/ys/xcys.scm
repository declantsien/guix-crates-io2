(define-module (crates-io xc ys xcys) #:use-module (crates-io))

(define-public crate-xcys-0.1.2 (c (n "xcys") (v "0.1.2") (d (list (d (n "async-process") (r "^1.6.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "octocrab") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "whoami") (r "^1.3.0") (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (t "cfg(windows)") (k 1)))) (h "1mhrvq1nfn9slxjn2a80v5hfiwjbd6zqzcs5zkh879w3l3ng52bi")))

