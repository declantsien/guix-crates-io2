(define-module (crates-io xc a9 xca9548a) #:use-module (crates-io))

(define-public crate-xca9548a-0.1.0 (c (n "xca9548a") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0n9kdwmkgnh5wdcw8p033ag6y60sh59p9wj0qvziybzwb6wjz3xp") (f (quote (("default"))))))

(define-public crate-xca9548a-0.2.0 (c (n "xca9548a") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "10nyh43d8mjvnc6nzsw3mdc953byd8inkwcf00h93v08gp3a0240") (f (quote (("default"))))))

(define-public crate-xca9548a-0.2.1 (c (n "xca9548a") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0271dj9qj3r0z1y0jqg2ykv3kz5qfdi7icpd3c8cqvhf01jcwmlw") (f (quote (("default"))))))

