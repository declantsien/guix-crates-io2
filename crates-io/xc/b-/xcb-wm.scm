(define-module (crates-io xc b- xcb-wm) #:use-module (crates-io))

(define-public crate-xcb-wm-0.1.0 (c (n "xcb-wm") (v "0.1.0") (d (list (d (n "xcb") (r "^1") (d #t) (k 0)))) (h "00br3s8aqxn63wihnr4cjys3089wjp64imaang2x2cfx7xd6q77z") (f (quote (("icccm") ("ewmh"))))))

(define-public crate-xcb-wm-0.2.0 (c (n "xcb-wm") (v "0.2.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "xcb") (r "^1") (d #t) (k 0)))) (h "170mlhfaymqdxdqxgv3x8m5sjx7dhz6lb77667jnl14ccljia1gh") (f (quote (("icccm") ("ewmh"))))))

(define-public crate-xcb-wm-0.2.1 (c (n "xcb-wm") (v "0.2.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "xcb") (r "^1") (d #t) (k 0)))) (h "1ak3lva6xv4g0ya24sx0npb5672mqk9q7gprqls1jfn1jgdimw9s") (f (quote (("icccm") ("ewmh"))))))

(define-public crate-xcb-wm-0.3.0 (c (n "xcb-wm") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "xcb") (r "^1") (d #t) (k 0)))) (h "1dma8f1lnqqhns097pr1h1bcybrzpiddk2s0z0c65ah2nbyjf3qk") (f (quote (("icccm") ("ewmh"))))))

(define-public crate-xcb-wm-0.3.1 (c (n "xcb-wm") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "xcb") (r "^1") (d #t) (k 0)))) (h "04rkq4k9v290y27src248xcvadsa6n02ay5qk5ny5zpy665l3sh3") (f (quote (("icccm") ("ewmh"))))))

(define-public crate-xcb-wm-0.3.2 (c (n "xcb-wm") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "xcb") (r "^1") (d #t) (k 0)))) (h "13dy7lglkkvxsr5c97vp4i325hccl62wmyw73wd1rmf12cras99m") (f (quote (("icccm") ("ewmh"))))))

(define-public crate-xcb-wm-0.3.3 (c (n "xcb-wm") (v "0.3.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "xcb") (r "^1") (d #t) (k 0)))) (h "1rq34426z26csiqs9bawqy3w5171lzafy1fzdfq8jhh5db4rip1b") (f (quote (("icccm") ("ewmh"))))))

(define-public crate-xcb-wm-0.3.4 (c (n "xcb-wm") (v "0.3.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "xcb") (r "^1") (d #t) (k 0)))) (h "0d3xnzkh8v116fqz9nymf6582lgchvcj62agxdva6ynmdr2sbjww") (f (quote (("icccm") ("ewmh"))))))

(define-public crate-xcb-wm-0.3.5 (c (n "xcb-wm") (v "0.3.5") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "xcb") (r "^1") (d #t) (k 0)))) (h "05445wb8q5h4vkdj1gb590v1n3yllfs6ijbfp2zkrz3z07dszhdl") (f (quote (("icccm") ("ewmh")))) (y #t)))

(define-public crate-xcb-wm-0.3.6 (c (n "xcb-wm") (v "0.3.6") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "xcb") (r "^1") (d #t) (k 0)))) (h "063j9labqg4g96cqd6papr9pz62wakgk0pmj8zhwzfh8kd53yfqr") (f (quote (("icccm") ("ewmh"))))))

(define-public crate-xcb-wm-0.4.0 (c (n "xcb-wm") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "xcb") (r "^1") (d #t) (k 0)))) (h "0vf751gnr6fid0czn7id4xybh3wyryhryhpyki1ms8s26yl8078m") (f (quote (("icccm") ("ewmh"))))))

