(define-module (crates-io xc b- xcb-util-cursor) #:use-module (crates-io))

(define-public crate-xcb-util-cursor-0.1.0 (c (n "xcb-util-cursor") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xcb") (r "^1.2.0") (d #t) (k 0)) (d (n "xcb-util-cursor-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1zdv67pymjsd6b7gxb0k7xsyjw9h5wya785dqqs0h1hkgfriy8m8")))

(define-public crate-xcb-util-cursor-0.1.1 (c (n "xcb-util-cursor") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xcb") (r "^1.2.0") (d #t) (k 0)) (d (n "xcb-util-cursor-sys") (r "^0.1.0") (d #t) (k 0)))) (h "17mxf1qwwcdgxbcl10z4avlk8w9a8bvb32w15m407c5dkcq283ga")))

(define-public crate-xcb-util-cursor-0.1.2 (c (n "xcb-util-cursor") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xcb") (r "^1.2.0") (d #t) (k 0)) (d (n "xcb-util-cursor-sys") (r "^0.1.0") (d #t) (k 0)))) (h "15qqnj599348wa4qd5ln7n3zyjsgcnq6id2vx7ivka1g1416x95c")))

(define-public crate-xcb-util-cursor-0.2.0 (c (n "xcb-util-cursor") (v "0.2.0") (d (list (d (n "xcb") (r "^1.2.0") (d #t) (k 0)) (d (n "xcb-util-cursor-sys") (r "^0.1.1") (d #t) (k 0)))) (h "18mnvabqh51g5z9r3k9lwl0kz9zgck4ljpll5hn2j2y3l5dkzfkg")))

(define-public crate-xcb-util-cursor-0.3.0 (c (n "xcb-util-cursor") (v "0.3.0") (d (list (d (n "xcb") (r "^1.2.0") (d #t) (k 0)) (d (n "xcb-util-cursor-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1yizv11f12llj33ihlcnrg3pjjcz7fqs19cgyqd4qb7dyidhqhl8")))

(define-public crate-xcb-util-cursor-0.3.1 (c (n "xcb-util-cursor") (v "0.3.1") (d (list (d (n "xcb") (r "^1.2.0") (d #t) (k 0)) (d (n "xcb-util-cursor-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1wmn8h9crgg0c7azn2bsvb8945y3gdlkd4dgmbjlky6hns0q2881")))

(define-public crate-xcb-util-cursor-0.3.2 (c (n "xcb-util-cursor") (v "0.3.2") (d (list (d (n "xcb") (r "^1.2.0") (d #t) (k 0)) (d (n "xcb-util-cursor-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0dmh6ns21hy1mvi3cc1dgz8kaij5mf4h54ncrmwf4913p17s1k22")))

(define-public crate-xcb-util-cursor-0.3.3 (c (n "xcb-util-cursor") (v "0.3.3") (d (list (d (n "xcb") (r "^1.4.0") (d #t) (k 0)) (d (n "xcb-util-cursor-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1b746cbp9g7lnakq0yk4m9909aq6vb38kbl4srq1bwdchinmdi5d")))

