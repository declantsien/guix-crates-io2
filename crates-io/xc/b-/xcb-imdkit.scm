(define-module (crates-io xc b- xcb-imdkit) #:use-module (crates-io))

(define-public crate-xcb-imdkit-0.1.0 (c (n "xcb-imdkit") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "xcb") (r "^0.9.0") (d #t) (k 0)))) (h "1g7xi6abxxywpgdnv62qhdrckh271r4n9x6g863lbbgck54xf2rd") (f (quote (("use-system-lib"))))))

(define-public crate-xcb-imdkit-0.1.1 (c (n "xcb-imdkit") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "xcb") (r "^0.9.0") (d #t) (k 0)))) (h "18ghxrghy5d255rrv4dcxcxdidnyqlgxaad9sbn81xw8xfb7kh1q") (f (quote (("use-system-lib"))))))

(define-public crate-xcb-imdkit-0.1.2 (c (n "xcb-imdkit") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "xcb") (r "^0.9.0") (d #t) (k 0)))) (h "10b95w31x6fwdx86i04zx80dqf5y05h28gs9yny43bg656v24x37") (f (quote (("use-system-lib"))))))

