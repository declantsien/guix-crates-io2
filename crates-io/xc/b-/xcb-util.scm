(define-module (crates-io xc b- xcb-util) #:use-module (crates-io))

(define-public crate-xcb-util-0.1.0 (c (n "xcb-util") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (d #t) (k 0)))) (h "1m8srvw0fnrb8qwgxjscm9yhz88a7laynicsjha3y4jpanqrzchi") (f (quote (("static") ("icccm"))))))

(define-public crate-xcb-util-0.1.1 (c (n "xcb-util") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (d #t) (k 0)))) (h "01fasj7bzhjzmraw0zfkwgaigskp6vdx9qqmy5k9jly3kvjrrd2s") (f (quote (("static") ("image" "xcb/shm") ("icccm"))))))

(define-public crate-xcb-util-0.1.2 (c (n "xcb-util") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (d #t) (k 0)))) (h "1d8w8l1wj3na3gcbls005qg7hysi98qzc167fwby92xvj2x8pw7v") (f (quote (("static") ("shm" "xcb/shm") ("image") ("icccm") ("ewmh"))))))

(define-public crate-xcb-util-0.1.3 (c (n "xcb-util") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (d #t) (k 0)))) (h "09q71rxd9bwfvirx9f11dn1w9rwadsqpi24ggcylmbk3kn5z0fdq") (f (quote (("static") ("shm" "xcb/shm") ("misc" "icccm") ("image") ("icccm") ("ewmh") ("cursor"))))))

(define-public crate-xcb-util-0.1.4 (c (n "xcb-util") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (d #t) (k 0)))) (h "09jjvklplr1j1nw3l1f7by16fk5v0p7csw3ry2sk68dq5mz5kc19") (f (quote (("thread" "xcb/thread") ("static") ("shm" "xcb/shm") ("misc" "icccm") ("image") ("icccm") ("ewmh") ("cursor"))))))

(define-public crate-xcb-util-0.1.5 (c (n "xcb-util") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (d #t) (k 0)))) (h "1pjxgfn4y21ylsyn62zi1gh84hjjvfxw7jbvzpjs0a47zy2civd2") (f (quote (("thread" "xcb/thread") ("static") ("shm" "xcb/shm") ("misc" "icccm") ("image") ("icccm") ("ewmh") ("cursor"))))))

(define-public crate-xcb-util-0.1.6 (c (n "xcb-util") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (d #t) (k 0)))) (h "0jz591zb96dvnv7m8diwd0hk8p0118kr4w6f70z2xf5hm4l1fglw") (f (quote (("thread" "xcb/thread") ("static") ("shm" "xcb/shm") ("misc" "icccm") ("image") ("icccm") ("ewmh") ("cursor"))))))

(define-public crate-xcb-util-0.1.7 (c (n "xcb-util") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (d #t) (k 0)))) (h "0zyqa5qnplb5prid7vimv9prfgr0a81n3myv3w86f203l481j4xp") (f (quote (("thread" "xcb/thread") ("static") ("shm" "xcb/shm") ("misc" "icccm") ("image") ("icccm") ("ewmh") ("cursor"))))))

(define-public crate-xcb-util-0.1.8 (c (n "xcb-util") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (d #t) (k 0)))) (h "09wyzjnjyzbwvwr80w3mbwwm9za7dpkvkwy4gpzf1c5g0n8qpb5m") (f (quote (("thread" "xcb/thread") ("static") ("shm" "xcb/shm") ("misc" "icccm") ("keysyms") ("image") ("icccm") ("ewmh") ("cursor"))))))

(define-public crate-xcb-util-0.2.0 (c (n "xcb-util") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (d #t) (k 0)))) (h "10scwi0vqlvj88qqhrpf2x2ya0kwfl3gxf4gfny8sbbv2rkf2vjv") (f (quote (("thread" "xcb/thread") ("static") ("shm" "xcb/shm") ("misc" "icccm") ("keysyms") ("image") ("icccm") ("ewmh") ("cursor"))))))

(define-public crate-xcb-util-0.2.0+1 (c (n "xcb-util") (v "0.2.0+1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (d #t) (k 0)))) (h "0h47s18sbfyfp4yrbn9ninjifzpsil3zyzjj5kxfpa65dfl36f22") (f (quote (("thread" "xcb/thread") ("static") ("shm" "xcb/shm") ("misc" "icccm") ("keysyms") ("image") ("icccm") ("ewmh") ("cursor"))))))

(define-public crate-xcb-util-0.2.1 (c (n "xcb-util") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (d #t) (k 0)))) (h "1ayq93ncbrjlzr9yyk7njql2rnplc47nf2pk49is8ja3pq5zqd8d") (f (quote (("thread" "xcb/thread") ("static") ("shm" "xcb/shm") ("misc" "icccm") ("keysyms") ("image") ("icccm") ("ewmh") ("cursor"))))))

(define-public crate-xcb-util-0.2.2 (c (n "xcb-util") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (d #t) (k 0)))) (h "1y0x0xr26d02lbbs3xx633lp8rfyqz690kwxv1401v7mfx0aa5yv") (f (quote (("thread" "xcb/thread") ("static") ("shm" "xcb/shm") ("misc" "icccm") ("keysyms") ("image") ("icccm") ("ewmh") ("cursor")))) (y #t)))

(define-public crate-xcb-util-0.3.0 (c (n "xcb-util") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (d #t) (k 0)))) (h "0j0w9h79icwvmms7wjripj87wiaawfhg7vlz90fxixvvy93kx2a3") (f (quote (("thread" "xcb/thread") ("static") ("shm" "xcb/shm") ("misc" "icccm") ("keysyms") ("image") ("icccm") ("ewmh") ("cursor"))))))

(define-public crate-xcb-util-0.4.0 (c (n "xcb-util") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.10") (d #t) (k 0)))) (h "1q41f63cbmmvw509ri41r5hpvqqvaxvn2pm41jlz16lwb08jvhfp") (f (quote (("thread" "xcb/thread") ("static") ("shm" "xcb/shm") ("render") ("misc" "icccm") ("keysyms") ("image") ("icccm") ("ewmh") ("cursor"))))))

