(define-module (crates-io xc b- xcb-util-cursor-sys) #:use-module (crates-io))

(define-public crate-xcb-util-cursor-sys-0.1.0 (c (n "xcb-util-cursor-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)))) (h "0psarjsakk3w59bl73248mrg340k1nv72n6155whn9vk4gyjjb6r") (l "xcb-cursor")))

(define-public crate-xcb-util-cursor-sys-0.1.1 (c (n "xcb-util-cursor-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)))) (h "03z1xr90fb93lnplxwxki552lvhr6arpywh1x5czg0qgmhr05wam") (l "xcb-cursor")))

(define-public crate-xcb-util-cursor-sys-0.1.2 (c (n "xcb-util-cursor-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "1ks892rwyi1ny9m5mp2qaxsm3j80iwyy4im6j4dnpambypdjkfi8") (l "xcb-cursor")))

(define-public crate-xcb-util-cursor-sys-0.1.3 (c (n "xcb-util-cursor-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "00hprhmwds39qb2lqg21ag55aypzp6cwhz31pldnv0c5mzij8dxi") (l "xcb-cursor")))

(define-public crate-xcb-util-cursor-sys-0.1.4 (c (n "xcb-util-cursor-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)) (d (n "xcb") (r "^1.4.0") (d #t) (k 0)))) (h "00d67fxdzvym65ln988a8x3aakg8bb0drki2bv4ac8s7avrmyrz2") (l "xcb-cursor")))

