(define-module (crates-io xc od xcode-discord-rpc) #:use-module (crates-io))

(define-public crate-xcode-discord-rpc-0.1.0 (c (n "xcode-discord-rpc") (v "0.1.0") (d (list (d (n "cargo-watch") (r "^8.4.0") (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.3") (d #t) (k 0)))) (h "1mrpwlhpjbjn7s7w9g6byklh6rgjn3y726dp6rmnxi1jvrkq8jz2")))

