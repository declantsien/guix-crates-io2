(define-module (crates-io xc el xcell) #:use-module (crates-io))

(define-public crate-xcell-0.0.0 (c (n "xcell") (v "0.0.0") (h "14sc2k1s89w35dwq5si8vhqfaisn9ki537mf4l21qk3nyq7xnhrx") (f (quote (("default"))))))

(define-public crate-xcell-0.1.0 (c (n "xcell") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (t "cfg(windows)") (k 1)) (d (n "xcell-core") (r "^0.1.0") (d #t) (k 0)))) (h "0s2n2fhdzcjk2ck3h2y7ryb9vwvqz962f6mw9ri2acbr1ns4kvgb") (f (quote (("default"))))))

(define-public crate-xcell-0.1.1 (c (n "xcell") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (t "cfg(windows)") (k 1)) (d (n "xcell-core") (r "^0.1.0") (d #t) (k 0)))) (h "1rkygkp5l9glwx0nwwzlbdinpbxg72k3c8dkyrj8ygjdz9f0dl6m") (f (quote (("default"))))))

