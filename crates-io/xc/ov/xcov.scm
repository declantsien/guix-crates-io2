(define-module (crates-io xc ov xcov) #:use-module (crates-io))

(define-public crate-xcov-0.2.0 (c (n "xcov") (v "0.2.0") (h "1izlg21r9ihqghngpvsxkgj2xvmn34fhbpfh81p1i6xxfx25qqqj") (y #t)))

(define-public crate-xcov-0.2.1 (c (n "xcov") (v "0.2.1") (h "0li9gk4siink3h5d1ask9wc2xcpf27rsjhvw4pazj73sr4mcxwgg") (y #t)))

(define-public crate-xcov-0.2.2 (c (n "xcov") (v "0.2.2") (h "0zv3r703jczvma2mvm0zm4ifvmqzx1k87dfv4f01bbxs0xmkyaw9")))

(define-public crate-xcov-0.3.0 (c (n "xcov") (v "0.3.0") (h "033ffqj4xmvp5gcin0vfdw92vygwwj9hsiqavjm7jnaxy4cpvqj6")))

(define-public crate-xcov-0.3.1 (c (n "xcov") (v "0.3.1") (h "1cgjh2hkh3a3qmz38y8rnvk7b80r8fpss7nl32lmx5b57j722g5j")))

