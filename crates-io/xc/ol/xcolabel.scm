(define-module (crates-io xc ol xcolabel) #:use-module (crates-io))

(define-public crate-xcolabel-0.1.0 (c (n "xcolabel") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "1pk1dxnn212rvlv94x3a0ipsi3z9q4qbpqzj9zjahx71qd033w2b")))

(define-public crate-xcolabel-0.2.0 (c (n "xcolabel") (v "0.2.0") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "0vdpiv9fh7k3zfamrf084c4f2s8agyhxff0fvvx01dklx0p9v84s")))

