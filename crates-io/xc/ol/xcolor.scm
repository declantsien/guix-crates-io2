(define-module (crates-io xc ol xcolor) #:use-module (crates-io))

(define-public crate-xcolor-0.1.0 (c (n "xcolor") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.5") (d #t) (k 0)) (d (n "xcb") (r "^0.8.2") (d #t) (k 0)))) (h "1gdl6d6ca9hlj50hgjh0hcdhdmz28gf6jbdkv8q0ggda06h53g2q")))

(define-public crate-xcolor-0.2.0 (c (n "xcolor") (v "0.2.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "xcb") (r "^0.8.2") (d #t) (k 0)))) (h "1aqhj737dgbn2dwc6d9i7rl6bi7l3jski2pjf68fggkk6v7qm6gn")))

(define-public crate-xcolor-0.3.0 (c (n "xcolor") (v "0.3.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 1)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "xcb") (r "^0.8.2") (d #t) (k 0)))) (h "1m651qqm8fi0mb52yaz0b33ac7jv7x9inyhxf31wf12ky05afsgc")))

(define-public crate-xcolor-0.3.1 (c (n "xcolor") (v "0.3.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 1)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "xcb") (r "^0.8.2") (d #t) (k 0)))) (h "1sg6449n5chkxzqcjj1harg33rw5k0cki5r3xhdr6f367psl1p8s")))

(define-public crate-xcolor-0.4.0 (c (n "xcolor") (v "0.4.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 1)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "xcb") (r "^0.8.2") (f (quote ("shape"))) (d #t) (k 0)))) (h "1h4kifa2y64vhgya78h2ks5kwyz9ksdzb0piz4p847nyj5k4652j")))

(define-public crate-xcolor-0.5.0 (c (n "xcolor") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "nom") (r "^6.2") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib" "xcursor"))) (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("xlib_xcb"))) (d #t) (k 0)))) (h "1br8041m2cs528ys7n9bb7yv23v4gp06n7lf9fybjm8sv52b53dh")))

(define-public crate-xcolor-0.5.1 (c (n "xcolor") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "x11") (r "^2") (f (quote ("xlib" "xcursor"))) (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("xlib_xcb"))) (d #t) (k 0)))) (h "1gzpkk9z659zdf07a087v4461wc89qsr6crdmd1px0s2z59n89z4")))

