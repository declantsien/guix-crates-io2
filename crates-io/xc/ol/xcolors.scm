(define-module (crates-io xc ol xcolors) #:use-module (crates-io))

(define-public crate-xcolors-0.1.0 (c (n "xcolors") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib"))) (d #t) (k 0)))) (h "17fq3xkrk4kl3900cjlp3sijx7qj9j27pi8ajssak18gyy9wbpvy") (y #t)))

