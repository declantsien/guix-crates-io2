(define-module (crates-io xc li xcli) #:use-module (crates-io))

(define-public crate-xcli-0.1.0 (c (n "xcli") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.2") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.3.1") (d #t) (k 0)))) (h "1393my6rcxdlcxxiplxya0q6cybg9y82plhz8c7im2j85djk43kv")))

(define-public crate-xcli-0.2.0 (c (n "xcli") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.2") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.3.1") (d #t) (k 0)))) (h "1w9x3yjfbn3xnrc7ds5qwywrdiz6v0sraf8nh4ilyaqd5w1vr0gs")))

(define-public crate-xcli-0.3.0 (c (n "xcli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.2") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "13i29j4v0l51zlwn6phryn5r7hy77xxjy3aizikcyasglq7xai1j")))

(define-public crate-xcli-0.5.0 (c (n "xcli") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.2") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "09j36jvwxz2025329p08kmfmqfh20s5hlk9m6vy40v61fjq131v9")))

(define-public crate-xcli-0.5.1 (c (n "xcli") (v "0.5.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.2") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1ywcvqrbg8sjs1h1741h9nkz9yf1bfjqz9h49xxg7k2nfrqb7dkl")))

(define-public crate-xcli-0.5.2 (c (n "xcli") (v "0.5.2") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.2") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0hprxxpx5qm5a2qn55bv9gnb823g53my70wn2dmb2sb5w3jh6wxi")))

