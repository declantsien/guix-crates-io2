(define-module (crates-io oe is oeis-utils) #:use-module (crates-io))

(define-public crate-oeis-utils-0.1.1 (c (n "oeis-utils") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "05wdgjw8dk89i1yfl1wijvmbgk6p2im0lgx70bh30xwp7vsdwfj2")))

(define-public crate-oeis-utils-0.2.1 (c (n "oeis-utils") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0brjq0hyg7yrk199dhjzz6zqy37cy4wc8d2yjyr2a52rqr32s2jg")))

(define-public crate-oeis-utils-0.2.2 (c (n "oeis-utils") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0k0rk3g3ma2y4b1s8ravvnd7jm5mip13k37fr6ph2xfa9r07kawd")))

(define-public crate-oeis-utils-0.3.0 (c (n "oeis-utils") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1jcbh2qvba0hpsjm6aj1shhms2z2rn3dmn4vh43vy4si3rx9kx3g")))

(define-public crate-oeis-utils-0.3.1 (c (n "oeis-utils") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0xby6ali757qqskj860k9yracwcsc6gc76k8ryjp73lg8swnhb3g")))

(define-public crate-oeis-utils-0.3.2 (c (n "oeis-utils") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0mg30v498854ac2wk6ig4palb3qgil6c7d1w7x2a8hb55vzjb3jn")))

