(define-module (crates-io oe cl oecli) #:use-module (crates-io))

(define-public crate-oecli-0.1.0 (c (n "oecli") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "14dd494nvxp827ir1m83awsnkcand0zagyndry0n352s3wjwnhb9")))

(define-public crate-oecli-0.1.1 (c (n "oecli") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1109i9ns94kribgafksva3jc88gj3cjhndrqxd6jkrh1i6195sss")))

