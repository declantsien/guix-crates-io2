(define-module (crates-io iw eb iweb) #:use-module (crates-io))

(define-public crate-iweb-0.1.0 (c (n "iweb") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("console" "Document" "Window" "Element" "NodeList" "HtmlElement" "Node" "Performance" "Attr" "Storage" "Headers" "Request" "RequestInit" "RequestMode" "Response"))) (d #t) (k 0)))) (h "15m0ksjrvkfg9kiibhng310l97llr8rkrm0hhl27hip0wgf5fhlp") (r "1.56")))

