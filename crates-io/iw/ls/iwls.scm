(define-module (crates-io iw ls iwls) #:use-module (crates-io))

(define-public crate-iwls-0.0.1 (c (n "iwls") (v "0.0.1") (d (list (d (n "clap") (r ">= 2.13.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.95") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r ">= 0.8.2") (d #t) (k 0)) (d (n "order-stat") (r ">= 0.1.3") (d #t) (k 0)) (d (n "users") (r ">= 0.5.2") (d #t) (k 0)) (d (n "wifiscanner") (r ">= 0.3.3") (d #t) (k 0)))) (h "0zqmjwj5mn6mkci7bqqylglk7hf7bn3h847wjp6yqdvswgmymd3h") (f (quote (("default")))) (y #t)))

(define-public crate-iwls-0.0.2 (c (n "iwls") (v "0.0.2") (d (list (d (n "clap") (r ">= 2.13.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.95") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r ">= 0.8.2") (d #t) (k 0)) (d (n "order-stat") (r ">= 0.1.3") (d #t) (k 0)) (d (n "users") (r ">= 0.5.2") (d #t) (k 0)) (d (n "wifiscanner") (r ">= 0.3.3") (d #t) (k 0)))) (h "1q8z2l4c8nx6adligmj120gh9bj5g7gw4gkj46755n84ga6y8d3r") (f (quote (("default"))))))

(define-public crate-iwls-0.0.3 (c (n "iwls") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "order-stat") (r "^0.1") (d #t) (k 0)) (d (n "users") (r "^0.9") (d #t) (k 0)) (d (n "wifiscanner") (r "^0.5") (d #t) (k 0)))) (h "092w6llmkk9s7ijlqxb92k07lapsp0hyai7zs5gpiwhav4hnbr36") (f (quote (("default"))))))

