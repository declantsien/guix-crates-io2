(define-module (crates-io iw li iwlib) #:use-module (crates-io))

(define-public crate-iwlib-0.1.0 (c (n "iwlib") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "iwlib_sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)))) (h "1z854laaz6mydgaq0dbjdx72sfifqjprirdbayww3wfil1371c3z")))

(define-public crate-iwlib-0.1.1 (c (n "iwlib") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "iwlib_sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)))) (h "179vr86m6cp4gbahri01pfspzmanziiaw379khjfnfkvd8b14xiz")))

