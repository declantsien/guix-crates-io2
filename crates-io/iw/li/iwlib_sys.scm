(define-module (crates-io iw li iwlib_sys) #:use-module (crates-io))

(define-public crate-iwlib_sys-0.1.0 (c (n "iwlib_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1hgrjd845ay9bhjcp7ikcb7d0zn08xfqdlfa706dm7qn3aqcqwn6")))

(define-public crate-iwlib_sys-0.1.1 (c (n "iwlib_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0vhyv3xw1mcm8ymzkmc954lcv2ry7jvjmhfwvskf4xhspfjz009a")))

