(define-module (crates-io f8 #{0a}# f80a18) #:use-module (crates-io))

(define-public crate-F80A18-0.0.0 (c (n "F80A18") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "hmac") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "smol_str") (r "^0.1.10") (d #t) (k 0)))) (h "1llqsjy3akpw685caf9dxy44gz279bcqcr32dz40gn9i3qf0k7h1") (f (quote (("webhooks" "hmac" "sha2") ("default" "webhooks") ("async"))))))

