(define-module (crates-io ch au chaum_pedersen_auth) #:use-module (crates-io))

(define-public crate-chaum_pedersen_auth-0.1.0 (c (n "chaum_pedersen_auth") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "16kkhwa95i67b6j45awwv737mdj293icw3mqssmw72x40mhb3wsm")))

(define-public crate-chaum_pedersen_auth-0.1.1 (c (n "chaum_pedersen_auth") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "17mk66khhwjqlv7z2fq1mvnp3j2qzy8fx468ydvql6i000rj0zcc")))

(define-public crate-chaum_pedersen_auth-0.1.2 (c (n "chaum_pedersen_auth") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "13965vxlfaca8i2n7834ddiihic310qd8sfv71ws926xxk3cbfxz")))

(define-public crate-chaum_pedersen_auth-0.1.3 (c (n "chaum_pedersen_auth") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.10.2") (d #t) (k 2)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "0cpwsj20v2p5hqy6mqpzfhg49lx8xf1qnbdj4h79r9dbnh3j1f7w")))

