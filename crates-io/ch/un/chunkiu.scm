(define-module (crates-io ch un chunkiu) #:use-module (crates-io))

(define-public crate-chunkiu-0.1.0 (c (n "chunkiu") (v "0.1.0") (h "1g9ckdfk25ffk59vmk20arl8lgb5wilvcwi69d1dzs6v841blbyv") (y #t)))

(define-public crate-chunkiu-0.0.1 (c (n "chunkiu") (v "0.0.1") (h "0ksfs9wranwgvxkmpb0bxdd0cmj5w8vyj6nwddwhdxqxsdvdh8ij") (y #t)))

