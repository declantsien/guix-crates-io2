(define-module (crates-io ch un chunked_transfer_encoding) #:use-module (crates-io))

(define-public crate-chunked_transfer_encoding-0.0.0 (c (n "chunked_transfer_encoding") (v "0.0.0") (d (list (d (n "http") (r "^0.2") (o #t) (k 0)))) (h "0r49qh4hz9572bkady0k8y90557iay3znlmmjwwrv5aps2rjfha4") (y #t)))

(define-public crate-chunked_transfer_encoding-0.1.0 (c (n "chunked_transfer_encoding") (v "0.1.0") (d (list (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (o #t) (k 0)) (d (n "http") (r "^0.2") (o #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0vr2m257v3g013rkd74x250r4kk7zzl1cggnqgh9bd8frsnc0y3b") (f (quote (("stream" "futures-util" "pin-project-lite") ("default" "stream") ("_priv_test_http" "http"))))))

