(define-module (crates-io ch un chunked-buffer) #:use-module (crates-io))

(define-public crate-chunked-buffer-0.1.0 (c (n "chunked-buffer") (v "0.1.0") (h "0f8iwr41539k5vw31wc6rw15d6fs44rglyvhymqz99pk5yd0jzbs") (r "1.60.0")))

(define-public crate-chunked-buffer-0.1.1 (c (n "chunked-buffer") (v "0.1.1") (h "1322cjsyzd1ik9gassxiyrndf7mzym9wn8rjqymd6x5d4vyvjx43") (r "1.60.0")))

(define-public crate-chunked-buffer-0.1.2 (c (n "chunked-buffer") (v "0.1.2") (h "093rwi031aql19vfbxcmhj0nqq3268parxm9wk0qh73s0pl1rs1m") (y #t) (r "1.60.0")))

(define-public crate-chunked-buffer-0.1.3 (c (n "chunked-buffer") (v "0.1.3") (h "1cc2hwwn68lmxdqp0gbyhrwdhabarqac0k962fvh3ficypv5bpmp") (r "1.60.0")))

(define-public crate-chunked-buffer-0.2.0 (c (n "chunked-buffer") (v "0.2.0") (h "129nwl3h87d3aq390dy90m3sknyh8h9lgbvnnwkb0pl32k9jnqps") (r "1.60.0")))

