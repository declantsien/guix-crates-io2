(define-module (crates-io ch un chunker) #:use-module (crates-io))

(define-public crate-chunker-0.1.0 (c (n "chunker") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "progression") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "1clqzdyf0dvxlsfn6pdyvrhiyqhrwmrabf0a8y21kxws31fc17mx") (f (quote (("default" "progression")))) (y #t)))

(define-public crate-chunker-0.1.1 (c (n "chunker") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "progression") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "18ii35j6ac3yrfcljkm9na1z08z8z2igwwqpvdg1626np5kqimvs") (f (quote (("default" "progression")))) (y #t)))

(define-public crate-chunker-0.1.2 (c (n "chunker") (v "0.1.2") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "progression") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "1jpkx9v89nl3anmxhr1314495xia0w834p9ni12mn7ar65a18n0j") (f (quote (("default" "progression")))) (y #t)))

(define-public crate-chunker-0.1.3 (c (n "chunker") (v "0.1.3") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "progression") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0msxqprzgic7qqvmb8bjfwq82jmaxqssynp0qbavqmgw1fabl52z") (f (quote (("default" "progression")))) (y #t)))

(define-public crate-chunker-0.1.4 (c (n "chunker") (v "0.1.4") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "progression") (r "^0.1.11") (o #t) (d #t) (k 0)))) (h "18nlks85x73f13bp9fb2vjbq094j9fbrlfjpdqkh3v0qm9gw3ld5") (f (quote (("default" "progression")))) (y #t)))

(define-public crate-chunker-0.1.5 (c (n "chunker") (v "0.1.5") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "progression") (r "^0.1.11") (o #t) (d #t) (k 0)))) (h "0dv7m5936j5v5v5703z7v3w308l2i8sm6cw25pp7zg27bxz88ilh") (f (quote (("default" "progression"))))))

