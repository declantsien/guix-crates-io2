(define-module (crates-io ch un chunkio) #:use-module (crates-io))

(define-public crate-chunkio-0.0.1 (c (n "chunkio") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (k 0)) (d (n "thiserror") (r "^1.0") (k 0)) (d (n "tokio") (r "^1.37") (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (k 0)))) (h "1rp3b0s8g8g0y8i1xa2ckh504918ii6s5abhfgrbykhkbv3xsqks")))

