(define-module (crates-io ch un chunked_transfer) #:use-module (crates-io))

(define-public crate-chunked_transfer-0.2.0 (c (n "chunked_transfer") (v "0.2.0") (h "15yq75280f7mnlxb983jns4mn301c26yhk5wxyzfyw01z66r8dng")))

(define-public crate-chunked_transfer-0.3.0 (c (n "chunked_transfer") (v "0.3.0") (h "12610sxinc6xmg9z7zbkssj81xiy72pvdm37phqji5fmimskjs65")))

(define-public crate-chunked_transfer-0.3.1 (c (n "chunked_transfer") (v "0.3.1") (h "11yghnd24w0i9p8g368c3pg7qh9nfz7kgri6pywja9pnmakj13a9")))

(define-public crate-chunked_transfer-1.0.0 (c (n "chunked_transfer") (v "1.0.0") (h "0v0xlp5kqwwyrd71ssy6l4jnm7f7cd4h3immsx5s226yaijyp2zr")))

(define-public crate-chunked_transfer-1.1.0 (c (n "chunked_transfer") (v "1.1.0") (h "1pddhllvjarnl2wxlqa6yszi6kiyhjr9j9v2rcwcix5r15zn92av")))

(define-public crate-chunked_transfer-1.2.0 (c (n "chunked_transfer") (v "1.2.0") (h "14vi1bj4bmilnjwp39vgvdxp3cs8p9yr93ysf4gkg0i72cayna8x")))

(define-public crate-chunked_transfer-1.3.0 (c (n "chunked_transfer") (v "1.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1jl2ms4ylf6hlaylkjhwp3y4rd996z6qpkzkgcb5gzm88mfhcxvl")))

(define-public crate-chunked_transfer-1.4.0 (c (n "chunked_transfer") (v "1.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0bkdlsrszfcscw3j6yhs7kj6jbp8id47jjk6h9k58px47na5gy7z")))

(define-public crate-chunked_transfer-1.4.1 (c (n "chunked_transfer") (v "1.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "06kx4083avirgifmj192qy8hkyqcqkq60gxg91r4vq36hqw9396c")))

(define-public crate-chunked_transfer-1.5.0 (c (n "chunked_transfer") (v "1.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "00a9h3csr1xwkqrzpz5kag4h92zdkrnxq4ppxidrhrx29syf6kbf")))

