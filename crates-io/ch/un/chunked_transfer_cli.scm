(define-module (crates-io ch un chunked_transfer_cli) #:use-module (crates-io))

(define-public crate-chunked_transfer_cli-0.1.0 (c (n "chunked_transfer_cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "chunked_transfer") (r "^1.4.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sgzaq2vlrxybwc24nhixk8jj1y3yvi6imx1lbbxgc8rfblyxsyj")))

(define-public crate-chunked_transfer_cli-0.1.1 (c (n "chunked_transfer_cli") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "chunked_transfer") (r "^1.4.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "15f1i2i063bmll1vc9bzc8kv4vnzsv7va4c3ml17dxjmk10f0rxp") (r "1.65")))

