(define-module (crates-io ch un chunks) #:use-module (crates-io))

(define-public crate-chunks-0.1.0 (c (n "chunks") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ij9w91h26nsiphfjax1r2f7m34lbai8w8qicm1gwr1z9nghmym2")))

(define-public crate-chunks-0.1.1 (c (n "chunks") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1qbmk9c3jjcy02jwl9q4y822n5m73n1ggip6pypwgik7vzhaiz8w")))

