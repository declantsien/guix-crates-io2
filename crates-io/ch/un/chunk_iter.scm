(define-module (crates-io ch un chunk_iter) #:use-module (crates-io))

(define-public crate-chunk_iter-0.1.0 (c (n "chunk_iter") (v "0.1.0") (h "0whp72wdlfk42sxdsirl41pz2nhh45z9qxmgyc8s0ifz0zhayrdz") (y #t)))

(define-public crate-chunk_iter-0.1.1 (c (n "chunk_iter") (v "0.1.1") (h "0yqw5ayfqb17sjd19zm42v80n42cw6k27ygjzg7l2gp3wiz7pmcz")))

(define-public crate-chunk_iter-0.1.2 (c (n "chunk_iter") (v "0.1.2") (d (list (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "08nqmb1visgr949kbkqm47390i7673sndd4ymg62y5k1y5952bna")))

(define-public crate-chunk_iter-0.1.3 (c (n "chunk_iter") (v "0.1.3") (d (list (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "00hfdzn9qxx51kncfqdz3vnivcyrj888vwi8lir8m4hlfi8cbvvw")))

(define-public crate-chunk_iter-0.1.4 (c (n "chunk_iter") (v "0.1.4") (d (list (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "0g4rds0wwflvghz4389ianksgc0f6ddcir3fqsv2qza89g0ck14n")))

(define-public crate-chunk_iter-0.1.5 (c (n "chunk_iter") (v "0.1.5") (d (list (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "16jv8glz70r2fhbqj0a1pc8lmybm7znam3ykmfvn5sj33h0m28q7")))

