(define-module (crates-io ch un chunk-json-lite) #:use-module (crates-io))

(define-public crate-chunk-json-lite-0.1.0 (c (n "chunk-json-lite") (v "0.1.0") (d (list (d (n "count-write") (r "^0.1.0") (d #t) (k 0)) (d (n "human-size") (r "^0.4.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "main_error") (r "^0.1.0") (d #t) (k 0)))) (h "0pzrkbjvnj2b373xflc1f3kbv3wdh4naqqzdpr7q1dk83fmwjgzi")))

