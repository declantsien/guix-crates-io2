(define-module (crates-io ch un chunked-bytes) #:use-module (crates-io))

(define-public crate-chunked-bytes-0.1.0-beta.1 (c (n "chunked-bytes") (v "0.1.0-beta.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pin-project") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0wvwjgqd6xa3g2dsvj6rkln1dfhqag5sa7sbg8cv6wzcv757gpjc")))

(define-public crate-chunked-bytes-0.1.0-beta.2 (c (n "chunked-bytes") (v "0.1.0-beta.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pin-project") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "rt-threaded" "tcp" "macros"))) (d #t) (k 2)))) (h "1l028gmjm3w41pgzr48wzyklfakdjk6dbih9idb249ycd5b2qxhk")))

(define-public crate-chunked-bytes-0.1.0-beta.3 (c (n "chunked-bytes") (v "0.1.0-beta.3") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pin-project") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "rt-threaded" "tcp" "macros"))) (d #t) (k 2)))) (h "0j2v19lw1i0cg9b51mkg8skbj1jivn09yga679xxw9c382cvzwnl")))

(define-public crate-chunked-bytes-0.1.0-beta.4 (c (n "chunked-bytes") (v "0.1.0-beta.4") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pin-project") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "rt-threaded" "tcp" "macros"))) (d #t) (k 2)))) (h "17vibs9ijdz0p6icpv4j3ixbc1lp3qn8i9c9kaqr035m8svqwnhr")))

(define-public crate-chunked-bytes-0.1.0 (c (n "chunked-bytes") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "generic-tests") (r "^0.1.0") (d #t) (k 2)) (d (n "pin-project") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "rt-threaded" "tcp" "macros"))) (d #t) (k 2)))) (h "17iv1x78d02qqc3k5np45prksl10acnisgdvn98j7plw3vj5xq92")))

(define-public crate-chunked-bytes-0.2.0 (c (n "chunked-bytes") (v "0.2.0") (d (list (d (n "bytes") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "generic-tests") (r "^0.1.1") (d #t) (k 2)) (d (n "pin-project") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.3") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1gdy7x7my74azdmdji4345prjhkyf2s10xr1i08cxqf82x2k2hn8")))

(define-public crate-chunked-bytes-0.3.0 (c (n "chunked-bytes") (v "0.3.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 2)) (d (n "generic-tests") (r "^0.1.1") (d #t) (k 2)) (d (n "pin-project") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "04klic06prfzy3j7x0b6cbs8gmq0i21c2458v1k6i3hz16979knz")))

