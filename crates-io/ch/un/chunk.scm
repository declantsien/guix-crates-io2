(define-module (crates-io ch un chunk) #:use-module (crates-io))

(define-public crate-chunk-0.1.0 (c (n "chunk") (v "0.1.0") (d (list (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1vaiv8p480fc6dna623r80mmw1822hdfs1zm3pq4c82sn32qpg7g")))

