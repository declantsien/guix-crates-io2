(define-module (crates-io ch il chillup) #:use-module (crates-io))

(define-public crate-chillup-0.1.0 (c (n "chillup") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "select") (r "^0.4.3") (d #t) (k 0)))) (h "0kd4q5vl8h6l4520680vymy08k25lxnki78f2kpc6iwybfa7034w")))

(define-public crate-chillup-0.2.0 (c (n "chillup") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "select") (r "^0.4.3") (d #t) (k 0)))) (h "0x09a1jc1zcbnbqndpabgglb4p132fjvg3h4ba9c9wdwvnr79hd1")))

(define-public crate-chillup-0.2.1 (c (n "chillup") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "select") (r "^0.4.3") (d #t) (k 0)))) (h "0715wbnysijnqg4d2b2sd0s5wnmlk42pj6nziib404m9wdb4vbfx")))

(define-public crate-chillup-0.4.0 (c (n "chillup") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0r5jr6nzd93gaxd0hdnv05w3fa74iin6kig74l5ybkp0kzxmv4wr")))

