(define-module (crates-io ch oy choyen_5000) #:use-module (crates-io))

(define-public crate-choyen_5000-0.1.0 (c (n "choyen_5000") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.17.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "0kjny2h5vwmym9yhcfb5118vnbg7wmyq3kx2q41bq7nvjyfhpvzz")))

(define-public crate-choyen_5000-0.1.1 (c (n "choyen_5000") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.17.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "0v5wfikb6yjfrwn45sbjn0yb6h78a5cfj2qwzf34znldn13s5fm8")))

(define-public crate-choyen_5000-0.1.2 (c (n "choyen_5000") (v "0.1.2") (d (list (d (n "pyo3") (r "^0.17.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "0gg7syr2h70qk05114yvcmwy5phvmhh984gd4ipscc4b8ava3mp8")))

(define-public crate-choyen_5000-0.1.3 (c (n "choyen_5000") (v "0.1.3") (d (list (d (n "pyo3") (r "^0.17.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "05y4wi1nar6ps2i1krissmji79azrj8c443xnhdd3w2v9d8zrpgi")))

(define-public crate-choyen_5000-0.2.0 (c (n "choyen_5000") (v "0.2.0") (d (list (d (n "pyo3") (r "^0.17.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "11rrhrf1wp4lg5iabn787afsr1fgwn85b6mj3yd59gsn51zis9lp")))

(define-public crate-choyen_5000-0.2.1 (c (n "choyen_5000") (v "0.2.1") (d (list (d (n "pyo3") (r "^0.17.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "1gvck8fmr8x33sckqi7689g31ypkgpi6a2brqbqjnkp5rzb2a2ay")))

