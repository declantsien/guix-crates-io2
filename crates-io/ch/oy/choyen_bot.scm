(define-module (crates-io ch oy choyen_bot) #:use-module (crates-io))

(define-public crate-choyen_bot-0.2.1 (c (n "choyen_bot") (v "0.2.1") (d (list (d (n "choyen_5000") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "teloxide") (r "^0.11.0") (f (quote ("macros" "ctrlc_handler"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1wkbdjczpc3aix9vkdrnjwb89r4yk5njpfgpza1khrym4bb6yx5h")))

