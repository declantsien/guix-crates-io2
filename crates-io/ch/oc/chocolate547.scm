(define-module (crates-io ch oc chocolate547) #:use-module (crates-io))

(define-public crate-chocolate547-0.1.0 (c (n "chocolate547") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("std" "cargo"))) (k 0)))) (h "08vxhpy5p6ifngfk1mas8d5y37gyfdcvihpzzqjk64smq1jwdfsl")))

(define-public crate-chocolate547-0.2.0 (c (n "chocolate547") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("std" "cargo"))) (k 0)))) (h "0bam3yaihniwy3n6hz05cgbqndamkv1xws06v1gbrlgg7xdhn0qr")))

