(define-module (crates-io ch oc choco) #:use-module (crates-io))

(define-public crate-choco-0.1.1 (c (n "choco") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (k 0)))) (h "04ngf5pbkxjv3f5alxgcmi11fhvdsy4r0zjg96fmlf5p1hqslva9")))

(define-public crate-choco-0.1.2 (c (n "choco") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (k 0)))) (h "18d1j7s0yj9qqdd0m3zzlw1slbj5z03lr2mj66lvgvpkcj0f7wqc")))

(define-public crate-choco-0.2.0 (c (n "choco") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (k 0)))) (h "1m48bc3jkycrpq8qys71w5sz3x9aqi0bpi4gi050d8jzxvb910y5")))

(define-public crate-choco-0.2.1 (c (n "choco") (v "0.2.1") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (k 0)))) (h "01cihfbjk1nbmfxh0p2vs1d00yqplcnrfh0dwwlaajq89dfy6v85")))

(define-public crate-choco-0.2.2 (c (n "choco") (v "0.2.2") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (k 0)))) (h "1vlrdf3znr2wrs5j1w4ppnjc49cgp0zn4va0gglbgrkh7hyvabss")))

