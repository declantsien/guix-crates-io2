(define-module (crates-io ch oc chocomint) #:use-module (crates-io))

(define-public crate-chocomint-0.1.0-alpha.1 (c (n "chocomint") (v "0.1.0-alpha.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wvssn51cyzl7kz85pdnxvdh1qkm1h47siyc00nam8nds37bmpxh")))

(define-public crate-chocomint-0.1.0-alpha.3.1 (c (n "chocomint") (v "0.1.0-alpha.3.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lbwmp81ys8dzamd54hmjbjp81idpa9kdsdg90hnl1dlv35vl4ih")))

(define-public crate-chocomint-0.1.0-rc (c (n "chocomint") (v "0.1.0-rc") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01ihl208v9x06pzg7amy05cjb15f2jvw2lnkr7y6zr3yd9zp3ds1")))

(define-public crate-chocomint-0.1.0 (c (n "chocomint") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "073bw6lcz8q5hlh8h7syrfzmhggardvph6vlylh1wl6dy04499px")))

