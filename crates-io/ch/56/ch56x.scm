(define-module (crates-io ch #{56}# ch56x) #:use-module (crates-io))

(define-public crate-ch56x-0.1.5 (c (n "ch56x") (v "0.1.5") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1hvprf7bmy4d697ffnq96s5in9spvpahka7841m8611n5irhg5z1") (f (quote (("rt" "riscv-rt") ("default" "rt") ("ch56x"))))))

(define-public crate-ch56x-0.1.6 (c (n "ch56x") (v "0.1.6") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0j4hgc50sdcij735hrbm0bph0r68yak4xid8rmgbqnvj886yxgxi") (f (quote (("rt" "riscv-rt") ("default" "rt") ("ch56x"))))))

