(define-module (crates-io ch #{56}# ch569-hal) #:use-module (crates-io))

(define-public crate-ch569-hal-0.0.1 (c (n "ch569-hal") (v "0.0.1") (d (list (d (n "ch569-pac") (r "^0.1") (f (quote ("critical-section"))) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (f (quote ("restore-state-bool"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (d #t) (k 0)))) (h "1b5s6vwb0s53skckn3avhmi8bqwm8ll1ic1llbar5y59xzpxd4ax")))

