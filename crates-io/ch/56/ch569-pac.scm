(define-module (crates-io ch #{56}# ch569-pac) #:use-module (crates-io))

(define-public crate-ch569-pac-0.1.0 (c (n "ch569-pac") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0pm719mcimilzmapjfjnfykbchg58hkllkxs49vy139h2p3n59gc") (f (quote (("rt" "riscv-rt") ("critical-section"))))))

