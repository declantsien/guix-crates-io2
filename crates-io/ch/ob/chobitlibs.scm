(define-module (crates-io ch ob chobitlibs) #:use-module (crates-io))

(define-public crate-chobitlibs-0.3.1 (c (n "chobitlibs") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "15wviir76ycrbcn9x0fcgqircsyxrd73f4b1l4kfsy6v8i1p7ka8")))

(define-public crate-chobitlibs-0.3.2 (c (n "chobitlibs") (v "0.3.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "01vjnwpbf71cnnkww4daiqaq0fzi2534sbni9mmdvxfy872l5q4l")))

(define-public crate-chobitlibs-0.3.3 (c (n "chobitlibs") (v "0.3.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "02ifpqy32fzpgrszxaxnh42l86g6zxn1pww5kvd1j6dl8dypcabk")))

(define-public crate-chobitlibs-0.4.0 (c (n "chobitlibs") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0hc1ym73cp8w5y4hskdh66mbznflhvp5pyblspqmwrqjjrqzgvxz")))

(define-public crate-chobitlibs-0.4.1 (c (n "chobitlibs") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1r9dcc2mzvj1h9fdqwkd0iy519nz87p1n3q4z2bvpkffbd472dnq")))

(define-public crate-chobitlibs-0.4.2 (c (n "chobitlibs") (v "0.4.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1c57lmh6fiwx2qp9i0k5h1s3w5qjdxqh8q536c24p2lcq1qqlxi2")))

(define-public crate-chobitlibs-0.5.0 (c (n "chobitlibs") (v "0.5.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "19gym0xdhcq8hymf8s1nip8nlk3svn6sxfny28zhkg4q9g0qrgfd")))

(define-public crate-chobitlibs-0.5.1 (c (n "chobitlibs") (v "0.5.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1a4hqyd3smc749lpvzd1y9mvaw7l2491k3xqrpcgwjbnvyfys42p")))

(define-public crate-chobitlibs-0.6.0 (c (n "chobitlibs") (v "0.6.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1hn8685wdgrjx2lfar53jnhz5zmb98vvrnmg532yzbm8vdqn19pw")))

(define-public crate-chobitlibs-0.7.0 (c (n "chobitlibs") (v "0.7.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1n26c1g40sblw99161396ay4j6c3pmjzc0bpjvgycz87ygp3ml60")))

(define-public crate-chobitlibs-0.8.0 (c (n "chobitlibs") (v "0.8.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "15k9iwn3d5xmvspspzn2inn6pswrc2z7mv3brskjl3yqj75qib5d")))

