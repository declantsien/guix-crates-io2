(define-module (crates-io ch or chore) #:use-module (crates-io))

(define-public crate-chore-0.1.0 (c (n "chore") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0g4dc755s54bqr00i7ylvb36l5qcr71lmklqmsjm653rfd4n3aik")))

