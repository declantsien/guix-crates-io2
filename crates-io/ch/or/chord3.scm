(define-module (crates-io ch or chord3) #:use-module (crates-io))

(define-public crate-chord3-0.3.0 (c (n "chord3") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "pdf") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1jcq3sc9m6zxa54s2d1929c7xx09gr8ychlvr20qil3gfzmzvznr")))

(define-public crate-chord3-0.3.2 (c (n "chord3") (v "0.3.2") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pdf-canvas") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0cxyz36g874pji6w2y03xkw0cn7afgkfgfa88nnzk6s6pxgh97py")))

(define-public crate-chord3-0.3.4 (c (n "chord3") (v "0.3.4") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pdf-canvas") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "16n4d677650yd054p1bkwyywx8y9ixhqcirsxssdfc65yw5yydd6")))

