(define-module (crates-io ch or chord) #:use-module (crates-io))

(define-public crate-chord-0.1.0 (c (n "chord") (v "0.1.0") (h "1v1d1j5isn5limkd6ypvqkdgnwiha5ggjmi282nnmwid09hjn10s")))

(define-public crate-chord-0.1.1 (c (n "chord") (v "0.1.1") (d (list (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "0964z7s3v89c4l0ldf61gclicqv8cc974xgmm5h293g962d1sm3z")))

(define-public crate-chord-0.1.3 (c (n "chord") (v "0.1.3") (d (list (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "0d3rrk6m4z9m7sl7b6q5sqi0rflw3fva2b4jinpvsiqjl2jfw9af")))

(define-public crate-chord-0.1.4 (c (n "chord") (v "0.1.4") (d (list (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (d #t) (k 0)))) (h "0qlzx5bsk5y13gg96cj0y0spzg9iwgcvz8bxnjzbxi51bigkirj2")))

(define-public crate-chord-0.1.5 (c (n "chord") (v "0.1.5") (d (list (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)))) (h "0jygz4azwhks8771q2cfiy03x4idahwdikv6yi46ck65f9xn2h4k")))

(define-public crate-chord-0.1.6 (c (n "chord") (v "0.1.6") (d (list (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "ureq") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)))) (h "0zf4362iszrb0axkmpb93bna2snpw719xsnipmg4l96qh7s49pxm")))

(define-public crate-chord-0.2.0 (c (n "chord") (v "0.2.0") (d (list (d (n "ureq") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)))) (h "0xhvvxvjv6hg7n64aaha5nqiprqi2fcq1204n748p6a8g4v1ppib")))

(define-public crate-chord-0.2.1 (c (n "chord") (v "0.2.1") (d (list (d (n "ureq") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)))) (h "1xm2qhnllyljckzjrhbvfkylzv03ibrvvjpr5pkzx4l76slrhl76")))

(define-public crate-chord-0.8.0 (c (n "chord") (v "0.8.0") (d (list (d (n "ureq") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)))) (h "1l5rp5jmkb273sagqg8qwxpbjiqpld9jiz0b975lfq0cckqs20q0")))

(define-public crate-chord-0.8.1 (c (n "chord") (v "0.8.1") (d (list (d (n "ureq") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)))) (h "0asxyncmrh4zvxs1gqpqccshfmkcfnfrqf73hpx9nl7vvsms4z5l")))

