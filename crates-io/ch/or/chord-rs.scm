(define-module (crates-io ch or chord-rs) #:use-module (crates-io))

(define-public crate-chord-rs-0.1.0 (c (n "chord-rs") (v "0.1.0") (d (list (d (n "chord-capnp") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "chord-grpc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (o #t) (d #t) (k 0)))) (h "144826nqnl7kqgzy9dpm5a1hqzq62n1kwgdd6izwis98xwsix0bi") (f (quote (("default")))) (s 2) (e (quote (("grpc" "dep:chord-grpc" "dep:tonic") ("capnp" "dep:chord-capnp"))))))

