(define-module (crates-io ch or chorus-macros) #:use-module (crates-io))

(define-public crate-chorus-macros-0.1.0 (c (n "chorus-macros") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1r5qa8jbqj2khhvs4iqfnwlaj12vr458ivi5xcha4n3rxi1srxnv")))

(define-public crate-chorus-macros-0.2.0 (c (n "chorus-macros") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1c0jfmh4v1ad36l5z8mf5fp988jhrm0bvnys2xaq2vwj1fk4a5d8")))

(define-public crate-chorus-macros-0.3.0 (c (n "chorus-macros") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0wkjj7jwva6jynamhch6g8336vckg2jdw7r6pkkcd1n41dq22hny")))

