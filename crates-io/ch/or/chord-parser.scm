(define-module (crates-io ch or chord-parser) #:use-module (crates-io))

(define-public crate-chord-parser-1.0.0 (c (n "chord-parser") (v "1.0.0") (h "0fzfilx6c949gws3cc5y5rqd63ym0l8bdrbh7l3k161jwkq5kmbs")))

(define-public crate-chord-parser-1.0.1 (c (n "chord-parser") (v "1.0.1") (h "12qpx3xrax1pw1gmlafdac6vb13cn3r805rhd33yh8nqkbyyn1xn")))

(define-public crate-chord-parser-1.1.0 (c (n "chord-parser") (v "1.1.0") (h "1jwvk9qvlczn6y9bhx20b96v3cdf0bwi75nljncb8ini2hdxniws")))

(define-public crate-chord-parser-1.1.1 (c (n "chord-parser") (v "1.1.1") (h "1fzlgrh6apax7xfjz97cbbyc7m1vbcxzsd4iblkn9x04pa0a1waj")))

