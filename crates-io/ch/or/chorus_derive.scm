(define-module (crates-io ch or chorus_derive) #:use-module (crates-io))

(define-public crate-chorus_derive-0.1.0 (c (n "chorus_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "163zin2y96kmwq27ws380prpq2225c18qya7gs09zmj3nj1ndicm")))

(define-public crate-chorus_derive-0.1.1 (c (n "chorus_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sv1wyzi6k6zqa72ips96bvfw5cq2qa2frrrk75psjj3lpxjy8zb")))

(define-public crate-chorus_derive-0.1.2 (c (n "chorus_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0az7qfcigacn00rjnxykdsqv14rxxkg7cz0bs8lfqzs0qrjvsy07")))

(define-public crate-chorus_derive-0.1.3 (c (n "chorus_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01yjbmg50rb4rwgr4gvrp4g62276smfb7521yzzfyqsqv1ncn09h")))

(define-public crate-chorus_derive-0.2.0 (c (n "chorus_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a3q77l620237lhr7xy8zbxqvb69szhyx5x94m8hcr5aw7m6ag2a")))

(define-public crate-chorus_derive-0.3.0 (c (n "chorus_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01z89jq1fj9qf5agsfddpcn67chb4a7bghhv3hpl0libim8l08zg")))

