(define-module (crates-io ch or chord-capnp) #:use-module (crates-io))

(define-public crate-chord-capnp-0.1.0 (c (n "chord-capnp") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.67") (d #t) (k 0)) (d (n "capnp") (r "^0.16.1") (d #t) (k 0)) (d (n "capnp-rpc") (r "^0.16.1") (d #t) (k 0)) (d (n "capnpc") (r "^0.16.2") (d #t) (k 1)) (d (n "chord-rs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "error-stack") (r "^0.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("rt-multi-thread" "net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("compat"))) (d #t) (k 0)))) (h "19njhxh4rgx1067ydkh4rdhh6crjvfkb1siz04blki2jfd5mfblb")))

