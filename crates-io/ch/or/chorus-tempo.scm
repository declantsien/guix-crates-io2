(define-module (crates-io ch or chorus-tempo) #:use-module (crates-io))

(define-public crate-chorus-tempo-0.1.0 (c (n "chorus-tempo") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.4") (d #t) (k 0)) (d (n "run_script") (r "^0.2.3") (d #t) (k 0)))) (h "0anyr97p0aw16flylpnag2bk9sydzaczj8fdii63m8v7flw1nrcv")))

