(define-module (crates-io ch or chordpro) #:use-module (crates-io))

(define-public crate-chordpro-0.1.0 (c (n "chordpro") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ysz1880pjs85xgfg20zz385plmadd428250imxha8nb1f6hl0vl") (f (quote (("default"))))))

(define-public crate-chordpro-0.1.1 (c (n "chordpro") (v "0.1.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s9xdp4bd5c7i4pgq8wbxb18jfj7awr5myylhkaxaps5bbsz5m51") (f (quote (("default"))))))

(define-public crate-chordpro-0.2.0 (c (n "chordpro") (v "0.2.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00crz3znxdmbgh2jx6s8g7wirzfbrwhq83bw9wcx4m7ysafmzxhr") (f (quote (("transpose") ("default"))))))

(define-public crate-chordpro-0.3.0 (c (n "chordpro") (v "0.3.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0db4brbpq72fb2i40hm2xxmpflkz9p1kx9fsss14mmk33l63l98a") (f (quote (("transpose") ("default"))))))

(define-public crate-chordpro-0.3.1 (c (n "chordpro") (v "0.3.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ch5j0bxx78kymz0jyh8cmpmkcz8nm88p778hjsa0m4wbjwilcw9") (f (quote (("transpose") ("default"))))))

