(define-module (crates-io ch or chordcalc) #:use-module (crates-io))

(define-public crate-chordcalc-0.1.0 (c (n "chordcalc") (v "0.1.0") (d (list (d (n "micromath") (r "^2.0") (d #t) (k 0)))) (h "0p2nx251li8bxbh47n423vbgwwi4z2b4kjs3a99zfkfs19c0waqy")))

(define-public crate-chordcalc-0.1.1 (c (n "chordcalc") (v "0.1.1") (d (list (d (n "micromath") (r "^2.0") (d #t) (k 0)))) (h "02w59bdl4j2ramg1dy62bdwcjnrfk8md03hb6iqnxczg25y9wzzz")))

