(define-module (crates-io ch or chord-rs-cli) #:use-module (crates-io))

(define-public crate-chord-rs-cli-0.1.0 (c (n "chord-rs-cli") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.67") (d #t) (k 0)) (d (n "chord-capnp") (r "^0.1.0") (d #t) (k 0)) (d (n "chord-grpc") (r "^0.1.0") (d #t) (k 0)) (d (n "chord-rs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 0)))) (h "0j95b8nj4zfcdkarkakpr6lwc8fcsdxnay5sz26fagwgbj7rzvz4")))

