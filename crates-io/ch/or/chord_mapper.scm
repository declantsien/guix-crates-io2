(define-module (crates-io ch or chord_mapper) #:use-module (crates-io))

(define-public crate-chord_mapper-0.1.0 (c (n "chord_mapper") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)))) (h "0b57pdni2pmcpvp26wfslfwmvazp1ybjjyyzm8k7fwic316h00n7")))

(define-public crate-chord_mapper-0.1.1 (c (n "chord_mapper") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)))) (h "0bapg1f8v6qxdgvfcjmb1l01pxgdzjsj76nicdaclrcsv0b31rfp")))

(define-public crate-chord_mapper-0.1.2 (c (n "chord_mapper") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)))) (h "1var1d26jc35kv9sgz5hqv5ycsy8y3985b9g2bm2gpcfz0f4ckm1")))

