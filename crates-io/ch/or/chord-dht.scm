(define-module (crates-io ch or chord-dht) #:use-module (crates-io))

(define-public crate-chord-dht-0.1.0 (c (n "chord-dht") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tarpc") (r "^0.27") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "rt-multi-thread"))) (d #t) (k 0)))) (h "06w7nbi7mgxg4xxd8kd35nj3651d6zjfmhyq48jsdm4532jdh045")))

