(define-module (crates-io ch or chord-rs-core) #:use-module (crates-io))

(define-public crate-chord-rs-core-0.1.0 (c (n "chord-rs-core") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.67") (d #t) (k 0)) (d (n "error-stack") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 0)) (d (n "seahash") (r "^4.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt-multi-thread" "sync" "macros" "time"))) (d #t) (k 0)))) (h "1hqa7nrr8rvi6zksf325lh5iv4s154lxpg5hbip7bxkhmmczilrm")))

