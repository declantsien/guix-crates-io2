(define-module (crates-io ch or choreographer) #:use-module (crates-io))

(define-public crate-choreographer-0.0.1 (c (n "choreographer") (v "0.0.1") (d (list (d (n "groundhog") (r "^0.1.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "micromath") (r "^2.0.0") (d #t) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)))) (h "1m37cf42gzx383cdkd8346x0z3cr15migzdvraawi1m9ynd4jq8p")))

(define-public crate-choreographer-0.0.2 (c (n "choreographer") (v "0.0.2") (d (list (d (n "groundhog") (r "^0.2.3") (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "micromath") (r "^2.0.0") (d #t) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)))) (h "11dac8srjkd9l491pnq6fv10cajk68swv7038a66547ypf5crddq") (f (quote (("testing" "groundhog/instant") ("default"))))))

(define-public crate-choreographer-0.0.3 (c (n "choreographer") (v "0.0.3") (d (list (d (n "groundhog") (r "^0.2.3") (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "micromath") (r "^2.0.0") (d #t) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)))) (h "1z2q88kviz209mp0yxds8yrxxsgwssnclvi8sq4f45irhsj4xcx6") (f (quote (("testing" "groundhog/instant") ("default"))))))

(define-public crate-choreographer-0.0.4 (c (n "choreographer") (v "0.0.4") (d (list (d (n "groundhog") (r "^0.2.3") (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "micromath") (r "^2.0.0") (d #t) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)))) (h "1884vdrxrx8xjc03c5k80q64zik8d7ldspsdc6nirkglyqnkxhkl") (f (quote (("testing" "groundhog/instant") ("default"))))))

