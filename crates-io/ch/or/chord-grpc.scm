(define-module (crates-io ch or chord-grpc) #:use-module (crates-io))

(define-public crate-chord-grpc-0.1.0 (c (n "chord-grpc") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.67") (d #t) (k 0)) (d (n "chord-rs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "error-stack") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prost") (r "^0.11.6") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "12g0y440qwhqgk89xdc3mc8aij9d020azxs187rfmqdh13wnpfcf")))

