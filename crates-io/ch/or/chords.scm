(define-module (crates-io ch or chords) #:use-module (crates-io))

(define-public crate-chords-0.1.0 (c (n "chords") (v "0.1.0") (h "0c01sny607qf5acmhw7pvi60dzysym4qwz3j5i6q6qb4qarjpfkb")))

(define-public crate-chords-0.1.1 (c (n "chords") (v "0.1.1") (h "0kg9lxw1wnl60mca6gy93qb38v8pvz00k2ihiclnnbjp7cd1j4bj")))

(define-public crate-chords-0.1.3 (c (n "chords") (v "0.1.3") (h "1dciw4w08ri4yrqj3czgmd8bval6xvk8dkqay9l9javbaazb74lm")))

(define-public crate-chords-0.1.4 (c (n "chords") (v "0.1.4") (h "14lcnba8lafkanczd080a5ya924d9i81zgmfrsvz7acx7pgmhycy")))

(define-public crate-chords-0.1.5 (c (n "chords") (v "0.1.5") (h "0dspnfy5zjiz6ifd6c22l57s4rm5wvyxc8m7igd8kpn7w113bsry")))

(define-public crate-chords-0.1.6 (c (n "chords") (v "0.1.6") (h "00xxbk9mnxb0m824qsjixsgy3h6yxk06f0bxb953s1gvs7dipmzg")))

(define-public crate-chords-0.1.7 (c (n "chords") (v "0.1.7") (d (list (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0aba5h4y5ngq4g6aswb20wijriic2wday5cj145gm6vryljqnxzk")))

(define-public crate-chords-0.1.8 (c (n "chords") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1girhd6ycmyb5jiv1m7nhks9djbhqfl2yfh37sn66z8f5z1jb30m")))

(define-public crate-chords-0.1.9 (c (n "chords") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "09llihj6cj9qzh2mn1y7106vmriwqigdqpgqk25xzl2d4qi29gv4")))

(define-public crate-chords-0.1.91 (c (n "chords") (v "0.1.91") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "175vn28vamrpz9prsp3b2p59j9ydw4pvnq2ris42k9byz3h0klh1")))

(define-public crate-chords-0.1.92 (c (n "chords") (v "0.1.92") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1r6iabw1ngcif5wvhkmhl41wykdscvsjvsh351pbjmpkh2bwpj0a")))

(define-public crate-chords-0.1.93 (c (n "chords") (v "0.1.93") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1dv0qxsgnjd65kb86pkxnxnh3vsjb2m60l8zwpywl9rh3hi3g9xf")))

(define-public crate-chords-0.2.4 (c (n "chords") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.5") (o #t) (d #t) (k 0)) (d (n "ordered-permutation") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "16ncfbmnpx6kcmyfqzzbydbkwd3qvkrb78d4cis6x1la47i5vy5f") (f (quote (("ffi_c" "libc") ("default"))))))

(define-public crate-chords-0.2.5 (c (n "chords") (v "0.2.5") (d (list (d (n "libc") (r "^0.2.5") (o #t) (d #t) (k 0)) (d (n "ordered-permutation") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1p1hisana5f2395mrg9qcjxhqfxf1j42f4r3pw5v5cpdcs41ngiv") (f (quote (("ffi_c" "libc") ("default"))))))

(define-public crate-chords-0.2.6 (c (n "chords") (v "0.2.6") (d (list (d (n "libc") (r "^0.2.5") (o #t) (d #t) (k 0)) (d (n "ordered-permutation") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "18kiwnk5nni1nnd7zvcbs2faifp7n9sbg3s6bjsmqfyab2lrlllc") (f (quote (("ffi_c" "libc") ("default"))))))

(define-public crate-chords-0.2.7 (c (n "chords") (v "0.2.7") (d (list (d (n "libc") (r "^0.2.5") (o #t) (d #t) (k 0)) (d (n "ordered-permutation") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1qphp7sj12j82b1zdn8w9lgm4f4wb17jbjgr54nm35wl7cgfp716") (f (quote (("ffi_c" "libc") ("default"))))))

