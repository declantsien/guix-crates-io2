(define-module (crates-io ch ul chull) #:use-module (crates-io))

(define-public crate-chull-0.1.0 (c (n "chull") (v "0.1.0") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ipadbklw5ad4j0vbdnb35f77hycbn3b7rh9zb7air5icv38ma1y")))

(define-public crate-chull-0.1.1 (c (n "chull") (v "0.1.1") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "07sjmnwdbcqxc0m0ksb1j130gijyad39nzcx6a6zcyiz71571rn0")))

(define-public crate-chull-0.1.2 (c (n "chull") (v "0.1.2") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1yhnxafpbmqd7fxj3fxjl29wy1zskgknlf6d3453yzgm3v5gp36q")))

(define-public crate-chull-0.1.3 (c (n "chull") (v "0.1.3") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1y0vynl3ifb3xnbvak2m4is43daka8hnj5z72qnp85islif086lb")))

(define-public crate-chull-0.1.4 (c (n "chull") (v "0.1.4") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1192jjn5b8wmq8sn075dsbbz1z7696rw5av97fz532xrsla7xps8")))

(define-public crate-chull-0.1.5 (c (n "chull") (v "0.1.5") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1wmraghbvb7jgh27gippbynmccgpl90sv43xv2ga14g8k29w9s35")))

(define-public crate-chull-0.1.6 (c (n "chull") (v "0.1.6") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1fa2r5ksiwv5n9vlqzmb88j6h274f8a8cdyq3scqcxk18n1glklb")))

(define-public crate-chull-0.1.7 (c (n "chull") (v "0.1.7") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0qqnckpl2hrf939qm3bcdchiw9b4czdzxm159278qs1vncfajypf")))

(define-public crate-chull-0.1.8 (c (n "chull") (v "0.1.8") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "05mjqf856y0i9svcf3g93glk48m1c0xkrnshxvypq46spbkq5nd7")))

(define-public crate-chull-0.2.0 (c (n "chull") (v "0.2.0") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0sl8m385fpyj81lrgdjsk2biwihswswlia76dcb8qnwg3l26671w") (y #t)))

(define-public crate-chull-0.2.1 (c (n "chull") (v "0.2.1") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0iy79xns9mi8bv8i0b086qybd03sf6n832rdy9j978rrnp16ivcy") (y #t)))

(define-public crate-chull-0.2.2 (c (n "chull") (v "0.2.2") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1z97wx45z968z0nc14m912xcsllmsrx0fz70f0a28sgzz0pi1l44") (y #t)))

(define-public crate-chull-0.2.3 (c (n "chull") (v "0.2.3") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1asx4hklnhida5xlybd4a1lrv905v854a3j6j2s62jacjq5wpg8c")))

(define-public crate-chull-0.2.4 (c (n "chull") (v "0.2.4") (d (list (d (n "num-bigint") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0yjxfld3pmgq93nyvkz5c86yji1q3psdqcihxadjg5pcbr7bazcp")))

