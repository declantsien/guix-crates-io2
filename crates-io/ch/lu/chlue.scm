(define-module (crates-io ch lu chlue) #:use-module (crates-io))

(define-public crate-chlue-0.1.0 (c (n "chlue") (v "0.1.0") (d (list (d (n "huelib") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "0z71dg5iv7ak7iz9vazy81ixlhs8ahizybfmc0ygr3dywlh27758")))

