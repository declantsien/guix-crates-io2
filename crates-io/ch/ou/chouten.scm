(define-module (crates-io ch ou chouten) #:use-module (crates-io))

(define-public crate-chouten-0.1.0 (c (n "chouten") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "v8") (r "^0.92.0") (d #t) (k 0)))) (h "0vhfzk60386xdz3ncb180bjiig2b9w7lzwqiphb8nzp5xbb25ji4")))

