(define-module (crates-io ch ud chudnovsky) #:use-module (crates-io))

(define-public crate-chudnovsky-0.0.0 (c (n "chudnovsky") (v "0.0.0") (d (list (d (n "dashu") (r "^0.4.0") (d #t) (k 0)) (d (n "latexify") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "05f12knx1wi75f6q3r2cf5cv6bj53widv7j9y3xmiybi6vf624mk") (f (quote (("default"))))))

(define-public crate-chudnovsky-0.0.1 (c (n "chudnovsky") (v "0.0.1") (d (list (d (n "dashu") (r "^0.4.0") (d #t) (k 0)) (d (n "latexify") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1j3z6wxx4grp3kkk7i7vsd8djwibnclf4xci57na49igd666c9hk") (f (quote (("default"))))))

