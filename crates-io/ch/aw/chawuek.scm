(define-module (crates-io ch aw chawuek) #:use-module (crates-io))

(define-public crate-chawuek-0.1.0 (c (n "chawuek") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tch") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "022f1kwwz1frl6vgbsdqd6r3p143d4kc0f80xm7s1wcw3sz6r79y")))

