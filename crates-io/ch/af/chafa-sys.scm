(define-module (crates-io ch af chafa-sys) #:use-module (crates-io))

(define-public crate-chafa-sys-0.1.0 (c (n "chafa-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 2)))) (h "1scx3q1z7yjjx2c8aazc1kbwhpd8h5fjg6fdiv7ds4biypin0lhm") (l "chafa")))

(define-public crate-chafa-sys-0.1.1-1.12.5 (c (n "chafa-sys") (v "0.1.1-1.12.5") (d (list (d (n "autotools") (r "^0.2.6") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "freetype-sys") (r "^0.19.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 2)))) (h "084dr5ml0v016n52140ixm3zn6cfggjsi9csp3mia9s64fxdqj12") (l "chafa")))

(define-public crate-chafa-sys-0.1.2 (c (n "chafa-sys") (v "0.1.2") (d (list (d (n "autotools") (r "^0.2.6") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 2)))) (h "16wp9kzwwyfw25n0b29pqj98dp6x0ilcd96ph10x0rfzdvasg3ql") (l "chafa")))

(define-public crate-chafa-sys-0.1.3 (c (n "chafa-sys") (v "0.1.3") (d (list (d (n "autotools") (r "^0.2.6") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 2)))) (h "1hji1fb14qar2fwgx03va5xqx03xpyy6p83gsb6xf94zkd1gi5zi") (l "chafa")))

(define-public crate-chafa-sys-0.1.4 (c (n "chafa-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 2)))) (h "0ga4f6bnp9v326cghq46jiqdija43cjs8205c3qs8v8rraw6vqsi") (l "chafa")))

