(define-module (crates-io ch af chafa) #:use-module (crates-io))

(define-public crate-chafa-0.1.0 (c (n "chafa") (v "0.1.0") (d (list (d (n "chafa-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1v7dlqsp558nr95k695axxfmcm1ll8rmywf2kf14s1sqpas75zm7")))

(define-public crate-chafa-0.1.1 (c (n "chafa") (v "0.1.1") (d (list (d (n "chafa-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "1h9xah001253ypnx950gz0wbdbb90xd6xfhz9bx6bxdq573nx214")))

