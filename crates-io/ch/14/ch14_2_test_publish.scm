(define-module (crates-io ch #{14}# ch14_2_test_publish) #:use-module (crates-io))

(define-public crate-ch14_2_test_publish-0.1.1 (c (n "ch14_2_test_publish") (v "0.1.1") (h "0vcgcl5gaszf21llsi55r3x9hrzxlxni645mbxmigqi19w9i44af")))

(define-public crate-ch14_2_test_publish-0.1.2 (c (n "ch14_2_test_publish") (v "0.1.2") (h "137w1bry8l6sgglcdr1ws3cf0l62g8j5wfysxwrhj8r6s8m1jyns")))

