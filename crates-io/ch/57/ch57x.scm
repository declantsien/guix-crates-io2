(define-module (crates-io ch #{57}# ch57x) #:use-module (crates-io))

(define-public crate-ch57x-0.1.5 (c (n "ch57x") (v "0.1.5") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0lkrppc1nzr34ll7kvgvkgmsnfji9rqaryq19fi3xcn2nwsb257v") (f (quote (("rt" "riscv-rt") ("default" "rt") ("ch57x"))))))

(define-public crate-ch57x-0.1.6 (c (n "ch57x") (v "0.1.6") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0jwy4kj623416mhnlpgmmc4l91d0ji48df6ygyak4n2zw8c2cwgs") (f (quote (("rt") ("default") ("ch57x"))))))

