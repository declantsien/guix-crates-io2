(define-module (crates-io ch en cheng_art) #:use-module (crates-io))

(define-public crate-cheng_art-0.1.0 (c (n "cheng_art") (v "0.1.0") (h "1kfwdyann1k1kl3krwyjg26nwlw3k0p62fzwgx1wmjaxjj2czb9f") (y #t)))

(define-public crate-cheng_art-0.1.1 (c (n "cheng_art") (v "0.1.1") (h "14zh2sg0hvi56rq935lhjqpgc1m1bal4av02ad3ci8rhi3ljpwva") (y #t)))

(define-public crate-cheng_art-0.1.2 (c (n "cheng_art") (v "0.1.2") (h "1mhhpg6bjvfl67mrfhzmlvn1sf0qc4nmga88hclsrqxc4765jy3a")))

