(define-module (crates-io ch eq cheque) #:use-module (crates-io))

(define-public crate-cheque-0.1.0 (c (n "cheque") (v "0.1.0") (d (list (d (n "num-traits") (r "0.1.*") (d #t) (k 0)))) (h "1d4cw7r64wp269z2zamdwgrrrlg2pnp3dy57vv7s5czj3fagaqpd")))

(define-public crate-cheque-0.2.0 (c (n "cheque") (v "0.2.0") (d (list (d (n "num-traits") (r "0.1.*") (d #t) (k 0)))) (h "1qnqb4zg8hy7bsxclvjn3y5gvlb3gvizsxy3x950jxj16agv5wf5")))

(define-public crate-cheque-0.3.0 (c (n "cheque") (v "0.3.0") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0daiicbkbc2qwq9633d2ya9vrldsb9mci0vv3pb95162z7qwjfsg")))

