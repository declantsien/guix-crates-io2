(define-module (crates-io ch zz chzzk) #:use-module (crates-io))

(define-public crate-chzzk-0.1.0 (c (n "chzzk") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1ynlpzhsybi4n6s3s0cv8szwkxqhl5rddkcwn7qv37xssn5iaj8w")))

(define-public crate-chzzk-0.2.0 (c (n "chzzk") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0f393rf7hbprvhrfb7i0kkqv9gl22zym2wai6nyjmz0hjxkrdsaz")))

