(define-module (crates-io ch ok choki) #:use-module (crates-io))

(define-public crate-choki-1.0.0 (c (n "choki") (v "1.0.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "07rpgvac1kyfmmywa89xhd61ifz2dkz056g8s1ig5ywkxsw2m1xc")))

(define-public crate-choki-1.0.1 (c (n "choki") (v "1.0.1") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "002ajvidpksawprm7nbfgb1yjkrf6axhw3c0gx12x7f8qs54gkmr")))

(define-public crate-choki-1.0.2 (c (n "choki") (v "1.0.2") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0g3kfrgqp5yrl89p3k2hahbpggzh41wqy2qscj60i7yzgdmzmpfi")))

(define-public crate-choki-1.0.3 (c (n "choki") (v "1.0.3") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0iv96i5wxdjbsk4akcc9ikpfqwya9arhmcp8385i41wyb793m7z4")))

(define-public crate-choki-1.0.4 (c (n "choki") (v "1.0.4") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0g5p3bgwv3cd5hp60rwyyiih6dll8pzbi4dj3iph8j1kqq8lk28k")))

