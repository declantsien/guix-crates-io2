(define-module (crates-io ch ry chrysanthemum) #:use-module (crates-io))

(define-public crate-chrysanthemum-0.0.1 (c (n "chrysanthemum") (v "0.0.1") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "rspirv") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spirv_headers") (r "^1.3") (d #t) (k 0)))) (h "014plg79azi5s0isd94qvvbrynxg1dlg6pd69cky00sna7jzlvkz")))

