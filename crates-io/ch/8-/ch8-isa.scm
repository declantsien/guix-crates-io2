(define-module (crates-io ch #{8-}# ch8-isa) #:use-module (crates-io))

(define-public crate-ch8-isa-0.1.0 (c (n "ch8-isa") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "15ga6qshjjqsnvjlkl72hskmp0q9i1fskqh72p4xldhacywzc4ka")))

(define-public crate-ch8-isa-0.1.1 (c (n "ch8-isa") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "1y5qsgx94p3q4iyj4swj107680kn2dmmlbmkbyg56bgc60r6x9zq")))

