(define-module (crates-io ch ek chekov) #:use-module (crates-io))

(define-public crate-chekov-0.0.1 (c (n "chekov") (v "0.0.1") (h "0mn6zg8b379rdknaa70ab3xblyd99wi6q8p9yj8syjcvd2cglbry")))

(define-public crate-chekov-0.1.0 (c (n "chekov") (v "0.1.0") (d (list (d (n "event_store") (r "^0") (d #t) (k 0)))) (h "0mpl53yxz6ipjkbvw2hsnr4lv2p3bw30w4y6jwan7hf24mg4askq")))

(define-public crate-chekov-0.1.1 (c (n "chekov") (v "0.1.1") (d (list (d (n "actix") (r "^0.12") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chekov_macros") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "event_store") (r "^0.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "futures") (r "^0") (d #t) (k 0)) (d (n "inventory") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("postgres" "chrono" "time" "uuid" "json" "offline" "runtime-actix-native-tls"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-futures") (r "^0") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "15rppdz05fanqkh1y0hjphi4chvzqrznxvrr0i0d1dpxcwzhw6f8")))

