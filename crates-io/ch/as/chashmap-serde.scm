(define-module (crates-io ch as chashmap-serde) #:use-module (crates-io))

(define-public crate-chashmap-serde-0.1.0 (c (n "chashmap-serde") (v "0.1.0") (d (list (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("owning_ref"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "1w8ysp3gv0ihrc7hfmyyc7nx6lj7f4pqpf6ynph6q0mns2q73rwi")))

(define-public crate-chashmap-serde-0.1.1 (c (n "chashmap-serde") (v "0.1.1") (d (list (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("owning_ref"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "1yb56fjcx3j6jx84mdkq8aikfl6ibf6aqf07b5w45rsrcgpgn6dk")))

(define-public crate-chashmap-serde-2.2.3 (c (n "chashmap-serde") (v "2.2.3") (d (list (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("owning_ref"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "05x86s6kxyysa0c4bf3vb9xivdgksdciq2jq1rwa4v0j1xm4wg2b")))

