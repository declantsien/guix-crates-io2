(define-module (crates-io ch as chasm-rs) #:use-module (crates-io))

(define-public crate-chasm-rs-0.1.0 (c (n "chasm-rs") (v "0.1.0") (d (list (d (n "blake3") (r "^1") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmi") (r "^0.9") (d #t) (k 2)))) (h "1qjixvvadgr2vhq8lrvyz5d7prjblbdkh7rnlvsbflacyhpk1wav")))

