(define-module (crates-io ch as chashmap) #:use-module (crates-io))

(define-public crate-chashmap-0.1.0 (c (n "chashmap") (v "0.1.0") (d (list (d (n "owning_ref") (r "^0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (d #t) (k 0)))) (h "13dmhhlivw32d1s43f672czy9909yhfvdd0mj72p0jwry0k0dg58")))

(define-public crate-chashmap-0.1.1 (c (n "chashmap") (v "0.1.1") (d (list (d (n "owning_ref") (r "^0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (d #t) (k 0)))) (h "0wma2ismphrjiavkdqjs2lz7d7h5901m7xfpfq389ar4s92ibgk6")))

(define-public crate-chashmap-0.1.2 (c (n "chashmap") (v "0.1.2") (d (list (d (n "owning_ref") (r "^0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (d #t) (k 0)))) (h "1x7sjrjar5i5p8w6px8m03brc2qygxndy2a2akkx1xrr1zz9jfqx")))

(define-public crate-chashmap-0.2.0 (c (n "chashmap") (v "0.2.0") (d (list (d (n "owning_ref") (r "^0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (d #t) (k 0)))) (h "1yfq4y983i47y0v0g6rlz0i5prq87i84kqzmvpd7gxx4xvfpgbzp")))

(define-public crate-chashmap-1.0.0 (c (n "chashmap") (v "1.0.0") (d (list (d (n "owning_ref") (r "^0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (d #t) (k 0)))) (h "1j5ksabmsydfmm1bv9mdlm6w2cqsyxnffm7vb8lxc843iy967rnn")))

(define-public crate-chashmap-1.1.0 (c (n "chashmap") (v "1.1.0") (d (list (d (n "owning_ref") (r "^0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (d #t) (k 0)))) (h "1rll61r6fdhyhlij8cj7bic1j1a6s8rjx1r5a95b12hvbgl1b1i3")))

(define-public crate-chashmap-1.2.0 (c (n "chashmap") (v "1.2.0") (d (list (d (n "owning_ref") (r "^0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (d #t) (k 0)))) (h "16kk3ldyb6m3k96yj692bfqj6bccaw6ca767kxw5i3fjd2wi6brs")))

(define-public crate-chashmap-2.0.0 (c (n "chashmap") (v "2.0.0") (d (list (d (n "owning_ref") (r "^0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (d #t) (k 0)))) (h "0z6fllz28rsdaq8yw240d2wgz8pdjp115x9jfhnpc0hd87zfkxwl")))

(define-public crate-chashmap-2.1.0 (c (n "chashmap") (v "2.1.0") (d (list (d (n "owning_ref") (r "^0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (d #t) (k 0)))) (h "033sac0jz1i70ynfn8krsjbsfg4mnv1s6nh8sa1wr10gl9n06hpp")))

(define-public crate-chashmap-2.1.1 (c (n "chashmap") (v "2.1.1") (d (list (d (n "owning_ref") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)))) (h "17nlar022qranickv6g4xl1c1z6d2vdbvph15a7gb0g5s76wwvsq")))

(define-public crate-chashmap-2.2.0 (c (n "chashmap") (v "2.2.0") (d (list (d (n "owning_ref") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (d #t) (k 0)))) (h "1y6z2d931z9ccq9fpqmvyb37clp764jmww0gffmbn37bq6l53rj7")))

(define-public crate-chashmap-2.2.2 (c (n "chashmap") (v "2.2.2") (d (list (d (n "owning_ref") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0igsvpc2ajd6w68w4dwn0fln6yww8gq4pq9x02wj36g3q71a6hgz")))

