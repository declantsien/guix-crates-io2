(define-module (crates-io ch as chasa) #:use-module (crates-io))

(define-public crate-chasa-0.1.0 (c (n "chasa") (v "0.1.0") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "1bmrxzh83sgjh9l46vjmqbd5874ckqvq3l7z3k60dc315acfpnly")))

(define-public crate-chasa-0.1.1 (c (n "chasa") (v "0.1.1") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "08kdg9ivf6v2kb8knkzig73wjbqnvpg5kazppdbh6wny5fkki5vy")))

(define-public crate-chasa-0.1.2 (c (n "chasa") (v "0.1.2") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "03dr9s46c3jar8srpm7k4wdgk3ay1xgrf1vcx388blaq3q3llsy6")))

(define-public crate-chasa-0.1.3 (c (n "chasa") (v "0.1.3") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "1b8gz8i3h6ivgqkzpy7n5gv0lswy1qn2s7my85b7p7hnx3m1p997")))

(define-public crate-chasa-0.1.4 (c (n "chasa") (v "0.1.4") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "1xj8h2vhhbqs5r7i0qlfg4fjg7jklhd1dh4pnlsrk4907rpnf5bf")))

(define-public crate-chasa-0.1.5 (c (n "chasa") (v "0.1.5") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "1swrhvl90rwwmf807dfa53jnbmgpvrr9g6gwl8rbjnj0vz4dap6k")))

(define-public crate-chasa-0.1.6 (c (n "chasa") (v "0.1.6") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "09wy5walnayibq4qwlkncnwdbivkmarrzy5bscvcb19a2h7rvmch")))

(define-public crate-chasa-0.1.7 (c (n "chasa") (v "0.1.7") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "0m4jyy39dzgn9qq61jc3wqzx2vjfj5m5rggvs9xjwgab4k55m070")))

(define-public crate-chasa-0.1.8 (c (n "chasa") (v "0.1.8") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "1xafzhf3f0ddrkpm3z9lyi8vqqcrmmv5fcgdsr999m3hrabnr20d")))

(define-public crate-chasa-0.1.9 (c (n "chasa") (v "0.1.9") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "0ambsgj486970bnhpr5zqy1mvj06448b90ywbrb2gh2xg6sqcjw1")))

(define-public crate-chasa-0.1.10 (c (n "chasa") (v "0.1.10") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "1avvhs8c4s2rlr39gadyk2hbfkdjybaqjjbb5ppy3l2jlqxr6m6i")))

(define-public crate-chasa-0.1.11 (c (n "chasa") (v "0.1.11") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "09cdjywzi1ki75970655srzzk08f5l7axlvrmmymlwbyv4nvc7a3")))

(define-public crate-chasa-0.1.12 (c (n "chasa") (v "0.1.12") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "0v2issplifxw1prv42fpc3716cmjhm3xxlxinr8658fqx55fdirj")))

(define-public crate-chasa-0.1.13 (c (n "chasa") (v "0.1.13") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)))) (h "0rpvfv8zrvxq1scsisg20hcfmfvn01caq8vzvkcl9768lg97yrsj")))

