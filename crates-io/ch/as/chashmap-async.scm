(define-module (crates-io ch as chashmap-async) #:use-module (crates-io))

(define-public crate-chashmap-async-0.1.0 (c (n "chashmap-async") (v "0.1.0") (d (list (d (n "async-lock") (r "^2.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "owning_ref") (r "^0.4.4") (f (quote ("async"))) (d #t) (k 0) (p "owning_ref_async")) (d (n "stable_deref_trait") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0a8jcs742xa0qr5yxny3xbynqlcy19y37cns2hkvasnwkywhw10f")))

(define-public crate-chashmap-async-0.1.1 (c (n "chashmap-async") (v "0.1.1") (d (list (d (n "async-lock") (r "^3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "owning_ref") (r "^0.4.4") (f (quote ("async"))) (d #t) (k 0) (p "owning_ref_async")) (d (n "stable_deref_trait") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0q2984qdillpsyndwmqbpkvwg4zkbfvbb8zh19hsvqjwrqn1g268")))

