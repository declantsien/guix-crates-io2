(define-module (crates-io ch as chassis) #:use-module (crates-io))

(define-public crate-chassis-0.1.0 (c (n "chassis") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.32") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0w5bd230p7fjbyqh326k8fyfz43blf7sahjqyps5c4lzc480w6xn")))

(define-public crate-chassis-0.1.1 (c (n "chassis") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.32") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1gwcbfzp575n20zbf3lgz87s5jpgza563bfp4zpk0d6z8n5bab4w")))

(define-public crate-chassis-0.2.0 (c (n "chassis") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "chassis-proc-macros") (r "=0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.84") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0aqpal48dq1a4044vfi2hcwcb5bncim6d0lc0w6mdcw6aqjmdfl2")))

