(define-module (crates-io ch as chassis-proc-macros) #:use-module (crates-io))

(define-public crate-chassis-proc-macros-0.2.0 (c (n "chassis-proc-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1bfwxr9zzkh11w9b5nw5yzrmm1f2d0ay98gagr8a4bgiy8kiyxnd")))

