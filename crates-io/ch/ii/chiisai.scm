(define-module (crates-io ch ii chiisai) #:use-module (crates-io))

(define-public crate-chiisai-0.1.0 (c (n "chiisai") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)))) (h "1vn30f3vh39d9nnj4njdypxcvsbnkw05gk8fhzaih0rlbpqvs8bl") (y #t)))

(define-public crate-chiisai-0.1.1 (c (n "chiisai") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "0mv3vngfvlqgy5gd4s4wysg97y9cnxd2b045r2nbf0nca20y00dh")))

(define-public crate-chiisai-0.1.2 (c (n "chiisai") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "05xv05bvm65x59i1qknzv8ghwxj2m8590g4j3g0j4cirdvg1qxam") (y #t)))

(define-public crate-chiisai-0.1.3 (c (n "chiisai") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "1jmmqnrxy20llabps23spy9h8n1n744gfshv76sjk78m5drz59l2") (y #t)))

(define-public crate-chiisai-0.1.4 (c (n "chiisai") (v "0.1.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "0kyv7ga1dgdis7mxyqiin6rz048b2ajwvw917j7r2fc2lnpb7rdf") (y #t)))

(define-public crate-chiisai-0.1.5 (c (n "chiisai") (v "0.1.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "0zvh98z2ip73580j58xcs3vwpkxm41z7q3fdl2zbk9rvwcxlz9id") (y #t)))

(define-public crate-chiisai-0.1.6 (c (n "chiisai") (v "0.1.6") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "01r66v3k151i3ksr2fzc2lp68d83a718g689nw6sh842dvazja81") (y #t)))

