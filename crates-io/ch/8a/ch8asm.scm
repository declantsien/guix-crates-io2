(define-module (crates-io ch #{8a}# ch8asm) #:use-module (crates-io))

(define-public crate-ch8asm-0.1.0 (c (n "ch8asm") (v "0.1.0") (d (list (d (n "ch8alib") (r "^0.1.0") (d #t) (k 0)))) (h "1xkp5ak0w5lhjji5z8hv1nxkk7gi8qiljx2ggfrcnw40apnymcna")))

(define-public crate-ch8asm-0.1.1 (c (n "ch8asm") (v "0.1.1") (d (list (d (n "ch8alib") (r "^0.1.0") (d #t) (k 0)))) (h "1c8q41pzldcmzzp1dqgs4xd7pj4rc545c2wk25b2nfv6qwk73x70")))

