(define-module (crates-io ch es chess-lib) #:use-module (crates-io))

(define-public crate-chess-lib-0.1.0 (c (n "chess-lib") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0g34c1j0h0ygs0vx85hmp9w5798x0xwd5lkm3b3626i2g9bqzshz") (f (quote (("expensive_tests") ("default" "expensive_tests"))))))

(define-public crate-chess-lib-0.1.1 (c (n "chess-lib") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "11pw6wvvxiqb56rds70j95kqsgrf0dxdbcp11h1qc0fy5pryf45a") (f (quote (("expensive_tests") ("default" "expensive_tests"))))))

(define-public crate-chess-lib-0.1.2 (c (n "chess-lib") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0ffz131f7gnjk1zjqcpl2qhncwg0z44yz646fsx5lhi3jg277kh9") (f (quote (("expensive_tests") ("default" "expensive_tests"))))))

(define-public crate-chess-lib-0.1.3 (c (n "chess-lib") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1df8mdfspc3j3yxbbsjr8n8d4dzixrlqjxnnlwmg2fwz3kkgsiby") (f (quote (("expensive_tests") ("default" "expensive_tests"))))))

