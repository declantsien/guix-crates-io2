(define-module (crates-io ch es chess-server) #:use-module (crates-io))

(define-public crate-chess-server-0.1.0 (c (n "chess-server") (v "0.1.0") (d (list (d (n "chess-core") (r "^0.1.0") (d #t) (k 0)) (d (n "chess-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.4") (f (quote ("crossbeam-channel" "crossbeam-queue"))) (d #t) (k 0)))) (h "1gnhl5xdsjfnc17l0l72snfmd09a0s6nrhdsjr38xgqnkl84704s")))

