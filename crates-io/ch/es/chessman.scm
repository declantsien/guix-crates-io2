(define-module (crates-io ch es chessman) #:use-module (crates-io))

(define-public crate-chessman-0.1.0 (c (n "chessman") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.3") (d #t) (k 2)))) (h "02cx2j55y49394g5jnnmf50mfwmlj7n3rrvzy328z09pv0122xm4")))

(define-public crate-chessman-0.1.1 (c (n "chessman") (v "0.1.1") (d (list (d (n "svg") (r "^0.13.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1b15a4y8hcqld2gqa9sn2s6jpdshvz3bysj4brs2q07x2girgdv4")))

