(define-module (crates-io ch es chess-huffman) #:use-module (crates-io))

(define-public crate-chess-huffman-0.1.0 (c (n "chess-huffman") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "huffman-compress") (r "^0.6.1") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.20") (d #t) (k 0)) (d (n "shakmaty") (r "^0.21") (d #t) (k 0)))) (h "1l5gah2gd86a7209cpa4rcb8b4vykisjs4rs06rvq0ids2brnh92")))

(define-public crate-chess-huffman-0.2.0 (c (n "chess-huffman") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "huffman-compress") (r "^0.6.1") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.25") (d #t) (k 0)) (d (n "shakmaty") (r "^0.26") (d #t) (k 0)))) (h "0a5bm4p09hvcjk5h4593ijyw6dcqv4g3v5fvgsmg57iqryhm92vz")))

(define-public crate-chess-huffman-0.3.0 (c (n "chess-huffman") (v "0.3.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "huffman-compress") (r "^0.6.1") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.25") (d #t) (k 0)) (d (n "shakmaty") (r "^0.26") (d #t) (k 0)))) (h "1pxr8g1q1zz6rc40zdnmj289vx9y2kh8l5r1a46gm6dqksbay0w3")))

(define-public crate-chess-huffman-0.3.1 (c (n "chess-huffman") (v "0.3.1") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "huffman-compress") (r "^0.6.1") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.25") (d #t) (k 0)) (d (n "shakmaty") (r "^0.26") (d #t) (k 0)))) (h "1nqixjhj4qzmp42vqxl5yn1a0manx021k6d1vb600cba4qk1yk23")))

(define-public crate-chess-huffman-0.4.0 (c (n "chess-huffman") (v "0.4.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "huffman-compress") (r "^0.6.1") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.26") (d #t) (k 0)) (d (n "shakmaty") (r "^0.27") (d #t) (k 0)))) (h "10ydxrxwm4przrkzk32yszvdgpc5i1fkpiinjxyygy5rl3pfr5h4")))

