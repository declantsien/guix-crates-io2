(define-module (crates-io ch es chessagon) #:use-module (crates-io))

(define-public crate-chessagon-0.1.0 (c (n "chessagon") (v "0.1.0") (d (list (d (n "chic") (r "^1.2.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "06agp4d0hp4x7bmhkvyyspk8i9a3jmpcfqprssgvri3fbx18f958") (f (quote (("default" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen"))))))

