(define-module (crates-io ch es chesscom-openapi) #:use-module (crates-io))

(define-public crate-chesscom-openapi-0.1.0 (c (n "chesscom-openapi") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "gzip"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "0mp2k04wxjdk5g11z2vlm9q10v75b616y3a6vdz9kwfc17daih7f")))

(define-public crate-chesscom-openapi-0.1.1 (c (n "chesscom-openapi") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1498lpjygfclcsm24ppkwl7g5abh0prsr81qdy27wqz127221q80")))

