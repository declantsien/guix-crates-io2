(define-module (crates-io ch es chesterfield) #:use-module (crates-io))

(define-public crate-chesterfield-0.0.0 (c (n "chesterfield") (v "0.0.0") (d (list (d (n "cargo-readme") (r "^3.1.2") (d #t) (k 1)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "shiplift") (r "^0.5.0") (d #t) (k 2)) (d (n "tokio") (r "^0.1.20") (d #t) (k 0)))) (h "0r2zva517rc2s8wj2dcy7rp10m0nxijr2jnm4fifgccd7yixh7ga")))

(define-public crate-chesterfield-0.0.1 (c (n "chesterfield") (v "0.0.1") (d (list (d (n "harbourmaster") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "shiplift") (r "^0.5.0") (d #t) (k 2)) (d (n "tokio") (r "^0.1.20") (d #t) (k 0)))) (h "0616slklg8l5l2hlzd8720fyd1y1i00r3w5mmlsdb1bv8p264knw") (f (quote (("container" "harbourmaster"))))))

