(define-module (crates-io ch es chessai) #:use-module (crates-io))

(define-public crate-chessai-0.1.0 (c (n "chessai") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "00qynm35ybayp7hm0ffkdh9yzn82n9jwpc41lk69b0dr6alws5p8")))

(define-public crate-chessai-0.1.1 (c (n "chessai") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1717g1432gmkaccx7rrcpzgxwg21hjww98dmmpbx0885c89a1q41")))

(define-public crate-chessai-0.1.2 (c (n "chessai") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "085axxp9qs2spjvx41242ifbs56vv0v24hllmk7p2h50pl08id17")))

(define-public crate-chessai-0.1.3 (c (n "chessai") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1q3nbdx614wk2l85r613prjksir0ihxsfv6bxv436ra21m09qxcz")))

(define-public crate-chessai-0.1.4 (c (n "chessai") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "00gjvv3y5zwmidbac7wgin2dhlc9x6vc5fkaiz2bxb8dwlclf1fn")))

(define-public crate-chessai-0.1.5 (c (n "chessai") (v "0.1.5") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "00vykcp79l5gm4hnp59s4h9mj56l7izx8b08rki93l8a0c44s7cp")))

(define-public crate-chessai-0.1.6 (c (n "chessai") (v "0.1.6") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0x3l57ahfd1318d1hgin8h4ypda3my6rcksrjqfwp0pvjx9ybzlx")))

(define-public crate-chessai-0.1.7 (c (n "chessai") (v "0.1.7") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1mhfd9qv5s2k4skd1vyq3aiyvzv3nw33kb7ynkndmraypx844r41")))

(define-public crate-chessai-0.1.8 (c (n "chessai") (v "0.1.8") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0ag2j6b6240wyyv2baprqdvpj0a402cvxhyv5ky5dx1hldc5yzap")))

