(define-module (crates-io ch es chess) #:use-module (crates-io))

(define-public crate-chess-0.0.1 (c (n "chess") (v "0.0.1") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1c0pkh7aw2q8k1kzah9r7g360q7vinglk601nl2jkikgisgz3c5b")))

(define-public crate-chess-0.0.2 (c (n "chess") (v "0.0.2") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0frz6w7qal92d2b0vp26xivfndczk05x9r3kg0fy29q13frx7xwd")))

(define-public crate-chess-0.0.3 (c (n "chess") (v "0.0.3") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0izfhskvr3zx2z1imngvqxfgrb93wg6ivyz6awdyj07ggaxm8zgb")))

(define-public crate-chess-0.0.4 (c (n "chess") (v "0.0.4") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1f1qk872br1irs4nwr89z0xbf78bpy4xihqaqxlc9q3mj1qrs340")))

(define-public crate-chess-0.0.5 (c (n "chess") (v "0.0.5") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1xakdc9c32gqyy9p5j5wql4b0ikd11is5h2f9rj6z9bdgc8lzhvs")))

(define-public crate-chess-0.1.0 (c (n "chess") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "16yki5m7fjl7bvrk0zijcnhvma372b3ywh0i2cc4lw769q7q4hg5")))

(define-public crate-chess-0.1.2 (c (n "chess") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1hwxv0zi4lcjmaxnvc89lr56is1nhsr87vkjn9nm1r6q3yb4jf8a")))

(define-public crate-chess-0.2.0 (c (n "chess") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0m31jmalvj761f2gfzdxxgdwxikkpvlzwpdqbp56k2j996jq5vyn")))

(define-public crate-chess-0.2.1 (c (n "chess") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1dh9nhs3hdrm2lf9m2xxbzdmiwp3gcbhdyy7b28rsajkasqb7cw5")))

(define-public crate-chess-0.3.0 (c (n "chess") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0r7331slq9zh5ivqr13fjqkchrqxqx1sa1qz0jal94fvxs43r874")))

(define-public crate-chess-0.3.1 (c (n "chess") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "18wznyzjjn001vp261y577gg86157gg2px7ap6wdlpnjdnxwbc95")))

(define-public crate-chess-0.3.2 (c (n "chess") (v "0.3.2") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1qdwybd0ydfg3cvqxlzp29gyva0xsh0zb6lvszmxnwg9147i7i08")))

(define-public crate-chess-0.3.3 (c (n "chess") (v "0.3.3") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "045239vcs9vp6mwkmk2x61j5jxqhwra9dqbyajniqw28q799hnis")))

(define-public crate-chess-0.3.4 (c (n "chess") (v "0.3.4") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1179pxz85p7d6w0vfdz3cfl6fjwqanw0rqv3qf0rjai012bv9zc6")))

(define-public crate-chess-0.4.0 (c (n "chess") (v "0.4.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 1)))) (h "045i3xn16c3wbh3zs7rbhvz0fm6bnz5p1c9mh4mz5c9z4wps5svr")))

(define-public crate-chess-0.4.1 (c (n "chess") (v "0.4.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 1)))) (h "18zy53icf6yr39n57jlfmyswr6m3kp0h635zwwilagflgjpl392r")))

(define-public crate-chess-0.4.2 (c (n "chess") (v "0.4.2") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 1)))) (h "1pfh4d0b1m1jhzmxprfy8yfnvkjv6bzmbfapzy2d9l6ga3gccbjf")))

(define-public crate-chess-0.4.3 (c (n "chess") (v "0.4.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 1)))) (h "1rrmy0n6z4gln0s3jzyn3qiwr8ihdl7s6x2nm6wng5g6bl786h16")))

(define-public crate-chess-0.4.4 (c (n "chess") (v "0.4.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 1)))) (h "1zlflx218xzgbmyna98c04g80kbd3b0x3rwcbrqyhdqc1aalvyha")))

(define-public crate-chess-1.0.0 (c (n "chess") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 1)))) (h "03jwad50705k1hzx73d8mfpp8qyj3w13gad9j7isqbkydgrcxyvf")))

(define-public crate-chess-1.0.1 (c (n "chess") (v "1.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 1)))) (h "0yzrvmlin1zafas6qwv4f45smrll6fj8s3xwxq7lq5w89g3wf1c1")))

(define-public crate-chess-1.0.2 (c (n "chess") (v "1.0.2") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 1)))) (h "0izgwx11nrd9b6b7frm366rb6a5jwc4gdz5278q6mcakk5ly1wz3")))

(define-public crate-chess-1.0.3 (c (n "chess") (v "1.0.3") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 1)))) (h "1jsj4azm61qpb6lsilr97maklr873vsdyqk4cp3llcnjyqdcin9s")))

(define-public crate-chess-1.0.4 (c (n "chess") (v "1.0.4") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 1)))) (h "0gznsvgr857wavziz5ngns4fn9w0ngxzspnm6r00n9y1zd0givy6")))

(define-public crate-chess-1.0.5 (c (n "chess") (v "1.0.5") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 1)))) (h "003blfxp9gs57r3ivqlvcj41jhjvg5p1zhi6d8zrrx5wqzfy71d7")))

(define-public crate-chess-1.0.6 (c (n "chess") (v "1.0.6") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 1)) (d (n "smallvec") (r "^0.6.5") (d #t) (k 0)))) (h "02475pcbckflav2xznpwpqzfm8s1lllf6hnwfqpwkq18g86z2qhj")))

(define-public crate-chess-1.0.7 (c (n "chess") (v "1.0.7") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 1)))) (h "0kdnpcydsrhhppmdzj5r86c44gvblaibwi49wdc4pbpclwq28r8w")))

(define-public crate-chess-2.0.0 (c (n "chess") (v "2.0.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 1)))) (h "01l5xzclf9asyj82ldl8vj44cj7d2jzmbiwy394gghx66miv7vbj")))

(define-public crate-chess-2.0.1 (c (n "chess") (v "2.0.1") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 1)))) (h "16183fmp7nl4h7vhjqwa9q8ywlnp323jpqvmb0iyq6f80r1hc0gj")))

(define-public crate-chess-2.0.2 (c (n "chess") (v "2.0.2") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 1)))) (h "1214zgbcbxy21npb5bf7cyiih88jpryh2yla877mk7pgjaqgfs8s")))

(define-public crate-chess-3.0.0 (c (n "chess") (v "3.0.0") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.13") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 1)))) (h "1ff7bmrl1j5q7l5i06h4n65mbdsxrzc21q570gp03n2qsfhbk64c")))

(define-public crate-chess-3.0.1 (c (n "chess") (v "3.0.1") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.13") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 1)))) (h "0r8vf1wc6dqq6s6jbd8iv3g16f949f7n6xbpmf1ffya1w3zrsz7j")))

(define-public crate-chess-3.0.2 (c (n "chess") (v "3.0.2") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.13") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 1)))) (h "106pxjgr9l4bhxiqzlc6n0y1hnwd9yhajam62ji2m93glmdgzazq")))

(define-public crate-chess-3.1.0 (c (n "chess") (v "3.1.0") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.13") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 1)))) (h "0xbm75i73nyb2273j3sz4dcpz3xq22dr751cqz6bbrp8v149yak7")))

(define-public crate-chess-3.1.1 (c (n "chess") (v "3.1.1") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.13") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 1)))) (h "0dy8bi1symlpzqhiqnd8yxlqrkgdbyljvrlldlx5z34hr43kl6hq")))

(define-public crate-chess-3.2.0 (c (n "chess") (v "3.2.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 1)) (d (n "nodrop") (r "^0.1.14") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (f (quote ("small_rng"))) (k 1)))) (h "1j0f5ymjp3plch7kif4zypaszln1gdpp5mjsjirg6d7cf6qrklif")))

