(define-module (crates-io ch es chesspos) #:use-module (crates-io))

(define-public crate-chesspos-0.1.0 (c (n "chesspos") (v "0.1.0") (h "1dhzn3jlbxd1fdhpg425l9hzd2a1dhd86iwm9abdg1cjziqmhg0p")))

(define-public crate-chesspos-0.1.1 (c (n "chesspos") (v "0.1.1") (h "0bmr3d5s3vj7vpnwrg937qjhbxw9ypgzd4pxsd7360g145k477aq")))

