(define-module (crates-io ch es chester) #:use-module (crates-io))

(define-public crate-chester-0.1.0 (c (n "chester") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "comfy-table") (r "^6.1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "zip-extract") (r "^0.1.2") (d #t) (k 0)))) (h "0zwxyhzq1mhhnc283q90nmfgxaf7nfbm9hjrl0i63vi2kjlf738s")))

