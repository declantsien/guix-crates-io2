(define-module (crates-io ch es chessgen) #:use-module (crates-io))

(define-public crate-chessgen-0.9.1 (c (n "chessgen") (v "0.9.1") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)))) (h "00n8dkjab8081i8sri928ysyz3y2nqrfg3pr2dswjyq1b5h3243v")))

(define-public crate-chessgen-0.9.2 (c (n "chessgen") (v "0.9.2") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)))) (h "0gpmna6ar1mm1bimmy3zfp3s50fir40mssy7bl9gnymg00fxr4j2") (y #t)))

(define-public crate-chessgen-0.9.3 (c (n "chessgen") (v "0.9.3") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)))) (h "1kk5la177vzxm92na9h6da2wgwq8y2n55ymvbbzaphj3fi45514y")))

(define-public crate-chessgen-0.9.4 (c (n "chessgen") (v "0.9.4") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)))) (h "1n46kypg3npd9pbknr72xasx0hg581smibf92rlgm273v1zbcqfk")))

