(define-module (crates-io ch es chessbored) #:use-module (crates-io))

(define-public crate-chessbored-0.1.0 (c (n "chessbored") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.14.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.7.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "1v06v2mkz2z8q3nzknk9hk9zjri8pp1ndi4h6b4fgf7j3szdi85i") (y #t)))

(define-public crate-chessbored-0.1.1 (c (n "chessbored") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.14.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.7.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "1qf1qsypgyb6y67giv5z8vdbx67w96sflhfwfsr99wxydazs15lp")))

(define-public crate-chessbored-0.1.2 (c (n "chessbored") (v "0.1.2") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.14.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.7.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "0g86f655k3wxpi5g4wclh47yii3ac6pmxvn3aabgjw1gc9p85msc")))

(define-public crate-chessbored-0.1.3 (c (n "chessbored") (v "0.1.3") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.14.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.7.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "012qx2cghqh5nkvxiikbf8dxx1kwlq1mbyqxq7j1284vaxb4fqqd")))

(define-public crate-chessbored-0.1.4 (c (n "chessbored") (v "0.1.4") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.14.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.7.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "099r84dymbipqb84li7h1dhnqp8g45hikad2m4d4y6s7251a55rk")))

(define-public crate-chessbored-0.2.0 (c (n "chessbored") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.15.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.8.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "19s8xm7rbxyalbfzlmvi4c237ci8gcsls2v170jlrbhx35klz55i")))

(define-public crate-chessbored-0.2.1 (c (n "chessbored") (v "0.2.1") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.15.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.8.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "19kc8ha0wsnjs0blmbxw8w8vpwrq4gwcdcdrvzsn1wadg1jpxyak")))

(define-public crate-chessbored-0.3.0 (c (n "chessbored") (v "0.3.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.16.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.9.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "0xf4cpqpw9712rljai7kf2rhqsi0cxsz3yz58bf2pdqgfa9yk3h0")))

(define-public crate-chessbored-0.3.1 (c (n "chessbored") (v "0.3.1") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.16.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.9.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "1lqvmhxjjjj0dcjkl7myvdpjmzgwbhgrd8kqk3pbimzp4vfzcj1h")))

(define-public crate-chessbored-0.3.2 (c (n "chessbored") (v "0.3.2") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.16.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.9.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "0adsfwvk98bbdbjcsrsgrqg3gl6zf3nx609ib30h1c60zij8bkff")))

(define-public crate-chessbored-0.3.3 (c (n "chessbored") (v "0.3.3") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.16.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.9.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "1q86wgy9va0kgchyymfaqdjyz82lc312zz8xk5ihs84a6nld4mc1")))

(define-public crate-chessbored-0.3.4 (c (n "chessbored") (v "0.3.4") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.16.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.9.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "0f50sg06i14mcarp5gwxxv7bnzh09lw2ysp19mhqa0dn84hk4bi6")))

(define-public crate-chessbored-0.4.0 (c (n "chessbored") (v "0.4.0") (d (list (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.16.0") (d #t) (k 0)) (d (n "bevy_mod_picking") (r "^0.9.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 0)))) (h "0ybp7a15m2a5zrp74sla51kx6dycw6vwcq3nccqcwsr1a4lhxwq4")))

