(define-module (crates-io ch es chess-core) #:use-module (crates-io))

(define-public crate-chess-core-0.1.0 (c (n "chess-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "chess-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)))) (h "05kxgiy85dv8k30d822pfhq9mr0wci6p55b6ggrwd3r5977mmzfh")))

