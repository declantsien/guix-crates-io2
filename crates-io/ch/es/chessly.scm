(define-module (crates-io ch es chessly) #:use-module (crates-io))

(define-public crate-chessly-0.1.1 (c (n "chessly") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "03f3qdskgvsdglpyfb60bqdz0x55v7my7cm1ib1vkv20xygangs4")))

(define-public crate-chessly-0.1.2 (c (n "chessly") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0336i2jygvg5fwcv9460zlkalji93mdg5lh46iw7dkiy2nnjm04d")))

(define-public crate-chessly-0.1.3 (c (n "chessly") (v "0.1.3") (d (list (d (n "colored") (r "^2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1xyyxn0wsh4xsnh7s863vpj338zjig7pmkhwkbfrqrmk3h21shzv")))

