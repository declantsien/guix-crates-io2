(define-module (crates-io ch es chess_clock) #:use-module (crates-io))

(define-public crate-chess_clock-0.1.0 (c (n "chess_clock") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.20") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "06ssq5k7d2qx6bpaa89wkdb2zbqfzszpxf9jxs54mknj9khzph56")))

(define-public crate-chess_clock-0.1.1 (c (n "chess_clock") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.20") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "0h4x1ndnpalcpgik16fa1q0kw129a32vjb7amsr7fg4n0kn362q9")))

