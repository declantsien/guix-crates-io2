(define-module (crates-io ch es chessdev) #:use-module (crates-io))

(define-public crate-chessdev-0.1.0 (c (n "chessdev") (v "0.1.0") (h "1g40spgb0438k03m0k1srpzj94xbqgdmg45zvm4wkq24gj13psmx")))

(define-public crate-chessdev-0.1.1 (c (n "chessdev") (v "0.1.1") (h "1mg6ibf6plvzz02rqvqikr69g28ihkl9h6fk3173rmqrx79kgjyc")))

