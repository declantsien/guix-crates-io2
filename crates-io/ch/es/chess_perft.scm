(define-module (crates-io ch es chess_perft) #:use-module (crates-io))

(define-public crate-chess_perft-0.0.3 (c (n "chess_perft") (v "0.0.3") (d (list (d (n "chess") (r "^0.1.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0kwnhvlf9rz30jfrmq7b73jz2nj8g37kx3chx278slmncfk72vys")))

(define-public crate-chess_perft-0.0.4 (c (n "chess_perft") (v "0.0.4") (d (list (d (n "chess") (r "^0.1.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "196a9hdva9x8nmjvwl45afc9hbgz2kc9i8f9xvl9mzzgcqv9zqfs")))

(define-public crate-chess_perft-0.0.5 (c (n "chess_perft") (v "0.0.5") (d (list (d (n "chess") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0avhl4i6qq5hhydmqlgr52fy35hzh88kbk43sf4gx5ipgam592dy")))

(define-public crate-chess_perft-0.0.6 (c (n "chess_perft") (v "0.0.6") (d (list (d (n "chess") (r "^0.2.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "125d3a1nv507vir7ar379dzgnx5h0429qwincf5yp0g7l6h8p9my")))

(define-public crate-chess_perft-0.0.7 (c (n "chess_perft") (v "0.0.7") (d (list (d (n "chess") (r "^0.3.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1iisxmpqnm5ccnjf8f17w2z82dpizr4navsirwv1wkic4c1ij0fi")))

(define-public crate-chess_perft-0.1.0 (c (n "chess_perft") (v "0.1.0") (d (list (d (n "chess") (r "^0.4.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0hh9450czrghqfxrg65r1cqb2p6k1nq65cvrvgi9bb3sdq84fksz")))

(define-public crate-chess_perft-1.0.0 (c (n "chess_perft") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chess") (r "^0.4.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "shakmaty") (r "^0.8.1") (d #t) (k 0)))) (h "0hq741v027raf1wyiy7q5zj27964c2mv9zvd2fwk1dg67ghrmsll")))

(define-public crate-chess_perft-1.0.1 (c (n "chess_perft") (v "1.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chess") (r "^0.4.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "shakmaty") (r "^0.8.1") (d #t) (k 0)))) (h "0n133minqlb14rq0m1y4jv3qlcqin765yd2zwp7741nk55rcqja4")))

(define-public crate-chess_perft-1.0.2 (c (n "chess_perft") (v "1.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chess") (r "^0.4.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "shakmaty") (r "^0.8.1") (d #t) (k 0)))) (h "17l707a0a0irsxyrq6a1chda0qy041sh2a8k9n4qyivkq94a523c")))

(define-public crate-chess_perft-1.0.3 (c (n "chess_perft") (v "1.0.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chess") (r "^1.0.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "shakmaty") (r "^0.8.1") (d #t) (k 0)))) (h "1i22jqwxkhbliq2dipnzv435nh424v0a40zbiwf8ril0dyx9az0z")))

(define-public crate-chess_perft-1.0.4 (c (n "chess_perft") (v "1.0.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chess") (r "^1.0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "shakmaty") (r "^0.8.1") (d #t) (k 0)))) (h "10gy9k8knahkq3xf6s5z4imawl95bvsra25968kfraa6c789bxgn")))

(define-public crate-chess_perft-1.0.5 (c (n "chess_perft") (v "1.0.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chess") (r "^1.0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "shakmaty") (r "^0.9.0") (d #t) (k 0)))) (h "0kh4m0aws75zmhq9zcsz7knk3dmhdjb4p1ki9dcnwjrbz4ms2zn0")))

(define-public crate-chess_perft-1.0.7 (c (n "chess_perft") (v "1.0.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chess") (r "^1.0.7") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "shakmaty") (r "^0.11.2") (d #t) (k 0)))) (h "15wa84ggvrmaz8bdmswq51m0smi57cjs7069mdq21ikpwh64kz7n")))

(define-public crate-chess_perft-2.0.1 (c (n "chess_perft") (v "2.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chess") (r "^2.0.1") (d #t) (k 0)) (d (n "chess-move-gen") (r "^0.6.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "shakmaty") (r "^0.11.2") (d #t) (k 0)))) (h "1b7gl7bxzkgxjccsjsvipagfgrflnys3lbq1zb5d517m10nw4sbd")))

(define-public crate-chess_perft-3.0.1 (c (n "chess_perft") (v "3.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chess") (r "^3.0.1") (d #t) (k 0)) (d (n "chess-move-gen") (r "^0.6.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "shakmaty") (r "^0.13.0") (d #t) (k 0)))) (h "10r7yh44pnksankzqhcqdgr3fakz4fsqf2pnbhhz56z6angpabwc")))

(define-public crate-chess_perft-3.1.1 (c (n "chess_perft") (v "3.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chess") (r "^3.1.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.19") (d #t) (k 0)) (d (n "shakmaty") (r "^0.15.0") (d #t) (k 0)))) (h "0gi5llh8s7yyv2czf7iy2m7lzmgvixj6lrfxv3j5msysy9f9wa0z")))

