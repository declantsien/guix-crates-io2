(define-module (crates-io ch es chess_compression) #:use-module (crates-io))

(define-public crate-chess_compression-0.1.0 (c (n "chess_compression") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "bitbit") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.19") (d #t) (k 2)) (d (n "shakmaty") (r "^0.20") (d #t) (k 0)))) (h "1b3nhm3xlk56m9pazalz01w96l6fgwk2l4vrbxpskbimsccj781c")))

