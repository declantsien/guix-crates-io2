(define-module (crates-io ch es chesscom) #:use-module (crates-io))

(define-public crate-chesscom-0.1.0 (c (n "chesscom") (v "0.1.0") (d (list (d (n "chesscom-openapi") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "17gxsfmsfkqv3dfbd6q2k7v4jm3lm79w8mvs4imvhnwnvxsz6b75")))

(define-public crate-chesscom-0.1.1 (c (n "chesscom") (v "0.1.1") (d (list (d (n "chesscom-openapi") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ivq6d32d38g54cf5jzfhmp8g2bqcg86p95qim90j4q1q4fvwwsy") (y #t)))

(define-public crate-chesscom-0.1.2 (c (n "chesscom") (v "0.1.2") (d (list (d (n "chesscom-openapi") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1zfvn16r31r73r6is55gjm5bkdiqbzs2yb1z1jblnl7pwwbd9h70")))

(define-public crate-chesscom-0.1.3 (c (n "chesscom") (v "0.1.3") (d (list (d (n "chesscom-openapi") (r "=0.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "05pd8685sa65xcs2a2lmsgvcbgchm84yqjkg43sf5phzd4v3x4p3")))

