(define-module (crates-io ch es chess-rs) #:use-module (crates-io))

(define-public crate-chess-rs-0.1.0 (c (n "chess-rs") (v "0.1.0") (d (list (d (n "matrix_display") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.2") (d #t) (k 0)))) (h "1wk8g7dk4rz5w2cf9cj1g5bapklkm0p03d44gyvf7d9qgf1lyq6r")))

