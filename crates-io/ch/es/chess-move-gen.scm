(define-module (crates-io ch es chess-move-gen) #:use-module (crates-io))

(define-public crate-chess-move-gen-0.1.0 (c (n "chess-move-gen") (v "0.1.0") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "simd") (r "0.*") (d #t) (k 0)) (d (n "unindent") (r "0.*") (d #t) (k 0)))) (h "14x5fzppacvy2mc2g7v0ga3ygf4gyzmjvv7ihn6nw5nqwz5hgy35")))

(define-public crate-chess-move-gen-0.2.0 (c (n "chess-move-gen") (v "0.2.0") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "simd") (r "0.*") (d #t) (k 0)) (d (n "unindent") (r "0.*") (d #t) (k 0)))) (h "01mr0qwxg2vvprh51cfwg8zvw9v79x37n6yg0yi9wqrmarbj0lzm")))

(define-public crate-chess-move-gen-0.2.1 (c (n "chess-move-gen") (v "0.2.1") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "simd") (r "0.*") (d #t) (k 0)) (d (n "unindent") (r "0.*") (d #t) (k 0)))) (h "0jvqf15d6i58rm9plq51fp4id967c4pjaxvma7m4557gmfwnfkb9")))

(define-public crate-chess-move-gen-0.2.2 (c (n "chess-move-gen") (v "0.2.2") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "simd") (r "0.*") (d #t) (k 0)) (d (n "unindent") (r "0.*") (d #t) (k 0)))) (h "0rhxmxqar5mrvnwc9w6scfypakm5mf4y02rsan76bqvqlwlzx12h")))

(define-public crate-chess-move-gen-0.3.0 (c (n "chess-move-gen") (v "0.3.0") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "simd") (r "0.*") (d #t) (k 0)) (d (n "unindent") (r "0.*") (d #t) (k 0)))) (h "0rmkg9i737dnasr1ymipa8sdbvgjm5s8psyf8mxk5p286qam54m3")))

(define-public crate-chess-move-gen-0.3.1 (c (n "chess-move-gen") (v "0.3.1") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "simd") (r "0.*") (d #t) (k 0)) (d (n "unindent") (r "0.*") (d #t) (k 0)))) (h "0ldalhg5f4did5naib8v30qmgx26y33xxl643yh4phh4c4r5m4hd")))

(define-public crate-chess-move-gen-0.3.2 (c (n "chess-move-gen") (v "0.3.2") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "simd") (r "0.*") (d #t) (k 0)) (d (n "unindent") (r "0.*") (d #t) (k 0)))) (h "03jh2lp2my11j7qpf6fksnfxwcjja4yrgl02vn3p8k25r0031vhm")))

(define-public crate-chess-move-gen-0.3.3 (c (n "chess-move-gen") (v "0.3.3") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "simd") (r "0.*") (d #t) (k 0)) (d (n "unindent") (r "0.*") (d #t) (k 0)))) (h "0wn88m5g33gzw0g8d9k6k59hr27wydh1f3ax88ka3hm4zdp735zd")))

(define-public crate-chess-move-gen-0.3.4 (c (n "chess-move-gen") (v "0.3.4") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "simd") (r "0.*") (d #t) (k 0)) (d (n "unindent") (r "0.*") (d #t) (k 0)))) (h "09yqksc2fyp2b97zb96m8jkk3xvxkhzjsygpk4jgabxcdksb34f6")))

(define-public crate-chess-move-gen-0.3.5 (c (n "chess-move-gen") (v "0.3.5") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "simd") (r "0.*") (d #t) (k 0)) (d (n "unindent") (r "0.*") (d #t) (k 0)))) (h "1qcqyl90vmrsx5hldg6d55vl5014zbkmc3vf4n1aps26qf25wrf8")))

(define-public crate-chess-move-gen-0.3.6 (c (n "chess-move-gen") (v "0.3.6") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "simd") (r "0.*") (d #t) (k 0)) (d (n "unindent") (r "0.*") (d #t) (k 0)))) (h "0lvfkbvivb42l4g1wwjwmkrl31g58nzbv6f1bv4w6ylmjqrjmfii")))

(define-public crate-chess-move-gen-0.3.9 (c (n "chess-move-gen") (v "0.3.9") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "simd") (r "0.*") (d #t) (k 0)) (d (n "unindent") (r "0.*") (d #t) (k 0)))) (h "1dys6avllvk7mm9j4v9nw1ha411wac6211yn8qqzmhsmkzd2skdg")))

(define-public crate-chess-move-gen-0.5.0 (c (n "chess-move-gen") (v "0.5.0") (d (list (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "simd") (r "0.*") (d #t) (k 0)) (d (n "unindent") (r "0.*") (d #t) (k 0)))) (h "1zi2qr0hqkpppnfk5biprn88sdcwkih3nn6j553i8l4vhdgkrsqj")))

(define-public crate-chess-move-gen-0.5.1 (c (n "chess-move-gen") (v "0.5.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "simd") (r "^0.2.2") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "02al996imqk7g9yily8syz4a3givc3hdb6h2pzjzxjlj8ln0403x")))

(define-public crate-chess-move-gen-0.5.2 (c (n "chess-move-gen") (v "0.5.2") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "simd") (r "^0.2.2") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "11r89qq2s1gnvfa3bc4jbm9sm3v7sdmfwix7aylyf20vh80jh9hj")))

(define-public crate-chess-move-gen-0.6.0 (c (n "chess-move-gen") (v "0.6.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "simd") (r "^0.2.2") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "0f7hrkn46klmahxl8nf0s4sm71rywq0cvprvc279m84mgvddfwd2")))

(define-public crate-chess-move-gen-0.6.1 (c (n "chess-move-gen") (v "0.6.1") (d (list (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "simd") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "02adp821n82zjd01xwd4xv82iim2yl443sg5wih45s1ycz0z0f5y")))

(define-public crate-chess-move-gen-0.6.2 (c (n "chess-move-gen") (v "0.6.2") (d (list (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "simd") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "1bgxpy4mqi9x5lbf5zy88151xpvz35cw9smp816icli0ipqnbbwc")))

(define-public crate-chess-move-gen-0.6.3 (c (n "chess-move-gen") (v "0.6.3") (d (list (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "simd") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "1393127i1aznqi4n2yn0rzbpy3phwkkb38ymbn7kc6kg53hrlllv")))

(define-public crate-chess-move-gen-0.6.4 (c (n "chess-move-gen") (v "0.6.4") (d (list (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "simd") (r "^0.2.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "17nq5i31i2n17dya89pjfskrgr2hafgvfln7xc34f9fa9f1i40pg")))

(define-public crate-chess-move-gen-0.6.5 (c (n "chess-move-gen") (v "0.6.5") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.6") (d #t) (k 0)))) (h "0jysd7aymz5fa5lnak0f9dqjhmg3pjybc9bpxq94vmn78afggsn7")))

(define-public crate-chess-move-gen-0.6.6 (c (n "chess-move-gen") (v "0.6.6") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.6") (d #t) (k 0)))) (h "12gcyjap6f8556jff5bikgn7dvdzqfa4yrlnn8lpymzyq65mdi89")))

(define-public crate-chess-move-gen-0.7.0 (c (n "chess-move-gen") (v "0.7.0") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.6") (d #t) (k 0)))) (h "1hf6b7a9dl2f72ca2g3hzqjksdrk6h8bsvl3cn5s6f1nydazw9j8")))

(define-public crate-chess-move-gen-0.7.2 (c (n "chess-move-gen") (v "0.7.2") (d (list (d (n "num_cpus") (r ">=1.13.0, <2.0.0") (d #t) (k 0)) (d (n "packed_simd") (r ">=0.3.3, <0.4.0") (d #t) (k 0)) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)) (d (n "threadpool") (r ">=1.8.1, <2.0.0") (d #t) (k 0)) (d (n "unindent") (r ">=0.1.6, <0.2.0") (d #t) (k 0)))) (h "17z8vg5aq1wkbskahy5kah95ql7vgbfp0gb785770rc63drhb258")))

(define-public crate-chess-move-gen-0.7.3 (c (n "chess-move-gen") (v "0.7.3") (d (list (d (n "num_cpus") (r ">=1.13.0, <2.0.0") (d #t) (k 0)) (d (n "packed_simd") (r ">=0.3.4, <0.4.0") (d #t) (k 0) (p "packed_simd_2")) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)) (d (n "threadpool") (r ">=1.8.1, <2.0.0") (d #t) (k 0)) (d (n "unindent") (r ">=0.1.7, <0.2.0") (d #t) (k 0)))) (h "0ac2ph5aqxyhzxw9xq3g3rk1zqwxamd66x20svcbqkl2sszvd2r3")))

(define-public crate-chess-move-gen-0.7.4 (c (n "chess-move-gen") (v "0.7.4") (d (list (d (n "num_cpus") (r ">=1.13.0, <2.0.0") (d #t) (k 0)) (d (n "packed_simd") (r ">=0.3.4, <0.4.0") (d #t) (k 0) (p "packed_simd_2")) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)) (d (n "threadpool") (r ">=1.8.1, <2.0.0") (d #t) (k 0)) (d (n "unindent") (r ">=0.1.7, <0.2.0") (d #t) (k 2)))) (h "0zss0is1ay8vlmmpsw9cxhlls21xs3xx6hb5gkvdl408nn5ffhch")))

(define-public crate-chess-move-gen-0.7.5 (c (n "chess-move-gen") (v "0.7.5") (d (list (d (n "num_cpus") (r ">=1.13.0, <2.0.0") (d #t) (k 0)) (d (n "packed_simd") (r ">=0.3.4, <0.4.0") (d #t) (k 0) (p "packed_simd_2")) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)) (d (n "threadpool") (r ">=1.8.1, <2.0.0") (d #t) (k 0)) (d (n "unindent") (r ">=0.1.7, <0.2.0") (d #t) (k 2)))) (h "1w5zvg9sf0zcj2xircbjhqqvmb3425a2zfam0l5xk137xg8fi7sh")))

(define-public crate-chess-move-gen-0.7.6 (c (n "chess-move-gen") (v "0.7.6") (d (list (d (n "num_cpus") (r ">=1.13.0, <2.0.0") (d #t) (k 0)) (d (n "packed_simd") (r ">=0.3.4, <0.4.0") (d #t) (k 0) (p "packed_simd_2")) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)) (d (n "threadpool") (r ">=1.8.1, <2.0.0") (d #t) (k 0)) (d (n "unindent") (r ">=0.1.7, <0.2.0") (d #t) (k 2)))) (h "14gfy93c6fzrjwnvpw0773hvxcncvjy76az3vgywwmcmwsnp4qgm")))

(define-public crate-chess-move-gen-0.7.7 (c (n "chess-move-gen") (v "0.7.7") (d (list (d (n "num_cpus") (r ">=1.13.0, <2.0.0") (d #t) (k 0)) (d (n "packed_simd") (r ">=0.3.4, <0.4.0") (d #t) (k 0) (p "packed_simd_2")) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)) (d (n "threadpool") (r ">=1.8.1, <2.0.0") (d #t) (k 0)) (d (n "unindent") (r ">=0.1.7, <0.2.0") (d #t) (k 2)))) (h "0b1j2q0rbyrp5y29b72qc0af4rpxw2scgwlc7c05vfz9kxd2iqwl")))

(define-public crate-chess-move-gen-0.7.8 (c (n "chess-move-gen") (v "0.7.8") (d (list (d (n "num_cpus") (r ">=1.13.0, <2.0.0") (d #t) (k 0)) (d (n "packed_simd") (r ">=0.3.4, <0.4.0") (d #t) (k 0) (p "packed_simd_2")) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)) (d (n "threadpool") (r ">=1.8.1, <2.0.0") (d #t) (k 0)) (d (n "unindent") (r ">=0.1.7, <0.2.0") (d #t) (k 2)))) (h "18y65xg2qgsqw6724k4dsiz01072gmci8lzywkadvn6d1nq7p8zs")))

(define-public crate-chess-move-gen-0.8.0 (c (n "chess-move-gen") (v "0.8.0") (d (list (d (n "num_cpus") (r ">=1.13.0, <2.0.0") (d #t) (k 0)) (d (n "packed_simd") (r ">=0.3.4, <0.4.0") (d #t) (k 0) (p "packed_simd_2")) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)) (d (n "threadpool") (r ">=1.8.1, <2.0.0") (d #t) (k 0)) (d (n "unindent") (r ">=0.1.7, <0.2.0") (d #t) (k 2)))) (h "05pi5sazvpfl8kbx58rmmzkbf6f5fyr2ccs1zlnkrg94n35kvkvq")))

(define-public crate-chess-move-gen-0.8.2 (c (n "chess-move-gen") (v "0.8.2") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.4") (d #t) (k 0) (p "packed_simd_2")) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.7") (d #t) (k 2)))) (h "1alba8nmdyk1p2yz5kqd2xfg6b770vysfmbh0j8bg7l4m71q4nix")))

