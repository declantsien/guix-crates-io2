(define-module (crates-io ch es chess-turn-engine) #:use-module (crates-io))

(define-public crate-chess-turn-engine-0.1.0 (c (n "chess-turn-engine") (v "0.1.0") (d (list (d (n "chess-notation-parser") (r "^0.2.0") (d #t) (k 0)))) (h "0jkyih0sv2h1ab6jy20pgzcjmnh7pf3pb2hpdbygwksibgm2927z")))

(define-public crate-chess-turn-engine-0.1.1 (c (n "chess-turn-engine") (v "0.1.1") (d (list (d (n "chess-notation-parser") (r "^0.2") (d #t) (k 0)))) (h "0j5nclvflywsxyfq2yf0455qns98p74s668sg1kh127gc91n8zn1")))

(define-public crate-chess-turn-engine-0.1.2 (c (n "chess-turn-engine") (v "0.1.2") (d (list (d (n "chess-notation-parser") (r "^0.2") (d #t) (k 0)))) (h "1w2b9q3hl088pjnvgn4xf5f1sv4hqamvx7pp1rxblfrc3wnplmb9")))

(define-public crate-chess-turn-engine-0.1.3 (c (n "chess-turn-engine") (v "0.1.3") (d (list (d (n "chess-notation-parser") (r "^0.2") (d #t) (k 0)))) (h "1r7ymb5dddh6m9sgl10pgym9pwcbn9ak994wfvab1yip55xgrasj")))

