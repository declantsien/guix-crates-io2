(define-module (crates-io ch es chess-engine) #:use-module (crates-io))

(define-public crate-chess-engine-0.1.0 (c (n "chess-engine") (v "0.1.0") (h "1w6325zcahdk9wcj09hk5wx88k8k1ldg8ar6jvx7nkq07d0xs6vx")))

(define-public crate-chess-engine-0.1.1 (c (n "chess-engine") (v "0.1.1") (h "161hq2hk5wdf0nf81dc15ha1m2ynimcvcc6blh8yl180mxx4sxa1")))

(define-public crate-chess-engine-0.1.2 (c (n "chess-engine") (v "0.1.2") (h "1dl6y4yhll6qk71z6awghkr3l76rggc3b4pqzqs6242gf7gq8g9c")))

