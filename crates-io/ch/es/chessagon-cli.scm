(define-module (crates-io ch es chessagon-cli) #:use-module (crates-io))

(define-public crate-chessagon-cli-0.1.0 (c (n "chessagon-cli") (v "0.1.0") (d (list (d (n "chessagon") (r "0.*") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "hext-boards") (r "0.*") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (f (quote ("with-sqlite-history"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0da1msdxdk8yq89mxvhphmc8msv90bs48si9wqjidj396mr7vsm9")))

