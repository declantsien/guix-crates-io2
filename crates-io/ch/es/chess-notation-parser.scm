(define-module (crates-io ch es chess-notation-parser) #:use-module (crates-io))

(define-public crate-chess-notation-parser-0.1.0 (c (n "chess-notation-parser") (v "0.1.0") (h "0mb2cp0f0rb058l4asgqk86xhs4vj334az72cb6b4yps9f11h8hk") (y #t)))

(define-public crate-chess-notation-parser-0.2.0 (c (n "chess-notation-parser") (v "0.2.0") (h "0hsvhgh77hzwk29frnkg7nygc755bw773l16nz56zxhinx9wz9wa")))

(define-public crate-chess-notation-parser-0.2.1 (c (n "chess-notation-parser") (v "0.2.1") (h "1d8zj568ia2w7mzx94is08fvlv4v3am8z256zagz6kwmjpq83jq4")))

(define-public crate-chess-notation-parser-0.2.2 (c (n "chess-notation-parser") (v "0.2.2") (h "1jb199fi4cgnnbh7w19h11wd3hx06x2i87n53bah6wld4yy520qs")))

(define-public crate-chess-notation-parser-0.2.3 (c (n "chess-notation-parser") (v "0.2.3") (h "1x4gjj86xvkl47jmpv664hap1dap5i74iygnd86z6bfk6gxgs8gs")))

