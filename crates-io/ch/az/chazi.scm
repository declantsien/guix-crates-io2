(define-module (crates-io ch az chazi) #:use-module (crates-io))

(define-public crate-chazi-0.1.0 (c (n "chazi") (v "0.1.0") (d (list (d (n "chazi_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "0bqys4jkx25jw079mqim3h7hkwnik18mn067sk5jmyfqasz1i9np")))

(define-public crate-chazi-0.1.2 (c (n "chazi") (v "0.1.2") (d (list (d (n "chazi_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "0cq8qnsaw6pfb36pav93hvpc8sk8sfx0miq2r0zhpqcdzl2zq9n7")))

