(define-module (crates-io ch id chidotestcrate_c) #:use-module (crates-io))

(define-public crate-chidotestcrate_c-0.1.0 (c (n "chidotestcrate_c") (v "0.1.0") (d (list (d (n "chidotestcrate_a") (r "=0.1.0") (d #t) (k 0)) (d (n "chidotestcrate_b") (r "=0.1.0") (d #t) (k 0)))) (h "01dxkb32xapjqlin226bdjv08zvzjqrdvk71pd1cywypl1n0l9wx")))

(define-public crate-chidotestcrate_c-0.2.0 (c (n "chidotestcrate_c") (v "0.2.0") (d (list (d (n "chidotestcrate_a") (r "=0.2.0") (d #t) (k 0)) (d (n "chidotestcrate_b") (r "=0.2.0") (d #t) (k 0)))) (h "1cd3x31h48k1zj91940y9adn5clzwchi1a5n8w1s4vr4r1wia0c9")))

(define-public crate-chidotestcrate_c-0.3.0 (c (n "chidotestcrate_c") (v "0.3.0") (d (list (d (n "chidotestcrate_a") (r "=0.3.0") (d #t) (k 0)) (d (n "chidotestcrate_b") (r "=0.3.0") (d #t) (k 0)))) (h "1hvw3d7f8g9fimn30bscck2kfka3hgr0sh9jxxfs9gn6gigfg57k")))

(define-public crate-chidotestcrate_c-0.5.0 (c (n "chidotestcrate_c") (v "0.5.0") (d (list (d (n "chidotestcrate_a") (r "=0.3.0") (d #t) (k 0)) (d (n "chidotestcrate_b") (r "=0.3.0") (d #t) (k 0)))) (h "1nr378rzrk3n4zcr07mmx8dd152hzkv0wcs7gh4r514p7409f58s")))

