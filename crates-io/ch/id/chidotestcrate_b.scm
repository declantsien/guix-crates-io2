(define-module (crates-io ch id chidotestcrate_b) #:use-module (crates-io))

(define-public crate-chidotestcrate_b-0.1.0 (c (n "chidotestcrate_b") (v "0.1.0") (d (list (d (n "chidotestcrate_a") (r "=0.1.0") (d #t) (k 0)))) (h "16943025ild2ms632z22iwg899sgyc6yk6gg3f67nss55kg84l75")))

(define-public crate-chidotestcrate_b-0.2.0 (c (n "chidotestcrate_b") (v "0.2.0") (d (list (d (n "chidotestcrate_a") (r "=0.2.0") (d #t) (k 0)))) (h "0xzfvp6l6xbmylz9sjlfhmlfl9hkbc5rnb04iim8f7gr6kzlknr4")))

(define-public crate-chidotestcrate_b-0.3.0 (c (n "chidotestcrate_b") (v "0.3.0") (d (list (d (n "chidotestcrate_a") (r "=0.3.0") (d #t) (k 0)))) (h "02nhpzc18vlh1sd3lkh7nal3k9pqzwkxyxhdcvpw3r46w2ya0hvg")))

(define-public crate-chidotestcrate_b-0.5.0 (c (n "chidotestcrate_b") (v "0.5.0") (d (list (d (n "chidotestcrate_a") (r "=0.3.0") (d #t) (k 0)))) (h "0s02zdjkzg2zn65jf8l58sf7jzbz85jpyvk1wncqfl78175i8qri")))

