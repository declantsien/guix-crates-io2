(define-module (crates-io ch id chidotestcrate_a) #:use-module (crates-io))

(define-public crate-chidotestcrate_a-0.1.0 (c (n "chidotestcrate_a") (v "0.1.0") (h "07nwrmmhfmzwnc058dd2nvi26qzwdpgd4kbscl3w3jbm3km2wx63")))

(define-public crate-chidotestcrate_a-0.2.0 (c (n "chidotestcrate_a") (v "0.2.0") (h "1bm6hjv6hafhsjwi1a71pp7ni3jl1ap0xzjjln7v0lmwfgqp7vpz")))

(define-public crate-chidotestcrate_a-0.3.0 (c (n "chidotestcrate_a") (v "0.3.0") (h "19mi6k1b1ni2i985ml4wbdw5ld01apl30s6hjns1qbyf2qi49vbp")))

(define-public crate-chidotestcrate_a-0.4.0 (c (n "chidotestcrate_a") (v "0.4.0") (h "0sqzw2bg2jayi8rzyy49i1nw5wk69d68zyhr77kiq79pvld3sg2f")))

(define-public crate-chidotestcrate_a-0.5.0 (c (n "chidotestcrate_a") (v "0.5.0") (h "1fn3plbs59nvdc6cjn9k37azngfny1m6v3accaqg8dcxapbpj154")))

