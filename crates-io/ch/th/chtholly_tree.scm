(define-module (crates-io ch th chtholly_tree) #:use-module (crates-io))

(define-public crate-chtholly_tree-0.1.0 (c (n "chtholly_tree") (v "0.1.0") (h "1rhvhm3hy1fb4pbk5kwmllksqbp472kx12sgsfsybmmpsw53vc58")))

(define-public crate-chtholly_tree-1.0.0 (c (n "chtholly_tree") (v "1.0.0") (h "02y0aq2na6cwd54vvg55863xdgb94b3s4nny6cmdnswyizfy7x5n")))

