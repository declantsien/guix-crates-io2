(define-module (crates-io ch th chtholly) #:use-module (crates-io))

(define-public crate-chtholly-0.1.0 (c (n "chtholly") (v "0.1.0") (h "1pbp3a1kb7jzvgg6ynbb8m1j9zwwwv939864jf6r62qzvv9qz272")))

(define-public crate-chtholly-0.2.0 (c (n "chtholly") (v "0.2.0") (h "16f1jy62k8xkkznafp6d803bgpafdl7g9svlgrhjvafsb0rxg82d")))

