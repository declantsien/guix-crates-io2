(define-module (crates-io ch iz chizmo) #:use-module (crates-io))

(define-public crate-chizmo-0.1.0 (c (n "chizmo") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "periodic_table") (r "^0.5.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1vqf8bmn5ybc1nrhp5a8i72392fv2903lz916q9x8hhagymmfp3b")))

