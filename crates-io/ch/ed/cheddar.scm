(define-module (crates-io ch ed cheddar) #:use-module (crates-io))

(define-public crate-cheddar-0.1.0 (c (n "cheddar") (v "0.1.0") (d (list (d (n "glsl") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "warmy") (r "^0.7") (d #t) (k 0)))) (h "0hb8hi3iag66b6wjykskjlnwsqrw74vf7433rynixfzzsszjn4xs")))

(define-public crate-cheddar-0.2.0 (c (n "cheddar") (v "0.2.0") (d (list (d (n "glsl") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "warmy") (r "^0.10") (d #t) (k 0)))) (h "1n2b7mrzaijay8cxs3dy62y3hb98ssmwszqfs3yg9271gi1frn7j")))

(define-public crate-cheddar-0.2.1 (c (n "cheddar") (v "0.2.1") (d (list (d (n "glsl") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "warmy") (r "^0.10") (d #t) (k 0)))) (h "08bmbj3n8grj2d2x2yvykvj4la2f29pl3v11b2s6xgm09qxi1grm")))

