(define-module (crates-io ch am chamomile_types) #:use-module (crates-io))

(define-public crate-chamomile_types-0.2.0 (c (n "chamomile_types") (v "0.2.0") (d (list (d (n "async-channel") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kb2yzssspa5n9slmfdfn5jyjjdsa134i6yzdvnmrg5g20rcpq2f")))

(define-public crate-chamomile_types-0.4.0 (c (n "chamomile_types") (v "0.4.0") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)))) (h "172x6vwk35r83ia3f2faa3qhm169zh4cidb25zywzrb0z018ar1a")))

(define-public crate-chamomile_types-0.5.0 (c (n "chamomile_types") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0p8nwdpfhhi95qlid32c5hp8rxvb7myc37c8jaxh3bychcwcivib")))

(define-public crate-chamomile_types-0.6.0 (c (n "chamomile_types") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "00xylcidwd8mcfida9g4ayrlxq5ymsr2p8qy3mp94afxj1fx43wl")))

(define-public crate-chamomile_types-0.6.1 (c (n "chamomile_types") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0dblf19qh7i1xb8x6hsrr2a7bihgy73mf7c2d6q8wn1zmrqzcdqy")))

(define-public crate-chamomile_types-0.7.0 (c (n "chamomile_types") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0mb0cm6n9yfmbw3fw7zj23qap90h9wx3hc09m0chxp3ngkvdg6mf")))

(define-public crate-chamomile_types-0.7.1 (c (n "chamomile_types") (v "0.7.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1j9ilpxh640bp1s2jg1lhszsn5wfv9619lzzs4ka86bxwgkid15s")))

(define-public crate-chamomile_types-0.8.0 (c (n "chamomile_types") (v "0.8.0") (d (list (d (n "ed25519-dalek") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "ripemd") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1g47id2cqc4rsz096x0md4wg01df8dwdmmmq46ma8x9vgmaxwxi3")))

(define-public crate-chamomile_types-0.10.0 (c (n "chamomile_types") (v "0.10.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.24") (f (quote ("recovery" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "10w2jynl7wglijbzfiv18m265d9v0zvm2zc3xhca3ixcg9z7y32x")))

(define-public crate-chamomile_types-0.10.1 (c (n "chamomile_types") (v "0.10.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.24") (f (quote ("recovery" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "13wlxxax9fnf7bwys4v2983g03lqddm3qfigk3xwfn8rrj56sb79")))

(define-public crate-chamomile_types-0.10.2 (c (n "chamomile_types") (v "0.10.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.25") (f (quote ("recovery" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1w3pdkq495mzb7jmqkwilhbwain45cgird2ndg79lmkp3inkaibz")))

(define-public crate-chamomile_types-0.10.3 (c (n "chamomile_types") (v "0.10.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.26") (f (quote ("recovery" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1kcgfwnv3qg4v0zj5j7hkm9hck9ymk78ckr6xim3ricqjp6frpjy")))

(define-public crate-chamomile_types-0.10.4 (c (n "chamomile_types") (v "0.10.4") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.27") (f (quote ("recovery" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "18mag40a7hy44qr9gyd4njv9bbvfswjngb8bi1m7c9ryv17ph83w")))

(define-public crate-chamomile_types-0.10.5 (c (n "chamomile_types") (v "0.10.5") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.27") (f (quote ("recovery" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0qqpnzja4i3pc6lkd33gcpb1n07rirk41v7ngzvm5gyvliid2qrh")))

(define-public crate-chamomile_types-0.10.6 (c (n "chamomile_types") (v "0.10.6") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.27") (f (quote ("recovery" "rand"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0yzhyvil1sbhs8andwnhdgph8hw6dnyr940486ndc7vcvdamk5rq")))

