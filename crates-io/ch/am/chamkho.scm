(define-module (crates-io ch am chamkho) #:use-module (crates-io))

(define-public crate-chamkho-0.0.1 (c (n "chamkho") (v "0.0.1") (h "0zyql8rsbiis5x914gf2hw2wzm1cd1ndhx81lcxqa22mbh8dfpsb")))

(define-public crate-chamkho-0.0.2 (c (n "chamkho") (v "0.0.2") (h "10ikgiqf8ha160ygln6xdv4pincyrjkr44k9ybqby8l6lvflq5hi")))

(define-public crate-chamkho-0.0.3 (c (n "chamkho") (v "0.0.3") (h "1j8zmh5ydh9fb9g71hp7gm5ff2df3bppb5x5fn9hr4ba5frlzcyb")))

(define-public crate-chamkho-0.0.4 (c (n "chamkho") (v "0.0.4") (h "0jnsb1qxmglzhcq4rl610h1jbwfbkiz0y052565q42rypry41rjc")))

(define-public crate-chamkho-0.0.5 (c (n "chamkho") (v "0.0.5") (h "1g2773xdnk47lj4qn7nymb8rq3a3y1qq38zcmwbyrgj38ansj864")))

(define-public crate-chamkho-0.0.7 (c (n "chamkho") (v "0.0.7") (h "1m2zrh0hvwh9k03py9r16p0z7pw4r3z74cxgalla2gj5mqzyp0zs")))

(define-public crate-chamkho-0.0.8 (c (n "chamkho") (v "0.0.8") (h "0bcf68vhpa8b6y5k6d8i04g36i20vs08wb4yypk95jam6qqdbi5n")))

(define-public crate-chamkho-0.0.9 (c (n "chamkho") (v "0.0.9") (h "1pkr8f8iscb7f3nfnhr3vavbx39mbv09zyfmi0684pa8sr3vkw2z")))

(define-public crate-chamkho-0.0.10 (c (n "chamkho") (v "0.0.10") (h "0c0g11b98yvkaqnxqxa33jsc5aaqhky9xbb6ad49vc0flwpplqlh")))

(define-public crate-chamkho-0.0.11 (c (n "chamkho") (v "0.0.11") (h "05p30ddmb3g8q1hp8ly97q2991bx02nv6rzdb8qy2q2h8m99sqyh")))

(define-public crate-chamkho-0.0.12 (c (n "chamkho") (v "0.0.12") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)))) (h "1l9sh5knx3wrn02qvqk60wbqrq7hq7xmll266da3kh5wi2xncsgc")))

(define-public crate-chamkho-0.0.13 (c (n "chamkho") (v "0.0.13") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)))) (h "0yi84kqq2flz7b86chkfvvabz335gckx6h798qn5w0zqnzpsfyvr")))

(define-public crate-chamkho-0.1.0 (c (n "chamkho") (v "0.1.0") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prefixtree") (r "^0.0.5") (d #t) (k 0)))) (h "0zmpysr7rvcfc81av7anygcwbwb9dmalz1arrd5ybik50jjf523z")))

(define-public crate-chamkho-0.2.0 (c (n "chamkho") (v "0.2.0") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "prefixtree") (r "^0.1.0") (d #t) (k 0)))) (h "11yk38xacc1klp1r4ynfzrq6q1s9lcwvrd6ic1g6hb4wwrz6ssd9")))

(define-public crate-chamkho-0.2.1 (c (n "chamkho") (v "0.2.1") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "prefixtree") (r "^0.1.0") (d #t) (k 0)))) (h "1jxs89a36nnk4dd0znz8w8sbn35fdic89im6di44xdm3r846g3ir")))

(define-public crate-chamkho-0.2.2 (c (n "chamkho") (v "0.2.2") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "prefixtree") (r "^0.1.2") (d #t) (k 0)))) (h "0v7wcwl4yra73d33n054qfd3sq1w4qlc5w1ai16z0jfhqp5i5zar")))

(define-public crate-chamkho-0.3.0 (c (n "chamkho") (v "0.3.0") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "wordcut-engine") (r "^0.1.1") (d #t) (k 0)))) (h "033kcqlvkmj122fpsyfvfjw9qa8bd1dcgx1l79sc6pdc317lvlg3")))

(define-public crate-chamkho-0.3.1 (c (n "chamkho") (v "0.3.1") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "wordcut-engine") (r "^0.1.1") (d #t) (k 0)))) (h "04p8pxlwqmb83vcan66cvvavalbkgphhi9420dld5aqjz7vnsqhy")))

(define-public crate-chamkho-0.4.0 (c (n "chamkho") (v "0.4.0") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "wordcut-engine") (r "^0.3.1") (d #t) (k 0)))) (h "0qgkksgcv66rflzgd584di2z1ha115dmdcn90ilx6b205n5fj9nc")))

(define-public crate-chamkho-0.5.0 (c (n "chamkho") (v "0.5.0") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "wordcut-engine") (r "^0.5.0") (d #t) (k 0)))) (h "10iwpyzsm0dcca1j4mrp6y6h71pvksq76s1a0chdzm7q9sfvq0mx")))

(define-public crate-chamkho-1.0.0 (c (n "chamkho") (v "1.0.0") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "wordcut-engine") (r "^0.5.0") (d #t) (k 0)))) (h "1g1ad122gablhysdq3gn5pniny5yi00435j8x40h38wb2mrac8xr")))

(define-public crate-chamkho-1.0.1 (c (n "chamkho") (v "1.0.1") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "wordcut-engine") (r "^0.5.0") (d #t) (k 0)))) (h "0vj5frig4xzxip0gvdf6xp4rx2kkr2q6g2y60ylc3avxqm87kgs7")))

(define-public crate-chamkho-1.0.2 (c (n "chamkho") (v "1.0.2") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "wordcut-engine") (r "^0.5.0") (d #t) (k 0)))) (h "02m39i5236wkpq9pqkcyc0zgrnmy4wdk78v6kdnzfbwclhf3p2lf")))

(define-public crate-chamkho-1.1.0 (c (n "chamkho") (v "1.1.0") (d (list (d (n "clap") (r "^1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "wordcut-engine") (r "^1.0.0") (d #t) (k 0)))) (h "1y180hjwi8xmihm01a900n3x4id66v5z8smdmns4gpnbpb28rc5s") (f (quote (("onedir"))))))

(define-public crate-chamkho-1.2.0 (c (n "chamkho") (v "1.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "wordcut-engine") (r "^1.0.0") (d #t) (k 0)))) (h "1j0sd6nw8gisc0699yqbrhx63734i57sva67481w9a4mc2689phi") (f (quote (("onedir"))))))

(define-public crate-chamkho-1.3.0 (c (n "chamkho") (v "1.3.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "wordcut-engine") (r "^1.1.3") (d #t) (k 0)))) (h "1yqzw3005ngjgrwafqjc85l4c5lcbrlmp126q73csx4x6rdj59gp") (f (quote (("onedir"))))))

(define-public crate-chamkho-1.3.1 (c (n "chamkho") (v "1.3.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "wordcut-engine") (r "^1.1.4") (d #t) (k 0)))) (h "1ngph99m41jzjkafj77apkiml29ag6d72695a97zfh1flqkk2knf") (f (quote (("onedir"))))))

(define-public crate-chamkho-1.4.0 (c (n "chamkho") (v "1.4.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "wordcut-engine") (r "^1.1.4") (d #t) (k 0)))) (h "0qw21kh6aqlkfrzc3xknqf06qcaak5g7vi32q1ifz2ni2rpwy7yv") (f (quote (("onedir"))))))

(define-public crate-chamkho-1.4.2 (c (n "chamkho") (v "1.4.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "wordcut-engine") (r "^1.1.4") (d #t) (k 0)))) (h "1qjdqsv21i3pbxnxchdnhj1zq7k62rdhy1f2kz5n3qz9js1dhbnc") (f (quote (("onedir"))))))

(define-public crate-chamkho-1.4.3 (c (n "chamkho") (v "1.4.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "wordcut-engine") (r "^1.1.5") (d #t) (k 0)))) (h "0mia2y50qlhzfcn4z32p853w21alx8pyj5jpy1v2hgcz8ldkpx15") (f (quote (("onedir"))))))

