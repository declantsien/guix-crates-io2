(define-module (crates-io ch eb chebyshev) #:use-module (crates-io))

(define-public crate-chebyshev-0.0.0 (c (n "chebyshev") (v "0.0.0") (h "0zaa0dihvwzdw9jy4h7l5jdnn39nni794sx6270533pylch8rf3z")))

(define-public crate-chebyshev-0.0.1 (c (n "chebyshev") (v "0.0.1") (h "12l1qzcyk42jxqkbzl0ivq4fvvkp9zpvshz4vr5nl32rkdj46jax")))

