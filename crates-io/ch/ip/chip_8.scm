(define-module (crates-io ch ip chip_8) #:use-module (crates-io))

(define-public crate-chip_8-0.1.0 (c (n "chip_8") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)))) (h "1f7b12pkx5l0ypwnqi7rbd8sff525alg6b3k6g58fxz19y45h1wq")))

(define-public crate-chip_8-0.2.0 (c (n "chip_8") (v "0.2.0") (d (list (d (n "bufrng") (r "^1.0.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.18") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "193658119qz39yq1yyiwkn9yflz2ghjz9drzbqfnf6y351gbjzj8")))

(define-public crate-chip_8-0.3.0 (c (n "chip_8") (v "0.3.0") (d (list (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (k 0)) (d (n "thiserror") (r "^1.0.30") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1fiys6hzd4pf32lakkmp26b2b8hli52rqqzxqghj8wznlj23hbgh") (f (quote (("std" "rand/std" "rand/std_rng" "thiserror") ("default" "std"))))))

(define-public crate-chip_8-0.3.1 (c (n "chip_8") (v "0.3.1") (d (list (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (k 0)) (d (n "thiserror") (r "^1.0.30") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1kiyrq2s7wphlaxicj754w7q3sn8xw9nj6k2jhaskf07sjpw0x11") (f (quote (("std" "rand/std" "rand/std_rng" "thiserror") ("default" "std"))))))

