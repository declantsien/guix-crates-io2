(define-module (crates-io ch ip chip_oxide) #:use-module (crates-io))

(define-public crate-chip_oxide-0.1.0 (c (n "chip_oxide") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nh7z783kg42svjscqlxqv0kcksgaxd2bf1j170cmdz86nmkiyl2")))

(define-public crate-chip_oxide-0.1.1 (c (n "chip_oxide") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16824wc00fv2i6k03ri2g0b60qsks753a0bmy0cgvr8l38hwygrq")))

