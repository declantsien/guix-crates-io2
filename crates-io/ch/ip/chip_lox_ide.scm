(define-module (crates-io ch ip chip_lox_ide) #:use-module (crates-io))

(define-public crate-chip_lox_ide-0.2.0 (c (n "chip_lox_ide") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6.0") (f (quote ("dynamic"))) (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.4.0") (d #t) (k 0)) (d (n "fraction") (r "^0.10.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rug") (r "^1.15.0") (d #t) (k 0)))) (h "0xli51phvnmkz978j5d3rpg9wazc6ij5kvans10w2afxiak37ka9")))

