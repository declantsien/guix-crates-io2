(define-module (crates-io ch ip chip_8_cpu_emulator) #:use-module (crates-io))

(define-public crate-chip_8_cpu_emulator-0.1.0 (c (n "chip_8_cpu_emulator") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0n9rx6yl5lh89ms1j75dnx91v1dg4sm1cg6zkjq650zfq6r951dh")))

