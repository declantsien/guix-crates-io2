(define-module (crates-io ch ip chip8_core) #:use-module (crates-io))

(define-public crate-chip8_core-0.1.0 (c (n "chip8_core") (v "0.1.0") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("wyrand"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (o #t) (d #t) (k 0)))) (h "0db6w3fljzq00x28g1hnp7mlqfsqcpwqfdx4jcnhzdkqvwsnhrpr") (f (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-chip8_core-0.1.1 (c (n "chip8_core") (v "0.1.1") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("wyrand"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (o #t) (d #t) (k 0)))) (h "18i18fkgxvkaf39rww743wfv97b43wld93iix6kcykqs68pi1an7") (f (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-chip8_core-0.1.2 (c (n "chip8_core") (v "0.1.2") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("wyrand"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (o #t) (d #t) (k 0)))) (h "1w8dprg4j54q8ndaak91xhdsv9agpbm3bkym098hlc1v49n6vk14") (f (quote (("std")))) (s 2) (e (quote (("wasm" "std" "dep:wasm-bindgen"))))))

(define-public crate-chip8_core-0.1.3 (c (n "chip8_core") (v "0.1.3") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("wyrand"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (o #t) (d #t) (k 0)))) (h "196nfkak3dnabbl6dag6z4rgb8dshigdaddv08dd3v9skh37rh63") (f (quote (("std")))) (s 2) (e (quote (("wasm" "std" "dep:wasm-bindgen"))))))

(define-public crate-chip8_core-0.1.4 (c (n "chip8_core") (v "0.1.4") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("wyrand"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (o #t) (d #t) (k 0)))) (h "0w3ncgkgjb39d07vb85083shblbfgljki0z6jfw4lskdldcrp5af") (f (quote (("std")))) (s 2) (e (quote (("wasm" "std" "dep:wasm-bindgen"))))))

(define-public crate-chip8_core-0.2.0 (c (n "chip8_core") (v "0.2.0") (d (list (d (n "nanorand") (r "^0.7.0") (f (quote ("wyrand"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (o #t) (d #t) (k 0)))) (h "121p7yk9j0n6h888j7f18l9szq8m5i3cmq31z4457wr0gxjz3739") (f (quote (("std")))) (s 2) (e (quote (("wasm" "std" "dep:wasm-bindgen"))))))

(define-public crate-chip8_core-0.3.0 (c (n "chip8_core") (v "0.3.0") (d (list (d (n "js-sys") (r "^0.3.65") (o #t) (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (f (quote ("wyrand"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (o #t) (d #t) (k 0)))) (h "026nkngz57njjazjsqwbhmsbwmal88w4kw0m6gscymi1vmdpfwsv") (f (quote (("std")))) (s 2) (e (quote (("wasm" "std" "dep:wasm-bindgen" "dep:js-sys"))))))

(define-public crate-chip8_core-0.4.0 (c (n "chip8_core") (v "0.4.0") (d (list (d (n "js-sys") (r "^0.3.65") (o #t) (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (f (quote ("wyrand"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tsify") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (o #t) (d #t) (k 0)))) (h "13prhxxpj7p6ms2j53bl015kkyppxkhjlk1633kkf1ls1h69par3") (f (quote (("std")))) (s 2) (e (quote (("wasm" "std" "dep:wasm-bindgen" "dep:js-sys" "dep:serde" "dep:serde-wasm-bindgen" "dep:tsify"))))))

