(define-module (crates-io ch ip chip8-rom-to-opcode) #:use-module (crates-io))

(define-public crate-chip8-rom-to-opcode-0.1.0 (c (n "chip8-rom-to-opcode") (v "0.1.0") (h "0qxdd62m278w9khbxw0y41gpi61vipxp8xckacrc6viyggsqrygk")))

(define-public crate-chip8-rom-to-opcode-0.1.1 (c (n "chip8-rom-to-opcode") (v "0.1.1") (h "1f3cpv8wajq11x4kgxkcdgs92vcpr2c3jsggy2m20v0s7crsgsn3")))

(define-public crate-chip8-rom-to-opcode-0.1.2 (c (n "chip8-rom-to-opcode") (v "0.1.2") (h "17rmjb2q2zvzi71l1gnyggxshy962kdzly990syl6lyrbnx348a7")))

(define-public crate-chip8-rom-to-opcode-0.1.3 (c (n "chip8-rom-to-opcode") (v "0.1.3") (h "0a64fr0hbrp583mnwafafnb9wz611k2g91py6gmijff2xmr7hq4g")))

