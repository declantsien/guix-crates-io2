(define-module (crates-io ch ip chiprust-emu) #:use-module (crates-io))

(define-public crate-chiprust-emu-0.1.0 (c (n "chiprust-emu") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0a9szjg5pxcly5algvgpyjzvwvpz6ql46qpgpdvzgmxmfpf1xz98")))

(define-public crate-chiprust-emu-0.2.0 (c (n "chiprust-emu") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0k1hch87ph4h4j0lv4vyb4xxf01yb8zmdvgfca4m36pk2jl4p72a")))

(define-public crate-chiprust-emu-0.2.1 (c (n "chiprust-emu") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1xzq06jsz1c5xa547zzzacfdqf0pl0sxkiqgjjbw089cnh25fi9x")))

