(define-module (crates-io ch ip chipper_player) #:use-module (crates-io))

(define-public crate-chipper_player-0.1.0 (c (n "chipper_player") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "15sm05wy08pli11n1cwswf3r0h3n47ncsqy12cvf9219497arymh") (y #t)))

(define-public crate-chipper_player-0.1.1 (c (n "chipper_player") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "19v0g82ymf2iyspw8w6h9vrvpgrigvskbzr7ak2qmmg8b58nq2as") (y #t)))

