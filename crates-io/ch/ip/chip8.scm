(define-module (crates-io ch ip chip8) #:use-module (crates-io))

(define-public crate-chip8-0.1.0 (c (n "chip8") (v "0.1.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "sdl2") (r "^0.27.0") (d #t) (k 2)))) (h "0c5hb5yy078gwkmcgvh0zrc2yyyi1bb5z1rvnawqcgv837xzfblr")))

(define-public crate-chip8-0.1.1 (c (n "chip8") (v "0.1.1") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "sdl2") (r "^0.27.0") (d #t) (k 2)))) (h "0bn04k3jsl27lwry22x9ql1lsmij4fs4vra5wazm72bx37scs4dr")))

