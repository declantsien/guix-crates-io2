(define-module (crates-io ch ip chiprust-emu-cli) #:use-module (crates-io))

(define-public crate-chiprust-emu-cli-0.1.0 (c (n "chiprust-emu-cli") (v "0.1.0") (d (list (d (n "chiprust-emu") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.9") (f (quote ("termination"))) (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)))) (h "1ny4c7izxfax618fm8qdq0grzfk3x578qxnnmd4m0zjyk81h4sk4")))

(define-public crate-chiprust-emu-cli-0.2.0 (c (n "chiprust-emu-cli") (v "0.2.0") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "chiprust-emu") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.9") (f (quote ("termination"))) (d #t) (k 0)) (d (n "device_query") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)))) (h "12sqqfmvqfn7z7jbyg47f4n47f7dap51zjpnaydjgmlhp7v2s8vb") (f (quote (("sound" "rodio") ("input" "device_query") ("default" "sound" "input"))))))

