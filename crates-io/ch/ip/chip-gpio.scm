(define-module (crates-io ch ip chip-gpio) #:use-module (crates-io))

(define-public crate-chip-gpio-0.1.0 (c (n "chip-gpio") (v "0.1.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.4") (d #t) (k 0)))) (h "1mj4m60p904xsdhym8r1672y4w3m7dza5vawkvzm2py5qy124p9g")))

(define-public crate-chip-gpio-0.1.1 (c (n "chip-gpio") (v "0.1.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5") (d #t) (k 0)))) (h "1h6vh508gm630d1c35ryavcikszflaakhnc6hz58n3a5nhxbwwgi")))

