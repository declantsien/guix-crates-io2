(define-module (crates-io ch ip chip8_vm) #:use-module (crates-io))

(define-public crate-chip8_vm-0.0.1 (c (n "chip8_vm") (v "0.0.1") (d (list (d (n "rand") (r "^0.1.2") (d #t) (k 0)))) (h "1rr94s809n0cv797x56m1k7mwaq8kspk47szqqmpwpf2g4dbr2ix")))

(define-public crate-chip8_vm-0.0.2 (c (n "chip8_vm") (v "0.0.2") (d (list (d (n "rand") (r "^0.1.2") (d #t) (k 0)))) (h "18wc8xb0gqpfxr8d4mi7c3spxvmc0vfvkwjfi6a1qf7djbn8qcg3")))

(define-public crate-chip8_vm-0.0.3 (c (n "chip8_vm") (v "0.0.3") (d (list (d (n "rand") (r "^0.1.2") (d #t) (k 0)))) (h "1bgk8f7dsw9nxvgqxc6vr08lk0ax77sa8s4nxm4j1lqc6h8ilghi")))

(define-public crate-chip8_vm-0.1.1 (c (n "chip8_vm") (v "0.1.1") (d (list (d (n "rand") (r "^0.1.2") (d #t) (k 0)))) (h "0qmc60nj542x1rvh455fvnxvgh2c5bgdayqvayhzzqc36xjldwqw")))

(define-public crate-chip8_vm-0.2.0 (c (n "chip8_vm") (v "0.2.0") (d (list (d (n "rand") (r "^0.1.2") (d #t) (k 0)))) (h "1kvha9nm4a80x35k1rig0akjfji0jkas2gn9pk1lcwb0cs5wlpl6")))

(define-public crate-chip8_vm-0.3.0 (c (n "chip8_vm") (v "0.3.0") (d (list (d (n "log") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.1.3") (d #t) (k 0)))) (h "03979yq3mq1asj28z3zi077m8fh7c3sypr4hdn834v7nrgh8xa3w")))

(define-public crate-chip8_vm-0.3.1 (c (n "chip8_vm") (v "0.3.1") (d (list (d (n "log") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.1.3") (d #t) (k 0)))) (h "1sbn5m4abdkjq8ir0p7fgjhz2zwvrnq3x39c09hjai37h1h3z2px")))

(define-public crate-chip8_vm-0.3.2 (c (n "chip8_vm") (v "0.3.2") (d (list (d (n "log") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.1.3") (d #t) (k 0)))) (h "1kg086plgyxklw2nfrfic2hjp7w3grdgjnl4l9m6wkz966l4f9m9")))

(define-public crate-chip8_vm-0.3.3 (c (n "chip8_vm") (v "0.3.3") (d (list (d (n "log") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.2.1") (d #t) (k 0)))) (h "1idwqzibgjpfc61zrrq8iapw97rwj5avq2qg85gz7xjlkb694lgy")))

(define-public crate-chip8_vm-0.3.4 (c (n "chip8_vm") (v "0.3.4") (d (list (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.3") (d #t) (k 0)))) (h "0x71k6l0a19xvd8s7lphz17maa8j9myfl3j3fq781425q5wqnhy5")))

(define-public crate-chip8_vm-0.3.5 (c (n "chip8_vm") (v "0.3.5") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.6") (d #t) (k 0)))) (h "0xz01rplvwknp2p6lfyvbxqmn0hn0488l223hi2hhxcqdz1i8awb")))

(define-public crate-chip8_vm-0.4.0 (c (n "chip8_vm") (v "0.4.0") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "02h8fpp53j7lpnbjh6pghp97485qz57wqan43znxi7bhbwfvsx78")))

