(define-module (crates-io ch ip chip8_db) #:use-module (crates-io))

(define-public crate-chip8_db-1.0.0 (c (n "chip8_db") (v "1.0.0") (d (list (d (n "base16ct") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "0s1b0jia6dfmv7fdpkklnlhsyza0vskml8k5gxrl86i2aiwmyafr") (f (quote (("extra-data")))) (y #t)))

(define-public crate-chip8_db-2.0.0 (c (n "chip8_db") (v "2.0.0") (d (list (d (n "base16ct") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "1g5nyyb3cm4v8a23ph75mn1g64bfn2bvj2vbqxq499b6bk6hnw61") (f (quote (("extra-data")))) (y #t)))

(define-public crate-chip8_db-2.1.0 (c (n "chip8_db") (v "2.1.0") (d (list (d (n "base16ct") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "0arvpd1magj3dd44dp0ysfhc4dqsh89i6fcnw2599587jjh7140z") (f (quote (("extra-data"))))))

