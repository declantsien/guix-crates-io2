(define-module (crates-io ch ip chip8-emulator-rs) #:use-module (crates-io))

(define-public crate-chip8-emulator-rs-0.1.0 (c (n "chip8-emulator-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.36.0") (d #t) (k 0)))) (h "10d5379dqzbi0hh6078bds9zq65m7508sy60lp3h5m05rvcc6awk")))

