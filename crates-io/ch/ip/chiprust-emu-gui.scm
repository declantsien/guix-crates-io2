(define-module (crates-io ch ip chiprust-emu-gui) #:use-module (crates-io))

(define-public crate-chiprust-emu-gui-0.1.0 (c (n "chiprust-emu-gui") (v "0.1.0") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "chiprust-emu") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "pixels") (r "^0.3.0") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "0gg05zvc7hhqa1698jdsv0bg6gycrsq9wh5h8sg985x5h5ygqwnx") (f (quote (("sound" "rodio"))))))

