(define-module (crates-io ch ip chipo) #:use-module (crates-io))

(define-public crate-chipo-2.0.0 (c (n "chipo") (v "2.0.0") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "js-sys") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0hpli49yrsxmgwc8vbyrj0w5df37rs1jmc4r9lk2bl9saijski8p")))

(define-public crate-chipo-2.1.0 (c (n "chipo") (v "2.1.0") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.1") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "js-sys") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0zsp1ynl78dqnsx53vgacxfi06agzp0w37pqk4jclkcr5az405r8")))

