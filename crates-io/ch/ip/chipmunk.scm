(define-module (crates-io ch ip chipmunk) #:use-module (crates-io))

(define-public crate-chipmunk-0.0.1 (c (n "chipmunk") (v "0.0.1") (d (list (d (n "chipmunk-sys") (r "0.0.*") (d #t) (k 0)))) (h "1qxhf18i1mij7jgy3ykz4vsh79q8m9p23ilx1dcyg1aal5xhmv47")))

(define-public crate-chipmunk-0.0.2 (c (n "chipmunk") (v "0.0.2") (d (list (d (n "chipmunk-sys") (r "*") (d #t) (k 0)) (d (n "void") (r "*") (d #t) (k 0)))) (h "0fi15id4q4fklydjqa7d9siq0rvm9w6pl4zhkbdf8s7nrp9f63zl")))

