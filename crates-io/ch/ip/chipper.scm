(define-module (crates-io ch ip chipper) #:use-module (crates-io))

(define-public crate-chipper-0.1.0 (c (n "chipper") (v "0.1.0") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)) (d (n "rodio") (r "^0.5.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30.0") (d #t) (k 0)))) (h "1ljcdgp8z2hdwik8wwhya3byf0skzsr8pamf7w0gkdgkd1d4cm2a")))

