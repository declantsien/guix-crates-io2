(define-module (crates-io ch ip chip-ahoyto) #:use-module (crates-io))

(define-public crate-chip-ahoyto-0.3.5 (c (n "chip-ahoyto") (v "0.3.5") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0ig2x1h7r9lxka8krfyxcx6f9xdlmb3sli0bki4kaikk6jbq3zlh") (f (quote (("wasm" "wasm-bindgen") ("quirks"))))))

(define-public crate-chip-ahoyto-0.3.6 (c (n "chip-ahoyto") (v "0.3.6") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1d5whxw1jjpjkxxp03nw8qs554y1m36569l7rf341rrmm29wg3cg") (f (quote (("wasm" "wasm-bindgen") ("quirks"))))))

(define-public crate-chip-ahoyto-0.4.1 (c (n "chip-ahoyto") (v "0.4.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1nmk5njphrl73m2q1rwmn0dlyc154l599g7f1nyi5azzasra35h6") (f (quote (("wasm" "wasm-bindgen") ("quirks"))))))

(define-public crate-chip-ahoyto-0.4.2 (c (n "chip-ahoyto") (v "0.4.2") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1m1fzcybvx8815vrhdm507p2dwkcgnpwxlg36amb8l1r78fr34an") (f (quote (("wasm" "wasm-bindgen") ("quirks"))))))

