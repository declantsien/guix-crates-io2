(define-module (crates-io ch ip chip8-rs) #:use-module (crates-io))

(define-public crate-chip8-rs-0.1.0 (c (n "chip8-rs") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (d #t) (k 0)))) (h "12jf76iyx6jyd31ab3vz9pyldpd95axbr9s5iz67s9hyfmblcmb3") (y #t)))

(define-public crate-chip8-rs-0.1.1 (c (n "chip8-rs") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (d #t) (k 0)))) (h "1q20r4fiw1jsamk6s9spw374i3q6x6f03aqmznr74lzxyrp3wa3m")))

