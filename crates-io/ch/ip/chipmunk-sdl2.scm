(define-module (crates-io ch ip chipmunk-sdl2) #:use-module (crates-io))

(define-public crate-chipmunk-sdl2-1.0.0 (c (n "chipmunk-sdl2") (v "1.0.0") (d (list (d (n "chipmunk-backend") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "0n8rgn2x5aq6dhqr77w8ymangm0h3d9d7zkxyp8q2vlicg9zsb78")))

