(define-module (crates-io ch ip chip8_base) #:use-module (crates-io))

(define-public crate-chip8_base-0.2.0 (c (n "chip8_base") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cpal") (r "^0.14") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.11") (d #t) (k 0)))) (h "04hmm5fgq403w6viq6zaf3s7zgqa2qlld01a0nf8c8vdiyj9m5ry")))

