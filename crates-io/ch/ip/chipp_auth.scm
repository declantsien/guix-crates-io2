(define-module (crates-io ch ip chipp_auth) #:use-module (crates-io))

(define-public crate-chipp_auth-2.1.0 (c (n "chipp_auth") (v "2.1.0") (d (list (d (n "core-foundation") (r "^0.9.4") (d #t) (k 0)) (d (n "rpassword") (r "^7.3") (d #t) (k 0)) (d (n "security-framework-sys") (r "^2.9") (k 0)))) (h "1pfvhihmjm0iraa5yi16cvwmji85fcakyid3za5alpj5sskcfsr1")))

