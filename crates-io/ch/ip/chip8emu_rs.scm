(define-module (crates-io ch ip chip8emu_rs) #:use-module (crates-io))

(define-public crate-chip8emu_rs-0.1.0 (c (n "chip8emu_rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "0ii46vyczd5gqskmcmyh7mw4viclrc6rqf1wljkf6b2gzx5d050q") (y #t)))

(define-public crate-chip8emu_rs-0.1.1 (c (n "chip8emu_rs") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "06al9a7fvz1081bl3d479b7fbxwvn3s9fdy1y1scxw878kl9kjql")))

