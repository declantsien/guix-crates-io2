(define-module (crates-io ch ip chippy) #:use-module (crates-io))

(define-public crate-chippy-0.1.0 (c (n "chippy") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "0h17pzkgj7drkzqyd43p5w2l1gxq8yjghfz5bb3bby84yvfg5fyd")))

