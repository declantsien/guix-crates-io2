(define-module (crates-io ch ip chiploxide) #:use-module (crates-io))

(define-public crate-chiploxide-0.2.0 (c (n "chiploxide") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6.0") (f (quote ("dynamic"))) (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.4.0") (d #t) (k 0)) (d (n "fraction") (r "^0.10.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rug") (r "^1.15.0") (d #t) (k 0)))) (h "07ghzhvl6qjbnc89qj1zvmgb4hvyf96p4kinjp3f01m4bcg98qik")))

