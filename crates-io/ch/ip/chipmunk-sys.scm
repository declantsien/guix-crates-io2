(define-module (crates-io ch ip chipmunk-sys) #:use-module (crates-io))

(define-public crate-chipmunk-sys-0.0.1 (c (n "chipmunk-sys") (v "0.0.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "0brzlv27yf4vh5fh0gmj3009ppni7v33ahns4351qxaygsd2jadk")))

(define-public crate-chipmunk-sys-0.0.2 (c (n "chipmunk-sys") (v "0.0.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "09bkszfpz78kjyqrzdh6l87n95j96lqg8g4wpsnvfmf1cy1jlgji")))

(define-public crate-chipmunk-sys-0.0.3 (c (n "chipmunk-sys") (v "0.0.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0w31wc6wssq2s5cv8nzlcbsg5jnac33x2g7y0rv2xczlrfvf3a3n")))

(define-public crate-chipmunk-sys-0.0.4 (c (n "chipmunk-sys") (v "0.0.4") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0f7wck2w9wj38z78sz2p3101hs3hlibkq50j78cn1rdy4kp54zia")))

