(define-module (crates-io ch ip chip) #:use-module (crates-io))

(define-public crate-chip-0.0.1 (c (n "chip") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.16.4") (d #t) (k 0)))) (h "0cnr1vb6bsy4mjc09ybhkpw5w0iyjq0230c1b08gzhflmygbdaa0") (f (quote (("strict"))))))

(define-public crate-chip-0.0.2 (c (n "chip") (v "0.0.2") (d (list (d (n "nalgebra") (r "^0.16.4") (d #t) (k 0)))) (h "1gljzvyln3slk6zsbwc2iafqb4fhp62ysi8jy82s20n0xc04zg23") (f (quote (("strict"))))))

(define-public crate-chip-0.0.3 (c (n "chip") (v "0.0.3") (d (list (d (n "nalgebra") (r "^0.16.4") (d #t) (k 0)))) (h "1k5j7mnw7p8k7f51zka633rlh0lxgay2y67g2zf3lms2bbw8lrfj") (f (quote (("strict"))))))

(define-public crate-chip-0.0.4 (c (n "chip") (v "0.0.4") (d (list (d (n "nalgebra") (r "^0.16.4") (d #t) (k 0)))) (h "15g0ylrrv8r75ipw5a3j6h3xqgmr6li2ilf2qz7p4m39ymdaf08f") (f (quote (("strict"))))))

(define-public crate-chip-0.0.5 (c (n "chip") (v "0.0.5") (d (list (d (n "nalgebra") (r "^0.16.4") (d #t) (k 0)))) (h "1ak347qi3l64g61v2m5f6r7hilfsx3rhff7n9z5cqajs4sf4xbgg") (f (quote (("strict"))))))

(define-public crate-chip-0.0.6 (c (n "chip") (v "0.0.6") (d (list (d (n "nalgebra") (r "^0.18.0") (d #t) (k 0)))) (h "0jwdppn1shphsvzpvx5i13v9s75bd1xj051nkkffpwsfqw954i54") (f (quote (("strict"))))))

