(define-module (crates-io ch ip chipmunk-rs) #:use-module (crates-io))

(define-public crate-chipmunk-rs-0.1.0 (c (n "chipmunk-rs") (v "0.1.0") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "09mm71dcgwmby3vsk1wf124kbaak8ky9yr18cs82q97ksc155x4a")))

(define-public crate-chipmunk-rs-0.1.1 (c (n "chipmunk-rs") (v "0.1.1") (d (list (d (n "cmake") (r "0.1.*") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1s5l0blpbkl3ng6xrzkhvq8n1q0w1hwzv4rlhr9i2pr5hj1qbcdg")))

