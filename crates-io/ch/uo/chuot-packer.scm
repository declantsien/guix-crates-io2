(define-module (crates-io ch uo chuot-packer) #:use-module (crates-io))

(define-public crate-chuot-packer-0.1.0 (c (n "chuot-packer") (v "0.1.0") (d (list (d (n "glam") (r "^0.27") (f (quote ("fast-math"))) (d #t) (k 0)) (d (n "glamour") (r "^0.11.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "19x27s7ybya5v7q7794w2mhkrggvj3i1wv5hpjy4idj50f2s1fw4") (r "1.77.0")))

(define-public crate-chuot-packer-0.1.1 (c (n "chuot-packer") (v "0.1.1") (d (list (d (n "glam") (r "^0.27") (f (quote ("fast-math"))) (d #t) (k 0)) (d (n "glamour") (r "^0.11.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1zdms8axzv30361d9grar3944aybgjn93iy0scr9n1zda00pr6h4") (r "1.77.0")))

