(define-module (crates-io ch ir chiropterm) #:use-module (crates-io))

(define-public crate-chiropterm-0.1.0 (c (n "chiropterm") (v "0.1.0") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)))) (h "0fvjf6am2x4z468hv83f22ixlcl0qx62kg93aa64dmf6izax94jh")))

(define-public crate-chiropterm-0.1.1 (c (n "chiropterm") (v "0.1.1") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)))) (h "0pfa091231zafrbdqkmdnqzkjkx94nllskvc9bqpscbgvkhnfxxf")))

(define-public crate-chiropterm-0.1.2 (c (n "chiropterm") (v "0.1.2") (d (list (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)))) (h "1xq02a7hp2l7flj6hm99b3w8gr4akvz5am9v9szk560lj729r8rj")))

(define-public crate-chiropterm-0.2.0 (c (n "chiropterm") (v "0.2.0") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)))) (h "1vb03sm2hsj8v5ciwf1zk87mbqr0ya5f3lhcva7hq7x4xbibl4nb")))

