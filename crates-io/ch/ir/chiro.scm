(define-module (crates-io ch ir chiro) #:use-module (crates-io))

(define-public crate-chiro-0.0.1 (c (n "chiro") (v "0.0.1") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)))) (h "05wbwy6613kw7w39q8qjmxxbdmfn71cs32iha825q5fgs2h8fw1w")))

(define-public crate-chiro-0.0.2 (c (n "chiro") (v "0.0.2") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)))) (h "1zfw2a18j0ij2c2aiv27pky8988llm6dr7vrp4z1ridsic0pqlhh")))

(define-public crate-chiro-0.0.3 (c (n "chiro") (v "0.0.3") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)))) (h "1x9l7ajrvmv5h663zky1iz9sag3lxz61chcxavyhckga645cd1cx")))

(define-public crate-chiro-0.0.4 (c (n "chiro") (v "0.0.4") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)))) (h "1n68i9haglibqvlx1fq5hf1cfvhybq9s392a0jakxxpwp1v2faf8")))

(define-public crate-chiro-0.0.5 (c (n "chiro") (v "0.0.5") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)))) (h "1anf1lffq5ag3wnqs3c1hm2w6bvaa8qyvblkhr0fpk5dsxq2jzay")))

(define-public crate-chiro-0.0.6 (c (n "chiro") (v "0.0.6") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "0aakc12d3p0d8i1dq6gc79w5a1qjy53hfyl52ikqz4ay7aqvdbhb")))

(define-public crate-chiro-0.0.8 (c (n "chiro") (v "0.0.8") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "0v9fxfdhqwnx2gb51cavwlgadv9hkw47q9rs7zlqn34426r0jh3y")))

(define-public crate-chiro-0.0.9 (c (n "chiro") (v "0.0.9") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "1wvrcjygyziy2gc9zm3z0im12q3gfyhj32xy6fcgq6f8yx2lm4k3")))

(define-public crate-chiro-0.0.10 (c (n "chiro") (v "0.0.10") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "02z3vaxmjb9zpr3snk8li4zsqylzx5dl7w1ffkm6v307i7gasp94")))

(define-public crate-chiro-0.0.11 (c (n "chiro") (v "0.0.11") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "1m6y3cqv2p4c8phsg5mmg9snhklwj87pjdwvgy936sqhqzws7gf8")))

(define-public crate-chiro-0.0.12 (c (n "chiro") (v "0.0.12") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "17kv0jh3rnklny3df1hljiqa5nqq14ip65vd1wvq480si2z432f9")))

(define-public crate-chiro-0.0.13 (c (n "chiro") (v "0.0.13") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "10l1q29lkfv9hcd21g3v39hwbikn0p4cfl4p4zk84wj40adv9mi6")))

(define-public crate-chiro-0.0.14 (c (n "chiro") (v "0.0.14") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "0d1c4jwrnsjkkzi2hb9h1k04sqd6ada0hv8lv3c43ahi4qsnf0qm")))

(define-public crate-chiro-0.0.15 (c (n "chiro") (v "0.0.15") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "1q0n0sqyxm4vv2fddsnxpf5di02ky39l5infxgam5fzf7cryj7db")))

(define-public crate-chiro-0.0.16 (c (n "chiro") (v "0.0.16") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "0qilwcsl0kq1bv9rsb4i6ahyysd9x8s74g4dh0529m1afn5x3z8g")))

(define-public crate-chiro-0.0.17 (c (n "chiro") (v "0.0.17") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "003p2jfrav29pvm9h8v5vbhhdjspbfazicrfjqgfizsxqdqya0xf")))

(define-public crate-chiro-0.0.18 (c (n "chiro") (v "0.0.18") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "1j8k4cfd8d4hsmz24qprl4lc0bfrzb6vdc3fasdj6qfh57ljllj8")))

(define-public crate-chiro-0.0.19 (c (n "chiro") (v "0.0.19") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "1kxb263f8j54mlx6d5d72m1bq1jmgk2wqckl5g3vm7lhfbr18cnq")))

(define-public crate-chiro-0.1.0 (c (n "chiro") (v "0.1.0") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "1lj0wcyzqpn0xpdcsvzchv8a19gb1vjh678qv031bzdpk2rhqk03")))

(define-public crate-chiro-0.1.1 (c (n "chiro") (v "0.1.1") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "1xzyk6n40nnsj4p644zlzl8q7s4d9jflp8riddx5pwf969mkxy2f")))

(define-public crate-chiro-0.1.2 (c (n "chiro") (v "0.1.2") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "00dajszzlsqyl4hhlgqjd1srcashxw98g13qb73v5spp29vch7k6")))

(define-public crate-chiro-0.1.3 (c (n "chiro") (v "0.1.3") (d (list (d (n "enum-map") (r "^1.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "minifb") (r "^0.19.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "1vhphnn6y9x0zbaqsfi311if7g2ran384z5hh9vly34y3ydwqp25")))

