(define-module (crates-io ch ir chiral-computing-unit) #:use-module (crates-io))

(define-public crate-chiral-computing-unit-0.1.0 (c (n "chiral-computing-unit") (v "0.1.0") (d (list (d (n "chiral-common") (r "^0.1.0") (d #t) (k 0)) (d (n "chiral-data") (r "^0.1.0") (d #t) (k 0)) (d (n "chiral-operator") (r "^0.1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "openbabel") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "197jgxqg18wdlpjzv6c4vzgjqb85hfhj9c1bwc7v2dc5b4v34w8f")))

(define-public crate-chiral-computing-unit-0.1.2 (c (n "chiral-computing-unit") (v "0.1.2") (d (list (d (n "chiral-common") (r "^0.1.2") (d #t) (k 0)) (d (n "chiral-data") (r "^0.1.2") (d #t) (k 0)) (d (n "chiral-operator") (r "^0.1.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "openbabel") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wkw81y71yzybywvib291s67nbh26ck42p8dm4kkq6a7n7pnlmpg")))

