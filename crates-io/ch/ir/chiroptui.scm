(define-module (crates-io ch ir chiroptui) #:use-module (crates-io))

(define-public crate-chiroptui-0.1.0 (c (n "chiroptui") (v "0.1.0") (d (list (d (n "chiropterm") (r "^0.2.0") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)))) (h "1wq95w901pbkp5x9nszv36xwdacxgjq4kycg97rc4va1j5diqfhp")))

