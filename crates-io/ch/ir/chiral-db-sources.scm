(define-module (crates-io ch ir chiral-db-sources) #:use-module (crates-io))

(define-public crate-chiral-db-sources-0.1.0 (c (n "chiral-db-sources") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jlpfid17gfq6sl1sdglcaj4jldvdn3c7spfb91ldxg5vrm9a47l")))

(define-public crate-chiral-db-sources-0.2.0 (c (n "chiral-db-sources") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zbn227s6djbwbi23mbbr0rlgmi7ziqmi2zlfmxf8bg9y2fy0nzl")))

