(define-module (crates-io ch ir chiral-operator) #:use-module (crates-io))

(define-public crate-chiral-operator-0.1.0 (c (n "chiral-operator") (v "0.1.0") (d (list (d (n "chiral-common") (r "^0.1.0") (d #t) (k 0)) (d (n "chiral-data") (r "^0.1.0") (d #t) (k 0)) (d (n "openbabel") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ldnzbkfhrmkk667k0908a1dhm8pa4vabrpz9f0llivm8zkj52vq")))

(define-public crate-chiral-operator-0.1.1 (c (n "chiral-operator") (v "0.1.1") (d (list (d (n "chiral-common") (r "^0.1.1") (d #t) (k 0)) (d (n "chiral-data") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rw9shvyggyxchr1z9675rmmrkrp21d3p3nyiklj0h7yxg3dykdp")))

(define-public crate-chiral-operator-0.1.2 (c (n "chiral-operator") (v "0.1.2") (d (list (d (n "chiral-common") (r "^0.1.2") (d #t) (k 0)) (d (n "chiral-data") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02c6mmppdcnhxg7mccqajwch2bddkd5y34bpvds2xh1a00mi9731")))

