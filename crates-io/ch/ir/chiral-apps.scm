(define-module (crates-io ch ir chiral-apps) #:use-module (crates-io))

(define-public crate-chiral-apps-0.1.3 (c (n "chiral-apps") (v "0.1.3") (d (list (d (n "chiral-common") (r "^0.1.3") (d #t) (k 0)) (d (n "chiral-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "openbabel") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "19i5q7jf5i38jd7n596m3yn4rcjzd81n3kf1ys9svzaqsb1d44x2")))

