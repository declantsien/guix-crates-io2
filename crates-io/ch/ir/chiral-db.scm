(define-module (crates-io ch ir chiral-db) #:use-module (crates-io))

(define-public crate-chiral-db-0.1.0 (c (n "chiral-db") (v "0.1.0") (d (list (d (n "chiral-db-sources") (r "^0.1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "openbabel") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "18vh7pa0wnk9dfyp6yq210g24nridl9vrgpwd2d9s43sx5v7pd05")))

(define-public crate-chiral-db-0.2.0 (c (n "chiral-db") (v "0.2.0") (d (list (d (n "chiral_db_sources") (r "^0.2.0") (d #t) (k 0) (p "chiral-db-sources")) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "openbabel") (r "^0.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "024n8ky7gcn9inal3aryl33v7hpgg36riiy6wzbp2yzzpl5pm5qg")))

(define-public crate-chiral-db-0.2.1 (c (n "chiral-db") (v "0.2.1") (d (list (d (n "chiral_db_sources") (r "^0.2.0") (d #t) (k 0) (p "chiral-db-sources")) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "openbabel") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "008am98anlzrvj3m38r7gwmcyr77nvpk6029jmmpbjy8i62b8ix5")))

