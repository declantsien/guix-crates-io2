(define-module (crates-io ch ir chiral-db-grpc-client-rust) #:use-module (crates-io))

(define-public crate-chiral-db-grpc-client-rust-0.2.0 (c (n "chiral-db-grpc-client-rust") (v "0.2.0") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt-multi-thread" "macros" "sync" "time"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7") (d #t) (k 1)))) (h "16bmylc5b2kj6lqib4zvnp4xwpdass182q5bl9na44pby1qlz4z8")))

