(define-module (crates-io ch ir chirpycritter) #:use-module (crates-io))

(define-public crate-chirpycritter-0.1.0 (c (n "chirpycritter") (v "0.1.0") (d (list (d (n "cpal") (r "^0.13.4") (d #t) (k 0)) (d (n "midly") (r "^0.5.2") (d #t) (k 0)))) (h "0pf4dpjfsnwh8sid2kd47p29y1cz0dvj4rrs3x1v3ajikp1cnsij")))

