(define-module (crates-io ch ir chiral-db-fp-kind) #:use-module (crates-io))

(define-public crate-chiral-db-fp-kind-0.1.0 (c (n "chiral-db-fp-kind") (v "0.1.0") (h "0kcq0rkbj2gm5kg6p4ym0g03adcg615kmx36kd1wgxhfq15vhvgx")))

(define-public crate-chiral-db-fp-kind-0.1.1 (c (n "chiral-db-fp-kind") (v "0.1.1") (h "1sacarxlz8f2rbnwqp8lq1fkzm6k5iaz8wbsm33ik7yzjr7rv9iq")))

