(define-module (crates-io ch ir chiral-derive) #:use-module (crates-io))

(define-public crate-chiral-derive-0.1.3 (c (n "chiral-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0987l3vxl00vi47grdwxkkk8bp3gyns2n888c425kgpmwg0v2wnb")))

(define-public crate-chiral-derive-0.1.4 (c (n "chiral-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qfyxhmz9p1kb7ydqfjq2hgbhkdi2qcwy36wd8cqzxxqld3iqras")))

