(define-module (crates-io ch ir chiral-data) #:use-module (crates-io))

(define-public crate-chiral-data-0.1.0 (c (n "chiral-data") (v "0.1.0") (d (list (d (n "chiral-common") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fjvmvhvddp5mmbrl6rd5x2p3lwl2crvpyy605ac4azjq64rvz1b")))

(define-public crate-chiral-data-0.1.1 (c (n "chiral-data") (v "0.1.1") (d (list (d (n "chiral-common") (r "^0.1.1") (d #t) (k 0)) (d (n "permutation") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11fnn18917g2zx580xyvxg2vsf0amp39giygn47k2gx84j4syrcs")))

(define-public crate-chiral-data-0.1.2 (c (n "chiral-data") (v "0.1.2") (d (list (d (n "chiral-common") (r "^0.1.2") (d #t) (k 0)) (d (n "permutation") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lk150lz0spj77ik2mn91sk02m7afnd7pq3kgydac6171p72js0v")))

