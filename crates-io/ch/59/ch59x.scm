(define-module (crates-io ch #{59}# ch59x) #:use-module (crates-io))

(define-public crate-ch59x-0.1.7 (c (n "ch59x") (v "0.1.7") (d (list (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0ffyllkdqhm2cwwiv2za9jqg8vc6mvkpl3fy2yvf6i9jx8imypig") (f (quote (("rt") ("default") ("ch59x"))))))

(define-public crate-ch59x-0.1.8 (c (n "ch59x") (v "0.1.8") (d (list (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1xbcpk070637gcfvyzradmrh89313pkzzyqn4iai75kpzgmi6rz3") (f (quote (("rt") ("default") ("ch59x"))))))

