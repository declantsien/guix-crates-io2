(define-module (crates-io ch vt chvt) #:use-module (crates-io))

(define-public crate-chvt-0.1.0 (c (n "chvt") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "1pzxlz1jrasnbakagf4amq6655mr5mdmnfd6hgglk7afljxxzyb1")))

(define-public crate-chvt-0.1.1 (c (n "chvt") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0d8ycghgfdx08fwa0fc7l69syqqnlvv2fmhg7pxzpszf07mca73g")))

(define-public crate-chvt-0.1.2 (c (n "chvt") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "15xn5a14s4kazpvfby5jjp39iqzvimfq4hzyb3sb922bf60kfgng")))

(define-public crate-chvt-0.1.3 (c (n "chvt") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "16fc6rra1xc0wrq5jm0hcwfllfxsz4ih5jsif24nqmzbh00x7yn8")))

(define-public crate-chvt-0.2.0 (c (n "chvt") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)))) (h "1vs8n6vlgvrb106ahvlm1y8fd1ffz79ksyq410dcrgjah3awjpc8")))

