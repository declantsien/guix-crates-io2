(define-module (crates-io ch el chell) #:use-module (crates-io))

(define-public crate-chell-0.1.0 (c (n "chell") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (d #t) (k 0)))) (h "0xfm6m6cplhasg50m9bq6kfg2i3sl8s1brm8qk12di322mgxhm1g")))

