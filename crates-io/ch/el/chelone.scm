(define-module (crates-io ch el chelone) #:use-module (crates-io))

(define-public crate-chelone-0.1.1 (c (n "chelone") (v "0.1.1") (d (list (d (n "pest") (r "^1") (d #t) (k 0)) (d (n "pest_derive") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0i8893d8criaqyqim8hsjl4p59d8jl5jzarmqy3nkwnri18xj14a")))

(define-public crate-chelone-0.1.2 (c (n "chelone") (v "0.1.2") (d (list (d (n "pest") (r "^1") (d #t) (k 0)) (d (n "pest_derive") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0z4wiz0w240ibk01iycfsn0sqnf64r3z5dgvmnp8dyvd9j9vrdsx")))

(define-public crate-chelone-0.1.3 (c (n "chelone") (v "0.1.3") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "pest") (r "^1") (d #t) (k 0)) (d (n "pest_derive") (r "^1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "unwrap_to") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1x5vphha8psaqpapcqgps4iv7m9pnmdcxa8sbpdxsd2jsrh42hc7")))

