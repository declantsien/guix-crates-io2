(define-module (crates-io ch ru chrustoph) #:use-module (crates-io))

(define-public crate-chrustoph-1.0.0 (c (n "chrustoph") (v "1.0.0") (h "0h49xmldzzbim8afbjvz19x7bhc31cv0i33l8zhhd8r4ka87whm0")))

(define-public crate-chrustoph-1.0.1 (c (n "chrustoph") (v "1.0.1") (h "1qf7hbrkd1m3nvm7hwpxs50l4w0jl77va7ydqg5rvn3mp7igks7f")))

