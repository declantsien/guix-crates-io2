(define-module (crates-io ch ik chikbip158) #:use-module (crates-io))

(define-public crate-chikbip158-1.5.1 (c (n "chikbip158") (v "1.5.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 2)))) (h "1sp48yls65kkaxxhwc04vdb8ksajxmfjvzkj01r713jp3j5cj5bp") (l "chikbip158")))

