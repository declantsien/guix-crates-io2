(define-module (crates-io ch ik chik_streamable_macro) #:use-module (crates-io))

(define-public crate-chik_streamable_macro-0.2.4 (c (n "chik_streamable_macro") (v "0.2.4") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1262l5qsgwkaxgr20bnd1zjgrf3kl855pi1bmvf4dn91hdxvpfzc")))

(define-public crate-chik_streamable_macro-0.2.12 (c (n "chik_streamable_macro") (v "0.2.12") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1s2qblpw5smyfbdlf5m3qs9g3s9vd51wyf6fj8mcfxnj42h2pl0l")))

(define-public crate-chik_streamable_macro-0.2.14 (c (n "chik_streamable_macro") (v "0.2.14") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "06zpnlp91fk01f0b8s54ywadis3ynj0qcgg0jj045anrhdn1lps9")))

(define-public crate-chik_streamable_macro-0.3.0 (c (n "chik_streamable_macro") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "17nj7cgmdjzhkx8zdpdk6dyyd7p0bxplsidnhqpr310c8d54f5jb")))

(define-public crate-chik_streamable_macro-0.6.0 (c (n "chik_streamable_macro") (v "0.6.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "11prvwdj3qygmdlidb0gmk5vgc345nz385430lchvmik3md0z88n")))

(define-public crate-chik_streamable_macro-0.8.0 (c (n "chik_streamable_macro") (v "0.8.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0kl008rppps35lpjksqxvf39fli3jqlfchpm14zzx3w6s41mf7bc")))

