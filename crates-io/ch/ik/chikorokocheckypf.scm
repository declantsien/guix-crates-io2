(define-module (crates-io ch ik chikorokocheckypf) #:use-module (crates-io))

(define-public crate-chikorokoCheckypf-0.1.0 (c (n "chikorokoCheckypf") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "nipper") (r "^0.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lsr61dh2d6kd7qin6xsr6k44x2znnydn6wwcdgs823nx2a0xnmy")))

