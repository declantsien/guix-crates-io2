(define-module (crates-io ch ik chik_py_streamable_macro) #:use-module (crates-io))

(define-public crate-chik_py_streamable_macro-0.1.4 (c (n "chik_py_streamable_macro") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)))) (h "10cy4vmnghnzfsw2fki7hvnnw26jinzl8py4fvzlmwnxl8lx1dh8")))

(define-public crate-chik_py_streamable_macro-0.2.13 (c (n "chik_py_streamable_macro") (v "0.2.13") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "1xh5a00kf2d7y00gqcl39x0rgb839vzwpjc3lh0w6mfz1ax0cq56")))

(define-public crate-chik_py_streamable_macro-0.2.14 (c (n "chik_py_streamable_macro") (v "0.2.14") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "0hr7s6rkz3yx62filw3j8g4192f071zs83c7n5jfw6k9x5hmd5ad")))

(define-public crate-chik_py_streamable_macro-0.3.0 (c (n "chik_py_streamable_macro") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "02vr7j42kzyiyqxp1packmmzgiizna0384jnsgh1ds2cv7w5gmd5")))

(define-public crate-chik_py_streamable_macro-0.5.0 (c (n "chik_py_streamable_macro") (v "0.5.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "164j88yxyjhb8gls19qk6k2x9dlqnnyhp9l0yqncg5z5f014mp3l")))

(define-public crate-chik_py_streamable_macro-0.5.1 (c (n "chik_py_streamable_macro") (v "0.5.1") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "1w8k8gfa27vacgqgq7922ziajpfj35sdc4qd3f3ir7wl4xjdfz83")))

(define-public crate-chik_py_streamable_macro-0.6.0 (c (n "chik_py_streamable_macro") (v "0.6.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "1nza5x64nysc9jmwpcgbxp7a1xvfg1p8l6ay00i0k8q6181psw0h")))

(define-public crate-chik_py_streamable_macro-0.7.0 (c (n "chik_py_streamable_macro") (v "0.7.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "1wmkhfhv6fgx194a23328jgyr5jgdb0zvwfy85ybkg77sl935fpm")))

