(define-module (crates-io ch db chdb-bindings-rs) #:use-module (crates-io))

(define-public crate-chdb-bindings-rs-0.1.0 (c (n "chdb-bindings-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.6") (d #t) (k 1)))) (h "0qlcdgblj5qnwmq1gjvc99vwin5qkf7fcgc8vj3p1db1bydh80sc") (y #t)))

(define-public crate-chdb-bindings-rs-0.1.1 (c (n "chdb-bindings-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.6") (d #t) (k 1)))) (h "1kb60if3mdkmvvbpmbx5f82fccghn8k4haichdxvidmqkrjfmjk1") (y #t)))

(define-public crate-chdb-bindings-rs-0.1.2 (c (n "chdb-bindings-rs") (v "0.1.2") (h "1x49b62ihwl4l48nyjnjfy4lqz0kia56g1haadvlgp00wz6h0c2b") (y #t)))

(define-public crate-chdb-bindings-rs-0.1.4 (c (n "chdb-bindings-rs") (v "0.1.4") (h "0xkwjmh6742zjwfl21gzqfbkwy8inglcw8z9wbnbdpglhmvkg7vd") (y #t)))

(define-public crate-chdb-bindings-rs-0.1.5 (c (n "chdb-bindings-rs") (v "0.1.5") (h "18lzr8k05za8q50b7gr60bfpc9c8g94yr4gxijr52zxa010dnmcq")))

(define-public crate-chdb-bindings-rs-0.1.6 (c (n "chdb-bindings-rs") (v "0.1.6") (h "0l4nj0qkys5w7g67a9dvrw7n17habbwcclhsm7zirzrzdmmlnzww")))

