(define-module (crates-io ch db chdb) #:use-module (crates-io))

(define-public crate-chdb-0.1.0 (c (n "chdb") (v "0.1.0") (d (list (d (n "chdb-bindings-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "enum2str") (r "^0.1.9") (d #t) (k 0)))) (h "16z8b2ac054iirqhncpp71wkaxnc1igqi9q956qjy3nm9889hxiq")))

(define-public crate-chdb-0.1.1 (c (n "chdb") (v "0.1.1") (d (list (d (n "chdb-bindings-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "enum2str") (r "^0.1.9") (d #t) (k 0)))) (h "0crfnns7ly054cc9vpaf9lba3k80gsr6zk6vlz07z7q4q5hyb9z7")))

(define-public crate-chdb-0.1.2 (c (n "chdb") (v "0.1.2") (d (list (d (n "chdb-bindings-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "enum2str") (r "^0.1.9") (d #t) (k 0)))) (h "08knggaph6vjcmmrn42p89y42psf3g79nww40mm1rssaslzjlpdn")))

