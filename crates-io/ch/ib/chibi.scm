(define-module (crates-io ch ib chibi) #:use-module (crates-io))

(define-public crate-chibi-0.1.0 (c (n "chibi") (v "0.1.0") (d (list (d (n "peg") (r "^0.8.0") (d #t) (k 0)) (d (n "postcard") (r "^0.7.3") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "18x403rhrhqx9gv61i885nn4w62xx6qipq19yvmysfbl3kc9a39p") (f (quote (("trace" "peg/trace"))))))

