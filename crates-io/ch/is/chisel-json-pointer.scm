(define-module (crates-io ch is chisel-json-pointer) #:use-module (crates-io))

(define-public crate-chisel-json-pointer-1.0.8 (c (n "chisel-json-pointer") (v "1.0.8") (h "03si80xm5v0vm5300jjk90b22zyxqf3dl76a32yxn1swc0nb9x6s") (r "1.56")))

(define-public crate-chisel-json-pointer-1.1.0 (c (n "chisel-json-pointer") (v "1.1.0") (h "0gij6dv3jvcykgzryjcs839l2x21mnx22a0r939npb9s4619z9an") (r "1.56")))

