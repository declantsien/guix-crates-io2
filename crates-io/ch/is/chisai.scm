(define-module (crates-io ch is chisai) #:use-module (crates-io))

(define-public crate-chisai-0.1.0 (c (n "chisai") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)))) (h "154xca3irzrshl7r0qbhiv67m0ffqn9a617qvzk2zp23hmyn9zhh") (y #t)))

(define-public crate-chisai-0.1.1 (c (n "chisai") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)))) (h "07x5zzhwng7l3r3bwcsd5978hgp3wmpx09i9yz2lnv9ahraixwpj")))

(define-public crate-chisai-0.1.2 (c (n "chisai") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "1y3g45nf67fbn5vqfvx3z3q8va2kzxwm6lrw7dkvfif1n24i4yxd")))

(define-public crate-chisai-0.1.3 (c (n "chisai") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "0x73md66yxqnj39vn3nwayx8sfvi38x44kn75k8shrfnfrpvd4yp")))

(define-public crate-chisai-0.2.0 (c (n "chisai") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "1lx494vl0i2wvc3f4cmbnp8naahmljipckh24vw62p5yk94apwfm")))

(define-public crate-chisai-0.2.1 (c (n "chisai") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "0ah1dfqw8xrq9c2xxcwby1fpkyhlxgbnv2c9cmh4fmczzdcj0c4f")))

(define-public crate-chisai-0.2.2 (c (n "chisai") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "04v3l208amk5xl009dcbhfnbivfjjk1lkdxc9pwl5zm7f3zs71cn")))

