(define-module (crates-io ch is chiselj) #:use-module (crates-io))

(define-public crate-chiselj-0.1.2 (c (n "chiselj") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chisel-json") (r "^0.1.20") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "0xqcqpph0d9ljls08l5rnhi7vvzyykxqb9hgan39i0fp6mfgvifg") (f (quote (("default" "crossterm") ("crossterm")))) (r "1.56")))

(define-public crate-chiselj-0.1.3 (c (n "chiselj") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chisel-json") (r "^0.1.22") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "1vgjzcxkr2cv5l8slnfn60saps2l4i1yjqzvbz4m4cws4rf1xqrl") (f (quote (("default" "crossterm") ("crossterm")))) (r "1.56")))

