(define-module (crates-io ch is chisel-charstream) #:use-module (crates-io))

(define-public crate-chisel-charstream-0.1.0 (c (n "chisel-charstream") (v "0.1.0") (h "160wqsi7351yydhpavq8adclqz2i9mj04rs2sk62pll2vqq4wp17") (y #t) (r "1.56")))

(define-public crate-chisel-charstream-0.1.1 (c (n "chisel-charstream") (v "0.1.1") (h "02kf66p57dgd9476cjzr7q66rf9j4d2lrfrmw650i14m5vil85dd") (y #t) (r "1.56")))

(define-public crate-chisel-charstream-0.1.2 (c (n "chisel-charstream") (v "0.1.2") (h "0lqzzvs90xnzhwjfz4f6s2vkvnjdij9qgmkc0y0cbqv8jzqdhg91") (y #t) (r "1.56")))

(define-public crate-chisel-charstream-0.1.3 (c (n "chisel-charstream") (v "0.1.3") (h "0fd7q13rx0h5i16bqnn3a22zb7vd2fd1zvmjppj8qrgy3glrhcfc") (y #t) (r "1.56")))

