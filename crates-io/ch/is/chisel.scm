(define-module (crates-io ch is chisel) #:use-module (crates-io))

(define-public crate-chisel-0.1.0 (c (n "chisel") (v "0.1.0") (d (list (d (n "libchisel") (r "^0.1.0") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.21") (d #t) (k 0)))) (h "0xhx2fx00mjkpnada3wkq1fivmwbjyjg1yqqd366zkpfdys6r5ps")))

(define-public crate-chisel-0.2.0 (c (n "chisel") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "libchisel") (r "^0.2.0") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.7") (d #t) (k 0)))) (h "1y1q46l7fs6qyp2j57wh1fzdzw9vjnac71ihrk3sgirz1h9akypm")))

(define-public crate-chisel-0.3.0 (c (n "chisel") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "libchisel") (r "^0.3.0") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.7") (d #t) (k 0)))) (h "1h7hd0ikvs1na1nch3l3v0sbrvknp79ac8ig3pcgwc1vqqc1bzfm")))

(define-public crate-chisel-0.4.0 (c (n "chisel") (v "0.4.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "libchisel") (r "^0.4.0") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.7") (d #t) (k 0)))) (h "1hkvp4ji8pxyd1d8kpr6gjk8adyxnf04x3d5ijsnm52gybx80nzb")))

(define-public crate-chisel-0.5.0 (c (n "chisel") (v "0.5.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "libchisel") (r "^0.5.0") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.7") (d #t) (k 0)))) (h "04ky8kg2hz2vj6a3v7vnqz44rws04a5z8pi1yky0rin7xqnxga9j")))

(define-public crate-chisel-0.6.0 (c (n "chisel") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "libchisel") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.7") (d #t) (k 0)) (d (n "wasmprinter") (r "^0.2.0") (d #t) (k 0)) (d (n "wat") (r "^1.0.7") (d #t) (k 0)))) (h "1p9b1nyj26wrmx7ah3njds8j8aabglv96kxnxr5iyi3inz8paj6f") (f (quote (("default") ("binaryen" "libchisel/binaryen"))))))

