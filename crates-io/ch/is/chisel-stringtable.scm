(define-module (crates-io ch is chisel-stringtable) #:use-module (crates-io))

(define-public crate-chisel-stringtable-0.1.0 (c (n "chisel-stringtable") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "0g1wbfy1r99indhl4m5i69snj6x9h6x3gibiwgzy4zvgiiqv2bdb") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-chisel-stringtable-0.1.1 (c (n "chisel-stringtable") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04n21hfybxa29r06h23pp19yhw8d25029qyffzazh9ddjllmwvhd") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-chisel-stringtable-0.1.2 (c (n "chisel-stringtable") (v "0.1.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "01ix0zkx3kvricdfc1lp46ixv2wlbrwn5w0haj4nrvnpw5pbggy2") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-chisel-stringtable-0.1.3 (c (n "chisel-stringtable") (v "0.1.3") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "13srw6q1w3s73bw8x2fh7ll51dc4lcbkmnj0050m2haa9vjxcpvr") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-chisel-stringtable-0.1.4 (c (n "chisel-stringtable") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1s8nnv9cqwrgwah60mhvr6yd9lwfvianpchlmnj413xnv0nr8ibq") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-chisel-stringtable-0.1.5 (c (n "chisel-stringtable") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1mr4wq01mcw5x7i86g5p02ffazg22sbs22kjg2bwk2hqgvgblgza") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-chisel-stringtable-0.1.6 (c (n "chisel-stringtable") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1696s5ws5y60ampb7328q2nsdjyfaysidvj0208r8c8h2dj28y9z") (f (quote (("default")))) (y #t) (r "1.56")))

