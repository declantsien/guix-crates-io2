(define-module (crates-io ch is chisel-common) #:use-module (crates-io))

(define-public crate-chisel-common-1.0.8 (c (n "chisel-common") (v "1.0.8") (h "1br31gjc2rk18ly0isalv27wl3ikjqcdigznsr0f0rl1pj3cxzpd") (y #t) (r "1.56")))

(define-public crate-chisel-common-1.0.9 (c (n "chisel-common") (v "1.0.9") (h "157d6avm8yrjpa3v4f0p9ybn982axxbv1v5kda34ndw5n6i4mh7g") (y #t) (r "1.56")))

(define-public crate-chisel-common-1.0.10 (c (n "chisel-common") (v "1.0.10") (h "1jnamafgxg1v97aj0r76frf5wwpgks1701d5b42vadgqpkjp8r1i") (y #t) (r "1.56")))

(define-public crate-chisel-common-1.0.11 (c (n "chisel-common") (v "1.0.11") (h "0k57zy5mkvn58fgcphq53ws2p1in5m5in08nr3d1k5a5kfrjb710") (y #t) (r "1.56")))

(define-public crate-chisel-common-1.0.12 (c (n "chisel-common") (v "1.0.12") (h "1s18rg8f4mqmqdgqi5zq55h9bw0x4vl969z5vcc9xlpf3f0h459l") (r "1.56")))

(define-public crate-chisel-common-1.1.0 (c (n "chisel-common") (v "1.1.0") (h "17i72xclq2yw420agla63l50qi40l040x5ax4i182i5lgm346078") (r "1.56")))

