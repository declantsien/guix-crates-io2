(define-module (crates-io ch op chop) #:use-module (crates-io))

(define-public crate-chop-0.0.1 (c (n "chop") (v "0.0.1") (d (list (d (n "chopper") (r "^0.0.5") (d #t) (k 0)))) (h "1wc7gzbm1ghkzhnnwwyxf9m2ad8i1ajx4apdhxw0ciglyksndw6f") (y #t)))

(define-public crate-chop-0.0.2 (c (n "chop") (v "0.0.2") (d (list (d (n "better-panic") (r "^0.2") (d #t) (k 0)) (d (n "chopper") (r "^0.1.0") (d #t) (k 0)))) (h "1ivr8yz95aj3n6q4w2639pz7zfjlvcg0fiklxqfdjwj1jcvf4sz8") (y #t)))

(define-public crate-chop-0.0.3 (c (n "chop") (v "0.0.3") (d (list (d (n "better-panic") (r "^0.2") (d #t) (k 0)) (d (n "chopper") (r "^0.2") (d #t) (k 0)))) (h "19gjhdb73camzg4rb1q9k7gxfi261vixgv4wfr2lv4hhyqakxjma") (y #t)))

(define-public crate-chop-0.0.4 (c (n "chop") (v "0.0.4") (d (list (d (n "better-panic") (r "^0.2") (d #t) (k 0)) (d (n "chopper") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1g7fgx1zkd9rj2z2ilzx0x5ayzbdp8x4pwfmjvjs6x8idvkbbzk8") (y #t)))

(define-public crate-chop-0.1.0 (c (n "chop") (v "0.1.0") (d (list (d (n "better-panic") (r "^0.3") (d #t) (k 0)) (d (n "chopper") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("full"))) (d #t) (k 0)))) (h "17n50dnsvl1qai1x45qb5mv66jn9mww05adpppy4wmg18s0ycwc9") (y #t)))

