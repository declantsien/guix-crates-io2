(define-module (crates-io ch ow chownr) #:use-module (crates-io))

(define-public crate-chownr-0.1.0 (c (n "chownr") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "nix") (r "^0.14.0") (d #t) (k 0)))) (h "0whpnm93har7fw4183gl4fmfkkx1mjg8ji3xxmbx165xhv01qqlg")))

(define-public crate-chownr-1.0.0 (c (n "chownr") (v "1.0.0") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nix") (r "^0.14.0") (d #t) (k 0)))) (h "17gl42939sxgnslyc1ar37i1xxbpmpk7p4bp5jqkyxkrjzvb87f8")))

(define-public crate-chownr-2.0.0 (c (n "chownr") (v "2.0.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nix") (r "^0.14.0") (d #t) (k 0)))) (h "1j8x3vj2gxswa6d0p06k7gqgp8fmzsbbaaiabfrs5kh28x8yxb2k")))

(define-public crate-chownr-3.0.0 (c (n "chownr") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.16") (d #t) (k 0)) (d (n "nix") (r "^0.14.0") (d #t) (k 0)))) (h "1jqc1w71wf79fh2hvvjc7pip2bva285hcl8yq7j2x5bqbpb5r44p")))

