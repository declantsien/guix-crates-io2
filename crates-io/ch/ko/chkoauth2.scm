(define-module (crates-io ch ko chkoauth2) #:use-module (crates-io))

(define-public crate-chkoauth2-0.1.0 (c (n "chkoauth2") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oauth2") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1can73hfjpahxp4j3yydplq9xawplmjkscwgjq7ix7rqffhwwwzd") (f (quote (("indieauth" "serde") ("default" "indieauth"))))))

