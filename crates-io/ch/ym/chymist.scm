(define-module (crates-io ch ym chymist) #:use-module (crates-io))

(define-public crate-chymist-0.1.0 (c (n "chymist") (v "0.1.0") (d (list (d (n "fructose") (r "^0.3.4") (d #t) (k 0)) (d (n "glucose") (r "^0.2.2") (d #t) (k 0)))) (h "1459x79knwgw38c2mvccgd9xxqhq6vih3a8fi3n43rp0gzy9zlxk")))

