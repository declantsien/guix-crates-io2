(define-module (crates-io ch mo chmod-conversion) #:use-module (crates-io))

(define-public crate-chmod-conversion-0.1.0 (c (n "chmod-conversion") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hy6jbdkmqzkwjn0zacvpa15h6p0darlp0znmd66glxcxiprvd9f")))

(define-public crate-chmod-conversion-0.1.1 (c (n "chmod-conversion") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "051fvhxqcac9kj76xf7qmmjpa3n464pyyf4agjzpmmskqbhl97nq")))

(define-public crate-chmod-conversion-0.1.2 (c (n "chmod-conversion") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1my5jfxwfw1drj27kriwfkdm13imihwxjpmmn9jlj2cycfbppmnv")))

(define-public crate-chmod-conversion-0.1.3 (c (n "chmod-conversion") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "031602d6akccw243cxy8g8c1fww4fb6gng3bcasq9w1ysdg5xjrl")))

(define-public crate-chmod-conversion-0.1.4 (c (n "chmod-conversion") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gd0ga5xyzf11hhppkpdn1qvq1cabrq0q4r469s4drh4l7jmzjy0")))

