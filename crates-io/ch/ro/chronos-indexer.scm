(define-module (crates-io ch ro chronos-indexer) #:use-module (crates-io))

(define-public crate-chronos-indexer-0.0.1 (c (n "chronos-indexer") (v "0.0.1") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)))) (h "1w8jsbw5m4iagi180dk1cs82hljprh3afhl4d2l2cs9n01jyx1j4") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint")))) (y #t)))

(define-public crate-chronos-indexer-0.0.2 (c (n "chronos-indexer") (v "0.0.2") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)))) (h "1f82mpz420ypb6j4fnv13yb4g7728d5r02dpd8q0grm038g4x413") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint")))) (y #t)))

