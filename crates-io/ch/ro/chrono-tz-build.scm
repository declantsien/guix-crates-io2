(define-module (crates-io ch ro chrono-tz-build) #:use-module (crates-io))

(define-public crate-chrono-tz-build-0.0.1 (c (n "chrono-tz-build") (v "0.0.1") (d (list (d (n "parse-zoneinfo") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (k 0)))) (h "13ywv3fqscbaj7rs5sdpqx3lq6jdrs72mr18ll7k2fra0hyh7jb1") (f (quote (("filter-by-regex" "regex"))))))

(define-public crate-chrono-tz-build-0.0.2 (c (n "chrono-tz-build") (v "0.0.2") (d (list (d (n "parse-zoneinfo") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("uncased"))) (k 0)) (d (n "phf_codegen") (r "^0.10") (k 0)) (d (n "regex") (r "^1") (o #t) (k 0)) (d (n "uncased") (r "^0.9") (o #t) (k 0)))) (h "0schy3z03psvmc6734hgkx52cdb3zvixgzhvhr0mzxmj7x4qs1fv") (f (quote (("filter-by-regex" "regex") ("case-insensitive" "uncased"))))))

(define-public crate-chrono-tz-build-0.0.3 (c (n "chrono-tz-build") (v "0.0.3") (d (list (d (n "parse-zoneinfo") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("uncased"))) (k 0)) (d (n "phf_codegen") (r "^0.11") (k 0)) (d (n "regex") (r "^1") (o #t) (k 0)) (d (n "uncased") (r "^0.9") (o #t) (k 0)))) (h "031s51dd2vbxdhz7fxk1jpbbvr800w57an14bsq3fd5khwx9ql3g") (f (quote (("filter-by-regex" "regex") ("case-insensitive" "uncased"))))))

(define-public crate-chrono-tz-build-0.1.0 (c (n "chrono-tz-build") (v "0.1.0") (d (list (d (n "parse-zoneinfo") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_codegen") (r "^0.11") (k 0)) (d (n "regex") (r "^1") (o #t) (k 0)) (d (n "uncased") (r "^0.9") (o #t) (k 0)))) (h "0lgp6przwk3l3w33saprjxgr94ignfzghns884b13cp9yywqz6fr") (f (quote (("filter-by-regex" "regex") ("case-insensitive" "uncased" "phf/uncased"))))))

(define-public crate-chrono-tz-build-0.2.0 (c (n "chrono-tz-build") (v "0.2.0") (d (list (d (n "parse-zoneinfo") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_codegen") (r "^0.11") (k 0)) (d (n "regex") (r "^1") (o #t) (k 0)) (d (n "uncased") (r "^0.9") (o #t) (k 0)))) (h "1kqywd9y8jn0kpw5npd2088qbrdsb6jd39k0snbfsmrgjkffpxg2") (f (quote (("filter-by-regex" "regex") ("case-insensitive" "uncased" "phf/uncased")))) (r "1.60")))

(define-public crate-chrono-tz-build-0.2.1 (c (n "chrono-tz-build") (v "0.2.1") (d (list (d (n "parse-zoneinfo") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_codegen") (r "^0.11") (k 0)) (d (n "regex") (r "^1") (o #t) (k 0)) (d (n "uncased") (r "^0.9") (o #t) (k 0)))) (h "03rmzd69cn7fp0fgkjr5042b3g54s2l941afjm3001ls7kqkjgj3") (f (quote (("filter-by-regex" "regex") ("case-insensitive" "uncased" "phf/uncased")))) (r "1.60")))

(define-public crate-chrono-tz-build-0.3.0 (c (n "chrono-tz-build") (v "0.3.0") (d (list (d (n "parse-zoneinfo") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_codegen") (r "^0.11") (k 0)) (d (n "regex") (r "^1") (o #t) (k 0)) (d (n "uncased") (r "^0.9") (o #t) (k 0)))) (h "1c8ixwwwsn9kgs1dr5mz963p0fgw9j9p7fzb3w2c7y8xhkp8l20c") (f (quote (("filter-by-regex" "regex") ("case-insensitive" "uncased" "phf/uncased")))) (s 2) (e (quote (("regex" "dep:regex")))) (r "1.60")))

