(define-module (crates-io ch ro chrono-systemd-time-ng) #:use-module (crates-io))

(define-public crate-chrono-systemd-time-ng-0.2.0 (c (n "chrono-systemd-time-ng") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)))) (h "0hagixm4il5vf40qhyl30yd1ya5xqfk4zbp22pcd6a0crd2awj19") (y #t)))

(define-public crate-chrono-systemd-time-ng-0.3.0 (c (n "chrono-systemd-time-ng") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)))) (h "1payw14y152ilagd8vvahx3zvpiv6vvw9c7szhlr74a90vvx8qgr") (y #t)))

(define-public crate-chrono-systemd-time-ng-0.3.1 (c (n "chrono-systemd-time-ng") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)))) (h "0p55lfbrs5nskl321x8rhb738w51rpi3g4pb38qp1hh6zzcz5mbw") (y #t)))

