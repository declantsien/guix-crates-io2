(define-module (crates-io ch ro chrono-systemd-time) #:use-module (crates-io))

(define-public crate-chrono-systemd-time-0.1.0 (c (n "chrono-systemd-time") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)))) (h "13c4kn49n2796lp43p75c26g7ynpzrf8q7h7pfd7d3yxz65ywnz2")))

(define-public crate-chrono-systemd-time-0.3.0 (c (n "chrono-systemd-time") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)))) (h "0v6xf3amp8nyjsf0bkih3vmm2yy4q2kx92x3i81cb1pvla9sx7s2")))

