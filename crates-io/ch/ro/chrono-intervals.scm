(define-module (crates-io ch ro chrono-intervals) #:use-module (crates-io))

(define-public crate-chrono-intervals-0.1.0 (c (n "chrono-intervals") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)))) (h "0zzgcilv90i05ngh1h1433sf22fswkqp3a6r8fsgfvbb936abscl")))

(define-public crate-chrono-intervals-0.2.0 (c (n "chrono-intervals") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03bndpl32v5ry4yfw60pvw1m5yhq9911i7wxbz3yjq5mjra3hhk9")))

(define-public crate-chrono-intervals-0.3.0 (c (n "chrono-intervals") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1dj9x3qab908g0bi2agnb43f65mhgggzarlk5nnalj6zq6k6vpsi")))

