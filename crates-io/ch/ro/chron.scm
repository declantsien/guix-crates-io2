(define-module (crates-io ch ro chron) #:use-module (crates-io))

(define-public crate-chron-0.0.0 (c (n "chron") (v "0.0.0") (h "1b8bijv5q4dsnawzwlwq9whvnj5q4xjrrrsk1fpg3pic2h9vkmy7")))

(define-public crate-chron-0.1.0 (c (n "chron") (v "0.1.0") (h "0kfa8cqrax1vbqsncqd7p4pp5w4z9q0p2hy6gys0cn62amqdj0qq")))

(define-public crate-chron-0.1.1 (c (n "chron") (v "0.1.1") (h "0klmnj04kpifad8pv2cwc5nvnnaa9yiy5jj41b91gq9wq9568yqf")))

(define-public crate-chron-0.1.2 (c (n "chron") (v "0.1.2") (d (list (d (n "ctrlc") (r "^3.4.0") (d #t) (k 2)))) (h "1l1fgyzspvizsrkicn64ypk4hk7s6z23qlxc8bvg1x9cf0h4cppz")))

(define-public crate-chron-0.1.3 (c (n "chron") (v "0.1.3") (d (list (d (n "ctrlc") (r "^3.4.0") (d #t) (k 2)))) (h "12rv101nri4l4z5abz7gvf09d73pfv9j80qz9yhjvyqjdb9vzpfn")))

(define-public crate-chron-0.1.4 (c (n "chron") (v "0.1.4") (d (list (d (n "ctrlc") (r "^3.4.0") (d #t) (k 2)))) (h "06m6gafaavv6aw80vr4wrclx0zj5l0051wgi04mzskwa26z78z2g")))

