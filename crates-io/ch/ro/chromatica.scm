(define-module (crates-io ch ro chromatica) #:use-module (crates-io))

(define-public crate-chromatica-1.0.0 (c (n "chromatica") (v "1.0.0") (h "01m9a3b2bb0q7yicpbbrfny9mh7apmq5gs9kklvfh0sldhrjkrkh")))

(define-public crate-chromatica-1.0.1 (c (n "chromatica") (v "1.0.1") (h "10h5z66lw23yhy5zszmld0b9vligmypww6mlfl05dfk9xq3v5r17")))

