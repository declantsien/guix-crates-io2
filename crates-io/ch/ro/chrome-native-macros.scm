(define-module (crates-io ch ro chrome-native-macros) #:use-module (crates-io))

(define-public crate-chrome-native-macros-1.0.0 (c (n "chrome-native-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)))) (h "19k8z3grqjlh8swy2n9frcr29mw2x6vdpqmdf41hb79qi37gl5qp")))

