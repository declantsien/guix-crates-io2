(define-module (crates-io ch ro chronlang-parser) #:use-module (crates-io))

(define-public crate-chronlang-parser-0.1.0 (c (n "chronlang-parser") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.2.0") (d #t) (k 0)) (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)))) (h "1j65l4v89ad8ip97bsdwqvhka04h9v149ddlfnn6vc9nzhgbs2c1")))

(define-public crate-chronlang-parser-0.1.2 (c (n "chronlang-parser") (v "0.1.2") (d (list (d (n "ariadne") (r "^0.2.0") (d #t) (k 0)) (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)))) (h "1pphsybnllf2l3cd5kxx4wsjzbqba6j820yzbfwq0jynwc8pf4rv")))

(define-public crate-chronlang-parser-0.1.3 (c (n "chronlang-parser") (v "0.1.3") (d (list (d (n "ariadne") (r "^0.2.0") (d #t) (k 0)) (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)))) (h "0z3ncrs8q5x4w6ka081s1d7xzgzb6pia1aniy4ckj782sac4scbp")))

(define-public crate-chronlang-parser-0.1.4 (c (n "chronlang-parser") (v "0.1.4") (d (list (d (n "ariadne") (r "^0.2.0") (d #t) (k 0)) (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)))) (h "00cwxk35d347ip4rvnr6rm1wvlz0830vjvzb389qxh4jd5p20qhb")))

(define-public crate-chronlang-parser-0.1.5 (c (n "chronlang-parser") (v "0.1.5") (d (list (d (n "ariadne") (r "^0.2.0") (d #t) (k 0)) (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qx4296ihx96ybfp9r3hc7523a6cz2fxxkizcg48ccvx6zc0512w")))

(define-public crate-chronlang-parser-0.1.6 (c (n "chronlang-parser") (v "0.1.6") (d (list (d (n "ariadne") (r "^0.2.0") (d #t) (k 0)) (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p7dnv2w7jkrl2xzwdpm9kw170dhfb8jdn1q82sd8403hxdjf1zr")))

(define-public crate-chronlang-parser-0.1.7 (c (n "chronlang-parser") (v "0.1.7") (d (list (d (n "ariadne") (r "^0.2.0") (d #t) (k 0)) (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rcflhxqmnfkai3m22qj2bjlv526psgzsmfm8aslk1xkf5mc5q92")))

