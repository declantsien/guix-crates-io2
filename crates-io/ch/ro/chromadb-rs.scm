(define-module (crates-io ch ro chromadb-rs) #:use-module (crates-io))

(define-public crate-chromadb-rs-0.1.0 (c (n "chromadb-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gbh7bfqv7qmfjqz25kr0bjg78jp8m4591ssdmj45wzvl2hmy60q")))

(define-public crate-chromadb-rs-0.1.1 (c (n "chromadb-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.12.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nqrq7d9gyjpk8fn1d5ck3ld39sfswds56c3a0hafv4292hk7pqz")))

(define-public crate-chromadb-rs-0.1.2 (c (n "chromadb-rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.12.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w166bzl79zwhlpbflcm61mq3sbiqixp90n3gavbvj8j9pa08dxk")))

(define-public crate-chromadb-rs-0.1.4 (c (n "chromadb-rs") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.12.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0kw5s169w4pl1r9v9bsavxzz8pq16kjz3ddnxzsqij68jd898paf")))

(define-public crate-chromadb-rs-0.1.6 (c (n "chromadb-rs") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.12.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0243g0cwwynqlbb6z16452k4vicpd7cizx49iy6xa8q87z8hkz8s")))

(define-public crate-chromadb-rs-0.1.8 (c (n "chromadb-rs") (v "0.1.8") (d (list (d (n "reqwest") (r "^0.12.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0rx3ppk4nvhc7wzr0bvb3y0gs0f3cab48m1rk9fn3nhc3n6ky70w")))

