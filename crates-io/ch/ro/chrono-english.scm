(define-module (crates-io ch ro chrono-english) #:use-module (crates-io))

(define-public crate-chrono-english-0.1.0 (c (n "chrono-english") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 2)) (d (n "scanlex") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1gs3q9jpa89csb495gqvczdvsh1jxsvfippksncyc1xc142n9r0n")))

(define-public crate-chrono-english-0.1.1 (c (n "chrono-english") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 2)) (d (n "scanlex") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "03za57r8hskz8n77gd2j528sg0crx7sg7acqp67sicxc53zx49vp")))

(define-public crate-chrono-english-0.1.2 (c (n "chrono-english") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 2)) (d (n "scanlex") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1zvzm2mpr0xjqx1fd1gr9fn819diing943hbsnmwdqf747ppkqxf")))

(define-public crate-chrono-english-0.1.3 (c (n "chrono-english") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 2)) (d (n "scanlex") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "06rl62kfkmr6kj3a50dzpas2vpjy75193znl3d77j2r5v41j4pqv")))

(define-public crate-chrono-english-0.1.4 (c (n "chrono-english") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 2)) (d (n "scanlex") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "01jjz6jqyypjxvcmx17j1qbzcrmi0m80ihnpnpfcyf976lcywcs2")))

(define-public crate-chrono-english-0.1.5 (c (n "chrono-english") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 2)) (d (n "scanlex") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1yg44iw0rrki58dnlpzgvqib1w2vplgspavz8yqh92r24ql4gz0f")))

(define-public crate-chrono-english-0.1.6 (c (n "chrono-english") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 2)) (d (n "scanlex") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1s9qvhizb5dwrja5nhbc2fw4vm3qiny3ih3b8711zi7pyl6iir8b")))

(define-public crate-chrono-english-0.1.7 (c (n "chrono-english") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 2)) (d (n "scanlex") (r "^0.1.2") (d #t) (k 0)))) (h "0vqdl2bfyv224xv2xnqa9rsnbn89pjhzbhvrqs47sjpblyfr0ggp")))

