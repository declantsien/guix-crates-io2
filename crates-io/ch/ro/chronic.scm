(define-module (crates-io ch ro chronic) #:use-module (crates-io))

(define-public crate-chronic-0.1.0 (c (n "chronic") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ybh0fsl2xd2jkryyv9dv0nhhss8jihr18cdn4mcivcnfwxl1jzc")))

(define-public crate-chronic-0.1.1 (c (n "chronic") (v "0.1.1") (h "1xa124yggk5nhawwg9vikmippd7aawb2245ipap96dihc5bbc1r7")))

(define-public crate-chronic-0.1.2 (c (n "chronic") (v "0.1.2") (h "1ggyjp7xhyp4mqx1hsss200pk04g6qzilqyy78fi92pi8dcg28dn")))

