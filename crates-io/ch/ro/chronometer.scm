(define-module (crates-io ch ro chronometer) #:use-module (crates-io))

(define-public crate-chronometer-0.1.0 (c (n "chronometer") (v "0.1.0") (h "1lzh9v4gpd7hxz7m11af2f6acsb7kjv0wwnnikh4dycazvhx9s52")))

(define-public crate-chronometer-0.1.1 (c (n "chronometer") (v "0.1.1") (h "00q4h15kr71zfl6g4s8rxm04n7galxvl8plvgkw3f83w2ngz6wbj")))

(define-public crate-chronometer-0.1.2 (c (n "chronometer") (v "0.1.2") (h "0ndqq5dnccv3ych9y85r69pmiiyiflk8jy0k8wfb6siiakpvsd1c")))

