(define-module (crates-io ch ro chromiumoxide_types) #:use-module (crates-io))

(define-public crate-chromiumoxide_types-0.1.0 (c (n "chromiumoxide_types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dmak8z043fhsah94xq31zqb6sj6ybzcwnanp8yprx3s1vv65mlm")))

(define-public crate-chromiumoxide_types-0.2.0 (c (n "chromiumoxide_types") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nl2apiqvwnqmsfkz3zzarpyqm2n176qyaq2f4xwwr109bggx986")))

(define-public crate-chromiumoxide_types-0.3.0 (c (n "chromiumoxide_types") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0jsjva7yds3km5yd3g96wxi3030pzbprgzp41bdw17npqs61g4j6")))

(define-public crate-chromiumoxide_types-0.3.1 (c (n "chromiumoxide_types") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0ha8519vqqqvb8f7w42z2rqh69x722693x7zhks08ps04wn9dlav")))

(define-public crate-chromiumoxide_types-0.3.2 (c (n "chromiumoxide_types") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "1yfznzn5k25si0kyvhc7q1fqsxcfb5d2g6jva3c292mpk06jwkns")))

(define-public crate-chromiumoxide_types-0.3.3 (c (n "chromiumoxide_types") (v "0.3.3") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "09gyq40jgvg3g46zvyhr4k2s1997fb7k4hbnqa01qzcarklhpmw2") (y #t)))

(define-public crate-chromiumoxide_types-0.3.4 (c (n "chromiumoxide_types") (v "0.3.4") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "04ir496zvbi69y2vbjm49aszhl05bb7gkrpird9v6sarzfxlcjzp")))

(define-public crate-chromiumoxide_types-0.3.5 (c (n "chromiumoxide_types") (v "0.3.5") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "0r1b15d3rf9li8lx9i5yzs8abzlky1f78vxc928kbq7iprvy6gp4")))

(define-public crate-chromiumoxide_types-0.4.0 (c (n "chromiumoxide_types") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "0vv7ivi11jrh119kdgy9s981hmd8x3flsyyc76bg1ixann1w3y8s")))

(define-public crate-chromiumoxide_types-0.5.0 (c (n "chromiumoxide_types") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "07sm6kxjwwap89dw8zrp3gbz2yhzbb9acd89d5aybf1phq2qg48c")))

(define-public crate-chromiumoxide_types-0.5.1 (c (n "chromiumoxide_types") (v "0.5.1") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "1lr4hcmbc4np22ljhrp4j3cnyzv9phc83c0slbmi5frwvgb50hrh")))

(define-public crate-chromiumoxide_types-0.5.2 (c (n "chromiumoxide_types") (v "0.5.2") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "099ylagp6iy7ax781rszlshm1pna1alrhxhgll17m6l99izp3z5m")))

(define-public crate-chromiumoxide_types-0.6.0 (c (n "chromiumoxide_types") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09x8aiqh9qm25navfbbll8pkid45yyyrpy65b0i1q2wrix5yzinm")))

