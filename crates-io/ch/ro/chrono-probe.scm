(define-module (crates-io ch ro chrono-probe) #:use-module (crates-io))

(define-public crate-chrono-probe-0.1.0 (c (n "chrono-probe") (v "0.1.0") (d (list (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04lv6lh74xr98km782gnvpdvqfl94b6jy8zdq3vvzm20bwxcgxwx") (f (quote (("debug"))))))

