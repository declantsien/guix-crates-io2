(define-module (crates-io ch ro chromium) #:use-module (crates-io))

(define-public crate-chromium-0.0.0-alpha (c (n "chromium") (v "0.0.0-alpha") (h "0mrapddvyd65a0908g5gpx9j465qg7jxv243vsjkjhd8f91wkgak")))

(define-public crate-chromium-0.0.1 (c (n "chromium") (v "0.0.1") (h "1835r5vk9ja40sa1wxmscra8874acdgpnh1p0k0sa3cfsgw5gy44")))

(define-public crate-chromium-0.0.2 (c (n "chromium") (v "0.0.2") (h "09mwfn7sk1nplclaq117jck71labar6hjwmdlvz17vgl0pvdjvja") (f (quote (("unsafe_alloc") ("default"))))))

