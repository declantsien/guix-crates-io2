(define-module (crates-io ch ro chroniker) #:use-module (crates-io))

(define-public crate-chroniker-0.1.0 (c (n "chroniker") (v "0.1.0") (h "06p5jxjpxjhsqwgql5f4qlrbh4zg0chb1iqp91vfc32fdxgi5qbi")))

(define-public crate-chroniker-0.1.1 (c (n "chroniker") (v "0.1.1") (h "1ywffmvqp4x3gdx3k8g624pgj75bmm4mi6lkkz1w146q1gd6dvr3")))

(define-public crate-chroniker-0.2.0 (c (n "chroniker") (v "0.2.0") (h "0hz6la9kah2dzwsb2xd77hdr6j7kkgnhh64ks7g2944r1fspl05k")))

(define-public crate-chroniker-0.3.0 (c (n "chroniker") (v "0.3.0") (h "16kn8x9gi7j3gdkr7gf3dqs2zipf39jix6iq6f2pcv1pi78m93ch")))

(define-public crate-chroniker-0.3.1 (c (n "chroniker") (v "0.3.1") (h "0pj9q1bk2793069bjr4ry00pqncm536rha4crck9n69dm9xq6mjz")))

(define-public crate-chroniker-0.4.0 (c (n "chroniker") (v "0.4.0") (h "04q0vpagpg5jmxwgz6ya3pmssf5yv3fiv97gnh2r7vn57qq3hhk0")))

(define-public crate-chroniker-0.4.1 (c (n "chroniker") (v "0.4.1") (h "1ls6w046lvnk2cxnllv349d7lc2bw4l70ly8i8jji61815r9l3lh")))

(define-public crate-chroniker-0.5.0 (c (n "chroniker") (v "0.5.0") (h "0agjvxkrb1866wy1a1lfxlaykbw1h59s04b70crc386qv0axgjc7")))

(define-public crate-chroniker-0.5.1 (c (n "chroniker") (v "0.5.1") (h "02d1n48xl14nd22h90yi78k4zihja91p5ii2ln5zl8n6yrpi4h3a")))

