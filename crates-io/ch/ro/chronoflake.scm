(define-module (crates-io ch ro chronoflake) #:use-module (crates-io))

(define-public crate-chronoflake-1.0.0 (c (n "chronoflake") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0cw1hf8rs6a13n6vgfvif5q80yl41jhadpa6qy7al3bxynbpyagz")))

(define-public crate-chronoflake-1.0.1 (c (n "chronoflake") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1abl4gys0bc04x9w0zw5k06vkv68h9xp43shak1lbw1njzppzssx")))

