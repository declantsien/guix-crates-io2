(define-module (crates-io ch ro chrome-wasm-bindgen) #:use-module (crates-io))

(define-public crate-chrome-wasm-bindgen-0.0.1 (c (n "chrome-wasm-bindgen") (v "0.0.1") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.6.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "1db8qq5wsx6i0xkjwksqv300lr836mrvwn1l8d9gwy4bmglhhx1j")))

