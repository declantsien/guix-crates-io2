(define-module (crates-io ch ro chrometracer-attributes) #:use-module (crates-io))

(define-public crate-chrometracer-attributes-0.1.0 (c (n "chrometracer-attributes") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09x9d2maaa91ahfn2gydws4fqksknhqy53jff6x7lqiy3yihzc6h")))

(define-public crate-chrometracer-attributes-0.1.1 (c (n "chrometracer-attributes") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "021kf8498f5pxy2w7qgd2k0ycpgra4v8ivn89vxkd3rkq9sxpwcs")))

