(define-module (crates-io ch ro chrome-native) #:use-module (crates-io))

(define-public crate-chrome-native-1.0.0 (c (n "chrome-native") (v "1.0.0") (d (list (d (n "chrome-native-macros") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "04vi2a24m62svml43y5f20hbk3w72ns05a2kgm7chy2dcrq77mgm") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json") ("macros" "dep:chrome-native-macros"))))))

