(define-module (crates-io ch ro chrono_elapsed) #:use-module (crates-io))

(define-public crate-chrono_elapsed-0.1.0 (c (n "chrono_elapsed") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "0bkfd91mi1d97dl8p5llxxa86rzf10a94a2y9nwld1zyqqdpvh3c")))

(define-public crate-chrono_elapsed-0.1.1 (c (n "chrono_elapsed") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "12sc68x9yx87619niwps0r215k1akb274hbgqaxr5c7c6591rikb")))

(define-public crate-chrono_elapsed-0.1.2 (c (n "chrono_elapsed") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "057p7k0hj3drfxv0pkyjk5jbn53mhidm60d378612vv5n7nyfmss")))

(define-public crate-chrono_elapsed-0.1.3 (c (n "chrono_elapsed") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "1xdjc6q6ym4h3qxb9f1p8mdcx5aw9imlcwq5ik83w176raizcd96")))

(define-public crate-chrono_elapsed-0.1.4 (c (n "chrono_elapsed") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "153mabzdkhq722c83h9q9ginkm57clh9gyzji2l8g4yxl754fgli")))

(define-public crate-chrono_elapsed-0.1.41 (c (n "chrono_elapsed") (v "0.1.41") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "1qzd9c51175zkamcp3lqa0yn3rqn7li2ikn5pi3bkdpmq155sa4b")))

(define-public crate-chrono_elapsed-0.1.42 (c (n "chrono_elapsed") (v "0.1.42") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "1j58iyfkcqhrkb7y4lfj42p6c91g00nb9s2y579xb1v0mscxpg1p")))

(define-public crate-chrono_elapsed-0.1.43 (c (n "chrono_elapsed") (v "0.1.43") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "1p2lflx1i58372wczvzh4gr9gpm6l0p74ppg7sc8v8naxvdq8f63")))

(define-public crate-chrono_elapsed-0.1.44 (c (n "chrono_elapsed") (v "0.1.44") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "0sz9dcmxbg0mahwmygf4diga6xz89d3dzd6yb7ya5mvy9d7jinb0")))

(define-public crate-chrono_elapsed-0.1.45 (c (n "chrono_elapsed") (v "0.1.45") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "0f5nmyxmihbyzmkk96s8qj7g3nmdxj6cggy7kivg55c7y2bjq2p8")))

(define-public crate-chrono_elapsed-0.1.46 (c (n "chrono_elapsed") (v "0.1.46") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "1y43b5vlwr99409izz59gxp9b6dfndmimqnk7ns0cfpf3r0a1wfn")))

(define-public crate-chrono_elapsed-0.1.47 (c (n "chrono_elapsed") (v "0.1.47") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "1gwrqikx98xfd6fgypbvqcyyb9rz87lggx8gq32cyn1s3028hxnf")))

(define-public crate-chrono_elapsed-1.0.0 (c (n "chrono_elapsed") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "1ls7n89iap1fzpwf9frrm6hxmfjl2g755xmxwdjjv0xy4fcz0547")))

(define-public crate-chrono_elapsed-1.0.1 (c (n "chrono_elapsed") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "1ip16x1jklbwh1nccjcgl6gpfjmam75f95mp7rj7103bwjx2zyc5")))

(define-public crate-chrono_elapsed-1.0.11 (c (n "chrono_elapsed") (v "1.0.11") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "0264kk87sfi30za99rpy1b33wbl90lxd18bqbbbjpc3jhfscxm8l")))

(define-public crate-chrono_elapsed-1.0.12 (c (n "chrono_elapsed") (v "1.0.12") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)))) (h "0c3kzwrwgxd0y1hwyknpvd3d3c6g2ajsr93jdl5h1mn3l4cc60zj")))

