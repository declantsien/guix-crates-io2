(define-module (crates-io ch ro chromaprint) #:use-module (crates-io))

(define-public crate-chromaprint-0.1.0 (c (n "chromaprint") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1vq92n1ncngwd4d2lfwf6gwgp6cwzn3fih7d4kvn75pqryb5cxf1")))

(define-public crate-chromaprint-0.1.1 (c (n "chromaprint") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1invx8n7h7bpqbbnp86rllwbw9i3gkpa4i0halpby23lajfdbnm3")))

(define-public crate-chromaprint-0.1.2 (c (n "chromaprint") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1lidrpwm9dik2zbxbbwixwnwfcdwl0nyq8kjky8smrl6llks0w5y")))

(define-public crate-chromaprint-0.2.0 (c (n "chromaprint") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0g9k667wsq7a10vi4s77kiv4hm5rp49wsl6917c68wxzcy24v0mn")))

