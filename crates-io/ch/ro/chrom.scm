(define-module (crates-io ch ro chrom) #:use-module (crates-io))

(define-public crate-chrom-1.0.0 (c (n "chrom") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1v4dk7b0x0xsw1jci1v2c7pcz4lnsc2959k7l1wmnm208pydwp4v")))

(define-public crate-chrom-1.1.0 (c (n "chrom") (v "1.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04zbnxf3qpahzgffiaf1mw5jv8z63cwqqivx1j7s3yxvvz9k2bhj")))

