(define-module (crates-io ch ro chrono_ext) #:use-module (crates-io))

(define-public crate-chrono_ext-0.1.0 (c (n "chrono_ext") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "1fd6gh2jla447shvnbr75mmdz0r8cn4my8v8q2lc1k7b9gv4sh82")))

(define-public crate-chrono_ext-0.1.1 (c (n "chrono_ext") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "13jaa3yncs98ldd5xva8clszzvcwpygwz7lr0981p0h8x1l38dqf")))

