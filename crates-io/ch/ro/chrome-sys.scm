(define-module (crates-io ch ro chrome-sys) #:use-module (crates-io))

(define-public crate-chrome-sys-0.0.0 (c (n "chrome-sys") (v "0.0.0") (h "0p0sil6p28hkq0i3l6q237swqvncx9s9lhap39sf816x53wbkkgl")))

(define-public crate-chrome-sys-0.1.0 (c (n "chrome-sys") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "0w4859grnmch9g9k3pl9gzhs5kamnrnzrj3zh44g51d224qds781")))

(define-public crate-chrome-sys-0.2.0 (c (n "chrome-sys") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "00y5szqdsdmkgkh7yk6djwafpwn0kqhk5xv5y1n795abrz133ih1")))

