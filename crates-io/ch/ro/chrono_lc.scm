(define-module (crates-io ch ro chrono_lc) #:use-module (crates-io))

(define-public crate-chrono_lc-0.1.3 (c (n "chrono_lc") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.29") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 1)))) (h "02irsmmvrssj5dgrccp20fww25vbjfc5hipg977k4ri8y436smd4")))

(define-public crate-chrono_lc-0.1.4 (c (n "chrono_lc") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 1)))) (h "023d07f0a1z3w4bfz31x2yywxy3l1akwsyvfh1i24jpzqgifnn7c")))

(define-public crate-chrono_lc-0.1.5 (c (n "chrono_lc") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 1)))) (h "1mnni0gk7mf3y1ihdwg55axqq58h0vypj0m0zc19i55lj65rd740")))

(define-public crate-chrono_lc-0.1.6 (c (n "chrono_lc") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 1)))) (h "0058aj2czlmk1myxwys7zspv8hkxp6hfnxahkpmqkpwl91ii465a")))

(define-public crate-chrono_lc-0.1.7 (c (n "chrono_lc") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 1)))) (h "1dif1fh40913p1pb9bkh10dkcincx0585c7b2rsn0bynd9fli3jn")))

