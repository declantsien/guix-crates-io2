(define-module (crates-io ch ro chronologic) #:use-module (crates-io))

(define-public crate-chronologic-0.1.0 (c (n "chronologic") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0m3yv31dbycvh56fhqdsivkgfjhv0iyinvsyxix3fkpcnvq4aai7") (y #t)))

(define-public crate-chronologic-0.1.1 (c (n "chronologic") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1yrpxl3gm3djfs0gmj8279vnqwc3agfiwx00rix760n7lri6jk22") (y #t)))

(define-public crate-chronologic-0.1.2 (c (n "chronologic") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "05vndbaza0kfr4c0lwq8l6kvlwmyj47yfks3lp5xjnqix891n6x5") (y #t)))

(define-public crate-chronologic-0.1.3 (c (n "chronologic") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1r4xby9cva7l28j0y8q61dy4z4r905g863i3j3ji45ypw5hr3vza") (y #t)))

(define-public crate-chronologic-0.1.4 (c (n "chronologic") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "00msv380ry0rp7lk8dgzrk751nmnfbrc0kcqss926j0hanyzp14x") (y #t)))

(define-public crate-chronologic-0.2.0 (c (n "chronologic") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "15mviw05iycsvz3icl767bb055hhsxm9hqii5ysafgh79p61vy5x") (y #t)))

(define-public crate-chronologic-0.2.1 (c (n "chronologic") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "08fw3a9nj8qn6yhszyhp6naljw71z2kn6py326bx8xjqb9rf84sc") (y #t)))

(define-public crate-chronologic-0.2.2 (c (n "chronologic") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1kc4sny4bhg6h0vhfbvn9chiczhcx6h2gxzid5l3hkp1lp7r3r6h") (y #t)))

(define-public crate-chronologic-0.2.3 (c (n "chronologic") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "0q3s4ddwvl757402is7wg5k6kcag5sj3cv1gi7zn82pwvxzkkf3c")))

(define-public crate-chronologic-0.3.0 (c (n "chronologic") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "11hjfc1jfh04wrah9jjhfry226rzk3dlnscclxcfjgp9gj1q85sj") (y #t)))

(define-public crate-chronologic-0.3.1 (c (n "chronologic") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1k52c3cp4djwizhvafrmj404r6qa3a963b3lnnagcfcqnnyingyh") (y #t)))

(define-public crate-chronologic-0.3.2 (c (n "chronologic") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "0jckxz055f49r4hq5mk0srwc5q267svzxp9i29jlsw0hvm3ci0wv") (y #t)))

(define-public crate-chronologic-0.4.0 (c (n "chronologic") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "0i96d5zax3xpm3jjrsflq1hlijq7yrgxc16s4ysp0i9v1zvwacvk") (y #t)))

(define-public crate-chronologic-0.4.1 (c (n "chronologic") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1ijny1299a9cvvr1b0vfpsfa50a8wqv8dh5r7nakha34k8f2xrdg") (y #t)))

(define-public crate-chronologic-0.5.0 (c (n "chronologic") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1404z896pg0rscdsq6b9cixnz30480n2dfibd8vp9vz5fdrms1vc") (y #t)))

(define-public crate-chronologic-0.6.1 (c (n "chronologic") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1kphpvfwx9knh61bfk3zfg3i0z8f7r453l7khm63clwj0g399ld6") (y #t)))

(define-public crate-chronologic-0.6.2 (c (n "chronologic") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "0rbp2gicswfvw85ps7dirlzpnd8wqc2qfpbcmsxpzmng6f2b1bg5") (y #t)))

(define-public crate-chronologic-0.6.3 (c (n "chronologic") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "073c9cwz6wnpix85flyzwkpb9jn62cm0xkmbw5wl2yx9mwgvsls1") (y #t)))

(define-public crate-chronologic-0.6.4 (c (n "chronologic") (v "0.6.4") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("alloc"))) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "04v5pp5pwzs4n9mi03y7rgqxn8kzl6dd9zzdvrznbg55jsg6pl8i") (y #t)))

(define-public crate-chronologic-0.6.5 (c (n "chronologic") (v "0.6.5") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("alloc"))) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "0zxkhgrr3qx4inbsr31lq7mpb1d7ihrmzzngszdv0sk0iqxqrwqf") (y #t)))

(define-public crate-chronologic-0.6.6 (c (n "chronologic") (v "0.6.6") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("alloc"))) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "0azxarb0srznv36d9vbwlhirfr7smi83dhfk112ylv05x5rjfal6") (y #t)))

(define-public crate-chronologic-0.6.7 (c (n "chronologic") (v "0.6.7") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("alloc"))) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1v09i8xlhigs9l03xiwky0lpflkv2za06wd6y1p1hl4sqac34k91") (y #t)))

(define-public crate-chronologic-0.6.8 (c (n "chronologic") (v "0.6.8") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("alloc"))) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "16fv8hgrd7482fi0gc2zrp3q3p2vy4j64zwa4nf46n2p9yn1191n")))

(define-public crate-chronologic-0.7.0 (c (n "chronologic") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("alloc"))) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1b1an9g3vblfwbrmjnfspq4yf1m7r7mw5b6v9z9fli8f34a5nab9")))

(define-public crate-chronologic-0.7.1 (c (n "chronologic") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("alloc"))) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "0z8xl0f1cwbrb6d2zc8h3kwfqz3j2l71yprf4vfd9nh1cvr75jf3") (y #t)))

(define-public crate-chronologic-0.7.2 (c (n "chronologic") (v "0.7.2") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("alloc"))) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "1lmwxgfhvp0v187y58l94p4g1wqiv0sxrsn4yrs2fvn5vq0bpapi") (y #t)))

(define-public crate-chronologic-0.7.3 (c (n "chronologic") (v "0.7.3") (d (list (d (n "chrono") (r "^0.4.37") (f (quote ("alloc"))) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)))) (h "0p2awdid2cm232wjhbzxrwamgqsxh7lhx11xpp3c8ld5p5sxan2v")))

