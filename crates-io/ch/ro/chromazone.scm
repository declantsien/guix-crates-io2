(define-module (crates-io ch ro chromazone) #:use-module (crates-io))

(define-public crate-chromazone-1.0.0 (c (n "chromazone") (v "1.0.0") (d (list (d (n "owo-colors") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "07fpbgr3mp4fc6dhm2b0dldyv5jl3nhq3z1fx16dlasy5g6inhfv")))

(define-public crate-chromazone-1.0.1 (c (n "chromazone") (v "1.0.1") (d (list (d (n "owo-colors") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "08nhrv5k811slg4af3vsafgl1ywkjgd1cxysnmxd5x8d4ivgjrgj")))

