(define-module (crates-io ch ro chrono_locale) #:use-module (crates-io))

(define-public crate-chrono_locale-0.1.0 (c (n "chrono_locale") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (d #t) (k 1)) (d (n "serde_derive") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0qsdcfhydw7k3zfh2km48z5lvfgbm2g4sgp9ind3gai527f2276g")))

(define-public crate-chrono_locale-0.1.1 (c (n "chrono_locale") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (d #t) (k 1)) (d (n "serde_derive") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "14f4wgcvzdprld0p6m6hcsjrq0s6rphzc3liywyx0b6gj3al5gqw")))

