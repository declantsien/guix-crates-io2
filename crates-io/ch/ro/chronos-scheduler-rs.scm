(define-module (crates-io ch ro chronos-scheduler-rs) #:use-module (crates-io))

(define-public crate-chronos-scheduler-rs-1.0.2 (c (n "chronos-scheduler-rs") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chronos-parser-rs") (r "^0.1.381") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "ulid-generator-rs") (r "^0.0.93") (d #t) (k 0)))) (h "0ci3ryhgnlh12jpbipvf6rhi9wnv63bi5aqwf187xsmcjsl4gkg5")))

