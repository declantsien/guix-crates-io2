(define-module (crates-io ch ro chromosome) #:use-module (crates-io))

(define-public crate-chromosome-0.0.5 (c (n "chromosome") (v "0.0.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wv6x6wmp9k34rwzj3vp3smfx188br211aq52fij14z0h80f8v8q")))

(define-public crate-chromosome-0.0.6 (c (n "chromosome") (v "0.0.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06vyb19c22xzvglvs06nmlzq9bc8c22ndxjs7v8afn2xfjw6q5rv")))

(define-public crate-chromosome-0.0.7 (c (n "chromosome") (v "0.0.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zs69dzld1my4kcaxsjnfjqrn9gsbv13zyh23b7j4i2b9w6kh9gv")))

(define-public crate-chromosome-0.0.8 (c (n "chromosome") (v "0.0.8") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (k 0)))) (h "14vpsgnkgvlmdwsl0c4l1zz1bj7jlr8msnkx05sd747sd13mc88l") (f (quote (("std" "rand/std" "rand/std_rng") ("default" "std")))) (y #t)))

(define-public crate-chromosome-0.0.9 (c (n "chromosome") (v "0.0.9") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (k 0)))) (h "08hhl4q9krrkvnyx6245in3ni247apziqrvm2mjdcgkmdk2xq5kv") (f (quote (("std" "rand/std" "rand/std_rng") ("default" "std"))))))

(define-public crate-chromosome-0.1.0 (c (n "chromosome") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (k 0)))) (h "16fbgblhdjna00iaybhmkjd1l871yzk5dhvyzxq6fp1c18mdcb2g") (f (quote (("std" "rand/std" "rand/std_rng") ("default" "std"))))))

