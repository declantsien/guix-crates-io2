(define-module (crates-io ch ro chronos-program) #:use-module (crates-io))

(define-public crate-chronos-program-0.0.1 (c (n "chronos-program") (v "0.0.1") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "chronos-indexer") (r "^0.0.1") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "0zlbfj27v8wj51gqz71a14jxsy66vxw7nvhra3kyb698m5iqph0b") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

