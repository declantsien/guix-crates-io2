(define-module (crates-io ch ro chrome-cache-parser) #:use-module (crates-io))

(define-public crate-chrome-cache-parser-0.1.0 (c (n "chrome-cache-parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "dirs") (r "^5.0.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.34") (f (quote ("derive"))) (d #t) (k 0)))) (h "01n6l8ngpyhs2z19jzmczhwypci1nzggb949rjf2nc9xaf2wfgbx")))

(define-public crate-chrome-cache-parser-0.1.1 (c (n "chrome-cache-parser") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "dirs") (r "^5.0.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.34") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hj9ha0imbgqcvcn16n8j2kspqcp2ppdfr46b6srxymfd4i1qwdw")))

(define-public crate-chrome-cache-parser-0.1.2 (c (n "chrome-cache-parser") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "dirs") (r "^5.0.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.34") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h6hn41r2ibslp569dc8dqcbibw7gxkpsnfkn47livvda2x0xidp")))

(define-public crate-chrome-cache-parser-0.1.3 (c (n "chrome-cache-parser") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "dirs") (r "^5.0.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.34") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kicjmx72y0fh0iwjsccdiy7ixanvl1p4qhjn8pm3hkb5ss0q94y")))

