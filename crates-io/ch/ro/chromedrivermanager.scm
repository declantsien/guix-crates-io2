(define-module (crates-io ch ro chromedrivermanager) #:use-module (crates-io))

(define-public crate-ChromedriverManager-0.1.0 (c (n "ChromedriverManager") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "zip-extensions") (r "^0.6.2") (d #t) (k 0)))) (h "0l9ldidnh3zc7zpdnhl1vg4rr3jmjfwxcjkkcnhh31psfdy54ghk") (y #t)))

