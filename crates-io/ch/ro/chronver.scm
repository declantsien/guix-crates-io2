(define-module (crates-io ch ro chronver) #:use-module (crates-io))

(define-public crate-chronver-0.1.0 (c (n "chronver") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "1h97yi2xjg34pwpnqza3p6x4gvmbq0xmn8dxwkdnh7awlcqn26gf")))

(define-public crate-chronver-0.2.0 (c (n "chronver") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "semver") (r "^1.0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3.3") (f (quote ("formatting" "macros" "parsing"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.3") (d #t) (k 2)))) (h "1sfx4gakr68bk6cya5xnkbz9mchlsyw7f4mk6k197sgsmdl284sr") (r "1.56")))

