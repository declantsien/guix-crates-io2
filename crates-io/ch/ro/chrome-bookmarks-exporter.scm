(define-module (crates-io ch ro chrome-bookmarks-exporter) #:use-module (crates-io))

(define-public crate-chrome-bookmarks-exporter-0.1.0 (c (n "chrome-bookmarks-exporter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "envmnt") (r "^0.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0p0ni9ilys27k1ny9rgxds3a6x5k59fr154m0ca5zhqywsi619wb")))

