(define-module (crates-io ch ro chronounit) #:use-module (crates-io))

(define-public crate-chronounit-0.1.0 (c (n "chronounit") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "08249v9nic919md6sxrc5zm80kb84zjglqm7qma8zcyqbnb2n0zd")))

(define-public crate-chronounit-0.1.1 (c (n "chronounit") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1jlvr4abjprid7s1qwr7wsxsm4k3iq32zyy6avl0hyyhbr175x5h")))

(define-public crate-chronounit-0.2.0 (c (n "chronounit") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0wbw8fzkbh0388h9j4snkmxjldkd0fa74g123d46p6ianpbvrmba")))

(define-public crate-chronounit-0.2.1 (c (n "chronounit") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "08q5jrd1pn6qjyryhl58z8rmm0yws3a90mb7zpzy9pwi8bbrmyi0")))

(define-public crate-chronounit-0.3.0 (c (n "chronounit") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "065mkrcg0mdp7pl0qblh64hi2ay0j088xqp8hm9d5rhlhbhqsari")))

