(define-module (crates-io ch ro chrono_utils) #:use-module (crates-io))

(define-public crate-chrono_utils-0.1.0 (c (n "chrono_utils") (v "0.1.0") (d (list (d (n "chrono") (r ">= 0.2") (d #t) (k 0)))) (h "1w65gnn8ii590xk698wjfv2gs2f4883m8dshv6h50zygf10mn2gw") (y #t)))

(define-public crate-chrono_utils-0.1.1 (c (n "chrono_utils") (v "0.1.1") (d (list (d (n "chrono") (r ">= 0.2") (d #t) (k 0)))) (h "0z81rm4rd748m12rd4d64d8hxv8k4rqhxky1wabpbqgf86j94a56") (y #t)))

(define-public crate-chrono_utils-0.1.2 (c (n "chrono_utils") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "1pjdaypih816fj5844ab57hjk5ckyyz0i4g0dixvx1ig8s33h7kg")))

(define-public crate-chrono_utils-0.1.3 (c (n "chrono_utils") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0457pp7ip36az2xq7gdz4zlfhy11c7v32pg0ljhr4y0iw9sfssbz")))

