(define-module (crates-io ch ro chrono-tz-postgres) #:use-module (crates-io))

(define-public crate-chrono-tz-postgres-0.1.0 (c (n "chrono-tz-postgres") (v "0.1.0") (d (list (d (n "chrono-tz") (r "^0.7") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gv63039igwvlmqvf0rgmk59n9l1xqs63q4m777ngk4giwkm70mb") (r "1.60.0")))

(define-public crate-chrono-tz-postgres-0.1.1 (c (n "chrono-tz-postgres") (v "0.1.1") (d (list (d (n "chrono-tz") (r "^0.7") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lljrrr2lq0glscc62nrjzxz21bj6kxz0gx7s31dl9rrlxpp4y8f") (r "1.60.0")))

(define-public crate-chrono-tz-postgres-0.1.2 (c (n "chrono-tz-postgres") (v "0.1.2") (d (list (d (n "chrono-tz") (r "^0.7") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nhrrkmszkh807q77gpv35g78y73ils1bvi1jy2hg3qbjhyyzjf6") (r "1.60.0")))

(define-public crate-chrono-tz-postgres-0.1.3 (c (n "chrono-tz-postgres") (v "0.1.3") (d (list (d (n "chrono-tz") (r "^0.8") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0py6qjismvzisa1fkh27mz5yk4qz1mmfks32szrmfi1p3r2z6iq5") (r "1.60.0")))

(define-public crate-chrono-tz-postgres-0.8.1 (c (n "chrono-tz-postgres") (v "0.8.1") (d (list (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sgr4v89m2yvkcihxplzak2vazf7qm91jj2bnag0nmfwq5p6c5ps") (r "1.60.0")))

