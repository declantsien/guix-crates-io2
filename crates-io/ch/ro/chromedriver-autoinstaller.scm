(define-module (crates-io ch ro chromedriver-autoinstaller) #:use-module (crates-io))

(define-public crate-chromedriver-autoinstaller-0.1.0 (c (n "chromedriver-autoinstaller") (v "0.1.0") (d (list (d (n "is_executable") (r "^1") (d #t) (k 0)) (d (n "quick-xml") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)) (d (n "zip") (r "^0") (d #t) (k 0)))) (h "1c9068fq5wax3214s49mp8pg9ailynhyiyssj3s71309v9z39xpk") (y #t)))

