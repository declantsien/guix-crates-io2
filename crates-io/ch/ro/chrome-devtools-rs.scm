(define-module (crates-io ch ro chrome-devtools-rs) #:use-module (crates-io))

(define-public crate-chrome-devtools-rs-0.0.0-alpha.0 (c (n "chrome-devtools-rs") (v "0.0.0-alpha.0") (d (list (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lnphw08yzmc72fc3fis2yb26jslh6rig7xd3ah848vbmnhvisfy") (f (quote (("color"))))))

(define-public crate-chrome-devtools-rs-0.0.0-alpha.1 (c (n "chrome-devtools-rs") (v "0.0.0-alpha.1") (d (list (d (n "console") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unescape") (r "^0.1") (d #t) (k 0)))) (h "06q6jg5yrv92zm590qb4s58n20sg95mb3rdzd5d1rvi6wakavl8p") (f (quote (("color"))))))

(define-public crate-chrome-devtools-rs-0.0.0-alpha.2 (c (n "chrome-devtools-rs") (v "0.0.0-alpha.2") (d (list (d (n "console") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unescape") (r "^0.1") (d #t) (k 0)))) (h "1r3qpw9aay92kzdwpayn0y91z6f2icf2ir6jjy9q2w4zfp29v9r6") (f (quote (("color"))))))

(define-public crate-chrome-devtools-rs-0.0.0-alpha.3 (c (n "chrome-devtools-rs") (v "0.0.0-alpha.3") (d (list (d (n "console") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unescape") (r "^0.1") (d #t) (k 0)))) (h "05iqqcp6q3xmricjlzjsbp42im6cxg1rdf43dkyzdawa16l721d2") (f (quote (("color"))))))

