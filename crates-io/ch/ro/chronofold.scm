(define-module (crates-io ch ro chronofold) #:use-module (crates-io))

(define-public crate-chronofold-0.1.0 (c (n "chronofold") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "1sb1x0090nlb4421xaqdd9mmc2ji611cs925mzas6js69lpxi4kz")))

(define-public crate-chronofold-0.2.0 (c (n "chronofold") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ziahvf6y2sr18vhv8v8p8ilcvgfd4k93qar5sv7a1zir5nlfdw7")))

(define-public crate-chronofold-0.2.1 (c (n "chronofold") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "17c67gz7g55q4syib3b74467nf4pi15f07a4h8730yifkz8lwnp8")))

(define-public crate-chronofold-0.3.0 (c (n "chronofold") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1s5c4m1bggivnqb3lxr9xqih1s1nksbm28fg43pc9hdm3q8pban9")))

(define-public crate-chronofold-0.4.0 (c (n "chronofold") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "151mfwybqzpaynn234khjkqapn7j7zcwk4m95s3m7anw6xysqsi0")))

