(define-module (crates-io ch ro chrono_parser) #:use-module (crates-io))

(define-public crate-chrono_parser-0.1.0 (c (n "chrono_parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0wp686gsyz74g8vh1bi14zxz3y5dfz2vlpmandl74vvgi9xk8yh1")))

