(define-module (crates-io ch ro chroma-rust) #:use-module (crates-io))

(define-public crate-chroma-rust-0.1.0 (c (n "chroma-rust") (v "0.1.0") (h "0pjj1k41j4m55f3wpcp8n8azph9hzakncm49a89bvdmwz8mxgzb7")))

(define-public crate-chroma-rust-0.1.1 (c (n "chroma-rust") (v "0.1.1") (h "15ryam2h3sq58k2lybbx49s8vdwkcr5q79cwwq730a0nxmsg0nxk")))

(define-public crate-chroma-rust-0.1.2 (c (n "chroma-rust") (v "0.1.2") (h "043jyrw3hslw4nnzrhmwghv06ssxqxrbz71b2pp9jhw31f14kih3")))

(define-public crate-chroma-rust-0.1.3 (c (n "chroma-rust") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "04d611vzxib1ldi5585xxgk7frk4089ws4fln1fxmkf2z8a8rzz6")))

(define-public crate-chroma-rust-0.1.4 (c (n "chroma-rust") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1gnyip79x23vmicbvz7n9qrwmck7380y41ahg8c0c1fn3a0grhxq")))

(define-public crate-chroma-rust-0.1.5 (c (n "chroma-rust") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1d7cl8zd8r9d9fnkzax64n5mgqcs756vk4pqkpb591pk0q7c4ggc")))

(define-public crate-chroma-rust-0.1.6 (c (n "chroma-rust") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0386sq2wv7vvs56y3xmx4ds6qdyj8diwqwf87phb4g7fvwylfpw0")))

