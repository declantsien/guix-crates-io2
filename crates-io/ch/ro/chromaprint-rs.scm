(define-module (crates-io ch ro chromaprint-rs) #:use-module (crates-io))

(define-public crate-chromaprint-rs-0.1.0 (c (n "chromaprint-rs") (v "0.1.0") (d (list (d (n "chromaprint_sys") (r "^1.0.1") (d #t) (k 0)) (d (n "claxon") (r "^0.4.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 2)))) (h "00r3y0xjj79hvm8c8xpy8xq9vkblx51y25v65nsa3f6l7q0b4jrp")))

