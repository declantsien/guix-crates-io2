(define-module (crates-io ch ro chrobry-core) #:use-module (crates-io))

(define-public crate-chrobry-core-1.0.0 (c (n "chrobry-core") (v "1.0.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "17whmvvwk2ckqjykagrw3s3hcsyi1rg2byvj17nbfq8p0gmiz6b4")))

(define-public crate-chrobry-core-1.1.0 (c (n "chrobry-core") (v "1.1.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1amya2s5mll8xpbqs8xsv3yp0vn7awrrg8irvp56p5yxl62zl16g")))

(define-public crate-chrobry-core-1.1.1 (c (n "chrobry-core") (v "1.1.1") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0hc8r1cwmmi7mnydwx4j869sfrm4ddzhrrm19wxmwgl9jgnin4xd")))

