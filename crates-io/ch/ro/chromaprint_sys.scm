(define-module (crates-io ch ro chromaprint_sys) #:use-module (crates-io))

(define-public crate-chromaprint_sys-1.0.0 (c (n "chromaprint_sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (o #t) (d #t) (k 1)))) (h "092wl0hlgggq6h8r28j743hsrpa6snxprbjzdckcikv2ldwayirh") (f (quote (("static" "cmake") ("dynamic" "metadeps") ("default" "dynamic")))) (y #t)))

(define-public crate-chromaprint_sys-1.0.1 (c (n "chromaprint_sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (o #t) (d #t) (k 1)))) (h "1gi93hf0x34ipa8f66mkkz3rwsm7r6rphrrqqx4rzkp4s16cc2jr") (f (quote (("static" "cmake") ("dynamic" "metadeps") ("default" "dynamic")))) (y #t)))

