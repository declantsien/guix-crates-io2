(define-module (crates-io ch ro chronobreak) #:use-module (crates-io))

(define-public crate-chronobreak-0.0.0 (c (n "chronobreak") (v "0.0.0") (h "1xp0if35bmwcwgm3bvilw3yhfky5gkjpg5sdi4pg5hk9p7fj2gni")))

(define-public crate-chronobreak-0.1.0 (c (n "chronobreak") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "chronobreak_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (o #t) (d #t) (k 0)) (d (n "futures-timer") (r "^3.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1h0nh6gag5w2dmqmw22k1ikdy6sj2q3mgg94vh3gq11wsg5dahng") (f (quote (("extended-apis") ("default" "futures" "futures-timer" "parking_lot"))))))

