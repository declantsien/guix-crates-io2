(define-module (crates-io ch ro chrono-utilities) #:use-module (crates-io))

(define-public crate-chrono-utilities-0.0.0-alpha1 (c (n "chrono-utilities") (v "0.0.0-alpha1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "time") (r "^0.1.43") (d #t) (k 0)))) (h "12ch4si5vrb0rszqq5z8sdb4drqximxmf2dnkgpp32zzxq7gk456")))

