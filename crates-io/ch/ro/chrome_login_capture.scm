(define-module (crates-io ch ro chrome_login_capture) #:use-module (crates-io))

(define-public crate-chrome_login_capture-0.1.0 (c (n "chrome_login_capture") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "03y9iikxrz5qvrkfcrnalzj4ggnk6lq2wfsgqpghs3v9zqvjaj9g")))

(define-public crate-chrome_login_capture-0.1.1 (c (n "chrome_login_capture") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0pqwphc9g1q59wam95gwnd4p2skm35yk4kssvjgj61778vbsqxnk")))

