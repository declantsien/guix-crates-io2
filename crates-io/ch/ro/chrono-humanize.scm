(define-module (crates-io ch ro chrono-humanize) #:use-module (crates-io))

(define-public crate-chrono-humanize-0.0.0 (c (n "chrono-humanize") (v "0.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "1gdrc97q4c3ppp4kycz8sl3ca47dv0prhdj279p77n2k3819ycz9")))

(define-public crate-chrono-humanize-0.0.2 (c (n "chrono-humanize") (v "0.0.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "02grcw9xs85fm5p2chmah3l45gpw3wzjbl1ipd03wr641l94fvhi")))

(define-public crate-chrono-humanize-0.0.3 (c (n "chrono-humanize") (v "0.0.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "17mmrgaprq969qn7f0prm9jd0dhk5r6hn4h12h1wdvxgvb1bshnl")))

(define-public crate-chrono-humanize-0.0.4 (c (n "chrono-humanize") (v "0.0.4") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "1zvmd8zhfy67i28736sh5am51pm4m1j6vyf1nsrxy3q7zqj9qjw8")))

(define-public crate-chrono-humanize-0.0.5 (c (n "chrono-humanize") (v "0.0.5") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "1jb620j0s0bdgkny70zqfyh2gzpw9df0jha06fxh9qi0as4czlza")))

(define-public crate-chrono-humanize-0.0.6 (c (n "chrono-humanize") (v "0.0.6") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "0kysk2jwywhys2s6a4i0bjacss8l1b9plx1wazny9c40c91v3bwj")))

(define-public crate-chrono-humanize-0.0.7 (c (n "chrono-humanize") (v "0.0.7") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)))) (h "0lv3avv0mbbwimbbpswyv6qmbn9y2qzgiyl4bz0sj6avwn367w6d")))

(define-public crate-chrono-humanize-0.0.8 (c (n "chrono-humanize") (v "0.0.8") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)))) (h "18447y8l9nnlvqacw4dgsdrmgq18820nnwkpmimyc46sb14savxf")))

(define-public crate-chrono-humanize-0.0.9 (c (n "chrono-humanize") (v "0.0.9") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)))) (h "1qq1vqxnm4mqnhwx04dysk7hwic2g76qhhyjvsi6rh9hyph8mjas")))

(define-public crate-chrono-humanize-0.0.10 (c (n "chrono-humanize") (v "0.0.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0c8cgfics6f7vqhbsg4sh3sghxf8hrimsk54rkcksc3n02mw3w1p")))

(define-public crate-chrono-humanize-0.0.11 (c (n "chrono-humanize") (v "0.0.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "03g9nnl9cmf249sf5i78g6l35zyqgypnd7m3x7dd5s2zcn5g8bzb") (f (quote (("pedantic"))))))

(define-public crate-chrono-humanize-0.1.0 (c (n "chrono-humanize") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1w6hdzsvvq5ap4l7bspj5rcsdx4lcpiyjlnyd08yvsy43b0kfjvj") (f (quote (("pedantic"))))))

(define-public crate-chrono-humanize-0.1.1 (c (n "chrono-humanize") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1j9kmy7fl13bmx5zix94yjfpd4wl9xd14asg3kz8bnxl8lhw797h") (f (quote (("pedantic"))))))

(define-public crate-chrono-humanize-0.1.2 (c (n "chrono-humanize") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "077pymca85qbs1vck2yfdliwsg982c1bgbij3zvlzw5si4qawr41") (f (quote (("wasmbind" "chrono/wasmbind") ("pedantic"))))))

(define-public crate-chrono-humanize-0.2.0 (c (n "chrono-humanize") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0jhysg45adrsfdb7gl15ija8fdrqc3z0xihvlrwbf29pmkyy60ya") (f (quote (("wasmbind" "chrono/wasmbind") ("pedantic"))))))

(define-public crate-chrono-humanize-0.2.1 (c (n "chrono-humanize") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1jvm6fcs3zvz4kcqkp9q5hjy0i2zc17194nb63ckyn0xa0cw3p9f") (f (quote (("wasmbind" "chrono/wasmbind") ("pedantic"))))))

(define-public crate-chrono-humanize-0.2.2 (c (n "chrono-humanize") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0vh9wrpg52mcs82hiv1ll793nli8b1113zqmk2gxznw837mf3p1j") (f (quote (("wasmbind" "chrono/wasmbind") ("pedantic"))))))

(define-public crate-chrono-humanize-0.2.3 (c (n "chrono-humanize") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "std"))) (k 0)))) (h "0fq25fcdqd7s39dx81hq123210q4lpcbjdz82jl2fy6jnkk2g5kr") (f (quote (("wasmbind" "chrono/wasmbind") ("pedantic"))))))

