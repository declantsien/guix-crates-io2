(define-module (crates-io ch ro chrome_native_messaging) #:use-module (crates-io))

(define-public crate-chrome_native_messaging-0.1.0 (c (n "chrome_native_messaging") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1dbgzag2xs19kqc38hcmx4h9j82jc20311dballh211yl004gigv")))

(define-public crate-chrome_native_messaging-0.1.1 (c (n "chrome_native_messaging") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0skhwpgrirhkx9sg7npkqh9gj2j7xr8lmmv220xsxg24n79khpq3")))

(define-public crate-chrome_native_messaging-0.1.2 (c (n "chrome_native_messaging") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.24") (d #t) (k 2)) (d (n "serde_derive") (r "~1.0.24") (d #t) (k 2)) (d (n "serde_json") (r "~1.0.2") (d #t) (k 0)))) (h "1pq0vw70603pn0ky0xf6jidw2w7a5jijgzhhgnkz1jjn18rz03nr")))

(define-public crate-chrome_native_messaging-0.2.0 (c (n "chrome_native_messaging") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1p6w2c8g3x4s5wppvs22dpm9af5g2ndzpshv07d7g7pcbcfyian1")))

(define-public crate-chrome_native_messaging-0.3.0 (c (n "chrome_native_messaging") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0k5kq1jrm9gyw5llh4k5s9v60lzbcnqirffs3bfxw1qx58c3hmq4")))

