(define-module (crates-io ch ro chronoutil) #:use-module (crates-io))

(define-public crate-chronoutil-0.1.1 (c (n "chronoutil") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0rb22hgbl9f95ywvmsgq5riqwbrnfjmalsky9qvj0nnmlf01x05a")))

(define-public crate-chronoutil-0.1.2 (c (n "chronoutil") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1ijizrbp74z5a90qqncmndkwrvhrxr5miqgnkgimiqn440knwlg0")))

(define-public crate-chronoutil-0.2.0 (c (n "chronoutil") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1a0yp0kpgp3g2p1mmivp1qw6dgwa3n15b910ka8cd8drsds34ij7")))

(define-public crate-chronoutil-0.2.1 (c (n "chronoutil") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1nq98cgn43zvra49a58js43iiy3dnxfyp8ch2qq0jgicmlhfzrvg")))

(define-public crate-chronoutil-0.2.2 (c (n "chronoutil") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0m04sjs33bhbj5j5awfmb3jng678l0gqavrd2wp8ahlbvrgk2n1r")))

(define-public crate-chronoutil-0.2.3 (c (n "chronoutil") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1zc33vbn7p93kk43nvafqrsh5dj6ihqgbb533lhalwmp9f98r9a3")))

(define-public crate-chronoutil-0.2.4 (c (n "chronoutil") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.24") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1shb9qjzlwnxzywybinylaw0lcr67aawjd1fy5rw0lh28z1r0pyf")))

(define-public crate-chronoutil-0.2.5 (c (n "chronoutil") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4.24") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1mx3p0lz420hjgp4277x6xw1jc8nwjf1xz3a8vlm27cq7hjsajhm")))

(define-public crate-chronoutil-0.2.6 (c (n "chronoutil") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4.24") (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "059pzilcvbkv7v6mj151sc4v0nldiv9kq0n5wjk6hq8f5ff1qlxa")))

(define-public crate-chronoutil-0.2.7 (c (n "chronoutil") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4.34") (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)))) (h "0hs53fq9v41dsnqhlpj413ykmkgiw2jyqw5j0aaxmbbwlq3qpdf9")))

