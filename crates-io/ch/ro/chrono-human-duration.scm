(define-module (crates-io ch ro chrono-human-duration) #:use-module (crates-io))

(define-public crate-chrono-human-duration-0.1.0 (c (n "chrono-human-duration") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "1xjs4xzr2wnfkqcjl83ifh8gchkwwqcnp9aw1bkj321ah4wyi34w")))

(define-public crate-chrono-human-duration-0.1.1 (c (n "chrono-human-duration") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "025rq7vzd60h395rbznd3ck3r7cky3bjmr38cppdxps4l7rksa0r")))

