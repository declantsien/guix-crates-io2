(define-module (crates-io ch ro chronicles) #:use-module (crates-io))

(define-public crate-chronicles-0.0.1 (c (n "chronicles") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "xz2") (r "^0.1.7") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0197bzz64gj3d2l48f0532y6zhxi396832a198w5n99kc77rhk2l")))

