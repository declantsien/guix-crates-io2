(define-module (crates-io ch ro chronicle) #:use-module (crates-io))

(define-public crate-chronicle-0.0.0 (c (n "chronicle") (v "0.0.0") (h "03xwb0ixlajw69xzswyznsxcazzbxqxprf64amjc76wp2hr81g1i") (y #t)))

(define-public crate-chronicle-0.1.0 (c (n "chronicle") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0av6ifw7c4hbd1bfq7h8hq2x7nh3jqch04s868b04224knkidffa")))

