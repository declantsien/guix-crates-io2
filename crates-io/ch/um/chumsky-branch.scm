(define-module (crates-io ch um chumsky-branch) #:use-module (crates-io))

(define-public crate-chumsky-branch-0.1.0 (c (n "chumsky-branch") (v "0.1.0") (d (list (d (n "chumsky") (r "^0.8") (d #t) (k 0)))) (h "06szqmlg9knb2yzv827p8vm131qg84ri6vzdn21b1ll89fabxilf") (y #t)))

(define-public crate-chumsky-branch-0.1.1 (c (n "chumsky-branch") (v "0.1.1") (d (list (d (n "chumsky") (r "^0.8") (d #t) (k 0)))) (h "160jy24897z9srdvpiflh0g1n2kck6ar8xfj4almx9hda8arm541")))

(define-public crate-chumsky-branch-0.2.0 (c (n "chumsky-branch") (v "0.2.0") (d (list (d (n "chumsky") (r "^0.9") (d #t) (k 0)))) (h "19063sw6m3xwzpw7b2jhd3l5yjydvr02ci9lyszpl3j7j4hqchs5")))

