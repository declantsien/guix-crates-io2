(define-module (crates-io ch ea cheats) #:use-module (crates-io))

(define-public crate-cheats-0.1.0 (c (n "cheats") (v "0.1.0") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1avz6q11d93dkafbknskxpkxq7dsw9aczi3sbvl7vbx0c6kavhax")))

(define-public crate-cheats-0.2.0 (c (n "cheats") (v "0.2.0") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "0ycv9mika6wn3ism0c6z6cw7ibwkh84vyx6ykmsnrcfkx5i8pdak")))

(define-public crate-cheats-0.2.1 (c (n "cheats") (v "0.2.1") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1whqgkvz20ckppjsz2xyx8hj3f245wzkyhgymp6b41fra87jm3zs")))

(define-public crate-cheats-0.3.0 (c (n "cheats") (v "0.3.0") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "070a2wpwa0xzv4f3fq2np2zsz21r34fdp7m7i16r1xf77rgqahpj")))

(define-public crate-cheats-0.3.1 (c (n "cheats") (v "0.3.1") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "0a68ffvqqx60dggzl6sdfvzp1awjznvvd22c7p36pd43njyna6sq")))

(define-public crate-cheats-0.4.0 (c (n "cheats") (v "0.4.0") (d (list (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "rstest") (r "^0.6.4") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "10s49w26mkyg7sg08kc8v51wqgkv1cjw7l9ifgkxd0ccrq1pqam5")))

