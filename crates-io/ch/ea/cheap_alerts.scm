(define-module (crates-io ch ea cheap_alerts) #:use-module (crates-io))

(define-public crate-cheap_alerts-0.1.0 (c (n "cheap_alerts") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lettre") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "07y3zhvgmh9pcsyn3qq69ddm804n04xxy4qbc535mpn8nmcm4whj")))

