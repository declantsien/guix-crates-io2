(define-module (crates-io ch ea cheapskate-ci) #:use-module (crates-io))

(define-public crate-cheapskate-ci-0.1.0 (c (n "cheapskate-ci") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "psst") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "06iq7gvf9hmik8if3bhjl0bk7sxdkw0mr5sjk2hmvyw2lbbims1i")))

(define-public crate-cheapskate-ci-0.1.1 (c (n "cheapskate-ci") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "psst") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0hfigxfdbypaq5rl903ykavm7bjxgmnnmcfb58c9ckj67fqhai30")))

(define-public crate-cheapskate-ci-0.1.2 (c (n "cheapskate-ci") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "psst") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1h3nnminwrb64n43zqf90zx3mjrx01q37njg6dks5xhhqjkk4lvb")))

(define-public crate-cheapskate-ci-0.1.3 (c (n "cheapskate-ci") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "psst") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0djh3iviavbyi6j782x9xrnln5mn906i9xijyyl3s9anrfn7nb2l")))

(define-public crate-cheapskate-ci-0.1.4 (c (n "cheapskate-ci") (v "0.1.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "psst") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0v4587cgx5k2i6g1h081n555x2yr996yqbj1mpi4cbxixg4fbzkb")))

(define-public crate-cheapskate-ci-0.1.5 (c (n "cheapskate-ci") (v "0.1.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "psst") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0dszrh3b92gar3gdrbwvyr2zcwjwrjcggidpfins1ybfp3sqqk5v")))

(define-public crate-cheapskate-ci-0.1.6 (c (n "cheapskate-ci") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "psst") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0m4w9xlwvahs1ykcd72vjn3bjhj95qnkdmca8wd1jhzfqbqxy7pr")))

(define-public crate-cheapskate-ci-0.1.7 (c (n "cheapskate-ci") (v "0.1.7") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "psst") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0z2rpajnf3iw1c0z6rplnzkbr8a86zp9lnas5nhd1hrq9brql9y7")))

(define-public crate-cheapskate-ci-0.1.8 (c (n "cheapskate-ci") (v "0.1.8") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "psst") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.30.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0s0sh1bfyz1kpx4l8w79kbkmwxrmdyi9mwc3ks5l8c56fkmmqv3z")))

