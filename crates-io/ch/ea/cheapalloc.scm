(define-module (crates-io ch ea cheapalloc) #:use-module (crates-io))

(define-public crate-cheapalloc-0.1.0 (c (n "cheapalloc") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "11lpyvyxfmkwg6hpgafyqdcqjkivwf1y10z180fa77sxsqf9raql") (f (quote (("provide_oom" "panic-halt" "cortex-m") ("default"))))))

