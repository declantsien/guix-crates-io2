(define-module (crates-io ch ks chksm) #:use-module (crates-io))

(define-public crate-chksm-0.1.0 (c (n "chksm") (v "0.1.0") (h "0nmqqravz7cny66s34r1fj98wjr1z8mpw7q6k7dyvzqzgnd92p7k")))

(define-public crate-chksm-0.2.0 (c (n "chksm") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pj4kdbs082f8kdgwpsjm3dvi5n3c3wwcz11fkfix8xjvgcw4mxp") (f (quote (("logging" "chrono"))))))

(define-public crate-chksm-0.2.1 (c (n "chksm") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hyf2y5cl3ac3vfizsmn92r050k5qiml8vnd29h83clvdklramdz") (f (quote (("logging" "chrono"))))))

(define-public crate-chksm-0.2.2 (c (n "chksm") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m6ry92zj61nd26q4a9ln60wjs7fmd5xqp1pcwzrlf0w78zmld8c") (f (quote (("net") ("logging" "chrono") ("default" "net" "crypto") ("crypto"))))))

(define-public crate-chksm-0.2.3 (c (n "chksm") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14x8d19dfbibnj0f3wz2g0gk5c495zdrawlhsxp4jdxzjkdrlqfc") (f (quote (("net") ("logging" "chrono") ("default" "net" "crypto") ("crypto"))))))

(define-public crate-chksm-0.2.4 (c (n "chksm") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07iagz07kz88ljkbzgsrsl07gjqgzn5j2vgwhnfvb0d87k895j9h") (f (quote (("net") ("logging" "chrono") ("default" "net" "crypto") ("crypto"))))))

(define-public crate-chksm-0.4.1 (c (n "chksm") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hs53bcgkz0j5kdwc0611dw25af347z2vqq34m6za30k31lzr0wl") (f (quote (("net") ("logging" "chrono") ("default" "net" "crypto") ("crypto"))))))

(define-public crate-chksm-0.4.3 (c (n "chksm") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0g9d9cpcvx8xjqbv2y3b0fffm9fk3dlh3wlxh6lwxl9hr0z1sjqf") (f (quote (("net") ("logging" "chrono") ("default" "net" "crypto") ("crypto"))))))

(define-public crate-chksm-0.4.4 (c (n "chksm") (v "0.4.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bmjank7v4xav97fp7bszixwd269a3rcmkzl1afidq3xg4kg8g7p") (f (quote (("net") ("logging" "chrono") ("default" "net" "crypto") ("crypto"))))))

