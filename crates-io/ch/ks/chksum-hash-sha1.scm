(define-module (crates-io ch ks chksum-hash-sha1) #:use-module (crates-io))

(define-public crate-chksum-hash-sha1-0.0.0 (c (n "chksum-hash-sha1") (v "0.0.0") (d (list (d (n "chksum-hash-core") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1ad1m9b2bqyc93yr2n863dh1lmrjrjw4nvg5rpqr02aa4j343bk2") (r "1.63.0")))

(define-public crate-chksum-hash-sha1-0.0.1 (c (n "chksum-hash-sha1") (v "0.0.1") (d (list (d (n "arbitrary") (r "^1.3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "chksum-hash-core") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1kdb7y96qgzv2q3hpf7dk3jyh1j7f31fximmqc2r6jjmg6fdbkhw") (f (quote (("fuzzing" "arbitrary") ("default")))) (r "1.63.0")))

