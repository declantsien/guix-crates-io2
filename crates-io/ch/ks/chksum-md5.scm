(define-module (crates-io ch ks chksum-md5) #:use-module (crates-io))

(define-public crate-chksum-md5-0.0.0 (c (n "chksum-md5") (v "0.0.0") (d (list (d (n "assert_fs") (r "^1.0.13") (f (quote ("color-auto"))) (d #t) (k 2)) (d (n "chksum-core") (r "^0.0.0") (d #t) (k 0)) (d (n "chksum-hash-md5") (r "^0.0.0") (d #t) (k 0)) (d (n "chksum-reader") (r "^0.0.0") (o #t) (d #t) (k 0)) (d (n "chksum-writer") (r "^0.0.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 2)))) (h "0la3cc9vkqczykspazmwxasf31kh8llajw638bh6jq5vdzvs1pcm") (f (quote (("writer" "chksum-writer") ("reader" "chksum-reader") ("default")))) (r "1.70.0")))

