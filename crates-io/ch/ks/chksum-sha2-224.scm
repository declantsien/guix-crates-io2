(define-module (crates-io ch ks chksum-sha2-224) #:use-module (crates-io))

(define-public crate-chksum-sha2-224-0.0.0 (c (n "chksum-sha2-224") (v "0.0.0") (d (list (d (n "assert_fs") (r "^1.0.13") (f (quote ("color-auto"))) (d #t) (k 2)) (d (n "chksum-core") (r "^0.0.0") (d #t) (k 0)) (d (n "chksum-hash-sha2-224") (r "^0.0.0") (d #t) (k 0)) (d (n "chksum-reader") (r "^0.0.0") (o #t) (d #t) (k 0)) (d (n "chksum-writer") (r "^0.0.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 2)))) (h "07z3bahn33mlb1x76adzsx36mxw40p2vpgpvdilwrryg213k8w6c") (f (quote (("writer" "chksum-writer") ("reader" "chksum-reader") ("default")))) (r "1.70.0")))

