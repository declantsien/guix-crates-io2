(define-module (crates-io ch ks chksum-hash-sha2-384) #:use-module (crates-io))

(define-public crate-chksum-hash-sha2-384-0.0.0 (c (n "chksum-hash-sha2-384") (v "0.0.0") (d (list (d (n "chksum-hash-core") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1ypwpx33xd6x5mydwp8ad0rm6jdpwi0fhma6k7wxzzprw1kvwzri") (r "1.63.0")))

(define-public crate-chksum-hash-sha2-384-0.0.1 (c (n "chksum-hash-sha2-384") (v "0.0.1") (d (list (d (n "arbitrary") (r "^1.3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "chksum-hash-core") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1z76n0hxlxa95cdi11q8fwnmhlnm057631pmgp5gmcmy9gkf6mhx") (f (quote (("fuzzing" "arbitrary") ("default")))) (r "1.63.0")))

