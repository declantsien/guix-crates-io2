(define-module (crates-io ch ks chksum-hash-sha2-224) #:use-module (crates-io))

(define-public crate-chksum-hash-sha2-224-0.0.0 (c (n "chksum-hash-sha2-224") (v "0.0.0") (d (list (d (n "chksum-hash-core") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0i2bhslvl7ndpvmkz6wxqjaaf9xxq153hyk7v331hcj3w9d20jpb") (r "1.63.0")))

(define-public crate-chksum-hash-sha2-224-0.0.1 (c (n "chksum-hash-sha2-224") (v "0.0.1") (d (list (d (n "arbitrary") (r "^1.3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "chksum-hash-core") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1c6vqlfmvn27x5gf7x9gb8bb4f7x5zlnf0qsjx9fhj6w01311h33") (f (quote (("fuzzing" "arbitrary") ("default")))) (r "1.63.0")))

