(define-module (crates-io ch ks chksum-config) #:use-module (crates-io))

(define-public crate-chksum-config-0.1.0-rc1 (c (n "chksum-config") (v "0.1.0-rc1") (h "04y4xwqb36s7qlwzpyv26dd1gfkghpcs6dr62awal09y30m3qcp6") (y #t) (r "1.58.0")))

(define-public crate-chksum-config-0.1.0-rc2 (c (n "chksum-config") (v "0.1.0-rc2") (h "0vw8dhxnvapgmsd1aizqsf0mmdgjlb9a7hhcr9rmmgd3yar10wkc") (y #t) (r "1.58.0")))

(define-public crate-chksum-config-0.1.0-rc3 (c (n "chksum-config") (v "0.1.0-rc3") (h "1janpp7psnmknygxckdzdhzi6wa5qwp3r76f6a61prmndk7640qm") (y #t) (r "1.58.0")))

