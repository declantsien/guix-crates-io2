(define-module (crates-io ch ks chksum-arch) #:use-module (crates-io))

(define-public crate-chksum-arch-0.1.0-rc1 (c (n "chksum-arch") (v "0.1.0-rc1") (d (list (d (n "chksum-traits") (r "^0.1.0-rc1") (f (quote ("std"))) (k 0)))) (h "04qwrd7r5542jygcinihqd88rdyl7f1715gbndsx43mmm34b2kf7") (f (quote (("simd") ("default")))) (y #t) (r "1.58.0")))

(define-public crate-chksum-arch-0.1.0-rc2 (c (n "chksum-arch") (v "0.1.0-rc2") (d (list (d (n "chksum-traits") (r "^0.1.0-rc2") (f (quote ("std"))) (k 0)))) (h "1zb5vjksyjfq36zy12v3cwvilkjlqq7kh99q782r77n7jq6y6ry8") (f (quote (("simd") ("default")))) (y #t) (r "1.58.0")))

(define-public crate-chksum-arch-0.1.0-rc3 (c (n "chksum-arch") (v "0.1.0-rc3") (d (list (d (n "chksum-traits") (r "^0.1.0-rc3") (f (quote ("std"))) (k 0)))) (h "0gnb6r68w7gkb3hzcm1jnacjv667rksxzaavcp7y7qrbqn2h0p9k") (f (quote (("simd") ("default")))) (y #t) (r "1.58.0")))

