(define-module (crates-io ch ks chksum-sync) #:use-module (crates-io))

(define-public crate-chksum-sync-0.1.0-rc1 (c (n "chksum-sync") (v "0.1.0-rc1") (d (list (d (n "chksum-config") (r "^0.1.0-rc1") (k 0)) (d (n "chksum-hash") (r "^0.1.0-rc1") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nclh0y60kywph8cld9sv312k74dqc0g280i6n0xycj7daxxcw27") (y #t) (r "1.58.0")))

(define-public crate-chksum-sync-0.1.0-rc2 (c (n "chksum-sync") (v "0.1.0-rc2") (d (list (d (n "chksum-config") (r "^0.1.0-rc2") (k 0)) (d (n "chksum-hash") (r "^0.1.0-rc2") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "049kjw3gwgy48vla01gyvzzck7i5ak1xq9qhcg7z1wpbvjbsd1x5") (y #t) (r "1.58.0")))

(define-public crate-chksum-sync-0.1.0-rc3 (c (n "chksum-sync") (v "0.1.0-rc3") (d (list (d (n "chksum-config") (r "^0.1.0-rc3") (k 0)) (d (n "chksum-hash") (r "^0.1.0-rc3") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v8sa2bvwxz4vkn55lch95askh4mb6gfibsdxi4s5n2lrg33jdw1") (y #t) (r "1.58.0")))

