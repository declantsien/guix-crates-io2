(define-module (crates-io ch ks chksum-traits) #:use-module (crates-io))

(define-public crate-chksum-traits-0.1.0-rc1 (c (n "chksum-traits") (v "0.1.0-rc1") (h "11nkjfpbpiy7c6vfpk0y7ymjh64ycwyvirag2qs025cbi6dz96d2") (f (quote (("std") ("default" "std")))) (y #t) (r "1.58.0")))

(define-public crate-chksum-traits-0.1.0-rc2 (c (n "chksum-traits") (v "0.1.0-rc2") (h "1jqpcvn41y0njinvrhas20vi89s7hnj542wrwj6l3ynifzsfvi7n") (f (quote (("std") ("default" "std")))) (y #t) (r "1.58.0")))

(define-public crate-chksum-traits-0.1.0-rc3 (c (n "chksum-traits") (v "0.1.0-rc3") (h "0kybbvcgsxb8yhp6l9714k50p36hypcfv3cvza3dgn0vnplv9m9c") (f (quote (("std") ("default" "std")))) (y #t) (r "1.58.0")))

