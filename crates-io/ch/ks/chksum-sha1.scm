(define-module (crates-io ch ks chksum-sha1) #:use-module (crates-io))

(define-public crate-chksum-sha1-0.0.0 (c (n "chksum-sha1") (v "0.0.0") (d (list (d (n "assert_fs") (r "^1.0.13") (f (quote ("color-auto"))) (d #t) (k 2)) (d (n "chksum-core") (r "^0.0.0") (d #t) (k 0)) (d (n "chksum-hash-sha1") (r "^0.0.0") (d #t) (k 0)) (d (n "chksum-reader") (r "^0.0.0") (o #t) (d #t) (k 0)) (d (n "chksum-writer") (r "^0.0.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 2)))) (h "1p1q5cnm2rp2fqwl71zh1makmijfzyjl5sgrkdjdif3zjjm4f76h") (f (quote (("writer" "chksum-writer") ("reader" "chksum-reader") ("default")))) (r "1.70.0")))

