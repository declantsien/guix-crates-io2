(define-module (crates-io ch ks chksum-hash-sha2-512) #:use-module (crates-io))

(define-public crate-chksum-hash-sha2-512-0.0.0 (c (n "chksum-hash-sha2-512") (v "0.0.0") (d (list (d (n "chksum-hash-core") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0pzf85dvg28c5vbbbr3ly8pqifkqjr9rbbfip7fmyizg3b0wf9ll") (r "1.63.0")))

(define-public crate-chksum-hash-sha2-512-0.0.1 (c (n "chksum-hash-sha2-512") (v "0.0.1") (d (list (d (n "arbitrary") (r "^1.3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "chksum-hash-core") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0p21wjsaip6hnzfhkdrsv1v5h0r6i4fsaxl4aabs9z6qj670h42d") (f (quote (("fuzzing" "arbitrary") ("default")))) (r "1.63.0")))

