(define-module (crates-io ch ks chksum-ffi) #:use-module (crates-io))

(define-public crate-chksum-ffi-0.1.0-rc1 (c (n "chksum-ffi") (v "0.1.0-rc1") (d (list (d (n "chksum") (r "^0.1.0-rc1") (k 0)))) (h "1s1c1dk4mrxqg2aj23mvw803f4ssjd3agwzdy6zpfzsp3jcijvzy") (y #t) (r "1.58.0")))

(define-public crate-chksum-ffi-0.1.0-rc2 (c (n "chksum-ffi") (v "0.1.0-rc2") (d (list (d (n "chksum") (r "^0.1.0-rc2") (k 0)))) (h "1cq74s9r9d64n7vj6nf49803i1wimj8k0yxc3jsw5wjna2r8rwdh") (y #t) (r "1.58.0")))

(define-public crate-chksum-ffi-0.1.0-rc3 (c (n "chksum-ffi") (v "0.1.0-rc3") (d (list (d (n "chksum") (r "^0.1.0-rc3") (k 0)))) (h "1jmn6sz8bhr6vcjza67248n25gikyyclandidrlmq2zw8ai9hyf9") (y #t) (r "1.58.0")))

