(define-module (crates-io ch ks chksum3) #:use-module (crates-io))

(define-public crate-chksum3-0.3.1 (c (n "chksum3") (v "0.3.1") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap_lex") (r "^0.2.0") (d #t) (k 0)) (d (n "wild") (r "^2.0.4") (d #t) (k 0)))) (h "16ygliqfdsbk0a7w4jf2y4q8a5zm64crxjbvgd31wnf21zrgz5rc") (r "1.60")))

(define-public crate-chksum3-0.3.2 (c (n "chksum3") (v "0.3.2") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap_lex") (r "^0.2.0") (d #t) (k 0)) (d (n "wild") (r "^2.0.4") (d #t) (k 0)))) (h "0jbrdqw8vyirljgz14nb1zcqw7ps7riqna79p9xkfbr261mbc9m9") (r "1.60")))

(define-public crate-chksum3-0.4.0 (c (n "chksum3") (v "0.4.0") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap_lex") (r "^0.2.0") (d #t) (k 0)) (d (n "vmap") (r "^0.5.1") (d #t) (k 0)) (d (n "wild") (r "^2.0.4") (d #t) (k 0)))) (h "1f7l3dv43ycdn5ppa09dx1r23vn8wcn1zf7zj7salrbfhhwhk4sh") (r "1.60")))

(define-public crate-chksum3-0.4.1 (c (n "chksum3") (v "0.4.1") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap_lex") (r "^0.2.4") (d #t) (k 0)) (d (n "vmap") (r "^0.5.1") (d #t) (k 0)) (d (n "wild") (r "^2.1.0") (d #t) (k 0)))) (h "056w1s3jjvafihxy4v220fc8m6ycjpl8nlacffcv4mzhgdz6p3m0") (r "1.60")))

(define-public crate-chksum3-0.5.0 (c (n "chksum3") (v "0.5.0") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap_lex") (r "^0.2.4") (d #t) (k 0)) (d (n "wild") (r "^2.1.0") (d #t) (k 0)))) (h "0d488gqlfwgapwrmkx7awfkfsr9j2yxnq1i2lmzrldj3rq3lfvcp") (r "1.60")))

