(define-module (crates-io ch ks chksum-hash-sha2-256) #:use-module (crates-io))

(define-public crate-chksum-hash-sha2-256-0.0.0 (c (n "chksum-hash-sha2-256") (v "0.0.0") (d (list (d (n "chksum-hash-core") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1yc1msf3i69nxij9nnd4fa6imswjjjwg93zafaxzn9fgs0lrp6xr") (r "1.63.0")))

(define-public crate-chksum-hash-sha2-256-0.0.1 (c (n "chksum-hash-sha2-256") (v "0.0.1") (d (list (d (n "arbitrary") (r "^1.3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "chksum-hash-core") (r "^0.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1y72wl4vhlkd6jxqjnf64gvghwq76773zrw9ay1r9d1x2xfbycc2") (f (quote (("fuzzing" "arbitrary") ("default")))) (r "1.63.0")))

