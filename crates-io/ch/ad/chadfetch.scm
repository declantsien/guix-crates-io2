(define-module (crates-io ch ad chadfetch) #:use-module (crates-io))

(define-public crate-chadfetch-0.1.0 (c (n "chadfetch") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "0pn3lb1lx4izxjczd8rxmxllfkj0x0qsyjlmy5cf0fyjn84d2749")))

(define-public crate-chadfetch-0.1.1 (c (n "chadfetch") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "1nc94dcq5g2gv0q6m2m0yq9l2iqcsr6bk4v7dc24hfdzvkbrkkjg")))

(define-public crate-chadfetch-0.2.0 (c (n "chadfetch") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "1dm55hnlnc99k6dirwczl27iglnjgkgvpb90h4avi8nw61hgd9n2")))

(define-public crate-chadfetch-0.2.1 (c (n "chadfetch") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "11mdylg0d78cmnpxhbkrlpsbn664s3afsvg35cdicrc3csw8yn7k")))

(define-public crate-chadfetch-0.2.2 (c (n "chadfetch") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "0dh44zjaxlydbq6rs33cvn1s36sjf01ywrqk54bihnvjmhvdsqn1")))

(define-public crate-chadfetch-0.2.3 (c (n "chadfetch") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "1cb5yd1320yppqad4c16k1gifw9zxn0g0631pxchvrf2axx73y5g")))

(define-public crate-chadfetch-0.2.4 (c (n "chadfetch") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "1dhhy5q2yinwfa5cg85hi828aszgp021akvdxsni0dyyv1yzg8sg")))

(define-public crate-chadfetch-0.2.5 (c (n "chadfetch") (v "0.2.5") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "06j6ba7p7c2bxv9b0brn33jhnczr5dvjgqaqhk3hxsj9sd0fkgf1")))

(define-public crate-chadfetch-0.2.6 (c (n "chadfetch") (v "0.2.6") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "118840dzgd9cz0ihklsragkrkhhbyrxwshkkhc0kpkcf6imqpgwm")))

