(define-module (crates-io ch ad chademo-rs) #:use-module (crates-io))

(define-public crate-chademo-rs-0.1.0 (c (n "chademo-rs") (v "0.1.0") (d (list (d (n "embedded-can") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (o #t) (d #t) (k 0)))) (h "0psrl5wqrs62pjh91algnbslpd84nwvzxd11q71klfj9rp71zjhh") (f (quote (("eh1" "embedded-can") ("eh0" "embedded-hal") ("default" "eh0"))))))

(define-public crate-chademo-rs-0.1.1 (c (n "chademo-rs") (v "0.1.1") (d (list (d (n "embedded-can") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (o #t) (d #t) (k 0)))) (h "13vf9080bjcr4nl1clw1kzrraiyqyfz6xjn1r8mm7pymnf49gm65") (f (quote (("eh1" "embedded-can") ("eh0" "embedded-hal") ("default" "eh0"))))))

