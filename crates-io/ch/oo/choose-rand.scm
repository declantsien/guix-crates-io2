(define-module (crates-io ch oo choose-rand) #:use-module (crates-io))

(define-public crate-choose-rand-0.1.0 (c (n "choose-rand") (v "0.1.0") (d (list (d (n "eq-float") (r "^0.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.6") (d #t) (k 0)))) (h "19rpsw1rspi4a2rz5xmgkq7kjinbnwjr815gqya5hwcbwndcxm8r")))

(define-public crate-choose-rand-0.2.0 (c (n "choose-rand") (v "0.2.0") (d (list (d (n "eq-float") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "weighted_rand") (r "^0.4.1") (d #t) (k 0)))) (h "0jr76y0a28magv5zwi10461avcc5z8bg9jkm34wi697rd30zn2d9") (f (quote (("eq_float") ("default"))))))

