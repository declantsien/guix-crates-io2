(define-module (crates-io ch oo choosen) #:use-module (crates-io))

(define-public crate-choosen-0.0.1 (c (n "choosen") (v "0.0.1") (d (list (d (n "reservoir-sampler") (r "^0.0.1") (d #t) (k 0)))) (h "1x14s8i7c7x6kbgi6a2xm01hjymlydzsxc2vlhb6vmbmpim0pjzm")))

(define-public crate-choosen-0.1.0 (c (n "choosen") (v "0.1.0") (d (list (d (n "reservoir-sampler") (r "^0.0.1") (d #t) (k 0)))) (h "0cii4ibxlzfmyqzjvsx32rzpj4ldgp9wnv02876w66jm60wa0d2c")))

