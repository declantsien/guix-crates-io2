(define-module (crates-io ch oo chooser) #:use-module (crates-io))

(define-public crate-chooser-0.1.0 (c (n "chooser") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "162wg7cpyi7pmnk6rx3kg85aivgb51xw7jcnhd9c89z4xxxn41kb")))

(define-public crate-chooser-0.2.0 (c (n "chooser") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qsgb4qg9limpa0gvqnhbsi006vnhwp5bf50zrwr7308gzgir5jg")))

