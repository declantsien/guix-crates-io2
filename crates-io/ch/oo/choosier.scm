(define-module (crates-io ch oo choosier) #:use-module (crates-io))

(define-public crate-choosier-0.1.0 (c (n "choosier") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "04b498ra50bgdh7f6fkgnp9yqzw71524qpvgh2gm9limzj26409n")))

