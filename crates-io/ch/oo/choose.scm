(define-module (crates-io ch oo choose) #:use-module (crates-io))

(define-public crate-choose-1.1.2 (c (n "choose") (v "1.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0080h3w75skxfsac8qflfhsn1ila92vkw4zpgsmjd99nsh9qhjqp")))

(define-public crate-choose-1.2.0 (c (n "choose") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0qyvvdkyxdf3rbbxczf6d9yd78mir9hiihrmf8gijkmb84rm9c4b")))

(define-public crate-choose-1.3.0 (c (n "choose") (v "1.3.0") (d (list (d (n "backslash") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1wxa1rjnc461scdlvafvv6srxp5sfy1jbdwdbgwhzpzybz6lkdvs")))

(define-public crate-choose-1.3.1 (c (n "choose") (v "1.3.1") (d (list (d (n "backslash") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1hck74ipxq97mvfdvfc1v0dl66hhxms13s4vi6j1pa3q272b41fq")))

(define-public crate-choose-1.3.2 (c (n "choose") (v "1.3.2") (d (list (d (n "backslash") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1xxlv15pj431c9ikjf8912iakdv9py92nmbcz7fazhqvd1c88b3s")))

(define-public crate-choose-1.3.3 (c (n "choose") (v "1.3.3") (d (list (d (n "backslash") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1iq04npk9ln4ky37zkvp84bagcy20a19bgsakd1fpwh8c2nazzrm")))

(define-public crate-choose-1.3.4 (c (n "choose") (v "1.3.4") (d (list (d (n "backslash") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0p14168ha5zg29b13km7j647f4wmddkw1hb3csqnlwxw3li0bc1c")))

