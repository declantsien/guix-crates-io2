(define-module (crates-io ch ur churn) #:use-module (crates-io))

(define-public crate-churn-0.1.0 (c (n "churn") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "axum") (r "^0.6.18") (f (quote ("macros"))) (d #t) (k 0)) (d (n "churn-domain") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "197yr60zwcf8sf966rp2fnw0869w8v45scnb121jwax5ras81vz6")))

