(define-module (crates-io ch ur church-lang) #:use-module (crates-io))

(define-public crate-church-lang-0.1.0 (c (n "church-lang") (v "0.1.0") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.24.1") (d #t) (k 0)))) (h "037m1ikqhxkvv9nlsb9z6dydwj476fcjr5zl1qyqlv05n6ns3iph")))

(define-public crate-church-lang-0.1.1 (c (n "church-lang") (v "0.1.1") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.24.1") (d #t) (k 0)))) (h "1592qnnl0f260grqiv99rq64g9dcs1la21h54hr7gmcirps12hv1")))

