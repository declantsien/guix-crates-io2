(define-module (crates-io ch ap chapter) #:use-module (crates-io))

(define-public crate-chapter-0.1.0 (c (n "chapter") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "intl_pluralrules") (r "^7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-build") (r "^0.12") (d #t) (k 1)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "0bjp7i6sk7js59i8vqcl7mkbv7i7l031qady3qg5irqz65s20ch4") (f (quote (("random" "rand") ("default" "random"))))))

