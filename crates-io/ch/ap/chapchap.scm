(define-module (crates-io ch ap chapchap) #:use-module (crates-io))

(define-public crate-chapchap-0.1.1 (c (n "chapchap") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.10") (d #t) (k 0)) (d (n "psutil") (r "^3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wqb4zj8afdc4i3mj3600mg4hl945yp53zlr5l65i88cv9zb6i2n")))

(define-public crate-chapchap-0.1.2 (c (n "chapchap") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "config") (r "^0.10") (d #t) (k 0)) (d (n "psutil") (r "^3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rd40g5rbk9ynnhrhd71dr9lhi37jghjpx4w9d6b6qm02bipg252")))

(define-public crate-chapchap-0.2.0 (c (n "chapchap") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "config") (r "^0.10") (d #t) (k 0)) (d (n "psutil") (r "^3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i1igmpfrr4ip8pagkh9s8kkbg3v4gvg4yl4p83q4jb9zb5h1015")))

