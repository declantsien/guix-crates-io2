(define-module (crates-io ch ap chap) #:use-module (crates-io))

(define-public crate-chap-2.0.1 (c (n "chap") (v "2.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "0pkavqmym6vwlkhv3nc7vzzkp1cg43ahh50wfn5jhis7m7jwa8cg")))

