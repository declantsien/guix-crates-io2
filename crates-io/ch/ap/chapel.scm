(define-module (crates-io ch ap chapel) #:use-module (crates-io))

(define-public crate-chapel-0.0.1 (c (n "chapel") (v "0.0.1") (h "12wqxhbia1bv0s7xnxrx1and7r7qsb7dqiwpa4zzd73zkyj7hdik")))

(define-public crate-chapel-0.0.2 (c (n "chapel") (v "0.0.2") (h "0bx3mywz4bkg3h62srv73i1xsbal83srgxykvjzpqxgxiiy338y3")))

