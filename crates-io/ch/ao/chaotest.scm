(define-module (crates-io ch ao chaotest) #:use-module (crates-io))

(define-public crate-chaotest-0.0.1 (c (n "chaotest") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1hadadlw0n8gvi590zpq1z9bx0dy676qlh5s2hna72g42awhvlpv")))

(define-public crate-chaotest-0.0.2 (c (n "chaotest") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "19ijps321lzzp0ianpcy7rb99vcgipnhf9563a4p4hn7dg62pidg")))

(define-public crate-chaotest-0.0.3 (c (n "chaotest") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0psi63yj7a1ba9h4h6c76mj6jjb2p2p5dixjcb7vij75pd3g1plc")))

