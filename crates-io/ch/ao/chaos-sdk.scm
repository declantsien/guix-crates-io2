(define-module (crates-io ch ao chaos-sdk) #:use-module (crates-io))

(define-public crate-chaos-sdk-0.1.6 (c (n "chaos-sdk") (v "0.1.6") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fwv7iw3b5nhyj10kh7456w93c83qlnr99q7c3ry6sx719rjxfwb") (f (quote (("default"))))))

