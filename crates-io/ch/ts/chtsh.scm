(define-module (crates-io ch ts chtsh) #:use-module (crates-io))

(define-public crate-chtsh-0.1.0 (c (n "chtsh") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "hyper") (r "^0.14.16") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "053xlgydb4pzfz787mzjkbly4715clyln6h6nrmm48512ilb1mw2")))

(define-public crate-chtsh-0.1.1 (c (n "chtsh") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "hyper") (r "^0.14.16") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gpsbry7hpdynv20nws2q5kpg99r11icwiv958xwbs4jhr14pvmh")))

