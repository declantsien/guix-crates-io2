(define-module (crates-io ch er cherrybomb-oas) #:use-module (crates-io))

(define-public crate-cherrybomb-oas-0.1.0 (c (n "cherrybomb-oas") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0s3ap1q8la69icm3z266482nksckl19nfvjshlvr08qa98kwbpk9")))

(define-public crate-cherrybomb-oas-0.1.2 (c (n "cherrybomb-oas") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "18ydbsnr9snwzc0yqr1ka9sr96axcifaq2pd9ninxrndqb2wdqfq") (f (quote (("default" "colored"))))))

