(define-module (crates-io ch er chert_derive) #:use-module (crates-io))

(define-public crate-chert_derive-0.1.0 (c (n "chert_derive") (v "0.1.0") (d (list (d (n "chert_accessor") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits" "test"))) (d #t) (k 0)))) (h "18cvqcyg720c8m2xp75gryalan779v4i7wi8vlx8dlp79mdlynzp")))

(define-public crate-chert_derive-0.2.0 (c (n "chert_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits" "test"))) (d #t) (k 0)))) (h "0iv62fkxl5ykv90h1qr0ah68805z8bgn2hhqjskmwvwzhhp9pqck")))

