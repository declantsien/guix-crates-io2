(define-module (crates-io ch er cherries) #:use-module (crates-io))

(define-public crate-cherries-0.1.0 (c (n "cherries") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uom") (r "^0.25.0") (f (quote ("autoconvert" "usize" "u8" "u16" "u32" "u64" "u128" "isize" "i8" "i16" "i32" "i64" "i128"))) (d #t) (k 0)))) (h "165zqhkxy2kifhh4sillp097dsh4zx3kr5765g94363cyvp6rj72")))

(define-public crate-cherries-0.1.1 (c (n "cherries") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uom") (r "^0.25.0") (f (quote ("autoconvert" "usize" "u8" "u16" "u32" "u64" "u128" "isize" "i8" "i16" "i32" "i64" "i128"))) (d #t) (k 0)))) (h "01jsqzh2k0vmnslpgaabf1lqpf02w0v5bdf6wj0b57xcrz3jsl5v")))

(define-public crate-cherries-0.2.0 (c (n "cherries") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uom") (r "^0.25.0") (f (quote ("autoconvert" "usize" "u8" "u16" "u32" "u64" "u128" "isize" "i8" "i16" "i32" "i64" "i128"))) (d #t) (k 0)))) (h "0a9av8ls2jpb15dkkgi87vv3a1sq7ma0inf6lnimj2fcg2cvxmyi")))

(define-public crate-cherries-0.2.1 (c (n "cherries") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uom") (r "^0.25.0") (f (quote ("autoconvert" "usize" "u8" "u16" "u32" "u64" "u128" "isize" "i8" "i16" "i32" "i64" "i128"))) (d #t) (k 0)))) (h "1zcm437kl4szx6srncfvy90q60zx09l084yhsy0qgmwlz806vq84")))

(define-public crate-cherries-0.2.2 (c (n "cherries") (v "0.2.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uom") (r "^0.25.0") (f (quote ("autoconvert" "usize" "u8" "u16" "u32" "u64" "u128" "isize" "i8" "i16" "i32" "i64" "i128"))) (d #t) (k 0)))) (h "0j67xaxzj26mm9x7c95l8p795jycr55rfzxjlprfvawdvfk3b357")))

(define-public crate-cherries-0.2.3 (c (n "cherries") (v "0.2.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uom") (r "^0.25.0") (f (quote ("autoconvert" "usize" "u8" "u16" "u32" "u64" "u128" "isize" "i8" "i16" "i32" "i64" "i128"))) (d #t) (k 0)))) (h "0dnp8faiihrxxa8pw15z2byw46gf90s7wfdlh4k1rlzrp0lh3645")))

(define-public crate-cherries-0.2.4 (c (n "cherries") (v "0.2.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uom") (r "^0.25.0") (f (quote ("autoconvert" "usize" "u8" "u16" "u32" "u64" "u128" "isize" "i8" "i16" "i32" "i64" "i128"))) (d #t) (k 0)))) (h "0yz36lr3v1hab40c58kwdzdnw45nb22y0xryzw3v16wq7kszh96s")))

(define-public crate-cherries-0.2.5 (c (n "cherries") (v "0.2.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uom") (r "^0.25.0") (f (quote ("autoconvert" "usize" "u8" "u16" "u32" "u64" "u128" "isize" "i8" "i16" "i32" "i64" "i128"))) (d #t) (k 0)))) (h "1j3ci82xbsfnc6q86j74ghdqkqfw8h8bm59qgfwfcdj54ipf03zz")))

(define-public crate-cherries-0.3.1 (c (n "cherries") (v "0.3.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)) (d (n "uom") (r "^0.25.0") (f (quote ("autoconvert" "usize" "u8" "u16" "u32" "u64" "u128" "isize" "i8" "i16" "i32" "i64" "i128"))) (d #t) (k 0)))) (h "1bw2fp1ikrw2xj5k7z9a2qamrhsq47lpms5zpaxk6him1ygsvl5c")))

(define-public crate-cherries-0.3.2 (c (n "cherries") (v "0.3.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)) (d (n "uom") (r "^0.25.0") (f (quote ("autoconvert" "usize" "u8" "u16" "u32" "u64" "u128" "isize" "i8" "i16" "i32" "i64" "i128"))) (d #t) (k 0)))) (h "0rfqzyq0dfrq6ppfd7825hlv5vpl2lk8xlph3bqldr1pvwydrrld")))

