(define-module (crates-io ch er cherry-derive) #:use-module (crates-io))

(define-public crate-cherry-derive-0.1.0 (c (n "cherry-derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (d #t) (k 0)))) (h "1qnz8sri43n4k6i7bnq5hhcndb0zjzr1dhyij81wb8wdfsdp4azq") (y #t)))

(define-public crate-cherry-derive-0.1.1 (c (n "cherry-derive") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (d #t) (k 0)))) (h "1qxa82k5c1czva60nfqg8p6pdlsxz7n46hsk7gg066w0q3hikqfm") (y #t)))

(define-public crate-cherry-derive-0.2.0 (c (n "cherry-derive") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (d #t) (k 0)))) (h "1mqy1pzfcjzi0p5ndgij5m52qqdmm35gfy9j615lsmnjv6gmm72w") (y #t)))

(define-public crate-cherry-derive-0.2.1 (c (n "cherry-derive") (v "0.2.1") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (d #t) (k 0)))) (h "19xh7rpmvs42fg409ph5gnpsi3zkpig88hp1fj4dncps3nbyx4kr") (y #t)))

(define-public crate-cherry-derive-0.2.2 (c (n "cherry-derive") (v "0.2.2") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (d #t) (k 0)))) (h "033baqkl3l8zf33dz6acpm66lsj260l3ppy5ngfcmkksangw7ans") (y #t)))

(define-public crate-cherry-derive-0.3.0 (c (n "cherry-derive") (v "0.3.0") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (d #t) (k 0)))) (h "1i46kllgqbk1rklsnzqcbwqqvzzzxbif1z5jdvcmw9mrbv8c9g4x") (y #t)))

(define-public crate-cherry-derive-0.4.0 (c (n "cherry-derive") (v "0.4.0") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (d #t) (k 0)))) (h "00aga1lpk1hk0gam0nxmfq50x33lqh30p1y0j2pkc0xbfckj7z6z") (f (quote (("sqlite") ("postgres") ("mysql"))))))

