(define-module (crates-io ch er chert_accessor) #:use-module (crates-io))

(define-public crate-chert_accessor-0.1.0 (c (n "chert_accessor") (v "0.1.0") (d (list (d (n "cidr") (r "^0.2.1") (d #t) (k 0)))) (h "0sxzfyaw9ppg4vvy1fg1zw4hhc03hscv10c3yjxrjq83kr2zf9jr")))

(define-public crate-chert_accessor-0.2.0 (c (n "chert_accessor") (v "0.2.0") (d (list (d (n "cidr") (r "^0.2.1") (d #t) (k 0)))) (h "1vy000ky8mgacrw1fj9x6zs6asvwh40myp3mwhhlivky5fxxbdqy")))

