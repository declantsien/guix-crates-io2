(define-module (crates-io ch #{64}# ch643) #:use-module (crates-io))

(define-public crate-ch643-0.1.7 (c (n "ch643") (v "0.1.7") (d (list (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1m0s23q6411s90nmzblwjvhgval7imliifvq50b0wi8s1300y0yv") (f (quote (("rt") ("default") ("ch643"))))))

