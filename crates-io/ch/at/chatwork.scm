(define-module (crates-io ch at chatwork) #:use-module (crates-io))

(define-public crate-chatwork-0.1.0 (c (n "chatwork") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("gzip" "json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0xqypxbz8scplz48wffp6iqjvhmxw3g4zajkv3nqnnmd89v0x69b") (f (quote (("trust-dns" "reqwest/trust-dns") ("rustls-tls" "reqwest/rustls-tls") ("default" "reqwest/default"))))))

