(define-module (crates-io ch at chatui_client) #:use-module (crates-io))

(define-public crate-chatui_client-0.1.0 (c (n "chatui_client") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tui") (r "^0.15") (f (quote ("crossterm"))) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "14d2ycmf1l168y3hk178xyxl352z470kv7fq5yrahapzg2wfnimi")))

(define-public crate-chatui_client-0.1.1 (c (n "chatui_client") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tui") (r "^0.15") (f (quote ("crossterm"))) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "10x1qar7bj9zkcpy2bmhz8m4gx0j0lnnpq6q6w7pawxpj9yw9d2s")))

