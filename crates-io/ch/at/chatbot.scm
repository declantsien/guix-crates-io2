(define-module (crates-io ch at chatbot) #:use-module (crates-io))

(define-public crate-chatbot-0.0.2 (c (n "chatbot") (v "0.0.2") (h "1rwz9bwzjj6nlc3lj6y4j6pswa1z8slw5gg62lz408ydkzsy1jm6")))

(define-public crate-chatbot-0.0.3 (c (n "chatbot") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.30") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "slack") (r "^0.6.0") (d #t) (k 0)))) (h "1zlh538rfviag96rfan50wk4aazwvpr36axkrc4dj0wqnp6la2rl")))

(define-public crate-chatbot-0.1.0 (c (n "chatbot") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.30") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "slack") (r "^0.6.0") (d #t) (k 0)))) (h "1fp752fk9hwzl535gppx2d58m0zk58ln3w137hh7s1xl6gnzlc61")))

(define-public crate-chatbot-0.2.0 (c (n "chatbot") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.5.0") (d #t) (k 0)) (d (n "irc") (r "^0.8.14") (d #t) (k 0)) (d (n "regex") (r "^0.1.30") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "slack") (r "^0.6.0") (d #t) (k 0)))) (h "0vcrpyb9a1h2s0qh0vr8jpcydf2xajcg35c8gsaf92853iyb3i53")))

(define-public crate-chatbot-0.2.1 (c (n "chatbot") (v "0.2.1") (d (list (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "hyper") (r "~0.5") (d #t) (k 0)) (d (n "irc") (r "~0.8") (d #t) (k 0)) (d (n "regex") (r "~0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "slack") (r "~0.6") (d #t) (k 0)) (d (n "startuppong") (r "~0.1") (d #t) (k 0)))) (h "1afz57cva06rvifw6gkj4jyrggwxy08m88ika2gd1nxgnw7g4khp")))

(define-public crate-chatbot-0.2.2 (c (n "chatbot") (v "0.2.2") (d (list (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "hyper") (r "~0.5") (d #t) (k 0)) (d (n "irc") (r "~0.8") (d #t) (k 0)) (d (n "regex") (r "~0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "slack") (r "~0.6") (d #t) (k 0)) (d (n "startuppong") (r "~0.1") (d #t) (k 0)))) (h "0d9rr8pl3ycfffdial0jly3h449dq9dgm9vnbq1a8zmipc1g738l")))

(define-public crate-chatbot-0.2.3 (c (n "chatbot") (v "0.2.3") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "hyper") (r "~0.5") (d #t) (k 0)) (d (n "irc") (r "~0.8") (d #t) (k 0)) (d (n "regex") (r "~0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "slack") (r "~0.6") (d #t) (k 0)) (d (n "startuppong") (r "~0.1") (d #t) (k 0)))) (h "0v0x7kwsxiwjahg66qk3kz57id66i6y4kgfmxabhq734qgvhfs58")))

