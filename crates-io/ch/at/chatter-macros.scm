(define-module (crates-io ch at chatter-macros) #:use-module (crates-io))

(define-public crate-chatter-macros-0.1.0 (c (n "chatter-macros") (v "0.1.0") (d (list (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1mx763gzdrxg1x9n07m17aj8gzagikj4aqv28d9h49rhy7irwr0j")))

