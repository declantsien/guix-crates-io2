(define-module (crates-io ch at chat-templates) #:use-module (crates-io))

(define-public crate-chat-templates-0.1.0 (c (n "chat-templates") (v "0.1.0") (d (list (d (n "minijinja") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0x0wp63ji4fncm6amhp3l7dhym0yzll16hqjcqqad17zv93f6w2f")))

(define-public crate-chat-templates-0.2.0 (c (n "chat-templates") (v "0.2.0") (d (list (d (n "minijinja") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0zfj19clal2jk5jbvfz82fc3za2ykmxnn51irhnrzm0k6b1gdqhb")))

(define-public crate-chat-templates-0.3.0 (c (n "chat-templates") (v "0.3.0") (d (list (d (n "minijinja") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1isia0lb1ipyx40l4q4y8418scf5gdpymzc5anpbmnvfqy0nk33q")))

