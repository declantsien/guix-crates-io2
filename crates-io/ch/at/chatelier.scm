(define-module (crates-io ch at chatelier) #:use-module (crates-io))

(define-public crate-chatelier-0.1.0 (c (n "chatelier") (v "0.1.0") (d (list (d (n "chem-eq") (r "^0.3.1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)) (d (n "reedline-repl-rs") (r "^1.0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0g3m700l029cjvx81f4ycs46x856zfss2jbvippbphixdcrfgai6") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "chem-eq/serde"))))))

