(define-module (crates-io ch at chatdbg) #:use-module (crates-io))

(define-public crate-chatdbg-0.1.0 (c (n "chatdbg") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1fjcbhnavmpqbl38qsmc06wkfa9c72z9zmi3izg7gzvkyii2pgjj") (y #t)))

(define-public crate-chatdbg-0.1.1 (c (n "chatdbg") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1apf37kfj6fyakfzjqbngg37day3bk9r94vqf5wgycadr4b67jr3") (y #t)))

(define-public crate-chatdbg-0.1.2 (c (n "chatdbg") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0pmbi09cfbifgi5iah6farspxkqa20rvrkmivmp6a6cqhv5mlmg1")))

(define-public crate-chatdbg-0.1.3 (c (n "chatdbg") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "03dh83kkwg85plcpyp85fdk81377sjladhvig3z1ajf41n29nfll")))

(define-public crate-chatdbg-0.6.2 (c (n "chatdbg") (v "0.6.2") (d (list (d (n "chatdbg_macros") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0g9gd6ssa42w7796xw8jhlggkdf1j3inlzy841m0lwi617n5hpcd")))

(define-public crate-chatdbg-0.6.3 (c (n "chatdbg") (v "0.6.3") (d (list (d (n "chatdbg_macros") (r "^0.6.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1w07dj6yb4z0q7g29prlgb7g9mczglp46kb0ndvf49z692rql0gj")))

(define-public crate-chatdbg-0.6.4 (c (n "chatdbg") (v "0.6.4") (d (list (d (n "chatdbg_macros") (r "^0.6.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0rx8kq983jhyn8f5f0230xl6wlpiwb1xmgl63h3gzvr9vzcb7dwj")))

