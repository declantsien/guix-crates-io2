(define-module (crates-io ch at chat-gpt-rs) #:use-module (crates-io))

(define-public crate-chat-gpt-rs-1.2.0 (c (n "chat-gpt-rs") (v "1.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qfjai5iyala4vh7w4873m2dwa6jp901ms0avbwd0lnjp570fmb4")))

(define-public crate-chat-gpt-rs-1.3.0 (c (n "chat-gpt-rs") (v "1.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1785rn65jvlqx7b19cm18j8aypdjps7kn5qbyyqcc12k0jfg0d68")))

(define-public crate-chat-gpt-rs-1.4.0 (c (n "chat-gpt-rs") (v "1.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01x2xlkzf3yghra89lr40z814rrrn4a2saklaind6narmkg1brm7")))

