(define-module (crates-io ch at chatdbg_macros) #:use-module (crates-io))

(define-public crate-chatdbg_macros-0.1.3 (c (n "chatdbg_macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0m63p3sz76xcm0qglvf1n8sh50ygba9pqj3xnyanq8rc16gxzymh")))

(define-public crate-chatdbg_macros-0.6.2 (c (n "chatdbg_macros") (v "0.6.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0idsvd1v69krd9gzvc25zgsb07adk5nk5fwvalv330fl47wbczzc")))

(define-public crate-chatdbg_macros-0.6.3 (c (n "chatdbg_macros") (v "0.6.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1xm8vypl2z0nq10ljy1g257igbn9jscnd8fzw7rdrw3nqy0yvlxz")))

(define-public crate-chatdbg_macros-0.6.4 (c (n "chatdbg_macros") (v "0.6.4") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0xb986fivzawqqlnpy1sqbc76g0zq7ylad4d95w2xgiplqaxp7v4")))

