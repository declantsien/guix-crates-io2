(define-module (crates-io ch at chat-splitter) #:use-module (crates-io))

(define-public crate-chat-splitter-0.1.0 (c (n "chat-splitter") (v "0.1.0") (d (list (d (n "async-openai") (r "^0.12.1") (k 0)) (d (n "async-openai") (r "^0.12.1") (d #t) (k 2)) (d (n "indxvec") (r "^1.8.0") (k 0)) (d (n "log") (r "^0.4.19") (k 0)) (d (n "tiktoken-rs") (r "^0.5.0") (k 0)) (d (n "tokio") (r "^1.29.1") (d #t) (k 2)))) (h "1hw0hk53rmnk42ij8778hz4j3ia5b374kq7lik553bf6zq86gya3") (r "1.65.0")))

(define-public crate-chat-splitter-0.1.1 (c (n "chat-splitter") (v "0.1.1") (d (list (d (n "async-openai") (r "^0.12.2") (k 0)) (d (n "async-openai") (r "^0.12.2") (d #t) (k 2)) (d (n "indxvec") (r "^1.8.0") (k 0)) (d (n "log") (r "^0.4.20") (k 0)) (d (n "tiktoken-rs") (r "^0.5.1") (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 2)))) (h "1cldmax4q2qfk3fi6yi7vmijbb1aqan20xj0y2sc3239z62p6jx7") (r "1.72.0")))

