(define-module (crates-io ch at chatora-errno) #:use-module (crates-io))

(define-public crate-chatora-errno-1.0.0 (c (n "chatora-errno") (v "1.0.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "minwindef" "ntdef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06qhmddrwd19ajgx96iplrp8a94r55jv7p2apnwil90m7sb6r5mk")))

(define-public crate-chatora-errno-1.0.1 (c (n "chatora-errno") (v "1.0.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "minwindef" "ntdef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "031gsb7d1yhj3mgcdp669w3769ybxcy1mlbd1p3ws46rbc334fl2")))

(define-public crate-chatora-errno-1.0.2 (c (n "chatora-errno") (v "1.0.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)))) (h "09w5lwgs54xnzrs350p52rnnicffcxbfm26i3sf0jnamc34zglsm")))

