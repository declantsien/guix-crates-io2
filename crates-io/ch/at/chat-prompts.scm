(define-module (crates-io ch at chat-prompts) #:use-module (crates-io))

(define-public crate-chat-prompts-0.1.0 (c (n "chat-prompts") (v "0.1.0") (d (list (d (n "endpoints") (r "^0.1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qb3xjcx6cnaac618pk51w12zrx2sl1f58wwd4ahah0sy6krdyyj")))

(define-public crate-chat-prompts-0.1.1 (c (n "chat-prompts") (v "0.1.1") (d (list (d (n "endpoints") (r "^0.1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dn6qn09fn6dn29lzrqbfmy5r365cczkvxy3pg1n6vjjzwqzza1l")))

(define-public crate-chat-prompts-0.1.2 (c (n "chat-prompts") (v "0.1.2") (d (list (d (n "endpoints") (r "^0.1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00x44hf8vnr00q9ypvxz2szm0s2kqap47l4kri2k40bnazl905ig")))

(define-public crate-chat-prompts-0.2.0 (c (n "chat-prompts") (v "0.2.0") (d (list (d (n "endpoints") (r "^0.1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0i6zg9n9vp8sv86dnzgbijab84lrij3zqimgrqpjayb5hrvssmzc")))

(define-public crate-chat-prompts-0.3.0 (c (n "chat-prompts") (v "0.3.0") (d (list (d (n "endpoints") (r "^0.1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1i6g6hdfwjln2y1a6xh17z418kdwh4bkfdvpqxisnq7b20nfx7d3")))

(define-public crate-chat-prompts-0.3.1 (c (n "chat-prompts") (v "0.3.1") (d (list (d (n "endpoints") (r "^0.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ggb7qjyxg93lrpka5q4hn0llxbn40dkx5pgx9w9vygzssfv609b")))

(define-public crate-chat-prompts-0.4.0 (c (n "chat-prompts") (v "0.4.0") (d (list (d (n "endpoints") (r "^0.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hsjicss167rljy7yira9lsknxbihvx35d3av2rb8ii8s19abafd")))

(define-public crate-chat-prompts-0.4.1 (c (n "chat-prompts") (v "0.4.1") (d (list (d (n "endpoints") (r "^0.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gq4210m9rw2n1vxb5j944iiy4ass5d928rkhlqmyqdyn60ra0wq")))

(define-public crate-chat-prompts-0.4.2 (c (n "chat-prompts") (v "0.4.2") (d (list (d (n "endpoints") (r "^0.4") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fsia5jfyxyvlzsjbhmcin6m48jzjhl9a4pd9m3qj6f2dfl8c240")))

(define-public crate-chat-prompts-0.5.0 (c (n "chat-prompts") (v "0.5.0") (d (list (d (n "endpoints") (r "^0.5") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17wf44lajivxvl41jp84f8d9x51m79zs97vvsyh610cvfcl5ih23")))

(define-public crate-chat-prompts-0.5.1 (c (n "chat-prompts") (v "0.5.1") (d (list (d (n "endpoints") (r "^0.5") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0v26a27sv4c4654kzi87xfkz2nrnfa3b33mq6lnjiz0p9xrwgl31")))

(define-public crate-chat-prompts-0.5.2 (c (n "chat-prompts") (v "0.5.2") (d (list (d (n "endpoints") (r "^0.5") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "08gi7ss732lsfbk903dz1hn37cs76dq2xwwbp50y2si7qbn472qj")))

(define-public crate-chat-prompts-0.5.3 (c (n "chat-prompts") (v "0.5.3") (d (list (d (n "base64") (r "=0.22.0") (d #t) (k 0)) (d (n "endpoints") (r "^0.5") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jc1fwiyhkajyggs91zfngwvbk2bb2m1nld9x9k7mrlsah2bv9dy")))

(define-public crate-chat-prompts-0.5.4 (c (n "chat-prompts") (v "0.5.4") (d (list (d (n "base64") (r "=0.22.0") (d #t) (k 0)) (d (n "endpoints") (r "^0.5") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19g1262adqixz1skkm81p9daf1yvv73xncrgflnpr23in7gwmlwb")))

(define-public crate-chat-prompts-0.5.5 (c (n "chat-prompts") (v "0.5.5") (d (list (d (n "base64") (r "=0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "endpoints") (r "^0.6") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1sb92sd7a3hmbnhq244a9rx3b6pvahfkp0q1d0k2mmqhffgamc5m")))

(define-public crate-chat-prompts-0.5.6 (c (n "chat-prompts") (v "0.5.6") (d (list (d (n "base64") (r "=0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "endpoints") (r "^0.6") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0r40h8wlm0di76bwh8gxc2bhn0v6v4b75l56pc12m4frsk8marla")))

(define-public crate-chat-prompts-0.5.7 (c (n "chat-prompts") (v "0.5.7") (d (list (d (n "base64") (r "=0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "endpoints") (r "^0.6") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "129wi3nl3z6a28avv911bhv0m063cbm3z685pjw819x5akdq2rj8")))

(define-public crate-chat-prompts-0.6.0 (c (n "chat-prompts") (v "0.6.0") (d (list (d (n "base64") (r "=0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "endpoints") (r "^0.7") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1izllwyl9skp7djh9rb0xrar6hy5sy5s4dyilj53a8nkiczgy2vr")))

(define-public crate-chat-prompts-0.6.1 (c (n "chat-prompts") (v "0.6.1") (d (list (d (n "base64") (r "=0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "endpoints") (r "^0.7") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "04crymrbkjnxc0vi0p07y1wlbnsyg704zp3jknhdsmp97pdms4w5")))

(define-public crate-chat-prompts-0.6.2 (c (n "chat-prompts") (v "0.6.2") (d (list (d (n "base64") (r "=0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "endpoints") (r "^0.7") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qgf9cks9cqg73bx3xg6lf1zyv32x49g8qqksm1rry6qf9cd6p2a")))

(define-public crate-chat-prompts-0.7.0 (c (n "chat-prompts") (v "0.7.0") (d (list (d (n "base64") (r "=0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "endpoints") (r "^0.7") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rslz4apmmz6m5g5hnsmif61144nza7hjdqp09avhbqv7am3wx6b")))

(define-public crate-chat-prompts-0.7.1 (c (n "chat-prompts") (v "0.7.1") (d (list (d (n "base64") (r "=0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "endpoints") (r "^0.8") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gfngpfc1z49zahf2axnaf0fxib7lnfn32pfddgs58xzx15wghad")))

(define-public crate-chat-prompts-0.8.0 (c (n "chat-prompts") (v "0.8.0") (d (list (d (n "base64") (r "=0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "endpoints") (r "^0.8") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12c1sl86kaf71xcz0kmkshj6sf8kzv7804izcqg7prnx9zm0kdjf")))

