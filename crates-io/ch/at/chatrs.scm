(define-module (crates-io ch at chatrs) #:use-module (crates-io))

(define-public crate-chatrs-0.1.0 (c (n "chatrs") (v "0.1.0") (d (list (d (n "actix") (r "^0.10.0") (d #t) (k 0)) (d (n "actix-files") (r "^0.5") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "actix-web-actors") (r "^3.0.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.5") (d #t) (k 0)))) (h "1h0fr9sggdkpcjc5p7yk6n6ydgynsb0ag1g7l4i369xbz45h60ax")))

