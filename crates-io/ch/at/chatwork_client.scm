(define-module (crates-io ch at chatwork_client) #:use-module (crates-io))

(define-public crate-chatwork_client-0.1.0 (c (n "chatwork_client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1xv316m2n26hmxpwbhbqk76pnx32dj3qq92mz6z95qhqawfv79yk")))

(define-public crate-chatwork_client-0.1.1 (c (n "chatwork_client") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0pgiwvsr5vkv5bpvl6rvmxhydrb6yxnnmcrgwma6frg8iwz1d92a")))

