(define-module (crates-io ch at chatui_server) #:use-module (crates-io))

(define-public crate-chatui_server-0.1.0 (c (n "chatui_server") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0n8f2y80s21v03y9lvvrxw62jfagfsnl5m05vrfi38wdym3gi00p")))

(define-public crate-chatui_server-0.1.1 (c (n "chatui_server") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0nrxm133zlmza5p5kqcy33bhy4s39rx0r3clrqhpihxcn2yisdjb")))

