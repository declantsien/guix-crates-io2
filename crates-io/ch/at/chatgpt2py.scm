(define-module (crates-io ch at chatgpt2py) #:use-module (crates-io))

(define-public crate-chatgpt2py-0.1.0 (c (n "chatgpt2py") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0rfq891c1lmz1vrjjpnsims9g20dfmzjs7zalmnp133inwqd688j")))

