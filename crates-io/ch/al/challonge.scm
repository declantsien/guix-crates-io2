(define-module (crates-io ch al challonge) #:use-module (crates-io))

(define-public crate-challonge-0.1.0 (c (n "challonge") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9.7") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.4") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "08gnm4wclzyhbv39sqr1lll13rdwgr43y6mv35j7grppjciqla1z")))

(define-public crate-challonge-0.2.0 (c (n "challonge") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9.7") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.4") (d #t) (k 0)))) (h "0kwi13pk780yq43v69jrdk23bz7kx4l4mxnld61rhy7dj8i6r3lv")))

(define-public crate-challonge-0.3.0 (c (n "challonge") (v "0.3.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9.7") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.4") (d #t) (k 0)))) (h "158xkd10d0hgijiaa5bfwqd2ipnbhqd6ggn3mg788xxy399bnllc")))

(define-public crate-challonge-0.4.0 (c (n "challonge") (v "0.4.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9.7") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.4") (d #t) (k 0)))) (h "1nwjbw4wkidx7z6fphijca18n3gbcvxkwjywvijdg34hgdia6jh5")))

(define-public crate-challonge-0.5.0 (c (n "challonge") (v "0.5.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "09h7nn9wdlg0i0345fnxsyg4shnr1jibicmgdf7myp14f3bzppj4")))

(define-public crate-challonge-0.5.1 (c (n "challonge") (v "0.5.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "1f2jisjcfiavva90gh7xjl1y61l7dhilsd1jr86rz1982api5j2g") (f (quote (("default-tls" "reqwest/default-tls") ("default" "reqwest/rustls-tls"))))))

(define-public crate-challonge-0.5.2 (c (n "challonge") (v "0.5.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "189lvrpl69vpgrgax9pbhbqm5k7j36h5hr2z0pi403jb1wi3s8h7") (f (quote (("default-tls" "reqwest/default-tls") ("default" "reqwest/rustls-tls"))))))

(define-public crate-challonge-0.5.3 (c (n "challonge") (v "0.5.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0w76fc19hk1c1p0nhrpqlhcxpl613wbxzkv6k7al56wx7afrh1sf") (f (quote (("default-tls" "reqwest/default-tls") ("default" "reqwest/rustls-tls"))))))

(define-public crate-challonge-0.5.4 (c (n "challonge") (v "0.5.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ijvjvd5vk7hqbarz796l5sx703n9j1mxl8vrcwz7gibdkh1pkg5") (f (quote (("default-tls" "reqwest/default-tls") ("default" "reqwest/rustls-tls"))))))

