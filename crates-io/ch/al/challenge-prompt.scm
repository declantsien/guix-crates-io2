(define-module (crates-io ch al challenge-prompt) #:use-module (crates-io))

(define-public crate-challenge-prompt-0.1.0 (c (n "challenge-prompt") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.9") (d #t) (k 2)) (d (n "predicates") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1yyldc3vsai6932jrxlrhp3icsxhy5l0jcs4cmx22h7zb5mz2bf0")))

(define-public crate-challenge-prompt-0.2.0 (c (n "challenge-prompt") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^0.9") (d #t) (k 2)) (d (n "predicates") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "01yrqwjj5kmg4qd0pji2imzwwyw2n4zkz41jc0x2010ms4r88jr8")))

(define-public crate-challenge-prompt-0.3.0 (c (n "challenge-prompt") (v "0.3.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "11akyka767w3w92w0z84lng542msjcpb4109yh2n6wqkdq7z7gas")))

(define-public crate-challenge-prompt-0.4.0 (c (n "challenge-prompt") (v "0.4.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1s7n899sb75jsamwhbcd0iw1sx7dan80y6s5dylvnclb3n2xawqc") (r "1.60")))

