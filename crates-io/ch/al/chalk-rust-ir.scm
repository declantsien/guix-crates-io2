(define-module (crates-io ch al chalk-rust-ir) #:use-module (crates-io))

(define-public crate-chalk-rust-ir-0.10.0 (c (n "chalk-rust-ir") (v "0.10.0") (d (list (d (n "chalk-derive") (r "^0.10.0") (d #t) (k 0)) (d (n "chalk-engine") (r "^0.10.0") (d #t) (k 0)) (d (n "chalk-ir") (r "^0.10.0") (d #t) (k 0)) (d (n "chalk-macros") (r "^0.10.0") (d #t) (k 0)))) (h "027bcff27v0sifc78fmmqcwh8pykw719w5x4zjnxnpxbw3drdv50")))

