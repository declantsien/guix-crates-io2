(define-module (crates-io ch al chalk-derive) #:use-module (crates-io))

(define-public crate-chalk-derive-0.10.0 (c (n "chalk-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "047a0jzyby0zfzfqikd57jf3qyhbjc6a3kqmz5irwkadmnpj0ild")))

(define-public crate-chalk-derive-0.11.0 (c (n "chalk-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1j9s6kgxvdvgcch2ria2h8gw3qdcdgjx58c366bps9w7mcgd16sv")))

(define-public crate-chalk-derive-0.12.0 (c (n "chalk-derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "15462r3ljrji8h9ppp43bi3cx2ycpwyd8yzknnycz9kxq7ns1izs")))

(define-public crate-chalk-derive-0.13.0 (c (n "chalk-derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1r7bya678wg87lvh05k1v7snnhp9x1p6l8w0f28nrc6vm0fvwfa9")))

(define-public crate-chalk-derive-0.14.0 (c (n "chalk-derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "078kwxm324bh0g7196kc2wlry9nk463n1s3jvs0y21yn0lcy0qyl")))

(define-public crate-chalk-derive-0.15.0 (c (n "chalk-derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "09l2cika0dcygqs3j3bazlzqxx8np115dplvzpy06hfhfam9qdy7")))

(define-public crate-chalk-derive-0.16.0 (c (n "chalk-derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0clbv2a1iqnlh504icsk82ax85v1iyayk375crn7xl1cvmikc93l")))

(define-public crate-chalk-derive-0.17.0 (c (n "chalk-derive") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1jvd6k1371c6jpy4mg6n6sbjrkbwpl6br0da3580v95i4cmg35lk")))

(define-public crate-chalk-derive-0.18.0 (c (n "chalk-derive") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0agrqcjcmaswg4rdz23p9lmbj00dmi6r6n5bqi551cih1hps58zf")))

(define-public crate-chalk-derive-0.19.0 (c (n "chalk-derive") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "15s6vxrwi55j9j133mr95jpmz5xghy371dwc0b2jj9ms8qcn2k35")))

(define-public crate-chalk-derive-0.20.0 (c (n "chalk-derive") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0y77q5vd59490g4bn126kwpf5sv0n0hzxbr1n4iag26wcqg3gkif")))

(define-public crate-chalk-derive-0.21.0 (c (n "chalk-derive") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1gs03khg7cj7z7cbkblynxv9gl5cn9mac3zj436lljypayxhvpy1")))

(define-public crate-chalk-derive-0.22.0 (c (n "chalk-derive") (v "0.22.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0gxm90whpskk5k37k4mhqdal9invq2gsns5q1agdwngh9kdqjqpj")))

(define-public crate-chalk-derive-0.23.0 (c (n "chalk-derive") (v "0.23.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "13whlkc8fryfs5jzcb2rj2dvylwjs37vvq657lcg3mqzjs747jy3")))

(define-public crate-chalk-derive-0.24.0 (c (n "chalk-derive") (v "0.24.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0pp6z1cfv85vnlf4yp2f4qg0b8f0khn3ig2c3rkr453rp98jgxrp")))

(define-public crate-chalk-derive-0.25.0 (c (n "chalk-derive") (v "0.25.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1d68ibfq7ywf1rpmnr90vzaxv61g13x1y24pzmpnw619y39i8kk2")))

(define-public crate-chalk-derive-0.26.0 (c (n "chalk-derive") (v "0.26.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1i2cpjwvd5d0bvgcbhdaginmp5hm364wjzi8zhqjm88svqhxxakj")))

(define-public crate-chalk-derive-0.27.0 (c (n "chalk-derive") (v "0.27.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1a6lsm6z2yb8xf71pqlx916k32ixihv6mla47s32mzhilbr4yi6m")))

(define-public crate-chalk-derive-0.28.0 (c (n "chalk-derive") (v "0.28.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0h1n04frn2il9ry7p7w59n66lmam5vk597r79an4c6yww89v11cc")))

(define-public crate-chalk-derive-0.29.0 (c (n "chalk-derive") (v "0.29.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0n8c5iiz419rc275i7cjcsza1yv9pl9c0cdffy46vifd7dz2azrs")))

(define-public crate-chalk-derive-0.30.0 (c (n "chalk-derive") (v "0.30.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "12qzqdymyic7y04c9mcibv4nlzib189gm8spwg0p0x3vb0c6ssd6")))

(define-public crate-chalk-derive-0.31.0 (c (n "chalk-derive") (v "0.31.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0f8zrvwqdlxmy2mcpgyr9bbzmyj57l845lw24an4lgpl41zcjh6a")))

(define-public crate-chalk-derive-0.32.0 (c (n "chalk-derive") (v "0.32.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0jh86zziicdhpvf94aid0psw8jr24lvqnpaigkdbmw13lwmjn1rd")))

(define-public crate-chalk-derive-0.33.0 (c (n "chalk-derive") (v "0.33.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0lmrl343sn8vwxsbrcag2jx9fy4gs337ql72dy16njh8p751942n")))

(define-public crate-chalk-derive-0.34.0 (c (n "chalk-derive") (v "0.34.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "02k09kdym05vfp60hfdkjya8h8frddcd958akn4dzd7ca1kri7qf")))

(define-public crate-chalk-derive-0.35.0 (c (n "chalk-derive") (v "0.35.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1kb4zyasnjjyappkvsid6sl9vd29smgm43vs983kj29wx6ajhvdw")))

(define-public crate-chalk-derive-0.36.0 (c (n "chalk-derive") (v "0.36.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "041smi3kq12qayxdmmih6pg8jm1dmv7i2xlvwiqwxnp1x96wx24z")))

(define-public crate-chalk-derive-0.37.0 (c (n "chalk-derive") (v "0.37.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1rmyf7zxgdnbx03yn2l7w61yf0n6nnazlipaqwys83b21ndm4jsn")))

(define-public crate-chalk-derive-0.38.0 (c (n "chalk-derive") (v "0.38.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0hr0lq15nfxgp8pmglgn8wkkfyrqchwn64hdag5lzflpnch94717")))

(define-public crate-chalk-derive-0.39.0 (c (n "chalk-derive") (v "0.39.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1s16z07jfldmn0ndl6jis6x0njr40ygzsxqkkjkal55pjjjk2kp0")))

(define-public crate-chalk-derive-0.40.0 (c (n "chalk-derive") (v "0.40.0") (d (list (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r ">=0.12.0, <0.13.0") (d #t) (k 0)))) (h "1hi9w4xnb0j2namxcxq55f0mcv2fyix3b3fkksin95997hdwnfy7")))

(define-public crate-chalk-derive-0.41.0 (c (n "chalk-derive") (v "0.41.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "10fvlymk2bqr9xxyy9y7d6k5db44ldlyys2xklvlszxj3w04i3g1")))

(define-public crate-chalk-derive-0.42.0 (c (n "chalk-derive") (v "0.42.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0xycskgbagyj7p7xkm3s0s7dgmcgr60myy4lr38whlw092h6f3ih")))

(define-public crate-chalk-derive-0.43.0 (c (n "chalk-derive") (v "0.43.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "16s6h39rya8l30z3cpj9bcyqj8krchw8mjx5zsq1mvfcmp4f1ng2")))

(define-public crate-chalk-derive-0.44.0 (c (n "chalk-derive") (v "0.44.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1jcqvk8l9w7vs4hcx1qlacsr5f754sfg6fy1add3scglxpjlddjn")))

(define-public crate-chalk-derive-0.45.0 (c (n "chalk-derive") (v "0.45.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1vmwfyskw2cjjf27gp7ab6pmj2468fa9s12xp4qaklaq97wsqzgc")))

(define-public crate-chalk-derive-0.46.0 (c (n "chalk-derive") (v "0.46.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0q5n4606shajik5kbsfw6v6rpc2bb0zp7dv0giyhly9jq06v4m4d")))

(define-public crate-chalk-derive-0.47.0 (c (n "chalk-derive") (v "0.47.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0p7axlca5x7vfpzm2jblkyngqyhi3qw6mlq2421dqziq58sgc01z")))

(define-public crate-chalk-derive-0.48.0 (c (n "chalk-derive") (v "0.48.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1if6ryd1vbsckyxx6y9gkq7n2z83nmmba7wh2lf1rkc54v51qazg")))

(define-public crate-chalk-derive-0.49.0 (c (n "chalk-derive") (v "0.49.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "17f63c9aiq18x2y9rwv1liadk98623085anbppj6s590w445jlmj")))

(define-public crate-chalk-derive-0.50.0 (c (n "chalk-derive") (v "0.50.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1q487062f4hsiffzr62zknqram3gxi5jsafpf7lp6d8117s5qq5c")))

(define-public crate-chalk-derive-0.51.0 (c (n "chalk-derive") (v "0.51.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1vjn3kdli1b6f9xbr8c2vah3vpjbrrpqsynfagy1yxv4p48av8pi")))

(define-public crate-chalk-derive-0.52.0 (c (n "chalk-derive") (v "0.52.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1gcyyk7pnhj3zpxkj9f62mid8ha35nsgsp0ljngc7jy7czsrx6fs")))

(define-public crate-chalk-derive-0.53.0 (c (n "chalk-derive") (v "0.53.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0mrnsww5sd6fwk4km96dgc096abma78wjbjxy6xpr75h3psi5q27")))

(define-public crate-chalk-derive-0.54.0 (c (n "chalk-derive") (v "0.54.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0jzf757bqlln8zfgvk7wcanyhkm4cyzldqva2akd9rwv99ckd5pa")))

(define-public crate-chalk-derive-0.55.0 (c (n "chalk-derive") (v "0.55.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0v4brsl7xniy41lzl4zip4fxi871wmznnrnb9a90y7yqmhy1k0rr")))

(define-public crate-chalk-derive-0.56.0 (c (n "chalk-derive") (v "0.56.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "18lcyhkpw4g14qp2lp2jpkkyr786as0gcz51qpvcy6d1n6jcgxji")))

(define-public crate-chalk-derive-0.57.0 (c (n "chalk-derive") (v "0.57.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "18sphby1mmsqpmdwdhf4vk17xb8k8q8q5700pfx1v2748mkfsxrj")))

(define-public crate-chalk-derive-0.58.0 (c (n "chalk-derive") (v "0.58.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1a3vfh5jp8860sqwk0h9rcbh4059ga7xx8090la869r7i33bf9g6")))

(define-public crate-chalk-derive-0.59.0 (c (n "chalk-derive") (v "0.59.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0jm9g44sxcvbfnw3dgbrbgc23lvpc89zvf9sjz43sdb7rgxh142b")))

(define-public crate-chalk-derive-0.60.0 (c (n "chalk-derive") (v "0.60.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1li2d1jdv025i60qnwwaisr9afy7ijbj67dw2w1azgjgbm2783xb")))

(define-public crate-chalk-derive-0.61.0 (c (n "chalk-derive") (v "0.61.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "040kfw2pm4avkwspq84zxanspg7msdgh77bjr3lch3923f45lhbq")))

(define-public crate-chalk-derive-0.62.0 (c (n "chalk-derive") (v "0.62.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0s3awsa9qmjg8cfx4i9qfdqk3hv3cyqs3ny9qxmy638h9py5qkj6")))

(define-public crate-chalk-derive-0.63.0 (c (n "chalk-derive") (v "0.63.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0jjf0pn87qd0zhgl5lica5m68jqf6ggzpn0msvfapnnf44anb86m")))

(define-public crate-chalk-derive-0.64.0 (c (n "chalk-derive") (v "0.64.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "091cv16k99sp195dp9yrwsjxfs8midmdhynp9kafg6mpxalz5b6r")))

(define-public crate-chalk-derive-0.65.0 (c (n "chalk-derive") (v "0.65.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "034c4r2cmxn3il6xzw6n3vp3byy44hirpjj2fbjl4fbckh354bnf")))

(define-public crate-chalk-derive-0.66.0 (c (n "chalk-derive") (v "0.66.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "096hiqsfc6jj70lff79irq05w78gcmcgdf1pfsbmdjkkxn4a0ylb")))

(define-public crate-chalk-derive-0.67.0 (c (n "chalk-derive") (v "0.67.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "037fa47vdpzq4y838vwqlwbglgx220gsy3grx5jp2kdjxvckq6km")))

(define-public crate-chalk-derive-0.68.0 (c (n "chalk-derive") (v "0.68.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0ss18zx2ddp50mvbwffs5jmmcs7a0pkiq1g62xa7z1bacvkm45ga")))

(define-public crate-chalk-derive-0.69.0 (c (n "chalk-derive") (v "0.69.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "00jw5a7bcc85sw1icdh73pwp9w1y2nlzsjai9665g6qkjw42qjgg")))

(define-public crate-chalk-derive-0.70.0 (c (n "chalk-derive") (v "0.70.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1xnpfzx78fl96pb0pjmfa8ndn3d2n3cpd6bhphlywyl6i3ps8a8v")))

(define-public crate-chalk-derive-0.71.0 (c (n "chalk-derive") (v "0.71.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "15vikrpycjlcy23h5l3r4hagsc6bhcz8sd2vyn1dsmqyli5wx705")))

(define-public crate-chalk-derive-0.72.0 (c (n "chalk-derive") (v "0.72.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1j4apjgrhr5nl93i7jm5fbw6i1pm42rlxdy3srsyx9mrf667m52h")))

(define-public crate-chalk-derive-0.73.0 (c (n "chalk-derive") (v "0.73.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "116mbnm8g73p7nmhxrxs8gwx4iyzi5iy814rzypkc2mf8162w61z")))

(define-public crate-chalk-derive-0.74.0 (c (n "chalk-derive") (v "0.74.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "0xa357sc8rajrl7pyyw9wllc5gd27jylm3mp7ynvwd726i8win09")))

(define-public crate-chalk-derive-0.75.0 (c (n "chalk-derive") (v "0.75.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "0v1i5sb1w2skqg5sjy3gimdglsq0in6mc1zz36qyc99lkrgknknm")))

(define-public crate-chalk-derive-0.76.0 (c (n "chalk-derive") (v "0.76.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1q70h770kxrkr3j2s10wb499v2y1zjjppazrnvdkl7paaa04phjq")))

(define-public crate-chalk-derive-0.77.0 (c (n "chalk-derive") (v "0.77.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "11x3nc4ffr5sl09xiaq0ph5c7zgcy6rz7paf9cd7bmjm53rqyvci")))

(define-public crate-chalk-derive-0.78.0 (c (n "chalk-derive") (v "0.78.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "09qq5v8zjqr9vqfl91shirkbda3qh07ag4nmfsdhsry4kkynjiw3")))

(define-public crate-chalk-derive-0.79.0 (c (n "chalk-derive") (v "0.79.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "05isrxc4ry72b61rrb0drldbblm6w243vrw8qkapjqrrfi3kc50b")))

(define-public crate-chalk-derive-0.80.0 (c (n "chalk-derive") (v "0.80.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1mib4lr3afvbkgrcnxsiixyzgy1ql678kqb5igh628zi1kgil06h")))

(define-public crate-chalk-derive-0.81.0 (c (n "chalk-derive") (v "0.81.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1bw4iah3bpv0ibaj10pkdp02bg6q9fdy8ldvzvpi10nsm65cmchb")))

(define-public crate-chalk-derive-0.82.0 (c (n "chalk-derive") (v "0.82.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1nis7h7smccrvqj31z9v0g2gkflz9gajijxbaglrrhj3sn8dg77f")))

(define-public crate-chalk-derive-0.83.0 (c (n "chalk-derive") (v "0.83.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "09qqz3gavi6xnlsn989m3xd9y8knr3ixjbglrnp5hzkiywp3qmc3")))

(define-public crate-chalk-derive-0.84.0 (c (n "chalk-derive") (v "0.84.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1hm68z2d62jay7aqn9wkvn35sa596yz92lv7wyq5g3bzsl4w2afg")))

(define-public crate-chalk-derive-0.85.0 (c (n "chalk-derive") (v "0.85.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "0lsh3afqmdb8nfv41iggmxhzr507bzm7raasiykx91d81n6f0l5q")))

(define-public crate-chalk-derive-0.86.0 (c (n "chalk-derive") (v "0.86.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "00klsbkdm2qi6324hq6b1vgd6fl9859q2958cr1hkdamv0ax96al")))

(define-public crate-chalk-derive-0.87.0 (c (n "chalk-derive") (v "0.87.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1nwc58axd4k1xa896msfnz838fiwkll1sywiddfc6pqz6kxb4lnm")))

(define-public crate-chalk-derive-0.88.0 (c (n "chalk-derive") (v "0.88.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "0nj5arhfp78h18hkq0nki9m5cmgn9flvrvpfc3smj3hzphzhmy2d")))

(define-public crate-chalk-derive-0.89.0 (c (n "chalk-derive") (v "0.89.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1avad28yl0vkljbmg31h4qm74npbx00m05mac5cpdi3xk186q5za")))

(define-public crate-chalk-derive-0.90.0 (c (n "chalk-derive") (v "0.90.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0gn6z8mncxsa8gwv6sc976vyrcv8xdwlihpahcp1v5hwmkshx9cs")))

(define-public crate-chalk-derive-0.91.0 (c (n "chalk-derive") (v "0.91.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0bfhij3qm5a5p4cyxzszw47x16a3b8754hdq8y6ffksrxpypi4f5")))

(define-public crate-chalk-derive-0.92.0 (c (n "chalk-derive") (v "0.92.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "108j9snkljy1nfr13wkcnx9i5p2nzi33jhl2k8kzbgrdljl56l7z")))

(define-public crate-chalk-derive-0.93.0 (c (n "chalk-derive") (v "0.93.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0aqn2h5xnhp8ybkrd2rmpiqnxkhy094izd9yq8i7zz0ij0ajcir6")))

(define-public crate-chalk-derive-0.94.0 (c (n "chalk-derive") (v "0.94.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "102f5aclnbm33nm3sx9bh917bn03lhmgg6iwl0bk5fnfi592scn0")))

(define-public crate-chalk-derive-0.95.0 (c (n "chalk-derive") (v "0.95.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1lz9dqdqrs8gd563jr5i3z31blw2f0ywskbwsjndmgfjikr2g51j")))

(define-public crate-chalk-derive-0.96.0 (c (n "chalk-derive") (v "0.96.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "06mpg90yja85hk59gfs9zbh23p86knz9aa68cpz908n3i2hcwxjn")))

(define-public crate-chalk-derive-0.97.0 (c (n "chalk-derive") (v "0.97.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0wj9yn1rcfw7076lwvfs52nq8ql1bj6c77nwnz0cbbf29bfax84j")))

