(define-module (crates-io ch al chalk_rs) #:use-module (crates-io))

(define-public crate-chalk_rs-1.0.0 (c (n "chalk_rs") (v "1.0.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("consoleapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07ki4g4kvx1wiy3cxbk626frfm5d9k45qzw2sz17mag6qr4zk70d") (y #t)))

(define-public crate-chalk_rs-1.0.1 (c (n "chalk_rs") (v "1.0.1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("consoleapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1i6d70n2lpq9191bfj50d32d72f48hfmyxvf1h1pbri0gsp1hak4")))

