(define-module (crates-io ch al chalkboard) #:use-module (crates-io))

(define-public crate-chalkboard-0.1.0 (c (n "chalkboard") (v "0.1.0") (d (list (d (n "druid") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1.0") (d #t) (k 0)))) (h "0a28c9sx13588s1yhrk1shic638iv7j4x7srkf5a4nq4a4wppnaa")))

