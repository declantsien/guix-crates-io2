(define-module (crates-io ch al chalk-ir) #:use-module (crates-io))

(define-public crate-chalk-ir-0.10.0 (c (n "chalk-ir") (v "0.10.0") (d (list (d (n "chalk-derive") (r "^0.10.0") (d #t) (k 0)) (d (n "chalk-engine") (r "^0.10.0") (d #t) (k 0)) (d (n "chalk-macros") (r "^0.10.0") (d #t) (k 0)) (d (n "lalrpop-intern") (r "^0.15.1") (o #t) (d #t) (k 0)))) (h "0mmr3hiz9fzm6h6kvsgb6zcq804pr7hwjcladsbpyli8cgxzapfl") (f (quote (("default-interner" "lalrpop-intern") ("default"))))))

(define-public crate-chalk-ir-0.11.0 (c (n "chalk-ir") (v "0.11.0") (d (list (d (n "chalk-derive") (r "=0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0w26jj4ia9ymib1wlqqq1c4a0895pddv3xs8qcykyv6ivqsmfpjr")))

(define-public crate-chalk-ir-0.12.0 (c (n "chalk-ir") (v "0.12.0") (d (list (d (n "chalk-derive") (r "=0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1dh2z81hr9m88xz0ca0hw5friacqmsxg5rnjb288glgm1dbyqwlr")))

(define-public crate-chalk-ir-0.13.0 (c (n "chalk-ir") (v "0.13.0") (d (list (d (n "chalk-derive") (r "=0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1z77g5f19gya8n2xpdkmwyz6mhv7fyz95raynarl3x3s9zznl4kw")))

(define-public crate-chalk-ir-0.14.0 (c (n "chalk-ir") (v "0.14.0") (d (list (d (n "chalk-derive") (r "=0.14.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1gmx2akkrasz1069nk2s3nr6zjpijl5kp8plh3z8yjb8kwgdqgzx")))

(define-public crate-chalk-ir-0.15.0 (c (n "chalk-ir") (v "0.15.0") (d (list (d (n "chalk-derive") (r "=0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01mlzqhrb2z23r8djndckf9xrzifakljpnbi863lbz610cd3j7i3")))

(define-public crate-chalk-ir-0.16.0 (c (n "chalk-ir") (v "0.16.0") (d (list (d (n "chalk-derive") (r "=0.16.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0jmm7q7q46shyvsqcqnrz6jgjkf8shh250i69d371a643gqgwpj4")))

(define-public crate-chalk-ir-0.17.0 (c (n "chalk-ir") (v "0.17.0") (d (list (d (n "chalk-derive") (r "=0.17.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1yfasiign3zy0q8gzvzvhq6j9zhfwixgpbr7vrgdc5q7a1ywdsc7")))

(define-public crate-chalk-ir-0.18.0 (c (n "chalk-ir") (v "0.18.0") (d (list (d (n "chalk-derive") (r "=0.18.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1gcw1jp7bg11p0ndq1fgy7ddapb2m68rwy822laknphl7rj7nqgv")))

(define-public crate-chalk-ir-0.19.0 (c (n "chalk-ir") (v "0.19.0") (d (list (d (n "chalk-derive") (r "=0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1s4smjrkjnsvh7lnh6ncgh2j3rphxdma615qfdmqijjlqvxl2lqa")))

(define-public crate-chalk-ir-0.20.0 (c (n "chalk-ir") (v "0.20.0") (d (list (d (n "chalk-derive") (r "=0.20.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0pr0mwwzgiqkvp3li0n2j335qrv8qyhq0w4rr5mgx7k0gjvgf400")))

(define-public crate-chalk-ir-0.21.0 (c (n "chalk-ir") (v "0.21.0") (d (list (d (n "chalk-derive") (r "=0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1m43ll8cnvzz35ws89jw4bri5jm93g1a7mvayllc87fvvcjildj4")))

(define-public crate-chalk-ir-0.22.0 (c (n "chalk-ir") (v "0.22.0") (d (list (d (n "chalk-derive") (r "=0.22.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0vjhggnxmr26s0mkssqr97ln27cwc95z6pkvm874zwyvcy6yjmg1")))

(define-public crate-chalk-ir-0.23.0 (c (n "chalk-ir") (v "0.23.0") (d (list (d (n "chalk-derive") (r "=0.23.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0pc5y3rzaf71v2fh24pkyyq4a9r82cfrpcxsdy618nq1rfzjlcxv")))

(define-public crate-chalk-ir-0.24.0 (c (n "chalk-ir") (v "0.24.0") (d (list (d (n "chalk-derive") (r "=0.24.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "04zai2kps4h3z7vh7b0rglib0yr5k514xi3l22h2nqqck2hx1qa5")))

(define-public crate-chalk-ir-0.25.0 (c (n "chalk-ir") (v "0.25.0") (d (list (d (n "chalk-derive") (r "=0.25.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0n79zz16rlzz6qyb174qfr7qbwsh8p4q9vxva2ph8mnsrpn6i30i")))

(define-public crate-chalk-ir-0.26.0 (c (n "chalk-ir") (v "0.26.0") (d (list (d (n "chalk-derive") (r "=0.26.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0n3ppi4hb0k7sszwk2wpb12gfn8gw5zlf7y260m3djk4nrlnx1bb")))

(define-public crate-chalk-ir-0.27.0 (c (n "chalk-ir") (v "0.27.0") (d (list (d (n "chalk-derive") (r "=0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1vzxzjnvdmgsf7chnw9bb4m9i2lrzvjn5apsb1vamgsavnqkv773")))

(define-public crate-chalk-ir-0.28.0 (c (n "chalk-ir") (v "0.28.0") (d (list (d (n "chalk-derive") (r "=0.28.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0asfkxx0x3y93c2bqf7yhrly41jg7mfjq0g3xkij1jcdd12dhd91")))

(define-public crate-chalk-ir-0.29.0 (c (n "chalk-ir") (v "0.29.0") (d (list (d (n "chalk-derive") (r "=0.29.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1c4dl3iwd9d4j642xv1190lrn5vrjb1n9z1vzyhvbcpc5400b903")))

(define-public crate-chalk-ir-0.30.0 (c (n "chalk-ir") (v "0.30.0") (d (list (d (n "chalk-derive") (r "=0.30.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0im9lp9la2x0n3gp40x4cyprjh4sjry72b84vbnxc7ryin8ki5cc")))

(define-public crate-chalk-ir-0.31.0 (c (n "chalk-ir") (v "0.31.0") (d (list (d (n "chalk-derive") (r "=0.31.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0hdv2lrydm9q0fff5vpm05rrwzp1s1z8cpsigcwcqkq8bsaq6hcc")))

(define-public crate-chalk-ir-0.32.0 (c (n "chalk-ir") (v "0.32.0") (d (list (d (n "chalk-derive") (r "=0.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01csgrpzc7b98n4gmv8qc13p0av2sfm6912ym2v5qif5307dn37n")))

(define-public crate-chalk-ir-0.33.0 (c (n "chalk-ir") (v "0.33.0") (d (list (d (n "chalk-derive") (r "=0.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0hcm7jb99341m1nnlvk7xxk853py2qvcmjc9kx0rddlhclmap7ld")))

(define-public crate-chalk-ir-0.34.0 (c (n "chalk-ir") (v "0.34.0") (d (list (d (n "chalk-derive") (r "=0.34.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0s63g8gbx8gcww19bgfbs6f3v3d7zm4a51jxvsrqnh7i9532qdaw")))

(define-public crate-chalk-ir-0.35.0 (c (n "chalk-ir") (v "0.35.0") (d (list (d (n "chalk-derive") (r "=0.35.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1mhlnh5g4s5qdbznj0hsdb4070dwrzwvcwkkgq8qkj6c1hagdms0")))

(define-public crate-chalk-ir-0.36.0 (c (n "chalk-ir") (v "0.36.0") (d (list (d (n "chalk-derive") (r "=0.36.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "18vxhz4f9v7960ykdl9ny56j5f1zfrq900h4kdisn510kii2qdk3")))

(define-public crate-chalk-ir-0.37.0 (c (n "chalk-ir") (v "0.37.0") (d (list (d (n "chalk-derive") (r "=0.37.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "092d3vqhs8mgwigwc067283g23zh4iyiy6wgdpnwpx474d42jzg6")))

(define-public crate-chalk-ir-0.39.0 (c (n "chalk-ir") (v "0.39.0") (d (list (d (n "chalk-derive") (r "=0.39.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0qs387486fhdil70xqlxgl4pg210p1pkyn46cdkp72wzyy9vxz43")))

(define-public crate-chalk-ir-0.40.0 (c (n "chalk-ir") (v "0.40.0") (d (list (d (n "chalk-derive") (r "=0.40.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)))) (h "09djx4fj15lyjxz0b1pgmb1x9mcnn24lxhcb969y6mfm4j57nrhk")))

(define-public crate-chalk-ir-0.41.0 (c (n "chalk-ir") (v "0.41.0") (d (list (d (n "chalk-derive") (r "=0.41.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1s1ndnpnyfyjxwanm4dlasi3vrh1w3d521rm953vwxr2ky5sp9b4")))

(define-public crate-chalk-ir-0.43.0 (c (n "chalk-ir") (v "0.43.0") (d (list (d (n "chalk-derive") (r "=0.43.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1lvvk1g16lyyk2l59zcfyvlwjhdb7x6yd5z97nac96vhlmk848f5")))

(define-public crate-chalk-ir-0.45.0 (c (n "chalk-ir") (v "0.45.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.45.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0rvcg6rxi29h3l0y4j30vd9mfsircbdvmx47v8byyz94vs7b79d1")))

(define-public crate-chalk-ir-0.46.0 (c (n "chalk-ir") (v "0.46.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.46.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0jg56nrvsdfx7v69zq1pnkl202y4mg2vrsl2qhdphl55qli4kcfg")))

(define-public crate-chalk-ir-0.47.0 (c (n "chalk-ir") (v "0.47.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.47.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1a8aqzqsn33qmdw91k4j1adkvnrrxv5p6vjjwm9yf6jr2fcyd1n6")))

(define-public crate-chalk-ir-0.48.0 (c (n "chalk-ir") (v "0.48.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.48.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "05594lkflnkr5lizvhpgaryg4jznpzsyi73mvznf361rxpx0n815")))

(define-public crate-chalk-ir-0.49.0 (c (n "chalk-ir") (v "0.49.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.49.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0mhpn5b7r0jlsw6w6ic5xzij9glpvp0vq6b0p81k2l964rqd36xc")))

(define-public crate-chalk-ir-0.50.0 (c (n "chalk-ir") (v "0.50.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.50.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1zq1sk2r07vk8mhphkspj285fhqfalhgg0frbkdpxdn8qarvy7gs")))

(define-public crate-chalk-ir-0.51.0 (c (n "chalk-ir") (v "0.51.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.51.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1vh0p15nj2hzw39a3bdklq90x2isv10ihwkk15gsm1wnl104pc8p")))

(define-public crate-chalk-ir-0.52.0 (c (n "chalk-ir") (v "0.52.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.52.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1acsf9ymzr75kn8vfcwaikdpmyw9w5m5naycvj6pxjzx45ap0k2s")))

(define-public crate-chalk-ir-0.55.0 (c (n "chalk-ir") (v "0.55.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.55.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1dic9w9apxf0rdgbsbkfw8x7n590642pws1d8y7ngrviz4xga8m5")))

(define-public crate-chalk-ir-0.56.0 (c (n "chalk-ir") (v "0.56.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.56.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "082yk4lh4m9psq1b7xrfnlwbkzqb6pg0ydhwf7iqyyd5qd5m4pvx")))

(define-public crate-chalk-ir-0.57.0 (c (n "chalk-ir") (v "0.57.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.57.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "146psfwghd5w6js798sb698vsc4s5n9wn2m2b5w4fyk7qidwlaqb")))

(define-public crate-chalk-ir-0.58.0 (c (n "chalk-ir") (v "0.58.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.58.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01pjjc8nr9v082gzrq1fyf8d9szxdxahs1k7cvaxfnc929qdh862")))

(define-public crate-chalk-ir-0.59.0 (c (n "chalk-ir") (v "0.59.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.59.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1z4dzq5gg9zaah0ph2wz6wxlaf0sfyh8yl6gxivccmrm3gb2hddj")))

(define-public crate-chalk-ir-0.60.0 (c (n "chalk-ir") (v "0.60.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.60.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1qjbcib59r4w2dcxdf40xjpjq7jqiajds08xdk83nnqa4731yjr9")))

(define-public crate-chalk-ir-0.61.0 (c (n "chalk-ir") (v "0.61.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.61.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0dw8fdhzzldp2sayd1a944nw11qjm5yz3gpkrmjwm7868irs1ikb")))

(define-public crate-chalk-ir-0.62.0 (c (n "chalk-ir") (v "0.62.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.62.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1nmj3igkabmvi7qaddi7vjkwgsjsyfp6m4a904f87vzprpfcsj23")))

(define-public crate-chalk-ir-0.63.0 (c (n "chalk-ir") (v "0.63.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.63.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0yl0ky61cx3n71vz00z22sph2bpnw71wlbr3kwgipjrabxdpij1s")))

(define-public crate-chalk-ir-0.64.0 (c (n "chalk-ir") (v "0.64.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.64.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0cphgxjpl7hi4s20mrcr13b71cbc68xw8hr9lxk3dfjlgxi62xl7")))

(define-public crate-chalk-ir-0.65.0 (c (n "chalk-ir") (v "0.65.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.65.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "064r1cfcpgqjv4j7yf012dsnwinvprcs4mv0w37k6xs4jw39gvx4")))

(define-public crate-chalk-ir-0.66.0 (c (n "chalk-ir") (v "0.66.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.66.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0sci6vf8lvvvk6ddxz8jqj1mhpn8mkk6655fdpjs846s9j1664c8")))

(define-public crate-chalk-ir-0.67.0 (c (n "chalk-ir") (v "0.67.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.67.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "15f49fvjvnjynlrjr3i06v11qch1m84hb3ycf8zhsx04an8dldj2")))

(define-public crate-chalk-ir-0.68.0 (c (n "chalk-ir") (v "0.68.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.68.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0riyv4fjmxys8gsyrikn9in34cjcgwgj55qyaj9w9fld8kqxbmqr")))

(define-public crate-chalk-ir-0.69.0 (c (n "chalk-ir") (v "0.69.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.69.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0hjv87gvag47a68xcmmbrbjramiqf85pmf4177ajvbbpppl6y1lm")))

(define-public crate-chalk-ir-0.70.0 (c (n "chalk-ir") (v "0.70.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.70.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0q8lnymagqmji8si19lcn76x6kb0cr0fl4g1pap3vbr31lk5pz9y")))

(define-public crate-chalk-ir-0.71.0 (c (n "chalk-ir") (v "0.71.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.71.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1q6bh6vljhwjyl13axk70j0gjhy7d9gyyb59z3dkdhgwyd0v1yf0")))

(define-public crate-chalk-ir-0.72.0 (c (n "chalk-ir") (v "0.72.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.72.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "10v2ckkwp9g1hrir7zxxrvbgbbzzy70h7603qn9zn9smy2ia1yrr")))

(define-public crate-chalk-ir-0.73.0 (c (n "chalk-ir") (v "0.73.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.73.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "055wc7p85b5cbx2s9ym8q615zxs6lb5qmhk9zvjmr5znp758v6az")))

(define-public crate-chalk-ir-0.74.0 (c (n "chalk-ir") (v "0.74.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.74.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0yday8dwmvc0gfdb1161a054vw8z6g53l6jjvzwzplv0l2s4d05h")))

(define-public crate-chalk-ir-0.75.0 (c (n "chalk-ir") (v "0.75.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.75.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "12iyziaw14qb0wz3pqx70qwqa9r0qsi5d4y6j0g32yabs2hyay9b")))

(define-public crate-chalk-ir-0.76.0 (c (n "chalk-ir") (v "0.76.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.76.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "06682gmvzl1m5r6993zq6s1zid6fb3jlm8mxr6ib9m7dy71xbjpk")))

(define-public crate-chalk-ir-0.77.0 (c (n "chalk-ir") (v "0.77.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.77.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "18pcfc33hx2sx69ndxdsk8xniqsvmsfsm5sfpmks6vncl2spgw4y")))

(define-public crate-chalk-ir-0.78.0 (c (n "chalk-ir") (v "0.78.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.78.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1p5lh2pci6075n3qgsaxvrn8vp9nqjqqjkb067lh2aa7nfxcv4bb")))

(define-public crate-chalk-ir-0.79.0 (c (n "chalk-ir") (v "0.79.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.79.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0bl8dfkl6qavx766arw78frwga9863z0niid59b5y1yk664iwmyd")))

(define-public crate-chalk-ir-0.80.0 (c (n "chalk-ir") (v "0.80.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.80.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1wdm0z0n73bly0wn28d0gfxw4gjn9qqf89k4m3yihdpj91askn4j")))

(define-public crate-chalk-ir-0.81.0 (c (n "chalk-ir") (v "0.81.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.81.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1mfhmckz0b58lixkbxnr1pw700gli5dcrq34grddsb7ga8vgla71")))

(define-public crate-chalk-ir-0.82.0 (c (n "chalk-ir") (v "0.82.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.82.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1997gyzbqidhfma920n6iayl1iydxw9v27jrqw46qrnyz48jrpyx")))

(define-public crate-chalk-ir-0.83.0 (c (n "chalk-ir") (v "0.83.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.83.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "14gapzq6h04mibq0yk3apxklllip5bk8s2pjb8mfrn3rsl3j3m1d")))

(define-public crate-chalk-ir-0.84.0 (c (n "chalk-ir") (v "0.84.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.84.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0609c84l5071r7mbnrq32rlgv1f5gs2x5xna2mg0mrdm4wq7d4fk")))

(define-public crate-chalk-ir-0.85.0 (c (n "chalk-ir") (v "0.85.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.85.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1npjcfbgrfwpdpms9jsnqb63mx6768nbihq43lxqzcaw97gkgs3k")))

(define-public crate-chalk-ir-0.86.0 (c (n "chalk-ir") (v "0.86.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.86.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1qb0p5xcch30dq4r7i03s9wc9d99lkrh2fnfxsq0g1d4fs61201q")))

(define-public crate-chalk-ir-0.87.0 (c (n "chalk-ir") (v "0.87.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.87.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0f08jndhkdaprswqgi2shc35mjgjf8rm8550kyqbszsfzzg5baj3")))

(define-public crate-chalk-ir-0.88.0 (c (n "chalk-ir") (v "0.88.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.88.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1yykwppp9q6nsj77cji75jbrgy06f0aabvnck1i9a5k905r557pk")))

(define-public crate-chalk-ir-0.89.0 (c (n "chalk-ir") (v "0.89.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.89.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0fqa3v0rsp0y0jjg7yjh192k5wkylk4z314g9v0jhm398414hfs7")))

(define-public crate-chalk-ir-0.90.0 (c (n "chalk-ir") (v "0.90.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.90.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0x3cmijgxgm7psbdwdsh45lfmdklaamvb8mxvpww9h4siw9sj5v3")))

(define-public crate-chalk-ir-0.91.0 (c (n "chalk-ir") (v "0.91.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.91.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0yz593j89gyckzvanj0rkp36j7zbg4ppkfsisllbh8v8hy9bw948")))

(define-public crate-chalk-ir-0.92.0 (c (n "chalk-ir") (v "0.92.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.92.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0akxnwhnq42g95lfbvz1r7mf879ghpdm1gglak6hzvd88qhxwmla")))

(define-public crate-chalk-ir-0.93.0 (c (n "chalk-ir") (v "0.93.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.93.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1pq0dndpc9dv34r788l857868qxr1z5lld44gy7nnxacgm01fp6n")))

(define-public crate-chalk-ir-0.94.0 (c (n "chalk-ir") (v "0.94.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.94.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0g5znrvvfm8b8yma6053vmwaj2jzif4g3p9d9ai80s8kv73cnih9")))

(define-public crate-chalk-ir-0.95.0 (c (n "chalk-ir") (v "0.95.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.95.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "115lkhswhhylkr6709v87l502ipzy5607ifvyz89imcb4dcic7ly")))

(define-public crate-chalk-ir-0.96.0 (c (n "chalk-ir") (v "0.96.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.96.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0vcn79bsy12zacvqiw6086k2hs9q0qyy1p0l88wp9zv3vln0qmgz")))

(define-public crate-chalk-ir-0.97.0 (c (n "chalk-ir") (v "0.97.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chalk-derive") (r "=0.97.0") (d #t) (k 0)))) (h "06f109rs5y06g4l58la7ch2w19f31x91x404d8kgg45id4slj66v")))

