(define-module (crates-io ch al chalk-macros) #:use-module (crates-io))

(define-public crate-chalk-macros-0.1.0 (c (n "chalk-macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "0zh43p2dlyglsm9s5iclx47hqi3290hg1dxf1grajfl5sspkami9")))

(define-public crate-chalk-macros-0.10.0 (c (n "chalk-macros") (v "0.10.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0cpb3znj0krdkiyyhy0n0z2k7ns8yaf932jdz7ya2874138q4iwy")))

