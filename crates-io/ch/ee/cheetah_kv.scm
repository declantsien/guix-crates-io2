(define-module (crates-io ch ee cheetah_kv) #:use-module (crates-io))

(define-public crate-cheetah_kv-0.1.0 (c (n "cheetah_kv") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "murmur3") (r "^0.5.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gp2v7bhr3alhvfyibk9l7y484xdb2v4xjyky5sr4jhp77lrgflb")))

