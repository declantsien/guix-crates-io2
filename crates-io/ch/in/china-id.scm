(define-module (crates-io ch in china-id) #:use-module (crates-io))

(define-public crate-china-id-0.1.0 (c (n "china-id") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1042yhpr27qa5jbjf6v62hszac7wkifph25sk983s8ayxnkf5cvm") (f (quote (("default")))) (y #t)))

(define-public crate-china-id-0.1.1 (c (n "china-id") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0ag7i81b9yp8qvwncyi2k5p0mgkghbjzan2vdvg1f1xqm10ax8zh") (y #t)))

(define-public crate-china-id-0.1.2 (c (n "china-id") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1yfim0giqkpcvwimr7v6l0lgj8x2375i8040yjg4cjm7k6pis2yh")))

