(define-module (crates-io ch in chinese_segmenter) #:use-module (crates-io))

(define-public crate-chinese_segmenter-0.1.0 (c (n "chinese_segmenter") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "character_converter") (r "^0.1.0") (d #t) (k 0)))) (h "0s3m5cj29qjw8p8j0ffqb0c89r1wsmx2qazc57yshrzjpnrb8cb8")))

(define-public crate-chinese_segmenter-0.1.1 (c (n "chinese_segmenter") (v "0.1.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "character_converter") (r "^0.1.0") (d #t) (k 0)))) (h "1zg5czr1d8dcgcfy8yaxsshvpfv4zh0h68yzw2hlygr34v60j7gy")))

(define-public crate-chinese_segmenter-0.1.2 (c (n "chinese_segmenter") (v "0.1.2") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "character_converter") (r "^0.1.0") (d #t) (k 0)))) (h "0q738xws2vf72mq3abpm9pm8nc4fji80vigrc0xdi5flvf0cmywq")))

(define-public crate-chinese_segmenter-1.0.0 (c (n "chinese_segmenter") (v "1.0.0") (d (list (d (n "character_converter") (r "^2.1.0") (d #t) (k 0)))) (h "0h5lp2rd5acbisrgh6w18vh777248b1l0vrc7hxdg13rp36sq2r9")))

(define-public crate-chinese_segmenter-1.0.1 (c (n "chinese_segmenter") (v "1.0.1") (d (list (d (n "character_converter") (r "^2.1.2") (d #t) (k 0)))) (h "1g1qhq3lmq52ir1qi4vpma1l8z9py5p54nqhl4mqc6mgrvh10d6h")))

