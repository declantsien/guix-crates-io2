(define-module (crates-io ch in chinese_detection) #:use-module (crates-io))

(define-public crate-chinese_detection-0.1.0 (c (n "chinese_detection") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)))) (h "00s0p1wrrqbdbvkmmwjqw25vhj0glkggb5cxdsdlbgn2b0vm2zxd")))

(define-public crate-chinese_detection-1.0.0 (c (n "chinese_detection") (v "1.0.0") (d (list (d (n "bincode") (r ">=1.2.1, <2.0.0") (d #t) (k 0)))) (h "1ymfqw6a9r86r2zxijwjl5wmizm90lsnyzxzvdjc1gcnh827crpq")))

(define-public crate-chinese_detection-2.0.0 (c (n "chinese_detection") (v "2.0.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "0a7xkhjdz2m33m93sdyk1asql984rn01sgmp3h39r8barglj9naf")))

(define-public crate-chinese_detection-2.0.1 (c (n "chinese_detection") (v "2.0.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "0yjsc8avj9p9pvnrk8dxb4rdy5glri7i786hw078pwfwjcqccb2n")))

(define-public crate-chinese_detection-2.0.2 (c (n "chinese_detection") (v "2.0.2") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "1f2igimziskc48aqygylnwlia04b7yqjb4ijvrvyb8fhh1kz4arj")))

(define-public crate-chinese_detection-2.0.3 (c (n "chinese_detection") (v "2.0.3") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "02ajp43hvlylx76li6djzqjbdv8shbbqwyjhngbfv5m8bmdhyjin")))

(define-public crate-chinese_detection-2.0.4 (c (n "chinese_detection") (v "2.0.4") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "1m2czgndh6pj7ldyi5f8y02z557dpd40vh530kq61hmx7j7pi425")))

