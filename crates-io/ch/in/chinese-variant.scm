(define-module (crates-io ch in chinese-variant) #:use-module (crates-io))

(define-public crate-chinese-variant-1.0.0 (c (n "chinese-variant") (v "1.0.0") (h "0m8j60v8cvlwqf1ysw76scxjww8dr435gdyq3h67h7b1xv7xq35c")))

(define-public crate-chinese-variant-1.0.1 (c (n "chinese-variant") (v "1.0.1") (h "0ic0960bqcq9zixz3n8sk7fx9w6id27597jppzsq8n5p4n5kmvdh")))

(define-public crate-chinese-variant-1.0.2 (c (n "chinese-variant") (v "1.0.2") (h "0j8fbwh45nhmaqqmmsxdc2az0agmy5dipzvcd08a47r370jxwdgx")))

(define-public crate-chinese-variant-1.0.3 (c (n "chinese-variant") (v "1.0.3") (h "15a94swamq3nlrvmm6wgqp8zahl3wbd9ppqapb8gvbdmnjpksp7q")))

(define-public crate-chinese-variant-1.0.4 (c (n "chinese-variant") (v "1.0.4") (h "1ifdjn5h43v8jsy68z00aj094j3bgk6wqzrmf7hcmjy3azh7mg85")))

(define-public crate-chinese-variant-1.0.5 (c (n "chinese-variant") (v "1.0.5") (h "06nrimi52sqnb48km6cq825mr0jgdrjz7mq6f2p02d6x67i274pl")))

(define-public crate-chinese-variant-1.0.6 (c (n "chinese-variant") (v "1.0.6") (h "18imixmx6kxhw21iv3zrp537x5nmwfq4ac8wfkwgkk1m2v12ag59")))

(define-public crate-chinese-variant-1.0.7 (c (n "chinese-variant") (v "1.0.7") (h "0n76f7pfv2659j5zpavclk0xcqjwamgpfcs3xmf52664fskpzyj0")))

(define-public crate-chinese-variant-1.0.8 (c (n "chinese-variant") (v "1.0.8") (h "0s6y2gbbngnbpcs95ci8h8rbhh51lnrhm5v1dv86ry7szgci3s1c")))

(define-public crate-chinese-variant-1.0.9 (c (n "chinese-variant") (v "1.0.9") (h "1xq0yy7cqh0hj7van9ppc71acrpv9qz5svlmf9wrbazgi6di7smf")))

(define-public crate-chinese-variant-1.0.10 (c (n "chinese-variant") (v "1.0.10") (h "0k9l3agzllbgmhc1bq85hgm83zkf1ww3vy8qbqh6yvna1fcfaaal") (r "1.56")))

(define-public crate-chinese-variant-1.1.0 (c (n "chinese-variant") (v "1.1.0") (d (list (d (n "enum-ordinalize") (r "^4") (o #t) (d #t) (k 0)))) (h "0rlxccqb3fql1biqjni46bs1z7206m9yqmky2ncdgpldrvvag52p") (f (quote (("default")))) (y #t) (s 2) (e (quote (("enum-ordinalize" "dep:enum-ordinalize")))) (r "1.56")))

(define-public crate-chinese-variant-1.1.1 (c (n "chinese-variant") (v "1.1.1") (d (list (d (n "enum-ordinalize") (r "^4") (o #t) (d #t) (k 0)))) (h "10j20pfqd56v5kgpfmxgg498x07l86y4ydz7y0vpfqkszqkhf5qj") (f (quote (("default")))) (r "1.56")))

(define-public crate-chinese-variant-1.1.2 (c (n "chinese-variant") (v "1.1.2") (d (list (d (n "enum-ordinalize") (r "^4") (o #t) (d #t) (k 0)))) (h "0bl5xrgv0lakbhfrkma4225z26fnzldms5hl690wakvhn0b2xpqp") (f (quote (("default")))) (r "1.56")))

(define-public crate-chinese-variant-1.1.3 (c (n "chinese-variant") (v "1.1.3") (d (list (d (n "enum-ordinalize") (r "^4") (o #t) (d #t) (k 0)))) (h "12s91vg2m9wfs9b3f0q2alj9am08y7r2prb0szg3fwjh8m8lg23m") (f (quote (("default")))) (r "1.60")))

