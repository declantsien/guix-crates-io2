(define-module (crates-io ch in chinese-rand) #:use-module (crates-io))

(define-public crate-chinese-rand-0.1.0 (c (n "chinese-rand") (v "0.1.0") (d (list (d (n "chinese-format") (r "^0.8.0") (d #t) (k 0)) (d (n "digit-sequence") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "fastrand") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "speculate2") (r "^0.2") (d #t) (k 2)))) (h "0c7j0dxd71078zfybzrfy0ii877nz8qabba3kpysmpzipchki8f7") (f (quote (("gregorian" "digit-sequence" "chinese-format/gregorian") ("default" "fastrand") ("currency" "chinese-format/currency")))) (s 2) (e (quote (("digit-sequence" "dep:digit-sequence" "chinese-format/digit-sequence"))))))

