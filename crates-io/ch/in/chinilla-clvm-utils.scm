(define-module (crates-io ch in chinilla-clvm-utils) #:use-module (crates-io))

(define-public crate-chinilla-clvm-utils-0.1.16 (c (n "chinilla-clvm-utils") (v "0.1.16") (d (list (d (n "clvmr_chinilla") (r "=0.2.0") (d #t) (k 0)))) (h "1x4dgzpynygjf1jgg30agwdkl34ydk2vs3ipdk9rjbn78acz8f6g") (y #t)))

(define-public crate-chinilla-clvm-utils-0.1.17 (c (n "chinilla-clvm-utils") (v "0.1.17") (d (list (d (n "clvmr_chinilla") (r "=0.2.0") (d #t) (k 0)))) (h "0hm3mqf26k5glh06nbs68km5l4n340p82rmjqjc64pgkis7vjipg") (y #t)))

(define-public crate-chinilla-clvm-utils-0.1.18 (c (n "chinilla-clvm-utils") (v "0.1.18") (d (list (d (n "clvmr_chinilla") (r "=0.2.0") (d #t) (k 0)))) (h "141633r8qr6qizcfxxxp9s1zwzn917p75hd41nz4s3shc7z5y0ym") (y #t)))

(define-public crate-chinilla-clvm-utils-0.1.19 (c (n "chinilla-clvm-utils") (v "0.1.19") (d (list (d (n "clvmr_chinilla") (r "=0.1.24") (d #t) (k 0)))) (h "1pvpv98hchcri1aw3g8ddw5njpjy4scd4ml5zqpqlyrq6bvrfjyd") (y #t)))

(define-public crate-chinilla-clvm-utils-0.1.21 (c (n "chinilla-clvm-utils") (v "0.1.21") (d (list (d (n "clvmr") (r "=0.1.24") (d #t) (k 0)))) (h "1x50vs182qm43wg18kf23ncfgk4dpgk60himc6c3p9b4liligy6p") (y #t)))

