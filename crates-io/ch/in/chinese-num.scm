(define-module (crates-io ch in chinese-num) #:use-module (crates-io))

(define-public crate-chinese-num-0.1.0 (c (n "chinese-num") (v "0.1.0") (h "0aflyjgicm83bl4pkda4za61h0qny0pd6siazf0zfyllhp1by50s")))

(define-public crate-chinese-num-0.1.1 (c (n "chinese-num") (v "0.1.1") (h "1my9927859jgh5zdjzb5l9d3m7cyg38a76l6il2pa2y7fsah3phm")))

(define-public crate-chinese-num-0.1.2 (c (n "chinese-num") (v "0.1.2") (h "0i1jq54mpvs4j6j7p7lhzjiiwpkfm9w8m1vx67yp2ma4k2ylzgbz")))

