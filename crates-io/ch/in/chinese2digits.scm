(define-module (crates-io ch in chinese2digits) #:use-module (crates-io))

(define-public crate-chinese2digits-1.0.0 (c (n "chinese2digits") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)))) (h "09sbh2dr1l0403s4bxyjsmv3wd3d5kpklhschfci455fb5bynj10")))

