(define-module (crates-io ch in chinese_holiday) #:use-module (crates-io))

(define-public crate-chinese_holiday-2004.0.1 (c (n "chinese_holiday") (v "2004.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (k 2)) (d (n "chrono-tz") (r "^0.8") (o #t) (k 0)) (d (n "chrono-tz") (r "^0.8") (k 2)))) (h "02kyn0jryzr7p5xr3qz475py17w45i3ysyy2s34w0yr39l5vc4l1") (f (quote (("default" "chrono-compatible") ("chrono-compatible" "chrono" "chrono-tz"))))))

(define-public crate-chinese_holiday-2023.0.1 (c (n "chinese_holiday") (v "2023.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (k 2)) (d (n "chrono-tz") (r "^0.8") (o #t) (k 0)) (d (n "chrono-tz") (r "^0.8") (k 2)))) (h "1hyq4diadlzplh4zj0dyxbcj6p34lsqmkm2n422hqy5bv281mm2q") (f (quote (("default" "chrono-compatible") ("chrono-compatible" "chrono" "chrono-tz"))))))

(define-public crate-chinese_holiday-2024.0.1 (c (n "chinese_holiday") (v "2024.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (k 2)) (d (n "chrono-tz") (r "^0.8") (o #t) (k 0)) (d (n "chrono-tz") (r "^0.8") (k 2)))) (h "0aq9xkpvh5kgvsx8470jxn0ydwjs20axl1jb0jmxv3k18ff4z18z") (f (quote (("default" "chrono-compatible") ("chrono-compatible" "chrono" "chrono-tz")))) (y #t)))

(define-public crate-chinese_holiday-2024.0.2 (c (n "chinese_holiday") (v "2024.0.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (k 2)) (d (n "chrono-tz") (r "^0.8") (o #t) (k 0)) (d (n "chrono-tz") (r "^0.8") (k 2)))) (h "0vhf39sljnb2r47hcvpma76p55s38nkjbq5qm0i86kxgr73fr341") (f (quote (("default" "chrono-compatible") ("chrono-compatible" "chrono" "chrono-tz"))))))

(define-public crate-chinese_holiday-2024.0.3 (c (n "chinese_holiday") (v "2024.0.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (k 2)) (d (n "chrono-tz") (r "^0.8") (o #t) (k 0)) (d (n "chrono-tz") (r "^0.8") (k 2)))) (h "13mkn0vyyc809gi4ap200v05hgfg5z402rnpaxly8wj660clpmx6") (f (quote (("default" "chrono-compatible") ("chrono-compatible" "chrono" "chrono-tz"))))))

