(define-module (crates-io ch in chinese-numerals) #:use-module (crates-io))

(define-public crate-chinese-numerals-0.1.0 (c (n "chinese-numerals") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1ilm9b8xvmz4fjil2szlmasacxffl0cdi5q4y6fci6nppbn1sjfx") (f (quote (("bigint" "num-bigint" "num-integer" "num-traits"))))))

(define-public crate-chinese-numerals-0.1.1 (c (n "chinese-numerals") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "09f5p1fa11iq0x7m69zxgqjr84adzazzi7n1j24k617691jnhffh") (f (quote (("bigint" "num-bigint" "num-integer" "num-traits"))))))

(define-public crate-chinese-numerals-0.1.2 (c (n "chinese-numerals") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1zlv5snz9hwm7zm8n1yc4x13nvczq8b4rxcmbn1wbf7x86s9qsxm") (f (quote (("bigint" "num-bigint" "num-integer" "num-traits"))))))

(define-public crate-chinese-numerals-0.2.0 (c (n "chinese-numerals") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0dgz8lnqpsyqq0mfi6a27zk7mpxr0s932h3lixlf0iyr1gffm9lv") (f (quote (("bigint" "num-bigint" "num-integer" "num-traits"))))))

(define-public crate-chinese-numerals-0.2.1 (c (n "chinese-numerals") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "10km7fwlr5ijf620qjvygvbc6z1py8vrvdja620wfc4g5ggk40i9") (f (quote (("bigint" "num-bigint" "num-integer" "num-traits"))))))

(define-public crate-chinese-numerals-0.2.2 (c (n "chinese-numerals") (v "0.2.2") (d (list (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "07ldg4604ml7za970nmccxfymnivwm0zlf9ypvp5asr5fl2s99bn") (f (quote (("bigint" "num-bigint" "num-integer" "num-traits"))))))

