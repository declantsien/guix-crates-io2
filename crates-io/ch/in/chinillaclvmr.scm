(define-module (crates-io ch in chinillaclvmr) #:use-module (crates-io))

(define-public crate-chinillaclvmr-0.2.0 (c (n "chinillaclvmr") (v "0.2.0") (d (list (d (n "bls12_381") (r "=0.7.0") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "=0.4.3") (d #t) (k 0)) (d (n "num-integer") (r "=0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "=0.2.15") (d #t) (k 0)) (d (n "openssl") (r "=0.10.40") (f (quote ("vendored"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "=0.10.2") (d #t) (k 0)))) (h "11761pw9v2c588ddyd3hprh02jmq7lq0clsycscsk13rvbv42smw") (y #t)))

(define-public crate-chinillaclvmr-0.1.24 (c (n "chinillaclvmr") (v "0.1.24") (d (list (d (n "bls12_381") (r "=0.7.0") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "=0.4.3") (d #t) (k 0)) (d (n "num-integer") (r "=0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "=0.2.15") (d #t) (k 0)) (d (n "openssl") (r "=0.10.40") (f (quote ("vendored"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "=0.10.2") (d #t) (k 0)))) (h "0gavjk6gaa3c2khfym2bg5pjx1vk9bxd50n2h4wrgvqwf8bk12zj") (y #t)))

