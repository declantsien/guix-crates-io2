(define-module (crates-io ch in chinese-number) #:use-module (crates-io))

(define-public crate-chinese-number-0.1.0 (c (n "chinese-number") (v "0.1.0") (h "0j3czifi2a2ibpx704821ag8pmxdn5j77c82zfbd17krhzz38v4h")))

(define-public crate-chinese-number-0.1.1 (c (n "chinese-number") (v "0.1.1") (h "0f4flppfkr112784xfxaaagnjh6zqicn7ng8855szfwyigq4zcnc")))

(define-public crate-chinese-number-0.1.2 (c (n "chinese-number") (v "0.1.2") (h "1xafd49vp5r9gpvh7m1ivssmy0ysxk5gykh63fshglv1a6zxpi30")))

(define-public crate-chinese-number-0.1.3 (c (n "chinese-number") (v "0.1.3") (h "0w8f9amn48s247ivpjkirgr37myjs0fggfs988lh6nhcdqgv0vim")))

(define-public crate-chinese-number-0.2.0 (c (n "chinese-number") (v "0.2.0") (h "1cy7dslx0xpn2wq071j62rhqdbimflmlvpmbsdajdng67cldh0hk")))

(define-public crate-chinese-number-0.2.1 (c (n "chinese-number") (v "0.2.1") (h "1ic7swvlyk0h333k6kw1byar3abj3vja4a9rgs12ddy5hkaw2awi")))

(define-public crate-chinese-number-0.3.0 (c (n "chinese-number") (v "0.3.0") (h "0j207sn8yjz6jj38mnm3qznj205z07cqf1vxvx40gnd4w52xxchd")))

(define-public crate-chinese-number-0.4.0 (c (n "chinese-number") (v "0.4.0") (h "1pbjk2h63smhyr9dbnljmfdpk829550cp2yc7bpl818kh9q24gks")))

(define-public crate-chinese-number-0.4.1 (c (n "chinese-number") (v "0.4.1") (h "16q665rhfyg5idd03z2qd4as0xnimgcdlc91yw8zpk7cy4v52708")))

(define-public crate-chinese-number-0.4.2 (c (n "chinese-number") (v "0.4.2") (h "0bhhj604dpg5rgbf6v6dyfhmmmyqq08mkxvwrblfq3dvj90l5b4g")))

(define-public crate-chinese-number-0.4.3 (c (n "chinese-number") (v "0.4.3") (h "0wba26m3i57442psqfrn4igy03x7lgwf51s5pcvmwrwvqs4a594d")))

(define-public crate-chinese-number-0.4.4 (c (n "chinese-number") (v "0.4.4") (h "1mc3ncbdkg5807kycqfsd8s9jja6cr4vnl6v3plb5sh04agw4x4y")))

(define-public crate-chinese-number-0.4.5 (c (n "chinese-number") (v "0.4.5") (h "1x2lhgvs92v9bl6p4r7fgw2zwn2kszqkg7c37sg5nkmf82psqii4")))

(define-public crate-chinese-number-0.4.6 (c (n "chinese-number") (v "0.4.6") (h "0i22w6jp64fi6wr670n5pjwh9i3gflgjy6spd54rqnhxxml59cbf")))

(define-public crate-chinese-number-0.4.7 (c (n "chinese-number") (v "0.4.7") (h "03vqgyljib4qc331j2s6q1dy9hb27b497f3vfn5z4j9wj06w8fkr")))

(define-public crate-chinese-number-0.4.8 (c (n "chinese-number") (v "0.4.8") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)))) (h "0509lw8qls5srdzq4yl61gqwcvv9l4gszgfwd7q98nm8rfaiy0wb")))

(define-public crate-chinese-number-0.5.0 (c (n "chinese-number") (v "0.5.0") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)))) (h "04z07xnv1xlhpjn2658bj1r8h799r6dj8n6pivyny6qc4rfkvw5q")))

(define-public crate-chinese-number-0.5.1 (c (n "chinese-number") (v "0.5.1") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)))) (h "18kzxagj4g1zglvcsm1mijd4a52lmhnmqvlwcrx13h06g3qnas4r")))

(define-public crate-chinese-number-0.5.2 (c (n "chinese-number") (v "0.5.2") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)))) (h "0v7aqz0ahq2hwza7z76g3l46krs7pg2a4sfcsr643imv4jpbmjan")))

(define-public crate-chinese-number-0.5.3 (c (n "chinese-number") (v "0.5.3") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)))) (h "014k2fydipi3c98y3jsijj1d0imgi5xi0lif5pxsrkz2r9cphjcw")))

(define-public crate-chinese-number-0.5.4 (c (n "chinese-number") (v "0.5.4") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)))) (h "1rvkj84pxrrhybp1l1b55fch5mbzqbjdfi1xadxqpbrmw92n3ddk")))

(define-public crate-chinese-number-0.5.5 (c (n "chinese-number") (v "0.5.5") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)))) (h "1sighshwvpd9rv6m5gq2ydlahphzvjnbmrbw6svvc475n0bn7gdl")))

(define-public crate-chinese-number-0.5.6 (c (n "chinese-number") (v "0.5.6") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)))) (h "1q2h4bya0z5czlrdajycsdbyf3x9lnilsamyvyc08rrx2053l454")))

(define-public crate-chinese-number-0.5.7 (c (n "chinese-number") (v "0.5.7") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)))) (h "0045dc4g2gmgwj13q4gij3q1rhpgljxr40b9bnfl8p3q0kn3scqv")))

(define-public crate-chinese-number-0.5.8 (c (n "chinese-number") (v "0.5.8") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)))) (h "0lgwvz8mnhc7y8ba454gm3amwpy42k46yi3v8hk6xafhj8fayzdv")))

(define-public crate-chinese-number-0.6.0 (c (n "chinese-number") (v "0.6.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)))) (h "1wf385ks3v5hfl507mk3ivnkvpjb76634dy0plxxmmgvzqq328kl") (f (quote (("std") ("default" "std"))))))

(define-public crate-chinese-number-0.6.1 (c (n "chinese-number") (v "0.6.1") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)))) (h "1961pskf4h9nbw78l71xbkq5x0a4zh95lpb7wdiqj2hbi2n5ljfv") (f (quote (("std") ("default" "std"))))))

(define-public crate-chinese-number-0.6.2 (c (n "chinese-number") (v "0.6.2") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)))) (h "194gsrf7ymzjvpn874x3c2wr38pgcmbxp6r4bgdcc4bnxrvfwzxb") (f (quote (("std") ("default" "std"))))))

(define-public crate-chinese-number-0.6.3 (c (n "chinese-number") (v "0.6.3") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)))) (h "1bjxgqnqli6gzq325l2l403dx3vgsjankvqv6wkq1bp3agqh7lh5") (f (quote (("std") ("default" "std"))))))

(define-public crate-chinese-number-0.6.4 (c (n "chinese-number") (v "0.6.4") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)))) (h "0api8081wwn8ls3rdka02j5ii33phzg6ywrj6nfsvvqg12kq9s6d") (f (quote (("std") ("default" "std"))))))

(define-public crate-chinese-number-0.6.5 (c (n "chinese-number") (v "0.6.5") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)))) (h "1mpk7n7xhdvqvpdz7mxdqcjnv8rrl13xwwf2fgbp0gmd2kcag1il") (f (quote (("std") ("default" "std"))))))

(define-public crate-chinese-number-0.7.0 (c (n "chinese-number") (v "0.7.0") (d (list (d (n "chinese-numerals") (r "^0.2") (d #t) (k 0)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0czrvxzlwry4jf8qa0j18bacl4vzmxmiml55zf2ixd3c9m82wnin") (f (quote (("std") ("number-to-chinese" "num-bigint" "num-traits") ("default" "std" "number-to-chinese" "chinese-to-number") ("chinese-to-number" "num-traits")))) (y #t)))

(define-public crate-chinese-number-0.7.1 (c (n "chinese-number") (v "0.7.1") (d (list (d (n "assert-eq-float") (r "^0.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0x5avq2h3fz4wj89ngjjfhzqlz7hi8aj2l1rz1vz9mzcmkzqr6lz") (f (quote (("std") ("number-to-chinese" "num-bigint" "num-traits") ("default" "std" "number-to-chinese" "chinese-to-number") ("chinese-to-number" "num-traits")))) (r "1.58")))

(define-public crate-chinese-number-0.7.2 (c (n "chinese-number") (v "0.7.2") (d (list (d (n "assert-eq-float") (r "^0.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "1j42xj1si21ppx2q914k4vf7ih0wv1jk52h5bibsgfvxsanmk1mv") (f (quote (("std") ("number-to-chinese" "num-bigint" "num-traits") ("default" "std" "number-to-chinese" "chinese-to-number") ("chinese-to-number" "num-traits")))) (r "1.58")))

(define-public crate-chinese-number-0.7.3 (c (n "chinese-number") (v "0.7.3") (d (list (d (n "assert-eq-float") (r "^0.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "1jqwba6fp4ds276lch1v8j4bhwn5mvyv3ry7fr49200bn7pwkknr") (f (quote (("std") ("number-to-chinese" "num-bigint" "num-traits") ("default" "std" "number-to-chinese" "chinese-to-number") ("chinese-to-number" "num-traits")))) (r "1.58")))

(define-public crate-chinese-number-0.7.4 (c (n "chinese-number") (v "0.7.4") (d (list (d (n "assert-eq-float") (r "^0.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^4.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "1pb425ada1g2d6rawa5y44w1hi11hawwjh54bx5gsp933z1zzcmk") (f (quote (("std") ("number-to-chinese" "num-bigint" "num-traits") ("default" "std" "number-to-chinese" "chinese-to-number") ("chinese-to-number" "num-traits")))) (r "1.58")))

(define-public crate-chinese-number-0.7.5 (c (n "chinese-number") (v "0.7.5") (d (list (d (n "assert-eq-float") (r "^0.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^4.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0mbr0p6ppq3w9dyap2pdm43sw1ihbv6m9wjfwr2sp6a5phqdqvvb") (f (quote (("std") ("number-to-chinese" "num-bigint" "num-traits") ("default" "std" "number-to-chinese" "chinese-to-number") ("chinese-to-number" "num-traits")))) (r "1.58")))

(define-public crate-chinese-number-0.7.6 (c (n "chinese-number") (v "0.7.6") (d (list (d (n "assert-eq-float") (r "^0.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^4.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0rbaprl8c3lywnin25qg0fgw7182z8w67mhs7rqaglw2yh3vz07h") (f (quote (("std") ("number-to-chinese" "num-bigint" "num-traits") ("default" "std" "number-to-chinese" "chinese-to-number") ("chinese-to-number" "num-traits")))) (r "1.58")))

(define-public crate-chinese-number-0.7.7 (c (n "chinese-number") (v "0.7.7") (d (list (d (n "assert-eq-float") (r "^0.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^4.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0v5va8drix8gs2kv6pmv5yzdxhlpzrwkp3ch86kxdxj6cgpwmz29") (f (quote (("std") ("number-to-chinese" "num-bigint" "num-traits") ("default" "std" "number-to-chinese" "chinese-to-number") ("chinese-to-number" "num-traits")))) (r "1.60")))

