(define-module (crates-io ch in chinilla_py_streamable_macro) #:use-module (crates-io))

(define-public crate-chinilla_py_streamable_macro-0.1.2 (c (n "chinilla_py_streamable_macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "10j2inaq7ijb2x4lidrcw0vsw90j0m92z18i1kc1x6x8zaz7irm9") (y #t)))

(define-public crate-chinilla_py_streamable_macro-0.1.3 (c (n "chinilla_py_streamable_macro") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1sxyjq2q6xbw4sb0rnc17xyl58ffxfkal3y5lxz80rksgh42q3qh") (y #t)))

(define-public crate-chinilla_py_streamable_macro-0.1.4 (c (n "chinilla_py_streamable_macro") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1rcsg6lq1kabpxdra6b5lv2lcaqg2vac18j1vsn7m7ymcyqa2q6f") (y #t)))

