(define-module (crates-io ch in chinese_currency) #:use-module (crates-io))

(define-public crate-chinese_currency-0.1.0 (c (n "chinese_currency") (v "0.1.0") (h "0ss683ay13bxr23n5kjsb8wsnzfiv987w6icalbwb0qzv6c6fm00")))

(define-public crate-chinese_currency-0.2.0 (c (n "chinese_currency") (v "0.2.0") (h "1ad9rf1xqlwdbzad1chn60b1vnwds52g6sxyyqvzm5iz87mgn7na")))

