(define-module (crates-io ch in chinese-ner) #:use-module (crates-io))

(define-public crate-chinese-ner-0.0.0 (c (n "chinese-ner") (v "0.0.0") (d (list (d (n "crfsuite") (r "^0.2") (d #t) (k 0)) (d (n "jieba-rs") (r "^0.2") (d #t) (k 0)))) (h "1p9lkya79l0a8pxqxx65ihhg4x5df68x4g7jb3y4yj4947r1qvcq")))

(define-public crate-chinese-ner-0.1.0 (c (n "chinese-ner") (v "0.1.0") (d (list (d (n "crfsuite") (r "^0.2") (d #t) (k 0)) (d (n "jieba-rs") (r "^0.2") (d #t) (k 0)))) (h "177fnlzfb9qwb5b0yfpym5yy1bb1jfbg08vfixsbp0crd1w6ckaa")))

(define-public crate-chinese-ner-0.1.1 (c (n "chinese-ner") (v "0.1.1") (d (list (d (n "crfsuite") (r "^0.2") (d #t) (k 0)) (d (n "jieba-rs") (r "^0.2") (d #t) (k 0)))) (h "01ad9z65k2w21m0dx9kljid61g09ad5pwfzjja94lkqq6hw8pvx7")))

(define-public crate-chinese-ner-0.2.0 (c (n "chinese-ner") (v "0.2.0") (d (list (d (n "crfsuite") (r "^0.2") (d #t) (k 0)) (d (n "jieba-rs") (r "^0.3") (d #t) (k 0)))) (h "0gqyclml8b5gmv6g2d15da24q5hpdmhzilsbjiicv4kj0fnhi3a5")))

(define-public crate-chinese-ner-0.2.1 (c (n "chinese-ner") (v "0.2.1") (d (list (d (n "crfsuite") (r "^0.2") (d #t) (k 0)) (d (n "jieba-rs") (r "^0.4") (d #t) (k 0)))) (h "0sd4c320nicbf54ir8bkc5h7nq3qb701gm989l2fjaz9cp8v95b3")))

(define-public crate-chinese-ner-0.2.2 (c (n "chinese-ner") (v "0.2.2") (d (list (d (n "crfsuite") (r "^0.2") (d #t) (k 0)) (d (n "jieba-rs") (r "^0.5") (d #t) (k 0)))) (h "0f1842a47gi4845hzn3n8xx1j4nxwzp283xl7r70218cvkrhcdci")))

(define-public crate-chinese-ner-0.2.3 (c (n "chinese-ner") (v "0.2.3") (d (list (d (n "crfsuite") (r "^0.2") (d #t) (k 0)) (d (n "jieba-rs") (r "^0.6") (d #t) (k 0)))) (h "1ahaizx3j8qgkx9xaxpx0w39zpjp65p92syan6v5181dissgv75a") (f (quote (("default" "bundled-model") ("bundled-model"))))))

(define-public crate-chinese-ner-0.2.4 (c (n "chinese-ner") (v "0.2.4") (d (list (d (n "crfsuite") (r "^0.3") (d #t) (k 0)) (d (n "jieba-rs") (r "^0.6") (d #t) (k 0)))) (h "0a7c556m77rj25q3icq6gpjp26lrkvwj12asmv88jri6n82mjxnf") (f (quote (("default" "bundled-model") ("bundled-model"))))))

