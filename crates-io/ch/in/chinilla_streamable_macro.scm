(define-module (crates-io ch in chinilla_streamable_macro) #:use-module (crates-io))

(define-public crate-chinilla_streamable_macro-0.2.3 (c (n "chinilla_streamable_macro") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "036ska427cigc04xinm2pbw373djkz2ax35nv0crrkjwvf74pfw7") (y #t)))

(define-public crate-chinilla_streamable_macro-0.2.4 (c (n "chinilla_streamable_macro") (v "0.2.4") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0n1ynz8l65iyph3npn7kcygigg43dxbjafwn5sz8gzsn7fpdwj69") (y #t)))

(define-public crate-chinilla_streamable_macro-0.2.5 (c (n "chinilla_streamable_macro") (v "0.2.5") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "05fah7n9v6jlmmbkcmalmfkxv5pv0pb8yqgz3g6vbik8ivhxha57") (y #t)))

