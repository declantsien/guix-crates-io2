(define-module (crates-io ch in chinese-lunisolar-calendar) #:use-module (crates-io))

(define-public crate-chinese-lunisolar-calendar-0.1.0 (c (n "chinese-lunisolar-calendar") (v "0.1.0") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "1pr6php5aiyl160rhmx0lz3qmglkgq8ghj9c9f2r5kak2ma69za6")))

(define-public crate-chinese-lunisolar-calendar-0.1.1 (c (n "chinese-lunisolar-calendar") (v "0.1.1") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "11z08pk6ls1w4c9zd76gjraa79abwkxlaaixm8hx7c5nvz87kd9g")))

(define-public crate-chinese-lunisolar-calendar-0.1.2 (c (n "chinese-lunisolar-calendar") (v "0.1.2") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "0jfh810kqm0rkkn169j00c6bv590y3x6slm4lky6jf4c600ddjap")))

(define-public crate-chinese-lunisolar-calendar-0.1.3 (c (n "chinese-lunisolar-calendar") (v "0.1.3") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "0kkjsb2faz6dihxr1lm0q9vs8asr32z67isz36j307xy6gb30vl5")))

(define-public crate-chinese-lunisolar-calendar-0.1.4 (c (n "chinese-lunisolar-calendar") (v "0.1.4") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "1ikgc3l9zp9m84yvgjkxg5k36vr6hin2j6m03z7z9x6axi1h2dw5")))

(define-public crate-chinese-lunisolar-calendar-0.1.5 (c (n "chinese-lunisolar-calendar") (v "0.1.5") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "1pc4sxwbxnc33fbm6mlwxa66psy65903xk4c47ssj17hdq354273")))

(define-public crate-chinese-lunisolar-calendar-0.1.6 (c (n "chinese-lunisolar-calendar") (v "0.1.6") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "1k1lhz8d5dm3qsp68pp5baws57m1amcdj291zz1kkvxfmsw1y42n")))

(define-public crate-chinese-lunisolar-calendar-0.1.7 (c (n "chinese-lunisolar-calendar") (v "0.1.7") (d (list (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "07c0h62nf0fj2v9x4l18l1xy42lirgnqxv97nsnrjlk28m7bdcgs")))

(define-public crate-chinese-lunisolar-calendar-0.1.8 (c (n "chinese-lunisolar-calendar") (v "0.1.8") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "103vwncx4r4s9ba95sbhv3aj7k31qxp3szqhy7n1w1g3pn055r02")))

(define-public crate-chinese-lunisolar-calendar-0.1.9 (c (n "chinese-lunisolar-calendar") (v "0.1.9") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "1c59k88cj1sxgflxfkjmcgr18g3r8sksqj30y51x3vfif1xsdmmq")))

(define-public crate-chinese-lunisolar-calendar-0.1.10 (c (n "chinese-lunisolar-calendar") (v "0.1.10") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "05s15wamv0yi42arysl3bichh66xq1v9mh0gqq955lhphyxq89w3")))

(define-public crate-chinese-lunisolar-calendar-0.1.11 (c (n "chinese-lunisolar-calendar") (v "0.1.11") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "0bs934l8bcjjj0frzmbs52vi6qmihsy36lcsnwwc00llfh0scxpi")))

(define-public crate-chinese-lunisolar-calendar-0.1.12 (c (n "chinese-lunisolar-calendar") (v "0.1.12") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "0p2yc578i2h4alcjxj9r703s1ssw3nl3hfhym9hnjhabf0pff13g")))

(define-public crate-chinese-lunisolar-calendar-0.1.13 (c (n "chinese-lunisolar-calendar") (v "0.1.13") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "129gj7148ry80c420pwz89p1zg0p3yv6a6mwmpnfx7n0mw2xkxqh")))

(define-public crate-chinese-lunisolar-calendar-0.1.14 (c (n "chinese-lunisolar-calendar") (v "0.1.14") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "01p37dg43zgsnn3di4p1wgff14w7i991lhnag4gb47ypvbcyjnjn")))

(define-public crate-chinese-lunisolar-calendar-0.1.15 (c (n "chinese-lunisolar-calendar") (v "0.1.15") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1dqyk1lpr82lg17s6sscr2qcbd39080jycwbnk2289vqx97wxlqk")))

(define-public crate-chinese-lunisolar-calendar-0.1.16 (c (n "chinese-lunisolar-calendar") (v "0.1.16") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0k2ifnwavzn0cl93ybmbpf21vhmgs1621rci8z3143khc6k5r7hg")))

(define-public crate-chinese-lunisolar-calendar-0.1.17 (c (n "chinese-lunisolar-calendar") (v "0.1.17") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0s2g6qbcqm9y06166dmqxk17imp3r6hjx4375m3nfn2ydafrjbfb")))

(define-public crate-chinese-lunisolar-calendar-0.1.18 (c (n "chinese-lunisolar-calendar") (v "0.1.18") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0c3j25x489dwb71p46xpxwxcf1j7lc71dhriq7j14arkql35gbxl")))

(define-public crate-chinese-lunisolar-calendar-0.1.19 (c (n "chinese-lunisolar-calendar") (v "0.1.19") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0f7shgh4nxg5r2yrz01qkwsf8b90ymlyl1asjd7f2rxj6xj0cwpd")))

(define-public crate-chinese-lunisolar-calendar-0.1.20 (c (n "chinese-lunisolar-calendar") (v "0.1.20") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "07f8rgjqmqmkhpyy0ym70n9ww05ncnbm3px0pd8djngwp39v4nb9")))

(define-public crate-chinese-lunisolar-calendar-0.1.21 (c (n "chinese-lunisolar-calendar") (v "0.1.21") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1l51s51vdgmvhkxccmqiplz7khsnbrbpc0l6l8c3p0nqjb2g7yav")))

(define-public crate-chinese-lunisolar-calendar-0.1.22 (c (n "chinese-lunisolar-calendar") (v "0.1.22") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "03w34md2q4pa9ymzg8247f6i0s7xcpvfsgnqvyr7sil7r1clscyg")))

(define-public crate-chinese-lunisolar-calendar-0.1.23 (c (n "chinese-lunisolar-calendar") (v "0.1.23") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1k6jrs3hdz0rbfnwap2nmi855my8gd631c4gdndv9a5lvzjrvddi")))

(define-public crate-chinese-lunisolar-calendar-0.1.24 (c (n "chinese-lunisolar-calendar") (v "0.1.24") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1d3v3hvh2s18gkw55yibkbqshdarbsqshpmkmxsj1j40489mz1nr")))

(define-public crate-chinese-lunisolar-calendar-0.1.25 (c (n "chinese-lunisolar-calendar") (v "0.1.25") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0fpn1s3p8mhph3w095yhfv800462lq9vb3khbb65jmq3898f6j7v") (f (quote (("default" "ba-zi-weight") ("ba-zi-weight"))))))

(define-public crate-chinese-lunisolar-calendar-0.1.26 (c (n "chinese-lunisolar-calendar") (v "0.1.26") (d (list (d (n "assert-eq-float") (r "^0.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0wnzhg9g9908y9gc53hvqk002wk5qgnksgnkrw3bfjyq30ycfks8") (f (quote (("default" "ba-zi-weight") ("ba-zi-weight")))) (r "1.60")))

(define-public crate-chinese-lunisolar-calendar-0.1.27 (c (n "chinese-lunisolar-calendar") (v "0.1.27") (d (list (d (n "assert-eq-float") (r "^0.1") (d #t) (k 2)) (d (n "chinese-variant") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1l59f3kjgf41xfwz0vbxsj42z6jlncxakjvkqlaj3fydbac6i7nd") (f (quote (("default" "ba-zi-weight") ("ba-zi-weight")))) (r "1.60")))

(define-public crate-chinese-lunisolar-calendar-0.2.0 (c (n "chinese-lunisolar-calendar") (v "0.2.0") (d (list (d (n "chinese-variant") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)) (d (n "enum-ordinalize") (r "^4.2") (f (quote ("derive" "traits"))) (k 0)) (d (n "year-helper") (r "^0.2") (d #t) (k 0)))) (h "0xai0qypilmmv4n79g3whly915mygawvjrvbrjzz36d0cnmgsyzy") (f (quote (("std" "chrono/std") ("default" "std") ("ba-zi-weight")))) (r "1.67")))

