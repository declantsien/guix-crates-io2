(define-module (crates-io ch #{58}# ch58x) #:use-module (crates-io))

(define-public crate-ch58x-0.1.4 (c (n "ch58x") (v "0.1.4") (d (list (d (n "riscv") (r "^0.10.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1nybaqai9b009arf9lpldpr90la564plqnd0zrm8wvzxci6ilklc") (f (quote (("rt" "riscv-rt") ("default") ("ch58x"))))))

(define-public crate-ch58x-0.1.6 (c (n "ch58x") (v "0.1.6") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1lhbkpn393bnzk5h9mj1s49mshwck5hnmgymjgmj437h98psr0bn") (f (quote (("rt") ("default") ("ch58x"))))))

(define-public crate-ch58x-0.2.0 (c (n "ch58x") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1jm31513wa7mhqfjinhiap45xy24x8zffxyzl8k27nlbgpm0n82c") (f (quote (("rt") ("default") ("ch58x"))))))

(define-public crate-ch58x-0.3.0 (c (n "ch58x") (v "0.3.0") (d (list (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1x4h8n5h7x5iaqa9zxxfqj0q984qi84s9rn0r8ixpn7p2rq4b6rg") (f (quote (("rt") ("default") ("ch58x"))))))

