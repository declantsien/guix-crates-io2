(define-module (crates-io ch ml chmlib) #:use-module (crates-io))

(define-public crate-chmlib-1.0.0 (c (n "chmlib") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "chmlib-sys") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ximpkl2ar1wsd0nsc096h4g0nmr114pynfhdry1yb1fnxnra388")))

