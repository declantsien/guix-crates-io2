(define-module (crates-io ch lo chlog) #:use-module (crates-io))

(define-public crate-chlog-0.1.0 (c (n "chlog") (v "0.1.0") (h "0zrn4zyj7vicw3pjr3xwnir9cbn4rbdlkhg9qf5gybhmrsmdmd1l")))

(define-public crate-chlog-0.2.0 (c (n "chlog") (v "0.2.0") (h "1xygvb9qkji3c7q2j3jkd3r2hyqiingvsjcrin0d1mg4sgg1zfrm")))

(define-public crate-chlog-0.2.1 (c (n "chlog") (v "0.2.1") (h "0wpp47k25gknsf7rp4asn1hj8jnfz7b7pwim47yrv5iwzim71vcy")))

(define-public crate-chlog-0.2.2 (c (n "chlog") (v "0.2.2") (h "1i67cqd870fs4xzl3ix7xg8kdj2dp65x0xxs8fasck7vpz1w2qk2")))

(define-public crate-chlog-0.2.3 (c (n "chlog") (v "0.2.3") (h "0qiywnfp2c7lgnn6aj45wnx5c5s3ahxrdizga83ifwvw4cwayz98")))

(define-public crate-chlog-0.2.4 (c (n "chlog") (v "0.2.4") (h "0cki2f1g87xvdkxf86xr9afhiafwbvy7li2ygy844jg5rdn733q3")))

(define-public crate-chlog-0.2.5-beta.0 (c (n "chlog") (v "0.2.5-beta.0") (h "1p8q0zdnmkikyy9vl1avjplznpmxz6blksidimvcx6pl3l422crj")))

(define-public crate-chlog-0.2.5 (c (n "chlog") (v "0.2.5") (h "1cjxlqwpiqfbvc9bwhvxp36y9xkl8v6ypl275l30wkirzjhsfb0a")))

(define-public crate-chlog-0.3.0 (c (n "chlog") (v "0.3.0") (h "1vb3dmjhmam7bkwq5yzqrzlrdj0h7z6iaarmygvvjjy84dkz74ql")))

