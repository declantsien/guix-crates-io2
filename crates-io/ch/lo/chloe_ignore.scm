(define-module (crates-io ch lo chloe_ignore) #:use-module (crates-io))

(define-public crate-chloe_ignore-0.1.0 (c (n "chloe_ignore") (v "0.1.0") (h "0vhgih75lyd3c9qjy887pa75h8isqarlnmlh797h5acf64dy9dvm")))

(define-public crate-chloe_ignore-0.1.1 (c (n "chloe_ignore") (v "0.1.1") (h "10xhsakwfzi6vicmn8vgcwb1wasl0xggkxlc29gxy2kh9dhi3q6d")))

(define-public crate-chloe_ignore-0.1.2 (c (n "chloe_ignore") (v "0.1.2") (h "1pkh2xhgcjyvmzhmyiwirvan82wzkb2a8dpi8bqhis1wa2wlqpda")))

(define-public crate-chloe_ignore-0.1.3 (c (n "chloe_ignore") (v "0.1.3") (h "0jki14p0sz8453vb7z0gsjh7q2zfvfhzjyggzln9iah8znq6h5i0")))

(define-public crate-chloe_ignore-0.1.4 (c (n "chloe_ignore") (v "0.1.4") (h "0648bd8pgsxdd7js4mb2jfsdji1zcmq1abl7bs4x9bkcb4w604xf")))

