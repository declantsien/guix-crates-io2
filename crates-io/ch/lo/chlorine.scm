(define-module (crates-io ch lo chlorine) #:use-module (crates-io))

(define-public crate-chlorine-1.0.0 (c (n "chlorine") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(not(windows))") (k 0)))) (h "0p8fc2la3037dpz9cw4204il2f4h8w8im21n5x705m5l9f2ss0gb")))

(define-public crate-chlorine-1.0.1 (c (n "chlorine") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (t "cfg(not(windows))") (k 0)))) (h "1w089qgizw7ccw7zyczq3f6sjmddr6n4xkv66wq5qs01y9i2ph4l")))

(define-public crate-chlorine-1.0.2 (c (n "chlorine") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (t "cfg(not(any(windows, all(target_arch = \"wasm32\", not(any(target_env = \"wasi\", target_env = \"emscripten\"))))))") (k 0)))) (h "15zv9d3gl8ybsdszxhs8p0p6vl8cvzpd8hiii7rj9nc76ij9a3k3")))

(define-public crate-chlorine-1.0.3 (c (n "chlorine") (v "1.0.3") (d (list (d (n "libc") (r "^0.2") (t "cfg(not(any(windows, all(target_arch = \"wasm32\", not(any(target_env = \"wasi\", target_env = \"emscripten\"))))))") (k 0)))) (h "1jfzznfp7mdp08fzgafks2026knlqrn2jwgnnf1blw3zk6zy6jqd")))

(define-public crate-chlorine-1.0.4 (c (n "chlorine") (v "1.0.4") (h "0rasb66z1qn7pk7gnb8zb6r5vysyx2zw3ssxrxnp1lx9m19xzcvf") (y #t)))

(define-public crate-chlorine-1.0.5 (c (n "chlorine") (v "1.0.5") (h "0sfapvksknim81ry0n13xkjl5lpgy6dlhggkdqnj7gy2jn46skzj")))

(define-public crate-chlorine-1.0.6 (c (n "chlorine") (v "1.0.6") (h "0zhvl5s5cs924wjw6qd94afh5cd43x8p5hd136l93nq7aad7jxs7") (f (quote (("int_extras"))))))

(define-public crate-chlorine-1.0.7 (c (n "chlorine") (v "1.0.7") (h "1rhbmi70k1m2a2n7hrc385c3dlzcglbwz1l90p2vff8f2590ardx") (f (quote (("int_extras"))))))

(define-public crate-chlorine-1.0.8 (c (n "chlorine") (v "1.0.8") (h "0xl544hdp3zl2ln765qbagswfz7ncmcl24hzij4na5zvyba97i2d") (f (quote (("int_extras")))) (y #t)))

(define-public crate-chlorine-1.0.9 (c (n "chlorine") (v "1.0.9") (h "0s97jxylbgqzfqvlzfs5s18v83l0qh8kz0kawfbr6rcjld389v0i") (f (quote (("int_extras")))) (y #t)))

(define-public crate-chlorine-1.0.10 (c (n "chlorine") (v "1.0.10") (h "1v2230qas1xapywgmk8zfi8l8zd8mwafb8z2x867rbx8cvlnyivm") (f (quote (("int_extras"))))))

(define-public crate-chlorine-1.0.11 (c (n "chlorine") (v "1.0.11") (h "1a66cbnn6x6ml9cl8gg088zza9d2ylcws12rjpvynq2w9ff3h7cx") (f (quote (("int_extras"))))))

