(define-module (crates-io ch lo chlorate) #:use-module (crates-io))

(define-public crate-chlorate-0.1.0 (c (n "chlorate") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)))) (h "18md1qvaz888969jxhasw290m349ajwk812qw9696yy9671q85g5")))

(define-public crate-chlorate-0.1.1 (c (n "chlorate") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)))) (h "11apxxzn56jcbvxdpd9syz162c4638ai5fqksq47flri58n054rg")))

(define-public crate-chlorate-0.1.3 (c (n "chlorate") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)))) (h "16dldlhrx6y0jm50dykh6c7vs47h6l7kh0kk4vjz0lhdgjhii9y1")))

(define-public crate-chlorate-0.1.4 (c (n "chlorate") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)))) (h "13hv361shcsigkvsml3ibsqjsrg0apbs02fid7b7gh06mjgy68rz")))

(define-public crate-chlorate-0.1.5 (c (n "chlorate") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)))) (h "0fjqaplnyr2c1cxaxa1kii8w8z575zv5g17dad2zn3rdz4r9795x")))

(define-public crate-chlorate-0.1.6 (c (n "chlorate") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)))) (h "0kp81wvxcm1if3p0xnqxl221na1nbidwjfphfxiy1x11ysvh3ici")))

