(define-module (crates-io ch lo chloe) #:use-module (crates-io))

(define-public crate-chloe-0.0.1 (c (n "chloe") (v "0.0.1") (h "09nsqxbfgw7i1xn2jym2l915a3qcs1hlrx0rfp4n8wjj6s3ipak2")))

(define-public crate-chloe-0.0.2 (c (n "chloe") (v "0.0.2") (h "0ak5mzkxav0fdypkn8gxksvsv7c67hrwzgl5rqhldnyv8h6cppfx")))

