(define-module (crates-io ch ak chakracore-sys) #:use-module (crates-io))

(define-public crate-chakracore-sys-0.0.1 (c (n "chakracore-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0mvz1d6phqjzv5vww9hdr3f25ybbas60aj9r18fc5s7gwk5sf7qz") (f (quote (("shared"))))))

(define-public crate-chakracore-sys-0.0.2 (c (n "chakracore-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0s5whpxfsshsy9ybapp5s5mdaabm6cw2sa99kf5q56qqwhy62hkv") (f (quote (("shared"))))))

(define-public crate-chakracore-sys-0.1.0 (c (n "chakracore-sys") (v "0.1.0") (d (list (d (n "clang-sys") (r "^0.8.0") (d #t) (k 1)) (d (n "libbindgen") (r "^0.1.1") (f (quote ("llvm_stable"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "regex") (r "^0.1") (d #t) (k 1)))) (h "1r2byx8d2813crs5g1jnj1g0iix3ilhi1qx9mh73zfbzj42yz4vp") (f (quote (("static"))))))

(define-public crate-chakracore-sys-0.2.0 (c (n "chakracore-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.21.2") (f (quote ("llvm_stable"))) (d #t) (k 1)) (d (n "clang-sys") (r "^0.8.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "regex") (r "^0.1") (d #t) (k 1)))) (h "1vb2nvk0p398qp74rjz3jl2c5l4c4k82d0ybx1kdycnlzsk71il8") (f (quote (("static"))))))

(define-public crate-chakracore-sys-0.2.1 (c (n "chakracore-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)) (d (n "clang-sys") (r "^0.18.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "regex") (r "^0.2") (d #t) (k 1)))) (h "19dlpy20n4vky8fmvnck9vrsmzd9kyc6hb1q0cvxx0k2hv0yjlpp") (f (quote (("static"))))))

(define-public crate-chakracore-sys-0.2.2 (c (n "chakracore-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)) (d (n "clang-sys") (r "^0.18.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "regex") (r "^0.2") (d #t) (k 1)))) (h "13d6xpn9f928w5j6v0r3q6z91xd62nfvxh0k4lvf11rzwg8dlngc") (f (quote (("static"))))))

(define-public crate-chakracore-sys-0.2.3 (c (n "chakracore-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)) (d (n "clang-sys") (r "^0.18.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)) (d (n "regex") (r "^0.2") (d #t) (k 1)))) (h "193941946yhj24hicadq86zva9hr8qm0dmr1j2brv1gd8rljx23h") (f (quote (("static"))))))

