(define-module (crates-io ch ak chakracore) #:use-module (crates-io))

(define-public crate-chakracore-0.1.0 (c (n "chakracore") (v "0.1.0") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "chakracore-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rxqj0m3z82i5azpgylirfm9zxgpawgzwkkd1fh2y9d6bb09bkdw") (f (quote (("unstable") ("static" "chakracore-sys/static"))))))

(define-public crate-chakracore-0.2.0 (c (n "chakracore") (v "0.2.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "boolinator") (r "^2.4.0") (d #t) (k 0)) (d (n "chakracore-sys") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)))) (h "0a743njd2i07m4c7zf24qk271w3phpqw5wdibd1p4hs5vqry0nrr") (f (quote (("unstable") ("static" "chakracore-sys/static"))))))

