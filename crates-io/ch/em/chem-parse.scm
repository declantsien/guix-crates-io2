(define-module (crates-io ch em chem-parse) #:use-module (crates-io))

(define-public crate-chem-parse-0.1.0 (c (n "chem-parse") (v "0.1.0") (h "0swqk7c55yw8kji0v3s0yjgyzw4apq5bxrc24jm9yp14f3qg61g3")))

(define-public crate-chem-parse-0.2.0 (c (n "chem-parse") (v "0.2.0") (h "172yvbwympmyc40bi63nwbs3x6g1sxb7ixjs0y8diwimq17fjqji")))

(define-public crate-chem-parse-0.3.0 (c (n "chem-parse") (v "0.3.0") (d (list (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "1m5khdqjacqq1kq83zv8vwgkw2yj7chb8m00bnch2nw51vigph2a")))

