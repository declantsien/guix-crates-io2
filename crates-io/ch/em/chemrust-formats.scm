(define-module (crates-io ch em chemrust-formats) #:use-module (crates-io))

(define-public crate-chemrust-formats-0.2.0 (c (n "chemrust-formats") (v "0.2.0") (d (list (d (n "castep-periodic-table") (r "^0.2.2") (d #t) (k 0)) (d (n "chemrust-core") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.5") (d #t) (k 0)) (d (n "rayon") (r "1.*") (d #t) (k 0)))) (h "0bss3mq4pmvwxa5xyb31g11ixgy4vzg4anpvnzm1fvbss8d412fk")))

