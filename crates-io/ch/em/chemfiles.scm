(define-module (crates-io ch em chemfiles) #:use-module (crates-io))

(define-public crate-chemfiles-0.4.0 (c (n "chemfiles") (v "0.4.0") (d (list (d (n "chemfiles-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1rvdw3r4g2di0hvx8vaxdvyf76mw82jdkj4dzyk14nnybf6r12yl")))

(define-public crate-chemfiles-0.5.0 (c (n "chemfiles") (v "0.5.0") (d (list (d (n "chemfiles-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "1kabpc7ms2g99vs06wv1mgb5lfj0xzgia2r6bydy7k73ihhlp1ks")))

(define-public crate-chemfiles-0.5.1 (c (n "chemfiles") (v "0.5.1") (d (list (d (n "chemfiles-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "0hmlh7r8ljmwbs6gk8mimd9n1h9m4zzlwdc3g39hgf08vg9m2kp0")))

(define-public crate-chemfiles-0.5.2 (c (n "chemfiles") (v "0.5.2") (d (list (d (n "chemfiles-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "144pcc8wb7hi9nrf6cxaq7zn645bx9m523vfrfv8b7vjfcqjs4c6")))

(define-public crate-chemfiles-0.5.3 (c (n "chemfiles") (v "0.5.3") (d (list (d (n "chemfiles-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "1j15vni6rqg9wkz3d30n0al8906m7lg54anddf4dy43vgmwfkjxn")))

(define-public crate-chemfiles-0.5.4 (c (n "chemfiles") (v "0.5.4") (d (list (d (n "chemfiles-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "048p0chgm8kkncrhiqb7jfqg87x68dlymn0x2sv1dajyylsisz1r")))

(define-public crate-chemfiles-0.5.5 (c (n "chemfiles") (v "0.5.5") (d (list (d (n "chemfiles-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "1930qv1jk0fzzpqsa454iqncwfvdfkmm8n8lj8xbkifz7fvp8xdg")))

(define-public crate-chemfiles-0.6.0 (c (n "chemfiles") (v "0.6.0") (d (list (d (n "chemfiles-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0crivfznrq5wk6jwjpbmimbhfam9965r6srcmhr48r67irf5c0l3")))

(define-public crate-chemfiles-0.6.1 (c (n "chemfiles") (v "0.6.1") (d (list (d (n "chemfiles-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hqf15cm08zg9vmqiz02mzib1imfvxvjrli1d4lp4y2y5h8kxxww")))

(define-public crate-chemfiles-0.7.2 (c (n "chemfiles") (v "0.7.2") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "chemfiles-sys") (r "^0.7.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l8wqfpvnwxdy5ch4h0bgrwp06jjp4kr4i7h3jmngr5908aiaz6w") (y #t)))

(define-public crate-chemfiles-0.7.3 (c (n "chemfiles") (v "0.7.3") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "chemfiles-sys") (r "^0.7.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0grsq7wv8br96pkqibg98f2f12ih6ipb4s28shk50smnw86hyy7w")))

(define-public crate-chemfiles-0.7.4 (c (n "chemfiles") (v "0.7.4") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "chemfiles-sys") (r "^0.7.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19n2vr2p23slmjrq50wi53y2qwfmypinqmrg4dmnhzb6jx1fppq3")))

(define-public crate-chemfiles-0.7.40 (c (n "chemfiles") (v "0.7.40") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "chemfiles-sys") (r "^0.7.40") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0z63hysghc4p7mhqq9bzjdsczbghc655h8znix48yxqhbjc6sab7")))

(define-public crate-chemfiles-0.8.0 (c (n "chemfiles") (v "0.8.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "chemfiles-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1n5d7g8cj6rl4yky33xs39xm9y4yrl90gwdmw0ifvi86nkvigq8q")))

(define-public crate-chemfiles-0.9.0 (c (n "chemfiles") (v "0.9.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "chemfiles-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07i02l9kf1j1f2x60lbav18s372zbq3p4cp4x9m4fy9i3c3cwaf3")))

(define-public crate-chemfiles-0.10.0 (c (n "chemfiles") (v "0.10.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "chemfiles-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hqy6a0mgbf7rqhc8amr7dmfjz3lyaqj5pjsvwf01vws0rs8glda")))

(define-public crate-chemfiles-0.10.1 (c (n "chemfiles") (v "0.10.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "chemfiles-sys") (r "^0.10.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qqy4mcw2jf62ambm01fx59qvbpdss9fqhs3y7chr0gyyqfb678d")))

(define-public crate-chemfiles-0.10.2 (c (n "chemfiles") (v "0.10.2") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "chemfiles-sys") (r "^0.10.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q9xqc93lgsshrygin63jdkcrg8wp9i7n703b99ha2vkjywkajnh") (f (quote (("build-from-sources" "chemfiles-sys/build-from-sources"))))))

(define-public crate-chemfiles-0.10.3 (c (n "chemfiles") (v "0.10.3") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "chemfiles-sys") (r "^0.10.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03xkz989d6bnfba82011j6yxn7crnamf044mxvq8zr2bmmi8p5dd") (f (quote (("build-from-sources" "chemfiles-sys/build-from-sources"))))))

(define-public crate-chemfiles-0.10.4 (c (n "chemfiles") (v "0.10.4") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "chemfiles-sys") (r "^0.10.4") (d #t) (k 0)))) (h "1n5lq7y2ln9dvqknclnmcfbirzvi381h9d7yjinfrk8vxawrby5f") (f (quote (("build-from-sources" "chemfiles-sys/build-from-sources"))))))

(define-public crate-chemfiles-0.10.40 (c (n "chemfiles") (v "0.10.40") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "chemfiles-sys") (r "^0.10.40") (d #t) (k 0)))) (h "0shd43pk6rd68vp2238mili98pm89y4qhnllrvqmbaaywji624z1") (f (quote (("build-from-sources" "chemfiles-sys/build-from-sources"))))))

(define-public crate-chemfiles-0.10.41 (c (n "chemfiles") (v "0.10.41") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "chemfiles-sys") (r "^0.10.41") (d #t) (k 0)))) (h "1zsafanvxirwgriyjdkks93v56n5b3sfv4g444mxxkah7446a9rc") (f (quote (("build-from-sources" "chemfiles-sys/build-from-sources"))))))

