(define-module (crates-io ch em chemical-balancer) #:use-module (crates-io))

(define-public crate-chemical-balancer-0.0.0 (c (n "chemical-balancer") (v "0.0.0") (d (list (d (n "latexify") (r "^0.0.1") (d #t) (k 0)) (d (n "mathml-core") (r "^0.1.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pex") (r "^0.1.6") (d #t) (k 0)) (d (n "rationalize") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "17cgi7ys276vps2m12hcbznx41v9c60xvf6101dsiqycynabggdk") (f (quote (("default"))))))

