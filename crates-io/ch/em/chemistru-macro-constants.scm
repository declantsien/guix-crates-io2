(define-module (crates-io ch em chemistru-macro-constants) #:use-module (crates-io))

(define-public crate-chemistru-macro-constants-0.1.0 (c (n "chemistru-macro-constants") (v "0.1.0") (d (list (d (n "chemistru-elements") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "12klyd51bxxrxcwsnvkszg153h5qim04dlfz631ljppnhswkf873")))

