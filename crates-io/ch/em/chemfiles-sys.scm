(define-module (crates-io ch em chemfiles-sys) #:use-module (crates-io))

(define-public crate-chemfiles-sys-0.4.0 (c (n "chemfiles-sys") (v "0.4.0") (d (list (d (n "cmake") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0g47r4nbxzsl3gi3sv12k44c51wask4hyaq3icwpl9r22q3nii7q")))

(define-public crate-chemfiles-sys-0.5.0 (c (n "chemfiles-sys") (v "0.5.0") (d (list (d (n "cmake") (r "^0.1.13") (d #t) (k 1)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "04z0bh6hm0cn9mxr976m83zfhhbv0xy4gq1vs0mnxgzzlfl12jq5")))

(define-public crate-chemfiles-sys-0.6.0 (c (n "chemfiles-sys") (v "0.6.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02zfi427jvmf1lbs9m86769v0wpk6jkycc5ibxj7ykr59w84vpjk")))

(define-public crate-chemfiles-sys-0.7.2 (c (n "chemfiles-sys") (v "0.7.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gv32mqir8i6p1j63z3m683v0w49k8df8wm6iapjrh8b2vvsffgr") (y #t)))

(define-public crate-chemfiles-sys-0.7.3 (c (n "chemfiles-sys") (v "0.7.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11hkxmsm0y4s5zylmpawqdbmnmv1rgj3bcbydlfw78j2dp10ci68")))

(define-public crate-chemfiles-sys-0.7.3+1 (c (n "chemfiles-sys") (v "0.7.3+1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16kq70x1vwmj509f57wgf84h79px1i54q1q38zlah4pbp8khfgxl") (y #t)))

(define-public crate-chemfiles-sys-0.7.4 (c (n "chemfiles-sys") (v "0.7.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04kcn2qjpwpw646m728crkwdcy3zny9cn98i35qfpvarfcjns52c")))

(define-public crate-chemfiles-sys-0.7.40 (c (n "chemfiles-sys") (v "0.7.40") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lhjcqphsvxhiyzd8x40fdzf52vz3qdbnwg6bar5xmlypm2zrcs2")))

(define-public crate-chemfiles-sys-0.8.0 (c (n "chemfiles-sys") (v "0.8.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bv80r12xww6r6y84yhn6n5ixy0hvw3c8l944hi62v3p7s8id08x")))

(define-public crate-chemfiles-sys-0.9.0 (c (n "chemfiles-sys") (v "0.9.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0knyhrzwhdiqv96g3i7pv4yk5sdq1rx5dkxh09a8svy59p8rv5r3") (l "chemfiles")))

(define-public crate-chemfiles-sys-0.9.1 (c (n "chemfiles-sys") (v "0.9.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nlrcffzjzqdlacl2af56yzas03h8s24ifw2phk9y3g6jy82gwcj") (l "chemfiles")))

(define-public crate-chemfiles-sys-0.9.2 (c (n "chemfiles-sys") (v "0.9.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fcf299b8vpjv4a9arfagszdz81v3b6h5bjsi0vx5kzdwbxx9d4n") (l "chemfiles")))

(define-public crate-chemfiles-sys-0.9.3 (c (n "chemfiles-sys") (v "0.9.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "133dc8jq8x68l22npp5nzi54q28a0vy0bmiwrsv7zciflf4vhqg2") (l "chemfiles")))

(define-public crate-chemfiles-sys-0.10.0 (c (n "chemfiles-sys") (v "0.10.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0njsw510sakg9malyidrwak34k6rs1vdqzavhz4kjbmfrq5ihf6x") (l "chemfiles")))

(define-public crate-chemfiles-sys-0.10.1 (c (n "chemfiles-sys") (v "0.10.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "047pzsz4k8h7xrfna6mflz4hdl78c98fp0y3j82yfwzlksaxlfw8") (l "chemfiles")))

(define-public crate-chemfiles-sys-0.10.2 (c (n "chemfiles-sys") (v "0.10.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xgk4dsy0fd5i2kf54gfz0jvh7kqckys5cp28v92f5nslh7qh090") (f (quote (("build-from-sources")))) (l "chemfiles")))

(define-public crate-chemfiles-sys-0.10.3 (c (n "chemfiles-sys") (v "0.10.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vm1ibpk01ck29i6frfnnqlcf68xm4wqgzgnj67zy0grk4wrs8lj") (f (quote (("build-from-sources")))) (l "chemfiles")))

(define-public crate-chemfiles-sys-0.10.4 (c (n "chemfiles-sys") (v "0.10.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "03kqg2v2yv90a80lgnvgshr6gg0r1370xdzbnwww1k6bikljbgaf") (f (quote (("build-from-sources")))) (l "chemfiles")))

(define-public crate-chemfiles-sys-0.10.40 (c (n "chemfiles-sys") (v "0.10.40") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1y8gd2p8a5jl12fn7flqsjs4lxwqjwrvbr1x89yng6k9abvqydx8") (f (quote (("build-from-sources")))) (l "chemfiles")))

(define-public crate-chemfiles-sys-0.10.41 (c (n "chemfiles-sys") (v "0.10.41") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0y1a0zspn80p0k9i6nqj1lxka99bq7nq1by6xh0axb8hhs32pd00") (f (quote (("build-from-sources")))) (l "chemfiles")))

