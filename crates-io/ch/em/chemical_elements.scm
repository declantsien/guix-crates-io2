(define-module (crates-io ch em chemical_elements) #:use-module (crates-io))

(define-public crate-chemical_elements-0.1.0 (c (n "chemical_elements") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0gzyfyvi4zcbfdy8md35s1a3vjygkg5n9j7ssg49l1k12bixna7z")))

(define-public crate-chemical_elements-0.2.0 (c (n "chemical_elements") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mzpeaks") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0kx8l2a82i2bhbbvd0dirvh6dkk2g47pg15fnrssjv02pbk3bwbl") (f (quote (("default-features" "mzpeaks"))))))

(define-public crate-chemical_elements-0.3.0 (c (n "chemical_elements") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mzpeaks") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "test-log") (r "^0.2.12") (d #t) (k 2)))) (h "0zis8r8bgwa4c30f09fbvx3fsyqax1lx02s677z4q8mvj2dggak3") (f (quote (("default" "mzpeaks" "serde"))))))

