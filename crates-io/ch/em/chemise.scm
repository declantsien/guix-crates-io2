(define-module (crates-io ch em chemise) #:use-module (crates-io))

(define-public crate-chemise-0.1.0 (c (n "chemise") (v "0.1.0") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)))) (h "0z83g3a78gxsqjl4jhqp25w71hbfayjfrldgnkdgs9j6b946hysq")))

(define-public crate-chemise-0.1.1 (c (n "chemise") (v "0.1.1") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)))) (h "19dhjs7k7506lpc4xh123hish9q6di7qfbwn3wl75acr1g2ilpfr")))

