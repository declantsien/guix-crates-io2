(define-module (crates-io ch em chemic) #:use-module (crates-io))

(define-public crate-chemic-0.1.0 (c (n "chemic") (v "0.1.0") (d (list (d (n "cpal") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)))) (h "1s117gj0qmi2w72aavdb2p39l7xrqp6dqmcjby706qjijmwypr1z")))

(define-public crate-chemic-0.1.1 (c (n "chemic") (v "0.1.1") (d (list (d (n "cpal") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)))) (h "1nzw78v1gi6hvhsm1a6mvxc8ikbwhmpp45bib8142814xlyz0h25")))

(define-public crate-chemic-0.1.2 (c (n "chemic") (v "0.1.2") (d (list (d (n "cpal") (r "^0.15.0") (d #t) (k 0)) (d (n "dasp_interpolate") (r "^0.11.0") (f (quote ("linear"))) (d #t) (k 0)) (d (n "dasp_signal") (r "^0.11.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)) (d (n "ringbuf") (r "^0.3.2") (d #t) (k 0)))) (h "1k8dbbljd0ql1x85qsjdyf5hym73qk8r7xidp0wbxl2pmc6v7s99")))

(define-public crate-chemic-0.1.3 (c (n "chemic") (v "0.1.3") (d (list (d (n "cpal") (r "^0.15.0") (d #t) (k 0)) (d (n "dasp_interpolate") (r "^0.11.0") (f (quote ("linear"))) (d #t) (k 0)) (d (n "dasp_signal") (r "^0.11.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)) (d (n "ringbuf") (r "^0.3.2") (d #t) (k 0)))) (h "024kwm3r25zillzl6cxc4qm0kk0v6lh759cvbjlx90p6r4bkvk6n")))

(define-public crate-chemic-0.2.1 (c (n "chemic") (v "0.2.1") (d (list (d (n "cpal") (r "^0.15.0") (d #t) (k 0)) (d (n "dasp_interpolate") (r "^0.11.0") (f (quote ("linear"))) (d #t) (k 0)) (d (n "dasp_signal") (r "^0.11.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "ringbuf") (r "^0.3.2") (d #t) (k 0)))) (h "1gwrhf106l5jxi5pw1sgf3bp3f5fh60bl2gwyfw9rnsl1r6y3api")))

