(define-module (crates-io ch em chemistru-macro-map) #:use-module (crates-io))

(define-public crate-chemistru-macro-map-0.1.0 (c (n "chemistru-macro-map") (v "0.1.0") (d (list (d (n "chemistru-elements") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "00337zhb0nd5gvxij33q3f5mfyvyzildbihwsn5qg1ha56xz5c8m")))

