(define-module (crates-io ch em chemrust-scanner) #:use-module (crates-io))

(define-public crate-chemrust-scanner-0.2.2 (c (n "chemrust-scanner") (v "0.2.2") (d (list (d (n "castep-periodic-table") (r "^0.2") (d #t) (k 0)) (d (n "chemrust-core") (r "^0.2") (d #t) (k 0)) (d (n "chemrust-parser") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "kd-tree") (r "^0.5.3") (f (quote ("nalgebra" "nalgebra-serde"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "src") (r "^0.0.6") (d #t) (k 0)))) (h "1m8pnz2106sd326z9xxg51ylkpnbndax13bfsywjqxdm525i7rlc")))

(define-public crate-chemrust-scanner-0.2.3 (c (n "chemrust-scanner") (v "0.2.3") (d (list (d (n "castep-periodic-table") (r "^0.2") (d #t) (k 0)) (d (n "chemrust-core") (r "^0.2") (d #t) (k 0)) (d (n "chemrust-parser") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "kd-tree") (r "^0.5.3") (f (quote ("nalgebra" "nalgebra-serde"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)))) (h "0yd8m95qym60lg0p2lpsny7iy03r8gq5psampmf9wayn384g7jjn")))

