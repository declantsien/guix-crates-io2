(define-module (crates-io ch em chembasics) #:use-module (crates-io))

(define-public crate-chembasics-0.1.0 (c (n "chembasics") (v "0.1.0") (h "0im3gx8arnxb97hqrjkbmijsrmi5j5afw6q2a3kj45h896yp7fm7")))

(define-public crate-chembasics-0.1.1 (c (n "chembasics") (v "0.1.1") (h "1ff1i5ik8x4r0p8nvlq5kl5rpj44dkkkdcg08hh1apw18w1mvxfy")))

(define-public crate-chembasics-0.1.2 (c (n "chembasics") (v "0.1.2") (h "0il0xjqqshwyfn3kwyvrnnr9jnwhxwfvr7m5kbbwn9zvh7g970fz")))

