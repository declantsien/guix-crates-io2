(define-module (crates-io ch em chemin-macros) #:use-module (crates-io))

(define-public crate-chemin-macros-0.1.0 (c (n "chemin-macros") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "pest") (r "^2.4.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04109x2dfiiqg336zi96j5lr4fpwd7rrfp0h8pz5dizy4hhfdjqr")))

