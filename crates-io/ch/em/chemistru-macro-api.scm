(define-module (crates-io ch em chemistru-macro-api) #:use-module (crates-io))

(define-public crate-chemistru-macro-api-0.1.0 (c (n "chemistru-macro-api") (v "0.1.0") (d (list (d (n "chemistru-elements") (r "^0.5.0") (d #t) (k 0)) (d (n "chemistru-macro-constants") (r "^0.1.0") (d #t) (k 0)) (d (n "chemistru-macro-map") (r "^0.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "17ad8y0mmrg9smz0l0cn8mmhgllms7drc9kisrkh0lpxc8wv071l")))

