(define-module (crates-io ch em chemical-formula) #:use-module (crates-io))

(define-public crate-chemical-formula-0.1.0 (c (n "chemical-formula") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.7") (d #t) (k 0)))) (h "0r3d28gk61m0jcggciq0xxqlh4kr8psb12xf1lrdpa02x2dl1pa8") (r "1.74")))

