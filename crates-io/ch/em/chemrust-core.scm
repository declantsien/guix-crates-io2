(define-module (crates-io ch em chemrust-core) #:use-module (crates-io))

(define-public crate-chemrust-core-0.2.0 (c (n "chemrust-core") (v "0.2.0") (d (list (d (n "castep-periodic-table") (r "^0.2.2") (d #t) (k 0)) (d (n "crystallographic-group") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nalgebra") (r "0.*") (f (quote ("debug" "compare" "rand" "macros" "alloc" "arbitrary"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0mg19h3x06hkgd6rq7amcaxkm95np21rhhp1cijjlxbls5bmwg8x")))

