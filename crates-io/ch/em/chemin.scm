(define-module (crates-io ch em chemin) #:use-module (crates-io))

(define-public crate-chemin-0.1.0 (c (n "chemin") (v "0.1.0") (d (list (d (n "chemin-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "qstring") (r "^0.7.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.3.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "0k7ah5h5qrkz9pkxw6b89rknwj5vasf3sg96jjxrz8is0ncbwbjc")))

