(define-module (crates-io ch em chemistru-elements-list) #:use-module (crates-io))

(define-public crate-chemistru-elements-list-0.1.0 (c (n "chemistru-elements-list") (v "0.1.0") (d (list (d (n "chemistru-elements") (r "^0.1.0") (d #t) (k 0)) (d (n "chemistru-elements-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1w1hxvjby49cibjsscjflk66wpn12j7ha2rgxh43y87if4dps3r4") (y #t)))

(define-public crate-chemistru-elements-list-0.1.1 (c (n "chemistru-elements-list") (v "0.1.1") (d (list (d (n "chemistru-elements") (r "^0.1.0") (d #t) (k 0)) (d (n "chemistru-elements-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1zfr08gaibn1g3h41az5bwlxrc0lpzp0cf29g9hi7nf84bcms8m5") (y #t)))

(define-public crate-chemistru-elements-list-0.1.2 (c (n "chemistru-elements-list") (v "0.1.2") (d (list (d (n "chemistru-elements") (r "^0.1.0") (d #t) (k 0)) (d (n "chemistru-elements-macro") (r "^0.1.0") (d #t) (k 0)))) (h "15s3kd53lxjjgwaldgjqcnm2qlvmh03akcbm3d58i6fy6hq1dpv2") (y #t)))

