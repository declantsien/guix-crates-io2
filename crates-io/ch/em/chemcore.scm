(define-module (crates-io ch em chemcore) #:use-module (crates-io))

(define-public crate-chemcore-0.1.0 (c (n "chemcore") (v "0.1.0") (d (list (d (n "gamma") (r "^0.1.0") (d #t) (k 0)) (d (n "purr") (r "^0.3.0") (d #t) (k 0)))) (h "0q87r81gglniydlyr67gfcpzcr3ligmsqxvzv0ihx4wh25i2f1hn")))

(define-public crate-chemcore-0.1.1 (c (n "chemcore") (v "0.1.1") (d (list (d (n "gamma") (r "^0.2.0") (d #t) (k 0)) (d (n "purr") (r "^0.3.0") (d #t) (k 0)))) (h "0d129hbif1awd9vsr2lww3whkq7s6yygai31cklqqa9jghmi6fj8")))

(define-public crate-chemcore-0.2.0 (c (n "chemcore") (v "0.2.0") (d (list (d (n "gamma") (r "^0.6") (d #t) (k 0)) (d (n "purr") (r "^0.6") (d #t) (k 0)))) (h "1w6a5vraqd1ckjghr1cqnzqyxxpbg00dj0h6xfb4hfi03dh32q73")))

(define-public crate-chemcore-0.2.1 (c (n "chemcore") (v "0.2.1") (d (list (d (n "gamma") (r "^0.6") (d #t) (k 0)) (d (n "purr") (r "^0.6") (d #t) (k 0)))) (h "192ishxrcjavbi8q2kx8c80bq97dgr28ia2yp7vsx4zhp7f9xqas")))

(define-public crate-chemcore-0.3.0 (c (n "chemcore") (v "0.3.0") (d (list (d (n "gamma") (r "^0.8.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "purr") (r "^0.6.3") (d #t) (k 0)))) (h "02qnp9d291m91jd7sacfib82y0f27lj4y2lr3jz73frn2kzm2j0a")))

(define-public crate-chemcore-0.3.1 (c (n "chemcore") (v "0.3.1") (d (list (d (n "gamma") (r "^0.8.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "purr") (r "^0.6.3") (d #t) (k 0)))) (h "00h6j2zm0ms586kan0dgcydkzc1p8m6rqlsbhihdqixxqgv9yg41")))

(define-public crate-chemcore-0.4.0 (c (n "chemcore") (v "0.4.0") (d (list (d (n "gamma") (r "^0.9.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "purr") (r "^0.7.0") (d #t) (k 0)))) (h "07yncpjvj5d9hli1pm0lnvgx7x5vm1fbx9iqvavxamfhpqal4ji9")))

(define-public crate-chemcore-0.4.1 (c (n "chemcore") (v "0.4.1") (d (list (d (n "gamma") (r "^0.9.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "purr") (r "^0.8.0") (d #t) (k 0)))) (h "0aq57n411rprzf0bsrbxlfaym7v82ps1cdlfn6v4pqvbcqga7z4h")))

