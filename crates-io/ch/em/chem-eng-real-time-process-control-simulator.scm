(define-module (crates-io ch em chem-eng-real-time-process-control-simulator) #:use-module (crates-io))

(define-public crate-chem-eng-real-time-process-control-simulator-0.0.1 (c (n "chem-eng-real-time-process-control-simulator") (v "0.0.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "uom") (r "^0.35.0") (d #t) (k 0)))) (h "0caxlcsjq32jy5rbqirnnr0vnl33dn5mj1wnvrlbxh078w988yaq")))

(define-public crate-chem-eng-real-time-process-control-simulator-0.0.2 (c (n "chem-eng-real-time-process-control-simulator") (v "0.0.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "uom") (r "^0.35.0") (d #t) (k 0)))) (h "15izz9ziz7hji5pbszhs3xp8jrkw5plpvblbqyaj27pn3myxms52")))

(define-public crate-chem-eng-real-time-process-control-simulator-0.0.3 (c (n "chem-eng-real-time-process-control-simulator") (v "0.0.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "uom") (r "^0.35.0") (d #t) (k 0)))) (h "1fx6233m6k7z6y7np1lhpn9rbxf8w30na8l8cs69f8lx98fla12q")))

(define-public crate-chem-eng-real-time-process-control-simulator-0.0.4 (c (n "chem-eng-real-time-process-control-simulator") (v "0.0.4") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "uom") (r "^0.35.0") (d #t) (k 0)))) (h "1n0rrc08fy0yxnbgrp9klccca6ai9zrp6n9l6dp86wd72f2rgznh")))

(define-public crate-chem-eng-real-time-process-control-simulator-0.0.5 (c (n "chem-eng-real-time-process-control-simulator") (v "0.0.5") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "uom") (r "^0.36.0") (d #t) (k 0)))) (h "1xpiy65c2phc6w4nkmyd9dbqqd2xz10nwnrzi7pfs4gcff43bgip")))

