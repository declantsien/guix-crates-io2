(define-module (crates-io ch em chem) #:use-module (crates-io))

(define-public crate-chem-0.0.1 (c (n "chem") (v "0.0.1") (d (list (d (n "purr") (r "^0.9") (d #t) (k 2)))) (h "0pqywskvf8fpzhbdb7v8r23lfdxjc9z2sc284nkdiqwqjxns3w2s")))

(define-public crate-chem-0.0.2 (c (n "chem") (v "0.0.2") (d (list (d (n "purr") (r "^0.9") (d #t) (k 2)))) (h "0g39966jb2asg4lrdxv813b40h6drx5fch2lc83j8qqca1c905hy")))

