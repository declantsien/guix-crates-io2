(define-module (crates-io ch #{34}# ch347_rs) #:use-module (crates-io))

(define-public crate-ch347_rs-0.1.0 (c (n "ch347_rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xfks11blm627b8r76d20arwclkh349asbna9gacimaibylb0dg1")))

(define-public crate-ch347_rs-0.2.0 (c (n "ch347_rs") (v "0.2.0") (d (list (d (n "byte-unit") (r "^4.0.14") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shadow-rs") (r "^0.16.3") (d #t) (k 0)) (d (n "shadow-rs") (r "^0.16.3") (d #t) (k 1)))) (h "1wrh8rrivmq6if2zp7f9l92s4c7xrannpkyn7a7i5n4nyq5ki6j8")))

(define-public crate-ch347_rs-0.2.1 (c (n "ch347_rs") (v "0.2.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shadow-rs") (r "^0.16.3") (d #t) (k 0)) (d (n "shadow-rs") (r "^0.16.3") (d #t) (k 1)))) (h "14hg443ph5a5952pcm4bby3idkads9xgqa5k77717h64vw0zldfw")))

