(define-module (crates-io ch ar chariot_drs) #:use-module (crates-io))

(define-public crate-chariot_drs-0.1.0 (c (n "chariot_drs") (v "0.1.0") (d (list (d (n "chariot_io_tools") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.5") (d #t) (k 0)))) (h "1dfdk5zpzipkj0cis9gg0bc1izrvni9q2jd3qn5cdrjjpcbpcvhp")))

(define-public crate-chariot_drs-0.1.1 (c (n "chariot_drs") (v "0.1.1") (d (list (d (n "chariot_io_tools") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.5") (d #t) (k 0)))) (h "1by5n8r4ngnqsc1hf6cpjlpgqnrv7xkx1288z8xb3yyw30xf2mnj")))

(define-public crate-chariot_drs-0.1.2 (c (n "chariot_drs") (v "0.1.2") (d (list (d (n "chariot_io_tools") (r "^0.1") (d #t) (k 0)) (d (n "either") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.5") (d #t) (k 0)))) (h "1rfgdvf98rm4lah1kaa63h6jm13fyw1hsjgp7r1fbycf0japs2p9")))

