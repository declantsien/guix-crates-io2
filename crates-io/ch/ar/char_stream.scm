(define-module (crates-io ch ar char_stream) #:use-module (crates-io))

(define-public crate-char_stream-0.1.0 (c (n "char_stream") (v "0.1.0") (d (list (d (n "tempfile") (r "^2.2.0") (d #t) (k 2)))) (h "1z5acqvbj33gyfd8jabm3yh8h0ddhvnm0lc50pxsi5z3cg34pwn0")))

(define-public crate-char_stream-0.1.1 (c (n "char_stream") (v "0.1.1") (d (list (d (n "tempfile") (r "^2.2.0") (d #t) (k 2)))) (h "1c2j862917p2mwyymnv0x5gzaaah1f328rygs8zjlzjn26ka50k4")))

(define-public crate-char_stream-0.1.2 (c (n "char_stream") (v "0.1.2") (d (list (d (n "tempfile") (r "^2.2.0") (d #t) (k 2)))) (h "1i5vsn8dhnmzcv46dbrfjql0qndq58wvbr2bjg273nccz7pjbz8j")))

(define-public crate-char_stream-0.1.3 (c (n "char_stream") (v "0.1.3") (d (list (d (n "tempfile") (r "^2.2.0") (d #t) (k 2)))) (h "1m65y3k8ml5949n2vs2pl2zw5xqb2fpnl7z3ac9kq7y2srnjknnm")))

(define-public crate-char_stream-0.1.4 (c (n "char_stream") (v "0.1.4") (d (list (d (n "tempfile") (r "^2.2.0") (d #t) (k 2)))) (h "1wmjb3g755fvjgz5m3hph35hah0c4am28mndpd6bw31j5r7ji781")))

(define-public crate-char_stream-0.1.5 (c (n "char_stream") (v "0.1.5") (d (list (d (n "tempfile") (r "^2.2.0") (d #t) (k 2)))) (h "0s2l8y1hnx91pwbn4n9jb3yg75gcffx8r838fb805pzva9qdb2dv")))

(define-public crate-char_stream-0.1.6 (c (n "char_stream") (v "0.1.6") (d (list (d (n "tempfile") (r "^2.2.0") (d #t) (k 2)))) (h "0m7gnrx0xh0d6ra81y7rr626g4gj0cvfw1jygvdxnj839yq2p34c")))

(define-public crate-char_stream-0.1.7 (c (n "char_stream") (v "0.1.7") (d (list (d (n "tempfile") (r "^2.2.0") (d #t) (k 2)))) (h "0wgzy4dy268phwb6mdnavndk4cgzph9mds3ni4yjzz5pk6h3wrvh")))

(define-public crate-char_stream-0.1.8 (c (n "char_stream") (v "0.1.8") (d (list (d (n "tempfile") (r "^2.2.0") (d #t) (k 2)))) (h "0rk68bckdnc2a9i186x9sig9xf8g80r4fsyzdrj82iqidvl16k58")))

