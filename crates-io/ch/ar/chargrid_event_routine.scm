(define-module (crates-io ch ar chargrid_event_routine) #:use-module (crates-io))

(define-public crate-chargrid_event_routine-0.1.0 (c (n "chargrid_event_routine") (v "0.1.0") (d (list (d (n "chargrid_app") (r "^0.1") (d #t) (k 0)) (d (n "chargrid_input") (r "^0.1") (d #t) (k 0)) (d (n "chargrid_render") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0h8qyjnj2hwrx57akbrnhnhxfv4mb02nb5p4wccsgb7kfimaickj") (f (quote (("serialize" "chargrid_render/serialize" "chargrid_input/serialize"))))))

(define-public crate-chargrid_event_routine-0.2.0 (c (n "chargrid_event_routine") (v "0.2.0") (d (list (d (n "chargrid_app") (r "^0.1") (d #t) (k 0)) (d (n "chargrid_input") (r "^0.1") (d #t) (k 0)) (d (n "chargrid_render") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "104016xcqjhy4q424j13sv5ksj11gp2yx5caicmppkk6vzsfbwx2") (f (quote (("serialize" "chargrid_render/serialize" "chargrid_input/serialize"))))))

(define-public crate-chargrid_event_routine-0.2.1 (c (n "chargrid_event_routine") (v "0.2.1") (d (list (d (n "chargrid_app") (r "^0.1") (d #t) (k 0)) (d (n "chargrid_input") (r "^0.1") (d #t) (k 0)) (d (n "chargrid_render") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06fkanjzy911za3xv8pks76vhqh6wq58kg9mcilbivnc6k3ywr4z") (f (quote (("serialize" "chargrid_render/serialize" "chargrid_input/serialize"))))))

(define-public crate-chargrid_event_routine-0.2.2 (c (n "chargrid_event_routine") (v "0.2.2") (d (list (d (n "chargrid_app") (r "^0.1") (d #t) (k 0)) (d (n "chargrid_input") (r "^0.1") (d #t) (k 0)) (d (n "chargrid_render") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "08djdl7836ywbkv38afhfnbq75lg34lhjf1vndnppanwfxj29xcy") (f (quote (("serialize" "chargrid_render/serialize" "chargrid_input/serialize"))))))

(define-public crate-chargrid_event_routine-0.2.3 (c (n "chargrid_event_routine") (v "0.2.3") (d (list (d (n "chargrid_app") (r "^0.1") (d #t) (k 0)) (d (n "chargrid_input") (r "^0.1") (d #t) (k 0)) (d (n "chargrid_render") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0qd2gyna0f98vbp0scfcygyz3v7jngf0z3yy8havl9bgy673li9p") (f (quote (("serialize" "chargrid_render/serialize" "chargrid_input/serialize"))))))

(define-public crate-chargrid_event_routine-0.3.0 (c (n "chargrid_event_routine") (v "0.3.0") (d (list (d (n "chargrid_app") (r "^0.2") (d #t) (k 0)) (d (n "chargrid_input") (r "^0.2") (d #t) (k 0)) (d (n "chargrid_render") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0lla76wbhbv38r87qma1c0r704g94007jwpnvpqhkz1vrc1yxana") (f (quote (("serialize" "chargrid_render/serialize" "chargrid_input/serialize"))))))

