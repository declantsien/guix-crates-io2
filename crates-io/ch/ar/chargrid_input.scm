(define-module (crates-io ch ar chargrid_input) #:use-module (crates-io))

(define-public crate-chargrid_input-0.1.0 (c (n "chargrid_input") (v "0.1.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "02rxwai72qldkpvgzf36696qi1z7s0hdc9ajcgw4wj70951srxdx") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-chargrid_input-0.1.1 (c (n "chargrid_input") (v "0.1.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1q1vx2bapwj14g2p9k5zgwrx5yrz0crf7ddhdb1k3qwj9wfjhilp") (f (quote (("serialize" "serde" "coord_2d/serialize") ("gamepad"))))))

(define-public crate-chargrid_input-0.2.0 (c (n "chargrid_input") (v "0.2.0") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zm36c625hjp7rcb9ayfw7736l3sv71fqbm3x9s6hfyijzsnh6p0") (f (quote (("serialize" "serde" "coord_2d/serialize") ("gamepad"))))))

(define-public crate-chargrid_input-0.3.0 (c (n "chargrid_input") (v "0.3.0") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00csgzyyv9q7p5k3w3xwgamzllibjq300pgs58pyhchqhrm3c8mz") (f (quote (("serialize" "serde" "coord_2d/serialize") ("gamepad"))))))

(define-public crate-chargrid_input-0.3.1 (c (n "chargrid_input") (v "0.3.1") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0v862mw4jbqm8r35r0zhlsk63ny0rkk3nnr5m7wbn2rrzs2mbqff") (f (quote (("serialize" "serde" "coord_2d/serialize") ("gamepad"))))))

(define-public crate-chargrid_input-0.4.0 (c (n "chargrid_input") (v "0.4.0") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ns5zyj36ljk6zmhhjpnr3b7hhw39invq8k2d8933q9rbv906y66") (f (quote (("serialize" "serde" "coord_2d/serialize") ("gamepad"))))))

(define-public crate-chargrid_input-0.4.1 (c (n "chargrid_input") (v "0.4.1") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "027z30d70fzp43hjsr3akqajcaiq03f6d08kfp17xgaki2n0qcvl") (f (quote (("serialize" "serde" "coord_2d/serialize") ("gamepad"))))))

(define-public crate-chargrid_input-0.5.0 (c (n "chargrid_input") (v "0.5.0") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vi8r0bbcpjfsjl8vni437rz0xm9xl05xd8c5j22kb5hb9lagbhw") (f (quote (("serialize" "serde" "coord_2d/serialize") ("gamepad"))))))

(define-public crate-chargrid_input-0.6.0 (c (n "chargrid_input") (v "0.6.0") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1qjrdjjsm6az4vs5bhr6bzahyxh7baf58861fqcxqc4kflyha8gh") (f (quote (("serialize" "serde" "coord_2d/serialize") ("gamepad"))))))

(define-public crate-chargrid_input-0.6.1 (c (n "chargrid_input") (v "0.6.1") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0m58gg1b3ijfmq1h8a1482syky0djy7ifpgbrpa8gikfhgmkn1hy") (f (quote (("serialize" "serde" "coord_2d/serialize") ("gamepad"))))))

