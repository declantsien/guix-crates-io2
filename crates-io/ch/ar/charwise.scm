(define-module (crates-io ch ar charwise) #:use-module (crates-io))

(define-public crate-charwise-1.0.0 (c (n "charwise") (v "1.0.0") (d (list (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "08g3s4hhz1pz95j5sjvl0if49nriqspbrm6cq7m6mxsxg87sqnyi")))

(define-public crate-charwise-1.0.1 (c (n "charwise") (v "1.0.1") (d (list (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0hw8pmq6f9mjqcjgjx1cxaximci1ygj9mbi6yc885jqz5gangml3")))

