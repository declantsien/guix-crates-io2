(define-module (crates-io ch ar chargrid_text) #:use-module (crates-io))

(define-public crate-chargrid_text-0.1.0 (c (n "chargrid_text") (v "0.1.0") (d (list (d (n "chargrid_render") (r "^0.1") (d #t) (k 0)) (d (n "chargrid_test_grid") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0sahyxc4jrcz13kxj6mpddmqxzjhz99pvczwgdi60gdzflssh5ga") (f (quote (("serialize" "serde" "chargrid_render/serialize"))))))

(define-public crate-chargrid_text-0.2.0 (c (n "chargrid_text") (v "0.2.0") (d (list (d (n "chargrid_render") (r "^0.2") (d #t) (k 0)) (d (n "chargrid_test_grid") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "01ahrygk7vc0m92db87yb7nl7csckw4cpaba0fxqzgxbw2ji9xhi") (f (quote (("serialize" "serde" "chargrid_render/serialize"))))))

