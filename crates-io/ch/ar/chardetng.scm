(define-module (crates-io ch ar chardetng) #:use-module (crates-io))

(define-public crate-chardetng-0.0.1 (c (n "chardetng") (v "0.0.1") (h "08szzm5ab137689hlbghk35bjl9z8rnbg0g9krv63kcwwvgg471m")))

(define-public crate-chardetng-0.1.0 (c (n "chardetng") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "178qxdx05fsh5qkf71n2i3lamp8j0ada5fk84jw3716y0zcqnpi6")))

(define-public crate-chardetng-0.1.1 (c (n "chardetng") (v "0.1.1") (d (list (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "0q4x8bhm8216sij4pzfisq60i3j3ypvpgix2k690ykwc5sy9znp2")))

(define-public crate-chardetng-0.1.2 (c (n "chardetng") (v "0.1.2") (d (list (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "1vj6nms8njhd19waj5amin3bdv760gddxjxb85wr1ybc6jdkj244")))

(define-public crate-chardetng-0.1.3 (c (n "chardetng") (v "0.1.3") (d (list (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "1g53hxfr10s82wax1n5s5x82rj9sypxxmbvwrsmk097is5hh480r")))

(define-public crate-chardetng-0.1.4 (c (n "chardetng") (v "0.1.4") (d (list (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "0vs6cw9yrs23dy6zsi5r39xbsmg8b6ps3qs3hq88x97h1j017c2d") (f (quote (("testing-only-no-semver-guarantees-do-not-use"))))))

(define-public crate-chardetng-0.1.5 (c (n "chardetng") (v "0.1.5") (d (list (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "1s5lcd4q0vj35j7dlqsd03qwr6bzk0hsjxa0xynyag4591krdlws") (f (quote (("testing-only-no-semver-guarantees-do-not-use"))))))

(define-public crate-chardetng-0.1.6 (c (n "chardetng") (v "0.1.6") (d (list (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "11ppxvqyfam5jnikj6isirhjgv79dljpsjnz5kf11lxs57g6fci9") (f (quote (("testing-only-no-semver-guarantees-do-not-use"))))))

(define-public crate-chardetng-0.1.7 (c (n "chardetng") (v "0.1.7") (d (list (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "0anamik0f7l8m4rgyj1fzp13axdn1h5bz1c52j6ka9hp24bpd56c") (f (quote (("testing-only-no-semver-guarantees-do-not-use"))))))

(define-public crate-chardetng-0.1.8 (c (n "chardetng") (v "0.1.8") (d (list (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "1g2528ykb1pag2g0h2z0jdwp5g77xbjg294s17q8a1x1agqnx3xk") (f (quote (("testing-only-no-semver-guarantees-do-not-use"))))))

(define-public crate-chardetng-0.1.9 (c (n "chardetng") (v "0.1.9") (d (list (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)))) (h "0f7wbwi9x78c94ms6b210180pjr89cjphlq9ryqz027xjyvvy5vq") (f (quote (("testing-only-no-semver-guarantees-do-not-use"))))))

(define-public crate-chardetng-0.1.10 (c (n "chardetng") (v "0.1.10") (d (list (d (n "arrayvec") (r ">=0.5.1, <0.6.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r ">=0.1.10, <0.2.0") (d #t) (k 0)) (d (n "detone") (r ">=1.0.0, <2.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r ">=0.8.17, <0.9.0") (d #t) (k 0)) (d (n "memchr") (r ">=2.2.0, <3.0.0") (d #t) (k 0)) (d (n "rayon") (r ">=1.3.0, <2.0.0") (o #t) (d #t) (k 0)))) (h "0990wm4rwmvxm0x254ciwnji81a63l89kqr30lh70bkcb6kwnrpq") (f (quote (("testing-only-no-semver-guarantees-do-not-use") ("multithreading" "rayon" "arrayvec"))))))

(define-public crate-chardetng-0.1.11 (c (n "chardetng") (v "0.1.11") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "1ddmsasjn4s65s4nmyf3rhi28m7jk94x1q5nw99656fiibx30753") (f (quote (("testing-only-no-semver-guarantees-do-not-use") ("multithreading" "rayon" "arrayvec"))))))

(define-public crate-chardetng-0.1.12 (c (n "chardetng") (v "0.1.12") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "1yghgi236gz5aj3ggycgchxak62gm1z0bq0y875dba0s5q1pi6a5") (f (quote (("testing-only-no-semver-guarantees-do-not-use") ("multithreading" "rayon" "arrayvec"))))))

(define-public crate-chardetng-0.1.13 (c (n "chardetng") (v "0.1.13") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "0065z2ylphppfcbyykmhq8szbi8jcgqb861830bk9qlfiw6ipa41") (f (quote (("testing-only-no-semver-guarantees-do-not-use") ("multithreading" "rayon" "arrayvec"))))))

(define-public crate-chardetng-0.1.14 (c (n "chardetng") (v "0.1.14") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "174ghgb1ykvqrkjqw711b5bd5h6yf0z3px9md3xijpcj8z5a599n") (f (quote (("testing-only-no-semver-guarantees-do-not-use") ("multithreading" "rayon" "arrayvec"))))))

(define-public crate-chardetng-0.1.15 (c (n "chardetng") (v "0.1.15") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.29") (k 0)) (d (n "memchr") (r "^2.2.0") (k 0)) (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "0bjwrnkg9x1pp0ymflir6nl3hcvr0n1mdk1fi2y2zhw1dg0jkvl3") (f (quote (("testing-only-no-semver-guarantees-do-not-use") ("multithreading" "rayon" "arrayvec"))))))

(define-public crate-chardetng-0.1.16 (c (n "chardetng") (v "0.1.16") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.29") (k 0)) (d (n "memchr") (r "^2.2.0") (k 0)) (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "0c6yi4lcm2d5npanxw5j341n4f4mfqabr9vn8v0y2kciqzyfyb88") (f (quote (("testing-only-no-semver-guarantees-do-not-use") ("multithreading" "rayon" "arrayvec"))))))

(define-public crate-chardetng-0.1.17 (c (n "chardetng") (v "0.1.17") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "detone") (r "^1.0.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.29") (k 0)) (d (n "memchr") (r "^2.2.0") (k 0)) (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "1spikjcnblwa5n1nnk46fxkwn86yfiqxgs47h4yaw23vbfvg1f0l") (f (quote (("testing-only-no-semver-guarantees-do-not-use") ("multithreading" "rayon" "arrayvec"))))))

