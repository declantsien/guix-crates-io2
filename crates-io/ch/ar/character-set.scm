(define-module (crates-io ch ar character-set) #:use-module (crates-io))

(define-public crate-character-set-0.1.0 (c (n "character-set") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1x9fnnyfz3hlfxn0831hk11s23aqvdhj8yks7kl3jsfdzn37zhg9")))

(define-public crate-character-set-0.2.0 (c (n "character-set") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (d #t) (k 0)))) (h "0xrmsh4rrwpscrnzyvih98vsg6f8vicsn4qlycqpigqgi4qbqx5z")))

(define-public crate-character-set-0.2.1 (c (n "character-set") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (d #t) (k 0)))) (h "0ylgk6x52w9gv1lk5y96hhky538g46785qy56zdaryv3rh0d4lp6")))

(define-public crate-character-set-0.2.2 (c (n "character-set") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (d #t) (k 0)))) (h "0pzc0w2dj54cxdw18zi3pbr97sc1d3bn179h1r8khm0qq8qgsbhi")))

(define-public crate-character-set-0.3.0 (c (n "character-set") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (d #t) (k 0)))) (h "1ghx7bhq040alr1x8kfrmn5dvp7pmd6vrrzaa1yi90iyn59h0axi")))

(define-public crate-character-set-0.4.0 (c (n "character-set") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (d #t) (k 0)))) (h "0wvwiy5rwgcsjh5jz9hjcd3910825m4psd63l0w7146fh12hq7ny")))

