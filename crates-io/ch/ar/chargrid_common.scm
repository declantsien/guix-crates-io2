(define-module (crates-io ch ar chargrid_common) #:use-module (crates-io))

(define-public crate-chargrid_common-0.1.0 (c (n "chargrid_common") (v "0.1.0") (d (list (d (n "chargrid_core") (r "^0.1") (d #t) (k 0)))) (h "1rg3lnphfgwb6dh68bp8mmbgpb2s4a3526y3x95aqxc9m1abvqiy") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_common-0.1.1 (c (n "chargrid_common") (v "0.1.1") (d (list (d (n "chargrid_core") (r "^0.1") (d #t) (k 0)))) (h "0bkzl1m487ahl0kzbp7s488981szhgawhw1l643nxils6h7si9xf") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_common-0.1.2 (c (n "chargrid_common") (v "0.1.2") (d (list (d (n "chargrid_core") (r "^0.1") (d #t) (k 0)))) (h "0pvgqs3ifnq1bvhy3dwi7zxx4ay30s721ndgmgcdz36syyj8g4jn") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_common-0.1.3 (c (n "chargrid_common") (v "0.1.3") (d (list (d (n "chargrid_core") (r "^0.1") (d #t) (k 0)))) (h "1qvbqkxqd6x8njrs6ljgmb65cj1mcqh433ciyr9abxg3l89yxli2") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_common-0.2.0 (c (n "chargrid_common") (v "0.2.0") (d (list (d (n "chargrid_core") (r "^0.1") (d #t) (k 0)))) (h "1pnf1dnvsz06w5i9qbqvym9qr75s1f5kjl0kgh0zn1fjl1dyyw9h") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_common-0.3.0 (c (n "chargrid_common") (v "0.3.0") (d (list (d (n "chargrid_core") (r "^0.1") (d #t) (k 0)))) (h "1sz9awdy6ps4xyc1xfbnly7m5ljmwnllzwhv8crnxs6h80a2j6w8") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_common-0.4.0 (c (n "chargrid_common") (v "0.4.0") (d (list (d (n "chargrid_core") (r "^0.2") (d #t) (k 0)))) (h "1x6ybs8jj529nnylhgm1bflr6j8gpg004kwnv6zayqnwcq7j00lw") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_common-0.5.0 (c (n "chargrid_common") (v "0.5.0") (d (list (d (n "chargrid_core") (r "^0.2") (d #t) (k 0)))) (h "03xf7fkyqff3kkfasy9xdqrnn3krvghwbcfgbmwbz9c8brfwh8ra") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_common-0.6.0 (c (n "chargrid_common") (v "0.6.0") (d (list (d (n "chargrid_core") (r "^0.3") (d #t) (k 0)))) (h "06d5rs2hk5fmjvmk95ywjly6sm7k5ckslhv7vpbg6p2h6wk8whar") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_common-0.7.0 (c (n "chargrid_common") (v "0.7.0") (d (list (d (n "chargrid_core") (r "^0.4") (d #t) (k 0)))) (h "0grvy5an2bsmpdk43fcc9fnr7i2pygxjvq9n19a4i25nwh50dvzl") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_common-0.7.1 (c (n "chargrid_common") (v "0.7.1") (d (list (d (n "chargrid_core") (r "^0.4") (d #t) (k 0)))) (h "0m3hdp4sm0phjfvr7jkrs9n8k3gzr7d93lsn098zyajs5z7368jd") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_common-0.7.2 (c (n "chargrid_common") (v "0.7.2") (d (list (d (n "chargrid_core") (r "^0.4") (d #t) (k 0)))) (h "03pdlrhsckhi75mp96qw10zv2akcydzalmymag6fsxxi1w7jj3b6") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_common-0.7.3 (c (n "chargrid_common") (v "0.7.3") (d (list (d (n "chargrid_core") (r "^0.4") (d #t) (k 0)))) (h "1k714bxfaahnqni21fsmjjdvcscjk0xl51k6q6agkim8g20h7p19") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_common-0.7.4 (c (n "chargrid_common") (v "0.7.4") (d (list (d (n "chargrid_core") (r "^0.4") (d #t) (k 0)))) (h "12vrn6wra9c1ncwhwgy9p67jimg13lr5pidvxnjn6yy3gcmfdvkx") (f (quote (("serialize" "chargrid_core/serialize") ("gamepad" "chargrid_core/gamepad"))))))

