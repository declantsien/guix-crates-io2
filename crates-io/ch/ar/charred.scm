(define-module (crates-io ch ar charred) #:use-module (crates-io))

(define-public crate-charred-0.1.0 (c (n "charred") (v "0.1.0") (h "05n8hfk16rhphzq5nq8bb289b99z7wfn4ahki9wygf03r1il3ykw")))

(define-public crate-charred-0.1.1 (c (n "charred") (v "0.1.1") (h "0g1bq6fl24pgcm3acxccwqissxsbr4cg7cnnn7mf9l7r6v7fx4h9")))

(define-public crate-charred-0.1.2 (c (n "charred") (v "0.1.2") (h "11ksrpkflrmppg6gbw06g4id9cfig30z5mmyix02h9v2hdh9pl5b")))

(define-public crate-charred-0.1.3 (c (n "charred") (v "0.1.3") (h "0xwwvwjsrkvm567yjjrkhsyjkb70ncwrxxkznw4kkik7vk9kybmc")))

(define-public crate-charred-0.1.4 (c (n "charred") (v "0.1.4") (h "177414qv5yh4ym7p9f72sjd12kj13xv1g5b73jpbda8basi6irsh")))

(define-public crate-charred-0.1.5 (c (n "charred") (v "0.1.5") (h "1gknyhhhnmh1wfp3jvf6dighz4a593marqjlb2nm8iz2d6ry13aj")))

(define-public crate-charred-0.1.6 (c (n "charred") (v "0.1.6") (h "0rlh1i8mr114vac4vy26zkjyjgpr8kpybyswymcanpzksncbgaqg")))

(define-public crate-charred-0.1.7 (c (n "charred") (v "0.1.7") (h "0m1wjr97r9wlsl9x10xv6nwhf91m5sm1bm14ay0g09qi56ciaypy")))

(define-public crate-charred-0.2.0 (c (n "charred") (v "0.2.0") (h "1ph9v121hvxrwzaavdksf2cdv60whxwqnly2i71hsr55knsvj1gn")))

(define-public crate-charred-0.2.1 (c (n "charred") (v "0.2.1") (h "1mxf0yc60dh4mkb5zl8236hx2facmvdr72s8a2cisyiq5jl6k38x")))

(define-public crate-charred-0.2.2 (c (n "charred") (v "0.2.2") (h "0qdhgcpdgn671rrrphw9fi5926xs7r0s9ilr7xjdandgpdzkrxwc")))

(define-public crate-charred-0.3.0 (c (n "charred") (v "0.3.0") (h "03jxdb4y1jhfljrb25rj8y0radyvl4lxa50f9bwn11di31pkni41")))

(define-public crate-charred-0.3.1 (c (n "charred") (v "0.3.1") (h "0lh6j2hvckd2mzvna6gx2vmd8j6xjfmfkvw5ggp1jl4dg4dgi670")))

(define-public crate-charred-0.3.3 (c (n "charred") (v "0.3.3") (h "1kh5akn8s7m7jh36z2gxp4554f2rfd5ghrd9pa82mnd3yqmk008y")))

(define-public crate-charred-0.3.4 (c (n "charred") (v "0.3.4") (h "1y2mpwgf3c1q32sdy9hh5ayfmxz6v855rfnkhbkwg287l0kbsxhd")))

(define-public crate-charred-0.3.5 (c (n "charred") (v "0.3.5") (h "1x8q6ksqqpp6d4m5g370mxwaka1a3z3wpl3ccgan582nbmqmk5zd")))

(define-public crate-charred-0.3.6 (c (n "charred") (v "0.3.6") (h "1a0f1qbhib9qsza7hlighi27hd0f3imsfddg3bgda0ii4y4bfqs1")))

(define-public crate-charred-2.0.0 (c (n "charred") (v "2.0.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("io-util" "io-std" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "13qw9426plr71pq7ww9ijs9byckryq4fvwn2ii4w3v2ab1ldlkjy")))

(define-public crate-charred-2.1.0 (c (n "charred") (v "2.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("io-util" "io-std" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1177gbamr9g29knf0b2s00l7zxhdczszpc0fc4bm74grpfif82qv")))

(define-public crate-charred-2.1.1 (c (n "charred") (v "2.1.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("io-util" "io-std" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1q9d6w15ajp2xfnrkgm6112icw5sqni4hlxm1hk689sin5jg3i69")))

(define-public crate-charred-2.2.0 (c (n "charred") (v "2.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("io-util" "io-std" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1l4rj994xapw66cndva87wnv1a8vk2xjbkbclirybyfwqxzn6z1s")))

