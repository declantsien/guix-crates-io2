(define-module (crates-io ch ar character-stream) #:use-module (crates-io))

(define-public crate-character-stream-0.1.0 (c (n "character-stream") (v "0.1.0") (h "03kjdrk9g0a9nvznpmp62p5cyrczw5nqwws7h6ky8x140b0xdw2j")))

(define-public crate-character-stream-0.2.0 (c (n "character-stream") (v "0.2.0") (h "03anaw2sg5zr3f1p88dx1ri63f17vq94q77nigqlyzmlf69r11ci")))

(define-public crate-character-stream-0.3.0 (c (n "character-stream") (v "0.3.0") (h "0361csapyvkilv572325k0dnya1blp32ijffqzfi3ms230bikq8a")))

(define-public crate-character-stream-0.4.0 (c (n "character-stream") (v "0.4.0") (h "1ym0w9k0r0z34hdf1vsqh2fgrwzbm5wd9rvg2x43am66z556b78a")))

(define-public crate-character-stream-0.5.0 (c (n "character-stream") (v "0.5.0") (h "1gfcqsriysf85l4wvf1q9wcgsvh04gd8nrrmxilfamk17pvpc2mb")))

(define-public crate-character-stream-0.6.0 (c (n "character-stream") (v "0.6.0") (h "0d47mahv4dzyykl2wyj0dvhk142iq9yncnw3z6c3ilv1cbav01r7")))

(define-public crate-character-stream-0.6.1 (c (n "character-stream") (v "0.6.1") (h "0zi2jvb4iif848dbcs01w2is2iizk157208clsspsnlh59cqx9cg")))

(define-public crate-character-stream-0.7.0 (c (n "character-stream") (v "0.7.0") (h "04hnfmkbvzzwh271cdhxx6ipxgz00i74j6g5b3wcbf0slvfr42x7")))

(define-public crate-character-stream-0.8.0 (c (n "character-stream") (v "0.8.0") (h "05p7dbdvidb6zl7mlgvz9nh8rapqyx7060jq0wgnmw2gvhn3bvr2")))

(define-public crate-character-stream-0.9.0 (c (n "character-stream") (v "0.9.0") (h "18d8q92r1bs3drs3kb5hrii9mb3vzhm5h5vs7hdnbcrx31k5a20q")))

(define-public crate-character-stream-0.10.0 (c (n "character-stream") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f26ansvc8c7lmh735h4sm069mhqzhvk67kxz741ianp240xnsla")))

(define-public crate-character-stream-0.11.0 (c (n "character-stream") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13gw46a7if0rvhambrzp8qi7zvhqlyz1jq08l1c67fkbzszdqwwg")))

(define-public crate-character-stream-0.12.0 (c (n "character-stream") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0r4vz5bsnmr7vl2zw9bwa7ffj3k7122kdw1j1b5q2vr1z5gxx98i")))

(define-public crate-character-stream-0.13.0 (c (n "character-stream") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1.4") (f (quote ("aarch64_neon"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z1lpqr0dw9qcl0bwg3dy5w1fp0jydxh3yxxd9js3lcllwdgzj5m")))

