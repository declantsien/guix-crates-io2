(define-module (crates-io ch ar chardetng_c) #:use-module (crates-io))

(define-public crate-chardetng_c-0.1.0 (c (n "chardetng_c") (v "0.1.0") (d (list (d (n "chardetng") (r "^0.1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)))) (h "03r8mzs4jwpb2bldbcy6p7k1bs8nqj57iwhvwq3x0i3924bpksca")))

(define-public crate-chardetng_c-0.1.1 (c (n "chardetng_c") (v "0.1.1") (d (list (d (n "chardetng") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)))) (h "0xh0a9g0xw7cxrmdws9d48kivkprl1r5hz0kd8sl142xf3yzjn04")))

(define-public crate-chardetng_c-0.1.2 (c (n "chardetng_c") (v "0.1.2") (d (list (d (n "chardetng") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)))) (h "1cw1jhsjisff80vxlzsphgh8845dc4skj1v7h3ikgla3799a5dby")))

