(define-module (crates-io ch ar char-circle) #:use-module (crates-io))

(define-public crate-char-circle-0.1.0-rc1 (c (n "char-circle") (v "0.1.0-rc1") (h "169rq43vv27srfms5048gh5wwsdh7dy1n4mp1c3ivdhnhl2byrn0")))

(define-public crate-char-circle-0.1.0-rc2 (c (n "char-circle") (v "0.1.0-rc2") (h "1fkjni0kzzi7vlcy5k00n27pys22zsdylicdfd51rxlwi0ddfxqh")))

(define-public crate-char-circle-0.1.0 (c (n "char-circle") (v "0.1.0") (h "0hf3qs7w1krif0cw6kxnjdxn9kgymnjgpgwkvwp8kkn4mmcvp3n2")))

