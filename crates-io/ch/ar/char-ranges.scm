(define-module (crates-io ch ar char-ranges) #:use-module (crates-io))

(define-public crate-char-ranges-0.1.0 (c (n "char-ranges") (v "0.1.0") (h "01kj542xx5v25zhv3l27ngjb905jwd20169w0yk9yli3lmbwgzg5")))

(define-public crate-char-ranges-0.1.1 (c (n "char-ranges") (v "0.1.1") (h "0vv94wnglcb5wcj9vb05gfkawz3g73rhz3qb0a882xxk8wyipd02")))

(define-public crate-char-ranges-0.1.2 (c (n "char-ranges") (v "0.1.2") (h "1rpbrxvgwgqjcxlazdy05m8rf6xv024cvxjd6gjlf13zdpi2bsr1")))

