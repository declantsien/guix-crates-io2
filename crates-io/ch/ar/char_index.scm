(define-module (crates-io ch ar char_index) #:use-module (crates-io))

(define-public crate-char_index-0.1.0 (c (n "char_index") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18p5gk9qrzyamdmr6i1h6fyv9f0074xkbmiiw24d1fd23ncx8qqk") (r "1.56")))

(define-public crate-char_index-0.1.1 (c (n "char_index") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0lwwac793cf28nqaql60fiqjqs823pbkx56zfcarla0ljr9a9al4") (r "1.56")))

(define-public crate-char_index-0.1.2 (c (n "char_index") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1wxc8bdvyvadjvj63ywf11r0k6x3br68krjsblh2ibmhqp2jc0dm") (r "1.56")))

