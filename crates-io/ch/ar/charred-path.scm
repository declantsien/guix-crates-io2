(define-module (crates-io ch ar charred-path) #:use-module (crates-io))

(define-public crate-charred-path-0.1.0 (c (n "charred-path") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "1wblvvwqdn5w53h7qq0cpv3w8nxlcsj242r1jpp5aiqf5z51pyhg") (y #t)))

(define-public crate-charred-path-0.1.1 (c (n "charred-path") (v "0.1.1") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)))) (h "1qrpvyw6s0mvw2c1g12zf805j11ws7fdbmxd5hb7lbl6zpmzycjr")))

