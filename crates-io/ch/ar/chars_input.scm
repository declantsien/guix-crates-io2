(define-module (crates-io ch ar chars_input) #:use-module (crates-io))

(define-public crate-chars_input-0.1.0 (c (n "chars_input") (v "0.1.0") (h "1s36rfhv27lq1laiwhqh8m3c35c9qcjngh0hr8bav4y5h10l2d8z") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-chars_input-0.1.1 (c (n "chars_input") (v "0.1.1") (h "0nsd8hc4799cm3zfcj1yj7mqz94ly05s08kvvwbd0aj93yy1jqc0") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-chars_input-0.1.2 (c (n "chars_input") (v "0.1.2") (h "10zslb61lgamcdyc8y7j4x7alq6g5cp67cp94fv5ljc208i8xvhf") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-chars_input-0.2.0 (c (n "chars_input") (v "0.2.0") (h "0mcc39mgr4naw1sr3hgc74z06y5pwkda5nn858cmvi150hpvvcbv") (f (quote (("std") ("default" "std"))))))

(define-public crate-chars_input-0.2.1 (c (n "chars_input") (v "0.2.1") (h "17iw5q69rclj22lc6gn92qcwhaz6qf5x45vsn4qmj4xsrrvkk0r1") (f (quote (("std") ("default" "std"))))))

(define-public crate-chars_input-0.2.2 (c (n "chars_input") (v "0.2.2") (h "1az45bmbfd80zxi7rc1rqg07296f6v51bafwm147iz3wmq5p9rmj") (f (quote (("std") ("default" "std"))))))

