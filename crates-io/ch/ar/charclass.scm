(define-module (crates-io ch ar charclass) #:use-module (crates-io))

(define-public crate-charclass-0.1.0 (c (n "charclass") (v "0.1.0") (h "0ainyhl9pdxhw5khi5xx8f7kncl5yaqq83nm77325nv5v1lk0zi0")))

(define-public crate-charclass-0.2.0 (c (n "charclass") (v "0.2.0") (h "12j8m8fmrmwk8gsw5zy0kp9f4zla8rcnjnk62yvykf9wg6gwkpx8")))

