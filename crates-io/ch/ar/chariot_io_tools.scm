(define-module (crates-io ch ar chariot_io_tools) #:use-module (crates-io))

(define-public crate-chariot_io_tools-0.1.0 (c (n "chariot_io_tools") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0rqjprlp3gcardc15nb0nm4j672w3r8l1cnb6ccwb612b2xy0abq")))

(define-public crate-chariot_io_tools-0.1.1 (c (n "chariot_io_tools") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0r292v424kincrinngnr9hh9rwgp1fhag2jr8z2sb5bwgrh26rjx")))

(define-public crate-chariot_io_tools-0.1.2 (c (n "chariot_io_tools") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "04hp96r1ik1zl3r2r34c1ppmlxgigfcm2yjjaamqz8jmjvq4hc0m")))

(define-public crate-chariot_io_tools-0.1.3 (c (n "chariot_io_tools") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "1k086ggsr3ybr5wjyjizjpm86gazq57ws4qbjvbin6b6v6ni0sp7")))

(define-public crate-chariot_io_tools-0.1.4 (c (n "chariot_io_tools") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0c48xkn13zha6ni0cc9nn6zvg0x3yijyfr3vm76s38sar7lkzz8p")))

