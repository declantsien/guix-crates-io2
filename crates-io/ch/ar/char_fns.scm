(define-module (crates-io ch ar char_fns) #:use-module (crates-io))

(define-public crate-char_fns-0.1.0 (c (n "char_fns") (v "0.1.0") (h "1a0sflsfg2gh19q2jxrq49np029ld95kwsvvws63d92p262gq0sz")))

(define-public crate-char_fns-0.1.1 (c (n "char_fns") (v "0.1.1") (h "1c8v3g51ppbrcmh07cbxkslgwqqr8pgprb6ksc3zka7rsp30vwqf")))

