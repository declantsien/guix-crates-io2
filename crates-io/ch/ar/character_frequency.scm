(define-module (crates-io ch ar character_frequency) #:use-module (crates-io))

(define-public crate-character_frequency-0.1.0 (c (n "character_frequency") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0y0g0488zm90y924nbgpaba3l59rdl2imfpd5fyvjkqyfqs35f3s")))

(define-public crate-character_frequency-0.1.1 (c (n "character_frequency") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0x0n0516rn78f08gjq67xsizjl6mi4azwmxanmlsks7g3bhcprlr")))

(define-public crate-character_frequency-0.1.2 (c (n "character_frequency") (v "0.1.2") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0n70k497ri5l43z8ah4ppdlbhd96iyzlh1w5vy2q6fnxa54nlpw0")))

(define-public crate-character_frequency-0.1.3 (c (n "character_frequency") (v "0.1.3") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0xl5hhqb5ckvhg5k3ljkz1zhx105b9qpmz65gfqh3wdqwwcwdah8")))

(define-public crate-character_frequency-0.2.0 (c (n "character_frequency") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "168phd97gclw9sjgqsnag740ypwnsjr1dgpkzihbb48drzayx2hm")))

