(define-module (crates-io ch ar chargo) #:use-module (crates-io))

(define-public crate-chargo-0.1.0 (c (n "chargo") (v "0.1.0") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1c9hwi8mpygfkixbyhzdb0b95fj4yb5yfnbkh5iy6yj9qyw2y2fv")))

(define-public crate-chargo-0.1.1 (c (n "chargo") (v "0.1.1") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0") (d #t) (k 0)) (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1b9hsrww2z8h5fnarqcbsmhw6gh32whh2h3ddi05siw2ac4p0jys")))

(define-public crate-chargo-0.1.2 (c (n "chargo") (v "0.1.2") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0") (d #t) (k 0)) (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "06s481inxr6wb2n4rj2m02jdvk0022fi8zsy6l73yk49fdk7akk3")))

