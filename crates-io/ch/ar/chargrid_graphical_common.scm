(define-module (crates-io ch ar chargrid_graphical_common) #:use-module (crates-io))

(define-public crate-chargrid_graphical_common-0.1.0 (c (n "chargrid_graphical_common") (v "0.1.0") (h "0z30m801jvl54d4abqr068rgp2g1wjm4km5b74j53qmgrjzmvfwh")))

(define-public crate-chargrid_graphical_common-0.2.0 (c (n "chargrid_graphical_common") (v "0.2.0") (h "0gvbylwsg9dmc0s910j30f9rzrfw9aaklg0azxj0gq37fjcv0a52")))

