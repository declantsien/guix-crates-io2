(define-module (crates-io ch ar chars) #:use-module (crates-io))

(define-public crate-chars-0.0.0 (c (n "chars") (v "0.0.0") (h "01cryssycjyy8wvj07q1nhrplmnmmxkm39dliv7p36fm05n75gxs")))

(define-public crate-chars-0.3.2 (c (n "chars") (v "0.3.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "fst") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest") (r "^0.6.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.2.1") (d #t) (k 0)))) (h "0333gw2ww0q60d5r5acs8a1sgw7bdyd1vkp8adf4p32v7qhx59d5") (f (quote (("default"))))))

(define-public crate-chars-0.4.0 (c (n "chars") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chars_data") (r "^0.4.0") (d #t) (k 1)) (d (n "fst") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest") (r "^0.6.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.3.0") (d #t) (k 0)))) (h "17yv82sxppld7k4kc5bx0c45gpp7qqjab5jlm0c9h29brhl9yv4z") (f (quote (("default"))))))

(define-public crate-chars-0.4.1 (c (n "chars") (v "0.4.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chars_data") (r "^0.4.1-dev") (d #t) (k 1)) (d (n "fst") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest") (r "^0.6.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.3.0") (d #t) (k 0)))) (h "0q0jrlzp49wdgrfc6a0pqzlxxx3gkmh00987619d9w9hpy4scw14") (f (quote (("default"))))))

(define-public crate-chars-0.5.0 (c (n "chars") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chars_data") (r "^0.5.0") (d #t) (k 1)) (d (n "fst") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.4.0") (d #t) (k 0)))) (h "08iax1bgqif2lafhkrl4d6cmp63j9al428cjsgm40hj0a9ap13px") (f (quote (("default"))))))

(define-public crate-chars-0.6.0 (c (n "chars") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chars_data") (r "^0.6.0") (d #t) (k 1)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.5.0") (d #t) (k 0)))) (h "1gp8x5hbbi82xcjnnc4nrnn9a1n6ijdcnjjn8an83i55m9qp0si4") (f (quote (("default"))))))

(define-public crate-chars-0.7.0 (c (n "chars") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chars_data") (r "^0.7.0") (d #t) (k 1)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.6.0") (d #t) (k 0)))) (h "0dpziwqdsn29haz60svqb58nwx0b1g1nzp2yv26lpv4cflqg0v36") (f (quote (("default"))))))

