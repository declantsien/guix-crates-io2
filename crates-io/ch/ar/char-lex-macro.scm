(define-module (crates-io ch ar char-lex-macro) #:use-module (crates-io))

(define-public crate-char-lex-macro-0.1.0 (c (n "char-lex-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0v7r42m81r5f7ixk3p300ryb2z96h6yyr1sgp9q8izz7919m7avv")))

(define-public crate-char-lex-macro-0.1.1 (c (n "char-lex-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0bjgxbghkj3y91xkm12sysb5x8wqc7cvkys2lmfiwm22sap9y4jz")))

(define-public crate-char-lex-macro-0.1.2 (c (n "char-lex-macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1llawx8whyclblhkwdbch38k3yjcl2ivcdc7ni87n4g1z0d38597")))

(define-public crate-char-lex-macro-0.1.3 (c (n "char-lex-macro") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1arjaacl9ajn52gkbw8kf6v9z6y4szgqpxm4lagpy9qp8ymc1v6a")))

(define-public crate-char-lex-macro-0.2.0 (c (n "char-lex-macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0x6psx0vvzar4cvcclmcsc7j61qyp1ifwpr0shffg934jnbxsnzj")))

(define-public crate-char-lex-macro-1.0.0 (c (n "char-lex-macro") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0wnvq8hmiwq4a8r844hsi3b1nj5bwv4zhwkqsmspmy3s5h63whfk")))

(define-public crate-char-lex-macro-1.0.1 (c (n "char-lex-macro") (v "1.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0gjl8m7sq2dwnw5n8a2217a3p4qwv7cx7iiikmh8yb8j36ypy3gx")))

(define-public crate-char-lex-macro-1.0.2 (c (n "char-lex-macro") (v "1.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "19zg23p24qg3q8dydpfaghr403l32sha89djdgvfzi0b9chh7wmy")))

