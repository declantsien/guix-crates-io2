(define-module (crates-io ch ar char-lex) #:use-module (crates-io))

(define-public crate-char-lex-0.1.0 (c (n "char-lex") (v "0.1.0") (d (list (d (n "char-lex-macro") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "13vy3awpaxkvwd0hny871gz46z7lnpk3xs96gb67aqjl5k2l775p") (f (quote (("export_derive" "char-lex-macro") ("default" "export_derive"))))))

(define-public crate-char-lex-0.1.1 (c (n "char-lex") (v "0.1.1") (d (list (d (n "char-lex-macro") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0955fv9i33bzg8vdwirqyh6kj5snlb1nxlll9r6gffqicyyplx1x") (f (quote (("export_derive" "char-lex-macro") ("default" "export_derive"))))))

(define-public crate-char-lex-0.1.2 (c (n "char-lex") (v "0.1.2") (d (list (d (n "char-lex-macro") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0hqfncwhpdfzdwcvy06m8ypwahdqiq59mdypbddapmw77p5vw0dp") (f (quote (("export_derive" "char-lex-macro") ("default" "export_derive"))))))

(define-public crate-char-lex-0.1.3 (c (n "char-lex") (v "0.1.3") (d (list (d (n "char-lex-macro") (r "^0.1") (o #t) (d #t) (k 0)))) (h "01q2h9m07vdawkr00c0316g0967lswndxs0isw4nhxlf6ypsvgqi") (f (quote (("export_derive" "char-lex-macro") ("default" "export_derive"))))))

(define-public crate-char-lex-0.2.0 (c (n "char-lex") (v "0.2.0") (d (list (d (n "char-lex-macro") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0fv8i1f5ija9hfxcv6hn4f98dwnac8b2ql766931dnql7pbsayb2") (f (quote (("export_derive" "char-lex-macro") ("default" "export_derive"))))))

(define-public crate-char-lex-1.0.0 (c (n "char-lex") (v "1.0.0") (d (list (d (n "char-lex-macro") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0zd3s5v31nbdy54lhqgkd0ljv2qg1jk78p7c1z5w2ixzi9bbz13a") (f (quote (("export_derive" "char-lex-macro") ("default" "export_derive"))))))

(define-public crate-char-lex-1.0.1 (c (n "char-lex") (v "1.0.1") (d (list (d (n "char-lex-macro") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "12arpv22cf0jskr60wls4hz4ns3fsvlsb8gb7livkam8piaj4h14") (f (quote (("export_derive" "char-lex-macro") ("default" "export_derive"))))))

(define-public crate-char-lex-1.0.2 (c (n "char-lex") (v "1.0.2") (d (list (d (n "char-lex-macro") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1m0ly7zwjk0ibzbs0l4l56kqlffzlm5r9dyqn9w81ww98yki54s8") (f (quote (("export_derive" "char-lex-macro") ("default" "export_derive"))))))

(define-public crate-char-lex-1.0.3 (c (n "char-lex") (v "1.0.3") (d (list (d (n "char-lex-macro") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "13717jnvi905n8hsijzmx1fx9yllsfrqs6h4yfhz433m4p3frnmr") (f (quote (("export_derive" "char-lex-macro") ("default" "export_derive"))))))

(define-public crate-char-lex-1.0.4 (c (n "char-lex") (v "1.0.4") (d (list (d (n "char-lex-macro") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "09403pz1dyfyf6afki8xn986niaqj98yiq74i0f2g234r7i1k3wy") (f (quote (("export_derive" "char-lex-macro") ("default" "export_derive"))))))

(define-public crate-char-lex-1.0.5 (c (n "char-lex") (v "1.0.5") (d (list (d (n "char-lex-macro") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "0sp4h0mibjfyf6adjgmbsi70dlmmxp255sixrsk8mzb4wib5wl99") (f (quote (("export_derive" "char-lex-macro") ("default" "export_derive"))))))

