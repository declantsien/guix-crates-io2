(define-module (crates-io ch ar chargrid_app) #:use-module (crates-io))

(define-public crate-chargrid_app-0.1.0 (c (n "chargrid_app") (v "0.1.0") (d (list (d (n "chargrid_input") (r "^0.1") (d #t) (k 0)) (d (n "chargrid_render") (r "^0.1") (d #t) (k 0)))) (h "1qrb7gsw18ainllsi29c7232fzqciig6ykdwq3s8ydyf0h4szp9h")))

(define-public crate-chargrid_app-0.2.0 (c (n "chargrid_app") (v "0.2.0") (d (list (d (n "chargrid_input") (r "^0.2") (d #t) (k 0)) (d (n "chargrid_render") (r "^0.2") (d #t) (k 0)))) (h "1pc3mcjs9g76slqp1jbmdm139kdw8pdf2j06hq4zyr6rwp2cvim3")))

