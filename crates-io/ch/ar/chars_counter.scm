(define-module (crates-io ch ar chars_counter) #:use-module (crates-io))

(define-public crate-chars_counter-0.1.0 (c (n "chars_counter") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "0fy7kkv04f9mr96xafxz2x0y7vn6q6yvzg1kkfv3w2wdxfrxahym")))

(define-public crate-chars_counter-0.1.1 (c (n "chars_counter") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "1g5wzqpcrjh43himni894zq2vcn7lgdwm7zidbfhvyr9ms9azhp3")))

