(define-module (crates-io ch ar chargrid_gamepad) #:use-module (crates-io))

(define-public crate-chargrid_gamepad-0.1.0 (c (n "chargrid_gamepad") (v "0.1.0") (d (list (d (n "chargrid_input") (r "^0.1") (f (quote ("gamepad"))) (d #t) (k 0)) (d (n "gilrs") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "08xpj1n875n9j8n59mzlfb8r4hfjkj796k0vg1pbmllw32xc3dpy")))

(define-public crate-chargrid_gamepad-0.1.1 (c (n "chargrid_gamepad") (v "0.1.1") (d (list (d (n "chargrid_input") (r "^0.1") (f (quote ("gamepad"))) (d #t) (k 0)) (d (n "gilrs") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1hsmi2li9hzgb337pf9xf8b23c3j92az2072i3xv4qj3hi9hzal0")))

(define-public crate-chargrid_gamepad-0.2.0 (c (n "chargrid_gamepad") (v "0.2.0") (d (list (d (n "chargrid_input") (r "^0.2") (f (quote ("gamepad"))) (d #t) (k 0)) (d (n "gilrs") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14jq2qga77218jssik7h6bnhpxaa4riqmfzm3f0lb0jw902h6hfi")))

(define-public crate-chargrid_gamepad-0.3.0 (c (n "chargrid_gamepad") (v "0.3.0") (d (list (d (n "chargrid_input") (r "^0.3") (f (quote ("gamepad"))) (d #t) (k 0)) (d (n "gilrs") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bjhrypmmra825m7gqmnin808zipk4ykgkk4rsqrnddcdh47n079")))

(define-public crate-chargrid_gamepad-0.3.1 (c (n "chargrid_gamepad") (v "0.3.1") (d (list (d (n "chargrid_input") (r "^0.3") (f (quote ("gamepad"))) (d #t) (k 0)) (d (n "gilrs") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1znnc2gy6pbhhks235csgk6ivf74zd94ssq5b08hbzsx24qm4qlp")))

(define-public crate-chargrid_gamepad-0.4.0 (c (n "chargrid_gamepad") (v "0.4.0") (d (list (d (n "chargrid_input") (r "^0.4") (f (quote ("gamepad"))) (d #t) (k 0)) (d (n "gilrs") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1iaqvq71slnk86h7kdp3ky29l2q74fiwmkhpbwhq2jlxlmg33dwl")))

(define-public crate-chargrid_gamepad-0.5.0 (c (n "chargrid_gamepad") (v "0.5.0") (d (list (d (n "chargrid_input") (r "^0.5") (f (quote ("gamepad"))) (d #t) (k 0)) (d (n "gilrs") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1jwc5nqwl624fmjh21vimfnfhy94cb3pvinswxicxb61pwn1k6g5")))

(define-public crate-chargrid_gamepad-0.6.0 (c (n "chargrid_gamepad") (v "0.6.0") (d (list (d (n "chargrid_input") (r "^0.6") (f (quote ("gamepad"))) (d #t) (k 0)) (d (n "gilrs") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "19lnfmly1p2fxxaai3x0cjmjd1g8px14vn46q3q4b8jcbc5a155i")))

