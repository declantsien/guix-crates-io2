(define-module (crates-io ch ar chargectl) #:use-module (crates-io))

(define-public crate-chargectl-0.1.0 (c (n "chargectl") (v "0.1.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("std" "derive" "help" "usage" "error-context"))) (k 0)))) (h "0r3dyqqf2lsmqh6b56c7hby48wz37ga5y2sbd24f9s1g9zjav8cc") (r "1.72")))

(define-public crate-chargectl-0.1.1 (c (n "chargectl") (v "0.1.1") (d (list (d (n "clap") (r "^4.5") (f (quote ("std" "derive" "help" "usage" "error-context"))) (k 0)))) (h "1is8smdw42r808n90g66xipx8h19k5hcfpbn6yii64333gyl0wak") (r "1.72")))

