(define-module (crates-io ch ar charlie_buffalo) #:use-module (crates-io))

(define-public crate-charlie_buffalo-1.0.0 (c (n "charlie_buffalo") (v "1.0.0") (d (list (d (n "chrono") (r "^0") (d #t) (k 2)) (d (n "rmp-serde") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "085217571pmca40xm44phr5hg8cvlqbi8gh3cvcw92bp5n3zbvsy")))

(define-public crate-charlie_buffalo-1.0.1 (c (n "charlie_buffalo") (v "1.0.1") (d (list (d (n "chrono") (r "^0") (d #t) (k 2)) (d (n "rmp-serde") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1m8jx5mjkb8w8c7rn4x7dadfr4kz0sw7hdnk6dhdaraxdznwyil5")))

(define-public crate-charlie_buffalo-1.0.2 (c (n "charlie_buffalo") (v "1.0.2") (d (list (d (n "chrono") (r "^0") (d #t) (k 2)) (d (n "rmp-serde") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1r52fs0j8f3bkmg2a2mdcfdfnxv9cp022vkgfkizv2cbsj5d10rd")))

