(define-module (crates-io ch ar charcoal) #:use-module (crates-io))

(define-public crate-charcoal-0.0.0 (c (n "charcoal") (v "0.0.0") (h "0zwwxq166hrzdw3mc3j84bvsflc5l828jwhsvdgfjgan9sn26ivl")))

(define-public crate-charcoal-1.0.0 (c (n "charcoal") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.5") (k 0)) (d (n "granite") (r "^1.0") (f (quote ("arrayvec"))) (d #t) (k 0)))) (h "0dpn5hq11lvqh0q9sl1y8m5rc5q9axs750mr51haxwzds7zxzk4h") (f (quote (("unwind_safety" "std") ("union_optimizations" "granite/union_optimizations") ("std" "alloc") ("smallvec" "granite/smallvec") ("slotmap" "granite/slotmap") ("slab" "granite/slab") ("quadtree") ("octree") ("freeform_tree") ("doc_cfg") ("default" "std" "unwind_safety" "alloc" "binary_tree" "octree" "quadtree" "freeform_tree") ("binary_tree") ("alloc"))))))

(define-public crate-charcoal-1.1.0 (c (n "charcoal") (v "1.1.0") (d (list (d (n "arrayvec") (r "^0.5") (k 0)) (d (n "granite") (r "^1.0") (f (quote ("arrayvec"))) (d #t) (k 0)))) (h "1f2vldb2zc0w8lyddysf93mk596ffqr6vhs5468qvgh3zpjpa54x") (f (quote (("unwind_safety" "std") ("union_optimizations" "granite/union_optimizations") ("std" "alloc") ("smallvec" "granite/smallvec") ("slotmap" "granite/slotmap") ("slab" "granite/slab") ("quadtree") ("octree") ("freeform_tree") ("doc_cfg") ("default" "std" "unwind_safety" "alloc" "binary_tree" "octree" "quadtree" "freeform_tree") ("binary_tree") ("alloc"))))))

