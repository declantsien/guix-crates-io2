(define-module (crates-io ch ar chargrid_runtime) #:use-module (crates-io))

(define-public crate-chargrid_runtime-0.1.0 (c (n "chargrid_runtime") (v "0.1.0") (d (list (d (n "chargrid_core") (r "^0.1") (d #t) (k 0)))) (h "0cvxb47xrpq940y756binpz7nwl1dlpzk1mvg9gcfkaqzpphjba4") (f (quote (("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_runtime-0.2.0 (c (n "chargrid_runtime") (v "0.2.0") (d (list (d (n "chargrid_core") (r "^0.2") (d #t) (k 0)))) (h "0g4c0y9sxk71dqnpfi8zhfs9mgc3ycwi1iqr8ccpi1z1z90lbf7b") (f (quote (("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_runtime-0.3.0 (c (n "chargrid_runtime") (v "0.3.0") (d (list (d (n "chargrid_core") (r "^0.3") (d #t) (k 0)))) (h "07fdscfjqx5plzmwh2b6bivja48bar26m5gk19bgkf9lz3lr9dpj") (f (quote (("gamepad" "chargrid_core/gamepad"))))))

(define-public crate-chargrid_runtime-0.4.0 (c (n "chargrid_runtime") (v "0.4.0") (d (list (d (n "chargrid_core") (r "^0.4") (d #t) (k 0)))) (h "1d0vvmd7yigir626nkhhlr8n0nnchlxncbys9apcv9wbwcflcsrc") (f (quote (("gamepad" "chargrid_core/gamepad"))))))

