(define-module (crates-io ch ar chariot_slp) #:use-module (crates-io))

(define-public crate-chariot_slp-0.1.0 (c (n "chariot_slp") (v "0.1.0") (d (list (d (n "chariot_io_tools") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.17") (d #t) (k 2)) (d (n "error-chain") (r "^0.5") (d #t) (k 0)))) (h "0gzj8m5virasan0f881iwc10jppy4x7argz45mj8sm2g9mbd3gka")))

(define-public crate-chariot_slp-0.1.1 (c (n "chariot_slp") (v "0.1.1") (d (list (d (n "chariot_io_tools") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.17") (d #t) (k 2)) (d (n "error-chain") (r "^0.5") (d #t) (k 0)))) (h "02bshmsv09sx5f9nv6svp9gzb6zxxqsmjkydc6bipl0klkqvqdzj")))

(define-public crate-chariot_slp-0.1.2 (c (n "chariot_slp") (v "0.1.2") (d (list (d (n "chariot_io_tools") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.17") (d #t) (k 2)) (d (n "error-chain") (r "^0.5") (d #t) (k 0)))) (h "17618iz1lh4ablmhivv5grh2nhjnm2fjjghha3lzjbnpdff6dhzq")))

