(define-module (crates-io ch ar charjpoet) #:use-module (crates-io))

(define-public crate-charjpoet-0.1.0 (c (n "charjpoet") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h6kqm1xyc369ykznx0ssgv6acgcmpmxfw1q4nr0v152zwzl5c67")))

