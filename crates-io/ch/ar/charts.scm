(define-module (crates-io ch ar charts) #:use-module (crates-io))

(define-public crate-charts-0.1.0 (c (n "charts") (v "0.1.0") (d (list (d (n "svg") (r "^0.7.1") (d #t) (k 0)))) (h "0r30n1jwi3ikpzlj24gmih33va33l6sb4x3k622309ml8wz679xx")))

(define-public crate-charts-0.2.0 (c (n "charts") (v "0.2.0") (d (list (d (n "svg") (r "^0.7.1") (d #t) (k 0)))) (h "139yzka22zknw3lhjjg9jggj356kmjw05gh72vrhxhhlxdhv9l6q")))

(define-public crate-charts-0.3.0 (c (n "charts") (v "0.3.0") (d (list (d (n "format_num") (r "^0.1.0") (d #t) (k 0)) (d (n "svg") (r "^0.7.1") (d #t) (k 0)))) (h "1rmjn9wxr2cihrgs46wv5sdadwnqq5f4x8pmfknly62l4h81f5kd")))

