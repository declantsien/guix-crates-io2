(define-module (crates-io ch ar char_reader) #:use-module (crates-io))

(define-public crate-char_reader-0.1.0 (c (n "char_reader") (v "0.1.0") (h "01qsxd27jq8xhg0vbsiqdd92lyjdr56wlvgb487vdb97694ngaid")))

(define-public crate-char_reader-0.1.1 (c (n "char_reader") (v "0.1.1") (h "0fpdhx6061a92fj3kw11rwv2zkdlx8z59gbkq7baf762vqi9p99p")))

