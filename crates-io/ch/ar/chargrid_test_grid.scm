(define-module (crates-io ch ar chargrid_test_grid) #:use-module (crates-io))

(define-public crate-chargrid_test_grid-0.1.0 (c (n "chargrid_test_grid") (v "0.1.0") (d (list (d (n "chargrid_render") (r "^0.1") (d #t) (k 0)))) (h "0pm5v249hnjc46djhmndp25f08i28ylar46dfqwkpnxaj20m1v5g")))

(define-public crate-chargrid_test_grid-0.2.0 (c (n "chargrid_test_grid") (v "0.2.0") (d (list (d (n "chargrid_render") (r "^0.2") (d #t) (k 0)))) (h "0fmmp1g83jk94jiff9is430lvgpzcwmhjbj2xwwmxlmrgwsm5xk5")))

