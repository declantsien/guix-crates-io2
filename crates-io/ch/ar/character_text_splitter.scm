(define-module (crates-io ch ar character_text_splitter) #:use-module (crates-io))

(define-public crate-character_text_splitter-0.1.0 (c (n "character_text_splitter") (v "0.1.0") (h "1j0bs05d7a06bbjgqhz1044bmgkylyjbkf5ylirar8s8q9zwkjgf")))

(define-public crate-character_text_splitter-0.1.1 (c (n "character_text_splitter") (v "0.1.1") (h "1vb9dmzci63sx2kdnbwk0f9psqgy6344xzaffq75mk08l3npva75")))

(define-public crate-character_text_splitter-0.1.2 (c (n "character_text_splitter") (v "0.1.2") (h "1jmdflv2fjrcfl0z1ln5nnwwkapf06l1xgf69fa0g630fc8xirw1")))

(define-public crate-character_text_splitter-0.1.3 (c (n "character_text_splitter") (v "0.1.3") (h "06lr64h3d5kj5jqsysc6rb6dfmbh586zcb7fz7g54gk69z4mhq52")))

