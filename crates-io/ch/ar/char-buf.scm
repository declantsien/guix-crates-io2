(define-module (crates-io ch ar char-buf) #:use-module (crates-io))

(define-public crate-char-buf-0.1.0 (c (n "char-buf") (v "0.1.0") (h "06z748my3mzmqcm47a6yc62xg4087q9gfkqimr8bndh8f3v2mw2f") (y #t)))

(define-public crate-char-buf-0.1.1 (c (n "char-buf") (v "0.1.1") (h "0dpmhfi8lzf89ipalbs2jlclrvlw3a5k304706r86vnz7mnig2ry") (y #t)))

(define-public crate-char-buf-0.1.2 (c (n "char-buf") (v "0.1.2") (h "1n116812j72933lgk045z0kv84ia992am7xwsvp8ga02viy4zj78")))

