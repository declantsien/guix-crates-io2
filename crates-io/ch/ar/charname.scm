(define-module (crates-io ch ar charname) #:use-module (crates-io))

(define-public crate-charname-0.1.0 (c (n "charname") (v "0.1.0") (d (list (d (n "phf") (r "^0.7.22") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.22") (d #t) (k 1)))) (h "0px6h1rdcs5468dyrw3an8kkkv6wlmbgn0f9c8x90mdcwpsl033y")))

(define-public crate-charname-0.1.1 (c (n "charname") (v "0.1.1") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "0vmhaz1glhxf7cnbwm3m9dkmdf20nf4qqzlfj2qhdnhzlr3y6yls")))

(define-public crate-charname-0.1.2 (c (n "charname") (v "0.1.2") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "1f0x60by2nz60np5835826dnybwvm1jfwc7f0xx7flbl7kvnqkkg")))

(define-public crate-charname-0.1.3 (c (n "charname") (v "0.1.3") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "14nw6lm8jdfn7d0aigmp63jiml1wywyhplz3i8w2cgawjjskxxx6") (y #t)))

(define-public crate-charname-0.2.0 (c (n "charname") (v "0.2.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "1p76lab60bi0sv04gd5jkh2qbn7hinhygdn9dlf8vpk7b3qnb14q")))

(define-public crate-charname-0.2.1 (c (n "charname") (v "0.2.1") (d (list (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 1)))) (h "19x2y53zy5dykrjx66hvljf1sy4g8909ldzbc9cq84cb9kxyri7y")))

(define-public crate-charname-0.2.14 (c (n "charname") (v "0.2.14") (d (list (d (n "phf") (r "^0.10.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10.0") (d #t) (k 1)))) (h "0yvs0y1f6m0vdj7958rx08k32b8ag14b3wixzqvl30blpsdk1i7x")))

(define-public crate-charname-0.2.15 (c (n "charname") (v "0.2.15") (d (list (d (n "phf") (r "^0.11.1") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11.1") (d #t) (k 1)))) (h "1qc0r9aqip29ba0q8i4khhlliv9yjvzm15zyizxnc2w4ypkhpqls")))

