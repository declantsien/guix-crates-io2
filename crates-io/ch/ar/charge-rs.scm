(define-module (crates-io ch ar charge-rs) #:use-module (crates-io))

(define-public crate-charge-rs-0.1.0 (c (n "charge-rs") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "119vizqv01v2kfj3ffl00k96akzlqypgylpbb25hvzw11zkb23b8")))

