(define-module (crates-io ch ar charo) #:use-module (crates-io))

(define-public crate-charo-0.1.0 (c (n "charo") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "diesel") (r "^2.0.3") (f (quote ("sqlite" "returning_clauses_for_sqlite_3_35"))) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "0bkzpfmfm06s49hqc3q078fbib9rh037fc8id3n7czwyb9s5d4aj") (y #t)))

(define-public crate-charo-0.1.1 (c (n "charo") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "diesel") (r "^2.0.3") (f (quote ("sqlite" "returning_clauses_for_sqlite_3_35"))) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "0sw42npaln7i0a2m952j7acmj9jqp5i5q0ym5x6rhayfgwqs4lfc") (y #t)))

