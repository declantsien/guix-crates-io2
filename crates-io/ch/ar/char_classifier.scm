(define-module (crates-io ch ar char_classifier) #:use-module (crates-io))

(define-public crate-char_classifier-0.0.0 (c (n "char_classifier") (v "0.0.0") (d (list (d (n "aster") (r "^0.16") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "panini") (r "^0.0") (d #t) (k 0)) (d (n "panini_codegen") (r "^0.0") (d #t) (k 0)) (d (n "panini_macros") (r "^0.0") (d #t) (k 0)) (d (n "quasi") (r "^0.10") (d #t) (k 0)) (d (n "quasi_macros") (r "^0.10") (d #t) (k 0)))) (h "12l404sxgaf5lz7f6lw6pgzagkwbspk7winvlsfvlkrzl1cf59z6")))

